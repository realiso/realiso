<?php
// inclui biblioteca de gera��o de documentos word
require_once("../../../src/formats/word/FWDWord.php");

// documento word
$soWord = new FWDWord("new_doc_text", "testetext");

// estilo 1
$soStyle1 = new FWDWordStyle();
$soStyle1->setFont("Courier");
$soStyle1->setSize("12");
$soStyle1->setColor(255,0,0);
$soStyle1->setAlignment("center");

// estilo 2
$soStyle2 = new FWDWordStyle();
$soStyle2->setFont("Arial");
$soStyle2->setSize("10");
$soStyle2->setItalic(true);
$soStyle2->setColor(0,255,0);
$soStyle2->setAlignment("right");

// estilo 3
$soStyle3 = new FWDWordStyle();
$soStyle3->setFont("Helvetica");
$soStyle3->setSize("8");
$soStyle3->setBold(true);
$soStyle3->setColor(0,0,255);
$soStyle3->setAlignment("justify");

// bloco de texto 1
$soBlock = new FWDWordTextBlock();
$soBlock->setStyle($soStyle3);

// texto 1
$soText = new FWDWordText();
$soText->setText("Em busca dos consumidores que controlam a quantidade de calorias ingeridas, a Coca-Cola lan�ou uma lata de refrigerante de tamanho menor, que mede o conte�do pela quantidade de calorias. A embalagem, que segue uma tend�ncia nos EUA, cont�m 100 calorias do refrigerante. Isso corresponde a 226 mililitros, 124 a menos do que a lata convencional, de 350 ml. A Coca-Cola tamb�m lan�ou a Sprite e a Cherry Coke (Coca-Cola com gosto de cereja) na embalagem de cem calorias.Segundo o porta-voz da companhia nos Estados Unidos, Scott Williamson, o novo tamanho � apenas uma quest�o de conveni�ncia, j� que, na maioria dos lugares, o pre�o do refrigerante nos dois tamanhos de lata � muito similar.Divulga��o.Nova lata tem 224 ml, ao inv�s dos 350 da lata convencional.Al�m da Coca-Cola, biscoitos, salgadinhos, pipoca e at� iogurte tamb�m j� existem em embalagens de cem calorias. A Kraft, fabricante dos biscoitos Oreo, Chips Ahoy e Nabisco, lan�ou seus pacotes de cem calorias no ano passado.Aa m�gica do marketing dos novos produtos, segundo especialista � a facilidade de calcular a quantidade de calorias ingeridas.De acordo com Tom Vierhile, da Datamonitor's ProductScam, as embalagens individuais reduzem a culpa que os consumidores sentem quando \"belisca\" esses alimentos.");

// adicionando a tabela no bloco
$soBlock->addObj($soText);

// bloco de texto 2
$soBlock2 = new FWDWordTextBlock();
$soBlock2->setStyle($soStyle2);

// texto 2
$soText2 = new FWDWordText();
$soText2->setText("Em busca dos consumidores que controlam a quantidade de calorias ingeridas, a Coca-Cola lan�ou uma lata de refrigerante de tamanho menor, que mede o conte�do pela quantidade de calorias. A embalagem, que segue uma tend�ncia nos EUA, cont�m 100 calorias do refrigerante. Isso corresponde a 226 mililitros, 124 a menos do que a lata convencional, de 350 ml. A Coca-Cola tamb�m lan�ou a Sprite e a Cherry Coke (Coca-Cola com gosto de cereja) na embalagem de cem calorias.Segundo o porta-voz da companhia nos Estados Unidos, Scott Williamson, o novo tamanho � apenas uma quest�o de conveni�ncia, j� que, na maioria dos lugares, o pre�o do refrigerante nos dois tamanhos de lata � muito similar.Divulga��o.Nova lata tem 224 ml, ao inv�s dos 350 da lata convencional.Al�m da Coca-Cola, biscoitos, salgadinhos, pipoca e at� iogurte tamb�m j� existem em embalagens de cem calorias. A Kraft, fabricante dos biscoitos Oreo, Chips Ahoy e Nabisco, lan�ou seus pacotes de cem calorias no ano passado.Aa m�gica do marketing dos novos produtos, segundo especialista � a facilidade de calcular a quantidade de calorias ingeridas.De acordo com Tom Vierhile, da Datamonitor's ProductScam, as embalagens individuais reduzem a culpa que os consumidores sentem quando \"belisca\" esses alimentos.");

// adicionando a tabela no bloco
$soBlock2->addObj($soText2);

// bloco de texto 3
$soBlock3 = new FWDWordTextBlock();
$soBlock3->setStyle($soStyle1);

// texto 3
$soText3 = new FWDWordText();
$soText3->setText("Em busca dos consumidores que controlam a quantidade de calorias ingeridas, a Coca-Cola lan�ou uma lata de refrigerante de tamanho menor, que mede o conte�do pela quantidade de calorias. A embalagem, que segue uma tend�ncia nos EUA, cont�m 100 calorias do refrigerante. Isso corresponde a 226 mililitros, 124 a menos do que a lata convencional, de 350 ml. A Coca-Cola tamb�m lan�ou a Sprite e a Cherry Coke (Coca-Cola com gosto de cereja) na embalagem de cem calorias.Segundo o porta-voz da companhia nos Estados Unidos, Scott Williamson, o novo tamanho � apenas uma quest�o de conveni�ncia, j� que, na maioria dos lugares, o pre�o do refrigerante nos dois tamanhos de lata � muito similar.Divulga��o.Nova lata tem 224 ml, ao inv�s dos 350 da lata convencional.Al�m da Coca-Cola, biscoitos, salgadinhos, pipoca e at� iogurte tamb�m j� existem em embalagens de cem calorias. A Kraft, fabricante dos biscoitos Oreo, Chips Ahoy e Nabisco, lan�ou seus pacotes de cem calorias no ano passado.Aa m�gica do marketing dos novos produtos, segundo especialista � a facilidade de calcular a quantidade de calorias ingeridas.De acordo com Tom Vierhile, da Datamonitor's ProductScam, as embalagens individuais reduzem a culpa que os consumidores sentem quando \"belisca\" esses alimentos.");

// adicionando a tabela no bloco
$soBlock3->addObj($soText3);

// adicionando o bloco no documento
$soWord->addBlock($soBlock);
$soWord->addBlock($soBlock2);
$soWord->addBlock($soBlock3);

// gera documento
$soWord->output();
?>