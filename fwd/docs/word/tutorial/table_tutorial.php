<?php
// inclui biblioteca de gera��o de documentos word
require_once("../../../src/formats/word/FWDWord.php");

// documento word
$soWord = new FWDWord("new_doc_table", "testetable");

// estilo 1
$soStyle1 = new FWDWordStyle();
$soStyle1->setFont("Courier");
$soStyle1->setSize("12");
$soStyle1->setColor(255,0,0);
$soStyle1->setAlignment("center");

// estilo 2
$soStyle2 = new FWDWordStyle();
$soStyle2->setFont("Arial");
$soStyle2->setSize("16");
$soStyle2->setItalic(true);
$soStyle2->setColor(0,255,0);
$soStyle2->setAlignment("right");

// estilo 3
$soStyle3 = new FWDWordStyle();
$soStyle3->setFont("Helvetica");
$soStyle3->setSize("10");
$soStyle3->setBold(true);
$soStyle3->setColor(0,0,255);
$soStyle3->setAlignment("justify");

// bloco de texto
$soBlock = new FWDWordTextBlock();

// tabela
$soTable = new FWDWordTable();
$soTable->setBorder(1);

for($siI=1;$siI<=25;$siI++) {

	// linha
	$soRow = new FWDWordRow();
	$soRow->setHeight(30);
	if(!($siI%2))$soRow->setBgColor(200,100,30);

	for($siJ=1;$siJ<=3;$siJ++) {
		// coluna
		$soCol = new FWDWordColumn();
		$soCol->setWidth(200);
		$soCol->setStyle(${"soStyle".$siJ});
		if(!($siI%2))$soCol->setBgColor(255,255,0);
			
		// textos para serem adicionados nas colunas
		$soText = new FWDWordText("linha $siI coluna $siJ");
			
		// adiciona texto nas colunas
		$soCol->setValue($soText);
			
		// adicionando colunas na linha
		$soRow->addColumn($soCol);
	}

	// adicionando a linha na tabela
	$soTable->addRow($soRow);
}

// adicionando a tabela no bloco
$soBlock->addObj($soTable);

// adicionando o bloco no documento
$soWord->addBlock($soBlock);

// gera documento
$soWord->output();
?>