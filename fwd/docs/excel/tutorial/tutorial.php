<?php
//	if(!$base_ref) {
$base_ref = "../../../src/";
//	}
require_once($base_ref."/formats/excel/FWDExcel.php");

// cria um arquivo excel
$soTeste = new FWDExcel();

// seta o nome do arquivo
$soTeste->setFilename("tutorial");

// cria um estilo com id = "s1"
$soStyle = new FWDExcelStyle("s1");

// cria um objeto para alinhamento e adiciona ao estilo "s1"
$soAlign = new FWDExcelStyleAlignment();
$soAlign->SetAttributes("Center", "Center", false, -90);
$soStyle->setAlignment($soAlign);

// cria um objeto para fonte e adiciona ao estilo "s1"
$soFont = new FWDExcelStyleFont();
$soFont->setAttributes("Helvetica", 10, array(0,255,0), "Single", "", 1, 1, 0);
$soStyle->setFont($soFont);

// cria um objeto para interior e adiciona ao estilo "s1"
$soInterior = new FWDExcelStyleInterior();
$soInterior->setAttributes(array(255,0,0), "Gray0625", array(0,255,0));
$soStyle->setInterior($soInterior);
	
// cria um objeto para bordas e adiciona ao estilo "s1"
$soBorders = new FWDExcelStyleBorders();
$soBorders->setExternalBorders("Continuous", array(0,0,0));
$soStyle->setBorders($soBorders);

// cria um estilo com id = "s2"
$soStyle2= new FWDExcelStyle("s2");

// cria um objeto para formatos de n�mero e adiciona ao estilo "s2"
$soNF = new FWDExcelStyleNumberFormat();
$soNF->SetAttributes("Number", "", 0, true);
$soStyle2->setNumberFormat($soNF);

// cria um objeto para fonte e adiciona ao estilo "s1"
$soFont2 = new FWDExcelStyleFont();
$soFont2->setAttributes("Courier", 14, array(0,0,255), "Double", "", 0, 0, 0);
$soStyle2->setFont($soFont2);

// cria um estilo com id = "s3"
$soStyle3= new FWDExcelStyle("s3");

// cria um objeto para formatos de n�mero e adiciona ao estilo "s3"
$soNF2 = new FWDExcelStyleNumberFormat();
$soNF2->SetAttributes("Currency", "Dollar", 5);
$soStyle3->setNumberFormat($soNF2);

$soFont3 = new FWDExcelStyleFont();
$soFont3->setAttributes("Courier", 8, array(0,255,255), "", "", 1, 1, 1);
$soStyle3->setFont($soFont3);

// adiciona os estilos criados ao documento
$soTeste->addStyle($soStyle);
$soTeste->addStyle($soStyle2);
$soTeste->addStyle($soStyle3);

// cria um worksheet chamado "Planilha1"
$soWorkSheet = new FWDExcelWorksheet("Planilha1");

// cria uma tabela para o worksheet
$soTable = new FWDExcelWorksheetTable();

// cria um coluna com o estilo "s1" e adiciona na tabela
$soCol = new FWDExcelColumn();
$soCol->setCellStyleId("s1");
$soTable->addColumn($soCol);

// adiciona mais duas colunas com largura de 150 na tabela
$soTable->addColumns(array(150,150));

// cria uma linha
$soRow1 = new FWDExcelRow();

// cria 3 c�lulas
$soCell1 = new FWDExcelCell(0);
$soCell2 = new FWDExcelCell("B�n�n�");
$soCell3 = new FWDExcelCell("123434");

// adiciona as c�lulas criadas na linha
$soRow1->addCell($soCell1);
$soRow1->addCell($soCell2);
$soRow1->addCell($soCell3);

// adiciona a linha na tabela
$soTable->addRow($soRow1);

// cria uma linha
$soRow2 = new FWDExcelRow();
$soRow2->setCellStyleId("s2");

// cria 3 c�lulas
$soCell4 = new FWDExcelCell("124334");
$soCell5 = new FWDExcelCell("C�nour�");
$soCell6 = new FWDExcelCell("128734");

// adiciona as c�lulas criadas na linha
$soRow2->addCell($soCell4);
$soRow2->addCell($soCell5);
$soRow2->addCell($soCell6);

// adiciona a linha na tabela
$soTable->addRow($soRow2);

// cria uma linha
$soRow3 = new FWDExcelRow();

// seta a altura da linha como 50
$soRow3->setHeight(50);

// cria 3 c�lulas
$soCell7 = new FWDExcelCell("TESTE");
$soCell7->setStyleId("s3");
$soCell8 = new FWDExcelCell("Uva");
$soCell8->setLink("http://www.axur.com.br"); // seta um link para a c�lula
$soCell9 = new FWDExcelCell("128734");

// adiciona as c�lulas criadas na linha
$soRow3->addCell($soCell7);
$soRow3->addCell($soCell8);
$soRow3->addCell($soCell9);

// adiciona a linha na tabela
$soTable->addRow($soRow3);

// adiciona a tabela ao worksheet
$soWorkSheet->setTable($soTable);

// adiciona o worksheet ao document
$soTeste->addWorksheet($soWorkSheet);

// cria um worksheet chamado "Planilha2"
$soTeste->addWorksheet(new FWDExcelWorksheet("Planilha2"));

// cria um worksheet chamado "Planilha3"
$soTeste->addWorksheet(new FWDExcelWorksheet("Planilha3"));

// cria um worksheet chamado "Planilha4"
$soTeste->addWorksheet(new FWDExcelWorksheet("Planilha4"));

// cria o documento XLS
$soTeste->output();
?>