<?php

function libxml_display_error($psError){
	$msReturn = "<br/>\n";
	switch ($psError->level) {
		case LIBXML_ERR_WARNING:
			$msReturn .= "<b>Warning $psError->code</b>: ";
			break;
		case LIBXML_ERR_ERROR:
			$msReturn .= "<b>Error $psError->code</b>: ";
			break;
		case LIBXML_ERR_FATAL:
			$msReturn .= "<b>Fatal Error $psError->code</b>: ";
			break;
	}
	$msReturn .= trim($psError->message);
	if ($psError->file) {
		$msReturn .=    " in <b>$psError->file</b>";
	}
	$msReturn .= " on line <b>$psError->line</b>\n";
	return $msReturn;
}

function libxml_display_errors() {
	$msErrors = libxml_get_errors();
	foreach ($msErrors as $msError) {
		print libxml_display_error($msError);
	}
	libxml_clear_errors();
}

// Enable user error handling
libxml_use_internal_errors(true);

function listXmlFiles($msPath,$level=0){
	// Para controle de recursividade descomente a linha abaixo.
	//if($level > 4) return array();
	$maFiles = array();
	$moDir = dir($msPath);
	while(false!==($msFile=$moDir->read())){
		$msFullPath = $msPath.'/'.$msFile;
		if(is_file($msFullPath) && substr($msFile,strlen($msFile)-4)=='.xml') $maFiles[] = $msFullPath;
		elseif($msFile != '.' && $msFile != '..' && is_dir($msFullPath)) $maFiles = array_merge($maFiles,listXmlFiles($msFullPath,$level+1));
	}
	return $maFiles;
}

/*
 $ssPath = '../../../isms2/src/';
 /*/
$ssPath = '../../../isms_incident/src/';
//*/
$ssFiles = listXmlFiles($ssPath);
$siValidos = 0;
$siInvalidos = 0;

$soXML = new DOMDocument();

echo "<table border='1'>";

for($siI=0;$siI<count($ssFiles);$siI++){
	$ssShortFileName = substr($ssFiles[$siI],1+strrpos($ssFiles[$siI],"/"));
	$ssString = "<tr><td><b>Arquivo</b> <a href='$ssPath/{$ssFiles[$siI]}'>{$ssShortFileName}</a></td>";
	//$soXML->load($ssPath.'/'.$ssFiles[$siI]);
	$soXML->load($ssFiles[$siI]);
	if($soXML->schemaValidate('FWD.xsd')){
		$siValidos++;
		/* Comentar as duas linhas abaixo para n�o mostrar v�lidos */
		//echo $ssString;
		//echo '<td><span style="font-weight:bolder;color:green;">V&Aacute;LIDO</span></td></tr>';
	}else{
		$siInvalidos++;
		echo $ssString;
		echo '<td><span style="font-weight:bolder;color:red;">INV&Aacute;LIDO</span>';
		echo '<br><b>DOMDocument::schemaValidate() Generated Errors!</b>';
		libxml_display_errors();
		echo '</td></tr>';
	}
}

echo "</table>";

echo "v&aacute;lidos: $siValidos<br>inv&aacute;lidos: $siInvalidos";

?>