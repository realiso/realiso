<?php
/**
 * ACL - ACL MANAGER
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ACLTagsExtractor.
 *
 * <p>Classe que extrai os tags de ACL de arquivos php e xml.</p>
 *
 * @package ACL
 * @subpackage classes
 */
class ACLTagsExtractor extends FWDXMLParser {

	/**
	 * Lista das tags encontradas e dos arquivos onde foram encontradas
	 * @var array
	 * @access protected
	 */
	protected $caTags;

	/**
	 * Indica o arquivo que est� sendo analizado (usado durante o parse do arquivo).
	 * @var string
	 * @access protected
	 */
	protected $csFileBeingParsed = '';

	/**
	 * Lista os arquivos php e xml de uma pasta.
	 *
	 * <p>Percorre uma pasta recursivamente buscando arquivos php e xml e retorna
	 * uma lista com o caminho de cada um deles.</p>
	 * @access protected
	 * @param string $psPath Pasta a ser percorrida
	 * @return array Lista com os caminhos dos arquivos
	 */
	protected function listFiles($psPath){
		$maFiles = array();
		$maSubDirs = array();
		$moDir = dir($psPath);
		while(($msFileName=$moDir->read())!==false){
			$msFullPath = "$psPath/$msFileName";
			if(is_file($msFullPath)){
				$msExt = strtolower(substr($msFileName,strrpos($msFileName,'.')+1));
				if($msExt=='php' || $msExt=='xml') $maFiles[] = $msFullPath;
			}elseif($msFileName!='.' && $msFileName!='..'){
				$maSubDirs[] = $msFileName;
			}
		}
		$moDir->close();
		foreach($maSubDirs as $msDirName){
			$msFullPath = "$psPath/$msDirName";
			$maFiles = array_merge($maFiles,$this->listFiles($msFullPath));
		}
		return $maFiles;
	}

	/**
	 * Extrai os atributos 'tag' de um arquivo XML.
	 *
	 * <p>Extrai os atributos 'tag' de um arquivo XML e as inclui na lista.</p>
	 * @access protected
	 * @param string $psFileName Arquivo XML
	 * @return boolean True, indica sucesso
	 */
	protected function extractTagsFromXMLFile($psFileName){
		$this->csFileBeingParsed = $psFileName;
		$mbParse = $this->parseFile($psFileName);
		$this->csFileBeingParsed = '';
		return $mbParse;
	}

	/**
	 * Adiciona uma tag � lista.
	 *
	 * <p>Adiciona uma tag � lista.</p>
	 * @access protected
	 * @param string $psTag Tag
	 */
	protected function addTag($psTag){
		if(!isset($this->caTags[$psTag])){
			$this->caTags[$psTag] = array();
		}
		if(isset($this->caTags[$psTag][$this->csFileBeingParsed])){
			$this->caTags[$psTag][$this->csFileBeingParsed]++;
		}else{
			$this->caTags[$psTag][$this->csFileBeingParsed] = 1;
		}
	}

	/**
	 * Retorna a lista de tags.
	 *
	 * <p>Retorna a lista de tags.</p>
	 * @access public
	 * @return array Lista das tags encontradas e dos arquivos onde foram encontradas
	 */
	public function getTags(){
		return $this->caTags;
	}

	/**
	 * Extrai as tags de um arquivo PHP
	 *
	 * <p>Extrai as tags de um arquivo PHP e as inclui na lista. N�o � poss�vel
	 * encontrar 100% das tags usadas no PHP, pois elas podem ser obtidas atrav�s
	 * de express�es complexas envolvendo fun��es e vari�veis, por isso, s�o
	 * consideradas tags, as strings constantes que batem com a express�o
	 * [A-Z]+(\.[A-Z]+)*(\.[0-9]+)*.</p>
	 * @access protected
	 * @param string $psFileName Arquivo PHP
	 * @return boolean True, indica sucesso
	 */
	protected function extractTagsFromPHPFile($psFileName){
		if(!file_exists($psFileName) || !is_readable($psFileName)){
			trigger_error("Could not open file '$psFileName'.",E_USER_WARNING);
			return false;
		}
		$this->csFileBeingParsed = $psFileName;

		$mrFile = fopen($psFileName,'r');
		if(filesize($psFileName)>0){
			$msFileContent = fread($mrFile,filesize($psFileName));
		}else{
			$msFileContent = '';
		}
		fclose($mrFile);

		preg_match_all('/[\'"]([A-Z]+(\.[A-Z]+)*(\.[0-9]+)*)[\'"]/',$msFileContent,$maMatches);
		foreach($maMatches[1] as $msTag){
			$this->addTag($msTag);
		}
		$this->csFileBeingParsed = '';
		return true;
	}

	/**
	 * Extrai as tags contidas em todos arquivos php e xml de um diret�rio.
	 *
	 * <p>Extrai as tags contidas em todos arquivos php e xml de um diret�rio
	 * (e sub-diret�rios) e as inclui na lista.</p>
	 * @access public
	 * @param string $psFullPath Arquivo ou pasta
	 */
	public function extractTagsFromFiles($psFullPath){
		if(!file_exists($psFullPath) || !is_readable($psFullPath)){
			trigger_error("Could not open directory '$psFullPath'.",E_USER_ERROR);
		}elseif(!is_dir($psFullPath)){
			trigger_error("'$psFullPath' is not a directory.",E_USER_ERROR);
		}else{
			$maFiles = $this->listFiles($psFullPath);
			foreach($maFiles as $msFilePath){
				$msExt = strtolower(substr($msFilePath,strrpos($msFilePath,'.')+1));
				if($msExt=='xml'){
					$this->extractTagsFromXMLFile($msFilePath);
				}else{
					$this->extractTagsFromPHPFile($msFilePath);
				}
			}
		}
	}

	/**
	 * Handler chamado quando o parser encontra uma tag de in�cio de elemento
	 *
	 * <p>Handler chamado quando o parser encontra uma tag de in�cio de elemento.</p>
	 * @access protected
	 * @param resource $prParser Parser
	 * @param string $psTagName Nome da tag
	 * @param array $paAttributes Array associativo contendo os atributos do elemento
	 */
	protected function startHandler($prParser, $psTagName, $paAttributes){
		if(isset($paAttributes['TAG'])){
			$maTags = explode(':',$paAttributes['TAG']);
			foreach($maTags as $msTag){
				if(preg_match('/^[A-Z]+(\.[A-Z]+)*(\.[0-9]+)*$/',$msTag)){
					$this->addTag($msTag);
				}
			}
		}
	}

	/**
	 * Handler chamado quando o parser encontra texto dentro de um elemento
	 *
	 * <p>Handler chamado quando o parser encontra texto dentro de um elemento.</p>
	 * @access protected
	 * @param resource $prParser Parser
	 * @param string $psData Conte�do de texto do elemento
	 */
	protected function dataHandler($prParser, $psData){}

	/**
	 * Handler chamado quando o parser encontra uma tag de fim de elemento
	 *
	 * <p>Handler chamado quando o parser encontra uma tag de fim de elemento.</p>
	 * @access protected
	 * @param resource $prParser Parser
	 * @param string $psTagName Nome da tag
	 */
	protected function endHandler($prParser, $psTagName){}

}

?>