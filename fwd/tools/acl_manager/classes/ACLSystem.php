<?php
/**
 * ACL - ACL MANAGER
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ACLSystem.
 *
 * <p></p>
 * @package ACL
 * @subpackage classes
 */
class ACLSystem extends FWDDBTable {

	public function __construct(){
		parent::__construct('acl_system');

		$this->csAliasId = 'system_id';

		$this->coDataset->addFWDDBField(new FWDDBField('pkSystem','system_id'  ,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField('sName'   ,'system_name',DB_STRING));
	}

}

?>