<?php
/**
 * ACL - ACL MANAGER
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryCopySystem.
 *
 * <p>Query para copiar todos dados de um sistema para outro, assumindo que o
 * segundo esteja vazio.</p>
 * @package ACL
 * @subpackage handlers
 */
class QueryCopySystem extends FWDDBQueryHandler {

	protected $ciSystemFrom = 0;
	protected $ciSystemTo = 0;

	public function setSystemFrom($piSystemFrom){
		$this->ciSystemFrom = $piSystemFrom;
	}

	public function setSystemTo($piSystemTo){
		$this->ciSystemTo = $piSystemTo;
	}

	public function makeQuery(){
		if($this->ciSystemFrom && $this->ciSystemTo){
			$this->csSQL = "INSERT INTO acl_acl(fksystem, pkacl, fkparent, sdescription, stag, sname, norder)
                      SELECT {$this->ciSystemTo}, pkacl, fkparent, sdescription, stag, sname, norder
                      FROM acl_acl
                      WHERE fksystem = {$this->ciSystemFrom};

                      INSERT INTO acl_profile(fksystem, pkprofile, sname)
                      SELECT {$this->ciSystemTo}, pkprofile, sname
                      FROM acl_profile
                      WHERE fksystem = {$this->ciSystemFrom};

                      INSERT INTO acl_profile_acl(fksystem, fkprofile, fkacl)
                      SELECT {$this->ciSystemTo}, fkprofile, fkacl
                      FROM acl_profile_acl
                      WHERE fksystem = {$this->ciSystemFrom};";
		}else{
			trigger_error("You must fill the ids of the two systems.",E_USER_ERROR);
		}
	}

	public static function copy($piSystemFrom,$piSystemTo){
		$moHandler = new QueryCopySystem();
		$moHandler->setSystemFrom($piSystemFrom);
		$moHandler->setSystemTo($piSystemTo);
		$moHandler->makeQuery();
		$moHandler->executeQuery();
	}

}

?>