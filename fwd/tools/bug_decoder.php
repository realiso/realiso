<?php

$base_ref = "../src/";
include_once "../src/base/setup.php";

$soWebLib = FWDWebLib::getInstance();
$soWebLib->setCSSRef($base_ref.'base/');
$soWebLib->setSysRef($base_ref);
$soWebLib->setGfxRef($base_ref . "gfx/");
$soWebLib->setSysJsRef($base_ref);

class DecodePostEvent extends FWDRunnable {
	public function run(){
		$msEncodedPost = FWDWebLib::getObject('encoded_post')->getValue();
		$maPost = FWDWebLib::unserializeString(base64_decode($msEncodedPost));
		$msDump = str_replace(array("\n","'"),array('\\n',"\'"),var_export($maPost,true));
		echo "gebi('post').value='$msDump';";
	}
}

class OutputMessageEvent extends FWDRunnable {
	public function run(){
		$msEncodedPost = FWDWebLib::getObject('input')->getValue();
		$strDecoded = base64_decode($msEncodedPost);
		$str = str_replace("&nbsp;", "", $strDecoded);
		$str = str_replace("\r", "  ", $str);
		$str = str_replace("\n", "  ", $str);
		$str = addslashes($str);
		$str =  strip_tags($str);
		echo "gobi('output').setValue('$str');";
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(''));
		$moStartEvent->addAjaxEvent(new DecodePostEvent('decode_post'));
		$moStartEvent->addAjaxEvent(new OutputMessageEvent('outmessage_event'));
		$moWebLib = FWDWebLib::getInstance();
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('bug_decoder.xml');

?>