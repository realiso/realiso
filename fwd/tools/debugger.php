<?php
set_time_limit(3600);
$base_ref = "../src/";
include_once "../src/base/setup.php";

$soWebLib = FWDWebLib::getInstance();
$soWebLib->setCSSRef($base_ref.'base/');
$soWebLib->setSysRef($base_ref);
$soWebLib->setGfxRef($base_ref . "gfx/");
$soWebLib->setSysJsRef($base_ref);

session_start();

class FilterEvent extends FWDRunnable {
	public function run() {
		$msOutput=gzuncompress($_SESSION["FILE_CONTENTS"]);
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(''));
		$moStartEvent->addAjaxEvent(new FilterEvent('filter_event'));
	}
}

class ScreenEvent extends FWDRunnable {
	public function run() {
		$moFile = FWDWebLib::getObject('file_input');
		if($moFile->isUploaded()) {
			if($moFile->getTempFileName() && $moFile->getErrorCode()==FWDFile::E_NONE) {
				$msDebug = file_get_contents($moFile->getTempFileName());
				preg_match_all('/#%#([dDfFsS].*)\n#%#/sU',$msDebug,$maLines);
				if (isset($maLines[1])) {
					$moCrypt = new FWDCrypt();
					$msOutput = "";
					foreach($maLines[1] as $msLine) {
						$msType = substr($msLine,0,1);
						$msLine = substr($msLine,1);
						if ($msType == 'D' || $msType == 'F') {
							$moCrypt->setIV(substr($msLine,0,strlen($moCrypt->getIV())));
							$msLine = substr($msLine,strlen($moCrypt->getIV()));
							$msLine = $moCrypt->decrypt($msLine);
						} elseif ($msType == 'S') {
							$msLine = base64_decode($msLine);
						}
						preg_match('/\((.*)\)/U',$msLine,$maDbgType);
						$msColorClass = isset($maDbgType[1])?$maDbgType[1]:"DEBUG";

						$msOutput .= "<pre wrap=hard>";
						$msOutput .= "<font class=FWDDebugColor_{$msColorClass}>{$msLine}</font>";
						$msOutput .= "</pre>";
					}
					$_SESSION["FILE_CONTENTS"] = gzcompress($msOutput);
					$maSelectedItems = FWDWebLib::getObject('filter_controller')->getAllItemsCheck();
					foreach($maSelectedItems as $moItem) {
						$moItem->setAttrCheck('false');
					}
					FWDWebLib::getObject('output')->setValue($msOutput);
					FWDWebLib::getObject('hide_viewgroup')->setAttrDisplay('true');
				}
			}
		}
		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
<script language="javascript">  
      function changecss(theClass,element,value) {
        msCssRules='';
        if (document.all) {
         msCssRules = 'rules';
        }
        else if (document.getElementById) {
         msCssRules = 'cssRules';
        }
        for (var miI=0; miI < document.styleSheets.length; miI++){
          for (var miJ=0; miJ < document.styleSheets[miI][msCssRules].length; miJ++) {
            if (document.styleSheets[miI][msCssRules][miJ].selectorText == theClass) {
              document.styleSheets[miI][msCssRules][miJ].style[element] = value;
            }
          }
        }
        miI=miJ=msCssRules=theClass=element=value='';
      }
    </script>
		<?
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('debugger.xml');

?>