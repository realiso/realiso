<?

$sys_ref = "";
$sys_ref_based_on_tab_main = "";

$classes_ref = "classes/";
$interfaces_ref = "interfaces/";
$handlers_ref = "handlers/";

$states_ref = $classes_ref . 'states/';

$lib_ref = $sys_ref."lib/";
$base_ref = $sys_ref."../../src/";
include $base_ref . "base/setup.php";

include $interfaces_ref . "IClassBuilder.php";
include $interfaces_ref . "IXMLBuilder.php";
include $interfaces_ref . "IImageBuilder.php";
include $interfaces_ref . "IFieldsEditor.php";
include $interfaces_ref . "IFilterEditor.php";
include $interfaces_ref . "ITableQueryBuilder.php";
include $interfaces_ref . "ITableClassBuilder.php";
include $interfaces_ref . "IContextNavBuilder.php";
include $interfaces_ref . "IReportBuilder.php";

include $classes_ref . "FWDFieldsEditor.php";
include $classes_ref . "FWDWizardLib.php";
include $classes_ref . "FWDWizard.php";
include $classes_ref . "FWDLinearWizard.php";
include $classes_ref . "FWDWizardSession.php";
include $classes_ref . "FWDDimensionsExtractor.php";
include $classes_ref . "FWDIconBuilder.php";
include $classes_ref . "FWDWizardState.php";
include $classes_ref . "FWDQueryBuilder.php";
include $classes_ref . "FWDReportBuilder.php";

include $states_ref . "FWDStateClassBasics.php";
include $states_ref . "FWDStateConfirmFinish.php";
include $states_ref . "FWDStateFIDAdjust.php";
include $states_ref . "FWDStateIconDraw.php";
include $states_ref . "FWDStateEditFields.php";
include $states_ref . "FWDStateEditFilters.php";
include $states_ref . "FWDStateEditReportFilters.php";
include $states_ref . "FWDStateSetQuery.php";
include $states_ref . "FWDStateSetupTableClass.php";
include $states_ref . "FWDStateContextNavOptions.php";
include $states_ref . "FWDStateReportBasics.php";
include $states_ref . "FWDStateEditReportLevels.php";

$soWeblib = FWDWebLib::getInstance();

$moConfig = new FWDConfig($sys_ref.'config.php');
if(!$moConfig->load()){
	trigger_error('error_config_corrupt',E_USER_ERROR);
}

$soWebLib = FWDWebLib::getInstance();
//$soWebLib->setCSSRef($sys_ref."lib/");
$soWebLib->setSysRef($sys_ref);
//$soWebLib->setGfxRef($sys_ref."gfx/");
//$soWebLib->setSysJsRef($sys_ref."lib/");
//$soWebLib->setSysRefBasedOnTabMain($sys_ref_based_on_tab_main);

$soWizardLib = FWDWizardLib::getInstance();

$soWizardLib->setSystemRef('../../../isms_ci/src/');

$soWizardLib->addModule('isms','',                     '',                      'grid/',            'select/'            );
$soWizardLib->addModule('rm',  'packages/risk/',       'risk_management/',      'grid/',            'select/'            );
$soWizardLib->addModule('pm',  'packages/policy/',     'policy_management/',    'grid/policy/',     'select/policy/'     );
$soWizardLib->addModule('ci',  'packages/improvement/','continual_improvement/','grid/improvement/','select/improvement/');
$soWizardLib->addModule('tst', 'packages/test/',       'test_management/',      'grid/test/',       'select/test/'       );

$soWizardLib->addPackage('ACL' ,'ACL MANAGER');
$soWizardLib->addPackage('AMS' ,'AXUR MONITOR SYSTEM');
$soWizardLib->addPackage('FWD' ,'FAST WEB DEVELOPMENT');
$soWizardLib->addPackage('ISMS','INTERNET SECURITY MANAGEMENT SYSTEM');

?>