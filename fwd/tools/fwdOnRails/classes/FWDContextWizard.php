<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

include_once $handlers_ref."QueryTables.php";
include_once $handlers_ref."QueryColumns.php";
include_once $handlers_ref."QueryForeignKeys.php";
include_once $handlers_ref."QueryStringFields.php";
include_once $handlers_ref."QueryReferences.php";

include_once $classes_ref."FWDTableBuilder.php";
include_once $classes_ref."FWDAssociationBuilder.php";
include_once $classes_ref."FWDPopUpEditBuilder.php";
include_once $classes_ref."FWDNavBuilder.php";
include_once $classes_ref."FWDQueryBuilder.php";
include_once $classes_ref."FWDSelectQueryBuilder.php";
include_once $classes_ref."FWDPopUpSearchBuilder.php";

/**
 * Classe FWDContextWizard.
 *
 * <p></p>
 * @package Rails
 * @subpackage classes
 */
class FWDContextWizard extends FWDLinearWizard implements ITableClassBuilder {

	protected $csOutputDir = 'output/';
	protected $csPrefix;
	protected $csSufix;
	protected $csTableName = '';
	protected $csLabel = '';
	protected $csPluralLabel = '';
	protected $csClassName = '';
	protected $csAliasId;
	protected $csNameField;
	protected $csNameFieldAlias;
	protected $coTableBuilder;
	protected $coPopUpEditBuilder;
	protected $coNavBuilder;
	protected $coIconBuilder;
	protected $cbMayNeedSearch = false;
	protected $cbMayNeedAssociation = false;

	protected function __construct(){
		parent::__construct();

		$this->coTableBuilder = new FWDTableBuilder();
		$this->coAssociationBuilder = new FWDAssociationBuilder();
		$this->coPopUpEditBuilder = new FWDPopUpEditBuilder();
		$this->coNavBuilder = new FWDNavBuilder();
		$this->coIconBuilder = new FWDIconBuilder(20,16);

		$this->addState('select_table'       ,'Sele��o de Tabela'                     ,new FWDStateSetupTableClass($this)                 );
		$this->addState('edit_columns'       ,'Edi��o de Colunas'                     ,new FWDStateEditFields($this->coTableBuilder)      );
		$this->addState('edit_associations'  ,'Edi��o de Associa��es'                 ,new FWDStateEditFields($this->coAssociationBuilder));
		$this->addState('edit_popup'         ,'Ajuste da PopUp de Edi��o'             ,new FWDStateFIDAdjust($this->coPopUpEditBuilder)   );
		$this->addState('edit_nav_columns'   ,'Edi��o de Colunas Comuns'              ,new FWDStateEditFields($this->coNavBuilder)        );
		$this->addState('edit_nav_fk_columns','Edi��o de Colunas de Chave Estrangeira',new FWDStateEditFields($this->coNavBuilder)        );
		$this->addState('edit_nav_options'   ,'Op��es da Grid da Nav'                 ,new FWDStateContextNavOptions($this->coNavBuilder) );
		$this->addState('edit_icon'          ,'Cria��o do �cone'                      ,new FWDStateIconDraw($this->coIconBuilder)         );
		$this->addState('end'                ,'Concluir'                              ,new FWDStateConfirmFinish()                        );
	}

	public static function getInstance(){
		if(self::$coInstance===null){
			$moSession = FWDWebLib::getInstance()->getSession(new FWDWizardSession('FWDWizard'));
			$moInstance = $moSession->getAttrWizardObject();
			if(!$moInstance){
				$msClass = get_class(new self());
				$moInstance = new $msClass;
				$moSession->setAttrWizardObject($moInstance);
			}
			self::$coInstance = $moInstance;
		}
		return self::$coInstance;
	}

	protected function getName(){
		return "Context Wizard";
	}

	public function setTableName($psTableName){
		$this->csTableName = $psTableName;
		$miPos = strpos($this->csTableName,'_');
		$this->csPrefix = substr($this->csTableName,0,$miPos);
		$this->csSufix = substr($this->csTableName,$miPos+1);

		$this->coTableBuilder->setTableName($this->csTableName);

		$this->coAssociationBuilder->setTableName($this->csTableName);

		$this->coNavBuilder->setTableName($this->csTableName);

		$this->coPopUpEditBuilder->setTableName($this->csTableName);
		$this->coPopUpEditBuilder->setContextName($this->csSufix);

		$this->coIconBuilder->setTableName($this->csTableName);
	}

	public function setClassName($psClassName){
		$this->csClassName = $psClassName;
		$this->coTableBuilder->setClassName($this->csClassName);
		$this->coNavBuilder->setClassName($this->csClassName);
	}

	public function setLabel($psLabel){
		$this->csLabel = $psLabel;
		$this->coPopUpEditBuilder->setContextLabel($this->csLabel);
		$this->coTableBuilder->setLabel($this->csLabel);
		$this->coNavBuilder->setLabel($this->csLabel);
	}

	public function setPluralLabel($psPluralLabel){
		$this->csPluralLabel = $psPluralLabel;
	}

	public function getTableName(){
		return $this->csTableName;
	}

	public function getClassName(){
		return $this->csClassName;
	}

	public function getLabel(){
		return $this->csLabel;
	}

	public function getPluralLabel(){
		return $this->csPluralLabel;
	}

	public function setAliasId($psAliasId){
		$this->csAliasId = $psAliasId;
		$this->coPopUpEditBuilder->setAliasId($psAliasId);
		$this->coNavBuilder->setAliasId($psAliasId);
	}

	public function setNameFieldAlias($psNameFieldAlias){
		$this->csNameFieldAlias = $psNameFieldAlias;
		$this->coPopUpEditBuilder->setNameFieldAlias($psNameFieldAlias);
		$this->coNavBuilder->setNameFieldAlias($psNameFieldAlias);
	}

	public function setNameField($psNameField){
		$this->csNameField = $psNameField;
		$this->coAssociationBuilder->setNameField($psNameField);
	}

	private function addToContextsInfo(){
		$msContextsInfoPath = FWDWizardLib::getInstance()->getSystemRef().'classes/contexts_info.php';
		$msNewContextsInfoPath = "{$this->csOutputDir}contexts_info.php";
		$msConstantName = "CONTEXT_".strtoupper($this->csTableName);
		require($msContextsInfoPath);
		$maContextsInfo = $laContextsInfo;
		$maExistentConstants = array();
		$miId = 0;
		foreach($laContextsInfo as $maTableInfo){
			if($maTableInfo['constant']==$msConstantName) return;
			$maExistentConstants[$maTableInfo['constant']] = true;
			$miId = max($maTableInfo['id']+1,$miId);
		}
		if(file_exists($msNewContextsInfoPath)){
			require($msNewContextsInfoPath);
			foreach($laContextsInfo as $maTableInfo){
				if(!isset($maExistentConstants[$maTableInfo['constant']])){
					$maExistentConstants[$maTableInfo['constant']] = true;
					$maTableInfo['id'] = $miId++;
					$maContextsInfo[] = $maTableInfo;
				}
			}
		}
		if(!isset($maExistentConstants[$msConstantName])){
			$maContextsInfo[] = array(
        'id' => $miId,
        'constant' => $msConstantName,
        'table' =>  $this->csTableName,
        'class' =>  $this->csClassName,
        'parameters' => array()
			);
		}
		$msContent = "<?php\n\n\$laContextsInfo = ".var_export($maContextsInfo,true).";\n\n?>";
		FWDWizardLib::createFile($msNewContextsInfoPath,$msContent);
	}

	protected function loadPostData(){
		parent::loadPostData();
		if($this->csCurrentState=='edit_columns'){
			$msIdField = $this->coTableBuilder->getSelectedField('alias_id');
			$this->setNameField($this->coTableBuilder->getSelectedField('name_field_alias'));

			$this->coPopUpEditBuilder->setup();
			$this->coNavBuilder->setup();

			$this->setAliasId($this->coTableBuilder->getFieldProperty($msIdField,'alias'));
			$this->setNameFieldAlias($this->coTableBuilder->getFieldProperty($this->csNameField,'alias'));

			// Para cada campo, preenche as propriedades do PopUpEditBuilder e do NavBuilder
			foreach($this->coTableBuilder->getFields() as $msFieldName){
				if($this->coTableBuilder->getFieldProperty($msFieldName,'element')){
					$msAlias = $this->coTableBuilder->getFieldProperty($msFieldName,'alias');
					$msElement = $this->coTableBuilder->getFieldProperty($msFieldName,'element');
					$mbRequired = $this->coTableBuilder->getFieldProperty($msFieldName,'required');
					$this->coPopUpEditBuilder->addField($msFieldName,$msAlias,$msElement,$mbRequired);
				}

				$maFieldProperties = array(
          'include' => true,
          'name' => $msFieldName,
          'alias' => $this->coTableBuilder->getFieldProperty($msFieldName,'alias'),
          'title' => $msFieldName,
          'draw_type' => '',
          'db_type' => $this->coTableBuilder->getFieldProperty($msFieldName,'type'),
          'name_column' => '',
          'display' => true,
          'canorder' => true,
          'tooltip' => false
				);
				$this->coNavBuilder->addField($msFieldName,$maFieldProperties);
			}
		}elseif($this->csCurrentState=='edit_associations'){
			foreach($this->coAssociationBuilder->getFields() as $msRelationTable){
				$msRelatedTable = $this->coAssociationBuilder->getFieldProperty($msRelationTable,'related_table');
				$msPluralLabel = $this->coAssociationBuilder->getFieldProperty($msRelationTable,'plural_label');
				$mbMenu = $this->coAssociationBuilder->getFieldProperty($msRelationTable,'menu');
				$mbPopUpEdit = $this->coAssociationBuilder->getFieldProperty($msRelationTable,'popup_edit');
				if($mbMenu){
					$this->coNavBuilder->addAssociation($msRelationTable, $msRelatedTable, $msPluralLabel);
				}
				if($mbPopUpEdit){
					$mbRequired = $this->coAssociationBuilder->getFieldProperty($msRelationTable,'required');
					$this->coPopUpEditBuilder->addAssociation($msRelationTable, $msRelatedTable, $msPluralLabel, $mbRequired);
				}
			}
			$this->coPopUpEditBuilder->buildXml();
		}
	}

	protected function drawBody(){
		if($this->csCurrentState=='edit_nav_columns'){
			$this->coNavBuilder->setForeignKeys(false);
		}elseif($this->csCurrentState=='edit_nav_fk_columns'){
			$this->coNavBuilder->setForeignKeys(true);
		}
		parent::drawBody();
	}

	protected function getPHPClass(){
		return $this->coTableBuilder->getPhp();
	}

	protected function getXMLPopUpEdit(){
		return $this->coPopUpEditBuilder->getXml();
	}

	protected function getPHPPopUpEdit(){
		return $this->coPopUpEditBuilder->getPhp();
	}

	protected function finish(){
		// Instancia o construtor da popup de search
		$moPopUpSearchBuilder = new FWDPopUpSearchBuilder($this->csTableName,$this->csClassName,$this->csLabel,$this->csAliasId,$this->csNameField,$this->csNameFieldAlias,$this->csPluralLabel);

		// Obt�m uma lista de tods refer�ncias a esta tabela
		$moQueryReferences = new QueryReferences();
		$moQueryReferences->setTableName($this->csTableName);
		$moQueryReferences->makeQuery();
		$moQueryReferences->executeQuery();
		while($moQueryReferences->fetch()){
			if($moQueryReferences->getFieldValue('is_relation')){
				$this->cbMayNeedAssociation = true;
			}else{
				$this->cbMayNeedSearch = true;
			}
		}

		// Cria o arquivo da classe do contexto
		FWDWizardLib::createFile("{$this->csOutputDir}{$this->csClassName}.php",$this->getPHPClass());
		// Cria o arquivo php da popup de edi��o
		FWDWizardLib::createFile("{$this->csOutputDir}popup_{$this->csSufix}_edit.php",$this->getPHPPopUpEdit());
		// Cria o arquivo php da popup de edi��o
		FWDWizardLib::createFile("{$this->csOutputDir}popup_{$this->csSufix}_edit.xml",$this->getXMLPopUpEdit());
		// Cria o arquivo xml da nav
		FWDWizardLib::createFile("{$this->csOutputDir}nav_{$this->csSufix}.xml",$this->coNavBuilder->getXml());
		// Cria o arquivo php da nav
		FWDWizardLib::createFile("{$this->csOutputDir}nav_{$this->csSufix}.php",$this->coNavBuilder->getPhp());
		// Cria o arquivo da query da grid da nav
		FWDWizardLib::createFile("{$this->csOutputDir}{$this->coNavBuilder->getQueryName()}.php",$this->coNavBuilder->getQueryPhp());
		// Cria os arquivos de �cone
		$this->coIconBuilder->createPng($this->csOutputDir);
		$this->coIconBuilder->createGif($this->csOutputDir);

		if($this->cbMayNeedSearch || $this->cbMayNeedAssociation){
			// Cria o arquivo da query das grids de busca (lookup e associa��o)
			FWDWizardLib::createFile("{$this->csOutputDir}{$moPopUpSearchBuilder->getQueryName()}.php",$moPopUpSearchBuilder->getQueryGrid());
			if($this->cbMayNeedSearch){
				// Cria o arquivo php da tela de busca simples (lookup)
				FWDWizardLib::createFile("{$this->csOutputDir}popup_{$this->csSufix}_search.php",$moPopUpSearchBuilder->getSingleSearchPhp());
				// Cria o arquivo xml da tela de busca simples (lookup)
				FWDWizardLib::createFile("{$this->csOutputDir}popup_{$this->csSufix}_search.xml",$moPopUpSearchBuilder->getSingleSearchXml());
			}else{
				// Cria as queries de associa��o
				$this->coAssociationBuilder->createSelectQueries($this->csOutputDir);
				// Cria o arquivo php da tela de busca m�ltipla (associa��o)
				FWDWizardLib::createFile("{$this->csOutputDir}popup_{$this->csSufix}_associate.php",$moPopUpSearchBuilder->getMultipleSearchPhp());
				// Cria o arquivo xml da tela de busca m�ltipla (associa��o)
				FWDWizardLib::createFile("{$this->csOutputDir}popup_{$this->csSufix}_associate.xml",$moPopUpSearchBuilder->getMultipleSearchXml());
			}
		}

		// Cria as queries de select que forem necess�rias
		foreach($this->coTableBuilder->getFields() as $msField){
			if($this->coTableBuilder->getFieldProperty($msField,'element')=='select'){
				$moQuery = new QueryForeignKeys();
				$moQuery->setSourceTable($this->csTableName);
				$moQuery->setSourceField($msField);

				$msReferencedTable = $moQuery->getReferencedTable();
				$msReferencedField = $moQuery->getReferencedField();

				$moQuery = new QueryStringFields();
				$moQuery->setTableName($msReferencedTable);
				$maStringFields = $moQuery->getStringFields();

				if(in_array('sname',$maStringFields)){
					$msNameField = 'sname';
				}elseif(in_array('tdescription',$maStringFields)){
					$msNameField = 'tdescription';
				}else{
					$msNameField = $maStringFields[0];
				}

				$msSelectQueryName = 'QuerySelect'.FWDWizardLib::getTitleCaseEntityName($msReferencedTable);

				$moSelectQueryBuilder = new FWDSelectQueryBuilder($msReferencedTable,$msReferencedField,$msNameField);
				$moSelectQueryBuilder->setPackage('ISMS');
				$moSelectQueryBuilder->setName($msSelectQueryName);

				$msFullPath = FWDWizardLib::getInstance()->getSystemRef().'handlers/'
				.FWDWizardLib::getInstance()->getSelectHandlersRef(FWDWizardLib::getPrefix($msReferencedTable))
				."{$msSelectQueryName}.php";
				$msOutputFullPath = "{$this->csOutputDir}{$msSelectQueryName}.php";
				if(!file_exists($msFullPath) && !file_exists($msOutputFullPath)){
					FWDWizardLib::createFile($msOutputFullPath,$moSelectQueryBuilder->getPhp());
				}
			}
		}

		// Adiciona uma linha no contexts_info.php com as informa��es sobre o contexto
		$this->addToContextsInfo();

	}

	protected function getFinishMessage(){
		$msTutorialsPath = preg_replace('|src/|','docs/tutorials/',FWDWizardLib::getInstance()->getSystemRef());
		$msSearchSuportTutorialPath = "{$msTutorialsPath}Suporte � pesquisa (para contextos e usando o Context Wizard).txt";
		$msTrashSuportTutorialPath = "{$msTutorialsPath}Suporte � Lixeira (usando o Context Wizard).txt";

		$msReturn = parent::getFinishMessage();
		$msReturn.= "<h3>Coisas que ainda precisam ser feitas manualmente:</h3>\n"
		."<ul>\n"
		."  <li>\n"
		."    Implementar os seguintes m�todos do contexto:\n"
		."    <ul>\n"
		."      <li>getDescription</li>\n"
		."      <li>getResponsible</li>\n"
		."      <li>userCanInsert</li>\n"
		."      <li>userCanEdit</li>\n"
		."      <li>userCanDelete</li>\n"
		."    </ul>\n"
		."  </li>\n"
		."  <li>Criar um �cone decente e com fundo transparente em png e em gif</li>\n"
		."  <li>Incluir testes de licen�a onde for necess�rio</li>\n"
		."  <li>Incluir testes de ACL onde for necess�rio</li>\n"
		."  <li>Incluir o arquivo <b>{$this->csClassName}.php</b> no <b>init.php</b></li>\n"
		."  <li>Implementar <a href='{$msTrashSuportTutorialPath}' target='__blank'>suporte � lixeira</a></li>\n"
		."  <li>Implementar <a href='{$msSearchSuportTutorialPath}'target='__blank'>suporte � pesquisa</a></li>\n"
		."  <li>\n"
		."    Caso o contexto possua possua alertas e/ou tarefas, incluir a constante\n"
		."    <b>CONTEXT_".strtoupper($this->csTableName)."</b> em algum tipo de email ou criar um novo tipo em\n"
		."    <b>ISMSEmailPreferences.php</b>, nesse caso � necess�rio criar uma checkbox para o novo tipo em\n"
		."    <b>popup_email_preferences.xml</b>"
		."  </li>\n"
		."</ul>\n";
		return $msReturn;
	}

	protected function getAgainButtonText(){
		return "Criar outro contexto";
	}

}

?>