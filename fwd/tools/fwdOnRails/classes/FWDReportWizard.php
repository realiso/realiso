<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

//include_once $handlers_ref."QueryColumns.php";
//include_once $classes_ref."FWDQueryBuilder.php";

/**
 * Classe FWDReportWizard.
 *
 * <p></p>
 * @package Rails
 * @subpackage classes
 */
class FWDReportWizard extends FWDLinearWizard {

	protected $csOutputDir = 'output/';
	//protected $csQuery = '';
	protected $coReportBuilder = null;

	protected function __construct(){
		parent::__construct();
		$this->coReportBuilder = new FWDReportBuilder();
		$this->addState('report_basics'    ,'Par�metros B�sicos',new FWDStateReportBasics($this->coReportBuilder)     );
		$this->addState('edit_levels'      ,'Edi��o de N�veis'  ,new FWDStateEditReportLevels($this->coReportBuilder) );
		$this->addState('edit_query_fields','Edi��o de Campos'  ,new FWDStateEditFields($this->coReportBuilder)       );
		$this->addState('fid_adjust'       ,'Ajuste de Layout'  ,new FWDStateFIDAdjust($this->coReportBuilder,true)   );
		$this->addState('edit_filters'     ,'Edi��o de Filtros' ,new FWDStateEditReportFilters($this->coReportBuilder));
		$this->addState('end'              ,'Concluir'          ,new FWDStateConfirmFinish()                          );
	}

	public static function getInstance(){
		if(self::$coInstance===null){
			$moSession = FWDWebLib::getInstance()->getSession(new FWDWizardSession('FWDWizard'));
			$moInstance = $moSession->getAttrWizardObject();
			if(!$moInstance){
				$msClass = get_class(new self());
				$moInstance = new $msClass;
				$moSession->setAttrWizardObject($moInstance);
			}
			self::$coInstance = $moInstance;
		}
		return self::$coInstance;
	}

	protected function getName(){
		return "Report Wizard";
	}
	/*
	 public function getQueryType(){
	 return $this->csQueryType;
	 }

	 public function setQueryType($psQueryType){
	 $this->csQueryType = $psQueryType;
	 }

	 public function getTableName(){
	 return $this->csTableName;
	 }

	 public function setTableName($psTableName){
	 $this->csTableName = $psTableName;
	 }

	 public function getQuery(){
	 return $this->csQuery;
	 }

	 public function setQuery($psQuery){
	 $this->csQuery = $psQuery;
	 }
	 */

	protected function loadPostData(){
		parent::loadPostData();

		if($this->csCurrentState=='edit_levels'){
			$maTypeOptions = array(
        'DB_UNKNOWN'  => 'DB_UNKNOWN',
        'DB_NUMBER'   => 'DB_NUMBER',
        'DB_STRING'   => 'DB_STRING',
        'DB_DATETIME' => 'DB_DATETIME',
			);
			$maLevels = $this->coReportBuilder->getLevels();
			$maLevelOptions = array();
			for($i=0;$i<count($maLevels);$i++){
				$maLevelOptions[$maLevels[$i]['name']] = $maLevels[$i]['name'];
			}
			$this->coReportBuilder->addProperty('alias' ,'Campo'  ,'static'  );
			$this->coReportBuilder->addProperty('type'  ,'Tipo'   ,'select'  ,$maTypeOptions);
			$this->coReportBuilder->addProperty('level' ,'N�vel'  ,'select'  ,$maLevelOptions);
			$this->coReportBuilder->addProperty('liquid','L�quida','checkbox');
			// Para cada campo extra�do da query, preenche as propriedades que ainda n�o est�o preenchidas
			foreach($this->coReportBuilder->getFields() as $msFieldName){
				$msLevel = '';
				for($i=0;$i<count($maLevels);$i++){
					$msAlias = $this->coReportBuilder->getFieldProperty($msFieldName,'alias');
					if(substr($msAlias,0,strlen($maLevels[$i]['name']))==$maLevels[$i]['name']){
						$msLevel = $maLevels[$i]['name'];
						break;
					}
				}
				$this->coReportBuilder->setFieldProperty($msFieldName,'level',$msLevel);
				if($this->coReportBuilder->getFieldProperty($msFieldName,'type')=='DB_STRING'){
					$this->coReportBuilder->setFieldProperty($msFieldName,'liquid',true);
				}else{
					$this->coReportBuilder->setFieldProperty($msFieldName,'liquid',false);
				}
			}
		}elseif($this->csCurrentState=='edit_query_fields'){
			$this->coReportBuilder->buildXml();
		}
	}

	protected function finish(){
		// Cria o arquivo da classe do relat�rio
		FWDWizardLib::createFile("{$this->csOutputDir}{$this->coReportBuilder->getReportClass()}.php",$this->coReportBuilder->getReportClassPhp());
		// Cria o arquivo da classe do filtro do relat�rio
		if($this->coReportBuilder->isFiltered()){
			FWDWizardLib::createFile("{$this->csOutputDir}{$this->coReportBuilder->getReportFilterClass()}.php",$this->coReportBuilder->getReportFilterPhp());
		}
		// Cria o arquivo php da popup do relat�rio
		FWDWizardLib::createFile("{$this->csOutputDir}{$this->coReportBuilder->getPopupName()}.php",$this->coReportBuilder->getReportPopupPhp());
		// Cria o arquivo xml da popup do relat�rio
		FWDWizardLib::createFile("{$this->csOutputDir}{$this->coReportBuilder->getPopupName()}.xml",$this->coReportBuilder->getReportPopupXml());
	}

	protected function getAgainButtonText(){
		return "Criar outro relat�rio";
	}

}

?>