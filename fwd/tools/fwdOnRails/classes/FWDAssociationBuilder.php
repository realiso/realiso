<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

include_once $handlers_ref."QueryReferences.php";
include_once $handlers_ref."QueryForeignKeys.php";
include_once $handlers_ref."QueryStringFields.php";

/**
 * Classe FWDAssociationBuilder.
 *
 * <p></p>
 * @package Rails
 * @subpackage classes
 */
class FWDAssociationBuilder extends FWDFieldsEditor {

	protected $csTableName;
	protected $csNameField;

	public function setup(){
		parent::setup();
		$this->addProperty('table'             ,'Tabela'               ,'static'  );
		$this->addProperty('menu'              ,'Item no Menu'         ,'checkbox');
		$this->addProperty('popup_edit'        ,'Associar na Edi��o'   ,'checkbox');
		$this->addProperty('required'          ,'Obrigat�rio na Edi��o','checkbox');
		$this->addProperty('plural_label'      ,'Label no Plural'      ,'text'    );
		$this->addProperty('related_name_field','Coluna de Nome'       ,'select'  );
	}

	public function setTableName($psTableName){
		$this->csTableName = $psTableName;
		$this->setup();
		// Obt�m uma lista de tods refer�ncias a esta tabela
		$moQueryReferences = new QueryReferences();
		$moQueryReferences->setTableName($this->csTableName);
		$moQueryReferences->makeQuery();
		$moQueryReferences->executeQuery();
		while($moQueryReferences->fetch()){
			if($moQueryReferences->getFieldValue('is_relation')){
				// Para cada tabela de relacionamento envolvendo esta tabela, verifica
				// se � um relacionamento entre 2 contextos e, se for, adiciona um campo pra ele
				$msColumn = $moQueryReferences->getFieldValue('column');
				$msRelationTable = $moQueryReferences->getFieldValue('table');
				$moQueryFK = new QueryForeignKeys();
				$moQueryFK->setSourceTable($msRelationTable);
				$moQueryFK->setReferencedField('fkcontext');
				$moQueryFK->makeQuery();
				$moQueryFK->executeQuery();
				$maFKs = array();
				while($moQueryFK->fetch()){
					$maFKs[] = array(
            'field' => $moQueryFK->getFieldValue('field'),
            'referenced_table' => $moQueryFK->getFieldValue('referenced_table')
					);
				}
				// S� trata as associa��es entre 2 contextos
				if(count($maFKs)==2){
					if($maFKs[0]['referenced_table']==$this->csTableName){
						$msRelatedTable = $maFKs[1]['referenced_table'];
						$msFKSelf = $maFKs[0]['field'];
						$msFKRelated = $maFKs[1]['field'];
					}else{
						$msRelatedTable = $maFKs[0]['referenced_table'];
						$msFKSelf = $maFKs[1]['field'];
						$msFKRelated = $maFKs[0]['field'];
					}

					$moQueryStringFields = new QueryStringFields();
					$moQueryStringFields->setTableName($msRelatedTable);
					$maStringFields = $moQueryStringFields->getStringFields();
					if(count($maStringFields)){
						if(in_array('sname',$maStringFields)){
							$msRelatedNameField = 'sname';
						}elseif(in_array('tdescription',$maStringFields)){
							$msRelatedNameField = 'tdescription';
						}else{
							$msRelatedNameField = $maStringFields[0];
						}
					}else{
						$msRelatedNameField = '';
					}

					$msRelatedNameField = 'sname';
					$maProperties = array(
            'table'              => $msRelationTable,
            'menu'               => true,
            'popup_edit'         => true,
            'required'           => false,
            'plural_label'       => FWDWizardLib::pluralize(FWDWizardLib::getTitleCaseEntityName($msRelatedTable)),
            'related_table'      => $msRelatedTable,
            'fk_self'            => $msFKSelf,
            'fk_related'         => $msFKRelated,
            'related_name_field' => $msRelatedNameField
					);
					$this->addField($msRelationTable,$maProperties);
					$this->setFieldPropertyOptions($msRelationTable,'related_name_field',array_combine($maStringFields,$maStringFields));
				}
			}
		}
	}

	public function setNameField($psNameField){
		$this->csNameField = $psNameField;
	}

	protected function getAssociationClassPhp($psRelationTable){

	}

	public function createSelectQueries($psOutputDir){
		foreach($this->getFields() as $msRelationTable){
			$msClassName = "QuerySelect".FWDWizardLib::getTitleCaseEntityName($msRelationTable);
			FWDWizardLib::createFile("{$psOutputDir}{$msClassName}.php",$this->getAssociationSelectQueryPhp($msRelationTable));
		}
	}

	protected function getAssociationSelectQueryPhp($psRelationTable){
		$msTitleCaseEntityName = FWDWizardLib::getTitleCaseEntityName($psRelationTable);
		$msRelationAlias = FWDWizardLib::getInstance()->getTableAcronym($psRelationTable);
		$msTable1 = $this->csTableName;
		$msTable2 = $this->getFieldProperty($psRelationTable,'related_table');
		$msAlias1 = FWDWizardLib::getInstance()->getTableAcronym($msTable1);
		$msAlias2 = FWDWizardLib::getInstance()->getTableAcronym($msTable2);
		$msFK1 = $this->getFieldProperty($psRelationTable,'fk_self');
		$msFK2 = $this->getFieldProperty($psRelationTable,'fk_related');
		$msNameField1 = $this->csNameField;
		$msNameField2 = $this->getFieldProperty($psRelationTable,'related_name_field');
		$msTCEntity1 = FWDWizardLib::getTitleCaseEntityName($msTable1);
		$msTCEntity2 = FWDWizardLib::getTitleCaseEntityName($msTable2);

		return "<?php\n"
		."/**\n"
		." * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM\n"
		." *\n"
		." * <p>These coded instructions,  technics, statements, and computer programs\n"
		." * contain  unpublished  proprietary information of  Axur Communications,\n"
		." * Inc.,  and are  protected  by applied  copyright law.  They may not be\n"
		." * disclosed to third parties, copied or duplicated in any form, in whole\n"
		." * or in part, without  the prior written consent of Axur Communications,\n"
		." * Inc.</p>\n"
		." * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem\n"
		." * informacao proprietaria  nao publicada pela Axur Communications, Inc.,\n"
		." * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem\n"
		." * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,\n"
		." * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur\n"
		." * Communications, Inc.</p>\n"
		." * @copyright Copyright (c) 2006, Axur Information Security\n"
		." * @link http://www.axur.com.br Axur Information Security\n"
		." */\n"
		."\n"
		."/**\n"
		." * Classe QuerySelect{$msTitleCaseEntityName}.\n"
		." *\n"
		." * <p></p>\n"
		." * @package ISMS\n"
		." * @subpackage handlers\n"
		." */\n"
		."class QuerySelect{$msTitleCaseEntityName} extends FWDDBQueryHandler {\n"
		."\n"
		."  protected \$ci{$msTCEntity1} = 0;\n"
		."  protected \$ci{$msTCEntity2} = 0;\n"
		."  \n"
		."  protected \$ca{$msTCEntity1}Ids = null;\n"
		."  protected \$ca{$msTCEntity2}Ids = null;\n"
		."\n"
		."  public function __construct(\$poDB=null){\n"
		."    parent::__construct(\$poDB);\n"
		."    \$this->coDataSet->addFWDDBField(new FWDDBField('','select_id'   ,DB_NUMBER));\n"
		."    \$this->coDataSet->addFWDDBField(new FWDDBField('','select_value',DB_STRING));\n"
		."  }\n"
		."\n"
		."  public function set{$msTCEntity1}(\$pi{$msTCEntity1}){\n"
		."    \$this->ci{$msTCEntity1} = \$pi{$msTCEntity1};\n"
		."  }\n"
		."\n"
		."  public function set{$msTCEntity2}(\$pi{$msTCEntity2}){\n"
		."    \$this->ci{$msTCEntity2} = \$pi{$msTCEntity2};\n"
		."  }\n"
		."\n"
		."  public function set{$msTCEntity1}Ids(\$ps{$msTCEntity1}Ids){\n"
		."    \$this->ca{$msTCEntity1}Ids = array_filter(explode(':',\$ps{$msTCEntity1}Ids));\n"
		."  }\n"
		."\n"
		."  public function set{$msTCEntity2}Ids(\$ps{$msTCEntity2}Ids){\n"
		."    \$this->ca{$msTCEntity2}Ids = array_filter(explode(':',\$ps{$msTCEntity2}Ids));\n"
		."  }\n"
		."\n"
		."  public function makeQuery(){\n"
		."    if(\$this->ci{$msTCEntity1}){\n"
		."      \$msSelectId = '{$msAlias2}.fkcontext';\n"
		."      \$msSelectName = '{$msAlias2}.{$msNameField2}';\n"
		."      \$msFrom = '{$psRelationTable} {$msRelationAlias} JOIN view_{$msTable2}_active {$msAlias2} ON ({$msAlias2}.fkcontext = {$msRelationAlias}.{$msFK2})';\n"
		."      \$msWhere = \"{$msRelationAlias}.{$msFK1} = {\$this->ci{$msTCEntity1}}\";\n"
		."    }elseif(\$this->ci{$msTCEntity2}){\n"
		."      \$msSelectId = '{$msAlias1}.fkcontext';\n"
		."      \$msSelectName = '{$msAlias1}.{$msNameField1}';\n"
		."      \$msFrom = '{$psRelationTable} {$msRelationAlias} JOIN view_{$msTable1}_active {$msAlias1} ON ({$msAlias1}.fkcontext = {$msRelationAlias}.{$msFK1})';\n"
		."      \$msWhere = \"{$msRelationAlias}.{$msFK2} = {\$this->ci{$msTCEntity2}}\";\n"
		."    }elseif(\$this->ca{$msTCEntity1}Ids!==null){\n"
		."      \$msSelectId = '{$msAlias1}.fkcontext';\n"
		."      \$msSelectName = '{$msAlias1}.{$msNameField1}';\n"
		."      \$msFrom = 'view_{$msTable1}_active {$msAlias1}';\n"
		."      if(count(\$this->ca{$msTCEntity1}Ids)>0){\n"
		."        \$msWhere = '{$msAlias1}.fkcontext IN ('.implode(',',\$this->ca{$msTCEntity1}Ids).')';\n"
		."      }else{\n"
		."        \$msWhere = '1=0';\n"
		."      }\n"
		."    }elseif(\$this->ca{$msTCEntity2}Ids!==null){\n"
		."      \$msSelectId = '{$msAlias2}.fkcontext';\n"
		."      \$msSelectName = '{$msAlias2}.{$msNameField2}';\n"
		."      \$msFrom = 'view_{$msTable2}_active {$msAlias2}';\n"
		."      if(count(\$this->ca{$msTCEntity2}Ids)>0){\n"
		."        \$msWhere = '{$msAlias2}.fkcontext IN ('.implode(',',\$this->ca{$msTCEntity2}Ids).')';\n"
		."      }else{\n"
		."        \$msWhere = '1=0';\n"
		."      }\n"
		."    }else{\n"
		."      trigger_error(\"You must specify a filter.\",E_USER_ERROR);\n"
		."    }\n"
		."    \n"
		."    \$this->csSQL = \"SELECT\n"
		."                      {\$msSelectId} AS select_id,\n"
		."                      {\$msSelectName} AS select_value\n"
		."                    FROM {\$msFrom}\n"
		."                    WHERE {\$msWhere}\n"
		."                    ORDER BY select_value\";\n"
		."  }\n"
		."  \n"
		."  public function getIds(){\n"
		."    \$this->makeQuery();\n"
		."    \$this->executeQuery();\n"
		."    \$maIds = array();\n"
		."    while(\$this->fetch()){\n"
		."      \$maIds[] = \$this->getFieldValue('select_id');\n"
		."    }\n"
		."    return \$maIds;\n"
		."  }\n"
		."\n"
		."}\n"
		."\n"
		."?>";
	}

}

?>