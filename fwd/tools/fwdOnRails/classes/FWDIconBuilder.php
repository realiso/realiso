<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDIconBuilder.
 *
 * <p></p>
 * @package Rails
 * @subpackage classes
 */
class FWDIconBuilder implements IImageBuilder {

	protected $csTableName;
	protected $caPixels;
	protected $ciHeight;
	protected $ciWidth;

	public function __construct($piHeight,$piWidth){
		$this->ciHeight = $piHeight;
		$this->ciWidth = $piWidth;
		$this->caPixels = array();
	}

	public function setTableName($psTableName){
		$this->csTableName = $psTableName;
	}

	public function getHeight(){
		return $this->ciHeight;
	}

	public function getWidth(){
		return $this->ciWidth;
	}

	public function setPixels($psPixels){
		$this->caPixels = array();
		$maLines = explode('|',$psPixels);
		for($i=0;$i<count($maLines);$i++){
			$this->caPixels[$i] = array();
			$maPixels = explode(',',$maLines[$i]);
			for($j=0;$j<count($maPixels);$j++){
				$maPixel = explode(':',chunk_split($maPixels[$j],2,':'));
				unset($maPixel[3]);
				$maPixel = array_map('hexdec',$maPixel);
				$this->caPixels[$i][$j] = $maPixel;
			}
		}
	}

	public function getPixels(){
		if(count($this->caPixels)==0){
			return '';
		}else{
			$maLines = array();
			for($i=0;$i<$this->ciHeight;$i++){
				$maLines[$i] = array();
				for($j=0;$j<$this->ciWidth;$j++){
					$maPixel = $this->caPixels[$i][$j];
					for($k=0;$k<3;$k++){
						$maPixel[$k] = ($maPixel[$k]<16?'0'.dechex($maPixel[$k]):dechex($maPixel[$k]));
					}
					$maLines[$i][$j] = implode('',$maPixel);
				}
				$maLines[$i] = implode(',',$maLines[$i]);
			}
			return implode('|',$maLines);
		}
	}

	private function bitmap2image($bitmap){
		$height = count($bitmap);
		$width = count($bitmap[0]);
		$image = imagecreatetruecolor($width,$height);
		for($i=0;$i<$height;$i++){
			for($j=0;$j<$width;$j++){
				if(-1==$index=imagecolorexact($image,$bitmap[$i][$j][0],$bitmap[$i][$j][1],$bitmap[$i][$j][2])){
					$index = imagecolorallocate($image,$bitmap[$i][$j][0],$bitmap[$i][$j][1],$bitmap[$i][$j][2]);
				}
				imagesetpixel($image,$j,$i,$index);
			}
		}
		return $image;
	}

	public function createGif($psTargetDir){
		$msPath = $psTargetDir.FWDWizardLib::getIconName($this->csTableName,'gif');
		imagegif($this->bitmap2image($this->caPixels),$msPath);
	}

	public function createPng($psTargetDir){
		$msPath = $psTargetDir.FWDWizardLib::getIconName($this->csTableName,'png');
		imagepng($this->bitmap2image($this->caPixels),$msPath);
	}

}

?>