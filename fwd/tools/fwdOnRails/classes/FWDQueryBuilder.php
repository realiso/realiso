<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDQueryBuilder.
 *
 * <p></p>
 * @package Rails
 * @subpackage classes
 */
class FWDQueryBuilder extends FWDFieldsEditor implements IClassBuilder, IFilterEditor {

	protected $csName = '';
	protected $csTableName = '';
	protected $csFrom = '';
	protected $csQuery;
	protected $caOrderBy;

	protected $cbGenerateGetters = false;

	protected $csDescription = '';
	protected $csPackageName = '';
	protected $csPackageAcronym = '';

	protected $caFilters;
	protected $csFilterProperties = '';
	protected $csFilterMethods = '';
	protected $csFilterMakeQuery = '';

	public function __construct($psTableName='',$psClass='',$psLabel=''){
		$this->csTableName = $psTableName;
		$this->caFields = array();
		$this->caFilters = array();
		$this->caOrderBy = array();
	}

	public function setTableName($psTableName){
		$this->csTableName = $psTableName;
	}

	public function getTableName(){
		return $this->csTableName;
	}

	public function setFrom($psFrom){
		$this->csFrom = $psFrom;
	}

	public function setDescription($psDescription){
		$this->csDescription = $psDescription;
	}

	public function getDescription(){
		return $this->csDescription;
	}

	public function setPackage($psPackageAcronym){
		$this->csPackageAcronym = $psPackageAcronym;
		$this->csPackageName = FWDWizardLib::getInstance()->getPackageName($psPackageAcronym);
	}

	public function setPackageName($psPackageName){
		$this->csPackageName = $psPackageName;
	}

	public function setPackageAcronym($psPackageAcronym){
		$this->csPackageAcronym = $psPackageAcronym;
	}

	public function getPackageAcronym(){
		return $this->csPackageAcronym;
	}

	public function clearFilters(){
		$this->caFilters = array();
		$this->csFilterProperties = '';
		$this->csFilterMethods = '';
		$this->csFilterMakeQuery = '';
	}

	protected function getFieldTypeByAlias($psAlias){
		$msFieldName = '';
		foreach($this->getFields() as $msName){
			$msAlias = $this->getFieldProperty($msName,'alias');
			if($msAlias==$psAlias){
				$msFieldName = $msName;
				break;
			}
		}
		if($msFieldName && isset($this->caFields[$msFieldName])){
			return $this->getFieldProperty($msFieldName,'type');
		}else{
			return null;
		}
	}

	public function addFilter($psAlias,$psFilterType,$psFilterName,$paAditionalProperties=null){
		$msFieldName = '';
		foreach($this->getFields() as $msName){
			$msAlias = $this->getFieldProperty($msName,'alias');
			if($msAlias==$psAlias){
				$msFieldName = $msName;
				break;
			}
		}
		if(isset($this->caFields[$msFieldName])){
			$mbError = true;
			$msFieldType = $this->getFieldProperty($msFieldName,'type');
			if(!$paAditionalProperties){
				$paAditionalProperties = array();
			}
			$maFilter = array_merge(
			$paAditionalProperties,
			array(
          'field' => $msAlias,
          'type' => $psFilterType,
          'name' => $psFilterName,
          'property' => ''
          )
          );
          switch($msFieldType){
          	case 'DB_NUMBER':{
          		switch($psFilterType){
          			case '=':
          			case '<':
          			case '>':
          			case '<=':
          			case '>=':{
              $maFilter['property'] = "ci{$psFilterName}";
              $this->csFilterProperties.= "  protected \$ci{$psFilterName} = 0;\n\n";
              $this->csFilterMethods.= FWDWizardLib::getSetMethod("ci{$psFilterName}");
              if($this->cbGenerateGetters){
              	$this->csFilterMethods.= FWDWizardLib::getGetMethod("ci{$psFilterName}");
              }
              $this->csFilterMakeQuery.= "    if(\$this->ci{$psFilterName}){\n"
              ."      \$maFilters[] = \"{$msFieldName} {$psFilterType} {\$this->ci{$psFilterName}}\";\n"
              ."    }\n"
              ."    \n";
              $mbError = false;
              break;
          			}
          			case 'IN':{
              $maFilter['property'] = "ca{$psFilterName}";
              $this->csFilterProperties.= "  protected \$ca{$psFilterName} = array();\n\n";
              $this->csFilterMethods.= FWDWizardLib::getSetMethod("ca{$psFilterName}");
              if($this->cbGenerateGetters){
              	$this->csFilterMethods.= FWDWizardLib::getGetMethod("ca{$psFilterName}");
              }
              $this->csFilterMakeQuery.= "    if(count(\$this->ca{$psFilterName})>0){\n"
              ."      \$maFilters[] = '{$msFieldName} IN ('.implode(',',\$this->ca{$psFilterName}).')';\n"
              ."    }\n"
              ."    \n";
              $mbError = false;
              break;
          			}
          			case 'NOT IN':{
              $maFilter['property'] = "ca{$psFilterName}";
              $this->csFilterProperties.= "  protected \$ca{$psFilterName} = array();\n\n";
              $this->csFilterMethods.= FWDWizardLib::getSetMethod("ca{$psFilterName}");
              if($this->cbGenerateGetters){
              	$this->csFilterMethods.= FWDWizardLib::getGetMethod("ca{$psFilterName}");
              }
              $this->csFilterMakeQuery.= "    if(count(\$this->ca{$psFilterName})>0){\n"
              ."      \$maFilters[] = '{$msFieldName} NOT IN ('.implode(',',\$this->ca{$psFilterName}).')';\n"
              ."    }\n"
              ."    \n";
              $mbError = false;
              break;
          			}
          		}
          	}
          			case 'DB_STRING':{
          				switch($psFilterType){
          					case 'LIKE':{
          						$maFilter['property'] = "cs{$psFilterName}";
          						$this->csFilterProperties.= "  protected \$cs{$psFilterName} = '';\n\n";
          						$this->csFilterMethods.= FWDWizardLib::getSetMethod("cs{$psFilterName}");
          						if($this->cbGenerateGetters){
          							$this->csFilterMethods.= FWDWizardLib::getGetMethod("cs{$psFilterName}");
          						}
          						$this->csFilterMakeQuery.= "    if(\$this->cs{$psFilterName}){\n"
          						."      \$maFilters[] = \"UPPER({$msFieldName}) LIKE '%\".strtoupper(\$this->cs{$psFilterName}).\"%'\";\n"
          						."    }\n"
          						."    \n";
          						$mbError = false;
          						break;
          					}
          				}
          			}
          					case 'DB_DATETIME':{
          						/*
          						 switch($psFilterType){
          						 case '<': case '>': case '<=': case '>=':{

          						 $mbError = false;
          						 break;
          						 }
          						 }
          						 */
          					}
          }
          $this->caFilters[] = $maFilter;
          if($mbError){
          	trigger_error("Filter '$psFilterType' is not defined for type '$msFieldType'.",E_USER_WARNING);
          }
		}else{
			trigger_error("No such field '$psAlias'.",E_USER_WARNING);
		}
	}

	public function getFieldTypes(){
		$maFieldTypes = array();
		foreach($this->getFields() as $msName){
			$msAlias = $this->getFieldProperty($msName,'alias');
			$msType = $this->getFieldProperty($msName,'type');
			$maFieldTypes[$msAlias] = $msType;
		}
		return $maFieldTypes;
	}

	public function getFilters(){
		return $this->caFilters;
	}

	public function addOrderBy($psField){
		$this->caOrderBy[] = $psField;
	}

	public function setName($psName){
		$this->csName = $psName;
	}

	public function getName(){
		return $this->csName;
	}

	public function getFieldsCreation(){
		$maMaxLenghts = array('name'=>0,'alias'=>0,'type'=>0);
		foreach($this->caFields as $maColumnInfo){
			$maMaxLenghts['name'] = max($maMaxLenghts['name'],strlen($maColumnInfo['name']));
			$maMaxLenghts['alias'] = max($maMaxLenghts['alias'],strlen($maColumnInfo['alias']));
			$maMaxLenghts['type'] = max($maMaxLenghts['type'],strlen($maColumnInfo['type']));
		}
		$msCode = "";
		foreach($this->caFields as $maColumnInfo){
			$msCode.= "    \$this->coDataSet->addFWDDBField(new FWDDBField("
			."'{$maColumnInfo['name']}'".str_repeat(' ',$maMaxLenghts['name'] - strlen($maColumnInfo['name'])).","
			."'{$maColumnInfo['alias']}'".str_repeat(' ',$maMaxLenghts['alias'] - strlen($maColumnInfo['alias'])).","
			."{$maColumnInfo['type']}".str_repeat(' ',$maMaxLenghts['type'] - strlen($maColumnInfo['type']))."));\n";
		}
		return $msCode;
	}

	public function setQuery($psQuery){
		$this->csQuery = $psQuery;

		$msRegExpLine = '/^[\s]*([^#]*)[\s]+AS[\s]+([^,\n\s]+)[\n\s]*$/i';
		$msRegExpPrefix = '/^([a-zA-Z]+\.)?([a-zA-Z0-9]+)$/';
		$maTypes = array(
      'b' => 'DB_NUMBER',
      'n' => 'DB_NUMBER',
      'pk' => 'DB_NUMBER',
      'fk' => 'DB_NUMBER',
      'd' => 'DB_DATETIME',
      's' => 'DB_STRING',
      't' => 'DB_STRING'
      );
      $msQuery = preg_replace('/[\r\n\t]+/',' ',$psQuery);
      $msUpperQuery = strtoupper($msQuery);
      if(strpos($msUpperQuery,' FROM ')!==false){
      	$miStart = strpos($msUpperQuery,'SELECT ') + 7;
      	$miEnd = strpos($msUpperQuery,' FROM ');
      	$msQuery = substr($msQuery, $miStart, $miEnd - $miStart);
      }
      $maLines = explode(',',$msQuery);
      foreach($maLines as $msLine){
      	if(preg_match($msRegExpLine,$msLine,$maMatches)){
        $maFieldInfo = array(
          'name' => '',
          'alias' => $maMatches[2],
          'type' => 'DB_UNKNOWN',
        );
        $msColExpression = $maMatches[1];
        if(preg_match($msRegExpPrefix,$msColExpression,$maMatches)){
        	$maFieldInfo['name'] = $msColExpression;
        	$msPrefix = FWDWizardLib::getFieldPrefix($maMatches[2]?$maMatches[2]:$maMatches[1]);
        	if(isset($maTypes[$msPrefix])){
        		$maFieldInfo['type'] = $maTypes[$msPrefix];
        	}
        }
        $this->addField($msColExpression,$maFieldInfo);
      	}
      }
	}

	public function getQuery(){
		if($this->csQuery){
			return $this->csQuery;
		}elseif(($this->csTableName || $this->csFrom) && count($this->caFields)>0){
			$maLines = array();
			foreach($this->caFields as $maColumnInfo){
				$maLines[] = "                      {$maColumnInfo['name']} AS {$maColumnInfo['alias']}";
			}
			if($this->csFrom){
				$msFrom = $this->csFrom;
			}else{
				$msFrom = "view_{$this->csTableName}_active";
			}
			$msCode = "SELECT\n".implode(",\n",$maLines)."\n                    FROM {$msFrom}";
			if(count($this->caOrderBy)){
				$msCode.= "\n                    ORDER BY ".implode(',',$this->caOrderBy);
			}
			return $msCode;
		}else{
			return '';
		}
	}

	public function getPhpFileHeader(){
		return "<?php\n"
		."/**\n"
		." * {$this->csPackageAcronym} - {$this->csPackageName}\n"
		." *\n"
		." * <p>These coded instructions,  technics, statements, and computer programs\n"
		." * contain  unpublished  proprietary information of  Axur Communications,\n"
		." * Inc.,  and are  protected  by applied  copyright law.  They may not be\n"
		." * disclosed to third parties, copied or duplicated in any form, in whole\n"
		." * or in part, without  the prior written consent of Axur Communications,\n"
		." * Inc.</p>\n"
		." * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem\n"
		." * informacao proprietaria  nao publicada pela Axur Communications, Inc.,\n"
		." * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem\n"
		." * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,\n"
		." * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur\n"
		." * Communications, Inc.</p>\n"
		." * @copyright Copyright (c) 2006, Axur Information Security\n"
		." * @link http://www.axur.com.br Axur Information Security\n"
		." */\n"
		."\n";
	}

	public function getPhpHeader(){
		$msName = $this->getName();
		return $this->getPhpFileHeader()
		."/**\n"
		." * Classe {$msName}.\n"
		." *\n"
		." * <p>{$this->csDescription}</p>\n"
		." * @package {$this->csPackageAcronym}\n"
		." * @subpackage handlers\n"
		." */\n"
		."class {$msName} extends FWDDBQueryHandler {\n"
		."\n";
	}

	public function getPhpConstructor(){
		return "  public function __construct(\$poDB=null){\n"
		."    parent::__construct(\$poDB);\n"
		.$this->getFieldsCreation()
		."  }\n\n";
	}

	public function getPhpMakeQuery(){
		$msOutput = "  public function makeQuery(){\n";
		if(count($this->caFilters)>0){
			$msOutput.= "    \$maFilters = array();\n\n"
			.$this->csFilterMakeQuery
			."    if(count(\$maFilters)==0){\n"
			."      \$msWhere = '';\n"
			."    }else{\n"
			."      \$msWhere = ' WHERE '.implode(' AND ',\$maFilters);\n"
			."    }\n"
			."    \$this->csSQL = \"{$this->getQuery()}\n"
			."                    \$msWhere\";\n";
		}else{
			$msOutput.= "    \$this->csSQL = \"{$this->getQuery()}\";\n";
		}
		$msOutput.= "  }\n\n";
		return $msOutput;
	}

	public function getPhpFooter(){
		return "}\n\n?>";
	}

	public function getPhp(){
		return $this->getPhpHeader()
		.$this->csFilterProperties
		.$this->getPhpConstructor()
		.$this->csFilterMethods
		.$this->getPhpMakeQuery()
		.$this->getPhpFooter();
	}

}

?>