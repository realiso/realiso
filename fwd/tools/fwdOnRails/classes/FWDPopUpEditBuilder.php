<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDPopUpEditBuilder.
 *
 * <p></p>
 * @package Rails
 * @subpackage classes
 */
class FWDPopUpEditBuilder implements IXMLBuilder {

	protected $ciInitialTop = 30;
	protected $ciMaxHeight = 500;
	protected $ciLabelWidth = 100;
	protected $ciElementWidth = 240;
	protected $caVariables;
	protected $caRequired;
	protected $csTableName;
	protected $csClassName;
	protected $csContextName;
	protected $csContextLabel;
	protected $csAliasId;
	protected $csNameFieldAlias;
	protected $caReferences;
	protected $caColumnsInfo;
	protected $caAssociations;
	protected $caLookUpTables;
	protected $csXml = '';

	public function __construct(){
		$this->setup();
		$this->caReferences = array();
	}

	public function setup(){
		$this->caRequired = array();
		$this->caColumnsInfo = array();
		$this->caVariables = array();
		$this->caLookUpTables = array();
		$this->caAssociations = array();
	}

	public function setTableName($psTableName){
		$this->csTableName = $psTableName;
		$this->csClassName = FWDWizardLib::getClassName($psTableName);
		$moQuery = new QueryForeignKeys();
		$moQuery->setSourceTable($psTableName);
		$moQuery->makeQuery();
		$moQuery->executeQuery();
		while($moQuery->fetch()){
			$msField = $moQuery->getFieldValue('field');
			$msRefTable = $moQuery->getFieldValue('referenced_table');
			$msRefField = $moQuery->getFieldValue('referenced_field');
			$this->caReferences[$msField] = array('table'=>$msRefTable,'field'=>$msRefField);
		}
	}

	public function setContextName($psContextName){
		$this->csContextName = $psContextName;
	}

	public function setContextLabel($psContextLabel){
		$this->csContextLabel = $psContextLabel;
	}

	public function setAliasId($psAliasId){
		$this->csAliasId = $psAliasId;
	}

	public function setNameFieldAlias($psNameFieldAlias){
		$this->csNameFieldAlias = $psNameFieldAlias;
	}

	private function stripOffPrefix($psAlias){
		$miPrefixLength = strlen($this->csContextName)+1;
		if(substr($psAlias,0,$miPrefixLength)=="{$this->csContextName}_"){
			return substr($psAlias,$miPrefixLength);
		}else{
			return $psAlias;
		}
	}

	protected function addVariable($psVariableName){
		if(!in_array($psVariableName,$this->caVariables)){
			$this->caVariables[] = $psVariableName;
		}
	}

	protected function addLookUpTable($psLookUpTable){
		if(!in_array($psLookUpTable,$this->caLookUpTables)){
			$this->caLookUpTables[] = $psLookUpTable;
		}
	}

	protected function getReferencedTable($psReferencingField){
		return $this->caReferences[$psReferencingField]['table'];
	}

	protected function getReferencedField($psReferencingField){
		return $this->caReferences[$psReferencingField]['field'];
	}

	public function addField($psName,$psAlias,$psElement,$pbRequired=false){
		$this->caColumnsInfo[$psName] = array(
      'name' => $psName,
      'alias' => $psAlias,
      'element' => $psElement,
      'required' => $pbRequired,
		);
	}

	public function addAssociation($psRelationTable, $psRelatedTable, $psPluralLabel, $pbRequired){
		$this->caAssociations[$psRelationTable] = array(
      'relation_table' => $psRelationTable,
      'related_table'  => $psRelatedTable,
      'plural_label'   => $psPluralLabel,
      'required'       => $pbRequired
		);
	}

	private function getFieldXml($psName,&$piLeft,&$piTop){
		$maHeights = array('memo'=>80,'text'=>20,'calendar'=>20,'select'=>20,'lookup'=>20,'association'=>50);

		if(isset($this->caColumnsInfo[$psName])){
			$maColumnInfo = $this->caColumnsInfo[$psName];
		}else{
			$maColumnInfo = $this->caAssociations[$psName];
			$maColumnInfo['element'] = 'association';
		}

		$miHeight = $maHeights[$maColumnInfo['element']];
		if($piTop + $miHeight + 10 > $this->ciMaxHeight){
			$miLeft = $piLeft + $this->ciLabelWidth + 15 + $this->ciElementWidth + 15;
			$miTop = $this->ciInitialTop;
		}else{
			$miLeft = $piLeft;
			$miTop = $piTop;
		}

		$miElementLeft = $miLeft + $this->ciLabelWidth + 15;

		if($maColumnInfo['element']!='association'){
			$msAlias = $maColumnInfo['alias'];
			$msShortAlias = $this->stripOffPrefix($msAlias);
			if($maColumnInfo['required']){
				$this->caRequired[] = $msAlias;
				$msXml = "<static name='label_{$msAlias}' horizontal='right' mustfill='true'>
                    <box top='{$miTop}' left='{$miLeft}' height='20' width='{$this->ciLabelWidth}'/>
                    <string id='lb_{$msAlias}_bl_cl'><b>{$msShortAlias}:</b></string>
                  </static>";
			}else{
				$msXml = "<static name='label_{$msAlias}' horizontal='right'>
                    <box top='{$miTop}' left='{$miLeft}' height='20' width='{$this->ciLabelWidth}'/>
                    <string id='lb_{$msAlias}_cl'>{$msShortAlias}:</string>
                  </static>";
			}
		}

		switch($maColumnInfo['element']){
			case 'memo':{
				$msXml.= "<memo name='$msAlias'>
                    <box top='{$miTop}' left='{$miElementLeft}' height='$miHeight' width='{$this->ciElementWidth}'/>
                  </memo>";
				break;
			}
			case 'text':{
				$msXml.= "<text name='$msAlias'>
                    <box top='{$miTop}' left='{$miElementLeft}' height='$miHeight' width='{$this->ciElementWidth}'/>
                  </text>";
				break;
			}
			case 'calendar':{
				$msXml.= "<viewgroup>
                    <box top='{$miTop}' left='{$miElementLeft}' height='$miHeight' width='100'/>
                    <calendar name='$msAlias' valign='center' singleclick='true'/>
                  </viewgroup>";
				break;
			}
			case 'select':{
				$msXml.= "<dbselect name='$msAlias'>
                    <box top='{$miTop}' left='{$miElementLeft}' height='$miHeight' width='{$this->ciElementWidth}'/>
                    <item key=''/>
                  </dbselect>";
				break;
			}
			case 'lookup':{
				$miButtonLeft = $miElementLeft + $this->ciElementWidth - 70;
				$msRefTable = $this->getReferencedTable($psName);
				$msPrefix = FWDWizardLib::getPrefix($msRefTable);
				$msScreensRef = FWDWizardLib::getInstance()->getScreensRef($msPrefix);
				$msLCEntityName = FWDWizardLib::getLowerCaseEntityName($msRefTable);
				$msPopUpName = "popup_{$msLCEntityName}_search";
				$msRefEntityVar = "var_{$msLCEntityName}_field";

				$msPopUpPath = "{$msScreensRef}{$msPopUpName}";

				$this->addVariable($msAlias);
				$this->addVariable($msRefEntityVar);
				$msXml.= "<static name='static_{$msAlias}' class='StDisplayInfo'>"
				."  <box top='{$miTop}' left='$miElementLeft' height='$miHeight' width='".($this->ciElementWidth-85)."'/>"
				."</static>"
				."<viewbutton class='FWDViewButtonISMS'>"
				."  <box top='{$miTop}' left='$miButtonLeft' height='$miHeight' width='70'/>"
				."  <clientevent event='onClick'>\n"
				."    gebi('{$msRefEntityVar}').value = '{$msAlias}';\n"
				."    isms_open_popup('{$msPopUpName}','{$msPopUpPath}.php','','true');\n"
				."  </clientevent>"
				."  <static horizontal='center'>"
				."    <box top='0' left='0' height='20' width='70'/>"
				."    <string id='vb_find'>Buscar</string>"
				."  </static>"
				."</viewbutton>";
				break;
			}
			case 'association':{
				$msRelatedTable = $maColumnInfo['related_table'];
				$msPluralLabel = $maColumnInfo['plural_label'];
				$msRelatedLCEntity = FWDWizardLib::getLowerCaseEntityName($msRelatedTable);
				$msRelatedPluralLCEntity = FWDWizardLib::pluralize($msRelatedLCEntity);
				$msRelatedScreenRef = FWDWizardLib::getInstance()->getScreensRef(FWDWizardLib::getPrefix($msRelatedTable));

				$this->addVariable("var_{$msRelatedPluralLCEntity}");

				$miButtonsWidth = 70;
				$miButtonLeft = $miElementLeft + $this->ciElementWidth - $miButtonsWidth;
				$miSelectWidth = $miButtonLeft - $miElementLeft - 15;

				if($maColumnInfo['required']){
					$this->caRequired[] = "var_{$msRelatedPluralLCEntity}";
					$msXml = "<static name='label_var_{$msRelatedPluralLCEntity}' horizontal='right' mustfill='true'>
                      <box top='{$miTop}' left='{$miLeft}' height='20' width='{$this->ciLabelWidth}'/>
                      <string id='lb_{$msRelatedPluralLCEntity}_bl_cl'><b>{$msPluralLabel}:</b></string>
                    </static>";
				}else{
					$msXml = "<static horizontal='right'>
                      <box top='{$miTop}' left='{$miLeft}' height='20' width='{$this->ciLabelWidth}'/>
                      <string id='lb_{$msRelatedPluralLCEntity}_cl'>{$msPluralLabel}:</string>
                    </static>";
				}

				$msXml.= "<dbselect name='current_{$msRelatedPluralLCEntity}' size='5'>\n"
				."  <box top='{$miTop}' left='{$miElementLeft}' height='{$miHeight}' width='{$miSelectWidth}'/>\n"
				."</dbselect>\n"
				."<viewbutton name='add_{$msRelatedPluralLCEntity}' class='FWDViewButtonISMS'>\n"
				."  <box top='{$miTop}' left='{$miButtonLeft}' height='20' width='{$miButtonsWidth}'/>\n"
				."  <clientevent event='onClick'>\n"
				."    isms_open_popup('popup_{$msRelatedLCEntity}_associate','{$msRelatedScreenRef}popup_{$msRelatedLCEntity}_associate.php?ids='+gobi('current_{$msRelatedPluralLCEntity}').getKeys().join(':'),'','true');\n"
				."  </clientevent>\n"
				."  <static horizontal='center' vertical='middle'>\n"
				."    <box top='0' left='0' height='20' width='{$miButtonsWidth}'/>\n"
				."    <string id='vb_add'>Adicionar</string>\n"
				."  </static>\n"
				."</viewbutton>\n"
				."<viewbutton class='FWDViewButtonISMS'>\n"
				."  <box top='".($miTop+30)."' left='{$miButtonLeft}' height='20' width='{$miButtonsWidth}'/>\n"
				."  <clientevent event='onClick'>gobi('current_{$msRelatedPluralLCEntity}').removeSelecteds();</clientevent>\n"
				."  <static horizontal='center' vertical='middle'>\n"
				."    <box top='0' left='0' height='20' width='{$miButtonsWidth}'/>\n"
				."    <string id='vb_remove'>Remover</string>\n"
				."  </static>\n"
				."</viewbutton>";
				break;
			}
			default:{
				trigger_error("Unknown element type '{$maColumnInfo['element']}'.",E_USER_ERROR);
			}
		}

		$piLeft = $miLeft;
		$piTop+= $miHeight + 10;
		return $msXml;
	}

	protected function getHeader($piWidth,$piHeight){
		return "<?xml version='1.0' encoding='ISO-8859-1'?>
            <dialog name='dialog' focus='{$this->csNameFieldAlias}'>
              <box top='0' left='0' height='{$piHeight}' width='{$piWidth}'/>
              <getparameter name='{$this->csAliasId}' parameter='{$this->csContextName}'/>
              <static name='popup_title' class='FWDStaticPopupTitle' target='title'>
                <box top='0' left='0' height='20' width='{$piWidth}'/>
                <string id='tt_{$this->csContextName}_edit'>Edi��o de {$this->csContextLabel}</string>
              </static>";
	}

	public function buildXml(){
		$miTop = $this->ciInitialTop;
		$miMaxTop = 0;
		$miLeft = 10;
		$msElementsXml = '';
		foreach($this->caColumnsInfo as $maColumnInfo){
			$msElementsXml.= $this->getFieldXml($maColumnInfo['name'],&$miLeft,&$miTop);
			$miMaxTop = max($miMaxTop,$miTop);
		}
		foreach($this->caAssociations as $maAssociation){
			$msElementsXml.= $this->getFieldXml($maAssociation['relation_table'],&$miLeft,&$miTop);
			$miMaxTop = max($miMaxTop,$miTop);
		}
		$msVariablesXml = '';
		foreach($this->caVariables as $msVariable){
			$msVariablesXml.= "  <variable name='{$msVariable}'/>\n";
		}
		$miWidth = $miLeft + $this->ciLabelWidth + 15 + $this->ciElementWidth + 10;
		$this->csXml = $this->getHeader($miWidth,$miMaxTop+50)
		.$msVariablesXml
		.$msElementsXml
		.$this->getFooter($miWidth,$miMaxTop);
	}

	protected function getFooter($piWidth,$piTop){
		$msSaveEvent = '';
		foreach($this->caAssociations as $maAssociation){
			$msRelatedLCEntity = FWDWizardLib::getLowerCaseEntityName($maAssociation['related_table']);
			$msRelatedPluralLCEntity = FWDWizardLib::pluralize($msRelatedLCEntity);
			$msSaveEvent.= "<clientevent event='onClick'>gebi('var_{$msRelatedPluralLCEntity}').value = gobi('current_{$msRelatedPluralLCEntity}').getKeys().join(':');</clientevent>";
		}
		if(count($this->caRequired)){
			$msSaveEvent.= "<requiredcheck event='onClick' elements='".implode(':',$this->caRequired)."'>
                        <serverevent event='eventok' function='save_event'/>
                      </requiredcheck>";
		}else{
			$msSaveEvent.= "<serverevent event='onClick' function='save_event'/>";
		}
		return "<horizontalrule>
              <box top='{$piTop}' left='10' height='1' width='".($piWidth-20)."'/>
            </horizontalrule>
            <viewbutton class='FWDViewButtonISMS'>
              <box top='".($piTop+20)."' left='".($piWidth-165)."' height='20' width='70'/>
              {$msSaveEvent}
              <static horizontal='center' vertical='middle'>
                <box top='0' left='0' height='20' width='70'/>
                <string id='vb_save'>Salvar</string>
              </static>
            </viewbutton>
            <viewbutton class='FWDViewButtonISMS'>
              <box top='".($piTop+20)."' left='".($piWidth-80)."' height='20' width='70'/>
              <clientevent event='onClick'>self.close();</clientevent>
              <static horizontal='center' vertical='middle'>
                <box top='0' left='0' height='20' width='70'/>
                <string id='vb_close'>Fechar</string>
              </static>
            </viewbutton>
          </dialog>";
	}

	public function setXml($psXml){
		$this->csXml = $psXml;
	}

	public function getXml(){
		return $this->csXml;
	}

	public function getPhp(){
		$msContextName = FWDWizardLib::getTitleCaseEntityName($this->csTableName);

		$msIncludes = '';
		$msPopulateOptions = '';
		$msPopulateFields = '';
		foreach($this->caColumnsInfo as $msColumnName=>$maColumnInfo){
			if($maColumnInfo['element']=='select'){
				$msRefTable = $this->getReferencedTable($maColumnInfo['name']);
				$msPath = FWDWizardLib::getInstance()->getSelectHandlersRef(FWDWizardLib::getPrefix($msRefTable));
				$msEntity = FWDWizardLib::getTitleCaseEntityName($msRefTable);
				$msIncludes.= "include_once \$handlers_ref.'{$msPath}QuerySelect{$msEntity}.php';\n";
				$msPopulateOptions.= "    \$moHandler = new QuerySelect{$msEntity}(FWDWebLib::GetConnection());\n"
				."    \$moSelect = FWDWebLib::getObject('{$maColumnInfo['alias']}');\n"
				."    \$moSelect->setQueryHandler(\$moHandler);\n"
				."    \$moSelect->populate();\n\n";
			}elseif($maColumnInfo['element']=='lookup'){
				$msRefTable = $this->getReferencedTable($maColumnInfo['name']);
				$this->addLookUpTable($msRefTable);
				$msLCEntityName = FWDWizardLib::getLowerCaseEntityName($msRefTable);
				$msPopulateFields.= "      FWDWebLib::getObject('static_{$maColumnInfo['alias']}')->setValue(get_{$msLCEntityName}_name(\$mo{$msContextName}->getFieldValue('{$maColumnInfo['alias']}')));\n";
			}
		}

		$msGetNameFuncs = '';
		$msLookUpEvents = '';
		$msAjaxEvents = '';
		$msJSFuncs = '';
		foreach($this->caLookUpTables as $msRefTable){

			$msTCEntityName = FWDWizardLib::getTitleCaseEntityName($msRefTable);
			$msLCEntityName = FWDWizardLib::getLowerCaseEntityName($msRefTable);
			$msClassName = FWDWizardLib::getClassName($msRefTable);

			$msGetNameFuncs.= "function get_{$msLCEntityName}_name(\$piId){\n"
			."  \$mo{$msTCEntityName} = new {$msClassName}();\n"
			."  \$mo{$msTCEntityName}->fetchById(\$piId);\n"
			."  return \$mo{$msTCEntityName}->getName();\n"
			."}\n\n";

			$msLookUpEvents.= "class Get{$msTCEntityName}NameEvent extends FWDRunnable {\n"
			."  public function run(){\n"
			."    \$ms{$msTCEntityName}Field = FWDWebLib::getObject('var_{$msLCEntityName}_field')->getValue();\n"
			."    \$mi{$msTCEntityName}Id = FWDWebLib::getObject(\$ms{$msTCEntityName}Field)->getValue();\n"
			."    \$ms{$msTCEntityName}Name = get_{$msLCEntityName}_name(\$mi{$msTCEntityName}Id);\n"
			."    echo \"gobi('static_\$ms{$msTCEntityName}Field').setValue('\$ms{$msTCEntityName}Name');\";\n"
			."  }\n"
			."}\n\n";

			$msAjaxEvents.= "    \$moStartEvent->addAjaxEvent(new Get{$msTCEntityName}NameEvent('get_{$msLCEntityName}_name'));\n";

			$msJSFuncs.= "        function set_{$msLCEntityName}(piId){\n"
			."          gebi(gebi('var_{$msLCEntityName}_field').value).value = piId;\n"
			."          trigger_event('get_{$msLCEntityName}_name',3);\n"
			."        }\n"
			."        \n";

		}

		foreach($this->caAssociations as $maAssociation){
			$msRelationTable = $maAssociation['relation_table'];
			$msRelationTCEntity = FWDWizardLib::getTitleCaseEntityName($msRelationTable);
			$msRelationSelectHandlersRef = FWDWizardLib::getInstance()->getSelectHandlersRef(FWDWizardLib::getPrefix($msRelationTable));
			$msIncludes.= "include_once \$handlers_ref . '{$msRelationSelectHandlersRef}QuerySelect{$msRelationTCEntity}.php';\n";
		}

		$msLCEntityName = FWDWizardLib::getLowerCaseEntityName($this->csTableName);

		$msPHPPopUpEdit = "<?php

include_once 'include.php';
		{$msIncludes}
		{$msGetNameFuncs}
		{$msLookUpEvents}
class SaveEvent extends FWDRunnable {
  public function run() {
    \$mi{$msContextName}Id = FWDWebLib::getObject('{$this->csAliasId}')->getValue();
    \$mo{$msContextName} = new {$this->csClassName}();
    \$mo{$msContextName}->fillFieldsFromForm();
    \$mo{$msContextName}->insertOrUpdate(\$mi{$msContextName}Id); //ja executa os testes de permiss�o de edi��o e inser��o
    \n";

		foreach($this->caAssociations as $maAssociation){
			$msRelationTable = $maAssociation['relation_table'];
			$msRelatedTable = $maAssociation['related_table'];
			$msRelationClass = FWDWizardLib::getClassName($msRelationTable);
			$msRelatedLCEntity = FWDWizardLib::getLowerCaseEntityName($msRelatedTable);
			$msRelatedPluralLCEntity = FWDWizardLib::pluralize($msRelatedLCEntity);
			$msRelatedTCEntity = FWDWizardLib::getTitleCaseEntityName($msRelatedTable);
			$msRelatedPluralTCEntity = FWDWizardLib::pluralize($msRelatedTCEntity);
			$msPHPPopUpEdit.= "    \$ma{$msRelatedPluralTCEntity} = array_filter(explode(':',FWDWebLib::getObject('var_{$msRelatedPluralLCEntity}')->getValue()));\n"
			."    \$mo{$msContextName}->updateRelation(new {$msRelationClass}(),'{$msRelatedLCEntity}_id',\$ma{$msRelatedPluralTCEntity});\n"
			."    \n";
		}

		$msPHPPopUpEdit.= "    echo 'soWindow = self.getOpener();'\n"
		."        .'if(soWindow.refresh_grid) soWindow.refresh_grid();'\n"
		."        .'self.close();';\n"
		."  }\n"
		."}\n\n";

		foreach($this->caAssociations as $maAssociation){
			$msRelationTable = $maAssociation['relation_table'];
			$msRelatedTable = $maAssociation['related_table'];
			$msRelationTCEntity = FWDWizardLib::getTitleCaseEntityName($msRelationTable);
			$msRelatedLCEntity = FWDWizardLib::getLowerCaseEntityName($msRelatedTable);
			$msRelatedPluralLCEntity = FWDWizardLib::pluralize($msRelatedLCEntity);
			$msRelatedTCEntity = FWDWizardLib::getTitleCaseEntityName($msRelatedTable);
			$msRelatedPluralTCEntity = FWDWizardLib::pluralize($msRelatedTCEntity);
			$msPHPPopUpEdit.= "class RefreshCurrent{$msRelatedPluralTCEntity}Event extends FWDRunnable {\n"
			."  public function run(){\n"
			."    \$moHandler = new QuerySelect{$msRelationTCEntity}();\n"
			."    \$moHandler->set{$msRelatedTCEntity}Ids(FWDWebLib::getObject('var_{$msRelatedPluralLCEntity}')->getValue());\n"
			."    \$moSelect = FWDWebLib::getObject('current_{$msRelatedPluralLCEntity}');\n"
			."    \$moSelect->setQueryHandler(\$moHandler);\n"
			."    \$moSelect->populate();\n"
			."    \$moSelect->execEventPopulate();\n"
			."  }\n"
			."}\n\n";
			$msAjaxEvents.= "    \$moStartEvent->addAjaxEvent(new RefreshCurrent{$msRelatedPluralTCEntity}Event('refresh_{$msRelatedPluralLCEntity}'));\n";
			$msPopulateFields.= "      \n"
			."      \$moHandler = new QuerySelect{$msRelationTCEntity}();\n"
			."      \$moHandler->set{$msContextName}(\$mi{$msContextName}Id);\n"
			."      \$moSelect = FWDWebLib::getObject('current_{$msRelatedPluralLCEntity}');\n"
			."      \$moSelect->setQueryHandler(\$moHandler);\n"
			."      \$moSelect->populate();\n";
			$msJSFuncs.= "        function set_{$msRelatedPluralLCEntity}(psIds){\n"
			."          gebi('var_{$msRelatedPluralLCEntity}').value = psIds;\n"
			."          trigger_event('refresh_{$msRelatedPluralLCEntity}',3);\n"
			."        }\n"
			."        \n";
		}

		if($msJSFuncs){
			$msJSFuncs = "\n"
			."    ?>\n"
			."      <script language=\"javascript\">\n"
			.$msJSFuncs
			."      </script>\n"
			."    <?\n";
		}

		$msPHPPopUpEdit.= "class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    \$moStartEvent = FWDStartEvent::getInstance();
    \$moStartEvent->setScreenEvent(new ScreenEvent(''));
    \$moStartEvent->addAjaxEvent(new SaveEvent('save_event'));
		{$msAjaxEvents}
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    \$mi{$msContextName}Id = FWDWebLib::getObject('{$this->csAliasId}')->getValue();
    \$moCtxUserTest = new {$this->csClassName}();
    
		{$msPopulateOptions}
    if(\$mi{$msContextName}Id){
      \$moCtxUserTest->testPermissionToEdit(\$mi{$msContextName}Id);
      \$mo{$msContextName} = new {$this->csClassName}();
      \$mo{$msContextName}->fetchById(\$mi{$msContextName}Id);
      \$mo{$msContextName}->fillFormFromFields();
		{$msPopulateFields}
    }else{
      \$moCtxUserTest->testPermissionToInsert();
    }
    //instala a seguran�a de ACL na p�gina
    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));{$msJSFuncs}
  }
}

//testa se a licen�a utilizada permite acessar o m�dulo de incidente
ISMSLib::testUserPermissionToIncidentMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_{$msLCEntityName}_edit.xml');

?>";
		return $msPHPPopUpEdit;
	}

}

?>