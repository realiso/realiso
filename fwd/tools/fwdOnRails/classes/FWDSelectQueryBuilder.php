<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDSelectQueryBuilder.
 *
 * <p></p>
 * @package Rails
 * @subpackage classes
 */
class FWDSelectQueryBuilder extends FWDQueryBuilder {

	protected $csTableName;
	protected $csClass;
	protected $csLabel;
	protected $csLowerCaseEntityName;
	protected $csTitleCaseEntityName;
	protected $caColumnsInfo;
	protected $caMaxLenghts;

	public function __construct($psTableName,$psIdField,$psValueField){
		parent::__construct($psTableName,'','');

		$this->addField($psIdField);
		$this->setFieldProperty($psIdField,'name',$psIdField);
		$this->setFieldProperty($psIdField,'alias','select_id');
		$this->setFieldProperty($psIdField,'type','DB_NUMBER');

		$this->addField($psValueField);
		$this->setFieldProperty($psValueField,'name',$psValueField);
		$this->setFieldProperty($psValueField,'alias','select_value');
		$this->setFieldProperty($psValueField,'type','DB_STRING');

		$this->addOrderBy($psValueField);
	}

}

?>