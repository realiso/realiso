<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDReportBuilder.
 *
 * <p></p>
 * @package Rails
 * @subpackage classes
 */
class FWDReportBuilder extends FWDQueryBuilder implements IReportBuilder, IXMLBuilder {

	protected $csTitle;
	protected $csSelect;
	protected $csWhere;
	protected $csOrderBy;

	protected $cbGenerateGetters = true;

	protected $caLevels = array();

	protected $csXml = '';

	public function getTitle(){
		return $this->csTitle;
	}

	public function setTitle($psTitle){
		$this->csTitle = $psTitle;
	}

	public function getSelect(){
		return $this->csSelect;
	}

	public function setSelect($psSelect){
		$this->csSelect = $psSelect;
		parent::setQuery($psSelect);

		$maAliases = array();
		foreach($this->getFields() as $msFieldName){
			$maAliases[] = $this->getFieldProperty($msFieldName,'alias');
		}
		sort($maAliases);

		$maLevels = array();
		foreach($maAliases as $msAlias){
			$maAlias = explode('_',$msAlias);
			if(!isset($maLevels[$maAlias[0]])){
				$maLevels[$maAlias[0]] = array();
			}
			$maLevels[$maAlias[0]][] = $maAlias;
		}
		// Inicializa a lista de n�veis a partir dos prefixos dos aliases
		$this->clearLevels();
		foreach($maLevels as $msPrefix => $maAliases){
			$miLast = count($maAliases) - 1;
			if($miLast>0){
				$i = 0;
				while($maAliases[0][$i]==$maAliases[$miLast][$i]) $i++;
			}else{
				$i = 1;
			}
			$maLevelName = $maAliases[0];
			array_splice($maLevelName,$i);
			$msLevelName = implode('_',$maLevelName);
			$this->addLevel($msLevelName,'false');
		}

	}

	public function getWhere(){
		return $this->csWhere;
	}

	public function setWhere($psWhere){
		$this->csWhere = $psWhere;
	}

	public function getOrderBy(){
		return $this->csOrderBy;
	}

	public function setOrderBy($psOrderBy){
		$this->csOrderBy = $psOrderBy;
	}

	public function getReportClass(){
		return "{$this->csPackageAcronym}Report".str_replace(' ','',ucwords($this->csName));
	}

	public function getReportFilterClass(){
		return "{$this->csPackageAcronym}Report".str_replace(' ','',ucwords($this->csName))."Filter";
	}

	public function getPopupName(){
		return "popup_report_".str_replace(' ','_',$this->csName);
	}

	public function setXml($psXml){
		$this->csXml = $psXml;
	}

	public function getXml(){
		return $this->csXml;
	}

	public function buildXml(){
		$msReportClass = $this->getReportClass();
		$this->csXml = "<?xml version='1.0' encoding='ISO-8859-1'?>"
		."<!DOCTYPE isms [ <!ENTITY gfx '../../../gfx/'> ]>"
		."<root>"
		."  <{$msReportClass} name='report'>"
		."    <reportline name='header_filter' height='18' target='header' bgcolor='E0F4FF'>"
		."      <reportstatic name='header_filter_text' width='700' bold='true'/>"
		."    </reportline>"
		."    <reportline name='header_filter_item' height='18' target='header' bgcolor='E0F4FF'>"
		."      <reportstatic width='20'/>"
		."      <reportstatic name='header_filter_item_text' width='680' bold='false'/>"
		."    </reportline>";

		$maFields = array();
		foreach($this->getFields() as $msFieldName){
			$msLevelName = $this->getFieldProperty($msFieldName,'level');
			if(!isset($maFields[$msLevelName])){
				$maFields[$msLevelName] = array();
			}
			if($this->fieldIsId($msFieldName)){
				$maFields[$msLevelName][$msFieldName] = 20;
			}else{
				$maFields[$msLevelName][$msFieldName] = 0;
			}
		}

		$miWidth = 700;
		$miOffset = 0;
		foreach($this->caLevels as $maLevel){
			$this->csXml.= "<reportlevel name='{$maLevel['name']}_level' offset='{$miOffset}'>";
			$msHeaderXml = "<reportline name='{$maLevel['name']}_header' height='20' target='header'>";
			$msBodyXml = "<reportline name='{$maLevel['name']}_body' height='20' target='body'>";
			$miRemainingWidth = $miWidth;
			$miCells = 0;
			foreach($maFields[$maLevel['name']] as $miTakenWidth){
				if($miTakenWidth){
					$miRemainingWidth-= $miTakenWidth;
				}else{
					$miCells++;
				}
			}
			$miDefaultCellWidth = floor($miRemainingWidth/$miCells);
			$mbIsFirstCell = true;
			$miAdditionalMargin = 0;
			foreach($maFields[$maLevel['name']] as $msFieldName=>$miCellWidth){
				$msAlias = $this->getFieldProperty($msFieldName,'alias');
				if($miCellWidth){
					$msBodyXml.= "<reporticon name='{$msAlias}' src='&gfx;icon-wrong.png' width='{$miCellWidth}' marginleft='2' marginright='2'/>";
					if($mbIsFirstCell){
						$miAdditionalMargin = 20;
					}else{
						$msHeaderXml.= "<reportstatic name='{$msAlias}_header' width='{$miCellWidth}' marginleft='2' bold='true'>"
						."  <string id='rs_{$msAlias}'>{$msAlias}</string>"
						."</reportstatic>";
					}
				}else{
					if($miCells>1){
						$miCellWidth = $miDefaultCellWidth;
						$miRemainingWidth-= $miDefaultCellWidth;
						$miCells--;
					}else{
						$miCellWidth = $miRemainingWidth;
					}
					$msHeaderXml.= "<reportstatic name='{$msAlias}_header' width='".($miCellWidth+$miAdditionalMargin)."' marginleft='".(2+$miAdditionalMargin)."' bold='true'>"
					."  <string id='rs_{$msAlias}'>{$msAlias}</string>"
					."</reportstatic>";
					$miAdditionalMargin = 0;
					$msBodyXml.= "<reportstatic name='{$msAlias}' width='{$miCellWidth}' marginleft='2'/>";
				}
				$mbIsFirstCell = false;
			}

			$msHeaderXml.= "</reportline>";
			$msBodyXml.= "</reportline>";
			$this->csXml.= $msHeaderXml.$msBodyXml;
			$miOffset = 20;
			$miWidth-= $miOffset;
		}

		$this->csXml.= str_repeat('</reportlevel>',count($this->caLevels));

		$this->csXml.= "  </{$msReportClass}>"
		."</root>";
	}

	public function addLevel($psName,$psIsSupressible){
		$this->caLevels[$psName] = array(
      'name' => $psName,
      'supressible' => $psIsSupressible
		);
	}

	public function clearLevels(){
		$this->caLevels = array();
	}

	public function getLevels(){
		return array_values($this->caLevels);
	}

	public function isFiltered(){
		return (count($this->caFilters)>0);
	}

	public function getPhpMakeQuery(){
		if($this->isFiltered()){
			$msWhere = '{$msWhere}';
		}elseif($this->csWhere){
			$msWhere = "WHERE {$this->csWhere}";
		}else{
			$msWhere = '';
		}

		$mmFirstLineEnd = strpos($this->csQuery,"\n");
		if($mmFirstLineEnd===false){
			$msQuery = $this->csQuery;
			if($msWhere){
				$msQuery.= " {$msWhere}";
			}
			if($this->csOrderBy){
				$msQuery.= " ORDER BY {$this->csOrderBy}";
			}
		}else{
			$msQuery = trim(substr($this->csQuery,0,$mmFirstLineEnd))."\n"
			.FWDWizardLib::stripIdentation(substr($this->csQuery,$mmFirstLineEnd));
			if($msWhere){
				$msQuery.= "\n{$msWhere}";
			}
			if($this->csOrderBy){
				$msQuery.= "\nORDER BY {$this->csOrderBy}";
			}
			$msQuery = FWDWizardLib::addIdentation($msQuery,22);
		}

		$msOutput = " /**\n"
		."  * Executa a query do relat�rio.\n"
		."  *\n"
		."  * <p>M�todo para executar a query do relat�rio.</p>\n"
		."  * @access public\n"
		."  */\n"
		."  public function makeQuery(){\n";
		if($this->isFiltered()){
			foreach($this->caFilters as $maFilter){
				$msVariableName = $maFilter['property'];
				$msVariableName[0] = 'm';
				$msGetterMethod = 'get'.substr($msVariableName,2);
				$msOutput.= "    \${$msVariableName} = \$this->coFilter->{$msGetterMethod}();\n";
			}

			$msOutput.= "    \n"
			."    \$maFilters = array();\n"
			."    \n";
			if($this->csWhere){
				$msOutput.= "    \$maFilters[] = \"{$this->csWhere}\";\n"
				."    \n";
			}
			$msOutput.= str_replace('$this->c','$m',$this->csFilterMakeQuery)
			."    if(count(\$maFilters)==0){\n"
			."      \$msWhere = '';\n"
			."    }else{\n"
			."      \$msWhere = ' WHERE '.implode(' AND ',\$maFilters);\n"
			."    }\n"
			."    \$this->csQuery = \"{$msQuery}\";\n";
		}else{
			$msOutput.= "    \$this->csQuery = \"{$msQuery}\";\n";
		}
		$msOutput.= "    return parent::executeQuery();\n"
		."  }\n\n";
		return $msOutput;
	}

	public function getReportClassPhp(){
		$msName = $this->getReportClass();

		$msSuppresModelsPHp = '';
		$maLevels = array_values($this->caLevels);

		for($i=0;$i<count($maLevels);$i++){
			if($maLevels[$i]['supressible']=='true'){
				$msSuppresModelsPHp.= "    \n"
				."    \$moSuppressModel = new FWDLiquidCellsSuppressModel();\n";
				for($j=$i+1;$j<count($maLevels);$j++){
					$maLiquidCells = array();
					foreach($this->getLevelFields($maLevels[$j]['name']) as $msFieldName){
						if($this->getFieldProperty($msFieldName,'liquid')){
							$maLiquidCells[] = $this->getFieldProperty($msFieldName,'alias');
						}
					}
					$msSuppresModelsPHp.= "    \$moSuppressModel->addCells('".implode("','",$maLiquidCells)."');\n";
				}
				$msSuppresModelsPHp.= "    \$this->addSuppressModel('{$maLevels[$i]['name']}_level',\$moSuppressModel);\n";
			}
		}

		if($this->isFiltered()){
			$msParentClass = 'ISMSFilteredReport';
		}else{
			$msParentClass = 'ISMSReport';
		}

		return $this->getPhpFileHeader()
		."/**\n"
		." * Classe {$msName}.\n"
		." *\n"
		." * <p>Classe que implementa o relat�rio de {$this->csDescription}.</p>\n"
		." * @package {$this->csPackageAcronym}\n"
		." * @subpackage report\n"
		." */\n"
		."class {$msName} extends {$msParentClass} {\n"
		."  \n"
		."  public function __construct(){\n"
		."    parent::__construct();\n"
		.$msSuppresModelsPHp
		."  }\n"
		."  \n"
		." /**\n"
		."  * Inicializa o relat�rio.\n"
		."  *\n"
		."  * <p>M�todo para inicializar o relat�rio.\n"
		."  * Necess�rio para funcionar com o XML compilado.</p>\n"
		."  * @access public\n"
		."  */\n"
		."  public function init(){\n"
		."    parent::init();\n"
		.$this->getFieldsCreation()
		."  }\n"
		."  \n"
		.$this->getPhpMakeQuery()
		."}\n"
		."\n"
		."?>";
	}

	public function getReportFilterPhp(){
		$msName = $this->getReportFilterClass();

		$msGetSummary = "  public function getSummary(){\n"
		."    \n"
		."    \$maFilters = array();\n"
		."    \n";

		foreach($this->caFilters as $maFilter){
			$msFieldType = $this->getFieldTypeByAlias($maFilter['field']);
			$msFilterName = $maFilter['name'];
			$msLCFilterName = strtolower($msFilterName);
			if($msFieldType=='DB_NUMBER'){
				if($maFilter['type']=='='){
					$msGetSummary.= "    if(\$this->ci{$msFilterName}){\n"
					."      \$maFilters[] = array(\n"
					."        'name' => FWDLanguage::getPHPStringValue('rs_{$msLCFilterName}_cl','{$maFilter['title']}:'),\n"
					."        'items' => array(\$this->ci{$msFilterName})\n"
					."      );\n"
					."    }\n"
					."    \n";
				}elseif($maFilter['type']=='IN' || $maFilter['type']=='NOT IN'){
					$msGetSummary.= "    \$maFilter = array(\n"
					."      'name' => FWDLanguage::getPHPStringValue('rs_{$msLCFilterName}_cl','{$maFilter['title']}:'),\n"
					."      'items' => array()\n"
					."    );\n"
					."    if(count(\$this->ca{$msFilterName})){\n"
					."      foreach(\$this->ca{$msFilterName} as \$miId){\n"
					."        /*\n"
					."        \$moExample = new TSTExample();\n"
					."        \$moExample->fetchById(\$miId);\n"
					."        \$maFilter['items'][] = \$moExample->getName();\n"
					."        /*/\n"
					."        \$maFilter['items'][] = \$miId;\n"
					."        //*/\n"
					."      }\n"
					."    }else{\n"
					."      \$maFilter['items'][] = FWDLanguage::getPHPStringValue('rs_all_{$msLCFilterName}','todos(as)');\n"
					."    }\n"
					."    \$maFilters[] = \$maFilter;\n"
					."    \n";
				}
			}elseif($msFieldType=='DB_STRING'){
				if($maFilter['type']=='LIKE'){
					$msGetSummary.= "    if(\$this->cs{$msFilterName}){\n"
					."      \$maFilters[] = array(\n"
					."        'name' => FWDLanguage::getPHPStringValue('rs_{$msLCFilterName}_cl','{$maFilter['title']}:'),\n"
					."        'items' => array(\$this->cs{$msFilterName})\n"
					."      );\n"
					."    }\n"
					."    \n";
				}
			}
		}
		$msGetSummary.= "    return \$maFilters;\n"
		."  }\n";

		return "<?php\n"
		."/**\n"
		." * {$this->csPackageAcronym} - {$this->csPackageName}\n"
		." *\n"
		." * <p>These coded instructions,  technics, statements, and computer programs\n"
		." * contain  unpublished  proprietary information of  Axur Communications,\n"
		." * Inc.,  and are  protected  by applied  copyright law.  They may not be\n"
		." * disclosed to third parties, copied or duplicated in any form, in whole\n"
		." * or in part, without  the prior written consent of Axur Communications,\n"
		." * Inc.</p>\n"
		." * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem\n"
		." * informacao proprietaria  nao publicada pela Axur Communications, Inc.,\n"
		." * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem\n"
		." * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,\n"
		." * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur\n"
		." * Communications, Inc.</p>\n"
		." * @copyright Copyright (c) 2006, Axur Information Security\n"
		." * @link http://www.axur.com.br Axur Information Security\n"
		." */\n"
		."\n"
		."/**\n"
		." * Classe {$msName}.\n"
		." *\n"
		." * <p>Classe que implementa o filtro do relat�rio de {$this->csDescription}.</p>\n"
		." * @package {$this->csPackageAcronym}\n"
		." * @subpackage report\n"
		." */\n"
		."class {$msName} extends FWDReportFilter {\n"
		."\n"
		.$this->csFilterProperties
		.$this->csFilterMethods
		.$msGetSummary
		."\n"
		."}\n"
		."\n"
		."?>";
	}

	protected function getLevelFields($psLevel){
		$maFields = array();
		foreach($this->getFields() as $msFieldName){
			$msLevelName = $this->getFieldProperty($msFieldName,'level');
			if($msLevelName==$psLevel){
				$maFields[] = $msFieldName;
			}
		}
		return $maFields;
	}

	protected function fieldIsId($psFieldName){
		$mmStrpos = strpos($psFieldName,'.');
		if($mmStrpos===false){
			$msFieldName = $psFieldName;
		}else{
			$msFieldName = substr($psFieldName,$mmStrpos+1);
		}
		return ($msFieldName[1]=='k' && ($msFieldName[0]=='p' || $msFieldName[0]=='f'));
	}

	public function getLevelIteratorPhp($psLevel){
		$msTCLevel = str_replace(' ','',ucwords(str_replace('_',' ',$psLevel)));
		$msIteratorField = '???';
		$msSets = '';
		foreach($this->getLevelFields($psLevel) as $msFieldName){
			$msFieldAlias = $this->getFieldProperty($msFieldName,'alias');
			$msFieldType = $this->getFieldProperty($msFieldName,'type');
			if($this->fieldIsId($msFieldName)){
				$msIteratorField = $msFieldAlias;
			}else{
				if($msFieldType=='DB_DATETIME'){
					$msSets.= "    FWDWebLib::getObject('{$msFieldAlias}')->setValue(FWDWebLib::getDate(\$poDataSet->getFieldByAlias('{$msFieldAlias}')->getValue()));\n";
				}else{
					$msSets.= "    FWDWebLib::getObject('{$msFieldAlias}')->setValue(\$poDataSet->getFieldByAlias('{$msFieldAlias}')->getValue());\n";
				}
			}
		}
		return "class {$msTCLevel}LevelIterator extends FWDReportLevelIterator {\n"
		."  \n"
		."  public function __construct(){\n"
		."    parent::__construct('{$msIteratorField}');\n"
		."  }\n"
		."  \n"
		."  public function fetch(FWDDBDataSet \$poDataSet){\n"
		.$msSets
		."  }\n"
		."  \n"
		."}\n";
	}

	public function getReportPopupPhp(){
		$msName = str_replace(' ','_',$this->csName);
		$msIteratorClasses = '';
		$msLevelIterators = '';
		foreach($this->getLevels() as $maLevel){
			$msIteratorClasses.= $this->getLevelIteratorPhp($maLevel['name']);
			$msTCLevel = str_replace(' ','',ucwords(str_replace('_',' ',$maLevel['name'])));
			if($maLevel['supressible']=='true'){
				$msLevelIterators.= "    if(\$moFilter->levelIsSuppressed('{$maLevel['name']}_level')){\n"
				."      FWDWebLib::getObject('report')->suppressLevel('{$maLevel['name']}_level');\n"
				."    }else{\n"
				."      FWDWebLib::getObject('{$maLevel['name']}_level')->setLevelIterator(new {$msTCLevel}LevelIterator());\n"
				."    }\n"
				."    \n";
			}else{
				$msLevelIterators.= "    FWDWebLib::getObject('{$maLevel['name']}_level')->setLevelIterator(new {$msTCLevel}LevelIterator());\n"
				."    \n";
			}
		}
		return "<?php\n"
		."\n"
		."set_time_limit(3600);\n"
		."include_once 'include.php';\n"
		."\n"
		.$msIteratorClasses
		."\n"
		."class ScreenBeforeEvent extends FWDRunnable {\n"
		."  public function run(){\n"
		."    \$moStartEvent = FWDStartEvent::getInstance();\n"
		."    \$moStartEvent->setScreenEvent(new ScreenEvent(''));\n"
		."  }\n"
		."}\n"
		."\n"
		."class ScreenEvent extends FWDRunnable {\n"
		."  public function run(){\n"
		."    \$moFilter = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter();\n"
		."    \n"
		.$msLevelIterators
		."    \$moComponent = new FWDReportGenerator();\n"
		."    \$moComponent->setReport(FWDWebLib::getObject('report'));\n"
		."    \$moComponent->setReportTemplate(new ISMSReportTemplate());\n"
		."    \n"
		."    \$moComponent->setReportFilter(\$moFilter);\n"
		."    \n"
		."    \$moComponent->setReportName(FWDLanguage::getPHPStringValue('mx_{$msName}','{$this->csTitle}'));\n"
		."    \$moComponent->generate();\n"
		."  }\n"
		."}\n"
		."\n"
		."FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));\n"
		."FWDWebLib::getInstance()->xml_load('{$this->getPopupName()}.xml');\n"
		."\n"
		."?>";
	}

	public function getReportPopupXml(){
		return $this->csXml;
	}

}

?>