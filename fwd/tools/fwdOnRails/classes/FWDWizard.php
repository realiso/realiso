<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDWizard.
 *
 * <p></p>
 * @package Rails
 * @subpackage classes
 */
abstract class FWDWizard {

	protected $caStates = array();

	protected $caTitles = array();

	protected $caStateObjects = array();

	protected $caFinalStates = array();

	protected $csInitialState;

	protected static $coInstance = null;

	protected $csCurrentState;

	protected $caTrace;

	protected function __construct(){
	}

	public static function getInstance(){
		if(self::$coInstance===null){
			$moSession = FWDWebLib::getInstance()->getSession(new FWDWizardSession('FWDWizard'));
			$moInstance = $moSession->getAttrWizardObject();
			if(!$moInstance){
				$msClass = get_class(new self());
				$moInstance = new $msClass;
				$moSession->setAttrWizardObject($moInstance);
			}
			self::$coInstance = $moInstance;
		}
		return self::$coInstance;
	}

	protected function addState($psName, $psTitle, /*FWDWizardState*/ $poState, $pbIsFinal=false, $pbIsInitial=false){
		if(in_array($psName,$this->caStates)){
			trigger_error("State '$psName' duplicated.",E_USER_ERROR);
		}
		$this->caStates[] = $psName;
		$this->caTitles[$psName] = $psTitle;
		$this->caStateObjects[$psName] = $poState;
		if($pbIsFinal){
			$this->caFinalStates[] = $psName;
		}
		if($pbIsInitial){
			if($this->csInitialState){
				trigger_error("Initial state is already defined.",E_USER_ERROR);
			}
			$this->csInitialState = $psName;
		}
	}

	abstract protected function getName();

	abstract protected function getNextState();

	protected function loadPostData(){
		if($this->caStateObjects[$this->csCurrentState]){
			$this->caStateObjects[$this->csCurrentState]->loadPostData();
		}
	}

	public function start(){
		$this->csCurrentState = $this->csInitialState;
		$this->caTrace = array();
		$this->draw();
	}

	public function isStarted(){
		return (bool) $this->csCurrentState;
	}

	public function goToNextState(){
		if(!$this->isStarted()){
			trigger_error("Wizard is not started.",E_USER_ERROR);
		}
		$this->loadPostData();
		$this->caTrace[] = $this->csCurrentState;
		$this->csCurrentState = $this->getNextState();
		$this->draw();
	}

	public function goToPreviousState(){
		if(!$this->isStarted()){
			trigger_error("Wizard is not started.",E_USER_ERROR);
		}
		$this->csCurrentState = array_pop($this->caTrace);
		$this->draw();
	}

	protected function isInitial($psState){
		return ($this->csInitialState==$psState);
	}

	protected function isFinal($psState){
		return in_array($psState,$this->caFinalStates);
	}

	public function run(){
		if($this->isStarted()){
			if(isset($_POST['wiz_state']) && $_POST['wiz_state']==$this->csCurrentState){
				if(isset($_POST['next'])){
					$this->goToNextState();
				}elseif(isset($_POST['previous'])){
					$this->goToPreviousState();
				}elseif(isset($_POST['finish'])){
					$this->finish();
					$this->drawFinish();
					$moSession = FWDWebLib::getInstance()->getSession(new FWDWizardSession('FWDWizard'));
					$moSession->destroySession();
					exit();
				}else{
					trigger_error("Undefined action.",E_USER_ERROR);
				}
			}else{
				$this->draw();
			}
		}else{
			$this->start();
		}
	}

	protected function drawHeader(){
		echo "<html>
          <head>
          <meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>
          <title>{$this->getName()}</title>
          <script src='".FWDWebLib::getInstance()->getLibRef()."js.php' type='text/javascript' language='javascript'></script>
          <link rel='StyleSheet' href='lib/css/basics.css' type='text/css'/>
          </head>
          <body>
          <form method='post'>
          <table border='0' width='100%'>
            <tr>
              <td width='20'></td>
              <td><h2>".($this->caTitles[$this->csCurrentState])."</h2></td>
              <td width='20'></td>
            </tr>
            <tr>
              <td></td>
              <td>";
	}

	protected function drawBody(){
		if($this->caStateObjects[$this->csCurrentState]){
			$this->caStateObjects[$this->csCurrentState]->draw();
		}else{
			echo $this->csCurrentState;
		}
	}

	abstract protected function finish();

	protected function drawFooter(){
		echo "</td><td></td></tr><tr><td></td><td><hr/>";
		echo "<input type='hidden' id='wiz_state' name='wiz_state' value='{$this->csCurrentState}'/>";
		echo "<input type='submit' class='button' name='previous' value='Voltar' accesskey='v' ".($this->isInitial($this->csCurrentState)?' disabled="disabled"':'')."/>&nbsp;&nbsp;&nbsp;";
		if($this->isFinal($this->csCurrentState)){
			echo "<input type='submit' class='button' name='finish' value='Concluir' accesskey='c'/>";
		}else{
			echo "<input type='submit' class='button' name='next' value='Avan�ar' accesskey='a'/>";
		}
		echo "</td><td></td></tr></table>";
		echo "</form></body></html>";
	}

	protected function draw(){
		$this->drawHeader();
		$this->drawBody();
		$this->drawFooter();
	}

	protected function getFinishMessage(){
		return "Os arquivos foram gerados na pasta '{$this->csOutputDir}'.";
	}

	protected function getAgainButtonText(){
		return "Iniciar novamente";
	}

	protected function drawFinish(){
		echo "<html>\n"
		."<head>\n"
		."<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>\n"
		."<title>{$this->getName()}</title>\n"
		."<link rel='StyleSheet' href='lib/css/basics.css' type='text/css'/>\n"
		."</head>\n"
		."<body>\n"
		."<form method='post'>\n"
		."<table border='0' width='100%'>\n"
		."  <tr>\n"
		."    <td width='20'></td>\n"
		."    <td><h2>Conclu�do</h2></td>\n"
		."    <td width='20'></td>\n"
		."  </tr>\n"
		."  <tr>\n"
		."    <td></td>\n"
		."    <td>\n"
		."      {$this->getFinishMessage()}<br/>\n"
		."    </td>\n"
		."    <td></td>\n"
		."  </tr>\n"
		."  <tr>\n"
		."    <td></td>\n"
		."    <td>\n"
		."      <hr/>\n"
		."      <input type='button' class='button' value='{$this->getAgainButtonText()}' onclick='location=location;'>\n"
		."    </td>\n"
		."    <td></td>\n"
		."  </tr>\n"
		."</table>\n"
		."</form>\n"
		."</body>\n"
		."</html>\n";
	}

}

?>