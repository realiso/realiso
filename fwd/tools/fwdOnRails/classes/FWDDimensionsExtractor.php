<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDDimensionsExtractor.
 * Extrai as dimens�es da dialog de um XML de tela.
 *
 * <p>Classe para extrair as dimens�es da dialog de um XML de tela.</p>
 * @package FWD5
 * @subpackage base
 */
class FWDDimensionsExtractor extends FWDXMLParser {

	protected $cbGetDimensions = false;
	protected $ciHeight = 0;
	protected $ciWidth = 0;

	protected function startHandler($prParser, $psTagName, $paAttributes){
		if($psTagName=='DIALOG'){
			$this->cbGetDimensions = true;
		}elseif($psTagName=='BOX'){
			if($this->cbGetDimensions){
				$this->ciHeight = $paAttributes['HEIGHT'];
				$this->ciWidth = $paAttributes['WIDTH'];
				$this->cbGetDimensions = false;
			}
		}
	}

	protected function dataHandler($prParser, $psData){}

	protected function endHandler($prParser, $psTagName){}

	public function getDialogHeight(){
		return $this->ciHeight;
	}

	public function getDialogWidth(){
		return $this->ciWidth;
	}

}

?>