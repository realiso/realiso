<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDWizardSession.
 *
 * <p></p>
 * @package Rails
 * @subpackage classes
 */
class FWDWizardSession extends FWDSession {

	public function __construct($psId){
		parent::__construct($psId);
		$this->addAttribute('WizardObject');
	}

	/**
	 * M�todo para autenticar o usu�rio
	 *
	 * <p>M�todo para autenticar o usu�rio.</p>
	 * @access public
	 * @param string $psUser Login do usu�rio
	 * @param string $psPassword Senha do usu�rio
	 * @return boolean Indica se o usu�rio foi autenticado ou n�o
	 */
	public function login($psUser,$psPassword = ""){
		return true;
	}

}

?>