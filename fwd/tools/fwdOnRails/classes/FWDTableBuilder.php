<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

include_once $handlers_ref."QueryColumns.php";

/**
 * Classe FWDTableBuilder.
 *
 * <p></p>
 * @package Rails
 * @subpackage classes
 */
class FWDTableBuilder extends FWDFieldsEditor {

	protected $csTableName;
	protected $csClassName;
	protected $csLabel;

	public function setup(){
		parent::setup();
		$this->csTableName = '';
		$this->csClassName = '';
		$this->csLabel = '';

		$maTypeOptions = array(
      'DB_UNKNOWN'  => 'DB_UNKNOWN',
      'DB_NUMBER'   => 'DB_NUMBER',
      'DB_STRING'   => 'DB_STRING',
      'DB_DATETIME' => 'DB_DATETIME',
		);

		$this->addProperty('name'            ,'Campo'     ,'text'    );
		$this->addProperty('alias'           ,'Alias'     ,'text'    );
		$this->addProperty('type'            ,'Tipo'      ,'select'  ,$maTypeOptions);
		$this->addProperty('alias_id'        ,'Id'        ,'radiobox');
		$this->addProperty('name_field_alias','Name Field','radiobox');
		$this->addProperty('sensitive'       ,'Sensitive' ,'checkbox');
		$this->addProperty('searchable'      ,'Searchable','checkbox');
		$this->addProperty('element'         ,'Element'   ,'select'  );
		$this->addProperty('required'        ,'Required'  ,'checkbox');
	}

	public function setTableName($psTableName){
		$this->csTableName = $psTableName;
		$this->loadFieldsFromDB();
	}

	public function setClassName($psClassName){
		$this->csClassName = $psClassName;
	}

	public function setLabel($psLabel){
		$this->csLabel = $psLabel;
	}

	public function getFieldPropertyOptions($psFieldName,$psPropertyName){
		if($psPropertyName=='element'){
			$msPrefix = FWDWizardLib::getFieldPrefix($psFieldName);
			$maOptions = array(''=>'N�o edit�vel');
			switch($msPrefix){
				case 's': case 't':
					$maOptions['text'] = 'text';
					$maOptions['memo'] = 'memo';
					break;
				case 'f': case 'n':
					$maOptions['text'] = 'text';
					break;
				case 'fk':
					$maOptions['select'] = 'select';
					$maOptions['lookup'] = 'lookup';
					break;
				case 'd':
					$maOptions['calendar'] = 'calendar';
					$maOptions['text'] = 'text';
					break;
			}
			return $maOptions;
		}else{
			return parent::getFieldPropertyOptions($psFieldName,$psPropertyName);
		}
	}

	protected function loadFieldsFromDB(){
		$moQuery = new QueryColumns();
		$moQuery->setTable($this->csTableName);
		$maColumns = $moQuery->getColumns();
		$this->caFields = array();
		$msLowerCaseEntityName = FWDWizardLib::getLowerCaseEntityName($this->csTableName);
		foreach($maColumns as $maColumnInfo){

			$maColumnInfo['sensitive'] = false;
			$maColumnInfo['searchable'] = false;
			$maColumnInfo['required'] = false;
			$maColumnInfo['element'] = '';

			switch($maColumnInfo['prefix']){
				case 'fk':{
					$maColumnInfo['required'] = true;
					$maColumnInfo['element'] = 'lookup';
					break;
				}
				case 's':{
					$maColumnInfo['searchable'] = true;
					$maColumnInfo['element'] = 'text';
					break;
				}
				case 't':{
					$maColumnInfo['searchable'] = true;
					$maColumnInfo['element'] = 'memo';
					break;
				}
				case 'd':{
					$maColumnInfo['element'] = 'calendar';
					break;
				}
				case 'n': case 'f':{
					$maColumnInfo['element'] = 'text';
					break;
				}
			}

			$maColumnInfo['short_alias'] = $maColumnInfo['alias'];
			$maColumnInfo['alias'] = "{$msLowerCaseEntityName}_{$maColumnInfo['alias']}";

			$maColumnInfo['alias_id'] = false;
			$maColumnInfo['name_field_alias'] = false;

			if($maColumnInfo['name']=='fkcontext'){
				$maColumnInfo['alias'] = "{$msLowerCaseEntityName}_id";
				$maColumnInfo['required'] = false;
				$maColumnInfo['element'] = '';
				$maColumnInfo['alias_id'] = true;
			}elseif($maColumnInfo['name']=='sname'){
				$maColumnInfo['required'] = true;
				$maColumnInfo['name_field_alias'] = true;
			}

			$this->addField($maColumnInfo['name'],$maColumnInfo);
		}
	}

	public function getPhp(){
		$msOutput = '';
		$maSearchable = array();
		$maSensitive = array();
		$msAliasId = $this->getFieldProperty($this->getSelectedField('alias_id'),'alias');
		$msNameFieldAlias = $this->getFieldProperty($this->getSelectedField('name_field_alias'),'alias');
		$msScreensRef = FWDWizardLib::getInstance()->getScreensRef(FWDWizardLib::getPrefix($this->csTableName));
		$msIconName = FWDWizardLib::getIconName($this->csTableName);
		$msConstantName = FWDWizardLib::getConstantName($this->csTableName);

		$msLowerCaseEntityName = FWDWizardLib::getLowerCaseEntityName($this->csTableName);

		$maMaxLenghts = array('name'=>0,'type'=>0,'alias'=>0);
		foreach($this->caFields as $maColumnInfo){
			foreach($maMaxLenghts as $msField=>$_){
				$maMaxLenghts[$msField] = max($maMaxLenghts[$msField],strlen($maColumnInfo[$msField]));
			}
		}

		foreach($this->caFields as $msColumnName=>$maColumnInfo){
			if($maColumnInfo['searchable']) $maSearchable[] = $maColumnInfo['alias'];
			if($maColumnInfo['sensitive']) $maSensitive[] = $maColumnInfo['alias'];
			$msOutput.= "    \$this->coDataset->addFWDDBField(new FWDDBField("
			."'{$maColumnInfo['name']}'".str_repeat(' ',$maMaxLenghts['name'] - strlen($maColumnInfo['name'])).","
			."'{$maColumnInfo['alias']}'".str_repeat(' ',$maMaxLenghts['alias'] - strlen($maColumnInfo['alias'])).","
			."{$maColumnInfo['type']}".str_repeat(' ',$maMaxLenghts['type'] - strlen($maColumnInfo['type']))."));\n";
		}
		$msPHPClass = "<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

class {$this->csClassName} extends ISMSContext {

 /**
  * Construtor.
  *
  * <p>Construtor da classe {$this->csClassName}.</p>
  * @access public
  */
  public function __construct(){
    \$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,\$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    parent::__construct('{$this->csTableName}');\n";

		$msPHPClass.= "\n    \$this->ciContextType = {$msConstantName};\n";

		if($msAliasId){
			$msPHPClass.= "\n    \$this->csAliasId = '$msAliasId';\n";
		}
		$msPHPClass.= $msOutput."\n";
		if(count($maSensitive)){
			$msPHPClass.= "    \$this->caSensitiveFields = array('".implode("', '",$maSensitive)."');\n";
		}
		if(count($maSearchable)){
			$msPHPClass.= "    \$this->caSearchableFields = array('".implode("', '",$maSearchable)."');\n";
		}
		$msPHPClass.= "  }";

		$msLabel = strtolower($this->csLabel);
		$msPHPClass.= "

 /**
  * Retorna o label do contexto.
  *
  * <p>M�todo para retornar o label do contexto.</p>
  * @access public
  * @return string Label do contexto
  */
  public function getLabel(){
    \$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,\$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    return FWDLanguage::getPHPStringValue('label_{$msLowerCaseEntityName}','{$this->csLabel}');
  }

 /**
  * Retorna o nome do(a) {$msLabel}.
  *
  * <p>Retorna o nome do(a) {$msLabel}.</p>
  * @access public
  * @return string Nome do(a) {$msLabel}
  */
  public function getName(){
    \$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,\$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    return \$this->getFieldValue('{$msNameFieldAlias}');
  }
  
 /**
  * Retorna a descri��o do(a) {$msLabel}.
  *
  * <p>Retorna a descri��o do(a) {$msLabel}.</p>
  * @access public
  * @return string Descri��o do(a) {$msLabel}
  */
  public function getDescription(){
    \$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,\$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    //<to_do>implementar esse m�todo</to_do>
    return '';
  }
  
 /**
  * Retorna o id do usu�rio respons�vel pelo(a) {$msLabel}.
  *
  * <p>Retorna o id do usu�rio respons�vel pelo(a) {$msLabel}.</p>
  * @access public
  * @return integer Id do respons�vel
  */
  public function getResponsible(){
    \$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,\$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    //<to_do>implementar esse m�todo</to_do>
    return 0;
  }
  
 /**
  * Verifica se o usu�rio pode inserir um(a) {$msLabel}.
  * 
  * <p>M�todo para verificar se o usu�rio pode inserir um(a) {$msLabel}.</p>
  * @access public
  * @return boolean True, sse pode inserir
  */
  public function userCanInsert(){
    \$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,\$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    //<to_do>implementar esse m�todo</to_do>
    return true;
  }
  
 /**
  * Verifica se o usu�rio pode editar o(a) {$msLabel}.
  * 
  * <p>M�todo para verificar se o usu�rio pode editar o(a) {$msLabel}.</p>
  * @access public
  * @param integer \$piContextId Id do(a) {$msLabel}
  * @return boolean True, sse pode editar
  */
  public function userCanEdit(\$piContextId,\$piUserResponsible = 0){
    \$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,\$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    //<to_do>implementar esse m�todo</to_do>
    return true;
  }
  
 /**
  * Verifica se o usu�rio pode deletar o(a) {$msLabel}.
  * 
  * <p>M�todo para verificar se o usu�rio pode deletar o(a) {$msLabel}.</p>
  * @access public
  * @param integer \$piContextId Id do(a) {$msLabel}
  * @return boolean True, sse pode deletar
  */
  public function userCanDelete(\$piContextId){
    \$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,\$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    //<to_do>implementar esse m�todo</to_do>
    return true;
  }
  
 /**
  * Retorna o c�digo JavaScript para abrir a popup de edi��o de {$msLabel}.
  * 
  * <p>Retorna o c�digo JavaScript para abrir a popup de edi��o de {$msLabel}.</p>
  * @param integer \$piContextId Id do(a) {$msLabel}
  * @param string \$psUniqId Id da popup de visualiza��o
  * @access public
  * @return string C�digo JavaScript para abrir a popup de edi��o
  */
  public function getVisualizeEditEvent(\$piContextId,\$psUniqId){
    \$soSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    \$msPackagePath = ((\$soSession->getMode()==MANAGER_MODE) ? '{$msScreensRef}' : '../../{$msScreensRef}');
    \$msPopupId = 'popup_{$msLowerCaseEntityName}_edit';
    
    return \"isms_open_popup('{\$msPopupId}','{\$msPackagePath}{\$msPopupId}.php?example={\$piContextId}','','true');\"
           .\"soPopUpManager.getPopUpById('{\$msPopupId}').setCloseEvent(function(){soPopUpManager.closePopUp('popup_visualize_{\$psUniqId}');});\";
  }
  
 /**
  * Retorna o c�digo HTML do �cone do contexto.
  * 
  * <p>M�todo para retornar o c�digo HTML do contexto</p>
  * @access public
  * @param integer \$piId Id do contexto
  * @param integer \$pbIsFetched True sse o contexto esta \"fetchado\"
  * @param integer \$piTop Top do �cone
  * @return string C�digo HTML do �cone do contexto
  */
  public function getIconCode(\$piId, \$pbIsFetched = false, \$piTop = 7){
    return ISMSLib::getIconCode('{$msIconName}', -2, \$piTop);
  }

 /**
  * Retorna o caminho do contexto.
  * 
  * <p>M�todo para retornar o caminho do contexto.</p>
  * @access public 
  * @return string Caminho do contexto
  */
  public function getSystemPath(){
    \$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,\$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    //<to_do>implementar esse m�todo</to_do>
    \$miModuleId = 0; //%MODULE%_MODE;
    \$msModuleName = FWDLanguage::getPHPStringValue('st_%module%_module_name','Gest�o de Alguma Coisa');
    \$miId = \$this->getFieldValue(\$this->getAliasId());
    \$msName = \$this->getName();
    \$msModulePath = \"<a href='javascript:isms_redirect_to_mode(\$miModuleId,0,0)'>\$msModuleName</a>\";
    \$msContextPath = \$this->getIconCode(\$miId,true,-4)
                    .\"<a href='javascript:isms_redirect_to_mode(\$miModuleId,{$msConstantName},\$miId.)'>\"
                    .\$msName
                    .\"</a>\";
    return \$msModulePath .'&nbsp;'.ISMSLib::getIconCode('icon-arrow-right.gif',-2,0).'&nbsp;' . \$msContextPath;
  }
  
 /**
  * Indica se o contexto � delet�vel ou n�o.
  * 
  * <p>M�todo que indica se o contexto � delet�vel ou n�o.</p>
  * @access public
  * @param integer \$piContextId id do contexto
  */
  public function isDeletable(\$piContextId){
    \$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,\$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    //<to_do>implementar esse m�todo</to_do>
    return true;
  }
  
 /**
  * Exibe uma popup caso n�o seja poss�vel remover o contexto.
  * 
  * <p>Exibe uma popup caso n�o seja poss�vel remover o contexto.</p>
  * @access public
  * @param integer \$piContextId id do contexto
  */
  public function showDeleteError(){
    \$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,\$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    //<to_do>implementar esse m�todo</to_do>
    \$msTitle = FWDLanguage::getPHPStringValue('tt_{$msLowerCaseEntityName}_remove_error_title','Erro ao remover {$this->csLabel}');
    \$msMessage = FWDLanguage::getPHPStringValue('st_{$msLowerCaseEntityName}_remove_error_message',\"N�o foi poss�vel remover o(a) {$this->csLabel}, pois...\");
    ISMSLib::openOk(\$msTitle,\$msMessage,'',60);
  }
  
}

?>";
		return $msPHPClass;
	}

}

?>