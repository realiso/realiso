<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDStateEditFields.
 *
 * <p>Classe que define o estado de edi��o de campos.</p>
 * @package Rails
 * @subpackage states
 */
class FWDStateEditFields extends FWDWizardState {

	public function __construct(IFieldsEditor $poStateHandler){
		parent::__construct($poStateHandler);
	}

	public function loadPostData(){
		$maProperties = $this->coHandler->getProperties();
		$maFields = $this->coHandler->getFields();
		foreach($maProperties as $msPropertyName){
			$msPropertyType = $this->coHandler->getPropertyType($msPropertyName);
			if($msPropertyType=='radiobox'){
				$msSelectedField = (isset($_POST[$msPropertyName])?$_POST[$msPropertyName]:'');
				foreach($maFields as $msFieldName){
					if($msFieldName==$msSelectedField){
						$this->coHandler->setFieldProperty($msFieldName,$msPropertyName,true);
					}else{
						$this->coHandler->setFieldProperty($msFieldName,$msPropertyName,false);
					}
				}
			}elseif($msPropertyType!='static'){
				foreach($maFields as $msFieldName){
					$msPostFieldName = str_replace('.','_',$msFieldName).'_'.$msPropertyName;
					$mmValue = (isset($_POST[$msPostFieldName])?$_POST[$msPostFieldName]:'');
					if($msPropertyType=='checkbox') $mmValue = (bool) $mmValue;
					$this->coHandler->setFieldProperty($msFieldName,$msPropertyName,$mmValue);
				}
			}
		}
	}

	public function draw(){
		$maProperties = $this->coHandler->getProperties();
		$maFields = $this->coHandler->getFields();
		// Desenha o cabe�alho da tabela
		echo "<table class='columns'>\n"
		."  <tr>\n";
		foreach($maProperties as $msPropertyName){
			$msPropertyLabel = $this->coHandler->getPropertyLabel($msPropertyName);
			echo "    <td class='colHeader'><b>{$msPropertyLabel}</b></td>\n";
		}
		echo "  </tr>\n";
		// Desenha as linhas da tabela
		$mbIsEven = false;
		foreach($maFields as $msFieldName){
			$msRowClass = ($mbIsEven?'evenRow':'oddRow');
			$mbIsEven = !$mbIsEven;
			echo "<tr class='{$msRowClass}'>\n";
			foreach($maProperties as $msPropertyName){
				$msPropertyValue = $this->coHandler->getFieldProperty($msFieldName,$msPropertyName);
				$msCellExtraAttributes = '';
				$msCellInnerHTML = '';
				switch($this->coHandler->getPropertyType($msPropertyName)){
					case 'static':{
						$msCellExtraAttributes = " style='text-align:left;font-weight:bolder;padding-left:10px;font-size:12pt;'";
						$msCellInnerHTML = $msPropertyValue;
						break;
					}
					case 'text':{
						$msCellInnerHTML = "<input type='text' name='{$msFieldName}_{$msPropertyName}' value='{$msPropertyValue}' style='width:100%'/>";
						break;
					}
					case 'select':{
						$maOptions = $this->coHandler->getFieldPropertyOptions($msFieldName,$msPropertyName);
						$msCellInnerHTML.= "  <select name='{$msFieldName}_{$msPropertyName}' style='width:100%'>\n";
						foreach($maOptions as $msValue=>$msText){
							$msSelected = ($msPropertyValue==$msValue?" selected='selected'":"");
							$msCellInnerHTML.= "<option value='$msValue'$msSelected>$msText</option>\n";
						}
						$msCellInnerHTML.= "  </select>\n";
						break;
					}
					case 'checkbox':{
						$msChecked = ($msPropertyValue?" checked='checked'":"");
						$msCellInnerHTML = "<input type='checkbox' name='{$msFieldName}_{$msPropertyName}'{$msChecked}/>";
						break;
					}
					case 'radiobox':{
						$msChecked = ($msPropertyValue?" checked='checked'":"");
						$msCellInnerHTML = "<input type='radio' name='{$msPropertyName}' value='{$msFieldName}'{$msChecked}/>";
						break;
					}
				}
				echo "<td class='colCell'{$msCellExtraAttributes}>\n";
				echo $msCellInnerHTML;
				echo "</td>\n";
			}
			echo "</tr>\n";
		}
		echo "</table>\n";
	}

}

?>