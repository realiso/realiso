<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

include_once $handlers_ref."QueryTables.php";

/**
 * Classe FWDStateSetQuery.
 *
 * <p>Classe que define o estado de defini��o da query.</p>
 * @package Rails
 * @subpackage states
 */
class FWDStateSetQuery extends FWDWizardState {

	public function __construct(ITableQueryBuilder $poStateHandler){
		parent::__construct($poStateHandler);
	}

	public function loadPostData(){
		$this->coHandler->setTableName(isset($_POST['table_name'])?$_POST['table_name']:'');
		$this->coHandler->setQuery(isset($_POST['custom_query'])?$_POST['custom_query']:'');
		$this->coHandler->setQueryType($_POST['query_type']);
	}

	public function draw(){
		$msSelectedTableName = $this->coHandler->getTableName();
		$msQuery = $this->coHandler->getQuery();
		$msQueryType = $this->coHandler->getQueryType();
		?>
<script type="text/javascript">
        function updateEnabled(psEnabled){
          if(psEnabled=='table'){
            gebi('custom_query').disabled = true;
            gebi('table_name').disabled = false;
          }else{
            gebi('custom_query').disabled = false;
            gebi('table_name').disabled = true;
          }
        }
      </script>
<input
	type="radio" name="query_type" value="table"
	onChange="updateEnabled(this.value)"
	<?=($msQueryType=='table'?' checked="checked"':'')?> />
<b>Query de uma s� tabela</b>
<br />
<div class="panel">
<table>
	<tr>
		<td class="fieldLabel">Selecione a tabela:</td>
		<td><select id="table_name" name="table_name">
			<option value=""></option>
			<?
			$moQuery = new QueryTables();
			$maTableNames = $moQuery->getTableNames();
			foreach($maTableNames as $msTableName){
				$msSelected = ($msTableName==$msSelectedTableName?' selected="selected"':'');
				echo "<option value='$msTableName'$msSelected>$msTableName</option>";
			}
			?>
		</select></td>
	</tr>
</table>
</div>
<input
	type="radio" name="query_type" value="custom_query"
	onChange="updateEnabled(this.value)"
	<?=($msQueryType=='custom_query'?' checked="checked"':'')?> />
<b>Query customizada</b>
<br />
<div class="panel">
<table>
	<tr>
		<td class="fieldLabel">SQL da query:</td>
		<td><textarea id="custom_query" name="custom_query" rows="10"
			cols="100" disabled="disabled"><?=$msQuery?></textarea></td>
	</tr>
</table>
</div>
<script type="text/javascript">updateEnabled('<?=$msQueryType?>');</script>
	<?
	}

}

?>