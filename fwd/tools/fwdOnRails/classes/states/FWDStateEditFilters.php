<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDStateEditFilters.
 *
 * <p>Classe que define o estado de edi��o de filtros.</p>
 * @package Rails
 * @subpackage states
 */
class FWDStateEditFilters extends FWDWizardState {

	public function __construct(IFilterEditor $poStateHandler){
		parent::__construct($poStateHandler);
	}

	public function loadPostData(){
		$maFilters = explode('|',$_POST['serializedFilters']);
		$this->coHandler->clearFilters();
		foreach($maFilters as $msFilter){
			if($msFilter){
				list($msFilterName,$msFieldAlias,$msFilterType) = explode(':',$msFilter);
				$this->coHandler->addFilter($msFieldAlias,$msFilterType,$msFilterName);
			}
		}
	}

	public function draw(){
		?>
<script type="text/javascript">
      
        function FWDItemList(psSelectName,psVariableName){
          var self = this;
          
          self.csSelectName = psSelectName;
          self.csVariableName = psVariableName;
          self.caProperties = [];
          self.caItems = {};
          
          document.getElementsByTagName('form')[0].onsubmit = function(){
            gebi(psVariableName).value = self.getValue();
          };
          
          self.addProperty = function(psPropertyName){
            self.caProperties.push(psPropertyName);
          }
          
          self.addItem = function(psItemName){
            var maProperties;
            var moSelect;
            var msText;
            var i;
            if(arguments.length==self.caProperties.length+1){
              maProperties = arguments.splice(0,1);
            }else if(arguments.length==1){
              maProperties = [];
              for(i=0;i<self.caProperties.length;i++){
                maProperties.push(gebi(self.caProperties[i]).value);
              }
            }else{
              alert('Wrong parameter count in FWDItemList.addItem');
              return;
            }
            self.caItems[psItemName] = {'name':psItemName};
            for(i=0;i<self.caProperties.length;i++){
              self.caItems[psItemName][self.caProperties[i]] = maProperties[i];
            }
            moSelect = gebi(self.csSelectName);
            msText = maProperties.join(' | ');
            moSelect.options[moSelect.options.length] = new Option(msText,psItemName);
          }
          
          self.removeSelectedItems = function(){
            var moSelect = gebi(self.csSelectName);
            var i;
            for(i=moSelect.options.length-1;i>=0;i--){
              if(moSelect.options[i].selected){
                delete self.caItems[moSelect.options[i].value];
                moSelect.options[i] = null;
              }
            }
          }
          
          self.getValue = function(){
            var maProperties;
            var maItems = [];
            var i,j;
            maItems.push(self.caProperties.join(':'));
            for(i in self.caItems){
              maProperties = [];
              for(j=0;j<self.caProperties.length;j++){
                maProperties.push(self.caItems[i][self.caProperties[j]]);
              }
              maItems.push(maProperties.join(':'));
            }
            return maItems.join('|');
          }
          
          self.loadFromVariable = function(){
            var moItem;
            var moSelect;
            var msText;
            var i,j;
            var msSerializedValue = gebi(self.csVariableName).value;
            var maItems = msSerializedValue.split('|');
            self.caProperties = maItems[0].split(':');
            self.caItems = {};
            for(i=1;i<maItems.length;i++){
              maItems[i] = maItems[i].split(':');
              moItem = {};
              for(j=0;j<self.caProperties.length;j++){
                moItem[self.caProperties[j]] = maItems[i][j];
              }
              self.caItems[moItem.name] = moItem;
              moSelect = gebi(self.csSelectName);
              msText = maProperties.join(' | ');
              moSelect.options[moSelect.options.length] = new Option(msText,psItemName);
            }
          }
          
        }
      
        var Filters = {
          
          'caFieldTypes': {},
          
          'caFiltersByType': {
            'DB_NUMBER': ['=','<','>','<=','>=','IN','NOT IN'],
            'DB_STRING': ['LIKE'],
            'DB_DATETIME': []
          },
          
          'caFilters': {},
          
          'addField': function(psField,psFieldType){
            this.caFieldTypes[psField] = psFieldType;
          },
          
          'init': function(){
            var i;
            var msText;
            var moSelect;
            // Inicializa select de campos
            moSelect = gebi('field');
            for(var i=moSelect.options.length-1;i>0;i--){
              moSelect.options[i] = null;
            }
            moSelect.options[0] = new Option('','');
            for(var msField in this.caFieldTypes){
              moSelect.options[moSelect.options.length] = new Option(msField,msField);
            }
            // Inicializa select de filtros
            for(i in this.caFilters){
              this.addFilterToSelect(i);
            }
          },
          
          'addFilterToSelect': function(psName){
            var moSelect = gebi('filters');
            var msText = 'Nome: '+this.caFilters[psName]['name']
                     +' | Campo: '+this.caFilters[psName]['field']
                     +' | Tipo: '+this.caFilters[psName]['type'];
            moSelect.options[moSelect.options.length] = new Option(msText,psName);
          },
          
          'refreshTypesSelect': function(){
            var msField = gebi('field').value;
            var moSelect = gebi('type');
            var msField = gebi('field').value;
            var msFieldType = this.caFieldTypes[msField];
            for(var i=moSelect.options.length-1;i>0;i--){
              moSelect.options[i] = null;
            }
            moSelect.options[0] = new Option('','');
            var maFilterTypes = this.caFiltersByType[msFieldType];
            for(var i=0;i<maFilterTypes.length;i++){
              moSelect.options[moSelect.options.length] = new Option(maFilterTypes[i],maFilterTypes[i]);
            }
          },
          
          'clearName': function(){
            gebi('name').value = '';
          },
          
          'addFilter': function(psName,psField,psType){
            if(arguments.length==3){
              this.caFilters[psName] = {'name': psName, 'field': psField, 'type': psType};
            }else{
              var msName = gebi('name').value;
              var msField = gebi('field').value;
              var msType = gebi('type').value;
              if(msName && msField && msType && !this.caFilters[msName]){
                this.caFilters[msName] = {'name': msName, 'field': msField, 'type': msType};
                this.addFilterToSelect(msName);
                this.clearName();
              }
            }
          },
          
          'removeFilters': function(){
            var moSelect = gebi('filters');
            var i;
            for(i=moSelect.options.length-1;i>=0;i--){
              if(moSelect.options[i].selected){
                delete this.caFilters[moSelect.options[i].value];
                moSelect.options[i] = null;
              }
            }
          },
          
          'toString': function(){
            var maFilters = [];
            for(var i in this.caFilters){
              maFilters.push(this.caFilters[i]['name']+':'+this.caFilters[i]['field']+':'+this.caFilters[i]['type']);
            }
            return maFilters.join('|');
          }
          
        }
        <?
          
          $maFieldTypes = $this->coHandler->getFieldTypes();
          foreach($maFieldTypes as $msFieldAlias=>$msFieldType){
            echo "Filters.addField('{$msFieldAlias}','{$msFieldType}');\n";
          }
          
          $maFilters = $this->coHandler->getFilters();
          foreach($maFilters as $maFilterInfo){
            echo "Filters.addFilter('{$maFilterInfo['name']}','{$maFilterInfo['field']}','{$maFilterInfo['type']}')\n";
          }
          
        ?>
      </script>
<input
	type="hidden" name="serializedFilters" id="serializedFilters" />
Campo:
<select name="field" id="field"
	onChange="Filters.refreshTypesSelect();Filters.clearName()">
</select>
Tipo:
<select name="type" id="type">
	<option value=""></option>
</select>
Nome:
<input type="text" name="name"
	id="name" />
<input
	type="button" class="button" value="Adicionar"
	onclick="Filters.addFilter()" />
<br />
Filtros
<br />
<select name="filters" id="filters" multiple
	style="height: 300; width: 500">
</select>
<br />
<input
	type="button" class="button" value="Remover"
	onclick="Filters.removeFilters()" />
<script type="text/javascript">
        Filters.init();
        document.getElementsByTagName('form')[0].onsubmit = function(){
          gebi('serializedFilters').value = Filters.toString();
        };
      </script>
        <?
	}

}

?>