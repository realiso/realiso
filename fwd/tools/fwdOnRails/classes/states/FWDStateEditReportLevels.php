<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDStateEditReportLevels.
 *
 * <p>Classe que define o estado de edi��o de n�veis de relat�rio.</p>
 * @package Rails
 * @subpackage states
 */
class FWDStateEditReportLevels extends FWDWizardState {

	public function __construct(IReportBuilder $poStateHandler){
		parent::__construct($poStateHandler);
	}

	public function loadPostData(){
		$maLevels = explode('|',$_POST['serializedLevels']);
		$maProperties = explode(':',$maLevels[0]);
		unset($maLevels[0]);
		$this->coHandler->clearLevels();
		foreach($maLevels as $msLevel){
			$maLevel = array_combine($maProperties,explode(':',$msLevel));
			$this->coHandler->addLevel($maLevel['name'],$maLevel['suppressible']);
		}
	}

	public function draw(){
		$maLevels = $this->coHandler->getLevels();
		for($i=0;$i<count($maLevels);$i++){
			$maLevels[$i] = implode(':',$maLevels[$i]);
		}
		$msSerializedValue = 'name:suppressible';
		if(count($maLevels)>0){
			$msSerializedValue.= '|'.implode('|',$maLevels);
		}
		?>
<script type="text/javascript">
      
        function FWDItemList(psSelectName,psVariableName){
          var self = this;
          
          self.csSelectName = psSelectName;
          self.csVariableName = psVariableName;
          self.caProperties = [];
          self.caItems = {};
          
          document.getElementsByTagName('form')[0].onsubmit = function(){
            gebi(psVariableName).value = self.getValue();
          };
          
          gebi(psSelectName).onchange = function(){
            var msSelectedItem = null;
            for(i=0;i<this.options.length;i++){
              if(this.options[i].selected){
                if(msSelectedItem){
                  msSelectedItem = null;
                  break;
                }else{
                  msSelectedItem = this.options[i].value;
                }
              }
            }
            if(msSelectedItem){
              for(i=0;i<self.caProperties.length;i++){
                gebi(self.caProperties[i]).value = self.caItems[msSelectedItem][self.caProperties[i]];
              }
            }else{
              for(i=0;i<self.caProperties.length;i++){
                gebi(self.caProperties[i]).value = '';
              }
            }
          }
          
          self.addProperty = function(psPropertyName){
            self.caProperties.push(psPropertyName);
          }
          
          self.addItem = function(psItemName){
            if(!psItemName) return;
            var maProperties;
            var moSelect;
            var msText;
            var i;
            if(arguments.length==self.caProperties.length+1){
              maProperties = arguments.splice(0,1);
            }else if(arguments.length==1){
              maProperties = [];
              for(i=0;i<self.caProperties.length;i++){
                maProperties.push(gebi(self.caProperties[i]).value);
              }
            }else{
              alert('Wrong parameter count in FWDItemList.addItem');
              return;
            }
            self.caItems[psItemName] = {'name':psItemName};
            for(i=0;i<self.caProperties.length;i++){
              self.caItems[psItemName][self.caProperties[i]] = maProperties[i];
            }
            moSelect = gebi(self.csSelectName);
            msText = maProperties.join(' | ');
            for(i=0;i<moSelect.options.length;i++){
              if(moSelect.options[i].value==psItemName) break;
            }
            moSelect.options[i] = new Option(msText,psItemName);
          }
          
          self.removeSelectedItems = function(){
            var moSelect = gebi(self.csSelectName);
            var i;
            for(i=moSelect.options.length-1;i>=0;i--){
              if(moSelect.options[i].selected){
                delete self.caItems[moSelect.options[i].value];
                moSelect.options[i] = null;
              }
            }
          }
          
          self.getValue = function(){
            var maProperties;
            var maItems = [];
            var i,j;
            var moSelect = gebi(self.csSelectName);
            maItems.push(self.caProperties.join(':'));
            for(i=0;i<moSelect.options.length;i++){
              maProperties = [];
              for(j=0;j<self.caProperties.length;j++){
                maProperties.push(self.caItems[moSelect.options[i].value][self.caProperties[j]]);
              }
              maItems.push(maProperties.join(':'));
            }
            return maItems.join('|');
          }
          
          self.loadFromVariable = function(){
            var moItem;
            var moSelect;
            var msText;
            var i,j;
            var msSerializedValue = gebi(self.csVariableName).value;
            var maItems = msSerializedValue.split('|');
            self.caProperties = maItems[0].split(':');
            self.caItems = {};
            for(i=1;i<maItems.length;i++){
              maItems[i] = maItems[i].split(':');
              moItem = {};
              for(j=0;j<self.caProperties.length;j++){
                moItem[self.caProperties[j]] = maItems[i][j];
              }
              self.caItems[moItem.name] = moItem;
              moSelect = gebi(self.csSelectName);
              msText = maItems[i].join(' | ');
              moSelect.options[moSelect.options.length] = new Option(msText,moItem.name);
            }
          }
          
        }
        
      </script>
<input
	type="hidden" name="serializedLevels" id="serializedLevels"
	value="<?=$msSerializedValue?>" />
Nome:
<input type="text" name="name"
	id="name" />
Suprim�vel:
<select name="suppressible" id="suppressible">
	<option value="false">false</option>
	<option value="true">true</option>
</select>
<input
	type="button" class="button" value="Adicionar"
	onclick="soLevelList.addItem(gebi('name').value)" />
<br />
<br />
<select
	name="levels" id="levels" multiple style="height: 300; width: 500"></select>
<br />
<input
	type="button" class="button" value="Remover"
	onclick="soLevelList.removeSelectedItems()" />
&nbsp;
<input
	type="button" class="button" value="Mover Para Cima"
	onclick="gobi('levels').moveUp()">
&nbsp;
<input
	type="button" class="button" value="Mover Para Baixo"
	onclick="gobi('levels').moveDown()">
<script type="text/javascript">
        gebi('levels').object = new FWDSelect('levels');
        var soLevelList = new FWDItemList('levels','serializedLevels');
        soLevelList.loadFromVariable();
      </script>
		<?
	}

}

?>