<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDStateClassBasics.
 *
 * <p>Classe que define o estado de defini��o de atributos b�sicos de uma classe.</p>
 * @package Rails
 * @subpackage states
 */
class FWDStateClassBasics extends FWDWizardState {

	public function __construct(IClassBuilder $poStateHandler){
		parent::__construct($poStateHandler);
	}

	public function loadPostData(){
		$this->coHandler->setName($_POST['name']);
		$this->coHandler->setPackage($_POST['package']);
		$this->coHandler->setDescription($_POST['description']);
	}

	public function draw(){
		$msClassName = $this->coHandler->getName();
		$msSelectedValue = $this->coHandler->getPackageAcronym();
		$msDescription = $this->coHandler->getDescription();
		$maPackages = FWDWizardLib::getInstance()->getPackages();
		echo "<table>\n"
		."  <tr>\n"
		."    <td class='fieldLabel'>Nome da classe:</td>\n"
		."    <td>\n"
		."      <input type='text' name='name' value='{$msClassName}'/>\n"
		."    </td>\n"
		."  </tr>\n"
		."  <tr>\n"
		."    <td class='fieldLabel'>Pacote:</td>\n"
		."    <td>\n"
		."      <select name='package'>\n"
		."        <option value=''></option>\n";
		foreach($maPackages as $maPackageInfo){
			$msValue = $maPackageInfo['acronym'];
			$msText = $maPackageInfo['acronym'].' - '.$maPackageInfo['name'];
			$msSelected = ($msValue==$msSelectedValue?' selected="selected"':'');
			echo "<option value='{$msValue}'{$msSelected}>{$msText}</option>\n";
		}
		echo "      </select>\n"
		."    </td>\n"
		."  </tr>\n"
		."  <tr>\n"
		."    <td class='fieldLabel'>Descri��o (coment�rio):</td>\n"
		."    <td>\n"
		."      <textarea name='description' rows='3' cols='60'>{$msDescription}</textarea>\n"
		."    </td>\n"
		."  </tr>\n"
		."</table>\n";
	}

}

?>