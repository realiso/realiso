<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDStateContextNavOptions.
 *
 * <p>Classe que define o estado de op��es da nav de contextos.</p>
 * @package Rails
 * @subpackage states
 */
class FWDStateContextNavOptions extends FWDWizardState {

	public function __construct(IContextNavBuilder $poStateHandler){
		parent::__construct($poStateHandler);
	}

	public function loadPostData(){
		$this->coHandler->setNameColumnTitle($_POST['name_column_title']);
		$this->coHandler->setShowLinkPopupVisualize(isset($_POST['show_link_popup_visualize']));
		$this->coHandler->setShowTooltipCreateModify(isset($_POST['show_tooltip_create_modify']));
		$this->coHandler->setShowStatusColumn(isset($_POST['show_status_column']));
		$this->coHandler->setColumnsOrder(explode(':',$_POST['serialized_columns_order']));
	}

	public function draw(){
		$msNameColumnTitle = $this->coHandler->getNameColumnTitle();
		$msShowLinkPopupVisualizeChecked = ($this->coHandler->getShowLinkPopupVisualize()?" checked='checked'":"");
		$msShowTooltipCreateModifyChecked = ($this->coHandler->getShowTooltipCreateModify()?" checked='checked'":"");
		$msShowStatusColumnChecked = ($this->coHandler->getShowStatusColumn()?" checked='checked'":"");
		$maColumns = $this->coHandler->getColumns();
		echo "<table>\n"
		."  <tr>\n"
		."    <td></td>\n"
		."    <td class='fieldLabel' style='text-align:left'>\n"
		."      <input type='checkbox' name='show_link_popup_visualize'{$msShowLinkPopupVisualizeChecked}/>\n"
		."      Link para abrir popup de visualize na coluna de nomes\n"
		."    </td>\n"
		."  </tr>\n"
		."  <tr>\n"
		."    <td></td>\n"
		."    <td class='fieldLabel' style='text-align:left'>\n"
		."      <input type='checkbox' name='show_tooltip_create_modify'{$msShowTooltipCreateModifyChecked}/>\n"
		."      ToolTip de create modify na coluna do �cone\n"
		."    </td>\n"
		."  </tr>\n"
		."  <tr>\n"
		."    <td></td>\n"
		."    <td class='fieldLabel' style='text-align:left'>\n"
		."      <input type='checkbox' name='show_status_column' id='show_status_column'{$msShowStatusColumnChecked}/>\n"
		."      Exibir coluna de status\n"
		."    </td>\n"
		."  </tr>\n"
		."  <tr>\n"
		."    <td class='fieldLabel'>T�tulo da Coluna de Nome:</td>\n"
		."    <td>\n"
		."      <input type='text' name='name_column_title' style='width:300' value='{$msNameColumnTitle}'/>\n"
		."    </td>\n"
		."  </tr>\n"
		."  <tr>\n"
		."    <td class='fieldLabel'>Ordem das Colunas:</td>\n"
		."    <td>\n"
		."      <select name='columns_order' id='columns_order' multiple='multiple' style='height:200;width:300'>\n";
		foreach($maColumns as $msKey=>$msValue){
			echo "<option value='{$msKey}'>{$msValue}</option>\n";
		}
		echo "      </select>\n"
		."      <input type='hidden' id='serialized_columns_order' name='serialized_columns_order'>\n"
		."      <br/>\n"
		."      <br/>\n"
		."      <input type='button' class='button' value='Mover Para Cima' onclick='gobi(\"columns_order\").moveUp()'>\n"
		."      &nbsp;\n"
		."      <input type='button' class='button' value='Mover Para Baixo' onclick='gobi(\"columns_order\").moveDown()'>\n"
		."    </td>\n"
		."  </tr>\n"
		."</table>\n";
		?>
<script type="text/javascript">
        
        gebi('columns_order').object = new FWDSelect('columns_order');
        
        document.getElementsByTagName('form')[0].onsubmit = function(){
          gebi('serialized_columns_order').value = gobi('columns_order').getKeys().join(':');
        };
        
        gebi('show_status_column').onchange = function(){
          if(this.checked){
            gobi('columns_order').addItem('status','Status');
          }else{
            gobi('columns_order').removeItem('status');
          }
        }
        
      </script>
		<?
	}

}

?>