<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDStateReportBasics.
 *
 * <p>Classe que define o estado de defini��o de atributos b�sicos de um relat�rio.</p>
 * @package Rails
 * @subpackage states
 */
class FWDStateReportBasics extends FWDWizardState {

	public function __construct(IReportBuilder $poStateHandler){
		parent::__construct($poStateHandler);
	}

	public function loadPostData(){
		$this->coHandler->setName($_POST['name']);
		$this->coHandler->setTitle($_POST['title']);
		$this->coHandler->setPackage($_POST['package']);
		$this->coHandler->setDescription($_POST['description']);
		$this->coHandler->setSelect($_POST['select']);
		$this->coHandler->setWhere($_POST['where']);
		$this->coHandler->setOrderBy($_POST['order_by']);
	}

	public function draw(){
		$msName = $this->coHandler->getName();
		$msTitle = $this->coHandler->getTitle();
		$msSelectedValue = $this->coHandler->getPackageAcronym();
		$maPackages = FWDWizardLib::getInstance()->getPackages();
		$msDescription = $this->coHandler->getDescription();
		$msSelect = $this->coHandler->getSelect();
		$msWhere = $this->coHandler->getWhere();
		$msOrderBy = $this->coHandler->getOrderBy();

		echo "<table style='width:90%'>\n"
		."  <tr>\n"
		."    <td class='fieldLabel'>Nome:</td>\n"
		."    <td>\n"
		."      <input type='text' name='name' value='{$msName}' style='width:95%'/>\n"
		."    </td>\n"
		."    <td>(Ex: 'risks by area')</td>\n"
		."  </tr>\n"
		."  <tr>\n"
		."    <td class='fieldLabel'>T�tulo:</td>\n"
		."    <td>\n"
		."      <input type='text' name='title' value='{$msTitle}' style='width:95%'/>\n"
		."    </td>\n"
		."    <td>(Ex: 'Riscos por �rea')</td>\n"
		."  </tr>\n"
		."  <tr>\n"
		."    <td class='fieldLabel'>Pacote:</td>\n"
		."    <td>\n"
		."      <select name='package' style='width:95%'>\n"
		."        <option value=''></option>\n";
		foreach($maPackages as $maPackageInfo){
			$msValue = $maPackageInfo['acronym'];
			$msText = $maPackageInfo['acronym'].' - '.$maPackageInfo['name'];
			$msSelected = ($msValue==$msSelectedValue?' selected="selected"':'');
			echo "<option value='{$msValue}'{$msSelected}>{$msText}</option>\n";
		}
		echo "      </select>\n"
		."    </td>\n"
		."    <td></td>\n"
		."  </tr>\n"
		."  <tr>\n"
		."    <td class='fieldLabel'>Descri��o:</td>\n"
		."    <td>\n"
		."      <textarea name='description' rows='3' style='width:95%'>{$msDescription}</textarea>\n"
		."    </td>\n"
		."    <td>(coment�rio)</td>\n"
		."  </tr>\n"
		."  <tr>\n"
		."    <td class='fieldLabel'>Select:</td>\n"
		."    <td>\n"
		."      <textarea id='select' name='select' rows='10' style='width:95%'>{$msSelect}</textarea>\n"
		."    </td>\n"
		."    <td>(sem where e order by)</td>\n"
		."  </tr>\n"
		."  <tr>\n"
		."    <td class='fieldLabel'>Where:</td>\n"
		."    <td>\n"
		."      <textarea id='where' name='where' rows='3' style='width:95%'>{$msWhere}</textarea>\n"
		."    </td>\n"
		."    <td>(sem a palavra 'WHERE')</td>\n"
		."  </tr>\n"
		."  <tr>\n"
		."    <td class='fieldLabel'>Order By:</td>\n"
		."    <td>\n"
		."      <textarea id='order_by' name='order_by' rows='3' style='width:95%'>{$msOrderBy}</textarea>\n"
		."    </td>\n"
		."    <td>(sem as palavras 'ORDER BY')</td>\n"
		."  </tr>\n"
		."</table>\n";
	}

}

?>