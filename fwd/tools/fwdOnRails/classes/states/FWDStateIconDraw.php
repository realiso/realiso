<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDStateIconDraw.
 *
 * <p>Classe que define o estado de desenho de �cone.</p>
 * @package Rails
 * @subpackage states
 */
class FWDStateIconDraw extends FWDWizardState {

	public function __construct(IImageBuilder $poStateHandler){
		parent::__construct($poStateHandler);
	}

	public function loadPostData(){
		$this->coHandler->setPixels($_POST['pixels']);
	}

	public function draw(){
		$msPixels = $this->coHandler->getPixels();
		$miRows = $this->coHandler->getHeight();
		$miCols = $this->coHandler->getWidth();
		?>
<script
	type="text/javascript" src="lib/js/icon_builder.js"></script>
<style type="text/css">
table.pixels {
	border-spacing: 0px;
	border-collapse: collapse;
	border: solid black 1px;
}

td {
	width: 20px;
	height: 20px;
}

td.q {
	border: solid 1px black;
}
</style>
<table style="width: 100%; border: none;">
	<tr>
		<td align="center">
		<table class="pixels" onmousedown="Canvas.startPainting()">
		<?

		for($i=0;$i<$miRows;$i++){
			echo "<tr>\n";
			for($j=0;$j<$miCols;$j++){
				echo "<td id='{$i}_{$j}' class=q onmousemove='Canvas.paint(this.id)'></td>\n";
			}
			echo "</tr>\n";
		}

		?>
		</table>
		</td>
	</tr>
	<tr>
		<td align="center">
		<table class="pixels">
		<?
		for($r=0;$r<256;$r+=0x33){
			$msR = ($r<16?'0'.dechex($r):dechex($r));
			echo "<tr>";
			for($g=0;$g<256;$g+=0x33){
				$msG = ($g<16?'0'.dechex($g):dechex($g));
				for($b=0;$b<256;$b+=0x33){
					$msB = ($b<16?'0'.dechex($b):dechex($b));
					echo "<td onclick='Canvas.setColor(\"#$msR$msG$msB\")' class=q style='background:#$msR$msG$msB'></td>\n";
				}
			}
			echo "</tr>";
		}
		?>
		</table>
		</td>
	</tr>
</table>
<input type="hidden"
	name="pixels" id="pixels" />
<script type="text/javascript">
        
        document.getElementsByTagName('form')[0].onsubmit = function(){
          document.getElementById('pixels').value = Canvas.getPixels();
        };
        
        window.onmouseup = Canvas.stopPainting;
        Canvas.init(<?=$miRows?>,<?=$miCols?>);
        <?
          if($msPixels){
            echo "Canvas.setPixels('{$msPixels}');";
          }
        ?>
        
      </script>
        <?
	}

}

?>