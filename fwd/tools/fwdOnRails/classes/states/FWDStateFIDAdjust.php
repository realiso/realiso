<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDStateFIDAdjust.
 *
 * <p>Classe que define o estado de ajuste de tela pela FID.</p>
 * @package Rails
 * @subpackage states
 */
class FWDStateFIDAdjust extends FWDWizardState {

	protected $cbShowToolbar;

	public function __construct(IXMLBuilder $poStateHandler, $pbShowToolbar = false){
		parent::__construct($poStateHandler);
		$this->cbShowToolbar = $pbShowToolbar;
	}

	public function setShowToolbar($pbShowToolbar){
		$this->cbShowToolbar = $pbShowToolbar;
	}

	public function loadPostData(){
		$this->coHandler->setXml($_POST['xmlContent']);
	}

	public function draw(){
		$_SESSION['xmlContent'] = $this->coHandler->getXml();
		$_SESSION['showToolbar'] = $this->cbShowToolbar;
		?>
<input
	type="hidden" name="xmlContent" id="xmlContent" />
<script type="text/javascript">
        document.getElementsByTagName('form')[0].onsubmit = function(){
          var msXmlContent = gebi('fid').contentWindow.fid.getXMLCode();
          gebi('xmlContent').value = msXmlContent;
        };
      </script>
<iframe
	name="fid" id="fid" src="../../src/fid/fid.php?action=none"
	style="width: 100%; height: 550;"></iframe>
		<?
	}

}

?>