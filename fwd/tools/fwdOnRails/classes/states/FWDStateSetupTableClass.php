<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

include_once $handlers_ref."QueryTables.php";

/**
 * Classe FWDStateSetupTableClass.
 *
 * <p>Classe que define o estado de setup de classe de tabela.</p>
 * @package Rails
 * @subpackage states
 */
class FWDStateSetupTableClass extends FWDWizardState {

	public function __construct(ITableClassBuilder $poStateHandler){
		parent::__construct($poStateHandler);
	}

	public function loadPostData(){
		$this->coHandler->setTableName($_POST['table_name']);
		$this->coHandler->setClassName($_POST['class_name']);
		$this->coHandler->setLabel($_POST['label']);
		$this->coHandler->setPluralLabel($_POST['plural_label']);
	}

	public function draw(){
		$msSelectedTableName = $this->coHandler->getTableName();
		$msClassName = $this->coHandler->getClassName();
		$msLabel = $this->coHandler->getLabel();
		$msPluralLabel = $this->coHandler->getPluralLabel();
		?>
<script type="text/javascript">
        function getClassName(psTableName){
          var maWords = psTableName.split('_');
          var msClassName = maWords[0].toUpperCase();
          for(var i=1;i<maWords.length;i++){
            msClassName+= maWords[i].substr(0,1).toUpperCase() + maWords[i].substr(1);
          }
          return msClassName;
        }
      </script>
<table>
	<tr>
		<td class="fieldLabel">Selecione a tabela:</td>
		<td><select name="table_name"
			onChange="gebi('class_name').value=getClassName(this.value)">
			<?
			$moQuery = new QueryTables();
			$maTableNames = $moQuery->getTableNames();
			foreach($maTableNames as $msTableName){
				$msSelected = ($msTableName==$msSelectedTableName?' selected="selected"':'');
				echo "<option value='$msTableName'$msSelected>$msTableName</option>";
			}
			?>
		</select></td>
	</tr>
	<tr>
		<td class="fieldLabel">Nome da classe:</td>
		<td><input type="text" name="class_name" id="class_name"
			value="<?=$msClassName?>" /></td>
	</tr>
	<tr>
		<td class="fieldLabel">Label:</td>
		<td><input type="text" name="label" value="<?=$msLabel?>" /> (em
		portuguÍs)</td>
	</tr>
	<tr>
		<td class="fieldLabel">Label no plural:</td>
		<td><input type="text" name="plural_label" value="<?=$msPluralLabel?>" />
		(em portuguÍs)</td>
	</tr>
</table>
			<?
	}

}

?>