<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

include_once $handlers_ref."QueryColumns.php";

/**
 * Classe FWDQueryWizard.
 *
 * <p></p>
 * @package Rails
 * @subpackage classes
 */
class FWDQueryWizard extends FWDWizard implements ITableQueryBuilder {

	protected $csOutputDir = 'output/';
	protected $csQueryType = 'table';
	protected $csTableName = '';
	protected $csQuery = '';
	protected $coQueryBuilder = null;

	protected function __construct(){
		parent::__construct();
		$this->coQueryBuilder = new FWDQueryBuilder();
		$this->addState('basics'           ,'Par�metros B�sicos',new FWDStateClassBasics($this->coQueryBuilder),false,true);
		$this->addState('set_query'        ,'Sele��o de Tabela' ,new FWDStateSetQuery($this)                   );
		$this->addState('edit_table_fields','Edi��o de Campos'  ,new FWDStateEditFields($this->coQueryBuilder) );
		$this->addState('edit_query_fields','Edi��o de Campos'  ,new FWDStateEditFields($this->coQueryBuilder) );
		$this->addState('edit_filters'     ,'Edi��o de Filtros' ,new FWDStateEditFilters($this->coQueryBuilder));
		$this->addState('end'              ,'Concluir'          ,new FWDStateConfirmFinish()                   ,true);
	}

	public static function getInstance(){
		if(self::$coInstance===null){
			$moSession = FWDWebLib::getInstance()->getSession(new FWDWizardSession('FWDWizard'));
			$moInstance = $moSession->getAttrWizardObject();
			if(!$moInstance){
				$msClass = get_class(new self());
				$moInstance = new $msClass;
				$moSession->setAttrWizardObject($moInstance);
			}
			self::$coInstance = $moInstance;
		}
		return self::$coInstance;
	}

	protected function getName(){
		return "Query Wizard";
	}

	public function getQueryType(){
		return $this->csQueryType;
	}

	public function setQueryType($psQueryType){
		$this->csQueryType = $psQueryType;
	}

	public function getTableName(){
		return $this->csTableName;
	}

	public function setTableName($psTableName){
		$this->csTableName = $psTableName;
	}

	public function getQuery(){
		return $this->csQuery;
	}

	public function setQuery($psQuery){
		$this->csQuery = $psQuery;
	}

	protected function getNextState(){
		switch($this->csCurrentState){
			case 'basics':            return 'set_query';
			case 'set_query':         return ($this->csQueryType=='table'?'edit_table_fields':'edit_query_fields');
			case 'edit_table_fields': return 'edit_filters';
			case 'edit_query_fields': return 'edit_filters';
			case 'edit_filters':      return 'end';
		}
	}

	protected function loadPostData(){
		parent::loadPostData();
		if($this->csCurrentState=='set_query'){
			$this->coQueryBuilder->setTableName($this->csTableName);
			$this->coQueryBuilder->setup();
			$this->coQueryBuilder->setQuery($this->csQuery);

			$maTypeOptions = array(
        'DB_UNKNOWN'  => 'DB_UNKNOWN',
        'DB_NUMBER'   => 'DB_NUMBER',
        'DB_STRING'   => 'DB_STRING',
        'DB_DATETIME' => 'DB_DATETIME',
			);
			if($this->csQueryType=='table'){
				$this->coQueryBuilder->addProperty('included',''     ,'checkbox');
				$this->coQueryBuilder->addProperty('name'    ,'Campo','static'  );
				$this->coQueryBuilder->addProperty('alias'   ,'Alias','text'    );
				$this->coQueryBuilder->addProperty('type'    ,'Tipo' ,'select'  ,$maTypeOptions);

				$moQuery = new QueryColumns();
				$moQuery->setTable($this->coQueryBuilder->getTableName());
				$maColumns = $moQuery->getColumns();
				foreach($maColumns as $maColumnInfo){
					$maColumnInfo['included'] = true;
					$this->coQueryBuilder->addField($maColumnInfo['name'],$maColumnInfo);
				}
			}else{
				$this->coQueryBuilder->addProperty('name'    ,'Campo','static'  );
				$this->coQueryBuilder->addProperty('alias'   ,'Alias','static'  );
				$this->coQueryBuilder->addProperty('type'    ,'Tipo' ,'select'  ,$maTypeOptions);
			}
		}
	}

	protected function finish(){
		FWDWizardLib::createFile("{$this->csOutputDir}{$this->coQueryBuilder->getName()}.php",$this->coQueryBuilder->getPhp());
	}

	protected function getAgainButtonText(){
		return "Criar outro query handler";
	}

}

?>