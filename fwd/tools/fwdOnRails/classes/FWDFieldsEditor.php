<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDFieldsEditor.
 *
 * <p></p>
 * @package Rails
 * @subpackage classes
 */
class FWDFieldsEditor implements IFieldsEditor {

	protected $caFields;
	protected $caProperties;
	protected $caFieldPropertyOptions;

	public function __construct(){
		$this->setup();
	}

	public function setup(){
		$this->caFields = array();
		$this->caProperties = array();
		$this->caFieldPropertyOptions = array();
	}

	public function addProperty($psPropertyName,$psPropertyLabel,$psPropertyType,$paOptions=null){
		$this->caProperties[$psPropertyName] = array(
      'label' => $psPropertyLabel,
      'type' => $psPropertyType,
		);
		if($psPropertyType=='select'){
			$this->caProperties[$psPropertyName]['options'] = ($paOptions?$paOptions:array());
			$this->caFieldPropertyOptions[$psPropertyName] = array();
		}
	}

	public function addField($psFieldName,$paProperties=null){
		$this->caFields[$psFieldName] = $paProperties;
	}

	public function removeField($psFieldName){
		unset($this->caFields[$psFieldName]);
	}

	public function getProperties(){
		return array_keys($this->caProperties);
	}

	public function getFields(){
		return array_keys($this->caFields);
	}

	public function getPropertyLabel($psPropertyName){
		return $this->caProperties[$psPropertyName]['label'];
	}

	public function getPropertyType($psPropertyName){
		return $this->caProperties[$psPropertyName]['type'];
	}

	public function setFieldPropertyOptions($psFieldName,$psPropertyName,$paOptions){
		if($this->getPropertyType($psPropertyName)=='select'){
			$this->caFieldPropertyOptions[$psPropertyName][$psFieldName] = $paOptions;
		}else{
			trigger_error("The method 'setFieldPropertyOptions' only applies to select properties.",E_USER_WARNING);
		}
	}

	public function getFieldPropertyOptions($psFieldName,$psPropertyName){
		if($this->getPropertyType($psPropertyName)=='select'){
			if(isset($this->caFieldPropertyOptions[$psPropertyName][$psFieldName])){
				return $this->caFieldPropertyOptions[$psPropertyName][$psFieldName];
			}else{
				return $this->caProperties[$psPropertyName]['options'];
			}
		}else{
			trigger_error("The method 'getFieldPropertyOptions' only applies to select properties.",E_USER_WARNING);
		}
	}

	public function setFieldProperty($psFieldName,$psProperty,$pmValue){
		$this->caFields[$psFieldName][$psProperty] = $pmValue;
	}

	public function getFieldProperty($psFieldName,$psProperty){
		return $this->caFields[$psFieldName][$psProperty];
	}

	public function getSelectedField($psPropertyName){
		if($this->getPropertyType($psPropertyName)=='radiobox'){
			foreach($this->caFields as $msFieldName=>$_){
				if($this->caFields[$msFieldName][$psPropertyName]){
					return $msFieldName;
				}
			}
			return '';
		}else{
			trigger_error("The method 'getSelectedField' only applies to radiobox properties.",E_USER_WARNING);
		}
	}

}

?>