
<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDPopUpSearchBuilder.
 *
 * <p></p>
 * @package Rails
 * @subpackage classes
 */
class FWDPopUpSearchBuilder {

	protected $csTableName;
	protected $csClass;
	protected $csLabel;
	protected $csPluralLabel;
	protected $csAliasId;
	protected $csNameField;
	protected $csNameFieldAlias;
	protected $csLowerCaseEntityName;
	protected $csTitleCaseEntityName;

	public function __construct($psTableName,$psClass,$psLabel,$psAliasId,$psNameField,$psNameFieldAlias,$psPluralLabel){
		$this->csTableName = $psTableName;
		$this->csClass = $psClass;
		$this->csLabel = $psLabel;
		$this->csAliasId = $psAliasId;
		$this->csNameField = $psNameField;
		$this->csNameFieldAlias = $psNameFieldAlias;
		$this->csPluralLabel = $psPluralLabel;
		$this->csLowerCaseEntityName = FWDWizardLib::getLowerCaseEntityName($this->csTableName);
		$this->csTitleCaseEntityName = FWDWizardLib::getTitleCaseEntityName($this->csTableName);
	}

	public function getQueryName(){
		return "QueryGrid{$this->csTitleCaseEntityName}Search";
	}

	public function getSingleSearchPhp(){
		$msQueryClassName = $this->getQueryName();
		$msPopUpName = "popup_{$this->csLowerCaseEntityName}_search";
		$msIconName = FWDWizardLib::getIconName($this->csTableName);
		$msGridHandlersPath = FWDWizardLib::getInstance()->getGridHandlersRef(FWDWizardLib::getPrefix($this->csTableName));
		return "<?php\n"
		."\n"
		."include_once 'include.php';\n"
		."include_once \$handlers_ref . '{$msGridHandlersPath}{$msQueryClassName}.php';\n"
		."include_once \$classes_isms_ref . 'nonauto/ISMSContextDrawGrid.php';\n"
		."\n"
		."class SearchEvent extends FWDRunnable {\n"
		."  public function run(){\n"
		."    \$moGrid = FWDWebLib::getObject('grid_search');\n"
		."    \$moGrid->execEventPopulate();\n"
		."  }\n"
		."}\n"
		."\n"
		."class ScreenBeforeEvent extends FWDRunnable {\n"
		."  public function run(){\n"
		."    \$moStartEvent = FWDStartEvent::getInstance();\n"
		."    \$moStartEvent->setScreenEvent(new ScreenEvent(''));\n"
		."    \$moStartEvent->addAjaxEvent(new SearchEvent('search_event'));\n"
		."    \n"
		."    \$moHandler = new {$msQueryClassName}();\n"
		."    \$moHandler->setName(FWDWebLib::getObject('var_name')->getValue());\n"
		."    \n"
		."    \$moGrid = FWDWebLib::getObject('grid_search');\n"
		."    \$moGrid->setQueryHandler(\$moHandler);\n"
		."    \$moGrid->setObjFwdDrawGrid(new ISMSContextDrawGrid('{$msIconName}'));\n"
		."  }\n"
		."}\n"
		."\n"
		."class ScreenEvent extends FWDRunnable {\n"
		."  public function run(){ \n"
		."    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));\n"
		."  }\n"
		."}\n"
		."\n"
		."FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));\n"
		."FWDWebLib::getInstance()->xml_load('{$msPopUpName}.xml');\n"
		."\n"
		."?>";
	}

	public function getSingleSearchXml(){
		$msSetFunction = "set_{$this->csLowerCaseEntityName}";
		return "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n"
		."<dialog name=\"dialog\" focus=\"name\">\n"
		."  <box top=\"0\" left=\"0\" height=\"415\" width=\"635\"/>\n"
		."  <variable name=\"var_name\"/>\n"
		."  <static class=\"FWDStaticPopupTitle\">\n"
		."    <box top=\"0\" left=\"0\" height=\"20\" width=\"635\"/>\n"
		."    <string id=\"tt_{$this->csLowerCaseEntityName}_search\">Busca de {$this->csLabel}</string>\n"
		."  </static>\n"
		."  <static>\n"
		."    <box top=\"30\" left=\"10\" height=\"20\" width=\"45\"/>\n"
		."    <string id=\"lb_name_cl\">Nome:</string>\n"
		."  </static>\n"
		."  <text name=\"name\" maxlength=\"255\">\n"
		."    <box top=\"30\" left=\"65\" height=\"20\" width=\"460\"/>\n"
		."  </text>\n"
		."  <viewbutton class=\"FWDViewButtonISMS\">\n"
		."    <box top=\"30\" left=\"540\" height=\"20\" width=\"85\"/>\n"
		."    <clientevent event=\"onClick\">\n"
		."      gebi('var_name').value = gebi('name').value;\n"
		."      gobi('grid_search').setPopulate(true);\n"
		."      trigger_event('search_event',3);\n"
		."    </clientevent>\n"
		."    <static horizontal=\"center\">\n"
		."      <box top=\"0\" left=\"0\" height=\"20\" width=\"85\"/>\n"
		."      <string id=\"vb_search\">Pesquisar</string>\n"
		."    </static>\n"
		."  </viewbutton>\n"
		."  <dbgrid name=\"grid_search\" rowheight=\"20\" selectcolumn=\"1\" dinamicfill=\"true\" populate=\"false\" selecttype=\"single\">\n"
		."    <box top=\"60\" left=\"10\" height=\"315\" width=\"615\"/>\n"
		."    <column alias=\"{$this->csAliasId}\">\n"
		."      <box width=\"25\"/>\n"
		."    </column>\n"
		."    <column alias=\"{$this->csNameFieldAlias}\" canorder=\"true\" tooltip=\"true\">\n"
		."      <static marginleft=\"10\" vertical=\"middle\">\n"
		."        <string id=\"gc_name\">Nome</string>\n"
		."      </static>\n"
		."    </column>\n"
		."  </dbgrid>\n"
		."  <viewbutton class=\"FWDViewButtonISMS\">\n"
		."    <box top=\"385\" left=\"470\" height=\"20\" width=\"70\"/>\n"
		."    <clientevent event=\"onClick\">\n"
		."      self.getOpener().{$msSetFunction}(gobi('grid_search').getValue());\n"
		."      self.close();\n"
		."    </clientevent>\n"
		."    <static horizontal=\"center\">\n"
		."      <box top=\"0\" left=\"0\" height=\"20\" width=\"70\"/>\n"
		."      <string id=\"vb_associate\">Associar</string>\n"
		."    </static>\n"
		."  </viewbutton>\n"
		."  <viewbutton class=\"FWDViewButtonISMS\">\n"
		."    <box top=\"385\" left=\"555\" height=\"20\" width=\"70\"/>\n"
		."    <clientevent event=\"onClick\">self.close();</clientevent>\n"
		."    <static horizontal=\"center\">\n"
		."      <box top=\"0\" left=\"0\" height=\"20\" width=\"70\"/>\n"
		."      <string id=\"vb_close\">Fechar</string>\n"
		."    </static>\n"
		."  </viewbutton>\n"
		."</dialog>";
	}

	public function getMultipleSearchPhp(){
		$msQueryClassName = $this->getQueryName();
		$msPopUpName = "popup_{$this->csLowerCaseEntityName}_associate";
		$msIconName = FWDWizardLib::getIconName($this->csTableName);
		$msGridHandlersPath = FWDWizardLib::getInstance()->getGridHandlersRef(FWDWizardLib::getPrefix($this->csTableName));
		return "<?php\n"
		."\n"
		."include_once 'include.php';\n"
		."include_once \$handlers_ref . '{$msGridHandlersPath}{$msQueryClassName}.php';\n"
		."include_once \$classes_isms_ref . 'nonauto/ISMSContextDrawGrid.php';\n"
		."\n"
		."function getCurrentIds(){\n"
		."  \$maValues = array_filter(explode(':', FWDWebLib::getObject('var_current_ids')->getValue()));\n"
		."  if(count(\$maValues)==0) \$maValues = array(0);\n"
		."  return \$maValues;\n"
		."}\n"
		."\n"
		."class SearchEvent extends FWDRunnable {\n"
		."  public function run(){\n"
		."    \$moGrid = FWDWebLib::getObject('grid_search');\n"
		."    \$moGrid->execEventPopulate();\n"
		."  }\n"
		."}\n"
		."\n"
		."class ScreenBeforeEvent extends FWDRunnable {\n"
		."  public function run(){\n"
		."    \$moStartEvent = FWDStartEvent::getInstance();\n"
		."    \$moStartEvent->setScreenEvent(new ScreenEvent(''));\n"
		."    \$moStartEvent->addAjaxEvent(new SearchEvent('search_event'));\n"
		."    \n"
		."    \$maCurrentIds = getCurrentIds();\n"
		."    \n"
		."    \$moGrid = FWDWebLib::getObject('grid_search');\n"
		."    if(FWDWebLib::getObject('var_must_search')->getValue()){\n"
		."      \$moHandler = new {$msQueryClassName}();\n"
		."      \$moHandler->setName(FWDWebLib::getObject('var_name')->getValue());\n"
		."      \$moHandler->setExcludedIds(\$maCurrentIds);\n"
		."      \$moGrid->setQueryHandler(\$moHandler);\n"
		."      \$moGrid->setAttrPopulate('true');\n"
		."      \$moGrid->setObjFwdDrawGrid(new ISMSContextDrawGrid('{$msIconName}'));\n"
		."    }else{\n"
		."      \$moGrid->setAttrPopulate('false');\n"
		."    }\n"
		."    \$moGrid = FWDWebLib::getObject('grid_current');\n"
		."    \$moHandler = new {$msQueryClassName}();\n"
		."    \$moHandler->setIds(\$maCurrentIds);\n"
		."    \$moGrid->setQueryHandler(\$moHandler);\n"
		."    \$moGrid->setObjFwdDrawGrid(new ISMSContextDrawGrid('{$msIconName}'));\n"
		."  }\n"
		."}\n"
		."\n"
		."class ScreenEvent extends FWDRunnable {\n"
		."  public function run(){\n"
		."    FWDWebLib::getObject('var_current_ids')->setValue(FWDWebLib::getObject('current_ids')->getValue());\n"
		."    \n"
		."    \$moGrid = FWDWebLib::getObject('grid_current');\n"
		."    \$moHandler = \$moGrid->getQueryHandler();\n"
		."    \$moHandler->setIds(getCurrentIds());\n"
		."    \$moGrid->setQueryHandler(\$moHandler);\n"
		."    \n"
		."    //instala a seguran�a de ACL na p�gina\n"
		."    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());\n"
		."    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));\n"
		."    ?>\n"
		."      <script language=\"javascript\">\n"
		."        \n"
		."        function search(){\n"
		."          gebi('var_must_search').value = 1;\n"
		."          gebi('var_name').value = gebi('name').value;\n"
		."          gobi('grid_search').setPopulate(true);\n"
		."          trigger_event('search_event',3);\n"
		."        }\n"
		."        \n"
		."        var soContextsAssociator = new ISMSContextsAssociator('grid_search','grid_current','var_current_ids','grids_pack');\n"
		."        \n"
		."      </script>\n"
		."    <?\n"
		."  }\n"
		."}\n"
		."\n"
		."// Testa se a licen�a utilizada permite acessar este m�dulo\n"
		."//ISMSLib::testUserPermissionTo%module%Mode();\n"
		."\n"
		."FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));\n"
		."FWDWebLib::getInstance()->xml_load('{$msPopUpName}.xml');\n"
		."\n"
		."?>";
	}

	public function getMultipleSearchXml(){
		$msPluralEntityName = FWDWizardLib::pluralize($this->csLowerCaseEntityName);
		return "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n"
		."<!DOCTYPE isms [\n"
		."  <!ENTITY gfx \"../../gfx/\">\n"
		."  <!ENTITY icon_insert \"&gfx;bt_iconPag_proximo.gif\"> \n"
		."]>\n"
		."<dialog name=\"dialog\" focus=\"name\">\n"
		."  <box top=\"0\" left=\"0\" height=\"400\" width=\"700\"/>\n"
		."  <getparameter name=\"current_ids\" parameter=\"ids\"/>\n"
		."  <variable name=\"var_must_search\" value=\"0\"/>\n"
		."  <variable name=\"var_current_ids\" value=\"\"/>\n"
		."  <variable name=\"var_name\" value=\"\"/>\n"
		."  <dbgridpack name=\"grids_pack\" elements=\"grid_current:grid_search\"/>\n"
		."  <static class=\"FWDStaticPopupTitle\">\n"
		."    <box top=\"0\" left=\"0\" height=\"20\" width=\"700\"/>\n"
		."    <string id=\"tt_{$this->csLowerCaseEntityName}_association\">Associa��o de {$this->csPluralLabel}</string>\n"
		."  </static>\n"
		."  <static>\n"
		."    <box top=\"30\" left=\"10\" height=\"20\" width=\"70\"/>\n"
		."    <string id=\"st_name\">Nome:</string>\n"
		."  </static>\n"
		."  <text name=\"name\" maxlength=\"255\">\n"
		."    <box top=\"30\" left=\"90\" height=\"20\" width=\"515\"/>\n"
		."    <clientevent event=\"onPressEnter\">search();</clientevent>\n"
		."  </text>\n"
		."  <viewbutton class=\"FWDViewButtonISMS\">\n"
		."    <box top=\"30\" left=\"620\" height=\"20\" width=\"70\"/>\n"
		."    <clientevent event=\"onClick\">search();</clientevent>\n"
		."    <static horizontal=\"center\">\n"
		."      <box top=\"0\" left=\"0\" height=\"20\" width=\"70\"/>\n"
		."      <string id=\"vb_search\">Pesquisar</string>\n"
		."    </static>\n"
		."  </viewbutton>\n"
		."  <dbgrid name=\"grid_search\" rowheight=\"20\" selectcolumn=\"1\" dinamicfill=\"true\" selecttype=\"multiple\" populate=\"false\">\n"
		."    <box top=\"60\" left=\"10\" height=\"260\" width=\"430\"/>\n"
		."    <column alias=\"{$this->csAliasId}\" edit=\"false\">\n"
		."      <box width=\"25\"/>\n"
		."    </column>\n"
		."    <column alias=\"{$this->csNameFieldAlias}\" tooltip=\"true\">\n"
		."      <static marginleft=\"10\" vertical=\"middle\">\n"
		."        <string id=\"gc_name\">Nome</string>\n"
		."      </static>\n"
		."    </column>\n"
		."  </dbgrid>\n"
		."  <dbgrid name=\"grid_current\" rowheight=\"20\" selecttype=\"multiple\" selectcolumn=\"1\">\n"
		."    <box top=\"60\" left=\"480\" height=\"260\" width=\"212\"/>\n"
		."    <viewgroup>\n"
		."      <box left=\"7\" top=\"0\" width=\"212\"/>\n"
		."      <static>\n"
		."        <box top=\"2\" left=\"0\" height=\"16\" width=\"212\"/>\n"
		."        <string id=\"tt_current_{$msPluralEntityName}_bl\"><b>{$this->csPluralLabel} Atuais</b></string>\n"
		."      </static>\n"
		."    </viewgroup>\n"
		."    <column alias=\"{$this->csAliasId}\" edit=\"false\">\n"
		."      <box width=\"25\"/>\n"
		."    </column>\n"
		."    <column alias=\"{$this->csNameFieldAlias}\" tooltip=\"true\">\n"
		."      <static marginleft=\"10\" vertical=\"middle\">\n"
		."        <string id=\"gc_name\">Nome</string>\n"
		."      </static>\n"
		."    </column>\n"
		."  </dbgrid>\n"
		."  <icon name=\"insert_button\" src=\"&icon_insert;\">\n"
		."    <box top=\"180\" left=\"455\" height=\"12\" width=\"8\"/>\n"
		."    <clientevent event=\"onMouseOver\" function=\"changecursor\" elements=\"insert_button\" parameters=\"pointer\"/>\n"
		."    <clientevent event=\"onClick\">soContextsAssociator.addSelected();</clientevent>\n"
		."  </icon>\n"
		."  <viewbutton class=\"FWDViewButtonISMS\">\n"
		."    <box top=\"330\" left=\"480\" height=\"20\" width=\"75\"/>\n"
		."    <clientevent event=\"onClick\">soContextsAssociator.removeSelected()</clientevent>\n"
		."    <static horizontal=\"center\">\n"
		."      <box top=\"0\" left=\"0\" height=\"20\" width=\"75\"/>\n"
		."      <string id=\"vb_remove\">Remover</string>\n"
		."    </static>\n"
		."  </viewbutton>\n"
		."  <horizontalrule>\n"
		."    <box top=\"355\" left=\"10\" height=\"1\" width=\"680\"/>\n"
		."  </horizontalrule>\n"
		."  <viewbutton class=\"FWDViewButtonISMS\">\n"
		."    <box top=\"370\" left=\"535\" height=\"20\" width=\"70\"/>\n"
		."    <clientevent event=\"onClick\">\n"
		."      self.getOpener().set_{$msPluralEntityName}(gebi('var_current_ids').value);\n"
		."      self.close();\n"
		."    </clientevent>\n"
		."    <static horizontal=\"center\">\n"
		."      <box top=\"0\" left=\"0\" height=\"20\" width=\"70\"/>\n"
		."      <string id=\"vb_associate\">Associar</string>\n"
		."    </static>\n"
		."  </viewbutton>\n"
		."  <viewbutton class=\"FWDViewButtonISMS\">\n"
		."    <box top=\"370\" left=\"620\" height=\"20\" width=\"70\"/>\n"
		."    <clientevent event=\"onClick\">self.close();</clientevent>\n"
		."    <static horizontal=\"center\">\n"
		."      <box top=\"0\" left=\"0\" height=\"20\" width=\"70\"/>\n"
		."      <string id=\"vb_close\">Fechar</string>\n"
		."    </static>\n"
		."  </viewbutton>\n"
		."</dialog>";
	}

	public function getQueryGrid(){
		$moQueryBuilder = new FWDQueryBuilder($this->csTableName,$this->csClass,$this->csLabel);
		$moQueryBuilder->setPackage('ISMS');
		$moQueryBuilder->setName($this->getQueryName());
		$moQueryBuilder->setDescription('Consulta para popular a grid de busca.');

		$msFieldName = 'fkcontext';
		$moQueryBuilder->addField($msFieldName);
		$moQueryBuilder->setFieldProperty($msFieldName,'name',$msFieldName);
		$moQueryBuilder->setFieldProperty($msFieldName,'alias',$this->csAliasId);
		$moQueryBuilder->setFieldProperty($msFieldName,'type','DB_NUMBER');

		$msFieldName = $this->csNameField;
		$moQueryBuilder->setFieldProperty($msFieldName,'name',$msFieldName);
		$moQueryBuilder->setFieldProperty($msFieldName,'alias',$this->csNameFieldAlias);
		$moQueryBuilder->setFieldProperty($msFieldName,'type','DB_STRING');

		$moQueryBuilder->addFilter($this->csAliasId,'IN','Ids');
		$moQueryBuilder->addFilter($this->csAliasId,'NOT IN','ExcludedIds');
		$moQueryBuilder->addFilter($this->csNameFieldAlias,'LIKE','Name');

		return $moQueryBuilder->getPhp();
	}

}

?>