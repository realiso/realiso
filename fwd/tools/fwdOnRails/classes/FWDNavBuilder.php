<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDNavBuilder.
 *
 * <p></p>
 * @package Rails
 * @subpackage classes
 */
class FWDNavBuilder extends FWDFieldsEditor implements IContextNavBuilder {

	protected $csTableName;
	protected $csClassName;
	protected $csLabel;
	protected $csAliasId;
	protected $csNameFieldAlias;
	protected $csLowerCaseEntityName;
	protected $csTitleCaseEntityName;

	protected $cbForeignKeys;
	protected $caForeignKeyFields;
	protected $caRegularFields;

	protected $csNameColumnTitle = 'Nome';
	protected $cbShowLinkPopupVisualize = true;
	protected $cbShowTooltipCreateModify = true;
	protected $cbShowStatusColumn = true;
	protected $caColumnsOrder = null;

	protected $caAssociations;

	public function __construct(){
		$this->csTableName = '';
		$this->csClassName = '';
		$this->csLabel = '';
		$this->csAliasId = '';
		$this->csNameFieldAlias = '';
		$this->setup();
	}

	public function setup(){
		parent::setup();
		$this->cbForeignKeys = false;
		$this->caForeignKeyFields = array();
		$this->caRegularFields = array();
		$this->caAssociations = array();

		$maDrawTypeOptions = array(
      'TYPE_STATE'          => 'TYPE_STATE',
      'TYPE_DATE'           => 'TYPE_DATE',
      'TYPE_SHORT_DATE'     => 'TYPE_SHORT_DATE',
      'TYPE_CLASSIFICATION' => 'TYPE_CLASSIFICATION'
      );

      $this->addProperty('include'    ,''              ,'checkbox');
      $this->addProperty('name'       ,'Campo'         ,'static'  );
      $this->addProperty('title'      ,'T�tulo'        ,'text'    );
      $this->addProperty('draw_type'  ,'Tipo'          ,'select'  ,array(''=>''));
      $this->addProperty('name_column','Coluna de Nome','select'  );
      $this->addProperty('display'    ,'Display'       ,'checkbox');
      $this->addProperty('canorder'   ,'CanOrder'      ,'checkbox');
      $this->addProperty('tooltip'    ,'ToolTip'       ,'checkbox');
	}

	public function setTableName($psTableName){
		$this->csTableName = $psTableName;
		$maEntityName = explode('_',$psTableName);
		unset($maEntityName[0]);
		$this->csLowerCaseEntityName = implode('_',$maEntityName);
		$this->csTitleCaseEntityName = implode('',array_map('ucfirst',$maEntityName));

		// Constr�i os arrays de options para cada tabela referenciada por uma FK
		$this->caTableStringFields = array();
		$this->caTableNameFields = array();
		$this->caReferencedTables = array();

		$moQueryForeignKeys = new QueryForeignKeys();
		$moQueryForeignKeys->setSourceTable($this->csTableName);
		$moQueryForeignKeys->makeQuery();
		$moQueryForeignKeys->executeQuery();

		while($moQueryForeignKeys->fetch()){
			$msFieldName = $moQueryForeignKeys->getFieldValue('field');
			$msReferencedTable = $moQueryForeignKeys->getFieldValue('referenced_table');
			$msReferencedField = $moQueryForeignKeys->getFieldValue('referenced_field');
			$this->caReferencedTables[$msFieldName] = $msReferencedTable;

			$this->setFieldProperty($msFieldName,'referenced_table',$msReferencedTable);
			$this->setFieldProperty($msFieldName,'referenced_field',$msReferencedField);

			if(!isset($this->caTableStringFields[$msReferencedTable])){
				$moQueryStringFields = new QueryStringFields();
				$moQueryStringFields->setTableName($msReferencedTable);
				$maStringFields = $moQueryStringFields->getStringFields();
				if(count($maStringFields)){
					$this->caTableStringFields[$msReferencedTable] = array_combine($maStringFields,$maStringFields);
					if(in_array('sname',$maStringFields)){
						$this->caTableNameFields[$msReferencedTable] = 'sname';
					}elseif(in_array('tdescription',$maStringFields)){
						$this->caTableNameFields[$msReferencedTable] = 'tdescription';
					}else{
						$this->caTableNameFields[$msReferencedTable] = $maStringFields[0];
					}
				}else{
					$this->caTableStringFields[$msReferencedTable] = array();
					$this->caTableNameFields[$msReferencedTable] = '';
				}
			}
		}

	}

	public function setClassName($psClassName){
		$this->csClassName = $psClassName;
	}

	public function setLabel($psLabel){
		$this->csLabel = $psLabel;
	}

	public function setAliasId($psAliasId){
		$this->csAliasId = $psAliasId;
	}

	public function setNameFieldAlias($psNameFieldAlias){
		$this->csNameFieldAlias = $psNameFieldAlias;
	}

	public function addField($psFieldName,$paProperties=null){
		parent::addField($psFieldName,$paProperties);
		$msFieldPrefix = FWDWizardLib::getFieldPrefix($psFieldName);
		if($msFieldPrefix=='fk'){
			if($paProperties['alias']!=$this->csAliasId){
				$this->caForeignKeyFields[] = $psFieldName;
			}
		}elseif($paProperties['alias']!=$this->csNameFieldAlias){
			$this->caRegularFields[] = $psFieldName;
		}

		switch($msFieldPrefix){
			case 'fk':{
				$msReferencedTable = $this->caReferencedTables[$psFieldName];
				$this->setFieldProperty($psFieldName,'name_column',$this->caTableNameFields[$msReferencedTable]);
				$this->setFieldPropertyOptions($psFieldName,'name_column',$this->caTableStringFields[$msReferencedTable]);
				$this->setFieldProperty($psFieldName,'tooltip',true);
				break;
			}
			case 't':{
				$this->setFieldProperty($psFieldName,'include',false);
				$this->setFieldProperty($psFieldName,'tooltip',true);
				break;
			}
			case 's':{
				$this->setFieldProperty($psFieldName,'tooltip',true);
				break;
			}
			case 'd':{
				$maDrawTypeOptionsDate = array(
          'DATE' => 'DATE',
          'SHORT_DATE' => 'SHORT_DATE'
          );
          $this->setFieldPropertyOptions($psFieldName,'draw_type',$maDrawTypeOptionsDate);
          $this->setFieldProperty($psFieldName,'draw_type','DATE');
          break;
			}
			case 'b':{
				$this->setFieldProperty($psFieldName,'display',false);
				break;
			}
		}
	}

	public function getFields(){
		if($this->cbForeignKeys){
			return $this->caForeignKeyFields;
		}else{
			return $this->caRegularFields;
		}
	}

	protected function getIncludedFields(){
		$maFields = parent::getFields();
		$maReturn = array();
		foreach($maFields as $miKey=>$msFieldName){
			if($this->getFieldProperty($msFieldName,'include')){
				$maReturn[] = $msFieldName;
			}
		}
		return $maReturn;
	}

	public function getProperties(){
		if($this->cbForeignKeys){
			return array_diff(parent::getProperties(),array('draw_type'));
		}else{
			return array_diff(parent::getProperties(),array('name_column'));
		}
	}

	public function setForeignKeys($pbForeignKeys){
		$this->cbForeignKeys = $pbForeignKeys;
	}

	public function getNameColumnTitle(){
		return $this->csNameColumnTitle;
	}

	public function setNameColumnTitle($psNameColumnTitle){
		$this->csNameColumnTitle = $psNameColumnTitle;
	}

	public function getShowLinkPopupVisualize(){
		return $this->cbShowLinkPopupVisualize;
	}

	public function setShowLinkPopupVisualize($pbShowLinkPopupVisualize){
		$this->cbShowLinkPopupVisualize = $pbShowLinkPopupVisualize;
	}

	public function getShowTooltipCreateModify(){
		return $this->cbShowTooltipCreateModify;
	}

	public function setShowTooltipCreateModify($pbShowTooltipCreateModify){
		$this->cbShowTooltipCreateModify = $pbShowTooltipCreateModify;
	}

	public function getShowStatusColumn(){
		return $this->cbShowStatusColumn;
	}

	public function setShowStatusColumn($pbShowStatusColumn){
		$this->cbShowStatusColumn = $pbShowStatusColumn;
	}

	public function setColumnsOrder($paOrderedColumns){
		$this->caColumnsOrder = $paOrderedColumns;
	}

	public function getColumns(){
		if($this->caColumnsOrder){
			$maFields = array_diff($this->caColumnsOrder,array('status'));
		}else{
			$maFields = array_merge($this->caForeignKeyFields,$this->caRegularFields);
		}
		$maColumns = array();
		foreach($maFields as $msFieldName){
			if($this->getFieldProperty($msFieldName,'include')){
				$maColumns[$msFieldName] = $this->getFieldProperty($msFieldName,'title');
			}
		}
		if($this->cbShowStatusColumn){
			$maColumns['status'] = 'Status';
		}
		return $maColumns;
	}

	public function addAssociation($psRelationTable, $psRelatedTable, $psPluralLabel){
		$this->caAssociations[$psRelationTable] = array(
      'relation_table' => $psRelationTable,
      'related_table'  => $psRelatedTable,
      'plural_label'   => $psPluralLabel
		);
	}

	private function stripOffPrefix($psAlias){
		$miPrefixLength = strlen($this->csLowerCaseEntityName)+1;
		if(substr($psAlias,0,$miPrefixLength)=="{$this->csLowerCaseEntityName}_"){
			return substr($psAlias,$miPrefixLength);
		}else{
			return $psAlias;
		}
	}

	public function getQueryName(){
		return "QueryGrid".FWDWizardLib::getTitleCaseEntityName($this->csTableName);
	}

	public function getXml(){
		$msVariableId = "selected_{$this->csAliasId}";
		$msScreensPath = FWDWizardLib::getInstance()->getScreensRef(FWDWizardLib::getPrefix($this->csTableName));

		$msXml = "<?xml version='1.0' encoding='ISO-8859-1'?>\n"
		."<dialog name='dialog'>\n"
		."  <box top='0' left='0' height='441' width='976'/>\n"
		."  <variable name='{$msVariableId}'/>\n";
		if(count($this->caAssociations)>0){
			$msXml.= "  <variable name='var_associated_ids'/>\n";
		}
		$msXml.= "  <dbgrid name='grid_{$this->csLowerCaseEntityName}' rowheight='20' selectcolumn='1' order='2' dinamicfill='true'>\n"
		."    <box top='10' left='10' height='390' width='956'/>\n"
		."    <viewgroup>\n"
		."      <box width='900'/>\n"
		."      <static marginleft='10'>\n"
		."        <box width='600' height='22'/>\n"
		."        <string id='tt_{$this->csLowerCaseEntityName}_bl'>\n"
		."          <b>{$this->csLabel}</b>\n"
		."        </string>\n"
		."      </static>\n"
		."    </viewgroup>\n"
		."    <menu name='menu' elements='lines'>\n"
		."      <box width='160'/>\n"
		."      <menuitem tag='edit'>\n"
		."        <icon src='../../gfx/icon-edit.gif'/>\n"
		."        <string id='mi_edit'>Editar</string>\n"
		."        <clientevent event='onClick'>\n"
		."          isms_open_popup('popup_{$this->csLowerCaseEntityName}_edit','{$msScreensPath}popup_{$this->csLowerCaseEntityName}_edit.php?{$this->csLowerCaseEntityName}='+FWDMenu.value,'','false');\n"
		."        </clientevent>\n"
		."      </menuitem>\n"
		."      <menuitem tag='delete'>\n"
		."        <icon src='../../gfx/icon-delete.gif'/>\n"
		."        <string id='mi_delete'>Remover</string>\n"
		."        <clientevent event='onClick'>gebi('$msVariableId').value = FWDMenu.value;</clientevent>\n"
		."        <serverevent event='onClick' function='confirm_remove'/>\n"
		."      </menuitem>\n";
		if(count($this->caAssociations)>0){
			$msXml.= "      <menuseparator height='3'/>\n";
			foreach($this->caAssociations as $maAssociation){
				$msRelatedTable = $maAssociation['related_table'];
				$msPluralLabel = $maAssociation['plural_label'];
				$msRelatedLCEntity = FWDWizardLib::getLowerCaseEntityName($msRelatedTable);
				$msRelatedPluralLCEntity = FWDWizardLib::pluralize($msRelatedLCEntity);
				$msRelatedIcon = FWDWizardLib::getIconName($msRelatedTable);
				$msXml.= "      <menuitem tag='{$msRelatedPluralLCEntity}'>\n"
				."        <icon src='../../gfx/{$msRelatedIcon}'/>\n"
				."        <string id='mi_associate_{$msRelatedPluralLCEntity}'>Associar {$msPluralLabel}</string>\n"
				."        <clientevent event='onClick'>open_popup_{$msRelatedLCEntity}_associate();</clientevent>\n"
				."      </menuitem>\n";
			}
		}
		$msXml.= "    </menu>\n"
		."    <column alias='{$this->csAliasId}' edit='false'>\n"
		."      <box width='25'/>\n"
		."    </column>\n"
		."    <column alias='{$this->csNameFieldAlias}' canorder='true' tooltip='true'>\n"
		."      <static marginleft='10' vertical='middle'>\n"
		."        <string id='gc_name'>Nome</string>\n"
		."      </static>\n"
		."    </column>\n";

		foreach(array_keys($this->getColumns()) as $msFieldName){
			if($msFieldName=='status'){
				$msAlias = "{$this->csLowerCaseEntityName}_state";
				$msCanOrder = 'true';
				$msToolTip = 'true';
				$msDisplay = 'true';
				$msShortAlias = 'state';
				$msColumnTitle = 'Status';
			}else{
				$msAlias = $this->getFieldProperty($msFieldName,'alias');
				$msCanOrder = ($this->getFieldProperty($msFieldName,'canorder')?'true':'false');
				$msToolTip = ($this->getFieldProperty($msFieldName,'tooltip')?'true':'false');
				$msDisplay = ($this->getFieldProperty($msFieldName,'display')?'true':'false');
				$msShortAlias = $this->stripOffPrefix($msAlias);
				$msColumnTitle = $this->getFieldProperty($msFieldName,'title');
				if(FWDWizardLib::getFieldPrefix($msFieldName)=='fk' && $msFieldName!='fkcontext'){
					$msAlias = preg_replace('/_id$/','',$msAlias);
				}
			}
			$msXml.= "    <column alias='{$msAlias}' canorder='{$msCanOrder}' tooltip='{$msToolTip}' display='{$msDisplay}'>\n"
			."      <static marginleft='10' vertical='middle'>\n"
			."        <string id='gc_{$msShortAlias}'>{$msColumnTitle}</string>\n"
			."      </static>\n"
			."    </column>\n";
		}

		if($this->cbShowTooltipCreateModify){
			$msXml.= "    <column alias='creator_name' display='false'/>\n"
			."    <column alias='date_created' display='false'/>\n"
			."    <column alias='modifier_name' display='false'/>\n"
			."    <column alias='date_modified' display='false'/>\n";
		}

		$msXml.= "  </dbgrid>\n"
		."  <viewbutton name='insert' class='FWDViewButtonISMS'>\n"
		."    <box top='412' left='896' height='20' width='70'/>\n"
		."    <clientevent event='onClick'>\n"
		."      isms_open_popup('popup_{$this->csLowerCaseEntityName}_edit','{$msScreensPath}popup_{$this->csLowerCaseEntityName}_edit.php','','false');\n"
		."    </clientevent>\n"
		."    <static horizontal='center'>\n"
		."      <box top='0' left='0' height='20' width='70'/>\n"
		."      <string id='vb_insert'>Inserir</string>\n"
		."    </static>\n"
		."  </viewbutton>\n"
		."</dialog>";
		return $msXml;
	}

	public function getPhp(){
		$msLowerCaseLabel = strtolower($this->csLabel);
		$msInstanceName = "\$mo{$this->csTitleCaseEntityName}";
		$msVariableId = "selected_{$this->csAliasId}";
		$msInstanceId = "\$mi{$this->csTitleCaseEntityName}Id";
		$msIconName = FWDWizardLib::getIconName($this->csTableName);
		$msGridHandlersPath = FWDWizardLib::getInstance()->getGridHandlersRef(FWDWizardLib::getPrefix($this->csTableName));

		$msConstantName = "CONTEXT_".strtoupper($this->csTableName);

		$msPhp = "<?php\n"
		."\n"
		."include_once 'include.php';\n";
		foreach($this->caAssociations as $maAssociation){
			$msRelationTable = $maAssociation['relation_table'];
			$msRelationTCEntity = FWDWizardLib::getTitleCaseEntityName($msRelationTable);
			$msRelationSelectHandlersRef = FWDWizardLib::getInstance()->getSelectHandlersRef(FWDWizardLib::getPrefix($msRelationTable));
			$msPhp.= "include_once \$handlers_ref . '{$msRelationSelectHandlersRef}QuerySelect{$msRelationTCEntity}.php';\n";
		}
		$msPhp.= "include_once \$handlers_ref . '{$msGridHandlersPath}QueryGrid{$this->csTitleCaseEntityName}.php';\n"
		."include_once \$classes_isms_ref . 'nonauto/ISMSContextDrawGrid.php';\n"
		."\n"
		."class {$this->csTitleCaseEntityName}DrawGrid extends ISMSContextDrawGrid {\n"
		."\n"
		."  public function __construct(){\n"
		."    parent::__construct('{$msIconName}'".($this->cbShowLinkPopupVisualize?",{$msConstantName}":'').");\n";

		if($this->cbShowTooltipCreateModify){
			$msPhp.= "    \$this->setupToolTipCreateModify('creator_name','date_created','modifier_name','date_modified');\n";
		}

		foreach(array_keys($this->getColumns()) as $msFieldName){
			if($msFieldName!='status'){
				$msDrawType = $this->getFieldProperty($msFieldName,'draw_type');
				if($msDrawType){
					$msAlias = $this->getFieldProperty($msFieldName,'alias');
					$msPhp.= "    \$this->setColumnType('{$msAlias}', ISMSContextDrawGrid::TYPE_{$msDrawType});\n";
				}
			}
		}

		if($this->cbShowStatusColumn){
			$msPhp.= "    \$this->setColumnType('{$this->csLowerCaseEntityName}_state', ISMSContextDrawGrid::TYPE_STATE);\n";
		}

		$msPhp.= "  }\n"
		."\n"
		."  protected function setupMenu(){\n"
		."    \$miUserId = ISMSLib::getCurrentUserId();\n"
		."    {$msInstanceId} = \$this->coCellBox->getValue();\n"
		."    {$msInstanceName} = new {$this->csClassName}();\n"
		."    {$msInstanceName}->fetchById({$msInstanceId});\n"
		."    \n"
		."    \$maMenuACLs = array('edit','delete');\n"
		."    \$maAllowed = array();\n"
		."    if({$msInstanceName}->userCanEdit({$msInstanceId})){ // teste se o usuario pode editar\n"
		."      \$maAllowed[] = 'edit';\n"
		."    }\n"
		."    if({$msInstanceName}->userCanDelete({$msInstanceId})){ // teste se o usuario pode remover\n"
		."      \$maAllowed[] = 'delete';\n"
		."    }\n"
		."    \$moACL = FWDACLSecurity::getInstance();\n"
		."    \$moACL->setNotAllowed(array_diff(\$maMenuACLs,\$maAllowed));\n"
		."    \$moMenu = FWDWebLib::getObject('menu');\n"
		."    \$moGrid = FWDWebLib::getObject('grid_{$this->csLowerCaseEntityName}');\n"
		."    \$msLine = \$moGrid->getAttrName().'_acl_'.\$this->ciRowIndex;\n"
		."    FWDGridMenuACLSecurity::getInstance()->installGridMenuACL(\$moMenu,\$moACL,\$moGrid,\$msLine);\n"
		."  }\n"
		."\n"
		."}\n"
		."\n"
		."class ConfirmRemove extends FWDRunnable {\n"
		."  public function run() {\n"
		."    \$msTitle = FWDLanguage::getPHPStringValue('tt_{$this->csLowerCaseEntityName}_remove','Remover {$this->csLabel}');\n"
		."    \$msMessage = FWDLanguage::getPHPStringValue('st_{$this->csLowerCaseEntityName}_remove_message','Voc� tem certeza que deseja excluir o(a) {$msLowerCaseLabel} <b>%name%</b>?');\n"
		."    \n"
		."    {$msInstanceName} = new {$this->csClassName}();\n"
		."    {$msInstanceId} = intval(FWDWebLib::getObject('{$msVariableId}')->getValue());\n"
		."    {$msInstanceName}->fetchById({$msInstanceId});\n"
		."    \$msName = {$msInstanceName}->getFieldValue('{$this->csNameFieldAlias}');\n"
		."    \$msMessage = str_replace('%name%',\$msName,\$msMessage);\n"
		."    \n"
		."    \$msEventValue = \"soPopUpManager.getPopUpById('popup_confirm').getOpener().remove();\";\n"
		."    \n"
		."    ISMSLib::openConfirm(\$msTitle,\$msMessage,\$msEventValue,50);\n"
		."  }\n"
		."}\n"
		."\n"
		."class RemoveEvent extends FWDRunnable {\n"
		."  public function run() {\n"
		."    {$msInstanceId} = intval(FWDWebLib::getObject('{$msVariableId}')->getValue());\n"
		."    \n"
		."    \$moCtxUserTest = new {$this->csClassName}();\n"
		."    \$moCtxUserTest->testPermissionToDelete({$msInstanceId});\n"
		."    \n"
		."    {$msInstanceName} = new {$this->csClassName}();\n"
		."    if({$msInstanceName}->isDeletable({$msInstanceId})){\n"
		."      {$msInstanceName}->delete({$msInstanceId},true);\n"
		."      FWDWebLib::getObject('grid_{$this->csLowerCaseEntityName}')->execEventPopulate();\n"
		."      echo 'refresh_grid();';\n"
		."    }else{\n"
		."      {$msInstanceName}->showDeleteError();\n"
		."    }\n"
		."  }\n"
		."}\n"
		."\n";
		foreach($this->caAssociations as $maAssociation){
			$msInstanceId = "\$mi{$this->csTitleCaseEntityName}Id";
			$msInstanceName = "\$mo{$this->csTitleCaseEntityName}";
			$msRelationTable = $maAssociation['relation_table'];
			$msRelatedTable = $maAssociation['related_table'];
			$msRelationClass = FWDWizardLib::getClassName($msRelationTable);
			$msRelationTCEntity = FWDWizardLib::getTitleCaseEntityName($msRelationTable);
			$msRelatedLCEntity = FWDWizardLib::getLowerCaseEntityName($msRelatedTable);
			$msRelatedTCEntity = FWDWizardLib::getTitleCaseEntityName($msRelatedTable);
			$msRelatedPluralTCEntity = FWDWizardLib::pluralize($msRelatedTCEntity);
			$msVariableId = "selected_{$this->csAliasId}";
			$msRelatedScreenRef = FWDWizardLib::getInstance()->getScreensRef(FWDWizardLib::getPrefix($msRelatedTable));

			$msPhp.= "class OpenPopup{$msRelatedTCEntity}AssociateEvent extends FWDRunnable {\n"
			."  public function run(){\n"
			."    \$moQuery = new QuerySelect{$msRelationTCEntity}();\n"
			."    \$moQuery->set{$this->csTitleCaseEntityName}(FWDWebLib::getObject('{$msVariableId}')->getValue());\n"
			."    \$msIds = implode(':',\$moQuery->getIds());\n"
			."    echo \"isms_open_popup('popup_{$msRelatedLCEntity}_associate','{$msRelatedScreenRef}popup_{$msRelatedLCEntity}_associate.php?ids=\$msIds','','true');\"\n"
			."        .\"gobi('grid_{$this->csLowerCaseEntityName}').hideGlass();\";\n"
			."  }\n"
			."}\n"
			."\n"
			."class Update{$msRelationTCEntity}RelationEvent extends FWDRunnable {\n"
			."  public function run(){\n"
			."    {$msInstanceId} = FWDWebLib::getObject('{$msVariableId}')->getValue();\n"
			."    {$msInstanceName} = new {$this->csClassName}();\n"
			."    {$msInstanceName}->setFieldValue('{$this->csAliasId}',{$msInstanceId});\n"
			."    \$ma{$msRelatedPluralTCEntity} = explode(':',FWDWebLib::getObject('var_associated_ids')->getValue());\n"
			."    {$msInstanceName}->updateRelation(new {$msRelationClass}(),'{$msRelatedLCEntity}_id',\$ma{$msRelatedPluralTCEntity});\n"
			."  }\n"
			."}\n"
			."\n";
		}
		$msPhp.= "class ScreenBeforeEvent extends FWDRunnable {\n"
		."  public function run(){\n"
		."    \$moStartEvent = FWDStartEvent::getInstance();\n"
		."    \$moStartEvent->setScreenEvent(new ScreenEvent(''));\n"
		."    \$moStartEvent->addAjaxEvent(new ConfirmRemove('confirm_remove'));\n"
		."    \$moStartEvent->addAjaxEvent(new RemoveEvent('remove_event'));\n";
		foreach($this->caAssociations as $maAssociation){
			$msRelatedTable = $maAssociation['related_table'];
			$msRelationTable = $maAssociation['relation_table'];
			$msRelatedLCEntity = FWDWizardLib::getLowerCaseEntityName($msRelatedTable);
			$msRelatedTCEntity = FWDWizardLib::getTitleCaseEntityName($msRelatedTable);
			$msRelationLCEntity = FWDWizardLib::getLowerCaseEntityName($msRelationTable);
			$msRelationTCEntity = FWDWizardLib::getTitleCaseEntityName($msRelationTable);
			$msPhp.= "    \$moStartEvent->addAjaxEvent(new OpenPopup{$msRelatedTCEntity}AssociateEvent('open_popup_{$msRelatedLCEntity}_associate'));\n"
			."    \$moStartEvent->addAjaxEvent(new Update{$msRelationTCEntity}RelationEvent('update_{$msRelationLCEntity}_relation'));\n";
		}
		$msPhp.= "    \n"
		."    //instala a seguran�a de ACL na p�gina\n"
		."    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());\n"
		."    FWDMenuACLSecurity::getInstance()->installMenuACL(FWDWebLib::getObject('menu'),FWDACLSecurity::getInstance());\n"
		."    \n"
		."    \$moGrid = FWDWebLib::getObject('grid_{$this->csLowerCaseEntityName}');\n"
		."    \$moHandler = new QueryGrid{$this->csTitleCaseEntityName}(FWDWebLib::getConnection());\n"
		."    \$moGrid->setQueryHandler(\$moHandler);\n"
		."    \$moGrid->setObjFwdDrawGrid(new {$this->csTitleCaseEntityName}DrawGrid());\n"
		."  }\n"
		."}\n"
		."\n"
		."class ScreenEvent extends FWDRunnable {\n"
		."  public function run(){\n"
		."    {$msInstanceName} = new {$this->csClassName}();\n"
		."    if(!{$msInstanceName}->userCanInsert()){\n"
		."      FWDWebLib::getObject('insert')->setShouldDraw(false);\n"
		."    }\n"
		."    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));\n"
		."    ?>\n"
		."      <script language='javascript'>\n"
		."        \n"
		."        function refresh_grid(){\n"
		."          gobi('grid_{$this->csLowerCaseEntityName}').refresh();\n"
		."        }\n"
		."        \n"
		."        function remove(){\n"
		."          trigger_event('remove_event',3);\n"
		."          refresh_grid();\n"
		."        }\n"
		."        \n";
		foreach($this->caAssociations as $maAssociation){
			$msVariableId = "selected_{$this->csAliasId}";
			$msRelationTable = $maAssociation['relation_table'];
			$msRelatedTable = $maAssociation['related_table'];
			$msRelatedLCEntity = FWDWizardLib::getLowerCaseEntityName($msRelatedTable);
			$msRelationLCEntity = FWDWizardLib::getLowerCaseEntityName($msRelationTable);
			$msRelatedPluralLCEntity = FWDWizardLib::pluralize($msRelatedLCEntity);
			$msPhp.= "        function open_popup_{$msRelatedLCEntity}_associate(){\n"
			."          gobi('grid_{$this->csLowerCaseEntityName}').showGlass();\n"
			."          gebi('{$msVariableId}').value = FWDMenu.value;\n"
			."          trigger_event('open_popup_{$msRelatedLCEntity}_associate',3);\n"
			."        }\n"
			."        \n"
			."        function set_{$msRelatedPluralLCEntity}(psIds){\n"
			."          gebi('var_associated_ids').value = psIds;\n"
			."          trigger_event('update_{$msRelationLCEntity}_relation',3);\n"
			."        }\n"
			."        \n";
		}
		$msPhp.= "      </script>\n"
		."    <?\n"
		."  }\n"
		."}\n"
		."\n"
		."// Testa se a licen�a utilizada permite acessar este m�dulo\n"
		."//ISMSLib::testUserPermissionTo%module%Mode();\n"
		."\n"
		."FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));\n"
		."FWDWebLib::getInstance()->xml_load('nav_{$this->csLowerCaseEntityName}.xml');\n"
		."\n"
		."?>";

		return $msPhp;
	}

	public function getQueryPhp(){
		$moQueryBuilder = new FWDQueryBuilder($this->csTableName,$this->csClassName,$this->csLabel);
		$moQueryBuilder->setPackage('ISMS');
		$moQueryBuilder->setName($this->getQueryName());

		$maUsedAcronyms = array();

		$msAcronym = FWDWizardLib::getInstance()->getTableAcronym($this->csTableName);
		$msFrom = "\n                      view_{$this->csTableName}_active {$msAcronym}";

		$maUsedAcronyms[$msAcronym] = true;

		foreach($this->getIncludedFields() as $msField){
			$msFieldPrefix = FWDWizardLib::getFieldPrefix($msField);
			if($msFieldPrefix=='fk' && $msField!='fkcontext'){
				$msReferencedTable = $this->caReferencedTables[$msField];
				$msRefTableAcronym = FWDWizardLib::getInstance()->getTableAcronym($msReferencedTable);
				// Se o alias j� existe, usa um n�mero pra diferenciar
				if(isset($maUsedAcronyms[$msRefTableAcronym])){
					$i = 2;
					while(isset($maUsedAcronyms[$msRefTableAcronym.$i])) $i++;
					$msRefTableAcronym.= $i;
				}

				$msFieldName = "{$msRefTableAcronym}.".$this->getFieldProperty($msField,'name_column');
				$msFrom.= "\n                      JOIN {$msReferencedTable} {$msRefTableAcronym} ON ({$msRefTableAcronym}.fkcontext = {$msAcronym}.{$msField})";
				$maUsedAcronyms[$msRefTableAcronym] = true;
				$msAlias = preg_replace('/_id$/','',$this->getFieldProperty($msField,'alias'));
			}else{
				$msFieldName = "{$msAcronym}.".$this->getFieldProperty($msField,'name');
				$msAlias = $this->getFieldProperty($msField,'alias');
			}
			$msDBType = $this->getFieldProperty($msField,'db_type');
			$moQueryBuilder->addField($msFieldName);
			$moQueryBuilder->setFieldProperty($msFieldName,'name',$msFieldName);
			$moQueryBuilder->setFieldProperty($msFieldName,'alias',$msAlias);
			$moQueryBuilder->setFieldProperty($msFieldName,'type',$msDBType);
		}

		if($this->cbShowStatusColumn){
			$msContextAcronym = FWDWizardLib::getInstance()->getTableAcronym('isms_context');
			$msFrom.= "\n                      JOIN isms_context {$msContextAcronym} ON ({$msContextAcronym}.pkcontext = {$msAcronym}.fkcontext)";
			$msFieldName = "{$msContextAcronym}.nstate";
			$moQueryBuilder->addField($msFieldName);
			$moQueryBuilder->setFieldProperty($msFieldName,'name',$msFieldName);
			$moQueryBuilder->setFieldProperty($msFieldName,'alias',"{$this->csLowerCaseEntityName}_state");
			$moQueryBuilder->setFieldProperty($msFieldName,'type','DB_NUMBER');
		}

		if($this->cbShowTooltipCreateModify){
			$msHistoryAcronym = FWDWizardLib::getInstance()->getTableAcronym('isms_context_history');
			$msFrom.= "\n                      JOIN context_history {$msHistoryAcronym} ON ({$msHistoryAcronym}.context_id = {$msAcronym}.fkcontext)";

			$msFieldName = "{$msHistoryAcronym}.context_creator_name";
			$moQueryBuilder->addField($msFieldName);
			$moQueryBuilder->setFieldProperty($msFieldName,'name',$msFieldName);
			$moQueryBuilder->setFieldProperty($msFieldName,'alias','creator_name');
			$moQueryBuilder->setFieldProperty($msFieldName,'type','DB_STRING');

			$msFieldName = "{$msHistoryAcronym}.context_date_created";
			$moQueryBuilder->addField($msFieldName);
			$moQueryBuilder->setFieldProperty($msFieldName,'name',$msFieldName);
			$moQueryBuilder->setFieldProperty($msFieldName,'alias','date_created');
			$moQueryBuilder->setFieldProperty($msFieldName,'type','DB_DATETIME');

			$msFieldName = "{$msHistoryAcronym}.context_modifier_name";
			$moQueryBuilder->addField($msFieldName);
			$moQueryBuilder->setFieldProperty($msFieldName,'name',$msFieldName);
			$moQueryBuilder->setFieldProperty($msFieldName,'alias','modifier_name');
			$moQueryBuilder->setFieldProperty($msFieldName,'type','DB_STRING');

			$msFieldName = "{$msHistoryAcronym}.context_date_modified";
			$moQueryBuilder->addField($msFieldName);
			$moQueryBuilder->setFieldProperty($msFieldName,'name',$msFieldName);
			$moQueryBuilder->setFieldProperty($msFieldName,'alias','date_modified');
			$moQueryBuilder->setFieldProperty($msFieldName,'type','DB_DATETIME');
		}

		$moQueryBuilder->setFrom($msFrom);

		return $moQueryBuilder->getPhp();
	}

}

?>