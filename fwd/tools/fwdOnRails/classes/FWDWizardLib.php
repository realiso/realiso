<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

include('Inflector.php');

/**
 * Classe FWDWizardLib.
 *
 * <p></p>
 * @package Rails
 * @subpackage classes
 */
class FWDWizardLib {

	protected static $coInstance = null;

	protected $csSystemRef;
	protected $caModules;
	protected $caPackages;
	protected $caTables;

	private function __construct(){
		$this->caModules = array();
		$this->caPackages = array();
		$this->caTables = array();
		require('tables.php');
		foreach($laTables as $maTable){
			$this->caTables[$maTable['name']] = $maTable;
		}
	}

	public static function getInstance(){
		if(self::$coInstance===null){
			self::$coInstance = new FWDWizardLib();
		}
		return self::$coInstance;
	}

	public function setSystemRef($psSystemRef){
		$this->csSystemRef = $psSystemRef;
	}

	public function getSystemRef(){
		return $this->csSystemRef;
	}

	public function addModule($psTablesPrefix,$psScreensRef,$psClassesRef,$psGridHandlersRef,$psSelectHandlersRef){
		$this->caModules[strtolower($psTablesPrefix)] = array(
      'screens_ref' => $psScreensRef,
      'classes_ref' => $psClassesRef,
      'grid_handlers_ref' => $psGridHandlersRef,
      'select_handlers_ref' => $psSelectHandlersRef,
		);
	}

	public function addPackage($psPackageAcronym,$psPackageName){
		$this->caPackages[strtolower($psPackageAcronym)] = array(
      'acronym' => $psPackageAcronym,
      'name' => $psPackageName,
		);
	}

	public function getPackageName($psPackageAcronym){
		return $this->caPackages[strtolower($psPackageAcronym)]['name'];
	}

	public function getPackages(){
		return $this->caPackages;
	}

	public function getScreensRef($psModulePrefix){
		return $this->caModules[strtolower($psModulePrefix)]['screens_ref'];
	}

	public function getClassesRef($psModulePrefix){
		return $this->caModules[strtolower($psModulePrefix)]['classes_ref'];
	}

	public function getGridHandlersRef($psModulePrefix){
		return $this->caModules[strtolower($psModulePrefix)]['grid_handlers_ref'];
	}

	public function getSelectHandlersRef($psModulePrefix){
		return $this->caModules[strtolower($psModulePrefix)]['select_handlers_ref'];
	}

	public static function getPrefix($psTableName){
		return substr($psTableName,0,strpos($psTableName,'_'));
	}

	public static function getClassName($psTableName){
		$maTableName = explode('_',$psTableName);
		$msClassName = strtoupper($maTableName[0]);
		unset($maTableName[0]);
		$msClassName.= implode('',array_map('ucfirst',$maTableName));
		return $msClassName;
	}

	public static function getTitleCaseEntityName($psTableName){
		$maTableName = explode('_',$psTableName);
		unset($maTableName[0]);
		return implode('',array_map('ucfirst',$maTableName));
	}

	public static function getLowerCaseEntityName($psTableName){
		$maTableName = explode('_',$psTableName);
		unset($maTableName[0]);
		return implode('_',$maTableName);
	}

	public static function getGridQueryName($psTableName){
		return "QueryGrid".self::getTitleCaseEntityName($psTableName);
	}

	public static function getSelectQueryName($psTableName){
		return "QuerySelect".self::getTitleCaseEntityName($psTableName);
	}

	public static function getIconName($psTableName,$psExtension='gif'){
		return "icon-{$psTableName}.{$psExtension}";
	}

	public static function getConstantName($psTableName){
		return 'CONTEXT_'.strtoupper($psTableName);
	}

	public static function getSetMethod($psPropertyName,$psIdentation='  '){
		$msPropertyName = substr($psPropertyName,2);
		$msTypePrefix = $psPropertyName[1];
		return $psIdentation."public function set{$msPropertyName}(\$p{$msTypePrefix}{$msPropertyName}){\n"
		.$psIdentation."  \$this->c{$msTypePrefix}{$msPropertyName} = \$p{$msTypePrefix}{$msPropertyName};\n"
		.$psIdentation."}\n\n";
	}

	public static function getGetMethod($psPropertyName,$psIdentation='  '){
		$msPropertyName = substr($psPropertyName,2);
		$msTypePrefix = $psPropertyName[1];
		return $psIdentation."public function get{$msPropertyName}(){\n"
		.$psIdentation."  return \$this->c{$msTypePrefix}{$msPropertyName};\n"
		.$psIdentation."}\n\n";
	}

	public static function getFieldPrefix($psFieldName){
		if($psFieldName[1]=='k' && ($psFieldName[0]=='p' || $psFieldName[0]=='f')){
			return substr($psFieldName,0,2);
		}else{
			return $psFieldName[0];
		}
	}

	public static function createFile($psPath,$psContent,$pbOverwrite=true){
		$msFolder = substr($psPath,0,strrpos($psPath,'/'));
		if(($pbOverwrite && is_writable($psPath)) || (!file_exists($psPath) && is_writable($msFolder))){
			$mrFile = fopen($psPath,'w');
			fwrite($mrFile,$psContent);
			fclose($mrFile);
			return true;
		}else{
			trigger_error("Could not create file '$psPath'.",E_USER_WARNING);
			return false;
		}
	}

	public static function getDialogDimensions($psXmlFilePath){
		$moParser = new FWDDimensionsExtractor();
		$moParser->parseFile($psXmlFilePath);
		$miDialogHeight = $moParser->getDialogHeight();
		$miDialogWidth = $moParser->getDialogWidth();
		return array('height'=>$miDialogHeight,'width'=>$miDialogWidth);
	}

	public function getTableAcronym($psTableName){
		if(isset($this->caTables[$psTableName])){
			return $this->caTables[$psTableName]['acronym'];
		}else{
			trigger_error("Unknown table '$psTableName'.",E_USER_WARNING);
			echo "<pre>";
			print_r($this);
			echo "</pre>";
		}
	}

	public static function pluralize($psWord){
		return Inflector::pluralize($psWord);
	}

	public static function stripIdentation($psTextBlock){
		$maLines = array_values(array_filter(explode("\n",$psTextBlock)));
		$miMinIdent = strlen($maLines[0]);
		for($i=0;$i<count($maLines);$i++){
			$miIdent = strlen($maLines[$i]) - strlen(ltrim($maLines[$i]));
			$miMinIdent = min($miMinIdent,$miIdent);
		}
		for($i=0;$i<count($maLines);$i++){
			$maLines[$i] = substr($maLines[$i],$miMinIdent);
		}
		return implode("\n",$maLines);
	}

	public static function addIdentation($psTextBlock,$piSpaces){
		$msIdent = str_repeat(' ',$piSpaces);
		return str_replace("\n","\n{$msIdent}",$psTextBlock);
	}

}

?>