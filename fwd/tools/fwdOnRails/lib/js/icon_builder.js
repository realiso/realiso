
var Canvas = {
  
  'ciRows': 0,
  
  'ciCols': 0,
  
  'cbPainting': 0,
  
  'csColor': '#000000',
  
  'startPainting': function(){
    Canvas.cbPainting = true;
  },
  
  'stopPainting': function(){
    Canvas.cbPainting = false;
  },
  
  'isPainting': function(){
    return this.cbPainting;
  },
  
  'getColor': function(){
    return this.csColor;
  },
  
  'setColor': function(psColor){
    this.csColor = psColor;
  },
  
  'setPixelColor': function(piRow,piCol,psColor){
    document.getElementById(piRow+'_'+piCol).style.background = psColor;
    this.caPixels[piRow][piCol] = psColor.substr(1);
  },
  
  'paint': function(psPixelId){
    var i,j;
    var maCoordinates = psPixelId.split('_');
    i = maCoordinates[0];
    j = maCoordinates[1];
    if(this.isPainting()){
      this.setPixelColor(i,j,this.getColor());
    }
  },
  
  'init': function(piRows,piCols){
    var i,j;
    this.ciRows = piRows;
    this.ciCols = piCols;
    this.caPixels = [];
    for(i=0;i<piRows;i++){
      this.caPixels[i] = [];
      for(j=0;j<piCols;j++){
        this.caPixels[i][j] = 'FFFFFF';
      }
    }
  },
  
  'getPixels': function(){
    var maLines = [];
    var i;
    for(i=0;i<this.ciRows;i++){
      maLines.push(this.caPixels[i].join(','));
    }
    return maLines.join('|');
  },
  
  'setPixels': function(psSerializedPixels){
    var i,j;
    var maPixels;
    var maLines = psSerializedPixels.split('|');
    for(i=0;i<maLines.length;i++){
      maPixels = maLines[i].split(',');
      for(j=0;j<maPixels.length;j++){
        this.setPixelColor(i,j,'#'+maPixels[j]);
      }
    }
  }
  
}
