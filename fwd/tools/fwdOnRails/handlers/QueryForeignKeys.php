<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryForeignKeys.
 *
 * <p>Consulta que retorna informações sobre as referências de uma tabela para outras.</p>
 * @package Rails
 * @subpackage handlers
 */
class QueryForeignKeys extends FWDDBQueryHandler {

	protected $csSourceTable = '';
	protected $csSourceField = '';
	protected $csReferencedTable = '';
	protected $csReferencedField = '';

	public function __construct($poDB = null){
		parent::__construct($poDB);
		$this->coDataSet->addFWDDBField(new FWDDBField('','constraint_name' ,DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField('','field'          ,DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField('','referenced_table',DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField('','referenced_field',DB_STRING));
	}

	public function setSourceTable($psSourceTable){
		$this->csSourceTable = $psSourceTable;
	}

	public function setSourceField($psSourceField){
		$this->csSourceField = $psSourceField;
	}

	public function setReferencedField($psReferencedField){
		$this->csReferencedField = $psReferencedField;
	}

	public function makeQuery(){
		$maFilters = array();

		if($this->csSourceTable){
			$maFilters[] = "c.conrelid = '{$this->csSourceTable}'::regclass";
		}else{
			trigger_error("You must specify the source table.",E_USER_ERROR);
		}

		if($this->csSourceField){
			$maFilters[] = "a.attname = '{$this->csSourceField}'";
		}

		if($this->csReferencedField){
			$maFilters[] = "ra.attname = '{$this->csReferencedField}'";
		}

		if(count($maFilters)==0){
			$msWhere = '';
		}else{
			$msWhere = ' AND '.implode(' AND ',$maFilters);
		}

		if(FWDWebLib::getConnection()->getDatabaseType()==DB_POSTGRES){
			$this->csSQL = "SELECT
                        c.conname AS constraint_name,
                        a.attname AS field,
                        r.relname AS referenced_table,
                        ra.attname AS referenced_field
                      FROM
                        pg_catalog.pg_constraint c
                        JOIN pg_class r ON (c.confrelid = r.oid)
                        JOIN pg_attribute a ON (a.attnum = c.conkey[1] AND a.attrelid = c.conrelid)
                        JOIN pg_attribute ra ON (ra.attnum = c.confkey[1] AND ra.attrelid = c.confrelid)
                      WHERE c.contype = 'f' $msWhere
                      ORDER BY c.conname";
		}else{
			trigger_error("Database not suported yet.",E_USER_ERROR);
		}
	}

	protected function executeAndFetch(){
		$this->makeQuery();
		$this->executeQuery();
		$this->fetch();
		$this->csReferencedTable = $this->getFieldValue('referenced_table');
		$this->csReferencedField = $this->getFieldValue('referenced_field');
	}

	public function getReferencedTable(){
		if(!$this->csReferencedTable) $this->executeAndFetch();
		return $this->csReferencedTable;
	}

	public function getReferencedField(){
		if(!$this->csReferencedField) $this->executeAndFetch();
		return $this->csReferencedField;
	}

}

?>