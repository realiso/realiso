<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryColumns.
 *
 * <p>Consulta que retorna uma lista com todas colunas de uma tabela.</p>
 * @package Rails
 * @subpackage handlers
 */
class QueryColumns extends FWDDBQueryHandler {

	protected $csTable;

	public function __construct($poDB = null){
		parent::__construct($poDB);
		$this->coDataSet->addFWDDBField(new FWDDBField('a.attname','field_name',DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField('',         'field_type',DB_STRING));
	}

	public function setTable($psTable){
		$this->csTable = $psTable;
	}

	public function makeQuery(){
		if(!$this->csTable){
			trigger_error("Table name is not defined.",E_USER_ERROR);
		}
		if(FWDWebLib::getConnection()->getDatabaseType()==DB_POSTGRES){
			$this->csSQL = "SELECT
                        a.attname AS field_name,
                        CASE
                          WHEN t.typname = 'timestamp' THEN 'DB_DATETIME'
                          WHEN t.typname LIKE 'int%' OR t.typname LIKE 'float%' THEN 'DB_NUMBER'
                          WHEN t.typname = 'text' OR t.typname = 'varchar' THEN 'DB_STRING'
                          ELSE 'DB_UNKNOWN'
                        END AS field_type
                      FROM
                        pg_class c,
                        pg_attribute a,
                        pg_type t
                      WHERE
                        c.relname = '{$this->csTable}'
                        AND a.attnum > 0
                        AND a.attrelid = c.oid
                        AND a.atttypid = t.oid
                      ORDER BY a.attnum";
		}else{
			trigger_error("Database not suported yet.",E_USER_ERROR);
		}
	}

	public function getColumns(){
		$this->makeQuery();
		$this->executeQuery();
		$maColumns = array();
		while($this->fetch()){
			$msFieldName = $this->getFieldValue('field_name');
			if($msFieldName[1]=='k' && ($msFieldName[0]=='p' || $msFieldName[0]=='f')){
				$msAlias = substr($msFieldName,2).'_id';
				$msPrefix = substr($msFieldName,0,2);
			}else{
				$msAlias = substr($msFieldName,1);
				$msPrefix = $msFieldName[0];
			}
			$maColumns[] = array(
        'name' => $msFieldName,
        'type' => $this->getFieldValue('field_type'),
        'alias'      => $msAlias,
        'prefix'     => $msPrefix,
			);
		}
		return $maColumns;
	}

}

?>