<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryStringFields.
 *
 * <p>Retorna os campos de texto de uma tabela.</p>
 * @package Rails
 * @subpackage handlers
 */
class QueryStringFields extends FWDDBQueryHandler {

	protected $csTableName = '';

	public function __construct($poDB = null){
		parent::__construct($poDB);
		$this->coDataSet->addFWDDBField(new FWDDBField('','field_name',DB_STRING));
	}

	public function setTableName($psTableName){
		$this->csTableName = $psTableName;
	}

	public function makeQuery(){

		if(!$this->csTableName){
			trigger_error("You must specify the table name.",E_USER_ERROR);
		}

		if(FWDWebLib::getConnection()->getDatabaseType()==DB_POSTGRES){
			$this->csSQL = "SELECT
                        a.attname AS field_name
                      FROM
                        pg_class c,
                        pg_attribute a,
                        pg_type t
                      WHERE
                        c.relname = '{$this->csTableName}'
                        AND a.attnum > 0
                        AND a.attrelid = c.oid
                        AND a.atttypid = t.oid
                        AND (a.attname LIKE 's%' OR a.attname LIKE 't%')
                      ORDER BY field_name";
		}else{
			trigger_error("Database not suported yet.",E_USER_ERROR);
		}
	}

	public function getStringFields(){
		$this->makeQuery();
		$this->executeQuery();
		$maReturn = array();
		while($this->fetch()){
			$maReturn[] = $this->getFieldValue('field_name');
		}
		return $maReturn;
	}

}

?>