<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryReferences.
 *
 * <p>Retorna as refer�ncias � uma determinada tabela.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryReferences extends FWDDBQueryHandler {

	protected $csTableName = '';

	public function __construct($poDB=null){
		parent::__construct($poDB);
		$this->coDataSet->addFWDDBField(new FWDDBField('t.relname'  ,'table'      ,DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField('a.attname'  ,'column'     ,DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField(''           ,'is_relation',DB_NUMBER));
	}

	public function setTableName($psTableName){
		$this->csTableName = $psTableName;
	}

	public function makeQuery(){
		if($this->csTableName){
			$this->csSQL = "SELECT
                        t.relname AS table,
                        a.attname AS column,
                        CASE WHEN rel.table_name IS NULL THEN 0 ELSE 1 END AS is_relation
                      FROM
                        pg_catalog.pg_constraint c
                        JOIN pg_class t ON (c.conrelid = t.oid)
                        JOIN pg_attribute a ON (a.attnum = c.conkey[1] AND a.attrelid = c.conrelid)
                        LEFT JOIN (
                          SELECT t.relname AS table_name
                          FROM
                            pg_class t
                            JOIN pg_namespace ns ON (ns.oid = t.relnamespace AND ns.nspname != 'pg_catalog' AND ns.nspname != 'information_schema')
                          WHERE
                            t.relkind = 'r'
                            AND NOT EXISTS (
                              SELECT *
                              FROM pg_attribute a
                              WHERE a.attnum NOT IN (
                                SELECT attnum
                                FROM pg_attribute ka
                                JOIN pg_constraint c ON (
                            c.contype IN ('p','f')
                            AND c.conrelid = ka.attrelid
                            AND ka.attnum = ANY (c.conkey)
                                )
                                WHERE ka.attrelid = a.attrelid
                              )
                              AND a.attnum > 0
                              AND a.attrelid = t.oid
                            )
                        ) rel ON (rel.table_name = t.relname)
                      WHERE
                        c.contype = 'f'
                        AND c.confrelid = '{$this->csTableName}'::regclass
                      ORDER BY c.conname";
		}else{
			trigger_error("You must specify a table.",E_USER_ERROR);
		}
	}

}

?>