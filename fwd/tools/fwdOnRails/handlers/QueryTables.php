<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryTables.
 *
 * <p>Consulta que retorna uma lista de todas tabelas do banco.</p>
 * @package Rails
 * @subpackage handlers
 */
class QueryTables extends FWDDBQueryHandler {

	public function __construct($poDB = null){
		parent::__construct($poDB);
		$this->coDataSet->addFWDDBField(new FWDDBField('tablename','table_name',DB_STRING));
	}

	public function makeQuery(){
		if(FWDWebLib::getConnection()->getDatabaseType()==DB_POSTGRES){
			$this->csSQL = "SELECT tablename as table_name
                      FROM pg_tables
                      WHERE
                        tablename !~* 'pg_*'
                        AND tablename !~* 'sql_*'
                      ORDER BY table_name";
		}else{
			trigger_error("Database not suported yet.",E_USER_ERROR);
		}
	}

	public function getTableNames(){
		$this->makeQuery();
		$this->executeQuery();
		$maTableNames = array();
		while($this->fetch()){
			$maTableNames[] = $this->getFieldValue('table_name');
		}
		return $maTableNames;
	}

}

?>