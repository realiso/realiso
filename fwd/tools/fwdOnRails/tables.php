<?

$laTables = array(
array('name'=>'ci_action_plan',               'acronym'=>'acpl'),
array('name'=>'ci_category',                  'acronym'=>'c'),
array('name'=>'ci_incident',                  'acronym'=>'i'),
array('name'=>'ci_incident_control',          'acronym'=>'ic'),
array('name'=>'ci_incident_financial_impact', 'acronym'=>'ifi'),
array('name'=>'ci_incident_process',          'acronym'=>'ip'),
array('name'=>'ci_incident_risk',             'acronym'=>'ir'),
array('name'=>'ci_incident_risk_value',       'acronym'=>'irv'),
array('name'=>'ci_incident_user',             'acronym'=>'iu'),
array('name'=>'ci_nc',                        'acronym'=>'n'),
array('name'=>'ci_nc_action_plan',            'acronym'=>'ncap'),
array('name'=>'ci_nc_process',                'acronym'=>'np'),
array('name'=>'ci_nc_seed',                   'acronym'=>'ns'),
array('name'=>'ci_occurrence',                'acronym'=>'o'),
array('name'=>'ci_risk_probability',          'acronym'=>'rp'),
array('name'=>'ci_risk_schedule',             'acronym'=>'rs'),
array('name'=>'ci_solution',                  'acronym'=>'slt'),
array('name'=>'isms_acl',                     'acronym'=>'acl'),
array('name'=>'isms_audit_log',               'acronym'=>'al'),
array('name'=>'isms_config',                  'acronym'=>'cnf'),
array('name'=>'isms_context',                 'acronym'=>'ctx'),
array('name'=>'isms_context_classification',  'acronym'=>'ccl'),
array('name'=>'isms_context_date',            'acronym'=>'cd'),
array('name'=>'isms_context_history',         'acronym'=>'ch'),
array('name'=>'isms_policy',                  'acronym'=>'pol'),
array('name'=>'isms_profile',                 'acronym'=>'prf'),
array('name'=>'isms_profile_acl',             'acronym'=>'pacl'),
array('name'=>'isms_scope',                   'acronym'=>'sc'),
array('name'=>'isms_solicitor',               'acronym'=>'slc'),
array('name'=>'isms_user',                    'acronym'=>'u'),
array('name'=>'isms_user_password_history',   'acronym'=>'uph'),
array('name'=>'isms_user_preference',         'acronym'=>'up'),
array('name'=>'pm_doc_approvers',             'acronym'=>'da'),
array('name'=>'pm_doc_context',               'acronym'=>'dc'),
array('name'=>'pm_doc_instance',              'acronym'=>'di'),
array('name'=>'pm_doc_read_history',          'acronym'=>'drh'),
array('name'=>'pm_doc_readers',               'acronym'=>'dr'),
array('name'=>'pm_doc_reference',             'acronym'=>'dref'),
array('name'=>'pm_doc_registers',             'acronym'=>'dreg'),
array('name'=>'pm_document',                  'acronym'=>'d'),
array('name'=>'pm_document_revision_history', 'acronym'=>'drevh'),
array('name'=>'pm_instance_comment',          'acronym'=>'dicm'),
array('name'=>'pm_instance_content',          'acronym'=>'dict'),
array('name'=>'pm_process_user',              'acronym'=>'pu'),
array('name'=>'pm_register',                  'acronym'=>'reg'),
array('name'=>'pm_register_readers',          'acronym'=>'rr'),
array('name'=>'pm_template',                  'acronym'=>'t'),
array('name'=>'pm_template_best_practice',    'acronym'=>'tbp'),
array('name'=>'pm_template_content',          'acronym'=>'tc'),
array('name'=>'rm_area',                      'acronym'=>'a'),
array('name'=>'rm_asset',                     'acronym'=>'at'),
array('name'=>'rm_asset_asset',               'acronym'=>'aa'),
array('name'=>'rm_asset_value',               'acronym'=>'av'),
array('name'=>'rm_best_practice',             'acronym'=>'bp'),
array('name'=>'rm_best_practice_event',       'acronym'=>'bpe'),
array('name'=>'rm_best_practice_standard',    'acronym'=>'bps'),
array('name'=>'rm_category',                  'acronym'=>'ec'),
array('name'=>'rm_control',                   'acronym'=>'ctr'),
array('name'=>'rm_control_best_practice',     'acronym'=>'cbp'),
array('name'=>'rm_control_cost',              'acronym'=>'cc'),
array('name'=>'rm_control_efficiency_history','acronym'=>'ceh'),
array('name'=>'rm_control_test_history',      'acronym'=>'cth'),
array('name'=>'rm_event',                     'acronym'=>'e'),
array('name'=>'rm_parameter_name',            'acronym'=>'pn'),
array('name'=>'rm_parameter_value_name',      'acronym'=>'pvn'),
array('name'=>'rm_process',                   'acronym'=>'prc'),
array('name'=>'rm_process_asset',             'acronym'=>'prca'),
array('name'=>'rm_rc_parameter_value_name',   'acronym'=>'rpvn'),
array('name'=>'rm_risk',                      'acronym'=>'r'),
array('name'=>'rm_risk_control',              'acronym'=>'rc'),
array('name'=>'rm_risk_control_value',        'acronym'=>'rcv'),
array('name'=>'rm_risk_limits',               'acronym'=>'rl'),
array('name'=>'rm_risk_value',                'acronym'=>'rv'),
array('name'=>'rm_section_best_practice',     'acronym'=>'sbp'),
array('name'=>'rm_standard',                  'acronym'=>'st'),
array('name'=>'tst_category',                 'acronym'=>'tstc'),
array('name'=>'tst_example',                  'acronym'=>'tste'),
array('name'=>'tst_user',                     'acronym'=>'tstu'),
array('name'=>'wkf_alert',                    'acronym'=>'alt'),
array('name'=>'wkf_control_efficiency',       'acronym'=>'ce'),
array('name'=>'wkf_control_test',             'acronym'=>'ct'),
array('name'=>'wkf_doc_revision',             'acronym'=>'drev'),
array('name'=>'wkf_task',                     'acronym'=>'tsk'),
);
/*
 $saAcronyms = array();
 $saNames = array();
 foreach($laTables as $miKey=>$saTable){
 if(isset($saNames[$saTable['name']])){
 unset($laTables[$miKey]);
 continue;
 }else{
 $saNames[$saTable['name']] = 1;
 }
 if($laTables[$miKey]['acronym']){
 $ssAcronym = $laTables[$miKey]['acronym'];
 }else{
 $saWords = explode('_',$saTable['name']);
 unset($saWords[0]);
 $ssAcronym = '';
 for($i=0;$i<=count($saWords);$i++){
 $ssAcronym.= $saWords[$i][0];
 }
 $laTables[$miKey]['acronym'] = $ssAcronym;
 }
 if(isset($saAcronyms[$ssAcronym])){
 $saAcronyms[$ssAcronym][] = $saTable['name'];
 }else{
 $saAcronyms[$ssAcronym] = array($saTable['name']);
 }
 }

 foreach($laTables as $miKey=>$saTable){
 if(count($saAcronyms[$saTable['acronym']])==1){
 //echo "{$saTable['name']} => {$saTable['acronym']}<br>";
 $ssOutput = "  array('name'=>'{$saTable['name']}',                                              'acronym'=>'{$saTable['acronym']}'),\n";
 }else{
 //echo "<font color='red'>{$saTable['name']} => {$saTable['acronym']}</font><br>";
 $ssOutput = "  array('name'=>'{$saTable['name']}',                                              'acronym'=>''),\n";
 }
 echo substr($ssOutput,0,48).str_replace(' ','',substr($ssOutput,48));
 }
 */
/*
 foreach($saAcronyms as $ssAcronym=>$saTables){
 if(count($saTables)==1){
 unset($saAcronyms[$ssAcronym]);
 }
 }
 //*/
/*
 echo "<pre>";
 //print_r($laTables);
 print_r($saAcronyms);
 echo "</pre>";
 //*/
?>