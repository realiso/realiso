<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe %reportFilterClass%.
 *
 * <p>Classe que implementa o filtro do relatório de %reportDescription%.</p>
 * @package ISMS
 * @subpackage report
 */
class %reportFilterClass% extends FWDReportFilter {

	protected $ciIdFilter = 0;
	protected $caListFilter = array();

	public function getIdFilter(){
		return $this->ciIdFilter;
	}

	public function setIdFilter($piIdFilter){
		$this->ciIdFilter = $piIdFilter;
	}

	public function getListFilter(){
		return $this->caListFilter;
	}

	public function setListFilter($paListFilter){
		$this->caListFilter = $paListFilter;
	}

	public function getSummary(){
		$maFilters = array();

		if($this->ciIdFilter){
			$maFilters[] = array(
        'name' => FWDLanguage::getPHPStringValue('report_id_filter','Filtro por id'),
        'items' => array($this->ciIdFilter)
			);
		}

		if(count($this->caListFilter)){
			$maFilter = array(
        'name' => FWDLanguage::getPHPStringValue('report_list_filter','Filtro de lista'),
        'items' => array()
			);
			foreach($this->caListFilter as $miId){
				$moUser = new ISMSUser();
				$moUser->fetchById($miId);
				$maFilter['items'][] = $moUser->getName();
			}
			$maFilters[] = $maFilter;
		}

		return $maFilters;
	}

}
?>