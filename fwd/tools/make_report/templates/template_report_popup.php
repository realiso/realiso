<?php

set_time_limit(3600);
include_once 'include.php';

class ExampleLevelIterator extends FWDReportLevelIterator {

	public function __construct(){
		parent::__construct('id_alias');
	}

	public function fetch(FWDDBDataSet $poDataSet){
		$msFieldValue = $poDataSet->getFieldByAlias('alias1')->getValue();
		FWDWebLib::getObject('field1')->setValue($msFieldValue);
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(''));
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		FWDWebLib::getObject('level_example')->setLevelIterator(new ExampleLevelIterator());
		$moComponent = new FWDReportGenerator();
		$moComponent->setReport(FWDWebLib::getObject('report'));
		$moComponent->setReportTemplate(new ISMSReportTemplate());

		$moComponent->setReportFilter(FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter());

		$moComponent->setReportName(FWDLanguage::getPHPStringValue('%underscores%','%reportNamePortuguese%'));
		$moComponent->generate();
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('%popUpName%.xml');

?>