<?

$ssTemplatesPath = 'templates/';
$ssOutputPath = 'output/';

function makeFile($psName,$psContent){
	echo "Criando arquivo '$psName'<br>";
	$mrFile = fopen($psName,'w');
	fwrite($mrFile,$psContent);
	fclose($mrFile);
}

if(isset($_POST['reportName'])){
	$saReportName = array_filter(explode(' ',strtolower($_POST['reportName'])));
	$ssReportDescription = $_POST['reportDescription'];
	$ssReportNamePortuguese = $_POST['reportNamePortuguese'];
	$ssReportQuery = str_replace("\n","                      ",$_POST['selectCode']);
	$ssReportConstructor = str_replace("\n","\n    ",$_POST['constructorCode']);

	$ssPascalCase = '';
	foreach($saReportName as $ssWord){
		$ssWord[0] = chr(ord($ssWord[0]) - 32);
		$ssPascalCase.= $ssWord;
	}
	echo "ssPascalCase: $ssPascalCase<br>";

	$ssUnderscores = implode('_',$saReportName);
	echo "ssUnderscores: $ssUnderscores<br>";

	$ssReportClass = "ISMSReport{$ssPascalCase}";
	$ssReportFilterClass = "ISMSReport{$ssPascalCase}Filter";
	$ssPopUpName = "popup_report_{$ssUnderscores}";

	$saSearch = array(
      '%reportClass%',
      '%reportFilterClass%',
      '%popUpName%',
      '%reportDescription%',
      '%reportNamePortuguese%',
      '%underscores%',
      '%reportQuery%',
      '%reportConstructor%'
      );
      $saReplace = array(
      $ssReportClass,
      $ssReportFilterClass,
      $ssPopUpName,
      $ssReportDescription,
      $ssReportNamePortuguese,
      $ssUnderscores,
      $ssReportQuery,
      $ssReportConstructor
      );

      makeFile($ssOutputPath."{$ssReportClass}.php",str_replace($saSearch,$saReplace,file_get_contents($ssTemplatesPath.'template_report_class.php')));
      makeFile($ssOutputPath."{$ssReportFilterClass}.php",str_replace($saSearch,$saReplace,file_get_contents($ssTemplatesPath.'template_report_filter_class.php')));
      makeFile($ssOutputPath."{$ssPopUpName}.php",str_replace($saSearch,$saReplace,file_get_contents($ssTemplatesPath.'template_report_popup.php')));
      makeFile($ssOutputPath."{$ssPopUpName}.xml",str_replace($saSearch,$saReplace,file_get_contents($ssTemplatesPath.'template_report_popup.xml')));

}else{
	?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Gerador de Relat�rios</title>
<script type="text/javascript" src="../functions.js"></script>
<script type="text/javascript">
          function makeConstructorCode(){
            document.getElementById('constructorCode').value = makeConstructor(document.getElementById('selectCode').value);
            /*
            alert(document.getElementById('constructorCode').value);
            return false;
            /*/
            return true;
            //*/
          }
        </script>
</head>
<body>
<form method="post" onSubmit="return makeConstructorCode()"><b>Report
Name: </b><input type="text" name="reportName" maxlength="50" size="50" /><br />
<b>Nome do relat�rio: </b><input type="text" name="reportNamePortuguese"
	maxlength="50" size="50" /><br />
<b>Descri��o do relat�rio: </b><input type="text"
	name="reportDescription" maxlength="100" size="100" /><br />
<textarea name="selectCode" id="selectCode" rows="10" cols="100"></textarea><br />
<input type="hidden" name="constructorCode" id="constructorCode"> <input
	type="submit" value="make"></form>
</body>
</html>
	<?
}

?>