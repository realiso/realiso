
function str_repeat(psStr,piTimes){
  var msOutput = '';
  for(var i=0;i<piTimes;i++){
    msOutput+= psStr;
  }
  return msOutput;
}

function makeConstructor(psSelectCode){
  var regExp = /^[\s]*([^#]*)[\s]+AS[\s]+([^,\n\s]+)[\n\s]*$/i;
  var moPrefixExp = /^([a-zA-Z0-9]+\.)?([a-z]+)[A-Z][a-zA-Z0-9]*$/;
  var linhas,msSelectCode,msUpperSelectCode;
  var maFields = [];
  var field,fieldObj,matches;
  var types = {
    'b':'DB_NUMBER',
    'n':'DB_NUMBER',
    'pk':'DB_NUMBER',
    'fk':'DB_NUMBER',
    'd':'DB_DATETIME',
    's':'DB_STRING',
    't':'DB_STRING'
  };
  
  msSelectCode = psSelectCode.replace(/[\r\n\t]+/g,' ');
  msUpperSelectCode = msSelectCode.toUpperCase();
  if(msUpperSelectCode.indexOf(' FROM ')>-1){
    var miStart,miEnd;
    miStart = msUpperSelectCode.indexOf('SELECT ') + 7;
    miEnd = msUpperSelectCode.indexOf(' FROM ');
    msSelectCode = msSelectCode.substring(miStart,miEnd);
  }
  linhas = msSelectCode.split(',');
  
  for(var i=0;i<linhas.length;i++){
    field = regExp.exec(linhas[i]);
    if(field!=null){
      fieldObj = new Object();
      fieldObj['alias'] = field[2];
      fieldObj['type'] = 'DB_UNKNOWN';
      matches = moPrefixExp.exec(field[1]);
      if(matches!=null){
        fieldObj['colExpr'] = field[1];
        fieldName = matches[2]?matches[2]:matches[1];
        if(types[fieldName]){
          fieldObj['type'] = types[fieldName];
        }
      }else{
        fieldObj['colExpr'] = '';
      }
      maFields.push(fieldObj);
    }
  }
  var output = '';
  var miExprMaxLen = 0;
  var miAliasMaxLen = 0;
  for(i=0;i<maFields.length;i++){
    if(maFields[i]['colExpr'].length > miExprMaxLen) miExprMaxLen = maFields[i]['colExpr'].length;
    if(maFields[i]['alias'].length > miAliasMaxLen) miAliasMaxLen = maFields[i]['alias'].length;
  }
  for(i=0;i<maFields.length;i++){
    fieldObj = maFields[i];
    output+= '$this->coDataSet->addFWDDBField(new FWDDBField('
            +"'"+fieldObj['colExpr']+"'" + str_repeat(' ',miExprMaxLen - fieldObj['colExpr'].length)
            +','
            +"'"+fieldObj['alias']+"'" + str_repeat(' ',miAliasMaxLen - fieldObj['alias'].length)
            +','
            +fieldObj['type']
            +'));\n';
  }
  return output;
}