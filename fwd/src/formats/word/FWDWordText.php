<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe que representa um texto.
 *
 * <p>Classe que representa um texto de um par�grafo.</p>
 *
 * @package FWD5
 * @subpackage formats-word
 */
class FWDWordText extends FWDWordObject {

	/**
	 * Texto.
	 * @var string
	 * @access private
	 */
	private $cstext = "";

	/**
	 * Link.
	 * @var string
	 * @access private
	 */
	private $cslink = "";

	/**
	 * Estilo.
	 * @var FWDWordStyle
	 * @access private
	 */
	private $coStyle = null;

	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDWordText.</p>
	 *
	 * @access public
	 *
	 * @param string $pstext Texto (opcional).
	 * @param FWDWordStyle $poStyle Estilo (opcional).
	 */
	public function __construct($pstext = "", $poStyle = null) {
		$this->cstext = $pstext;
		$this->coStyle = $poStyle;
	}

	/**
	 * Seta o texto.
	 *
	 * <p>M�todo para setar o texto.</p>
	 *
	 * @access public
	 *
	 * @param string $pstext Texto.
	 */
	public function setText($pstext) {
		$this->cstext = $pstext;
	}

	/**
	 * Retorna o texto.
	 *
	 * <p>M�todo para retornar o texto.</p>
	 *
	 * @access public
	 * @return string Texto.
	 */
	public function getText() {
		return $this->cstext;
	}

	/**
	 * Seta o link.
	 *
	 * <p>M�todo para setar o link.</p>
	 *
	 * @access public
	 *
	 * @param string $pslink Link.
	 */
	public function setLink($pslink) {
		$this->cslink = $pslink;
	}

	/**
	 * Retorna o link.
	 *
	 * <p>M�todo para retornar o link.</p>
	 *
	 * @access public
	 *
	 * @return string Link.
	 */
	public function getLink() {
		return $this->cslink;
	}

	/**
	 * Seta o objeto de estilo do texto.
	 *
	 * <p>M�todo para setar o objeto de estilo do texto.</p>
	 *
	 * @access public
	 *
	 * @param FWDWordStyle $poStyle Estilo.
	 */
	public function setStyle(FWDWordStyle $poStyle) {
		$this->coStyle = $poStyle;
	}

	/**
	 * Retorna o objeto de estilo do texto.
	 *
	 * <p>M�todo para retornar o objeto de estilo do texto.</p>
	 *
	 * @access public
	 *
	 * @param FWDWordStyle Estilo.
	 */
	public function getStyle() {
		return $this->coStyle;
	}

	/**
	 * Retorna o html que representa o par�grafo.
	 *
	 * <p>M�todo para retornar o html que representa o par�grafo.</p>
	 *
	 * @access public
	 *
	 * @return string C�digo html para o par�grafo.
	 */
	public function createHtml() {
		$mscontent = "";
		$mscontent .= $this->coStyle ? "<font ".$this->coStyle->createHtml().">" : "";
		$mscontent .= $this->cslink ? "<a href=\"".$this->cslink."\">" : "";
		$mscontent .= $this->cstext;
		$mscontent .= $this->cslink ? "</a>" : "";
		$mscontent .= $this->coStyle ? "</font>" : "";
			
		return $mscontent;
	}
}
?>