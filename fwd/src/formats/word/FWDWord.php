<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe para gera��o de documentos no formato do Word (doc).
 *
 * <p>Classe que implementa fun��es b�sicas para gere��o de documentos
 * no word.</p>
 * @package FWD5
 * @subpackage formats-word
 */
class FWDWord {
		
	/**
	 * Nome do arquivo DOC.
	 * @var string
	 * @access private
	 */
	private $csfilename;

	/**
	 * T�tulo do documento.
	 * @var string
	 * @access private
	 */
	private $cstitle;

	/**
	 * Array de FWDWordTextBlock.
	 * @var array
	 * @access private
	 */
	private $caBlocks = array();

	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDWord.</p>
	 *
	 * @access public
	 *
	 * @param string $psfilename Nome do arquivo (opcional).
	 * @param string $pstitle T�tulo do documento (opcional).
	 */
	public function __construct($psfilename = "", $pstitle = "") {
		$this->csfilename = $psfilename;
		$this->cstitle = $pstitle;
	}

	/**
	 * Seta o nome do arquivo DOC.
	 *
	 * <p>M�todo para setar o nome do arquirvo DOC.</p>
	 *
	 * @access public
	 * @param string $psname Nome do arquivo.
	 */
	public function setFilename($psname) {
		$this->csfilename = $psname;
	}

	/**
	 * Retorna o nome do arquivo DOC.
	 *
	 * <p>M�todo para retornar o nome do arquirvo DOC.</p>
	 *
	 * @access public
	 * @return string Nome do arquivo.
	 */
	public function getFilename() {
		return $this->csfilename;
	}

	/**
	 * Seta o t�tulo do documento.
	 *
	 * <p>M�todo para setar o t�tulo do documento.</p>
	 *
	 * @access public
	 * @param string $pstitle T�tulo do documento.
	 */
	public function setTitle($pstitle) {
		$this->cstitle = $pstitle;
	}

	/**
	 * Retorna o t�tulo do documento.
	 *
	 * <p>M�todo para retornar o t�tulo do documento.</p>
	 *
	 * @access public
	 * @return string T�tulo do documento.
	 */
	public function getTitle() {
		return $this->cstitle;
	}

	/**
	 * Adiciona objeto de bloco de texto ao documento.
	 *
	 * <p>M�todo para adicionar um objeto de bloco de texto ao documento.</p>
	 *
	 * @access public
	 *
	 * @param FWDWordTextBlock $poBlock Bloco de texto.
	 */
	public function addBlock(FWDWordTextBlock $poBlock) {
		$this->caBlocks[] = $poBlock;
	}

	/**
	 * Gera o arquivo DOC.
	 *
	 * <p>M�todo que gera o arquivo DOC constru�do.</p>
	 *
	 * @access public
	 */
	public function output() {

		$modoc = new HTML_TO_DOC();

		if ($this->csfilename) $modoc->setDocFileName($this->csfilename);
		else $modoc->setDocFileName("word");

		$mscontent = "<html>";

		$mscontent .= $this->cstitle ? "<head><title>".$this->cstitle."</title></head>" : "";

		$mscontent .= "<body></pre>";

		foreach ($this->caBlocks as $block)
		$mscontent .= $block->createHtml();

		$mscontent .= "</pre></body></html>";

		$modoc->createDoc($mscontent, $this->getFilename(), true);
	}
}
?>