<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe que representa uma coluna de uma tabela.
 *
 * <p>Classe que representa a tag "td" de uma tabela.</p>
 *
 * @package FWD5
 * @subpackage formats-word
 */
class FWDWordColumn {

	/**
	 * Largura.
	 * @var integer
	 * @access private
	 */
	private $ciwidth = 0;

	/**
	 * Estilo.
	 * @var FWDWordStyle
	 * @access private
	 */
	private $coStyle = null;

	/**
	 * Valor.
	 * @var FWDWordObject
	 * @access private
	 */
	private $coValue = null;

	/**
	 * Cor do fundo.
	 * @var string
	 * @access private
	 */
	private $csbgColor = "";
		
	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDWordColumn.</p>
	 *
	 * @access public
	 *
	 * @param integer $piwidth Largura (default = 100).
	 */
	public function __construct($piwidth = 100) {
		$this->setWidth($piwidth);
	}

	/**
	 * Seta a largura da coluna.
	 *
	 * <p>M�todo para setar a largura da coluna.</p>
	 *
	 * @access public
	 * @param integer $piwidth Largura.
	 */
	public function setWidth($piwidth) {
		if ($piwidth > 0) $this->ciwidth = $piwidth;
		else trigger_error("Invalid column width: ",E_USER_ERROR);
	}

	/**
	 * Retorna a largura da coluna.
	 *
	 * <p>M�todo para retornar a largura da coluna.</p>
	 *
	 * @access public
	 * @param integer largura.
	 */
	public function getWidth() {
		return $this->ciwidth;
	}

	/**
	 * Seta o objeto de estilo da coluna.
	 *
	 * <p>M�todo para setar o objeto de estilo da coluna.</p>
	 *
	 * @access public
	 * @param FWDWordStyle $poStyle Estilo.
	 */
	public function setStyle(FWDWordStyle $poStyle) {
		$this->coStyle = $poStyle;
	}

	/**
	 * Retorna o objeto de estilo.
	 *
	 * <p>M�todo para retornar o objeto de estilo.</p>
	 *
	 * @access public
	 * @return FWDWordStyle Estilo.
	 */
	public function getStyle() {
		return $this->coStyle;
	}

	/**
	 * Seta o valor da coluna. Pode ser um FWDWordText ou um FWDWordTable.
	 *
	 * <p>M�todo para setar o objeto com o valor da coluna. Pode ser um FWDWordText ou um FWDWordTable.</p>
	 *
	 * @access public
	 * @param FWDWordObject $poValue Valor.
	 */
	public function setValue(FWDWordObject $poValue) {
		$this->coValue = $poValue;
	}

	/**
	 * Retorna o objeto de valor.
	 *
	 * <p>M�todo para retornar o objeto de valor.</p>
	 *
	 * @access public
	 * @return FWDWordObject Valor.
	 */
	public function getValue() {
		return $this->coValue;
	}

	/**
	 * Seta a cor do fundo.
	 *
	 * <p>M�todo para setar a cor do fundo.</p>
	 *
	 * @access public
	 * @param integer $piR Cor do fundo (Vermelho).
	 * @param integer $piG Cor do fundo (Verde).
	 * @param integer $piB Cor do fundo (Azul).
	 */
	public function setBgColor($piR, $piG, $piB) {
		if(($msbgColor = FWDWordColors::color($piR, $piG, $piB))) $this->csbgColor = $msbgColor;
		else trigger_error("Invalid rgb color: \"" . "($piR,$piG,$piB)" .  "\"",E_USER_ERROR);
	}

	/**
	 * Retorna a cor do fundo.
	 *
	 * <p>M�todo para retornar a cor do fundo.</p>
	 *
	 * @access public
	 * @return string Cor do fundo.
	 */
	public function getBgColor() {
		return $this->csbgColor;
	}

	/**
	 * Retorna o html que representa a tag "td".
	 *
	 * <p>M�todo para retornar o c�digo html para a tag "td".</p>
	 *
	 * @access public
	 * @return string C�digo html para tag "td".
	 */
	public function createHtml() {

		$mscontent = "<td";
		$mscontent .= $this->csbgColor ? " bgcolor=\"".$this->csbgColor."\"" : "";
		$mscontent .= $this->ciwidth ? " width=\"".$this->ciwidth."\"" : "";
		$mscontent .= $this->coStyle ? $this->coStyle->createHtml() : "";
		$mscontent .= ">";
		$mscontent .= "<div>";
		$mscontent .= $this->coValue ? $this->coValue->createHtml() : "";
		$mscontent .= "</div></td>";

		return $mscontent;
	}
}
?>