<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe que representa uma linha de uma tabela.
 *
 * <p>Classe que representa a tag "tr" de uma tabela.</p>
 *
 * @package FWD5
 * @subpackage formats-word
 */
class FWDWordRow {

	/**
	 * Altura.
	 * @var integer
	 * @access private
	 */
	private $ciheight = 0;
		
	/**
	 * Estilo.
	 * @var FWDWordStyle
	 * @access private
	 */
	private $coStyle = null;

	/**
	 * Array de FWDWordColumn.
	 * @var array
	 * @access private
	 */
	private $cacolumns = array();

	/**
	 * Cor do fundo.
	 * @var string
	 * @access private
	 */
	private $csbgColor = "";

	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDWordRow.</p>
	 *
	 * @access public
	 *
	 * @param integer $piheight Altura (default = 15).
	 */
	public function __construct($piheight = 15) {
		$this->setHeight($piheight);
	}

	/**
	 * Seta a altura da linha.
	 *
	 * <p>M�todo para setar a altura da linha.</p>
	 *
	 * @access public
	 * @param integer $piheight Altura.
	 */
	public function setHeight($piheight) {
		if ($piheight >= 0) $this->ciheight = $piheight;
		else trigger_error("Invalid height: ",E_USER_ERROR);
	}

	/**
	 * Retorna a altura da linha.
	 *
	 * <p>M�todo para retornar a altura da linha.</p>
	 *
	 * @access public
	 * @param integer altura.
	 */
	public function getHeight() {
		return $this->ciheight;
	}

	/**
	 * Adiciona uma coluna na tabela.
	 *
	 * <p>M�todo para adicionar uma coluna na tabela.</p>
	 *
	 * @access public
	 * @param FWDWordColumn $pocolumn Coluna.
	 */
	public function addColumn(FWDWordColumn $pocolumn) {
		$this->cacolumns[] = $pocolumn;
	}

	/**
	 * Seta o objeto de estilo da linha.
	 *
	 * <p>M�todo para setar o objeto de estilo da linha.</p>
	 *
	 * @access public
	 * @param FWDWordStyle $poStyle Estilo.
	 */
	public function setStyle(FWDWordStyle $poStyle) {
		$this->coStyle = $poStyle;
	}

	/**
	 * Retorna o objeto de estilo.
	 *
	 * <p>M�todo para retornar o objeto de estilo.</p>
	 *
	 * @access public
	 * @return FWDWordStyle Estilo.
	 */
	public function getStyle() {
		return $this->coStyle;
	}

	/**
	 * Retorna o n�mero de colunas da linha.
	 *
	 * <p>M�todo para retornar o n�mero de colunas da linha.</p>
	 *
	 * @access public
	 * @return integer N�mero de colunas.
	 */
	public function getColumnCount() {
		return count($this->cacolumns);
	}

	/**
	 * Seta a cor do fundo.
	 *
	 * <p>M�todo para setar a cor do fundo.</p>
	 *
	 * @access public
	 * @param integer $piR Cor do fundo (Vermelho).
	 * @param integer $piG Cor do fundo (Verde).
	 * @param integer $piB Cor do fundo (Azul).
	 */
	public function setBgColor($piR, $piG, $piB) {
		if(($msbgColor = FWDWordColors::color($piR, $piG, $piB))) $this->csbgColor = $msbgColor;
		else trigger_error("Invalid rgb color: \"" . "($piR,$piG,$piB)" .  "\"",E_USER_ERROR);
	}

	/**
	 * Retorna a cor do fundo.
	 *
	 * <p>M�todo para retornar a cor do fundo.</p>
	 *
	 * @access public
	 * @return string Cor do fundo.
	 */
	public function getBgColor() {
		return $this->csbgColor;
	}

	/**
	 * Retorna o html que representa a tag "tr".
	 *
	 * <p>M�todo para retornar o c�digo html para a tag "tr".</p>
	 *
	 * @access public
	 * @return string C�digo html para tag "tr".
	 */
	public function createHtml() {
		$mscontent = "<tr";
		$mscontent .= $this->csbgColor ? " bgcolor=\"".$this->csbgColor."\"" : "";
		$mscontent .= $this->ciheight ? " height=\"".$this->ciheight."\"" : "";
		$mscontent .= $this->coStyle ? $this->coStyle->createHtml() : "";
		$mscontent .= ">";

		foreach($this->cacolumns as $column) $mscontent .= $column->createHtml();

		$mscontent .= "</tr>";

		return $mscontent;
	}
}
?>