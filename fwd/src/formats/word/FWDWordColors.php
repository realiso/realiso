<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe para tratamento de cores.
 *
 * <p>Classe para tratamento de cores em documentos DOC.</p>
 *
 * @package FWD5
 * @subpackage formats-word
 */
class FWDWordColors {

	/**
	 * Converte de rgb para o formato html.
	 *
	 * <p>M�todo para converter uma cor especificada em rgb
	 * para o formato html (ex: #ff8800).</p>
	 *
	 * @access public
	 * @param integer $piR Vermelho.
	 * @param integer $piG Verde.
	 * @param integer $piB Azul.
	 *
	 * @return string Html color (ex: #FF00FF).
	 *
	 * @static
	 */
	public static function color($piR, $piG, $piB) {
		$mscolor = "";
		if (($piR >=0 && $piR <= 255) && ($piG >=0 && $piG <= 255) && ($piB >=0 && $piB <= 255))
		$mscolor = "#" . str_pad(dechex(($piR<<16)|($piG<<8)|$piB), 6, "0", STR_PAD_LEFT);
		else
		$mscolor = "";

		return $mscolor;
	}
}
?>