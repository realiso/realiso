<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe que representa uma tabela.
 *
 * <p>Classe que representa uma tabela de um arquivo word.</p>
 *
 * @package FWD5
 * @subpackage formats-word
 */
class FWDWordTable extends FWDWordObject {

	/**
	 * Tamanho da borda.
	 * @var integer
	 * @access private
	 */
	private $ciborder = 0;

	/**
	 * Array de FWDWordRow.
	 * @var array
	 * @access private
	 */
	private $carows = array();
		
	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDWordTable.</p>
	 *
	 * @access public
	 */
	public function __construct() {}

	/**
	 * Seta o tamanho da borda.
	 *
	 * <p>M�todo para setar o tamanho da borda.</p>
	 *
	 * @access public
	 * @param integer $piborder Tamanho da borda.
	 */
	public function setBorder($piborder) {
		if ($piborder > 0) $this->ciborder = $piborder;
		else trigger_error("Invalid border size: " . $piborder,E_USER_ERROR);
	}

	/**
	 * Retorna o tamanho da borda.
	 *
	 * <p>M�todo para retornar o tamanho da borda.</p>
	 *
	 * @access public
	 * @return integer Tamanho da borda.
	 */
	public function getBorder() {
		return $this->ciborder;
	}

	/**
	 * Retorna o n�mero de linhas da tabela.
	 *
	 * <p>M�todo para retornar o n�mero de linhas da tabela.</p>
	 *
	 * @access public
	 * @return integer N�mero de linhas.
	 */
	public function getRowCount() {
		return count($this->carows);
	}

	/**
	 * Adiciona uma linha na tabela.
	 *
	 * <p>M�todo para adicionar uma linha na tabela.</p>
	 *
	 * @access public
	 * @param FWDWordRow $porow Linha.
	 */
	public function addRow(FWDWordRow $porow) {
		$this->carows[] = $porow;
	}

	/**
	 * Retorna o html que representa a tabela.
	 *
	 * <p>M�todo para retornar o c�digo html da tabela.</p>
	 *
	 * @access public
	 * @return string C�digo html da tabela.
	 */
	public function createHtml() {
		$mscontent = "<table";
		$mscontent .= $this->ciborder ? " border='" . $this->ciborder . "'>" : ">";
		foreach ($this->carows as $moRow) $mscontent .= $moRow->createHtml();
		$mscontent .= "</table>";
		return $mscontent;
	}
}
?>