<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe que representa um bloco de texto.
 *
 * <p>Classe que representa um bloco de texto de um documento.</p>
 *
 * @package FWD5
 * @subpackage formats-word
 */
class FWDWordTextBlock {
		
	/**
	 * Array de FWDWordObject.
	 * @var array
	 * @access private
	 */
	private $caobjects = array();

	/**
	 * Estilo.
	 * @var FWDWordStyle
	 * @access private
	 */
	private $coStyle = null;

	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDWordTextBlock.</p>
	 *
	 * @access public
	 */
	public function __construct() {}

	/**
	 * Seta o objeto de estilo do bloco de texto.
	 *
	 * <p>M�todo para setar o objeto de estilo do bloco de texto.</p>
	 *
	 * @access public
	 *
	 * @param FWDWordStyle $poStyle Estilo.
	 */
	public function setStyle(FWDWordStyle $poStyle) {
		$this->coStyle = $poStyle;
	}

	/**
	 * Retorna o objeto de estilo do bloco de texto.
	 *
	 * <p>M�todo para retornar o objeto de estilo do bloco de texto.</p>
	 *
	 * @access public
	 *
	 * @param FWDWordStyle Estilo.
	 */
	public function getStyle() {
		return $this->coStyle;
	}

	/**
	 * Adiciona objeto ao bloco de texto.
	 *
	 * <p>M�todo para adicionar um objeto ao bloco de texto. Pode ser um FWDWordText ou um FWDWordTable</p>
	 *
	 * @access public
	 *
	 * @param FWDWordObject $poValue Valor.
	 */
	public function addObj(FWDWordObject $poValue) {
		$this->caobjects[] = $poValue;
	}

	/**
	 * Adiciona objeto de texto ao bloco de texto.
	 *
	 * <p>M�todo para adicionar um objeto de texto ao bloco de texto.</p>
	 *
	 * @access public
	 *
	 * @param string $pstext Texto.
	 * @param FWDWordStyle $poStyle Estilo.
	 */
	public function addText($pstext, $poStyle = null) {
		$moText = new FWDWordText();
		$moText->setText($pstext);
		$poStyle ? $moText->setStyle($poStyle) : "";
		$this->caobjects[] = $moText;
	}

	/**
	 * Adiciona uma ou mais linhas ao bloco de texto.
	 *
	 * <p>M�todo para adicionar uma ou mais linhas ao bloco de texto.</p>
	 *
	 * @access public
	 *
	 * @param integer $pilines N�mero de linhas (default = 1).
	 */
	public function addLine($pilines = 1) {
		$mslines = "";
		for($miI=0;$miI<$pilines;$miI++) $mslines.="<br/>";
		$this->addText($mslines);
	}

	/**
	 * Retorna o html que representa o bloco de texto.
	 *
	 * <p>M�todo para retornar o html que representa o bloco de texto.</p>
	 *
	 * @access public
	 * @return string C�digo html para o bloco de texto.
	 */
	public function createHtml() {
		$mscontent = $this->coStyle ? "<div " . $this->coStyle->createHtml() . ">" : "<div>";

		if (count($this->caobjects)) foreach ($this->caobjects as $object) $mscontent .= $object->createHtml();

		$mscontent .= "</div>";
			
		return $mscontent;
	}
}
?>