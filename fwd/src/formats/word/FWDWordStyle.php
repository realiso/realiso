<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe que representa um estilo.
 *
 * <p>Classe que representa um estilo de um arquivo word.</p>
 *
 * @package FWD5
 * @subpackage formats-word
 */
class FWDWordStyle {

	/**
	 * Array com os poss�veis valores para o alinhamento do par�grafo.
	 * @var array
	 * @access private
	 */
	private $caalignmentOptions = array("", "left", "center", "right", "justify");

	/**
	 * Alinhamento.
	 * @var string
	 * @access private
	 */
	private $csalignment = "";

	/**
	 * Nome da fonte.
	 * @var string
	 * @access private
	 */
	private $csfont = "";

	/**
	 * Tamanho da fonte.
	 * @var integer
	 * @access private
	 */
	private $cisize = 0;

	/**
	 * Cor da fonte.
	 * @var string
	 * @access private
	 */
	private $cscolor = "";

	/**
	 * Negrito.
	 * @var boolean
	 * @access private
	 */
	private $cbbold = false;

	/**
	 * It�lico.
	 * @var boolean
	 * @access private
	 */
	private $cbitalic = false;
		
	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDWordStyle.</p>
	 *
	 * @access public
	 */
	public function __construct() {}

	/**
	 * Seta o nome da fonte.
	 *
	 * <p>M�todo para setar o nome da fonte.</p>
	 *
	 * @access public
	 *
	 * @param string $psfont Nome da fonte.
	 */
	public function setFont($psfont) {
		$this->csfont = $psfont;
	}

	/**
	 * Retorna o nome da fonte.
	 *
	 * <p>M�todo para retornar o nome da fonte.</p>
	 *
	 * @access public
	 * @return string Nome da lista.
	 */
	public function getFont() {
		return $this->csfont;
	}

	/**
	 * Seta o tamanho da fonte.
	 *
	 * <p>M�todo para setar o tamanho da fonte.</p>
	 *
	 * @access public
	 * @param integer $pisize Tamanho da fonte.
	 */
	public function setSize($pisize) {
		if ($pisize > 0) $this->cisize = $pisize;
		else trigger_error("Invalid font size: ",E_USER_ERROR);
	}

	/**
	 * Retorna o tamanho da fonte.
	 *
	 * <p>M�todo para retornar o tamanho da fonte.</p>
	 *
	 * @access public
	 * @return integer Tamanho da fonte.
	 */
	public function getSize() {
		return $this->cisize;
	}

	/**
	 * Seta a cor da fonte.
	 *
	 * <p>M�todo para setar a cor da fonte.</p>
	 *
	 * @access public
	 * @param integer $piR Cor da fonte (Vermelho).
	 * @param integer $piG Cor da fonte (Verde).
	 * @param integer $piB Cor da fonte (Azul).
	 */
	public function setColor($piR, $piG, $piB) {
		if(($mscolor = FWDWordColors::color($piR, $piG, $piB))) $this->cscolor = $mscolor;
		else trigger_error("Invalid rgb color: \"" . "($piR,$piG,$piB)" .  "\"",E_USER_ERROR);
	}

	/**
	 * Retorna a cor da fonte.
	 *
	 * <p>M�todo para retornar a cor da fonte.</p>
	 *
	 * @access public
	 * @return string Cor da fonte.
	 */
	public function getColor() {
		return $this->cscolor;
	}

	/**
	 * Seta o texto como negrito.
	 *
	 * <p>M�todo para setar o texto como negrito.</p>
	 *
	 * @access public
	 *
	 * @param boolean $pbbold Negrito.
	 */
	public function setBold($pbbold) {
		$this->cbbold = $pbbold;
	}

	/**
	 * Retorna o estilo da fonte (negrito ou n�o).
	 *
	 * <p>M�todo para retornar o estilo da fonte (negrito ou n�o).</p>
	 *
	 * @access public
	 * @return string Estilo da fonte (negrito ou n�o).
	 */
	public function isBold() {
		return $this->cbbold;
	}

	/**
	 * Seta o texto como it�lico.
	 *
	 * <p>M�todo para setar o o texto como it�lico.</p>
	 *
	 * @access public
	 *
	 * @param boolean $pbitalic It�lico.
	 */
	public function setItalic($pbitalic) {
		$this->cbitalic = $pbitalic;
	}

	/**
	 * Retorna o estilo da fonte (it�lico ou n�o).
	 *
	 * <p>M�todo para retornar o estilo da fonte (it�lico ou n�o).</p>
	 *
	 * @access public
	 * @return string Estilo da fonte (it�lico ou n�o).
	 */
	public function isItalic() {
		return $this->cbitalic;
	}

	/**
	 * Seta o alinhamento.
	 *
	 * <p>M�todo para setar o alinhamento do estilo.</p>
	 *
	 * @access public
	 * @param string $psalign Alinhamento do estilo {Left | Center | Right | Justify}.
	 */
	public function setAlignment($psalign) {
		if(in_array($psalign, $this->caalignmentOptions)) $this->csalignment = $psalign;
		else trigger_error("Invalid token: \"" . $psalign . "\"",E_USER_ERROR);
	}

	/**
	 * Retorna o alinhamento.
	 *
	 * <p>M�todo para retornar o alinhamento do par�grafo.</p>
	 *
	 * @access public
	 * @return string Alinhamento do par�grafo.
	 */
	public function getAlignment() {
		return $this->csalignment;
	}

	/**
	 * Retorna o html que representa o estilo.
	 *
	 * <p>M�todo para retornar o c�digo html do estilo.</p>
	 *
	 * @access public
	 * @return string C�digo html do estilo.
	 */
	public function createHtml() {
		if ($this->csfont || $this->cisize || $this->cscolor || $this->cbitalic || $this->cbbold || $this->csalignment) {
			$mscontent = "style=\"";
			$mscontent .= $this->csalignment ? "text-align:{$this->csalignment};" : "";
			$mscontent .= $this->csfont ? "font-family:{$this->csfont};" : "";
			$mscontent .= $this->cisize ? "font-size:{$this->cisize}.0pt;" : "";
			$mscontent .= $this->cscolor ? "color:{$this->cscolor};" : "";
			$mscontent .= $this->cbitalic ? "font-style:italic;" : "";
			$mscontent .= $this->cbbold ? "font-weight:bold;" : "";
			$mscontent .= "\"";
		}
		else $mscontent = "";
		return $mscontent;
	}
}
?>