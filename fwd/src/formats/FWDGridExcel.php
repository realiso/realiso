<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDGridExcel. Exporta objetos FWDGrid ou FWDDBGrid para o formato XLS.
 *
 * <p>Classe que exporta objetos FWDGrid ou FWDDBGrid para uma planilha do
 * excel (formato .XLS).</p>
 * @package FWD5
 * @subpackage Formats
 */
class FWDGridExcel {

	/**
	 * Array de FWDGrids e/ou FWDDBGrids
	 * @var array
	 * @access private
	 */
	private $caGrid = array();

	/**
	 * Nome do arquivo Excel (*.XLS)
	 * @var string
	 * @access private
	 */
	private $csFilename = '';

	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDGridExcel.</p>
	 *
	 * @access public
	 * @param string $psFilename Nome do arquivo
	 * @param array $paGrid Array de Grids e/ou DBGrids
	 */
	public final function __construct($psFilename = '',$paGrid){
		if($paGrid){
			$this->caGrid = $paGrid;
			$this->csFilename = $psFilename;
			foreach($paGrid as $moGrid){
				if(!($moGrid instanceof FWDGrid || $moGrid instanceof FWDDBGrid)){
					trigger_error("Object FWDGridExcel must have an array of FWDGrid and/or FWDDBGrid",E_USER_ERROR);
				}
			}
		}else{
			trigger_error("Object FWDGridExcel must have an array of FWDGrid and/or FWDDBGrid",E_USER_ERROR);
		}
	}

	/**
	 * Seta os valores da grid nos objetos do arquivo XLS.
	 *
	 * <p>M�todo para setar os valores da grid nos objetos do arquivo XLS.</p>
	 *
	 * @access public
	 */
	public final function execute(){
		$moExcelWriter = new FWDExcelWriter($this->csFilename);

		// Cria os estilos
		$moHeaderStyle = new FWDExcelStyle('h');
		$moOddRowStyle = new FWDExcelStyle('o');
		$moEvenRowStyle = new FWDExcelStyle('e');

		$moAlign = new FWDExcelStyleAlignment();
		$moAlign->SetAttributes('Center', 'Center');
		$moHeaderStyle->setAlignment($moAlign);
		$moOddRowStyle->setAlignment($moAlign);
		$moEvenRowStyle->setAlignment($moAlign);

		$moBorders = new FWDExcelStyleBorders();
		$moBorders->setExternalBorders('Continuous',array(0,0,0));
		$moHeaderStyle->setBorders($moBorders);
		$moOddRowStyle->setBorders($moBorders);
		$moEvenRowStyle->setBorders($moBorders);

		$moInteriorOddRow = new FWDExcelStyleInterior();
		$moInteriorOddRow->setAttributes(array(230,230,230), 'Solid', array(0,0,0));
		$moOddRowStyle->setInterior($moInteriorOddRow);

		$moInteriorEvenRow = new FWDExcelStyleInterior();
		$moInteriorEvenRow->setAttributes(array(200,200,200), 'Solid', array(0,0,0));
		$moEvenRowStyle->setInterior($moInteriorEvenRow);

		$moInteriorHeader = new FWDExcelStyleInterior();
		$moInteriorHeader->setAttributes(array(150,150,150), 'Solid', array(0,0,0));
		$moHeaderStyle->setInterior($moInteriorHeader);

		$moExcelWriter->addStyle($moHeaderStyle);
		$moExcelWriter->addStyle($moOddRowStyle);
		$moExcelWriter->addStyle($moEvenRowStyle);

		// Cria uma planilha para cada grid
		foreach($this->caGrid as $moGrid){
			// Abre uma planilha
			$moExcelWriter->openWorksheet($moGrid->getAttrName());
			// Se � uma DBGrid, popula
			if($moGrid instanceof FWDDBGrid){
				if($moGrid->GetAttrDinamicFill()){
					$moGrid->setAttrDinamicFill('false');
					$moGrid->populate();
					$moGrid->setAttrDinamicFill('true');
				} else {
					$moGrid->populate();
				}
			}
			// Define a altura das linhas
			$miRowHeight = ($moGrid->getAttrRowHeight()?$moGrid->getAttrRowHeight():50);
			// Cria as colunas
			$maColumns = $moGrid->getColumns();
			foreach($maColumns as $moColumn){
				$moExcelWriter->addColumn($moColumn->getAttrWidth()?$moColumn->getAttrWidth():150);
			}
			// Cria o cabe�alho
			$moHeader = new FWDExcelRow();
			$moHeader->setStyleId('h');
			foreach($maColumns as $moColumn){
				$moHeader->addCell(new FWDExcelCell($moColumn->getValue()));
			}
			$moExcelWriter->drawRow($moHeader);
			// Cria as linhas
			$miRowsCount = $moGrid->getAttrRowsCount();
			$miColsCount = count($maColumns);
			$mbOdd = true;
			for($i=1;$i<=$miRowsCount;$i++){
				$moExcelRow = new FWDExcelRow();
				if($mbOdd) $moExcelRow->setStyleId('o');
				else $moExcelRow->setStyleId('e');
				$mbOdd = !$mbOdd;
				$moExcelRow->setHeight($miRowHeight);
				for($j=1;$j<=$miColsCount;$j++){
					$moExcelRow->addCell(new FWDExcelCell($this->getCellValue($moGrid,$i,$j)));
				}
				$moExcelWriter->drawRow($moExcelRow);
			}
		}
		// Encerra o arquivo
		$moExcelWriter->endFile();
	}

	/**
	 * Retorna o valor de uma c�lula.
	 *
	 * <p>Retorna o valor de uma c�lula da grid. Deve ser sobrescrito para
	 * funcionar com grids que sobrescrevem o drawItem da DrawGrid.</p>
	 * @access protected
	 * @param FWDGrid $moGrid Grid
	 * @param integer $piRow Linha da c�lula
	 * @param integer $piCol Coluna da c�lula
	 */
	protected function getCellValue($moGrid,$piRow,$piCol){
		$msOut = $moGrid->getCellValue($piRow,$piCol);
		if($msOut===false) return '';
		else return $moGrid->getCellValue($piRow,$piCol);
	}

}
?>