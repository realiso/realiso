<?php
ini_set('memory_limit','512M');
set_time_limit(0);

/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDDataSetExcel. Exporta objetos FWDDBDataSet para o formato XLS.
 *
 * <p>Classe que exporta objetos FWDDBDataSet para uma planilha no formato .xls.</p>
 * @package FWD5
 * @subpackage Formats
 */

class FWDDataSetExcel {

	/**
	 * DataSet a ser exportado
	 * @var FWDDBDataSet
	 * @access protected
	 */
	protected $coDataSet = null;

	/**
	 * Nome do arquivo Excel (*.xls)
	 * @var string
	 * @access private
	 */
	private $csFilename = "";

	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDDataSetExcel.</p>
	 *
	 * @access public
	 * @param string $psFilename Nome do arquivo
	 * @param FWDDBDataSet $poDataSet DataSet a ser exportado
	 */
	public function __construct($psFilename = '',FWDDBDataSet $poDataSet = null){
		$this->coDataSet = $poDataSet;
		$this->csFilename = $psFilename;
	}
	
	public function setFileName($file){
	    $this->csFilename = $file;
	}

	public function setDataSet($dataSet){
	    $this->coDataSet = $dataSet;
	}
	
	// essa funcao pode ser sobrescrita para melhorar o texto da coluna excel.
	public function checkData($field){
        return $field->getValue();
	}
	
	/**
	 * Exporta o DataSet
	 *
	 * <p>Faz o fetch das linhas do DataSet e exporta em formato de planilha (.xls).</p>
	 * @access public
	 */
	public function execute(){
		$moFields = $this->coDataSet->getFields();
		$sheet = 0;

        $cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_phpTemp;
        $cacheSettings = array( ' memoryCacheSize ' => '200MB');
        PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);		
		
		$objPHPExcel = new PHPExcel();
		
		$objPHPExcel->setActiveSheetIndex($sheet);
		$activeSheet = $objPHPExcel->getActiveSheet();
		
		$title = mb_substr($this->csFilename, 0, 20, 'UTF-8');

		$activeSheet->setTitle($title);
		$activeSheet->getDefaultColumnDimension()->setWidth(20);
		
		$row=1;
		$col=0;

		// cria o header
		foreach($moFields as $moField){
		  
		  if($moField->getShowColumn()){
			// estilo
			$cell = $activeSheet->getCellByColumnAndRow($col, $row);
			
            if($moField->getLabel())
            	$cell->setValue($moField->getLabel());
            else
            	$cell->setValue($moField->getAlias());
            
            $col++;
		  }
            
		}		

        $cell1 = $activeSheet->getCellByColumnAndRow(0, $row);
        $cord1 = $cell1->getCoordinate();

        $cell2 = $activeSheet->getCellByColumnAndRow($col-1, $row);
        $cord2 = $cell2->getCoordinate();

        $style = $activeSheet->getStyle("{$cord1}:{$cord2}");
        $style->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $style->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FF969696');
          
        $style->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	
		$col=0;
		$row++;

		$mbOdd = true;
		
		// popula o resto das linhas
		while($this->coDataSet->fetch()){
			foreach($moFields as $moField){
			  
			    if($moField->getShowColumn()){
				  $activeSheet->setCellValueByColumnAndRow($col, $row, $this->checkData($moField));
				  $col++;
			    }
			}

		    $cell1 = $activeSheet->getCellByColumnAndRow(0, $row);
            $cord1 = $cell1->getCoordinate();
    
            $cell2 = $activeSheet->getCellByColumnAndRow($col-1, $row);
            $cord2 = $cell2->getCoordinate();
    
            $style = $activeSheet->getStyle("{$cord1}:{$cord2}");

            $style->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            
            $fill = $style->getFill();
              
            // estilo "zebra"
            if($mbOdd){ 
              $fill->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFe6e6e6');
            } else {
              $fill->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFc8c8c8');
            }
              
		    $style->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

			$mbOdd = !$mbOdd;
			
			if($row < 3000){
                $row++;
			    $col=0;		        
			} else {
                $sheet += 1;
                $objPHPExcel->createSheet($sheet);
			  	$objPHPExcel->setActiveSheetIndex($sheet);
		        $activeSheet = $objPHPExcel->getActiveSheet();
        		$activeSheet->setTitle($title);
        		$activeSheet->getDefaultColumnDimension()->setWidth(20);
		        
			$row=0;
			$col=0;		        
			}
		}				

		
		header('Cache-Control: ');// leave blank to avoid IE errors
		header('Pragma: ');// leave blank to avoid IE errors		
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="' . rawurlencode($this->csFilename) . '.xls"');
		header('Cache-Control: max-age=0');
		
		$objPHPExcel = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objPHPExcel->save('php://output'); 
	}
}
?>