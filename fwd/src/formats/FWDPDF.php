<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe que extende a FPDF para desenhar cabe�alho e rodap�.
 *
 * <p>Classe que extende a FPDF para desenhar cabe�alho e rodap�.</p>
 * @package FWD5
 * @subpackage report
 */
class FWDPDF extends FPDF {

	/**
	 * @var FWDReportPDFWriter Writer de PDF respons�vel por desenhar o cabe�alho das p�ginas.
	 * @access protected
	 */
	protected $coWriter = null;

	/**
	 * Seta o writer de PDF.
	 *
	 * <p>Seta o writer de PDF (respons�vel por desenhar o cabe�alho das p�ginas).</p>
	 * @access public
	 */
	public function setWriter(FWDReportPDFWriter $poWriter){
		$this->coWriter = $poWriter;
	}

	/**
	 * Desenha o cabe�alho da p�gina
	 *
	 * <p>Desenha o cabe�alho da p�gina.</p>
	 * @access public
	 */
	public function Header(){
		$this->coWriter->drawHeader();
	}

	/**
	 * Desenha o rodap� da p�gina
	 *
	 * <p>Desenha o rodap� da p�gina.</p>
	 * @access public
	 */
	public function Footer(){
		$this->SetY(-40);
		//Arial italic 8
		$this->SetFont('Arial','',8);
		//Page number
		$this->Cell(0,10,FWDLanguage::getPHPStringValue('fpdf_page',"P�gina").' '.$this->PageNo().'/{nb}',0,0,'R');
	}

	/**
	 * Quebra uma string em quantas linhas for necess�rio.
	 *
	 * <p>Quebra uma string em quantas linhas for necess�rio para, com a fonte
	 * atual, obedecer � restri��o de largura. A quebra � feita introduzindo
	 * caracteres "\n".</p>
	 * @access public
	 * @param string $text String a ser quebrada
	 * @param integer $maxwidth Limite de largura
	 * @return integer N�mero de linhas da string resultante
	 */
	public function wordWrap(&$text, $maxwidth){
		$text = trim($text);
		if($text==='') return 0;
		$space = $this->GetStringWidth(' ');
		$lines = explode("\n", $text);
		$text = '';
		$count = 0;
		foreach($lines as $line){
			$words = preg_split('/ +/', $line);
			$width = 0;
			foreach($words as $word){
				$wordwidth = $this->GetStringWidth($word);
				if($wordwidth > $maxwidth){
					// Word is too long, we cut it
					for($i=0; $i<strlen($word); $i++){
						$wordwidth = $this->GetStringWidth(substr($word, $i, 1));
						if($width + $wordwidth <= $maxwidth){
							$width += $wordwidth;
							$text .= substr($word, $i, 1);
						}else{
							$width = $wordwidth;
							$text = rtrim($text)."\n".substr($word, $i, 1);
							$count++;
						}
					}
				}elseif($width + $wordwidth <= $maxwidth){
					$width += $wordwidth + $space;
					$text .= $word.' ';
				}else{
					$width = $wordwidth + $space;
					$text = rtrim($text)."\n".$word.' ';
					$count++;
				}
			}
			$text = rtrim($text)."\n";
			$count++;
		}
		$text = rtrim($text);
		return $count;
	}

	/**
	 * Retorna o comprimento de uma string
	 *
	 * <p>Retorna o comprimento de uma string considerando a fonte atual.</p>
	 * @access public
	 * @param string $psString String a ser medida
	 * @return integer Comprimento da string
	 */
	public function GetStringWidth($psString){
		$miPogFactor = 0.5;
		return $miPogFactor + parent::GetStringWidth($psString);
	}

}

?>