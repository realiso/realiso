<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe para gera��o de documentos no formato do Excel (xls).
 *
 * <p>Classe que implementa fun��es b�sicas para gere��o de planilhas
 * no excel.</p>
 * @package FWD5
 * @subpackage formats-excel
 */
class FWDExcel {
		
	/**
	 * Nome do arquivo XLS
	 * @var string
	 * @access private
	 */
	private $csfilename;

	/**
	 * Array de FWDExcelStyle
	 * @var array
	 * @access private
	 */
	private $castyles = array();

	/**
	 * Array de FWDExcelWorksheet
	 * @var array
	 * @access private
	 */
	private $caworksheets = array();

	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDExcel.</p>
	 *
	 * @access public
	 * @param string $psfilename Nome do arquivo.
	 */
	public function __construct($psfilename = "") {
		$this->csfilename = $psfilename;
	}

	/**
	 * Seta o nome do arquivo XLS.
	 *
	 * <p>M�todo para setar o nome do arquirvo XLS.</p>
	 *
	 * @access public
	 * @param string $psname Nome do arquivo.
	 */
	public function setFilename($psname) {
		$this->csfilename = $psname;
	}

	/**
	 * Retorna o nome do arquivo XLS.
	 *
	 * <p>M�todo para retornar o nome do arquirvo XLS.</p>
	 *
	 * @access public
	 * @return string Nome do arquivo.
	 */
	public function getFilename() {
		return $this->csfilename;
	}

	/**
	 * Adiciona um estilo ao arquivo XLS.
	 *
	 * <p>M�todo para adicionar um estilo no arquirvo XLS.</p>
	 *
	 * @access public
	 * @param FWDExcelStyle $poStyle Estilo.
	 */
	public function addStyle(FWDExcelStyle $poStyle) {
		$this->castyles[] = $poStyle;
	}

	/**
	 * Adiciona uma planilha ao arquivo XLS.
	 *
	 * <p>M�todo para adicionar uma planilha no arquirvo XLS.</p>
	 *
	 * @access public
	 * @param FWDExcelWorksheet $poWorksheet Planilha.
	 */
	public function addWorksheet(FWDExcelWorksheet $poWorksheet) {
		$this->caworksheets[] = $poWorksheet;
	}

	/**
	 * Gera o arquivo XLS.
	 *
	 * <p>M�todo que gera o arquivo XLS constru�do.</p>
	 *
	 * @access public
	 */
	public function output() {

		$msfilename = $this->csfilename ? $this->csfilename : "excel";

		header('Cache-Control: ');// leave blank to avoid IE errors
		header('Pragma: ');// leave blank to avoid IE errors
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="'.utf8_decode($this->csfilename).'.xls"');

		echo '<?xml version="1.0"?>
			<?mso-application progid="Excel.Sheet"?>
			<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
			xmlns:o="urn:schemas-microsoft-com:office:office"
			xmlns:x="urn:schemas-microsoft-com:office:excel"
			xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
			xmlns:html="http://www.w3.org/TR/REC-html40">';

		if(count($this->castyles) > 0) {
			echo "<Styles>";
			foreach($this->castyles as $mastyle) $mastyle->createTag();
			echo "</Styles>";
		}

		if(count($this->caworksheets) > 0) {
			foreach($this->caworksheets as $maworksheet) $maworksheet->createTag();
		}
		else trigger_error("Excel document must have at least one worksheet",E_USER_ERROR);

		echo '</Workbook>';

		exit();
	}
}
?>