<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe para alinhamento.
 *
 * <p>Classe que representa a tag "Alignment" de um estilo.</p>
 *
 * @package FWD5
 * @subpackage formats-excel
 */
class FWDExcelStyleAlignment {

	/**
	 * Array com os poss�veis valores para o atributo Vertical
	 * @var array
	 * @access private
	 */
	private $caverticalOptions = array("", "Bottom", "Top", "Center", "Justify", "Distributed");

	/**
	 * Array com os poss�veis valores para o atributo Horizontal
	 * @var array
	 * @access private
	 */
	private $cahorizontalOptions = array("", "Left", "Center", "Right", "Fill", "Justify", "Distributed", "CenterAcrossSelection");

	/**
	 * Alinhamento vertical
	 * @var string
	 * @access private
	 */
	private $csvertical = "";

	/**
	 * Alinhamento horizontal
	 * @var string
	 * @access private
	 */
	private $cshorizontal = "";

	/**
	 * Quebra de linha
	 * @var boolean
	 * @access private
	 */
	private $cbwrapText = false;

	/**
	 * �ngulo de rota��o {-90, 90}
	 * @var integer
	 * @access private
	 */
	private $cirotate = 0;
		
	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDExcelStyleAlignment.</p>
	 *
	 * @access public
	 */
	public function __construct() {}

	/**
	 * Seta o alinhamento vertical.
	 *
	 * <p>M�todo para setar o alinhamento vertical do estilo.</p>
	 *
	 * @access public
	 * @param string $psvalign Alinhamento vertical do estilo {Bottom | Top | Center | Justify | Distributed}.
	 */
	public function setVerticalAlignment($psvalign) {
		if(in_array($psvalign, $this->caverticalOptions)) $this->csvertical = $psvalign;
		else trigger_error("Invalid token: \"" . $psvalign . "\"",E_USER_ERROR);

	}

	/**
	 * Retorna o alinhamento vertical.
	 *
	 * <p>M�todo para retornar o alinhamento vertical do estilo.</p>
	 *
	 * @access public
	 * @return string Alinhamento vertical do estilo.
	 */
	public function getVerticalAlignment() {
		return $this->csvertical;
	}

	/**
	 * Seta o alinhamento horizontal.
	 *
	 * <p>M�todo para setar o alinhamento horizontal do estilo.</p>
	 *
	 * @access public
	 * @param string $pshalign Alinhamento horizontal do estilo {Left | Center | Right | Fill | Justify | Distributed | CenterAcrossSelection}.
	 */
	public function setHorizontalAlignment($pshalign) {
		if(in_array($pshalign, $this->cahorizontalOptions)) $this->cshorizontal = $pshalign;
		else trigger_error("Invalid token: \"" . $pshalign . "\"",E_USER_ERROR);
	}

	/**
	 * Retorna o alinhamento horizontal.
	 *
	 * <p>M�todo para retornar o alinhamento horizontal do estilo.</p>
	 *
	 * @access public
	 * @return string Alinhamento horizontal do estilo.
	 */
	public function getHorizontalAlignment() {
		return $this->cshorizontal;
	}

	/**
	 * Seta a op��o de quebra de linha.
	 *
	 * <p>M�todo para setar a op��o de quebra de linha do estilo.</p>
	 *
	 * @access public
	 * @param boolean $pbwrap Quebra de linha do estilo.
	 */
	public function setWrapText($pbwrap) {
		$this->cbwrapText = $pbwrap;
	}

	/**
	 * Retorna a op��o de quebra de linha.
	 *
	 * <p>M�todo para retornar a op��o de quebra de linha do estilo.</p>
	 *
	 * @access public
	 * @return boolean Op��o de quebra de linha do estilo.
	 */
	public function isWrapText() {
		return $this->cbwrapText;
	}

	/**
	 * Seta o �ngulo de rota��o.
	 *
	 * <p>M�todo para setar o �ngulo de rota��o do estilo.</p>
	 *
	 * @access public
	 * @param boolean $pirotate �ngulo de rota��o do estilo.
	 */
	public function setRotate($pirotate) {
		if(($pirotate >= -90) && ($pirotate <= 90)) $this->cirotate = $pirotate;
		else trigger_error("Value ". $pirotate . " out of bounds",E_USER_ERROR);
	}

	/**
	 * Retorna o �ngulo de rota��o.
	 *
	 * <p>M�todo para retornar o �ngulo de rota��o do estilo.</p>
	 *
	 * @access public
	 * @return integer �ngulo de rota��o do estilo.
	 */
	public function getRotate() {
		return $this->cirotate;
	}

	/**
	 * Seta os atributos de alinhamento.
	 *
	 * <p>M�todo para setar o alinhamento vertical e horizontal,
	 * quebra de texto e rota��o.</p>
	 * @access public
	 * @param string $psvalign Alinhamento vertical.
	 * @param string $pshalign Alinhamento horizontal.
	 * @param boolean $pbwrap Quebra de texto (default = false).
	 * @param integer $pirotate Rota��o (default = 0).
	 */
	public function setAttributes($psvalign, $pshalign, $pbwrap = false, $pirotate = 0) {
		$this->setVerticalAlignment($psvalign);
		$this->setHorizontalAlignment($pshalign);
		$this->setWrapText($pbwrap);
		$this->setRotate($pirotate);
	}

	/**
	 * Retorna o xml que representa a tag "Alignment".
	 *
	 * <p>M�todo para retornar o c�digo xml para a tag "Alignment".</p>
	 *
	 * @access public
	 * @return string C�digo xml para tag "Alignment".
	 */
	public function createTag() {
		echo "<Alignment";
		echo $this->cshorizontal ? " ss:Horizontal=\"".$this->cshorizontal."\"" : "";
		echo $this->csvertical ? " ss:Vertical=\"".$this->csvertical."\"" : "";
		echo $this->cbwrapText ? " ss:WrapText=\"".$this->cbwrapText."\"" : "";
		echo $this->cirotate ? " ss:Rotate=\"".$this->cirotate."\"" : "";
		echo "/>";
	}
}
?>