<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe para formatos de n�meros.
 *
 * <p>Classe que representa a tag "NumberFormat" de um estilo.</p>
 *
 * @package FWD5
 * @subpackage formats-excel
 */
class FWDExcelStyleNumberFormat {
		
	/**
	 * Array com os poss�veis valores para moeda
	 * @var array
	 * @access private
	 */
	private $cacurrencyOptions = array("", "Real", "Dollar", "Pound", "Euro");

	/**
	 * Array com os poss�veis valores para o formato de um n�mero
	 * @var array
	 * @access private
	 */
	private $canumberFormatOptions = array("", "General", "Number", "Currency");

	/**
	 * Formato do n�mero
	 * @var string
	 * @access private
	 */
	private $csnumberFormat = "General";

	/**
	 * N�mero de casas decimais
	 * @var string
	 * @access private
	 */
	private $ciprecision = -1;

	/**
	 * Usar ponto para separar 1000 (1.000)
	 * @var boolean
	 * @access private
	 */
	private $cbseparator = false;

	/**
	 * Moeda
	 * @var string
	 * @access private
	 */
	private $cscurrency = "";

	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDExcelStyleNumberFormat.</p>
	 *
	 * @access public
	 */
	public function __construct() {}

	/**
	 * Seta a precis�o do n�mero.
	 *
	 * <p>M�todo para setar a precis�o do n�mero (n�mero de casas
	 * decimais ap�s a v�rgula).</p>
	 *
	 * @access public
	 * @param integer $piprecision Precis�o {0,30}.
	 */
	public function setPrecision($piprecision) {
		if (($piprecision >= 0) && ($piprecision <= 30)) $this->ciprecision = $piprecision;
		else trigger_error("Invalid precision: \"" . $piprecision . "\"",E_USER_ERROR);
	}

	/**
	 * Desabilita a precis�o.
	 *
	 * <p>M�todo para desabilitar a op��o de precis�o de um n�mero.</p>
	 *
	 * @access public
	 */
	public function unsetPrecision() {
		$this->ciprecision = -1;
	}

	/**
	 * Retorna a precis�o do n�mero.
	 *
	 * <p>M�todo para retornar a precis�o do n�mero.</p>
	 *
	 * @access public
	 * @return integer Precis�o do n�mero (-1 = n�o definida).
	 */
	public function getPrecision() {
		return $this->ciprecision;
	}

	/**
	 * Retorna a precis�o do n�mero no formato do xml.
	 *
	 * <p>M�todo para retornar a precis�o do n�mero no formato que vai ser inserido
	 * no xml.</p>
	 *
	 * @access private
	 * @return string Precis�o do n�mero.
	 */
	private function getPrecisionString() {
		$msprecision = "";
		if($this->ciprecision > -1) {
			$msprecision .= "0";
			for($i = 0; $i < $this->ciprecision; $i++) $i ? $msprecision .= "0" : $msprecision .= ".0";
			return $msprecision;
		}
		else return "";
	}

	/**
	 * Habilita o "." como separador.
	 *
	 * <p>M�todo para setar o "." como separador. Ex: 1000 -> 1.000</p>
	 *
	 * @access public
	 * @param integer $pbseparator Separador.
	 */
	public function setSeparator($pbseparator) {
		$this->cbseparator = $pbseparator;
	}

	/**
	 * Retorna se o n�mero � separado por um "." ou n�o.
	 *
	 * <p>M�todo para verificar se o n�mero � separado por um "." ou n�o.</p>
	 *
	 * @access public
	 * @return boolean Possui "." ou n�o.
	 */
	public function isSeparated() {
		return $this->cbseparator;
	}

	/**
	 * Retorna a string referente ao "." no formato xml.
	 *
	 * <p>M�todo para retornar a string referente ao "." no formato xml.</p>
	 *
	 * @access private
	 * @return string String no formato xml do ".".
	 */
	private function getSeparatorString() {
		if ($this->cbseparator) return "#,##";
		else return "";
	}

	/**
	 * Seta a moeda.
	 *
	 * <p>M�todo para setar a moeda que ser� usada.</p>
	 *
	 * @access public
	 * @param string $pscurrency Moeda {Real | Dollar | Pound | Euro}.
	 */
	public function setCurrency($pscurrency) {
		if(in_array($pscurrency, $this->cacurrencyOptions)) $this->cscurrency = $pscurrency;
		else trigger_error("Invalid token: \"" . $pscurrency . "\"",E_USER_ERROR);
	}

	/**
	 * Retorna a moeda.
	 *
	 * <p>M�todo para retornar a moeda que est� sendo usada.</p>
	 *
	 * @access public
	 * @return string Moeda.
	 */
	public function getCurrency() {
		return $this->cscurrency;
	}

	/**
	 * Seta o formato do n�mero.
	 *
	 * <p>M�todo para setar o formato do n�mero.</p>
	 *
	 * @access public
	 * @param string $psnumberFormat Formato {General | Number | Currency}.
	 */
	public function setFormat($psnumberFormat) {
		if(in_array($psnumberFormat, $this->canumberFormatOptions)) $this->csnumberFormat = $psnumberFormat;
		else trigger_error("",E_USER_ERROR);
	}

	/**
	 * Retorna o formato do n�mero.
	 *
	 * <p>M�todo para retornar o formato do n�mero.</p>
	 *
	 * @access public
	 * @return string Formato.
	 */
	public function getFormat() {
		return $this->csnumberFormat;
	}

	/**
	 * Retorna a string que representa a moeda no xml.
	 *
	 * <p>M�todo para retornar a string que representa a moeda no formato xml.</p>
	 *
	 * @access public
	 * @return string Moeda no formato xml.
	 */
	public function getCurrencyString() {
		switch ($this->cscurrency) {
				
			case "Real":
				return "&quot;R$ &quot;";
				break;
					
			case "Dollar":
				return "[$$-409]";
				break;

			case "Pound":
				return "[$£-809]";
				break;
					
			case "Euro":
				return "[$€-2]\ ";
				break;
					
			default:
				return "";
				break;
		}
	}

	/**
	 * Seta os atributos de formato de n�mero.
	 *
	 * <p>M�todo para setar o formato, a moeda, a precis�o e o separador.</p>
	 *
	 * @access public
	 *
	 * @param string $psformat Formato.
	 * @param string $pscurrency Moeda.
	 * @param integer $piprecision Precis�o (default = -1).
	 * @param boolean $pbseparator Separador (default = false).
	 */
	public function setAttributes($psformat, $pscurrency, $piprecision = -1, $pbseparator = false) {
		$this->setFormat($psformat);
		$this->setCurrency($pscurrency);
		$this->setPrecision($piprecision);
		$this->setSeparator($pbseparator);
	}

	/**
	 * Retorna o xml que representa a tag "NumberFormat".
	 *
	 * <p>M�todo para retornar o c�digo xml para a tag "NumberFormat".</p>
	 *
	 * @access public
	 * @return string C�digo xml para tag "NumberFormat".
	 */
	public function createTag() {

		$msformat = "";

		switch($this->csnumberFormat) {
				
			case "Number":
				$msformat = $this->getSeparatorString().$this->getPrecisionString();
				break;
					
			case "Currency":
				if (($mscur = $this->getCurrencyString())) {
					$this->setSeparator(true);
					$msformat = $mscur.$this->getSeparatorString().$this->getPrecisionString();
				}
				else trigger_error("Currency type not defined",E_USER_ERROR);
				break;
					
			default:
				$msformat = "";
				break;
					
		}

		echo "<NumberFormat";
		echo $msformat ? " ss:Format=\"".$msformat."\"" : "";
		echo "/>";
	}
}
?>