<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe que representa uma planilha.
 *
 * <p>Classe que representa a tag "Worksheet" de uma arquivo xml.</p>
 *
 * @package FWD5
 * @subpackage formats-excel
 */
class FWDExcelWorksheet {
		
	/**
	 * Nome da planilha.
	 * @var string
	 * @access private
	 */
	private $csname = "";

	/**
	 * Tabela.
	 * @var FWDExcelWorksheetTable
	 * @access private
	 */
	private $coTable = null;

	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDExcelWorksheet.</p>
	 *
	 * @access public
	 *
	 * @param string $psname Nome da planilha.
	 */
	public function __construct($psname) {
		$this->setName($psname);
	}

	/**
	 * Seta o nome da planilha.
	 *
	 * <p>M�todo para setar o nome da planilha.</p>
	 *
	 * @access public
	 * @param string $psname Nome da planilha.
	 */
	public function setName($psname) {
		if ($psname) $this->csname = $psname;
		else trigger_error("Invalid worksheet name",E_USER_ERROR);
	}

	/**
	 *  Retorna o nome da planilha.
	 *
	 * <p>M�todo para retornar o nome da planilha.</p>
	 *
	 * @access public
	 * @return string Nome da planilha.
	 */
	public function getName() {
		return $this->csname;
	}

	/**
	 * Seta o objeto de tabela da planilha.
	 *
	 * <p>M�todo para setar o objeto de tabela da planilha.</p>
	 *
	 * @access public
	 * @param FWDExcelWorksheetTable $poTable Tabela.
	 */
	public function setTable(FWDExcelWorksheetTable $poTable) {
		$this->coTable = $poTable;
	}

	/**
	 *  Retorna o objeto de tabela da planilha.
	 *
	 * <p>M�todo para retornar o objeto de tabela da planilha.</p>
	 *
	 * @access public
	 * @return FWDExcelWorksheetTable Tabela da planilha.
	 */
	public function getTable() {
		return $this->coTable;
	}
		
	/**
	 * Retorna o xml que representa a tag "Style".
	 *
	 * <p>M�todo para retornar o c�digo xml para a tag "Style".</p>
	 *
	 * @access public
	 * @return string C�digo xml para tag "Style".
	 */
	public function createTag() {
		echo "<Worksheet ss:Name=\"" . $this->csname . "\">";
		if($this->coTable) $this->coTable->createTag();
		echo "</Worksheet>";
	}
}
?>