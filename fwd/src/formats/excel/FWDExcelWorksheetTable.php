<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe que representa uma tabela.
 *
 * <p>Classe que representa a tag "Table" de uma planilha.</p>
 *
 * @package FWD5
 * @subpackage formats-excel
 */
class FWDExcelWorksheetTable {

	/**
	 * Contador de linhas.
	 * @var integer
	 * @access private
	 */
	private $cirowCount = 0;

	/**
	 * Contador de colunas.
	 * @var integer
	 * @access private
	 */
	private $cicolumnCount = 0;

	/**
	 * Array de FWDExcelColumn.
	 * @var array
	 * @access private
	 */
	private $cacolumns = array();

	/**
	 * Array de FWDExcelRow.
	 * @var array
	 * @access private
	 */
	private $carows = array();
		
	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDExcelWorksheetTable.</p>
	 *
	 * @access public
	 */
	public function __construct() {}

	/**
	 * Seta o n�mero de linhas da tabela.
	 *
	 * <p>M�todo para setar o n�mero de linhas da tabela.</p>
	 *
	 * @access public
	 * @param integer $pirowCount N�mero de linhas.
	 */
	public function setRowCount($pirowCount) {
		if ($pirowCount > 0) $this->cirowCount = $pirowCount;
		else trigger_error("Each worksheet must have at least one row",E_USER_ERROR);
	}

	/**
	 *  Retorna o n�mero de linhas da tabela.
	 *
	 * <p>M�todo para retornar o n�mero de linhas da tabela.</p>
	 *
	 * @access public
	 * @return integer N�mero de linhas.
	 */
	public function getRowCount() {
		return $this->cirowCount;
	}

	/**
	 * Seta o n�mero de colunas da tabela.
	 *
	 * <p>M�todo para setar o n�mero de colunas da tabela.</p>
	 *
	 * @access public
	 * @param integer $picolumnCount N�mero de colunas.
	 */
	public function setColumnCount($picolumnCount) {
		if ($picolumnCount > 0) $this->cicolumnCount = $picolumnCount;
		else trigger_error("Each worksheet must have at least one column",E_USER_ERROR);
	}

	/**
	 *  Retorna o n�mero de colunas da tabela.
	 *
	 * <p>M�todo para retornar o n�mero de colunas da tabela.</p>
	 *
	 * @access public
	 * @return integer N�mero de colunas.
	 */
	public function getColumnCount() {
		return $this->cicolumnCount;
	}

	/**
	 * Adiciona uma coluna na tabela.
	 *
	 * <p>M�todo para adicionar uma coluna na tabela.</p>
	 *
	 * @access public
	 * @param FWDExcelColumn $pocolumn Coluna.
	 */
	public function addColumn(FWDExcelColumn $pocolumn) {
		$this->cacolumns[] = $pocolumn;
		$this->setColumnCount($this->getColumnCount()+1);
	}

	/**
	 * Adiciona um determinado n�mero de colunas.
	 *
	 * <p>M�todo para adicionar um determinado n�mero de colunas na tabela.</p>
	 *
	 * @access public
	 *
	 * @param array $picolParameters Array com a largura de cada coluna.
	 */
	public function addColumns($picolParameters) {
		for($i=0; $i < count($picolParameters); $i++)
		$this->cacolumns[] = new FWDExcelColumn($picolParameters[$i]);
		$this->setColumnCount($this->getColumnCount()+count($picolParameters));
	}

	/**
	 * Adiciona uma linha na tabela.
	 *
	 * <p>M�todo para adicionar uma linha na tabela.</p>
	 *
	 * @access public
	 * @param FWDExcelRow $porow Linha.
	 */
	public function addRow(FWDExcelRow $porow) {
		$this->carows[] = $porow;
		$this->setRowCount($this->getRowCount()+1);
	}

	/**
	 * Retorna o xml que representa a tag "Table".
	 *
	 * <p>M�todo para retornar o c�digo xml para a tag "Table".</p>
	 *
	 * @access public
	 * @return string C�digo xml para tag "Table".
	 */
	public function createTag() {

		echo "<Table ss:ExpandedColumnCount=\"" . $this->getColumnCount() . "\" ss:ExpandedRowCount=\"" . $this->getRowCount() . "\">";

		foreach($this->cacolumns as $column) $column->createTag();
		$msStyleArray = array();
		foreach($this->carows as $row) {
			foreach($this->cacolumns as $column) $msStyleArray[] = $column->getCellStyleId();
			$row->setColumnsStyleIds($msStyleArray);
			$row->createTag();
		}

		echo "</Table>";

	}
}
?>