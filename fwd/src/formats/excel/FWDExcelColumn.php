<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe que representa uma coluna de uma tabela.
 *
 * <p>Classe que representa a tag "Column" de uma tabela.</p>
 *
 * @package FWD5
 * @subpackage formats-excel
 */
class FWDExcelColumn {

	/**
	 * Largura.
	 * @var integer
	 * @access private
	 */
	private $ciwidth = 0;

	/**
	 * Id do estilo.
	 * @var string
	 * @access private
	 */
	private $csstyleId = "";

	/**
	 * Id do estilo da c�lula.
	 * @var string
	 * @access private
	 */
	private $cscellStyleId = "";
		
	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDExcelColumn.</p>
	 *
	 * @access public
	 *
	 * @param integer $piwidth Largura (default = 150).
	 */
	public function __construct($piwidth = 150) {
		$this->setWidth($piwidth);
	}

	/**
	 * Seta a largura da coluna.
	 *
	 * <p>M�todo para setar a largura da coluna.</p>
	 *
	 * @access public
	 * @param integer $piwidth Largura.
	 */
	public function setWidth($piwidth) {
		if ($piwidth > 0) $this->ciwidth = $piwidth;
		else trigger_error("Invalid width: ",E_USER_ERROR);
			
	}

	/**
	 * Retorna a largura da coluna.
	 *
	 * <p>M�todo para retornar a largura da coluna.</p>
	 *
	 * @access public
	 *
	 * @return integer Largura.
	 */
	public function getWidth() {
		return $this->ciwidth;
	}

	/**
	 * Seta o identificador do estilo da coluna.
	 *
	 * <p>M�todo para setar o identificador do estilo da coluna.
	 * Todas as c�lulas dessa coluna receber�o esse estilo (n�o apenas
	 * as c�lulas preenchidas).</p>
	 *
	 * @access public
	 * @param string $psstyleId Id do estilo.
	 */
	public function setStyleId($psstyleId) {
		$this->csstyleId = $psstyleId;
	}

	/**
	 * Retorna o identificador do estilo.
	 *
	 * <p>M�todo para retornar o identificador do estilo.</p>
	 *
	 * @access public
	 * @return string Identificador do estilo.
	 */
	public function getStyleId() {
		return $this->csstyleId;
	}

	/**
	 * Seta o identificador do estilo das c�lulas.
	 *
	 * <p>M�todo para setar o identificador do estilo das c�lulas. Somente
	 * as c�lulas preenchidas receber�o o estilo.</p>
	 *
	 * @access public
	 * @param string $pscellStyleId Id do estilo das c�lulas.
	 */
	public function setCellStyleId($pscellStyleId) {
		$this->cscellStyleId = $pscellStyleId;
	}

	/**
	 * Retorna o identificador do estilo das c�lulas.
	 *
	 * <p>M�todo para retornar o identificador do estilo das c�lulas.</p>
	 *
	 * @access public
	 * @return string Identificador do estilo das c�lulas.
	 */
	public function getCellStyleId() {
		return $this->cscellStyleId;
	}

	/**
	 * Retorna o xml que representa a tag "Column".
	 *
	 * <p>M�todo para retornar o c�digo xml para a tag "Column".</p>
	 *
	 * @access public
	 * @return string C�digo xml para tag "Column".
	 */
	public function createTag() {
		echo "<Column";
		echo $this->ciwidth ? " ss:Width=\"".$this->ciwidth."\"" : "";
		echo $this->csstyleId ? " ss:StyleID=\"".$this->csstyleId."\"" : "";
		echo "/>";
	}
}
?>