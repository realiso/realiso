<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe para fonte.
 *
 * <p>Classe que representa a tag "Font" de um estilo.</p>
 *
 * @package FWD5
 * @subpackage formats-excel
 */
class FWDExcelStyleFont {

	/**
	 * Array com os poss�veis valores para o atributo Underline
	 * @var array
	 * @access private
	 */
	private $caunderlineOptions = array("", "Single", "Double", "SingleAccounting", "DoubleAccounting");

	/**
	 * Array com os poss�veis valores para o atributo VerticalAlign
	 * @var array
	 * @access private
	 */
	private $caverticalAlignOptions = array("", "Subscript", "Superscript");
		
	/**
	 * Nome da fonte.
	 * @var string
	 * @access private
	 */
	private $csname = "";

	/**
	 * Tipo de sublinhado.
	 * @var string
	 * @access private
	 */
	private $csunderline = "";

	/**
	 * Tipo alinhamento vertical.
	 * @var string
	 * @access private
	 */
	private $csverticalAlign = "";

	/**
	 * Negrito.
	 * @var boolean
	 * @access private
	 */
	private $cbbold = false;

	/**
	 * It�lico.
	 * @var boolean
	 * @access private
	 */
	private $cbitalic = false;

	/**
	 * Tachado.
	 * @var boolean
	 * @access private
	 */
	private $cbstrikeThrough = false;

	/**
	 * Cor da fonte.
	 * @var string
	 * @access private
	 */
	private $cscolor = "";

	/**
	 * Tamanho da fonte.
	 * @var integer
	 * @access private
	 */
	private $cisize = 0;

	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDExcelStyleFont.</p>
	 *
	 * @access public
	 */
	public function __construct() {}

	/**
	 * Seta o nome da fonte.
	 *
	 * <p>M�todo para setar o nome da fonte.</p>
	 *
	 * @access public
	 * @param string $psname Nome da fonte.
	 */
	public function setName($psname) {
		$this->csname = $psname;
	}

	/**
	 * Retorna o nome da fonte.
	 *
	 * <p>M�todo para retornar o nome da fonte.</p>
	 *
	 * @access public
	 * @return string Nome da fonte.
	 */
	public function getName() {
		return $this->csname;
	}

	/**
	 * Seta estilo da fonte como negrito.
	 *
	 * <p>M�todo para setar o estilo da fonte como negrito.</p>
	 *
	 * @access public
	 * @param boolean $pbbold Negrito.
	 */
	public function setBold($pbbold) {
		$this->cbbold = $pbbold;
	}

	/**
	 * Retorna o estilo da fonte (negrito ou n�o).
	 *
	 * <p>M�todo para retornar o estilo da fonte (negrito ou n�o).</p>
	 *
	 * @access public
	 * @return string Estilo da fonte (negrito ou n�o).
	 */
	public function isBold() {
		return $this->cbbold;
	}

	/**
	 * Seta estilo da fonte como it�lico.
	 *
	 * <p>M�todo para setar o estilo da fonte como it�lico.</p>
	 *
	 * @access public
	 * @param boolean $pbitalic It�lico.
	 */
	public function setItalic($pbitalic) {
		$this->cbitalic = $pbitalic;
	}

	/**
	 * Retorna o estilo da fonte (it�lico ou n�o).
	 *
	 * <p>M�todo para retornar o estilo da fonte (it�lico ou n�o).</p>
	 *
	 * @access public
	 * @return string Estilo da fonte (it�lico ou n�o).
	 */
	public function isItalic() {
		return $this->cbitalic;
	}

	/**
	 * Seta o tipo de sublinhado.
	 *
	 * <p>M�todo para setar o tipo de sublinhado.</p>
	 *
	 * @access public
	 * @param string $psunderline Tipo de sublinhado {Single | Double | SingleAccounting | DoubleAccounting}.
	 */
	public function setUnderline($psunderline) {
		if(in_array($psunderline, $this->caunderlineOptions)) $this->csunderline = $psunderline;
		else trigger_error("Invalid token: \"" . $psunderline . "\"",E_USER_ERROR);
	}

	/**
	 * Retorna o tipo de sublinhado.
	 *
	 * <p>M�todo para retornar o tipo de sublinhado.</p>
	 *
	 * @access public
	 * @return string Tipo de sublinhado.
	 */
	public function getUnderline() {
		return $this->csunderline;
	}

	/**
	 * Seta o tipo de alinhamento vertical.
	 *
	 * <p>M�todo para setar o tipo de alinhamento vertical.</p>
	 *
	 * @access public
	 * @param string $psverticalAlign Tipo de alinhamento vertical {Subscript | Superscript}.
	 */
	public function setVerticalAlign($psverticalAlign) {
		if(in_array($psverticalAlign, $this->caverticalAlignOptions)) $this->csverticalAlign = $psverticalAlign;
		else trigger_error("Invalid token: \"" . $psverticalAlign . "\"",E_USER_ERROR);
	}

	/**
	 * Retorna o tipo de alinhamento vertical.
	 *
	 * <p>M�todo para retornar o tipo de alinhamento vertical.</p>
	 *
	 * @access public
	 * @return string Tipo de alinhamento vertical.
	 */
	public function getVerticalAlign() {
		return $this->csverticalAlign;
	}

	/**
	 * Seta estilo da fonte como tachada.
	 *
	 * <p>M�todo para setar o estilo da fonte como tachada.</p>
	 *
	 * @access public
	 * @param boolean $pbstrikeThrough Tachado.
	 */
	public function setStrikeThrough($pbstrikeThrough) {
		$this->cbstrikeThrough = $pbstrikeThrough;
	}

	/**
	 * Retorna o estilo da fonte (tachado ou n�o).
	 *
	 * <p>M�todo para retornar o estilo da fonte (tachado ou n�o).</p>
	 *
	 * @access public
	 * @return string Estilo da fonte (tachado ou n�o).
	 */
	public function isStrikeThrough() {
		return $this->cbstrikeThrough;
	}

	/**
	 * Seta a cor da fonte.
	 *
	 * <p>M�todo para setar a cor da fonte.</p>
	 *
	 * @access public
	 * @param integer $piR Cor da fonte (Vermelho).
	 * @param integer $piG Cor da fonte (Verde).
	 * @param integer $piB Cor da fonte (Azul).
	 */
	public function setColor($piR, $piG, $piB) {
		if(($mscolor = FWDExcelColors::color($piR, $piG, $piB))) $this->cscolor = $mscolor;
		else trigger_error("Invalid rgb color: \"" . "($piR,$piG,$piB)" .  "\"",E_USER_ERROR);
	}

	/**
	 * Retorna a cor da fonte.
	 *
	 * <p>M�todo para retornar a cor da fonte.</p>
	 *
	 * @access public
	 * @return string Cor da fonte.
	 */
	public function getColor() {
		return $this->cscolor;
	}

	/**
	 * Seta o tamanho da fonte.
	 *
	 * <p>M�todo para setar o tamanho da fonte.</p>
	 *
	 * @access public
	 * @param integer $pisize Tamanho da fonte.
	 */
	public function setSize($pisize) {
		if ($pisize > 0) $this->cisize = $pisize;
		else trigger_error("Invalid font size: " . $pisize,E_USER_ERROR);
	}

	/**
	 * Retorna o tamanho da fonte.
	 *
	 * <p>M�todo para retornar o tamanho da fonte.</p>
	 *
	 * @access public
	 * @return integer Tamanho da fonte.
	 */
	public function getSize() {
		return $this->cisize;
	}

	/**
	 * Seta os atributos de fonte.
	 *
	 * <p>M�todo para setar o nome, a cor, o tamanho,
	 * o tipo de sublinhamento, negrito, it�lico, tachado.</p>
	 * @access public
	 *
	 * @param string $psname Nome da fonte.
	 * @param integer $pisize Tamanho da fonte.
	 * @param array $pacolor Cor da fonte (array(R,G,B)).
	 * @param string $psunderline Tipo de sublinhamento (default = "").
	 * @param string $psverticalAlign Alinhamento vertical (default = "").
	 * @param boolean $pbbold Negrito (default = false).
	 * @param boolean $pbitalic It�lico (default = false).
	 * @param boolean $pbstrike Tachado (default = false).
	 */
	public function setAttributes($psname, $pisize, $pacolor, $psunderline = "", $psverticalAlign = "", $pbbold = false, $pbitalic = false, $pbstrike = false) {
		$this->setName($psname);
		$this->setSize($pisize);
		if((is_array($pacolor)) && (count($pacolor) == 3)) $this->setColor($pacolor[0], $pacolor[1], $pacolor[2]);
		else trigger_error("Invalid parameter: pacolor must be an array with 3 elements",E_USER_ERROR);
		$this->setUnderline($psunderline);
		$this->setVerticalAlign($psverticalAlign);
		$this->setBold($pbbold);
		$this->setItalic($pbitalic);
		$this->setStrikeThrough($pbstrike);
	}
		
	/**
	 * Retorna o xml que representa a tag "Font".
	 *
	 * <p>M�todo para retornar o c�digo xml para a tag "Font".</p>
	 *
	 * @access public
	 * @return string C�digo xml para tag "Font".
	 */
	public function createTag() {

		echo "<Font";
		echo $this->csname ? " ss:FontName=\"".$this->csname."\"" : "";
		echo $this->cbbold ? " ss:Bold=\"".$this->cbbold."\"" : "";
		echo $this->cbitalic ? " ss:Italic=\"".$this->cbitalic."\"" : "";
		echo $this->cbstrikeThrough ? " ss:StrikeThrough=\"".$this->cbstrikeThrough."\"" : "";
		echo $this->csunderline ? " ss:Underline=\"".$this->csunderline."\"" : "";
		echo $this->csverticalAlign ? " ss:VerticalAlign=\"".$this->csverticalAlign."\"" : "";
		echo $this->cscolor ? " ss:Color=\"".$this->cscolor."\"" : "";
		echo $this->cisize ? " ss:Size=\"".$this->cisize."\"" : "";
		echo "/>";
	}
}
?>