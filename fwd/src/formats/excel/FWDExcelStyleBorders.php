<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe para bordas.
 *
 * <p>Classe que representa a tag "Borders" de um estilo.</p>
 *
 * @package FWD5
 * @subpackage formats-excel
 */
class FWDExcelStyleBorders {

	/**
	 * Array de FWDExcelStyleBorder.
	 * @var array
	 * @access private
	 */
	private $caBorders = array();

	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDExcelStyleBorder.</p>
	 *
	 * @access public
	 */
	public function __construct() {}

	/**
	 * Seta as bordas externas do estilo.
	 *
	 * <p>M�todo para setar as bordas externas do estilo</p>
	 *
	 * @param string $psline Tipo de linha.
	 * @param array $pacolor Cor (array (R,G,B)).
	 */
	public function setExternalBorders($psline, $pacolor) {
		$this->caBorders["Left"] = new FWDExcelStyleBorder();
		$this->caBorders["Right"] = new FWDExcelStyleBorder();
		$this->caBorders["Top"] = new FWDExcelStyleBorder();
		$this->caBorders["Bottom"] = new FWDExcelStyleBorder();

		$this->caBorders["Left"]->setAttributes("Left", $psline, $pacolor);
		$this->caBorders["Right"]->setAttributes("Right", $psline, $pacolor);
		$this->caBorders["Top"]->setAttributes("Top", $psline, $pacolor);
		$this->caBorders["Bottom"]->setAttributes("Bottom", $psline, $pacolor);
	}

	/**
	 * Seta uma borda espec�fica do estilo.
	 *
	 * <p>M�todo para setar uma borda espec�fica do estilo.</p>
	 *
	 * @param string $psposition Posi��o.
	 * @param string $psline Tipo de linha.
	 * @param array $pacolor Cor (array (R,G,B)).
	 */
	public function setBorder($psposition, $psline, $pacolor) {
		$this->caBorders[$psposition] = new FWDExcelStyleBorder();
		$this->caBorders[$psposition]->setLine($psline);
		$this->caBorders[$psposition]->setColor($pacolor);
	}

	/**
	 * Retorna uma borda espec�fica do estilo.
	 *
	 * <p>M�todo para retorna uma borda espec�fica do estilo.</p>
	 *
	 * @param string $psposition Posi��o da borda.
	 *
	 * @return FWDExcelStyleBorder
	 */
	public function getBorder($psposition) {
		if (array_key_exists($psposition, $this->caBorders))
		return $this->caBorders[$psposition];
		else trigger_error("Invalid border position: $psposition",E_USER_ERROR);
	}

	/**
	 * Retorna todas as bordas do estilo.
	 *
	 * <p>M�todo para retorna todas as bordas do estilo.</p>
	 *
	 * @param string $psposition Posi��o.
	 *
	 * @return array Array com todas as bordas do estilo (FWDExcelStyleBorder).
	 */
	public function getBorders($psposition) {
		return $this->caBorders[$psposition];
	}

	/**
	 * Retorna o xml que representa a tag "Borders".
	 *
	 * <p>M�todo para retornar o c�digo xml para a tag "Borders".</p>
	 *
	 * @access public
	 * @return string C�digo xml para tag "Borders".
	 */
	public function createTag() {
		echo "<Borders>";
		foreach($this->caBorders as $msborder) $msborder->createTag();
		echo "</Borders>";
	}
}
?>