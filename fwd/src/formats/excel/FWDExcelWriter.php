<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe para gera��o eficiente de documentos no formato do Excel (xls).
 *
 * <p>Classe mais restrita, mas mais eficiente que a classe FWDExcel na gera��o
 * de planilhas no formato xls.</p>
 * @package FWD5
 * @subpackage formats-excel
 */
class FWDExcelWriter {

	/**
	 * Nome do arquivo XLS
	 * @var string
	 * @access private
	 */
	private $csFilename;

	/**
	 * Array de FWDExcelStyle
	 * @var array
	 * @access private
	 */
	private $caStyles = array();

	/**
	 * Array com as larguras das colunas da WorkSheet atual
	 * @var array
	 * @access private
	 */
	private $caColumnsWidths;

	/**
	 * Nome da WorkSheet atual
	 * @var string
	 * @access private
	 */
	private $csWorksheetName;

	/**
	 * Indica se h� uma WorkSheet aberta
	 * @var boolean
	 * @access private
	 */
	private $cbOpenWorksheet = false;

	/**
	 * Indica se o arquivo j� come�ou a ser escrito
	 * @var boolean
	 * @access private
	 */
	private $cbFileOutputStarted = false;

	/**
	 * Indica se a WorkSheet j� come�ou a ser escrita
	 * @var boolean
	 * @access private
	 */
	private $cbWorksheetOutputStarted = false;

	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDExcelWriter.</p>
	 *
	 * @access public
	 * @param string $psfilename Nome do arquivo.
	 */
	public function __construct($psFileName = ""){
		$this->csFilename = strtr($psFileName,'\/:*?"<>|','_________');
	}

	/**
	 * Retorna o nome do arquivo XLS.
	 *
	 * <p>M�todo para retornar o nome do arquirvo XLS.</p>
	 *
	 * @access public
	 * @return string Nome do arquivo.
	 */
	public function getFilename(){
		return $this->csFilename;
	}

	/**
	 * Adiciona um estilo ao arquivo XLS.
	 *
	 * <p>M�todo para adicionar um estilo no arquirvo XLS.</p>
	 *
	 * @access public
	 * @param FWDExcelStyle $poStyle Estilo.
	 */
	public function addStyle(FWDExcelStyle $poStyle){
		$this->caStyles[] = $poStyle;
	}

	/**
	 * Abre uma nova WorkSheet
	 *
	 * <p>Abre uma nova WorkSheet. Se j� existia uma aberta, ela � fechada.</p>
	 * @access public
	 * @param string $psName Nome da nova WorkSheet
	 */
	public function openWorksheet($psName){
		if($this->cbOpenWorksheet) $this->closeWorksheet();
		
		if(strlen($psName) > 30)
			$this->csWorksheetName = substr($psName, 0, 27) . "...";
		else
			$this->csWorksheetName = $psName;
		
		$this->caColumnsWidths = array();
		$this->cbOpenWorksheet = true;
	}

	/**
	 * Fecha a WorkSheet atual
	 *
	 * <p>Fecha a WorkSheet atual.</p>
	 * @access public
	 */
	public function closeWorksheet(){
		if($this->cbOpenWorksheet){
			echo '</Table></Worksheet>';
			$this->cbOpenWorksheet = false;
			$this->cbWorksheetOutputStarted = false;
		}else{
			trigger_error('There is no open WorkSheet.',E_USER_WARNING);
		}
	}

	/**
	 * Adiciona uma coluna � WorkSheet atual
	 *
	 * <p>Adiciona uma coluna � WorkSheet atual.</p>
	 * @access public
	 * @param integer $piWidth Largura da coluna
	 */
	public function addColumn($piWidth){
		if($this->cbWorksheetOutputStarted){
			trigger_error('All columns must be added before first row is drawn.',E_USER_ERROR);
		}else{
			$this->caColumnsWidths[] = $piWidth;
		}
	}

	/**
	 * Adiciona uma linha � WorkSheet atual
	 *
	 * <p>Adiciona uma linha � WorkSheet atual.</p>
	 * @access public
	 * @param FWDExcelRow $poRow Linha a ser adicionada
	 */
	public function drawRow(FWDExcelRow $poRow){
		if($this->cbOpenWorksheet){
			if(!$this->cbFileOutputStarted){
				$this->drawHeader();
			}
			if(!$this->cbWorksheetOutputStarted){
				$this->drawWorksheetHeader();
			}
			$poRow->createTag();
		}else{
			trigger_error('To draw a row a WorkSheet must be open.',E_USER_ERROR);
		}
	}

	/**
	 * Desenha o cabe�alho do arquivo
	 *
	 * <p>Desenha o cabe�alho do arquivo.</p>
	 * @access private
	 */
	private function drawHeader(){
		$msFilename = $this->csFilename ? $this->csFilename : "excel";
		header('Cache-Control: ');// leave blank to avoid IE errors
		header('Pragma: ');// leave blank to avoid IE errors
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="'.utf8_decode($msFilename).'.xls"');
		echo '<?xml version="1.0" encoding="UTF-8"?>'
		.'<?mso-application progid="Excel.Sheet"?>'
		.'<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"'
		.' xmlns:o="urn:schemas-microsoft-com:office:office"'
		.' xmlns:x="urn:schemas-microsoft-com:office:excel"'
		.' xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"'
		.' xmlns:html="http://www.w3.org/TR/REC-html40">';
		if(count($this->caStyles) > 0){
			echo "<Styles>";
			foreach($this->caStyles as $moStyle) $moStyle->createTag();
			echo "</Styles>";
		}
		$this->cbFileOutputStarted = true;
	}

	/**
	 * Desenha o cabe�alho da WorkSheet atual
	 *
	 * <p>Desenha o cabe�alho da WorkSheet atual.</p>
	 * @access private
	 */
	private function drawWorksheetHeader(){
		echo "<Worksheet ss:Name=\"{$this->csWorksheetName}\"><Table>";
		foreach($this->caColumnsWidths as $miWidth){
			echo "<Column ss:Width=\"$miWidth\"/>";
		}
		$this->cbWorksheetOutputStarted = true;
	}

	/**
	 * Encerra o arquivo
	 *
	 * <p>Encerra o arquivo.</p>
	 * @access public
	 */
	public function endFile(){
		if($this->cbOpenWorksheet) $this->closeWorksheet();
		echo '</Workbook>';
		exit();
	}

}
?>