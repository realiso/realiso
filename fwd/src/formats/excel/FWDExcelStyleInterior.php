<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe para sombreamento.
 *
 * <p>Classe que representa a tag "Interior" de um estilo.</p>
 *
 * @package FWD5
 * @subpackage formats-excel
 */
class FWDExcelStyleInterior {

	/**
	 * Array com os poss�veis valores para o atributo Pattern
	 * @var array
	 * @access private
	 */
	private $capatternOptions = array("", "Solid", "Gray75", "Gray50", "Gray25", "Gray125", "Gray0625",
										"HorzStripe", "VertStripe", "ReverseDiagStripe", "DiagStripe",
										"DiagCross", "ThickDiagCross", "ThinHorzStripe", "ThinVertStripe",
										"ThinReverseDiagStripe", "ThinDiagStripe", "ThinHorzCross","ThinDiagCross");	

	/**
	 * Padr�o.
	 * @var string
	 * @access private
	 */
	private $cspattern = "";

	/**
	 * Cor do fundo.
	 * @var string
	 * @access private
	 */
	private $csbgColor = "";

	/**
	 * Cor do padr�o.
	 * @var string
	 * @access private
	 */
	private $cspatternColor = "";

	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDExcelStyleInterior.</p>
	 *
	 * @access public
	 */
	public function __construct() {}

	/**
	 * Seta o padr�o.
	 *
	 * <p>M�todo para setar o padr�o.</p>
	 *
	 * @access public
	 * @param string $pspattern Padr�o {Solid | Gray75 | Gray50 | Gray25 | Gray125 | Gray0625 |
	 * 									HorzStripe | VertStripe | ReverseDiagStripe | DiagStripe |
	 * 									DiagCross | ThickDiagCross | ThinHorzStripe | ThinVertStripe |
	 * 									ThinReverseDiagStripe | ThinDiagStripe | ThinHorzCross |ThinDiagCross}.
	 */
	public function setPattern($pspattern) {
		if(in_array($pspattern, $this->capatternOptions)) $this->cspattern = $pspattern;
		else trigger_error("Invalid token: \"" . $pspattern . "\"",E_USER_ERROR);
	}

	/**
	 * Retorna o padr�o.
	 *
	 * <p>M�todo para retornar o padr�o.</p>
	 *
	 * @access public
	 * @return string Padr�o.
	 */
	public function getPattern() {
		return $this->cspattern;
	}

	/**
	 * Seta a cor do fundo.
	 *
	 * <p>M�todo para setar a cor do fundo.</p>
	 *
	 * @access public
	 * @param integer $piR Cor do fundo (Vermelho).
	 * @param integer $piG Cor do fundo (Verde).
	 * @param integer $piB Cor do fundo (Azul).
	 */
	public function setBgColor($piR, $piG, $piB) {
		if(($mscolor = FWDExcelColors::color($piR, $piG, $piB))) $this->csbgColor = $mscolor;
		else trigger_error("Invalid rgb color: \"" . "($piR,$piG,$piB)" .  "\"",E_USER_ERROR);
	}

	/**
	 * Retorna a cor do fundo.
	 *
	 * <p>M�todo para retornar a cor do fundo.</p>
	 *
	 * @access public
	 * @return string Cor do fundo.
	 */
	public function getBgColor() {
		return $this->csbgColor;
	}

	/**
	 * Seta a cor do padr�o.
	 *
	 * <p>M�todo para setar a cor do padr�o.</p>
	 *
	 * @access public
	 * @param integer $piR Cor do padr�o (Vermelho).
	 * @param integer $piG Cor do padr�o (Verde).
	 * @param integer $piB Cor do padr�o (Azul).
	 */
	public function setPatternColor($piR, $piG, $piB) {
		if(($mscolor = FWDExcelColors::color($piR, $piG, $piB))) $this->cspatternColor = $mscolor;
		else trigger_error("Invalid rgb color: \"" . "($piR,$piG,$piB)" .  "\"",E_USER_ERROR);
	}

	/**
	 * Retorna a cor do padr�o.
	 *
	 * <p>M�todo para retornar a cor do padr�o.</p>
	 *
	 * @access public
	 * @return string Cor do padr�o.
	 */
	public function getPatternColor() {
		return $this->cspatternColor;
	}

	/**
	 * Seta os atributos de interior.
	 *
	 * <p>M�todo para setar a cor do fundo, o nome do padr�o e
	 * a cor do padr�o.</p>
	 *
	 * @access public
	 *
	 * @param array $pabgColor Cor do fundo (array(R,G,B)).
	 * @param string $pspattern Nome do padr�o.
	 * @param array $papatternColor Cor do padr�o (array(R,G,B)).
	 */
	public function setAttributes($pabgColor, $pspattern, $papatternColor) {
		$this->setPattern($pspattern);

		if((is_array($pabgColor)) && (count($pabgColor) == 3)) $this->setBgColor($pabgColor[0], $pabgColor[1], $pabgColor[2]);
		else trigger_error("Invalid parameter: pabgColor must be an array with 3 elements",E_USER_ERROR);

		if((is_array($papatternColor)) && (count($papatternColor) == 3)) $this->setPatternColor($papatternColor[0], $papatternColor[1], $papatternColor[2]);
		else trigger_error("Invalid parameter: papatternColor must be an array with 3 elements",E_USER_ERROR);
	}
		
	/**
	 * Retorna o xml que representa a tag "Interior".
	 *
	 * <p>M�todo para retornar o c�digo xml para a tag "Interior".</p>
	 *
	 * @access public
	 * @return string C�digo xml para tag "Interior".
	 */
	public function createTag() {

		echo "<Interior";
		echo $this->csbgColor ? " ss:Color=\"".$this->csbgColor."\"" : "";
		echo $this->cspattern ? " ss:Pattern=\"".$this->cspattern."\"" : "";
		echo $this->cspatternColor ? " ss:PatternColor=\"".$this->cspatternColor."\"" : "";
		echo "/>";
	}
}
?>