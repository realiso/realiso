<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe que representa uma c�lula.
 *
 * <p>Classe que representa a tag "Cell" de uma linha.</p>
 *
 * @package FWD5
 * @subpackage formats-excel
 */
class FWDExcelCell {

	/**
	 * Array com os poss�veis valores para o tipo da c�lula.
	 * @var array
	 * @access private
	 */
	private $catypeOptions = array("", "Number", "String");

	/**
	 * Id do estilo.
	 * @var string
	 * @access private
	 */
	private $csstyleId = "";

	/**
	 * Tipo.
	 * @var string
	 * @access private
	 */
	private $cstype = "";

	/**
	 * Valor.
	 * @var string
	 * @access private
	 */
	private $csvalue = "";

	/**
	 * Link.
	 * @var string
	 * @access private
	 */
	private $cslink = "";
		
	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDExcelCell.</p>
	 *
	 * @access public
	 *
	 * @param string $psvalue Valor.
	 */
	public function __construct($psvalue) {
		$this->setValue($psvalue);
	}

	/**
	 * Seta o identificador do estilo da c�lula.
	 *
	 * <p>M�todo para setar o identificador do estilo da c�lula.</p>
	 *
	 * @access public
	 * @param string $psstyleId Id do estilo.
	 */
	public function setStyleId($psstyleId) {
		$this->csstyleId = $psstyleId;
	}

	/**
	 * Retorna o identificador do estilo da c�lula.
	 *
	 * <p>M�todo para retornar o identificador do estilo da c�lula.</p>
	 *
	 * @access public
	 * @param string Identificador do estilo.
	 */
	public function getStyleId() {
		return $this->csstyleId;
	}

	/**
	 * Seta o tipo da c�lula.
	 *
	 * <p>M�todo para setar o tipo da c�lula.</p>
	 *
	 * @access public
	 * @param string $pstype Tipo {Number | String}.
	 */
	public function setType($pstype) {
		if (in_array($pstype, $this->catypeOptions)) $this->cstype = $pstype;
		else trigger_error("Invalid cell type: ",E_USER_ERROR);
	}

	/**
	 * Retorna o tipo da c�lula.
	 *
	 * <p>M�todo para retornar o tipo da c�lula.</p>
	 *
	 * @access public
	 * @param string Tipo da c�lula.
	 */
	public function getType() {
		return $this->cstype;
	}

	/**
	 * Seta o valor da c�lula.
	 *
	 * <p>M�todo para setar o valor da c�lula. Automaticamente seta o tipo da c�lula
	 * para "Number" ou "String".</p>
	 *
	 * @access public
	 * @param string $psvalue Valor.
	 */
	public function setValue($psvalue) {
		$this->csvalue = $psvalue;
		if(is_numeric($psvalue)) $this->setType("Number");
		else $this->setType("String");
	}

	/**
	 * Retorna o valor da c�lula.
	 *
	 * <p>M�todo para retornar o valor da c�lula.</p>
	 *
	 * @access public
	 * @param string Valor da c�lula.
	 */
	public function getValue() {
		return $this->csvalue;
	}

	/**
	 * Seta o link.
	 *
	 * <p>M�todo para setar o link.</p>
	 *
	 * @access public
	 * @param string $pslink Link.
	 */
	public function setLink($pslink) {
		$this->cslink = $pslink;
	}

	/**
	 * Retorna o link.
	 *
	 * <p>M�todo para retornar o link.</p>
	 *
	 * @access public
	 * @param string Link.
	 */
	public function getLink() {
		return $this->cslink;
	}

	/**
	 * Retorna o xml que representa a tag "Cell".
	 *
	 * <p>M�todo para retornar o c�digo xml para a tag "Cell".</p>
	 *
	 * @access public
	 * @return string C�digo xml para tag "Cell".
	 */
	public function createTag() {
		echo "<Cell";
		echo $this->csstyleId ? " ss:StyleID=\"".$this->csstyleId."\"" : "";
		echo $this->cslink ? " ss:HRef=\"".$this->cslink."\"" : "";
		echo ">";

		echo "<Data";
		echo $this->cstype ? " ss:Type=\"".$this->cstype."\"" : "";
		echo ">";
		echo $this->csvalue;
		echo "</Data>";
			
		echo "</Cell>";
	}
}
?>