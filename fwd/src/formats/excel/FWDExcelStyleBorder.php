<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe para borda.
 *
 * <p>Classe que representa a tag "Border" de um estilo.</p>
 *
 * @package FWD5
 * @subpackage formats-excel
 */
class FWDExcelStyleBorder {

	/**
	 * Array com os poss�veis valores para o atributo Position
	 * @var array
	 * @access private
	 */
	private $capositionOptions = array("", "Bottom", "Left", "Right", "Top", "DiagonalLeft", "DiagonalRight");

	/**
	 * Array com os poss�veis valores para o atributo LineStyle
	 * @var array
	 * @access private
	 */
	private $calineOptions = array("", "Continuous", "Double", "Dash");

	/**
	 * Posi��o da borda.
	 * @var string
	 * @access private
	 */
	private $csposition = "";

	/**
	 * Tipo da borda.
	 * @var string
	 * @access private
	 */
	private $csline = "";

	/**
	 * Cor da borda.
	 * @var string
	 * @access private
	 */
	private $cscolor = "";

	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDExcelStyleBorder.</p>
	 *
	 * @access public
	 */
	public function __construct() {}

	/**
	 * Seta a posi��o da borda.
	 *
	 * <p>M�todo para setar a posi��o da borda.</p>
	 *
	 * @access public
	 * @param string $psposition Posi��o da borda {Bottom | Left | Right | Top | DiagonalLeft | DiagonalRight}.
	 */
	public function setPosition($psposition) {
		if(in_array($psposition, $this->capositionOptions)) $this->csposition = $psposition;
		else trigger_error("Invalid token: \"" . $psposition . "\"",E_USER_ERROR);
	}

	/**
	 * Retorna a posi��o da borda.
	 *
	 * <p>M�todo para retornar a posi��o da borda.</p>
	 *
	 * @access public
	 * @return string Posi��o da borda.
	 */
	public function getPosition() {
		return $this->csposition;
	}

	/**
	 * Seta o tipo da borda.
	 *
	 * <p>M�todo para setar o tipo da borda.</p>
	 *
	 * @access public
	 * @param string $psline Tipo da borda {Continuous | Double | Dash}.
	 */
	public function setLine($psline) {
		if(in_array($psline, $this->calineOptions)) $this->csline = $psline;
		else trigger_error("Invalid token: \"" . $psline . "\"",E_USER_ERROR);
	}

	/**
	 * Retorna o tipo da borda.
	 *
	 * <p>M�todo para retornar o tipo da borda.</p>
	 *
	 * @access public
	 * @return string Tipo da borda.
	 */
	public function getLine() {
		return $this->csline;
	}

	/**
	 * Seta a cor da borda.
	 *
	 * <p>M�todo para setar a cor da borda.</p>
	 *
	 * @access public
	 * @param integer $piR Cor da borda (Vermelho).
	 * @param integer $piG Cor da borda (Verde).
	 * @param integer $piB Cor da borda (Azul).
	 */
	public function setColor($piR, $piG, $piB) {
		if(($mscolor = FWDExcelColors::color($piR, $piG, $piB))) $this->cscolor = $mscolor;
		else trigger_error("Invalid rgb color: \"" . "($piR,$piG,$piB)" .  "\"",E_USER_ERROR);
	}

	/**
	 * Retorna a cor da borda.
	 *
	 * <p>M�todo para retornar a cor da borda.</p>
	 *
	 * @access public
	 * @return string Cor da borda.
	 */
	public function getColor() {
		return $this->cscolor;
	}

	/**
	 * Seta os atributos de borda.
	 *
	 * <p>M�todo para setar a posi��o, tipo de linha e cor da borda.</p>
	 *
	 * @access public
	 * @param string $psposition Posi��o da borda {Bottom | Left | Right | Top | DiagonalLeft | DiagonalRight}.
	 * @param string $psline Tipo da borda {Continuous | Double | Dash}.
	 * @param array $pacolor Cores (array (R,G,B)).
	 */
	public function setAttributes($psposition, $psline, $pacolor) {
		$this->setPosition($psposition);
		$this->setLine($psline);
		if((is_array($pacolor)) && (count($pacolor) == 3)) $this->setColor($pacolor[0], $pacolor[1], $pacolor[2]);
		else trigger_error("Invalid parameter: pacolor must be an array with 3 elements",E_USER_ERROR);
	}

	/**
	 * Retorna o xml que representa a tag "Border".
	 *
	 * <p>M�todo para retornar o c�digo xml para a tag "Border".</p>
	 *
	 * @access public
	 * @return string C�digo xml para tag "Border".
	 */
	public function createTag() {

		echo "<Border";
		echo $this->csposition ? " ss:Position=\"".$this->csposition."\"" : "";
		if ($this->csline == "Continuous") echo " ss:LineStyle=\"".$this->csline."\" ss:Weight=\"1\"";
		else echo $this->csline ? " ss:LineStyle=\"".$this->csline."\"" : "";
		echo $this->cscolor ? " ss:Color=\"".$this->cscolor."\"" : "";
		echo "/>";
	}
}
?>