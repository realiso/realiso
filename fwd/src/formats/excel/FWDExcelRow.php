<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe que representa uma linha de uma tabela.
 *
 * <p>Classe que representa a tag "Row" de uma tabela.</p>
 *
 * @package FWD5
 * @subpackage formats-excel
 */
class FWDExcelRow {

	/**
	 * Altura.
	 * @var integer
	 * @access private
	 */
	private $ciheight = 0;

	/**
	 * Array de FWDExcelCell.
	 * @var array
	 * @access private
	 */
	private $cacells = array();

	/**
	 * Id do estilo.
	 * @var string
	 * @access private
	 */
	private $csstyleId = "";

	/**
	 * Id do estilo das c�lulas da linha.
	 * @var string
	 * @access private
	 */
	private $cscellStyleId = "";

	/**
	 * Array de identificadores de estilo.
	 * @var array
	 * @access private
	 */
	private $cacolumnsStyles = array();
		
	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDExcelRow.</p>
	 *
	 * @access public
	 *
	 * @param integer $piheight Altura (default = 15).
	 */
	public function __construct($piheight = 15) {
		$this->setHeight($piheight);
	}

	/**
	 * Seta a altura da linha.
	 *
	 * <p>M�todo para setar a altura da linha.</p>
	 *
	 * @access public
	 * @param integer $piheight Altura.
	 */
	public function setHeight($piheight) {
		if ($piheight >= 0) $this->ciheight = $piheight;
		else trigger_error("Invalid height: ",E_USER_ERROR);
	}

	/**
	 * Retorna a altura da linha.
	 *
	 * <p>M�todo para retornar a altura da linha.</p>
	 *
	 * @access public
	 * @param integer altura.
	 */
	public function getHeight() {
		return $this->ciheight;
	}

	/**
	 * Adiciona uma c�lula.
	 *
	 * <p>M�todo para adicionar uma c�lula � linha.</p>
	 *
	 * @access public
	 * @param FWDExcelCell $pocell C�lula.
	 */
	public function addCell(FWDExcelCell $pocell) {
		$this->cacells[] = $pocell;
	}

	/**
	 * Seta o identificador do estilo.
	 *
	 * <p>M�todo para setar o identificador do estilo. Todas as c�lulas dessa
	 * linha receber�o esse estilo (n�o apenas as c�lulas preenchidas).</p>
	 *
	 * @access public
	 * @param string $psstyleId Id do estilo.
	 */
	public function setStyleId($psstyleId) {
		$this->csstyleId = $psstyleId;
	}

	/**
	 * Retorna o identificador do estilo.
	 *
	 * <p>M�todo para retornar o identificador do estilo.</p>
	 *
	 * @access public
	 * @param string Identificador do estilo.
	 */
	public function getStyleId() {
		return $this->csstyleId;
	}

	/**
	 * Seta o identificador do estilo das c�lulas.
	 *
	 * <p>M�todo para setar o identificador do estilo das c�lulas. Somente
	 * as c�lulas preenchidas receber�o o estilo.</p>
	 *
	 * @access public
	 * @param string $pscellStyleId Id do estilo.
	 */
	public function setCellStyleId($pscellStyleId) {
		$this->cscellStyleId = $pscellStyleId;
	}

	/**
	 * Retorna o identificador do estilo das c�lulas.
	 *
	 * <p>M�todo para retornar o identificador do estilo das c�lulas.</p>
	 *
	 * @access public
	 * @param string Identificador do estilo das c�lulas.
	 */
	public function getCellStyleId() {
		return $this->cscellStyleId;
	}

	/**
	 * Seta o array de identificadores dos estilos das colunas.
	 *
	 * <p>M�todo para setar o array de identificadores dos estilos das colunas.</p>
	 *
	 * @access public
	 * @param array $pacolumnsStyle Ids dos estilos das colunas.
	 */
	public function setColumnsStyleIds($pacolumnsStyle) {
		$this->cacolumnsStyle = $pacolumnsStyle;
	}

	/**
	 * Retorna o xml que representa a tag "Row".
	 *
	 * <p>M�todo para retornar o c�digo xml para a tag "Row".</p>
	 *
	 * @access public
	 * @return string C�digo xml para tag "Row".
	 */
	public function createTag() {
		echo "<Row";
		echo $this->ciheight ? " ss:Height=\"".$this->ciheight."\"" : "";
		echo $this->csstyleId ? " ss:StyleID=\"".$this->csstyleId."\"" : "";
		echo ">";

		foreach($this->cacells as $key => $cell) {
			$cell->createTag();
		}

		echo "</Row>";
	}
}
?>