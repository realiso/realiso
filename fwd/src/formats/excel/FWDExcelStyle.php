<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe para adicionar estilos em um arquivo XLS.
 *
 * <p>Classe que representa a tag "Style" de uma arquivo xml.</p>
 *
 * @package FWD5
 * @subpackage formats-excel
 */
class FWDExcelStyle {
		
	/**
	 * Identificador do estilo
	 * @var string
	 * @access private
	 */
	private $csid;

	/**
	 * Alinhamento do estilo
	 * @var FWDExcelStyleAlignment
	 * @access private
	 */
	private $coAlignment = null;

	/**
	 * Fonte do estilo
	 * @var FWDExcelStyleFont
	 * @access private
	 */
	private $coFont = null;

	/**
	 * Interior do estilo
	 * @var FWDExcelStyleInterior
	 * @access private
	 */
	private $coInterior = null;

	/**
	 * Formato de n�mero do estilo
	 * @var FWDExcelStyleNumberFormat
	 * @access private
	 */
	private $coNumberFormat = null;

	/**
	 * Array dos estilos das bordas
	 * @var FWDExcelStyleBorders
	 * @access private
	 */
	private $coBorders = null;
		
	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDExcelStyle.</p>
	 *
	 * @access public
	 * @param string $psid Identificador do estilo.
	 */
	public function __construct($psid) {
		$this->setId($psid);
	}

	/**
	 * Seta o id do estilo.
	 *
	 * <p>M�todo para setar o id do estilo.</p>
	 *
	 * @access public
	 * @param string $psid Id do estilo.
	 */
	public function setId($psid) {
		if ($psid) $this->csid = $psid;
		else trigger_error("Invalid style id",E_USER_ERROR);
	}

	/**
	 *  Retorna o id do estilo.
	 *
	 * <p>M�todo para retornar o id do estilo.</p>
	 *
	 * @access public
	 * @return string Id do estilo.
	 */
	public function getId() {
		return $this->csid;
	}

	/**
	 * Seta o objeto de alinhamento do estilo.
	 *
	 * <p>M�todo para setar o objeto de alinhamento do estilo.</p>
	 *
	 * @access public
	 * @param FWDExcelStyleAlignment $poAlignment Alinhamento do estilo.
	 */
	public function setAlignment(FWDExcelStyleAlignment $poAlignment) {
		$this->coAlignment = $poAlignment;
	}

	/**
	 *  Retorna o objeto de alinhamento do estilo.
	 *
	 * <p>M�todo para retornar o objeto de alinhamento do estilo.</p>
	 *
	 * @access public
	 * @return FWDExcelStyleAlignment Alinhamento do estilo.
	 */
	public function getAlignment() {
		return $this->coAlignment;
	}

	/**
	 * Seta o objeto de fonte do estilo.
	 *
	 * <p>M�todo para setar o objeto de fonte do estilo.</p>
	 *
	 * @access public
	 * @param FWDExcelStyleFont $poFont Fonte do estilo.
	 */
	public function setFont(FWDExcelStyleFont $poFont) {
		$this->coFont = $poFont;
	}

	/**
	 *  Retorna o objeto de fonte do estilo.
	 *
	 * <p>M�todo para retornar o objeto de fonte do estilo.</p>
	 *
	 * @access public
	 * @return FWDExcelStyleFont Fonte do estilo.
	 */
	public function getFont() {
		return $this->coFont;
	}

	/**
	 * Seta o objeto de interior do estilo.
	 *
	 * <p>M�todo para setar o objeto de interior do estilo.</p>
	 *
	 * @access public
	 * @param FWDExcelStyleInterior $poInterior Interior do estilo.
	 */
	public function setInterior(FWDExcelStyleInterior $poInterior) {
		$this->coInterior = $poInterior;
	}

	/**
	 *  Retorna o objeto de interior do estilo.
	 *
	 * <p>M�todo para retornar o objeto de interior do estilo.</p>
	 *
	 * @access public
	 * @return FWDExcelStyleInterior Interior do estilo.
	 */
	public function getInterior() {
		return $this->coInterior;
	}

	/**
	 * Seta o objeto de formato de n�mero do estilo.
	 *
	 * <p>M�todo para setar o objeto de formato de n�mero do estilo.</p>
	 *
	 * @access public
	 * @param FWDExcelStyleNumberFormat $poNumberFormat Formato de n�mero do estilo.
	 */
	public function setNumberFormat(FWDExcelStyleNumberFormat $poNumberFormat) {
		$this->coNumberFormat = $poNumberFormat;
	}

	/**
	 *  Retorna o objeto de formato de n�mero do estilo.
	 *
	 * <p>M�todo para retornar o objeto de formato de n�mero do estilo.</p>
	 *
	 * @access public
	 * @return FWDExcelStyleNumberFormat Formato de n�mero do estilo.
	 */
	public function getNumberFormat() {
		return $this->coNumberFormat;
	}

	/**
	 * Seta o objeto de bordas do estilo.
	 *
	 * <p>M�todo para setar o objeto de bordas do estilo.</p>
	 *
	 * @access public
	 * @param FWDExcelStyleBorders $poBorders Bordas do estilo.
	 */
	public function setBorders(FWDExcelStyleBorders $poBorders) {
		$this->coBorders = $poBorders;
	}

	/**
	 *  Retorna o objeto de bordas do estilo.
	 *
	 * <p>M�todo para retornar o objeto de bordas do estilo.</p>
	 *
	 * @access public
	 * @return FWDExcelStyleBorders Bordas do estilo.
	 */
	public function getBorders() {
		return $this->coBorders;
	}

	/**
	 * Retorna o xml que representa a tag "Style".
	 *
	 * <p>M�todo para retornar o c�digo xml para a tag "Style".</p>
	 *
	 * @access public
	 * @return string C�digo xml para tag "Style".
	 */
	public function createTag() {
		echo "<Style ss:ID=\"" . $this->csid . "\">";

		if($this->coAlignment) $this->coAlignment->createTag();
		if($this->coBorders) $this->coBorders->createTag();
		if($this->coFont) $this->coFont->createTag();
		if($this->coInterior) $this->coInterior->createTag();
		if($this->coNumberFormat) $this->coNumberFormat->createTag();
		echo "</Style>";
	}
}
?>