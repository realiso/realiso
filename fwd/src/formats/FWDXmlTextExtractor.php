<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDXmlTextExtractor. Extrai texto de arquivos XML.
 *
 * <p>Classe para extrair texto de arquivos XML.</p>
 * @package FWD5
 * @subpackage base
 */
class FWDXmlTextExtractor extends FWDXMLParser {

	/**
	 * Controla quando espa�os devem ser introduzidos no texto extra�do
	 * @var boolean
	 * @access protected
	 */
	protected $cbAddSpace = false;

	/**
	 * Acumula o texto extra�do
	 * @var string
	 * @access protected
	 */
	protected $csContent = '';

	/**
	 * Handler chamado quando o parser encontra uma tag de in�cio de elemento
	 *
	 * <p>Handler chamado quando o parser encontra uma tag de in�cio de elemento.</p>
	 * @access protected
	 * @param resource $prParser Parser
	 * @param string $psTagName Nome da tag
	 * @param array $paAttributes Array associativo contendo os atributos do elemento
	 */
	protected function startHandler($prParser, $psTagName, $paAttributes){
		$this->cbAddSpace = true;
	}

	/**
	 * Handler chamado quando o parser encontra texto dentro de um elemento
	 *
	 * <p>Handler chamado quando o parser encontra texto dentro de um elemento.</p>
	 * @access protected
	 * @param resource $prParser Parser
	 * @param string $psData Conte�do de texto do elemento
	 */
	protected function dataHandler($prParser, $psData){
		if($this->cbAddSpace){
			$this->csContent.= ' '.$psData;
			$this->cbAddSpace = false;
		}else{
			$this->csContent.= $psData;
		}
	}

	/**
	 * Handler chamado quando o parser encontra uma tag de fim de elemento
	 *
	 * <p>Handler chamado quando o parser encontra uma tag de fim de elemento.</p>
	 * @access protected
	 * @param resource $prParser Parser
	 * @param string $psTagName Nome da tag
	 */
	protected function endHandler($prParser, $psTagName){
		$this->cbAddSpace = true;
	}

	/**
	 * Retorna o texto extra�do pelo parser.
	 *
	 * <p>Retorna o texto extra�do pelo parser.</p>
	 * @access public
	 * @return string Texto extra�do
	 */
	public function getContent(){
		return $this->csContent;
	}

}

?>