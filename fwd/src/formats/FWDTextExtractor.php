<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDTextExtractor. Extrai texto de arquivos de v�rios formatos.
 *
 * <p>Classe para extrair texto de arquivos de v�rios formatos</p>
 * @package FWD5
 * @subpackage base
 */
class FWDTextExtractor {

	/**
	 * Lista de extens�es a serem ignoradas.
	 * @var array
	 * @access private
	 */
	private $caExtensionsToIgnore;

	/**
	 * Path para a pasta onde est�o os execut�veis usados para extrair texto de alguns formatos de arquivo.
	 * @var string
	 * @access private
	 */
	private $csExtractorsPath;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDTextExtractor.</p>
	 * @access public
	 */
	public function __construct(){
		$this->caExtensionsToIgnore = array('jpg','jpeg','gif','png','bmp','exe');
		$msCurrentFile = $_SERVER['SCRIPT_FILENAME'];
		if(strtoupper(substr(PHP_OS,0,3)) === 'WIN'){
			$this->csExtractorsPath = FWDWebLib::getInstance()->getLibRef().'bin\\windows\\';
			$this->csExtractorsPath = strtr($this->csExtractorsPath,"/","\\");
		}else{
			$this->csExtractorsPath = FWDWebLib::getInstance()->getLibRef().'bin/linux/';
		}
	}

	/**
	 * Retira as tags de um trecho de c�digo XML.
	 *
	 * <p>Retira as tags de um trecho de c�digo XML. Retira tamb�m espa�os desnecess�rios.</p>
	 * @access private
	 * @param string $psString C�digo XML
	 */
	private function stripTags($psString){
		return trim(preg_replace('/([\n\s]+(&nbsp;)*)+/',' ',preg_replace('/<[^>]+>/',' ',$psString)));
	}

	/**
	 * Extrai texto de um arquivo.
	 *
	 * <p>Extrai texto de um arquivo.</p>
	 * @param string $psFileName Nome do arquivo
	 * @param string $psExtension Extens�o do arquivo (somente necess�rio se o nome do arquivo n�o tiver extens�o)
	 * @access public
	 */
	public function extractTextFromFile($psFileName,$psExtension=''){
		if($psExtension==''){
			$msExtension = strtolower(substr($psFileName,strrpos($psFileName,'.')+1));
		}else{
			$msExtension = strtolower($psExtension);
		}

		if(in_array($msExtension,$this->caExtensionsToIgnore)) return false;
		switch($msExtension){
			case 'dot':
			case 'doc':
				$moPHPWordLib = new PHPWordLib();
				$msContent = $moPHPWordLib->loadFile($psFileName);
				if($msContent===false){
					return false;
				}else{
					return $moPHPWordLib->GetPlainText($msContent);
				}
			case 'pdf':
				return shell_exec("{$this->csExtractorsPath}pdftotext $psFileName -");
			case 'rtf':
				$moRtf = new rtf((file_get_contents($psFileName)));
				$moRtf->output('html');
				$moRtf->parse();
				if(count($moRtf->err) == 0){
					return preg_replace(array('/<\/?div [^>]+>/','/(<[^>]+>)+/'),array(' ',''),$moRtf->out);
				}else{
					return false;
				}
			case 'xlsx':
				$moArchive = new ZipArchive();
				if($moArchive->open($psFileName)===true){
					$msContent = '';
					for($i=0;$i<$moArchive->numFiles;$i++){
						$maStat = $moArchive->statIndex($i);
						if(substr($maStat['name'],0,14)=='xl/worksheets/'){
							$msContent.= ' '.$this->stripTags($moArchive->getFromIndex($i));
						}
					}
					return $msContent;
				}else{
					return false;
				}
			case 'pptx':
			case 'ppsx':
				$moArchive = new ZipArchive();
				if($moArchive->open($psFileName)===true){
					$msContent = '';
					for($i=0;$i<$moArchive->numFiles;$i++){
						$maStat = $moArchive->statIndex($i);
						if(substr($maStat['name'],0,11)=='ppt/slides/'){
							$msContent.= ' '.$this->stripTags($moArchive->getFromIndex($i));
						}
					}
					return $msContent;
				}else{
					return false;
				}
			case 'docx':
				$moArchive = new ZipArchive();
				if($moArchive->open($psFileName)===true){
					$msContent = $moArchive->getFromName('word/document.xml');
					if($msContent===false) return false;
					$miStart = strpos($msContent,'<w:body');
					$miEnd = strrpos($msContent,'</w:body');
					$msContent = substr($msContent,$miStart,$miEnd - $miStart);
					return utf8_decode($this->stripTags($msContent));
				}else{
					return false;
				}
			case 'odt':
			case 'ods':
			case 'odp':
				$moArchive = new ZipArchive();
				if($moArchive->open($psFileName)===true){
					$msContent = $moArchive->getFromName('content.xml');
					if($msContent===false) return false;
					$miStart = strpos($msContent,'<office:body');
					$miEnd = strrpos($msContent,'</office:body');
					$msContent = substr($msContent,$miStart,$miEnd - $miStart);
					return utf8_decode($this->stripTags($msContent));
				}else{
					return false;
				}
			case 'xls':
				$msOutput = shell_exec("{$this->csExtractorsPath}xlhtml -nc -a -fw $psFileName");
				$msOutput = preg_replace(array('/<title>[^<]*<\/title>/i','/<[^>]+>/'),array('',' '),$msOutput);
				$msOutput = substr($msOutput,0,strrpos($msOutput,'Created with'));
				return preg_replace('/([\n\s]+(&nbsp;)*)+/',' ',$msOutput);
			case 'ppt':
			case 'pps':
				$msOutput = shell_exec("{$this->csExtractorsPath}ppthtml $psFileName");
				$msOutput = preg_replace(array('/<title>[^<]*<\/title>/i','/<[^>]+>/'),array('',' '),$msOutput);
				$msOutput = substr($msOutput,0,strrpos($msOutput,'Created with'));
				return preg_replace('/([\n\s]+(&nbsp;)*)+/',' ',$msOutput);
			case 'html':
				return $this->stripTags(file_get_contents($psFileName));
			case 'txt':
				return str_replace("\0","",file_get_contents($psFileName));
			default:
				$moXmlParser = new FWDXmlTextExtractor();
				if($moXmlParser->parseFile($psFileName,false)){
					return $this->stripTags($moXmlParser->getContent());
				}else{
					return false;
				}
		}
	}

}

?>