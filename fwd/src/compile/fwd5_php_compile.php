<?php
set_time_limit(0);
require_once('../base/FWDCodeCompresser.php');

function check_extension($file, $extensions) {
    $aux = explode(".",strtolower(trim($file)));
    $file_extension = $aux[count($aux)-1];
    foreach($extensions as $extension) if ($file_extension == $extension) return false;
    return true;
}

function dircopy($srcdir, $dstdir, $directories_to_ignore = array(), $extensions_to_ignore = array(), $extensions_to_allow = array(), $files_to_ignore = array()) {
    $num = 0;
    if(!is_dir($dstdir)) mkdir($dstdir, 0777, true);
    if($curdir = opendir($srcdir)) {
        while($file = readdir($curdir)) {
            if($file != '.' && $file != '..' && !in_array($file, $directories_to_ignore)) {
                $srcfile = $srcdir . '/' . $file;
                $dstfile = $dstdir . '/' . $file;
                if(is_file($srcfile) && !in_array($file, $files_to_ignore)) {
                    if (count($extensions_to_ignore) && !check_extension($file, $extensions_to_ignore)) continue;
                    if (count($extensions_to_allow) && check_extension($file, $extensions_to_allow)) continue;
                    echo "<br>Copying '$srcfile' to '$dstfile'...";
                    if(copy($srcfile, $dstfile)) {
                        touch($dstfile, filemtime($srcfile)); $num++;
                        echo "OK\n";
                    }
                    else echo "<br>Error: File '$srcfile' could not be copied!\n";
                }
                else if(is_dir($srcfile)) {
                    $num += dircopy($srcfile, $dstfile, $directories_to_ignore, $extensions_to_ignore, $extensions_to_allow, $files_to_ignore);
                }
            }
        }
        closedir($curdir);
    }
    return $num;
}

function phpCompile($paDirectories, $psCompiledFileName, $psSrcRef) {
    $mrHandle = fopen($psCompiledFileName, "w+");
    $moCompresser = new FWDCodeCompresser();
    if($mrHandle){
        foreach($paDirectories as $msDirectory => $maFiles){
            if($msDirectory == "db2") $msDirectory = "db";      // gambia
            $msDirectory = $psSrcRef.$msDirectory;
            if(is_dir($msDirectory)){
                foreach ($maFiles as $msFile) {
                    if(substr($msFile,0,4)=='php:'){
                        fwrite($mrHandle,substr($msFile,4));
                    }else{
                        $msFileFullPath = $msDirectory . "/" . $msFile;
                        echo $msFileFullPath . "<br/>";
                        $msExt = strtolower(substr($msFile,strrpos($msFile,'.')+1));
                        $msFileContent = file_get_contents($msFileFullPath);
                        if($msExt=='js' || $msExt=='css'){
                            fwrite($mrHandle,$moCompresser->compressString($msFileContent,$msExt));
                        }else{
                            fwrite($mrHandle,$msFileContent);
                        }
                    }
                }
            }else{
                echo "$msDirectory is not a directory!<br/>";
            }
        }
        return true;
    }
    else {
        echo "Could not create file = $psCompiledFileName!</br>";
        return false;
    }
}

// diret�rio base onde se encontram os sistemas
$ssBaseDir = '/home/real/Projects/realiso/realiso';

// nome do diret�rio da fwd
$ssFwdFolder = 'fwd';

// diret�rio do sistema que ser� compilado
$ssSystemFolder = '';

// diret�rio onde a fwd ser� armazenada ap�s compilada
$ssDeployFolder = '';

// arquivos javascript do sistema em quest�o
$saJSFiles = array();

// arquivos css do sistema em quest�o
$saCSSFiles = array();

// indica para qual sistema a fwd ser� compilada
$ssSystemId = 'isms';

switch ($ssSystemId) {
    case 'isms':
        $ssSystemFolder = 'isms';
        $ssDeployFolder = 'new_deploy_isms';
        $saJSFiles = array("basics.js", "isms.js", "RiskParameters.js", "ISMSContextsAssociator.js", "error_handling.js");
        $saCSSFiles = array("basics.css", "grid.css", "tab.css", "tree.css", "isms.css", "isms_basic.css");
        break;
    case 'bcms':
        $ssSystemFolder = 'bcms';
        $ssDeployFolder = 'new_deploy_bcms';
        $saJSFiles = array("basics.js", "isms.js", "RiskParameters.js", "ISMSContextsAssociator.js", "error_handling.js");
        $saCSSFiles = array("basics.css", "grid.css", "tab.css", "tree.css", "isms.css", "isms_basic.css");
        break;

    case 'admin':
        $ssSystemFolder = 'saasadmin';
        $ssDeployFolder = 'new_deploy_saas_admin';
        $saJSFiles = array("basics.js");
        $saCSSFiles = array("basics.css", "grid.css", "tab.css", "tree.css", "saas.css", "saas_basic.css", "activation.css");
        break;

    default:
        trigger_error("Invalid system id = $ssSystemId", E_USER_ERROR);
        break;
}

// diret�rio na pasta deploy onde ser�o armazenados os arquivos da fwd ap�s a compila��o

$ssDir = "$ssBaseDir/$ssDeployFolder/lib/";


if (!is_dir($ssDir)) mkdir($ssDir);

// c�digo inserido nos arquivos js.php e css.php para fazer cache das informa��es
$ssCache =
'php:<?
if (isset( $_SERVER["HTTP_IF_MODIFIED_SINCE"] )) {
   $if_modified = $_SERVER["HTTP_IF_MODIFIED_SINCE"];
   $js_time = filemtime($_SERVER[\'SCRIPT_FILENAME\']);
      if (strtotime( $if_modified ) == $js_time) {
              header(\'Last-Modified: \'.gmdate(\'D, d M Y H:i:s\', $js_time) .\' GMT\', true, 304);
              exit();
      }
}
$js_time = filemtime($_SERVER[\'SCRIPT_FILENAME\']);
header(\'Last-Modified: \'.gmdate(\'D, d M Y H:i:s\', $js_time) .\' GMT\', true, 200);
?>';

// FWD0
$saFiles = array(
"base" => array('php:<?$lbRelease = true;?>',"phpchartdir.php", "color.php", "FWDActivationHandler.php", "FWDXmlLoad.php", "FWDXmlStatic.php", "FWDException.php", "FWDLanguage.php", "FWDDefaultErrorHandler.php", "FWDDoNothingErrorHandler.php", "FWDWebLib.php", "FWDLicense.php", "FWDXmlCompile.php","FWDColor.php", "FWDACLSecurity.php", "FWDMenuACLSecurity.php", "FWDGridMenuACLSecurity.php", "FWDParameter.php", "FWDSyslog.php", "FWDCrypt.php", "FWDEmail.php", "FWDConfig.php", "FWDSMTPConnectionTest.php", "FWDSYSLogConnectionTest.php", "FWDActiveDirectory.php", "FWDASaaS.php"),
"interfaces" => array("FWDPostValue.php", "QueryAutoCompleter.php", "FWDIDB.php", "FWDCollectable.php", "FWDErrorHandler.php")
);
echo "<h3>COMPILING FWD0.php</h3>";
phpCompile($saFiles, $ssDir . "fwd0.php", "../");

// FWD1
$saFiles = array(
"abstracts" => array("FWDRunnable.php")
);
echo "<h3>COMPILING FWD1.php</h3>";
phpCompile($saFiles, $ssDir . "fwd1.php", "../");

// FWD2
$saFiles = array(
"abstracts" => array("FWDDrawing.php", "FWDReportDrawing.php", "FWDView.php", "FWDContainer.php", "FWDItemController.php", "FWDSession.php", "FWDViewGrid.php", "FWDGridAbstract.php", "FWDGridParameter.php"),
"db" => array("FWDDBQueryHandler.php"),
"event" => array("FWDEvent.php"),
"report" => array("FWDReportWriter.php", "FWDReportLevelIterator.php", "FWDReportCell.php"),
"formats/word" => array("FWDWordObject.php"),
"translation" => array("FWDXMLParser.php")
);
echo "<h3>COMPILING FWD2.php</h3>";
phpCompile($saFiles, $ssDir . "fwd2.php", "../");

// FWD3
$saFiles = array(
"db" => array("FWDDB.php", "FWDDBTable.php", "FWDDBType.php", "FWDDBEntity.php", "FWDDBDataBinder.php", "FWDDBField.php", "FWDDBFilter.php", "FWDDBDataSet.php"),
"event" => array("FWDTempEvent.php", "FWDEventHandler.php", "FWDAjaxEvent.php", "FWDServerEvent.php", "FWDClientEvent.php", "FWDJsEvent.php", "FWDMask.php", "FWDMaskAutoComplete.php", "FWDStartEvent.php", "FWDToolTip.php", "FWDRequiredCheck.php"),
"event/classEvent" => array("FWDGridPopulate.php", "FWDGridChangePageChange.php", "FWDGridChangePageBack.php", "FWDGridChangePageFirst.php", "FWDGridChangePageLast.php", "FWDGridChangePageNext.php", "FWDGridRefresh.php", "FWDGridExport.php", "FWDTreeAjaxExpand.php", "FWDGridColumnOrder.php", "FWDRequiredCheckAjax.php", "FWDGridUserEdit.php", "FWDGridDrawUserEdit.php", "FWDGridUserEditDefaultValues.php", "FWDGridCreatePopupEditPref.php", "FWDDBGridPackRefresh.php"),
"view/viewgroup" => array("FWDViewGroup.php", "FWDDialog.php", "FWDPanel.php"),
"view" => array("FWDBox.php", "FWDIFrame.php", "FWDString.php", "FWDVariable.php", "FWDText.php", "FWDMemo.php", "FWDButton.php", "FWDCalendar.php", "FWDIcon.php", "FWDStatic.php", "FWDWarning.php", "FWDItem.php", "FWDLink.php", "FWDViewButton.php", "FWDTreeBase.php", "FWDTree.php", "FWDTreeAjax.php", "FWDMenu.php", "FWDMenuItem.php", "FWDMenuSeparator.php", "FWDSelect.php", "FWDSelector.php", "FWDOptgroup.php", "FWDController.php", "FWDRadioBox.php", "FWDCheckBox.php", "FWDMessageBox.php", "FWDTabGroup.php", "FWDTabItem.php", "FWDUserPreferences.php", "FWDGrid.php", "FWDColumn.php", "FWDDrawGrid.php", "FWDGetParameter.php", "FWDSessionParameter.php", "FWDNodeBuilder.php","FWDDBTreeBase.php", "FWDDBTree.php", "FWDSelectQuery.php", "FWDFile.php", "FWDScrolling.php", "FWDChartDirector.php", "FWDDiv.php", "FWDStaticGrid.php", "FWDHorizontalRule.php", "FWDMemoStatic.php", "FWDPack.php", "FWDDBGridPack.php", "FWDColorPicker.php", "FWDAutocompleter.php", "FWDAutolisting.php"),
"db2" => array("FWDDBSelect.php", "FWDDBGrid.php"),
"report" => array("FWDReport.php", "FWDReportLevel.php", "FWDReportLine.php", "FWDReportStatic.php", "FWDReportIcon.php", "FWDReportHTMLWriter.php", "FWDReportDocWriter.php", "FWDReportPDFWriter.php", "FWDReportGenerator.php", "FWDReportITemplate.php", "FWDReportClassificator.php", "FWDReportFilter.php", "FWDSuppressModel.php", "FWDLiquidCellsSuppressModel.php"),
"translation" => array("FWDStringList.php", "FWDTranslationDriver.php", "FWDXMLDriver.php", "FWDDBDriver.php", "FWDTranslate.php"),

"formats/excel" => array("PHPExcel.php"),

"formats/excel/PHPExcel" => array(
"Autoloader.php",
"CachedObjectStorageFactory.php",
"Calculation.php",
"Cell.php",
"Comment.php",
"DocumentProperties.php",
"DocumentSecurity.php",
"HashTable.php",
"IComparable.php",
"IOFactory.php",
"NamedRange.php",
"ReferenceHelper.php",
"RichText.php",
"Settings.php",
"Style.php",
"Worksheet.php",
"WorksheetIterator.php"),

"formats/excel/PHPExcel/CachedObjectStorage" => array(
"APC.php",
"CacheBase.php",
"DiscISAM.php",
"ICache.php",
"Memcache.php",
"Memory.php",
"MemoryGZip.php",
"MemorySerialized.php",
"PHPTemp.php",
"Wincache.php"),

"formats/excel/PHPExcel/Calculation" => array(
"Exception.php",
"ExceptionHandler.php",
"FormulaParser.php",
"FormulaToken.php",
"Function.php",
"Functions.php"),

"formats/excel/PHPExcel/Cell" => array(
"AdvancedValueBinder.php",
"DataType.php",
"DataValidation.php",
"DefaultValueBinder.php",
"Hyperlink.php",
"IValueBinder.php"),

"formats/excel/PHPExcel/Style" => array(
"Alignment.php",
"Border.php",
"Borders.php",
"Color.php",
"Conditional.php",
"Fill.php",
"Font.php",
"NumberFormat.php",
"Protection.php"),


"formats/excel/PHPExcel/Worksheet" => array(
"BaseDrawing.php",
"CellIterator.php",
"ColumnDimension.php",
"Drawing.php",
"HeaderFooter.php",
"HeaderFooterDrawing.php",
"MemoryDrawing.php",
"PageMargins.php",
"PageSetup.php",
"Protection.php",
"Row.php",
"RowDimension.php",
"RowIterator.php",
"SheetView.php"),

"formats/excel/PHPExcel/Worksheet/Drawing" => array(
"Shadow.php"),

"formats/excel/PHPExcel/Writer" => array(
"CSV.php",
"Excel5.php",
"Excel2007.php",
"HTML.php",
"IWriter.php",
"PDF.php",
"Serialized.php"),

"formats/excel/PHPExcel/Writer/Excel5" => array(
 "BIFFwriter.php",
 "Escher.php",
 "Font.php",
 "Parser.php",
 "Workbook.php",
 "Worksheet.php",
 "Xf.php"),

"formats/excel/PHPExcel/Shared" => array(
"CodePage.php",
"Date.php",
"Drawing.php",
"Escher.php",
"Excel5.php",
"File.php",
"Font.php",
"OLE.php",
"OLERead.php",
"PasswordHasher.php",
"String.php",
"XMLWriter.php",
"ZipStreamWrapper.php"),

"formats/excel/PHPExcel/Shared/JAMA" => array(
"CholeskyDecomposition.php",
"EigenvalueDecomposition.php",
"LUDecomposition.php",
"Matrix.php",
"QRDecomposition.php",
"SingularValueDecomposition.php"),

"formats/excel/PHPExcel/Shared/JAMA/utils" => array(
"Error.php",
"Maths.php"),


"formats/excel/PHPExcel/Shared/OLE" => array(
"ChainedBlockStream.php",
"PPS.php"),

"formats/excel/PHPExcel/Shared/OLE/PPS" => array(
"File.php",
"Root.php"),


"formats/excel/PHPExcel/Shared/trend" => array(
"bestFitClass.php",
"exponentialBestFitClass.php",
"linearBestFitClass.php",
"logarithmicBestFitClass.php",
"polynomialBestFitClass.php",
"powerBestFitClass.php",
"trendClass.php"),

"formats/word" => array("html_to_doc.inc.php", "FWDWordText.php", "FWDWordStyle.php", "FWDWordTextBlock.php", "FWDWordTable.php", "FWDWordColumn.php", "FWDWordRow.php", "FWDWordColors.php", "FWDWord.php", "PHPWordLib.php"),
"formats/rtf" => array("rtfclass.php"),
"formats" => array("FWDGridExcel.php", "FWDDataSetExcel.php", "FWDPDF.php", "FWDXmlTextExtractor.php", "FWDTextExtractor.php")

);

echo "<h3>COMPILING FWD3.php</h3>";
phpCompile($saFiles, $ssDir . "fwd3.php", "../");

// JS
$saJs = array(
"$ssBaseDir/$ssFwdFolder/src/base" => array('php:<?$lbRelease = true;?>', $ssCache, 'js_setup.php'),
"$ssBaseDir/$ssFwdFolder/src/base/javascript" => array("base64.js", "FWDEventManager.js", "basics.js", "ajaxevent.js", "check_n_radio.js", "js_event.js", "move.js", "resize.js", "tree.js", "grid.js", "tab.js", "menu.js", "scrolling.js", "popups.js", "fwdviewbutton.js", "FWDCheckBox.js", "FWDMemoryCleaner.js", "FWDMemoStatic.js", "FWDStatic.js", "FWDText.js", "FWDFile.js", "FWDSelect.js", "FWDListEditor.js", "FWDSelector.js", "constants.js", "FWDDBGridPack.js", "FWDPack.js", "FWDCalendar.js", "FWDController.js", "FWDUnSerializer.js", "FWDItemController.js", "FWDRadioBox.js", "FWDWarning.js", "FWDColorPicker.js"),
"$ssBaseDir/$ssSystemFolder/src/lib/javascript" => $saJSFiles
);
echo "<h3>COMPILING JS.php</h3>";
phpCompile($saJs, $ssDir . "js.php", "");

// CSS
$saCss = array(
"$ssBaseDir/$ssFwdFolder/src/base" => array($ssCache, 'css_setup.php'),
"$ssBaseDir/$ssSystemFolder/src/lib/css" => $saCSSFiles
);
echo "<h3>COMPILING CSS.php</h3>";
phpCompile($saCss, $ssDir . "css.php", "");

// ADODB
echo "<h3>COPYING ADODB</h3>";
dircopy($ssBaseDir.'/'.$ssFwdFolder.'/src/db/adodb_light', $ssDir.'adodb', array('.svn'));

// FPDF
echo "<h3>COPYING FPDF</h3>";
dircopy($ssBaseDir.'/'.$ssFwdFolder.'/src/formats/fpdf', $ssDir.'fpdf', array('.svn'));

// BINARIES - somente no caso do isms
if ($ssSystemId == 'isms') {
    echo "<h3>COPYING BINARIES</h3>";
    dircopy($ssBaseDir.'/'.$ssFwdFolder.'/src/base/bin', $ssDir.'bin', array('.svn'));
}

// UTILS
echo "<h3>COPYING UTILS</h3>";
if (copy($ssBaseDir.'/'.$ssFwdFolder.'/src/base/mockPage.php', $ssDir.'mockPage.php')) echo "<br>mockPage.php copied!";
else echo "<br>Error while copying mockPage.php!";
if (copy($ssBaseDir.'/'.$ssFwdFolder.'/src/base/FWDGetImg.php', $ssDir.'FWDGetImg.php')) echo "<br>FWDGetImg.php copied!";
else echo "<br>Error while copying FWDGetImg.php!";
if (copy($ssBaseDir.'/'.$ssFwdFolder.'/src/base/FWDCodeCompresser.php', $ssDir.'FWDCodeCompresser.php')) echo "<br>FWDCodeCompresser.php copied!";
else echo "<br>Error while copying FWDCodeCompresser.php!";

// DEBUG
echo "<h3>CREATING DEBUG.TXT</h3>";
$ssDebugFile = $ssDir.'debug.txt';
touch($ssDebugFile);
if (file_exists($ssDebugFile)) echo "<br/>debug.txt created!";
else echo "<br/>Error while creating debug.txt!";

// FWD5_SETUP.PHP
echo "<h3>CREATING FWD5_SETUP.PHP</h3>";
$ssFWD5Setup =
'<?    
    include_once "adodb/adodb.inc.php";
    include_once "fpdf/fpdf.php";
        
    include_once "fwd0.php";    
    include_once "fwd1.php";
    include_once "fwd2.php";
    include_once "fwd3.php";
    
    $soWebLib = FWDWebLib::getInstance();
    $soWebLib->setLibRef($sys_ref."lib/");
    $soWebLib->setJsRef($sys_ref."lib/javascript/");
    $soWebLib->setCssRef($sys_ref."lib/css/");
?>';
$ssFWD5SetupFile = $ssDir.'fwd5_setup.php';
if (file_put_contents($ssFWD5SetupFile, $ssFWD5Setup))
echo "<br/>fwd5_setup.php created!";
else
echo "<br/>Error while creating fwd5_setup.php!";
?>
