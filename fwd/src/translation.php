<?php
set_time_limit(3000);
$miTimestampTotal = microtime(1);

require_once("base/setup.php");
require_once("translation/include.php");

$base_ref = '';
$soWebLib = FWDWebLib::getInstance();
$soWebLib->setCSSRef($base_ref.'base/');
$soWebLib->setSysRef($base_ref);
$soWebLib->setGfxRef($base_ref . "gfx/");
$soWebLib->setSysJsRef($base_ref);

//******************************************************************************
$miSystemId = 50;// id do sistema da tabela de 'lic_system' da base de tools da axur

//******************************************************************************
// Inicializa o DataSet pro driver de BD

$soDB = new FWDDB(DB_POSTGRES, "isms_tools", "isms", "isms", "127.0.0.1");
FWDWebLib::setConnection($soDB);
$soDataSet = new FWDDBDataSet($soDB, 'translation_strings');
$soDataSet->addFWDDBField(new FWDDBField('skSystem','string_system_id',DB_NUMBER,$miSystemId));
$soDataSet->addFWDDBField(new FWDDBField('zId','string_id',DB_STRING));
$soDataSet->addFWDDBField(new FWDDBField('zFile','string_file',DB_STRING));
$soDataSet->addFWDDBField(new FWDDBField('zValue','string_value',DB_STRING));
$soDataSet->addFWDDBField(new FWDDBField('dDateInclusion','string_date_inclusion',DB_DATETIME));
//$soDataSet->addFWDDBField(new FWDDBField('zPortugues','string_portugues', DB_STRING));
$soDataSet->addFWDDBField(new FWDDBField('zEnglishIsms','string_english', DB_STRING));
$soDataSet->addFWDDBField(new FWDDBField('zSpanishIsms','string_spanish', DB_STRING));
$soDataSet->addFWDDBField(new FWDDBField('zFrenchSox','string_french', DB_STRING));
$soDataSet->addFWDDBField(new FWDDBField('zGermanSox','string_german', DB_STRING));

$soDataSet->getFieldByAlias('string_system_id')->addFilter(new FWDDBFilter("=", $miSystemId));

$soDataSet->setOrderBy('string_file', '+');
$soDataSet->setOrderBy('string_id', '+');

//**********************************************************************
//carrega as strings extraidas dos arquivos php
//e insere elas no banco do sistema de tradução da axur
/*
 $miTimeStampLocal = microtime(1);
 echo "<br/><h2>Transferência do PHP para o Banco";
 $soDBDriverPHP = new FWDDBDriver($soDataSet,true);
 $soTranslatePHP = new FWDTranslate($soDBDriverPHP);
 $soTranslatePHP->extractPHPStrings('./abstracts',true);
 $soTranslatePHP->extractPHPStrings('./base',true);
 $soTranslatePHP->extractPHPStrings('./db',true);
 $soTranslatePHP->extractPHPStrings('./event',true);
 $soTranslatePHP->extractPHPStrings('./formats',true);
 $soTranslatePHP->extractPHPStrings('./interfaces',true);
 $soTranslatePHP->extractPHPStrings('./report',true);
 $soTranslatePHP->extractPHPStrings('./view',true);
 echo ' em ' . number_format(microtime(1) - $miTimeStampLocal,2) . ' segundo(s).</h2>';
 */
//******************************************************************************
//obtem do sistema de tradução, as traduções e armazena elas nos arquivos
//de tradução do sistema

$miTimeStampLocal = microtime(1);
echo "<br/><h2>Transferência do banco para o PHP";
// Cria o driver de BD usando o DataSet
$soDBDriver = new FWDDBDriver($soDataSet);
$soTranslate = new FWDTranslate($soDBDriver);
// Armazena num arquivo .php as strings lidas com driver de BD
//$soTranslate->storeToPHPFile('portuguese.php', 'portugues');
$soTranslate->storeToPHPFile('english.php', 'english');
$soTranslate->storeToPHPFile('spanish.php', 'spanish');
$soTranslate->storeToPHPFile('french.php', 'french');
$soTranslate->storeToPHPFile('german.php', 'german');
//$soTranslate->storeToPHPFile('chinese.php', 'chinese');
echo ' em ' . number_format(microtime(1) - $miTimeStampLocal,2) . ' segundo(s).</h2>';

echo "<br/><br/><h2>TEMPO TOTAL<br/>";
echo ' em ' . number_format(microtime(1) - $miTimestampTotal,2) . ' segundo(s).</h2>';
?>