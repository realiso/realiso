<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDVariable. Representa a tag 'variable' do xml.
 *
 * <p>Classe que representa a tag 'variable' do xml. Utilizada para
 * criar uma vari�vel no xml.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDVariable extends FWDView implements FWDPostValue{

	/**
	 * Nome do label associado ao objeto
	 * @var string
	 * @access protected
	 */
	protected $csLabel = "";

	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDVariable.</p>
	 * @access public
	 */
	public function __construct() {
		parent::__construct();
		$this->setAttrDisplay("false");
		$this->setAttrNoEscape('true');
	}

	/**
	 * Retorna o html referente a tag 'variable'.
	 *
	 * <p>Retorna o html referente a tag 'variable'.</p>
	 * @param FWDBox $poBox Box limitante
	 * @access public
	 */
	public function drawBody($poBox = null) {
		if (!($msName = $this->getAttrName()))
		trigger_error("Class FWDVariable must have a name",E_USER_WARNING);

		$msValue = $this->getValue();

		$msLabel = '';
		if($this->csLabel) $msLabel = "label='{$this->csLabel}'";
			
		$msOut = "<input name='{$msName}' id='{$msName}' {$msLabel} type='hidden' value='{$msValue}'/>";
		return $msOut;
	}

	/**
	 * Atribui valor � vari�vel value.
	 *
	 * <p>Atribui valor � vari�vel value, devendo retirar espa�os em branco
	 * no inicio e/ou no fim da string.</p>
	 * @access public
	 * @param string $psValue Valor a ser atribu�do
	 */
	public function setAttrValue($psValue) {
		$this->setValue($psValue);
	}

	/**
	 * Seta o atributo noescape
	 *
	 * <p>Seta o atributo noescape. True indica que o conte�do do elemento n�o
	 * deve ter as aspas escapadas e false, que deve.</p>
	 * @param string $pbNoEscape Indica se o conte�do do elemento vai ter as aspas escapadas ou n�o
	 */
	public function setAttrNoEscape($pbNoEscape){
		$this->coString->setAttrNoEscape($pbNoEscape);
	}

	/**
	 * Seta o nome do Label do objeto.
	 *
	 * <p>M�todo para setar o nome do label do objeto, utilizado para a troca de cor, no caso do objeto ter
	 * preenchimento obrigat�rio e n�o estar preenchido.</p>
	 * @access public
	 * @param string $psLabel nome do label associado ao objeto
	 */
	public function setAttrLabel($psLabel) {
		$this->csLabel = $psLabel;
	}

}
?>