<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDPack. Pacote de elementos.
 *
 * <p>Empacota um grupo de elementos, permitindo que eles sejam manipulados como
 * se fossem um s�.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDPack extends FWDView {

	/**
	 * Lista dos ids dos elementos que pertencem ao pacote.
	 * @var array
	 * @access protected
	 */
	protected $caElements = array();

	/**
	 * Desenha o bot�o.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o bot�o.</p>
	 * @access public
	 *
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawBody(){
		$msOut = "<input type='hidden' {$this->getGlobalProperty()}/>\n"
		."<script type='text/javascript'>"
		."gebi('{$this->csName}').object=new ".get_class($this)."('{$this->csName}');"
		."var soPack = gobi('{$this->csName}');";
		foreach($this->caElements as $msElementId){
			$msOut.= "soPack.addElement('{$msElementId}');";
		}
		$msOut.= "soPack = null;"
		."</script>";
		return $msOut;
	}

	public function getElements(){
		return $this->caElements;
	}

	/**
	 * Retorna o valor do atributo elements
	 *
	 * <p>Retorna o valor do atributo elements.</p>
	 * @access public
	 * @return array Valor do atributo elements
	 */
	public function getAttrElements(){
		return $this->caElements;
	}

	/**
	 * Seta o valor do atributo elements
	 *
	 * <p>Seta o valor do atributo elements.</p>
	 * @access public
	 * @param string $psElements Novo valor do atributo
	 */
	public function setAttrElements($psElements){
		$this->caElements = explode(':',$psElements);
	}

	/**
	 * Seta o valor booleano do atributo de display.
	 *
	 * <p>Seta o valor booleano do atributo que define se o elemento
	 * deve ser exibido. Para o elemento ser exibido, o atributo deve
	 * conter o valor booleano TRUE. Para n�o ser exibido, o atributo deve
	 * conter o valor booleano FALSE.</p>
	 * @access public
	 * @param string $psDisplay Define se o elemento deve ser exibido ou n�o
	 */
	public function setAttrDisplay($psDisplay){
		$this->cbDisplay = FWDWebLib::attributeParser($psDisplay,array("true"=>true, "false"=>false),"psDisplay");
		foreach($this->caElements as $msElementId){
			$moElement = FWDWebLib::getObject($msElementId);
			if($moElement){
				$moElement->setAttrDisplay($psDisplay);
			}
		}
	}

}

?>