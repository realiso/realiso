<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDDBGridPack. Pacote de DBGrids.
 *
 * <p>Empacota um grupo de elementos, permitindo que eles sejam manipulados como
 * se fossem um s�.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDDBGridPack extends FWDPack {

	/**
	 * M�todo executado quando o XML do elemento termina de carregar.
	 *
	 * <p>M�todo executado quando o XML do elemento termina de carregar.
	 * Cria o evento de refresh das grids.</p>
	 *
	 * @access public
	 */
	public function execute(){
		$moEventHandler = FWDStartEvent::getInstance();
		$moEventHandler->addAjaxEvent(new FWDDBGridPackRefresh("{$this->csName}_refresh",$this->csName));
	}

}

?>