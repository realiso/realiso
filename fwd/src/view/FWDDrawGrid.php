<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDDrawGrid. Implementa o Draw da Classe FWDGrid.
 *
 * <p>Classe que implementa o Draw que ser� utilizado pela Grid para
 * desenhar cada um dos elementos "c�lulas" da Grid, permitindo que
 * pelo PHP seja possivel sobrescrever o Draw para uma ou mais colunas,
 * desenhando algum outro elemento nessas colunas.</p>
 * @package FWD5
 * @subpackage view
 */

class FWDDrawGrid
{

	/**
	 * �ndice da linha do elemento que ser� desenhado, utilizado para o efeito de "zebra da grid"
	 * @var integer
	 * @access protected
	 */
	protected $ciRowIndex;

	/**
	 * �ndice da Coluna do elemento que ser� desenhado, utilizado para sobrescrever o Draw do Item da Grid
	 * @var integer
	 * @access protected
	 */
	protected $ciColumnIndex;

	/**
	 * Objeto que ser� desenhado na c�lula da grid.
	 * @var FWD"Object"
	 * @access protected
	 */
	protected $coCellBox;

	/**
	 * Css class para a div que conter� a c�lula da grid
	 * @var string
	 * @access protected
	 */
	protected $csDivUnitClass="";

	/**
	 * Array que cont�m os dados da linha correspondente ao cellUnit sendo renderizado.
	 * @var Array
	 * @access protected
	 */
	protected $caData = "";

	/**
	 * Define se deve ser exibido um tooltip com as informa��es da c�lula
	 * @var boolean
	 * @access protected
	 */
	protected $cbToolTip = false;

	protected $toolTipMaxLength = null;

	/**
	 * Define o id da coluna da grid que ser� o source do Tooltip da grid
	 * @var boolean
	 * @access protected
	 */
	protected $ciTTSource;

	/**
	 * Cont�m a string dos eventos de click da linha da grid
	 * @var boolean
	 * @access protected
	 */
	protected $bpRowEventBlock = false;

	/**
	 * Array associativo com os alias das colunas da grid
	 * @var Array
	 * @access protected
	 */
	protected $caAlias = array();

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDDrawGrid.</p>
	 * @access public
	 * @param FWDBox $poBox Box que n�o � utilizado nesse contrutor, mantido aqui s� por padr�o
	 */
	public function __construct($poBox = null)
	{

	}

	/**
	 * Desenha o Item da FWDDrawGrid.
	 *
	 * <p>M�todo para desenhar o Item que j� foi setado no objeto da FWDDrawGrid. Este � o
	 * m�todo que � sobrescrito para desenhar em uma ou mais colunas um outro objeto, diferente
	 *  dos objetos das outras colunas da grid</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do objeto que ser� desenhado
	 */
	public function drawItem($poBox = null)
	{
		return $this->coCellBox->drawHeader($poBox) . $this->coCellBox->drawBody($poBox) .$this->coCellBox->drawFooter($poBox);
	}


	/**
	 * Desenha o Item da FWDDrawGrid.
	 *
	 * <p>M�todo utilizado para ajustar os valores da Div que que limitar� o desenho do Item da grid.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do objeto que ser� desenhado
	 * @param string $clickLine Linha selecionada
	 */
	public function draw($poBox = null,$psCellName,$msclickLine = "")
	{
		if($poBox)
		{
			$moOriginalBox = clone $poBox ;
			$msOriginalClass = $this->coCellBox->getAttrClass();
			$miLeft=$poBox->getAttrLeft();
			$miWidth=$poBox->getAttrWidth();
			$miHeight=$poBox->getAttrHeight();
			$miTop=$poBox->getAttrTop();

			$moAuxBox=$this->coCellBox->getObjFWDBox();
			$moAuxBox->setAttrLeft(0);
			$moAuxBox->setAttrTop(0);
			$moAuxBox->setAttrWidth($poBox->getAttrWidth());
			$moAuxBox->setAttrHeight($miHeight);
			$this->coCellBox->setObjFWDBox($moAuxBox);

			$msAuxReturn = $this->drawItem($poBox);

			$msRenderEventClickLine = "";
			if(!$this->bpRowEventBlock){
				$moEvent = new FWDClientEvent();
				$moEvent->setAttrEvent('onClick');
				$moEvent->setAttrValue($msclickLine);
				$maOtherEvents = $this->coCellBox->getArrayEvent();
				$this->coCellBox->cleanEvents();
				$this->coCellBox->addObjFWDEvent($moEvent);
				foreach($maOtherEvents as $moObjEventHandler){
					foreach($moObjEventHandler->getAttrContent() as $moObjEvent){
						$this->coCellBox->addObjFWDEvent($moObjEvent);
					}
				}
			}

			$moToolTipSt = null;
			if ($this->cbToolTip){
				if($this->ciTTSource == $this->ciColumnIndex){
					$msToolTipText = strip_tags($this->coCellBox->getObjFWDString()->getAttrString());
				}
				else{
					$msToolTipText = $this->caData[$this->ciTTSource];
				}
				$msToolTipText = trim($msToolTipText);
				if($this->toolTipMaxLength && mb_strlen($msToolTipText, 'UTF8') > $this->toolTipMaxLength){
					$msToolTipText = mb_substr($msToolTipText, 0, $this->toolTipMaxLength-3, 'UTF-8') . "...";
				}
				if($msToolTipText!=''){
					$moToolTip = new FWDToolTip();
					$moToolTip->setAttrShowDelay(1000);
					//$msToolTipText = str_replace('\\','\\\\',$msToolTipText);
					//$msToolTipText = str_replace('"','\"',$msToolTipText);
					$moStr = new FWDString();
					$moStr->setAttrValue($msToolTipText);
					$moToolTip->setObjFWDString($moStr);
					$this->coCellBox->addObjFWDEvent($moToolTip);
				}
			}

			$msToolTipCode = '';
			$moToolTipSt = $this->coCellBox->getObjFWDToolTip();
			if($moToolTipSt){
				$msToolTipCode = $moToolTipSt->render();
			}
			$msStaticEvent = $this->coCellBox->getEvents();
			$msReturn =" <div id='$psCellName' class='{$this->csDivUnitClass}' {$msStaticEvent} {$msToolTipCode} style='height:{$miHeight};width:{$miWidth};left:{$miLeft};top:{$miTop};'>{$msAuxReturn}</div>";

			$this->coCellBox->setObjFWDBox($moOriginalBox);
			$this->coCellBox->setAttrClass($msOriginalClass);
			$this->bpRowEventBlock=false;
		}
		else
		{
			$msReturn = $this->drawItem($poBox);
		}
		return $msReturn;
	}

	/**
	 * Seta o Item que ser� desenhado na grid.
	 *
	 * <p>M�todo utilizado para setar na FWDDrawGrid o Objeto que ser� desenhado na grid e a linha correspondente desse objeto na gri.</p>
	 * @access public
	 * @param integer $piRowIndex que define a linha que pertence o objeto que ser� desenhado na grid
	 * @param FWD"Object" $poCellBox Objeto que ser� desenhado em uma c�lula da grid
	 */
	function setItem($piRowIndex,$piColumnIndex,$poCellBox,$poDivUnitClass,$paData,$piToolTip,$piSourceTT,$paAlias,$toolTipMaxLength)
	{
		$this->toolTipMaxLength   = $toolTipMaxLength;
		$this->cbToolTip   = $piToolTip;
		$this->ciRowIndex   = $piRowIndex;
		$this->coCellBox   = $poCellBox;
		$this->ciColumnIndex = $piColumnIndex;
		$this->csDivUnitClass = $poDivUnitClass;
		$this->caData = $paData;
		$this->ciTTSource = $piSourceTT;
		$this->caAlias = $paAlias;
	}

	/**
	 * Retorna o indice da coluna da grid correspondente ao alias passado.
	 *
	 * <p>Retorna o indice da coluna da grid correspondente ao alias passado.</p>
	 * @access public
	 * @param string $psAlias alias correspondente a coluna da grid que se quer descobrir o indice
	 * @return mixed index da coluna da grid correspondente ao alias passado para a fun��o
	 */
	public function getIndexByAlias($psAlias){
		$mxReturn = '';
		if($this->caAlias[$psAlias]){
			$mxReturn = $this->caAlias[$psAlias];
		}else{
			trigger_error("Trying to access an exist column index!",E_USER_ERROR);
		}
		return $mxReturn;
	}

	/**
	 * Retorna o alias da coluna da grid correspondente ao �ndice passado.
	 *
	 * <p>Retorna o alias da coluna da grid correspondente ao �ndice passado.</p>
	 * @access public
	 * @param integer $piIndex �ndice da coluna
	 * @return string Alias da coluna
	 */
	public function getAliasByIndex($piIndex){
		$mmAlias = array_search($piIndex,$this->caAlias);
		if($mmAlias===false){
			trigger_error("Inexistent column index '$piIndex'.",E_USER_ERROR);
		}
		return $mmAlias;
	}

	/**
	 * retorna o valor da coluna correspondende ao alias passapo para a fun��o
	 *
	 * <p>retorna o valor da coluna correspondende ao alias passapo para a fun��o.</p>
	 * @access public
	 * @param string $psAlias alias da coluna da grid que se quer obter o valor retornado pela query
	 * @return mixed index da coluna da grid correspondente ao alias passado para a fun��o
	 */
	public function getFieldValue($psAlias){
		$mxReturn = '';
		if(isset($this->caAlias[$psAlias])){
			if(isset($this->caData[$this->caAlias[$psAlias]])){
				$mxReturn = $this->caData[$this->caAlias[$psAlias]];
			}
		}else{
			trigger_error("Trying to access an inexistent alias ('$psAlias').",E_USER_ERROR);
		}
		return $mxReturn;
	}
}

?>