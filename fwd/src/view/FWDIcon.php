<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDIcon. Implementa �cones (imagens).
 *
 * <p>Classe simples para manipula��o de �cones (imagens, html tag 'img scr').</p>
 * @package FWD5
 * @subpackage view
 */
class FWDIcon extends FWDView {

	/**
	 * Source do �cone (path relativo ao sistema de arquivos local)
	 * @var string
	 * @access protected
	 */
	protected $csSrc = "";
	
	protected $top = "";
		
	/**
	 * Define a profundidade do �cone, para manipular sobreposi��o de elementos (zenith-index)
	 * @var integer
	 * @access protected
	 */
	protected $ciIndex = 0;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDIcon.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do �cone (imagem)
	 */
	public function __construct($poBox = null) {
		parent::__construct($poBox);
	}

	/**
	 * Desenha o cabe�alho do �cone.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o cabe�alho do �cone.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do �cone
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	protected function drawHeader($poFWDBox = null) {
		return "<img ";
	}

	/**
	 * Desenha o corpo do �cone.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o corpo do �cone.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do �cone
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	protected function drawBody($poFWDBox = null) {
		$msOut = "";

		$msSrc = $this->getAttrSrc();
		$msSrc = "src='{$msSrc}'";

		//$this->getImageBox(); /* Esse linha serve para funcionar com o XML compilado */
		$msStyle = $this->getStyle();

		if ($this->ciIndex != 0)
		$msStyle .= "z-index:".$this->getAttrIndex().";";

		if ($this->top)
		$msStyle .= "top:".$this->top.";";
			
		$msAttrDefault = $this->getGlobalProperty();

		$msOut .= "{$msSrc} style='{$msStyle} ' {$msAttrDefault} {$this->getEvents()}";
		return $msOut;
	}

	/**
	 * Desenha o rodap� do �cone.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o rodap� do �cone.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do �cone
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	protected function drawFooter($poFWDBox = null) {
		return " />";
	}

	/**
	 * Seta o source do �cone.
	 *
	 * <p>Seta o source do �cone (path da imagem). Seta tamb�m a altura
	 * e a largura do �cone.</p>
	 * @access public
	 * @param string $psSrc Source do �cone
	 */
	public function setAttrSrc($psSrc) {
		$this->csSrc = $psSrc;
		//$this->getImageBox();
	}

	/**
	 * Seta a profundidade do �cone.
	 *
	 * <p>Seta a profundidade do �cone (zenith-index).</p>
	 * @access public
	 * @param integer $piIndex Profundidade do �cone
	 */
	public function setAttrIndex($piIndex) {
		$this->ciIndex = $piIndex;
	}

	public function setAttrTop($top) {
		$this->top = $top;
	}

	/**
	 * Retorna o source do �cone.
	 *
	 * <p>Retorna o source do �cone (path da imagem).</p>
	 * @access public
	 * @return string Source do �cone
	 */
	public function getAttrSrc() {
		return $this->csSrc;
	}

	/**
	 * Retorna a profundidade do �cone.
	 *
	 * <p>Retorna a profundidade do �cone (zenith-index).</p>
	 * @access public
	 * @return integer Profundidade do �cone
	 */
	public function getAttrIndex() {
		return $this->ciIndex;
	}

	/**
	 * Seta a altura e a largura do �cone.
	 *
	 * <p>Seta a altura e a largura do �cone (imagem).</p>
	 * @access public
	 */
	public function getImageBox(){
		$msSrc = $this->csSrc;
		if(file_exists($msSrc)){
			$maBox = @getimagesize($msSrc);
			if ($maBox) {
				$this->coBox->setAttrWidth($maBox[0]);
				$this->coBox->setAttrHeight($maBox[1]);
			}
		}
	}

}
?>