<?
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDGetParameter.
 *
 * <p>Classe utilizada para receber, via xml, par�metros do GET.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDGetParameter {

	/**
	 * Nome do par�metro
	 * @var string
	 * @access protected
	 */
	protected $csParameter;

	/**
	 * Valor do par�metro
	 * @var string
	 * @access protected
	 */
	protected $csValue;

	/**
	 * Nome do objeto
	 * @var string
	 * @access protected
	 */
	protected $csName;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDParamenter.</p>
	 * @access public
	 */
	public function __construct() {}

	/**
	 * Atribui o nome do par�metro.
	 *
	 * @access public
	 * @param string $psParameter Nome do parametro
	 */
	public function setAttrParameter($psParameter) {
		$this->csParameter = $psParameter;
	}

	/**
	 * Atribui valor � vari�vel value.
	 *
	 * <p>Atribui valor � vari�vel value, devendo retirar espa�os em branco
	 * no inicio e/ou no fim da string.</p>
	 * @access public
	 * @param string $psValue Valor a ser atribu�do
	 */
	public function setValue($psValue) {
		$this->csValue = trim($psValue);
	}

	/**
	 * Atribui nome ao objeto.
	 *
	 * @access public
	 * @param string $psName Nome do objeto
	 */
	public function setAttrName($psName) {
		$this->csName = $psName;
	}

	/**
	 * Retorna o valor da vari�vel parameter.
	 *
	 * @access public
	 * @return string Nome do par�metro
	 */
	public function getAttrParameter() {
		return $this->csParameter;
	}

	/**
	 * Retorna o valor da vari�vel value.
	 *
	 * @access public
	 * @return string Valor do objeto
	 */
	public function getValue() {
		return $this->csValue;
	}

	/**
	 * Retorna o nome do objeto.
	 *
	 * @access public
	 * @return string Nome do objeto
	 */
	public function getAttrName() {
		return $this->csName;
	}

	/**
	 * Pega o valor do par�metro passado por GET.
	 *
	 * @access public
	 */
	public function execute() {
		isset($_GET[$this->getAttrParameter()]) ? $this->setValue($_GET[$this->getAttrParameter()]) : "";
	}
}
?>