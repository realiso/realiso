<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDSelect. Implementa menus drop down.
 *
 * <p>Classe que implementa menus drop down (html tag 'select').</p>
 * @package FWD5
 * @subpackage view
 */
class FWDSelect extends FWDView implements FWDPostValue, FWDCollectable {

	/**
	 * Define o n�mero de linhas do select
	 * @var integer
	 * @access protected
	 */
	protected $ciSize = 0;

	/**
	 * Define se o select suporta m�ltiplas sele��es
	 * @var boolean
	 * @access protected
	 */
	protected $cbMultiple = false;

	/**
	 * Array de objetos (FWDItem) do select
	 * @var array
	 * @access protected
	 */
	protected $caItem = array();

	/**
	 * Array de objetos (FWDOptgroup) do select
	 * @var array
	 * @access protected
	 */
	protected $caOptgroup = array();

	/**
	 * Nome do label associado ao objeto
	 * @var string
	 * @access protected
	 */
	protected $csLabel = "";

	/**
	 * Array com os valores do select
	 * @var array
	 * @access protected
	 */
	protected $caValues = array();

	/**
	 * Define se o select est� desabilitado ou n�o
	 * @var boolean
	 * @access protected
	 */
	protected $cbDisabled = false;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDSelect.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do select
	 */
	public function __construct($poBox = null) {
		parent::__construct($poBox);
	}

	/**
	 * Retorna as propriedades globais do elemento.
	 *
	 * <p>M�todo para retornar as propriedades globais do elemento.</p>
	 * @access public
	 * @return string Propriedades globais do elemento
	 */
	public function getGlobalProperty() {
		$msTabIndex = (($this->ciTabIndex)?"tabindex='{$this->ciTabIndex}'":"");
		$msAccessKey = (($this->csAccessKey)?"accesskey='{$this->csAccessKey}'":"");
		$msResize = (($this->cbResize)?"resize='true'":"");
		$msClass = (($this->csClass)?"class='{$this->csClass}'":"");

		$msName = $this->csName;
		if ($this->cbMultiple)
		$msName .= "[]";

		$msValue = "name='{$msName}' id='{$this->csName}' {$msTabIndex} {$msAccessKey} {$msResize} {$msClass}";
		return $msValue;
	}

	/**
	 * Desenha o cabe�alho do select.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o cabe�alho do select.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do select
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawHeader($poBox = null) {
		$msStyle = "style='{$this->getStyle()}'";

		$this->setAttrResize("false");

		$msMultiple = (($this->cbMultiple)?"multiple":"");

		$msSize = (($this->ciSize)?"size='{$this->ciSize}'":"");

		$msDisabled = $this->cbDisabled ? "disabled" : "";

		$msLabel="";
		if($this->csLabel)
		$msLabel = "label='{$this->csLabel}'";

		return "<select {$this->getGlobalProperty()} {$msSize} {$msDisabled} {$msMultiple} {$msStyle} {$msLabel} {$this->getEvents()}>\n";
	}

	/**
	 * Desenha o corpo do select.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o corpo do select.</p>
	 * @access public
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawBody() {
		$msOut = "";
		if (count($this->caOptgroup)) foreach ($this->caOptgroup as $moOptgroup) $msOut .= $moOptgroup->draw();
		else foreach ($this->caItem as $moItem) $msOut .= $moItem->draw();
		return $msOut;
	}

	/**
	 * Desenha o rodap� do select.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o rodap� do select.</p>
	 * @access public
	 *
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawFooter() {
		return "</select><script language='javascript'>new FWDSelect('{$this->csName}')</script>";
	}

	/**
	 * Adiciona um objeto de item.
	 *
	 * <p>Adiciona um objeto de item da classe FWDItem na classe FWDSelect.</p>
	 * @access public
	 * @param FWDItem $poItem Item
	 */
	public function addObjFWDItem(FWDItem $poItem) {
		$this->caValues[] = $poItem->getAttrKey();
		$this->caItem[$poItem->getAttrKey()] = $poItem;
	}

	/**
	 * Adiciona um objeto de optgroup.
	 *
	 * <p>Adiciona um objeto de optgroup da classe FWDOptgroup na classe FWDSelect.</p>
	 * @access public
	 * @param FWDOptgroup $poOptgroup Item
	 */
	public function addObjFWDOptgroup(FWDOptgroup $poOptgroup) {
		$poOptgroup->setSelect($this);
		$maItems = $poOptgroup->getItems();
		foreach ($maItems as $moItem)	$this->addObjFWDItem($moItem);
		$this->caOptgroup[] = $poOptgroup;
	}

	/**
	 * Seta um item do select como selecionado.
	 *
	 * <p>Seta um item (objeto) do select como selecionado.</p>
	 * @access public
	 * @param integer $piId Valor chave do item
	 */
	public function checkItem($piId) {
		if($this->cbMultiple) {
			if (isset($this->caItem[$piId]))
			$this->caItem[$piId]->setAttrCheck(true);
			else
			trigger_error("Invalid item key: '{$piId}'",E_USER_WARNING);
		}
		else {
			foreach($this->caItem as $moItem) {
				if($moItem->getAttrKey() == $piId)
				$moItem->setAttrCheck(true);
				else
				$moItem->setAttrCheck(false);
			}
		}
	}

	/**
	 * Seta o select como desabilitado.
	 *
	 * <p>M�todo para setar o select como desabilitado.</p>
	 * @access public
	 * @param boolean $pbDisabled Define se o select est� desabilitado ou n�o
	 */
	public function setAttrDisabled($pbDisabled) {
		$this->cbDisabled = FWDWebLib::attributeParser($pbDisabled,array("true"=>true, "false"=>false),"pbDisabled");
	}

	/**
	 * Retorna o valor booleano do atributo Disabled.
	 *
	 * <p>Retorna o valor booleano TRUE se o select est� desabilitado.
	 * Retorna FALSE caso contr�rio.</p>
	 * @access public
	 * @return boolean Indica se o select est� desabilitado ou n�o
	 */
	public function getAttrDisabled() {
		return $this->cbDisabled;
	}

	/**
	 * Seta o valor booleano do atributo de suporte a m�ltiplas sele��es.
	 *
	 * <p>Seta o valor booleano do atributo que define se m�ltiplas sele��es
	 * devem ser suportadas. Para o select suportar m�ltiplas sele��es,
	 * o atributo deve conter o valor booleano TRUE. Para n�o suportar,
	 * o atributo deve conter o valor booleano FALSE.</p>
	 * @access public
	 * @param string $psMultiple Define se m�ltiplas sele��es devem ser suportadas
	 */
	public function setAttrMultiple($psMultiple) {
		$this->cbMultiple = FWDWebLib::attributeParser($psMultiple,array("true"=>true, "false"=>false),"psMultiple");
	}

	/**
	 * Retorna o valor booleano do atributo de suporte a m�ltiplas sele��es.
	 *
	 * <p>Retorna o valor booleano TRUE se m�ltiplas sele��es devem ser
	 * suportadas. Retorna FALSE caso contr�rio.</p>
	 * @access public
	 * @return boolean Indica se m�ltiplas sele��es devem ser suportadas
	 */
	public function getAttrMultiple() {
		return $this->cbMultiple;
	}

	/**
	 * Seta o n�mero de linhas do select.
	 *
	 * <p>M�todo para setar o n�mero de linhas do select.</p>
	 * @access public
	 * @param integer $piSize Define o n�mero de linhas do select
	 */
	public function setAttrSize($piSize) {
		if ($piSize > 0)
		$this->ciSize = $piSize;
		else
		trigger_error("Invalid select size: '{$piSize}'",E_USER_WARNING);
	}

	/**
	 * Seta o nome do Label do objeto.
	 *
	 * <p>M�todo para setar o nome do label do objeto, utilizado para a troca de cor, no caso do objeto ter
	 * preenchimento obrigat�rio e n�o estar preenchido.</p>
	 * @access public
	 * @param string $psLabel nome do label associado ao objeto
	 */
	public function setAttrLabel($psLabel) {
		$this->csLabel = $psLabel;
	}

	/**
	 * Retorna o n�mero de linhas do select.
	 *
	 * <p>M�todo para retornar o n�mero de linhas do select.</p>
	 * @access public
	 * @return integer N�mero de linhas do select
	 */
	public function getAttrSize() {
		return $this->ciSize;
	}

	/**
	 * Retorna array de objetos do select.
	 *
	 * <p>Retorna array de objetos (itens) do select.</p>
	 * @access public
	 * @return array Array de objetos (FWDItem) do select
	 */
	public function getItems() {
		return $this->caItem;
	}

	/**
	 * Seta o valor de um item do select.
	 *
	 * <p>M�todo para setar o valor de um item do select. Caso o item n�o
	 * exista, ele � criado.</p>
	 * @access public
	 * @param integer $piId Id do Valor a ser atribu�do
	 * @param string $psValue Valor a ser atribu�do
	 */
	public function setItemValue($piId, $psValue = "") {
		if (!isset($this->caItem[$piId])) {
			$moItem = new FWDItem();
			$moItem->setAttrKey($piId);
			$moItem->setValue($psValue);
			$this->addObjFWDItem($moItem);
		}
		else {
			$this->caItem[$piId]->setValue($psValue);
		}
	}

	/**
	 * Atribui valor � vari�vel value.
	 *
	 * <p>M�todo para atribuir valor � vari�vel value.</p>
	 * @access public
	 * @param mixed $pmValue Valor a ser atribu�do
	 */
	public function setAttrValue($pmValue) {
		if(is_array($pmValue)){
			if($this->cbMultiple){
				foreach($pmValue as $msValue){
					$this->setItemValue($msValue);
					$this->checkItem($msValue);
				}
			}else{
				trigger_error('Trying to set multiple values in a FWDDBSelect not multiple.',E_USER_WARNING);
			}
		}else{
			$this->caValues = array($pmValue);
			$this->checkItem($pmValue);
		}
	}

	/**
	 * Atribui valor � vari�vel value.
	 *
	 * <p>M�todo para atribuir valor � vari�vel value.</p>
	 * @access public
	 * @param array $paValue Valor a ser atribu�do
	 */
	public function setValue($paValue, $pbForce = true) {
		$this->setAttrValue($paValue);
	}

	/**
	 * Retorna um array com as chaves de todos os items do select.
	 *
	 * <p>M�todo para retornar um array com as chaves de todos os items do select.</p>
	 * @access public
	 * @return array Chaves dos items
	 */
	public function getItemsKeys() {
		return array_keys($this->caItem);
	}

	/**
	 * Retorna um array com as chaves dos items selecionados.
	 *
	 * <p>M�todo para retornar um array com as chaves dos items selecionados.
	 * Caso seja sele��o simples, retorna um inteiro.</p>
	 * @access public
	 * @return array/integer Chaves dos items selecionados
	 */
	public function getValue() {
		if($this->cbMultiple){
			return $this->caValues;
		}else{
			if (count($this->caValues)) return $this->caValues[0];
			else return 0;
		}
	}

	/**
	 * Retorna um array com todos itens do select e o pr�prio select
	 *
	 * <p>Retorna um array contendo todos itens do select e o pr�prio select.</p>
	 * @access public
	 * @return array Array de itens do select e o pr�prio select
	 */
	public function collect() {
		$maOut = array($this);
		if ($this->caItem) $maOut = array_merge($maOut,$this->caItem);
		if ($this->caOptgroup) $maOut = array_merge($maOut,$this->caOptgroup);
		return $maOut;
	}

	/**
	 * Renderiza o c�digo javascript necess�rio para popular o select por Ajax.
	 *
	 * <p>Renderiza o c�digo javascript necess�rio para popular o select por Ajax.</p>
	 * @access public
	 */
	public function execEventPopulate(){
		echo "var soSel=gobi('{$this->csName}');";
		echo "soSel.clear();";
		foreach($this->caItem as $moItem){
			echo "soSel.addItem('{$moItem->getAttrKey()}','{$moItem->getValue()}','{$moItem->getAttrClass()}');";
		}
		echo "soSel=null;";
	}
}
?>