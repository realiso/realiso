<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDMenuItem. Implementa itens para o menu de contexto.
 *
 * <p>Classe que implementa itens para o menu de contexto.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDMenuItem extends FWDDrawing implements FWDCollectable {

	/**
	 * Nome do MenuItem
	 * @var string
	 * @access protected
	 */
	protected $csName = "";

	/**
	 * classe do item
	 * @var string
	 * @access protected
	 */
	protected $csClass = "FWDMenuItem";

	/**
	 * classe do item (onMouseOver)
	 * @var string
	 * @access protected
	 */
	protected $csClassOver = "FWDMenuItemOver";

	/**
	 * Valor do MenuItem
	 * @var FWDString
	 * @access protected
	 */
	protected $coString = null;

	/**
	 * Box do MenuItem
	 * @var FWDBox
	 * @access protected
	 */
	protected $coBox = null;

	/**
	 * Altura do MenuItem
	 * @var integer
	 * @access protected
	 */
	protected $ciHeight = 25;

	/**
	 * Margem do �cone em rela��o ao texto do MenuItem
	 * @var integer
	 * @access protected
	 */
	protected $ciIconMargin = 30;

	/**
	 * Margem da sombra do menu (canto superior direito e inferior esquerdo)
	 * @var integer
	 * @access protected
	 */
	protected $ciShadowMargin = 10;

	/**
	 * Largura da sombra do menu (em pixels)
	 * @var integer
	 * @access protected
	 */
	protected $ciShadowWidth = 7;

	/**
	 * Evento onClick suportado pelo MenuItem
	 * @var FWDEvent
	 * @access protected
	 */
	protected $caEvent = array();

	/**
	 * �cone (imagem clic�vel) do MenuItem
	 * @var FWDIcon
	 * @access protected
	 */
	protected $coIcon = null;

	/**
	 * Tag do MenuItem (permiss�es: define se o elemento deve ser desenhado ou n�o)
	 * @var string
	 * @access protected
	 */
	protected $csTag = "";

	/**
	 * �ndice do MenuItem (array(X,Y): X=>�ndice deste menuitem, Y=> total de menuitems)
	 * @var array
	 * @access protected
	 */
	protected $caIndex = array(1,1);

	/**
	 * Nome do Menu
	 * @var string
	 * @access protected
	 */
	protected $csMenuName = "";

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDMenuItem.</p>
	 * @access public
	 */
	public function __construct() {
		$this->coString = new FWDString();
		$this->csClass = get_class($this);
	}

	/**
	 * Atribui altura ao MenuItem.
	 *
	 * <p>Atribui valor a v�riavel height, valor esse considerado como a altura do intem de menu.</p>
	 * @access public
	 * @param string $psValue Valor a ser atribu�do
	 */
	public function setAttrHeight($psHeight) {
		$this->ciHeight = $psHeight;
	}

	/**
	 * Seta a tag do elemento.
	 *
	 * <p>M�todo para setar a tag do elemento.</p>
	 * @access public
	 * @param string $psTag Tag do elemento
	 */
	public function setAttrTag($psTag) {
		$this->csTag = $psTag;
	}

	/**
	 * Adiciona uma string.
	 *
	 * <p>Adiciona um objeto string da classe FWDString na classe FWDMenuItem.</p>
	 * @access public
	 * @param FWDString $poString �cone
	 */
	public function addObjFWDString(FWDString $poString) {
		$this->coString = $poString;
	}

	/**
	 * Adiciona um �cone.
	 *
	 * <p>Adiciona um objeto �cone da classe FWDIcon na classe FWDMenuItem.</p>
	 * @access public
	 * @param FWDIcon $poIcon �cone
	 */
	public function addObjFWDIcon(FWDIcon $poIcon) {
		$this->coIcon = $poIcon;
	}

	/**
	 * Seta eventos do MenuItem.
	 *
	 * <p>Seta os eventos suportados pelo MenuItem.</p>
	 * @access public
	 * @param FWDEvent $poEvent Evento
	 */
	public function addEvent($poEvent) {
		$this->caEvent[] = $poEvent;
	}

	/**
	 * Adiciona um objeto de evento.
	 *
	 * <p>Adiciona um objeto de evento da classe FWDEvent
	 * na classe FWDMenuItem.</p>
	 * @access public
	 * @param FWDEvent $poEvent Evento
	 */
	public function addObjFWDEvent(FWDEvent $poEvent) {
		$this->addEvent($poEvent);
	}

	/**
	 * Retorna a altura do MenuItem.
	 *
	 * <p>Retorna a altura do MenuItem.</p>
	 * @access public
	 * @return integer Altura do intem
	 */
	public function getAttrHeight()	{
		return $this->ciHeight;
	}

	/**
	 * Retorna o evento onClick suportado pelo MenuItem.
	 *
	 * <p>Retorna o evento onClick suportado pelo MenuItem.</p>
	 * @access public
	 * @return FWDEvent Evento onClick suportado pelo MenuItem
	 */
	public function getAttrEvent() {
		return $this->caEvent;
	}

	/**
	 * Retorna o �cone do MenuItem.
	 *
	 * <p>Retorna o �cone do MenuItem.</p>
	 * @access public
	 * @return FWDIcon �cone do MenuItem
	 */
	public function getAttrIcon() {
		return $this->coIcon;
	}

	/**
	 * Retorna a tag do elemento.
	 *
	 * <p>M�todo para retornar a tag do elemento.</p>
	 * @access public
	 * @return string Tag do elemento
	 */
	public function getAttrTag() {
		return $this->csTag;
	}

	/**
	 * Retorna o valor do MenuItem.
	 *
	 * <p>Retorna o valor do MenuItem.</p>
	 * @access public
	 * @return string Valor do MenuItem
	 */
	public function getValue() {
		return $this->coString->getAttrString();
	}

	/**
	 * Seta a classe do MenuItem.
	 *
	 * <p>M�todo para setar a classe do MenuItem.</p>
	 * @access public
	 * @param string $psClass Classe do MenuItem
	 */
	public function setAttrClass($psClass) {
		$this->csClass = $psClass;
	}

	/**
	 * Retorna a classe do MenuItem.
	 *
	 * <p>M�todo para retornar a classe do MenuItem.</p>
	 * @access public
	 * @return string Classe do MenuItem
	 */
	public function getAttrClass() {
		return $this->csClass;
	}

	/**
	 * Seta a classe do MenuItem (onMouseOver).
	 *
	 * <p>M�todo para setar a classe do MenuItem (onMouseOver).</p>
	 * @access public
	 * @param string $psClassOver Classe do MenuItem (onMouseOver)
	 */
	public function setAttrClassOver($psClassOver) {
		$this->csClassOver = $psClassOver;
	}

	/**
	 * Retorna a classe do MenuItem (onMouseOver).
	 *
	 * <p>M�todo para retornar a classe do MenuItem (onMouseOver).</p>
	 * @access public
	 * @return string Classe do MenuItem (onMouseOver)
	 */
	public function getAttrClassOver() {
		return $this->csClassOver;
	}

	/**
	 * Atribui valor, condicionalmente.
	 *
	 * <p>Atribui valor ao item, condicionando atrav�s do segundo par�metro.
	 * Se force for FALSE, concatena o valor se a vari�vel tiver conte�do;
	 * se TRUE atribui mesmo que tenha conte�do (sobrescreve).</p>
	 * @access public
	 * @param string $psValue Valor a ser atribu�do
	 * @param boolean $pbForce For�ar o valor mesmo que j� tenha conte�do
	 */
	public function setValue($psValue, $pbForce = true) {
		$this->coString->setValue($psValue, $pbForce);
	}

	/**
	 * Cria o MenuItem.
	 *
	 * <p>Cria o MenuItem do menu de contexto. Seta o nome e a box do
	 * elemento.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do MenuItem
	 * @param string $psName Nome do MenuItem
	 * @param array $paIndex array com o �ndice deste menuitem e o total de menuitems
	 * @param string $psMenuName Nome do Menu
	 */
	public function createMenuElement($poBox,$psName,$paIndex,$psMenuName) {
		$this->csName = $psName;
		$this->coBox = $poBox;
		$this->caIndex = $paIndex;
		$this->csMenuName = $psMenuName;
	}

	/**
	 * Desenha o cabe�alho do MenuItem.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o cabe�alho do MenuItem.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do MenuItem
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawHeader($poBox = null) {
		$msGlobalProperty = "name='{$this->csName}' id='{$this->csName}' class='{$this->csClass}'";
		$msStyle  = "left:{$this->coBox->getAttrLeft()};";
		$msStyle .= "top:{$this->coBox->getAttrTop()};";
		$msStyle .= "height:{$this->coBox->getAttrHeight()};";
		$msStyle .= "width:{$this->coBox->getAttrWidth()};";
		$msStyle .= "position:absolute;overflow:hidden;";

		$msEvents  = "onmouseover='this.className=\"{$this->csClassOver}\"; this.parentNode.parentNode.oncontextmenu=\"return false;\"; ' ";
		$msEvents .= "onmouseout='this.className=\"{$this->csClass}\";      this.parentNode.parentNode.oncontextmenu={$this->csMenuName}; '";

		if ($this->caEvent) {
			$msEvents .= "onclick=' ";
			foreach ($this->caEvent as $moEvent) {
				$msEvents .= $moEvent->render();
			}
			$msEvents .= "'";
		}

		return "\n<div {$msGlobalProperty} style='{$msStyle}' {$msEvents} >\n";
	}

	/**
	 * Desenha o corpo do MenuItem.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o corpo do MenuItem.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do MenuItem
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawBody($poBox = null) {
		$msOut = "";

		if ($this->coIcon) {
			$this->coIcon->getImageBox();
				
			$miIconTop = ceil($this->coBox->getAttrHeight() / 2);
			$miIconTop -= ceil($this->coIcon->getObjFWDBox()->getAttrHeight() / 2);
			$this->coIcon->getObjFWDBox()->setAttrTop($miIconTop);
				
			$miIconLeft = ceil($this->ciIconMargin / 2);
			$miIconLeft -= ceil($this->coIcon->getObjFWDBox()->getAttrWidth() / 2);
			$this->coIcon->getObjFWDBox()->setAttrLeft($miIconLeft);
			$this->coIcon->setAttrName($this->csName."_icon");
			$msOut .= $this->coIcon->draw();
		}

		$moStaticBox = new FWDBox($this->ciIconMargin, 0, $this->coBox->getAttrHeight(), $this->coBox->getAttrWidth()-$this->ciIconMargin);
		$moStatic = new FWDStatic($moStaticBox);
		$moStatic->setAttrName($this->csName."_static");
		$moStatic->setObjFWDString($this->coString);
		$moStatic->setAttrHorizontal("left");
		$moStatic->setAttrVertical("middle");
		$msOut .= $moStatic->draw();

		return $msOut;
	}

	/**
	 * Desenha o rodap� do MenuItem.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o rodap� do MenuItem.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do MenuItem
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawFooter($poBox = null) {
		$msOut = "</div>\n";

		if ($this->caIndex[0]==1) {
			if ($this->caIndex[0]==$this->caIndex[1]) {
				$msName = "menu_shadow_".$this->caIndex[0]."_".$this->caIndex[1];
				$msClass = "FWDMenuShadowRight";
				$msStyle  = "left:".($this->coBox->getAttrWidth()+1).";";
				$msStyle .= "top:{$this->ciShadowMargin};";
				$msStyle .= "height:".($this->coBox->getAttrHeight()-$this->ciShadowMargin+1).";";
				$msStyle .= "width:{$this->ciShadowWidth};";
				$msStyle .= "position:absolute;overflow:hidden;";
				if (FWDWebLib::browserIsIE())
				$msStyle .= "Filter: Alpha(Opacity=40, FinishOpacity=10, Style=1, StartX=0, StartY=0, FinishX=800, FinishY=0)";
				$msOut .= "<div name='{$msName}' id='{$msName}' class='{$msClass}' style='{$msStyle}'></div>\n\n";
			}
			else {
				$msName = "menu_shadow_".$this->caIndex[0]."_".$this->caIndex[1];
				$msClass = "FWDMenuShadowRight";
				$msStyle  = "left:".($this->coBox->getAttrWidth()+1).";";
				$msStyle .= "top:{$this->ciShadowMargin};";
				$msStyle .= "height:".($this->coBox->getAttrHeight()-$this->ciShadowMargin).";";
				$msStyle .= "width:{$this->ciShadowWidth};";
				$msStyle .= "position:absolute;overflow:hidden;";
				if (FWDWebLib::browserIsIE())
				$msStyle .= "Filter: Alpha(Opacity=40, FinishOpacity=10, Style=1, StartX=0, StartY=0, FinishX=800, FinishY=0)";
				$msOut .= "<div name='{$msName}' id='{$msName}' class='{$msClass}' style='{$msStyle}'></div>\n\n";
			}
		}

		else {
			if ($this->caIndex[0]==$this->caIndex[1]) {
				$msName = "menu_shadow_".$this->caIndex[0]."_".$this->caIndex[1];
				$msClass = "FWDMenuShadowRight";
				$msStyle  = "left:".($this->coBox->getAttrWidth()+1).";";
				$msStyle .= "top:{$this->coBox->getAttrTop()};";
				$msStyle .= "height:".($this->coBox->getAttrHeight()+1).";";
				$msStyle .= "width:{$this->ciShadowWidth};";
				$msStyle .= "position:absolute;overflow:hidden;";
				if (FWDWebLib::browserIsIE())
				$msStyle .= "Filter: Alpha(Opacity=40, FinishOpacity=10, Style=1, StartX=0, StartY=0, FinishX=800, FinishY=0)";
				$msOut .= "<div name='{$msName}' id='{$msName}' class='{$msClass}' style='{$msStyle}'></div>\n\n";
			}
			else {
				$msName = "menu_shadow_".$this->caIndex[0]."_".$this->caIndex[1];
				$msClass = "FWDMenuShadowRight";
				$msStyle  = "left:".($this->coBox->getAttrWidth()+1).";";
				$msStyle .= "top:{$this->coBox->getAttrTop()};";
				$msStyle .= "height:{$this->coBox->getAttrHeight()};";
				$msStyle .= "width:{$this->ciShadowWidth};";
				$msStyle .= "position:absolute;overflow:hidden;";
				if (FWDWebLib::browserIsIE())
				$msStyle .= "Filter: Alpha(Opacity=40, FinishOpacity=10, Style=1, StartX=0, StartY=0, FinishX=800, FinishY=0)";
				$msOut .= "<div name='{$msName}' id='{$msName}' class='{$msClass}' style='{$msStyle}'></div>\n\n";
			}
		}

		if ($this->caIndex[0]==$this->caIndex[1]) {
			$msName = "menu_shadow_bottom";
			$msClass = "FWDMenuShadowBottom";
			$msStyle  = "left:{$this->ciShadowMargin};";
			$msStyle .= "top:".($this->coBox->getAttrTop()+$this->coBox->getAttrHeight()+1).";";
			$msStyle .= "height:{$this->ciShadowWidth};";
			$msStyle .= "width:".($this->coBox->getAttrWidth()-$this->ciShadowMargin+1).";";
			$msStyle .= "position:absolute;overflow:hidden;";
			if (FWDWebLib::browserIsIE())
			$msStyle .= "Filter: Alpha(Opacity=40, FinishOpacity=10, Style=1, StartX=0, StartY=0, FinishX=0, FinishY=800)";
			$msOut .= "<div name='{$msName}' id='{$msName}' class='{$msClass}' style='{$msStyle}'></div>\n";
				
			$msName = "menu_shadow_corner";
			$msClass = "FWDMenuShadowCorner";
			$msStyle  = "left:".($this->coBox->getAttrWidth()+1).";";
			$msStyle .= "top:".($this->coBox->getAttrTop()+$this->coBox->getAttrHeight()+1).";";
			$msStyle .= "height:{$this->ciShadowWidth};";
			$msStyle .= "width:{$this->ciShadowWidth};";
			$msStyle .= "position:absolute;overflow:hidden;";
			if (FWDWebLib::browserIsIE())
			$msStyle .= "Filter: Alpha(Opacity=40, FinishOpacity=10, Style=1, StartX=0, StartY=0, FinishX=60, FinishY=60)";
			$msOut .= "<div name='{$msName}' id='{$msName}' class='{$msClass}' style='{$msStyle}'></div>\n\n";
		}
		return $msOut;
	}

	/**
	 * Retorna o array de eventos do elemento e o pr�prio elemento.
	 *
	 * <p>M�todo para retornar o array de eventos do elemento e o pr�prio elemento.</p>
	 * @access public
	 * @return array Eventos do elemento e elemento
	 */
	public function collect() {
		$maOut = array($this);
		$maOut = array_merge($maOut,$this->caEvent);
		return $maOut;
	}
}
?>