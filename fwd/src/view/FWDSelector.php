<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDSelector.
 *
 * <p>Select no qual cada item corresponde a um elemento da tela, e s� o selecionado fica vis�vel.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDSelector extends FWDSelect {

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDSelector.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do select
	 */
	public function __construct($poBox = null){
		parent::__construct($poBox);

		$moEvent = new FWDClientEvent();
		$moEvent->setAttrEvent('onChange');
		$moEvent->setAttrValue("this.object.select(this.value);");
		$this->addObjFWDEvent($moEvent);
	}

	/**
	 * Desenha o rodap� do select.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o rodap� do select.</p>
	 * @access public
	 *
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawFooter(){
		$msSelectedElement = $this->getValue();
		$msJs = "new FWDSelector('{$this->csName}','{$msSelectedElement}');try{";
		foreach($this->caItem as $moItem){
			$msElement = $moItem->getAttrKey();
			$moElement = FWDWebLib::getObject($msElement);
			if($moElement && $msElement!=$msSelectedElement){
				$msJs.= "js_hide('{$msElement}');";
				$moElement->setAttrDisplay('false');
			}
		}
		if($msSelectedElement){
			$msJs.= "js_show('{$msSelectedElement}');";
			if(FWDWebLib::getObject($msSelectedElement)){
				FWDWebLib::getObject($msSelectedElement)->setAttrDisplay('true');
			}else{
				trigger_error("Object '$msSelectedElement' not found.",E_USER_ERROR);
			}
		}
		$msJs.= "}catch(e){}";
		return "</select><script language='javascript'>{$msJs}</script>";
	}

}

?>