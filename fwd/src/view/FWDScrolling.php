<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDScrolling. Classe que implementa rolagem de texto sem barras de
 * rolagem.
 *
 * <p>Classe que implementa rolagem de texto sem barras de rolagem, o conte�do
 * do elemento rola quando o cursor do mouse � posicionado sobre uma das
 * setas.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDScrolling extends FWDView {

	/**
	 * Conte�do do Scrolling (� o Static que rola)
	 * @var FWDStatic
	 * @access protected
	 */
	protected $coContent = null;

	/**
	 * Indica a orienta��o da rolagem (vertical ou horizontal)
	 * @var string
	 * @access protected
	 */
	protected $csOrientation = 'horizontal';

	/**
	 * Velocidade da rolagem em pixels por segundo
	 * @var integer
	 * @access protected
	 */
	protected $ciSpeed = 100;

	/**
	 * Tamanho da aresta das imagens das setas (necessariamente quadradas)
	 * @var integer
	 * @access protected
	 */
	protected $ciImageSize = 9;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDScrolling.</p>
	 * @access public
	 */
	public function __construct(){
		parent::__construct();
	}

	/**
	 * Desenha o cabe�alho do objeto.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o cabe�alho do objeto.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do objeto
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawHeader($poBox = null){
		$this->setAttrTabIndex(0);
		$msStyle = "style='{$this->getStyle()}'";
		$msAttrDefault = $this->getGlobalProperty();
		$msEvents = $this->getEvents();
		$msOut = "<div $msAttrDefault $msStyle $msEvents>\n";
		return $msOut;
	}

	/**
	 * Desenha o corpo do objeto.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o corpo do objeto.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do objeto
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawBody(){
		$msOut = '';
		$miHeight = $this->coBox->getAttrHeight();
		$miWidth = $this->coBox->getAttrWidth();
		$moWebLib = FWDWebLib::getInstance();
		$msGfxRef = $moWebLib->getGfxRef();
		$moContentBox = $this->coContent->getObjFWDBox();
		$this->coContent->setAttrPosition('relative');
		$moContentBox->setAttrTop(0);
		$moContentBox->setAttrLeft(0);
		if($this->csOrientation=='vertical'){
			$moContentBox->setAttrHeight('');
			$moContentBox->setAttrWidth($this->coBox->getAttrWidth());
			$msDir1 = 'up';
			$msDir2 = 'down';
			$miBarWidth = $miWidth;
			$miBarHeight = $this->ciImageSize;
			$miWindowH = $miHeight - 2*$miBarHeight;
			$miWindowW = $miWidth;
			$miWindowTop = $miBarHeight;
			$miWindowLeft = 0;
			$miImageTop = 0;
			$miImageLeft = ($miWidth - $this->ciImageSize)/2;
			$miBar2Top = $miWindowH + $miBarHeight;
			$miBar2Left = 0;
			$msContentCode = $this->coContent->draw();
		}else{
			$moContentBox->setAttrWidth('');
			$moContentBox->setAttrHeight($this->coBox->getAttrHeight());
			$msDir1 = 'left';
			$msDir2 = 'right';
			$miBarWidth = $this->ciImageSize;
			$miBarHeight = $miHeight;
			$miWindowH = $miHeight;
			$miWindowW = $miWidth - 2*$this->ciImageSize;
			if(FWDWebLib::browserIsIE())
			$miWindowTop = -2;
			else
			$miWindowTop = -1;
			 
			$miWindowLeft = $this->ciImageSize;
			$miImageTop = ($miHeight - $this->ciImageSize)/2;
			$miImageLeft = 0;
			$miBar2Top = 0;
			$miBar2Left = $miWindowW + $miBarWidth;
			$msContentCode = str_replace('<td ','<td nowrap="nowrap" ',$this->coContent->draw());
		}
		$msOut.= "
      <div style='position:absolute;top:0;left:0;width:$miBarWidth;height:$miBarHeight;'>
        <img src='$msGfxRef$msDir1.gif' onMouseOver='startScroll(\"{$this->csName}\",\"$msDir1\",{$this->ciSpeed})' onMouseOut='stopScroll()' style='position:absolute;top:$miImageTop;left:$miImageLeft;'>
      </div>
      <div style='position:absolute;top:$miBar2Top;left:$miBar2Left;width:$miBarWidth;height:$miBarHeight;'>
        <img src='$msGfxRef$msDir2.gif' onMouseOver='startScroll(\"{$this->csName}\",\"$msDir2\",{$this->ciSpeed})' onMouseOut='stopScroll()' style='position:absolute;top:$miImageTop;left:$miImageLeft;'>
      </div>
      <div id='{$this->csName}_window' style='position:absolute;top:$miWindowTop;left:$miWindowLeft;width:$miWindowW;height:$miWindowH;overflow:hidden;'>
        <div id='{$this->csName}_content' style='position:absolute;left:0px;top:0px;'>
        $msContentCode
        </div>
      </div>
    ";
        return $msOut;
	}

	/**
	 * Desenha o rodap� do objeto.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o rodap� do objeto.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do objeto
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawFooter(){
		return "\n</div>\n";
	}

	/**
	 * Seta o Static que ser� o conte�do do Scrolling.
	 *
	 * <p>Seta o Static que ser� o conte�do do Scrolling.</p>
	 * @access public
	 * @param FWDStatic $poStatic Static que ser� o conte�do do Scrolling
	 */
	public function setObjFWDStatic(FWDStatic $poStatic){
		$poStatic->setAttrClass('FWDStaticScrolling');
		$this->coContent = $poStatic;
	}

	/**
	 * Retorna o valor do atributo orientation
	 *
	 * <p>Retorna o valor do atributo orientation.</p>
	 * @access public
	 * @return string Valor do atributo orientation
	 */
	public function getAttrOrientation(){
		return $this->csOrientation;
	}

	/**
	 * Seta o valor do atributo orientation
	 *
	 * <p>Seta o valor do atributo orientation.</p>
	 * @access public
	 * @param string $psOrientation Novo valor do atributo
	 */
	public function setAttrOrientation($psOrientation){
		$this->csOrientation = FWDWebLib::attributeParser($psOrientation,array("vertical"=>"vertical", "horizontal"=>"horizontal"),"orientation");
	}

	/**
	 * Retorna o valor do atributo speed
	 *
	 * <p>Retorna o valor do atributo speed.</p>
	 * @access public
	 * @return integer Valor do atributo speed
	 */
	public function getAttrSpeed(){
		return $this->ciSpeed;
	}

	/**
	 * Seta o valor do atributo speed
	 *
	 * <p>Seta o valor do atributo speed.</p>
	 * @access public
	 * @param integer $piSpeed Novo valor do atributo
	 */
	public function setAttrSpeed($piSpeed){
		$this->ciSpeed = $piSpeed;
	}

	/**
	 * Retorna um array com todos objetos do scrolling e o pr�prio scrolling
	 *
	 * <p>Retorna um array contendo todos objetos do scrolling e o pr�prio scrolling.</p>
	 * @access public
	 * @return array Array de objetos do scrolling e o pr�prio scrolling
	 */
	public function collect() {
		$maOut = array($this);
		if ($this->coContent)
		$maOut = array_merge($maOut,array($this->coContent));
		return $maOut;
	}
}
?>