<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDChartDirector. Integra gr�ficos do ChartDirector com a FWD
 *
 * <p>Classe para integrar gr�ficos do ChartDirector com a FWD.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDChartDirector extends FWDView {

	/**
	 * Formato da imagem do gr�fico
	 * @var integer
	 * @access protected
	 */
	protected $ciFormat = PNG;

	/**
	 * Formato da imagem do gr�fico (usado pra indicar o content-type)
	 * @var string
	 * @access protected
	 */
	protected $csFormat = 'png';

	/**
	 * Objeto do gr�fico
	 * @var BaseChart
	 * @access protected
	 */
	protected $coChart = null;

	/**
	 * C�digo HTML do mapa
	 * @var string
	 * @access protected
	 */
	protected $csMapCode = '';

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDChartDirector.</p>
	 * @access public
	 */
	public function __construct(){
		parent::__construct();
	}

	/**
	 * Desenha o cabe�alho do gr�fico.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o cabe�alho do gr�fico.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do gr�fico
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	protected function drawHeader($poBox = null){
		return '<img ';
	}

	/**
	 * Desenha o corpo do gr�fico.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o corpo do gr�fico.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do gr�fico
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	protected function drawBody($poBox = null){
		if($this->coChart===null){
			$msSrc = '';
			if($this->cbDisplay){
				trigger_error('Chart object not set.',E_USER_ERROR);
			}
		}else{
			$msImgId = $this->getChartName();
			$msSrc = FWDWebLib::getInstance()->getLibRef()."FWDGetImg.php?img=$msImgId&id=".uniqid(session_id());
		}
		return "src='$msSrc' {$this->getGlobalProperty()} {$this->getEvents()} style='{$this->getStyle()}'";
	}

	/**
	 * Desenha o rodap� do gr�fico.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o rodap� do gr�fico.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do gr�fico
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	protected function drawFooter($poBox = null){
		if($this->csMapCode!=''){
			$msMapId = $this->csName.'_map';
			return " usemap='#$msMapId'/><map name='$msMapId'>{$this->csMapCode}</map>";
		}else{
			return "/>";
		}
	}

	/**
	 * Seta o c�digo HTML do mapa
	 *
	 * <p>Seta o c�digo HTML do mapa.</p>
	 * @access public
	 * @param string $psMapCode C�digo HTML do mapa
	 */
	public function setMapCode($psMapCode){
		$this->csMapCode = $psMapCode;
	}

	/**
	 * Seta o objeto chart
	 *
	 * <p>Seta o objeto chart.</p>
	 * @access public
	 * @param BaseChart $poChart Objeto chart
	 */
	public function setChart($poChart){
		$this->coChart = $poChart;
		$msImgId = $this->getChartName();
		FWDWebLib::getInstance()->checkSession();
		$_SESSION[$msImgId] = $this->coChart->makeChart2($this->ciFormat);
		$_SESSION[$msImgId.'_contentType'] = $this->csFormat;
	}

	/**
	 * Retorna o valor do atributo format
	 *
	 * <p>Retorna o valor do atributo format.</p>
	 * @access public
	 * @return integer Valor do atributo format
	 */
	public function getAttrFormat(){
		return $this->ciFormat;
	}

	/**
	 * Seta o valor do atributo format
	 *
	 * <p>Seta o valor do atributo format.</p>
	 * @access public
	 * @param string $psFormat Novo valor do atributo
	 */
	public function setAttrFormat($psFormat){
		$this->ciFormat = FWDWebLib::attributeParser($psFormat,array('png'=>PNG,'jpg'=>JPG,'gif'=>GIF),"psFormat");
		$this->csFormat = ($psFormat=='jpg'?'jpeg':$psFormat);
	}

	/**
	 * Pega o nome do chart
	 *
	 * <p>Pega o nome do chart.</p>
	 * @access public
	 * @return string Nome do chart
	 */
	public function getChartName() {
		return "fwdchart_{$this->csName}";
	}

}
?>