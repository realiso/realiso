<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDButton. Implementa um bot�o.
 *
 * <p>Classe que implementa bot�o.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDButton extends FWDView {

	/**
	 * Define se a memo est� desabilitado ou n�o
	 * @var booleanDefine se a memo est� desabilitado ou n�o
	 * @access protected
	 */
	protected $cbDisabled = false;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDButton.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites da imagem clic�vel
	 */
	public function __construct($poBox = null) {
		parent::__construct($poBox);
	}

	/**
	 * Desenha o bot�o.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o bot�o.</p>
	 * @access public
	 *
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawBody() {
		$msDisabled = $this->cbDisabled ? "disabled" : "";
		return "<input type='button' {$this->getGlobalProperty()} style='{$this->getStyle()}'  value='{$this->getValue()}' {$this->getEvents()} {$msDisabled}/>\n";
	}

	/**
	 * Seta a memo como desabilitada.
	 *
	 * <p>M�todo para setar a memo como desabilitada.</p>
	 * @access public
	 * @param boolean $pbDisabled Define se a memo est� desabilitada ou n�o
	 */
	public function setAttrDisabled($pbDisabled) {
		$this->cbDisabled = FWDWebLib::attributeParser($pbDisabled,array("true"=>true, "false"=>false),"pbDisabled");
	}

	/**
	 * Retorna o valor booleano do atributo Disabled.
	 *
	 * <p>Retorna o valor booleano TRUE se a memo est� desabilitada.
	 * Retorna FALSE caso contr�rio.</p>
	 * @access public
	 * @return boolean Indica se a memo est� desabilitada ou n�o
	 */
	public function getAttrDisabled() {
		return $this->cbDisabled;
	}
}
?>