<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDCheckBox. Implementa Check Boxes.
 *
 * <p>Classe que implementa Check Boxes (imagens clic�veis com atributos
 * especiais e suporte a eventos). HTML tag 'input type="checkbox"'.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDCheckBox extends FWDItemController {

	/**
	 * Define se todas as CheckBoxes devem ser selecionadas ou n�o
	 * @var boolean
	 * @access protected
	 */
	protected $cbSelectAll = false;

	/**
	 * Define se o checkbox est� desabilitado
	 * @var boolean
	 * @access protected
	 */
	protected $cbDisabled = false;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDCheckBox.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites da CheckBox
	 */
	public function __construct($poBox = null){
		parent::__construct($poBox);
	}

	/**
	 * Desenha o cabe�alho do checkbox.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o cabe�alho do checkbox.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do checkbox
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawHeader($poBox = null){
		$moWebLib = FWDWebLib::getInstance();
		$msGFX = $moWebLib->getGfxRef();
		$msController = $this->getAttrController();
		$msKey = $this->csKey;
		$msName = $msController."_".$msKey;
		$msSrc = ($this->getAttrCheck()?$msGFX."check.gif":$msGFX."uncheck.gif");
		$msJs = "gfx_check(\"$msController\",\"$msKey\");";
		$msClass = $this->getAttrClass();
		$moEvent = new FWDClientEvent();
		$moEvent->setAttrEvent("OnClick");
		$moEvent->setAttrValue($msJs);
		$this->addObjFWDEvent($moEvent);
		$msStyle = "{$this->getStyle()};cursor:pointer";
		return  "\n<img name='{$msName}' id='$msName' src='$msSrc' style='$msStyle' class='$msClass' {$this->getEvents()}/>\n";
	}

	/**
	 * Desenha o rodap� do checkbox.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o rodap� do checkbox.</p>
	 * @access public
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawFooter() {
		$msName = $this->csController."_".$this->csKey;
		$msDisabled = "";
		if($this->cbDisabled)
		$msDisabled .="gobi('{$msName}').disable();";
		if($this->csName)
		return "\n <script language='javascript'>gebi('{$msName}').object = new FWDCheckBox('{$msName}','{$this->csController}','{$this->csKey}');$msDisabled </script> ";
		else
		return "";
	}

	/**
	 * Seta o atributo de SelectAll.
	 *
	 * <p>Seta o valor booleano do atributo de SelectAll. Para que todas as
	 * Check Boxes sejam selecionadas, o atributo pbSelectAll deve conter o valor
	 * booleano TRUE. Caso contr�rio, deve conter o valor booleano FALSE.</p>
	 * @access public
	 * @param string $psSelectAll Define se todas as Check Boxes devem ser selecionadas
	 */
	public function setAttrSelectAll($psSelectAll){
		$this->cbSelectAll = FWDWebLib::attributeParser($psSelectAll,array("true"=>true, "false"=>false),"psSelectAll");
	}

	/**
	 * Retorna o valor booleano do atributo de SelectAll.
	 *
	 * <p>Retorna o valor booleano do atributo de SelectAll.</p>
	 * @access public
	 * @return boolean Indica se todas as Check Boxes devem ser selecionadas ou n�o
	 */
	public function getAttrSelectAll(){
		return $this->cbSelectAll;
	}

	/**
	 * Seta a memo como desabilitada.
	 *
	 * <p>M�todo para setar a memo como desabilitada.</p>
	 * @access public
	 * @param boolean $pbDisabled Define se a memo est� desabilitada ou n�o
	 */
	public function setAttrDisabled($pbDisabled) {
		$this->cbDisabled = FWDWebLib::attributeParser($pbDisabled,array("true"=>true, "false"=>false),"pbDisabled");
	}

	/**
	 * Retorna o valor booleano do atributo Disabled.
	 *
	 * <p>Retorna o valor booleano TRUE se a memo est� desabilitada.
	 * Retorna FALSE caso contr�rio.</p>
	 * @access public
	 * @return boolean Indica se a memo est� desabilitada ou n�o
	 */
	public function getAttrDisabled() {
		return $this->cbDisabled;
	}
}
?>