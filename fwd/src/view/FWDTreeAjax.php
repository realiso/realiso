<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDTreeAjax. Implementa �rvores com suporte a eventos Ajax.
 *
 * <p>Classe que implementa �rvores em javascript com suporte a eventos Ajax.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDTreeAjax extends FWDTree {

	/**
	 * Fun��o do evento Ajax. OBS: N�o esta sendo usado, por enquanto.
	 * @var string
	 * @access protected
	 */
	protected $csFunction;

	/**
	 * Par�metros do evento Ajax. OBS: N�o esta sendo usado, por enquanto.
	 * @var string
	 * @access protected
	 */
	protected $csParameter;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDTreeAjax.</p>
	 * @access public
	 */
	public function __construct(){
		parent::__construct();
	}

	/**
	 * Desenha o corpo do objeto.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o corpo do objeto.</p>
	 * @access public
	 * @param boolean $pbLastSibling Verdadeiro se for o �ltimo elemento
	 * @param FWDBox $poBox Box que define os limites do objeto
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	protected function drawBody($pbLastSibling,$poBox = null){
		$moWebLib = FWDWebLib::getInstance();
		$msGFX = $moWebLib->getGfxRef();
		$msId = "{$this->getTreeBaseName()}_{$this->csId}";
		$miNumNodes = count($this->caNodes);
		$mbOpen = false;
		$msStyleWithBackground = "style='vertical-align:top;background:url({$msGFX}line.gif);background-repeat:repeat-y;background-position:top;'";
		$msStyleWithoutBackground = "style='vertical-align:top;'";
		if($miNumNodes>0){
			$moEvent = new FWDAjaxEvent('',$this->getAttrName().'_expand');
		}else{
			$moEvent = new FWDAjaxEvent('',$this->getAttrFunction());
		}
		$msEvent = str_replace("\"","'",$moEvent->render());
		$msOut = '';
		$msTreeClass = $this->getAttrClass();

		$maCollection = $this->coViewGroup->collect();
		if(count($maCollection)>0){
			foreach($maCollection as $key => $moView) $moView->setAttrPosition('relative');
		}
		$this->coViewGroup->setAttrName($this->getAttrName().'_vg');

		$msOut.= "<tr class='{$msTreeClass}_tr'>\n";
		$msOut.= "<td class='{$msTreeClass}_td' ".($pbLastSibling?$msStyleWithoutBackground:$msStyleWithBackground).">\n";
		if($miNumNodes>0){
			$msOut.= "<a id='{$msId}_a' href=\"javascript:toggle('$msId');$msEvent\"><img id='{$msId}_img' src='{$msGFX}".($mbOpen?'minus':'plus').".gif'></a>\n";
		}else{
			$msOut.= "<img id='{$msId}_img' src='{$msGFX}join.gif'>\n";
		}
		$msOut.= "</td>\n";
		$msOut.= "<td class='{$msTreeClass}_td'>\n";
		$msOut.= $this->coViewGroup->drawHeader();

		$maContent = $this->coViewGroup->getContent();
		$mbNeedshift = false;
		foreach($maContent as $moContent){
			if($moContent instanceof FWDStatic){
				$moContent->setAttrClass('StaticTree');
			}
			$msOut .= $moContent->draw();
			if($this->getAttrAutoFill() && $moContent instanceof FWDCheckBox){
				$msOut .= "<script> addTreeCheckBox('".$moContent->getAttrController()."', '".$moContent->getAttrKey()."');</script>";
			}
		}
		$msOut .= $this->coViewGroup->drawFooter();

		$msOut.= "</td>\n";
		$msOut.= "</tr>\n";
		if($miNumNodes>0){
			$msOut.= "<tr id='{$msId}_tr' class='{$msTreeClass}_tr'".($mbOpen?'':' style="display:none"').">\n";
			$msOut.= "<td class='{$msTreeClass}_td' ".($pbLastSibling?$msStyleWithoutBackground:$msStyleWithBackground)."></td>\n";
			$msOut.= "<td id='{$msId}_td' class='{$msTreeClass}_td'>\n";
			$msOut.= '<span class="FWDLoading" style="position:relative;">'.FWDLanguage::getPHPStringValue('FWDLoading','Carregando')."<img style='margin-top:9px' src='{$msGFX}loading.gif'/></span>";
			$msOut.= "</td>\n";
			$msOut.= "</tr>\n";
		}
		return $msOut;
	}

	/**
	 * Seta a fun��o do evento Ajax.
	 *
	 * <p>Seta a fun��o do evento Ajax.</p>
	 * @access public
	 * @param string $psFunction Fun��o do evento Ajax
	 */
	public function setAttrFunction($psFunction){
		$this->csFunction = $psFunction;
	}

	/**
	 * Retorna a fun��o do evento Ajax.
	 *
	 * <p>Retorna a fun��o do evento Ajax.</p>
	 * @access public
	 * @return string Fun��o do evento Ajax
	 */
	public function getAttrFunction(){
		return $this->csFunction;
	}

	/**
	 * Seta os par�metros do evento Ajax.
	 *
	 * <p>Seta os par�metros do evento Ajax.</p>
	 * @access public
	 * @param string $psParameter Par�metros do evento Ajax
	 */
	public function setAttrParameter($psParameter){
		$this->csParameter = $psParameter;
	}

	/**
	 * Retorna os par�metros do evento Ajax.
	 *
	 * <p>Retorna os par�metros do evento Ajax.</p>
	 * @access public
	 * @return string Par�metros do evento Ajax
	 */
	public function getAttrParameter(){
		return $this->csParameter;
	}

	/**
	 * M�todo executado quando o XML do elemento termina de carregar.
	 *
	 * <p>M�todo executado quando o XML do elemento termina de carregar. Se a
	 * �rvore tem filhos, independentemente do atributo function, o evento �
	 * FWDTreeAjaxExpand.</p>
	 *
	 * @access public
	 */
	public function execute(){
		if(count($this->caNodes)>0){
			$msEventName = $this->getAttrName().'_expand';
			$StartEvent_instance = FWDStartEvent::getInstance();
			$StartEvent_instance->addAjaxEvent(new FWDTreeAjaxExpand($msEventName,$this));
		}
	}

	/**
	 * Expande a �rvore, carregando seus filhos do XML
	 *
	 * <p>Retorna o c�digo em javascript que expande a �rvore, adicionando seus
	 * filhos carregados a partir do XML.</p>
	 *
	 * @access public
	 * @return string C�digo javascript
	 */
	public function expand(){
		$msId = "{$this->getTreeBaseName()}_{$this->csId}";
		$msContent = '';
		$miNumNodes = count($this->caNodes);
		$js = "";
		for($miI=0;$miI<$miNumNodes-1;$miI++){
			$msContent.= $this->caNodes[$miI]->draw(false);
			if($this->getAttrAutoFill()){
				$maContent = $this->caNodes[$miI]->coViewGroup->getContent();
				foreach($maContent as $moContent){
					if($moContent instanceof FWDCheckBox){
						$js .= " addTreeCheckBox('".$moContent->getAttrController()."', '".$moContent->getAttrKey()."');";
					}
				}
			}
		}
		$msContent.= $this->caNodes[$miI]->draw(true);
		if($this->getAttrAutoFill()){
			$maContent = $this->caNodes[$miI]->coViewGroup->getContent();
			foreach($maContent as $moContent){
				if($moContent instanceof FWDCheckBox){
					$js .= " addTreeCheckBox('".$moContent->getAttrController()."', '".$moContent->getAttrKey()."');";
				}
			}
		}
		$msContent = str_replace(array("\n","\r","'"),array(" "," ","\'"),$msContent);
		$msOut = "gebi('{$msId}_td').innerHTML='{$msContent}';gebi('{$msId}_a').href=\"javascript:toggle('{$msId}');\";tt_Init(false,'');".$js;
		return $msOut;
	}
}
?>