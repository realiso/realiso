<?
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDViewButton. Implementa um elemento clic�vel.
 *
 * <p>Classe que implementa um elemento clic�vel.</p>
 *
 * @package FWD5
 * @subpackage view
 */
class FWDViewButton extends FWDViewGroup {

	/**
	 * Atributo disabled do viewButton
	 * @var boolean
	 * @access protected
	 */
	private $cbDisabled = false;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDViewButton.</p>
	 * @access public
	 * @param FWDBox poBox Box que define os limites da �rea do elemento
	 */
	public function __construct($poBox = null) {
		parent::__construct($poBox);
	}

	/**
	 * Desenha o cabe�alho do viewbuttom.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o cabe�alho do viewbuttom.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do viewbuttom
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawHeader($poBox = null) {
		$this->setAttrTabIndex(0);

		$msOut = '';
		//div da imagem que fica por cima da div do viewbutton
		if(($this->cbDisplay==true))
		$mbShowIt = true;
		else
		$mbShowIt = false;
		 
		$msImgStyle = $this->coBox->draw();
		$msImgStyle.= $mbShowIt ? "display:block;" : "display:none;";
		$msImgStyle.= "position:{$this->getAttrPosition()};";
		$msImgStyle.= $this->csDebug ? $this->debugView() : '';
		$msOut .= "  <div id='{$this->csName}_dvImg' name='{$this->csName}_dvImg' class='FWDViewButtonTransp' style='{$msImgStyle}' {$this->getEvents()}></div>\n";

		//div para o viewButton disabled
		$msImgDisableStyle = $this->coBox->draw();
		$msImgDisableStyle.= "position:{$this->getAttrPosition()};";
		$msImgDisableStyle.= ($this->cbDisabled && $this->cbDisplay)? "display:block;" : "display:none;";
		if (FWDWebLib::browserIsIE())
		$msImgDisableStyle .= " Filter: Alpha(Opacity=50); ";
		else
		$msImgDisableStyle .= " opacity:0.5;";
		$msOut .= "  <div id='{$this->csName}_dvImgDisabled' name='{$this->csName}_dvImgDisabled' class='FWDViewButtonNotTransp' style='{$msImgDisableStyle}'></div>\n";

		$msOut .= "  <div {$this->getGlobalProperty()} style='{$this->getStyle()}'>\n";
		return $msOut;
	}
	 
	/**
	 * Desenha o rodap� do viewbutton.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o rodap� do viewbutton.</p>
	 * @access public
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawFooter() {
		if($this->csName)
		return "\n  </div> <script language='javascript'>gebi('{$this->csName}').object = new FWDViewButton('{$this->csName}','{$this->cbDisabled}'); </script> ";
		else
		return "\n  </div> \n";
	}
	 
	/**
	 * Seta o campo como desabilitado.
	 *
	 * <p>M�todo para setar o campo como desabilitado.</p>
	 * @access public
	 * @param boolean $pbDisabled Define se o campo est� desabilitado ou n�o
	 */
	public function setAttrDisabled($pbDisabled) {
		$this->cbDisabled = FWDWebLib::attributeParser($pbDisabled,array("true"=>true, "false"=>false),"pbDisabled");
	}

	/**
	 * Retorna o valor booleano do atributo Disabled.
	 *
	 * <p>Retorna o valor booleano TRUE se o campo est� desabilitado.
	 * Retorna FALSE caso contr�rio.</p>
	 * @access public
	 * @return boolean Indica se o campo est� desabilitado ou n�o
	 */
	public function getAttrDisabled() {
		return $this->cbDisabled;
	}
}
?>