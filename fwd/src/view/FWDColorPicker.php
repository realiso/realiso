<?
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

class FWDColorPicker extends FWDDrawing {

	private $cbDisabled = false;

	private $selectedColor = "";
	
	private $name = "";

	public function drawHeader($poBox = null) {
		return "
		<script type='text/javascript'>
		addary = new Array();addary[0] = new Array(0, 1, 0);addary[1] = new Array(-1, 0, 0);addary[2] = new Array(0, 0, 1);addary[3] = new Array(0, -1, 0);
		addary[4] = new Array(1, 0, 0);addary[5] = new Array(0, 0, -1);addary[6] = new Array(255, 1, 1);clrary = new Array(360);
		for (i = 0; i < 6; i++)
			for (j = 0; j < 60; j++) {
				clrary[60 * i + j] = new Array(3);
				for (k = 0; k < 3; k++) {
					clrary[60 * i + j][k] = addary[6][k];
					addary[6][k] += (addary[i][k] * 4);
				}
			}
		hexary = new Array(\"#666666\", \"#555555\", \"#545657\");
		picary = new Array(\"picka\");
		initary = new Array(\"#444444\", \"#777777\", \"#aaaaaa\", \"#bbbbbb\", \"#cccccc\",
				\"#dddddd\", \"#eeeeee\");
		pickindex = 0;
		function capture() {
			hoverColor();
			initColor();
			if (document.layers) {
				layobj = document.layers['wheel'];
				layobj.document.captureEvents(Event.MOUSEMOVE);
				layobj.document.onmousemove = mouseMoved;
			} else if (document.all) {
				layobj = document.all[\"wheel\"];
				layobj.onmousemove = mouseMoved;
			} else if (document.getElementById) {
				window.document.getElementById(\"wheel\").onmousemove = mouseMoved;
			}
		}
		function mouseMoved(e) {
			if (document.layers) {
				x = 4 * e.layerX;
				y = 4 * e.layerY;
			} else if (document.all) {
				x = 4 * event.offsetX;
				y = 4 * event.offsetY;
			} else if (document.getElementById) {
				x = 4 * (e.pageX - document.getElementById(\"wheel\").offsetLeft);
				y = 4 * (e.pageY - document.getElementById(\"wheel\").offsetTop);
			}
			sx = x - 512;
			sy = y - 512;
			qx = (sx < 0) ? 0 : 1;
			qy = (sy < 0) ? 0 : 1;
			q = 2 * qy + qx;
			quad = new Array(-180, 360, 180, 0);
			xa = Math.abs(sx);
			ya = Math.abs(sy);
			d = ya * 45 / xa;
			if (ya > xa)
				d = 90 - (xa * 45 / ya);
			deg = Math.floor(Math.abs(quad[q] - d));
			n = 0;
			sx = Math.abs(x - 512);
			sy = Math.abs(y - 512);
			r = Math.sqrt((sx * sx) + (sy * sy));
			if (x == 512 & y == 512) {
				c = \"000000\";
			} else {
				for (i = 0; i < 3; i++) {
					r2 = clrary[deg][i] * r / 256;
					if (r > 256)
						r2 += Math.floor(r - 256);
					if (r2 > 255)
						r2 = 255;
					n = 256 * n + Math.floor(r2);
				}
				c = n.toString(16);
				while (c.length < 6)
					c = \"0\" + c;
			}
			hexary[1] = \"#\" + c.charAt(0) + c.charAt(0) + c.charAt(2) + c.charAt(2)
					+ c.charAt(4) + c.charAt(4);
			hexary[2] = \"#\" + c;
			hexary[0] = safetyFirst(c);
			hoverColor();
			return false;
		}
		function hoverColor() {
			if (document.layers) {
				document.layers[\"wheel\"].bgColor = hexary[1];
				document.layers[\"democ\"].bgColor = hexary[2];
				document.getElementById(\"newColor\").value = hexary[2];
			} else if (document.all) {
				document.all[\"wheel\"].style.backgroundColor = hexary[1];
				document.all[\"democ\"].style.backgroundColor = hexary[2];
				document.getElementById(\"newColor\").value = hexary[2];
			} else if (document.getElementById) {
				document.getElementById(\"wheel\").style.backgroundColor = hexary[1];
				document.getElementById(\"democ\").style.backgroundColor = hexary[2];
				document.getElementById(\"newColor\").value = hexary[2];
			}
			return false;
		}
		
		function pickColor() {
			document.getElementById(\"choice\").value = hexary[2];
			document.getElementById(\"picka\").style.backgroundColor = hexary[2];
			pickindex += 1;
			if (pickindex >= picary.length)
				pickindex = 0;
		}
		function initColor() {
		}
		function safetyFirst(c) {
			cary = new Array(c.charAt(0), c.charAt(2), c.charAt(4));
			for (ci = 0; ci < 3; ci++) {
				switch (cary[ci]) {
				case \"1\":
					cary[ci] = \"0\";
					break;
				case \"2\":
					cary[ci] = \"3\";
					break;
				case \"4\":
					cary[ci] = \"3\";
					break;
				case \"5\":
					cary[ci] = \"6\";
					break;
				case \"7\":
					cary[ci] = \"6\";
					break;
				case \"8\":
					cary[ci] = \"9\";
					break;
				case \"a\":
					cary[ci] = \"9\";
					break;
				case \"b\":
					cary[ci] = \"c\";
					break;
				case \"d\":
					cary[ci] = \"c\";
					break;
				case \"e\":
					cary[ci] = \"f\";
					break;
				}
			}
			safecolor = \"#\" + cary[0] + cary[0] + cary[1] + cary[1] + cary[2] + cary[2];
			return safecolor;
		}
		</script>";
	}

	public function drawBody($poBox = null) {
		$weblib_instance = FWDWebLib::getInstance();
		$gfx_ref = $weblib_instance->getGfxRef();
	  
		$body = "

		<style type='text/css'>
		h1 {
			position: absolute;
			visibility: visible;
			top: 30px;
			left: 80px;
			font-family: 'Trebuchet MS', Skia, 'Lucida Sans Unicode', sans-serif;
			font-size: x-large;
		}
		
		div#wheel {
			position: absolute;
			visibility: visible;
			top: 15px;
			left: 25px; 
			width: 256px;
			height: 256px;
		}
		
		div#democ,div#picka {
			visibility: visible;
			float: left;
			width: 95px;
			height: 100px;
			text-align: center;
			top: 51px;
			position: absolute;
			padding-top: 15px;
		}
		
		div#picka {
			left: 120px;
		}
		
		div#rights {
			position: absolute;
			visibility: visible;
			left: 300px;
			width: 250px;
		}
		
		div#instructs {
			font-family: 'Trebuchet MS', Skia, 'Lucida Sans Unicode', sans-serif;
		}
		
		div#notes {
			padding-top: 30px;
			font-size: smaller;
		}
		
		input#choice,input#newColor{
			margin-bottom: 16px;
		}
		
		.btxt {
			color: #000000;
		}
		
		.wtxt {
			color: #ffffff;
		}
		
		div#btContainer {
			float: left;
			height: 60px;
			width: 230px;
		}
		</style>
		
		<!-- on the left, the wheel and display -->
		<div id='wheel'><a href='javascript://'
			onclick='javascript:pickColor(); return false'><img
			src='$gfx_ref/colorwheel.png' alt='color wheel' width='256' height='256'
			border='0'></a></div>
		
		<!-- on the right, the picks and instructions -->
		
		<div id='rights'>
		<div id='instructs'>
		<p>".FWDLanguage::getPHPStringValue('colorHelper1', 'Passe o mouse acima da paleta para ver as cores.')."<br>
		".FWDLanguage::getPHPStringValue('colorHelper2', 'Clique para escolher uma cor.')."<br>
		</p>
		</div>
		<div id='democ'>
		<input type='text' id='newColor' name='newColor' size='9'>
		<span class='btxt'>".FWDLanguage::getPHPStringValue('newColor', 'Nova Cor')."</span><br>
		<span class='wtxt'>".FWDLanguage::getPHPStringValue('newColor', 'Nova Cor')."</span></div>
		<div id='picka' style='background-color: ".$this->selectedColor."'>
			<input type='text' name='choice' id='choice' size='9' value='".$this->selectedColor."'>
			<span class='btxt'>".FWDLanguage::getPHPStringValue('choosenColor', 'Cor Escolhida')."</span><br>
			<span class='wtxt'>".FWDLanguage::getPHPStringValue('choosenColor', 'Cor Escolhida')."</span></div>
		</div>";
		$button = $this->getButtonClose();
		$buttonSave = $this->getButtonSave();
		$body .= $button->draw($poBox);
		$body .= $buttonSave->draw($poBox);
		$body .= "</div>

		";

		return $body;
	}

	public function drawFooter($poBox){
		return "<script>
					capture();
					
					function setColor(){
						var choice = document.getElementById('choice');
						if(choice && choice.value && choice.value.length > 0){
							soPopUp = soPopUpManager.getPopUpById('colorPickerPopup');
							soWindow = soPopUp.getOpener();
						
							if(soWindow.setColor){
								soWindow.setColor(choice.value);
							}
							soPopUpManager.closePopUp('colorPickerPopup');
						}else{
							alert('".FWDLanguage::getPHPStringValue('chooseColor', 'Selecione uma cor!')."');
						}
					}
					
				</script>";
	}

	public function setAttrDisabled($pbDisabled) {
		$this->cbDisabled = FWDWebLib::attributeParser($pbDisabled,array('true'=>true, 'false'=>false),'pbDisabled');
	}

	public function getAttrDisabled() {
		return $this->cbDisabled;
	}

	private function getButtonClose(){
		$button = new FWDViewButton(new FWDBox(375,250,20,70));
		$button->setAttrClass("FWDViewButtonISMS");
		$st = new FWDStatic(new FWDBox(0,0,20,70));
		$st->setAttrValue(FWDLanguage::getPHPStringValue('vb_close', 'Fechar'));
		$st->setAttrHorizontal('center');
		$moEvent = new FWDClientEvent();
		$moEvent->setAttrValue("soPopUpManager.closePopUp('colorPickerPopup');");
		$moEvent->setAttrEvent("onClick");
		$button->addObjFWDEvent($moEvent);
			
		$button->addObjFWDView($st,'close');
		return $button;
	}

	private function getButtonSave(){
		$button = new FWDViewButton(new FWDBox(457,250,20,70));
		$button->setAttrClass("FWDViewButtonISMS");
		$st = new FWDStatic(new FWDBox(0,0,20,70));
		$st->setAttrValue(FWDLanguage::getPHPStringValue('vb_save', 'Salvar'));
		$st->setAttrHorizontal('center');
		$moEvent = new FWDClientEvent();
		$moEvent->setAttrValue("setColor()");
		$moEvent->setAttrEvent("onClick");
		$button->addObjFWDEvent($moEvent);
		$button->addObjFWDView($st,'save');
		return $button;
	}

	public function setAttrSelectedColor($color){
		$this->selectedColor = $color;
	}

	public function getAttrSelectedColor(){
		return $this->selectedColor;
	}
	
	public function setAttrName($name){
		$this->name = $name;
	}

	public function getAttrName(){
		return $this->name;
	}
}
?>