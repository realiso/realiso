<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDDBTree. Nodo de �rvore populada a partir do banco
 *
 * <p>Classe que implementa uma �rvore populada a partir do banco. Uma
 * DBTree n�o pode ser a raiz da �rvore e n�o pode ter filhos definidos no XML.
 * OBS: As defini��es das classes FWDDBTreeBase e FWDDBTree s�o praticamente
 * id�nticas, exceto que elas extendem classes diferentes, ent�o se uma
 * modifica��o for feita numa das duas classes, provavelmente, a modifica��o
 * deve ser replicada na outra.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDDBTree extends FWDTree {

	/**
	 * DataSet usado para construir a �rvore
	 * @var FWDDBDataSet
	 * @access protected
	 */
	protected $coDataSet = null;

	/**
	 * Objeto que constr�i os nodos da DbTree
	 * @var FWDNodeBuilder
	 * @access protected
	 */
	protected $coNodeBuilder = null;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDDBTree.</p>
	 * @access public
	 */
	public function __construct(){
		parent::__construct();
		$this->coNodeBuilder = new FWDNodeBuilder();
	}

	/**
	 * Seta o DataSet
	 *
	 * <p>Seta o DataSet respons�vel pelo conte�do da �rvore.</p>
	 * @access public
	 * @param FWDDBDataSet $poDataSet Objeto DBDataSet
	 */
	public function setDataSet(FWDDBDataSet $poDataSet){
		$this->coDataSet = $poDataSet;
	}

	/**
	 * Retorna o DataSet
	 *
	 * <p>Retorna o DataSet respons�vel pelo conte�do da �rvore.</p>
	 * @access public
	 * @return FWDDBDataSet Objeto DataSet
	 */
	public function getDataSet(){
		return $this->coDataSet;
	}

	/**
	 * Seta o NodeBuilder
	 *
	 * <p>Seta o NodeBuilder que constr�i os nodos da DbTree.</p>
	 * @access public
	 * @param FWDNodeBuilder $poNodeBuilder Objeto DBNodeBuilder
	 */
	public function setNodeBuilder(FWDNodeBuilder $poNodeBuilder){
		$this->coNodeBuilder = $poNodeBuilder;
	}

	/**
	 * Retorna o NodeBuilder
	 *
	 * <p>Retorna o NodeBuilder que constr�i os nodos da DbTree.</p>
	 * @access public
	 * @return FWDNodeBuilder Objeto NodeBuilder
	 */
	public function getNodeBuilder(){
		return $this->coNodeBuilder;
	}

	/**
	 * Constr�i a �rvore
	 *
	 * <p>Monta a �rvore a partir do DataSet.</p>
	 * @access public
	 * @return boolean True, caso a opera��o seja bem sucedida
	 */
	public function buildTree(){
		if($this->coDataSet==null){
			trigger_error("FWDDBTree object must have a NodeBuilder to be built.",E_USER_WARNING);
			return false;
		}else{
			$moDataSet = $this->coDataSet;
			$moDataSet->execute();
			$moNodeBuilder = $this->coNodeBuilder;
			$maCurrent = array($this);
			while($moDataSet->fetch()){
				$msId = $moDataSet->getFieldByAlias('id')->getValue();
				$miLevel = $moDataSet->getFieldByAlias('node_level')->getValue();
				$msValue = $moDataSet->getFieldByAlias('node_value')->getValue();
				if (isset($maCurrent[$miLevel]) || $miLevel == 0) {
					$moNode = $moNodeBuilder->buildNode($msId,$msValue);
					$maCurrent[$miLevel]->addObjFwdTree($moNode);
					$maCurrent[$miLevel+1] = $moNode;
				}
			}
			$this->setTreeBaseName();
			$this->setIds();
			return true;
		}
	}

	/**
	 * Seta o nome da TreeBase de todos descendentes
	 *
	 * <p>Seta o nome da TreeBase de todos descendentes.</p>
	 * @access public
	 * @param string $psTreeBaseName Nome da TreeBase a que a Tree pertence
	 */
	public function setTreeBaseName($psTreeBaseName=''){
		$msTreeBaseName = $psTreeBaseName;
		if(!$msTreeBaseName) $msTreeBaseName = $this->getTreeBaseName();
		if(!$msTreeBaseName) $msTreeBaseName = $this->getAttrName();
		$this->csTreeBaseName = $msTreeBaseName;
		if(count($this->caNodes)>0){
			foreach($this->caNodes as $moNode){
				$moNode->setTreeBaseName($msTreeBaseName);
			}
		}
	}

}

?>