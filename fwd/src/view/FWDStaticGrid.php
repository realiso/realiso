<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,	technics, statements, and computer programs
 * contain	unpublished	proprietary information of	Axur Communications,
 * Inc.,	and are	protected	by applied	copyright law.	They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without	the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes	codigos,	tecnicas, tratados e	programas de computador contem
 * informacao proprietaria	nao publicada pela Axur Communications, Inc.,
 * e sao	protegidas pelas leis	de direito registrado.	Essas, nao podem
 * ser dispostas	a terceiros, copiadas ou	duplicadas de qualquer forma,
 * no	todo ou	em parte,	sem	consentimento	previo	escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDStatic. Implementa texto est�tico.
 *
 * <p>Classe que implementa texto est�tico (html tag 'span').</p>
 * @package FWD5
 * @subpackage view
 */
class FWDStaticGrid extends FWDViewGrid{

	/**
	 * Define o tipo de alinhamento horizontal do texto contido no static
	 * @var string
	 * @access protected
	 */
	protected $csHorizontal = "left";

	/**
	 * Define o tipo de alinhamento vertical do texto contido no static
	 * @var boolean
	 * @access protected
	 */
	protected $csVertical = "top";

	/**
	 * Define a margem � esquerda do static
	 * @var boolean
	 * @access protected
	 */
	protected $ciMarginLeft=0;


	/**
	 * Define a margem � direita do static
	 * @var boolean
	 * @access protected
	 */
	protected $ciMarginRight=0;


	/**
	 * Define a margem de top do static
	 * @var boolean
	 * @access protected
	 */
	protected $ciMarginTop = 0;

	/**
	 * Define a margem de bottom do static
	 * @var boolean
	 * @access protected
	 */
	protected $ciMarginBottom=0;

	/**
	 * Define se o static deve ter nowrap ou n�o
	 * @var boolean
	 * @access protected
	 */
	protected $cbNoWrap = true;


	/**
	 * Valor do elemento
	 * @var FWDString
	 * @access protected
	 */
	protected $coString = null;

	/**
	 * Codigo HTML do Icone do Static
	 * @var String
	 * @access protected
	 */
	protected $csIconCode = '';

	/**
	 * Path do Icone que o static deve centralizar
	 * @var String
	 * @access protected
	 */
	protected $csIconSrc = '';

	/**
	 * Tooltip do Static
	 * @var Object FWDToolTip
	 * @access protected
	 */
	protected $coToolTip = null;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDView.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites da �rea do elemento
	 */
	public function __construct($poBox = null) {
		$this->coBox = new FWDBox();
		$this->coString = new FWDString();
		$this->setAttrClass(get_class($this));
		if ($poBox) {
			$this->coBox->setAttrLeft($poBox->getAttrLeft());
			$this->coBox->setAttrTop($poBox->getAttrTop());
			$this->coBox->setAttrHeight($poBox->getAttrHeight());
			$this->coBox->setAttrWidth($poBox->getAttrWidth());
		}
	}

	/**
	 * Desenha o cabe�alho do texto est�tico.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o cabe�alho do texto est�tico.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do texto est�tico
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawHeader($poBox = null) {

		//recalcula as dimen��es da box de acordo com a margem definida
		$moBox=$this->getObjFWDBox();

		//clone da box para evitar o efeito "mem�ria" nas altera��es das dimen��es da box do static
		$moAuxBox = clone $moBox;

		//margem a esquerda
		$moBox->setAttrWidth($moBox->getAttrWidth() - $this->ciMarginLeft);
		$moBox->setAttrLeft($moBox->getAttrLeft() + $this->ciMarginLeft);

		//margem a direita
		$moBox->setAttrWidth($moBox->getAttrWidth() - $this->ciMarginRight);

		//margem Top
		$moBox->setAttrTop($moBox->getAttrTop() + $this->ciMarginTop);
		$moBox->setAttrHeight($moBox->getAttrHeight() - $this->ciMarginTop);

		//margem Bottom
		//$moBox->setAttrTop($moBox->getAttrTop() + $this->ciMarginBottom);
		$moBox->setAttrHeight($moBox->getAttrHeight() - $this->ciMarginBottom);

		$msTDclass= $this->cbNoWrap?"class='FWDTD'":'';

		$msVerticalAlign = $this->csVertical? " valign='".$this->csVertical."'":'';
		$msHorizontalAlign = $this->csHorizontal?" align='".$this->csHorizontal."'":'';

		$msOut = " <table class='{$this->csClass}' style='position:absolute;{$moBox->draw()}'><tr><td {$msHorizontalAlign} {$msVerticalAlign} {$msTDclass}> ";

		$this->setObjFWDBox($moAuxBox);
		return $msOut;
	}

	/**
	 * Desenha o corpo do texto est�tico.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o corpo do texto est�tico.</p>
	 * @access public
	 *
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawBody() {
		if($this->csIconSrc==''){
			return $this->coString->getAttrString() . $this->csIconCode;
		}else{
			$msGfxRef = FWDWebLib::getInstance()->getGfxRef();
			$moIcon = new FWDIcon();
			$moIcon->setAttrSrc("{$msGfxRef}{$this->csIconSrc}");
			$moIcon->getImageBox();
			$miTop = ceil( ($this->coBox->getAttrHeight() - $moIcon->getObjFWDBox()->getAttrHeight()) / 2  );
			$miLeft = ceil( ($this->coBox->getAttrWidth() - $moIcon->getObjFWDBox()->getAttrWidth()) / 2  );
			$moIcon->getObjFWDBox()->setAttrTop($miTop);
			$moIcon->getObjFWDBox()->setAttrLeft($miLeft);
			return $moIcon->draw();
		}
	}

	/**
	 * Desenha o rodap� do texto est�tico.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o rodap� do texto est�tico.</p>
	 * @access public
	 *
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawFooter() {
		$msReturn =" ";
		return $msReturn .= "</table>";
	}

	/**
	 * Atribui valor, condicionalmente.
	 *
	 * <p>Atribui valor � view, condicionando atrav�s do segundo par�metro.
	 * Se force for FALSE, concatena o valor se a vari�vel tiver conte�do;
	 * se TRUE atribui mesmo que tenha conte�do (sobrescreve).</p>
	 * @access public
	 * @param string $psValue Valor a ser atribu�do
	 * @param boolean $pbForce For�ar o valor mesmo que j� tenha conte�do
	 */
	public function setValue($psValue, $pbForce = true) {
		$this->coString->setValue($psValue, $pbForce);
	}

	/**
	 * Seta o valor do elemento.
	 *
	 * <p>M�todo para setar o valor do elemento.</p>
	 * @access public
	 * @param FWDString $poString Seta o valor do elemento
	 */
	final public function setObjFWDString(FWDString $poString) {
		if (isset($this->coString)) {
			$this->coString->setValue($poString->getAttrString());
		}
		else {
			$this->coString = $poString;
		}
	}

	/**
	 * Retorna o objeto de string do elemento.
	 *
	 * <p>M�todo para retornar o objeto de string do elemento.</p>
	 * @access public
	 * @return FWDString String do elemento
	 */
	final public function getObjFWDString() {
		return $this->coString;
	}

	/**
	 * Retorna o valor do elemento.
	 *
	 * <p>M�todo para retornar o valor do elemento.</p>
	 * @access public
	 * @return string Valor do elemento
	 */
	public function getValue() {
		return $this->coString->getAttrString();
	}

	/**
	 * Seta o tipo de alinhamento horizontal do static.
	 *
	 * <p>Seta o tipo de alinhamento horizontal do static (valores possiveis -> left, center, right , justify)</p>
	 * @access public
	 * @param string $psHorizontal Define o tipo de alinhamento horizontal do static
	 */
	function setAttrHorizontal($psHorizontal)
	{
		$this->csHorizontal= FWDWebLib::attributeParser($psHorizontal,array("left","center","right","justify"),"psHorizontal");

	}

	/**
	 * Seta o tipo de alinhamento vertical do static.
	 *
	 * <p>Seta o tipo de alinhamento vertical do static (valores possiveis -> top, middle, bottom)</p>
	 * @access public
	 * @param string $psHorizontal Define o tipo de alinhamento horizontal do static
	 */
	function setAttrVertical($psVertical)
	{
		$this->csVertical = FWDWebLib::attributeParser($psVertical,array("top","middle","bottom"),"psVertical");
	}

	/**
	 * Retorna o valor do alinhamento horizontal.
	 *
	 * <p>Retorna o valor do alinhamento horizontal do static</p>
	 * @access public
	 * @return string Alinhamento horizontal do static
	 */
	function getAttrVertical()
	{
		return $this->csVertical;
	}

	/**
	 * Retorna o valor do alinhamento vertical.
	 *
	 * <p>Retorna o valor do alinhamento vertical do static</p>
	 * @access public
	 * @return string Alinhamento vertical do static
	 */
	function getAttrHorizontal()
	{
		return $this->csHorizontal;
	}

	/**
	 * Seta a margem left do static.
	 *
	 * <p>Seta o valor da margem left do static</p>
	 * @access public
	 * @param string $psMargin Define a margem left do static
	 */
	function setAttrMarginLeft($piMarginLeft)
	{
		$this->ciMarginLeft = $piMarginLeft;
	}

	/**
	 * Seta a margem right do static.
	 *
	 * <p>Seta o valor da margem right do static</p>
	 * @access public
	 * @param string $psMargin Define a margem right do static
	 */
	function setAttrMarginRight($piMarginRight)
	{
		$this->ciMarginRight = $piMarginRight;
	}

	/**
	 * Seta a margem top do static.
	 *
	 * <p>Seta o valor da margem top do static</p>
	 * @access public
	 * @param string $psMargin Define a margem top do static
	 */
	function setAttrMarginTop($piMarginTop)
	{
		$this->ciMarginTop = $piMarginTop;
	}

	/**
	 * Seta a margem bottom do static.
	 *
	 * <p>Seta o valor da margem bottom do static</p>
	 * @access public
	 * @param string $psMargin Define a margem bottom do static
	 */
	function setAttrMarginBottom($piMarginBottom)
	{
		$this->ciMarginBottom = $piMarginBottom;
	}

	/**
	 * Retorna o valor da margem left do static.
	 *
	 * <p>Retorna o valor da margem left do static</p>
	 * @access public
	 * @return integer Valor da margem left do static
	 */
	function getAttrMarginLeft()
	{
		return $this->ciMarginLeft;
	}

	/**
	 * Retorna o valor da margem top do static.
	 *
	 * <p>Retorna o valor da margem top do static</p>
	 * @access public
	 * @return integer Valor da margem top do static
	 */
	function getAttrMarginTop()
	{
		return $this->ciMarginTop;
	}

	/**
	 * Retorna o valor da margem right do static.
	 *
	 * <p>Retorna o valor da margem right do static</p>
	 * @access public
	 * @return integer Valor da margem right do static
	 */
	function getAttrMarginRight()
	{
		return $this->ciMarginRight;
	}

	/**
	 * Retorna o valor da margem bottom do static.
	 *
	 * <p>Retorna o valor da margem bottom do static</p>
	 * @access public
	 * @return integer Valor da margem bottom do static
	 */
	function getAttrMarginBottom()
	{
		return $this->ciMarginBottom;
	}

	/**
	 * Atribui valor NoEscape ao objeto string do Static.
	 *
	 * <p>Atribui valor NOEscape ao objeto FWDString do static para evitar que sejam escapados os "'" da string.</p>
	 * @access public
	 * @param boolean $pbNoEscape valor do noEscape ("true" ou "false")
	 */
	public function setAttrStringNoEscape($pbNoEscape) {
		$this->coString->setAttrNoEscape($pbNoEscape);
	}

	/**
	 * Atribui A classe css noWrap ao TD do Static.
	 *
	 * <p>Atribui valor booleando a variavel cbNoWrap para o static 'saber' se ele
	 * deve utilizar a classe css de noWrap em seu TD na hora de se desejar.</p>
	 * @access public
	 * @param boolean $pbNoWrap valor do cbnoWrap ("true" ou "false")
	 */
	public function setAttrNoWrap($pbNoWrap){
		$this->cbNoWrap = FWDWebLib::attributeParser($pbNoWrap,array("true"=>true, "false"=>false),"pbNoWrap");
	}

	/**
	 * Zera o array de eventos do static.
	 *
	 * <p>Zera o array de eventos do static</p>
	 * @access public
	 */
	public function cleanEvents(){
		$this->caEvent = array();
	}


	/**
	 * Atribui o c�digo html de um �cone no static
	 *
	 * <p>Atribui o c�digo html de um �cone no static.</p>
	 * @access public
	 * @param string $psIconCode
	 */
	public function setFWDIconCode($psIconCode) {
		$this->csIconCode = $psIconCode;
	}

	/**
	 * Atribui o source de um icone ao static
	 *
	 * <p>Atribui o source de um icone ao static.</p>
	 * @access public
	 * @param string $psIconSource
	 */
	public function setIconSrc($psIconSource){
		$this->csIconSrc = $psIconSource;
	}

	/**
	 * Atribui um ToolTip ao Static
	 *
	 * <p>Atribui um ToolTip ao Static.</p>
	 * @access public
	 * @param boolean $pbNoWrap valor do cbnoWrap ("true" ou "false")
	 */
	public function setObjFWDToolTip(FWDToolTip $poToolTip){
		$mskey = $poToolTip->getAttrEvent();
		$msArrayKey = explode(":",$mskey);
		foreach($msArrayKey as $msKey){
			if(!isset($this->caEvent[$msKey]))
			$this->caEvent[$msKey] = new FWDEventHandler($msKey,$this->csName);
			$this->caEvent[$msKey]->setAttrContent($poToolTip);
		}
	}

	/**
	 * Retorna o objeto de tooltipo do static.
	 *
	 * <p>Retorna o objeto de tooltip do static</p>
	 * @access public
	 * @return Object FWDToolTip tooltip do static
	 */
	public function getObjFWDToolTip(){
		return $this->coToolTip;
	}

	public function getArrayEvent(){
		return $this->caEvent;
	}
}
?>