<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDText. Implementa campos de texto.
 *
 * <p>Classe que implementa campos de texto (html tag 'input type=text').</p>
 * @package FWD5
 * @subpackage view
 */
class FWDText extends FWDView implements FWDPostValue {

	/**
	 * Define se o texto � do tipo password ou n�o
	 * @var boolean
	 * @access protected
	 */
	protected $cbPassword = false;

	/**
	 * Define se o campo est� desabilitado ou n�o
	 * @var boolean
	 * @access protected
	 */
	protected $cbDisabled = false;

	/**
	 * N�mero m�ximo de caracteres
	 * @var integer
	 * @access protected
	 */
	protected $ciMaxLength = 0;

	/**
	 * M�scara a ser aplicada sobre o texto
	 * @var FWDMask
	 * @access protected
	 */
	protected $coMask = null;

	/**
	 * Nome do label associado ao objeto
	 * @var string
	 * @access protected
	 */
	protected $csLabel = "";

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDText.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do campo de texto
	 */
	public function __construct($poBox = null) {
		parent::__construct($poBox);
	}

	/**
	 * Desenha o campo de texto.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML,
	 * todas as informa��es necess�rias para implementar o campo de texto.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do campo de texto
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawBody($poBox = null){
		$msOut = '';

		if($this->getAttrAlign()){
			$this->alignView($this->getAttrAlign(),$poBox);
		}

		$msStyle = $this->GetStyle();
		$msStyle = "style='{$msStyle}'";

		$msLabel = '';
		if($this->csLabel){
			$msLabel = "label='{$this->csLabel}'";
		}

		$msAttrDefault = $this->getGlobalProperty();

		$msType = $this->cbPassword ? "type='password'" : "type='text'";

		$msMaxLength = $this->ciMaxLength ? "maxlength='{$this->ciMaxLength}'" : "";

		$msDisabled = $this->cbDisabled ? "disabled" : "";

		$msValue = "value='{$this->getValue()}'";

		$msMaskEvent = "";
		if(isset($this->coMask)){
			$msMaskEvent = "onpaste='return false'".$this->coMask->getAttrEvent()."='".$this->coMask->render($this->getAttrName())."'";
		}

		$msOut .= "<input {$msType} {$msAttrDefault} {$this->getEvents()} {$msStyle} {$msLabel} {$msMaxLength} {$msDisabled} {$msValue} {$msMaskEvent}/>\n";

		return $msOut;
	}

	/**
	 * Desenha o rodap� do texto.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o rodap� do texto.</p>
	 * @access public
	 *
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawFooter(){
		if($this->csName){
			$msOut = "<script language='javascript'>";
			$msOut.= "gebi('{$this->csName}').object = new FWDText('{$this->csName}');";
			if(isset($this->coMask)){
				$msOut.= "gobi('{$this->csName}').setMaskRegExp({$this->coMask->getRegExp()});";
			}
			$msOut.= "</script> ";
			return $msOut;
		}else{
			return '';
		}
	}

	/**
	 * Atribui valor � vari�vel value.
	 *
	 * <p>Atribui valor � vari�vel value, devendo retirar espa�os em branco
	 * no inicio e/ou no fim da string.</p>
	 * @access public
	 * @param string $psValue Valor a ser atribu�do
	 */
	public function setAttrValue($psValue) {
		$this->coString->setAttrValue(trim($psValue));
	}

	/**
	 * Seta o campo como password.
	 *
	 * <p>M�todo para setar o campo como password.</p>
	 * @access public
	 * @param boolean $pbPassword Define se o texto � do tipo password ou n�o
	 */
	public function setAttrPassword($pbPassword) {
		$this->cbPassword = FWDWebLib::attributeParser($pbPassword,array("true"=>true, "false"=>false),"pbPassword");
	}

	/**
	 * Seta o campo como desabilitado.
	 *
	 * <p>M�todo para setar o campo como desabilitado.</p>
	 * @access public
	 * @param boolean $pbDisabled Define se o campo est� desabilitado ou n�o
	 */
	public function setAttrDisabled($pbDisabled) {
		$this->cbDisabled = FWDWebLib::attributeParser($pbDisabled,array("true"=>true, "false"=>false),"pbDisabled");
	}

	/**
	 * Seta o n�mero m�ximo de caracteres do campo.
	 *
	 * <p>M�todo para setar o n�mero m�ximo de caracteres do campo.</p>
	 * @access public
	 * @param integer $piMaxLength N�mero m�ximo de caracteres
	 */
	public function setAttrMaxLength($piMaxLength) {
		if($piMaxLength > 0)
		$this->ciMaxLength = $piMaxLength;
		else
		trigger_error("Invalid maxlength value: '{$piMaxLength}'",E_USER_WARNING);
	}

	/**
	 * Seta o nome do Label do objeto.
	 *
	 * <p>M�todo para setar o nome do label do objeto, utilizado para a troca de cor, no caso do objeto ter
	 * preenchimento obrigat�rio e n�o estar preenchido.</p>
	 * @access public
	 * @param string $psLabel nome do label associado ao objeto
	 */
	public function setAttrLabel($psLabel) {
		$this->csLabel = $psLabel;
	}

	/**
	 * Retorna o valor booleano do atributo Password.
	 *
	 * <p>Retorna o valor booleano TRUE se o texto est�tico deve ser exibido
	 * como password. Retorna FALSE caso contr�rio.</p>
	 * @access public
	 * @return boolean Indica se o texto � do tipo password ou n�o
	 */
	public function getAttrPassword() {
		return $this->cbPassword;
	}

	/**
	 * Retorna o valor booleano do atributo Disabled.
	 *
	 * <p>Retorna o valor booleano TRUE se o campo est� desabilitado.
	 * Retorna FALSE caso contr�rio.</p>
	 * @access public
	 * @return boolean Indica se o campo est� desabilitado ou n�o
	 */
	public function getAttrDisabled() {
		return $this->cbDisabled;
	}

	/**
	 * Retorna o n�mero m�ximo de caracteres do campo.
	 *
	 * <p>M�todo para retornar o n�mero m�ximo de caracteres do campo.</p>
	 * @access public
	 * @return integer N�mero m�ximo de caracteres
	 */
	public function getAttrMaxLength() {
		return $this->ciMaxLength;
	}

	/**
	 * Adiciona uma m�scara.
	 *
	 * <p>Adiciona um objeto m�scara da classe FWDMask na classe FWDText.</p>
	 * @access public
	 * @param FWDMask $poMask M�scara
	 */
	public function addObjFWDMask($poMask) {
		$this->coMask = $poMask;
	}
}
?>