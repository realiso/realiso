<?
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These codedf instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDGrid. Implementa uma Grid Simples.
 *
 * <p>Classe que implementa uma Grid tableless. Grid com preenchimento est�tico
 * de dados ou via Ajax. Via ajax, a grid utiliza pagina��o. Pode-se definir
 * menu de contexto para a grid ou para o cabe�alho e / ou para cada uma das
 * c�lulas da grid.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDGrid extends FWDGridAbstract
{

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDGrid.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites da Grid
	 */
	public function __construct($poBox = null){
		parent::__construct($poBox);
	}


	/**
	 * Desenha O bot�o de edi��o das propriedades da grid.
	 *
	 * <p>Retorna o c�digo HTML que desenha o bot�o de edi��o das propriedades das colunas da grid</p>
	 * @access public
	 * @return string C�digo HTML do bot�o de edi��o das propriedades da grid
	 */
	public function drawButtonEdit(){
		$moButtonEdit = new FWDViewButton();
		$moButtonEdit->setObjFWDBox(new FWDBox(0,0,$this->ciButtonEditHeight,$this->ciButtonEditWidth));
		$moButtonEdit->setAttrClass($this->csCSSClass.$this->csClassButtonEditGrid);
		$moButtonEdit->setAttrName("{$this->csName}_button_edit");
		$moEvent = new FWDClientEvent();
		$moEvent->setAttrValue("trigger_event('{$this->csName}_grid_open_popup_pref',3);");
		$moEvent->setAttrEvent("onClick");
		$moButtonEdit->addObjFWDEvent($moEvent);
		$moButtonEdit->setAttrTabIndex(0);
		return $moButtonEdit->draw();
	}

	/**
	 * Desenha o cabe�alho da Grid.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o cabe�alho da Grid.
	 * retorna o c�digo HTML do t�tulo da grid, dos bot�es de navega��o, do cabe�alho,
	 * e a div do corpo da grid</p>
	 * @access public
	 * @return string C�digo HTML que ser� inserido no cabe�alho da grid
	 */
	public function drawHeader()
	{
		$weblib_instance = FWDWebLib::getInstance();
		$gfx_ref = $weblib_instance->getGfxRef();
		$msReturn = "";
		$msColumnCode = "";
		$msPageButtonsCode = "";
		$msTitleCode="";
		$miSumHeaderHeight=0;
		$miTitleWidth=0;

		$miHeaderWidth = $this->coBox->getAttrWidth();

		$miHeaderColumnWidth = $this->coBox->getAttrWidth() - $this->ciRollerBarWidth -1;

		//arruma as dimen��es da box de cada uma das colunas
		$this->arrangeAllDimensions();

		$msStyle = $this->coBox->draw();
		$msStyle.= $this->cbDisplay ? '' : 'display:none;';
		$msStyle.= $this->csDebug ? $this->debugView() : '';
		$msClass = (($this->csClass)?"class='{$this->csClass}'":"");
		$msEvent=$this->getEvents();
		$msReturn .= "<div name='{$this->csName}' id='{$this->csName}' {$msClass} style='{$msStyle}' {$msEvent}>";

		//renderiza os headers das colunas
		$msColumnCode .= $this->drawHeaderColumns();
		//renderiza o titulo da grid
		$msReturn .= $this->drawGridTitle();

		if ((!$this->cbIsIE)&&($miSumHeaderHeight==0))
		$miSumHeaderHeight=-1;
		$miPageButtonLeftBegin = $this->coBox->getAttrWidth() - 237;
		$miPageButtonWidth= 220;
		$miAuxLeftButtomBegin = + 1;

		// bot�o de exporta��o pra Excel
		if($this->cbShowExcel){
			$moButtonExcel = new FWDViewButton();
			$miHeight = $this->ciButtonExcelHeight;
			$miAuxLeftButtomBegin += $this->ciButtonExcelWidth;
			$miLeft = $this->coBox->getAttrWidth() - $miAuxLeftButtomBegin;
			$miTop = floor(($this->ciTitleHeight - $miHeight)/2);
			$moButtonExcel->setObjFWDBox(new FWDBox($miLeft,$miTop,$miHeight,$this->ciButtonExcelWidth));
			$moButtonExcel->setAttrClass($this->csClassButtonExcel);
			$moButtonExcel->setAttrName("{$this->csName}_button_export");
			$moEvent = new FWDClientEvent();
			$moEvent->setAttrFunction("submit");
			$moEvent->setAttrEvent("onClick");
			$moEvent->setAttrParameters("{$this->csName}_button_export");
			$moButtonExcel->addObjFWDEvent($moEvent);
			$moButtonExcel->setAttrTabIndex(0);
			$msReturn.= " ".$moButtonExcel->draw();
			//atualiza onde os bot�es de pagina��o v�o aparecer
			$miPageButtonLeftBegin-=$this->ciButtonExcelWidth;
			$miAuxLeftButtomBegin += 8;
		}

		$msEditButtomCode = '';
		//bot�o de edi��o das propriedades da grid
		if(($this->cbShowEdit)&&($this->cbPopulateXML==true)&&($this->cbPopulate)){
			$msEditButtomCode .= $this->drawButtonEdit();
		}

		$msEditButtomCode = '';
		//bot�o de edi��o das propriedades da grid
		if(($this->cbShowEdit)&&($this->cbPopulateXML)&&($this->cbDinamicFill==false)){
			$msEditButtomCode .= $this->drawButtonEdit();
		}
		$mbDisplayDivEdit = !$this->cbDinamicFill?'block':'none';

		//atualiza onde os bot�es de pagina��o v�o aparecer
		if($this->cbShowEdit){
			$miLeft = $this->coBox->getAttrWidth() - $miAuxLeftButtomBegin - $this->ciButtonExcelWidth;
			$miTop = floor(($this->ciTitleHeight - $this->ciButtonExcelHeight)/2);
			$msReturn .="<div name='{$this->csName}_edit_dv_button' id='{$this->csName}_edit_dv_button' class='{$this->csCSSClass}{$this->csClassDivPageButton}' style='position:absolute;height:{$this->ciButtonExcelHeight};width:{$this->ciButtonExcelWidth};left:{$miLeft};top:{$miTop};overflow:hidden;display:{$mbDisplayDivEdit};'>{$msEditButtomCode}</div>";
			$miPageButtonLeftBegin-=$this->ciButtonEditWidth;
		}
		//desenha a div dos botoes de navega��o
		$msReturn .="<div name='{$this->csName}_page_buttons' id='{$this->csName}_page_buttons' class='{$this->csCSSClass}{$this->csClassDivPageButton}' style='position:absolute;height:1;width:{$miPageButtonWidth};left:{$miPageButtonLeftBegin};top:{$miSumHeaderHeight};overflow:hidden;display:none;'> </div>";

		$miSumHeaderHeight +=$this->ciTitleHeight;
		$miHeightColumn = $this->ciColumnHeight -1;

		if ($this->cbIsIE)
		$miSumHeaderHeight -= 2;
		else
		$miHeaderColumnWidth -=2;
		//cria a div dos headers das colunas da grid
		$msReturn .= "<div name='{$this->csName}_column_code' id='{$this->csName}_column_code' class='{$this->csCSSClass}{$this->csClassDivHeaderColumns}' style='position:absolute;height:{$miHeightColumn};width:{$miHeaderColumnWidth};left:-1;top:{$miSumHeaderHeight};' > {$msColumnCode} </div>";
		//cria o bot�o de refresh da grid
		$msReturn .= $this->drawButtonRefresh($miSumHeaderHeight,$miHeaderColumnWidth);
		//div para os botoes de pagina��o
		$msReturn .= "<div name='{$this->csName}_menu_acl' id='{$this->csName}_menu_acl'> </div>";

		//posiciona a div do body em rela��o a altura das divs anteriores..........
		$miBodyHeight = $this->coBox->getAttrHeight() - $this->ciColumnHeight - $miSumHeaderHeight - 1;
		$miBodyWidth = $this->coBox->getAttrWidth();
		$miBodyTop = $this->ciColumnHeight + $miSumHeaderHeight;

		if ($this->cbIsIE){
			$miBodyTop -=2;
			$miBodyHeight +=2;
		}
		$cbShowGlass = false;
		if($this->cbDinamicFill && $this->cbPopulateXML){
			$cbShowGlass = true;
		}
		if($this->cbRefreshOnCurrentPage){
			$mbRefreshCurr = 1;
		}else{
			$mbRefreshCurr = 2;
		}
		$msReturn .= "<input type=hidden id='{$this->csName}_page_button_columns_height' value='{$this->ciTitleHeight}'/>
		<input type=hidden name='{$this->csName}_row_over' id='{$this->csName}_row_over' value='{$this->csCSSClass}{$this->csClassBodyZebraOver}'/>
		<input type=hidden name='{$this->csName}_row_selected' id='{$this->csName}_row_selected' value='".$this->csCSSClass.$this->csClassBodyRowSelected."'/>
		<input type=hidden name='{$this->csName}_preferences_code_values' id='{$this->csName}_preferences_code_values' value=''/>
		<input type=hidden name='{$this->csName}_refresh_on_current_page' id='{$this->csName}_refresh_on_current_page' value='{$mbRefreshCurr}'/>
		<div name='{$this->csName}_body_content' id='{$this->csName}_body_content'
		class='{$this->csCSSClass}{$this->csClassDivBody}' style='position:absolute;height:{$miBodyHeight};width:{$miBodyWidth};left:-1;top:{$miBodyTop};' >
		<script language=\"JavaScript\">var obj{$this->csName} = new FWDGridScript('{$this->csName}','{$cbShowGlass}');</script>";

		return $msReturn ;
	}

	/**
	 * Desenha o Body da Grid via Ajax.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o corpo da Grid.
	 * retorna o c�digo HTML de cada uma das linhas da grid quando chamado via Ajax</p>
	 * @access public
	 * @return string C�digo HTML que ser� inserido na div do body da grid
	 */
	public function drawBodyAjax()
	{
		$this->cbPopulate=true;
		return $this->drawBody();
	}

	/**
	 * Desenha o Body da Grid.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o corpo da Grid.
	 * retorna o c�digo HTML de cada uma das linhas da grid</p>
	 * @access public
	 * @return string C�digo HTML que ser� inserido na div do body da grid
	 */
	function drawBody()
	{
		$msReturn="";

		if(($this->cbPopulate==true)&&($this->cbPopulateXML==true)){
			$miRelativeFirstRow = 0;
			$ciDataShift = 0;
			$miContDisplay=0;
			$ciShiftRow=0;
			$msIdsRowSelected = '';
			$msGridValue = "";
			$maRowsSelected = array();
			$msColumnActive="";
			$miIAux = 0;
			$miGridWidth = $this->coBox->getAttrWidth() - $this->ciRollerBarWidth;
			$miRowHeight=$this->ciRowHeight;
			$msUnitClass = $this->csCSSClass . $this->csClassBodyCellUnit;
			$miContSelectAux=0;
				
			//hakeia a sele��o de linhas da grid (isto � um hack, ou seja, um perigo!)
			if($this->cbHackExecHackSelect ==true)
			$this->setSelectRowsGrid();
				
			//cria um array associativo para testar se a linha tah selecionada
			foreach($this->caGridValue as $msValue)
			if($msValue!='')
			$maRowsSelected["{$msValue}"]=1;

			if(!($this instanceof FWDDBGrid)){
				if($this->csOrder)
				$this->orderGrid();
				$ciDataShift = $this->ciRowsPerPage * ($this->ciCurrentPage -1);
			}
			if($this->cbDinamicFill==true){
				$this->arrangeAllDimensions();
				$maxRowToDraw = min($this->ciRowsPerPage,($this->ciRowsCount - ($this->ciRowsPerPage * ($this->ciCurrentPage -1))));
				$ciShiftRow = $this->ciRowsPerPage * ($this->ciCurrentPage -1);
			}else{
				$maxRowToDraw =$this->ciRowsCount;
			}
			$maColumnAlias = array();
			$miCount = 1;
			foreach ($this->caColumns as $moColumn){
				$maColumnAlias[$moColumn->getAttrAlias()] = $miCount;
				$miCount++;
			}
			for($miCont = 1;$miCont <= count($this->caColumnDisplay); $miCont++)
			if($this->caColumnDisplay[$miCont]==1){
				if($this->caEventBlock[$miCont]==0)
				$msColumnActive .="{$miCont}:";
			}
			//else{ }	//a coluna n�o est� ativa ou est� com os eventos bloqueados , assim n�o eh necess�rio criar
			//os alvos do menu de contexto para esta colunas do corpo da grid
			$maCellBody = array();
			$miContCellDraw = 1;

			foreach($this->caOrderColumnShow as $miI){
				$moColumnBox = $this->caColumns[$miI]->getObjFWDBox();
				$moBox = new FWDBox($moColumnBox->getAttrLeft(),0, $miRowHeight+1,$moColumnBox->getAttrWidth());
				$moStatic = new FWDStaticGrid($moBox);
				if($this->caColumns[$miI]->getAttrColNoWrap()){
					$moStatic->setAttrClass($this->csCSSClass . $this->csClassStaticBodyCellUnit);
				}else{
					$moStatic->setAttrClass($this->csCSSClass . $this->csClassStaticBodyCellUnitNFix);
					$moStatic->setAttrNoWrap('false');
				}
				foreach($this->caColumns[$miI]->getContent() as $moView){
					if(($moView instanceof FWDStatic)||($moView instanceof FWDStaticGrid)){
						$moStatic->setAttrHorizontal($moView->getAttrHorizontal());
						$moStatic->setAttrVertical($moView->getAttrVertical());
						$moStatic->setAttrMarginLeft($moView->getAttrMarginLeft());
						$moStatic->setAttrMarginRight($moView->getAttrMarginRight());
						$moStatic->setAttrMarginTop($moView->getAttrMarginTop());
						$moStatic->setAttrMarginBottom($moView->getAttrMarginBottom());
					}
				}//end foreach
				$maCellBody[$miContCellDraw] = $moStatic;
				$miContCellDraw++;
			}//end foreach

			for($miI=1;$miI<=$maxRowToDraw;$miI++){
				$miContColumn = 1;
				$msLineCode="";
				$msOnClickLine="";
				$msSelectColumn='';
				$msRowName = $this->csName."_row_".($miI+$ciShiftRow);
				$msRowNameToCell = $this->csName.'_r'.($miI+$ciShiftRow);

				$mbCanSelect =true;
				$msZebra = (fmod($miI, 2)==0)?$this->csCSSClass . $this->csClassBodyZebra0:$this->csCSSClass . $this->csClassBodyZebra1;
				$msZebraAux = $msZebra;
				$miAuxTop = $miRowHeight*($miI -1)-1; ;

				if (($this->ciSelectType==GRID_SEL_SINGLE)||($this->ciSelectType==GRID_SEL_MULTIPLE) &&($mbCanSelect==true)&&($this->csSelectColumn!=0))
				$msOnClickLine = " obj{$this->csName}.sel_r(\"{$msRowName}\",{$this->ciSelectType},event); ";
				$miContCellColBody = 1;
				foreach($this->caOrderColumnShow as $miJ){
					$miAuxNumCol = $miJ+1;
					if(isset($this->caRows[$miI + $ciDataShift][$miAuxNumCol]))
					$maCellBody[$miContCellColBody]->setValue($this->caRows[$miI + $ciDataShift][$miAuxNumCol]);
					else
					$maCellBody[$miContCellColBody]->setValue('');
					if(isset($this->caRows[$miI + $ciDataShift])){
						
						$this->coDrawGrid->setItem(
						$miI + $ciShiftRow, 
						$miAuxNumCol, 
						$maCellBody[$miContCellColBody],
						$msUnitClass,
						$this->caRows[$miI + $ciDataShift],
						$this->caToolTip[$miAuxNumCol],
						$this->caToolTipSource[$miAuxNumCol],
						$maColumnAlias, 
						$this->caColumns[$miAuxNumCol-1]->getAttrToolTipMaxLength());
						
						if(($this->caEventBlock[$miAuxNumCol]==1)||($this->csDebug))
						$msLineCode .= $this->coDrawGrid->Draw($maCellBody[$miContCellColBody]->getObjFWDBox(),$msRowNameToCell.'_c'.$miAuxNumCol,"");
						else
						$msLineCode .= $this->coDrawGrid->Draw($maCellBody[$miContCellColBody]->getObjFWDBox(),$msRowNameToCell.'_c'.$miAuxNumCol,$msOnClickLine);
						//esclui os eventos q foram inseridos no drawItem
						$maCellBody[$miContCellColBody]->cleanEvents();
						//atribui o valor do static de volta no array da grid
						//??????????????????COMENTADO PARA FUNCIONAR NO AMS -> AQUI EHRA FEITA A C�PIA DO VALOR QUE EST� NA TELA PARA O ARRAY DA GRID????????????????????????????????????????????????????????
						//$this->caRows[$miI + $ciDataShift][$miAuxNumCol] = $maCellBody[$miContCellColBody]->getObjFWDString()->getAttrString();
					}
					$miContCellColBody++;
				}//end foreach

				//obtem os valores do actionTarget para a linha da grid
				$maSelCol = explode(":", $this->csSelectColumn);
				$miK = 1;
				foreach($maSelCol as $msSelCol) {
					if (isset($this->caRows[$miI + $ciDataShift][$msSelCol])){
						if ($miK != 1) $msSelectColumn .= ":".$this->caRows[$miI + $ciDataShift][$msSelCol];
						else $msSelectColumn .= $this->caRows[$miI + $ciDataShift][$msSelCol];
					}
					else {
						$mbCanSelect = false;
					}//foi definida uma coluna inexistente para a sele��o ou n�o foi definido sele��o para a grid
					$miK++;
				}
				if(($mbCanSelect)&& (isset($maRowsSelected["{$msSelectColumn}"]))){
					$msZebraAux = $this->csCSSClass.$this->csClassBodyRowSelected;
				}
				//codigo htm de cada linha da grid
				$msReturn .= "<div name='{$msRowName}' id='{$msRowName}' actionTarget='{$msSelectColumn}' classDefault='{$msZebra}'  class='{$msZebraAux}' onMouseOver='obj{$this->csName}.o_in(\"{$msRowName}\");' onMouseOut='obj{$this->csName}.o_out(\"{$msRowName}\"); ' style='height:{$miRowHeight};width:{$miGridWidth};left:0;top:{$miAuxTop};'> {$msLineCode} </div> ";
			}//end for
				

			if(!$this->coNoResultAlert){
				$moView = new FWDViewGroup(new FWDBox(0,0,40,$this->coBox->getAttrWidth()));
				$moView->setAttrClass($this->csCSSClass . 'BdNoInf');
				$moSt = new FWDStaticGrid(new FWDBox(0,0,40,$this->coBox->getAttrWidth()));
				$moSt->setValue(FWDLanguage::getPHPStringValue('fwdgrid_info_no_result_rows','Nenhum resultado encontrado'));
				$moSt->setAttrClass($this->csCSSClass . 'BdNoInfST');
				$moSt->setAttrVertical("middle");
				$moSt->setAttrNoWrap("false");
				$moSt->setAttrMarginLeft(10);
				$moView->addObjFWDView($moSt);
				$this->coNoResultAlert = $moView;
			}

			//cria a div informando que nenhum resultado foi encontrado
			if($maxRowToDraw<=0){
				$msReturn .= $this->coNoResultAlert->draw();
			}
				
			$msValuePopulate="";
			if($this->cbPopulateXML==true)
			$msValuePopulate="1";
			else
			$msValuePopulate="2";

			$msRowsIdSerialized="a:0:{}";
			$msGridValueSerialized="a:0:{}";
			if(($this->ciSelectType==GRID_SEL_SINGLE)||(($this->ciSelectType==GRID_SEL_MULTIPLE))){
				if(count($this->caGridRowsSelected)>=1)
				$msRowsIdSerialized = serialize($this->caGridRowsSelected);
				if(count($this->caGridValue)>=1)
				$msGridValueSerialized = serialize($this->caGridValue);
			}
			//else{} a grid n�o tem sele��o
				
			//inputhiddens do body da grid
			$msReturn .= " <input type=hidden name='{$this->csName}_ActiveColums' value='{$msColumnActive}' /><input type=hidden name='{$this->csName}_RowsDraw' value='1:{$miI}' />
<input type=hidden name='{$this->csName}_populatexml' id='{$this->csName}_populatexml' value='{$msValuePopulate}' />
<input type=hidden name='{$this->csName}_value_row_id' id='{$this->csName}_value_row_id' value='{$msRowsIdSerialized}' />
<input type=hidden name='{$this->csName}_value' id='{$this->csName}_value' value='{$msGridValueSerialized}' />";
	 }
		return $msReturn ;
	}

	/**
	 * cria os ids de de se��o de linhas da grid.
	 *
	 * <p>cria os ids de de se��o de linhas da grid e atribui nas variaveis de sele��o da grid,
	 * ap�s, dispara o evento de refresh da gridp>.
	 */
	public function setSelectRows($paSelectRows,$pbAddValues){
		$this->caHackSelectRows = $paSelectRows;
		$this->cbHackCleanSelect =$pbAddValues;
		$this->cbHackExecHackSelect =true;
		$this->execEventPopulate('refreshselect');
	}

	/**
	 * cria os ids de de se��o de linhas da grid.
	 *
	 * <p>cria os ids de de se��o de linhas da grid e atribui nas variaveis de sele��o da grid,
	 * ap�s, dispara o evento de refresh da gridp>.
	 */
	public function setSelectRowsGrid(){
		$ciDataShift = 0;
		$ciShiftRow = 0;
		if($this->cbHackCleanSelect==true){
			$this->caGridRowsSelected = array();
			$this->caGridValue = array();
		}
		if(!($this instanceof FWDDBGrid)){
			$ciDataShift = $this->ciRowsPerPage * ($this->ciCurrentPage -1);
		}
		if($this instanceof FWDDBGrid){
			$ciShiftRow = $this->ciRowsPerPage * ($this->ciCurrentPage -1);
		}
		$msRowName = $this->csName."_row_";
		$maAuxRowName = array();
		$maSelCol = explode(':',$this->csSelectColumn);
		for($miI=0;$miI<=count($this->caRows);$miI++){
			$msSelValue = '';
			foreach($maSelCol as $miSel){
				if(isset($this->caRows[$miI][$miSel])){
					if($msSelValue=='')
					$msSelValue .=$this->caRows[$miI][$miSel];
					else
					$msSelValue .=':' . $this->caRows[$miI][$miSel]+1;
				}
			}
			$maAuxRowName[$msSelValue] = $miI + $ciShiftRow;
		}
		$teste = implode(':',$maAuxRowName);

		foreach ($this->caHackSelectRows as $psSelRow){
			if(isset($maAuxRowName[$psSelRow])){
				$this->caGridRowsSelected[] = $msRowName . $maAuxRowName[$psSelRow] .":{$psSelRow}";
				$this->caGridValue[] = $psSelRow;
			}
		}
		$this->cbHackExecHackSelect =false;
	}

	/**
	 * Desenha o Footer da Grid.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o "p�" da Grid.
	 * retorna o c�digo HTML do "p�" da grid, incluindo o c�digo do menu de contexto, caso ele tenha sido definido dentro da grid</p>
	 * @access public
	 * @return string C�digo HTML que ser� inserido no "p�" da grid.
	 */
	function drawFooter($poBox = null)
	{

		$miCountColumns = count($this->caColumns);

		$msReturnAux = "";

		$msReturn ="</div>
<div name='{$this->csName}_contextmenu_header' id='{$this->csName}_contextmenu_header'> {$this->DrawMenu()} </div>
<input type=hidden name='{$this->csName}_columns_count' id='{$this->csName}_columns_count' value='{$miCountColumns}'/>
<input type=hidden name='{$this->csName}_rows_count' id='{$this->csName}_rows_count' value='{$this->ciRowsCount}'/> 
<script language=\"JavaScript\">
  moCheckCtrl{$this->csName} = gebi('{$this->csName}_col_ctrl');
	if(moCheckCtrl{$this->csName}) moCheckCtrl{$this->csName}.value = '';
	moInputHidden{$this->csName} = gebi('{$this->csName}_value'); 
	if(moInputHidden{$this->csName}) moInputHidden{$this->csName}.value = null;		
	moInputHidden{$this->csName}rowid = gebi('{$this->csName}_value_row_id'); 
	if(moInputHidden{$this->csName}rowid)	moInputHidden{$this->csName}rowid.value = null;		
	moInputColumnsCount{$this->csName} = gebi('{$this->csName}_columns_count'); 
	if(moInputColumnsCount{$this->csName}) moInputColumnsCount{$this->csName}.value = null;		
	moInputRowsCount{$this->csName} = gebi('{$this->csName}_rows_count'); 
	if(moInputRowsCount{$this->csName})	moInputRowsCount{$this->csName}.value = null;		
";
		if($this->cbDinamicFill)
		$msReturnAux .="trigger_event(\"{$this->csName}populate\",\"3\");";

		if($this->cbIsIE)
		$msReturnAux .= "		gebi('{$this->csName}').onselectstart=new Function (\"return false\");";
		else{
			$msReturnAux .= " gebi('{$this->csName}').onmousedown=disableselect; gebi('{$this->csName}').onclick=reEnable; ";
		}
		$msReturnAux .= "</script>";
		if($this->cbSelectionRequired){
			$msSelectionRequired = 'true';
		}else{
			$msSelectionRequired = 'false';
		}
		$msReturn.= $msReturnAux."
   </div>
   <script language='javascript'>
      var moElement = gebi('{$this->csName}');
      moElement.object = obj{$this->csName};
      moElement.object.cbIsMultiple = ".($this->ciSelectType==GRID_SEL_MULTIPLE?'true':'false').";
      moElement.object.setSelectionRequired($msSelectionRequired);
      moElement.object.ciRowsPerPage = {$this->ciRowsPerPage};
    </script>
";
		return $msReturn;
	}

	/**
	 * Desenha o Titulo da Grid.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para desenhar os headers das colunas da grid.</p>
	 * @access public
	 * @return string C�digo HTML que ser� inserido na div dos header das colunas da grid
	 */
	public function drawHeaderColumns(){
		$msColumnCode = "";

		foreach($this->caColumns as $moColumn){
			$moColumn->setOrderBy("");
			if(($moColumn->getAttrCanOrder())&&(!$moColumn->getAttrEventBlock()))
			$moColumn->setAttrClass($this->csCSSClass . $this->csClassColumnHeaderCanOrder);
			else
			$moColumn->setAttrClass($this->csCSSClass . $this->csClassColumnHeader);
		}

		$msOrder = $this->getAttrOrder();
		$maOrder = explode(":",$msOrder);
		foreach($maOrder as $msColOrder){
			$msColOrder=trim($msColOrder);
			if($msColOrder!=""){
				$miColumn="";
				$msChar = substr(trim($msColOrder),0,1);
				if(($msChar=="+")||($msChar=="-"))// ascendente
				$miColumn = substr(trim($msColOrder),1);
				else{//assumindo que soh veio um n�mero , ordena��o ascendente
					$miColumn = trim($msColOrder);
					$msChar="+";
				}
				$miColumn--;
				$moColumn = $this->caColumns[$miColumn];
				if($moColumn){
					if($moColumn->getAttrCanOrder()){
						if($msChar=="+"){
							$moColumn->setOrderBy("asc");
							$moColumn->setAttrClass($this->csCSSClass . $this->csClassColumnHeaderOrderASC);
						}
						elseif($msChar=="-"){
							$moColumn->setOrderBy("desc");
							$moColumn->setAttrClass($this->csCSSClass . $this->csClassColumnHeaderOrderDESC);
						}
						else{
							$moColumn->setOrderBy("asc");
							$moColumn->setAttrClass($this->csCSSClass . $this->csClassColumnHeaderOrderDESC);
						}
					}
				}else
				trigger_error("Trying to order an inexistent column! column number '{$miColumn}'",E_USER_WARNING);
			}//else{}//msColOrder vazio devido a um : a mais na string
				
		}
		foreach($this->caOrderColumnShow as $miI)
		{
			if($this->caColumns[$miI]->getAttrIconColumnOrderMarginLeft()==0)
			$this->caColumns[$miI]->setAttrIconColumnOrderMarginLeft($this->getAttrIconColumnOrderMarginLeft());
			else{}//foi atribuido um valor especifico para a margem esquerda da coluna

			if($this->caColumns[$miI]->getAttrIconColumnOrderMarginRight()==0)
			$this->caColumns[$miI]->setAttrIconColumnOrderMarginRight($this->getAttrIconColumnOrderMarginRight());
			else{}////foi atribuido um valor especifico para a margem direita da coluna
				
			$msColumnCode .= $this->caColumns[$miI]->drawHeader();
			foreach($this->caColumns[$miI]->getContent() as $moColumnView){
				if(($moColumnView instanceof FWDStatic)||($moColumnView instanceof FWDStaticGrid))
				$moColumnView->setAttrClass($this->csCSSClass . $this->csClassStaticColumnHeaders);
					
				$msColumnCode .= $moColumnView->draw();
			}
			$msColumnCode .= $this->caColumns[$miI]->drawFooter();
		}
		$msValuePopulate="";
		if($this->cbPopulateXML==true)
		$msValuePopulate="1";
		else
		$msValuePopulate="2";

		$msInputs = "<input type=hidden name='{$this->csName}_column_order' id='{$this->csName}_column_order' value='{$this->csOrder}' />
		<input type=hidden name='{$this->csName}_populatexml' id='{$this->csName}_populatexml' value='{$msValuePopulate}' />";

		return $msColumnCode . $msInputs;
	}

	/**
	 * Desenha o Titulo da Grid.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para desenhar o t�tulo da grid.
	 * retorna o c�digo HTML do t�tulo da grid</p>
	 * @access public
	 * @return string C�digo HTML que ser� inserido na div do titulo da grid
	 */
	public function drawGridTitle(){
		$msReturn = "";

		$miTitleWidth= $this->coBox->getAttrWidth();
		if($this->coGridTitle instanceof FWDViewGroup){
			$moBoxViewGroup = $this->coGridTitle->getObjFWDBox();
			if($moBoxViewGroup->getAttrHeight() > 0){
				$this->ciTitleHeight = $moBoxViewGroup->getAttrHeight();
			}
			if($this->coGridTitle){
				$msReturn .= "<div name='{$this->csName}_header_title' id='{$this->csName}_header_title' class='{$this->csCSSClass}{$this->csClassDivHeaderTitle}' style='position:absolute;height:{$this->ciTitleHeight};width:{$miTitleWidth};left:-1;top:-1;' >";
				foreach($this->coGridTitle->getContent() as $moViewGroupUnit)
				if(($moViewGroupUnit instanceof FWDStatic)||($moViewGroupUnit instanceof FWDStaticGrid))
				$moViewGroupUnit->setAttrClass($this->csCSSClass . $this->csClassStaticGridTitle);

				$msReturn .= $this->coGridTitle->drawBody();
				$msReturn .= "</div> ";
			}
			else{} //n�o tem TITLE da grid definida , assim n�o � necessario desenhar o TITLE
		}
		else{
			if($this->cbDinamicFill==true){
				$msReturn .= "<div name='{$this->csName}_header_title' id='{$this->csName}_header_title' class='{$this->csCSSClass}{$this->csClassDivHeaderTitle}' style='position:absolute;height:{$this->ciTitleHeight};width:{$miTitleWidth};left:-1;top:-1;' > </div> ";
			}else
			$this->ciTitleHeight=0;
		}
		return $msReturn;
	}

	/**
	 * retorna o evento que aciona o vidro na grid.
	 *
	 * <p>retorna o evento que aciona o vidro na grid, impedindo assim que algum
	 * evento seja executado enquanto outro evento � executado na grid</p>
	 * @access public
	 * @return FWDClientEvent evento cliente que aciona o vidro na grid
	 */
	public function getEventGlass(){
		$moBeforeEventGrid = new FWDClientEvent();
		$moBeforeEventGrid->setAttrValue(" gobi('{$this->csName}').showGlass();");
		$moBeforeEventGrid->setAttrEvent("onClick");
		return $moBeforeEventGrid;
	}
	/**
	 * Desenha o bot�o de refresh da grid.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para desenhar o bot�o de refresh da grid.
	 * retorna o c�digo HTML do t�tulo da grid</p>
	 * @access public
	 * @return string C�digo HTML que renderiza o bot�o de refresh da grid
	 */
	public function drawButtonRefresh($piHeight, $piWidth){
		$msReturn = "";
		$msReturnAux = '';

		if ($this->cbIsIE)
		$piWidth -= 2;
		$miButtonRefreshWidth = $this->ciRollerBarWidth+2;

		if($this->cbShowRefresh){
			$moButtonRefresh= new FWDViewButton();
			$miLeft = ceil(($this->ciRollerBarWidth - $this->ciButtonRefreshWidth)/2);
			$miTop = ceil(($this->ciColumnHeight - $this->ciButtonRefreshHeight)/2)-1;
			$moBoxRefresh = new FWDBox($miLeft,$miTop,$this->ciButtonRefreshHeight,$this->ciButtonRefreshWidth);
			$moButtonRefresh->setObjFWDBox($moBoxRefresh);
			$moButtonRefresh->setAttrClass($this->csCSSClass . $this->csClassButtonRefresh);
			$moButtonRefresh->setAttrName("{$this->csName}_button_refresh");
			$moEvent = new FWDServerEvent();
			$moEvent->setAttrFunction("{$this->csName}refresh");
			$moEvent->setAttrEvent("onClick");
			$moButtonRefresh->addObjFWDEvent($moEvent);
			$moButtonRefresh->addObjFWDEvent( $this->getEventGlass());


			$moButtonRefresh->setAttrTabIndex(0);
			$msReturnAux .= " ". $moButtonRefresh->draw();
		}
		$msReturn .= "<div name='{$this->csName}_button_refresh_div' id='{$this->csName}_button_refresh_div' class='{$this->csCSSClass}{$this->csClassDivRefreshButton}' style='position:absolute;height:{$this->ciColumnHeight};width:{$miButtonRefreshWidth};left:$piWidth;top:{$piHeight};overflow:hidden;'> {$msReturnAux}</div>";
		return $msReturn;
	}

	/**
	 * Desenha a Barra de Bot�es da grid.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar a barra de bot�es de pagina��o da grid.
	 * retorna o c�digo HTML da barra de botoes de pagina��o da grid</p>
	 * @access public
	 * @return string C�digo HTML que cont�m as inform��es da barra de bot�es de navega��o da grid
	 */
	function drawPageButtons()
	{
		$miDivTop=0;
		$miPageButtonHeight= $this->ciTitleHeight;
		$miPageCount = ceil($this->ciRowsCount / $this->ciRowsPerPage);
		$this->setLastPage($miPageCount);
		$miButtonHeight=$this->ciButtonChangeHeight;
		$miButtonWidth=$this->ciButtonChangeWidth;
		$miLeftBegin=0;
		$miButtonTop = floor(($miPageButtonHeight - $miButtonHeight) / 2);
		$miCumulativeWidth=0;
		$miProp = 1.5;
		$miButtonPropHeight = $miButtonHeight * $miProp;
		$miButtonPropTop =  $miButtonTop - 4;
		if($this->cbIsIE){
			$miButtonTop -=1;
		}else{
			$miButtonPropTop+=1;

		}
		$moEventNext = new FWDServerEvent();
		$moEventNext->setAttrFunction("{$this->csName}changepagenext");
		$moEventNext->setAttrEvent("onClick");
		$moEventNext->setAttrElements("{$this->csName}");

		$moEventBack = new FWDServerEvent();
		$moEventBack->setAttrFunction("{$this->csName}changepageback");
		$moEventBack->setAttrEvent("onClick");
		$moEventBack->setAttrElements("{$this->csName}");

		$moEventFirst = new FWDServerEvent();
		$moEventFirst->setAttrFunction("{$this->csName}changepagefirst");
		$moEventFirst->setAttrEvent("onClick");
		$moEventFirst->setAttrElements("{$this->csName}");

		$moEventLast = new FWDServerEvent();
		$moEventLast->setAttrFunction("{$this->csName}changepagelast");
		$moEventLast->setAttrEvent("onClick");
		$moEventLast->setAttrElements("{$this->csName}");

		$moEventText = new FWDClientEvent();
		$msJs = "return obj{$this->csName}.checkKeyPressTextPageButton(event);";
		$moEventText->setAttrEvent("onKeyPress");
		$moEventText->setValue($msJs,"true");

		$moEventTextOnClick = new FWDClientEvent();
		$moEventTextOnClick->setAttrEvent("onClick");
		if($this->cbIsIE){
			$msReturnAux = " gebi('{$this->csName}_actual_page').onselectstart=new Function (\"return true;\");";
		}else{
			$msReturnAux = " gebi('{$this->csName}_actual_page').onmousedown=reEnable; gebi('{$this->csName}_actual_page').onclick=reEnable;";
		}
		$moEventTextOnClick->setValue($msReturnAux,"true");

		//static para o texto "P�gina"
		$moStaticBox= new FWDBox($miLeftBegin + $miCumulativeWidth,$miButtonPropTop,$miButtonPropHeight,50);
		$miCumulativeWidth += $moStaticBox->getAttrWidth();
		$moStaticPage = new FWDStaticGrid($moStaticBox);
		$moStaticString = new FWDString();
		$moStaticString->setAttrValue(FWDLanguage::getPHPStringValue('fwdgrid_page_label',"p�gina"));
		$moStaticPage->setObjFWDString($moStaticString);
		$moStaticPage->setAttrName("{$this->csName}_pagina");
		$moStaticPage->setAttrClass($this->csCSSClass . $this->csClassStaticHeaderPageButton);

		//objeto textBox para o n�mero da p�gina atual
		$moActualPage = new FWDText();
		$moTextBox = new FWDBox($miLeftBegin+ $miCumulativeWidth,$miButtonPropTop,$miButtonPropHeight,30);
		$miCumulativeWidth += $moTextBox->getAttrWidth() + 5;
		$moActualPage->setValue($this->ciCurrentPage,"true");
		$moActualPage->setObjFWDBox($moTextBox);
		$moActualPage->setAttrName("{$this->csName}_actual_page");
		$moActualPage->setAttrClass($this->csCSSClass . $this->csClassHeaderTextEdit);
		$moActualPage->addObjFWDEvent($moEventText);
		$moActualPage->addObjFWDEvent($moEventTextOnClick);

		//objeto string para a frase "de"
		$moStaticDe = new FWDStaticGrid();
		$moTotalPagesString = new FWDString();
		$moTotalPagesString->setAttrValue(FWDLanguage::getPHPStringValue('fwdgrid_number_page_of',"de"));
		$moStaticDe->setObjFWDString($moTotalPagesString);
		$moStaticBox2= new FWDBox($miLeftBegin+$miCumulativeWidth,$miButtonPropTop,$miButtonPropHeight,20);
		$miCumulativeWidth += $moStaticBox2->getAttrWidth();
		$moStaticDe->setObjFWDBox($moStaticBox2);
		$moStaticDe->setAttrName("{$this->csName}_de_text");
		$moStaticDe->setAttrClass($this->csCSSClass . $this->csClassStaticHeaderPageButton);

		//objeto string para a frase "#totalPages"
		$moStaticDe2 = new FWDStaticGrid();
		$moTotalPagesString = new FWDString();
		$moTotalPagesString->setAttrValue("{$this->getLastPage()}");
		$moStaticDe2->setObjFWDString($moTotalPagesString);
		$moStaticBox2= new FWDBox($miLeftBegin+$miCumulativeWidth,$miButtonPropTop,$miButtonPropHeight,36);
		$miCumulativeWidth += $moStaticBox2->getAttrWidth()+10;
		$moStaticDe2->setObjFWDBox($moStaticBox2);
		$moStaticDe2->setAttrName("{$this->csName}_de");
		$moStaticDe2->setAttrClass($this->csCSSClass . $this->csClassStaticHeaderPageButton);

		//bot�o para a p�gina inicial
		$moFirstPage = new FWDViewButton();
		$moButtonNavigateFP = new FWDBox($miLeftBegin+$miCumulativeWidth,$miButtonTop,$miButtonHeight,$miButtonWidth);
		$miCumulativeWidth += $moButtonNavigateFP->getAttrWidth()+6;
		$moFirstPage->setObjFWDBox($moButtonNavigateFP);
		$moFirstPage->addObjFWDEvent($moEventFirst);
		$moFirstPage->setAttrName("{$this->csName}_first_page");
		$moFirstPage->setAttrClass($this->csCSSClass . $this->csClassButtonFirstPage);

		//bot�o para pagina anterior
		$moBackPage = new FWDViewButton();
		$moButtonNavigateBP = new FWDBox($miLeftBegin+$miCumulativeWidth,$miButtonTop,$miButtonHeight,$miButtonWidth);
		$miCumulativeWidth += $moButtonNavigateBP->getAttrWidth()+6;
		$moBackPage->setObjFWDBox($moButtonNavigateBP);
		$moBackPage->addObjFWDEvent($moEventBack);
		$moBackPage->setAttrName("{$this->csName}_back_page");
		$moBackPage->setAttrClass($this->csCSSClass . $this->csClassButtonBackPage);

		//bot�o para prox. p�gina
		$moNextPage = new FWDViewButton();
		$moButtonNavigateNP = new FWDBox($miLeftBegin+$miCumulativeWidth,$miButtonTop,$miButtonHeight,$miButtonWidth);
		$miCumulativeWidth += $moButtonNavigateNP->getAttrWidth()+6;
		$moNextPage->setObjFWDBox($moButtonNavigateNP);
		$moNextPage->addObjFWDEvent($moEventNext);
		$moNextPage->setAttrName("{$this->csName}_next_page");
		$moNextPage->setAttrClass($this->csCSSClass . $this->csClassButtonNextPage);

		//bot�o para ultima p�gina
		$moLastPage = new FWDViewButton();
		$moButtonNavigateLP = new FWDBox($miLeftBegin+$miCumulativeWidth,$miButtonTop,$miButtonHeight,$miButtonWidth);
		$miCumulativeWidth += $moButtonNavigateLP->getAttrWidth()+6;
		$moLastPage->setObjFWDBox($moButtonNavigateLP);
		$moLastPage->addObjFWDEvent($moEventLast);
		$moLastPage->setAttrName("{$this->csName}_last_page");
		$moLastPage->setAttrClass($this->csCSSClass . $this->csClassButtonLastPage);

		$moFirstPage->addObjFWDEvent($this->getEventGlass());
		$moBackPage->addObjFWDEvent($this->getEventGlass());
		$moNextPage->addObjFWDEvent($this->getEventGlass());
		$moLastPage->addObjFWDEvent($this->getEventGlass());

			
		$msReturn =	$moStaticPage->draw() .
		$moActualPage->draw() .
		$moStaticDe->draw() .
		$moStaticDe2->draw() .
		$moFirstPage->draw() .
		$moBackPage->draw() .
		$moNextPage->draw() .
		$moLastPage->draw() .
		  " <input type=hidden id='{$this->csName}_gridpageinfo' name='{$this->csName}_gridpageinfo' value='{$this->getCurrentPage()}:{$this->getLastPage()}:{$this->getAttrRowsCount()}' /> ";

		//elimina todos os <enter> do c�digo para funcionar com o ajax
		if($this->getAttrDinamicFill()==true)
		$msReturn = $this->scapeStringToAjax($msReturn);

		return $msReturn;
	}


	/**
	 * Renderiza o Menu de Contexto definido dentro da grid.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o menu de contexto definido dentro da grid.
	 * Esse menu de contexto s� pode ter como subtarget os valores (lines e / ou headers). Esse m�todo arruma
	 * o subtarget do menu de contexto para todas as c�lulas do corpo da grid (lines) e / ou para cada um dos
	 * cabe�alhos das colunas do corpo da grid (headers).
	 * retorna o c�digo HTML do menu de contexto definido dentro da grid</p>
	 * @access public
	 * @return string C�digo HTML que cont�m as inform��es do menu de contexto
	 */
	public function drawMenu()
	{
		$msCodeMenu = "";

		foreach($this->caMenus as $moMenu){
			$msMenuAllElements = strtolower($moMenu->getAttrElements());
			$maElements = explode(":",$msMenuAllElements);
			$msAuxElements="";
			$msAuxName = $this->csName."_r";
			foreach($maElements as $msElement){
				if(($msElement=="lines")&&($this->cbPopulate==true)&&($this->cbPopulateXML==true))
				for($miJ=1;$miJ<=$this->ciRowsCount;$miJ++)
				for($miI=1;$miI<=$this->ciColumnsCount;$miI++)
				if( ($this->caEventBlock[$miI]==0) && ($this->caColumnDisplay[$miI]==1))
				$msAuxElements .= $msAuxName.$miJ."_c".$miI.":";
				else{ }	//nao � necess�rio criar o alvo para o menu pois ou a coluna est� "escondida"
				//ou os eventos dessa coluna est�o bloqueados
				elseif(($msElement=="headers")){
					foreach($this->caColumns as $moColumn)
					if((!$moColumn->getAttrEventBlock()==true)&&($moColumn->getAttrDisplay()))
					$msAuxElements .= $moColumn->getAttrName() .":";
					else { } //this column can't receive any event
				}//end elseif
				else {}	// ou foi definido um element ou subelement nao permitido ou nao deve-se desenhar
				// o codigo do menu para o body jah que a grid tem preenchimento via ajax
			}
			$moMenu->setAttrElements($msAuxElements);
				
			$msCodeMenu .= $moMenu->drawHeader() . "<script language=\"JavaScript\">".$this->drawBodyMenuGrid($moMenu->getAttrName())." </script>" . $moMenu->drawFooter();
		}//end foreach

		return $msCodeMenu . $this->drawMenuACL();;
	}

	/**
	 * Renderiza o Menu de Contexto ACL.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o menu de contexto ACL.
	 * retorna o c�digo HTML do menu de contexto definido ACL</p>
	 * @access public
	 * @return string C�digo HTML que cont�m as inform��es do menu de contexto ACL
	 */
	public function drawMenuACL()
	{
		$msCodeMenu="";
		//renderiza os menus com ACL
		foreach($this->caMenusACL as $moMenu){
			$msMenuAllElements = strtolower($moMenu->getAttrElements());
			$maElements = explode(":",$msMenuAllElements);
			$msAuxElements="";
			foreach($maElements as $msElement){
				if(strpos($msElement,'_acl_')){
					$miRowNumber = substr($msElement,strpos($msElement,"_acl_")+5);
					for ($miJ=1; $miJ <= count($this->caColumns); $miJ++) {
						if ( ($this->caEventBlock[$miJ]==0) && ($this->caColumnDisplay[$miJ]==1)) {
							$msAuxElements .= $this->csName ."_r{$miRowNumber}_c".$miJ.":";
						}else{ }	//nao � necess�rio criar o alvo para o menu pois ou a coluna est� "escondida"
						//ou os eventos dessa coluna est�o bloqueados
					}
				}
			}
			$moMenu->setAttrElements($msAuxElements);
			$msCodeMenu .= $moMenu->drawHeader(). "<script language=\"JavaScript\">".$this->drawBodyMenuGrid($moMenu->getAttrName())." </script>" . $moMenu->drawFooter();
		}
		if($this->cbDinamicFill==true)
		$msCodeMenu =  $this->scapeStringToAjax($msCodeMenu);
		return $msCodeMenu;
	}

	/**
	 * Renderiza o o Footer do Menu de Contexto definido dentro da grid e o draw dos menus de contexto ACL da grid.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar os menu de contexto definido dentro da grid.
	 * Esse menu de contexto s� pode ter como subtarget os valores (lines e / ou headers). Esse m�todo arruma
	 * o subtarget do menu de contexto para todas as c�lulas do corpo da grid (lines) e / ou para cada um dos
	 * cabe�alhos das colunas do corpo da grid (headers).
	 * retorna os c�digos javascript dos menus de contexto e os c�digos HTML dos menus de contexto ACL
	 *  definido dentro da grid</p>
	 * @access public
	 * @return string C�digo HTML todas as informa��es necess�rias para o funcionamento dos menus de contexto via ajax
	 */

	public function drawMenuAjax()
	{
		$msAuxElements=" ";
		$msAuxName = $this->csName."_r";
		$maCodeMenu = array();

		if($this->cbDinamicFill==true){
			$miShiftRow =$this->ciRowsPerPage * ($this->ciCurrentPage -1)+1;
			$miRowscount = $miShiftRow + min($this->ciRowsPerPage,$this->ciRowsCount - ($this->ciRowsPerPage*($this->ciCurrentPage -1)) );
		}else{
			$miShiftRow = 1;
			$miRowscount = $this->ciRowsCount+1;
		}

		foreach($this->caMenus as $moMenu){
			$msMenuAllElements = strtolower($moMenu->getAttrElements());
			$maElements = explode(":",$msMenuAllElements);
			foreach($maElements as $msElement)
			if(($msElement=="lines")&&($this->cbPopulate==true)&&($this->cbPopulateXML))
			for($miJ=$miShiftRow;$miJ<$miRowscount;$miJ++)
			for($miI=1;$miI<=$this->ciColumnsCount;$miI++)
			if( ($this->caEventBlock[$miI]==0) && ($this->caColumnDisplay[$miI]==1))
			$msAuxElements .=" gebi('".$msAuxName.$miJ."_c".$miI."').oncontextmenu=".$moMenu->getAttrName().";";
			else{ }	//nao � necess�rio criar o alvo para o menu pois ou a coluna est� "escondida"
			//ou os eventos dessa coluna est�o bloqueados
			elseif(($msElement=="headers"))
			foreach($this->caColumns as $moColumn)
			if((!$moColumn->getAttrEventBlock()==true)&&($moColumn->getAttrDisplay()))
			$msAuxElements .="gebi('{$moColumn->getAttrName()}').oncontextmenu = {$moMenu->getAttrName()}; ";
			else { } //this column can't receive any event
			else {}	// ou foi definido um element ou subelement nao permitido ou nao deve-se desenhar
			// o codigo do menu para o body jah que a grid tem preenchimento via ajax
		}

		$msCodeMenu=" ";
		$msCodeMenuFunction=" ";
		//renderiza os menus com ACL
		foreach($this->caMenusACL as $moMenu){
			$msMenuAllElements = strtolower($moMenu->getAttrElements());
			$maElements = explode(":",$msMenuAllElements);
			foreach($maElements as $msElement){
				if(strpos($msElement,'_acl_')){
					$miRowNumber = substr($msElement,strpos($msElement,"_acl_")+5);
					for ($miJ=1; $miJ <= count($this->caColumns); $miJ++)
					if ( ($this->caEventBlock[$miJ]==0) && ($this->caColumnDisplay[$miJ]==1))
					$msAuxElements .= "gebi(\"{$this->csName}_r{$miRowNumber}_c{$miJ}\").oncontextmenu = {$moMenu->getAttrName()}; ";
					else{ }	//nao � necess�rio criar o alvo para o menu pois ou a coluna est� "escondida"
					//ou os eventos dessa coluna est�o bloqueados
				}
			}
			$msCodeMenu .= " " . $moMenu->drawHeader();
			$msCodeMenuFunction .= $this->drawBodyMenuGrid($moMenu->getAttrName());
		}

		$maCodeMenu[] = $this->scapeStringToAjax($msCodeMenu);
		$maCodeMenu[] = $this->scapeStringToAjax($msCodeMenuFunction);
		$maCodeMenu[] = $this->scapeStringToAjax($msAuxElements);

		return 	$maCodeMenu;
	}//end class


	/**
	 * Desenha o corpo menu de contexto para a grid.
	 *
	 * <p>Re�ne em uma vari�vel string, o c�gido da fun��o javascript respons�vel por acionar
	 * o menu de contexto utilizado pela grid
	 * @return string C�digo javascript da fun��o do menu de contexto
	 */
	public function drawBodyMenuGrid($msMenuName) {
		return "
    soMenuManager.addMenu('{$msMenuName}',window);
    function {$msMenuName}(event){
    var id='{$msMenuName}';
    obj{$this->csName}.js_action_target_menu(id,event);
    soMenuManager.hideMenus(); 
    js_show_menu(id,event);}";
	}


	/**
	 * Renderiza a grid via ajax.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para renderizar os bot�es de p�gina��o, o conte�do do corpo,
	 * e os menus de contexto, incluindo os menus ACL da grid</p>
	 * @access public
	 * @return string C�digo HTML serializado que cont�m os bot�es de pag., o corpo, e os menus da grid
	 */
	public function execEventPopulate($psGridEvent = 'populategrid'){
		$msGridCode = array();
		$maGridCode[]=$psGridEvent;
		$this->caGridRowsSelected=array();
		$this->caGridValue = array();
		 
		//obtem o c�digo html dos bot�es de pagina��o da grid
		$this->cbPopulate=true;
		if($this->cbDinamicFill==true)
		$maGridCode[] = $this->drawPageButtons();
		else{
			$maGridCode[] = ' ';
			$this->arrangeAllDimensions();
		}
		//desenha o body da grid via ajax
		$maGridCode[] = $this->drawBody();

		$maGridButtomCode = array();
		if($psGridEvent == 'user_edit'){
			//desenha os headers das colunas
			$maGridCode[] = $this->drawHeaderColumns();
		}else{
			if(($this->cbShowEdit)&&($this->cbPopulateXML)){
				//bot�o de edi��o das propriedades da grid
				$maGridButtomCode[] = $this->drawButtonEdit();
				//desenha os headers das colunas
				$maGridButtomCode[] = $this->drawHeaderColumns();
			}
		}
		//obtem o c�digo html do menu de contexto
		$maGridCodeAux = array_merge($maGridCode,$this->drawMenuAjax());
		$maGridCodeAux = array_merge($maGridCodeAux,$maGridButtomCode);

		//escapa os /r e os /n
		for($miI=0;$miI<count($maGridCodeAux);$miI++){
			$maGridCodeAux[$miI] = $this->scapeStringToAjax($maGridCodeAux[$miI]);
		}

		$msGridCodeAux = serialize($maGridCodeAux);
		$msGridCodeAux = str_replace('\\','\\\\',$msGridCodeAux);
		$msGridCodeScape = str_replace('"','\"',$msGridCodeAux);
		$moJsGridEvent = new FWDJsEvent(JS_GRID_EVENT,$this->csName,$msGridCodeScape);
		echo $moJsGridEvent->render();
	}

	/**
	 * Renderiza a grid via ajax (utilizando pelos eventos changepage[first,next,back,last,change] e refresh).
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para renderizar o conte�do do corpo,
	 * e os menus de contexto, incluindo os menus ACL da grid</p>
	 * @access public
	 * @return string C�digo HTML que cont�m o corpo, e os menus da grid
	 */
	public function gridPopulateChange($psGridEvent,$psCodePage){
		$maGridCode = array();
		$maGridCode[] = $psGridEvent;

		$this->cbPopulate=true;
		//desenha o body da grid via ajax
		$maGridCode[] = $this->drawBody();
		//obtem o c�digo html do menu de contexto
		$maGridCode = array_merge($maGridCode,$this->drawMenuAjax());
		$maGridCode[] = $psCodePage;
		//escapa os \r e os \n
		for($miI=0;$miI<count($maGridCode);$miI++){
			$maGridCode[$miI] = $this->scapeStringToAjax($maGridCode[$miI]);
		}
		$msGridCodeAux = serialize($maGridCode);

		$msGridCodeAux = str_replace('\\','\\\\',$msGridCodeAux);
			
		$msGridCodeScape = str_replace('"','\"',$msGridCodeAux);
		$moJsGridEvent = new FWDJsEvent(JS_GRID_EVENT,$this->csName,$msGridCodeScape);
		echo $moJsGridEvent->render();
	}

	/**
	 * Renderiza a grid via ajax (utilizado para alterar o ordenamento das colunas da grid).
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para renderizar o conte�do dos headers das colunas, do corpo,
	 * e os menus de contexto, incluindo os menus ACL da grid</p>
	 * @access public
	 * @return string C�digo HTML que cont�m o corpo, e os menus da grid
	 */
	public function gridPopulateOrderColumn($psGridEvent,$psCodePage){
		$maGridCode = array();
		$maGridCode[] = $psGridEvent;

		if(($this->cbDinamicFill==false))
		$this->arrangeAllDimensions();

		$this->caGridRowsSelected=array();
		$this->caGridValue = array();

		$this->cbPopulate=true;
		//desenha o body da grid via ajax
		$maGridCode[] = $this->drawBody();
		//arruma as dimens�es caso a grid seja est�tica
		//renderiza as colunas
		$maGridCode[] = $this->drawHeaderColumns();
		//obtem o c�digo html do menu de contexto
		$maGridCode = array_merge($maGridCode,$this->drawMenuAjax());
		$maGridCode[] = $psCodePage;
		//escapa os \r e os \n
		for($miI=0;$miI<count($maGridCode);$miI++){
			$maGridCode[$miI] = $this->scapeStringToAjax($maGridCode[$miI]);
		}
		$msGridCodeAux = serialize($maGridCode);

		$msGridCodeAux = str_replace('\\','\\\\',$msGridCodeAux);
		$msGridCodeScape = str_replace('"','\"',$msGridCodeAux);
		$moJsGridEvent = new FWDJsEvent(JS_GRID_EVENT,$this->csName,$msGridCodeScape);
		echo $moJsGridEvent->render();
	}

	/**
	 * Ordena a grid que n�o � ordenada via BD
	 *
	 * <p>Ordena os dados que est�o no vetor de ordenamento da grid conforme o tipo de ordenamento
	 * que est� nas colunas da grid</p>
	 * @access public
	 * @return string C�digo HTML que cont�m o corpo, e os menus da grid
	 */
	function orderGrid()
	{

		$maRowAux= array();
		$maOrder = explode(":",$this->csOrder);
		$miCont = 0;
		if(($maOrder)&&($this->cbPopulateXML==true))
		foreach($maOrder as $msColOrder){
			$miCont++;
			if($msColOrder!=""){
				$miColumn="";
				$msChar = substr(trim($msColOrder),0,1);
				if(($msChar=="+")||($msChar=="-"))// ascendente
				$miColumn = substr(trim($msColOrder),1);
				else{//assumindo que soh veio um n�mero , ordena��o ascendente
					$miColumn = trim($msColOrder);
					$msChar="+";
				}
				$moColumn = $this->caColumns[$miColumn -1];
				if($moColumn)
				if(($moColumn->getAttrCanOrder()=="true")){
					$maOrder = array();
					for ($miI=1; $miI <= $this->ciRowsCount; $miI++) {
						if (isset($this->caRows[ $miI]))
						$maRow = &$this->caRows[$miI];
						else {
							$maOrder[$miI] = "";
							continue;
						}
						$maOrder[$miI] = isset( $maRow[$miColumn]) ? $maRow[$miColumn] : "";
					}

					if ($msChar == "+"){
						asort($maOrder);
					}else
					arsort($maOrder);
						
					$miI = 1;
					$maRowAux{$miCont} = array();
					foreach( $maOrder as $miKey => $msValue) {
						$maRowAux{$miCont}[$miI++] = $this->caRows[$miKey];
					}
					unset($this->caRows);
					unset($maOrder);
					$this->caRows = &$maRowAux{$miCont};
				}
				else trigger_error("Trying to order an inexistent column! column number '{$miColumn}'",E_USER_WARNING);
			}//else{}//msColOrder vazio devido a um : amais na string
		}
	}

	/**
	 * M�todo executado quando o XML do elemento termina de carregar.
	 *
	 * <p>M�todo executado quando o XML do elemento termina de carregar. Utilizado para
	 * ajustar n�mero da pagina atual e do n�mero de p�ginas da grid.</p>
	 *
	 * @access public
	 */
	public function execute()
	{
		$moEventHandler = FWDStartEvent::getInstance();
		$moEventHandler->addAjaxEvent(new FWDGridRefresh("{$this->csName}refresh",$this->csName));
		$moEventHandler->addSubmitEvent(new FWDGridExport("{$this->csName}_button_export",$this->csName));
		$moEventHandler->addAjaxEvent(new FWDGridChangePageFirst("{$this->csName}changepagefirst",$this->csName));
		$moEventHandler->addAjaxEvent(new FWDGridChangePageChange("{$this->csName}changepagechange",$this->csName));
		$moEventHandler->addAjaxEvent(new FWDGridPopulate("{$this->csName}populate",$this->csName));
		//$moEventHandler->addAjaxEvent(new FWDGridRefreshSelect("{$this->csName}refreshselect",$this->csName));

		if($this->cbShowEdit){
			$moEventHandler->addAjaxEvent(new FWDGridUserEdit("{$this->csName}_grid_preference_save",$this->csName));
			$moEventHandler->addAjaxEvent(new FWDGridDrawUserEdit("{$this->csName}_draw_user_edit",$this->csName));
			$moEventHandler->addAjaxEvent(new FWDGridUserEditDefaultValues("{$this->csName}_grid_preference_default",$this->csName));
			$moEventHandler->addAjaxEvent(new FWDGridCreatePopupEditPref("{$this->csName}_grid_open_popup_pref",$this->csName));
		}
			
		if($this->cbDinamicFill == true) {
			$msInfoPages = FWDWebLib::getPOST($this->csName."_gridpageinfo");
			if($msInfoPages){
				$maInfoPage = explode(":",$msInfoPages);
				$this->setCurrentPage($maInfoPage[0]);
				$this->setLastPage($maInfoPage[1]);
				$this->setAttrRowsCount($maInfoPage[2]);
			}
			//else { }//nao existe informa��o de paginas (pag. atual e quant. de pag.) da grid no html lido pelo ajax
			$moEventHandler->addAjaxEvent(new FWDGridChangePageNext("{$this->csName}changepagenext",$this->csName));
			$moEventHandler->addAjaxEvent(new FWDGridChangePageBack("{$this->csName}changepageback",$this->csName));
			$moEventHandler->addAjaxEvent(new FWDGridChangePageLast("{$this->csName}changepagelast",$this->csName));
			$moEventHandler->addAjaxEvent(new FWDGridChangePageFirst("{$this->csName}changepagefirst",$this->csName));
			$moEventHandler->addAjaxEvent(new FWDGridChangePageChange("{$this->csName}changepagechange",$this->csName));
		}
		//else { } // a grid n�o tem preenchimento via ajax, assim os eventos n�o precisam ser criados
		foreach($this->caColumns as $moColumn){
			if(($moColumn->getAttrAlias())&&($moColumn->getAttrCanOrder()=="true")&&(!$moColumn->getAttrEventBlock())&&($this instanceof FWDDBGrid))
			$moEventHandler->addAjaxEvent(new FWDGridColumnOrder("{$this->csName}_{$moColumn->getAttrAlias()}",$this->csName.":".$moColumn->getAttrAlias()));
			elseif(($moColumn->getAttrCanOrder()=="true")&&(!$moColumn->getAttrEventBlock())&&(!($this instanceof FWDDBGrid)))
			$moEventHandler->addAjaxEvent(new FWDGridColumnOrder("{$this->csName}_{$moColumn->getAttrName()}",$this->csName.":".$moColumn->getAttrName()));

			if($moColumn->getAttrCanOrder()=="true"){
				if($this instanceof FWDDBGrid)
				$msInfoOrderBy = FWDWebLib::getPOST("{$this->csName}_{$moColumn->getAttrAlias()}_orderby_value");
				else
				$msInfoOrderBy = FWDWebLib::getPOST("{$this->csName}_{$moColumn->getAttrName()}_orderby_value");
				if($msInfoOrderBy)
				$moColumn->setOrderBy($msInfoOrderBy);
			}
		}

		// reconfigura a grid conforme as preferencias do usu�rio
		if(($this->cbShowEdit==true)&&($this->csPreferencesHandle!=''))
		{
			$moHandlePref = FWDWebLib::getObject($this->csPreferencesHandle);
			if($moHandlePref){
				$moHandlePref->loadPrefereces($this->csName);
				//altera as colunas que ser�o exibidas e a ordem em que as colunas ser�o exibidas
				if($moHandlePref->getOrderColumn()){
					$this->caOrderColumnShow = explode(':',$moHandlePref->getOrderColumn());
					for($miI=0;$miI<count($this->caColumns);$miI++)
					$this->caColumns[$miI]->setAttrDisplay('false');
					for($miI=0;$miI<count($this->caOrderColumnShow);$miI++)
					if(isset($this->caColumns[$this->caOrderColumnShow[$miI]]))
					$this->caColumns[$this->caOrderColumnShow[$miI]]->setAttrDisplay('true');
				}else{
					for($miI=0;$miI<count($this->caColumns);$miI++)
					if($this->caColumns[$miI]->getAttrDisplay()==true)
					$this->caOrderColumnShow[]=$miI;
				}
				//altera as propriedades de largura e noWrap de cada uma das colunas da grid
				if($moHandlePref->getColumnsProp()){
					$maAuxColumnsProp = explode(';',$moHandlePref->getColumnsProp());
					for($miI = 0;$miI<count($maAuxColumnsProp);$miI++){
						$maColumnProp = explode(':',$maAuxColumnsProp[$miI]);
						$this->caColumns[$maColumnProp[0]]->getObjFWDBox()->setAttrWidth($maColumnProp[1]);
						if($maColumnProp[2]=='false')
						$this->caColumns[$maColumnProp[0]]->setAttrColNoWrap("true");
						else
						$this->caColumns[$maColumnProp[0]]->setAttrColNoWrap("false");
					}
				}
				//altera a ordena��o dos dados das colunas da grid
				if($moHandlePref->getOrderColumnsData()){
					$this->csOrder = $moHandlePref->getOrderColumnsData();
				}
				//altera o numero de linhas por p�gina
				if($moHandlePref->getRowsPerPage()){
					$this->ciRowsPerPage = $moHandlePref->getRowsPerPage();
				}
				//altera a altura das linhas
				if($moHandlePref->getRowsHeight()){
					$this->ciRowHeight = $moHandlePref->getRowsHeight();
				}
				//cria um tooltip na coluna alvo com o conte�do da coluna source
				if($moHandlePref->getToolTipCol()){
					for($miI=0;$miI<count($this->caColumns);$miI++)
					$this->caColumns[$miI]->setAttrToolTip('false');
					$maTTaux = explode(':',$moHandlePref->getToolTipCol());
					$this->caColumns[$maTTaux[1]-1]->setAttrToolTip('true');
					$this->caColumns[$maTTaux[1]-1]->setSourceToolTip($maTTaux[0]);
				}
			}else{}
		}else{
			$msInfoOrder = trim(FWDWebLib::getPOST("{$this->csName}_column_order"));
			if($msInfoOrder)
			$this->csOrder = $msInfoOrder;
		}

		$msPopulateValue = FWDWebLib::getPOST("{$this->csName}_populatexml");
		if($msPopulateValue=="1")
		$this->setAttrPopulate("true");
		elseif($msPopulateValue=="2")
		$this->setAttrPopulate("false");

		$msClassOver = FWDWebLib::getPOST("{$this->csName}_row_over");
		if($msClassOver){
			if($this->csCSSClass){
				$maClassOver = explode("$this->csCSSClass",$msClassOver);
				if(isset($maClassOver[1])){
					$this->csClassBodyZebraOver = $maClassOver[1];
				}
			}else
			$this->csClassBodyZebraOver = $msClassOver;
		}

		$msClassSelected = FWDWebLib::getPOST("{$this->csName}_row_selected");
		if($msClassSelected){
				
			if($this->csCSSClass){
				$maClassSelected = explode("$this->csCSSClass",$msClassSelected);
				if(isset($maClassSelected[1])){
					$this->csClassBodyRowSelected = $maClassSelected[1];
				}
			}else
			$this->csClassBodyRowSelected = $msClassSelected;
		}
		//value da grid contendo as linhas selecionadas da grid
		$auxRowsSelectedValue=FWDWebLib::convertToISO(FWDWebLib::getPOST("{$this->csName}_value"),true);
		//echo "alert('{$auxRowsSelectedValue}');";
		if(($auxRowsSelectedValue)&&($auxRowsSelectedValue!=":"))
		if(!(strpos($auxRowsSelectedValue,'a')===false)&&(strpos($auxRowsSelectedValue,'a')==0) )
		$this->caGridValue=FWDWebLib::unserializeString($auxRowsSelectedValue);
		else $this->caGridValue= array();
			
		//ids das linas com os selected columns da grid
		$auxRowsSelected = FWDWebLib::convertToISO(FWDWebLib::getPOST("{$this->csName}_value_row_id"),true);
		//echo "alert('{$auxRowsSelected}');";
		if(!(strpos($auxRowsSelected,'a')===false)&&(strpos($auxRowsSelected,'a')==0))
		$this->caGridRowsSelected=FWDWebLib::unserializeString($auxRowsSelected);
		else
		$this->caGridRowsSelected= array();

		$msAuxSelectHack = FWDWebLib::getPOST("{$this->csName}_hack_selected_rows");
		if($msAuxSelectHack){
			if(!(strpos($msAuxSelectHack,'a')===false)&&(strpos($msAuxSelectHack,'a')==0)){
				$maHackSel=FWDWebLib::unserializeString($msAuxSelectHack);
				if(($maHackSel[1]==1)||($maHackSel[1]==2)){
					$this->pbAddHackSelectedRows = $maHackSel[1];
					$this->caHackSelectRows = $maHackSel[0];
				}else{
					$this->pbAddHackSelectedRows = null;
					$this->caHackSelectRows = null;
				}
			}else{
				$this->pbAddHackSelectedRows = null;
				$this->caHackSelectRows = null;
			}
		}
	}

	public function scapeStringToAjax($psValue)
	{
		return str_replace(array("\n","\r"),array(' ',' '),$psValue);;
	}
}
?>