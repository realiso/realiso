<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDDiv. Implementa uma div simples (html tag 'div').
 *
 * <p>Classe que implementa uma div simples (html tag 'div').</p>
 * @package FWD5
 * @subpackage view
 */
class FWDDiv extends FWDDrawing {

	/**
	 * Box da div
	 * @var FWDBox
	 * @access protected
	 */
	protected $coBox = null;

	/**
	 * Classe CSS
	 * @var string
	 * @access protected
	 */
	protected $csClass = "";

	/**
	 * Valor da div (conteudo entre as tags 'div' e '/div')
	 * @var string
	 * @access protected
	 */
	protected $csValue = "";

	/**
	 * Nome da div (utilizada como 'name' e 'id' da div)
	 * @var string
	 * @access protected
	 */
	protected $csName = "";

	/**
	 * Par�metros extra para o atributo 'style'
	 * @var string
	 * @access protected
	 */
	protected $csExtraStyle = "";

	/**
	 * Atributo extra da div (pode conter mais de um atributo, separando por espa�os)
	 * @var string
	 * @access protected
	 */
	protected $csExtraAttribute = "";

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDDiv.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites da div
	 */
	public function __construct($poBox = null) {
		$this->coBox = $poBox;
	}

	/**
	 * Desenha o cabe�alho da div.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o cabe�alho da div.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites da div
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	protected function drawHeader($poBox = null) {
		$msGlobalProperty = "name='{$this->csName}' id='{$this->csName}' class='{$this->csClass}'";
		$msStyle  = "left:{$this->coBox->getAttrLeft()};";
		$msStyle .= "top:{$this->coBox->getAttrTop()};";
		$msStyle .= "height:{$this->coBox->getAttrHeight()};";
		$msStyle .= "width:{$this->coBox->getAttrWidth()};";
		$msStyle .= "position:absolute;";
		$msStyle .= $this->csExtraStyle;
		return "<div {$msGlobalProperty} style='{$msStyle}' {$this->csExtraAttribute} >\n";
	}

	/**
	 * Desenha o corpo da div.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o corpo da div.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites da div
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	protected function drawBody($poBox = null) {
		return $this->csValue;
	}

	/**
	 * Desenha o rodap� da div.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o rodap� da div.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites da div
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	protected function drawFooter($poBox = null) {
		return "</div>\n";
	}

	/**
	 * Seta a box da div.
	 *
	 * <p>Seta a box da div.</p>
	 * @access public
	 * @param FWDBox $poBox Box da div
	 */
	public function setObjFWDBox($poBox) {
		$this->coBox = $poBox;
	}

	/**
	 * Seta a classe da div.
	 *
	 * <p>Seta a classe da div.</p>
	 * @access public
	 * @param string $psClass Classe da div
	 */
	public function setClass($psClass) {
		$this->csClass = $psClass;
	}

	/**
	 * Seta o valor da div.
	 *
	 * <p>Seta o valor da div.</p>
	 * @access public
	 * @param string $psValue Valor da div
	 */
	public function setValue($psValue) {
		$this->csValue = $psValue;
	}

	/**
	 * Seta o nome da div.
	 *
	 * <p>Seta o nome da div.</p>
	 * @access public
	 * @param string $psValue Nome da div
	 */
	public function setName($psName) {
		$this->csName = $psName;
	}

	/**
	 * Seta par�metros extra para o atributo 'style'.
	 *
	 * <p>Seta par�metros extra para o atributo 'style'.</p>
	 * @access public
	 * @param string $psExtraStyle Par�metros extra para o atributo 'style'
	 */
	public function setExtraStyle($psExtraStyle) {
		$this->csExtraStyle = $psExtraStyle;
	}

	/**
	 * Seta o atributo extra da div.
	 *
	 * <p>Seta o atributo extra da div. Pode conter mais de um atributo
	 * (separados por espa�os em branco).</p>
	 * @access public
	 * @param string $psExtraAttribute Atributo extra da div
	 */
	public function setExtraAttribute($psExtraAttribute) {
		$this->csExtraAttribute = $psExtraAttribute;
	}

	/**
	 * Retorna a box da div.
	 *
	 * <p>Retorna a box da div.</p>
	 * @access public
	 * @return FWDBox Box da div
	 */
	public function getObjFWDBox() {
		return $this->coBox;
	}

	/**
	 * Retorna a classe da div.
	 *
	 * <p>Retorna a classe da div.</p>
	 * @access public
	 * @return string Classe da div
	 */
	public function getClass() {
		return $this->csClass;
	}

	/**
	 * Retorna o valor da div.
	 *
	 * <p>Retorna o valor da div.</p>
	 * @access public
	 * @return string Valor da div
	 */
	public function getValue() {
		return $this->csValue;
	}

	/**
	 * Retorna o nome da div.
	 *
	 * <p>Retorna o nome da div.</p>
	 * @access public
	 * @return string Nome da div
	 */
	public function getName() {
		return $this->csName;
	}

	/**
	 * Retorna par�metros extra para o atributo 'style'.
	 *
	 * <p>Retorna par�metros extra para o atributo 'style'.</p>
	 * @access public
	 * @return string Par�metros extra para o atributo 'style'
	 */
	public function getExtraStyle() {
		return $this->csExtraStyle;
	}

	/**
	 * Retorna atributo extra da div.
	 *
	 * <p>Retorna atributo extra da div. Pode conter mais de um atributo
	 * (separados por espa�os em branco).</p>
	 * @access public
	 * @return string Atributo extra da div
	 */
	public function getExtraAttribute() {
		return $this->csExtraAttribute;
	}
}
?>