<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDMemo. Implementa �reas de texto.
 *
 * <p>Classe que implementa �reas de texto (html tag 'textarea').</p>
 * @package FWD5
 * @subpackage view
 */
class FWDMemo extends FWDView implements FWDPostValue {

	/**
	 * Define se a memo est� desabilitado ou n�o
	 * @var boolean
	 * @access protected
	 */
	protected $cbDisabled = false;

	/**
	 * Define se deve existir uma barra de rolagem no caso de um overflow
	 * @var boolean
	 * @access protected
	 */
	protected $cbScrollbar = true;

	protected $readonly = false;

	/**
	 * Nome do label associado ao objeto
	 * @var string
	 * @access protected
	 */
	protected $csLabel = "";

	protected $ciMaxLength = 0;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDMemo.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites da memo
	 */
	public function __construct($poBox = null) {
		parent::__construct($poBox);
	}

	/**
	 * Desenha o cabe�alho do memo.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o cabe�alho do memo.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do memo
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawHeader($poBox = null) {

		$msScrollbar = ($this->getAttrScrollbar()?"overflow:auto;":"overflow:hidden;");

		$msStyle = $this->getStyle();
		$msStyle = "style='{$msStyle}{$msScrollbar}'";

		$msAttrDefault = $this->getGlobalProperty();

		$msDisabled = $this->cbDisabled ? "disabled" : "";
		$readonly = $this->readonly ? "readonly=\"readonly\"" : "";
		$msLabel = "";
		if($this->csLabel)
		$msLabel = "label='{$this->csLabel}'";

		$msMaxLength = $this->ciMaxLength ? "onKeyDown='limitTextArea(this, {$this->ciMaxLength});' onKeyUp='limitTextArea(this, {$this->ciMaxLength});'" : "";

		return "<textarea {$msAttrDefault} {$this->getEvents()} {$msStyle} {$msMaxLength} {$msLabel} {$msDisabled} {$readonly}>";
	}

	/**
	 * Desenha o corpo do memo.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o corpo do memo.</p>
	 * @access public
	 *
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawBody() {
		return $this->getValue();
	}

	/**
	 * Desenha o rodap� do memo.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o rodap� do memo.</p>
	 * @access public
	 *
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawFooter() {
		return "</textarea>\n";
	}

	/**
	 * Atribui valor � vari�vel value.
	 *
	 * <p>Atribui valor � vari�vel value, devendo retirar espa�os em branco
	 * no inicio e/ou no fim da string.</p>
	 * @access public
	 * @param string $psValue Valor a ser atribu�do
	 */
	public function setAttrValue($psValue) {
		$this->coString->setAttrValue(trim($psValue));
	}

	/**
	 * Seta a memo como desabilitada.
	 *
	 * <p>M�todo para setar a memo como desabilitada.</p>
	 * @access public
	 * @param boolean $pbDisabled Define se a memo est� desabilitada ou n�o
	 */
	public function setAttrDisabled($pbDisabled) {
		$this->cbDisabled = FWDWebLib::attributeParser($pbDisabled,array("true"=>true, "false"=>false),"pbDisabled");
	}

	/**
	 * Seta o nome do Label do objeto.
	 *
	 * <p>M�todo para setar o nome do label do objeto, utilizado para a troca de cor, no caso do objeto ter
	 * preenchimento obrigat�rio e n�o estar preenchido.</p>
	 * @access public
	 * @param string $psLabel nome do label associado ao objeto
	 */
	public function setAttrLabel($psLabel) {
		$this->csLabel = $psLabel;
	}

	/**
	 * Retorna o valor booleano do atributo Disabled.
	 *
	 * <p>Retorna o valor booleano TRUE se a memo est� desabilitada.
	 * Retorna FALSE caso contr�rio.</p>
	 * @access public
	 * @return boolean Indica se a memo est� desabilitada ou n�o
	 */
	public function getAttrDisabled() {
		return $this->cbDisabled;
	}

	/**
	 * Seta o valor booleano do atributo Scrollbar.
	 *
	 * <p>Seta o valor booleano do atributo que define se deve existir
	 * uma barra de rolagem no caso de um overflow. Para a barra de rolagem
	 * existir, o atributo deve conter o valor booleano TRUE. Para n�o existir,
	 * o atributo deve conter o valor booleano FALSE.</p>
	 * @access public
	 * @param string $pbScrollbar Define se deve existir uma barra de rolagem no caso de um overflow
	 */
	public function setAttrScrollbar($pbScrollbar) {
		$this->cbScrollbar = FWDWebLib::attributeParser($pbScrollbar,array("true"=>true, "false"=>false),"pbScrollbar");
	}

	public function getAttrReadonly() {
		return $this->readonly;
	}

	public function setAttrReadonly($readonly) {
		$this->readonly = $readonly;
	}

	/**
	 * Retorna o valor booleano do atributo Scrollbar.
	 *
	 * <p>Retorna o valor booleano TRUE se deve existir uma barra de rolagem
	 * no caso de um overflow. Retorna FALSE caso contr�rio.</p>
	 * @access public
	 * @return boolean Indica se deve existir uma barra de rolagem no caso de um overflow
	 */
	public function getAttrScrollbar() {
		return $this->cbScrollbar;
	}

	public function setAttrMaxLength($piMaxLength) {
		if($piMaxLength > 0){
			$this->ciMaxLength = $piMaxLength;
		}else{
			trigger_error("Invalid maxlength value: '{$piMaxLength}'",E_USER_WARNING);
		}
	}

	public function getAttrMaxLength() {
		return $this->ciMaxLength;
	}
}
?>