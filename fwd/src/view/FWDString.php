<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDString. Representa a tag 'string' do xml.
 *
 * <p>Classe que representa a tag 'string' do xml. Utilizada para
 * inserir textos no xml.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDString {

	/**
	 * String do xml
	 * @var string
	 * @access protected
	 */
	protected $csString = "";

	/**
	 * Id da string
	 * @var string
	 * @access protected
	 */
	protected $csId = "";

	/**
	 * Indica se o conte�do do elemento vai ter as aspas escapadas ou n�o
	 * @var boolean
	 * @access protected
	 */
	protected $cbNoEscape = false;

	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDString.</p>
	 * @access public
	 */
	public function __construct() {
	}

	/**
	 * Seta o valor da string.
	 *
	 * <p>M�todo para setar o valor da string.</p>
	 * @param string $psString String
	 */
	public function setAttrValue($psString) {
		$this->csString = ($psString);
	}

	/**
	 * Atribui valor, condicionalmente, � vari�vel value.
	 *
	 * <p>Atribui valor � vari�vel value, condicionando atrav�s do segundo par�metro.
	 * Se force for FALSE, concatena o valor se a vari�vel tiver conte�do;
	 * se TRUE atribui mesmo que tenha conte�do (sobrescreve).</p>
	 * @access public
	 * @param string $psValue Valor a ser atribu�do.
	 * @param boolean $pbForce For�ar o valor mesmo que j� tenha conte�do
	 */
	public function setValue($psString, $psForce = false) {
		/* Teste se est� dentro da fun��o xml_load, para que os valores sejam traduzidos
		 * quando o sistema estiver usando tradu��o.
		 */
		$moFWDWebLib = FWDWebLib::getInstance();
		if(!isset($this->csString) || !$this->csString || $psForce){
			if($moFWDWebLib->isXmlLoadRunning()) {
				if($psString=="" && !empty($this->csString))
				$psString = $this->csString;
				$this->setAttrValue(FWDLanguage::getXMLStringValue($this->csId,$psString));
			} else {
				$this->setAttrValue($psString);
			}
		}else{
			$this->setAttrValue($this->csString.$psString);
		}
	}

	/**
	 * Retorna o valor da string.
	 *
	 * <p>M�todo para retornar o valor da string.</p>
	 * @return string Valor da string
	 */
	public function getAttrString(){
		$msString = $this->csString;
		if($msString=="" && !empty($this->csId)) {
			$msString = FWDLanguage::getXMLStringValue($this->csId,$this->csString);
		}
		if(!$this->getAttrNoEscape()){
			$msString = str_replace(array("'",'"'),array('&#39;','&quot;'),$msString);
		}
		return $msString;
	}

	/**
	 * Seta o identificador da string.
	 *
	 * <p>M�todo para setar o identificador da string.</p>
	 * @param string $psId Identificador da string
	 */
	public function setAttrId($psId) {
		if ($psId)
		$this->csId = $psId;
		else
		trigger_error("Invalid string id: '{$psId}'",E_USER_WARNING);
	}

	/**
	 * Retorna o identificador da string.
	 *
	 * <p>M�todo para retornar o identificador da string.</p>
	 * @return string Identificador da string
	 */
	public function getAttrId() {
		return $this->csId;
	}

	/**
	 * Seta o atributo noescape
	 *
	 * <p>Seta o atributo noescape. True indica que o conte�do do elemento n�o
	 * deve ter as aspas escapadas e false, que deve.</p>
	 * @param string $pbNoEscape Indica se o conte�do do elemento vai ter as aspas escapadas ou n�o
	 */
	public function setAttrNoEscape($pbNoEscape){
		$this->cbNoEscape = FWDWebLib::attributeParser($pbNoEscape,array("true"=>true, "false"=>false),"noescape");
	}

	/**
	 * Retorna o valor do atributo noescape
	 *
	 * <p>Retorna o atributo noescape. True indica que o conte�do do elemento n�o
	 * deve ter as aspas escapadas e false, que deve.</p>
	 * @return string Indica se o conte�do do elemento vai ter as aspas escapadas ou n�o
	 */
	public function getAttrNoEscape(){
		return $this->cbNoEscape;
	}

	/**
	 * Retorna informa��es do objeto necess�rias para a tradu��o.
	 *
	 * <p>M�todo para retornar informa��es do objeto necess�rias para a tradu��o.</p>
	 * @access public
	 * @return array Array com a classe, valor e nome do objeto
	 */
	public function getTranslateTable() {
		if ($this->csId)
		return array( "{$this->csId}" => array("value" => $this->csString));
	}
}
?>