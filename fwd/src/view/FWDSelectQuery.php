<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDSelectQuery. Implementa Querys para popular elementos FWDDBSelect.
 *
 * <p>Classe que implementa Querys para popular elementos FWDDBSelect com
 * informa��es do BD.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDSelectQuery extends FWDDBDataSet {

	/**
	 * Id do Item do Select ('id' a ser retornado pela Query)
	 * @var string
	 * @access private
	 */
	protected $csId = "";

	/**
	 * Nome do Item do Select ('value' a ser retornado pela Query)
	 * @var string
	 * @access private
	 */
	protected $csValue = "";

	/**
	 * Nome da tabela do banco em que encontram-se as colunas 'id' e 'value'
	 * @var string
	 * @access private
	 */
	protected $csTable = "";

	/**
	 * Cria a Query.
	 *
	 * <p>M�todo para criar a Query.</p>
	 * @access public
	 */
	public function execute() {
		$this->addFWDDBField(new FWDDBField($this->getId(), "select_id", DB_NUMBER));
		$this->addFWDDBField(new FWDDBField($this->getValue(), "select_value", DB_STRING));
		$this->setQuery("SELECT ".$this->getId()." as select_id, ".$this->getValue()." as select_value FROM ".$this->getTable());
		parent::execute();
	}

	/**
	 * Seta o Id da Query.
	 *
	 * <p>M�todo para setar o Id da Query.</p>
	 * @access public
	 * @param string $psId Id a ser retornado pela Query
	 */
	public function setId($psId) {
		$this->csId = $psId;
	}

	/**
	 * Retorna o Id da Query.
	 *
	 * <p>M�todo para retornar o Id da Query.</p>
	 * @access public
	 * @return string Id a ser retornado pela Query
	 */
	public function getId() {
		return $this->csId;
	}

	/**
	 * Seta o Value da Query.
	 *
	 * <p>M�todo para setar o Value da Query.</p>
	 * @access public
	 * @param string $psValue Value a ser retornado pela Query
	 */
	public function setValue($psValue) {
		$this->csValue = $psValue;
	}

	/**
	 * Retorna o Value da Query.
	 *
	 * <p>M�todo para retornar o Value da Query.</p>
	 * @access public
	 * @return string Value a ser retornado pela Query
	 */
	public function getValue() {
		return $this->csValue;
	}

	/**
	 * Seta o nome da tabela na qual ser� efetuada a Query.
	 *
	 * <p>M�todo para setar o nome da tabela na qual ser� efetuada a Query.</p>
	 * @access public
	 * @param string $psTable Nome da Tabela onde ser� efetuada a Query
	 */
	public function setTable($psTable) {
		$this->csTable = $psTable;
	}

	/**
	 * Retorna o nome da tabela na qual ser� efetuada a Query.
	 *
	 * <p>M�todo para retornar o nome da tabela na qual ser� efetuada a Query.</p>
	 * @access public
	 * @return string Nome da Tabela onde ser� efetuada a Query
	 */
	public function getTable() {
		return $this->csTable;
	}
}
?>