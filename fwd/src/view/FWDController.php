<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDController. Implementa um Controlador.
 *
 * <p>Classe que implementa um Controlador na Framework FWD5.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDController extends FWDView implements FWDPostValue {

	/**
	 * N�mero de itens do Controlador
	 * @var integer
	 * @access protected
	 */
	protected $ciItems = 0;

	/**
	 * Array de FWDItemControllers
	 * @var array
	 * @access protected
	 */
	protected $caContent = array();

	/**
	 * Nome do label associado ao objeto
	 * @var string
	 * @access protected
	 */
	protected $csLabel = "";

	/**
	 * Classe dos itens controlados pelo controller
	 * @var string
	 * @access protected
	 */
	protected $csItensClass = '';

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDController.</p>
	 * @access public
	 */
	public function __construct(){
		parent::__construct();
	}

	/**
	 * Desenha o cabe�alho do controller.
	 * @todo Otimizar baseado no indice do array
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o cabe�alho do controller.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do checkbox
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawHeader($poBox = null){
		$msExtraAttrs = '';
		$msValue = '';
		if($this->csItensClass=='FWDRadioBox'){
			foreach($this->getContent() as $msElemKey => $moElem){
				if($moElem->getAttrCheck()){
					$msValue = $msElemKey;
					break;
				}
			}
		}else{
			$maValue = array();
			$maCheckAll = array();
			$maItemKeys = array();
			foreach($this->getContent() as $msElemKey=>$moElem){
				if($moElem->getAttrCheck())
				$maValue[] = $msElemKey;
				$maItemKeys[] = $msElemKey;
				if($moElem->getAttrSelectAll()){
					$maCheckAll[] = $msElemKey;
				}
			}
			$msExtraAttrs.= " checkAll='".implode(':',$maCheckAll)."'";
			$msExtraAttrs.= " itemKeys='".implode(':',$maItemKeys)."'";
			$msValue = implode(':',$maValue);
		}
		$msExtraAttrs.= ($this->csLabel?" label='{$this->csLabel}'":'');

		$msOut = "<input type='hidden' id='{$this->csName}' name='{$this->csName}' value='$msValue'{$msExtraAttrs}/>\n"
		."<script type='text/javascript'>"
		."var soElem=new ".get_class($this)."('{$this->csName}');"
		."gebi('{$this->csName}').object=soElem;";
		foreach($this->getContent() as $msKey=>$_){
			$msOut.= "soElem.addElement('{$this->csName}_{$msKey}');";
		}
		$msOut.= "soElem=null;"
		."</script>";
		return $msOut;
	}

	/**
	 * Adiciona um Controlador de itens {checkbox ou radiobox}.
	 *
	 * <p>Adiciona um Controlodor de itens da classe FWDItemController
	 * na classe FWDController.</p>
	 * @access public
	 * @param FWDItemController $poItemController Controlador de itens
	 */
	public function addObjFWDItemController(FWDItemController $poItemController){
		$msItemClass = get_class($poItemController);

		if($this->csItensClass=='') $this->csItensClass = $msItemClass;
		if($this->csItensClass==$msItemClass){
			$this->setAttrItems($this->getAttrItems() + 1);
			$this->caContent[$poItemController->getAttrKey()] = $poItemController;
		}else{
			trigger_error('A Controller cannot control itens of different classes.',E_USER_ERROR);
		}

		/*
		 * � necess�rio chamar essa fun��o para funcionar com a utiliza��o do XML_STATIC.
		 */
		$this->rebuildValue();
	}

	/**
	 * Seta o n�mero de itens do Controlador.
	 *
	 * <p>Seta o n�mero de itens do Controlador.</p>
	 * @access public
	 * @param integer $piItems N�mero de itens do Controlador
	 */
	public function setAttrItems($piItems){
		$this->ciItems = $piItems;
	}

	/**
	 * Seta o nome do Label do objeto.
	 *
	 * <p>M�todo para setar o nome do label do objeto, utilizado para a troca de cor, no caso do objeto ter
	 * preenchimento obrigat�rio e n�o estar preenchido.</p>
	 * @access public
	 * @param string $psLabel nome do label associado ao objeto
	 */
	public function setAttrLabel($psLabel){
		$this->csLabel = $psLabel;
	}

	/**
	 * Seta a classe do tipo de itens do controlador.
	 *
	 * <p>Seta a classe do tipo de itens do controlador.</p>
	 * @access public
	 * @param integer $piItems N�mero de itens do Controlador
	 */
	public function setAttrItensClass($psItensClass){
		$this->csItensClass = FWDWebLib::attributeParser($psItensClass,array("checkbox"=>"FWDCheckBox", "radiobox"=>"FWDRadioBox"),"psItensClass");
	}

	/**
	 * Retorna um item, fornecida sua chave.
	 *
	 * <p>Retorna um item, fornecida sua chave.</p>
	 * @access public
	 * @param string $psKey Chave do Item
	 * @return object Item
	 */
	public function getItemByKey($psKey){
		if (isset($this->caContent[$psKey])) {
			return $this->caContent[$psKey];
		} else {
			return null;
		}
	}

	/**
	 * Retorna o n�mero de itens do Controlador.
	 *
	 * <p>Retorna o n�mero de itens do Controlador.</p>
	 * @access public
	 * @return integer N�mero de itens do Controlador
	 */
	public function getAttrItems(){
		return $this->ciItems;
	}

	/**
	 * Retorna um array com os itens selecionados.
	 *
	 * <p>Retorna um array com os itens selecionados.</p>
	 * @access public
	 * @return array Array de itens selecionados (Checked)
	 */
	public function getAllItemsCheck(){
		$maOut = array();
		$msElemCheck = explode(':',$this->getValue());
		foreach($msElemCheck as $msElem){
			if($msElem!='') $maOut[] = $this->getItemByKey($msElem);
		}
		return $maOut;
	}

	/**
	 * Seta o atributo Check de um item.
	 *
	 * <p>Seta o atributo Check (true, false) de um item do controlador.
	 * por este controle.</p>
	 * @access public
	 * @param string $psKey Chave do item que ser� marcado
	 */
	public function checkItem($psKey){
		$moItemController = $this->getItemByKey($psKey);
		if($moItemController instanceof FWDCheckBox){
			$moItemController->setAttrCheck("true");
			parent::setValue(":".$psKey);
			return;
		} elseif($moItemController instanceof FWDRadioBox) {
			$moItemController->setAttrCheck("true");
			parent::setValue(":".$moItemController->getAttrKey());
		} elseif(!($moItemController instanceof FWDCheckBox)){
			$moItemController->setAttrCheck("false");
		}
	}

	/**
	 * Atribui valor � vari�vel value.
	 *
	 * <p>M�todo para atribuir valor � vari�vel value.</p>
	 * @access public
	 * @param mixed $pmValue Valor a ser atribu�do
	 */
	public function setAttrValue($pmValue){
		if(!is_array($pmValue) && !empty($pmValue)){
			$paValue = explode(':',$pmValue);
		}
		elseif (!is_array($pmValue)) { // n�o h� valores a serem adicionados no controller
			return;
		}
		else {
			$paValue = $pmValue;
		}
		foreach($this->caContent as $moItem){
			$moItem->setAttrCheck('false');
		}

		foreach($paValue as $msValue){
			if(isset($this->caContent[$msValue])) {
				$this->checkItem($msValue);
			}
			elseif($this->csItensClass){
				$moItemController = new $this->csItensClass;
				$moItemController->setAttrName($this->getAttrName()."_".$msValue);
				$moItemController->setAttrKey($msValue);
				$moItemController->setAttrCheck(true);
				$moItemController->setAttrController($this->getAttrName());
				$moItemController->execute();
			}
		}
	}
	/**
	 * Atribui valor � vari�vel value.
	 *
	 * <p>M�todo para atribuir valor � vari�vel value.</p>
	 * @access public
	 * @param array $paValue Valor a ser atribu�do
	 */
	public function setValue($paValue, $pbForce = true){
		$this->setAttrValue($paValue);
	}

	/**
	 * Retorna o valor do controlador
	 *
	 * <p>M�todo para retornar o valor do controlador.</p>
	 * @access public
	 * @return string Valor do controlador
	 */
	public function getValue(){
		$msOut = "";
		foreach($this->getContent() as $msItemControllerKey => $moItemController){
			if ($moItemController instanceof FWDCheckBox && $moItemController->getAttrSelectAll())
			continue;
			if($moItemController->getAttrCheck()) $msOut.= ':'.$msItemControllerKey;
		}
		return $msOut;
	}

	/**
	 * Atualiza o valor do controlador
	 *
	 * <p>M�todo para atualizar o valor do controlador.</p>
	 * @access public
	 */
	public function rebuildValue(){
		$this->setValue(FWDWebLib::getPOST($this->getAttrName()));
	}

	/**
	 * Retorna todas as views do container.
	 *
	 * <p>M�todo para retornar todas as views do container.</p>
	 * @access public
	 * @return array Views
	 */
	public function getContent(){
		return $this->caContent;
	}

	/**
	 * Retorna o array de controladores e o pr�prio controlador
	 *
	 * <p>Retorna o array de controladores e o pr�prio controlador.</p>
	 * @access public
	 * @return array Array de controladores e o pr�prio controlador
	 */
	public function collect(){
		$maOut = array($this);
		$maOut = array_merge($maOut,$this->caContent);
		return $maOut;
	}
}
?>