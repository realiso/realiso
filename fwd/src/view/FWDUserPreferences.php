<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDUserPreferences. Armazena as modifica��es feitas pelo usu�rio
 *
 * <p>Armazena as modifica��es feitas pelo usu�rio nos elementos mov�veis e/ou
 * redimension�veis.</p>
 *
 * @package FWD5
 * @subpackage view
 */
class FWDUserPreferences extends FWDButton implements FWDPostValue{

	/**
	 * String que cont�m os ids dos elementos edit�veis pelo usu�rio que devem ser armazenados separados por ':'
	 * @var string
	 * @access protected
	 */
	protected $csSource;

	/**
	 * Vari�vel que ir� conter o valor do elemento
	 * @var FWDVariable
	 * @access protected
	 */
	protected $coVariable;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDUserPreferences.</p>
	 *
	 * @access public
	 * @param FWDBox $poBox Box que define os limites da imagem clic�vel
	 */
	public function __construct($poBox = null){
		parent::__construct($poBox);
		$this->coVariable = new FWDVariable();
	}

	/**
	 * Desenha o elemento.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o elemento.</p>
	 *
	 * @access public
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawBody(){
		$msName = $this->getAttrName();
		$moEvent = new FWDClientEvent('onmousedown',JS_SAVE_USER_PREFS,$msName,$this->getAttrSource());
		$this->addEvent($moEvent);
		$this->setAttrName("button_$msName");
		$this->coVariable->setAttrName($msName);
		return parent::drawBody();
	}

	/**
	 * Desenha o rodap� do objeto.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o rodap� do objeto.</p>
	 *
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do objeto
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	protected function drawFooter($poBox = null){
		return $this->coVariable->draw();
	}

	/**
	 * Define os atributos das boxes dos elementos
	 *
	 * <p>Dada uma string contendo um array serializado contendo os atributos
	 * das boxes de elementos, redefine os atributos das boxes dos elementos
	 * contidos na string.</p>
	 *
	 * @access public
	 * @param string $psValue Array serializado
	 * @param boolean $pbForce Sem efeito
	 */
	public function setValue($psValue, $pbForce = true){
		if(strpos($psValue,":") === FALSE) {
			return false;
		}
		$maElemsAttrs = @FWDWebLib::unserializeString($psValue);
		if($maElemsAttrs!==false){
			$moWebLib = FWDWebLib::getInstance();
			for($i=0;$i<count($maElemsAttrs);$i++){
				$moElem = $moWebLib->getObject($maElemsAttrs[$i]['name']);
				if($moElem instanceof FWDView){
					$moBox = $moElem->getObjFWDBox();
					$moBox->setAttrTop($maElemsAttrs[$i]['top']);
					$moBox->setAttrLeft($maElemsAttrs[$i]['left']);
					$moBox->setAttrHeight($maElemsAttrs[$i]['height']);
					$moBox->setAttrWidth($maElemsAttrs[$i]['width']);
				}
			}
		}
	}

	/**
	 * Atribui valor � vari�vel value.
	 *
	 * <p>Atribui valor � vari�vel value, devendo retirar espa�os em branco
	 * no inicio e/ou no fim da string.</p>
	 * @access public
	 * @param string $psValue Valor a ser atribu�do
	 */
	public function setAttrValue($psValue) {
		$this->setValue($psValue);
	}

	/**
	 * Seta o valor do atributo source
	 *
	 * <p>Seta o valor do atributo source. O atributo source cont�m uma lista de
	 * ids de elementos separados por ':'.</p>
	 *
	 * @access public
	 * @param string $psSource O novo valor do atributo source
	 */
	public function setAttrSource($psSource){
		$this->csSource = $psSource;
	}

	/**
	 * Retorna o valor do atributo source
	 *
	 * <p>Retorna o valor do atributo source. O atributo source cont�m uma lista de
	 * ids de elementos separados por ':'.</p>
	 *
	 * @access public
	 * @return string O valor do atributo source
	 */
	public function getAttrSource(){
		return $this->csSource;
	}
}
?>