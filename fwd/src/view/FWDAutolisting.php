<?
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
class FWDAutolisting extends FWDView implements FWDPostValue{

	protected $left = 0;

	protected $top = 0;

	protected $popup = "";

	protected $label = "";

	protected $value;

	protected $idValue;
	
	protected $display = 'true';
	
	public function setAttrDisplay($display){
		$this->display = $display;
	}

	public function drawHeader($poBox = null) {
		return $this->getVariable()->draw()."";//$this->getIdObject()->draw();
	}

	public function drawBody($poBox = null) {
		$body = "";
		$body .= $this->getText()->draw();
		//$body .= $this->getSearchButton()->draw();
		//$body .= $this->getClearButton()->draw();
		return $body;
	}

	public function drawFooter($poBox){
		return "<script>
		function set".$this->getAttrName()."(id, name){
            gebi('".$this->getAttrName()."Id').value = id;
            gebi('".$this->getAttrName()."').value = name;
        } 
		</script>";
	}

	public function getVariable(){
		$var = new FWDVariable();
		$var->setAttrName($this->getAttrName()."Old");
		$var->setAttrValue("0");
		return $var;
	}

	public function getIdObject(){
		$var = new FWDVariable();
		$var->setAttrName($this->getAttrName()."Id");
		$var->setAttrValue($this->idValue);
		return $var;
	}

	public function setValue($psValue, $pbForce = true){
		$this->value = $psValue;
	}

	public function setIdValue($psValue){
		$this->idValue = $psValue;
	}

	public function setAttrValue($psValue){
		$this->value = $psValue;
	}

	public function getValue(){
		return $this->value;
	}

	public function getIdValue(){
		return FWDWebLib::getPOST($this->getAttrName()."Id");
	}

	public function getText(){
		$box = new FWDBox($this->left, $this->top, 22, 158);

		$event = new FWDServerEvent();
		$event->setAttrEvent("onKeyUp");
		$event->setAttrFunction("search_".$this->getAttrName());

		$text = new FWDText($box);
		$text->setAttrValue($this->value);
		$text->setAttrName($this->getAttrName());
		$text->setAttrMaxLength(255);
		$text->setAttrDisplay($this->display);
		$text->setAttrLabel($this->label);
		$text->addObjFWDEvent($event);

		return $text;
	}
		
	public function setAttrPopup($popup){
		$this->popup = $popup;
	}

	public function getAttrPopup(){
		return $this->popup;
	}

	public function setAttrLabel($label){
		$this->label = $label;
	}

	public function getAttrLabel(){
		return $this->label;
	}

	public function setAttrLeft($piLeft) {
		$this->left = $piLeft;
	}

	public function setAttrTop($piTop) {
		$this->top = $piTop;
	}

	public function getAttrLeft() {
		return $this->left;
	}

	public function getAttrTop() {
		return $this->top;
	}
}

?>