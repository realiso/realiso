<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDItem. Implementa Itens.
 *
 * <p>Classe simples para manipula��o de Itens (utilizados em selects, em
 * radioboxes, etc).</p>
 * @package FWD5
 * @subpackage view
 */
class FWDItem extends FWDDrawing {

	/**
	 * Valor chave do Item
	 * @var string
	 * @access protected
	 */
	protected $csKey = "";
		
	/**
	 * Define se o Item est� marcado (checked) ou n�o
	 * @var boolean
	 * @access protected
	 */
	protected $cbCheck = false;

	/**
	 * Classe CSS
	 * @var string
	 * @access protected
	 */
	protected $csClass = '';

	/**
	 * Valor do elemento
	 * @var FWDString
	 * @access protected
	 */
	protected $coString = null;

	/**
	 * Tag do elemento (permiss�es: define se o elemento deve ser desenhado ou n�o)
	 * @var string
	 * @access protected
	 */
	protected $csTag = "";

	/**
	 * Nome do elemento
	 * @var string
	 * @access protected
	 */
	protected $csName = "";

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDItem.</p>
	 * @access public
	 */
	public function __construct() {
		$this->coString = new FWDString();
	}

	/**
	 * Desenha o cabe�alho do item.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o cabe�alho do item.</p>
	 * @access public
	 *
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawHeader() {
		$msSelected = (($this->getAttrCheck())?"selected":"");
		$msKey = $this->getAttrKey();
		$msName = ($this->csName?$this->csName:'');
		if($msName)
		$msName = "name='".$msName."'";

		if($this->csClass) $msClass = " class='{$this->csClass}'";
		else $msClass = '';
		return "<option $msName value='{$msKey}' {$msSelected}{$msClass}>";
	}

	/**
	 * Desenha o corpo do item.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o corpo do item.</p>
	 * @access public
	 *
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawBody() {
		return $this->getValue();
	}

	/**
	 * Desenha o rodap� do item.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o rodap� do item.</p>
	 * @access public
	 *
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawFooter() {
		return "</option>\n";
	}

	/**
	 * Seta o valor do item.
	 *
	 * <p>M�todo para setar o valor do item.</p>
	 * @access public
	 * @param FWDString $poString Seta o valor do item
	 */
	public function setObjFWDString(FWDString $poString) {
		$this->coString = $poString;
	}

	/**
	 * Seta o valor chave do Item.
	 *
	 * <p>Seta o valor chave do Item.</p>
	 * @access public
	 * @param string $psValue Valor chave do Item
	 */
	public function setAttrKey($psValue) {
		$this->csKey = $psValue;
	}

	/**
	 * Seta o Item como Checked/Not Checked.
	 *
	 * <p>Seta o valor booleano de Item marcado/n�o marcado.</p>
	 * @access public
	 * @param boolean $pbValue Define se o Item est� marcado ou n�o
	 */
	public function setAttrCheck($pbValue) {
		$this->cbCheck = $pbValue;
	}

	/**
	 * Atribui valor, condicionalmente.
	 *
	 * <p>Atribui valor ao item, condicionando atrav�s do segundo par�metro.
	 * Se force for FALSE, concatena o valor se a vari�vel tiver conte�do;
	 * se TRUE atribui mesmo que tenha conte�do (sobrescreve).</p>
	 * @access public
	 * @param string $psValue Valor a ser atribu�do
	 * @param boolean $pbForce For�ar o valor mesmo que j� tenha conte�do
	 */
	public function setValue($psValue, $pbForce = true) {
		$this->coString->setValue($psValue, $pbForce);
	}

	/**
	 * Seta o nome do elemento.
	 *
	 * <p>M�todo para setar o nome do elemento.</p>
	 * @access public
	 * @param string $psName Nome do elemento
	 */
	public function setAttrName($psName) {
		$this->csName = $psName;
	}

	/**
	 * Retorna o nome do elemento.
	 *
	 * <p>M�todo para retornar o nome do elemento.</p>
	 * @access public
	 * @return string Nome do elemento
	 */
	final public function getAttrName() {
		return $this->csName;
	}

	/**
	 * Retorna o valor chave do item.
	 *
	 * <p>Retorna o valor chave do item.</p>
	 * @access public
	 * @return string Valor chave do item
	 */
	public function getAttrKey() {
		return $this->csKey;
	}

	/**
	 * Retorna o valor booleano do atributo Check.
	 *
	 * <p>Retorna o valor booleano TRUE se o item est� marcado. Retorna
	 * FALSE caso contr�rio.</p>
	 * @access public
	 * @return boolean Indica se o Item est� marcado ou n�o
	 */
	public function getAttrCheck() {
		return $this->cbCheck;
	}

	/**
	 * Retorna o valor do item.
	 *
	 * <p>M�todo para retornar o valor do item.</p>
	 * @access public
	 * @return string Valor do item
	 */
	public function getValue() {
	    $this->coString->setAttrNoEscape(true);
		return $this->coString->getAttrString();
	}

	/**
	 * Retorna o objeto de string do elemento.
	 *
	 * <p>M�todo para retornar o objeto de string do elemento.</p>
	 * @access public
	 * @return FWDString String do elemento
	 */
	public function getObjFWDString() {
		return $this->coString;
	}

	/**
	 * Retorna o valor do atributo class
	 *
	 * <p>Retorna o valor do atributo class.</p>
	 * @access public
	 * @return string Valor do atributo class
	 */
	public function getAttrClass(){
		return $this->csClass;
	}

	/**
	 * Seta o valor do atributo class
	 *
	 * <p>Seta o valor do atributo class.</p>
	 * @access public
	 * @param string $psClass Novo valor do atributo
	 */
	public function setAttrClass($psClass){
		$this->csClass = $psClass;
	}

	/**
	 * Seta a tag do elemento.
	 *
	 * <p>M�todo para setar a tag do elemento.</p>
	 * @access public
	 * @param string $psTag Tag do elemento
	 */
	public function setAttrTag($psTag) {
		$this->csTag = $psTag;
	}

	/**
	 * Retorna a tag do elemento.
	 *
	 * <p>M�todo para retornar a tag do elemento.</p>
	 * @access public
	 * @return string Tag do elemento
	 */
	public function getAttrTag() {
		return $this->csTag;
	}

}
?>