<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDBox. Implementa uma box que limita o tamanho dos elementos.
 *
 * <p>Esta classe cont�m informa��es sobre uma box que limita o tamanho
 * dos elementos (margem � esquerda, margem superior, altura e largura).</p>
 * @package FWD5
 * @subpackage view
 */
class FWDBox extends FWDDrawing {

	/**
	 * Posi��o horizontal do canto superior esquerdo da box
	 * @var integerPosi��o horizontal do canto superior esquerdo da box
	 * @access protected
	 */
	protected $ciLeft = 0;

	/**
	 * Posi��o vertical do canto superior esquerdo da box
	 * @var integerPosi��o vertical do canto superior esquerdo da box
	 * @access protected
	 */
	protected $ciTop = 0;

	/**
	 * Altura da box, em pixels
	 * @var integer
	 * @access protected
	 */
	protected $ciHeight = 0;

	/**
	 * Largura da box, em pixels
	 * @var integerLargura da box, em pixels
	 * @access protected
	 */
	protected $ciWidth = 0;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDBox.</p>
	 * @access public
	 * @param integer $piLeft Posi��o horizontal do canto superior esquerdo da box
	 * @param integer $piTop Posi��o vertical do canto superior esquerdo da box
	 * @param integer $piHeight Altura da box
	 * @param integer $piWidth Largura da box
	 */
	public function __construct($piLeft = 0,$piTop = 0,$piHeight = 0,$piWidth = 0) {
		$this->setAttrLeft($piLeft);
		$this->setAttrTop($piTop);
		$this->setAttrHeight($piHeight);
		$this->setAttrWidth($piWidth);
	}

	/**
	 * Retorna o html referente a tag 'box'.
	 *
	 * <p>Retorna o html referente a tag 'box'.</p>
	 * @access public
	 * @return string Html da tag 'box'
	 */
	public function drawBody() {
		$msHeight = (($this->getAttrHeight())?"height:{$this->getAttrHeight()};":"");
		$msWidth = (($this->getAttrWidth())?"width:{$this->getAttrWidth()};":"");
		$msLeft = (($this->getAttrLeft())?"left:{$this->getAttrLeft()};":"");
		$msTop = "top:{$this->getAttrTop()};";
		$msOut = "{$msLeft}{$msTop}{$msHeight}{$msWidth}";
		return $msOut;
	}

	/**
	 * Seta largura da box, em pixels.
	 *
	 * <p>Seta largura da box, em pixels.</p>
	 * @access public
	 * @param integer $piWidth Largura da box, em pixels
	 */
	public function setAttrWidth($piWidth) {
		$this->ciWidth = $piWidth;
	}

	/**
	 * Seta altura da box, em pixels.
	 *
	 * <p>Seta altura da box, em pixels.</p>
	 * @access public
	 * @param integer $piHeight Altura da box, em pixels
	 */
	public function setAttrHeight($piHeight) {
		$this->ciHeight = $piHeight;
	}

	/**
	 * Seta posi��o horizontal do canto superior esquerdo da box.
	 *
	 * <p>Seta posi��o horizontal do canto superior esquerdo da box.</p>
	 * @access public
	 * @param integer $piLeft Posi��o horizontal do canto superior esquerdo da box
	 */
	public function setAttrLeft($piLeft) {
		$this->ciLeft = $piLeft;
	}

	/**
	 * Seta posi��o vertical do canto superior esquerdo da box.
	 *
	 * <p>Seta posi��o vertical do canto superior esquerdo da box.</p>
	 * @access public
	 * @param integer $piTop Posi��o vertical do canto superior esquerdo da box
	 */
	public function setAttrTop($piTop) {
		$this->ciTop = $piTop;
	}

	/**
	 * Retorna a largura da box, em pixels.
	 *
	 * <p>Retorna a largura da box, em pixels.</p>
	 * @access public
	 * @return integer Largura da box, em pixels
	 */
	public function getAttrWidth() {
		return $this->ciWidth;
	}

	/**
	 * Retorna a altura da box, em pixels.
	 *
	 * <p>Retorna a altura da box, em pixels.</p>
	 * @access public
	 * @return integer Altura da box, em pixels
	 */
	public function getAttrHeight() {
		return $this->ciHeight;
	}

	/**
	 * Retorna posi��o horizontal do canto superior esquerdo da box.
	 *
	 * <p>Retorna posi��o horizontal do canto superior esquerdo da box.</p>
	 * @access public
	 * @return integer Posi��o horizontal do canto superior esquerdo da box
	 */
	public function getAttrLeft() {
		return $this->ciLeft;
	}

	/**
	 * Retorna posi��o vertical do canto superior esquerdo da box.
	 *
	 * <p>Retorna posi��o vertical do canto superior esquerdo da box.</p>
	 * @access public
	 * @return integer Posi��o vertical do canto superior esquerdo da box
	 */
	public function getAttrTop() {
		return $this->ciTop;
	}
}
?>