<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2007, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDOptgroup. Implementa um agrupador de itens do select.
 *
 * <p>Classe que implementa um agrupador de itens do select.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDOptgroup extends FWDDrawing {

	/**
	 * Valor do elemento
	 * @var FWDString
	 * @access protected
	 */
	protected $coString = null;

	/**
	 * Array com os item do optgroup
	 * @var array
	 * @access protected
	 */
	protected $caItemKey = array();

	/**
	 * Objeto FWDSelect ao qual o optgroup pertence
	 * @var array
	 * @access protected
	 */
	protected $coSelect = null;

	/**
	 * Indica se o optgroup est� desabilitado
	 * @var array
	 * @access protected
	 */
	protected $cbDisabled = false;

	/**
	 * Tag do elemento (permiss�es: define se o elemento deve ser desenhado ou n�o)
	 * @var string
	 * @access protected
	 */
	protected $csTag = "";

	/**
	 * Nome do elemento
	 *
	 * @var string
	 * @access protected
	 */
	protected $csName = "";

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDItem.</p>
	 * @access public
	 */
	public function __construct() {
		$this->coString = new FWDString();
	}

	/**
	 * Desenha o cabe�alho do optgroup.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o cabe�alho do optgroup.</p>
	 * @access public
	 *
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawHeader() {
		$msDisabled = '';
		if ($this->cbDisabled) $msDisabled = 'disabled';
		return "<optgroup $msDisabled label='{$this->getValue()}' name='{$this->getAttrName()}' id='{$this->getAttrName()}'>\n";
	}

	/**
	 * Desenha o corpo do optgroup.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o corpo do optgroup.</p>
	 * @access public
	 *
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawBody() {
		$msOut = '';
		foreach ($this->caItemKey as $msItemKey => $moItem) {
			$msOut .= $moItem->draw();
		}
		return $msOut;
	}

	/**
	 * Desenha o rodap� do optgroup.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o rodap� do optgroup.</p>
	 * @access public
	 *
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawFooter() {
		return "</optgroup>\n";
	}

	/**
	 * Seta o valor do optgroup.
	 *
	 * <p>M�todo para setar o valor do optgroup.</p>
	 * @access public
	 * @param FWDString $poString Seta o valor do optgroup
	 */
	public function setObjFWDString(FWDString $poString) {
		$this->coString = $poString;
	}

	/**
	 * Seta o select ao qual o optgroup pertence.
	 *
	 * <p>M�todo para setar o select ao qual o optgroup pertence.</p>
	 * @access public
	 * @param FWDSelect $poSelect Obejto do select
	 */
	public function setSelect(FWDSelect $poSelect) {
		$this->coSelect = $poSelect;
	}

	/**
	 * Adiciona um objeto de item.
	 *
	 * <p>Adiciona um objeto de item da classe FWDItem.</p>
	 * @access public
	 * @param FWDItem $poItem Item
	 */
	public function addObjFWDItem(FWDItem $poItem) {
		$this->caItemKey[$poItem->getAttrKey()] = $poItem;
	}

	/**
	 * Habilita/Desabilita o optgroup.
	 *
	 * <p>M�todo para habilitar/desabilitar o optgroup.</p>
	 * @access public
	 * @param boolean $pbValue Define se o optgroup est� habilitado ou n�o
	 */
	public function setAttrDisabled($pbValue) {
		$this->cbDisabled = $pbValue;
	}

	/**
	 * Verifica se o optgroup est� habilitado ou n�o.
	 *
	 * <p>M�todo para verificar se o optgroup est� habilitado ou n�o.</p>
	 * @access public
	 * @return boolean Define se o optgroup est� habilitado ou n�o
	 */
	public function getAttrDisabled() {
		return $this->cbDisabled;
	}

	/**
	 * Atribui valor, condicionalmente.
	 *
	 * <p>Atribui valor ao item, condicionando atrav�s do segundo par�metro.
	 * Se force for FALSE, concatena o valor se a vari�vel tiver conte�do;
	 * se TRUE atribui mesmo que tenha conte�do (sobrescreve).</p>
	 * @access public
	 * @param string $psValue Valor a ser atribu�do
	 * @param boolean $pbForce For�ar o valor mesmo que j� tenha conte�do
	 */
	public function setValue($psValue, $pbForce = true) {
		$this->coString->setValue($psValue, $pbForce);
	}

	/**
	 * Retorna o valor do item.
	 *
	 * <p>M�todo para retornar o valor do item.</p>
	 * @access public
	 * @return string Valor do item
	 */
	public function getValue() {
		return $this->coString->getAttrString();
	}

	/**
	 * Retorna o objeto de string do elemento.
	 *
	 * <p>M�todo para retornar o objeto de string do elemento.</p>
	 * @access public
	 * @return FWDString String do elemento
	 */
	public function getObjFWDString() {
		return $this->coString;
	}

	/**
	 * Retorna array de objetos do optgroup.
	 *
	 * <p>Retorna array de objetos (itens) do optgroup.</p>
	 * @access public
	 * @return array Array de objetos (FWDItem) do optgroup
	 */
	public function getItems() {
		return $this->caItemKey;
	}

	/**
	 * Seta a tag do elemento.
	 *
	 * <p>M�todo para setar a tag do elemento.</p>
	 * @access public
	 * @param string $psTag Tag do elemento
	 */
	public function setAttrTag($psTag) {
		$this->csTag = $psTag;
	}

	/**
	 * Retorna a tag do elemento.
	 *
	 * <p>M�todo para retornar a tag do elemento.</p>
	 * @access public
	 * @return string Tag do elemento
	 */
	public function getAttrTag() {
		return $this->csTag;
	}

	/**
	 * Seta o atributo name.
	 *
	 * <p>M�todo para setar o atributo name.</p>
	 * @access public
	 * @param string $psName Nome do elemento
	 */
	public function setAttrName($psName) {
		$this->csName = $psName;
	}

	/**
	 * Retorna o atributo name.
	 *
	 * <p>M�todo para retornar o atributo name.</p>
	 * @access public
	 * @return string Atributo name do elemento
	 */
	public function getAttrName() {
		return $this->csName;
	}
}
?>