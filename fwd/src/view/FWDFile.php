<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDFile. Implementa campos de upload de arquivos
 *
 * <p>Classe que implementa campos de upload de arquivos (html tag 'input type=file').</p>
 * @package FWD5
 * @subpackage view
 */
class FWDFile extends FWDView {

	/**
	 * C�digo de erro que indica a aus�ncia de erros
	 * @const integer
	 * @access public
	 */
	const E_NONE = 0;

	/**
	 * C�digo de erro que indica que o tamanho do arquivo excede o tamanho m�ximo
	 * @const integer
	 * @access public
	 */
	const E_MAX_SIZE = 1;

	/**
	 * C�digo de erro que indica que a extens�o do arquivo n�o est� entre as
	 * extens�es permitidas
	 * @const integer
	 * @access public
	 */
	const E_EXTENSION = 2;

	/**
	 * C�digo de erro que indica que o upload s� foi feito parcialmente
	 * @const integer
	 * @access public
	 */
	const E_PARTIAL = 4;

	/**
	 * C�digo de erro que indica que n�o existe a pasta tempor�ria
	 * @const integer
	 * @access public
	 */
	const E_NO_TMP_DIR = 8;

	/**
	 * C�digo de erro que indica falha ao tentar escrever no disco
	 * @const integer
	 * @access public
	 */
	const E_CANT_WRITE = 16;

	/**
	 * Define se o campo est� desabilitado ou n�o
	 * @var boolean
	 * @access protected
	 */
	protected $cbDisabled = false;

	/**
	 * Nome do label associado ao objeto
	 * @var string
	 * @access protected
	 */
	protected $csLabel = '';

	/**
	 * Tamanho m�ximo do arquivo em bytes
	 * @var integer
	 * @access protected
	 */
	protected $ciMaxFileSize = 300000000;

	/**
	 * Array de extens�es permitidas
	 * @var array
	 * @access protected
	 */
	protected $caAllowedExts = array();

	/**
	 * C�digo de erro. String de bits, cada bit ligado indica um erro diferente.
	 * @var integer
	 */
	protected $ciErrorCode;

	/**
	 * Nome do arquivo tempor�rio
	 * @var string
	 * @access protected
	 */
	protected $csTempFileName = '';

	/**
	 * Nome do arquivo original
	 * @var string
	 * @access protected
	 */
	protected $csFileName = '';

	/**
	 * Tamanho do arquivo em bytes
	 * @var string
	 * @access protected
	 */
	protected $ciFileSize = '';

	/**
	 * Indica se foi feito o upload de um arquivo
	 * @var boolean
	 * @access protected
	 */
	protected $cbUploaded = false;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDFile.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do campo de texto
	 */
	public function __construct($poBox = null){
		parent::__construct($poBox);
		$this->ciErrorCode = FWDFile::E_NONE;
	}

	/**
	 * Desenha o campo de texto.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML,
	 * todas as informa��es necess�rias para implementar o campo de texto.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do campo de texto
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawBody($poBox = null){
		if($this->getAttrAlign()) $this->alignView($this->getAttrAlign(),$poBox);
		$msStyle = "style='{$this->getStyle()}'";
		$msDisabled = ($this->cbDisabled?'disabled':'');
		$msValue = ($this->getValue()?"value='{$this->getValue()}'":"");
		$msLabel = ($this->csLabel?"label='{$this->csLabel}'":'');
		$msOut = "<input type='hidden' name='MAX_FILE_SIZE' value='{$this->ciMaxFileSize}'/>\n";
		$msOut.= "<input type='file' onKeyDown='this.blur()' onContextMenu='return false;' {$this->getGlobalProperty()} $msStyle $msLabel {$this->getEvents()} {$msDisabled} {$msValue}/>\n";
		$msOut.= '<iframe style="display:none" name="'.$this->csName.'_iframe" id="'.$this->csName.'_iframe"></iframe>';
		return $msOut;
	}

	public function drawFooter($poBox = null){
		return "<script language='javascript'>new FWDFile('{$this->csName}');</script>";
	}

	/**
	 * Atribui valor � vari�vel value.
	 *
	 * <p>Atribui valor � vari�vel value, devendo retirar espa�os em branco
	 * no inicio e/ou no fim da string.</p>
	 * @access public
	 * @param string $psValue Valor a ser atribu�do
	 */
	public function setAttrValue($psValue){
		$this->setValue(trim($psValue));
	}

	/**
	 * Seta o nome do Label do objeto.
	 *
	 * <p>M�todo para setar o nome do label do objeto, utilizado para a troca de cor, no caso do objeto ter
	 * preenchimento obrigat�rio e n�o estar preenchido.</p>
	 * @access public
	 * @param string $psLabel nome do label associado ao objeto
	 */
	public function setAttrLabel($psLabel){
		$this->csLabel = $psLabel;
	}

	/**
	 * Seta o campo como desabilitado.
	 *
	 * <p>M�todo para setar o campo como desabilitado.</p>
	 * @access public
	 * @param boolean $pbDisabled Define se o campo est� desabilitado ou n�o
	 */
	public function setAttrDisabled($pbDisabled){
		$this->cbDisabled = FWDWebLib::attributeParser($pbDisabled,array("true"=>true, "false"=>false),"pbDisabled");
	}

	/**
	 * Seta o tamanho m�ximo do arquivo
	 *
	 * <p>Seta o tamanho m�ximo do arquivo em bytes.</p>
	 * @access public
	 * @param integer $piMaxFileSize Tamanho m�ximo do arquivo
	 */
	public function setAttrMaxFileSize($piMaxFileSize){
		if($piMaxFileSize > 0){
			$this->ciMaxFileSize = $piMaxFileSize;
		}else{
			trigger_error("Invalid maxfilesize value: '{$piMaxFileSize}'",E_USER_WARNING);
		}
	}

	/**
	 * Seta o atributo allowedexts
	 *
	 * <p>Seta o atributo allowedexts, que define as extens�es permitidas.</p>
	 * @access public
	 * @param string $psExtensions Extens�es permitidas separadas por ':'
	 */
	public function setAttrAllowedExts($psExtensions){
		if($psExtensions==''){
			$this->caAllowedExts = array();
		}else{
			$this->caAllowedExts = explode(':',strtolower($psExtensions));
		}
	}

	/**
	 * Testa se a extens�o do arquivo � v�lida
	 *
	 * <p>Testa se a extens�o do arquivo � v�lida e seta o c�digo de erro. Se um
	 * array de extens�es � passado como par�metro, testa se a extens�o pertence a
	 * esse array, sen�o testa se pertence ao array de extens�es previamente
	 * definido.</p>
	 * @access public
	 * @param array $paExtensions Array com as extens�es permitidas (opcional)
	 * @return boolean Indica se a extens�o � v�lida
	 */
	public function testExtension($paExtensions=null){
		if($paExtensions!=null) $this->caAllowedExts = $paExtensions;
		if(count($this->caAllowedExts)==0){ // Permite todas extens�es
			$mbReturn = true;
		}else{
			$mbReturn = in_array(strtolower($this->getFileExt()),$this->caAllowedExts);
		}
		if($mbReturn){
			$this->ciErrorCode&=~FWDFile::E_EXTENSION;
		}else{
			$this->ciErrorCode|= FWDFile::E_EXTENSION;
		}
		return $mbReturn;
	}

	/**
	 * Retorna o valor booleano do atributo Disabled.
	 *
	 * <p>Retorna o valor booleano TRUE se o campo est� desabilitado.
	 * Retorna FALSE caso contr�rio.</p>
	 * @access public
	 * @return boolean Indica se o campo est� desabilitado ou n�o
	 */
	public function getAttrDisabled(){
		return $this->cbDisabled;
	}

	/**
	 * Retorna o tamanho m�ximo do arquivo
	 *
	 * <p>Retorna o tamanho m�ximo do arquivo em bytes.</p>
	 * @access public
	 * @return integer Tamanho m�ximo do arquivo
	 */
	public function getAttrMaxFileSize(){
		return $this->ciMaxFileSize;
	}

	/**
	 * Retorna o nome do arquivo tempor�rio
	 *
	 * <p>Retorna o nome do arquivo tempor�rio.</p>
	 * @access public
	 * @return string Nome do arquivo tempor�rio
	 */
	public function getTempFileName(){
		return $this->csTempFileName;
	}

	/**
	 * Retorna o nome do arquivo original
	 *
	 * <p>Retorna o nome do arquivo original.</p>
	 * @access public
	 * @return string Nome do arquivo original
	 */
	public function getFileName(){
		return $this->csFileName;
	}

	/**
	 * Retorna o tamanho do arquivo
	 *
	 * <p>Retorna o tamanho do arquivo em bytes.</p>
	 * @access public
	 * @return integer Tamanho do arquivo
	 */
	public function getFileSize(){
		return $this->ciFileSize;
	}

	/**
	 * Retorna a extens�o do arquivo
	 *
	 * <p>Retorna a extens�o do arquivo.</p>
	 * @access public
	 * @return string Extens�o do arquivo
	 */
	public function getFileExt(){
		return substr($this->csFileName, strrpos($this->csFileName,'.')+1);
	}

	/**
	 * Indica se foi feito o upload de um arquivo
	 *
	 * <p>Indica se foi feito o upload de um arquivo.</p>
	 * @access public
	 * @return boolean True indica que um arquivo foi enviado com sucesso
	 */
	public function isUploaded(){
		return $this->cbUploaded;
	}

	/**
	 * Copia o arquivo
	 *
	 * <p>Copia o arquivo tempor�rio para o local indicado com o nome indicado.</p>
	 * @access public
	 * @param string $psPath Caminho do diret�rio destino
	 * @param string $psName Nome do arquivo
	 * @return boolean True, indica sucesso da opera��o
	 */
	public function copyTo($psPath,$psName){
		$msFullPath = "$psPath/$psName";
		if($this->cbUploaded && $this->ciErrorCode==FWDFile::E_NONE){
			if(move_uploaded_file($this->csTempFileName,$msFullPath)){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	/**
	 * Retorna o c�digo de erro
	 *
	 * <p>Retorna o c�digo de erro.</p>
	 * @access public
	 * @return integer C�digo de erro
	 */
	public function getErrorCode(){
		return $this->ciErrorCode;
	}

	/**
	 * Se foi feito o upload de um arquivo, pega suas informa��es
	 *
	 * <p>Se foi feito o upload de um arquivo, pega suas informa��es. M�todo
	 * executado quando o objeto acaba de ser carregado.</p>
	 * @access public
	 */
	public function execute(){
		if(isset($_FILES[$this->getAttrName()]) && $_FILES[$this->getAttrName()]['error']!=UPLOAD_ERR_NO_FILE){
			$this->cbUploaded = true;
			$this->ciErrorCode = FWDFile::E_NONE;
			$maFileInfo = $_FILES[$this->getAttrName()];
			$this->csFileName = $maFileInfo['name'];
			$this->csTempFileName = $maFileInfo['tmp_name'];
			$this->ciFileSize = $maFileInfo['size'];
			$this->testExtension();
			if($this->ciFileSize > $this->ciMaxFileSize || $maFileInfo['error']==UPLOAD_ERR_INI_SIZE || $maFileInfo['error']==UPLOAD_ERR_FORM_SIZE){
				$this->ciErrorCode|= FWDFile::E_MAX_SIZE;
			}
			switch($maFileInfo['error']){
				case UPLOAD_ERR_PARTIAL   : $this->ciErrorCode|= FWDFile::E_PARTIAL;    break;
				case UPLOAD_ERR_NO_TMP_DIR: $this->ciErrorCode|= FWDFile::E_NO_TMP_DIR; break;
				case UPLOAD_ERR_CANT_WRITE: $this->ciErrorCode|= FWDFile::E_CANT_WRITE; break;
			}
		}else{
			$this->cbUploaded = false;
		}
	}

}

?>