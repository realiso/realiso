<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDRadioBox. Implementa Radio Boxes.
 *
 * <p>Classe que implementa Radio Boxes (imagens clic�veis com atributos
 * especiais e suporte a eventos). HTML tag 'input type="radio"'.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDRadioBox extends FWDItemController {

	private $cbDisabled = false;
	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDRadioBox.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites da RadioBox
	 */
	public function __construct($poBox = null){
		parent::__construct($poBox);
	}

	/**
	 * Desenha o cabe�alho do radiobox.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o cabe�alho do radiobox.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do radiobox
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawHeader($poBox = null){
		$moWebLib = FWDWebLib::getInstance();
		$msGFX = $moWebLib->getGfxRef();
		$msController = $this->getAttrController();
		$moController = FWDWebLib::getObject($msController);
		$msName = "{$msController}_{$this->getAttrKey()}";
		$msSrc = ($this->getAttrCheck()?$msGFX."radio.gif":$msGFX."uncheck.gif");
		$msJs = "gfx_radio(\"$msController\",\"{$this->getAttrKey()}\");";
		$msEventCode = '';
		$msCursor = '';
		if($this->cbDisabled==false){
			$moEvent = new FWDClientEvent();
			$moEvent->setAttrEvent("OnClick");
			$moEvent->setAttrValue($msJs);
			$this->addObjFWDEvent($moEvent);
			$msEventCode = $this->getEvents();
			$msCursor.= 'cursor:pointer;';
		}
		$msStyle = "{$this->getStyle()};{$msCursor}";
		return  "\n<img name='{$msName}' id='$msName' src='$msSrc' style='$msStyle' {$msEventCode}/>\n";
	}

	/**
	 * Desenha o rodap� do radiobox.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o rodap� do radiobox.</p>
	 * @access public
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawFooter(){
		$msName = "{$this->csController}_{$this->csKey}";
		$msDisabled = '';
		if($this->cbDisabled){
			$msDisabled.="gobi('{$msName}').disable();";
		}
		if($this->csName){
			return "<script language='javascript'>"
			."gebi('{$msName}').object = new FWDRadioBox('{$msName}','{$this->csController}','{$this->csKey}');"
			.$msDisabled
			."</script>";
		}else{
			return '';
		}
	}


	/**
	 * Retorna se o radiobox � readOnly.
	 *
	 * <p>Retorna se o radiobox � readOnly.</p>
	 * @access public
	 * @return boolean Retorna se o radiobox � readOnly.
	 */
	public function getAttrDisabled(){
		return $this->cbDisabled;
	}

	/**
	 * Define se o radio � readOnly.
	 *
	 * <p>Metodo que define se um radio � readOnly.</p>
	 * @access public
	 * @param boolean $pbReadOnly "true" ou "false"
	 */
	public function setAttrDisabled($pbDisable){
		$this->cbDisabled = FWDWebLib::attributeParser($pbDisable,array("true"=>true, "false"=>false),"pbDisable");
	}
}
?>