<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDCalendar. Implementa calend�rios customiz�vies em javascript.
 *
 * <p>Utiliza bibliotecas javascript para acessar o calend�rio. Atributos
 * customiz�veis atrav�s da passagem de par�metros para o arquivo
 * calendar-setup.js. Estilo customiz�vel atrav�s de arquivos .css.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDCalendar extends FWDDrawing implements FWDPostValue,FWDCollectable {

	/**
	 * Define se o calend�rio � ou n�o o primeiro que aparece na p�gina (necess�rio para carregar os arquivos javascript somente uma vez)
	 * @var boolean
	 * @access private
	 */
	private static $cbFirstCalendar = true;

	/**
	 * Define se o input onde a data � inserida � vis�vel ou n�o
	 * @var boolean
	 * @access private
	 */
	private $cbHidden = false;

	/**
	 * Nome do calend�rio ( 'name' do input e 'id' do img scr)
	 * @var string
	 * @access private
	 */
	private $csName = "";

	/**
	 * Define o valor do calend�rio atribu�do pelo usu�rio (POST) ou pelo programador (XML)
	 * @var string
	 * @access private
	 */
	private $csValue = "";

	/**
	 * Define se os dias s�o selecionados com um ou dois cliques
	 * @var string
	 * @access private
	 */
	private $csSingleClick = "false";

	/**
	 * Define o estilo (arquivo .css) do calend�rio (ex.: "blue" ir� carregar o arquivo "calendar-blue.css")
	 * @var string
	 * @access private
	 */
	private $csStyle = "win2k-cold-1";

	/**
	 * Define se a coluna com os n�meros das semanas deve ser vis�vel ou n�o
	 * @var string
	 * @access private
	 */
	private $csWeekNumbers = "false";

	/**
	 * Objeto EventHandler de eventos a executar (trigger = OnUpdate)
	 * @var FWDEventHandler
	 * @access private
	 */
	private $coEvent = null;
		
	/**
	 * Componente vertical do alinhamento do calendario
	 * @var string
	 * @access private
	 */
	private $csValign = "B";

	/**
	 * Componente horizontal do alinhamento do calendario
	 * @var string
	 * @access private
	 */
	private $csHalign = "l";

	/**
	 * Define se o trigger button � exibido antes ou depois do input field
	 * @var boolean
	 * @access private
	 */
	private $cbBeforeInput = false;

	/**
	 * Nome do label associado ao objeto
	 * @var string
	 * @access private
	 */
	private $csLabel = "";

	/**
	 * Define se o campo est� desabilitado ou n�o
	 * @var boolean
	 * @access protected
	 */
	protected $cbDisabled = false;

	/**
	 * Define se o timestamp retorna a data com hor�rio 00:00(false) ou 23:59(true)
	 * @var boolean
	 * @access private
	 */
	private $cbEndDate = false;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDCalendar.</p>
	 * @access public
	 */
	public function __construct(){
	}

	/**
	 * Desenha o cabe�alho do calend�rio.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML/javascript, todas
	 * as informa��es necess�rias para implementar o cabe�alho do calend�rio.</p>
	 * @access public
	 * @param FWDBox $poFWDBox Box que define os limites do calend�rio
	 * @return string C�digo HTML/javascript que ser� inserido na p�gina
	 */
	public function drawHeader($poFWDBox = null) {
		$moWebLib = FWDWebLib::getInstance();
		$msGFXRef = $moWebLib->getGfxRef();

		$msOut = "";

		$msType = ($this->cbHidden ? "hidden":"text");

		$msLabel = $this->csLabel ? "label='{$this->csLabel}'":"";
		$msClass = get_class($this);

		/*
		 * Monta o evento que vai ser disparado quando ocorrer o evento de Onclick
		 * no campo text associado ao calend�rio.
		 */
		$msEvent = "if (gebi(\"{$this->csName}\").value != \"\") {gebi(\"{$this->csName}\").value = \"\";{$this->getEvents()};}";

		$msTriggerButtonStyle = "cursor: pointer;";
		$msFakeButtonStyle = (FWDWebLib::browserIsIE()?'margin-left:-4;':''); // Contorna bug do IE que deixa a imagem deslocada

		if($this->cbDisabled){
			$msTriggerButtonStyle.= "display:none;";
			$msInputFieldDisabled = " disabled";
		}else{
			$msFakeButtonStyle.= "display:none;";
			$msInputFieldDisabled = "";
		}

		$msTriggerButton = "<img src='{$msGFXRef}icon-calendar.gif' id='".$this->csName."_trigger_button"."' style='{$msTriggerButtonStyle}' />\n"
		."<img src='{$msGFXRef}icon-calendar.gif' id='".$this->csName."_fake_button"."' style='{$msFakeButtonStyle}' />\n";
		$msInputField = "<input class='{$msClass} type='{$msType}' value='".$this->csValue."' id='".$this->csName."' name='".$this->csName."' {$msLabel} readonly='1' size='8' style='height:20px' onclick='{$msEvent}'{$msInputFieldDisabled}/>\n";

		$msOut .= $this->cbBeforeInput ? $msTriggerButton.$msInputField : $msInputField.$msTriggerButton;

		return $msOut;
	}

	/**
	 * Desenha o corpo do calend�rio.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML/javascript, todas
	 * as informa��es necess�rias para implementar o corpo do calend�rio.</p>
	 * @access public
	 * @param FWDBox $poFWDBox Box que define os limites do calend�rio
	 * @return string C�digo HTML/javascript que ser� inserido na p�gina
	 */
	public function drawBody($poFWDBox = null) {
		$moWebLib = FWDWebLib::getInstance();
		$msCSSRef = $moWebLib->getCSSRef();
		$msJsRef = $moWebLib->getJsRef();
		$msOut = '';
		if(self::$cbFirstCalendar){
			$msOut .= "<link rel='stylesheet' type='text/css' media='all' href='{$msCSSRef}css/nonauto/calendar-" . $this->csStyle . ".css' title='{$this->csStyle}' />\n";
			$msOut .= "<script type='text/javascript' src='{$msJsRef}calendar.js'></script>\n";
			$msOut .= "<script type='text/javascript' src='{$msJsRef}calendar-setup.js'></script>\n";
			$msOut .= "<script type='text/javascript' src='{$msJsRef}calendar-" . FWDLanguage::getPHPStringValue('locale', "pt_br") . ".js'></script>\n";
			self::$cbFirstCalendar = false;
		}else{
		}
		return $msOut;
	}

	/**
	 * Desenha o rodap� do calend�rio.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML/javascript, todas
	 * as informa��es necess�rias para implementar o rodap� do calend�rio.</p>
	 * @access public
	 * @param FWDBox $poFWDBox Box que define os limites do calend�rio
	 * @return string C�digo HTML/javascript que ser� inserido na p�gina
	 */
	public function drawFooter($poFWDBox = null) {
		$msOut = "";
		$msOut .= "<script type='text/javascript'>\n";
		$msOut .= "  gebi('{$this->csName}').object = new FWDCalendar('{$this->csName}');\n";
		$msOut .= "  function func_OnUpdate_{$this->csName}() { {$this->getEvents()} }\n";
		$msOut .= "  Calendar.setup({\n";
		$msOut .= "    inputField     :    '" . $this->csName . "',\n";
		$msOut .= "    ifFormat       :    '" . FWDLanguage::getPHPStringValue('mx_shortdate_format','') . "',\n";
		$msOut .= "    button         :    '" . $this->csName . "_trigger_button"."',\n";
		$msOut .= "    align          :    '" . $this->csValign . $this->csHalign . "',\n";
		$msOut .= "    singleClick    :    " . $this->csSingleClick . ",\n";
		$msOut .= "    weekNumbers    :    " . $this->csWeekNumbers . ",\n";
		$msOut .= "    onUpdate       :    func_OnUpdate_" . $this->csName . "\n";
		$msOut .= "  });\n";
		$msOut .= "</script>\n";

		return $msOut;
	}

	/**
	 * Seta o nome do Label do objeto.
	 *
	 * <p>M�todo para setar o nome do label do objeto, utilizado para a troca de cor, no caso do objeto ter
	 * preenchimento obrigat�rio e n�o estar preenchido.</p>
	 * @access public
	 * @param string $psLabel nome do label associado ao objeto
	 */
	public function setAttrLabel($psLabel) {
		$this->csLabel = $psLabel;
	}

	/**
	 * Define se o campo 'input' deve ser vis�vel ou n�o.
	 *
	 * <p>Para o campo 'input' (no qual � inserida a data selecionada no
	 * calend�rio) ser invis�vel, o atributo cbHidden deve conter o valor
	 * booleano TRUE. Para o ser vis�vel, o atributo cdHidden deve conter
	 * o valor booleano FALSE.</p>
	 * @access public
	 * @param string $pbHidden Flag para exibir/esconder o campo 'input'
	 */
	public function setAttrHidden($pbHidden) {
		$this->cbHidden = FWDWebLib::attributeParser($pbHidden,array("true"=>true, "false"=>false),"cbHidden");
	}

	/**
	 * Seta o nome do calend�rio.
	 *
	 * <p>A string contida no atributo csName � utilizada diretamente como
	 * 'name' do input, e concatenada ao sufixo '_target_button' como 'id'
	 * do img scr.</p>
	 * @access public
	 * @param string $psName Nome que identifica o calend�rio
	 */
	public function setAttrName($psName) {
		$this->csName = $psName;
	}

	/**
	 * Atribui valor � vari�vel value.
	 *
	 * <p>Atribui valor � vari�vel value retirando espa�os em branco no
	 * inicio e/ou no fim da string.</p>
	 * @access public
	 * @param string $psValue Valor a ser atribu�do
	 */
	public function setAttrValue($psValue) {
		$this->csValue = trim($psValue);
	}

	/**
	 * Define se os dias devem ser selecionados com um ou dois cliques.
	 *
	 * <p>Para selecionar os dias com um �nico clique, o atributo csSingleClick
	 * deve conter a string "true". Para selecionar com dois cliques,
	 * csSingleClick deve conter a string "false".</p>
	 * @access public
	 * @param string $psSingleClick Flag para ativar/desativar a sele��o dos dias com um �nico clique
	 */
	public function setAttrSingleClick($psSingleClick) {
		$this->csSingleClick = FWDWebLib::attributeParser($psSingleClick,array("true"=>"true", "false"=>"false"),"csSingleClick");
	}

	/**
	 * Seta o nome do arquivo de estilo a ser utilizado.
	 *
	 * <p>O atributo csStyle deve conter apenas o n�cleo do nome do arquivo .css.
	 * (i.e. ignorar o prefixo 'calendar-' e o sufixo '.css').
	 * Por exemplo: para carregar o arquivo 'calendar-blue.css', o atributo
	 * csStyle dever� conter a string "blue" .</p>
	 * @access public
	 * @param string $psStyle N�cleo do nome do arquivo .css
	 */
	public function setAttrStyle($psStyle) {
		$this->csStyle = FWDWebLib::attributeParser($psStyle,array("blue","blue2","brown","green","system","win2k-1","win2k-2","win2k-cold-1","win2k-cold-2"),"csStyle");
	}

	/**
	 * Define se a coluna referente aos n�meros das semanas deve ser vis�vel ou n�o.
	 *
	 * <p>Para a coluna referente aos n�meros das semanas ser vis�vel, o
	 * atributo csWeekNumbers deve conter a string "true". Para ser invis�vel,
	 * deve conter a string "false".</p>
	 * @access public
	 * @param string $psWeekNumbers Flag para ativar/desativar a coluna dos n�meros das semanas
	 */
	public function setAttrWeekNumbers($psWeekNumbers) {
		$this->csWeekNumbers = FWDWebLib::attributeParser($psWeekNumbers,array("true"=>"true", "false"=>"false"),"csWeekNumbers");
	}

	/**
	 * Define a componente vertical do alinhamento do calendario.
	 *
	 * <p>Utiliza o attributeParser para verificar a consist�ncia da entrada e
	 * ja realizar a convers�o para o formato utilizado internamente pelo calendario:
	 *  T -- completamente acima do elemento de refer�ncia (margem inferior do calendario alinhada � margem superior do elemento).
	 *	t -- acima do elemento mas pode sobrescrev�-lo (margem inferior do calendario alinhada � margem inferior do elemento).
	 *	C -- verticalmente centralizado em rela��o ao elemento de refer�ncia. Pode sobrescrev�-lo (depende do alinhamento horizontal).
	 *	b -- abaixo do elemento mas pode sobrescrev�-lo (margem superior do calendario alinhada � margem superior do elemento).
	 *	B -- completamente abaixo do elemento de refer�ncia (margem superior do calendario alinhada � margem inferior do elemento).</p>
	 * @access public
	 * @param string $psValign Componente vertical do alinhamento do calendario
	 */
	public function setAttrValign($psValign) {
		$this->csValign = FWDWebLib::attributeParser($psValign,array("top"=>"T", "center"=>"C", "bottom"=>"B"),"csValign");
	}

	/**
	 * Define a componente horizontal do alinhamento do calendario.
	 *
	 * <p>Utiliza o attributeParser para verificar a consist�ncia da entrada e
	 * ja realizar a convers�o para o formato utilizado internamente pelo calendario:
	 *	L -- completamente � esquerda do elemento de refer�ncia (margem direita do calendario alinhada � margem esquerda do elemento).
	 *	l -- � esquerda do elemento mas pode sobrescrev�-lo (margem esquerda do calendario alinhada � margem esquerda do elemento).
	 *	C -- horizontalmente centralizado em rela��o ao elemento de refer�ncia. Pode sobrescrev�-lo, dependendo do alinhamento vertical.
	 *	r -- � direita do elemento mas pode sobrescrev�-lo (margem direita do calendario alinhada � margem direita do elemento).
	 *	R -- completamente � direita do elemento de refer�ncia (margem esquerda do calendario alinhada � margem direita do elemento).</p>
	 * @access public
	 * @param string $psHalign Componente horizontal do alinhamento do calendario
	 */
	public function setAttrHalign($psHalign) {
		$this->csHalign = FWDWebLib::attributeParser($psHalign,array("left"=>"l", "center"=>"C", "right"=>"r"),"csHalign");
	}

	/**
	 * Define se o trigger button do calendario � exibido antes ou depois do input field.
	 *
	 * <p>Para o trigger button ser exibido antes (� esquerda) do input field,
	 * o atributo cbBeforeInput deve conter o valor booleano 'true'. Para ser
	 * exibido depois (� direita), o atributo cbBeforeInput deve conter o valor
	 * booleano 'false'.</p>
	 * @access public
	 * @param boolean $pbBeforeInput Define se o trigger button deve ser exibido antes ou depois do input field
	 */
	public function setAttrBeforeInput($pbBeforeInput) {
		$this->cbBeforeInput = FWDWebLib::attributeParser($pbBeforeInput,array("true"=>true, "false"=>false),"cbBeforeInput");
	}

	/**
	 * Seta o evento de OnUpdate, se existir.
	 *
	 * <p>Seta o evento de OnUpdate. Antes � realizada uma verifia��o
	 * de consist�ncia atrav�s da chamada do attributeParser.</p>
	 * @access public
	 * @param object $poEvent Objeto de evento
	 * @param string $psEvent Alvo do evento
	 */
	public function addEvent($poEvent,$psEvent) {
		if(!isset($this->coEvent)) {
			$this->coEvent = new FWDEventHandler("onupdate",$this->getAttrName());
		}
		FWDWebLib::attributeParser(strtolower($this->coEvent->getAttrEvent()),array("onupdate"),"psEvent");
		$this->coEvent->setAttrContent($poEvent);
	}

	/**
	 * Adiciona um objeto de evento (OnUpdate).
	 *
	 * <p>Adiciona um objeto de evento (OnUpdate) da classe FWDEvent
	 * na classe FWDCalendar.</p>
	 * @access public
	 * @param object $poEvent Objeto de evento
	 * @param string $psEvent Alvo do evento
	 */
	public function addObjFWDEvent($poEvent,$psEvent) {
		$this->addEvent($poEvent,$psEvent);
	}

	/**
	 * Retorna a string contendo os eventos adicionados no calend�rio.
	 *
	 * <p>Retorna a string contendo os eventos adicionados no calend�rio.</p>
	 * @access public
	 * @return string Eventos
	 */
	public function getEvents() {
		$msEvent = "";
		if(isset($this->coEvent)) {
			$maEvent = $this->coEvent->getAttrContent();
			foreach($maEvent as $moEvent) {
				$moEvent->setAttrSource("gebi('" . $this->csName . "')");
				$msEvent .= " " . $moEvent->render();
			}
		}
		else {
			$msEvent = "return 0;";
		}
		return $msEvent;
	}

	/**
	 * Atribui valor, condicionalmente, � vari�vel value.
	 *
	 * <p>Atribui valor � vari�vel value, condicionando atrav�s do segundo par�metro.
	 * Se pbForce for FALSE, n�o atribui o valor se a vari�vel tiver conte�do;
	 * se TRUE atribui mesmo que tenha conte�do.</p>
	 * @access public
	 * @param string $psValue Valor a ser atribu�do
	 * @param boolean $pbForce For�ar o valor mesmo que j� tenha conte�do
	 */
	public function setValue($psValue, $pbForce = true) {
		if (!isset($this->csValue) || !$this->csValue || $pbForce ){
			$this->setAttrValue($psValue);
		}
	}

	/**
	 * Retorna o valor da vari�vel value.
	 *
	 * <p>Retorna o valor da vari�vel value.</p>
	 * @access public
	 * @return string Data contida no objeto de calend�rio
	 */
	public function getValue() {
		return $this->csValue;
	}

	/**
	 * Seta a data baseado em um timestamp.
	 *
	 * <p>M�todo para setar a data baseado em um timestamp.</p>
	 * @access public
	 * @param integer $piTimestamp Timestamp
	 */
	public function setTimestamp($piTimestamp){
		$maDate = getdate($piTimestamp);
		$msDateFormat = FWDLanguage::getPHPStringValue('mx_shortdate_format','');
		if($msDateFormat == "%m/%d/%Y"){
			$msDay = $maDate['mon'] . "/" . $maDate['mday'] . "/" . $maDate['year'];
		}else{
			$msDay = $maDate['mday'] . "/" . $maDate['mon'] . "/" . $maDate['year'];
		}
		$this->setValue($msDay);
	}

	/**
	 * Retorna o timestamp da data.
	 *
	 * <p>M�todo para retornar o timestamp da data.</p>
	 * @access public
	 * @return integer Timestamp
	 */
	public function getTimestamp() {
		$miTimestamp = 0;
		if ($this->csValue) {
			$maDate = explode("/", $this->csValue);
			$msDateFormat = FWDLanguage::getPHPStringValue('mx_shortdate_format','');
			if ($msDateFormat == "%m/%d/%Y")
			$miTimestamp = mktime(0, 0, 0, $maDate[0], $maDate[1], $maDate[2]);
			else if ($msDateFormat == "%d/%m/%Y")
			$miTimestamp = mktime(0, 0, 0, $maDate[1], $maDate[0], $maDate[2]);
			else
			trigger_error("Invalid date format!", E_USER_WARNING);
		}
		if ($miTimestamp && $this->getAttrEndDate()) {
			$miTimestamp = $miTimestamp + 86399;
		}
		return $miTimestamp;
	}

	/**
	 * Retorna o nome atribuido ao calend�rio.
	 *
	 * <p>Retorna o nome atribuido ao calend�rio.</p>
	 * @access public
	 * @return string Nome do objeto de calend�rio
	 */
	public function getAttrName() {
		return $this->csName;
	}

	/**
	 * Retorna o dia atribuido ao calend�rio.
	 *
	 * <p>Retorna o dia atribuido ao calend�rio.</p>
	 * @access public
	 * @return integer Dia do objeto de calend�rio
	 */
	public function getDay() {
		$maDate = getdate($this->getTimestamp());
		return $maDate["mday"];
	}

	/**
	 * Retorna o m�s atribuido ao calend�rio.
	 *
	 * <p>Retorna o m�s atribuido ao calend�rio.</p>
	 * @access public
	 * @return integer M�s do objeto de calend�rio
	 */
	public function getMonth() {
		$maDate = getdate($this->getTimestamp());
		return $maDate["mon"];
	}

	/**
	 * Retorna o ano atribuido ao calend�rio.
	 *
	 * <p>Retorna o ano atribuido ao calend�rio.</p>
	 * @access public
	 * @return integer Ano do objeto de calend�rio
	 */
	public function getYear() {
		$maDate = getdate($this->getTimestamp());
		return $maDate["year"];
	}

	/**
	 * Retorna o array de eventos do calendario e o pr�prio calend�rio.
	 *
	 * <p>Retorna o array de eventos do calendario e o pr�prio calend�rio.</p>
	 * @access public
	 * @return array Eventos do calend�rio e o pr�prio calend�rio
	 */
	public function collect() {
		$maOut = array($this);
		if (isset($this->coEvent))
		$maOut = array_merge($maOut,$this->coEvent->getAttrContent());
		return $maOut;
	}

	/**
	 * Seta o campo como desabilitado.
	 *
	 * <p>M�todo para setar o campo como desabilitado.</p>
	 * @access public
	 * @param boolean $pbDisabled Define se o campo est� desabilitado ou n�o
	 */
	public function setAttrDisabled($pbDisabled){
		$this->cbDisabled = FWDWebLib::attributeParser($pbDisabled,array('true'=>true, 'false'=>false), 'disable');
	}

	/**
	 * Retorna o valor do atributo enddate
	 *
	 * <p>Retorna o valor do atributo enddate.</p>
	 * @access public
	 * @return boolean Valor do atributo enddate
	 */
	public function getAttrEndDate(){
		return $this->cbEndDate;
	}

	/**
	 * Seta o valor do atributo enddate
	 *
	 * <p>Seta o valor do atributo enddate.</p>
	 * @access public
	 * @param string $psEndDate Novo valor do atributo
	 */
	public function setAttrEndDate($psEndDate){
		$this->cbEndDate = FWDWebLib::attributeParser($psEndDate,array("true"=>true, "false"=>false),"psEndDate");
	}

}
?>