<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDMessageBox. Implementa Message Boxes.
 *
 * <p>Classe que implementa Message Boxes.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDMessageBox extends FWDContainer {

	/**
	 * T�tulo da Message Box (texto est�tico)
	 * @var FWDStatic
	 * @access protected
	 */
	protected $coTitle = null;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDMessageBox.</p>
	 * @access public
	 * @param FWDPanel $pobox Box que define os limites da Message Box
	 */
	public function __construct($poBox = null) {
		parent::__construct($poBox);
	}

	/**
	 * Desenha a Message Box.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML,
	 * todas as informa��es necess�rias para implementar a Message Box.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites da messagebox
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawBody($poBox = null) {
		$msOut = "";

		if ($this->csDebug!="")
		$msOut .= $this->debugView();

		$moPanel = new FWDPanel($this->getObjFWDBox());
		$moPanel->setAttrDisplay("false");
		$moPanel->setAttrName($this->getAttrName());

		$moTitle = $this->getAttrTitle();
		$moPanel->addObjFWDView($moTitle,"title");

		$msMessage = $this->getMessage($this->getValue());
		$moPanel->addObjFWDView($msMessage);

		foreach ($this->caContent as $moButton) {
			$moPanel->addObjFWDView($moButton);
		}

		$msOut .= $moPanel->draw();

		return $msOut;
	}

	/**
	 * Adiciona um button.
	 *
	 * <p>Adiciona um button (imagem clic�vel) da classe FWDButton na
	 * classe FWDMessageBox.</p>
	 * @access public
	 * @param FWDButton $poButton Button
	 */
	public function addObjFWDButton($poButton) {
		$this->caContent[] = $poButton;
	}

	/**
	 * Cria e seta o T�tulo da Message Box.
	 *
	 * <p>Cria e seta o T�tulo da Message Box. Instancia um objeto da classe
	 * Static, seta o nome e o valor (string) do t�tulo.</p>
	 * @access public
	 * @param string $psValue Texto a ser inserido no T�tulo da MessageBox.
	 */
	public function setAttrTitle($psValue) {
		$moTitle = new FWDStatic(new FWDBox(50,100,100,100));
		$moTitle->setAttrName("title_".$this->getAttrName());
		$moTitle->setAttrValue($psValue);
		$this->coTitle = $moTitle;
	}

	/**
	 * Retorna o objeto de T�tulo.
	 *
	 * <p>Retorna o objeto de T�tulo (inst�ncia da classe Static) da
	 * Message Box.</p>
	 * @access public
	 * @return FWDStatic T�tulo da Message Box (texto est�tico)
	 */
	public function getAttrTitle() {
		return $this->coTitle;
	}

	/**
	 * Retorna o objeto de Mensagem.
	 *
	 * <p>Retorna o objeto de Mensagem (inst�ncia da classe Static) da
	 * Message Box.</p>
	 * @access public
	 * @param string $psValue Texto ser inserido na Message Box
	 * @return FWDStatic Mensagem da Message Box (texto est�tico)
	 */
	public function getMessage($psValue) {
		$moMessage = new FWDStatic(new FWDBox(50,50,100,100));
		$moMessage->setAttrName("message_".$this->getAttrName());
		$moMessage->setAttrValue($psValue);
		return $moMessage;
	}
}
?>