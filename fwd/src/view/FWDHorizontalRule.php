<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDHorizontalRule. Implementa uma linha horizontal (tag 'hr').
 *
 * <p>Classe que implementa uma linha horizontal (tag 'hr').</p>
 * @package FWD5
 * @subpackage view
 */
class FWDHorizontalRule extends FWDDrawing {

	/**
	 * Box do elemento
	 * @var FWDBox
	 * @access protected
	 */
	protected $coBox = null;

	/**
	 * Classe CSS
	 * @var string
	 * @access protected
	 */
	protected $csClass = "";

	/**
	 * Nome do elemento (utilizada como 'name' e 'id')
	 * @var string
	 * @access protected
	 */
	protected $csName = "";

	/**
	 * Par�metros extra para o atributo 'style'
	 * @var string
	 * @access protected
	 */
	protected $csExtraStyle = "";

	/**
	 * Atributo extra da div (pode conter mais de um atributo, separando por espa�os)
	 * @var string
	 * @access protected
	 */
	protected $csExtraAttribute = "";

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDHorizontalRule.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do elemento
	 */
	public function __construct($poBox = null){
		$this->coBox = $poBox;
	}

	/**
	 * Desenha o cabe�alho do elemento.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o cabe�alho do elemento.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do elemento
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	protected function drawHeader($poBox = null){
		return "<hr ";
	}

	/**
	 * Desenha o corpo do elemento.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o corpo do elemento.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do elemento
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	protected function drawBody($poBox = null){
		$msGlobalProperty = "name='{$this->csName}' id='{$this->csName}' class='{$this->csClass}'";
		$msStyle = "left:{$this->coBox->getAttrLeft()};";
		$miTop = (FWDWebLib::BrowserIsIE()? $this->coBox->getAttrTop()+6 : $this->coBox->getAttrTop());
		$msStyle.= "top:{$miTop};";
		$msStyle.= "height:{$this->coBox->getAttrHeight()};";
		$msStyle.= "width:{$this->coBox->getAttrWidth()};";
		$msStyle.= "position:absolute;";
		$msStyle.= $this->csExtraStyle;
		return "{$msGlobalProperty} style='{$msStyle}' {$this->csExtraAttribute}";
	}

	/**
	 * Desenha o rodap� do elemento.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o rodap� do elemento.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do elemento
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	protected function drawFooter($poBox = null){
		return "/>";
	}

	/**
	 * Seta a box do elemento.
	 *
	 * <p>Seta a box do elemento.</p>
	 * @access public
	 * @param FWDBox $poBox Box do elemento
	 */
	public function setObjFWDBox($poBox){
		$this->coBox = $poBox;
	}

	/**
	 * Seta a classe CSS do elemento.
	 *
	 * <p>Seta a classe CSS do elemento.</p>
	 * @access public
	 * @param string $psClass Classe do elemento
	 */
	public function setAttrClass($psClass){
		$this->csClass = $psClass;
	}

	/**
	 * Seta o nome do elemento.
	 *
	 * <p>Seta o nome do elemento.</p>
	 * @access public
	 * @param string $psName Nome do elemento
	 */
	public function setAttrName($psName){
		$this->csName = $psName;
	}

	/**
	 * Seta par�metros extra para o atributo 'style'.
	 *
	 * <p>Seta par�metros extra para o atributo 'style'.</p>
	 * @access public
	 * @param string $psExtraStyle Par�metros extra para o atributo 'style'
	 */
	public function setExtraStyle($psExtraStyle){
		$this->csExtraStyle = $psExtraStyle;
	}

	/**
	 * Seta o atributo extra do elemento.
	 *
	 * <p>Seta o atributo extra do elemento. Pode conter mais de um atributo
	 * (separados por espa�os em branco).</p>
	 * @access public
	 * @param string $psExtraAttribute Atributo extra do elemento
	 */
	public function setExtraAttribute($psExtraAttribute){
		$this->csExtraAttribute = $psExtraAttribute;
	}

	/**
	 * Retorna a box do elemento.
	 *
	 * <p>Retorna a box do elemento.</p>
	 * @access public
	 * @return FWDBox Box do elemento
	 */
	public function getObjFWDBox(){
		return $this->coBox;
	}

	/**
	 * Retorna a classe CSS do elemento.
	 *
	 * <p>Retorna a classe CSS do elemento.</p>
	 * @access public
	 * @return string Classe CSS do elemento
	 */
	public function getClass(){
		return $this->csClass;
	}

	/**
	 * Retorna o nome do elemento.
	 *
	 * <p>Retorna o nome do elemento.</p>
	 * @access public
	 * @return string Nome do elemento
	 */
	public function getAttrName(){
		return $this->csName;
	}

	/**
	 * Retorna par�metros extra para o atributo 'style'.
	 *
	 * <p>Retorna par�metros extra para o atributo 'style'.</p>
	 * @access public
	 * @return string Par�metros extra para o atributo 'style'
	 */
	public function getExtraStyle(){
		return $this->csExtraStyle;
	}

	/**
	 * Retorna atributo extra do elemento.
	 *
	 * <p>Retorna atributo extra do elemento. Pode conter mais de um atributo
	 * (separados por espa�os em branco).</p>
	 * @access public
	 * @return string Atributo extra do elemento
	 */
	public function getExtraAttribute(){
		return $this->csExtraAttribute;
	}

}
?>