<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDViewGroup. Organiza as views verticalmente.
 *
 * <p>Viewgroup cl�ssica. Organiza cada view verticalmente, ou seja, uma
 * depois da outra, em camadas verticais.</p>
 * @package FWD5
 * @subpackage viewgroup
 */
class FWDViewGroup extends FWDContainer {

	/**
	 * Define se deve existir uma barra de rolagem no caso de um overflow
	 * @var boolean
	 * @access protected
	 */
	protected $cbScrollbar = false;

	/**
	 * Define a profundidade da view, para manipular sobreposi��o de elementos (zenith-index)
	 * @var integer
	 * @access protected
	 */
	protected $ciIndex = 0;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDViewGroup.</p>
	 * @access public
	 * @param FWDBox $pobox Box que define os limites da viewgroup
	 */
	public function __construct($poBox = null) {
		parent::__construct($poBox);
	}

	/**
	 * Desenha o cabe�alho da viewgroup.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o cabe�alho da viewgroup.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites da viewgroup
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawHeader($poBox = null) {
		$this->setAttrTabIndex(0);
			
		$msStyle = $this->getStyle();

		if ($this->ciIndex)
		$msStyle .= "z-index:".$this->getAttrIndex().";";
			
		$msScrollbar = ($this->getAttrScrollbar()?"overflow:auto;":"overflow:visible;");
		$msStyle .= $msScrollbar;
		$msStyle = "style='{$msStyle}'";
			
		return "<div {$this->getGlobalProperty()} {$msStyle} {$this->getEvents()}>\n";
	}

	/**
	 * Desenha o corpo da viewgroup.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o corpo da viewgroup.</p>
	 * @access public
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawBody(){
		$msOut = '';
		$msWarnings = '';
		$poBox = $this->getObjFWDBox();
		foreach($this->getContent() as $msView){
			if($msView instanceof FWDWarning){
				$msWarnings.= $msView->draw($poBox);
			}else{
				$msOut.= $msView->draw($poBox);
			}
		}
		return $msOut.$msWarnings;
	}

	/**
	 * Desenha o rodap� da viewgroup.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o rodap� da viewgroup.</p>
	 * @access public
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawFooter() {
		return "\n</div>\n";
	}

	/**
	 * Adiciona um calend�rio.
	 *
	 * <p>Adiciona um objeto de calend�rio da classe FWDCalendar
	 * na classe FWDViewGroup.</p>
	 * @access public
	 * @param FWDCalendar $poCalendar Calend�rio
	 */
	public function addObjFWDCalendar($poCalendar) {
		$this->addObjFWDView($poCalendar);
	}

	/**
	 * Seta a profundidade da viewgroup.
	 *
	 * <p>Seta a profundidade da viewgroup (zenith-index).</p>
	 * @access public
	 * @param integer $piIndex Profundidade da view
	 */
	public function setAttrIndex($piIndex) {
		$this->ciIndex = $piIndex;
	}

	/**
	 * Seta o valor booleano do atributo de barra de rolagem.
	 *
	 * <p>Seta o valor booleano do atributo que define se deve existir
	 * uma barra de rolagem no caso de um overflow. Para a barra de rolagem
	 * existir, o atributo deve conter o valor booleano TRUE. Para n�o existir,
	 * o atributo deve conter o valor booleano FALSE.</p>
	 * @access public
	 * @param string $psScrollbar Define se deve existir uma barra de rolagem no caso de um overflow
	 */
	public function setAttrScrollbar($psScrollbar) {
		$this->cbScrollbar = FWDWebLib::attributeParser($psScrollbar,array("true"=>true, "false"=>false),"psScrollbar");
	}

	/**
	 * Retorna a profundidade da view.
	 *
	 * <p>Retorna a profundidade da view (zenith-index).</p>
	 * @access public
	 * @return integer Profundidade da view
	 */
	public function getAttrIndex() {
		return $this->ciIndex;
	}

	/**
	 * Retorna o valor booleano do atributo de barra de rolagem.
	 *
	 * <p>Retorna o valor booleano TRUE se deve existir uma barra de rolagem
	 * no caso de um overflow. Retorna FALSE caso contr�rio.</p>
	 * @access public
	 * @return boolean Indica se deve existir uma barra de rolagem no caso de um overflow
	 */
	public function getAttrScrollbar() {
		return $this->cbScrollbar;
	}

	/**
	 * Retorna um array com todos sub-elementos do viewgroup
	 *
	 * <p>Retorna um array contendo todos elementos contidos no viewgroup ou em
	 * seus descendentes.</p>
	 * @access public
	 * @return array Array de sub-elementos
	 */
	public function collect() {
		$maOut = array();
		$maOut = array_merge($maOut,parent::collect());
		return $maOut;
	}
}
?>