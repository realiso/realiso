<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDDialog. Implementa o controle das views.
 *
 * <p>Classe que implementa o controle das views. Objeto principal da
 * framework. Controla as views como um form/dialog em uma estrutura
 * dirigida a eventos.</p>
 * @package FWD5
 * @subpackage viewgroup
 */
class FWDDialog extends FWDViewGroup {

	/**
	 * Evento temporal a ser executado
	 * @var FWDTempEvent
	 * @access protected
	 */
	protected $coTempEvent = null;

	/**
	 * Menu de contexto
	 * @var FWDMenu
	 * @access protected
	 */
	protected $caMenus = array();

	/**
	 * Array de parametros por GET
	 * @var array
	 * @access private
	 */
	private $caGetParameter = array();

	/**
	 * Array de parametros por SESSION
	 * @var array
	 * @access private
	 */
	private $caSessionParameter = array();

	/**
	 * Elemento que deve ser focado ao carregar a p�gina
	 * @var string
	 * @access private
	 */
	private $csFocus = "";

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDDialog.</p>
	 * @access public
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Retorna as propriedades globais do elemento.
	 *
	 * <p>M�todo para retornar as propriedades globais do elemento.</p>
	 * @access public
	 * @return string Propriedades globais do elemento
	 */
	public function getGlobalProperty(){
		$msAccessKey = (($this->csAccessKey)?"accesskey='{$this->csAccessKey}'":"");
		$msResize = (($this->cbResize)?"resize='true'":"");
		$msClass = (($this->csClass)?"class='{$this->csClass}'":"");
		$msId = 'dialog';
		$msValue = "name='{$msId}' id='{$msId}' {$msAccessKey} {$msResize} {$msClass}";
		return $msValue;
	}

	/**
	 * Desenha o cabe�alho da Dialog.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o cabe�alho da Dialog.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites da dialog
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawHeader($poBox = null) {
		$miTop = $this->coBox->getAttrTop();
		if($miTop == 0) {
			$this->coBox->setAttrTop(1);
		}
		return parent::drawHeader($poBox);
	}

	/**
	 * Desenha a tela.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML/javascript,
	 * todas as informa��es necess�rias para implementar o menu de contexto.</p>
	 * @access public
	 * @return string C�digo HTML/javascript que ser� inserido na p�gina
	 */
	public function drawBody() {
		$msCodeMenu = "";
		foreach($this->getMenus() as $moMenu){
			$msCodeMenu .= $moMenu->draw();
		}
		$msOut = parent::drawBody().$msCodeMenu;
		return $msOut;
	}

	/**
	 * Desenha o rodap� da Dialog.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o rodap� da Dialog.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites da dialog
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawFooter($poBox = null) {
		$msFocus = "";

		if ($this->getAttrFocus())
		$msFocus = "\n\n<script> gebi('{$this->getAttrFocus()}').focus(); </script>\n\n";
		else {
			$dialog = FWDWebLib::getObject($this->getAttrName());
			$collection = $dialog->collect();

			$msMin = 0;
			$msMinName = "";

			if(count($collection)>0) {
				foreach($collection as $key => $view) {
					if($view instanceof FWDView) {
						$temp = $view->getAttrTabIndex();
						if (($temp<$msMin) && ($temp>0)) {
							$msMin = $temp;
							$msMinName = $view->getAttrName();
						}
					}
				}
			}
			if($msMin)
			$msFocus = "\n\n<script> gebi('{$msMinName}').focus(); </script>\n\n";
		}

		$msOut = parent::drawFooter($poBox).$msFocus;
		return $msOut;
	}

	/**
	 * Seta um evento temporal.
	 *
	 * <p>Seta um evento temporal.</p>
	 * @access public
	 * @param FWDTempEvent $poTempEvent Evento temporal
	 */
	public function setObjFWDTempEvent($poTempEvent) {
		$this->coTempEvent = $poTempEvent;
	}

	/**
	 * Retorna o objeto de evento temporal.
	 *
	 * <p>Retorna o objeto de evento temporal.</p>
	 * @access public
	 * @return object Evento temporal
	 */
	public function getObjFWDTempEvent() {
		return $this->coTempEvent;
	}

	/**
	 * Renderiza evento temporal.
	 *
	 * <p>Renderiza evento temporal.</p>
	 * @access public
	 * @return string Depende do objeto de evento que foi adicionado
	 */
	public function renderLoad(){
		$moOnload = new FWDClientEvent('onLoad');
		$moOnload->setAttrValue('resize_popup();');
		$this->addEvent($moOnload);
		$msOnLoad="";
		$msOnUnload="";
		if(isset($this->caEvent["onload"])){
			$msOnLoad = $this->caEvent["onload"]->render();
		}
		if(isset($this->caEvent["onunload"])){
			$msOnUnload = $this->caEvent["onunload"]->render();
		}
		return $msOnLoad." ".$msOnUnload;
	}

	/**
	 * Adiciona um objeto de menu de contexto.
	 *
	 * <p>Adiciona um objeto de menu de contexto da classe FWDMenu
	 * na classe FWDDialog.</p>
	 * @access public
	 * @param FWDMenu $poMenu Menu de contexto
	 */
	public function addObjFWDMenu($poMenu) {
		$this->caMenus[] = $poMenu;
		$moEvent = new FWDClientEvent('OnClick','hidemenu',$poMenu->getAttrName());
		$this->addEvent($moEvent);
	}

	/**
	 * Retorna o array de objetos de menu de contexto.
	 *
	 * <p>Retorna o array com os menu de contexto.</p>
	 * @access public
	 * @return array Array de FWDMenu
	 */
	public function getMenus() {
		return $this->caMenus;
	}

	/**
	 * Adiciona um objeto de GET.
	 *
	 * <p>Adiciona um objeto de GET.</p>
	 * @access public
	 * @param FWDGetParameter $poGetParameter objeto GetParameter
	 */
	public function addObjFWDGetParameter(FWDGetParameter $poGetParameter) {
		$this->caGetParameter[] = $poGetParameter;
	}

	/**
	 * Adiciona um objeto SessionParameter.
	 *
	 * <p>Adiciona um objeto de SessionParameter.</p>
	 * @access public
	 * @param FWDSessionParameter $poSessionParameter Objeto SessionParameter
	 */
	public function addObjFWDSessionParameter(FWDSessionParameter $poSessionParameter) {
		$this->caSessionParameter[] = $poSessionParameter;
	}

	/**
	 * Retorna um array com todos sub-elementos da dialog
	 *
	 * <p>Retorna um array contendo todos elementos contidos na dialog ou em
	 * seus descendentes e os menus da dialog</p>
	 * @access public
	 * @return array Array de sub-elementos
	 */
	public function collect() {
		$maOut = array();
		$maOut = array_merge($maOut,$this->caGetParameter);
		$maOut = array_merge($maOut,$this->caSessionParameter);
		$maOut = array_merge($maOut,parent::collect());
		if($this->caMenus)
		foreach ($this->caMenus as $moMenu)
		$maOut = array_merge($maOut,$moMenu->collect());
		return $maOut;
	}

	/**
	 * Seta o elemento que deve ser focado ao carregar a p�gina.
	 *
	 * <p>Seta o elemento que deve ser focado ao carregar a p�gina.</p>
	 * @access public
	 * @param string $psFocus Elemento que deve ser focado
	 */
	public function setAttrFocus($psFocus) {
		$this->csFocus = $psFocus;
	}

	/**
	 * Retorna o elemento que deve ser focado ao carregar a p�gina.
	 *
	 * <p>Retorna o elemento que deve ser focado ao carregar a p�gina.</p>
	 * @access public
	 * @return string Elemento que deve ser focado
	 */
	public function getAttrFocus() {
		return $this->csFocus;
	}
}
?>