<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDPanel. Cerca a view com uma borda mais trabalhada.
 *
 * <p>Classe similar a classe FWDViewGroup, mas cerca a view com uma borda
 * mais trabalhada.</p>
 * @package FWD5
 * @subpackage viewgroup
 */
class FWDPanel extends FWDViewGroup {

	/**
	 * Array das FWDViews que serao desenhadas no in�cio do panel
	 * @var array
	 * @access protected
	 */
	protected $caTitleViews = array();

	/**
	 * Inteiro para controlar a altura do t�tulo
	 * @var integer
	 * @access private
	 */
	private $ciTitleHeight = 0;

	private $ciTitleMarginLeft=10;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDPanel.</p>
	 * @access public
	 * @param FWDPanel $pobox Box que define os limites do panel
	 */
	public function __construct($poBox = null) {
		parent::__construct($poBox);
	}

	/**
	 * Desenha o corpo do panel.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o corpo do panel.</p>
	 * @access public
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawBody() {
		$msOut = "";

		foreach($this->caTitleViews as $moTitleView) {
			$this->alignTitle($moTitleView);
			$newDiv = new FWDDiv();
			$newBox = new FWDBox();
			$newBox->setAttrWidth($moTitleView->getObjFWDBox()->getAttrWidth());
			$newBox->setAttrLeft(0);
			$newBox->setAttrTop($moTitleView->getObjFWDBox()->getAttrTop());
			$newBox->setAttrHeight($moTitleView->getObjFWDBox()->getAttrHeight());
			$newDiv->setObjFWDBox($newBox);
			$newDiv->setClass($moTitleView->getAttrClass());
			if($moTitleView instanceof FWDStatic)
			if($moTitleView->getAttrMarginLeft()==0)
			$moTitleView->setAttrMarginLeft($this->ciTitleMarginLeft);

			$msOut .= $moTitleView->draw();
			$msOut .= $newDiv->draw();
		}

		foreach($this->getContent() as $moView) $msOut .= $moView->draw($this->getObjFWDBox());

		return $msOut;
	}

	/**
	 * Adiciona uma view.
	 *
	 * <p>Adiciona um objeto view da classe fwdView na classe fwdPanel.</p>
	 * @access public
	 * @param FWDView $poView View
	 * @param string $psTarget Alvo
	 */
	public function addObjFWDView($poView, $psTarget = "") {
		switch($psTarget) {
			case "title":
				$this->caTitleViews[] = $poView;
				break;
			default:
				parent::addObjFWDView( $poView, $psTarget);
		}
	}

	/**
	 * Retorna o array de views do t�tulo.
	 *
	 * <p>Retorna o array de views do t�tulo.</p>
	 * @access public
	 * @return array Array de views
	 */
	public function getTitles() {
		return $this->caTitleViews;
	}

	/**
	 * Posiciona e alinha a view dentro do panel.
	 *
	 * <p>Posiciona e alinha a view dentro do panel.</p>
	 * @access public
	 * @param FWDView $poView View
	 */
	public function alignTitle($poView) {
		$miTop = $this->ciTitleHeight;
		$miLeft = 0;
		if(FWDWebLib::browserIsIE())
		$miWidth = $this->getObjFWDBox()->getAttrWidth()-2;
		else
		$miWidth = $this->getObjFWDBox()->getAttrWidth();

		$miHeight = $poView->getObjFWDBox()->getAttrHeight();
		$this->ciTitleHeight += $miHeight;

		$poView->setObjFWDBox(new FWDBox($miLeft, $miTop, $miHeight, $miWidth));
	}

	/**
	 * Retorna um array com todos sub-elementos do panel
	 *
	 * <p>Retorna um array contendo todos elementos contidos no panel ou em
	 * seus descendentes.</p>
	 * @access public
	 * @return array Array de sub-elementos
	 */
	public function collect() {
		$maOut = array();
		$maOut = array_merge($maOut,$this->caTitleViews);
		$maOut = array_merge($maOut,parent::collect());
		return $maOut;
	}
}
?>