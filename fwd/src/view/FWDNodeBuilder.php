<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDNodeBuilder. Classe que constr�i nodos.
 *
 * <p>Classe, usada pela DBTree, que constr�i nodos a partir de um id e um
 * valor obtidos atrav�s do DataSet.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDNodeBuilder {

	/**
	 * Recebe um id e um valor e constr�i um nodo
	 *
	 * <p>Recebe um id e um valor e constr�i um nodo (objeto FWDTree).</p>
	 * @access public
	 * @param string $psId Identificador
	 * @param string $psValue Valor
	 * @return FWDTree Nodo construido
	 */
	public function buildNode($psId, $psValue){
		$moNode = new FWDTree();
		$moNode->setAttrName($psId);
		$moStatic = new FWDStatic();
		$moStatic->setValue($psValue);
		$moNode->addObjFWDView($moStatic);
		return $moNode;
	}
}
?>