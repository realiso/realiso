<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDTree. Implementa �rvores com suporte a itens.
 *
 * <p>Classe que implementa �rvores em javascript com suporte a itens (radio,check).</p>
 * @package FWD5
 * @subpackage view
 */
class FWDTree extends FWDTreeBase {

	/**
	 * Container que cont�m todos elementos presentes no nodo
	 * @var FWDViewgroup
	 * @access protected
	 */
	protected $coViewGroup = null;

	/**
	 * Nome da TreeBase a que a Tree pertence
	 * @var FWDTreeBaseName
	 * @access protected
	 */
	protected $csTreeBaseName;

	/**
	 * Indica se o nodo est� aberto inicialmente
	 * @var boolean
	 * @access protected
	 */
	protected $cbExpanded = null;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDTree.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites da tree
	 */
	public function __construct($poBox = null) {
		parent::__construct($poBox);
		$this->coViewGroup = new FWDViewgroup();
		$this->coViewGroup->setAttrClass('FWDTree_ViewGroup');
		$this->coViewGroup->setAttrPosition('relative');
	}

	/**
	 * Desenha em tela.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML/javascript,
	 * todas as informa��es necess�rias para exibir em tela o objeto.</p>
	 * @access public
	 * @param boolean $pbLastSibling Verdadeiro se for o �ltimo elemento
	 * @param FWDBox $poBox Box que define os limites do objeto
	 * @return string C�digo HTML/javascript que ser� inserido na p�gina
	 */
	public function draw($pbLastSibling,$poFWDBox = null){
		$msOut = "";
		if ($this->cbShouldDraw) {

			$msOut .= $this->drawHeader($poFWDBox);
			$msOut .= $this->drawBody($pbLastSibling,$poFWDBox);
			$msOut .=  $this->drawFooter($poFWDBox);

		}
		return $msOut;
	}

	/**
	 * Desenha o cabe�alho do objeto.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o cabe�alho do objeto.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do objeto
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	protected function drawHeader($poBox = null){
		return "<table border='0' {$this->getGlobalProperty()}>\n";
	}

	/**
	 * Desenha o corpo do objeto.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o corpo do objeto.</p>
	 * @access public
	 * @param boolean $pbLastSibling Verdadeiro se for o �ltimo elemento
	 * @param FWDBox $poBox Box que define os limites do objeto
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	protected function drawBody($pbLastSibling,$poBox = null){
		$moWebLib = FWDWebLib::getInstance();
		$msGFX = $moWebLib->getGfxRef();
		$msId = "{$this->getTreeBaseName()}_{$this->csId}";
		$miNumNodes = count($this->caNodes);
		$mbOpen = $this->getAttrExpanded();
		$msStyleWithBackground = "style='vertical-align:top;background:url({$msGFX}line.gif);background-repeat:repeat-y;background-position:top;'";
		$msStyleWithoutBackground = "style='vertical-align:top;'";
		$msOut = '';
		$msTreeClass = $this->getAttrClass();

		$maCollection = $this->coViewGroup->collect();
		if(count($maCollection)>0){
			foreach($maCollection as $msKey => $moView){
				$moView->setAttrPosition('relative');
			}
		}

		$this->coViewGroup->setAttrName($this->getAttrName().'_vg');

		$msOut.= "<tr class='{$msTreeClass}_tr'>\n";
		$msOut.= "<td class='{$msTreeClass}_td' ".($pbLastSibling?$msStyleWithoutBackground:$msStyleWithBackground).">\n";
		if($miNumNodes>0){
			$msOut.= "<a href=\"javascript:toggle('$msId')\"><img id='{$msId}_img' src='{$msGFX}".($mbOpen?'minus':'plus').".gif'></a>\n";
		}else{
			$msOut.= "<img id='{$msId}_img' src='{$msGFX}join.gif'>\n";
		}
		$msOut.= "</td>\n";
		$msOut.= "<td class='{$msTreeClass}_td'>\n";

		$msOut.= $this->coViewGroup->drawHeader();
		$maContent = $this->coViewGroup->getContent();
		$mbNeedshift = false;
		foreach($maContent as $moContent){
			if($moContent instanceof FWDStatic){
				$moContent->setAttrClass('StaticTree');
			}
			$msOut .= $moContent->draw();
			if($this->getAttrAutoFill() && $moContent instanceof FWDCheckBox){
				$msOut .= "<script> addTreeCheckBox('".$moContent->getAttrController()."', '".$moContent->getAttrKey()."');</script>";
			}
		}
		$msOut .= $this->coViewGroup->drawFooter();

		$msOut.= "</td>\n";
		$msOut.= "</tr>\n";
		if($miNumNodes>0){
			$msOut.= "<tr id='{$msId}_tr' class='{$msTreeClass}_tr'".($mbOpen?'':' style="display:none"').">\n";
			$msOut.= "<td class='{$msTreeClass}_td' ".($pbLastSibling?$msStyleWithoutBackground:$msStyleWithBackground)."></td>\n";
			$msOut.= "<td class='{$msTreeClass}_td'>\n";
			for($miI=0;$miI<$miNumNodes-1;$miI++){
				$msOut.= $this->caNodes[$miI]->draw(false);
			}
			$msOut.= $this->caNodes[$miI]->draw(true);
			$msOut.= "</td>\n";
			$msOut.= "</tr>\n";
		}
		return $msOut;
	}

	/**
	 * Desenha o rodap� do objeto.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o rodap� do objeto.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do objeto
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	protected function drawFooter($poBox = null){
		return '</table>';
	}

	/**
	 * Adiciona uma �rvore.
	 *
	 * <p>Adiciona um objeto �rvore da classe FWDTree na classe FWDTree
	 * (�rvores podem conter outras �rvores).</p>
	 * @access public
	 * @param FWDTree $poTree �rvore a ser adicionada
	 * @deprecated 28/04/2006
	 */
	public function addObjFWDTree($poTree){
		$this->caNodes[] = $poTree;
	}

	/**
	 * Adiciona uma view
	 *
	 * <p>Adiciona uma view ao nodo raiz da �rvore.</p>
	 * @access public
	 * @param FWDView $poView View
	 */
	public function addObjFWDView($poView){
		$this->coViewGroup->addObjFWDView($poView);
	}

	/**
	 * Retorna um array com todos sub-elementos da �rvore
	 *
	 * <p>Retorna um array contendo todos elementos contidos no nodo da �rvore
	 * ou em suas sub-�rvores.</p>
	 * @access public
	 * @return array Array de sub-elementos
	 */
	public function collect(){
		$maOut = $this->coViewGroup->collect();
		$maOut = array_merge($maOut,parent::collect());
		return $maOut;
	}

	/**
	 * Seta o atributo expanded.
	 *
	 * <p>Seta o valor do atributo expanded, que indica se o nodo est� aberto inicialmente.</p>
	 * @access public
	 * @param string $psExpanded Define se o nodo est� aberto inicialmente
	 */
	public function setAttrExpanded($psExpanded){
		$this->cbExpanded = FWDWebLib::attributeParser($psExpanded,array("true"=>true, "false"=>false),"expanded");
	}

	/**
	 * Retorna o valor do atributo expanded.
	 *
	 * <p>Retorna o valor booleano do atributo expanded.</p>
	 * @access public
	 * @return boolean Indica se o nodo est� aberto inicialmente
	 */
	public function getAttrExpanded(){
		if($this->cbExpanded!==null){
			return $this->cbExpanded;
		}else{
			$moWebLib = FWDWebLib::getInstance();
			$moTreeBase = $moWebLib->getObject($this->getTreeBaseName());
			if(!$moTreeBase) trigger_error("treeBaseName undefined",E_USER_WARNING);
			return $moTreeBase->getAttrExpandAll();
		}
	}

	/**
	 * Seta o nome da TreeBase a que a Tree pertence
	 *
	 * <p>Seta o nome da TreeBase a que a Tree pertence (propaga recursivamente
	 * para todos descendentes).</p>
	 * @access public
	 * @param string $psTreeBaseName Nome da TreeBase a que a Tree pertence
	 */
	public function setTreeBaseName($psTreeBaseName){
		$this->csTreeBaseName = $psTreeBaseName;
		if(count($this->caNodes)>0){
			foreach($this->caNodes as $moNode){
				$moNode->setTreeBaseName($psTreeBaseName);
			}
		}
	}

	/**
	 * Retorna o nome da TreeBase a que a Tree pertence
	 *
	 * <p>Retorna o nome da TreeBase a que a Tree pertence.</p>
	 * @access public
	 * @return string Nome da TreeBase a que a Tree pertence
	 */
	public function getTreeBaseName(){
		return $this->csTreeBaseName;
	}

	/**
	 * Elimina todas as trees pertencentes � mesma.
	 *
	 * <p>Deleta as sub-�rvores desta.
	 * @access public
	 */
	public function cleanTree() {
		$this->caNodes = array();
	}
}
?>