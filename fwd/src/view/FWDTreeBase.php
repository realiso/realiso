<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDTreeBase. Implementa �rvores em javascript.
 *
 * <p>Classe que implementa �rvores em javascript.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDTreeBase extends FWDView implements FWDCollectable {

	/**
	 * Id da �rvore (necess�rio para as fun��es em JavaScript que manipulam a �rvore)
	 * @var integer
	 * @access protected
	 */
	protected $ciTreeId = 0;

	/**
	 * Array de objetos FWDTree representando os nodos da �rvore
	 * @var array
	 * @access protected
	 */
	protected $caNodes = array();

	/**
	 * Array de objetos FWDController
	 * @var array
	 * @access protected
	 */
	protected $caControllers = array();

	/**
	 * Id do nodo (o id � usado pelo javascript)
	 * @var integer
	 * @access protected
	 */
	protected $csId;

	/**
	 * Indica se os nodos da �rvore est�o abertos por default
	 * @var boolean
	 * @access protected
	 */
	protected $cbExpandAll = false;

	/**
	 * Indica se o nodo ir� auto preencher os pais e filhos ao ser selecionado
	 * @var boolean
	 * @access protected
	 */
	protected $cbAutoFill = null;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDTreeBase.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites da �rvore
	 */
	public function __construct($poBox = null){
		parent::__construct($poBox);
	}

	/**
	 * Desenha o cabe�alho do objeto.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o cabe�alho do objeto.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do objeto
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	protected function drawHeader($poBox = null){
		$msOut = '';
		if(count($this->caControllers)>0){
			foreach($this->caControllers as $moController){
				$msOut.= $moController->draw();
			}
		}
		$msOut.= "<div {$this->getGlobalProperty()} style='{$this->getStyle()}' {$this->getEvents()}>";
		return $msOut;
	}

	/**
	 * Desenha o corpo do objeto.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o corpo do objeto.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do objeto
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	protected function drawBody($poBox = null){
		$moWebLib = FWDWebLib::getInstance();
		$msGFX = $moWebLib->getGfxRef();
		$msOut = "<script> var gfx_ref = '$msGFX'; </script>\n";
		$miNumNodes = count($this->caNodes);
		for($miI=0;$miI<$miNumNodes-1;$miI++){
			$msOut.= $this->caNodes[$miI]->draw(false);
		}
		$msOut.= $this->caNodes[$miI]->draw(true);
		return $msOut;
	}

	/**
	 * Desenha o rodap� do objeto.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o rodap� do objeto.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do objeto
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	protected function drawFooter($poBox = null){
		return '</div>';
	}

	/**
	 * Retorna o Id da �rvore.
	 *
	 * <p>Retorna o Id da �rvore.</p>
	 * @access public
	 * @return integer Id da �rvore
	 */
	public function getTreeId(){
		return $this->ciTreeId;
	}

	/**
	 * Seta o Id da �rvore.
	 *
	 * <p>Seta o valor do atributo Id da �rvore.</p>
	 * @access public
	 * @param integer $piTreeId Id da �rvore
	 */
	public function setTreeId($piTreeId){
		$this->ciTreeId = $piTreeId;
	}

	/**
	 * Seta o atributo expandall.
	 *
	 * <p>Seta o valor do atributo expandall, que indica se os nodos da �rvore est�o abertos por default.</p>
	 * @access public
	 * @param string $psExpandAll Define se os nodos da �rvore est�o abertos por default
	 */
	public function setAttrExpandAll($psExpandAll){
		$this->cbExpandAll = FWDWebLib::attributeParser($psExpandAll,array("true"=>true, "false"=>false),"expandall");
	}

	/**
	 * Retorna o valor do atributo expandall.
	 *
	 * <p>Retorna o valor booleano do atributo expandall.</p>
	 * @access public
	 * @return boolean Indica se os nodos da �rvore est�o abertos por default
	 */
	public function getAttrExpandAll(){
		return $this->cbExpandAll;
	}

	public function setAttrAutoFill($psAutoFill){
		$this->cbAutoFill = $psAutoFill;
	}

	public function getAttrAutoFill(){
		return $this->cbAutoFill;
	}

	/**
	 * Adiciona uma �rvore.
	 *
	 * <p>Adiciona uma �rvore com suporte a itens da classe FWDTree
	 * na classe FWDTreeBase.</p>
	 * @access public
	 * @param FWDTree $poTree �rvore a ser adicionada
	 */
	public function addObjFWDTree($poTree){
		$poTree->setTreeBaseName($this->getAttrName());
		$this->caNodes[] = $poTree;
	}

	/**
	 * Adiciona um Controller
	 *
	 * <p>Adiciona um objeto FWDController na classe FWDTreeBase.</p>
	 * @access public
	 * @param FWDController $poController Controller a ser adicionado
	 */
	public function addObjFWDController($poController){
		$this->caControllers[] = $poController;
	}

	/**
	 * Retorna um array com todos sub-elementos da �rvore e a pr�pria �rvore
	 *
	 * <p>Retorna um array contendo todos elementos contidos na �rvore ou em suas
	 * sub-�rvores (retorna tamb�m a pr�pria �rvore).</p>
	 * @access public
	 * @return array Array de sub-elementos
	 */
	public function collect(){
		$maOut = array($this);
		if(count($this->caNodes)>0){
			foreach($this->caNodes as $moTree){
				$maCollection = $moTree->collect();
				$maOut = array_merge($maOut,$maCollection);
			}
		}
		if(count($this->caControllers)>0){
			foreach($this->caControllers as $moController){
				$maCollection = $moController->collect();
				$maOut = array_merge($maOut,$maCollection);
			}
		}
		return $maOut;
	}

	/**
	 * Seta os ids de todos os nodos da �rvore
	 *
	 * <p>Percorre a �rvore setando os ids de todos os nodos.
	 * OBS: Os par�metros somente s�o usados na recurs�o.
	 * </p>
	 *
	 * @access public
	 * @param string $psParentId Id do pai
	 * @param integer $piNumber N�mero
	 */
	public function setIds($psParentId='',$piNumber=1){
		if($psParentId!='') $this->csId = $psParentId.'.'.$piNumber;
		elseif($this->csId=='') $this->csId = $piNumber;
		$miNumNodes = count($this->caNodes);
		for($miI=0;$miI<$miNumNodes;$miI++){
			$this->caNodes[$miI]->setIds($this->csId,$miI+1);
		}
	}

	/**
	 * M�todo executado quando o XML do elemento termina de carregar.
	 *
	 * <p>M�todo executado quando o XML do elemento termina de carregar. Seta os
	 * ids de todos os nodos da �rvore, mas s� se n�o estiver respondendo a um
	 * evento e sim carregando a tela.</p>
	 *
	 * @access public
	 */
	public function execute(){
		if(!FWDWebLib::getPOST("event_js")) $this->setIds();
	}

}
?>