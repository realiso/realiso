<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDLink. Implementa Links.
 *
 * <p>Classe que implementa Links em elementos da FWD5.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDLink extends FWDDrawing {

	/**
	 * Uniform Resource Identifier do recurso Web
	 * @var string
	 * @access private
	 */
	private $csURI = "";

	/**
	 * Window do Link (atributo target da tag html 'a')
	 * @var string
	 * @access private
	 */
	private $csWindow = "_self";

	/**
	 * Classe CSS
	 * @var string
	 * @access private
	 */
	private $csClass = "FWDLink";

	/**
	 * Valor do Link
	 * @var FWDString
	 * @access private
	 */
	private $coString = null;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDLink.</p>
	 * @access public
	 */
	public function __construct() {
	}

	/**
	 * Desenha o cabe�alho do link.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o cabe�alho do link.</p>
	 * @access public
	 *
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawHeader() {
		$msURI = $this->getAttrURI();
		if (!$msURI)
		trigger_error("Object FWDLink must have an URI",E_USER_WARNING);

		$msWindow = $this->getAttrWindow();
		$msClass = $this->getAttrClass() ? $this->getAttrClass() : "";

		return "\n<a href='{$msURI}' target='{$msWindow}' class='{$msClass}' style='cursor:pointer' >\n";
	}

	/**
	 * Desenha o corpo do link.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o corpo do link.</p>
	 * @access public
	 *
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawBody() {
		$msOut = $this->coString->getAttrString();
		if (!$msOut)
		trigger_error("Object FWDLink must have a non-empty string",E_USER_WARNING);

		return $msOut;
	}

	/**
	 * Desenha o rodap� do link.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o rodap� do link.</p>
	 * @access public
	 *
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawFooter() {
		return "</a>\n";
	}

	/**
	 * Seta o URI do link.
	 *
	 * <p>M�todo para setar o URI do link.</p>
	 * @access public
	 * @param string $psURI URI do link
	 */
	public function setAttrURI($psURI) {
		$this->csURI = $psURI;
	}

	/**
	 * Retorna o URI do link.
	 *
	 * <p>M�todo para retornar o URI do link.</p>
	 * @access public
	 * @return string URI do link
	 */
	public function getAttrURI() {
		return $this->csURI;
	}

	/**
	 * Seta a Window do link.
	 *
	 * <p>M�todo para setar a Window do link.</p>
	 * @access public
	 * @param string $psWindow Window do link
	 */
	public function setAttrWindow($psWindow) {
		$this->csWindow = $psWindow;
	}

	/**
	 * Retorna a Window do link.
	 *
	 * <p>M�todo para retornar a Window do link.</p>
	 * @access public
	 * @return string Window do link
	 */
	public function getAttrWindow() {
		return $this->csWindow;
	}

	/**
	 * Seta a classe de CSS do link.
	 *
	 * <p>M�todo para setar a classe de CSS do link.</p>
	 * @access public
	 * @param string $psClass Classe
	 */
	public function setAttrClass($psClass) {
		$this->csClass = $psClass;
	}

	/**
	 * Retorna a classe de CSS do link.
	 *
	 * <p>M�todo para retornar a classe de CSS do link.</p>
	 * @access public
	 * @return string Classe de CSS
	 */
	public function getAttrClass() {
		return $this->csClass;
	}

	/**
	 * Adiciona um objeto String (valor do Link).
	 *
	 * <p>Adiciona um objeto String (valor do Link).</p>
	 * @access public
	 * @param FWDString $poString Seta o valor do Link
	 */
	public function addObjFWDString(FWDString $poString) {
		$this->coString = $poString;
	}
}
?>