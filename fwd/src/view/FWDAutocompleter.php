<?
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
class FWDAutocompleter extends FWDView implements FWDPostValue{

	protected $left = 0;

	protected $top = 0;

	protected $popup = "";

	protected $label = "";

	protected $value;

	protected $idValue;
	
	protected $display = 'true';
	
	public function setAttrDisplay($display){
		$this->display = $display;
	}

	public function drawHeader($poBox = null) {
		return $this->getVariable()->draw()."";//$this->getIdObject()->draw();
	}

	public function drawBody($poBox = null) {
		$body = "";
		$body .= $this->getText()->draw();
		$body .= $this->getSearchButton()->draw();
		$body .= $this->getClearButton()->draw();
		return $body;
	}

	public function drawFooter($poBox){
		return "<script>
		function set".$this->getAttrName()."(id, name){
            gebi('".$this->getAttrName()."Id').value = id;
            gebi('".$this->getAttrName()."').value = name;
        } 
		</script>";
	}

	public function getVariable(){
		$var = new FWDVariable();
		$var->setAttrName($this->getAttrName()."Old");
		$var->setAttrValue("0");
		return $var;
	}

	public function getIdObject(){
		$var = new FWDVariable();
		$var->setAttrName($this->getAttrName()."Id");
		$var->setAttrValue($this->idValue);
		return $var;
	}

	public function setValue($psValue, $pbForce = true){
		$this->value = $psValue;
	}

	public function setIdValue($psValue){
		$this->idValue = $psValue;
	}

	public function setAttrValue($psValue){
		$this->value = $psValue;
	}

	public function getValue(){
		return $this->value;
	}

	public function getIdValue(){
		return FWDWebLib::getPOST($this->getAttrName()."Id");
	}

	public function getText(){
		$box = new FWDBox($this->left, $this->top, 22, 158);

		$event = new FWDServerEvent();
		$event->setAttrEvent("onKeyUp");
		$event->setAttrFunction("search_".$this->getAttrName());

		$text = new FWDText($box);
		$text->setAttrValue($this->value);
		$text->setAttrName($this->getAttrName());
		$text->setAttrMaxLength(255);
		$text->setAttrDisplay($this->display);
		$text->setAttrLabel($this->label);
		$text->addObjFWDEvent($event);

		return $text;
	}

	public function getSearchButton(){
		$event = new FWDClientEvent();
		$event->setAttrEvent("onClick");
		$event->setValue("isms_open_popup('popup_".$this->getAttrName()."_single_search','".$this->popup."','','true',402,600);", true);

		$text = new FWDString();
		$text->setAttrId("search_button");
		$text->setValue(FWDLanguage::getPHPStringValue('rs_search','Buscar'), true);

		$static = new FWDStatic(new FWDBox(0, 0, 20, 60));
		$static->setObjFWDString($text);
		$static->setAttrHorizontal("center");

		$button = new FWDViewButton(new FWDBox($this->left+163, $this->top, 20, 60));
		$button->setAttrClass("FWDViewButtonISMS");
		$button->addEvent($event);
		$button->addObjFWDDrawing($static);
		$button->setAttrDisplay($this->display);

		return $button;
	}

	public function getClearButton(){

		$event = new FWDClientEvent();
		$event->setAttrEvent("onClick");
		$event->setValue("gebi('".$this->getAttrName()."').value = '';gebi('".$this->getAttrName()."Id').value = '';", true);

		$text = new FWDString();
		$text->setAttrId("clear_button");
		$text->setValue(FWDLanguage::getPHPStringValue('rs_clean','Limpar'), true);

		$static = new FWDStatic(new FWDBox(0, 0, 20, 60));
		$static->setAttrHorizontal("center");
		$static->setObjFWDString($text);

		$button = new FWDViewButton(new FWDBox($this->left+233, $this->top, 20, 60));
		$button->addEvent($event);
		$button->setAttrClass("FWDViewButtonISMS");
		$button->addObjFWDDrawing($static);
		$button->setAttrDisplay($this->display);

		return $button;
	}
	
	public function setAttrPopup($popup){
		$this->popup = $popup;
	}

	public function getAttrPopup(){
		return $this->popup;
	}

	public function setAttrLabel($label){
		$this->label = $label;
	}

	public function getAttrLabel(){
		return $this->label;
	}

	public function setAttrLeft($piLeft) {
		$this->left = $piLeft;
	}

	public function setAttrTop($piTop) {
		$this->top = $piTop;
	}

	public function getAttrLeft() {
		return $this->left;
	}

	public function getAttrTop() {
		return $this->top;
	}
}

class DefaultFinder extends FWDRunnable {
	
	private $label;
	
	private $query;

	public function set($label, $query) {
		$this->label = $label;
		$this->query = $query;
	}

	public function run(){
		$obj = FWDWebLib::getObject($this->label)->getValue();
		$objOld = FWDWebLib::getPOST($this->label."Old");

		if($obj != $objOld && trim($obj) != ""){
			//$query = new QueryGridGroupResourceSearch(FWDWebLib::getConnection());
			//$query->setNameFilter($group);
			$this->query->setNameFilter($obj);
			$this->query->makeQuery();
			$this->query->executeQuery(5);

			$objTop = FWDWebLib::getObject($this->label)->getAttrTop()+22;
			$objLeft = FWDWebLib::getObject($this->label)->getAttrLeft();

			echo "var position = {top: {$objTop}, left:{$objLeft}}; ";
			echo "var listElements = new Array(); ";
			$i = 0;
			while($this->query->fetch()){
				$name = addslashes($this->query->getNameValue());
				$id = $this->query->getIdValue();

				echo "listElements[{$i}] = {name:'{$name}', id:'{$id}'}; ";
				$i++;
			}
			echo "createAutoCompleteDiv(listElements, '".$this->label."', '".$this->label."Id', position);";
		}
		echo "gebi('".$this->label."Old').value = '{$obj}';";
	}
}

?>