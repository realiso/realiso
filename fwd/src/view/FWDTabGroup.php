<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDTabGroup. Implementa um grupo de Tabs.
 *
 * <p>Classe que implementa um grupo de Tabs.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDTabGroup extends FWDContainer {

	/**
	 * Altura do TabItem (em pixels)
	 * @var integer
	 * @access protected
	 */
	protected $ciTabItemHeight = 33;

	/**
	 * �ndice do TabItem que deve ser carregado por default
	 * @var integer
	 * @access protected
	 */
	protected $ciLoadTabItemIndex = 1;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDTabGroup.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do TabGroup
	 */
	public function __construct($poBox = null){
		parent::__construct($poBox);
	}

	/**
	 * Seta o valor do atributo LoadTabItemIndex.
	 *
	 * <p>Seta o valor do atributo LoadTabItemIndex.</p>
	 * @access public
	 * @param string $psLoadTabItemIndex Define qual TabItem deve ser carregado por default
	 */
	public function setAttrLoadTabItemIndex($psLoadTabItemIndex) {
		$this->ciLoadTabItemIndex = $psLoadTabItemIndex;
	}

	/**
	 * Retorna o valor do atributo LoadTabItemIndex.
	 *
	 * <p>Retorna o valor do atributo LoadTabItemIndex.</p>
	 * @access public
	 * @return integer Indica qual TabItem deve ser carregado por default
	 */
	public function getAttrLoadTabItemIndex() {
		$this->ciLoadTabItemIndex;
	}

	/**
	 * Desenha o cabe�alho da Tabgroup.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o cabe�alho da tabgroup.</p>
	 * @access public
	 */
	public function drawHeader(){
		if(($this->caContent[$this->ciLoadTabItemIndex-1]->getShouldDraw()==false)||($this->caContent[$this->ciLoadTabItemIndex-1]->getAttrDisplay()==false)){
			$miCont = 0;
			$this->ciLoadTabItemIndex = -1;
			foreach($this->getContent() as $moItem){
				$miCont++;
				if(($moItem->getShouldDraw()==true)&&($moItem->getAttrDisplay()==true)){
					$this->ciLoadTabItemIndex = $miCont;
					return "<script>soTabSubManager.initTabGroup('{$this->csName}',{$this->ciLoadTabItemIndex});</script>";
					break;
				}
			}
		}else{
			return "<script>soTabSubManager.initTabGroup('{$this->csName}',{$this->ciLoadTabItemIndex});</script>";
		}
	}

	/**
	 * Desenha o corpo da TabGroup.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para desenhar o corpo da TabGroup.</p>
	 * @access public
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawBody() {
		$moTabGroup = new FWDViewGroup($this->getObjFWDBox());
		$moTabGroup->setAttrDebug($this->getAttrDebug());
		$moTabGroup->setAttrName($this->getAttrName());
		$moTabGroup->setAttrClass($this->getAttrClass());
		$miActiveTabs = 0;
		foreach($this->getContent() as $moItem){
			if($moItem->getShouldDraw() && $moItem->getAttrDisplay()){
				$miActiveTabs++;
			}
		}
		$miLeft = 0;
		$moTabGroupBox = $this->getObjFWDBox();
		$miTabGroupAvaliableWidth = $moTabGroupBox->getAttrWidth();
		foreach($this->getContent() as $miNumElem=>$moItem){
			$miNumElem++;
			if ($moItem->getShouldDraw() && $moItem->getAttrDisplay()){
				$moTabItemBox = $moItem->getObjFWDBox();
				$msFrameName = $this->getAttrName()."_iframe_".$miNumElem;
				$msDivName = $this->getAttrName()."_content_".$miNumElem;
				$msElems = $msFrameName.' '.$msDivName;
				$moAuxBox = new FWDBox($miLeft, 0, $this->ciTabItemHeight, $moTabItemBox->getAttrWidth());
				if($moItem->getAttrClass()==''){
					$moItem->setAttrClass($this->getAttrClass()=="FWDTabGroup"?"FWDTabItem":"FWDSubTabItem");
				}
				$moTabitem = $moItem->createItem($moAuxBox, $this->getAttrName(), $miNumElem, $miActiveTabs, $msElems, $moItem->getAttrUrl(), $this->ciLoadTabItemIndex);
				$miLeft += $moTabItemBox->getAttrWidth();
				$moTabGroup->addObjFWDView($moTabitem);
				$miTabGroupAvaliableWidth -= $moTabitem->getObjFWDBox()->getAttrWidth();
				$miFrameHeight = $moTabGroupBox->getAttrHeight() - $this->ciTabItemHeight;
				$miFrameWidth = $moTabGroupBox->getAttrWidth();
				$moFrame = new FWDDiv(new FWDBox(0,$this->ciTabItemHeight-1,$miFrameHeight,$miFrameWidth));
				$moFrame->setName($msDivName);
				$moFrame->setClass('');
				$moFrame->setExtraStyle(($miNumElem==$this->ciLoadTabItemIndex) ? '':'display:none;');
				$msValue = "<iframe name='$msFrameName' id='$msFrameName' width='100%' height='100%' marginwidth=0 marginheight=0"
				." hspace=0 vspace=0 frameborder=0 style='z-index:-1;' src=''></iframe>\n";
				$moFrame->setValue($msValue);
				$moTabGroup->addObjFWDView($moFrame);
			}
		}
		$miFillDivWidth = $miTabGroupAvaliableWidth;
		if($this->getAttrClass()=="FWDTabGroup"){
			//if(FWDWebLib::browserIsIE()){
			//	$miFillDivWidth-= 2;
			//}else{
				$miFillDivWidth-= 4;
			//}
		}
		$moFillDiv = new FWDDiv(new FWDBox($moTabGroupBox->getAttrWidth() - $miTabGroupAvaliableWidth,0,$this->ciTabItemHeight,$miFillDivWidth));
		$moFillDiv->setName($this->getAttrName()."_fill_div");
		$moFillDiv->setClass($this->getAttrClass()=="FWDTabGroup"?"FWDTabItem_notSelected":"FWDSubTabItem_notSelected");
		$moTabGroup->addObjFWDView($moFillDiv);
		return $moTabGroup->draw();
	}

	/**
	 * Desenha o rodap� da Tabgroup.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o rodap� da tabgroup.</p>
	 * @access public
	 */
	public function drawFooter(){
		if($this->ciLoadTabItemIndex>0){
			$miSelected = $this->ciLoadTabItemIndex;
			$msFrameName = "{$this->csName}_iframe_{$miSelected}";
			$msDivName = "{$this->csName}_content_{$miSelected}";
			$msUrl = $this->caContent[$miSelected-1]->getAttrUrl();
			return "<script language='javascript'>loadIframe(gebi('$msFrameName'),'$msUrl','$msDivName');</script>";
		}
		else {
			// N�o h� nenhuma aba a ser carregada
			// return "<script language='javascript'>alert('deu erro no tabgroup.php');</script>";
		}
		 
	}

	/**
	 * Adiciona um TabItem.
	 *
	 * <p>Adiciona um TabItem da classe FWDTabItem na classe FWDTabGroup.</p>
	 * @access public
	 *
	 * @param FWDTabItem $poTabItem Tabitem.
	 */
	public function addObjFWDTabItem(FWDTabItem $poTabItem) {
		$this->addObjFWDView($poTabItem);
	}
}
?>