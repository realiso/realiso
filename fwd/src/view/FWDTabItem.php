<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDTabItem. Implementa os itens da TabGroup.
 *
 * <p>Classe que implementa os itens da TabGroup.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDTabItem extends FWDView {

	/**
	 * Url para a qual a tab ir� redirecionar
	 * @var string
	 * @access protected
	 */
	protected $csUrl;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDTabItem. Overload do construtor da view,
	 * para n�o setar a classe css padr�o como sendo a mesma do objeto (para o
	 * tabitem poder 'herdar' a classe css de sua tabgroup, caso nenhuma classe
	 * seja especificada).</p>
	 * @access public
	 * @param string $pobox Box que define os limites do TabGroup
	 */
	public function __construct($poBox = null) {
		$this->coBox = new FWDBox();
		$this->coString = new FWDString();
		if ($poBox) {
			$this->coBox->setAttrLeft($poBox->getAttrLeft());
			$this->coBox->setAttrTop($poBox->getAttrTop());
			$this->coBox->setAttrHeight($poBox->getAttrHeight());
			$this->coBox->setAttrWidth($poBox->getAttrWidth());
		}
	}

	/**
	 * Cria o TabItem.
	 *
	 * <p>Cria o TabItem. Seta os eventos, o nome, as dimens�es.</p>
	 *
	 * <p>Cria o TabItem. Seta os eventos, o nome, as dimens�es e as cores.</p>
	 * @access public
	 * @param string $pobox Box que limita o tamanho do TabItem
	 * @param integer $piOffset Deslocamento da tab dentro do tabgroup
	 * @param integer $piActiveTabs N�mero de tabs ativas
	 * @param string $psElements Elementos que ser�o alvis dos eventos disparados pela tab
	 * @param string $psUrl Url
	 * @param integer $piLoadTabItemIndex �ndice do TabItem que deve ser carregado por default
	 * @return FWDTabItem TabItem
	 */
	public function createItem($poBox = null, $psName, $piOffset, $piActiveTabs, $psElements, $psUrl, $piLoadTabItemIndex) {

		$moViewButton = new FWDViewButton($poBox);

		if ($this->getObjFWDEvent()) {
			foreach ($this->getObjFWDEvent() as $moEvent)
			$moViewButton->setAttrEvent($moEvent);
		}

		if(!$this->getAttrDisplay())
		$moViewButton->setAttrDisplay("false");

		$moViewButton->setAttrDebug($this->getAttrDebug());
		$moViewButton->setAttrName($psName."_tab_".$piOffset);
		$moViewButton->setAttrClass("");

		$moStatic = new FWDStatic(new FWDBox(0, $poBox->getAttrTop(), $poBox->getAttrHeight(), $poBox->getAttrWidth()));
		$moStatic->setAttrName($psName."_tab_".$piOffset."_value");

		$class = $this->getAttrClass();
		$classes = explode(" ", $class);
		$newClasses = null;
		if ($piOffset == $piLoadTabItemIndex){
			foreach ($classes as $inClass){
				if($newClasses){
					$newClasses .= " ".$inClass."_selected";
				}else{
					$newClasses = $inClass."_selected";
				}
			}
		}else{
			foreach ($classes as $inClass){
				if($newClasses){
					$newClasses .= " ".$inClass."_notSelected";
				}else{
					$newClasses = $inClass."_notSelected";
				}
			}
		}
		$moStatic->setAttrClass($newClasses);

		$moStatic->setValue($this->getValue());
		$moStatic->setAttrHorizontal("center");
		$moStatic->setAttrVertical("middle");

		$moViewButton->addObjFWDView($moStatic);

		$msParameters = $piActiveTabs.' '.$psUrl;
		$msTargetElements = $psElements.' '.$moViewButton->getAttrName();
		$moEventOnClick = new FWDClientEvent("onclick", "changetab", '', "$piOffset $psUrl");

		$moEventOnMouseOver = new FWDClientEvent("onmouseover", "inverttabitemclass", $moStatic->getAttrName());
		$moEventOnMouseOut = new FWDClientEvent("onmouseout", "inverttabitemclass", $moStatic->getAttrName());

		$moViewButton->addEvent($moEventOnClick);
		$moViewButton->addEvent($moEventOnMouseOver);
		$moViewButton->addEvent($moEventOnMouseOut);

		return $moViewButton;
	}

	/**
	 * Seta a Url de redirecionamento.
	 *
	 * <p>Seta a Url de redirecionamento.</p>
	 * @access public
	 * @param string $psUrl Url de redirecionamento
	 */
	public function setAttrUrl($psUrl) {
		$this->csUrl = $psUrl;
	}

	/**
	 * Retorna a Url de redirecionamento.
	 *
	 * <p>Retorna a Url de redirecionamento.</p>
	 * @access public
	 * @return string Url de redirecionamento
	 */
	public function getAttrUrl() {
		return $this->csUrl;
	}
}
?>