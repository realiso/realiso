<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDMenuSeparator. Implementa um separador de itens para o menu de contexto.
 *
 * <p>Classe que implementa um separador de itens (div com suporte a eventos)
 * para o menu de contexto.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDMenuSeparator extends FWDMenuItem {

	/**
	 * Margem do separador
	 * @var integer
	 * @access protected
	 */
	protected $ciMargin = 5;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDMenuSeparator.</p>
	 * @access public
	 */
	public function __construct() {
		$this->csClass = get_class($this);
	}

	/**
	 * Cria o separador.
	 *
	 * <p>Cria o separador de itens do menu de contexto. Seta o nome,
	 * a box e o evento onClick.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do separador
	 * @param string $psName Elemento sobre o qual o evento onMouseover atuar�
	 * @param array $paIndex array com o �ndice deste menuitem e o total de menuitems
	 * @param string $psParent Elemento sobre o qual o evento onClick atuar�
	 */
	public function createMenuElement($poBox,$psName,$paIndex,$psParent) {
		$this->csName = $psName;
		$this->coBox = $poBox;
		$this->caIndex = $paIndex;
		$this->coEvent = new FWDClientEvent('onClick','hide',$psParent);
	}

	/**
	 * Desenha o cabe�alho do MenuSeparator.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o cabe�alho do MenuSeparator.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do MenuSeparator
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawHeader($poBox = null) {
		$msGlobalProperty = "name='{$this->csName}' id='{$this->csName}' ";
		$msStyle  = "left:{$this->coBox->getAttrLeft()};";
		$msStyle .= "top:{$this->coBox->getAttrTop()};";
		$msStyle .= "height:{$this->coBox->getAttrHeight()};";
		$msStyle .= "width:{$this->coBox->getAttrWidth()};";
		$msStyle .= "position:absolute;overflow:hidden;";

		$moEventOver = new FWDClientEvent('onMouseover','changecursor',$this->csName,'default');
		$msEvents  = "onmouseover='{$moEventOver->render()}' ";
		$msEvents .= "onclick='{$this->coEvent->render()}' ";

		return "\n<div {$msGlobalProperty} style='{$msStyle}' {$msEvents} >";
	}

	/**
	 * Desenha o corpo do MenuSeparator.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o corpo do MenuSeparator.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do MenuSeparator
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawBody($poBox = null) {
		$msGlobalProperty = "name='{$this->csName}_inner' id='{$this->csName}_inner' class='{$this->csClass}'";
		$msStyle  = "left:{$this->ciMargin};";
		$msStyle .= "top:".floor($this->coBox->getAttrHeight()/2).";";
		if (FWDWebLib::browserIsIE())
		$msStyle .= "height:2;";
		else
		$msStyle .= "height:0;";
		$msStyle .= "width:".($this->coBox->getAttrWidth()-(2*$this->ciMargin)).";";
		$msStyle .= "position:absolute;overflow:hidden;";

		return "\n<div {$msGlobalProperty} style='{$msStyle}' ></div>";
	}

	/**
	 * Desenha o rodap� do MenuSeparator.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o rodap� do MenuSeparator.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do MenuSeparator
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawFooter($poBox = null) {
		return parent::drawFooter();
	}

	/**
	 * Retorna o valor do MenuItem.
	 *
	 * <p>Retorna o valor do MenuItem.</p>
	 * @access public
	 * @return string Valor do MenuItem
	 */
	public function getValue(){
		return '';
	}

}
?>