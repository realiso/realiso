<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDColumn. Implementa Colunas.
 *
 * <p>Classe que implementa Colunas.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDColumn extends FWDViewGrid {

	/**
	 * Define a profundidade da coluna, para manipular sobreposi��o de elementos (zenith-index)
	 * @var integer
	 * @access protected
	 */
	protected $ciIndex;

	/**
	 * Define se eventos devem ser bloqueados ou n�o
	 * @var boolean
	 * @access protected
	 */
	protected $cbBlockEvent=false;

	/**
	 * Define o alias da coluna para a DBGrid
	 * @var string
	 * @access protected
	 */
	protected $csAlias="";

	/**
	 * Define se a coluna pode ordenar a grid
	 * @var boolean
	 * @access protected
	 */
	protected $cbCanOrder=false;

	/**
	 * Define o tipo de ordenamento da coluna
	 * @var string
	 * @access protected
	 */
	protected $csOrderBy = "";

	/**
	 * Define se a coluna deve exibir um tooltip com as informa��es da c�lula
	 * @var protected
	 * @access protected
	 */
	protected $cbToolTip=false;

	/**
	 * Define o n�mero m�ximo de caracteres a serem exibidos como tooltip.
	 * @var protected
	 * @access protected
	 */
	protected $cbToolTipMaxLength = null;

	/**
	 * Define O a coluna fonte da do conte�do da TT
	 * @var protected
	 * @access protected
	 */
	protected $ciSourceTT = '';
	/**
	 * Margem esquerda do �cone ascentende e descendente
	 * @var integer
	 * @access protected
	 */
	protected $ciIconColumnOrderMarginLeft =0;

	/**
	 * Margem do �cone at� o Static do �cone ascentende e descendente
	 * @var integer
	 * @access protected
	 */
	protected $ciIconColumnOrderMarginRight =0;


	/**
	 * Se a coluna pode ser editada ou n�o
	 * @var boolean
	 * @access protected
	 */
	protected $cbEdit = true;

	/**
	 * Armazena o conte�do do conteiner da coluna
	 * @var array
	 * @access protected
	 */
	protected $caContent = array();

	/**
	 * Se a coluna do corpo da grid deve ter nowrap ou n�o
	 * @var array
	 * @access protected
	 */
	protected $cbColNoWrap = true;

	/**
	 * Armazena o nome da grid que a coluna esta inserida
	 * @var string
	 * @access protected
	 */
	protected $csGridName = '';

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDColumn.</p>
	 * @access public
	 * @param FWDBox $poBox Box que limita o tamanho da coluna
	 */
	public function __construct($poBox = null) {
		parent::__construct($poBox);
	}

	/**
	 * Retorna o html do header da coluna.
	 *
	 * <p>Retorna o html do footer coluna.</p>
	 * @access public
	 * @param FWDBox $poBox Box que limita o tamanho da coluna
	 * @return string html do header da coluna
	 */
	public function drawHeader($poBox = null)
	{
		if((!$this->cbBlockEvent)){
			if(($this->cbCanOrder)&&($this->csAlias))
			{
				$moEvent = new FWDServerEvent();
				$moEvent->setAttrFunction("{$this->csGridName}_{$this->csAlias}");
				$moEvent->setAttrEvent("onClick");
				$this->addObjFWDEvent($moEvent);

				$moBeforeEventGrid = new FWDClientEvent();
				$moBeforeEventGrid->setAttrValue(" gobi('{$this->csGridName}').showGlass();");
				$moBeforeEventGrid->setAttrEvent("onClick");
				$this->addObjFWDEvent($moBeforeEventGrid);


			}else{
				if($this->getAttrCanOrder())	{
					$moEvent = new FWDServerEvent();
					$moEvent->setAttrFunction("{$this->csGridName}_{$this->csName}");
					$moEvent->setAttrEvent("onClick");
					$this->addObjFWDEvent($moEvent);

					$moBeforeEventGrid = new FWDClientEvent();
					$moBeforeEventGrid->setAttrValue(" gobi('{$this->csGridName}').showGlass();");
					$moBeforeEventGrid->setAttrEvent("onClick");
					$this->addObjFWDEvent($moBeforeEventGrid);
				}//else { } //a coluna n�o pode ordenar , n�o � necess�rio adicionar o evento na coluna
			}

			$msEvent=$this->getEvents();
		}else {// eventBlock = true - coluna nao deve ter eventos
			$msEvent='';
		}

		if(($this->csOrderBy=="+")||($this->csOrderBy=="-")){
			$msIcon = "";
			$moWebLib = FWDWebLib::getInstance();
			$csGFX = $moWebLib->getGfxRef();
			if($this->csOrderBy=="+")
			$msIcon=("{$csGFX}order_asc.gif");
			else
			$msIcon = ("{$csGFX}order_desc.gif");
			foreach($this->caContent as $moView){
				if(($moView instanceof FWDStatic)||($moView instanceof FWDStaticGrid)){
					$msLeftBlank = "";
					$msRightBlank = "";
					for($miI=1;$miI<=$this->ciIconColumnOrderMarginLeft;$miI++)
					$msLeftBlank .="&nbsp;";
					if($moView->getAttrHorizontal()=="right")
					for($miJ=1;$miJ<=$this->ciIconColumnOrderMarginRight;$miJ++)
					$msRightBlank .="&nbsp;";
					$moView->setFWDIconCode("{$msLeftBlank}<img src='{$msIcon}' class='FWDIcon'/>{$msRightBlank}");
				}
			}
		}
		else{}// a coluna nao deve ter o �cone de ordenamento

		$msStyle = $this->coBox->draw();
		$msStyle.= $this->cbDisplay ? '' : 'display:none;';
		$msClass = (($this->csClass)?"class='{$this->csClass}'":"");
		return "<div name='{$this->csName}' id='{$this->csName}' {$msClass} style='{$msStyle}' {$msEvent}>";
	}

	/**
	 * Retorna o html do body da coluna.
	 *
	 * <p>Retorna o html do body (do static e da string contida na coluna) coluna.</p>
	 * @access public
	 * @param FWDBox $poBox Box que limita o tamanho da coluna
	 * @return string html do body da coluna
	 */
	public function drawBody($poBox = null)
	{
		$msReturn="";
		$miMax = count ($this->caContent);
		for($miI = 0; $miI < $miMax; $miI++)
		$msReturn .= $this->caContent[$miI]->draw();

		return $msReturn;
	}

	/**
	 * Retorna o html do footer da coluna.
	 *
	 * <p>Retorna o html do footer da coluna.</p>
	 * @access public
	 * @param FWDBox $poBox Box que limita o tamanho da coluna
	 * @return string html do footer da coluna
	 */
	public function drawFooter($poBox = null)
	{
		if($this->csOrderBy == "+")
		$msOrderByValue="asc";
		elseif($this->csOrderBy == "-")
		$msOrderByValue="desc";
		else
		$msOrderByValue="";

		if($this->getAttrAlias()){
			return "</div> <input type=hidden name='{$this->csGridName}_{$this->getAttrAlias()}_orderby_value' id='{$this->csGridName}_{$this->getAttrAlias()}_orderby_value' value='{$msOrderByValue}' />";
		}else{
			return "</div> <input type=hidden name='{$this->csGridName}_{$this->getAttrName()}_orderby_value' id='{$this->csGridName}_{$this->getAttrName()}_orderby_value' value='{$msOrderByValue}' />";
		}
	}

	/**
	 * Seta o valor booleano do atributo BlockEvent.
	 *
	 * <p>Seta o valor booleano do atributo BlockEvent. Para bloquear eventos,
	 * o atributo deve conter o valor booleano TRUE. Para permitir eventos,
	 * deve conter o valor booleano FALSE.</p>
	 * @access public
	 * @param string $psBlockEvent Define se eventos devem ser bloqueados ou n�o
	 */
	public function setAttrEventBlock($psBlockEvent) {
		$this->cbBlockEvent = FWDWebLib::attributeParser($psBlockEvent,array("true"=>true, "false"=>false),"psBlockEvent");
	}

	/**
	 * Seta o valor do alias do atributo csAlias.
	 *
	 * <p>Seta o valor string do atributo Alias. Utilizado para a DBGrid para associar uma coluna
	 * da grid a uma coluna da consulta ao BD.</p>
	 * @access public
	 * @param string $psAlias Define o Alias da Coluna
	 */
	public function setAttrAlias($psAlias) {
		$this->csAlias = $psAlias;
	}

	/**
	 * Retorna a largura da coluna.
	 *
	 * <p>Retorna a largura da coluna (que � a mesma da box), em pixels.</p>
	 * @access public
	 * @return integer Largura da coluna
	 */
	public function getAttrWidth() {
		return $this->coBox->getAttrWidth();
	}

	/**
	 * Retorna o Alias da coluna.
	 *
	 * <p>Retorna o Alias da coluna , utilizado para a DBGrid , para associar uma coluna da grid a uma coluna
	 * de uma consulta ao BD.</p>
	 * @access public
	 * @return string Alias da coluna
	 */
	public function getAttrAlias() {
		return $this->csAlias;
	}

	/**
	 * Retorna o valor booleano do artributo EventBlock.
	 *
	 * <p>Retorna o valor booleano do artributo EventBlock.</p>
	 * @access public
	 * @return boolean Indica se eventos devem ser bloqueados ou n�o
	 */
	public function getAttrEventBlock() {
		return $this->cbBlockEvent;
	}

	/**
	 * Retorna se a coluna pode ou n�o ordenar.
	 *
	 * <p>Retorna se a coluna pode ou n�o ordenar o conte�do da grid.</p>
	 * @access public
	 * @return boolean Se a coluna pode ordenar ou n�o
	 */
	public function getAttrCanOrder(){
		return $this->cbCanOrder;
	}

	/**
	 * Seta o valor do atributo de CanOrder da coluna.
	 *
	 * <p>Seta o valor do atributo canOrder da coluna, utilizando somente se a grid for uma instancia de DBGrid.</p>
	 * @access public
	 * @param string $pbCanOrder Define se a coluna pode ou nao ordenar a grid
	 */
	public function setAttrCanOrder($pbCanOrder){
		$this->cbCanOrder = FWDWebLib::attributeParser($pbCanOrder,array("true"=>true, "false"=>false),"pbCanOrder");
	}

	/**
	 * Seta o valor do atributo de OrderBy da coluna.
	 *
	 * <p>Seta o valor do atributo OrderBy da coluna para ser utilizado para ordenar a dbGrid. tipos de
	 * ordenamentos possiveis (asc -> ascendente , desc -> descendente)</p>
	 * @access public
	 * @param string $psOrderBy Define o tipo de ordenamento da coluna da dbGrid
	 */
	public function setOrderBy($psOrderBy){
		$psOrderBy = trim(strtolower($psOrderBy));
		if(($psOrderBy == "asc")||($psOrderBy=='+'))
		$this->csOrderBy = "+";
		elseif(($psOrderBy=="desc")||($psOrderBy=="-"))
		$this->csOrderBy = "-";
		elseif($psOrderBy=="")
		$this->csOrderBy = "";
		else
		trigger_error("Trying to use a wrong orderBy, using '{$psOrderBy}' not ['asc','desc']",E_USER_ERROR);
	}

	/**
	 * Retorna o tipo de ordenamento da coluna.
	 *
	 * <p>Retorna o tipo de ordenamento da coluna da grid.</p>
	 * @access public
	 * @return string Tipo de ordenamento da coluna da grid
	 */
	public function getOrderBy()
	{
		return $this->csOrderBy;
	}

	/**
	 * Seta o valor do atributo ToolTip da coluna.
	 *
	 * <p>Seta o valor do atributo ToolTip da coluna para, caso necess�rio,
	 * exibir um tooltip com as informa��es da c�lula no evento onMouseOver.</p>
	 * @access public
	 * @param string $psToolTip Define se a coluna deve exibir um tooltip com as informa��es da c�lula
	 */
	public function setAttrToolTip($psToolTip){
		$this->cbToolTip = FWDWebLib::attributeParser($psToolTip,array("true" => true, "false" => false),"psToolTip");
	}

	/**
	 * Retorna o valor do atributo ToolTip da coluna.
	 *
	 * <p>Retorna o valor do atributo ToolTip da coluna para, caso necess�rio,
	 * exibir um tooltip com as informa��es da c�lula no evento onMouseOver.</p>
	 * @access public
	 * @return boolean Indica se a coluna deve exibir um tooltip ou n�o
	 */
	public function getAttrToolTip(){
		return $this->cbToolTip;
	}

	public function setAttrToolTipMaxLength($psToolTipMaxLength){
		$this->cbToolTipMaxLength = $psToolTipMaxLength;
	}

	public function getAttrToolTipMaxLength(){
		return $this->cbToolTipMaxLength;
	}

	/**
	 * Seta o valor do atributo ToolTip da coluna.
	 *
	 * <p>Seta o valor do atributo source da ToolTip da coluna.</p>
	 * @access public
	 * @param string $piToolTipSource Seta o valor do atributo source da ToolTip da coluna.
	 */
	public function setSourceToolTip($piToolTipSource){
		$this->ciSourceTT = $piToolTipSource;
	}

	/**
	 * Retorna o source do tooltip da coluna.
	 *
	 * <p>Retorna o source do tooltip da coluna da grid.</p>
	 * @access public
	 * @return integer Indica qual o source do tooltip da coluna da grid
	 */
	public function getSourceToolTip(){
		return $this->ciSourceTT;
	}

	/**
	 * Seta o valor da Margem a esquerda do �cone de ordenamento.
	 *
	 * <p>Seta o valor da margem � esquerda do �cone de ordenamento do header das colunas da grid</p>
	 * @access public
	 * @param string $piIconMarginLeft Margem a esquerda do �cone de ordenamento da coluna
	 */
	public function setAttrIconColumnOrderMarginLeft($piIconMarginLeft){
		$this->ciIconColumnOrderMarginLeft = $piIconMarginLeft;
	}

	/**
	 * Seta a margem � direita do �cone de ordenamento.
	 *
	 * <p>Seta o valor da margem � direita (entre o �cone e o Static) do �cone de ordenamento do header das colunas da grid</p>
	 * @access public
	 * @param string $piIconMarginLeft Margem a direita do �cone de ordenamento da coluna
	 */
	public function setAttrIconColumnOrderMarginRight($piIconMarginRight){
		$this->ciIconColumnOrderMarginRight = $piIconMarginRight;
	}

	/**
	 * Retorna A margem � esquerda do Icone de Ordenamento do header das colunas da grid.
	 *
	 * <p>Retorna A margem � a esquerda do Icone de Ordenamento do header das colunas da grid..</p>
	 * @access public
	 * @return integer Margem � esquerda e o Static do header da coluna
	 */
	public function getAttrIconColumnOrderMarginLeft(){
		return $this->ciIconColumnOrderMarginLeft;
	}

	/**
	 * Retorna A margem � direita do Icone de Ordenamento do header das colunas da grid.
	 *
	 * <p>Retorna A margem � direita do Icone de Ordenamento do header das colunas da grid..</p>
	 * @access public
	 * @return integer Margem a direita do icone de ordenamento e o Static do header da coluna
	 */
	public function getAttrIconColumnOrderMarginRight(){
		return $this->ciIconColumnOrderMarginRight;
	}

	/**
	 * Seta se a coluna pode ser editada.
	 *
	 * <p>Seta se a coluna pode ser exibida pelas preferencias do usu�rio</p>
	 * @access public
	 * @param string $pbEdit se a coluna pode ser editada pelo usu�rio
	 */
	public function setAttrEdit($pbEdit){
		$this->cbEdit = FWDWebLib::attributeParser($pbEdit,array("true"=>true, "false"=>false),"pbEdit");
	}

	/**
	 * Retorna se a coluna pode ser Editada.
	 *
	 * <p>Retorna se a coluna pode ser editada pela grid quando o usu�rio esta mudando as
	 * preferencias de visualiza��o da grid.</p>
	 * @access public
	 * @return boolean Indica se a coluna pode ser editada
	 */
	public function getAttrEdit(){
		return $this->cbEdit;
	}

	/**
	 * Seta o nome da grid que a coluna pertence.
	 *
	 * <p>Seta o nome da grid que a coluna pertense, utilizado para formar o
	 * nome do evento de ordenamento das colunas, pois pode haver duas grids
	 * na mesma p�gina com colunas com o mesmo alias</p>
	 * @access public
	 * @param string $psGridName se a coluna pode ser editada pelo usu�rio
	 */
	public function setGridName($psGridName){
		$this->csGridName = $psGridName;
	}

	/**
	 * Retorna o nome da grid que a coluna pertence.
	 *
	 * <p>Retorna o nome da grid que a coluna pertence.</p>
	 * @access public
	 * @return string Nome da grid
	 */
	public function getGridName(){
		return $this->csGridName;
	}

	/**
	 * Seta se os statics do corpo da grid da coluna devem ter nowrap.
	 *
	 * <p>Seta se os statics do corpo da grid da coluna devem ter nowrap</p>
	 * @access public
	 * @param boolean $pbColNoWrap Margem a direita do �cone de ordenamento da coluna
	 */
	public function setAttrColNoWrap($pbColNoWrap){
		$this->cbColNoWrap = FWDWebLib::attributeParser($pbColNoWrap,array("true"=>true, "false"=>false),"pbColNoWrap");
	}

	/**
	 * Retorna se os statics do corpo da grid da coluna devem ter nowrap.
	 *
	 * <p>Retorna se os statics do corpo da grid da coluna devem ter nowrap.</p>
	 * @access public
	 * @return boolean Indica se o nowrap deve ser aplicado nos statics do corpo da grid
	 */
	public function getAttrColNoWrap(){
		return $this->cbColNoWrap;
	}

	/**
	 * Adiciona um Drawing.
	 *
	 * <p>Adiciona um Drawing no container.</p>
	 * @access public
	 * @param FWDDrawing $poDrawing View
	 */
	public function addObjFWDDrawing(FWDDrawing $poDrawing){
		$this->caContent[] = $poDrawing;
	}

	/**
	 * Retorna todas as views do container.
	 *
	 * <p>M�todo para retornar todas as views do container.</p>
	 * @access public
	 * @return array Views
	 */
	public function getContent(){
		return $this->caContent;
	}

	/**
	 * Retorna o valor do static da coluna.
	 *
	 * <p>Retorna o valor do static da coluna da grid.</p>
	 * @access public
	 * @return boolean Indica se a coluna pode ser exibida
	 */
	public function getValue($poBox = null)
	{
		$msReturn="";
		for($miI = 0; $miI < count ($this->caContent); $miI++) {
			if(($this->caContent[$miI] instanceof FWDStatic)||($this->caContent[$miI] instanceof FWDStaticGrid))
			$msReturn .= $this->caContent[$miI]->getObjFWDString()->getAttrString();
		}
		return $msReturn;
	}

	/**
	 * Retorna um array com todos sub-elementos do container
	 *
	 * <p>Retorna um array contendo todos elementos contidos no container ou em
	 * seus descendentes.
	 * OBS: O array pode conter elementos repetidos, caso haja algum ItemController
	 * que perten�a ao Container e a um Controller simultaneamente.
	 * </p>
	 * @access public
	 * @return array Array de sub-elementos
	 */
	public function collect() {
		$maOut = array();
		$maOut = array_merge($maOut,parent::collect());
		foreach ($this->caContent as $moView) {
			if ($moView instanceof FWDCollectable) {
				$maItems = $moView->collect();
				if (count($maItems)>0)
				$maOut = array_merge($maOut,$maItems);
			}
			else
			$maOut[] = $moView;
		}
		return $maOut;
	}
}
?>