<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,	technics, statements, and computer programs
 * contain	unpublished	proprietary information of	Axur Communications,
 * Inc.,	and are	protected	by applied	copyright law.	They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without	the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes	codigos,	tecnicas, tratados e	programas de computador contem
 * informacao proprietaria	nao publicada pela Axur Communications, Inc.,
 * e sao	protegidas pelas leis	de direito registrado.	Essas, nao podem
 * ser dispostas	a terceiros, copiadas ou	duplicadas de qualquer forma,
 * no	todo ou	em parte,	sem	consentimento	previo	escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDStatic. Implementa texto est�tico.
 *
 * <p>Classe que implementa texto est�tico (html tag 'span').</p>
 * @package FWD5
 * @subpackage view
 */
class FWDStatic extends FWDView implements FWDPostValue {

	/**
	 * Define se deve existir uma barra de rolagem no caso de um overflow
	 * @var boolean
	 * @access protected
	 */
	protected $cbScrollbar = false;


	/**
	 * Define o tipo de alinhamento horizontal do texto contido no static
	 * @var string
	 * @access protected
	 */
	protected $csHorizontal = "left";

	/**
	 * Define o tipo de alinhamento vertical do texto contido no static
	 * @var boolean
	 * @access protected
	 */
	protected $csVertical = "top";

	/**
	 * Define a margem � esquerda do static
	 * @var boolean
	 * @access protected
	 */
	protected $ciMarginLeft=0;


	/**
	 * Define a margem � direita do static
	 * @var boolean
	 * @access protected
	 */
	protected $ciMarginRight=0;


	/**
	 * Define a margem de top do static
	 * @var boolean
	 * @access protected
	 */
	protected $ciMarginTop = 0;

	/**
	 * Define a margem de bottom do static
	 * @var boolean
	 * @access protected
	 */
	protected $ciMarginBottom=0;

	/**
	 * Define se o static deve ter nowrap ou n�o
	 * @var boolean
	 * @access protected
	 */
	protected $cbNoWrap = true;

	/**
	 * Define se o text associado com o static deve ser preenchido
	 * @var boolean
	 * @access protected
	 */
	protected $cbMustFill = false;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDStatic.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do texto est�tico
	 */
	public function __construct($poBox = null) {
		parent::__construct($poBox);
	}

	/**
	 * Desenha o cabe�alho do texto est�tico.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o cabe�alho do texto est�tico.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do texto est�tico
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawHeader($poBox = null) {
		$msScrollbar = ($this->getAttrScrollbar()?"overflow:auto;":"overflow:hidden;");
		$this->setAttrTabIndex(0);

		//recalcula as dimen��es da box de acordo com a margem definida
		$moBox=$this->getObjFWDBox();

		//clone da box para evitar o efeito "mem�ria" nas altera��es das dimen��es da box do static
		$moAuxBox = clone $moBox;
		if($moBox)
		{
			//margem a esquerda
			$moBox->setAttrWidth($moBox->getAttrWidth() - $this->ciMarginLeft);
			$moBox->setAttrLeft($moBox->getAttrLeft() + $this->ciMarginLeft);
				
			//margem a direita
			$moBox->setAttrWidth($moBox->getAttrWidth() - $this->ciMarginRight);
				
			//margem Top
			$moBox->setAttrTop($moBox->getAttrTop() + $this->ciMarginTop);
			$moBox->setAttrHeight($moBox->getAttrHeight() - $this->ciMarginTop);
				
			//margem Bottom
			//$moBox->setAttrTop($moBox->getAttrTop() + $this->ciMarginBottom);
			$moBox->setAttrHeight($moBox->getAttrHeight() - $this->ciMarginBottom);
		}
		else
		{
			//trigger error
		}
		$msTDclass="";
		if($this->cbNoWrap==true)
		$msTDclass .= "class='FWDTD'";
			
		$msAlign = "";
		$msAlign.= " align=\"".$this->csHorizontal."\" valign=\"" . $this->csVertical . "\" ";
		$msStyle = $this->getStyle();
		$msStyle = "style='{$msStyle}{$msScrollbar}'";
		$msAttrDefault = $this->getGlobalProperty();

		$msOut = "\n<table {$msAttrDefault} {$this->getEvents()} {$msStyle} > <tr> <td id='".$this->csName."_td' {$msAlign} {$msTDclass}>";

		$this->setObjFWDBox($moAuxBox);
		return $msOut;
	}

	/**
	 * Desenha o corpo do texto est�tico.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o corpo do texto est�tico.</p>
	 * @access public
	 *
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawBody() {
		$msValue = $this->getValue();
		if ($this->cbMustFill)
		$msValue = "<font color='red'>* </font>" . $msValue;
		return $msValue;
	}

	/**
	 * Desenha o rodap� do texto est�tico.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o rodap� do texto est�tico.</p>
	 * @access public
	 *
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawFooter() {
		$msReturn ="";
		$msReturn .= "</td></tr> </table> \n";
		if($this->csName)
		$msReturn .=" <script language='javascript'>gebi('{$this->csName}').object = new FWDStatic('{$this->csName}'); </script> ";

		return $msReturn;
	}

	/**
	 * Atribui valor � vari�vel value.
	 *
	 * <p>Atribui valor � vari�vel value, devendo retirar espa�os em branco
	 * no inicio e/ou no fim da string.</p>
	 * @access public
	 * @param string $psValue Valor a ser atribu�do
	 */
	public function setAttrValue($psValue) {
		$this->setValue(trim($psValue));
	}

	/**
	 * Concatena um valor na string.
	 *
	 * <p>M�todo para concatenar um valor na string.</p>
	 * @access public
	 * @param string $psValue Valor a ser concatenado
	 */
	public function concatValue($psValue) {
		$this->setValue(" " . trim($psValue), false);
	}

	/**
	 * Seta o valor booleano do atributo Scrollbar.
	 *
	 * <p>Seta o valor booleano do atributo que define se deve existir
	 * uma barra de rolagem no caso de um overflow. Para a barra de rolagem
	 * existir, o atributo deve conter o valor booleano TRUE. Para n�o existir,
	 * o atributo deve conter o valor booleano FALSE.</p>
	 * @access public
	 * @param string $pbScrollbar Define se deve existir uma barra de rolagem no caso de um overflow
	 */
	public function setAttrScrollbar($pbScrollbar) {
		$this->cbScrollbar = FWDWebLib::attributeParser($pbScrollbar,array("true"=>true, "false"=>false),"pbScrollbar");
	}

	/**
	 * Seta o valor booleano do atributo MustFill.
	 *
	 * <p>Seta o valor booleano do atributo que define se o campo text
	 * associado ao static deve ser preenchido.</p>
	 * @access public
	 * @param boolean $pbMustFill Define se o text associado com o static deve ser preenchido
	 */
	public function setAttrMustFill($pbMustFill) {
		$this->cbMustFill = FWDWebLib::attributeParser($pbMustFill,array("true"=>true, "false"=>false),"pbMustFill");
	}

	/**
	 * Seta o tipo de alinhamento horizontal do static.
	 *
	 * <p>Seta o tipo de alinhamento horizontal do static (valores possiveis -> left, center, right , justify)</p>
	 * @access public
	 * @param string $psHorizontal Define o tipo de alinhamento horizontal do static
	 */
	function setAttrHorizontal($psHorizontal)
	{
		$this->csHorizontal= FWDWebLib::attributeParser($psHorizontal,array("left","center","right","justify"),"psHorizontal");

	}

	/**
	 * Seta o tipo de alinhamento vertical do static.
	 *
	 * <p>Seta o tipo de alinhamento vertical do static (valores possiveis -> top, middle, bottom)</p>
	 * @access public
	 * @param string $psHorizontal Define o tipo de alinhamento horizontal do static
	 */
	function setAttrVertical($psVertical)
	{
		$this->csVertical = FWDWebLib::attributeParser($psVertical,array("top","middle","bottom"),"psVertical");
	}

	/**
	 * Retorna o valor do alinhamento horizontal.
	 *
	 * <p>Retorna o valor do alinhamento horizontal do static</p>
	 * @access public
	 * @return string Alinhamento horizontal do static
	 */
	function getAttrVertical()
	{
		return $this->csVertical;
	}

	/**
	 * Retorna o valor do alinhamento vertical.
	 *
	 * <p>Retorna o valor do alinhamento vertical do static</p>
	 * @access public
	 * @return string Alinhamento vertical do static
	 */
	function getAttrHorizontal()
	{
		return $this->csHorizontal;
	}


	/**
	 * Seta a margem left do static.
	 *
	 * <p>Seta o valor da margem left do static</p>
	 * @access public
	 * @param string $psMargin Define a margem left do static
	 */
	function setAttrMarginLeft($piMarginLeft)
	{
		if($piMarginLeft>=0)
		$this->ciMarginLeft = $piMarginLeft;
		else
		trigger_error("MarginLeft must be a positive integer, got ={$piMarginLeft}",E_USER_ERROR);

	}

	/**
	 * Seta a margem right do static.
	 *
	 * <p>Seta o valor da margem right do static</p>
	 * @access public
	 * @param string $psMargin Define a margem right do static
	 */
	function setAttrMarginRight($piMarginRight)
	{
		if($piMarginRight>=0)
		$this->ciMarginRight = $piMarginRight;
		else
		trigger_error("MarginRight must be a positive integer, got ={$piMarginRight}",E_USER_ERROR);
	}

	/**
	 * Seta a margem top do static.
	 *
	 * <p>Seta o valor da margem top do static</p>
	 * @access public
	 * @param string $psMargin Define a margem top do static
	 */
	function setAttrMarginTop($piMarginTop)
	{
		if($piMarginTop>=0)
		$this->ciMarginTop = $piMarginTop;
		else
		trigger_error("MarginTop must be a positive integer, got ={$piMarginTop}",E_USER_ERROR);
	}

	/**
	 * Seta a margem bottom do static.
	 *
	 * <p>Seta o valor da margem bottom do static</p>
	 * @access public
	 * @param string $psMargin Define a margem bottom do static
	 */
	function setAttrMarginBottom($piMarginBottom)
	{
		if($piMarginBottom>=0)
		$this->ciMarginBottom = $piMarginBottom;
		else
		trigger_error("MarginBottom must be a positive integer, got ={$piMarginBottom}",E_USER_ERROR);
	}

	/**
	 * Retorna o valor da margem left do static.
	 *
	 * <p>Retorna o valor da margem left do static</p>
	 * @access public
	 * @return integer Valor da margem left do static
	 */
	function getAttrMarginLeft()
	{
		return $this->ciMarginLeft;
	}

	/**
	 * Retorna o valor da margem top do static.
	 *
	 * <p>Retorna o valor da margem top do static</p>
	 * @access public
	 * @return integer Valor da margem top do static
	 */
	function getAttrMarginTop()
	{
		return $this->ciMarginTop;
	}

	/**
	 * Retorna o valor da margem right do static.
	 *
	 * <p>Retorna o valor da margem right do static</p>
	 * @access public
	 * @return integer Valor da margem right do static
	 */
	function getAttrMarginRight()
	{
		return $this->ciMarginRight;
	}

	/**
	 * Retorna o valor da margem bottom do static.
	 *
	 * <p>Retorna o valor da margem bottom do static</p>
	 * @access public
	 * @return integer Valor da margem bottom do static
	 */
	function getAttrMarginBottom()
	{
		return $this->ciMarginBottom;
	}

	/**
	 * Retorna o valor booleano do atributo Scrollbar.
	 *
	 * <p>Retorna o valor booleano TRUE se deve existir uma barra de rolagem
	 * no caso de um overflow. Retorna FALSE caso contr�rio.</p>
	 * @access public
	 * @return boolean Indica se deve existir uma barra de rolagem no caso de um overflow
	 */
	public function getAttrScrollbar() {
		return $this->cbScrollbar;
	}

	/**
	 * Retorna o valor booleano do atributo MustFill.
	 *
	 * <p>M�todo para retornar o valor booleano do atributo MustFill.</p>
	 * @access public
	 * @return boolean Indica se deve existir uma barra de rolagem no caso de um overflow
	 */
	public function getAttrMustFill() {
		return $this->cbMustFill;
	}

	/**
	 * Atribui valor NoEscape ao objeto string do Static.
	 *
	 * <p>Atribui valor NOEscape ao objeto FWDString do static para evitar que sejam escapados os "'" da string.</p>
	 * @access public
	 * @param boolean $pbNoEscape valor do noEscape ("true" ou "false")
	 */

	public function setAttrStringNoEscape($pbNoEscape) {
		$this->coString->setAttrNoEscape($pbNoEscape);
	}

	/**
	 * Adiciona um link ao valor do Static.
	 *
	 * <p>M�todo para adicionar um link ao valor do Static. Insere
	 * (ou concatena, caso o objeto coString ja exista) um link no valor
	 * do Static (objeto FWDString).</p>
	 * @access public
	 * @param FWDLink $poLink Link a ser adicionado (concatenado) ao Static
	 */
	public function setObjFWDLink(FWDLink $poLink) {
		$this->coString->setAttrNoEscape(true);
		if (isset($this->coString)) {
			$this->coString->setValue($poLink->draw());
		}
		else {
			$this->coString = $poLink->draw();
		}
	}

	public function setFWDIconCode($psIcon) {
		$this->coString->setAttrNoEscape(true);
		if (isset($this->coString)) {
			$this->coString->setValue($psIcon);
				
		}
	}

	public function setAttrNoWrap($pbNoWrap){
		$this->cbNoWrap = FWDWebLib::attributeParser($pbNoWrap,array("true"=>true, "false"=>false),"pbNoWrap");
	}

	public function getAttrNoWrap(){
		return $this->cbNoWrap;
	}

	/**
	 * Substitui uma string por outra no valor do static.
	 *
	 * <p>M�todo para substituir uma string por outra no valor do static.</p>
	 * @access public
	 * @param string $psSearch String que deve ser retirada
	 * @param string $psReplace String que deve ser inserida
	 */
	public function replace($psSearch, $psReplace) {
		$msNewValue = str_replace($psSearch, $psReplace, $this->getValue());
		$this->setValue($msNewValue);
	}
}
?>