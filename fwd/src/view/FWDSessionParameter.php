<?
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDSessionParameter.
 *
 * <p>Classe utilizada para receber, via xml, par�metros pela sess�o.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDSessionParameter {

	/**
	 * Id da sess�o
	 * @var string
	 * @access protected
	 */
	protected $csSessionId = "";

	/**
	 * Nome do atributo
	 * @var string
	 * @access protected
	 */
	protected $csAttribute = "";

	/**
	 * Valor do atributo
	 * @var string
	 * @access protected
	 */
	protected $csValue = "";

	/**
	 * Nome do objeto
	 * @var string
	 * @access protected
	 */
	protected $csName = "";

	/**
	 * Autoclean
	 * @var boolean
	 * @access protected
	 */
	protected $cbAutoClean = false;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDParamenter.</p>
	 * @access public
	 */
	public function __construct() {}

	/**
	 * Atribui o nome do par�metro.
	 *
	 * @access public
	 * @param string $psAttribute Nome do atributo
	 */
	public function setAttrAttribute($psAttribute) {
		$this->csAttribute = $psAttribute;
	}

	/**
	 * Seta o id da sess�o.
	 *
	 * @access public
	 * @param string $psSessionId Id da sess�o
	 */
	public function setAttrSessionId($psSessionId) {
		$this->csSessionId = $psSessionId;
	}

	/**
	 * Seta o atributo autoclean. Ou seja, limpa o valor do atributo ap�s utiliz�-lo.
	 *
	 * @access public
	 * @param string $pbAutoClean Nome do atributo
	 */
	public function setAttrAutoClean($pbAutoClean) {
		$this->cbAutoClean = FWDWebLib::attributeParser($pbAutoClean,array("true"=>true, "false"=>false),"pbAutoClean");
	}

	/**
	 * Atribui valor � vari�vel value.
	 *
	 * <p>Atribui valor � vari�vel value, devendo retirar espa�os em branco
	 * no inicio e/ou no fim da string.</p>
	 * @access public
	 * @param string $psValue Valor a ser atribu�do
	 */
	public function setValue($psValue) {
		$this->csValue = trim($psValue);
	}

	/**
	 * Atribui nome ao objeto.
	 *
	 * @access public
	 * @param string $psName Nome do objeto
	 */
	public function setAttrName($psName) {
		$this->csName = $psName;
	}

	/**
	 * Retorna o nome do atributo.
	 *
	 * @access public
	 * @return string Nome do par�metro
	 */
	public function getAttrAttribute() {
		return $this->csAttribute;
	}

	/**
	 * Retorna o id da sess�o.
	 *
	 * @access public
	 * @return string Id da sess�o
	 */
	public function getAttrSessionId() {
		return $this->csSessionId;
	}

	/**
	 * Retorna o valor da vari�vel value.
	 *
	 * @access public
	 * @return string Valor do objeto
	 */
	public function getValue() {
		return $this->csValue;
	}

	/**
	 * Retorna o nome do objeto.
	 *
	 * @access public
	 * @return string Nome do objeto
	 */
	public function getAttrName() {
		return $this->csName;
	}

	/**
	 * Retorna o estado do atributo autoclean.
	 *
	 * @access public
	 * @param string $pbAutoClean Nome do atributo
	 */
	public function getAttrAutoClean() {
		return $this->cbAutoClean;
	}

	/**
	 * Pega o valor do atributo passado por sess�o.
	 *
	 * @access public
	 */
	public function execute() {
		$moSession = FWDWebLib::getInstance()->getSessionById($this->csSessionId);
		if ($moSession->attributeExists($this->csAttribute)) {
			$msGetFunction = "getAttr" . $this->csAttribute;
			$this->csValue = $moSession->{$msGetFunction}();
			if ($this->cbAutoClean) $moSession->deleteAttribute($this->csAttribute);
		}
	}
}
?>