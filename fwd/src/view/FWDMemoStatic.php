<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,	technics, statements, and computer programs
 * contain	unpublished	proprietary information of	Axur Communications,
 * Inc.,	and are	protected	by applied	copyright law.	They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without	the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes	codigos,	tecnicas, tratados e	programas de computador contem
 * informacao proprietaria	nao publicada pela Axur Communications, Inc.,
 * e sao	protegidas pelas leis	de direito registrado.	Essas, nao podem
 * ser dispostas	a terceiros, copiadas ou	duplicadas de qualquer forma,
 * no	todo ou	em parte,	sem	consentimento	previo	escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDStatic. Implementa texto est�tico.
 *
 * <p>Classe que implementa texto est�tico (html tag 'span').</p>
 * @package FWD5
 * @subpackage view
 */
class FWDMemoStatic extends FWDView{

	/**
	 * Define se deve existir uma barra de rolagem no caso de um overflow
	 * @var boolean
	 * @access protected
	 */
	protected $cbScrollbar = true;

	/**
	 * Define o tipo de alinhamento horizontal do texto contido no static
	 * @var string
	 * @access protected
	 */
	protected $csHorizontal = "left";

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDStatic.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do texto est�tico
	 */
	public function __construct($poBox = null) {
		parent::__construct($poBox);
	}

	/**
	 * Desenha o cabe�alho do texto est�tico.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o cabe�alho do texto est�tico.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do texto est�tico
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawHeader($poBox = null) {
		$msScrollbar = ($this->getAttrScrollbar()?"overflow:auto;":"overflow:hidden;");
		$msAlign = "";
		$msAlign.= " align=\"".$this->csHorizontal."\" ";

		$msStyle = $this->getStyle();
		$msStyle = "style='{$msStyle}{$msScrollbar}'";
		$msAttrDefault = $this->getGlobalProperty();

		$msOut = "<div {$msAttrDefault} {$this->getEvents()} {$msStyle} align=\"".$this->csHorizontal."\">";

		return $msOut;
	}

	/**
	 * Desenha o corpo do texto est�tico.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o corpo do texto est�tico.</p>
	 * @access public
	 *
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawBody() {
		//return trim(str_replace("\n",' ',$this->getValue()));
		return trim($this->getValue());
	}

	/**
	 * Retorna o valor do elemento.
	 *
	 * <p>M�todo para retornar o valor do elemento.</p>
	 * @access public
	 * @return string Valor do elemento
	 */
	public function getValue() {
		$msReturn = $this->coString->getAttrString();
		if($this->cbScrollbar==true)
		$msReturn = str_replace("\n",'<br>',$msReturn);
			
		return $msReturn;
	}

	/**
	 * Desenha o rodap� do texto est�tico.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o rodap� do texto est�tico.</p>
	 * @access public
	 *
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawFooter() {
		if($this->csName)
		return "</div> <script language='javascript'>gebi('{$this->csName}').object = new FWDMemoStatic('{$this->csName}'); </script> ";
		else
		return "</div>";
	}

	/**
	 * Atribui valor � vari�vel value.
	 *
	 * <p>Atribui valor � vari�vel value, devendo retirar espa�os em branco
	 * no inicio e/ou no fim da string.</p>
	 * @access public
	 * @param string $psValue Valor a ser atribu�do
	 */
	public function setAttrValue($psValue) {
		$this->setValue(trim($psValue));
	}

	/**
	 * Seta o valor booleano do atributo Scrollbar.
	 *
	 * <p>Seta o valor booleano do atributo que define se deve existir
	 * uma barra de rolagem no caso de um overflow. Para a barra de rolagem
	 * existir, o atributo deve conter o valor booleano TRUE. Para n�o existir,
	 * o atributo deve conter o valor booleano FALSE.</p>
	 * @access public
	 * @param string $pbScrollbar Define se deve existir uma barra de rolagem no caso de um overflow
	 */
	public function setAttrScrollbar($pbScrollbar) {
		$this->cbScrollbar = FWDWebLib::attributeParser($pbScrollbar,array("true"=>true, "false"=>false),"pbScrollbar");
	}

	/**
	 * Atribui valor NoEscape ao objeto string do Static.
	 *
	 * <p>Atribui valor NOEscape ao objeto FWDString do static para evitar que sejam escapados os "'" da string.</p>
	 * @access public
	 * @param boolean $pbNoEscape valor do noEscape ("true" ou "false")
	 */

	public function setAttrStringNoEscape($pbNoEscape) {
		$this->coString->setAttrNoEscape($pbNoEscape);
	}

	/**
	 * Retorna o valor booleano do atributo Scrollbar.
	 *
	 * <p>Retorna o valor booleano TRUE se deve existir uma barra de rolagem
	 * no caso de um overflow. Retorna FALSE caso contr�rio.</p>
	 * @access public
	 * @return boolean Indica se deve existir uma barra de rolagem no caso de um overflow
	 */
	public function getAttrScrollbar() {
		return $this->cbScrollbar;
	}

	/**
	 * Seta o tipo de alinhamento horizontal do static.
	 *
	 * <p>Seta o tipo de alinhamento horizontal do static (valores possiveis -> left, center, right , justify)</p>
	 * @access public
	 * @param string $psHorizontal Define o tipo de alinhamento horizontal do static
	 */
	function setAttrHorizontal($psHorizontal)
	{
		$this->csHorizontal= FWDWebLib::attributeParser($psHorizontal,array("left","center","right","justify"),"psHorizontal");

	}

	/**
	 * Retorna o valor do alinhamento vertical.
	 *
	 * <p>Retorna o valor do alinhamento vertical do static</p>
	 * @access public
	 * @return string Alinhamento vertical do static
	 */
	function getAttrHorizontal()
	{
		return $this->csHorizontal;
	}
}
?>