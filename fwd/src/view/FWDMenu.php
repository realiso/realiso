<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDMenu. Implementa Menus de Contexto.
 *
 * <p>Classe que implementa menus de contexto na framework FWD.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDMenu extends FWDView {

	/**
	 * Menu de contexto (array de itens (objetos): FWDMenuItem, FWDMenuSeparator)
	 * @var array
	 * @access protected
	 */
	protected $caItems = array();

	/**
	 * Elementos sobre os quais o menu de contexto atuar�
	 * @var string
	 * @access protected
	 */
	protected $csElements = "";

	/**
	 * SubTarget do Menu de contexto (lines / headers)
	 * @var string
	 * @access protected
	 */
	protected $csSubElements = "";

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDMenu.</p>
	 * @access public
	 */
	public function __construct(){
	}

	/**
	 * Desenha o corpo menu de contexto.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML/javascript,
	 * todas as informa��es necess�rias para implementar o menu de contexto.</p>
	 * @access public
	 * @return string C�digo HTML/javascript que ser� inserido na p�gina
	 */
	public function draw() {
		$msCodeMenu="";
		if ($this->cbShouldDraw) {
			$msCodeMenu .= $this->drawHeader();
			$msCodeMenu .=  "\n<script language=\"JavaScript\">\n".$this->drawBody()." </script>\n\n";
			$msCodeMenu .= $this->drawFooter();
		}
		return $msCodeMenu;
	}

	/**
	 * Desenha o corpo menu de contexto.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML/javascript,
	 * todas as informa��es necess�rias para implementar o menu de contexto.</p>
	 * @access public
	 * @return string C�digo HTML/javascript que ser� inserido na p�gina
	 */
	public function drawHeader() {
		$msOut = "";
		$moBox = $this->getObjFWDBox();

		$miHeight = 0;
		$miWidth = 0;

		$maDrawItems = array();

		$mbHasBeforeItem = false;
		$miContItems = count($this->caItems);
		for($miI=0;$miI<$miContItems;$miI++){
			if ($this->caItems[$miI]->getShouldDraw()) {
				if($this->caItems[$miI] instanceof FWDMenuSeparator){
					if($mbHasBeforeItem==true){
						for($miJ=$miI+1;$miJ<$miContItems;$miJ++)
						if(($this->caItems[$miJ] instanceof FWDMenuItem)){
							if($this->caItems[$miJ]->getShouldDraw()){
								$maDrawItems[] = $this->caItems[$miI];
								$miHeight += $this->caItems[$miI]->getAttrHeight();
								$mbHasBeforeItem = false;
								break;
							}
						}else{
							break;
						}
					}else{}//falta um item antes do menu separator, assim ele n�o vai ser renderizado
				}else{
					$mbHasBeforeItem=true;
					$maDrawItems[] = $this->caItems[$miI];
					$miHeight += $this->caItems[$miI]->getAttrHeight();
				}
			}
		}

		$this->caItems = $maDrawItems;

		if ($moBox!=null)
		{
			$miWidth = $moBox->getAttrWidth();
				
			//caso nenhum dos itens de menu tenham largura , � utilizada a largura default de (116)
			if($miWidth==0)
			{
				$miWidth=198;
			}
			else
			{
				//pelo menos um item de menu tem largura definida
			}
		}
		else
		{
			$miWidth=198;
		}
		if (FWDWebLib::browserIsIE())
		$moMenuPanel = new FWDPanel(new FWDBox(0,0,$miHeight+2,$miWidth));
		else
		$moMenuPanel = new FWDPanel(new FWDBox(0,0,$miHeight,$miWidth));
		$moMenuPanel->setAttrClass("FWDMenu");
		$moMenuPanel->setAttrIndex(2);
		$moMenuPanel->setAttrName($this->getAttrName());

		$miNumElem = 0;
		$miSumHeight = 0;

		$moBox = $moMenuPanel->getObjFWDBox();

		foreach ($this->getAttrItems() as $moItem) {
			if (FWDWebLib::browserIsIE())
			$moBox = new FWDBox(0,$miSumHeight,$moItem->getAttrHeight(),$miWidth-2);
			else
			$moBox = new FWDBox(0,$miSumHeight,$moItem->getAttrHeight(),$miWidth);
			$miSumHeight += $moItem->getAttrHeight();
			$miNumElem++;
			$msName = $this->getAttrName()."_MenuElement".$miNumElem;
				
			$moItem->createMenuElement($moBox,$msName,array($miNumElem,count($this->caItems)),$this->getAttrName());
				
			$moMenuPanel->addObjFWDView($moItem);
		}

		$moMenuPanel->setAttrDisplay("false");

		$moEvent = new FWDClientEvent('OnClick','hidemenu',$this->getAttrName());
		$moMenuPanel->addEvent($moEvent);

		//input hidden utilizado para armazenar o valor da coluna da grid q sofreu a a��o do contextmenu
		$msInputs = " \n\n\n\n<input type=hidden id='{$this->getAttrName()}_action_target' value=''> \n";

		$msCodeMenu = $moMenuPanel->draw();

		$msOut .= $msInputs . $msCodeMenu;
		$msOut .= "<script language='javascript'>  gebi('dialog').onclick = function(){soMenuManager.hideMenus()}  </script>";

		return $msOut;
	}

	/**
	 * Desenha o corpo menu de contexto.
	 *
	 * <p>Re�ne em uma vari�vel string, o c�gido da fun��o javascript respons�vel por acionar
	 * o menu de contexto
	 * @return string C�digo javascript da fun��o do menu de contexto
	 */
	public function drawBody() {
		$msReturn = "";
		$msReturn .= "      soMenuManager.addMenu('{$this->getAttrName()}',window);\n";
		$msReturn .= "function {$this->getAttrName()}(event){\n";
		$msReturn .= "      var id='{$this->getAttrName()}';\n";
		$msReturn .= "      soMenuManager.hideMenus();\n";
		$msReturn .= "      js_show_menu(id,event);\n";
		$msReturn .= " }\n ";

		return $msReturn;
	}

	/**
	 * Obtem o codigo javascript para o menu de contexto.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo javascript,
	 * todas as informa��es necess�rias para o menu de context ser acionado
	 * nos elementos em que deve aparecer.</p>
	 * @access public
	 * @return string C�digo HTML/javascript que ser� inserido na p�gina
	 */
	public function drawFooter() {
		$msAllElements="";
		$maElements="";
		$msDiv="";

		$msAllElements = $this->getAttrElements();
		$maElements = explode(":",$msAllElements);

		foreach($maElements as $msElement)
		{
			if($msElement)
			{
				$msAllSubElements = $this->getAttrSubElements();

				if(!$msAllSubElements)
				{
					$msDiv .= "gebi('{$msElement}').oncontextmenu = {$this->getAttrName()};\n";
				}
				else
				{
					$maSubElement=explode(":",$msAllSubElements);
					foreach($maSubElement as $msSubElement)
					{
						if($msSubElement)
						{
							$msSubElementComplete = $msElement . "_" . $msSubElement;
							$msDiv .= "gebi('{$msSubElementComplete}').oncontextmenu = {$this->getAttrName()};\n";
						}
						else{ }
						//subtarget vazio, provavelmente gerado no explode se existia algum ":" a mais no $this->getAttrSubElement()
					}
				}//end else

			}//end if
			else { }//target vazio, provavelmente gerado no explode se existia algum ":" a mais no $this->getAttrElement()
		}//end foreach
		$msDiv = "\n<script language=\"JavaScript\">\n".$msDiv."</script>\n\n";
		return $msDiv;
	}

	/**
	 * Adiciona um item de menu.
	 *
	 * <p>Adiciona um objeto item de menu da classe FWDMenuItem
	 * na classe FWDMenu.</p>
	 * @access public
	 * @param FWDMenuItem $poItem Item de Menu
	 */
	public function setObjFWDMenuItem(FWDMenuItem $poItem) {
		$this->caItems[] = $poItem;
	}

	/**
	 * Adiciona um separador de itens.
	 *
	 * <p>Adiciona um objeto separador de itens da classe FWDMenuSeparator
	 * na classe FWDMenu.</p>
	 * @access public
	 * @param FWDMenuSeparator $poItem Separador de itens
	 */
	public function setObjFWDMenuSeparator(FWDMenuSeparator $poItem) {
		$this->caItems[] = $poItem;
	}

	/**
	 * Seta o valor do atributo SubElements.
	 *
	 * <p>Seta o valor do atributo SubTarget.</p>
	 * @access public
	 * @param string $psSubElements Sub-Elementos do Element do Menu de contexto
	 */
	public function setAttrSubTarget($psSubElements) {
		$this->csSubElements = $psSubElements;
	}

	/**
	 * Seta o valor do atributo Elements.
	 *
	 * <p>Seta o valor do atributo Elements.</p>
	 * @access public
	 * @param string $psElements Elementos sobre os quais o Menu de contexto atuar�
	 */
	public function setAttrElements($psElements) {
		$this->csElements = $psElements;
	}

	/**
	 * Adiciona valores (separados por ':') ao atributo Elements.
	 *
	 * <p>Adiciona valores (separados por ':') ao atributo Elements.</p>
	 * @access public
	 * @param string $psElements Elementos a serem adicionados
	 */
	public function addElements($psElements) {
		$this->csElements .= $psElements;
	}

	/**
	 * Retorna os �tens internos ao menu de contexto.
	 *
	 * <p>Retorna um array com os �tens internos ao menu de contexto.</p>
	 * @access public
	 * @return array Itens do Menu de contexto
	 */
	public function getAttrItems() {
		return $this->caItems;
	}

	/**
	 * Retorna o SubElement do Menu de contexto.
	 *
	 * <p>Retorna o SubElements do Menu de contexto. Ou seja sub-elementos do elemento definido em
	 * csElement. Utilizado principalmente na grid.</p>
	 * @access public
	 * @return string SubElements do Menu de contexto
	 */
	public function getAttrSubElements() {
		return $this->csSubElements;
	}

	/**
	 * Retorna o Elements do Menu de contexto.
	 *
	 * <p>Retorna o Elements do Menu de contexto.</p>
	 * @access public
	 * @return string Elements sobre os quais o Menu de contexto atuar�
	 */
	public function getAttrElements() {
		return $this->csElements;
	}

	/**
	 * M�todo usado para sobrescrever o setValue do pai (FWDView).
	 *
	 * <p>A Classe menu n�o tem valor para ser setado.</p>
	 * @access public
	 */
	public function setValue() {
	}

	/**
	 * Retorna o array de menuitens, e seus sub-elementos, e o pr�prio menu
	 *
	 * <p>Retorna o array de menuitens, e seus sub-elementos, e o pr�prio menu.</p>
	 * @access public
	 * @return array Array de menuitens, e seus sub-elementos, e o pr�prio menu
	 */
	public function collect() {
		$maOut = array();
		$maOut = array_merge($maOut,parent::collect());
		foreach ($this->caItems as $moView) {
			if ($moView instanceof FWDCollectable) {
				$maItems = $moView->collect();
				if (count($maItems)>0)
				$maOut = array_merge($maOut,$maItems);
			}
			else {
				$maOut[] = $moView;
			}
		}
		return $maOut;
	}

	/**
	 * Clona os itens do Menu de contexto.
	 *
	 * <p>Clona os itens do Menu de contexto..</p>
	 * @access public
	 */
	public function __clone() {
		$maNewItems = array();
		foreach ($this->caItems as $moItem)
		$maNewItems[] = clone $moItem;
		$this->caItems = $maNewItems;
	}
}
?>