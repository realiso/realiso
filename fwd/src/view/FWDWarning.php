<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDWarning. Implementa uma alerta.
 *
 * <p>Classe que implementa alertas de aviso para o usu�rio.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDWarning extends FWDStatic {

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDWarning.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do texto est�tico
	 */
	public function __construct($poBox = null){
		parent::__construct($poBox);
		$this->cbNoWrap = false;
		$this->cbDisplay = false;
	}

	/**
	 * Desenha o cabe�alho do texto est�tico.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o cabe�alho do texto est�tico.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do texto est�tico
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawHeader($poBox = null){
		$this->setAttrTabIndex(0);

		$msGfxRef = FWDWebLib::getInstance()->getGfxRef();

		if($this->cbNoWrap==true){
			$msTDclass = "class='FWDWarning_TD_nowrap'";
		}else{
			$msTDclass = "class='FWDWarning_TD'";
		}

		$msAlign = " align=\"".$this->csHorizontal."\" valign=\"" . $this->csVertical . "\" ";
		$msStyle = "style='{$this->getStyle()}border-spacing:0px;border-collapse:collapse;'";

		$moEvent = new FWDClientEvent();
		$moEvent->setAttrEvent('onClick');
		$moEvent->setAttrValue("gobi('{$this->csName}').hide();");
		$this->addObjFWDEvent($moEvent);

		return "<table {$this->getGlobalProperty()} {$this->getEvents()} {$msStyle}>"
		."<tr>"
		."<td style='padding:0px;left;background-image: url({$msGfxRef}bg_warning_NW.gif);width:5'></td>"
		."<td style='padding:0px;background-image: url({$msGfxRef}bg_warning_N.gif);height:5'></td>"
		."<td style='padding:0px;background-image: url({$msGfxRef}bg_warning_NE.gif);width:5'></td>"
		."</tr>"
		."<tr>"
		."<td style='padding:0px;background-image: url({$msGfxRef}bg_warning_W.gif);width:5'></td>"
		."<td style='cursor: pointer;' id='{$this->csName}_td' {$msAlign} {$msTDclass}>";
	}

	/**
	 * Desenha o corpo do texto est�tico.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o corpo do texto est�tico.</p>
	 * @access public
	 *
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawBody(){
		return $this->getValue();
	}

	/**
	 * Desenha o rodap� do texto est�tico.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o rodap� do texto est�tico.</p>
	 * @access public
	 *
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawFooter(){
		$msGfxRef = FWDWebLib::getInstance()->getGfxRef();
		$msReturn = "</td>"
		."<td style='padding:0px;background-image: url({$msGfxRef}bg_warning_E.gif);width:5'></td>"
		."</tr>"
		."<tr>"
		."<td style='padding:0px;background-image: url({$msGfxRef}bg_warning_SW.gif);width:5'></td>"
		."<td style='padding:0px;background-image: url({$msGfxRef}bg_warning_S.gif);height:5'></td>"
		."<td style='padding:0px;background-image: url({$msGfxRef}bg_warning_SE.gif);width:5'></td>"
		."</tr>"
		."</table>";
		if($this->csName){
			$msReturn .=" <script language='javascript'>gebi('{$this->csName}').object = new FWDWarning('{$this->csName}'); </script> ";
		}
		return $msReturn;
	}

}

?>