<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDReportWriter.
 *
 * <p>Classe abstrata que define um objeto capaz de gerar um relat�rio.</p>
 * @package FWD5
 * @subpackage report
 */
abstract class FWDReportWriter {

	/**
	 * Desenha uma linha do relat�rio
	 *
	 * <p>Desenha uma linha do relat�rio (representada por um objeto
	 * FWDReportLine).</p>
	 * @param FWDReportLine $poLine Linha a ser desenhada
	 * @param array $paOffsets Array de offsets dos n�veis superiores
	 * @access public
	 */
	abstract public function drawLine(FWDReportLine $poLine, $paOffsets);

	/**
	 * Gera o relat�rio.
	 *
	 * <p>M�todo para gerar o relat�rio.</p>
	 * @access public
	 */
	abstract public function generateReport();

}
?>