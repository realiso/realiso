<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Interface FWDSuppressModel.
 *
 * <p>Interface para definir classes que fazem os ajustes em um relat�rio quando
 * um de seus n�veis � suprimido.</p>
 * @package FWD5
 * @subpackage report
 */
interface FWDSuppressModel {

	/**
	 * Faz os ajustes necess�rios.
	 *
	 * <p>Faz os ajustes necess�rios.</p>
	 * @access public
	 * @param FWDReportLevel $poReportLevel N�vel suprimido
	 */
	public function makeAdjusts(FWDReportLevel $poReportLevel);

}

?>