<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe para gerar relat�rios.
 *
 * <p>Classe para gerar relat�rios independentemente do formato.</p>
 * @package FWD5
 * @subpackage report
 */
class FWDReport extends FWDReportDrawing implements FWDCollectable {

	/**
	 * Nome do relat�rio
	 * @var string
	 * @access protected
	 */
	protected $csName = "";

	/**
	 * N�mero m�ximo de linhas que o relat�rio pode retornar
	 * @var integer
	 * @access protected
	 */
	protected $ciRowLimit = 0;

	/**
	 * Consulta
	 * @var string
	 * @access protected
	 */
	protected $csQuery = "";

	/**
	 * �rea do relat�rio
	 * @var FWDBox
	 * @access protected
	 */
	protected $coBox = null;

	/**
	 * Array de linhas do cabe�alho do relat�rio
	 * @var array
	 * @access protected
	 */
	protected $caHeaders = array();

	/**
	 * Array de linhas do rodap� do relat�rio
	 * @var array
	 * @access protected
	 */
	protected $caFooters = array();

	/**
	 * Primeiro n�vel do relat�rio
	 * @var FWDReportLevel
	 * @access protected
	 */
	protected $coLevel = null;

	/**
	 * Objeto para manipular o resultado da consulta
	 * @var FWDDBDataSet
	 * @access protected
	 */
	protected $coDataSet = null;

	/**
	 * Filtro do relat�rio
	 * @var FWDReportFilter
	 * @access protected
	 */
	protected $coFilter;

	/**
	 * Array de SuppressModels dos n�veis que podem ser suprimidos
	 * @var array
	 * @access protected
	 */
	protected $caSuppressModels = array();

	/**
	 * Contrutor.
	 *
	 * <p>Construtor da classe FWDReport.</p>
	 * @access public
	 */
	public function __construct(){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
	}

	/**
	 * Inicializa o relat�rio.
	 *
	 * <p>M�todo para inicializar o relat�rio.
	 * Necess�rio para funcionar com o XML compilado.</p>
	 * @access public
	 */
	protected function init() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		$this->setDataSet(new FWDDBDataSet(FWDWebLib::getConnection()));
	}

	/**
	 * Seta o nome do relat�rio.
	 *
	 * <p>M�todo para setar o nome do relat�rio.</p>
	 * @access public
	 * @param string psName Nome do relat�rio
	 */
	public function setAttrName($psName){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		$this->csName = $psName;
	}

	/**
	 * Retorna o nome do relat�rio.
	 *
	 * <p>M�todo para retornar o nome do relat�rio.</p>
	 * @access public
	 * @return string psName Nome do relat�rio
	 */
	public function getAttrName(){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		return $this->csName;
	}

	/**
	 * Seta o n�mero m�ximo de linhas que um relat�rio pode retornar.
	 *
	 * <p>M�todo para setar o n�mero m�ximo de linhas que um relat�rio pode retornar.</p>
	 * @access public
	 * @param integer piRowLimit M�ximo de linhas
	 */
	public function setAttrRowLimit($piRowLimit){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		$this->ciRowLimit = $piRowLimit;
	}

	/**
	 * Retorna o n�mero m�ximo de linhas que um relat�rio pode retornar.
	 *
	 * <p>M�todo para retornar o n�mero m�ximo de linhas que um relat�rio pode retornar.</p>
	 * @access public
	 * @return integer M�ximo de linhas do relat�rio
	 */
	public function getAttrRowLimit(){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		return $this->ciRowLimit;
	}

	/**
	 * Executa a query do relat�rio.
	 *
	 * <p>M�todo para executar a query do relat�rio.</p>
	 * @access public
	 */
	public function executeQuery(){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		if(isset( $this->coDataSet,$this->csQuery)){
			$this->coDataSet->setQuery($this->csQuery);
			$this->coDataSet->execute($this->ciRowLimit);
		}else{
			trigger_error("You must specify a dataset AND a query before running makeQuery()!",E_USER_WARNING);
		}
	}

	/**
	 * Seta o primeiro n�vel do relat�rio.
	 *
	 * <p>M�todo para setar o primeiro n�vel do relat�rio.</p>
	 * @access public
	 * @param FWDReportLevel poLevel Primeiro n�vel do relat�rio
	 * @param boolean $pbReplace Indica se o novo n�vel deve substituir o antigo, caso exista algum
	 */
	public function setObjFWDReportLevel(FWDReportLevel $poLevel, $pbReplace=false){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		if(!isset($this->coLevel) || $pbReplace){
			$this->coLevel = $poLevel;
		}else{
			trigger_error("Report level already defined!", E_USER_WARNING);
		}
	}

	/**
	 * Seta a �rea do relat�rio.
	 *
	 * <p>M�todo para setar a �rea do relat�rio.</p>
	 * @access public
	 * @param FWDBOX poBox �rea do relat�rio
	 */
	public function setObjFWDBox(FWDBox $poBox){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		$this->coBox = $poBox;
	}

	/**
	 * Seta o DataSet.
	 *
	 * <p>M�todo para setar o DataSet do relat�rio.</p>
	 * @access public
	 * @param FWDDBDataSet poDataSet DataSet
	 */
	public function setDataSet(FWDDBDataSet $poDataSet){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		$this->coDataSet = $poDataSet;
	}

	/**
	 * Retorna o DataSet.
	 *
	 * <p>M�todo para retornar o DataSet do relat�rio.</p>
	 * @access public
	 * @return FWDDBDataSet DataSet
	 */
	public function getDataSet(){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		return $this->coDataSet;
	}

	/**
	 * Seta o filtro do relat�rio.
	 *
	 * <p>Seta o filtro do relat�rio.</p>
	 * @access public
	 * @param FWDReportFilter $poFilter Filtro do relat�rio
	 */
	public function setFilter(FWDReportFilter $poFilter) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		$this->coFilter = $poFilter;
	}

	/**
	 * Retorna o filtro do relat�rio.
	 *
	 * <p>Retorna o filtro do relat�rio.</p>
	 * @access public
	 * @return ISMSReportFilter Filtro do relat�rio
	 */
	public function getFilter() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		return $this->coFilter;
	}

	/**
	 * Adiciona um campo ao relat�rio.
	 *
	 * <p>M�todo para adicionar um campo ao relat�rio.</p>
	 * @access public
	 * @param string psFieldName Nome do campo
	 * @param string psFieldAlias Alias do campo
	 */
	public function addField($psFieldName, $psFieldAlias){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		$this->coDataSet->addFWDDBField(new FWDDBField($psFieldName, $psFieldAlias, DB_UNKNOWN));
	}

	/**
	 * Adiciona uma linha ao cabe�alho/rodap� do relat�rio.
	 *
	 * <p>Adiciona uma linha ao cabe�alho/rodap� do relat�rio.</p>
	 * @access public
	 * @param FWDReportViewGroup poRepViewGroup Views
	 * @param string psTarget Cabe�alho (header) ou Rodap� (footer)
	 * @param boolean $pbFirst Indica que a linha deve sera adicionada no in�cio do cabe�alho/rodap�
	 */
	public function addObjFWDReportLine(FWDReportLine $poLine, $psTarget = "", $pbFirst = false){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		switch($psTarget){
			case "header":
				if($pbFirst){
					$this->caHeaders = array_merge(array($poLine),$this->caHeaders);
				}else{
					$this->caHeaders[] = $poLine;
				}
				break;
			case "footer":
				if($pbFirst){
					$this->caFooters = array_merge(array($poLine),$this->caFooters);
				}else{
					$this->caFooters[] = $poLine;
				}
				break;
			default:
				trigger_error("Invalid target! You must specify a target [header|footer] for FWDReportLine when it is a FWDReport's child", E_USER_NOTICE);
				break;
		}
	}

	/**
	 * Seta o objeto que vai desenhar o relat�rio.
	 *
	 * <p>M�todo para setar o objeto que vai desenhar o relat�rio. Tamb�m seta o
	 * objeto para o primeiro n�vel.</p>
	 * @access public
	 * @param FWDReportWriter poWriter Objeto que desenha o relat�rio
	 */
	public function setWriter(FWDReportWriter $poWriter){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		$this->coWriter = $poWriter;
		if($this->coLevel){
			$this->coLevel->setWriter($poWriter);
		}else{
			trigger_error("Report without any level!", E_USER_WARNING);
		}
	}

	/**
	 * Desenha o cabe�alho do relat�rio.
	 *
	 * <p>M�todo para desenhar o cabe�alho do relat�rio.</p>
	 * @access public
	 */
	public function drawHeader(){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		foreach($this->caHeaders as $moHeader){
			$this->coWriter->drawLine($moHeader,array());
		}
	}

	/**
	 * Desenha o corpo do relat�rio.
	 *
	 * <p>M�todo para desenhar o corpo do relat�rio.</p>
	 * @access public
	 */
	public function drawBody(){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		while($this->coDataSet->fetch()){
			$this->coLevel->draw($this->coDataSet,$this->coBox);
		}
		$this->coLevel->drawFooter($this->coBox,true);
	}

	/**
	 * Desenha o rodap� do relat�rio.
	 *
	 * <p>M�todo para desenhar o rodap� do relat�rio.</p>
	 * @access public
	 */
	public function drawFooter(){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		foreach($this->caFooters as $moFooter){
			$this->coWriter->drawLine($moFooter,array());
		}
	}

	/**
	 * Seta os pais dos n�veis
	 *
	 * <p>Faz com que todos os n�veis do relat�rio setem seus pais recursivamente.</p>
	 * @access public
	 */
	public function setParents(){
		$this->coLevel->setParentLevel();
	}

	/**
	 * Relaciona um objeto de SuppressModel a um n�vel.
	 *
	 * <p>Relaciona um objeto de SuppressModel a um n�vel.</p>
	 * @param string $psLevelId Id do n�vel
	 * @param FWDSuppressModel $poSuppressModel Objeto respons�vel por fazer os ajustes quando o n�vel � suprimido
	 */
	protected function addSuppressModel($psLevelId, FWDSuppressModel $poSuppressModel){
		$this->caSuppressModels[$psLevelId] = $poSuppressModel;
	}

	/**
	 * Suprime um n�vel
	 *
	 * <p>Faz com que um n�vel n�o seja renderizado no desenho do relat�rio.</p>
	 * @param string $psLevelId Id do n�vel a ser suprimido
	 */
	public function suppressLevel($psLevelId){
		if(isset($this->caSuppressModels[$psLevelId])){
			FWDWebLib::getObject($psLevelId)->suppress($this->caSuppressModels[$psLevelId],$this);
		}else{
			trigger_error("Missing SuppressModel for level '{$psLevelId}'.",E_USER_WARNING);
		}
	}

	/**
	 * M�todo executado quando acaba de carregar o XML
	 *
	 * <p>Faz com que todos os n�veis do relat�rio setem seus pais recursivamente.</p>
	 * @access public
	 */
	public function execute(){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		$this->setParents();
	}

	/**
	 * Retorna um array com todos sub-elementos do report
	 *
	 * <p>Retorna um array contendo todos elementos contidos no report ou em
	 * seus descendentes.</p>
	 * @access public
	 * @return array Array de sub-elementos
	 */
	public function collect(){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		$maOut = array($this);
		foreach($this->caHeaders as $moHeader){
			$maOut = array_merge($maOut,$moHeader->collect());
		}
		foreach($this->caFooters as $moFooter){
			$maOut = array_merge($maOut,$moFooter->collect());
		}
		$maOut = array_merge($maOut,$this->coLevel->collect());
		return $maOut;
	}

}
?>