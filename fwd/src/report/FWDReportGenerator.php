<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Componente para auxiliar na gera��o do relat�rio.
 *
 * <p>Componente para auxiliar na gera��o desenho do relat�rio.</p>
 * @package FWD5
 * @subpackage report
 */
class FWDReportGenerator {
	/**
	 * Objeto do relat�rio
	 * @var FWDReport
	 * @access protected
	 */
	protected $coReport = null;

	/**
	 * Filtro do relat�rio
	 * @var FWDReportFilter
	 * @access protected
	 */
	protected $coReportFilter = null;

	/**
	 * Template do relat�rio
	 * @var FWDReportITemplate
	 * @access protected
	 */
	protected $coReportTemplate = null;

	/**
	 * Nome do relat�rio
	 * @var string
	 * @access protected
	 */
	protected $csReportName = "";

	/**
	 * Nome do arquivo do relat�rio
	 * @var string
	 * @access protected
	 */
	protected $csReportFilename = "";

	/**
	 * Seta o relat�rio.
	 *
	 * <p>M�todo para setar o relat�rio.</p>
	 * @access public
	 * @param FWDReport $poReport Objeto do relat�rio
	 */
	public function setReport(FWDReport $poReport) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		$this->coReport = $poReport;
	}

	/**
	 * Seta o filtro relat�rio.
	 *
	 * <p>M�todo para setar o filtro relat�rio.</p>
	 * @access public
	 * @param FWDReport $poReportFilter Filtro do relat�rio
	 */
	public function setReportFilter(FWDReportFilter $poReportFilter) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		$this->coReportFilter = $poReportFilter;
	}

	/**
	 * Seta o template relat�rio.
	 *
	 * <p>M�todo para setar o template relat�rio.</p>
	 * @access public
	 * @param FWDReportITemplate $poReportTemplate Template do relat�rio
	 */
	public function setReportTemplate(FWDReportITemplate $poReportTemplate) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		$this->coReportTemplate = $poReportTemplate;
	}

	/**
	 * Seta o nome relat�rio.
	 *
	 * <p>M�todo para setar o nome relat�rio.</p>
	 * @access public
	 * @param string $psReportName Nome do relat�rio
	 */
	public function setReportName($psReportName) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		$this->csReportName = $psReportName;
	}

	/**
	 * Seta o nome do arquivo do relat�rio.
	 *
	 * <p>M�todo para setar o nome do arquivo do relat�rio.</p>
	 * @access public
	 * @param string $psFilename Nome do arquivo do relat�rio
	 */
	public function setReportFilename($psFilename) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		$this->csReportFilename = $psFilename;
	}

	/**
	 * Retorna os tipos de formatos dispon�veis para relat�rio.
	 *
	 * <p>M�todo para retornar os tipos de formatos dispon�veis para relat�rio.</p>
	 * @access public
	 * @return array Formatos dispon�veis de relat�rios (Ex: array(id => 'nome', id2 => 'nome2'))
	 */
	public static function getAvailableFormats($paSupported = array()) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		$maReps = array();
		$mbAllSupported = count($paSupported) ? false : true;
		if ($mbAllSupported || in_array(REPORT_FILETYPE_HTML, $paSupported)) $maReps[REPORT_FILETYPE_HTML] = FWDLanguage::getPHPStringValue('report_html', 'HTML');
		if ($mbAllSupported || in_array(REPORT_FILETYPE_HTML_DOWNLOAD, $paSupported)) $maReps[REPORT_FILETYPE_HTML_DOWNLOAD] = FWDLanguage::getPHPStringValue('report_html_download', 'HTML Download');
		if ($mbAllSupported || in_array(REPORT_FILETYPE_WORD, $paSupported)) $maReps[REPORT_FILETYPE_WORD] = FWDLanguage::getPHPStringValue('report_word', 'Word');
		if ($mbAllSupported || in_array(REPORT_FILETYPE_EXCEL, $paSupported)) $maReps[REPORT_FILETYPE_EXCEL] = FWDLanguage::getPHPStringValue('report_excel', 'Excel');
		if ($mbAllSupported || in_array(REPORT_FILETYPE_PDF, $paSupported)) $maReps[REPORT_FILETYPE_PDF] = FWDLanguage::getPHPStringValue('report_pdf', 'PDF');
		return $maReps;
	}

	/**
	 * Retorna os tipos de classifica��es dispon�veis para relat�rio.
	 *
	 * <p>M�todo para retornar os tipos de classifica��es dispon�veis para relat�rio.</p>
	 * @access public
	 * @return array Classifica��es dispon�veis de relat�rios (Ex: array(id => 'classifica��o', id2 => 'classifica��o2'))
	 */
	public static function getAvailableClassifications() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		return array(
		REPORT_CLASS_CONFIDENTIAL => FWDLanguage::getPHPStringValue('report_confidential_report', 'Relat�rio Confidencial'),
		REPORT_CLASS_INSTITUTIONAL => FWDLanguage::getPHPStringValue('report_institutional_report', 'Relat�rio Institucional'),
		REPORT_CLASS_PUBLIC => FWDLanguage::getPHPStringValue('report_public_report', 'Relat�rio P�blico')
		);
	}

	/**
	 * M�todo para gerar o relat�rio.
	 *
	 * <p>M�todo para gerar o relat�rio.</p>
	 * @access public
	 */
	public function generate() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		/*
		 * Seta os atributos necess�rios para preencher o template.
		 */
		$this->coReportTemplate->setReportType($this->csReportName);
		$this->coReportTemplate->setReportPrivacy($this->coReportFilter->getClassificationName());
		$this->coReportTemplate->setReportComment($this->coReportFilter->getComment());

		/*
		 * Adiciona os headers do template no in�cio do relat�rio e o footer no final.
		 */
		$maHeaders = $this->coReportTemplate->getHeaders();
		foreach($maHeaders as $moHeader){
			$this->coReport->addObjFWDReportLine($moHeader,"header",true);
		}
		$this->coReport->addObjFWDReportLine($this->coReportTemplate->getFooter(), "footer");

		/*
		 * Seta o filtro e o tamanho da janela do relat�rio
		 */
		$this->coReport->setFilter($this->coReportFilter);
		$this->coReport->setObjFWDBox(new FWDBox(0,0,600,700));

		// Seta o limite de linhas
		$this->coReport->setAttrRowLimit($this->coReportFilter->getRowLimit());

		/*
		 * Gera o relat�rio de acordo com o tipo especificado.
		 */
		$msFileName = $this->csReportFilename ? $this->csReportFilename : $this->csReportName;
		$msFileName.= "-".date('Ymd');
		$msFactor = 0.75;
		$mbHTMLDownload = false;
		switch ($this->coReportFilter->getFileType()){
			case REPORT_FILETYPE_HTML_DOWNLOAD:
				$mbHTMLDownload = true;
			case REPORT_FILETYPE_HTML:
				$moWriter = new FWDReportHTMLWriter($msFileName);
				$moWriter->setIsForDownload($mbHTMLDownload);
				$this->coReport->setWriter($moWriter);
				$this->coReport->draw();
				break;
			case REPORT_FILETYPE_WORD:
				$this->coReport->setWriter(new FWDReportDocWriter($msFileName));
				$this->coReport->draw();
				break;
			case REPORT_FILETYPE_EXCEL:
				$this->coReport->init();
				$this->coReport->makeQuery();
				
				$moDataSetExcel = null;
				if($this->reportExcelWriter == null)
				{
    			    $moDataSetExcel = new FWDDataSetExcel($msFileName,$this->coReport->getDataSet());
				}
    			else
    			{
				    $moDataSetExcel = $this->reportExcelWriter;
				    
				    $moDataSetExcel->setFileName($msFileName);
				    $moDataSetExcel->setDataSet($this->coReport->getDataSet());
		        }

				$moDataSetExcel->execute();
				
				break;
			case REPORT_FILETYPE_PDF:
				$this->coReport->setWriter(new FWDReportPDFWriter($msFactor,count($maHeaders)-1,$msFileName));
				$this->coReport->draw();
				break;
		}
	}
	
	protected $reportExcelWriter = null;
	
	public function setReportExcelWriter($w){
	  $this->reportExcelWriter = $w;
	}
}
?>