<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe abstrata que define uma c�lula de uma linha de um relat�rio
 *
 * <p>Classe abstrata que define uma c�lula de uma linha de um relat�rio.</p>
 * @package FWD5
 * @subpackage report
 */
abstract class FWDReportCell {

	/**
	 * Nome da c�lula
	 * @var string
	 * @access protected
	 */
	protected $csName = "";

	/**
	 * Largura da c�lula
	 * @var integer
	 * @access protected
	 */
	protected $ciWidth = 100;

	/**
	 * Largura da margem esquerda da c�lula (na verdade, � um padding com nome de margem para manter o padr�o da FWD)
	 * @var integer
	 * @access protected
	 */
	protected $ciMarginLeft = 0;

	/**
	 * Largura da margem direita da c�lula (na verdade, � um padding com nome de margem para manter o padr�o da FWD)
	 * @var integer
	 * @access protected
	 */
	protected $ciMarginRight = 0;

	/**
	 * Retorna o valor do atributo name
	 *
	 * <p>Retorna o valor do atributo name.</p>
	 * @access public
	 * @return string Valor do atributo name
	 */
	public function getAttrName(){
		return $this->csName;
	}

	/**
	 * Seta o valor do atributo name
	 *
	 * <p>Seta o valor do atributo name.</p>
	 * @access public
	 * @param string $psName Novo valor do atributo
	 */
	public function setAttrName($psName){
		$this->csName = $psName;
	}

	/**
	 * Retorna o valor do atributo width
	 *
	 * <p>Retorna o valor do atributo width.</p>
	 * @access public
	 * @return integer Valor do atributo width
	 */
	public function getAttrWidth(){
		return $this->ciWidth;
	}

	/**
	 * Seta o valor do atributo width
	 *
	 * <p>Seta o valor do atributo width.</p>
	 * @access public
	 * @param integer $piWidth Novo valor do atributo
	 */
	public function setAttrWidth($piWidth){
		$this->ciWidth = $piWidth;
	}

	/**
	 * Retorna o valor do atributo marginleft
	 *
	 * <p>Retorna o valor do atributo marginleft.</p>
	 * @access public
	 * @return integer Valor do atributo marginleft
	 */
	public function getAttrMarginLeft(){
		return $this->ciMarginLeft;
	}

	/**
	 * Seta o valor do atributo marginleft
	 *
	 * <p>Seta o valor do atributo marginleft.</p>
	 * @access public
	 * @param integer $piMarginLeft Novo valor do atributo
	 */
	public function setAttrMarginLeft($piMarginLeft){
		$this->ciMarginLeft = $piMarginLeft;
	}

	/**
	 * Retorna o valor do atributo marginright
	 *
	 * <p>Retorna o valor do atributo marginright.</p>
	 * @access public
	 * @return integer Valor do atributo marginright
	 */
	public function getAttrMarginRight(){
		return $this->ciMarginRight;
	}

	/**
	 * Seta o valor do atributo marginright
	 *
	 * <p>Seta o valor do atributo marginright.</p>
	 * @access public
	 * @param integer $piMarginRight Novo valor do atributo
	 */
	public function setAttrMarginRight($piMarginRight){
		$this->ciMarginRight = $piMarginRight;
	}

}

?>