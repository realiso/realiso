<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDLiquidCellsSuppressModel.
 *
 * <p>Classe de SuppressModel que ajusta as larguras das c�lulas para ocupar o
 * espa�o deixado pelo offset do n�vel suprimido.</p>
 * @package FWD5
 * @subpackage report
 */
class FWDLiquidCellsSuppressModel implements FWDSuppressModel {

	/**
	 * Array de arrays de ids de c�lulas l�quidas. Cada sub-array cont�m os ids das c�lulas de uma mesma linha.
	 * @var array
	 * @access protected
	 */
	protected $caLiquidCells = array();

	/**
	 * Adiciona c�lulas l�quidas
	 *
	 * <p>Adiciona c�lulas l�quidas de uma mesma linha.</p>
	 * @access public
	 * @param string $psIdK Id de uma c�lula
	 */
	public function addCells(/* $psId1, $psId2, ... , $psIdN */){
		$this->caLiquidCells[] = func_get_args();
	}

	/**
	 * Faz os ajustes necess�rios.
	 *
	 * <p>Distribui igualmente o offset entre as c�lulas l�quidas de cada linha.</p>
	 * @access public
	 * @param FWDReportLevel $poReportLevel N�vel suprimido
	 */
	public function makeAdjusts(FWDReportLevel $poReportLevel){
		$moSubLevel = $poReportLevel->getSubLevel();
		if($moSubLevel){
			$miOffset = $moSubLevel->getOffset();
			foreach($this->caLiquidCells as $maLineCells){
				$miCells = count($maLineCells);
				$miCellOffset = floor($miOffset / $miCells);
				$miRest = $miOffset % $miCells;
				foreach($maLineCells as $msCell){
					$moCell = FWDWebLib::getObject($msCell);
					$miWidth = $moCell->getAttrWidth() + $miCellOffset;
					if($miRest--){
						$moCell->setAttrWidth($miWidth + 1);
					}else{
						$moCell->setAttrWidth($miWidth);
					}
				}
			}
		}
	}

}

?>