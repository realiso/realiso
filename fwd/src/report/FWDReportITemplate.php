<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Interface FWDReportITemplate.
 *
 * <p>Interface para implementar o template default para header e footer de relatórios.</p>
 * @package FWD5
 * @subpackage report
 */
interface FWDReportITemplate {
	/**
	 * Seta o nome e o ip do gestor.
	 *
	 * <p>Seta o nome e o ip do gestor.</p>
	 * @access public
	 */
	public function setManager();

	/**
	 * Seta a data do relatório.
	 *
	 * <p>Seta a data do relatório.</p>
	 * @access public
	 */
	public function setDate();

	/**
	 * Seta a o tipo do relatório.
	 *
	 * <p>Seta o tipo do relatório.</p>
	 * @access public
	 * @param string $psValue Tipo do relatório
	 */
	public function setReportType($psValue);

	/**
	 * Seta um comentário para o relatório.
	 *
	 * <p>Seta um comentário para o relatório.</p>
	 * @access public
	 * @param string $psValue Comentário
	 */
	public function setReportComment($psValue);

	/**
	 * Seta a classificação do relatório.
	 *
	 * <p>Seta a classificação do relatório.</p>
	 * @access public
	 * @param string $psValue Classificação do relatório
	 */
	public function setReportPrivacy($psValue);

	/**
	 * Retorna os Headers do relatório.
	 *
	 * <p>Retorna os Headers do relatório.</p>
	 * @access public
	 * @return array Headers do relatório
	 */
	public function getHeaders();

	/**
	 * Retorna o Footer do relatório.
	 *
	 * <p>Retorna o Footer do relatório.</p>
	 * @access public
	 * @return FWDReportLine Footer do relatório
	 */
	public function getFooter();
}
?>