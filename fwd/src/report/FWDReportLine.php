<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDReportLine. Representa uma linha de relat�rio
 *
 * <p>Representa uma linha de relat�rio.</p>
 * @package FWD5
 * @subpackage report
 */
class FWDReportLine implements FWDCollectable {

	/**
	 * Nome do elemento
	 * @var string
	 * @access protected
	 */
	protected $csName = "";

	/**
	 * Altura da linha
	 * @var integer
	 * @access protected
	 */
	protected $ciHeight = 20;

	/**
	 * Cor de fundo da linha. Pode ser qualquer cor v�lida de acordo com a classe FWDColor.
	 * @var string
	 * @access protected
	 */
	protected $csBGColor = 'white';

	/**
	 * Segunda cor de fundo da linha. Se usada, cria um efeito de zebra.
	 * @var string
	 * @access protected
	 */
	protected $csBGColor2 = '';

	/**
	 * Espa�amento vertical da linha (padding-top e padding-bottom)
	 * @var integer
	 * @access protected
	 */
	protected $ciLineSpacing = 0;

	/**
	 * Array ordenado contendo as c�lulas da linha
	 * @var array
	 * @access protected
	 */
	protected $caCells = array();

	/**
	 * Indica se a linha � par (usado para fazer zebrado)
	 * @var boolean
	 * @access protected
	 */
	protected $cbIsEven = false;

	/**
	 * Seta o valor do atributo IsEven
	 *
	 * <p>Seta o valor do atributo IsEven.</p>
	 * @access public
	 * @param boolean $pbIsEven Novo valor do atributo
	 */
	public function setEven($pbValue){
		$this->cbIsEven = $pbValue;
	}

	/**
	 * Retorna o nome da linha
	 *
	 * <p>Retorna o valor do atributo name.</p>
	 * @access public
	 * @return string Valor do atributo name
	 */
	public function getAttrName(){
		return $this->csName;
	}

	/**
	 * Seta o nome da linha
	 *
	 * <p>Seta o valor do atributo name.</p>
	 * @access public
	 * @param string $psName Novo valor do atributo
	 */
	public function setAttrName($psName){
		$this->csName = $psName;
	}

	/**
	 * Retorna a altura da linha
	 *
	 * <p>Retorna o valor do atributo height.</p>
	 * @access public
	 * @return integer Valor do atributo height
	 */
	public function getAttrHeight(){
		return $this->ciHeight;
	}

	/**
	 * Seta a altura da linha
	 *
	 * <p>Seta o valor do atributo height.</p>
	 * @access public
	 * @param integer $piHeight Novo valor do atributo
	 */
	public function setAttrHeight($piHeight){
		$this->ciHeight = $piHeight;
	}

	/**
	 * Retorna a cor de fundo da linha
	 *
	 * <p>Retorna a cor de fundo da linha.</p>
	 * @access public
	 * @return string Valor do atributo bgcolor
	 */
	public function getAttrBGColor(){
		if($this->cbIsEven && $this->csBGColor2){
			return $this->csBGColor2;
		}else{
			return $this->csBGColor;
		}
	}

	/**
	 * Seta a cor de fundo da linha
	 *
	 * <p>Seta a cor de fundo da linha.</p>
	 * @access public
	 * @param string $psBGColor Novo valor do atributo bgcolor
	 */
	public function setAttrBGColor($psBGColor){
		$this->csBGColor = $psBGColor;
	}

	/**
	 * Seta a segunda cor de fundo da linha
	 *
	 * <p>Seta a segunda cor de fundo da linha.</p>
	 * @access public
	 * @param string $psBGColor Novo valor do atributo bgcolor
	 */
	public function setAttrBGColor2($psBGColor){
		$this->csBGColor2 = $psBGColor;
	}

	/**
	 * Retorna o valor do atributo linespacing
	 *
	 * <p>Retorna o valor do atributo linespacing.</p>
	 * @access public
	 * @return integer Valor do atributo linespacing
	 */
	public function getAttrLineSpacing(){
		return $this->ciLineSpacing;
	}

	/**
	 * Seta o valor do atributo linespacing
	 *
	 * <p>Seta o valor do atributo linespacing.</p>
	 * @access public
	 * @param integer $piLineSpacing Novo valor do atributo
	 */
	public function setAttrLineSpacing($piLineSpacing){
		$this->ciLineSpacing = $piLineSpacing;
	}

	/**
	 * Adiciona uma c�lula � linha
	 *
	 * <p>Adiciona uma c�lula � linha.</p>
	 * @access public
	 * @param FWDReportCell $poCell C�lula a ser adicionada � linha
	 */
	public function addObjFWDReportCell(FWDReportCell $poCell){
		$this->caCells[] = $poCell;
	}

	/**
	 * Retorna um array com todas as c�lulas da linha
	 *
	 * <p>Retorna um array com todas as c�lulas da linha.</p>
	 * @access public
	 * @return array Array de c�lulas
	 */
	public function getCells(){
		return $this->caCells;
	}

	/**
	 * Retorna um array com todos sub-elementos do reportLine
	 *
	 * <p>Retorna um array contendo todos elementos contidos no reportLine
	 * ou em seus descendentes.</p>
	 * @access public
	 * @return array Array de sub-elementos
	 */
	public function collect(){
		$maOut = array($this);
		$maOut = array_merge($maOut,$this->caCells);
		return $maOut;
	}

}

?>