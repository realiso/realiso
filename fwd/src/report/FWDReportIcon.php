<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDReportIcon. Uma c�lula de relat�rio contendo uma imagem.
 *
 * <p>Representa uma c�lula de uma linha de relat�rio contendo uma imagem.</p>
 * @package FWD5
 * @subpackage report
 */
class FWDReportIcon extends FWDReportCell {

	/**
	 * Source do �cone (path relativo ao sistema de arquivos local)
	 * @var string
	 * @access protected
	 */
	protected $csSrc = '';

	/**
	 * Altura do �cone (em pixels)
	 * @var integer
	 * @access protected
	 */
	protected $ciHeight = 0;

	/**
	 * Largura do �cone (em pixels)
	 * @var integer
	 * @access protected
	 */
	protected $ciWidth = 0;

	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDReportIcon. Seta a altura e a largura do
	 * �cone, caso n�o tenham sido especificadas.</p>
	 * @access public
	 */
	public function __construct(){
/*		if ($this->csSrc){
			$maBox = @getimagesize($this->csSrc);
			if (!$this->ciHeight) $this->ciHeight = $maBox[1];
			if (!$this->ciWidth) $this->ciWidth = $maBox[0];
		} */
		$this->detectHeightAndWidth();
	}
	
	/** essa funcao nao tava implementada apesar da classe dizer que fazia
	 * Pelo sim pelo n�o, eu fiz.. (Piero)
	 * @return unknown_type
	 */
	public function detectHeightAndWidth(){
		if ($this->csSrc && file_exists($this->csSrc)){
			$maBox = @getimagesize($this->csSrc);
			if (!$this->ciHeight) $this->ciHeight = $maBox[1];
			if (!$this->ciWidth) $this->ciWidth = $maBox[0];
		}
	}

	/**
	 * Seta o source do �cone.
	 *
	 * <p>Seta o source do �cone (path da imagem). Seta tamb�m a altura
	 * e a largura do �cone.</p>
	 * @access public
	 * @param string $psSrc Source do �cone
	 */
	public function setAttrSrc($psSrc){
		$this->csSrc = $psSrc;
		$this->detectHeightAndWidth();
	}

	/**
	 * Seta a altura do �cone.
	 *
	 * <p>Seta a altura do �cone, em pixels.</p>
	 * @access public
	 * @param string $piHeight Altura do �cone
	 */
	public function setAttrHeight($piHeight){
		$this->ciHeight = $piHeight;
	}

	/**
	 * Seta a largura do �cone.
	 *
	 * <p>Seta a largura do �cone, em pixels.</p>
	 * @access public
	 * @param string $piWidth Altura do �cone
	 */
	public function setAttrWidth($piWidth){
		$this->ciWidth = $piWidth;
	}

	/**
	 * Retorna o source do �cone.
	 *
	 * <p>Retorna o source do �cone (path da imagem).</p>
	 * @access public
	 * @return string Source do �cone
	 */
	public function getAttrSrc(){
		return $this->csSrc;
	}

	/**
	 * Retorna a altura do �cone.
	 *
	 * <p>Retorna a altura do �cone, em pixels.</p>
	 * @access public
	 * @return integer Altura do �cone
	 */
	public function getAttrHeight(){
		return $this->ciHeight;
	}

	/**
	 * Retorna a largura do �cone.
	 *
	 * <p>Retorna a largura do �cone, em pixels.</p>
	 * @access public
	 * @return integer Largura do �cone
	 */
	public function getAttrWidth(){
		return $this->ciWidth;
	}

	/**
	 * Retorna a extens�o do arquivo de imagem
	 *
	 * <p>Retorna a extens�o do arquivo de imagem.</p>
	 * @access public
	 * @return string Extens�o do arquivo
	 */
	public function getExtension(){
		return strtolower(substr($this->csSrc, strrpos($this->csSrc,'.')+1));
	}

}

?>