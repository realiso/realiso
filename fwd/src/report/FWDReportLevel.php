<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe que representa um n�vel de um relat�rio.
 *
 * <p>Classe que representa um n�vel de um relat�rio.</p>
 * @package FWD5
 * @subpackage report
 */
class FWDReportLevel implements FWDCollectable {

	/**
	 * Nome do n�vel
	 * @var string
	 * @access protected
	 */
	protected $csName = "";

	/**
	 * Subn�vel
	 * @var FWDReportLevel
	 * @access protected
	 */
	protected $coSubLevel = null;

	/**
	 * N�vel pai
	 * @var FWDReportLevel
	 * @access protected
	 */
	protected $coParentLevel = null;

	/**
	 * Indica se o sub-n�vel tem algum item
	 * @var boolean
	 * @access protected
	 */
	protected $cbSubLevelHasItens = false;

	/**
	 * Cabe�alho do n�vel
	 * @var FWDReportLine
	 * @access protected
	 */
	protected $coHeader = null;

	/**
	 * Corpo do n�vel
	 * @var FWDReportLine
	 * @access protected
	 */
	protected $coBody = null;

	/**
	 * N�vel buffer, utilizado para relat�rios montados manualmente
	 * @var FWDReportLine
	 * @access protected
	 */
	protected $caBuffer = array();

	/**
	 * Rodap� do n�vel
	 * @var FWDReportLine
	 * @access protected
	 */
	protected $coFooter = null;

	/**
	 * Iterador do n�vel
	 * @var FWDReportLevelIterator
	 * @access protected
	 */
	protected $coIterator = null;

	/**
	 * Deslocamento do n�vel (em rela��o a margem esquerda do n�vel pai)
	 * @var integer
	 * @access protected
	 */
	protected $ciOffset = 20;

	/**
	 * Margem deixada abaixo de um elemento desse n�vel
	 * @var integer
	 * @access protected
	 */
	protected $ciBottomMargin = 0;

	/**
	 * Campo para controle interno da zebra durante o desenho
	 * @var boolean
	 * @access private
	 */
	private $cbEvenLine = false;

	/**
	 * Contrutor.
	 *
	 * <p>Construtor da classe FWDReportLevel.</p>
	 * @access public
	 */
	public function __construct(){}

	/**
	 * Seta o nome do n�vel.
	 *
	 * <p>M�todo para setar o nome do n�vel.</p>
	 * @access public
	 * @param string psName Nome do n�vel
	 */
	public function setAttrName($psName){
		$this->csName = $psName;
	}

	/**
	 * Retorna o nome do relat�rio.
	 *
	 * <p>M�todo para retornar o nome do relat�rio.</p>
	 * @access public
	 * @return string Nome do relat�rio
	 */
	public function getAttrName(){
		return $this->csName;
	}

	/**
	 * Retorna o valor do atributo bottommargin
	 *
	 * <p>Retorna o valor do atributo bottommargin.</p>
	 * @access public
	 * @return integer Valor do atributo bottommargin
	 */
	public function getAttrBottomMargin(){
		return $this->ciBottomMargin;
	}

	/**
	 * Seta o valor do atributo bottommargin
	 *
	 * <p>Seta o valor do atributo bottommargin.</p>
	 * @access public
	 * @param integer $piBottomMargin Novo valor do atributo
	 */
	public function setAttrBottomMargin($piBottomMargin){
		$this->ciBottomMargin = $piBottomMargin;
	}

	/**
	 * Especifica qual ser� o objeto iterador para o n�vel.
	 *
	 * <p>M�todo para especificar qual ser� o objeto iterador para o n�vel.</p>
	 * @access public
	 * @param FWDReportLevelIterator poIterator Iterador do n�vel
	 */
	public function setLevelIterator(FWDReportLevelIterator $poIterator){
		$this->coIterator = $poIterator;
	}

	/**
	 * Seta o subn�vel do relat�rio.
	 *
	 * <p>M�todo para setar o subn�vel do relat�rio.</p>
	 * @access public
	 * @param FWDReportLevel poSubLevel Subn�vel do relat�rio
	 * @param boolean $pbReplace Indica se o novo n�vel deve substituir o antigo, caso exista algum
	 */
	public function setObjFWDReportLevel($poSubLevel, $pbReplace=false){
		if(!isset($this->coSubLevel) || $pbReplace){
			$this->coSubLevel = $poSubLevel;
		}else{
			trigger_error("Sublevel already defined!", E_USER_WARNING);
		}
	}

	/**
	 * Seta o cabe�alho/corpo/rodap� do n�vel.
	 *
	 * <p>M�todo para setar o cabe�alho/corpo/rodap� do n�vel.</p>
	 * @access public
	 * @param FWDReportViewGroup poRepViewGroup Views
	 * @param string psTarget Cabe�alho (header), Corpo (body = default), Rodap� (footer)
	 */
	public function setObjFWDReportLine(FWDReportLine $poLine, $psTarget = ""){
		switch($psTarget){
			case "header":
				$this->coHeader = $poLine;
				break;
			case "footer":
				$this->coFooter = $poLine;
				break;
			case "body": case "":
				$this->coBody = $poLine;
				break;
			case "buffer": case "":
				$this->caBuffer[] = $poLine;
				break;
			default:
				trigger_error("Invalid FWDReportViewGroup target", E_USER_NOTICE);
				break;
		}
	}

	/**
	 * Seta o objeto que vai desenhar o n�vel.
	 *
	 * <p>M�todo para setar o objeto que vai desenhar o n�vel. Tamb�m seta o
	 * objeto para o subn�vel (se existir).</p>
	 * @access public
	 * @param FWDReportWriter poWriter Objeto que desenha o n�vel
	 */
	public function setWriter(FWDReportWriter $poWriter){
		$this->coWriter = $poWriter;
		if($this->coSubLevel) $this->coSubLevel->setWriter($poWriter);
	}

	/**
	 * Seta o deslocamento do n�vel.
	 *
	 * <p>M�todo para setar o deslocamento do n�vel.</p>
	 * @access public
	 * @param integer piOffset Deslocamento
	 */
	public function setAttrOffset($piOffset){
		$this->ciOffset = $piOffset;
	}

	/**
	 * Retorna o deslocamento do n�vel.
	 *
	 * <p>M�todo para retornar o deslocamento do n�vel.</p>
	 * @access public
	 * @return integer Deslocamento
	 */
	public function getOffset(){
		return $this->ciOffset;
	}

	/**
	 * Testa se � o primeiro item do n�vel
	 *
	 * <p>Testa se � o primeiro item do n�vel.</p>
	 * @access public
	 * return boolean Indica se � o primeiro item do n�vel
	 */
	public function isFirst(){
		return !$this->coIterator->hasValue();
	}

	/**
	 * Desenha o corpo do n�vel.
	 *
	 * <p>M�todo para desenhar o corpo do n�vel.</p>
	 * @access public
	 * @param FWDDBDataSet poDataSet
	 * @param FWDBox poBox
	 */
	public function draw(FWDDBDataSet $poDataSet, FWDBox $poBox){
		if($this->coIterator->changedLevel($poDataSet)){
			if($this->isFirst()){
				$this->cbEvenLine = false;
				if($this->coHeader){
					$this->coWriter->drawLine($this->coHeader,$this->getOffsets());
				}
			}else{
				if(isset($this->coSubLevel)){
					if($this->cbSubLevelHasItens){
						$this->coSubLevel->drawFooter($poBox);
						$this->cbSubLevelHasItens = false;
					}
				}
				if($this->ciBottomMargin>0){
					$moBlankLine = new FWDReportLine();
					$moBlankLine->setAttrHeight($this->ciBottomMargin);
					$moBlankLine->setAttrBGColor('ffffff');
					$moBlankLine->addObjFWDReportCell(new FWDReportStatic());
					$this->coWriter->drawLine($moBlankLine,array());
				}
			}
			if(isset($this->coSubLevel)){
				$this->coSubLevel->clean();
			}
			$this->coIterator->advance($poDataSet);
			$this->coIterator->fetch($poDataSet);
			$this->coIterator->prepare($poDataSet);
			if($this->coBody){
				if($this->coParentLevel!=null) $this->coParentLevel->setSubLevelHasItens(true);
				$this->coBody->setEven($this->cbEvenLine);
				$this->coWriter->drawLine($this->coBody,$this->getOffsets());
				$this->cbEvenLine = !$this->cbEvenLine;
			}
		}
		else {
			$this->coIterator->incElements($poDataSet);
			$this->coIterator->fetch($poDataSet);
		}
		if(isset($this->coSubLevel)){
			$this->coSubLevel->draw($poDataSet, $poBox);
		}
	}

	/**
	 * Seta o n�vel pai
	 *
	 * <p>Seta o n�vel pai e faz com que todos seus descendentes fa�am o mesmo.
	 * O primeiro n�vel tem pai nulo.</p>
	 * @access public
	 * @param FWDReportLevel poParentLevel O n�vel pai
	 */
	public function setParentLevel(/*FWDReportLevel !PHP5.0.x*/ $poParentLevel = null){
		$this->coParentLevel = $poParentLevel;
		if(isset($this->coSubLevel)) $this->coSubLevel->setParentLevel($this);
	}

	/**
	 * Seta o atributo subLevelHasItens
	 *
	 * <p>Seta o atributo subLevelHasItens, que indica se o sub-n�vel tem algum
	 * item.</p>
	 * @access public
	 * @param boolean pbSubLevelHasItens Novo valor do atributo
	 */
	public function setSubLevelHasItens($pbSubLevelHasItens){
		$this->cbSubLevelHasItens = $pbSubLevelHasItens;
	}

	/**
	 * Desenha o rodap� do n�vel
	 *
	 * <p>Desenha o rodap� do n�vel.</p>
	 * @access public
	 * @param FWDBox poBox Box
	 * @param boolean pbRecursive Indica se vai desenhar os rodap�s dos sub-n�veis
	 */
	public function drawFooter($poBox,$pbRecursive=false){
		if($this->coFooter || $pbRecursive){
			if($pbRecursive && isset($this->coSubLevel) && $this->cbSubLevelHasItens){
				$this->coSubLevel->drawFooter($poBox,true);
			}
			if($this->coFooter) $this->coWriter->drawLine($this->coFooter,$this->getOffsets());
		}
	}

	/**
	 * Reseta o n�vel.
	 *
	 * <p>M�todo para resetar o n�vel.</p>
	 * @access public
	 */
	public function clean(){
		$this->coIterator->clean();
	}

	/**
	 * Retorna os deslocamentos dos n�veis superiores.
	 *
	 * <p>Retorna uma lista de pares representando a largura e a cor dos
	 * deslocamentos dos n�veis superiores.</p>
	 * @access public
	 * @return array Lista dos deslocamentos dos n�veis superiores
	 */
	public function getOffsets(){
		$maOffsets = array();
		$moLevel = $this;
		while($moLevel->coParentLevel!=null){
			$miWidth = $moLevel->getOffset();
			$moLevel = $moLevel->coParentLevel;
			if($miWidth>0){
				$msBGColor = $moLevel->coBody->getAttrBGColor();
				$maOffsets[] = array('width'=>$miWidth,'color'=>$msBGColor);
			}
		}
		return array_reverse($maOffsets);
	}

	/**
	 * Retorna o n�vel filho
	 *
	 * <p>Retorna o n�vel filho.</p>
	 * @return FWDReportLevel n�vel filho
	 */
	public function getSubLevel(){
		return $this->coSubLevel;
	}

	/**
	 * Retorna o n�vel pai
	 *
	 * <p>Retorna o n�vel pai.</p>
	 * @return FWDReportLevel n�vel pai
	 */
	public function getParentLevel(){
		return $this->coParentLevel;
	}

	/**
	 * Suprime o n�vel
	 *
	 * <p>Faz com que o n�vel n�o seja renderizado no desenho do relat�rio.</p>
	 * @param FWDSuppressModel $poSuppressModel Objeto respons�vel por fazer os ajustes necess�rios no layout dos n�veis
	 * @param FWDReport $poReport Relat�rio que cont�m os n�veis
	 */
	public function suppress($poSuppressModel, FWDReport $poReport){
		$moParent = $this->getParentLevel();
		if(!$moParent){
			$moParent = $poReport;
		}
		$poSuppressModel->makeAdjusts($this);
		$moParent->setObjFWDReportLevel($this->getSubLevel(),true);
		$poReport->setParents();
	}

	/**
	 * Retorna um array com todos sub-elementos do reportLevel
	 *
	 * <p>Retorna um array contendo todos elementos contidos no reportLevel
	 * ou em seus descendentes.</p>
	 * @access public
	 * @return array Array de sub-elementos
	 */
	public function collect() {
		$maOut = array($this);
		if($this->coHeader instanceof FWDCollectable) $maOut = array_merge($maOut,$this->coHeader->collect());
		if($this->coFooter instanceof FWDCollectable) $maOut = array_merge($maOut,$this->coFooter->collect());
		if($this->coBody instanceof FWDCollectable) $maOut = array_merge($maOut,$this->coBody->collect());
		if($this->coSubLevel && ($this->coSubLevel instanceof FWDCollectable))
		$maOut = array_merge($maOut,$this->coSubLevel->collect());
		foreach ($this->caBuffer as $moBuffer)
		if($moBuffer instanceof FWDCollectable) $maOut = array_merge($maOut,$moBuffer->collect());
		return $maOut;
	}

}
?>