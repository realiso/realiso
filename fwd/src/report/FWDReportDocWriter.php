<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe para escrever um relat�rio no formato Doc.
 *
 * <p>Classe para escrever um relat�rio no formato Doc.</p>
 * @package FWD5
 * @subpackage report
 */
class FWDReportDocWriter extends FWDReportWriter {

	/**
	 * Acumula o c�digo html do relat�rio
	 * @var string
	 * @access protected
	 */
	protected $csReport;

	/**
	 * Nome do arquivo (sem extens�o) a ser salvo
	 * @var string
	 * @access protected
	 */
	protected $csFileName;

	/**
	 * Construtor
	 *
	 * <p>Instancia e inicializa o objeto que constr�i o arquivo Doc.</p>
	 * @param string $psFileName Nome do arquivo (sem extens�o) a ser salvo
	 * @access public
	 */
	public function __construct($psFileName='report'){
		$this->csFileName = strtr($psFileName,'\/:*?"<>|','_________');
		$this->csReport = '';
	}

	/**
	 * Gera o relat�rio.
	 *
	 * <p>M�todo para gerar o relat�rio.</p>
	 * @access public
	 */
	public function generateReport(){
		@header("Cache-Control: ");// leave blank to avoid IE errors
		@header("Pragma: ");// leave blank to avoid IE errors
		@header("Content-type: application/octet-stream");
		@header("Content-Disposition: attachment; filename=\"".rawurlencode($this->csFileName).".doc\"");
		echo $this->csReport;
		exit();
	}

	/**
	 * Desenha uma linha do relat�rio
	 *
	 * <p>Desenha uma linha do relat�rio (representada por um objeto
	 * FWDReportLine).</p>
	 * @param FWDReportLine $poLine Linha a ser desenhada
	 * @param array $paOffsets Array de offsets dos n�veis superiores
	 * @access public
	 */
	public function drawLine(FWDReportLine $poLine,$paOffsets){
		$miHeight = $poLine->getAttrHeight();
		$this->csReport.= "<table cellpadding='0' cellspacing='0' style='border-spacing:0px;border-collapse:collapse;'>\n<tr style='height:$miHeight;'>\n";
		for($miI=0;$miI<count($paOffsets);$miI++){
			$miWidth = $paOffsets[$miI]['width'];
			$msBGColor = $paOffsets[$miI]['color'];
			if(FWDColor::nameToHex($msBGColor)===false) $msBGColor = "#$msBGColor";
			$this->csReport.= "<td style='margin:0px;padding:0px;width:$miWidth;background:$msBGColor;'>&nbsp;</td>";
		}
		$msBGColor = $poLine->getAttrBGColor();
		if(FWDColor::nameToHex($msBGColor)===false) $msBGColor = "#$msBGColor";
		$maCells = $poLine->getCells();
		if(count($maCells)>0){
			foreach($maCells as $moCell){
				if($moCell instanceof FWDReportStatic){
					$msStyle = '';
					if($moCell->getAttrBold()) $msStyle.= 'font-weight:bold;';
					if($moCell->getAttrItalic()) $msStyle.= 'font-style:italic;';
					if($moCell->getAttrUnderline()) $msStyle.= 'text-decoration:underline;';
					$msStyle.= "color:{$moCell->getAttrColor()};";
					$msStyle.= "text-align:{$moCell->getAttrAlign()};";
					$msStyle.= "font-family:{$moCell->getAttrFontFamily()};";
					$msStyle.= "font-size:{$moCell->getAttrFontSize()}pt;";
					$msStyle.= "width:{$moCell->getAttrWidth()};";
					
					$cellBgColor = $moCell->getAttrBgColor();
					if($cellBgColor != '')
						$msStyle.= "background-color:#$cellBgColor";
					else
						$msStyle.= "background:$msBGColor";					

					$msString = strip_tags(FWDWeblib::decodeUTF8($moCell->getValue()));
					$lineWidth = ($moCell->getAttrWidth())/7;
					$msString = wordwrap($msString, round($lineWidth), "<br />\n", true);

					$this->csReport.= "<td style='margin:0px;padding:0px;$msStyle'>$msString</td>\n";
				}else{
					$this->csReport.= "<td style='margin:0px;padding:0px;width:{$moCell->getAttrWidth()};background:$msBGColor;'></td>";
				}
			}
		}else{
			$this->csReport.= "<td style='margin:0px;padding:0px;'></td>\n";
		}
		$this->csReport.= "</tr>\n</table>\n";
	}

}
?>