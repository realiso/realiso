<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDReportFilter.
 *
 * <p>Classe para filtragem de relat�rios.</p>
 * @package FWD5
 * @subpackage report
 */
class FWDReportFilter {

	/**
	 * Coment�rio
	 * @var string
	 * @access protected
	 */
	protected $csComment = '';

	/**
	 * Classificador
	 * @var ISMSReportClassificator
	 * @access protected
	 */
	protected $coClassificator = null;

	/**
	 * N�mero m�ximo de linhas a retornar da consulta
	 * @var integer
	 * @access protected
	 */
	protected $ciRowLimit;

	protected $caLevelsToSuppress = array();

	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDReportFilter.</p>
	 * @access public
	 */
	public function __construct() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		$this->coClassificator = new FWDReportClassificator();
	}

	/**
	 * Seta a classifica��o do relat�rio.
	 *
	 * <p>Seta a classifica��o do relat�rio.</p>
	 * @access public
	 * @param integer $piClassification Classifica��o do relat�rio
	 */
	public function setClassification($piClassification) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		$this->coClassificator->setClassification($piClassification);
	}

	/**
	 * Seta manualmente a classifica��o do relat�rio.
	 *
	 * <p>Seta manualmente a classifica��o do relat�rio.</p>
	 * @access public
	 * @param string $psClassification Classifica��o manual do relat�rio
	 */
	public function setManualClassification($psClassification) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		$this->coClassificator->setManualClassification($psClassification);
	}

	/**
	 * Retorna o nome da classifica��o do relat�rio.
	 *
	 * <p>Retorna o nome da classifica��o do relat�rio.</p>
	 * @access public
	 * @return string Nome da classifica��o do relat�rio
	 */
	public function getClassificationName() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		return $this->coClassificator->getClassificationName();
	}

	/**
	 * Seta o tipo de arquivo do relat�rio.
	 *
	 * <p>Seta o tipo de arquivo do relat�rio.</p>
	 * @access public
	 * @param integer $piFileType Tipo de arquivo do relat�rio
	 */
	public function setFileType($piFileType) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		$this->coClassificator->setFileType($piFileType);
	}

	/**
	 * Retorna o tipo de arquivo do relat�rio.
	 *
	 * <p>Retorna o tipo de arquivo do relat�rio.</p>
	 * @access public
	 * @return integer Tipo de arquivo do relat�rio
	 */
	public function getFileType() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		return $this->coClassificator->getFileType();
	}

	/**
	 * Seta o coment�rio do relat�rio.
	 *
	 * <p>Seta o coment�rio do relat�rio.</p>
	 * @access public
	 * @param string $psComment Coment�rio do relat�rio
	 */
	public function setComment($psComment) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		$this->csComment = $psComment;
	}

	/**
	 * Retorna o coment�rio do relat�rio.
	 *
	 * <p>Retorna o coment�rio do relat�rio.</p>
	 * @access public
	 * @return string coment�rio do relat�rio
	 */
	public function getComment() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		return $this->csComment;
	}

	/**
	 * Retorna o n�mero m�ximo de linhas que deve retornar da consulta
	 *
	 * <p>Retorna o n�mero m�ximo de linhas que deve retornar da consulta.</p>
	 * @access public
	 * @return integer N�mero m�ximo de linhas que deve retornar da consulta
	 */
	public function getRowLimit(){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		return $this->ciRowLimit;
	}

	/**
	 * Seta o n�mero m�ximo de linhas que deve retornar da consulta
	 *
	 * <p>Seta o n�mero m�ximo de linhas que deve retornar da consulta.</p>
	 * @access public
	 * @param integer $piRowLimit N�mero m�ximo de linhas que deve retornar da consulta
	 */
	public function setRowLimit($piRowLimit){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		$this->ciRowLimit = $piRowLimit;
	}

	/**
	 * Indica que um n�vel deve ser suprimido.
	 *
	 * <p>Indica que um n�vel deve ser suprimido.</p>
	 * @access public
	 * @param string $psLevel Nome do n�vel
	 */
	public function suppressLevel($psLevel){
		$this->caLevelsToSuppress[$psLevel] = 1;
	}

	/**
	 * Indica que um n�vel n�o deve ser suprimido.
	 *
	 * <p>Indica que um n�vel n�o deve ser suprimido.</p>
	 * @access public
	 * @param string $psLevel Nome do n�vel
	 */
	public function unsuppressLevel($psLevel){
		unset($this->caLevelsToSuppress[$psLevel]);
	}

	/**
	 * Indica se um n�vel deve ou n�o ser suprimido.
	 *
	 * <p>Indica se um n�vel deve ou n�o ser suprimido.</p>
	 * @access public
	 * @return boolean True, sse o n�vel deve ser suprimido
	 */
	public function levelIsSuppressed($psLevel){
		return isset($this->caLevelsToSuppress[$psLevel]);
	}

	/**
	 * Seta para quem se destina o relat�rio.
	 *
	 * <p>Seta para quem se destina o relat�rio.</p>
	 * @access public
	 * @param string $psTo Para quem se destina o relat�rio
	 */
	public function setDest($psDest) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		$this->coClassificator->setDest($psDest);
	}

	/**
	 * Retorna para quem se destina o relat�rio.
	 *
	 * <p>Retorna para quem se destina o relat�rio.</p>
	 * @access public
	 * @return string para quem se destina o relat�rio
	 */
	public function getDest() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		return $this->coClassificator->getDest();
	}
}
?>