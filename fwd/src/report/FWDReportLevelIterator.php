<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe para iteragir com um n�vel do relat�rio.
 *
 * <p>Classe para iteragir com um n�vel do relat�rio.</p>
 * @package FWD5
 * @subpackage report
 */
abstract class FWDReportLevelIterator {

	/**
	 * @var string Nome do campo respons�vel pela itera��o
	 * @access protected
	 */
	protected $csFieldIterator = "";

	/**
	 * @var string �ltimo valor do iterador
	 * @access protected
	 */
	protected $csLastValue = "";

	/**
	 * @var integer N�mero de que foram percorridos
	 * @access protected
	 */
	protected $ciElements = 0;

	/**
	 * Contrutor.
	 *
	 * <p>Construtor da classe FWDReportLevelIterator.</p>
	 * @access public
	 * @param string psFieldIterator Nome do campo respons�vel pela itera��o (opcional)
	 */
	public function __construct($psFieldIterator = ""){
		$this->csFieldIterator = $psFieldIterator;
	}

	/**
	 * Incrementa o n�mero de elementos percorridos at� o momento.
	 *
	 * <p>M�todo para incrementar o n�mero de elementos percorridos at� o momento.</p>
	 * @access public
	 */
	public function incElements(){
		$this->ciElements++;
	}

	/**
	 * Retorna o n�mero de elementos percorridos at� o momento.
	 *
	 * <p>M�todo para retornar o n�mero de elementos percorridos at� o momento.</p>
	 * @access public
	 */
	public function getElements(){
		return $this->ciElements;
	}

	/**
	 * Avan�a o iterador.
	 *
	 * <p>M�todo para avan�ar o iterador.</p>
	 * @access public
	 */
	public function advance(FWDDBDataSet $poDataSet){
		$moField = $poDataSet->getFieldByAlias($this->csFieldIterator);
		if(!$moField) trigger_error("Field not set", E_USER_WARNING);
		$this->csLastValue = $moField->getValue();
	}

	/**
	 * M�todo abstrato que busca os valores referentes ao n�vel.
	 *
	 * <p>M�todo abstrato para buscar os valores referentes ao n�vel.</p>
	 * @access public
	 */
	abstract public function fetch(FWDDBDataSet $poDataSet);

	/**
	 * M�todo chamado antes de desenhar uma linha do n�vel
	 *
	 * <p>M�todo chamado antes de desenhar uma linha do n�vel. Sobrescreva-a caso
	 * seja necess�rio algum pr�-processamento antes de desenhar o n�vel.</p>
	 * @access public
	 */
	public function prepare(FWDDBDataSet $poDataSet){}

	/**
	 * Verifica se mudou o n�vel.
	 *
	 * <p>M�todo para verificar se houve mudan�a de n�vel.</p>
	 * @access public
	 * @return boolean Verdadeiro ou falso
	 */
	public function changedLevel(FWDDBDataSet $poDataSet){
		$moField = $poDataSet->getFieldByAlias($this->csFieldIterator);
		if($moField){
			return ($moField->getValue() != $this->csLastValue);
		}else{
			trigger_error("Field '{$this->csFieldIterator}' not set", E_USER_WARNING);
			return false;
		}
	}

	/**
	 * Reseta o iterador.
	 *
	 * <p>M�todo para resetar o iterador.</p>
	 * @access public
	 */
	public function clean(){
		$this->csLastValue = "";
	}

	/**
	 * Verifica se o iterador j� possui algum valor.
	 *
	 * <p>M�todo para verificar se o iterador j� possui algum valor. �til
	 * para verificar se � a primeira itera��o ou n�o.</p>
	 * @access public
	 * @return boolean Verdadeiro ou falso
	 */
	public function hasValue(){
		return $this->csLastValue ? true : false;
	}
}
?>