<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe para escrever um relat�rio no formato HTML.
 *
 * <p>Classe para escrever um relat�rio no formato HTML.</p>
 * @package FWD5
 * @subpackage report
 */
class FWDReportHTMLWriter extends FWDReportWriter {

	/**
	 * Buffer de declara��es em CSS. Usado pra diminuir o n�mero de tags style.
	 * @var array
	 * @access protected
	 */
	protected $caCssOutput = array();

	/**
	 * Array associativo que armazena os identificadores das classes CSS. A chave � o md5 do conte�do da classe.
	 * @var array
	 * @access protected
	 */
	protected $caCssClasses = array();

	/**
	 * String que cont�m os "d�gitos" usados nos identificadores de classes CSS.
	 * @var string
	 * @access protected
	 */
	protected $csDigits;

	/**
	 * Armazena o n�mero de d�gitos (tamanho de $csDigits). Serve para evitar m�ltiplas chamadas de strlen().
	 * @var integer
	 * @access protected
	 */
	protected $ciNumDigits;

	/**
	 * Guarda o �ltimo identificador de classe CSS gerado.
	 * @var string
	 * @access protected
	 */
	protected $csLastId;

	/**
	 * Define o n�mero de linhas de um grupo. Ao final de cada grupo, caso seja
	 * necess�rio, uma tag style � escrita pelo flushCss.
	 * @staticvar integer
	 * @access protected
	 */
	protected static $LINE_GROUP_SIZE = 5;

	/**
	 * Contador de linhas. Indica a linha atual do grupo atual.
	 * @var integer
	 * @access protected
	 */
	protected $ciLineGroupCounter = 0;

	/**
	 * Contador de tags style. O IE s� permite 31 tags style.
	 * @var integer
	 * @access protected
	 */
	protected $ciStyleCounter = 0;

	/**
	 * Define se o HTML deve ser enviado via Download
	 * @var boolean
	 * @access protected
	 */
	protected $cbIsForDownload = false;

	/**
	 * Nome do arquivo (sem extens�o) a ser salvo
	 * @var string
	 * @access protected
	 */
	protected $csFileName;

	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDReportHTMLWriter.</p>
	 * @param string $psFileName Nome do arquivo (sem extens�o) a ser salvo
	 * @access public
	 */
	public function __construct($psFileName='report'){
		$this->csFileName = strtr($psFileName,'\/:*?"<>|','_________');
		$this->caCssOutput[] = "table{border-spacing:0;border-collapse:collapse}";
		$this->caCssOutput[] = 'td{margin:0;padding:0;vertical-align:top}';
		$this->caCssOutput[] = 'span{margin:0;padding:0;overflow:hidden;text-overflow:ellipsis}';
		//$this->csDigits = 'abcdefghijklmnopqrstuvwxyz��������������������������񧰺������';
		$this->csDigits = 'abcdefghijklmnopqrstuvwxyz';
		$this->ciNumDigits = strlen($this->csDigits);
		$this->csLastId = '';
	}

	/**
	 * Gera um novo identificador para classes CSS.
	 *
	 * <p>Gera um novo identificador para classes CSS.</p>
	 * @access protected
	 * @return string Identificador gerado
	 */
	protected function nextId(){
		if($this->csLastId==''){
			$msId = $this->csDigits[0];
		}else{
			$msId = $this->csLastId;
			$miNumDigits = strlen($msId);
			for($i=0;$i<$miNumDigits;$i++){
				$miIndex = strpos($this->csDigits,$msId[$i]);
				if($miIndex<$this->ciNumDigits-1){
					$msId[$i] = $this->csDigits[$miIndex+1];
					$this->csLastId = $msId;
					return $msId;
				}else{
					$msId[$i] = $this->csDigits[0];
				}
			}
			$msId.= $this->csDigits[0];
		}
		$this->csLastId = $msId;
		return $msId;
	}

	/**
	 * Retorna o identificador de uma classe CSS.
	 *
	 * <p>Retorna o identificador de uma classe CSS a partir de seu conte�do.
	 * Se a classe n�o existir ainda, ela � criada.</p>
	 * @access protected
	 * @return string Identificador gerado
	 */
	protected function getCssClass($psClassContent){
		$msHash = md5($psClassContent);
		if(!isset($this->caCssClasses[$msHash])){
			$msId = $this->nextId();
			$this->caCssClasses[$msHash] = $msId;
			$this->caCssOutput[] = ".$msId{".$psClassContent.'}';
		}
		return $this->caCssClasses[$msHash];
	}

	/**
	 * Define se o HTML ser� enviado para download
	 *
	 * <p>Define se o HTML ser� enviado para download</p>
	 * @access public
	 * @param boolean cbIsForDownload Define se o HTML ser� enviado para download
	 */
	public function setIsForDownload($pbIsForDownload){
		$this->cbIsForDownload = $pbIsForDownload;
	}

	/**
	 * Escreve na sa�da todas declara��es CSS que est�o bufferizadas.
	 *
	 * <p>Escreve na sa�da todas declara��es CSS que est�o bufferizadas em
	 * $caCssOutput e esvazia o buffer.</p>
	 * @param boolean Indica se � para for�ar o flush (s� o �ltimo deve for�ar)
	 * @access protected
	 */
	protected function flushCss($pbForceFlush=false){
		$this->ciLineGroupCounter++;
		// se 30 tags style j� tiverem sido escritas, s� permite mais uma (a com pbForceFlush = true)
		if($pbForceFlush || ($this->ciStyleCounter<30 && $this->ciLineGroupCounter==self::$LINE_GROUP_SIZE)){
			$this->ciLineGroupCounter = 0;
			if(count($this->caCssOutput)>0){
				echo "<style>\n";
				foreach($this->caCssOutput as $msOutput) echo $msOutput . "\n";
				echo "</style>\n";
				$this->caCssOutput = array();
				$this->ciStyleCounter++;
			}
		}
	}

	/**
	 * Gera o relat�rio.
	 *
	 * <p>M�todo para gerar o relat�rio.</p>
	 * @access public
	 */
	public function generateReport(){
		$this->flushCss(true);
		?>
</body>
</html>
		<?
	}

	/**
	 * Desenha uma linha do relat�rio
	 *
	 * <p>Desenha uma linha do relat�rio (representada por um objeto
	 * FWDReportLine).</p>
	 * @param FWDReportLine $poLine Linha a ser desenhada
	 * @param array $paOffsets Array de offsets dos n�veis superiores
	 * @access public
	 */
	public function drawLine(FWDReportLine $poLine,$paOffsets){
		if($this->cbIsForDownload) {
			$ssProtocol = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] && $_SERVER['HTTPS']!='off') ? 'https' : 'http';
			$ssPath = "$ssProtocol://{$_SERVER['SERVER_NAME']}{$_SERVER['PHP_SELF']}";
			$ssImgPath = substr($ssPath,0,strrpos($ssPath,'/')+1);
		}
		else {
			$ssImgPath = "";
		}
		if($this->csLastId==''){
			if($this->cbIsForDownload) {
				header("Cache-Control: ");// leave blank to avoid IE errors
				header("Pragma: ");// leave blank to avoid IE errors
				header("Content-type: application/octet-stream");
				header("Content-Disposition: attachment; filename=\"".rawurlencode($this->csFileName).".htm\"");
			}
			?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body topmargin=0 leftmargin=0 oncontextmenu='return true'>
			<?
		}
		$miNumOffsets = count($paOffsets);
		if($miNumOffsets>1){
			$maOffsets = array($paOffsets[0]);
			$miLastOffset = 0;
			for($i=1;$i<$miNumOffsets;$i++){
				if($paOffsets[$i]['color']==$maOffsets[$miLastOffset]['color']){
					$maOffsets[$miLastOffset]['width']+= $paOffsets[$i]['width'];
				}else{
					$maOffsets[] = $paOffsets[$i];
					$miLastOffset++;
				}
			}
		}else{
			$maOffsets = $paOffsets;
		}
		$msOffsetsHtml = '';
		for($i=0;$i<count($maOffsets);$i++){
			$miWidth = $maOffsets[$i]['width'];
			$msBGColor = $maOffsets[$i]['color'];
			if(FWDColor::nameToHex($msBGColor)===false) $msBGColor = "#$msBGColor";
			$msClass = $this->getCssClass("width:$miWidth;background:$msBGColor");
			$msOffsetsHtml.= "<td class=$msClass>&nbsp;";
		}

		$msBGColor = $poLine->getAttrBGColor();
		if(FWDColor::nameToHex($msBGColor)===false) $msBGColor = "#$msBGColor";
		$maCells = $poLine->getCells();

		$miLineSpacing = $poLine->getAttrLineSpacing();
		$msLineSpacingHtml = '';
		if($miLineSpacing){
			$msClass = $this->getCssClass("height:{$miLineSpacing};line-height:{$miLineSpacing}px;");
			$msLineSpacingHtml.= "<table><tr class={$msClass}>{$msOffsetsHtml}";
			$miWidth = 0;
			foreach($maCells as $moCell){
				$miWidth+= $moCell->getAttrWidth();
			}
			$msClass = $this->getCssClass("width:{$miWidth};background:{$msBGColor}");
			$msLineSpacingHtml.= "<td class={$msClass}></table>";
			echo $msLineSpacingHtml . "\n";
		}

		$miHeight = $poLine->getAttrHeight();
		$msClass = $this->getCssClass("height:$miHeight;line-height:{$miHeight}px;");
		echo "<table><tr class={$msClass}>{$msOffsetsHtml}\n";
		if(count($maCells)>0){
			foreach($maCells as $moCell){

				$miMarginLeft = $moCell->getAttrMarginLeft();
				$miMarginRight = $moCell->getAttrMarginRight();
				$miWidth = $moCell->getAttrWidth() - $miMarginLeft - $miMarginRight;

				if($miMarginLeft){
					$msClass = $this->getCssClass("width:{$miMarginLeft};background:{$msBGColor}");
					echo "<td class={$msClass}>\n";
				}

				if($moCell instanceof FWDReportStatic){
					$msStyle = '';
					if($moCell->getAttrBold()) $msStyle.= 'font-weight:bold;';
					if($moCell->getAttrItalic()) $msStyle.= 'font-style:italic;';
					if($moCell->getAttrUnderline()) $msStyle.= 'text-decoration:underline;';
					$msStyle.= "color:{$moCell->getAttrColor()};";
					$msStyle.= "text-align:{$moCell->getAttrAlign()};";
					$msStyle.= "font-family:{$moCell->getAttrFontFamily()};";
					$msStyle.= "font-size:{$moCell->getAttrFontSize()}pt;";
					$msStyle.= "width:{$miWidth};";
					
					$cellBgColor = $moCell->getAttrBgColor();
					if($cellBgColor != '')
						$msStyle.= "background-color:$cellBgColor";
					else
						$msStyle.= "background:$msBGColor";
										
					$msString = $moCell->getValue();
					$msClass = $this->getCssClass($msStyle);
					echo "<td class=$msClass><span class=$msClass>$msString</span></td>\n";
				}elseif($moCell instanceof FWDReportIcon){
					$msClass = $this->getCssClass("width:{$miWidth};background:$msBGColor");
					echo "<td class=$msClass>\n";
					$msClass = $this->getCssClass("height:{$moCell->getAttrHeight()};width:{$miWidth}");
					$msSrc = $moCell->getAttrSrc();
					$miDotPos = strrpos($msSrc,'.');
					$msImgName = substr($msSrc,0,$miDotPos);
					$msExtension = strtolower(substr($msSrc,$miDotPos+1));
					if($msExtension=='png' && file_exists("$msImgName.gif")) $msSrc = "$msImgName.gif";
					echo "<img src='".$ssImgPath.$msSrc."'class=$msClass> </td>\n";
				}else{
					trigger_error('This kind of ReportCell is not supported by HTMLWriter.',E_USER_WARNING);
				}

				if($miMarginRight){
					$msClass = $this->getCssClass("width:{$miMarginRight};background:{$msBGColor}");
					echo "<td class={$msClass}>\n";
				}

			}
		}else{
			echo "<td>\n";
		}
		echo "</table>\n";
		echo $msLineSpacingHtml . "\n";
		$this->flushCss();
	}

}
?>