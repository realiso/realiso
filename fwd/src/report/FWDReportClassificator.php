<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

define("REPORT_CLASS_NONE", 7500);
define("REPORT_CLASS_CONFIDENTIAL", 7501);
define("REPORT_CLASS_INSTITUTIONAL", 7502);
define("REPORT_CLASS_PUBLIC", 7503);

define("REPORT_FILETYPE_HTML_DOWNLOAD", 7300);
define("REPORT_FILETYPE_HTML", 7301);
define("REPORT_FILETYPE_WORD", 7302);
define("REPORT_FILETYPE_EXCEL", 7303);
define("REPORT_FILETYPE_PDF", 7304);

/**
 * Classe FWDReportClassificator.
 *
 * <p>Classe para classificação de relatórios.</p>
 * @package FWD5
 * @subpackage report
 */
class FWDReportClassificator {

	/**
	 * Tipo de classificação
	 * @var integer
	 * @access protected
	 */
	protected $ciClassification = REPORT_CLASS_CONFIDENTIAL;

	/**
	 * Tipo de arquivo
	 * @var integer
	 * @access protected
	 */
	protected $ciFileType = REPORT_FILETYPE_HTML;

	/**
	 * Array de tipos possíveis de classificação
	 * @var array
	 * @access protected
	 */
	protected $caClassificationArray = array();

	/**
	 * Array de tipos possíveis de arquivos para relatórios
	 * @var array
	 * @access protected
	 */
	protected $caFileTypeArray = array();

	/**
	 * A quem se destina o relatório
	 * @var string
	 * @access protected
	 */
	protected $csDest = '';

	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDReportClassificator. Seta os valores
	 * dos tipos de classificação dos relatórios, bem como os possíveis
	 * tipos de arquivos para relatórios.</p>
	 * @access public
	 */
	public function __construct() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		$this->caClassificationArray =
		array(
		REPORT_CLASS_NONE => '',
		REPORT_CLASS_CONFIDENTIAL	=> FWDLanguage::getPHPStringValue('report_confidencial', "Relatório Confidencial"),
		REPORT_CLASS_INSTITUTIONAL => FWDLanguage::getPHPStringValue('report_institutional', "Relatório Institucional"),
		REPORT_CLASS_PUBLIC => FWDLanguage::getPHPStringValue('report_public', "Relatório Público")
		);
		$this->caFileTypeArray =
		array(
		REPORT_FILETYPE_HTML_DOWNLOAD,
		REPORT_FILETYPE_HTML,
		REPORT_FILETYPE_WORD,
		REPORT_FILETYPE_EXCEL,
		REPORT_FILETYPE_PDF
		);
	}

	/**
	 * Seta o tipo de classificação.
	 *
	 * <p>Seta o tipo de classificação.</p>
	 * @access public
	 * @param integer $piClassification Tipo de classificação
	 */
	public function setClassification($piClassification) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		if (!array_key_exists($piClassification,$this->caClassificationArray))
		trigger_error("Invalid Report Classification: $piClassification.",E_USER_ERROR);
		$this->ciClassification = $piClassification;
	}

	/**
	 * Seta a classificação manualmente.
	 *
	 * <p>Seta a classificação manualmente.</p>
	 * @access public
	 * @param string $psClassification Classificação manual do relatório
	 */
	public function setManualClassification($psClassification) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		$this->caClassificationArray[0] = $psClassification;
		$this->ciClassification = 0;
	}

	/**
	 * Retorna o tipo de classificação.
	 *
	 * <p>Retorna o tipo de classificação.</p>
	 * @access public
	 * @return integer Tipo de classificação
	 */
	public function getClassification() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		return $this->ciClassification;
	}

	/**
	 * Seta o tipo de arquivo.
	 *
	 * <p>Seta o tipo de arquivo.</p>
	 * @access public
	 * @param integer $piFileType Tipo de arquivo
	 */
	public function setFileType($piFileType) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		if (!in_array($piFileType,$this->caFileTypeArray))
		trigger_error("Invalid Report FileType: $piFileType.",E_USER_ERROR);
		$this->ciFileType = $piFileType;
	}

	/**
	 * Retorna o tipo de arquivo.
	 *
	 * <p>Retorna o tipo de arquivo.</p>
	 * @access public
	 * @return integer Tipo de arquivo
	 */
	public function getFileType() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		return $this->ciFileType;
	}

	/**
	 * Retorna o array com os tipos possíveis de classificação.
	 *
	 * <p>Retorna o array com os tipos possíveis de classificação.</p>
	 * @access public
	 * @return array Tipos possíveis de classificação
	 */
	public function getClassificationArray() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		return $this->caClassificationArray;
	}

	/**
	 * Retorna o array com os tipos possíveis de arquivos.
	 *
	 * <p>Retorna o array com os tipos possíveis de arquivos.</p>
	 * @access public
	 * @return array Tipos possíveis de arquivos
	 */
	public function getFileTypeArray() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		return $this->caFileTypeArray;
	}

	/**
	 * Retorna o nome da classificação.
	 *
	 * <p>Retorna o nome da classificação.</p>
	 * @access public
	 * @return string Nome do tipo de classificação
	 */
	public function getClassificationName() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		if(!array_key_exists($this->ciClassification,$this->caClassificationArray))
		trigger_error("Invalid Report Classification: $this->ciClassification.",E_USER_ERROR);
		$msReturn = $this->caClassificationArray[$this->ciClassification];
		if ($msReturn && $this->csDest) {
			$msReturn .= ": ".$this->csDest;
		}
		return $msReturn;
	}

	/**
	 * Seta para quem se destina o relatório.
	 *
	 * <p>Seta para quem se destina o relatório.</p>
	 * @access public
	 * @param string $psTo Para quem se destina o relatório
	 */
	public function setDest($psDest) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		$this->csDest = $psDest;
	}

	/**
	 * Retorna para quem se destina o relatório.
	 *
	 * <p>Retorna para quem se destina o relatório.</p>
	 * @access public
	 * @return string para quem se destina o relatório
	 */
	public function getDest() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		return $this->csDest;
	}
}
?>