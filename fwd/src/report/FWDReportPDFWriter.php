<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe para escrever um relat�rio no formato PDF.
 *
 * <p>Classe para escrever um relat�rio no formato PDF.</p>
 * @package FWD5
 * @subpackage report
 */
class FWDReportPDFWriter extends FWDReportWriter {

	/**
	 * Objeto que constr�i o arquivo PDF
	 * @var FPDF
	 * @access protected
	 */
	protected $coFpdf = null;

	/**
	 * Linhas que s�o usadas como cabe�alho das p�ginas
	 * @var array
	 * @access protected
	 */
	protected $caHeaders = array();

	/**
	 * N�mero de linhas do cabe�alho das p�ginas
	 * @var integer
	 * @access protected
	 */
	protected $ciHeaderLines = 0;

	/**
	 * Nome do arquivo
	 * @var string
	 * @access protected
	 */
	protected $csFileName = "";

	/**
	 * Contrutor
	 *
	 * <p>Instancia e inicializa o objeto que constr�i o arquivo PDF.
	 * A classe FPDF foi alterada para escalar o tamanho de tudo (inclusive fonte).</p>
	 * @param float $pfFactor Fator de escala para ajustar o tamanho do relat�rio
	 * @param integer $piHeaderLines N�mero de linhas do cabe�alho das p�ginas
	 * @param string $psFileName Nome do arquivo (sem o '.pdf')
	 * @access public
	 */
	public function __construct($pfFactor=0.77262693,$piHeaderLines=0, $psFileName='report'){
		$this->coFpdf = new FWDPDF('P',$pfFactor);
		$this->coFpdf->setWriter($this);
		$this->coFpdf->AliasNbPages();
		$this->coFpdf->AddPage();
		$this->coFpdf->SetFont('Arial','',10);
		$this->coFpdf->SetFillColor(255,255,255);
		$this->ciHeaderLines = $piHeaderLines;
		$this->csFileName = strtr($psFileName,'\/:*?"<>|','_________');
	}

	/**
	 * Gera o relat�rio.
	 *
	 * <p>M�todo para gerar o relat�rio.</p>
	 * @access public
	 */
	public function generateReport() {
		$this->coFpdf->Output("{$this->csFileName}.pdf", "D");
		exit();
	}

	/**
	 * Desenha uma linha do relat�rio
	 *
	 * <p>Desenha uma linha do relat�rio (representada por um objeto
	 * FWDReportLine).</p>
	 * @param FWDReportLine $poLine Linha a ser desenhada
	 * @param array $paOffsets Array de offsets dos n�veis superiores
	 * @access public
	 */
	public function drawLine(FWDReportLine $poLine,$paOffsets){
		if(count($this->caHeaders)<$this->ciHeaderLines){
			$this->caHeaders[] = $poLine;
		}
		$miHeight = $poLine->getAttrHeight();

		// Constr�i o array com as informa��es dos deslocamentos pra evitar re-convers�o das cores
		$maOffsetsInfo = array();
		for($i=0;$i<count($paOffsets);$i++){
			$miWidth = $paOffsets[$i]['width'];
			$msBGColor = $paOffsets[$i]['color'];
			$maRgb = FWDColor::nameToRgb($msBGColor);
			if($maRgb==false) $maRgb = FWDColor::hexToRgb($msBGColor);
			if($maRgb==false) $maRgb = array(255,255,255);
			$maOffsetsInfo[] = array(
        'width' => $miWidth,
        'color' => $maRgb
			);
		}

		// Obt�m a cor de fundo da linha
		$msBGColor = $poLine->getAttrBGColor();
		$maBGRgb = FWDColor::nameToRgb($msBGColor);
		if($maBGRgb==false) $maBGRgb = FWDColor::hexToRgb($msBGColor);
		if($maBGRgb==false) $maBGRgb = array(255,255,255);

		$maCells = $poLine->getCells();

		// Se a linha tem espa�amento, renderiza o espa�amento superior
		$miLineSpacing = $poLine->getAttrLineSpacing();
		if($miLineSpacing){
			foreach($maOffsetsInfo as $maOffsetInfo){
				$this->coFpdf->SetFillColor($maOffsetInfo['color'][0],$maOffsetInfo['color'][1],$maOffsetInfo['color'][2]);
				$this->coFpdf->Cell($maOffsetInfo['width'],$miLineSpacing,'',0,0,'L',1);
			}
			$miLineWidth = 0;
			foreach($maCells as $moCell){
				$miLineWidth+= $moCell->getAttrWidth();
			}
			$this->coFpdf->SetFillColor($maBGRgb[0],$maBGRgb[1],$maBGRgb[2]);
			$this->coFpdf->Cell($miLineWidth,$miLineSpacing,'',0,0,'L',1);
			$this->coFpdf->Ln();
		}

		// renderiza os deslocamentos
		foreach($maOffsetsInfo as $maOffsetInfo){
			$this->coFpdf->SetFillColor($maOffsetInfo['color'][0],$maOffsetInfo['color'][1],$maOffsetInfo['color'][2]);
			$this->coFpdf->Cell($maOffsetInfo['width'],$miHeight,'',0,0,'L',1);
		}

		if(count($maCells)>0){
			$miActualLines = 1;
			$maCellsInfo = array();

			$this->coFpdf->SetFillColor($maBGRgb[0],$maBGRgb[1],$maBGRgb[2]);

			// renderiza as c�lulas
			foreach($maCells as $moCell){

				$miMarginLeft = $moCell->getAttrMarginLeft();
				$miMarginRight = $moCell->getAttrMarginRight();
				$miWidth = $moCell->getAttrWidth() - $miMarginLeft - $miMarginRight;

				if($miMarginLeft){
					$this->coFpdf->Cell($miMarginLeft,$miHeight,'',0,0,'L',1);
				}

				if($moCell instanceof FWDReportStatic){
					$msFontStyle = '';
					if($moCell->getAttrBold()) $msFontStyle.= 'B';
					if($moCell->getAttrItalic()) $msFontStyle.= 'I';
					if($moCell->getAttrUnderline()) $msFontStyle.= 'U';
					$msColor = $moCell->getAttrColor();
					$maRgb = FWDColor::nameToRgb($msColor);
					if($maRgb==false) $maRgb = FWDColor::hexToRgb($msColor);
					if($maRgb==false) $maRgb = array(0,0,0);
					$msAlign = $moCell->getAttrAlign();
					if($msAlign=='right') $msAlign = 'R';
					elseif($msAlign=='center') $msAlign = 'C';
					else $msAlign = 'L';

					$moCell->setAttrStringNoEscape(true);

					$msString = strip_tags($moCell->getValue());
					$this->coFpdf->SetFont($moCell->getAttrFontFamily(),$msFontStyle,$moCell->getAttrFontSize());
					$this->coFpdf->SetTextColor($maRgb[0],$maRgb[1],$maRgb[2]);
					$miLines = $this->coFpdf->wordWrap($msString,$miWidth);
					$maString = explode("\n",$msString);
					$msString = $maString[0];
					$miActualLines = max($miActualLines,$miLines);
					$this->coFpdf->Cell($miWidth,$miHeight,$msString,0,0,$msAlign,1);

					$maCellsInfo[] = array(
            'fontFamily' => $moCell->getAttrFontFamily(),
            'fontStyle' => $msFontStyle,
            'fontSize' => $moCell->getAttrFontSize(),
            'textColor' => $maRgb,
            'align' => $msAlign,
            'width' => $moCell->getAttrWidth(),
            'marginleft' => $miMarginLeft,
            'marginright' => $miMarginRight,
            'lines' => $maString
					);

				}elseif($moCell instanceof FWDReportIcon){
					$msExtension = $moCell->getExtension();
					$msSrc = $moCell->getAttrSrc();
					if($msExtension=='gif'){
						$msSrc = substr($msSrc,0,strrpos($msSrc,'.')+1).'png';
						$msExtension = 'png';
					}
					if($msExtension=='jpg' || $msExtension=='jpeg' || $msExtension=='png'){
						$mfCurrentX = $this->coFpdf->GetX();
						$this->coFpdf->Cell($miWidth,$miHeight,'',0,0,'L',1);
						$this->coFpdf->Image($msSrc,$mfCurrentX,$this->coFpdf->GetY(),$miWidth,$miHeight);
					}else{
						trigger_error('PDFWriter only supports the following image formats: jpg, jpeg and png.',E_USER_WARNING);
					}
					$maCellsInfo[] = array('width' => $moCell->getAttrWidth());
				}else{
					trigger_error('This kind of ReportCell is not supported by PDFWriter.',E_USER_WARNING);
				}

				if($miMarginRight){
					$this->coFpdf->Cell($miMarginRight,$miHeight,'',0,0,'L',1);
				}

			}
			$this->coFpdf->Ln();

			// Se alguma coluna teve quebra de linha, renderiza quantas linhas forem necess�rias para terminar a string
			for($i=1;$i<$miActualLines;$i++){
				foreach($maOffsetsInfo as $maOffsetInfo){
					$this->coFpdf->SetFillColor($maOffsetInfo['color'][0],$maOffsetInfo['color'][1],$maOffsetInfo['color'][2]);
					$this->coFpdf->Cell($maOffsetInfo['width'],$miHeight,'',0,0,'L',1);
				}
				foreach($maCellsInfo as $maCellInfo){
					$this->coFpdf->SetFillColor($maBGRgb[0],$maBGRgb[1],$maBGRgb[2]);
					if(isset($maCellInfo['lines']) && isset($maCellInfo['lines'][$i])){
						$miMarginLeft = $maCellInfo['marginleft'];
						$miMarginRight = $maCellInfo['marginright'];
						$miWidth = $maCellInfo['width'] - $miMarginLeft - $miMarginRight;
						$this->coFpdf->SetFont($maCellInfo['fontFamily'],$maCellInfo['fontStyle'],$maCellInfo['fontSize']);
						$this->coFpdf->SetTextColor($maCellInfo['textColor'][0],$maCellInfo['textColor'][1],$maCellInfo['textColor'][2]);
						if($miMarginLeft){
							$this->coFpdf->Cell($miMarginLeft,$miHeight,'',0,0,'L',1);
						}
						$this->coFpdf->Cell($miWidth,$miHeight,$maCellInfo['lines'][$i],0,0,$maCellInfo['align'],1);
						if($miMarginRight){
							$this->coFpdf->Cell($miMarginRight,$miHeight,'',0,0,'L',1);
						}
					}else{
						$this->coFpdf->Cell($maCellInfo['width'],$miHeight,'',0,0,'L',1);
					}
				}
				$this->coFpdf->Ln();
			}

		}

		// renderiza o espa�amento inferior
		if($miLineSpacing){
			foreach($maOffsetsInfo as $maOffsetInfo){
				$this->coFpdf->SetFillColor($maOffsetInfo['color'][0],$maOffsetInfo['color'][1],$maOffsetInfo['color'][2]);
				$this->coFpdf->Cell($maOffsetInfo['width'],$miLineSpacing,'',0,0,'L',1);
			}
			$this->coFpdf->SetFillColor($maBGRgb[0],$maBGRgb[1],$maBGRgb[2]);
			$this->coFpdf->Cell($miLineWidth,$miLineSpacing,'',0,0,'L',1);
			$this->coFpdf->Ln();
		}

	}

	/**
	 * Desenha o cabe�alho da p�gina
	 *
	 * <p>Desenha o cabe�alho da p�gina.</p>
	 * @access public
	 */
	public function drawHeader(){
		foreach($this->caHeaders as $moHeader){
			$this->drawLine($moHeader,array());
		}
	}

}
?>