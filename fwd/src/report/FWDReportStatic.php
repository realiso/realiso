<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDReportStatic. Uma c�lula de relat�rio contendo texto.
 *
 * <p>Representa uma c�lula de uma linha de relat�rio contendo texto.</p>
 * @package FWD5
 * @subpackage report
 */
class FWDReportStatic extends FWDReportCell {

	/**
	 * Objeto string contendo o texto da c�lula
	 * @var FWDString
	 * @access protected
	 */
	protected $coString = null;

	/**
	 * Alinhamento do texto dentro da c�lula (left|center|right)
	 * @var string
	 * @access protected
	 */
	protected $csAlign = 'left';

	/**
	 * Indica se o texto da c�lula est� em negrito
	 * @var boolean
	 * @access protected
	 */
	protected $cbBold = false;

	/**
	 * Indica se o texto da c�lula est� em it�lico
	 * @var boolean
	 * @access protected
	 */
	protected $cbItalic = false;

	/**
	 * Indica se o texto da c�lula est� sublinhado
	 * @var boolean
	 * @access protected
	 */
	protected $cbUnderline = false;

	/**
	 * Cor do texto da c�lula. Pode ser qualquer cor v�lida de acordo com a classe FWDColor.
	 * @var string
	 * @access protected
	 */
	protected $csColor = '000000';
	
	
	
	protected $csBgColor = '';

	/**
	 * Tipo da fonte do texto da c�lula
	 * @var string
	 * @access protected
	 */
	protected $csFontFamily = 'Arial';

	/**
	 * Tamanho da fonte do texto da c�lula
	 * @var integer
	 * @access protected
	 */
	protected $ciFontSize = 8;

	/**
	 * Atribui um valor ao objeto string da c�lula.
	 *
	 * <p>Atribui um valor ao objeto string da c�lula.</p>
	 * @access public
	 * @param string $psValue Novo texto da c�lula
	 */
	public function setValue($psValue, $psForce = true){
		if($this->coString==null) $this->coString = new FWDString();
		$this->coString->setValue($psValue, $psForce);
	}

	/**
	 * Retorna o texto da c�lula.
	 *
	 * <p>Retorna o texto da c�lula.</p>
	 * @access public
	 * @return string Texto da c�lula
	 */
	public function getValue(){
		if($this->coString) return $this->coString->getAttrString();
		else return '';
	}

	/**
	 * Seta o valor do atributo noEscape do objeto string
	 *
	 * <p>Seta o valor do atributo noEscape do objeto string para evitar que os
	 * caracteres "'" sejam escapados.</p>
	 * @access public
	 * @param boolean $pbNoEscape Novo valor do atributo noEscape
	 */
	public function setAttrStringNoEscape($pbNoEscape){
		if($this->coString==null){
			$this->coString = new FWDString();
		}
		$this->coString->setAttrNoEscape($pbNoEscape);
	}

	/**
	 * Seta o valor do elemento.
	 *
	 * <p>M�todo para setar o valor do elemento.</p>
	 * @access public
	 * @param FWDString $poString Seta o valor do elemento
	 */
	public function setObjFWDString(FWDString $poString){
		if(isset($this->coString)){
			$this->coString->setValue($poString->getAttrString());
		}else{
			$this->coString = $poString;
		}
	}

	/**
	 * Retorna o valor do atributo align
	 *
	 * <p>Retorna o valor do atributo align.</p>
	 * @access public
	 * @return string Valor do atributo align
	 */
	public function getAttrAlign(){
		return $this->csAlign;
	}

	/**
	 * Seta o valor do atributo align
	 *
	 * <p>Seta o valor do atributo align.</p>
	 * @access public
	 * @param string $psAlign Novo valor do atributo
	 */
	public function setAttrAlign($psAlign){
		$this->csAlign = FWDWebLib::attributeParser($psAlign,array('left'=>'left','center'=>'center','right'=>'right'),"psAlign");
	}

	/**
	 * Retorna o valor do atributo bold
	 *
	 * <p>Retorna o valor do atributo bold.</p>
	 * @access public
	 * @return boolean Valor do atributo bold
	 */
	public function getAttrBold(){
		return $this->cbBold;
	}

	/**
	 * Seta o valor do atributo bold
	 *
	 * <p>Seta o valor do atributo bold.</p>
	 * @access public
	 * @param boolean $pbBold Novo valor do atributo
	 */
	public function setAttrBold($psBold){
		$this->cbBold = FWDWebLib::attributeParser($psBold,array("true"=>true, "false"=>false),"psBold");
	}

	/**
	 * Retorna o valor do atributo italic
	 *
	 * <p>Retorna o valor do atributo italic.</p>
	 * @access public
	 * @return boolean Valor do atributo italic
	 */
	public function getAttrItalic(){
		return $this->cbItalic;
	}

	/**
	 * Seta o valor do atributo italic
	 *
	 * <p>Seta o valor do atributo italic.</p>
	 * @access public
	 * @param boolean $pbItalic Novo valor do atributo
	 */
	public function setAttrItalic($psItalic){
		$this->cbItalic = FWDWebLib::attributeParser($psItalic,array("true"=>true, "false"=>false),"psItalic");
	}

	/**
	 * Retorna o valor do atributo underline
	 *
	 * <p>Retorna o valor do atributo underline.</p>
	 * @access public
	 * @return boolean Valor do atributo underline
	 */
	public function getAttrUnderline(){
		return $this->cbUnderline;
	}

	/**
	 * Seta o valor do atributo underline
	 *
	 * <p>Seta o valor do atributo underline.</p>
	 * @access public
	 * @param boolean $pbUnderline Novo valor do atributo
	 */
	public function setAttrUnderline($psUnderline){
		$this->cbUnderline = FWDWebLib::attributeParser($psUnderline,array("true"=>true, "false"=>false),"psUnderline");
	}

	
	
	/**
	 * Retorna o valor do atributo color
	 *
	 * <p>Retorna o valor do atributo color.</p>
	 * @access public
	 * @return string Valor do atributo color
	 */
	public function getAttrBgColor(){
		return $this->csBgColor;
	}

	/**
	 * Seta o valor do atributo color
	 *
	 * <p>Seta o valor do atributo color.</p>
	 * @access public
	 * @param string $psColor Novo valor do atributo
	 */
	public function setAttrBgColor($psColor){
		if(FWDColor::isValid($psColor)){
			$this->csBgColor = $psColor;
		}else{
			trigger_error('Invalid color name or code.',E_USER_WARNING);
		}
	}	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Retorna o valor do atributo color
	 *
	 * <p>Retorna o valor do atributo color.</p>
	 * @access public
	 * @return string Valor do atributo color
	 */
	public function getAttrColor(){
		return $this->csColor;
	}

	/**
	 * Seta o valor do atributo color
	 *
	 * <p>Seta o valor do atributo color.</p>
	 * @access public
	 * @param string $psColor Novo valor do atributo
	 */
	public function setAttrColor($psColor){
		if(FWDColor::isValid($psColor)){
			$this->csColor = $psColor;
		}else{
			trigger_error('Invalid color name or code.',E_USER_WARNING);
		}
	}

	/**
	 * Retorna o valor do atributo fontfamily
	 *
	 * <p>Retorna o valor do atributo fontfamily.</p>
	 * @access public
	 * @return string Valor do atributo fontfamily
	 */
	public function getAttrFontFamily(){
		return $this->csFontFamily;
	}

	/**
	 * Seta o valor do atributo fontfamily
	 *
	 * <p>Seta o valor do atributo fontfamily.</p>
	 * @access public
	 * @param string $psFontFamily Novo valor do atributo
	 */
	public function setAttrFontFamily($psFontFamily){
		$this->csFontFamily = $psFontFamily;
	}

	/**
	 * Retorna o valor do atributo fontsize
	 *
	 * <p>Retorna o valor do atributo fontsize.</p>
	 * @access public
	 * @return integer Valor do atributo fontsize
	 */
	public function getAttrFontSize(){
		return $this->ciFontSize;
	}

	/**
	 * Seta o valor do atributo fontsize
	 *
	 * <p>Seta o valor do atributo fontsize.</p>
	 * @access public
	 * @param integer $piFontSize Novo valor do atributo
	 */
	public function setAttrFontSize($piFontSize){
		$this->ciFontSize = $piFontSize;
	}

}
?>