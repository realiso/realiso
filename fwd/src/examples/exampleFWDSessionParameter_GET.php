<?php
include_once "include.php";

$soWebLib = FWDWebLib::getInstance();
$soWebLib->xml_load('exampleFWDSessionParameter_GET.xml');

$static1 = $soWebLib->getObject('static1');
$static1->setValue($soWebLib->getObject('parameter1')->getValue());

$static2 = $soWebLib->getObject('static2');
$static2->setValue($soWebLib->getObject('parameter2')->getValue());

$static3 = $soWebLib->getObject('static3');
$static3->setValue($soWebLib->getObject('parameter3')->getValue());

$soWebLib->dump_html($soWebLib->getObject("dialog"));
?>