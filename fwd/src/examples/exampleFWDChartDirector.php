<?php

include_once "include.php";

$soWebLib = FWDWebLib::getInstance();

$soWebLib->xml_load("exampleFWDChartDirector.xml");

// Dados para os gr�ficos
$siMin = 1;
$siMax = 250;
$saDataSets = array(
array(rand($siMin,$siMax),rand($siMin,$siMax),rand($siMin,$siMax),rand($siMin,$siMax),rand($siMin,$siMax)),
array(rand($siMin,$siMax),rand($siMin,$siMax),rand($siMin,$siMax),rand($siMin,$siMax),rand($siMin,$siMax)),
array(rand($siMin,$siMax),rand($siMin,$siMax),rand($siMin,$siMax),rand($siMin,$siMax),rand($siMin,$siMax)),
);
// Labels para os gr�ficos
$saLabels = array("Mon", "Tue", "Wed", "Thu", "Fri");

////////////////////////////////////////////////////////////////////////////////
/// Gr�fico de Barras Simples
////////////////////////////////////////////////////////////////////////////////

// Cria um objeto XYChart de tamanho 300 x 240 pixels
$soBarChart = new XYChart(300, 240);
// Define a area do grafico em (25, 20) e de tamanho 250 x 200 pixels
$soBarChart->setPlotArea(25, 20, 250, 200);
// Adiciona uma bar chart layer usando os dados para gr�ficos simples
$soBarChart->addBarLayer($saDataSets[0],0xff0000);
// Seta os labels no eixo x
$soBarChart->xAxis->setLabels($saLabels);

////////////////////////////////////////////////////////////////////////////////
/// Gr�fico de Torta
////////////////////////////////////////////////////////////////////////////////

// Cria um objeto PieChart de tamanho 300 x 240 pixels
$soPieChart = new PieChart(300, 240);
// Define o centro do gr�fico em (150, 80) e com raio de 100 pixels
$soPieChart->setPieSize(150, 120, 80);
// Adiciona um t�tulo ao gr�fico
$soPieChart->addTitle(utf8_encode("Exemplo de T�tulo"));
// Define que o gr�fico � 3D
$soPieChart->set3D();
// Seta os dados e os labels do gr�fico
$soPieChart->setData($saDataSets[1], $saLabels);
// Explode o maior setor
$soPieChart->setExplode(array_search(max($saDataSets[1]),$saDataSets[1]));

////////////////////////////////////////////////////////////////////////////////
/// Gr�fico de Barras M�ltiplo
////////////////////////////////////////////////////////////////////////////////

// Cria um objeto XYChart de tamanho 250 x 250 pixels
$soMultiBarChart = new XYChart(300, 240);
// Adiciona um t�tulo ao gr�fico usando fonte Arial 10pt
$soMultiBarChart->addTitle("         Average Weekday Network Load", "", 10);
// Define a area do grafico em (45, 25) e de tamanho 239 x 180 pixels. Usa duas cores alternadas para o fundo.
$soPlotAreaObj = $soMultiBarChart->setPlotArea(45, 25, 239, 180);
$soPlotAreaObj->setBackground(0xffffc0, 0xffffe0);
// Adiciona uma legenda em (45, 20) usando layout horizontal, fonte Arial 8pt e com fundo transparente.
$soLegendObj = $soMultiBarChart->addLegend(45, 20, false, "", 8);
$soLegendObj->setBackground(Transparent);
// Adiciona um t�tulo ao eixo y
$soMultiBarChart->yAxis->setTitle("Throughput (MBytes Per Hour)");
// Reserva 20 pixels no topo do eixo x pra legenda
$soMultiBarChart->yAxis->setTopMargin(20);
// Seta os labels
$soMultiBarChart->xAxis->setLabels($saLabels);
// Adiciona uma multi-bar layer com 3 data sets
$soLayer = $soMultiBarChart->addBarLayer2(Side, 3);
$soLayer->addDataSet($saDataSets[0], 0xff8080, "Server #1");
$soLayer->addDataSet($saDataSets[1], 0x80ff80, "Server #2");
$soLayer->addDataSet($saDataSets[2], 0x8080ff, "Server #3");

////////////////////////////////////////////////////////////////////////////////
/// Gr�fico de Linhas
////////////////////////////////////////////////////////////////////////////////

// Cria um objeto XYChart de tamanho 300 x 240 pixels com fundo cinza claro, borda preta e 1px de borda 3D
$soLineChart = new XYChart(300, 240, 0xf0f0f0, 0x0, 1);
// Define a area do grafico em (50, 45), tamanho de 220 x 150 pixels, fundo branco e linhas cinza claro
$soLineChart->setPlotArea(50, 45, 220, 150, 0xffffff, -1, -1, 0xc0c0c0, -1);
// Adiciona uma legenda em (45, 25) usando layout horizontal, fonte Arial 8pt e fundo transparente
$soLegendObj = $soLineChart->addLegend(45, 25, false, "", 8);
$soLegendObj->setBackground(Transparent);
// Adiciona um t�tulo ao gr�fico usando fonte Arial Bold Italic 11pt. Texto branco e fundo azul com 1 pixel de borda 3D
$soTitleObj = $soLineChart->addTitle("Daily Server Load", "arialbi.ttf", 11, 0xffffff);
$soTitleObj->setBackground(0x5050f0, -1, 1);
// Adiciona um t�tulo ao eixo y
$soLineChart->yAxis->setTitle("MBytes");
// Seta os labels
$soLineChart->xAxis->setLabels($saLabels);
// Adiciona um t�tulo ao eixo x
$soLineChart->xAxis->setTitle("Jun 12, 2001");
// Adiciona um line layer ao gr�fico
$layer = $soLineChart->addLineLayer2();
// Seta a espessura default das linhas para 2 pixels
$layer->setLineWidth(2);
// Adiciona os 3 data sets � line layer
$layer->addDataSet($saDataSets[0], -1, "Server #1");
$layer->addDataSet($saDataSets[1], -1, "Server #2");
$layer->addDataSet($saDataSets[2], -1, "Server #3");

////////////////////////////////////////////////////////////////////////////////
/// Seta os gr�ficos nos objetos da FWD
////////////////////////////////////////////////////////////////////////////////

// Seta o gr�fico de barras no seu objeto FWDChartDirector correspondente e cria um mapa para ele
$soChart = $soWebLib->getObject('barChart');
$soChart->setChart($soBarChart);
$ssMapCode = $soBarChart->getHTMLImageMap("javascript:alert('Label: {xLabel}\\nValor: {value}')", " ","title='{xLabel}: {value}'");
$soChart->setMapCode($ssMapCode);
// Seta o gr�fico de torta no seu objeto FWDChartDirector correspondente
$soWebLib->getObject('pieChart')->setChart($soPieChart);
// Seta o gr�fico de m�ltiplas barras no seu objeto FWDChartDirector correspondente
$soWebLib->getObject('multiBarChart')->setChart($soMultiBarChart);
// Seta o gr�fico de m�ltiplas linhas no seu objeto FWDChartDirector correspondente
$soChart = $soWebLib->getObject('multiLineChart');
$soChart->setChart($soLineChart);
$ssMapCode = $soLineChart->getHTMLImageMap("javascript:return false", " "," onmouseover='gebi(\"label\").innerHTML=\"<tr><td>{dataSetName}<br>Month: {xLabel}<br>{value} MB</td></tr>\"' onmouseout='gebi(\"label\").innerHTML=\"<tr><td></td></tr>\"'");
$soChart->setMapCode($ssMapCode);

$soWebLib->dump_html($soWebLib->getObject("dialog"));

?>