<?php

function getmicrotime() {
	list($usec, $sec) = explode(" ", microtime());
	return ((float)$usec + (float)$sec);
}

$tempo = getmicrotime();

include_once "include.php";

$weblib_instance = FWDWebLib::getInstance();
$weblib_instance->xml_load("exampleBenchMark.xml");

class Example_SubmitEvent extends FWDRunnable {

	public function run(){
		$memo = FWDWebLib::getObject("Memo1");
		$memo->setValue("voc� clicou no submit para chegar aqui");
	}

}

$StartEvent_instance = FWDStartEvent::getInstance();
$StartEvent_instance->addSubmitEvent(new Example_SubmitEvent("Button1"));

$weblib_instance->dump_html($dialog);


$tempo = getmicrotime() - $tempo;
print("\n\n<BR><BR>");
print("O processamento demorou ". substr($tempo,0,4). " segundos<BR>");

?>