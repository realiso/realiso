<?php

include_once "include.php";

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(''));
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
<script language="javascript">
        if(soPopUpManager.ciCount==undefined){
          soPopUpManager.ciCount = 0;
        }
        
        function newPopUp(pbModal){
          var miNumber = ++soPopUpManager.ciCount;
          js_open_popup(
            'popup'+miNumber,
            'exampleFWDPopUp3.php?dummy',
            'PopUp '+miNumber,
            (pbModal?'true':'false'),
            340 + (browser.isIE?25:27),
            480 + (browser.isIE?2:4)
          );
        }
        
        function populateSelect(){
          var moSelect = gobi('list');
          var msId,msText;
          moSelect.clear();
          var moPopUpList = soPopUpManager.coPopUpList;
          for(var i=moPopUpList.ciMinZIndex;i<=moPopUpList.ciMaxZIndex;i++){
            if(moPopUpList.caList[i]){
              msId = moPopUpList.caList[i].coPopUp.id;
              msText = i+': '+msId;
              if(moPopUpList.caList[i].coPopUp.coPopUp.cbModal){
                msText+= ' (modal)';
              }
              moSelect.addItem(msId, msText);
            }
          }
        }
        
        function closeSelected(){
          var maSelected = gobi('list').getValue();
          for(var i=0;i<maSelected.length;i++){
            soPopUpManager.closePopUp(maSelected[i]);
          }
        }
        
        function setFocus(){
          var maSelected = gobi('list').getValue();
          if(maSelected.length==1){
            soPopUpManager.setFocus(maSelected[0]);
          }else{
            alert('Selecione exatamente uma PopUp para receber foco.');
          }
        }
        
        var soSelector = new FWDSelector();
        soSelector.select('main_panel');
        
      </script>
		<?
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('exampleFWDPopUp3.xml');

?>