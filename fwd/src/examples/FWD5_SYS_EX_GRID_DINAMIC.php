<?php

include_once "include.php";

function mainScreenFunc()
{
	$moWebLib = FWDWebLib::getInstance();
	$moGrid2 = $moWebLib->getObject('grid2');
	$moGrid2->SetObjFwdDrawGrid( new testDrawGrid());

	//obter o conteudo da grid com preenchimento via ajax e inserir ele na grid
	for( $miRow = 1; $miRow < 300; $miRow++)
	for( $col = 1; $col <7; $col++)
	if($col == 1)
	$moGrid2->SetItem($col, $miRow, $miRow);
	else
	if($col == 3)
	$moGrid2->SetItem($col, $miRow, $miRow);
	else
	$moGrid2->SetItem($col, $miRow, $miRow."---".$col);
}

class testDrawGrid extends FWDDrawGrid
{
	function DrawItem()
	{
		switch( $this->ciColumnIndex){
			case 1:
				$this->coCellBox->setValue($this->coCellBox->getValue()+10);
				return $this->coCellBox->draw();
				break;
			case 2:
				$moFunction = FWDWebLib::getObject("function");

				$msReturn="";
				$this->coCellBox->setValue($moFunction->getValue());
				return $this->coCellBox->draw();
				break;
			case 3:
				$moToolTip = new FWDToolTip();
				$moToolTip->setAttrStatic('true');
				$moToolTip->setObjFWDString($this->coCellBox->getObjFWDString());
				$this->coCellBox->addObjFWDEvent($moToolTip);
				return $this->coCellBox->draw();
				break;

			default:
				return parent::DrawItem();
				break;
		}
	}
}

class EventBeforeAjax extends FWDRunnable {
	public function run() {
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent("screen_event"));
		mainScreenFunc();
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$moWebLib = FWDWebLib::getInstance();
		$moWebLib->dump_html($moWebLib->getObject('dialog'));

	}

}


$soWebLib = FWDWebLib::getInstance();
$soStartEvent = FWDStartEvent::getInstance();
$soStartEvent->addBeforeAjax(new EventBeforeAjax("before_ajax"));

$soWebLib->xml_load("FWD5_SYS_EX_GRID_DINAMIC.xml");
?>