<?php

include_once "include.php";

$soWebLib = FWDWebLib::getInstance();



class ExampleEventsMenu_GetData extends FWDRunnable {

	public function run() {
		$soWebLib = FWDWebLib::getInstance();
		$moObject = $soWebLib->getObject("target");
		$msName = $moObject->getAttrName();
		// definir rotina a ser executada
		$msData = "data example";

		$moJs = new FWDJsEvent(JS_SET_CONTENT,$msName,$msData);
		echo $moJs->render();
	}

}

class ExampleEventsMenu_CreateMessageBox extends FWDRunnable {

	public function run() {
		$msMessage = "<div name='box_message' id='box_message' style='height:300;width:330;left:1150;top:10;position:absolute;background-color:ivory;border:black 1px solid;display:block;z-index:0;' ><span name='title_' id='title_'    style='height:26;width:328;left:0;top:0;position:absolute;background-color:gray;'>box_message</span><span name='message_box_message' id='message_box_message'    style='height:100;width:230;left:50;top:50;position:absolute;'>Clique 'Yes' para votar na Nega Diaba!</span><input name='aprove' id='aprove'   type='button' value='Yes' style='height:30;width:80;left:120;top:250;position:absolute;font:12 px verdana arial helvetica sans-serif;border:1px solid;'>";
		$msMessage .="<input name='deny' id='deny'   type='button' value='No' style='height:30;width:80;left:10;top:250;position:absolute;font:12 px verdana arial helvetica sans-serif;border:1px solid;'><input name='close' id='close'   type='button' value='CLOSE'  style='height:30;width:80;left:230;top:250;position:absolute;font:12 px verdana arial helvetica sans-serif;border:1px solid;'></div>";

		$moJs = new FWDJsEvent(JS_CREATE_ELEM,"",$msMessage);
		echo $moJs->render();
	}

}

$soWebLib->xml_load("FWD5_SYS_EX_BASIC_EVENTS_MENU.xml");

$soStartEvent = FWDStartEvent::getInstance();
$soStartEvent->addAjaxEvent(new ExampleEventsMenu_GetData("getData"));
$soStartEvent->addAjaxEvent(new ExampleEventsMenu_CreateMessageBox("createMessageBox"));

$soWebLib->dump_html($soWebLib->getObject("dialog"));

?>