<?php

include_once "include.php";
include_once "../formats/FWDDataSetExcel.php";

$soWebLib = FWDWebLib::getInstance();

class AssetLevelIterator extends FWDReportLevelIterator {
	public function __construct() {
		parent::__construct("Asset_Id");
	}
	function fetch(FWDDBDataSet $poDataSet)  {
		$msFieldName = $poDataSet->getFieldByAlias("Asset_Name");
		$moWebLib = FWDWebLib::getInstance();
		$moWebLib->getObject("asset_name")->setValue("Ativo: {$msFieldName->getValue()}");
	}
}

class RiskLevelIterator extends FWDReportLevelIterator {
	public function __construct() {
		parent::__construct("Risk_Id");
	}
	public function fetch(FWDDBDataSet $poDataSet)  {
		$moFieldName = $poDataSet->getFieldByAlias("Risk_Name");
		$moFieldValue = $poDataSet->getFieldByAlias("Risk_Value");
		$moFieldStatus = $poDataSet->getFieldByAlias("Risk_Accept_Mode");
		$moFieldResidual = $poDataSet->getFieldByAlias("Risk_Value_Residual");

		$moWebLib = FWDWebLib::getInstance();
		$moRiskName = $moWebLib->getObject("risk_name");
		$moRiskValue = $moWebLib->getObject("risk_value");
		$moRiskStatus = $moWebLib->getObject("risk_status");
		$moRiskValueResidual = $moWebLib->getObject("risk_value_residual");

		$moRiskName->setValue($moFieldName->getValue());
		$moRiskValue->setValue($moFieldValue->getValue());
		$moRiskStatus->setValue($moFieldStatus->getValue());
		$moRiskValueResidual->setValue($moFieldResidual->getValue());
	}
}

class ControlLevelIterator extends FWDReportLevelIterator {
	public function __construct() {
		parent::__construct("Control_Id");
	}
	public function fetch(FWDDBDataSet $poDataSet)  {
		$moFieldName = $poDataSet->getFieldByAlias( "Control_Name");
		$moFieldResp = $poDataSet->getFieldByAlias( "Responsible_Name");
		$moFieldDead = $poDataSet->getFieldByAlias( "Deadline");

		$moWebLib = FWDWebLib::getInstance();
		$moControlName = $moWebLib->getObject("control_name");
		$moControlResp = $moWebLib->getObject("control_resp");
		$moControlDeadLine = $moWebLib->getObject("control_deadline");

		$moControlName->setValue($moFieldName->getValue());
		$moControlResp->setValue($moFieldResp->getValue());
		$moControlDeadLine->setValue($moFieldDead->getValue());
	}
}

$soWebLib->xml_load("FWD5_SYS_EX_REPORT_PTR.xml");

$soWebLib->getObject("level_asset")->setLevelIterator(new AssetLevelIterator());
$soWebLib->getObject("level_risk")->setLevelIterator(new RiskLevelIterator());
$soWebLib->getObject("level_control")->setLevelIterator(new ControlLevelIterator());

$soReport = $soWebLib->getObject("report");
$soReport->setObjFWDBox(new FWDBox(0,0,00,700));

$saFormat = $soWebLib->getObject('slFormat')->getValue();

if($saFormat=="0"){
	$soWebLib->dump_html($soWebLib->getObject("dialog"));
}else{
	$ssFormat = $saFormat;
	if($ssFormat=='pdf'){
		$sfFactor = $soWebLib->getObject('txFactor')->getValue();
		$soReport->setWriter(new FWDReportPDFWriter($sfFactor));
		$soReport->draw();
	}elseif($ssFormat=='doc'){
		$sfFileName = $soWebLib->getObject('txFileName')->getValue();
		$soReport->setWriter(new FWDReportDocWriter($sfFileName));
		$soReport->draw();
	}elseif($ssFormat=='xls'){
		$sfFileName = $soWebLib->getObject('txFileName')->getValue();
		$soDataSetExcel = new FWDDataSetExcel($sfFileName,$soReport->getDataSet());
		$soDataSetExcel->execute();
	}else{
		$soReport->setWriter(new FWDReportHTMLWriter());
		$soWebLib->dump_html($soReport);
	}
}

?>