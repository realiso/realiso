<?php
include_once "include.php";


class QueryTest extends FWDDBQueryHandler {

	public function __construct($poDB) {
		parent::__construct($poDB);

		$this->coDataSet->addFWDDBField(new FWDDBField("coluna1","coluna1", DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("coluna2","coluna2", DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("coluna3","coluna3", DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField("coluna4","coluna4", DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField("coluna5","coluna5", DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("coluna6","coluna6", DB_STRING));

	}

	public function makeQuery() {
		$this->csSQL = "select skTemplate as coluna1, skSectionTemplate as coluna2, zName as coluna3, zDescription as coluna4, nControlType as coluna5, zImplementationGuide as coluna6 from rm_template";
	}
}

class gridValue extends FWDRunnable
{
	function run()
	{
		$moWebLib = FWDWebLib::getInstance();
		$moGrid = $moWebLib->getObject("grid2");
		$maValue = $moGrid->getValue();
		$msValueSel ='';
		foreach ($maValue as $maValueRow)
		foreach ($maValueRow as $msValue)
		$msValueSel .= $msValue.'<>';
			
		echo "alert('getValue = {$msValueSel}');";
		$msValueSel='';
		$maColSelect = $moGrid->getArraySelectColumns(2);
		foreach ($maColSelect as $msValue)
		$msValueSel .= $msValue.'<>';
			
		echo "alert('getArraySelectColumns(5) = {$msValueSel}');";
	}
}

class EventBeforeAjax extends FWDRunnable {
	public function run() {
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent("screen_event"));
		$soEventHandler = FWDStartEvent::getInstance();
		$soEventHandler->addAjaxEvent(new gridValue("test_grid_value"));
		$soQueryHandler = new QueryTest(FWDWebLib::getConnection());
		FWDWebLib::getObject('grid2')->setQueryHandler($soQueryHandler);
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$moWebLib = FWDWebLib::getInstance();
		$moWebLib->dump_html($moWebLib->getObject('dialog'));
	}
}
$soWebLib = FWDWebLib::getInstance();
$soStartEvent = FWDStartEvent::getInstance();
$soStartEvent->addBeforeAjax(new EventBeforeAjax("before_ajax"));
$soWebLib->xml_load("FWD5_SYS_EX_DB_GRID_STATIC.xml");
?>