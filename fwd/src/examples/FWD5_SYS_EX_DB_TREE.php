<?php
include_once "include.php";

$soWebLib = FWDWebLib::getInstance();
$soWebLib->xml_load("FWD5_SYS_EX_DB_TREE.xml");

// extende a classe FWDNodeBuilder para incluir CheckBoxes nos nodos
class ExampleNodeBuilder extends FWDNodeBuilder{
	public function buildNode($psId, $psValue){
		$moNode = new FWDTree();
		$moNode->setAttrName($psId);
		$moStatic = new FWDStatic();
		$moStatic->setValue($psValue);
		$moCheck = new FWDCheckBox();
		$moCheck->setAttrName('check'.$psId);
		$moCheck->setAttrController('controller1');
		$moCheck->execute();
		$moNode->addObjFWDView($moCheck);
		$moNode->addObjFWDView($moStatic);
		return $moNode;
	}
}

// Inicializa o DataSet
$soDataSet = new FWDDBDataSet($soWebLib->getConnection());
$soDataSet->addFWDDBField(new FWDDBField('','id', DB_STRING));
$soDataSet->addFWDDBField(new FWDDBField('','level', DB_NUMBER));
$soDataSet->addFWDDBField(new FWDDBField('','value', DB_STRING));
$soDataSet->setQuery("SELECT skCategory as id, zName as value, nLevel as level FROM isms.make_tree_category(null,-1)");

// Constr�i a DBTree
$soDbTree = $soWebLib->getObject('dbTree1');
$soDbTree->setDataSet($soDataSet);
$soDbTree->setNodeBuilder(new ExampleNodeBuilder());
$soDbTree->buildTree();

// Constr�i a DBTreeBase
$soDbTreeBase1 = $soWebLib->getObject('dbTreeBase1');
$soDbTreeBase1->setDataSet($soDataSet);
$soDbTreeBase1->buildTree();

$soWebLib->dump_html($soWebLib->getObject("dialog"));

?>