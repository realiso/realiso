<?php

include_once "include.php";

$soWebLib = FWDWebLib::getInstance();

$soWebLib->xml_load("FWD5_SYS_EX_BASIC_RADIO_CHECK_SELECT.xml");

$soWebLib->dump_html($soWebLib->getObject("dialog"));

?>

<script>
	function checkPOST() {
		document.location.href='./FWD5_SYS_EX_BASIC_POST_CHECK.php';
	}
	function radioPOST() {
		document.location.href='./FWD5_SYS_EX_BASIC_POST_RADIO.php';
	}
	function selectPOST() {
		document.location.href='./FWD5_SYS_EX_BASIC_POST_SELECT.php';
	}
</script>
