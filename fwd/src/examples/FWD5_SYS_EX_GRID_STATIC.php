<?php

include_once "include.php";

class EventBeforeAjax extends FWDRunnable {
	public function run() {
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent("screen_event"));
		$moWebLib = FWDWebLib::getInstance();
		$moGrid2 = $moWebLib->getObject('Grid2');

		//obter o conteudo da grid com preenchimento via ajax e inserir ele na grid
		for( $miRow = 1; $miRow < 100; $miRow++)
		for( $col = 1; $col <7; $col++)
		if($col == 3)
		$moGrid2->SetItem($col, $miRow, $miRow);
		else
		$moGrid2->SetItem($col, $miRow, $miRow."---".$col);
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$moWebLib = FWDWebLib::getInstance();
		$moWebLib->dump_html($moWebLib->getObject('dialog'));

	}

}

$soWebLib = FWDWebLib::getInstance();
$soStartEvent = FWDStartEvent::getInstance();
$soStartEvent->addBeforeAjax(new EventBeforeAjax("before_ajax"));

$soWebLib->xml_load("FWD5_SYS_EX_GRID_STATIC.xml");
?>