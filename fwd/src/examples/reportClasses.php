<?php
class ismsReportPTR extends FWDReport{
	public function __construct(){
		parent::__construct();
		$soDB = new FWDDB(DB_MSSQL, "isms_current5", "isms", "isms", "192.168.0.131");
		$this->setDataSet(new FWDDBDataSet($soDB));
		$this->makeQuery();
	}
	public function makeQuery(){
		$this->addField("", "Asset_Id");
		$this->addField("", "Asset_Name");
		$this->addField("", "Asset_Value");
		$this->addField("", "Risk_Id");
		$this->addField("", "Risk_Name");
		$this->addField("", "Risk_Value");
		$this->addField("", "Risk_Accept_Mode");
		$this->addField("", "Risk_Value_Residual");
		$this->addField("", "Control_Id");
		$this->addField("", "Control_Name");
		$this->addField("", "Responsible_Id");
		$this->addField("", "Responsible_Name");
		$this->addField("", "Deadline");
		$this->addField("", "Control_Is_Active");
		$ssQuery = "SELECT A.skAsset as Asset_Id, A.zName as Asset_Name, A.nValue as Asset_Value, R.skRisk as Risk_Id, R.zName as Risk_Name, R.nValue as Risk_Value, R.bAcceptRisk as Risk_Accept_Mode, R.nValueResidual as Risk_Value_Residual, C.skControl as Control_Id, C.zName as Control_Name, C.skUser as Responsible_Id, U.zName as Responsible_Name, C.dDateImplementation as Deadline, C.bIsActive as Control_Is_Active FROM rm_asset A LEFT JOIN rm_risk R ON ( A.skAsset = R.skAsset ) LEFT JOIN rm_risk_control RC ON ( R.skRisk = RC.skRisk ) LEFT JOIN rm_control C ON ( RC.skControl = C.skControl ) LEFT JOIN isms_user U ON ( C.skUser = U.skUser ) WHERE R.nValue > 3 ORDER BY A.skAsset";
		$this->csQuery = $ssQuery;
		return parent::executeQuery();
	}
}
?>