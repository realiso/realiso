<?php
include_once "include.php";

$soDataSet = new FWDDBDataSet(FWDWebLib::getConnection(), "rm_area");

$soFieldArea = new FWDDBField("skArea", "rm_area_id", DB_NUMBER, "", false);
$soFieldUser = new FWDDBField("skUser", "rm_user_id", DB_NUMBER);
$soFieldName = new FWDDBField("zName", "rm_area_name", DB_STRING);
$soFieldValue = new FWDDBField("nValue", "rm_area_value", DB_NUMBER);

$soDataSet->addFWDDBField($soFieldArea);
$soDataSet->addFWDDBField($soFieldUser);
$soDataSet->addFWDDBField($soFieldName);
$soDataSet->addFWDDBField($soFieldValue);

$soFieldUser->setValue(1);
$soFieldName->setValue("AREA_1");
$soFieldValue->setValue(1);

echo $soDataSet->insert() . "<br>";

$soFieldUser->setValue(1);
$soFieldName->setValue("AREA_2");
$soFieldValue->setValue(2);

echo $soDataSet->insert() . "<br>";

$soFieldUser->setValue(1);
$soFieldName->setValue("AREA_3");
$soFieldValue->setValue(3);

echo $soDataSet->insert() . "<br>";

/*echo $soDataSet->update() . "<br>";
 echo $soDataSet->delete() . "<br>";*/

/*$soFilter = new FWDDBFilter("in", 2);
 $soFilter->addValue(3);
 $soFieldValue->addFilter($soFilter);*/

$soFilter2 = new FWDDBFilter(">", 100);
$soFieldArea->addFilter($soFilter2);

$soDataSet->setOrderBy("rm_area_value", "+");
$soDataSet->setOrderBy("rm_area_name", "+");

echo $soDataSet->select() . "<br>";

/*$soAreaName = $soDataSet->getFieldByAlias("rm_area_name");
 $soAreaName->setValue("AREA_EDITADA");*/

//$soDataSet->delete();

while ($soDataSet->fetch()) {
	$soRmAreaName = $soDataSet->getFieldByAlias("rm_area_name");
	$soRmAreaValue = $soDataSet->getFieldByAlias("rm_area_value");
	echo $soRmAreaName->getValue() . " VALOR: " . $soRmAreaValue->getValue() . "<br>";
}

?>