<?php

include_once "include.php";

$soWebLib = FWDWebLib::getInstance();
$soWebLib->xml_load("FWD5_SYS_EX_DB_DATA_BINDER.xml");

class MyEntity extends FWDDBEntity {
	public function __construct(FWDDB $poDB) {
		parent::__construct($poDB, "rm_area");
		$this->addField(new FWDDBField("skArea", "rm_area_id", DB_NUMBER, "", false));
		$this->addField(new FWDDBField("skUser", "rm_area_user", DB_NUMBER));
		$this->addField(new FWDDBField("zName", "rm_area_name", DB_STRING));
		$this->addField(new FWDDBField("nValue", "rm_area_value", DB_NUMBER));
	}
}

class UpdateEvent extends FWDRunnable {
	public function run(){
		$moBind = FWDWebLib::getObject("binder");
		$moBind->update();
	}
}

class InsertEvent extends FWDRunnable {
	public function run(){
		$moBind = FWDWebLib::getObject("binder");
		$moBind->insert();
	}
}

$soStartEvent = FWDStartEvent::getInstance();
$soStartEvent->addSubmitEvent(new InsertEvent("Button1"));
$soStartEvent->addSubmitEvent(new UpdateEvent("Button2"));

$soEntity = new MyEntity(FWDWebLib::getConnection());
$soDataBinder = new FWDDBDataBinder($soEntity);
$soDataBinder->setKey("rm_area_id", 1508);

$soDataBinder->addView($soWebLib->getObject("text0"), "rm_area_user");
$soDataBinder->addView($soWebLib->getObject("text1"), "rm_area_name");
$soDataBinder->addView($soWebLib->getObject("text2"), "rm_area_value");

if (!$soWebLib->getObject('text1')->getValue()) $soDataBinder->populate();

$soWebLib->addObject("binder", $soDataBinder);
$soWebLib->dump_html($soWebLib->getObject("dialog"));

?>