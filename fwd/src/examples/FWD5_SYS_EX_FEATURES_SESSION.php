<?php

include_once "include.php";

$soWebLib = FWDWebLib::getInstance();
$soWebLib->xml_load('FWD5_SYS_EX_FEATURES_SESSION.xml');

// � preciso definir uma classe que extenda a classe FWDSession e defina o m�todo login()
class ExampleSession extends FWDSession{
	public function __construct($psId){
		parent::__construct($psId);
		$this->addAttribute('login');
	}
	public function login($psUser,$psPassword){
		$mbRet = ($psUser!='' && $psPassword==$psUser);
		$this->setLoggedIn($mbRet);
		return $mbRet;
	}
}

// cria/carrega a sess�o
$soSession = $soWebLib->getSession(new ExampleSession('example'));

// indica a operacao
$ssOp = FWDWebLib::getObject('op')->getValue();

// mensagem a ser exibida
$ssMsg = '';

// executa a opera��o
if($ssOp=='login'){
	$ssLogin = FWDWebLib::getObject('txLogin')->getValue();
	$ssPassword = FWDWebLib::getObject('txPassword')->getValue();
	if($soSession->login($ssLogin,$ssPassword)){
		$soSession->setAttrLogin($ssLogin);
	}else{
		$ssMsg = 'Usu�rio ou senha inv�lidos.';
	}
	$soWebLib->getObject('txPassword')->setValue('');
}elseif($ssOp=='logout'){
	$soSession->logout();
	$ssMsg = 'Usu�rio deslogado.';
}

// se est� logado, mostra a tela "interna"
if($soSession->isLoggedIn()){
	$soWebLib->getObject('Viewgroup1')->setAttrDisplay('false');
	$soWebLib->getObject('Viewgroup2')->setAttrDisplay('true');
	$ssMsg = "Usu�rio logado: {$soSession->getAttrLogin()}";
}

$soWebLib->getObject('stMsg')->setValue($ssMsg);

$soWebLib->dump_html($soWebLib->getObject("dialog"));

?>