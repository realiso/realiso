<?php

include_once "include.php";

$soWebLib = FWDWebLib::getInstance();
$soWebLib->xml_load('exampleFWDSessionParameter_SEND.xml');

$soSession = $soWebLib->getSession(new SessionParameterExample('example_session'));

if(!$soSession->attributeExists('att1')) $soSession->addAttribute('att1');
$soSession->setAttrAtt1(1);

if(!$soSession->attributeExists('att2')) $soSession->addAttribute('att2');
$soSession->setAttrAtt2(2);

if(!$soSession->attributeExists('att3')) $soSession->addAttribute('att3');
$soSession->setAttrAtt3("STRING");

$soWebLib->dump_html($soWebLib->getObject("dialog"));
?>