<?php

include_once "include.php";

include_once "../formats/FWDGridExcel.php";

$soWebLib = FWDWebLib::getInstance();

$soWebLib->xml_load("FWD5_SYS_EX_GRID_TO_EXCEL.xml");

class ExportGrid0 extends FWDRunnable {

	public function run() {
		$moGrid0 = FWDWebLib::getObject("grid0");
		$moGridExcel = new FWDGridExcel("GridZero",array($moGrid0));
		$moGridExcel->execute();
	}

}

class ExportGrid2 extends FWDRunnable {

	public function run() {
		$moGrid2 = FWDWebLib::getObject("grid2");
		$moGridExcel = new FWDGridExcel("GridDois",array($moGrid2));
		$moGridExcel->execute();
	}

}

class ExportGridAll extends FWDRunnable {

	public function run() {
		$moGrid0 = FWDWebLib::getObject("grid0");
		$moGrid2 = FWDWebLib::getObject("grid2");
		$moGridExcel = new FWDGridExcel("AllGrids",array($moGrid0,$moGrid2));
		$moGridExcel->execute();
	}
}


$soGrid0 = $soWebLib->getObject("grid0");
$soGrid2 = $soWebLib->getObject("grid2");
//conte�do da grid est�tica
for ($siRow=1; $siRow<133; $siRow++)
for ($siCol=1; $siCol<7; $siCol++)
{
	$grid2->SetItem($siCol, $siRow, $siRow."---".$siCol);
	$grid0->SetItem($siCol, $siRow, $siRow."---".$siCol);
}


$soGridDataSet = new FWDDBDataSet(FWDWebLib::getConnection());

$soGridDataSet->addFWDDBField(new FWDDBField("skTemplate", "coluna01", DB_NUMBER, "", false));
$soGridDataSet->addFWDDBField(new FWDDBField("skSectionTemplate", "coluna02", DB_NUMBER));
$soGridDataSet->addFWDDBField(new FWDDBField("zName", "coluna03", DB_STRING));
$soGridDataSet->addFWDDBField(new FWDDBField("zDescription", "coluna04", DB_STRING));
$soGridDataSet->addFWDDBField(new FWDDBField("nControlType", "coluna05", DB_NUMBER));
$soGridDataSet->addFWDDBField(new FWDDBField("zImplementationGuide", "coluna06", DB_STRING));


$soWebLib->getObject('grid2')->setDataSet($soGridDataSet);
$soWebLib->getObject('grid2')->setQuery("select skTemplate as coluna01, skSectionTemplate as coluna02, zName as coluna03, zDescription as coluna04, nControlType as coluna05, zImplementationGuide as coluna06 from rm_template");
$soWebLib->getObject('grid2')->populate();


$soStartEvent = FWDStartEvent::getInstance();
$soStartEvent->addSubmitEvent(new ExportGrid0("Button_Grid0"));
$soStartEvent->addSubmitEvent(new ExportGrid2("Button_Grid2"));
$soStartEvent->addSubmitEvent(new ExportGridAll("Button_GridAll"));

$soWebLib->dump_html($soWebLib->getObject("dialog"));
?>
