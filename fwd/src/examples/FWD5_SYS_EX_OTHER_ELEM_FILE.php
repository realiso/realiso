<?php

include_once "include.php";

function download(){
	FWDWebLib::forceDownload('dummy.txt');
	echo 'teste';
	exit();
}

class SubmitEvent extends FWDRunnable {
	public function run(){
		/*
		 $msDebug = '$_POST = '.str_replace(array("\n","'"),array('\\n',"\'"),var_export($_POST,true)).';\\n';
		 $msDebug.= '$_FILES = '.str_replace(array("\n","'"),array('\\n',"\'"),var_export($_FILES,true));
		 echo("<script>alert('$msDebug');</script>");
		 //*/
		$moFile = FWDWebLib::getObject('File1');
		$miError = $moFile->getErrorCode();
		if($miError==FWDFile::E_NONE){
			$msMsg = "Arquivo enviado com sucesso.".'\\n'
			."Nome: {$moFile->getFileName()}".'\\n'
			."Tamanho: {$moFile->getFileSize()} Bytes".'\\n'
			."Tipo: {$moFile->getFileExt()}".'\\n';
		}else{
			$msMsg = 'Erro: arquivo inv�lido.\\n';
			if($miError & FWDFile::E_MAX_SIZE) $msMsg.= 'Tamanho m�ximo excedido.\\n';
			if($miError & FWDFile::E_EXTENSION) $msMsg.= 'Extens�o n�o permitida.';
		}
		?>
<script language="javascript">alert('<?=$msMsg?>');</script>
		<?
	}
}

class DownloadEvent extends FWDRunnable {
	public function run(){
		download();
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(''));
		$moStartEvent->addSubmitEvent(new SubmitEvent('upload_ajax'));
		$moStartEvent->addSubmitEvent(new DownloadEvent('download'));
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){

		if(FWDWebLib::getObject('download')->getValue()){
			download();
		}

		$moFile = FWDWebLib::getObject('File1');
		if($moFile->isUploaded()){
			$miError = $moFile->getErrorCode();
			if($miError==FWDFile::E_NONE){
				$msMsg = "<h3>Arquivo enviado com sucesso.</h3><br>"
				."Nome: {$moFile->getFileName()}<br>"
				."Tamanho: {$moFile->getFileSize()} Bytes<br>"
				."Tipo: {$moFile->getFileExt()}";
			}else{
				$msMsg = 'Erro: arquivo inv�lido.';
				if($miError & FWDFile::E_MAX_SIZE) $msMsg.= '<br>Tamanho m�ximo excedido.';
				if($miError & FWDFile::E_EXTENSION) $msMsg.= '<br>Extens�o n�o permitida.';
			}
			FWDWebLib::getObject('stMsg')->setValue($msMsg);
		}

		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('FWD5_SYS_EX_OTHER_ELEM_FILE.xml');

?>