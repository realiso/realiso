<?php

require_once('include.php');
require_once("$base_ref/translation/include.php");

// Inicializa o DataSet pro driver de BD
$soDB = new FWDDB(DB_MSSQL, "Axur_translations", "sa", "isms", "192.168.0.131");
$soDataSet = new FWDDBDataSet($soDB, 'exemplo');
$soDataSet->addFWDDBField(new FWDDBField('id','trans_id',DB_STRING));
$soDataSet->addFWDDBField(new FWDDBField('arquivo','trans_file',DB_STRING));
$soDataSet->addFWDDBField(new FWDDBField('valor','trans_value',DB_STRING));
$soDataSet->addFWDDBField(new FWDDBField('data_inclusao','trans_date',DB_DATETIME));

// Cria o driver de BD usando o DataSet
$soDBDriver = new FWDDBDriver($soDataSet);

// Cria o driver de XML
$soXMLDriver = new FWDXMLDriver('exampleStringsXML.xml');

// Cria o objeto de tradu��o usando o driver de XML
$soTranslate = new FWDTranslate($soXMLDriver);

// Extrai todas as strings de todos arquivos .xml da pasta atual e usa o driver de XML para armazen�-las
$soTranslate->extractXMLStrings('.');

// Muda o driver do objeto de tradu��o para o driver de BD
$soTranslate->setDriver($soDBDriver);

// Extrai todas as strings de cada um dos arquivos usando o driver de BD, ou seja, armazena no banco
$soTranslate->extractXMLStrings('FWD5_SYS_EX_OTHER_ELEM_FILE.xml');
$soTranslate->extractXMLStrings('FWD5_SYS_EX_OTHERS_COLLECT.xml');
$soTranslate->extractXMLStrings('FWD5_SYS_EX_BASIC_MEMO_TEXT.xml');
$soTranslate->extractXMLStrings('FWD5_SYS_EX_BASIC_CALENDARIO.xml');
$soTranslate->extractXMLStrings('FWD5_SYSTEM_EXAMPLE_OTHER_ELEM.xml');
$soTranslate->extractXMLStrings('FWD5_SYSTEM_EXAMPLE_OTHERS.xml');
$soTranslate->extractXMLStrings('FWD5_SYSTEM_EXAMPLE_MAIN.xml');
$soTranslate->extractXMLStrings('FWD5_SYSTEM_EXAMPLE_GRID.xml');
$soTranslate->extractXMLStrings('FWD5_SYSTEM_EXAMPLE_FEATURES.xml');
$soTranslate->extractXMLStrings('FWD5_SYSTEM_EXAMPLE_BD.xml');
$soTranslate->extractXMLStrings('FWD5_SYSTEM_EXAMPLE_BASIC.xml');
$soTranslate->extractXMLStrings('exampleBenchMark.xml');
$soTranslate->extractXMLStrings('exampleFWDScrolling.xml');
$soTranslate->extractXMLStrings('FWD5_SYS_EX_FEATURES_MASK.xml');

// Armazena num arquivo .php as strings lidas com driver de BD
$soTranslate->storeToPHPFile('exampleStrings.php');

// Cria outro driver de XML
$soXMLDriver2 = new FWDXMLDriver('exampleStringsPHP.xml');

// Muda o driver do objeto de tradu��o para o novo driver de XML
$soTranslate->setDriver($soXMLDriver2);

// Extrai todas as strings de cada um dos arquivos PHP usando o novo driver de XML
$soTranslate->extractPHPStrings($base_ref.'view',false);

?>