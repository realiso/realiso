<?php

include_once "include.php";

$soWebLib = FWDWebLib::getInstance();

$soWebLib->xml_load("FWD5_SYS_EX_DB_SELECT.xml");

$soSelectQuery = new FWDSelectQuery(FWDWebLib::getConnection());
$soSelectQuery->setId("skAcl");
$soSelectQuery->setValue("zDescription");
$soSelectQuery->setTable("isms_acl");

$soWebLib->getObject('select0')->setDataSet($soSelectQuery);
$soWebLib->getObject('select0')->populate();

$soSelectQuery->setId("skAuditLog");
$soSelectQuery->setValue("zName");
$soSelectQuery->setTable("isms_audit_log");

$soWebLib->getObject('select1')->setDataSet($soSelectQuery);
$soWebLib->getObject('select1')->populate();

$soSelectQuery->setId("skUser");
$soSelectQuery->setValue("zName");
$soSelectQuery->setTable("isms_user");

$soWebLib->getObject('select2')->setDataSet($soSelectQuery);
$soWebLib->getObject('select2')->populate();

$soWebLib->dump_html($soWebLib->getObject("dialog"));
?>