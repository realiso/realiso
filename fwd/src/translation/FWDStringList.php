<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDStringList. Descreve o tipo de dados StringList.
 *
 * <p>Classe que descreve o tipo de dados StringList. Uma StringList � uma lista
 * de strings identificadas pelo seu id organizadas pelos nomes dos arquivos de
 * onde elas foram extra�das.</p>
 * @abstract
 * @package FWD5
 * @subpackage translation
 */
class FWDStringList {

	/**
	 * Array que armazena a StringList em si
	 * @var array
	 * @access protected
	 */
	protected $caList;

	/**
	 * Construtor da FWDStringList.
	 *
	 * <p>Construtor da FWDStringList. Inicializa a StringList com uma lista vazia.</p>
	 * @access public
	 */
	public function __construct(){
		$this->caList = array();
	}

	/**
	 * Adiciona um arquivo � StringList.
	 *
	 * <p>Adiciona um arquivo � StringList.</p>
	 * @access public
	 * @param string $psFileName Nome do arquivo a ser adicionado
	 */
	public function addFile($psFileName){
		$this->caList[$psFileName] = array();
	}

	/**
	 * Remove um arquivo da StringList.
	 *
	 * <p>Remove um arquivo da StringList.</p>
	 * @access public
	 * @param string $psFileName Nome do arquivo a ser removido
	 */
	public function removeFile($psFileName){
		if(isset($this->caList[$psFileName])){
			unset($this->caList[$psFileName]);
		}else{
			trigger_error("Trying to remove inexistent file '$psFileName'.",E_USER_WARNING);
		}
	}

	/**
	 * Adiciona uma string � StringList.
	 *
	 * <p>Adiciona uma string � StringList.</p>
	 * @access public
	 * @param string $psFileName Nome do arquivo a que a string pertence
	 * @param string $psStringId Identificador da string a ser adicionada
	 * @param string $psStringValue Valor da string a ser adicionada
	 */
	public function addString($psFileName,$psStringId,$psStringValue){
		if(isset($this->caList[$psFileName])){
			$this->caList[$psFileName][$psStringId] = $psStringValue;
		}else{
			$this->caList[$psFileName] = array($psStringId => $psStringValue);
		}
	}

	/**
	 * Remove uma string da StringList.
	 *
	 * <p>Remove uma string da StringList.</p>
	 * @access public
	 * @param string $psFileName Nome do arquivo a que a string pertence
	 * @param string $psStringId Identificador da string a ser removida
	 */
	public function removeString($psFileName,$psStringId){
		if(isset($this->caList[$psFileName])){
			unset($this->caList[$psFileName][$psStringId]);
		}else{
			trigger_error("Trying to remove string from inexistent file '$psFileName'.",E_USER_WARNING);
		}
	}

	/**
	 * Seta o valor de uma string da StringList.
	 *
	 * <p>Seta o valor de uma string da StringList.</p>
	 * @access public
	 * @param string $psFileName Nome do arquivo a que a string pertence
	 * @param string $psStringId Identificador da string
	 * @param string $psStringValue Novo valor da string
	 */
	public function setString($psFileName,$psStringId,$psStringValue){
		if(isset($this->caList[$psFileName])){
			$this->caList[$psFileName][$psStringId] = $psStringValue;
		}else{
			trigger_error("Trying to set a string from inexistent file '$psFileName'.",E_USER_WARNING);
		}
	}

	/**
	 * Retorna o valor de uma string da StringList.
	 *
	 * <p>Retorna o valor de uma string da StringList.</p>
	 * @access public
	 * @param string $psFileName Nome do arquivo a que a string pertence
	 * @param string $psStringId Identificador da string
	 */
	public function getString($psFileName,$psStringId){
		if(isset($this->caList[$psFileName]) && isset($this->caList[$psFileName][$psStringId])){
			return $this->caList[$psFileName][$psStringId];
		}else{
			return false;
		}
	}

	/**
	 * Retorna um array associativo contendo todas as strings da StringList.
	 *
	 * <p>Retorna um array associativo contendo todas as strings da StringList.</p>
	 * @access public
	 * @return array Array associativo contendo todas as strings
	 */
	public function dump(){
		return $this->caList;
	}

}
?>