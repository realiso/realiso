<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDTranslate. Classe para ajudar na tradu��o das strings.
 *
 * <p>Classe para ajudar na tradu��o das strings. Ela extrai listas de strings
 * de arquivos XML, exporta e importa essas listas.</p>
 *
 * @package FWD5
 * @subpackage translation
 */
class FWDTranslate extends FWDXMLParser {

	/**
	 * Driver para fazer a leitura e/ou a escrita de StringList's em um meio de armazenamento
	 * @var FWDTranslationDriver
	 * @access protected
	 */
	protected $coDriver;

	/**
	 * StringList interna ao objeto. Usada durante as opera��es de leitura e escrita.
	 * @var FWDStringList
	 * @access protected
	 */
	protected $coStringList = null;

	/**
	 * Usado durante o parse do arquivo
	 * @var string
	 * @access protected
	 */
	protected $csFileBeingParsed = '';

	/**
	 * Usado durante o parse do arquivo
	 * @var array
	 * @access protected
	 */
	protected $caCurrentString = null;

	/**
	 * Construtor da classe FWDTranslate.
	 *
	 * <p>Construtor da classe FWDTranslate. Associa um driver ao objeto.</p>
	 * @access public
	 * @param FWDTranslationDriver $poDriver Driver a ser associado ao objeto
	 */
	public function __construct(FWDTranslationDriver $poDriver){
		$this->coDriver = $poDriver;
	}

	/**
	 * Modifica o driver associado ao objeto.
	 *
	 * <p>Modifica o driver associado ao objeto.</p>
	 * @access public
	 * @param FWDTranslationDriver $poDriver Driver a ser associado ao objeto
	 */
	public function setDriver(FWDTranslationDriver $poDriver){
		if($this->coStringList==null){
			$this->coDriver = $poDriver;
		}else{
			trigger_error("The driver can't be changed while an operation is in progress.",E_USER_WARNING);
		}
	}

	/**
	 * Lista os arquivos de uma pasta com uma determinada extens�o
	 *
	 * <p>Percorre uma pasta buscando arquivos com a extens�o dada e retorna uma
	 * lista com o caminho de cada um deles.</p>
	 * @access protected
	 * @param string $psPath Pasta a ser percorrida
	 * @param boolean $pbRecursive Indica se as subpastas devem ser percorridas tamb�m
	 * @param string $psExtension Extens�o desejada
	 * @return array Lista com os caminhos dos arquivos
	 */
	protected function listFiles($psPath,$pbRecursive=false,$psExtension){
		$maFiles = array();
		$maSubDirs = array();
		$moDir = dir($psPath);
		$psExtension = strtolower($psExtension);
		while(($msFileName=$moDir->read())!==false){
			$msFullPath = "$psPath/$msFileName";
			if(is_file($msFullPath)){
				$msExt = strtolower(substr($msFileName,strrpos($msFileName,'.')+1));
				if($msExt==$psExtension) $maFiles[] = $msFullPath;
			}elseif($pbRecursive && $msFileName!='.' && $msFileName!='..'){
				$maSubDirs[] = $msFileName;
			}
		}
		$moDir->close();
		if($pbRecursive){
			foreach($maSubDirs as $msDirName){
				$msFullPath = "$psPath/$msDirName";
				$maFiles = array_merge($maFiles,$this->listFiles($msFullPath,true,$psExtension));
			}
		}
		return $maFiles;
	}

	/**
	 * Extrai as strings de um arquivo XML
	 *
	 * <p>Extrai as strings de um arquivo XML e as inclui na StringList interna.</p>
	 * @access protected
	 * @param string $psFileName Arquivo XML
	 * @return boolean True, indica sucesso
	 */
	protected function singleExtractXMLStrings($psFileName){
		$this->csFileBeingParsed = $psFileName;
		$this->coStringList->addFile($psFileName);
		$mbParse = $this->parseFile($psFileName);
		$this->csFileBeingParsed = '';
		if(!$mbParse) $this->coStringList->removeFile($psFileName);
		return $mbParse;
	}

	/**
	 * Extrai as strings de um arquivo PHP
	 *
	 * <p>Extrai as strings de um arquivo PHP (chamadas de FWDLanguage::getPHPStringValue())
	 * e as inclui na StringList interna.
	 * As chamadas do m�todo getPHPStringValue com par�metros que n�o s�o strings
	 * constantes s�o consideradas inv�lidas e geram um warning.</p>
	 * @access protected
	 * @param string $psFileName Arquivo PHP
	 * @return boolean True, indica sucesso
	 */
	protected function singleExtractPHPStrings($psFileName){
		if(!file_exists($psFileName) || !is_readable($psFileName)){
			trigger_error("Could not open file '$psFileName'.",E_USER_WARNING);
			return false;
		}
		$this->csFileBeingParsed = $psFileName;
		$this->coStringList->addFile($psFileName);
		$mrFile = fopen($psFileName,'r');
		if(filesize($psFileName)>0)
		$msFileContent = fread($mrFile,filesize($psFileName));
		else{
			$msFileContent = '';
		}

		fclose($mrFile);
		$miFileSize = strlen($msFileContent);
		$i = 0;
		$msNeedle = 'FWDLanguage::getPHPStringValue';
		$miNeedleLength = strlen($msNeedle);
		$miParamStart=0;
		while($i<$miFileSize){
			$i = strpos($msFileContent,$msNeedle,$i);
			if($i===false) break;
			$mbInDblQuote = false;
			$mbInSngQuote = false;
			$i+= $miNeedleLength;
			$miStart = $i;
			$miEnd = -1;
			$mbValid = true;
			$maParams = array();
			while($miEnd==-1 && $i<$miFileSize){
				if(!$mbInSngQuote && !$mbInDblQuote){
					if(strpos(" '\"()\n\t,",$msFileContent[$i])===false) $mbValid = false;
				}
				switch($msFileContent[$i]){
					case '"':
						if(!$mbInSngQuote){
							if($mbInDblQuote){
								$mbInDblQuote = false;
								$maParams[] = str_replace("'","\'",stripslashes(substr($msFileContent,$miParamStart,$i - $miParamStart)));
							}else{
								$mbInDblQuote = true;
								$miParamStart = $i + 1;
							}
						}
						$i++;
						break;
					case "'":
						if(!$mbInDblQuote){
							if($mbInSngQuote){
								$mbInSngQuote = false;
								$maParams[] = substr($msFileContent,$miParamStart,$i - $miParamStart);
							}else{
								$mbInSngQuote = true;
								$miParamStart = $i + 1;
							}
						}
						$i++;
						break;
					case ')':
						$i++;
						if(!$mbInSngQuote && !$mbInDblQuote) $miEnd = $i;
						break;
					case "\\":
						$i++;
						if($mbInDblQuote || $mbInSngQuote) $i++;
						break;
					case '$':
						if($mbInDblQuote) $mbValid = false;
						$i++;
						break;
					default: $i++;
				}
			}
			if($miEnd==-1){
				$miEnd = $miFileSize;
				$mbValid = false;
			}elseif(count($maParams)!=2){
				$mbValid = false;
			}elseif(!preg_match('/^[\w\d_]+$/i',$maParams[0])){
				$mbValid = false;
			}
			if($mbValid){
				$this->coStringList->addString($this->csFileBeingParsed,$maParams[0],$maParams[1]);
			}else{
				$msMsg = "Invalid call of '$msNeedle' in file '{$this->csFileBeingParsed}: "
				.$msNeedle.substr($msFileContent,$miStart,$miEnd-$miStart);
				trigger_error($msMsg,E_USER_WARNING);
			}
		}
		$this->csFileBeingParsed = '';
		return true;
	}

	/**
	 * Extrai as strings de um arquivo ou de todos arquivos XML ou PHP numa pasta
	 *
	 * <p>Extrai as strings de um arquivo ou de todos arquivos XML ou PHP numa pasta e
	 * usa o driver para armazenar a StringList final.</p>
	 * @access public
	 * @param string $psFullPath Arquivo ou pasta
	 * @param boolean $pbRecursive Indica se as subpastas devem ser percorridas tamb�m
	 * @param string $psExtension Extens�o dos arquivos a serem parseados (php|xml).
	 */
	public function extractStringsFromFiles($psFullPath,$pbRecursive,$psExtension){
		$this->coStringList = new FWDStringList();
		if(!file_exists($psFullPath) || !is_readable($psFullPath)){
			trigger_error("Could not open file or directory '$psFullPath'.",E_USER_ERROR);
			return;
		}elseif(is_dir($psFullPath)){
			$maFiles = $this->listFiles($psFullPath,$pbRecursive,$psExtension);
			if($psExtension=='php'){
				foreach($maFiles as $msFilePath){
					$this->singleExtractPHPStrings($msFilePath);
				}
			}elseif($psExtension=='xml'){
				foreach($maFiles as $msFilePath){
					$this->singleExtractXMLStrings($msFilePath);
				}
			}else{
				trigger_error('Invalid file extension.',E_USER_ERROR);
			}
		}else{
			if($psExtension=='php'){
				$this->singleExtractPHPStrings($psFullPath);
			}elseif($psExtension=='xml'){
				$this->singleExtractXMLStrings($psFullPath);
			}else{
				trigger_error('Invalid file extension.',E_USER_ERROR);
			}
		}
		$this->coDriver->store($this->coStringList);
		$this->coStringList = null;
	}

	/**
	 * Extrai as strings de um arquivo XML ou de todos arquivos XML numa pasta
	 *
	 * <p>Extrai as strings de um arquivo XML ou de todos arquivos XML numa pasta e
	 * usa o driver para armazenar a StringList final.</p>
	 * @access public
	 * @param string $psFullPath Arquivo ou pasta
	 * @param boolean $pbRecursive Indica se as subpastas devem ser percorridas tamb�m
	 */
	public function extractXMLStrings($psFullPath,$pbRecursive=false){
		$this->extractStringsFromFiles($psFullPath,$pbRecursive,'xml');
	}

	/**
	 * Extrai as strings de um arquivo PHP ou de todos arquivos PHP numa pasta
	 *
	 * <p>Extrai as strings de um arquivo PHP ou de todos arquivos PHP numa pasta e
	 * usa o driver para armazenar a StringList final.</p>
	 * @access public
	 * @param string $psFullPath Arquivo ou pasta
	 * @param boolean $pbRecursive Indica se as subpastas devem ser percorridas tamb�m
	 */
	public function extractPHPStrings($psFullPath,$pbRecursive=false){
		$this->extractStringsFromFiles($psFullPath,$pbRecursive,'php');
	}

	/**
	 * Armazena num arquivo PHP, uma StringList lida a partir do driver
	 *
	 * <p>Armazena num arquivo PHP, uma StringList lida a partir do driver.</p>
	 * @access public
	 * @param string $psFileName Arquivo PHP, onde a StringList vai ser armazenada
	 * @param string $psLanguage L�ngua que deve ser carregada do banco
	 */
	public function storeToPHPFile($psFileName, $psLanguage=""){
		$this->coStringList = $this->coDriver->load($psLanguage);
		if($this->coStringList==null){
			trigger_error("Could not load StringList from TranslationDriver.",E_USER_ERROR);
		}else{
			if(!is_writable($psFileName)){
				trigger_error("Could not write to file '$psFileName'.",E_USER_ERROR);
				return false;
			}
			$msOutput = "<?php\n\$gaStrings = array(\n";
			foreach($this->coStringList->dump() as $msFileName=>$maStrings){
				if(count($maStrings)>0){
					$msOutput.= "\n/* '$msFileName' */\n\n";
					foreach($maStrings as $msId=>$msValue){
						if ($msValue)
						$msOutput.= "'$msId'=>'".str_replace("'","\'",stripslashes($msValue))."',\n";
					}
				}
			}
			$msOutput.= ");\n?>";
			$mrFile = fopen($psFileName,'w');
			if($mrFile){
				fwrite($mrFile,$msOutput);
				fclose($mrFile);
			}
			$this->coStringList = null;
		}
	}

	/**
	 * Handler chamado quando o parser encontra uma tag de in�cio de elemento
	 *
	 * <p>Handler chamado quando o parser encontra uma tag de in�cio de elemento.</p>
	 * @access protected
	 * @param resource $prParser Parser
	 * @param string $psTagName Nome da tag
	 * @param array $paAttributes Array associativo contendo os atributos do elemento
	 */
	protected function startHandler($prParser, $psTagName, $paAttributes){
		if($psTagName=='STRING'){
			if(isset($paAttributes['ID'])){
				$msId = $paAttributes['ID'];
				$this->caCurrentString = array('id'=>$msId,'value'=>'');
			}else{
				$msErrorMsg = "String element without id attribute in file {$this->csFileBeingParsed}"
				." at line ".xml_get_current_line_number($prParser);
				trigger_error($msErrorMsg,E_USER_WARNING);
			}
		} elseif ($psTagName=='B') {
			if (isset($this->caCurrentString['value'])) $this->caCurrentString['value'].='<b>';
		} elseif ($psTagName=='BR') {
			if (isset($this->caCurrentString['value'])) $this->caCurrentString['value'].='<br/>';
		}
	}

	/**
	 * Handler chamado quando o parser encontra texto dentro de um elemento
	 *
	 * <p>Handler chamado quando o parser encontra texto dentro de um elemento.</p>
	 * @access protected
	 * @param resource $prParser Parser
	 * @param string $psData Conte�do de texto do elemento
	 */
	protected function dataHandler($prParser, $psData){
		if($this->caCurrentString!=null){
			$msId = $this->caCurrentString['id'];
			$this->caCurrentString['value'].= $psData;
		}
	}

	/**
	 * Handler chamado quando o parser encontra uma tag de fim de elemento
	 *
	 * <p>Handler chamado quando o parser encontra uma tag de fim de elemento.</p>
	 * @access protected
	 * @param resource $prParser Parser
	 * @param string $psTagName Nome da tag
	 */
	protected function endHandler($prParser, $psTagName){
		if($this->caCurrentString!=null){
			if ($psTagName=='STRING') {
				$msId = $this->caCurrentString['id'];
				$msValue = trim(preg_replace('/[\s\n]+/',' ',$this->caCurrentString['value']));
				$msValue = utf8_encode($msValue);
				// testando pra ver se resolveu bug de encoding no windows
				//echo "era " .  $this->caCurrentString['value'] . " e ficou $msValue<br>";
				$this->coStringList->addString($this->csFileBeingParsed,$msId, $msValue);
				$this->caCurrentString = null;
			} elseif ($psTagName=='B') {
				if (isset($this->caCurrentString['value'])) $this->caCurrentString['value'].='</b>';
			}
		}
	}

}

?>