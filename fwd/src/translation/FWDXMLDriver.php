<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDXMLDriver. Driver para ler e escrever StringList's em arquivos XML.
 *
 * <p>Driver para ler e escrever StringList's em arquivos XML.</p>
 *
 * @package FWD5
 * @subpackage translation
 */
class FWDXMLDriver extends FWDXMLParser implements FWDTranslationDriver {

	/**
	 * Arquivo associado ao driver
	 * @var string
	 * @access protected
	 */
	protected $csFileName;

	/**
	 * Usado durante o parse do arquivo
	 * @var string
	 * @access protected
	 */
	protected $csCurrentFile = '';

	/**
	 * Usado durante o parse do arquivo
	 * @var string
	 * @access protected
	 */
	protected $csCurrentString = '';

	/**
	 * StringList interna ao objeto. Usada durante as opera��es de leitura e escrita.
	 * @var FWDStringList
	 * @access protected
	 */
	protected $coStringList = null;

	/**
	 * Construtor da classe FWDXMLDriver.
	 *
	 * <p>Construtor da classe FWDXMLDriver. Associa um arquivo XML ao driver.</p>
	 * @access public
	 * @param string $psFileName Arquivo a ser associado ao driver
	 */
	public function __construct($psFileName){
		$this->csFileName = $psFileName;
	}

	/**
	 * Modifica o arquivo associado ao driver.
	 *
	 * <p>Modifica o arquivo associado ao driver.</p>
	 * @access public
	 * @param string $psFileName Arquivo a ser associado ao driver
	 */
	public function setFileName($psFileName){
		$this->csFileName = $psFileName;
	}

	/**
	 * L� uma StringList.
	 *
	 * <p>L� uma StringList a partir do arquivo XML associado ao objeto.</p>
	 * @access public
	 * @param string $psLanguage L�ngua que deve ser carregada do banco (N�o utilizada)
	 * @return FWDStringList StringList lida
	 */
	public function load($psLanguage = ""){
		$this->coStringList = new FWDStringList();
		if(!$this->parseFile($this->csFileName)){
			$this->coStringList = null;
		}
		return $this->coStringList;
	}

	/**
	 * Armazena uma StringList.
	 *
	 * <p>Armazena uma StringList no arquivo XML associado ao objeto.</p>
	 * @access public
	 * @param FWDStringList $poStringList StringList a ser armazenada
	 */
	public function store(FWDStringList $poStringList){
		if(!is_writable($this->csFileName)){
			trigger_error("Could not write to file '{$this->csFileName}'.",E_USER_ERROR);
			return false;
		}
		$msOutput = "<?xml version='1.0' encoding='ISO-8859-1'?>\n<root>\n";
		foreach($poStringList->dump() as $msFileName=>$maStrings){
			if(count($maStrings)>0){
				$msOutput.= "<file name='$msFileName'>\n";
				foreach($maStrings as $msId=>$msValue){
					$msOutput.= "  <string id='$msId'>$msValue</string>\n";
				}
				$msOutput.= "</file>\n";
			}
		}
		$msOutput.= "</root>";
		$mrFile = fopen($this->csFileName,'w');
		if($mrFile){
			fwrite($mrFile,$msOutput);
			fclose($mrFile);
		}
	}

	/**
	 * Handler chamado quando o parser encontra uma tag de in�cio de elemento
	 *
	 * <p>Handler chamado quando o parser encontra uma tag de in�cio de elemento.</p>
	 * @access protected
	 * @param resource $prParser Parser
	 * @param string $psTagName Nome da tag
	 * @param array $paAttributes Array associativo contendo os atributos do elemento
	 */
	protected function startHandler($prParser, $psTagName, $paAttributes){
		if($psTagName=='FILE'){
			$this->csCurrentFile = $paAttributes['NAME'];
			$this->coStringList->addFile($this->csCurrentFile);
		}elseif($psTagName=='STRING'){
			$this->csCurrentString = $paAttributes['ID'];
			$this->coStringList->addString($this->csCurrentFile,$this->csCurrentString,'');
		}
	}

	/**
	 * Handler chamado quando o parser encontra texto dentro de um elemento
	 *
	 * <p>Handler chamado quando o parser encontra texto dentro de um elemento.</p>
	 * @access protected
	 * @param resource $prParser Parser
	 * @param string $psData Conte�do de texto do elemento
	 */
	protected function dataHandler($prParser, $psData){
		if($this->csCurrentString!=''){
			$msString = $this->coStringList->getString($this->csCurrentFile,$this->csCurrentString);
			$msString.= $psData;
			$this->coStringList->setString($this->csCurrentFile,$this->csCurrentString,$msString);
		}
	}

	/**
	 * Handler chamado quando o parser encontra uma tag de fim de elemento
	 *
	 * <p>Handler chamado quando o parser encontra uma tag de fim de elemento.</p>
	 * @access protected
	 * @param resource $prParser Parser
	 * @param string $psTagName Nome da tag
	 */
	protected function endHandler($prParser, $psTagName){
		if($psTagName=='FILE'){
			$this->csCurrentFile = '';
		}elseif($psTagName=='STRING'){
			$msString = $this->coStringList->getString($this->csCurrentFile,$this->csCurrentString);
			$msString = trim(preg_replace('/[\s\n]+/',' ',$msString));
			$this->coStringList->setString($this->csCurrentFile,$this->csCurrentString,$msString);
			$this->csCurrentString = '';
		}
	}

}
?>