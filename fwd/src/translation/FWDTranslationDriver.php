<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Interface FWDTranslationDriver. Interface para classes que sabem ler e escrever StringList's
 *
 * <p>Interface para classes que sabem ler e escrever StringList's em algum meio
 * de armazenagem. Esses drivers s�o usados pela classe FWDTranslate.</p>
 *
 * @package FWD5
 * @subpackage translation
 */
interface FWDTranslationDriver {

	/**
	 * L� uma StringList.
	 *
	 * <p>L� uma StringList a partir do meio de armazenagem.</p>
	 * @access public
	 * @param string $psLanguage L�ngua que deve ser carregada do banco
	 * @return FWDStringList StringList lida
	 */
	public function load($psLanguage = "");

	/**
	 * Armazena uma StringList.
	 *
	 * <p>Armazena uma StringList no meio de armazenagem.</p>
	 * @access public
	 * @param FWDStringList $poStringList StringList a ser armazenada
	 */
	public function store(FWDStringList $poStringList);

}
?>