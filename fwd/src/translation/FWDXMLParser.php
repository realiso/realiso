<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe abstrata FWDXMLParser. Classe base para parsers de XML.
 *
 * <p>Classe base para parsers de XML.</p>
 *
 * @package FWD5
 * @subpackage translation
 */
abstract class FWDXMLParser {

	/**
	 * Parser retornado pela fun��o xml_parser_create
	 * @var resource
	 * @access protected
	 */
	protected $crParser;

	/**
	 * Inicializa o parser para parsear um arquivo
	 *
	 * <p>Inicializa o parser para parsear um arquivo.</p>
	 * @access protected
	 */
	protected function setupParser(){
		$this->crParser = xml_parser_create('iso-8859-1');//no XML funciona sem o iso
		xml_set_object($this->crParser, $this);
		xml_set_element_handler($this->crParser,'startHandler','endHandler');
		xml_set_character_data_handler($this->crParser,'dataHandler');
	}

	/**
	 * Libera o parser ao terminar de parsear um arquivo
	 *
	 * <p>Libera o parser ao terminar de parsear um arquivo.</p>
	 * @access protected
	 */
	protected function freeParser(){
		xml_parser_free($this->crParser);
	}

	/**
	 * Parseia um arquivo XML
	 *
	 * <p>Parseia um arquivo XML.</p>
	 * @access public
	 * @param string $psFileName Arquivo XML a ser parseado
	 * @param boolean $pbShowErrors Indica que, se ocorrerem erros de parse, eles devem ser exibidos
	 * @return boolean Indica se o arquivo foi parseado com sucesso
	 */
	public function parseFile($psFileName,$pbShowErrors=true){
		if(!file_exists($psFileName) || !is_readable($psFileName)){
			trigger_error("Could not open file '$psFileName'.",E_USER_WARNING);
			return false;
		}else{
			$mrFile = fopen($psFileName,'r');
		}
		$this->setupParser();
		while($msData = fread($mrFile,512)){
			$mbParse = xml_parse($this->crParser,$msData,feof($mrFile));
			if(!$mbParse){
				if($pbShowErrors){
					$msErrorMsg = sprintf(
            "XML error: %s in file $psFileName at line %d",
					xml_error_string(xml_get_error_code($this->crParser)),
					xml_get_current_line_number($this->crParser)
					);
					trigger_error($msErrorMsg,E_USER_WARNING);
				}
				$this->csFileBeingParsed = '';
				$this->freeParser();
				return false;
			}
		}
		fclose($mrFile);
		$this->freeParser();
		return true;
	}

	/**
	 * Handler chamado quando o parser encontra uma tag de in�cio de elemento
	 *
	 * <p>Handler chamado quando o parser encontra uma tag de in�cio de elemento.</p>
	 * @access protected
	 * @param resource $prParser Parser
	 * @param string $psTagName Nome da tag
	 * @param array $paAttributes Array associativo contendo os atributos do elemento
	 */
	abstract protected function startHandler($prParser, $psTagName, $paAttributes);

	/**
	 * Handler chamado quando o parser encontra texto dentro de um elemento
	 *
	 * <p>Handler chamado quando o parser encontra texto dentro de um elemento.</p>
	 * @access protected
	 * @param resource $prParser Parser
	 * @param string $psData Conte�do de texto do elemento
	 */
	abstract protected function dataHandler($prParser, $psData);

	/**
	 * Handler chamado quando o parser encontra uma tag de fim de elemento
	 *
	 * <p>Handler chamado quando o parser encontra uma tag de fim de elemento.</p>
	 * @access protected
	 * @param resource $prParser Parser
	 * @param string $psTagName Nome da tag
	 */
	abstract protected function endHandler($prParser, $psTagName);

}

?>