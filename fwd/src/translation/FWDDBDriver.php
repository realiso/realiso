<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDDBDriver. Driver para ler e escrever StringList's em banco.
 *
 * <p>Driver para ler e escrever StringList's em banco.</p>
 *
 * @package FWD5
 * @subpackage translation
 */
class FWDDBDriver implements FWDTranslationDriver {

	/**
	 * DataSet para ler e escrever no banco
	 * @var FWDDBDataSet
	 * @access protected
	 */
	protected $coDataSet = null;

	/**
	 * StringList interna ao objeto. Usada durante as opera��es de leitura e escrita.
	 * @var FWDStringList
	 * @access protected
	 */
	protected $coStringList = null;

	/**
	 * Boolean indica ao objeto se as strings que devem ser inseridas no banco s�o provenientes
	 * do php ou de outra fonte.
	 * @var cbStringsFromPHP
	 * @access protected
	 */
	protected $cbStringsFromPHP = false;
	/**
	 * Construtor da classe FWDDBDriver.
	 *
	 * <p>Construtor da classe FWDDBDriver. Associa um DataSet ao driver.</p>
	 * @access public
	 * @param FWDDBDataSet $poDataSet DataSet a ser associado ao driver
	 */
	public function __construct(FWDDBDataSet $poDataSet,$pbStringsFromPHP = false){
		$this->coDataSet = $poDataSet;
		$this->cbStringsFromPHP = $pbStringsFromPHP;
	}

	/**
	 * Modifica o DataSet associado ao driver.
	 *
	 * <p>Modifica o DataSet associado ao driver.</p>
	 * @access public
	 * @param FWDDBDataSet $poDataSet DataSet a ser associado ao driver
	 */
	public function setDataSet(FWDDBDataSet $poDataSet){
		$this->coDataSet = $poDataSet;
	}

	/**
	 * L� uma StringList.
	 *
	 * <p>L� uma StringList a partir do DataSet.</p>
	 * @access public
	 * @param string $psLanguage L�ngua que deve ser carregada do banco
	 * @return FWDStringList StringList lida
	 */
	public function load($psLanguage = ""){
		$this->coStringList = new FWDStringList();
		$this->coDataSet->select();
		while($this->coDataSet->fetch()){
			$msFileName = trim($this->coDataSet->getFieldByAlias('string_file')->getValue());
			$msId = trim($this->coDataSet->getFieldByAlias('string_id')->getValue());
			if ($psLanguage) $msValue = trim($this->coDataSet->getFieldByAlias('string_'.$psLanguage)->getValue());
			else $msValue = trim($this->coDataSet->getFieldByAlias('string_value')->getValue());
			$this->coStringList->addString($msFileName,$msId,$msValue);
		}
		return $this->coStringList;
	}

	/**
	 * Armazena uma StringList.
	 *
	 * <p>Armazena uma StringList no banco, atrav�s do DataSet.</p>
	 * @access public
	 * @param FWDStringList $poStringList StringList a ser armazenada
	 */
	public function store(FWDStringList $poStringList){
		$this->coDataSet->getFieldByAlias('string_file')->clearFilters();
		$this->coDataSet->getFieldByAlias('string_id')->clearFilters();
		$this->coDataSet->getFieldByAlias('string_value')->clearFilters();
		$this->coDataSet->getFieldByAlias('string_date_inclusion')->clearFilters();
		$this->coDataSet->getFieldByAlias('string_date_inclusion')->setValue(time());
		foreach($poStringList->dump() as $msFileName=>$maStrings){
			if(count($maStrings)>0){
				$moFieldFile = $this->coDataSet->getFieldByAlias('string_file');
				$moFieldFile->setValue($msFileName);
				foreach($maStrings as $msId=>$msValue){
					$moFieldId = $this->coDataSet->getFieldByAlias('string_id');
					$moFieldId->clearFilters();
					$moFieldId->addFilter(new FWDDBFilter('=',$msId));
					$moFieldId->setValue($msId);

					if($this->cbStringsFromPHP)
					$this->coDataSet->getFieldByAlias('string_value')->setValue(utf8_encode($msValue), true,true);
					else
					$this->coDataSet->getFieldByAlias('string_value')->setValue($msValue, true,true);

					$this->coDataSet->select();
					if($this->coDataSet->getRowCount() == 0){
						$this->coDataSet->insert();
					}else{
						$this->coDataSet->update();
					}
				}
			}
		}
		$this->coDataSet->getFieldByAlias('string_id')->clearFilters();
	}

}
?>