<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDXmlCompile. Singleton. Compila os arquivos XML da FWD5.
 *
 * <p>Classe singleton que compila os arquivos XML da FWD5. Percorre
 * recursivamente toda a estrutura de diret�rios da Framework FWD5 em busca
 * de arquivos com a extens�o '.xml'. Cria uma lista com todos os arquivos XML
 * de um dado diret�rio, e, para cada um, executa o m�todo xml_load para
 * obter os objetos PHP, serializa-os, comprime-os e grava os dados em um
 * arquivo TAR. Garante a exist�ncia de uma e apenas uma inst�ncia da classe
 * FWDXmlCompile.</p>
 * @package FWD5
 * @subpackage base
 */
class FWDXmlCompile {

	/**
	 * Inst�ncia �nica da classe FWDXmlCompile
	 * @staticvar FWDXmlCompile
	 * @access private
	 */
	private static $coXmlCompile = NULL;

	/**
	 * Nome do arquivo TAR onde ser�o armazenados os arquivos XMC
	 * @var string
	 * @access private
	 */
	private $csTarFileName;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDXmlCompile.</p>
	 * @access private
	 */
	private function __construct() {
		$this->csTarFileName = "forms.xmc";
	}

	/**
	 * Retorna a inst�ncia �nica da classe FWDXmlCompile (singleton).
	 *
	 * <p>Retorna a inst�ncia �nica da classe FWDXmlCompile. Se a inst�ncia
	 * ainda n�o existe, cria a �nica inst�ncia. Se a inst�ncia j� existe,
	 * retorna a �nica inst�ncia (singleton).</p>
	 * @access public
	 * @return FWDXmlCompile Inst�ncia �nica da classe FWDXmlCompile
	 * @static
	 */
	public static function getInstance() {
		if (self::$coXmlCompile == NULL) {
			self::$coXmlCompile = new FWDXmlCompile();
		}
		else {}	// inst�ncia �nica j� existe, retorne-a
		return self::$coXmlCompile;
	}

	/**
	 * Seta o nome do arquivo TAR.
	 *
	 * <p>Seta o nome do arquivo TAR onde ser�o armazenados os arquivos XMC.</p>
	 * @access public
	 * @param string $psTarFileName Nome do arquivo TAR
	 */
	public function setAttrTarFileName($psTarFileName) {
		$this->csTarFileName = $psTarFileName;
	}

	/**
	 * Percorre a estrutura de diret�rios da Framework FWD5.
	 *
	 * <p>Percorre recursivamente (a partir do diret�rio passado como par�metro,
	 * percorrendo seus subdiret�rios) toda a estrutura de diret�rios da
	 * Framework em busca de arquivos com a extens�o '.xml'. Uma vez que o
	 * m�todo termine de percorrer os arquivos do diret�rio, antes de fech�-lo
	 * e continuar as chamadas recursivas, � realizada uma chamada do m�todo
	 * createTar(), o qual � o respons�vel por carregar os objetos PHP,
	 * serializ�-los, comprimi-los e grav�-los em um arquivo TAR (estas
	 * opera��es s�o realizadas para cada arquivo com a extens�o '.xml'
	 * encontrados naquele diret�rio).</p>
	 *
	 * @access public
	 * @param string $psDir Refer�ncia para o diret�rio root da estrutura de diret�rios que cont�m os arquivos '.xml'
	 * @param string $psRecursive Indica se a compila��o deve entrar nos subdiret�rios
	 */
	public function xmlCompile($psDir, $pbRecursive = false) {
		if (is_dir($psDir)) {
			echo "Entering directory '".$psDir."'<br/>";
			if ($dirHandler = opendir($psDir)) {
				while ((($file = readdir($dirHandler)) !== false) && $pbRecursive) {
					if( ($file[0] == '.') || (is_file($file)) )
					continue;
					$this->xmlCompile($psDir."/".$file);
				}
				$this->createTar($psDir);
				closedir($dirHandler);
			}
		}
	}

	/**
	 * 'Compila' todos os arquivos XML de um diret�rio e grava-os em um arquivo TAR.
	 *
	 * <p>Cria um array com os nomes de todos os arquivos com a extens�o '.xml'
	 * encontrados no diret�rio passado como par�metro. Para cada um, executa
	 * o m�todo xml_load() da classe singleton FWDXmlLoad, o qual ir� gerar
	 * os objetos PHP, os quais ser�o serializados e comprimidos. Uma vez que
	 * todos os arquivos XML do diret�rio tenha passado por este procedimento,
	 * � realizada a grava��o destes em um arquivo com a extens�o '.tar'.</p>
	 *
	 * @access private
	 * @param string $psDir Refer�ncia para um diret�rio onde encontram-se os arquivos '.xml'
	 */
	private function createTar($psDir) {
		$maFiles = scandir($psDir,1);
		$msPath = FWDWebLib::getObject('Base_Ref').$psDir."/";
		$maXMLFiles = array();
		foreach($maFiles as $msFileName)
		if(strlen($msFileName) > 4 && (strrpos($msFileName, ".xml") == strlen($msFileName)-4))
		$maXMLFiles[] = $msFileName;

		echo "<br/><hr><h3>XML FILES FROM DIRECTORY '". $psDir ."' [Total: ".count($maXMLFiles)."]</h3>";
		foreach($maXMLFiles as $msFileName)
		echo $msFileName."<br/>";

		if (count($maXMLFiles)>0) {
			echo "<br/><br/>Encoding files...<br/>";
				
			$moXmlLoad = FWDXmlLoad::getInstance();
			$moXmlLoad->setCompiling(true);
				
			$maXMCFiles = array();
			foreach($maXMLFiles as $xmlFile) {
				FWDWebLib::cleanObjects();

				$moXmlLoad->xml_load($msPath.$xmlFile);

				$msData = "";
				foreach ($moXmlLoad->getAttrXmlObjects() as $moObjectName => $moObject) {
					$msData .= base64_encode(gzcompress(serialize($moObject)))."\n";
				}
				$maXMCFiles[$xmlFile] = $msData;
				echo "<br/><b>[ File <font color='blue'>".$xmlFile."</font> encoded ]</b>";
			}
			$moXmlLoad->setCompiling(false);
				
			echo "<br/><br/>Creating TAR archive ({$this->csTarFileName}) ...<br/>";
			$msArchive = "";
			$miStoredFiles = 0;
			foreach ($maXMCFiles as $msCurrentName => $msCurrentData) {
				if (strlen($msCurrentName) > 99) {
					trigger_error("Could not add {$msPath}{$msCurrentName} to tar archive: Filename is too long.",E_USER_WARNING);
					continue;
				}
				$maCurrentStat = stat($msPath.$msCurrentName);

				$msBlock = pack("a100a8a8a8a12a12a8a1a100a6a2a32a32a8a8a155a12",
				$msCurrentName,	// filename
				sprintf("%07o", $maCurrentStat[2]),		// mode
				sprintf("%07o", $maCurrentStat[4]),		// uid
				sprintf("%07o", $maCurrentStat[5]),		// gid
				sprintf("%011o", strlen($msCurrentData)),	// filesize
				sprintf("%011o", $maCurrentStat[9]),		// mtime
					"        ",		// checksum
				0,				// filetype (always 0=>file)
					"",				// filename (if simbolic link [not implemented])
					"ustar ",		// magic
					" ",			// version
					"Unknown",		// uname
					"Unknown",		// gname
					"",				// devmajor
					"",				// devminor
					"",				// just to fill 512
					""				// just to fill 512
				);
				$miChecksum = 0;
				for ($i = 0; $i < 512; $i++)
				$miChecksum += ord(substr($msBlock, $i, 1));
				$miChecksum = pack("a8", sprintf("%07o", $miChecksum));

				$msBlock = substr_replace($msBlock, $miChecksum, 148, 8);
				if (is_link($msCurrentName) || $maCurrentStat[7] == 0)
				$msArchive .= $msBlock;
				else {
					$msCurrentData .= "\n";
					$delta = (strlen($msCurrentData) % 512) > 0 ? (512 - (strlen($msCurrentData) % 512)) : 0;
					for ($i=0; $i<$delta; $i++)
					$msCurrentData .= "\0";
					$msArchive .= $msBlock.$msCurrentData;
				}
				$miStoredFiles++;
				echo "#$miStoredFiles";
			}
				
			$msArchive .= pack("a1024", "");
				
			if ( !($tarFileStream = fopen($msPath.$this->csTarFileName, "wb+")) )
			trigger_error("Error creating file {$msPath}{$this->csTarFileName}",E_USER_ERROR);
			elseif (!fwrite($tarFileStream,$msArchive))
			trigger_error("Error writing data to file {$msPath}{$this->csTarFileName}",E_USER_ERROR);
			elseif (!fclose($tarFileStream))
			trigger_error("Error closing file {$msPath}{$this->csTarFileName}",E_USER_ERROR);
			else
			echo "<center><h4><font color='green'>FILE {$msPath}{$this->csTarFileName} SUCCESSFULLY CREATED!</font></h4></center>";
		}
		else
		echo "<br/>No XML files found in this directory.<br/>";
	}
}
?>