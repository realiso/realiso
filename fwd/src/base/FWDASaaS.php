<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDASaaS. Classe que se comunica com o Webservice do ASaaS.
 *
 * <p>Classe que se comunica com o Webservice do ASaaS.</p>
 * @package FWD5
 * @subpackage base
 */
class FWDASaaS {
	protected $clientSOAP = null;

	function __construct(){
		$this->clientSOAP = new SoapClient('https://app.asaas.com/asaas-app-asaas-ejb-1.0-SNAPSHOT/AccountBean?wsdl');
	}

	public function clientGetSubscriptionFromASaaS($keyClient, $keySubscription){
		$register = array(
			"keyClient" => $keyClient,
			"keySubscription" => $keySubscription
		);
		$valuesFromSignature = $this->clientSOAP->clientGetSubscription($register);
		return $valuesFromSignature;
	}

	public function clientUpdatePlanAccount($keyClient, $keySubscription, $planCode){
		$register = array(
			"keyClient" => $keyClient,
			"keySubscription" => $keySubscription,
			"planCode" => $planCode
		);
		$updatedPlanAccount = $this->clientSOAP->clientUpdatePlanAccount($register);
		return $updatedPlanAccount;
	}

	public function clientGetURLPayPal($keyClient, $keySubscription, $planCode, $instance, $urlSuccess, $urlCancel){
		$register = array(
			"keyClient" => $keyClient,
			"keySubscription" => $keySubscription,
			"planCode" => $planCode,
			"urlSuccess" => $urlSuccess,
			"urlCancel" => $urlCancel
 		 );
		$url = $this->clientSOAP->clientGetURLPayPal($register);
		return $url->return;
	}

	public function clientActivePayPal($keyClient, $keySubscription, $planCode, $payerID, $token){
		$register = array(
			"keyClient" => $keyClient,
			"keySubscription" => $keySubscription,
			"planCode" => $planCode,
			"PayerID" => $payerID,
			"token" => $token
		);
		$clientActived = $this->clientSOAP->clientActivePayPal($register);
		return $clientActived->return;
	}

	public function subscriptionGetPayments($keyClient, $keySubscription){
		$register = array(
			"keyClient" => $keyClient,
			"keySubscription" => $keySubscription
		);
		$valuesFromPayments = $this->clientSOAP->subscriptionGetPayments($register);
		return $valuesFromPayments;
	}

	public function clientConfirmStatusSubscription($keyClient, $keySubscription){
		$register = array(
			"keyClient" => $keyClient,
			"keySubscription" => $keySubscription
		);
		$statusChanged = $this->clientSOAP->clientConfirmStatusSubscription($register);
		return $statusChanged->return->statusResponse;
	}

	public function subscriptionNewCheckPoint($keyClient, $accountId, $check, $module){
		$register = array(
			"keyClient" => $keyClient,
			"accountId" => $accountId,
			"check" => $check,
			"module" => $module);

		$statusNewCheckpoint = $this->clientSOAP->subscriptionNewCheckPoint($register);
		return $statusNewCheckpoint->return->statusResponse;
	}

}
?>
