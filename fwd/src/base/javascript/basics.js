/*****************************************************************************
 * Alias para getElementById
 *
 * <p>Alias para getElementById. Retorna um elemento HTML a partir de seu
 * id.</p>
 *
 * @param string id Id do elemento
 * @return element Elemento HTML
 *****************************************************************************/
function gebi(id){
  return document.getElementById?document.getElementById(id):null;
}

/*****************************************************************************
 * Retorna o objeto associado a um elemento, caso ele exista.
 *
 * <p>Retorna o objeto associado a um elemento, caso ele exista, ou null se
 * ele n�o existir.</p>
 *
 * @param String id Id do elemento
 * @return Object O objeto
 *****************************************************************************/
function gobi(psId){
  var moElement = gebi(psId);
  if(moElement && moElement.object){
    return moElement.object;
  }else{
    return null;
  }
}

/*****************************************************************************
 * Verifica se os campos obrigat�rios est�o preenchidos satisfatoriamente.
 *
 * <p> Verifica se os campos definidos como obrigat�rios est�o preenchidos
 * satisfatoriamente e sinaliza sua situa��o usando a fun��o 
 * js_required_check_feedback().
 * Um elemento � considerado preenchido satisfatoriamente quando:
 *  - Ele possui um objeto associado (gobi()), esse objeto possui um m�todo
 *    isFilled() e esse m�todo retornar um valor n�o vazio;
 *  - Ele possui um campo value n�o vazio;
 *  - Ele possui um objeto associado (gobi()), esse objeto possui um m�todo
 *    isFillable() e esse m�todo retornar false;
 *  - O elemento est� desabilitado ou escondido.
 * </p>
 * @param String psElementsId Ids dos elementos required separados por ':'
 * @param String psEventsOk C�digo dos eventos a serem executados no eventok
 * @param String psEventsNotOk C�digo dos eventos a serem executados no
 *        eventerror
 *****************************************************************************/
function js_required_check_client(psElementsId, psEventsOk, psEventsNotOk){
  try{
    var i, moElement, moObject, mbFilled;
    var maElementsIds = psElementsId.split(':');
    
    var maIdsElementsOk = [];
    var maIdsElementsNotOk = [];
    for(i=0;i<maElementsIds.length;i++){
      if(maElementsIds[i]!==''){
        moElement = gebi(maElementsIds[i]);
        if(moElement){
          moObject = gobi(maElementsIds[i]);
          if(moObject && moObject.isFilled){
            mbFilled = moObject.isFilled();
          }else{
            mbFilled = (trim(moElement.value)?true:false);
          }
          
          if(mbFilled){
            maIdsElementsOk.push(maElementsIds[i]);
          }else{
            var mbFillable;
            if(moObject && moObject.isFillable){
              mbFillable = moObject.isFillable();
            }else{
              mbFillable = true;
              if(moElement.disabled){
                mbFillable = false;
              }else{
                while(moElement && moElement.style){
                  if(moElement.style.display=='none'){
                    mbFillable = false;
                    break;
                  }
                  moElement = moElement.parentNode;
                }
              }
            }
            if(mbFillable){
              maIdsElementsNotOk.push(maElementsIds[i]);
            }else{
              maIdsElementsOk.push(maElementsIds[i]);
            }
          }
        }else{
          showDebug("Element '"+maElementsIds[i]+"' is required, but is not defined.");
          return;
        }
      }
    }
    if(maIdsElementsNotOk.length==0){
      fwd_eval(psEventsOk);
    }else{
      fwd_eval(psEventsNotOk);
    }
    js_required_check_feedback(maIdsElementsOk,maIdsElementsNotOk);
    return;
  }catch(moException){
    debugException(moException);
    return false;
  }
}

/*****************************************************************************
 * Exibe um feedback para o usu�rio indicando quais campos n�o est�o
 * satisfatoriamente preenchidos.
 *
 * <p> Exibe um feedback para o usu�rio indicando quais campos n�o est�o
 * satisfatoriamente preenchidos da seguinte maneira:
 *  - Se o elemento possui um objeto associado (gobi()), � chamado o m�todo
 *    onRequiredCheckOk, se ele estiver satisfatoriamente preenchido ou o m�todo
 *    onRequiredCheckNotOk, caso contr�rio;
 *  - Se existe um static associado ao elemento atrav�s do atributo label ou se
 *    existe um static cujo nome � igual ao nome do elemento prefixado por
 *    'label_', esse static tem o nome de sua classe CSS sufixado por '_Label'
 *    caso n�o esteja satisfatoriamente preenchido.
 *****************************************************************************/
function js_required_check_feedback(paIdsElementsOk, paIdsElementsNotOk){
	var i, moObject, msLabelId, moLabel, msClassName;
	for(i=0;i<paIdsElementsOk.length;i++){
		moObject = gobi(paIdsElementsOk[i]);
		if(moObject && moObject.onRequiredCheckOk){
			moObject.onRequiredCheckOk();
		}else{
			if(gebi(paIdsElementsOk[i])){
	    		msLabelId = gebi(paIdsElementsOk[i]).getAttribute('label');
	    		if(!msLabelId){
	    			msLabelId = 'label_'+paIdsElementsOk[i];
	    		}
	    		moLabel = gebi(msLabelId);
	    		if(moLabel){
	    			moLabel.className = moLabel.className.replace(/_Label$/,'');
	    		}    		
			}
		}
	}
	for(i=0;i<paIdsElementsNotOk.length;i++){
		moObject = gobi(paIdsElementsNotOk[i]);
		if(moObject && moObject.onRequiredCheckNotOk){
			moObject.onRequiredCheckNotOk();
		}else{
			msLabelId = gebi(paIdsElementsNotOk[i]).getAttribute('label');
			if(!msLabelId){
				msLabelId = 'label_'+paIdsElementsNotOk[i];
			}
			moLabel = gebi(msLabelId);
			if(moLabel){
				msClassName = moLabel.className;
				if(msClassName.substr(msClassName.length-6)!='_Label'){
					moLabel.className = moLabel.className+'_Label';
				}
			}
		}
	}
}

/*****************************************************************************
 * Verifica se os campos obrigat�rios est�o preenchidos
 *
 * <p> Modifica a classe css dos Static associados (pelo atributo "label" dos elementos) aos elementos
 * que N�O est�o preenchidos (os testes foram feitos no PHP)para a classe css FWDStaticLabel.
 * Para os elementos que est�o preenchidos corretamente, a classe css dos Static associados s�o
 * restaurados para a classe default (FWDStatic). Este evento � executado via Ajax</p>
 *
 * @param String psElementsId Id dos campos obrigat�rios que n�o est�o preenchidos (separados por ":")
 * @param String psElementsOkId Id dos campos que est�o preenchidos (separados por ":") utilizado para
 *     trocar a classe css para a classe css default dos Static
 *****************************************************************************/
function js_required_check_ajax(psElementsId,psElementsOkId){
  var maElementId = psElementsId.split(':');
  for(i=0;i<maElementId.length;i++){
    var moObj = gebi(maElementId[i]);
    if(moObj){
      var msLabelName = moObj.getAttribute('label');
      var moLabel = gebi(msLabelName);
      if(moLabel){
        moLabel.className="FWDStaticLabel";
      }
    }
  }
  var maElementOkId = psElementsOkId.split(':');
  for(i=0;i<maElementOkId.length;i++){
    var moObjOk = gebi(maElementOkId[i]);
    if(moObjOk){
      var msLabelNameOk = moObjOk.getAttribute('label');
      var moLabelOk = gebi(msLabelNameOk);
      if(moLabelOk){
        moLabelOk.className="FWDStatic";
      }
    }
  }
  return true;
}

/*****************************************************************************
 * Indica se o elemento � um array
 *
 * <p>Indica se um elemento � um array.</p>
 * 
 * @param object paElement Elemento
 * @return boolean Indica se o elemento � um array ou n�o
 *****************************************************************************/
function is_array(poElement){
  //return poElement.constructor == Array; // Teste n�o estava funcionando com nova classe de serialize.
  if (poElement.shift) return true;
  else return false;
}

/*****************************************************************************
 * Indica se um elemento pertence a um array
 *
 * <p>Indica se um elemento pertence a um array. Retorna true se achou o
 * elemento no array ou false caso contr�rio.</p>
 * 
 * @param object paElement Elemento
 * @param array paArray Array
 * @return boolean Indica se o elemento pertencia ao array ou n�o
 *****************************************************************************/
function in_array(poElement, paArray){
  var i,mbReturn = false;
  if(poElement && paArray){
    for(i=0;i<paArray.length;i++){
      if(paArray[i]==poElement){
        mbReturn = true;
        break;
      }
    }
  }
  poElement = paArray = null;
  return mbReturn;
}

/*****************************************************************************
 * Filtra elementos indesejados de um array
 *
 * <p>Filtra elementos indesejados de um array. Remove elementos 'undefined' e
 * strings vazias de arrays.</p>
 * 
 * @param array paArray Array a ser filtrado
 * @return array Array filtrado
 *****************************************************************************/
function array_filter(paArray) {
  var maNewArray = new Array();
  if (paArray) {
    var k=0;
    var i=0;
    for (i=0;i<paArray.length;i++) {
      if ( (paArray[i]!="") && (paArray[i]!=undefined) ) {
        maNewArray[k] = paArray[i];
        k++;
      }
    }
  }
  return maNewArray;
}

/*****************************************************************************
 * Gera um numero unico
 *
 * <p>Gera um numero unico. Retorna um numero unico, para fins de diferenciacao
 * de elementos.</p>
 *
 * @return string Numero unico
 *****************************************************************************/
function getNumber(){
  var date = new Date();
  var number = "";
  number += date.getTime();
  return number;
}

/*****************************************************************************
 * Cria o loading
 *
 * <p>Cria o loading. Cria uma DIV com o texto "Loading...".</p>
 *
 * @param string iframe Inner Frame
 * @param string static_text Id do elemento
 *****************************************************************************/
function makeLoading(iframe,static_text) {
  var moIframe;
  if(static_text){
    newDiv = document.createElement("DIV");
    newDiv.id = 'FWDLoading'+static_text;
    newDiv.className = "FWDLoading";
    newDiv.innerHTML = gebi('FWDLoading').innerHTML;
    if(typeof(iframe)=='string'){
      moIframe = gebi(iframe);
    }else{
      moIframe = iframe;
    }
    if(moIframe){
      gebi(static_text).appendChild(newDiv);
      gebi(static_text).insertBefore(newDiv,moIframe);
      return true;
    }
    ac = document.getElementById("dialog");
    ac.parentNode.insertBefore(newDiv,ac);
    moIframe = null;
  }
  iframe = static_text = null;
}

/*****************************************************************************
 * Carrega uma URL em um iframe e exibe um loading correspondente.
 *
 * <p>Carrega uma URL em um iframe e exibe um loading correspondente.</p>
 *
 * @param Object poIframe O elemento iframe
 * @param String psSrc URL a ser carregada no iframe
 * @param String psLoadingId Identificador do loading
 *****************************************************************************/
function loadIframe(poIframe,psSrc,psLoadingId){
  var moLoading = gebi('FWDLoading'+psLoadingId);
  if(moLoading){
    moLoading.style.display = 'block';
  }else{
    makeLoading(poIframe,psLoadingId);
  }
  poIframe.src = psSrc;
  poIframe = psSrc = psLoadingId = moLoading = null;
}

/*****************************************************************************
 * Exibe o loading
 *
 * <p>Exibe o loading. Seta o attributo de display do "Loading..."
 * para 'block'.</p>
 *
 * @param string value Nome do elemento
 *****************************************************************************/
function showLoading(value){
  elem = ((typeof(value)!="undefined")?gebi('FWDLoading'+value):gebi('FWDLoading'));
  if(elem) {
	  elem.style.display = "block";
	  elem.style.visibility = "visible";
  }
  else makeLoading();
}

/*****************************************************************************
 * Esconde o loading
 *
 * <p>Esconde o loading. Seta o attributo de display do "Loading..."
 * para 'none'.</p>
 *
 * @param string value Nome do elemento
 *****************************************************************************/
function hideLoading(value){
  var fsId = (value?'FWDLoading'+value:'FWDLoading');
  var foElem = gebi(fsId);
  if(foElem){
	  foElem.style.visibility = "hidden";
	  foElem.style.display = "none";
  }
}

function Browser(){
  var ua, s, i;
  this.isIE    = false;
  this.isNS    = false;
  this.version = null;
  ua = navigator.userAgent;
  s = "MSIE";
  if((i = ua.indexOf(s)) >= 0){
    this.isIE = true;
    return;
  }
  s = "Gecko";
  if((i = ua.indexOf(s)) >= 0){
    this.isNS = true;
    return;
  }
}

var browser = new Browser();


/*****************************************************************************
 * Retorna a posi��o de um elemento no array
 *
 * <p>Passando um array e um elementos que deve estar contido nele,
 * a fun�ao ir� retornar qual a posi��o do array este elemento ocupa
 * caso n�o seja encontrado -1 ser� retornado</p>
 *
 * @param array paArray Array de Elementos
 * @param string psKey Elemento a ser procurado
 * @return integer posi��o do elemento
 *****************************************************************************/
function arrayIndexOf(paArray,psKey){
  var miReturn = -1;
  for(var i=0;i<paArray.length;i++){
    if(paArray[i]==psKey) {
      miReturn = i;
      break;
    }
  }
  return miReturn;
}

/*****************************************************************************
 * Exibe uma popup de debug
 *
 * <p>Exibe uma mensagem de erro em uma popup de debug.</p>
 *
 * @param String psMessage A mensagem a ser exibida
 * @param integer piClientHeight Altura da �rea cliente da popup (opcional)
 * @param integer piClientWidth Largura da �rea cliente da popup (opcional)
 * @param boolean pbKnownError Indica se � um erro conhecido (opcional)
 *****************************************************************************/
function showDebug(psMessage,piClientHeight,piClientWidth,pbKnownError){
  var miClientHeight = (piClientHeight?piClientHeight:400);
  var miClientWidth = (piClientWidth?piClientWidth:600);
  var miHeight = (browser.isIE ? miClientHeight + 25 : miClientHeight + 27);
  var miWidth = (browser.isIE ? miClientWidth + 2 : miClientWidth + 4);
  if(!pbKnownError && lbRelease){
    var miMessageHeight = 110;
    var miBoxHeight = miClientHeight - miMessageHeight - 40;
    var miBoxWidth = miClientWidth - 20;
    var msEmailCode = '<a href="http://www.realiso.com">www.realiso.com</a>';
    var msDebug = '<div style="position:absolute;left:10;height:'+miClientHeight+'px;width:'+(miWidth-20)+'px;overflow:hidden;">'
                    +'<div style="color:red;height:'+miMessageHeight+';">'
                      +'Se voc� est� vendo esta mensagem, um erro ocorreu. '
                      +'Por favor, copie o c�digo de erro abaixo e o envie pelo WSS, atrav�s da abertura de um ticket: '
                      +'<b>'+msEmailCode+'</b><br><br>'
                      +'If you are seeing this message, an error has occurred. '
                      +'Please, copy the error code below and send it through WSS, by opening a ticket: '
                      +'<b>'+msEmailCode+'</b><br><br>'
                    +'</div>'
                    +'<input type="button" style="position:absolute" value="Exibir/Esconder c�digo de erro | Show/Hide error code" onClick="if(gebi(\'errorCode\').style.display==\'block\')js_hide(\'errorCode\');else js_show(\'errorCode\');"/>'
                    +'<div id="errorCode" style="display:none;top:'+(miMessageHeight+30)+';height:'+miBoxHeight+';width:'+miBoxWidth+';font-size:8pt;font-family:courier new;border:2px inset;position:absolute;overflow:auto">'
                      +encodeBase64(psMessage)
                    +'</div>'
                 +'</div>';
  }else{
    var msDebug = '<div style="height:'+miClientHeight+'px;width:'+miWidth+'px;overflow:auto;">'
                    +psMessage
                 +'</div>';
  }
  js_open_popup('debug','','<b>Error</b>',false,miHeight,miWidth);
  soPopUpManager.getPopUpById('debug').setHtmlContent(msDebug);
}

/*****************************************************************************
 * Exibe uma popup de debug de uma exce��o
 *
 * <p>Exibe uma popup de debug de uma exce��o.</p>
 *
 * @param Exception poException Exce��o a ser exibida
 * @param integer piClientHeight Altura da �rea cliente da popup (opcional)
 * @param integer piClientWidth Largura da �rea cliente da popup (opcional)
 *****************************************************************************/
function debugException(poException,piClientHeight,piClientWidth){
  var msDebug = '<h3>[Exception]</h3>';
  for(var msProperty in poException){
	  if(msProperty != 'extraInfo'){
		  msDebug+= '<b>'+msProperty+':</b> '+poException[msProperty]+'<br>';		  
	  }
  }
  if(poException.extraInfo){
	  msDebug += "<b>EXTRA INFO</b><br>{";
	  msDebug += poException.extraInfo;
	  msDebug += "<br>}";
  }
  showDebug(msDebug,piClientHeight,piClientWidth);
}

function stripHTML(str){
	var re= /<\S[^><]*>/g;
	var r = str.replace(re, "");
	return r;
}

/*****************************************************************************
 * Avalia uma express�o / executa um trecho de c�digo em javascript.
 *
 * <p>Avalia uma express�o / executa um trecho de c�digo em javascript.</p>
 *
 * @param string psExpr Express�o a ser avaliada
 * @param boolean pbDebug Indica se � para exibir uma mensagem de debug em
 *                caso de erro (false por default).
 * @return boolean Indica se a express�o foi avaliada com sucesso
 *****************************************************************************/
function fwd_eval(psExpr,pbDebug){
  try{
    eval(psExpr);
    return true;
  }catch(moException){
    if(pbDebug){
      var mbKnownError;
      if(psExpr.substring(0,13)=='[FWD_ERROR]: '){
        msDebug = psExpr.substring(13);
        mbKnownError = true;
      }else{
        mbKnownError = false;
        var msDebug = '<h3>[Debug]</h3><b>Error evaluating expression:</b>'+stripHTML(psExpr)+'<h3>[Exception]</h3>';
        for(var msProperty in moException){
          msDebug+= '<b>'+msProperty+':</b> '+moException[msProperty]+'<br>';
        }
      }
      showDebug(msDebug,400,600,mbKnownError);
    }
    return false;
  }
}

/*****************************************************************************
 * Caso a janela atual seja uma popup, ajusta o seu tamanho.
 *
 * <p>Caso a janela atual seja uma popup (popup de verdade), ajusta o seu
 * tamanho para o caso de existirem barras de ferramentas que diminuam a �rea
 * cliente da popup.</p>
 *****************************************************************************/
function resize_popup(){
  if(window.opener){
    var body = window.document.body;
    if(browser.isIE){
      doc_w = body.scrollWidth;
      nav_w = body.clientWidth;
      doc_h = body.scrollHeight;
      nav_h = body.clientHeight;
    }else{
      doc_w = body.offsetWidth + 8;
      nav_w = window.innerWidth;
      doc_h = body.scrollHeight;
      nav_h = window.innerHeight;
    }
    diff_w = doc_w - nav_w;
    diff_h = doc_h - nav_h;
    window.resizeBy(diff_w,diff_h);
  }
}

/*****************************************************************************
 * Testa se duas URLs s�o iguais.
 *
 * <p>Testa se duas URLs s�o iguais, independente de se o caminho � absoluto
 * ou relativo ao atual e independente da ordem dos par�metros da query
 * string.</p>
 *
 * @param String psUrl1 Primeira URL
 * @param String psUrl2 Segunda URL
 * @return boolean True sse as URLs s�o iguais
 *****************************************************************************/
function equalUrls(psUrl1,psUrl2){
  var msBaseUrl,maBaseUrl,maUrls,moUrl,maParameters,maPath;
  var mbEqual,i,miQSStart;
  if(psUrl1===psUrl2){
    mbEqual = true;
  }else{
    maUrls = [ {'url': psUrl1}, {'url': psUrl2} ];
    msBaseUrl = location.toString();
    msBaseUrl = msBaseUrl.substr(0,msBaseUrl.lastIndexOf('/'));
    maBaseUrl = msBaseUrl.split('/');
    for(i=0;i<2;i++){
      moUrl = maUrls[i];
      miQSStart = moUrl.url.indexOf('?');
      if(miQSStart > -1){
        moUrl.path = moUrl.url.substring(0,moUrl.url.indexOf('?'));
        moUrl.query = moUrl.url.substr(moUrl.url.indexOf('?')+1);
        maParameters = moUrl.query.split('&');
        maParameters.sort();
        moUrl.query = maParameters.join('&');
      }else{
        moUrl.path = moUrl.url;
        moUrl.query = '';
      }
      if(moUrl.path.indexOf(':') == -1){
        maPath = moUrl.path.split('/');
        j = maBaseUrl.length;
        while(maPath[0]=='..'){
          maPath.splice(0,1);
          j--;
        }
        moUrl.path = maBaseUrl.slice(0,j).join('/') + '/' + maPath.join('/');
      }
    }
    if(maUrls[0].path!=maUrls[1].path){
      mbEqual = false;
    }else if(maUrls[0].query!=maUrls[1].query){
      mbEqual = false;
    }else{
      mbEqual = true;
    }
    msBaseUrl = maBaseUrl = maUrls = moUrl = maParameters = maPath = null;
  }
  psUrl1 = psUrl2 = null;
  return mbEqual;
}


/*****************************************************************************
 * Calcula a diferen�a entre dois arrays.
 *
 * <p>Esse � um m�todo que foi adicionado � classe Array. Retorna um array
 * contendo os elementos do array original que n�o est�o presentes no array
 * passado como par�metro. N�o altera os arrays originais.</p>
 *
 * @param Array paArray Array a ser "subtra�do" do array original
 *****************************************************************************/
Array.prototype.diff = function(paArray){
  var i,j,mbInclude;
  var msElement1,msElement2,maReturn;
  maReturn = [];
  for(i=0;i<this.length;i++){
    msElement1 = this[i];
    if(typeof(msElement1)!='string') msElement1 = msElement1.toString();
    mbInclude = true;
    for(j=0;j<paArray.length;j++){
      msElement2 = paArray[j];
      if(typeof(msElement2)!='string') msElement2 = msElement2.toString();
      if(msElement1===msElement2){
        mbInclude = false;
        break;
      }
    }
    if(mbInclude){
      maReturn.push(this[i]);
    }
  }
  paArray = msElement1 = msElement2 = null;
  return maReturn;
}


/*****************************************************************************
 * Retorna os par�metros recebidos por get.
 *
 * <p>Retorna os par�metros recebidos por get.</p>
 *
 * @return Object Array associativo contendo os valores dos par�metros
 *         indexados pelos seus nomes
 *****************************************************************************/
function js_get_get_parameters(){
  var params = {};
  var url = location.toString();
  var queryStringStart = url.indexOf('?');
  if(queryStringStart>-1){
    var queryString = url.substr(queryStringStart+1);
    var tmpParams = queryString.split('&');
    var tmpStr;
    for(var i=0;i<tmpParams.length;i++){
      tmpStr = tmpParams[i].split('=');
      params[tmpStr[0]] = tmpStr[1];
    }
  }
  return params;
}

/*****************************************************************************
 * Retorna o valor de um par�metro recebido por get.
 *
 * <p>Retorna o valor de um par�metro recebido por get, dado seu nome.</p>
 *
 * @return Mixed Valor do par�metro
 *****************************************************************************/
function js_get_get_parameter(psParameterName){
  var maParams = js_get_get_parameters();
  return (maParams[psParameterName]!=undefined) ? maParams[psParameterName] : 0;
}

/*****************************************************************************
 * Move a div dialog para o centro da tela
 *
 * <p>Move a div dialog para o centro da tela, baseado no tamanho da janela do broswer do usu�rio</p>
 *
 *****************************************************************************/
function center_dialog_div(){
  var moDialog = gebi('dialog');
  var miBroswerWidth = document.body.clientWidth;
  var miDialogWidth = parseInt(moDialog.style.width);
  if(miDialogWidth<miBroswerWidth){
    moDialog.style.left = (miBroswerWidth - miDialogWidth)/2;
  }else{
    moDialog.style.left = 0;
  }
  moDialog = null;
}

/*****************************************************************************
 * Remove espa�os em branco no in�cio e no fim de uma string
 *
 * <p>Remove espa�os em branco no in�cio e no fim de uma string</p>
 *
 *****************************************************************************/
function trim(psStr){
  return psStr.replace(/^[ \t]+/,'').replace(/[ \t]+$/,'');
}

/*****************************************************************************
 * Limita o tamanho de uma memo
 *
 * <p>Limita o tamanho de uma memo</p>
 *
 *****************************************************************************/
function memo_length(memobox,maxLength,event){  
  if (memobox.value.length > maxLength) {
    memobox.value = memobox.value.substr(0,maxLength);
  }
};

/*****************************************************************************
 * Trata o bug do IE6 para suportar transpar�ncia em PNG
 *
 * <p>Trata o bug do IE6 para suportar transpar�ncia em PNG</p>
 *  Correctly handle PNG transparency in Win IE 5.5 & 6.
 *  http://homepage.ntlworld.com/bobosola. Updated 18-Jan-2006. 
 * 
 *****************************************************************************/
 
function correct_png(){ 
	var arVersion = navigator.appVersion.split("MSIE");
	var version = parseFloat(arVersion[1]);	
	
	if ((version >= 5.5) && (document.body.filters)){   
	   for(i=0; i < document.images.length;i++){	   
	      var img = document.images[i];
	      var imgName = img.src.toUpperCase();
	      if (imgName.substring(imgName.length-3, imgName.length) == "PNG"){
	         var imgID = (img.id) ? "id='" + img.id + "' " : "";
	         var imgClass = (img.className) ? "class='" + img.className + "' " : "";
	         var imgTitle = (img.title) ? "title='" + img.title + "' " : "title='" + img.alt + "' ";
	         var imgStyle = "display:inline-block;" + img.style.cssText; 
	         if (img.align == "left") imgStyle = "float:left;" + imgStyle;
	         if (img.align == "right") imgStyle = "float:right;" + imgStyle;
	         if (img.parentElement.href) imgStyle = "cursor:hand;" + imgStyle;
	         var strNewHTML = "<span " + imgID + imgClass + imgTitle + " style=\"" + "width:" + img.width + "px; height:" + img.height + "px;" + imgStyle + ";" + "filter:progid:DXImageTransform.Microsoft.AlphaImageLoader" + "(src=\'" + img.src + "\', sizingMethod='scale');\"></span>"; 
	         img.outerHTML = strNewHTML;
	         i = i-1;
	      }
	   }
	}
}

function limitTextArea(ta, limit) {
	if (ta.value.length > limit) {
		ta.value = ta.value.substring(0, limit);
	}
}