/**
 * FWD - FAST WEB DEVELOPMENT
 * 
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/*******************************************************************************
 * Classe FWDItemController. Classe com as fun��es utilizadas pelo FWDItemController.
 * 
 * <p>Classe com as fun��es utilizadas pelo FWDItemController.</p>
 * @package FWD
 * @param String psName Nome do elemento
 *******************************************************************************/
function FWDItemController(psName,psController,psKey){

 /**
  * Refer�ncia para si mesmo
  * @var FWDItemController
  * @access private
  */
  var self = this;
  
 /**
  * Objeto HTML que representa o componente
  * @var HTMLElement
  * @access private
  */
  var coObj = gebi(psName);
  
 /**
  * Nome do componente
  * @var String
  */
  self.csName = psName;
  
 /**
  * Vidro usado para desabilitar o componente
  * @var FWDGlass
  */
  self.coGlass = null;
  
 /**
  * Indica se o componente est� habilitado
  * @var boolean
  */
  self.cbEnabled = true;
  
 /**
  * Nome do controller respons�vel pelo item
  * @var String
  */
  self.csController = psController;
  
 /**
  * Chave do item
  * @var String
  */
  self.csKey = psKey;
  
  self.checkListener = null;
  
 /*****************************************************************************
  * Testa se o componente est� vis�vel.
  *
  * <p>Testa se o componente est� vis�vel.</p>
  * @return True sse o componente est� vis�vel
  *****************************************************************************/
  self.isVisible = function(){
    var mbIsVisible = true;
    var moElement = gebi(self.csName);
    while(moElement && moElement.style){
      if(moElement.style.display=='none'){
        mbIsVisible = false;
        break;
      }
      moElement = moElement.parentNode;
    }
    moElement = null;
    return mbIsVisible;
  }
  
 /*****************************************************************************
  * Desabilita o componente
  *
  * <p>Mosta o vidro para desabilitar o componente.</p>
  *****************************************************************************/
  self.disable = function(){
    if(!coObj){
      coObj = gebi(self.csName);
    }
    if(!self.coGlass){
      self.coGlass = new FWDGlass('silver',40);
      self.coGlass.atachToElement(self.csName);
    }
    if(self.isVisible()){
      self.coGlass.show(2);
    }
    self.cbEnabled = false;
  }
  
 /*****************************************************************************
  * Habilita o componente.
  *
  * <p>Esconde o vidro que estava desabilitando o componente</p>
  *****************************************************************************/
  self.enable = function(){
    if(self.coGlass){
      self.coGlass.hide();
    }
    self.cbEnabled = true;
  }
  
 /*****************************************************************************
  * Esconde o componente
  *
  * <p>Esconde o componente.</p>
  *****************************************************************************/
  self.hide = function(){
    coObj.style.display = 'none';
    if(self.coGlass){
      self.coGlass.hide();
    }
  }
  
 /*****************************************************************************
  * Exibe o componente
  *
  * <p>Exibe o componente.</p>
  *****************************************************************************/
  self.show = function(){
    coObj.style.display = 'block';
    if(self.cbEnabled==false){
      self.disable();
    }
  }
  
 /*****************************************************************************
  * Verifica se o item est� marcado.
  *
  * <p>Verifica se o item est� marcado.</p>
  * @return boolean True sse o item est� marcado
  *****************************************************************************/
  self.isChecked = function(){
    var i,mbChecked = false;
    var maChecked = gebi(self.csController).value.split(':');
    for(i=0;i<maChecked.length;i++){
      if(maChecked[i]==self.csKey){
        mbChecked = true;
        break;
      }
    }
    maChecked = null;
    return mbChecked;
  }

 /*****************************************************************************
  * Indica se o componente est� apto a ser preenchido.
  *
  * <p>Indica se o componente est� apto a ser preenchido.
  * (um dos m�todos necess�rios para customizar o required check).</p>
  * @return boolean True sse est� apto a ser preenchido
  *****************************************************************************/
  self.isFillable = function(){
    return (self.cbEnabled && self.isVisible());
  }

};
