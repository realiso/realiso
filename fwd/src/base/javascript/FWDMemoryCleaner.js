/**
 * FWD - FAST WEB DEVELOPMENT
 * 
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/*******************************************************************************
 * Objeto FWDMemoryCleaner. Objeto para evitar memory leaks no IE.
 * 
 * <p>Objeto para evitar memory leaks no IE.</p>
 *
 * @package javascript
 *******************************************************************************/
FWDMemoryCleaner = {

 /**
  * Lista de propriedades a serem removidas de cada elemento DOM
  * @var Array
  */
  'caPropertiesToRemove': ['onabort', 'onblur', 'onchange', 'onclick', 'ondragdrop',
      'onerror', 'onfocus', 'onkeydown', 'onkeypress', 'onkeyup', 'onload',
      'onmousedown', 'onmousemove', 'onmouseout', 'onmouseover', 'onmouseup',
      'onreset', 'onresize', 'onselect', 'onsubmit', 'onunload', 'onupdate', 'object'
  ],
  
 /**
  * Remove, de um elemento, as propriedades que possivelmente geram leak no IE.
  *
  * <p>Remove, de um elemento, as propriedades que possivelmente geram leak no
  * IE.</p>
  * @param Object poElement Elemento a ser limpo
  * @param boolean pbRecursive Indica se deve limpar recursivamente os filhos
  *        do elemento (false por default).
  */
  'cleanUpElement': function(poElement,pbRecursive){
    var i;
    for(i=0;i<this.caPropertiesToRemove.length;i++){
      if(poElement[this.caPropertiesToRemove[i]]){
        poElement[this.caPropertiesToRemove[i]] = null;
      }
    }
    if(pbRecursive && poElement.childNodes){
      for(i in poElement.childNodes){
        this.cleanUpElement(poElement.childNodes[i],true);
      }
    }
  },
  
 /**
  * Limpa todos elementos (s� no IE).
  *
  * <p>Limpa todos elementos (s� no IE).</p>
  */
  'cleanUp': function(){
    var miElement, msMember, moException;
    if(browser.isIE){
      if(FWDMemoryCleaner == undefined) {
        return 0;
      }      
      for(miElement = document.all.length - 1; miElement>=0; miElement--){
        FWDMemoryCleaner.cleanUpElement(document.all(miElement));
      }
      FWDMemoryCleaner.cleanUpElement(document);
      FWDMemoryCleaner.cleanUpElement(window);
    }
    
    FWDEventManager.cleanUp();
    msMember = moException = null;
    FWDMemoryCleaner.caPropertiesToRemove = null;
    FWDMemoryCleaner = null;
  },

 /**
  * Adiciona o cleanUp no evento de unload da window.
  *
  * <p>Adiciona o cleanUp no evento de unload da window.</p>
  */
  'scheduleCleanUp': function(){
    FWDEventManager.addEvent(window, 'unload', FWDMemoryCleaner.cleanUp);
  }

};

try{
  FWDMemoryCleaner.scheduleCleanUp();
}catch(soException){
  soException = null;
};
