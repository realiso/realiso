/**
 * FWD - FAST WEB DEVELOPMENT
 * 
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/*******************************************************************************
 * Classe FWDController. Classe com as fun��es utilizadas pelo FWDController.
 * 
 * <p>Classe com as fun��es utilizadas pelo FWDController.</p>
 * @package FWD
 * @param String psName Nome do elemento
 *******************************************************************************/
function FWDController(psName){

 /**
  * Refer�ncia para si mesmo
  * @var FWDController
  * @access private
  */
  var self = new FWDPack(psName);

 /*****************************************************************************
  * Indica se o elemento est� satisfatoriamente preenchido.
  *
  * <p>Indica se o elemento est� satisfatoriamente preenchido
  * (um dos m�todos necess�rios para customizar o required check).</p>
  * @return boolean True sse est� satisfatoriamente preenchido
  *****************************************************************************/
  self.isFilled = function(){
    if(trim(gebi(self.csName).value).replace(':','')!=''){
      return true;
    }else{
      return false;
    }
  }

 /*****************************************************************************
  * Indica se o elemento est� apto a ser preenchido.
  *
  * <p>Indica se o elemento est� apto a ser preenchido.
  * (um dos m�todos necess�rios para customizar o required check).</p>
  * @return boolean True sse est� apto a ser preenchido
  *****************************************************************************/
  self.isFillable = function(){
    var i;
    var moElement;
    var mbIsFillable = false;
    for(var i=0;i<self.caElements.length;i++){
      moElement = gobi(self.caElements[i]);
      if(moElement && moElement.isFillable && moElement.isFillable()){
        mbIsFillable = true;
        break;
      }
    }
    moElement = null;
    return mbIsFillable;
  }
  
  return self;

};