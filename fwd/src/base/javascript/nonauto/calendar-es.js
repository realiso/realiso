// ** I18N

// Calendar es language
// Author: 
// Encoding: any
// Distributed under the same terms as the calendar itself.

// For translators: please use UTF-8 if possible.  We strongly believe that
// Unicode is the answer to a real internationalized world.  Also please
// include your contact information in the header, as can be seen above.

// full day names
Calendar._DN = new Array
("Domingo",
"Lunes",
"Martes",
"Mi&eacute;rcoles",
"Jueves",
"Viernes",
"S&aacute;bado",
"Domingo");

// Please note that the following array of short day names (and the same goes
// for short month names, _SMN) isn't absolutely necessary.  We give it here
// for exemplification on how one can customize the short day names, but if
// they are simply the first N letters of the full name you can simply say:
//
//   Calendar._SDN_len = N; // short day name length
//   Calendar._SMN_len = N; // short month name length
//
// If N = 3 then this is not needed either since we assume a value of 3 if not
// present, to be compatible with translation files that were written before
// this feature.

// short day names
Calendar._SDN = new Array
("Dom",
 "Lun",
 "Mar",
 "Mi&eacute;",
 "Jue",
 "Vie",
 "S&aacute;b",
 "Dom");

// First day of the week. "0" means display Sunday first, "1" means display
// Monday first, etc.
Calendar._FD = 0;

// full month names
Calendar._MN = new Array
("Enero",
"Febrero",
"Marzo",
"Abril",
"Mayo",
"Junio",
"Julio",
"Agosto",
"Septiembre",
"Octubre",
"Noviembre",
"Diciembre");


// short month names
Calendar._SMN = new Array
("Ene",
 "Feb",
 "Mar",
 "Abr",
 "May",
 "Jun",
 "Jul",
 "Ago",
 "Sep",
 "Oct",
 "Nov",
 "Dic");

// tooltips
Calendar._TT = {};
Calendar._TT["INFO"] = "Sobre el calendario";

Calendar._TT["ABOUT"] =
"DHTML Date/Time Selector\n" +
"(c) dynarch.com 2002-2005 / Author: Mihai Bazon\n" + // don't translate this this ;-)
"Para la visita &uacute;ltima versi&oacute;n: http://www.dynarch.com/projects/calendar/\n" +
"Distribuido bajo licencia GNU LGPL. Visita http://gnu.org/licenses/lgpl.html para m&aacute;s detalles." +
"\n\n" +
"Seleccione la fecha:\n" +
"- Utilice las teclas \xab, \xbb para seleccionar el a&ntilde;o\n" +
"- Utilice las teclas " + String.fromCharCode(0x2039) + ", " + String.fromCharCode(0x203a) + " para seleccionar el mes\n" +
"- Haga clic y mantenga el cursor sobre cualquier bot&oacute;n para seleccionar r&aacute;pidamente.";
Calendar._TT["ABOUT_TIME"] = "\n\n" +
"Seleccione el tiempo:\n" +
"- Haga clic en cualquiera de las partes de la hora para agrandar\n" +
"- o Shift-click para reducirla\n" +
"- o haga clic y arrastre para seleccionar m&aacute;s rápidamente.";

Calendar._TT["PREV_YEAR"] = "A&ntilde;o anterior (Presione para menu)";
Calendar._TT["PREV_MONTH"] = "Mes anterior (Presione para menu)";
Calendar._TT["GO_TODAY"] = "Seleccione la fecha actual";
Calendar._TT["NEXT_MONTH"] = "Mes Siguiente (Presione para menu)";
Calendar._TT["NEXT_YEAR"] = "A&ntilde;o siguiente (mantener para menu)";
Calendar._TT["SEL_DATE"] = "Seleccione una fecha";
Calendar._TT["DRAG_TO_MOVE"] = "Seleccione una fecha";
Calendar._TT["PART_TODAY"] = " (Hoy)";

// the following is to inform that "%s" is to be the first day of week
// %s will be replaced with the day name.
Calendar._TT["DAY_FIRST"] = "Mostrar %s primero";

// This may be locale-dependent.  It specifies the week-end days, as an array
// of comma-separated numbers.  The numbers are from 0 to 6: 0 means Sunday, 1
// means Monday, etc.
Calendar._TT["WEEKEND"] = "0,6";

Calendar._TT["CLOSE"] = "Cerrar";
Calendar._TT["TODAY"] = "Hoy";
Calendar._TT["TIME_PART"] = "(Shift-)Haga clic o arrastre para cambiar el valor";

// date formats
Calendar._TT["DEF_DATE_FORMAT"] = "%m/%d/%Y";
Calendar._TT["TT_DATE_FORMAT"] = "%d de %B de %Y";

Calendar._TT["WK"] = "sm";
Calendar._TT["TIME"] = "Tiempo:";
