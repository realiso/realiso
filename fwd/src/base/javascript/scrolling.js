/**
 * FWD - FAST WEB DEVELOPMENT
 * 
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Intervalo criado pela fun��o setInterval() para controlar o scrolling
 * @var interval
 */
var scrollInterval;

/*****************************************************************************
 * Executa um passo da rolagem de um scrolling
 *
 * <p>Rola um scrolling um determinado n�mero de pixels e satura no limite
 * dado.</p>
 * 
 * @param string id Id do elemento scrolling
 * @param string dir Dire��o em que o elemento deve rolar
 * @param integer step N�mero de pixels a rolar
 * @param integer limit Valor limite que o elemento n�o pode ultrapassar
 *****************************************************************************/
function scroll(id,dir,step,limit){
  var content = gebi(id+'_content');
  if(dir=='down'){
    var newTop = parseInt(content.style.top)-step;
    if(newTop<limit) newTop = limit;
    content.style.top = newTop+'px';
  }else if(dir=='up'){
    var newTop = parseInt(content.style.top)+step;
    if(newTop>limit) newTop = limit;
    content.style.top = newTop+'px';
  }else if(dir=='right'){
    var newLeft = parseInt(content.style.left)-step;
    if(newLeft<limit) newLeft = limit;
    content.style.left = newLeft+'px';
  }else if(dir=='left'){
    var newLeft = parseInt(content.style.left)+step;
    if(newLeft>limit) newLeft = limit;
    content.style.left = newLeft+'px';
  }
}

/*****************************************************************************
 * Inicia a rolagem de um scrolling
 *
 * <p>Inicia a rolagem de um scrolling.</p>
 * 
 * @param string id Id do elemento scrolling
 * @param string dir Dire��o em que o elemento deve rolar
 * @param integer speed Velocidade com que o scrolling deve rolar em px/s
 *****************************************************************************/
function startScroll(id,dir,speed){
  document.getElementsByTagName('body').item(0).style.cursor = 'pointer';
  var window = gebi(id+'_window');
  var limit = 0;
  var period = Math.ceil(1000/speed);
  var content = gebi(id+'_content');
  if(dir=='down'){
    limit = parseInt(window.style.height) - parseInt(content.offsetHeight);
  }else if(dir=='right'){
    limit = parseInt(window.style.width) - parseInt(content.offsetWidth);
  }
  if(limit<=0){
    scrollInterval = setInterval('scroll("'+id+'","'+dir+'",1,'+limit+')',period);
  }
}

/*****************************************************************************
 * Encerra a rolagem de um scrolling
 *
 * <p>Encerra a rolagem do scrolling que estiver rolando.</p>
 *****************************************************************************/
function stopScroll(){
  clearInterval(scrollInterval);
  document.getElementsByTagName('body').item(0).style.cursor = '';
};
