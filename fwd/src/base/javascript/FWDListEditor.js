/**
 * FWD - FAST WEB DEVELOPMENT
 * 
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2007, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/*******************************************************************************
 * Classe FWDListEditor. Classe com as fun��es utilizadas pelo FWDListEditor.
 * 
 * <p>Classe com as fun��es utilizadas pelo FWDListEditor.</p>
 * @package FWD
 * @param String psName Nome do elemento
 *******************************************************************************/
function FWDListEditor(psName){
 /**
  * Refer�ncia para si mesmo
  * @var FWDListEditor
  * @access private
  */
  var self = this;  
 /**
  * Nome do elemento
  * @var String
  * @access private
  */
    self.csName = psName;
 /**
  * Elemento Select com todas as chaves
  * @var HTMLElement
  * @access private
  */
  self.selAll = gebi(self.csName+'_all');
 /**
  * Elemento Select com as chaves selecionadas
  * @var HTMLElement
  * @access private
  */
    self.selSelect = gebi(self.csName+'_selected');
 /**
  * Elemento FWDSelect objeto da fwd que implementa o objeto do HTMLSelect que cont�m todas as chaves
  * @var FWDSelect
  * @access private
  */
    self.selAllObj = gobi(self.csName+'_all');
 /**
  * Elemento FWDSelect objeto da fwd que implementa o objeto do HTMLSelect que cont�m as chaves selecionadas
  * @var FWDSelect
  * @access private
  */
    self.selSelectObj = gobi(self.csName+'_selected');
   /*******************************************************************************
   * executada no onclick dos botoes de up
   * 
   * <p>move o �tem do select uma posi��o para cima</p>
   *******************************************************************************/
  self.moveUp = function(){
    self.selSelectObj.moveUp();
  }
  /*******************************************************************************
   * executada no onclick dos botoes de down
   * 
   * <p>move o �tem do select uma posi��o para baixo</p>
   *******************************************************************************/
  self.moveDown = function(){
    self.selSelectObj.moveDown();
  }
  
  /*******************************************************************************
   * Adiciona no select de selecionados os itens selecionados no selectAll
   * 
   * <p> adiciona no select de selecionados os itens selecionados no selectAll</p>
   *******************************************************************************/
  self.addSelecteds = function(){
    self.moveValueSel(self.selAll,self.selSelect);
  }
  /*******************************************************************************
   * Remove do select de selecionados os itens selecionados e insere esses itens no selectAll
   * 
   * <p> remove do select de selecionados os itens selecionados e insere esses itens no selectAll</p>
   *******************************************************************************/
  self.removeSelecteds = function(){
    self.moveValueSel(self.selSelect,self.selAll);
  }
  
   /*******************************************************************************
   * Move os itens selecionados do select source para o selectTarguet
   * 
   * <p>Move os itens selecionados do select source para o selectTarguet</p>
   * @param Object FWDSelect poSelSource objeto select fonte dos itens
   * @param Object FWDSelect poSelSource objeto select destino dos itens selecionados
   *******************************************************************************/
  self.moveValueSel = function (poSelSource,poSelTarget){
    var i;
    for(i=0;i<poSelSource.options.length;i++){
      if(poSelSource.options[i].selected){
        poSelTarget.options[poSelTarget.options.length] = new Option(poSelSource.options[i].text,poSelSource.options[i].value,false,true);
        poSelSource.object.removeItem(poSelSource.options[i].value);
        /*alterado o valor da variavel de controle pois quando remove-se o �tem de um select o n�mero de �tens dele diminui fazendo com que
         * "pule-se" um dos �tens a cada �tem removido*/
        i = i-1;
      }
    }
    i = null;
  }
  /*******************************************************************************
   * fun��o que retorna os valores selecionados
   * 
   * <p>retorna todos os valores selecionados na ordem de exibi��o deles</p>
  * @return Array Array com as chaves dos �tens selecionados e na ordem de exibi��o deles
   *******************************************************************************/
  self.getValue = function(){
    return self.selSelectObj.getKeys().join(':');
  }
}