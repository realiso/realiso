
function resizeObject() {
	this.el     = null;    //pointer to the object
	this.dir    = "";      //type of current resize (n, s, e, w, ne, nw, se, sw)
	this.grabx  = null;    //Some useful values
	this.graby  = null;
	this.width  = null;
	this.height = null;
	this.left   = null;
	this.top    = null;
}

//Find out what kind of resize! Return a string inlcluding the directions
function getDirection(el,e) {
	var xPos, yPos, offset, dir;
	dir = "";

	xPos = ((browser.isIE)?window.event.offsetX:e.layerX);
	yPos = ((browser.isIE)?window.event.offsetY:e.layerY);
	
	offset = 8; //The distance from the edge in pixels

	if (yPos<offset) dir += "n";
	else if (yPos > el.offsetHeight-offset) dir += "s";
	if (xPos<offset) dir += "w";
	else if (xPos > el.offsetWidth-offset) dir += "e";

	return dir;
}

function doDown(e) {
	var el;
try{	
	target = ((browser.isIE)?event.srcElement:e.target);
	el = getReal(target, "resize", "true");

	if(el.getAttribute('resize') != "true") 
       return 0;
	
	if (el == null) {
		theobject = null;
		return;
	}

	dir = getDirection(el,e);
	if (dir == "") 
		return;


	theobject = new resizeObject();
		
	theobject.el = el;
	theobject.dir = dir;

	theobject.grabx = ((browser.isIE)?window.event.clientX:e.clientX);
	theobject.graby = ((browser.isIE)?window.event.clientY:e.clientY);
	theobject.width = el.offsetWidth;
	theobject.height = el.offsetHeight;
	theobject.left = el.offsetLeft;
	theobject.top = el.offsetTop;

	if(browser.isIE){
		window.event.returnValue = false;
		window.event.cancelBubble = true;
	}
	else{
		e.returnValue = false;
		e.cancelBubble = true;
	}
}
catch(moEvent){}	

}

function doUp() {
	if (theobject != null) {		
		if (theobject.el.getAttribute('atresize')) {
			func = theobject.el.getAttribute('atresize');
			eval(func);
		}
		theobject = null;
	}
}

function doMove(e) {
	var el, xPos, yPos, str, xMin, yMin;
	xMin = 8; //The smallest width possible
	yMin = 8; //             height
try{
	target = ((browser.isIE)?event.srcElement:e.target);
	el = getReal(target, "resize", "true");

	if (el.getAttribute('resize') == "true") {
		str = getDirection(el,e);
	//Fix the cursor	
		if (str == "") 
			str = "default";
		else 
			str += "-resize";
		el.style.cursor = str;
	}
}	
catch(moEvent){}
//Dragging starts here
	if(theobject != null) {
		
		valueX	 = ((browser.isIE)?window.event.clientX :e.clientX);
		valueY = ((browser.isIE)?window.event.clientY :e.clientY);
		
		if (dir.indexOf("e") != -1)
			theobject.el.style.width = Math.max(xMin, theobject.width + valueX - theobject.grabx) + "px";
	
		if (dir.indexOf("s") != -1)
			theobject.el.style.height = Math.max(yMin, theobject.height + valueY - theobject.graby) + "px";

		if (dir.indexOf("w") != -1) {
			theobject.el.style.left = Math.min(theobject.left + valueX - theobject.grabx, theobject.left + theobject.width - xMin) + "px";
			theobject.el.style.width = Math.max(xMin, theobject.width - valueX + theobject.grabx) + "px";
		}
		if (dir.indexOf("n") != -1) {
			theobject.el.style.top = Math.min(theobject.top + valueY - theobject.graby, theobject.top + theobject.height - yMin) + "px";
			theobject.el.style.height = Math.max(yMin, theobject.height - valueY + theobject.graby) + "px";
		}
		
		if(browser.isIE){
			window.event.returnValue = false;
			window.event.cancelBubble = true;
		}
		else{
			e.returnValue = false;
			e.cancelBubble = true;
		}
	
	} 
}

function getReal(el, type, value) {
	temp = el;
	while ((temp != null) && (temp.tagName != "BODY")) {
		if (eval("temp." + type) == value) {
			el = temp;
			return el;
		}
		temp = temp.parentElement;
	}
	return el;
}

var theobject = null; //This gets a value as soon as a resize start

document.onmousedown = doDown;
document.onmouseup   = doUp;
document.onmousemove = doMove;
