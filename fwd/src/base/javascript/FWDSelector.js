/**
 * FWD - FAST WEB DEVELOPMENT
 * 
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/*******************************************************************************
 * Classe FWDSelector.
 * 
 * <p>Classe para selecionar 1 entre N elementos para ficar vis�vel.</p>
 * @package FWD
 * @param String psName Nome do elemento correspondente (se houver)
 * @param String psSelected Nome do elemento inicialmente selecionado (opcional)
 *******************************************************************************/
function FWDSelector(psName,psSelected){

 /**
  * Refer�ncia para si mesmo
  * @var FWDFile
  * @access private
  */
  var self = new FWDSelect(psName);

 /**
  * Nome do elemento
  * @var String
  * @access private
  */
  var csName = psName;

 /**
  * Elemento Select correspondente
  * @var HTMLElement
  * @access private
  */
  var coElement;

 /**
  * Indica se selecionar o elemento selecionado o deseleciona
  * @var boolean
  * @access private
  */
  var cbToggle = false;
  
 /**
  * Elemento exibido quando n�o h� elemento selecionado
  * @var String
  * @access private
  */
  var csDefault = '';

 /**
  * Elemento selecionado (vis�vel)
  * @var String
  * @access private
  */
  var csSelected = psSelected;

  if(csName){
    coElement = gebi(psName);
    coElement.object = self;
  }
  
 /*****************************************************************************
  * Seta o atributo toggle.
  *
  * <p>Seta o atributo toggle, que indica se selecionar o elemento selecionado
  * o deseleciona.</p>
  * @param boolean pbToggle Novo valor do atributo
  *****************************************************************************/
  self.setToggle = function(pbToggle){
    cbToggle = pbToggle;
  }
  
 /*****************************************************************************
  * Seta o elemento default.
  *
  * <p>Seta o elemento que deve ser exibido quando n�o h� elemento selecionado.
  * </p>
  * @param String psDefault Id do elemento
  *****************************************************************************/
  self.setDefault = function(psDefault){
    csDefault = psDefault;
    if(!csSelected && csDefault){
      js_show(csDefault);
    }
  }
  
 /*****************************************************************************
  * Retorna o id do elemento selecionado.
  *
  * <p>Retorna o id do elemento selecionado. Se n�o houver um elemento
  * selecionado, retorna uma string vazia, mesmo que exista um elemento default.
  * </p>
  * @return String Id do elemento
  *****************************************************************************/
  self.getSelected = function(){
    return csSelected;
  }
  
 /*****************************************************************************
  * Seleciona o item vis�vel
  *
  * <p>Seleciona o item vis�vel</p>
  * @param String psName Identificador do objeto a ser selecionado
  *****************************************************************************/
  self.select = function(psName){
    if(gebi(psName)){
      if(psName==csSelected){
        if(cbToggle){
          js_hide(csSelected);
          csSelected = '';
        }
      }else{
        if(csSelected){
          js_hide(csSelected);
        }
        csSelected = psName;
        js_show(csSelected);
      }
      if(csSelected && csDefault){
        js_hide(csDefault);
      }
    }else{
      csSelected = '';
    }
    if(!csSelected && csDefault){
      js_show(csDefault);
    }
  }
  
  return self;

};