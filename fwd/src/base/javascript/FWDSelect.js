/**
 * FWD - FAST WEB DEVELOPMENT
 * 
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/*******************************************************************************
 * Classe FWDSelect. Classe com as fun��es utilizadas pelo FWDSelect.
 * 
 * <p>Classe com as fun��es utilizadas pelo FWDSelect.</p>
 * @package FWD
 * @param String psName Nome do elemento
 *******************************************************************************/
function FWDSelect(psName){

 /**
  * Refer�ncia para si mesmo
  * @var FWDFile
  * @access private
  */
  var self = this;

 /**
  * Nome do elemento
  * @var String
  * @access private
  */
  var csName = psName;

 /**
  * Elemento Select correspondente
  * @var HTMLElement
  * @access private
  */
  var coElement = gebi(psName);
  
  if(coElement) coElement.object = self;
  
 /*****************************************************************************
  * Remove todos itens do select.
  *
  * <p>Remove todos itens do select.</p>
  *****************************************************************************/
  self.clear = function(){
    var i;
    for (i=coElement.options.length; i>=0; i--){
      coElement.options[i] = null;
    }
  }
  
   
 /*****************************************************************************
  * Adiciona um item ao select.
  *
  * <p>Adiciona um item ao select.</p>
  * @param String psKey Chave do item
  * @param String psText Texto do item.
  *****************************************************************************/
  self.addItem = function(psKey,psText,psClass){
    var moOption = new Option(psText,psKey);
    if(psClass){
      moOption.className = psClass;
    }
    coElement.options[coElement.options.length] = moOption;
    moOption = null;
  }
  /*******************************************************************************
   * Move os itens selecionados do select uma posi��o para cima
   * 
   * <p>move os itens do select uma posi��o para cima</p>
   *******************************************************************************/
  self.moveUp = function(){
    var i;
    var maSelecteds = [];
    maSelecteds = self.getValue();
    for(i=0;i<coElement.options.length;i++){
      if(coElement.options[i].selected){
        if(i>=1){
          if(!(in_array(coElement.options[i-1].value,maSelecteds))){
            var msAuxKey = coElement.options[i-1].value;
            var msAuxValue = coElement.options[i-1].text;
            coElement.options[i-1] = new Option(coElement.options[i].text,coElement.options[i].value,false,true);
            coElement.options[i] = new Option(msAuxValue,msAuxKey,false,false);
          }
        }
      }
    }
    maSelected = i = null;
  }
  /*******************************************************************************
   * Move os itens selecionados uma posi��o para baixo
   * 
   * <p>Move os itens selecionados uma posi��o para baixo</p>
   *******************************************************************************/
  self.moveDown = function(){
    var i;
    var maSelecteds = [];
    maSelecteds = self.getValue();
    for(i=coElement.options.length-2;i>=0;i--){
      if(coElement.options[i].selected){
        if(!(in_array(coElement.options[i+1].value,maSelecteds))){
          var msAuxKey = coElement.options[i+1].value;
          var msAuxValue = coElement.options[i+1].text;
          coElement.options[i+1] = new Option(coElement.options[i].text,coElement.options[i].value,false,true);
          coElement.options[i] = new Option(msAuxValue,msAuxKey,false,false);
        }
      }
    }
    maSelecteds = i = msAuxKey = msAuxValue = null;
  }
 /*****************************************************************************
  * Remove um item do select.
  *
  * <p>Remove um item do select.</p>
  * @param String psKey Chave do item a ser removido
  *****************************************************************************/
  self.removeItem = function(psKey){
    var i;
    for(i=0;i<coElement.options.length;i++){
      if(coElement.options[i].value==psKey){
        coElement.removeChild(coElement.options[i]);
        return true;
      }
    }
    return false;
  }
 /*****************************************************************************
  * Retorna os key(s) do(s) item(ns) selecionado(s).
  *
  * <p>Retorna a key do item selecionado, se for single ou um array com os keys
  * dos items selecionados, se for multiple.</p>
  * @return Mixed Key(s) do(s) item(ns) selecionado(s)
  *****************************************************************************/
  self.getValue = function(){
    var i;
    if(coElement.multiple){
      var maSelecteds = [];
      for(i=0;i<coElement.options.length;i++){
        if(coElement.options[i].selected){
          maSelecteds.push(coElement.options[i].value);
        }
      }
      return maSelecteds;
    }else{
      return coElement.options[coElement.selectedIndex].value;
    }
  }
  /*****************************************************************************
  * Popula um select via javascript.
  *
  * <p>Popula um select removendo os itens anteriores e inserindo os novos.
  * formato da string serializada passada por parametro=serialize (array(array(key,'lable')))</p>
  * @param String psSerializedInfo String serializada que cont�m os �tens do select
  *****************************************************************************/
  self.populate = function(psSerializedInfo){
    if(psSerializedInfo){
      self.clear();
      maItens = soUnSerializer.unserialize(psSerializedInfo);
      var maOption = new Array();
      for(i=0;i<maItens.length;i++){
        maOption = maItens[i];
        coElement.options[i] = new Option(maOption[1], maOption[0], false, false);
      }
      if (coElement.options[0]){
        coElement.options[0].selected = true;
      }
    }
  }
  /*****************************************************************************
  * Testa se a key passada por parametro est� selecionada.
  *
  * <p>Testa se a key passada por parametro pertence ao conjunto das keys selecionadas.</p>
  * @param String pmKey Chave do Item a ser testado
  * @return Boolean Se a key est� ou n�o selecionada
  *****************************************************************************/
  self.isKeySelected = function(pmKey){
    var mbReturn = false;
    if(coElement.multiple){
      maSelected = self.getValue();
      for(i=0;i<maSelected.length;i++){
        if(maSelected[i]==pmKey){
          mbReturn = true;
        }
      }
    }else{
      if(self.getValue()==pmKey){
        mbReturn = true;
      }
    }
    return mbReturn;
  }
 /*****************************************************************************
  * Retorna um array com as chaves dos itens.
  *
  * <p>Retorna um array com as chaves dos itens.</p>
  * @return Array Array com as chaves dos itens
  *****************************************************************************/
  self.getKeys = function(){
    var i;
    var maKeys = [];
    for(i=0;i<coElement.options.length;i++){
      maKeys.push(coElement.options[i].value);
    }
    return maKeys;
  }
 /*****************************************************************************
  * Desabilita o select
  *
  * <p>Desabilita o select.</p>
  *****************************************************************************/
  self.disable = function(){
    coElement.disabled = true;
  }
 /*****************************************************************************
  * Abilita o select
  *
  * <p>Abilita o select.</p>
  *****************************************************************************/
  self.enable = function(){
    coElement.disabled = false;
  }
 /*****************************************************************************
  * Seleciona um item do select.
  *
  * <p>Seleciona um item do select levando em considera��o a sem�ntica entre um select single e multiple.</p>
  * @param String pmKey Chave do Item a ser selecionado
  *****************************************************************************/
  self.checkItem = function(pmKey){
    var i;
    if(!coElement.multiple){
      for(i=0;i<coElement.options.length;i++){
        if(coElement.options[i].value==pmKey){
          coElement.options[i].selected=true;
        }else{
          coElement.options[i].selected=false;
        }
      }
    }else{
      if(coElement.Options[pmKey]){
        coElement.Options.selected=true;
      }
    }
  }
  
  /*****************************************************************************
   * Seleciona todos os itens de um multiselect
   *****************************************************************************/
  self.checkAll = function(pmKey){
    if(coElement.multiple){
      var i;
      for(i=0;i<coElement.options.length;i++){
        coElement.options[i].selected=true;
      }
    }
  }
 /*****************************************************************************
  * Seleciona um ou v�rios �tens do select.
  *
  * <p>Seleciona (1 item se single XOR 1 ou mais se multiple) levando em considera��o a sem�ntica entre um select single e multiple.</p>
  * @param Mixed pmKeys Chave(s) do(s) Item(s) a ser(em) selecionado(s). Pode ser um identificador, v�rios identificadores separados por ':' ou um array de identificadores
  *****************************************************************************/
  self.setValue = function(pmKeys){
    if(coElement.multiple){
      if(is_array(pmKeys)){
        maValues = pmKeys;
      }else{
        maValues = pmKeys.split(':');
      }
      for(i=0;i<maValues.length;i++){
        self.checkItem(maValues[i]);
      }
    }else{
      self.checkItem(pmKeys);
    }
  }
  
 /*****************************************************************************
  * Remove todos itens selecionados.
  *
  * <p>Remove todos itens selecionados.</p>
  *****************************************************************************/
  self.removeSelecteds = function(){
    var i;
    for(i=coElement.options.length-1;i>=0;i--){
      if(coElement.options[i].selected){
        coElement.removeChild(coElement.options[i]);
      }
    }
  }
  
  self.show = function(){
    coElement.style.display="block";
  }
  
  
  self.hide = function(){
    coElement.style.display="none";
  }
  
 /*****************************************************************************
  * Seleciona todos os itens de um select multiplo.
  *
  * <p>Seleciona todos os itens de um select multiplo. Utilizado em telas que
  * preenchem o select via ajax e esse select � requiredCheck.</p>
  *****************************************************************************/
  self.checkAllItens = function(){
    for(var i=0;i<coElement.options.length;i++){
     coElement.options[i].selected = true;
    }
  }
  
 /*****************************************************************************
  * Retorna o texto de um item.
  *
  * <p>Retorna o texto de um item.</p>
  * @param String psKey Key do item
  * @return String O texto do item
  *****************************************************************************/
  self.getItemText = function(psKey){
    for(var i=0;i<coElement.options.length;i++){
      if(coElement.options[i].value==psKey){
        return coElement.options[i].text;
      }
    }
  }

};