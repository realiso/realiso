/**
 * FWD - FAST WEB DEVELOPMENT
 * 
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/*******************************************************************************
 * Classe FWDText. Classe com as fun��es utilizadas pelo FWDText.
 * 
 * <p>Classe com as fun��es utilizadas pelo FWDText.</p>
 * @package FWD
 * @param String psName Nome do elemento
 *******************************************************************************/
function FWDText(psName)
{
  /** @var Object referencia para ele mesmo. */
  var self = this;

  var coObj = gebi(psName);
  /** @var String Nome do elemento. */
  self.csName = psName;
  
	/** @var Int N�mero de vezes que o elemento � clicado **/
	var numClicks=0;
	
	/** @var String Valor padr�o a ser colocado na caixa de pesquisa. **/
	var defaultValue = "";
  
  /** @var RegExp Express�o regular da m�scara. **/
  var coMaskRegExp = null;
  
/*****************************************************************************
 * Limpa o valor no text, se o n�mero de clicks for 1
 *
 * <p>Limpa o valor no text</p>
 *
 *****************************************************************************/
  self.clearOnFirstClick = function(){
		numClicks++;
		if (numClicks==1) {
	  	defaultValue = coObj.value;
			coObj.value='';
		}
  }

/*****************************************************************************
 * Reseta o campo
 *
 * <p>Reseta o campo text</p>
 *
 *****************************************************************************/
 self.resetField = function() {
 		numClicks=0;
 		coObj.value=defaultValue;
 }

 /*****************************************************************************
  * Testa se o elemento est� habilitado.
  *
  * <p>Testa se o elemento est� habilitado.</p>
  * @return boolean True, sse est� habilitado
  *****************************************************************************/
  self.isEnabled = function(){
    return !gebi(self.csName).disabled;
  }
  
 /*****************************************************************************
  * Habilita o elemento
  *
  * <p>Habilita o elemento</p>
  *****************************************************************************/
  self.enable = function(){
    gebi(self.csName).disabled = false;
  }

 /*****************************************************************************
  * Desabilita o elemento
  *
  * <p>Desabilita o elemento</p>
  *****************************************************************************/
  self.disable = function(){
    gebi(self.csName).disabled = true;
  }

 /*****************************************************************************
  * Seta a express�o regular da m�scara.
  *
  * <p>Seta a express�o regular da m�scara.</p>
  * @param RegExp poMaskRegExp Express�o regular
  *****************************************************************************/
  self.setMaskRegExp = function(poMaskRegExp){
    coMaskRegExp = poMaskRegExp;
  }

 /*****************************************************************************
  * Indica se o elemento est� satisfatoriamente preenchido.
  *
  * <p>Indica se o elemento est� satisfatoriamente preenchido
  * (um dos m�todos necess�rios para customizar o required check).</p>
  * @return boolean True sse est� satisfatoriamente preenchido
  *****************************************************************************/
  self.isFilled = function(){
    var moField = gebi(self.csName);
    var mbReturn;
    if(moField.style.display=='none' || moField.disabled){
      mbReturn = true;
    }else if(coMaskRegExp==null){
      mbReturn = (trim(moField.value)!='');
    }else{
      mbReturn = coMaskRegExp.test(moField.value);
    }
    moField = null;
    return mbReturn;
  }
  
  self.show = function(){
	  gebi(self.csName).style.display = '';
  }
  
  self.hide = function(){
	  gebi(self.csName).style.display = 'none';
  }

};