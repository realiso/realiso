/**
 * FWD - FAST WEB DEVELOPMENT
 * 
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/*******************************************************************************
 * Classe FWDCalendar. Classe com as fun��es utilizadas pelo FWDCalendar.
 * 
 * <p>Classe com as fun��es utilizadas pelo FWDCalendar.</p>
 * @package FWD
 * @param String psName Nome do elemento
 *******************************************************************************/
function FWDCalendar(psName){

 /**
  * Refer�ncia para si mesmo
  * @var FWDCalendar
  * @access private
  */
  var self = this;

 /**
  * Nome do elemento
  * @var String
  * @access private
  */
  var csName = psName;

 /*****************************************************************************
  * Habilita o elemento
  *
  * <p>Habilita o elemento</p>
  *****************************************************************************/
  self.enable = function(){
    gebi(csName).disabled = false;
    gebi(csName+'_fake_button').style.display = 'none';
    gebi(csName+'_trigger_button').style.display = 'inline';
    gebi(csName).style.display = 'inline';
  }
  
 /*****************************************************************************
  * Desabilita o elemento
  *
  * <p>Desabilita o elemento</p>
  *****************************************************************************/
  self.disable = function(){
    gebi(csName).disabled = true;
    gebi(csName+'_trigger_button').style.display = 'none';
    gebi(csName+'_fake_button').style.display = 'inline';
    gebi(csName).style.display = 'inline';
    if(browser.isIE){
      gebi(csName+'_fake_button').style.left = '0px';
    }
  }
  
};