/**
 * FWD - FAST WEB DEVELOPMENT
 * 
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/*******************************************************************************
 * Expande um nodo
 * 
 * <p>
 * Expande um nodo, exibindo seus filhos.
 * </p>
 * 
 * @param string
 *            id Identificador da tree
 ******************************************************************************/
function openTree(id){
  gebi(id+'_tr').style.display = (window.ActiveXObject?'':'table-row');
  gebi(id+'_img').src = gfx_ref+'minus.gif';
}

/*******************************************************************************
 * Recolhe um nodo
 * 
 * <p>
 * Recolhe um nodo, ocultando seus filhos.
 * </p>
 * 
 * @param string
 *            id Identificador da tree
 ******************************************************************************/
function closeTree(id){
  gebi(id+'_tr').style.display = 'none';
  gebi(id+'_img').src = gfx_ref+'plus.gif';
}

/*******************************************************************************
 * Expande ou recolhe um nodo
 * 
 * <p>
 * Se o nodo estiver expandido, recolhe, se estiver recolhido, expande.
 * </p>
 * 
 * @param string
 *            id Identificador da tree
 ******************************************************************************/
function toggle(id){
  if(gebi(id+'_tr').style.display=='none') openTree(id);
  else closeTree(id);
};

function TreeCheckBoxNode(parent, controller, key) {
	this.parent=parent;
	this.key=key;
	this.controller=controller;
	this.childNodes = new Array();
	var el = gebi(controller+"_"+key);
	el.checkListener = this.checkListener;
}

var addChildNode = function(controller, key) {
	var newNode = new TreeCheckBoxNode(this, controller, key);
	var newArray = this.childNodes.concat([newNode]);
	this.childNodes = newArray;
}

var checkChildren = function(){
	for(var t = 0; t < this.childNodes.length; t++){
		var childNode = this.childNodes[t];
		gfx_check(childNode.controller, childNode.key, {check: true});
		childNode.checkChildren();
	}
}

var checkParents = function(){
	var parent = this.parent;
	while(parent != null){
		gfx_check(parent.controller, parent.key, {check: true});
		parent = parent.parent
	}
}

var uncheckChildren = function(){
	for(var t = 0; t < this.childNodes.length; t++){
		var childNode = this.childNodes[t];
		gfx_check(childNode.controller, childNode.key, {uncheck: true});
		childNode.uncheckChildren();
	}
}

var checkListener = function(controller, key, checked){
	if(!checkTreeSemaphore){
		checkTreeSemaphore = true;
		try{
			var node = findTreeCheckBoxNodeByKey(key);
			if(node){
				if(checked){
					node.uncheckChildren();
				}else{
					node.checkChildren();
					node.checkParents();
				}
			}
		}finally{
			checkTreeSemaphore = false;
		}
	}
}

TreeCheckBoxNode.prototype.checkListener = checkListener;
TreeCheckBoxNode.prototype.uncheckChildren = uncheckChildren;
TreeCheckBoxNode.prototype.checkChildren = checkChildren;
TreeCheckBoxNode.prototype.checkParents = checkParents;
TreeCheckBoxNode.prototype.addChildNode = addChildNode;

var checkTreeSemaphore = false;

function buildTree(node, str){
	var tr = "";
	if(!str){
		str = "\n-";
	}
	tr += str+node.key;
	str += "-";
	for(var t = 0; t < node.childNodes.length; t++){
		var childNode = node.childNodes[t];
		tr += buildTree(childNode, str);
	}
	return tr;
}

var chks = new Array();

function addTreeCheckBox(controller, key){
	var parentKey = getTreeCheckBoxParentKey(key);
	if(parentKey){
		var parentNode = findTreeCheckBoxNodeByKey(parentKey);
		if(parentNode && parentNode.addChildNode){
			parentNode.addChildNode(controller, key);
		}else{
			alert(parentNode + " - " + parentNode.addChildNode);
		}
	}else{
		var newNode = new TreeCheckBoxNode(null, controller, key);
		chks.push(newNode);
	}
}

function findTreeCheckBoxNodeByKey(key){
	for(var i = 0; i < chks.length; i++){
		var node = chks[i];
		var nodeF = findTreeCheckBoxNode(node, key);
		if(nodeF){
			return nodeF;
		}
	}
	return null;
}

function findTreeCheckBoxNode(node, key){
	if(node.key == key){
		return node;
	}
	for(var j = 0; j < node.childNodes.length; j++){
		var childNode = node.childNodes[j];
		if(childNode.key == key){
			return childNode;
		}else{
			var nodeFound = findTreeCheckBoxNode(childNode, key);
			if(nodeFound){
				return nodeFound;
			}
		}
	}
	return null;
}

function getTreeCheckBoxParentKey(key){
	if(key){
		var keySplit = key.split(".");
		if(keySplit.length > 2){
			var parentKey = keySplit[0] + "." + keySplit[1];
			for(var k = 2; k < keySplit.length; k++){
				if(k+1 < keySplit.length){
					parentKey += "." + keySplit[k]; 
				}
			}
			return parentKey;
		}
	}
	return null;
}