/**
 * FWD - FAST WEB DEVELOPMENT
 * 
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/*******************************************************************************
 * Objeto FWDEventManager. Gerenciador de eventos.
 * 
 * <p>Objeto para adicionar e remover handlers de eventos em elementos DOM.</p>
 *
 * @package javascript
 *******************************************************************************/
FWDEventManager = {

 /**
  * Lista de todos eventos adicionados atrav�s do EventManager.
  * @var Array
  */
  'caEvents': [],

 /**
  * Adiciona um handler de evento a um elemento.
  *
  * <p>Adiciona um handler de evento a um elemento.</p>
  * @param Object poElement Elemento a receber o handler
  * @param String psEvent Nome do evento (sem o prefixo 'on')
  * @param Function poFunc Handler do evento
  * @return integer Identificador do evento (usado para remov�-lo)
  */
  'addEvent': function(poElement, psEvent, poFunc){
    this.caEvents.push({
      'coElement': poElement,
      'csEvent'  : psEvent,
      'coFunc'   : poFunc
    });
    if(document.attachEvent){
      poElement.attachEvent('on'+psEvent, poFunc);
    }else{
      poElement.addEventListener(psEvent, poFunc, true);
    }
    poElement = psEvent = poFunc = null;
    return this.caEvents.length - 1;
  },
  
 /**
  * Remove um handler de evento.
  *
  * <p>Remove um handler de evento.</p>
  * @param integer piEventId Identificador do evento (retorno de addEvent())
  * @return boolean True se o evento foi realmente removido, false se ele j� n�o existia.
  */
  'removeEvent': function(piEventId){
    var moEventEntry = this.caEvents[piEventId];
    if(moEventEntry){
      if(document.detachEvent){
        moEventEntry.coElement.detachEvent('on'+moEventEntry.csEvent, moEventEntry.coFunc);
      }else{
        moEventEntry.coElement.removeEventListener(moEventEntry.csEvent, moEventEntry.coFunc, true);
      }
      this.caEvents[piEventId] = null;
      moEventEntry = null;
      return true;
    }else{
      return false;
    }
  },

 /**
  * Remove todos handlers de evento adicionados atrav�s do EventManager.
  *
  * <p>Remove todos handlers de evento adicionados atrav�s do EventManager.</p>
  */
  'cleanUp': function(){
    if(browser.isIE){
      for(var i = FWDEventManager.caEvents.length; i>=0; i--){
        FWDEventManager.removeEvent(i);
      }
    }
    FWDEventManager.caEvents = null;
    FWDEventManager = null;
  }
  
};

try{
  FWDMemoryCleaner.scheduleCleanUp();
}catch(soException){
  soException = null;
};
