/*****************************************************************************
 * Constantes para identificar os diferentes formatos de relatórios
 *****************************************************************************/
var REPORT_FILETYPE_HTML_DOWNLOAD = 7300;
var REPORT_FILETYPE_HTML = 7301;
var REPORT_FILETYPE_WORD = 7302;
var REPORT_FILETYPE_EXCEL = 7303;
var REPORT_FILETYPE_PDF = 7304;