function js_erase( id ){
  gebi(id).value = '';
  return true;
}

function js_color_background(id,attr){
  gebi(id).style.background = attr;
  return true;
}

function js_color_font(id,attr){
  gebi(id).style.color = attr;
  return true;
}

function js_open(url,name,w,h,extra){
  center = "No";
  if(extra){
    if(extra.indexOf("center_screen") != -1){
      var p_top = (screen.availHeight - h)/2 - 20;
      var p_left = (screen.availWidth - w)/2;
      var screen_location = 'top='+p_top+',left='+p_left;
      extra = extra.replace("center_screen",screen_location);
      center = "Yes";
    }
  }else{
    extra = "";
  }
  win = window.open('',name,"dependent=0,fullscreen=0,toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=auto,resizable=0,height="+h+",width="+w+","+extra);
  if (win) {
    try{
      win.location=url;
    } catch(e) {}
    win.focus();
  }
  return win;
}

function js_has_popup_blocker() {
  var test_popup = window.open("","win1",'width=1,height=1');
  if (test_popup) {
    test_popup.close();
    return false;
  }
  else {
    return true;
  }
}

function js_clear_select(psSelectId) {
  if (gebi(psSelectId)) {
    var moSelect = gebi(psSelectId);
    var i=0;
    for (i=moSelect.options.length; i>=0; i--)
      moSelect.options[i] = null;
    return true;
  }
  return false;
}

function js_clear_optgroup(psOptGroupId) {
  if (gebi(psOptGroupId)) {
    var moOptGroup = gebi(psOptGroupId);
    while(moOptGroup.lastChild) {
      moOptGroup.removeChild(moOptGroup.lastChild);
    }
    return true;
  }
  return false;
}

function js_populate_optgroup(psOptGroupId, psSerializedInfo) {
  if ((gebi(psOptGroupId)) && (psSerializedInfo)) {
    var moOptGroup = gebi(psOptGroupId);
    var maUnserializedInfo = soUnSerializer.unserialize(psSerializedInfo);
    var maOption = new Array();
    js_clear_optgroup(psOptGroupId);
    for (i=0;i<maUnserializedInfo.length;i++) {
      maOption = maUnserializedInfo[i];
      moOption = new Option(maOption[1], maOption[0], false, false);
      moOptGroup.appendChild(moOption);
    }
    if (moOptGroup.firstChild) moOptGroup.firstChild.selected = true;
  }
  return false;
}

function js_populate_select(psSelectId, psSerializedInfo) {
  if ((gebi(psSelectId)) && (psSerializedInfo)) {
    var moSelect = gebi(psSelectId);
    var maUnserializedInfo = soUnSerializer.unserialize(psSerializedInfo);
    var maOption = new Array();
    js_clear_select(psSelectId);
    for (i=0;i<maUnserializedInfo.length;i++) {
      maOption = maUnserializedInfo[i];
      moSelect.options[i] = new Option(maOption[1], maOption[0], false, false);
    }
    if (moSelect.options[0]) moSelect.options[0].selected = true;
    return true;
  }
  return false;
}

// array associativo para substitui��o de substrings
var StrReplaceArray = new Array();

function js_replace(psStaticId, psFindStr, psNewStr) {
  if (!psStaticId || !psFindStr || !psNewStr) return false;
  psStaticId += '_td';	// id do <TD> fixo como '<nome_do_static>_td'
  if (!StrReplaceArray[psStaticId])
    StrReplaceArray[psStaticId] = gebi(psStaticId).innerHTML;
  var str = StrReplaceArray[psStaticId];
  gebi(psStaticId).innerHTML = str.replace(psFindStr, psNewStr);
  return true;
}

function js_submit(psEventName,psMode){
  var mbAsync = (psMode=='ajax');
  frm = document.getElementsByTagName("form");
  first = frm[0].elements[0];
  frm_value = document.createElement('input');
  frm_value.name = "fwd_submit_trigger";
  frm_value.type = "hidden";
  frm_value.value = psEventName;
  first.parentNode.insertBefore(frm_value,first);
  if(mbAsync){
    frm[0].target = 'fwd_iframe_upload';
    frm[0].submit();
    frm[0].target = '';
  }else{
    frm[0].submit();
  }
  first.parentNode.removeChild(frm_value); 
  return true;
}

function goto_url(url, popup){
		if (!popup) document.location.href = url;
		else window.open(url , '' , 'width=800,height=600,location=YES,menubar=YES,titlebar=YES,resizable=YES,status=YES,directories=YES,scrollbars=YES');
}

function js_check_value(id,attr){
  if(gebi(id).value == attr){
    return true;
  }
  return false;
}

function js_element_in_controller(psElement,psControllerName){
  var maController = gebi(psControllerName).value.split(':');
  for (var i=0; i<maController.length; i++) {
    if (maController[i] == psElement)
      return true;
  }
  return false;
}

function js_resize(id,attr_width,attr_height){
  pos = attr_width.search('%');
  if(pos>0){
    attr_width = attr_width.substr(0,pos);
    gebi(id).style.width = parseInt(gebi(id).style.width)*(attr_width/100);
  }else{
    gebi(id).style.width = attr_width;
  }
  pos = attr_height.search('%');
  if(pos>0){
    attr_height = attr_height.substr(0,pos);
    gebi(id).style.height = parseInt(gebi(id).style.height)*(attr_height/100);
  }else{
    gebi(id).style.height = attr_height;
  }
  return true;
}

function js_set_content(id,attr){
  // verificar funcionalidade e qual atributo ( value ou innerHTML) deve ser modificado
  gebi(id).value = attr;
  return true;
}

function js_show(id){
  moObject = gobi(id);
  if(moObject && moObject.show){
       moObject.show();
  }else{
      gebi(id).style.display = 'block';
  }
  return true;
}

function js_hide(id){
  moObject = gobi(id);
  if(moObject && moObject.hide){
       moObject.hide();
  }else{
      gebi(id).style.display = 'none';
  }
  return true;
}

function js_create_elem(id,content){
  newDiv = document.createElement('div');newDiv.id=id;
  newDiv.style.position='absolute';newDiv.style.left='0px';newDiv.style.top='0px';
  gebi('dialog').parentNode.insertBefore(newDiv,gebi('dialog'));
  newDiv.innerHTML=content;
  return true;
}

function js_reposition(id,left,top){
  gebi(id).style.left = left;
  gebi(id).style.top = top;
  return true;
}

function js_temp(time){
  setTimeout('js_temp('+time+')',time);
  return true;
}

function js_change_cursor(id,cursor){
  gebi(id).style.cursor = cursor;
  return;
}

function js_change_class(id,new_class){
  gebi(id).className = new_class;
  return;
}

function js_invert_tabitem_class(id){
  var classNameSplit = gebi(id).className.split(" ");
  
  var newClass = null;
  for(i = 0; i < classNameSplit.length; i++){
	  full_class_name = classNameSplit[i];
	  if(!full_class_name || full_class_name.length == 0){
		  continue;
	  }
	  class_prefix = full_class_name.substring(0, full_class_name.lastIndexOf('_'));
	  
	  class_suffix = full_class_name.substring(full_class_name.lastIndexOf('_'));
	  
	  if (class_suffix=='_selected'){ 
		  continue;
	  } else {
		if (class_suffix=='_notSelected'){
			if(newClass){
				newClass += " "+class_prefix+'_over';
			}else{
				newClass = class_prefix+'_over';
			}
		}else{
			if(newClass){
				newClass += " " +class_prefix+'_notSelected';
			}else{
				newClass = class_prefix+'_notSelected';	
			}
		}
	  }
  }
  if(newClass && newClass.length > 0){
	  gebi(id).className = newClass;
  }
  
  return;
}

function js_save_user_prefs(userPrefsId,elementsIds){
  var userPrefs = gebi(userPrefsId);
  if(!userPrefs) return;
  var ids = elementsIds.split(':');
  var element,obj;
  var objects = new Array();
  for(var i=0;i<ids.length;i++){
    element = gebi(ids[i]);
    if(element){
      obj = new Object();
      obj.name = element.id;
      obj.height = element.style.height;
      obj.width = element.style.width;
      obj.left = element.style.left;
      obj.top = element.style.top;
      obj.serializeAttribute = function(attributeName){
        var attribute = String(this[attributeName]);
        var str = 's:'+attributeName.length+':"'+attributeName+'";';
        str+= 's:'+attribute.length+':"'+attribute+'";';
        return str;
      }
      obj.toString = function(){
        var str = 'a:5:{';
        str+= this.serializeAttribute('name');
        str+= this.serializeAttribute('top');
        str+= this.serializeAttribute('left');
        str+= this.serializeAttribute('height');
        str+= this.serializeAttribute('width');
        str+= '}';
        return str;
      }
      objects.push(obj);
    }
  }
  var length = objects.length;
  var str = 'a:'+objects.length+':{';
  for(var i=0;i<length;i++){
    str+= 'i:'+i+';'+objects[i];
  }
  str+= '}';
  userPrefs.value = str;
}

function js_grid_correct_selected_rows(grid_name) {

  function row_exists(gridName,rowId) {
    return gebi(gridName+'_row_'+rowId) ? true:false;
  }
  
  var corrected_rows = '';
  var i = 1;
  while ( row_exists(grid_name,i) && (gebi(grid_name+'_row_'+i).className == gebi(grid_name+'_row_selected').value) ) {
    corrected_rows += ':'+grid_name+'_row_'+i+';'+gebi(grid_name+'_row_'+i).getAttribute('actionTarget');
    i++;
    if (!gebi(grid_name+'_row_'+i)) break;
  }
  gebi(grid_name+'_value_row_id').value = corrected_rows;
  return;
}

function js_backup(event) {
	e = event;		
	kc = e.keyCode ? e.keyCode : e.which;
	str = String.fromCharCode(kc);
	alert('kc = ' + kc + ' str = ' + str);
	invalid_key_codes = new Array('9','13','33','34','35','36','37','38','39','40','45');
    if(arrayIndexOf(invalid_key_codes,kc) == -1) {		  
	  return true;
	}
	else return false;
}

/**
 * Array com valores de elementos. Indexado pelo id do elemento.
 * @var array
 * @access public
 */
var ElementsArray = new Array();

/**
 * Verifica se o valor de um elemento mudou.
 *
 * <p>M�todo para verificar se o valor de um elemento foi modificado. Caso tenha sido, retorna
 * verdadeiro, caso contr�rio, retorna falso.</p>
 *
 * @param string psElementId Id do elemento
 */
function js_has_changed(psElementId) {
	moElement = gebi(psElementId);	
	
	if (ElementsArray[psElementId] == undefined)
		ElementsArray[psElementId] = '';

	if (ElementsArray[psElementId] != moElement.value) {
		ElementsArray[psElementId] = moElement.value;
		return true;
	}
	else
		return false;
}


/*******************************************************************************
 * Dispara o evento de refresh da grid. [DEPRECATED]
 * 
 * <p>Dispara o refresh da grid.
 * [DEPRECATED]: Use o m�todo refresh() diretamente.</p>
 *
 * @package javascript
 * @param String Nome da grid
 *******************************************************************************/
function js_refresh_grid(psGridName) {
  //soTabManager.lockTabs();
  moGrid = gobi(psGridName);
  if(moGrid)
    moGrid.showGlass();
	trigger_event(psGridName+'refresh',3);
};