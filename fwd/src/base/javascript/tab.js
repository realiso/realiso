/*******************************************************************************
 * Classe FWDDummyHistoryManager. Gerenciador de hist�rico espec�fico do IE.
 * 
 * <p>Classe que gerencia o hist�rico, permitindo usar o bot�o de back do
 * browser em p�ginas baseadas em AJAX. Na verdade esse classe n�o faz nada.
 * Ela existe pois no IE estava dando erro de javascript em alguns casos. Como
 * n�o consegui identificar o motivo do erro, criei essa classe dummy para ser utilizada
 * quando o browser for o IE.</p>
 *
 * @package javascript
 *******************************************************************************/
function FWDDummyHistoryManager(){
  var moParent = window;
  var moChild = null;
  while(moChild!=moParent){
    moChild = moParent;
    moParent = moParent.parent;
  }
  if(moParent.soHistoryManager){
    moChild = null;
    return moParent.soHistoryManager;
  }
  var self = this;
  self.coDictionary = {};
  self.csCurrentState = location.hash.substr(1);
  self.coIframe = document.createElement('IFRAME');
 
  self.getHash = function(){return '';}
  self.checkHash = function(){}
  self.addEntry = function(psHash){}
}

/*******************************************************************************
 * Classe FWDHistoryManager. Gerenciador de hist�rico.
 * 
 * <p>Classe que gerencia o hist�rico, permitindo usar o bot�o de back do
 * browser em p�ginas baseadas em AJAX.</p>
 *
 * @package javascript
 *******************************************************************************/
function FWDHistoryManager(){
  // O FWDHistoryManager � �nico, se j� existe um, retorna ele mesmo.
  var moParent = window;
  var moChild = null;
  while(moChild!=moParent){
    moChild = moParent;
    moParent = moParent.parent;
  }
  if(moParent.soHistoryManager){
    moChild = null;
    return moParent.soHistoryManager;
  }

 /**
  * Refer�ncia para si mesmo
  * @var FWDHistoryManager
  */
  var self = this;

 /**
  * Dicion�rio de endere�os. Guarda os endere�os completos de p�ginas que t�m uma tab default.
  * @var Object
  */
  self.coDictionary = {};

 /**
  * Guarda o endere�o atual
  * @var String
  */
  self.csCurrentState = location.hash.substr(1);

 /**
  * Iframe usado para criar entradas no hist�rico no IE
  * @var HTMLIFrameElement
  */
  self.coIframe = document.createElement('IFRAME');

  if(browser.isIE){
    self.coIframe.style.visibility = 'hidden';
    self.coIframe.style.position = 'absolute';
    self.coIframe.src = lsBaseRef+'mockPage.php'; // lsBaseRef � definida no arquivo js.php
    FWDEventManager.addEvent(
      window,
      'load',
      function(){
        document.getElementsByTagName('body').item(0).appendChild(self.coIframe);
        self.ciInterval = setInterval('soHistoryManager.checkHash()',200);
      }
    );
  }else{
    self.ciInterval = setInterval('soHistoryManager.checkHash()',200);
  }
  
  // derreferencia os objetos locais
  moChild = moParent = null;
  
 /*****************************************************************************
  * Retorna o endere�o atual (hash)
  *
  * <p>Retorna o endere�o atual (hash).</p>
  * 
  * @return String Endere�o atual
  *****************************************************************************/
  self.getHash = function(){
    if(browser.isIE){
      return self.coIframe.contentWindow.getHash();
    }else{
      return location.hash.substr(1);
    }
  }
  
 /*****************************************************************************
  * M�todo executado periodicamente para verificar se o endere�o foi modificado
  *
  * <p>M�todo executado periodicamente para verificar se o endere�o foi
  * modificado pelo usu�rio. Se o endere�o foi modificado, muda de aba para
  * refletir a mudan�a de endere�o.</p>
  *****************************************************************************/
  self.checkHash = function(){
    var maHash,msSrc,msHash,moException;
    try{
      msHash = self.getHash();
      if(msHash!=self.csCurrentState && (!self.coDictionary[msHash] || (self.coDictionary[msHash] && self.coDictionary[msHash]!=self.csCurrentState))){
        self.csCurrentState = msHash;
        if(self.coDictionary[msHash] && self.coDictionary[msHash].length>msHash.length) msHash = self.coDictionary[msHash];
        maHash = msHash.split('=');
        if(self.coDictionary[msHash]){
          msSrc = soTabManager.getTabSrc(self.coDictionary[msHash]);
        }else{
          msSrc = soTabManager.getTabSrc(msHash);
        }
        soTabManager.changeTab(maHash[0],maHash[1],msSrc,false);
      }
    }catch(e){
      // Esse try-catch existe porque, no IE, dependendo de quando esse m�todo �
      // executado, por uma infelicidade do destino, um erro � gerado.
    }
    maHash = msSrc = msHash = moException = null;
  }
  
 /*****************************************************************************
  * Adiciona uma entrada no hist�rico.
  *
  * <p>Adiciona uma entrada no hist�rico e redefine o endere�o atual.</p>
  * 
  * @param String psHash Novo endere�o
  *****************************************************************************/
  self.addEntry = function(psHash){
    var msSrc,miPos,msNewSrc;
    if(browser.isIE){
      msSrc = self.coIframe.src;
      miPos = msSrc.indexOf('?');
      msNewSrc = (miPos==-1?msSrc:msSrc.substr(0,miPos));
      self.coIframe.contentWindow.getHash = null;
      self.coIframe.src = msNewSrc+'?'+psHash;
    }
    self.csCurrentState = psHash;
    location.hash = psHash;
    msSrc = miPos = msNewSrc = psHash = null;
  }

}

/*******************************************************************************
 * Classe FWDTabManager. Gerenciador global de tabs.
 * 
 * <p>Classe que gerencia as tabs de todos os n�veis de tabgroups.</p>
 *
 * @package javascript
 *******************************************************************************/
function FWDTabManager(){
  // Se estiver dentro de um iframe, retorna a inst�ncia do pai
  var moParent = window;
  var moChild = null;
  while(moChild!=moParent){
    moChild = moParent;
    moParent = moParent.parent;
  }
  if(moParent.soTabManager){
    moChild = null;
    return moParent.soTabManager;
  }

 /**
  * Refer�ncia para si mesmo
  * @var FWDTabManager
  */
  var self = this;

 /**
  * Janela raiz (onde fica a �nica inst�ncia do gerenciador de PopUps e todas as PopUps)
  * @var Window
  */
  self.coRootWindow = moParent;

 /**
  * N�mero de abas que est�o sendo carregadas
  * @var integer
  */
  self.ciLoading = 0;

 /**
  * Vidro que impede o usu�rio de clicar nas abas antes delas terminarem de carregar
  * @var FWDGlass
  */
  self.coGlass = new FWDGlass('blue',5);
  
  // derreferencia os objetos locais
  moChild = moParent = null;
  
 /*****************************************************************************
  * Retorna a URL que est� carregada numa determinada aba.
  *
  * <p>Retorna a URL que est� carregada numa determinada aba.</p>
  * 
  * @param String psPath Caminho da aba
  * @return String URL que est� carregada na aba
  *****************************************************************************/
  self.getTabSrc = function(psPath){
    var maPath,moWindow,moIframe,msIframeId,msSrc;
    maPath = psPath.replace('=',':').split(':');
    moWindow = window;
    // Pega a window da folha selecionada
    for(var i=0;i<maPath.length;i+=2){
      msIframeId = maPath[i] + '_iframe_' + maPath[i+1];
      moIframe = moWindow.document.getElementById(msIframeId);
      moWindow = moIframe.contentWindow;
    }
    msSrc = moIframe.src;
    maPath = moWindow = moIframe = msIframeId = null;
    return msSrc;
  }
  
 /*****************************************************************************
  * Muda a aba selecionada de um TabGroup
  *
  * <p>Muda a aba selecionada de um TabGroup.</p>
  * 
  * @param String psTabGroupPath Caminho completo do TabGroup
  * @param integer piTabNumber N�mero da aba a ser selecionada
  * @param String psUrl Url a ser carregada na aba
  * @param boolean pbAddEntry Indica se deve ser criada uma entrada no hist�rico
  *****************************************************************************/
  self.changeTab = function(psTabGroupPath,piTabNumber,psUrl,pbAddEntry){
    if(pbAddEntry==undefined) pbAddEntry = true;
    var maPath = psTabGroupPath.split(':');
    var msTabGroupId = maPath[maPath.length-1];
    var moWindow = window;
    var msIframeId;
    var msHash = psTabGroupPath+'='+piTabNumber;
    // Garante que todo o caminho certo esteja selecionado
    if(maPath.length>1){
      var miParentNumber = maPath[maPath.length-2];
      var msPath = maPath.slice(0,maPath.length-2).join(':');
      self.changeTab(msPath,miParentNumber,false,false);
    }
    // Pega a window da folha selecionada
    for(var i=0;i<maPath.length-1;i+=2){
      msIframeId = maPath[i] + '_iframe_' + maPath[i+1];
      moWindow = moWindow.document.getElementById(msIframeId).contentWindow;
    }
    //POG para esconder os do sistema
    soMenuManager.hideMenus();
    var moTabSubManager = moWindow.soTabSubManager;
    var gebi = moWindow.gebi;
    var makeLoading = moWindow.makeLoading;
    var msIframeId = msTabGroupId+'_iframe_'+piTabNumber;
    var msContentId = msTabGroupId+'_content_'+piTabNumber;
    var msTabId = msTabGroupId+'_tab_'+piTabNumber;
    var miSelectedTab = moTabSubManager.getSelectedTab();
    var moTab = gebi(msTabId+'_value');
    // Deseleciona a aba que estava selecionada
    gebi(msTabGroupId+'_content_'+moTabSubManager.ciSelectedTab).style.display = "none";
    var moOldSelected = gebi(msTabGroupId+'_tab_'+moTabSubManager.ciSelectedTab+'_value');
    if(moOldSelected){
    	var classNameSplit = moOldSelected.className.split(" ");
    	var newClass = null;
    	for(i = 0; i < classNameSplit.length; i++){
    		var split = classNameSplit[i];
    		if(!split || split.length == 0){
    			  continue;
    		}
    		if(newClass){
    			newClass += " " + split.substr(0,split.lastIndexOf('_'))+'_notSelected';	
    		}else{
    			newClass = split.substr(0,split.lastIndexOf('_'))+'_notSelected';
    		}
    	}
    	if(newClass && newClass.length > 0){
    		
    		moOldSelected.className = newClass;    		
    	}
    }
    // Seleciona a nova aba
    moTabSubManager.ciSelectedTab = piTabNumber;
    if(gebi(msContentId)) gebi(msContentId).style.display = "block";
    if(moTab){
    	var classNameSplit = moTab.className.split(" ");
    	var newClass = null;
    	for(i = 0; i < classNameSplit.length; i++){
    		var split = classNameSplit[i];
    		if(!split || split.length == 0){
    			  continue;
    		}
    		if(newClass){
    			newClass += " " + split.substr(0,split.lastIndexOf('_'))+'_selected';	
    		}else{
    			newClass = split.substr(0,split.lastIndexOf('_'))+'_selected';
    		}
    	}
    	if(newClass && newClass.length > 0){
    		moTab.className = newClass;    		
    	}
    }
    	
    
    var mbSrcChanged;
    if(psUrl){
      mbSrcChanged = !moWindow.equalUrls(psUrl,gebi(msIframeId).src);
    }else{
      mbSrcChanged = false;
    }
    // Testa se tem que carregar a p�gina
    if(psUrl && pbAddEntry && (mbSrcChanged || miSelectedTab==piTabNumber)){
      self.initTabGroup();
      var moOldIframe = gebi(msIframeId);
      var moIframeParent = moOldIframe.parentNode;
      var moNewIframe = moWindow.document.createElement('IFRAME');
      moNewIframe.id = moNewIframe.name = msIframeId;
      moNewIframe.width = moNewIframe.height = '100%';
      moNewIframe.marginwidth = moNewIframe.marginheight = 0;
      moNewIframe.hspace = moNewIframe.vspace = 0;
      moNewIframe.frameBorder = 0;
      moNewIframe.style.zIndex = -1;
      if(!psUrl) psUrl = moOldIframe.src;
      var msJoin = ((psUrl.indexOf('?')==-1)?'?':'&');
      var msUrl = psUrl+msJoin+getNumber();
      moOldIframe.src = '';
      moIframeParent.removeChild(moOldIframe);
      moIframeParent.appendChild(moNewIframe);
      moWindow.loadIframe(moNewIframe,psUrl,msContentId);
    }else{
      // j� est� carregado
      var moChildWindow = window;
      var msTabGroupId;
      var msSelectedTab;
      var msChildIframeId;
      var maFullPath = new Array();
      while(moChildWindow.soTabSubManager){
        msTabGroupId = moChildWindow.soTabSubManager.getTabGroupId();
        msSelectedTab = moChildWindow.soTabSubManager.getSelectedTab();
        msChildIframeId = msTabGroupId+'_iframe_'+msSelectedTab;
        if(moChildWindow.document.getElementById(msChildIframeId)){
          moChildWindow = moChildWindow.document.getElementById(msChildIframeId).contentWindow;
          maFullPath.push(msTabGroupId);
          maFullPath.push(msSelectedTab);
        }else{
          moChildWindow = new Object();
        }
      }
      var msUrl = psTabGroupPath+'='+piTabNumber;
      if(maFullPath.length>maPath.length){
        var msFullPath = maFullPath.slice(0,maFullPath.length-1).join(':');
        msFullPath+= '='+maFullPath[maFullPath.length-1];
        msHash = msFullPath;
      }
    }
    if(pbAddEntry) soHistoryManager.addEntry(msHash);
  }
  
 /*****************************************************************************
  * Avisa para o gerenciador que uma aba terminou de carregar
  *
  * <p>Avisa para o gerenciador que a aba identificada por psPath terminou de
  * carregar.</p>
  * 
  * @param String psPath Caminho da aba que terminou de carregar
  *****************************************************************************/
  self.notifyLoading = function(psPath){
    var msHash,moDictionary;
    msHash = location.hash.substr(1);
    moDictionary = soHistoryManager.coDictionary;
    if(--self.ciLoading==0) self.coGlass.hide();
    else if(self.ciLoading<0) self.ciLoading = 0;
    if(!moDictionary[msHash] || moDictionary[msHash].length<psPath.length){
      moDictionary[msHash] = psPath;
    }
    msHash = moDictionary = psPath = null;
  }
  
 /*****************************************************************************
  * Avisa para o gerenciador que uma aba est� sendo carregada.
  *
  * <p>Avisa para o gerenciador que uma aba est� sendo carregada.</p>
  *****************************************************************************/
  self.initTabGroup = function(){
    if(self.ciLoading==0){
      document.getElementsByTagName('body').item(0).style.overflow = 'hidden';
      self.coGlass.show(10000);
    }
    self.ciLoading++;
  }

 /*******************************************************************************
  * Retorna a janela principal.
  * 
  * <p>Retorna a janela principal.</p>
  *
  * @return Window Janela principal
  *******************************************************************************/
  self.getRootWindow = function(){
    return self.coRootWindow;
  }

 /*****************************************************************************
  * Exibe o vidro que bloqueia as abas.
  *
  * <p>Exibe o vidro que bloqueia as abas.</p>
  *****************************************************************************/
  self.lockTabs = function(){
    document.getElementsByTagName('body').item(0).style.overflow = 'hidden';
    self.coGlass.show(10000);
  }
  
 /*****************************************************************************
  * Oculta o vidro que bloqueia as abas.
  *
  * <p>Oculta o vidro que bloqueia as abas.</p>
  *****************************************************************************/
  self.unlockTabs = function(){
    self.coGlass.hide();
  }
  
}

/*******************************************************************************
 * Classe FWDTabSubManager. Gerenciador local de tabs.
 * 
 * <p>Classe que gerencia as tabs somente da p�gina atual.</p>
 *
 * @package javascript
 *******************************************************************************/
function FWDTabSubManager(){

 /**
  * Refer�ncia para si mesmo
  * @var FWDTabSubManager
  */
  var self = this;

 /**
  * Id do TabGroup (s� pode ter um TabGroup na p�gina)
  * @var String
  */
  self.csTabGroupId = '';

 /**
  * N�mero da aba selecionada
  * @var integer
  */
  self.ciSelectedTab = 0;

 /**
  * Caminho da janela atual dentro da hierarquia de abas
  * @var String
  */
  self.csPath = '';

  if(parent!=window && parent.soTabSubManager){
    var moParentManager,msSelectedTab,msParentPath;
    moParentManager = parent.soTabSubManager;
    msSelectedTab = moParentManager.getTabGroupId()+':'+moParentManager.getSelectedTab();
    msParentPath = parent.soTabSubManager.getPath();
    if(msParentPath!='') self.csPath =  msParentPath + ':' + msSelectedTab;
    else self.csPath = msSelectedTab;
    moParentManager = msSelectedTab = msParentPath = null;
    FWDEventManager.addEvent(
      window,
      'load',
      function(){ parent.soTabSubManager.notifyLoading(); }
    );
  }
  
  
 /*****************************************************************************
  * Inicializa o TabGroup.
  *
  * <p>Inicializa o TabGroup da janela atual.</p>
  * 
  * @param integer psTabGroupId Id do TabGroup
  * @param integer piTabNumber N�mero da aba inicialmente selecionada
  *****************************************************************************/
  self.initTabGroup = function(psTabGroupId,piTabNumber){
    self.csTabGroupId = psTabGroupId;
    self.ciSelectedTab = piTabNumber;
    soTabManager.initTabGroup();
    if(parent && parent.soTabSubManager){
      parent.soTabSubManager.hideLoading();
    }
    psTabGroupId = null;
  }
  
 /*****************************************************************************
  * Retorna o id do TabGroup.
  *
  * <p>Retorna o id do TabGroup.</p>
  * 
  * @return String Id do TabGroup
  *****************************************************************************/
  self.getTabGroupId = function(){
    return self.csTabGroupId;
  }
  
 /*****************************************************************************
  * Retorna o n�mero da aba selecionada.
  *
  * <p>Retorna o n�mero da aba selecionada.</p>
  * 
  * @return integer N�mero da aba selecionada
  *****************************************************************************/
  self.getSelectedTab = function(){
    return self.ciSelectedTab;
  }
  
 /*****************************************************************************
  * Retorna o caminho da janela atual.
  *
  * <p>Retorna o caminho da janela atual dentro da hierarquia de abas.</p>
  * 
  * @return String Caminho da janela atual
  *****************************************************************************/
  self.getPath = function(){
    return self.csPath;
  }
  
 /*****************************************************************************
  * Muda a aba selecionada
  *
  * <p>Muda a aba selecionada.</p>
  * 
  * @param integer piTabNumber N�mero da aba a ser selecionada
  * @param String psUrl Url a ser carregada na aba
  *****************************************************************************/
  self.changeTab = function(piTabNumber,psUrl){
    var msPath = self.csPath;
    if(msPath!='') msPath+= ':';
    msPath+= self.csTabGroupId;
    soTabManager.changeTab(msPath,piTabNumber,psUrl);
    self.ciSelectedTab = piTabNumber;
    msPath = psUrl = null;
  }
  
 /*****************************************************************************
  * Avisa para o gerenciador que uma aba terminou de carregar
  *
  * <p>Avisa para o gerenciador que uma aba terminou de carregar.</p>
  *****************************************************************************/
  self.notifyLoading = function(){
    var msPath;
    msPath = self.csPath;
    if(msPath!='') msPath+= ':';
    msPath+= self.csTabGroupId+'='+self.ciSelectedTab;
    self.hideLoading();
    soTabManager.notifyLoading(msPath);
    msPath = null;
  }
  
 /*****************************************************************************
  * Oculta o loading correspondente � aba selecionada.
  *
  * <p>Oculta o loading correspondente � aba selecionada.</p>
  *****************************************************************************/
  self.hideLoading = function(){
    hideLoading(self.csTabGroupId+'_content_'+self.ciSelectedTab);
  }
  
 /*****************************************************************************
  * Retorna o objeto window contido em uma aba.
  *
  * <p>Retorna o objeto window contido em uma aba.</p>
  * 
  * @param integer piIndex N�mero da aba desejada
  * @return Window Objeto window contido na aba desejada
  *****************************************************************************/
  self.getChildWindowByIndex = function(piIndex){
    var moIframe = gebi(self.csTabGroupId+'_iframe_'+piIndex);
    if(moIframe){
      return moIframe.contentWindow;
    }else{
      return null;
    }
  }
  
 /*****************************************************************************
  * Retorna o objeto window contido na aba selecionada.
  *
  * <p>Retorna o objeto window contido na aba selecionada.</p>
  * 
  * @return Window Objeto window contido na aba selecionada
  *****************************************************************************/
  self.getSelectedWindow = function(){
    return self.getChildWindowByIndex(self.ciSelectedTab);
  }
  
}

/**
 * Inst�ncia �nica do gerenciador global de abas
 * @var FWDTabManager
 */
var soTabManager = new FWDTabManager();

/**
 * Inst�ncia �nica do gerenciador local de abas
 * @var FWDTabSubManager
 */
var soTabSubManager = new FWDTabSubManager();

/**
 * Inst�ncia �nica do gerenciador de hist�rico
 * @var FWDHistoryManager
 */
if(!browser.isIE) var soHistoryManager = new FWDHistoryManager();
else var soHistoryManager = new FWDDummyHistoryManager();
