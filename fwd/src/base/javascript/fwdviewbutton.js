/**
 * FWD - FAST WEB DEVELOPMENT
 * 
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/*******************************************************************************
 * Classe FWDViewButton. Classe com as fun��es utilizadas pelo viewButton.
 * 
 * <p>Classe com as fun��es utilizadas pelo viewButton.</p>
 * @package FWD
 * @param String psName Nome do elemento
 * @param String psDisabled se o viewbutton esta desabilitado
 
 *******************************************************************************/
function FWDViewButton(psName,psDisabled)
{
  /** @var Object referencia para ele mesmo. */
  var self = this;
  /** @var Object objeto da div que cont�m o conte�do do viewbutton. */
  self.coDv = gebi(psName);
  /** @var Object objeto da div que cont�m a imagem transparente e os eventos do viewbutton. */
  self.coImgEvent = gebi(psName+'_dvImg');
  /** @var Object objeto da div que cont�m o vidro que simula um viewbutton desabilitado. */  
  self.coImgGlass = gebi(psName+'_dvImgDisabled');
  /** @var Boolean indica se o viewbutton est� desabilitado. */  
  self.cbDisabled = psDisabled;
  /** @var String Nome do elemento. */
  self.csName = psName;
  
/*****************************************************************************
 * Esconde o viewButton
 *
 * <p> Esconde o viewButton</p>
 *
 *****************************************************************************/
  self.hide = function(){
	self.coDv.style.display='none';
	self.coImgEvent.style.display='none';  
	self.coImgGlass.style.display='none';
  }
/*****************************************************************************
 * Exibe o viewButton
 *
 * <p> Exibe o viewButton e o vidro ou a imagem transparente, dependendo do valor do 'disable'
 * do viewButton</p>
 *
 *****************************************************************************/
  self.show = function(){
  	self.coDv.style.display='block';
	if(self.cbDisabled==true)
		self.coImgGlass.style.display='block';
	else
		self.coImgEvent.style.display='block';  
  }
 /*****************************************************************************
 * Desabilita o viewButton
 *
 * <p> Desabilita o viewButton, mostrando um vidro na frente dele, n�o permitindo que os eventos
 *sejam disparados no onclick do viewbutton</p>
 *
 * @param String psCSSClass Nome da classe css que deve ser utilizada (parametro opcional)
 *****************************************************************************/
  self.disable = function(psCSSClass){
  	self.coImgEvent.style.display='none';
  	self.coImgGlass.style.display='block';
  	  	self.coImgGlass.style.zIndex=2;
  	self.cbDisabled = true;
	if(psCSSClass!=undefined)  	
  		self.coImgGlass.className=psCSSClass;
  }
 /*****************************************************************************
 * Abilita o viewButton
 *
 * <p>Abilita o viewButton</p>
 *
 *****************************************************************************/
  self.enable = function(){
  	self.coImgEvent.style.display='block';
  	self.coImgGlass.style.display='none';
  	self.coImgGlass.style.zIndex=0;
  	self.cbDisabled= false;
  }	
};