/**
 * FWD - FAST WEB DEVELOPMENT
 * 
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/*******************************************************************************
 * Classe FWDGridScript. Classe com as fun��es utilizadas pela FWDGrid.
 * 
 * <p>Classe abstrata que serve de base para todos elementos da FID.</p>
 * @package FWD
 * @param String name Nome do elemento
 *******************************************************************************/
function FWDGridScript(psName,pbDinamicFill)
{
  var self = this;
  /** @var String Nome do elemento. */
  self.csName = psName;
  /** @var String Nome da ultima (ou ultimas) linhas selecionadas da grid. */
  self.cs_grid_selected_row = "";
  /** @var Object InputHidden que guarda o valor da coluna "selectColumn" da grid corespondente a linha(s) seleciondada(s). */
  self.coInputHidden=null;//gebi(self.csName+'_value');
  /** @var Object Div dos bot�es de pagina��o da grid. */
  self.coDivPageButton = gebi(self.csName+'_page_buttons');
  /** @var Object Div dos headers das colunas da grid. */
  self.coDivColumns = gebi(self.csName+'_column_code');
  /** @var Object Div do t�tulo da grid. */
  self.coDivTitle = gebi(self.csName+'_header_title');
  /** @var Object Grid (self). */
  self.coThis = gebi(self.csName);
  /** @var Integer Altura da Div dos headers das colunas da grid. */
  self.ciColumnsHeight = gebi(self.csName+'_page_button_columns_height');
  /** @var Object Div do corpo da grid. */
  self.coDivBody = gebi(self.csName+'_body_content');
  /** @var Object Static do bot�es de p�gina��o que cont�m o n�mero total de p�ginas da grid. */
  self.moStaticTotalPages = null;
  /** @var Object TextBox dos bot�es de pagina��o que cont�m n�mero da p�gina atual da grid. */
  self.coTextBoxActualPage = null;  
  /** @var Object InputHidden que cont�m o valor da p�gina atual e do n�mero total de p�ginas da grid. */
  self.coInputPagesInfo = null; 
  /** @var Array que cont�m a p�gina atual e o n�mero total de p�ginas da grid. */
  self.caInfoPages =""; 
  /** @var Object Div dos menus de contexto ACL da grid. */
  self.divMenuACL = gebi(self.csName+'_menu_acl');
  /** @var Object Button de refresh da grid. */
  self.buttonRefresh = gebi(self.csName+'_button_refresh_div');
  /** @var Object Armazena a classe zebraOver da grid. */
  self.classRowOver =  gebi(self.csName+'_row_over');
  /** @var Object Armazena a classe rowSelected da grid. */
  self.classRowSelected =  gebi(self.csName+'_row_selected');
  /** @var Integer Id da ultima linha da sele��o de linhas atuais. */
  self.ciShiftLastRow = 0;
  /** @var Integer Id da primeira linha da sele��o de linhas atuais. */
  self.ciShiftFirstRow = 0;
  /** @var Integer Id da linha clicada na sele��o de linhas passada. */
  self.ciShiftPastRow = 0;
  /** @var Object InputHidden que guarda os ids das linhas concatedados com o valor da coluna selectColumn da grid. */
  self.coInputHiddenRowId = null;
  /** @var Integer Indica se deve atualizar ou n�o o conte�do da grid em um refresh. */
  self.must_update_content = 1;
  /** @var Boolean Indica se o refresh da grid deve ser executado na p�gina atual ou na primeira p�gina*/
  self.refreshOnCurrentPage = gebi(self.csName + '_refresh_on_current_page').value;

  self.coGlass = new FWDGlass('blue',5);
  self.coGlass.atachToElement(self.csName);
  
//{$this->csName}
  /*****************************************************************************
   * Mostra o vidro encima da grid.
   *
   * <p>Mostra o vidro encima da grid para bloquear qualquer tentativa do usu�rio executar
   * um evento da grid enquanto outro evento est� sendo executado</p>
   * 
   *****************************************************************************/
  self.showGlass = function(){
    var moParent = gebi(self.csName);
    var mbShowGlass = true;
    while(moParent){
      if(moParent.style){
	      if(moParent.style.display == 'none'){
	        mbShowGlass = false;
	        break;
	      }
	    }
      moParent = moParent.parentNode;
    }
    if(mbShowGlass){
      self.coGlass.show(2);
    }
  }
  
 /*****************************************************************************
   * Esconde o vidro da grid.
   *
   * <p>m�todo para esconder o vidro da grid</p>
   * 
   *****************************************************************************/
  self.hideGlass = function(){
    self.coGlass.hide();
  }
  
  if(pbDinamicFill){
    self.showGlass();
  }
 /**
  * Indica que o requiredCheck testa, n�o se a grid n�o est� vazia, mas se existe
  * pelo menos uma linha selecionada.
  * @var boolean
  * @access private
  */
  var selectionRequired = false;

  self.coDivButtonEdit = gebi(self.csName+'_edit_dv_button');
  /*****************************************************************************
   * Trata o evento onMouseOver para dar eveito de "zebra" na grid
   *
   * <p>Trata o evento onMouse over para a troca de cor de background da linha da grid,
   *  tambem considerando os casos em que a linha esta selecionada</p>
   * 
   * @param String psRowId Id da linha em que o mouse est� em cima.
   *****************************************************************************/
  self.o_out = function(psRowId) 
  {
    var moRow = gebi(psRowId);
    if(moRow.className != self.classRowSelected.value)
      moRow.className = moRow.getAttribute('classDefault');
  }

  /*****************************************************************************
   * Trata o evento onMouseOut para dar efeito de "zebra" na grid
   *
   * <p>Trata o evento onMouse out para a troca de cor de background da linha da grid,
   *  tambem considerando os casos em que a linha esta selecionada</p>
   * 
   * @param String psRowId Id da linha em que o mouse est� em cima.
   *****************************************************************************/
  self.o_in = function(psRowId) 
  {
    var moRow = gebi(psRowId);
    if (moRow.className != self.classRowSelected.value){
      moRow.className = self.classRowOver.value;
    }
  }

  /*****************************************************************************
   * Trata a sele��o "simples" das linhas da grid
   *
   * <p>Trata o evento onClick na linhas da grid selecinando uma �nica linha da grid, alterando assim a classe
        CSS dessa linha selecionada. Tambem � utilizada para retirar a sele��o de uma linha da grid</p>
   * 
   * @param String psRowId Id da linha que sofreu o evento onClick do mouse.
   * @param String psValue valor da coluna "selectColumn" da grid correspondente a a linha selecionada
   *****************************************************************************/
  self.grid_select_single_row = function( psRowId,psValue)
  {
    var moActualRow = gebi(psRowId);
    
    if (!((moActualRow.className == moActualRow.getAttribute('classDefault'))||(moActualRow.className == self.classRowOver.value))){
      //desceleciona a linha selecionada
      moActualRow.className = moActualRow.getAttribute('classDefault');
      self.coInputHiddenRowId.value=null;
      self.cs_grid_selected_row=null;
      self.coInputHidden.value=null;
    }else{
      //restaura a classe css de uma linha que estava selecionada anteriormente
      if (self.cs_grid_selected_row){
        var moRow = gebi(self.cs_grid_selected_row);
        if(moRow){
          moRow.className = moRow.getAttribute('classDefault');
        }
      }
      //seleciona a linha
      self.cs_grid_selected_row = psRowId;
      moActualRow.className = self.classRowSelected.value;
      //guarda nos imputHidden o valor de sele��o da grid
      var objSerialize = new fwdObjSerialize();
      self.coInputHiddenRowId.value = objSerialize.serializeByString(psRowId +';'+psValue);
      self.coInputHidden.value=objSerialize.serializeByString(psValue);
    }
  }

 /*****************************************************************************
   * Trata da sele��o multipla das linhas da grid
   *
   * <p>Trata da sele��o multipla das linhas da grid, considerando o efeito da utiliza��o da tecla
   *shift na sele��o das linhas da grid</p>
   *
   * @param String psRowId Id da linha que sofreu o evento onClick do mouse.
   *****************************************************************************/
     self.selectMultipleRows = function(psRowId)
  {
    var maRowName = psRowId.split('_');
    var posLastCarac = maRowName.length-1;

    var objSerialize = new fwdObjSerialize();
    /*
       rpereira: Esse teste � necess�rio pois em algumas situa��es o valor
       passado � '' ou 'null' ocasionando erro na nova fun��o de unserialize.
    */
    if ((self.coInputHiddenRowId.value == '') || (self.coInputHiddenRowId.value == 'null')) self.coInputHiddenRowId.value = 'a:0:{}';
    var maRowsId = objSerialize.unserializeString(self.coInputHiddenRowId.value);
		if(maRowsId==null)
		  maRowsId = new Array();
    var mbSelectRows = false;
    var moActualRow = gebi(psRowId);
    var miForBegin = 0;
    var miForEnd = 0;
    if((moActualRow.className == moActualRow.getAttribute('classDefault'))||(moActualRow.className == self.classRowOver.value))
      mbSelectRow = true;
    else
      mbSelectRow = false;
    if(self.ciShiftPastRow <= parseInt(maRowName[posLastCarac])){
        miForBegin = self.ciShiftPastRow;
        miForEnd = parseInt(maRowName[posLastCarac]);
    }else{
      miForBegin = parseInt(maRowName[posLastCarac]);
      miForEnd = self.ciShiftPastRow;
    }
    //alterar do atual at� o shiftPast e atualizar o last para o shiftPast
    if(mbSelectRow){
      //seleciona as linhas entre a linha atual at� a shiftPast
      for(i=miForBegin; i <= miForEnd; i++){
        var auxRowName = self.csName+'_row_'+i;
        moRow = gebi(auxRowName);
        if(moRow){
          if(moRow.className != self.classRowSelected.value){
            moRow.className = self.classRowSelected.value;
            var id_name = auxRowName+';'+moRow.getAttribute('actionTarget');
            maRowsId.push(id_name);
          }
        }
      }
    }//end mbSelectRow
    else{
      for(i=miForBegin; i <= miForEnd; i++){
        var auxRowName = self.csName+'_row_'+i;
        moRow = gebi(auxRowName);
        if(moRow){
          moRow.className = moRow.getAttribute('classDefault');
          for (j=0; j < maRowsId.length; j++){
            if (maRowsId[j] == auxRowName +';'+ moRow.getAttribute('actionTarget')){
              maRowsId.splice(j,1);
               break;
            }
          }
        }
      }
    }//end else mbSelectedRow
    
    self.coInputHiddenRowId.value = objSerialize.serializeArray(maRowsId);

    var auxValue = new Array();
    while(maRowsId.length>=1)
      for (j=0; j < maRowsId.length; j++){
        var aux = maRowsId.splice(j,1);
           var aux2 = aux[0].split(';');
           auxValue.push(aux2[1]);
      }
    self.coInputHidden.value = objSerialize.serializeArray(auxValue);
    self.ciShiftPastRow = parseInt(maRowName[posLastCarac]);

    return true;
  }

 /*****************************************************************************
   * Atualiza as v�riaveis de controle das linhas da grid
   *
   * <p>Atualiza as vari�veis de controle das linhas selecionadas da grid. firstRow fica sempre
   *indicando a primeira linha com menor id das linhas selecionadas com um shift a linha atual caso
   *nao tenha sido utilizado um shift. lastRow indica a �ltima linha selecionada com um shift ou
   *a linha atual, caso n�o tenha sido utilizado um shift. pastRow informa linha "atual" da sele��o
   *de linhas anterior caso tenha sido utilizado um shift, ou a linha atual caso n�o tenha sido
   *utilizado um shift</p>
   *
   * @param String psRowId Id da linha que sofreu o evento onClick do mouse.
   * @param String piAction Indica se foi utilizada a tecla shift (1 => "n�o", 0=>"sim").
   *****************************************************************************/
  self.updateFirstLastRow = function(piRowIndex,piAction){
    var teste = '';
    var msRowsId = new Array();
    var maActualRowIndex = piRowIndex.split('_');
    var miActualRow = parseInt(maActualRowIndex[maActualRowIndex.length-1]);

    if((piAction==1)||(self.ciShiftFirstRow==0)){//a��o de um ctrl
      self.ciShiftLastRow = miActualRow;
      self.ciShiftFirstRow = miActualRow;
      self.ciShiftPastRow = miActualRow;
    }else{//a��o de um shift
      if(self.ciShiftFirstRow == miActualRow){
        self.ciShiftLastRow = miActualRow;
        self.ciShiftFirstRow = miActualRow;
      }else
        if(miActualRow < self.ciShiftFirstRow ){//first abaixo da linha atual
          self.ciShiftFirstRow = miActualRow;
        }else{ //first acima da linha atual , atualizar soh o last
          if(self.ciShiftPastRow>0){
            if(self.ciShiftPastRow==self.ciShiftLastRow){
              self.ciShiftLastRow=miActualRow;
            }else{
              if(self.ciShiftPastRow==self.ciShiftFirstRow){
                self.ciShiftFirstRow= miActualRow;
              }else
                alert('o shiftPast eh diferente do first e do last (grid.js function = updateFirstLastRow)');
            }
          }
        }  
     }
  }
  
  //func�o default para tratar da sele��o das linhas da grid
  /*****************************************************************************
   * Trata a sele��o "simples" e "multipla" das linhas da grid
   *
   * <p>Trata o evento onClick na linhas da grid selecinando multiplas linha da grid (uma a cada evento onclick, � claro)
   * alterando assim a classe CSS dessas linhas selecionadas. Tambem � utilizada para retirar a sele��o de uma linha previamente selecionada
   * da grid</p>
   * 
   * @param String psRowId Id da linha que sofreu o evento onClick do mouse.
   * @param String piAction Indica qual o tipo de sele��o de linhas da grid (1 => "simples", 2=>"multipla").
   * @param String psValue valor da coluna "selectColumn" da grid correspondente a a linha selecionada
   * @param Object evento de teclado para verificar se a tecla shift est� pressionada.
   *****************************************************************************/
  self.sel_r = function(psRowId, piAction,event)
  {
    self.coInputHiddenRowId=gebi(self.csName+'_value_row_id');
    self.coInputHidden=gebi(self.csName+'_value');
    var msValue = gebi(psRowId).getAttribute('actionTarget');
    if (piAction == 1)
      self.grid_select_single_row( psRowId, msValue);
    else{
      //sele��o multipla de linhas
      if(!event) event = window.event;
      if(event.shiftKey)
        self.updateFirstLastRow(psRowId,0);
      else
        self.updateFirstLastRow(psRowId,1);

      self.selectMultipleRows(psRowId,msValue);
      }

    }

  /*****************************************************************************
   * Trata o evento onKeyPress no TextBox da barra  de p�gina��o da grid
   *
   * <p>Trata o evento onKeyPress no TextBox da barra de pagina��o da grid, utilizado para a altera��o da p�gina
   * atual da grid, quanto � pressionada a tecla <enter> a fun��o que troca a p�gina atual da grid � disparada,
   * e ap�s , o evento ajax para popular o corpo da grid � chamado</p>
   * 
   * @param Object event Evento do teclado.
   *****************************************************************************/
  self.checkKeyPressTextPageButton = function(event)
  {
    if(!event) event = window.event;
    
    if(event.keyCode==13)
        return self.showGlass() + trigger_event(self.csName+"changepagechange","3");  
    else
    {
    	if(event.keyCode < 48 || event.keyCode > 57)
    		return false;    
    }    
    
    return true;
  }
  
  /********************************WARNING**************************************
   *
   * ATRIBUTO E M�TODOS UTILIZADOS PARA FAZER COM QUE QUANDO UM REFRESH DA GRID
   * FOR DISPARADO, A P�GINA ATUAL N�O SEJA SETADA PARA 1. CUIDADO AO UTILIZAR
   * ESSAS FUN��ES! POGS S�O NECESS�RIOS NA TELA QUE UTILIZAR� ESSAS FUN��ES!
   *
   *****************************************************************************/  
  
  /*****************************************************************************
   * Utilizado para atualizar o conte�do da grid.
   *
   * <p>M�todo utilizado para atualizar o conte�do da grid.</p>
   *
   *****************************************************************************/
  self.js_grid_update_content = function()
  {
  	self.must_update_content = 1;
  }
  
  /*****************************************************************************
   * Utilizado para n�o atualizar o conte�do da grid.
   *
   * <p>M�todo utilizado para n�o atualizar o conte�do da grid.</p>
   *
   *****************************************************************************/
  self.js_grid_dont_update_content = function()
  {
  	self.must_update_content = 0;
  }
  
  /*****************************************************************************
   * Utilizado para setar a p�gina corrente da grid.
   *
   * <p>M�todo utilizado para setar a p�gina corrente da grid.</p>
   *
   * @param Integer piPageIndex N�mero da p�gina
   *****************************************************************************/
  self.js_set_grid_current_page = function(piPageIndex)  
  {
     self.caInfoPages[0] = parseInt(piPageIndex);
		 gebi(self.csName+'_actual_page').value = parseInt(piPageIndex);		 
     if(self.coInputPagesInfo) self.coInputPagesInfo.value = self.caInfoPages.join(':');
  }
  
  /*****************************************************************************
   * Utilizado para retornar a p�gina corrente da grid.
   *
   * <p>M�todo utilizado para retornar a p�gina corrente da grid.</p>
   *
   *****************************************************************************/
  self.js_get_grid_current_page = function()  
  {
		return self.coTextBoxActualPage ? self.coTextBoxActualPage.value : 1;
  }
  
  /********************************END WARNING**********************************
   *****************************************************************************/
  
   /*****************************************************************************
   * Utilizada para "popular" a grid
   *
   * <p>Trata o evento de preencher com o c�gigo HTML a grid , incluindo os bot�es de pagina��o , o corpo da grid,
   * os c�digos dos menus de contexto da grid</p>
   * 
   * @param String paPageButtom C�digo html da barra dos bot�es de pagina��o da grid.
   * @param String paCodeBody C�digo html do corpo da grid
   * @param String paCodeMenu C�digo html dos menus de contexto que precisam ser renderizados dinamicamente
   * @param String paMenuJavascript c�digo javascript do corpo de cada um dos menus de contexto
   * @param String paMenuGebi C�gigo javascrip de associa��o de cada c�lula da grid com seu respectivo menu de contexto
   *****************************************************************************/
  self.js_populate_grid = function(paPageButtom, paCodeBody, paCodeMenu, paMenuJavascript, paMenuGebi, paCodeButtonEdit)  
  {
       var miTitleHeight = 0;

       if((self.coDivPageButton)&&(paPageButtom.length>10)){
         if(browser.isIE){
           self.coDivPageButton.style.height = parseInt(self.ciColumnsHeight.value);
           self.coDivPageButton.style.top = miTitleHeight -1;
         }else{
           self.coDivPageButton.style.height = parseInt(self.ciColumnsHeight.value);
           self.coDivPageButton.style.top = miTitleHeight -1;
         }
       }
       if(self.coDivColumns){
         if(browser.isIE){
        self.coDivColumns.style.height = parseInt(self.coDivColumns.style.height);           
           self.coDivColumns.style.top = parseInt(miTitleHeight + parseInt(self.ciColumnsHeight.value)-2);
         }else
           self.coDivColumns.style.top = parseInt(miTitleHeight + parseInt(self.ciColumnsHeight.value));
       }
       if(self.buttonRefresh)
      if(browser.isIE)           
        self.buttonRefresh.style.top = parseInt(miTitleHeight + parseInt(self.ciColumnsHeight.value))-2;
      else
        self.buttonRefresh.style.top = parseInt(miTitleHeight + parseInt(self.ciColumnsHeight.value));

    if(self.coDivPageButton)
      if(paPageButtom.length>10){
        self.coDivPageButton.style.display = 'block';
        self.coDivPageButton.innerHTML = paPageButtom;
      }
    //---------------------------body code-----------------------------------------------
    if(browser.isIE){
        self.coDivBody.style.top = parseInt(self.coDivColumns.style.top) + parseInt(self.coDivColumns.style.height)-1;
      self.coDivBody.style.height = parseInt(self.coThis.style.height) - (parseInt(self.coDivColumns.style.top) + parseInt(self.coDivColumns.style.height)); 
    }else{
       self.coDivBody.style.top = parseInt(self.coDivColumns.style.top) + parseInt(self.coDivColumns.style.height)+1;
      self.coDivBody.style.height = parseInt(self.coThis.style.height) - (parseInt(self.coDivColumns.style.top) + parseInt(self.coDivColumns.style.height)+2); 
    }
    if((paCodeBody.length>10) && self.must_update_content) 
      self.coDivBody.innerHTML = paCodeBody;
    //--------------------------menu code including menu ACL-------------------------------------------------
    if(paCodeMenu.length>10)
      self.divMenuACL.innerHTML = paCodeMenu; 
    //-------------------------eval dos c�digos javascript dos menus-----------------------------------------
    if(paMenuJavascript.length>10)  
      eval(paMenuJavascript);
    //-------------------------eval dos gebi dos menus de contexto-------------------------------------------  
    if(paMenuGebi.length>10)  
      eval(paMenuGebi);
    
    if(paCodeButtonEdit!=undefined)
    if(paCodeButtonEdit.length>10){
      self.coDivButtonEdit.style.display='block';
      self.coDivButtonEdit.innerHTML = paCodeButtonEdit;  
    }
    
    
    return true;  
  }

   /*****************************************************************************
   * Utilizada para "popular" a grid via refresh
   *
   * <p>Trata o evento de preencher com o c�gigo HTML a grid , incluindo os bot�es de pagina��o , o corpo da grid,
   * os c�digos dos menus de contexto da grid</p>
   * 
   * @param String paPageButtom C�digo html da barra dos bot�es de pagina��o da grid.
   * @param String paCodeBody C�digo html do corpo da grid
   * @param String paCodeMenu C�digo html dos menus de contexto que precisam ser renderizados dinamicamente
   * @param String paMenuJavascript c�digo javascript do corpo de cada um dos menus de contexto
   * @param String paMenuGebi C�gigo javascrip de associa��o de cada c�lula da grid com seu respectivo menu de contexto
   *****************************************************************************/
  self.js_populate_refresh = function(paPageButtom, paCodeBody, paCodeMenu, paMenuJavascript, paMenuGebi)  
  {
    if(self.coDivPageButton)
      if(paPageButtom.length>10){
        self.coDivPageButton.style.display = 'block';
        self.coDivPageButton.innerHTML = paPageButtom;
      }
    if(paCodeBody.length>10)
      self.coDivBody.innerHTML = paCodeBody;
    //--------------------------menu code including menu ACL-------------------------------------------------
    if(paCodeMenu.length>10)
      self.divMenuACL.innerHTML = paCodeMenu; 
    //-------------------------eval dos c�digos javascript dos menus-----------------------------------------
    if(paMenuJavascript.length>10)  
      eval(paMenuJavascript);
    //-------------------------eval dos gebi dos menus de contexto-------------------------------------------  
    if(paMenuGebi.length>10)  
      eval(paMenuGebi);
      
    return true;  
  }

   /*****************************************************************************
   * Utilizada para "popular" a grid nos eventos changepage[next, first, back, last, change] e refresh
   *
   * <p>Trata o evento de preencher com o c�gigo HTML o corpo da grid e desenhar os menus de contexto</p>
   * 
   * @param String paCodeBody C�digo html do corpo da grid
   * @param String paCodeMenu C�digo html dos menus de contexto que precisam ser renderizados dinamicamente
   * @param String paMenuJavascript c�digo javascript do corpo de cada um dos menus de contexto
   * @param String paMenuGebi C�gigo javascrip de associa��o de cada c�lula da grid com seu respectivo menu de contexto
   *****************************************************************************/
  self.js_populate_grid_change = function(paCodeBody, paCodeMenu, paMenuJavascript, paMenuGebi)  
  {
    if(paCodeBody.length>10)
      self.coDivBody.innerHTML = paCodeBody;
    //--------------------------menu code including menu ACL-------------------------------------------------
    if(paCodeMenu.length>10)
      self.divMenuACL.innerHTML = paCodeMenu; 
    //-------------------------eval dos c�digos javascript dos menus-----------------------------------------
    if((paMenuJavascript.length>10)&&(paMenuJavascript.value!='::'))  
      eval(paMenuJavascript);
    //-------------------------eval dos gebi dos menus de contexto-------------------------------------------  
    if(paMenuGebi.length>10)  
      eval(paMenuGebi);
      
    return true;  
  }

   /*****************************************************************************
   * Utilizada para "popular" o header das colunas na ordena��o das colunas da grid
   *
   * <p>Trata o evento de preencher com o c�gigo HTML o header das colunas da grid</p>
   * 
   * @param String paCodeHeaderColumn C�digo html do header das colunas da grid
   *****************************************************************************/
   self.js_populate_grid_order_column = function(paCodeHeaderColumn){
    if(paCodeHeaderColumn.length>10)
      self.coDivColumns.innerHTML = paCodeHeaderColumn;
    return true;
   }
   
   /*****************************************************************************
   * Utilizada para Inicializar varias vari�veis da grid
   *
   * <p>Inicializa as vari�veis da grid para a grid perder a 'mem�ria' dependendo dos eventos
   *que foram acionados</p>
   *****************************************************************************/
   self.InitSetup = function(){
    self.ciShiftLastRow = 0;
    self.ciShiftFirstRow = 0;
    self.ciShiftPastRow =  0;
    self.cs_grid_selected_row = "";
    self.cs_grid_selected_row = null;
         if(self.coInputHidden)
       self.coInputHidden.value = null;
      if(self.coInputHiddenRowId)    
      self.coInputHiddenRowId.value = null;
   }
   /*****************************************************************************
   * Utilizada para disparar as fun��es dos eventos da grid din�mica.
   *
   * <p>Trata de disparar o evento apropriado dependendo do primeiro par�metro passado em psContent</p>
   * 
   * @param String psContent Concatena��o de c�digos utilizados nos eventos da grid
   *****************************************************************************/
   self.js_grid_event = function(psContent)
   {
    //soTabManager.lockTabs();
    var objSerialize = new fwdObjSerialize();
    var maCode = objSerialize.unserializeString(psContent);
    var length = maCode.length;
    maCode[1] = maCode[1]+ " ";
    maCode[2] = maCode[2] + " ";
    maCode[3] = maCode[3] + " ";
    maCode[4] = maCode[4] + " ";
    maAuxCode = maCode[0].split(' ');
    maCode[0] = maAuxCode.join('');
    
    self.coInputHiddenRowId = gebi(self.csName+'_value_row_id');
    self.coInputHidden = gebi(self.csName+'_value');
    
    //desloca a barra de rolagem do corpo da grid para cima caso o evento de refresh da grid 
    //seja do tipo refresh voltando para a primeira p�gina
    if(self.refreshOnCurrentPage!=1){
       self.coDivBody.scrollTop=0;
    }
    
    switch (maCode[0])
    {
       case "populategrid":
           maCode[5] = maCode[5] + " ";
           maCode[6] = maCode[6] + " ";
           self.InitSetup();
           self.js_populate_grid(maCode[1],maCode[2],maCode[3],maCode[4],maCode[5],maCode[6]);
           if(maCode[7])
             self.js_populate_grid_order_column(maCode[7]);
             break;      
       case "refresh":
             maCode[5] = maCode[5] + " ";        
           self.InitSetup();
           self.js_populate_refresh(maCode[1],maCode[2],maCode[3],maCode[4],maCode[5]);
             break;
       case 'user_edit':
               maCode[5] = maCode[5] + " ";
               self.InitSetup();
               self.js_populate_grid_order_column(maCode[3]);        
             self.js_populate_grid(maCode[1],maCode[2],maCode[4],maCode[5],maCode[6]);  
          break;       
       case "first":
       case "back":
       case "last":
       case "next":
        case "change":       
              self.js_populate_grid_changepage(maCode[5]);
           self.js_populate_grid_change(maCode[1],maCode[2],maCode[3],maCode[4]);  
             break;
       case "populateordercolumn":
            self.InitSetup();
            maCode[5] = maCode[5] + " ";
            maCode[6] = maCode[6] + " ";        
            self.js_populate_grid_order_column(maCode[2]);
            self.js_populate_grid_changepage(maCode[6]);
              self.js_populate_grid_change(maCode[1],maCode[3],maCode[4],maCode[5]);  
          break;      
       default:
           alert("js_grid_event switch(default) -> value:"+maCode[0]+": ! ");
         break;      
       }

       window.setTimeout('tt_Init(true,"'+self.csName+'")',100);
       self.coGlass.hide();
       return true;
   }
  
  
   /*****************************************************************************
   * Utilizado para trocar a p�gina atual da grid
   *
   * <p>Trata o evento de trocar a p�gina atual da grid </p>
   * 
   * @param String psInfoPages string que cont�m as informa��es de p�gina atual, ultima p�gina e 
   * n�mero total de linhas da grid.
   *****************************************************************************/
  self.js_populate_grid_changepage = function(psInfoPages)  
  {
    self.coStaticTotalPages = gebi(self.csName+'_de');
       self.coTextBoxActualPage = gebi(self.csName+'_actual_page');
       self.coInputPagesInfo = gebi(self.csName+'_gridpageinfo');
    self.caInfoPages = psInfoPages.split(':');
    if( self.coTextBoxActualPage)
      self.coTextBoxActualPage.value = parseInt(self.caInfoPages[0]); 
    if(self.coStaticTotalPages)
      self.coStaticTotalPages.value = "de "+ parseInt(self.caInfoPages[1]); 
    if(self.coInputPagesInfo)
      self.coInputPagesInfo.value =  self.caInfoPages.join(':');
    return true;  
  }
  
  //salva o valor da coluna selecionada da grid no input hidden do menu    
  /*****************************************************************************
   * Salva o valor da coluna selecionada da grid no input hidden do menu
   *
   * <p>Salva o valor da coluna selecionada da grid no input hidden do menu, atualmente essa
   *fun��o s� funciona para a grid, pois � o �nico objeto da FWD que possui o atributo "actionTarget"
   *e porque o gebi est� sendo feito da seguinte forma :(gebi(moEventTarget.parentNode.parentNode.id))</p>
   * 
   * @param string id Identificador do menu
   * @param string event Evento de mouse disparado quando foi exibido o menu de contexto
   *****************************************************************************/
  self.js_action_target_menu = function(id,event){
    if(!event) event = window.event;
    var moEventTarget = new Object();
    if(browser.isIE) moEventTarget = window.event.srcElement;
    if(browser.isNS) moEventTarget = event.target;
    var msActionTarget='';
    var moObjAux = moEventTarget;
    var moInputMenu = gebi(id +'_action_target');
    if(moInputMenu)
    while((msActionTarget=='')&&(moObjAux.parentNode.id!='dialog')){
      moObjAux = moObjAux.parentNode;
    if((moObjAux.id!='')){
         if(moObjAux.getAttribute('actionTarget')){
            msActionTarget = moObjAux.getAttribute('actionTarget');
              moInputMenu.value = msActionTarget;
        FWDMenu.value = moInputMenu.value;
      }
    }
    }  
  }
  
 /**
  * Seta o valor do atributo selectionrequired
  * 
  * <p>Seta o valor do atributo selectionrequired.</p>
  * @param boolean pbValue Novo valor do atributo
  */
  self.setSelectionRequired = function(pbValue){
    selectionRequired = pbValue;
  }
  
 /**
  * Testa se existe alguma linha selecionada.
  * 
  * <p>Testa se existe alguma linha selecionada.</p>
  * @return boolean True sse n�o existe uma linha selecionada
  */
  self.selectionIsEmpty = function(){
	if(!self.coInputHidden)
		return true;
	
	if(self.coInputHidden.value == null || self.coInputHidden.value == '' || self.coInputHidden.value == 'null' || self.coInputHidden.value==='a:0:{}')
		return true;
	
	return false;
  }
  
 /*****************************************************************************
  * Indica se o elemento est� satisfatoriamente preenchido.
  *
  * <p>Indica se o elemento est� satisfatoriamente preenchido
  * (um dos m�todos necess�rios para customizar o required check).</p>
  * @return boolean True sse est� satisfatoriamente preenchido
  *****************************************************************************/
  self.isFilled = function(){
  var miActualFirstRow = 1;
  if(self.coInputPagesInfo){
    var maAuxPages = self.coInputPagesInfo.value.split(':');
      miActualFirstRow = (parseInt(maAuxPages[0]) -1 )* parseInt(self.ciRowsPerPage) + 1;
      maAuxPages = null;
  }

    if(selectionRequired){
      return !self.selectionIsEmpty();
    }else{
      return gebi(self.csName+'_row_'+miActualFirstRow)?true:false;
    }
  }
  
 /*****************************************************************************
  * M�todo executado quando o elemento est� preenchido satisfatoriamente.
  *
  * <p>M�todo executado quando o elemento est� preenchido satisfatoriamente.
  * (um dos m�todos necess�rios para customizar o required check).</p>
  *****************************************************************************/
  self.onRequiredCheckOk = function(){
    var moLabel = gebi('label_'+self.csName);
    if(moLabel){
      moLabel.className = 'FWDStatic';
    }
    moLabel = null;
  }
  
 /*****************************************************************************
  * M�todo executado quando o elemento n�o est� preenchido satisfatoriamente.
  *
  * <p>M�todo executado quando o elemento n�o est� preenchido satisfatoriamente.
  * (um dos m�todos necess�rios para customizar o required check).</p>
  *****************************************************************************/
  self.onRequiredCheckNotOk = function(){
    var moLabel = gebi('label_'+self.csName);
    if(moLabel){
      var msClassName = moLabel.className;
      if(msClassName.substr(msClassName.length-6)!='_Label'){
        moLabel.className = moLabel.className+'_Label';
      }
    }
    moLabel = msClassName = null;
  }
  
 /*****************************************************************************
  * Retorna os valores de todas linhas da p�gina atual.
  *
  * <p>Retorna os valores de todas linhas da p�gina atual.</p>
  *
  * @return Array Array com os valores
  *****************************************************************************/
  self.getAllIds = function(){
     var miActualFirstRow = 1;
  if(self.coInputPagesInfo){
    var maAuxPages = self.coInputPagesInfo.value.split(':');
      miActualFirstRow = (parseInt(maAuxPages[0]) -1 )* parseInt(self.ciRowsPerPage) + 1;
      maAuxPages = null;
  }
    var moRow,maIds = [];
    while(moRow = gebi(self.csName+'_row_'+miActualFirstRow)){
      maIds.push(moRow.getAttribute('actionTarget'));
      miActualFirstRow++;
    }
    return maIds;
  }
  
 /*****************************************************************************
  * Retorna o(s) valor(es) selecionado(s) da grid.
  *
  * <p>Retorna o(s) valor(es) selecionado(s) da grid. Se a sele��o for m�ltipla,
  * retorna um array, se for simples, retorna uma string ou null, caso n�o haja
  * linhas selecionadas.</p>
  *
  * @return mixed Valor(es) selecionado(s)
  *****************************************************************************/
  self.getValue = function(){
    var objSerialize,moGridValue;
    moGridValue = gebi(self.csName+'_value');
    if((!moGridValue)||(!moGridValue.value)||(moGridValue.value==null)||(moGridValue.value=='null')){
      if(self.cbIsMultiple){
        return [];
      }else{
        return null;
      }
    }else{
      objSerialize = new fwdObjSerialize();
      if(self.cbIsMultiple){
        return objSerialize.unserializeString(moGridValue.value);
      }else{
        return objSerialize.unserializeString(moGridValue.value)[0];
      }
    }
  }
  
 /*******************************************************************************
  * Dispara o evento de refresh da grid.
  * 
  * <p>Dispara o refresh da grid.</p>
  *******************************************************************************/
  self.refresh = function(){
    self.showGlass();
    trigger_event(self.csName+'refresh',3);
  }
 /*******************************************************************************
  * Seta o populate da grid.
  * 
  * <p>seta o populateXML da grid.</p>
  * @param boolean pbPopulate Novo valor do populate da grid e ativa o vidro da grid se o populate for true
  *******************************************************************************/
  self.setPopulate = function(pbPopulate){
     if(pbPopulate){
       gebi(self.csName+'_populatexml').value = 1;
       self.showGlass();
     }else{
       gebi(self.csName+'_populatexml').value = 2;
     }

  }
}//end classe

/*****************************************************************************
 * Retorna o(s) valor(es) selecionado(s) da grid. [DEPRECATED]
 *
 * <p>Retorna o(s) valor(es) selecionado(s) da grid. Se a sele��o for m�ltipla,
 * retorna um array, se for simples, retorna uma string. Se n�o houver nenhuma
 * linha selecionada, retorna null.
 * [DEPRECATED]: Use o m�todo getValue(); diretamente.</p>
 *
 * @param String psId Id da grid.
 * @return mixed Valor(es) selecionado(s)
 *****************************************************************************/
function js_get_grid_value(psId){
  return gobi(psId).getValue();
}

/*****************************************************************************
* fun��o que abstrai a biblioteca de unserialize e de serialize
*
* <p>fun��o que abstrai a biblioteca de unserialize e de serialize utiliza
*da pela grid, tem v�rias maneiras de obter uma string serializade e de
*descerializar uma string</p>
*
* @param String psId Id da grid.
*****************************************************************************/
function fwdObjSerialize()
{
  this.objects = new Array();
  /*****************************************************************************
   * limpa o vetor de objetos do serialize
   *
   * <p>limpa o vetor de objetos do serialize</p>
   * 
   *****************************************************************************/
  this.clean = function(){
      this.objects = new Array;
   }
  /* adiciona um array de valores ao objeto de serializa��o
   *
   * <p>adiciona um array de valores ao objeto de serializa��o</p>
   *
   * @param Array maValues array de valores.
   *****************************************************************************/
   this.addArray = function(maValues){
      this.clean();
      var length = maValues.length;
      for(var i=0;i<length;i++)
      this.add(maValues[i]);
   }
  /* adiciona v�rios valores ao objeto de serializa��o
   *
   * <p>adiciona v�rios valores ao objeto de serializa��o (os valores devem estar separados por ':')</p>
   *
   * @param String psAllValues valores separados por ':'.
   * @return String a string serializada dos valores passados por par�metro (valores estes, separados por ':')
   *****************************************************************************/
   this.addAll = function(psAllValues){
      this.clean();
      var auxValue = psAllValues.split(':');
      var length = auxValue.length;
      for(var i=0;i<length;i++)
      this.add(auxValue[i]);
   }
  /* retorna a string serializada da string passada por par�metro
   *
   * <p>adiciona um string ao objeto de serializa��o e retorna esta string serializada</p>
   *
   * @param String psString valor a ser serializado.
   * @return String a string serializada da string passada por par�metro
   *****************************************************************************/
   this.serializeByString = function(psString){
       this.clean();
       this.add(psString);
       return this.getValue();  
   }
  /* adiciona um valor ao objeto de serializa��o
   *
   * <p>adiciona um valor ao objeto de serializa��o</p>
   *
   * @param String value valor a ser adicionado no objeto de serializa��o.
   *****************************************************************************/
   this.add = function(value){
      obj = new Object();
      obj.value = value;
      obj.toString = function(){
        var str = '';
        var attribute = String(this.value);
        str+= 's:'+attribute.length+':"'+attribute+'";';
        return str;
      }
      this.objects.push(obj);
   }
  /* seta uma string serializada no objeto de serializa��o
   *
   * <p>seta uma string serializada no objeto de serializa��o</p>
   *
   * @param String psValue string serializada a ser adicionada no objeto de serializa��o.
   *****************************************************************************/
   this.setValue = function(psValue){
     this.value = psValue;
   }  
  /* serializa o array passado por par�metro
   *
   * <p>serializa o array passado por par�metro e retorna a string serializada dos elementos do array</p>
   *
   * @param Array maValues array de valores a ser serializado.
   * @return String string seralizada dos valores do array
   *****************************************************************************/
   this.serializeArray = function(maValues){
      this.clean();
      this.addArray(maValues);
      return this.getValue();
   }
  /* Retorna o array da string descerializada
   *
   * <p>descerializa a string e retorna o array da string descerializada</p>
   *
   * @return Array array dos valores da string descerializada
   *****************************************************************************/
   this.getArray = function(){
   var maArray = new Array();
   var msValue = this.getValue();
   maArray = soUnSerializer.unserialize(msValue);
   return maArray;
   }  
  /* Retorna o array da string (passada por parametro) descerializada
   *
   * <p>descerializa a string (passada por par�metro) e retorna o array da string descerializada</p>
   *
   * @param String psValues string com valores separados por ':'.
   * @return Array array dos valores da string descerializada
   *****************************************************************************/
  this.unserializeString = function(psValues){
  //
  var maArray = new Array();
  maArray = soUnSerializer.unserialize(psValues);
  return maArray;
  }
  /* Retorna a string srializada referente aos valores contidos no objeto de serializa��o
   *
   * <p>Retorna a string srializada referente aos valores contidos no objeto de serializa��o</p>
   *
   * @return String string serializada referente aos valores contidos no objeto de serializa��o
   *****************************************************************************/
  this.getValue = function(){
    var length = this.objects.length;
    var str = 'a:'+length+':{';
    for(var i=0;i<length;i++)
      str+= 'i:'+i+';'+ this.objects[i];
    str+= '}';
  return str;
  }
}
  /* utilizado pelo firefox para desabilitar a sele��o de texto na grid
   *
   * <p>utilizado pelo firefox para desabilitar a sele��o de texto na grid</p>
   *
   * @return Boolean false
   *****************************************************************************/
  function disableselect(){
    return false;
  }

  /* utilizado pelo firefox para realibiltar a sele��o de texto na grid
   *
   * <p>utilizado pelo firefox para realibiltar a sele��o de texto na grid</p>
   *
   * @return Boolean true
   *****************************************************************************/
  function reEnable(){
    return true;
  }

/*******************************************************************************
 * Classe FWDGridPreferences.
 * 
 * <p>Classe que trada dos eventos e da manipula��o de dados da tela de preferencias da grid</p>
 * @package FWD
 *******************************************************************************/
function FWDGridPreferences(){
  // Se estiver dentro de uma tab, retorna a inst�ncia do pai
  var moParent = window;
  var moChild = null;
  while(moChild!=moParent){
    moChild = moParent;
    moParent = moParent.parent;
  }

  if(moParent.objManagerGrifPref) return moParent.objManagerGrifPref;

    var self = this;  
  /*******************************************************************************
   * Inicializa o objeto.
   * 
   * <p>Inicializa o objeto com todas as referencias que ser�o necess�rias para a manipula��o dos dados da popup de preferencias</p>
   * @param string psGridiName - nome da grid que esta sendo editada
   *******************************************************************************/
  self.init = function(psGridName){

    
    /** @var String Nome da grid. */
    self.gridName = psGridName;
    /** @var Object Variable que cont�m as finforma��es de ordenamento dos dados da grid*/
    self.coOrderColumnsData = gebi(self.gridName+'_sel_order_column_data');
    /** @var Object Select das colunas que ser�o exibidas. */
    self.coOrderColumn = gebi(self.gridName+'_sel_col_show');
    /** @var Object InputHidden que guarda as configura��es das colunas da grid. */
    self.coColumnsProp = gebi(self.gridName+'_columns_prop_var');
    /** @var Object Text guarda o valor do n�mero de linhas por p�gina. */
    self.ciRowsPerPage = gebi(self.gridName+'_popupRowsPage');
    /** @var Object Text guarda o valor da altura das linhas da grid. */
    self.ciRowsHeight = gebi(self.gridName+'_popupRowsHeight');
    /** @var Object Select das colunas do source do tooltip.*/
    self.coTooltipSource = gebi(self.gridName+'_sel_tt');
    /** @var Object Select das colunas do target do tooltip.*/
    self.coTooltipTarget = gebi(self.gridName+'_sel_tt_target');
    /** @var Object Select das colunas do panel de propriedade das colunas.*/
    self.coSelPropCol = gebi(self.gridName+'_sel_col_prop');
    /** @var Object Text da largura das colunas.*/
    self.coTextPropColWidth = gebi(self.gridName+'_popupColWidth');
    /** @var Object Select do atributo nowrap das colunas.*/
    self.coSelPropNoWrap = gebi(self.gridName+'_sel_no_wrap');
    /** @var Integer id da ultima coluna selecionada.*/
    self.ciLastColSelProp;
    /** @var Array propriedade das colunas indexado pelo id da coluna.*/
    self.maColProp = {};
    maAuxColProp = self.coColumnsProp.value.split(';');
    var length = maAuxColProp.length;
        for(var i=0;i<length;i++){
        maColInst = maAuxColProp[i].split(':');
        self.maColProp[maColInst[0]] = new Array(maColInst[0],maColInst[1],maColInst[2]);
    }
    //select com a order de ordenamento das colunas da grid
    /** @var Object Select com a ordem de ordenamento das colunas da grid.*/
    self.coSelColOrder = gebi(self.gridName+'_sel_col_order');
    /** @var Object Select com as colunas que n�o est�o ordenando a grid.*/
    self.coSelColNotOrder = gebi(self.gridName+'_sel_col_notorder');
    /** @var Object Select que indica qual a ordem de ordenamento da coluna.*/
    self.colSelColOrderBy = gebi(self.gridName+'_sel_order_column_by');
    /** @var Integer indica a ultima coluna selecionada do select de ordenamento das colunas da grid*/
    self.ciLastOrderCol;
    /** @var Static st do select do tipo de ordenamento*/
    self.coStColOrderBy = gebi(self.gridName+'_st_order_by');
    /** @var Array vetor que cont�m as informa��es de ordenamento dos dados das colunas da grid .*/
    self.maColOrder = new Array();
    maAuxOrderProp = self.coOrderColumnsData.value.split(';');
    var length = maAuxOrderProp.length;
        for(var i=0;i<length;i++){
        maColOrderAux = maAuxOrderProp[i].split(':');
        self.maColOrder[maColOrderAux[0]] = new Array(maColOrderAux[0],maColOrderAux[1]);
    }
    /** @var Inputhidden que cont�m a informa��o de quais colunas exibidas podem ser editadas .*/
    moColEdit = gebi(self.gridName + '_var_col_editable');
    var maColEditAux = moColEdit.value.split(';');
    /** @var Array que cont�m os ids das colunas com as respectivas informa��es de edit.*/
    self.maColEdit = new Array();
    var length = maColEditAux.length;
        for(var i=0;i<length;i++){
        var maEditAux = maColEditAux[i].split(':');
        self.maColEdit[maEditAux[0]] = maEditAux[1];
    }
    /** @var ViewButton de mover pra direita do panel de exibi��o das colunas.*/
    self.btRightShow = gebi(self.gridName+'btRightShow');
    /** @var ViewButton de mover pra esquerda do panel de exibi��o das colunas.*/
    self.btLeftShow = gebi(self.gridName+'btLeftShow');
    /** popula o select das propriedades das colunas*/
    var lengthShow = self.coOrderColumn.length;
    for(var j=0;j<lengthShow;j++){
      if(self.maColEdit[self.coOrderColumn.options[j].value]==1)
        self.coSelPropCol.options[self.coSelPropCol.length] = new Option(self.coOrderColumn.options[j].text,self.coOrderColumn.options[j].value,false,false);
    }
  }
  /*******************************************************************************
   * Fun��o executada antes da fun��o de inser��o da coluna no select das colunas que ordenam a grid
   * 
   * <p>utilizada para adicionar a coluna no vetor de colunas que ordenar�o a grid e para mostrar o select de sele��o da ordem da coluna</p>
   *******************************************************************************/
  self.orderColClickAdd = function(){
    if(self.coSelColNotOrder.selectedIndex>=0){
      self.maColOrder[self.coSelColNotOrder.value] = new Array(self.coSelColNotOrder.value,'+');
      self.propOrderClickBeforeSave();
      self.ciLastOrderCol = self.coSelColNotOrder.value;
      self.colSelColOrderBy.value='+';
      self.coStColOrderBy.style.display='block'; 
      self.colSelColOrderBy.style.display='block';
    } 
  }
  /*******************************************************************************
   * Fun��o executada antes da fun��o de remo��o da coluna que ordenaria a grid do select das colunas q ordenam a grid.
   * 
   * <p>utilizada para remover do vetor a coluna e para esconder o select de sele��o da ordem da coluna</p>
   *******************************************************************************/
  self.orderColClickRemove = function(){
    if(self.coSelColOrder.selectedIndex>=0){
      self.maColOrder[self.coSelColOrder.value] = null;
      self.ciLastOrderCol = undefined;
      self.coStColOrderBy.style.display='none'; 
      self.colSelColOrderBy.style.display='none';
    } 
  }
  /*******************************************************************************
   * executada no onclick do panel de colunas que ordenam a grid
   * 
   * <p>salva o valor de ordenamento da coluna anteior e seta o valor do ordenamento da coluna atualmente selecionada</p>
   *******************************************************************************/
  self.orderColClick = function(){
    if(self.coSelColOrder.selectedIndex>=0){
      self.coStColOrderBy.style.display='block'; 
      self.colSelColOrderBy.style.display='block'; 
      if(!((self.ciLastOrderCol == undefined)||(self.ciLastOrderCol == '')|| (self.ciLastOrderCol == null) )){
        self.maColOrder[self.ciLastOrderCol][0] = self.ciLastOrderCol;
        self.maColOrder[self.ciLastOrderCol][1] = self.colSelColOrderBy.value;
      }
      self.ciLastOrderCol = self.coSelColOrder.value;
      self.coSelColOrder.value = self.maColOrder[self.coSelColOrder.value][0];
      self.colSelColOrderBy.value = self.maColOrder[self.coSelColOrder.value][1];
    }
  }  
  /*******************************************************************************
   * executada no evento da salvar as preferencias do usu�rio
   * 
   * <p>salva o valor de ordenamento da ultima coluna selecionda no panel das colunas q ordenar�o a grid</p>
   *******************************************************************************/
  self.propOrderClickBeforeSave = function(){
    if((self.ciLastOrderCol != undefined)&&(self.ciLastOrderCol != '')&&(self.ciLastOrderCol != null)){
      self.maColOrder[self.ciLastOrderCol][0] = self.coSelColOrder.value;
      self.maColOrder[self.ciLastOrderCol][1] = self.colSelColOrderBy.value;
    }
  }
  /*******************************************************************************
   * executada no evento da salvar as preferencias do usu�rio
   * 
   * <p>salva o valor das propriedades ultima coluna editada no panel de edi��o das propriedades das colunas</p>
   *******************************************************************************/
  self.propColClickBeforeSave = function(){
    if(!( (self.ciLastColSelProp == undefined) || (self.ciLastColSelProp == '') || (self.ciLastColSelProp == null) )){
      if(self.coSelPropNoWrap.value=='')
        self.coSelPropNoWrap.value = true;
      if(((parseInt(self.coTextPropColWidth.value)>0) && (parseInt(self.coTextPropColWidth.value)<10) ) || (self.coTextPropColWidth.value=='') )
        self.coTextPropColWidth.value = 10;
      self.maColProp[self.ciLastColSelProp][1] = self.coTextPropColWidth.value;
      self.maColProp[self.ciLastColSelProp][2] = self.coSelPropNoWrap.value;
    }
  }
  /*******************************************************************************
   * executada no onclick do panel das propriedades das colunas
   * 
   * <p>salva o valores das propriedades da coluna anteior e seta os valores das propriedades da coluna atualmente selecionada</p>
   *******************************************************************************/
  self.propColClick = function(){
    if(!( (self.ciLastColSelProp == undefined) || (self.ciLastColSelProp == '') || (self.ciLastColSelProp == null) )){
      if(self.coSelPropNoWrap.value=='')
        self.coSelPropNoWrap.value = true;
      if(((parseInt(self.coTextPropColWidth.value)>0) && (parseInt(self.coTextPropColWidth.value)<10) ) || (self.coTextPropColWidth.value=='') )
        self.coTextPropColWidth.value = 10;
      self.maColProp[self.ciLastColSelProp][1] = self.coTextPropColWidth.value;
      self.maColProp[self.ciLastColSelProp][2] = self.coSelPropNoWrap.value;
    }
    var miSelValue = self.coSelPropCol.value;
    if(miSelValue != ''){
      self.ciLastColSelProp = miSelValue;
      self.coTextPropColWidth.value = self.maColProp[miSelValue][1];

      self.coSelPropNoWrap.value = self.maColProp[miSelValue][2];
    }
    else{
      self.ciLastColSelProp = undefined;
      self.coTextPropColWidth.value = '';
      self.coSelPropNoWrap.value = '';
    }
  }  
  /*******************************************************************************
   * executada no onclick dos botoes de up
   * 
   * <p>move o �tem do select uma posi��o para cima</p>
   * @param string psSelectedId nome do select alvo do evento
   *******************************************************************************/
  self.moveUp = function(psSelectId){
    var moSel = gebi(psSelectId);
    if(moSel.selectedIndex > 0){
      var i = moSel.selectedIndex;
      var msAuxKey = moSel.options[i-1].value;
      var msAuxValue = moSel.options[i-1].text;
      moSel.options[i-1] = new Option(moSel.options[i].text,moSel.options[i].value,false,true);//moSel.options[i];
      moSel.options[i] = new Option(msAuxValue,msAuxKey,false,false);
    }
  }
  /*******************************************************************************
   * executada no onclick dos botoes de down
   * 
   * <p>move o �tem do select uma posi��o para baixo</p>
   * @param string psSelectId nome do select alvo do evento
   *******************************************************************************/
  self.moveDown = function(psSelectId){
    var moSel = gebi(psSelectId);
    if((moSel.selectedIndex < moSel.length-1)&&(moSel.selectedIndex>=0)){
      var i = moSel.selectedIndex;
      var msAuxKey = moSel.options[i+1].value;
      var msAuxValue = moSel.options[i+1].text;
      moSel.options[i+1] = new Option(moSel.options[i].text,moSel.options[i].value,false,true);//moSel.options[i];
      moSel.options[i] = new Option(msAuxValue,msAuxKey,false,false);
    }
  }
  /*******************************************************************************
   * executada dos bot�es de direita e esquerda dos select do panel das colunas exibidas
   * 
   * <p>move o �tem selecionado do select psSelSource para o psSelTarget</p>
   * @param string psSelectedId nome do select alvo do evento
   * @param string psSelSource select fonte do �tem que ser� movido
    * @param string psSelTarget select onde o �tem ser� inserido
   *******************************************************************************/
  self.moveValueSel = function (psSelSource,psSelTarget){
    var moSelSource = gebi(psSelSource);
    var moSelTarget = gebi(psSelTarget);
    if(moSelSource.selectedIndex>=0){
      var moAuxOpt = moSelSource[moSelSource.selectedIndex];
      moSelSource.remove(moSelSource.selectedIndex);
      moSelTarget[moSelTarget.length] = moAuxOpt;
    }
  }
  /*******************************************************************************
   * executada dos bot�es de direita e esquerda dos select do panel das colunas exibidas
   * 
   * <p>move o �tem selecionado do select psSelSource para o psSelTarget</p>
   * @param string psSelectedId nome do select alvo do evento
   * @param string psSelSource select fonte do �tem que ser� movido
    * @param string psSelTarget select onde o �tem ser� inserido
   *******************************************************************************/
  self.moveValueSelMod = function (psSelSource,psSelTarget){
    var moSelSource = gebi(psSelSource);
    var moSelTarget = gebi(psSelTarget);
    if(moSelSource.selectedIndex>=0){
      self.ciLastColSelProp=undefined;
      if(psSelSource == self.gridName+'_sel_col_show'){
        lengthProp = self.coSelPropCol.length;
        for(var k=0;k<lengthProp;k++)
          if(self.coSelPropCol[k].value ==moSelSource.value){
            self.coSelPropCol.remove(k);
            break;
          }
      }else{
        var i = moSelSource.selectedIndex;
        self.coSelPropCol.options[self.coSelPropCol.length] = new Option(moSelSource.options[i].text,moSelSource.options[i].value,false,false);
      }
      var moAuxOpt = moSelSource[moSelSource.selectedIndex];
      moSelSource.remove(moSelSource.selectedIndex);
      moSelTarget[moSelTarget.length] = moAuxOpt;
    }
  }
  /*******************************************************************************
   * fun��o que verifica se a coluna pode ser retirada da grid
   * 
   * <p>fun��o que verifica se a coluna pode ser retirada da grid, se n�o pode, desabilita o viewbutton</p>
   *******************************************************************************/
  self.selShowOnClick = function(psSelTarget){
    var moSel = gebi(psSelTarget);
    if((moSel.selectedIndex>=0)&&(psSelTarget==self.gridName+'_sel_col_show')){
      if(self.maColEdit[moSel.value]==1){
        if(self.btLeftShow.object)
          self.btLeftShow.object.enable();
      }else{
        self.btLeftShow.object.disable('VBPrefLeftGrey');
      }
    }
  
  }
  /*******************************************************************************
   * fun��o que salva os valores editados
   * 
   * <p>serializa todos os valores editados e coloca em um inputHidden da grid</p>
   *******************************************************************************/
  self.savePreferencesCodeInGrid = function(){
    //atualiza as pref das propriedades da coluna antes de salvar
    self.propColClickBeforeSave();
    //atualiza as pref de ordenamento das colunas da grid
    self.propOrderClickBeforeSave();
    
    var maValues = new Array();
    //ordenamento dos dados das colunas da grid
    var msAuxColumnOrder = '';
    var length = self.coSelColOrder.length;
        for(var j=0;j<length;j++){
        var i =  self.coSelColOrder.options[j].value;
        if((self.maColOrder[i]!=null)&&(self.maColOrder[i]!='')&&(self.maColOrder[i]!=' '))
          if(msAuxColumnOrder=='')
            msAuxColumnOrder += self.maColOrder[i][1]+self.maColOrder[i][0];
          else
            msAuxColumnOrder += ':'+ self.maColOrder[i][1]+self.maColOrder[i][0];
    }
    maValues.push(msAuxColumnOrder);
    //atualiza quais colunas devem ser exibidas e qual a ordem entre elas
    var msAuxColumnShow = '';
    for(i=0;i<self.coOrderColumn.length;i++)
     if(i==0)
       msAuxColumnShow += self.coOrderColumn[i].value;
      else
        msAuxColumnShow += ':' + self.coOrderColumn[i].value;
    maValues.push(msAuxColumnShow);
    //salva as propriedades das colunas
    self.coColumnsProp.value = '';
    if(self.maColProp){
      var length = self.maColProp.length;
          for (i in self.maColProp){
          if(self.coColumnsProp.value=='')
            self.coColumnsProp.value += self.maColProp[i].join(':');
          else
            self.coColumnsProp.value += ';'+ self.maColProp[i].join(':');
      }
    }
    
    maValues.push(self.coColumnsProp.value);
    //linhas por p�gina na grid
    maValues.push(self.ciRowsPerPage.value);
    //altura das linhas da grid
    maValues.push(self.ciRowsHeight.value);
    //tooltip das colunas da grid
    var msAuxToolTip = '';
    if((self.coTooltipSource.value!='')&&(self.coTooltipTarget.value!=''))
      msAuxToolTip = self.coTooltipSource.value+':'+self.coTooltipTarget.value;
    maValues.push(msAuxToolTip);

    var moObjSer = new fwdObjSerialize();
    var moPopup = soPopUpManager.getPopUpById(self.gridName+'_preferences');
    moPopup.getOpener().document.getElementById(self.gridName+'_preferences_code_values').value = moObjSer.serializeArray(maValues);
  
    moPopup.getOpener().js_grid_exec_trigger(self.gridName+'_grid_preference_save');
  }
  /*******************************************************************************
   * limpa as v�riaveis do objeto
   * 
   * <p>limpa todas as vari�veis utilizadas pelo objeto</p>
   *******************************************************************************/
  self.cleanVars = function(){
    self.coOrderColumnsData.value = '' ;
    self.coOrderColumn.value = '';
    self.coColumnsProp.value = '';
    self.ciRowsPerPage.value = '';
    self.ciRowsHeight.value = '';
    self.coTooltipSource.value = '';
    self.coTooltipTarget.value = '';
    self.coSelPropCol.value = '';
    self.coTextPropColWidth.value = '';
    self.coSelPropNoWrap.value = '';
    self.ciLastColSelProp = undefined;
    self.maColProp = new Array();
    self.coSelColOrder.value = '';
    self.coSelColNotOrder.value = '';
    self.colSelColOrderBy.value = '';
    self.ciLastOrderCol='';
    self.coStColOrderBy.value = '';
    self.maColOrder = new Array();
  }
  /*******************************************************************************
   * fun��o de executa os eventos que resetam a configura��o de prefer�ncias da grid
   * 
   * <p>executa os eventos para resetar as configura��es de prefer�ncias da grid,
   *redesenha a grid , e fecha a popup de preferencias</p>
   *******************************************************************************/
  //moPopup.setCloseEvent('objManagerGrifPref.cleanVars()');
  
  self.resetConfig = function(){
    var moPopup = soPopUpManager.getPopUpById(self.gridName+'_preferences');
    moPopup.getOpener().js_grid_exec_trigger(self.gridName+'_grid_preference_default');
    moPopup.getOpener().js_grid_exec_trigger(self.gridName+'_draw_user_edit');
    soPopUpManager.closePopUp(self.gridName+'_preferences');
  }
}
/** @var Object Objeto de preferencias da grid. */
objManagerGrifPref = new FWDGridPreferences();
/*******************************************************************************
 * fun��o que dispara uma trigger de um evento passado por par�metro
 * 
 * <p>dispara uma trigger de um evento passado por par�metro</p>
 * @param string psSelectId nome do select alvo do evento
 *******************************************************************************/
js_grid_exec_trigger = function(psGridEvent){
  return trigger_event(psGridEvent,3);
};