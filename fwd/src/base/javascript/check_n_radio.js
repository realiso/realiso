/**
 * Seleciona um radiobox
 *
 * <p>Seleciona o radiobox identificado pela chave psKey e deseleciona os outros
 * radioboxes controlados pelo mesmo controller.</p>
 *
 * @param string psControllerId Id do controller
 * @param string psKey Atributo key do radiobox
 */
function gfx_radio(psControllerId,psKey){
  var foController = gebi(psControllerId);
  var foOld = gebi(psControllerId+'_'+foController.value);
  var foNew = gebi(psControllerId+'_'+psKey);
  var fsDir = foNew.src.substr(0,foNew.src.lastIndexOf('/')+1);
  if(foOld) foOld.src = fsDir+'uncheck.gif';
  foNew.src = fsDir+'radio.gif';
  foController.value = psKey;
}

/**
 * Deseleciona um radiobox
 *
 * <p>Desmarca o radiobox identificado pela chave psKey e zera o valor do controller do radiobox</p>
 *
 * @param string psControllerId Id do controller
 */
function uncheck_radio(psControllerId){
  var foController = gebi(psControllerId);
  var foOld = gebi(psControllerId+'_'+foController.value);
  if(foOld){
    var fsDir = foOld.src.substr(0,foOld.src.lastIndexOf('/')+1);
    foOld.src = fsDir+'uncheck.gif';
  }
  foController.value = '';
}

/**
 * Seleciona ou deseleciona um checkbox
 *
 * <p>Seleciona ou deseleciona um checkbox. Se ele tiver o atributo checkAll,
 * (des)seleciona os outros checkboxes controlados pelo mesmo controller.
 * Se estiver deselecionando, deseleciona todos checkboxes que t�m o atributo
 * checkAll.</p>
 *
 * @param string psControllerId Id do controller
 * @param string psKey Atributo key do checkbox
 */
function gfx_check(psControllerId, psKey, force){
  var foController = gebi(psControllerId);
  var foImg = gebi(psControllerId+'_'+psKey);
  var fiPos = foImg.src.lastIndexOf('/');
  var fsDir = foImg.src.substr(0,fiPos+1);
  var fsImg = foImg.src.substr(fiPos+1);
  var fbChecked = (fsImg=='check.gif'?true:false);
  if(foImg.checkListener){
	  foImg.checkListener(psControllerId, psKey, fbChecked);
  }
  
  if(force){
	  if(force.check){
		  if(fbChecked){
			  return;
		  }
	  }
	  if(force.uncheck){
		  if(!fbChecked){
			  return;
		  }
	  }
  }
  
  var faSelected = foController.value.split(':');
  var faCheckAll = foController.getAttribute('checkAll');
  faCheckAll = (faCheckAll==''?new Array():faCheckAll.split(':'));
  var fsNewImgSrc;
  if(arrayIndexOf(faCheckAll,psKey)>-1){ // (des)checar todos
    var fsItemKeys = foController.getAttribute('itemKeys');
    var faItemKeys = fsItemKeys.split(':');
    if(fbChecked){ // deschecar
      fsNewImgSrc = fsDir+'uncheck.gif';
      foController.value = '';
    }else{ // checar
      fsNewImgSrc = fsDir+'check.gif';
      foController.value = fsItemKeys;
    }
    for(var i=0;i<faItemKeys.length;i++){
      gebi(psControllerId+'_'+faItemKeys[i]).src = fsNewImgSrc;
    }
  }else{ // (des)checar s� ele
    if(fbChecked){ // deschecar
      fsNewImgSrc = fsDir+'uncheck.gif';
      faSelected.splice(arrayIndexOf(faSelected,psKey),1);
      // desmarca qq checkAll que estiver marcado
      for(var i=0;i<faCheckAll.length;i++){
        fiPos = arrayIndexOf(faSelected,faCheckAll[i]);
        if(fiPos>-1){
          gebi(psControllerId+'_'+faCheckAll[i]).src = fsNewImgSrc;
          faSelected.splice(fiPos,1);
        }
      }
    }else{ // checar
      fsNewImgSrc = fsDir+'check.gif';
      faSelected.push(psKey);
    }
    foImg.src = fsNewImgSrc;
    foController.value = faSelected.join(':');
  }
};