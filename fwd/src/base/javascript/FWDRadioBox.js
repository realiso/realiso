/**
 * FWD - FAST WEB DEVELOPMENT
 * 
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/*******************************************************************************
 * Classe FWDRadioBox. Classe com as fun��es utilizadas pelo FWDRadioBox.
 * 
 * <p>Classe com as fun��es utilizadas pelo FWDRadioBox.</p>
 * @package FWD
 * @param String psName Nome do elemento
 *******************************************************************************/
function FWDRadioBox(psName,psController,psKey){
  
 /**
  * Refer�ncia para si mesmo
  * @var FWDItemController
  * @access private
  */
  var self = new FWDItemController(psName,psController,psKey);
  
  return self;
  
};
