/**
 * FWD - FAST WEB DEVELOPMENT
 * 
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/*******************************************************************************
 * Classe FWDMemoStatic. Classe com as fun��es utilizadas pelo FWDMemoStatic.
 * 
 * <p>Classe com as fun��es utilizadas pelo FWDMemoStatic.</p>
 * @package FWD
 * @param String psName Nome do elemento
 *******************************************************************************/
function FWDStatic(psName)
{
  /** @var Object referencia para ele mesmo. */
  var self = this;
  /** @var Object TD do static. */
  var coObjTD = gebi(psName+'_td');
  /** @var Object table do static. */
  var coObj = gebi(psName);
  /** @var String Nome do elemento. */
  self.csName = psName;
  

/*****************************************************************************
 * Seta o valor no memostatic
 *
 * <p>seta o valor no memostatic</p>
 *
 *****************************************************************************/
  self.setValue = function(psValue){
	coObjTD.innerHTML = psValue;
  }

/*****************************************************************************
 * Seta o valor no memostatic
 *
 * <p>seta o valor no memostatic</p>
 *
 *****************************************************************************/
  self.getValue = function(){
	return coObjTD.innerHTML;
  }

 /*****************************************************************************
  * Indica se o elemento est� satisfatoriamente preenchido.
  *
  * <p>Indica se o elemento est� satisfatoriamente preenchido
  * (um dos m�todos necess�rios para customizar o required check).</p>
  * @return boolean True sse est� satisfatoriamente preenchido
  *****************************************************************************/
  self.isFilled = function(){
	if((coObjTD.innerHTML=='')||(coObjTD.innerHTML==null)||(coObjTD.innerHTML==undefined))
		return false;
	else
		return true;
  }
 /*****************************************************************************
 * Esconde o static
 *
 * <p>Esconde o td do static</p>
 *
 *****************************************************************************/
  self.hide = function(){
	coObj.style.display='none';
  }
/*****************************************************************************
 * Exibe o static
 *
 * <p> Exibe o static mudando o valor do display do td
 * do viewButton</p>
 *
 *****************************************************************************/
  self.show = function(){
	coObj.style.display='';
  }
};