/**
 * FWD - FAST WEB DEVELOPMENT
 * 
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/*******************************************************************************
 * Classe Ajaxevent. Implementa uma fila de eventos Ajax.
 * 
 * <p>Classe que implementa uma fila de eventos Ajax.</p>
 *
 * @package javascript
 *******************************************************************************/

var actionChain = {

 chainLengthMAX: 0,

 chainLengthMID: 0,

 chainLengthMIN: 0,
 
 chainLengthZERO: 0,

  actionsMAX: new Array(),

  actionsMID: new Array(),

  actionsMIN: new Array(),
  
  actionsZERO: new Array(),

  initialize: function() {
				this.chainLengthMAX=0;
				this.chainLengthMID=0;
				this.chainLengthMIN=0;
				this.chainLengthZERO=0;
				this.actionsMAX = new Array();
				this.actionsMID = new Array();
				this.actionsMIN = new Array();
				this.actionsZERO = new Array();
              },

   addAction: function(actionName,priority) {
				switch (parseInt(priority)) {
				case 1:	this.actionsMAX.push(actionName);
						this.chainLengthMAX++;
						break;
				case 2:	this.actionsMID.push(actionName);
						this.chainLengthMID++;
						break;
				case 3:	this.actionsMIN.push(actionName);
						this.chainLengthMIN++;
						break;
				case 0:
						this.actionsZERO = new Array();
						this.chainLengthZERO = 0;
						this.actionsZERO.push(actionName);
						this.chainLengthZERO++;
						break;
				}
              },

getCurAction: function() {
				var curAction = "";
				if (this.actionsZERO.length>0) {
					curAction = this.actionsZERO[0];
					this.actionsZERO.shift();
					this.chainLengthZERO--;
				}
				else if (this.actionsMAX.length>0) {
					curAction = this.actionsMAX[0];
					this.actionsMAX.shift();
					this.chainLengthMAX--;
				}
				else {
					if (this.actionsMID.length>0) {
						curAction = this.actionsMID[0];
						this.actionsMID.shift();
						this.chainLengthMID--;
					}
					else {
						curAction = this.actionsMIN[0];
						this.actionsMIN.shift();
						this.chainLengthMIN--;
					}
				}
				return curAction;
              },

 executeHTTP: function(request) {

				http_request = createXMLHttpRequest();

				frm = document.getElementsByTagName("form");
				string = ((frm[0])?getAllfromForm(frm[0]):""); // pega possiveis alteracoes do form

				final_parameters = request + string;

				
				// variavel que determina se este evento j� foi executado
				http_request_exec = false;
				pog_url = window.location.href.split('#');
        http_request.open('POST', pog_url[0], true);
				http_request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
				http_request.setRequestHeader("Content-length", final_parameters.length);
				// Descomentando a linha abaixo da erro em autentica��o NTLM da Microsoft
				//http_request.setRequestHeader("Connection", "close");
				http_request.onreadystatechange = alertContents;
				http_request.send(final_parameters);
              },

        fire: function() {
				if (this.chainLengthMAX>0 || this.chainLengthMID>0 || this.chainLengthMIN>0 || this.chainLengthZERO>0) {
					request_is_active = true;
					showLoading();

					this.executeHTTP(this.getCurAction());

				}
				else {
					request_is_active = false;
					hideLoading();
				}
              },

alertActions: function() {
				actionsStr = "";
				if (this.actionsMAX.length>0) {
					for(i=0; i<this.actionsMAX.length; i++)
						actionsStr += "*1: MAX* Action "+i+" : "+this.actionsMAX[i]+"\n";
				}
				if (this.actionsMID.length>0) {
					for(i=0; i<this.actionsMID.length; i++)
						actionsStr += "*2: MID* Action "+i+" : "+this.actionsMID[i]+"\n";
				}
				if (this.actionsMIN.length>0) {
					for(i=0; i<this.actionsMIN.length; i++)
						actionsStr += "*3: MIN* Action "+i+" : "+this.actionsMIN[i]+"\n";
				}
				alert(actionsStr);
              }
}

var http_request = null;
var http_request_exec = false;
var request_is_active = false;
var debug = false;
//var debug = true;

actionChain.initialize();

/*****************************************************************************
 * Retorna todos os valores do form
 *
 * <p>Retorna todos os valores do form.</p>
 *
 * @param string form Elemento Form
 * @return string Elementos do Form
 *****************************************************************************/
function getAllfromForm(form) {
	var temp_string = "";
	if(form[form.name+"_val"]) {
		temp_string += form.name + "=" + form[form.name+"_val"].value;
	}
	for (i=0; i<form.elements.length; i++) {
		if (form.elements[i].name.indexOf("[]") > 0) { // select multiple ?
			for (j=0; j<form.elements[i].length; j++ )
				if (form.elements[i].options[j].selected){
					var auxString = form.elements[i].options[j].value;
					auxString = auxString.replace(/&/g,'%26');
					auxString = auxString.replace(/\+/g,'%2B');
					temp_string += '&' + form.elements[i].name + '=' + auxString;
				}
		}
		else{
				var auxString = form.elements[i].value;
				auxString = auxString.replace(/&/g,'%26');
				auxString = auxString.replace(/\+/g,'%2B');
				temp_string += '&' + form.elements[i].name + '=' + auxString;
			}
	}
	return temp_string;
}

/*****************************************************************************
 * Executa a funcao http_request.responseText
 *
 * <p>Executa a funcao http_request.responseText.</p>
 *
 *****************************************************************************/
// Handler dos eventos do objeto de consulta
function alertContents() {
  // Caso tenha terminado de buscar as informa��es, executa procedimento
  try{
    if(http_request.readyState == 4 && !http_request_exec){
      if(http_request.status == 200){
        // marca que o evento j� foi executado
        http_request_exec = true;
        if(debug){
          alert(http_request.responseText);
        }else{
          fwd_eval(http_request.responseText,true);
        }
        http_request.abort();
        actionChain.fire();
      } else if (http_request.status == 12029 || http_request.status == 12031){
    	alert(unescape("Erro: Ocorreu um erro ao estabelecer uma conex%E3o com o servidor, verifique a conectividade de rede.\nError: An error has occurred while establishing a connection to the server, check network connectivity."));
      }else{
        alert('There was a problem with the ajax request.\n'+http_request.statusText+' - '+http_request.status);
      }        
    }
  }catch(e){e=null;}
}

/*****************************************************************************
 * Cria um objeto XMLHttpRequest
 *
 * <p>Cria um objeto XMLHttpRequest. Objeto para requisicao do evento Ajax.</p>
 * 
 * @return object XMLHttpRequest
 *****************************************************************************/
function createXMLHttpRequest() {
	// Cria o objeto para buscar os dados no servidor
	if (window.XMLHttpRequest) { // Mozilla, Safari,...
		new_http_request = new XMLHttpRequest();
		if (new_http_request.overrideMimeType) {
			new_http_request.overrideMimeType('text/js');
			// See note below about this line
		}
	}
	else if (window.ActiveXObject) { // IE
		try {
			new_http_request = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e) {
			try {
				new_http_request = new ActiveXObject("Microsoft.XMLHTTP");
			}
			catch (e) {}
		}
	}
	if (!new_http_request) {
		alert('Giving up :( Cannot create a XMLHTTP instance');
		return false;
	}
	return new_http_request;
}

/*****************************************************************************
 * Adiciona o evento AJAX na fila e, caso possivel, dispara-o
 *
 * <p>Adiciona o evento AJAX na fila. Caso nao exista nenhuma requisicao ativa,
 * dispara o evento.</p>
 *
 * @param string trigger Nome unico do evento Ajax (gerado automaticamente)
 * @param string priority Prioridade do evento Ajax (3=MIN, 2=MID, 1=MAX)
 *****************************************************************************/

function trigger_event2(trigger,priority) {
	msFWDParameters = 'fwd_ajax_trigger=1&fwd_ajax_event_name=' + trigger;
	actionChain.addAction(msFWDParameters,priority);  // 'string' sera concatenada imediatamente antes da execucao

	if (!request_is_active) {
		actionChain.fire();
	}
}

function trigger_event(trigger,priority) {
	setTimeout("trigger_event2('"+trigger+"','"+priority+"')",1);
};