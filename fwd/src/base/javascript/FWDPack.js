/**
 * FWD - FAST WEB DEVELOPMENT
 * 
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/*******************************************************************************
 * Classe FWDPack. Classe com as fun��es utilizadas pelo FWDPack.
 * 
 * <p>Classe com as fun��es utilizadas pelo FWDPack.</p>
 * @package FWD
 * @param String psName Nome do elemento
 *******************************************************************************/
function FWDPack(psName){

 /**
  * Refer�ncia para si mesmo
  * @var FWDPack
  * @access private
  */
  var self = this;

 /**
  * Nome do elemento
  * @var String
  * @access public
  */
  self.csName = psName;

 /**
  * Elemento correspondente
  * @var HTMLElement
  * @access private
  */
  var coElement = gebi(psName);
  
 /**
  * Lista dos ids dos elementos do pacote
  * @var Array
  * @access public
  */
  self.caElements = [];
  
  coElement.object = self;
  
 /*****************************************************************************
  * Adiciona um elementos ao pacote.
  *
  * <p>Adiciona um elementos ao pacote.</p>
  * @param String psElementId Id do elemento
  *****************************************************************************/
  self.addElement = function(psElementId){
    self.caElements.push(psElementId);
  }
  
 /*****************************************************************************
  * Torna os elementos vis�veis.
  *
  * <p>Torna os elementos vis�veis.</p>
  *****************************************************************************/
  self.show = function(){
    var i;
    var moElement;
    for(var i=0;i<self.caElements.length;i++){
      moElement = gobi(self.caElements[i]);
      if(moElement && moElement.show){
        moElement.show();
      }else{
        js_show(self.caElements[i]);
      }
    }
    moElement = null;
  }

 /*****************************************************************************
  * Torna os elementos invis�veis.
  *
  * <p>Torna os elementos invis�veis.</p>
  *****************************************************************************/
  self.hide = function(){
    var i;
    var moElement;
    for(var i=0;i<self.caElements.length;i++){
      moElement = gobi(self.caElements[i]);
      if(moElement && moElement.hide){
        moElement.hide();
      }else{
        js_hide(self.caElements[i]);
      }
    }
    moElement = null;
  }

 /*****************************************************************************
  * Habilita os elementos.
  *
  * <p>Habilita os elementos.</p>
  *****************************************************************************/
  self.enable = function(){
    var i;
    var moElement;
    for(var i=0;i<self.caElements.length;i++){
      moElement = gobi(self.caElements[i]);
      if(moElement && moElement.enable){
        moElement.enable();
      }else{
        gebi(self.caElements[i]).disable = false;
      }
    }
    moElement = null;
  }

 /*****************************************************************************
  * Desabilita os elementos.
  *
  * <p>Desabilita os elementos.</p>
  *****************************************************************************/
  self.disable = function(){
    var i;
    var moElement;
    for(var i=0;i<self.caElements.length;i++){
      moElement = gobi(self.caElements[i]);
      if(moElement && moElement.disable){
        moElement.disable();
      }else{
        gebi(self.caElements[i]).disable = true;
      }
    }
    moElement = null;
  }

};