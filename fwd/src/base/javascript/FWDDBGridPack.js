/**
 * FWD - FAST WEB DEVELOPMENT
 * 
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/*******************************************************************************
 * Classe FWDDBGridPack. Classe com as fun��es utilizadas pelo FWDDBGridPack.
 * 
 * <p>Classe com as fun��es utilizadas pelo FWDDBGridPack.</p>
 * @package FWD
 * @param String psName Nome do elemento
 *******************************************************************************/
function FWDDBGridPack(psName){

 /**
  * Refer�ncia para si mesmo
  * @var FWDPack
  * @access private
  */
  var self = new FWDPack(psName);

 /*****************************************************************************
  * Executa o refresh de todas grids do pacote.
  *
  * <p>Executa o refresh de todas grids do pacote em um �nico evento.</p>
  *****************************************************************************/
  self.refresh = function(){
    for(var i=0;i<self.caElements.length;i++){
      gobi(self.caElements[i]).setPopulate(true);
    }
    trigger_event(self.csName+'_refresh',3);
  }
  
  return self;

};