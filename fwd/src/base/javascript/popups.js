/**
 * FWD - FAST WEB DEVELOPMENT
 * 
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/*******************************************************************************
 * Classe FWDPopUp. Implementa uma janela virtual dentro de uma p�gina.
 * 
 * <p>Implementa PopUps em janelas virtuais dentro de uma p�gina.</p>
 *
 * @package javascript
 * @param String psPopUpId Id da PopUp
 * @param String psSrc URL da p�gina a ser aberta na PopUp
 * @param mixed pmTitle String ou elemento HTML que ser� o t�tulo da PopUp
 * @param integer piHeight Altura da PopUp
 * @param integer piWidth Largura da PopUp
 * @param Window poOpener Janela que chamou a PopUp
 * @param integer piTop Top da PopUp (opcional)
 * @param integer piLeft Left da PopUp (opcional)
 * OBS: Se piTop e piLeft n�o forem passados, a PopUp � criada centralizada
 *******************************************************************************/
function FWDPopUp(psPopUpId,psSrc,pmTitle,piHeight,piWidth,poOpener,piTop,piLeft){

 /**
  * Refer�ncia para si mesmo.
  * @var FWDPopUp
  */
  var self = this;
  
  var moBody = document.getElementsByTagName('body').item(0);
  moBody.style.overflow = 'hidden';
  var miClientHeight = piHeight - (browser.isIE?0:2);
  var miClientWidth = piWidth - 2;
  // popup
  var moPopUp = document.createElement('DIV');
  moPopUp.coPopUp = self;
  moPopUp.id = psPopUpId;
  moPopUp.style.border = 'none';
  moPopUp.style.position = 'absolute';
  moPopUp.style.top = (piTop?piTop:(moBody.clientHeight - piHeight)/2);
  moPopUp.style.left = (piLeft?piLeft:(moBody.clientWidth - piWidth)/2);
  moPopUp.style.height = piHeight;
  moPopUp.style.width = piWidth;
  moBody.appendChild(moPopUp);
  // div onde fica todo conte�do da popup
  var moContent = document.createElement('DIV');
  moContent.className = 'FWDPopUp';
  moContent.style.height = miClientHeight;
  moContent.style.width = miClientWidth;
  moContent.style.borderWidth = 1;
  moPopUp.appendChild(moContent);
  // iframe escondido atras de tudo (para contornar o bug do z-index dos selects do IE)
  var moIframeTitle = document.createElement('IFRAME');
  moIframeTitle.style.position = 'absolute';
  moIframeTitle.style.top = 0;
  moIframeTitle.style.left = 0;
  moIframeTitle.style.width = piWidth;
  moIframeTitle.style.height = miClientHeight;
  moIframeTitle.style.border = 'none';
  moIframeTitle.style.zIndex = -1;
  moIframeTitle.frameBorder = 0;
  moPopUp.style.overflow = 'hidden';
  moPopUp.appendChild(moIframeTitle);
  
 /*****************************************************************************
  * Destr�i a PopUp e todo seu conte�do
  *
  * <p>Destr�i a PopUp e todo seu conte�do.</p>
  *****************************************************************************/
  self.destroy = function(){
    var moException;
    if(self.closeEvent) self.closeEvent();
    self.coGlass.destroy();
    self.coIframe.src = '';
    FWDMemoryCleaner.cleanUpElement(self.coPopUp,true);
    self.coPopUp.parentNode.removeChild(self.coPopUp);
    self = null;
  }
  
 /*****************************************************************************
  * Fecha a PopUp
  *
  * <p>Fecha a PopUp.</p>
  *****************************************************************************/
  self.close = function(){
    soPopUpManager.closePopUp(self.csPopUpId);
  }
  
  // close button
  var moCloseButton = document.createElement('INPUT');
  moCloseButton.type = 'button';
  moCloseButton.value = 'X';
  moCloseButton.style.position = 'absolute';
  moCloseButton.style.height = 20;
  moCloseButton.style.width = 20;
  moCloseButton.style.top = 2;
  moCloseButton.style.left = piWidth - 22;
  moCloseButton.className = 'FWDPopUpCloseButton';
  moPopUp.appendChild(moCloseButton);
  
  // title
  var moTitle = document.createElement('DIV');
  moTitle.id = psPopUpId+'_title';
  if(typeof(pmTitle)=='string'){
    moTitle.innerHTML = pmTitle;
  }else{
    moTitle.innerHTML = pmTitle.innerHTML;
    moTitle.style.height = pmTitle.style.height;
  }
  moTitle.style.top = 0;
  moTitle.style.left = 0;
  moTitle.style.width = miClientWidth;
  moTitle.style.cursor = 'move';
  moTitle.className = 'FWDPopUpTitle';
  moContent.appendChild(moTitle);
  // Pega a altura do titulo. Precisa ser depois de inserir na pagina.
  miTitleHeight = moTitle.offsetHeight;

  // iframe
  var moIframe = document.createElement('IFRAME');
  moIframe.style.position = 'absolute';
  moIframe.style.top = miTitleHeight;
  moIframe.style.left = 1;
  moIframe.style.height = piHeight - miTitleHeight - 1;
  moIframe.style.width = miClientWidth;
  moIframe.style.border = 'none';
  moIframe.style.overflow = 'hidden';
  moIframe.src = psSrc;
  moIframe.frameBorder = 0;
  moContent.appendChild(moIframe);
  // vidro
  var moGlass = new FWDGlass('silver',20,piHeight - miTitleHeight,piWidth);

 /**
  * Janela que abriu a PopUp
  * @var Window
  */
  self.coOpener = poOpener;

 /**
  * Div que representa a janela PopUp
  * @var HTMLDivElement
  */
  self.coPopUp = moPopUp;
  
 /**
  * Id da PopUp
  * @var String
  */
  self.csPopUpId = psPopUpId;

 /**
  * Div que representa a barra de t�tulo da PopUp
  * @var HTMLDivElement
  */
  self.coTitle = moTitle;

 /**
  * Div de conte�do da PopUp
  * @var HTMLDivElement
  */
  self.coContent = moContent;

 /**
  * IFrame do conte�do da PopUp
  * @var HTMLIFrameElement
  */
  self.coIframe = moIframe;
  
 /**
  * Div de conte�do HTML da PopUp. Quando usado, substitui o iframe.
  * @var HTMLDivElement
  */
  self.coContentHtml = null;
  
 /**
  * IFrame que fica atr�s do t�tulo para mascarar bug do z-index do select no IE 6
  * @var HTMLIFrameElement
  */
  self.coIframeTitle = moIframeTitle;

 /**
  * Bot�o para fechar a PopUp
  * @var HTMLInputElement
  */
  self.coCloseButton = moCloseButton;

 /**
  * Div transparente que cobre o conte�do da PopUp quando ela n�o est� ativa para capturar eventos de mouse
  * @var HTMLDivElement
  */
  self.coGlass = moGlass;

 /**
  * Altura da barra de t�tulo
  * @var integer
  */
  self.ciTitleHeight = miTitleHeight;
  
 /**
  * Fun��o que deve ser executada ao fechar a popup
  * @var Function
  */
  self.closeEvent = null;
  
  // derreferencia os objetos locais
  moBody = moPopUp = moContent = moIframeTitle = moIframe = moCloseButton = moTitle = moGlass = null;
  psPopUpId = psSrc = pmTitle = poOpener = null;
  
 /*****************************************************************************
  * Seta o z-index da PopUp
  *
  * <p>Seta o z-index da PopUp.</p>
  * 
  * @param integer piZIndex Novo z-index para PopUp
  *****************************************************************************/
  self.setZIndex = function(piZIndex){
    self.coPopUp.style.zIndex = piZIndex;
    self.coGlass.setZIndex(piZIndex+1);
  }
  
 /*****************************************************************************
  * Retorna o z-index da PopUp
  *
  * <p>Retorna o z-index da PopUp.</p>
  * 
  * @return integer z-index da PopUp
  *****************************************************************************/
  self.getZIndex = function(){
    return Number(self.coPopUp.style.zIndex);
  }
  
 /*****************************************************************************
  * M�todo chamado quando a PopUp ganha o foco
  *
  * <p>M�todo chamado quando a PopUp ganha o foco.</p>
  *****************************************************************************/
  self.focus = function(){
    self.coGlass.hide();
  }
  
 /*****************************************************************************
  * Define a fun��o que deve ser executada ao fechar a popup.
  *
  * <p>Define a fun��o que deve ser executada ao fechar a popup.</p>
  * @param Function poCloseFunc Fun��o a ser chamada pela popup ao ser fechada
  *****************************************************************************/
  self.setCloseEvent = function(poCloseFunc){
    self.closeEvent = poCloseFunc;
    poCloseFunc = null;
  }

 /*****************************************************************************
  * M�todo chamado quando a PopUp perde o foco
  *
  * <p>M�todo chamado quando a PopUp perde o foco.</p>
  *****************************************************************************/
  self.blur = function(){
    var miLeft = self.coPopUp.style.left;
    var miTop = parseInt(self.coPopUp.style.top) + self.ciTitleHeight;
    self.coGlass.show(self.getZIndex()+1,miLeft,miTop);
  }
  
 /*****************************************************************************
  * Retorna a window contida na PopUp.
  *
  * <p>Retorna a window contida na PopUp.</p>
  * @return Window Objeto Window contido na PopUp
  *****************************************************************************/
  self.getWindow = function(){
    return self.coIframe.contentWindow;
  }

 /*****************************************************************************
  * Retorna a janela que abriu a PopUp.
  *
  * <p>Retorna a janela que abriu a PopUp.</p>
  * @return Window Janela que abriu a PopUp
  *****************************************************************************/
  self.getOpener = function(){
    return self.coOpener;
  }
  
 /*****************************************************************************
  * Seta o conte�do da PopUp.
  *
  * <p>Define um conte�do pra PopUp em HTML para ser exibido ao inv�s de um
  * iframe.</p>
  * @param String psHtml C�digo HTML a ser inserido na PopUp
  * @param boolean pbKeepSrc Sse for true, mant�m o src do iframe (usado pra
  *   carregar a p�gina em background)
  *****************************************************************************/
  self.setHtmlContent = function(psHtml,pbKeepSrc){
    if(!pbKeepSrc){
      self.coIframe.src = '';
      self.coIframe.style.display = 'none';
    }
    if(!self.coContentHtml){
      var moContentHtml = document.createElement('DIV');
      moContentHtml.style.display = 'block';
      moContentHtml.style.height = self.coIframe.style.height;
      moContentHtml.style.width = self.coIframe.style.width;
      self.coContent.appendChild(moContentHtml);
      self.coContentHtml = moContentHtml;
      // Adi��o de 2px para aparecer a borda inferior no IE.
      self.coPopUp.style.height = (parseInt(self.coPopUp.style.height) + 2);
    }
    var maHtml = psHtml.split(/<\/?script[^>]*>/);
    var mbScript = false;
    var msHtml = '';
    var msScript = '';
    for(i=0;i<maHtml.length;i++){
      if(mbScript){
        msScript+= maHtml[i];
      }else{
        msHtml+= maHtml[i];
      }
      mbScript = !mbScript;
    }
    self.coContentHtml.innerHTML = msHtml;
    fwd_eval(msScript);
    moContentHtml = psHtml = maHtml = msHtml = msScript = null;
  }
  
 /*****************************************************************************
  * Reposiciona a PopUp.
  *
  * <p>Define a posi��o do canto superior esquerdo da PopUp.</p>
  * @param integer piTop Coordenada vertical
  * @param integer piLeft Coordenada horizontal
  *****************************************************************************/
  self.setPosition = function(piTop,piLeft){
    self.coPopUp.style.top = piTop;
    self.coPopUp.style.left = piLeft;
  }
  
 /*****************************************************************************
  * Centraliza a PopUp.
  *
  * <p>Centraliza a PopUp em rela��o a �rea cliente do browser.</p>
  *****************************************************************************/
  self.centralize = function(){
    var miHeight = parseInt(self.coPopUp.style.height);
    var miWidth = parseInt(self.coPopUp.style.width);
    var moBody = document.getElementsByTagName('body').item(0);
    var miTop = (moBody.clientHeight - miHeight)/2;
    var miLeft = (moBody.clientWidth - miWidth)/2;
    self.setPosition(miTop,miLeft);
  }
  
 /*****************************************************************************
  * Redimensiona a PopUp.
  *
  * <p>Redefine as dimens�es externas da PopUp, ou seja, contando a barra de
  * t�tulo.</p>
  * @param integer piHeight Nova altura
  * @param integer piWidth Nova largura
  * @param boolean pbCentralize Sse for true, centraliza a PopUp
  *****************************************************************************/
  self.resize = function(piHeight,piWidth,pbCentralize){
    var miClientHeight = piHeight - (browser.isIE?0:2);
    var miClientWidth = piWidth - 2;
    if(pbCentralize){
      var moBody = document.getElementsByTagName('body').item(0);
      var miTop = (moBody.clientHeight - piHeight)/2;
      var miLeft = (moBody.clientWidth - piWidth)/2;
      self.setPosition(miTop,miLeft);
    }
    // popup
    self.coPopUp.style.height = piHeight;
    self.coPopUp.style.width = piWidth;
    // div onde fica todo conte�do da popup
    self.coContent.style.height = miClientHeight;
    self.coContent.style.width = miClientWidth;
    // iframe escondido atras de tudo (para contornar o bug do z-index dos selects do IE)
    self.coIframeTitle.style.width = piWidth;
    self.coIframeTitle.style.height = miClientHeight;
    // close button
    self.coCloseButton.style.top = 2;
    self.coCloseButton.style.left = piWidth - 22;
    // title
    self.coTitle.style.width = miClientWidth;
    // iframe
    self.coIframe.style.height = piHeight - self.ciTitleHeight - 1;
    self.coIframe.style.width = miClientWidth;
    // vidro
    self.coGlass.setSize(piWidth,piHeight - self.ciTitleHeight);
  }

 /*****************************************************************************
  * M�todo chamado quando termina o carregamento de uma p�gina em background.
  *
  * <p>M�todo que deve ser chamado quando termina o carregamento de uma p�gina
  * em background. Redimensiona, centraliza, oculta o conte�do HTML embutido e
  * exibe o iframe.</p>
  *****************************************************************************/
  self.finishLoad = function(){
    if(self.coContentHtml){
      var moDialog = self.getWindow().gebi('dialog');
      var miClientHeight = parseInt(moDialog.style.height);
      var miClientWidth = parseInt(moDialog.style.width);
      var miHeight = miClientHeight + self.ciTitleHeight;
      var miWidth = miClientWidth + 2;
      if(!browser.isIE){
        miHeight+= 2;
        miWidth+= 2;
      }
      self.coContentHtml.style.display = 'none';
      self.coIframe.style.display = 'block';
      self.resize(miHeight,miWidth,true);
      soPopUpManager.getRootWindow().hideLoading();
    }
  }
  
 /*****************************************************************************
  * Exibe o bot�o de fechar da PopUp.
  *
  * <p>Exibe o bot�o de fechar da PopUp.</p>
  *****************************************************************************/
  self.showCloseButton = function(){
    self.coCloseButton.style.display = 'block';
  }
  
 /*****************************************************************************
  * Esconde o bot�o de fechar da PopUp.
  *
  * <p>Esconde o bot�o de fechar da PopUp.</p>
  *****************************************************************************/
  self.hideCloseButton = function(){
    self.coCloseButton.style.display = 'none';
  }
  
}

/*******************************************************************************
 * Classe FWDPopUpList. Implementa a lista de PopUps abertas.
 * 
 * <p>Implementa a lista de PopUps abertas.</p>
 *
 * @package javascript
 *******************************************************************************/
function FWDPopUpList(){

 /**
  * Refer�ncia para si mesmo.
  * @var FWDPopUpList
  */
  var self = this;

 /**
  * Array associativo de PopUps indexadas pelo seu z-index
  * @var Array
  */
  self.caList = new Array();

 /**
  * Valor m�nimo que o z-index de uma PopUp pode ter.
  * @var integer
  */
  self.ciMinZIndex = 1000;

 /**
  * Valor do z-index da PopUp da frente.
  * @var integer
  */
  self.ciMaxZIndex = self.ciMinZIndex;

 /******************************************************************************
  * Adiciona uma PopUp � lista
  *
  * <p>Adiciona uma PopUp � lista. A PopUp adicionada fica na frente das j�
  * existentes.</p>
  * 
  * @param FWDPopUp poPopUp PopUp a ser adicionada � lista
  *****************************************************************************/
  self.addPopUp = function(poPopUp){
    self.ciMaxZIndex+=3;
    var miZIndex = self.ciMaxZIndex;
    poPopUp.setZIndex(miZIndex);
    self.caList[miZIndex] = poPopUp;
    poPopUp = null;
  }

 /******************************************************************************
  * Remove uma PopUp da lista
  *
  * <p>Remove uma PopUp da lista. As PopUps que estavam a frente da PopUp
  * removida s�o deslocadas para tr�s para preencher o espa�o deixado por ela.</p>
  * 
  * @param FWDPopUp poPopUp PopUp a ser removida da lista
  *****************************************************************************/
  self.removePopUp = function(poPopUp){
    for(var i=poPopUp.getZIndex();i<self.ciMaxZIndex;i+=3){
      self.caList[i] = self.caList[i+3];
      self.caList[i].setZIndex(i);
    }
    delete self.caList[self.ciMaxZIndex];
    self.ciMaxZIndex-=3;
    poPopUp = null;
  }

 /******************************************************************************
  * Traz uma PopUp da lista para frente
  *
  * <p>Traz uma PopUp da lista para frente, deslocando as PopUps que estavam �
  * sua frente para tr�s para preencher o espa�o deixado pela mesma.</p>
  * 
  * @param FWDPopUp poPopUp PopUp a ser trazida para frente
  *****************************************************************************/
  self.bringToFront = function(poPopUp){
    if(poPopUp.getZIndex()<self.ciMaxZIndex){
      self.removePopUp(poPopUp);
      self.addPopUp(poPopUp);
    }
    poPopUp = null;
  }
  
 /******************************************************************************
  * Retorna o n�mero de PopUps na lista
  *
  * <p>Retorna o n�mero de PopUps na lista.</p>
  * 
  * @return integer N�mero de PopUps na lista
  *****************************************************************************/
  self.getLength = function(){
    return (self.ciMaxZIndex-self.ciMinZIndex)/3;
  }
  
  
 /******************************************************************************
  * Retorna a PopUp da frente
  *
  * <p>Retorna a PopUp da frente.</p>
  * 
  * @return FWDPopUp PopUp da frente
  *****************************************************************************/
  self.getFirst = function(){
    return self.caList[self.ciMaxZIndex];
  }

}

/*******************************************************************************
 * Classe FWDGlass. Implementa um "vidro" que captura eventos de mouse.
 * 
 * <p>Implementa um "vidro" que fica na frente de outros componentes e captura
 * eventos de mouse.</p>
 *
 * @package javascript
 * @param String psColor Cor do vidro
 * @param integer piOpacity Percentual de opacidade do vidro (0 pra transparente)
 * @param integer piHeight Altura do vidro (opcional)
 * @param integer piWidth Largura do vidro (opcional)
 * OBS1: Se piHeight e piWidth n�o forem passados, o vidro tem o tamanho da tela
 * OBS2: Quando um vidro � criado, a rolagem do body � desabilitada
 *******************************************************************************/
function FWDGlass(psColor,piOpacity,piHeight,piWidth){
  var self = this;
  
 /**
  * Indica se o vidro j� foi adicionado ao corpo da p�gina. Quando o vidro �
  * criado antes da p�gina carregar (dentro do head), ele s� � adicionado �
  * p�gina quando o m�todo show � chamado pela 1� vez.
  * @var boolean
  */
  self.cbAppended = false;

 /**
  * Div que representa o vidro
  * @var HTMLDivElement
  */
  self.coGlass = document.createElement('DIV');

  self.coGlass.style.display = 'none';
  self.coGlass.style.position = 'absolute';
  self.coGlass.style.top = '0px';
  self.coGlass.style.left = '0px';
  self.coGlass.style.background = psColor;
  self.coGlass.style.width = (piWidth?piWidth:screen.width);
  self.coGlass.style.height = (piHeight?piHeight:screen.height);
  if(browser.isIE){
    self.coGlass.style.filter = 'Alpha(opacity='+piOpacity+')';
  }else{
    self.coGlass.style.opacity = piOpacity/100;
  }
  var moBody = document.getElementsByTagName('body').item(0);
  if(moBody){
    moBody.insertBefore(self.coGlass,gebi('form_fwd'));
    self.cbAppended = true;
  }

  // derreferencia os objetos locais
  psColor = moBody = null;

 /*****************************************************************************
  * Seta o tamanho do vidro
  *
  * <p>Seta o tamanho do vidro.</p>
  * 
  * @param integer piWidth Nova largura do vidro
  * @param integer piHeight Nova altura do vidro
  *****************************************************************************/
  self.setSize = function(piWidth,piHeight){
    self.coGlass.style.width = piWidth;
    self.coGlass.style.height = piHeight;
  }

 /*****************************************************************************
  * Seta o z-index do vidro
  *
  * <p>Seta o z-index do vidro.</p>
  * 
  * @param integer piZIndex Novo z-index do vidro
  *****************************************************************************/
  self.setZIndex = function(piZIndex){
    self.coGlass.style.zIndex = piZIndex;
  }

 /*****************************************************************************
  * Exibe o vidro
  *
  * <p>Exibe (ativa) o vidro.</p>
  * 
  * @param integer piZIndex z-index onde o vidro vai se posicionar
  * @param integer piLeft Coordenada x onde o vidro vai se posicionar (opcional)
  * @param integer piTop Coordenada y onde o vidro vai se posicionar (opcional)
  * OBS: Se piLeft e piTop n�o forem passados, o vidro � exibido onde estiver
  * (a posi��o inicial � (0,0)).
  *****************************************************************************/
  self.show = function(piZIndex,piLeft,piTop){
    var moBody = document.getElementsByTagName('body').item(0);
    if(moBody){
      if(!self.cbAppended){
        moBody.insertBefore(self.coGlass,gebi('form_fwd'));
        self.cbAppended = true;
      }
      self.coGlass.style.zIndex = piZIndex;
      self.coGlass.style.display = 'block';
      self.coGlass.style.visibility = 'visible';
      if(piLeft) self.coGlass.style.left = piLeft;
      if(piTop) self.coGlass.style.top = piTop;
    }
    moBody = null;
  }

 /*****************************************************************************
  * Posiciona o vidro em rela��o ao id do objeto passado por parametro
  *
  * <p>Fixa o vidro para aparecer em cima do objeto indicado pelo id.</p>
  * 
  * @param string psIdElemen Id do elemento alvo do vidro
  *****************************************************************************/
  self.atachToElement = function(psIdElemen){
  	moTarget = gebi(psIdElemen);
	//algura e largura do objeto alvo do vidro
	var miWidth = parseInt(moTarget.style.width);
	var miHeight = parseInt(moTarget.style.height);
	self.setSize(miWidth,miHeight);
	
	//calcula a posi��o absoluta do objeto alvo do vidro
  	var miLeft = 0;
    var miTop = 0;
    if(moTarget.style.left)
  	  miLeft = parseInt(moTarget.style.left);
  	if(moTarget.style.top)
  	   miTop = parseInt(moTarget.style.top);
	while(moTarget.parentNode){
		moTarget = moTarget.parentNode;
		if((moTarget.style)){
		  	if(parseInt(moTarget.style.left))
		  		miLeft += parseInt(moTarget.style.left);
	  		if(parseInt(moTarget.style.top))
	  			miTop += parseInt(moTarget.style.top);
	  	}
	}
	self.coGlass.style.left = parseInt(miLeft) +1+'px';
    self.coGlass.style.top = parseInt(miTop) +1+'px';
    
	//IE bug: diminui o tamanho da fonte para dentro da div para funcionar a altura setada     
    self.coGlass.style.fontSize = '4pt';
  }

 /*****************************************************************************
  * Oculta o vidro
  *
  * <p>Oculta (desativa) o vidro.</p>
  *****************************************************************************/
  self.hide = function(){
	self.coGlass.style.display = 'none';
	self.coGlass.style.visibility = 'hidden';
  }

 /*****************************************************************************
  * Destr�i o vidro
  *
  * <p>Destr�i o vidro.</p>
  *****************************************************************************/
  self.destroy = function(){
    self.coGlass.parentNode.removeChild(self.coGlass);
    self = null;
  }

}

/*******************************************************************************
 * Classe FWDPopUpManager. Implementa o gerenciador de PopUps.
 * 
 * <p>Implementa o gerenciador de PopUps. S� deve haver uma int�ncia do
 * gerenciador, ela deve ser global e se chamar soPopUpManager. Se o construtor
 * � chamado de dentro de uma PopUp, a inst�ncia do pai � retornada.</p>
 *
 * @package javascript
 *******************************************************************************/
function FWDPopUpManager(){
  // Se estiver dentro de uma PopUp, retorna a inst�ncia do pai
  var moParent = window;
  var moChild = null;
  while(moChild!=moParent){
    moChild = moParent;
    moParent = moParent.parent;
  }
  if(moParent.soPopUpManager) return moParent.soPopUpManager;
  
 /**
  * Refer�ncia para si mesmo.
  * @var FWDPopUpManager
  */
  var self = this;

 /**
  * Janela raiz (onde fica a �nica inst�ncia do gerenciador de PopUps e todas as PopUps)
  * @var Window
  */
  self.coRootWindow = moParent;

 /**
  * Vidro que bloqueia tudo que est� atr�s de uma PopUp modal.
  * @var FWDGlass
  */
  self.coBackGlass = new FWDGlass('black',70);

 /**
  * Vidro ativado durante uma opera��o de arrastar PopUp, na frente de tudo,
  * para capturar o evento de mousemove.
  * @var FWDGlass
  */
  self.coFrontGlass = new FWDGlass('blue',0);

 /**
  * Lista dos valores de z-index onde, logicamente, existe um backGlass (atr�s
  * de cada PopUp modal).
  * @var Array
  */
  self.caBackGlassHistory = new Array();

 /**
  * Lista contendo todas PopUps existentes
  * @var FWDPopUpList
  */
  self.coPopUpList = new FWDPopUpList();

 /**
  * PopUp ativa (a mais da frente)
  * @var FWDPopUp
  */
  self.coFocused = null;

  // derreferencia os objetos locais
  moParent = moChild = null;

 /*****************************************************************************
  * Exibe um backGlass no z-index indicado
  *
  * <p>Exibe um backGlass no z-index indicado.</p>
  * 
  * @param integer piZIndex z-index onde o backGlass vai se posicionar
  *****************************************************************************/
  self.showGlass = function(piZIndex){
    self.coBackGlass.show(piZIndex);
    self.caBackGlassHistory.push(piZIndex);
  }
  
 /*****************************************************************************
  * Oculta o backGlass mais da frente
  *
  * <p>Oculta o backGlass mais da frente.</p>
  * @param integer piZIndex z-index do vidro a ser escondido
  *****************************************************************************/
  self.hideGlass = function(piZIndex){
    var miPosition = arrayIndexOf(self.caBackGlassHistory,piZIndex);
    if(miPosition>-1){
      self.caBackGlassHistory.splice(miPosition,1);
    }
    if(self.caBackGlassHistory.length==0){
      self.coBackGlass.hide();
    }else{
      var miZIndex = self.caBackGlassHistory[self.caBackGlassHistory.length-1];
      self.coBackGlass.setZIndex(miZIndex);
    }
  }
  
 /*****************************************************************************
  * D� foco a uma PopUp
  *
  * <p>D� foco a uma PopUp.</p>
  * @param mixed pmPopUp FWDPopUp que deve receber o foco ou seu id
  *****************************************************************************/
  self.setFocus = function(pmPopUp){
    var moPopUp;
    if(typeof(pmPopUp)=='string'){
      moPopUp = self.getPopUpById(pmPopUp);
    }else{
      moPopUp = pmPopUp;
    }
    var moOldPopUp = self.coFocused;
    if(moOldPopUp!=null && moOldPopUp!=moPopUp){
      self.coPopUpList.bringToFront(moPopUp);
      moOldPopUp.blur();
    }
    moPopUp.focus();
    self.coFocused = moPopUp;
    moOldPopUp = moPopUp = pmPopUp = null;
  }
  
 /*******************************************************************************
  * Abre uma nova PopUp.
  * 
  * <p>Abre uma nova PopUp.</p>
  *
  * @param String psPopUpId Id da PopUp
  * @param String psSrc URL da p�gina a ser aberta na PopUp
  * @param mixed pmTitle String ou elemento HTML que ser� o t�tulo da PopUp
  * @param integer pbModal Indica se a PopUp � modal ou n�o
  * @param integer piHeight Altura da PopUp
  * @param integer piWidth Largura da PopUp
  * @param integer piTop Top da PopUp (opcional)
  * @param integer piLeft Left da PopUp (opcional)
  * OBS1: Se piTop e piLeft n�o forem passados, a PopUp � criada centralizada.
  * OBS2: Se j� existir uma PopUp com o mesmo id, ela � substitu�da pela nova.
  *******************************************************************************/
  self.openPopUp = function(psPopUpId,psSrc,pmTitle,pbModal,piHeight,piWidth,piTop,piLeft,moWindow){
    var moPopUp = gebi(psPopUpId);
    if(moPopUp) self.closePopUp(psPopUpId);
    moPopUp = new FWDPopUp(psPopUpId,psSrc,pmTitle,piHeight,piWidth,piTop,piLeft);
    self.coPopUpList.addPopUp(moPopUp);
    // Seta o evento do titulo para poder mover a popup
    moPopUp.coTitle.onmousedown = function(poEvent){
      var moPopUp,moEvent;
      if(poEvent){
        moEvent = poEvent;
        moPopUp = moEvent.currentTarget.parentNode.parentNode.coPopUp;
        poEvent = null;
      }else{
        moEvent = window.event;
        moPopUp = moEvent.srcElement.parentNode.parentNode.coPopUp;
      }
      soPopUpManager.setFocus(moPopUp);
      soPopUpManager.coRootWindow.dragStart(moEvent,moPopUp.coPopUp.id);
      soPopUpManager.coFrontGlass.show(moPopUp.getZIndex()+1);
      FWDEventManager.addEvent(document, 'mouseup', function(e){dragStop(e);self.coFrontGlass.hide();});
      moPopUp = moEvent = null;
    };
    // Seta o evento do bot�o de fechar da popup
    moPopUp.coCloseButton.onclick = function(poEvent){
      var msPopUpId;
      if(poEvent){
        msPopUpId = poEvent.currentTarget.parentNode.id;
        poEvent = null;
      }else{
        msPopUpId = window.event.srcElement.parentNode.id;
      }
      self.closePopUp(msPopUpId);
      msPopUpId = null;
    };
    // Faz com que a div do vidro enxerge a popup
    moPopUp.coGlass.coGlass.coPopUp = moPopUp;
    // Seta o evento de dar foco quando se clica no vidro da popup
    moPopUp.coGlass.coGlass.onmousedown = function(poEvent){
      var moPopUp;
      if(poEvent){
        moPopUp = poEvent.currentTarget.coPopUp;
        poEvent = null;
      }else{
        moPopUp = window.event.srcElement.coPopUp;
      }
      soPopUpManager.setFocus(moPopUp);
      moPopUp = null;
    };
    self.setFocus(moPopUp);
    if(pbModal=='true'){
      self.showGlass(moPopUp.getZIndex()-1);
      moPopUp.cbModal = true;
    }
    psPopUpId = moPopUp = psSrc = pmTitle = moWindow = moPopUp = null;
  }

 /*******************************************************************************
  * Fecha uma PopUp.
  * 
  * <p>Fecha (destr�i) uma PopUp.</p>
  *
  * @param String psPopUpId Id da PopUp a ser fechada.
  *******************************************************************************/
  self.closePopUp = function(psPopUpId){
    var moPopUp = gebi(psPopUpId);
    var moNewFocused;
    if(!moPopUp || !moPopUp.coPopUp){
      alert("Error: Trying to close inexistent PopUp '"+psPopUpId+"'.");
    }else{
      moPopUp = moPopUp.coPopUp;
      var miZIndex = moPopUp.getZIndex();
      self.coFocused = null;
      self.coPopUpList.removePopUp(moPopUp);
      if(self.caBackGlassHistory.length){
        for(var i=self.caBackGlassHistory.length-1;i>=0;i--){
          if(self.caBackGlassHistory[i] > miZIndex){
            self.caBackGlassHistory[i]-= 3;
          }else{
            break;
          }
        }
      }
      self.hideGlass(miZIndex-1);
      moPopUp.destroy();
      moNewFocused = self.coPopUpList.getFirst(); 
      if(moNewFocused){
        self.setFocus(moNewFocused);
      }
    }
    psPopUpId = moPopUp = moNewFocused = null;
  }
  
 /*******************************************************************************
  * Retorna uma PopUp a partir de seu id.
  * 
  * <p>Retorna uma PopUp a partir de seu id.</p>
  *
  * @param String psPopUpId Id da PopUp a ser fechada.
  * @return FWDPopUp Objeto PopUp
  *******************************************************************************/
  self.getPopUpById = function(psPopUpId){
    var moPopUp = gebi(psPopUpId);
    if(!moPopUp || !moPopUp.coPopUp){
      return null;
    }else{
      moPopUp = null;
      return gebi(psPopUpId).coPopUp;
    }
  }
  
 /*******************************************************************************
  * Retorna a janela principal.
  * 
  * <p>Retorna a janela principal.</p>
  *
  * @return Object Janela principal
  *******************************************************************************/
  self.getRootWindow = function(){
    return self.coRootWindow;
  }

}

/**
 * Inst�ncia �nica da classe FWDPopUpManager.
 * @var FWDPopUpManager
 */
var soPopUpManager = new FWDPopUpManager();

/*******************************************************************************
 * Abre uma PopUp.
 * 
 * <p>Abre (cria) uma PopUp.</p>
 *
 * @package javascript
 * @param String psPopUpId Id da PopUp
 * @param String psSrc URL da p�gina a ser aberta na PopUp
 * @param String psTitle String ou id do elemento HTML que ser� o t�tulo da PopUp
 * @param integer pbModal Indica se a PopUp � modal ou n�o
 * @param integer piHeight Altura da PopUp
 * @param integer piWidth Largura da PopUp
 * @param integer piTop Top da PopUp (opcional)
 * @param integer piLeft Left da PopUp (opcional)
 * OBS: Se piTop e piLeft n�o forem passados, a PopUp � criada centralizada
 *******************************************************************************/
function js_open_popup(psPopUpId,psSrc,psTitle,pbModal,piHeight,piWidth,piTop,piLeft){
  var moTitle = gebi(psTitle);
  var mmTitle = (moTitle?moTitle:psTitle);
  soPopUpManager.openPopUp(psPopUpId,psSrc,mmTitle,pbModal,piHeight,piWidth,window,piTop,piLeft);
  psPopUpId = psSrc = psTitle = moTitle = mmTitle = null;
};