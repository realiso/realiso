/**
 * FWD - FAST WEB DEVELOPMENT
 * 
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

function FWDMenu() {  
  this.value; 
}

/*******************************************************************************
 * Classe FWDMenuManager. Classe que gerencia os menus de contexto atualmente instanciados no sistema.
 * 
 * <p>Classe que gerencia os menus de contexto atualmente instanciados no sistema.</p>
 * @package FWD
 *******************************************************************************/
function FWDMenuManager(){

  /** @var Object - window corrente. */
  var moParent = window;
  /** @var Object window filha da window corrente. */
  var moChild = null;
  //busca pela inst�ncia �nica do gerenciador de menus
  while(moChild!=moParent){
    moChild = moParent;
    moParent = moParent.parent;
  }
  if(moParent.soMenuManager){
    moChild = null;
    return moParent.soMenuManager;
  }
  /** @var Object referencia para o objeto. */
  var self = this;

  /** @var Object Integer - contador do numero de menus que foram criados no sistema. */
  self.count = 0;

  /** @var Object array associativo dos menus de contexto. */
  self.maMenus = {};
	/*****************************************************************************
	 * Adiciona um menu no array de menus do gerenciador de menus
	 *
	 * <p>Adiciona um menu no array de menus do gerenciador de menus.</p>
	 * 
	 * @param string id Identificador do menu de contexto atual (o menu que disparou este evento)
	 *****************************************************************************/
  self.addMenu = function(idMenu,poWindow){
  	 if((idMenu)&&(poWindow)){
  	     self.count = self.count + 1;
		 self.maMenus[idMenu+'_'+self.count] = new Array();
		 self.maMenus[idMenu+'_'+self.count][0]=poWindow;
		 self.maMenus[idMenu+'_'+self.count][1]=idMenu;
	 }
  }
	/*****************************************************************************
	 * Esconde todos os menus de contexto exceto o menu que disparou o evento
	 *
	 * <p>Esconde todos os menus de contexto presentes na tela. utilizado para que seja exibido
	 * somente um menu de contexto na tela de cada vez.</p>
	 * 
	 * @param string id Identificador do menu de contexto atual (o menu que disparou este evento)
	 *****************************************************************************/
  self.hideMenus = function(pbExecByTab){
	    for (id_menu in self.maMenus){
			  if(self.maMenus[id_menu]!=null){
				  try{
					var moWindowMenu = self.maMenus[id_menu][0];
		            var moMenu = moWindowMenu.document.getElementById(self.maMenus[id_menu][1]);
		            moMenu.style.display = 'none';
		            moMenu = moWindowMenu = null;
				  }catch(e){
					 e=null;
					 self.maMenus[id_menu][0]=null;
					 self.maMenus[id_menu][1]=null;
					 self.maMenus[id_menu]=null;
				  }	
			  }
		}
  }
}  

/*****************************************************************************
 * Exibe o menu de contexto
 *
 * <p>Exibe o menu de contexto. Faz o calculo da possi��o da tela que o menu deve ser exibido</p>
 * 
 * @param string id Identificador do menu de contexto que deve ser exibido
 * @param string event Evento de mouse que disparou este evento, utilizado para obter-se a posi��o
 * 					   X e Y na tela e reposicionar adequadamente o menu de contexto
 *****************************************************************************/
function js_show_menu(id,event){
  if(!event) event = window.event;
  var moObj = (gebi(id)).parentNode;
  var auxX=0;
  var auxY=0;
  
  function findPosX(obj) {
    var curleft = 0;
    if(obj.offsetParent)
        while(1) {
          curleft += obj.offsetLeft;
          if(!obj.offsetParent)
            break;
          obj = obj.offsetParent;
        }
    else if(obj.x)
        curleft += obj.x;
    return curleft;
  }

  function findPosY(obj) {
    var curtop = 0;
    if(obj.offsetParent)
        while(1) {
          curtop += obj.offsetTop;
          if(!obj.offsetParent)
            break;
          obj = obj.offsetParent;
        }
    else if(obj.y)
        curtop += obj.y;
    return curtop;
  }
  
  if ((moObj.style.height<1)&&(moObj.style.height<1)&&(moObj.parentNode))
  		moObj = moObj.parentNode;
  
  var absolute_x = parseInt((event.clientX?event.clientX:event.x));
  var absolute_y = parseInt((event.clientY?event.clientY:event.y));
  
  var parent_left = findPosX(moObj);
  var parent_top = findPosY(moObj);
  
  var relative_x = absolute_x - parent_left;
  var relative_y = absolute_y - parent_top;
  
  var parent_w = parseInt(moObj.style.width);
  var parent_h = parseInt(moObj.style.height);
  
  var menu_w = parseInt((gebi(id)).style.width);
  var menu_h = parseInt((gebi(id)).style.height);
  
  while(moObj.parentNode != null){
    if(parseInt(moObj.style.left) > 0) auxX = auxX + parseInt(moObj.style.left);
    if(parseInt(moObj.style.top) > 0) auxY = auxY + parseInt(moObj.style.top);
    if(parseInt(moObj.scrollLeft) > 0) auxX = auxX - parseInt(moObj.scrollLeft);
    if(parseInt(moObj.scrollTop) > 0) auxY = auxY - parseInt(moObj.scrollTop);
    moObj = moObj.parentNode;
  }
  
  var x = parseInt((event.clientX?event.clientX:event.x)) - auxX;
  var y = parseInt((event.clientY?event.clientY:event.y)) - auxY;
  
  
  var max_left = parent_w - menu_w - 10;
  if (x > max_left) x=max_left;
  var max_top = parent_h - menu_h - 10;
  if (y > max_top) y=max_top;

  /*
  if ( parent_w - relative_x < menu_w+10 )	// 10px: right shadow
    x -= (menu_w+10);
  if ( parent_h - relative_y < menu_h+10 )	// 10px: bottom shadow
    y = y - (menu_h+10) + (parent_h - relative_y);
  */
  gebi(id).style.left = x;
  gebi(id).style.top = y;
  
  if(parseInt(gebi(id).style.height)>10)
  	js_show(id);
  return true;
}

/*****************************************************************************
 * Exibe o valor gravado no input hidden do menu
 *
 * <p>Exibe o valor gravado no input hidden do menu de contexto. Este valor � "gravado" no input 
 *	hidden quando o menu � acionado sobre um elemento que possui o atributo "actionTarget"</p>
 * 
 * @param string id Identificador do menu correspondente ao input hidden quem deve ser exibido
 *****************************************************************************/
function js_show_menu_action_target(id){
  var moInputMenu = gebi(id+'_action_target');
  if(moInputMenu) alert(moInputMenu.value);
}

function js_show_grid_input_value(id){
  var moInputGrid = gebi(id+'_value');
  if(moInputGrid) alert(moInputGrid.value);
}

/*****************************************************************************
 * Esconte o menu de contexto
 *
 * <p>Esconde o menu de contexto</p>
 * 
 * @param string id Identificador do menu que deve ser escondido
 *****************************************************************************/
function js_hide_menu(id,event){
	soMenuManager.hideMenus();
}


//instancia �nica do gerenciador de menus
soMenuManager = new FWDMenuManager();
