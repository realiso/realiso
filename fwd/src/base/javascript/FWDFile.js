/**
 * FWD - FAST WEB DEVELOPMENT
 * 
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/*******************************************************************************
 * Classe FWDFile. Classe com as fun��es utilizadas pelo FWDFile.
 * 
 * <p>Classe com as fun��es utilizadas pelo FWDFile.</p>
 * @package FWD
 * @param String psName Nome do elemento
 *******************************************************************************/
function FWDFile(psName){

 /**
  * Refer�ncia para si mesmo
  * @var FWDFile
  * @access private
  */
  var self = this;

 /**
  * Nome do elemento
  * @var String
  * @access private
  */
  var csName = psName;

 /**
  * Elemento file correspondente
  * @var HTMLElement
  * @access private
  */
  var coElement = gebi(psName);
  
  coElement.object = self;
  
 /*****************************************************************************
  * Faz o upload do arquivo sem dar refresh na p�gina
  *
  * <p>Faz o upload do arquivo sem dar refresh na p�gina</p>
  *****************************************************************************/
  self.sendAsynch = function(){
    var moForm = gebi('form_fwd');
    moForm.target = csName+'_iframe';
    js_submit(coElement);
    moForm.target = '';
    moForm = null;
  }

};