<?php

/*********************************************************************
 *
 *             $HeadURL: syslog.php $
 * $LastChangedRevision: 1.1 $
 *             Language: PHP 4.x or higher
 *            Copyright: SysCo syst�mes de communication sa
 *         CreationDate: 2005-11-05
 *            CreatedBy: SysCo/al
 *     $LastChangedDate: 2005-12-24 $
 *       $LastChangedBy: SysCo/al $
 *              WebSite: http://developer.sysco.ch/php/
 *                Email: developer@sysco.ch
 *
 *
 * Description
 *
 *   The Syslog class is a syslog device implementation in PHP
 *   following the RFC 3164 rules.
 *
 *
 *   Severity values:
 *     0 Emergency: system is unusable
 *     1 Alert: action must be taken immediately
 *     2 Critical: critical conditions
 *     3 Error: error conditions
 *     4 Warning: warning conditions
 *     5 Notice: normal but significant condition (default value)
 *     6 Informational: informational messages
 *     7 Debug: debug-level messages
 *
 *
 * Usage
 *
 *   require_once('FWDSyslog.php');
 *   $syslog = new FWDSyslog($facility = 16, $severity = 5, $hostname = "", $fqdn= "", $ip_from = "", $process="", $content = "");
 *   $syslog->send($server = "", $content = "", $timeout = 0);
 *
 *
 * Examples
 *
 *   Example 1
 *     <?php
 *         require_once('FWDSyslog.php');
 *         $syslog = new FWDSyslog();
 *         $syslog->send('192.168.0.12', 'My first PHP syslog message');
 *     ?>
 *
 *   Example 2
 *     <?php
 *         require_once('FWDSyslog.php');
 *         $syslog = new FWDSyslog(23, 7, 'MYSERVER', 'myserver.mydomain.net', '192.168.0.1', 'webautomation');
 *         $syslog->send('192.168.0.12', 'My second PHP syslog message');
 *     ?>
 *
 *   Example 3
 *     <?php
 *         require_once('FWDSyslog.php');
 *         $syslog = new FWDSyslog();
 *         $syslog->setFacility(23);
 *         $syslog->setSeverity(7);
 *         $syslog->setHostname('MYSERVER');
 *         $syslog->setFqdn('myserver.mydomain.net');
 *         $syslog->setIpFrom('192.168.0.1');
 *         $syslog->setProcess('webautomation');
 *         $syslog->setContent('My third PHP syslog message');
 *         $syslog->setServer('192.168.0.12');
 *         $syslog->send();
 *     ?>
 *
 *   Example 4
 *     <?php
 *         // Do not follow the conventions of the RFC
 *         // and send a customized MSG part instead of
 *         // the recommanded format "process fqdn ip content"
 *         require_once('FWDSyslog.php');
 *         $syslog = new FWDSyslog();
 *         $syslog->setFacility(23);
 *         $syslog->setSeverity(7);
 *         $syslog->setHostname('MYSERVER');
 *         $syslog->setMsg('My customized MSG PHP syslog message');
 *         $syslog->setServer('192.168.0.12');
 *         $syslog->send();
 *     ?>
 *
 *
 * External file needed
 *
 *   none.
 *
 *
 * External file created
 *
 *   none.
 *
 *
 * Special issues
 *
 *   - Sockets support must be enabled.
 *     * In Linux and *nix environments, the extension is enabled at
 *       compile time using the --enable-sockets configure option
 *     * In Windows, PHP Sockets can be activated by un-commenting
 *       extension=php_sockets.dll in php.ini
 *
 *
 * Licence
 *
 *   Copyright (c) 2005, SysCo syst�mes de communication sa
 *   SysCo (tm) is a trademark of SysCo syst�mes de communication sa
 *   All rights reserved.
 *
 *   Redistribution and use in source and binary forms, with or without
 *   modification, are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *   - Neither the name of SysCo syst�mes de communication sa nor the names of its
 *     contributors may be used to endorse or promote products derived from this
 *     software without specific prior written permission.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 *   EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *   OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 *   SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 *   OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 *   TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *   EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * Change Log
 *
 *   2005-12-24 1.1 SysCo/al Generic release and documentation
 *   2005-11-05 1.0 SysCo/al Initial release
 *
 *********************************************************************/

class FWDSyslog
{

	/**
	 * Aplica��o do remetente do log
	 * Facility values:
	 * 0 kernel messages
	 * 1 user-level messages
	 * 2 mail system
	 * 3 system daemons
	 * 4 security/authorization messages
	 * 5 messages generated internally by syslogd
	 * 6 line printer subsystem
	 * 7 network news subsystem
	 * 8 UUCP subsystem
	 * 9 clock daemon
	 * 10 security/authorization messages
	 * 11 FTP daemon
	 * 12 NTP subsystem
	 * 13 log audit
	 * 14 log alert
	 * 15 clock daemon
	 * 16 local user 0 (local0) (valor padr�o)
	 * 17 local user 1 (local1)
	 * 18 local user 2 (local2)
	 * 19 local user 3 (local3)
	 * 20 local user 4 (local4)
	 * 21 local user 5 (local5)
	 * 22 local user 6 (local6)
	 * 23 local user 7 (local7)
	 * @var integer
	 * @access protected
	 */
	protected $ciFacility;

	/**
	 * Severidade do log
	 * Severity values:
	 * 0 Emergency: system is unusable
	 * 1 Alert: action must be taken immediately
	 * 2 Critical: critical conditions
	 * 3 Error: error conditions
	 * 4 Warning: warning conditions
	 * 5 Notice: normal but significant condition (valor padr�o)
	 * 6 Informational: informational messages
	 * 7 Debug: debug-level messages
	 * @var integer
	 * @access protected
	 */
	protected $ciSeverity;

	/**
	 * Hostname da aplica��o remetente, ex: myhost
	 * @var string
	 * @access protected
	 */
	protected $csHostname;

	/**
	 * Fully Qualified Domain Name, ex: myhost.mydomain.com
	 * @var string
	 * @access protected
	 */
	protected $csFQDN;

	/**
	 * Endere�o IP do Remetente
	 * @var string
	 * @access protected
	 */
	protected $csIpFrom;

	/**
	 * Nome do processo remetente
	 * @var string
	 * @access protected
	 */
	protected $csProcess;

	/**
	 * Conte�do da mensagem de log
	 * @var string
	 * @access protected
	 */
	protected $csContent;

	/**
	 * Definir mensagem completa a ser enviada, n�o obedecendo a RFC
	 * Se n�o definida ser�: "processo: FQDN IP conteudo"
	 * @var string
	 * @access protected
	 */
	protected $csMsg;

	/**
	 * IP do servidor syslog, valor padr�o: 127.0.0.1
	 * @var string
	 * @access protected
	 */
	protected $csServer;

	/**
	 * Porta do servidor syslog, valor padr�o: 514
	 * @var string
	 * @access protected
	 */
	protected $ciPort;

	/**
	 * Timeout da conex�o UDP (em segundos) , valor padr�o: 10
	 * @var string
	 * @access protected
	 */
	protected $ciTimeout;

	 

	function __construct($piFacility = 16, $piSeverity = 5, $psHostname = "", $psFQDN= "", $psIpFrom = "", $psProcess="", $psContent = "")
	{
		$this->csMsg      = '';
		$this->csServer   = '127.0.0.1';
		$this->ciPort     = 514;
		$this->ciTimeout  = 10;

		$this->ciFacility = $piFacility;

		$this->ciSeverity = $piSeverity;

		$this->csHostname = $psHostname;
		if ($this->csHostname == "")
		{
			if (isset($_ENV["COMPUTERNAME"]))
			{
				$this->csHostname = $_ENV["COMPUTERNAME"];
			}
			elseif (isset($_ENV["HOSTNAME"]))
			{
				$this->csHostname = $_ENV["HOSTNAME"];
			}
			else
			{
				$this->csHostname = "WEBSERVER";
			}
		}
		$this->csHostname = substr($this->csHostname, 0, strpos($this->csHostname.".", "."));

		$this->csFQDN = $psFQDN;
		if ($this->csFQDN == "")
		{
			if (isset($_SERVER["SERVER_NAME"]))
			{
				$this->csFQDN = $_SERVER["SERVER_NAME"];
			}
		}

		$this->csIpFrom = $psIpFrom;
		if ($this->csIpFrom == "")
		{
			if (isset($_SERVER["SERVER_ADDR"]))
			{
				$this->csIpFrom = $_SERVER["SERVER_ADDR"];
			}
		}

		$this->csProcess = $psProcess;
		if ($this->csProcess == "")
		{
			$this->csProcess = "FWD5";
		}

		$this->csContent = $psContent;
		if ($this->csContent == "")
		{
			$this->csContent = "FWD5 generated message";
		}

		FWDLanguage::getPHPStringValue('error_connection_syslog',"Could not connect to syslog server.");
	}

	function setFacility($piFacility)
	{
		$this->ciFacility = $piFacility;
	}


	function setSeverity($piSeverity)
	{
		$this->ciSeverity = $piSeverity;
	}


	function setHostname($psHostname)
	{
		$this->csHostname = $psHostname;
	}


	function setFqdn($psFQDN)
	{
		$this->csFQDN = $psFQDN;
	}


	function setIpFrom($psIpFrom)
	{
		$this->csIpFrom = $psIpFrom;
	}


	function setProcess($csProcess)
	{
		$this->csProcess = $csProcess;
	}


	function setContent($csContent)
	{
		$this->csContent = $csContent;
	}


	function setMsg($csMsg)
	{
		$this->csMsg = $csMsg;
	}


	function setServer($psServer)
	{
		$this->csServer = $psServer;
	}


	function setPort($piPort)
	{
		if ((intval($piPort) > 0) && (intval($piPort) < 65536))
		{
			$this->ciPort = intval($piPort);
		}
	}


	function setTimeout($piTimeout)
	{
		if (intval($piTimeout) > 0)
		{
			$this->ciTimeout = intval($piTimeout);
		}
	}


	function send($psServer = "", $psContent = "", $piTimeout = 0)
	{
		if ($psServer != "")
		{
			$this->csServer = $psServer;
		}

		if ($psContent != "")
		{
			$this->csContent = $psContent;
		}

		if (intval($piTimeout) > 0)
		{
			$this->ciTimeout = intval($piTimeout);
		}

		if ($this->ciFacility <  0) { $this->ciFacility =  0; }
		if ($this->ciFacility > 23) { $this->ciFacility = 23; }
		if ($this->ciSeverity <  0) { $this->ciSeverity =  0; }
		if ($this->ciSeverity >  7) { $this->ciSeverity =  7; }

		$this->csProcess = substr($this->csProcess, 0, 32);

		$miActualtime = time();
		$miMonth      = date("M", $miActualtime);
		$miDay        = substr("  ".date("j", $miActualtime), -2);
		$msHHMMSS     = date("H:i:s", $miActualtime);
		$miTimestamp  = $miMonth." ".$miDay." ".$msHHMMSS;

		$msPri    = "<".($this->ciFacility*8 + $this->ciSeverity).">";
		$msHeader = $miTimestamp." ".$this->csHostname;

		if ($this->csMsg != "")
		{
			$msMsg = $this->csMsg;
		}
		else
		{
			$msMsg = $this->csProcess.": ".$this->csFQDN." ".$this->csIpFrom." ".$this->csContent;
		}

		$msMessage = substr($msPri.$msHeader." ".$msMsg, 0, 1024);

		FWDWebLib::addKnownError('error_connection_syslog',array('fsockopen'));
		$fp = fsockopen("udp://".$this->csServer, $this->ciPort, $miErrno, $msErrstr);
		FWDWebLib::removeKnownError('error_connection_syslog');
		if($fp){
			fwrite($fp, $msMessage);
			fclose($fp);
			$msResult = $msMessage;
		}else{
			$msResult = "ERROR: $miErrno - $msErrstr";
		}
		return $msResult;
	}
}
?>