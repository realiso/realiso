<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDDefaultErrorHandler. Manipulador de erros default da FWD.
 *
 * <p>Manipulador de erros default da FWD.</p>
 * @package FWD5
 * @subpackage base
 */
class FWDDefaultErrorHandler implements FWDErrorHandler {

	/**
	 * Indica se o pr�ximo erro a ser encontrado ser� o primeiro
	 * @var boolean
	 * @access protected
	 */
	protected static $cbFirstError = true;

	/**
	 * Array associativo de erros conhecidos. Cada elemento � um array de
	 * strings que, se alguma delas for encontrada dentro de uma mensagem de erro,
	 * o handler de erros vai tratar o erro como um erro conhecido identificado
	 * pela chave desse elemento neste array.
	 * @var array
	 * @access protected
	 */
	protected static $caKnownErrors = array();

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDErrorHandler.</p>
	 * @access public
	 */
	public function __construct(){
		FWDLanguage::getPHPStringValue('error_db_connection',"Database connection error.");
		FWDLanguage::getPHPStringValue('error_configuration',"The configuration file is corrupt.");
		FWDLanguage::getPHPStringValue('error_smtp',"Failed to connect to mailserver.");
		FWDLanguage::getPHPStringValue('error_db_invalid_object_name',"Invalid database object name. Probably using wrong DB user.");
		FWDLanguage::getPHPStringValue('error_session_permission_denied',"Permission denied in session file.");
		FWDLanguage::getPHPStringValue('error_debug_permission_denied',"Permission denied in debug file.");
		FWDLanguage::getPHPStringValue('error_config_corrupt',"The configuration file is corrupt.");

		self::addKnownError(
      'error_db_connection',
		array(
        'function.mssql-connect',
        'function.mssql-select-db',
        'function.pg-connect',
        'function.ocilogon',
		)
		);
		self::addKnownError('error_configuration',array('The configuration file is corrupt'));
		self::addKnownError('error_smtp',array('function.mail'));
		self::addKnownError('error_session_permission_denied',array('function.session-start'));
		self::addKnownError('error_db_invalid_object_name',array('Invalid object name'));
		self::addKnownError('error_debug_permission_denied',array('debug.txt'));
		self::addKnownError('error_config_corrupt',array('error_config_corrupt'));
	}

	/**
	 * Error Handler da FWD5.
	 *
	 * <p>Error Handler da FWD5. Identifica o tipo de erro e levanta uma
	 * exce��o que ir� efetuar seu tratamento. Aborta se necess�rio.</p>
	 * @access public
	 * @param integer $piErrNo N�mero do erro
	 * @param string $psErrStr Mensagem de erro
	 * @param string $psErrFile Nome do arquivo que gerou o erro
	 * @param integer $piErrLine N�mero da linha em que ocorreu o erro
	 */
	public function handleError($piErrNo, $psErrStr, $psErrFile, $piErrLine){
		try{
			$exception = new FWDException($psErrStr,$piErrNo);
			$exception->setFile($psErrFile);
			$exception->setLine($piErrLine);
			throw $exception;
		}catch(FWDException $moException){
			self::handleException($moException);
		}
		if($piErrNo==E_USER_ERROR){
			$msOut = '';
			$msOut.= "<b>FWD Fatal error</b> in <strong>$psErrFile</strong> on line <strong>$piErrLine</strong>";
			$msOut.= ", PHP " . PHP_VERSION . " (" . PHP_OS . ")<br/>\n";
			$msOut.= "Aborting...<br/>\n";
			echo $msOut;
			die(1);
		}
	}

	/**
	 * Exception Handler da FWD5.
	 *
	 * <p>Exception Handler da FWD5. Captura a exce��o e gera um trace dos erros.</p>
	 * @access public
	 * @param FWDException $poFWDException Exce��o
	 * @static
	 */
	public static function handleException($poFWDException){
		switch($poFWDException->getCode()){
			case E_USER_ERROR:   $msError = "FWD ERROR";              break;
			case E_USER_WARNING: $msError = "FWD WARNING";            break;
			case E_USER_NOTICE:  $msError = "FWD NOTICE";             break;
			case E_STRICT:       return;                              break;
			default:             $msError = "FWD Unknown error type"; break;
		}

		$msExceptionMessage = $poFWDException->getMessage();

		foreach(self::$caKnownErrors as $msErrorId=>$maErrorSubstrs){
			foreach($maErrorSubstrs as $msErrorSubstr){
				if(strpos($msExceptionMessage,$msErrorSubstr)!==false){
					$msMessage = $msErrorId;
					echo "[FWD_ERROR]: $msMessage";
					exit();
				}
			}
		}

		if(self::$cbFirstError){
			echo self::dumpPost()."\n";
			self::$cbFirstError = false;
		}

		$msErrorMsg = "<b>FWD Exception Caught:</b> {$msError} [{$poFWDException->getCode()}]<br/>"
		."<br/>&nbsp;&nbsp;&nbsp; Message: {$poFWDException->getMessage()} <br/>"
		."<br/>&nbsp;&nbsp;&nbsp; File: {$poFWDException->getFile()} on line {$poFWDException->getLine()}<br/>";

		FWDWebLib::getInstance()->writeStringDebug($msErrorMsg,FWD_DEBUG_ERROR);

		echo $msErrorMsg;
		echo "<br/>TraceAsString:<br/><pre>";
		echo $poFWDException->getTraceAsString();
		echo "</pre><br/><br/>";
		return 1;
	}

	/**
	 * Adiciona um novo erro conhecido.
	 *
	 * <p>Adiciona um novo erro conhecido ao array de erros conhecidos.</p>
	 * @access public
	 * @param string $psErrorStringId Id do erro
	 * @param array $paMsgSubstrings Array de strings que caracterizam o erro
	 * @static
	 */
	public static function addKnownError($psErrorStringId,$paMsgSubstrings){
		self::$caKnownErrors[$psErrorStringId] = $paMsgSubstrings;
	}

	/**
	 * Remove um erro conhecido.
	 *
	 * <p>Remove um erro conhecido do array de erros conhecidos.</p>
	 * @access public
	 * @param string $psErrorStringId Id do erro
	 * @static
	 */
	public static function removeKnownError($psErrorStringId){
		unset(self::$caKnownErrors[$psErrorStringId]);
	}

	/**
	 * Retorna o conte�do do array $_POST codificado na forma de string
	 *
	 * <p>Retorna o conte�do do array $_POST codificado na forma de string</p>
	 * @access protected
	 * @return string Conte�do do array $_POST codificado na forma de string
	 */
	protected static function dumpPost(){
		return base64_encode(serialize($_POST));
	}

}

?>