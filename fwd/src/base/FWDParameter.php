<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDParameter.
 *
 * <p>Classe utilizada para passar par�metros por refer�ncia.</p>
 * @package FWD5
 * @subpackage base
 */
class FWDParameter {

	/**
	 * Valor do par�metro
	 * @var mixed
	 * @access protected
	 */
	protected $cmValue;

	/**
	 * Seta o valor do par�metro
	 *
	 * <p>Seta o valor do par�metro.</p>
	 * @access public
	 * @param mixed $pmValue Valor a ser atribu�do
	 */
	public function setValue($pmValue){
		$this->cmValue = $pmValue;
	}

	/**
	 * Retorna o valor do par�metro
	 *
	 * <p>Retorna o valor do par�metro.</p>
	 *
	 * @access public
	 * @return mixed Valor do par�metro
	 */
	public function getValue(){
		return $this->cmValue;
	}

}
?>