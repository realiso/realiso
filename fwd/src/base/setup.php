<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

if(!isset($base_ref)) {
	$base_ref = "";
}

include_once $base_ref."interfaces/FWDErrorHandler.php";

include_once $base_ref."base/FWDXmlLoad.php";
include_once $base_ref."base/FWDXmlStatic.php";
include_once $base_ref."base/FWDException.php";
include_once $base_ref."base/FWDLanguage.php";
include_once $base_ref."base/FWDDefaultErrorHandler.php";
include_once $base_ref."base/FWDDoNothingErrorHandler.php";
include_once $base_ref."base/FWDWebLib.php";

FWDWebLib::getInstance($base_ref);

ini_set( "error_reporting", E_ALL);

include_once $base_ref."base/FWDActivationHandler.php";
include_once $base_ref."base/FWDLicense.php";
include_once $base_ref."base/FWDXmlCompile.php";
include_once $base_ref."base/FWDColor.php";
include_once $base_ref."base/FWDACLSecurity.php";
include_once $base_ref."base/FWDMenuACLSecurity.php";
include_once $base_ref."base/FWDGridMenuACLSecurity.php";
include_once $base_ref."base/FWDParameter.php";
include_once $base_ref."base/FWDSyslog.php";
include_once $base_ref."base/FWDCrypt.php";
include_once $base_ref."base/FWDEmail.php";
include_once $base_ref."base/FWDActiveDirectory.php";
include_once $base_ref."base/FWDASaaS.php";

include_once $base_ref."base/phpchartdir.php";
include_once $base_ref."base/FWDSMTPConnectionTest.php";
include_once $base_ref."base/FWDSYSLogConnectionTest.php";

include_once $base_ref."interfaces/FWDPostValue.php";
include_once $base_ref."interfaces/QueryAutoCompleter.php";
include_once $base_ref."interfaces/FWDIDB.php";
include_once $base_ref."interfaces/FWDCollectable.php";

include_once $base_ref."abstracts/FWDDrawing.php";
include_once $base_ref."abstracts/FWDReportDrawing.php";
include_once $base_ref."abstracts/FWDView.php";
include_once $base_ref."abstracts/FWDContainer.php";
include_once $base_ref."abstracts/FWDItemController.php";
include_once $base_ref."abstracts/FWDRunnable.php";
include_once $base_ref."abstracts/FWDSession.php";
include_once $base_ref."abstracts/FWDViewGrid.php";
include_once $base_ref."abstracts/FWDGridAbstract.php";
include_once $base_ref."abstracts/FWDGridParameter.php";


/*
 * Foi feita uma vers�o light da adodb. Essa vers�o possui
 * somente os arquivos que est�o sendo utilizados atualmente
 * pela FWD. Caso seja necess�rio utilizar outras fun��es n�o
 * inclu�das na vers�o light, trocar os coment�rios da linha
 * abaixo para utilizar a vers�o completa da adodb ou ent�o
 * verificar quais os arquivos necess�rios e copi�-los para
 * vers�o light.
 */
//include_once $base_ref."db/adodb/adodb.inc.php";
include_once $base_ref."db/adodb_light/adodb.inc.php";
include_once $base_ref."db/adodb_light/drivers/adodb-mssql.inc.php";

include_once $base_ref."db/FWDDB.php";
include_once $base_ref."db/FWDDBTable.php";
include_once $base_ref."db/FWDDBType.php";
include_once $base_ref."db/FWDDBEntity.php";
include_once $base_ref."db/FWDDBDataBinder.php";
include_once $base_ref."db/FWDDBField.php";
include_once $base_ref."db/FWDDBFilter.php";
include_once $base_ref."db/FWDDBDataSet.php";
include_once $base_ref."db/FWDDBQueryHandler.php";

include_once $base_ref."event/FWDEvent.php";
include_once $base_ref."event/FWDTempEvent.php";
include_once $base_ref."event/FWDEventHandler.php";
include_once $base_ref."event/FWDAjaxEvent.php";
include_once $base_ref."event/FWDServerEvent.php";
include_once $base_ref."event/FWDClientEvent.php";
include_once $base_ref."event/FWDJsEvent.php";
include_once $base_ref."event/FWDMask.php";
include_once $base_ref."event/FWDMaskAutoComplete.php";
include_once $base_ref."event/FWDStartEvent.php";
include_once $base_ref."event/FWDToolTip.php";

include_once $base_ref."event/classEvent/FWDGridPopulate.php";
include_once $base_ref."event/classEvent/FWDGridChangePageChange.php";
include_once $base_ref."event/classEvent/FWDGridChangePageBack.php";
include_once $base_ref."event/classEvent/FWDGridChangePageFirst.php";
include_once $base_ref."event/classEvent/FWDGridChangePageLast.php";
include_once $base_ref."event/classEvent/FWDGridChangePageNext.php";
include_once $base_ref."event/classEvent/FWDGridRefresh.php";
include_once $base_ref."event/classEvent/FWDGridExport.php";
include_once $base_ref."event/classEvent/FWDTreeAjaxExpand.php";
include_once $base_ref."event/classEvent/FWDGridColumnOrder.php";
include_once $base_ref."event/FWDRequiredCheck.php";
include_once $base_ref."event/classEvent/FWDRequiredCheckAjax.php";
include_once $base_ref."event/classEvent/FWDGridUserEdit.php";
include_once $base_ref."event/classEvent/FWDGridDrawUserEdit.php";
include_once $base_ref."event/classEvent/FWDGridUserEditDefaultValues.php";
include_once $base_ref."event/classEvent/FWDGridCreatePopupEditPref.php";
include_once $base_ref."event/classEvent/FWDDBGridPackRefresh.php";

include_once $base_ref."view/FWDBox.php";
include_once $base_ref."view/FWDString.php";
include_once $base_ref."view/FWDVariable.php";

include_once $base_ref."view/viewgroup/FWDViewGroup.php";
include_once $base_ref."view/viewgroup/FWDDialog.php";
include_once $base_ref."view/viewgroup/FWDPanel.php";

include_once $base_ref."view/FWDText.php";
include_once $base_ref."view/FWDMemo.php";
include_once $base_ref."view/FWDButton.php";
include_once $base_ref."view/FWDCalendar.php";
include_once $base_ref."view/FWDIcon.php";
include_once $base_ref."view/FWDStatic.php";
include_once $base_ref."view/FWDWarning.php";
include_once $base_ref."view/FWDItem.php";
include_once $base_ref."view/FWDLink.php";
include_once $base_ref."view/FWDViewButton.php";
include_once $base_ref."view/FWDColorPicker.php";
include_once $base_ref."view/FWDAutocompleter.php";
include_once $base_ref."view/FWDAutolisting.php";
include_once $base_ref."view/FWDTreeBase.php";
include_once $base_ref."view/FWDTree.php";
include_once $base_ref."view/FWDTreeAjax.php";
include_once $base_ref."view/FWDMenu.php";
include_once $base_ref."view/FWDMenuItem.php";
include_once $base_ref."view/FWDMenuSeparator.php";
include_once $base_ref."view/FWDSelect.php";
include_once $base_ref."view/FWDSelector.php";
include_once $base_ref."view/FWDOptgroup.php";
include_once $base_ref."view/FWDController.php";
include_once $base_ref."view/FWDRadioBox.php";
include_once $base_ref."view/FWDCheckBox.php";
include_once $base_ref."view/FWDMessageBox.php";
include_once $base_ref."view/FWDTabGroup.php";
include_once $base_ref."view/FWDTabItem.php";
include_once $base_ref."view/FWDUserPreferences.php";
include_once $base_ref."view/FWDGrid.php";
include_once $base_ref."view/FWDColumn.php";
include_once $base_ref."view/FWDDrawGrid.php";
include_once $base_ref."view/FWDGetParameter.php";
include_once $base_ref."view/FWDSessionParameter.php";
include_once $base_ref."view/FWDNodeBuilder.php";
include_once $base_ref."view/FWDDBTreeBase.php";
include_once $base_ref."view/FWDDBTree.php";
include_once $base_ref."view/FWDSelectQuery.php";
include_once $base_ref."view/FWDFile.php";
include_once $base_ref."view/FWDScrolling.php";
include_once $base_ref."view/FWDChartDirector.php";
include_once $base_ref."view/FWDDiv.php";
include_once $base_ref."view/FWDIFrame.php";
include_once $base_ref."view/FWDHorizontalRule.php";
include_once $base_ref."view/FWDStaticGrid.php";
include_once $base_ref."view/FWDMemoStatic.php";
include_once $base_ref."view/FWDPack.php";
include_once $base_ref."view/FWDDBGridPack.php";
include_once $base_ref."view/FWDColorPicker.php";

include_once $base_ref."db/FWDDBSelect.php";
include_once $base_ref."db/FWDDBGrid.php";

include_once $base_ref."report/FWDReport.php";
include_once $base_ref."report/FWDReportLevel.php";
include_once $base_ref."report/FWDReportLevelIterator.php";
include_once $base_ref."report/FWDReportWriter.php";
include_once $base_ref."report/FWDReportLine.php";
include_once $base_ref."report/FWDReportCell.php";
include_once $base_ref."report/FWDReportStatic.php";
include_once $base_ref."report/FWDReportIcon.php";
include_once $base_ref."report/FWDReportGenerator.php";
include_once $base_ref."report/FWDReportITemplate.php";
include_once $base_ref."report/FWDReportClassificator.php";
include_once $base_ref."report/FWDReportFilter.php";
include_once $base_ref."report/FWDSuppressModel.php";
include_once $base_ref."report/FWDLiquidCellsSuppressModel.php";

include_once $base_ref."report/FWDReportHTMLWriter.php";
include_once $base_ref."report/FWDReportDocWriter.php";
include_once $base_ref."report/FWDReportPDFWriter.php";

include_once $base_ref."base/color.php";

include_once $base_ref."formats/excel/PHPExcel.php";
include_once $base_ref."formats/excel/PHPExcel/Autoloader.php";
include_once $base_ref."formats/excel/PHPExcel/CachedObjectStorageFactory.php";
include_once $base_ref."formats/excel/PHPExcel/Calculation.php";
include_once $base_ref."formats/excel/PHPExcel/Cell.php";
include_once $base_ref."formats/excel/PHPExcel/Comment.php";
include_once $base_ref."formats/excel/PHPExcel/DocumentProperties.php";
include_once $base_ref."formats/excel/PHPExcel/DocumentSecurity.php";
include_once $base_ref."formats/excel/PHPExcel/HashTable.php";
include_once $base_ref."formats/excel/PHPExcel/IComparable.php";
include_once $base_ref."formats/excel/PHPExcel/IOFactory.php";
include_once $base_ref."formats/excel/PHPExcel/NamedRange.php";
include_once $base_ref."formats/excel/PHPExcel/ReferenceHelper.php";
include_once $base_ref."formats/excel/PHPExcel/RichText.php";
include_once $base_ref."formats/excel/PHPExcel/Settings.php";
include_once $base_ref."formats/excel/PHPExcel/Style.php";
include_once $base_ref."formats/excel/PHPExcel/Worksheet.php";
include_once $base_ref."formats/excel/PHPExcel/WorksheetIterator.php";
include_once $base_ref."formats/excel/PHPExcel/CachedObjectStorage/APC.php";
include_once $base_ref."formats/excel/PHPExcel/CachedObjectStorage/CacheBase.php";
include_once $base_ref."formats/excel/PHPExcel/CachedObjectStorage/DiscISAM.php";
include_once $base_ref."formats/excel/PHPExcel/CachedObjectStorage/ICache.php";
include_once $base_ref."formats/excel/PHPExcel/CachedObjectStorage/Memcache.php";
include_once $base_ref."formats/excel/PHPExcel/CachedObjectStorage/Memory.php";
include_once $base_ref."formats/excel/PHPExcel/CachedObjectStorage/MemoryGZip.php";
include_once $base_ref."formats/excel/PHPExcel/CachedObjectStorage/MemorySerialized.php";
include_once $base_ref."formats/excel/PHPExcel/CachedObjectStorage/PHPTemp.php";
include_once $base_ref."formats/excel/PHPExcel/CachedObjectStorage/Wincache.php";
include_once $base_ref."formats/excel/PHPExcel/Calculation/Exception.php";
include_once $base_ref."formats/excel/PHPExcel/Calculation/ExceptionHandler.php";
include_once $base_ref."formats/excel/PHPExcel/Calculation/FormulaParser.php";
include_once $base_ref."formats/excel/PHPExcel/Calculation/FormulaToken.php";
include_once $base_ref."formats/excel/PHPExcel/Calculation/Function.php";
include_once $base_ref."formats/excel/PHPExcel/Calculation/Functions.php";
include_once $base_ref."formats/excel/PHPExcel/Cell/AdvancedValueBinder.php";
include_once $base_ref."formats/excel/PHPExcel/Cell/DataType.php";
include_once $base_ref."formats/excel/PHPExcel/Cell/DataValidation.php";
include_once $base_ref."formats/excel/PHPExcel/Cell/DefaultValueBinder.php";
include_once $base_ref."formats/excel/PHPExcel/Cell/Hyperlink.php";
include_once $base_ref."formats/excel/PHPExcel/Cell/IValueBinder.php";
include_once $base_ref."formats/excel/PHPExcel/Style/Alignment.php";
include_once $base_ref."formats/excel/PHPExcel/Style/Border.php";
include_once $base_ref."formats/excel/PHPExcel/Style/Borders.php";
include_once $base_ref."formats/excel/PHPExcel/Style/Color.php";
include_once $base_ref."formats/excel/PHPExcel/Style/Conditional.php";
include_once $base_ref."formats/excel/PHPExcel/Style/Fill.php";
include_once $base_ref."formats/excel/PHPExcel/Style/Font.php";
include_once $base_ref."formats/excel/PHPExcel/Style/NumberFormat.php";
include_once $base_ref."formats/excel/PHPExcel/Style/Protection.php";
include_once $base_ref."formats/excel/PHPExcel/Worksheet/BaseDrawing.php";
include_once $base_ref."formats/excel/PHPExcel/Worksheet/CellIterator.php";
include_once $base_ref."formats/excel/PHPExcel/Worksheet/ColumnDimension.php";
include_once $base_ref."formats/excel/PHPExcel/Worksheet/Drawing.php";
include_once $base_ref."formats/excel/PHPExcel/Worksheet/HeaderFooter.php";
include_once $base_ref."formats/excel/PHPExcel/Worksheet/HeaderFooterDrawing.php";
include_once $base_ref."formats/excel/PHPExcel/Worksheet/MemoryDrawing.php";
include_once $base_ref."formats/excel/PHPExcel/Worksheet/PageMargins.php";
include_once $base_ref."formats/excel/PHPExcel/Worksheet/PageSetup.php";
include_once $base_ref."formats/excel/PHPExcel/Worksheet/Protection.php";
include_once $base_ref."formats/excel/PHPExcel/Worksheet/Row.php";
include_once $base_ref."formats/excel/PHPExcel/Worksheet/RowDimension.php";
include_once $base_ref."formats/excel/PHPExcel/Worksheet/RowIterator.php";
include_once $base_ref."formats/excel/PHPExcel/Worksheet/SheetView.php";
include_once $base_ref."formats/excel/PHPExcel/Worksheet/Drawing/Shadow.php";
include_once $base_ref."formats/excel/PHPExcel/Writer/CSV.php";
include_once $base_ref."formats/excel/PHPExcel/Writer/Excel5.php";
include_once $base_ref."formats/excel/PHPExcel/Writer/Excel2007.php";
include_once $base_ref."formats/excel/PHPExcel/Writer/HTML.php";
include_once $base_ref."formats/excel/PHPExcel/Writer/IWriter.php";
include_once $base_ref."formats/excel/PHPExcel/Writer/PDF.php";
include_once $base_ref."formats/excel/PHPExcel/Writer/Serialized.php";
include_once $base_ref."formats/excel/PHPExcel/Writer/Excel5/BIFFwriter.php";
include_once $base_ref."formats/excel/PHPExcel/Writer/Excel5/Escher.php";
include_once $base_ref."formats/excel/PHPExcel/Writer/Excel5/Font.php";
include_once $base_ref."formats/excel/PHPExcel/Writer/Excel5/Parser.php";
include_once $base_ref."formats/excel/PHPExcel/Writer/Excel5/Workbook.php";
include_once $base_ref."formats/excel/PHPExcel/Writer/Excel5/Worksheet.php";
include_once $base_ref."formats/excel/PHPExcel/Writer/Excel5/Xf.php";
include_once $base_ref."formats/excel/PHPExcel/Shared/CodePage.php";
include_once $base_ref."formats/excel/PHPExcel/Shared/Date.php";
include_once $base_ref."formats/excel/PHPExcel/Shared/Drawing.php";
include_once $base_ref."formats/excel/PHPExcel/Shared/Escher.php";
include_once $base_ref."formats/excel/PHPExcel/Shared/Excel5.php";
include_once $base_ref."formats/excel/PHPExcel/Shared/File.php";
include_once $base_ref."formats/excel/PHPExcel/Shared/Font.php";
include_once $base_ref."formats/excel/PHPExcel/Shared/OLE.php";
include_once $base_ref."formats/excel/PHPExcel/Shared/OLERead.php";
include_once $base_ref."formats/excel/PHPExcel/Shared/PasswordHasher.php";
include_once $base_ref."formats/excel/PHPExcel/Shared/String.php";
include_once $base_ref."formats/excel/PHPExcel/Shared/XMLWriter.php";
include_once $base_ref."formats/excel/PHPExcel/Shared/ZipStreamWrapper.php";
include_once $base_ref."formats/excel/PHPExcel/Shared/JAMA/CholeskyDecomposition.php";
include_once $base_ref."formats/excel/PHPExcel/Shared/JAMA/EigenvalueDecomposition.php";
include_once $base_ref."formats/excel/PHPExcel/Shared/JAMA/LUDecomposition.php";
include_once $base_ref."formats/excel/PHPExcel/Shared/JAMA/Matrix.php";
include_once $base_ref."formats/excel/PHPExcel/Shared/JAMA/QRDecomposition.php";
include_once $base_ref."formats/excel/PHPExcel/Shared/JAMA/SingularValueDecomposition.php";
include_once $base_ref."formats/excel/PHPExcel/Shared/JAMA/utils/Error.php";
include_once $base_ref."formats/excel/PHPExcel/Shared/JAMA/utils/Maths.php";
include_once $base_ref."formats/excel/PHPExcel/Shared/OLE/ChainedBlockStream.php";
include_once $base_ref."formats/excel/PHPExcel/Shared/OLE/PPS.php";
include_once $base_ref."formats/excel/PHPExcel/Shared/OLE/PPS/File.php";
include_once $base_ref."formats/excel/PHPExcel/Shared/OLE/PPS/Root.php";
include_once $base_ref."formats/excel/PHPExcel/Shared/trend/bestFitClass.php";
include_once $base_ref."formats/excel/PHPExcel/Shared/trend/exponentialBestFitClass.php";
include_once $base_ref."formats/excel/PHPExcel/Shared/trend/linearBestFitClass.php";
include_once $base_ref."formats/excel/PHPExcel/Shared/trend/logarithmicBestFitClass.php";
include_once $base_ref."formats/excel/PHPExcel/Shared/trend/polynomialBestFitClass.php";
include_once $base_ref."formats/excel/PHPExcel/Shared/trend/powerBestFitClass.php";
include_once $base_ref."formats/excel/PHPExcel/Shared/trend/trendClass.php";

include_once $base_ref."formats/word/html_to_doc.inc.php";
include_once $base_ref."formats/word/FWDWordObject.php";
include_once $base_ref."formats/word/FWDWordText.php";
include_once $base_ref."formats/word/FWDWordStyle.php";
include_once $base_ref."formats/word/FWDWordTextBlock.php";
include_once $base_ref."formats/word/FWDWordTable.php";
include_once $base_ref."formats/word/FWDWordColumn.php";
include_once $base_ref."formats/word/FWDWordRow.php";
include_once $base_ref."formats/word/FWDWordColors.php";
include_once $base_ref."formats/word/FWDWord.php";
include_once $base_ref."formats/word/PHPWordLib.php";

include_once $base_ref."formats/rtf/rtfclass.php";

include_once $base_ref."formats/FWDGridExcel.php";
include_once $base_ref."formats/FWDDataSetExcel.php";

include_once $base_ref."formats/fpdf/fpdf.php";
include_once $base_ref."formats/FWDPDF.php";

include_once $base_ref."translation/FWDXMLParser.php";

include_once $base_ref."formats/FWDXmlTextExtractor.php";
include_once $base_ref."formats/FWDTextExtractor.php";

include_once $base_ref."base/FWDConfig.php";

FWDLanguage::getPHPStringValue('mx_shortdate_format', "%d/%m/%Y");

?>