<?php
/**
 * Classe FWDSyslogConnectionTest. Classe que teste a conec��o do protocolo syslog.
 *
 * <p>Classe utilizada para testar se a conec��o com o servidor de email esta funcionando
 * PHP.</p>
 * @package FWD5
 * @subpackage base
 */

class FWDSYSLogConnectionTest
{

	/**
	 * nome da maquina servidora de syslog
	 * @var string
	 * @access protected
	 */
	protected $csHostName="";

	/**
	 * porta do servi�o de syslog
	 * @var integer
	 * @access protected
	 */
	protected $ciHostPort=514;

	/**
	 * mensagem de erro retornado quando a conec��o com o servidor for mal sucedida
	 * @var string
	 * @access protected
	 */
	protected $csError="";

	/**
	 * tempo de espera de conec��o com o servidor de syslog
	 * @var integer
	 * @access protected
	 */
	protected $ciTimeout=0;

	/**
	 * status da conec��o
	 * @var string
	 * @access private
	 */
	private $csState="Disconnected";

	/**
	 * handle da cone��o com o servidor de syslog
	 * @var integer
	 * @access private
	 */
	private $ciConnection=0;

	/**
	 * seta o nome (ip) do servidor de syslog.
	 *
	 * <p>seta o nome (ip) do servidor de syslog.</p>
	 * @access public
	 * @param integer nome do servidor de syslog
	 */
	public function setHostName($psHostName){
		$this->csHostName = $psHostName;
	}

	/**
	 * seta a porta do host do servidor de syslog.
	 *
	 * <p>seta a porta do host do servidor de smpt.</p>
	 * @access public
	 * @param integer porta do host do servidor de email
	 */
	public function setHostPort($piHostPort){
		$this->ciHostPort = $piHostPort;
	}

	/**
	 * retorna os erros ocorridos na tentativa de connec��o
	 *
	 * <p>retorna os erros ocorridos na tentativa de connec��o ao servidor de syslog.</p>
	 * @access public
	 * @return string contento os erros ocorridos na tentativa de conec��o ao servidor de syslog
	 */
	public function getError(){
		return $this->csError;
	}

	/**
	 * seta o timeout da tentativa de conec��o ao servidor de syslog
	 *
	 * <p>seta o timeout da tentativa de conec��o ao servidor de syslog.</p>
	 * @access public
	 * @param integer timeout de conec��o
	 */
	public function setTimeout($piTimeout){
		$this->ciTimeout = $piTimeout;
	}

	/**
	 * escreve as mensagens do teste de syslog no debug do sistema
	 *
	 * <p>escreve as mensagens do teste de syslog no debug do sistema.</p>
	 * @access protected
	 * @param string $psMessage string que deve ser escrita no debug do sistema
	 */
	protected function outputDebug($psMessage)
	{
		FWDWebLib::getInstance()->writeStringDebug($psMessage);
	}

	/**
	 * executa a conec��o com o servidor de syslog
	 *
	 * <p>executa a conec��o com o servidor de syslog.</p>
	 * @access protected
	 * @param string $psDomain nome (ip) do servidor de email
	 * @param string $piPort porta do servi�o syslog
	 * @param string $psResolveMessage string q informa a mensagem que vai ser escrita no debug
	 * @return string $msMessage retorna mensagem de erro caso tenha acontecido algum erro
	 */
	protected function connectToHost($psDomain, $piPort,$psResolveMessage)
	{
		$this->outputDebug($psResolveMessage);
		$msIP=gethostbyname($psDomain);
		if($msIP!=$psDomain){
			$msMessage = "Could not resolve host '%domain_name%'";
			$msMessage = str_replace('%domain_name%', $psDomain,$msMessage);
			return($msMessage);
		}
		$msErrno="";
		$this->outputDebug("Connecting to host address '".$msIP."' port ".$piPort."...");

		FWDWebLib::setErrorHandler(new FWDDoNothingErrorHandler());

		if($this->ciConnection=( $this->ciTimeout ? fsockopen("udp://".$msIP,$piPort,$msErrno,$msError,$this->ciTimeout)
		: fsockopen("udp://".$msIP,$piPort)
		)
		){
			FWDWebLib::restoreLastErrorHandler();
			return("");
		}
		FWDWebLib::restoreLastErrorHandler();

		$msError =($this->ciTimeout ? strval($msError) : "??");
		$msMessage ="";
		switch($msError)
		{
			case "-3":
				$msMessage = "-3 socket could not be created";
				break;
			case "-4":
				$msMessage = "-4 dns lookup on hostname '%domain_name%' failed";
				$msMessage = str_replace("%domain_name%",$this->csHostName,$msMessage);
				break;
			case "-5":
				$msMassage = "-5 Connection refused or timed out";
				break;
			case "-6":
				$msMassage = "-6 fdopen() call failed";
				break;
			case "-7":
				$msMassage = "-7 setvbuf() call failed";
				break;
			default:
				$msMessage = "Could not connect to the host '%domain_name%' ";
				$msMessage = str_replace("%domain_name%",$this->csHostName,$msMessage);
				$msMessage .= $msError;
				break;
		}
		return $msMessage;
	}

	/**
	 * conecta ao servidor syslog indicado nas variaveis do objeto
	 *
	 * <p>conecta ao servidor syslog indicado nas variaveis do objeto utilizando os parametros previamente setados no objeto.</p>
	 * @access public
	 * @return boolean 1 se a cone��o com o servidor de email funcionou, 0 caso contr�rio
	 */
	public function connect()
	{
		if(strcmp($this->csState,"Disconnected"))
		{
			$this->csError="ciConnection is already established";
			return(0);
		}
		$this->csError="";
		$msError="";

		if(!$this->csHostName)
		{
			$this->csError="Could not determine the syslog to connect";
			return(0);
		}
		$msMessage = "Resolving syslog server domain '%domain_name%'...";
		$msMassage = str_replace("%domain_name%",$this->csHostName,$msMessage);
		$msError = $this->connectToHost($this->csHostName, $this->ciHostPort, $msMassage);

		if(strlen($msError))
		{
			$this->csError=$msError;
			$this->ciConnection=0;
			return(0);
		}else{
			$this->outputDebug("Connection to server established sucessfully!");
			return (1);
		}

	}
};

?>