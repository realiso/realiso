<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDGridMenuACLSecurity. Aplica ACL a um Menu e adiciona este na Grid.
 *
 * <p>Classe que aplica ACL a um Menu e adiciona este na Grid.</p>
 * @package FWD5
 * @subpackage base
 */
class FWDGridMenuACLSecurity extends FWDMenuACLSecurity {

	/**
	 * Inst�ncia �nica da classe FWDGridMenuACLSecurity
	 * @staticvar FWDGridMenuACLSecurity
	 * @access private
	 */
	private static $coGridMenuACLSecurity = NULL;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDGridMenuACLSecurity.</p>
	 * @access private
	 */
	private function __construct() {
	}

	/**
	 * Retorna a inst�ncia �nica da classe FWDGridMenuACLSecurity (singleton).
	 *
	 * <p>Retorna a inst�ncia �nica da classe FWDGridMenuACLSecurity. Se a
	 * inst�ncia ainda n�o existe, cria a �nica inst�ncia. Se a inst�ncia j�
	 * existe, retorna a �nica inst�ncia (singleton).</p>
	 * @access public
	 * @return FWDGridMenuACLSecurity Inst�ncia �nica da classe FWDGridMenuACLSecurity
	 * @static
	 */
	public static function getInstance() {
		if (self::$coGridMenuACLSecurity == NULL) {
			self::$coGridMenuACLSecurity = new FWDGridMenuACLSecurity();
		}
		else {}	// inst�ncia �nica j� existe, retorne-a
		return self::$coGridMenuACLSecurity;
	}

	/**
	 * Desabilita itens do menu, se necess�rio.
	 *
	 * <p>Desabilita itens do menu, se necess�rio. Percorre todos os itens
	 * do menu, desabilitando aqueles cuja tag estiver presente no array de
	 * tags para as quais o usu�rio n�o possui permiss�o.</p>
	 *
	 * @access public
	 * @param FWDMenu $poMenu FWDMenu no qual os itens ser�o desabilitados (se necess�rio)
	 * @param FWDACLSecurity $poACLSecurity ACL que cont�m as informa��es necess�rias para desabilitar os itens
	 * @param FWDGrid $poGrid Grid onde o menu ser� utilizado
	 * @param string $psLine Linha da Grid onde o menu ser� chamado
	 */
	public function installGridMenuACL(FWDMenu $poMenu, FWDACLSecurity $poACLSecurity, FWDGrid $poGrid, $psLine) {
		$moNewMenu = clone $poMenu;
		$moMenu = parent::installMenuACL($moNewMenu,$poACLSecurity);
		$moMenu->addElements($psLine.":");
		$poGrid->addMenu($moMenu);
	}

}
?>