<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDException. Realiza a adequa��o dos atributos de uma exce��o para a FWD5.
 *
 * <p>Classe para corre��o dos valores dos atributos 'file' e 'line' da classe
 * nativa Exception. Seta os valores segundo o arquivo que disparou o erro, e n�o
 * segundo o arquivo que levantou a exce��o (este �ltimo � sempre a classe
 * singleton FWDWebLib).</p>
 * @package FWD5
 * @subpackage base
 */
class FWDException extends Exception {

	/**
	 * Seta o nome do arquivo que disparou o erro.
	 *
	 * <p>M�todo que seta o atributo 'file' da classe nativa Exception para o
	 * valor do nome do arquivo em que foi realizada a chamada da fun��o
	 * 'trigger_error()'.</p>
	 * @access public
	 * @param string $psFile Nome do arquivo que disparou o erro
	 */
	public function setFile($psFile) {
		$this->file = $psFile;
	}

	/**
	 * Seta a linha do arquivo que disparou o erro.
	 *
	 * <p>M�todo que seta o atributo 'line' da classe nativa Exception para o
	 * valor da linha do arquivo em que foi realizada a chamada da fun��o
	 * 'trigger_error()'.</p>
	 * @access public
	 * @param string $psLine Linha do arquivo em que o erro foi disparado
	 */
	public function setLine($psLine) {
		$this->line = $psLine;
	}
}
?>