<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDConfig. Transforma a estrutura XML de conex�o (config.php) em objeto PHP.
 *
 * <p>Classe que transforma Transforma a estrutura XML de conex�o (config.php) em objeto PHP.</p>
 * @package FWD5
 * @subpackage base
 */
class FWDConfig {
	protected $crParser;
	protected $csErrorMsg;

	protected $csFile="";
	protected $ciLevel=0;
	protected $cbGetData=false;
	protected $csBase="";
	protected $csVar="";

	protected $caFields = array();
	protected $ciDebugType=FWD_DEBUG_NONE;

	/**
	 * Construtor.
	 *
	 * <p>Construtor. Recebe o caminho para o arquivo de configura��o.</p>
	 * @access public
	 * @param string psFile Caminho para o arquivo de configura��o do banco de dados.
	 */
	public function __construct($psFile) {
		$this->csFile = $psFile;
	}

	/**
	 * M�todo para configurar o xml parser.
	 *
	 * <p>M�todo para configurar o xml parser. Seta as fun��es
	 * que ser�o executadas na abertura e fechamento de tags
	 * e a fun��o que ser� usada para coletar os dados.</p>
	 * @access protected
	 */
	protected function setupParser() {
		$this->crParser = xml_parser_create('ISO-8859-1');
		xml_set_object($this->crParser, $this);
		xml_set_element_handler($this->crParser,'startHandler','endHandler');
		xml_set_character_data_handler($this->crParser,'dataHandler');
	}

	/**
	 * M�todo para liberar o xml parser.
	 *
	 * <p>M�todo para liberar o xml parser.</p>
	 * @access protected
	 */
	protected function freeParser(){
		xml_parser_free($this->crParser);
	}

	/**
	 * M�todo para parsear uma string de conte�do xml.
	 *
	 * <p>M�todo para parsear uma string de conte�do xml.</p>
	 * @access public
	 * @param string psData String com conte�do xml.
	 * @return True se conseguiu parsear; False caso contr�rio.
	 */
	public function parseString($psData){
		$this->setupParser(); //Define os m�todos que parseiam a string.
		if (!$psData)
		return false;
		$mbParse = xml_parse($this->crParser,$psData);
		if(!$mbParse){
			$this->csErrorMsg = sprintf(
        "XML error: %s at line %d",
			xml_error_string(xml_get_error_code($this->crParser)),
			xml_get_current_line_number($this->crParser)
			);
			$this->freeParser();
			return false;
		}
		$this->freeParser();
		return true;
	}

	/**
	 * Retorna o erro gerado durante parseamento, se houver algum.
	 *
	 * <p>Retorna o erro gerado durante parseamento, se houver algum.</p>
	 * @access public
	 * @return string Mensagem contendo o erro de parseamento.
	 */
	public function getErrorMsg() {
		return $this->csErrorMsg;
	}

	/**
	 * M�todo para realizar a conex�o com o banco de dados.
	 *
	 * <p>M�todo para realizar a conex�o com o banco de dados.
	 * O arquivo indicado no construtor � lido, parseado, e a
	 * conex�o � feita.</p>
	 * @access public
	 */
	public function load() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD1,__FILE__,__LINE__);

		$msConfig = file_get_contents($this->csFile); //Pega o conte�do do arquivo passado no construtor.
		$this->parseString($msConfig); //Parseia o conte�do.

		$maRequiredTags = array('DB_MSSQL'=>array('databaseType','databaseDatabase','databaseSchema','databasePassword','databaseHost'),
                            'DB_POSTGRES'=>array('databaseType','databaseDatabase','databaseSchema','databasePassword','databaseHost','databasePort'),
                            'DB_ORACLE'=>array('databaseType','databaseService','databaseSchema','databasePassword')
		);
		if (!isset($this->caFields['databaseType']) || !defined($this->caFields['databaseType'])) {
			return false;
		}
		foreach($maRequiredTags[$this->caFields['databaseType']] as $msField) {
			if (!isset($this->caFields[$msField])) {
				return false;
			}
		}

		//Decripta a senha, quando estiver compilado.
		if ($GLOBALS['load_method']==XML_STATIC) {
			$moModule = mcrypt_module_open(MCRYPT_BLOWFISH,'',MCRYPT_MODE_ECB,'');
			$msKey = "b306737041cb40b1492e0debcfe45433";
			mcrypt_generic_init($moModule, $msKey, "12345678");
			$msPassword = mdecrypt_generic($moModule, base64_decode($this->caFields['databasePassword']));
			$msPadding = substr($msPassword,-1);
			$maPaddings = array(chr(0),chr(1),chr(2),chr(3),chr(4),chr(5),chr(6),chr(7),chr(8));
			if (in_array($msPadding,$maPaddings)) $msPassword = rtrim($msPassword,$msPadding);
			$this->caFields['databasePassword'] = $msPassword;
		} else {
			$msPassword = $this->caFields['databasePassword'];
		}

		switch($this->caFields['databaseType']) {
			case 'DB_MSSQL':
				$msDatabase = $this->caFields['databaseDatabase'];
				$msHost = $this->caFields['databaseHost'];
				FWDWebLib::setConnection(new FWDDB(constant($this->caFields['databaseType']),$msDatabase,$this->caFields['databaseSchema'],$msPassword,$msHost));
				break;
			case 'DB_POSTGRES':
				$msDatabase = $this->caFields['databaseDatabase'];
				$msHost = $this->caFields['databaseHost'];
				$msPort = $this->caFields['databasePort'];
				FWDWebLib::setConnection(new FWDDB(constant($this->caFields['databaseType']),$msDatabase,$this->caFields['databaseSchema'],$msPassword,$msHost.($msPort?":".$msPort:"")));
				break;
			case 'DB_ORACLE':
				$msDatabase = $this->caFields['databaseService'];
				$msHost = "";
				FWDWebLib::setConnection(new FWDDB(constant($this->caFields['databaseType']),$msDatabase,$this->caFields['databaseSchema'],$msPassword,$msHost));
				break;
			default:
				trigger_error('Database type is invalid: '.$this->caFields['databaseType'],E_USER_ERROR);
				break;
		}

		//Seta debug
		if (isset($this->ciDebugType) && $this->ciDebugType != 0) {
			FWDWebLib::setDebugMode(true,$this->ciDebugType);
		} else {
			FWDWebLib::setDebugMode(false,FWD_DEBUG_NONE);
		}

		//seta o path do arquivo de debug
		if (isset($this->caFields['debugFile'])){
			FWDWebLib::setDebugFilePath($this->caFields['debugFile']);
		}
		return true;
	}

	/**
	 * Retorna o endere�o do servidor RPC.
	 *
	 * <p>Retorna o endere�o do servidor RPC, se houver. (AMS)</p>
	 * @access public
	 * @return string Endere�o do servidor RPC.
	 */
	public function getRPCServerAddress() {
		if (isset($this->caFields['xmlrpcHost']) && isset($this->caFields['xmlrpcPort']))
		return "http://{$this->caFields['xmlrpcHost']}:{$this->caFields['xmlrpcPort']}/";
		else
		return "";
	}

	/**
	 * M�todo executado durante parseamento na abertura de tags.
	 *
	 * <p>M�todo executado durante parseamento na abertura de tags.</p>
	 * @access protected
	 */
	protected function startHandler($prParser, $psTagName, $paAttributes){
		$this->cbGetData = false; //N�o pegar os dados entre tags
		if ($this->ciLevel == 1) {
			$this->csBase = strtolower($psTagName);
		}
		if ($this->ciLevel == 2) {
			$this->csVar = ucfirst(strtolower($psTagName));
			$this->cbGetData=true; //Pegar os dados entre tags de nivel 2
		}
		$this->ciLevel++;
	}

	/**
	 * M�todo executado durante parseamento para pegar os dados entre tags.
	 *
	 * <p>M�todo executado durante parseamento para pegar os dados entre tags.</p>
	 * @access protected
	 */
	protected function dataHandler($prParser, $psData){
		if ($this->cbGetData && $this->ciLevel==3) { //Pegar os dados quando as tags forem de nivel 2. Verifica-se o nivel 3, pois foi incrementado antes de entrar neste m�todo.
			if ($this->csBase=='debug') { //Se a tag for 'debug', n�o colocar no array de tags. Setar o debugType.
				if (defined($psData)) {
					$this->ciDebugType = $this->ciDebugType | constant($psData); //Concatena os modos de debug.
				} else {
					if($this->csVar = 'File'){
						$this->caFields[$this->csBase.$this->csVar]=$psData;
					}else{
						trigger_error('Debug type is invalid: '.$psData,E_USER_ERROR);
					}
				}
			} else {
				$this->caFields[$this->csBase.$this->csVar]=$psData;
			}
		}
	}

	/**
	 * M�todo executado durante parseamento no fechamento de tags.
	 *
	 * <p>M�todo executado durante parseamento no fechamento de tags.</p>
	 * @access protected
	 */
	protected function endHandler($prParser, $psTagName){
		$this->ciLevel--; //Quando fecha uma tag, volta um n�vel.
	}

	/**
	 * Retorna os campos extra�dos do XML.
	 *
	 * <p>Retorna os campos extra�dos do XML.</p>
	 * @access public
	 * @return array Array associativo com os valores dos campos indexados por seus nomes
	 */
	public function getFields(){
		return $this->caFields;
	}

}
?>