<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

define("XML_LOAD", 2);
define("XML_STATIC", 4);

define("FWD_DEBUG_NONE",        0);
define("FWD_DEBUG_DB",          1);
define("FWD_DEBUG_INFO",        2);
define("FWD_DEBUG_ERROR",       4);
define("FWD_DEBUG_FWD1",        8);
define("FWD_DEBUG_FWD2",       16);
define("FWD_DEBUG_ALL",0xFFFFFFFF);

$GLOBALS['load_method'] = ((isset($lbRelease) && $lbRelease)?XML_STATIC:XML_LOAD);

/**
 * Classe FWDWebLib. Singleton. Implementa vari�veis e m�todos globais na FWD5.
 *
 * <p>Classe singleton que implementa vari�veis e m�todos globais na Framework FWD5.
 * Garante a exist�ncia de uma e apenas uma inst�ncia da classe FWDWebLib.</p>
 * @package FWD5
 * @subpackage base
 */
class FWDWebLib {

	/**
	 * Atributo que controla a vers�o da FWD
	 * @var string
	 * @access private
	 */
	private $csVersion = '$Id: FWDWebLib.php 1147 2006-12-18 17:17:51Z eschein $';

	/**
	 * Inst�ncia �nica da classe FWDWebLib
	 * @staticvar FWDWebLib
	 * @access private
	 */
	private static $coWebLib = NULL;

	/**
	 * Inst�ncia �nica da classe FWDXmlLoad
	 * @staticvar FWDXmlLoad
	 * @access private
	 */
	private static $coXmlLoad = NULL;

	/**
	 * Define se os objetos PHP vem de um XmlLoad ou de um XmlStatic
	 * @var integer
	 * @access private
	 */
	private $ciXmlLoadMethod;

	/**
	 * Inst�ncia �nica de conex�o com o banco de dados
	 * @staticvar FWDDB
	 * @access private
	 */
	private static $coDBConnection = NULL;

	/**
	 * Refer�ncia base do sistema (url, pode ser relativa ou absoluta)
	 * @var string
	 * @access private
	 */
	private  $csSysRef;

	/**
	 * Refer�ncia para base (../base/)
	 * @var string
	 * @access private
	 */
	private $csLibRef;

	/**
	 * Refer�ncia para os arquivos javascript (../base/javascript)
	 * @var string
	 * @access private
	 */
	private $csJsRef;

	private $csSysJsRef;

	/**
	 * Refer�ncia para o sys_ref baseado no tab_main
	 * @var string
	 * @access private
	 */
	private $csSysRefBasedOnTabMain;

	/**
	 * Refer�ncia para os arquivos CSS do projeto
	 * @var string
	 * @access private
	 */
	private $csCSSRef;

	/**
	 * Refer�ncia para os arquivos de imagens (../gfx/)
	 * @var string
	 * @access private
	 */
	private $csGfxRef;

	/**
	 * Indica se a fun��o session_start() j� foi chamada.
	 * @var boolean
	 * @access private
	 */
	private $cbSessionStarted = false;

	/**
	 * Indica se o dump_html j� foi iniciado. �til, pois algumas opera��es n�o podem ser feitas depois de iniciar o dump.
	 * @var boolean
	 * @access private
	 */
	private $cbDumpStarted = false;

	/**
	 * Indica se a FWD est� dentro da fun��o xml_load . �til, pois algumas opera��es n�o podem ser feitas durante o xml_load1.
	 * @var boolean
	 * @access private
	 */
	private $cbXmlLoadRunning = false;

	/**
	 * Objeto que manipula os erros e as exce��es
	 * @var FWDErrorHandler
	 * @access private
	 */
	private static $coErrorHandler = null;

	/**
	 * Pilha de error handlers
	 * @var array
	 * @access private
	 */
	private static $caErrorHandlerStack = array();

	/**
	 * Indica se o sistema est� em modo debug
	 * @var boolean
	 * @access private
	 */
	private static $cbDebugMode = false;

	/**
	 * Indica o tipo de debug gravado
	 * @var integer
	 * @access private
	 */
	private static $ciDebugType = FWD_DEBUG_NONE;

	/**
	 * Indica se o debug gravado ser� encriptado ou n�o
	 * @var integer
	 * @access private
	 */
	private static $cbDebugEncrypted = true;

	/**
	 * Indica o caminho do arquivo de debug
	 * @var integer
	 * @access private
	 */
	private static $csDebugFilePath = "./lib/debug.txt";

	/**
	 * Titulo da janela do browser
	 * @var string
	 * @access private
	 */
	private $csWindowTitle = 'FWD5 - Fast Web Development';

	/**
	 * Meta Keywords da p�gina
	 * @var string
	 * @access private
	 */
	private $csMetaKeywords = '';

	/**
	 * FavIcon
	 * @var string
	 * @access private
	 */
	private $csFavIcon = '';

	/**
	 * Avoid Google
	 * @var boolean
	 * @access private
	 */
	private $cbAvoidGoogle = false;

	/**
	 * Tracker code (ex. Google Analytics)
	 * @var string
	 * @access private
	 */
	private $csTracker = '';

	/**
	 * CSS extra
	 * @var string
	 * @access private
	 */
	private $csExtraCSS = '';

	/**
	 * Primeiro arquivo XML carregado. � o que representa a tela e desencadeia os eventos.
	 * @var string
	 * @access private
	 */
	private $csXmlName = '';

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDWebLib. Inicializa as vari�veis globais
	 * com os valores padr�o.</p>
	 * @access private
	 * @param string $psBaseDefault Refer�ncia base padr�o
	 */
	private function __construct($psBaseDefault = "./"){
		ob_start();
		$this->csLibRef = $psBaseDefault."base/";
		$this->csJsRef = $this->csLibRef."javascript/nonauto/";
		$this->csSysJsRef = '';
		$this->csGfxRef = $psBaseDefault."gfx/";
		$this->ciXmlLoadMethod = $GLOBALS['load_method'];
		switch($this->ciXmlLoadMethod){
			case XML_LOAD  : self::$coXmlLoad = FWDXmlLoad::getInstance(); break;
			case XML_STATIC: self::$coXmlLoad = FWDXmlStatic::getInstance(); break;
			default        : trigger_error("Invalid XmlLoad Method. Must be either 'XML_LOAD' or 'XML_STATIC'.",E_USER_ERROR); break;
		}

		self::setErrorHandler(new FWDDefaultErrorHandler());
	}

	/**
	 * Seta o objeto que manipula os erros e as exce��es.
	 *
	 * <p>Seta o objeto que manipula os erros e as exce��es.</p>
	 * @access public
	 * @param FWDErrorHandler $poErrorHandler Objeto que manipula os erros e as exce��es
	 * @static
	 */
	public static function setErrorHandler(FWDErrorHandler $poErrorHandler){
		if(self::$coErrorHandler){
			array_push(self::$caErrorHandlerStack,self::$coErrorHandler);
		}
		self::$coErrorHandler = $poErrorHandler;
		set_error_handler(array(self::$coErrorHandler,'handleError'));
		set_exception_handler(array(self::$coErrorHandler,'handleException'));
	}

	/**
	 * Restaura o error handler anterior, se houver.
	 *
	 * <p>Restaura o error handler anterior, se houver.</p>
	 * <p>� poss�vel aninhar chamadas de setErrorHandler e restoreLastErrorHandler,
	 * mas n�o � poss�vel ficar alternando entre dois error handlers com sucesivas
	 * chamadas de restoreLastErrorHandler.</p>
	 * @access public
	 * @return boolean Indica se havia um error handler anterior
	 * @static
	 */
	public static function restoreLastErrorHandler(){
		if(count(self::$caErrorHandlerStack)){
			self::$coErrorHandler = array_pop(self::$caErrorHandlerStack);
			set_error_handler(array(self::$coErrorHandler,'handleError'));
			set_exception_handler(array(self::$coErrorHandler,'handleException'));
			return true;
		}else{
			return false;
		}
	}

	/**
	 * Retorna a inst�ncia �nica da classe FWDWebLib (singleton).
	 *
	 * <p>Retorna a inst�ncia �nica da classe FWDWebLib. Se a inst�ncia
	 * ainda n�o existir, cria a �nica inst�ncia. Se a inst�ncia j� existir,
	 * retorna a �nica inst�ncia (singleton).</p>
	 * @access public
	 * @param string $psBaseDefault Refer�ncia base padr�o
	 * @return FWDWebLib Inst�ncia �nica da classe FWDWebLib
	 * @static
	 */
	public static function getInstance($psBaseDefault = "./") {
		if (self::$coWebLib == NULL) {
			self::$coWebLib = new FWDWebLib($psBaseDefault);
		}
		else {}  // inst�ncia �nica j� existe, retorne-a
		return self::$coWebLib;
	}

	/**
	 * Seta a inst�ncia �nica de conex�o com o banco de dados (singleton).
	 *
	 * <p>Seta a inst�ncia �nica de conex�o com o banco de dados. Se a inst�ncia
	 * ainda n�o existir, cria a �nica inst�ncia. Se a inst�ncia j� existir,
	 * n�o faz nada (singleton).</p>
	 * @access public
	 * @param FWDDB $poDB Objeto de conex�o com o banco de dados
	 * @static
	 */
	public static function setConnection(FWDDB $poDB) {
		if (self::$coDBConnection == NULL) self::$coDBConnection = $poDB;
	}

	/**
	 * Retorna a inst�ncia �nica da classe FWDWebLib (singleton).
	 *
	 * <p>Retorna a inst�ncia �nica da classe de conex�o do banco de dados.</p>
	 * @access public
	 * @return FWDDB Inst�ncia �nica de conex�o do banco de dados
	 * @static
	 */
	public static function getConnection() {
		if (self::$coDBConnection == NULL)
		trigger_error("No database connection available",E_USER_ERROR);
		return clone self::$coDBConnection;
	}

	/**
	 * Adiciona um objeto global.
	 *
	 * <p>Insere no array $GLOBALS um objeto definido pelo usu�rio.</p>
	 * @access public
	 * @param string $psName Nome da vari�vel global a ser retornada
	 * @param object $poObject Objeto
	 * @static
	 */
	public static function addObject($psName, $poObject) {
		if(!isset($GLOBALS[$psName]))
		$GLOBALS[$psName] = $poObject;
		else
		trigger_error("An object with the same name ($psName) already exists", E_USER_WARNING);
	}

	/**
	 * Retorna uma vari�vel global gen�rica.
	 *
	 * <p>Retorna uma vari�vel global gen�rica. Procura por uma ocorr�ncia
	 * da string passada como par�metro no array $GLOBALS[]. Caso encontre,
	 * retorna o conte�do desta posi��o do array. Caso contr�rio, retorna NULL.</p>
	 * @access public
	 * @param string $psName Nome da vari�vel global a ser retornada
	 * @return object Vari�vel global
	 * @static
	 */
	public static function getObject($psName = "") {
		$moObject = NULL;
		if ( isset($GLOBALS[$psName]) ) {
			$moObject = $GLOBALS[$psName];
		}
		else{}  // objeto n�o existe, retorne NULL
		return $moObject;
	}

	/**
	 * Limpa o array $GLOBALS.
	 *
	 * <p>Limpa o array $GLOBALS.</p>
	 * @access public
	 * @static
	 */
	public static function cleanObjects() {
		$GLOBALS = array();
	}

	/**
	 * Retorna uma vari�vel do POST.
	 *
	 * <p>Retorna uma vari�vel do POST. Procura por uma ocorr�ncia
	 * da string passada como par�metro no array $_POST[]. Caso encontre,
	 * retorna o conte�do desta posi��o do array. Caso contr�rio, retorna NULL.</p>
	 * @access public
	 * @param string $psName Nome da vari�vel do POST a ser retornada
	 * @return object Vari�vel do POST
	 * @static
	 */
	public static function getPOST($psName = "") {
		$moObject = NULL;
		if ( isset($_POST[$psName]) ) {
			$moObject = $_POST[$psName];
		}
		else{}  // objeto n�o existe, retorne NULL
		return $moObject;
	}

	/**
	 * Indica se o browser utilizado � IE ou n�o.
	 *
	 * <p>Retorna o valor booleano 'true' caso o browser utilizado seja
	 * o Internet Explorer. Retorna 'false' caso contr�rio.</p>
	 * @access public
	 * @return boolean Indica se o browser � IE ou n�o
	 * @static
	 */
	public static function browserIsIE() {
		return (strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), "MSIE")) ? true:false;
	}

	/**
	 * Exibe o conte�do da inst�ncia �nica FWDWebLib.
	 *
	 * <p>Exibe na tela o conte�do de todos os atributos da �nica inst�ncia
	 * da classe FWDWebLib.</p>
	 * @access public
	 */
	public function dump_instance() {
		echo "<br/><br/><br/><br/>--------------[ FWDWebLib Singleton Dump <strong>BEGIN</strong> ]--------------<br/>";
		echo "<h3>coWebLib:</h3>";
		var_dump(self::$coWebLib);
		echo "<br/><h3>coXmlLoad:</h3>";
		var_dump(self::$coXmlLoad);
		echo "<br/><h3>csSysRef:</h3>";
		var_dump($this->csSysRef);
		echo "<br/><h3>csLibRef:</h3>";
		var_dump($this->csLibRef);
		echo "<br/><h3>csJsRef:</h3>";
		var_dump($this->csJsRef);
		echo "<br/><h3>csSysJsRef:</h3>";
		var_dump($this->csSysJsRef);
		echo "<br/><h3>csGfxRef:</h3>";
		var_dump($this->csGfxRef);
		echo "<h3></h3>--------------[ FWDWebLib Singleton Dump <strong>END</strong> ]--------------<br/><br/><br/><br/>";
	}

	/**
	 * Traduz um valor, caso este esteja presente no array.
	 *
	 * <p>Se o array passado como par�metro for uma lista de valores, por exemplo
	 * 'array("a","b","c",...)', retorna o pr�prio valor caso encontre uma ocorr�ncia
	 * deste no array. Caso n�o encontre, retorna NULL.
	 * Se o array passado como par�metro for um array do tipo
	 * 'array("a"=>x, "b"=>y, "c"=>z,...)', procura por uma ocorr�ncia do valor a
	 * ser traduzido (par�metro 1) no lado esquerdo da seta. Caso encontre, retorna
	 * o valor existente do lado direito da seta. Caso n�o encontre, retorna NULL.</p>
	 * @access public
	 * @param string $psValue Valor a ser traduzido
	 * @param array $paSet Array de valores OU array do tipo valor=>tradu��o
	 * @param string $psAttribute Nome do atributo
	 * @return string Depende dos par�metros passados para a fun��o
	 */
	public static function attributeParser($psValue,$paSet,$psAttribute) {
		$miMatched = 0;
		foreach($paSet as $msKey => $msKeyValue) {
			if ( (is_int($msKey)?$msKeyValue:$msKey) == $psValue ) {
				$miMatched++;
				break;
			}
			else{}
		}

		if (!$miMatched) {
			trigger_error("Value '{$psValue}' does not belong to the set of possible values in attribute '{$psAttribute}'",E_USER_WARNING);
			return NULL;
		}
		else {
			return $msKeyValue;
		}
	}

	/**
	 * Transforma arquivos XML em objetos PHP.
	 *
	 * <p>Transforma arquivos XML em objetos PHP.</p>
	 * @access public
	 * @param string $psFilename Nome do arquivo XML a ser transformado
	 */
	public function xml_load($psFilename, $pbStartEvent = TRUE) {
		if($this->csXmlName=='') $this->csXmlName = $psFilename;
		$this->cbXmlLoadRunning = true;
		self::$coXmlLoad->xml_load($psFilename, $pbStartEvent);
		$this->cbXmlLoadRunning = false;
		//Dispara os eventos definidos pelo usu�rio
		if($pbStartEvent) {
			$StartEvent_instance = FWDStartEvent::getInstance();
			$StartEvent_instance->start();
		}
	}

	/**
	 * Seta o t�tulo da janela do browser.
	 *
	 * <p>Seta o t�tulo da janela do browser.</p>
	 * @access public
	 * @param string $psWindowTitle T�tulo da janela
	 */
	public function setWindowTitle($psWindowTitle) {
		$this->csWindowTitle = $psWindowTitle;
	}

	/**
	 * Seta o meta keywords da p�gina.
	 *
	 * <p>Seta o meta keywords da p�gina.</p>
	 * @access public
	 * @param string $psMetaKeywords Palavras-chave da p�gina
	 */
	public function setMetaKeywords($psMetaKeywords) {
		$this->csMetaKeywords = $psMetaKeywords;
	}

	/**
	 * Seta o FavIcon p�gina.
	 *
	 * <p>Seta o FavIcon da p�gina.</p>
	 * @access public
	 * @param string $psUrl Url do FavIcon
	 */
	public function setFavIcon($psUrl) {
		$this->csFavIcon = $psUrl;
	}

	/**
	 * Seta o Tracker da p�gina.
	 *
	 * <p>Seta o Tracker da p�gina.</p>
	 * @access public
	 * @param string $psTracker C�digo do Tracker
	 */
	public function setTracker($psTracker) {
		$this->csTracker = $psTracker;
	}

	/**
	 * Indica que o sistema n�o deve ser indexado pelo google.
	 *
	 * <p>Indica que o sistema n�o deve ser indexado pelo google.</p>
	 * @access public
	 * @param boolean $pbAvoidGoogle True or false
	 */
	public function avoidGoogle($pbAvoidGoogle) {
		$this->cbAvoidGoogle = $pbAvoidGoogle;
	}

	/**
	 * Adiciona um css extra no sistema.
	 *
	 * <p>M�todo para adicionar um css extra no sistema. Utilizado para
	 * diferenciar entre os diferentes tipos de licen�a.</p>
	 * @access public
	 * @param string $psExtraCSS Nome do arquivo css
	 */
	public function setExtraCSS($psExtraCSS) {
		$this->csExtraCSS = $psExtraCSS;
	}

	/**
	 * Cria o c�digo HTML de uma p�gina.
	 *
	 * <p>Cria o c�digo HTML de uma p�gina. Escreve o cabe�alho, desenha o dialog
	 * (escreve o corpo da p�gina) e fecha as tags abertas no cabe�alho.</p>
	 * @access public
	 * @param FWDDialog $poDialog Corpo da p�gina
	 */
	public function dump_html(FWDDialog $poDialog){
		$this->cbDumpStarted = true;
		// Para resolver o problema com a FPDF, somente adiciona-se o gzhandler no
		// dump_html, e para que funcione � necess�rio armazenar o que se encontra no
		// buffer e dar o flush depois de ter setado o hzhandler, ob_end_flush n�o funciona.
		$msOutput = ob_get_contents();
		ob_end_clean();
		ob_start("ob_gzhandler");
		echo $msOutput;

		$msPopUpId = substr($this->csXmlName,0,strrpos($this->csXmlName,'.'));

		$msRenderLoad = $poDialog->renderLoad();
		?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<? if ($this->csMetaKeywords) echo "<meta name='keywords' content='".$this->csMetaKeywords."'>"; ?>
	<? if ($this->cbAvoidGoogle) echo "<meta name='robots' content='noindex, nofollow'>"; ?>
	<?=($this->csFavIcon)?'<link rel="shortcut icon" href="'.$this->csFavIcon.'">':''?>
		<link rel="StyleSheet" href="<?=$this->getCSSRef()?>css.php" type="text/css" />
	<? if ($this->csExtraCSS) {?>
		<link rel="StyleSheet" href="<?=$this->getCSSRef()?>css/nonauto/<?=$this->csExtraCSS?>" type="text/css" />
	<?}?>
		<script src='<?=$this->getLibRef()?>js.php' type='text/javascript' language='javascript'></script>
	<? if($this->csSysJsRef!=''){ ?>
		<script src="<?=$this->csSysJsRef?>js.php" type="text/javascript" language="javascript"></script>
	<?}?>

<style type="text/css">
	<?
	//customiza��o das logos
	/* FIX: Descomentar na publica��o
	if(!empty($_SESSION)) {
		$session = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
	
		$mode = $session->getMode();
		
		$msDirectory = '';
		
		if(isset($GLOBALS['custom_gfx_ref']) && ISMSLib::isCompiled()){
			$msDirectory = $GLOBALS['custom_gfx_ref'];
		} else {
			$msDirectory = ($mode == ADMIN_MODE) ? '../../custom_gfx/' : 'custom_gfx/';
		}
		
		$customGfxPhp = ($mode == ADMIN_MODE) ? '../../custom_gfx.php' : 'custom_gfx.php';
		
		$customLogoFile = "custom_logo.png";
		$customLogoFilePath = $msDirectory . $customLogoFile;
		
		if(file_exists($customLogoFilePath))
		{
			$tm = time();
			print ".SystemLogo{ background-image: url($customGfxPhp?f=$customLogoFile&$tm); }\n"; 
		}
		
		$customLogoSmallFile = "custom_logo_small.png";
		$customLogoSmallFilePath = $msDirectory . $customLogoSmallFile;
		
		if(file_exists($customLogoSmallFilePath))
		{
			$tm = time();
			print ".SystemLogoSmall{ background-image: url($customGfxPhp?f=$customLogoSmallFile&$tm); }\n"; 
		}
		
		$lightColor = ISMSLib::getConfigById(LIGHT_COLOR);
		$darkColor = ISMSLib::getConfigById(DARK_COLOR);
		
		if($darkColor){
			print ".CustomDarkColor{ color: ".$darkColor."; }\n";
			print ".CustomDarkColorBackground_over{ background-color: ".$darkColor."; }\n";
			print ".CustomDarkColorBackground_selected{ background-color: ".$darkColor."; }\n";
			print ".CustomDarkColorBackground_notSelected{ }\n";
			print ".CustomDarkColorTop{ background-color: ".$darkColor."; width: 100%; height: 4px; top: 1 !important;}\n";
		}else{
			$moISMSSaaS = new ISMSSaaS();
			$ciExpiracy             = $moISMSSaaS->getConfig(ISMSSaaS::LICENSE_EXPIRACY);
			
			$expired =  $ciExpiracy && $ciExpiracy < ISMSLib::ISMSTime();
			if(!$expired){
				$csClientName           = $moISMSSaaS->getConfig(ISMSSaaS::LICENSE_CLIENT_NAME);
				$csModules              = $moISMSSaaS->getConfig(ISMSSaaS::LICENSE_MODULES);
				$ciMaxUsers             = $moISMSSaaS->getConfig(ISMSSaaS::LICENSE_MAX_USERS);
				$ciActivationDate       = $moISMSSaaS->getConfig(ISMSSaaS::LICENSE_ACTIVATION_DATE);
				
				$ciMaxSimultaneousUsers = $moISMSSaaS->getConfig(ISMSSaaS::LICENSE_MAX_SIMULTANEOUS_USERS);
				$ciLicenseType          = $moISMSSaaS->getConfig(ISMSSaaS::LICENSE_TYPE);
				
				$hash = md5($csClientName.$csModules.$ciMaxUsers.$ciActivationDate.$ciExpiracy.$ciMaxSimultaneousUsers.$ciLicenseType.'LICENSEHASH');
				$expired = $hash != $moISMSSaaS->getConfig(ISMSSaaS::LICENSE_HASH);
			}

			$defColor = null;			
			if(!$expired){
				$moLicense = new ISMSLicense();

				if($moLicense->isSOX() || $moLicense->isTHREE()){
					$defColor = "#787878";
				}else if($moLicense->isOHS()){
					$defColor = "#B000C8";
				}else if($moLicense->isPCI()){
					$defColor = "#DF6800";
				}else if($moLicense->isEMS()){
					$defColor = "#339944";
				}
			}
			if($defColor){
				print ".CustomDarkColorBackground_over{ background-color: ".$defColor."; }\n";
				print ".CustomDarkColorBackground_selected{ background-color: ".$defColor."; }\n";
				print ".CustomDarkColorBackground_notSelected{ }\n";
			}
			
			print ".CustomDarkColorTop{ background-color: #005C90; width: 100%; height: 4px; top: 1 !important;}\n";
		}
		if($lightColor){
			print ".CustomLightColor{ color: ".$lightColor."; }\n";
			print ".CustomLightColorBackground{ background-color: ".$lightColor."; }\n";
			print ".CustomLightColorTop{ background-color: ".$lightColor."; width: 100%; height: 4px; top: 5 !important;}\n";
		}else{
			print ".CustomLightColorTop{ background-color: #387AD8; width: 100%; height: 4px; top: 5 !important;}\n";	
		}
		
		print "#defaultTop{ width: 100%; height: 4px; top: 9 !important; background-color: #F8F8F8;}\n";
		
	}
	//*/
	?>
</style>
	
<script type='text/javascript' language='javascript'>
  if (soPopUpManager.getPopUpById('<?=$msPopUpId?>')) {
    eval('var self = soPopUpManager.getPopUpById(\'<?=$msPopUpId?>\');');
  }
</script>
<title><?=$this->csWindowTitle?></title>
</head>
<body topmargin=0 leftmargin=0 <?=$msRenderLoad?>
	oncontextmenu='return false'>
<form name="form_fwd" id="form_fwd" enctype="multipart/form-data"
	onsubmit="return false;" method="post"><span id="FWDLoading"
	class="FWDLoading" style="display: none;"><?=FWDLanguage::getPHPStringValue('FWDLoading','Carregando')?><img
	src="<?=$this->csGfxRef?>loading.gif" /></span> <?=$poDialog->draw()?>
<div id='tooltip_code_tt_init' name='tooltip_code_tt_init'></div>
<script src="<?=$this->getJsRef()?>wz_tooltip.js" type="text/javascript"
	language="javascript"></script> <iframe
	name="force_report_download_iframe" id="force_report_download_iframe"
	style="display: none"></iframe> <iframe name="fwd_iframe_upload"
	id="fwd_iframe_upload" style="display: none"></iframe> <input
	type="hidden" value="" /> <script type='text/javascript'
	language='javascript'>if(self && self.finishLoad)self.finishLoad();</script>
</form>
		<?=$this->csTracker?>
</body>
</html>
		<?
	}
	/*criado o input hidden acima (sem nome e sem valor) para que a fun��o js_submit funcione em uma tela em que n�o tem elementos de form
	 assim o bug da tela document_read_details seja corrigido (a fun��o js_submit � utilizada nessa tela para fazer o download do documento)*/

	/**
	 * Adiciona um novo erro conhecido.
	 *
	 * <p>Adiciona um novo erro conhecido ao array de erros conhecidos.</p>
	 * @access public
	 * @param string $psErrorStringId Id do erro
	 * @param array $paMsgSubstrings Array de strings que caracterizam o erro
	 * @static
	 */
	public static function addKnownError($psErrorStringId,$paMsgSubstrings){
		self::$coErrorHandler->addKnownError($psErrorStringId,$paMsgSubstrings);
	}

	/**
	 * Remove um erro conhecido.
	 *
	 * <p>Remove um erro conhecido do array de erros conhecidos.</p>
	 * @access public
	 * @param string $psErrorStringId Id do erro
	 * @static
	 */
	public static function removeKnownError($psErrorStringId){
		self::$coErrorHandler->removeKnownError($psErrorStringId);
	}

	/**
	 * Seta o modo de debug do sistema.
	 *
	 * <p>Seta o modo de debug do sistema.</p>
	 * @access public
	 * @param boolean $pbMode True ou Flase
	 */
	public static function setDebugMode($pbMode,$piType,$pbEncrypted=false) {
		self::$cbDebugMode = $pbMode;
		self::$ciDebugType = $piType;
		self::$cbDebugEncrypted=$GLOBALS['load_method']==XML_STATIC?false:$pbEncrypted; //quando compilado, sempre encripta o debug.txt
	}

	/**
	 * Retorna se o debug est� sendo criptografado ou n�o.
	 *
	 * <p>Retorna se o debug est� sendo criptografado ou n�o.</p>
	 * @access public
	 * @return boolean True ou False
	 */
	public static function isDebugEncrypted() {
		return self::$cbDebugEncrypted;
	}

	/**
	 * Retorna o modo de debug do sistema.
	 *
	 * <p>Retorna o modo de debug do sistema.</p>
	 * @access public
	 * @return boolean True ou Flase
	 */
	public function getDebugMode() {
		return self::$cbDebugMode;
	}

	/**
	 * Retorna o modo de debug do sistema.
	 *
	 * <p>Retorna o modo de debug do sistema.</p>
	 * @access public
	 * @return boolean True ou Flase
	 */
	public static function getDebugType() {
		return self::$ciDebugType;
	}

	/**
	 * Retorna o caminho para o arquivo de debug.
	 *
	 * <p>Retorna o caminho para o arquivo de debug.</p>
	 * @access public
	 * @return string Caminho para o arquivo
	 */
	public static function getDebugFilePath() {
		return self::$csDebugFilePath;
	}

	/**
	 * Seta o caminho para o arquivo de debug.
	 *
	 * <p>Seta o caminho para o arquivo de debug.</p>
	 * @access public
	 * @return string Caminho para o arquivo
	 */
	public static function setDebugFilePath($psDebugFilePath){
		self::$csDebugFilePath = $psDebugFilePath;
	}

	/**
	 * Seta o valor do atributo SysRef.
	 *
	 * <p>Seta o valor do atributo SysRef (url absoluta ou relativa).</p>
	 * @access public
	 * @param string $psSysRef Valor de base (url)
	 */
	public function setSysRef($psSysRef) {
		$this->csSysRef = $psSysRef;
	}

	/**
	 * Retorna o valor do atributo SysRef.
	 *
	 * <p>Retorna o valor do atributo SysRef (url absoluta ou relativa).</p>
	 * @access public
	 * @return string Valor de base (url)
	 */
	public function getSysRef() {
		return $this->csSysRef;
	}

	/**
	 * Seta o valor do atributo LibRef.
	 *
	 * <p>Seta o valor do atributo LibRef (refer�ncia para base).</p>
	 * @access public
	 * @param string $psLibRef Refer�ncia para base (url)
	 */
	public function setLibRef($psLibRef) {
		$this->csLibRef = $psLibRef;
	}

	/**
	 * Retorna o valor do atributo LibRef.
	 *
	 * <p>Retorna o valor do atributo LibRef (refer�ncia para base).</p>
	 * @access public
	 * @return string Refer�ncia para base (url)
	 */
	public function getLibRef() {
		return $this->csLibRef;
	}

	/**
	 * Seta o valor do atributo JsRef.
	 *
	 * <p>Seta o valor do atributo JsRef (refer�ncia para arquivos javascript).</p>
	 * @access public
	 * @param string $psJsRef Refer�ncia para arquivos javascript (url)
	 */
	public function setJsRef($psJsRef) {
		$this->csJsRef = $psJsRef;
	}

	/**
	 * Retorna o valor do atributo JsRef.
	 *
	 * <p>Retorna o valor do atributo JsRef (refer�ncia para arquivos javascript).</p>
	 * @access public
	 * @return string Refer�ncia para arquivos javascript (url)
	 */
	public function getJsRef() {
		return $this->csJsRef;
	}

	/**
	 * Seta o valor do atributo SysJsRef.
	 *
	 * <p>Seta o valor do atributo SysJsRef (refer�ncia para arquivos javascript
	 * do sistema).</p>
	 * @access public
	 * @param string $psSysJsRef Refer�ncia para arquivos javascript do sistema
	 */
	public function setSysJsRef($psSysJsRef){
		$this->csSysJsRef = $psSysJsRef;
	}

	/**
	 * Retorna o valor do atributo SysJsRef.
	 *
	 * <p>Retorna o valor do atributo SysJsRef (refer�ncia para arquivos javascript
	 * do sistema).</p>
	 * @access public
	 * @return string Refer�ncia para arquivos javascript do sistema
	 */
	public function getSysJsRef(){
		return $this->csSysJsRef;
	}

	/**
	 * Seta o valor do atributo GfxRef.
	 *
	 * <p>Seta o valor do atributo GfxRef (refer�ncia para arquivos de imagens).</p>
	 * @access public
	 * @param string $psGfxRef Refer�ncia para arquivos de imagens (url)
	 */
	public function setGfxRef($psGfxRef) {
		$this->csGfxRef = $psGfxRef;
	}

	/**
	 * Retorna o valor do atributo GfxRef.
	 *
	 * <p>Retorna o valor do atributo GfxRef (refer�ncia para arquivos de imagens).</p>
	 * @access public
	 * @return string Refer�ncia para arquivos de imagens (url)
	 */
	public function getGfxRef() {
		return $this->csGfxRef;
	}

	/**
	 * Seta o valor do atributo CSSRef.
	 *
	 * <p>Seta o valor do atributo CSSRef (refer�ncia para arquivos de CSS).</p>
	 * @access public
	 * @param string $psCSSRef Refer�ncia para arquivos de CSS
	 */
	public function setCSSRef($psCSSRef) {
		$this->csCSSRef = $psCSSRef;
	}

	/**
	 * Retorna o valor do atributo CSSRef.
	 *
	 * <p>Retorna o valor do atributo CSSRef (refer�ncia para arquivos de css).</p>
	 * @access public
	 * @return string Refer�ncia para arquivos de imagens CSS
	 */
	public function getCSSRef() {
		return $this->csCSSRef;
	}

	/**
	 * Seta o valor do atributo SysRefBasedOnTabMain.
	 *
	 * <p>Seta o valor do atributo SysRefBasedOnTabMain (refer�ncia para sys_ref baseado
	 * no diret�rio onde encontra-se o arquivo tab_main.php).</p>
	 * @access public
	 * @param string $psSysRefBasedOnTabMain Refer�ncia para sys_ref baseado no tab_main
	 */
	public function setSysRefBasedOnTabMain($psSysRefBasedOnTabMain) {
		$this->csSysRefBasedOnTabMain = $psSysRefBasedOnTabMain;
	}

	/**
	 * Retorna o valor do atributo SysRefBasedOnTabMain.
	 *
	 * <p>Retorna o valor do atributo SysRefBasedOnTabMain (refer�ncia para sys_ref baseado
	 * no diret�rio onde encontra-se o arquivo tab_main.php).</p>
	 * @access public
	 * @return string Refer�ncia para sys_ref baseado no tab_main
	 */
	public function getSysRefBasedOnTabMain() {
		return $this->csSysRefBasedOnTabMain;
	}

	/**
	 * Retorna o valor do atributo cbDumpStarted.
	 *
	 * <p>Retorna o valor do atributo cbDumpStarted (determina se o dump_html foi iniciado).</p>
	 * @access public
	 * @return boolean True ou False
	 */
	public function isDumpStarted() {
		return $this->cbDumpStarted;
	}

	/**
	 * Retorna o valor do atributo cbXmlLoadRunning.
	 *
	 * <p>Retorna o valor do atributo cbXmlLoadRunning (determina se o xml_load est� rodando).</p>
	 * @access public
	 * @return boolean True ou False
	 */
	public function isXmlLoadRunning() {
		return $this->cbXmlLoadRunning;
	}

	/**
	 * Obt�m uma sess�o armazenada
	 *
	 * <p>Recebe um objeto sess�o e, se ele j� existe armazenado, retorna a vers�o
	 * armazenada, sen�o retorna ele mesmo.</p>
	 * @access public
	 * @param FWDSession $poSession Sess�o
	 * @return FWDSession Sess�o
	 */
	public function getSession($poSession) {
		$this->checkSession();
		$msId = $poSession->getId();
		if (!isset($_SESSION[$msId])) {
			$_SESSION[$msId] = $poSession;
		}
		return $_SESSION[$msId];
	}

	/**
	 * Obt�m uma sess�o a partir de seu id.
	 *
	 * <p>M�todo para obter uma sess�o a partir de seu identificador.</p>
	 * @access public
	 * @param string $psSessionId Id da sess�o
	 * @return FWDSession Sess�o
	 */
	public function getSessionById($psSessionId) {
		$this->checkSession();
		if (!isset($_SESSION[$psSessionId]))
		trigger_error("There is no session with the id '{$psSessionId}'!", E_USER_ERROR);
		return $_SESSION[$psSessionId];
	}

	/**
	 * Inicia a sess�o caso ela ainda n�o tenha sido iniciada.
	 *
	 * <p>M�todo para iniciar a sess�o caso ela ainda n�o tenha sido iniciada.</p>
	 * @access private
	 */
	public function checkSession() {
		if (!$this->cbSessionStarted) {
			session_start();
			$this->cbSessionStarted = true;
		}
	}

	/**
	 * Destr�i uma sess�o
	 *
	 * <p>Destr�i uma sess�o.</p>
	 * @access public
	 * @param string $psSessionId Id da sess�o a ser destru�da
	 */
	public function destroySession($psSessionId) {
		unset($_SESSION[$psSessionId]);
	}

	/**
	 * Retorna o metodo a ser utilizado para carregar os objetos PHP
	 *
	 * <p>Retorna o metodo a ser utilizado para carregar os objetos PHP.</p>
	 * @access public
	 * @return integer Constante que indica o metodo a ser utilizado para carregar os objetos PHP
	 */
	public function getXmlLoadMethod() {
		return $this->ciXmlLoadMethod;
	}

	/**
	 * Retorna a URL atual
	 *
	 * <p>Retorna a URL atual (sem a query string).</p>
	 * @access public
	 * @return string URL atual
	 */
	public static function getCurrentURL(){
		if(isset($_SERVER['HTTPS']))
		$msProtocol = ($_SERVER['HTTPS']=='on'?'https':'http');
		else
		$msProtocol = 'http';

		return "$msProtocol://${_SERVER['HTTP_HOST']}${_SERVER['PHP_SELF']}";
  }

  /**
   * Converte a string para ISO.
   *
   * <p>M�todo para converter a string para ISO.</p>
   * @access public
   * @return string String no formato ISO
   */
  public static function convertToISO($psValue, $pbForce = false){return $psValue;
  /*VERIFICAR EM QUE SITUA��O � UM ARRAY*/
  if (!is_array($psValue)) {
  	$msValue = $psValue;
  	if (!$pbForce) {
  		$msEncoding = mb_detect_encoding($msValue,"ISO-8859-1,UTF-8");
  		if ($msEncoding == 'UTF-8') $msValue = mb_convert_encoding($psValue,"ISO-8859-1", "UTF-8");
  	}
  	else $msValue = mb_convert_encoding($msValue,"ISO-8859-1", "UTF-8");
  	return $msValue;
  }
  else return $psValue;
  }

  /**
   * Envia os headers para for�ar download
   *
   * <p>Envia os headers para for�ar download</p>
   * @access public
   * @param string $psFilename Nome do arquivo (opcional)
   */
  public static function forceDownload($psFilename=''){
  	header("Cache-Control: ");// leave blank to avoid IE errors
  	header("Pragma: ");// leave blank to avoid IE errors
  	header("Content-type: application/octet-stream");
  	if($psFilename){
  		header("Content-Disposition: attachment; filename=\"".rawurlencode($psFilename)."\"");
  	}
  }

  /**
   * Retorna o Usu�rio que deve ser utilizado como prefixo das functions do banco
   *
   * <p>Usu�rio que deve ser utilizado como prefixo das functions do banco</p>
   * @access public
   * @return string Usu�rio da conex�o com o banco que deve ser utilizado antes do nome das functions
   */
  public static function getFunctionUserDB(){
  	return self::$coDBConnection->getFunctionUserDB();
  }

  /**
   * Retorna a chamada da fun��o de acordo com o banco de dados.
   *
   * <p>M�todo para retornar a chamada da fun��o de acordo com o banco de dados.</p>
   * @access public
   * @param string $psFunction Fun��o que deve ser chamada
   * @return string Chamada da fun��o
   */
  public static function getFunctionCall($psFunction){
  	return self::$coDBConnection->getFunctionCall($psFunction);
  }

  /**
   * Retorna uma express�o de "like" case insensitive entre uma coluna e uma string constante.
   *
   * <p>Retorna uma express�o de "like" case insensitive entre uma coluna e uma
   * string constante de acordo com o banco sendo usado.</p>
   * @access public
   * @param string $psColumn Nome/express�o da coluna
   * @param string $psString A string constante
   * @return string Uma string contendo a express�o
   */
  public static function getCaseInsensitiveLike($psColumn,$psString){
  	if(self::$coDBConnection->getDatabaseType()==DB_MSSQL){
  		return "{$psColumn} LIKE '%{$psString}%'";
  	}else{
  		//return "UPPER({$psColumn}) LIKE '%".strtoupper($psString)."%'";
  		return "UPPER({$psColumn}) LIKE UPPER('%".$psString."%')";
  	}
  }

  /**
   * Retorna o banco de dados que est� sendo usado pelo sistema.
   *
   * <p>M�todo para retornar o banco de dados que est� sendo usado pelo sistema.</p>
   * @access public
   * @return integer Constante que identifica qual o banco que est� sendo usado.
   */
  public static function getDatabaseType(){
  	return self::$coDBConnection->getDatabaseType();
  }

  /**
   * Retorna um valor constante do banco de dados.
   *
   * <p>M�todo para retornar um valor constante do banco de dados. Necess�rio
   * pois no Oracle n�o s�o aceitas consultas no formato 'select 1' e sim
   * no formato 'select 1 from dual'.</p>
   * @access public
   * @param string $psValue Valores [Ex: '1 as first, 2 as second, ...]
   * @return string Select para retornar uma constante
   */
  public function selectConstant($psValues) {
  	return self::$coDBConnection->selectConstant($psValues);
  }

  /**
   * Abstra��o da fun��o array_diff_key que s� existe a partir do php 5.1.0
   *
   * <p>abstrai a existencia da fun��o array_diff_key do php, para que funcione no php 5.0.x</p>
   * @access public
   * @param Array $maPar1 array cujas keys nao devem estar no $maPar2 para essa key e seu valor irem para o resultado
   * @param Array $maPar2 array alvo da compara��o
   * @return Array array associativo com o resultado do array_diff_key
   */
  public static function fwd_array_diff_key($maPar1,$maPar2){
  	if(function_exists('array_diff_key')){
  		return array_diff_key($maPar1,$maPar2);
  	}
  	else
  	{
  		$maReturn = array();
  		foreach($maPar1 as $miKey => $miValue){
  			if(!isset($maPar2[$miKey]))
  			$maReturn[$miKey] = $miValue;
  		}
  		return $maReturn;
  	}
  }

  /**
   * Retorna a hora local corrigindo erro de GMT.
   *
   * <p>O PHP est� considerando o timezone do sistema operacional para calcular a hora
   * Esta fun��o retorna a hora local corrigindo este erro.</p>
   * @access public
   * @return timestamp
   */
  public static function getTime(){
  	return time();
  }

  /**
   * Retorna um timestamp baseado em uma data no formato YYYY-MM-DD HH-NN-SS.
   *
   * <p>M�todo para retornar um timestamp baseado em uma data no formato YYYY-MM-DD HH-NN-SS.</p>
   * @access public
   * @param string $psDate Data
   * @return integer Timestamp
   */
  public static function getTimestamp($psDate){
  	return self::getInstance()->getConnection()->getTimestamp($psDate);
  }

  /**
   * Retorna um timestamp baseado em uma data em um dos formatos aceitos.
   *
   * <p>Retorna um timestamp baseado em uma data em um dos seguintes formatos:</p>
   * <p>timestamp</p>
   * <p>YYYY-MM-DD</p>
   * <p>YYYY-MM-DD HH-NN-SS</p>
   * <p>mx_shortdate_format</p>
   * @access public
   * @param string $pmDate Data
   * @return integer Timestamp
   */
  public static function dateToTimestamp($pmDate){
  	if($pmDate){
  		if(is_int($pmDate)){
  			return $pmDate;
  		}elseif(is_string($pmDate)){
  			if(preg_match('/^\d{4}-\d{2}-\d{2}/',$pmDate)){
  				return strtotime($pmDate);
  			}elseif(preg_match('|^\d{1,2}/\d{1,2}/\d{1,4}$|',$pmDate)){
  				$msDateFormat = FWDLanguage::getPHPStringValue('mx_shortdate_format','');
  				$maDate = explode('/',$pmDate);
  				if($msDateFormat=='%d/%m/%Y'){
  					return mktime(0, 0, 0, $maDate[1], $maDate[0], $maDate[2]);
  				}elseif($msDateFormat=='%m/%d/%Y'){
  					return mktime(0, 0, 0, $maDate[0], $maDate[1], $maDate[2]);
  				}
  			}
  		}
  	}else{
  		return 0;
  	}
  	trigger_error("Invalid date format.",E_USER_ERROR);
  }

  /**
   * Retorna uma data no formato do banco.
   *
   * <p>M�todo para retornar uma data no formato do banco.</p>
   * @access public
   * @param string $piTimestamp Timestamp
   * @return string Data
   */
  public static function timestampToDBFormat($piTimestamp){
  	return self::getInstance()->getConnection()->timestampFormat($piTimestamp);
  }

  /**
   * Retorna uma data formatada para exibi��o.
   *
   * <p>M�todo para retornar uma data (com horas e minutos) formatada para exibi��o.</p>
   * @access public
   * @param mixed $pmDate Timestamp ou string de data
   * @param boolean $pbIsTimestamp Indica se o primeiro par�metro � um timestamp ou uma string de data
   * @return string Data formatada
   */
  public static function getDate($pmDate = '', $pbIsTimestamp = false){
  	if(!$pbIsTimestamp){
  		if($pmDate) $miTimestamp = self::getTimestamp($pmDate);
  		else $miTimestamp = self::getTime();
  	}else{
  		$miTimestamp = $pmDate;
  	}

  	$msDateFormat = FWDLanguage::getPHPStringValue('mx_shortdate_format','');
  	if($msDateFormat == "%m/%d/%Y"){
  		return date("m/d/Y h:i a", $miTimestamp);
  	}elseif($msDateFormat == "%d/%m/%Y"){
  		return date("d/m/Y H:i", $miTimestamp);
  	}else{
  		trigger_error("Invalid date format!", E_USER_WARNING);
  	}
  }

  /**
   * Retorna uma data "curta" formatada para exibi��o.
   *
   * <p>M�todo para retornar uma data "curta" (dia, m�s e ano) formatada para exibi��o.</p>
   * @access public
   * @param mixed $pmDate Timestamp ou string de data
   * @param boolean $pbIsTimestamp Indica se o primeiro par�metro � um timestamp ou uma string de data
   * @return string Data formatada
   */
  public static function getShortDate($pmDate = '', $pbIsTimestamp = false){
  	if(!$pbIsTimestamp){
  		if($pmDate) $miTimestamp = self::getTimestamp($pmDate);
  		else $miTimestamp = self::getTime();
  	}else{
  		$miTimestamp = $pmDate;
  	}

  	$msDateFormat = FWDLanguage::getPHPStringValue('mx_shortdate_format','');
  	if($msDateFormat == "%m/%d/%Y"){
  		return date("m/d/Y", $miTimestamp);
  	}elseif($msDateFormat == "%d/%m/%Y"){
  		return date("d/m/Y", $miTimestamp);
  	}else{
  		trigger_error("Invalid date format!", E_USER_WARNING);
  	}
  }

  /**
   * M�todo para deserializar uma string.
   *
   * <p>M�todo para deserializar uma string. Criado para resolver
   * erros que ocorriam com strings codificadas em UTF-8</p>
   * @access public
   * @param string $psString String
   * @return mixed Valor deserializado
   */
  public static function unserializeString($psString, $pbReplace=true) {
  	if ($pbReplace) return unserialize(preg_replace('!s:(\d+):"(.*?)";!se', "'s:'.strlen('$2').':\"$2\";'", $psString));
  	else return unserialize($psString);
  }

  /**
   * M�todo para para abstrair a fun��o utf8_decode do php.
   *
   * <p>M�todo para para abstrair a fun��o utf8_decode do php.</p>
   * @access public
   * @param string $psString String
   * @return string String decodificada
   */
  public static function decodeUTF8($psString) {
  	return utf8_decode($psString);
  }

  /**
   * Escreve uma string no arquivo de debug.
   *
   * <p>Escreve uma string no arquivo de debug.</p>
   * @access public
   * @param string psString String a escrever
   */
  public function writeStringDebug($psString="",$piType=FWD_DEBUG_INFO) {
  	if ($this->getDebugMode()) {
  		if ($piType & self::$ciDebugType) {
  			$msSessionId = session_id()?session_id():'none';
  			switch($piType) {
  				case FWD_DEBUG_DB: $msDebugType='DB'; break;
  				case FWD_DEBUG_INFO: $msDebugType='INFO'; break;
  				case FWD_DEBUG_ERROR: $msDebugType='ERROR'; break;
  				case FWD_DEBUG_FWD1: $msDebugType='FWD1'; break;
  				case FWD_DEBUG_FWD2: $msDebugType='FWD2'; break;
  				default: $msDebugType='DEBUG'; break;
  			}
  			$msString = "($msDebugType) ".date('Y-m-d H:i:s')." [Session Id: {$msSessionId}] {$psString}";
  			$moCrypt = new FWDCrypt();
  			if (self::$cbDebugEncrypted) $msString = $moCrypt->getIV().$moCrypt->encrypt($msString);
  			$msString = PHP_EOL."#%#".(self::$cbDebugEncrypted?"D":"d").$msString."\n#%#".PHP_EOL;
  			$msDebugFile = self::getInstance()->getSysRef().self::getDebugFilePath();
  			$moHandle = fopen($msDebugFile, "a+");
  			fwrite($moHandle, $msString);
  		}
  	}
  	return;
  }

  public function arrayToString(&$item,$key) {
  	if (is_array($item)) {
  		$temp_array = $item;
  		array_walk($temp_array, array($this,'arrayToString'));
  		$item = "[array] {".implode(",",$temp_array)."}";
  	} elseif(is_object($item) && !method_exists($item,'__toString')){
  		$item = "[".get_class($item)." object]";
  	}
  }

  public function writeFunction2Debug($psClassName,$psFunctionName,$paAttributes,$piType=FWD_DEBUG_INFO,$psFileName='',$piLineNumber='') {
  	if ($this->getDebugMode()) {
  		if ($piType & self::$ciDebugType) {
  			foreach($paAttributes as $miIndex=>$mmAttribute){
  				if(is_object($mmAttribute) && !method_exists($mmAttribute,'__toString')){
  					$paAttributes[$miIndex] = "[".get_class($mmAttribute)." object]";
  				} elseif(is_array($mmAttribute)) {
  					array_walk($mmAttribute,array($this,'arrayToString'));
  					$paAttributes[$miIndex] = "[array] { ".implode(',',$mmAttribute)." }";
  				}
  			}
  			$msString = ($psFileName?"[$psFileName: $piLineNumber] ":"").$psClassName.": ".$psFunctionName."(".implode(",",$paAttributes).")";
  			$this->writeStringDebug($msString,$piType);
  		}
  	}
  }

}

?>
