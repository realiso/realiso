<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe para criptografar e decriptografar.
 *
 * <p>Classe para criptografar e decriptografar.</p>
 * @package FWD5
 * @subpackage base
 */

class FWDCrypt {
	protected $csPublicKey = "-----BEGIN CERTIFICATE-----
MIIEFzCCA4CgAwIBAgIBATANBgkqhkiG9w0BAQQFADCBujELMAkGA1UEBhMCQlIx
GjAYBgNVBAgTEVJpbyBHcmFuZGUgZG8gU3VsMRUwEwYDVQQHEwxQb3J0byBBbGVn
cmUxIjAgBgNVBAoTGUF4dXIgSW5mb3JtYXRpb24gU2VjdXJpdHkxFjAUBgNVBAsT
DWxpY2Vuc2UgZGVwdC4xHDAaBgNVBAMTE0F4dXIgTGljZW5zZSBTeXN0ZW0xHjAc
BgkqhkiG9w0BCQEWD2Fsc0BheHVyLmNvbS5icjAeFw0wNDA2MDkxNzU0NTBaFw0w
OTA2MTAxNzU0NTBaMIG6MQswCQYDVQQGEwJCUjEaMBgGA1UECBMRUmlvIEdyYW5k
ZSBkbyBTdWwxFTATBgNVBAcTDFBvcnRvIEFsZWdyZTEiMCAGA1UEChMZQXh1ciBJ
bmZvcm1hdGlvbiBTZWN1cml0eTEWMBQGA1UECxMNbGljZW5zZSBkZXB0LjEcMBoG
A1UEAxMTQXh1ciBNb25pdG9yIFN5c3RlbTEeMBwGCSqGSIb3DQEJARYPYW1zQGF4
dXIuY29tLmJyMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCzKZ+FEPy6DbvF
IOQ6hp6pCnth0kr5FFSh39pMsq0ytv4RRAxnWxjPvyfx0w7vpnKbARF8zDaStN5K
JIxocA7SYmYM2ItYZdAu+6JU1m9/EYT8eb7w110cYdEIJJr+CPrx2iivXVYBbl9J
VkKlf/1xFnkwhIlZ6CYAwFfnxdIswwIDAQABo4IBKTCCASUwCQYDVR0TBAIwADAP
BglghkgBhvhCAQ0EAhYAMB0GA1UdDgQWBBTAEH8Y7ZVtOpNxtuO5djrXmDp59zCB
5wYDVR0jBIHfMIHcgBTIsD9pVTsIJx25scfn1t6xQmdPZaGBwKSBvTCBujELMAkG
A1UEBhMCQlIxGjAYBgNVBAgTEVJpbyBHcmFuZGUgZG8gU3VsMRUwEwYDVQQHEwxQ
b3J0byBBbGVncmUxIjAgBgNVBAoTGUF4dXIgSW5mb3JtYXRpb24gU2VjdXJpdHkx
FjAUBgNVBAsTDWxpY2Vuc2UgZGVwdC4xHDAaBgNVBAMTE0F4dXIgTGljZW5zZSBT
eXN0ZW0xHjAcBgkqhkiG9w0BCQEWD2Fsc0BheHVyLmNvbS5icoIBADANBgkqhkiG
9w0BAQQFAAOBgQAabwBzSL+Ee3up3MxfE0gz4UAmjtaJMTlmclz+fGX22CVVwMXX
1WhH4QacRA9UnZp5tYEAwPMXpOGrlgR0tAkI3O5SaLaki6qc07h5mqfczN2L2BMp
LvUwF7gTTmNITm+ooJRP96Q+16G/UhUp4FNQvJyG2OmovKnH/8ZKuGgwlQ==
-----END CERTIFICATE----- ";

	protected $csAlgorithm;
	protected $csMode;
	protected $coIV;
	protected $csKey;

	public function __construct() {
		$this->csAlgorithm=MCRYPT_BLOWFISH;
		$this->csMode=MCRYPT_MODE_CBC;
		$moModule = mcrypt_module_open($this->csAlgorithm,'',$this->csMode,'');
		$this->coIV = mcrypt_create_iv(mcrypt_enc_get_iv_size($moModule),MCRYPT_RAND);
		/*if (strpos($this->csPublicKey,"\r\n")===false) {
		 $this->csKey = substr(md5(str_replace("\n","\r\n",$this->csPublicKey)),0,mcrypt_enc_get_key_size($moModule));
		 } else {
		 $this->csKey = substr(md5($this->csPublicKey),0,mcrypt_enc_get_key_size($moModule));
		 }*/
		//O md5 abaixo � o md5 da chave p�blica acima, pois n�o faz diferen�a usar diretamente o c�digo md5 ou faz�-lo no momento em que � rodado o script, podendo gerar os bugs de codifica��o que ocorriam antes.
		$this->csKey = substr('22a8ceb57d73e0ec7e78fcc3d23fb480',0,mcrypt_enc_get_key_size($moModule));
		mcrypt_module_close($moModule);
	}

	public function setIV($psSource) {
		$moModule = mcrypt_module_open($this->csAlgorithm,'',$this->csMode,'');
		if (strlen($psSource) == mcrypt_enc_get_iv_size($moModule))
		$this->coIV = $psSource;
		mcrypt_module_close($moModule);
	}

	public function getIV() {
		return $this->coIV;
	}

	public function encrypt($psSource) {
		if (!trim($psSource))
		return '';
		$moModule = mcrypt_module_open($this->csAlgorithm,'',$this->csMode,'');
		mcrypt_generic_init($moModule, $this->csKey, $this->coIV);
		$enc = mcrypt_generic($moModule, base64_encode($psSource));
		mcrypt_generic_deinit($moModule);
		mcrypt_module_close($moModule);
		return base64_encode($enc)."\n";
	}

	public function decrypt($psSource) {
		$moModule = mcrypt_module_open($this->csAlgorithm,'',$this->csMode,'');
		if (!$this->coIV)
		return '';
		if (!trim($psSource))
		return '';
		mcrypt_generic_init($moModule, $this->csKey, $this->coIV);
		$dec = rtrim(mdecrypt_generic($moModule, base64_decode($psSource)),"\0");
		mcrypt_generic_deinit($moModule);
		mcrypt_module_close($moModule);
		return base64_decode($dec);
	}

	public function encryptNoBase64($psSource) {
		if (!trim($psSource))
		return '';
		$moModule = mcrypt_module_open($this->csAlgorithm,'',$this->csMode,'');
		mcrypt_generic_init($moModule, $this->csKey, $this->coIV);
		$enc = mcrypt_generic($moModule, $psSource);
		mcrypt_generic_deinit($moModule);
		mcrypt_module_close($moModule);
		return $enc;
	}

	public function decryptNoBase64($psSource) {
		if (!trim($psSource))
		return '';
		$moModule = mcrypt_module_open($this->csAlgorithm,'',$this->csMode,'');
		if (!$this->coIV)
		return '';
		mcrypt_generic_init($moModule, $this->csKey, $this->coIV);
		$dec = mdecrypt_generic($moModule, $psSource);
		mcrypt_generic_deinit($moModule);
		mcrypt_module_close($moModule);
		return $dec;
	}

	public function decryptEmbeddedIV($psSource) {
		$this->setIV(substr($psSource,0,strlen($this->getIV())));
		return $this->decrypt(substr($psSource,strlen($this->getIV())));
	}
}
?>