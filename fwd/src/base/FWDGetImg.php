<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Exibe uma imagem armazenada numa vari�vel de sess�o.
 * O nome da vari�vel de sess�o onde a imagem est� armazenada precisa ter um
 * determinado prefixo e ser passado por get com o nome 'img'.
 */

session_start();

/**
 * Prefixo que a vari�vel de sess�o precisa ter
 * @var string
 */
$lsPrefix = 'fwdchart_';

if(isset($_GET['img'])){
	$lsImgId = $_GET['img'];
	if(substr($lsImgId,0,strlen($lsPrefix))==$lsPrefix && isset($_SESSION[$lsImgId])){
		header("Content-type: image/".$_SESSION[$lsImgId.'_contentType']);
		echo $_SESSION[$lsImgId];
	}
}

?>