<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

define('LANGUAGE_DEFAULT', 3300);

/**
 * Classe FWDLanguage. Classe que faz a tradu��o das strings.
 *
 * <p>Classe est�tica que faz a tradu��o das strings, tanto do XML, quanto do
 * PHP.</p>
 * @package FWD5
 * @subpackage base
 */
class FWDLanguage {

	/**
	 * Array contendo os caminhos dos arquivos de linguagem
	 * @var array
	 * @access private
	 */
	private static $caLanguages = array();

	/**
	 * Array contendo os identificadores das linguagem default
	 * Linguagem na qual as string que est�o no PHP e no XML s�o escritas
	 * Utilizado para saber quando o xml_load deve usar o dicion�rio e quando
	 * ele deve utilizar o valor original (dos PHPs e XMLs)
	 * @var array
	 * @access private
	 */
	private static $caDefaultLanguages = array(LANGUAGE_DEFAULT);

	/**
	 * Array contendo os nomes das linguagens
	 * @var array
	 * @access private
	 */
	private static $caLanguagesName = array();

	/**
	 * Array contendo os valores traduzidos das strings
	 * @var array
	 * @access private
	 */
	private static $caStrings = array();

	/**
	 * Array contendo os valores default de algumas strings
	 * Utilizado principalmente pelas string de KnownErrors
	 * @var array
	 * @access private
	 */
	private static $caStringsDefault = array();

	/**
	 * Identificador da linguagem selecionada
	 * @var integer
	 * @access private
	 */
	private static $ciSelectedLanguage = null;
	 
	/**
	 * Construtor. N�o � usado.
	 *
	 * <p>Construtor. N�o � usado, serve s� para tornar a classe est�tica, pois o
	 * seu acesso � private.</p>
	 * @access private
	 */
	private function __construct(){
	}

	/**
	 * Adiciona uma linguagem ao array de linguagens
	 *
	 * <p>Adiciona uma linguagem ao array de linguagens.</p>
	 * @access public
	 * @param integer piLanguageId Identificador da linguagem
	 * @param string piLanguageName Nome da linguagem
	 * @param string psStringFile Caminho do arquivo contendo as strings
	 */
	public static function addLanguage($piLanguageId,$psLanguageName,$psStringFile){
		self::$caLanguages[$piLanguageId] = $psStringFile;
		self::$caLanguagesName[$piLanguageId] = $psLanguageName;
	}

	/**
	 * Seta o nome da linguagem.
	 *
	 * <p>M�todo para setar o nome da linguagem.</p>
	 * @access public
	 * @param integer piLanguageId Identificador da linguagem
	 * @param string piLanguageName Nome da linguagem
	 */
	public static function setLanguageName($piLanguageId,$psLanguageName){
		self::$caLanguagesName[$piLanguageId] = $psLanguageName;
	}

	/**
	 * Adiciona uma linguagem default ao array de linguagens default
	 * Linguagem na qual as string que est�o no PHP e no XML s�o escritas
	 *
	 * <p>Adiciona uma linguagem default ao array de linguagens default</p>
	 * @access public
	 * @param integer piLanguageId Identificador da linguagem
	 */
	public static function addDefaultLanguage($piLanguageId){
		self::$caDefaultLanguages[] = $piLanguageId;
	}

	/**
	 * Seleciona uma linguagem do array de linguagens
	 *
	 * <p>Seleciona uma linguagem do array de linguagens.</p>
	 * @access public
	 * @param integer piLanguageId Identificador da linguagem
	 * @return boolean True indica sucesso
	 */
	public static function selectLanguage($piLanguageId){

		if ($piLanguageId == LANGUAGE_DEFAULT) return false;
		elseif(!isset(self::$caLanguages[$piLanguageId])){
			//trigger_error("Unknown language id: $piLanguageId",E_USER_ERROR);
			return false;
		}

		self::$ciSelectedLanguage = $piLanguageId;
		$msStringFile = self::$caLanguages[$piLanguageId];

		if(!file_exists($msStringFile) || !is_readable($msStringFile)){
			trigger_error("Could not read file '$msStringFile'.",E_USER_ERROR);
			return false;
		}

		require($msStringFile);

		if(isset($gaStrings)){
			if(empty(self::$caStringsDefault)) {
				self::$caStringsDefault = self::$caStrings;
			}
			self::$caStrings = array_merge(self::$caStringsDefault,$gaStrings);
			unset($gaStrings);
		}else{
			trigger_error("File '$msStringFile' does not contain \$gaStrings array.",E_USER_ERROR);
			return false;
		}
		return true;
	}

	/**
	 * Substitui uma string no c�digo PHP pela sua tradu��o.
	 *
	 * <p>Substitui uma string no c�digo PHP pela sua tradu��o.</p>
	 * @access public
	 * @param string psId Id da string a ser traduzida
	 * @param string psValue String a ser traduzida
	 * @param boolean pbForce For�a o novo valor mesmo que a string j� exista.
	 * @return string String traduzida
	 */
	public static function getPHPStringValue($psId,$psValue="",$pbForce=false){
		if($pbForce || !isset(self::$caStrings[$psId]) || empty(self::$caStrings[$psId]))
		self::$caStrings[$psId] = utf8_encode(FWDWebLib::convertToISO($psValue));
		return self::$caStrings[$psId];
	}

	/**
	 * Substitui uma string no c�digo XML pela sua tradu��o.
	 *
	 * <p>Se o id da string existir no array de tradu��o, retorna o valor
	 * traduzido, sen�o retorna o pr�prio valor recebido como par�metro.</p>
	 * @access public
	 * @param string psId Id da string a ser traduzida
	 * @param string psValue String a ser traduzida
	 * @return string String traduzida
	 */
	public static function getXMLStringValue($psId,$psValue){
		if(isset(self::$caStrings[$psId]) && !empty(self::$caStrings[$psId])){
			return self::$caStrings[$psId];
		}else{
			return $psValue;
		}
	}

	/**
	 * Obtem o identificados da linguagem selecionada
	 *
	 * <p>Obtem o identificados da linguagem selecionada.</p>
	 * @access public
	 * @return integer Identificador
	 */
	public static function getSelectedLanguage(){
		return self::$ciSelectedLanguage;
	}

	/**
	 * Retorna os identificadores das linguagens default
	 *
	 * <p>Retorna os identificadores das linguagens default</p>
	 * @access public
	 * @return array Identificadores
	 */
	public static function getDefaultLanguages(){
		return self::$caDefaultLanguages;
	}

	/**
	 * Retorna as linguagens do sistema.
	 *
	 * <p>M�todo para retornar as linguagens do sistema.</p>
	 * @access public
	 * @param boolean $pbDefault Return Default language in results
	 * @return array Array[LanguageId] => LanguageName
	 */
	public static function getLanguages($pbDefault=false){
		if ($pbDefault) {
			$maReturn = self::$caLanguagesName;
			$maReturn[LANGUAGE_DEFAULT] = FWDLanguage::getPHPStringValue('default_language', 'Portugu�s');
			return $maReturn;
		} else {
			if (count(self::$caLanguagesName)) return self::$caLanguagesName;
			else return array(LANGUAGE_DEFAULT => FWDLanguage::getPHPStringValue('default_language', 'Portugu�s'));
		}
	}
}
?>