<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDDoNothingErrorHandler. Error Handler para mascarar erros.
 *
 * <p>Error Handler para mascarar erros.</p>
 * @package FWD5
 * @subpackage base
 */
class FWDDoNothingErrorHandler implements FWDErrorHandler {

	protected $cbErrorCaptured = false;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDDoNothingErrorHandler.</p>
	 * @access public
	 */
	public function __construct(){
	}

	/**
	 * Error Handler para fazer nada.
	 *
	 * <p>Error Handler para fazer nada.</p>
	 * @access public
	 * @param integer $piErrNo N�mero do erro
	 * @param string $psErrStr Mensagem de erro
	 * @param string $psErrFile Nome do arquivo que gerou o erro
	 * @param integer $piErrLine N�mero da linha em que ocorreu o erro
	 */
	public function handleError($piErrNo, $psErrStr, $psErrFile, $piErrLine){
		$this->cbErrorCaptured = true;
	}

	/**
	 * Exception Handler para fazer nada.
	 *
	 * <p>Exception Handler para fazer nada.</p>
	 * @access public
	 * @param FWDException $poFWDException Exce��o
	 * @static
	 */
	public static function handleException($poFWDException){
	}

	/**
	 * Indica se o error handler capturou algum erro.
	 *
	 * <p>Indica se o error handler capturou algum erro.</p>
	 * @access public
	 * @return boolean True sse o error handler capturou algum erro
	 */
	public function errorCaptured(){
		return $this->cbErrorCaptured;
	}

}

?>