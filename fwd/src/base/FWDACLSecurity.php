<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDACLSecurity. Singleton. Implementa a Access Control List da FWD5.
 *
 * <p>Classe singleton que implementa a Access Control List da FWD5. Percorre
 * todos os elementos da tela, desabilitando os elementos para os quais o
 * usu�rio n�o possui permiss�o. Garante a exist�ncia de uma e apenas uma
 * inst�ncia da classe FWDACLSecurity.</p>
 * @package FWD5
 * @subpackage base
 */
class FWDACLSecurity {

	/**
	 * Inst�ncia �nica da classe FWDACLSecurity
	 * @staticvar FWDACLSecurity
	 * @access private
	 */
	private static $coACLSecurity = NULL;

	/**
	 * Array de tags que identificam elementos os quais n�o devem ser desenhados
	 * @var array
	 * @access private
	 */
	private $caNotAllowed = array();

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDACLSecurity.</p>
	 * @access private
	 */
	private function __construct() {
	}

	/**
	 * Retorna a inst�ncia �nica da classe FWDACLSecurity (singleton).
	 *
	 * <p>Retorna a inst�ncia �nica da classe FWDACLSecurity. Se a inst�ncia
	 * ainda n�o existe, cria a �nica inst�ncia. Se a inst�ncia j� existe,
	 * retorna a �nica inst�ncia (singleton).</p>
	 * @access public
	 * @return FWDACLSecurity Inst�ncia �nica da classe FWDACLSecurity
	 * @static
	 */
	public static function getInstance() {
		if (self::$coACLSecurity == NULL) {
			self::$coACLSecurity = new FWDACLSecurity();
		}
		else {}	// inst�ncia �nica j� existe, retorne-a
		return self::$coACLSecurity;
	}

	/**
	 * Seta o array de tags de elementos que n�o devem ser desenhados.
	 *
	 * <p>M�todo que seta o array de tags de elementos que n�o devem ser
	 * desenhados.</p>
	 * @access public
	 * @param array $paNotAllowed Array de tags
	 */
	public function setNotAllowed($paNotAllowed) {
		$this->caNotAllowed = $paNotAllowed;
	}

	/**
	 * Desabilita subelementos da view, se necess�rio.
	 *
	 * <p>Desabilita subelementos da view, se necess�rio. Percorre todos os
	 * subelementos da view, desabilitando aqueles cuja tag estiver presente
	 * no array de tags passado como par�metro.</p>
	 *
	 * @access public
	 * @param FWDView $poView Elemento no qual ser�o desabilitados subelementos (se necess�rio)
	 * @param array $paNotAllowed Array com as Tags que identificam elementos que n�o devem ser desenhados
	 */
	public function installSecurity($poView,$paNotAllowed = array()) {
		if ($paNotAllowed)
		$this->caNotAllowed = $paNotAllowed;
		$this->matchACL($poView);
	}

	/**
	 * Desabilita subelementos da view, se necess�rio.
	 *
	 * <p>Desabilita subelementos da view, se necess�rio. Percorre todos os
	 * subelementos da view, desabilitando aqueles cuja tag estiver presente
	 * no array de tags para as quais o usu�rio n�o possui permiss�o.</p>
	 *
	 * @access private
	 * @param FWDView $poView Elemento no qual ser�o desabilitados subelementos (se necess�rio)
	 */
	private function matchACL(FWDView $poView) {
		$maViews = $poView->collect();
		foreach ($maViews as $moObject) {
			if(method_exists($moObject,"getAttrTag")){
				if($moObject->getAttrTag()){
					$maTags = explode(':',$moObject->getAttrTag());
					foreach($maTags as $msTag){
						if (($msTag) && in_array($msTag,$this->caNotAllowed)){
							$moObject->setShouldDraw(false);
						}
					}
				}
			}
		}
	}

}
?>