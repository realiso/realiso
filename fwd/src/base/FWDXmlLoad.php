<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDXmlLoad. Singleton. Transforma arquivos XML em objetos PHP.
 *
 * <p>Classe singleton que transforma arquivos XML em objetos PHP. Garante a
 * exist�ncia de uma e apenas uma inst�ncia da classe FWDXmlLoad.</p>
 * @package FWD5
 * @subpackage base
 */
class FWDXmlLoad {

	/**
	 * Inst�ncia �nica da classe FWDXmlLoad
	 * @staticvar FWDXmlLoad
	 * @access private
	 */
	private static $coXmlLoad = NULL;

	/**
	 * Contador para gerar ids �nicos
	 * @staticvar integer
	 * @access private
	 */
	private static $ciLastId = 0;

	/**
	 * Array de Views (usa ciXmlDepth para guardar informa��es de aninhamento/identa��o)
	 * @var array
	 * @access private
	 */
	private $caXmlRoot;

	/**
	 * Alvo da View
	 * @var array
	 * @access private
	 */
	private $caXmlTarget;

	/**
	 * Array de Views (todas as views da dialog)
	 * @var array
	 */
	private $caXmlObjects;

	/**
	 * Profundidade das tags xml (aninhamento/identa��o)
	 * @var integer
	 * @access private
	 */
	private $ciXmlDepth;

	/**
	 * Array de tags html
	 * @var array
	 * @access private
	 */
	private $caHtmlTags;

	/**
	 * Lista das classes que podem ter conte�do de texto
	 * @var array
	 * @access private
	 */
	private $caTextContainers;

	/**
	 * Vari�vel para verificar se o XMLLOAD est� sendo usado para compila��o
	 * @var integer
	 * @access private
	 */
	private $cbIsCompiling = false;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDXmlLoad.</p>
	 * @access private
	 */
	private function __construct() {
		$this->caXmlRoot = array();
		$this->caXmlTarget = array();
		$this->caXmlObjects = array();
		$this->caObjects = array();
		$this->ciXmlDepth = 0;
		$this->caHtmlTags = array( "img", "b", "br", "p", "div", "font", "i", "u", "a");
		$this->caTextContainers = array('FWDString','FWDClientEvent');
	}

	/**
	 * Retorna a inst�ncia �nica da classe FWDXmlLoad (singleton).
	 *
	 * <p>Retorna a inst�ncia �nica da classe FWDXmlLoad. Se a inst�ncia
	 * ainda n�o existir, cria a �nica inst�ncia. Se a inst�ncia j� existir,
	 * retorna a �nica inst�ncia (singleton).</p>
	 * @access public
	 * @return FWDXmlLoad Inst�ncia �nica da classe FWDXmlLoad
	 * @static
	 */
	public static function getInstance() {
		if (self::$coXmlLoad == NULL) {
			self::$coXmlLoad = new FWDXmlLoad();
		}
		else {}	// inst�ncia �nica j� existe, retorne-a
		return self::$coXmlLoad;
	}

	/**
	 * Limpa a inst�ncia �nica da classe FWDXmlLoad (singleton).
	 *
	 * <p>Limpa a inst�ncia �nica da classe FWDXmlLoad. Reseta as vari�veis
	 * do singleton FWDXmlLoad para os valores padr�o.</p>
	 * @access public
	 * @static
	 */
	public function cleanInstance() {
		$this->caXmlRoot = array();
		$this->caXmlTarget = array();
		$this->caXmlObjects = array();
		$this->ciXmlDepth = 0;
	}

	/**
	 * Exibe o conte�do da inst�ncia �nica FWDXmlLoad.
	 *
	 * <p>Exibe na tela o conte�do de todos os atributos da �nica inst�ncia
	 * da classe FWDXmlLoad.</p>
	 * @access public
	 */
	public function dump_instance() {
		echo "<br/><br/><br/><br/>--------------[ FWDXmlLoad Singleton Dump <strong>BEGIN</strong> ]--------------<br/>";
		echo "<h3>coXmlLoad:</h3>";
		var_dump(self::$coXmlLoad);
		echo "<br/><h3>caXmlRoot:</h3>";
		var_dump($this->caXmlRoot);
		echo "<br/><h3>caXmlTarget:</h3>";
		var_dump($this->caXmlTarget);
		echo "<br/><h3>caXmlObjects:</h3>";
		var_dump($this->caXmlObjects);
		echo "<br/><h3>ciXmlDepth:</h3>";
		var_dump($this->ciXmlDepth);
		echo "<br/><h3>caHtmlTags:</h3>";
		var_dump($this->caHtmlTags);
		echo "<h3></h3>--------------[ FWDXmlLoad Singleton Dump <strong>END</strong> ]--------------<br/><br/><br/><br/>";
	}

	// NAO USADA
	/**
	 * Seta os valores dos atributos necess�rios da view.
	 *
	 * <p>Seta os valores dos atributos necess�rios da view.</p>
	 * @access private
	 * @param object $poView View
	 * @param array $paAttribs Array de atributos cujos valores ser�o setados (se necess�rio)
	 * @param array $paNeeded Array de atributos necess�rios
	 */
	private function xml_decode_attribute_array($poView,$paAttribs,$paNeeded) {
		foreach( $paAttribs as $key => $value ) {
			$key = strtolower($key);
			if (in_array($key,$paNeeded))
			$poView->{$key} = $value;
		}
	}

	/**
	 * Abre uma tag que n�o � HTML.
	 *
	 * <p>Abre uma tag que n�o � HTML.</p>
	 * @access private
	 * @param string $psClass_Name Nome da classe (tag)
	 * @param array $paAttribs Atributos da tag
	 * @return boolean Indica se conseguiu abrir a tag adequadamente
	 */
	private function generic_start($psClass_Name,$paAttribs) {
		$psClass_Name_Orig = strtolower($psClass_Name);
		if(substr($psClass_Name,0,3)!='fwd'){
			$psClass_Name = "FWD".$psClass_Name_Orig;
		}
		if(!class_exists($psClass_Name)){
			if(class_exists($psClass_Name_Orig)) {
				$psClass_Name = $psClass_Name_Orig;
			}
			else {
				return false;
			}
		}

		$view = new $psClass_Name;

		$name = "";
		$this->caXmlRoot[$this->ciXmlDepth] = $view;

		$target = "";
		if (isset($paAttribs['TARGET']))
		$target = strtolower($paAttribs['TARGET']);
		$this->caXmlTarget[$this->ciXmlDepth] = $target;

		// check if it has a "name" attribute
		if (isset($paAttribs['NAME'])) {
			$name = $paAttribs['NAME'];
		}
		else {
			$name = $this->generateName();
		}

		if(method_exists($view,"setAttrName")) {
			$view->setAttrName($name);
			FWDWebLib::addObject($name,$view);
			//$this->caXmlObjects[$name] = $view;
		}

		if (!$this->ciXmlDepth) {
			if ($name)
			$this->caXmlObjects[$name] = $view;
			else
			$this->caXmlObjects[] = $view;
		}

		// decode attributes;
		foreach( $paAttribs as $key => $value ) {
			$methodName = "setAttr" . strtolower($key);
			if (method_exists($view,$methodName)) {
				$view->{$methodName}($value);
			}elseif($key!='TARGET' && $key!='LABEL'){
				trigger_error("Parameter '$key' not alowed in class '$psClass_Name'.",E_USER_WARNING);
			}
		}

		$this->ciXmlDepth++;
		return true;
	}

	/**
	 * Abre uma tag HTML.
	 *
	 * <p>Abre uma tag HTML.</p>
	 * @access private
	 * @param string $psName Nome da tag a ser aberta
	 * @param array $paAttribs Array de atributos da tag
	 * @return boolean Indica se conseguiu abrir a tag ou n�o
	 */
	private function tag_html_start($psName,$paAttribs) {
		if( $this->ciXmlDepth == 0)
		return false;
		$msView = $this->caXmlRoot[$this->ciXmlDepth-1];
		// just add the html tag to the view's value
		// :TRICK: the parent view must be a static type, otherwise it wont work.
		$msOut = "";

		$psName = strtolower($psName);
		if (in_array($psName,$this->caHtmlTags)) {
			$msOut .= "<" . $psName;
			foreach( $paAttribs as $key => $value ) {
				$msOut .= ' ' . $key . '="' . $value . '"';
			}
			if (strcasecmp($psName,"BR")==0)
			$msOut.="/";
			$msOut .= '>';
			$msView->setValue($msOut);
			return true;
		}
		return false;
	}

	/**
	 * Start element Handler.
	 *
	 * <p>Start element Handler.</p>
	 * @access private
	 * @param resource $prParser Handler do analisador XML
	 * @param string $psName Nome do elemento para o qual o handler � chamado
	 * @param array $paAttribs Atributos do elemento
	 */
	private function xml_start_element($prParser,$psName,$paAttribs) {
		if (!$this->generic_start($psName,$paAttribs)) {
			if (!$this->tag_html_start($psName,$paAttribs)) {
				//                    	$this->caXmlRoot[ $this->ciXmlDepth++] = new fwdNone();
			}
		}
	}

	/**
	 * Adiciona objetos no pai do objeto corrente.
	 *
	 * <p>Adiciona o objeto corrente no seu pai. Executa os m�todos de setObj e
	 * addObj do pai do objeto corrente. Executa o m�todo 'execute' caso exista na classe.</p>
	 * @access private
	 * @param object $poView View a ter seus m�todos executados
	 * @param object $poParent Pai da View
	 * @param string $psView_Class Nome da classe que foi adicionada (setObj, addObj)
	 * @param string $psTarget Alvo
	 * @return boolean Indica se conseguiu executar m�todos
	 */
	private function execute_view($poView,$poParent,$psView_Class,$psTarget) {

		$parent_functions = array(
			"addObj" . $psView_Class,
			"setObj" . $psView_Class);
			
		$view_functions = array ("execute");
			
		$msOut = false;
		if ($poParent) {
			foreach($parent_functions as $funct_name) {
				if ( method_exists($poParent,$funct_name) ) {
					$poParent->{$funct_name}($poView,$psTarget);
					$msOut = true;
				}
			}
		}
		return $msOut;
	}

	/**
	 * Fecha uma tag que n�o � HTML.
	 *
	 * <p>Fecha uma tag que n�o � HTML.</p>
	 * @access private
	 * @param string $psName Nome da tag
	 * @return boolean Indica se conseguiu fechar a tag adequadamente
	 */
	private function generic_end($psName) {
		if ($this->ciXmlDepth < 1)
		return false;

		$view = $this->caXmlRoot[$this->ciXmlDepth-1];
		$view_class = get_class($view);

		if (method_exists($view,"CloseXml"))
		$view->CloseXml();
	  
		if ( isset($this->caXmlRoot[$this->ciXmlDepth-2]) )
		$parent_view = $this->caXmlRoot[$this->ciXmlDepth-2];
		else
		$parent_view = 0;

		$target = $this->caXmlTarget[$this->ciXmlDepth-1];
	  
		// scan the inheritance of each object
		$out = true;
//		$msErrorMessage = "Element '$view_class' is not expected in '".get_class($parent_view)."'.";
		if(!$this->execute_view($view,$parent_view,$view_class,$target)){
			if($parent_view){
				$mbFound = false;
				while(($parent_class = get_parent_class($view_class))){
					if($this->execute_view($view,$parent_view,$parent_class,$target)){
						$mbFound = true;
						break;
					}
					$view_class = $parent_class;
				}
				if(!$mbFound){
					trigger_error($msErrorMessage,E_USER_ERROR);
				}
			}else{
				// no parent view...
				$out = false;     // onde � usado ?
			}
		}

		//execute das fun��es foi retirado da fun�ao {$this->execute_view} pois nessa fun��o a fun��o
		//executeView era chamada mais de uma vez por causa da recur��o para chamar os metodos da classe
		//mae - caso esta altera��o venha a ocasionar algum erro - � s� copiar o c�digo abaixo para
		// a fun��o execute_view modificando o nome da vari�vel $view para $poView
		$executeFunction="execute";
		if (!$this->cbIsCompiling)
		if ( method_exists($view,$executeFunction) )
		$view->{$executeFunction}();

		$this->ciXmlDepth--;
		unset($this->caXmlRoot[$this->ciXmlDepth]);
		unset($this->caXmlTarget[$this->ciXmlDepth]);

		return true;
	}

	/**
	 * Fecha uma tag HTML.
	 *
	 * <p>Fecha uma tag HTML.</p>
	 * @access private
	 * @param string $psName Nome da tag a ser fechada
	 * @return boolean Indica se conseguiu fechar a tag ou n�o
	 */
	private function tag_html_end($psName) {
		if( $this->ciXmlDepth == 0)
		return false;
		$msView = $this->caXmlRoot[$this->ciXmlDepth-1];
		$psName = strtolower($psName);
		if (strcasecmp($psName,"BR")==0)
		return true;
		if (in_array($psName,$this->caHtmlTags)) {
			$msView->setValue("</" . $psName . ">");
			return true;
		}
		return false;
	}

	/**
	 * End element Handler.
	 *
	 * <p>End element Handler.</p>
	 * @access private
	 * @param resource $prParser Handler do analisador XML
	 * @param string $psName Nome do elemento para o qual o handler � chamado
	 */
	private function xml_end_element($prParser,$psName) {
		if (!$this->tag_html_end($psName)) {
			$this->generic_end($psName);
		}
	}

	/**
	 * Insere texto do xml que nao esteja dentro de uma tag.
	 *
	 * <p>Insere texto do xml que nao esteja dentro de uma tag.</p>
	 * @access private
	 * @param resource $prParser Handler do analisador XML
	 * @param string $psData Texto a ser inserido
	 */
	private function xml_data($prParser,$psData){
		if($msTmp = trim($psData)){
			// se trim(psData) = nulo, entao matem o psData, senao desconta os espa�os em branco
			$psData = str_replace("\\n","<BR/>",$psData);
		}else{
			//$psData = $msTmp;
		}
		if($this->ciXmlDepth == 0) return false;
		$moView = $this->caXmlRoot[$this->ciXmlDepth-1];
		if(is_object($moView)
		&& method_exists($moView,"setValue")
		&& !empty($psData)
		&& in_array(get_class($moView),$this->caTextContainers)
		) {
			$miSelectedLanguage = FWDLanguage::getSelectedLanguage();
			$maDefaultLanguages = FWDLanguage::getDefaultLanguages();
			if(!empty($miSelectedLanguage) && $moView instanceof FWDString
			&& !in_array($miSelectedLanguage,$maDefaultLanguages)
			) {
				$moView->setValue(FWDWebLib::convertToISO(""),false);
			}
			else {
				$moView->setValue(FWDWebLib::convertToISO($psData),false);
			}
		}
	}

	/**
	 * Transforma arquivos XML em objetos PHP.
	 *
	 * <p>Transforma arquivos XML em objetos PHP.</p>
	 * @access public
	 * @param string $psFilename Nome do arquivo XML a ser transformado
	 * @return boolean Indica se conseguiu transformar o arquivo XML em objetos PHP
	 */
	public function xml_load($psFilename) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD1,__FILE__,__LINE__);
		$this->cleanInstance();

		$fp = fopen($psFilename,"r");
		if (!$fp)
		return false;

		//$xml_parser = xml_parser_create('ISO-8859-1');
		$xml_parser = xml_parser_create('UTF-8');

		xml_set_object ($xml_parser,$this);

		xml_set_element_handler($xml_parser,"xml_start_element","xml_end_element");
		xml_set_character_data_handler($xml_parser,"xml_data");


		$data = fread ($fp,filesize($psFilename));
		fclose ($fp);
		if (!xml_parse($xml_parser,$data,true)) {
			trigger_error(sprintf("xml_load error: %s at line %d",
			xml_error_string(xml_get_error_code($xml_parser)),
			xml_get_current_line_number($xml_parser)),E_USER_ERROR);
		}

		xml_parser_free($xml_parser);

		$xml_count = count($this->caXmlObjects);

		if (!isset($_POST))		// ZendEncoder 1.2 bugfix
		global $_POST;
		// Atribui valor do post nos objetos, e executa os eventos
		if(!empty($_POST)) {
			$moStartEvent = FWDStartEvent::getInstance();
			foreach ($this->caXmlObjects as $moObject) {
				if(method_exists($moObject, "collect")) {
						
					$maViews = $moObject->collect();
						
					foreach ($maViews as $moView) {
						if($moView instanceof FWDPostValue) {
							isset($_POST[$moView->getAttrName()]) ? $moView->setAttrValue( FWDWebLib::convertToISO($_POST[$moView->getAttrName()]) ) : "";
						}
						if (get_class($moView) == "FWDAjaxEvent")
						$moStartEvent->addAjaxEvent($moView);
					}
				}
				else {
					if($moObject instanceof FWDPostValue) {
						isset($_POST[$moObject->getAttrName()]) ? $moObject->setAttrValue( FWDWebLib::convertToISO($_POST[$moObject->getAttrName()]) ) : "";
					}
					if(get_class($moObject) == "FWDAjaxEvent") {
						$moStartEvent->addAjaxEvent($moObject);
					}
				}
			}
		}

		return true;
	}

	/**
	 * Retorna um nome gerado automaticamente.
	 *
	 * <p>Retorna um nome gerado automaticamente.</p>
	 * @access private
	 * @return string Nome �nico gerado automaticamente
	 */
	private function generateName(){
		return 'FWDID'.++self::$ciLastId;
	}

	/**
	 * Retorna o array de Views (todas as views da dialog).
	 *
	 * <p>Retorna o array de Views (todas as views da dialog).</p>
	 * @access public
	 * @return array Array de Views
	 */
	public function getAttrXmlObjects() {
		return $this->caXmlObjects;
	}

	/**
	 * Indica que o XMLLOAD est� sendo usado para compila��o ou n�o.
	 *
	 * <p>M�todo para indicar que o XMLLOAD est� sendo usado para compila��o ou n�o.</p>
	 * @access public
	 * @param boolean Verdadeiro ou falso
	 */
	public function setCompiling($pbCompiling) {
		$this->cbIsCompiling = $pbCompiling;
	}

	/**
	 * Verifica se o XMLLOAD est� sendo usado para compila��o ou n�o.
	 *
	 * <p>M�todo para verificar se o XMLLOAD est� sendo usado para compila��o ou n�o.</p>
	 * @access public
	 * @return array Array de Views
	 */
	public function isCompiling() {
		return $this->cbIsCompiling;
	}
}
?>