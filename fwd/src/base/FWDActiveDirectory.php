<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDActiveDirectory. Classe simples que trata conex�o e busca de dados com o Active Directory.
 *
 * <p>Classe simples que trata conex�o e busca de dados com o Active Directory.</p>
 * @package FWD5
 * @subpackage base
 */
class FWDActiveDirectory {

	protected $connection = null;
	protected $results = null;
	protected $search = null;

	/*
	 * Basicamente tenta conex�o com o servidor AD.
	 */
	public function connect($hostname, $username, $password, $port = 389, $ssl = false){

		FWDWebLib::setErrorHandler(new FWDDoNothingErrorHandler());

		if($ssl){
			$host = "ldaps://{$hostname}:{$port}";
		} else {
			$host = "ldap://{$hostname}:{$port}";
		}

		$this->connection = @ldap_connect($host);

		if(!$this->connection){
			return false;
		}

		if(!@ldap_set_option($this->connection, LDAP_OPT_PROTOCOL_VERSION, 3)){
			return false;
		}

		if(!@ldap_bind($this->connection, $username, $password)){
			return false;	
		}

		return true;
	}

	/*
	 * Realiza busca e disponibiliza o resultado para a classe.
	 * $baseDN (distinguishedName), por exemplo: CN=fulano, OU=support, DC=realiso, DC=com
	 * $filter (string) = Filtro da busca, exemplo: (&(objectClass=inetOrgPerson)(uid=*))
	 * $fields (array) = Campos que ser�o disponibilizados. Por exemplo: "cn", "givenname", "samaccountname", "homedirectory", "telephonenumber", "mail", etc... 
	 */
	public function search($baseDN, $filter, $fields){
		$this->search = @ldap_search($this->connection, $baseDN, $filter, $fields);

		if(!$this->search)
			return false;

        return true;
	}

	/*
	 * Disponibiliza um array com os resultados encontrados pela busca.
	 */
	public function getResults(){
		$this->results = ldap_get_entries($this->connection, $this->search);
		return $this->results;
	}
}
?>