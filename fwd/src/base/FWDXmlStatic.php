<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDXmlStatic. Singleton. Carrega para mem�ria os objetos PHP
 * que est�o armazenados nos arquivos XMC (XML Compilado).
 *
 * <p>Classe singleton que carrega para a mem�ria os objetos PHP que est�o
 * armazenados nos arquivos XMC (XML Compilado). Garante a exist�ncia de
 * uma e apenas uma inst�ncia da classe FWDXmlStatic.</p>
 * @package FWD5
 * @subpackage base
 */
class FWDXmlStatic {

	/**
	 * Inst�ncia �nica da classe FWDXmlStatic
	 * @staticvar FWDXmlStatic
	 * @access private
	 */
	private static $coXmlStatic = NULL;

	/**
	 * Array de Views (todas as views da dialog)
	 * @var array
	 * @access private
	 */
	private $caXmlObjects;

	/**
	 * Nome do arquivo TAR que ser� pelo xml_load
	 * @var string
	 * @access private
	 */
	private $csTarFileName;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDXmlStatic.</p>
	 * @access private
	 */
	private function __construct() {
		$this->caXmlObjects = array();
		$this->csTarFileName = "forms.xmc";
	}

	/**
	 * Retorna a inst�ncia �nica da classe FWDXmlStatic (singleton).
	 *
	 * <p>Retorna a inst�ncia �nica da classe FWDXmlStatic. Se a inst�ncia
	 * ainda n�o existe, cria a �nica inst�ncia. Se a inst�ncia j� existe,
	 * retorna a �nica inst�ncia (singleton).</p>
	 * @access public
	 * @return FWDXmlStatic Inst�ncia �nica da classe FWDXmlStatic
	 * @static
	 */
	public static function getInstance() {
		if (self::$coXmlStatic == NULL) {
			self::$coXmlStatic = new FWDXmlStatic();
		}
		else {}	// inst�ncia �nica j� existe, retorne-a
		return self::$coXmlStatic;
	}

	/**
	 * Exibe o conte�do da inst�ncia �nica FWDXmlStatic.
	 *
	 * <p>Exibe na tela o conte�do de todos os atributos da �nica inst�ncia
	 * da classe FWDXmlStatic.</p>
	 * @access public
	 */
	public function dump_instance() {
		echo "<br/><br/><br/><br/>--------------[ FWDXmlStatic Singleton Dump <strong>BEGIN</strong> ]--------------<br/>";
		echo "<h3>coXmlStatic:</h3>";
		var_dump(self::$coXmlStatic);
		echo "<br/><h3>caXmlRoot:</h3>";
		var_dump($this->caXmlObjects);
		echo "<br/><h3>csTarFileName:</h3>";
		var_dump($this->csTarFileName);
		echo "<h3></h3>--------------[ FWDXmlStatic Singleton Dump <strong>END</strong> ]--------------<br/><br/><br/><br/>";
	}

	/**
	 * Seta o nome do arquivo TAR.
	 *
	 * <p>Seta o nome do arquivo TAR.</p>
	 * @access public
	 * @param string $psTarFileName Nome do arquivo TAR
	 */
	public function setAttrTarFileName($psTarFileName) {
		$this->csTarFileName = $psTarFileName;
	}

	/**
	 * L� os dados de um arquivo dentro de um pacote TAR.
	 *
	 * <p>L� os dados de um arquivo dentro de um pacote TAR.</p>
	 * @access private
	 * @param string $psTarFile Arquivo TAR a ser lido
	 * @param string $psFileItem Arquivo a ser extra�do de dentro do TAR
	 * @return string Conte�do do arquivo lido
	 */
	private function readTarFile($psTarFile,$psFileItem) {
		if ( !($tarFileStream = fopen($psTarFile,"rb")) )
		trigger_error("Error openning file $psTarFile.",E_USER_ERROR);
		else {
			while(!feof($tarFileStream)) {
				$msBlock = fread($tarFileStream,512);
				if (strlen($msBlock)<512)
				trigger_error("File $psFileItem was not found in $psTarFile.",E_USER_ERROR);

				$maData = unpack("a100name/a8mode/a8uid/a8gid/a12size/a12mtime/a8checksum/a1type/a100symlink/a6magic/a2temp/a32temp/a32temp/a8temp/a8temp/a155prefix/a12temp", $msBlock);

				if (!strcmp(trim($maData['name']),$psFileItem)) {
					if ($maData['checksum'] == 0x00000000) break;
					$checksum = 0;
					$msBlock = substr_replace($msBlock, "        ", 148, 8);
					for ($i = 0; $i < 512; $i++)
					$checksum += ord(substr($msBlock, $i, 1));
					if (octdec($maData['checksum']) != $checksum)
					trigger_error("Could not extract $psFileItem from $psTarFile, $psFileItem's header is corrupted.",E_USER_ERROR);
						
					$msBuffer = fread($tarFileStream,octdec($maData['size']));
						
					fclose($tarFileStream);
					if (strlen($msBuffer) == octdec($maData['size']))
					return $msBuffer;
					else
					trigger_error("Could not extract $psFileItem from $psTarFile, it is corrupted.",E_USER_ERROR);
				}
			}
		}
	}

	/**
	 * Transforma os dados em objetos PHP.
	 *
	 * <p>Transforma os dados em objetos PHP. Adiciona-os na WebLib e no
	 * array caXmlObjects.</p>
	 * @access private
	 * @param string $psData Dados a serem carregados
	 */
	private function xmlStatic($psData) {

		if (!$psData) return;

		$moObject = FWDWebLib::unserializeString(gzuncompress(base64_decode($psData)), false);

		if (method_exists($moObject,"getAttrName") && $moObject->getAttrName()) {
			$msKey = $moObject->getAttrName();
		}
		else {
			$msKey = "";
		}

		if ($msKey) {
			FWDWebLib::addObject($msKey,$moObject);
			$this->caXmlObjects[$msKey] = $moObject;
		}
		else {
			$this->caXmlObjects[] = $moObject;
		}

		if (method_exists($moObject, "execute")) {
			$moObject->execute();
		}

		if (!method_exists($moObject, "collect"))
		return true;

		$maViews = $moObject->collect();
		$moStartEvent = FWDStartEvent::getInstance();
		$moWebLib = FWDWebLib::getInstance();

		foreach ($maViews as $moView) {
			if (method_exists($moView, "execute")) {
				$moView->execute();
			}
			if (method_exists($moView,"getAttrName") && $moView->getAttrName()) {
				$exists = FWDWebLib::getObject($moView->getAttrName());
				if (!isset($exists))
				FWDWebLib::addObject($moView->getAttrName(),$moView);
			}

			// Para funcionar a tradu��o.
			$miSelectedLanguage = FWDLanguage::getSelectedLanguage();
			$maDefaultLanguages = FWDLanguage::getDefaultLanguages();
			if(!empty($miSelectedLanguage)
			&& !in_array($miSelectedLanguage,$maDefaultLanguages)
			) {
				if(!($moView instanceof FWDSelect)
				&& !($moView instanceof FWDController)
				&& !($moView instanceof FWDVariable)
				&& method_exists($moView,"setObjFWDString")
				&& method_exists($moView,"setValue"))
				$moView->setValue(FWDWebLib::convertToISO(""),true);

				if($moView instanceof FWDSelect) {
					$moItens = $moView->getItems();
					foreach ($moItens as $moItem)
					$moItem->setValue(FWDWebLib::convertToISO(""),true);
				}

				if($moView instanceof FWDMenu) {
					$moItens = $moView->getAttrItems();
					foreach ($moItens as $moItem)
					if(get_class($moItem) == "FWDMenuItem")
					$moItem->setValue(FWDWebLib::convertToISO(""),true);
				}
			}

			if ($moView instanceof FWDPostValue) {
				isset($_POST[$moView->getAttrName()]) ? $moView->setAttrValue( $_POST[$moView->getAttrName()] ) : "";
			}
			if (get_class($moView) == "FWDAjaxEvent")
			$moStartEvent->addAjaxEvent($moView);
		}
		return true;
	}

	/**
	 * Transforma arquivos XML em objetos PHP.
	 *
	 * <p>Transforma arquivos XML em objetos PHP.</p>
	 * @access public
	 * @param string $psFilename Nome do arquivo XML a ser transformado
	 * @return boolean Indica se conseguiu transformar o arquivo XML em objetos PHP
	 */
	public function xml_load($psFilename) {
		$maParameters = func_get_args();		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD1,__FILE__,__LINE__);
		$this->caXmlObjects = array();

		/*
		 * Esse trecho trata os casos em que o xml do arquivo
		 * n�o est� no forms.xmc do diret�rio corrente.
		 */
		$miPos = strrpos($psFilename, "/");
		if ($miPos===false) {
			$msRef = '';
			$msFile = $psFilename;
		}
		else {
			$msRef = substr($psFilename, 0, $miPos+1);
			$msFile = substr($psFilename, $miPos+1, strlen($psFilename)-$miPos);
		}

		$msInstDir = dirname( $_SERVER[ "SCRIPT_FILENAME" ] ) . '/' . $msRef;
		$msData = $this->readTarFile($msInstDir.$this->csTarFileName,$msFile);
		if (!isset($msData))
		trigger_error("Error loading XML file. Please contact the administrator.", E_USER_ERROR);

		$maData = explode("\n", $msData);

		foreach($maData as $msItem) {
			$this->xmlStatic($msItem);
		}

		if (!isset($_POST))		// ZendEncoder 1.2 bugfix
		global $_POST;
		// Atribui valor do post nos objetos, e executa os eventos
		if (!empty($_POST)) {
			$moStartEvent = FWDStartEvent::getInstance();
			foreach ($this->caXmlObjects as $moObject) {
				if ($moObject instanceof FWDPostValue) {
					isset($_POST[$moObject->getAttrName()]) ? $moObject->setAttrValue( $_POST[$moObject->getAttrName()] ) : "";
				}
				if (get_class($moObject) == "FWDAjaxEvent") {
					$moStartEvent->addAjaxEvent($moObject);
				}
			}
		}
	}
}
?>
