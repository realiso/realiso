<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDMenuACLSecurity. Singleton. Aplica ACL nos FWDMenuItems de um FWDMenu.
 *
 * <p>Classe que aplica ACL nos FWDMenuItems de um FWDMenu. Percorre todos os
 * elementos do menu, desabilitando aqueles para os quais o usu�rio n�o possui
 * permiss�o. Garante a exist�ncia de uma e apenas uma inst�ncia da classe
 * FWDMenuACLSecurity.</p>
 * @package FWD5
 * @subpackage base
 */
class FWDMenuACLSecurity {

	/**
	 * Inst�ncia �nica da classe FWDMenuACLSecurity ou da classe FWDGridMenuACLSecurity
	 * @staticvar FWDMenuACLSecurity ou FWDGridMenuACLSecurity
	 * @access private
	 */
	private static $coMenuACLSecurity = NULL;

	/**
	 * N�mero �nico identificador de menus de contexto com itens desabilitados
	 * @staticvar integer
	 * @access private
	 */
	private static $ciNumMenus=0;

	/**
	 * Array de hash de Menus em que ja foram aplicadas ACLs
	 * @var array
	 * @access private
	 */
	private $caMenuHash = array();

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDMenuACLSecurity.</p>
	 * @access private
	 */
	private function __construct() {
	}

	/**
	 * Retorna a inst�ncia �nica da classe FWDMenuACLSecurity (singleton).
	 *
	 * <p>Retorna a inst�ncia �nica da classe FWDMenuACLSecurity. Se a inst�ncia
	 * ainda n�o existe, cria a �nica inst�ncia. Se a inst�ncia j� existe,
	 * retorna a �nica inst�ncia (singleton).</p>
	 * @access public
	 * @return FWDMenuACLSecurity Inst�ncia �nica da classe FWDMenuACLSecurity
	 * @static
	 */
	public static function getInstance() {
		if (self::$coMenuACLSecurity == NULL) {
			self::$coMenuACLSecurity = new FWDMenuACLSecurity();
		}
		else {}	// inst�ncia �nica j� existe, retorne-a
		return self::$coMenuACLSecurity;
	}

	/**
	 * Desabilita itens do menu, se necess�rio.
	 *
	 * <p>Desabilita itens do menu, se necess�rio. Percorre todos os itens
	 * do menu, desabilitando aqueles cuja tag estiver presente no array de
	 * tags para as quais o usu�rio n�o possui permiss�o.</p>
	 *
	 * @access public
	 * @param FWDMenu $poMenu FWDMenu no qual os itens ser�o desabilitados (se necess�rio)
	 * @param FWDACLSecurity $poACLSecurity ACL que cont�m as informa��es necess�rias para desabilitar os itens
	 * @return FWDMenu Menu com itens possivelmente desabilitados
	 */
	public function installMenuACL(FWDMenu $poMenu, FWDACLSecurity $poACLSecurity) {
		$poACLSecurity->installSecurity($poMenu);
		$msMD5 = "";
		foreach ($poMenu->getAttrItems() as $moItem) {
			$msMD5 .= $moItem->getValue();
			$msMD5 .= $moItem->getShouldDraw() ? "true" : "false";
		}
		$msMD5 = md5($msMD5);
		if (array_key_exists($msMD5,$this->caMenuHash)) {
			unset($poMenu);
		}
		else {
			$poMenu->setAttrName($poMenu->getAttrName() . self::$ciNumMenus++);
			$this->caMenuHash[$msMD5] = $poMenu;
		}
		return $this->caMenuHash[$msMD5];
	}

}
?>
