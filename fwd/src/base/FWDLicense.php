<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDLicense. Classe de licen�a para os sistemas
 *
 * <p>Classe de licen�a para os sistemas.</p>
 * @package FWD5
 * @subpackage base
 */
 
class FWDLicense {

	/**
	 * Chave p�blica
	 * @var string
	 * @access protected
	 */
	protected $csPublicKey = '';

	/**
	 * Chave privada
	 * @var string
	 * @access protected
	 */
	protected $csPrivateKey = '';

	/**
	 * Array de atributos
	 * @var array
	 * @access protected
	 */
	protected $caAttributes = array();

	/**
	 * Array de Ativa��o
	 * @var array
	 * @access protected
	 */
	protected $caActivation = array();

	/**
	 * Sistema Operacional
	 * @var string
	 * @access protected
	 */
	protected $csOS;

	/**
	 * Arquivo de Licen�a
	 * @var string
	 * @access protected
	 */
	protected $csLicenseFile;

	/**
	 * Arquivo de Licen�a Encriptado
	 * @var string
	 * @access protected
	 */
	protected $csEncryptedLicenseFile;

	/**
	 * Indica erro na leitura de dados com gzuncompress
	 * @var string
	 * @access protected
	 */
	protected $cbGZUncompressError=false;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDLicense.</p>
	 * @access public
	 * @param string $psPublicKey Chave p�blica
	 * @param string $psLicenseFile Arquivo de licen�a
	 * @param string $psEncryptedLicenseFile Arquivo que cont�m o hash de ativa��o do sistema
	 */
	public function __construct($psPublicKey,$psLicenseFile="",$psEncryptedLicenseFile=""){
		$this->csPublicKey = $psPublicKey;
		$this->caActivation['WIN']['COMMAND'] = "ipconfig /all";
		$this->caActivation['WIN']['HWSTRING'] = "Physical Address. . . . . . . . . : ";
		$this->caActivation['WIN']['CHAR'] = "\r\n";
		$this->caActivation['LINUX']['COMMAND'] = "ifconfig";
		$this->caActivation['LINUX']['HWSTRING'] = "HWaddr ";
		$this->caActivation['LINUX']['CHAR'] = " ";
		if (substr(PHP_OS, 0, 3) == "WIN") {
			$this->csOS = "WIN";
		} else {
			$this->csOS = "LINUX";
		}
		if (stripos($_SERVER['SERVER_SOFTWARE'],'iis')!==false) { //Server: IIS
			$this->caActivation[$this->csOS]['SERVER_IP'] = "LOCAL_ADDR";
		} else { //Server: Apache, etc.
			$this->caActivation[$this->csOS]['SERVER_IP'] = "SERVER_ADDR";
		}

		$this->csLicenseFile = $psLicenseFile;
		$this->csEncryptedLicenseFile = $psEncryptedLicenseFile;
		FWDLanguage::getPHPStringValue('error_corrupt_license',"License is corrupted.");
		FWDLanguage::getPHPStringValue('error_corrupt_hash',"Hash is corrupted.");
		FWDLanguage::getPHPStringValue('error_permission_denied',"Permission denied in activation file.");
	}

	/**
	 * Calcula a assinatura para os dados especificados
	 *
	 * <p>Calcula a assinatura para os dados especificados usando a chave privada.</p>
	 * @access public
	 * @param string $psPrivateKey Chave privada
	 * @param string $psData Dados a serem assinados
	 */
	public function sign($psPrivateKey, $psData){
		$mrKeyId = openssl_get_privatekey($psPrivateKey);
		$msSignature="";
		openssl_sign($psData,$msSignature,$mrKeyId);
		openssl_free_key($mrKeyId);
		return $msSignature;
	}

	/**
	 * Verifica a assinatura para os dados especificados
	 *
	 * <p>Verifica se a assinatura para os dados especificados est� correta usando
	 * a chave p�blica.</p>
	 * @access public
	 * @param string $psSignature Assinatura
	 * @param string $psData Dados a partir dos quais a assinatura foi gerada
	 * @return boolean True, se a assinatura est� correta
	 */
	public function verify($psSignature, $psData){
		$mrKeyId = openssl_get_publickey($this->csPublicKey);
		$mbOk = openssl_verify($psData,$psSignature,$mrKeyId);
		openssl_free_key($mrKeyId);
		return ($mbOk?true:false);
	}

	/**
	 * Seta um atributo da licen�a
	 *
	 * <p>Seta um atributo da licen�a.</p>
	 * @access public
	 * @param string $psName Nome do atributo
	 * @param string $psValue Valor do atributo
	 */
	public function setAttribute($psName, $psValue){
		$this->caAttributes[strtolower($psName)] = $psValue;
	}

	/**
	 * Retorna um atributo da licen�a
	 *
	 * <p>Retorna um atributo da licen�a.</p>
	 * @access public
	 * @param string $psName Nome do atributo
	 * @return string Valor do atributo
	 */
	public function getAttribute($psName){
		$psName = strtolower($psName);
		if(isset($this->caAttributes[$psName])) return $this->caAttributes[$psName];
		else return false;
	}

	/**
	 * Seta a chave privada
	 *
	 * <p>Seta a chave privada.</p>
	 * @access public
	 * @param string $psPrivateKey Chave privada
	 */
	public function setPrivateKey($psPrivateKey){
		$this->csPrivateKey = $psPrivateKey;
	}

	/**
	 * Retorna uma string com os atributos da licen�a
	 *
	 * <p>Retorna uma string contendo uma lista dos atributos da licen�a, com seus
	 * valores.</p>
	 * @access public
	 * @return string String com os atributos da licen�a
	 */
	public function summarizeLicense(){
		$msRet = "; &lt;?php exit(); ?&gt;\n";
		foreach($this->caAttributes as $msKey => $msValue){
			$msRet.= "; $msKey: $msValue \n";
		}
		return $msRet;
	}

	/**
	 * Gera uma chave de licen�a
	 *
	 * <p>Gera uma chave de licen�a.</p>
	 * @access public
	 * @return string Chave de licen�a
	 */
	public function generateLicenseKey(){
		$msSerializedAttrs = serialize($this->caAttributes);
		$msCodedAtrrs = base64_encode(gzcompress($msSerializedAttrs));
		$msSignature = $this->Sign($this->csPrivateKey,$msSerializedAttrs);
		$msCodedSignature = base64_encode(gzcompress($msSignature));
		$msLicense = "--------------------- BEGIN LICENSE KEY ------------------------\n"
		.chunk_split("$msCodedAtrrs|$msCodedSignature",64)
		."---------------------- END LICENSE KEY -------------------------\n";
		return $msLicense;
	}

	/**
	 * Gera uma licen�a
	 *
	 * <p>Gera uma licen�a (atributos + chave).</p>
	 * @access public
	 * @return string licen�a
	 */
	public function generateLicense(){
		return $this->summarizeLicense().$this->generateLicenseKey();
	}

	/**
	 * Carrega e parseia a licen�a de um arquivo
	 *
	 * <p>Carrega e parseia a licen�a de um arquivo. Retorna um array com dois
	 * elementos, uma string contendo os atributos codificados e uma string
	 * contendo a assinatura codificada. Se n�o conseguir ler o arquivo, retorna
	 * false.</p>
	 * @access protected
	 * @param string $psFileName Nome do arquivo de licen�a
	 * @return array Array contendo os atributos e a assinatura codificados
	 */
	protected function loadLicenseFromFile($psFileName){
		if(!file_exists($psFileName) || !is_readable($psFileName)) return false;
		$msContent = implode('',file($psFileName));
		$maLines = explode("\n",$msContent);
		$miI = 0;
		while($maLines[$miI][0]==';') $miI++;
		$maArr = array();
		$miLines = count($maLines);
		for($miI++;$miI<$miLines;$miI++){
			if(strpos($maLines[$miI],'-')!==false) break;
			$maArr[] = trim($maLines[$miI]);
		}
		$maParts = explode('|',implode('',$maArr));
		return $maParts;
	}

	/**
	 * Carrega de um arquivo e verifica a licen�a
	 *
	 * <p>Carrega de um arquivo e verifica a licen�a. Se a licen�a for inv�lida, gera
	 * um erro.</p>
	 * @access public
	 * @param string $psFileName Nome do arquivo de licen�a
	 * @return boolean Indica se a licen�a � v�lida ou n�o
	 */
	public function loadLicense($psFileName){
		$mbReturn = false;
		FWDWebLib::addKnownError('error_corrupt_license',array('gzuncompress','base64_decode','unserialize','Uninitialized string offset'));
		$maParts = $this->loadLicenseFromFile($psFileName);
		if($maParts!==false){
			$msSerializedAttrs = gzuncompress(base64_decode($maParts[0]));
			$msSignature = gzuncompress(base64_decode($maParts[1]));
			if($this->verify($msSignature,$msSerializedAttrs)){
				$maAttributes = FWDWebLib::unserializeString($msSerializedAttrs);
				foreach($maAttributes as $msName => $msValue){
					$this->setAttribute($msName,$msValue);
				}
				$mbReturn = true;
			}
		}
		FWDWebLib::removeKnownError('error_corrupt_license');
		return $mbReturn;
	}

	/**
	 * Retorna o MAC Address da m�quina
	 *
	 * <p>Retorna o MAC Address da m�quina</p>
	 * @access private
	 * @return string MAC Address
	 */
	private function getMACAddress() {
		$msMACAddress = shell_exec($this->caActivation[$this->csOS]['COMMAND']);
		$miIPPos = strpos($msMACAddress,$_SERVER[$this->caActivation[$this->csOS]['SERVER_IP']]);
		$msMACAddress = substr($msMACAddress,0,$miIPPos);
		$msHWString = $this->caActivation[$this->csOS]['HWSTRING'];
		$miHWStringPos = strrpos($msMACAddress,$msHWString);
		$msMACAddress = substr($msMACAddress,$miHWStringPos + strlen($msHWString));
		$miSpacePos = strpos($msMACAddress,$this->caActivation[$this->csOS]['CHAR']);
		$msMACAddress = substr($msMACAddress,0,$miSpacePos);
		return $msMACAddress;
	}

	/**
	 * Retorna o Hostname da m�quina
	 *
	 * <p>Retorna o Hostname da m�quina</p>
	 * @access private
	 * @return string Hostname
	 */
	private function getHostname() {
		$msHostname = shell_exec('hostname');
		$miSpacePos = strpos($msHostname,"\n");
		$msHostname = substr($msHostname,0,$miSpacePos);
		return $msHostname;
	}

	/**
	 * Retorna o hash md5 do MACAddress|Hostname|Licen�a
	 *
	 * <p>Retorna o hash md5 do MACAddress|Hostname|Licen�a</p>
	 * @access public
	 * @return string Hash md5
	 */
	public function getSystemHash() {
		if (!$this->csLicenseFile || !file_exists($this->csLicenseFile) || !is_readable($this->csLicenseFile)) {
			return false;
		}
		$msLicenseContent = "";
		$maLicenseContent = file($this->csLicenseFile);
		foreach($maLicenseContent as $msLine) {
			if ($msLine[0]==";" || $msLine[0]=="-") {
				continue;
			}
			$msLicenseContent.=$msLine;
		}
		$msMD5SystemHash = md5($this->getMACAddress()."|".$this->getHostname())."|".md5(trim($msLicenseContent));
		return chunk_split(base64_encode($msMD5SystemHash),64);
	}

	/**
	 * Trata erros da fun��o gzuncompress.
	 *
	 * <p>Trata erros da fun��o gzuncompress.</p>
	 * @access private
	 */
	private function activationGZUncompressError() {
		$this->cbGZUncompressError = true;
	}

	/**
	 * Retira a criptografia de um hash.
	 *
	 * <p>Retira a criptografia de um hash obtido do arquivo criptografado
	 * ou da gera��o de um hash de ativa��o.</p>
	 * @access public
	 * @param string psSource String em base64 contendo o hash criptografado.
	 * @return string Hash descriptografado.
	 */
	public function decryptHash($psSource) {
		FWDWebLib::addKnownError('error_corrupt_hash',array('base64_decode','mdecrypt_generic'));
		$msActivationHash = base64_decode($psSource);
		$moCrypt = new FWDCrypt();
		$moCrypt->setIV(substr($msActivationHash,0,strlen($moCrypt->getIV())));
		$msActivationHash = substr($msActivationHash,strlen($moCrypt->getIV()));
		$msActivationHash = $moCrypt->decrypt($msActivationHash);
		FWDWebLib::removeKnownError('error_corrupt_hash');
		return $msActivationHash;
	}

	/**
	 * Gera um hash de ativa��o
	 *
	 * <p>Gera um hash de ativa��o.</p>
	 * @access public
	 * @param string psSystemHash Hash md5 do sistema, obtido atrav�s de getSystemHash() pelo cliente
	 * @return string Hash de ativa��o
	 */
	public function generateActivationHash($psSystemHash){
		$msCodedHash = base64_encode(gzcompress($psSystemHash));
		$msSignature = $this->sign($this->csPrivateKey,$psSystemHash);
		$msCodedSignature = base64_encode(gzcompress($msSignature));
		$msCodedTimeStamp = base64_encode(gzcompress(time()));
		$msActivationHash = "$msCodedHash|$msCodedSignature|$msCodedTimeStamp";
		$moCrypt = new FWDCrypt();
		return chunk_split(base64_encode($moCrypt->getIV().$moCrypt->encrypt($msActivationHash)),64);
	}

	/**
	 * Desativa o sistema, gerando um hash de desativa��o.
	 *
	 * <p>Desativa o sistema, gerando um hash de desativa��o.
	 * Limpa o arquivo de ativa��o criptografado e escreve
	 * o hash de desativa��o nele.</p>
	 * @access public
	 */
	public function generateDeactivationHash(){
		$moActivationHandler = new FWDActivationHandler($this->csEncryptedLicenseFile);
		$miMaxTimeStamp = 0;
		while($msHash = $moActivationHandler->fetch()) {
			$msHash = $this->decryptHash($msHash);
			$maParts = explode('|',$msHash);
			if (array_key_exists(2,$maParts)) {
				$miTimestamp = gzuncompress(base64_decode($maParts[2]));
				$miMaxTimeStamp = $miTimestamp>$miMaxTimeStamp?$miTimestamp:$miMaxTimeStamp;
			}
		}
		$msSystemHash = $this->getSystemHash();
		$msCodedSystemHash = base64_encode(gzcompress($msSystemHash));
		$msCodedToken = base64_encode(gzcompress("DEACTIVATED"));
		$msCodedTimeStamp = base64_encode(gzcompress($miMaxTimeStamp));
		$msDeactivationHash = "$msCodedSystemHash|$msCodedToken|$msCodedTimeStamp";
		$moCrypt = new FWDCrypt();
		$msDeactivationHash = chunk_split(base64_encode($moCrypt->getIV().$moCrypt->encrypt($msDeactivationHash)),64);
		$moActivationHandler->clearFile();
		return $moActivationHandler->insertHash($msDeactivationHash);
	}

	/**
	 * Escreve o hash no arquivo de ativa��o criptografado.
	 *
	 * <p>Escreve o hash no arquivo de ativa��o criptografado.
	 * Se o arquivo possuir um hash de desativa��o, verifica se o timestamp
	 * da ativa��o � maior que o da desativa��o, para prevenir que o usu�rio
	 * insira um ativa��o gerada antes da desativa��o. Ent�o limpa o arquivo
	 * antes de escrever o novo hash.</p>
	 * @access public
	 * @param string psHash Hash que ser� escrito no arquivo.
	 */
	public function writeHashToFile($psHash) {
		$moActivationHandler = new FWDActivationHandler($this->csEncryptedLicenseFile);
		if ($msHash = $moActivationHandler->getHash()) {
			$msHash = $this->decryptHash($msHash);
			$maParts = explode('|',$msHash);
			if(array_key_exists(1,$maParts)) {
				FWDWebLib::addKnownError('error_corrupt_activation_hash',array('gzuncompress','base64_decode','mdecrypt_generic'));
				$msToken = gzuncompress(base64_decode($maParts[1]));
				if ($msToken=='DEACTIVATED') {
					$miDeactivationTimestamp = gzuncompress(base64_decode($maParts[2]));
					$msHash = $this->decryptHash($psHash);
					$maHash = explode('|',$msHash);
					$miTimestamp = gzuncompress(base64_decode($maHash[2]));
					if ($miTimestamp <= $miDeactivationTimestamp) return false;
					$moActivationHandler->clearFile();
				}
				FWDWebLib::removeKnownError('error_corrupt_activation_hash');
			}
		}
		return $moActivationHandler->insertHash($psHash);
	}

	/**
	 * Verifica a validade de um hash.
	 *
	 * <p>Verifica a validade de um hash.</p>
	 * @access public
	 * @param string psHash Hash
	 * @return V�lido(true) / Inv�lido(false)
	 */
	public function checkHash($psHash) {
		$msHash = $this->decryptHash($psHash);
		set_error_handler(array($this,'activationGZUncompressError'));
		$maParts = explode('|',$msHash);
		$msSystemHash = gzuncompress(base64_decode($maParts[0]));
		$msSignature = gzuncompress(base64_decode($maParts[1]));
		restore_error_handler();
		if ($this->cbGZUncompressError) {
			return false;
		}
		if ($msSignature=='DEACTIVATED') {
			return false;
		}
		if ($this->verify($msSignature,$msSystemHash)) {
			$msMD5Hash = base64_decode($msSystemHash);
			$maMD5Parts = explode('|',$msMD5Hash);
			$msCurrentSystemHash = base64_decode($this->getSystemHash());
			$maCurrentMD5Parts = explode('|',$msCurrentSystemHash);
			if ($maCurrentMD5Parts[0] != $maMD5Parts[0]) return false;
			if ($maCurrentMD5Parts[1] != $maMD5Parts[1]) return false;
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Verifica se o sistema est� ativado.
	 *
	 * <p>Verifica se o sistem est� ativado, passando por cada registro de ativa��o no
	 * arquivo de ativa��es criptografado.</p>
	 * @access public
	 * @return Ativado(true) / Desativado(false)
	 */
	public function isActive() {
		$moActivationHandler = new FWDActivationHandler($this->csEncryptedLicenseFile);
		while($msHash = $moActivationHandler->fetch()) {
			if ($this->checkHash($msHash))
			return true;
		}
		return false;
	}

	/**
	 * Verifica se o sistema est� dentro do per�odo de ativa��o, e retorna um
	 * array com os dados do per�odo de ativa��o
	 *
	 * <p>Verifica se o sistema est� dentro do per�odo de ativa��o, e retorna um
	 * array com os dados do per�odo de ativa��o.</p>
	 * @access public
	 * @return array(isActivationPeriod, newActivationPeriodCounter,
	 * newActivationPeriodTime, newActivationPeriodHash)
	 */
	public function getActivationPeriodData($piActivationPeriodCounter,$psActivationPeriodTime,$psActivationPeriodHash) {
		$mbReturn = false;
		$miReturnCounter = 0;
		$msReturnTime = 0;
		$msReturnHash = "";
		if ($piActivationPeriodCounter<intval($this->getAttribute("activation_period"))) { //Verifica se o contador eh menor que o periodo para ativacao
			if (md5("SYSTEM_ACTIVATION_PERIOD_TIME:$psActivationPeriodTime;SYSTEM_ACTIVATION_PERIOD_COUNTER:$piActivationPeriodCounter")==$psActivationPeriodHash) { //Verifica se o hash confirma os dados passados
				$mbReturn = true;
				if (date('Y-m-d',strtotime($psActivationPeriodTime)) != date('Y-m-d')) { //se o time passado for diferente da data atual
					$msReturnTime = date('Y-m-d H:i:s');
					$miReturnCounter = $piActivationPeriodCounter + floor(abs((strtotime($psActivationPeriodTime)-strtotime($msReturnTime))/86400)); //Contador = Contador + diferenca entre a data de hoje e ultima utilizacao
					$msReturnHash = md5("SYSTEM_ACTIVATION_PERIOD_TIME:$msReturnTime;SYSTEM_ACTIVATION_PERIOD_COUNTER:$miReturnCounter");
				}
			}
		}
		return array($mbReturn,$miReturnCounter,$msReturnTime,$msReturnHash);
	}

	/**
	 * Retorna o hash de desativa��o, caso o sistema esteja desativado.
	 *
	 * <p>Retorna o hash de desativa��o, caso o sistema esteja desativado.
	 * Se o sistema n�o estiver desativado, retorna string vazia.</p>
	 * @return string Hash de desativa��o
	 */
	public function getDeactivationHash() {
		$moActivationHandler = new FWDActivationHandler($this->csEncryptedLicenseFile);
		if ($msHash = $moActivationHandler->getHash()) {
			$msDeactivationHash = $this->decryptHash($msHash);
			$maParts = explode("|",$msDeactivationHash);
			set_error_handler(array($this,'activationGZUncompressError'));
			$msToken = gzuncompress(base64_decode($maParts[1]));
			restore_error_handler();
			if ($msToken == "DEACTIVATED") {
				return $msHash;
			}
		}
		return '';
	}
}
?>