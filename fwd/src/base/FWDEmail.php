<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDEmail. Classe para enviar emails.
 *
 * <p>Classe para enviar emails de texto puro ou HTML. Suporta anexos e imagens
 * embutidas no HTML.</p>
 * @package FWD5
 * @subpackage base
 */

include 'xpm/SMTP.php';

class FWDEmail {

	/**
	 * Remetente do email
	 * @var string
	 * @access protected
	 */
	protected $csFrom = '';

	/**
	 * Nome do remetente do email
	 * @var string
	 * @access protected
	 */
	protected $csSenderName = '';

	/**
	 * Destinat�rio do email
	 * @var string
	 * @access protected
	 */
	protected $csTo = '';

	/**
	 * Destinat�rios que receber�o c�pia
	 * @var string
	 * @access protected
	 */
	protected $csCc = '';

	/**
	 * Destinat�rios que receber�o c�pia oculta
	 * @var string
	 * @access protected
	 */
	protected $csBcc = '';

	/**
	 * Endere�o para onde devem ser enviadas respostas para o email
	 * @var string
	 * @access protected
	 */
	protected $csReplyTo = '';

	/**
	 * Assunto do email
	 * @var string
	 * @access protected
	 */
	protected $csSubject = '';

	/**
	 * Conte�do da mensagem. Pode ser HTML ou texto puro.
	 * @var string
	 * @access protected
	 */
	protected $csMessage = '';

	/**
	 * Lista de anexos j� no formato em que ser�o enviados
	 * @var array
	 * @access protected
	 */
	protected $caAttachments = array();

	/**
	 * Lista de anexos embutidos j� no formato em que ser�o enviados
	 * @var array
	 * @access protected
	 */
	protected $caEmbeddedAttachments = array();

	/**
	 * Indica se o corpo da mensagem est� em HTML
	 * @var boolean
	 * @access protected
	 */
	protected $cbHtml = true;

	/**
	 * Indica que o sistema usa email em servidor externo
	 * @var boolean
	 */
	protected $cbExternal = false;

	/**
	 * Indica o host de email, caso seja externo
	 * @var String
	 */
	protected $csHost = null;

	/**
	 * Indica a porta de smtp do servidor de email externo
	 * @var integer
	 */
	protected $ciPort = null;

	/**
	 * Nome de usu�rio do servidor externo
	 * @var String
	 */
	protected $csUser = null;

	/**
	 * senha do usu�rio do servidor externo
	 * @var String
	 */
	protected $csPassword = null;

	/**
	 * tipo de encripta��o usada para o email
	 * pode ser tls, ssl, sslv2 or sslv3
	 * @var String
	 */
	protected $csEncryption = null;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDEmail.</p>
	 * @access public
	 */
	public function __construct($external = false, $host = null, $port = null, $user = null, $password = null, $enc = null){
		if ($external){
			if ($host && $port && $user && $password){
				$this->cbExternal = $external;
				$this->csHost = $host;
				$this->ciPort = $port;
				$this->csUser = $user;
				$this->csPassword = $password;
				$this->csEncryption = $enc;
			}
			else {
				trigger_error("Mail options not configured", E_USER_ERROR);
			}
		}
	}

	/**
	 * Seta o remetente do email.
	 *
	 * <p>Seta o remetente do email.</p>
	 * @access public
	 * @param string $psFrom Remetente do email
	 */
	public function setFrom($psFrom){
		$this->csFrom = $psFrom;
	}

	/**
	 * Seta o nome do remetente do email.
	 *
	 * <p>Seta o remetente do remetente do email.</p>
	 * @access public
	 * @param string $psSenderName Nome do remetente do email
	 */
	public function setSenderName($psSenderName){
		$this->csSenderName = $psSenderName;
	}

	/**
	 * Seta o destinat�rio do email.
	 *
	 * <p>Seta o destinat�rio do email.</p>
	 * @access public
	 * @param string $psTo Destinat�rio do email.
	 */
	public function setTo($psTo){
		$this->csTo = $psTo;
	}

	/**
	 * Seta os destinat�rios que receber�o c�pia.
	 *
	 * <p>Seta os destinat�rios que receber�o c�pia.</p>
	 * @access public
	 * @param string $psCc Destinat�rios que receber�o c�pia
	 */
	public function setCc($psCc){
		$this->csCc = $psCc;
	}

	/**
	 * Seta os destinat�rios que receber�o c�pia oculta.
	 *
	 * <p>Seta os destinat�rios que receber�o c�pia oculta.</p>
	 * @access public
	 * @param string $psBcc Destinat�rios que receber�o c�pia oculta
	 */
	public function setBcc($psBcc){
		$this->csBcc = $psBcc;
	}

	/**
	 * Seta o endere�o para onde devem ser enviadas respostas para o email.
	 *
	 * <p>Seta o endere�o para onde devem ser enviadas respostas para o email.</p>
	 * @access public
	 * @param string $psReplyTo Endere�o para onde devem ser enviadas respostas para o email.
	 */
	public function setReplyTo($psReplyTo){
		$this->csReplyTo = $psReplyTo;
	}

	/**
	 * Seta o assunto do email.
	 *
	 * <p>Seta o assunto do email.</p>
	 * @access public
	 * @param string $psSubject Assunto do email
	 */
	public function setSubject($psSubject){
		$this->csSubject = $psSubject;
	}

	/**
	 * Seta o formato do email.
	 *
	 * <p>Seta o formato do email.</p>
	 * @access public
	 * @param boolean $pbHtml true = html, false = text
	 */
	public function setHtml($pbHtml){
		$this->cbHtml = $pbHtml;
	}

	/**
	 * Verifica se o formato do email � html.
	 *
	 * <p>Verifica se o formato do email � html.</p>
	 * @access public
	 * @return boolean true = html, false = text
	 */
	public function isHtml(){
		return $this->cbHtml;
	}

	/**
	 * Seta o conte�do do corpo do email.
	 *
	 * <p>Seta o conte�do do corpo do email. Se o email for HTML e contiver
	 * imagens, elas s�o anexadas.</p>
	 * @access public
	 * @param string $psMessage Conte�do do corpo do email
	 * @param boolean $psHtml Indica se o conte�do deve ser interpretado como HTML
	 */
	public function setMessage($psMessage){
		$this->caEmbeddedAttachments = array();
		if($this->cbHtml){
			$this->csMessage = '';
			$msGfxRef = FWDWebLib::getInstance()->getGfxRef();
			$msSearch = "/[ ]+(src|background)[ ]*=[ ]*(('([^']*)')|(\"([^\"]*)\"))/";
			preg_match_all($msSearch,$psMessage,$maMatches,PREG_OFFSET_CAPTURE|PREG_SET_ORDER);
			$i = 0;
			foreach($maMatches as $maMatch){
				if($maMatch[4][1]==-1){
					$msSrc = $maMatch[6][0];
				}else{
					$msSrc = $maMatch[4][0];
				}
				$miStart = $maMatch[0][1];
				$msFileName = substr($msSrc,strrpos($msSrc,'/')+1);
				$msExt = strtolower(substr($msFileName,strrpos($msFileName,'.')+1));
				$msId = "$msFileName@0123.456";
				$msContentType = ($msExt=='jpg'?'image/jpeg':"image/$msExt");
				$this->addAttachment($msSrc,$msContentType,$msId,true);
				$this->csMessage.= substr($psMessage,$i,$miStart-$i)." ".$maMatch[1][0]."='cid:".$msId."'";
				$i = $miStart+strlen($maMatch[0][0]);
			}
			$this->csMessage.= substr($psMessage,$i);
		}else{
			$this->csMessage = $psMessage;
		}
	}

	/**
	 * Seta o conte�do do corpo do email a partir de uma View.
	 *
	 * <p>Seta o conte�do do corpo do email a partir de uma View. Se a view ou seus
	 * componentes possu�rem imagens, elas s�o anexadas.</p>
	 * @access public
	 * @param FWDView $poView View que representa o conte�do do corpo do email
	 */
	public function setMessageView(FWDView $poView){
		$msMessage = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">"
		."<html><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8'><title></title></head>"
		."<body topmargin=0 leftmargin=0>{$poView->draw()}</body></html>";
		$this->setMessage($msMessage);
	}

	/**
	 * Anexa um arquivo ao email.
	 *
	 * <p>Anexa um arquivo ao email.</p>
	 * @access public
	 * @param string $psFilePath Caminho do arquivo
	 * @param string $psContentType Tipo do conte�do do arquivo (Ex: 'image/jpeg')
	 * @param string $psContentId Identificador do conte�do (s� � necess�rio para embutidos)
	 * @param boolean $pbEmbedded Indica se o anexo � embutido (falso por default)
	 * @return boolean True indica sucesso ao anexar o arquivo
	 */
	public function addAttachment($psFilePath,$psContentType,$psContentId='',$pbEmbedded=false){
		if(!file_exists($psFilePath) || !is_readable($psFilePath) || !is_file($psFilePath)){
			trigger_error("Could not read file '$psFilePath'.",E_USER_WARNING);
			return false;
		}elseif($pbEmbedded && isset($this->caEmbeddedAttachments[$psContentId])){ // j� foi adicionado
			return true;
		}else{
			$psFileName = substr(strrchr($psFilePath,'/'),1);
			$msContent = $this->encode(file_get_contents($psFilePath));
			$msAttachmentCode = "Content-Type: $psContentType; name=\"$psFileName\"\n"
			."Content-Transfer-Encoding: base64\n"
			.($psContentId!=''?"Content-ID: <$psContentId>\n":'')
			."\n$msContent";
			if($pbEmbedded){
				$this->caEmbeddedAttachments[$psContentId] = $msAttachmentCode;
				unset($this->caAttachments[$psContentId]);
			}else{
				$this->caAttachments[$psContentId] = $msAttachmentCode;
				unset($this->caEmbeddedAttachments[$psContentId]);
			}
			return true;
		}
	}

	/**
	 * Retorna o cabe�alho do email.
	 *
	 * <p>Retorna o cabe�alho do email.</p>
	 * @access public
	 * @param string $psBoundary Separador do 1� n�vel do email
	 * @return string Header do email
	 */
	protected function drawHeader($psBoundary, $pbIncludeSubject = false){
		$msSenderName = $this->csSenderName ? $this->csSenderName : 'noreply';
		$msHeader = "MIME-Version: 1.0\n"
		."Content-Type: multipart/mixed; boundary=\"$psBoundary\"\n"
		."X-Mailer: PHP/".phpversion()."\n"
		."To: {$this->csTo} <{$this->csTo}>\n"
		."From: {$msSenderName} <{$this->csFrom}>\n";
		if($this->csReplyTo!='') $msHeader.= "ReplyTo: {$this->csReplyTo}\n";
		if($this->csCc!='') $msHeader.= "Cc: {$this->csCc}\n";
		if($this->csBcc!='') $msHeader.= "Bcc: {$this->csBcc}\n";
		if($pbIncludeSubject) $msHeader.= "Subject: {$this->csSubject}\n";
		$msHeader .= "\n";
		return $msHeader;
	}

	/**
	 * Retorna o trecho do email que cont�m os anexos.
	 *
	 * <p>Retorna o trecho do email que cont�m os anexos.</p>
	 * @access public
	 * @param string $psBoundary Separador a ser usado entre os anexos
	 * @param boolean $pbEmbedded Indica se � para retornar o c�digo dos anexos embutidos ou dos anexos normais
	 * @return string Trecho do email que cont�m os anexos
	 */
	protected function drawAttachments($psBoundary,$pbEmbedded){
		$msOutput = '';
		if($pbEmbedded){
			$maAttachments = &$this->caEmbeddedAttachments;
		}else{
			$maAttachments = &$this->caAttachments;
		}
		foreach($maAttachments as $msAttachmentCode){
			$msOutput.= "--$psBoundary\n$msAttachmentCode\n\n";
		}
		return $msOutput;
	}

	/**
	 * Codifica uma string em base64 e quebra em linhas de tamanho fixo.
	 *
	 * <p>Codifica uma string em base64 e quebra em linhas de tamanho fixo.</p>
	 * @access public
	 * @param string $psString String a ser codificada
	 * @param integer $piLineLength Tamanho em que as linhas devem ser quebradas
	 * @return string String codificada
	 */
	protected function encode($psString,$piLineLength=76){
		return chunk_split(base64_encode($psString),$piLineLength,"\n");
	}

	/**
	 * Envia o email.
	 *
	 * <p>Envia o email.</p>
	 * @access public
	 * @return boolean True or False.
	 */
	public function send(){
		$msBoundary = md5(uniqid(time()));
		$msLv1Boundary = "_1_$msBoundary";
		$msLv2Boundary = "_2_$msBoundary";
		$msLv3Boundary = "_3_$msBoundary";
		$msHeader = $this->drawHeader($msLv1Boundary);
		if($this->cbHtml){
			$msHTMLMessage = $this->csMessage;
			$msTextMessage = strip_tags($this->csMessage);
			$msMessage = "This is a multipart message in MIME format.\n\n"
			."--$msLv1Boundary\n"
			."Content-Type: multipart/related; boundary=\"$msLv2Boundary\"\n\n"
			."--$msLv2Boundary\n"
			."Content-Type: multipart/alternative; boundary=\"$msLv3Boundary\"\n\n"
			."--$msLv3Boundary\n"
			."Content-Type: text/plain; charset=\"utf-8\"\n"
			."Content-Transfer-Encoding: base64\n"
			."\n".$this->encode($msTextMessage)."\n\n"
			."--$msLv3Boundary\n"
			."Content-Type: text/html; charset=\"utf-8\"\n"
			."Content-Transfer-Encoding: base64\n"
			."\n".$this->encode($msHTMLMessage)."\n\n"
			."--$msLv3Boundary--\n\n"
			.$this->drawAttachments($msLv2Boundary,true)
			."--$msLv2Boundary--\n\n"
			.$this->drawAttachments($msLv1Boundary,false)
			."--$msLv1Boundary--\n\n";
		}else{
			$msTextMessage = $this->csMessage;
			$msMessage = "This is a multipart message in MIME format.\n\n"
			."--$msLv1Boundary\n"
			."Content-Type: text/plain; charset=\"utf-8\"\n"
			."Content-Transfer-Encoding: base64\n"
			."\n".$this->encode($msTextMessage)."\n\n"
			."--$msLv1Boundary--\n\n";
		}

		if(strtoupper(substr(php_uname('s'),0,3))==='WIN'){
			if ($this->cbExternal){
				return $this->sendSmtp($this->drawHeader($msLv1Boundary,true) . $msMessage);
			} else {
				ini_set('sendmail_from',$this->csFrom);
				return mail($this->csTo,$this->csSubject,$msMessage,$msHeader);
			}
		}else{
			if ($this->cbExternal){
				return $this->sendSmtp($this->drawHeader($msLv1Boundary,true) . $msMessage);
			} else {
				return mail($this->csTo,$this->csSubject,$msMessage,$msHeader," -f {$this->csFrom}");
			}
		}
	}

	function sendSmtp($msMessage){
		
		$c = SMTP::connect($this->csHost, (int) $this->ciPort, $this->csUser, $this->csPassword, $this->csEncryption, 10);
		if (!$c)  { 
			trigger_error("Error connecting to mail server", E_USER_ERROR);
			return(false);
		}
		$s = SMTP::Send($c, array($this->csTo), $msMessage, $this->csFrom);
		if (!$s) {
            //$error = implode(' ',$_RESULT);
			//trigger_error('Error sending email', E_USER_ERROR);

            if(strtoupper(substr(php_uname('s'),0,3))!=='WIN'){
    			file_put_contents("/tmp/mail_errors", "Error sending email to: " . $this->csTo . "\n", FILE_APPEND);
            }
			return(false);
		}
		SMTP::Disconnect($c);
		return $s;
	}

}

?>