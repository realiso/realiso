<?php

ob_start("ob_gzhandler");

if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] && $_SERVER['HTTPS']!='off'){
	$ssProtocol = 'https';
}else{
	$ssProtocol = 'http';
}

$ssPath = "{$ssProtocol}://{$_SERVER['SERVER_NAME']}{$_SERVER['PHP_SELF']}";
$ssBaseRef = substr($ssPath,0,strrpos($ssPath,'/')+1);

if(isset($lbRelease) && $lbRelease){
	$lsRelease = 'true';
}else{
	$lsRelease = 'false';
}

// Vari�vel JS que indica o caminho para a pasta 'base' da FWD.
echo "var lsBaseRef = '$ssBaseRef';\n";

/**
 * Indica se est� no modo release
 * @var boolean
 */
echo "var lbRelease = $lsRelease;\n"

?>