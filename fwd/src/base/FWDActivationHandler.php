<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDActivationHandler. Classe para controle do arquivo de ativa��es.
 *
 * <p>Classe para controle do arquivo de ativa��es.</p>
 * @package FWD5
 * @subpackage base
 */

class FWDActivationHandler {

	/**
	 * Posi��o do primeiro byte onde ser� lido/gravado o hash no arquivo criptografado.
	 * Se este valor for modificado, os sistemas de clientes que forem atualizados
	 * precisar�o de nova ativa��o.
	 * O arquivo criptografado deve conter, seguindo essa posi��o, dois bytes cujo valor
	 * seja maior que o tamanho at� o final do arquivo. Isto �, se o arquivo possuir 1024 bytes,
	 * os dois bytes devem possuir valor maior que 1006 (0x03EE).
	 * @var constant integer
	 */
	const BYTE_TO_WRITE = 18;

	/**
	 * Ponteiro para arquivo criptografado, que cont�m ativa��es/desativa��o.
	 * @var object
	 * @access private
	 */
	private $coFile;

	/**
	 * Apontador para a posi��o atual de leitura do arquivo.
	 * @var integer
	 * @access private
	 */
	private $ciCurrPos;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDActivationHandler.</p>
	 * @access public
	 * @param string $psFile Arquivo criptogrado, ao qual ser�o gravadas as ativa��es e a desativa��o.
	 */
	public function __construct($psLicense) {
		if(!file_exists($psLicense) || !is_writable($psLicense)) return false;
		FWDWebLib::addKnownError('error_permission_denied',array('fopen'));
		$this->coFile = fopen($psLicense,'rb+'); //Carrega o arquivo e salva o ponteiro.
		FWDWebLib::removeKnownError('error_permission_denied');
		$this->ciCurrPos = self::BYTE_TO_WRITE; //Define o primeiro byte de escrita como posi��o atual.
	}

	/**
	 * Destrutor da classe.
	 *
	 * <p>Destrutor da classe FWDActivationHandler.
	 * "Fecha" o ponteiro para o arquivo.</p>
	 * @access public
	 */
	public function __destruct() {
		if ($this->coFile) fclose($this->coFile);
	}

	/**
	 * Pega o hash da posi��o atual e seta o apontador para a pr�xima posi��o de hash.
	 *
	 * <p>Pega o hash da posi��o atual e seta o apontador para a pr�xima posi��o de hash.</p>
	 * @access public
	 * @return string Hash da posi��o atual
	 */
	public function fetch() {
		$msHash = $this->getHash();
		$this->ciCurrPos += $this->getHashSize() + 2; // + 2 bytes indicadores de tamanho.
		return $msHash;
	}

	/**
	 * Pega o hash da posi��o atual.
	 *
	 * <p>Pega o hash da posi��o atual.</p>
	 * @access public
	 * @return string Hash da posi��o atual.
	 */
	public function getHash() {
		FWDWebLib::addKnownError('error_corrupt_activation_hash',array('gzuncompress'));
		$miHashSize = $this->getHashSize();
		if(!$miHashSize){
			FWDWebLib::removeKnownError('error_corrupt_activation_hash');
			return "";
		}
		fseek($this->coFile,$this->ciCurrPos+2);
		$msHash = fread($this->coFile,$miHashSize);
		$msHash = gzuncompress($msHash);
		FWDWebLib::removeKnownError('error_corrupt_activation_hash');
		return $msHash;
	}

	/**
	 * Pega o tamanho do hash da posi��o atual.
	 *
	 * <p>Pega o tamanho do hash da posi��o atual. Se o tamanho do hash
	 * for maior que o tamanho desta posi��o at� o final do arquivo,
	 * retorna 0, pois n�o h� hash nessa posi��o.</p>
	 * @access public
	 * @return integer Tamanho do hash da posi��o atual.
	 */
	public function getHashSize() {
		$maFileStat = fstat($this->coFile);
		fseek($this->coFile,$this->ciCurrPos);
		$miFirstByte = ord(fread($this->coFile,1)) <<8;
		$miSecondByte = ord(fread($this->coFile,1));
		$miLen = $miFirstByte+$miSecondByte;
		if ($miLen < $maFileStat['size'] - $this->ciCurrPos)
		return $miLen;
		else
		return 0;
	}

	/**
	 * Volta o apontador de hash atual para a primeira posi��o de hash.
	 *
	 * <p>Volta o apontador de hash atual para a primeira posi��o de hash.</p>
	 * @access public
	 */
	public function resetCurrPos() {
		$this->ciCurrPos = self::BYTE_TO_WRITE;
	}

	/**
	 * Insere um hash no arquivo criptografado.
	 *
	 * <p>Insere um hash no arquivo criptografado. Os hash s�o inseridos
	 * no in�cio do arquivo, na posi��o definida por BYTE_TO_WRITE.</p>
	 * @access public
	 * @param string psHash Hash que ser� inserido
	 * @return boolean True em caso de sucesso.
	 */
	public function insertHash($psHash) {
		$this->resetCurrPos();
		$psHash = gzcompress($psHash);
		$miHashLen = strlen($psHash);
		$miMSB = (0xFF00 & $miHashLen)>>8;
		$miLSB = 0x00FF & $miHashLen;
		$msBinaryLen = chr($miMSB).chr($miLSB);
		fseek($this->coFile,$this->ciCurrPos);
		$msPostBuffer = "";
		while(!feof($this->coFile)) {
			$msPostBuffer .= fread($this->coFile,8192);
		}
		fseek($this->coFile,$this->ciCurrPos);
		fwrite($this->coFile,$msBinaryLen);
		fwrite($this->coFile,$psHash);
		fwrite($this->coFile,$msPostBuffer);
		return true;
	}

	/**
	 * Limpa todos os registros de ativa��es/desativa��o do arquivo criptografado.
	 *
	 * <p>Limpa todos os registros de ativa��es/desativa��o do arquivo criptografado.
	 * O arquivo criptografado fica como se nunca houvesse sido feita alguma ativa��o.</p>
	 * @access public
	 */
	public function clearFile() {
		$miTotal=0;
		$this->resetCurrPos();
		do {
			if ($miLen = $this->getHashSize()) {
				$this->ciCurrPos += $miLen + 2;
			}
		} while($miLen);
		rewind($this->coFile);
		$msPrevBuffer = fread($this->coFile,self::BYTE_TO_WRITE);
		fseek($this->coFile,$this->ciCurrPos);
		$msPostBuffer = "";
		while(!feof($this->coFile)) {
			$msPostBuffer .= fread($this->coFile,8192);
		}
		ftruncate($this->coFile,0);
		rewind($this->coFile);
		fwrite($this->coFile,$msPrevBuffer);
		fwrite($this->coFile,$msPostBuffer);
	}
}
?>
