<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe para convers�o de c�digos de cores
 *
 * <p>Classe para convers�o de c�digos de cores. Converte de nomes pra hexa ou
 * RGB e entre RGB e hexa.</p>
 * @package FWD5
 * @subpackage report
 */
class FWDColor {

	/**
	 * Tabela de cores pr�-definidas
	 * @var array
	 * @access protected
	 */
	protected static $caTable = array(
    'aliceblue' => 'f0f8ff',
    'antiquewhite' => 'faebd7',
    'aqua' => '00ffff',
    'aquamarine' => '7fffd4',
    'azure' => 'f0ffff',
    'beige' => 'f5f5dc',
    'bisque' => 'ffe4c4',
    'black' => '000000',
    'blanchedalmond' => 'ffebcd',
    'blue' => '0000ff',
    'blueviolet' => '8a2be2',
    'brown' => 'a52a2a',
    'burlywood' => 'deb887',
    'cadetblue' => '5f9ea0',
    'chartreuse' => '7fff00',
    'chocolate' => 'd2691e',
    'coral' => 'ff7f50',
    'cornflowerblue' => '6495ed',
    'cornsilk' => 'fff8dc',
    'crimson' => 'dc143c',
    'cyan' => '00ffff',
    'darkblue' => '00008b',
    'darkcyan' => '008b8b',
    'darkgoldenrod' => 'b8860b',
    'darkgray' => 'a9a9a9',
    'darkgreen' => '006400',
    'darkkhaki' => 'bdb76b',
    'darkmagenta' => '8b008b',
    'darkolivegreen' => '556b2f',
    'darkorange' => 'ff8c00',
    'darkorchid' => '9932cc',
    'darkred' => '8b0000',
    'darksalmon' => 'e9967a',
    'darkseagreen' => '8fbc8f',
    'darkslateblue' => '483d8b',
    'darkslategray' => '2f4f4f',
    'darkturquoise' => '00ced1',
    'darkviolet' => '9400d3',
    'deeppink' => 'ff1493',
    'deepskyblue' => '00bfff',
    'dimgray' => '696969',
    'dodgerblue' => '1e90ff',
    'feldspar' => 'd19275',
    'firebrick' => 'b22222',
    'floralwhite' => 'fffaf0',
    'forestgreen' => '228b22',
    'fuchsia' => 'ff00ff',
    'gainsboro' => 'dcdcdc',
    'ghostwhite' => 'f8f8ff',
    'gold' => 'ffd700',
    'goldenrod' => 'daa520',
    'gray' => '808080',
    'green' => '008000',
    'greenyellow' => 'adff2f',
    'honeydew' => 'f0fff0',
    'hotpink' => 'ff69b4',
    'indianred' => 'cd5c5c',
    'indigo' => '4b0082',
    'ivory' => 'fffff0',
    'khaki' => 'f0e68c',
    'lavender' => 'e6e6fa',
    'lavenderblush' => 'fff0f5',
    'lawngreen' => '7cfc00',
    'lemonchiffon' => 'fffacd',
    'lightblue' => 'add8e6',
    'lightcoral' => 'f08080',
    'lightcyan' => 'e0ffff',
    'lightgoldenrodyellow' => 'fafad2',
    'lightgrey' => 'd3d3d3',
    'lightgreen' => '90ee90',
    'lightpink' => 'ffb6c1',
    'lightsalmon' => 'ffa07a',
    'lightseagreen' => '20b2aa',
    'lightskyblue' => '87cefa',
    'lightslateblue' => '8470ff',
    'lightslategray' => '778899',
    'lightsteelblue' => 'b0c4de',
    'lightyellow' => 'ffffe0',
    'lime' => '00ff00',
    'limegreen' => '32cd32',
    'linen' => 'faf0e6',
    'magenta' => 'ff00ff',
    'maroon' => '800000',
    'mediumaquamarine' => '66cdaa',
    'mediumblue' => '0000cd',
    'mediumorchid' => 'ba55d3',
    'mediumpurple' => '9370d8',
    'mediumseagreen' => '3cb371',
    'mediumslateblue' => '7b68ee',
    'mediumspringgreen' => '00fa9a',
    'mediumturquoise' => '48d1cc',
    'mediumvioletred' => 'c71585',
    'midnightblue' => '191970',
    'mintcream' => 'f5fffa',
    'mistyrose' => 'ffe4e1',
    'moccasin' => 'ffe4b5',
    'navajowhite' => 'ffdead',
    'navy' => '000080',
    'oldlace' => 'fdf5e6',
    'olive' => '808000',
    'olivedrab' => '6b8e23',
    'orange' => 'ffa500',
    'orangered' => 'ff4500',
    'orchid' => 'da70d6',
    'palegoldenrod' => 'eee8aa',
    'palegreen' => '98fb98',
    'paleturquoise' => 'afeeee',
    'palevioletred' => 'd87093',
    'papayawhip' => 'ffefd5',
    'peachpuff' => 'ffdab9',
    'peru' => 'cd853f',
    'pink' => 'ffc0cb',
    'plum' => 'dda0dd',
    'powderblue' => 'b0e0e6',
    'purple' => '800080',
    'red' => 'ff0000',
    'rosybrown' => 'bc8f8f',
    'royalblue' => '4169e1',
    'saddlebrown' => '8b4513',
    'salmon' => 'fa8072',
    'sandybrown' => 'f4a460',
    'seagreen' => '2e8b57',
    'seashell' => 'fff5ee',
    'sienna' => 'a0522d',
    'silver' => 'c0c0c0',
    'skyblue' => '87ceeb',
    'slateblue' => '6a5acd',
    'slategray' => '708090',
    'snow' => 'fffafa',
    'springgreen' => '00ff7f',
    'steelblue' => '4682b4',
    'tan' => 'd2b48c',
    'teal' => '008080',
    'thistle' => 'd8bfd8',
    'tomato' => 'ff6347',
    'turquoise' => '40e0d0',
    'violet' => 'ee82ee',
    'violetred' => 'd02090',
    'wheat' => 'f5deb3',
    'white' => 'ffffff',
    'whitesmoke' => 'f5f5f5',
    'yellow' => 'ffff00',
    'yellowgreen' => '9acd32'
    );

    /**
     * Testa se uma string � uma cor v�lida
     *
     * <p>Testa se uma string � uma cor v�lida, ou seja, se � o nome ou o c�digo
     * hexadecimal de uma cor.</p>
     * @access public
     * @param string $psColor String que identifica a cor
     * @return boolean True indica que a string � uma cor v�lida
     */
    public static function isValid($psColor){
    	$psColor = strtolower($psColor);
    	if(isset(self::$caTable[$psColor])) return true;
    	else return preg_match('/[0-9a-f]{6}/',$psColor);
    }

    /**
     * Obt�m os valores de R, G e B de uma cor a partir de seu c�digo hexadecimal
     *
     * <p>Obt�m os valores de R, G e B de uma cor a partir de seu c�digo hexadecimal.
     * Caso o par�metro passado n�o seja uma cor v�lida, retorna false.</p>
     * @access public
     * @param string $psHexCode C�digo hexadecimal da cor
     * @return array Array de inteiros contendo os valores de R, G e B da cor
     */
    public static function hexToRgb($psHexCode){
    	$psHexCode = strtolower($psHexCode);
    	if(!preg_match('/[0-9a-f]{6}/',$psHexCode)) return false;
    	$maRgb = array_map('hexdec',explode('|',chunk_split($psHexCode,2,'|')));
    	unset($maRgb[3]);
    	return $maRgb;
    }

    /**
     * Obt�m o c�digo hexadecimal de uma cor a partir de seus valores R, G e B
     *
     * <p>Obt�m o c�digo hexadecimal de uma cor a partir de seus valores R, G e B.
     * </p>
     * @access public
     * @param integer $piR Valor do componente R da cor
     * @param integer $piG Valor do componente G da cor
     * @param integer $piB Valor do componente B da cor
     * @return string C�digo hexadecimal da cor
     */
    public static function rgbToHex($piR,$piG,$piB){
    	return implode('',array_map(create_function('$n','$h=dechex($n);return (strlen($h)>1?$h:"0$h");'),array($piR,$piG,$piB)));
    }

    /**
     * Obt�m o c�digo hexadecimal de uma cor a partir de seu nome
     *
     * <p>Obt�m o c�digo hexadecimal de uma cor a partir de seu nome. Caso a cor
     * n�o conste na lista de nomes, retorna false.</p>
     * @access public
     * @param string $psName Nome da cor
     * @return string C�digo hexadecimal da cor
     */
    public static function nameToHex($psName){
    	$psName = strtolower($psName);
    	if(isset(self::$caTable[$psName])) return self::$caTable[$psName];
    	else return false;
    }

    /**
     * Obt�m os valores de R, G e B de uma cor a partir de seu nome
     *
     * <p>Obt�m os valores de R, G e B de uma cor a partir de seu nome. Caso a cor
     * n�o conste na lista de nomes, retorna false.</p>
     * @access public
     * @param string $psName Nome da cor
     * @return array Array de inteiros contendo os valores de R, G e B da cor
     */
    public static function nameToRgb($psName){
    	if(isset(self::$caTable[$psName])) return self::hexToRgb(self::$caTable[$psName]);
    	else return false;
    }

}

?>