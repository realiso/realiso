<?php

interface QueryAutoCompleter {
	public function getIdValue();
	public function getNameValue();
	public function setNameFilter($name);
}

?>