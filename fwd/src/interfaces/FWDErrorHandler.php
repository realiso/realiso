<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Interface FWDErrorHandler. Interface para manipuladores de erros e exce��es.
 *
 * <p>Interface para manipuladores de erros e exce��es.</p>
 *
 * @package FWD5
 * @subpackage interface
 */
interface FWDErrorHandler {

	/**
	 * M�todo que manipula erros.
	 *
	 * <p>M�todo que manipula erros.</p>
	 * @access public
	 * @param integer $piErrNo N�mero do erro
	 * @param string $psErrStr Mensagem de erro
	 * @param string $psErrFile Nome do arquivo que gerou o erro
	 * @param integer $piErrLine N�mero da linha em que ocorreu o erro
	 */
	public function handleError($piErrNo, $psErrStr, $psErrFile, $piErrLine);

	/**
	 * M�todo que manipula exce��es.
	 *
	 * <p>M�todo que manipula exce��es.</p>
	 * @access public
	 * @param FWDException $poFWDException Exce��o
	 * @static
	 */
	public static function handleException($poFWDException);

}

?>