<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Interface FWDCollectable. Interface para elementos que podem listar seus sub-elementos
 *
 * <p>Interface para elementos que possuem sub-elementos e t�m a habilidade
 * de list�-los recursivamente.</p>
 *
 * @package FWD5
 * @subpackage interface
 */
interface FWDCollectable {

	/**
	 * Retorna um array com todos sub-elementos do elemento
	 *
	 * <p>Retorna um array contendo todos elementos contidos no elemento ou em
	 * seus descendentes.</p>
	 * @access public
	 * @return array Array de sub-elementos
	 */
	public function collect();
}
?>