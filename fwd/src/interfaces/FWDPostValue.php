<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Interface FWDPostValue. Interface para objetos que recebem valor via POST.
 *
 * <p>Interface para objetos que recebem valor via POST. Esta interface foi
 * criada para verificar os objetos que devem ser setados no XML_LOAD. Para
 * que isto ocorra, o objeto deve ter implementado os m�todos desta interface.</p>
 * @package FWD5
 * @subpackage interface
 */
interface FWDPostValue {

	/**
	 * Atribui valor � vari�vel value.
	 *
	 * <p>Atribui valor � vari�vel value, devendo retirar espa�os em branco
	 * no inicio e/ou no fim da string.</p>
	 * @access public
	 * @param string $psValue Valor a ser atribu�do
	 */
	public function setAttrValue($psValue);

	/**
	 * Atribui valor, condicionalmente, � vari�vel value.
	 *
	 * <p>Atribui valor � vari�vel value, condicionando atrav�s do segundo par�metro.
	 * Se pbForce for FALSE, n�o atribui o valor se a vari�vel tiver conte�do;
	 * se TRUE atribui mesmo que tenha conte�do.</p>
	 * @access public
	 * @param string $psValue Valor a ser atribu�do
	 * @param boolean $pbForce For�ar o valor mesmo que j� tenha conte�do
	 */
	public function setValue($psValue, $pbForce = true);

	/**
	 * Atribui nome ao objeto.
	 *
	 * @access public
	 * @param string $psName Nome do objeto
	 */
	public function setAttrName($psName);

	/**
	 * Retorna o valor da vari�vel value.
	 *
	 * @access public
	 * @return string Valor do objeto
	 */
	public function getValue();

	/**
	 * Retorna o nome do objeto.
	 *
	 * @access public
	 * @return string Nome do objeto
	 */
	public function getAttrName();
}
?>