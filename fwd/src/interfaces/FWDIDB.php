<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

define("DB_NONE", "");
define("DB_POSTGRES", "pgsql");
define("DB_ORACLE", "oci8");
define("DB_MSSQL", "mssql");
define("DB_MYSQL", "mysql");

/**
 * Interface FWDIDB.
 *
 * <p>Interface para abstra��o do banco de dados.</p>
 * @package FWD5
 * @subpackage interface
 */
interface FWDIDB {

	/**
	 * Contrutor.
	 *
	 * <p>Construtor da interface de acesso ao banco de dados.</p>
	 * @access public
	 * @param string $psType Identificador do BD {DB_NONE | DB_POSTGRES | DB_ORACLE | DB_MSSQL}
	 * @param string $psDatabase Nome da base de dados
	 * @param string $psUser Nome do usu�rio
	 * @param string $psPassword Senha do usu�rio
	 * @param string $psHost Nome do host
	 */
	public function __construct($psType, $psDatabase, $psUser, $psPassword, $psHost);

	/**
	 * Clona o objeto de conex�o com o banco de dados.
	 *
	 * <p>M�todo para clonar o objeto de conex�o com o banco de dados.</p>
	 * @access public
	 * @return FWDDB Objeto de conex�o clonado
	 */
	public function __clone();

	/**
	 * Conecta no banco.
	 *
	 * <p>M�todo para conectar no banco.</p>
	 * @access public
	 * @return boolean Verdadeiro ou Falso
	 */
	public function connect();

	/**
	 * Verifica se est� conectado ao banco.
	 *
	 * <p>M�todo para verificar se est� conectado no banco.</p>
	 * @access public
	 * @return boolean Verdadeiro ou Falso
	 */
	public function isConnected();

	/**
	 * Fecha conex�o com o banco de dados.
	 *
	 * <p>M�todo para fechar conex�o com o banco de dados.</p>
	 * @access public
	 */
	public function disconnect();

	/**
	 * Executa um sql no banco.
	 *
	 * <p>M�todo para executar um sql no banco.</p>
	 * @access public
	 * @param string $psQuery Query que vai ser executada
	 * @param string $piLimit N�mero de registros que deve ser retornado
	 * @param string $psOffset A partir de qual posi��o deve come�ar a retornar registros
	 * @return boolean Verdadeiro (sucesso) ou Falso (falha)
	 */
	public function execute($psQuery, $piLimit = 0, $piOffset = 0);

	/**
	 * Gera SQL para INSERT automaticamente.
	 *
	 * <p>M�todo para gerar o SQL para INSERT automaticamente.</p>
	 * @access public
	 * @param string $psTable Nome da tabela
	 * @param array $paFieldValues Array com os valores dos campos (Ex: array['zName'] = "Fulano")
	 * @return boolean Verdadeiro ou Falso
	 */
	public function autoInsert($psTable, $paFieldValues);

	/**
	 * Gera SQL para UPDATE automaticamente.
	 *
	 * <p>M�todo para gerar o SQL para UPDATE automaticamente.</p>
	 * @access public
	 * @param string $psTable Nome da tabela
	 * @param array $paFieldValues Array com os valores dos campos (Ex: array['zName'] = "Fulano")
	 * @param string $psWhere Cl�usula "where" para restringir o UPDATE
	 * @return boolean Verdadeiro ou Falso
	 */
	public function autoUpdate($psTable, $paFieldValues, $psWhere);

	/**
	 * Gera DELETE automaticamente.
	 *
	 * <p>M�todo para gerar o SQL DELETE automaticamente.</p>
	 * @access public
	 * @param string $psTable Nome da tabela
	 * @param string $psWhere Cl�usula "where" para restringir o UPDATE
	 * @return boolean Verdadeiro ou Falso
	 */
	public function autoDelete($psTable, $psWhere);

	/**
	 * Retorna um registro do resultado.
	 *
	 * <p>M�todo para retornar um registro do resultado.</p>
	 * @access public
	 * @return array Array contendo a informa��o ou falso caso atinja o fim
	 */
	public function fetch();

	/**
	 * Retorna um array com todos os registros retornados pelo consulta.
	 *
	 * <p>M�todo para retornar um array com todos os registros retornados pelo consulta.</p>
	 * @access public
	 * @return array Array contendo todos os registros
	 */
	public function fetchAll();

	/**
	 * Retorna o n�mero de registros retornado em um SELECT.
	 *
	 * <p>M�todo para retornar o n�mero de registros retornado em um SELECT.</p>
	 * @access public
	 * @return integer N�mero de registros
	 */
	public function getRowCount();

	/**
	 * Retorna o n�mero de registros afetados por DELETE/UPDATE.
	 *
	 * <p>M�todo para retornar o n�mero de registros afetados por DELETE/UPDATE.</p>
	 * @access public
	 * @return integer N�mero de registros
	 */
	public function getAffectedRows();

	/**
	 * Retorna o id do �ltimo registro inserido.
	 *
	 * <p>M�todo para retornar o id do �ltimo registro inserido.</p>
	 * @access public
	 * @return integer Id do �ltimo registro inserido
	 */
	public function getLastId();

	/**
	 * Retorna o n�mero de campos retornados por uma consulta.
	 *
	 * <p>M�todo para retornar o n�mero de campos retornados por uma consulta.</p>
	 * @access public
	 * @return integer N�mero de campos
	 */
	public function getFieldCount();

	/**
	 * Seta o modo de debug.
	 *
	 * <p>M�todo para setar o m�todo de debug.</p>
	 * @access public
	 * @param boolean $pbDebug Verdadeiro ou Falso
	 */
	public function debugMode($pbDebug);

	/**
	 * Retorna a �ltima mensagem de erro.
	 *
	 * <p>M�todo para retornar a �ltima mensagem de erro.</p>
	 * @access public
	 * @return string Mensagem de erro
	 */
	public function getErrorMessage();

	/**
	 * Libera da mem�ria o resultado da consulta.
	 *
	 * <p>M�todo para liberar da mem�ria o resultado da consulta.</p>
	 * @access public
	 */
	public function freeResult();

	/**
	 * Retorna a data no formato do banco.
	 *
	 * <p>M�todo para retornar a data no formato aceito pelo banco.</p>
	 * @access public
	 * @param integer $piTimestamp Timestamp
	 * @return string Data
	 */
	public function timestampFormat($piTimestamp);

	/**
	 * Retorna um timestamp baseado em uma data no formato YYYY-MM-DD HH-NN-SS.
	 *
	 * <p>M�todo para retornar um timestamp baseado em uma data no formato YYYY-MM-DD HH-NN-SS.</p>
	 * @access public
	 * @param integer $psDate Data
	 * @return integer Timestamp
	 */
	public function getTimestamp($psDate);

	/**
	 * Retorna o tipo do banco de dados
	 *
	 * <p>Retorna o tipo do banco de dados.</p>
	 * @access public
	 * @return string Tipo do banco de dados
	 */
	public function getDatabaseType();

	/**
	 * Retorna o nome da base de dados.
	 *
	 * <p>M�todo para retornar o nome da base de dados.</p>
	 * @access public
	 * @return string Nome da base de dados
	 */
	public function getDatabaseName();

	/**
	 * Retorna o host da base de dados.
	 *
	 * <p>M�todo para retornar o host da base de dados.</p>
	 * @access public
	 * @return string Host da base de dados
	 */
	public function getHost();

	/**
	 * Retorna o usu�rio que deve ser utilizado como prefixo das functions do banco
	 *
	 * <p>Usu�rio que deve ser utilizado como prefixo das functions do banco</p>
	 * @access public
	 * @return string Usu�rio da conex�o com o banco que deve ser utilizado antes do nome das functions
	 */
	public function getFunctionUserDB();

	/**
	 * Retorna a chamada da fun��o de acordo com o banco de dados.
	 *
	 * <p>M�todo para retornar a chamada da fun��o de acordo com o banco de dados.</p>
	 * @access public
	 * @param string $psFunction Fun��o que deve ser chamada
	 * @return string Chamada da fun��o
	 */
	public function getFunctionCall($psFunction);

	/**
	 * Retorna um valor constante do banco de dados.
	 *
	 * <p>M�todo para retornar um valor constante do banco de dados. Necess�rio
	 * pois no Oracle n�o s�o aceitas consultas no formato 'select 1' e sim
	 * no formato 'select 1 from dual'.</p>
	 * @access public
	 * @param string $psValue Valores [Ex: '1 as first, 2 as second, ...]
	 * @return string Select para retornar uma constante
	 */
	public function selectConstant($psValues);

	/**
	 * Retorna o nome do usu�rio do banco.
	 *
	 * <p>M�todo para retornar o nome do usu�rio do banco.</p>
	 * @access public
	 * @return string Login do usu�rio do banco
	 */
	public function getUser();

	/**
	 * Retorna a string escapada de acordo com o banco.
	 *
	 * <p>M�todo para retornar a string escapada de acordo com o banco.</p>
	 * @access public
	 * @param string $psString String
	 * @return string String escapada
	 */
	public function quote($psString);

	/**
	 * Retorna as strings concatenadas.
	 *
	 * <p>M�todo para retornar as strings concatenadas.</p>
	 * @access public
	 * @param string $psString1 String
	 * @param string $psString2 String
	 * @return string Par�metros concatenados
	 */
	public function concat($psString1, $psString2);

	/**
	 * Inicia uma transa��o.
	 *
	 * <p>M�todo para iniciar uma transa��o.</p>
	 * @access public
	 */
	public function startTransaction();

	/**
	 * Completa uma transa��o.
	 *
	 * <p>M�todo para completar uma transa��o.</p>
	 * @access public
	 * @return Verdadeiro se obtiver sucesso, falso caso contr�rio
	 */
	public function completeTransaction();

	/**
	 * For�a a falha de uma transa��o.
	 *
	 * <p>M�todo para for�ar a falha em um transa��o.</p>
	 * @access public
	 */
	public function rollbackTransaction();
}
?>