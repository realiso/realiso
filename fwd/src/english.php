<?php
$gaStrings = array(

/* './abstracts/FWDGridAbstract.php' */

'fwdgrid_pref_column_label'=>'Column:',
'fwdgrid_pref_column_name'=>'Column',
'fwdgrid_pref_column_order'=>'Order:',
'fwdgrid_pref_column_order_title'=>'Grid columns order',
'fwdgrid_pref_column_properties'=>'Column Properties',
'fwdgrid_pref_column_show_title'=>'Columns Shown and Order of Display between them',
'fwdgrid_pref_column_width'=>'Width*:',
'fwdgrid_pref_columns_grid'=>'Grid columns:',
'fwdgrid_pref_columns_show'=>'Columns Shown:',
'fwdgrid_pref_false_value'=>'No',
'fwdgrid_pref_grid_columns_title'=>'Grid columns:',
'fwdgrid_pref_grid_properties'=>'Grid Properties Edit',
'fwdgrid_pref_rows_height'=>'Rows Height:',
'fwdgrid_pref_rows_per_page'=>'Rows per Page:',
'fwdgrid_pref_select_source'=>'Select Source',
'fwdgrid_pref_select_target'=>'Select Target',
'fwdgrid_pref_target'=>'Target:',
'fwdgrid_pref_tooltip_label'=>'ToolTip:',
'fwdgrid_pref_true_value'=>'Yes',
'fwdgrid_pref_width_obs'=>'* \'0\' = automatic width',

/* './base/FWDDefaultErrorHandler.php' */

'error_config_corrupt'=>'The configuration file is corrupt.',
'error_configuration'=>'The configuration file is corrupt.',
'error_db_connection'=>'Database connection error.',
'error_db_invalid_object_name'=>'Invalid database object name. Probably using wrong DB user.',
'error_debug_permission_denied'=>'Permission denied in debug file.',
'error_session_permission_denied'=>'Permission denied in session file.',
'error_smtp'=>'Failed to connect to mailserver.',

/* './base/FWDLanguage.php' */

'default_language'=>'Portuguese',

/* './base/FWDLicense.php' */

'error_corrupt_hash'=>'Hash is corrupted.',
'error_corrupt_license'=>'License is corrupted.',
'error_permission_denied'=>'Permission denied in activation file.',

/* './base/FWDSyslog.php' */

'error_connection_syslog'=>'Could not connect to syslog server.',

/* './event/classEvent/FWDGridUserEdit.php' */

'fwdGridUserEditErrorWidthGrid'=>'Inconsistencies were found: The sum of grid\'s columns width must be less than or equal to:',
'fwdGridUserEditErrorWidthGridExplain'=>'however the obtained value is:',

/* './formats/FWDPDF.php' */

'fpdf_page'=>'Page',

/* './report/FWDReportClassificator.php' */

'report_confidencial'=>'Confidential Report',
'report_institutional'=>'Institucional Report',
'report_public'=>'Public Report',

/* './report/FWDReportGenerator.php' */

'report_confidential_report'=>'Confidential Report',
'report_excel'=>'Excel',
'report_html'=>'HTML',
'report_html_download'=>'HTML Download',
'report_institutional_report'=>'Institucional Report',
'report_pdf'=>'PDF',
'report_public_report'=>'Public Report',
'report_word'=>'Word',

/* './view/FWDCalendar.php' */

'locale'=>'en_us',

/* './view/FWDGrid.php' */

'fwdgrid_info_no_result_rows'=>'No results were found',
'fwdgrid_number_page_of'=>'of',
'fwdgrid_page_label'=>'page',

/* './view/FWDTreeAjax.php' */

'FWDLoading'=>'Loading',
);
?>