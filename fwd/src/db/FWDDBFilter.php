<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDDBFilter.
 *
 * <p>Armazena os filtros que ser�o aplicados no campo.</p>
 * @package FWD5
 * @subpackage db
 */
class FWDDBFilter {

	/**
	 * Array com os poss�veis valores de operadores
	 * @var array
	 * @access protected
	 */
	private $caOperators = array('=', '!=', '<>', '>', '<', '>=', '<=', 'like', 'in', 'null', 'notnull');

	/**
	 * Array com os valores para restri��o do campo
	 * @var array
	 * @access protected
	 */
	protected $caValues = array();

	/**
	 * Operador {=, !=, <>, >, <, >=, <=, like, in, null, notnull}
	 * @var string
	 * @access protected
	 */
	protected $csOperator = "";

	/**
	 * Nome do campo ao qual o filtro pertence
	 * @var string
	 * @access protected
	 */
	protected $csField = "";

	/**
	 * Tipo do campo ao qual o filtro pertence
	 * @var integer
	 * @access protected
	 */
	protected $ciType = DB_UNKNOWN;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDDBFilter.</p>
	 * @access public
	 * @param string $psField Nome do campo
	 * @param string $psOperator Operador
	 * @param string $psValue Valor para compara��o
	 */
	public function __construct($psOperator, $psValue = null){
		$this->setOperator($psOperator);
		if($psValue!==null){
			if($psOperator=='null' || $psOperator=='notnull'){
				trigger_error("Operator '$psOperator' does not take values.", E_USER_WARNING);
			}else{
				if(is_array($psValue)) $this->caValues = $psValue;
				else $this->addValue($psValue);
			}
		}
	}

	/**
	 * Seta o operador.
	 *
	 * <p>M�todo para setar o operador.</p>
	 * @access public
	 * @param string $psOperator Operador
	 */
	public function setOperator($psOperator){
		if (in_array($psOperator, $this->caOperators)) $this->csOperator = $psOperator;
		else trigger_error("Invalid SQL Operator: $psOperator", E_USER_WARNING);
	}

	/**
	 * Retorna o operador.
	 *
	 * <p>M�todo para retornar o operador.</p>
	 * @access public
	 * @return string Operador
	 */
	public function getOperator(){
		return $this->csOperator;
	}

	/**
	 * Seta o nome do campo ao qual o filtro se refere.
	 *
	 * <p>M�todo para setar o nome do campo ao qual o filtro se refere.</p>
	 * @access public
	 * @param string $psField Campo
	 */
	public function setField($psField){
		$this->csField = $psField;
	}

	/**
	 * Retorna o nome do campo ao qual o filtro se refere.
	 *
	 * <p>M�todo para retornar o nome do campo ao qual o filtro se refere.</p>
	 * @access public
	 * @return string Campo
	 */
	public function getField(){
		return $this->csField;
	}

	/**
	 * Seta o tipo do campo ao qual o filtro se refere.
	 *
	 * <p>M�todo para setar o tipo do campo ao qual o filtro se refere.</p>
	 * @access public
	 * @param integer $piType Campo
	 */
	public function setFieldType($piType){
		$this->ciType = $piType;
	}

	/**
	 * Retorna o tipo do campo ao qual o filtro se refere.
	 *
	 * <p>M�todo para retornar o tipo do campo ao qual o filtro se refere.</p>
	 * @access public
	 * @return integer Tipo
	 */
	public function getFieldType(){
		return $this->ciType;
	}

	/**
	 * Adiciona um valor ao filtro.
	 *
	 * <p>M�todo para adicionar valores no filtro.</p>
	 * @access public
	 * @param string $psValue Valor
	 */
	public function addValue($psValue){
		if($this->csOperator=='null' || $this->csOperator=='notnull'){
			trigger_error("Operator '{$this->csOperator}' does not take values.", E_USER_WARNING);
		}else{
			$this->caValues[] = $psValue;
		}
	}

	/**
	 * Retorna o valor do campo.
	 *
	 * <p>M�todo para retornar o valor do campo.</p>
	 * @access public
	 * @return string Valor do campo
	 */
	public function getValue(){
		return $this->caValues;
	}

	/**
	 * Retorna o SQL referente ao filtro.
	 *
	 * <p>M�todo para retornar o SQL referente ao filtro.</p>
	 * @access public
	 * @return string SQL do filtro
	 */
	public function getSQL(){
		$msSQL = "";
		$miCount = count($this->caValues);

		switch($this->csOperator){
			case "=":
				$miI = 0;
				foreach($this->caValues as $msValue){
					if ($miI) $msSQL .= " OR ";
					$msSQL .= $this->csField . " = " . FWDDBType::translate($msValue, $this->getFieldType());
					$miI++;
				}
				if ($miCount > 1) $msSQL = "(" . $msSQL . ")";
				break;

			case "!=": case "<>":
				$miI = 0;
				foreach($this->caValues as $msValue){
					if ($miI) $msSQL .= " AND ";
					$msSQL .= $this->csField . " != " . FWDDBType::translate($msValue, $this->getFieldType());
					$miI++;
				}
				if ($miCount > 1) $msSQL = "(" . $msSQL . ")";
				break;

			case ">": case ">=": case "<": case "<=":
				$miI = 0;
				foreach($this->caValues as $msValue){
					if ($miI) $msSQL .= " AND ";
					$msSQL .= $this->csField . " {$this->csOperator} " . FWDDBType::translate($msValue, $this->getFieldType());
					$miI++;
				}
				if (count($this->caValues) != 1)
				trigger_error("SQL could be better written: $msSQL", E_USER_WARNING);
				break;

			case "in":
				$msSQL = $this->csField . " in (";
				if ($miCount){
					$miI = 0;
					foreach($this->caValues as $msValue){
						$msSQL .= FWDDBType::translate($msValue, $this->getFieldType());
						if ($miI != $miCount - 1) $msSQL .= ", ";
						$miI++;
					}
					$msSQL .= ")";
				} else trigger_error("SQL \"in\" operator must have at least one value: $msSQL", E_USER_WARNING);
				break;

			case "like":
				$miI = 0;
				foreach($this->caValues as $msValue){
					if ($miI) $msSQL .= " OR ";
					$msDBType = FWDWebLib::getConnection()->getDatabaseType();
					if($msDBType==DB_MSSQL){
						$msValue = str_replace('[','[[]',$msValue);
					}
					$msSQL .= $this->csField . " like " . FWDDBType::translate($msValue, $this->getFieldType());
					$miI++;
				}
				if ($miCount > 1) $msSQL = "(" . $msSQL . ")";
				break;

			case 'null':
				$msSQL = $this->csField.' is NULL';
				break;

			case 'notnull':
				$msSQL = $this->csField.' is not NULL';
				break;
		}
		return $msSQL;
	}
}
?>