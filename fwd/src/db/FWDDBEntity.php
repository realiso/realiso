<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDDBEntity.
 *
 * <p>Classe que representa uma entidade do banco de dados.</p>
 * @package FWD5
 * @subpackage db
 */
class FWDDBEntity {
		
	/**
	 * Objeto FWDDBDataSet
	 * @var FWDDBDataSet
	 * @access protected
	 */
	protected $coDataSet = NULL;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDDBEntity.</p>
	 * @access public
	 * @param FWDDB $poDB Objeto de conex�o com o banco
	 * @param string $psTable Nome da tabela (opcional)
	 */
	public function __construct(FWDDB $poDB, $psTable = "") {
		$this->coDataSet = new FWDDBDataSet($poDB, $psTable);
	}

	/**
	 * Adiciona um campo � entidade.
	 *
	 * <p>M�todo para adicionar um campo � entidade.</p>
	 * @access public
	 * @param FWDDBField $poField Campo
	 */
	public function addField(FWDDBField $poField) {
		$this->coDataSet->addFWDDBField($poField);
	}

	/**
	 * Executa uma query.
	 *
	 * <p>M�todo para executar uma query.</p>
	 * @access public
	 * @param object $psQuery Consulta que deve ser executada
	 */
	public function execute($psQuery) {
		$this->coDataSet->setQuery($psQuery);
		$this->coDataSet->execute();
	}

	/**
	 * Executa a consulta para buscar os dados da entidade.
	 *
	 * <p>M�todo para executa a consulta para buscar os dados da entidade.</p>
	 * @access public
	 * @return FWDDBDataSet DataSet
	 */
	public function select() {
		$this->coDataSet->select();
		return $this->coDataSet;
	}

	/**
	 * Insere os dados referentes � entidade no banco de dados.
	 *
	 * <p>M�todo para inserir os dados referentes � entidade no banco de dados.</p>
	 * @access public
	 * @return integer Id do registro inserido ou falso
	 */
	public function insert() {
		return $this->coDataSet->insert();
	}

	/**
	 * Atualiza dados referentes � entidade no banco de dados.
	 *
	 * <p>M�todo para atualizar dados referentes � entidade no banco de dados.</p>
	 * @access public
	 * @return integer N�mero de registros afetados pelo update ou falso em caso de falha
	 */
	public function update() {
		return $this->coDataSet->update();
	}

	/**
	 * Deleta do banco baseado nos valores da view.
	 *
	 * <p>M�todo para deletar do banco baseado nos valores da view.</p>
	 * @access public
	 * @return integer N�mero de registros afetados pelo delete ou falso em caso de falha
	 */
	public function delete() {
		return $this->coDataSet->delete();
	}
	/**
	 * Seta um filtro para a consulta.
	 *
	 * <p>M�todo para setar um filtro para consulta (normalmente o id do objeto buscado).</p>
	 * @access public
	 * @param string $psKeyAlias Alias do campo chave
	 * @param string $psValue Valor do campo
	 * @return booleam Verdadeiro ou falso
	 */
	public function setKey($psKeyAlias, $psValue) {
		$moField = $this->coDataSet->GetFieldByAlias($psKeyAlias);
		if(isset($moField)){
			$moFilter = new FWDDBFilter("=", $psValue);
			$moField->addFilter($moFilter);
			return true;
		}
		return false;
	}

	/**
	 * Seta o valor de um campo da entidade.
	 *
	 * <p>M�todo para setar o valor de um campo da entidiade.</p>
	 * @access public
	 * @param string $psAlias Alias do campo
	 * @param string $psValue Valor do campo
	 * @return boolean Verdadeiro ou Falso
	 */
	public function setValue($psAlias, $psValue) {
		$moField = $this->coDataSet->getFieldByAlias($psAlias);
		if(isset($moField)) {
			$moField->setValue($psValue);
			return true;
		} else
		return false;
	}

	/**
	 * Retorna o valor de um campo da entidade.
	 *
	 * <p>M�todo para retornar o valor de um campo da entidiade.</p>
	 * @access public
	 * @param string $psAlias Alias do campo
	 * @return string Valor do campo ou falso
	 */
	public function getValue($psAlias) {
		$moField = $this->coDataSet->GetFieldByAlias($psAlias);
		if(isset($moField))
		return $moField->GetValue();
		else return false;
	}
}
?>