<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDDBGrid. Popula elementos FWDDBGrid com informa��es do BD.
 *
 * <p>Classe que popula elementos FWDDBGrid com informa��es do BD.</p>
 * @package FWD5
 * @subpackage db
 */
class FWDDBGrid extends FWDGrid {

	/**
	 * Objeto de conex�o com o banco de dados
	 * @var FWDDBDataSet
	 * @access protected
	 */
	protected $coDataSet = null;

	/**
	 * QueryHandler
	 * @var FWDDBQueryHandler
	 * @access protected
	 */
	protected $coQueryHandler = NULL;

	/**
	 * Numero m�ximo de linhas que devem ser exibidas pela grid
	 * @var string
	 * @access protected
	 */
	protected $ciMaxRow = 0;

	/**
	 * Seta o DataSet.
	 *
	 * <p>M�todo para setar o objeto DataSet.</p>
	 * @param FWDDBDataSet $poDataSet DataSet
	 * @access public
	 */
	public function setDataSet(FWDDBDataSet $poDataSet) {
		$this->coDataSet = $poDataSet;
	}

	/**
	 * Seta o n�mero maximo de linhax a serem exibidas.
	 *
	 * <p>M�todo utilizado para limitar o n�mero m�ximo de linhas que ser�o
	 * exibidas em uma dbGrid est�tica.</p>
	 * @param integer $piMaxRow numero maximo de linhas que ser�o exibidas
	 * @access public
	 */
	public function setAttrMaxRow($piMaxRow){
		$this->ciMaxRow = $piMaxRow;
	}

	/**
	 * Retorna o numero m�ximo de linhas que ser�o desenhadas.
	 *
	 * <p>M�todo para retornar o m�ximo de linhas que ser�o desenhadas em uma dbGrid
	 * est�tica.</p>
	 * @access public
	 * @return integer ciMaxRow
	 */
	public function getAttrMaxRow($piMaxRow){
		return $this->ciMaxRow;
	}

	/**
	 * Seta o QueryHandler.
	 *
	 * <p>M�todo para setar o objeto QueryHandler.</p>
	 * @param FWDDBQueryHandler $poQueryHandler QueryHandler
	 * @access public
	 */
	public function setQueryHandler(FWDDBQueryHandler $poQueryHandler) {
		$poQueryHandler->makeQuery();
		$this->coDataSet = $poQueryHandler->getDataset();
		$this->coDataSet->setQuery($poQueryHandler->getSQL());
		$this->coQueryHandler = $poQueryHandler;
	}

	/**
	 * Retorna o QueryHandler.
	 *
	 * <p>M�todo para retornar o objeto QueryHandler.</p>
	 * @access public
	 * @return FWDDBQueryHandler QueryHandler
	 */
	public function getQueryHandler() {
		return $this->coQueryHandler;
	}

	/**
	 * Retorna o DataSet.
	 *
	 * <p>M�todo para retornar o objeto DataSet.</p>
	 * @return FWDDBDataSet DataSet
	 * @access public
	 */
	public function getDataSet() {
		return $this->coDataSet;
	}

	/**
	 * Seta a Query.
	 *
	 * <p>M�todo para setar a Query do DataSet.</p>
	 * @param string $psQuery Query
	 * @access public
	 */
	public function setQuery($psQuery) {
		$this->coDataSet->setQuery($psQuery);
	}

	/**
	 * Desenha o Body da dbGrid.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o corpo da dbGrid.
	 * retorna o c�digo HTML de cada uma das linhas da grid</p>
	 * @access public
	 * @return string C�digo HTML que ser� inserido na div do body da dbGrid
	 */
	function drawBody($pbExecPopulate = true, $pbOrderColumn = true)
	{
		if(($this->cbPopulateXML==true)&&($this->cbPopulate==true)){
			$this->populate($pbExecPopulate,$pbOrderColumn);
		}
		return parent::drawBody();
	}

	/**
	 * Renderiza a grid via ajax.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para renderizar os bot�es de p�gina��o, o conte�do do corpo,
	 * e os menus de contexto, incluindo os menus ACL da grid</p>
	 * @access public
	 * @return string C�digo HTML que cont�m os bot�es de pag., o corpo, e os menus da grid
	 */
	public function execEventPopulate($psGridEvent = 'populategrid'){
		$msHeaderPageButton="";
		$msBodyContent="";
		$maContextMenuCode = array();
		$maGridCode = array();

		if(!($psGridEvent=='refreshselect')){
			$maGridCode[] = $psGridEvent;
			$this->caGridRowsSelected=array();
			$this->caGridValue = array();
		}else{
			$maGridCode[] = 'refresh';
		}
		if($this->cbPopulateXML==true){
			//obtem o total de linhas retornadas na query e seta na grid
			if(!($psGridEvent=='refreshselect') && ($this->getAttrRefreshOnCurrentPage()==false)){
				$this->ciCurrentPage=1;
				$this->ciLastPage=1;
			}

			if(($this->cbDinamicFill==true)&&($this->cbPopulateXML)){
				if(!$this->getAttrRefreshOnCurrentPage()){
					$moDataSet = $this->getDataSet();
					$msOrder = $this->getAttrOrder();
					$maOrder = explode(":",$msOrder);
					if(($maOrder)&&($moDataSet)){
						foreach($maOrder as $msColOrder){
							$msColOrder=trim($msColOrder);
							if($msColOrder!=""){
								$miColumn="";
								$msChar = substr(trim($msColOrder),0,1);
								if(($msChar=="+")||($msChar=="-"))// ascendente
								$miColumn = substr(trim($msColOrder),1);
								else{//assumindo que soh veio um n�mero , ordena��o ascendente
									$miColumn = trim($msColOrder);
									$msChar="+";
								}
								$miColumn--;
								$moColumn = $this->caColumns[$miColumn];
								if($moColumn)
								if(($moColumn->getAttrAlias())&&($moColumn->getAttrCanOrder()))
								$moDataSet->setOrderBy($moColumn->getAttrAlias(),$msChar);
								else
								trigger_error("Trying to order an inexistent column! column number '{$miColumn}'",E_USER_WARNING);
							}//else{}//msColOrder vazio devido a um : amais na string
						}
					}
					if($this->coDataSet){
						$this->coDataSet->execute();
						$miTotalRows = $this->coDataSet->getRowCount();
						$this->ciRowsCount=$miTotalRows;
						$msHeaderPageButton = $this->drawPageButtons();
					}
					//desenha o body da grid via ajax
					$this->cbPopulate=true;
					$msBodyContent = $this->drawBody(false,false);
				}else{
					/*redesenha a grid sem trocar de p�gina*/
					if($this->coDataSet){
						$this->coDataSet->execute();
						$miTotalRows = $this->coDataSet->getRowCount();
						$this->ciRowsCount = $miTotalRows;
						$miCurrPage = $this->getCurrentPage();
						if( (( ($miCurrPage-1) * $this->ciRowsPerPage + 1 ) > $this->ciRowsCount ) && ( $miCurrPage > 1 ) ){
							$miNewActualPage = floor( $this->ciRowsCount / $this->ciRowsPerPage );
							$miNewActualPage = $miNewActualPage > 0 ? $miNewActualPage : 1;
							$this->setCurrentPage( $miNewActualPage );
						}
						$msHeaderPageButton = $this->drawPageButtons();
					}
					$this->cbPopulate=true;
					$msBodyContent = $this->drawBody();
				}
			}else{
				$this->cbPopulate=true;
				$this->arrangeAllDimensions();
				$msBodyContent = $this->drawBody(false,true);
			}
			//obtem o c�digo html do menu de contexto
			$maContextMenuCode = $this->drawMenuAjax();
		}

		$maGridCode[] = $msHeaderPageButton;
		$maGridCode[] = $msBodyContent;
		$maGridButtomCode = array();
		if($psGridEvent == 'user_edit'){
			//desenha os headers das colunas
			$maGridCode[] = $this->drawHeaderColumns();
		}else{
			if(($this->cbShowEdit)&&($this->cbPopulateXML)){
				//bot�o de edi��o das propriedades da grid
				$maGridButtomCode[] = $this->drawButtonEdit();
				//desenha os headers das colunas
				$maGridButtomCode[] = $this->drawHeaderColumns();
			}
		}
		//obtem o c�digo html do menu de contexto
		$maGridCodeAux = array_merge($maGridCode,$this->drawMenuAjax());
		$maGridCodeAux = array_merge($maGridCodeAux,$maGridButtomCode);
		//escapa os /r e os /n
		for($miI=0;$miI<count($maGridCodeAux);$miI++){
			$maGridCodeAux[$miI] = $this->scapeStringToAjax($maGridCodeAux[$miI]);
		}
		$msGridCodeAux = serialize($maGridCodeAux);

		$msGridCodeAux = str_replace('\\','\\\\',$msGridCodeAux);
		$msGridCodeScape = str_replace('"','\"',$msGridCodeAux);
		$moJsGridEvent = new FWDJsEvent(JS_GRID_EVENT,$this->csName,$msGridCodeScape);
		echo $moJsGridEvent->render();
	}

	/**
	 * Popula o FWDDBGrid.
	 *
	 * <p>M�todo para popular o FWDDBGrid com itens (linhas retornadas na query).</p>
	 * @access public
	 */
	public function populate($pbExecPopulate = true,$cbOrderColumn =true) {

		$moDataSet = $this->getDataSet();
		$msOrder = $this->getAttrOrder();
		$maOrder = explode(":",$msOrder);
		if(($maOrder)&&($moDataSet)&&($cbOrderColumn==true))
		foreach($maOrder as $msColOrder){
			$msColOrder=trim($msColOrder);
			if($msColOrder!=""){
				$miColumn="";
				$msChar = substr(trim($msColOrder),0,1);
				if(($msChar=="+")||($msChar=="-"))// ascendente
				$miColumn = substr(trim($msColOrder),1);
				else{//assumindo que soh veio um n�mero , ordena��o ascendente
					$miColumn = trim($msColOrder);
					$msChar="+";
				}
				$miColumn--;
				$moColumn = $this->caColumns[$miColumn];
				if($moColumn)
				if(($moColumn->getAttrAlias())&&($moColumn->getAttrCanOrder()))
				$moDataSet->setOrderBy($moColumn->getAttrAlias(),$msChar);
				else
				trigger_error("Trying to order an inexistent column! column number '{$miColumn}'",E_USER_WARNING);
			}//else{}//msColOrder vazio devido a um : amais na string
		}

		$this->setDataSet($moDataSet);
		if($this->cbPopulateXML==true){
			if (!$this->cbDinamicFill) {
				$this->coDataSet->execute();
				if($this->ciMaxRow>0)
				$miContMaxRowToDraw = min($this->coDataSet->getRowCount(),$this->ciMaxRow);
				else
				$miContMaxRowToDraw = 	$this->coDataSet->getRowCount();

				for ($miLin=1; $miLin<=$miContMaxRowToDraw; $miLin++) {
					$this->coDataSet->fetch();
					$miCol=0;
					foreach($this->caColumns as $moColumn){
						$miCol++;
						$moField = $this->coDataSet->getFieldByAlias($moColumn->getAttrAlias());
						if($moField){
							$this->setItem($miCol,$miLin,$moField->getValue());
						}
						//else{}//coluna sem dados ou os dados n�o vem do banco para esta coluna
					}
				}
			}
			else{
				$miOffSet = (($this->getCurrentPage()-1) * $this->ciRowsPerPage);
				/*descobrir se a p�gina do offset n�o esta v�zia, se tiver voltar � uma p�gina cheia*/
				if($pbExecPopulate==true)
				$this->coDataSet->execute($this->ciRowsPerPage, $miOffSet);

				$miContField=$this->coDataSet->getFieldCount();

				for ($miLin=1; $miLin<=min($this->ciRowsPerPage,$this->ciRowsCount-$miOffSet); $miLin++)
				{
					$this->coDataSet->fetch();
					$miCol=0;
					foreach($this->caColumns as $moColumn){
						$miCol++;
						$moField = $this->coDataSet->getFieldByAlias($moColumn->getAttrAlias());
						if($moField)
						$this->setItem($miCol,$miLin,$moField->getValue());
						if($miCol > $miContField)
						break;
					}//foreach
				}//for
			}
		}
	}
}
?>