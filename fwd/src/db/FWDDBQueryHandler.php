<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDDBQueryHandler.
 *
 * <p>Classe abstrata que representa uma consulta SQL que deve ser armazenada em um FWDDBDataSet.</p>
 * @package FWD5
 * @subpackage db
 */
abstract class FWDDBQueryHandler {
		
	/**
	 * Objeto FWDDBDataSet
	 * @var FWDDBDataSet
	 * @access protected
	 */
	protected $coDataSet = NULL;

	/**
	 * Cosulta SQL
	 * @var string
	 * @access protected
	 */
	protected $csSQL = "";

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDDBQueryHandler.</p>
	 * @access public
	 * @param FWDDB $poDB Objeto de conex�o com o banco
	 */
	public function __construct($poDB = null){
		if($poDB){
			$moDB = $poDB;
		}else{
			$moDB = FWDWebLib::getConnection();
		}
		$this->coDataSet = new FWDDBDataSet($moDB);
	}

	/**
	 * Retorna o dataset com os resultados da consulta.
	 *
	 * <p>M�todo para retornar o dataset com os resultados da consulta.</p>
	 * @access public
	 * @return FWDDBDataSet Dataset
	 */
	public function getDataset() {
		return $this->coDataSet;
	}

	/**
	 * Retorna o SQL da consulta.
	 *
	 * <p>M�todo para retornar o SQL da consulta.</p>
	 * @access public
	 * @return string SQL
	 */
	public function getSQL() {
		return $this->csSQL;
	}

	/**
	 * Constr�i a consulta.
	 *
	 * <p>M�todo para construir a consulta.</p>
	 * @access protected
	 */
	abstract protected function makeQuery();

	/**
	 * Executa a consulta.
	 *
	 * <p>M�todo para executar a consulta.</p>
	 * @access public
	 * @return integer N�mero de registros retornados pela consulta
	 */
	public function executeQuery($piLimit = 0, $piOffset = 0, $skipInjection = false) {
		if(isset($this->coDataSet, $this->csSQL)) {
			$this->coDataSet->setQuery($this->csSQL);
			if ($this->coDataSet->execute($piLimit,$piOffset, true))
			return $this->coDataSet->getRowCount();
			else
			trigger_error("Invalid SQL statement", E_USER_WARNING);
		}
		else
		trigger_error("Dataset and/or query not initialized", E_USER_WARNING);
	}

	/**
	 * Busca um registro do resultado e avan�a para o pr�ximo.
	 *
	 * <p>M�todo para buscar um registro do resultado e avan�ar para o pr�ximo.</p>
	 * @access public
	 * @return boolean Verdadeiro (sucesso) ou Falso (falha)
	 */
	public function fetch(){
		return $this->coDataSet->fetch();
	}

	/**
	 * Retorna o valor de uma campo do dataset.
	 *
	 * <p>M�todo para retornar o valor de um campo do dataset.</p>
	 * @access public
	 * @param string $psFieldAlias Alias do campo
	 * @return string Valor do campo
	 */
	public function getFieldValue($psFieldAlias){
		$moField = $this->coDataSet->getFieldByAlias($psFieldAlias);
		if($moField){
			return $moField->getValue();
		}else{
			trigger_error("There is no field with the alias \"{$psFieldAlias}\"", E_USER_WARNING);
		}
	}

	/**
	 * Retorna um array associativo com os valores dos campos.
	 *
	 * <p>Retorna um array associativo com os valores dos campos, no formato alias=>valor.</p>
	 * @access public
	 * @return array Array associativo com os valores dos campos
	 */
	public function getFieldValues(){
		return $this->coDataSet->getFieldValues();
	}

}
?>