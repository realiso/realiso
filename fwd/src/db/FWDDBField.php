<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

define("DB_UNKNOWN", 0);
define("DB_NUMBER", 1);
define("DB_STRING", 2);
define("DB_DATETIME", 3);

/**
 * Classe FWDDBField. Representa um campo do banco de dados.
 *
 * <p>Classe que representa um campo do banco de dados.</p>
 * @package FWD5
 * @subpackage db
 */
class FWDDBField {

	/**
	 * Nome do campo
	 * @var string
	 * @access protected
	 */
	protected $csName = "";

	/**
	 * Alias do campo
	 * @var string
	 * @access protected
	 */
	protected $csAlias = "";

	/**
	 * Tipo do campo
	 * @var integer
	 * @access protected
	 */
	protected $ciType = DB_UNKNOWN;
	
	/**
	 * Indica nome da coluna
	 * @var string
	 * @access protected
	 */
	protected $label = "";
	
	/**
	 * Indica se a coluna deve aparecer em relat�rio
	 * @var boolean
	 * @access protected
	 */
	protected $showColumn = true;

	/**
	 * Valor do campo
	 * @var string
	 * @access protected
	 */
	protected $csValue = "";

	/**
	 * Array de FWDDBFilter
	 * @var array
	 * @access protected
	 */
	protected $caFilters = array();

	/**
	 * Define se o campo pode ser atualizado
	 * @var boolean
	 * @access private
	 */
	private $cbUpdatable = false;

	/**
	 * Define se o campo deve ser decodificado antes de ir para o banco
	 * @var boolean
	 * @access private
	 */
	private $cbDecode = true;

	/**
	 * Define se deve separar os filtros por AND (=true) ou por OR (=false)
	 * @var boolean
	 * @access protected
	 */
	protected $cbAnd = true;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDDBField.</p>
	 * @access public
	 * @param string $psName Nome
	 * @param string $psAlias Alias
	 * @param integer $piType Tipo
	 * @param string $psValue Valor
	 * @param boolean $pbUpdatable Pode ser atualizado ou n�o
	 * @param string $label Nome real para o campo (nome exibido na tela).
	 */
	public function __construct($psName, $psAlias, $piType, $psValue = "", $pbUpdatable = false) {
		$this->csName = $psName;
		$this->csAlias = $psAlias;
		$this->ciType = $piType;
		if (!($psValue === "")) {
			$this->csValue = $psValue;
			$this->cbUpdatable = true;
		}
		else $this->cbUpdatable = $pbUpdatable;
	}

	/**
	 * Seta o nome do campo.
	 *
	 * <p>M�todo para setar o nome do campo.</p>
	 * @access public
	 * @param string $psName Nome do campo
	 */
	public function setName($psName) {
		$this->csName = $psName;
	}

	/**
	 * Retorna o nome do campo.
	 *
	 * <p>M�todo para retornar o nome do campo.</p>
	 * @access public
	 * @return string Nome do campo
	 */
	public function getName() {
		return $this->csName;
	}

	/**
	 * Define se o campo � atualiz�vel ou n�o.
	 *
	 * <p>M�todo para definir se o campo � atualiz�vel ou n�o.</p>
	 * @access public
	 * @param boolean $pbUpdatable Verdadeiro ou Falso
	 */
	public function setUpdatable($pbUpdatable) {
		$this->cbUpdatable = $pbUpdatable;
	}

	/**
	 * Verifica se o campo � atualiz�vel.
	 *
	 * <p>M�todo para verificar se o campo � atualiz�vel.</p>
	 * @access public
	 * @return boolean Verdadeiro ou Falso
	 */
	public function isUpdatable() {
		return $this->cbUpdatable;
	}

	/**
	 * Define se o campo � atualiz�vel ou n�o.
	 *
	 * <p>M�todo para definir se o campo � atualiz�vel ou n�o.</p>
	 * @access public
	 * @param boolean $pbUpdatable Verdadeiro ou Falso
	 */
	public function setDecode($pbUpdatable) {
		$this->cbDecode = $pbUpdatable;
	}

	/**
	 * Verifica se o campo � atualiz�vel.
	 *
	 * <p>M�todo para verificar se o campo � atualiz�vel.</p>
	 * @access public
	 * @return boolean Verdadeiro ou Falso
	 */
	public function mustDecode() {
		return $this->cbDecode;
	}

	/**
	 * Define se deve separar os filtros por AND (=true) ou OR (=false).
	 *
	 * <p>M�todo para definir se deve separar os filtros por AND (=true) ou OR (=false).</p>
	 * @access public
	 * @param boolean $pbAnd Verdadeiro (and) ou Falso (or)
	 */
	public function setFilterSeparator($pbAnd) {
		$this->cbAnd = $pbAnd;
	}

	/**
	 * Retorna o separador de filtros.
	 *
	 * <p>M�todo para retornar o separador de filtros (true = and | false = or).</p>
	 * @access public
	 * @return boolean Verdadeiro ou Falso
	 */
	public function getFilterSeparator() {
		return $this->cbAnd;
	}

	/**
	 * Seta o alias do campo.
	 *
	 * <p>M�todo para setar o alias do campo.</p>
	 * @access public
	 * @param string $psAlias Alias do campo
	 */
	public function setAlias($psAlias) {
		$this->csAlias = $psAlias;
	}

	/**
	 * Retorna o alias do campo.
	 *
	 * <p>M�todo para retornar o alias do campo.</p>
	 * @access public
	 * @return string Alias do campo
	 */
	public function getAlias() {
		return $this->csAlias;
	}
	
	
	/**
	 * Seta o nome real do campo.
	 *
	 * <p>M�todo para setar o nome real do campo.</p>
	 * @access public
	 * @return string Nome do campo
	 */
	public function setLabel($label) {
		$this->label = $label;
	}	

	/**
	 * Retorna o nome real do campo.
	 *
	 * <p>M�todo para retornar o nome real do campo.</p>
	 * @access public
	 * @return string Alias do campo
	 */
	public function getLabel() {
		return $this->label;
	}
	
	public function setShowColumn($b) {
		$this->showColumn = $b;
	}
	
	public function getShowColumn() {
		return $this->showColumn;
	}	
	
	/**
	 * Seta o tipo do campo.
	 *
	 * <p>M�todo para setar o tipo do campo.</p>
	 * @access public
	 * @param integer $piType Tipo do campo
	 */
	public function setType($piType) {
		$this->ciType = $piType;
	}

	/**
	 * Retorna o tipo do campo.
	 *
	 * <p>M�todo para retornar o tipo do campo.</p>
	 * @access public
	 * @return integer Tipo do campo
	 */
	public function getType() {
		return $this->ciType;
	}

	/**
	 * Seta o valor do campo.
	 *
	 * <p>M�todo para setar o valor do campo.</p>
	 * @access public
	 * @param string $psName Valor do campo
	 * @param string $pbDecode Decodificar o campo antes de inserir no banco?
	 */
	public function setValue($psValue, $pbUpdatable = true, $pbDecode = true) {
		$this->setUpdatable($pbUpdatable);
		$this->setDecode($pbDecode);
		$this->csValue = $this->ciType == DB_STRING ? FWDWebLib::convertToISO($psValue) : $psValue;
	}

	/**
	 * Retorna o valor do campo.
	 *
	 * <p>M�todo para retornar o valor do campo.</p>
	 * @access public
	 * @return string Valor do campo
	 */
	public function getValue() {
		$msValue = '';
		if ($this->ciType == DB_STRING) $msValue = FWDWebLib::convertToISO($this->csValue);
		elseif ($this->ciType == DB_DATETIME){
			if($this->csValue==='null'){
				$msValue = 'null';
			}else{
				$msValue = (is_numeric($this->csValue) || !$this->csValue) ? $this->csValue : date('Y-m-d H:i:s', strtotime($this->csValue));
			}
		}else $msValue = $this->csValue;
		return $msValue;
	}

	/**
	 * Adiciona um filtro no campo.
	 *
	 * <p>M�todo para adicionar um filtro no campo.</p>
	 * @access public
	 * @param FWDDBFilter $poFilter Filtro
	 */
	public function addFilter(FWDDBFilter $poFilter) {
		$poFilter->setField($this->getName());
		$poFilter->setFieldType($this->getType());
		$this->caFilters[] = $poFilter;
	}

	/**
	 * Remove todos os filtros do campo.
	 *
	 * <p>Remove todos os filtros do campo.</p>
	 * @access public
	 */
	public function clearFilters(){
		$this->caFilters = array();
	}

	/**
	 * Retorna todos os filtros do campo.
	 *
	 * <p>M�todo para retornar todos os filtros do campo.</p>
	 * @access public
	 * @return array Array de filtros
	 */
	public function getFilters() {
		return $this->caFilters;
	}

	/**
	 * Retorna o SQL referente aos filtros.
	 *
	 * <p>M�todo para retornar o SQL referente ao filtros.</p>
	 * @access public
	 * @return string SQL do filtro
	 */
	public function getFiltersSQL() {
		$msFilter = "";
		$miCount = count($this->caFilters);
		$miI = 0;
		foreach($this->caFilters as $moFilter) {
			$msFilter .= $moFilter->getSQL();
			if ($miI != $miCount - 1) {
				$msFilter .=  $this->getFilterSeparator() ? " AND " : " OR ";
			}
			$miI++;
		}
		return $msFilter;
	}
}
?>