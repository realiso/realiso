<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDDBSelect. Popula elementos FWDDBSelect com informa��es do BD.
 *
 * <p>Classe que popula elementos FWDDBSelect com informa��es do BD.</p>
 * @package FWD5
 * @subpackage db
 */
class FWDDBSelect extends FWDSelect {
	/**
	 * Objeto de conex�o com o banco de dados
	 * @var FWDDBDataSet
	 * @access protected
	 */
	protected $coDataSet = NULL;

	/**
	 * Seta o DataSet.
	 *
	 * <p>M�todo para setar o objeto DataSet.</p>
	 * @param FWDDBDataSet $poDataSet DataSet
	 * @access public
	 * @deprecated
	 */
	public function setDataSet(FWDDBDataSet $poDataSet){
		$this->coDataSet = $poDataSet;
	}

	/**
	 * Seta o QueryHandler.
	 *
	 * <p>M�todo para setar o objeto QueryHandler.</p>
	 * @param FWDDBDataSet $poQueryHandler QueryHandler
	 * @access public
	 */
	public function setQueryHandler($poQueryHandler){
		$poQueryHandler->makeQuery();
		$this->coDataSet = $poQueryHandler->getDataset();
		$this->coDataSet->setQuery($poQueryHandler->getSQL());
	}

	/**
	 * Popula o FWDDBSelect.
	 *
	 * <p>M�todo para popular o FWDDBSelect com itens (linhas retornadas na query).</p>
	 * @access public
	 */
	public function populate(){
		$this->coDataSet->execute();
		while($this->coDataSet->fetch()){
			$moItem = new FWDItem();
			$msKey = $this->coDataSet->getFieldByAlias("select_id")->getValue();
			$msValue = $this->coDataSet->getFieldByAlias("select_value")->getValue();
			$moItem->setAttrKey($msKey);
			$moItem->setValue($msValue);
			if(in_array($msValue,$this->caValues)){
				$moItem->setAttrCheck('true');
			}
			$this->addObjFWDItem($moItem);
		}
	}
}
?>