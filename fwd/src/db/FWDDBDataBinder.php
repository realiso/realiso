<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDDBDataBinder.
 *
 * <p>Classe utilizada para associar uma View com um campo do banco de dados.</p>
 * @package FWD5
 * @subpackage db
 */
class FWDDBDataBinder {

	/**
	 * Array de Views (indexado pelo nome da View)
	 * @var FWDView
	 * @access protected
	 */
	protected $caViews = array();

	/**
	 * Entidade de banco de dados
	 * @var FWDEntity
	 * @access protected
	 */
	protected $coEntity = NULL;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDDBDataBinder.</p>
	 * @access public
	 * @param FWDDBEntity $poEntity Entidade
	 */
	public function __construct(FWDDBEntity $poEntity) {
		$this->coEntity = $poEntity;
	}

	/**
	 * Adiciona uma view.
	 *
	 * <p>M�todo para adicionar uma view.</p>
	 * @access public
	 * @param FWDView $poView View
	 * @param string $psAlias Alias do campo ao qual a View est� associada
	 */
	public function addView(FWDView $poView, $psAlias) {
		$this->caViews[$psAlias] = $poView;
	}

	/**
	 * Insere os valores das views no banco de dados.
	 *
	 * <p>M�todo para inserir os valores das views no banco de dados.</p>
	 * @access public
	 * @return integer Id do registro inserido ou falso
	 */
	public function insert() {
		$this->setEntityValues();
		return $this->coEntity->insert();
	}

	/**
	 * Atualiza os valores das views no banco de dados.
	 *
	 * <p>M�todo para atualizar os valores das views no banco de dados.</p>
	 * @access public
	 * @return integer N�mero de registros afetados pelo update ou falso em caso de falha
	 */
	public function update() {
		$this->setEntityValues();
		return $this->coEntity->update();
	}

	/**
	 * Deleta do banco baseado nos valores da view.
	 *
	 * <p>M�todo para deletar do banco baseado nos valores da view.</p>
	 * @access public
	 * @return integer N�mero de registros afetados pelo delete ou falso em caso de falha
	 */
	public function delete() {
		$this->setEntityValues();
		return $this->coEntity->delete();
	}

	/**
	 * Popula as views.
	 *
	 * <p>M�todo para popular as views.</p>
	 * @access public
	 */
	public function populate() {
		$moDataset = $this->coEntity->select();
		while ($moDataset->fetch()) {
			foreach($this->caViews as $msAlias => $moView)
			$moView->setValue($this->coEntity->getValue($msAlias));
		}
	}

	/**
	 * Seta os valores dos campos da entidade de acordo com os valores das views.
	 *
	 * <p>M�todo para setar os valores dos campos da entidade de acordo com os valores das views.</p>
	 * @access public
	 */
	function setEntityValues() {
		foreach($this->caViews as $msAlias => $moView) {
			$this->coEntity->setValue($msAlias, $moView->getValue());
		}
	}

	/**
	 * Seta um filtro para a entidade.
	 *
	 * <p>M�todo para setar um filtro para a entidade.</p>
	 * @access public
	 */
	public function setKey($psKeyAlias, $psValue) {
		if (!$this->coEntity->setKey($psKeyAlias, $psValue))
		trigger_error("Invalid entity field alias: $psKeyAlias", E_USER_WARNING);
	}
}
?>