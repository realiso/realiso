<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDDBDataSet.
 *
 * <p>Classe que representa uma abstra��o de uma tabela do banco de dados.</p>
 * @package FWD5
 * @subpackage db
 */
class FWDDBDataSet {

	/**
	 * Objeto de conex�o com o banco de dados
	 * @var FWDDB
	 * @access protected
	 */
	protected $coDatabase = null;

	/**
	 * Array de FWDDBField (indexado pelo alias do campo)
	 * @var array
	 * @access protected
	 */
	protected $caFields = array();

	/**
	 * Array com os campos que devem ordenar o resultado (ex: array['rm_area'] = '+')
	 * @var array
	 * @access protected
	 */
	protected $caOrderBy = array();

	/**
	 * Nome da tabela
	 * @var string
	 * @access protected
	 */
	protected $csTable = "";

	/**
	 * Query que vai ser executada
	 * @var string
	 * @access protected
	 */
	protected $csQuery = "";

	/**
	 * N�mero m�ximo de linhas que devem ser retornadas
	 * @var integer
	 * @access protected
	 */
	protected $ciLimit;

	/**
	 * N�mero da linha corrente durante o retorno de uma consulta
	 * @var integer
	 * @access protected
	 */
	protected $ciCurrentRow;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDDBDataSet.</p>
	 * @access public
	 * @param FWDDB $poDB Objeto de conex�o
	 * @param string $psTable Tabela (opcional)
	 */
	public function __construct(FWDDB $poDB, $psTable = "") {
		$this->coDatabase = $poDB;
		$this->csTable = $psTable;
		if (!$this->coDatabase->connect())
		trigger_error("Error while connecting to database", E_USER_ERROR);
	}

	/**
	 * Limpa o dataset.
	 *
	 * <p>M�todo para limpar o dataset.</p>
	 * @access public
	 */
	public function clean() {
		$this->coDatabase = NULL;
		$this->caFields = array();
		$this->caOrderBy = array();
		$this->csTable = "";
		$this->csQuery = "";
	}

	/**
	 * Seta o objeto objeto de conex�o com o banco.
	 *
	 * <p>M�todo para setar o objeto de conex�o com o banco.</p>
	 * @access public
	 * @param FWDDB $poDB Objeto de conex�o
	 */
	public function setDatabase(FWDDB $poDB) {
		$this->coDatabase = $poDB;
	}

	/**
	 * Retorna o objeto objeto de conex�o com o banco.
	 *
	 * <p>M�todo para retornar o objeto objeto de conex�o com o banco.</p>
	 * @access public
	 * @return FWDDB Objeto de conex�o
	 */
	public function getDatabase() {
		return $this->coDatabase;
	}

	/**
	 * Seta o nome da tabela.
	 *
	 * <p>M�todo para setar o nome da tabela.</p>
	 * @access public
	 * @param string $psTable Tabela
	 */
	public function setTable($psTable) {
		$this->csTable = $psTable;
	}

	/**
	 * Retorna o nome da tabela.
	 *
	 * <p>M�todo para retornar o nome da tabela.</p>
	 * @access public
	 * @return string Tabela
	 */
	public function getTable() {
		return $this->csTable;
	}

	/**
	 * Adiciona um campo na tabela.
	 *
	 * <p>M�todo para adicionar um campo na tabela.</p>
	 * @access public
	 * @param FWDDBField $poField Campo
	 */
	public function addFWDDBField(FWDDBField $poField) {
		$this->caFields[strtolower($poField->getAlias())] = $poField;
	}

	/**
	 * Retorna o objeto FWDDBField relativo ao alias passado.
	 *
	 * <p>M�todo para retornar o objeto FWDDBField relativo ao alias passado.</p>
	 * @access public
	 * @param string $psAlias Alias do campo
	 * @return FWDDBField Campo
	 */
	public function getFieldByAlias($psAlias) {
		$psAlias = strtolower($psAlias);
		if (isset($this->caFields[$psAlias])) return $this->caFields[$psAlias];
		else return NULL;
	}

	/**
	 * Retorna o n�mero total de campos.
	 *
	 * <p>M�todo para retornar o n�mero total de campos.</p>
	 * @access public
	 * @return integer N�mero total de campos
	 */
	public function getFieldCount() {
		if (count($this->caFields)) return count($this->caFields);
		else return $this->coDatabase->getFieldCount();
	}

	/**
	 * Retorna um array com os campos do DataSet.
	 *
	 * <p>Retorna um array com os campos (objetos FWDDBField) do DataSet.</p>
	 * @access public
	 * @return array Array de campos
	 */
	public function getFields(){
		return $this->caFields;
	}

	/**
	 * Executa um sql qualquer no banco.
	 *
	 * <p>M�todo para executar um sql qualquer no banco.</p>
	 * @access public
	 * @param string $piLimit N�mero de registros que deve ser retornado
	 * @param string $psOffset A partir de qual registro deve come�ar a retornar
	 * @return boolean Verdadeiro (sucesso) ou Falso (falha)
	 */
	public function execute($piLimit = 0, $piOffset = 0, $skipInjection = false) {
		if($this->csQuery){
			$msOrder = $this->getOrderBy();
			$this->ciCurrentRow = 0;
			$this->ciLimit = $piLimit;
			$miLimit = ($piLimit?$piLimit+1:0);
			return $this->coDatabase->execute($this->csQuery . $msOrder, $miLimit, $piOffset, $skipInjection);
		}else{
			return false;
		}
	}

	/**
	 * Retorna o n�mero total de linhas.
	 *
	 * <p>M�todo para retornar o n�mero total de linhas.</p>
	 * @access public
	 * @return integer N�mero total de linhas
	 */
	public function getRowCount(){
		$miRowCount = $this->coDatabase->getRowCount();
		if($this->ciLimit){
			return min($miRowCount,$this->ciLimit);
		}else{
			return $miRowCount;
		}
	}

	/**
	 * Seta a query que ser� executada.
	 *
	 * <p>M�todo para setar a query que ser� executada.</p>
	 * @access public
	 * @param string $psQuery Query que vai ser executada
	 */
	public function setQuery($psQuery) {
		$this->csQuery = $psQuery;
	}

	/**
	 * Retorna a query que ser� executada.
	 *
	 * <p>M�todo para retornar a query que ser� executada.</p>
	 * @access public
	 * @return string Query que vai ser executada
	 */
	public function getQuery() {
		return $this->csQuery;
	}

	/**
	 * Busca um registro do resultado e avan�a para o pr�ximo.
	 *
	 * <p>M�todo para buscar um registro do resultado e avan�ar para o pr�ximo.</p>
	 * @access public
	 * @return boolean Verdadeiro (sucesso) ou Falso (falha)
	 */
	public function fetch(){
		$maData = $this->coDatabase->fetch();
		if($maData && ($this->ciLimit==0 || $this->ciCurrentRow < $this->ciLimit)){
			$this->ciCurrentRow++;
			foreach($maData as $msAlias => $msData){
				$moField = $this->getFieldByAlias($msAlias);
				if($moField){
					$moField->setValue($msData);
				}else{
					trigger_error("QueryHandler without a dbfield with alias = '{$msAlias}'", E_USER_WARNING);
				}
			}
			return true;
		}else{
			return false;
		}
	}

	/**
	 * Ordena o resultado da consulta a partir do campo especificado.
	 *
	 * <p>M�todo para ordenar o resultado da consulta a partir do campo especificado.</p>
	 * @access public
	 * @param integer $psAlias Alias do campo
	 * @param integer $psOrder Ordem crescente ("+") ou decrescente ("-")
	 */
	public function setOrderBy($psAlias, $psOrder) {
		switch ($psOrder) {
			case "+":
				$this->caOrderBy[$psAlias] = "ASC";
				break;
			case "-":
				$this->caOrderBy[$psAlias] = "DESC";
				break;
			default:
				trigger_error("Invalid query order! got '{$psOrder}'" . " !", E_USER_WARNING);
				break;
		}
	}

	/**
	 * Reseta o campos que ordenam a consulta.
	 *
	 * <p>M�todo para resetar os campos que ordenam a consulta.</p>
	 * @access public
	 */
	public function resetOrderBy() {
		unset($this->caOrderBy);
	}

	/**
	 * Obtem o c�digo do orderby das colunas.
	 *
	 * <p>M�todo para obter o c�gigo order by das colunas da consulta.</p>
	 * @access private
	 */
	public function getOrderBy(){
		$miCountOrderBy = count($this->caOrderBy);
		$msOrder = " ";
		if ($miCountOrderBy) {
			$miI = 0;
			$msOrder .= "ORDER BY";
			foreach($this->caOrderBy as $msAlias => $msOp) {
				if ($this->getFieldByAlias($msAlias))
				$msOrder .= " ". $msAlias . " " . $msOp;
				else
				trigger_error("Trying to use order by in a column without alias or with an inexistent dbField!",E_USER_ERROR);
				if ($miI != $miCountOrderBy - 1) $msOrder .= ",";
				$miI++;
			}
		}
		return $msOrder . " ";
	}

	/**
	 * Executa um select no banco de dados.
	 *
	 * <p>M�todo para executar um select no banco de dados.</p>
	 * @access public
	 * @param integer $piLimit N�mero m�ximo de registros que deve retornar (opcional)
	 * @param integer $piOffset A partir de qual registro deve come�ar a retornar (opcional)
	 * @return integer N�mero de registros retornados
	 */
	public function select($piLimit = 0, $piOffset = 0){
		$this->ciCurrentRow = 0;
		$this->ciLimit = $piLimit;
		$miLimit = ($piLimit?$piLimit+1:0);

		$msSelect = "SELECT ";

		$miCount = count($this->caFields);
		$miI = 0;
		foreach ($this->caFields as $moField) {
			if ($moField->getAlias()) $msSelect .= $moField->getName() . " as " . $moField->getAlias();
			else $msSelect .= $moField->getName();
			if ($miI != $miCount - 1) $msSelect .= ", ";
			$miI++;
		}

		$msSelect .= " FROM " . $this->getTable();

		$msWhere = "";
		$mbFirstFilter = true;
		foreach ($this->caFields as $moField) {
			$msFilterSQL = $moField->getFiltersSQL();
			if ($mbFirstFilter && $msFilterSQL) {
				$msWhere .= $msFilterSQL ? $msFilterSQL : "";
				$mbFirstFilter = false;
			} else $msWhere .= $msFilterSQL ?  " AND " . $msFilterSQL : "";
		}

		if ($msWhere) $msSelect .= " WHERE " . $msWhere;

		$this->setQuery($msSelect);
		if ($this->execute($miLimit, $piOffset))
		return $this->coDatabase->getRowCount();
		else
		trigger_error("Invalid select statement: " . $msSelect, E_USER_ERROR);
	}

	/**
	 * Insere dados no banco de dados.
	 *
	 * <p>M�todo para inserir dados no banco de dados.</p>
	 * @access public
	 * @param string $psKey Nome da chave prim�ria
	 * @return integer Id do �ltimo registro inserido ou falso
	 */
	public function insert($psKey = '') {
		$maFieldsInfo = array();
		 
		//adicionado utf8_decode para fazer a convers�o de charset que vai ser gravado no banco
		foreach ($this->caFields as $moField) {
			if ($moField->isUpdatable()) {
				$msValue = '';
				if ($moField->mustDecode()) $msValue = html_entity_decode($moField->getValue(), ENT_QUOTES);
				else $msValue = $moField->getValue();
				$maFieldsInfo[] = array('name' => $moField->getName(), 'value' => $msValue, 'type' => $moField->getType());
			}
		}
	  
		if (count($maFieldsInfo)) {
			$mbRes = $this->coDatabase->autoInsert($this->getTable(), $maFieldsInfo);
			if ($mbRes)
			return $psKey ? $this->coDatabase->getLastId($this->getTable(), $psKey) : true;
			else
			return false;
		} else
		return false;
	}

	/**
	 * Atualiza dados no banco de dados.
	 *
	 * <p>M�todo para atualizar dados no banco de dados.</p>
	 * @access public
	 * @return integer N�mero de registros afetados pelo update ou falso em caso de falha
	 */
	public function update() {
		$maFieldsInfo = array();
		$msWhere = "";
		$mbFirstFilter = true;

		//adicionado utf8_decode para fazer a convers�o de charset que vai ser gravado no banco
		foreach ($this->caFields as $moField) {
			if($moField->isUpdatable()){
				$msValue = '';
				if ($moField->mustDecode()) $msValue = html_entity_decode($moField->getValue(), ENT_QUOTES);
				else $msValue = $moField->getValue();
				$maFieldsInfo[] = array('name' => $moField->getName(), 'value' => $msValue, 'type' => $moField->getType());
			}
			$msFilterSQL = $moField->getFiltersSQL();
			if ($mbFirstFilter && $msFilterSQL) {
				$msWhere .= $msFilterSQL ? $msFilterSQL : "";
				$mbFirstFilter = false;
			} else $msWhere .= $msFilterSQL ?  " AND " . $msFilterSQL : "";
		}
		if (count($maFieldsInfo)) {
			$mbRes = $this->coDatabase->autoUpdate($this->getTable(), $maFieldsInfo, $msWhere);
			if ($mbRes)
			return $this->coDatabase->getAffectedRows();
			else return false;
		} else
		return false;
	}

	/**
	 * Exclui dados do banco de dados.
	 *
	 * <p>M�todo para excluir dados do banco de dados.</p>
	 * @access public
	 * @return integer N�mero de registros afetados pelo update ou falso em caso de falha
	 */
	public function delete() {
		$msWhere = "";
		$mbFirstFilter = true;
		foreach ($this->caFields as $moField) {
			$msFilterSQL = $moField->getFiltersSQL();
			if ($mbFirstFilter && $msFilterSQL) {
				$msWhere .= $msFilterSQL ? $msFilterSQL : "";
				$mbFirstFilter = false;
			} else $msWhere .= $msFilterSQL ?  " AND " . $msFilterSQL : "";
		}
		$mbRes = $this->coDatabase->autoDelete($this->getTable(), $msWhere);
		if ($mbRes)
		return $this->coDatabase->getAffectedRows();
		else return false;
	}

	/**
	 * Indica se o resultado da consulta foi truncado.
	 *
	 * <p>Indica se o resultado da consulta foi truncado.</p>
	 * @access public
	 * @return boolean True, sse o resultado da consulta foi truncado
	 */
	public function isTruncated(){
		return ($this->ciLimit && $this->coDatabase->getRowCount() > $this->ciLimit);
	}

	/**
	 * Preenche um campo a partir de um objeto de interface.
	 *
	 * <p>Preenche um campo a partir de um objeto de interface.</p>
	 * @access public
	 * @param string $psFieldAlias Alias do campo
	 * @param string psObjectName Nome do objeto de interface
	 * @return boolean True sse o campo foi preenchido com sucesso
	 */
	public function fillFieldFromObject($psFieldAlias,$psObjectName){
		$moObject = FWDWebLib::getObject($psObjectName);
		if($moObject && isset($this->caFields[$psFieldAlias])){
			$moField = $this->caFields[$psFieldAlias];
			switch($moField->getType()){
				case DB_NUMBER:{
					if($moObject->getValue()!==''){
						$moField->setValue($moObject->getValue(), true, true);
						return true;
					}else{
						return false;
					}
				}
				case DB_STRING:{
					$moField->setValue($moObject->getValue(), true, true);
					return true;
				}
				case DB_DATETIME:{
					if(get_class($moObject)=='FWDCalendar'){
						$moField->setValue($moObject->getTimestamp(), true, true);
						return true;
					}
				}
			}
			trigger_error("Translation from '".get_class($moObject)."' object to '".$moField->getType()."' type not implemented.",E_USER_WARNING);
		}
		return false;
	}

	/**
	 * Preenche os campos a partir dos objetos de interface.
	 *
	 * <p>Preenche os campos a partir dos objetos de interface, quando houver
	 * objetos de interface com nomes iguais aos aliases dos campos.</p>
	 * @access public
	 * @return boolean True sse todos campos foram preenchidos com sucesso
	 */
	public function fillFieldsFromForm(){
		$mbReturn = true;
		foreach($this->caFields as $msAlias=>$moField){
			$mbReturn&= $this->fillFieldFromObject($msAlias,$msAlias);
		}
		return $mbReturn;
	}

	/**
	 * Preenche os campos a partir de um array associativo.
	 *
	 * <p>Preenche os campos a partir de um array associativo no formato alias=>valor.</p>
	 * @access public
	 * @return boolean True sse todos campos foram preenchidos com sucesso
	 */
	public function fillFieldsFromArray($paFields){
		$mbReturn = true;
		foreach($this->caFields as $msAlias=>$moField){
			if(isset($paFields[$msAlias])){
				if($moField->getType()==DB_DATETIME){
					$moField->setValue(FWDWebLib::dateToTimestamp($paFields[$msAlias]));
				}else{
					$moField->setValue($paFields[$msAlias]);
				}
			}else{
				$mbReturn = false;
			}
		}
		return $mbReturn;
	}

	/**
	 * Preenche um objeto de interface a partir de um campo do dataset.
	 *
	 * <p>Preenche um objeto de interface a partir de um campo do dataset.</p>
	 * @access public
	 * @param string psObjectName Nome do objeto de interface
	 * @param string $psFieldAlias Alias do campo
	 * @return boolean True sse o objeto foi preenchido com sucesso
	 */
	public function fillObjectFromField($psObjectName,$psFieldAlias){
		$moObject = FWDWebLib::getObject($psObjectName);
		if($moObject && isset($this->caFields[$psFieldAlias])){
			$moField = $this->caFields[$psFieldAlias];
			$moObject->setValue($moField->getValue());
			return true;
		}
		return false;
	}

	/**
	 * Preenche os objetos de interface a partir dos campos do dataset.
	 *
	 * <p>Preenche os objetos de interface a partir dos campos do dataset, quando
	 * houver objetos de interface com nomes iguais aos aliases dos campos.</p>
	 * @access public
	 * @return boolean True sse todos objetos foram preenchidos com sucesso
	 */
	public function fillFormFromFields(){
		$mbReturn = true;
		foreach($this->caFields as $msAlias=>$moField){
			$mbReturn&= $this->fillObjectFromField($msAlias,$msAlias);
		}
		return $mbReturn;
	}

	/**
	 * Retorna um array associativo com os valores dos campos.
	 *
	 * <p>Retorna um array associativo com os valores dos campos, no formato alias=>valor.</p>
	 * @access public
	 * @return array Array associativo com os valores dos campos
	 */
	public function getFieldValues(){
		$maReturn = array();
		foreach($this->caFields as $msAlias=>$moField){
			$maReturn[$msAlias] = $moField->getValue();
		}
		return $maReturn;
	}

}
?>