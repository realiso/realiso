<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDDBFilter.
 *
 * <p>Armazena os filtros que ser�o aplicados no campo.</p>
 * @package FWD5
 * @subpackage db
 */
class FWDDBType {

	/**
	 * Converte o tipo de acordo com o banco de dados.
	 *
	 * <p>M�todo para converter o tipo de acordo com o banco de dados.</p>
	 * @access public
	 * @param string $psValue Valor
	 * @param integer $piType Tipo
	 * @return string SQL para ser inserido no banco
	 */
	static public function translate($psValue, $piType) {
		$moConn = FWDWebLib::getConnection();
		$msValue = "";

		switch ($piType) {
			case DB_UNKNOWN:
				trigger_error("Undefined field type", E_USER_WARNING);
				break;
					
			case DB_NUMBER:
				if (($moConn->getDatabaseType() == DB_ORACLE) && ($psValue == 'null')) return "''";
				else if ($moConn->getDatabaseType() == DB_POSTGRES 
					&& strtoupper($psValue) == 'NULL') return "null";
				else if ($psValue) $msValue = 0 + $psValue;
				else $msValue = 0;
				break;
					
			case DB_STRING:
				$msValue = $moConn->quote($psValue);
				break;
					
			case DB_DATETIME:
				if($psValue!='null')
				$msValue = $moConn->timestampFormat($psValue);
				else if ($moConn->getDatabaseType() != DB_ORACLE)
				return 'null';
				else
				return "''";
				break;
		}
		return $msValue;
	}
}
?>