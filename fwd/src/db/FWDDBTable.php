<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe AMSTable. Manipula informa��es b�sicas de uma tabela qualquer.
 *
 * <p>Classe abstrata que manipula informa��es b�sicas de uma tabela qualquer.</p>
 * @package FWD5
 * @subpackage db
 */
abstract class FWDDBTable {

	/**
	 * Alias da chave prim�ria da tabela
	 * @var string
	 * @access protected
	 */
	protected $csAliasId = "";

	/**
	 * Objeto para conex�o com o banco de dados
	 * @var FWDDBDataset
	 * @access protected
	 */
	protected $coDataset = null;

	/**
	 * Array com os filtros da tabela
	 * @var array
	 * @access private
	 */
	private $caFilters = array();

	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDDBTable.</p>
	 * @access public
	 * @param string $psTable Nome da tabela
	 */
	public function __construct($psTable){
		$this->coDataset = new FWDDBDataset(FWDWebLib::getConnection(), $psTable);
	}

	/**
	 * Retorna o nome da tabela.
	 *
	 * <p>M�todo para retornar o nome da tabela.</p>
	 * @access public
	 * @return string Nome da tabela
	 */
	public function getTable() {
		return $this->coDataset->getTable();
	}

	/**
	 * Retorna o nome de um campo.
	 *
	 * <p>M�todo para retornar o nome de um campo.</p>
	 * @access public
	 * @param string $psFieldAlias Alias do campo
	 * @return string Nome do campo
	 */
	public function getFieldName($psFieldAlias) {
		return $this->coDataset->getFieldByAlias($psFieldAlias)->getName();
	}

	/**
	 * Seta o valor de uma campo da tabela.
	 *
	 * <p>M�todo para setar o valor de um campo da tabela.</p>
	 * @access public
	 * @param string $psFieldAlias Alias do campo
	 * @param string $psValue Valor do campo
	 * @param string $pbDecode Decodificar o campo antes de inserir no banco?
	 */
	public function setFieldValue($psFieldAlias, $psValue, $pbDecode = true) {
		$moField = $this->coDataset->getFieldByAlias($psFieldAlias);
		if($moField) $moField->setValue($psValue, true, $pbDecode);
		else trigger_error("There is no field with the alias \"{$psFieldAlias}\"", E_USER_WARNING);
	}

	/**
	 * Retorna o valor de uma campo da tabela.
	 *
	 * <p>M�todo para retornar o valor de um campo da tabela.</p>
	 * @access public
	 * @return string Valor do campo
	 */
	public function getFieldValue($psFieldAlias) {
		$moField = $this->coDataset->getFieldByAlias($psFieldAlias);
		if($moField) return $moField->getValue();
		else trigger_error("There is no field with the alias \"{$psFieldAlias}\"", E_USER_WARNING);
	}

	/**
	 * Retorna o objeto FWDDBField relativo ao alias passado.
	 *
	 * <p>M�todo para retornar o objeto FWDDBField relativo ao alias passado.</p>
	 * @access public
	 * @param string $psAlias Alias do campo
	 * @return FWDDBField Campo
	 */
	public function getFieldByAlias($psAlias) {
		return $this->coDataset->getFieldByAlias($psAlias);
	}

	/**
	 * Busca um registro atrav�s da chave prim�ria da tabela.
	 *
	 * <p>M�todo para buscar um registro atrav�s da chave prim�ria da tabela.</p>
	 * @access protected
	 * @param string $psId Identificador
	 * @param string $psFieldAlias Alias do campo
	 */
	protected function fetchById($psId, $psFieldAlias) {
		if(($psId=='')||($psId==0)||($psId===false)||($psId==null))
		return false;
		else{
			$this->createFilter($psId, $psFieldAlias);
			$this->coDataset->select();
			return $this->fetch();
		}
	}

	/**
	 * Remove todos os filtros do campo.
	 *
	 * <p>Remove todos os filtros do campo.</p>
	 * @access public
	 * @param string $psAlias Alias do campo
	 */
	public function removeFilters($psAlias){
		unset($this->caFilters[$psAlias]);
		$this->coDataset->getFieldByAlias($psAlias)->clearFilters();
	}

	/**
	 * Remove todos os filtros de todos os campos.
	 *
	 * <p>Remove todos os filtros de todos os campo.</p>
	 * @access public
	 */
	public function removeAllFilters(){
		$maFields = $this->coDataset->getFields();
		unset($this->caFilters);
		foreach($maFields as $moField) $moField->clearFilters();
	}

	/**
	 * Cria um filtro.
	 *
	 * <p>M�todo para criar um filtro.</p>
	 * @access public
	 * @param string $psValue Valor
	 * @param string $psFieldAlias Alias do campo
	 * @param string $psOperator Operador
	 */
	public function createFilter($psValue, $psFieldAlias, $psOperator = "=") {
		if (isset($this->caFilters[$psFieldAlias])) {
			$this->caFilters[$psFieldAlias]->addValue($psValue);
		}
		else {
			$moFilter = new FWDDBFilter($psOperator, $psValue);
			$moField = $this->coDataset->getFieldByAlias($psFieldAlias);
			if($moField){
				$moField->addFilter($moFilter);
			}else{
				trigger_error("Unknown field alias '$psFieldAlias'.",E_USER_ERROR);
			}
			$this->caFilters[$psFieldAlias] = $moFilter;
		}
	}

	/**
	 * Busca um registro do resultado.
	 *
	 * <p>M�todo para buscar um registro do resultado.</p>
	 * @access public
	 * @return boolean Verdadeiro ou falso
	 */
	public function fetch() {
		return $this->coDataset->fetch();
	}

	/**
	 * Busca informa��es de uma tabela.
	 *
	 * <p>M�todo para buscar informa��es de uma tabela.</p>
	 * @access public
	 * @return integer N�mero de registros retornados
	 */
	public function select() {
		return $this->coDataset->select();
	}

	/**
	 * Atualiza a tabela.
	 *
	 * <p>M�todo para atualizar a tabela.</p>
	 * @access public
	 * @return integer N�mero de registros afetados pelo update ou falso em caso de falha
	 */
	public function update() {
		return $this->coDataset->update();
	}

	/**
	 * Remove um registro da tabela.
	 *
	 * <p>M�todo para remover um registro da tabela.</p>
	 * @access public
	 */
	public function delete() {
		return $this->coDataset->delete();
	}

	/**
	 * Insere um registro aa tabela.
	 *
	 * <p>M�todo para inserir um registro na tabela.</p>
	 * @param boolean $pbGetId Verdadeiro para retornar o id do registro inserido
	 * @return integer Id do �ltimo registro inserido ou falso
	 * @access public
	 */
	public function insert($pbGetId = false) {
		if ($pbGetId) {
			if (FWDWebLib::getConnection()->getDatabaseType() != DB_POSTGRES) {
				return $this->coDataset->insert(true);
			}
			else {
				/*
				 * No caso do POSTGRES � necess�rio que se especifique a chave prim�ria
				 * da tabela para que seja poss�vel retornar o id do �ltimo registro inserido.
				 */
				if ($this->csAliasId) {
					return $this->coDataset->insert($this->coDataset->getFieldByAlias($this->csAliasId)->getName());
				}
				else {
					trigger_error('Error while getting the last inserted id: $this->csAliasId not set (You must specify the primary to get the last inserted id when using POSTGRES)!', E_USER_WARNING);
					return false;
				}
			}
		}
		else return $this->coDataset->insert();
	}

	/**
	 * Executa um sql qualquer no banco.
	 *
	 * <p>M�todo para executar um sql qualquer no banco.</p>
	 * @access public
	 * @param string $psQuery SQL Query
	 * @param string $piLimit N�mero de registros que deve ser retornado
	 * @param string $psOffset A partir de qual registro deve come�ar a retornar
	 * @return boolean Verdadeiro (sucesso) ou Falso (falha)
	 */
	public function execute($psQuery = "", $piLimit = 0, $piOffset = 0) {
		$this->coDataset->setQuery($psQuery);
		return $this->coDataset->execute($piLimit, $piOffset);
	}

	/**
	 * Ordena o resultado da consulta a partir do campo especificado.
	 *
	 * <p>M�todo para ordenar o resultado da consulta a partir do campo especificado.</p>
	 * @access public
	 * @param integer $psAlias Alias do campo
	 * @param integer $psOrder Ordem crescente ("+") ou decrescente ("-")
	 */
	public function setOrderBy($psAlias, $psOrder){
		$this->coDataset->setOrderBy($psAlias,$psOrder);
	}

	/**
	 * Preenche um campo a partir de um objeto de interface.
	 *
	 * <p>Preenche um campo a partir de um objeto de interface.</p>
	 * @access public
	 * @param string $psFieldAlias Alias do campo
	 * @param string psObjectName Nome do objeto de interface
	 * @return boolean True sse o campo foi preenchido com sucesso
	 */
	public function fillFieldFromObject($psFieldAlias,$psObjectName){
		return $this->coDataset->fillFieldFromObject($psFieldAlias,$psObjectName);
	}

	/**
	 * Preenche os campos a partir dos objetos de interface.
	 *
	 * <p>Preenche os campos a partir dos objetos de interface, quando houver
	 * objetos de interface com nomes iguais aos aliases dos campos.</p>
	 * @access public
	 * @return boolean True sse todos campos foram preenchidos com sucesso
	 */
	public function fillFieldsFromForm(){
		$mbReturn = true;
		foreach($this->coDataset->getFields() as $msAlias=>$moField){
			$mbReturn&= $this->fillFieldFromObject($msAlias,$msAlias);
		}
		return $mbReturn;
	}

	/**
	 * Preenche os campos a partir de um array associativo.
	 *
	 * <p>Preenche os campos a partir de um array associativo no formato alias=>valor.</p>
	 * @access public
	 * @return boolean True sse todos campos foram preenchidos com sucesso
	 */
	public function fillFieldsFromArray($paFields){
		return $this->coDataset->fillFieldsFromArray($paFields);
	}

	/**
	 * Preenche um objeto de interface a partir de um campo da tabela.
	 *
	 * <p>Preenche um objeto de interface a partir de um campo da tabela.</p>
	 * @access public
	 * @param string psObjectName Nome do objeto de interface
	 * @param string $psFieldAlias Alias do campo
	 * @return boolean True sse o objeto foi preenchido com sucesso
	 */
	public function fillObjectFromField($psObjectName,$psFieldAlias){
		return $this->coDataset->fillObjectFromField($psObjectName,$psFieldAlias);
	}

	/**
	 * Preenche os objetos de interface a partir dos campos da tabela.
	 *
	 * <p>Preenche os objetos de interface a partir dos campos da tabela, quando
	 * houver objetos de interface com nomes iguais aos aliases dos campos.</p>
	 * @access public
	 * @return boolean True sse todos objetos foram preenchidos com sucesso
	 */
	public function fillFormFromFields(){
		$mbReturn = true;
		foreach($this->coDataset->getFields() as $msAlias=>$moField){
			$mbReturn&= $this->fillObjectFromField($msAlias,$msAlias);
		}
		return $mbReturn;
	}

	/**
	 * Atualiza a rela��o entre o registro e registros de outra tabela.
	 *
	 * <p>Atualiza a rela��o entre o registro e registros de outra tabela.</p>
	 * @access public
	 * @param FWDDBTable $poRelationTable Inst�ncia de registro da tabela de rela��o
	 * @param string $psForeignKey Alias da chave da outra tabela
	 * @param array $paIds Ids dos registros da outra tabela a serem relacionados
	 * @param boolean $pbDelete Sse for true, deleta rela��es com registros n�o inclu�dos em $paIds
	 */
	public function updateRelation(FWDDBTable $poRelationTable,$psForeignKey,$paIds,$pbDelete=true){
		$msAliasId = $this->csAliasId;

		if(!$this->getFieldByAlias($msAliasId)){
			trigger_error("You must specify the alias of the primary key.",E_USER_ERROR);
			return;
		}elseif(!$poRelationTable->getFieldByAlias($msAliasId) || !$poRelationTable->getFieldByAlias($psForeignKey)){
			trigger_error("The aliases and the relation table don't match.",E_USER_ERROR);
			return;
		}elseif(!$this->getFieldValue($msAliasId)){
			trigger_error("You must define the id.",E_USER_ERROR);
			return;
		}

		$miId = $this->getFieldValue($msAliasId);
		$msRelationClass = get_class($poRelationTable);

		$moRelation = new $msRelationClass;
		$moRelation->createFilter($miId,$msAliasId);
		$moRelation->select();
		$maCurrentIds = array();
		while($moRelation->fetch()){
			$maCurrentIds[] = $moRelation->getFieldValue($psForeignKey);
		}

		$maToInsert = array_diff($paIds,$maCurrentIds);
		$moRelation = new $msRelationClass;
		$moRelation->setFieldValue($msAliasId,$miId);
		foreach($maToInsert as $miForeignKey){
			$moRelation->setFieldValue($psForeignKey,$miForeignKey);
			$moRelation->insert();
		}

		if($pbDelete){
			$maToDelete = array_diff($maCurrentIds,$paIds);
			if(count($maToDelete)>0){
				$moRelation = new $msRelationClass;
				$moRelation->createFilter($miId,$msAliasId);
				$moRelation->getFieldByAlias($psForeignKey)->addFilter(new FWDDBFilter('in',$maToDelete));
				$msRelationAliasId = $moRelation->csAliasId;
				if($msRelationAliasId){
					$moRelation->select();
					$moDeleteRelation = new $msRelationClass;
					while($moRelation->fetch()){
						$moDeleteRelation->delete($moRelation->getFieldValue($msRelationAliasId),true);
					}
				}else{
					$moRelation->delete();
				}
			}
		}
	}

	/**
	 * Retorna um array associativo com os valores dos campos.
	 *
	 * <p>Retorna um array associativo com os valores dos campos, no formato alias=>valor.</p>
	 * @access public
	 * @return array Array associativo com os valores dos campos
	 */
	public function getFieldValues(){
		return $this->coDataset->getFieldValues();
	}
	
	public function getQuery() {
		return $this->coDataset->getQuery();
	}

}

?>