<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe para acesso ao banco de dados.
 *
 * <p>Classe para acesso ao banco de dados. Utiliza a biblioteca ADODB.</p>
 * @package FWD5
 * @subpackage db
 */
class FWDDB implements FWDIDB {

	/**
	 * Nome do host
	 * @var string
	 * @access protected
	 */
	protected $csHost = "";

	/**
	 * Nome do usu�rio
	 * @var string
	 * @access protected
	 */
	protected $csUser = "";

	/**
	 * Senha do usu�rio
	 * @var string
	 * @access protected
	 */
	protected $csPassword = "";

	/**
	 * Nome da base de dados
	 * @var string
	 * @access protected
	 */
	protected $csDatabase = "";

	/**
	 * Tipo do banco de dados
	 * @var string
	 * @access protected
	 */
	protected $csType = DB_NONE;

	/**
	 * Objeto de conex�o do banco
	 * @var object
	 * @access private
	 */
	private $coConnection = null;

	/**
	 * Objeto que representa o conjunto de registros retornados em um SELECT
	 * @var object
	 * @access private
	 */
	private $coRecordSet = null;

	/**
	 * Contrutor.
	 *
	 * <p>Construtor da classe FWDDB.</p>
	 * @access public
	 * @param string $psType Identificador do BD {DB_NONE | DB_POSTGRES | DB_ORACLE | DB_MSSQL}
	 * @param string $psDatabase Nome da base de dados
	 * @param string $psUser Nome do usu�rio
	 * @param string $psPassword Senha do usu�rio
	 * @param string $psHost Nome do host
	 */
	public function __construct($psType, $psDatabase, $psUser, $psPassword, $psHost) {
		$this->csType 		= $psType;
		$this->csDatabase 	= $psDatabase;
		$this->csUser 	 	= $psUser;
		$this->csPassword 	= $psPassword;
		$this->csHost	 	= $psHost;
		$this->coConnection = ADONewConnection($this->csType);
	}

	/**
	 * Retorna o nome do usu�rio do banco.
	 *
	 * <p>M�todo para retornar o nome do usu�rio do banco.</p>
	 * @access public
	 * @return string Login do usu�rio do banco
	 */
	public function getUser() {
		return $this->csUser;
	}

	/**
	 * Clona o objeto de conex�o com o banco de dados.
	 *
	 * <p>M�todo para clonar o objeto de conex�o com o banco de dados.</p>
	 * @access public
	 * @return FWDDB Objeto de conex�o clonado
	 */
	public function __clone() {
		$moDB = new FWDDB($this->csType, $this->csDatabase, $this->csUser, $this->csPassword, $this->csHost);
		return $moDB;
	}

	/**
	 * Conecta no banco.
	 *
	 * <p>M�todo para conectar no banco.</p>
	 * @access public
	 * @return boolean Verdadeiro ou Falso
	 */
	public function connect() {
		return $this->coConnection->Connect($this->csHost,$this->csUser,$this->csPassword,$this->csDatabase);
	}

	/**
	 * Verifica se est� conectado ao banco.
	 *
	 * <p>M�todo para verificar se est� conectado no banco.</p>
	 * @access public
	 * @return boolean Verdadeiro ou Falso
	 */
	public function isConnected() {
		return $this->coConnection->IsConnected();
	}

	/**
	 * Fecha conex�o com o banco de dados.
	 *
	 * <p>M�todo para fechar conex�o com o banco de dados.</p>
	 * @access public
	 */
	public function disconnect() {
		$this->coConnection->Close();
	}

	/**
	 * Executa um sql no banco.
	 *
	 * <p>M�todo para executar um sql no banco.</p>
	 * @access public
	 * @param string $psQuery Query que vai ser executada
	 * @param string $piLimit N�mero de registros que deve ser retornado
	 * @param string $psOffset A partir de qual registro deve come�ar a retornar
	 * @param boolean $skipInjection Se for true pula a verifica��o de injection
	 * @return boolean Verdadeiro (sucesso) ou Falso (falha)
	 */
	public function execute($psQuery, $piLimit = 0, $piOffset = 0, $skipInjection = false) {
		if (FWDWebLib::getInstance()->getDebugType() & FWD_DEBUG_DB) {
			$this->debugMode(true);
		}
		unset($this->coRecordSet);
		if (!$skipInjection && !$this->checkInjection($psQuery)) {
			trigger_error("injection technique" . "\nQuery: " . $psQuery, E_USER_ERROR);
			return false;
		}
		if($piLimit) $this->coRecordSet = $this->coConnection->SelectLimit($psQuery, $piLimit, $piOffset);
		else $this->coRecordSet = $this->coConnection->Execute($psQuery);
		if(!$this->coRecordSet) trigger_error($this->coConnection->ErrorMsg() . "\nQuery: " . $psQuery, E_USER_ERROR);
		else return true;
		return false;
	}
	 
	/**
	 * <p>M�todo para garantir que n�o h� SQL Injection.</p>
	 * @access public
	 * @param $psQuery
	 * @return boolean Verdadeiro (tudo ok) ou Falso (h� injection)
	 */
	 
	public function checkInjection($psQuery){
		$psQuery = utf8_decode($psQuery);
		$msString = $this->stripStrings($psQuery);
		if(preg_match("/((;\s*\S+)|(')|(-{2,}))/i", $msString)){
			$moMatchArray1 = Array();
			$moMatchArray1 = preg_match_all("/(;\s*\S+|'|--)/s", $msString, $bla, PREG_OFFSET_CAPTURE);
			return false;
		}
		return true;
	}
	 
	/**
	 * <p>M�todo que retira strings de dentro de uma query string</p>
	 * @param $psString
	 * @return string sem as strings internas
	 */
	public function stripStrings($psString){
		$prestr1 = preg_replace("/\\\\/s", '', $psString);
		$str1 = preg_replace("/\\\'/s", '', $prestr1);
		return preg_replace("/\'.*\'/sU", '', $str1);
	}

	/**
	 * Retorna o n�mero de campos retornados por uma consulta.
	 *
	 * <p>M�todo para retornar o n�mero de campos retornados por uma consulta.</p>
	 * @access public
	 * @return integer N�mero de campos
	 */
	public function getFieldCount() {
		if ($this->coRecordSet) return $this->coRecordSet->FieldCount();
		else trigger_error("RecordSet not initialized yet", E_USER_WARNING);
	}

	/**
	 * Gera SQL para INSERT automaticamente.
	 *
	 * <p>M�todo para gerar o SQL para INSERT automaticamente.</p>
	 * @access public
	 * @param string $psTable Nome da tabela
	 * @param array $paFieldsInfo Array com as informa��es dos campos
	 * @return boolean Verdadeiro ou Falso
	 */
	public function autoInsert($psTable, $paFieldsInfo) {
		$msInsert = '';
		$msFieldNames = "";
		$msFieldValues = "";
		$miI = 0;
		$miCount = count($paFieldsInfo);
		if ($this->csType == DB_ORACLE) {
			$msInsert = "BEGIN EXECUTE IMMEDIATE 'INSERT INTO " . $psTable . " (";
			$maValues = array();
			foreach ($paFieldsInfo as $maFieldInfo) {
				$msFieldNames .= ($miI != ($miCount-1)) ? $maFieldInfo['name'] . ", " : $maFieldInfo['name'];
				$maValues[] = FWDDBType::translate($maFieldInfo['value'], $maFieldInfo['type']);
				$msFieldValues .= ($miI != ($miCount-1)) ? ':'.$miI . ", " : ':'.$miI;
				$miI++;
			}
			$msInsert .= $msFieldNames . ") VALUES (" . $msFieldValues . ")' USING " . implode(',', $maValues) . "; END;";
		}
		else {
			$msInsert = "INSERT INTO " . $psTable . " (";
			foreach ($paFieldsInfo as $maFieldInfo) {
				$msFieldNames .= ($miI != ($miCount-1)) ? $maFieldInfo['name'] . ", " : $maFieldInfo['name'];
				$msFieldValue = FWDDBType::translate($maFieldInfo['value'], $maFieldInfo['type']);
				$msFieldValues .= ($miI != ($miCount-1)) ? $msFieldValue . ", " : $msFieldValue;
				$miI++;
			}
			$msInsert .= $msFieldNames . ") VALUES (" . $msFieldValues . ")";
		}
		return $this->execute($msInsert);
	}

	/**
	 * Gera SQL para UPDATE automaticamente.
	 *
	 * <p>M�todo para gerar o SQL para UPDATE automaticamente.</p>
	 * @access public
	 * @param string $psTable Nome da tabela
	 * @param array $paFieldsInfo Array com as informa��es dos campos
	 * @param string $psWhere Cl�usula "where" para restringir o UPDATE
	 * @return boolean Verdadeiro ou Falso
	 */
	public function autoUpdate($psTable, $paFieldsInfo, $psWhere) {
		$msUpdate = '';
		$miI = 0;
		$miCount = count($paFieldsInfo);
		$msNewValues = "";
		if ($this->csType == DB_ORACLE) {
			$msUpdate = "BEGIN EXECUTE IMMEDIATE 'UPDATE " . $psTable . " SET ";
			$maValues = array();
			foreach ($paFieldsInfo as $maFieldInfo) {
				$maValues[] = FWDDBType::translate($maFieldInfo['value'], $maFieldInfo['type']);
				$msNewValues .= $maFieldInfo['name'] . " = " . ':'.$miI;
				$msNewValues .= ($miI != ($miCount-1)) ? ", " : "";
				$miI++;
			}
			$msUpdate .= $msNewValues . " WHERE " . str_replace("'", "''", $psWhere) . "' USING " . implode(',', $maValues) . "; END;";
		}
		else {
			$msUpdate = "UPDATE " . $psTable . " SET ";
			foreach ($paFieldsInfo as $maFieldInfo) {
				$msNewValues .= $maFieldInfo['name'] . " = " . FWDDBType::translate($maFieldInfo['value'], $maFieldInfo['type']);
				$msNewValues .= ($miI != ($miCount-1)) ? ", " : "";
				$miI++;
			}
			$msUpdate .= $msNewValues . " WHERE " . $psWhere;
		}
		return $this->execute($msUpdate);
	}

	/**
	 * Gera DELETE automaticamente.
	 *
	 * <p>M�todo para gerar o SQL DELETE automaticamente.</p>
	 * @access public
	 * @param string $psTable Nome da tabela
	 * @param string $psWhere Cl�usula "where" para restringir o UPDATE
	 * @return boolean Verdadeiro ou Falso
	 */
	public function autoDelete($psTable, $psWhere) {
		if (!$psWhere)
		trigger_error("Invalid parameter: autoDelete method must have a \"where\" clause (for security reasons)", E_USER_ERROR);
		$msDelete = "DELETE FROM " . $psTable . " WHERE " . $psWhere;
		return $this->execute($msDelete);
	}

	/**
	 * Atualiza um CLOB no bnco de dados.
	 *
	 * <p>M�todo para atualizar um clob no banco de dados.</p>
	 * @access public
	 * @param string $psTable Nome da tabela
	 * @param string $psCollumn Nome da coluna
	 * @param string $psValue Valor do clob
	 * @param string $psWhere Cl�usula where
	 */
	public function updateClob($psTable, $psCollumn, $psValue, $psWhere) {
		if ($this->csType == DB_ORACLE) $this->coConnection->UpdateClob($psTable, $psCollumn, $psValue, $psWhere);
		else trigger_error('The method updateClob() is only for oracle!', E_USER_WARNING);
	}

	/**
	 * Seta o modo de debug.
	 *
	 * <p>M�todo para setar o m�todo de debug.</p>
	 * @access public
	 * @param boolean $pbDebug Verdadeiro ou Falso
	 */
	public function debugMode($pbDebug) {
		$this->coConnection->debug = $pbDebug;
		$this->coConnection->setDebugFilePath(FWDWebLib::getInstance()->getSysRef().FWDWebLib::getDebugFilePath());
	}

	/**
	 * Retorna a �ltima mensagem de erro.
	 *
	 * <p>M�todo para retornar a �ltima mensagem de erro.</p>
	 * @access public
	 * @return string Mensagem de erro
	 */
	public function getErrorMessage() {
		return $this->coConnection->ErrorMsg();
	}

	/**
	 * Libera da mem�ria o resultado da consulta.
	 *
	 * <p>M�todo para liberar da mem�ria o resultado da consulta.</p>
	 * @access public
	 */
	public function freeResult() {
		$this->coRecordSet->Close();
	}

	/**
	 * Retorna o n�mero de registros retornado em um SELECT.
	 *
	 * <p>M�todo para retornar o n�mero de registros retornado em um SELECT.</p>
	 * @access public
	 * @return integer N�mero de registros
	 */
	public function getRowCount() {
		return $this->coRecordSet->RowCount();
	}

	/**
	 * Retorna o n�mero de registros afetados por DELETE/UPDATE.
	 *
	 * <p>M�todo para retornar o n�mero de registros afetados por DELETE/UPDATE.</p>
	 * @access public
	 * @return integer N�mero de registros
	 */
	public function getAffectedRows() {
		return $this->coConnection->Affected_Rows();
	}

	/**
	 * Retorna o id do �ltimo registro inserido.
	 *
	 * <p>M�todo para retornar o id do �ltimo registro inserido.</p>
	 * @access public
	 * @param string $psTable Nome da tabela
	 * @param string $psKey Nome da coluna chave prim�ria
	 * @return integer Id do �ltimo registro inserido
	 */
	public function getLastId($psTable = "", $psKey = "") {
		if ($this->csType != DB_ORACLE) return $this->coConnection->Insert_ID($psTable, $psKey);
		else {
			$this->execute("SELECT S_" . $psTable . ".CURRVAL as pk FROM dual");
			$maPK = $this->fetch();
			return $maPK["PK"];
		}
	}

	/**
	 * Retorna um registro do resultado.
	 *
	 * <p>M�todo para retornar um registro do resultado.</p>
	 * @access public
	 * @return array Array contendo a informa��o ou falso caso atinja o fim
	 */
	public function fetch() {
		if (!$this->coRecordSet->EOF) {
			$maData = $this->coRecordSet->GetRowAssoc(2);
			$this->coRecordSet->MoveNext();
			return $maData;
		} else return false;
	}

	/**
	 * Retorna um array com todos os registros retornados pelo consulta.
	 *
	 * <p>M�todo para retornar um array com todos os registros retornados pelo consulta.</p>
	 * @access public
	 * @return array Array contendo todos os registros
	 */
	public function fetchAll() {
		return $this->coRecordSet->GetAll();
	}

	/**
	 * Inicia uma transa��o.
	 *
	 * <p>M�todo para iniciar uma transa��o.</p>
	 * @access public
	 */
	public function startTransaction() {
		$this->coConnection->StartTrans();
	}

	/**
	 * Completa uma transa��o.
	 *
	 * <p>M�todo para completar uma transa��o.</p>
	 * @access public
	 * @return boolean Verdadeiro se obtiver sucesso, falso caso contr�rio
	 */
	public function completeTransaction() {
		$this->coConnection->CompleteTrans();
	}

	/**
	 * For�a a falha de uma transa��o.
	 *
	 * <p>M�todo para for�ar a falha em um transa��o.</p>
	 * @access public
	 */
	public function rollbackTransaction() {
		$this->coConnection->FailTrans();
	}

	/**
	 * Retorna a data no formato do banco.
	 *
	 * <p>M�todo para retornar a data no formato aceito pelo banco.</p>
	 * @access public
	 * @param integer $piTimestamp Timestamp
	 * @return string Data
	 */
	public function timestampFormat($piTimestamp) {
		return $this->coConnection->DBTimeStamp($piTimestamp);
	}

	/**
	 * Retorna um timestamp baseado em uma data no formato YYYY-MM-DD HH-NN-SS.
	 *
	 * <p>M�todo para retornar um timestamp baseado em uma data no formato YYYY-MM-DD HH-NN-SS.</p>
	 * @access public
	 * @param integer $psDate Data
	 * @return integer Timestamp
	 */
	public function getTimestamp($psDate) {
		return $this->coConnection->UnixTimestamp($psDate);
	}

	/**
	 * Retorna o tipo do banco de dados
	 *
	 * <p>Retorna o tipo do banco de dados.</p>
	 * @access public
	 * @return string Tipo do banco de dados
	 */
	public function getDatabaseType(){
		return $this->csType;
	}

	/**
	 * Retorna o nome da base de dados.
	 *
	 * <p>M�todo para retornar o nome da base de dados.</p>
	 * @access public
	 * @return string Nome da base de dados
	 */
	public function getDatabaseName() {
		return $this->csDatabase;
	}

	/**
	 * Retorna o host da base de dados.
	 *
	 * <p>M�todo para retornar o host da base de dados.</p>
	 * @access public
	 * @return string Host da base de dados
	 */
	public function getHost() {
		return $this->csHost;
	}

	/**
	 * Retorna o Usu�rio que deve ser utilizado como prefixo das functions do banco
	 *
	 * <p>Usu�rio que deve ser utilizado como prefixo das functions do banco</p>
	 * @access public
	 * @return string Usu�rio da conex�o com o banco que deve ser utilizado antes do nome das functions
	 */
	public function getFunctionUserDB(){
		$msUserDB = "";
		switch($this->getDatabaseType()){
			case DB_POSTGRES:
			case DB_ORACLE:
				$msUserDB = '';
				break;
			default:
				$msUserDB = $this->getUser() . ".";
				break;
		}
		return $msUserDB;
	}

	/**
	 * Retorna a chamada da fun��o de acordo com o banco de dados.
	 *
	 * <p>M�todo para retornar a chamada da fun��o de acordo com o banco de dados.</p>
	 * @access public
	 * @param string $psFunction Fun��o que deve ser chamada
	 * @return string Chamada da fun��o
	 */
	public function getFunctionCall($psFunction){
		$msFunctionCall = "";
		switch($this->getDatabaseType()){
			case DB_ORACLE:
				$msFunctionCall = 'TABLE(' . $this->getFunctionUserDB() . $psFunction . ')';
				break;
			default:
				$msFunctionCall = $this->getFunctionUserDB() . $psFunction;
				break;
		}
		return $msFunctionCall;
	}

	/**
	 * Retorna um valor constante do banco de dados.
	 *
	 * <p>M�todo para retornar um valor constante do banco de dados. Necess�rio
	 * pois no Oracle n�o s�o aceitas consultas no formato 'select 1' e sim
	 * no formato 'select 1 from dual'.</p>
	 * @access public
	 * @param string $psValue Valores [Ex: '1 as first, 2 as second, ...]
	 * @return string Select para retornar uma constante
	 */
	public function selectConstant($psValues) {
		$msSelect = '';
		switch($this->getDatabaseType()){
			case DB_ORACLE:
				$msSelect = 'SELECT ' . $psValues . ' FROM DUAL';
				break;
			default:
				$msSelect = 'SELECT ' . $psValues;
				break;
		}
		return $msSelect;
	}

	/**
	 * Retorna uma express�o de "like" case insensitive entre uma coluna e uma string constante.
	 *
	 * <p>Retorna uma express�o de "like" case insensitive entre uma coluna e uma string constante.</p>
	 * @access public
	 * @param string $psColumn Nome/express�o da coluna
	 * @param string $psString A string constante
	 * @return string Uma string contendo a express�o
	 */
	public function getCaseInsensitiveLike($psColumn,$psString){
		if($this->csType==DB_MSSQL){
			return "{$psColumn} LIKE '%{$psString}%'";
		}else{
			//return "UPPER({$psColumn}) LIKE '%".strtoupper($psString)."%'";
			return "UPPER({$psColumn}) LIKE UPPER('%".$psString."%')";
		}
	}

	/**
	 * Retorna a string escapada de acordo com o banco.
	 *
	 * <p>M�todo para retornar a string escapada de acordo com o banco.</p>
	 * @access public
	 * @param string $psString String
	 * @return string String escapada
	 */
	public function quote($psString) {
		return $this->coConnection->Quote($psString);
	}

	/**
	 * Retorna as strings concatenadas.
	 *
	 * <p>M�todo para retornar as strings concatenadas.</p>
	 * @access public
	 * @param string $psString1 String
	 * @param string $psString2 String
	 * @return string Par�metros concatenados
	 */
	public function concat($psString1, $psString2) {
		return $this->coConnection->concat($psString1, $psString2);
	}

	/*************************************ADODB ONLY****************************************/

	/**
	 * Loga as consultas feitas no banco.
	 *
	 * <p>M�todo para logar as consultas feitas no banco.</p>
	 * @access public
	 * @param boolean $pbLog True ou False
	 */
	public function logSQL($pbLog) {
		$this->coConnection->LogSQL($pbLog);
	}

	/**
	 * Mostra os resultados do log.
	 *
	 * <p>M�todo para mostrar os resultados do log.</p>
	 * @access public
	 */
	public function showLog() {
		$perf = NewPerfMonitor($this->coConnection);
		echo $perf->SuspiciousSQL();
		echo $perf->ExpensiveSQL();
		echo $perf->InvalidSQL();
	}

}
?>