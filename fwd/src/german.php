<?php
$gaStrings = array(

/* './abstracts/FWDGridAbstract.php' */

'fwdgrid_pref_column_label'=>'Spalte:',
'fwdgrid_pref_column_name'=>'Rubrik',
'fwdgrid_pref_column_order'=>'Auftrag:',
'fwdgrid_pref_column_order_title'=>'Rasterfeldspalten order',
'fwdgrid_pref_column_properties'=>'Spalten Eigenschaften',
'fwdgrid_pref_column_show_title'=>'Spalten zeigen und /oder ein Abstand dazwischen',
'fwdgrid_pref_column_width'=>'Weite*:',
'fwdgrid_pref_columns_grid'=>'Spalten:',
'fwdgrid_pref_columns_show'=>'Felder anzeigen:',
'fwdgrid_pref_false_value'=>'Nein',
'fwdgrid_pref_grid_columns_title'=>'Rasterfeldspalten:',
'fwdgrid_pref_grid_properties'=>'Feldeigenschaftsbeschreibung',
'fwdgrid_pref_rows_height'=>'Reihenhöhe:',
'fwdgrid_pref_rows_per_page'=>'Reihen per Seite:',
'fwdgrid_pref_select_source'=>'Auswahl der Quelle',
'fwdgrid_pref_select_target'=>'Ziel auswählen',
'fwdgrid_pref_target'=>'Ziel:',
'fwdgrid_pref_tooltip_label'=>'Hilfsmittel:',
'fwdgrid_pref_true_value'=>'ja',
'fwdgrid_pref_width_obs'=>'* \'0\' = automatische Weite',

/* './base/FWDDefaultErrorHandler.php' */

'error_config_corrupt'=>'Die Konfigurationsakte ist falsch.',
'error_configuration'=>'Die Konfiguration ist falsch.',
'error_db_connection'=>'Databankverbindungsfehler.',
'error_db_invalid_object_name'=>'Ungültiger Datenbank-Objektname. Vermutlich falscher DB Nutzer',
'error_debug_permission_denied'=>'Erlaubnis verweigert in Prüfakte.',
'error_session_permission_denied'=>'Erlaubnis verweigert in Sitzungsakte.',
'error_smtp'=>'Fehler bei der Verbindung zum Mailserver.',

/* './base/FWDLanguage.php' */

'default_language'=>'Portugiesisch',

/* './base/FWDLicense.php' */

'error_corrupt_hash'=>'Hash ist falsch.',
'error_corrupt_license'=>'Lizenz ist falsch.',
'error_permission_denied'=>'Erlaubnis verweigert in Aktivierungskartei.',

/* './base/FWDSyslog.php' */

'error_connection_syslog'=>'Konnte nicht verbinden zum Syslog Server.',

/* './event/classEvent/FWDGridUserEdit.php' */

'fwdGridUserEditErrorWidthGrid'=>'Widersprüchliches wurde gefunden: Die Summe der Spalten muss kleiner sein oder gleich zu:',
'fwdGridUserEditErrorWidthGridExplain'=>'der enthaltene Er ist jedoch:',

/* './formats/FWDPDF.php' */

'fpdf_page'=>'Seite',

/* './report/FWDReportClassificator.php' */

'report_confidencial'=>'Vertraulicher Bericht',
'report_institutional'=>'Einheitlicher Bericht',
'report_public'=>'Öffentlicher Bericht',

/* './report/FWDReportGenerator.php' */

'report_confidential_report'=>'Vertraulicher Bericht',
'report_excel'=>'Excel',
'report_html'=>'HTML',
'report_html_download'=>'HTML Download',
'report_institutional_report'=>'Einheitlicher Bericht',
'report_pdf'=>'PDF',
'report_public_report'=>'Öffentlicher Bericht',
'report_word'=>'Word',

/* './view/FWDCalendar.php' */

'locale'=>'de',

/* './view/FWDGrid.php' */

'fwdgrid_info_no_result_rows'=>'Es wurden keine Ergebnisse gefunden',
'fwdgrid_number_page_of'=>'von',
'fwdgrid_page_label'=>'Seite',

/* './view/FWDTreeAjax.php' */

'FWDLoading'=>'Loading',
);
?>