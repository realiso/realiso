/**
 * FWD - FAST WEB DEVELOPMENT
 * 
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/*******************************************************************************
 * Classe CodeViewer. Extende SpecialElement para definir a janela que exibe o
 * c�digo XML.
 * 
 * <p>Extende SpecialElement para definir a janela que exibe o c�digo XML.</p>
 *
 * @package FID
 * @param function funcSetXML Fun��o de callback chamada quando o bot�o de
 *        carregar o XML � clicado
 * @param function funcGetXML Fun��o usada para obter o c�digo XML a ser exibido
 * @param function funcClose Fun��o de callback chamada quando o CodeViewer �
 *        ocultado clicando-se no bot�o de fechar
 *******************************************************************************/
function CodeViewer(funcSetXML,funcGetXML,funcClose){
  var self = new SpecialElement('codeViewer');
  self.elementType = 'CodeViewer';
  self.defaultClassName = '';

 /**
  * Fun��o de callback chamada quando o bot�o de carregar o XML � clicado
  * @var function
  */
  self.setXML = funcSetXML;
  
 /**
  * Fun��o usada para obter o c�digo XML a ser exibido
  * @var function
  */
  self.getXML = funcGetXML;
  
 /**
  * Fun��o de callback chamada quando o CodeViewer � ocultado clicando-se no bot�o de fechar
  * @var function
  */
  self.closeFunc = funcClose;
  
  self.createHtmlElement('TABLE');
  self.setPosition(250,100);
  self.setSize(500,600);
  // Constroi a tabela
  // Primeira linha (cabecalho)
  var codeViewer = gebi(self.id);
  var tbody = document.createElement('TBODY');
  var line = document.createElement('TR');
  var cel = document.createElement('TD');
  var button;
  codeViewer.className = 'editor';
  cel.innerHTML = 'Code Viewer';
  cel.className = 'header';
  line.appendChild(cel);
  tbody.appendChild(line);
  // Segunda linha (conteudo)
  line = document.createElement('TR');
  cel = document.createElement('TD');
  cel.style.height = '100%';
  cel.style.width = '100%';
  cel.innerHTML = '<textarea id="codeViewer_code" wrap="off" style="height:100%;width:100%;"></textarea>';
  line.appendChild(cel);
  tbody.appendChild(line);
  // Terceira linha (botoes)
  line = document.createElement('TR');
  cel = document.createElement('TD');
  button = document.createElement('INPUT');
  button.type = 'button';
  button.value = 'Load from XML';
  button.className = 'regularButton';
  button.onclick = function(){ self.loadFromXML(); }
  cel.className = 'attributeName';
  cel.appendChild(button);
  button = document.createElement('INPUT');
  button.type = 'button';
  button.className = 'regularButton';
  button.value = 'Close';
  button.onclick = function(){ self.hide(); self.closeFunc(); }
  cel.className = 'attributeName';
  cel.appendChild(button);
  line.appendChild(cel);
  tbody.appendChild(line);

  codeViewer.appendChild(tbody);
  // A linha abaixo serve para contornar um BUG do IE que faz com que apareca uma barra preta no meio da tela
  //gebi('codeViewer_code').value = '';
  self.hide();
  
 /*****************************************************************************
  * Torna o CodeViewer vis�vel.
  *
  * <p>Torna o CodeViewer vis�vel e atualiza o seu conte�do.</p>
  *****************************************************************************/
  self.show = function(){
    self.visible = true;
    codeViewer.style.visibility = 'visible';
    gebi('codeViewer_code').value = self.getXML();
    gebi('codeViewer_code').select();
  }
  
 /*****************************************************************************
  * Parseia o c�digo XML
  *
  * <p>Parseia o c�digo XML, criando um objeto XMLDocument e o passa para a
  * fun��o de callback setXML().</p>
  *****************************************************************************/
  self.loadFromXML = function(){
    var xmlDom;
    var xmlCode = gebi('codeViewer_code').value;
    if(window.ActiveXObject){ // IE
      xmlDom = new ActiveXObject("Microsoft.XMLDOM");
      xmlDom.async = false;
      xmlDom.loadXML(xmlCode);
    }else if (document.implementation && document.implementation.createDocument){ // Mozilla, etc.
      var parser = new DOMParser();
      xmlDom = parser.parseFromString(xmlCode,"text/xml");
    }else{
      alert('Erro: O seu navegador n�o suporta essa funcionalidade.');
      return false;
    }
    self.setXML(xmlDom);
  }
  
  return self;
}