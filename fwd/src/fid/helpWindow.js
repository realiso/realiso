/**
 * FWD - FAST WEB DEVELOPMENT
 * 
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/*******************************************************************************
 * Classe HelpWindow. Extende SpecialElement para definir a janela que exibe o
 * help.
 * 
 * <p>Extende SpecialElement para definir a janela que exibe o help. � criado
 * invis�vel.</p>
 *
 * @package FID
 * @param String content Conte�do do help em HTML
 *******************************************************************************/
function HelpWindow(content){
  var self = new SpecialElement('helpWindow');
  self.elementType = 'HelpWindow';
  self.defaultClassName = '';
  self.createHtmlElement('TABLE');
  self.setPosition(200,70);
  self.setSize(500,600);
  // Constroi a tabela
  // A primeira linha eh o cabecalho
  var helpWindow = gebi(self.id);
  var tbody = document.createElement('TBODY');
  var line = document.createElement('TR');
  var cel = document.createElement('TD');
  helpWindow.className = 'editor';
  cel.innerHTML = 'Help';
  cel.className = 'header';
  line.appendChild(cel);
  tbody.appendChild(line);
  // Na segunda linha vai o conteudo
  line = document.createElement('TR');
  cel = document.createElement('TD');
  cel.style.height = '100%';
  cel.style.width = '100%';
  cel.innerHTML = content;
  line.appendChild(cel);
  tbody.appendChild(line);
  helpWindow.appendChild(tbody);
  self.hide();
  return self;
}

