/**
 * FWD - FAST WEB DEVELOPMENT
 * 
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/*******************************************************************************
 * Classe Selection. Elemento que representa a �rea da sele��o por �rea.
 * 
 * <p>Elemento que representa a �rea da sele��o por �rea.</p>
 * @package FID
 *******************************************************************************/
function Selection(){
  var self = new Element('Selection');
  self.elementType = 'Selection';
  self.htmlElement = self.createHtmlElement('DIV');
  self.htmlElement.className = 'selection';
  self.htmlElement.style.display = 'none';
  
 /*****************************************************************************
  * Exibe o elemento.
  *
  * <p>Exibe o elemento.</p>
  *****************************************************************************/
  self.show = function(){
    self.htmlElement.style.display = 'block';
  }
  
 /*****************************************************************************
  * Oculta o elemento.
  *
  * <p>Oculta o elemento.</p>
  *****************************************************************************/
  self.hide = function(){
    self.htmlElement.style.display = 'none';
  }
  
  return self;
}

