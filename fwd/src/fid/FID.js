/**
 * FWD - FAST WEB DEVELOPMENT
 * 
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/*******************************************************************************
 * Classe FID. Classe principal que define a FID em si.
 * 
 * <p>Classe principal que define a FID em si.</p>
 *
 * @package FID
 *******************************************************************************/
function FID(){

 /**
  * Refer�ncia para si mesmo. � necess�ria porque this n�o � realmente
  * uma refer�ncia e sim um construtor da linguagem. Na hora de definir um
  * handler de evento, por exemplo, self deve ser usado para referenciar a FID,
  * j� que this referenciaria o objeto que tivesse capturado o evento.
  * @var FID
  */
  var self = this;

 /**
  * Lista de elementos criados pelo usu�rio
  * @var Array
  */
  this.elements = new Array();
  
 /**
  * Lista de elementos de interface
  * @var Array
  */
  this.specialElements = new Array();
  
 /**
  * Lista de elementos criados pelo usu�rio que est�o selecionados.
  * Esse Array n�o cont�m objetos Element, e sim strings contendo os paths dos
  * elementos.
  * @var Array
  */
  this.selecteds = new Array();
  
 /**
  * Indica se o usu�rio est� arrastando os objetos selecionados
  * @var boolean
  */
  this.dragging = false;
  
 /**
  * Indica se o usu�rio est� redimensionando o objeto selecionado. Caso esteja,
  * a string indica por que borda o elemento est� sendo redimensionado usando os
  * pontos cardeais e colaterais.
  * @var String
  */
  this.resizing = '';
  
 /**
  * Indica se o usu�rio est� selecionando elementos por �rea.
  * @var boolean
  */
  this.selecting = false;
  
 /**
  * Elemento que representa a �rea atual da sele��o por �rea.
  * @var Selection
  */
  this.selection = new Selection();

 /**
  * Armazena o valor da coordenada x do mouse quando os elementos selecionados
  * est�o sendo arrastados para indicar quanto eles deem ser movidos ao
  * acontecer um evento de mousemove.
  * @var integer
  */
  this.startDragX = 0;
  
 /**
  * Armazena o valor da coordenada y do mouse quando os elementos selecionados
  * est�o sendo arrastados para indicar quanto eles deem ser movidos ao
  * acontecer um evento de mousemove.
  * @var integer
  */
  this.startDragY = 0;
  
 /**
  * Armazena o valor da coordenada x inicial do mouse quando os elementos
  * selecionados come�am a ser arrastados. � usado para restaurar a posi��o
  * inicial dos elementos, caso o usu�rio tente arrast�-los para uma posi��o
  * ilegal.
  * @var integer
  */
  this.realStartDragX = 0;
  
 /**
  * Armazena o valor da coordenada y inicial do mouse quando os elementos
  * selecionados come�am a ser arrastados. � usado para restaurar a posi��o
  * inicial dos elementos, caso o usu�rio tente arrast�-los para uma posi��o
  * ilegal.
  * @var integer
  */
  this.realStartDragY = 0;
  
 /**
  * Tamanho da grade
  * @var integer
  */
  this.gridSize = 5;
  
 /**
  * Array associativo de entidades declaradas no doctype. As chaves do array s�o
  * os valores e os valores, seus nomes.
  * @var Object
  */
  this.entities = {};
  
 /**
  * String contendo a tag doctype com a declara��o das entidades usadas no XML.
  * @var String
  */
  this.doctype = '';
  
 /**
  * Div que serve como um vidro para tampar a tela durante a cria��o de um elemento
  * @var HTMLDivElement
  */
  this.glass = document.createElement('DIV');

  // Inicializa o vidro e o adiciona ao corpo da p�gina.
  this.glass.style.zIndex = 1000;
  this.glass.style.display = 'none';
  this.glass.style.position = 'absolute';
  this.glass.style.top = '0px';
  this.glass.style.left = '0px';
  document.getElementsByTagName('body').item(0).appendChild(this.glass);
  
 /**
  * Indica se o browser � o IE
  * @var boolean
  */
  this.isIE = (window.ActiveXObject?true:false);
  
 /**
  * O editor de elementos
  * @var ElementEditor
  */
  this.editor = new ElementEditor();
  
  this.specialElements[this.editor.name] = this.editor;
  this.editor.setAttribute('width',204);
  
 /**
  * A janela que exibe o help
  * @var HelpWindow
  */
  this.helpWindow = new HelpWindow(gebi('helpText').innerHTML);
  
  this.specialElements[this.helpWindow.name] = this.helpWindow;
  
 /*****************************************************************************
  * Retorna o c�digo XML da dialog
  *
  * <p>Retorna o c�digo XML da dialog, ou seja, de todos elementos.</p>
  *****************************************************************************/
  self.getXMLCode = function(){
    return '<?xml version="1.0" encoding="ISO-8859-1"?>\n'
          +self.doctype
          +self.dialog.getXMLCode('',new TabIndex());
  }
  
 /*****************************************************************************
  * Constr�i os elementos a partir do XML
  *
  * <p>Dado um objeto XMLDocument, destr�i os elementos existentes e cria novos
  * de acordo com o XMLDocument.</p>
  * 
  * @param XMLDocument xmlDom Objeto que representa o XML j� parseado
  *****************************************************************************/
  self.loadFromXmlDom = function(xmlDom){
    self.clear();
    var root = null;
    var dialog = null;
    for(elem in xmlDom.childNodes){
      if(xmlDom.childNodes[elem].nodeName=='root'){
        root = xmlDom.childNodes[elem];
        break;
      }else if(xmlDom.childNodes[elem].nodeName=='dialog'){
        dialog = xmlDom.childNodes[elem];
        break;
      }else if(xmlDom.childNodes[elem].internalSubset){
        self.loadEntities(xmlDom.childNodes[elem].internalSubset,xmlDom.childNodes[elem].nodeName);
      }
    }
    if(root && !dialog){
      for(elem in root.childNodes){
        if(root.childNodes[elem].nodeName=='dialog'){
          dialog = root.childNodes[elem];
          break;
        }else if(/report/i.test(root.childNodes[elem].nodeName)){
          var moBox = {
            'top': parseInt(self.dialog.getAttribute('top')),
            'left': parseInt(self.dialog.getAttribute('left'))
          };
          var moReport = new Report(root.childNodes[elem].nodeName);
          moReport.loadFromXML(root.childNodes[elem],moBox);
          gebi('dialog').style.display = 'none';
          self.dialog = moReport;
          self.elements['report'] = moReport;
          for(var childName in self.dialog.children){
            self.recursivelyAddElement(self.dialog.children[childName]);
          }
          self.unselectAll();
          selectMode('report');
          return;
        }
      }
    }
    gebi('dialog').style.display = 'block';
    self.dialog = gebi('dialog').elementObject;
    self.dialog.loadFromXML(dialog);
    for(var childName in self.dialog.children){
      self.recursivelyAddElement(self.dialog.children[childName]);
    }
    self.unselectAll();
    selectMode('dialog');
  }
  
 /*****************************************************************************
  * Carrega as entidades e monta a declara��o da tag doctype.
  *
  * <p>Carrega as entidades e monta a declara��o da tag doctype.</p>
  * @param String entitiesString String contendo a declara��o das entidades
  * @param String doctype Nome do doctype
  *****************************************************************************/
  self.loadEntities = function(entitiesString,doctype){
    var tags = entitiesString.split('>');
    var entities = new Object();
    var regExpEnt = /<!ENTITY[\s]+([\w\d]+)[\s]+"([^"]+)"/;
    var regExpRef = /\&([\w\d]+);/;
    var matches;
    for(var tag in tags){
      matches = regExpEnt.exec(tags[tag]);
      if(matches && matches.length==3){
        entities[matches[1]] = matches[2];
      }
    }
    var changed;
    do{
      changed = false;
      for(e in entities){
        matches = regExpRef.exec(entities[e]);
        if(matches && entities[matches[1]]){
          entities[e] = entities[e].replace(matches[0],entities[matches[1]]);
          changed = true;
        }
      }
    }while(changed);
    self.entities = {};
    var hasEntities = false;
    for(e in entities){
      self.entities[entities[e]] = e;
      hasEntities = true;
    }
    if(hasEntities){
      entitiesString = entitiesString.replace(/[\n\t\s]+/g,' ');
      entitiesString = entitiesString.replace(/> </g,'>\n  <');
      entitiesString = entitiesString.replace(/(^ )|( $)/,'');
      self.doctype = '<!DOCTYPE '+doctype+' [\n  '+entitiesString+'\n]>\n';
    }else{
      self.doctype = '';
    }
  }
  
 /*****************************************************************************
  * Substitui valores de entidades por refer�ncias a elas.
  *
  * <p>Dada uma string, se ela for igual ao valor de alguma entidade, substitui
  * por uma refer�ncia a essa entidade, sen�o retorna ela mesma.</p>
  * @param String str String a ser substituida
  *****************************************************************************/
  self.replaceEntities = function(str){
    if(self.entities && self.entities[str]){
      return '&'+self.entities[str]+';';
    }else{
      return str;
    }
  }
  
 /*****************************************************************************
  * Fun��o chamada quando o CodeViewer � fechado
  *
  * <p>Fun��o chamada quando o CodeViewer � fechado. Ela s� muda o �cone do
  * bot�o de exibir/ocultar para indicar que o CodeViewer est� invis�vel.</p>
  *****************************************************************************/
  self.closeCodeViewer = function(){
    var button = gebi('bt_codeViewer');
    button.show = false;
    button.src = 'imgs/'+button.name+'_.png';
    button.title = 'Show '+button.label;
  }
  
 /**
  * Janela que exibe o c�digo XML.
  * @var CodeViewer
  */
  this.codeViewer = new CodeViewer(this.loadFromXmlDom,this.getXMLCode,this.closeCodeViewer);
  
  this.specialElements[this.codeViewer.name] = this.codeViewer;
  
 /**
  * Lista de tipos de elementos conhecidos pela FID. Cada tipo tem nome, altura
  * e largura default e um contador de quantos elementos do tipo existem.
  * @var Array
  */
  this.elementTypes = new Array();
  
 /**
  * Tipo selecionado. Quando o usu�rio clica num bot�o de elemento, ele
  * seleciona um tipo e quando ele clica na dialog, ele cria um elemento do tipo
  * que estava selecionado na posi��o do cursor.
  * @var String
  */
  this.selectedType = '';

 /**
  * O elemento raiz, o container que cont�m todos elementos.
  * @var Dialog
  */
  this.dialog = new Dialog(210,40);
  
  this.dialog.setSize(600,800);
  this.elements['dialog'] = this.dialog;
  // As duas linhas abaixo s�o para contornar um BUG do IE que faz com que apareca uma barra preta no meio da tela
  //this.helpWindow.show();
  //this.helpWindow.hide();
  
 /*****************************************************************************
  * Exibe o vidro
  *
  * <p>Exibe (ativa) o vidro.</p>
  *****************************************************************************/
  this.showGlass = function(){
    var body = document.getElementsByTagName('body').item(0);
    this.glass.style.display = 'block';
    this.glass.style.width = body.scrollWidth;
    this.glass.style.height = body.scrollHeight;
  }

 /*****************************************************************************
  * Oculta o vidro
  *
  * <p>Oculta (desativa) o vidro.</p>
  *****************************************************************************/
  this.hideGlass = function(){
    this.glass.style.display = 'none';
  }
  
 /*****************************************************************************
  * Registra um tipo de elementos na FID
  *
  * <p>Registra um tipo de elementos na FID para que ela possa instanciar
  * elementos desse tipo</p>
  * 
  * @param String name Nome do tipo
  * @param integer defaultHeight Altura inicial dos elementos desse tipo
  * @param integer defaultWidth Largura inicial dos elementos desse tipo
  *****************************************************************************/
  this.registerElementType = function(name,defaultHeight,defaultWidth){
    var newType = new Object();
    newType.name = name;
    newType.defaultHeight = defaultHeight;
    newType.defaultWidth = defaultWidth;
    newType.counter = 0;
    this.elementTypes[newType.name] = newType;
  }
  
 /*****************************************************************************
  * Verifica se um tipo est� registrado
  *
  * <p>Dado o nome de um tipo, verifica se ele est� registrado na FID</p>
  * 
  * @param String typeName Nome do tipo
  * @return boolean Indica se o tipo est� registrado
  *****************************************************************************/
  this.typeIsRegistered = function(typeName){
    return (this.elementTypes[typeName]!=undefined);
  }
  
 /*****************************************************************************
  * Retorna a defini��o de um tipo
  *
  * <p>Dado o nome de um tipo, retorna sua defini��o. Caso o tipo n�o esteja
  * registrado, uma mensagem de erro ser� exibida.</p>
  * 
  * @param String typeName Nome do tipo
  * @return Object Objeto usado como uma struct contendo os dados do tipo
  *****************************************************************************/
  this.getType = function(typeName){
    return this.elementTypes[typeName];
  }
  
 /*****************************************************************************
  * Seleciona um tipo de elementos
  *
  * <p>Seleciona um tipo de elementos. Quando o usu�rio clicar em algum lugar
  * um elemento do tipo selecionado ser� criado nesse lugar. Caso o tipo n�o
  * esteja registrado, uma mensagem de erro ser� exibida.</p>
  * 
  * @param String typeName Nome do tipo
  * @return boolean Indica sucesso da opera��o
  *****************************************************************************/
  this.selectType = function(typeName){
    if(this.typeIsRegistered(typeName)){
      this.selectedType = typeName;
      this.setCursorStyle('crosshair');
      this.showGlass();
      return true;
    }else{
      alert("Erro: O tipo '"+typeName+"' n�o foi registrado.");
      return false;
    }
  }
  
 /*****************************************************************************
  * Cria um elemento
  *
  * <p>Cria um elemento do tipo especificado no lugar indicado. Caso o tipo n�o
  * esteja registrado, uma mensagem de erro ser� exibida.</p>
  * 
  * @param String typeName Nome do tipo do elemento a ser criado
  * @param integer x Coordenada x do canto superior esquerdo do elemento
  * @param integer y Coordenada y do canto superior esquerdo do elemento
  * @return boolean Indica sucesso da opera��o
  *****************************************************************************/
  this.createElement = function(typeName,x,y){
    if(!this.typeIsRegistered(typeName)){
      alert("O tipo '"+typeName+"' n�o foi registrado.");
      return false;
    }else{
      var newElement;
      var type = this.getType(typeName);
      var elementName = typeName + ++type.counter;
      eval('newElement = new '+type.name+'(elementName);');
      newElement.setSize(type.defaultHeight,type.defaultWidth);
      newElement.setPosition(x,y);
      this.elements[elementName] = newElement;
      
      gobi('elementSelector').addItem(elementName,elementName);
      
      return newElement;
    }
  }
  
 /*****************************************************************************
  * Adiciona um elemento � FID
  *
  * <p>Adiciona um elemento j� criado � FID. Caso o tipo do elemento n�o
  * esteja registrado, uma mensagem de erro ser� exibida. Usado na carga de
  * XML.</p>
  * 
  * @param Element Elemento a ser inserido na FID.
  * @return boolean Indica sucesso da opera��o
  *****************************************************************************/
  this.addElement = function(element){
    var typeName = element.elementType;
    if(!this.typeIsRegistered(typeName)){
      alert("O tipo '"+typeName+"' n�o foi registrado.");
      return false;
    }else{
      this.getType(typeName).counter++;
      this.elements[element.name] = element;
      
      gobi('elementSelector').addItem(element.name,element.name);
      
      return true;
    }
  }
  
 /*****************************************************************************
  * Mesmo que addElement(), mas recursivo
  *
  * <p>Adiciona um elemento e seus descendentes � FID. Caso o tipo de um dos
  * elementos n�o esteja registrado, uma mensagem de erro � exibida e a
  * opera��o � abortada. Usado na carga de XML.
  * </p>
  * 
  * @param Element Elemento a ser inserido na FID.
  * @return boolean Indica sucesso da opera��o
  *****************************************************************************/
  this.recursivelyAddElement = function(element){
    if(this.addElement(element)){
      for(var childName in element.children){
        if(!this.recursivelyAddElement(element.children[childName])) return false;
      }
      return true;
    }else return false;
  }
  
 /*****************************************************************************
  * Seta o tipo de cursor a ser exibido.
  *
  * <p>Seta o tipo de cursor a ser exibido.</p>
  * 
  * @param String style String contendo o estilo do cursor
  *****************************************************************************/
  this.setCursorStyle = function(style){
    document.getElementsByTagName('body').item(0).style.cursor = style;
  }
  
 /*****************************************************************************
  * Verifica se um elemento est� numa posi��o permitida
  *
  * <p>Verifica se um elemento est� numa posi��o legal. Posi��o legal �, no
  * caso da dialog, n�o colidir com elementos especiais e, no caso dos demais
  * elementos, estar dentro da dialog.</p>
  * 
  * @param Element Elemento a ser testado
  * @return boolean Indica se a posi��o do elemento � legal
  *****************************************************************************/
  this.positionIsLegal = function(element){
    if(element!=this.dialog && !element.isInside(this.dialog)) return false;
    var left = parseInt(element.getAttribute('left'));
    var right = left + parseInt(element.getAttribute('width'));
    var top = parseInt(element.getAttribute('top'));
    var bottom = top + parseInt(element.getAttribute('height'));
    return !this.colideWithSpecial(left,top,right,bottom);
  }
  
 /*****************************************************************************
  * Retorna um elemento, dado o seu nome
  *
  * <p>Retorna um elemento contido na FID, dado o seu nome. Se a FID n�o cont�m
  * um elemento com esse nome, retorna null.</p>
  * 
  * @param String elementName Nome do elemento
  * @return Element O elemento em si
  *****************************************************************************/
  this.getElement = function(elementName){
    if(this.elements[elementName]==undefined) return null;
    else return this.elements[elementName];
  }
  
 /*****************************************************************************
  * Seleciona um elemento
  *
  * <p>Seleciona um elemento, adicionando-o � lista de elementos selecionados.
  * N�o desseleciona os elementos que j� est�o selecionados</p>
  * 
  * @param Element element Elemento a ser selecionado
  *****************************************************************************/
  this.select = function(element){
    var path = element.name;
    var el = element;
    var depth = 0;
    while(el.parent!=null){
      path = el.parent.name+'/'+path;
      el = el.parent;
      depth++;
    }
    if(this.selecteds[depth]==undefined) this.selecteds[depth] = new Object();
    this.selecteds[depth][path] = element.name;
    gebi(element.name).className = 'selected';
    this.debugSelected();
  }
  
  this.debugSelected = function(){
    var miDepth,msPath;
    var msDebug = '';
    for(miDepth in this.selecteds){
      for(msPath in this.selecteds[miDepth]){
        msDebug+= this.selecteds[miDepth][msPath]+': '+msPath+'<br>';
      }
    }
    gebi('debug').innerHTML = msDebug;
  }

 /*****************************************************************************
  * Desseleciona um elemento
  *
  * <p>Desseleciona um elemento, removendo-o da lista de elementos
  * selecionados.</p>
  * 
  * @param Element element Elemento a ser desselecionado
  *****************************************************************************/
  this.unselect = function(element){
    var path = element.name;
    var el = element;
    var depth = 0;
    while(el.parent!=null){
      path = el.parent.name+'/'+path;
      el = el.parent;
      depth++;
    }
    gebi(element.name).className = element.defaultClassName;
    delete this.selecteds[depth][path];
    this.debugSelected();
  }
  
 /*****************************************************************************
  * Desseleciona todos elementos
  *
  * <p>Desseleciona todos elementos selecionados, limpando a lista de elementos
  * selecionados.</p>
  *****************************************************************************/
  this.unselectAll = function(){
    var i,j,elementName;
    for(i=this.selecteds.length;i>=0;i--){
      for(j in this.selecteds[i]){
        elementName = this.selecteds[i][j];
        gebi(elementName).className = this.getElement(elementName).defaultClassName;
        delete this.selecteds[i][j];
      }
    }
    this.debugSelected();
  }
  
 /*****************************************************************************
  * Testa se um elemento est� selecionado
  *
  * <p>Testa se um elemento est� selecionado.</p>
  * 
  * @param Element element Elemento a ser testado
  * @return boolean Indica se o elemento est� selecionado
  *****************************************************************************/
  this.isSelected = function(element){
    var path = element.getPath();
    var depth = path.split('/').length - 1;
    if(this.selecteds[depth]==undefined) return false;
    else return (this.selecteds[depth][path]!=undefined);
  }

 /*****************************************************************************
  * Testa se algum ancestral do elemento est� selecionado
  *
  * <p>Testa se algum ancestral do elemento est� selecionado.</p>
  * 
  * @param String path Caminho do elemento a ser testado
  * @return boolean Indica se um ancestral do elemento est� selecionado
  *****************************************************************************/
  this.ancestralIsSelected = function(path){
    var str;
    var pathArray = path.split('/');
    var maxDepth = pathArray.length - 1;
    for(var depth=0;depth<maxDepth;depth++){
      if(depth==0) str = pathArray[depth];
      else str+= '/'+pathArray[depth];
      if(this.selecteds[depth] && this.selecteds[depth][str]!=undefined) return true;
    }
    return false;
  }
  
 /*****************************************************************************
  * Move os elementos selecionados
  *
  * <p>Move todos elementos selecionados e seus descendentes de acordo com o
  * deslocamento indicado.</p>
  * 
  * @param integer offsetX Deslocamento na coordenada x
  * @param integer offsetY Deslocamento na coordenada y
  *****************************************************************************/
  this.moveSelected = function(offsetX,offsetY){
    var i,j;
    for(i=this.selecteds.length;i>=0;i--){
      for(j in this.selecteds[i]){
        if(!this.ancestralIsSelected(j)) this.getElement(this.selecteds[i][j]).move(offsetX,offsetY);
      }
    }
  }
  
  this.alignSelectedsToGrid = function(){
    var i,j;
    for(i=self.selecteds.length;i>=0;i--){
      for(j in self.selecteds[i]){
        if(!self.ancestralIsSelected(j)) self.getElement(self.selecteds[i][j]).alignToGrid();
      }
    }
  }
  
 /*****************************************************************************
  * Iguala o tamanho de todos elementos selecionados
  *
  * <p>Iguala o tamanho de todos elementos selecionados. O primeiro elemento da
  * lista de selecionados serve de refer�ncia.</p>
  * 
  * @return boolean Indica sucesso da opera��o
  *****************************************************************************/
  this.equalSelectedsSize = function(){
    if(this.selectedsLength()>1){
      var first = this.getFirstSelected();
      var newHeight = parseInt(first.getAttribute('height'));
      var newWidth = parseInt(first.getAttribute('width'));
      var i,j;
      for(i=0;i<this.selecteds.length;i++){
        for(j in this.selecteds[i]){
          this.getElement(this.selecteds[i][j]).setSize(newHeight,newWidth);
        }
      }
      this.updateHierarchy();
      return true;
    }else return false;
  }
  
 /*****************************************************************************
  * Alinha os elementos selecionados
  *
  * <p>Alinha os elementos selecionados acima, abaixo, � esquerda ou � 
  * direita. O primeiro elemento da lista de selecionados serve de refer�ncia.
  * </p>
  * 
  * @param String direction Indica em que dire��o os elementos devem ser
  *        alinhados (top, bottom, left ou right)
  * @return boolean Indica sucesso da opera��o
  *****************************************************************************/
  this.alignSelecteds = function(direction){
    if(this.selectedsLength()>1){
      var i,j;
      var first = this.getFirstSelected();
      if(direction=='top' || direction=='bottom'){
        var top = parseInt(first.getAttribute('top'));
        if(direction=='bottom') var bottom = top + parseInt(first.getAttribute('height'));
      }else if(direction=='left' || direction=='right'){
        var left = parseInt(first.getAttribute('left'));
        if(direction=='right') var right = left + parseInt(first.getAttribute('width'));
      }else{
        alert('Erro: As �nicas dire��es v�lidas para o m�todo alignSelecteds s�o: top, bottom, left e right.');
        return false;
      }
      for(i=0;i<this.selecteds.length;i++){
        for(j in this.selecteds[i]){
          element = this.getElement(this.selecteds[i][j]);
          switch(direction){
            case 'top':    element.setAttribute('top',top);                                               break;
            case 'left':   element.setAttribute('left',left);                                             break;
            case 'bottom': element.setAttribute('top',bottom - parseInt(element.getAttribute('height'))); break;
            case 'right':  element.setAttribute('left',right - parseInt(element.getAttribute('width')));  break;
          }
        }
      }
      this.updateHierarchy();
      return true;
    }else return false;
  }
  
 /*****************************************************************************
  * Deleta um elemento
  *
  * <p>Deleta um elemento. O elemento dialog � o �nico que n�o pode ser
  * deletado.</p>
  * 
  * @param Element element Elemento a ser deletado
  *****************************************************************************/
  this.deleteElement = function(element){
    if(element==this.dialog){
      alert("Erro: O elemento raiz n�o pode ser deletado.");
    }else{
      if(this.isSelected(element)) this.unselect(element);
      for(var childName in element.children){
        this.deleteElement(element.children[childName]);
      }
      element.destroy();
      delete this.elements[element.name];
      
      gobi('elementSelector').removeItem(element.name);
      
    }
  }
  
 /*****************************************************************************
  * Deleta os elementos selecionados
  *
  * <p>Deleta os elementos selecionados. O elemento dialog � o �nico que n�o
  * pode ser deletado.</p>
  *****************************************************************************/
  this.deleteSelected = function(){
    var i,j;
    for(i=this.selecteds.length;i>=0;i--){
      for(j in this.selecteds[i]){
        element = this.getElement(this.selecteds[i][j]);
        this.deleteElement(element);
      }
    }
  }
  
 /*****************************************************************************
  * Indica o n�mero de elementos selecionados
  *
  * <p>Indica o n�mero de elementos selecionados</p>
  * 
  * @return integer N�mero de elementos selecionados
  *****************************************************************************/
  this.selectedsLength = function(){
    var i,j,length = 0;
    for(i=0;i<this.selecteds.length;i++){
      for(j in this.selecteds[i]) length++;
    }
    return length;
  }
  
 /*****************************************************************************
  * Retorna o primeiro elemento da lista de selecionados
  *
  * <p>Retorna o primeiro elemento da lista de selecionados. Se n�o houver
  * elementos selecionados, retorna null.</p>
  * 
  * @return Element Primeiro elemento da lista de selecionados
  *****************************************************************************/
  this.getFirstSelected = function(){
    var i,j;
    for(i=0;i<this.selecteds.length;i++){
      for(j in this.selecteds[i]) return this.getElement(this.selecteds[i][j]);
    }
    return null;
  }
  
 /*****************************************************************************
  * Verifica se os elementos selecionados est�o em posi��es permitidas
  *
  * <p>Verifica se todos elementos selecionados est�o em posi��es legais.</p>
  * 
  * @return boolean Indica se a posi��o do elementos selecionados � legal
  *****************************************************************************/
  this.selectedsPositionIsLegal = function(){
    var i,j;
    for(i=this.selecteds.length;i>=0;i--){
      for(j in this.selecteds[i]){
        if(!this.ancestralIsSelected(j)){
          if(!this.positionIsLegal(this.getElement(this.selecteds[i][j]))) return false;
        }
      }
    }
    return true;
  }
  
 /*****************************************************************************
  * Inicia a a��o de arrastar os elementos selecionados
  *
  * <p>Inicia a a��o de arrastar os elementos selecionados</p>
  * 
  * @param MouseEvent event Evento de mouse que iniciou a a��o
  *****************************************************************************/
  this.startDrag = function(event){
    event.clientX = self.alignToGrid(event.clientX);
    event.clientY = self.alignToGrid(event.clientY);
    self.dragging = true;
    self.startDragX = self.realStartDragX = event.clientX;
    self.startDragY = self.realStartDragY = event.clientY;
    self.setCursorStyle('move');
  }
  
 /*****************************************************************************
  * Atualiza as informa��es sobre a a��o (em andamento) de arrastar os
  * elementos selecionados
  *
  * <p>Atualiza as informa��es sobre a a��o (em andamento) de arrastar os
  * elementos selecionados.</p>
  * 
  * @param MouseEvent event Evento de mouse que desencadeou a chamada
  *****************************************************************************/
  this.updateDrag = function(event){
    if(self.dragging){
      event.clientX = self.alignToGrid(event.clientX);
      event.clientY = self.alignToGrid(event.clientY);
      self.moveSelected(event.clientX - self.startDragX, event.clientY - self.startDragY);
      self.startDragX = event.clientX;
      self.startDragY = event.clientY;
    }
  }
  
 /*****************************************************************************
  * Encerra a a��o de arrastar os elementos selecionados
  *
  * <p>Encerra a a��o de arrastar os elementos selecionados.</p>
  * 
  * @param MouseEvent event Evento de mouse que desencadeou a chamada
  *****************************************************************************/
  this.stopDrag = function(event){
    if(self.dragging){
      var i,j,element;
      var mbError = false;
      if(!self.selectedsPositionIsLegal()){
        mbError = true;
      }else{
        self.updateHierarchy();
        for(i=this.selecteds.length;i>=0;i--){
          for(j in this.selecteds[i]){
            if(j!='dialog' && j!='report' && !this.ancestralIsSelected(j)){
              element = this.getElement(this.selecteds[i][j]);
              if(!element.parent){
                mbError = true;
                break;
              }
            }
          }
          if(mbError) break;
        }
      }
      if(mbError){
        self.moveSelected(
          self.alignToGrid(self.realStartDragX - event.clientX),
          self.alignToGrid(self.realStartDragY - event.clientY)
        );
        self.updateHierarchy();
      }
      self.dragging = false;
      self.setCursorStyle('');
      self.updateEditor();
    }
  }
  
 /*****************************************************************************
  * Inicia a a��o de redimensionar o elemento sobre o qual o mouse est�
  *
  * <p>Inicia a a��o de redimensionar o elemento sobre o qual o mouse est�.</p>
  * 
  * @param MouseEvent event Evento de mouse que desencadeou a chamada
  * @param Element resizingElement Elemento que est� sendo redimensionado
  * @param String position Indica por que borda o elemento est� sendo
  *        redimensionado usando os pontos cardeais e colaterais
  *****************************************************************************/
  this.startResize = function(event,resizingElement,position){
    event.clientX = self.alignToGrid(event.clientX);
    event.clientY = self.alignToGrid(event.clientY);
    self.unselectAll();
    self.select(resizingElement);
    self.resizing = position;
    self.startDragX = self.realStartDragX = event.clientX;
    self.startDragY = self.realStartDragY = event.clientY;
  }
  
 /*****************************************************************************
  * Atualiza as informa��es sobre a a��o (em andamento) de redimensionar o
  * elemento sobre o qual o mouse est�
  *
  * <p>Atualiza as informa��es sobre a a��o (em andamento) de redimensionar o
  * elemento sobre o qual o mouse est�.</p>
  * 
  * @param MouseEvent event Evento de mouse que desencadeou a chamada
  *****************************************************************************/
  this.updateResize = function(event){
    event.clientX = self.alignToGrid(event.clientX);
    event.clientY = self.alignToGrid(event.clientY);
    var element = this.getFirstSelected();
    var top = parseInt(element.getAttribute('top'));
    var left = parseInt(element.getAttribute('left'));
    var height = parseInt(element.getAttribute('height'));
    var width = parseInt(element.getAttribute('width'));
    var newTop = top;
    var newLeft = left;
    var newHeight = height;
    var newWidth = width;
    if(self.resizing.indexOf('s')!=-1){
      newHeight = height + event.clientY - self.startDragY;
    }else if(self.resizing.indexOf('n')!=-1){
      newHeight = height - (event.clientY - self.startDragY);
      newTop = top + (event.clientY - self.startDragY);
    }
    if(self.resizing.indexOf('e')!=-1){
      newWidth = width + event.clientX - self.startDragX;
    }else if(self.resizing.indexOf('w')!=-1){
      newWidth = width - (event.clientX - self.startDragX);
      newLeft = left + (event.clientX - self.startDragX);
    }
    if(newHeight<0){
      newHeight = 0;
      if(newTop > top+height) newTop = top+height;
      self.startDragY = newTop;
    }else self.startDragY = event.clientY;
    if(newWidth<0){
      newWidth = 0;
      if(newLeft > left+width) newLeft = left+width;
      self.startDragX = newLeft;
    }else self.startDragX = event.clientX;
    element.setPosition(newLeft,newTop);
    element.setSize(newHeight,newWidth);
  }
  
 /*****************************************************************************
  * Encerra a a��o de redimensionar o elemento sobre o qual o mouse est�
  *
  * <p>Encerra a a��o de redimensionar o elemento sobre o qual o mouse est�.
  * </p>
  * 
  * @param MouseEvent event Evento de mouse que desencadeou a chamada
  *****************************************************************************/
  this.stopResize = function(event){
    if(self.resizing!=''){
      var mbError = false;
      if(self.selectedsPositionIsLegal()){
        self.updateHierarchy();
        var i,j,element;
        for(i=this.selecteds.length;i>=0;i--){
          for(j in this.selecteds[i]){
            if(j!='dialog' && j!='report' && !this.ancestralIsSelected(j)){
              element = this.getElement(this.selecteds[i][j]);
              if(!element.parent){
                mbError = true;
                break;
              }
            }
          }
          if(mbError) break;
        }
      }else{
        mbError = true;
      }
      if(mbError){
        event.clientX = self.realStartDragX;
        event.clientY = self.realStartDragY;
        self.updateResize(event);
        self.updateHierarchy();
      }else{
        self.updateEditor();
      }
      self.resizing = '';
      self.setCursorStyle('');
    }
  }
  
 /*****************************************************************************
  * Inicia a a��o de sele��o por �rea.
  *
  * <p>Inicia a a��o de sele��o por �rea. A menos que a tecla CTRL esteja
  * pressionada, todos elementos selecionados s�o deselecionados.</p>
  * 
  * @param MouseEvent event Evento de mouse que desencadeou a chamada
  *****************************************************************************/
  this.startSelect = function(event){
    if(!event.ctrlKey) self.unselectAll();
    self.selecting = true;
    self.startDragX = self.realStartDragX = event.clientX;
    self.startDragY = self.realStartDragY = event.clientY;
    self.selection.setPosition(self.startDragX,self.startDragY);
    self.selection.setSize(0,0);
    self.selection.show();
  }

 /*****************************************************************************
  * Atualiza a �rea de sele��o.
  *
  * <p>Atualiza a �rea de sele��o.</p>
  * 
  * @param MouseEvent event Evento de mouse que desencadeou a chamada
  *****************************************************************************/
  this.updateSelect = function(event){
    if(self.realStartDragX < event.clientX){
      var x0 = self.realStartDragX;
      var x1 = event.clientX;
    }else{
      var x0 = event.clientX;
      var x1 = self.realStartDragX;
    }
    if(self.realStartDragY < event.clientY){
      var y0 = self.realStartDragY;
      var y1 = event.clientY;
    }else{
      var y0 = event.clientY;
      var y1 = self.realStartDragY;
    }
    self.selection.setPosition(x0,y0);
    self.selection.setSize(y1-y0,x1-x0);
  }

 /*****************************************************************************
  * Encerra a a��o de sele��o por �rea.
  *
  * <p>Encerra a a��o de sele��o por �rea. Os elementos completamente dentro da
  * �rea de sele��o s�o selecionados.</p>
  * 
  * @param MouseEvent event Evento de mouse que desencadeou a chamada
  *****************************************************************************/
  this.stopSelect = function(event){
    if(self.selecting){
      if(self.realStartDragX < event.clientX){
        var x0 = self.realStartDragX;
        var x1 = event.clientX;
      }else{
        var x0 = event.clientX;
        var x1 = self.realStartDragX;
      }
      if(self.realStartDragY < event.clientY){
        var y0 = self.realStartDragY;
        var y1 = event.clientY;
      }else{
        var y0 = event.clientY;
        var y1 = self.realStartDragY;
      }
      self.selection.hide();
      self.selecting = false;
      for(elementName in self.elements){
        if(self.elements[elementName].isInsideRect(x0,y0,x1,y1)){
          self.select(self.elements[elementName]);
        }
      }
      self.updateEditor();
    }
  }
  
 /*****************************************************************************
  * Arredonda um valor para o m�ltiplo mais pr�ximo do tamanho da grade.
  *
  * <p>Arredonda um valor para o m�ltiplo mais pr�ximo do tamanho da grade.</p>
  *
  * @param integer value valor a ser arredondado
  *****************************************************************************/
  this.alignToGrid = function(value){
    return Math.round(value/self.gridSize)*self.gridSize;
  }
  
 /*****************************************************************************
  * Atualiza o ElementEditor de acordo com a sele��o atual
  *
  * <p>Caso haja exatamente um elemento selecionado, ele � exibido no
  * ElementEditor, caso contr�rio, o ElementEditor fica vazio.</p>
  *****************************************************************************/
  this.updateEditor = function(){
    if(this.selectedsLength()==1) self.editor.showElement(this.getFirstSelected());
    else self.editor.fillWithDummyAttributes();
  }
  
 /*****************************************************************************
  * Retorna o container que cont�m o elemento
  *
  * <p>Retorna o container mais interno (hierarquicamente) do qual o elemento
  * alvo esteja dentro (espacialmente, n�o pela hierarquia).</p>
  * 
  * @param Element element Elemento alvo
  * @return Container Container que cont�m o elemento
  *****************************************************************************/
  this.getContainerElement = function(element){
    var msElementType = element.elementType.toLowerCase();
    var container,child,count,changed;
    if(this.dialog.contain(element)) container = this.dialog;
    else return null;
    do{
      changed = false;
      for(child in container.children){
        if(container.children[child].children!=undefined && container.children[child].contain(element)){
          container = container.children[child];
          changed = true;
          break;
        }
      }
    }while(changed);
    while(!container.isAllowedChildType(msElementType)){
      if(container.parent==null){
        return null;
      }
      container = container.parent;
    }
    return container;
  }
  
 /*****************************************************************************
  * Retorna o elemento que cont�m o ponto dado
  *
  * <p>Retorna o elemento mais interno (hierarquicamente) que contenha o ponto
  * dado.</p>
  * 
  * @param integer x Coordenada x do ponto
  * @param integer y Coordenada y do ponto
  * @return Element Elemento que cont�m o ponto
  *****************************************************************************/
  this.getElementAt = function(x,y){
    var element,childName,changed;
    if(this.dialog.colideWith(x,y)) element = this.dialog;
    else return null;
    do{
      changed = false;
      for(childName in element.children){
        if(element.children[childName].colideWith(x,y)){
          element = element.children[childName];
          changed = true;
          break;
        }
      }
    }while(changed);
    return element;
  }
  
 /*****************************************************************************
  * Atualiza a hieraquia entre os elementos
  *
  * <p>Atualiza a hieraquia entre os elementos, supondo que o(s) elemento(s)
  * selecionado(s) tenha(m) sido movido(s) ou redimensionado. S� reposiciona
  * dentro da hierarquia os elementos selecionados.</p>
  *****************************************************************************/
  this.updateHierarchy = function(){
    var i,j,element,newParent;
    for(i=this.selecteds.length;i>=0;i--){
      for(j in this.selecteds[i]){
        if(j!='dialog' && !this.ancestralIsSelected(j)){
          element = this.getElement(this.selecteds[i][j]);
          newParent = this.getContainerElement(element);
          if(newParent!=element.parent){
            this.unselect(element);
            if(element.parent!=null) element.parent.deattachChild(element);
            if(newParent!=null) newParent.attachChild(element);
            this.select(element);
          }
        }
      }
    }
  }
  
 /*****************************************************************************
  * Testa se algum elemento especial colide com um determinado ponto ou
  * ret�ngulo
  *
  * <p>Testa se algum elemento especial colide com o ret�ngulo definido por
  * (x1,y1) e (x2,y2). Se x2 e y2 forem omitidos, testa colis�o com o ponto
  * (x1,y1)</p>
  * 
  * @param integer x1 Coordenada x inicial do ret�ngulo.
  * @param integer y1 Coordenada y inicial do ret�ngulo.
  * @param integer x2 Coordenada x final do ret�ngulo. [opcional]
  * @param integer y2 Coordenada y final do ret�ngulo. [opcional]
  * @return boolean Indica se algum elemento especial colide como o
  *         ret�ngulo/ponto.
  *****************************************************************************/
  this.colideWithSpecial = function(x1,y1,x2,y2){
    if(glass_is_active) return true;
    for(elementName in this.specialElements){
      if(this.specialElements[elementName].visible){
        if(this.specialElements[elementName].colideWith(x1,y1,x2,y2)) return true;
      }
    }
    return false;
  }
  
 /*****************************************************************************
  * Alterna entre mostrar e n�o mostrar um determinado elemento especial
  *
  * <p>Alterna entre mostrar e n�o mostrar um determinado elemento especial,
  * ou seja, se ele est� vis�vel, fica invis�vel e vice-versa.
  * </p>
  * 
  * @param String elementName Nome do elemento especial
  *****************************************************************************/
  this.showHideSpecial = function(elementName){
    var specialElement = this.specialElements[elementName];
    if(specialElement==undefined){
      alert('O elemento especial '+elementName+' n�o existe.');
    }else{
      if(specialElement.visible) specialElement.hide();
      else specialElement.show();
      if(this.isIE){
        var element;
        var visibility = (specialElement.visible?'hidden':'visible');
        var top = parseInt(specialElement.getAttribute('top'));
        var left = parseInt(specialElement.getAttribute('left'));
        var bottom = top+parseInt(specialElement.getAttribute('height'));
        var right = left+parseInt(specialElement.getAttribute('width'));
        for(elementName in this.elements){
          element = this.elements[elementName];
          if(element.elementType=='Select' && element.colideWith(left,top,right,bottom)){
            gebi(element.id).style.visibility = visibility;
          }
        }
      }
    }
  }
  
 /*****************************************************************************
  * Limpa a FID, eliminando todos elementos, exceto a dialog
  *
  * <p>Limpa a FID, eliminando todos elementos, exceto a dialog, e resetando os
  * contadores de tipos de elementos.</p>
  *****************************************************************************/
  this.clear = function(){
    this.unselectAll();
    element = this.dialog;
    for(var childName in element.children) this.deleteElement(element.children[childName]);
    for(var typeName in this.elementTypes) this.elementTypes[typeName].counter = 0;
  }
  
 /*****************************************************************************
  * Testa se algum dos elementos especiais capturou o evento de teclado
  *
  * <p>Testa se algum dos elementos especiais j� capturou o evento de teclado,
  * o que significa que ele n�o deve ser tratado pela FID.</p>
  * 
  * @return boolean Indica se o evento foi capturado
  *****************************************************************************/
  this.keyboardEventCaptured = function(){
    for(elementName in this.specialElements){
      if(this.specialElements[elementName].keyboardEventCaptured){
        this.specialElements[elementName].keyboardEventCaptured = false;
        return true;
      }
    }
    return false;
  }
  
 /*****************************************************************************
  * Testa se algum dos elementos especiais capturou o evento de mouse
  *
  * <p>Testa se algum dos elementos especiais j� capturou o evento de mouse,
  * o que significa que ele n�o deve ser tratado pela FID.</p>
  * 
  * @return boolean Indica se o evento foi capturado
  *****************************************************************************/
  this.mouseEventCaptured = function(){
    for(elementName in this.specialElements){
      if(this.specialElements[elementName].mouseEventCaptured){
        this.specialElements[elementName].mouseEventCaptured = false;
        return true;
      }
    }
    return false;
  }
  
 /*****************************************************************************
  * Tratador de eventos de mouse
  *
  * <p>Trata eventos de mouse</p>
  * 
  * @param MouseEvent e Evento de mouse a ser tratado
  *****************************************************************************/
  function mouseHandler(e){
    if(e.button==1) return; // ignora eventos com bot�o do meio
    if(e.button==2){ // bot�o direito deseleciona tudo
      self.unselectAll();
      return;
    }
    var body = document.getElementsByTagName('body').item(0);
    // Desconsidera eventos de mousedown e mousemove sobre as barras de rolagem
    if(e.type!='mouseup' && (e.clientY > body.clientHeight || e.clientX > body.clientWidth)) return;
    // Corrige a posi��o do cursor levando em considera��o a rolagem, ou seja,
    // considera a posi��o absoluta, n�o a posi��o relativa � parte vis�vel.
    e.clientX+= body.scrollLeft;
    e.clientY+= body.scrollTop;
    
    if(e.type=='mouseup'){
      var element = self.getElementAt(e.clientX,e.clientY);
      if(element!=null && element==self.elementClicked){
        if(self.isSelected(element)){
          if(e.ctrlKey){
            self.unselect(element);
          }else{
            self.unselectAll();
            self.select(element);
          }
        }else{
          if(!e.ctrlKey) self.unselectAll();
          self.select(element);
        }
        self.updateEditor();
      }else if(self.creatingElement){
        self.creatingElement = false;
      }
      self.elementClicked = null;
      self.stopSelect(e);
      self.stopDrag(e);
      self.stopResize(e);
    }else if(e.type=='mousedown'){
      if(self.mouseEventCaptured()) return;
      if(self.colideWithSpecial(e.clientX,e.clientY)) return;
      if(self.selectedType!=''){ //--------------> Criando elemento
        var left = self.alignToGrid(e.clientX);
        var top = self.alignToGrid(e.clientY);
        var parent = self.getElementAt(left,top);
        if(parent){
          if(!parent.children) parent = parent.parent;
          var newElement = self.createElement(self.selectedType,left,top);
          var parentTop = parseInt(parent.getAttribute('top'));
          var parentLeft = parseInt(parent.getAttribute('left'));
          var parentHeight = parseInt(parent.getAttribute('height'));
          var parentWidth = parseInt(parent.getAttribute('width'));
          newElement.fitIn(parentLeft,parentTop,parentLeft+parentWidth,parentTop+parentHeight);
          self.unselectAll();
          self.select(newElement);
          self.updateHierarchy();
          self.selectedType = '';
          self.creatingElement = true;
          self.setCursorStyle('');
          self.hideGlass();
        }
      }else{
        var element = self.getElementAt(e.clientX,e.clientY);
        if(element==null){
          self.startSelect(e);
        }else{
          self.elementClicked = element;
          var position = element.positionOf(e.clientX,e.clientY);
          if(self.isSelected(element)){
            if(self.selectedsLength()==1 && position!='center'){
              self.startResize(e,element,position);
            }else{
              self.startDrag(e);
            }
          }else{
            self.startSelect(e);
          }
        }
      }
      self.updateEditor();
    }else{ // mousemove
      self.elementClicked = null;
      if(self.dragging){
        self.updateDrag(e);
      }else if(self.resizing){
        self.updateResize(e);
      }else if(self.selecting){
        self.updateSelect(e);
      }else if(self.selectedType!=''){
        if(self.getElementAt(e.clientX,e.clientY)) self.setCursorStyle('crosshair');
        else self.setCursorStyle('not-allowed');
      }else{
        var element = self.getElementAt(e.clientX,e.clientY);
        if(element && self.selectedsLength()==1 && self.isSelected(element)){
          var position = element.positionOf(e.clientX,e.clientY);
          if(position=='center') self.setCursorStyle('');
          else self.setCursorStyle(position+'-resize');
        }else{
          self.setCursorStyle('');
        }
      }
    }
  }
  
 /*****************************************************************************
  * Tratador de eventos de teclado
  *
  * <p>Trata eventos de teclado.</p>
  * 
  * @param KeyboardEvent e Evento de teclado a ser tratado
  *****************************************************************************/
  function keyboardHandler(e){
    if(!self.keyboardEventCaptured()){
      if(e.type=='keydown'){
        if(e.keyCode==46){ // delete
          self.deleteSelected();
          self.updateEditor();
        }else if(e.altKey && e.ctrlKey){
          switch(e.keyCode){
            case  37: self.alignSelecteds('left');   break; // seta para a esquerda
            case  38: self.alignSelecteds('top');    break; // seta para cima
            case  39: self.alignSelecteds('right');  break; // seta para a direita
            case  40: self.alignSelecteds('bottom'); break; // seta para baixo
            case  61:
            case 187: self.equalSelectedsSize();     break; // caractere '='
            case  65:
              self.alignSelectedsToGrid();
              self.updateEditor();
              break;
          }
        }
      }
    }
  }
  
 /*****************************************************************************
  * Mastiga o evento de mouse deixando s� o que interessa e o passa para o
  * m�todo mouseHandler()
  *
  * <p>Mastiga o evento de mouse deixando s� o que interessa e o passa para o
  * m�todo mouseHandler()</p>
  * <p>OBS: Funciona para IE e FF e o evento "mastigado" � igual independente
  * do browser.</p>
  * 
  * @param MouseEvent e Evento de mouse a ser tratado
  *****************************************************************************/
  function rawMouseHandler(e){
    if(e){ // FF
      var button = e.button;
    }else{ // IE
      e = window.event;
      var button = e.button;
      if(button==1) button = 0;
      else if(button==4) button = 1;
    }
    var event = {
      'type': e.type,
      'button': button,
      'ctrlKey': e.ctrlKey,
      'altKey': e.altKey,
      'shiftKey': e.shiftKey,
      'clientX': (e.clientX?e.clientX:e.x),
      'clientY': (e.clientY?e.clientY:e.y),
      'screenX': e.screenX,
      'screenY': e.screenY
    }
    mouseHandler(event);
  }
  
 /*****************************************************************************
  * Mastiga o evento de teclado deixando s� o que interessa e o passa para o
  * m�todo keyboardHandler()
  *
  * <p>Mastiga o evento de teclado deixando s� o que interessa e o passa para o
  * m�todo keyboardHandler()</p>
  * <p>OBS: Funciona para IE e FF e o evento "mastigado" � igual independente
  * do browser.</p>
  * 
  * @param KeyboardEvent e Evento de teclado a ser tratado
  *****************************************************************************/
  function rawKeyboardHandler(e){
    if(!e) e = window.event;
    var event = new Object();
    event.type = e.type;
    event.ctrlKey = e.ctrlKey;
    event.altKey = e.altKey;
    event.shiftKey = e.shiftKey;
    event.keyCode = e.keyCode;
    event.chr = String.fromCharCode(e.keyCode);
    keyboardHandler(event);
  }
  
  function safeMouseHandler(e){
    try{ rawMouseHandler(e); }
    catch(exception){ debugException(exception); }
  }
  
  function safeKeyboardHandler(e){
    try{ rawKeyboardHandler(e); }
    catch(exception){ debugException(exception); }
  }
  
  // Seta os handlers de eventos
  document.onmousemove = safeMouseHandler;
  document.onmouseup = safeMouseHandler;
  document.onmousedown = safeMouseHandler;
  document.onkeyup = safeKeyboardHandler;
  document.onkeydown = safeKeyboardHandler;
  
}