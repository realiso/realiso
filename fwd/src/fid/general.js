/**
 * FWD - FAST WEB DEVELOPMENT
 * 
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/*****************************************************************************
 * Alias para getElementById
 *
 * <p>Alias para getElementById. Retorna um elemento HTML a partir de seu
 * id.</p>
 * 
 * @param String id Id do elemento.
 * @return element Elemento HTML
 *****************************************************************************/
function gebi(id){
   return document.getElementById?document.getElementById(id):null;
}

/*******************************************************************************
 * Classe Attribute. Define um atributo de um elemento
 * 
 * <p>Define um atributo de um elemento. S� serve como uma struct, quem decide
 * como manipular um atributo, � o pr�prio elemento.</p>
 *
 * @package FID
 * @param String name Nome do atributo
 * @param String type Tipo do atributo
 * @param boolean special Indica se o atributo deve receber tratamento especial
 *        na gera��o de c�digo XML
 * @param function setFunc Fun��o que seta o valor do atributo
 * @param function getFunc Fun��o que retorna o valor do atributo
 *******************************************************************************/
function Attribute(name,type,special,setFunc,getFunc,paOptions){
  this.name = name;
  this.type = type;
  this.special = special;
  this.setValue = setFunc;
  this.getValue = getFunc;
  this.caOptions = paOptions;
}

/*******************************************************************************
 * Classe TabIndex.
 * 
 * <p>Essa classe � s� um envelope para um integer para que o TabIndex possa ser
 * passado por refer�ncia.</p>
 *
 * @package FID
 *******************************************************************************/
function TabIndex(){
  this.value = 1;
  this.inc = function(){ this.value++; return this; }
  this.toString = function(){ return String(this.value); }
}

/*******************************************************************************
 * Remove caracteres de espa�o e tabula��o do in�cio e do fim de uma string
 * 
 * <p>Remove caracteres de espa�o e tabula��o do in�cio e do fim de uma string.
 * </p>
 * @param String str String possivelmente com espa�os
 * @return String A String sem os espa�os
 *******************************************************************************/
function trim(str){
  return str.replace(/^[\s\n]+/,'').replace(/[\s\n]+$/,'');
}