<?php
$sys_ref = "../../../isms/src/";
//$sys_ref = "../../../fwd5/src/";
$lib_ref = $sys_ref  . "lib/";
$classes_ref = $sys_ref  . "classes/";
$handlers_ref = $sys_ref  . "handlers/";
$report_ref = $sys_ref  . "report/";
$graphs_ref = $sys_ref  . "graphs/";

include_once $lib_ref . "fwd5_setup.php";
include_once $classes_ref . "ISMSLib.php";
include_once $classes_ref . "ISMSTable.php";
//include_once $classes_ref . "ISMSUser.php";
//include_once $classes_ref . "Context.php";
//include_once $classes_ref . "RMArea.php";
//include_once $classes_ref . "RMProcess.php";
//include_once $classes_ref . "RMAsset.php";
//include_once $classes_ref . "RiskParameters.php";
include_once $classes_ref . "database.php";

$soWebLib = FWDWebLib::getInstance();
$soWebLib->setCSSRef($sys_ref . "lib/");
$soWebLib->setSysRef($sys_ref);
$soWebLib->setGfxRef($sys_ref . "gfx/");
$soWebLib->setSysJsRef($sys_ref. "lib/");

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));

		$moDialog = FWDWebLib::getObject('dialog');
		$maViews = $moDialog->collect();
		foreach ($maViews as $moView){
			if(($moView instanceof FWDDBGrid)||($moView instanceof FWDGrid)){
				$moView->setAttrPopulate('false');
			}
		}
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("preview.xml");

?>