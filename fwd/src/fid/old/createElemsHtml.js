// Cria elemento ( DIV )
function create_object(name,type,dontResizeAttach) {
    if(!name) name = getNumber();
    if(myElems.contain(name)) {
       alert('J� existe um elemento com este nome');
       return false;
    }
    newDiv = document.createElement("div");
    newDiv.id = name;
    newDiv.name = name;
    newDiv.className = ((dontResizeAttach)?"":"resizeMe");
    newDiv.style.height = gebi("height").value + "px";
    newDiv.style.width =  gebi("width").value + "px";
    newDiv.style.cursor = "s-resize";
    newDiv.style.position = "absolute";
//    newDiv.style.overflow = "hidden";
    newDiv.style.left = gebi("posleft").value + "px";
    newDiv.style.top =  gebi("postop").value  + "px";
    newDiv.style.border = gebi("color_border").value + " 3px solid";
	newDiv.style.backgroundColor = gebi("color_div").value;
	gebi("color_div").value = "";
	// Anexa "movedor"
    movedor = document.createElement("div");
    movedor.id = "movedor";
    movedor.style.cursor = "move";
    movedor.style.backgroundColor = "red";
    movedor.style.width = "10px";
    movedor.style.height = "1px";
    movedor.style.left = "-20px";
    movedor.style.position = "relative";
    movedor.style.border = "";
    movedor.onmousedown=function() {
            dragStart(event, name);
         }
    newDiv.appendChild(movedor);
	// Anexa elemento html 
    if (type) {
    	type = ((typeof(type) == "object")?type.options[type.selectedIndex].value:type);	
    	newElem = create_element(type,name);
    	if (!newElem) 
    		return false;
    	newDiv.appendChild(newElem);
    }
    if(!dontResizeAttach) 
        myElems.newElem(newDiv);
    ac = document.getElementById("DivCont1");
    ac.parentNode.insertBefore(newDiv,ac);	
    return  gebi(name);

}

// Cria elemento ( HTML )
function create_element (type,name){
	switch (type){
		case "radiobox":
			newElem = document.createElement("input");
			newElem.type = "radio";
			newElem.name = gebi("name_radio").value;
			newElem.value = gebi("value_radio").value;
			break;
		case "checkbox":
			newElem = document.createElement("input");
			newElem.type = type;
			newElem.name = gebi("name_checkbox").value;
			newElem.value = gebi("value_checkbox").value;
			break;
		case "text":
			newElem = document.createElement("input");
			newElem.type = type;
			value = gebi("width").value - 10;
			newElem.style.width =  value + "px";
			newElem.value = gebi("value_text").value;
			break;
		case "select":
			newElem = document.createElement(type);
			value = gebi("width").value - 10;
			newElem.style.width =  value + "px";
			break;
		case "memo": 
			newElem = document.createElement("textarea");
			if (gebi("cols").value == "" || isNaN(gebi("cols").value))
				 gebi("cols").value = 1;
			if (gebi("rows").value == "" || isNaN(gebi("rows").value))
				 gebi("rows").value = 1;	 
			newElem.cols = gebi("cols").value ;
			newElem.rows = gebi("rows").value;
			break;
		case "box":
			newElem = document.createElement("div");
			break;
		case "static":
			newElem = document.createElement("span");
			if (gebi("color_font").value)
				newElem.style.color = gebi("color_font").value;
			newElem.innerHTML = gebi("value_static").value;
			break;
		case "img":
			newElem = document.createElement(type);
			newElem.src = gebi("end_image").value;	 
			break;
		case "password":
			newElem = document.createElement("input");
			newElem.type = type;
			value = gebi("width").value - 10;
			newElem.style.width =  value + "px";
			break;
		case "file":
			newElem = document.createElement("input");
			newElem.type = type;
			newElem.style.border = "1px solid";
			newElem.style.font = "12px verdana, arial, helvetica, sans-serif";
			elem_width = gebi("width").value - 10;
			newElem.style.width =  elem_width + "px";
			break;
		case "button":
			newElem = document.createElement("input");
			newElem.type = "button";
			value = gebi("value_button").value;
			color = gebi("color_button").value;
			newElem.value = value;
			newElem.style.border = "1px solid";
			newElem.style.font = "12px verdana, arial, helvetica, sans-serif";
			if (color)
				newElem.style.background = color;
			elem_width = gebi("width").value - 10;
			newElem.style.width =  elem_width + "px";
			break;	
		default:
			return false;
			break;	
	}
	if (!newElem.name)
		newElem.name = name;	
	newElem.id = "elem_" + name;
	newElem.style.position = "absolute";
	newElem.style.left = "1px";
	newElem.style.top = "1px";
	newElem.className = "";
	newElem.style.cursor = "";
	return newElem;		
}

function createConfig(){
	var edit = create_object("CONFIG","","TRUE");
	edit.style.cursor = "none";
	edit.style.borderWidth = "1px";
	edit.style.zIndex = "1";
	edit.style.left = "1050px";
	edit.style.width = "210px";
	edit.style.top = "30px";
	edit.appendChild(gebi("edit01"));
	edit.style.backgroundColor = "ivory";
}

function createEdit(){
	var edit = create_object("EDIT","","TRUE");
	edit.style.cursor = "none";
	edit.style.borderWidth = "1px";
	edit.style.zIndex = "1";
	edit.style.left = "990px";
	edit.style.width = "150px";
	edit.style.top = "30px";
	edit.style.height = "60px";
	edit.style.display = "none";
	edit.appendChild(gebi("edit03"));
	gebi("save_edit").style.border = "1px solid";
	gebi("save_edit").style.font = "11px verdana, arial, helvetica, sans-serif";
	gebi("close_edit").style.border = "1px solid";
	gebi("close_edit").style.font = "11px verdana, arial, helvetica, sans-serif";
	gebi("edit03").style.display = "block";
	edit.style.backgroundColor = "ivory";
}

function createTextarea(id){
	value = "";
	id_textarea = "textarea_"+id;
    area = gebi(id_textarea);
    //cria div na qual textarea sera inserido
	elem = create_object(id,"","TRUE");
	elem.style.cursor = "none";
	elem.style.borderWidth = "1px";
	elem.style.left = "880px";
	elem.style.width = "375px";
	elem.style.zIndex = "1";
	elem.style.height = "430px";
	elem.style.top = "340px";
	elem.style.display = "none";
	elem.appendChild(area);
	area.style.position = "absolute";
	area.style.left = "4px";
	area.style.top = "4px";
	area.style.display = "block";
	elem.style.backgroundColor = "ivory";
	ac = gebi("DivCont1");
	ac.parentNode.insertBefore(elem,ac);
	// cria label
	label_type = document.createElement("DIV");
	label_type.id = "label_textarea";
	id = id.toUpperCase();
	label_type.innerHTML = id;
	label_type.style.height = "20px";
	label_type.style.left = "150px";
	label_type.style.top = "-20px";
	label_type.style.position = "absolute";
	elem.appendChild(label_type);
	//criar bot�o make elems
	if (id=="XML"){
		buttonCode = document.createElement("input");
		buttonCode.type = "button";
		buttonCode.style.width = "115px";
		buttonCode.style.left = "260px";
		buttonCode.style.top = "430px";
		buttonCode.style.position = "absolute";
		buttonCode.value = "LOAD XML";
		buttonCode.style.border = "1px solid";
		buttonCode.style.font = "12px verdana, arial, helvetica, sans-serif";
		elem.appendChild(buttonCode);
		buttonCode.onclick = function() {
			loadXml(); 
		}
	}	
}

function create_object_from_xml(object,type) {
	name = getAttr(object,"name");
	if (getAttr(object,"password"))
		type = "password";
	newDiv = create_object(name,type);
	if(type!="box")
		object = object.parentNode;
	newDiv.style.position = "absolute";
	newDiv.style.height = getAttr(object,"height") + "px";
    newDiv.style.width =  getAttr(object,"width") + "px";
    newDiv.style.cursor = "s-resize";
    newDiv.style.left = getAttr(object,"left") + "px";
    newDiv.style.top =  getAttr(object,"top")  + "px";
    newDiv.style.border = "black 3px solid";
	newDiv.style.backgroundColor = getAttr(object,"backgroundColor");
	myElems.updateParent(newDiv);
	myElems.checkInnerOuter(newDiv);
	if (name!="dialog"){
		elem = gebi(getAttr(object.parentNode,"name"));
		elem.appendChild(newDiv);
	}
}
