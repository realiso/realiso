// gera c�digo XML
function getXml(object,prefix){
	var value = "";
	var box="";
	tag = object.tagName;
	name = object.name;
	x_screen = formatAttr(myElems[object.parentNode.id].x);
	y_screen = formatAttr(myElems[object.parentNode.id].y);
	elem_height = formatAttr(object.parentNode.style.height);
	elem_width = formatAttr(object.parentNode.style.width);
	// cria��o da tag "box" para todas as tags (exceto para a pr�pia "box")
	if (tag!="DIV"){
		box ="box";
		box+=" name='box_"+name+"'";
		box+=" id='box_"+name+"'";
		box+=" left='"+x_screen+"'";
		box+=" top='"+y_screen+"'";
		box+=" height='"+elem_height+"'";
		box+=" width='"+elem_width+"'";
		value = "<"+box+">\n"+prefix+"   "+"<";
	}		
	switch (tag){
		case "DIV":
			value+=((name=="dialog")?"<dialog":"<box");
			value+=" height='"+elem_height+"'";
			value+=" width='"+elem_width+"'";
			value+=" left='"+x_screen+"'";
			value+=" top='"+y_screen+"'";
			if (object.parentNode.style.backgroundColor!="")
				value+=" backgroundColor='" + object.parentNode.style.backgroundColor+"'";
			break;
		case "SPAN":
			value+="static";
			break;
		case "IMG":
			value+="img";
			value+=" src='"+object.src+"'";
			break;
		case "TEXTAREA":
			value+="memo";
			value+=" cols='"+object.cols+"'";
			value+=" rows='"+object.rows+"'";
			break;
		case "INPUT": 
			if (object.type=="radio")
				value += "radiobox";
			if (object.type=="checkbox")
				value += "checkbox";	
			if (object.type=="text")
				value += "text";
			if (object.type=="password"){
				value += "text";
				value += " password='true'";
			}
			if (object.type=="file")
				value += "file";
			if (object.type=="button")
				value += "button";
			break;
		case "SELECT":
			value+= "select";
			value+= " size='"+formatAttr(object.style.width)+"'";				
			break;
	}
	value+=" name='"+name+"'";
	value+=" id='"+name+"'";
	value+= ">";
	if (tag == "INPUT" && (object.type=="button" || object.type=="text" || object.type=="password")){
		value += object.value;
		return value;
	}
	if (tag == "SPAN")
		value+=object.innerHTML;
	value+="\n";
	return value;	
}
  
// gera c�digo HTML
function getCodeHtml(object){
	var value = "";
	tag = object.tagName;
	x_screen = myElems[object.parentNode.id].xscreen;
	y_screen = myElems[object.parentNode.id].yscreen;
	position = "absolute";
	style = " style ='position:"+position+";left:"+x_screen+";top:"+y_screen+";";
	elem_height = object.parentNode.style.height;
	elem_height = elem_height.substr(0,elem_height.lastIndexOf("p"));  
	elem_width = object.parentNode.style.width;
	elem_width = elem_width.substr(0,elem_width.lastIndexOf("p"));  
	value+="<"+tag;
	value+=" name='"+object.name+"'";
	switch (tag){
		case "DIV":
			style+="height:"+elem_height+";";
			style+="width:"+elem_width+";";
			style+="background-color:" + object.parentNode.style.backgroundColor+";";
			break;
		case "SPAN":
			style+="height:"+elem_height+";";
			style+="width:"+elem_width+";";
			style+="color"+object.style.color;
			break;
		case "IMG":
			value+=" src='"+object.src+"'";
			break;
		case "TEXTAREA":
			value+=" cols='"+object.cols+"'";
			value+=" rows='"+object.rows+"'";
			break;
		case "INPUT":
			value+=" type="+object.type;
			if (object.type!="text"||object.type!="file")
				value+=" value='"+object.value+"'";
			if (object.type=="text")
				style+= "width:"+object.style.width+";";
			if (object.type=="file"||object.type=="button")
				style+= "border:"+ object.style.border+";font:"+object.style.font+";width:"+object.style.width+";";
			color = object.style.backgroundColor;
			if (color)
				style+= "background:"+color+";";
			break;
		case "SELECT":
			value+="SELECT";
			style+= " width:"+object.style.width+";";				
			break;
	}
	value+=style;
	value+= "'>";
	if (tag=="SPAN")
		value+=object.innerText;
	return value;	
}

// gera c�digo (HTML ou XML) para ser imprimido no textarea respectivo  
function getCode(elem, value, index, type){
    var j = "";
    var prefix = "";
    var value = "";
    if (!elem)
    	return value;
	for(i=0;i<index;i++)
		prefix += "   ";
	value += prefix;
	value += ( (type == "HTML")? getCodeHtml(elem.childNodes[1]) : getXml(elem.childNodes[1],prefix) );
	index++;
    //percorre todos os filhos ( se existirem )
    for (j=2; j<=elem.childNodes.length; j++)
	   	if (elem.childNodes[j])
			value += getCode(elem.childNodes[j],value,index,type)
	// fecha as tags
	if(type!="HTML"){
		switch (elem.childNodes[1].tagName){
			case "DIV":
				tag = ((elem.childNodes[1].name=="dialog")?"dialog":"box")	
				break;
			case "SPAN":
				tag = "static";
				break;
			case "IMG":
				tag = "img";
				break;
			case "TEXTAREA":
				tag = "memo";
				break;
			case "INPUT":
				if (elem.childNodes[1].type=="text" || elem.childNodes[1].type=="password")
					tag = "text";
				if (elem.childNodes[1].type=="file")
					tag = "file";
				if (elem.childNodes[1].type=="button")
					tag = "button";
				if (elem.childNodes[1].type=="radio")
					tag = "radiobox";
				if (elem.childNodes[1].type=="checkbox")
					tag = "checkbox";
				break;
			case "SELECT":
				tag = "select";
				break;
		}
		value+=((elem.childNodes[1].tagName!="DIV")?prefix+"   ":prefix);
		value += "</"+tag+">" ;
		if (elem.childNodes[1].tagName!="DIV")
			value+= "\n"+prefix+"</box>";
	}		
	else
		value += prefix+"</"+elem.childNodes[1].tagName+">" ;
	value += "\n";
	return value;
}

// mostra campos de configura��o
function show_config(index,id) {
	myElems.hide_children(id);
	if (index!= "nothing")
		gebi("elem_"+index).style.display = "block";
	return;
}

// mostra campos de edi��o do elemento
function showEdit(id){
	// verifica se elemento � edit�vel
	if  (id=="CONFIG" || id=="xml" || id=="EDIT" || id=="html" )
		return;
	closeEdit();
	gebi("CONFIG").style.display = "none";
	gebi("EDIT").style.display = "block";
	elem_id = "elem_"+id;
	gebi("edit_name").value = gebi(elem_id).name;
	gebi("id").value = gebi(id).id;
	tag = gebi(elem_id).tagName;
	type = gebi(elem_id).type;
	switch  (tag){
		case "INPUT":
			if(type != "text" && type != "file" && type != "password"){
				gebi("editValue").style.display = "block";
				gebi("edit_value").value = gebi(elem_id).value;
			}
			if(type == "button"){
				gebi("editColorButton").style.display = "block";
				gebi("edit_color_button").value = gebi(elem_id).style.background;
			}
			break;
		case "SPAN":
			gebi("editValue").style.display = "block";
			gebi("editColorFont").style.display = "block";
			gebi("edit_value").value = gebi(elem_id).innerText;
			gebi("edit_color_font").value = gebi(elem_id).style.color;
			break;
		case "TEXTAREA":
			gebi("editCols").style.display = "block";		
			gebi("editRows").style.display = "block";
			gebi("edit_cols").value = gebi(elem_id).cols;
			gebi("edit_rows").value = gebi(elem_id).rows;
			break;
		case "IMG":
			gebi("editSrc").style.display = "block";	
			gebi("edit_src").value = gebi(elem_id).src;
			break;
		case "DIV":
			gebi("editColorDiv").style.display = "block";
			gebi("edit_color_div").value = gebi(elem_id).parentNode.style.backgroundColor;
			break;
	}	
}

function saveEdit(){
	elem_id = "elem_"+gebi("id").value;
	gebi(elem_id).name = gebi("edit_name").value;
	elem_value = gebi("edit_value").value;
	tag = gebi(elem_id).tagName;
	switch  (tag){
		case "INPUT":
			type = gebi(elem_id).type;
			if (elem_value != "")
				gebi(elem_id).value = elem_value;
			if (type == "submit")
				gebi(elem_id).style.background = gebi("edit_color_button").value;
			break;
		case "SPAN":
			gebi(elem_id).style.color = gebi("edit_color_font").value;
			gebi(elem_id).innerHTML = elem_value;
			break;
		case "TEXTAREA":	
			gebi(elem_id).cols = gebi("edit_cols").value;
			gebi(elem_id).rows = gebi("edit_rows").value
			break;
		case "IMG":
			gebi(elem_id).src = gebi("edit_src").value;	
			break;
		case "DIV":
			gebi(elem_id).parentNode.style.backgroundColor = gebi("edit_color_div").value;	
	}	
	closeEdit();
}

function closeEdit(){
	gebi("edit_name").value = "";
	gebi('EDIT').style.display = 'none';
	gebi("edit_value").value = "";
	gebi("editValue").style.display = "none";
	gebi("editColorFont").style.display = "none";
	gebi("editColorButton").style.display = "none";
	gebi("editCols").style.display = "none";		
	gebi("editRows").style.display = "none";
	gebi("editSrc").style.display = "none";
	gebi("editColorDiv").style.display = "none";
	gebi('CONFIG').style.display = 'block';
}

// cria elemento html
function makeHtml(object){
	tag = object.tagName;
	if(!tag)
		tag = "NO TAG";
	tag = tag.toLowerCase();	
	switch(tag){
		case "box":
			if (object.childNodes.length != 0 && object.childNodes.item(0).tagName){
				value = object.childNodes.item(0).tagName;
				value = value.toLowerCase();
			}
			if(object.childNodes.length==0 || value==tag)
				create_object_from_xml(object,tag);
			break;
		case "dialog":	
				create_object_from_xml(object,"box");
			break;
		case "no tag":
			elem = getAttr(object.parentNode,"name");
			tagParent = object.parentNode.tagName;
			tagParent = tagParent.toLowerCase();
			value = trim(object.nodeValue);
			if(tagParent=="button"||tagParent=="text")
				gebi('elem_'+elem).value= value;
			if(tagParent=="static")
				gebi('elem_'+elem).innerHTML = value;
			break;
		default:
			// verifica se tag do elemento � tag v�lida
			if (!containTag(tag))
				return false;
			create_object_from_xml(object,tag);
			break;	
	}	
}

function makeElems(object){
	var i;
	var children;
	makeHtml(object);
	children = object.childNodes.length;
	children--;
	//percorre todos filhos
	if (children!=-1)
		for(i=0;i<=children;i++)
			makeElems(object.childNodes.item(i));
}

// carrega xml e cria elementos html
function loadXml(){
	value = gebi("textarea_XML").value;
	if (value=="" || !confirm('Tem certeza que deseja carregar o xml? \n\nObs: todos elementos que foram criados, at� o momento, ser�o exclu�dos!')){
		return;}
	var xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
	xmlDoc.async = false;
	xmlDoc.loadXML(value);
	root = xmlDoc.childNodes.item(0).tagName;
	root = root.toLowerCase();
	if(root!="dialog"){
		alert('First element must be "dialog" !');
		return;
	}	
	if (xmlDoc.parseError.errorCode != 0) {
   		var myErr = xmlDoc.parseError;
   		alert("You have error " + myErr.reason);
	} 
	else{
		deleteAllElems();
		makeElems(xmlDoc.childNodes.item(0));
	}
}

