/* myElems -- Classe que armazena valores dos objetos da tela */
var myElems = new Array();
    myElems.lastObj = null;
    myElems.contain = function(id) {
        if( ((typeof this[id]) == 'object') && gebi(id))
           return true;
        else
           return false;
    }
    
    myElems.newElem = function(obj) {
    	var id;
        id = obj.id;
        elem = new Object();
        myElems[id]= elem;
        myElems[id].x = obj.style.left;
        myElems[id].y = obj.style.top;
        myElems[id].h = obj.style.height;
        myElems[id].w = obj.style.width;
        myElems[id].z = obj.style.zIndex;	
        myElems[id].xscreen = obj.style.left;
        myElems[id].yscreen = obj.style.top;
        myElems[id].attached = false;
        myElems[id].selected = false;
        myElems[id].parent = 0;
        myElems[id].hierarchy = 0;
    }
    
    myElems.updateParent = function(obj) {
        var id = obj.id;
        if(this.contain(id)) {
            prnt = this[id].parent;
            this[id].x = obj.style.left;
            this[id].y = obj.style.top;
        	this[id].h = obj.style.height;
            this[id].w = obj.style.width;
            this[id].z = obj.style.zIndex
            if(this[id].attached) {
               x = oper_pixel("+",this[prnt].xscreen,this[id].x);
               y = oper_pixel("+",this[prnt].yscreen,this[id].y);
               dif_x = oper_pixel("-",x,this[id].xscreen);
               dif_y = oper_pixel("-",y,this[id].yscreen);
               this[id].xscreen = x + "px";
               this[id].yscreen = y + "px";
            }
            else {
               dif_x = oper_pixel("-",this[id].x,this[id].xscreen);
               dif_y = oper_pixel("-",this[id].y,this[id].yscreen);
               this[id].xscreen = this[id].x;
               this[id].yscreen = this[id].y;
            }
            if (this[id].selected)
	        	mySelecteds.update(dif_x, dif_y,id);
        }
    }
    
    myElems.updateParentChildren = function(obj) {
        var id = obj.id;
        if(this.contain(id)) {
        	this.updateParent(obj);        	
    		for(elem in this) {
	            if( id == elem || !gebi(elem) || this[elem].parent==0 )
               		continue;
            	if( this[elem].parent == id ) 
            		this.updateParentChildren(gebi(elem));
             }
         }    
    }
    
    myElems.getObjString = function(obj) {
        var id = obj.id;
        if(this.contain(id)) {
          	this.lastObj = id;
          	var text = gebi(id).name + " | ";
            text += this[id].x + " | ";
            text += this[id].y + " | ";
            text += this[id].w + " | ";
            text += this[id].h + " | ";
            text += this[id].z + " | ";
            text += this[id].xscreen + " | ";
            text += this[id].yscreen + " | ";
            text += "\n";
            return text;
        }
        return "";
    }
	
	//esconde borda dos elementos
	myElems.hideBorder = function() {
		if (stateBorder == "visible"){
			border = "";
			stateBorder = "hidden";
		}
		else{
			border = gebi("color_border").value + " 3px solid";
			stateBorder = "visible";
		}
		for(elem in this) {
			if(!this.contain(elem)) 
		    	continue;
			gebi(elem).style.border = border;
		}
	}
	
	//esconde "movedor"	dos elementos	
	myElems.hideMover = function() {
		mover = ((stateMover=="visible")?"hidden":"visible");
		stateMover = mover;
		for(elem in this) {
			if(!this.contain(elem)) 
		    	continue;
			gebi(elem).firstChild.style.visibility = mover;
		}
	}		
		
    //"esconde" todos elementos anexados
	myElems.hide_children = function(id, exception){
		children = gebi(id).childNodes;
		for(elem in children)
			if (elem != "length" && gebi(id).childNodes[elem].id!=exception) 
				gebi(id).childNodes[elem].style.display = "none";
	}
		
    myElems.getAllString = function() {
        texto = "Identificador | Left | Top | Largura | Altura | Z-Index | Left (absolute) | Top (absolute)\n";
        for(elem in this) {
            if(!this.contain(elem)) {
              continue;
            }
            texto += this.getObjString(gebi(elem));
        }
        alert(texto);
    }

    myElems.deleteCheckeds = function() {
        for(elem in this) {
            // Se n�o for um elemento v�lido, nem estiver checado, pula
            if(!gebi(elem) || !this[elem].selected) 
              continue;
            gebi(elem).parentNode.removeChild(gebi(elem));
            this[elem] = null;
        }
    }

	myElems.deleteAllElems = function() {
        for(elem in this) {
            // Se n�o for um elemento v�lido, pula
            if(!gebi(elem)) 
              continue;
            gebi(elem).parentNode.removeChild(gebi(elem));
            this[elem] = null;
        }
    }
	
	myElems.editCheckeds = function() {
		i = 0;
		for (elem in this){
			if(!gebi(elem) || !this[elem].selected) 
              continue;
			createEdit(elem,i);
			i++;
		}
	}

    myElems.searchElemToInner = function(id, elem){
    	max = this[elem].hierarchy;
    	aux = elem;
    	 for(elem in this) {
    	    if( id == elem || !gebi(elem) || this[id].parent==elem ) 
               continue;
    	 	x1 = this[elem].xscreen;
	        y1 = this[elem].yscreen;
	        x2 = oper_pixel("+",this[elem].xscreen,this[elem].w);
	        y2 = oper_pixel("+",this[elem].yscreen,this[elem].h);
    	  	if( oper_pixel(">",this[id].xscreen,x1)  && oper_pixel(">",this[id].yscreen,y1) &&
                oper_pixel("<",this[id].xscreen,x2)  && oper_pixel("<",this[id].yscreen,y2) && oper_pixel(">",this[elem].hierarchy,max)){
 					max = this[elem].hierarchy;
 					aux = elem;               
             }   
         }
         return aux;
    }            
    
    myElems.checkInnerOuter = function(obj){ 
    	var id = obj.id;
    	if (!this.contain(id))
        	return false; 
    	if (!this.checkInner(obj)) 	
 			this.checkOuter(obj);
    }
    
    myElems.checkInner = function(obj) {
        var id = obj.id;
        var parent = this[id].parent;	
        if (parent != 0) {
	        x1parent = this[parent].xscreen;
	        y1parent = this[parent].yscreen;
	        x2parent = oper_pixel("+",this[parent].xscreen,this[parent].w);
	        y2parent = oper_pixel("+",this[parent].yscreen,this[parent].h);
	    }
        for(elem in this) {
            if( id == elem || !gebi(elem) || this[id].parent==elem) 
               continue;
            /*
                  (x1,y1)  _____________
                          |             |
                          |             |
                          |             |
                          |_____________|
                                         (x2,y2)
            */
            x1 = this[elem].xscreen;
            y1 = this[elem].yscreen;	
            x2 = oper_pixel("+",this[elem].xscreen,this[elem].w);
            y2 = oper_pixel("+",this[elem].yscreen,this[elem].h);
            //alert(elem + 'X' + id + '\n' + x1 + " | " +  y1 + " | " + x2 + " | " + y2 + " \n" + this[id].xscreen + " | " + this[id].yscreen);
            // Verifica se � para anexar
            if( oper_pixel(">",this[id].xscreen,x1)  && oper_pixel(">",this[id].yscreen,y1) &&
                oper_pixel("<",this[id].xscreen,x2)  && oper_pixel("<",this[id].yscreen,y2) ) {
               if (this[id].attached && oper_pixel(">",this[id].xscreen,x1parent)&& oper_pixel(">",this[id].yscreen,y1parent) 
               && oper_pixel("<",this[id].xscreen,x2parent)&& oper_pixel("<",this[id].yscreen,y2parent) && 
               oper_pixel(">",this[id].hierarchy,this[elem].hierarchy)) 
	               	  continue;
	           elem = this.searchElemToInner(id, elem);
	           // Anexa div filho em div pai
               this.attach(elem,id);
               this[id].parent = elem; 
               this[id].attached = true;
               return 1;
            }
        }
        return 0;
    }
    
    myElems.checkOuter = function(obj) {
        var id = obj.id;
        for(elem in this) {
            if( id == elem || !gebi(elem) || elem != this[id].parent || !this[id].attached ) 
               continue;
            /*
                  (x1,y1)  _____________
                          |             |
                          |             |
                          |             |
                          |_____________|
                                         (x2,y2)
            */
            x1 = this[elem].x;
            y1 = this[elem].y;
            x2 = oper_pixel("+",this[elem].x,this[elem].w);
            y2 = oper_pixel("+",this[elem].y,this[elem].h);
    	      // Verifica se � para desanexar
    	     if ( oper_pixel("<",this[id].x,0)  || oper_pixel("<",this[id].y,0) ||
                 oper_pixel(">",this[id].x,this[elem].w)  || oper_pixel(">",this[id].y,this[elem].h) ){
               // Desanexa div filho do div pai
               this.detach(elem,id);
               this[id].parent = 0;
               this[id].attached = false; 
               return 1;
            }
        }
        return 0;
    }

    // Anexa div filho em div pai
	myElems.attach = function(parent,child) {
    	elem_parent = gebi(parent);
    	elem_child = gebi(child);
    	elem_parent.appendChild(elem_child);
	    this[child].hierarchy = this[parent].hierarchy + 1; 
	    //Reposiciona div dentro do div pai
	    elem_child.style.left = oper_pixel("-",this[child].xscreen,this[parent].xscreen);
	    this[child].x = elem_child.style.left;
		elem_child.style.top = oper_pixel("-",this[child].yscreen,this[parent].yscreen);
		this[child].y = elem_child.style.top;
	    gebi("status").innerHTML = "<font color='red'> ATTACHED</font>";
	}

// Desanexa div filho do div pai
	myElems.detach = function(parent,child) {
	   elem_child = gebi(child);
	   ac = gebi("DivCont1");
	   ac.parentNode.insertBefore(elem_child,ac)
	   this[child].hierarchy = 0;
	   // Reposiciona div na tela
	   elem_child.style.left = this[child].xscreen;
	   elem_child.style.top = this[child].yscreen;
	   gebi("status").innerHTML = "<font color='green'> DETACHED</font>";
	}