var stateMover = "visible";
var stateBorder = "visible";
var stateAll = "visible";
var elem_edit = new Array();
	elem_edit[0] = "button";
	elem_edit[1] = "help";
	elem_edit[2] = "CONFIG";
var elem_move = "";

/* myTags -- Array que armazena todas as tag*/
var myTags = new Array();
	myTags[0] = "dialog";
	myTags[1] = "box";
	myTags[2] = "static";
	myTags[3] = "button";
	myTags[4] = "text";
	myTags[5] = "select";
	myTags[6] = "radiobox";
	myTags[7] = "checkbox";
	myTags[8] = "memo";
	myTags[9] = "file";
	myTags[10] = "img";
	
if (moz) {
	extendElementModel();
	extendEventObject();
	emulateEventHandlers(["mousemove", "mousedown", "mouseup"]);
}
// verifica se tag est� contida no array myTags (array quey contem todas as tags v�lidas)
function containTag(value){
	for (elem in myTags){
		if (myTags[elem]==value)
			return true;
	}
	return false;
}		

//formata value das tags "TEXT","BUTTON" e "STATIC"
function trim(s) {
  while (s.substring(0,1) == ' ' || s.substring(0,1) == '\n') {
    s = s.substring(1,s.length);
  }
  while (s.substring(s.length-1,s.length) == ' ' || s.substring(s.length-1,s.length) == '\n') {
    s = s.substring(0,s.length-1);
  }
  return s;
}
	
// Pega o timestamp para colocar como identificador do objeto
function getNumber() {
	var date = new Date();
	var number = "";
	number = date.getTime();
	return number;
}

// Alias para getElementById
function gebi(a){
   return document.getElementById?document.getElementById(a):null;
}

function forwardEvent(e,id) {
   if(e.ctrlKey) {
     if(myElems[id].selected) {
        gebi(id).style.borderColor = "black";
        myElems[id].selected = false;
        mySelecteds.removeElem(id);
     }
     else {
        gebi(id).style.borderColor = "blue";
        myElems[id].selected = true;
        if (mySelecteds.checkEmpty())
        	mySelecteds.insertElem(id,0);
        else
        	mySelecteds.insertElem(id);
     }
   }
   if(e.altKey)
		showEdit(id);
}

// Captura eventos do teclado
function keyboardEvent() {
   e = event;
   //alert(e.keyCode);
   switch(e.keyCode) {
      case 45 :  // Insert
           create_object(gebi('name').value,gebi('selectType'));
           break;
      case 46 :  // Delete
           myElems.deleteCheckeds();
           break;
      case 113:  //  Refresh Html
      		refresh();
      		break;     
      case 118:  // Show Html
      		showHideCode("HTML");
      		break;		
      case 119: // Create element "dialog"
      		create_object("dialog","box");
      		break;		
      case 120: // Show Xml
      		showHideCode("XML");
      		break;				
      case 123: // load xml
      		loadXml();
      		break;
      case 109: // aling left
      		mySelecteds.align("top");
      		break;
      case 107: // aling horizontal
      		mySelecteds.align("left");
      		break;
      case 111: // aling horizontal
      		mySelecteds.align("right");
      		break;
      case 194: // aling horizontal
      		mySelecteds.align("down");
      		break;				
      case 106: // equal width and height
      		mySelecteds.equalDimensions();
      		break;		
      default : return true;
   }
}

//Realiza opera��es com valores que contenham extens�o (px,cm,...)
function oper_pixel(oper,x,y) {
   x = parseInt(x);
   y = parseInt(y);
   switch(oper) {
      case "+": return (x+y);
      case "-": return (x-y);
      case "*": return (x*y);
      case "/": return (x/y);
      case ">": return (x>y);
      case "<": return (x<y);
      case "==": return (x==y);
      default : return 0;
   }
   return 0;
}

//Retorna um dos atributos (height, width, lef, top) sem px no fim
function formatAttr(value){
	value = value.substr(0,value.lastIndexOf("p"));  
	return value;
}

function appendChildConfig(){	
	gebi("edit02").appendChild(gebi("elem_radio"));
	gebi("edit02").appendChild(gebi("elem_checkbox"));
	gebi("edit02").appendChild(gebi("elem_select"));
	gebi("edit02").appendChild(gebi("elem_text"));
	gebi("edit02").appendChild(gebi("elem_static"));
	gebi("edit02").appendChild(gebi("elem_div"));
	gebi("edit02").appendChild(gebi("elem_textarea"));
	gebi("edit02").appendChild(gebi("elem_image"));
	gebi("edit02").appendChild(gebi("elem_file"));
	gebi("edit02").appendChild(gebi("elem_button"));
}

//refresh textarea
function refresh() {
	if (!gebi("dialog"))
		return false;
		
	gebi("textarea_html").value = getCode(gebi("dialog"),"",0,"HTML");
	gebi("textarea_xml").value = getCode(gebi("dialog"),"",0,"XML");
}

function showHideCode(value)  {
	var area;
	elem = gebi(value);
	area = gebi("textarea_"+value);
	//oculta xml
	if (elem.style.display == "block")
		elem.style.display = "none"
	//mostra xml
	else{
		elem.style.display = "block";
	}
	return;
}

function showHideAll(){
	if (stateAll=="visible"){
		state = "none"
		stateAll = "not visible"
	}
	else{
		state = "block"
		stateAll = "visible"
	}
	for(i=0;i<=2;i++){
		gebi(elem_edit[i]).style.display = state;
	}
	return;		
}

// Retorna determinado atributo da tag (XML)
function getAttr(object, attribute){
	children = object.attributes.length;
	children--;
	for(i=0;i<=children;i++){
		if (object.attributes(i).name == attribute){
			value = object.attributes(i).value;
			return value;
		}
	}
	return false;
}

//exclui todos elementos
function deleteAllElems(){
	myElems.deleteAllElems();
}
