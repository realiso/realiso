/* mySelecteds -- Array que armazena todas os ids dos elementos selecionados*/
var mySelecteds = new Array();
	
	// verifica se existe algum elemento selecionado
	mySelecteds.checkEmpty = function(){
		for(elem in this)
			if (gebi(this[elem]))
				return false;
		return true;
	}
	
	// insere id no array 
	mySelecteds.insertElem = function(id,position){
		if (position!=0)
			position = getNumber();	
		mySelecteds[position] = id;		
	}
	
	// remove id do array 
	mySelecteds.removeElem = function(id){
		for(elem in this)
			if (this[elem]==id){
				this[elem] = null;
				return;
			}
	}	
	
	// iguala altura e largura dos elemento selecionado 
	mySelecteds.equalDimensions = function(){
		var width;
		var height;
		if (this.checkEmpty())
			return;
		width = gebi(this[0]).style.width;
		height = gebi(this[0]).style.height;
		for(elem in this){
			if (!gebi(this[elem]) || elem == 0)
				continue;
			gebi(this[elem]).style.width = width;
			gebi(this[elem]).style.height = height;
			myElems[this[elem]].h = height;
			myElems[this[elem]].w = width;
		}
	}
	
	// alinha (left, right, top, down) elementos selecionados
	mySelecteds.align = function(value){
		var attr;
		if (this.checkEmpty())
			return;
		for(elem in this){
			if (!gebi(this[elem]) || elem == 0)
				continue;
			switch (value){
				case "left":
					attr = gebi(this[0]).style.left;
					break;
				case "top": 
					attr = gebi(this[0]).style.top;
					break;
				case "right":
					attr = 	oper_pixel("+",gebi(this[0]).style.left,gebi(this[0]).style.width);
					attr =  oper_pixel("-",attr,gebi(this[elem]).style.width);
					break;
				case "down":
					attr = 	oper_pixel("+",gebi(this[0]).style.top,gebi(this[0]).style.height);
					attr =  oper_pixel("-",attr,gebi(this[elem]).style.height);
					break;	
			}	
			// atualiza posi��o do elemento html e posi��o do elemento que est� no array myElems 
			if (value=="top" || value=="down"){
				gebi(this[elem]).style.top = attr
				myElems[this[elem]].y = attr;alert(attr);
				myElems[this[elem]].yscreen = attr;
			}
			else{
				gebi(this[elem]).style.left = attr;alert(attr);
				myElems[this[elem]].x = attr;
				myElems[this[elem]].xscreen = attr;
			}
		}
	}
	
	mySelecteds.update = function(dif_x,dif_y,id){
		for(elem in this){
	       	object = gebi(this[elem]);
	       	if (object && this[elem]!=id){ 
	       	//if (object && this[elem]!=id && (!myElems[elem_move].parent || myElems[this[elem]].parent!=elem_move)){
	       		new_x = oper_pixel("+",object.style.left,dif_x);
	       		new_y = oper_pixel("+",object.style.top,dif_y); 
	       		object.style.left = new_x;
	       		object.style.top = new_y;
	       		myElems[this[elem]].x = new_x;
            	myElems[this[elem]].y = new_y;
            	if (!myElems[id].attached){
            		myElems[this[elem]].xscreen = new_x;
	       			myElems[this[elem]].yscreen = new_y;
	       		}
	       		else{
	       			x = oper_pixel("+",dif_x,myElems[this[elem]].xscreen);
              		y = oper_pixel("+",dif_y,myElems[this[elem]].yscreen);
              		myElems[this[elem]].xscreen = x;
	       			myElems[this[elem]].yscreen = y;
	       		}
	        }
		}
	}