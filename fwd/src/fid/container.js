/**
 * FWD - FAST WEB DEVELOPMENT
 * 
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/*******************************************************************************
 * Classe Container. Extende Element para poder conter outros elementos.
 * 
 * <p>Classe abstrata que extende Element para poder conter outros elementos.</p>
 * @package FID
 * @param String name Nome do elemento
 *******************************************************************************/
function Container(name){
  var self = new Element(name);
  self.elementType = 'Container';

 /**
  * Contador de filhos com nome gerado automaticamente
  * @var integer
  */
  self.autoNameCounter = 0;

 /**
  * Lista de elementos filhos
  * @var Array
  */
  self.children = new Array();
  
 /**
  * Lista de nomes dos tipos permitidos de elementos filhos.
  * Filhos de tipos que n�o constem nessa lista ser�o filhos fantasmas.
  * @var Array
  */
  self.allowedChildTypes = new Array(
    'static',
    'warning',
    'panel',
    'scrolling',
    'memo',
    'memostatic',
    'text',
    'button',
    'checkbox',
    'viewgroup',
    'radiobox',
    'select',
    'dbselect',
    'selector',
    'file',
    'treebase',
    'icon',
    'grid',
    'dbgrid',
    'tabgroup',
    'horizontalrule',
    'viewbutton'
  );
  
 /*****************************************************************************
  * Testa se o container pode ter filhos de um determinado tipo.
  *
  * <p>Testa se o container pode ter filhos de um determinado tipo.</p>
  * 
  * @param String typeName Tipo a ser testado
  * @return boolean True indica que o tipo � permitido
  *****************************************************************************/
  self.isAllowedChildType = function(typeName){
    return (self.allowedChildTypes.indexOf(typeName) != -1);
  }

 /*****************************************************************************
  * Seta o atributo top do container.
  *
  * <p>Seta o atributo top do container. Desloca tamb�m todos os elementos
  * filhos. Fun��o de callback para o atributo top.</p>
  * 
  * @param integer top Novo valor do atributo top
  *****************************************************************************/
  self.setTop = function(top){
    var offsetY = top - parseInt(self.getAttribute('top'));
    gebi(self.id).style.top = top;
    for(var childName in self.children){
      self.children[childName].move(0,offsetY);
    }
  }

 /*****************************************************************************
  * Retorna o atributo top do container.
  *
  * <p>Retorna o atributo top do container. Fun��o de callback para o atributo
  * top.</p>
  * 
  * @return integer Valor do atributo top
  *****************************************************************************/
  self.getTop = function(){
    return gebi(self.id).style.top;
  }

 /*****************************************************************************
  * Seta o atributo left do container.
  *
  * <p>Seta o atributo left do container. Desloca tamb�m todos os elementos
  * filhos. Fun��o de callback para o atributo left.</p>
  * 
  * @param integer left Novo valor do atributo left
  *****************************************************************************/
  self.setLeft = function(left){
    var offsetX = left - parseInt(self.getAttribute('left'));
    gebi(self.id).style.left = left;
    for(var childName in self.children){
      self.children[childName].move(offsetX,0);
    }
  }

 /*****************************************************************************
  * Retorna o atributo left do container.
  *
  * <p>Retorna o atributo left do container. Fun��o de callback para o atributo
  * left.</p>
  * 
  * @return integer Valor do atributo left
  *****************************************************************************/
  self.getLeft = function(){
    return gebi(self.id).style.left;
  }

  // Sobrescreve os atributos top e left da classe Element
  self.addAttribute('top','pixel',true,self.setTop,self.getTop);
  self.addAttribute('left','pixel',true,self.setLeft,self.getLeft);
  
 /*****************************************************************************
  * Testa se o container cont�m um determinado elemento.
  *
  * <p>Testa se o container cont�m um determinado elemento. � dito que o
  * container cont�m um elemento, quando o elemento est� totalmente dentro do
  * container e � de um tipo que o container aceita.</p>
  * 
  * @param Element element Elemento a ser testado
  * @return boolean True indica que ele cont�m o elemento
  *****************************************************************************/
  self.contain = function(element){
    return element.isInside(self);
  }
  
 /*****************************************************************************
  * Gera um nome autom�tico para um filho que n�o tem nome.
  *
  * <p>Gera um nome autom�tico para um filho que n�o tem nome.</p>
  * 
  * @return String Nome automaticamente gerado
  *****************************************************************************/
  self.getChildAutoName = function(){
    return self.name+'_autoname'+(++self.autoNameCounter);
  }

 /*****************************************************************************
  * Anexa um elemento ao container
  *
  * <p>Anexa um elemento ao container, ou seja, acrescenta um filho ao
  * container.</p>
  * 
  * @param Element Elemento a ser adicionado
  *****************************************************************************/
  // Anexa o elemento element ao container
  self.attachChild = function(element){
    if(element==self){
      alert("Erro: O elemento '"+self.name+"' n�o pode ser anexado a ele mesmo.");
      return false;
    }
    element.loadedFromXML = self.loadedFromXML;
    self.children[element.name] = element;
    element.parent = self;
  }
  
 /*****************************************************************************
  * Remove um elemento anexado.
  *
  * <p>Desanexa um elemento do container, ou seja, remove um filho do
  * container.</p>
  * 
  * @param Element Elemento a ser removido
  *****************************************************************************/
  self.deattachChild = function(element){
    element.parent = null;
    delete self.children[element.name];
  }
  
 /*****************************************************************************
  * Move o elemento
  *
  * <p>Move o elemento de acordo com o deslocamento indicado, levando consigo
  * os seus filhos</p>
  * 
  * @param integer offsetX Deslocamento na coordenada x
  * @param integer offsetY Deslocamento na coordenada y
  *****************************************************************************/
  self.move = function(offsetX,offsetY){
    self.setAttribute('left',parseInt(self.getAttribute('left'))+offsetX);
    self.setAttribute('top',parseInt(self.getAttribute('top'))+offsetY);
  }
  
 /*****************************************************************************
  * Alinha o elemento � grade.
  *
  * <p>Alinha o elemento � grade.</p>
  *****************************************************************************/
  self.alignToGrid = function(){
    var miRight,miBottom;
    var miLeft = parseInt(self.getAttribute('left'));
    var miTop = parseInt(self.getAttribute('top'));
    var miWidth = parseInt(self.getAttribute('width'));
    var miHeight = parseInt(self.getAttribute('height'));
    miLeft = fid.alignToGrid(miLeft);
    miTop = fid.alignToGrid(miTop);
    miRight = miLeft + miWidth;
    miBottom = miTop + miHeight;
    self.setAttribute('left',miLeft);
    self.setAttribute('top',miTop);
    for(var childName in self.children){
      self.children[childName].alignToGrid();
      self.children[childName].fitIn(miLeft,miTop,miRight,miBottom);
    }
  }
  
 /*****************************************************************************
  * Determina a posi��o do elemento.
  *
  * <p>Determina a posi��o do elemento na tela. A posi��o relativa dos seus
  * filhos permanece inalterada, ou seja, eles se movem junto com o pai.</p>
  * 
  * @param integer x Coordenada x do canto superior esquerdo do elemento.
  * @param integer y Coordenada y do canto superior esquerdo do elemento.
  *****************************************************************************/
  self.setPosition = function(x,y){
    self.setAttribute('left',x);
    self.setAttribute('top',y);
  }

 /*****************************************************************************
  * Destr�i o container
  *
  * <p>Destr�i o container e seus filhos.</p>
  *****************************************************************************/
  self.destroy = function(){
    for(var childName in self.children) self.children[childName].destroy();
    if(self.parent!=null) delete self.parent.children[self.name];
    var htmlElement = gebi(self.id);
    htmlElement.parentNode.removeChild(htmlElement);
  }
  
 /*****************************************************************************
  * Retorna o c�digo XML do conte�do do container
  *
  * <p>Retorna o c�digo XML do conte�do do container. Nesse conte�do est� o
  * c�digo XML de todos os seus filhos e, caso algum deles seja um Radiobox ou
  * um Checkbox que n�o tenha o atributo controller setado, cont�m tamb�m o(s)
  * controller(s).</p>
  * 
  * @param String ident Identa��o do conte�do do elemento no c�digo XML.
  * @return String Trecho de c�digo XML contendo todo o conte�do do elemento.
  *****************************************************************************/
  self.getXMLContent = function(ident,tabIndex){
    var childName,childObj,i;
    var xmlCode = self.getBoxCode(ident);
    var sortedChildren = new Array();
    var checkController = self.name+'CheckController';
    var radioController = self.name+'RadioController';
    var hasCheckboxes = false;
    var hasRadioboxes = false;
    for(childName in self.children){
      childObj = new Object();
      childObj.name = childName;
      childObj.top = parseInt(self.children[childName].getAttribute('top'));
      childObj.left = parseInt(self.children[childName].getAttribute('left'));
      if(self.children[childName].elementType=='Checkbox' && !self.children[childName].getAttribute('controller')){
        hasCheckboxes = true;
        self.children[childName].setAttribute('controller',checkController);
      }else if(self.children[childName].elementType=='Radiobox' && !self.children[childName].getAttribute('controller')){
        hasRadioboxes = true;
        self.children[childName].setAttribute('controller',radioController);
      }
      i = 0;
      while(i<sortedChildren.length){
        if(childObj.top < sortedChildren[i].top) break;
        if(childObj.top==sortedChildren[i].top && childObj.left < sortedChildren[i].left) break;
        i++;
      }
      sortedChildren.splice(i,0,childObj);
    }
    if(hasCheckboxes) xmlCode+= ident+'<controller name="'+checkController+'" />\n';
    if(hasRadioboxes) xmlCode+= ident+'<controller name="'+radioController+'" />\n';
    xmlCode+= this.getXMLGhostChildren(ident);
    for(i=0;i<sortedChildren.length;i++){
      xmlCode+= self.children[sortedChildren[i].name].getXMLCode(ident,tabIndex.inc());
    }
    return xmlCode;
  }
  
 /*****************************************************************************
  * Preenche o container a partir do XML
  *
  * <p>Preenche os dados do container e cria seus filhos a partir de sua
  * representa��o em XML (objeto XMLDocument, n�o texto XML)</p>
  * 
  * @param XMLDocument xmlElement Objeto contendo a representa��o do elemento
  *        em XML.
  *****************************************************************************/
  self.loadFromXML = function(xmlElement,parentBox){
    self.loadedFromXML = true;
    self.ghostAttributes = new Array();
    self.ghostChildren = new Array();
    var boxAttributes = null;
    if(xmlElement.tagName && xmlElement.tagName.toLowerCase()==self.elementType.toLowerCase()){
      var xmlChildren = xmlElement.childNodes;
      var childName,childType,child;
      this.loadAttributesFromXML(xmlElement.attributes);
      for(var i=0;i<xmlChildren.length;i++){
        if(xmlChildren[i].tagName){
          if(xmlChildren[i].tagName=='box'){
            self.loadBoxFromXML(xmlChildren[i]);
          }else if(self.isAllowedChildType(xmlChildren[i].tagName.toLowerCase())){
            if(!boxAttributes){
              if(parentBox){
                self.move(parentBox['left'],parentBox['top']);
                self.fitIn(parentBox['left'],parentBox['top'],parentBox['right'],parentBox['bottom']);
              }
              boxAttributes = {
                'top'   : parseInt(self.getAttribute('top')),
                'left'  : parseInt(self.getAttribute('left'))
              };
              boxAttributes['bottom'] = boxAttributes['top'] + parseInt(self.getAttribute('height'));
              boxAttributes['right'] = boxAttributes['left'] + parseInt(self.getAttribute('width'));
            }
            childType = xmlChildren[i].tagName.toLowerCase();
            childType = childType.substr(0,1).toUpperCase() + childType.substr(1);
            if(childName = xmlChildren[i].attributes.getNamedItem('name')){
              childName = xmlChildren[i].attributes.getNamedItem('name').nodeValue;
              eval('child = new '+childType+'(childName);');
            }else{
              childName = self.getChildAutoName();
              eval('child = new '+childType+'(childName);');
              child.setAutoname(true);
            }
            self.attachChild(child);
            child.loadFromXML(xmlChildren[i],boxAttributes);
          }else{
            self.addGhostChild(xmlChildren[i]);
          }
        }
      }
      if(!boxAttributes){
        if(parentBox){
          self.move(parentBox['left'],parentBox['top']);
          self.fitIn(parentBox['left'],parentBox['top'],parentBox['right'],parentBox['bottom']);
        }
        boxAttributes = {
          'top'   : parseInt(self.getAttribute('top')),
          'left'  : parseInt(self.getAttribute('left'))
        };
        boxAttributes['bottom'] = boxAttributes['top'] + parseInt(self.getAttribute('height'));
        boxAttributes['right'] = boxAttributes['left'] + parseInt(self.getAttribute('width'));
      }
    }else return false;
  }

  return self;

}