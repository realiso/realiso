/**
 * FWD - FAST WEB DEVELOPMENT
 * 
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/*******************************************************************************
 * Classe Select. Especializa��o de Element que define o elemento Select.
 * 
 * <p>Especializa��o de Element que define o elemento Select.</p>
 *
 * @package FID
 * @param String name Nome do elemento
 *******************************************************************************/
function Select(name){
  var self = new Element(name);
  self.elementType = 'Select';
  self.defaultClassName = '';
  self.hasTabIndex = true;
  self.createHtmlElement('SELECT');
  var htmlElement = gebi(self.id);
  htmlElement.options[0] = new Option('default option','');
  self.setAttribute('class','');
  
  /*****************************************************************************
   * Retorna o c�digo XML do conte�do do select.
   *
   * <p>Retorna o c�digo XML do conte�do do select. Cont�m sempre uma tag item
   * somente para servir de exemplo na hora de editar o XML manualmente.</p>
   * 
   * @param String ident Identa��o do conte�do do select no c�digo XML.
   * @return String Trecho de c�digo XML contendo todo o conte�do do select.
   *****************************************************************************/
  self.getXMLContent = function(ident){
    var xmlCode = self.getBoxCode(ident);
    if(self.ghostChildren.length>0){
      xmlCode+= self.getXMLGhostChildren(ident);
    }else if(!self.loadedFromXML){
      xmlCode+= ident+'<item key="default">\n'
               +ident+'  <string id="select_default_option">default option</string>\n'
               +ident+'</item>\n';
    }
    return xmlCode;
  }
  
  return self;
}

