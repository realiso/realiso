/**
 * FWD - FAST WEB DEVELOPMENT
 * 
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/*******************************************************************************
 * Classe Reportstatic. Especialização de View que define o elemento ReportStatic.
 * 
 * <p>Especialização de Element que define o elemento ReportStatic.</p>
 *
 * @package FID
 * @param String name Nome do elemento
 *******************************************************************************/
function Reportstatic(name){
  var self = new View(name);
  self.elementType = 'Reportstatic';
  self.hasTabIndex = false;
  self.csStringId = '';
  self.createHtmlElement('SPAN');
  // Sobrescreve o atributo value do View
  self.addAttribute('value','text',true,'innerHTML');
  self.setAttribute('value',name);
  
  delete self.attributes['class'];
  delete self.attributes['align'];
  self.addAttribute('top','hidden',true,'style.top');
  self.addAttribute('height','hidden',true,'style.height');
  self.addAttribute('width','pixel',false,
    function(psValue){gebi(self.id).style.width = psValue;},
    function(){return parseInt(gebi(self.id).style.width);}
  );
  self.addAttribute('bold','boolean',false,'cbBold');
  self.addAttribute('marginleft','integer',false,'ciMarginLeft');
  self.addAttribute('marginright','integer',false,'ciMarginRight');
  self.setAttribute('marginleft',2);
  self.setAttribute('marginright',2);
  
  self.setPosition = function(piLeft,piTop){
    if(self.parent){
      var miParentLeft = parseInt(self.parent.getAttribute('left'));
      var miParentRight = miParentLeft + parseInt(self.parent.getAttribute('width'));
      var miNewLeft = piLeft;
      var miWidth = parseInt(self.getAttribute('width'));
      
      if(miNewLeft < miParentLeft){
        miNewLeft = miParentLeft;
      }else if(miNewLeft+miWidth > miParentRight){
        miNewLeft = miParentRight - miWidth;
      }
      self.setAttribute('left',miNewLeft);
      self.setAttribute('top',parseInt(self.parent.getAttribute('top')));
    }else{
      self.setAttribute('left',piLeft);
      self.setAttribute('top',piTop);
    }
  }
  
  self.setSize = function(height,width){
    if(self.parent){
      self.setAttribute('height',parseInt(self.parent.getAttribute('height')));
    }else{
      self.setAttribute('height',height);
    }
    self.setAttribute('width',width);
  }
  
  self.loadFromXML = function(xmlElement,parentBox){
    self.loadedFromXML = true;
    if(xmlElement.tagName && xmlElement.tagName.toLowerCase()==self.elementType.toLowerCase()){
      var xmlChildren = xmlElement.childNodes;
      self.setAttribute('width',20);
      self.setAttribute('marginleft',0);
      self.setAttribute('marginright',0);
      self.loadAttributesFromXML(xmlElement.attributes);
      var value = '';
      for(var i=0;i<xmlChildren.length;i++){
        if(xmlChildren[i].tagName){
          if(xmlChildren[i].tagName=='string'){
            self.csStringId = xmlChildren[i].attributes.getNamedItem('id').nodeValue;
            for(var j=0;j<xmlChildren[i].childNodes.length;j++){
              var child = xmlChildren[i].childNodes[j];
              if(!child.tagName){
                value+= child.nodeValue;
              }
            }
          }
        }
      }
      self.setAttribute('value',value);
      self.setAttribute('top',parentBox['top']);
      self.setAttribute('height',parentBox['height']);
      self.setAttribute('left',parentBox['left']);
      return true;
    }else return false;
  }

  self.getXMLContent = function(ident,tabIndex){
    var msValue,msStringId;
    var xmlCode = '';
    if(self.getAttribute('value')){
      msValue = self.getAttribute('value');
      msValue = msValue.replace('&','&amp;','g');
      msValue = msValue.replace('\t','  ','g');
      if(self.csStringId){
        msStringId = self.csStringId;
      }else{
        msStringId = 'rs_'+self.getAttribute('name').toLowerCase();
      }
      xmlCode = ident+'<string id="'+msStringId+'">'+msValue+'</string>\n';
    }
    return xmlCode;
  }

  return self;
}
