/**
 * FWD - FAST WEB DEVELOPMENT
 * 
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/*******************************************************************************
 * Classe Grid. Especializa��o de Element que define o elemento Grid.
 * 
 * <p>Especializa��o de Element que define o elemento Grid. Um Grid tem uma
 * lista de colunas, cada uma contendo nome e largura.</p>
 *
 * @package FID
 * @param String name Nome do elemento
 *******************************************************************************/
function Grid(name){
  var self = new Element(name);
  self.elementType = 'Grid';
  self.defaultClassName = 'grid';
  var htmlElement = self.createHtmlElement('DIV');
  self.setAttribute('class','');
  
 /**
  * Lista contendo os dados das colunas da Grid
  * @var Array
  */
  self.columns = new Array();
  
 /**
  * Altura das linhas da Grid
  * @var integer
  */
  self.rowHeight = 20;
  
 /*****************************************************************************
  * Atualiza a representa��o do Grid
  *
  * <p>Atualiza a representa��o do Grid. O Grid � representado por uma tabela
  * HTML contendo n�meros de linhas e de colunas iguais aos do Grid, mas as
  * larguras das colunas da tabela n�o tem rela��o com as larguras das colunas
  * do Grid.</p>
  *****************************************************************************/
  self.update = function(){
    var height = parseInt(self.getAttribute('height'));
    var rows = Math.ceil(height/self.rowHeight);
    var i,j,html = '<table style="height:100%;width:100%;border-collapse:collapse;padding:0px;margin:0px;text-align:center;">';
    html+= '<tr>';
    for(j=0;j<self.columns.length;j++){
      html+= '<td style="border:solid 1px black;padding:0px;margin:0px;">'+self.columns[j].getName()+'</td>';
    }
    html+= '</tr>';
    for(i=1;i<rows;i++){
      html+= '<tr>';
      for(j=0;j<self.columns.length;j++){
        html+= '<td style="border:solid 1px black;padding:0px;margin:0px;">&nbsp;</td>';
      }
      html+= '</tr>';
    }
    html+= '</table>';
    gebi(self.id).innerHTML = html;
  }

 /*****************************************************************************
  * Define o valor do atributo colNames
  *
  * <p>Define o valor do atributo colNames, que, apesar do nome, � uma lista
  * com os nomes e as larguras das colunas do Grid.</p>
  * 
  * @param String colNames Lista de nomes e larguras implodidos numa string
  *****************************************************************************/
  self.setColNames = function(colNames){
    var colNames = colNames.split('\n');
    var column;
    if(self.columns.length > colNames.length) self.columns.length = colNames.length;
    var i = 0;
    while(i<self.columns.length){
      colNames[i] = trim(colNames[i]);
      if(colNames[i]!=''){
        colNames[i] = colNames[i].split('|');
        self.columns[i].setName(colNames[i][0]);
        self.columns[i].setWidth(colNames[i][1]);
      }
      i++;
    }
    while(i<colNames.length){
      colNames[i] = trim(colNames[i]);
      if(colNames[i]!=''){
        colNames[i] = colNames[i].split('|');
        self.columns[i] = new Column(colNames[i][0],colNames[i][1]);
      }
      i++;
    }
    self.update();
  }
  
 /*****************************************************************************
  * Retorna o valor do atributo colNames
  *
  * <p>Retorna o valor do atributo colNames, que, apesar do nome, � uma lista
  * com os nomes e as larguras das colunas do Grid.</p>
  * 
  * @return String Lista de nomes e larguras implodidos numa string
  *****************************************************************************/
  self.getColNames = function(){
    var colNames = new Array();
    for(var i=0;i<self.columns.length;i++){
      colNames[i] = self.columns[i].getName()+'|'+self.columns[i].getWidth();
    }
    return colNames.join('\n');
  }
  
 /*****************************************************************************
  * Retorna o c�digo XML do conte�do do Grid
  *
  * <p>Retorna o c�digo XML do conte�do do Grid, que cont�m o c�digo de cada
  * uma de suas colunas.</p>
  * 
  * @param String ident Identa��o do conte�do do Grid no c�digo XML.
  * @return String Trecho de c�digo XML contendo todo o conte�do do Grid.
  *****************************************************************************/
  self.getXMLContent = function(ident){
    if(!ident) ident = '';
    var colName;
    var xmlCode = self.getBoxCode(ident);
    xmlCode+= self.getXMLGhostChildren(ident);
    for(var i=0;i<self.columns.length;i++){
      xmlCode+= self.columns[i].getXMLCode(ident);
    }
    return xmlCode;
  }
  
 /*****************************************************************************
  * Preenche o Grid a partir do XML
  *
  * <p>Preenche os dados do Grid e das suas colunas a partir de sua
  * representa��o em XML (objeto XMLDocument, n�o texto XML)</p>
  * 
  * @param XMLDocument xmlElement Objeto contendo a representa��o do Grid em
  *        XML.
  * @param Object parentBox Array associativo contendo os atributos da box do
  *        container pai.
  *****************************************************************************/
  self.loadFromXML = function(xmlElement,parentBox){
    if(xmlElement.tagName && xmlElement.tagName.toLowerCase()==self.elementType.toLowerCase()){
      var xmlChildren = xmlElement.childNodes;
      var column, colName;
      var colCounter = 0;
      self.columns = new Array();
      self.loadAttributesFromXML(xmlElement.attributes);
      for(var i=0;i<xmlChildren.length;i++){
        if(xmlChildren[i].tagName){
          if(xmlChildren[i].tagName=='box'){
            self.loadBoxFromXML(xmlChildren[i]);
          }else if(xmlChildren[i].tagName=='column'){
            colName = xmlChildren[i].attributes.getNamedItem('name');
            if(colName){
              colName = colName.nodeValue;
            }else{
              colName = '';
            }
            column = new Column(colName);
            column.loadFromXML(xmlChildren[i]);
            self.columns[colCounter++] = column;
          }else{
            self.addGhostChild(xmlChildren[i]);
          }
        }
      }
      self.move(parentBox['left'],parentBox['top']);
      self.fitIn(parentBox['left'],parentBox['top'],parentBox['right'],parentBox['bottom']);
      self.cols = colCounter;
      self.update();
      return true;
    }else return false;
  }
  
 /*****************************************************************************
  * Retorna a altura da Grid
  *
  * <p>Retorna a altura da Grid. Fun��o de callback para o par�metro height.</p>
  * @return integer Altura da Grid
  *****************************************************************************/
  self.getHeight = function(){
    return gebi(self.id).style.height;
  }
  
 /*****************************************************************************
  * Seta a altura da Grid
  *
  * <p>Seta a altura da Grid. Fun��o de callback para o par�metro height.</p>
  * @param integer Nova altura da Grid
  *****************************************************************************/
  self.setHeight = function(height){
    gebi(self.id).style.height = height;
    self.update();
  }
  
 /*****************************************************************************
  * Retorna a altura das linhas da Grid
  *
  * <p>Retorna a altura das linhas da Grid. Fun��o de callback para o par�metro
  * rowheight.</p>
  * @return integer Altura das linhas da Grid
  *****************************************************************************/
  self.getRowHeight = function(){
    return self.rowHeight;
  }
  
 /*****************************************************************************
  * Seta a altura das linhas da Grid
  *
  * <p>Seta a altura das linhas da Grid. Fun��o de callback para o par�metro
  * rowheight.</p>
  * @param integer Nova altura das linhas da Grid
  *****************************************************************************/
  self.setRowHeight = function(rowHeight){
    self.rowHeight = rowHeight;
    self.update();
  }

  colNamesSetFunc = function(value){ self.setColNames(value); };
  colNamesGetFunc = function(){ return self.getColNames(); }

  // Define os atributos da Grid
  self.addAttribute('height','pixel',true,self.setHeight,self.getHeight);
  self.addAttribute('rowheight','pixel',false,self.setRowHeight,self.getRowHeight);
  self.addAttribute('colNames','list',true,colNamesSetFunc,colNamesGetFunc);

  self.setColNames('col1|\ncol2|\ncol3|');
  return self;
}