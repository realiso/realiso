/**
 * FWD - FAST WEB DEVELOPMENT
 * 
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/*******************************************************************************
 * Classe ElementEditor. Extende SpecialElement para definir o editor de
 * elementos.
 * 
 * <p>Extende SpecialElement para definir o elemento que serve para que o
 * usu�rio edite os atributos dos elementos.</p>
 *
 * @package FID
 *******************************************************************************/
function ElementEditor(){
  var self = new SpecialElement('elementEditor');
  this.elementType = 'ElementEditor';
  // Constroi a tabela
  // A primeira linha eh o cabecalho
  self.createHtmlElement('TABLE');
  var editor = gebi(self.id);
  var tbody = document.createElement('TBODY');
  var header = document.createElement('TR');
  var cel = document.createElement('TD');
  editor.className = 'editor';
  cel.innerHTML = 'Element Editor';
  cel.colSpan = 2;
  cel.className = 'header';
  header.appendChild(cel);
  tbody.appendChild(header);
  editor.appendChild(tbody);
  // Redefine os atributos basicos de posicao e tamanho exceto width
  // Esses atributos s�o somente de leitura
  self.addAttribute('top','pixel',false,'offsetTop');
  self.addAttribute('left','pixel',false,'offsetLeft');
  self.addAttribute('height','pixel',false,'offsetHeight');
  
 /**
  * @var Array Atributos falsos. S�o exibidos quando n�o h� exatamente um
  * elemento selecionado
  */
  self.dummyAttributes = new Array('name','top','left','height','width');
  
 /*****************************************************************************
  * Adiciona um atributo de um elemento no ElementEditor
  *
  * <p>Adiciona um atributo de um elemento no ElementEditor</p>
  * 
  * @param String attributeName Nome do atributo a ser adicionado
  * @param Element element Elemento � que pertence o atributo a ser exibido
  *****************************************************************************/
  self.addAttribute = function(attributeName,element){
    var attributeType = element.getAttributeType(attributeName);
    if(attributeType=='hidden') return;
    var tbody = gebi(this.name).tBodies[0];
    var tr = document.createElement('TR');
    var cel = document.createElement('TD');
    cel.innerHTML = attributeName;
    cel.className = 'attributeName';
    cel.style.width = '100%';
    tr.appendChild(cel);
    cel = document.createElement('TD');
    if(attributeType=='alignment'){
      var field = document.createElement('SELECT');
      var i = 0;
      field.options[i++] = new Option('none','none');
      field.options[i++] = new Option('--------','');
      field.options[i++] = new Option('top','top');
      field.options[i++] = new Option('bottom','bottom');
      field.options[i++] = new Option('left','left');
      field.options[i++] = new Option('right','right');
      field.name = attributeName;
      field.value = element.getAttribute(attributeName);
      field.element = element;
      field.onchange = function(){
        this.element.setAttribute(this.name,this.value);
        if(this.value=='') this.value = 'none';
      };
      field.onmousedown = function(){ self.mouseEventCaptured = true; };
    }else if(attributeType=='select'){
      var field = document.createElement('SELECT');
      var maOptions = element.attributes[attributeName].caOptions;
      for(var i=0;i<maOptions.length;i++){
        field.options[i] = new Option(maOptions[i],maOptions[i]);
      }
      field.name = attributeName;
      field.value = element.getAttribute(attributeName);
      field.element = element;
      field.onchange = function(){
        this.element.setAttribute(this.name,this.value);
      };
      field.onmousedown = function(){ self.mouseEventCaptured = true; };
    }else if(attributeType=='color'){
      var field = document.createElement('INPUT');
      field.type = 'text';
      field.name = attributeName;
      field.element = element;
      field.value = element.getAttribute(attributeName);
      field.onchange = function(){ this.element.setAttribute(this.name,this.value); };
    }else if(attributeType=='list'){
      var field = document.createElement('INPUT');
      field.type = 'button';
      field.value = '...';
      field.name = attributeName;
      field.element = element;
      field.onmousedown = function(){ new ListEditor(element,attributeName); };
    }else if(attributeType=='boolean'){
      var field = document.createElement('SELECT');
      var i = 0;
      field.options[i++] = new Option('','');
      field.options[i++] = new Option('true','true');
      field.options[i++] = new Option('false','false');
      field.name = attributeName;
      field.value = element.getAttribute(attributeName);
      field.element = element;
      field.onchange = function(){
        this.element.setAttribute(this.name,this.value);
      };
      field.onmousedown = function(){ self.mouseEventCaptured = true; };
    }else{
      var field = document.createElement('INPUT');
      field.type = 'text';
      field.name = attributeName;
      field.element = element;
      field.value = element.getAttribute(attributeName);
      field.onchange = function(){ this.element.setAttribute(this.name,this.value); };
    }
    field.style.width = 120;
    cel.appendChild(field);
    cel.className = 'attributeValue';
    tr.appendChild(cel);
    tbody.appendChild(tr);
  }
  
 /*****************************************************************************
  * Preenche o ElementEditor com os atributos falsos
  *
  * <p>Preenche o ElementEditor com os atributos falsos</p>
  *****************************************************************************/
  self.fillWithDummyAttributes = function(){
    self.clear();
    var tbody = gebi(this.name).tBodies[0];
    var tr,cel;
    for(att in self.dummyAttributes){
      tr = document.createElement('TR');
      cel = document.createElement('TD');
      cel.innerHTML = self.dummyAttributes[att];
      cel.className = 'attributeName';
      cel.style.width = '100%';
      tr.appendChild(cel);
      cel = document.createElement('TD');
      cel.innerHTML = '<input type="text" readonly="readonly" style="width:120px" value="">';
      cel.className = 'attributeValue';
      tr.appendChild(cel);
      tbody.appendChild(tr);
    }
  }
  
 /*****************************************************************************
  * Esvazia o ElementEditor
  *
  * <p>Esvazia o ElementEditor</p>
  *****************************************************************************/
  self.clear = function(){
    var tbody = gebi(this.name).tBodies[0];
    var i = tbody.rows.length;
    while(i-- > 1) tbody.removeChild(tbody.lastChild);
  }
  
 /*****************************************************************************
  * Exibe os atributos de um elemento no ElementEditor
  *
  * <p>Exibe os atributos de um elemento no ElementEditor.</p>
  * 
  * @param Element element Elemento a ser exibido
  *****************************************************************************/
  self.showElement = function(element){
    if(element){
      self.clear();
      for(attributeName in element.attributes) self.addAttribute(attributeName,element);
      self.setAttribute('width',204);
    }else{
      alert('Invalid element passed to ElementEditor.');
    }
  }
  
  // Inicializa o ElementEditor com os atributos falsos
  self.fillWithDummyAttributes();
  return self;
}