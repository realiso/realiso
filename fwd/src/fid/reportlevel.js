/**
 * FWD - FAST WEB DEVELOPMENT
 * 
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/*******************************************************************************
 * Classe Reportlevel. Especialização de Container que define o elemento ReportLevel
 * 
 * <p>Especialização de Container que define o elemento Reportlevel.</p>
 * @package FID
 * @param String name Nome do elemento
 *******************************************************************************/
function Reportlevel(name){
  var self = new Container(name);
  self.elementType = 'Reportlevel';
  self.createHtmlElement('DIV');
  self.allowedChildTypes = ['reportlevel','reportline'];
  
  delete self.attributes['class'];
  delete self.attributes['align'];
  self.addAttribute('offset','integer',false,'ciOffset');
  self.addAttribute('bottommargin','integer',false,'ciBottomMargin');
  self.setAttribute('offset',20);
  self.setAttribute('bottommargin',0);
  
  self.getBoxCode = function(ident){
    return '';
  }

  self.loadFromXML = function(xmlElement,parentBox){
    self.loadedFromXML = true;
    self.setAttribute('top',parentBox['top']);
    self.setAttribute('left',parentBox['left']);
    self.setAttribute('offset',0);
    var boxAttributes = {};
    if(xmlElement.tagName && xmlElement.tagName.toLowerCase()==self.elementType.toLowerCase()){
      var xmlChildren = xmlElement.childNodes;
      var childName,child;
      this.loadAttributesFromXML(xmlElement.attributes);
      self.setAttribute('left',parentBox['left']+parseInt(self.getAttribute('offset')));
      boxAttributes['top'] = parseInt(self.getAttribute('top'));
      boxAttributes['left'] = parseInt(self.getAttribute('left'));
      boxAttributes['width'] = 0;
      var miTotalHeight = 0;
      for(var i=0;i<xmlChildren.length;i++){
        if(xmlChildren[i].tagName){
          var msChildType = xmlChildren[i].tagName.toLowerCase();
          if(self.isAllowedChildType(msChildType)){
            msChildType = msChildType.substr(0,1).toUpperCase() + msChildType.substr(1);
            if(childName = xmlChildren[i].attributes.getNamedItem('name')){
              childName = xmlChildren[i].attributes.getNamedItem('name').nodeValue;
              eval('child = new '+msChildType+'(childName);');
            }else{
              childName = self.getChildAutoName();
              eval('child = new '+msChildType+'(childName);');
              child.setAutoname(true);
            }
            self.attachChild(child);
            child.loadFromXML(xmlChildren[i],boxAttributes);
            miTotalHeight+= child.getAttribute('height');
            if(msChildType=='Reportlevel'){
              boxAttributes['width'] = Math.max(boxAttributes['width'], parseInt(child.getAttribute('width')) + parseInt(self.getAttribute('offset')));
            }else{
              boxAttributes['width'] = Math.max(boxAttributes['width'], parseInt(child.getAttribute('width')));
            }
            boxAttributes['top'] = parentBox['top'] + miTotalHeight;
          }
        }
      }
      self.setAttribute('width',boxAttributes['width']);
      self.setAttribute('height',miTotalHeight);
      //alert('['+self.elementType+']\n'+self);
      return true;
    }else{
      return false;
    }
  }

  return self;
}

