/**
 * FWD - FAST WEB DEVELOPMENT
 * 
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/*******************************************************************************
 * Classe Report. Especialização de Reportlevel que define o elemento Report.
 * 
 * <p>Especialização de Reportlevel que define o elemento Report.</p>
 * @package FID
 * @param String name Nome do elemento
 *******************************************************************************/
function Report(psClass){
  name = 'report';
  var self = new Reportlevel(name);
  self.name = name;
  self.elementType = 'Report';
  self.createHtmlElement('DIV');
  delete self.attributes['bottommargin'];
  self.setAttribute('offset',0);
  
  self.csClass = psClass;
  
  self.loadFromXML = function(xmlElement,parentBox){
    self.loadedFromXML = true;
    self.setAttribute('top',parentBox['top']);
    self.setAttribute('left',parentBox['left']);
    self.setAttribute('offset',0);
    var boxAttributes = {};
    var xmlChildren = xmlElement.childNodes;
    var childName,child;
    this.loadAttributesFromXML(xmlElement.attributes);
    boxAttributes['top'] = parseInt(self.getAttribute('top'));
    boxAttributes['left'] = parseInt(self.getAttribute('left'));
    boxAttributes['width'] = 0;
    var miTotalHeight = 0;
    for(var i=0;i<xmlChildren.length;i++){
      if(xmlChildren[i].tagName){
        var msChildType = xmlChildren[i].tagName.toLowerCase();
        if(self.isAllowedChildType(msChildType)){
          msChildType = msChildType.substr(0,1).toUpperCase() + msChildType.substr(1);
          if(childName = xmlChildren[i].attributes.getNamedItem('name')){
            childName = xmlChildren[i].attributes.getNamedItem('name').nodeValue;
            eval('child = new '+msChildType+'(childName);');
          }else{
            childName = self.getChildAutoName();
            eval('child = new '+msChildType+'(childName);');
            child.setAutoname(true);
          }
          self.attachChild(child);
          child.loadFromXML(xmlChildren[i],boxAttributes);
          miTotalHeight+= parseInt(child.getAttribute('height'));
          if(msChildType=='Reportlevel'){
            boxAttributes['width'] = Math.max(boxAttributes['width'], parseInt(child.getAttribute('width')) + parseInt(self.getAttribute('offset')));
          }else{
            boxAttributes['width'] = Math.max(boxAttributes['width'], parseInt(child.getAttribute('width')));
          }
          boxAttributes['top'] = parentBox['top'] + miTotalHeight;
        }
      }
    }
    self.setAttribute('width',boxAttributes['width']);
    self.setAttribute('height',miTotalHeight);
    return true;
  }
  
  self.getXMLCode = function(psIdent,poTabIndex){
    return '<root>\n'
          +'  <'+self.csClass+self.getXMLAttributes(poTabIndex)+'>\n'
          +      self.getXMLContent(psIdent+'    ',poTabIndex)
          +'  </'+self.csClass+'>\n'
          +'</root>';
  }
  
  return self;
}

