/**
 * FWD - FAST WEB DEVELOPMENT
 * 
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/*******************************************************************************
 * Classe Reportline. Especialização de Container que define o elemento ReportLine
 * 
 * <p>Especialização de Container que define o elemento ReportLine.</p>
 * @package FID
 * @param String name Nome do elemento
 *******************************************************************************/
function Reportline(name){
  var self = new Container(name);
  self.allowedChildTypes = ['reportstatic','reporticon'];
  self.elementType = 'Reportline';
  self.createHtmlElement('DIV');
  
  delete self.attributes['class'];
  delete self.attributes['align'];
  self.addAttribute('height','pixel',false,
    function(psValue){gebi(self.id).style.height = psValue;},
    function(){return parseInt(gebi(self.id).style.height);}
  );
  self.addAttribute('target','select',false,'csTarget',['header','body','footer']);
  self.addAttribute('bgcolor','color',false,'csBgColor');
  self.addAttribute('bgcolor2','color',false,'csBgColor2');
  self.addAttribute('linespacing','integer',false,'ciLineSpacing');
  self.setAttribute('bgcolor','');
  self.setAttribute('bgcolor2','');
  self.setAttribute('linespacing',0);
  
  self.attachChild = function(element){
    if(element==self){
      alert("Erro: O elemento '"+self.name+"' não pode ser anexado a ele mesmo.");
      return false;
    }
    element.loadedFromXML = self.loadedFromXML;
    self.children[element.name] = element;
    element.parent = self;
    
    element.setAttribute('height',parseInt(self.getAttribute('height')));
  }

  self.setSize = function(piHeight,piWidth){
    self.setAttribute('height',piHeight);
    self.setAttribute('width',piWidth);
    for(childName in self.children){
      self.children[childName].setAttribute('height',piHeight);
    }
  }

  self.getBoxCode = function(ident){
    return '';
  }

  self.loadFromXML = function(xmlElement,parentBox){
    self.loadedFromXML = true;
    self.setAttribute('top',parentBox['top']);
    self.setAttribute('left',parentBox['left']);
    self.setAttribute('height',0);
    var boxAttributes = {};
    if(xmlElement.tagName && xmlElement.tagName.toLowerCase()==self.elementType.toLowerCase()){
      var xmlChildren = xmlElement.childNodes;
      var childName,child;
      this.loadAttributesFromXML(xmlElement.attributes);
      boxAttributes['top'] = self.getAttribute('top');
      boxAttributes['left'] = self.getAttribute('left');
      boxAttributes['height'] = self.getAttribute('height');
      var miTotalWidth = 0;
      for(var i=0;i<xmlChildren.length;i++){
        if(xmlChildren[i].tagName){
          var msChildType = xmlChildren[i].tagName.toLowerCase();
          if(self.isAllowedChildType(msChildType)){
            msChildType = msChildType.substr(0,1).toUpperCase() + msChildType.substr(1);
            if(childName = xmlChildren[i].attributes.getNamedItem('name')){
              childName = xmlChildren[i].attributes.getNamedItem('name').nodeValue;
              eval('child = new '+msChildType+'(childName);');
            }else{
              childName = self.getChildAutoName();
              eval('child = new '+msChildType+'(childName);');
              child.setAutoname(true);
            }
            self.attachChild(child);
            child.loadFromXML(xmlChildren[i],boxAttributes);
            miTotalWidth+= child.getAttribute('width');
            boxAttributes['left'] = parentBox['left'] + miTotalWidth;
          }
        }
      }
      self.setAttribute('width',miTotalWidth);
      return true;
    }else{
      return false;
    }
  }

  return self;
}

