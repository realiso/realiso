/**
 * FWD - FAST WEB DEVELOPMENT
 * 
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/*******************************************************************************
 * Classe Scrolling. Especializa��o de Container que define o elemento Scrolling
 * 
 * <p>Especializa��o de Container que define o elemento Scrolling.</p>
 * @package FID
 * @param String name Nome do elemento
 *******************************************************************************/
function Scrolling(name){
  var self = new Container(name);
  self.elementType = 'Scrolling';
  self.createHtmlElement('DIV');
  self.setAttribute('class','');
  
 /**
  * Indica se o Scrolling est� vazio
  * @var boolean
  */
  self.empty = true;
  
 /**
  * Lista de nomes dos tipos permitidos de elementos filhos.
  * Filhos de tipos que n�o constem nessa lista ser�o filhos fantasmas.
  * @var Array
  */
  self.allowedChildTypes = new Array('static');
  
 /*****************************************************************************
  * Anexa um elemento ao container
  *
  * <p>Anexa um elemento ao container, ou seja, acrescenta um filho ao
  * container.</p>
  * 
  * @param Element Elemento a ser adicionado
  *****************************************************************************/
  // Anexa o elemento element ao container
  self.attachChild = function(element){
    if(element==self){
      alert("Erro: O elemento '"+self.name+"' n�o pode ser anexado a ele mesmo.");
      return false;
    }
    element.loadedFromXML = self.loadedFromXML;
    self.children[element.name] = element;
    element.parent = self;
    self.empty = false;
  }
  
 /*****************************************************************************
  * Remove um elemento anexado.
  *
  * <p>Desanexa um elemento do container, ou seja, remove um filho do
  * container.</p>
  * 
  * @param Element Elemento a ser removido
  *****************************************************************************/
  self.deattachChild = function(element){
    element.parent = null;
    delete self.children[element.name];
    self.empty = true;
  }

 /*****************************************************************************
  * Testa se o container cont�m um determinado elemento.
  *
  * <p>Testa se o Scrolling cont�m um determinado elemento. O Scrolling s�
  * aceita elementos do tipo static e somente um elemento.</p>
  * 
  * @param Element element Elemento a ser testado
  * @return boolean True indica que ele cont�m o elemento
  *****************************************************************************/
  self.contain = function(element){
    return (element.isInside(self) && self.empty && self.isAllowedChildType(element.elementType.toLowerCase()));
  }
  
  // Define o atributo speed
  self.addAttribute('speed','integer',false,'speed');
  self.setAttribute('speed',100);
  
  return self;
}

