/**
 * FWD - FAST WEB DEVELOPMENT
 * 
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/*******************************************************************************
 * Classe Radiobox. Especialização de Element que define o elemento Radiobox.
 * 
 * <p>Especialização de Element que define o elemento Radiobox. Além dos
 * atributos de Element, possui os atributos controllername e key.</p>
 *
 * @package FID
 * @param String name Nome do elemento
 *******************************************************************************/
function Radiobox(name){
  var self = new Element(name);
  self.elementType = 'Radiobox';
  self.defaultClassName = 'unselected';
  var htmlElement = self.createHtmlElement('DIV');
  htmlElement.style.backgroundRepeat = "no-repeat";
  htmlElement.style.backgroundPosition = "left top";
  htmlElement.style.backgroundImage = 'url(imgs/radio.png)';
  self.addAttribute('controller','hidden',false,'controller');
  self.addAttribute('key','text',false,'value');
  self.setAttribute('key',name);
  self.setAttribute('class','');
  return self;
}

