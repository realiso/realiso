/**
 * FWD - FAST WEB DEVELOPMENT
 * 
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/*******************************************************************************
 * Classe Element. Classe abstrata que serve de base para todos elementos.
 * 
 * <p>Classe abstrata que serve de base para todos elementos da FID.</p>
 * @package FID
 * @param String name Nome do elemento
 *******************************************************************************/
function Element(name){

 /**
  * Refer�ncia para si mesmo.
  * @var Element
  */
  var self = this;
  
 /**
  * Nome do elemento.
  * @var String
  */
  this.name = name;
  
 /**
  * Id do elemento.
  * @var String
  */
  this.id = name;
  
 /**
  * Refer�ncia para o pai do elemento, caso ele exista.
  * @var Element
  */
  this.parent = null;
  
 /**
  * Classe CSS do elemento HTML quando ele n�o est� selecionado.
  * @var String
  */
  this.defaultClassName = 'unselected';
  
 /**
  * Lista dos atributos do elemento.
  * @var Array
  */
  this.attributes = new Array();

 /**
  * @var String Indica o nome da classe do elemento.
  * OBS: Todas classes de elementos devem ter esse campo id�ntico ao seu nome.
  */
  this.elementType = 'Element';

 /**
  * Indica se a tela (e nao o elemento) foi carregada do XML
  * @var boolean
  */
  this.loadedFromXML = false;
  
  /**
   * Indica se o elemento tem tabindex
   * @var boolean
   */
  this.hasTabIndex = false;
  
  /**
   * Indica se o nome do elemento foi gerado automaticamente pela FID
   * @var boolean
   */
  this.autoname = false;
  
 /**
  * Seta o valor do atributo autoname
  *
  * <p>Seta o valor do atributo autoname, que indica se o nome do elemento foi
  * gerado automaticamente pela FID.</p>
  * @param boolean value novo valor do atributo
  */
  this.setAutoname = function(value){
    this.autoname = value;
  }

 /**
  * Array associativo de atributos fantasma. Atributos fantasma servem para que
  * atributos desconhecidos pela FID, carregados do XML, n�o sejam perdidos.
  * @var Array
  */
  this.ghostAttributes = new Array();
  
 /**
  * Array de elementos fantasma. Elementos fantasma servem para que elementos de
  * tipos desconhecidos pela FID, carregados do XML, n�o sejam perdidos.
  * @var Array
  */
  this.ghostChildren = new Array();
  
 /**
  * Valores default pros atributos da box. Usado quando eles s�o omitidos no XML.
  * @var Object
  */
  this.boxDefaultAttributes = {'top':0,'left':0,'width':100,'height':20};
  
 /*****************************************************************************
  * Adiciona um atributo � classe.
  *
  * <p>Adiciona um atributo � classe (usado na defini��o de sub-classes)</p>
  * 
  * @param String name Nome do atributo.
  * @param String type Nome do tipo do atributo.
  * @param boolean special Indica se o atributo � especial, ou seja, se na
  *        gera��o de XML ele deve receber tratamento especial.
  * @param String arg4 Nome do atributo real para o qual o atributo deve ser
  *        mapeado.
  * @param function arg4 Fun��o que seta o valor do atributo.
  * @param function getFunc Fun��o que retorna o valor do atributo. [opcional]
  *****************************************************************************/
  this.addAttribute = function(name,type,special,arg4,arg5,arg6){
    var maOptions = null;
    if(typeof(arg4)=='string'){
      realAtrribute = arg4;
      eval('setFunc = function(value){ gebi("'+this.id+'").'+realAtrribute+' = value; }');
      if(type=='pixel'){
        eval('getFunc = function(){ return parseInt(gebi("'+this.id+'").'+realAtrribute+'); }');
      }else{
        eval('getFunc = function(){ return gebi("'+this.id+'").'+realAtrribute+'; }');
      }
      if(type=='select'){
        maOptions = arg5;
      }
    }else{
      setFunc = arg4;
      getFunc = arg5;
      if(type=='select'){
        maOptions = arg6;
      }
    }
    this.attributes[name] = new Attribute(name,type,special,setFunc,getFunc,maOptions);
  }
  
 /******************************************************************************
  * Adiciona um atributo fantasma ao elemento.
  *
  * <p>Adiciona um atributo fantasma ao elemento. Atributos fantasma servem para
  * que atributos desconhecidos pela FID, carregados do XML, n�o sejam perdidos.
  * </p>
  * 
  * @param String name Nome do atributo.
  * @param String value Valor do atributo.
  *****************************************************************************/
  this.addGhostAttribute = function(name,value){
    this.ghostAttributes[name] = value;
  }

 /******************************************************************************
  * Adiciona um elemento fantasma ao elemento.
  *
  * <p>Adiciona um elemento fantasma ao elemento. Elementos fantasma servem para
  * que elementos de tipos desconhecidos pela FID, carregados do XML, n�o sejam
  * perdidos.</p>
  * 
  * @param XMLDocument child Elemento fantasma
  *****************************************************************************/
  this.addGhostChild = function(child){
    this.ghostChildren.push(child);
  }

 /******************************************************************************
  * Retorna uma string representando uma lista de atributos.
  *
  * <p>Retorna uma string contendo um trecho de c�digo XML correspondente a
  * lista de atributos dada.</p>
  * 
  * @param NamedNodeMap attributes Lista de atributos
  * @return String String contendo os atributos
  *****************************************************************************/
  this.attributesToString = function(attributes){
    var xmlCode = '';
    var nodeName;
    var nodeValue;
    for(var i=0;i<attributes.length;i++){
      nodeName = attributes[i].nodeName.toLowerCase();
      nodeValue = attributes[i].nodeValue;
      xmlCode+= ' '+nodeName+'="'+nodeValue+'"';
    }
    return xmlCode
  }

 /*****************************************************************************
  * Cria o elemento HTML que representa o elemento.
  *
  * <p>Cria o elemento HTML que representa o elemento (usado na defini��o de
  * sub-classes)</p>
  * 
  * @param String tag Tag do elemento a ser criado.
  * @param String type Tipo do elemento, no caso de um input. [opcional]
  *****************************************************************************/
  this.createHtmlElement = function(tag,type){
    var htmlElement = document.createElement(tag);
    if(type) htmlElement.type = type;
    htmlElement.name = this.name;
    htmlElement.id = this.id;
    htmlElement.elementObject = this;
    htmlElement.style.position = 'absolute';
    htmlElement.readOnly = true;
    htmlElement.className = this.defaultClassName;
    if(this.onkeydown!=undefined) htmlElement.onkeydown = this.onkeydown;
    document.getElementsByTagName('body').item(0).appendChild(htmlElement);
    return htmlElement;
  }
  
  // Indica que o nome do elemento foi gerado automaticamente e, portanto, n�o aparece no XML.
  self.setAutoname(name);
  
  // Adiciona os atributos basicos
  eval('setName = function(name){'
      +  'var element = gebi("'+self.id+'");'
      +  'element.name = name;'
      +  'element.elementObject.setAutoname(false);' // Indica que o nome do elemento N�O foi gerado automaticamente.
      +'}');
  eval('getName = function(){ return gebi("'+self.id+'").name; }');
  self.addAttribute('name','text',false,setName,getName);
  this.addAttribute('top','pixel',true,'style.top');
  this.addAttribute('left','pixel',true,'style.left');
  this.addAttribute('height','pixel',true,'style.height');
  this.addAttribute('width','pixel',true,'style.width');
  // Define o atributo align
  eval('setFunc = function(value){'
      +  'var element = gebi("'+self.id+'").elementObject;'
      +  'if(element.align(value)) element.attributes["align"].value = value;'
      +'}');
  eval('getFunc = function(){ return gebi("'+self.id+'").elementObject.attributes["align"].value; }');
  self.addAttribute('align','alignment',false,setFunc,getFunc);
  self.addAttribute('class','text',false,'attrClassName');
  
 /*****************************************************************************
  * Determina a posi��o do elemento.
  *
  * <p>Determina a posi��o do elemento na tela.</p>
  * 
  * @param integer x Coordenada x do canto superior esquerdo do elemento.
  * @param integer y Coordenada y do canto superior esquerdo do elemento.
  *****************************************************************************/
  this.setPosition = function(x,y){
    this.setAttribute('top',y);
    this.setAttribute('left',x);
  }
  
 /*****************************************************************************
  * Move o elemento
  *
  * <p>Move o elemento de acordo com o deslocamento indicado.</p>
  * 
  * @param integer offsetX Deslocamento na coordenada x
  * @param integer offsetY Deslocamento na coordenada y
  *****************************************************************************/
  this.move = function(offsetX,offsetY){
    this.setPosition(parseInt(this.getAttribute('left'))+offsetX, parseInt(this.getAttribute('top'))+offsetY);
  }
  
 /*****************************************************************************
  * Alinha o elemento � grade.
  *
  * <p>Alinha o elemento � grade.</p>
  *****************************************************************************/
  this.alignToGrid = function(){
    var miLeft = parseInt(this.getAttribute('left'));
    var miTop = parseInt(this.getAttribute('top'));
    this.setAttribute('left',fid.alignToGrid(miLeft));
    this.setAttribute('top',fid.alignToGrid(miTop));
  }
  
 /*****************************************************************************
  * Define o tamanho do elemento
  *
  * <p>Define o tamanho do elemento.</p>
  * 
  * @param integer height Altura do elemento
  * @param integer width Largura do elemento
  *****************************************************************************/
  this.setSize = function(height,width){
    this.setAttribute('height',height);
    this.setAttribute('width',width);
  }
  
 /*****************************************************************************
  * Alinha o elemento de acordo com o pai
  *
  * <p>Alinha o elemento de acordo com o pai.</p>
  * 
  * @param String alignment Pode ser <code>top</code>, <code>bottom</code>,
  *        <code>left</code> ou <code>right</code>
  *****************************************************************************/
  self.align = function(alignment){
    if(self.parent!=null && (alignment=='top' || alignment=='bottom' || alignment=='left' || alignment=='right')){
      var parentTop = parseInt(self.parent.getAttribute('top'));
      var parentLeft = parseInt(self.parent.getAttribute('left'));
      if(alignment!='left') var parentWidth = parseInt(self.parent.getAttribute('width'));
      if(alignment!='top')  var parentHeight = parseInt(self.parent.getAttribute('height'));
      if(alignment=='bottom'){
        var height = parseInt(self.getAttribute('height'));
        self.setPosition(parentLeft, parentTop + parentHeight - height);
        self.setAttribute('width',parentWidth);
      }else if(alignment=='right'){
        var width = parseInt(self.getAttribute('width'));
        self.setPosition(parentLeft + parentWidth - width,parentTop);
        self.setAttribute('height',parentHeight);
      }else{
        self.setPosition(parentLeft,parentTop);
        if(alignment=='top') self.setAttribute('width',parentWidth);
        else self.setAttribute('height',parentHeight);
      }
      return true;
    }else return false;
  }
  
 /*****************************************************************************
  * Verifica se o elemento possui o atributo
  *
  * <p>Verifica se o elemento possui o atributo especificado.</p>
  * 
  * @param String attributeName Nome do atributo.
  * @return boolean Indica a exist�ncia ou n�o do atributo.
  *****************************************************************************/
  this.hasAttribute = function(attributeName){
    return (this.attributes[attributeName]!=undefined);
  }
  
 /*****************************************************************************
  * Define o valor de um atributo
  *
  * <p>Define o valor de um atributo.</p>
  * 
  * @param String attributeName Nome do atributo.
  * @param mixed value Valor do atributo.
  *****************************************************************************/
  this.setAttribute = function(attributeName,value){
    if(this.hasAttribute(attributeName)){
      var type = this.attributes[attributeName].type;
      var regExp;
      switch(type){
        case 'pixel':     regExp = /^[0-9]+(px)?$/;           break;
        case 'integer':   regExp = /^(-?)[0-9]+$/;            break;
        case 'text':      regExp = /^(.*\s*)*$/;              break;
        case 'alignment': regExp = /^top|bottom|left|right$/; break;
        case 'list':      regExp = /^(.*\s*)*$/m;             break;
        case 'boolean':   regExp = /^true|false$/;            break;
        case 'hidden':    regExp = /^(.*\s*)*$/m;             break;
        case 'select':    regExp = /^(.*\s*)*$/m;             break;
        case 'color':     regExp = /^([0-9A-F]{6})|$/m;       break;
        default:
          alert("Erro: O tipo de atributo '"+type+"' n�o existe.");
          return false;
      }
      if(!regExp.test(value)) return false;
      this.attributes[attributeName].setValue(value);
      return true;
    }else{
      alert("Erro: O elemento '"+this.name+"' n�o possui o atributo '"+attributeName+"'");
      return false;
    }
  }
  
 /*****************************************************************************
  * Retorna o valor do atributo
  *
  * <p>Retorna o valor do atributo especificado.</p>
  * 
  * @param String attributeName Nome do atributo.
  * @return mixed Valor do atributo.
  *****************************************************************************/
  this.getAttribute = function(attributeName){
    if(this.hasAttribute(attributeName)){
      return this.attributes[attributeName].getValue();
    }else{
      alert("O elemento '"+this.name+"' n�o possui o atributo '"+attributeName+"'");
      return undefined;
    }
  }
  
 /*****************************************************************************
  * Retorna o nome do tipo do atributo.
  *
  * <p>Retorna o nome do tipo do atributo especificado.</p>
  * 
  * @param String attributeName Nome do atributo.
  * @return String Nome do tipo do atributo.
  *****************************************************************************/
  this.getAttributeType = function(attributeName){
    if(this.hasAttribute(attributeName)){
      return this.attributes[attributeName].type;
    }else{
      alert("O elemento '"+this.name+"' n�o possui o atributo '"+attributeName+"'");
      return undefined;
    }
  }
  
 /*****************************************************************************
  * Retorna uma representa��o do caminho do elemento
  *
  * <p>Retorna uma string contendo uma representa��o do caminho desde o
  * elemento raiz at� o pr�prio elemento.</p>
  * 
  * @return String Caminho do elemento
  *****************************************************************************/
  this.getPath = function(){
    var path = this.name;
    var element = this;
    while(element.parent!=null){
      path = element.parent.name+'/'+path;
      element = element.parent;
    }
    return path;
  }
  
 /*****************************************************************************
  * Destr�i o elemento
  *
  * <p>Destr�i o elemento.</p>
  *****************************************************************************/
  this.destroy = function(){
    if(this.parent!=null) delete this.parent.children[this.name];
    var htmlElement = gebi(this.id);
    htmlElement.parentNode.removeChild(htmlElement);
  }
  
 /*****************************************************************************
  * Indica a posi��o de um ponto em rela��o ao elemento.
  *
  * <p>Indica a posi��o do ponto especificado em rela��o ao elemento.</p>
  * <p>Os valores poss�veis de retorno s�o:</p>
  * <p>'n', 's', 'e', 'w', 'ne', 'nw', 'se' e 'sw': indicam que o ponto est�
  * sobre uma borda do elemento (usa os pontos cardeais e colaterais para
  * indicar qual borda)</p>
  * <p>'center': indica que o ponto est� dentro do elemento</p>
  * <p>null: indica que o ponto est� fora do elemento</p>
  * 
  * @param integer x Coordenada x do ponto.
  * @param integer y Coordenada y do ponto.
  * @return String Posi��o do ponto
  *****************************************************************************/
  this.positionOf = function(x,y){
    var treshold = 3;
    var position;
    var top = parseInt(this.getAttribute('top'));
    var left = parseInt(this.getAttribute('left'));
    var height = parseInt(this.getAttribute('height'));
    var width = parseInt(this.getAttribute('width'));
    var relX = x - left;
    var relY = y - top;
    if(relX >= -treshold && relY >= -treshold && relX <= width+treshold && relY <= height+treshold){
      // posicao vertical
      if(relY <= treshold) position = 'n';
      else if(relY >= height - treshold) position = 's';
      else position = '';
      // posicao horizontal
      if(relX <= treshold) position+= 'w';
      else if(relX >= width - treshold) position+= 'e';
      if(position=='') return 'center';
      else return position;
    }else return null;
  }
  
 /*****************************************************************************
  * Testa se o elemento colide com um determinado ponto ou ret�ngulo
  *
  * <p>Testa se o elemento colide com o ret�ngulo definido por (x0,y0) e (x1,y1)</p>
  * <p>Se x1 e y1 forem omitidos, testa colis�o com o ponto (x0,y0)</p>
  * 
  * @param integer x0 Coordenada x inicial do ret�ngulo.
  * @param integer y0 Coordenada y inicial do ret�ngulo.
  * @param integer x1 Coordenada x final do ret�ngulo. [opcional]
  * @param integer y1 Coordenada y final do ret�ngulo. [opcional]
  * @return boolean Indica se o elemento colide como o ret�ngulo/ponto.
  *****************************************************************************/
  this.colideWith = function(x0,y0,x1,y1){
    if(!x1) x1 = x0;
    var left = parseInt(this.getAttribute('left'));
    if(x0<left && x1<left) return false;
    var right = left + parseInt(this.getAttribute('width'));
    if(x0>right && x1>right) return false;
    if(!y1) y1 = y0;
    var top = parseInt(this.getAttribute('top'));
    if(y0<top && y1<top) return false;
    var bottom = top + parseInt(this.getAttribute('height'));
    if(y0>bottom && y1>bottom) return false;
    return true;
  }

 /*****************************************************************************
  * Testa se o elemento est� dentro de um determinado ret�ngulo.
  *
  * <p>Testa se o elemento est� dentro de um determinado ret�ngulo.</p>
  * 
  * @param integer x0 Coordenada x inicial do ret�ngulo.
  * @param integer y0 Coordenada y inicial do ret�ngulo.
  * @param integer x1 Coordenada x final do ret�ngulo.
  * @param integer y1 Coordenada y final do ret�ngulo.
  * @return boolean Indica se o elemento est� dentro do ret�ngulo
  *****************************************************************************/
  this.isInsideRect = function(x0,y0,x1,y1){
    var top = parseInt(self.getAttribute('top'));
    if(top < y0) return false;
    var left = parseInt(self.getAttribute('left'));
    if(left < x0) return false;
    var bottom = top + parseInt(self.getAttribute('height'));
    if(bottom > y1) return false;
    var right = left + parseInt(self.getAttribute('width'));
    if(right > x1) return false;
    return true;
  }

 /*****************************************************************************
  * Testa se o elemento est� dentro de outro
  *
  * <p>Testa se o elemento est� inteiramente contido pelo elemento
  * especificado.</p>
  * 
  * @param Element element Elemento a ser testado.
  * @return boolean Indica se o elemento est� contido.
  *****************************************************************************/
  this.isInside = function(element){
    if(element==this) return false;
    var top = parseInt(this.getAttribute('top'));
    var elemTop = parseInt(element.getAttribute('top'));
    if(top < elemTop) return false;
    
    var left = parseInt(this.getAttribute('left'));
    var elemLeft = parseInt(element.getAttribute('left'));
    if(left < elemLeft) return false;
    
    var height = parseInt(this.getAttribute('height'));
    var elemHeight = parseInt(element.getAttribute('height'));
    if(top+height > elemTop+elemHeight) return false;
    
    var width = parseInt(this.getAttribute('width'));
    var elemWidth = parseInt(element.getAttribute('width'));
    if(left+width > elemLeft+elemWidth) return false;
    
    return true;
  }
  
 /*****************************************************************************
  * Reposiciona e/ou redimensiona o elemento para que ele caiba num ret�ngulo
  * especificado.
  *
  * <p>Reposiciona e/ou redimensiona o elemento para que ele caiba num ret�ngulo
  * especificado.</p>
  * @param integer x0 Coordenada x inicial do ret�ngulo.
  * @param integer y0 Coordenada y inicial do ret�ngulo.
  * @param integer x1 Coordenada x final do ret�ngulo.
  * @param integer y1 Coordenada y final do ret�ngulo.
  *****************************************************************************/
  self.fitIn = function(x0,y0,x1,y1){
    var top = parseInt(self.getAttribute('top'));
    var left = parseInt(self.getAttribute('left'));
    var height = parseInt(self.getAttribute('height'));
    var width = parseInt(self.getAttribute('width'));
    var right = left + width;
    var bottom = top + height;
    var dx = x1 - x0;
    var dy = y1 - y0;
    if(left < x0){
      self.setAttribute('left',x0);
      if(width > dx) self.setAttribute('width',dx);
    }else if(right > x1){
      if(width > dx){
        self.setAttribute('width',dx);
        self.setAttribute('left',x0);
      }else{
        self.setAttribute('left',x1-width);
      }
    }
    if(top < y0){
      self.setAttribute('top',y0);
      if(height > dy) self.setAttribute('height',dy);
    }else if(bottom > y1){
      if(height > dy){
        self.setAttribute('height',dy);
        self.setAttribute('top',y0);
      }else{
        self.setAttribute('top',y1-height);
      }
    }
  }
  
 /*****************************************************************************
  * Retorna o codigo XML dos atributos do elemento
  *
  * <p>Retorna o codigo XML dos atributos do elemento. Cont�m todos atributos
  * n�o-especiais mais o pseudo-atributo tabindex.</p>
  * 
  * @param TabIndex tabIndex Objeto TabIndex que cont�m o tabindex do elemento.
  * @return String Trecho de c�digo XML contendo somente a lista de atributos
  *         do elemento.
  *****************************************************************************/
  this.getXMLAttributes = function(tabIndex){
    var xmlCode = '';
    var attributeName,attributeValue;
    for(attributeName in this.attributes){
      if(!this.attributes[attributeName].special){
        attributeValue = this.getAttribute(attributeName);
        if(attributeName=='name'){
          if(!this.autoname){
            xmlCode+= ' name="'+this.getAttribute('name')+'"';
          }
        }else if(attributeValue){
          xmlCode+= ' '+attributeName.toLowerCase()+'="'+this.getAttribute(attributeName)+'"';
        }
      }
    }
    for(attributeName in this.ghostAttributes){
      xmlCode+= ' '+attributeName+'="'+this.ghostAttributes[attributeName]+'"';
    }
    if(this.hasTabIndex && !this.loadedFromXML) xmlCode+= ' tabindex="'+tabIndex+'"';
    return xmlCode;
  }
  
 /*****************************************************************************
  * Retorna o c�digo XML da box do elemento
  *
  * <p>Retorna o c�digo XML da box do elemento.</p>
  * 
  * @param String ident Identa��o do elemento no c�digo XML.
  * @return String Trecho de c�digo XML contendo o elemento box do elemento.
  *****************************************************************************/
  this.getBoxCode = function(ident){
    var xmlCode = ident+'<box';
    var attributes = new Array();
    attributes['top'] = parseInt(this.getAttribute('top'));
    attributes['left'] = parseInt(this.getAttribute('left'));
    attributes['height'] = parseInt(this.getAttribute('height'));
    attributes['width'] = parseInt(this.getAttribute('width'));
    if(this.parent!=null){
      attributes['top']-= parseInt(this.parent.getAttribute('top'));
      attributes['left']-= parseInt(this.parent.getAttribute('left'));
    }
    for(var attrName in attributes){
      if(!isNaN(attributes[attrName])){
        xmlCode+= ' '+attrName+'="'+attributes[attrName]+'"';
      }
    }
    xmlCode+= '/>\n';
    return xmlCode;
  }
  
 /*****************************************************************************
  * Retorna o c�digo XML de um elemento fantasma.
  *
  * <p>Retorna o c�digo XML de um elemento fantasma.</p>
  * 
  * @param XMLDocument element Elemento fantasma
  * @param String ident Identa��o do elemento no c�digo XML.
  * @return String Trecho de c�digo XML contendo o elemento.
  *****************************************************************************/
  this.getXMLGhostElement = function(element,ident){
    var xmlCode = ident+'<'+element.tagName;
    if(element.attributes){
      for(var i=0;i<element.attributes.length;i++){
        xmlCode+= ' '+element.attributes[i].nodeName+'="'+element.attributes[i].nodeValue+'"';
      }
    }
    if(element.tagName=='string'){
      xmlCode+= '>';
      for(var i=0;i<element.childNodes.length;i++){
        if(element.childNodes[i].tagName){
          xmlCode+= this.getXMLGhostElement(element.childNodes[i],'').replace(/[\n\s]+$/,'','g');
        }else{
          var msValue = element.childNodes[i].nodeValue;
          msValue = msValue.replace('&','&amp;','g');
          msValue = msValue.replace('<','&lt;','g');
          msValue = msValue.replace('>','&gt;','g');
          msValue = msValue.replace('\t','  ','g');
          msValue = msValue.replace(/^[\n\s]+/,'','g');
          msValue = msValue.replace(/[\n\s]+$/,'','g');
          xmlCode+= msValue;
        }
      }
      xmlCode+= '</'+element.tagName+'>\n';
    }else{
      if(element.childNodes.length==0){ // vazio
        xmlCode+= '/>\n';
      }else if(element.childNodes.length==1 && !element.firstChild.tagName){ // texto
        var msValue = element.firstChild.nodeValue;
        msValue = msValue.replace('&','&amp;','g');
        msValue = msValue.replace('<','&lt;','g');
        msValue = msValue.replace('>','&gt;','g');
        msValue = msValue.replace('\t','  ','g');
        xmlCode+= '>'+msValue+'</'+element.tagName+'>\n';
      }else{ // com sub-elementos
        xmlCode+= '>\n';
        for(var i=0;i<element.childNodes.length;i++){
          if(element.childNodes[i].tagName){
            xmlCode+= this.getXMLGhostElement(element.childNodes[i],'  '+ident);
          }
        }
        xmlCode+= ident+'</'+element.tagName+'>\n';
      }
    }
    return xmlCode;
  }
  
 /*****************************************************************************
  * Retorna o c�digo XML dos filhos fantasma do elemento.
  *
  * <p>Retorna o c�digo XML dos filhos fantasma do elemento.</p>
  * 
  * @param String ident Identa��o dos elementos no c�digo XML.
  * @return String Trecho de c�digo XML contendo os elementos fantasma.
  *****************************************************************************/
  this.getXMLGhostChildren = function(ident){
    var xmlCode = '';
    for(var i=0;i<this.ghostChildren.length;i++){
      xmlCode+= this.getXMLGhostElement(this.ghostChildren[i],ident);
    }
    return xmlCode;
  }
  
 /*****************************************************************************
  * Retorna o c�digo XML do conte�do do elemento
  *
  * <p>Retorna o c�digo XML do conte�do do elemento.</p>
  * 
  * @param String ident Identa��o do conte�do do elemento no c�digo XML.
  * @return String Trecho de c�digo XML contendo todo o conte�do do elemento.
  *****************************************************************************/
  this.getXMLContent = function(ident){
    var xmlCode = '';
    xmlCode+= this.getBoxCode(ident);
    xmlCode+= this.getXMLGhostChildren(ident);
    return xmlCode;
  }
  
 /*****************************************************************************
  * Retorna o c�digo XML do elemento
  *
  * <p>Retorna o c�digo XML do elemento</p>
  * 
  * @param String ident Identa��o do elemento no c�digo XML.
  * @param TabIndex tabIndex Objeto TabIndex que cont�m o tabindex do elemento.
  * @return String C�digo XML do elemento.
  *****************************************************************************/
  this.getXMLCode = function(psIdent,poTabIndex){
    var msTag = this.elementType.toLowerCase();
    var msXmlCode = '';
    var msXmlContent = this.getXMLContent(psIdent+'  ',poTabIndex);
    if(msXmlContent){
      msXmlCode+= psIdent+'<'+msTag+this.getXMLAttributes(poTabIndex)+'>\n'
                         +msXmlContent
                 +psIdent+'</'+msTag+'>\n';
    }else{
      msXmlCode+= psIdent+'<'+msTag+this.getXMLAttributes(poTabIndex)+'/>\n';
    }
    return msXmlCode;
  }
  
 /*****************************************************************************
  * Preenche os atributos do elemento a partir do XML
  *
  * <p>Preenche os atributos do elemento a partir de sua representa��o em XML
  * (objeto XMLDocument, n�o texto XML)</p>
  * <p>OBS: Atributos desconhecidos s�o ignorados.</p>
  * 
  * @param NamedNodeMap xmlAttributes Objeto contendo os atributos do elemento.
  *****************************************************************************/
  this.loadAttributesFromXML = function(xmlAttributes){
    var nodeName;
    var nodeValue;
    for(var i=0;i<xmlAttributes.length;i++){
      nodeName = xmlAttributes[i].nodeName.toLowerCase();
      nodeValue = xmlAttributes[i].nodeValue;
      if(this.hasAttribute(nodeName)){
        this.setAttribute(nodeName,nodeValue);
      }else{
        this.addGhostAttribute(nodeName,nodeValue);
      }
    }
  }
  
 /*****************************************************************************
  * Carrega os atributos do elemento contidos em sua box a partir do XML
  *
  * <p>Carrega os atributos do elemento contidos em sua box a partir de seu XML
  * (objeto XMLDocument, n�o texto XML)</p>
  * 
  * @param XMLDocument xmlDom Objeto contendo a representa��o da box do
  *        elemento em XML.
  *****************************************************************************/
  this.loadBoxFromXML = function(xmlBox){
    var boxAttributes = new Object();
    var value;
    for(var j=0;j<xmlBox.attributes.length;j++){
      value = parseInt(xmlBox.attributes[j].nodeValue);
      if(value<0) value = 0;
      boxAttributes[xmlBox.attributes[j].nodeName] = value;
    }
    for(j in self.boxDefaultAttributes){
      if(boxAttributes[j]){
        self.setAttribute(j,boxAttributes[j]);
      }else{
        self.setAttribute(j,self.boxDefaultAttributes[j]);
      }
    }
  }
  
 /*****************************************************************************
  * Preenche o elemento a partir do XML
  *
  * <p>Preenche os dados do elemento a partir de sua representa��o em XML
  * (objeto XMLDocument, n�o texto XML)</p>
  * 
  * @param XMLDocument xmlElement Objeto contendo a representa��o do elemento
  *        em XML.
  * @param Object parentBox Array associativo contendo os atributos da box do
  *        container pai.
  *****************************************************************************/
  this.loadFromXML = function(xmlElement,parentBox){
    self.loadedFromXML = true;
    self.ghostAttributes = new Array();
    self.ghostChildren = new Array();
    if(xmlElement.tagName && xmlElement.tagName.toLowerCase()==self.elementType.toLowerCase()){
      var xmlChildren = xmlElement.childNodes;
      self.loadAttributesFromXML(xmlElement.attributes);
      var foundBox = false;
      for(var i=0;i<xmlChildren.length;i++){
        if(xmlChildren[i].tagName){
          if(xmlChildren[i].tagName=='box'){
            self.loadBoxFromXML(xmlChildren[i]);
            foundBox = true;
          }else{
            self.addGhostChild(xmlChildren[i]);
          }
        }
      }
      if(!foundBox){
        for(var j in self.boxDefaultAttributes){
          self.setAttribute(j,self.boxDefaultAttributes[j]);
        }
      }
      self.move(parentBox['left'],parentBox['top']);
      self.fitIn(parentBox['left'],parentBox['top'],parentBox['right'],parentBox['bottom']);
      return true;
    }else return false;
  }

 /*****************************************************************************
  * Retorna uma representa��o em forma de string do elemento
  *
  * <p>Retorna uma representa��o em forma de string do elemento contendo
  * somente alguns atributos dele (usado somente para debug).</p>
  * 
  * @return String String contendo alguns atributos do elemento.
  *****************************************************************************/
  this.toString = function(){
    var msStr = 'name: '+this.name+'\n';
    var miTop    = parseInt(this.getAttribute('top'));
    var miLeft   = parseInt(this.getAttribute('left'));
    var miHeight = parseInt(this.getAttribute('height'));
    var miWidth  = parseInt(this.getAttribute('width'));
    var miBottom = miTop + miHeight;
    var miRight  = miLeft + miWidth;
    msStr+= 'top: '    + miTop    + '\n';
    msStr+= 'left: '   + miLeft   + '\n';
    msStr+= 'height: ' + miHeight + '\n';
    msStr+= 'width: '  + miWidth  + '\n';
    msStr+= 'bottom: ' + miBottom + '\n';
    msStr+= 'right: '  + miRight  + '\n';
    return msStr;
  }
  
}