/**
 * FWD - FAST WEB DEVELOPMENT
 * 
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/*******************************************************************************
 * Classe Icon. Especializa��o de Element que define o elemento Icon.
 * 
 * <p>Especializa��o de Element que define o elemento Icon.</p>
 *
 * @package FID
 * @param String name Nome do elemento
 *******************************************************************************/
function Icon(name){
  var self = new Element(name);
  self.elementType = 'Icon';
  self.defaultClassName = 'unselected';
  var htmlElement = self.createHtmlElement('DIV');
  htmlElement.style.backgroundRepeat = "no-repeat";
  htmlElement.style.backgroundPosition = "left top";
  htmlElement.style.backgroundImage = 'url(imgs/icon.png)';
  self.setAttribute('class','');
  
  self.csSrc = '';
  
  self.setSrc = function(psSrc){
    self.csSrc = psSrc;
    var Parts = psSrc.split('\\');
    if( Parts.length < 2 )
    Parts = psSrc.split('/');
    psSrc = Parts[ Parts.length -1 ];
    htmlElement.style.backgroundImage = 'url(icons/'+psSrc+')';
  }
  
  self.getSrc = function(){
    return self.csSrc;
  }
  
  setFunc = function(value){ self.setSrc(value); };
  getFunc = function(){ return self.getSrc(); }
  self.addAttribute('src','text',false,setFunc,getFunc);
  
 /*****************************************************************************
  * Retorna o codigo XML dos atributos do elemento
  *
  * <p>Retorna o codigo XML dos atributos do elemento. Cont�m todos atributos
  * n�o-especiais mais o pseudo-atributo tabindex.</p>
  * 
  * @param TabIndex tabIndex Objeto TabIndex que cont�m o tabindex do elemento.
  * @return String Trecho de c�digo XML contendo somente a lista de atributos
  *         do elemento.
  *****************************************************************************/
  self.getXMLAttributes = function(tabIndex){
    var xmlCode = '';
    var attributeName,attributeValue;
    for(attributeName in self.attributes){
      if(!self.attributes[attributeName].special){
        attributeValue = self.getAttribute(attributeName);
        if(attributeName=='name'){
          if(!self.autoname){
            xmlCode+= ' name="'+self.getAttribute('name')+'"';
          }
        }else if(attributeValue!=undefined){
          if(attributeName.toLowerCase()=='src'){
            xmlCode+= ' src="'+fid.replaceEntities(self.getAttribute('src'))+'"';
          }else if(attributeValue){
            xmlCode+= ' '+attributeName.toLowerCase()+'="'+self.getAttribute(attributeName)+'"';
          }
        }
      }
    }
    for(attributeName in self.ghostAttributes){
      xmlCode+= ' '+attributeName+'="'+self.ghostAttributes[attributeName]+'"';
    }
    if(self.hasTabIndex && !self.loadedFromXML) xmlCode+= ' tabindex="'+tabIndex+'"';
    return xmlCode;
  }
  
  return self;
}
