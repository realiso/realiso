/**
 * FWD - FAST WEB DEVELOPMENT
 * 
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/*******************************************************************************
 * Classe SpecialElement. Extende Element para definir elementos especiais.
 * 
 * <p>Classe abstrata que extende Element para definir elementos especiais.
 * Elementos especiais s�o elementos da interface da FID e que n�o s�o tratados
 * pela FID da mesma maneira que os outros.</p>
 *
 * @package FID
 * @param String name Nome do elemento
 *******************************************************************************/
function SpecialElement(name){
  var self = new Element(name);
  
 /**
  * Indica se o elemento est� vis�vel
  * @var boolean
  */
  self.visible = true;
  
 /**
  * Indica se o elemento capturou um evento de teclado
  * @var boolean
  */
  self.keyboardEventCaptured = false;
  
 /**
  * Indica se o elemento capturou um evento de mouse
  * @var boolean
  */
  self.mouseEventCaptured = false;
  
 /*****************************************************************************
  * Intercepta os eventos de teclado sobre o elemento
  *
  * <p>Sinaliza que interceptou um evento de teclado. A responsabilidade de
  * verificar a ocorr�ncia dessa intercepta��o e trat�-la � da FID. No pr�prio
  * objeto esse m�todo n�o tem efeito, mas ao criar o elemento HTML, usando o
  * m�todo createHtmlElement(), ele � copiado para o elemento HTML.</p>
  * 
  * @param KeyboardEvent event Evento de teclado
  *****************************************************************************/
  self.onkeydown = function(event){
    self.keyboardEventCaptured = true;
  }
  
 /*****************************************************************************
  * Torna o elemento vis�vel.
  *
  * <p>Torna o elemento vis�vel.</p>
  *****************************************************************************/
  self.show = function(){
    self.visible = true;
    gebi(self.id).style.visibility = 'visible';
  }
  
 /*****************************************************************************
  * Torna o elemento invis�vel.
  *
  * <p>Torna o elemento invis�vel.</p>
  *****************************************************************************/
  self.hide = function(){
    self.visible = false;
    gebi(self.id).style.visibility = 'hidden';
  }
  
  return self;
}
