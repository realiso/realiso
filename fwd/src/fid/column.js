/**
 * FWD - FAST WEB DEVELOPMENT
 * 
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/*******************************************************************************
 * Classe Column. Define uma coluna da Grid.
 * 
 * <p>Define uma coluna da Grid. Essa classe s� � usada internamente � Grid e,
 * portanto, n�o tem um elemento HTML que a represente.</p>
 * @package FID
 * @param String name Nome da coluna
 * @param integer width Largura da coluna
 *******************************************************************************/
function Column(name,width){
  var self = new Element(name);
  self.elementType = 'Column';
  self.hasTabIndex = false;
  if(!width) width = '';
  
 /**
  * Largura da coluna
  * @var integer
  */
  self.width = width;

 /**
  * Altura do cabe�alho da coluna
  * @var integer
  */
  self.height = '';

 /**
  * Objeto que guarda as informa��es sobre o static que cont�m a string que cont�m o nome da coluna
  * @var Object
  */
  self.name = new Object();

  self.name.staticAttributes = new Array();
  self.name.stringId = name+'String';
  self.name.stringAttributes = new Array();
  self.name.value = name;
  
 /*****************************************************************************
  * Retorna a largura da coluna
  *
  * <p>Retorna a largura da coluna. Fun��o de callback para o par�metro
  * width.</p>
  * @return integer Largura da coluna
  *****************************************************************************/
  self.getWidth = function(){
    return self.width;
  }

 /*****************************************************************************
  * Seta a largura da coluna
  *
  * <p>Seta a largura da coluna. Fun��o de callback para o par�metro width.</p>
  * @param integer width Nova largura da coluna
  *****************************************************************************/
  self.setWidth = function(width){
    self.width = width;
  }

 /*****************************************************************************
  * Retorna a altura do cabe�alho da coluna
  *
  * <p>Retorna a altura do cabe�alho da coluna. Fun��o de callback para o
  * par�metro height.</p>
  * @return integer Altura do cabe�alho da coluna
  *****************************************************************************/
  self.getHeight = function(){
    return self.height;
  }

 /*****************************************************************************
  * Seta a altura do cabe�alho da coluna
  *
  * <p>Seta a altura do cabe�alho da coluna. Fun��o de callback para o
  * par�metro height.</p>
  * @param integer height Nova altura do cabe�alho da coluna
  *****************************************************************************/
  self.setHeight = function(height){
    self.height = height;
  }

 /*****************************************************************************
  * Retorna o nome da coluna
  *
  * <p>Retorna o nome da coluna.</p>
  * @return string Nome da coluna
  *****************************************************************************/
  self.getName = function(){
    if(!self.name) return '';
    if(!self.name.value) return '';
    return self.name.value;
  }

 /*****************************************************************************
  * Seta o nome da coluna
  *
  * <p>Seta o nome da coluna.</p>
  * @param string nome Novo nome da coluna
  *****************************************************************************/
  self.setName = function(name){
    if(!self.name){
      self.name = {};
      self.name.stringAttributes = {};
      self.name.staticAttributes = {};
      self.name.staticChildren = [];
    }
    self.name.value = name;
  }
  
  // Define os atributos da classe column
  self.addAttribute('height','pixel',true,self.setHeight,self.getHeight);
  self.addAttribute('width','pixel',true,self.setWidth,self.getWidth);
  delete self.attributes['top'];
  delete self.attributes['left'];
  delete self.attributes['name'];
  delete self.attributes['align'];
  delete self.attributes['class'];
  
  /*****************************************************************************
   * Retorna o c�digo XML da box da coluna
   *
   * <p>Retorna o c�digo XML da box da coluna.</p>
   * 
   * @param String ident Identa��o do elemento no c�digo XML.
   * @return String Trecho de c�digo XML contendo o elemento box do elemento.
   *****************************************************************************/
  self.getBoxCode = function(ident){
    if(self.height==''){
      if(self.width){
        return ident+'<box width="'+self.width+'"/>\n';
      }else{
        return '';
      }
    }else{
      return ident+'<box height="'+self.height+'" width="'+self.width+'"/>\n';
    }
  }
  
 /*****************************************************************************
  * Retorna o c�digo XML do conte�do da coluna
  *
  * <p>Retorna o c�digo XML do conte�do da coluna.</p>
  * 
  * @param String ident Identa��o do conte�do do elemento no c�digo XML.
  * @return String Trecho de c�digo XML contendo todo o conte�do do elemento.
  *****************************************************************************/
  self.getXMLContent = function(ident){
    if(!ident) ident = '';
    var xmlCode = self.getBoxCode(ident);
    xmlCode+= self.getXMLGhostChildren(ident);
    var colName;
    var stringCode;
    if(self.name){
      if(self.name.stringAttributes!=undefined){
        stringCode = '<string';
        if(self.name.stringAttributes.getNamedItem){
          var id = self.name.stringAttributes.getNamedItem('id');
          if(!id) stringCode+= ' id="'+self.getName()+'String"';
          stringCode+= self.attributesToString(self.name.stringAttributes);
        }else{
          stringCode+= ' id="'+self.getName().replace(/[^a-zA-Z0-9_]/gi,'_')+'String"';
        }
        stringCode+= '>'+self.getName()+'</string>';
        xmlCode+= ident+'<static'+self.attributesToString(self.name.staticAttributes)+'>\n'
                 +ident+'  '+stringCode+'\n'
                 +ident+'</static>\n';
      }else{
        xmlCode+= ident+'<static'+self.attributesToString(self.name.staticAttributes)+'/>\n';
      }
    }
    return xmlCode;
  }
  
 /*****************************************************************************
  * Carrega os atributos do elemento contidos em sua box a partir do XML
  *
  * <p>Carrega os atributos do elemento contidos em sua box a partir de seu XML
  * (objeto XMLDocument, n�o texto XML)</p>
  * 
  * @param XMLDocument xmlDom Objeto contendo a representa��o da box do
  *        elemento em XML.
  *****************************************************************************/
  self.loadBoxFromXML = function(xmlBox){
    var value;
    for(var j=0;j<xmlBox.attributes.length;j++){
      value = parseInt(xmlBox.attributes[j].nodeValue);
      if(value<0) value = 0;
      self.setAttribute(xmlBox.attributes[j].nodeName,value);
    }
  }
  
 /*****************************************************************************
  * Preenche o elemento a partir do XML
  *
  * <p>Preenche os dados do elemento a partir de sua representa��o em XML
  * (objeto XMLDocument, n�o texto XML)</p>
  * 
  * @param XMLDocument xmlElement Objeto contendo a representa��o do elemento
  *        em XML.
  *****************************************************************************/
  self.loadFromXML = function(xmlElement){
    if(xmlElement.tagName && xmlElement.tagName.toLowerCase()==self.elementType.toLowerCase()){
      var xmlChildren = xmlElement.childNodes;
      var childName,childType,child;
      var nameless = true;
      this.loadAttributesFromXML(xmlElement.attributes);
      self.name = null;
      for(var i=0;i<xmlChildren.length;i++){
        if(xmlChildren[i].tagName){
          if(xmlChildren[i].tagName=='box'){
            self.loadBoxFromXML(xmlChildren[i]);
          }else if(xmlChildren[i].tagName=='static' && nameless){
            nameless = false;
            self.name = new Object();
            self.name.staticAttributes = xmlChildren[i].attributes;
            self.name.staticChildren = new Array();
            var xmlStaticChildren = xmlChildren[i].childNodes;
            for(var j=0;j<xmlStaticChildren.length;j++){
              if(xmlStaticChildren[j].tagName){
                if(xmlStaticChildren[j].tagName=='string'){
                  if(xmlStaticChildren[j].firstChild){
                    self.name.value = xmlStaticChildren[j].firstChild.nodeValue;
                  }else{
                    self.name.value = '';
                  }
                  self.name.stringAttributes = xmlStaticChildren[j].attributes;
                }else{
                  self.name.staticChildren.push(xmlStaticChildren[j]);
                }
              }
            }
          }else{
            self.addGhostChild(xmlChildren[i]);
          }
        }
      }
    }else return false;
  }

  return self;
}

