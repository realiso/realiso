/**
 * FWD - FAST WEB DEVELOPMENT
 * 
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/*******************************************************************************
 * Classe Dialog. Especializa��o de Viewgroup que define o elemento Dialog.
 * 
 * <p>Especializa��o de Viewgroup que define o elemento Dialog, o container que
 * cont�m todos os demais elementos.</p>
 *
 * @param integer x0 Coordenada x real que corresponde a coordenada 0 da dialog
 * @param integer y0 Coordenada y real que corresponde a coordenada 0 da dialog
 * @package FID
 *******************************************************************************/
function Dialog(x0,y0){
  var self = new Viewgroup('dialog');
  self.elementType = 'Dialog';
  delete self.attributes['align'];
  self.setAttribute('name','dialog');
  
 /**
  * Coordenada x real que corresponde a coordenada 0 da dialog
  * @var integer
  */
  self.x0 = x0;
  
 /**
  * Coordenada y real que corresponde a coordenada 0 da dialog
  * @var integer
  */
  self.y0 = y0;
  
  self.setPosition(x0,y0);
  
 /*****************************************************************************
  * Carrega os atributos da dialog contidos em sua box a partir do XML
  *
  * <p>Carrega os atributos da dialog contidos em sua box a partir de seu XML,
  * levando em considera��o o x0 e o y0.</p>
  * 
  * @param XMLDocument xmlDom Objeto contendo a representa��o da box do
  *        elemento em XML.
  *****************************************************************************/
  self.loadBoxFromXML = function(xmlBox){
    var boxAttributes = new Array();
    for(var j=0;j<xmlBox.attributes.length;j++){
      boxAttributes[xmlBox.attributes[j].nodeName] = parseInt(xmlBox.attributes[j].nodeValue);
    }
    self.setPosition(boxAttributes['left']+self.x0,boxAttributes['top']+self.y0);
    self.setSize(boxAttributes['height'],boxAttributes['width']);
  }
  
 /*****************************************************************************
  * Retorna o c�digo XML da box do elemento
  *
  * <p>Retorna o c�digo XML da box do elemento, levando em considera��o o x0 e
  * o y0.</p>
  * 
  * @param String ident Identa��o do elemento no c�digo XML.
  * @return String Trecho de c�digo XML contendo o elemento box do elemento.
  *****************************************************************************/
  self.getBoxCode = function(ident){
    var xmlCode = '';
    var top = parseInt(self.getAttribute('top')) - self.y0;
    var left = parseInt(self.getAttribute('left')) - self.x0;
    var height = parseInt(self.getAttribute('height'));
    var width = parseInt(self.getAttribute('width'));
    if(self.parent!=null){
      top-= parseInt(self.parent.getAttribute('top'));
      left-= parseInt(self.parent.getAttribute('left'));
    }
    xmlCode+= ident+'<box top="'+top+'" left="'+left+'" height="'+height+'" width="'+width+'"/>\n';
    return xmlCode;
  }
  
  return self;
}
