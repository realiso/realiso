/**
 * FWD - FAST WEB DEVELOPMENT
 * 
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/*******************************************************************************
 * Classe ListEditor. Extende SpecialElement para definir o List Editor
 * 
 * <p>Extende SpecialElement para definir o List Editor, elemento de interface
 * que permite definir os valores de atributos do tipo lista</p>
 *
 * @package FID
 * @param Element element Elemento � que pertence o atributo a ser editado
 * @param String attributeName Nome do atributo a ser editado
 *******************************************************************************/
function ListEditor(element,attributeName){
  var self = new SpecialElement('listEditor');
  self.attributeName = attributeName;
  self.elementType = 'ListEditor';
  self.defaultClassName = '';
  self.createHtmlElement('TABLE');
  self.setPosition(300,100);
  self.setSize(250,300);
  
 /**
  * Elemento � que pertence o atributo a ser editado
  * @var Element
  */
  self.element = element;
  
  // Constroi a tabela
  // 1a linha: cabecalho
  var htmlElement = gebi(self.id);
  var tbody = document.createElement('TBODY');
  var line = document.createElement('TR');
  var cel = document.createElement('TD');
  htmlElement.className = 'editor';
  cel.innerHTML = 'List Editor';
  cel.className = 'header';
  line.appendChild(cel);
  tbody.appendChild(line);
  // 2a linha: a linha com a textarea
  line = document.createElement('TR');
  cel = document.createElement('TD');
  cel.style.height = '100%';
  cel.style.width = '100%';
  cel.innerHTML = '<textarea id="listEditor_code" wrap="off" style="height:100%;width:100%;"></textarea>';
  line.appendChild(cel);
  tbody.appendChild(line);
  // ultima linha: a linha com o botao
  line = document.createElement('TR');
  cel = document.createElement('TD');
  var button = document.createElement('INPUT');
  button.type = 'button';
  button.value = '  OK  ';
  button.className = 'regularButton';
  button.onclick = function(){ self.close(); }
  cel.appendChild(button);
  line.appendChild(cel);
  tbody.appendChild(line);

  htmlElement.appendChild(tbody);
  
  gebi('listEditor_code').value = self.element.getAttribute(self.attributeName);
  
  // Desabilita os event handlers de teclado e mouse do document enquanro o ListEditor estiver aberto
  self.onkeydown = document.onkeydown;
  self.onmousedown = document.onmousedown;
  self.onmouseup = document.onmouseup;
  self.onmousemove = document.onmousemove;
  document.onkeydown = function(){};
  document.onmousedown = function(){};
  document.onmouseup = function(){};
  document.onmousemove = function(){};

 /*****************************************************************************
  * Fecha o ListEditor e atualiza o atributo
  *
  * <p>Fecha o ListEditor e atualiza o atributo.</p>
  *****************************************************************************/
  self.close = function(){
    self.element.setAttribute(self.attributeName,gebi('listEditor_code').value);
    // Habilita os event handlers de teclado e mouse do document novamente
    document.onkeydown = self.onkeydown;
    document.onmousedown = self.onmousedown;
    document.onmouseup = self.onmouseup;
    document.onmousemove = self.onmousemove;
    self.destroy();
  }
  
  return self;
}

