/**
 * FWD - FAST WEB DEVELOPMENT
 * 
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/*******************************************************************************
 * Classe Reporticon. Especialização de Element que define o elemento ReportIcon.
 * 
 * <p>Especialização de Element que define o elemento ReportIcon.</p>
 *
 * @package FID
 * @param String name Nome do elemento
 *******************************************************************************/
function Reporticon(name){
  var self = new Icon(name);
  self.elementType = 'Reporticon';
  self.defaultClassName = 'unselected';
  var htmlElement = self.createHtmlElement('DIV');
  htmlElement.style.backgroundRepeat = "no-repeat";
  htmlElement.style.backgroundPosition = "left top";
  htmlElement.style.backgroundImage = 'url(imgs/icon.png)';
  self.addAttribute('src','text',false,'iconSrc');
  self.addAttribute('marginleft','integer',false,'ciMarginLeft');
  self.addAttribute('marginright','integer',false,'ciMarginRight');
  self.setAttribute('src','');
  self.setAttribute('marginleft',2);
  self.setAttribute('marginright',2);

  delete self.attributes['class'];
  delete self.attributes['align'];
  self.addAttribute('top','hidden',true,'style.top');
  self.addAttribute('height','hidden',true,'style.height');
  self.addAttribute('width','pixel',false,
    function(psValue){gebi(self.id).style.width = psValue;},
    function(){return parseInt(gebi(self.id).style.width);}
  );

  self.setPosition = function(piLeft,piTop){
    if(self.parent){
      var miParentLeft = parseInt(self.parent.getAttribute('left'));
      var miParentRight = miParentLeft + parseInt(self.parent.getAttribute('width'));
      var miNewLeft = piLeft;
      var miWidth = parseInt(self.getAttribute('width'));
      
      if(miNewLeft < miParentLeft){
        miNewLeft = miParentLeft;
      }else if(miNewLeft+miWidth > miParentRight){
        miNewLeft = miParentRight - miWidth;
      }
      self.setAttribute('left',miNewLeft);
      self.setAttribute('top',parseInt(self.parent.getAttribute('top')));
    }else{
      self.setAttribute('left',piLeft);
      self.setAttribute('top',piTop);
    }
  }
  
  self.setSize = function(height,width){
    if(self.parent){
      self.setAttribute('height',parseInt(self.parent.getAttribute('height')));
    }else{
      self.setAttribute('height',height);
    }
    self.setAttribute('width',width);
  }
  
  self.getBoxCode = function(ident){
    return '';
  }

  self.loadFromXML = function(xmlElement,parentBox){
    self.loadedFromXML = true;
    if(xmlElement.tagName && xmlElement.tagName.toLowerCase()==self.elementType.toLowerCase()){
      var xmlChildren = xmlElement.childNodes;
      self.setAttribute('marginleft',0);
      self.setAttribute('marginright',0);
      self.loadAttributesFromXML(xmlElement.attributes);
      self.setAttribute('top',parentBox['top']);
      self.setAttribute('height',parentBox['height']);
      self.setAttribute('left',parentBox['left']);
      return true;
    }else return false;
  }

  return self;
}
