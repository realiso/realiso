/**
 * FWD - FAST WEB DEVELOPMENT
 * 
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/*******************************************************************************
 * Classe View. Especializa��o de Element que tem um atributo value.
 * 
 * <p>Classe abstrata que extende Element e tem um atributo value.</p>
 * @package FID
 * @param String name Nome do elemento
 *******************************************************************************/
function View(name){
  var self = new Element(name);
  self.elementType = 'View';
  self.addAttribute('value','text',true,'value');
  
 /**
  * Guarda os atributos da string do elemento
  * @var array
  */
  self.stringAttributes = new Array();
  
 /*****************************************************************************
  * Retorna o c�digo XML do view
  *
  * <p>Retorna o c�digo XML do view</p>
  * 
  * @param String ident Identa��o do view no c�digo XML.
  * @param TabIndex tabIndex Objeto TabIndex que cont�m o tabindex do view.
  * @return String C�digo XML do view.
  *****************************************************************************/
  self.getXMLContent = function(ident,tabIndex){
    var msStringId,msValue,xmlCode = this.getBoxCode(ident);
    if(self.getAttribute('value')){
      if(self.stringAttributes.length>0){
        xmlCode+= ident+'<string'+self.attributesToString(self.stringAttributes);
      }else{
        msStringId = self.elementType.toLowerCase()+'_'+self.getAttribute('name').toLowerCase();
        xmlCode+= ident+'<string id="'+msStringId+'"';
      }
      msValue = self.getAttribute('value');
      msValue = msValue.replace('&','&amp;','g');
      msValue = msValue.replace('\t','  ','g');
      msValue = msValue.replace('<br>','<br/>','g');
      xmlCode+= '>'+msValue+'</string>\n';
    }
    xmlCode+= self.getXMLGhostChildren(ident);
    return xmlCode;
  }
  
  /*****************************************************************************
   * Preenche o view a partir do XML
   *
   * <p>Preenche os dados do view a partir de sua representa��o em XML
   * (objeto XMLDocument, n�o texto XML)</p>
   * 
   * @param XMLDocument xmlElement Objeto contendo a representa��o do view em
   *        XML.
   *****************************************************************************/
   self.loadFromXML = function(xmlElement,parentBox){
    self.ghostAttributes = new Array();
    self.ghostChildren = new Array();
    if(xmlElement.tagName && xmlElement.tagName.toLowerCase()==self.elementType.toLowerCase()){
      var xmlChildren = xmlElement.childNodes;
      var column;
      var colCounter = 0;
      self.loadAttributesFromXML(xmlElement.attributes);
      var foundBox = false;
      self.setAttribute('value','');
      for(var i=0;i<xmlChildren.length;i++){
        if(xmlChildren[i].tagName){
          if(xmlChildren[i].tagName=='box'){
            self.loadBoxFromXML(xmlChildren[i]);
            foundBox = true;
          }else if(xmlChildren[i].tagName=='string'){
            self.stringAttributes = xmlChildren[i].attributes;
            var value = '';
            for(var j=0;j<xmlChildren[i].childNodes.length;j++){
              var child = xmlChildren[i].childNodes[j];
              if(child.tagName=='b'){
                value+= '<b>'+child.firstChild.nodeValue+'</b>';
              }else if(child.tagName=='br'){
                value+= '<br>';
              }else{
                value+= child.nodeValue;
              }
            }
            self.setAttribute('value',value);
          }else{
            self.addGhostChild(xmlChildren[i]);
          }
        }
      }
      if(!foundBox){
        for(var j in self.boxDefaultAttributes){
          self.setAttribute(j,self.boxDefaultAttributes[j]);
        }
      }
      self.move(parentBox['left'],parentBox['top']);
      self.fitIn(parentBox['left'],parentBox['top'],parentBox['right'],parentBox['bottom']);
      return true;
    }else return false;
  }

  return self;
}

