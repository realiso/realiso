<?php
$gaStrings = array(

/* './abstracts/FWDGridAbstract.php' */

'fwdgrid_pref_column_label'=>'Columna:',
'fwdgrid_pref_column_name'=>'Columna',
'fwdgrid_pref_column_order'=>'Orden:',
'fwdgrid_pref_column_order_title'=>'Orden de Columnas de Grilla:',
'fwdgrid_pref_column_properties'=>'Propiedades de la columna',
'fwdgrid_pref_column_show_title'=>'Columnas Mostradas y Orden de Visualización entre ellas',
'fwdgrid_pref_column_width'=>'Ancho*:',
'fwdgrid_pref_columns_grid'=>'Columnas de Grilla:',
'fwdgrid_pref_columns_show'=>'Columnas Mostradas:',
'fwdgrid_pref_false_value'=>'No',
'fwdgrid_pref_grid_columns_title'=>'Columnas de Grilla:',
'fwdgrid_pref_grid_properties'=>'Edición de las propiedades de la Grilla:',
'fwdgrid_pref_rows_height'=>'Altura de Filas:',
'fwdgrid_pref_rows_per_page'=>'Filas por página:',
'fwdgrid_pref_select_source'=>'Seleccionar Origen',
'fwdgrid_pref_select_target'=>'Seleccionar Destino',
'fwdgrid_pref_target'=>'Destino:',
'fwdgrid_pref_tooltip_label'=>'Mensaje Textual:',
'fwdgrid_pref_true_value'=>'Sí',
'fwdgrid_pref_width_obs'=>'* \'0\' = ancho automático',

/* './base/FWDDefaultErrorHandler.php' */

'error_config_corrupt'=>'El archivo de configuración está dañado.',
'error_configuration'=>'El archivo de configuración está dañado.',
'error_db_connection'=>'Error de conexión con la Base de Datos.',
'error_db_invalid_object_name'=>'Nombre de objeto de base de datos no válido. Probablemente usando un usuario de DB no válido.',
'error_debug_permission_denied'=>'Permiso denegado en archivo de depuración.',
'error_session_permission_denied'=>'Permiso denegado en archivo de sesión.',
'error_smtp'=>'Falló al conectarse al servidor de e-mail.',

/* './base/FWDLanguage.php' */

'default_language'=>'Portugués',

/* './base/FWDLicense.php' */

'error_corrupt_hash'=>'El resumen (hash) está dañado.',
'error_corrupt_license'=>'La licencia está dañada.',
'error_permission_denied'=>'Permiso denegado en archivo de activación.',

/* './base/FWDSyslog.php' */

'error_connection_syslog'=>'No se puede conectar a servidor de registro (syslog).',

/* './event/classEvent/FWDGridUserEdit.php' */

'fwdGridUserEditErrorWidthGrid'=>'Fueron encontradas inconsistencias: La suma de las columnas de la grilla debe ser menor o igual que:',
'fwdGridUserEditErrorWidthGridExplain'=>'de todos modos el valor obtenido es:',

/* './formats/FWDPDF.php' */

'fpdf_page'=>'Página',

/* './report/FWDReportClassificator.php' */

'report_confidencial'=>'Informe Confidencial',
'report_institutional'=>'Informe Institucional',
'report_public'=>'Informe Público',

/* './report/FWDReportGenerator.php' */

'report_confidential_report'=>'Informe Confidencial',
'report_excel'=>'Excel',
'report_html'=>'HTML',
'report_html_download'=>'Descarga HTML',
'report_institutional_report'=>'Informe Institucional',
'report_pdf'=>'PDF',
'report_public_report'=>'Informe Público',
'report_word'=>'Word',

/* './view/FWDCalendar.php' */

'locale'=>'en_us',

/* './view/FWDGrid.php' */

'fwdgrid_info_no_result_rows'=>'No se encontraron resultados',
'fwdgrid_number_page_of'=>'de',
'fwdgrid_page_label'=>'página',

/* './view/FWDTreeAjax.php' */

'FWDLoading'=>'Cargando',
);
?>