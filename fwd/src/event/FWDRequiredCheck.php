<?
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

define( "REQUIRED_CLIENT_EVENT", 1);
define( "REQUIRED_AJAX_EVENT", 2);

/**
 * Classe FWDRequiredCheck. Implementa a verifica��o da obrigatoriedade no preenchimento.
 *
 * <p>Classe que implementa a verifica��o de preenchimento de campos que s�o
 * considerados obrigat�rios.</p>
 * @package FWD5
 * @subpackage event
 */
class FWDRequiredCheck extends FWDEvent
{

	/**
	 * Nome do Evento. Obs: o nome dado pelo usu�rio ser� concatenado com "RequiredCheck"
	 * @var string
	 * @access protected
	 */
	protected $csName = "RequiredCheck";

	/**
	 * Elementos alvo do teste de "RequiredCheck", devem ser separadas por ":"
	 * @var string
	 * @access protected
	 */
	protected $csElements;

	/**
	 * Tipo do evento que deve executar o "RequiredCheck". Par�metros que devem ser passados s�o ("clientEvent" ou "ajaxEvent" ou "serverEvent")
	 * @var integer
	 * @access protected
	 */
	protected $ciEventType = REQUIRED_CLIENT_EVENT;

	/**
	 * Eventos que devem ser disparados quando a verifica��o "RequiredCheck" for Positiva
	 * @var string
	 * @access protected
	 */
	protected $csEventsOk ="";

	/**
	 * Eventos que devem ser disparados quando a verifica��o "RequiredCheck" for Negativa
	 * @var string
	 * @access protected
	 */
	protected $csEventsError="";

	/**
	 * Array de Eventos definidos dentro da tag "requiredcheck" no xml. OBS: (esses eventos devem ter Event = 'eventok' OU 'eventerror')
	 * @var array
	 * @access protected
	 */
	protected $caEvent = array();

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDRequiredCheck.</p>
	 * @access public
	 * @param string psElements Elementos que devem ser testados se est�o ou n�o preenchidos
	 */
	public function __construct($psElements = "") {
		$this->csElements = $psElements;
	}

	/**
	 * Seta o nome deste da inst�ncia da classe
	 *
	 * <p>M�todo utilizado para setar o nome da inst�ncia desta classe</p>
	 * @access public
	 * @param string $psName  Nome da inst�ncia da classe
	 */
	public function setAttrName($psName) {
		$this->csName = $psName;
	}

	/**
	 * Seta o tipo de evento que deve ser usado para o teste de preenchimento
	 *
	 * <p>M�todo para setar o tipo de evento que deve ser usado para fazer o teste
	 * de preenchimento. par�metros possiveis (clientEvent,ajaxEvent,serverEvent)</p>
	 * @access public
	 * @param string $psEventType  Tipo de evento que deve ser utilizado (clientEvent,ajaxEvent)
	 */
	public function setAttrEventType($psEventType) {
		$this->ciEventType = FWDWebLib::attributeParser($psEventType,array("clientEvent"=>REQUIRED_CLIENT_EVENT, "ajaxEvent"=>REQUIRED_AJAX_EVENT),"EventType");
	}

	/**
	 * Seta o nome dos elementos que se deve verificar se est�o preenchidos.
	 *
	 * <p>M�todo para setar o nome dos elementos que deve ser verificado se est�o ou
	 * n�o preenchidos , esses nomes devem estar separados por ":".</p>
	 * @access public
	 * @param string $psElements Elementos que devem ser testados se est�o ou n�o preenchidos
	 */
	public function setAttrElements($psElements) {
		$this->csElements = $psElements;
	}

	/**
	 * Seta a TRIGGER dos eventos que devem ser disparados em caso de verifica��o Positiva
	 *
	 * <p>seta a TRIGGER dos eventos que devem ser disparados em caso de verifica��o Positiva.
	 * Essas TRIGGERS devem estar separadas por ";".</p>
	 * @access public
	 * @param string $psEventsOk TRIGGERS de eventos que devem ser disparados em caso de verifica��o positiva
	 */
	public function setAttrEventsOk($psEventsOk) {
		$this->csEventsOk = $psEventsOk;
	}

	/**
	 * Seta a TRIGGER dos eventos que devem ser disparados em caso de verifica��o Negativa
	 *
	 * <p>seta a TRIGGER dos eventos que devem ser disparados em caso de verifica��o Negativa.
	 * Essas TRIGGERS devem estar separadas por ";".</p>
	 * @access public
	 * @param string $psEventError TRIGGERS de eventos que devem ser disparados em caso de verifica��o negativa
	 */
	public function setAttrEventsError($psEventError) {
		$this->csEventsError = $psEventError;
	}

	/**
	 * Seta os objetos de eventos definidos entre as tags da classe no XML
	 *
	 * <p>seta os objetos de eventos definidos entre as tags da classe no XML.
	 * OBS: o Event desses evento deve ser ("eventok" ou "eventerror") para qualquer outro Event, o evento n�o ser�
	 * renderizado nesta classe</p>
	 * @access public
	 * @param ObjFWDEvent $poAjaxEvent Eventos que devem ser tratados pela classe , devem ter Event = ("eventok" ou "eventerror")
	 */
	public function addObjFWDEvent($poAjaxEvent) {
		$this->addEvent($poAjaxEvent);
	}

	/**
	 * Seta um evento para o elemento.
	 *
	 * <p>M�todo para setar um evento para o elemento.</p>
	 * @access public
	 * @param FWDEvent $poEvent Evento
	 */
	public function addEvent(FWDEvent $poEvent) {
		$mskey = strtolower($poEvent->getAttrEvent());
		$msArrayKey = explode(":",$mskey);
		foreach($msArrayKey as $key)
		{
			if(!isset($this->caEvent[$key]))
			$this->caEvent[$key] = new FWDEventHandler($key,$this->getAttrName());
			$this->caEvent[$key]->setAttrContent($poEvent);
		}
	}

	/**
	 * Retorna os identificadores dos elementos que tem preenchimento obrigat�rio.
	 *
	 * <p>M�todo para retornar os identificadores dos elementos que devem ter
	 * preenchimento obrigat�rio</p>
	 * @access public
	 * @return string indentificadores dos elementos que devem tem preenchimento obrigat�rio.
	 */
	public function getAttrElements() {
		return $this->csElements;
	}

	/**
	 * Retorna o tipo de evento que deve ser utilizado para verificar se os campos est�o preenchidos.
	 *
	 * <p>M�todo para retornar o tipo de evento que deve ser utilizado para verificar se os campos
	 * est�o preenchidos</p>
	 * @access public
	 * @return integer Tipo de evento que deve ser utilizado para verificar se os campos est�o preenchidos.
	 */
	public function getAttrEventType() {
		return $this->ciEventType;
	}

	/**
	 * Renderiza o Evento RequiredCheck.
	 *
	 * <p>Renderiza o Evento RequiredCheck e dispara os eventos definidos para este evento caso a verifica��o
	 * seja possitiva ou negativa
	 * @access public
	 * @return string String que ir� disparar o Evento
	 */
	public function render()
	{
		if($this->ciEventType==REQUIRED_CLIENT_EVENT)
		{
			$msEventsOk = "";
			$msEventsError = "";

			foreach($this->caEvent as $moEventHandler)
			{

				$maEvents = $moEventHandler->getAttrContent();
					
				if($moEventHandler->getAttrEvent()=="eventok")
				{
					foreach($maEvents as $moEvent)
					{
						$msEventsOk .= $moEvent->render($moEventHandler->getAttrElem());
					}
				}
				elseif($moEventHandler->getAttrEvent()=="eventerror")
				{
					foreach($maEvents as $moEvent)
					{
						$msEventsError .= $moEvent->render($moEventHandler->getAttrElem());
					}
				}
				else
				{
					//evento n�o tem Event no padr�o adequando para esta classe , assim eh despresado
				}
			}

			$msEventsOk = str_replace(array('"',"\n"),array('\"',' '),$msEventsOk.$this->csEventsOk);
			$msEventsError = str_replace(array('"',"\n"),array('\"',' '),$msEventsError.$this->csEventsError);
			return "js_required_check_client(\"{$this->csElements}\",\"{$msEventsOk}\",\"{$msEventsError}\");";
				
		}

		elseif($this->ciEventType==REQUIRED_AJAX_EVENT)
		{
			return "trigger_event(\"{$this->getAttrName()}ajax\",\"3\");";
		}
		else
		{
				
		}
	}

	/**
	 * M�todo executado quando o XML do elemento termina de carregar.
	 *
	 * <p>M�todo executado quando o XML do elemento termina de carregar. Utilizado para criar os
	 * eventos a serem utilizados para verificar o preenchimento as vari�veis.</p>
	 *
	 * @access public
	 */
	public function execute()
	{
		$msEventsOk="";
		$msEventsError="";

		$StartEvent_instance = FWDStartEvent::getInstance();

		if($this->ciEventType==REQUIRED_AJAX_EVENT)
		{
			$moAjaxEvent = new FWDRequiredCheckAjax("{$this->getAttrName()}ajax","3");
			$moAjaxEvent->setAttrElements("{$this->getAttrElements()}");

			foreach($this->caEvent as $moEventHandler)
			{
				$maEvents = $moEventHandler->getAttrContent();
					
				if($moEventHandler->getAttrEvent()=="eventok")
				{
					foreach($maEvents as $moEvent)
					{
						$msEventsOk .= $moEvent->render($moEventHandler->getAttrElem());
					}

				}
				elseif($moEventHandler->getAttrEvent()=="eventerror")
				{
					foreach($maEvents as $moEvent)
					{
						$msEventsError .= $moEvent->render($moEventHandler->getAttrElem());
					}
				}
				else
				{
					//evento n�o tem Event no padr�o adequando para esta classe , assim eh despresado
				}
			}
			$moAjaxEvent->setAttrEventsOk("{$msEventsOk}{$this->csEventsOk}");
			$moAjaxEvent->setAttrEventsError("{$msEventsError}{$this->csEventsError}");
			$StartEvent_instance->addAjaxEvent($moAjaxEvent);
		}
	}
}
?>