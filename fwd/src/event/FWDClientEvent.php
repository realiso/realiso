<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDClientEvent. Implementa Eventos Cliente.
 *
 * <p>Classe que implementa Eventos Cliente na Framework FWD5.</p>
 * @package FWD5
 * @subpackage event
 */
class FWDClientEvent extends FWDEvent {

	/**
	 * Evento Cliente
	 * @var string
	 * @access protected
	 */
	protected $csValue = "";

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDClientEvent. Seta os atributos de
	 * alvo, fun��o, elementos e par�metros do evento.</p>
	 * @access public
	 * @param string $psEvent Alvo do evento
	 * @param string $psFunction Fun��o do evento
	 * @param string $psElements Elementos sobre os quais o evento atuar�
	 * @param string $psParameters Par�metros necess�rios a alguns eventos
	 */
	public function __construct($psEvent = "" ,$psFunction = "",$psElements = "",$psParameters = ""){
		parent::__construct($psEvent,$psFunction,$psElements,$psParameters);
	}

	/**
	 * Renderiza o Evento Cliente.
	 *
	 * <p>Renderiza o Evento Cliente. Instancia um objeto de evento javascript
	 * com os valores dos atributos de fun��o, elementos e par�metros e renderiza
	 * tal evento.</p>
	 * @access public
	 * @return string String que ir� disparar o Evento Cliente
	 */
	public function render() {
		if (!$this->csValue){
			$msFunction = $this->getAttrFunction();
			$msElems = $this->getAttrElements();
			$msAttr = $this->getAttrParameters();
			$moJs = new FWDJsEvent($msFunction,$msElems,$msAttr);
			return "try{{$moJs->render()}}catch(e){debugException(e)}";
		}
		else
		return "try{{$this->getAttrValue()}}catch(e){debugException(e)}";
	}

	/**
	 * Este m�todo n�o deve fazer nada.
	 *
	 * <p>Esta classe n�o deve utilizar este m�todo.</p>
	 * @access public
	 */
	public function run() {
	}

	/**
	 * Atribui valor ao Evento Cliente.
	 *
	 * <p>Atribui valor ao Evento Cliente retirando espa�os em branco no
	 * inicio e/ou no fim da string.</p>
	 * @access public
	 * @param string $psValue Valor a ser atribu�do
	 */
	public function setAttrValue($psValue) {
		$this->csValue = trim($psValue);
	}

	/**
	 * Atribui valor, condicionalmente, ao Evento Cliente.
	 *
	 * <p>Atribui valor ao Evento Cliente, condicionando atrav�s do segundo par�metro.
	 * Se pbForce for FALSE, concatena o valor se a vari�vel tiver conte�do;
	 * se TRUE atribui mesmo que tenha conte�do (sobrescreve).</p>
	 * @access public
	 * @param string p$sValue Valor a ser atribu�do
	 * @param boolean $pbForce For�ar o valor mesmo que j� tenha conte�do
	 */
	public function setValue($psValue, $pbForce) {
		if (!isset($this->csValue) || !$this->csValue || $pbForce ){
			$this->setAttrValue($psValue);
		}
		else
		$this->csValue .= $psValue;
	}

	/**
	 * Retorna o valor do Evento Cliente.
	 *
	 * <p>Retorna o valor do Evento Cliente.</p>
	 * @access public
	 * @return string Evento Cliente
	 */
	public function getAttrValue() {
		return str_replace("'","\"",$this->csValue);
	}
}
?>