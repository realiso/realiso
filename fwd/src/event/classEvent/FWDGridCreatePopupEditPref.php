<?
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDGridCreatePopupEditPref Implementa Eventos Servidor para a FWDGrid.
 *
 * <p>Classe que implementa Eventos Servidor para criar a popup de edi��o de preferencias da FWDGrid
 * na Framework FWD5.</p>
 * @package FWD5
 * @subpackage eventClass
 */
class FWDGridCreatePopupEditPref extends FWDRunnable
{
	function run()
	{
		$moGrid = FWDWebLib::getObject($this->getAttrEventObj());
		if($moGrid)
		{
			$msReturn = $moGrid->createHTMLPopup();
				
			$msGridCodeScape = str_replace('"','\"',$msReturn);
			$moStatic = new FWDStaticGrid(new FWDBox(0,1,22,718));
			$moStatic->setValue('Editar Prefer�ncias do Usu�rio');
			$moStatic->setAttrClass($moGrid->getAttrCSSClass().$moGrid->getCssClassTitlePanel());
			$moStatic->setAttrHorizontal('center');
			$moStatic->setAttrVertical('middle');
			$msStaticCode = str_replace('"','\"',$moStatic->draw());
				
			if(FWDWebLib::browserIsIE())
			echo "js_open_popup('{$moGrid->getAttrName()}_preferences','about:blank',\"{$msStaticCode}\",'true',381,720);var moPopup = soPopUpManager.getPopUpById('{$moGrid->getAttrName()}_preferences');moPopup.setHtmlContent(\"{$msGridCodeScape}\");moPopup.setCloseEvent(function(){objManagerGrifPref.cleanVars();});window.setTimeout(\"objManagerGrifPref.init('{$moGrid->getAttrName()}')\",100);";
			else
			echo "js_open_popup('{$moGrid->getAttrName()}_preferences','about:blank',\"{$msStaticCode}\",'true',382,722);var moPopup = soPopUpManager.getPopUpById('{$moGrid->getAttrName()}_preferences');moPopup.setHtmlContent(\"{$msGridCodeScape} \");moPopup.setCloseEvent(function(){objManagerGrifPref.cleanVars();});window.setTimeout(\"objManagerGrifPref.init('{$moGrid->getAttrName()}')\",100);";
		}
		//else { }
		//n�o foi encontrado no Post o objeto da grid cujo � indicado por "$this->getAttrEventObj()" assim
		//n�o ha nada pra fazer
	}
}
?>