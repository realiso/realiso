<?
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDGridRefresh. Implementa Eventos Servidor para a FWDGrid.
 *
 * <p>Classe que implementa Eventos Servidor para refresh  da FWDGrid
 * na Framework FWD5.</p>
 * @package FWD5
 * @subpackage eventClass
 */
class FWDGridUserEdit extends FWDRunnable
{
	function run()
	{
		$moWebLib = FWDWebLib::getInstance();
		$moGrid = $moWebLib->getObject($this->getAttrEventObj());
		if($moGrid)
		{
			$msInfoFirst = FWDLanguage::getPHPStringValue('fwdGridUserEditErrorWidthGrid','Inconsist�ncias Encontradas: O somat�rio das larguras das colunas da grid deve ser menor ou igual a:');
			$msInfoSecond =FWDLanguage::getPHPStringValue('fwdGridUserEditErrorWidthGridExplain','por�m o valor obtido � de:') ;

			$moUserPref = $moWebLib->getObject($moGrid->getAttrPreferencesHandle());
			if($moUserPref){
				$msGridName = $moGrid->getAttrName();
				$msPrefCode = $moWebLib->getPOST("{$msGridName}_preferences_code_values");
				if($msPrefCode){
					$maPrefCode = FWDWebLib::unserializeString($msPrefCode);
					$maColumnProp = array();
					$maAuxColumnsProp = explode(';',$maPrefCode[2]);
					for($miI = 0;$miI<count($maAuxColumnsProp);$miI++){
						$maColumnPropAux = explode(':',$maAuxColumnsProp[$miI]);
						$maColumnProp[$maColumnPropAux[0]] = $maColumnPropAux[1];
					}
					$miContWidth = 0;
					$maColumnShow = (explode(':',$maPrefCode[1]));
					for($miI = 0;$miI<count($maColumnShow);$miI++)
					if(isset($maColumnProp[$maColumnShow[$miI]]))
					$miContWidth += $maColumnProp[$maColumnShow[$miI]];
					if($miContWidth > $moGrid->getObjFWDBox()->getAttrWidth()){
						echo "alert('{$msInfoFirst}      {$moGrid->getObjFWDBox()->getAttrWidth()}px {$msInfoSecond}      {$miContWidth}px');";
					}else{
						//ordenamento dos dados da grid
						$moUserPref->setOrderColumnsData($maPrefCode[0]);
						//ordem de exibi��o das colunas na grid
						$moUserPref->setOrderColumn($maPrefCode[1]);
						//propriedades das colunas {numCol:width:noWrap}*
						$moUserPref->setColumnsProp($maPrefCode[2]);
						//n�mero de linhas por p�gina
						$moUserPref->setRowsPerPage($maPrefCode[3]);
						//altura das linhas da grid
						$moUserPref->setRowsHeight($maPrefCode[4]);
						//tooltip da coluna
						$moUserPref->setToolTipCol($maPrefCode[5]);
						//apos setar todos os atributos no objeto de preferencias, salvar no banco
						$moUserPref->savePreferences($moGrid->getAttrName());
						echo "trigger_event('{$msGridName}_draw_user_edit',3);soPopUpManager.closePopUp('{$msGridName}_preferences');";
					}
				}
					
			}else
			trigger_error("Handle of GridUserPreferences don't exist in WebLib!",E_USER_WARNING);
		}
	}
}
?>