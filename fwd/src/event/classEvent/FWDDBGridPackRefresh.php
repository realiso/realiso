<?
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDDBGridPackRefresh. Implementa o refresh das grids contidas em um FWDDBGridPack.
 *
 * <p>Classe que implementa o refresh das grids contidas em um FWDDBGridPack.</p>
 * @package FWD5
 * @subpackage eventClass
 */
class FWDDBGridPackRefresh extends FWDRunnable {
	public function run(){
		$moPack = FWDWebLib::getObject($this->getAttrEventObj());
		foreach($moPack->getElements() as $msElementId){
			$moGrid = FWDWebLib::getObject($msElementId);
			if($moGrid->getAttrPopulate()){
				if(!$moGrid->getAttrRefreshOnCurrentPage()){
					$moGrid->setCurrentPage(1);
				}
				$moGrid->execEventPopulate('refresh');
			}else{
				echo "gobi('{$msElementId}').hideGlass();";
			}
		}
	}
}

?>