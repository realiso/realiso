<?
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDGridChagePageLast. Implementa Eventos Servidor para a FWDGrid.
 *
 * <p>Classe que implementa Eventos Servidor para lastPage  da FWDGrid
 * na Framework FWD5.</p>
 * @package FWD5
 * @subpackage eventClass
 */
class FWDGridChangePageLast extends FWDRunnable
{
	function run()
	{
		$moGrid = FWDWebLib::getObject($this->getAttrEventObj());
		if($moGrid)
		{
			$msInfoPages = FWDWebLib::getPOST($moGrid->getAttrName()."_gridpageinfo");
			$msPages="::";
			if($msInfoPages){
				$maInfoPage = explode(":",$msInfoPages);
				$maInfoPage[0]=$maInfoPage[1];
				$msPages = "{$maInfoPage[0]}:{$maInfoPage[1]}:{$maInfoPage[2]}";

				$moGrid->setCurrentPage($maInfoPage[0]);
				$moGrid->setLastPage($maInfoPage[1]);
				$moGrid->setAttrRowsCount($maInfoPage[2]);
			}
			$moGrid->gridPopulateChange('back',$msPages);
		}
		else { }
		//n�o foi encontrado no Post o objeto da grid cujo � indicado por "$this->getAttrEventObj()" assim
		//n�o ha nada pra fazer
	}
}
?>