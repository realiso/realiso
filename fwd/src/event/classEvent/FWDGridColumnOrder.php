<?
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDGridPopulate. Implementa Eventos Servidor para a FWDGrid.
 *
 * <p>Classe que implementa Eventos Servidor de preenchimento dos bot�es de pagina��o, corpo da grid,
 * menus de contexto da FWDGrid na Framework FWD5.</p>
 * @package FWD5
 * @subpackage eventClass
 */
class FWDGridColumnOrder extends FWDRunnable
{
	function run()
	{
		$maEventObj = explode(":",$this->getAttrEventObj());
		$moGrid = FWDWebLib::getObject($maEventObj[0]);
		if(($moGrid))
		{
			$miIndex = 1;
			if($moGrid->getAttrPopulate()){
				$msColumnOrder="";
				if(($moGrid instanceof FWDDBGrid)&&($moGrid->getDataSet())){
					$moDataSet = $moGrid->getDataSet();
					foreach($moGrid->getColumns() as $moColumn){
						if(($moColumn->getAttrAlias())&&($moColumn->getAttrAlias() == $maEventObj[1]))
						{
							if($moColumn->getOrderBy()=="+"){
								$moColumn->setOrderBy("desc");
								$msColumnOrder .= "-{$miIndex}:";
							}else{
								$moColumn->setOrderBy("asc");
								$msColumnOrder .= "+{$miIndex}:";
							}
							$moDataSet->setOrderBy($moColumn->getAttrAlias(),$moColumn->getOrderBy());
						}
						//else{}//coluna n�o est� ordenando a grid
						$miIndex++;
					}
					$moGrid->setDataSet($moDataSet);
					$moGrid->setAttrOrder($msColumnOrder);
				}
				else{ //ordena��o em grid com preenchimento manual
					foreach($moGrid->getColumns() as $moColumn){
						if($moColumn->getAttrName() == $maEventObj[1]){
							if($moColumn->getOrderBy()=="+"){
								$moColumn->setOrderBy("desc");
								$msColumnOrder .= "-{$miIndex}:";
							}else{
								$moColumn->setOrderBy("asc");
								$msColumnOrder .= "+{$miIndex}:";
							}
						}
						//else{}//coluna n�o est� ordenando a grid
						$miIndex++;
					}
					$moGrid->setAttrOrder($msColumnOrder);
				}
				$msInfoPages = FWDWebLib::getPOST($moGrid->getAttrName()."_gridpageinfo");
				$msPages="::";
				if($msInfoPages){
					$maInfoPage = explode(":",$msInfoPages);
					$msPages = "{$maInfoPage[0]}:{$maInfoPage[1]}:{$maInfoPage[2]}";

					$moGrid->setCurrentPage($maInfoPage[0]);
					$moGrid->setLastPage($maInfoPage[1]);
					$moGrid->setAttrRowsCount($maInfoPage[2]);
				}
				$moGrid->gridPopulateOrderColumn('populateordercolumn',$msPages);
			}else{
				echo " gobi('".$moGrid->getAttrName()."').coGlass.hide(); ";
			}
		}
		//else { }
		//n�o foi encontrado no Post o objeto da grid cujo � indicado por "$this->getAttrEventObj()" assim
		//n�o ha nada pra fazer
	}
}
?>