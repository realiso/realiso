<?
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDGridRefresh. Implementa Eventos Servidor para a FWDGrid.
 *
 * <p>Classe que implementa Eventos Servidor para refresh  da FWDGrid
 * na Framework FWD5.</p>
 * @package FWD5
 * @subpackage eventClass
 */
class FWDGridUserEditDefaultValues extends FWDRunnable
{
	function run()
	{
		$moWebLib = FWDWebLib::getInstance();
		$moGrid = $moWebLib->getObject($this->getAttrEventObj());
		if($moGrid)
		{
			$moUserPref = $moWebLib->getObject($moGrid->getAttrPreferencesHandle());
			if($moUserPref){
				$moUserPref->resetPreferenses($moGrid->getAttrName());
			}else
			trigger_error("Handle of GridUserPreferences don't exist in WebLib!",E_USER_WARNING);
		}
	}
}
?>