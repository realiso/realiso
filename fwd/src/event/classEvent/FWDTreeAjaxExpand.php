<?
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDTreeAjaxExpand. Implementa evento ajax para a FWDTreeAjax.
 *
 * <p>Implementa o evento ajax que expande uma FWDTreeAjax, buscando seus filhos
 * no XML no servidor.</p>
 * @package FWD5
 * @subpackage eventClass
 */
class FWDTreeAjaxExpand extends FWDRunnable {

	/**
	 * Objeto dono do evento
	 * var FWDTreeAjax
	 * @access private
	 */
	private $coOwner = null;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDTreeAjaxExpand. Seta o identificador e o dono do
	 * evento.</p>
	 * @access public
	 * @param string $psIdentifier Identificador do evento do usu�rio
	 * @param FWDTreeAjax $poOwner Objeto dono do evento
	 */
	public function __construct($psIdentifier,$poOwner){
		parent::__construct($psIdentifier);
		$this->coOwner = $poOwner;
	}
	 
	/**
	 * M�todo que trata o evento
	 *
	 * <p>Carrega, do XML, os filhos da �rvore, os adiciona � �rvore e a expande.</p>
	 * @access public
	 */
	public function run(){
		$msTreeBaseName = $this->coOwner->getTreeBaseName();
		$moTreeBase = FWDWebLib::getObject($msTreeBaseName);
		$moTreeBase->setIds();
		echo $this->coOwner->expand();
	}
}
?>