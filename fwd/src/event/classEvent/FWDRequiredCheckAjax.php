<?
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDGridChangePageBack. Implementa Eventos Servidor para a FWDGrid.
 *
 * <p>Classe que implementa Eventos Servidor de backPage para a FWDGrid
 * na Framework FWD5.</p>
 * @package FWD5
 * @subpackage eventClass
 */
class FWDRequiredCheckAjax extends FWDRunnable
{

	/**
	 * Alvos do evento  (separados por ":")
	 * @var string
	 * @access protected
	 */
	protected $csElements="";

	/**
	 * Eventos que devem ser disparados em caso de verifica��o "Required OK"  (separados por ";")
	 * @var string
	 * @access protected
	 */
	protected $csEventsOk ="";

	/**
	 * Eventos que devem ser disparados em caso de verifica��o "Required Error"  (separados por ";")
	 * @var string
	 * @access protected
	 */
	protected $csEventsError="";

	/**
	 * M�todo que trata o evento
	 *
	 * <p>Carrega, do XML, os valores dos elementos que devem ser tratados se foram preenchidos ou n�o,
	 * caso afirmativo, dispara o evento para desfazer as marca��es possivelmente feitas anteriormente e
	 * dispara os eventos que foram definidos no XML para serem executados em caso de todos os campos estarem
	 * preenchidos. Em caso negativo, troca a classe dos campos definidos como "label" dos campos que
	 * devem estar preenchidos, e depois dispara todos os eventos que devem ser executados em caso de pelo menos
	 * um dos campos n�o estarem preenchidos.</p>
	 * @access public
	 */
	public function run()
	{
		$moEvent = FWDWebLib::getObject($this->getAttrIdentifier());

		if($moEvent)
		{
			$msRequiredErrors = "";
			$msRequireOk="";
			$maElements = explode(':',$moEvent->csElements);
				
			foreach ($maElements as $msElementName)
			{
				$msElement = FWDWebLib::getPOST($msElementName);

				if(!$msElement)
				{
					$msRequiredErrors .= $msElementName . ":";
				}
				else
				{
					$msRequireOk .= $msElementName . ":";
				}
			}
				
			$moJs = new FWDJsEvent(JS_REQUIRED_CHECK_AJAX,$msRequiredErrors,$msRequireOk);
			echo $moJs->render();
				
			if($msRequiredErrors=="")
			{
				echo $this->csEventsOk;
			}
			else
			{
				echo $this->csEventsError;

			}
		}
		else
		{
			//n�o foi encontrado no Post o objeto do FWDRequiredCheck que � indicado por "$this->getAttrEventObj()" assim
			//n�o ha nada pra fazer
		}
	}

	/**
	 * Seta os eventos que devem ser disparados caso a verifica��o de "RequiredCheck" seja Positiva.
	 *
	 * <p>Seta os que devem ser disparados caso a verifica��o de "RequiredCheck" seja Positiva, ou seja
	 * os eventos que devem ser disparados caso todos os campos estejam preenchidos</p>
	 * @access public
	 * @param string $psEventsOk Eventos que devem ser disparados caso Verifica��o seja Positiva
	 */
	public function setAttrEventsOk($psEventsOk)
	{
		$this->csEventsOk = $psEventsOk;
	}

	/**
	 * Seta os eventos que devem ser disparados caso a verifica��o de "RequiredCheck" seja Negativa.
	 *
	 * <p>Seta os que devem ser disparados caso a verifica��o de "RequiredCheck" seja Negativa, ou seja
	 * os eventos que devem ser disparados caso pelo menos um dos campos n�o esteja preenchido</p>
	 * @access public
	 * @param string $psEventsError Eventos que devem ser disparados caso Verifica��o seja Negativa
	 */
	public function setAttrEventsError($psEventsError)
	{
		$this->csEventsError = $psEventsError;
	}

	/**
	 * Seta elementos do evento.
	 *
	 * <p>Seta os elementos sobre os quais o evento atuar�.</p>
	 * @access public
	 * @param string $psElements Elementos alvo do evento
	 */
	public function setAttrElements($psElements)
	{
		$this->csElements = $psElements;
	}
}
?>