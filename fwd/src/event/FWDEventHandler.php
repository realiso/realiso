<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDEventHandler. Manipula eventos.
 *
 * <p>Classe que manipula eventos dentro da Framework FWD5.</p>
 * @package FWD5
 * @subpackage event
 */
class FWDEventHandler implements FWDCollectable {

	/**
	 * Array de eventos
	 * @var array
	 * @access protected
	 */
	protected $caContent = array();

	/**
	 * Alvo do evento
	 * @var string
	 * @access protected
	 */
	protected $csEvent = "";

	/**
	 * Elementos sobre os quais o evento atuar�
	 * @var string
	 * @access protected
	 */
	protected $csElem = "";

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDEventHandler. Seta os atributos de
	 * Alvo e Elementos do evento.</p>
	 * @access public
	 * @param string $psEvent Alvo do evento
	 * @param string $psElem Elementos sobre os quais o evento atuar�
	 */
	public function __construct($psEvent,$psElem = "") {
		$this->setAttrEvent($psEvent);
		$this->setAttrElem($psElem);
	}

	/**
	 * Gera c�digo para renderizar os eventos.
	 *
	 * <p>Retorna uma string que cont�m o c�digo para renderizar os eventos.</p>
	 * @access public
	 * @return string C�digo para renderizar os eventos
	 */
	public function render(){
		if(strtolower($this->getAttrEvent())=='onpressenter'){
			$msHeader = "onkeydown='e=(arguments.length?arguments[0]:window.event);if(e.keyCode==13){";
			$msFooter = "}'";
		}else{
			$msHeader = "{$this->getAttrEvent()}='";
			$msFooter = "'";
		}
		$msBody = '';
		foreach($this->getAttrContent() as $moEvent){
			$msBody.= " ".$moEvent->render($this->getAttrElem());
		}
		return $msHeader.$msBody.$msFooter;
	}

	/**
	 * Seta um evento.
	 *
	 * <p>Seta um evento.</p>
	 * @access public
	 * @param FWDEvent $poContent Evento.
	 */
	public function setAttrContent(FWDEvent $poContent) {
		$this->caContent[] = $poContent;
	}

	/**
	 * Seta alvo do evento.
	 *
	 * <p>Seta alvo do evento.</p>
	 * @access public
	 * @param string $psEvent Alvo do evento
	 */
	public function setAttrEvent($psEvent) {
		$this->csEvent = $psEvent;
	}

	/**
	 * Seta elementos do evento.
	 *
	 * <p>Seta os elementos sobre os quais o evento atuar�.</p>
	 * @access public
	 * @param string $psElem Elementos do evento
	 */
	public function setAttrElem($psElem) {
		$this->csElem = $psElem;
	}

	/**
	 * Retorna o array de eventos.
	 *
	 * <p>Retorna o array de eventos (objetos).</p>
	 * @access public
	 * @return array Array de eventos (objetos)
	 */
	public function getAttrContent() {
		return $this->caContent;
	}

	/**
	 * Retorna alvo do evento.
	 *
	 * <p>Retorna alvo do evento.</p>
	 * @access public
	 * @return string Alvo do evento
	 */
	public function getAttrEvent() {
		return $this->csEvent;
	}

	/**
	 * Retorna elementos do evento.
	 *
	 * <p>Retorna os elementos sobre os quais o evento atuar�.</p>
	 * @access public
	 * @return string Elementos do evento
	 */
	public function getAttrElem() {
		return $this->csElem;
	}

	/**
	 * Retorna o array de eventos do elemento e o pr�prio elemento.
	 *
	 * <p>Retorna o array de eventos do elemento e o pr�prio elemento.</p>
	 * @access public
	 * @return array Eventos do elemento e elemento
	 */
	public function collect() {
		$maOut = array($this);
		if ($this->caContent)
		$maOut = array_merge($maOut,$this->caContent);
		return $maOut;
	}
}
?>