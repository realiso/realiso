<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDStartEvent. Singleton. Manipula eventos criados pelo usu�rio.
 *
 * <p>Classe singleton que manipula eventos criados pelo usu�rio na Framework FWD5.</p>
 * @package FWD5
 * @subpackage event
 */

//define as constantes de prioridade do tipo de evento beforeEvent
define( "HIGH",1);
define( "ABOVE_MIDDLE",2);
define( "MIDDLE",3);
define( "BELOW_MIDDLE",4);
define( "LOW",5);
class FWDStartEvent {

	/**
	 * Inst�ncia �nica da classe FWDStartEvent
	 * @staticvar FWDStartEvent
	 * @access private
	 */
	private static $coStartEvent = NULL;

	/**
	 * Estrutura que armazena os eventos ajax
	 * @var array
	 * @access protected
	 */
	protected $caAjaxEvent = array();

	/**
	 * Estrutura que armazena os eventos de submit
	 * @var array
	 * @access protected
	 */
	protected $caSubmitEvent = array();

	/**
	 * Estrutura que armazena os eventos que devem ser executados antes dos eventos ajax e submit
	 * @var array
	 * @access protected
	 */
	protected $caBeforeEvent = array();

	/**
	 * Estrutura que armazena os eventos que devem ser executados caso nao exista evento ajax e de submit
	 * a serem executados
	 * @var array
	 * @access protected
	 */
	protected $coAfterEvent = null;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDStartEvent.</p>
	 * @access private
	 */
	private function __construct() {}

	/**
	 * Retorna a inst�ncia �nica da classe FWDStartEvent (singleton).
	 *
	 * <p>Retorna a inst�ncia �nica da classe FWDStartEvent. Se a inst�ncia
	 * ainda n�o existir, cria a �nica inst�ncia. Se a inst�ncia j� existir,
	 * retorna a �nica inst�ncia (singleton).</p>
	 * @access public
	 * @return FWDStartEvent Inst�ncia �nica da classe FWDStartEvent
	 * @static
	 */
	public static function getInstance() {
		if (self::$coStartEvent == NULL) {
			self::$coStartEvent = new FWDStartEvent();
		}
		else {}	// inst�ncia �nica j� existe, retorne-a
		return self::$coStartEvent;
	}

	/**
	 * Exibe o conte�do da inst�ncia �nica FWDStartEvent.
	 *
	 * <p>Exibe na tela o conte�do de todos os atributos da �nica inst�ncia
	 * da classe FWDStartEvent.</p>
	 * @access public
	 */
	public function dump_instance() {
		echo "<br/><br/><br/><br/>--------------[ FWDStartEvent Singleton Dump <strong>BEGIN</strong> ]--------------<br/>";
		echo "<h3>coStartEvent:</h3>";
		var_dump(self::$coStartEvent);
		echo "<br/><h3>caEventArray:</h3>";
		var_dump($this->caAjaxEvent);
		echo "<br/><h3>caSubmitEvent:</h3>";
		var_dump($this->caSubmitEvent);
		echo "<br/><h3>caBeforeEventEvent:</h3>";
		var_dump($this->caBeforeEventEvent);
		echo "<br/><h3>caAfterEvent:</h3>";
		var_dump($this->caAfterEvent);

		echo "<h3></h3>--------------[ FWDStartEvent Singleton Dump <strong>END</strong> ]--------------<br/><br/><br/><br/>";
	}

	/**
	 * Adiciona um evento ajax criado pelo usu�rio.
	 *
	 * <p>Adiciona um evento ajax criado pelo usu�rio.</p>
	 * @access public
	 * @param object $poEvent Evento ajax criado pelo usu�rio
	 */
	public function addAjaxEvent(FWDRunnable $poEvent) {
		$this->caAjaxEvent[strtolower($poEvent->getAttrIdentifier())] = $poEvent;
		if(!(FWDWebLib::getObject($poEvent->getAttrIdentifier())))
		FWDWebLib::addObject($poEvent->getAttrIdentifier(), $poEvent);
	}

	/**
	 * Seta o evento de tela definido pelo usu�rio.
	 *
	 * <p>Seta o evento de tela definido pelo usu�rio</p>
	 * @access public
	 * @param object $poEvent Evento ajax criado pelo usu�rio
	 */
	public function addBeforeAjax(FWDRunnable $poEvent,$piPriority = MIDDLE) {
		FWDWebLib::attributeParser($piPriority,array("1"=>HIGH, "2"=>ABOVE_MIDDLE, "3"=>MIDDLE, "4"=>BELOW_MIDDLE, "5"=>LOW, ),"piPriority");
		if(!isset($this->caBeforeEvent[$piPriority])){
			$maRow = array();
			$maRow[strtolower($poEvent->getAttrIdentifier())] = $poEvent;
			$this->caBeforeEvent[$piPriority] =  $maRow;
		}else{
			$this->caBeforeEvent[$piPriority][strtolower($poEvent->getAttrIdentifier())] = $poEvent;
		}
		if(!(FWDWebLib::getObject($poEvent->getAttrIdentifier())))
		FWDWebLib::addObject($poEvent->getAttrIdentifier(), $poEvent);
	}

	/**
	 * Seta o evento de tela definido pelo usu�rio.
	 *
	 * <p>Seta o evento de tela definido pelo usu�rio</p>
	 * @access public
	 * @param object $poEvent Evento ajax criado pelo usu�rio
	 */
	public function setScreenEvent(FWDRunnable $poEvent) {
		$this->coAfterEvent = $poEvent;
		if(!(FWDWebLib::getObject($poEvent->getAttrIdentifier())))
		FWDWebLib::addObject($poEvent->getAttrIdentifier(), $poEvent);
	}

	/**
	 * Adiciona um evento submit criado pelo usu�rio.
	 *
	 * <p>Adiciona um evento submit criado pelo usu�rio.</p>
	 * @access public
	 * @param object $poEvent Evento submit criado pelo usu�rio
	 */
	public function addSubmitEvent(FWDRunnable $poEvent) {
		$this->caSubmitEvent[strtolower($poEvent->getAttrIdentifier())] = $poEvent;
		if(!(FWDWebLib::getObject($poEvent->getAttrIdentifier())))
		FWDWebLib::addObject($poEvent->getAttrIdentifier(), $poEvent);
	}

	/**
	 * Retorna o nome do evento ajax que foi disparado.
	 *
	 * <p>Retorna o nome do evento ajax que foi disparado.</p>
	 * @access public
	 */
	public function getAjaxEventName(){
		return FWDWebLib::getPOST("fwd_ajax_event_name");
	}

	/**
	 * Executa os eventos criados pelo usu�rio.
	 *
	 * <p>Executa os eventos criados pelo usu�rio.</p>
	 * @access public
	 */
	public function start() {
		$mbExecute = false;
		$msEvent = FWDWebLib::getPOST("fwd_ajax_event_name");
		$miAjax = FWDWebLib::getPOST("fwd_ajax_trigger");
		$msSubmitTrigger = FWDWebLib::getPOST("fwd_submit_trigger");


		if(isset($this->caBeforeEvent[HIGH])){
			foreach($this->caBeforeEvent[HIGH] as $moEvent)
			if(isset($this->caBeforeEvent[HIGH][strtolower($msEvent)])){
				FWDWebLib::getInstance()->writeFunction2Debug(get_class($moEvent),'run',array(),FWD_DEBUG_FWD1);
				$moEvent->run();
			}
		}
		if(isset($this->caBeforeEvent[ABOVE_MIDDLE]))
		foreach($this->caBeforeEvent[ABOVE_MIDDLE] as $moEvent){
			FWDWebLib::getInstance()->writeFunction2Debug(get_class($moEvent),'run',array(),FWD_DEBUG_FWD1);
			$moEvent->run();
		}
		if(isset($this->caBeforeEvent[MIDDLE]))
		foreach($this->caBeforeEvent[MIDDLE] as $moEvent){
			FWDWebLib::getInstance()->writeFunction2Debug(get_class($moEvent),'run',array(),FWD_DEBUG_FWD1);
			$moEvent->run();
		}
		if(isset($this->caBeforeEvent[BELOW_MIDDLE]))
		foreach($this->caBeforeEvent[BELOW_MIDDLE] as $moEvent){
			FWDWebLib::getInstance()->writeFunction2Debug(get_class($moEvent),'run',array(),FWD_DEBUG_FWD1);
			$moEvent->run();
		}
		if(isset($this->caBeforeEvent[LOW]))
		foreach($this->caBeforeEvent[LOW] as $moEvent){
			FWDWebLib::getInstance()->writeFunction2Debug(get_class($moEvent),'run',array(),FWD_DEBUG_FWD1);
			$moEvent->run();
		}


		if((isset($miAjax))&&(isset($msEvent))&&(isset($this->caAjaxEvent[strtolower($msEvent)]))){
			FWDWebLib::getInstance()->writeFunction2Debug(get_class($this->caAjaxEvent[strtolower($msEvent)]),'run',array(),FWD_DEBUG_FWD1);
			$this->caAjaxEvent[strtolower($msEvent)]->run();
			$mbExecute = true;
		}

		if ($mbExecute || isset($miAjax))
		exit();

		if((!isset($miAjax)) && (isset($msSubmitTrigger)) && (isset($this->caSubmitEvent[strtolower($msSubmitTrigger)])) ){
			FWDWebLib::getInstance()->writeFunction2Debug(get_class($this->caSubmitEvent[strtolower($msSubmitTrigger)]),'run',array(),FWD_DEBUG_FWD1);
			$this->caSubmitEvent[strtolower($msSubmitTrigger)]->run();
			$mbExecute=true;
		}

		if($mbExecute)
		exit();
			
		if(isset($this->coAfterEvent)){
			FWDWebLib::getInstance()->writeFunction2Debug(get_class($this->coAfterEvent),'run',array(),FWD_DEBUG_FWD1);
			$this->coAfterEvent->run();
		}
	}
}
?>