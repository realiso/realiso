<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDToolTip. Implementa ToolTips.
 *
 * <p>Classe que implementa ToolTips em elementos da FWD5. Um ToolTip � tratado
 * como um evento onMouseOver.</p>
 * @package FWD5
 * @subpackage event
 */
class FWDToolTip extends FWDEvent {

	/**
	 * Valor do ToolTip
	 * @var FWDString
	 * @access private
	 */
	private $coString = null;

	/**
	 * Classe CSS do ToolTip (wz_tooltip.js: ttFWDClass)
	 * @var string
	 * @access private
	 */
	private $csClass = "FWDToolTip";

	/**
	 * Tempo que o ToolTip demora para aparecer [milisegundos] (wz_tooltip.js: ttDelay)
	 * @var integer
	 * @access private
	 */
	private $ciShowDelay = 0;

	/**
	 * Tempo que o ToolTip demora para desaparecer [milisegundos] (wz_tooltip.js: ttTemp)
	 * @var integer
	 * @access private
	 */
	private $ciHideDelay = 0;

	/**
	 * Transpar�cia do ToolTip ([0..100], 100=solid) (wz_tooltip.js: ttOpacity)
	 * @var integer
	 * @access private
	 */
	private $ciOpacity = 100;

	/**
	 * ToolTip nao se move com o mouse (wz_tooltip.js: ttStatic)
	 * @var boolean
	 * @access private
	 */
	private $cbStatic = false;

	/**
	 * ToolTip nao desaparece no evento onMouseOut (wz_tooltip.js: ttSticky)
	 * @var boolean
	 * @access private
	 */
	private $cbSticky = false;

	/**
	 * Largura do ToolTip, em pixels (a altura � determinada automaticamente) (wz_tooltip.js: ttWidth)
	 * @var integer
	 * @access private
	 */
	private $ciWidth = 300;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDToolTip.</p>
	 * @access public
	 */
	public function __construct() {
		$this->coString = new FWDString();
		$this->setAttrEvent("onmouseover");
		$this->setAttrFunction("tooltip");
	}

	/**
	 * Renderiza o ToolTip (se possuir valor).
	 *
	 * <p>Renderiza o ToolTip. Instancia um objeto de evento javascript
	 * com os valores necess�rios e renderiza o evento (onMouseOver).</p>
	 * @access public
	 * @return string String que ir� disparar o Evento Cliente
	 */
	public function render() {
		if ($this->coString->getAttrString()) {
			$msValue = $this->coString->getAttrString();
			$msValue = html_entity_decode($msValue);
			$msValue = str_replace(array("\n","\r"),array(" ",""),$msValue);
			$msValue = str_replace('\\','\\\\',$msValue);
			$msValue = str_replace('"','\"',$msValue);
			$msReturn = '';
			if(strlen($msValue)>60){
				for($miI=0;$miI<mb_strlen($msValue);$miI+=50){
					$msSub = mb_substr($msValue,$miI,60, mb_detect_encoding($msValue, 'UTF-8, ISO-8859-1'));
					if(mb_strpos($msSub,' ', null, mb_detect_encoding($msSub, 'UTF-8, ISO-8859-1'))===false)
					$msReturn .= mb_substr($msSub,0,50,mb_detect_encoding($msSub, 'UTF-8, ISO-8859-1')) . ' ';
					else
					$msReturn .= mb_substr($msSub,0,50,mb_detect_encoding($msSub, 'UTF-8, ISO-8859-1'));
				}
			}else{
				$msReturn = $msValue;
			}
			if ($this->coString->getAttrString()===false)
			trigger_error("Object FWDToolTip must have an non-empty object FWDString",E_USER_WARNING);

			if ($this->getAttrShowDelay() < 0)
			trigger_error("Object FWDToolTip: attribute ShowDelay must be a positive integer",E_USER_ERROR);

			if ($this->getAttrHideDelay() < 0)
			trigger_error("Object FWDToolTip: attribute HideDelay must be a positive integer",E_USER_ERROR);

			if ($this->getAttrOpacity() < 0 || $this->getAttrOpacity() > 100)
			trigger_error("Object FWDToolTip: attribute Opacity must be a positive integer less than or equal to 100",E_USER_ERROR);

			$msElems = "";
			$msElems .= "ttFWDClass=\"{$this->getAttrClass()}\"; ";
			$msElems .= "ttDelay={$this->getAttrShowDelay()}; ";
			$msElems .= "ttTemp={$this->getAttrHideDelay()}; ";
			$msElems .= "ttOpacity={$this->getAttrOpacity()}; ";
			$msElems .= $this->getAttrStatic() ? "ttStatic=true; " : "ttStatic=false; ";
			$msElems .= $this->getAttrSticky() ? "ttSticky=true; " : "ttSticky=false; ";
			$msElems .= ($this->getAttrWidth()<60) ? "ttWidth=60; " : "ttWidth={$this->getAttrWidth()};";

			$moJs = new FWDJsEvent($this->getAttrFunction(),$msElems,/*htmlentities*/($msReturn));
			return $moJs->render();
		}
	}

	/**
	 * Este m�todo n�o deve fazer nada.
	 *
	 * <p>Esta classe n�o deve utilizar este m�todo.</p>
	 * @access public
	 */
	public function run() {
	}

	/**
	 * Seta o valor do ToolTip.
	 *
	 * <p>M�todo que setar o valor do ToolTip.
	 * Se force for FALSE, concatena o valor se a vari�vel tiver conte�do;
	 * se TRUE atribui mesmo que tenha conte�do (sobrescreve).</p>
	 * @access public
	 * @param string $psValue Valor do ToolTip
	 * @param boolean $pbForce For�ar o valor mesmo que j� tenha conte�do
	 */
	public function setValue($psValue,$pbForce = false) {
		$this->coString->setValue($psValue,$pbForce);
	}

	/**
	 * Seta o valor do ToolTip.
	 *
	 * <p>M�todo que setar o valor do ToolTip. Metodo criado para que o tooltip funcione da tree</p>
	 * @access public
	 * @param string $psValue Valor da posi��o
	 */
	public function setAttrPosition($psValue) {
	}

	/**
	 * Seta o tempo que o ToolTip demora para aparecer.
	 *
	 * <p>M�todo que seta o tempo que o ToolTip demora para aparecer
	 * (onMouseOver).</p>
	 * @access public
	 * @param string $psShowDelay Tempo que o ToolTip demora para aparecer
	 */
	public function setAttrShowDelay($psShowDelay) {
		$this->ciShowDelay = $psShowDelay;
	}

	/**
	 * Retorna o tempo que o ToolTip demora para aparecer.
	 *
	 * <p>M�todo que retorna o tempo que o ToolTip demora para aparecer
	 * (onMouseOver).</p>
	 * @access public
	 * @return integer Tempo que o ToolTip demora para aparecer
	 */
	public function getAttrShowDelay() {
		return $this->ciShowDelay;
	}

	/**
	 * Seta o tempo que o ToolTip demora para desaparecer.
	 *
	 * <p>M�todo que seta o tempo que o ToolTip demora para desaparecer
	 * (onMouseOut).</p>
	 * @access public
	 * @param string $psHideDelay Tempo que o ToolTip demora para desaparecer
	 */
	public function setAttrHideDelay($psHideDelay) {
		$this->ciHideDelay = $psHideDelay;
	}

	/**
	 * Retorna o tempo que o ToolTip demora para desaparecer.
	 *
	 * <p>M�todo que retorna o tempo que o ToolTip demora para desaparecer
	 * (onMouseOut).</p>
	 * @access public
	 * @return integer Tempo que o ToolTip demora para desaparecer
	 */
	public function getAttrHideDelay() {
		return $this->ciHideDelay;
	}

	/**
	 * Seta a transpar�ncia do ToolTip.
	 *
	 * <p>M�todo que seta a transpar�ncia do ToolTip. Intervalo inteiro fechado
	 * de 0 a 100: 0=M�xima transpar�ncia, 100=M�nima transpar�ncia (solid).</p>
	 * @access public
	 * @param string $psOpacity Transpar�ncia do ToolTip
	 */
	public function setAttrOpacity($psOpacity) {
		$this->ciOpacity = $psOpacity;
	}

	/**
	 * Retorna a transpar�ncia do ToolTip.
	 *
	 * <p>M�todo que retorna a transpar�ncia do ToolTip.</p>
	 * @access public
	 * @return integer Transpar�ncia do ToolTip
	 */
	public function getAttrOpacity() {
		return $this->ciOpacity;
	}

	/**
	 * Seta o atributo Static do ToolTip.
	 *
	 * <p>M�todo que seta o valor booleano do atributo Static do ToolTip.
	 * Para o ToolTip mover-se junto com o mouse, cbStatic deve conter o valor
	 * booleano FALSE. Para n�o acompanhar o movimento do mouse, o atributo
	 * deve conter o valor booleano TRUE.</p>
	 * @access public
	 * @param string $psStatic Define se o ToolTip n�o deve mover-se junto com o mouse
	 */
	public function setAttrStatic($psStatic) {
		$this->cbStatic = FWDWebLib::attributeParser($psStatic,array("true"=>true, "false"=>false),"psStatic");
	}

	/**
	 * Retorna o atributo Static do ToolTip.
	 *
	 * <p>M�todo que retorna o atributo Static do ToolTip. Retorna FALSE se
	 * o ToolTip deve mover-se junto com o mouse. Retorna TRUE caso contr�rio.</p>
	 * @access public
	 * @return boolean Indica se o ToolTip n�o deve mover-se junto com o mouse
	 */
	public function getAttrStatic() {
		return $this->cbStatic;
	}

	/**
	 * Seta o atributo Sticky do ToolTip.
	 *
	 * <p>M�todo que seta o valor booleano do atributo Sticky do ToolTip.
	 * Para o ToolTip desaparecer no evento onMouseOut, cbSticky deve conter
	 * o valor booleano FALSE. Para n�o desaparecer, o atributo deve conter
	 * o valor booleano TRUE.</p>
	 * @access public
	 * @param string $psSticky Define se o ToolTip n�o deve desaparecer no evento onMouseOut
	 */
	public function setAttrSticky($psSticky) {
		$this->cbSticky = FWDWebLib::attributeParser($psSticky,array("true"=>true, "false"=>false),"psSticky");
	}

	/**
	 * Retorna o atributo Sticky do ToolTip.
	 *
	 * <p>M�todo que retorna o atributo Sticky do ToolTip. Retorna FALSO se
	 * o ToolTip deve desaparecer no evento onMouseOut. Retorna TRUE caso contr�rio.</p>
	 * @access public
	 * @return boolean Indica se o ToolTip n�o deve desaparecer no evento onMouseOut
	 */
	public function getAttrSticky() {
		return $this->cbSticky;
	}

	/**
	 * Seta a largura do ToolTip.
	 *
	 * <p>M�todo que seta a largura do ToolTip, em pixels.</p>
	 * @access public
	 * @param string $psWidth Largura do ToolTip, em pixels
	 */
	public function setAttrWidth($psWidth) {
		$this->ciWidth = $psWidth;
	}

	/**
	 * Retorna a largura do ToolTip.
	 *
	 * <p>M�todo que retorna a largura do ToolTip, em pixels.</p>
	 * @access public
	 * @return integer Largura do ToolTip, em pixels
	 */
	public function getAttrWidth() {
		return $this->ciWidth;
	}

	/**
	 * Seta a classe CSS do ToolTip.
	 *
	 * <p>M�todo que seta a classe CSS do ToolTip.</p>
	 * @access public
	 * @param string $psClass Classe CSS do ToolTip
	 */
	public function setAttrClass($psClass) {
		$this->csClass = $psClass;
	}

	/**
	 * Retorna a classe CSS do ToolTip.
	 *
	 * <p>M�todo que retorna a classe CSS do ToolTip.</p>
	 * @access public
	 * @return string Classe CSS do ToolTip
	 */
	public function getAttrClass() {
		return $this->csClass;
	}

	/**
	 * Adiciona um valor ao ToolTip.
	 *
	 * <p>M�todo para adicionar um valor ao ToolTip. Insere
	 * (ou concatena, caso o objeto coString ja exista) um valor no ToolTip
	 * (objeto FWDString).</p>
	 * @access public
	 * @param FWDString $poString Valor a ser adicionado (concatenado) ao ToolTip
	 */
	public function setObjFWDString(FWDString $poString) {
		$this->coString->setValue($poString->getAttrString());
		$msStringId = $poString->getAttrId();
		if(!empty($msStringId))
		$this->coString->setAttrId($msStringId);
	}

	/**
	 * Adiciona um link ao valor do ToolTip.
	 *
	 * <p>M�todo para adicionar um link ao valor do ToolTip. Insere
	 * (ou concatena, caso o objeto coString ja exista) um link no valor
	 * do ToolTip (objeto FWDString).</p>
	 * @access public
	 * @param FWDLink $poLink Link a ser adicionado (concatenado) ao ToolTip
	 */
	public function setObjFWDLink(FWDLink $poLink) {
		$msAnchorTag = $poLink->draw();
		$msAnchorTag = str_replace(array("'","\""), array("\"","\\\""), $msAnchorTag);
		$this->coString->setAttrNoEscape("true");
		$this->coString->setValue($msAnchorTag);
	}
}
?>