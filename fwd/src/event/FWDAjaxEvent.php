<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDAjaxEvent. Implementa Eventos Ajax.
 *
 * <p>Classe que implementa Eventos Ajax na Framework FWD5.</p>
 * @package FWD5
 * @subpackage event
 */
class FWDAjaxEvent extends FWDEvent {

	/**
	 * Prioridade do Evento Ajax
	 * @var integer
	 * @access private
	 */
	private $ciPriority = 3;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDAjaxEvent. Seta os atributos de
	 * alvo, fun��o, elementos e par�metros do evento.</p>
	 * @access public
	 * @param string $psEvent Alvo do evento
	 * @param string $psFunction Fun��o do evento
	 * @param string $psElements Elementos sobre os quais o evento atuar�
	 * @param string $psParameters Par�metros necess�rios a alguns eventos
	 */
	public function __construct($psEvent = "" ,$psFunction = "",$psElements = "",$psParameters = ""){
		parent::__construct($psEvent,$psFunction,$psElements,$psParameters);
	}

	/**
	 * Renderiza o Evento Ajax.
	 *
	 * <p>Renderiza o Evento Ajax. Re�ne em uma vari�vel string
	 * todas as informa��es necess�rias para implementar o Evento Ajax.</p>
	 * @access public
	 * @return string String que ir� disparar o Evento Ajax
	 */
	public function render() {
		return "trigger_event(\"{$this->getAttrName()}\",\"{$this->getAttrPriority()}\");";
	}

	/**
	 * Seta a prioridade do evento.
	 *
	 * <p>Seta a prioridade do evento (1=M�xima, 2=M�dia, 3=M�nima[default]).</p>
	 * @access public
	 * @param string $psPriority Prioridade do Evento Ajax
	 */
	public function setAttrPriority($psPriority) {
		if ($psPriority<0 || $psPriority>3)
		trigger_error("Ajax event priority must be '0', '1', '2' or '3' (got '{$psPriority}')",E_USER_ERROR);
		else
		$this->ciPriority = $psPriority;
	}

	/**
	 * Retorna a prioridade do evento.
	 *
	 * <p>Retorna a prioridade do evento (1=M�xima, 2=M�dia, 3=M�nima[default]).</p>
	 * @access public
	 * @param string psPriority Prioridade do Evento Ajax
	 */
	public function getAttrPriority() {
		return $this->ciPriority;
	}
}
?>