<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDTempEvent. Implementa Eventos Temporais.
 *
 * <p>Classe que implementa Eventos Temporais na Framework FWD5.</p>
 * @package FWD5
 * @subpackage event
 */
class FWDTempEvent {

	/**
	 * Evento Temporal
	 * @var FWDEvent
	 * @access protected
	 */
	protected $coContent = null;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDTempEvent.</p>
	 * @access public
	 */
	public function __construct() {
	}

	/**
	 * Renderiza o Evento Temporal.
	 *
	 * <p>Renderiza o Evento Temporal. Re�ne em uma vari�vel string
	 * todas as informa��es necess�rias para implementar o Evento Temporal.</p>
	 * @access public
	 * @return string String que ir� disparar o Evento Temporal.
	 */
	public function render() {
		return $this->getContent()->render();
	}

	/**
	 * Adiciona um objeto de Evento.
	 *
	 * <p>Adiciona um objeto de Evento FWDTempEvent.</p>
	 * @access public
	 * @param object $poContent Evento.
	 */
	public function setObjFWDEvent(FWDEvent $poContent)	{
		$this->coContent = $poContent;
	}

	/**
	 * Retorna o objeto de Evento Temporal.
	 *
	 * <p>Retorna o objeto de Evento Temporal.</p>
	 * @access public
	 * @return object Evento Temporal
	 */
	public function getContent() {
		return $this->coContent;
	}
}
?>