<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDMaskAutoComplete. Implementa a funcionalidade 'AutoCompletar'.
 *
 * <p>Classe que implementa a funcionalidade 'AutoCompletar' de entradas
 * utilizando m�scaras. A verifica��o/valida��o da entrada atrav�s da m�scara
 * � realizada sempre que uma tecla � pressionada.</p>
 * @package FWD5
 * @subpackage event
 */
class FWDMaskAutoComplete extends FWDMask {

	/**
	 * Evento javascript que dispara a valida��o da entrada (atrav�s da m�scara)
	 * @var string
	 * @access protected
	 */
	protected $csEvent = "OnKeyPress";

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDMaskAutoComplete.</p>
	 * @access public
	 */
	public function __construct() {
	}

	/**
	 * Realiza a verifica��o/valida��o da entrada atrav�s da m�scara.
	 *
	 * <p>Re�ne em uma vari�vel string, contendo c�digo HTML/javascript,
	 * todas as informa��es necess�rias para implementar a funcionalidade
	 * AutoCompletar (valida��o da entrada atrav�s da m�scara a cada tecla
	 * pressionada).</p>
	 * @access public
	 * @param string $psElem Id do elemento
	 * @return string String que ir� disparar a valida��o da entrada
	 */
	public function render($psElem) {
		switch($this->getAttrType()){
			case MASK_DD_MM_YYYY_1:  // 31/12/2006
				$msCode  = "e=event;";
				$msCode .= "if(e.keyCode==e.which || e.which==0) return true;";
				$msCode .= "kc=e.keyCode?e.keyCode:e.which;";
				$msCode .= "if(String.fromCharCode(kc)==\" \") return false;";
				$msCode .= "if(String.fromCharCode(kc)!=0)";
				$msCode .= "if(!parseInt(String.fromCharCode(kc))) {";
				$msCode .= "  return false;";
				$msCode .= "}";
				$msCode .= "value=gebi(\"$psElem\").value;";
				$msCode .= "if(value.length==2||value.length==5) {";
				$msCode .= "  gebi(\"$psElem\").value += \"/\";";
				$msCode .= "}";
				$msCode .= "if(value.length > 9) {";
				$msCode .= "  return false;";
				$msCode .= "}";
				$msCode .= str_replace(array("\n","\r"),array(' ',' '),"
        d = String.fromCharCode(kc);
        
        daysInMonth=function(month,year){
          if(month==4||month==6||month==9||month==11){
            return 30;
          }else if(month==2){
            if(!year||year%400==0||(year%4==0&&year%100)){
              return 29;
            }else{
              return 28;
            }
          }else{
            return 31;
          }
        };
        
        var a = value.split(\"/\");
        if(a.length==1){
          day = a[0]+d;
          if(a[0].length==0){
            if(d>3) return false;
          }else{
            if(day>31||day==\"00\") return false;
          }
        }else if(a.length==2){
          day = a[0];
          month = a[1]+d;
          if(month.length==1&&month>1) return false;
          if(month==\"00\"||month>12) return false;
          if(daysInMonth(month)<day) return false;
        }else{
          day = a[0];
          month = a[1];
          year = a[2]+d;
          if(year.length==4&&daysInMonth(month,year)<day) return false;
        }
        
        ");
				break;
			case MASK_MM_DD_YYYY:  // 12/31/2006
				$msCode  = "e=event;";
				$msCode .= "if(e.keyCode==e.which || e.which==0) return true;";
				$msCode .= "kc=e.keyCode?e.keyCode:e.which;";
				$msCode .= "if(String.fromCharCode(kc)==\" \") return false;";
				$msCode .= "if(String.fromCharCode(kc)!=0)";
				$msCode .= "if(!parseInt(String.fromCharCode(kc))) {";
				$msCode .= "  return false;";
				$msCode .= "}";
				$msCode .= "value=gebi(\"$psElem\").value;";
				$msCode .= "if(value.length==2||value.length==5) {";
				$msCode .= "  gebi(\"$psElem\").value += \"/\";";
				$msCode .= "}";
				$msCode .= "if(value.length > 9) {";
				$msCode .= "  return false;";
				$msCode .= "}";
				$msCode .= str_replace(array("\n","\r",'  '),array(' ',' ',' '),"
        d = String.fromCharCode(kc);
        
        daysInMonth=function(month,year){
          if(month==4||month==6||month==9||month==11){
            return 30;
          }else if(month==2){
            if(!year||year%400==0||(year%4==0&&year%100)){
              return 29;
            }else{
              return 28;
            }
          }else{
            return 31;
          }
        };
        
        var a = value.split(\"/\");
        if(a.length==1){
          month = a[0]+d;
          if(a[0].length==0){
            if(d>1) return false;
          }else{
            if(month>12||month==\"00\") return false;
          }
        }else if(a.length==2){
          month = a[0];
          day = a[1]+d;
          if(day==\"00\") return false;
          if(day.length==1&&day>daysInMonth(month)/10) return false;
          if(daysInMonth(month)<day) return false;
        }else{
          month = a[0];
          day = a[1];
          year = a[2]+d;
          if(year.length==4&&daysInMonth(month,year)<day) return false;
        }
        
        ");
				break;
			case MASK_DD_MM_YYYY_2:  // 31-Dec-2006
				$msCode  = "e=event;";
				$msCode .= "if(e.keyCode==e.which || e.which==0) return true;";
				$msCode .= "kc=e.keyCode?e.keyCode:e.which;";
				$msCode .= "if(parseInt(String.fromCharCode(kc)) && (value.length==2 || value.length==3)) {";
				$msCode .= "  return false;";
				$msCode .= "}";
				$msCode .= "if(!parseInt(String.fromCharCode(kc))&& (value.length<2 || value.length>5))";
				$msCode .= "  return false;";
				$msCode .= "value=gebi(\"$psElem\").value;";
				$msCode .= "if(value.length==2||value.length==6) {";
				$msCode .= "  gebi(\"$psElem\").value += \"/\";";
				$msCode .= "}";
				$msCode .= "if(value.length > 10) {";
				$msCode .= "  return false;";
				$msCode .= "}";
				break;
			case MASK_HH_MM_SS:    // 23:59:59
				$msCode  = "e=event;";
				$msCode .= "if(e.keyCode==e.which || e.which==0) return true;";
				$msCode .= "kc=e.keyCode?e.keyCode:e.which;";
				$msCode .= "chr=String.fromCharCode(kc);";
				$msCode .= "if(chr==\" \") return false;";
				$msCode .= "if(chr!=0)";
				$msCode .= "if(!parseInt(chr))return false;";
				$msCode .= "if(value.length==0 && chr>\"2\")return false;";
				$msCode .= "if(value==2 && chr>\"3\")return false;";
				$msCode .= "value=gebi(\"$psElem\").value;";
				$msCode .= "if(value.length==2||value.length==5||value.length==3||value.length==6){";
				$msCode .= "  if(chr>\"5\"){return false}";
				$msCode .= "  else if(value.length==2||value.length==5){gebi(\"$psElem\").value += \":\";}";
				$msCode .= "}else if(value.length>7)return false;";
				break;
			case MASK_HH_MM:    // 23:59
				$msCode  = "e=event;";
				$msCode .= "if(e.keyCode==e.which || e.which==0) return true;";
				$msCode .= "kc=e.keyCode?e.keyCode:e.which;";
				$msCode .= "chr=String.fromCharCode(kc);";
				$msCode .= "if(chr==\" \") return false;";
				$msCode .= "if(chr!=0)";
				$msCode .= "if(!parseInt(chr))return false;";
				$msCode .= "if(value.length==0 && chr>\"2\")return false;";
				$msCode .= "if(value==2 && chr>\"3\")return false;";
				$msCode .= "value=gebi(\"$psElem\").value;";
				$msCode .= "if(value.length==2||value.length==3){";
				$msCode .= "  if(chr>\"5\"){return false}";
				$msCode .= "  else if(value.length==2){gebi(\"$psElem\").value += \":\";}";
				$msCode .= "}else if(value.length>4)return false;";
				break;
			case MASK_INTEGER:    // ...-2,-1,0,1,2...
				$msCode  = "e=event;";
				$msCode .= "if(e.keyCode==e.which || e.which==0) return true;";
				$msCode .= "kc=e.keyCode?e.keyCode:e.which;";
				$msCode .= "if(String.fromCharCode(kc)==\"-\" && value.length==0)";
				$msCode .= "  return;";
				$msCode .= "if(String.fromCharCode(kc)==\" \") return false;";
				$msCode .= "if(String.fromCharCode(kc)!=0)";
				$msCode .= "if(!parseInt(String.fromCharCode(kc))) {";
				$msCode .= "  return false;";
				$msCode .= "}";
				break;
			case MASK_FLOAT:    // 0.3, -12.34, 1234.67, ...
				$msCode  = "e=event;";
				$msCode .= "if(e.keyCode==e.which || e.which==0) return true;";
				$msCode .= "kc=e.keyCode?e.keyCode:e.which;";
				$msCode .= "if(String.fromCharCode(kc)==\"-\" && value.length==0)";
				$msCode .= "  return;";
				$msCode .= "if(String.fromCharCode(kc)==\".\" && value.indexOf(\".\")== -1)";
				$msCode .= "  return;";
				$msCode .= "if(String.fromCharCode(kc)==\" \") return false;";
				$msCode .= "if(String.fromCharCode(kc)!=0)";
				$msCode .= "if(!parseInt(String.fromCharCode(kc))) {";
				$msCode .= "  return false;";
				$msCode .= "}";
				break;
			case MASK_DIGIT:    // 0,1,2,3,4...
				$msCode  = "e=event;";
				$msCode .= "if(e.keyCode==e.which || e.which==0) return true;";
				$msCode .= "kc=e.keyCode?e.keyCode:e.which;";
				$msCode .= "if(String.fromCharCode(kc)==\" \") return false;";
				$msCode .= "if(String.fromCharCode(kc)!=0)";
				$msCode .= "if(!parseInt(String.fromCharCode(kc))) {";
				$msCode .= "  return false;";
				$msCode .= "}";
				break;
			case MASK_LETTER:    // a,b,c,...,z A,B,C,...,Z
				$msCode  = "e=event;";
				$msCode .= "if(e.keyCode==e.which || e.which==0) return true;";
				$msCode .= "kc=e.keyCode?e.keyCode:e.which;";
				$msCode .= "if(String.fromCharCode(kc)==0) return false;";
				$msCode .= "if(e.ctrlKey && kc==118) return false;";
				$msCode .= "if(parseInt(String.fromCharCode(kc))) {";
				$msCode .= "  return false;";
				$msCode .= "}";
				break;
			case MASK_EMAIL:    // fulano@fulanomail.com
				$msCode  = "e=event;";
				$msCode .= "if(e.keyCode==e.which || e.which==0) return true;";
				$msCode .= "kc=e.keyCode?e.keyCode:e.which;";
				$msCode .= "value=gebi(\"$psElem\").value;";
				$msCode .= "if(e.ctrlKey && kc==118) return false;";
				$msCode .= "if(String.fromCharCode(kc)==\"@\" && value.length < 1) {";
				$msCode .= "  return false;";
				$msCode .= "}";
				$msCode .= "if( (kc<45) || (kc==47) || (kc>57 && (kc<64)) || (kc>90 && kc<97 && kc!=95) || (kc>122 && kc<128)){";
				$msCode .= "  return false;";
				$msCode .= " }";
				$msCode .= "if(String.fromCharCode(kc)==\"@\" && value.indexOf(\"@\") > -1){";
				$msCode .= "  return false;";
				$msCode .= "}";
				break;
			case MASK_CEP:      // 91482-010
				$msCode  = "e=event;";
				$msCode .= "if(e.keyCode==e.which || e.which==0) return true;";
				$msCode .= "kc=e.keyCode?e.keyCode:e.which;";
				$msCode .= "if(String.fromCharCode(kc)==\" \") return false;";
				$msCode .= "if(String.fromCharCode(kc)!=0)";
				$msCode .= "if(!parseInt(String.fromCharCode(kc))) {";
				$msCode .= "  return false;";
				$msCode .= "}";
				$msCode .= "value=gebi(\"$psElem\").value;";
				$msCode .= "if(value.length==5) {";
				$msCode .= "  gebi(\"$psElem\").value += \"-\";";
				$msCode .= "}";
				$msCode .= "if(value.length > 8) {";
				$msCode .= "  return false;";
				$msCode .= "}";
				break;
			case MASK_MONEY:
				$msCode  = "
                    e=event;
                    value=gebi(\"$psElem\").value;
                    //pro�be o uso de setas e da tecla delete.                    
                    if(e.keyCode==37 || e.keyCode==39 || e.keyCode==46) return false;
                    if(e.keyCode!=8 && (e.keyCode==e.which || e.which==0)) return true;
                    value = value.replace(/[\.,]/g,\"\"); //remove dots and commas
                    value=value.replace(/^0*/,\"\"); //remove leading zeros
                    if (e.keyCode==8) {
                      tam = value.length;
                      if (tam==3) value=\"0.\" + value;
                      else if (tam==2) value=\"0.0\"+value;
                      else if (tam==1 || tam==0) value=\"0.000\";
                      else {                      
                        value=value.substring(0,tam-3) + \".\" + value.substring(tam-3);
                        tam = tam - 4;
                        for (i=tam;i>0;i--) {
                          if ((tam-i) > 1 && (tam-i)%3==2)
                            value=value.substring(0,i) + \",\" + value.substring(i);
                        }
                      }
                    } else {
                      kc=e.keyCode?e.keyCode:e.which;
                      if(String.fromCharCode(kc)==\" \") return false;
                      if(String.fromCharCode(kc)!=0 && !parseInt(String.fromCharCode(kc)))
                        return false;
                      value = value.replace(/[\.,]/g,\"\"); //remove dots and commas
                      value=value.replace(/^0*/,\"\"); //remove leading zeros
                      tam = value.length;
                      if (tam==0) value=\"0.0\";
                      else if (tam==1) value=\"0.\" + value;
                      else {
                        value=value.substring(0,tam-1) + \".\" + value.substring(tam-1);
                        tam = tam - 2;
                        for (i=tam;i>0;i--) {
                          if ((tam-i) > 1 && (tam-i)%3==2)
                            value=value.substring(0,i) + \",\" + value.substring(i);
                        }
                      }
                    }
                   ";
				break;
			case MASK_IP:
				$msCode  = "
      				e=event;
        			if(e.keyCode==e.which || e.which==0) return true;
        			kc=e.keyCode?e.keyCode:e.which;
        			if(String.fromCharCode(kc)==\" \") return false;
        			if(String.fromCharCode(kc)!=0){
        				if(!(parseInt(String.fromCharCode(kc))) && String.fromCharCode(kc) != \".\") {
			         	 	return false;
        				}
        			}
        			value=gebi(\"$psElem\").value;
        			values = value.split(\".\");
        			if (String.fromCharCode(kc) == \".\" && values.length > 3) return false;
        			var string = value + String.fromCharCode(kc);
        			if (values.length < 4 && values[values.length -1].length == 3 && String.fromCharCode(kc) != \".\"){
         				gebi(\"$psElem\").value += \".\";
        			}
        			return true;
        			";
				break;
			default:
				$msCode  = "alert(\"Oops... Invalid mask type.\");";
				break;
		}
		$msOut ="$msCode";
		return $msOut;
	}
}
?>