<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

define("MASK_NONE", 0);
define("MASK_DD_MM_YYYY_1", 1);
define("MASK_MM_DD_YYYY", 2);
define("MASK_DD_MM_YYYY_2", 3);
define("MASK_HH_MM_SS", 4);
define("MASK_HH_MM", 5);
define("MASK_INTEGER", 6);
define("MASK_FLOAT", 7);
define("MASK_DIGIT", 8);
define("MASK_LETTER", 9);
define("MASK_EMAIL", 10);
define("MASK_CEP", 11);
define("MASK_MONEY", 12);
define("MASK_IP", 13);

/**
 * Classe FWDMask. Implementa M�scaras para valida��o de entradas.
 *
 * <p>Classe que implementa m�scaras para valida��o de entradas (data,
 * hor�rio, valores, email, cep, etc).</p>
 * @package FWD5
 * @subpackage event
 */
class FWDMask {

	/**
	 * Constante que representa a m�scara
	 * @var integer
	 * @access protected
	 */
	protected $ciType;

	/**
	 * Evento javascript que dispara a valida��o da entrada (atrav�s da m�scara)
	 * @var string
	 * @access protected
	 */
	protected $csEvent = "OnBlur";

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDMask.</p>
	 * @access public
	 */
	public function __construct() {}

	/**
	 * Seta a m�scara.
	 *
	 * <p>Seta o atributo de m�scara (traduz a m�scara passada
	 * como par�metro de string para sua respectiva constante).</p>
	 * @access public
	 * @param string $psType M�scara
	 */
	public function setAttrType($psType) {
		$this->ciType = $this->translateStringToConstant($psType);
	}

	/**
	 * Seta o tipo da m�scara.
	 *
	 * <p>Seta o tipo da m�scara.</p>
	 * @access public
	 * @param integer $piType Constante de tipo de m�scara
	 */
	public function setType($piType){
		$this->ciType = $piType;
	}

	/**
	 * Retorna a m�scara.
	 *
	 * <p>Retorna a constante inteira referente a m�scara.</p>
	 * @access public
	 * @return integer M�scara
	 */
	public function getAttrType() {
		return $this->ciType;
	}

	/**
	 * Retorna o evento que valida a entrada utilizando a m�scara.
	 *
	 * <p>Retorna o evento que ir� disparar a valida��o da entrada, utilizando
	 * a m�scara.</p>
	 * @access public
	 * @return string Evento que realiza a valida��o da entrada
	 */
	public function getAttrEvent() {
		return $this->csEvent;
	}

	/**
	 * Retorna a express�o regular da m�scara.
	 *
	 * <p>Retorna a express�o regular da m�scara.</p>
	 * @access public
	 * @return string Express�o regular da m�scara
	 */
	public function getRegExp(){
		switch($this->getAttrType()){
			case MASK_DD_MM_YYYY_1:  // 31/12/2006
				return "/^(((3[01]|[0-2]\d)\/(0[13578]|10|12)|(30|[0-2]\d)\/(0[469]|11)|([01]\d|2[0-8])\/02)\/\d{4})|(29\/02\/((\d\d([2468][048]|0[48]|[13579][26]))|((([02468][048])|([13579][26]))00)))$/";
			case MASK_MM_DD_YYYY:  // 12/31/2006
				return "/^(((0[13578]|10|12)\/(3[01]|[0-2]\d)|(0[469]|11)\/(30|[0-2]\d)|02\/([01]\d|2[0-8]))\/\d{4})|(02\/29\/((\d\d([2468][048]|0[48]|[13579][26]))|((([02468][048])|([13579][26]))00)))$/";
			case MASK_DD_MM_YYYY_2:  // 31-Dec-2006
				return "/^([0-3]\d)\/([a-zA-Z]{3})\/(\d{4})$/";
			case MASK_HH_MM_SS :  // 23:59:59
				return  "/^([01]\d|2[0-3]):([0-5]\d):([0-5]\d)$/";
			case MASK_HH_MM:    // 23:59
				return  "/^([01]\d|2[0-3]):([0-5]\d)$/";
			case MASK_INTEGER:    // ...-2,-1,0,1,2...
				return  "/^(-{0,1}\d+)$/";
			case MASK_FLOAT:    // 0.3, -12.34, 1234.67, ...
				return "/^-?\d+(\.\d+)?$/";
			case MASK_DIGIT:    // 0,1,2,3,4...
				return  "/^(\d+)$/";
			case MASK_LETTER:    // a,b,c,...,z A,B,C,...,Z
				return "/^([A-Za-z]+)$/";
			case MASK_EMAIL:    // fulano.ciclano_outro-nome@fula-no.mail.com
				return  "/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/";
			case MASK_CEP:      // 91482-010
				return  "/^(\d){5}-(\d){3}$/";
			case MASK_MONEY: //2,445,200.14 || 1,000 || 50 ...
				return "/^\d{1,3}(,\d{3})*(\.\d\d){0,1}$/";
			case MASK_IP: // 123.123.123.123
				return "/(([1]?\d\d)|(2[0-4]\d)|(25[0-5]))\.(([1]?\d\d)|(2[0-4]\d)|(25[0-5]))\.(([1]?\d\d)|(2[0-4]\d)|(25[0-5]))\.(([1]?\d\d)|(2[0-4]\d)|(25[0-5]))$/";
			default:
				return "";
		}
	}

	/**
	 * Realiza a valida��o da entrada atrav�s da m�scara.
	 *
	 * <p>Re�ne em uma vari�vel string, contendo c�digo HTML/javascript,
	 * todas as informa��es necess�rias para implementar a valida��o da
	 * entrada atrav�s da m�scara.</p>
	 * @access public
	 * @param string $psElem Id do elemento
	 * @return string String que ir� disparar a valida��o da entrada
	 */
	public function render($psElem) {
		$msMask = $this->getRegExp();

		$msOut  = "elem=gebi(\"$psElem\"); re=$msMask; ";

		switch($this->getAttrType()){
			case MASK_DD_MM_YYYY_1:  // 31/12/2006
				$msOut .= " if(!re.exec(elem.value))";
				$msOut .= " if (elem.value.length>7) {";
				$msOut .= "   var temp1 = elem.value.slice(0,2);";
				$msOut .= "   var temp2 = elem.value.slice(2,4);";
				$msOut .= "   var temp3 = elem.value.slice(4,8);";
				$msOut .= "   var temp = \"\".concat(temp1,\"/\",temp2,\"/\",temp3);";
				$msOut .= "   if(re.exec(temp))";
				$msOut .= "     elem.value = temp;";
				$msOut .= " }";
				break;
			case MASK_MM_DD_YYYY:  // 12/31/2006
				$msOut .= " if(!re.exec(elem.value))";
				$msOut .= " if (elem.value.length>7) {";
				$msOut .= "   var temp1 = elem.value.slice(0,2);";
				$msOut .= "   var temp2 = elem.value.slice(2,4);";
				$msOut .= "   var temp3 = elem.value.slice(4,8);";
				$msOut .= "   var temp = \"\".concat(temp1,\"/\",temp2,\"/\",temp3);";
				$msOut .= "   if(re.exec(temp))";
				$msOut .= "     elem.value = temp;";
				$msOut .= " }";
				break;
			case MASK_DD_MM_YYYY_2:  // 31-Dec-2006
				$msOut .= " if(!re.exec(elem.value))";
				$msOut .= " if (elem.value.length>8) {";
				$msOut .= "   var temp1 = elem.value.slice(0,2);";
				$msOut .= "   var temp2 = elem.value.slice(2,5);";
				$msOut .= "   var temp3 = elem.value.slice(5,9);";
				$msOut .= "   var temp = \"\".concat(temp1,\"-\",temp2,\"-\",temp3);";
				$msOut .= "   if(re.exec(temp))";
				$msOut .= "     elem.value = temp;";
				$msOut .= " }";
				break;
			case MASK_HH_MM_SS :  // 23:59:59
				$msOut .= " if(!re.exec(elem.value))";
				$msOut .= " if (elem.value.length>5) {";
				$msOut .= "   var temp1 = elem.value.slice(0,2);";
				$msOut .= "   var temp2 = elem.value.slice(2,4);";
				$msOut .= "   var temp3 = elem.value.slice(4,6);";
				$msOut .= "   var temp = \"\".concat(temp1,\":\",temp2,\":\",temp3);";
				$msOut .= "   if(re.exec(temp))";
				$msOut .= "     elem.value = temp;";
				$msOut .= " }";
				break;
			case MASK_HH_MM:    // 23:59
				$msOut .= " if(!re.exec(elem.value))";
				$msOut .= " if (elem.value.length>3) {";
				$msOut .= "   var temp1 = elem.value.slice(0,2);";
				$msOut .= "   var temp2 = elem.value.slice(2,4);";
				$msOut .= "   var temp = \"\".concat(temp1,\":\",temp2);";
				$msOut .= "   if(re.exec(temp))";
				$msOut .= "     elem.value = temp;";
				$msOut .= " }";
				break;
			case MASK_INTEGER:    // ...-2,-1,0,1,2...
				break;
			case MASK_FLOAT:    // 0.3, -12.34, 1234.67, ...
				break;
			case MASK_DIGIT:    // 0,1,2,3,4...
				break;
			case MASK_LETTER:    // a,b,c,...,z A,B,C,...,Z
				break;
			case MASK_EMAIL:    // fulano.ciclano_outro-nome@fula-no.mail.com
				break;
			case MASK_CEP:      // 91482-010
				$msOut .= " if(!re.exec(elem.value))";
				$msOut .= " if (elem.value.length>7) {";
				$msOut .= "   var temp1 = elem.value.slice(0,5);";
				$msOut .= "   var temp2 = elem.value.slice(5,8);";
				$msOut .= "   var temp = \"\".concat(temp1,\"-\",temp2);";
				$msOut .= "   if(re.exec(temp))";
				$msOut .= "     elem.value = temp;";
				$msOut .= " }";
				break;
			case MASK_MONEY:
				$msOut .= "if(!re.exec(elem.value)) {";
				$msOut .= "  value = elem.value;";
				$msOut .= "  value = value.toString().replace(\",\",\"\",\"g\");";
				$msOut .= "  if(isNaN(value))  value = \"0\";";
				$msOut .= "  value = Math.floor(value*100+0.50000000001);";
				$msOut .= "  cents = value%100;";
				$msOut .= "  value = Math.floor(value/100).toString();";
				$msOut .= "  if(cents<10)  cents = \"0\" + cents;";
				$msOut .= "  for (var i = 0; i < Math.floor((value.length-(1+i))/3); i++)";
				$msOut .= "    value = value.substring(0,value.length-(4*i+3))+\",\"+value.substring(value.length-(4*i+3));";
				$msOut .= "  value += \".\" + cents;";
				$msOut .= "}";
				break;
			case MASK_IP:
				break;
			default:
				$msOut .= "";
				break;
		}

		$msOut .= " if(!re.exec(elem.value)) {";
		$msOut .= "   elem.style.borderRightColor = \"#FF0000\";";
		$msOut .= "   elem.style.borderWidth = \"1px\";";
		$msOut .= " }";
		$msOut .= " else {";
		$msOut .= "   elem.style.borderRightColor = \"#CCCCCC\";";
		$msOut .= "   elem.style.borderWidth = \"1px\";";
		$msOut .= " }";
		$msOut .= " return;";
		return $msOut;
	}

	/**
	 * Traduz a m�scara de string para sua respectiva constante.
	 *
	 * <p>Traduz a string referente a m�scara passada como par�metro
	 * em sua respectiva constante inteira.</p>
	 * @access public
	 * @param string $psString M�scara (string)
	 * @return integer M�scara (integer)
	 */
	public function translateStringToConstant($psString) {
		switch($psString){
			case "dd/mm/yyyy":
				return MASK_DD_MM_YYYY_1;
			case "mm/dd/yyyy":
				return MASK_MM_DD_YYYY;
			case "dd/MM/yyyy":
				return MASK_DD_MM_YYYY_2;
			case "hh:mm:ss":
				return MASK_HH_MM_SS;
			case "hh:mm":
				return MASK_HH_MM;
			case "integer":
				return MASK_INTEGER;
			case "float":
				return MASK_FLOAT;
			case "digit":
				return MASK_DIGIT;
			case "letter":
				return MASK_LETTER;
			case "email":
				return MASK_EMAIL;
			case "cep":
				return MASK_CEP;
			case "money":
				return MASK_MONEY;
			case "ip":
				return MASK_IP;
			default:
				return MASK_NONE;
		}
	}
}
?>