<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDEvent. Implementa eventos.
 *
 * <p>Classe que implementa eventos dentro da Framework FWD5.</p>
 * @package FWD5
 * @subpackage event
 * @abstract
 */
abstract class FWDEvent extends FWDRunnable {

	/**
	 * Nome do evento
	 * @var string
	 * @access protected
	 */
	protected $csName = "";

	/**
	 * Fonte do evento
	 * @var string
	 * @access protected
	 */
	protected $csSource = "this";

	/**
	 * Alvo do evento
	 * @var string
	 * @access protected
	 */
	protected $csEvent = "";

	/**
	 * Fun��o do evento (pode ser uma constante num�rica ou uma string)
	 * @var integer
	 * @access protected
	 */
	protected $csFunction = "";

	/**
	 * Elementos sobre os quais o evento atuar�
	 * @var string
	 * @access protected
	 */
	protected $csElements = "";

	/**
	 * Par�metros necess�rios a alguns eventos
	 * @var string
	 * @access protected
	 */
	protected $csParameters = "";

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDEvent. Seta os atributos de alvo,
	 * fun��o, elementos e par�metros do evento.</p>
	 *
	 * @access public
	 *
	 * @param string $psEvent Alvo do evento.
	 * @param string $psFunction Fun��o do evento.
	 * @param string $psElements Elemento sobre o qual o evento atuar�.
	 * @param string $psParameters Par�metros necess�rios a alguns eventos.
	 */
	public function __construct($psEvent,$psFunction,$psElements,$psParameters) {
		$this->csEvent = $psEvent;
		$this->csFunction = $this->translateStringToConstant($psFunction);
		$this->csElements = $psElements;
		$this->csParameters = $psParameters;
		parent::__construct($psFunction);
	}

	/**
	 * Renderiza evento.
	 *
	 * <p>M�todo abstrato para renderiza��o do evento (Deve ser implementado
	 * nas classes que estenderem a FWDEvent).</p>
	 * @access public
	 * @return string Evento renderizado
	 * @abstract
	 */
	abstract public function render();

	/**
	 * Cria um objeto de evento javascript n�o definido pelo usu�rio.
	 *
	 * <p>Cria um objeto de evento, caso o evento n�o seja um evento criado pelo
	 * usu�rio (i.e, caso o evento seja um evento javascript "num�rico").
	 * OBS.: O Client Event ja possui cria tal objeto. Deve existir um m�todo
	 * run() que n�o fa�a nada, apenas sobreescreva este na classe FWDClientEvent.</p>
	 * @access public
	 */
	public function run() {
		$js = new FWDJsEvent($this->getAttrFunction(),$this->getAttrElements(),$this->getAttrParameters());
		echo $js->render();
	}

	/**
	 * Seta o nome do evento.
	 *
	 * <p>Seta o nome do evento.</p>
	 * @access public
	 * @param string $psName Nome do evento
	 */
	public function setAttrName($psName) {
		$this->csName = $psName;
		$this->setAttrIdentifier($psName);
	}

	/**
	 * Seta a fonte do evento.
	 *
	 * <p>Seta a fonte do evento.</p>
	 * @access public
	 * @param string $psSource Fonte do evento
	 */
	public function setAttrSource($psSource) {
		$this->csSource = $psSource;
	}

	/**
	 * Seta o alvo do evento.
	 *
	 * <p>Seta o alvo do evento.</p>
	 * @access public
	 * @param string $psEvent Alvo do evento
	 */
	public function setAttrEvent($psEvent) {
		$this->csEvent = $psEvent;
	}

	/**
	 * Seta a fun��o do evento.
	 *
	 * <p>Seta o atributo de fun��o do evento (traduz a fun��o passada
	 * como par�metro de string para sua respectiva constante). Seta o
	 * identificador do evento do usu�rio.</p>
	 * @access public
	 * @param string $psFunction Fun��o do evento
	 */
	public function setAttrFunction($psFunction) {
		$this->csFunction = $this->translateStringToConstant($psFunction);
	}

	/**
	 * Seta os elementos sobre os quais o evento atuar�.
	 *
	 * <p>Seta os elementos sobre os quais o evento atuar�.</p>
	 * @access public
	 * @param string $psElements Elementos sobre os quais o evento atuar�
	 */
	public function setAttrElements($psElements) {
		$this->csElements = $psElements;
	}

	/**
	 * Seta os par�metros necess�rios a alguns eventos.
	 *
	 * <p>Seta os par�metros necess�rios a alguns eventos.</p>
	 * @access public
	 * @param string $psParameters Par�metros necess�rios a alguns eventos
	 */
	public function setAttrParameters($psParameters) {
		$this->csParameters = $psParameters;
	}

	/**
	 * Retorna o nome do evento.
	 *
	 * <p>Retorna o nome do evento.</p>
	 * @access public
	 * @return string Nome do evento
	 */
	public function getAttrName() {
		$FWDXmlLoad = FWDXmlLoad::getInstance();
		$msReturn = "";
		if($this->csName)
		$msReturn = $this->csName;
		else
		$msReturn = $this->csFunction;
		return $msReturn;
	}

	/**
	 * Retorna a fonte do evento.
	 *
	 * <p>Retorna a fonte do evento.</p>
	 * @access public
	 * @return string Fonte do evento
	 */
	public function getAttrSource() {
		return $this->csSource;
	}

	/**
	 * Retorna o alvo do evento.
	 *
	 * <p>Retorna o alvo do evento.</p>
	 * @access public
	 * @return string Alvo do evento
	 */
	public function getAttrEvent() {
		return $this->csEvent;
	}

	/**
	 * Retorna a fun��o do evento.
	 *
	 * <p>Retorna a fun��o do evento.</p>
	 * @access public
	 * @return string Fun��o do evento (pode ser uma constante num�rica)
	 */
	public function getAttrFunction() {
		return $this->csFunction;
	}

	/**
	 * Retorna os elementos do evento.
	 *
	 * <p>Retorna os elementos do evento.</p>
	 * @access public
	 * @return string Elementos do evento
	 */
	public function getAttrElements() {
		return $this->csElements;
	}

	/**
	 * Retorna os par�metros do evento
	 *
	 * <p>Retorna os par�metros do evento.</p>
	 * @access public
	 * @return string Par�metros necess�rios a alguns eventos
	 */
	public function getAttrParameters() {
		return $this->csParameters;
	}

	/**
	 * Traduz a fun��o do evento de string para sua respectiva constante.
	 *
	 * <p>Traduz a string referente a fun��o do evento passada como par�metro
	 * em sua respectiva constante. **Caso a string passada como par�metro n�o
	 * se encaixe em nenhuma op��o do switch, a pr�pria string � retornada.**</p>
	 * @access public
	 * @param string $psString Fun��o do evento (string)
	 * @return integer Fun��o do evento (pode ser uma string)
	 */
	public function translateStringToConstant($psString) {
		$msValue = strtolower($psString);
		switch($msValue){
			case "erasefield":
				return JS_ERASE;
			case "colorbackground":
				return JS_COLOR_BACKGROUND;
			case "colorfont":
				return JS_COLOR_FONT;
			case "checkvalue":
				return JS_CHECK_VALUE;
			case "resize":
				return JS_RESIZE;
			case "setcontent":
				return JS_SET_CONTENT;
			case "show":
				return JS_SHOW;
			case "hide":
				return JS_HIDE;
			case "showmessage":
				return JS_SHOW;
			case "hidemessage":
				return JS_SHOW;
			case "createelem":
				return JS_CREATE_ELEM;
			case "exampletemp":
				return JS_EXAMPLE_TEMP;
			case "showmenu":
				return JS_SHOW_MENU;
			case "hidemenu":
				return JS_HIDE_MENU;
			case "reposition":
				return JS_REPOSITION;
			case "changecursor":
				return JS_CHANGE_CURSOR;
			case "movedor":
				return JS_MOVEDOR;
			case "changetab":
				return JS_CHANGE_TAB;
			case "pageload":
				return JS_PAGE_LOAD;
			case "showloading":
				return JS_SHOW_LOADING;
				// tempor�rio
			case "resizecol":
				return JS_RESIZE_COL;
			case "activatetab":
				return JS_ACTIVATE_TAB;
			case "open":
				return JS_OPEN;
			case "tooltip":
				return JS_TOOLTIP;
			case "submit":
				return JS_SUBMIT;
			case "inputhiddengrid":
				return JS_INPUT_HIDDEN_VALUE;
			case "menuactiontarget":
				return JS_MENU_ACTION_TARGET;
			case "changeclass":
				return JS_CHANGE_CLASS;
			case "inverttabitemclass":
				return JS_INVERT_TABITEM_CLASS;
			case "gridcorrectselectedrows":
				return JS_GRID_CORRECT_SELECTED_ROWS;
			case "elementincontroller":
				return JS_ELEMENT_IN_CONTROLLER;
			case "haspopupblocker":
				return JS_HAS_POPUP_BLOCKER;
			case "clearselect":
				return JS_CLEAR_SELECT;
			case "populateselect":
				return JS_POPULATE_SELECT;
			case "haschanged":
				return JS_HAS_CHANGED;
			case "replace":
				return JS_REPLACE;
			case "download":
				return JS_DOWNLOAD;
			default:
				return $psString;
		}
	}
}
?>