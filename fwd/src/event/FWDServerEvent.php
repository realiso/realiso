<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDServerEvent. Implementa Eventos Servidor.
 *
 * <p>Classe que implementa Eventos Servidor na Framework FWD5.</p>
 * @package FWD5
 * @subpackage event
 */
class FWDServerEvent extends FWDAjaxEvent {

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDServerEvent.</p>
	 * @access public
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Seta a Fun��o do Evento.
	 *
	 * <p>Seta o atributo de Fun��o do Evento Servidor.</p>
	 * @access public
	 * @param string $psFunction Fun��o do Evento Servidor
	 */
	public function setAttrFunction($psFunction) {
		$this->csFunction = $psFunction;
		$this->setAttrName($psFunction);
	}
}
?>