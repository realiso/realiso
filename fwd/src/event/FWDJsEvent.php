<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

define("JS_NONE", 0);
define("JS_ERASE", 1);
define("JS_COLOR_BACKGROUND", 2);
define("JS_COLOR_FONT", 3);
define("JS_CHECK_VALUE", 4);
define("JS_RESIZE", 5);
define("JS_SET_CONTENT", 6);
define("JS_SHOW", 7);
define("JS_HIDE", 8);
define("JS_SHOW_MENU", 9);
define("JS_HIDE_MENU", 10);
define("JS_CREATE_ELEM", 11);	// server only
define("JS_EXAMPLE_TEMP", 12);
define("JS_REPOSITION", 13);
define("JS_CHANGE_CURSOR", 14);
define("JS_MOVEDOR", 15);
define("JS_CHANGE_TAB", 16);
define("JS_PAGE_LOAD", 17);
define("JS_SHOW_LOADING", 18);
define("JS_RESIZE_COL", 19);
define("JS_ACTIVATE_TAB", 21);
define("JS_SAVE_USER_PREFS", 22);
define("JS_POPULATE_GRID",24);
define("JS_GRID_EVENT",25);
define("JS_GRID_CHANGEPAGENEXT",26);
define("JS_GRID_CHANGEPAGEFIRST",27);
define("JS_GRID_CHANGEPAGEBACK",28);
define("JS_GRID_CHANGEPAGELAST",29);
define("JS_GRID_REFRESH",30);
define("JS_OPEN",31);
define("JS_TOOLTIP",32);
define("JS_CHANGE_CLASS",33);
define("JS_INVERT_TABITEM_CLASS",34);
define("JS_GRID_CORRECT_SELECTED_ROWS",35);
define("JS_ELEMENT_IN_CONTROLLER",36);
define("JS_HAS_CHANGED",37);
define("JS_SUBMIT",100);
define("JS_INPUT_HIDDEN_VALUE",150);
define("JS_REQUIRED_CHECK_AJAX",151);
define("JS_REQUIRED_CHECK_CLIENT",152);
define("JS_REQUIRED_CHECK_SERVER",153);
define("JS_MENU_ACTION_TARGET",154);
define("JS_HAS_POPUP_BLOCKER",155);
define("JS_CLEAR_SELECT",156);
define("JS_POPULATE_SELECT",157);
define("JS_REPLACE",158);
define("JS_DOWNLOAD",159);

/**
 * Classe FWDJsEvent. Implementa Eventos Javascript.
 *
 * <p>Classe que implementa Eventos Javascript na Framework FWD5.</p>
 * @package FWD5
 * @subpackage event
 */
class FWDJsEvent {

	/**
	 * Constante que representa o Evento Javascript
	 * @var integer
	 * @access protected
	 */
	protected $ciEvent;

	/**
	 * Elementos sobre os quais o evento atuar�
	 * @var string
	 * @access protected
	 */
	protected $csElements;

	/**
	 * Par�metros do evento
	 * @var string
	 * @access protected
	 */
	protected $csParameters;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDJsEvent. Seta o evento, os elementos
	 * sobre os quais o evento atuar� e os par�metros do evento.</p>
	 * @access public
	 * @param integer $piEvent Evento
	 * @param string $psElems Elementos sobre os quais o evento atuar�
	 * @param string $psAttr Par�metros do evento
	 */
	public function __construct($piEvent,$psElems,$psAttr){
		$this->ciEvent = $piEvent;
		$this->csElements = $psElems;
		$this->csParameters = $psAttr;
	}

	/**
	 * Renderiza o evento Javascript.
	 *
	 * <p>Renderiza o evento Javascript. Re�ne em uma vari�vel string
	 * todas as informa��es necess�rias para implementar o evento Javascript.</p>
	 * @access public
	 * @return string String que ir� disparar o evento Javascript
	 */
	public function render() {
		$msCode = "";
		$maElems = explode(" ",$this->csElements);
		$maAttr = explode(" ",$this->csParameters);
		switch($this->ciEvent){
			case JS_ERASE:
				foreach($maElems as $id)
				$msCode .= "js_erase(\"$id\");";
				break;
			case JS_COLOR_BACKGROUND:
				foreach($maElems as $key => $id)
				$msCode .= "js_color_background(\"$id\", \"$maAttr[$key]\");";
				break;
			case JS_COLOR_FONT:
				foreach($maElems as $key => $id)
				$msCode .= "js_color_font(\"$id\", \"$maAttr[$key]\");";
				break;
			case JS_CHECK_VALUE:
				foreach($maElems as $key => $id)
				$msCode .= "js_check_value(\"$id\",\"$maAttr[$key]\");";
				break;
			case JS_RESIZE:
				$msCode .= "js_resize(\"$maElems[0]\",\"$maAttr[0]\",\"$maAttr[1]\");";
				break;
			case JS_SET_CONTENT:
				foreach($maElems as $key => $id)
				$msCode .= "js_set_content(\"$id\",\"$this->csParameters\");";
				break;
			case JS_SHOW_MENU:
				$msCode .= "js_show_menu(\"$maElems[0]\",event);";
				break;
			case JS_HIDE_MENU:
				$msCode .= "js_hide_menu(\"$maElems[0]\",event);";
				break;
			case JS_SHOW:
				$msCode .= "js_show(\"$maElems[0]\");";
				break;
			case JS_HIDE:
				$msCode .= "js_hide(\"$maElems[0]\");";
				break;
			case JS_CREATE_ELEM:
				$msCode .= "js_create_elem(\"$maElems[0]\", \"".$this->csParameters."\");";
				break;
			case JS_EXAMPLE_TEMP:
				$msCode .= "js_temp($maAttr[0]);";
				break;
			case JS_CHANGE_CURSOR:
				$msCode .= "js_change_cursor(\"$maElems[0]\",\"$maAttr[0]\");";
				break;
			case JS_REPOSITION:
				$msCode .= "js_reposition(\"$maElems[0]\",\"$maAttr[0]\",\"$maAttr[1]\");";
				break;
			case JS_MOVEDOR:
				$msCode .= "dragStart(event,\"$maElems[0]\");";
				break;
			case JS_CHANGE_TAB:
				$msCode .= "soTabSubManager.changeTab(\"$maAttr[0]\",\"$maAttr[1]\");";
				break;
			case JS_INVERT_TABITEM_CLASS:
				$msCode .= "js_invert_tabitem_class(\"$maElems[0]\");";
				break;
			case JS_PAGE_LOAD:
				$msCode .= "js_page_load(\"$maElems[0]\",\"$maElems[1]\",\"$maAttr[0]\",\"$maAttr[1]\");";
				break;
			case JS_SHOW_LOADING:
				$msCode .= "js_show_loading(\"$maElems[0]\",\"$maElems[1]\");";
				break;
			case JS_RESIZE_COL:
				$msCode .= "js_resize_col(\"$maElems[0]\",\"$maElems[1]\",\"$maAttr[0]\",\"$maAttr[1]\",\"$maAttr[2]\");";
				break;
			case JS_POPULATE_GRID:
				$msCode .= "obj{$this->csElements}.js_populate_grid(\"{$this->csParameters}\");";
				break;
			case JS_GRID_EVENT:
				$msCode .= "obj".$this->csElements.".js_grid_event(\"".$this->csParameters."\");";
				break;
			case JS_GRID_CHANGEPAGENEXT:
				$msCode .= "obj{$this->csElements}.js_populate_grid_changepage(\"{$this->csParameters}\");";
				break;
			case JS_GRID_CHANGEPAGEBACK:
				$msCode .= "obj{$this->csElements}.js_populate_grid_changepage(\"{$this->csParameters}\");";
				break;
			case JS_GRID_CHANGEPAGEFIRST:
				$msCode .= "obj{$this->csElements}.js_populate_grid_changepage(\"{$this->csParameters}\");";
				break;
			case JS_GRID_CHANGEPAGELAST:
				$msCode .= "obj{$this->csElements}.js_populate_grid_changepage(\"{$this->csParameters}\");";
				break;
			case JS_GRID_REFRESH:
				$msCode .= "obj{$this->csElements}.js_populate_grid_changepage(\"{$this->csParameters}\");";
				break;
			case JS_ACTIVATE_TAB:
				$msCode .= "js_activate_tab(\"{$this->csElements}\",\"{$this->csParameters}\");";
				break;
			case JS_SAVE_USER_PREFS:
				$msCode.= "js_save_user_prefs(\"$maElems[0]\",\"$maAttr[0]\");";
				break;
			case JS_OPEN:
				$msCode.= "js_open(\"$maAttr[0]\",\"$maAttr[1]\",\"$maAttr[2]\",\"$maAttr[3]\",\"$maAttr[4]\");";
				break;
			case JS_TOOLTIP:
				$msCode.= "{$this->csElements} return escape(\"{$this->csParameters}\");";
				break;
			case JS_SUBMIT:
				if(isset($maAttr[1])){
					$msCode.= "js_submit(\"$maAttr[0]\",\"$maAttr[1]\");";
				}elseif(isset($maAttr[0])){
					$msCode.= "js_submit(\"$maAttr[0]\");";
				}else{
					$msCode.= "js_submit();";
				}
				break;
			case JS_MENU_ACTION_TARGET:
				$msCode.= "js_show_menu_action_target(\"$maElems[0]\");";
				break;
			case JS_INPUT_HIDDEN_VALUE:
				$msCode.= " js_show_grid_input_value(\"$maElems[0]\");";
				break;
			case JS_REQUIRED_CHECK_AJAX:
				$msCode.= "js_required_check_ajax(\"{$this->csElements}\",\"$this->csParameters\");";
				break;
			case JS_REQUIRED_CHECK_CLIENT:
				$msCode.= "js_required_check_client(\"{$this->csElements}\",\"$this->csParameters\");";
				break;
			case JS_REQUIRED_CHECK_SERVER:
				$msCode.= "js_required_check_server(\"{$this->csElements}\",\"$this->csParameters\");";
				break;
			case JS_CHANGE_CLASS:
				$msCode .= "js_change_class(\"$maElems[0]\",\"$maAttr[0]\");";
				break;
			case JS_GRID_CORRECT_SELECTED_ROWS:
				$msCode .= "js_grid_correct_selected_rows(\"$maElems[0]\")";
				break;
			case JS_ELEMENT_IN_CONTROLLER:
				$msCode .= "js_element_in_controller(\"$maElems[0]\",\"$maElems[1]\")";
				break;
			case JS_HAS_POPUP_BLOCKER:
				$msCode .= "js_has_popup_blocker()";
				break;
			case JS_CLEAR_SELECT:
				$msCode .= "js_clear_select(\"{$this->csElements}\")";
				break;
			case JS_POPULATE_SELECT:
				$msCode .= "js_populate_select(\"{$this->csElements}\",\"$this->csParameters\")";
				break;
			case JS_HAS_CHANGED:
				$msCode .= "if(!js_has_changed(\"$maElems[0]\")) return false;";
				break;
			case JS_REPLACE:
				$msCode .= "js_replace(\"{$this->csElements}\",\"$maAttr[0]\",\"$maAttr[1]\")";
				break;
			case JS_DOWNLOAD:
				$msCode.= "location=\"$maAttr[0]\";";
				break;
		}
		return $msCode;
	}
}
?>