<?php
$gaStrings = array(

/* './abstracts/FWDGridAbstract.php' */

'fwdgrid_pref_column_label'=>'Colonne:',
'fwdgrid_pref_column_name'=>'Colonne',
'fwdgrid_pref_column_order'=>'Commande:',
'fwdgrid_pref_column_order_title'=>'Ordre des Colonnes de la Grille',
'fwdgrid_pref_column_properties'=>'Propriétés de Colonne',
'fwdgrid_pref_column_show_title'=>'Colonnes Affichées et Ordre d\'Affichage entre elles',
'fwdgrid_pref_column_width'=>'Largeur*:',
'fwdgrid_pref_columns_grid'=>'Colonnes de la grille:',
'fwdgrid_pref_columns_show'=>'Colonnes Affichées:',
'fwdgrid_pref_false_value'=>'Non',
'fwdgrid_pref_grid_columns_title'=>'Colonnes de la Grille:',
'fwdgrid_pref_grid_properties'=>'Modifier les Propriétés de la Grille',
'fwdgrid_pref_rows_height'=>'Hauteur de Lignes:',
'fwdgrid_pref_rows_per_page'=>'Lignes par page:',
'fwdgrid_pref_select_source'=>'Sélection de la Source',
'fwdgrid_pref_select_target'=>'Sélectionner la Cible',
'fwdgrid_pref_target'=>'Cible:',
'fwdgrid_pref_tooltip_label'=>'Info Bulle:',
'fwdgrid_pref_true_value'=>'Oui',
'fwdgrid_pref_width_obs'=>'* \'0\' = largeur automatique',

/* './base/FWDDefaultErrorHandler.php' */

'error_config_corrupt'=>'Le fichier de configuration est corrompu.',
'error_configuration'=>'Le fichier de configuration est corrompu.',
'error_db_connection'=>'Erreur de connexion à la Base de Données.',
'error_db_invalid_object_name'=>'Invalide nom d\'objet de base de données. Probablement',
'error_debug_permission_denied'=>'Autorisation refusée dans le fichier de débogage.',
'error_session_permission_denied'=>'Autorisation refusée dans le fichier de session.',
'error_smtp'=>'Échec de la connexion au serveur mail.',

/* './base/FWDLanguage.php' */

'default_language'=>'Portugais',

/* './base/FWDLicense.php' */

'error_corrupt_hash'=>'Le Hash est corrompu.',
'error_corrupt_license'=>'La licence est endommagée.',
'error_permission_denied'=>'Autorisation refusée dans le fichier d\'activation.',

/* './base/FWDSyslog.php' */

'error_connection_syslog'=>'Impossible de se connecter au serveur syslog.',

/* './event/classEvent/FWDGridUserEdit.php' */

'fwdGridUserEditErrorWidthGrid'=>'Des incohérences ont été trouvées: La somme de la largeur des colonnes de la grille doit être inférieure ou égale à:',
'fwdGridUserEditErrorWidthGridExplain'=>'toutefois',

/* './formats/FWDPDF.php' */

'fpdf_page'=>'Page',

/* './report/FWDReportClassificator.php' */

'report_confidencial'=>'Rapport Confidentiel',
'report_institutional'=>'Rapport Institutionnel',
'report_public'=>'Rapport Public',

/* './report/FWDReportGenerator.php' */

'report_confidential_report'=>'Rapport Confidentiel',
'report_excel'=>'Excel',
'report_html'=>'HTML',
'report_html_download'=>'HTML Télécharger',
'report_institutional_report'=>'Rapport Institutionnel',
'report_pdf'=>'PDF',
'report_public_report'=>'Rapport Public',
'report_word'=>'Word',

/* './view/FWDCalendar.php' */

'locale'=>'fr',

/* './view/FWDGrid.php' */

'fwdgrid_info_no_result_rows'=>'Pas de résultats ont été trouvés',
'fwdgrid_number_page_of'=>'de',
'fwdgrid_page_label'=>'page',

/* './view/FWDTreeAjax.php' */

'FWDLoading'=>'Chargement',
);
?>