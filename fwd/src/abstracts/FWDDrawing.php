<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe abstrata FWDDrawing.
 *
 * <p>Classe abstrata para objetos que possam ser desenhados em tela.</p>
 * @package FWD5
 * @subpackage abstracts
 */
abstract class FWDDrawing {

	/**
	 * Define se o elemento deve ser desenhado ou n�o
	 * @var boolean
	 * @access protected
	 */
	protected $cbShouldDraw = true;

	/**
	 * Seta o valor do atributo ShouldDraw.
	 *
	 * <p>Seta o valor do atributo ShouldDraw.</p>
	 * @access public
	 * @param boolean $pbShouldDraw Valor a ser atribu�do
	 */
	public function setShouldDraw($pbShouldDraw) {
		$this->cbShouldDraw = $pbShouldDraw;
	}

	/**
	 * Retorna o valor booleano do atributo ShouldDraw.
	 *
	 * <p>Retorna o valor booleano do atributo ShouldDraw.</p>
	 * @access public
	 * @return boolean Valor booleano do atributo ShouldDraw
	 */
	public function getShouldDraw() {
		return $this->cbShouldDraw;
	}

	/**
	 * Desenha o cabe�alho do objeto.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o cabe�alho do objeto.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do objeto
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	protected function drawHeader($poBox = null) {}

	/**
	 * Desenha o corpo do objeto.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o corpo do objeto.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do objeto
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	protected function drawBody($poBox = null) {}

	/**
	 * Desenha o rodap� do objeto.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o rodap� do objeto.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do objeto
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	protected function drawFooter($poBox = null) {}

	/**
	 * Desenha em tela.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML/javascript,
	 * todas as informa��es necess�rias para exibir em tela o objeto.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do objeto
	 * @return string C�digo HTML/javascript que ser� inserido na p�gina
	 */
	public function draw($poBox = null) {
		$msOut = "";
		if ($this->cbShouldDraw) {
			$msOut .= $this->drawHeader($poBox);
			$msOut .= $this->drawBody($poBox);
			$msOut .= $this->drawFooter($poBox);
		}
		return $msOut;
	}
}
?>
