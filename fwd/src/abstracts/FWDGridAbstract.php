<?
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These codedf instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

define( "GRID_SEL_NONE", 0);
define( "GRID_SEL_SINGLE", 1);
define( "GRID_SEL_MULTIPLE", 2);

/**
 * Classe abstrata FWDGrid. Classe abstrata com os atributos b�sicos da Grid.
 *
 * <p>Classe abstrata que cont�m os atributos b�sicos da Grid.</p>
 * @abstract
 * @package FWD5
 * @subpackage abstracts
 */

class FWDGridAbstract extends FWDViewGrid
{

	protected $csCSSClass = '';
	/**
	 * Array de FWDColumn que cont�m os objetos das colunas da grid
	 * @var array
	 * @access protected
	 */
	public $caColumns = array();

	/**
	 * Array de linhas (que s�o arrays) que cont�m os arrays de cada uma das linhas da grid.
	 * @var array
	 * @access protected
	 */
	protected $caRows = array();

	/**
	 * Array de FWDViewGroup contem os objetos de cada uma das linhas da grid
	 * @var array
	 * @access protected
	 */
	protected $caRowsObj = array();

	/**
	 * Armazena o numero total de linhas da grid
	 * @var integer
	 * @access protected
	 */
	protected $ciRowsCount = 0;

	/**
	 * Armazena o numero total de linhas da grid
	 * @var integer
	 * @access protected
	 */
	protected $ciColumnsCount = 0;

	/**
	 * Armazena a largura da grid
	 * @var integer
	 * @access protected
	 */
	protected $ciGridWidth = 0;

	/**
	 * Array de FWDMenu que cont�m os menus de contextos definidos dentro da grid.
	 * @var array
	 * @access protected
	 */
	protected $caMenus = array();

	/**
	 * Array de FWDMenu que s�o criados como modifica��o de um menu "esqueleto" definido no xml atravez das permi��es
	 * @var array
	 * @access protected
	 */
	protected $caMenusACL = array();

	/**
	 * Armazena o tipo de cele��o de linhas da grid
	 * @var integer
	 * @access protected
	 */
	protected $ciSelectType = GRID_SEL_NONE;

	/**
	 * Armazena as colunas q devem ser usadas como ID da linhas da grid em consultas
	 * @var string
	 * @access protected
	 */
	protected $csSelectColumn = 0;

	/**
	 * Objeto respons�vel por desenhar cada uma das c�lulas da grid.
	 * @var FWDDrawGrid
	 * @access protected
	 */
	protected $coDrawGrid;

	/**
	 * Armazena a p�gina atual da grid
	 * @var integer
	 * @access protected
	 */
	protected $ciCurrentPage=1;

	/**
	 * Armazena o n�mero total de p�ginas da grid
	 * @var integer
	 * @access protected
	 */
	protected $ciLastPage=1;

	/**
	 * Armazena o n�mero de linhas por p�gina da grid
	 * @var integer
	 * @access protected
	 */
	protected $ciRowsPerPage = 50;

	/**
	 * Define o tipo de preenchimento da grid. se � por Ajax(true) ou preencimento est�tico(false)
	 * @var boolean
	 * @access protected
	 */
	protected $cbDinamicFill = false;

	/**
	 * Define A altura de cada uma das linhas da grid.
	 * @var integer
	 * @access protected
	 */
	protected $ciRowHeight =22;

	/**
	 * Define a largura da barra de rolagem utilizada pela grid.
	 * @var integer
	 * @access protected
	 */
	protected $ciRollerBarWidth=16;
	/**
	 * Array Inteiros (0 ou 1) que armazenam se cada coluna deve ser exibida ou n�o
	 * @var array
	 * @access protected
	 */
	protected	$caColumnDisplay = array();

	/**
	 * Array Inteiros (0 ou 1) que armazenam se os eventos devem ou n�o ser renderizados em cada uma das colunas
	 * @var array
	 * @access protected
	 */
	protected	$caEventBlock = array();

	/**
	 * Array Inteiros (0 ou 1) que armazenam se cada coluna deve possuir tooltip ou n�o
	 * @var array
	 * @access protected
	 */
	protected	$caToolTip = array();


	/**
	 * Array Source de tooltip das colunas da grid
	 * @var array
	 * @access protected
	 */
	protected $caToolTipSource = array();
	/**
	 * Define se a grid deve ser populada ou n�o
	 * @var boolean
	 * @access protected
	 */
	protected $cbPopulate = true;

	/**
	 * Define se a grid deve ser populada via xml
	 * @var boolean
	 * @access protected
	 */
	protected $cbPopulateXML = true;

	/**
	 * Define A altura da barra de titulos da grid.
	 * @var integer
	 * @access protected
	 */
	protected $ciTitleHeight = 22;

	/**
	 * Define A altura da barra dos headers das colunas da grid
	 * @var integer
	 * @access protected
	 */
	protected $ciColumnHeight= 22 ;

	/**
	 * Objeto respons�vel por conter o t�tulo da grid e o botao de refresh da grid.
	 * @var FWDViewGroup
	 * @access protected
	 */
	protected $coGridTitle = null;

	/**
	 * @var integer Define A altura do bot�o de refresh da grid.
	 * @access protected
	 */
	protected $ciButtonRefreshHeight = 16;

	/**
	 * Define A largura da barra de bot�es de navega��o da grid.
	 * @var integer
	 * @access protected
	 */
	protected $ciButtonRefreshWidth = 13;

	/**
	 * Define a altura do bot�o de exporta��o para xls da grid.
	 * @var integer
	 * @access protected
	 */
	protected $ciButtonExcelHeight = 16;

	/**
	 * Define a largura do bot�o de exporta��o para xls da grid.
	 * @var integer
	 * @access protected
	 */
	protected $ciButtonExcelWidth = 16;

	/**
	 * Define A largura dos bot�es de navega��o da grid.
	 * @var integer
	 * @access protected
	 */
	protected $ciButtonChangeWidth = 12;


	/**
	 * Define A altura dos bot�es de navega��o da grid.
	 * @var integer
	 * @access protected
	 */
	protected $ciButtonChangeHeight = 12;

	/**
	 * Define A altura do bot�o de edi��o da grid.
	 * @var integer
	 * @access protected
	 */
	protected $ciButtonEditHeight = 16;

	/**
	 * Define A largura do bot�o de edi��o da grid
	 * @var integer
	 * @access protected
	 */
	protected $ciButtonEditWidth = 16;
	/**
	 * @var integer Margem esquerda do �cone ascentende e descendente
	 * @access protected
	 */
	protected $ciIconColumnOrderMarginLeft = 2;

	/**
	 * Margem do �cone at� o Static do �cone ascentende e descendente
	 * @var integer
	 * @access protected
	 */
	protected $ciIconColumnOrderMarginRight = 1;

	/**
	 * Exibe uma div dentro da grid para modificar as colunas que devem ser exibidas na grid
	 * @var integer
	 * antes do valor numerico da coluna indica ordenamento (ascendente, descendente)
	 * @access protected
	 */
	protected $cbShowEdit = false;

	/**
	 * Handle do objeto respons�vel por tratar os dados da popup de edi��o das preferencias da grid
	 * @var string
	 * @access protected
	 */
	protected $csPreferencesHandle = '';

	/**
	 * Colunas que devem ser ordenadas (separadas com ':' ) sinais de (+ e -)
	 * @var integer
	 * antes do valor numerico da coluna indica ordenamento (ascendente, descendente)
	 * @access protected
	 */
	protected $csOrder = "";

	/**
	 * Utilizado para exibir o bot�o de refresh da grid
	 * @var boolean
	 * @access protected
	 */
	protected $cbShowRefresh = true;

	/**
	 * Utilizado para exibir o bot�o de exporta��o da grid
	 * @var boolean
	 * @access protected
	 */
	protected $cbShowExcel = false;

	/**
	 * Ids da selectColumn das linhas que est�o selecionadas na grid
	 * @var array
	 * @access protected
	 */
	protected $caGridValue=array();

	/**
	 * Ids das linhas e dos selectColumn das linhas que est�o selecionadas na grid
	 * formato (array de {nomedalinha;valorSelectColumn1:valorSelectColumn2:...})
	 * @var String
	 * @access protected
	 */
	protected $caGridRowsSelected=array();

	/**
	 * Armazena se est� sendo utilizando o Internet Explorer
	 * @var string
	 * @access protected
	 */
	protected $cbIsIE = false;

	/**
	 * Indica que o requiredCheck testa, n�o se a grid n�o est� vazia, mas se existe
	 * pelo menos uma linha selecionada.
	 * @var boolean
	 * @access protected
	 */
	protected $cbSelectionRequired = false;


	/**
	 * Define cor do contorno do elemento (utilizado para debug)
	 * @var string
	 * @access protected
	 */
	protected $csDebug = "";

	/**
	 * Define A ordem em que as colunas devem ser exibidas na grid
	 * @var array
	 * @access protected
	 */
	protected $caOrderColumnShow = array();

	/**
	 * Ids dos selectColuns que devem ser selecionados pela grid (hakeamento da sele��o!)
	 * @var array
	 * @access protected
	 */
	protected $caHackSelectRows = array();

	/**
	 * Se a sele��o anterior a sele��o hakeada deve ser mantida
	 * @var boolean
	 * @access protected
	 */
	protected $cbHackCleanSelect = false;

	/**
	 * Define se o hakeamento da sele��o de linhas da grid deve ser executada
	 * @var boolean
	 * @access protected
	 */
	protected $cbHackExecHackSelect = false;

	/**
	 * Define a viewgroup contendo o que deve ser mostrado caso a grid tenha Zero Linhas exibidas
	 * @var Object
	 * @access protected
	 */
	protected $coNoResultAlert = null;

	/**
	 * Armazena a classe Zebra0 das linhas da grid
	 * @var string
	 * @access protected
	 */
	protected $csClassBodyZebra0 = "BdZ0";

	/**
	 * Armazena a classe Zebra1 das linhas da grid
	 * @var string
	 * @access protected
	 */
	protected $csClassBodyZebra1 = "BdZ1";

	/**
	 * Armazena a classe ZebraOver (quando o mouse est� sobre uma linha da grid) das linhas da grid
	 * @var string
	 * @access protected
	 */
	protected $csClassBodyZebraOver = "BdZOver";

	/**
	 * Armazena a classe BodyRowSelected (quando uma ou mais linha da grid s�o selecionadas)
	 * @var string
	 *  das linhas da grid
	 * @access protected
	 */
	protected $csClassBodyRowSelected = "BdRSel";

	/**
	 * Armazena a classe HeaderTextEdit (edit que indica a p�gina atual da grid)
	 * @var string
	 * @access protected
	 */
	protected $csClassHeaderTextEdit = "HdTextEdit";

	/**
	 * Armazena a classe da DIV do T�tulo da grid (que engloba o t�tulo e o bot�o de refresh)
	 * @var string
	 * @access protected
	 */
	protected $csClassDivHeaderTitle = "DvHdTitle";

	/**
	 * Armazena a classe do bot�o de refresh da grid.
	 * @var string
	 * @access protected
	 */
	protected $csClassButtonRefresh = "BtRefresh";

	/**
	 * Armazena a classe do bot�o de exporta��o da grid.
	 * @var string
	 * @access protected
	 */
	protected $csClassButtonExcel = "BtExcel";

	/**
	 * Armazena a classe da DivPageButton (div da barra de bot�es de navega��o)
	 * @var string
	 * @access protected
	 */
	protected $csClassDivPageButton = "DvPgBt";

	/**
	 * Armazena a classe  da DIV dos cabe�alhos das colunas da grid
	 * @var string
	 * @access protected
	 */
	protected $csClassDivHeaderColumns = "DvHdCol";

	/**
	 * Armazena a classe DIV do body da grid
	 * @var string
	 * @access protected
	 */
	protected $csClassDivBody = "DvBd";

	/**
	 * Armazena a classe dos Headers das colunas da grid
	 * @var string
	 * @access protected
	 */
	protected $csClassColumnHeader = "ColHd";

	/**
	 * Armazena a classe das C�ludas do corpo da grid
	 * @var string
	 * @access protected
	 */
	protected $csClassBodyCellUnit = "BdUt";

	/**
	 * Armazena a classe da viewGroup do HeaderTitle da grid
	 * @var string
	 * @access protected
	 */
	protected $csClassHeaderViewGroup = "HdViewGroup";

	/**
	 * Armazena a classe dos statics das c�lulas do body da grid
	 * @var string
	 * @access protected
	 */
	protected $csClassStaticBodyCellUnit = "StBdUt";

	/**
	 * Armazena a classe dos statics das c�lulas do body da grid com sem nowrap
	 * @var string
	 * @access protected
	 */
	protected $csClassStaticBodyCellUnitNFix = "StBdUtNFix";

	/**
	 * Armazena a classe dos statics dos headers das colunas da grid
	 * @var string
	 * @access protected
	 */
	protected $csClassStaticColumnHeaders = "StColHd";

	/**
	 * Armazena a classe do static do T�tulo da grid
	 * @var string
	 * @access protected
	 */
	protected $csClassStaticGridTitle = "StGridTitle";

	/**
	 * Armazena a classe do static do HeaderPageButton ("p�gina # de #") da grid
	 * @var string
	 * @access protected
	 */
	protected $csClassStaticHeaderPageButton = "StHdPgBt";

	/**
	 * Armazena a classe do button FirstPage da grid
	 * @var string
	 * @access protected
	 */
	protected $csClassButtonFirstPage = "BtFirstPg";

	/**
	 * Armazena a classe do button NextPage da grid
	 * @var string
	 * @access protected
	 */
	protected $csClassButtonNextPage = "BtNextPg";

	/**
	 * Armazena a classe do button BackPage da grid
	 * @var string
	 * @access protected
	 */
	protected $csClassButtonBackPage = "BtBackPg";

	/**
	 * Armazena a classe do button LastPage da grid
	 * @var string
	 * @access protected
	 */
	protected $csClassButtonLastPage = "BtLastPg";

	/**
	 * Armazena a classe da DIV do refreshButton
	 * @var string
	 * @access protected
	 */
	protected $csClassDivRefreshButton = "DvRefreshBt";

	/**
	 * Armazena a classe da coluna ordenada ascendentemente
	 * @var string
	 * @access protected
	 */
	protected $csClassColumnHeaderOrderASC = "ColHdOrderASC";

	/**
	 * Armazena a classe das colunas que podem ser ordenadas
	 * @var string
	 * @access protected
	 */
	protected $csClassColumnHeaderCanOrder = "ColHdCanOrder";

	/**
	 * Armazena a classe da coluna ordenada descendentemente
	 * @var string
	 * @access protected
	 */
	protected $csClassColumnHeaderOrderDESC= "ColHdOrderDESC";

	/**
	 * Armazena a classe do Static da DIV de troca das colunas que ser�o exibidas na grid
	 * @var string
	 * @access protected
	 */
	protected $csClassTitlePanel = 'StTitlePanel';

	/**
	 * Classe css do bot�o de edi��o da grid
	 * @var string
	 * @access protected
	 */
	protected $csClassButtonEditGrid = 'BtEditPreference';

	/**
	 * Classe css dos bot�es da popup de edi��o de prefer�ncias da grid
	 * @var string
	 * @access protected
	 */
	protected $csClassViewButtonPopupEdit = 'ViewBt';

	/**
	 * Classe css dos statics dos bot�es da popup de edi��o de prefer�ncias da grid
	 * @var string
	 * @access protected
	 */
	protected $csClassStaticPopupPreferences = 'StBtPopupPreference';

	/**
	 * Classe css dos statics da popup de edi��o de prefer�ncias da grid
	 * @var string
	 * @access protected
	 */
	protected $csClassStPopupPreferences = 'StPref';

	/**
	 * Classe css dos Panel da popup de edi��o de prefer�ncias da grid
	 * @var string
	 * @access protected
	 */
	protected $csClassPanelPopupPreferences = 'PanelPref';

	/**
	 * Classe css do bot�o de inserir da popup de edi��o de prefer�ncias da grid
	 * @var string
	 * @access protected
	 */
	protected $csClassVBRight = 'VBPrefRight';

	/**
	 * Classe css do bot�o de remover da popup de edi��o de prefer�ncias da grid
	 * @var string
	 * @access protected
	 */
	protected $csClassVBLeft = 'VBPrefLeft';

	/**
	 * Classe css do bot�o de mover p/ cima da popup de edi��o de prefer�ncias da grid
	 * @var string
	 * @access protected
	 */
	protected $csClassVBTop = 'VBPrefTop';

	/**
	 * Classe css do bot�o de mover p/ baixo da popup de edi��o de prefer�ncias da grid
	 * @var string
	 * @access protected
	 */
	protected $csClassVBBotton = 'VBPrefBotton';

	/**
	 * Classe css da div principal da popup de edi��o de prefer�ncias da grid
	 * @var string
	 * @access protected
	 */
	protected $csClassDialogPopup = 'DialogPopup';


	/**
	 * Indica se ao popular / dar refresh na grid, ela deve voltar para a primeira p�gina
	 * @var boolean
	 * @access protected
	 */
	protected $cbRefreshOnCurrentPage = false;


	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDGridAbstract.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites da Grid
	 */
	public function __construct($poBox = null){
		parent::__construct($poBox);
		$this->coDrawGrid = new FWDDrawGrid();
		$this->cbIsIE = FWDWebLib::browserIsIE();
	}

	/**
	 * Adiciona uma coluna da grid.
	 *
	 * <p>M�todo para adiconar uma coluna na grid.</p>
	 * @access public
	 * @param FWDColumn $poColumn Coluna
	 */
	public function addObjFWDColumn(FWDColumn $poColumn)
	{
		$miIndex = count($this->caColumns);
		//seta o nome da grid na coluna para ser usado como identificador unico nos
		//eventos de ordemamento das colunas
		$poColumn->setGridName($this->csName);
		$this->caColumns[$miIndex] = $poColumn;
		$this->ciColumnsCount = count($this->caColumns);
	}

	/**
	 * Adiciona um menu de contexto na grid.
	 *
	 * <p>M�todo para adiconar um menu de contexto na grid.</p>
	 * @access public
	 * @param FWDMenu $poMenu Menu
	 */
	public function addObjFWDMenu(FWDMenu $poMenu){
		$this->caMenus[] = $poMenu;
		$moEvent = new FWDClientEvent('OnClick','hidemenu',$poMenu->getAttrName());
		$this->addObjFWDEvent($moEvent);
	}

	/**
	 * Adiciona um menu no array de menus da grid, indexando pelo nome de menu.
	 *
	 * <p>Adiciona um menu no array de menus da grid, indexando pelo nome de menu.</p>
	 * @access public
	 * @param FWDMenu $poMenu Menu
	 */
	public function addMenu(FWDMenu $poMenu){
		if (!array_key_exists($poMenu->getAttrName(),$this->caMenusACL)) {
			$this->caMenusACL[$poMenu->getAttrName()] = $poMenu;
			$moEvent = new FWDClientEvent('OnClick','hidemenu',$poMenu->getAttrName());
			$this->addObjFWDEvent($moEvent);
		}
		else{};	// Menu ja foi adicionado na grid, nao precisa adicion�-lo novamente
	}

	/**
	 * Adiciona Uma viewGroup na Grid
	 *
	 * <p>M�todo para adiconar uma ViewGroup na grid que contem o Titulo
	 * e o Bot�o de refresh da grid se o target for o default. se o target for setado,
	 * a viewgroup ser� inserida no atributo do alerta que ser� mostrado quando a grid
	 * nao tem linhas desenhadas</p>
	 *
	 * @access public
	 * @param FWDViewGroup $poViewGroup GridTitle
	 */
	public function addObjFWDViewGroup(FWDViewGroup $poViewGroup, $psTarget = '')
	{
		if(!$psTarget){
			$this->coGridTitle = $poViewGroup;
		}else{
			$this->coNoResultAlert = $poViewGroup;
		}
	}

	/**
	 * seta o objeto respons�vel pelo Draw de cada uma das c�lulas do corpo da Grid.
	 *
	 * <p>Seta o objeto respons�vel pelo Draw de cada uma das c�lulas do corpo da Grid..</p>
	 * @access public
	 * @param FWDDrawGrid $poDrawGrid DrawGrid
	 */
	public function setObjFWDDrawGrid(FWDDrawGrid $poDrawGrid)
	{
		$this->coDrawGrid = $poDrawGrid;
	}

	/**
	 * Define o n�mero de linhas.
	 *
	 * <p>Define o n�mero de linhas por p�gina do corpo da grid.</p>
	 * @access public
	 * @param integer $piRowsPerPage N�mero de linhas por p�gina
	 */
	public function setAttrRowsPerPage($piRowsPerPage)
	{
		if($piRowsPerPage>0)
		$this->ciRowsPerPage=$piRowsPerPage;
		else
		trigger_error("Grid attribute RowsPerPage must be a positive integer",E_USER_ERROR);
	}

	/**
	 * Retorna o n�mero de linhas por p�ginas da grid.
	 *
	 * <p>retorna o n�mero de linhas por p�gina da grid.</p>
	 * @access public
	 * @return integer n�mero de linhas por p�gina da grid.
	 */
	public function getAttrRowsPerPage(){
		return $this->ciRowsPerPage;
	}

	/**
	 * Define o n�mero total de linhas.
	 *
	 * <p>Define o n�mero total de linhas da grid.</p>
	 * @access public
	 * @param integer $piRowsCount N�mero total de linhas
	 */
	public function setAttrRowsCount($piRowsCount)
	{
		if($piRowsCount>=0)
		$this->ciRowsCount=$piRowsCount;
		else
		trigger_error("Grid attribute RowsCount must be a positive integer",E_USER_ERROR);
	}

	/**
	 * Retorna o n�mero de linhas por p�ginas da grid.
	 *
	 * <p>define o comportamento do evento de refresh/populate. se o evento executar� na p�gina atual da grid ou na primeira p�gina.</p>
	 * @access public
	 * @return boolean se o refresh/populate executar� o evento na p�gina atual da grid
	 */
	public function getAttrRefreshOnCurrentPage(){
		return $this->cbRefreshOnCurrentPage;
	}

	/**
	 * Define se o refresh/populate retornar� para a p�gina inicial da grid.
	 *
	 * <p>Define se o refresh/populate retornar� para a p�gina inicial da grid.</p>
	 * @access public
	 * @param boolean $pbRefreshOnCurrentPage Refresh/populate retorna para a p�gina inicial da grid
	 */
	public function setAttrRefreshOnCurrentPage($pbRefreshOnCurrentPage){
		return $this->cbRefreshOnCurrentPage = FWDWebLib::attributeParser($pbRefreshOnCurrentPage,array("true"=>true, "false"=>false),"pbRefreshOnCurrentPage");
	}

	/**
	 * Define a p�gina atual da grid.
	 *
	 * <p>Define p�gina atual a ser exibida pela grid.</p>
	 * @access public
	 * @param integer $piCurrentPage P�gina atual da grid
	 */
	public function setCurrentPage($piCurrentPage)
	{
		if($piCurrentPage > 0)
		$this->ciCurrentPage = $piCurrentPage;
		else
		trigger_error("Grid attribute CurrentPage must be a positive integer",E_USER_ERROR);
	}

	/**
	 * Define se a grid tem preenchimento est�tico ou din�mico.
	 *
	 * <p>M�todo para definir se a grid tem preenchimento est�tico ou din�mico
	 * (via Ajax).</p>
	 * @access public
	 * @param boolean $piDinamicFill "true" ou "false"
	 */
	public function setAttrDinamicFill($pbDinamicFill)
	{
		$this->cbDinamicFill = FWDWebLib::attributeParser($pbDinamicFill,array("true"=>true, "false"=>false),"pbDinamicFill");

		if($this->cbDinamicFill == true)
		{
			$this->cbPopulate=false;
		}
	}

	/**
	 * Seta se deve aparecer a barra de rolagem no corpo da grid.
	 *
	 * <p>M�todo para setar se deve aparecer a barra de rolagem no corpo da grid.</p>
	 * @access public
	 * @param boolean $pbRollerBar valor que indica se deve aparecer a barra de rolagem no corpo da grid.
	 */
	public function setAttrShowRollerBar($pbRollerBar){
		$mbAuxValue = FWDWebLib::attributeParser($pbRollerBar,array("true"=>true, "false"=>false),"pbRollerBar");
		if($mbAuxValue)
		$this->csClassDivBody ="DvBd";
		else{
			$this->csClassDivBody ="DvBdHidden";
			if($this->cbIsIE)
			$this->ciRollerBarWidth=-1;
			else
			$this->ciRollerBarWidth=-2;
		}
	}

	/**
	 * Seta o valor da ultima p�gina da grid.
	 *
	 * <p>M�todo para setar o valor (inteiro) da ultima p�gina da grid.</p>
	 * @access public
	 * @param integer $piLastPage valor da ultima p�gina da grid
	 */
	public function setLastPage($piLastPage)
	{
		if($piLastPage > 0)
		{
			$this->ciLastPage=$piLastPage;
		}
		else
		{
			$this->ciLastPage=1;
		}
	}

	/**
	 * Seta a largura da barra de rolagem na grid.
	 *
	 * <p>M�todo para setar a largura da barra de rolagem na grid.</p>
	 * @access public
	 * @param integer $piWidth Valor para alterar a largura da barra de rolagem na grid
	 */
	public function setAttrRollerBarWidth($piWidth){
		$this->ciRollerBarWidth=$piWidth;
	}

	/**
	 * Seta a altura de cada linha do body da grid.
	 *
	 * <p>M�todo para setar a altura de cada linha do body da grid.</p>
	 * @access public
	 * @param integer $piRowHeight Valor para alterar a altura default das linhas do corpo da grid
	 */
	public function setAttrRowHeight($piRowHeight)
	{
		if($piRowHeight > 0)
		$this->ciRowHeight = $piRowHeight;
		else
		trigger_error("Grid attribute GridLineHeight must be a positive integer",E_USER_ERROR);
	}

	/**
	 * Seta a altura da barra de t�tulo da grid.
	 *
	 * <p>M�todo para setar a altura da barra de t�tulo da grid.</p>
	 * @access public
	 * @param integer $piTitleHeight Valor para alterar a altura default da barra de t�tulo da grid
	 */
	public function setAttrTitleHeight($piTitleHeight)
	{
		if($piTitleHeight > 0)
		$this->ciTitleHeight = $piTitleHeight;
		else
		trigger_error("Grid attribute GridTitleHeight must be a positive integer",E_USER_ERROR);
	}

	/**
	 * Seta a altura da barra do header das colunas da grid
	 *
	 * <p>M�todo para setar a altura do header das colunas da grid.</p>
	 * @access public
	 * @param integer $piColumnHeight Valor para alterar a altura default da barra de header das colunas
	 */
	public function setAttrColumnHeight($piColumnHeight)
	{
		if($piColumnHeight > 0)
		$this->ciColumnHeight = $piColumnHeight;
		else
		trigger_error("Grid attribute GridColumnHeight must be a positive integer",E_USER_ERROR);
	}

	/**
	 * Adiciona um dado na tabela da grid.
	 *
	 * <p>Adiciona um dado (valor de uma c�lula) na tabela de conte�do da grid.</p>
	 * @access public
	 * @param integer $piColumnIndex �ndice da coluna da tabela
	 * @param integer $piRowIndex �ndice da linha da tabela
	 * @param integer $psData informa��o a ser inserida na tabela
	 */
	public function setItem($piColumnIndex, $piRowIndex, $psData)
	{

		if($piColumnIndex<=$this->ciColumnsCount)
		{
			if ($this->ciRowsCount < $piRowIndex)
			{
				$this->ciRowsCount = $piRowIndex;
			}
			else{ }	//$row_index � menor q o contador de linhas , assim n�o eh necess�rio atualiza-lo
				
			if (!isset( $this->caRows[ $piRowIndex]))
			{
				$maRow = array();
				$maRow[ $piColumnIndex] = $psData;
				$this->caRows[ $piRowIndex] = $maRow;
			}
			else
			{
				$this->caRows[$piRowIndex][$piColumnIndex] = $psData;
			}
		}
		else
		{
			trigger_error("Trying to set a value in a inexistent column ({$piColumnIndex}).",E_USER_ERROR);
		}
	}

	/**
	 * Seta o tipo de sele��o das linhas da grid.
	 *
	 * <p>M�todo para setar o tipo de sele��o (none, single, multiple) da grid.</p>
	 * @access public
	 * @param string $psSelectType  tipo de sele��o (none, single, multiple) da grid
	 */
	public function setAttrSelectType($psSelectType)
	{
		$this->ciSelectType = FWDWebLib::attributeParser($psSelectType,array("single"=>GRID_SEL_SINGLE,
								"multiple"=>GRID_SEL_MULTIPLE,"none" =>GRID_SEL_NONE),"psSelectType");
	}

	/**
	 * Seta as colunas da grid que ser�o selecionadas.
	 *
	 * <p>M�todo para setar as colunas que ser�o selecionadas (cujos Ids ser�o utilizados para a sele��o das linhas
	 * da grid e para passar para o menu de contexto da grid) da grid.</p>
	 * @access public
	 * @param string $psSelectColumn  colunas da grid que ser�o utilizadas para a sele��o da grid e para o
	 * menu de contexto
	 */
	public function setAttrSelectColumn($psSelectColumn)
	{
		$this->csSelectColumn = $psSelectColumn;
	}

	/**
	 * Adiciona uma linha na grid.
	 *
	 * <p>M�todo para adiconar uma linha ao corpo da grid.</p>
	 * @access protected
	 * @param FWDViewGroup poRowsObj ViewGroup
	 * @param integer $piIndex �ndice da linha
	 */
	protected function setObjRow(FWDViewGroup $poRowsObj, $piIndex)
	{
		$this->caRowsObj[$piIndex] = $poRowsObj;
	}

	/**
	 * Seta via XML se a grid deve ser populada ou n�o.
	 *
	 * <p>M�todo para setar via XML se a grid grid deve ser populada (deve ser desenhado o
	 * body da grid).</p>
	 * @access public
	 * @param boolean $pbPopulateXML valor "true" "false" para popular / nao popular a grid
	 */
	public function setAttrPopulate($pbPopulateXML)
	{
		$this->cbPopulateXML = FWDWebLib::attributeParser($pbPopulateXML,array("true"=>true, "false"=>false),"pbPopulateXML");
	}

	/**
	 * Retorna se a grid deve ser populada ou n�o.
	 *
	 * <p>M�todo para retornar se a grid deve ser populada ou n�o (NUNCA usar o metodo setPopulate
	 * e SIM o m�todo setAttrPopulate()).</p>
	 * @access public
	 * @return integer p�gina atual da grid
	 */
	public function getAttrPopulate(){
		return $this->cbPopulateXML;
	}

	/**
	 * Seta se a grid deve exibir o bot�o de refresh.
	 *
	 * <p>M�todo para setar se a grid grid deve exibir o bot�o de refresh.</p>
	 * @access public
	 * @param boolean pbShowRefresh valor "true" "false" para mostrar / nao mostrar o bot�o de refresh da grid
	 */
	public function setAttrShowRefresh($pbShowRefresh)
	{
		$this->cbShowRefresh = FWDWebLib::attributeParser($pbShowRefresh,array("true"=>true, "false"=>false),"pbShowRefresh");
	}

	/**
	 * Retorna se deve ser exibido o bot�o de refresh da grid.
	 *
	 * <p>M�todo para retornar o valor do bot�o de refresh da grid.</p>
	 * @access public
	 * @return boolean Se o bot�o de refresh da grid deve ser exibido ou n�o
	 */
	public function getAttrShowRefresh(){
		return $this->sbShowRefresh;
	}

	/**
	 * Seta se a grid deve exibir o bot�o de exporta��o para Excel.
	 *
	 * <p>M�todo para setar se a grid deve exibir o bot�o de exporta��o para Excel.</p>
	 * @access public
	 * @param boolean pbShow True indica que o bot�o deve ser exibido
	 */
	public function setAttrShowExcel($pbShow){
		$this->cbShowExcel = FWDWebLib::attributeParser($pbShow,array("true"=>true, "false"=>false),"showexcel");
	}

	/**
	 * Seta a largura interna da grid.
	 *
	 * <p>M�todo para setar a largura interna da grid com base na soma das larguras das
	 * colunas </p>
	 * @access protected
	 * @param integer $piGridWidth valor da largura da coluna
	 */
	protected function setAttrGridWidth($piGridWidth)
	{
		if($piGridWidth > 0)
		$this->ciGridWidth = $piGridWidth;
		else
		trigger_error("Grid attribute GridWidth must be a positive integer",E_USER_ERROR);
	}

	/**
	 * Retorna o valor da p�gina atual da grid.
	 *
	 * <p>M�todo para retornar o valor da p�gina atual da grid.</p>
	 * @access public
	 * @return integer p�gina atual da grid
	 */
	public function getCurrentPage()
	{
		return $this->ciCurrentPage;
	}

	/**
	 * Retorna o valor da ultima p�gina da grid.
	 *
	 * <p>M�todo para retornar o valor (inteiro) correspondente a ultima p�gina da
	 * grid.</p>
	 * @access public
	 * @return integer �ltima p�gina da grid
	 */
	public function getLastPage()
	{
		return $this->ciLastPage;
	}

	/**
	 * Retorna o array de linhas da grid.
	 *
	 * <p>M�todo para retornar o array de linhas da grid.</p>
	 * @access public
	 * @return array Array de linhas da grid
	 */
	public function getRows()
	{
		return $this->caRows;
	}

	/**
	 * Retorna o conte�do de uma c�lula da grid.
	 *
	 * <p>Retorna o conte�do de uma c�lula da grid.</p>
	 * @access public
	 * @return string O conte�do da c�lula ou false caso ela n�o exista.
	 */
	public function getCellValue($piRow,$piCol){
		if(isset($this->caRows[$piRow][$piCol])){
			return $this->caRows[$piRow][$piCol];
		}else{
			return false;
		}
	}

	/**
	 * Retorna o array de colunas da grid.
	 *
	 * <p>M�todo para retornar o array de colunas da grid.</p>
	 * @access public
	 * @return array Array de colunas da grid
	 */
	public function getColumns()
	{
		return $this->caColumns;
	}

	/**
	 * Retorna o total de linhas da grid.
	 *
	 * <p>M�todo para retornar o total de linhas da grid.</p>
	 * @access public
	 * @return integer Total de linhas da grid
	 */
	public function getAttrRowsCount()
	{
		return $this->ciRowsCount;
	}

	/**
	 * Retorna a altura das linhas da grid.
	 *
	 * <p>M�todo para retornar a altura das linhas da grid.</p>
	 * @access public
	 * @return integer Altura das linhas da grid
	 */
	public function getAttrRowHeight()
	{
		return $this->ciRowHeight;
	}

	/**
	 * Retorna se a grid � populada estaticamente o dinamicamente.
	 *
	 * <p>M�todo utilizado para obter o tipo de preenchimento da grid
	 * se � est�tico ou din�mico.</p>
	 * @access public
	 * @return booblean "true" / "false" preenchimento din�mico / est�tico
	 **/
	public function getAttrDinamicFill()
	{
		return $this->cbDinamicFill;
	}

	/**
	 * Retorna o tipo de sele��o das linhas da grid.
	 *
	 * <p>M�todo utilizado para obter o tipo de sele��o das linhas da grid (none, single, multiple).</p>
	 * @access public
	 * @return integer (0, 1, 2) tipo de sele��o da grid
	 **/
	public function getAttrSelectType()
	{
		return $this->ciSelectType;
	}

	/**
	 * Retorna o n�mero da coluna que deve ser utilizada para sele��o.
	 *
	 * <p>M�todo utilizado para obter o n�mero da coluna que ser� utilizado para sele��o das linhas da grid e
	 * do menu de contexto.</p>
	 * @access public
	 * @return string n�mero da coluna que ser� utlilizada para sele��o das linhas da grid e do menu de contexto
	 **/
	public function getAttrSelectColumn()
	{
		return $this->csSelectColumn;
	}

	/**
	 * Retorna o array de menus de contexto definidos dentro da grid.
	 *
	 * <p>M�todo utilizado para obter o array de menus de contexto definidos dentro da grid.</p>
	 * @access public
	 * @return array array dos menus de contexto definidos dentro da grid.
	 **/
	public function getArrayObjFWDMenus()
	{
		return $this->caMenus;
	}

	/**
	 * Seta o valor da Margem a esquerda do �cone de ordenamento.
	 *
	 * <p>Seta o valor da margem � esquerda do �cone de ordenamento do header das colunas da grid</p>
	 * @access public
	 * @param string $piIconMarginLeft Margem a esquerda do �cone de ordenamento da coluna
	 */
	public function setAttrIconColumnOrderMarginLeft($piIconMarginLeft){
		$this->ciIconColumnOrderMarginLeft = $piIconMarginLeft;
	}

	/**
	 * Seta a margem � direita do �cone de ordenamento.
	 *
	 * <p>Seta o valor da margem � direita (entre o �cone e o Static) do �cone de ordenamento do header das colunas da grid</p>
	 * @access public
	 * @param string $piIconMarginLeft Margem a direita do �cone de ordenamento da coluna
	 */
	public function setAttrIconColumnOrderMarginRight($piIconMarginRight){
		$this->ciIconColumnOrderMarginRight = $piIconMarginRight;
	}

	/**
	 * Retorna A margem � esquerda do Icone de Ordenamento do header das colunas da grid.
	 *
	 * <p>Retorna A margem � a esquerda do Icone de Ordenamento do header das colunas da grid..</p>
	 * @access public
	 * @return integer Margem � esquerda e o Static do header da coluna
	 */
	public function getAttrIconColumnOrderMarginLeft(){
		return $this->ciIconColumnOrderMarginLeft;
	}

	/**
	 * Retorna A margem � direita do Icone de Ordenamento do header das colunas da grid.
	 *
	 * <p>Retorna A margem � direita do Icone de Ordenamento do header das colunas da grid..</p>
	 * @access public
	 * @return integer Margem a direita do icone de ordenamento e o Static do header da coluna
	 */
	public function getAttrIconColumnOrderMarginRight(){
		return $this->ciIconColumnOrderMarginRight;
	}

	/**
	 * Seta as colunas que v�o ordenar a grid.
	 *
	 * <p>Seta as colunas que v�o ordenar a grid (separadas por ':'). Os sinais de (+,-)
	 * antes do n�mero da coluna inticam ordenamento (ascendente, descendente).</p>
	 * @access public
	 * @param string $psOrder Colunas que v�o ordenar a grid.
	 */
	public function setAttrOrder($psOrder){
		$this->csOrder = $psOrder;
	}

	/**
	 * Retorna as colunas que devem ordenar a grid.
	 *
	 * <p>Retorna as colunas que devem ordenar a grid (separadas por ':'). Os sinais de (+,-)
	 * antes do n�mero da coluna inticam ordenamento (ascendente, descendente).</p>
	 * @access public
	 * @return String Colunas que devem ordenar a grid
	 */
	public function getAttrOrder(){
		return $this->csOrder;
	}

	/**
	 * Atribui o valor dos ids das colunas selecionadas da grid.
	 *
	 * <p>Atribui o valor dos ids das colunas selecionadas na grid</p>
	 * @access public
	 * @param string $paGridValue Ids da linhas selecionadas na grid
	 */
	public function setSelectValue($paGridValue) {
		if($paGridValue =='')
		$this->caGridValue = array();
		else
		$this->caGridValue = $paGridValue;
	}

	/**
	 * Retorna os Ids(ids dos selectColumn) das linhas selecionadas da grid.
	 *
	 * <p>M�todo para retornar os Ids da linhas selecionadas da grid.</p>
	 * @access public
	 * @return string Ids das linhas selecionadas da grid
	 */
	public function getSelectValue() {
		return $this->caGridValue;
	}

	/**
	 * Seta o nome do objeto que ir� tratar as preferencias do usu�rio para a grid
	 *
	 * <p>Seta o nome do objeto que ir� tratar as preferencias do usu�rio para a grid, permitindo a
	 * configura��o de v�rios itens da grid.</p>
	 * @access public
	 * @param boolean $psPreferencesHandle Nome do objeto que tratar� os dados de configura��o da grid
	 */
	public function setAttrPreferencesHandle($psPreferencesHandle){
		$this->csPreferencesHandle = $psPreferencesHandle;
	}

	/**
	 * Retorna o nome do objeto que trata das preferencias do usu�rio
	 *
	 * <p>M�todo para retornar o nome do objeto que tratar� as preferencias de usu�rio da grid.</p>
	 * @access public
	 * @return string Nome do objeto que trata das preferencias do usu�rio
	 */
	public function getAttrPreferencesHandle(){
		return $this->csPreferencesHandle;
	}
	/**
	 * Retorna os Ids(ids dos selectColumn) das linhas selecionadas da grid.
	 *
	 * <p>M�todo para retornar os Ids da linhas selecionadas da grid.</p>
	 * @access public
	 * @return array Ids das linhas selecionadas da grid
	 */
	public function getValue(){
		$maReturn = array();
		$maColumnIndex = explode(':',$this->csSelectColumn);
		if(count($maColumnIndex)>1){
			foreach ($this->caGridValue as $csValue){
				$caValue = explode(':',$csValue);
				$miCont=0;
				$maRow = array();
				foreach ($maColumnIndex as $csColumn){
					if(isset($caValue[$miCont]))
					$maRow[$csColumn] = $caValue[$miCont];
					else
					$maRow[$csColumn] ='';
						
					$miCont++;
				}
				$maReturn[]=$maRow;
				unset($maRow);
			}
		}else
		foreach ($this->caGridValue as $csValue)
		$maReturn[]=$csValue;

		return $maReturn;
	}

	/**
	 * Retorna os Ids(ids dos selectColumn) das linhas selecionadas da grid.
	 *
	 * <p>M�todo para retornar os Ids da linhas selecionadas da grid.</p>
	 * @access public
	 * @param integer $piColumnIndex index da coluna dos selectColumns q devem ser retornados
	 * @return array valores de selectColumns das linhas referentes ao columnIndex desejahdo
	 */
	public function getArraySelectColumns($piColumnIndex){
		$maValue = $this->getValue();
		$maReturn = array();
		$maColumnIndex = explode(':',$this->csSelectColumn);
		if(count($maColumnIndex)>1)
		foreach($maValue as $maValueRow){
			if(isset($maValueRow[$piColumnIndex]))
			$maReturn[]= $maValueRow[$piColumnIndex];
		}
		else
		if($piColumnIndex == $maColumnIndex[0])
		foreach($maValue as $maValueRow){
			$maReturn[]= $maValueRow;
		}
		return $maReturn;
	}

	/**
	 * Retorna os Ids (ids da linha;id do selectColumn)das linhas selecionadas da grid.
	 *
	 * <p>M�todo para retornar os Ids da linhas selecionadas da grid.</p>
	 * @access public
	 * @return string Ids das linhas selecionadas da grid
	 */
	public function getRowsSelected(){
		return $this->caGridRowsSelected;
	}

	/**
	 * Atribui o valor dos ids (ids da linha;id do selectColumn)das colunas selecionadas da grid.
	 *
	 * <p>Atribui o valor dos ids (ids da linha;id do selectColumn) das colunas selecionadas na grid</p>
	 * @access public
	 * @param string $paRowsSelected Ids (ids da linha;id do selectColumn) da linhas selecionadas na grid
	 */
	public function setRowsSelected($paRowsSelected){
		if($paRowsSelected=='')
		$this->caGridRowsSelected = array();
		else
		$this->caGridRowsSelected = $paRowsSelected;
	}

	/**
	 * Seta se deve ser exibido o bot�o de edi��o de preferencias da grid
	 *
	 * <p>Seta se deve ser exibido o bot�o de edi��o de preferencias da grid, assim � permitido
	 * configurar as op��o de cada usu�rio em uma grid.</p>
	 * @access public
	 * @param boolean $pbShowEdit Se vai ser exibido o o bot�o de edi��o de preferencias da grid.
	 */
	public function setAttrShowEdit($pbShowEdit){
		$this->cbShowEdit = FWDWebLib::attributeParser($pbShowEdit,array("true"=>true, "false"=>false),"pbShowEdit");
	}

	/**
	 * Seta o Prefixo da Classe CSS dos elementos da grid
	 *
	 * <p>Seta o prefixo da classe css da grid , assim � permitido alterar a familia de classes css da grid.</p>
	 * @access public
	 * @param string $psCSSClass Seta o Prefixo da Classe CSS dos elementos da grid.
	 */
	public function setAttrCSSClass($psCSSClass){
		$this->csCSSClass = $psCSSClass;
	}

	/**
	 * Retorna o prefixo das classes css da grid.
	 *
	 * <p>Metodo para retornar o prefixo das classes css da grid.</p>
	 * @access public
	 * @return String Prefixo das classes css da grid.
	 */
	public function getAttrCSSClass(){
		$this->csCSSClass;
	}

	/**
	 * Retorna a classe css do titulo do panel de preferencias da grid
	 *
	 * <p>Metodo para retornar a classe css do titulo do panel de preferencias da grid.</p>
	 * @access public
	 * @return String Classe CSS do titulo do panel da grid.
	 */
	public function getCssClassTitlePanel(){
		return $this->csClassTitlePanel;
	}

	/**
	 * Redefine as dimens�es das c�lulas do header da Grid
	 *
	 * <p>M�todo para  redefinir as dimens�es da box dos statics dentro da coluna para
	 * ter as mesmas dimen��es da box de cada coluna</p>
	 * @access protected
	 */
	protected function defineBoxStatic($piLastColumnWidth)
	{
		//redefine a box do static dentro da coluna p/ ter as mesmas dimen��es da box da coluna]
		if(count($this->caOrderColumnShow)>0){
			$miContColumn = $this->caOrderColumnShow[count($this->caOrderColumnShow)-1];
			foreach($this->caOrderColumnShow as $miI)
			{
				$moBox= $this->caColumns[$miI]->getObjFWDBox();
				if ($this->cbIsIE)
				$moBoxStatic = new FWDBox(-1,-1,$moBox->getAttrHeight()+2,$moBox->getAttrWidth()+2);
				else
				$moBoxStatic = new FWDBox(-1,-1,$moBox->getAttrHeight()+2,$moBox->getAttrWidth()+2);
				foreach($this->caColumns[$miI]->getContent() as $moColStatic)
				if(!($moColStatic instanceof FWDIcon))
				$moColStatic->setObjFWDBox($moBoxStatic);
			}
		}
	}

	/**
	 * Redefine as dimens�es das boxes de cada uma das colunas definidas na grid
	 *
	 * <p>M�todo para  redefinir as dimens�es da box das colunas da grid e para setar
	 * algumas v�riaveis da grid que ser�o utilizadas em outros m�todos da grid como:
	 * a largura interna da grid (soma das larguras de todas as colunas definidas na grid)
	 * um vetor para sinalizar quais colunas estao ativas (devem ser exibidas na tela)
	 * um vetor para sinalizar quais colunas nao devem ter eventos
	 * </p>
	 * @access protected
	 */
	public function arrangeAllDimensions()
	{
		//obtem dados q ser�o usados frequentemente
		// largura das colunas
		// se a coluna tah ativa ou n�o
		// se os eventos est�o bloqueados ou n�o para cada coluna
		// n�menro total de colunas da grid

		$miI = 1;
		$miContColumnWidth=0;//largura acumulada das colunas da grid
		$miColWithoutWidth=0;//contador de colunas que n�o tem largura definida
		$miMaxColumnHeight=0;

		$mbHasColumnOrderToShow = count($this->caOrderColumnShow);

		foreach($this->caColumns as $moColumn)
		{
			$moBox=$moColumn->getObjFWDBox();
			$this->caColumnDisplay[$miI] =(($moColumn->getAttrDisplay() == true)?1:0);
			$this->caEventBlock[$miI] =  (($moColumn->getAttrEventBlock() == true)?1:0);
			$this->caToolTip[$miI] =(($moColumn->getAttrToolTip() == true)?1:0);
			$this->caToolTipSource[$miI] = ($moColumn->getSourceToolTip()?$moColumn->getSourceToolTip():$miI);

			if(!$mbHasColumnOrderToShow){
				if($moColumn->getAttrDisplay()){
					$this->caOrderColumnShow[]=$miI -1;
				}
			}
			$miI++;

			if($moColumn->getAttrDisplay())
			{
				if($miMaxColumnHeight < $moBox->getAttrHeight())
				$miMaxColumnHeight = $moBox->getAttrHeight();
				if($moBox->getAttrWidth()<=0)
				$miColWithoutWidth++;
				else
				$miContColumnWidth+=$moBox->getAttrWidth();
			}
			//else { }//a coluna n�o vai ser exibida, ou seja, n�o eh necess�rio calcular a largura acumulada da grid
		}

		/*descobre qual a ultima coluna que vai ser exibida*/
		$miLastColumn = count($this->caOrderColumnShow)-1;

		//verifica se foi definido no xml a altura da barra do header das colunas
		//se nao foi definido, usar a maior altura das box dos header das colunas
		if($this->ciColumnHeight<=0)
		$this->ciColumnHeight = $miMaxColumnHeight;
		//else { }//foi definido pelo xml a altura dos headers das colunas

		if($miColWithoutWidth<=0)
		//apenas para evitar dividir por zero
		$miColWithoutWidth=1;
		//else {}//uma ou mais colunas foram definidas sem a largura, assim ser� necess�rio dividir a largura restante
		//da grid entre essas colunas.
		//calcula largura para as colunas em rela��o a largura da grid, j� considerando a largura da barra de rolagem
		$miContColumnWidth = $this->coBox->getAttrWidth() - $miContColumnWidth - $this->ciRollerBarWidth;
		//calcula a largura para de cada uma das colunas que n�o tem largura definida
		$miContColumnWidth = floor($miContColumnWidth / $miColWithoutWidth);
		$miSumWidth=0;
		$miContColumn=0;
		//redefine as dimen�oes das colunas com base nos calculos executados acima
		$miLastWidth = $this->coBox->getAttrWidth() - $this->ciRollerBarWidth -4;

		foreach($this->caOrderColumnShow as $miI){
			$moBox=$this->caColumns[$miI]->getObjFWDBox();
			$moBox->setAttrLeft($miSumWidth-1);
			$moBox->setAttrTop(-1);
			$moBox->setAttrHeight($this->ciColumnHeight -1);
			//caso a box da coluna n�o foi definida, ser� utilizada a largura calculada da sobra da lar
			//gura da grid que foi calculada anteriormente
			if($moBox->getAttrWidth()<=0)
			$moBox->setAttrWidth($miContColumnWidth);
			//else { }//a coluna tem sua largura definida assim n�o � necessario utilizar a largura calculada.
			$miSumWidth += $moBox->getAttrWidth();
			if($miLastColumn == $miContColumn){
				if ($this->cbIsIE)
				$miLastWidth = $miLastWidth - $moBox->getAttrLeft()+2;
				else
				$miLastWidth = $miLastWidth - $moBox->getAttrLeft();
				$moBox->setAttrWidth($miLastWidth);
			}
			else
			if ($this->cbIsIE)
			$moBox->setAttrWidth($moBox->getAttrWidth() +1);
			else
			$moBox->setAttrWidth($moBox->getAttrWidth() -1);
			$this->caColumns[$miI]->setObjFWDBox($moBox);
			unset($moBox);
			$miContColumn++;
		}//end foreach
		$this->setAttrGridWidth($this->coBox->getAttrWidth());
		$this->defineBoxStatic($miLastWidth);
	}

	/**
	 * Seta o modo de debug do elemento.
	 *
	 * <p>M�todo para setar o modo de debug do elemento.</p>
	 * @access public
	 * @param string $psDebug Seta o modo de debug
	 */
	final public function setAttrDebug($psDebug) {
		$this->csDebug = (($psDebug=="false")?"":$psDebug);
	}

	/**
	 * Seta o valor do atributo selectionrequired
	 *
	 * <p>Seta o valor do atributo selectionrequired.</p>
	 * @access public
	 * @param boolean $pbSelectionRequired Novo valor do atributo
	 */
	public final function setAttrSelectionRequired($psSelectionRequired){
		$this->cbSelectionRequired = FWDWebLib::attributeParser($psSelectionRequired,array("true"=>true, "false"=>false),"selectionrequired");
	}

	/**
	 * Retorna o estilo para o modo de debug.
	 *
	 * <p>M�todo para retornar o estilo para o modo de debug.</p>
	 * @access private
	 * @return string Estilo do debug
	 */
	public function debugView() {
		$moRGBTable = FWDWebLib::getObject("goRGBTable");
		$msDebug = $this->csDebug;
		$msColor = ((in_array($msDebug,$moRGBTable)||($msDebug[0]=="#"&&strlen($msDebug)==7))?$msDebug:"red");
		return "border-color:{$msColor};border-style:dashed;border-width:1px;";
	}

	/**
	 * Retorna um array com todos sub-elementos da grid
	 *
	 * <p>Retorna um array contendo todos elementos contidos na grid ou em
	 * seus descendentes.</p>
	 * @access public
	 * @return array Array de sub-elementos
	 */
	public function collect() {
		$maOut = parent::collect();
		if($this->coGridTitle)
		$maOut = array_merge($maOut,$this->coGridTitle->collect());
		if($this->caMenus)
		foreach ($this->caMenus as $moMenu)
		$maOut = array_merge($maOut,$moMenu->collect());
		if($this->caMenusACL)
		foreach ($this->caMenusACL as $moMenuACL)
		$maOut = array_merge($maOut,$moMenuACL->collect());
		if($this->caColumns)
		foreach ($this->caColumns as $moColumn)
		$maOut = array_merge($maOut,$moColumn->collect());
		return $maOut;
	}


	/**
	 * Retorna o nome da coluna passada por parametro
	 *
	 * <p>Cria um nome para a coluna passada por parametro caso ela n�o tenha nome e retorna esse nome.</p>
	 * @access public
	 * @return string Nome para a coluna
	 */
	private function getColumnName($poColumn,$piIndex){
		$msReturn = '';
		$msReturn = $poColumn->getValue();
		if($msReturn==''){
			$msReturn = $poColumn->getAttrAlias();
			if($msReturn=='')
			$msReturn = FWDLanguage::getPHPStringValue('fwdgrid_pref_column_name', 'Coluna').$piIndex;
		}
		return $msReturn;
	}

	/**
	 * Cria os objetos do panel de edi��o das propriedades da grid
	 *
	 * <p>Cria os objetos do panel de edi��o das propriedades da grid.</p>
	 * @access public
	 * @return Objeto FWDPanel
	 */
	private function getCodePanelGridEdit(){
		//panel de propriedades da grid
		$moPanel = new FWDPanel(new FWDBox(410,10,150,300));
		$moPanel->setAttrClass($this->csCSSClass . $this->csClassPanelPopupPreferences);
		//static do titulu do panel
		$moStP = new FWDStatic(new FWDBox(0,0,20,300));
		$moStP->setAttrValue(FWDLanguage::getPHPStringValue('fwdgrid_pref_grid_properties', 'Edi��o das Propriedades da Grid'));
		$moStP->setAttrClass($this->csCSSClass . $this->csClassTitlePanel);
		$moStP->setAttrVertical('middle');
		$moStP->setAttrMarginLeft(10);
		$moPanel->addObjFWDView($moStP,'title');
		//st linhas por p�gina
		$moStRowPage = new FWDStaticGrid(new FWDBox(10,30,20,120));
		$moStRowPage->setValue(FWDLanguage::getPHPStringValue('fwdgrid_pref_rows_per_page', 'Linhas por P�gina:'));
		$moStRowPage->setAttrClass($this->csCSSClass . $this->csClassStPopupPreferences);
		$moStRowPage->setAttrHorizontal('right');
		$moPanel->addObjFWDView($moStRowPage);
		//text row page
		$moTextRowPage = new FWDText(new FWDBox(140,30,20,90));
		$moTextRowPage->setAttrName("{$this->csName}_popupRowsPage");
		$moTextRowPage->setValue($this->ciRowsPerPage);
		$moMask = new FWDMaskAutoComplete();
		$moMask->setAttrType('digit');
		$moTextRowPage->addObjFWDMask($moMask);
		$moPanel->addObjFWDView($moTextRowPage);
		//st altura das linhas
		$moStRowHeight = new FWDStaticGrid(new FWDBox(10,60,20,120));
		$moStRowHeight->setValue(FWDLanguage::getPHPStringValue('fwdgrid_pref_rows_height', 'Altura das Linhas:'));
		$moStRowHeight->setAttrClass($this->csCSSClass . $this->csClassStPopupPreferences);
		$moStRowHeight->setAttrHorizontal('right');
		$moPanel->addObjFWDView($moStRowHeight);
		//text altura das linhas
		$moTextRowHeight = new FWDText(new FWDBox(140,60,20,90));
		$moTextRowHeight->setAttrName("{$this->csName}_popupRowsHeight");
		$moTextRowHeight->setValue($this->ciRowHeight);
		$moMaskHeight = new FWDMaskAutoComplete();
		$moMaskHeight->setAttrType('digit');
		$moTextRowHeight->addObjFWDMask($moMaskHeight);
		$moPanel->addObjFWDView($moTextRowHeight);

		//st tooltip conte�do
		$moStRowTT = new FWDStaticGrid(new FWDBox(10,90,20,120));
		$moStRowTT->setValue(FWDLanguage::getPHPStringValue('fwdgrid_pref_tooltip_label', 'ToolTip:'));
		$moStRowTT->setAttrClass($this->csCSSClass . $this->csClassStPopupPreferences);
		$moStRowTT->setAttrHorizontal('right');
		$moPanel->addObjFWDView($moStRowTT);
		//select da coluna que ser� o conte�do da tooltip
		$moSelectTT = new FWDSelect(new FWDBox(140,90,20,150));
		$moSelectTT->setAttrName("{$this->csName}_sel_tt");
		//select da coluna que receber� o tooltip
		$moSelectTTtarget = new FWDSelect(new FWDBox(140,120,20,150));
		$moSelectTTtarget->setAttrName("{$this->csName}_sel_tt_target");
		//itens auxiliares do select para explica��o ao usu�rio
		$moItem = new FWDItem();
		$moItem->setValue(FWDLanguage::getPHPStringValue('fwdgrid_pref_select_source','Selecione a Origem'));
		$moSelectTT->addObjFWDItem($moItem);
		$moItem = new FWDItem();
		$moItem->setValue(FWDLanguage::getPHPStringValue('fwdgrid_pref_select_target', 'Selecione o Alvo'));
		$moSelectTTtarget->addObjFWDItem($moItem);
		$miSourceTT = -1;
		for($miI=0;$miI<$this->ciColumnsCount;$miI++){
			//item do select p/ cada coluna
			if($this->caColumns[$miI]->getAttrEdit()){
				$moItemTgt = new FWDItem();
				$moItemTgt->setAttrKey($miI+1);
				$moItemTgt->setValue($this->getColumnName($this->caColumns[$miI],$miI));
				if($this->caColumns[$miI]->getAttrToolTip()){
					$moItemTgt->setAttrCheck('true');
					$miSourceTT = $this->caColumns[$miI]->getSourceToolTip();
					if(!$miSourceTT)
					$miSourceTT = $miI+1;
				}
				$moSelectTTtarget->addObjFWDItem($moItemTgt);
				unset($moItem);
			}
		}
		for($miI=0;$miI<$this->ciColumnsCount;$miI++){
			//item do select p/ cada coluna
			if($this->caColumns[$miI]->getAttrEdit()){
				$moItemSrc = new FWDItem();
				$moItemSrc->setAttrKey($miI+1);
				$moItemSrc->setValue($this->getColumnName($this->caColumns[$miI],$miI));
				if($miI == ($miSourceTT -1))
				$moItemSrc->setAttrCheck('true');
				$moSelectTT->addObjFWDItem($moItemSrc);
				unset($moItem);
			}
		}
		$moPanel->addObjFWDView($moSelectTT);
		$moPanel->addObjFWDView($moSelectTTtarget);
		//st tooltip target
		$moStRowTTtarget = new FWDStaticGrid(new FWDBox(10,120,20,120));
		$moStRowTTtarget->setValue(FWDLanguage::getPHPStringValue('fwdgrid_pref_target', 'Alvo:'));
		$moStRowTTtarget->setAttrClass($this->csCSSClass . $this->csClassStPopupPreferences);
		$moStRowTTtarget->setAttrHorizontal('right');
		$moPanel->addObjFWDView($moStRowTTtarget);
		return $moPanel;
	}

	/**
	 * Cria os objetos do panel de edi��o das propriedades das colunas da grid
	 *
	 * <p>Cria os objetos do panel de edi��o das propriedades das colunas da grid.</p>
	 * @access public
	 * @return Objeto FWDPanel
	 */
	public function getCodePanelColumnEdit(){
		$msVarCode = '';
		$moPanel = new FWDPanel(new FWDBox(410,170,150,300));
		$moPanel->setAttrClass($this->csCSSClass . $this->csClassPanelPopupPreferences);
		//static do titulu do panel
		$moStP = new FWDStatic(new FWDBox(0,0,20,300));
		$moStP->setAttrValue(FWDLanguage::getPHPStringValue('fwdgrid_pref_column_properties','Propriedades da Coluna'));
		$moStP->setAttrClass($this->csCSSClass . $this->csClassTitlePanel);
		$moStP->setAttrVertical('middle');
		$moStP->setAttrMarginLeft(10);
		$moPanel->addObjFWDView($moStP,'title');
		//st tooltip conte�do
		$moStCol = new FWDStaticGrid(new FWDBox(10,30,20,120));
		$moStCol->setValue(FWDLanguage::getPHPStringValue('fwdgrid_pref_column_label', 'Coluna:'));
		$moStCol->setAttrClass($this->csCSSClass . $this->csClassStPopupPreferences);
		$moStCol->setAttrHorizontal('right');
		$moPanel->addObjFWDView($moStCol);
		//select que exibe as colunas
		$moSelectColProp = new FWDSelect(new FWDBox(140,30,20,150));
		$moSelectColProp->setAttrName("{$this->csName}_sel_col_prop");
		//evento onclick do select
		$moSelColEvent = new FWDClientEvent();
		$moSelColEvent->setAttrValue('objManagerGrifPref.propColClick();');
		$moSelColEvent->setAttrEvent('onClick');
		$moSelectColProp->addObjFWDEvent($moSelColEvent);

		$moItem = new FWDItem();
		$moSelectColProp->addObjFWDItem($moItem);
		$msAuxSeparate = '';
		for($miI=0;$miI<$this->ciColumnsCount;$miI++){
			//item do select p/ cada coluna
			if(($this->caColumns[$miI]->getAttrEdit())&&($this->caColumns[$miI]->getAttrDisplay())){
				$moItem = new FWDItem();
				$moItem->setAttrKey($miI);
				$moItem->setValue($this->getColumnName($this->caColumns[$miI],$miI));
			}
			if($this->caColumns[$miI]->getAttrEdit()){
				$msNoWrap ='true';
				if($this->caColumns[$miI]->getAttrColNoWrap()==true)
				$msNoWrap ='false';
				$msVarCode .= $msAuxSeparate . $miI .':'. $this->caColumns[$miI]->getObjFWDBox()->getAttrWidth() .':'.$msNoWrap ;
				$msAuxSeparate = ';';
			}
		}
		$moPanel->addObjFWDView($moSelectColProp);
		//st linhas por p�gina
		$moStColWidth = new FWDStaticGrid(new FWDBox(10,60,20,120));
		$moStColWidth->setValue(FWDLanguage::getPHPStringValue('fwdgrid_pref_column_width','Largura*:'));
		$moStColWidth->setAttrClass($this->csCSSClass . $this->csClassStPopupPreferences);
		$moStColWidth->setAttrHorizontal('right');
		$moPanel->addObjFWDView($moStColWidth);
		//text Col page
		$moTextColWidth = new FWDText(new FWDBox(140,60,20,70));
		$moTextColWidth->setAttrName("{$this->csName}_popupColWidth");
		$moMask = new FWDMaskAutoComplete();
		$moMask->setAttrType('digit');
		$moTextColWidth->addObjFWDMask($moMask);
		$moPanel->addObjFWDView($moTextColWidth);
		//st tooltip conte�do
		$moStnoWrap = new FWDStaticGrid(new FWDBox(10,90,20,120));
		$moStnoWrap->setValue('Quebra de linha:');
		$moStnoWrap->setAttrClass($this->csCSSClass . $this->csClassStPopupPreferences);
		$moStnoWrap->setAttrHorizontal('right');
		$moPanel->addObjFWDView($moStnoWrap);
		//select do noWrap da coluna selecionada
		$moSelectnoWrap = new FWDSelect(new FWDBox(140,90,20,70));
		$moSelectnoWrap->setAttrName("{$this->csName}_sel_no_wrap");
		$moItem = new FWDItem();
		$moSelectnoWrap->addObjFWDItem($moItem);
		$moItemT = new FWDItem();
		$moItemT->setAttrKey('true');
		$moItemT->setValue(FWDLanguage::getPHPStringValue('fwdgrid_pref_true_value', 'Sim'));
		$moSelectnoWrap->addObjFWDItem($moItemT);
		$moItemF = new FWDItem();
		$moItemF->setAttrKey('false');
		$moItemF->setValue(FWDLanguage::getPHPStringValue('fwdgrid_pref_false_value', 'N�o'));
		$moSelectnoWrap->addObjFWDItem($moItemF);
		$moPanel->addObjFWDView($moSelectnoWrap);
		$moColumnsPropVar = new FWDVariable();
		$moColumnsPropVar->setAttrName("{$this->csName}_columns_prop_var");
		$moColumnsPropVar->setAttrValue($msVarCode);
		$moPanel->addObjFWDView($moColumnsPropVar);
		//st tooltip conte�do
		if($this->cbIsIE)
		$moStObs = new FWDStaticGrid(new FWDBox(135,117,130,120));
		else
		$moStObs = new FWDStaticGrid(new FWDBox(135,70,130,120));

		$moStObs->setValue(FWDLanguage::getPHPStringValue('fwdgrid_pref_width_obs', "* '0' = largura autom�tica"));
		$moStObs->setAttrClass($this->csCSSClass . $this->csClassStPopupPreferences);
		$moPanel->addObjFWDView($moStObs);


		return $moPanel;
	}

	/**
	 * Cria os objetos do panel de edi��o das propriedades das colunas da grid
	 *
	 * <p>Cria os objetos do panel de edi��o das propriedades das colunas da grid.</p>
	 * @access public
	 * @return Objeto FWDPanel
	 */
	public function getCodePanelColumnShow(){
		$moPanel = new FWDPanel(new FWDBox(10,10,150,390));
		$moPanel->setAttrClass($this->csCSSClass . $this->csClassPanelPopupPreferences);
		//st do titulo do panel
		$moStP = new FWDStatic(new FWDBox(0,0,20,390));
		$moStP->setAttrValue(FWDLanguage::getPHPStringValue('fwdgrid_pref_column_show_title','Colunas Exibidas e Ordem de Exibi��o entre elas'));
		$moStP->setAttrClass($this->csCSSClass . $this->csClassTitlePanel);
		$moStP->setAttrVertical('middle');
		$moStP->setAttrMarginLeft(10);
		$moPanel->addObjFWDView($moStP,'title');
		//select das colunas n�o exibidas
		$moSelectCol = new FWDSelect(new FWDBox(10,50,90,150));
		$moSelectCol->setAttrName("{$this->csName}_sel_col_notshow");
		$moSelectCol->setAttrSize(5);
		$moSelColEvent = new FWDClientEvent('onClick');
		$moSelColEvent->setAttrValue("objManagerGrifPref.selShowOnClick(\"{$this->csName}_sel_col_notshow\"); ");
		$moSelectCol->addObjFWDEvent($moSelColEvent);

		//select das colunas que ser�o exibidas
		$moSelectColShow = new FWDSelect(new FWDBox(200,50,90,150));
		$moSelectColShow->setAttrSize(5);
		$moSelectColShow->setAttrName("{$this->csName}_sel_col_show");
		$moSelColShowEvent = new FWDClientEvent('onClick');
		$moSelColShowEvent->setAttrValue("objManagerGrifPref.selShowOnClick(\"{$this->csName}_sel_col_show\"); ");
		$moSelectColShow->addObjFWDEvent($moSelColShowEvent);
		$moSelColShowEvent2 = new FWDClientEvent('onchange');
		$moSelColShowEvent2->setAttrValue("objManagerGrifPref.selShowOnClick(\"{$this->csName}_sel_col_show\"); ");
		$moSelectColShow->addObjFWDEvent($moSelColShowEvent2);

		$maAuxDisplayCol = array();
		$msAuxDisplayColEdit = '';
		foreach($this->caOrderColumnShow as $miI){
			$moItem = new FWDItem();
			$moItem->setAttrKey($miI);
			$moItem->setValue($this->getColumnName($this->caColumns[$miI],$miI));
			$moSelectColShow->addObjFWDItem($moItem);
			$maAuxDisplayCol[$miI]=$miI;
		}
		for($miI=0;$miI<count($this->caColumns);$miI++){
			if(!(isset($maAuxDisplayCol[$miI])))
			if(($this->caColumns[$miI]->getAttrDisplay()==false)&&($this->caColumns[$miI]->getAttrEdit())){
				$moItem = new FWDItem();
				$moItem->setAttrKey($miI);
				$moItem->setValue($this->getColumnName($this->caColumns[$miI],$miI));
				$moSelectCol->addObjFWDItem($moItem);
			}
			if($this->caColumns[$miI]->getAttrEdit()==true)
			$msAux = $miI . ':1';
			else
			$msAux = $miI . ':2';
			if($msAuxDisplayColEdit == '')
			$msAuxDisplayColEdit .= $msAux;
			else
			$msAuxDisplayColEdit .= ';'.$msAux;
		}
		$moPanel->addObjFWDView($moSelectCol);
		$moPanel->addObjFWDView($moSelectColShow);
		//variable que ir� guardar o c�digo de quais colunas podem ser edit�veis ou n�o
		$moColEditVar = new FWDVariable();
		$moColEditVar->setAttrName("{$this->csName}_var_col_editable");
		$moColEditVar->setValue($msAuxDisplayColEdit);
		$moPanel->addObjFWDView($moColEditVar);
		//st colunas da grid
		$moStColGrid = new FWDStaticGrid(new FWDBox(10,30,20,150));
		$moStColGrid->setValue(FWDLanguage::getPHPStringValue('fwdgrid_pref_grid_columns_title','Colunas da grid:'));
		$moStColGrid->setAttrClass($this->csCSSClass . $this->csClassStPopupPreferences);
		$moPanel->addObjFWDView($moStColGrid);
		//st linhas por p�gina
		$moStColShow = new FWDStaticGrid(new FWDBox(200,30,20,150));
		$moStColShow->setValue(FWDLanguage::getPHPStringValue('fwdgrid_pref_columns_show','Colunas exibidas:'));
		$moStColShow->setAttrClass($this->csCSSClass . $this->csClassStPopupPreferences);
		$moPanel->addObjFWDView($moStColShow);
		//VB adicionar
		$moVBRight = new FWDViewButton(new FWDBox(170,70,20,20));
		$moVBRight->setAttrName($this->csName ."btRightShow");
		$moVBRight->setAttrClass($this->csClassVBRight);
		$moRightEvent = new FWDClientEvent('onClick');
		$moRightEvent->setAttrValue("objManagerGrifPref.moveValueSelMod(\"{$this->csName}_sel_col_notshow\",\"{$this->csName}_sel_col_show\");");
		$moVBRight->addObjFWDEvent($moRightEvent);
		$moPanel->addObjFWDView($moVBRight);
		//VB remover
		$moVBLeft = new FWDViewButton(new FWDBox(170,95,20,20));
		$moVBLeft->setAttrName($this->csName . "btLeftShow");
		$moVBLeft->setAttrClass($this->csClassVBLeft);
		$moLeftEvent = new FWDClientEvent('onClick');
		$moLeftEvent->setAttrValue("objManagerGrifPref.moveValueSelMod(\"{$this->csName}_sel_col_show\",\"{$this->csName}_sel_col_notshow\");");
		$moVBLeft->addObjFWDEvent($moLeftEvent);
		$moPanel->addObjFWDView($moVBLeft);
		//VB mover p/ cima
		$moVBTop = new FWDViewButton(new FWDBox(355,70,20,20));
		$moVBTop->setAttrClass($this->csClassVBTop);
		$moUpEvent = new FWDClientEvent('onClick');
		$moUpEvent->setAttrValue("objManagerGrifPref.moveUp('{$this->csName}_sel_col_show');");
		$moVBTop->addObjFWDEvent($moUpEvent);
		$moPanel->addObjFWDView($moVBTop);
		//VB mover p/ cima
		$moVBBotton = new FWDViewButton(new FWDBox(355,95,20,20));
		$moVBBotton->setAttrClass($this->csClassVBBotton);
		$moDownEvent = new FWDClientEvent('onClick');
		$moDownEvent->setAttrValue("objManagerGrifPref.moveDown('{$this->csName}_sel_col_show');");
		$moVBBotton->addObjFWDEvent($moDownEvent);
		$moPanel->addObjFWDView($moVBBotton);
		return $moPanel;
	}

	/**
	 * Cria os objetos do panel de ordena��o das colunas da grid
	 *
	 * <p>Cria os objetos do panel de ordena��o das colunas da grid.</p>
	 * @access public
	 * @return Objeto FWDPanel
	 */
	public function getCodePanelColumnOrder(){
		$moPanel = new FWDPanel(new FWDBox(10,170,150,390));
		$moPanel->setAttrClass($this->csCSSClass . $this->csClassPanelPopupPreferences);
		//st do titulo do panel
		$moStP = new FWDStatic(new FWDBox(0,0,20,390));
		$moStP->setAttrValue(FWDLanguage::getPHPStringValue('fwdgrid_pref_column_order_title','Ordenamento das colunas da grid'));
		$moStP->setAttrClass($this->csCSSClass . $this->csClassTitlePanel);
		$moStP->setAttrVertical('middle');
		$moStP->setAttrMarginLeft(10);
		$moPanel->addObjFWDView($moStP,'title');
		//select das colunas n�o exibidas
		$moSelectCol = new FWDSelect(new FWDBox(10,50,60,150));
		$moSelectCol->setAttrName("{$this->csName}_sel_col_notorder");
		$moSelectCol->setAttrSize(5);
		//select das colunas que ser�o exibidas
		$moSelectColShow = new FWDSelect(new FWDBox(200,50,60,150));
		$moSelectColShow->setAttrSize(5);
		$moSelectColShow->setAttrName("{$this->csName}_sel_col_order");

		$mbAchou = false;
		$maOrder = explode(":",$this->csOrder);
		if(($maOrder))
		foreach($maOrder as $msColOrder){
			$msColOrder=trim($msColOrder);
			if($msColOrder!=""){
				$miColumn="";
				$msChar = substr(trim($msColOrder),0,1);
				if(($msChar=="+")||($msChar=="-"))// ascendente
				$miColumn = substr(trim($msColOrder),1);
				else//assumindo que soh veio um n�mero , ordena��o ascendente
				$miColumn = trim($msColOrder);
				if($this->caColumns[$miColumn-1]->getOrderBy()!=''){
					$mbAchou=true;
					$moItem = new FWDItem();
					$moItem->setAttrKey($miColumn);
					$moItem->setValue($this->getColumnName($this->caColumns[$miColumn-1],$miColumn-1));
					$moSelectColShow->addObjFWDItem($moItem);
				}
			}
		}

		$msOrderColumnData = '';
		for($miI=0;$miI<$this->ciColumnsCount;$miI++){
			//item do select p/ cada coluna
			if(($this->caColumns[$miI]->getAttrEdit())&&($this->caColumns[$miI]->getAttrCanOrder())){
				$miColumnId = $miI+1;
				$moItem = new FWDItem();
				$moItem->setAttrKey($miColumnId);
				$moItem->setValue($this->getColumnName($this->caColumns[$miI],$miColumnId));
				if($this->caColumns[$miI]->getOrderBy()!=''){
					$msOrderBy = $this->caColumns[$miI]->getOrderBy()=='-'?'-':'+';
					if($msOrderColumnData=='')
					$msOrderColumnData .= $miColumnId .':'. $msOrderBy;
					else
					$msOrderColumnData .= ';' . $miColumnId .':'. $msOrderBy;
					if(!$mbAchou)
					$moSelectColShow->addObjFWDItem($moItem);
				}else
				$moSelectCol->addObjFWDItem($moItem);
				unset($moItem);
			}
		}
		//evento que trata dos valores de ordenamento das colunas da grid
		$moOrderEvent = new FWDClientEvent('onClick');
		$moOrderEvent->setAttrValue("objManagerGrifPref.orderColClick();");
		$moSelectColShow->addObjFWDEvent($moOrderEvent);

		$moPanel->addObjFWDView($moSelectCol);
		$moPanel->addObjFWDView($moSelectColShow);
		//st colunas da grid
		$moStColGrid = new FWDStaticGrid(new FWDBox(10,30,20,150));
		$moStColGrid->setValue(FWDLanguage::getPHPStringValue('fwdgrid_pref_columns_grid','Colunas da grid:'));
		$moStColGrid->setAttrClass($this->csCSSClass . $this->csClassStPopupPreferences);
		$moPanel->addObjFWDView($moStColGrid);
		//st linhas por p�gina
		$moStColShow = new FWDStaticGrid(new FWDBox(200,30,20,150));
		$moStColShow->setValue(FWDLanguage::getPHPStringValue('fwdgrid_pref_column_order','Ordenamento:'));
		$moStColShow->setAttrClass($this->csCSSClass . $this->csClassStPopupPreferences);
		$moPanel->addObjFWDView($moStColShow);
		//VB adicionar
		$moVBRight = new FWDViewButton(new FWDBox(170,55,20,20));
		$moVBRight->setAttrClass($this->csClassVBRight);
		$moRightEvent = new FWDClientEvent('onClick');
		$moRightEvent->setAttrValue("objManagerGrifPref.orderColClickAdd();objManagerGrifPref.moveValueSel(\"{$this->csName}_sel_col_notorder\",\"{$this->csName}_sel_col_order\");");
		$moVBRight->addObjFWDEvent($moRightEvent);
		$moPanel->addObjFWDView($moVBRight);
		//VB remover
		$moVBLeft = new FWDViewButton(new FWDBox(170,80,20,20));
		$moVBLeft->setAttrClass($this->csClassVBLeft);
		$moLeftEvent = new FWDClientEvent('onClick');
		$moLeftEvent->setAttrValue("objManagerGrifPref.orderColClickRemove();objManagerGrifPref.moveValueSel(\"{$this->csName}_sel_col_order\",\"{$this->csName}_sel_col_notorder\");");
		$moVBLeft->addObjFWDEvent($moLeftEvent);
		$moPanel->addObjFWDView($moVBLeft);
		//VB mover p/ cima
		$moVBTop = new FWDViewButton(new FWDBox(355,55,20,20));
		$moVBTop->setAttrClass($this->csClassVBTop);
		$moUpEvent = new FWDClientEvent('onClick');
		$moUpEvent->setAttrValue("objManagerGrifPref.moveUp('{$this->csName}_sel_col_order');");
		$moVBTop->addObjFWDEvent($moUpEvent);
		$moPanel->addObjFWDView($moVBTop);
		//VB mover p/ cima
		$moVBBotton = new FWDViewButton(new FWDBox(355,80,20,20));
		$moVBBotton->setAttrClass($this->csClassVBBotton);
		$moDownEvent = new FWDClientEvent('onClick');
		$moDownEvent->setAttrValue("objManagerGrifPref.moveDown('{$this->csName}_sel_col_order');");
		$moVBBotton->addObjFWDEvent($moDownEvent);
		$moPanel->addObjFWDView($moVBBotton);
		//st ordenamento da coluna
		$moStColOrder = new FWDStatic(new FWDBox(170,120,20,65));
		$moStColOrder->setValue('Ordem:');
		$moStColOrder->setAttrName("{$this->csName}_st_order_by");
		$moStColOrder->setAttrDisplay('false');
		$moStColOrder->setAttrClass($this->csCSSClass . $this->csClassStPopupPreferences);
		$moStColOrder->setAttrHorizontal('right');
		$moPanel->addObjFWDView($moStColOrder);
		//select do ColOrder da coluna selecionada
		$moSelectColOrder = new FWDSelect(new FWDBox(240,120,20,110));
		$moSelectColOrder->setAttrName("{$this->csName}_sel_order_column_by");
		$moSelectColOrder->setAttrDisplay('false');
		$moItemASC = new FWDItem();
		$moItemASC->setAttrKey('+');
		$moItemASC->setValue('Ascendente');
		$moSelectColOrder->addObjFWDItem($moItemASC);
		$moItemDESC = new FWDItem();
		$moItemDESC->setAttrKey('-');
		$moItemDESC->setValue('Descendente');
		$moSelectColOrder->addObjFWDItem($moItemDESC);
		$moPanel->addObjFWDView($moSelectColOrder);
		//variable que ir� guardar o c�digo do ordenamento da grid
		$moOrderColumnCode = new FWDVariable();
		$moOrderColumnCode->setAttrName("{$this->csName}_sel_order_column_data");
		$moOrderColumnCode->setValue($msOrderColumnData);
		$moPanel->addObjFWDView($moOrderColumnCode);
		return $moPanel;
	}

	/**
	 * Cria a popup de edi��o das preferencias da grid.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar a popup de edi��o das preferencias da grid.</p>
	 * @access public
	 * @return string C�digo HTML da popup de edi��o de preferencias da grid
	 */
	public function createHTMLPopup(){
		//dimens�es da div
		$miLeft = 0;
		$miHeight = 360;
		$miWidth = 720;
		$miTop = 21;

		$moBox = new FWDBox(20,50);
		//c�digo que � gerado por intera��o
		$msReturn ="";
		//panel principal da popup
		$moPanel = new FWDViewGroup(new FWDBox(0,-1,350,720));

		//objetos do panel de edi��o das colunas que ser�o exibidas
		$moPanel->addObjFWDView($this->getCodePanelColumnShow());
		//objetos do panel de edi��o da grid
		$moPanel->addObjFWDView($this->getCodePanelGridEdit());
		//objetos do panel de edi��o da ordena��o da grid pelas colunas
		$moPanel->addObjFWDView($this->getCodePanelColumnOrder());
		//objetos do panel de edi��o da coluna
		$moPanel->addObjFWDView($this->getCodePanelColumnEdit());

		$moCleanEvent = new FWDClientEvent('onUnLoad');
		$moCleanEvent->setAttrValue("objManagerGrifPref.resetConfig();");
		$moPanel->addObjFWDEvent($moCleanEvent);

		//bot�o para fechar a janela
		$moBtClose = new FWDViewButton(new FWDBox(470,330,20,70));
		$moBtClose->setAttrClass($this->csCSSClass . $this->csClassViewButtonPopupEdit);
		$moCloseEvent = new FWDClientEvent('onClick');
		$moCloseEvent->setAttrValue("soPopUpManager.closePopUp('{$this->csName}_preferences');");
		$moBtClose->addObjFWDEvent($moCloseEvent);
		$moStaticClose = new FWDStaticGrid();
		$moStaticClose->setValue('Fechar');
		$moStaticClose->setObjFWDBox(new FWDBox(0,0,20,70));
		$moStaticClose->setAttrHorizontal('center');
		$moStaticClose->setAttrVertical('middle');
		$moStaticClose->setAttrClass($this->csCSSClass . $this->csClassStaticPopupPreferences);
		$moBtClose->addObjFWDView($moStaticClose);
		$moPanel->AddObjFWDView($moBtClose);
		//bot�o para salvar as modifica��es
		$moBtSave = new FWDViewButton(new FWDBox(555,330,20,70));
		$moBtSave->setAttrClass($this->csCSSClass . $this->csClassViewButtonPopupEdit);
		$moSaveEvent = new FWDClientEvent('onClick');
		$moSaveEvent->setAttrValue(" objManagerGrifPref.savePreferencesCodeInGrid();");
		$moBtSave->addObjFWDEvent($moSaveEvent);
		$moStaticSave = new FWDStaticGrid();
		$moStaticSave->setValue('Salvar');
		$moStaticSave->setObjFWDBox(new FWDBox(0,0,20,70));
		$moStaticSave->setAttrHorizontal('center');
		$moStaticSave->setAttrVertical('middle');
		$moStaticSave->setAttrClass($this->csCSSClass . $this->csClassStaticPopupPreferences);
		$moBtSave->addObjFWDView($moStaticSave);
		$moPanel->AddObjFWDView($moBtSave);
		//bot�o para resetar as modifica��es do usu�rio
		$moBtReset = new FWDViewButton(new FWDBox(640,330,20,70));
		$moBtReset->setAttrClass($this->csCSSClass . $this->csClassViewButtonPopupEdit);
		$moResetEvent = new FWDClientEvent('onClick');
		$moResetEvent->setAttrValue("objManagerGrifPref.resetConfig();");
		$moBtReset->addObjFWDEvent($moResetEvent);
		$moStaticReset = new FWDStaticGrid();
		$moStaticReset->setValue('Resetar');
		$moStaticReset->setObjFWDBox(new FWDBox(0,0,20,70));
		$moStaticReset->setAttrHorizontal('center');
		$moStaticReset->setAttrVertical('middle');
		$moStaticReset->setAttrClass($this->csCSSClass . $this->csClassStaticPopupPreferences);
		$moBtReset->addObjFWDView($moStaticReset);
		$moPanel->AddObjFWDView($moBtReset);

		return $this->scapeStringToAjax("<div name='edit_preferences' id='edit_preferences' class='{$this->csCSSClass}{$this->csClassDialogPopup}' style='top:$miTop;left:$miLeft;height:$miHeight;width:$miWidth;display:block;'>{$moPanel->draw()}</div>");
	}

	public function setAttrClassDivHeaderTitle($psClassDivHeaderTitle) {
		$this->csClassDivHeaderTitle = $psClassDivHeaderTitle;
	}

	public function setAttrClassStaticGridTitle($psClassStaticGridTitle) {
		$this->csClassStaticGridTitle = $psClassStaticGridTitle;
	}

	public function setAttrZebra0($psClassName) {
		$this->csClassBodyZebra0 = $psClassName;
	}

	public function setAttrZebra1($psClassName) {
		$this->csClassBodyZebra1 = $psClassName;
	}

	public function setAttrZebraOver($psClassName) {
		$this->csClassBodyZebraOver = $psClassName;
	}
}
?>