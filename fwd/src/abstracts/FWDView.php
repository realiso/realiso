<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe abstrata FWDView. Classe abstrata com os atributos b�sicos de um componente.
 *
 * <p>Classe abstrata que cont�m os atributos b�sicos de um componente.</p>
 * @abstract
 * @package FWD5
 * @subpackage abstracts
 */
abstract class FWDView extends FWDDrawing implements FWDCollectable {

	/**
	 * Alinhamento relativo ao pai
	 * @var string
	 * @access protected
	 */
	protected $csAlign = "";

	/**
	 * Nome do elemento
	 * @var string
	 * @access protected
	 */
	protected $csName = "";

	/**
	 * Css
	 * @var string
	 * @access protected
	 */
	protected $csClass = "";

	/**
	 * �ndice do elemento
	 * @var integer
	 * @access protected
	 */
	protected $ciTabIndex = 0;

	/**
	 * Atalho para acessar o elemento
	 * @var string
	 * @access protected
	 */
	protected $csAccessKey = "";

	/**
	 * Tag do elemento (permiss�es: define se o elemento deve ser desenhado ou n�o)
	 * @var string
	 * @access protected
	 */
	protected $csTag = "";

	/**
	 * Z-index para o elemento
	 * @var integer
	 * @access protected
	 */
	protected $ciZIndex = 1;

	/**
	 * Define se o elemento � redimension�vel
	 * @var boolean
	 * @access protected
	 */
	protected $cbResize = false;

	/**
	 * Define cor do contorno do elemento (utilizado para debug)
	 * @var string
	 * @access protected
	 */
	protected $csDebug = "";

	/**
	 * Define se o elemento deve ser exibido ou n�o
	 * @var boolean
	 * @access protected
	 */
	protected $cbDisplay = true;

	/**
	 * Define se o posicionamento do elemento � absoluto ou relativo
	 * @var string
	 * @access protected
	 */
	protected $csPosition = 'absolute';

	/**
	 * Objeto FWDBox que define o tamanho e a posi��o do elemento
	 * @var FWDBox
	 * @access protected
	 */
	protected $coBox = null;
		
	/**
	 * Valor do elemento
	 * @var FWDString
	 * @access protected
	 */
	protected $coString = null;

	/**
	 * Array de FWDEvent do elemento
	 * @var array
	 * @access protected
	 */
	protected $caEvent = array();

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDView.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites da �rea do elemento
	 */
	public function __construct($poBox = null) {
		$this->coBox = new FWDBox();
		$this->coString = new FWDString();
		$this->setAttrClass(get_class($this));
		if ($poBox) {
			$this->coBox->setAttrLeft($poBox->getAttrLeft());
			$this->coBox->setAttrTop($poBox->getAttrTop());
			$this->coBox->setAttrHeight($poBox->getAttrHeight());
			$this->coBox->setAttrWidth($poBox->getAttrWidth());
		}
	}

	/**
	 * Atribui valor, condicionalmente.
	 *
	 * <p>Atribui valor � view, condicionando atrav�s do segundo par�metro.
	 * Se force for FALSE, concatena o valor se a vari�vel tiver conte�do;
	 * se TRUE atribui mesmo que tenha conte�do (sobrescreve).</p>
	 * @access public
	 * @param string $psValue Valor a ser atribu�do
	 * @param boolean $pbForce For�ar o valor mesmo que j� tenha conte�do
	 */
	public function setValue($psValue, $pbForce = true) {
		$this->coString->setValue($psValue, $pbForce);
	}

	/**
	 * Seta o nome do elemento.
	 *
	 * <p>M�todo para setar o nome do elemento.</p>
	 * @access public
	 * @param string $psName Nome do elemento
	 */
	final public function setAttrName($psName) {
		$this->csName = $psName;
	}

	/**
	 * Seta a classe de CSS.
	 *
	 * <p>M�todo para setar a classe de CSS.</p>
	 * @access public
	 * @param string $psClass Classe
	 */
	final public function setAttrClass($psClass) {
		$this->csClass = $psClass;
	}

	/**
	 * Seta o alinhamento do elemento.
	 *
	 * <p>M�todo para setar o alinhamento do elemento.</p>
	 * @access public
	 * @param string $psAlign Alinhamento do elemento
	 */
	final public function setAttrAlign($psAlign) {
		$this->csAlign = $psAlign;
	}

	/**
	 * Seta o modo de debug do elemento.
	 *
	 * <p>M�todo para setar o modo de debug do elemento.</p>
	 * @access public
	 * @param string $psDebug Seta o modo de debug
	 */
	final public function setAttrDebug($psDebug) {
		$this->csDebug = (($psDebug=="false")?"":$psDebug);
	}

	/**
	 * Seta o atalho para acessar do elemento.
	 *
	 * <p>M�todo para setar o atalho para acessar do elemento.</p>
	 * @access public
	 * @param string $psAccessKey Seta o atalho do elemento
	 */
	final public function setAttrAccessKey($psAccessKey) {
		$this->csAccessKey = $psAccessKey;
	}

	/**
	 * Seta a tag do elemento.
	 *
	 * <p>M�todo para setar a tag do elemento.</p>
	 * @access public
	 * @param string $psTag Tag do elemento
	 */
	final public function setAttrTag($psTag) {
		$this->csTag = $psTag;
	}

	/**
	 * Seta o ordenamento do elemento.
	 *
	 * <p>M�todo para setar o ordenamento do elemento.</p>
	 * @access public
	 * @param integer $piTabIndex Seta o ordenamento do elemento
	 */
	final public function setAttrTabIndex($piTabIndex) {
		if (is_numeric($piTabIndex) && $piTabIndex >= 0)
		$this->ciTabIndex = $piTabIndex;
		else
		trigger_error("Invalid index value: '{$piTabIndex}'",E_USER_WARNING);
	}

	/**
	 * Seta o valor do elemento.
	 *
	 * <p>M�todo para setar o valor do elemento.</p>
	 * @access public
	 * @param FWDString $poString Seta o valor do elemento
	 */
	final public function setObjFWDString(FWDString $poString) {
		if (isset($this->coString)) {
			$this->coString->setValue($poString->getAttrString());
			$this->coString->setAttrId($poString->getAttrId());
		}
		else {
			$this->coString = $poString;
		}
	}

	/**
	 * Seta a �rea do elemento.
	 *
	 * <p>M�todo para setar a �rea do elemento.</p>
	 * @access public
	 * @param FWDBox $poBox Seta a �rea do elemento
	 */
	final public function setObjFWDBox(FWDBox $poBox) {
		$this->coBox = $poBox;
	}

	/**
	 * Seta um evento para o elemento.
	 *
	 * <p>M�todo para setar um evento para o elemento.</p>
	 * @access public
	 * @param FWDEvent $poEvent Evento
	 */
	final public function addObjFWDEvent(FWDEvent $poEvent) {
		$this->addEvent($poEvent);
	}


	/**
	 * Seta um evento para o elemento.
	 *
	 * <p>M�todo para setar um evento para o elemento.</p>
	 * @access public
	 * @param FWDEvent $poEvent Evento
	 */
	final public function setAttrEvent(FWDEvent $poEvent) {
		$this->caEvent[] = $poEvent;
	}


	/**
	 * Seta um evento para o elemento.
	 *
	 * <p>M�todo para setar um evento para o elemento.</p>
	 * @access public
	 * @param FWDEvent $poEvent Evento
	 */
	final public function addEvent(FWDEvent $poEvent) {
		$mskey = strtolower($poEvent->getAttrEvent());
		$msArrayKey = explode(":",$mskey);
		foreach($msArrayKey as $msKey){
			if(!isset($this->caEvent[$msKey]))
			$this->caEvent[$msKey] = new FWDEventHandler($msKey,$this->getAttrName());
			$this->caEvent[$msKey]->setAttrContent($poEvent);
		}
	}

	/**
	 * Seta o valor booleano do atributo de display.
	 *
	 * <p>Seta o valor booleano do atributo que define se o elemento
	 * deve ser exibido. Para o elemento ser exibido, o atributo deve
	 * conter o valor booleano TRUE. Para n�o ser exibido, o atributo deve
	 * conter o valor booleano FALSE.</p>
	 * @access public
	 * @param string $psDisplay Define se o elemento deve ser exibido ou n�o
	 */
	public function setAttrDisplay($psDisplay) {
		$this->cbDisplay = FWDWebLib::attributeParser($psDisplay,array("true"=>true, "false"=>false),"psDisplay");
	}

	/**
	 * Retorna o valor booleano do atributo de display.
	 *
	 * <p>Retorna o valor booleano TRUE se o elemento deve ser exibido.
	 * Retorna FALSE caso contr�rio.</p>
	 * @access public
	 * @return boolean Indica se o elemento deve ser exibido ou n�o
	 */
	final public function getAttrDisplay() {
		return $this->cbDisplay;
	}

	/**
	 * Seta o posicionamento do elemento
	 *
	 * <p>Seta o valor do atributo que define se o posicionamento do elemento �
	 * relativo ou absoluto.</p>
	 *
	 * @access public
	 * @param string $psPosition Define o posicionamento do elemento
	 */
	final public function setAttrPosition($psPosition){
		$this->csPosition = $psPosition;
	}

	/**
	 * Retorna o valor do atributo de position.
	 *
	 * <p>Retorna o valor do atributo position.</p>
	 *
	 * @access public
	 * @return string Valor do atributo position
	 */
	final public function getAttrPosition(){
		return $this->csPosition;
	}

	/**
	 * Seta o valor booleano do atributo de resize.
	 *
	 * <p>Seta o valor booleano do atributo que define se o elemento
	 * pode ser redimensionado. Para o elemento poder ser redimensionado, o atributo deve
	 * conter o valor booleano TRUE. Caso contr�rio, o atributo deve
	 * conter o valor booleano FALSE.</p>
	 * @access public
	 * @param string $psResize Define se o elemento pode ser redimensionado ou n�o
	 */
	final public function setAttrResize($psResize) {
		$this->cbResize = FWDWebLib::attributeParser($psResize,array("true"=>true, "false"=>false),"psResize");
	}

	/**
	 * Retorna o valor booleano do atributo de resize.
	 *
	 * <p>Retorna o valor booleano TRUE se o elemento pode ser redimensionado.
	 * Retorna FALSE caso contr�rio.</p>
	 * @access public
	 * @return boolean Indica se o elemento pode ser redimensionado ou n�o
	 */
	final public function getAttrResize() {
		return $this->cbResize;
	}

	/**
	 * Retorna o nome do elemento.
	 *
	 * <p>M�todo para retornar o nome do elemento.</p>
	 * @access public
	 * @return string Nome do elemento
	 */
	final public function getAttrName() {
		return $this->csName;
	}

	/**
	 * Retorna a classe de CSS.
	 *
	 * <p>M�todo para retornar a classe de CSS.</p>
	 * @access public
	 * @return string Classe de CSS
	 */
	final public function getAttrClass() {
		return $this->csClass;
	}

	/**
	 * Retorna os eventos do elemento.
	 *
	 * <p>M�todo para retornar os eventos do elemento.</p>
	 * @access public
	 * @return array Array de FWDEvent
	 */
	final public function getObjFWDEvent() {
		return $this->caEvent;
	}

	/**
	 * Retorna a Box do elemento.
	 *
	 * <p>M�todo para retornar a Box do elemento.</p>
	 * @access public
	 * @return FWDBox �rea do elemento
	 */
	final public function getObjFWDBox() {
		return $this->coBox;
	}

	/**
	 * Retorna o objeto de string do elemento.
	 *
	 * <p>M�todo para retornar o objeto de string do elemento.</p>
	 * @access public
	 * @return FWDString String do elemento
	 */
	final public function getObjFWDString() {
		return $this->coString;
	}

	/**
	 * Retorna o valor do elemento.
	 *
	 * <p>M�todo para retornar o valor do elemento.</p>
	 * @access public
	 * @return string Valor do elemento
	 */
	public function getValue() {
		return $this->coString->getAttrString();
	}

	/**
	 * Retorna o valor do debug do elemento.
	 *
	 * <p>M�todo para retornar o valor do debug do elemento.</p>
	 * @access public
	 * @return string Valor do debug do elemento
	 */
	final public function getAttrDebug() {
		return $this->csDebug;
	}

	/**
	 * Retorna o atalho para acessar o elemento.
	 *
	 * <p>M�todo para retornar o atalho para acessar o elemento.</p>
	 * @access public
	 * @return string Atalho do elemento
	 */
	final public function getAttrAccessKey() {
		return $this->csAccessKey;
	}

	/**
	 * Retorna a tag do elemento.
	 *
	 * <p>M�todo para retornar a tag do elemento.</p>
	 * @access public
	 * @return string Tag do elemento
	 */
	final public function getAttrTag() {
		return $this->csTag;
	}

	/**
	 * Retorna o ordenamento do elemento.
	 *
	 * <p>M�todo para retornar o ordenamento do elemento.</p>
	 * @access public
	 * @return integer Ordenamento do elemento
	 */
	final public function getAttrTabIndex() {
		return $this->ciTabIndex;
	}

	/**
	 * Retorna o alinhamento do elemento.
	 *
	 * <p>M�todo para retornar o alinhamento do elemento.</p>
	 *
	 * @access public
	 * @return string Alinhamento do elemento
	 */
	final public function getAttrAlign() {
		return $this->csAlign;
	}

	/**
	 * Alinha o elemeto.
	 *
	 * <p>M�todo para alinhar o elemento.</p>
	 * @access public
	 * @param string $psValue Tipo de alinhamento
	 * @param FWDBox $poBox Box
	 */
	final public function alignView($psValue, $poBox) {
		switch ($psValue) {
			case "left":
				$miX = 0;
				$miY = 0;
				$miHeight = $poBox->getAttrHeight() - 2;
				$miWidth = $this->coBox->getAttrWidth();
				break;
			case "right":
				$miX = $poBox->getAttrWidth() - $this->coBox->getAttrWidth();
				$miY = 0;
				$miHeight = $poBox->getAttrHeight() - 2;
				$miWidth = $this->coBox->getAttrWidth() - 2;
				break;
			case "top":
				$miX = 0;
				$miY = 0;
				$miHeight = $this->coBox->getAttrHeight();
				$miWidth = $poBox->getAttrWidth() - 2;
				break;
			case "bottom":
				$miX = 0;
				$miY = $poBox->getAttrHeight() - $this->coBox->getAttrHeight();
				$miHeight = $this->coBox->getAttrHeight() - 2;
				$miWidth = $poBox->getAttrWidth() - 2;
				break;
			case "parent":
				$miX = 0;
				$miY = 0;
				$miHeight = $poBox->getAttrHeight() - 2;
				$miWidth = $poBox->getAttrWidth() - 2;
				break;
			default:
				return;
		}
		$this->coBox->setAttrLeft($miX);
		$this->coBox->setAttrTop($miY);
		$this->coBox->setAttrHeight($miHeight);
		$this->coBox->setAttrWidth($miWidth);
	}

	/**
	 * Retorna as propriedades globais do elemento.
	 *
	 * <p>M�todo para retornar as propriedades globais do elemento.</p>
	 * @access public
	 * @return string Propriedades globais do elemento
	 */
	public function getGlobalProperty() {
		$msTabIndex = (($this->ciTabIndex)?"tabindex='{$this->ciTabIndex}'":"");
		$msAccessKey = (($this->csAccessKey)?"accesskey='{$this->csAccessKey}'":"");
		$msResize = (($this->cbResize)?"resize='true'":"");
		$msClass = (($this->csClass)?"class='{$this->csClass}'":"");
		$msId = $this->csName;

		$msValue = "name='{$msId}' id='{$msId}' {$msTabIndex} {$msAccessKey} {$msResize} {$msClass}";
		return $msValue;
	}

	/** Seta o valor do zIndex do elemento
	 *
	 * <p>Seta o valor do atributo zIndex do elemento</p>
	 * @access public
	 * @param integer $piZIndex Define o valor do zIndex do elemento
	 */
	final public function setAttrZIndex($piZIndex){
		$this->ciZIndex = $piZIndex;
	}

	/**
	 * Retorna o ZIndex do elemento.
	 *
	 * <p>M�todo para retornar o zIndex do elemento.</p>
	 * @access public
	 * @return Integer Propriedade zIndex do elemento
	 */
	final public function getAttrZIndex(){
		return $this->ciZIndex;
	}

	/**
	 * Retorna o estilo do elemento.
	 *
	 * <p>M�todo para retornar o estilo do elemento.</p>
	 * @access public
	 * @return string Estilo do elemento
	 */
	final public function getStyle(){
		$msStyle = '';
		$msStyle.= $this->coBox->draw();
		$msStyle.= "position:{$this->getAttrPosition()};z-index:{$this->getAttrZIndex()};";
		$msStyle.= $this->cbDisplay ? '' : 'display:none;';
		$msStyle.= $this->csDebug ? $this->debugView() : '';
		return $msStyle;
	}

	/**
	 * Retorna os eventos do elemento.
	 *
	 * <p>M�todo para retornar os eventos do elemento.</p>
	 * @access public
	 * @return string Eventos
	 */
	final public function getEvents() {
		$msEvents = "";
		foreach ($this->getObjFWDEvent() as $moEvent) {
			$msEvents .= $moEvent->render();
		}
		return $msEvents;
	}

	/**
	 * Retorna o array de eventos do elemento e o pr�prio elemento.
	 *
	 * <p>M�todo para retornar o array de eventos do elemento e o pr�prio elemento.</p>
	 * @access public
	 * @return array Eventos do elemento e elemento
	 */
	public function collect() {
		$maOut = array($this);
		if ($this->caEvent)
		foreach ($this->caEvent as $event)
		$maOut = array_merge($maOut,$event->getAttrContent());
		return $maOut;
	}

	/**
	 * Retorna o estilo para o modo de debug.
	 *
	 * <p>M�todo para retornar o estilo para o modo de debug.</p>
	 * @access private
	 * @return string Estilo do debug
	 */
	private function debugView() {
		$moRGBTable = FWDWebLib::getObject("goRGBTable");

		$msDebug = $this->getAttrDebug();
		$msColor = ((in_array($msDebug,$moRGBTable)||($msDebug[0]=="#"&&strlen($msDebug)==7))?$msDebug:"red");

		return "border-color:{$msColor};border-style:dashed;border-width:1px;";
	}

	/**
	 * Retorna uma representa��o da view como string para debug.
	 *
	 * <p>Retorna uma representa��o da view como string para debug.</p>
	 * @access public
	 * @return string representa��o da view como string
	 */
	public function __toString(){
		return "[".get_class($this)." ({$this->csName}): '{$this->getValue()}']";
	}

}
?>