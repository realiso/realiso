<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe abstrata FWDContainer.
 *
 * <p>Classe abstrata que possui um conjunto de FWDView.</p>
 * @package FWD5
 * @subpackage abstracts
 */
abstract class FWDContainer extends FWDView implements FWDCollectable {

	/**
	 * Array com as views que est�o dentro do container
	 * @var array
	 * @access protected
	 */
	protected $caContent = array();


	/**
	 * Adiciona um Drawing.
	 *
	 * <p>Adiciona um Drawing no container.</p>
	 * @access public
	 * @param FWDDrawing $poDrawing View
	 */
	public function addObjFWDDrawing(FWDDrawing $poDrawing){
		$this->caContent[] = $poDrawing;
	}

	/**
	 * Adiciona uma view.
	 *
	 * <p>Adiciona uma view no container.</p>
	 * @access public
	 * @param FWDView $poView View
	 * @param string $psTarget Alvo do evento
	 */
	public function addObjFWDView($poView,$psTarget = ""){
		if(isset($psTarget) && $psTarget=="movedor"){
			$moEvent = new FWDClientEvent('OnMouseDown','movedor',$this->getAttrName());
			$poView->addEvent($moEvent);
			$moEvent = new FWDClientEvent('OnMouseOver','changecursor',$poView->getAttrName(),'pointer');
			$poView->addEvent($moEvent);
		}
		$this->caContent[] = $poView;
	}

	/**
	 * Retorna todas as views do container.
	 *
	 * <p>M�todo para retornar todas as views do container.</p>
	 * @access public
	 * @return array Views
	 */
	public function getContent(){
		return $this->caContent;
	}

	/**
	 * Retorna um array com todos sub-elementos do container
	 *
	 * <p>Retorna um array contendo todos elementos contidos no container ou em
	 * seus descendentes.
	 * OBS: O array pode conter elementos repetidos, caso haja algum ItemController
	 * que perten�a ao Container e a um Controller simultaneamente.
	 * </p>
	 * @access public
	 * @return array Array de sub-elementos
	 */
	public function collect() {
		$maOut = array();
		$maOut = array_merge($maOut,parent::collect());
		foreach ($this->caContent as $moView) {
			if ($moView instanceof FWDCollectable) {
				$maItems = $moView->collect();
				if (count($maItems)>0)
				$maOut = array_merge($maOut,$maItems);
			}
			else {
				$maOut[] = $moView;
			}
		}
		return $maOut;
	}
}
?>