<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe abstrata FWDItemController. Implementa um Controlador de Itens.
 *
 * <p>Classe abstrata que implementa um Controlador de Itens (radioboxes, checkboxes).</p>
 * @package FWD5
 * @subpackage abstracts
 */
abstract class FWDItemController extends FWDView {

	/**
	 * Define o n�mero de items que fazem parte do agrupamento
	 * @var integer
	 * @access private
	 */
	private $ciItems = 0;

	/**
	 * Define se o Item est� marcado ou n�o
	 * @var boolean
	 * @access protected
	 */
	protected $cbCheck = false;

	/**
	 * Valor chave do Item
	 * @var string
	 * @access protected
	 */
	protected $csKey = "";

	/**
	 * Nome do Controlador de Itens
	 * @var string
	 * @access protected
	 */
	protected $csController = "";

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDItemController.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do Controlador de Itens
	 */
	public function __construct($poBox = null){
		parent::__construct($poBox);
	}

	/**
	 * Seta o valor chave do Item.
	 *
	 * <p>Seta o valor chave do Item.</p>
	 * @access public
	 * @param string $psValue Valor chave do Item
	 */
	public function setAttrKey($psValue){
		$this->csKey = $psValue;
	}

	/**
	 * Seta o valor booleano do atributo Check.
	 *
	 * <p>Seta o valor booleano do atributo Check.</p>
	 * @access public
	 * @param string $psCheck Define se o item est� marcado ou n�o
	 */
	public function setAttrCheck($psCheck){
		$this->cbCheck = FWDWebLib::attributeParser($psCheck,array("true"=>true, "false"=>false),"psCheck");
	}

	/**
	 * Seta o valor do Controlador.
	 *
	 * <p>Seta o valor do Controlador.</p>
	 * @access public
	 * @param string $psController Valor do Controlador
	 */
	public function setAttrController($psController){
		$this->csController = $psController;
	}

	/**
	 * Seta o n�mero de items que fazem parte do agrupamento.
	 *
	 * <p>M�todo para setar o n�mero de itens que fazem parte do agrupamento.</p>
	 * @access public
	 * @param integer $psItems N�mero de itens
	 */
	public function setItems($psItems){
		$this->ciItems = $psItems;
	}

	/**
	 * Retorna o valor chave do item.
	 *
	 * <p>Retorna o valor chave do item.</p>
	 * @access public
	 * @return string Valor chave do item
	 */
	public function getAttrKey(){
		return $this->csKey;
	}

	/**
	 * Retorna o n�mero de items que fazem parte do agrupamento.
	 *
	 * <p>M�todo para retornar o n�mero de items que fazem parte do agrupamento.</p>
	 * @access public
	 * @return integer N�mero de itens.
	 */
	public function getItems(){
		return $this->ciItems;
	}

	/**
	 * Retorna o valor do Controlador de Itens.
	 *
	 * <p>Retorna o valor do Controlador de Itens.</p>
	 * @access public
	 * @return string Valor do Controlador de Itens.
	 */
	public function getAttrController(){
		return $this->csController;
	}

	/**
	 * Retorna o valor booleano do atributo Check.
	 *
	 * <p>Retorna o valor booleano do atributo Check.</p>
	 * @access public
	 * @return boolean Indica se o item est� marcado ou n�o
	 */
	public function getAttrCheck(){
		return $this->cbCheck;
	}

	public function execute() {
		if(($moController = FWDWebLib::getObject($this->csController)))
		$moController->addObjFWDItemController($this);
		else
		trigger_error("Invalid controller name: '{$this->csController}'",E_USER_WARNING);
	}
}
?>