<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe abstrata FWDReportDrawing.
 *
 * <p>Classe abstrata para objetos que possam ser desenhados em relat�rios.</p>
 * @package FWD5
 * @subpackage abstracts
 */
abstract class FWDReportDrawing {

	/**
	 * Objeto que escreve o relat�rio
	 * @var FWDReportWriter
	 * @access protected
	 */
	protected $coWriter = null;

	/**
	 * Desenha o cabe�alho do objeto.
	 *
	 * <p>M�todo para desenhar o cabe�alho do objeto.</p>
	 * @access public
	 */
	protected function drawHeader() {}

	/**
	 * Desenha o corpo do objeto.
	 *
	 * <p>M�todo para desenhar o corpo do objeto.</p>
	 * @access public
	 */
	protected function drawBody() {}

	/**
	 * Desenha o rodap� do objeto.
	 *
	 * <p>M�todo para desenhar o rodap� do objeto.</p>
	 * @access public
	 */
	protected function drawFooter() {}

	/**
	 * Desenha o relat�rio.
	 *
	 * <p>M�todo para desenhar o relat�rio.</p>
	 * @access public
	 */
	public function draw() {
		$this->drawHeader();
		$this->drawBody();
		$this->drawFooter();
		$this->coWriter->generateReport();
	}
}
?>
