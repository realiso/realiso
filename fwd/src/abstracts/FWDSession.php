<?
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDSession. Controla sess�o e timeout.
 *
 * <p>Classe que controla sess�o e timeout.</p>
 *
 * @package FWD5
 * @subpackage abstracts
 */
abstract class FWDSession{

	/**
	 * Id do usu�rio logado
	 * @var string
	 * @access protected
	 */
	protected $csUserId = '';

	/**
	 * Endere�o do usu�rio logado
	 * @var string
	 * @access protected
	 */
	protected $csRemoteAddress = '';

	/**
	 * Browser do usu�rio logado
	 * @var string
	 * @access protected
	 */
	protected $csUserAgent = '';

	/**
	 * Segundos para expirar a sess�o. Zero significa que n�o expira.
	 * @var integer
	 * @access protected
	 */
	protected $ciTimeout = 0;

	/**
	 * Tempo inicial, no formato de timestamp do Unix, para checar timeout.
	 * @var integer
	 * @access protected
	 */
	protected $ciStartTimestamp;

	/**
	 * Indica se o usu�rio est� logado
	 * @var boolean
	 * @access protected
	 */
	protected $cbLoggedIn = false;

	/**
	 * URL da p�gina de login
	 * @var string
	 * @access protected
	 */
	protected $csLoginUrl = '';

	/**
	 * Id da sess�o
	 * @var string
	 * @access protected
	 */
	protected $csId = '';

	/**
	 * Array de atributos da sess�o
	 * @var array
	 * @access protected
	 */
	protected $caAttributes = array();

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDSession.</p>
	 * @access public
	 * @param string $psId Id da sess�o
	 */
	public function __construct($psId){
		$this->csId = $psId;
		$this->csRemoteAddress = $this->getRemoteAddress();
		$this->csUserAgent = $this->getUserAgent();
		$this->resetTimeout();
	}

	/**
	 * Seta o id do usu�rio
	 *
	 * <p>Seta o id do usu�rio.</p>
	 * @access public
	 * @param string $psUserId Id do usu�rio
	 */
	public function setUserId($psUserId){
		$this->csUserId = $psUserId;
	}

	/**
	 * Retorna o id do usu�rio
	 *
	 * <p>Retorna o id do usu�rio.</p>
	 * @access public
	 * @return string Id do usu�rio
	 */
	public function getUserId(){
		return $this->csUserId;
	}

	/**
	 * Reseta o tempo inicial para timeout
	 *
	 * <p>Reseta o tempo inicial para timeout. Deve ser chamado a cada refresh.</p>
	 * @access public
	 */
	public function resetTimeout(){
		$this->ciStartTimestamp = time();
	}

	/**
	 * M�todo para autenticar o usu�rio
	 *
	 * <p>M�todo para autenticar o usu�rio.</p>
	 * @access public
	 * @param string $psUser Login do usu�rio
	 * @param string $psPassword Senha do usu�rio
	 * @return boolean Indica se o usu�rio foi autenticado ou n�o
	 */
	abstract public function login($psUser,$psPassword = "");

	/**
	 * Destr�i a sess�o
	 *
	 * <p>Destr�i a sess�o</p>
	 * @access public
	 */
	public function destroySession(){
		$weblib_instance = FWDWebLib::getInstance();
		$weblib_instance->destroySession($this->getId());
	}

	/**
	 * Desloga o usu�rio.
	 *
	 * <p>Destr�i a sess�o e redireciona para a p�gina de login.</p>
	 * @access public
	 */
	public function logout(){
		$this->cbLoggedIn = false;
		$this->destroySession();
		if($this->csLoginUrl) header("Location: {$this->csLoginUrl}");
	}

	/**
	 * Checa se o usu�rio est� logado
	 *
	 * <p>Checa se o usu�rio est� logado.</p>
	 * @access public
	 * @return boolean Indica se o usu�rio est� logado
	 */
	public function isLoggedIn(){
		return ($this->cbLoggedIn && $this->checkSession());
	}

	/**
	 * Seta o atributo loggedIn
	 *
	 * <p>Seta o atributo loggedIn, que indica se o usu�rio est� logado.</p>
	 * @access public
	 * @param boolean $pbLoggedIn Indica se o usu�rio est� logado
	 */
	public function setLoggedIn($pbLoggedIn){
		$this->cbLoggedIn = $pbLoggedIn;
	}

	/**
	 * Verifica se a sess�o expirou
	 *
	 * <p>Verifica se a sess�o expirou.</p>
	 * @access public
	 * @return boolean True se a sess�o expirou, falso caso contr�rio
	 */
	public function timeout(){
		if($this->ciTimeout > 0){
			$miElapsedTime = time() - $this->ciStartTimestamp;
			return $miElapsedTime > $this->ciTimeout;
		}else{
			return false;
		}
	}

	/**
	 * Seta a URL da p�gina de login
	 *
	 * <p>Seta a URL da p�gina de login.</p>
	 * @access public
	 * @param string $psLoginUrl URL da p�gina de login
	 */
	public function setLoginUrl($psLoginUrl){
		$this->csLoginUrl = $psLoginUrl;
	}

	/**
	 * Seta o tempo para expirar a sess�o
	 *
	 * <p>Seta o tempo para expirar a sess�o.</p>
	 * @access public
	 * @param integer $piTimeout Tempo para expirar a sess�o em segundos
	 */
	public function setTimeout($piTimeout){
		$this->ciTimeout = $piTimeout;
	}

	/**
	 * Retorna o id da sess�o
	 *
	 * <p>Retorna o id da sess�o.</p>
	 * @access public
	 * @return string Id da sess�o
	 */
	public function getId(){
		return $this->csId;
	}

	/**
	 * Retorna o endere�o remoto atual
	 *
	 * <p>Retorna o endere�o remoto atual.</p>
	 * @access public
	 * @return string Endere�o remoto atual
	 */
	public function getRemoteAddress(){
		$msIp = '';
		if(getenv("HTTP_X_FORWARDED_FOR")) $msIp.= getenv( "HTTP_X_FORWARDED_FOR")." ";
		if(getenv( "HTTP_CLIENT_IP"))      $msIp.= getenv( "HTTP_CLIENT_IP")." ";
		if(getenv( "REMOTE_ADDR"))         $msIp.= getenv( "REMOTE_ADDR");
		return $msIp;
	}

	/**
	 * Retorna o browser atual
	 *
	 * <p>Retorna o browser atual.</p>
	 * @access protected
	 * @return string Browser atual
	 */
	protected function getUserAgent(){
		$msUserAgent = getenv("HTTP_USER_AGENT");
		if($msUserAgent) return $msUserAgent;
		else return '';
	}

	/**
	 * Verifica se o usu�rio atual � o mesmo do momento da cria��o do objeto
	 *
	 * <p>Verifica se o usu�rio atual � o mesmo do momento da cria��o do objeto.</p>
	 * @access public
	 * @return boolean Indica se o usu�rio � o mesmo
	 */
	public function verifyUser(){
		if($this->csRemoteAddress==$this->getRemoteAddress() && $this->csUserAgent==$this->getUserAgent()){
			return true;
		}else{
			return false;
		}
	}

	/**
	 * Checa se a sess�o est� ok
	 *
	 * <p>Checa se a sess�o est� ok, ou seja, se ela n�o expirou e o usu�rio
	 * continua o mesmo.</p>
	 * @access public
	 */
	public function checkSession(){
		return ($this->verifyUser() && !$this->timeout());
	}

	/**
	 * Adiciona um atributo � sess�o
	 *
	 * <p>Adiciona um atributo � sess�o. Os nomes dos atributos adicionados dessa
	 * maneira s�o case insensitive.</p>
	 * @access public
	 * @param string $psName Nome do atributo
	 */
	public function addAttribute($psName){
		$psName = strtolower($psName);
		if(isset($this->caAttributes[$psName])){
			//trigger_error("Cannot add session attribute. Name '$psName' already in use.",E_USER_WARNING);
			return false;
		}else{
			$this->caAttributes[$psName] = '';
			return true;
		}
	}

	/**
	 * Verifica se um atributo existe.
	 *
	 * <p>M�todo para verificar se um atributo existe.</p>
	 * @access public
	 * @param string $psName Nome do atributo
	 * @return boolean Verdairo ou falso
	 */
	public function attributeExists($psName) {
		$psName=strtolower($psName);
		if(isset($this->caAttributes[$psName]))
		return true;
		else
		return false;
	}

	/**
	 * Limpa um atributo da sess�o.
	 *
	 * <p>M�todo para limpar um atributo da sess�o.</p>
	 * @access public
	 * @param string $psAttribute Nome do atributo
	 */
	public function cleanAttribute($psAttribute) {
		$psAttribute = strtolower($psAttribute);
		$this->caAttributes[$psAttribute] = "";
	}

	/**
	 * Apaga um atributo da sess�o.
	 *
	 * <p>M�todo para apagar um atributo da sess�o.</p>
	 * @access public
	 * @param string $psAttribute Nome do atributo
	 */
	public function deleteAttribute($psAttribute) {
		$psAttribute = strtolower($psAttribute);
		unset($this->caAttributes[$psAttribute]);
	}

	/**
	 * Atualiza a sess�o fechando ela e reabrindo.
	 *
	 * <p>M�todo para for�ar a atualiza��o dos dados da sess�o.</p>
	 * @access public
	 */
	public function commit(){
		session_write_close();
		session_start();
	}

	/**
	 * M�todo que sobrecarrega m�todos setAttr e getAttr
	 *
	 * <p>M�todo de sobrecarga de m�todos. Esse m�todo � chamado pelo PHP quando
	 * um m�todo n�o definido � chamado. Usado para que atributos adicionados com
	 * o m�todo addAttribute() possam ser acessados por m�todos setAttr<atributo>()
	 * e getAttr<atributo>().</p>
	 * @access public
	 * @param string $psMethodName Nome do m�todo
	 * @param array paArguments Array com os par�metros passados para o m�todo
	 */
	public function __call($psMethodName,$paArguments){
		$msPrefix = substr($psMethodName,0,7);
		$msSuffix = strtolower(substr($psMethodName,7));
		if($msPrefix=='setAttr' || $msPrefix=='getAttr'){
			if(isset($this->caAttributes[$msSuffix])){
				if($msPrefix=='setAttr'){
					$this->caAttributes[$msSuffix] = $paArguments[0];
				}else{
					return $this->caAttributes[$msSuffix];
				}
			}else{
				trigger_error("Trying to ".substr($psMethodName,0,3)." undefined session attribute '$msSuffix'.",E_USER_WARNING);
			}
		}else{
			trigger_error("Calling undefined method '$psMethodName'.",E_USER_WARNING);
		}
	}

	/**
	 * M�todo para 'serializar' a classe de sess�o
	 *
	 * <p>Este m�todo � necess�rio para suportar um bug no PHP 5.0.x que n�o serializa
	 * automaticamente os objetos que est�o sendo armazenados na sess�o. Est� fun��o retorna
	 * para o PHP os atributos que ele deve considerar na serializa��o de um objeto desta classe</p>
	 * @access public
	 * @return array Nome dos atributos a serem considerados na serializa��o
	 */
	public function __sleep() {
		$maAttributes = (array)$this;
		return array_keys($maAttributes);
	}

	/**
	 * M�todo para 'deserializar' a classe de sess�o
	 *
	 * <p>Este m�todo � necess�rio para suportar um bug no PHP 5.0.x. Se esta fun��o
	 * n�o est� declarada o m�todo __call retona erro, pois o PHP tenta executar um
	 * m�todo com o nome __wakeup e ele n�o existe.</p>
	 * @access public
	 */
	public function __wakeup() {}

}
?>