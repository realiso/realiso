<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDIcon. Implementa �cones (imagens).
 *
 * <p>Classe simples para manipula��o de �cones (imagens, html tag 'img scr').</p>
 * @package FWD5
 * @subpackage abstracts
 */
abstract class FWDGridParameter {

	/**
	 * Armazena as colunas que ir�o ordenar a grid. formato:{+ ou -)numeroColuna}*
	 * (qdo existir mais de uma ocorr�ncia, elas ser�o separadas por ':') a ordem da string � importante
	 * @var string
	 * @access protected
	 */
	private $csOrderColumnsData;

	/**
	 * Armazena as colunas que ser�o exibidas na grid e a ordem de exibi��o delas na grid
	 * s�o separadas por ':' e a ordem do n�mero da coluna � importante (ser� a ordem de exibi��o da coluna na grid)
	 * @var string
	 * @access protected
	 */
	private $csOrderColumn = '';

	/**
	 * Armazena as propriedades da cada coluna da grid.
	 * formato: {numCol:width:noWrap;}*
	 * @var string
	 * @access protected
	 */
	private $csColumnsProp = '';

	/**
	 * Armazena o identificador do usu�rio que est� logado no sistema.
	 * @var string
	 * @access protected
	 */
	private $csUserId = '';

	/**
	 * Armazena a string serializada das preferencias do usu�rio
	 * formato: {numCol:width:noWrap;}*
	 * @var string
	 * @access protected
	 */
	private $csPreferenceValue = '';

	/**
	 * Armazena o n�mero de linhas por p�gina da grid
	 * @var integer
	 * @access protected
	 */
	private $ciRowsPerPage = '';

	/**
	 * Armazena a altura das linhas da grid
	 * @var integer
	 * @access protected
	 */
	private $ciRowsHeight = '';

	/**
	 * Armazena a coluna fonte de dados e a coluna alvo do tooltip da grid
	 * @var integer
	 * @access protected
	 */
	private $csToolTipCol = '';

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDGridParameter.</p>
	 * @access public
	 * @param string $psUserId Id do usu�rio logado no sistema
	 */
	public function __construct($psUserId) {
		$this->csUserId = $psUserId;
	}

	/**
	 * Carrega no objeto de preferencias a preferencia do usu�rio para a grid passada por par�metro.
	 *
	 * <p>Carrega no objeto de preferencias a preferencia do usu�rio para a grid passada por par�metro.
	 * A fonte dos dados depende de cada sistema, podendo ser BD, cookies, etc</p>
	 * @access protected
	 * @param string $psObjId Id da grid
	 */
	abstract protected function getPreference($psObjId);

	/**
	 * Salva os dados armazenados no objeto de preferencias do usu�rio.
	 *
	 * <p>Salva os dados armazenados no objeto de preferencias do usu�rio no destino definido no sistema.
	 * O destino dos dados depende de cada sistema, podendo ser BD, cookies, etc</p>
	 * @access protected
	 * @param string $psObjId Id da grid
	 * @param string $psData string serializada contendo as preferencias do usu�rio da grid
	 */
	abstract protected function updatePreference($psObjId,$psData);

	/**
	 * Deleta as preferencias do usu�rio para o objeto definido em $psObjId.
	 *
	 * <p>deleta as preferencias do usu�rio para o objeto definido em $psObjId do sistema de armazenamento definido no sistema.
	 * O meio de armazenamento dos dados depende de cada sistema, podendo ser BD, cookies, etc</p>
	 * @param string $psObjId Id da grid
	 * @access protected
	 */
	abstract protected function deletePreference($psObjId);

	/**
	 * Salva os dados armazenados no objeto de preferencias do usu�rio.
	 *
	 * <p>Salva os dados armazenados no objeto de preferencias do usu�rio no destino definido no sistema.
	 * O destino dos dados depende de cada sistema, podendo ser BD, cookies, etc</p>
	 * @access public
	 * @param string $psObjId Id da grid
	 */
	public function savePreferences($psObjId){
		$maAuxValue = array();
		$maAuxValue[] = $this->csOrderColumnsData;
		$maAuxValue[] = $this->csOrderColumn;
		$maAuxValue[] = $this->csColumnsProp;
		$maAuxValue[] = $this->ciRowsPerPage;
		$maAuxValue[] = $this->ciRowsHeight;
		$maAuxValue[] = $this->csToolTipCol;

		$this->UpdatePreference($psObjId, serialize($maAuxValue));
	}

	/**
	 * Carrega no objeto de preferencias a preferencia do usu�rio para a grid passada por par�metro.
	 *
	 * <p>Carrega no objeto de preferencias a preferencia do usu�rio para a grid passada por par�metro.
	 * A fonte dos dados depende de cada sistema, podendo ser BD, cookies, etc</p>
	 * @access protected
	 * @param string $psObjId Id da grid
	 */
	public function loadPrefereces($psObjId){
		$maAuxValue = FWDWebLib::unserializeString($this->getPreference($psObjId));
		$this->csOrderColumnsData = $maAuxValue[0];
		$this->csOrderColumn = $maAuxValue[1];
		$this->csColumnsProp = $maAuxValue[2];
		$this->ciRowsPerPage = $maAuxValue[3];
		$this->ciRowsHeight = $maAuxValue[4];
		$this->csToolTipCol = $maAuxValue[5];
	}

	/**
	 * Deleta as preferencias do usu�rio para o objeto definido em $psObjId.
	 *
	 * <p>deleta as preferencias do usu�rio para o objeto definido em $psObjId do sistema de armazenamento definido no sistema.
	 * O meio de armazenamento dos dados depende de cada sistema, podendo ser BD, cookies, etc</p>
	 * @param string $psObjId Id da grid
	 * @access protected
	 */
	public function resetPreferenses($psObjId){
		$this->deletePreference($psObjId);
	}

	/**
	 * Seta a ordem e quais colunas devem ser exibidas na grid.
	 *
	 * <p>Seta a ordem e quais colunas devem ser exibidas na grid.</p>
	 * @access public
	 * @param string $psOrderColumn ordem e quais colunas devem ser exibidas na grid.
	 */
	public function setOrderColumn($psOrderColumn){
		$this->csOrderColumn = $psOrderColumn;
	}

	/**
	 * Retorna a ordem de exibi��o das colunas da grid.
	 *
	 * <p>Retorna a ordem de exibi��o das colunas da grid</p>
	 * @access public
	 * @return string ordem de exibi��o  das colunas da grid
	 */
	public function getOrderColumn(){
		return $this->csOrderColumn;
	}

	/**
	 * Seta as propriedades das colunas da grid.
	 *
	 * <p>Seta as propriedades das colunas da grid.</p>
	 * @access public
	 * @param string $psColumnsProp propriedades das colunas da grid..
	 */
	public function setColumnsProp($psColumnsProp){
		$this->csColumnsProp = $psColumnsProp;
	}

	/**
	 * Retorna as propriedades das colunas da grid.
	 *
	 * <p>Retorna as propriedades das colunas da grid</p>
	 * @access public
	 * @return string propriedades das colunas da grid
	 */
	public function getColumnsProp(){
		return $this->csColumnsProp;
	}

	/**
	 * Seta as colunas q devem ordenar a grid e o tipo de ordenamento dos dados das colunas da grid.
	 *
	 * <p>Seta as colunas q devem ordenar a grid e o tipo de ordenamento dos dados das colunas da grid.</p>
	 * @access public
	 * @param string $psColumnOrder colunas q devem ordenar a grid e o tipo de ordenamento dos dados das colunas da grid
	 */
	public function setOrderColumnsData($psColumnOrder){
		$this->csOrderColumnsData = $psColumnOrder;
	}

	/**
	 * Retorna as colunas q devem ordenar a grid e o tipo de ordenamento dos dados das colunas da grid.
	 *
	 * <p>Retorna as colunas q devem ordenar a grid e o tipo de ordenamento dos dados das colunas da grid</p>
	 * @access public
	 * @return string colunas q devem ordenar a grid e o tipo de ordenamento dos dados das colunas da grid.
	 */
	public function getOrderColumnsData(){
		return $this->csOrderColumnsData;
	}

	/**
	 * Seta o n�mero de linhas por p�gina da grid.
	 *
	 * <p>Seta o n�mero de linhas por p�gina da grid.</p>
	 * @access public
	 * @param integer $piRowsPerPage n�mero de linhas por p�gina da grid
	 */
	public function setRowsPerPage($piRowsPerPage){
		$this->ciRowsPerPage = $piRowsPerPage;
	}

	/**
	 * Retorna o n�mero de linhas por p�gina da grid.
	 *
	 * <p>Retorna o n�mero de linhas por p�gina da grid.</p>
	 * @access public
	 * @return integer n�mero de linhas por p�gina da grid.
	 */
	public function getRowsPerPage(){
		return $this->ciRowsPerPage;
	}

	/**
	 * Seta a altura das linhas da grid.
	 *
	 * <p>Seta a altura das linhas da grid.</p>
	 * @access public
	 * @param integer $piRowsHeight altura das linhas da grid.
	 */
	public function setRowsHeight($piRowsHeight){
		$this->ciRowsHeight = $piRowsHeight;
	}

	/**
	 * Retorna a altura das linhas da grid.
	 *
	 * <p>Retorna a altura das linhas da grid..</p>
	 * @access public
	 * @return string altura das linhas da grid.
	 */
	public function getRowsHeight(){
		return $this->ciRowsHeight;
	}

	/**
	 * Seta as informa��es de tooltip da grid.
	 *
	 * <p>Seta as informa��es de tooltip da grid.</p>
	 * @access public
	 * @param integer $psToolTipCol informa��es de tooltip da grid.
	 */
	public function setToolTipCol($psToolTipCol){
		$this->csToolTipCol = $psToolTipCol;
	}

	/**
	 * Retorna as informa��es de tooltip da grid.
	 *
	 * <p>Retorna as informa��es de tooltip da grid.</p>
	 * @access public
	 * @return string informa��es de tooltip da grid.
	 */
	public function getToolTipCol(){
		return $this->csToolTipCol;
	}
}
?>