<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDStatic. Implementa texto est�tico.
 *
 * <p>Classe que implementa texto est�tico (html tag 'span').</p>
 * @package FWD5
 * @subpackage abstract
 */
abstract class FWDViewGrid extends FWDDrawing  implements FWDCollectable{

	/**
	 * Define o nome do static
	 * @var string
	 * @access protected
	 */
	protected $csName = '';

	/**
	 * Css
	 * @var string
	 * @access protected
	 */
	protected $csClass = "";

	/**
	 * Objeto FWDBox que define o tamanho e a posi��o do elemento
	 * @var FWDBox
	 * @access protected
	 */
	protected $coBox = null;

	/**
	 * Array de FWDEvent do elemento
	 * @var array
	 * @access protected
	 */
	protected $caEvent = array();

	/**
	 * Define se o elemento deve ser exibido ou n�o
	 * @var boolean
	 * @access protected
	 */
	protected $cbDisplay = true;

	/**
	 * Tag do elemento (permiss�es: define se o elemento deve ser desenhado ou n�o)
	 * @var string
	 * @access protected
	 */
	protected $csTag = '';

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDView.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites da �rea do elemento
	 */
	public function __construct($poBox = null) {
		$this->coBox = new FWDBox();
		$this->csClass = get_class($this);
		if ($poBox) {
			$this->coBox->setAttrLeft($poBox->getAttrLeft());
			$this->coBox->setAttrTop($poBox->getAttrTop());
			$this->coBox->setAttrHeight($poBox->getAttrHeight());
			$this->coBox->setAttrWidth($poBox->getAttrWidth());
		}
	}

	/**
	 * Seta o nome do elemento.
	 *
	 * <p>M�todo para setar o nome do elemento.</p>
	 * @access public
	 * @param string $psName Nome do elemento
	 */
	final public function setAttrName($psName) {
		$this->csName = $psName;
	}

	/**
	 * Seta a classe de CSS.
	 *
	 * <p>M�todo para setar a classe de CSS.</p>
	 * @access public
	 * @param string $psClass Classe
	 */
	final public function setAttrClass($psClass) {
		$this->csClass = $psClass;
	}

	/**
	 * Seta a �rea do elemento.
	 *
	 * <p>M�todo para setar a �rea do elemento.</p>
	 * @access public
	 * @param FWDBox $poBox Seta a �rea do elemento
	 */
	final public function setObjFWDBox(FWDBox $poBox) {
		$this->coBox = $poBox;
	}

	/**
	 * Retorna o nome do elemento.
	 *
	 * <p>M�todo para retornar o nome do elemento.</p>
	 * @access public
	 * @return string Nome do elemento
	 */
	final public function getAttrName() {
		return $this->csName;
	}

	/**
	 * Retorna a classe de CSS.
	 *
	 * <p>M�todo para retornar a classe de CSS.</p>
	 * @access public
	 * @return string Classe de CSS
	 */
	final public function getAttrClass() {
		return $this->csClass;
	}

	/**
	 * Retorna a Box do elemento.
	 *
	 * <p>M�todo para retornar a Box do elemento.</p>
	 * @access public
	 * @return FWDBox �rea do elemento
	 */
	final public function getObjFWDBox() {
		return $this->coBox;
	}

	/**
	 * Seta o valor booleano do atributo de display.
	 *
	 * <p>Seta o valor booleano do atributo que define se o elemento
	 * deve ser exibido. Para o elemento ser exibido, o atributo deve
	 * conter o valor booleano TRUE. Para n�o ser exibido, o atributo deve
	 * conter o valor booleano FALSE.</p>
	 * @access public
	 * @param string $psDisplay Define se o elemento deve ser exibido ou n�o
	 */
	final public function setAttrDisplay($psDisplay) {
		$this->cbDisplay = FWDWebLib::attributeParser($psDisplay,array("true"=>true, "false"=>false),"psDisplay");
	}

	/**
	 * Retorna o valor booleano do atributo de display.
	 *
	 * <p>Retorna o valor booleano TRUE se o elemento deve ser exibido.
	 * Retorna FALSE caso contr�rio.</p>
	 * @access public
	 * @return boolean Indica se o elemento deve ser exibido ou n�o
	 */
	final public function getAttrDisplay() {
		return $this->cbDisplay;
	}

	/**
	 * Retorna o valor do atributo tag
	 *
	 * <p>Retorna o valor do atributo tag.</p>
	 * @access public
	 * @return string Valor do atributo tag
	 */
	public function getAttrTag(){
		return $this->csTag;
	}

	/**
	 * Seta o valor do atributo tag
	 *
	 * <p>Seta o valor do atributo tag.</p>
	 * @access public
	 * @param string $psTag Novo valor do atributo
	 */
	public function setAttrTag($psTag){
		$this->csTag = $psTag;
	}

	/**
	 * Retorna os eventos do elemento.
	 *
	 * <p>M�todo para retornar os eventos do elemento.</p>
	 * @access public
	 * @return string Eventos
	 */
	final public function getEvents() {
		$msEvents = "";
		foreach ($this->caEvent as $moEvent) {
			$msEvents .= $moEvent->render();
		}
		return $msEvents;
	}

	/**
	 * Seta um evento para o elemento.
	 *
	 * <p>M�todo para setar um evento para o elemento.</p>
	 * @access public
	 * @param FWDEvent $poEvent Evento
	 */
	final public function addObjFWDEvent(FWDEvent $poEvent) {
		$mskey = $poEvent->getAttrEvent();
		$msArrayKey = explode(":",$mskey);
		foreach($msArrayKey as $msKey){
			if(!isset($this->caEvent[$msKey]))
			$this->caEvent[$msKey] = new FWDEventHandler($msKey,$this->getAttrName());
			$this->caEvent[$msKey]->setAttrContent($poEvent);
		}
	}

	/**
	 * Retorna o array de eventos do elemento e o pr�prio elemento.
	 *
	 * <p>M�todo para retornar o array de eventos do elemento e o pr�prio elemento.</p>
	 * @access public
	 * @return array Eventos do elemento e elemento
	 */
	public function collect() {
		$maOut = array($this);
		if ($this->caEvent)
		foreach ($this->caEvent as $moEvent)
		$maOut = array_merge($maOut,$moEvent->getAttrContent());
		return $maOut;
	}

}
?>