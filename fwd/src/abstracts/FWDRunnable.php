<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe abstrata FWDRunnable. Classe abstrata para cria��o de eventos pelo usu�rio.
 *
 * <p>Classe abstrata para cria��o de eventos pelo usu�rio.</p>
 * @abstract
 * @package FWD5
 * @subpackage abstracts
 */
abstract class FWDRunnable {

	/**
	 * Identificador do evento do usu�rio (e function do evento ajax)
	 * @var string
	 * @access private
	 */
	private $csIdentifier;

	/**
	 * Identificador do objeto alvo do evento do usu�rio
	 * @var string
	 * @access private
	 */
	private $csEventObj;

	/**
	 * Parametro passado pelo usu�rio ao evento
	 * @var mixed
	 * @access private
	 */
	private $cmParameter;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDRunnable. Seta o atributo Identifier
	 * do evento do usu�rio.</p>
	 * @access public
	 * @param string $psIdentifier Identificador do evento do usu�rio
	 * @param string $psTargetObj Objeto alvo
	 */
	public function __construct($psIdentifier, $psEventObj = "",$pmParameter = "") {
		$this->csIdentifier = $psIdentifier;
		$this->csEventObj = $psEventObj;
		$this->cmParameter = $pmParameter;
	}

	/**
	 * Evento criado pelo usu�rio (m�todo).
	 *
	 * <p>Evento criado pelo usu�rio. Deve ser implementado nas classes que
	 * estenderem a FWDRunnable. Deve existir um e somente um evento (i.e.,
	 * m�todo run() ) na classe que extender a FWDRunnable. Caso seja necess�rio
	 * criar mais eventos, � necess�rio criar novas classes.</p>
	 * @access public
	 */
	abstract public function run();

	/**
	 * Seta o identificador do evento do usu�rio.
	 *
	 * <p>Seta o identificador do evento do usu�rio.</p>
	 * @access public
	 * @param string $psIdentifier Identificador do evento do usu�rio
	 */
	public function setAttrIdentifier($psIdentifier) {
		$this->csIdentifier = $psIdentifier;
	}

	/**
	 * Retorna o identificador do evento do usu�rio.
	 *
	 * <p>Retorna o identificador do evento do usu�rio.</p>
	 * @access public
	 * @return string Identificador do evento do usu�rio
	 */
	public function getAttrIdentifier() {
		return $this->csIdentifier;
	}

	/**
	 * Seta o id do objeto alvo do envento do usu�rio.
	 *
	 * <p>Seta o id do objeto alvo do evento do usu�rio.</p>
	 * @access public
	 * @param string $psTargetObj Identificador do evento do usu�rio
	 */
	public function setAttrEventObj($psEventObj) {
		$this->csEventObj = $psEventObj;
	}

	/**
	 * Retorna o identificador do objeto alvo do evento do usu�rio.
	 *
	 * <p>Retorna o identificador do objeto alvo do evento do usu�rio.</p>
	 * @access public
	 * @return string Identificador do objeto alvo do evento do usu�rio
	 */
	public function getAttrEventObj() {
		return $this->csEventObj;
	}

	/**
	 * Seta o parametro passado para o evento.
	 *
	 * <p>Seta o o parametro passado para o evento pelo usu�rio.</p>
	 * @access public
	 * @param mixed $pmParameter Par�metro passado para o evento
	 */
	public function setParameter($pmParameter) {
		$this->cmParameter = $pmParameter;
	}

	/**
	 * Retorna o parametro passado para o evento.
	 *
	 * <p>Retorna o par�metro passado para o evento.</p>
	 * @access public
	 * @return string Parametro do evento
	 */
	public function getParameter() {
		return $this->cmParameter;
	}

}
?>