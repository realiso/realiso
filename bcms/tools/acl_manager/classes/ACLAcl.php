<?php
/**
 * ACL - ACL MANAGER
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ACLAcl.
 *
 * <p></p>
 * @package ACL
 * @subpackage classes
 */
class ACLAcl extends FWDDBTable {

  public function __construct(){
    parent::__construct('acl_acl');
    
    $this->csAliasId = 'acl_id';
    
    $this->coDataset->addFWDDBField(new FWDDBField('fkSystem'    ,'acl_system_id'  ,DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('pkAcl'       ,'acl_id'         ,DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('fkParent'    ,'acl_parent_id'  ,DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('sDescription','acl_description',DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField('sTag'        ,'acl_tag'        ,DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField('sName'       ,'acl_name'       ,DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField('nOrder'      ,'acl_order'      ,DB_NUMBER));
  }

  public static function deleteAll($piSystemId){
    $moAcl = new ACLAcl();
    $moAcl->createFilter($piSystemId,'acl_system_id');
    $moAcl->delete();
  }
  
  public static function getTagsFromSystem($piSystemId){
    $moAcl = new ACLAcl();
    $moAcl->createFilter($piSystemId,'acl_system_id');
    $moAcl->select();
    $maAcls = array();
    while($moAcl->fetch()){
      $maAcls[] = $moAcl->getFieldValue('acl_tag');
    }
    return $maAcls;
  }

}

?>