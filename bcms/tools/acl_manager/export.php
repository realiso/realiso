<?php

include_once 'include.php';
include_once $handlers_ref . 'QueryACLTree.php';

$siSystemId = $_GET['system'];

$soHandler = new QueryACLTree();
$soHandler->setSystem($siSystemId);
$soHandler->makeQuery();
$soHandler->executeQuery();

header('Cache-Control: ');// leave blank to avoid IE errors
header('Pragma: ');// leave blank to avoid IE errors
header('Content-type: text/enriched');
header('Content-Disposition: attachment; filename="acl_tree.xml"');

echo "<?xml version='1.0' encoding='ISO-8859-1'?>\n";
echo "<root>\n";
echo "<treebase name='ACL_profile_tree_base'>\n";
echo "  <box top='10' left='10' height='380' width='540'/>\n";

$siCurrentLevel = 0;
while($soHandler->fetch()){
  $siId = $soHandler->getFieldValue('acl_id');
  $siParentId = $soHandler->getFieldValue('acl_parent_id');
  $siParentId = ($siParentId?$siParentId:0);
  $siLevel = $soHandler->getFieldValue('acl_level');
  $ssTag = $soHandler->getFieldValue('acl_tag');
  $ssName = utf8_decode($soHandler->getFieldValue('acl_name'));
  $ssDescription = utf8_decode($soHandler->getFieldValue('acl_description'));
  
  while($siCurrentLevel >= $siLevel){
    if($siCurrentLevel==1){
      echo str_repeat('  ',$siCurrentLevel)."</tree>\n";
    }else{
      echo str_repeat('  ',$siCurrentLevel)."</treeajax>\n";
    }
    $siCurrentLevel--;
  }
  
  $siCurrentLevel = $siLevel;
  
  $ssIdent = str_repeat('  ',$siCurrentLevel);
  if($siLevel==1){
    echo $ssIdent."<tree name='tree_{$ssTag}' expanded='true'>\n";
  }else{
    echo $ssIdent."<treeajax name='tree_{$ssTag}'>\n";
    echo $ssIdent."  <checkbox controller='ACL_profile_check_controller' key='{$ssTag}'/>\n";
  }
  $ssIdent.= '  ';
  echo $ssIdent."<static>\n";
  echo $ssIdent."  <string id='tr_acl_{$ssTag}'>{$ssName}</string>\n";
  echo $ssIdent."  <tooltip showdelay='1000'>\n";
  echo $ssIdent."    <string id='to_acl_{$ssTag}'>{$ssDescription}</string>\n";
  echo $ssIdent."  </tooltip>\n";
  echo $ssIdent."</static>\n";
}
while($siCurrentLevel > 0){
  if($siCurrentLevel==1){
    echo str_repeat('  ',$siCurrentLevel)."</tree>\n";
  }else{
    echo str_repeat('  ',$siCurrentLevel)."</treeajax>\n";
  }
  $siCurrentLevel--;
}

echo "</treebase>\n";
echo "</root>";

?>