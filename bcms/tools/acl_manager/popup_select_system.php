<?

include_once 'include.php';
include_once $handlers_ref . 'QuerySelectSystem.php';

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $miNewSystemId = FWDWebLib::getObject('new_system')->getValue();
    $moHandler = new QuerySelectSystem();
    $moHandler->setExcludedIds(array($miNewSystemId));
    $moSelect = FWDWebLib::getObject('old_system');
    $moSelect->setQueryHandler($moHandler);
    $moSelect->populate();
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_select_system.xml');

?>