
function FWDItemList(psSelectName,psVariableName){
  var self = this;
  
  self.csSelectName = psSelectName;
  self.csVariableName = psVariableName;
  self.caProperties = [];
  self.caItems = {};
  
  document.getElementsByTagName('form')[0].onsubmit = function(){
    gebi(psVariableName).value = self.getValue();
  };
  
  gebi(psSelectName).onchange = function(){
    var msSelectedItem = null;
    for(i=0;i<this.options.length;i++){
      if(this.options[i].selected){
        if(msSelectedItem){
          msSelectedItem = null;
          break;
        }else{
          msSelectedItem = this.options[i].value;
        }
      }
    }
    if(msSelectedItem){
      for(i=0;i<self.caProperties.length;i++){
        if(gebi(self.caProperties[i])){
          gebi(self.caProperties[i]).value = self.caItems[msSelectedItem][self.caProperties[i]];
        }
      }
    }else{
      for(i=0;i<self.caProperties.length;i++){
        if(gebi(self.caProperties[i])){
          gebi(self.caProperties[i]).value = '';
        }
      }
    }
  }
  
  self.addProperty = function(psPropertyName){
    self.caProperties.push(psPropertyName);
  }

  self.getItemText = function(psItemName){
    var i;
    var maProperties = [];
    for(i=0;i<self.caProperties.length;i++){
      maProperties.push(self.caItems[psItemName][self.caProperties[i]]);
    }
    return maProperties.join(' | ');
  }

  self.addItem = function(psItemName){
    if(!psItemName) return;
    var maProperties;
    var moSelect;
    var i;
    if(arguments.length==self.caProperties.length+1){
      maProperties = arguments.splice(0,1);
    }else if(arguments.length==1){
      maProperties = [];
      for(i=0;i<self.caProperties.length;i++){
        maProperties.push(gebi(self.caProperties[i]).value);
      }
    }else{
      alert('Wrong parameter count in FWDItemList.addItem');
      return;
    }
    self.caItems[psItemName] = {'name':psItemName};
    for(i=0;i<self.caProperties.length;i++){
      self.caItems[psItemName][self.caProperties[i]] = maProperties[i];
    }
    moSelect = gebi(self.csSelectName);
    for(i=0;i<moSelect.options.length;i++){
      if(moSelect.options[i].value==psItemName) break;
    }
    moSelect.options[i] = new Option(self.getItemText(psItemName),psItemName);
  }
  
  self.getItemProperty = function(psItemName,psPropertyName){
    if(self.caItems[psItemName]){
      return self.caItems[psItemName][psPropertyName];
    }else{
      return null;
    }
  }
  
  self.removeSelectedItems = function(){
    var moSelect = gebi(self.csSelectName);
    var i;
    for(i=moSelect.options.length-1;i>=0;i--){
      if(moSelect.options[i].selected){
        delete self.caItems[moSelect.options[i].value];
        moSelect.options[i] = null;
      }
    }
  }
  
  self.getValue = function(){
    var maProperties;
    var maItems = [];
    var i,j;
    var moSelect = gebi(self.csSelectName);
    maItems.push(self.caProperties.join(':'));
    for(i=0;i<moSelect.options.length;i++){
      maProperties = [];
      for(j=0;j<self.caProperties.length;j++){
        maProperties.push(self.caItems[moSelect.options[i].value][self.caProperties[j]]);
      }
      maItems.push(maProperties.join(':'));
    }
    return maItems.join('|');
  }
  
  self.loadFromVariable = function(){
    var moItem;
    var moSelect;
    var i,j;
    var msSerializedValue = gebi(self.csVariableName).value;
    var maItems = msSerializedValue.split('|');
    self.caProperties = maItems[0].split(':');
    self.caItems = {};
    for(i=1;i<maItems.length;i++){
      maItems[i] = maItems[i].split(':');
      moItem = {};
      for(j=0;j<self.caProperties.length;j++){
        moItem[self.caProperties[j]] = maItems[i][j];
      }
      self.caItems[moItem.name] = moItem;
      moSelect = gebi(self.csSelectName);
      moSelect.options[moSelect.options.length] = new Option(self.getItemText(moItem.name),moItem.name);
    }
  }
  
}