
function FWDItemTree(psSelectName,psVariableName){
  var self = new FWDItemList(psSelectName,psVariableName);
  
  self.ciLastId = 0;
  
  self.clear = function(){
    gobi(self.csSelectName).clear();
    self.ciLastId = 0;
    self.caItems = {};
  }
  
  self.getNewId = function(){
    return ++self.ciLastId;
  }
  
  self.getItemText = function(piId){
    /*
    return '['+piId+']('+self.caItems[piId]['parent']+') '+self.caItems[piId]['tag']+' - '+self.caItems[piId]['name'];
    /*/
    return self.caItems[piId]['tag']+' - '+self.caItems[piId]['name'];
    //*/
  }
  
  self.getSelectedItemId = function(){
    var miSelectedId = null;
    var moSelect = gebi(self.csSelectName);
    for(var i=0;i<moSelect.options.length;i++){
      if(moSelect.options[i].selected){
        if(miSelectedId){
          return null;
        }else{
          miSelectedId = moSelect.options[i].value;
        }
      }
    }
    return miSelectedId;
  }
  
  self.addItem = function(){
    var maProperties;
    var moSelect;
    var i,miLevel,miId = arguments[0];
    if(arguments.length==self.caProperties.length){
      maProperties = arguments;
    }else{
      alert('Wrong parameter count in FWDItemList.addItem');
      return;
    }
    self.caItems[miId] = {};
    for(i=0;i<self.caProperties.length;i++){
      self.caItems[miId][self.caProperties[i]] = maProperties[i];
    }
    
    if(self.caItems[miId]['parent']){
      miLevel = self.caItems[self.caItems[miId]['parent']]['level'] + 1;
    }else{
      miLevel = 1;
    }
    
    self.caItems[miId]['level'] = miLevel;
    
    moSelect = gebi(self.csSelectName);
    for(i=0;i<moSelect.options.length;i++){
      if(moSelect.options[i].value==miId) break;
    }
    var moOption = new Option(self.getItemText(miId),miId);
    moOption.className = 'siLv'+miLevel;
    moSelect.options[i] = moOption;
    
    self.ciLastId = Math.max(self.ciLastId,miId);
  }
  
  self.addChild = function(){
    var miParentId = self.getSelectedItemId();
    if(miParentId){
      var moSelect = gebi(self.csSelectName);
      var moInsertionPoint = moSelect.options[moSelect.selectedIndex].nextSibling;
      var miParentLevel = self.caItems[miParentId]['level'];
      while(moInsertionPoint && self.caItems[moInsertionPoint.value]['level'] > miParentLevel){
        moInsertionPoint = moInsertionPoint.nextSibling;
      }
      var miId = self.getNewId();
      var moItem = {};
      for(var i=0;i<self.caProperties.length;i++){
        if(gebi(self.caProperties[i])){
          moItem[self.caProperties[i]] = gebi(self.caProperties[i]).value;
        }
      }
      moItem['id'] = miId;
      moItem['parent'] = miParentId;
      moItem['level'] = miParentLevel + 1;
      self.caItems[miId] = moItem;
      
      var moOption = new Option(self.getItemText(miId),miId);
      moOption.className = 'siLv'+(miParentLevel+1);
      moSelect.insertBefore(moOption,moInsertionPoint);
      
      return true;
    }else{
      return false;
    }
  }
  
  self.editSelected = function(){
    var moSelect,moOption;
    var miSelectedId = self.getSelectedItemId();
    if(miSelectedId){
      for(var i=0;i<self.caProperties.length;i++){
        if(gebi(self.caProperties[i])){
          self.caItems[miSelectedId][self.caProperties[i]] = gebi(self.caProperties[i]).value;
        }
      }
      moSelect = gebi(self.csSelectName);
      moSelect.options[moSelect.selectedIndex].text = self.getItemText(miSelectedId);
      return true;
    }else{
      return false;
    }
  }
  
  self.swapSiblings = function(piIndexLower,piIndexUpper){
    var moSelect = gebi(self.csSelectName);
    var i,miLevel = self.caItems[moSelect.options[piIndexLower].value]['level'];
    var moFirstSibling = moSelect.options[piIndexLower];
    moSelect.insertBefore(moSelect.options[piIndexUpper],moFirstSibling);
    i = piIndexUpper + 1;
    while(moSelect.options[i] && self.caItems[moSelect.options[i].value]['level'] > miLevel){
      moSelect.insertBefore(moSelect.options[i],moFirstSibling);
      i++;
    }
  }
  
  self.moveUp = function(){
    var miSelectedId = self.getSelectedItemId();
    var miSelectedIndex;
    var moSelect = gebi(self.csSelectName);
    var i,miLevel;
    if(miSelectedId){
      miSelectedIndex = moSelect.selectedIndex;
      miLevel = self.caItems[moSelect.options[miSelectedIndex].value]['level'];
      i = miSelectedIndex - 1;
      while(i>=0 && self.caItems[moSelect.options[i].value]['level'] > miLevel) i--;
      if(i>=0 && self.caItems[moSelect.options[i].value]['level'] == miLevel){
        self.swapSiblings(i,miSelectedIndex);
      }
    }
  }
  
  self.moveDown = function(){
    var miSelectedId = self.getSelectedItemId();
    var miSelectedIndex;
    var moSelect = gebi(self.csSelectName);
    var i,miLevel;
    if(miSelectedId){
      miSelectedIndex = moSelect.selectedIndex;
      miLevel = self.caItems[moSelect.options[miSelectedIndex].value]['level'];
      i = miSelectedIndex + 1;
      while(moSelect.options[i] && self.caItems[moSelect.options[i].value]['level'] > miLevel) i++;
      if(i<moSelect.options.length && self.caItems[moSelect.options[i].value]['level'] == miLevel){
        self.swapSiblings(miSelectedIndex,i);
      }
    }
  }
  
  self.removeSelected = function(){
    var i,miLevel;
    var moSelect = gebi(self.csSelectName);
    for(i=moSelect.options.length-1;i>=0;i--){
      if(moSelect.options[i].selected){
        miLevel = self.caItems[moSelect.options[i].value]['level'];
        delete self.caItems[moSelect.options[i].value];
        moSelect.removeChild(moSelect.options[i]);
        while(moSelect.options[i] && self.caItems[moSelect.options[i].value]['level'] > miLevel){
          delete self.caItems[moSelect.options[i].value];
          moSelect.removeChild(moSelect.options[i]);
        }
      }
    }
  }
  
  return self;
}
