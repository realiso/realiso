<?php

include_once 'include.php';
include_once $handlers_ref . 'QuerySelectSystem.php';
include_once $handlers_ref . 'QueryACLTree.php';
include_once $handlers_ref . 'QueryCopySystem.php';
include_once $classes_ref . 'ACLTagsExtractor.php';

class CopySystemEvent extends FWDRunnable {
  public function run(){
    $miSystemFrom = FWDWebLib::getObject('system')->getValue();
    $msSystemName = FWDWebLib::getObject('var_system_name')->getValue();
    
    $moSystem = new ACLSystem();
    $moSystem->setFieldValue('system_name',$msSystemName);
    $miSystemTo = $moSystem->insert(true);
    
    QueryCopySystem::copy($miSystemFrom,$miSystemTo);
    
    $moHandler = new QuerySelectSystem();
    $moSelect = FWDWebLib::getObject('system');
    $moSelect->setQueryHandler($moHandler);
    $moSelect->populate();
    $moSelect->execEventPopulate();
    
    $msSystemName = FWDWebLib::convertToISO($msSystemName,true);
    
    echo "gebi('system').value = '{$miSystemFrom}';"
        ."showInfo('Sistema \"{$msSystemName}\" criado.');";
  }
}

class ExtractTagsEvent extends FWDRunnable {
  public function run(){
    $msSystemPath = FWDWebLib::getObject('var_system_path')->getValue();
    
    if(file_exists($msSystemPath) && is_readable($msSystemPath)){
      $moTagsExtractor = new ACLTagsExtractor();
      $moTagsExtractor->extractTagsFromFiles($msSystemPath);
      $maTags = $moTagsExtractor->getTags();
      
      $maSystemTags = ACLAcl::getTagsFromSystem(FWDWebLib::getObject('system')->getValue());
      
      // ACLs existentes no BD, mas n�o encontradas nos arquivos
      $maUnusedAcls = array_diff($maSystemTags,array_keys($maTags));
      
      // ACLs encontradas nos arquivos, mas que n�o est�o no BD
      $maAclsNotInBD = array();
      foreach($maTags as $msTag=>$maFiles){
        if(!in_array($msTag,$maSystemTags) && strpos($msTag,'.')!==false){
          $maAclsNotInBD[$msTag] = $maFiles;
        }
      }
      
      $msHtml = '<h2>ACLs que n�o constam no BD</h2><ul>';
      foreach($maAclsNotInBD as $msTag=>$maFiles){
        $msHtml.= "<li>{$msTag}<br><ul>";
        foreach($maFiles as $msFileName=>$miOccurrences){
          $msHtml.= "<li>{$msFileName}</li>";
        }
        $msHtml.= '</ul>';
      }
      $msHtml.= '</ul></li>';
      
      $msHtml.= '<h2>ACLs n�o encontradas nos arquivos</h2><ul>';
      foreach($maUnusedAcls as $msTag){
        $msHtml.= "<li>{$msTag}</li>";
      }
      $msHtml.= '</ul>';
      echo "showInfo(\"{$msHtml}\")";
    }else{
      echo "showError('Diret�rio \"{$msSystemPath}\" n�o existe ou o sistema n�o tem permiss�o para l�-lo.');";
    }
  }
}

class SaveEvent extends FWDRunnable {
  public function run(){
    $miSystemId = FWDWebLib::getObject('system')->getValue();
    $msSerializedAclTree = FWDWebLib::getObject('var_acl_tree')->getValue();
    $maAclLines = explode('|',$msSerializedAclTree);
    $maProperties = explode(':',$maAclLines[0]);
    ACLAcl::deleteAll($miSystemId);
    for($i=1;$i<count($maAclLines);$i++){
      $maAclProperties = array_combine($maProperties,explode(':',$maAclLines[$i]));
      $moAcl = new ACLAcl();
      if($maAclProperties['parent']){
        $moAcl->setFieldValue('acl_parent_id'  ,$maAclProperties['parent']);
      }
      $moAcl->setFieldValue('acl_system_id'  ,$miSystemId);
      $moAcl->setFieldValue('acl_id'         ,$maAclProperties['id']);
      $moAcl->setFieldValue('acl_tag'        ,$maAclProperties['tag']);
      $moAcl->setFieldValue('acl_name'       ,$maAclProperties['name']);
      $moAcl->setFieldValue('acl_description',$maAclProperties['description']);
      $moAcl->setFieldValue('acl_order'      ,$i);
      $moAcl->insert();
    }
    echo "showInfo('Sistema salvo.');";
  }
}

class ChangeSystemEvent extends FWDRunnable {
  public function run(){
    $moHandler = new QueryACLTree();
    $moHandler->setSystem(FWDWebLib::getObject('system')->getValue());
    $moHandler->makeQuery();
    $moHandler->executeQuery();
    while($moHandler->fetch()){
      $miId = $moHandler->getFieldValue('acl_id');
      $miParentId = $moHandler->getFieldValue('acl_parent_id');
      $miParentId = ($miParentId?$miParentId:0);
      $miLevel = $moHandler->getFieldValue('acl_level');
      $msTag = $moHandler->getFieldValue('acl_tag');
      $msName = str_replace("'","\'",$moHandler->getFieldValue('acl_name'));
      $msDescription = str_replace("'","\'",$moHandler->getFieldValue('acl_description'));
      echo "soACLTree.addItem({$miId},{$miParentId},'{$msTag}','{$msName}','{$msDescription}');";
    }
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
    $moStartEvent->addAjaxEvent(new ChangeSystemEvent('change_system'));
    $moStartEvent->addAjaxEvent(new SaveEvent('save'));
    $moStartEvent->addAjaxEvent(new ExtractTagsEvent('extract_tags'));
    $moStartEvent->addAjaxEvent(new CopySystemEvent('copy_system'));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moHandler = new QuerySelectSystem();
    $moSelect = FWDWebLib::getObject('system');
    $moSelect->setQueryHandler($moHandler);
    $moSelect->populate();
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script type="text/javascript">
        
        function showInfo(psMessage){
          gebi('message').className = 'FWDMemoStaticInfo';
          gobi('message').setValue(psMessage);
        }
        
        function showError(psMessage){
          gebi('message').className = 'FWDMemoStaticError';
          gobi('message').setValue(psMessage);
        }
        
        function getSelectTexts(psSelectId){
          var moSelect = gebi(psSelectId);
          var maTexts = [];
          for(i=0;i<moSelect.options.length;i++){
            maTexts.push(moSelect.options[i]);
          }
          return maTexts;
        }
        
        function copySystem(){
          var moSelect = gebi('system');
          var msSystemName = moSelect.options[moSelect.selectedIndex].text;
          var maTexts;
          if(moSelect.value){
            msSystemName = prompt('Nome do novo sistema: ','C�pia de '+msSystemName);
            maTexts = getSelectTexts('system');
            if(msSystemName===null){
              showInfo('C�pia de sistema abortada.');
            }else if(msSystemName && maTexts.indexOf(msSystemName)<0){
              gebi('var_system_name').value = msSystemName;
              trigger_event('copy_system',3);
            }else{
              showError('Nome inv�lido.');
            }
          }else{
            showError('Para criar uma c�pia, um sistema precisa estar selecionado.');
          }
        }
        
        var soACLTree = new FWDItemTree('acl_tree','var_acl_tree');
        soACLTree.addProperty('id');
        soACLTree.addProperty('parent');
        soACLTree.addProperty('tag');
        soACLTree.addProperty('name');
        soACLTree.addProperty('description');
      </script>
    <?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('default.xml');

?>