CREATE TABLE acl_system
(
  pksystem             serial               NOT NULL,
  sname                varchar(256)         NOT NULL,
  CONSTRAINT pk_acl_system PRIMARY KEY (pksystem)
) 
WITH OIDS;

CREATE TABLE acl_acl
(
  fksystem             integer              NOT NULL,
  pkacl                serial               NOT NULL,
  fkparent             integer              NULL,
  sdescription         varchar(256)         NULL,
  stag                 varchar(256)         NOT NULL,
  sname                varchar(256)         NULL,
  norder               integer              NULL,
  CONSTRAINT pk_acl_acl PRIMARY KEY (fksystem,pkacl)
)
WITH OIDS;
/*
CREATE TABLE acl_profile
(
  fksystem             integer              NOT NULL,
  pkprofile            integer              NOT NULL,
  sname                varchar(256)         NOT NULL,
  CONSTRAINT pk_acl_profile PRIMARY KEY (fksystem,pkprofile)
)
WITH OIDS;

CREATE TABLE acl_profile_acl (
  fksystem             integer              NOT NULL,
  fkprofile            integer              NOT NULL,
  fkacl                integer              NOT NULL,
  CONSTRAINT pk_acl_profile_acl PRIMARY KEY (fksystem, fkprofile, fkacl)
)
WITH OIDS;
*/
ALTER TABLE acl_acl
  ADD CONSTRAINT fk_acl_acl_fksystem_acl_system FOREIGN KEY (fksystem)
      REFERENCES acl_system (pksystem)
      ON UPDATE RESTRICT ON DELETE CASCADE;

ALTER TABLE acl_acl
  ADD CONSTRAINT fk_acl_acl_fksystem_fkparent_acl_acl FOREIGN KEY (fksystem,fkparent)
      REFERENCES acl_acl (fksystem,pkacl)
      ON UPDATE RESTRICT ON DELETE CASCADE;
/*
ALTER TABLE acl_profile
  ADD CONSTRAINT fk_acl_profile_fksystem_acl_system FOREIGN KEY (fksystem)
      REFERENCES acl_system (pksystem)
      ON UPDATE RESTRICT ON DELETE CASCADE;

ALTER TABLE acl_profile_acl
  ADD CONSTRAINT fk_acl_profile_acl_fksystem_fkacl_acl_acl FOREIGN KEY (fksystem,fkacl)
      REFERENCES acl_acl (fksystem,pkacl)
      ON UPDATE RESTRICT ON DELETE CASCADE;

ALTER TABLE acl_profile_acl
  ADD CONSTRAINT fk_acl_profile_acl_fksystem_fkprofile_acl_acl FOREIGN KEY (fksystem,fkprofile)
      REFERENCES acl_profile (fksystem,pkprofile)
      ON UPDATE RESTRICT ON DELETE CASCADE;
*/
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

CREATE TRUSTED PROCEDURAL LANGUAGE 'plpgsql'
  HANDLER plpgsql_call_handler
  VALIDATOR plpgsql_validator;

CREATE TYPE tree_acl AS (
  pkacl                integer,
  fkparent             integer,
  nlevel               integer,
  stag                 varchar(256),
  sname                varchar(256),
  sdescription         varchar(256),
  norder               integer
);

CREATE OR REPLACE FUNCTION get_acl_tree(pnSystem integer, pnParent integer, pnLevel integer) RETURNS SETOF tree_acl AS
$BODY$
DECLARE
  fnLevel integer;
  fsQueryRoot varchar(255);
  root tree_acl%rowtype;
  node tree_acl%rowtype;
BEGIN
  fnLevel:= pnLevel + 1;
  fsQueryRoot:= ' SELECT pkAcl, fkParent, ' || fnLevel::text || '  as nLevel, sTag, sName, sDescription, norder FROM acl_acl  WHERE  fksystem = ' || pnSystem || ' AND fkParent ';

  IF pnParent > 0 THEN
   fsQueryRoot:= fsQueryRoot || ' = ' || pnParent || ' ORDER BY nOrder';
  ELSE 
   fsQueryRoot:= fsQueryRoot || ' IS NULL ORDER BY nOrder';
  END IF;

  FOR root IN EXECUTE fsQueryRoot LOOP
    RETURN NEXT root;
    FOR node IN
      SELECT pkAcl, fkParent, nLevel, sTag, sName, sDescription, nOrder
      FROM get_acl_tree(pnSystem, root.pkAcl, fnLevel)
      ORDER BY nOrder
    LOOP
      RETURN NEXT node;
    END LOOP;
  END LOOP;
  RETURN ;
END
$BODY$
  LANGUAGE 'plpgsql' VOLATILE;
