<?php

include_once 'include.php';

$siSystemId = $_GET['system'];

$soAcl = new ACLAcl();
$soAcl->createFilter($siSystemId,'acl_system_id');
$soAcl->select();

$saTags = array();
while($soAcl->fetch()){
  $saTags[] = $soAcl->getFieldValue('acl_tag');
}

header('Cache-Control: ');// leave blank to avoid IE errors
header('Pragma: ');// leave blank to avoid IE errors
header('Content-type: text/enriched');
header('Content-Disposition: attachment; filename="acl_tags.php"');

echo "<? \$laTags = array('".implode("','",$saTags)."'); ?>";

?>