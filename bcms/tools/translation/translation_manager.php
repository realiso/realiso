<?php
include_once "include.php";
include_once $handlers_ref . "QuerySystemList.php";
include_once $handlers_ref . "QueryGridStrings.php";

class LoadSystemStrings extends FWDRunnable {
  public function run() {        
    $moGrid = FWDWebLib::getObject("grid_strings");      
      $moGrid->execEventPopulate();
  }
}

class TranslationGrid extends FWDDrawGrid {
  public function drawItem() {      
    switch($this->ciColumnIndex){    
      case $this->getIndexByAlias('string_is_translated'):
        if ($this->coCellBox->getValue()) $this->coCellBox->setValue("Sim");
        else $this->coCellBox->setValue("Nao");
        return $this->coCellBox->draw();
      break;
                  
      default:
        return parent::drawItem();
      break;
     }
  }
}

class SaveTranslation extends FWDRunnable {
  public function run() {        
    $miStringKeyId = FWDWebLib::getObject('string_key_id')->getValue();
    
    $moTranslationString = new MPTranslationString();
    $moTranslationString->createFilter($miStringKeyId, "string_key_id");    
    $moTranslationString->setFieldValue("string_value", FWDWebLib::getObject('xml_text')->getValue());
    $moTranslationString->setFieldValue("string_portugues_isms", FWDWebLib::getObject('portugues_text')->getValue());
    $moTranslationString->setFieldValue("string_english_isms", FWDWebLib::getObject('english_text')->getValue());
    $moTranslationString->setFieldValue("string_espanol", FWDWebLib::getObject('espanol_text')->getValue());
    $moTranslationString->setFieldValue("string_chinese_isms", FWDWebLib::getObject('chinese_isms_text')->getValue());
    $moTranslationString->setFieldValue("string_portugues_ems", FWDWebLib::getObject('portugues_ems_text')->getValue());
    $moTranslationString->setFieldValue("string_portugues_ohs", FWDWebLib::getObject('portugues_ohs_text')->getValue());
    $moTranslationString->setFieldValue("string_english_ohs", FWDWebLib::getObject('english_ohs_text')->getValue());
    $moTranslationString->setFieldValue("string_english_ems", FWDWebLib::getObject('english_ems_text')->getValue());
    $moTranslationString->setFieldValue("string_english_sox", FWDWebLib::getObject('english_sox_text')->getValue());
    $moTranslationString->setFieldValue("string_english_pci", FWDWebLib::getObject('english_pci_text')->getValue());
    $moTranslationString->setFieldValue("string_german_sox", FWDWebLib::getObject('german_sox_text')->getValue());
    $moTranslationString->setFieldValue("string_french_sox", FWDWebLib::getObject('french_sox_text')->getValue());
    $mbIsTranslated = FWDWebLib::getObject('check_translated_controller')->getValue() ? 1 : 0;
    $moTranslationString->setFieldValue("string_is_translated", $mbIsTranslated);    
    $moTranslationString->update();    

    FWDWebLib::getObject("grid_strings")->execEventPopulate();    
    echo "gebi('edit_window').style.display='none';";
  }
}

class OpenEditWindow extends FWDRunnable {
  public function run() {        
    $miStringKeyId = FWDWebLib::getObject('string_key_id')->getValue();
    $moTranslationString = new MPTranslationString();
    $moTranslationString->fetchById($miStringKeyId);    
    
    $msPortuguesISMS = $moTranslationString->getFieldValue("string_portugues_isms");
    $msEnglishISMS = $moTranslationString->getFieldValue("string_english_isms");
    $msEspanol = $moTranslationString->getFieldValue("string_espanol");
    $msChineseISMS = $moTranslationString->getFieldValue("string_chinese_isms");
    $msPortuguesEMS = $moTranslationString->getFieldValue("string_portugues_ems");
    $msPortuguesOHS = $moTranslationString->getFieldValue("string_portugues_ohs");
    $msEnglishEMS = $moTranslationString->getFieldValue("string_english_ems");
    $msEnglishOHS = $moTranslationString->getFieldValue("string_english_ohs");
    $msEnglishSOX = $moTranslationString->getFieldValue("string_english_sox");
    $msEnglishPCI = $moTranslationString->getFieldValue("string_english_pci");
    $msGermanSOX = $moTranslationString->getFieldValue("string_german_sox");
    $msFrenchSOX = $moTranslationString->getFieldValue("string_french_sox");
    $msCurrentValue = $moTranslationString->getFieldValue("string_value");
    $mbIsTranslated = $moTranslationString->getFieldValue("string_is_translated");
    $msStringId = $moTranslationString->getFieldValue("string_id");
    FWDWebLib::getObject('portugues_text')->setValue($msPortuguesISMS);
    FWDWebLib::getObject('english_text')->setValue($msEnglishISMS);
    FWDWebLib::getObject('espanol_text')->setValue($msCurrentValue);    
    FWDWebLib::getObject('chinese_isms_text')->setValue($msChineseISMS);
    FWDWebLib::getObject('portugues_ems_text')->setValue($msPortuguesEMS);
    FWDWebLib::getObject('portugues_ohs_text')->setValue($msPortuguesOHS);
    FWDWebLib::getObject('english_ems_text')->setValue($msEnglishEMS);
    FWDWebLib::getObject('english_ohs_text')->setValue($msEnglishOHS);
    FWDWebLib::getObject('english_sox_text')->setValue($msEnglishSOX);
    FWDWebLib::getObject('english_pci_text')->setValue($msEnglishPCI);
    FWDWebLib::getObject('german_sox_text')->setValue($msGermanSOX);
    FWDWebLib::getObject('french_sox_text')->setValue($msFrenchSOX);
    FWDWebLib::getObject('xml_text')->setValue($msEspanol);
        
    if ($mbIsTranslated){
      echo" if(!gobi('check_translated_controller_only_translated').isChecked()){
              gfx_check('check_translated_controller','only_translated');
            }";
      FWDWebLib::getObject('is_translated')->setAttrCheck("true");
    }else{
      echo" if(gobi('check_translated_controller_only_translated').isChecked()){
              gfx_check('check_translated_controller','only_translated');
            }";
      FWDWebLib::getObject('is_translated')->setAttrCheck("true");
    }
    
    $msPortuguesISMS = preg_replace("/([^\\\])'/","$1\\'",$msPortuguesISMS);
    $msEnglishISMS = preg_replace("/([^\\\])'/","$1\\'",$msEnglishISMS);
    $msEspanol = preg_replace("/([^\\\])'/","$1\\'",$msEspanol);
    $msChineseISMS = preg_replace("/([^\\\])'/","$1\\'",$msChineseISMS);
    $msPortuguesEMS = preg_replace("/([^\\\])'/","$1\\'",$msPortuguesEMS);
    $msPortuguesOHS = preg_replace("/([^\\\])'/","$1\\'",$msPortuguesOHS);    
    $msEnglishEMS = preg_replace("/([^\\\])'/","$1\\'",$msEnglishEMS);
    $msEnglishOHS = preg_replace("/([^\\\])'/","$1\\'",$msEnglishOHS);
    $msEnglishSOX = preg_replace("/([^\\\])'/","$1\\'",$msEnglishSOX);
    $msEnglishPCI = preg_replace("/([^\\\])'/","$1\\'",$msEnglishPCI);
    $msGermanSOX = preg_replace("/([^\\\])'/","$1\\'",$msGermanSOX);
    $msFrenchSOX = preg_replace("/([^\\\])'/","$1\\'",$msFrenchSOX);
    $msCurrentValue = preg_replace("/([^\\\])'/","$1\\'",$msCurrentValue);
    $msStringId =  preg_replace("/([^\\\])'/","$1\\'",$msStringId);
    
    echo "gebi('string_id').value='$msStringId';";
    echo "gebi('portugues_text').value='$msPortuguesISMS';";
    echo "gebi('english_text').value='$msEnglishISMS';";
    echo "gebi('espanol_text').value='$msEspanol';";    
    echo "gebi('chinese_isms_text').value='$msChineseISMS';";
    echo "gebi('portugues_ems_text').value='$msPortuguesEMS';";
    echo "gebi('portugues_ohs_text').value='$msPortuguesOHS';";
    echo "gebi('english_ems_text').value='$msEnglishEMS';";
    echo "gebi('english_ohs_text').value='$msEnglishOHS';";
    echo "gebi('english_sox_text').value='$msEnglishSOX';";
    echo "gebi('english_pci_text').value='$msEnglishPCI';";
    echo "gebi('german_sox_text').value='$msGermanSOX';";
    echo "gebi('french_sox_text').value='$msFrenchSOX';";
    echo "gebi('xml_text').value='$msCurrentValue';";
    
    echo "gebi('edit_window').style.display='inline';";
  }
}

class SearchStringEvent extends FWDRunnable {
  public function run(){
    FWDWebLib::getObject('grid_strings')->execEventPopulate();        
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new LoadSystemStrings("load_system_strings"));
    $moStartEvent->addAjaxEvent(new OpenEditWindow("open_edit_window"));  
    $moStartEvent->addAjaxEvent(new SaveTranslation("save_translation"));
    $moStartEvent->addAjaxEvent(new SearchStringEvent("search_string_event"));
    
    $msIdFilter = FWDWebLib::getObject('search_id_filter')->getValue();  
    $msValueFilter = FWDWebLib::getObject('search_value_filter')->getValue();
    $msFileFilter = FWDWebLib::getObject('search_file_filter')->getValue();
    $mbOnlyNotTranslated = FWDWebLib::getObject('search_not_translated_filter')->getValue() ? 1 : 0;
    $mbPortugueseNotNull = FWDWebLib::getObject('portuguese_controller')->getValue() ? 1 : 0;
    $mbEnglishNull = FWDWebLib::getObject('english_controller')->getValue() ? 1 : 0;
    
    $moGrid = FWDWebLib::getObject('grid_strings');
    $moGridHandler = new QueryGridStrings(FWDWebLib::getConnection());
    $moGridHandler->setSystem(FWDWebLib::getObject('system_id')->getValue());
    $moGridHandler->setIdFilter($msIdFilter);
    $moGridHandler->setValueFilter(FWDWebLib::convertToISO($msValueFilter));
    $moGridHandler->setFileFilter($msFileFilter);
    $moGridHandler->setOnlyNotTranslatedFilter($mbOnlyNotTranslated);
    
    $moGridHandler->setEnglishFilter(trim(FWDWebLib::getObject('search_english_filter')->getValue()));
    $moGridHandler->setPortugueseFilter(trim(FWDWebLib::getObject('search_portuguese_filter')->getValue()));
    $moGridHandler->setPortugueseNotNull($mbPortugueseNotNull);
    $moGridHandler->setEnglishNull($mbEnglishNull);
                   
    $moGrid->setQueryHandler($moGridHandler);
      $moGrid->setObjFwdDrawGrid(new TranslationGrid());    
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){    
    $moWebLib = FWDWebLib::getInstance();    
    $moHandler = new QuerySystemList(FWDWebLib::getConnection());      
      $moSelect = $moWebLib->getObject('select_system');
      $moSelect->setQueryHandler($moHandler);
      $moSelect->populate();    
    
    $moWebLib->dump_html(FWDWebLib::getObject('dialog'));  
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("translation_manager.xml");
?>