<?php

include_once "include.php";

include_once $handlers_ref."QueryClientsByIds.php";
include_once $handlers_ref."QuerySystemsByClientId.php";


class PopulateSystemSelectEvent extends FWDRunnable {

	public function run() {
		echo "js_clear_select('system_select');";
		
		$miClientId = FWDWebLib::getObject("client_id")->getValue();
		$moQuerySystemByClient = new QuerySystemsByClientId(FWDWebLib::getConnection());
		$moQuerySystemByClient->setClientId($miClientId);
		$moQuerySystemByClient->makeQuery();
		$moQuerySystemByClient->executeQuery();
		$maInfo = array();
		foreach ($moQuerySystemByClient->getSystems() as $miSysId => $msSysName)
			$maInfo[] = array($miSysId,$msSysName);
		$msInfo = serialize($maInfo);
		
		echo "js_populate_select('system_select','$msInfo');";
	}

}


class RemoveSystemEvent extends FWDRunnable {

	public function run() {
		$miSystemId = FWDWebLib::getObject("system_id")->getValue();
		$miClientId = FWDWebLib::getObject("client_id")->getValue();
		$moSystemClientAttribute = new MPSystemClientAttribute();
		$moSystemClientAttribute->createFilter($miSystemId,"system_id");
		$moSystemClientAttribute->createFilter($miClientId,"client_id");
		$moSystemClientAttribute->delete();
		echo "coWindow = soPopUpManager.getRootWindow();
          coWindow.update_selects('all');
          trigger_event('populate_system_select_event','3');
         ";
	}

}


class InitEvent extends FWDRunnable {

	public function run() {
		$moQuery = new QueryClientsByIds(FWDWebLib::getConnection());
		$moQuery->makeQuery();
		$moQuery->executeQuery();
		$maInfo = array();
		foreach ($moQuery->getClients() as $miCliId => $msCliName)
			$maInfo[] = array($miCliId,$msCliName);
		$msInfo = serialize($maInfo);
		
		echo "js_populate_select('client_select','$msInfo');";
		echo "gebi('client_id').value = gebi('client_select').value;";
		
		$maClients = $moQuery->getClients();
		$maClients = array_keys($maClients);
		$miClientId = array_shift($maClients);
		
		if ($miClientId) {	
			// popula select de sistema do cliente atual
			$moQuerySystemByClient = new QuerySystemsByClientId(FWDWebLib::getConnection());
			$moQuerySystemByClient->setClientId($miClientId);
			$moQuerySystemByClient->makeQuery();
			$moQuerySystemByClient->executeQuery();
			$maInfo = array();
			foreach ($moQuerySystemByClient->getSystems() as $miSysId => $msSysName)
				$maInfo[] = array($miSysId,$msSysName);
			$msInfo = serialize($maInfo);
			
			echo "js_populate_select('system_select','$msInfo');";
			echo "gebi('system_id').value = gebi('system_select').value;";
		}
	}

}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent("screen_event"));
		
		$moStartEvent->addAjaxEvent(new InitEvent("init_event"));
		$moStartEvent->addAjaxEvent(new PopulateSystemSelectEvent("populate_system_select_event"));
		$moStartEvent->addAjaxEvent(new RemoveSystemEvent("remove_system_event"));
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
?>
		<script language="javascript">
			function refresh_system_select() {
				trigger_event('populate_system_select_event','3');
			}
			
			trigger_event('init_event','3');
		</script>
<?
	}

}

$soWebLib = FWDWebLib::getInstance();

$soStartEvent = FWDStartEvent::getInstance();
$soStartEvent->addBeforeAjax(new ScreenBeforeEvent("before_ajax_event"));

$soWebLib->xml_load("license_manager_system_client_association.xml");

?>