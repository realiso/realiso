<?php

include_once "include.php";

class SaveClientNameEvent extends FWDRunnable {

	public function run() {
		$miClientId = FWDWebLib::getObject('param_cli_id')->getValue();
		$msClientName = FWDWebLib::getObject('var_client_name')->getValue();
		$moClient = new MPClient();
		if ($miClientId) {
			$moClient->createFilter($miClientId,"client_id");
			$moClient->setFieldValue("client_name",$msClientName);
			$moClient->update();
		}
		else {
			$moClient->setFieldValue("client_name",$msClientName);
			$moClient->insert();
		}
		echo "soPopUp = soPopUpManager.getPopUpById('license_manager_client_edit');
         soWindow = soPopUp.getOpener();
         soWindow.update_selects('all');
         soPopUpManager.closePopUp('license_manager_client_edit');";
	}

}

function screenMainCode(){
	$miClientId = FWDWebLib::getObject("param_cli_id")->getValue();
	if ($miClientId) {
		$moClient = new MPClient();
		$moClient->fetchById($miClientId);
		FWDWebLib::getObject("client_name")->setValue($moClient->getFieldValue("client_name"));
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));
		
		$moStartEvent->addAjaxEvent(new SaveClientNameEvent("save_client_name_event"));
		$miClientId = FWDWebLib::getObject("param_cli_id")->getValue();
    if ($miClientId) {
      $moClient = new MPClient();
      $moClient->fetchById($miClientId);
      FWDWebLib::getObject("client_name")->setValue($moClient->getFieldValue("client_name"));
    }
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
	}

}

$soWebLib = FWDWebLib::getInstance();

$soStartEvent = FWDStartEvent::getInstance();
$soStartEvent->addBeforeAjax(new ScreenBeforeEvent(""));

$soWebLib->xml_load("license_manager_client_edit.xml");

?>