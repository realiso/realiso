<?php
/**
 * MINI PROJECTS
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QuerySystemList.
 *
 * <p>Consulta que retorna todos os sistemas.</p>
 * @package MINI PROJECTS
 * @subpackage handlers
 */
class QuerySystemList extends FWDDBQueryHandler {
  protected $caSystemsIds=array();
  protected $caSystems=array();
  

  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField('skSystem','select_id'   ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('zName'   ,'select_value',DB_STRING));
  }

  public function setSystemsIds($paSystemsIds) {
    $this->caSystemsIds=$paSystemsIds;
  }
  
	public function makeQuery() {
    $msWhere = count($this->caSystemsIds)?" WHERE skSystem IN (".implode(',',$this->caSystemsIds).") ":"";	
		$this->csSQL = "
SELECT skSystem as select_id, 
       zName as select_value 
  FROM lic_system 
  $msWhere
  ORDER BY zName ASC
";		
	}
  
  public function executeQuery() {
    parent::executeQuery();
    while($this->coDataSet->fetch()) {
      $miSelectId = $this->getFieldValue("select_id");
      $msSelectValue = $this->getFieldValue("select_value");
      $this->caSystems[$miSelectId] = $msSelectValue;
    }
  }
  
  public function getSystems() {
    return $this->caSystems;
  }
}
?>