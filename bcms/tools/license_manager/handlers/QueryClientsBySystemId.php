<?php
/**
 * MINI PROJECTS
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryClientsBySystemId.
 *
 * <p>Classe que busca informa��es de todos os clientes que est�o associados a
 * um sistema, atrav�s do id deste sistema.</p>
 * @package MINI PROJECTS
 * @subpackage handlers
 */
class QueryClientsBySystemId extends FWDDBQueryHandler {
	
	protected $caClients = array();
	
	protected $ciSystemId = 0;

  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField('c.skClient','client_id'  ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('c.zName'   ,'client_name',DB_STRING));
  }
	
	public function makeQuery() {
		$msWhere = "";
		
		$this->csSQL = "
SELECT c.skClient as client_id, 
       c.zName as client_name
	FROM lic_client c, 
       lic_system_client_attribute sca
	WHERE c.skClient = sca.skClient 
    AND sca.skSystem = {$this->ciSystemId}
	ORDER BY client_name
";
	}
	
	public function executeQuery() {
		parent::executeQuery();		
		while ($this->coDataSet->fetch()) {
			$miClientId = $this->coDataSet->getFieldByAlias("client_id")->getValue();
			$this->caClients[$miClientId] = $this->coDataSet->getFieldByAlias("client_name")->getValue();
		}
	}
	
	public function setSystemId($piSystemId) {
		$this->ciSystemId = $piSystemId;
	}
	
	public function getClients() {
		return $this->caClients;
	}
}
?>