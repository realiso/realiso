<?php
/**
 * MINI PROJECTS
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryLicenseListBySystemIdAndClientId.
 *
 * <p>Classe que busca as licen�as associadas a um sitema de um cliente,
 * atrav�s do id do sistema e do id do cliente.</p>
 * @package MINI PROJECTS
 * @subpackage handlers
 */
class QueryLicenseListBySystemIdAndClientId extends FWDDBQueryHandler {
	
	protected $ciSystemId = 0;
	
	protected $ciClientId = 0;
	
	protected $caLicenses = array();
	
  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField('l.skLicense','select_id'    ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('l.dDatetime','select_value' ,DB_DATETIME));
  }
	public function makeQuery() {
		if (!$this->ciSystemId || !$this->ciClientId)
			return false;
		
		$this->csSQL = "
SELECT DISTINCT 
       l.skLicense as select_id, 
       l.dDatetime as select_value
	FROM lic_license_system_client l, 
       lic_system_client sc  
  WHERE l.skSystemClient = sc.skSystemClient 
    AND sc.skSystem = {$this->ciSystemId} 
    AND sc.skClient = {$this->ciClientId}
	ORDER BY select_value
";
	}
	
	public function executeQuery() {
		parent::executeQuery();		
		while ($this->coDataSet->fetch()) {
			$miSelectId = $this->coDataSet->getFieldByAlias("select_id")->getValue();
			$msSelectValue = $this->coDataSet->getFieldByAlias("select_value")->getValue();
			
      $this->caLicenses[] = array($miSelectId,$msSelectValue);
		}
	}
	
	public function setSystemId($piSystemId) {
		$this->ciSystemId = $piSystemId;
	}
	
	public function setClientId($piClientId) {
		$this->ciClientId = $piClientId;
	}
	
	public function getLicenses() {
		return $this->caLicenses;
	}
}
?>