<?php
/**
 * MINI PROJECTS
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryAttributesBySystemId.
 *
 * <p>Classe que busca informa��es de atributos atrav�s de um id de sistema.</p>
 * @package MINI PROJECTS
 * @subpackage handlers
 */
class QueryAttributesBySystemId extends FWDDBQueryHandler {
	
	protected $ciSystemId = 0;
	
	protected $caAttributes = array();

  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField('skAttribute','skAttribute',DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('zName'      ,'zName',DB_STRING));
  }
	
	public function makeQuery() {
		$msWhere = "";
		
		if ($this->ciSystemId)
			$msWhere .= ($msWhere?" OR ":" WHERE ")."skSystem = {$this->ciSystemId}";
		
		$this->csSQL = "
SELECT skAttribute, 
       zName
  FROM lic_attribute
    $msWhere
  ORDER BY zName
";
	}
	
	public function executeQuery() {
		parent::executeQuery();		
		while ($this->coDataSet->fetch()) {
			$miAttributeId = $this->coDataSet->getFieldByAlias("skAttribute")->getValue();
			$this->caAttributes[$miAttributeId] = $this->coDataSet->getFieldByAlias("zName")->getValue();
		}
	}
	
	public function setSystemId($piSystemId) {
		$this->ciSystemId = $piSystemId;
	}
	
	public function getAttributes() {
		return $this->caAttributes;
	}
}
?>