<?php
/**
 * MINI PROJECTS
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QuerySelectClient.
 *
 * <p>Classe que busca informações de clientes.</p>
 * @package MINI PROJECTS
 * @subpackage handlers/select
 */
class QuerySelectClient extends FWDDBQueryHandler {
  protected $caClients=array();
  protected $caClientsSelect=array();
  protected $ciSystemId;

  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField('skClient','select_id'   ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('zName'   ,'select_value',DB_STRING));
  }
  
  public function setSystem($piSystemId) {
    $this->ciSystemId = $piSystemId;
  }
  
  public function makeQuery() {
    $msSystem = "";
    if ($this->ciSystemId) {
      $msSystem = " JOIN lic_system_client_attribute sca ON (c.skClient = sca.skClient AND sca.skSystem={$this->ciSystemId}) ";
    }
    $this->csSQL = "
SELECT c.skClient as select_id, 
       c.zName as select_value
  FROM lic_client c
    $msSystem
  ORDER BY select_value
";        
  }
  
  public function executeQuery() {
    parent::executeQuery();   
    while ($this->coDataSet->fetch()) {
      $miSystemId = $this->coDataSet->getFieldByAlias("select_id")->getValue();
      $this->caClients[$miSystemId] = $this->coDataSet->getFieldByAlias("select_value")->getValue();
    }
  }
  
  public function getClients() {
    return $this->caClients;
  }
  
  /**
   * Retorna um array pronto para ser populado em um select.
   */
  public function getSelect() {
    foreach($this->caClients as $miClientId=>$msClientName) {
      $this->caClientsSelect[] = array($miClientId,$msClientName);
    }
    return serialize($this->caClientsSelect);
  }
}
?>