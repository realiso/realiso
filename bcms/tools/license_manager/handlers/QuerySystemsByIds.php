<?php
/**
 * MINI PROJECTS
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QuerySystemsByIds.
 *
 * <p>Classe que busca informa��es de sistemas atrav�s de um array de ids.</p>
 * @package MINI PROJECTS
 * @subpackage handlers
 */
class QuerySystemsByIds extends FWDDBQueryHandler {
	
	protected $caSystemsIds = array();
	
	protected $caSystems = array();
	
  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField('skSystem','skSystem',DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('zName'   ,'zName'   ,DB_STRING));
  }

	public function makeQuery() {
		$msWhere = "";
		
		foreach($this->caSystemsIds as $miSystemId)
			if ($miSystemId)
				$msWhere .= ($msWhere?" OR ":" WHERE ")."skSystem = $miSystemId";
		
		$this->csSQL = "
SELECT skSystem as skSystem, 
       zName as zName
	FROM lic_system
	$msWhere
	ORDER BY zName
";
	}
	
	public function executeQuery() {
		parent::executeQuery();		
		while ($this->coDataSet->fetch()) {
			$miSystemId = $this->coDataSet->getFieldByAlias("skSystem")->getValue();
			$this->caSystems[$miSystemId] = $this->coDataSet->getFieldByAlias("zName")->getValue();
		}
	}
	
	public function setSystemsIds($paSystemsIds) {
		$this->caSystemsIds = $paSystemsIds;
	}
	
	public function getSystems() {
		return $this->caSystems;
	}
}
?>