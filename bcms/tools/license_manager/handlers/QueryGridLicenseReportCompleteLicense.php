<?php
/**
 * MINI PROJECTS
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGridLicenseReportCompleteLicense.
 *
 * <p>Classe que busca informa��es para gerar o relat�rio de licen�as.</p>
 * @package MINI PROJECTS
 * @subpackage handlers
 */
class QueryGridLicenseReportCompleteLicense extends FWDDBQueryHandler {
	
  protected $caFilters=array();
  
	public function __construct($poDB) {
		parent::__construct($poDB);

		$this->coDataSet->addFWDDBField(new FWDDBField("c.zName","client_name", DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField("s.zName","system_name", DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField("la1.zValue","initial_date", DB_DATETIME));
    $this->coDataSet->addFWDDBField(new FWDDBField("la2.zValue","period_value", DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("la3.zValue","period_type", DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("la4.zValue","value", DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("la5.zValue","expiracy_str", DB_DATETIME));	
	}
	
	public function makeQuery() {
		$this->csSQL = "
                    SELECT 
                      c.zName as client_name,
                      s.zName as system_name,
                      la1.zValue as initial_date,
                      la2.zValue as period_value,
                      la3.zValue as period_type,
                      la4.zValue as value,
                      la5.zValue as expiracy_str
                    FROM 
                      lic_license_system_client lsc 
                      JOIN lic_system_client sc ON (lsc.sksystemclient=sc.sksystemclient)
                      JOIN lic_client c ON (sc.skclient=c.skclient)
                      JOIN lic_system s ON (sc.sksystem=s.sksystem)
                      
                      JOIN lic_license_attribute la1 ON (la1.sklicense=lsc.sklicense)    
                      JOIN lic_attribute a1 ON (la1.skattribute=a1.skattribute AND a1.zname='initial_date')
                      
                      JOIN lic_license_attribute la2 ON (la2.sklicense=lsc.sklicense)    
                      JOIN lic_attribute a2 ON (la2.skattribute=a2.skattribute AND a2.zname='period_value')
                      
                      JOIN lic_license_attribute la3 ON (la3.sklicense=lsc.sklicense)    
                      JOIN lic_attribute a3 ON (la3.skattribute=a3.skattribute AND a3.zname='period_type')
                      
                      JOIN lic_license_attribute la4 ON (la4.sklicense=lsc.sklicense)    
                      JOIN lic_attribute a4 ON (la4.skattribute=a4.skattribute AND (a4.zname='users' OR a4.zname='capacity'))
                      
                      JOIN lic_license_attribute la5 ON (la5.sklicense=lsc.sklicense)    
                      JOIN lic_attribute a5 ON (la5.skattribute=a5.skattribute AND a5.zname='expiracy')

                      WHERE (sc.sksystem=22 OR sc.sksystem=23) 
                    ";
  }
}
?>