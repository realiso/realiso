<?php
/**
 * MINI PROJECTS
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryClientsByIds.
 *
 * <p>Classe que busca informa��es de clientes atrav�s de um array de ids.</p>
 * @package MINI PROJECTS
 * @subpackage handlers
 */
class QueryClientsByIds extends FWDDBQueryHandler {
	
	protected $caClientsIds = array();
	
	protected $caClients = array();
	
  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField('skClient','skClient',DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('zName'   ,'zName'   ,DB_STRING));
  }

	public function makeQuery() {
		$msWhere = "";
		
		foreach($this->caClientsIds as $miClientId)
			if ($miClientId)
				$msWhere .= ($msWhere?" OR ":" WHERE ")."skClient = $miClientId";
		
		$this->csSQL = "
SELECT skClient as skClient, 
       zName as zName
	FROM lic_client
	$msWhere
	ORDER BY zName
";
	}
	
	public function executeQuery() {
		parent::executeQuery();		
		while ($this->coDataSet->fetch()) {
			$miClientId = $this->coDataSet->getFieldByAlias("skClient")->getValue();
			$this->caClients[$miClientId] = $this->coDataSet->getFieldByAlias("zName")->getValue();
		}
	}
	
	public function setClientsIds($paClientsIds) {
		$this->caClientsIds = $paClientsIds;
	}
	
	public function getClients() {
		return $this->caClients;
	}
}
?>