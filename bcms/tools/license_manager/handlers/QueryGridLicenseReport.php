<?php
/**
 * MINI PROJECTS
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGridLicenseReport.
 *
 * <p>Classe que busca informa��es para gerar o relat�rio de licen�as.</p>
 * @package MINI PROJECTS
 * @subpackage handlers
 */
class QueryGridLicenseReport extends FWDDBQueryHandler {
	
  protected $caFilters=array();
  
	public function __construct($poDB) {
		parent::__construct($poDB);

		$this->coDataSet->addFWDDBField(new FWDDBField("c.zName","client_name", DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField("s.zName","system_name", DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField("la.zValue","expiracy_str", DB_DATETIME));		
	}
	
	public function makeQuery() {
    $this->caFilters[] = " a.zname='expiracy' ";
    $this->caFilters[] = " (sc.sksystem=22 OR sc.sksystem=23) ";

    $msWhere = count($this->caFilters)?"WHERE ".implode(" AND ",$this->caFilters):"";
		$this->csSQL = "
SELECT c.zName as client_name,
       s.zName as system_name,
       la.zValue as expiracy_str
  FROM lic_license_attribute la
    JOIN lic_license_system_client lsc ON (la.sklicense=lsc.sklicense)
    JOIN lic_system_client sc ON (lsc.sksystemclient=sc.sksystemclient)
    JOIN lic_client c ON (sc.skclient=c.skclient)
    JOIN lic_system s ON (sc.sksystem=s.sksystem)
    JOIN lic_attribute a ON (la.skattribute=a.skattribute)
  $msWhere
";
	}
  
  public function setActiveClients() {
    $this->caFilters[] = " (a.zname='expiracy' AND la.zValue >= '".time()."' AND lsc.nsystemactivated=1) ";
  }
  
  public function setClientId($piClientId) {
    $this->caFilters[] = " sc.skclient=$piClientId ";
  }
  
  public function setExpiredLicenses() {
    $this->caFilters[] = " (a.zname='expiracy' AND la.zValue < '".time()."') ";
  }
  
  public function setSystemId($piSystemId) {
    $this->caFilters[] = " sc.sksystem=$piSystemId ";
  }
  
  public function setActiveNotActivated() {
    $this->caFilters[] = " (a.zname='expiracy' AND la.zValue >= '".time()."' AND lsc.nsystemactivated=0) ";
  }
}
?>