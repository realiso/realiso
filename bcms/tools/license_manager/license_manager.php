<?php

include_once "include.php";
include_once $handlers_ref."select/QuerySelectSystem.php";
include_once $handlers_ref."select/QuerySelectClient.php";
include_once $handlers_ref."QueryAttributesInfoBySystemIdAndClientId.php";

class SaveNewAttributeValueEvent extends FWDRunnable {
	public function run() {
		$miSystemId = FWDWebLib::getObject("system_id")->getValue();
		$miClientId = FWDWebLib::getObject("client_id")->getValue();
		$miAttributeId = FWDWebLib::getObject("attribute_id")->getValue();
		
    $moAttribute = new MPAttribute();
    $moAttribute->fetchById($miAttributeId);
    $msAttributeName = $moAttribute->getFieldValue('attribute_name');
    
    $moSystemClientAttribute = new MPSystemClientAttribute();
    
    if ($msAttributeName == "expiracy_str") {
      $miExpiracy = FWDWebLib::getObject('expiracy_date')->getTimestamp();
      $msExpiracyStr = date("d/m/Y",$miExpiracy);
      $miExpiracyId = FWDWebLib::getObject("var_attribute_expiracy_id")->getValue();
      $miExpiracyStrId = FWDWebLib::getObject("var_attribute_expiracy_str_id")->getValue();
            
      // atualizar o atributo 'expiracy' (timestamp)
      $moSystemClientAttribute->createFilter($miSystemId,"system_id");
      $moSystemClientAttribute->createFilter($miClientId,"client_id");
      $moSystemClientAttribute->createFilter($miExpiracyId,"attribute_id");
      $moSystemClientAttribute->setFieldValue("attribute_value",$miExpiracy);
      $moSystemClientAttribute->update();
      
      // remover o filtro de 'attribute_id' para setar o 'expiracy_str'
      $moSystemClientAttribute->removeFilters("attribute_id");
      
      // atualizar o atributo 'expiracy_str' (string)
      $moSystemClientAttribute->createFilter($miExpiracyStrId,"attribute_id");
      $moSystemClientAttribute->setFieldValue("attribute_value",$msExpiracyStr);
      $moSystemClientAttribute->update();
    } else {
      $msAttributeValue = FWDWebLib::getObject('attribute_value')->getValue();
  		$moSystemClientAttribute->createFilter($miSystemId,"system_id");
  		$moSystemClientAttribute->createFilter($miClientId,"client_id");
  		$moSystemClientAttribute->createFilter($miAttributeId,"attribute_id");
  		$moSystemClientAttribute->setFieldValue("attribute_value",$msAttributeValue);
  		$moSystemClientAttribute->update();
    }
		
		// atualiza o select de atributos
		echo "populate_attributes();";
	}
}

class GenerateLicenseEvent extends FWDRunnable {

	public function run() {
	
		global $ssPublicKey, $ssPrivateKey;
		
		$moLicense = new FWDLicense($ssPublicKey);
		$moLicense->setPrivateKey($ssPrivateKey);
		
		$miSystemId = FWDWebLib::getObject("system_id")->getValue();
		$miClientId = FWDWebLib::getObject("client_id")->getValue();
    $moAttrQuery = new QueryAttributesInfoBySystemIdAndClientId(FWDWebLib::getConnection());
		$moAttrQuery->setSystemId($miSystemId);
		$moAttrQuery->setClientId($miClientId);
		$moAttrQuery->makeQuery();
		$moAttrQuery->executeQuery();
		$mbCanGenerateLicense = true;
		foreach ($moAttrQuery->getAttributes() as $miAttrId => $maAttrInfo) {
      if ($maAttrInfo["name"]=="system_hash") continue;
			$moLicense->setAttribute($maAttrInfo["name"], $maAttrInfo["value"]);
			if (trim($maAttrInfo["value"])=="") $mbCanGenerateLicense = false;
		}
		if ($mbCanGenerateLicense) {
			$msLicense = $moLicense->generateLicense();
			$msLicense = str_replace("\n","<BR/>",$msLicense);
			$msLicense = str_replace("\r","",$msLicense);
			echo "gobi('license').setValue('$msLicense');";
      echo "js_show('btn_save_license');";
		}
		else {
			echo "alert('N�o foi poss�vel gerar a licen�a, pois h� campos vazios!');";
      echo "gobi('license').setValue('');";
		}
	}
}

class DownloadLicenseEvent extends FWDRunnable {
  public function run() {
    $miClientId = FWDWebLib::getObject("client_id")->getValue();
    $moClient = new MPClient();
    $moClient->createFilter($miClientId,"client_id");
    $moClient->select();
    $moClient->fetch();
    $msClientName = $moClient->getFieldValue('client_name');
    $msClientName = str_replace(" ","_",$msClientName);
    $msClientName = strtolower($msClientName);
    $msCurrDate = date("Ymd");
    
    header('Cache-Control: ');// leave blank to avoid IE errors
    header('Pragma: ');// leave blank to avoid IE errors
    header('Content-type: text/plain');
    header('Content-Disposition: attachment; filename="license_'.$msClientName.'_'.$msCurrDate.'.txt"');
    $msLicense = FWDWebLib::getObject('license_text')->getValue();
    $msLicense = str_ireplace("<br>","\r\n",$msLicense);
    echo $msLicense;
  }
}


class PopulateSystemEvent extends FWDRunnable {
  public function run() {
    $msFixed = FWDWebLib::getObject('fixed')->getValue();
    $moQuerySelectSystem = new QuerySelectSystem(FWDWebLib::getConnection());
    if ($msFixed=="client") {
      $miSelectedClient = FWDWebLib::getObject('client_id')->getValue();
      $moQuerySelectSystem->setClient($miSelectedClient);
    }
    $moQuerySelectSystem->makeQuery();
    $moQuerySelectSystem->executeQuery();
    $msSystems = $moQuerySelectSystem->getSelect();
    echo "js_populate_select('system_select', '$msSystems');
          update_selects('system');
         ";
  }
}

class PopulateClientEvent extends FWDRunnable {
  public function run() {
    $msFixed = FWDWebLib::getObject('fixed')->getValue();
    $moQuerySelectClient = new QuerySelectClient(FWDWebLib::getConnection());
    if ($msFixed=="system") {
      $miSelectedSystem = FWDWebLib::getObject('system_id')->getValue();
      $moQuerySelectClient->setSystem($miSelectedSystem);
    }
    $moQuerySelectClient->makeQuery();
    $moQuerySelectClient->executeQuery();
    $msClients = $moQuerySelectClient->getSelect();
    echo "js_populate_select('client_select', '$msClients');
          update_selects('client');
         ";
  }
}

class PopulateAttributesEvent extends FWDRunnable {
  public function run() {
    $miSystemId = FWDWebLib::getObject("system_id")->getValue();
    $miClientId = FWDWebLib::getObject("client_id")->getValue();
    if ($miSystemId && $miClientId) {
      $moAttrQuery = new QueryAttributesInfoBySystemIdAndClientId(FWDWebLib::getConnection());
      $moAttrQuery->setSystemId($miSystemId);
      $moAttrQuery->setClientId($miClientId);
      $moAttrQuery->makeQuery();
      $moAttrQuery->executeQuery();
      $mbCanGenerateLicense = true;
      
      $maAssociativeAttrInfo = array();
      $maInfo = array();
      foreach ($moAttrQuery->getAttributes() as $miAttrId => $maAttrInfo) {
        $pbNext=false;
        switch($maAttrInfo["name"]) {
          case "initial_date":$pbNext=true;break;
          case "period_value":$pbNext=true;break;
          case "period_type":$pbNext=true;break;
          default:break;
        }
        if ($pbNext) continue;
        $maInfo[] = array($miAttrId,$maAttrInfo["name"]);
        $msValue = $maAttrInfo["value"];
        $msValue = str_replace("\n","",$msValue);
        $msValue = str_replace("\r","",$msValue);
        
        $maAssociativeAttrInfo[] = array($miAttrId,$maAttrInfo["name"],$maAttrInfo["description"],trim($msValue));
        if (trim($maAttrInfo["value"])=="") $mbCanGenerateLicense = false;
        if ($maAttrInfo["name"]=="expiracy")
          echo "gebi('var_attribute_expiracy_id').value = '$miAttrId';";
        else if ($maAttrInfo["name"]=="expiracy_str")
          echo "gebi('var_attribute_expiracy_str_id').value = '$miAttrId';";
        else if ($maAttrInfo["name"]=="system_hash")
          echo "gebi('var_attribute_system_hash_id').value = '$miAttrId';";
      }
      $msInfo = serialize($maInfo);
      $msAssociativeAttrInfo = serialize($maAssociativeAttrInfo);
      
      // popula o select de atributos
      echo "js_populate_select('attribute_select', '$msInfo');
            gebi('attributes_info').value = '$msAssociativeAttrInfo';
            create_associative_attribute_array();
            set_attribute();
           ";
    } else {
      echo "js_clear_select('attribute_select');
            gebi('attribute_id').value = 0;
            js_hide('btn_save_license');
           ";
      return false;
    }
  }
}

class GenerateValidationHashEvent extends FWDRunnable {
  public function run() {
    global $ssPublicKey, $ssPrivateKey;
    $moLicense = new FWDLicense($ssPublicKey);
    $moLicense->setPrivateKey($ssPrivateKey);
    $msAttributeValue = FWDWebLib::getObject('attribute_value')->getValue();
    $msValidationHash = $moLicense->generateActivationHash($msAttributeValue);
    $msValidationHash = str_replace("\n","<BR/>",$msValidationHash);
    $msValidationHash = str_replace("\r","",$msValidationHash);
    echo "set_attribute();gobi('license').setValue('$msValidationHash');";
  }
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));
		
    $moStartEvent->addAjaxEvent(new GenerateLicenseEvent("generate_license_event"));
    $moStartEvent->addSubmitEvent(new DownloadLicenseEvent("download_license"));
    $moStartEvent->addAjaxEvent(new PopulateSystemEvent("populate_system"));
    $moStartEvent->addAjaxEvent(new PopulateClientEvent("populate_client"));
    $moStartEvent->addAjaxEvent(new PopulateAttributesEvent("populate_attributes"));
    $moStartEvent->addAjaxEvent(new SaveNewAttributeValueEvent("save_new_attribute_value_event"));
    $moStartEvent->addAjaxEvent(new GenerateValidationHashEvent("generate_validation_hash"));
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
    
		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
?>
		<script language="javascript">
    function sys_ok() {
      return (gebi('system_id').value!=0) ? true : false;
    }
    
    function cli_ok() {
      return (gebi('client_id').value!=0) ? true : false;
    }
    
    function fix_system() {
      gebi('fixed').value = 'system';
      js_hide('system_name');
      js_show('system_name_fixed');
      js_show('client_name');
      js_hide('client_name_fixed');
    }
    
    function fix_client() {
      gebi('fixed').value = 'client';
      js_show('system_name');
      js_hide('system_name_fixed');
      js_hide('client_name');
      js_show('client_name_fixed');
    }
    
    function populate_system() {
      trigger_event('populate_system',3);
    }
    
    function populate_client() {
      trigger_event('populate_client',3);
    }
    
    function populate_attributes() {
      trigger_event('populate_attributes',3);
    }
    
		function update_selects(origin) {
      msFixed = gebi('fixed').value;
      if (origin=='all') {
        trigger_event('populate_'+msFixed,3);
      } else if (msFixed=='system' && origin==msFixed) {
        gebi('system_id').value = gebi('system_select').value;
        populate_client();
      } else if (msFixed=='client' && origin==msFixed) {
        gebi('client_id').value = gebi('client_select').value;
        populate_system()
      } else {
        gebi('system_id').value = gebi('system_select').value;
        gebi('client_id').value = gebi('client_select').value;
        populate_attributes();
      }
    }
    
    var maAttributes = new Array();
    function create_associative_attribute_array() {
      if ( (sys_ok()) && (cli_ok()) && (gebi('attributes_info').value) ) {
        var maAttrInfo = soUnSerializer.unserialize(gebi('attributes_info').value);
        var maInfo = new Array();
        var i=0;
        for (i in maAttrInfo) {
          maInfo = maAttrInfo[i];
          miAttrId = maInfo[0];
          maAttributes[miAttrId] = maAttrInfo[i];
        }
      }
    }
    
    function set_attribute() {
        var miAttributeSelectId = gebi('attribute_select').value;
        var maInfo = maAttributes[miAttributeSelectId];

        gebi('attribute_id').value = gebi('attribute_select').value
        gebi('attribute_name').value = maInfo[1];
        gebi('attribute_description').value = maInfo[2];
        gebi('attribute_value').value = maInfo[3];
        
        gebi('attribute_value').disabled=false;
        js_show('attribute_value');
        js_hide('expiracy_date_viewgroup');
        js_hide('btn_validation');
        
        if (miAttributeSelectId == gebi('var_attribute_expiracy_id').value) {
          gebi('attribute_value').disabled=true;
          js_show('attribute_value');
          js_hide('expiracy_date_viewgroup');
        } else if (miAttributeSelectId == gebi('var_attribute_expiracy_str_id').value) {
          gebi('expiracy_date').value=maInfo[3];
          gebi('attribute_value').disabled=true;  
          js_hide('attribute_value');
          js_show('expiracy_date_viewgroup');
        } else if (miAttributeSelectId == gebi('var_attribute_system_hash_id').value) {  
          js_show('btn_validation');
        }
        
        miAttributeSelectId=null;
        maInfo=null;
        gobi('license').setValue('');
        js_hide('btn_save_license');
    }
		</script>
<?
	}

}
$ssPublicKey = "-----BEGIN CERTIFICATE-----
MIIEFzCCA4CgAwIBAgIBATANBgkqhkiG9w0BAQQFADCBujELMAkGA1UEBhMCQlIx
GjAYBgNVBAgTEVJpbyBHcmFuZGUgZG8gU3VsMRUwEwYDVQQHEwxQb3J0byBBbGVn
cmUxIjAgBgNVBAoTGUF4dXIgSW5mb3JtYXRpb24gU2VjdXJpdHkxFjAUBgNVBAsT
DWxpY2Vuc2UgZGVwdC4xHDAaBgNVBAMTE0F4dXIgTGljZW5zZSBTeXN0ZW0xHjAc
BgkqhkiG9w0BCQEWD2Fsc0BheHVyLmNvbS5icjAeFw0wNDA2MDkxNzU0NTBaFw0w
OTA2MTAxNzU0NTBaMIG6MQswCQYDVQQGEwJCUjEaMBgGA1UECBMRUmlvIEdyYW5k
ZSBkbyBTdWwxFTATBgNVBAcTDFBvcnRvIEFsZWdyZTEiMCAGA1UEChMZQXh1ciBJ
bmZvcm1hdGlvbiBTZWN1cml0eTEWMBQGA1UECxMNbGljZW5zZSBkZXB0LjEcMBoG
A1UEAxMTQXh1ciBNb25pdG9yIFN5c3RlbTEeMBwGCSqGSIb3DQEJARYPYW1zQGF4
dXIuY29tLmJyMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCzKZ+FEPy6DbvF
IOQ6hp6pCnth0kr5FFSh39pMsq0ytv4RRAxnWxjPvyfx0w7vpnKbARF8zDaStN5K
JIxocA7SYmYM2ItYZdAu+6JU1m9/EYT8eb7w110cYdEIJJr+CPrx2iivXVYBbl9J
VkKlf/1xFnkwhIlZ6CYAwFfnxdIswwIDAQABo4IBKTCCASUwCQYDVR0TBAIwADAP
BglghkgBhvhCAQ0EAhYAMB0GA1UdDgQWBBTAEH8Y7ZVtOpNxtuO5djrXmDp59zCB
5wYDVR0jBIHfMIHcgBTIsD9pVTsIJx25scfn1t6xQmdPZaGBwKSBvTCBujELMAkG
A1UEBhMCQlIxGjAYBgNVBAgTEVJpbyBHcmFuZGUgZG8gU3VsMRUwEwYDVQQHEwxQ
b3J0byBBbGVncmUxIjAgBgNVBAoTGUF4dXIgSW5mb3JtYXRpb24gU2VjdXJpdHkx
FjAUBgNVBAsTDWxpY2Vuc2UgZGVwdC4xHDAaBgNVBAMTE0F4dXIgTGljZW5zZSBT
eXN0ZW0xHjAcBgkqhkiG9w0BCQEWD2Fsc0BheHVyLmNvbS5icoIBADANBgkqhkiG
9w0BAQQFAAOBgQAabwBzSL+Ee3up3MxfE0gz4UAmjtaJMTlmclz+fGX22CVVwMXX
1WhH4QacRA9UnZp5tYEAwPMXpOGrlgR0tAkI3O5SaLaki6qc07h5mqfczN2L2BMp
LvUwF7gTTmNITm+ooJRP96Q+16G/UhUp4FNQvJyG2OmovKnH/8ZKuGgwlQ==
-----END CERTIFICATE----- ";

$ssPrivateKey = "-----BEGIN RSA PRIVATE KEY-----
MIICXAIBAAKBgQCzKZ+FEPy6DbvFIOQ6hp6pCnth0kr5FFSh39pMsq0ytv4RRAxn
WxjPvyfx0w7vpnKbARF8zDaStN5KJIxocA7SYmYM2ItYZdAu+6JU1m9/EYT8eb7w
110cYdEIJJr+CPrx2iivXVYBbl9JVkKlf/1xFnkwhIlZ6CYAwFfnxdIswwIDAQAB
AoGAZmVTlmmvB2bx+ihiSFDIPTSQb8gQsWq9NAcMbOlxs5qCeru5mxilhZZq7fsC
flVTCuQBaqIMTqywnw9kcAwba42j8v1rTfM95tXqLSUigQO5QvEQ6YqKOG9wDIz9
4C52w9mbHzwBUQbKrvZ8tL/4rtVuFsVhJJQ0s8FAqm1j+wECQQDWklvP5BHLLkEW
cPAbXw/8ZYJD1GqQ9YsRIBl+EqWsgYy5aPZ4RCrh0c3CaZ/sHwFOE9CKNlJb6I36
yWRPerBZAkEA1cEYzWUJ0AaviW/sceh6MbQnk7diHA74Qva6ZikdxaBPQSAA6Tit
7ESA7YxlgS/OD0dwB4CP/Hvx6S0blpzCewJBALuZnQInAlOlbizDs3oK5DwlN/47
8qwoslzXttIeVZF8duAIYC2IVAG54G8Q0EyuUwLDmjmtAtbufWv9tmEzAIkCQCmv
r9OWqdQ9CYzHHBiW2wXIeZNwRxzrunTswytbR2gcPHiZ8jOJjzmtnms1XzJTjV8j
cnC0HOCDl4j++AtmZPMCQBZswfRiZYlHLrgzm+SRfwDfNzeLR9qmMPgsdPO2dx/y
pSQ9YXv6lnY2yTQ1Qtt+ePB4IYOFM9N6XCXDvxdZio0=
-----END RSA PRIVATE KEY----- ";

$soWebLib = FWDWebLib::getInstance();

$soStartEvent = FWDStartEvent::getInstance();
$soStartEvent->addBeforeAjax(new ScreenBeforeEvent(""));

$soWebLib->xml_load("license_manager.xml");

?>