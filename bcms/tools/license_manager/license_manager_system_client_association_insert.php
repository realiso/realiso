<?php

include_once "include.php";

include_once $handlers_ref."QuerySystemsByClientId.php";
include_once $handlers_ref."QueryAttributesBySystemId.php";

class InsertSystemClientAssociationEvent extends FWDRunnable {

	public function run() {
		$miSystemId = FWDWebLib::getObject("system_id")->getValue();
		$miClientId = FWDWebLib::getObject("param_cli_id")->getValue();
		
    $moMPClient = new MPClient();
    $msClientName="";
    if ($moMPClient->fetchById($miClientId)) {
      $msClientName=$moMPClient->getFieldValue("client_name");
    }
    
		$moQuery = new QueryAttributesBySystemId(FWDWebLib::getConnection());
		$moQuery->setSystemId($miSystemId);
		$moQuery->makeQuery();
		$moQuery->executeQuery();
		$maAttributesIds = $moQuery->getAttributes();
		
		$moSystemClientAttribute = new MPSystemClientAttribute();
		foreach ($maAttributesIds as $miAttrId=>$msAttrName) {
      if($msAttrName == "client") {
        $moSystemClientAttribute->setFieldValue("system_id",$miSystemId);
        $moSystemClientAttribute->setFieldValue("client_id",$miClientId);
        $moSystemClientAttribute->setFieldValue("attribute_id",$miAttrId);
        $moSystemClientAttribute->setFieldValue("attribute_value",$msClientName);
        $moSystemClientAttribute->setFieldValue("license_id",0);
        $moSystemClientAttribute->insert();
      } else {
  			$moSystemClientAttribute->setFieldValue("system_id",$miSystemId);
  			$moSystemClientAttribute->setFieldValue("client_id",$miClientId);
  			$moSystemClientAttribute->setFieldValue("attribute_id",$miAttrId);
  			$moSystemClientAttribute->setFieldValue("attribute_value","");
        $moSystemClientAttribute->setFieldValue("license_id",0);
  			$moSystemClientAttribute->insert();
      }
		}
		
		echo "var soPopUp = soPopUpManager.getPopUpById('license_manager_system_client_association');
					if(soPopUp) {
						var soWindow = soPopUp.getWindow();
						soWindow.refresh_system_select();
					}";
		echo "soPopUpManager.closePopUp('license_manager_system_client_association_insert');";
	}

}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));
		
		$moStartEvent->addAjaxEvent(new InsertSystemClientAssociationEvent("insert_system_client_association_event"));
		$miClientId = FWDWebLib::getObject("param_cli_id")->getValue();
    $moSystemSelect = FWDWebLib::getObject("system_select");
    
    // popula select de sistema do cliente atual
    $moQuerySystemByClient = new QuerySystemsByClientId(FWDWebLib::getConnection());
    $moQuerySystemByClient->setClientId($miClientId);
    $moQuerySystemByClient->setNotIn(true);
    $moQuerySystemByClient->makeQuery();
    $moQuerySystemByClient->executeQuery();
    foreach ($moQuerySystemByClient->getSystems() as $miSysId => $msSysName)
      $moSystemSelect->setItemValue($miSysId,$msSysName);
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
	}

}

$soWebLib = FWDWebLib::getInstance();

$soStartEvent = FWDStartEvent::getInstance();
$soStartEvent->addBeforeAjax(new ScreenBeforeEvent(""));

$soWebLib->xml_load("license_manager_system_client_association_insert.xml");

?>