<?php
include_once "include.php";

class RemoveSystemEvent extends FWDRunnable {
  public function run(){    
    $miSystemId = FWDWebLib::getObject('element_id')->getValue();
    $moMPSystem = new MPSystem();
    $moMPSystem->createFilter($miSystemId,"system_id");
    $moMPSystem->delete();
    
    echo "soPopUp = soPopUpManager.getPopUpById('popup_confirm');
          soWindow = soPopUp.getOpener();
          soWindow.populate_system();
          soPopUpManager.closePopUp('popup_confirm');
         ";
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new RemoveSystemEvent("remove_system"));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $miSystemId = FWDWebLib::getObject("element_id")->getValue();
    $msTitle = FWDLanguage::getPHPStringValue('system_remove_title','Remover Sistema');
    $msMessage = FWDLanguage::getPHPStringValue('system_remove_message',"Voc� tem certeza que deseja excluir o sistema <b>%system_name%</b>?");
    $msIconSrc = 'gfx/'.'icon-exclamation.gif';
    
    $moMPSystem = new MPSystem();
    $moMPSystem->fetchById($miSystemId);
    $msSystemName = $moMPSystem->getFieldValue('system_name');
    $msMessage = str_replace("%system_name%",$msSystemName,$msMessage);
    
    //FWDWebLib::getObject('confirm_title')->setValue($msTitle);
    FWDWebLib::getObject('confirm_message')->setValue($msMessage);
    FWDWebLib::getObject('confirm_icon')->setAttrSrc($msIconSrc);
    
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
    <script language="javascript">
      function remove_element() {
        trigger_event("remove_system",3);
      }
    </script>
    <?
  }
}

$soWebLib = FWDWebLib::getInstance();

$soStartEvent = FWDStartEvent::getInstance();
$soStartEvent->addBeforeAjax(new ScreenBeforeEvent(""));

$soWebLib->xml_load("confirm.xml");
?>
