<?php
include_once "include.php";
include_once $handlers_ref."/QueryClientList.php";
include_once $handlers_ref."/QuerySystemList.php";
include_once $handlers_ref."/QuerySystemsByClientId.php";

class InitEvent extends FWDRunnable {
  public function run() {
    $maSystemAttributes = array(22=>array(76,77,80,81,82,137,149,150,151),23=>array(85,86,92,145,146,147,148,152));
    
    $moQueryClientList = new QueryClientList(FWDWebLib::getConnection());
    $moQueryClientList->setSystemsIds(array_keys($maSystemAttributes));
    $moQueryClientList->makeQuery();
    $moQueryClientList->executeQuery();
    $maClients = $moQueryClientList->getClients();
    $msClients = serialize($maClients);

    $moQuerySystemList = new QuerySystemList(FWDWebLib::getConnection());
    $moQuerySystemList->setSystemsIds(array_keys($maSystemAttributes));
    $moQuerySystemList->makeQuery();
    $moQuerySystemList->executeQuery();
    $maSystems = $moQuerySystemList->getSystems();
    $msSystems = serialize($maSystems);
    $maSystemsSelect = array();
    foreach($maSystems as $miSystemId=>$msSystemName) {
      $maSystemsSelect[]=array($miSystemId,$msSystemName);
    }
    $msSystemsSelect = serialize($maSystemsSelect);
    
    $maSystemsByClient = array();
    foreach($maClients as $maClient) {
      $miClientId = $maClient[0];
      $moQuerySystemsByClientId = new QuerySystemsByClientId(FWDWebLib::getConnection());
      $moQuerySystemsByClientId->setClientId($miClientId);
      $moQuerySystemsByClientId->makeQuery();
      $moQuerySystemsByClientId->executeQuery();
      $maSystemsByClient[$miClientId] = array_keys($moQuerySystemsByClientId->getSystems());
    }
    $msSystemsByClient = count($maSystemsByClient)?serialize($maSystemsByClient):"";
    
    echo "js_populate_select('select_client_report', '$msClients');
          js_populate_select('select_system_report', '$msSystemsSelect');
         ";
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new InitEvent("init_event"));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        trigger_event('init_event',3);
      </script>
    <?
  }
}
$soWebLib = FWDWebLib::getInstance();
$soStartEvent = FWDStartEvent::getInstance();
$soStartEvent->addBeforeAjax(new ScreenBeforeEvent(""));
$soWebLib->xml_load("nav_reports.xml");
?>
