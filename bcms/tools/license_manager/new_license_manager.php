<?php
include_once "include.php";
include_once $handlers_ref."QueryClientList.php";
include_once $handlers_ref."QuerySystemList.php";
include_once $handlers_ref."QuerySystemsByClientId.php";
include_once $handlers_ref."QueryLicenseListBySystemIdAndClientId.php";
include_once $handlers_ref."QueryAttributesInfoByLicenseId.php";

class StaticVariables {
  static $saSystemViewgroup = array(22=>"viewgroup_isms",23=>"viewgroup_ams");
  static $saSystemAttributes = array(22=>array(76,77,80,81,82,137,149,150,151,159),23=>array(85,86,92,145,146,147,148,152,160));
  static $saBasicAttributes = array(76=>"expiracy",81=>"client_name",77=>"expiracy_str",85=>"expiracy",92=>"client_name",86=>"expiracy_str");
  static $saFieldAttributes = array(82=>"select_users",137=>"system_hash",149=>"initial_date",150=>"period",151=>"select_period",145=>"select_capacity",152=>"system_hash",146=>"initial_date",147=>"period",148=>"select_period", 80=>"module_controller",159=>"activation_period",160=>"activation_period");
  static $ssPublicKey = "-----BEGIN CERTIFICATE-----
MIIEFzCCA4CgAwIBAgIBATANBgkqhkiG9w0BAQQFADCBujELMAkGA1UEBhMCQlIx
GjAYBgNVBAgTEVJpbyBHcmFuZGUgZG8gU3VsMRUwEwYDVQQHEwxQb3J0byBBbGVn
cmUxIjAgBgNVBAoTGUF4dXIgSW5mb3JtYXRpb24gU2VjdXJpdHkxFjAUBgNVBAsT
DWxpY2Vuc2UgZGVwdC4xHDAaBgNVBAMTE0F4dXIgTGljZW5zZSBTeXN0ZW0xHjAc
BgkqhkiG9w0BCQEWD2Fsc0BheHVyLmNvbS5icjAeFw0wNDA2MDkxNzU0NTBaFw0w
OTA2MTAxNzU0NTBaMIG6MQswCQYDVQQGEwJCUjEaMBgGA1UECBMRUmlvIEdyYW5k
ZSBkbyBTdWwxFTATBgNVBAcTDFBvcnRvIEFsZWdyZTEiMCAGA1UEChMZQXh1ciBJ
bmZvcm1hdGlvbiBTZWN1cml0eTEWMBQGA1UECxMNbGljZW5zZSBkZXB0LjEcMBoG
A1UEAxMTQXh1ciBNb25pdG9yIFN5c3RlbTEeMBwGCSqGSIb3DQEJARYPYW1zQGF4
dXIuY29tLmJyMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCzKZ+FEPy6DbvF
IOQ6hp6pCnth0kr5FFSh39pMsq0ytv4RRAxnWxjPvyfx0w7vpnKbARF8zDaStN5K
JIxocA7SYmYM2ItYZdAu+6JU1m9/EYT8eb7w110cYdEIJJr+CPrx2iivXVYBbl9J
VkKlf/1xFnkwhIlZ6CYAwFfnxdIswwIDAQABo4IBKTCCASUwCQYDVR0TBAIwADAP
BglghkgBhvhCAQ0EAhYAMB0GA1UdDgQWBBTAEH8Y7ZVtOpNxtuO5djrXmDp59zCB
5wYDVR0jBIHfMIHcgBTIsD9pVTsIJx25scfn1t6xQmdPZaGBwKSBvTCBujELMAkG
A1UEBhMCQlIxGjAYBgNVBAgTEVJpbyBHcmFuZGUgZG8gU3VsMRUwEwYDVQQHEwxQ
b3J0byBBbGVncmUxIjAgBgNVBAoTGUF4dXIgSW5mb3JtYXRpb24gU2VjdXJpdHkx
FjAUBgNVBAsTDWxpY2Vuc2UgZGVwdC4xHDAaBgNVBAMTE0F4dXIgTGljZW5zZSBT
eXN0ZW0xHjAcBgkqhkiG9w0BCQEWD2Fsc0BheHVyLmNvbS5icoIBADANBgkqhkiG
9w0BAQQFAAOBgQAabwBzSL+Ee3up3MxfE0gz4UAmjtaJMTlmclz+fGX22CVVwMXX
1WhH4QacRA9UnZp5tYEAwPMXpOGrlgR0tAkI3O5SaLaki6qc07h5mqfczN2L2BMp
LvUwF7gTTmNITm+ooJRP96Q+16G/UhUp4FNQvJyG2OmovKnH/8ZKuGgwlQ==
-----END CERTIFICATE----- ";
  static $ssPrivateKey = "-----BEGIN RSA PRIVATE KEY-----
MIICXAIBAAKBgQCzKZ+FEPy6DbvFIOQ6hp6pCnth0kr5FFSh39pMsq0ytv4RRAxn
WxjPvyfx0w7vpnKbARF8zDaStN5KJIxocA7SYmYM2ItYZdAu+6JU1m9/EYT8eb7w
110cYdEIJJr+CPrx2iivXVYBbl9JVkKlf/1xFnkwhIlZ6CYAwFfnxdIswwIDAQAB
AoGAZmVTlmmvB2bx+ihiSFDIPTSQb8gQsWq9NAcMbOlxs5qCeru5mxilhZZq7fsC
flVTCuQBaqIMTqywnw9kcAwba42j8v1rTfM95tXqLSUigQO5QvEQ6YqKOG9wDIz9
4C52w9mbHzwBUQbKrvZ8tL/4rtVuFsVhJJQ0s8FAqm1j+wECQQDWklvP5BHLLkEW
cPAbXw/8ZYJD1GqQ9YsRIBl+EqWsgYy5aPZ4RCrh0c3CaZ/sHwFOE9CKNlJb6I36
yWRPerBZAkEA1cEYzWUJ0AaviW/sceh6MbQnk7diHA74Qva6ZikdxaBPQSAA6Tit
7ESA7YxlgS/OD0dwB4CP/Hvx6S0blpzCewJBALuZnQInAlOlbizDs3oK5DwlN/47
8qwoslzXttIeVZF8duAIYC2IVAG54G8Q0EyuUwLDmjmtAtbufWv9tmEzAIkCQCmv
r9OWqdQ9CYzHHBiW2wXIeZNwRxzrunTswytbR2gcPHiZ8jOJjzmtnms1XzJTjV8j
cnC0HOCDl4j++AtmZPMCQBZswfRiZYlHLrgzm+SRfwDfNzeLR9qmMPgsdPO2dx/y
pSQ9YXv6lnY2yTQ1Qtt+ePB4IYOFM9N6XCXDvxdZio0=
-----END RSA PRIVATE KEY----- ";  
}

class InitEvent extends FWDRunnable {
  public function run() {
    $moQueryClientList = new QueryClientList(FWDWebLib::getConnection());
    $moQueryClientList->setSystemsIds(array_keys(StaticVariables::$saSystemAttributes));
    $moQueryClientList->makeQuery();
    $moQueryClientList->executeQuery();
    $maClients = $moQueryClientList->getClients();
    $msClients = serialize($maClients);

    $moQuerySystemList = new QuerySystemList(FWDWebLib::getConnection());
    $moQuerySystemList->setSystemsIds(array_keys(StaticVariables::$saSystemAttributes));
    $moQuerySystemList->makeQuery();
    $moQuerySystemList->executeQuery();
    $maSystems = $moQuerySystemList->getSystems();
    $msSystems = serialize($maSystems);
    $maSystemsSelect = array();
    foreach($maSystems as $miSystemId=>$msSystemName) {
      $maSystemsSelect[]=array($miSystemId,$msSystemName);
    }
    $msSystemsSelect = serialize($maSystemsSelect);
    
    $maSystemsByClient = array();
    foreach($maClients as $maClient) {
      $miClientId = $maClient[0];
      $moQuerySystemsByClientId = new QuerySystemsByClientId(FWDWebLib::getConnection());
      $moQuerySystemsByClientId->setClientId($miClientId);
      $moQuerySystemsByClientId->makeQuery();
      $moQuerySystemsByClientId->executeQuery();
      $maSystemsByClient[$miClientId] = array_keys($moQuerySystemsByClientId->getSystems());
    }
    $msSystemsByClient = count($maSystemsByClient)?serialize($maSystemsByClient):"";
    
    echo "js_populate_select('select_client', '$msClients');
          js_populate_select('select_client_report', '$msClients');
          js_populate_select('select_system_report', '$msSystemsSelect');
          gebi('select_client').value=gebi('selected_client').value;
          gebi('select_system').value=gebi('selected_system').value;
          gebi('system_ids').value='$msSystems';
          gebi('systems_by_client').value='$msSystemsByClient';
          change_client();
         ";
  }
}

class ChangeSystemEvent extends FWDRunnable {
  public function run() {
    $miClientId = FWDWebLib::getObject('select_client')->getValue();
    $miSystemId = FWDWebLib::getObject('select_system')->getValue();
    if ($miClientId && $miSystemId) {
      $moLicenses = new QueryLicenseListBySystemIdAndClientId(FWDWebLib::getConnection());
      $moLicenses->setClientId($miClientId);
      $moLicenses->setSystemId($miSystemId);
      $moLicenses->makeQuery();
      $moLicenses->executeQuery();
      $maLicenses = $moLicenses->getLicenses();
      
      for($miI=0;$miI<count($maLicenses);$miI++) {
        $maLicenses[$miI][1] = date('d/m/Y H:i:s',strtotime($maLicenses[$miI][1]));
      }
      
      $maNewLicenseValue = array('',FWDLanguage::getPHPStringValue('new','Nova'));
      array_unshift($maLicenses,$maNewLicenseValue);
      $msLicenses = serialize($maLicenses);
      echo "gebi('selected_system').value='$miSystemId';
            js_populate_select('select_license','$msLicenses');
            gebi('select_license').value=gebi('selected_license').value;
            change_license();
           ";
      
      foreach(StaticVariables::$saSystemViewgroup as $miKey => $msSystemViewgroup) {
        if ($miKey == $miSystemId)
          echo "js_show('".$msSystemViewgroup."');";
        else
          echo "js_hide('".$msSystemViewgroup."');";
      }
    }
  }
}

class ChangeLicenseEvent extends FWDRunnable {
  public function run() {
    $miLicenseId = FWDWebLib::getObject('select_license')->getValue();
    if ($miLicenseId) {
      $moAttributes = new QueryAttributesInfoByLicenseId(FWDWebLib::getConnection());
      $moAttributes->setLicenseId($miLicenseId);
      $moAttributes->makeQuery();
      $moAttributes->executeQuery();
      $maAttributes = $moAttributes->getAttributes();
      foreach($maAttributes as $miAttributeId=>$maAttributeInfo) {
        if (!in_array($miAttributeId,array_keys(StaticVariables::$saFieldAttributes))) {
          continue; 
        } else {
          if (StaticVariables::$saFieldAttributes[$miAttributeId] == "initial_date") {
            $msValue = date('d/m/Y',strtotime($maAttributeInfo["value"]));
          } elseif (StaticVariables::$saFieldAttributes[$miAttributeId] == "system_hash") {
            $msValue = trim($maAttributeInfo["value"]);
            $msValue = str_replace("\n","",$msValue);
            $msValue = str_replace("\r","",$msValue);
          } elseif (StaticVariables::$saFieldAttributes[$miAttributeId] == "module_controller") {
          	$maValue = explode('|', $maAttributeInfo["value"]);
          	$msValue = '';
          	echo "if (js_element_in_controller('risk', 'module_controller')) gfx_check('module_controller', 'risk');";
          	echo "if (js_element_in_controller('policy', 'module_controller')) gfx_check('module_controller', 'policy');";
            echo "if (js_element_in_controller('improvement', 'module_controller')) gfx_check('module_controller', 'improvement');";
          	echo "gebi('module_controller').value='';";
          	foreach ($maValue as $msModType) {
          		switch ($msModType) {
          			case 'RM': $msValue.= ':risk'; echo "gfx_check('module_controller', 'risk');"; break;
          			case 'PM': $msValue.= ':policy'; echo "gfx_check('module_controller', 'policy');"; break;
                case 'CI': $msValue.= ':improvement'; echo "gfx_check('module_controller', 'improvement');"; break;
          		}
          	}      
          } elseif (StaticVariables::$saFieldAttributes[$miAttributeId] == "select_users") {
            if (in_array($maAttributeInfo["value"],array(5,20,50,100,99999))) {
              $msValue = $maAttributeInfo["value"];
            } else {
              $msValue = 'C';
              echo "gebi('custom_users').value='".$maAttributeInfo["value"]."';";
            }
          } elseif (StaticVariables::$saFieldAttributes[$miAttributeId] == "select_capacity") {
            if (in_array($maAttributeInfo["value"],array(500,1000,1500,2000,2500,3000,3500,4000,4500,5000,1024000))) {
              $msValue = $maAttributeInfo["value"];
            } else {
              $msValue = 'C';
              echo "gebi('custom_capacity').value='".$maAttributeInfo["value"]."';";
            }
          }else {
            $msValue = $maAttributeInfo["value"];
          }          
          if (StaticVariables::$saFieldAttributes[$miAttributeId] != "module_controller") echo "gebi('".StaticVariables::$saFieldAttributes[$miAttributeId]."').value='".$msValue."';";
        }
      }
      echo "gebi('selected_license').value='$miLicenseId';change_isms_users();change_ams_capacity();";
    }
  }
}

class SaveLicenseDataEvent extends FWDRunnable {
  public function run() {
    $miLicenseId = FWDWebLib::getObject('select_license')->getValue();
    $miSystemId = FWDWebLib::getObject('select_system')->getValue();
    $miClientId = FWDWebLib::getObject('select_client')->getValue();
    
    if (!$miClientId || !$miSystemId) {
      //TODO: mostrar aviso de erro! O usuario conseguiu nao selecionar um cliente e um sistema.
      echo "alert('O usuario conseguiu nao selecionar um cliente e um sistema.');";
      return false;
    }
    
    $moSystemClient = new MPSystemClient();
    $moSystemClient->createFilter($miSystemId,'system_id');
    $moSystemClient->createFilter($miClientId,'client_id');
    $moSystemClient->select();
    if ($moSystemClient->fetch()) {
      $miSystemClientId = $moSystemClient->getFieldValue('system_client_id');
    } else {
      //TODO: mostrar aviso de erro! A associa��o de sistema e cliente nao existe.
      echo "alert('A associa��o de sistema e cliente nao existe.');";
      return false;
    }
    
    $msDate = date('Y-m-d H:i:s',time());
    $moLicense = new MPLicense();
    $moLicense->setFieldValue('system_client_id',$miSystemClientId);
    $moLicense->setFieldValue('date_time',$msDate);
    if ($miLicenseId) {
      $moLicense->createFilter($miLicenseId,'license_id');
      $moLicense->update();
    } else {
      $miLicenseId = $moLicense->insert(true);
    }
    
    if (array_key_exists($miSystemId,StaticVariables::$saSystemAttributes)) {
      foreach(StaticVariables::$saSystemAttributes[$miSystemId] as $miAttributeId) {
        $mbInsert=true;
        if (array_key_exists($miAttributeId,StaticVariables::$saFieldAttributes)) {
          $msField = StaticVariables::$saFieldAttributes[$miAttributeId];
          if ($msField=="initial_date") {
            $msValue = date('Y-m-d',FWDWebLib::getObject($msField)->getTimestamp());
          }
          else if ($msField == "module_controller") {          	
          	$maModules = array_filter(explode(':', FWDWebLib::getObject($msField)->getValue()));          	
          	$maMod = array();
          	foreach ($maModules as $msModule) {
          		switch ($msModule) {
          			case 'risk': $maMod[] = 'RM'; break; 
          			case 'policy': $maMod[] = 'PM'; break;
                case 'improvement': $maMod[] = 'CI'; break;
          		}
          	}
          	$msValue = implode('|', $maMod);
          }	
          else {
            $msValue = FWDWebLib::getObject($msField)->getValue();
            if ($msField=="select_users") {
              switch($msValue) {
                case 5:case 20:case 50:case 100:case 99999:break;
                case 'C': $msValue=FWDWebLib::getObject('custom_users')->getValue();break;
                default: $msValue=5;break;
              }
            }
            if ($msField=="select_capacity") {
              switch($msValue) {
                case 500:case 1000:case 1500:case 2000:case 2500:case 3000:case 3500:case 4000:case 4500:case 5000:case 1024000:break;
                case 'C': $msValue=FWDWebLib::getObject('custom_capacity')->getValue();break;
                default: $msValue=500;break;
              }
            }
          }
        } elseif (array_key_exists($miAttributeId,StaticVariables::$saBasicAttributes)) {
          $msField = StaticVariables::$saBasicAttributes[$miAttributeId];
          switch($msField) {
            case "client_name":
              $moClient = new MPClient();
              $moClient->fetchById($miClientId);
              $msValue = $moClient->getFieldValue('client_name');
              break;
            case "expiracy_str":
              $miDate = FWDWebLib::getObject('initial_date')->getTimestamp();
              $miPeriodValue = intval(FWDWebLib::getObject('period')->getValue());
              $miPeriodType = FWDWebLib::getObject('select_period')->getValue();
              switch($miPeriodType) {
                case 1:$msPeriodType = "day";break;
                case 2:$msPeriodType = "week";break;
                case 3:$msPeriodType = "month";break;
                case 4:$msPeriodType = "year";break;
                default:$msPeriodType = "month";break;
              }
              $msValue = date('d/m/Y',strtotime(date('Y-m-d',$miDate) . " + $miPeriodValue $msPeriodType"));
              break;
            case "expiracy":
              $miDate = FWDWebLib::getObject('initial_date')->getTimestamp();
              $miPeriodValue = intval(FWDWebLib::getObject('period')->getValue());
              $miPeriodType = FWDWebLib::getObject('select_period')->getValue();
              switch($miPeriodType) {
                case 1:$msPeriodType = "day";break;
                case 2:$msPeriodType = "week";break;
                case 3:$msPeriodType = "month";break;
                case 4:$msPeriodType = "year";break;
                default:$msPeriodType = "month";break;
              }
              $msValue = strtotime(date('Y-m-d',$miDate) . " + $miPeriodValue $msPeriodType");
              break;
          }
        } else {
          $mbInsert=false;
        }       
        
        if ($mbInsert) {
          $moAttribute = new MPLicenseAttribute();
          if ($moAttribute->fetch($miLicenseId,$miAttributeId)) {
            $moAttribute->createFilter($miLicenseId,"license_id");
            $moAttribute->createFilter($miAttributeId,"attribute_id");
            $moAttribute->setFieldValue("attribute_value",$msValue);
            $moAttribute->update();
          }
          else { 
            $moAttribute->setFieldValue("license_id",$miLicenseId);
            $moAttribute->setFieldValue("attribute_id",$miAttributeId);
            $moAttribute->setFieldValue("attribute_value",$msValue);
            $moAttribute->insert();
          }
        }
      }
    }
    
    $moLicense = new FWDLicense(StaticVariables::$ssPublicKey);
    $moLicense->setPrivateKey(StaticVariables::$ssPrivateKey);
    $moAttrQuery = new QueryAttributesInfoByLicenseId(FWDWebLib::getConnection());
    $moAttrQuery->setLicenseId($miLicenseId);
    $moAttrQuery->makeQuery();
    $moAttrQuery->executeQuery();
    $mbCanGenerateLicense = true;
    foreach ($moAttrQuery->getAttributes() as $miAttrId => $maAttrInfo) {
      if ($maAttrInfo["name"]=="system_hash") continue;
      if ($maAttrInfo["name"]=="initial_date") continue;
      if ($maAttrInfo["name"]=="period_type") continue;
      if ($maAttrInfo["name"]=="period_value") continue;
      $moLicense->setAttribute($maAttrInfo["name"], $maAttrInfo["value"]);
      if (trim($maAttrInfo["value"])=="") $mbCanGenerateLicense = false;
    }
    $msLicense = $moLicense->generateLicenseKey();
    $msLicense = str_replace("--------------------- BEGIN LICENSE KEY ------------------------\n","",$msLicense);
    $msLicense = str_replace("---------------------- END LICENSE KEY -------------------------\n","",$msLicense);
    $moLicense = new MPLicense();
    $moLicense->createFilter($miLicenseId,'license_id');
    $moLicense->setFieldValue('license_hash',md5(trim($msLicense)));
    $moLicense->update();
    
    echo "gebi('selected_system').value='{$miSystemId}';gebi('selected_license').value='{$miLicenseId}';change_client();";
    $msSaveAction = FWDWebLib::getObject('save_action')->getValue();
    switch($msSaveAction) {
      case "generate_license":
        echo "trigger_event('generate_license',3);";
      break;
      case "generate_activation_code":
        echo "trigger_event('generate_activation_code',3);";
      break;
      default:break;
    }
    echo "gebi('save_action').value='';";
  }
}

class GenerateLicenseEvent extends FWDRunnable {
  public function run() {
    $moLicense = new FWDLicense(StaticVariables::$ssPublicKey);
    $moLicense->setPrivateKey(StaticVariables::$ssPrivateKey);

    $miLicenseId = FWDWebLib::getObject("select_license")->getValue();
    $miClientId = FWDWebLib::getObject("select_client")->getValue();
    $miSystemId = FWDWebLib::getObject("select_system")->getValue();
    if (!$miLicenseId || !$miClientId || !$miSystemId) {
      //TODO: exibe erro... deve selecionar licen�a, cliente e sistema antes de gerar a mesma.
      echo "alert('deve selecionar licen�a, cliente e sistema antes de gerar a mesma.');";
      return false;
    } 
    $moAttrQuery = new QueryAttributesInfoByLicenseId(FWDWebLib::getConnection());
    $moAttrQuery->setLicenseId($miLicenseId);
    $moAttrQuery->makeQuery();
    $moAttrQuery->executeQuery();
    $mbCanGenerateLicense = true;
    foreach ($moAttrQuery->getAttributes() as $miAttrId => $maAttrInfo) {
      if (!in_array($miAttrId,StaticVariables::$saSystemAttributes[$miSystemId])) continue;
      if ($maAttrInfo["name"]=="system_hash") continue;
      if ($maAttrInfo["name"]=="initial_date") continue;
      if ($maAttrInfo["name"]=="period_type") continue;
      if ($maAttrInfo["name"]=="period_value") continue;
      $moLicense->setAttribute($maAttrInfo["name"], $maAttrInfo["value"]);
      if (trim($maAttrInfo["value"])=="") $mbCanGenerateLicense = false;
    }
    if ($mbCanGenerateLicense) {
      $msLicense = $moLicense->generateLicense();
      $msLicense = str_replace("\n","<BR/>",$msLicense);
      $msLicense = str_replace("\r","",$msLicense);
      echo "gobi('license').setValue('$msLicense');";
      echo "js_show('btn_save_license');";
    }
    else {
      echo "alert('N�o foi poss�vel gerar a licen�a, pois h� campos vazios!');";
      echo "gobi('license').setValue('');";
    }
  }
}

class DownloadLicenseEvent extends FWDRunnable {
  public function run() {
    $miClientId = FWDWebLib::getObject("select_client")->getValue();
    $moClient = new MPClient();
    $moClient->createFilter($miClientId,"client_id");
    $moClient->select();
    $moClient->fetch();
    $msClientName = $moClient->getFieldValue('client_name');
    $msClientName = str_replace(" ","_",$msClientName);
    $msClientName = strtolower($msClientName);
    $msCurrDate = date("Ymd");
    
    header('Cache-Control: ');// leave blank to avoid IE errors
    header('Pragma: ');// leave blank to avoid IE errors
    header('Content-type: text/plain');
    header('Content-Disposition: attachment; filename="license_'.$msClientName.'_'.$msCurrDate.'.txt"');
    $msLicense = html_entity_decode(FWDWebLib::getObject('license_text')->getValue());
    $msLicense = str_ireplace("<br>","\r\n",$msLicense);
    echo $msLicense;
  }
}

class GenerateActivationCodeEvent extends FWDRunnable {
  public function run() {
    $moLicense = new FWDLicense(StaticVariables::$ssPublicKey);
    $moLicense->setPrivateKey(StaticVariables::$ssPrivateKey);
    
    $miLicenseId = FWDWebLib::getObject("select_license")->getValue();
    $moAttrQuery = new QueryAttributesInfoByLicenseId(FWDWebLib::getConnection());
    $moAttrQuery->setLicenseId($miLicenseId);
    $moAttrQuery->makeQuery();
    $moAttrQuery->executeQuery();
    $mbCanGenerateLicense = true;
    foreach ($moAttrQuery->getAttributes() as $miAttrId => $maAttrInfo) {
      if ($maAttrInfo["name"]=="system_hash") {
        $msHashContent = $maAttrInfo["value"];
        $msActivationHash = $moLicense->generateActivationHash($msHashContent);
        $msActivationHash = str_replace("\n","<BR/>",$msActivationHash);
        $msActivationHash = str_replace("\r","",$msActivationHash);
        if ($msActivationHash) {
          $moLicense = new MPLicense();
          $moLicense->createFilter($miLicenseId,'license_id');
          $moLicense->setFieldValue('system_activated',1);
          $moLicense->update();
          
          $moLicenseActivation = new MPLicenseActivation();
          $moLicenseActivation->setFieldValue('license_id',$miLicenseId);
          $moLicenseActivation->setFieldValue('date_time',date('Y-m-d H:i:s'));
          $moLicenseActivation->insert();
          echo "gobi('license').setValue('$msActivationHash');";
        }
        break;
      }
    }
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new InitEvent("init_event"));
    $moStartEvent->addAjaxEvent(new ChangeSystemEvent("change_system"));
    $moStartEvent->addAjaxEvent(new ChangeLicenseEvent("change_license"));
    $moStartEvent->addAjaxEvent(new SaveLicenseDataEvent("save_license_data"));
    $moStartEvent->addAjaxEvent(new GenerateLicenseEvent("generate_license"));
    $moStartEvent->addAjaxEvent(new GenerateActivationCodeEvent("generate_activation_code"));
    $moStartEvent->addSubmitEvent(new DownloadLicenseEvent("download_license"));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        trigger_event('init_event',3);
        
        function change_client() {
          miClientId = gebi('select_client').value;
          if (gebi('systems_by_client').value) {
            maSystemsByClient = soUnSerializer.unserialize(gebi('systems_by_client').value);
            if (maSystemsByClient[miClientId]) {
              display_systems(maSystemsByClient[miClientId]);
            }
          }
          gebi('selected_client').value=miClientId;
        }
        
        function display_systems(maSystemsList) {
          moSelSystem = gobi('select_system');
          moSelSystem.clear();
          maSystems = soUnSerializer.unserialize(gebi('system_ids').value);
          miSystemsShown=0;
          for (i=0;maSystemsList.length>i;i++) {
            if (maSystems[maSystemsList[i]]) {
              miSystemsShown++;
              moSelSystem.addItem(maSystemsList[i],maSystems[maSystemsList[i]]);
            }
          }
          if (miSystemsShown > 1)
            js_show('btn_delete_association');
          else
            js_hide('btn_delete_association');
          gebi('select_system').value=gebi('selected_system').value; 
          trigger_event('change_system',3);
        }
        
        function change_license() {
          clear_fields();
          js_hide('btn_save_license');
          miLicenseId = gebi('select_license').value;
          if (!miLicenseId) {
            js_hide('btn_delete_license');
            change_isms_users();
          } else {
            trigger_event('change_license',3);
            js_show('btn_delete_license');
          }
        }
        
        function clear_fields() {
          gobi('license').setValue('');
          gebi('selected_license').value = '';
          gebi('system_hash').value = '';
          gebi('select_users').value=5;
          gebi('select_capacity').value=500;
          gebi('initial_date').value='';
          gebi('period').value='';
          gebi('select_period').value=3;
        }
        
        function set_selected_client(piClient) {
          gebi('selected_client').value=piClient;
        }
        
        function set_selected_system(piSystem) {
          gebi('selected_system').value=piSystem;
        }
        
        function change_isms_users() {
          if (gebi('select_users').value == 'C') {
            gebi('select_users').style.width="220";
            js_show('custom_users')
          } else {
            gebi('select_users').style.width="300";
            js_hide('custom_users');
          }
        }
        
        function change_ams_capacity() {
          if (gebi('select_capacity').value == 'C') {
            gebi('select_capacity').style.width="220";
            js_show('custom_capacity')
          } else {
            gebi('select_capacity').style.width="300";
            js_hide('custom_capacity');
          }
        }
      </script>
    <?
  }
}
$soWebLib = FWDWebLib::getInstance();
$soStartEvent = FWDStartEvent::getInstance();
$soStartEvent->addBeforeAjax(new ScreenBeforeEvent(""));
$soWebLib->xml_load("new_license_manager.xml");
?>
