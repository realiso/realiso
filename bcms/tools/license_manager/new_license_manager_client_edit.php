<?php

include_once "include.php";

class SaveClientEvent extends FWDRunnable {
	public function run() {
		$miClientId = FWDWebLib::getObject('param_client_id')->getValue();
		$msClientName = FWDWebLib::getObject('client_name')->getValue();
    $msContract = FWDWebLib::getObject('contract')->getValue();
    $msContact = FWDWebLib::getObject('contact')->getValue();
    $msEmail = FWDWebLib::getObject('email')->getValue();
    $msLicenseType = FWDWebLib::getObject('select_license_type')->getValue();
    switch($msLicenseType) {
      case "client":case "channel":case "demo":break;
      default: $msLicenseType = "client";break;
    }
    $miSystemId = FWDWebLib::getObject('select_system')->getValue();
    switch($miSystemId) {
      case 22:case 23:break;
      default: $miSystemId=22;break;
    }
    
		$moClient = new MPClient();
    $moClient->setFieldValue('client_name',$msClientName);
    $moClient->setFieldValue('client_contract',$msContract);
    $moClient->setFieldValue('client_contact',$msContact);
    $moClient->setFieldValue('client_email',$msEmail);
    $moClient->setFieldValue('client_license_type',$msLicenseType);
		if ($miClientId) {
      $moClient->createFilter($miClientId,'client_id');
      $moClient->update();
    } else {
      $miClientId = $moClient->insert(true);
    }
    
    $miPrevSystemId = FWDWebLib::getObject("param_system_id")->getValue();
    $moEditSystemClient = new MPSystemClient();
    $moEditSystemClient->setFieldValue('system_id',$miSystemId);
    $moEditSystemClient->setFieldValue('client_id',$miClientId);
    
    $moSystemClient = new MPSystemClient();
    $moSystemClient->createFilter($miPrevSystemId,'system_id');
    $moSystemClient->createFilter($miClientId,'client_id');
    $moSystemClient->select();
    if ($moSystemClient->fetch()) {
      $miSystemClientId = $moSystemClient->getFieldValue('system_client_id');
      $moEditSystemClient->createFilter($miSystemClientId,'system_client_id');
      $moEditSystemClient->update();
    } else {
      $moEditSystemClient->insert();
    }
    
    echo "soPopUp = soPopUpManager.getPopUpById('new_license_manager_client_edit');
          soWindow = soPopUp.getOpener();
          soWindow.set_selected_client('$miClientId');
          soWindow.set_selected_system('$miSystemId');
          soWindow.trigger_event('init_event',3);
          soPopUpManager.closePopUp('new_license_manager_client_edit');";
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));
		$moStartEvent->addAjaxEvent(new SaveClientEvent("save_client"));
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
    $miClientId = FWDWebLib::getObject("param_client_id")->getValue();
    $miSystemId = FWDWebLib::getObject("param_system_id")->getValue();
    if ($miClientId) {
      $moClient = new MPClient();
      $moClient->fetchById($miClientId);
      FWDWebLib::getObject("client_name")->setValue($moClient->getFieldValue("client_name"));
      FWDWebLib::getObject("select_license_type")->setValue($moClient->getFieldValue("client_license_type"));
      FWDWebLib::getObject("contract")->setValue($moClient->getFieldValue("client_contract"));
      FWDWebLib::getObject("contact")->setValue($moClient->getFieldValue("client_contact"));
      FWDWebLib::getObject("email")->setValue($moClient->getFieldValue("client_email"));
      FWDWebLib::getObject("select_system")->setValue($miSystemId);
      
      //esconde a sele��o de sistema, pois a troca de sistema acarretaria alguns problemas.
      FWDWebLib::getObject("label_system")->setAttrDisplay("false");
      FWDWebLib::getObject("select_system")->setAttrDisplay("false");
    }
		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
	}

}

$soWebLib = FWDWebLib::getInstance();

$soStartEvent = FWDStartEvent::getInstance();
$soStartEvent->addBeforeAjax(new ScreenBeforeEvent(""));

$soWebLib->xml_load("new_license_manager_client_edit.xml");

?>