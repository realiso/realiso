<?php

include_once "include.php";

include_once $handlers_ref."QueryGridLicenseReport.php";

class GridLicenseReport extends FWDDrawGrid {
  public function DrawItem(){
    switch($this->ciColumnIndex){
      case 3:
        if ($this->coCellBox->getValue() != 0)
          $this->coCellBox->setValue(date("d/m/Y",$this->coCellBox->getValue()));
        else 
          $this->coCellBox->setValue("");
        return parent::drawItem();
      default:
        return parent::drawItem();
    }
  }
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));
		
    $moGridLicenseReport = FWDWebLib::getObject("grid_license_report");
    $moGridLicenseReport->setObjFwdDrawGrid(new GridLicenseReport());
    
    $moHandler = new QueryGridLicenseReport(FWDWebLib::getConnection());
    $msReportName = FWDWebLib::getObject('param_report_name')->getValue();
    switch($msReportName) {
      case 'active_clients':
        $moHandler->setActiveClients();
      break;
      case 'licenses_per_client':
        $moHandler->setClientId(FWDWebLib::getObject('param_element_id')->getValue());
      break;
      case 'expired_licenses_clients':
        $moHandler->setExpiredLicenses();
      break;
      case 'licenses_per_system':
        $moHandler->setSystemId(FWDWebLib::getObject('param_element_id')->getValue());
      break;
      case 'active_not_activated_license_clients':
        $moHandler->setActiveNotActivated();
      break;
      default:
      break;
    }
    
    $moGridLicenseReport->setQueryHandler($moHandler);
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
	}

}

$soWebLib = FWDWebLib::getInstance();

$soStartEvent = FWDStartEvent::getInstance();
$soStartEvent->addBeforeAjax(new ScreenBeforeEvent(""));

$soWebLib->xml_load("license_manager_report.xml");

?>