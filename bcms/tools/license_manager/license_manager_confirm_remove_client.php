<?php
include_once "include.php";

class RemoveClientEvent extends FWDRunnable {
  public function run(){    
    $miClientId = FWDWebLib::getObject('element_id')->getValue();
    
    $moMPClient = new MPClient();
    $moMPClient->createFilter($miClientId,"client_id");
    $moMPClient->delete();
    
    echo "soPopUp = soPopUpManager.getPopUpById('popup_confirm');
          soWindow = soPopUp.getOpener();
          soWindow.trigger_event('init_event',3);
          soPopUpManager.closePopUp('popup_confirm');
         ";
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new RemoveClientEvent("remove_client"));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $miClientId = FWDWebLib::getObject("element_id")->getValue();
    $msTitle = FWDLanguage::getPHPStringValue('client_remove_title','Remover Cliente');
    $msMessage = FWDLanguage::getPHPStringValue('client_remove_message',"Voc� tem certeza que deseja excluir o cliente <b>%client_name%</b>?");
    $msIconSrc = 'gfx/'.'icon-exclamation.gif';
    
    $moMPClient = new MPClient();
    $moMPClient->fetchById($miClientId);
    $msClientName = $moMPClient->getFieldValue('client_name');
    $msMessage = str_replace("%client_name%",$msClientName,$msMessage);
    
    //FWDWebLib::getObject('confirm_title')->setValue($msTitle);
    FWDWebLib::getObject('confirm_message')->setValue($msMessage);
    FWDWebLib::getObject('confirm_icon')->setAttrSrc($msIconSrc);
    
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
    <script language="javascript">
      function remove_element() {
        trigger_event("remove_client",3);
      }
    </script>
    <?
  }
}

$soWebLib = FWDWebLib::getInstance();

$soStartEvent = FWDStartEvent::getInstance();
$soStartEvent->addBeforeAjax(new ScreenBeforeEvent(""));

$soWebLib->xml_load("confirm.xml");
?>
