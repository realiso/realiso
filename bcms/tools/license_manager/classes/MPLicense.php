<?php
/**
 * MINI PROJECTS
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe MPLicense.
 *
 * <p>Classe que representa a tabela de licen�as associados a um sistema associado
 * a um cliente.</p>
 * @package MINI_PROJECTS
 * @subpackage classes
 */
class MPLicense extends MPTable {
	
	/**
	 * Construtor.
	 * 
	 * <p>Construtor da classe MPLicense.</p>
	 * @access public 
	 */
	public function __construct(){		
		parent::__construct("lic_license_system_client");
    
    $this->csAliasId = 'license_id';
		$this->coDataset->addFWDDBField(new FWDDBField("skLicense",			    "license_id",							DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("skSystemClient",    "system_client_id",       DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("dDatetime", 		    "date_time",				      DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField("zLicenseHash",      "license_hash",           DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField("nSystemActivated",  "system_activated",       DB_NUMBER));
	}
  
  /**
   * Busca informa��es de uma licen�a atrav�s de seu identificador.
   * 
   * <p>M�todo para buscar informa��es de uma licen�a atrav�s de seu identificador.</p>
   * @access public
   * @param integer $piId Identificador
   */
  public function fetchById($piId) {
    return parent::fetchById($piId, "license_id");
  }
}
?>