<?php
/**
 * MINI PROJECTS
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe MPLicenseAttribute.
 *
 * <p>Classe que representa a tabela de atributos associados a uma licen�a.</p>
 * @package MINI_PROJECTS
 * @subpackage classes
 */
class MPLicenseAttribute extends MPTable {
	
	/**
	 * Construtor.
	 * 
	 * <p>Construtor da classe MPSystemClientAttribute.</p>
	 * @access public 
	 */
	public function __construct(){		
		parent::__construct("lic_license_attribute");
		$this->coDataset->addFWDDBField(new FWDDBField("skLicense",			"license_id",							DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("skAttribute",		"attribute_id",						DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("zValue", 				"attribute_value",				DB_STRING));
	}
  
	/**
	 * Busca informa��es de atributo de uma licen�a.
	 * 
	 * <p>M�todo para buscar informa��es de atributo de uma licen�a
	 * atrav�s das chaves prim�rias da licen�a e do atributo.</p>
	 * @access public
	 * @param integer $piLicenseId Identificador da licen�a
	 * @param integer $piAttributeId Identificador do atributo
	 */
	public function fetch($piLicenseId,$piAttributeId) {
    $this->removeAllFilters();
		$this->createFilter($piLicenseId, "license_id");
		$this->createFilter($piAttributeId, "attribute_id");
		$this->coDataset->select();
		return parent::fetch();
  }
}
?>