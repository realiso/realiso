<?php
  if (!isset($sys_ref))
  	$sys_ref = "";
    	   
  $classes_ref = $sys_ref  . "classes/";    
  $handlers_ref = $sys_ref  . "handlers/";  
    	    	
  include_once $sys_ref  . "lib/fwd5_setup.php";  
  
  include_once $classes_ref . "MPTable.php";
  include_once $classes_ref . "MPSystem.php";
  include_once $classes_ref . "MPClient.php";
  include_once $classes_ref . "MPAttribute.php";
  include_once $classes_ref . "MPSystemClientAttribute.php";
  include_once $classes_ref . "MPLicenseAttribute.php";
  include_once $classes_ref . "MPLicense.php";
  include_once $classes_ref . "MPLicenseActivation.php";
  include_once $classes_ref . "MPLicenseDeactivation.php";
  include_once $classes_ref . "MPSystemClient.php";
  
  include_once $classes_ref . "database.php";
  
  $soWebLib = FWDWebLib::getInstance();
  $soWebLib->setWindowTitle("Gestor de Licen�a");
  $soWebLib->setGfxRef("gfx/");
  $soWebLib->setCSSRef("lib/");
  $soWebLib->setSysRef(""); 
?>