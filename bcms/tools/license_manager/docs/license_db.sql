/*==============================================================*/
/* DBMS name:      PostgreSQL 7.3                               */
/* Created on:     20/10/2008 14:33:50                          */
/*==============================================================*/


drop index fk_lic_attr_sksystem2_lic_syst_;

drop index fk_lic_license_attribute_lic_at;

drop index fk_lic_license_attribute_lic_li;

drop index fk_lic_license_system_client_li;

drop index fk_lic_system_client_lic_client;

drop table lic_attribute;

drop table lic_client;

drop table lic_license_activation;

drop table lic_license_attribute;

drop table lic_license_deactivation;

drop table lic_license_system_client;

drop table lic_system;

drop table lic_system_client;

drop table lic_system_client_attribute;

/*==============================================================*/
/* Table: lic_attribute                                         */
/*==============================================================*/
create table lic_attribute (
skattribute          serial               not null,
sksystem             int4                 null,
zname                varchar(256)         not null,
zdescription         varchar(256)         null,
constraint pk_lic_attribute primary key (skattribute)
);

/*==============================================================*/
/* Index: fk_lic_attr_sksystem2_lic_syst_                       */
/*==============================================================*/
create  index fk_lic_attr_sksystem2_lic_syst_ on lic_attribute (
sksystem
);

/*==============================================================*/
/* Table: lic_client                                            */
/*==============================================================*/
create table lic_client (
skclient             serial               not null,
zname                varchar(256)         not null,
zcontract            varchar(256)         null,
zcontact             varchar(256)         null,
zemail               varchar(256)         null,
zlicensetype         varchar(50)          null,
constraint pk_lic_client primary key (skclient)
);

/*==============================================================*/
/* Table: lic_license_activation                                */
/*==============================================================*/
create table lic_license_activation (
sklicense            numeric              not null,
ddatetime            date                 not null,
constraint pk_lic_license_activation primary key (sklicense, ddatetime)
);

/*==============================================================*/
/* Table: lic_license_attribute                                 */
/*==============================================================*/
create table lic_license_attribute (
skattribute          int4                 not null,
sklicense            int4                 not null,
zvalue               varchar(256)         null,
constraint pk_lic_license_attribute primary key (skattribute, sklicense)
);

/*==============================================================*/
/* Index: fk_lic_license_attribute_lic_at                       */
/*==============================================================*/
create  index fk_lic_license_attribute_lic_at on lic_license_attribute (
skattribute
);

/*==============================================================*/
/* Index: fk_lic_license_attribute_lic_li                       */
/*==============================================================*/
create  index fk_lic_license_attribute_lic_li on lic_license_attribute (
sklicense
);

/*==============================================================*/
/* Table: lic_license_deactivation                              */
/*==============================================================*/
create table lic_license_deactivation (
sklicense            numeric              not null,
ddatetime            date                 not null,
constraint pk_lic_license_deactivation primary key (sklicense, ddatetime)
);

/*==============================================================*/
/* Table: lic_license_system_client                             */
/*==============================================================*/
create table lic_license_system_client (
sklicense            serial               not null,
sksystemclient       int4                 not null,
ddatetime            date                 not null,
zlicensehash         varchar(256)         null,
nsystemactivated     int4                 not null default 0,
constraint pk_lic_license_system_client primary key (sklicense)
);

/*==============================================================*/
/* Index: fk_lic_license_system_client_li                       */
/*==============================================================*/
create  index fk_lic_license_system_client_li on lic_license_system_client (
sksystemclient
);

/*==============================================================*/
/* Table: lic_system                                            */
/*==============================================================*/
create table lic_system (
sksystem             serial               not null,
zname                varchar(256)         not null,
constraint pk_lic_system primary key (sksystem)
);

/*==============================================================*/
/* Table: lic_system_client                                     */
/*==============================================================*/
create table lic_system_client (
sksystemclient       serial               not null,
skclient             int4                 not null,
sksystem             numeric              not null,
constraint pk_lic_system_client primary key (sksystemclient)
);

/*==============================================================*/
/* Index: fk_lic_system_client_lic_client                       */
/*==============================================================*/
create  index fk_lic_system_client_lic_client on lic_system_client (
skclient
);

/*==============================================================*/
/* Table: lic_system_client_attribute                           */
/*==============================================================*/
create table lic_system_client_attribute (
sksystem             numeric              not null,
skattribute          numeric              not null,
skclient             numeric              not null,
zvalue               varchar(256)         null,
sklicense            numeric              not null,
constraint pk_lic_system_client_attribute primary key (sksystem, skattribute, skclient, sklicense)
);

alter table lic_attribute
   add constraint fk_lic_attr_fk_lic_at_lic_syst foreign key (sksystem)
      references lic_system (sksystem)
      on delete restrict on update restrict;

alter table lic_license_attribute
   add constraint fk_lic_lice_fk_lic_li_lic_attr foreign key (skattribute)
      references lic_attribute (skattribute)
      on delete restrict on update restrict;

alter table lic_license_attribute
   add constraint fk_lic_lice_fk_lic_li_lic_lice foreign key (sklicense)
      references lic_license_system_client (sklicense)
      on delete restrict on update restrict;

alter table lic_license_system_client
   add constraint fk_lic_lice_fk_lic_li_lic_syst foreign key (sksystemclient)
      references lic_system_client (sksystemclient)
      on delete restrict on update restrict;

alter table lic_system_client
   add constraint fk_lic_syst_fk_lic_sy_lic_clie foreign key (skclient)
      references lic_client (skclient)
      on delete restrict on update restrict;

