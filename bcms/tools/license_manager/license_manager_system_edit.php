<?php

include_once "include.php";

include_once $handlers_ref."QueryAttributesBySystemId.php";


class PopulateAttributeSelectEvent extends FWDRunnable {

	public function run() {
		$miSystemId = FWDWebLib::getObject('param_sys_id')->getValue();
		$moQuery = new QueryAttributesBySystemId(FWDWebLib::getConnection());
		$moQuery->setSystemId($miSystemId);
		$moQuery->makeQuery();
		$moQuery->executeQuery();
		$maInfo = array();
		foreach ($moQuery->getAttributes() as $miAttributeId => $msAttributeName)
			$maInfo[] = array($miAttributeId,$msAttributeName);
		$msInfo = serialize($maInfo);
		
		// popula o select de atributos
		echo "js_populate_select('attribute_select', '$msInfo');";
	}

}


class SaveSystemNameEvent extends FWDRunnable {

	public function run() {
		$miSystemId = FWDWebLib::getObject("param_sys_id")->getValue();
		$msSystemName = FWDWebLib::getObject("var_system_name")->getValue();
		$moSystem = new MPSystem();
		$moSystem->createFilter($miSystemId,"system_id");
		$moSystem->setFieldValue("system_name",$msSystemName);
		$moSystem->update();
	}

}


class RemoveAttributeEvent extends FWDRunnable {

	public function run() {
		$miAttributeId = FWDWebLib::getObject("var_attribute_id")->getValue();
		$moAttribute = new MPAttribute();
		$moAttribute->createFilter($miAttributeId,"attribute_id");
		$moAttribute->delete();
		
		// atualiza o select de atributos
		echo "refresh_attribute_select();";
	}

}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));
		
		$moStartEvent->addAjaxEvent(new PopulateAttributeSelectEvent("populate_attribute_select_event"));
		$moStartEvent->addAjaxEvent(new SaveSystemNameEvent("save_system_name_event"));
		$moStartEvent->addAjaxEvent(new RemoveAttributeEvent("remove_attribute_event"));

    $miSystemId = FWDWebLib::getObject("param_sys_id")->getValue();
    FWDWebLib::getObject("var_sys_id")->setValue($miSystemId);
    
    $moSystem = new MPSystem();
    $moSystem->fetchById($miSystemId);
    FWDWebLib::getObject("system_name")->setValue($moSystem->getFieldValue("system_name"));
  
    $moAttributeSelect = FWDWebLib::getObject("attribute_select");
    $moQuery = new QueryAttributesBySystemId(FWDWebLib::getConnection());
    $moQuery->setSystemId($miSystemId);
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    foreach ($moQuery->getAttributes() as $miAttributeId => $msAttributeName) {
      $moAttributeSelect->setItemValue($miAttributeId,$msAttributeName);
      if ($msAttributeName=="expiracy")
        FWDWebLib::getObject("var_attribute_expiracy_id")->setValue($miAttributeId);
      else if ($msAttributeName=="expiracy_str")
        FWDWebLib::getObject("var_attribute_expiracy_str_id")->setValue($miAttributeId);
    }
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
?>
		<script language="javascript">
		function refresh_attribute_select() {
			trigger_event('populate_attribute_select_event','3');
		}
		</script>
<?
	}

}

$soWebLib = FWDWebLib::getInstance();

$soStartEvent = FWDStartEvent::getInstance();
$soStartEvent->addBeforeAjax(new ScreenBeforeEvent(""));

$soWebLib->xml_load("license_manager_system_edit.xml");

?>