<?php
include_once "include.php";

$moLicSystemClientAttribute = new FWDDBDataSet(FWDWebLib::getConnection());
$moLicSystemClientAttribute->addFWDDBField(new FWDDBField('skclient','skclient' ,DB_NUMBER));
$moLicSystemClientAttribute->addFWDDBField(new FWDDBField('sksystem','sksystem' ,DB_NUMBER));
$moLicSystemClientAttribute->setQuery("
SELECT DISTINCT 
       sca.skclient as skclient,
       sca.sksystem as sksystem
  FROM lic_system_client_attribute sca 
  JOIN lic_client c ON (sca.skclient=c.skclient)
");
$moLicSystemClientAttribute->execute();
while($moLicSystemClientAttribute->fetch()) {
  $miClientId = $moLicSystemClientAttribute->getFieldByAlias('skclient')->getValue();
  $miSystemId = $moLicSystemClientAttribute->getFieldByAlias('sksystem')->getValue();
  
  $moSystemClient = new MPSystemClient();
  $moSystemClient->setFieldValue('system_id',$miSystemId);
  $moSystemClient->setFieldValue('client_id',$miClientId);
  $miSystemCientId = $moSystemClient->insert(true);
  
  $moLicense = new MPLicense();
  $moLicense->setFieldValue('system_client_id',$miSystemCientId);
  $moLicense->setFieldValue('date_time',date('Y-m-d H:i:s'));
  $miLicenseId = $moLicense->insert(true);
  
  $moAttribute = new FWDDBDataSet(FWDWebLib::getConnection());
  $moAttribute->addFWDDBField(new FWDDBField('skattribute','skattribute',DB_NUMBER));
  $moAttribute->addFWDDBField(new FWDDBField('zvalue'     ,'zvalue'     ,DB_STRING));
  $moAttribute->setQuery("
SELECT skattribute as skattribute, 
       zvalue as zvalue 
  FROM lic_system_client_attribute sca 
  WHERE sksystem='".$miSystemId."' 
    AND skclient='".$miClientId."' 
    AND sklicense=0
");

  $moAttribute->execute();
  while($moAttribute->fetch()) {
    $moLicenseAttribute = new MPLicenseAttribute();
    $moLicenseAttribute->setFieldValue('license_id',$miLicenseId);
    $moLicenseAttribute->setFieldValue('attribute_id',$moAttribute->getFieldByAlias('skattribute')->getValue());
    $moLicenseAttribute->setFieldValue('attribute_value',$moAttribute->getFieldByAlias('zvalue')->getValue());
    $moLicenseAttribute->insert();
  }
}
?>
