<?php

include_once "include.php";

include_once $handlers_ref."QueryClientsBySystemId.php";


class SaveAttributeEvent extends FWDRunnable {

	public function run() {
		$miSystemId = FWDWebLib::getObject('param_sys_id')->getValue();
		$miAttributeId = FWDWebLib::getObject('param_attr_id')->getValue();
		$msAttributeName = FWDWebLib::getObject("var_attribute_name")->getValue();
		$msAttributeDescription = FWDWebLib::getObject("var_attribute_description")->getValue();
		$moAttribute = new MPAttribute();
		if ($miAttributeId) {
			$moAttribute->createFilter($miAttributeId,"attribute_id");
			$moAttribute->setFieldValue("attribute_name",$msAttributeName);
			$moAttribute->setFieldValue("attribute_description",$msAttributeDescription);
			$moAttribute->update();
		}
		else {
			$moAttribute->setFieldValue("system_id",$miSystemId);
			$moAttribute->setFieldValue("attribute_name",$msAttributeName);
			$moAttribute->setFieldValue("attribute_description",$msAttributeDescription);
			$miAttributeId = $moAttribute->insert(true);
			
			$moCliBySysIdQuery = new QueryClientsBySystemId(FWDWebLib::getConnection());
			$moCliBySysIdQuery->setSystemId($miSystemId);
			$moCliBySysIdQuery->makeQuery();
			$moCliBySysIdQuery->executeQuery();
			$moSystemClientAttribute = new MPSystemClientAttribute();
			$moSystemClientAttribute->setFieldValue("system_id",$miSystemId);
			$moSystemClientAttribute->setFieldValue("attribute_id",$miAttributeId);
			$moSystemClientAttribute->setFieldValue("attribute_value","");
			foreach (array_keys($moCliBySysIdQuery->getClients()) as $miClientId) {
				$moSystemClientAttribute->setFieldValue("client_id",$miClientId);
				$moSystemClientAttribute->insert();
			}
		}
		
		echo "var soPopUp = soPopUpManager.getPopUpById('license_manager_system_edit');
					if(soPopUp) {
						var soWindow = soPopUp.getWindow();
						soWindow.refresh_attribute_select();
					}";
		echo "soPopUpManager.closePopUp('license_manager_attribute_edit');";
	}

}
class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));
		
		$moStartEvent->addAjaxEvent(new SaveAttributeEvent("save_attribute_event"));
		
    $miSystemId = FWDWebLib::getObject('param_sys_id')->getValue();
    $moSystem = new MPSystem();
    $moSystem->fetchById($miSystemId);
    FWDWebLib::getObject("system_name")->setValue($moSystem->getFieldByAlias("system_name")->getValue());
    
    $miAttributeId = FWDWebLib::getObject('param_attr_id')->getValue();
    if ($miAttributeId) {
      $moAttribute = new MPAttribute();
      $moAttribute->fetchById($miAttributeId);
      FWDWebLib::getObject("attribute_name")->setValue($moAttribute->getFieldByAlias("attribute_name")->getValue());
      FWDWebLib::getObject("attribute_description")->setValue($moAttribute->getFieldByAlias("attribute_description")->getValue());
    }
    
    $mbBlockAlias = FWDWebLib::getObject('param_block_alias')->getValue() ? true:false;
    if ($mbBlockAlias)
      FWDWebLib::getObject("attribute_name")->setAttrDisabled("true");
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
	}

}

$soWebLib = FWDWebLib::getInstance();

$soStartEvent = FWDStartEvent::getInstance();
$soStartEvent->addBeforeAjax(new ScreenBeforeEvent(""));

$soWebLib->xml_load("license_manager_attribute_edit.xml");

?>