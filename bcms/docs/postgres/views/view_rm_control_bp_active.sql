CREATE VIEW view_rm_control_bp_active AS
SELECT fkContext, fkBestPractice, fkControl FROM rm_control_best_practice
WHERE
fkBestPractice NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2808 AND nState = 2705)
AND
fkControl NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2805 AND nState = 2705);