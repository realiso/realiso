CREATE VIEW view_isms_context_hist_active AS
SELECT h.pkId, h.fkContext, h.sType, h.dDate, h.nValue
FROM isms_context c RIGHT JOIN isms_context_history h ON (c.pkContext = h.fkContext AND c.nState <> 2705);