CREATE VIEW view_rm_control_active AS
SELECT
	ctr.fkContext, ctr.fkType, ctr.fkResponsible, ctr.sName, ctr.tDescription,
	ctr.sDocument, ctr.sEvidence, ctr.nDaysBefore, ctr.bIsActive, ctr.bFlagImplAlert, ctr.bFlagImplExpired,
	ctr.dDateDeadline, ctr.dDateImplemented, ctr.nImplementationState
FROM isms_context c JOIN rm_control ctr ON (c.pkContext = ctr.fkContext AND c.nState <> 2705);