CREATE VIEW view_rm_process_asset_active AS
SELECT fkContext, fkProcess, fkAsset FROM rm_process_asset
WHERE
fkProcess NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2802 AND nState = 2705)
AND
fkAsset NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2803 AND nState = 2705);