CREATE VIEW view_pm_di_with_content AS
SELECT
	i.fkContext, i.fkDocument, i.nMajorVersion, i.nRevisionVersion, i.tRevisionJustification, i.sPath, i.tModifyComment,
	i.dBeginProduction, i.dEndProduction, i.sFilename, i.bIsLink, i.sLink, ic.tContent
FROM isms_context c 
JOIN pm_doc_instance i ON (c.pkContext = i.fkContext)
LEFT JOIN pm_instance_content ic ON (i.fkContext = ic.fkInstance);