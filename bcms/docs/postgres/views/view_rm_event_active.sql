CREATE VIEW view_rm_event_active AS
SELECT e.fkContext, e.fkType, e.fkCategory, e.sDescription, e.tObservation, e.bPropagate, e.tImpact
FROM isms_context c JOIN rm_event e ON (c.pkContext = e.fkContext AND c.nState <> 2705);