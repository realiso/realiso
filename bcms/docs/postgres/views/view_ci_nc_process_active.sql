CREATE VIEW view_ci_nc_process_active AS
SELECT np.fkProcess,np.fkNC
FROM ci_nc_process np
JOIN isms_context ctx_p ON (ctx_p.pkContext = np.fkProcess AND ctx_p.nState <> 2705)
JOIN isms_context ctx_n ON (ctx_n.pkContext = np.fkNC AND ctx_n.nState <> 2705);