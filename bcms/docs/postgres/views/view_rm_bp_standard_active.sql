CREATE VIEW view_rm_bp_standard_active AS
SELECT fkContext, fkStandard, fkBestPractice FROM rm_best_practice_standard
WHERE
fkStandard NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2809 AND nState = 2705)
AND
fkBestPractice NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2808 AND nState = 2705);