CREATE VIEW view_isms_scope_active AS
SELECT s.fkContext, s.tDescription
FROM isms_context c JOIN isms_scope s ON (c.pkContext = s.fkContext AND c.nState <> 2705);