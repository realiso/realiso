CREATE VIEW view_isms_context_active AS
SELECT c.pkContext, c.nType, c.nState
FROM isms_context c WHERE c.nState <> 2705;