CREATE VIEW view_pm_user_roles AS
SELECT
  u.fkContext AS user_id,
  d.fkContext AS document_id,
  CASE
    WHEN u.fkContext = d.fkMainApprover THEN 1
    ELSE 0
  END AS user_is_main_approver,
  CASE
    WHEN u.fkContext = d.fkAuthor THEN 1
    ELSE 0
  END AS user_is_author,
  CASE
    WHEN u.fkContext IN ( SELECT da.fkUser FROM pm_doc_approvers da WHERE da.fkDocument = d.fkContext ) THEN 1
    ELSE 0
  END AS user_is_approver,
  CASE
    WHEN u.fkContext IN ( SELECT dr.fkUser FROM pm_doc_readers dr WHERE dr.fkDocument = d.fkContext ) THEN 1
    ELSE 0
  END AS user_is_reader
FROM
  view_isms_user_active u,
  view_pm_document_active d;
