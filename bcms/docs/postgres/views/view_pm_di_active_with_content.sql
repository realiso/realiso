CREATE VIEW view_pm_di_active_with_content AS
SELECT
	i.fkContext, i.fkDocument, i.nMajorVersion, i.nRevisionVersion, i.tRevisionJustification, i.sPath, i.tModifyComment,
	i.dBeginProduction, i.dEndProduction, i.sFilename, i.bIsLink, i.sLink, ic.tContent,i.dBeginApprover,i.dBeginRevision
FROM isms_context c 
JOIN pm_doc_instance i ON (c.pkContext = i.fkContext AND c.nState <> 2705)
JOIN isms_context ctx_doc ON (ctx_doc.pkContext = i.fkDocument AND ctx_doc.nState <> 2705)
LEFT JOIN pm_instance_content ic ON (i.fkContext = ic.fkInstance);