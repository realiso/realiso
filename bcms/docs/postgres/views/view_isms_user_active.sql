CREATE VIEW view_isms_user_active AS
SELECT
	u.fkContext, u.fkProfile, u.sName, u.sLogin, u.sPassword, u.sEmail, u.nIp,
	u.nLanguage, u.dLastLogin, u.nWrongLogonAttempts, u.bIsBlocked, u.bMustChangePassword, u.sRequestPassword, u.sSession, u.bAnsweredSurvey, u.bShowHelp
FROM isms_context c JOIN isms_user u ON (c.pkContext = u.fkContext AND c.nState <> 2705);
