CREATE VIEW view_rm_category_active AS
SELECT cat.fkContext, cat.fkParent, cat.sName, cat.tDescription
FROM isms_context c JOIN rm_category cat ON (c.pkContext = cat.fkContext AND c.nState <> 2705);