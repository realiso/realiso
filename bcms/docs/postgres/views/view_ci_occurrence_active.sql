CREATE VIEW view_ci_occurrence_active AS
SELECT o.fkContext, o.fkIncident, o.tDescription, o.tDenialJustification, o.dDate
FROM isms_context c JOIN ci_occurrence o ON (c.pkContext = o.fkContext AND c.nState <> 2705);