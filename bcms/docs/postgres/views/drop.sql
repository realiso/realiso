DROP VIEW context_history;

DROP VIEW context_names;

DROP VIEW view_pm_document_active;

DROP VIEW view_pm_doc_instance_active;

DROP VIEW view_pm_di_active_with_content;

DROP VIEW view_pm_di_with_content;

DROP VIEW view_isms_user_active;

DROP VIEW view_rm_area_active;

DROP VIEW view_rm_process_active;

DROP VIEW view_rm_process_asset_active;

DROP VIEW view_rm_risk_control_active;

DROP VIEW view_rm_risk_active;

DROP VIEW view_rm_asset_active;

DROP VIEW view_pm_process_user_active;

DROP VIEW view_rm_control_bp_active;

DROP VIEW view_rm_control_active;

DROP VIEW view_rm_best_practice_active;

DROP VIEW view_isms_context_active;

DROP VIEW view_rm_asset_asset_active;

DROP VIEW view_rm_bp_standard_active;

DROP VIEW view_rm_bp_event_active;

DROP VIEW view_rm_standard_active;

DROP VIEW view_pm_register_active;

DROP VIEW view_isms_context_hist_active;

DROP VIEW view_isms_profile_active;

DROP VIEW view_pm_doc_registers_active;

DROP VIEW view_pm_doc_context_active;

DROP VIEW view_pm_doc_approvers_active;

DROP VIEW view_pm_doc_readers_active;

DROP VIEW view_isms_scope_active;

DROP VIEW view_isms_policy_active;

DROP VIEW view_rm_category_active;

DROP VIEW view_rm_event_active;

DROP VIEW view_rm_sec_bp_active;

DROP VIEW view_pm_template_active;

DROP VIEW view_pm_tp_active_with_content;

DROP VIEW view_pm_template_bp_active;

DROP VIEW view_ci_solution_active;

DROP VIEW view_ci_nc_active;

DROP VIEW view_ci_incident_active;

DROP VIEW view_ci_category_active;

DROP VIEW view_ci_incident_risk_active;

DROP VIEW view_ci_inc_control_active;

DROP VIEW view_pm_published_docs;

DROP VIEW view_pm_read_docs;

DROP VIEW view_ci_action_plan_active;

DROP VIEW view_ci_inc_process_active;

DROP VIEW view_ci_incident_user_active;

DROP VIEW view_ci_nc_process_active;

DROP VIEW view_ci_occurrence_active;

DROP VIEW view_pm_doc_instance_status;

DROP VIEW view_pm_user_roles;