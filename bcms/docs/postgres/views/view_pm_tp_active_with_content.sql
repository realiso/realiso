CREATE VIEW view_pm_tp_active_with_content AS
SELECT
  t.fkContext, t.sName, t.nContextType, t.sPath, t.sFileName, t.tDescription, t.sKeywords, tc.tContent, t.nfilesize
FROM isms_context c 
JOIN pm_template t ON (c.pkContext = t.fkContext AND c.nState <> 2705)
LEFT JOIN pm_template_content tc ON (t.fkContext = tc.fkContext);