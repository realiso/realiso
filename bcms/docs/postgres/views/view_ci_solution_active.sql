CREATE VIEW view_ci_solution_active AS
SELECT s.fkContext, s.fkCategory, s.tProblem, s.tSolution, s.tKeywords
FROM isms_context c JOIN ci_solution s ON (c.pkContext = s.fkContext AND c.nState <> 2705);