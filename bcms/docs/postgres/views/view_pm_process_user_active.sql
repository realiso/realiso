CREATE VIEW view_pm_process_user_active AS
SELECT fkProcess, fkUser FROM pm_process_user
WHERE
fkProcess NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2802 AND nState = 2705)
AND
fkUser NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2816 AND nState = 2705);