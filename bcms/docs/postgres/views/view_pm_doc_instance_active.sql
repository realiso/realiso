CREATE VIEW view_pm_doc_instance_active AS
SELECT
	i.fkContext, i.fkDocument, i.nMajorVersion, i.nRevisionVersion, i.tRevisionJustification, i.sPath, i.tModifyComment,
	i.dBeginProduction, i.dEndProduction, i.sFilename, i.bIsLink, i.sLink,i.dBeginApprover,i.dBeginRevision,i.sManualVersion
FROM isms_context c 
  JOIN pm_doc_instance i ON (c.pkContext = i.fkContext AND c.nState <> 2705)
  JOIN isms_context ctx_doc ON (ctx_doc.pkContext = i.fkDocument AND ctx_doc.nState <> 2705);