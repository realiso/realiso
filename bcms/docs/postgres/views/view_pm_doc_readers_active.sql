CREATE VIEW view_pm_doc_readers_active AS
SELECT fkUser, fkDocument, bManual, bDenied, bHasRead FROM pm_doc_readers
WHERE
fkDocument NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2823 AND nState = 2705)
AND
fkUser NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2816 AND nState = 2705);