CREATE VIEW view_rm_bp_event_active AS
SELECT fkContext, fkBestPractice, fkEvent FROM rm_best_practice_event
WHERE
fkBestPractice NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2808 AND nState = 2705)
AND
fkEvent NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2807 AND nState = 2705);