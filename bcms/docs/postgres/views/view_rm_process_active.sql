CREATE VIEW view_rm_process_active AS
SELECT
	p.fkContext, p.fkPriority, p.fkType, p.fkArea, p.fkResponsible, p.sName,
	p.tDescription, p.sDocument, p.nValue, p.tJustification
FROM isms_context c JOIN rm_process p ON (c.pkContext = p.fkContext AND c.nState <> 2705);