CREATE OR REPLACE VIEW view_ci_action_plan_active AS 
 SELECT ac.fkcontext, ac.fkresponsible, ac.sname, ac.tactionplan, ac.nactiontype, ac.ddatedeadline, ac.ddateconclusion, ac.bisefficient, ac.ddateefficiencyrevision, ac.ddateefficiencymeasured, ac.ndaysbefore, ac.bflagrevisionalert, ac.bflagrevisionalertlate, ac.sdocument
   FROM isms_context c
   JOIN ci_action_plan ac ON c.pkcontext = ac.fkcontext AND c.nstate <> 2705;
