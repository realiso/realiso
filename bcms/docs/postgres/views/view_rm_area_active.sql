CREATE VIEW view_rm_area_active AS
SELECT
	a.fkContext, a.fkPriority, a.fkType, a.fkParent, a.fkResponsible, a.sName,
	a.tDescription, a.sDocument, a.nValue
FROM isms_context c JOIN rm_area a ON (c.pkContext = a.fkContext AND c.nState <> 2705);