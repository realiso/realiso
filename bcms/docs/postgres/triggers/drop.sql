DROP TRIGGER recalculation_of_asset_for_risk_delete ON rm_risk;

DROP TRIGGER recalculation_of_context_value ON isms_context;

DROP TRIGGER delete_user ON isms_user;

DROP TRIGGER recalculation_of_area_by_area ON rm_area;

DROP TRIGGER recalculation_of_dependent_asset ON rm_asset;

DROP TRIGGER recalculation_of_process ON rm_asset;

DROP TRIGGER recalculation_of_asset_association ON rm_asset_asset;

DROP TRIGGER recalculation_of_area_by_process_delete ON rm_process;

DROP TRIGGER recalculation_of_area_by_process_update ON rm_process;

DROP TRIGGER recalculation_of_process_association ON rm_process_asset;

DROP TRIGGER recalculation_of_risk_for_insert ON rm_risk;

DROP TRIGGER recalculation_of_risk_for_update ON rm_risk;

DROP TRIGGER recauculate_risk_for_risk_control_association ON rm_risk_control;

DROP TRIGGER recalculation_of_risks_values ON rm_control;

DROP TRIGGER update_control_is_active ON rm_control;

DROP TRIGGER update_alert_sent ON wkf_task;

DROP FUNCTION delete_user();

DROP FUNCTION recalculation_of_area_by_area();

DROP FUNCTION recalculation_of_area_by_process_delete();

DROP FUNCTION recalculation_of_area_by_process_update();

DROP FUNCTION recalculation_of_asset_association();

DROP FUNCTION recalculation_of_asset_for_risk_delete();

DROP FUNCTION recalculation_of_context_value();

DROP FUNCTION recalculation_of_dependent_asset();

DROP FUNCTION recalculation_of_process();

DROP FUNCTION recalculation_of_process_association();

DROP FUNCTION recalculation_of_risk_for_insert();

DROP FUNCTION recalculation_of_risk_for_update();

DROP FUNCTION recauculate_risk_for_risk_control_association();

DROP FUNCTION recalculation_of_risks_values();

DROP FUNCTION update_control_is_active();

DROP FUNCTION update_alert_sent();