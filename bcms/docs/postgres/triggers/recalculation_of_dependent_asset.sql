CREATE OR REPLACE FUNCTION recalculation_of_dependent_asset()
  RETURNS "trigger" AS
'DECLARE
    miVchg double precision;
BEGIN

IF (TG_OP = ''UPDATE'') THEN
    miVchg := (NEW.nValue - OLD.nValue);
    IF (miVchg <> 0) THEN
      UPDATE rm_asset
        SET nValue = get_asset_value(fkContext)
        WHERE
            fkContext IN 
                (select  fkDependent from rm_asset_asset where fkAsset = OLD.fkContext);

    END IF;
END IF;
RETURN NEW;
END'
  LANGUAGE 'plpgsql' VOLATILE;

CREATE TRIGGER recalculation_of_dependent_asset
  AFTER UPDATE
  ON rm_asset
  FOR EACH ROW
  EXECUTE PROCEDURE recalculation_of_dependent_asset();