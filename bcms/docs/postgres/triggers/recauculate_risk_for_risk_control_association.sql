CREATE OR REPLACE FUNCTION recauculate_risk_for_risk_control_association()
  RETURNS "trigger" AS
'DECLARE
BEGIN
    IF (TG_OP = ''DELETE'') THEN
	UPDATE rm_risk
	  SET nvalue = get_risk_value(OLD.fkRisk),
          nValueResidual = get_risk_value_residual(OLD.fkRisk)
             WHERE fkContext = OLD.fkRisk;
    END IF;
RETURN NEW;
END'
  LANGUAGE 'plpgsql' VOLATILE;

CREATE TRIGGER recauculate_risk_for_risk_control_association
  AFTER DELETE
  ON rm_risk_control
  FOR EACH ROW
  EXECUTE PROCEDURE recauculate_risk_for_risk_control_association();