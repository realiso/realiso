CREATE OR REPLACE FUNCTION recalculation_of_area_by_area()
  RETURNS "trigger" AS
'DECLARE
    miVchg double precision;
BEGIN

IF (TG_OP = ''UPDATE'') THEN
    miVchg := (NEW.nValue - OLD.nValue);
    IF (miVchg <> 0) THEN
        UPDATE rm_area
        SET nValue = get_area_value(fkContext)
        WHERE fkContext = OLD.fkParent;
    END IF;
END IF;
RETURN NEW;
END'
  LANGUAGE 'plpgsql' VOLATILE;

CREATE TRIGGER recalculation_of_area_by_area
  AFTER UPDATE
  ON rm_area
  FOR EACH ROW
  EXECUTE PROCEDURE recalculation_of_area_by_area();