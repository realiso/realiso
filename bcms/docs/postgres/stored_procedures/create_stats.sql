CREATE OR REPLACE FUNCTION create_stats()
  RETURNS integer AS
'DECLARE
risk_low FLOAT;
risk_high FLOAT;
quantity INTEGER;
cost FLOAT;
id_value id_value%rowtype;
id_values id_values%rowtype;
control_cost control_cost%rowtype;
query varchar(4096);
periodicy_value INTEGER;
periodicy_type INTEGER;
date_last_run TIMESTAMP;

BEGIN

/*======================================TESTE PARA VER SE NECESSITA EXECUTAR A FUNÇÃO=========================================*/

periodicy_value = (SELECT sValue FROM isms_config WHERE pkConfig = 417);
periodicy_type = (SELECT sValue FROM isms_config WHERE pkConfig = 418);
date_last_run = (SELECT sValue FROM isms_config WHERE pkConfig = 420);

IF get_next_date(date_last_run, periodicy_type, periodicy_value) <= NOW()
THEN

SELECT rl.nLow, rl.nHigh
INTO   risk_low,risk_high
FROM rm_risk_limits rl JOIN isms_context c ON (rl.fkContext = c.pkContext)
WHERE c.nState = 2702;
/*===============================================INFORMAÇÕES SOBRE ÁREA========================================================*/

-- Valor da área

query := ''SELECT fkContext, nValue FROM view_rm_area_active'';

FOR id_value IN EXECUTE query LOOP
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (id_value.id, now(), ''area_value'', id_value.value);
END LOOP;

-- N° total de áreas

quantity := (SELECT count(*) FROM view_rm_area_active);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), ''areas_total'', quantity);

-- N° áreas com risco alto

quantity := (SELECT count(*) FROM view_rm_area_active WHERE nValue >= risk_high);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), ''areas_with_high_risk'', quantity);

-- N° áreas com risco médio

quantity := (SELECT count(*) FROM view_rm_area_active WHERE nValue > risk_low AND nValue < risk_high);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), ''areas_with_medium_risk'', quantity);

-- N° áreas com risco baixo

quantity := (SELECT count(*) FROM view_rm_area_active WHERE nValue <= risk_low AND nValue <> 0);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), ''areas_with_low_risk'', quantity);

-- N° áreas com risco não parametrizado

quantity := (SELECT count(*) FROM view_rm_area_active WHERE nValue = 0);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), ''areas_with_no_value_risk'', quantity);

-- N° áreas completas

quantity := (
	SELECT count(*) FROM view_rm_area_active a
	WHERE a.nValue > 0 AND NOT EXISTS (SELECT * FROM view_rm_process_active p WHERE p.fkArea = a.fkContext AND p.nValue=0)
);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), ''complete_areas'', quantity);

-- N° áreas parcias

quantity := (
	SELECT count(*) FROM view_rm_area_active a
	WHERE a.nValue > 0 AND EXISTS (SELECT * FROM view_rm_process_active p WHERE p.fkArea = a.fkContext AND p.nValue = 0)
);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), ''partial_areas'', quantity);

-- N° áreas não gerenciadas

quantity := (SELECT count(*) FROM view_rm_area_active a WHERE a.nValue = 0);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), ''not_managed_areas'', quantity);

-- N° áreas sem processos

quantity := (
	SELECT count(*)
	FROM view_rm_area_active a
	WHERE a.fkContext NOT IN (SELECT fkArea FROM view_rm_process_active)
);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), ''areas_without_processes'', quantity);

-- Total de impacto das áreas e qtd. de riscos por área

query := ''SELECT area_id, count(risk_id) as risk_count, CASE WHEN sum(r.nCost) IS NULL THEN 0 ELSE sum(r.nCost) END as total_impact
		FROM
		(
			SELECT ar.fkContext as area_id, r.fkContext as risk_id
			FROM view_rm_area_active ar
			LEFT JOIN view_rm_process_active p ON (ar.fkContext = p.fkArea)
			LEFT JOIN view_rm_process_asset_active pa ON (p.fkContext = pa.fkProcess)
			LEFT JOIN view_rm_asset_active a ON (pa.fkAsset = a.fkContext)
			LEFT JOIN view_rm_risk_active r ON (a.fkContext = r.fkAsset)
			GROUP BY ar.fkContext, r.fkContext
		) res
		LEFT JOIN view_rm_risk_active r ON (res.risk_id = r.fkContext)
		GROUP BY area_id'';

FOR id_values IN EXECUTE query LOOP	
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (id_values.id, now(), ''risk_amount_by_area'', id_values.value1);
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (id_values.id, now(), ''total_area_impact'', id_values.value2);
END LOOP;

-- Custo e quantidade dos controles por área

query := ''
	/*Calcula o custo total dos controles de cada área e a quantidade de controles de cada área*/
	SELECT res.area_id, count(control_id) as control_count, CASE WHEN sum(cc.nCost1+cc.nCost2+cc.nCost3+cc.nCost4+cc.nCost5) IS NULL THEN 0 ELSE sum(cc.nCost1+cc.nCost2+cc.nCost3+cc.nCost4+cc.nCost5) END as control_cost
	FROM
	(
		/*Garante que um controle só irá ser contabilizado uma vez para uma determinada área*/
		SELECT ar.fkContext as area_id, rc.fkControl as control_id
		FROM view_rm_area_active ar
		LEFT JOIN view_rm_process_active p ON (ar.fkContext = p.fkArea)
		LEFT JOIN view_rm_process_asset_active pa ON (p.fkContext = pa.fkProcess)
		LEFT JOIN view_rm_asset_active a ON (pa.fkAsset = a.fkContext)
		LEFT JOIN view_rm_risk_active r ON (a.fkContext = r.fkAsset)
		LEFT JOIN view_rm_risk_control_active rc ON (r.fkContext = rc.fkRisk)
		GROUP BY ar.fkContext, rc.fkControl
	) res
	LEFT JOIN rm_control_cost cc ON (res.control_id = cc.fkControl)
	GROUP BY area_id'';

FOR id_values IN EXECUTE query LOOP	
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (id_values.id, now(), ''control_amount_by_area'', id_values.value1);
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (id_values.id, now(), ''control_cost_by_area'', id_values.value2);	
END LOOP;
	
-- Qtd. de processos de uma área

query := ''SELECT a.fkContext as area_id, count(p.fkContext) as process_count
	FROM view_rm_area_active a
	LEFT JOIN view_rm_process_active p ON (a.fkContext = p.fkArea)
	GROUP BY a.fkContext'';

FOR id_value IN EXECUTE query LOOP	
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (id_value.id, now(), ''process_amount_by_area'', id_value.value);	
END LOOP;

-- Qtd. e custo dos ativos de uma área

query := ''
	SELECT area_id, count(asset_id) as asset_count, CASE WHEN sum(a.nCost) IS NULL THEN 0 ELSE sum(a.nCost) END as asset_cost
	FROM
	(
		SELECT a.fkContext as area_id, pa.fkAsset as asset_id
		FROM view_rm_area_active a
		LEFT JOIN view_rm_process_active p ON (a.fkContext = p.fkArea)
		LEFT JOIN view_rm_process_asset_active pa ON (p.fkContext = pa.fkProcess)
		GROUP BY a.fkContext, pa.fkAsset
	) res
	LEFT JOIN view_rm_asset_active a ON (res.asset_id = a.fkContext)
	GROUP BY area_id'';

FOR id_values IN EXECUTE query LOOP	
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (id_values.id, now(), ''asset_amount_by_area'', id_values.value1);
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (id_values.id, now(), ''asset_cost_by_area'', id_values.value2);	
END LOOP;

/*===============================================INFORMAÇÕES SOBRE PROCESSO========================================================*/

-- Valor do processo

query := ''SELECT fkContext, nValue FROM view_rm_process_active'';

FOR id_value IN EXECUTE query LOOP	
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (id_value.id, now(), ''process_value'', id_value.value);	
END LOOP;

-- Quantidade e custo dos ativos por processo

query := ''SELECT p.fkContext, count(pa.fkAsset) as asset_count, CASE WHEN sum(a.nCost) IS NULL THEN 0 ELSE sum(a.nCost) END
	FROM view_rm_process_active p
	LEFT JOIN view_rm_process_asset_active pa ON (p.fkContext = pa.fkProcess)
	LEFT JOIN view_rm_asset_active a ON (pa.fkAsset = a.fkContext)
	GROUP BY p.fkContext'';

FOR id_values IN EXECUTE query LOOP	
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (id_values.id, now(), ''asset_amount_by_process'', id_values.value1);
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (id_values.id, now(), ''asset_cost_by_process'', id_values.value2);	
END LOOP;
	
-- Quantidade de riscos por processo

query := ''SELECT p.fkContext, count(r.fkContext) as risk_count
	FROM view_rm_process_active p
	LEFT JOIN view_rm_process_asset_active pa ON (p.fkContext = pa.fkProcess)
	LEFT JOIN view_rm_asset_active a ON (pa.fkAsset = a.fkContext)
	LEFT JOIN view_rm_risk_active r ON (a.fkContext = r.fkAsset)
	GROUP BY p.fkContext'';

FOR id_value IN EXECUTE query LOOP	
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (id_value.id, now(), ''risk_amount_by_process'', id_value.value);	
END LOOP;

-- N° total de processos

quantity := (SELECT count(*) FROM view_rm_process_active);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), ''process_total'', quantity);

-- N° processos com risco alto

quantity := (SELECT count(*) FROM view_rm_process_active WHERE nValue >= risk_high);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), ''processes_with_high_risk'', quantity);

-- N° processos com risco médio

quantity := (SELECT count(*) FROM view_rm_process_active WHERE nValue > risk_low AND nValue < risk_high);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), ''processes_with_medium_risk'', quantity);

-- N° processos com risco baixo

quantity := (SELECT count(*) FROM view_rm_process_active WHERE nValue <= risk_low AND nValue <> 0);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), ''processes_with_low_risk'', quantity);

-- N° processos com risco não parametrizado

quantity := (SELECT count(*) FROM view_rm_process_active WHERE nValue = 0);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), ''processes_with_no_value_risk'', quantity);

-- N° processos completos

quantity := (
	SELECT count(*)
	FROM view_rm_process_active p
	WHERE p.nValue > 0 AND NOT EXISTS (
		SELECT * 
		FROM view_rm_process_asset_active pa 
		JOIN view_rm_asset_active a ON (pa.fkAsset = a.fkContext) 
		WHERE pa.fkProcess = p.fkContext AND a.nValue = 0
	)
);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), ''complete_processes'', quantity);

-- N° processos parcias

quantity := (
	SELECT count(*)
	FROM view_rm_process_active p
	WHERE p.nValue > 0 AND EXISTS (
		SELECT * 
		FROM view_rm_process_asset_active pa 
		JOIN view_rm_asset_active a ON (pa.fkAsset = a.fkContext) 
		WHERE pa.fkProcess = p.fkContext AND a.nValue = 0
	)
);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), ''partial_processes'', quantity);

-- N° processos não gerenciados

quantity := (SELECT count(*) FROM view_rm_process_active p WHERE p.nValue = 0);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), ''not_managed_processes'', quantity);

-- N° processos sem ativos

quantity := (
	SELECT count(*)
	FROM view_rm_process_active p
	WHERE p.fkContext NOT IN (SELECT fkProcess FROM view_rm_process_asset_active)
);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), ''processes_without_assets'', quantity);

-- Total de impacto por processo

query := ''SELECT p.fkContext as process_id, CASE WHEN sum(r.nCost) IS NULL THEN 0 ELSE sum(r.nCost) END as total_impact
	FROM view_rm_process_active p
	LEFT JOIN view_rm_process_asset_active pa ON (p.fkContext = pa.fkProcess)
	LEFT JOIN view_rm_asset_active a ON (pa.fkAsset = a.fkContext)
	LEFT JOIN view_rm_risk_active r ON (a.fkContext = r.fkAsset)
	GROUP BY p.fkContext'';

FOR id_value IN EXECUTE query LOOP	
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (id_value.id, now(), ''total_process_impact'', id_value.value);	
END LOOP;

-- Custo e quantidade dos controles por processo

query := ''
	/*Calcula o custo total dos controles de cada processo e a quantidade de controles de cada processo*/
	SELECT process_id, count(control_id) as control_count, CASE WHEN sum(cc.nCost1+cc.nCost2+cc.nCost3+cc.nCost4+cc.nCost5) IS NULL THEN 0 ELSE sum(cc.nCost1+cc.nCost2+cc.nCost3+cc.nCost4+cc.nCost5) END as control_cost
	FROM
	(
		/*Garante que um controle só irá ser contabilizado uma vez para um determinado processo*/
		SELECT p.fkContext as process_id, rc.fkControl as control_id
		FROM view_rm_process_active p
		LEFT JOIN view_rm_process_asset_active pa ON (p.fkContext = pa.fkProcess)
		LEFT JOIN view_rm_asset_active a ON (pa.fkAsset = a.fkContext)
		LEFT JOIN view_rm_risk_active r ON (a.fkContext = r.fkAsset)
		LEFT JOIN view_rm_risk_control_active rc ON (r.fkContext = rc.fkRisk)
		GROUP BY p.fkContext, rc.fkControl
	) res
	LEFT JOIN rm_control_cost cc ON (res.control_id = cc.fkControl)
	GROUP BY process_id'';

FOR id_values IN EXECUTE query LOOP	
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (id_values.id, now(), ''control_amount_by_process'', id_values.value1);
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (id_values.id, now(), ''control_cost_by_process'', id_values.value2);
END LOOP;

/*===============================================INFORMAÇÕES SOBRE ATIVO========================================================*/

-- Valor do ativo

query := ''SELECT fkContext, nValue FROM view_rm_asset_active'';

FOR id_value IN EXECUTE query LOOP	
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (id_value.id, now(), ''asset_value'', id_value.value);	
END LOOP;	

-- Quantidade de processos por ativo

query := ''SELECT a.fkContext, count(pa.fkProcess) as process_count
	FROM view_rm_asset_active a
	LEFT JOIN view_rm_process_asset_active pa ON (a.fkContext = pa.fkAsset)
	GROUP BY a.fkContext'';

FOR id_value IN EXECUTE query LOOP	
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (id_value.id, now(), ''process_amount_by_asset'', id_value.value);	
END LOOP;

-- Quantidade de riscos por ativo

query := ''SELECT a.fkContext, count(r.fkContext) as risk_count
	FROM view_rm_asset_active a
	LEFT JOIN view_rm_risk_active r ON (a.fkContext = r.fkAsset)
	GROUP BY a.fkContext'';

FOR id_value IN EXECUTE query LOOP	
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (id_value.id, now(), ''risk_amount_by_asset'', id_value.value);	
END LOOP;

-- N° total de ativos

quantity := (SELECT count(*) FROM view_rm_asset_active);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), ''asset_total'', quantity);

-- N° ativos com risco alto

quantity := (SELECT count(*) FROM view_rm_asset_active WHERE nValue >= risk_high);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), ''assets_with_high_risk'', quantity);

-- N° ativos com risco médio

quantity := (SELECT count(*) FROM view_rm_asset_active WHERE nValue > risk_low AND nValue < risk_high);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), ''assets_with_medium_risk'', quantity);

-- N° ativos com risco baixo

quantity := (SELECT count(*) FROM view_rm_asset_active WHERE nValue <= risk_low AND nValue <> 0);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), ''assets_with_low_risk'', quantity);

-- N° ativos com risco não parametrizado

quantity := (SELECT count(*) FROM view_rm_asset_active WHERE nValue = 0);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), ''assets_with_no_value_risk'', quantity);

-- N° ativos completos

quantity := (
	SELECT count(*) FROM view_rm_asset_active a
	WHERE a.nValue > 0 AND NOT EXISTS (
		SELECT * FROM view_rm_risk_active r WHERE r.fkAsset = a.fkContext AND r.nValue = 0
	)
);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), ''complete_assets'', quantity);

-- N° ativos parcias

quantity := (
	SELECT count(*) FROM view_rm_asset_active a
	WHERE a.nValue > 0 AND EXISTS (
		SELECT * FROM view_rm_risk_active r WHERE r.fkAsset = a.fkContext AND r.nValue = 0
	)
);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), ''partial_assets'', quantity);

-- N° ativos não gerenciados

quantity := (SELECT count(*) FROM view_rm_asset_active a WHERE a.nValue = 0);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), ''not_managed_assets'', quantity);

-- N° ativos sem riscos

quantity := (
	SELECT count(*)
	FROM view_rm_asset_active a
	WHERE a.fkContext NOT IN (SELECT fkAsset FROM view_rm_risk_active)
);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), ''assets_without_risks'', quantity);

-- Total de impacto dos ativos

query := ''SELECT a.fkContext, CASE WHEN sum(r.nCost) IS NULL THEN 0 ELSE sum(r.nCost) END
	FROM view_rm_asset_active a 
	LEFT JOIN view_rm_risk_active r ON (a.fkContext = r.fkAsset)
	GROUP BY a.fkContext'';

FOR id_value IN EXECUTE query LOOP	
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (id_value.id, now(), ''total_asset_impact'', id_value.value);	
END LOOP;	

-- Custo e quantidade dos controles por ativo

query := ''
	/*Calcula o custo total dos controles de cada ativo e a quantidade de controles de cada ativo*/
	SELECT res.asset_id, count(control_id) as control_count, CASE WHEN sum(cc.nCost1+cc.nCost2+cc.nCost3+cc.nCost4+cc.nCost5) IS NULL THEN 0 ELSE sum(cc.nCost1+cc.nCost2+cc.nCost3+cc.nCost4+cc.nCost5) END as control_cost
	FROM
	(
		/*Garante que um controle só irá ser contabilizado uma vez para um determinado ativo*/
		SELECT a.fkContext as asset_id, rc.fkControl as control_id
		FROM view_rm_asset_active a
		LEFT JOIN view_rm_risk_active r ON (a.fkContext = r.fkAsset)
		LEFT JOIN view_rm_risk_control_active rc ON (r.fkContext = rc.fkRisk)
		GROUP BY a.fkContext, rc.fkControl
	) res 
	LEFT JOIN rm_control_cost cc ON (res.control_id = cc.fkControl)
	GROUP BY asset_id'';

FOR id_values IN EXECUTE query LOOP	
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (id_values.id, now(), ''control_amount_by_asset'', id_values.value1);
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (id_values.id, now(), ''control_cost_by_asset'', id_values.value2);
END LOOP;

/*===============================================INFORMAÇÕES SOBRE RISCO========================================================*/

-- Valor real e residual do risco

query := ''SELECT fkContext, nValue, nValueResidual FROM view_rm_risk_active'';

FOR id_values IN EXECUTE query LOOP	
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (id_values.id, now(), ''risk_value'', id_values.value1);
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (id_values.id, now(), ''risk_residual_value'', id_values.value2);
END LOOP;

-- Quantidade de controles dos riscos

query := ''SELECT r.fkContext, count(rc.fkControl) as control_count
	FROM view_rm_risk_active r
	LEFT JOIN view_rm_risk_control_active rc ON (r.fkContext = rc.fkRisk)
	GROUP BY r.fkContext'';

FOR id_value IN EXECUTE query LOOP	
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (id_value.id, now(), ''control_amount_by_risk'', id_value.value);	
END LOOP;	

-- N° total de riscos

quantity := (SELECT count(*) FROM view_rm_risk_active);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), ''risk_total'', quantity);

-- N° riscos com risco real alto

quantity := (SELECT count(*) FROM view_rm_risk_active WHERE nValue >= risk_high);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), ''risks_with_high_real_risk'', quantity);

-- N° riscos com risco residual alto

quantity := (SELECT count(*) FROM view_rm_risk_active WHERE nValueResidual >= risk_high);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), ''risks_with_high_residual_risk'', quantity);

-- N° riscos com risco real médio

quantity := (SELECT count(*) FROM view_rm_risk_active WHERE nValue > risk_low AND nValue < risk_high);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), ''risks_with_medium_real_risk'', quantity);

-- N° riscos com risco residual médio

quantity := (SELECT count(*) FROM view_rm_risk_active WHERE nValueResidual > risk_low AND nValueResidual < risk_high);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), ''risks_with_medium_residual_risk'', quantity);

-- N° riscos com risco real baixo

quantity := (SELECT count(*) FROM view_rm_risk_active WHERE nValue <= risk_low AND nValue <> 0);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), ''risks_with_low_real_risk'', quantity);

-- N° riscos com risco residual baixo

quantity := (SELECT count(*) FROM view_rm_risk_active WHERE nValueResidual <= risk_low AND nValueResidual <> 0);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), ''risks_with_low_residual_risk'', quantity);

-- N° riscos com risco não parametrizado

quantity := (SELECT count(*) FROM view_rm_risk_active WHERE nValue = 0);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), ''risks_with_no_value_risk'', quantity);

-- N° riscos tratados

quantity := (SELECT count(*) FROM view_rm_risk_active r WHERE r.nAcceptMode<>0 OR r.nValue != r.nValueResidual);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), ''risks_treated'', quantity);

-- N° riscos não tratados

quantity := (SELECT count(*) FROM view_rm_risk_active r WHERE r.nAcceptMode=0 AND r.nValue = r.nValueResidual AND r.nValue > 0);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), ''risks_not_treated'', quantity);

-- N° riscos aceitos

quantity := (SELECT count(*) FROM view_rm_risk_active r WHERE r.nAcceptMode = 72601);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), ''risks_accepted'', quantity);

-- N° riscos transferidos

quantity := (SELECT count(*) FROM view_rm_risk_active r WHERE r.nAcceptMode = 72602);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), ''risks_transfered'', quantity);

-- N° riscos evitados

quantity := (SELECT count(*) FROM view_rm_risk_active r WHERE r.nAcceptMode = 72603);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), ''risks_avoided'', quantity);

/*===============================================INFORMAÇÕES SOBRE CONTROLE========================================================*/

-- Quantidade de riscos por controle

query := ''SELECT c.fkContext, count(rc.fkRisk) as risk_count
	FROM view_rm_control_active c
	LEFT JOIN view_rm_risk_control_active rc ON (c.fkContext = rc.fkControl)
	GROUP BY c.fkContext'';

FOR id_value IN EXECUTE query LOOP	
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (id_value.id, now(), ''risk_amount_by_control'', id_value.value);	
END LOOP;

-- N° total de controles

quantity := (SELECT count(*) FROM view_rm_control_active);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), ''control_total'', quantity);

-- N° de controles com implantação planejada

quantity := (SELECT count(*) FROM view_rm_control_active c WHERE c.dDateDeadline >= now() AND c.dDateImplemented IS NULL);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), ''planned_implantation_controls'', quantity);

-- N° de controles com implantação atrasada

quantity := (SELECT count(*) FROM view_rm_control_active c WHERE c.dDateDeadline < now() AND c.dDateImplemented IS NULL);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), ''late_implantation_controls'', quantity);

-- N° de controles implementados adequadamente

quantity := (
	SELECT count(*)
	FROM view_rm_control_active c
	LEFT JOIN rm_control_test_history ct ON (c.fkContext = ct.fkControl)
	LEFT JOIN wkf_control_efficiency ce ON (c.fkContext = ce.fkControlEfficiency)
	WHERE c.dDateImplemented IS NOT NULL AND ((ct.bTestedValue = 1 AND ce.nRealEfficiency >= ce.nExpectedEfficiency) OR (ct.bTestedValue = 1 AND ce.nRealEfficiency IS NULL) OR (ct.bTestedValue IS NULL AND ce.nRealEfficiency >= ce.nExpectedEfficiency))
);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), ''adequate_implemented_controls'', quantity);

-- N° de controles implementados inadequadamente

quantity := (
	SELECT count(*)
	FROM view_rm_control_active c
	LEFT JOIN rm_control_test_history ct ON (c.fkContext = ct.fkControl)
	LEFT JOIN wkf_control_efficiency ce ON (c.fkContext = ce.fkControlEfficiency)
	WHERE 
	(
		(ct.bTestedValue=0 AND ce.nRealEfficiency>=ce.nExpectedEfficiency) OR
		(ct.bTestedValue=0 AND ce.nRealEfficiency IS NULL) OR
		(ct.bTestedValue IS NULL AND ce.nRealEfficiency<ce.nExpectedEfficiency) OR
		(ct.bTestedValue=1 AND ce.nRealEfficiency<ce.nExpectedEfficiency) OR
		(ct.bTestedValue=0 AND ce.nRealEfficiency<ce.nExpectedEfficiency)
	)
);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), ''inadequate_implemented_controls'', quantity);

-- N° de controles implementados mas não medidos

quantity := (
	SELECT count(*)
	FROM view_rm_control_active c
	LEFT JOIN rm_control_test_history ct ON (c.fkContext = ct.fkControl)
	LEFT JOIN wkf_control_efficiency ce ON (c.fkContext = ce.fkControlEfficiency)
	WHERE
	c.fkContext NOT IN (SELECT fkControlEfficiency FROM wkf_control_efficiency) AND
	c.fkContext NOT IN (SELECT fkControlTest FROM wkf_control_test)
);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), ''not_measured_implemented_controls'', quantity);

-- N° de controles testados com sucesso

quantity := (
	SELECT count(*)
	FROM view_rm_control_active c
	JOIN rm_control_test_history ct ON (c.fkContext = ct.fkControl AND ct.bTestedValue = 1 AND c.dDateImplemented IS NOT NULL)
);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), ''tested_ok_controls'', quantity);

-- N° de controles testados sem sucesso

quantity := (
	SELECT count(*)
	FROM view_rm_control_active c
	JOIN rm_control_test_history ct ON (c.fkContext = ct.fkControl AND ct.bTestedValue = 0 AND c.dDateImplemented IS NOT NULL)
);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), ''tested_not_ok_controls'', quantity);

-- N° de controles revisados com sucesso

quantity := (
	SELECT count(*)
	FROM view_rm_control_active c
	JOIN wkf_control_efficiency ce ON (c.fkContext = ce.fkControlEfficiency AND ce.nRealEfficiency >= ce.nExpectedEfficiency AND c.dDateImplemented IS NOT NULL)
);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), ''revision_ok_controls'', quantity);

-- N° de controles revisados sem sucesso

quantity := (
	SELECT count(*)
	FROM view_rm_control_active c

	JOIN wkf_control_efficiency ce ON (c.fkContext = ce.fkControlEfficiency AND ce.nRealEfficiency < ce.nExpectedEfficiency AND c.dDateImplemented IS NOT NULL)
);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), ''revision_not_ok_controls'', quantity);

-- N° de controles sem riscos

quantity := (
	SELECT count(*)
	FROM view_rm_control_active c
	WHERE c.fkContext NOT IN (SELECT fkControl FROM view_rm_risk_control_active)
);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), ''controls_without_risks'', quantity);

/*===============================================INFORMAÇÕES SOBRE NORMA========================================================*/

-- Quantidade de melhores práticas por norma

query := ''SELECT s.fkContext, count(bps.fkBestPractice)
	FROM view_rm_standard_active s
	LEFT JOIN view_rm_bp_standard_active bps ON (s.fkContext = bps.fkStandard)
	GROUP BY s.fkContext'';

FOR id_value IN EXECUTE query LOOP	
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (id_value.id, now(), ''best_practice_amount_by_standard'', id_value.value);	
END LOOP;

-- Quantidade de melhores práticas aplicadas por norma

query := ''SELECT s.fkContext, count(bps.fkBestPractice)
	FROM view_rm_standard_active s
	LEFT JOIN rm_best_practice_standard bps ON (s.fkContext = bps.fkStandard)
	AND bps.fkBestPractice IN (SELECT cbp.fkBestPractice FROM view_rm_control_bp_active cbp)
	GROUP BY s.fkContext'';

FOR id_value IN EXECUTE query LOOP	
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (id_value.id, now(), ''applied_best_practice_amount_by_standard'', id_value.value);	
END LOOP;

/*===============================================INFORMAÇÕES FINANCEIRAS========================================================*/

-- Custo total dos ativos

cost := (SELECT CASE WHEN sum(nCost) IS NULL THEN 0 ELSE sum(nCost) END FROM view_rm_asset_active);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), ''asset_total_cost'', cost);

-- Custo total dos controles

query := ''SELECT 
	CASE WHEN sum(cc.nCost1) IS NULL THEN 0 ELSE sum(cc.nCost1) END,
	CASE WHEN sum(cc.nCost2) IS NULL THEN 0 ELSE sum(cc.nCost2) END,
	CASE WHEN sum(cc.nCost3) IS NULL THEN 0 ELSE sum(cc.nCost3) END,
	CASE WHEN sum(cc.nCost4) IS NULL THEN 0 ELSE sum(cc.nCost4) END,
	CASE WHEN sum(cc.nCost5) IS NULL THEN 0 ELSE sum(cc.nCost5) END
	FROM view_rm_control_active c
	JOIN rm_control_cost cc ON (c.fkContext = cc.fkControl)'';

FOR control_cost IN EXECUTE query LOOP	
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (NULL, now(), ''control_total_cost1'', control_cost.cost1);
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (NULL, now(), ''control_total_cost2'', control_cost.cost2);
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (NULL, now(), ''control_total_cost3'', control_cost.cost3);
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (NULL, now(), ''control_total_cost4'', control_cost.cost4);
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (NULL, now(), ''control_total_cost5'', control_cost.cost5);
END LOOP;

-- Custo dos riscos potencialmente baixos

cost := (SELECT CASE WHEN sum(nCost) IS NULL THEN 0 ELSE sum(nCost) END FROM view_rm_risk_active r WHERE r.nValue <= risk_low);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), ''potentially_low_risk_cost'', cost);

-- Custo dos riscos mitigados

cost := (SELECT CASE WHEN sum(nCost) IS NULL THEN 0 ELSE sum(nCost) END FROM view_rm_risk_active r WHERE r.fkContext IN (SELECT fkRisk FROM view_rm_risk_control_active));
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), ''mitigated_risk_cost'', cost);

-- Custo dos riscos evitados

cost := (SELECT CASE WHEN sum(nCost) IS NULL THEN 0 ELSE sum(nCost) END FROM view_rm_risk_active r WHERE r.nAcceptMode = 72603);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), ''avoided_risk_cost'', cost);

-- Custo dos riscos transferidos

cost := (SELECT CASE WHEN sum(nCost) IS NULL THEN 0 ELSE sum(nCost) END FROM view_rm_risk_active r WHERE r.nAcceptMode = 72602);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), ''transfered_risk_cost'', cost);

-- Custo dos riscos aceitos

cost := (SELECT CASE WHEN sum(nCost) IS NULL THEN 0 ELSE sum(nCost) END FROM view_rm_risk_active r WHERE r.nAcceptMode = 72601);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), ''accepted_risk_cost'', cost);

-- Custo dos riscos médio / alto / não tratados

cost := (
	SELECT CASE WHEN sum(nCost) IS NULL THEN 0 ELSE sum(nCost) END FROM view_rm_risk_active r
	WHERE r.nAcceptMode = 0 AND r.nValue > risk_low AND r.fkContext NOT IN (SELECT fkRisk FROM view_rm_risk_control_active)
);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), ''not_treated_and_medium_high_risk'', cost);

/*================================================= POLICY MANAGEMENT ==========================================================*/

-- Quantidade de Documentos Publicados
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT CAST(NULL AS INTEGER), NOW(), ''document_total'', count(*) FROM view_pm_published_docs;

-- Quantidade de Documentos por Estado
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT CAST(NULL AS INTEGER), NOW(), ''documents_by_state_developing'', count(d.fkContext)
FROM view_pm_document_active d JOIN isms_context c ON (c.pkContext = d.fkContext)
WHERE c.nState = 2751
UNION
SELECT CAST(NULL AS INTEGER), NOW(), ''documents_by_state_approved'', count(d.fkContext)
FROM view_pm_document_active d JOIN isms_context c ON (c.pkContext = d.fkContext)
WHERE c.nState = 2752
UNION
SELECT CAST(NULL AS INTEGER), NOW(), ''documents_by_state_pendant'', count(d.fkContext)
FROM view_pm_document_active d JOIN isms_context c ON (c.pkContext = d.fkContext)
WHERE c.nState = 2753;

-- Quantidade de Documentos por Tipo
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT CAST(NULL AS INTEGER), NOW(), ''documents_by_type_none'', count(d.fkContext)
FROM view_pm_document_active d
WHERE d.nType = 2800
UNION
SELECT CAST(NULL AS INTEGER), NOW(), ''documents_by_type_area'', count(d.fkContext)
FROM view_pm_document_active d
WHERE d.nType = 2801
UNION
SELECT CAST(NULL AS INTEGER), NOW(), ''documents_by_type_process'', count(d.fkContext)
FROM view_pm_document_active d
WHERE d.nType = 2802
UNION
SELECT CAST(NULL AS INTEGER), NOW(), ''documents_by_type_asset'', count(d.fkContext)
FROM view_pm_document_active d
WHERE d.nType = 2803
UNION
SELECT CAST(NULL AS INTEGER), NOW(), ''documents_by_type_control'', count(d.fkContext)
FROM view_pm_document_active d
WHERE d.nType = 2805
UNION
SELECT CAST(NULL AS INTEGER), NOW(), ''documents_by_type_scope'', count(d.fkContext)
FROM view_pm_document_active d
WHERE d.nType = 2820
UNION
SELECT CAST(NULL AS INTEGER), NOW(), ''documents_by_type_policy'', count(d.fkContext)
FROM view_pm_document_active d
WHERE d.nType = 2821
UNION
SELECT CAST(NULL AS INTEGER), NOW(), ''documents_by_type_management'', count(d.fkContext)
FROM view_pm_document_active d
WHERE d.nType = 3801
UNION
SELECT CAST(NULL AS INTEGER), NOW(), ''documents_by_type_others'', count(d.fkContext)
FROM view_pm_document_active d
WHERE d.nType = 3802;

-- Quantidade de Registros
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT CAST(NULL AS INTEGER), NOW(), ''register_total'', count(*) FROM view_pm_register_active;

-- Quantidade de Documentos Já Lidos
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT CAST(NULL AS INTEGER), NOW(), ''documents_read'', count(*) FROM view_pm_read_docs;

-- Quantidade de Documentos Não Lidos
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT CAST(NULL AS INTEGER), NOW(), ''documents_not_read'', count(fkContext)
FROM view_pm_published_docs
WHERE fkContext NOT IN (SELECT fkContext FROM view_pm_read_docs);

-- Quantidade de Documentos com Revisão Agendada
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT CAST(NULL AS INTEGER), NOW(), ''documents_scheduled'', count(*)
FROM view_pm_published_docs WHERE fkSchedule IS NOT NULL;

-- Ocupação dos arquivos de documentos
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT CAST(NULL AS INTEGER), NOW(), ''occupation_documents'', CASE WHEN SUM(di.nFileSize) > 0 THEN ROUND(CAST(SUM(di.nFileSize) AS FLOAT)/1048576.0) ELSE 0 END
FROM pm_doc_instance di
WHERE NOT EXISTS (
  SELECT * FROM pm_document d JOIN pm_register r ON (r.fkDocument = d.fkContext)
  WHERE d.fkContext = di.fkDocument
);

-- Ocupação dos arquivos de template
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT CAST(NULL AS INTEGER), NOW(), ''occupation_template'', CASE WHEN SUM(t.nFileSize) > 0 THEN ROUND(CAST(SUM(t.nFileSize) AS FLOAT)/1048576.0) ELSE 0 END
FROM pm_template t;

-- Ocupação dos arquivos de registro
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT CAST(NULL AS INTEGER), NOW(), ''occupation_registers'', CASE WHEN SUM(di.nFileSize) > 0 THEN ROUND(CAST(SUM(di.nFileSize) AS FLOAT)/1048576.0) ELSE 0 END
FROM
  pm_doc_instance di
  JOIN pm_document d ON (d.fkContext = di.fkDocument)
  JOIN pm_register r ON (r.fkDocument = d.fkContext);

-- Ocupação total
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT CAST(NULL AS INTEGER), NOW(), ''occupation_total'', CASE WHEN SUM(f.nFileSize) > 0 THEN ROUND(CAST(SUM(f.nFileSize) AS FLOAT)/1048576.0) ELSE 0 END
FROM (
  SELECT nFileSize FROM pm_doc_instance
  UNION ALL
  SELECT nFileSize FROM pm_template
) f;

/*=============================================== CONTINUAL IMPROVEMENT ========================================================*/

-- Quantidade de Incidentes
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT CAST(NULL AS INTEGER), NOW(), ''incident_total'', count(*) FROM view_ci_incident_active;

-- Quantidade de Não-Conformidades
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT CAST(NULL AS INTEGER), NOW(), ''non_conformity_total'', count(*) FROM view_ci_nc_active;

-- Quantidade de Incidentes por Estado
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT CAST(NULL AS INTEGER), NOW(), ''incidents_by_state_open'', count(i.fkContext)
FROM view_ci_incident_active i JOIN isms_context c ON (c.pkContext = i.fkContext)
WHERE c.nState = 2779
UNION
SELECT CAST(NULL AS INTEGER), NOW(), ''incidents_by_state_directed'', count(i.fkContext)
FROM view_ci_incident_active i JOIN isms_context c ON (c.pkContext = i.fkContext)
WHERE c.nState = 2780
UNION
SELECT CAST(NULL AS INTEGER), NOW(), ''incidents_by_state_pendant_disposal'', count(i.fkContext)
FROM view_ci_incident_active i JOIN isms_context c ON (c.pkContext = i.fkContext)
WHERE c.nState = 2781
UNION
SELECT CAST(NULL AS INTEGER), NOW(), ''incidents_by_state_waiting_solution'', count(i.fkContext)
FROM view_ci_incident_active i JOIN isms_context c ON (c.pkContext = i.fkContext)
WHERE c.nState = 2782
UNION
SELECT CAST(NULL AS INTEGER), NOW(), ''incidents_by_state_pendant_solution'', count(i.fkContext)
FROM view_ci_incident_active i JOIN isms_context c ON (c.pkContext = i.fkContext)
WHERE c.nState = 2783
UNION
SELECT CAST(NULL AS INTEGER), NOW(), ''incidents_by_state_solved'', count(i.fkContext)
FROM view_ci_incident_active i JOIN isms_context c ON (c.pkContext = i.fkContext)
WHERE c.nState = 2784;

-- Quantidade de Não-Conformidades por Estado
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT CAST(NULL AS INTEGER), NOW(), ''nc_by_state_ci_sent'', count(nc.fkContext)
FROM view_ci_nc_active nc JOIN isms_context c ON (c.pkContext = nc.fkContext)
WHERE c.nState = 2770
UNION
SELECT CAST(NULL AS INTEGER), NOW(), ''nc_by_state_ci_open'', count(nc.fkContext)
FROM view_ci_nc_active nc JOIN isms_context c ON (c.pkContext = nc.fkContext)
WHERE c.nState = 2771
UNION
SELECT CAST(NULL AS INTEGER), NOW(), ''nc_by_state_ci_directed'', count(nc.fkContext)
FROM view_ci_nc_active nc JOIN isms_context c ON (c.pkContext = nc.fkContext)
WHERE c.nState = 2772
UNION
SELECT CAST(NULL AS INTEGER), NOW(), ''nc_by_state_ci_nc_pendant'', count(nc.fkContext)
FROM view_ci_nc_active nc JOIN isms_context c ON (c.pkContext = nc.fkContext)
WHERE c.nState = 2773
UNION
SELECT CAST(NULL AS INTEGER), NOW(), ''nc_by_state_ci_ap_pendant'', count(nc.fkContext)
FROM view_ci_nc_active nc JOIN isms_context c ON (c.pkContext = nc.fkContext)
WHERE c.nState = 2774
UNION
SELECT CAST(NULL AS INTEGER), NOW(), ''nc_by_state_ci_waiting_conclusion'', count(nc.fkContext)
FROM view_ci_nc_active nc JOIN isms_context c ON (c.pkContext = nc.fkContext)
WHERE c.nState = 2775
UNION
SELECT CAST(NULL AS INTEGER), NOW(), ''nc_by_state_ci_finished'', count(nc.fkContext)
FROM view_ci_nc_active nc JOIN isms_context c ON (c.pkContext = nc.fkContext)
WHERE c.nState = 2776
UNION
SELECT CAST(NULL AS INTEGER), NOW(), ''nc_by_state_ci_closed'', count(nc.fkContext)
FROM view_ci_nc_active nc JOIN isms_context c ON (c.pkContext = nc.fkContext)
WHERE c.nState = 2777
UNION
SELECT CAST(NULL AS INTEGER), NOW(), ''nc_by_state_denied'', count(nc.fkContext)
FROM view_ci_nc_active nc JOIN isms_context c ON (c.pkContext = nc.fkContext)
WHERE c.nState = 2703;

/*=============================================== ACTION PLAN STATISTICS ========================================================*/

-- Quantidade de Planos de Ação
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT CAST(NULL AS INTEGER), NOW(), ''ap_total'', count(*) FROM view_ci_action_plan_active;

-- Quantidade de Planos de Ação finalizandos antes do deadline
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT CAST(NULL AS INTEGER), NOW(), ''ap_finished'', count(*) 
   FROM view_ci_action_plan_active
   WHERE dDateConclusion IS NOT NULL
         AND dDateConclusion <= dDateDeadLine;

-- Quantidade de Planos de Ação finalizandos em atrazo
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT CAST(NULL AS INTEGER), NOW(), ''ap_finished_late'', count(*) 
   FROM view_ci_action_plan_active
   WHERE dDateConclusion IS NOT NULL
         AND dDateConclusion > dDateDeadLine;

-- Quantidade de Planos de Ação eficientes
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT CAST(NULL AS INTEGER), NOW(), ''ap_efficient'', count(*) 
   FROM view_ci_action_plan_active
   WHERE dDateConclusion IS NOT NULL
         AND bIsEfficient = 1;

-- Quantidade de Planos de Ação não eficientes
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT CAST(NULL AS INTEGER), NOW(), ''ap_non_efficient'', count(*) 
   FROM view_ci_action_plan_active
   WHERE dDateConclusion IS NOT NULL
         AND bIsEfficient = 0;

-- média de não conformidades por plano de ação
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT CAST(NULL AS INTEGER), NOW(), ''nc_per_ap_average'', 
	CASE WHEN (SELECT COUNT(*) FROM (SELECT COUNT(*) FROM ci_nc_action_plan r JOIN isms_context ctx2 ON (r.fknc = ctx2.pkContext AND ctx2.pkContext <> 2705) GROUP BY fkActionPlan)aps) > 0
	            THEN (SELECT COUNT(*) FROM (SELECT COUNT(*) FROM ci_nc_action_plan r JOIN isms_context ctx1 ON (r.fknc = ctx1.pkContext AND ctx1.pkContext <> 2705) GROUP BY r.fknc)ncs)::float
		          /
		         (SELECT COUNT(*) FROM (SELECT COUNT(*) FROM ci_nc_action_plan r JOIN isms_context ctx2 ON (r.fknc = ctx2.pkContext AND ctx2.pkContext <> 2705) GROUP BY fkActionPlan)aps)::float
	            ELSE 0 END;

/*==============================================================================================================================*/

-- Grava a data da coleta no banco de dados

UPDATE isms_config set sValue = date_part(''year'', now()) || ''-'' || date_part(''month'', now()) || ''-'' || date_part(''day'', now()) WHERE pkConfig = 420;

END IF;

RETURN 1;
END'
  LANGUAGE 'plpgsql' VOLATILE;
