CREATE OR REPLACE FUNCTION delete_area(integer)
  RETURNS integer AS
'DECLARE 
    ctx context_id%rowtype;
BEGIN
   FOR ctx IN EXECUTE '' SELECT fkContext FROM rm_area WHERE fkParent = '' || $1 LOOP
	if(ctx.id IS NOT NULL) THEN	
		PERFORM delete_area(ctx.id);
	END IF;
   END LOOP;
   DELETE FROM rm_area	WHERE fkContext = $1;
   DELETE FROM isms_context WHERE pkContext = $1;
   RETURN 1;	
END'
  LANGUAGE 'plpgsql' VOLATILE;