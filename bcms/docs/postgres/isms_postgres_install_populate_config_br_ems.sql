/* PERFIL */
INSERT INTO isms_context (pkContext,nType,nState) VALUES (1,2817,2700);          INSERT INTO isms_profile (fkContext,sName,nId) VALUES (1,'Gestor Ambiental',8801);         	INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1,2901,NOW());      INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1,2902,NOW());      
INSERT INTO isms_context (pkContext,nType,nState) VALUES (2,2817,2700);          INSERT INTO isms_profile (fkContext,sName,nId) VALUES (2,'Gestor',8802);           					INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,2,2901,NOW());      INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,2,2902,NOW());      
INSERT INTO isms_context (pkContext,nType,nState) VALUES (3,2817,2700);          INSERT INTO isms_profile (fkContext,sName,nId) VALUES (3,'Administrador',8803);       			INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,3,2901,NOW());      INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,3,2902,NOW());      
INSERT INTO isms_context (pkContext,nType,nState) VALUES (4,2817,2700);          INSERT INTO isms_profile (fkContext,sName,nId) VALUES (4,'Sem Permissões',8804);     INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,4,2901,NOW());      INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,4,2902,NOW());      
INSERT INTO isms_context (pkContext,nType,nState) VALUES (5,2817,2700);          INSERT INTO isms_profile (fkContext,sName,nId) VALUES (5,'Leitor de Documentos',8805);     INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5,2901,NOW());      INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5,2902,NOW());

/* USUARIO */
INSERT INTO isms_context (pkcontext, ntype, nstate) values (13, 2816, 0);
INSERT INTO isms_user values (13, 1, '%user_fullname%', '%user_login%', '%user_passwordmd5%', '%user_email%', null, '%user_language%', null, 0, 0, 0);
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,13,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,13,2902,NOW());

/***** NOMES DOS PARAMETROS DE RISCO *****/
INSERT INTO rm_parameter_name (pkparametername, sname, nweight) VALUES (1, 'Severidade', 1);

/***** VALORES DOS PARAMETROS DE RISCO *****/
/* ATIVO E RISCO */
INSERT INTO rm_parameter_value_name (pkvaluename, nvalue, simportance, simpact, sriskprobability) VALUES (1, 1.0, 'Baixa',  'Desprezível'  , 'Remota');
INSERT INTO rm_parameter_value_name (pkvaluename, nvalue, simportance, simpact, sriskprobability) VALUES (2, 2.0, 'Média',  'Crítico'       , 'Provável');
INSERT INTO rm_parameter_value_name (pkvaluename, nvalue, simportance, simpact, sriskprobability) VALUES (3, 3.0, 'Alta',   'Catastrófico'    , 'Frequente');

/* RISCO x CONTROLE */
INSERT INTO rm_rc_parameter_value_name (pkrcvaluename, nvalue, srcprobability, scontrolimpact) VALUES (1, 0.0, 'Não reduz',         'Não reduz');
INSERT INTO rm_rc_parameter_value_name (pkrcvaluename, nvalue, srcprobability, scontrolimpact) VALUES (2, 1.0, 'Reduz um nível',    'Reduz um nível');
INSERT INTO rm_rc_parameter_value_name (pkrcvaluename, nvalue, srcprobability, scontrolimpact) VALUES (3, 2.0, 'Reduz dois níveis', 'Reduz dois níveis');

/***** NOMES DOS CUSTOS *****/
INSERT INTO isms_config (pkconfig, svalue) VALUES (201, 'Treinamento');
INSERT INTO isms_config (pkconfig, svalue) VALUES (202, 'Obras');
INSERT INTO isms_config (pkconfig, svalue) VALUES (203, 'Consultoria');
INSERT INTO isms_config (pkconfig, svalue) VALUES (204, 'Aquisições');
INSERT INTO isms_config (pkconfig, svalue) VALUES (205, 'Manutenções');

/***** GERAL *****/
INSERT INTO isms_config (pkconfig, svalue) VALUES (401, '0');
INSERT INTO isms_config (pkconfig, svalue) VALUES (402, '1');
INSERT INTO isms_config (pkconfig, svalue) VALUES (403, 'localhost');
INSERT INTO isms_config (pkconfig, svalue) VALUES (404, '1');
INSERT INTO isms_config (pkconfig, svalue) VALUES (405, '1');
INSERT INTO isms_config (pkconfig, svalue) VALUES (406, '0');
INSERT INTO isms_config (pkconfig, svalue) VALUES (407, '0');
INSERT INTO isms_config (pkconfig, svalue) VALUES (408, '1');
INSERT INTO isms_config (pkconfig, svalue) VALUES (409, '0');
INSERT INTO isms_config (pkconfig, svalue) VALUES (410, '00:00');
INSERT INTO isms_config (pkconfig, svalue) VALUES (411, '1970-01-01 00:00:00');
INSERT INTO isms_config (pkConfig, svalue) VALUES (415,'%default_sender%');
INSERT INTO isms_config (pkconfig, svalue) VALUES (416, '0');
INSERT INTO isms_config (pkconfig, svalue) VALUES (417, '1');
INSERT INTO isms_config (pkconfig, svalue) VALUES (418, '7801');
INSERT INTO isms_config (pkconfig, svalue) VALUES (419, '00:00');
INSERT INTO isms_config (pkconfig, svalue) VALUES (420, date_part('year', now()) || '-' || date_part('month', now()) || '-' || date_part('day', now()) );
INSERT INTO isms_config (pkconfig, svalue) VALUES (421, '1');
INSERT INTO isms_config (pkconfig, svalue) VALUES (426, '1');
INSERT INTO isms_config (pkconfig, svalue) VALUES (427, 'Confidencial');
INSERT INTO isms_config (pkconfig, svalue) VALUES (428, '');
INSERT INTO isms_config (pkconfig, svalue) VALUES (429, '1');
INSERT INTO isms_config (pkconfig, svalue) VALUES (430, '%default_sender_name%');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7404, '0');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7500, '0');

/***** POLITICA DE SENHAS *****/
INSERT INTO isms_config (pkconfig, svalue) VALUES (7201, '5');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7202, '0');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7203, '0');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7204, '0');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7205, '3');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7206, '365');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7207, '5');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7208, '7');

/***** USUARIOS ESPECIAIS *****/
INSERT INTO isms_config (pkconfig, svalue) VALUES (801, '13');
INSERT INTO isms_config (pkconfig, svalue) VALUES (803, '13');
INSERT INTO isms_config (pkconfig, svalue) VALUES (804, '13');
INSERT INTO isms_config (pkconfig, svalue) VALUES (805, '13');
INSERT INTO isms_config (pkconfig, svalue) VALUES (806, '13');
INSERT INTO isms_config (pkconfig, svalue) VALUES (807, '13');
INSERT INTO isms_config (pkconfig, svalue) VALUES (808, '13');
INSERT INTO isms_config (pkconfig, svalue) VALUES (809, '13');

/***** CLASSIFICACAO *****/
/* AREA */
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (1, 2801, 'Produção', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (2, 2801, 'Administrativo', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (3, 2801, 'Auxiliares', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (4, 2801, 'Área de Apoio', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (5, 2801, 'Alta', 5502);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (6, 2801, 'Média', 5502);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (7, 2801, 'Baixa', 5502);

/* PROCESSO */
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (8, 2802, 'Produção e Entrega', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (9, 2802, 'Logística e Transporte', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (10, 2802,'Administrativo', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (11, 2802,'Processos de Apoio', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (12, 2802, 'Alta', 5502);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (13, 2802, 'Média', 5502);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (14, 2802, 'Baixa', 5502);

/* RISCO */
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (15, 2804, 'Água', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (16, 2804, 'Ar', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (17, 2804, 'Solo', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (18, 2804, 'Recursos Naturais', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (19, 2804, 'Degradação Ambiental', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (20, 2804, 'Ruído e Vibrações', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (21, 2804, 'Resíduos Reaproveitáveis', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (22, 2804, 'Residuos Não Aproveitáveis', 5501);

/* EVENTO */
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (23, 2807, 'Interno', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (24, 2807, 'Externo', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (25, 2807, 'Ambos', 5501);

/* CONTROLE */
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (26, 2805, 'Requisitos Legais', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (27, 2805, 'Melhoria Interna', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (28, 2805, 'Outros', 5501);

/* DOCUMENTO */
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (29, 2823, 'Confidential', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (30, 2823, 'Restrito', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (31, 2823, 'Institucional', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (32, 2823, 'Público', 5501);

/* REGISTRO */
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (33, 2826, 'Confidencial', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (34, 2826, 'Restrito', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (35, 2826, 'Institucional', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (36, 2826, 'Público', 5501);

/* CUSTO DO INCIDENTE */
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (37, 2831, 'Multas', 5503);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (38, 2831, 'Recuperação do Ambiente', 5503);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (39, 2831, 'Danos Morais', 5503);

/* RISK_LOW */
INSERT INTO isms_context (pkcontext, ntype, nstate) values (14, 2822, 2702);
INSERT INTO rm_risk_limits (fkcontext, nlow, nhigh) VALUES (14, 3, 6);

/* RISK_VALUE_COUNT */
INSERT INTO isms_config (pkconfig, svalue) VALUES (7403, '3');

/* SAAS Config */
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711701, '%saas_report_email%');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711703, '');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711704, '');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711705, '');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711706, '');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711707, '');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711708, '');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711709, '1');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711710, '');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711712, '');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711713, '%user_email%');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711714, '%config_path%');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711715, '%max_file_size%');

/* ERROR_REPORT_EMAIL */
INSERT INTO isms_config (pkconfig,svalue) VALUES (7209,'%error_report_email%');

/*RISK_FORMULA_TYPE*/
INSERT INTO isms_config (pkConfig, sValue) VALUES (5701, '8301');