DROP TYPE area_id;

DROP TYPE area_ids;

DROP TYPE area_parent_ids;

DROP TYPE area_risk_id;

DROP TYPE best_practices;

DROP TYPE category_id;

DROP TYPE document_id;

DROP TYPE context_id;

DROP TYPE ctrl_date;

DROP TYPE ctrl_eff;

DROP TYPE event_id;

DROP TYPE process_id;

DROP TYPE process_risk_id;

DROP TYPE recalc_risk_update;

DROP TYPE risk_id;

DROP TYPE risk_par_reduction;

DROP TYPE risk_prob;

DROP TYPE risk_value_cont;

DROP TYPE risk_values;

DROP TYPE row_int;

DROP TYPE section_id;

DROP TYPE tree_acl;

DROP TYPE tree_area;

DROP TYPE tree_area_aux;

DROP TYPE tree_best_practice;

DROP TYPE tree_category;

DROP TYPE tree_document;

DROP TYPE asset_id;

DROP TYPE id_value;

DROP TYPE id_values;

DROP TYPE control_cost;

DROP TYPE context_doc_count;

DROP TYPE ctx_names;

DROP TYPE context_rev_count;

DROP TYPE asset_value_cont;

DROP TYPE row_double;

DROP TYPE asset_dependency;

DROP TYPE asset_dependent;