CREATE OR REPLACE FUNCTION get_risks_from_asset(integer)
  RETURNS SETOF risk_id AS
'DECLARE
   piAsset ALIAS FOR $1;
   queryRisks varchar(255);
   riskIds context_id%rowtype;
BEGIN
     queryRisks := ''
          SELECT fkContext FROM rm_risk r
          JOIN isms_context ctx1 ON(ctx1.pkContext = r.fkContext and ctx1.nState <> 2705)
          JOIN isms_context ctx2 ON(ctx2.pkContext = r.fkAsset and ctx2.nState <> 2705 and r.fkAsset = '' || piAsset || '')
     '';

     FOR riskIds IN EXECUTE queryRisks LOOP
        if(riskIds.id IS NOT NULL) THEN
		RETURN NEXT riskIds;
	END IF;
     END LOOP;

     RETURN ;
END'
  LANGUAGE 'plpgsql' VOLATILE;