CREATE OR REPLACE FUNCTION get_risk_value(integer)
  RETURNS double precision AS
'DECLARE
  piRisk ALIAS FOR $1;
  queryCompare varchar(1024);
  queryValues varchar(1024);
  queryProb varchar(256);
  riskCont risk_value_cont%rowtype;
  riskVal2 risk_values%rowtype;
  riskProb row_int%rowtype;
  miResult double precision;
  miRes1 double precision;
  miRes2 double precision;
  miParWeight double precision;
  miBlueRisk integer;
  msFormulaType varchar(32);
BEGIN
  miResult := 0;
  miRes1 := 0;
  miRes2 := 0;
  miBlueRisk := 0; 
  miParWeight :=0;

  queryCompare := ''
    SELECT count_sys, count_risk, count_asset 
      FROM (SELECT count(pkparametername) as count_sys  FROM rm_parameter_name pn) buffer_sys,
           (SELECT count(fkparametername) as count_risk FROM rm_risk_value WHERE fkrisk =  '' || piRisk || '') buffer_risk,
           (SELECT count(fkparametername) as count_asset 
              FROM rm_asset_value av
                JOIN view_rm_risk_active r ON (r.fkasset = av.fkasset AND r.fkcontext =  '' || piRisk || '')
                JOIN isms_context ctx ON(ctx.pkContext = r.fkAsset AND ctx.nState <> 2705)
            ) buffer_asset
   '';
  queryValues := ''
    SELECT pn.sname as parameter_name, pvn.nvalue as risk_value, pvn2.nvalue as asset_value, pn.nweight as parameter_weight 
      FROM view_rm_risk_active r
        JOIN isms_context ctx ON (ctx.pkContext = r.fkAsset AND ctx.nState <> 2705 AND r.fkContext = '' || piRisk || '')
        JOIN rm_risk_value rv ON (rv.fkrisk = r.fkcontext)
        JOIN rm_parameter_value_name pvn ON (rv.fkvaluename = pvn.pkvaluename)
        JOIN rm_parameter_name pn ON (rv.fkparametername = pn.pkparametername)
        JOIN rm_asset_value av ON (pn.pkparametername = av.fkparametername AND av.fkasset = r.fkasset)
        JOIN rm_parameter_value_name pvn2 ON (av.fkvaluename = pvn2.pkvaluename)
  '';
  queryProb :=''
    SELECT pvn.nvalue as risk_prob_value 
      FROM view_rm_risk_active r
      JOIN rm_parameter_value_name pvn ON (r.fkprobabilityvaluename = pvn.pkvaluename)
      WHERE r.fkcontext = '' || piRisk || ''
   '';

  FOR riskCont IN EXECUTE queryCompare LOOP
    IF ( (riskCont.cont_system <> riskCont.cont_risk) OR (riskCont.cont_system <> riskCont.cont_asset) ) THEN
      miBlueRisk := 1;
    END IF;
  END LOOP;

  SELECT INTO msFormulaType sValue FROM isms_config WHERE pkConfig = 5701;
  
  IF (msFormulaType = ''8301'') THEN
	    IF(miBlueRisk <> 1) THEN
	      FOR riskVal2 IN EXECUTE queryValues LOOP
      		miResult := miResult + (riskVal2.asset + riskVal2.risk)* riskVal2.par_weight;
      		miParWeight := miParWeight + riskVal2.par_weight;
	      END LOOP;
	      IF(miParWeight >= 1) THEN
      		FOR riskProb IN EXECUTE queryProb LOOP
      		  miResult := (miResult * riskProb.value) / (2 * miParWeight);
      		END LOOP;
	      ELSE
          miResult := 0;
	      END IF;
	    END IF;
  ELSE
	  IF(miBlueRisk <> 1) THEN
	    FOR riskVal2 IN EXECUTE queryValues LOOP
	      miRes1 := miRes1 + (riskVal2.asset * riskVal2.risk) * riskVal2.par_weight;
	      miRes2 := miRes2 + (riskVal2.asset + riskVal2.risk) * riskVal2.par_weight;	      
	      miParWeight := miParWeight + riskVal2.par_weight;
	    END LOOP;
	    miRes1 := miRes1 / miParWeight;
	    miRes2 := miRes2 / miParWeight / 2;
	    IF(miParWeight >= 1) THEN
	      FOR riskProb IN EXECUTE queryProb LOOP		
          miResult := (miRes1 / miRes2) * riskProb.value;
	      END LOOP;
	    ELSE
	      miResult := 0;
	    END IF;
	  END IF;	
  END IF;

  RETURN miResult;
END'
  LANGUAGE 'plpgsql' VOLATILE;