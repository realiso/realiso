CREATE OR REPLACE FUNCTION get_sub_documents(iddoc integer) RETURNS SETOF document_id AS
'
  DECLARE
    doc document_id;
    subDoc document_id;
  BEGIN
    FOR doc IN SELECT fkContext FROM pm_document WHERE fkParent = idDoc LOOP
      RETURN NEXT doc;
      FOR subDoc IN SELECT * FROM get_sub_documents(doc.document_id) LOOP
        RETURN NEXT subDoc;
      END LOOP;
    END LOOP;
  END
'
LANGUAGE 'plpgsql' VOLATILE;