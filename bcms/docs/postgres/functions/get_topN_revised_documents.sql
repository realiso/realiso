CREATE OR REPLACE FUNCTION get_topn_revised_documents(pitruncatenumber integer)
  RETURNS SETOF context_rev_count AS
'
  DECLARE
    miDocument INTEGER;
    msName VARCHAR(256);
    miCurrentVersion INT;
    miDocCount INTEGER;
    miCounter INTEGER;
    rec context_rev_count%ROWTYPE;
  BEGIN
    miCounter = 0;
    FOR rec IN
      SELECT
        d.fkContext,
        d.sName,
        d.fkCurrentVersion,
        count(di.fkContext) as rev_count
      FROM
        view_pm_document_active d
        JOIN view_pm_doc_instance_active di ON (di.fkDocument = d.fkContext 
                                                AND di.nMajorVersion>1 
                                                AND di.dBeginProduction IS NOT NULL)
      GROUP BY d.fkContext, d.sName, d.fkCurrentVersion
      ORDER BY rev_count DESC, d.sName
    LOOP
      EXIT WHEN piTruncateNumber > 0 AND miCounter = piTruncateNumber;
      RETURN NEXT rec;
      miCounter = miCounter + 1;
    END LOOP;
    RETURN;
  END
'
LANGUAGE 'plpgsql' VOLATILE;