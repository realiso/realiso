CREATE OR REPLACE FUNCTION get_process_value(integer)
  RETURNS double precision AS
'
   SELECT MAX(value) FROM (
      SELECT MAX(a.nValue) as value
	  FROM rm_asset a
	  JOIN isms_context ctxA ON(ctxA.pkContext = a.fkContext and ctxA.nState <> 2705)
	  JOIN rm_process_asset pa ON(pa.fkAsset = a.fkContext)
	  JOIN isms_context ctxPA ON(ctxPA.pkContext = pa.fkContext and ctxPA.nState <> 2705)
	  WHERE pa.fkProcess = $1
      UNION
	  SELECT 0 as value
   ) as buffer
'
  LANGUAGE 'sql' VOLATILE;