CREATE OR REPLACE FUNCTION get_section_parents(integer)
  RETURNS SETOF section_id AS
'DECLARE
   piSection ALIAS FOR $1;
   queryParentSec varchar(255);
   queryEventCat varchar(255);
   ctxSec context_id%rowtype;
   ctxSecRec context_id%rowtype;
BEGIN
     queryParentSec := '' SELECT fkParent FROM rm_section_best_practice sbp
	JOIN isms_context ctx1 ON(ctx1.pkContext = sbp.fkContext and ctx1.nState <> 2705 and sbp.fkContext = '' || piSection || '') '';

     FOR ctxSec IN EXECUTE queryParentSec LOOP
        if(ctxSec.id IS NOT NULL) THEN
		RETURN NEXT ctxSec;
                FOR ctxSecRec IN EXECUTE '' SELECT pkSection FROM get_section_parents( '' || ctxSec.id || '') '' LOOP
                    RETURN NEXT ctxSecRec;
                END LOOP;
	END IF;
     END LOOP;

     RETURN ;
END'
  LANGUAGE 'plpgsql' VOLATILE;