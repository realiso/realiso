CREATE OR REPLACE FUNCTION get_sub_sections(integer)
  RETURNS SETOF section_id AS
'DECLARE
   piSection ALIAS FOR $1;
   querySubSec varchar(255);
   queryEventSec varchar(255);
   ctxSec context_id%rowtype;
   ctxSecRec context_id%rowtype;
BEGIN
     querySubSec := '' SELECT fkContext FROM rm_section_best_practice sec
          WHERE sec.fkParent = '' || piSection;

     FOR ctxSec IN EXECUTE querySubSec LOOP
        IF(ctxSec.id IS NOT NULL) THEN
            RETURN NEXT ctxSec;
            FOR ctxSecRec IN EXECUTE '' SELECT pkSection FROM get_sub_sections( '' || ctxSec.id || '') '' LOOP
                RETURN NEXT ctxSecRec;
            END LOOP;
        END IF;
     END LOOP;

  RETURN ;
END'
  LANGUAGE 'plpgsql' VOLATILE;