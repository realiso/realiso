CREATE OR REPLACE FUNCTION get_document_tree(integer, integer, integer)
RETURNS SETOF tree_document AS
'DECLARE
piParent ALIAS FOR $1;
pnLevel ALIAS FOR $2;
pbPublished ALIAS FOR $3;
nAuxLevel integer;
queryDoc varchar(255);
queryDocAux varchar(255);
queryDocRecursion varchar(255);
documentRoot tree_document%rowtype;
subDoc tree_document%rowtype;

BEGIN
  nAuxLevel := pnLevel + 1;
  queryDocAux := '' SELECT fkContext, sName, '' || nAuxLevel::text || '' as nLevel FROM view_pm_document_active WHERE fkParent '';
  IF piParent > 0 THEN
    queryDoc := queryDocAux || '' = '' || piParent;
  ELSE 
    queryDoc := queryDocAux || '' is NULL'';
  END IF;
  IF (pbPublished=1) THEN
    FOR documentRoot IN EXECUTE queryDoc LOOP
      IF EXISTS (SELECT fkContext FROM view_pm_published_docs WHERE fkContext = documentRoot.fkContext) THEN
        RETURN NEXT documentRoot;
        queryDocRecursion := ''SELECT fkContext, sName, nLevel FROM get_document_tree('' || documentRoot.fkContext::text || '' , '' || nAuxLevel::text || '' , '' || pbPublished::text || '') '';
        FOR subDoc IN EXECUTE queryDocRecursion LOOP
          RETURN NEXT subDoc;
        END LOOP;
      END IF;
    END LOOP;
  ELSE
    FOR documentRoot IN EXECUTE queryDoc LOOP
      RETURN NEXT documentRoot;
      queryDocRecursion := ''SELECT fkContext, sName, nLevel FROM get_document_tree('' || documentRoot.fkContext::text || '' , '' || nAuxLevel::text || '' , '' || pbPublished::text || '' ) '';
      FOR subDoc IN EXECUTE queryDocRecursion LOOP
        RETURN NEXT subDoc;
      END LOOP;
    END LOOP;
  END IF;
  RETURN ;
END'
LANGUAGE 'plpgsql' VOLATILE;
