CREATE OR REPLACE FUNCTION get_all_asset_and_dependents()
  RETURNS SETOF asset_dependent AS
'DECLARE
asset_id context_id%rowtype;
asset_dep_id context_id%rowtype;
asset_assoc asset_dependent%rowtype;
BEGIN
   FOR asset_id IN EXECUTE ''SELECT fkContext FROM view_rm_asset_active'' LOOP
	    FOR asset_dep_id IN EXECUTE ''SELECT pkAsset FROM get_asset_dependents(''|| asset_id.id ||'')'' LOOP
		asset_assoc.asset = asset_id.id;
		asset_assoc.dependent = asset_dep_id.id;
		RETURN NEXT asset_assoc;
	    END LOOP;
   END LOOP;
   RETURN;
END'
  LANGUAGE 'plpgsql' VOLATILE;