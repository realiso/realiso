CREATE OR REPLACE FUNCTION get_all_asset_and_dependencies()
  RETURNS SETOF asset_dependency AS
'DECLARE 
asset_id context_id%rowtype;
asset_dep_id context_id%rowtype;
asset_assoc asset_dependency%rowtype;
BEGIN
   FOR asset_id IN EXECUTE ''SELECT fkContext FROM view_rm_asset_active'' LOOP
	    FOR asset_dep_id IN EXECUTE ''SELECT pkAsset FROM get_asset_dependencies(''|| asset_id.id ||'')'' LOOP
		asset_assoc.asset = asset_id.id;
		asset_assoc.dependency = asset_dep_id.id;
		RETURN NEXT asset_assoc;
	    END LOOP;	
   END LOOP;
   RETURN;	
END'
  LANGUAGE 'plpgsql' VOLATILE;