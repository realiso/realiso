CREATE OR REPLACE FUNCTION get_sub_categories(integer)
  RETURNS SETOF category_id AS
'DECLARE
   piCategory ALIAS FOR $1;
   querySubCat varchar(255);
   queryEventCat varchar(255);
   ctxCat context_id%rowtype;
   ctxCatRec context_id%rowtype;
BEGIN
    querySubCat := '' SELECT fkContext FROM rm_category cat
        WHERE cat.fkParent = '' || piCategory;

    FOR ctxCat IN EXECUTE querySubCat LOOP
        if(ctxCat.id IS NOT NULL) THEN
            RETURN NEXT ctxCat;
            FOR ctxCatRec IN EXECUTE '' SELECT pkCategory FROM get_sub_Categories( '' || ctxCat.id || '') '' LOOP
                RETURN NEXT ctxCatRec;
            END LOOP;
        END IF;
    END LOOP;
    RETURN ;
END'
  LANGUAGE 'plpgsql' VOLATILE;