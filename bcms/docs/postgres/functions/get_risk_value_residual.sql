CREATE OR REPLACE FUNCTION get_risk_value_residual(integer)
  RETURNS double precision AS
'DECLARE
  piRisk ALIAS FOR $1;
  queryCompare varchar(1024);
  queryValues varchar(1024);
  queryProb varchar(1024);
  miResult double precision;
  miRes1 double precision;
  miRes2 double precision;
  miProbResult double precision;
  miParWeight double precision;
  miBlueRisk integer;
  riskCont risk_value_cont%rowtype;
  riskProb risk_prob%rowtype;
  riskValue risk_values%rowtype;
  msFormulaType varchar(32);
BEGIN
  miParWeight:=0;
  miBlueRisk := 0;
  miResult := 0;
  miRes1 := 0;
  miRes2 := 0;
  miProbResult := 0;
  queryCompare := ''
    SELECT count_sys, count_risk, count_asset
      FROM (SELECT count(pkparametername) as count_sys  FROM rm_parameter_name pn) buffer_sys,
           (SELECT count(fkparametername) as count_risk FROM rm_risk_value WHERE fkrisk = '' || piRisk || '') buffer_risk,
           (SELECT count(fkparametername) as count_asset FROM rm_asset_value av
              JOIN view_rm_risk_active r ON (r.fkasset = av.fkasset AND r.fkContext = '' || piRisk || '')
              JOIN view_rm_asset_active a ON (a.fkContext = av.fkAsset)
            ) buffer_asset
       '';
  queryValues :=''
    SELECT pn.sname as parameter_name,  pn.pkparametername, pvn.nvalue as asset_value, pn.nweight as parameter_weight 
      FROM view_rm_risk_active r
        JOIN isms_context ctx ON(ctx.pkContext = r.fkAsset AND r.fkContext = '' || piRisk || '')
        JOIN rm_asset_value av ON (av.fkasset = r.fkasset)
        JOIN rm_parameter_name pn ON (av.fkparametername = pn.pkparametername)
        JOIN rm_parameter_value_name pvn ON (av.fkvaluename = pvn.pkvaluename)
       '';
  queryProb := ''
    SELECT prob_reduction_value, risk_prob_value FROM 
    (
      SELECT sum(rcpvn.nvalue) as prob_reduction_value
        FROM rm_risk_control rc
          JOIN isms_context ctx ON(ctx.pkContext = rc.fkRisk and ctx.nState <> 2705 AND rc.fkrisk = '' || piRisk || '')
          JOIN view_rm_control_active c ON (c.fkContext = rc.fkControl AND c.bIsActive = 1)
          JOIN rm_rc_parameter_value_name rcpvn ON (rc.fkprobabilityvaluename = rcpvn.pkrcvaluename)
      ) prv,
      (
      SELECT pvn.nvalue as risk_prob_value 
        FROM view_rm_risk_active r
          JOIN rm_parameter_value_name pvn ON (r.fkprobabilityvaluename = pvn.pkvaluename AND r.fkContext = '' || piRisk || '')
      ) rpv
  '';
  FOR riskCont IN EXECUTE queryCompare LOOP
    IF((riskCont.cont_system <> riskCont.cont_risk) or (riskCont.cont_system <> riskCont.cont_asset)) THEN
      miBlueRisk:=1;
    END IF;
  END LOOP;
  IF(miBlueRisk <> 1) THEN
    FOR riskProb IN EXECUTE queryProb LOOP
      IF(riskProb.reduct IS NULL) THEN
         miProbResult := riskProb.value;
      ELSE
        miProbResult := riskProb.value - riskProb.reduct;
        IF(miProbResult < 1) THEN
          miProbResult := 1;
        END IF;
      END IF;
    END LOOP;

    SELECT INTO msFormulaType sValue FROM isms_config WHERE pkConfig = 5701;

    IF (msFormulaType = ''8301'') THEN
	    FOR riskValue IN EXECUTE queryValues LOOP
	      miParWeight := miParWeight + riskValue.par_weight;
	      miResult := miResult + (riskValue.asset + get_risk_parameter_reduction(piRisk,riskValue.risk) ) * riskValue.par_weight;
	    END LOOP;
	    IF(miParWeight >= 1) THEN
	      miResult := (miResult * miProbResult) / (2 * miParWeight);
	    ELSE
	      miResult := 0;
	    END IF;
     ELSE
      FOR riskValue IN EXECUTE queryValues LOOP
	      miRes1 := miRes1 + (riskValue.asset * get_risk_parameter_reduction(piRisk,riskValue.risk)) * riskValue.par_weight;
	      miRes2 := miRes2 + (riskValue.asset + get_risk_parameter_reduction(piRisk,riskValue.risk)) * riskValue.par_weight;	      
	      miParWeight := miParWeight + riskValue.par_weight;
	    END LOOP;
	    miRes1 := miRes1 / miParWeight;
	    miRes2 := miRes2 / miParWeight / 2;
	    IF(miParWeight >= 1) THEN	      
	      miResult := (miRes1 / miRes2) * miProbResult;	      
	    ELSE
	      miResult := 0;
	    END IF;
     END IF;
  END IF;
  RETURN miResult;
END'
  LANGUAGE 'plpgsql' VOLATILE;