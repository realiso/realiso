CREATE OR REPLACE FUNCTION get_sub_areas(integer)
  RETURNS SETOF area_id AS
'DECLARE
   piArea ALIAS FOR $1;
   querySubArea varchar(255);
   queryEventCat varchar(255);
   ctxArea context_id%rowtype;
   ctxAreaRec context_id%rowtype;
BEGIN
     querySubArea := '' SELECT fkContext FROM rm_area a
          WHERE fkParent = '' || piArea;

     FOR ctxArea IN EXECUTE querySubArea LOOP
        if(ctxArea.id IS NOT NULL) THEN
		RETURN NEXT ctxArea;
                FOR ctxAreaRec IN EXECUTE '' SELECT pkArea FROM get_sub_areas( '' || ctxArea.id || '') '' LOOP
                    RETURN NEXT ctxAreaRec;
                END LOOP;
	END IF;
     END LOOP;

     RETURN ;
END'
  LANGUAGE 'plpgsql' VOLATILE;