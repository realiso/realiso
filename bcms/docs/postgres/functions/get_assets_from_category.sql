CREATE OR REPLACE FUNCTION get_assets_from_category(integer)
  RETURNS SETOF asset_id AS
'DECLARE
piCategory ALIAS FOR $1;
queryCat varchar(255);
queryAsset varchar(255);
assetCat context_id%rowtype;
category context_id%rowtype;
assetSubCat context_id%rowtype;
BEGIN
     queryCat := '' SELECT fkContext FROM rm_category cat
		JOIN isms_context c ON(c.pkContext = cat.fkContext and c.nState <> 2705)	
		WHERE  fkParent = '' || piCategory;

     queryAsset := '' SELECT fkContext FROM rm_asset a
		JOIN isms_context c ON(c.pkContext = a.fkContext and c.nState <> 2705)	
		WHERE fkCategory = '' || piCategory;

     FOR assetCat IN EXECUTE queryAsset LOOP
        if(assetCat.id IS NOT NULL) THEN
		RETURN NEXT assetCat;
	END IF;
     END LOOP;

     FOR category IN EXECUTE queryCat LOOP
        if(category.id IS NOT NULL) THEN
	        FOR assetSubCat IN EXECUTE '' SELECT pkAsset FROM get_assets_from_category('' || category.id || '') '' LOOP
		        RETURN NEXT assetSubCat;
	        END LOOP;
	    END IF;
     END LOOP;

     RETURN ;
END'
  LANGUAGE 'plpgsql' VOLATILE;