CREATE OR REPLACE FUNCTION get_category_parents(integer)
  RETURNS SETOF category_id AS
'DECLARE
piCategory ALIAS FOR $1;
queryCat varchar(255);
queryCatRecursion varchar(255);
category context_id%rowtype;
catParent context_id%rowtype;
BEGIN
     queryCat := '' SELECT cat.fkParent FROM rm_category cat
		JOIN isms_context c ON(c.pkContext = cat.fkContext and c.nState <> 2705)	
		WHERE  cat.fkContext = '' || piCategory;

     FOR category IN EXECUTE queryCat LOOP
	if(category.id IS NOT NULL) THEN
	    RETURN NEXT category;
	    FOR catParent IN EXECUTE ''SELECT pkCategory FROM get_category_parents(''|| category.id ||'')'' LOOP
	      RETURN NEXT catParent;
	    END LOOP;
	END IF;
     END LOOP;

     RETURN ;
END'
  LANGUAGE 'plpgsql' VOLATILE;