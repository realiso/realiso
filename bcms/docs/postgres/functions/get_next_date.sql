CREATE OR REPLACE FUNCTION get_next_date(pdLastTime TIMESTAMP, piPeriodUnitId INT, piPeriodsCount INT) RETURNS TIMESTAMP AS
'DECLARE
  miYear  INT;
  miMonth INT;
  miDay   INT;
  miHour  INT;
  miMin   INT;
  miSec   INT;
  miDaysInMonth INT;
BEGIN
  IF piPeriodUnitId = 7801 THEN
    -- SCHEDULE_BYDAY
    RETURN pdLastTime::DATE + piPeriodsCount;
  ELSEIF piPeriodUnitId = 7802 THEN
    -- SCHEDULE_BYWEEK
    RETURN pdLastTime::DATE + piPeriodsCount * 7;
  ELSEIF piPeriodUnitId = 7803 THEN
    -- SCHEDULE_BYMONTH
    SELECT
      extract(year   from pdLastTime),
      extract(month  from pdLastTime),
      extract(day    from pdLastTime),
      extract(hour   from pdLastTime),
      extract(minute from pdLastTime),
      extract(second from pdLastTime)
    INTO
      miYear ,
      miMonth,
      miDay  ,
      miHour ,
      miMin  ,
      miSec;
    
    miMonth:= miMonth + piPeriodsCount;
    IF miMonth > 12 THEN
      miYear:= miYear + miMonth/12;
      miMonth:= miMonth % 12;
      IF miMonth = 0 THEN miMonth = 12; END IF;
    END IF;
    IF miDay > 28 THEN
      IF miMonth = 2 THEN
        IF (miYear%4=0 AND miYear%100!=0) OR miYear%400=0 THEN
          miDaysInMonth:= 29;
        ELSE
          miDaysInMonth:= 28;
        END IF;
      ELSEIF miMonth IN (4,6,9,11) THEN
        miDaysInMonth:= 30;
      ELSE
        miDaysInMonth:= 31;
      END IF;
      IF miDay > miDaysInMonth THEN miDay:= miDaysInMonth; END IF;
    END IF;
    RETURN (miYear || ''-'' || miMonth || ''-'' || miDay || '' '' || miHour || '':'' || miMin || '':'' || miSec)::TIMESTAMP;
  ELSE
    RAISE EXCEPTION ''Invalid period unit identifier ''''%''''.'', piPeriodUnitId;
  END IF;
END'
LANGUAGE 'plpgsql' VOLATILE;