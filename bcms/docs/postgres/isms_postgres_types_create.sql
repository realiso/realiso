CREATE TYPE area_id AS
   (pkarea integer);

CREATE TYPE area_ids AS
   (area_id integer);

CREATE TYPE area_parent_ids AS
   (parent_id integer);

CREATE TYPE area_risk_id AS
   (area_id integer,
    risk_id integer);

CREATE TYPE best_practices AS
   (pkbestpractice integer);

CREATE TYPE category_id AS
   (pkcategory integer);

CREATE TYPE document_id AS
  (document_id integer);

CREATE TYPE context_id AS
   (id integer);

CREATE TYPE ctrl_date AS
   (ddeadline timestamp without time zone,
    dimpl timestamp without time zone);

CREATE TYPE ctrl_eff AS
   (nre integer,
    nee integer);

CREATE TYPE event_id AS
   (pkevent integer);

CREATE TYPE process_id AS
   (pkprocess integer);

CREATE TYPE process_risk_id AS
   (process_id integer,
    risk_id integer);

CREATE TYPE recalc_risk_update AS
   ("miVRchg" double precision,
    "miAMchg" integer,
    "miRisk" integer,
    "miAsset" integer);

CREATE TYPE risk_id AS
   (pkrisk integer);

CREATE TYPE risk_par_reduction AS
   (par_red_value integer,
    risk_par_value integer);

CREATE TYPE risk_prob AS
   (reduct integer,
    value integer);

CREATE TYPE risk_value_cont AS
   (cont_system integer,
    cont_risk integer,
    cont_asset integer);

CREATE TYPE risk_values AS
   (par_name character varying(255),
    risk integer,
    asset integer,
    par_weight integer);

CREATE TYPE row_int AS
   (value integer);

CREATE TYPE section_id AS
   (pksection integer);

CREATE TYPE tree_acl AS
   (pkacl integer,
    nlevel integer,
    sname character varying(256),
    stag character varying(256),
    sdescription character varying(256));

CREATE TYPE tree_area AS
   (pkid numeric,
    fkcontext integer,
    sname character varying(255),
    nlevel integer);

CREATE TYPE tree_area_aux AS
   (fkcontext integer,
    sname character varying(255),
    nlevel integer);

CREATE TYPE tree_best_practice AS
   (fkcontext integer,
    sname character varying(255),
    nlevel integer);

CREATE TYPE tree_category AS
   (fkcontext integer,
    sname character varying(255),
    nlevel integer);

CREATE TYPE tree_document AS
   (fkcontext integer,
    sname character varying(255),
    nlevel integer);

CREATE TYPE asset_id AS
   (pkAsset integer);
   
CREATE TYPE id_value AS
   (id integer, value float);
   
CREATE TYPE id_values AS
   (id integer, value1 float, value2 float);

CREATE TYPE control_cost AS
   (cost1 float, cost2 float, cost3 float, cost4 float, cost5 float);

CREATE TYPE context_doc_count AS
   (fkcontext integer,
    sname character varying(256),
    nvalue double precision,
    doc_count integer);
    
CREATE TYPE ctx_names AS
   (str character varying(255));

CREATE TYPE context_rev_count AS
   (fkcontext integer,
    sname character varying(256),
    fkcurrentversion integer,
    rev_count integer);

CREATE TYPE asset_value_cont AS
   (cont_system integer,
    cont_asset integer);

CREATE TYPE row_double AS
   (value double precision);

CREATE TYPE asset_dependency AS
   (asset integer, dependency integer);

CREATE TYPE asset_dependent AS
   (asset integer, dependent integer);