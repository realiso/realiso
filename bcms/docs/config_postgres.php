<?php
$laConfig = array (
  '%user_fullname%' => 'ISMS User',
  '%user_login%' => 'isms',
  '%user_passwordmd5%' => md5('default'),
  '%user_email%' => 'noemail@axur.com.br',
  '%user_language%' => '3302',
  '%email_enabled%' => '0',
  '%smtp_server%' => 'localhost',
  '%allow_digests%' => '0',
  '%digest_time%' => '00:00:00',
  '%general_smtp_auth_required%' => '0',
  '%general_smtp_auth_username%' => '',
  '%general_smtp_auth_password%' => '',
  '%default_sender%' => 'noreply@isms.com.br',
  '%data_collection_periodicy_value%' => '1',
  '%data_collection_periodicy_type%' => '7801',
  '%data_collection_time%' => '00:00',
  '%data_collection_enabled%' => '1',
  '%data_collection_db_database%' => 'postgres',
  '%data_collection_db_login%' => 'postgres',
  '%data_collection_db_pass%' => 'postgres',
  '%data_collection_db_host%' => '192.168.0.181',  
  '%risk_value_count%' => '5',
  '%user_email%' => 'rmunari@axur.net',
  '%config_path%' => ''
);
?>