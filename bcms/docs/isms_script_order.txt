
 1) isms_<banco>_sql_create.sql
 2) isms_<banco>_views_create.sql
 3) isms_postgres_types_create.sql (s� no postgres)
 4) isms_postgres_language_create.sql (s� no postgres)
 5) isms_<banco>_functions_create.sql
 6) isms_<banco>_stored_procedures_create.sql
 7) isms_<banco>_triggers_create.sql
 8) isms_<banco>_install_populate_config.sql
 9) isms_<banco>_populate_05_rm_category.sql
10) isms_<banco>_populate_06_rm_event.sql
11) isms_<banco>_populate_07_rm_section_best_practice.sql
12) isms_<banco>_populate_08_rm_best_practice.sql
13) isms_<banco>_populate_09_rm_standard.sql
14) isms_<banco>_populate_10_rm_best_practice_standard.sql
15) isms_<banco>_populate_11_rm_best_practice_event.sql
16) isms_postgres_populate_14_alter_table_isms_context.sql (s� no postgres)
17) isms_<banco>_acl.sql
18) isms_<banco>_profile_acl.sql
________________________________________________________________________________________

POSTGRES
---------
Somente na base de dados 'postgres' (scripts de cria��o dos schedules):
postgres_pgagent.sql
isms_postgres_jobs_create.sql

SQLServer
----------
isms_sqlserver_jobs_create.sql
