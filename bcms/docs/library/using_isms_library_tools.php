<?
$base_ref =  "../../../FWD/src/";
include $base_ref . "base/setup.php";  
$soWeblib = FWDWebLib::getInstance();

include_once('LibraryTools.php');
include_once('ISMSLibraryTools.php');
include_once('EMSLibraryTools.php');
include_once('PCILibraryTools.php');
include_once('SOXLibraryTools.php');

// cria o sql das bibliotecas do ISMS
$foLib = new ISMSLibraryTools();
$foLib->createSQLFromLibraryArray('en','postgres',true);
//$foLib->createSQLFromLibraryArray('br','postgres',true);  // n�o � utilizada "oficialmente"

// cria o sql das bibliotecas do SOX
$foLib = new SOXLibraryTools();
$foLib->createSQLFromLibraryArray('en_sox','postgres',true);

// cria o sql das bibliotecas do PCI
$foLib = new PCILibraryTools();
$foLib->createSQLFromLibraryArray('en_pci','postgres',true);

// cria o sql das bibliotecas do EMS
$foLib = new EMSLibraryTools();
$foLib->createSQLFromLibraryArray('br_ems','postgres',true);

// cria o sql das bibliotecas do OHS 
// OHS ainda n�o possui bibliotecas!!!

// cria os arrays com os elementos da biblioteca a partir de um banco de dados populado
// utilizado quando as bibliotecas s�o criadas no sistema e � preciso deixar os dados como default
//$foLib->createArrayLibraryFromBD('postgres','inst_fabiosox','saas','saas','192.168.0.11','en_sox');
?>