<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

  
  include_once('arrays/isms_05_rm_category_en.php');
  include_once('arrays/isms_06_rm_event_en.php');
  include_once('arrays/isms_07_rm_section_best_practice_en.php');
  include_once('arrays/isms_08_rm_best_practice_en.php');
  include_once('arrays/isms_09_rm_standard_en.php');
  include_once('arrays/isms_10_rm_best_practice_standard_en.php');
  include_once('arrays/isms_11_rm_best_practice_event_en.php');
  include_once('arrays/isms_05_rm_category_br.php');
  include_once('arrays/isms_06_rm_event_br.php');
  include_once('arrays/isms_07_rm_section_best_practice_br.php');
  include_once('arrays/isms_08_rm_best_practice_br.php');
  include_once('arrays/isms_09_rm_standard_br.php');
  include_once('arrays/isms_10_rm_best_practice_standard_br.php');
  include_once('arrays/isms_11_rm_best_practice_event_br.php');


/**
 * Classe LibraryTools Implementa metodos de manipulação dos dados da biblioteca do ISMS.
 *
 * <p>Classe LibraryTools Implementa metodos de manipulação dos dados da biblioteca do ISMS.</p>
 * @package ISMS
 * @subpackage classes
 */
class ISMSLibraryTools extends LibraryTools {
  public function __construct(){
    $this->caLibraryStructure = array(
      2806=>array('name'=>'laCategory'             ,'table'=>'rm_category'               ,'file_name'=>'isms_postgres_populate_05_rm_category_'               ,'alias'=>array('fkContext'=>DB_NUMBER ,'fkParent'              =>DB_NUMBER ,'sName'        =>DB_STRING, 'tDescription' => DB_STRING) ),
      2807=>array('name'=>'laEvent'                ,'table'=>'rm_event'                  ,'file_name'=>'isms_postgres_populate_06_rm_event_'                  ,'alias'=>array('fkContext'=>DB_NUMBER ,'fkCategory'            =>DB_NUMBER ,'sDescription' =>DB_STRING ) ),
      2811=>array('name'=>'laSectionBestPractice'  ,'table'=>'rm_section_best_practice'  ,'file_name'=>'isms_postgres_populate_07_rm_section_best_practice_'  ,'alias'=>array('fkContext'=>DB_NUMBER ,'fkParent'              =>DB_NUMBER ,'sName'        =>DB_STRING ) ),
      2808=>array('name'=>'laBestPractice'         ,'table'=>'rm_best_practice'          ,'file_name'=>'isms_postgres_populate_08_rm_best_practice_'          ,'alias'=>array('fkContext'=>DB_NUMBER ,'fkSectionBestPractice' =>DB_NUMBER ,'nControlType' =>DB_NUMBER ,'sName'=>DB_STRING ,'tDescription'=>DB_STRING ,'tImplementationGuide'=>DB_STRING ) ),
      2809=>array('name'=>'laStandard'             ,'table'=>'rm_standard'               ,'file_name'=>'isms_postgres_populate_09_rm_standard_'               ,'alias'=>array('fkContext'=>DB_NUMBER ,'sName'                 =>DB_STRING ,'tDescription' =>DB_STRING ,'tApplication' => DB_STRING, 'tObjective' => DB_STRING) ),
      2814=>array('name'=>'laBestPracticeStandard' ,'table'=>'rm_best_practice_standard' ,'file_name'=>'isms_postgres_populate_10_rm_best_practice_standard_' ,'alias'=>array('fkContext'=>DB_NUMBER ,'fkContext'             =>DB_NUMBER ,'fkStandard'   =>DB_NUMBER ,'fkBestPractice'=>DB_NUMBER) ),
      2812=>array('name'=>'laBestPracticeEvent'    ,'table'=>'rm_best_practice_event'    ,'file_name'=>'isms_postgres_populate_11_rm_best_practice_event_'    ,'alias'=>array('fkContext'=>DB_NUMBER ,'fkBestPractice'        =>DB_NUMBER , 'fkEvent'     =>DB_NUMBER ) ),
    );
    
    $this->caLangs = array('br','en');
    
    $this->csSqlPathToCreateArray = 'sql_temp';
  }
};
?>