<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */ 
  
  include_once('arrays/isms_05_rm_category_br_ems.php');
  include_once('arrays/isms_06_rm_event_br_ems.php');

/**
 * Classe LibraryTools Implementa metodos de manipulação dos dados da biblioteca do EMS.
 *
 * <p>Classe LibraryTools Implementa metodos de manipulação dos dados da biblioteca do EMS.</p>
 * @package ISMS
 * @subpackage classes
 */
class EMSLibraryTools extends LibraryTools {
  public function __construct(){
    $this->caLibraryStructure = array(
      2806=>array('name'=>'laCategory'             ,'table'=>'rm_category'               ,'file_name'=>'isms_postgres_populate_05_rm_category_'               ,'alias'=>array('fkContext'=>DB_NUMBER ,'fkParent'              =>DB_NUMBER ,'sName'        =>DB_STRING, 'tDescription' => DB_STRING) ),
      2807=>array('name'=>'laEvent'                ,'table'=>'rm_event'                  ,'file_name'=>'isms_postgres_populate_06_rm_event_'                  ,'alias'=>array('fkContext'=>DB_NUMBER ,'fkCategory'            =>DB_NUMBER ,'sDescription' =>DB_STRING, 'tImpact' => DB_STRING ) ),
    );
    
    $this->caLangs = array('br_ems');
    
    $this->csSqlPathToCreateArray = 'sql_temp';
  }
};
?>