<?php
			$laBestPracticeDE_SOX = array (0 => array(
	'fkContext' => '10364',
	'fkSectionBestPractice' => '10363',
	'nControlType' => '0',
	'sName' => 'PO1.1 IT Value Management (Management des Wertbeitrags der IT)',
	'tDescription' => 'Arbeite mit dem Kerngesch�ft zusammen, um sicherzustellen, dass unternehmensweite Portfolio von IT-unterst�tzten
Investitionen Programme enthalten, welche stichhaltige Business-Cases aufweisen. Erkenne, dass es notwendige, laufende und
dem Ermessen �berlassene Investitionen gibt, die sich bei der Zuteilung von Finanzmitteln in Komplexit�t und
Entscheidungsspielraum unterscheiden. IT-Prozesse sollten eine effektive und effiziente Bereitstellung der IT-Komponenten f�r
Programme und ein Fr�hwarnsystem bieten f�r alle Planabweichungen (inklusive Kosten, Terminplan oder Funktionalit�t),
welche die im Programm geplanten Ergebnisse beeintr�chtigen k�nnen. IT-Services sollten entsprechend vern�nftiger und
durchsetzbarer Service Level Agreements erbracht werden. Verantwortlichkeiten f�r die Erreichung des Wertbeitrags und f�r
Kostenkontrolle sind klar festgelegt und werden �berwacht. F�hre eine angemessene, transparente, wiederholbare und
vergleichbare Beurteilung von Busines-Cases durch, welche eine Aussage zur finanziellen Rechtfertigung, zum Risiko einer
Nichterbringung eines Potentials und zum Risiko einer Nichtaussch�pfung von erwartetem Nutzen zum Inhalt hat.',
	'tImplementationGuide' => '',
),
1 => array(
	'fkContext' => '10365',
	'fkSectionBestPractice' => '10363',
	'nControlType' => '0',
	'sName' => 'PO1.2 Business-IT Alignment (Ausrichtung Kerngesch�ft und IT)',
	'tDescription' => 'Unterrichte die Gesch�ftsf�hrung �ber aktuelle technologische M�glichkeiten und k�nftige Richtungen, �ber die M�glichkeiten,
welche die IT bietet sowie �ber die durch das Unternehmen zu ergreifenden Ma�nahmen, um diese M�glichkeiten nutzen zu
k�nnen. Stelle sicher, dass das Gesch�ft, an dem die IT ausgerichtet ist, verstanden wird. Die Gesch�fts- und IT-Strategie sollten
integriert und allgemein kommuniziert werden; es sollte eine klare Verbindung zwischen Unternehmenszielen, IT-Zielen,
erkannten M�glichkeiten und Grenzen des Potentials geben. Identifiziere, in welchen Bereichen die Gesch�ftsstrategie von der IT
kritisch abh�ngt und vermittle zwischen den Erfordernissen des Kerngesch�fts und der Technologie, damit vereinbarte Priorit�ten
festgehalten werden k�nnen.',
	'tImplementationGuide' => '',
),
2 => array(
	'fkContext' => '10366',
	'fkSectionBestPractice' => '10363',
	'nControlType' => '0',
	'sName' => 'PO1.3 Assessment of Current Performance (Bewertung der gegenw�rtigen Performance)',
	'tDescription' => 'Bewerte die Performance der bestehenden Pl�ne und Informationssysteme auf deren Beitrag zu Gesch�ftszielen, Funktionalit�t,
Stabilit�t, Komplexit�t, Kosten, St�rken und Schw�chen.',
	'tImplementationGuide' => '',
),
3 => array(
	'fkContext' => '10367',
	'fkSectionBestPractice' => '10363',
	'nControlType' => '0',
	'sName' => 'PO1.4 IT Strategic Plan (Strategischer IT-Plan)',
	'tDescription' => 'Erstelle in Zusammenarbeit mit den relevanten Stakeholdern einen strategischen IT-Plan, welcher festlegt, inwieweit die IT zu
den strategischen Zielen des Unternehmens beitr�gt und der die damit verbundenen Kosten und Risiken aufzeigt. Der Plan
bestimmt, inwieweit die IT die durch IT erm�glichten Investitionsvorhaben und die operative Leistungserbringung unterst�tzt. Er
definiert, wie die Ziele erreicht und gemessen werden und wie diese durch die Stakeholder formell freigegeben werden. Der
strategische IT-Plan sollte das Investitions- und operative Budget, Finanzierungsquellen, die Sourcing-Strategie, die
Beschaffungsstrategie, sowie rechtliche und regulatorische Anforderungen abdecken. Der strategische IT-Plan sollte detailliert
genug gehalten sein, um die Definition von taktischen IT-Pl�nen zu erm�glichen.',
	'tImplementationGuide' => '',
),
4 => array(
	'fkContext' => '10368',
	'fkSectionBestPractice' => '10363',
	'nControlType' => '0',
	'sName' => 'PO1.5 IT Tactical Plans (Taktische IT-Pl�ne)',
	'tDescription' => 'Erstelle ein Portfolio von taktischen IT-Pl�nen, welche vom strategischen IT-Plan abgeleitet wurden. Diese taktischen Pl�ne
beschreiben notwendige IT-Vorhaben, Anforderungen an Ressourcen und wie die Verwendung von Ressourcen und die
Generierung von Nutzen �berwacht und gemanaged werden. Die taktischen Pl�ne sollten gen�gend detailliert gehalten sein, um
die Festlegung von Projektpl�nen zu erm�glichen. Manage die taktischen Pl�ne und Initiativen aktiv durch die Analyse von
Projekt- und Service-Portfolios. Dies umfasst die regelm��ige Abstimmung von Anforderungen und Ressourcen, den Abgleich
derselben mit strategischen und taktischen Zielen und erwartetem Nutzen und das Ergreifen geeigneter Ma�nahmen bei
Abweichungen.',
	'tImplementationGuide' => '',
),
5 => array(
	'fkContext' => '10369',
	'fkSectionBestPractice' => '10363',
	'nControlType' => '0',
	'sName' => 'PO1.6 IT Portfolio Management (IT-Portfoliomanagement)',
	'tDescription' => 'Manage das Portfolio an IT-unterst�tzten Investitionsvorhaben, die f�r die Erreichung der strategischen Unternehmensziele
erforderlich sind, aktiv und in Abstimmung mit dem Kerngesch�ft, in dem die Programme identifiziert, definiert, evaluiert,
priorisiert, ausgew�hlt, initiiert, gemanaged und gesteuert werden. Dies umfasst auch die Abkl�rung der erw�nschten
Gesch�ftsergebnisse, die Sicherstellung, dass Programmziele die Erzielung der Ergebnisse unterst�tzten, das Verstehen des
Gesamtaufwands, um die Ergebnisse zu erreichen, die Zuweisung klarer Verantwortlichkeiten mit unterst�tzenden Ma�nahmen,
die Definition von Projekten innerhalb des Programms, die Bereitstellung von Ressourcen und Finanzmitteln, die �bertragung
von Autorit�t und die Beauftragung von erforderlichen Projekten zu Beginn des Programms.',
	'tImplementationGuide' => '',
),
6 => array(
	'fkContext' => '10371',
	'fkSectionBestPractice' => '10370',
	'nControlType' => '0',
	'sName' => 'PO2.1 Information Architecture Model (Informationsarchitekturmodell)',
	'tDescription' => 'Errichte und unterhalte ein Modell der Unternehmensinformation, um die Entwicklung von Anwendungen und
Entscheidungsprozesse zu unterst�tzen, in �bereinstimmung mit den IT-Pl�nen wie in PO1 beschrieben. Dieses Modell
erleichtert die optimale Errichtung, Verwendung und gemeinsame Benutzung von Informationen durch das Kerngesch�ft, und auf
eine Art, die die Datenintegrit�t erh�lt und dabei flexibel, funktionell, kosteng�nstig, fristgerecht, sicher und fehlertolerant ist.',
	'tImplementationGuide' => '',
),
7 => array(
	'fkContext' => '10372',
	'fkSectionBestPractice' => '10370',
	'nControlType' => '0',
	'sName' => 'PO2.2 Enterprise Data Dictionary and Data Syntax Rules (Unternehmensweites Data Dictionary und Datensyntaxregeln)',
	'tDescription' => 'F�hre ein unternehmensweites Data Dictionary, welches die Datensyntaxregeln der Organisation enth�lt. Dieses Data Dictionary
erm�glicht den gemeinsamen Zugriff auf Datenelemente �ber Anwendungen und Systeme, f�rdert unter den IT- und
Businessanwendern ein gemeinsames Datenverst�ndnis und verhindert das Entstehen von inkompatiblen Datenelementen.',
	'tImplementationGuide' => '',
),
8 => array(
	'fkContext' => '10373',
	'fkSectionBestPractice' => '10370',
	'nControlType' => '0',
	'sName' => 'PO2.3 Data Classification Scheme (Datenklassifikationsschema)',
	'tDescription' => 'Richte ein im gesamten Unternehmen anwendbares Klassifikationsschema ein, dem die Kritikalit�t und Sensitivit�t (zB
�ffentlich, vertraulich, streng geheim) der Unternehmensdaten zugrunde liegt. Dieses Schema beinhaltet Details �ber
Dateneigent�merschaft, die Festlegung von angemessenen Sicherheitsstufen und Schutzmechanismen und eine kurze
Beschreibung der Vorgaben f�r die Datenaufbewahrung und -zerst�rung, Kritikalit�t und Sensitivit�t. Das Schema ist Grundlage
f�r die Anwendung von Controls wie zB Zutrittskontrollen, Archivierung oder Verschl�sselung.',
	'tImplementationGuide' => '',
),
9 => array(
	'fkContext' => '10374',
	'fkSectionBestPractice' => '10370',
	'nControlType' => '0',
	'sName' => 'PO2.4 Integrity Management (Handhabung der Integrit�t)',
	'tDescription' => 'Definiere und implementiere Verfahren zur Sicherstellung der Integrit�t und Konsistenz aller in elektronischer Form
gespeicherten Daten, wie Datenbanken, Data Warehouses und Datenarchiven.',
	'tImplementationGuide' => '',
),
10 => array(
	'fkContext' => '10376',
	'fkSectionBestPractice' => '10375',
	'nControlType' => '0',
	'sName' => 'PO3.1 Technological Direction Planning (Planung der technologischen Ausrichtung)',
	'tDescription' => 'Analysiere bestehende und k�nftige Technologien und plane, welche technologische Richtung f�r die Umsetzung der ITStrategie
und der Architektur der Gesch�ftsanwendungen angemessen ist. Identifiziere im Plan, welche Technologien ein
Potential zur Generierung von Gesch�ftschancen in sich bergen. Der Plan sollte f�r die Komponenten der Infrastruktur die
Systemarchitektur, technologische Richtung, Migrationsstrategien sowie Aspekte im Rahmen der Notfallplanung (engl.:
contingency) behandeln.',
	'tImplementationGuide' => '',
),
11 => array(
	'fkContext' => '10377',
	'fkSectionBestPractice' => '10375',
	'nControlType' => '0',
	'sName' => 'PO3.2 Technical Infrastructure Plan ? Scope and Coverage (Technischer Infrastrukturplan ? Umfang und Abdeckung)',
	'tDescription' => 'Erstelle und unterhalte einen technischen Infrastrukturplan, der mit den strategischen und taktischen IT-Pl�nen abgestimmt ist.
Der Plan basiert auf der technologischen Ausrichtung und umfasst Ma�nahmen zur Notfallvorkehrung und Vorgaben f�r die
Beschaffung von technischen Ressourcen. Er betrachtet �nderungen im Wettbewerb, Skaleneffekte bei Stellenbesetzung und
Investitionen, sowie die verbesserte Interoperabilit�t von Plattformen und Applikationen.',
	'tImplementationGuide' => '',
),
12 => array(
	'fkContext' => '10378',
	'fkSectionBestPractice' => '10375',
	'nControlType' => '0',
	'sName' => 'PO3.3 Monitoring of Future Trends and Regulations (�berwachung von zuk�nftigen Trends und Bestimmungen)',
	'tDescription' => 'Entwickle einen Prozess, um Trends von Branche/Sektor, Technologie, Infrastruktur sowie der rechtlichen und regulatorischen
Rahmenbedingungen zu �berwachen. Ber�cksichtige die Auswirkungen dieser Trends bei der Erstellung des technologischen ITInfrastrukturplans.',
	'tImplementationGuide' => '',
),
13 => array(
	'fkContext' => '10379',
	'fkSectionBestPractice' => '10375',
	'nControlType' => '0',
	'sName' => 'PO3.4 Technology Standards (Technologische Standards)',
	'tDescription' => 'Etabliere ein technologisches Forum, das Technologierichtlinien, Beratung zu Infrastrukturprodukten und Anleitung zur Auswahl
von Technologien bereitstellt, messe die Compliance mit diesen Standards und Richtlinien, um konsistente, effektive und sichere
technische L�sungen unternehmensweit bereitzustellen. Dieses Forum legt technologische Standards und Methoden, basierend
auf deren Gesch�ftsrelevanz, Risiken und Einhaltung externer Anforderungen fest.',
	'tImplementationGuide' => '',
),
14 => array(
	'fkContext' => '10380',
	'fkSectionBestPractice' => '10375',
	'nControlType' => '0',
	'sName' => 'PO3.5 IT Architecture Board (IT-Architekturgremium)',
	'tDescription' => 'Schaffe ein IT-Architekturgremium, das Vorgaben im Bereich der Architektur erstellt und Ratschl�ge f�r ihre Anwendung und
Einhaltung bereitstellt. Diese Einheit lenkt das Design der IT-Architektur und stellt sicher, dass diese die Unternehmensstrategie
unterst�tzt und die Anforderungen der regulatorischen Compliance und der Notfallplanung ber�cksichtigt werden. Dies geschieht
im Kontext der Unternehmensarchitektur.',
	'tImplementationGuide' => '',
),
15 => array(
	'fkContext' => '10382',
	'fkSectionBestPractice' => '10381',
	'nControlType' => '0',
	'sName' => 'PO4.1 IT Process Framework (Framework der IT-Prozesse)',
	'tDescription' => 'Definiere ein Framework der IT-Prozesse, um den strategischen IT-Plan umzusetzen. Dieses Framework umfasst die Struktur
und Beziehung von IT-Prozessen (zB um L�cken und �berlappungen bei den Prozessen zu managen), Eigent�merschaft,
Reifegrad, Messung der Performance, Verbesserung, Compliance, Qualit�tsziele und Pl�ne, um diese zu erreichen. Es bildet die
Integration der IT-spezifischen Prozesse, der Prozesse im Unternehmensmanagement, Gesch�ftsprozesse und den Change
Prozessen des Unternehmens. Das Framework der IT-Prozesse sollte in ein Qualit�tsmanagementsystem und ein Framework der
Internal Controls integriert sein.',
	'tImplementationGuide' => '',
),
16 => array(
	'fkContext' => '10383',
	'fkSectionBestPractice' => '10381',
	'nControlType' => '0',
	'sName' => 'PO4.2 IT Strategy Committee (IT-Strategieausschuss)',
	'tDescription' => 'Etabliere einen IT-Strategieausschuss auf Ebene der Unternehmensleitung. Dieser Ausschuss stellt sicher, dass IT-Governance,
als Teil der Corporate Governance angemessen adressiert wird. Er ber�t bei der strategischen Ausrichtung und beurteilt im
Namen der Unternehmensleitung wesentliche Investitionen.',
	'tImplementationGuide' => '',
),
17 => array(
	'fkContext' => '10384',
	'fkSectionBestPractice' => '10381',
	'nControlType' => '0',
	'sName' => 'PO4.3 IT Steering Committee (IT-Lenkungsausschuss)',
	'tDescription' => 'Etabliere einen IT-Lenkungsausschuss (oder ein �quivalentes Gremium), das sich aus Mitgliedern der Unternehmensleitung,
Kerngesch�ftsprozess- und IT-Management zusammensetzt, um,
? die Priorit�ten der durch IT unterst�tzten Programme in Abstimmung mit der Unternehmensstrategie und deren
Priorit�ten festzulegen,
? Stati von Projekten zu verfolgen und Ressourcenkonflikte zu l�sen und
? die Service-Levels und Verbesserung von Services zu monitoren.',
	'tImplementationGuide' => '',
),
18 => array(
	'fkContext' => '10385',
	'fkSectionBestPractice' => '10381',
	'nControlType' => '0',
	'sName' => 'PO4.4 Organisational Placement of the IT Function (Organisatorische Eingliederung der IT-Organisation)',
	'tDescription' => 'Platziere die IT-Organisationseinheit in die Gesamtorganisation unter Beachtung der Bedeutung der IT f�r das Unternehmen,
speziell deren Kritikalit�t f�r die Unternehmensstrategie und die Abh�ngigkeit des operativen Betriebs von der IT. Die Stelle, an
die der/die CIO berichtet, entspricht der Bedeutung der IT im Unternehmen.',
	'tImplementationGuide' => '',
),
19 => array(
	'fkContext' => '10386',
	'fkSectionBestPractice' => '10381',
	'nControlType' => '0',
	'sName' => 'PO4.5 IT Organisational Structure (IT-Organisationsstruktur)',
	'tDescription' => 'Entwickle eine interne und externe IT-Organisationsstruktur, die die Unternehmenserfordernisse widerspiegelt. Etabliere
au�erdem einen Prozess, der periodisch die IT-Organisationsstruktur �berpr�ft, um die Anforderungen an die Personalausstattung
und die Beschaffungsstrategien den erwarteten Unternehmenszielen und sich �ndernden Umst�nden anzugleichen.',
	'tImplementationGuide' => '',
),
20 => array(
	'fkContext' => '10387',
	'fkSectionBestPractice' => '10381',
	'nControlType' => '0',
	'sName' => 'PO4.6 Roles and Responsibilities (Rollen und Verantwortlichkeiten)',
	'tDescription' => 'Definiere und kommuniziere Rollen und Verantwortlichkeiten f�r alle Mitarbeiter der Organisation, die mit
Informationssystemen in Verbindung stehen, um ausreichend Autorit�t f�r die Umsetzung der festgelegten Rollen und
Verantwortlichkeiten zu erm�glichen. Erstelle Rollenbeschreibungen und aktualisiere diese regelm��ig. Diese beschreiben
sowohl Autorit�t als auch Verantwortung, umfassen eine Festlegung der Kenntnisse und Erfahrungen, die f�r die Position
erforderlich sind, und k�nnen auch geeignet f�r die Performancebeurteilungen. Rollenbeschreibungen sollten die Verantwortung
f�r Internal Control umfassen.',
	'tImplementationGuide' => '',
),
21 => array(
	'fkContext' => '10388',
	'fkSectionBestPractice' => '10381',
	'nControlType' => '0',
	'sName' => 'PO4.7 Responsibility for IT Quality Assurance (Verantwortung f�r IT-Qualit�tssicherung)',
	'tDescription' => 'Weise die Verantwortung f�r die Ausf�hrung der Qualit�tssicherungsfunktion zu und statte die Qualit�tssicherungsgruppe mit
geeignetem Qualit�tssicherungssystemen, Controls und Kommunikationsexpertise aus. Die organisatorische Eingliederung, die
Verantwortlichkeiten und Gr��e der Qualit�tssicherungsgruppe stellt den Bedarf der Organisation sicher.',
	'tImplementationGuide' => '',
),
22 => array(
	'fkContext' => '10389',
	'fkSectionBestPractice' => '10381',
	'nControlType' => '0',
	'sName' => 'PO4.8 Responsibility for Risk, Security and Compliance (Verantwortung f�r Risiko, Sicherheit und Compliance)',
	'tDescription' => 'Verankere Eigent�merschaft und Verantwortung f�r IT-bezogene Risiken im Kerngesch�ft auf angemessen hoher Ebene.
Definiere und weise Rollen zu, die f�r das Management von IT-Risiken kritisch sind, inklusive spezifischer Verantwortung f�r
Informationssicherheit, physische Sicherheit und Compliance. Etabliere die Verantwortlichkeit f�r Risiko- und
Sicherheitsmanagement auf unternehmensweiter Ebene, um unternehmensweite Belange zu regeln. Weitere Verantwortlichkeiten
f�r Sicherheitsmanagement k�nnen bei Bedarf systemspezifisch zugewiesen werden, um relevante Sicherheitsbelange zu
behandeln. Hole von der Gesch�ftsf�hrung die grunds�tzliche Stossrichtung hinsichtlich der IT-Risikobereitschaft und die
Freigabe von IT-Restrisiken ein.',
	'tImplementationGuide' => '',
),
23 => array(
	'fkContext' => '10390',
	'fkSectionBestPractice' => '10381',
	'nControlType' => '0',
	'sName' => 'PO4.9 Data and System Ownership (Daten- und Systemeignerschaft)',
	'tDescription' => 'Unterst�tze das Kerngesch�ft mit Verfahren und Werkzeugen, um die �bernahme der Eigent�merschaft f�r Daten- und
Informationssysteme zu erm�glichen. Eigner f�llen Entscheidungen hinsichtlich der Klassifikation von Informationen und
Systemen und dem der Klassifikation entsprechenden Schutz.',
	'tImplementationGuide' => '',
),
24 => array(
	'fkContext' => '10391',
	'fkSectionBestPractice' => '10381',
	'nControlType' => '0',
	'sName' => 'PO4.10 Supervision (Beaufsichtigung)',
	'tDescription' => 'Bette angemessene Verfahren zur Beaufsichtigung in die IT-Organisation ein, um sicherzustellen, dass Rollen und
Verantwortlichkeiten korrekt ausgef�hrt werden, um beurteilen zu k�nnen, ob das Personal ausreichende Autorit�t und
Ressourcen zur �bernahme ihrer Rollen und Verantwortlichkeiten besitzen, und, um allgemein die Key Performance Indicators
�berblicken zu k�nnen.',
	'tImplementationGuide' => '',
),
25 => array(
	'fkContext' => '10392',
	'fkSectionBestPractice' => '10381',
	'nControlType' => '0',
	'sName' => 'PO4.11 Segregation of Duties (Funktionstrennung)',
	'tDescription' => 'Etabliere eine Trennung von Rollen und Verantwortlichkeiten, die die Wahrscheinlichkeit reduziert, dass eine Einzelperson einen
kritischen Prozess untergr�bt. Das Management stellt weiters sicher, dass das Personal ausschlie�lich genehmigte, ihrer Stelle
und Position entsprechende Aktivit�ten ausf�hrt.',
	'tImplementationGuide' => '',
),
26 => array(
	'fkContext' => '10393',
	'fkSectionBestPractice' => '10381',
	'nControlType' => '0',
	'sName' => 'PO4.12 IT Staffing (Stellenbesetzung der IT)',
	'tDescription' => 'Evaluiere Anforderungen an die Stellenbesetzung regelm��ig oder nach wesentlichen �nderungen im Unternehmen, Betrieb oder
der IT-Umgebung, um sicher zu stellen, dass die IT-Organisation eine ausreichende Zahl kompetenter Mitarbeiter hat. Die
Stellenbesetzung beachtet auch Zusammenarbeit zwischen Unternehmens- und IT-Personal, funktions�bergreifende Ausbildung,
Job Rotation und M�glichkeiten zum Outsourcing.',
	'tImplementationGuide' => '',
),
27 => array(
	'fkContext' => '10394',
	'fkSectionBestPractice' => '10381',
	'nControlType' => '0',
	'sName' => 'PO4.13 Key IT Personnel (Schl�sselpersonal der IT)',
	'tDescription' => 'Bestimme und identifiziere Schl�sselpersonal der IT und minimiere die �berm��ige Abh�ngigkeit von diesen. Ein Plan sollte
existieren, um im Notfall mit ihnen Kontakt aufnehmen zu k�nnen.',
	'tImplementationGuide' => '',
),
28 => array(
	'fkContext' => '10395',
	'fkSectionBestPractice' => '10381',
	'nControlType' => '0',
	'sName' => 'PO4.14 Contracted Staff Policies and Procedures (Policies und Verfahren f�r beigezogenes Personal)',
	'tDescription' => 'Definiere Policies und Verfahren f�r die Steuerung der Aktivit�ten von Consultants und anderem Vertragspersonal der ITFunktion,
um sicher zu stellen, dass der Schutz der Informationen und Informationssysteme der Organisation gew�hrleistet ist
und die vertraglichen Vereinbarungen erreicht werden.',
	'tImplementationGuide' => '',
),
29 => array(
	'fkContext' => '10396',
	'fkSectionBestPractice' => '10381',
	'nControlType' => '0',
	'sName' => 'PO4.15 Relationships (Beziehungen)',
	'tDescription' => 'Erstelle und unterhalte eine optimale Koordinations-, Kommunikations- und Verbindungsstruktur zwischen der IT-Organisation
und den verschiedenen anderen Interessen innerhalb und au�erhalb der IT, wie beispielsweise die Gesch�ftsf�hrung,
Bereichsleiter, Unternehmenseinheiten, einzelne User, Lieferanten, Security Officers, Risk Manager, die unternehmensweite
Compliance Gruppe, Outsourcer und Management von ausgelagerten Einheiten.',
	'tImplementationGuide' => '',
),
30 => array(
	'fkContext' => '10398',
	'fkSectionBestPractice' => '10397',
	'nControlType' => '0',
	'sName' => 'PO5.1 Financial Management Framework (Framework f�r das Management von Finanzen)',
	'tDescription' => 'Entwickle ein finanzielles Framework IT, das den Budgetierungsprozess und Kosten-/Nutzenanalysen vorantreibt und das auf
Portfolios f�r Investitionen, Services und Anlagen aufgebaut ist. Unterhalte das Portfolio IT-gest�tzter Investitionsprogramme,
IT-Services und IT-Werten, das die Basis f�r das aktuelle IT-Budget darstellt. Liefere dem Kerngesch�ft einen Input f�r Neu-
Investitionen unter Ber�cksichtigung der aktuellen IT-Werte und IT Service Portfolios. Neuinvestitionen und Unterhalt von
Service- und Asset Portfolios haben einen Einfluss auf k�nftige IT-Budgets. Kommuniziere die Kosten- und Nutzenaspekte
dieser Portfolios in den Budgetpriorisierungs-, Kostenmanagement- und Nutzenmanagement-Prozessen.',
	'tImplementationGuide' => '',
),
31 => array(
	'fkContext' => '10399',
	'fkSectionBestPractice' => '10397',
	'nControlType' => '0',
	'sName' => 'PO5.2 Priorisation Within IT Budget (Priorisierung innerhalb des IT-Budgets)',
	'tDescription' => 'Implementiere einen Entscheidungsfindungsprozess, der die Zuordnung der IT-Ressourcen f�r den laufenden Betrieb, Projekte
und Unterhalt priorisiert. Dies mit dem Ziel, den Beitrag von IT-gest�tzten Investitionsprogrammen und anderer IT-Services und
IT?Werten zu maximieren.',
	'tImplementationGuide' => '',
),
32 => array(
	'fkContext' => '10400',
	'fkSectionBestPractice' => '10397',
	'nControlType' => '0',
	'sName' => 'PO5.3 IT Budgeting Process (IT-Budgetierungsprozess)',
	'tDescription' => 'F�hre einen Prozess zur Erstellung und Steuerung des Budgets ein, der die Priorit�ten f�r IT-unterst�tzte Investitionsprogramme
widerspiegelt und die laufenden Kosten f�r Betrieb und Unterhalt der bestehenden Infrastruktur umfasst. Der Prozess sollte die
Entwicklung eines gesamthaften IT-Budgets, sowie jenes von individuellen Programmen unterst�tzen, und sich auf die
jeweiligen IT-Komponenten dieser Programme konzentrieren. Der Prozess sollte den laufenden Review, Anpassungen und
Freigaben von Gesamtbudgets und Budgets f�r einzelne Programme erm�glichen.',
	'tImplementationGuide' => '',
),
33 => array(
	'fkContext' => '10401',
	'fkSectionBestPractice' => '10397',
	'nControlType' => '0',
	'sName' => 'PO5.4 Cost Management (Kostenmanagement)',
	'tDescription' => 'Etabliere einen Kostenmanagement-Prozess, der die aktuellen Kosten mit dem Budget vergleicht. Kosten sollten �berwacht und
berichtet werden. Eventuelle Abweichungen sollten rechtzeitig identifiziert und deren Auswirkungen auf Programme beurteilt
werden. Gemeinsam mit den Programm-Sponsoren aus den Kerngesch�ftsprozessen sollten geeignete Sanierungsma�nahmen
definiert werden, gegebenenfalls sollte der Business-Case angepasst werden.',
	'tImplementationGuide' => '',
),
34 => array(
	'fkContext' => '10402',
	'fkSectionBestPractice' => '10397',
	'nControlType' => '0',
	'sName' => 'PO5.5 Benefit Management (Nutzenmanagement)',
	'tDescription' => 'Implementiere einen Prozess, der den Nutzen monitort. Der von der IT erwartete Beitrag zu den Gesch�ftsergebnissen, entweder
als Komponente von IT-unterst�tzten Investitions-Programmen oder als Teil der regelm��igen Betriebsunterst�tzung, sollte
identifiziert, vereinbart, gemonitort und berichtet werden. Die Reports sollten einem Review unterzogen werden und, wo die
M�glichkeit einer Erh�hung des IT-Beitrags besteht, sollten geeignete Aktionen festgelegt und durchgef�hrt werden. Wenn sich
der Beitrag der IT zum Programm �ndert oder wo �nderungen anderer Projekte das Programm beeinflussen, sollte der Business-
Case des Programms aktualisiert werden.',
	'tImplementationGuide' => '',
),
35 => array(
	'fkContext' => '10404',
	'fkSectionBestPractice' => '10403',
	'nControlType' => '0',
	'sName' => 'PO6.1 IT Policy and Control Environment (IT-Richtlinien und Control-Umfeld)',
	'tDescription' => 'Lege die Elemente des IT Control-Umfelds fest, das mit der Philosophie und dem Arbeitsstil des Unternehmensmanagement
�bereinstimmt. Diese Elemente umfassen Erwartungen/Anforderungen hinsichtlich Generierung von Wertbeitr�gen aus
Investitionen in IT, die Risikobereitschaft, Integrit�t, ethische Werte, Kompetenz des Personals, Verantwortlichkeit und
Zust�ndigkeit. Das Control-Umfeld gr�ndet auf einer Kultur, welche die Nutzenerbringung unterst�tzt und dabei das
Management signifikanter Risiken unterst�tzt, die Zusammenarbeit �ber mehrere Abteilungen hinweg und Teamwork f�rdert,
Compliance und laufende Prozessverbesserung f�rdert und Prozessabweichungen (inklusive Fehler) vern�nftig handhabt.',
	'tImplementationGuide' => '',
),
36 => array(
	'fkContext' => '10405',
	'fkSectionBestPractice' => '10403',
	'nControlType' => '0',
	'sName' => 'PO6.2 Enterprise IT Risk and Internal Control Framework (Unternehmensweites Framework f�r IT-Risiken und
Internal Controls)',
	'tDescription' => 'Entwickle und unterhalte ein Framework, das den unternehmensweiten, �bergeordneten Ansatz zum Risikomanagement und
Internal Controls darstellt, um Nutzen zu generieren und gleichzeitig die Ressourcen und Systeme der sch�tzt. Das Framework
sollte in das IT-Prozessmodell und das Qualit�tsmanagementsystem integriert sein und den �bergeordneten Unternehmenszielen
entsprechen. Es sollte ausgerichtet sein auf die Maximierung der Erfolge der Nutzenerbringung unter gleichzeitiger Minimierung
von Risiken f�r Informationswerte mittels vorbeugender Ma�nahmen, rechtzeitiger Identifikation von Unregelm��igkeiten,
Begrenzung von Verlusten und der zeitnahen Wiederherstellung der Unternehmenswerte.',
	'tImplementationGuide' => '',
),
37 => array(
	'fkContext' => '10406',
	'fkSectionBestPractice' => '10403',
	'nControlType' => '0',
	'sName' => 'PO6.3 IT Policies Management (Management der IT-Richtlinien)',
	'tDescription' => 'Entwickle und unterhalte einen Satz von Richtlinien zur Unterst�tzung der IT-Strategie. Diese Richtlinien sollten die Absicht der
Richtlinie, Rollen und Verantwortlichen, Prozesse zur Ausnahmebehandlung, Ansatz zur Compliance und Referenzen zu
Verfahren, Standards und Anleitungen umfassen. Die Richtlinen sollten die wichtigsten Themen, wie Qualit�t, Sicherheit,
Vertraulichkeit, Internal Controls und Schutz von geistigem Eigentum behandeln. Die Relevanz der Richtlinien sollte regelm��ig
best�tigt und bewilligt werden.',
	'tImplementationGuide' => '',
),
38 => array(
	'fkContext' => '10407',
	'fkSectionBestPractice' => '10403',
	'nControlType' => '0',
	'sName' => 'PO6.4 Policy Rollout (Kommunikation der IT-Richtlinien)',
	'tDescription' => 'Stelle sicher, dass IT-Richtlinien an alle relevanten Mitarbeiter kommuniziert und in Kraft gesetzt werden, und dass sie zu einem
integralen Bestandteil der Unternehmensabl�ufe werden. Die eingesetzten Kommunikationstechniken sollten Ressourcen- und
Kenntnisbedarf und deren Auswirkungen ber�cksichtigen.',
	'tImplementationGuide' => '',
),
39 => array(
	'fkContext' => '10408',
	'fkSectionBestPractice' => '10403',
	'nControlType' => '0',
	'sName' => 'PO6.5 Communication of IT Objectives and Direction (Kommunikation von Zielen und Ausrichtung der IT)',
	'tDescription' => 'Stelle sicher, dass das Bewusstsein und Verst�ndnis f�r Ziele und Ausrichtung des Unternehmens und der IT im gesamten
Unternehmen kommuniziert werden. Die kommunizierte Information sollte eine klar festgelegte Mission, Ziele von Services,
Security, Internal Controls, Qualit�t, Ethische- und Verfahrensgrunds�tze, Richtlinien, Verfahren, etc. umfassen und in ein
kontinuierliches Kommunikationsprogramm eingebettet sein, das durch die Gesch�ftsf�hrung Wort und Tat unterst�tzt wird. Das
Management sollte speziell darauf achten, dass IT-Sicherheitsbewusstsein und die Botschaft vermittelt wird, dass f�r ITSicherheit
alle verantwortlich sind.',
	'tImplementationGuide' => '',
),
40 => array(
	'fkContext' => '10410',
	'fkSectionBestPractice' => '10409',
	'nControlType' => '0',
	'sName' => 'PO7.1 Personnel Recruitment and Retention (Personalrekrutierung und -bindung)',
	'tDescription' => 'Stelle sicher, dass der IT-Personalrekrutierungsprozess �bereinstimmt mit den unternehmensweiten Richtlinien und Verfahren f�r
Personal (zB Anstellung, positive Arbeitsumgebung und Orientierung). Das Management implementiert Prozesse, die
sicherstellen, dass die Organisation angemessenes IT-Personal einsetzt, welche die notwendigen F�higkeiten besitzen,
Unternehmensziele zu erreichen.',
	'tImplementationGuide' => '',
),
41 => array(
	'fkContext' => '10411',
	'fkSectionBestPractice' => '10409',
	'nControlType' => '0',
	'sName' => 'PO7.2 Personnel Competencies (Kompetenzen des Personals)',
	'tDescription' => '�berpr�fe regelm��ig, dass das Personal die n�tige Kompetenz besitzt, um seine Aufgaben anhand seiner Bildung, Schulungen
und/oder Erfahrungen durchzuf�hren. Definiere Anforderungen f�r IT-Kernkompetenzen und stelle sicher, dass diese, wo
geeignet, durch Programme f�r Qualifikation und Zertifizierung unterhalten werden.',
	'tImplementationGuide' => '',
),
42 => array(
	'fkContext' => '10412',
	'fkSectionBestPractice' => '10409',
	'nControlType' => '0',
	'sName' => 'PO7.3 Staffing of Roles (Besetzung von Rollen)',
	'tDescription' => 'Definiere, monitore und �berwache Rollen, Verantwortlichkeiten und den Verg�tungsrahmen der Mitarbeiter ? einschlie�lich der
Erfordernis, Richtlinien und Verfahren des Management, ethische Grunds�tze und professionelle Praktiken einzuhalten. Die
Bedingungen des Angestelltenverh�ltnisses sollten die Verantwortlichkeiten der Mitarbeiter hinsichtlich Informationssicherheit,
Internal Controls und Compliance mit Regulativen betonen. Der Grad der �berwachung sollte an die Sensitivit�t der Position und
dem Ausma� der zugewiesenen Verantwortlichkeiten angepasst sein.',
	'tImplementationGuide' => '',
),
43 => array(
	'fkContext' => '10413',
	'fkSectionBestPractice' => '10409',
	'nControlType' => '0',
	'sName' => 'PO7.4 Personnel Training (Ausbildung des Personals)',
	'tDescription' => 'Biete dem IT-Personal bei der Anstellung eine entsprechend Einweisung an und f�hre laufend Schulungen durch, um Wissen,
F�higkeiten, Begabungen und ein Bewusstsein f�r Internal Controls und Security auf dem Niveau zu erhalten, das notwendig ist,
um die Unternehmensziele zu erreichen.',
	'tImplementationGuide' => '',
),
44 => array(
	'fkContext' => '10414',
	'fkSectionBestPractice' => '10409',
	'nControlType' => '0',
	'sName' => 'PO7.5 Dependence Upon Individuals (Abh�ngigkeit von Einzelpersonen)',
	'tDescription' => 'Minimiere die Gefahr kritischer Abh�ngigkeiten von Schl�sselpersonen durch Wissensaufzeichnung (engl.: knowledge capture)
(Dokumentation), Teilen von Wissen, Nachfolgeplanung und Vertretung von Personal.',
	'tImplementationGuide' => '',
),
45 => array(
	'fkContext' => '10415',
	'fkSectionBestPractice' => '10409',
	'nControlType' => '0',
	'sName' => 'PO7.6 Personnel Clearance Procedures (Verfahren zur �berpr�fung von Personal)',
	'tDescription' => 'Schlie�e Hintergrund-Checks im IT Recruiting-Prozess ein. Das Ausma� und die H�ufigkeit der �berpr�fung dieser Checks sind
von der Sensitivit�t und/oder der Kritikalit�t der Funktion abh�ngig; und sie sollten f�r Angestellte, Vertragspartner und
Lieferanten durchgef�hrt werden.',
	'tImplementationGuide' => '',
),
46 => array(
	'fkContext' => '10416',
	'fkSectionBestPractice' => '10409',
	'nControlType' => '0',
	'sName' => 'PO7.7 Employee Job Performance Evaluation (Beurteilung der Leistung von Mitarbeitern)',
	'tDescription' => 'Fordere auf regelm��iger Basis die Durchf�hrung zeitgerechter Beurteilung in Bezug auf individuelle Ziele, welche von den
Unternehmenszielen, bestehenden Standards und spezifischen Aufgaben abgeleitet werden. Mitarbeiter sollten, wo m�glich in
der Leistung und in Ihrem Verhalten unterst�tzt werden.',
	'tImplementationGuide' => '',
),
47 => array(
	'fkContext' => '10417',
	'fkSectionBestPractice' => '10409',
	'nControlType' => '0',
	'sName' => 'PO7.8 Job Change and Termination (Stellenwechsel und K�ndigung)',
	'tDescription' => 'Ergreife rasch Aktionen bei Jobwechsel, insbesondere bei der Aufl�sung des Arbeitsverh�ltnisses. Der Wissenstransfer muss
vorbereitet sein, Verantwortlichkeiten neu zugewiesen und Zugriffsrechte entfernt werden, um Risken zu minimieren und die
Fortf�hrung der Funktion zu gew�hrleisten.',
	'tImplementationGuide' => '',
),
48 => array(
	'fkContext' => '10419',
	'fkSectionBestPractice' => '10418',
	'nControlType' => '0',
	'sName' => 'PO8.1 Quality Management System (Qualit�tsmanagement-System)',
	'tDescription' => 'Entwickle und unterhalte ein QMS, das einen standardisierten, formalen und kontinuierlichen Ansatz hinsichtlich
Qualit�tsmanagement bietet, der an die Unternehmenserfordernisse ausgerichtet ist. Das QMS identifiziert
Qualit�tsanforderungen und -kriterien, wesentliche IT-Prozesse mit deren Abfolge und Interaktion und die Richtlinien, Kriterien
und Methoden f�r die Definition, Erkennung, Korrektur und Verhinderung der Nichteinhaltung. Das QMS sollte die
organisatorische Struktur f�r Qualit�tsmanagement festlegen, und die Rollen, Aufgaben und Verantwortlichkeiten abdecken. Alle
wesentlichen Bereiche entwickeln ihre Qualit�tsplane entsprechend der Kriterien und Richtlinien und zeichnen
Qualit�tsinformationen auf. Monitore und messe die Wirksamkeit und Akzeptanz des QMS und verbessere es, wenn notwendig.',
	'tImplementationGuide' => '',
),
49 => array(
	'fkContext' => '10420',
	'fkSectionBestPractice' => '10418',
	'nControlType' => '0',
	'sName' => 'PO8.2 IT Standards and Quality Practices (Standards und Qualit�tssicherungs-Verfahren der IT)',
	'tDescription' => 'Identifiziere und unterhalte Standards, Methoden und Praktiken f�r die wesentlichen IT-Prozesse, um die Organisation in der
Erreichung der Ziele des QMS zu unterst�tzen. Wende Best-Practices der Branche bei der Verbesserung oder Anpassung der
Qualit�tspraktiken der Organisation an.',
	'tImplementationGuide' => '',
),
50 => array(
	'fkContext' => '10421',
	'fkSectionBestPractice' => '10418',
	'nControlType' => '0',
	'sName' => 'PO8.3 Development and Acquisition Standards (Entwicklungs- und Beschaffungs-Standards)',
	'tDescription' => '�bernehme und unterhalte ? dem Lebenszyklus eines Endproduktes folgenden ? Standards f�r alle Entwicklungen und
Beschaffungen und ber�cksichtige Freigaben von wichtigen Milestones auf Basis von vereinbarten Abnahme-Kriterien. Zu
ber�cksichtigende Punkte umfassen Standards zur Programmierung, Namenskonventionen, Dateiformate, Designstandards f�r
Datenschema und Data Dictionaries, Standards f�r das User-Interface, Interoperabilit�t, Effizienz der Systemperformance,
Skalierbarkeit, Standards f�r Entwicklung und Tests, Validierung der Anforderungen, Testpl�ne sowie Modul-, Regressions- und
Integrationstests.',
	'tImplementationGuide' => '',
),
51 => array(
	'fkContext' => '10422',
	'fkSectionBestPractice' => '10418',
	'nControlType' => '0',
	'sName' => 'PO8.4 Customer Focus (Kundenorientierung)',
	'tDescription' => 'Stelle sicher, dass sich das Qualit�tsmanagement auf Kunden fokussiert, indem ihre Anforderungen erhoben werden und diese
mit den IT-Standards und -Praktiken in Einklang gebracht werden. Rollen und Verantwortlichkeiten f�r die Konfliktbew�ltigung
zwischen Usern/Kunden und der IT-Organisation sind festgelegt.',
	'tImplementationGuide' => '',
),
52 => array(
	'fkContext' => '10423',
	'fkSectionBestPractice' => '10418',
	'nControlType' => '0',
	'sName' => 'PO8.5 Continuous Improvement (Kontinuierliche Verbesserung)',
	'tDescription' => 'Ein allgemeiner Qualit�tsplan, der eine kontinuierliche Verbesserung f�rdert, wird regelm��ig gewartet und kommuniziert.',
	'tImplementationGuide' => '',
),
53 => array(
	'fkContext' => '10424',
	'fkSectionBestPractice' => '10418',
	'nControlType' => '0',
	'sName' => 'PO8.6 Quality Measurement, Monitoring and Review (Messung, Management und Review der Qualit�t)',
	'tDescription' => 'Definiere, plane und implementiere Ma�nahmen f�r das regelm��ige Monitoring der Compliance mit dem QMS und den Nutzen,
den das QMS bringt. Messung, Monitoring und Aufzeichnung der Information sollte von den Prozesseignern verwendet werden,
um geeignete korrektive und pr�ventive Ma�nahmen zu treffen.',
	'tImplementationGuide' => '',
),
54 => array(
	'fkContext' => '10426',
	'fkSectionBestPractice' => '10425',
	'nControlType' => '0',
	'sName' => 'PO9.1 IT and Business Risk Management Alignment (Abstimmung des Risikomanagements der IT und des
Unternehmens)',
	'tDescription' => 'Integriere die Frameworks f�r IT-Governance, Risikomanagement und Controls in das unternehmensweite Modell f�r
Risikomanagement. Dies beinhaltet die Ausrichtung bez�glich des Risikoappetits des Unternehmens und dem Grad der
Risikotoleranz.',
	'tImplementationGuide' => '',
),
55 => array(
	'fkContext' => '10427',
	'fkSectionBestPractice' => '10425',
	'nControlType' => '0',
	'sName' => 'PO9.2 Establishment of Risk Context (Festlegung des Risikokontext)',
	'tDescription' => 'Entwickle den Kontext, in den das Risikobeurteilungs-Framework eingebettet wird, um die Angemessenheit der Ergebnisse
sicher zu stellen. Dies umfasst die Bestimmung der internen und externen Rahmenbedingungen f�r jede Risikobewertung, die
Ziele der Bewertung und die Kriterien, nach denen Risiken evaluiert werden.',
	'tImplementationGuide' => '',
),
56 => array(
	'fkContext' => '10428',
	'fkSectionBestPractice' => '10425',
	'nControlType' => '0',
	'sName' => 'PO9.3 Event Identification (Ereignisidentifikation)',
	'tDescription' => 'Identifiziere s�mtliche Ereignisse (Bedrohungen oder Verletzbarkeiten) mit einer potentiellen Auswirkung auf die Ziele oder den
Betrieb des Unternehmens; einschlie�lich der folgenden Aspekte: Gesch�ftst�tigkeit, Verordnungen, Recht, Technologie,
Handelspartner, Personal und Betrieb. Bestimme die Art der Auswirkungen ? positiv, negativ oder beides ? und unterhalte diese
Informationen.',
	'tImplementationGuide' => '',
),
57 => array(
	'fkContext' => '10429',
	'fkSectionBestPractice' => '10425',
	'nControlType' => '0',
	'sName' => 'PO9.4 Risk Assessment (Bewertung von Risiken)',
	'tDescription' => 'Bewerte regelm��ig, unter Anwendung qualitativer und quantitativer Methoden, die Wahrscheinlichkeit und Auswirkungen aller
identifizierten Risiken. Die Wahrscheinlichkeit und Auswirkungen, die mit inh�renten und Restrisiken verbunden sind, sollten
einzeln, pro Kategorie und auf Basis eines Portfolios bestimmt werden.',
	'tImplementationGuide' => '',
),
58 => array(
	'fkContext' => '10430',
	'fkSectionBestPractice' => '10425',
	'nControlType' => '0',
	'sName' => 'PO9.5 Risk Response (Ma�nahmen zur Risikobehandlung)',
	'tDescription' => 'Bestimme einen Risikoeigner und betroffene Prozesseigner, entwickle und unterhalte Risikoantworten, um sicher zu stellen, dass
kosteng�nstige Controls und Sicherheitsma�nahmen Risiken kontinuierlich reduzieren. Die Risikoreaktion sollte
Risikostrategien, wie Vermeidung, Reduktion, Teilung oder Akzeptanz identifizieren. Ber�cksichtige bei der Entwicklung der
Ma�nahmen die Kosten und den Nutzen und w�hle Ma�nahmen, die das Restrisiko innerhalb des festgelegten Toleranzniveaus
halten.',
	'tImplementationGuide' => '',
),
59 => array(
	'fkContext' => '10431',
	'fkSectionBestPractice' => '10425',
	'nControlType' => '0',
	'sName' => 'PO9.6 Maintenance and Monitoring of a Risk Action Plan (Erhalt und Monitoring eines Plans zur Risikobehandlung)',
	'tDescription' => 'Priorisiere und Plane die Kontrollaktivit�ten auf allen Ebenen, um die Risikoantworten wie festgelegt umzusetzen; einschlie�lich
Kosten, Nutzen und Verantwortlichkeiten f�r die Ausf�hrung. Hole Genehmigung f�r die empfohlenen Aktivit�ten und
Freigaben f�r alle verbleibenden Risiken ein und stelle sicher, dass zugesagte Aktivit�ten durch die betroffenen Prozesseigner
verantwortet werden. Monitore die Umsetzung der Pl�ne und berichte der Gesch�ftsf�hrung s�mtliche Abweichungen.',
	'tImplementationGuide' => '',
),
60 => array(
	'fkContext' => '10433',
	'fkSectionBestPractice' => '10432',
	'nControlType' => '0',
	'sName' => 'PO10.1 Programme Management Framework (Programmmanagement-Framework)',
	'tDescription' => 'Unterhalte das Projekt-Programm in Verbindung mit dem Portfolio an IT-gest�tzten Investitionsprogrammen ? durch
Identifikation, Festlegung, Evaluierung, Priorisierung, Auswahl, Initiierung, Management und Steuerung von Projekten. Stelle
sicher, dass die Projekte die Ziele des Programms unterst�tzen. Koordiniere die Aktivit�ten und gegenseitigen Abh�ngigkeiten
von mehreren Projekten, manage den Beitrag aller Projekte innerhalb des Programms zu den erwarteten Ergebnissen und l�se
Ressorcenbedarf und -konflikte.',
	'tImplementationGuide' => '',
),
61 => array(
	'fkContext' => '10434',
	'fkSectionBestPractice' => '10432',
	'nControlType' => '0',
	'sName' => 'PO10.2 Project Management Framework (Projektmanagement-Framework)',
	'tDescription' => 'Erstelle und unterhalte ein allgemeines Projektmanagement-Framework, das den Umfang und Grenzen des Projektmanagements
sowie die f�r alle unternommenen Projekte anzuwendenden Methodologien definiert. Diese Methodologien sollten mindestens
die Initiierungs-, Planungs-, Ausf�hrungs-, Controlling- und Projektabschlussphasen umfassen, sowie die Kontrollpunkte und
Freigaben. Das Framework und die unterst�tzenden Methodologien sollten in das unternehmensweite
Projektportfoliomanagement und die Programmmanagement-Prozesse integriert werden.',
	'tImplementationGuide' => '',
),
62 => array(
	'fkContext' => '10435',
	'fkSectionBestPractice' => '10432',
	'nControlType' => '0',
	'sName' => 'PO10.3 Project Management Approach (Projektmanagement-Ansatz)',
	'tDescription' => 'Etabliere einen generischen Projektmanagement-Ansatz passend f�r Projekte unterschiedlicher Gr��e, Komplexit�t und
rechtlicher Rahmenbedingungen. Die Struktur zur Projektsteuerung kann Rollen, Verantwortlichkeiten und Zust�ndigkeiten von
Programm- und Projektsponsoren, Lenkungsausschuss, Projektb�ro und Projektmanager und die Mechanismen, durch die diese
die Verantwortlichkeiten �bernehmen k�nnen (wie Berichterstattung und Phasen-Reviews). Stelle sicher, dass alle IT-Projekte
Sponsoren mit ausreichender Autorit�t besitzen, um die Verantwortung f�r die Projektumsetzung im Rahmen der strategischen
Gesamtprogramms umzusetzen.',
	'tImplementationGuide' => '',
),
63 => array(
	'fkContext' => '10436',
	'fkSectionBestPractice' => '10432',
	'nControlType' => '0',
	'sName' => 'PO10.4 Stakeholder Commitment (Beteiligung der Stakeholder)',
	'tDescription' => 'Hole die Zusage und Beteiligung der betroffenen Stakeholder bei der Festlegung und Ausf�hrung des Projektes im Rahmen des
�bergeordneten IT-gest�tzten Investitionsprogramms ein.',
	'tImplementationGuide' => '',
),
64 => array(
	'fkContext' => '10437',
	'fkSectionBestPractice' => '10432',
	'nControlType' => '0',
	'sName' => 'PO10.5 Project scope statement (Beschreibung des Projektumfangs)',
	'tDescription' => 'Definiere und dokumentiere die Art und den Umfang des Projekts, um unter den Stakeholdern ein gemeinsames Verst�ndnis f�r
den Projektumfang zu best�tigen und zu entwickeln ? und wie dieses Projekt sich verh�lt mit anderen Projekten innerhalb des ITgest�tzten
Investitionsprogramms. Die Definition sollte vor der Projektinitiierung durch die Programm- und Projektsponsoren
formal freigegeben sein.',
	'tImplementationGuide' => '',
),
65 => array(
	'fkContext' => '10438',
	'fkSectionBestPractice' => '10432',
	'nControlType' => '0',
	'sName' => 'PO10.6 Project Phase Initiation (Initiierung von Projektphasen)',
	'tDescription' => 'Stelle sicher, dass die Initialisierung wesentlicher Projektphasen formell verabschiedet und allen Stakeholdern kommuniziert
wird. Die Genehmigung der Initialisierungsphase sollte auf Entscheiden der Programmsteuerung basieren. Die Genehmigung der
nachfolgenden Phasen sollte auf einer �berpr�fung und Abnahme der Ergebnisse der vorhergehenden Phase basieren und einer
Abnahme eines aktualisierten Business-Case anl�sslich der n�chsten gr��eren �berpr�fung des Programms. Im Fall sich
�berlappender Projektphasen sollte ein Punkt zur Freigabe durch die Programm- und Projektsponsoren festgelegt werden, um die
Projektfortf�hrung zu genehmigen.',
	'tImplementationGuide' => '',
),
66 => array(
	'fkContext' => '10439',
	'fkSectionBestPractice' => '10432',
	'nControlType' => '0',
	'sName' => 'PO10.7 Integrated Project Plan (Integrierter Projektplan)',
	'tDescription' => 'Erstelle einen formellen und genehmigten integrierten (die Unternehmens- und IT Ressourcen umfassenden) Projektplan zur
Steuerung der Projektumsetzung und Projektsteuerung w�hrend des gesamten Projekts. Die Aktivit�ten und gegenseitigen
Abh�ngigkeiten von mehreren Projekten innerhalb eines Programms sollten verstanden und dokumentiert sein. Der Projektplan
sollte w�hrend der Projektlaufzeit unterhalten werden. Der Projektplan und die �nderungen daran sollten entsprechend der
Frameworks zur Programm- und Projektsteuerung genehmigt werden.',
	'tImplementationGuide' => '',
),
67 => array(
	'fkContext' => '10440',
	'fkSectionBestPractice' => '10432',
	'nControlType' => '0',
	'sName' => 'PO10.8 Project Resources (Projekt-Ressourcen)',
	'tDescription' => 'Lege die Verantwortlichkeiten, Beziehungen, Kompetenzen und Leistungskriterien der Projektteam-Mitglieder fest und
spezifiziere f�r das Projekt die Grundlage f�r die Beschaffung und Zuweisung kompetenter Mitarbeiter und/oder
Vertragsnehmer. Die Beschaffung von Produkten oder Diensten, welche f�r jedes Projekt ben�tigt werden, sollten geplant und
gemanagt werden, um die Projektziele durch Verwendung der Beschaffungspraktiken des Unternehmens zu erreichen.',
	'tImplementationGuide' => '',
),
68 => array(
	'fkContext' => '10441',
	'fkSectionBestPractice' => '10432',
	'nControlType' => '0',
	'sName' => 'PO10.9 Project Risk Management (Projekt-Risikomanagement)',
	'tDescription' => 'Beseitige oder reduziere spezifische, mit einzelnen Projekten in Verbindung stehende Risiken durch einen systematischen
Prozess zur Planung, Identifikation, Analyse, Reaktion, Monitoring und Steuerung der Bereiche oder Ereignisse, die das Potential
besitzen, unerw�nschte �nderungen zu verursachen. Die Risiken, denen der Projektmanagement-Prozesses ausgesetzt ist, und
der Projektergebnisse sollten festgehalten und zentral aufgezeichnet werden.',
	'tImplementationGuide' => '',
),
69 => array(
	'fkContext' => '10442',
	'fkSectionBestPractice' => '10432',
	'nControlType' => '0',
	'sName' => 'PO10.10 Project Quality Plan (Projekt-Qualit�tsplan)',
	'tDescription' => 'Bereite einen Qualit�tsmanagementplan vor, der das Projekt-Qualit�tssystem und dessen Umsetzung beschreibt. Der Plan sollte
formell gepr�ft und durch alle betroffenen Parteien abgenommen werden und dann in den Projektplan integriert werden.',
	'tImplementationGuide' => '',
),
70 => array(
	'fkContext' => '10443',
	'fkSectionBestPractice' => '10432',
	'nControlType' => '0',
	'sName' => 'PO10.11 Project Change Control (Steuerung der �nderung von Projekten)',
	'tDescription' => 'Entwickle ein System zur Steuerung von �nderungen f�r alle Projekte, so dass alle grundlegenden �nderungen am Projekt (zB
Kosten, Zeitplan, Umfang und Qualit�t) angemessen �berpr�ft, freigegeben und, entsprechend der Vorgaben des Programms und
des Projekt-Governance-Frameworks, in den integrierten Projektplan eingearbeitet werden.',
	'tImplementationGuide' => '',
),
71 => array(
	'fkContext' => '10444',
	'fkSectionBestPractice' => '10432',
	'nControlType' => '0',
	'sName' => 'PO10.12 Project Planning of Assurance Methods (Planung von Best�tigungs-Methoden)',
	'tDescription' => 'Identifiziere w�hrend der Projektplanung Best�tigungs-Methoden, die zur Unterst�tzung der Akkreditierung von neuen oder
ge�nderten Systemen ben�tigt werden, und nehme diese in den integrierten Projektplan auf. Die Aufgaben sollten Gewissheit
verschaffen, dass Internal Controls und Sicherheitseigenschaften den festgelegten Anforderungen entsprechen.',
	'tImplementationGuide' => '',
),
72 => array(
	'fkContext' => '10445',
	'fkSectionBestPractice' => '10432',
	'nControlType' => '0',
	'sName' => 'PO10.13 Project Performance Measurement, Reporting and Monitoring (Messung, Berichterstattung und Monitoring der
Projektperformance)',
	'tDescription' => 'Messe die Projektperformance an hand der wesentlichen Projektkriterien (zB Umfang, Zeitplan, Qualit�t, Kosten und Risiken).
Identifiziere s�mtliche Abweichungen vom Plan, beurteile deren Auswirkungen auf das Projekt und das �bergeordnete
Programm; berichte die Ergebnisse an die wesentlichen Stakeholder. Empfehle, implementiere und �berwache ? wo notwendig ?
Verbesserungsma�nahmen entsprechend der Frameworks f�r Programm- und Projektsteuerung.',
	'tImplementationGuide' => '',
),
73 => array(
	'fkContext' => '10446',
	'fkSectionBestPractice' => '10432',
	'nControlType' => '0',
	'sName' => 'PO10.14 Project Closure (Projektabschluss)',
	'tDescription' => 'Fordere, dass am Ende jedes Projektes die Projekt-Stakeholder best�tigen, ob das Projekt die geplanten Ergebnisse und den
geplanten Nutzen erbracht hat. Identifiziere und kommuniziere alle offenen Aktivit�ten, die notwendig sind, um die geplanten
Projektergebnisse und den Nutzen des Programms zu erzielen, und identifiziere und dokumentiere die Lessons-Learned f�r
k�nftige Projekte und Programme.',
	'tImplementationGuide' => '',
),
74 => array(
	'fkContext' => '10449',
	'fkSectionBestPractice' => '10448',
	'nControlType' => '0',
	'sName' => 'AI1.1 Definition and Maintenance of Business Functional and Technical Requirements (Festlegung und Aktualisierung
von funktionalen Gesch�fts- und technischen Erfordernissen)',
	'tDescription' => 'Identifiziere, priorisiere, spezifiziere und vereinbare die funktionalen Gesch�fts- und technischen Erfordernisse, die den vollen
Umfang aller n�tigen Initiativen abdecken, um die vom IT-gest�tzten Investitionsprogramm erwarteten Ergebnisse zu erreichen.
Definiere Kriterien f�r die Abnahme der Anforderungen. Diese Initiativen sollten s�mtliche auf Grund der Art des
Unternehmensgesch�fts, den Gesch�ftsprozessen, der Fertigkeiten und F�higkeiten von Mitarbeitern, Organisationsstrukturen
oder der Basistechnologie erforderlichen �nderungen beinhalten.Die Anforderungen ber�cksichtigen funktionale Erfordernisse des Kerngesch�fts, die technologische Ausrichtung des
Unternehmens, Leistungsf�higkeit, Kosten, Verl�sslichkeit, Kompatibilit�t, Auditierbarkeit, Sicherheit (engl.: security),
Verf�gbarkeit und Kontinuit�t, Ergonomie, Verwendbarkeit (engl.: usability), Betriebssicherheit (engl.: safety) und gesetzliche
Bestimmungen. Entwickle Prozesse, um die Integrit�t, Richtigkeit und Aktualit�t von Unternehmensanforderungen als Basis f�r
die Steuerung der laufenden Systembeschaffung und -entwicklung sicherzustellen und zu steuern. Der Eigent�mer (engl.: owner)
dieser Anforderungen sollte der Business Sponsor sein.',
	'tImplementationGuide' => '',
),
75 => array(
	'fkContext' => '10450',
	'fkSectionBestPractice' => '10448',
	'nControlType' => '0',
	'sName' => 'AI1.2 Risk Analysis Report (Risikoanalyse-Bericht)',
	'tDescription' => 'Identifiziere, dokumentiere und analysiere im Rahmen der Anforderungsdefinition Risiken, die mit den Gesch�ftsprozessen
einhergehen. Risiken beinhalten Gef�hrdungen der Datenintegrit�t, Sicherheit, Verf�gbarkeit, Datenschutz und die Einhaltung
von Gesetzen und Verordnungen. Als Teil der Anforderungen sollten ben�tigte Ma�nahmen f�r Internal Controls und Pr�fspuren
identifiziert werden.',
	'tImplementationGuide' => '',
),
76 => array(
	'fkContext' => '10451',
	'fkSectionBestPractice' => '10448',
	'nControlType' => '0',
	'sName' => 'AI1.3 Feasibility Study and Formulation of Alternative Courses of Action (Machbarkeitsstudie und Formulierung von
alternativen Umsetzungsm�glichkeiten)',
	'tDescription' => 'F�hre eine Machbarkeitsstudie durch, die die M�glichkeit der Implementierung der Anforderungen pr�ft. Darin sollten
alternative Vorgehensweisen f�r Software, Hardware, Services und F�higkeiten identifiziert werden, welche die festgelegten
funktionalen Gesch�fts- und technischen Erfordernisse erf�llen. Ebenso sollte die technologische und wirtschaftliche
Machbarkeit (Analyse von potentiellen Kosten und Nutzen) jeder identifizierten Alternative im Zusammenhang mit dem ITgest�tzten
Investitionsprogramm evaluiert werden. Als Folge der Beurteilung von Faktoren wie �nderungen an
Gesch�ftsprozessen, Technologie und F�higkeiten k�nnen bei der Entwicklung der Machbarkeitsstudie mehrere Iterationen
notwendig sein. Mit Unterst�tzung der IT-Organisation soll das Management der Kernprozesse die Machbarkeitsstudie sowie die
alternativen Vorgehensweisen bewerten und eine Empfehlung an den Auftraggeber (engl.: business sponsor) abgeben.',
	'tImplementationGuide' => '',
),
77 => array(
	'fkContext' => '10452',
	'fkSectionBestPractice' => '10448',
	'nControlType' => '0',
	'sName' => 'AI1.4 Requirements and Feasibility Decision and Approval (Freigabe der Anforderungsdefinition und Machbarkeit)',
	'tDescription' => 'Der Auftraggeber (engl.: business sponsor) genehmigt und unterzeichnet entsprechend der vorab definierten Phasen die
funktionalen Gesch�fts- und technischen Anforderungen sowie die Ergebnisse der Machbarkeitsstudie. Jede Freigabe folgt auf
Basis der erfolgreichen Beendigung von Qualit�tsreviews. Der Auftraggeber trifft die endg�ltige Entscheidung hinsichtlich der
L�sungsauswahl und Beschaffungsansatz.',
	'tImplementationGuide' => '',
),
78 => array(
	'fkContext' => '10454',
	'fkSectionBestPractice' => '10453',
	'nControlType' => '0',
	'sName' => 'AI2.1 High-level Design (Grobdesign)',
	'tDescription' => '�berf�hre Unternehmenserfordernisse in eine grobe Designspezifikation f�r die Softwareentwicklung unter Ber�cksichtigung der
technologischen Ausrichtung der Organisation sowie der Informationsarchitektur. Lasse die Designspezifikation genehmigen, um
sicherzustellen, dass das Grobdesign den Anforderungen entspricht.',
	'tImplementationGuide' => '',
),
79 => array(
	'fkContext' => '10455',
	'fkSectionBestPractice' => '10453',
	'nControlType' => '0',
	'sName' => 'AI2.2 Detailed Design (Detailliertes Design)',
	'tDescription' => 'Erstelle ein detailliertes Design und technische Software-Anforderungen an die Anwendung. Definiere Abnahmekriterien f�r die
Anforderungen. Lasse die Anforderungen abnehmen, um sicherzugehen, dass sie dem Grobdesign entsprechen. Hierbei zu
ber�cksichtigende Aspekte sind unter anderem: Festlegung und Dokumentation von Eingabeerfordernissen,
Schnittstellendefinition, Benutzerschnittstelle, Design der Sammlung von Quelldaten, Anwendungsspezifikation, Festlegung und
Dokumentation von Dateianforderungen, Verarbeitungserfordernisse, Definition der Anforderungen f�r Ausgaben, Steuerung
und Auditierbarkeit, Sicherheit und Verf�gbarkeit sowie Test. F�hre eine neuerliche Bewertung durch, wenn w�hrend der
Entwicklung oder Wartung wesentliche technische oder logische �nderungen auftreten',
	'tImplementationGuide' => '',
),
80 => array(
	'fkContext' => '10456',
	'fkSectionBestPractice' => '10453',
	'nControlType' => '0',
	'sName' => 'AI2.3 Application Control and Auditability (Anwendungskontrollen und Nachvollziehbarkeit)',
	'tDescription' => 'Stelle sicher, dass Unternehmenskontrollen angemessen in Anwendungskontrollen �bergeleitet werden, sodass die Verarbeitung
richtig, vollst�ndig, zeitgerecht, autorisiert und nachvollziehbar erfolgt. Dabei speziell zu ber�cksichtigende Themen sind wie
Autorisierungsmechanismen, Integrit�t von Informationen, Zugriffsschutz, Backup und der Entwurf der Pr�fspur.',
	'tImplementationGuide' => '',
),
81 => array(
	'fkContext' => '10457',
	'fkSectionBestPractice' => '10453',
	'nControlType' => '0',
	'sName' => 'AI2.4 Application Security and Availability (Sicherheit und Verf�gbarkeit der Anwendung)',
	'tDescription' => 'Behandle Anforderungen an Sicherheit und Verf�gbarkeit der Anwendung in Bezug auf identifizierte Risiken, unter
Ber�cksichtigung der Datenklassifikation, der Informationssicherheitarchitektur der Organisation und dem Risikoprofil.
Ber�cksichtige dabei unter anderem Aspekte wie Zugriffsberechtigungen und Rechtemanagement, den Schutz sensitiver
Informationen auf allen Ebenen, Authentisierung und Transaktionsintegrit�t sowie automatische Wiederherstellung.',
	'tImplementationGuide' => '',
),
82 => array(
	'fkContext' => '10458',
	'fkSectionBestPractice' => '10453',
	'nControlType' => '0',
	'sName' => 'AI2.5 Configuration and Implementation of Acquired Application Software (Konfiguration und Implementierung von
beschaffter Anwendungssoftware)',
	'tDescription' => '�ndere und implementiere zugekaufte, automatisierte Funktionen unter Anwendung der Verfahren f�r Konfiguration, Abnahme
und Test. Zu ber�cksichtigende Gesichtspunkte sind: Validierung gegen�ber Vertragsbedingungen, die Informationsarchitektur
der Organisation, bestehende Anwendungen, Interoperabilit�t mit bestehende Anwendungen und Datenbanksystemen,
Systemperformance, Dokumentation und Benutzerhandb�cher, Pl�ne f�r Integrations und Systemtests.',
	'tImplementationGuide' => '',
),
83 => array(
	'fkContext' => '10459',
	'fkSectionBestPractice' => '10453',
	'nControlType' => '0',
	'sName' => 'AI2.6 Major Upgrades to Existing Systems (Wesentliche Upgrades bestehender Systeme)',
	'tDescription' => 'Im Falle von wesentlichen Upgrades vorhandener Systeme, die in signifikanten �nderungen im derzeitigen Design und/oder der
Funktionalit�t resultieren, folge einem �hnlichen Prozess wie f�r die Entwicklung neuer Systeme. Ber�cksichtige
Auswirkungsanalyse, Kosten-/Nutzenanalyse und Anforderungsmanagement.',
	'tImplementationGuide' => '',
),
84 => array(
	'fkContext' => '10460',
	'fkSectionBestPractice' => '10453',
	'nControlType' => '0',
	'sName' => 'AI2.7 Development of Application Software (Entwicklung von Anwendungssoftware)',
	'tDescription' => 'Stelle sicher, dass automatisierte Funktionalit�t entsprechend der Designspezifikationen, Entwicklungs- und
Dokumentationsstandards und Qualit�tsanforderungen entwickelt wird. Best�tige und beschlie�e jede wichtige Phase im
Software-Entwicklungsprozess nach erfolgreichen Reviews von Funktionalit�t, Leistung und Qualit�t. Ber�cksichtige hierbei: die
Best�tigung, dass die Designspezifikationen mit den gesch�ftlichen, funktionalen und technischen Erfordernissen �bereinstimmt;
die Freigabe von Change-Requests; sowie die Best�tigung, dass die Anwendungssoftware mit vorhandenen Produktionssystemen
kompatibel und f�r eine Migration bereit ist. Stelle au�erdem sicher, dass alle rechtlichen und vertraglichen Aspekte f�r durch
Dritte entwickelte Anwendungssoftware identifiziert und behandelt werden.',
	'tImplementationGuide' => '',
),
85 => array(
	'fkContext' => '10461',
	'fkSectionBestPractice' => '10453',
	'nControlType' => '0',
	'sName' => 'AI2.8 Software Quality Assurance (Software-Qualit�tssicherung)',
	'tDescription' => 'Entwickle einen Softwarequalit�tssicherungsplan, stelle ben�tigte Ressourcen bereit und setze den Plan um, um die in der
Anforderungsdefinition und den Qualit�tsrichtlinien und Verfahren der Organisation festgelegte Qualit�t zu erreichen. Beachte
im Qualit�tssicherungsplan die Spezifikation von Qualit�tskriterien sowie einen Validierungs- und Verifikationsprozess, der auch
Inspektion, Walkthroughs und Testen beinhaltet.',
	'tImplementationGuide' => '',
),
86 => array(
	'fkContext' => '10462',
	'fkSectionBestPractice' => '10453',
	'nControlType' => '0',
	'sName' => 'AI2.9 Application Requirements Management (Management von Anforderungen an die Anwendung)',
	'tDescription' => 'Stelle sicher, dass w�hrend Entwurf, Entwicklung und Implementierung der Status jeder Anforderung (einschlie�lich der
abgelehnten Anforderungen) nachvollzogen werden kann und �nderungen von Anforderungen in einem etablierten Change-
Management-Prozess genehmigt werden.',
	'tImplementationGuide' => '',
),
87 => array(
	'fkContext' => '10463',
	'fkSectionBestPractice' => '10453',
	'nControlType' => '0',
	'sName' => 'AI2.10 Application Software Maintenance (Wartung von Anwendungssoftware)',
	'tDescription' => 'Entwickle eine Strategie und einen Plan f�r Wartung und Release von Software. Beachte dabei unter anderem Releaseplanung
und -steuerung, Ressourcenplanung, Fehlerbehandlung und -behebung (engl: bugfixing und fault correction), geringf�gige
Verbesserungen, Pflege der Dokumentation, Notfalls-Changes, Interdependenzen mit anderen Anwendungsprogrammen und
Infrastruktur, Strategien f�r Upgrades, vertragliche Konditionen wie Support und Upgrades, periodische Reviews gegen�ber den
Unternehmensanforderungen, Risiken und Sicherheitsanforderungen.',
	'tImplementationGuide' => '',
),
88 => array(
	'fkContext' => '10465',
	'fkSectionBestPractice' => '10464',
	'nControlType' => '0',
	'sName' => 'AI3.1 Technological Infrastructure Acquisition Plan (Beschaffungsplan f�r technologische Infrastruktur)',
	'tDescription' => 'Entwickle einen Plan f�r die Beschaffung, Implementierung und Wartung der technologischen Infrastruktur, der die bestehenden
funktionalen Gesch�fts- und technischen Anforderungen erf�llt und im Einklang mit der unternehmensweiten technologischen
Richtung steht. Der Plan sollte k�nftige Flexibilit�t zur Kapazit�tserweiterungen, Kosten f�r den �bergang, technische Risiken
und die Gesamtausgaben �ber den Lebenszyklus von Technologie-Upgrades umfassen. Beurteile bei Einsatz von neueren
technischen M�glichkeiten deren Komplexit�tskosten und die wirtschaftliche Stabilit�t des Anbieters und Produktes.',
	'tImplementationGuide' => '',
),
89 => array(
	'fkContext' => '10466',
	'fkSectionBestPractice' => '10464',
	'nControlType' => '0',
	'sName' => 'AI3.2 Infrastructure Resource Protection and Availability (Schutz und Verf�gbarkeit von Infrastrukturressourcen)',
	'tDescription' => 'Implementiere Ma�nahmen zur Internal Control, Sicherheit und Pr�fbarkeit w�hrend der Konfiguration, Integration und Wartung
von Hardware und Infrastruktur-Software, um Ressourcen zu sch�tzen und Verf�gbarkeit und Integrit�t sicher zu stellen. Die
Verantwortung f�r die Verwendung von empfindlichen Infrastrukturkomponenten sollten klar festgelegt und von denen
verstanden werden, die Infrastrukturkomponenten entwickeln und integrieren. Die Verwendung sollte gemonitort und evaluiert
werden.',
	'tImplementationGuide' => '',
),
90 => array(
	'fkContext' => '10467',
	'fkSectionBestPractice' => '10464',
	'nControlType' => '0',
	'sName' => 'AI3.3 Infrastructure Maintenance (Wartung von Infrastruktur)',
	'tDescription' => 'Entwickle eine Strategie und einen Plan f�r die Wartung der Infrastruktur und stelle sicher, dass Changes entsprechend des
unternehmensweiten Change-Management-Prozesses gesteuert ablaufen. Ber�cksichtige regelm��ige Reviews an Hand des
Unternehmensbedarfs, Strategien f�r Patch-Management und Upgrade, Risiken, Verletzbarkeitsanalysen und
Sicherheitsanforderungen.',
	'tImplementationGuide' => '',
),
91 => array(
	'fkContext' => '10468',
	'fkSectionBestPractice' => '10464',
	'nControlType' => '0',
	'sName' => 'AI3.4 Feasibility Test Environment (Testumgebung)',
	'tDescription' => 'Etabliere eine Entwicklungs- und Testumgebung, um in fr�hen Stadien des Beschaffungs- und Entwicklungsprozesses wirksame
und wirtschaftliche Machbarkeits- und Integrationstests f�r Anwendungen und Infrastrukturen zu unterst�tzen. Ber�cksichtige
Funktionalit�t, Hard- und Softwarekonfiguration, Integrations- und Performancetests, Migration zwischen den Umgebungen,
Versionskontrolle, Werkzeuge und Daten f�r Tests sowie Sicherheit.',
	'tImplementationGuide' => '',
),
92 => array(
	'fkContext' => '10470',
	'fkSectionBestPractice' => '10469',
	'nControlType' => '0',
	'sName' => 'AI4.1 Planning for Operational Solutions (Planung f�r operative L�sungen)',
	'tDescription' => 'Entwickle einen Plan, um als Ergebnis einer Einf�hrung oder eines Upgrades eines automatisierten Systems oder von
Infrastruktur, alle technischen Aspekte, betrieblichen M�glichkeiten und erforderlichen Service Levels zu identifizieren und zu
dokumentieren, damit alle Stakeholder fr�hzeitig die Verantwortung f�r die Erstellung von Verfahren f�r das Management, f�r
User und f�r den Betrieb �bernehmen k�nnen.',
	'tImplementationGuide' => '',
),
93 => array(
	'fkContext' => '10471',
	'fkSectionBestPractice' => '10469',
	'nControlType' => '0',
	'sName' => 'AI4.2 Knowledge Transfer to Business Management (Transfer von Knowledge an den Fachbereich)',
	'tDescription' => 'Transferiere an das Fachbereichsmanagement Wissen, um ihm zu erm�glichen, die Eigent�merschaft �ber die Anwendung und
Daten zu �bernehmen und die Verantwortung f�r die Leistungserbringung und -qualit�t, Internal Control und
Administrationsprozesse der Anwendung zu �bernehmen. Der Wissenstransfer soll Freigaben f�r den Zugriff, Rechteverwaltung,
Funktionstrennung, automatisierte Gesch�ftskontrollen, Backup und Recovery, physische Sicherheit und Archivierung von
Urbelegen umfassen.',
	'tImplementationGuide' => '',
),
94 => array(
	'fkContext' => '10472',
	'fkSectionBestPractice' => '10469',
	'nControlType' => '0',
	'sName' => 'AI4.3 Knowledge Transfer to End Users (Transfer von Knowledge and Endbenutzer)',
	'tDescription' => 'Transferiere an Endbenutzer Wissen und Fertigkeiten, um ihnen die wirksame und wirtschaftliche Verwendung der Anwendung
zur Unterst�tzung der Gesch�ftsprozesse zu erm�glichen. Der Wissenstransfer sollte die Entwicklung eines Trainingsplan f�r
erstmalige und laufende Schulung und die Entwicklung der Fertigkeiten, Schulungsmaterialien, Benutzerhandb�cher,
Verfahrenshandb�cher, Online-Hilfe, Unterst�tzung durch den Service Desk, Identifikation von Key-Usern und Evaluation
umfassen.',
	'tImplementationGuide' => '',
),
95 => array(
	'fkContext' => '10473',
	'fkSectionBestPractice' => '10469',
	'nControlType' => '0',
	'sName' => 'AI4.4 Knowledge Transfer to Operations and Support Staff (Transfer von Knowledge an Betriebs- und
Supportmitarbeiter)',
	'tDescription' => 'Transferiere an den Betrieb und den technischen Supportmitarbeiter Wissen, um ihnen die wirksame und wirtschaftliche
Bereitstellung, Unterst�tzung und Wartung der Anwendung und der korrespondierenden Infrastruktur zu erm�glichen, die den
erforderlichen Service Levels entspricht. Der Wissenstransfer soll die Entwicklung eines Trainingsplans f�r erstmalige und
laufende Schulung und die Entwicklung der Fertigkeiten, Schulungsmaterialien, Betriebshandb�cher, Verfahrenshandb�cher
sowie Szenarien f�r den Service Desk umfassen.',
	'tImplementationGuide' => '',
),
96 => array(
	'fkContext' => '10475',
	'fkSectionBestPractice' => '10474',
	'nControlType' => '0',
	'sName' => 'AI5.1 Procurement Control (Steuerung der Beschaffung)',
	'tDescription' => 'Entwickle und befolge Verfahren und Standards, die mit dem unternehmensweit g�ltigen Einkaufsprozess und der
Beschaffungsstrategie �bereinstimmen, um sicher zu stellen, dass die Beschaffung von IT-bezogener Infrastruktur,
Einrichtungen, Hardware, Software und Dienstleistungen die Unternehmenserfordernisse erf�llt.',
	'tImplementationGuide' => '',
),
97 => array(
	'fkContext' => '10476',
	'fkSectionBestPractice' => '10474',
	'nControlType' => '0',
	'sName' => 'AI5.2 Supplier Contract Management (Vertragsmanagement f�r Lieferanten)',
	'tDescription' => 'Erstelle ein Verfahren f�r die Festlegung, �nderung und Beendigung von Vertr�gen f�r alle Lieferanten. Das Verfahren sollte im
Minimum rechtliche, finanzielle, organisatorische, dokumentarische, Performance-, Sicherheits-, Urheberrechts- und
K�ndigungsverantwortlichkeiten und Haftung (inklusive Konventionalstrafen) abdecken. Alle Vertr�ge und Vertrags�nderungen
sollten durch Rechtsberater �berpr�ft werden.',
	'tImplementationGuide' => '',
),
98 => array(
	'fkContext' => '10477',
	'fkSectionBestPractice' => '10474',
	'nControlType' => '0',
	'sName' => 'AI5.3 Supplier Selection (Lieferantenauswahl)',
	'tDescription' => 'W�hle Lieferanten entsprechend einer fairen und formalen Praktik aus, um eine optimale Eignung sicher zu stellen gem�ss der
Anforderungen, welche auf Basis von Inputs potentieller Lieferanten entwickelt und mit Kunden und Lieferanten vereinbart
wurden.',
	'tImplementationGuide' => '',
),
99 => array(
	'fkContext' => '10478',
	'fkSectionBestPractice' => '10474',
	'nControlType' => '0',
	'sName' => 'AI5.4 Software Acquisition (Softwarebeschaffung)',
	'tDescription' => 'Stelle sicher, dass die Interessen der Organisation bei allen vertraglichen Beschaffungsvereinbarungen gesch�tzt sind.
Ber�cksichtige in den Vertragsbedingungen f�r die Beschaffung der Software die Rechte und Pflichten aller Parteien, die in der
Lieferung und der laufenden Verwendung der Software involviert sind und setze diese durch. Diese Rechte und Pflichten k�nnen
Eigentum und Lizenzierung von geistigem Eigentum, Wartung, Gew�hrleistung, Schiedsgerichtsverfahren, Bedingungen f�r
Upgrades und die Tauglichkeit zum Gebrauch, wie Sicherheit, Hinterlegung und Recht auf Zugriffe umfassen.',
	'tImplementationGuide' => '',
),
100 => array(
	'fkContext' => '10480',
	'fkSectionBestPractice' => '10479',
	'nControlType' => '0',
	'sName' => 'AI6.1 Change Standards and Procedures (Standards und Verfahren f�r Changes)',
	'tDescription' => 'Erstelle formelle Change-Management-Verfahren, um in geregelter Weise alle Anfragen (inklusive Wartung und Patches) f�r
Changes an Anwendungen, Verfahren, Prozessen, System- oder Serviceparametern sowie an Basisplattformen zu behandeln.',
	'tImplementationGuide' => '',
),
101 => array(
	'fkContext' => '10481',
	'fkSectionBestPractice' => '10479',
	'nControlType' => '0',
	'sName' => 'AI6.2 Impact Assessment, Prioritisation and Authorisation (Bewertung von Auswirkungen, Priorisierung und Freigabe)',
	'tDescription' => 'Stelle sicher, dass alle Anfragen f�r Changes in einer strukturierten Art und Weise auf deren Auswirkungen auf die operativen
Systeme und deren Funktionalit�t hin beurteilt werden. Diese Beurteilung sollte eine Kategorisierung und Priorisierung der
Changes umfassen. Vor der Migration in die Produktion werden Changes durch die jeweiligen Stakeholder genehmigt.',
	'tImplementationGuide' => '',
),
102 => array(
	'fkContext' => '10482',
	'fkSectionBestPractice' => '10479',
	'nControlType' => '0',
	'sName' => 'AI6.3 Emergency Changes (Notfalls-Changes)',
	'tDescription' => 'Erstelle eine Prozess f�r die Definition, Aufnahme, Beurteilung und Genehmigung von Notfalls-Changes, die nicht dem
bestehenden Change-Prozess folgen. Dokumentation und Tests sollten durchgef�hrt werden, auch nach der Implementierung des
Notfalls-Changes.',
	'tImplementationGuide' => '',
),
103 => array(
	'fkContext' => '10483',
	'fkSectionBestPractice' => '10479',
	'nControlType' => '0',
	'sName' => 'AI6.4 Change Status Tracking and Reporting (Statusverfolgung und Berichterstattung)',
	'tDescription' => 'Erstelle ein Nachverfolgungs- und Reportingsystem, um Anfordernde und die entsprechenden Stakeholder �ber den Status der
�nderung an Anwendungen, Verfahren, Prozessen, System- oder Serviceparametern sowie an den Basisplattformen informiert zu
halten.',
	'tImplementationGuide' => '',
),
104 => array(
	'fkContext' => '10484',
	'fkSectionBestPractice' => '10479',
	'nControlType' => '0',
	'sName' => 'AI6.5 Change Closure and Documentation (Abschluss und Dokumentation von Changes)',
	'tDescription' => 'Sobald System-Changes umgesetzt sind, aktualisiere die betreffende System- und Benutzerdokumentation sowie die Verfahren
entsprechend. Erstelle einen Review-Prozess, um die vollst�ndige Umsetzung der Changes sicher zu stellen.',
	'tImplementationGuide' => '',
),
105 => array(
	'fkContext' => '10486',
	'fkSectionBestPractice' => '10485',
	'nControlType' => '0',
	'sName' => 'AI7.1 Training (Schulung)',
	'tDescription' => 'Schule ? als Teil jedes Entwicklungs-, Implementierungs- oder �nderungsprojektes ? Mitarbeiter der betroffenen Abteilungen
und das Betriebspersonal der IT in Einklang mit den festgelegten Schulungs- und Implementierungspl�nen und den
entsprechenden Unterlagen.',
	'tImplementationGuide' => '',
),
106 => array(
	'fkContext' => '10487',
	'fkSectionBestPractice' => '10485',
	'nControlType' => '0',
	'sName' => 'AI7.2 Test Plan (Testplan)',
	'tDescription' => 'Erstelle einen Testplan und hole das Einverst�ndnis der relevanten Parteien ein. Der Testplan basiert auf unternehmensweiten
Standards und definiert Rollen, Verantwortlichkeiten und Erfolgskriterien. Der Plan ber�cksichtigt die Testvorbereitung
(inklusive Vorbereitung notwendiger R�umlichkeiten), Schulungserfordernisse, Installation oder Update einer definierten
Testumgebung, der Planung, Umsetzung, Dokumentation und Aufbewahrung von Testf�llen, Fehlerbehandlung und -korrektur
sowie formelle Abnahme. Basierend auf der Beurteilung des Risikos von Systemfehlern oder St�rungen bei der
Implementierung, sollte der Plan die Erfordernis f�r Leistungs-, Stress-, Usability-, Pilot- und Sicherheitstests umfassen.',
	'tImplementationGuide' => '',
),
107 => array(
	'fkContext' => '10488',
	'fkSectionBestPractice' => '10485',
	'nControlType' => '0',
	'sName' => 'AI7.3 Implementation Plan (Implementierungsplan)',
	'tDescription' => 'Erstelle einen Implementierungsplan und hole das Einverst�ndnis von den relevanten Parteien ein. Der Plan definiert das
Release-Design, den Aufbau von Release-Paketen, Verfahren f�r Roll-out/Installation, Umgang mit Ereignissen, Steuerung der
Verteilung (inklusive Werkzeuge), Speicherung von Software, Review des Release und die Dokumentation von Changes. Der
Plan sollte auch Vorkehrungen f�r eine R�cksetzung (engl.: fallback/backout) enthalten.',
	'tImplementationGuide' => '',
),
108 => array(
	'fkContext' => '10489',
	'fkSectionBestPractice' => '10485',
	'nControlType' => '0',
	'sName' => 'AI7.4 Test Environment (Testumgebung)',
	'tDescription' => 'Erstelle eine separate Testumgebung f�r Tests. Diese Umgebung sollte der k�nftigen Betriebsumgebung entsprechen (zB
�hnliche Sicherheit, Internal Controls und Workloads), um sinnvolles Testen zu erm�glichen. Verfahren sollen vorhanden sein,
um sicher zu stellen, dass die in der Testumgebung verwendeten Daten den sp�ter in der Produktivumgebung verwendeten Daten
entsprechen und, wo n�tig, anonymisiert sind. Ergreife angemessene Ma�nahmen, um die Ver�ffentlichung sensitiver Testdaten
zu verhindern. Das dokumentierte Testergebnis sollte aufbewahrt werden.',
	'tImplementationGuide' => '',
),
109 => array(
	'fkContext' => '10490',
	'fkSectionBestPractice' => '10485',
	'nControlType' => '0',
	'sName' => 'AI7.5 System and Data Conversion (System- und Datenkonvertierung)',
	'tDescription' => 'Stelle sicher, dass die Entwicklungsmethoden der Organisation f�r alle Entwicklungs-, Implementierungs- oder
�nderungsprojekte alle notwendigen Bestandteile, wie Hardware, Software, Transaktionsdaten, Stammdaten, Backups und
Archive, Schnittstellen mit anderen Systemen, Verfahren, Systemdokumentation etc vom Altsystem in das Neusystem
entsprechend eines vorher entwickelten Plans �berf�hrt werden. Eine Pr�fspur von Ergebnissen vor und nach der Konvertierung
sollte entwickelt und aufbewahrt werden. Eine detaillierte Verifikation der ersten Verarbeitung des neuen Systems sollte durch
die Systemeigner durchgef�hrt werden, um die erfolgreiche �berf�hrung zu best�tigen.',
	'tImplementationGuide' => '',
),
110 => array(
	'fkContext' => '10491',
	'fkSectionBestPractice' => '10485',
	'nControlType' => '0',
	'sName' => 'AI7.6 Testing of Changes (Test von Changes)',
	'tDescription' => 'Stelle sicher, dass Changes entsprechend der definierten Abnahmepl�ne getestet werden, die auf einer Beurteilung von
Auswirkungen und Ressourcenbedarf basieren. Diese Beurteilung ber�cksichtigt die Bestimmung der notwendigen Performance
in einer separaten Testumgebung durch eine (von Entwicklern) unabh�ngige Testgruppe, bevor eine Verwendung in der
Normalbetriebsumgebung erfolgt. Die Sicherheitsma�nahmen sollten vor der Auslieferung getestet werden, so dass die
Wirksamkeit der Sicherheit zertifiziert werden kann. Pl�ne f�r eine R�cksetzung (engl.: fallback/backout plan) sollten vor der
Umsetzung des Change in die Produktion entwickelt und getestet werden.',
	'tImplementationGuide' => '',
),
111 => array(
	'fkContext' => '10492',
	'fkSectionBestPractice' => '10485',
	'nControlType' => '0',
	'sName' => 'AI7.7 Final Acceptance Test (Abschlie�ender Akzeptanztest)',
	'tDescription' => 'Stelle sicher, dass Verfahren, als Teil der Abnahme oder abschlie�enden Qualit�tssicherung neuer oder modifizierter
Informationssysteme, eine formale Evaluierung und Freigabe der Testergebnisse durch das Management der betroffenen User-
Abteilung(en) und der IT vorsehen. Die Tests sollten alle Komponenten des Informationssystems (zB Anwendungssoftware,
Einrichtungen, Technologie und User-Verfahren) umfassen und sicherstellen, dass die Anforderungen an die
Informationssicherheit durch alle Komponenten eingehalten werden. Die Testdaten sollten als Pr�fspur und f�r k�nftige Tests
aufbewahrt werden.',
	'tImplementationGuide' => '',
),
112 => array(
	'fkContext' => '10493',
	'fkSectionBestPractice' => '10485',
	'nControlType' => '0',
	'sName' => 'AI7.8 Promotion to Production (Produktivstellung)',
	'tDescription' => 'Erstelle formale und dem Umsetzungsplan entsprechende Verfahren zur Steuerung der �bergabe des Systems von der
Entwicklung zum Test und dann in die Produktion. Das Management sollte verlangen, dass das Einverst�ndnis des Systemeigners
eingeholt wird, bevor eine neue Anwendung in die Produktion verschoben wird, und dass das neue System erfolgreich die Tages-
, Monats-, Quartals- und Jahresendverarbeitung durchgef�hrt hat, bevor das Altsystem au�er Betrieb genommen wird.',
	'tImplementationGuide' => '',
),
113 => array(
	'fkContext' => '10494',
	'fkSectionBestPractice' => '10485',
	'nControlType' => '0',
	'sName' => 'AI7.9 Post-implementation Review (Post-Implementation Review)',
	'tDescription' => 'Entwickle, in �bereinstimmung mit dem unternehmensweiten Entwicklungs- und Change-Standards, Verfahren, die einen Post
Implementation Review der operativen Systeme verlangen, um zu bewerten und zu berichten, ob der Change auf die
kostenwirksamste Art die Kundenanforderungen erf�llt und den vorgesehenen Nutzen erbracht hat.',
	'tImplementationGuide' => '',
),
114 => array(
	'fkContext' => '10497',
	'fkSectionBestPractice' => '10496',
	'nControlType' => '0',
	'sName' => 'DS1.1 Service Level Management Framework',
	'tDescription' => 'Definiere ein Framework, das einen formalisierten Service Level Management-Prozess zwischen Kunden und dem
Dienstleistungsanbieter zur Verf�gung stellt. Das Framework erh�lt die laufende Anpassung mit den
Unternehmenserfordernissen und -priorit�ten aufrecht und unterst�tzt das gemeinsame Verst�ndnis von Kunden und
Dienstleister(n). Das Framework umfasst Prozesse f�r die Erstellung von Anforderungen an Services, Definition von Services,
Service Level Agreements (SLAs), Operating Level Agreements (OLAs) und Finanzierungsmittel. Diese Attribute sind in einem
Service-Katalog gesammelt. Das Framework definiert die organisatorische Struktur f�r das Service Level Management, die aus
Rollen, Aufgaben und Verantwortlichkeiten von internen und externen Leistungsanbietern sowie von Kunden besteht.',
	'tImplementationGuide' => '',
),
115 => array(
	'fkContext' => '10498',
	'fkSectionBestPractice' => '10496',
	'nControlType' => '0',
	'sName' => 'DS1.2 Definition of Services (Definition von Services)',
	'tDescription' => 'St�tze die Definition von IT-Services auf die Charakteristika der Services und die Unternehmensanforderungen, die zentral durch
die Anwendung eines Service-Katalogs oder Service-Portfolios verwaltet und gespeichert sind.',
	'tImplementationGuide' => '',
),
116 => array(
	'fkContext' => '10499',
	'fkSectionBestPractice' => '10496',
	'nControlType' => '0',
	'sName' => 'DS1.3 Service Level Agreements (Service Level Agreements)',
	'tDescription' => 'Definiere und vereinbare, basierend auf Kundenanforderungen und F�higkeiten der IT, Service Level Agreements f�r alle
entscheidenden IT-Services. Diese decken Kundenverpflichtungen, Unterst�tzungsanforderungen, quantitative und qualitative
Messgr��en zur Messung des mit den Stakeholdern vereinbarten Service, finanzielle und wirtschaftliche Vereinbarungen (falls
anwendbar) sowie Rollen und Verantwortlichkeiten, inklusive Beaufsichtigung des SLA. Zu ber�cksichtigende Einzelheiten sind
Rahmenbedingungen f�r Verf�gbarkeit, Verl�sslichkeit, Performance, Wachstumsf�higkeiten, Support-Levels,
Kontinuit�tsplanung, Sicherheit und Nachfrage.',
	'tImplementationGuide' => '',
),
117 => array(
	'fkContext' => '10500',
	'fkSectionBestPractice' => '10496',
	'nControlType' => '0',
	'sName' => 'DS1.4 Operating Level Agreements (Operating Level Agreements)',
	'tDescription' => 'Stelle sicher, dass Operating Level Agreements beschreiben, wie die Services technisch bereitgestellt werden, um die SLA(s)
optimal zu unterst�tzen. Die OLAs spezifizieren die technischen Prozesse in einer f�r den Anbieter verst�ndlichen Weise und
k�nnen mehrere SLAs unterst�tzen.',
	'tImplementationGuide' => '',
),
118 => array(
	'fkContext' => '10501',
	'fkSectionBestPractice' => '10496',
	'nControlType' => '0',
	'sName' => 'DS1.5 Monitoring and Reporting of Service Level Achievements (Monitoring und Berichterstattung der Erreichung von
Service Levels)',
	'tDescription' => 'Monitore kontinuierlich festgelegte Kriterien der Service Levels Performance. Berichte �ber die Erreichung der Service Levels
werden in einem f�r Stakeholder verst�ndlichen Format erstellt. Die �berwachungsstatistiken werden analysiert und verfolgt, um
negative und positive Trends f�r einzelne Services sowie die gesamten Services zu identifizieren.',
	'tImplementationGuide' => '',
),
119 => array(
	'fkContext' => '10502',
	'fkSectionBestPractice' => '10496',
	'nControlType' => '0',
	'sName' => 'DS1.6 Review of Service Level Agreements and Contracts (Review von Service Level Agreements und Underpinning
Contracts)',
	'tDescription' => 'Reviewe regelm��ig Service Level Agreements und Underpinning Contracts mit internen und externen Leistungsanbietenden,
um sicherzustellen, dass diese wirksam und aktuell sind und dass �nderungen der Anforderungen ber�cksichtigt wurden.',
	'tImplementationGuide' => '',
),
120 => array(
	'fkContext' => '10504',
	'fkSectionBestPractice' => '10503',
	'nControlType' => '0',
	'sName' => 'DS2.1 Identification of All Supplier Relationships (Identifikation aller Beziehungen mit Lieferanten)',
	'tDescription' => 'Identifiziere alle Leistungen von Lieferanten und kategorisiere sie entsprechend Lieferantentyp, Bedeutung und Kritikalit�t.
Unterhalte formelle Dokumentation technischer und organisatorischer Beziehungen mit Rollen und Verantwortlichkeiten, Zielen,
erwarteten Leistungen und Empfehlungsschreiben von Beauftragten dieser Lieferanten.',
	'tImplementationGuide' => '',
),
121 => array(
	'fkContext' => '10505',
	'fkSectionBestPractice' => '10503',
	'nControlType' => '0',
	'sName' => 'DS2.2 Supplier Relationship Management (Lieferanten-Beziehungsmanagement)',
	'tDescription' => 'Formalisiere den Prozess des Beziehungsmanagements f�r alle Lieferanten. Der Beziehungsverantwortliche muss eine
Verbindung zwischen Kunde und Lieferant herstellen und sicherstellen, dass die Beziehung auf Vertrauen und Transparenz
basiert (zB durch Service Level Agreements).',
	'tImplementationGuide' => '',
),
122 => array(
	'fkContext' => '10506',
	'fkSectionBestPractice' => '10503',
	'nControlType' => '0',
	'sName' => 'DS2.3 Supplier Risk Management (Lieferanten-Risikomanagement)',
	'tDescription' => 'Identifiziere und behandle Risiken bez�glich der F�higkeit des Lieferanten, weiterhin wirksam Leistungen in einer sicheren und
wirtschaftlichen Weise auf einer konstanten Basis zu erbringen. Stelle sicher, dass Vertr�ge sich nach den allgemeing�ltigen
Unternehmensstandards richten und in �bereinstimmung stehen mit rechtlichen und regulativen Anforderungen. Das
Risikomanagement sollte auch Geheimhaltungsvereinbarungen (engl.: non-disclosure agreements (NDAs)),
Hinterlegungsvertr�ge, andauernde �berlebensf�higkeit des Anbieters, Konformit�t mit Sicherheitserfordernissen, alternative
Lieferanten, Konventionalstrafen und Boni, etc ber�cksichtigen.',
	'tImplementationGuide' => '',
),
123 => array(
	'fkContext' => '10507',
	'fkSectionBestPractice' => '10503',
	'nControlType' => '0',
	'sName' => 'DS2.4 Supplier Performance Monitoring (Monitoring der Performance von Lieferanten)',
	'tDescription' => 'Etabliere einen Prozess, um die Leistungserbringung zu �berwachen, um sicherzustellen, dass der Lieferant die bestehenden
Unternehmenserfordernisse erf�llt und weiterhin das Vertragswerk und die Service Level Agreements einh�lt und dass die
Leistungen konkurrenzf�hig sind mit alternativen Anbietern und den Marktverh�ltnissen sind.',
	'tImplementationGuide' => '',
),
124 => array(
	'fkContext' => '10509',
	'fkSectionBestPractice' => '10508',
	'nControlType' => '0',
	'sName' => 'DS3.1 Performance and Capacity Planning (Planung von Performance und Kapazit�t)',
	'tDescription' => 'Richte einen Planungsprozess zur �berpr�fung der Performance und Kapazit�t von IT-Ressourcen ein, um sicherzustellen, dass
kostenm��ig begr�ndbare Kapazit�t und Performance verf�gbar ist, um den in den Service Level Agreements festgelegten
Workload zu bew�ltigen. Kapazit�ts- und Performance-Pl�ne sollten geeignete Modellierungstechniken anwenden, um ein
Modell der derzeitigen und k�nftigen Performance, Kapazit�t und Durchsatz der IT-Ressourcen zu erhalten.',
	'tImplementationGuide' => '',
),
125 => array(
	'fkContext' => '10510',
	'fkSectionBestPractice' => '10508',
	'nControlType' => '0',
	'sName' => 'DS3.2 Current Capacity and Performance (Gegenw�rtige Kapazit�t und Performance)',
	'tDescription' => '�berpr�fe die derzeitige Kapazit�t und Performance der IT-Ressourcen, um zu bestimmen, ob ausreichende Kapazit�t und
Performance vorhanden ist, um die Leistungen entsprechend der Service Level Agreements zu erbringen.',
	'tImplementationGuide' => '',
),
126 => array(
	'fkContext' => '10511',
	'fkSectionBestPractice' => '10508',
	'nControlType' => '0',
	'sName' => 'DS3.3 Future Capacity and Performance (K�nftige Kapazit�t und Performance)',
	'tDescription' => 'Erstelle in regelm��igen Abst�nden Vorhersagen der Performance und Kapazit�t von IT-Ressourcen, um das Risiko einer
Serviceunterbrechung auf Grund ungen�gender Kapazit�t oder einer Performanceverschlechterung zu minimieren. Identifiziere
auch �bersch�ssige Kapazit�ten f�r m�gliche andere Eins�tze. Identifiziere Auslastungstrends und erstelle Prognosen als Input
f�r Performance- und Kapazit�tspl�ne.',
	'tImplementationGuide' => '',
),
127 => array(
	'fkContext' => '10512',
	'fkSectionBestPractice' => '10508',
	'nControlType' => '0',
	'sName' => 'DS3.4 IT Resources Availability (Verf�gbarkeit der IT-Ressourcen)',
	'tDescription' => 'Stelle die ben�tigte Kapazit�t und Performance bereit unter Ber�cksichtigung von Aspekten wie normale Auslastung, Notf�lle,
Speicheranforderungen und Lebenszyklus von IT-Ressourcen. Vorkehrungen wie Priorisierung von Aufgaben,
Fehlertoleranzmechanismen und Ressourcenverteilungspraktiken sollten getroffen werden, falls die Performance und Kapazit�t
nicht dem erforderlichen Niveau entspricht. Das Management sollte sicherstellen, dass Kontinuit�tspl�ne die Verf�gbarkeit,
Kapazit�t und Performance der einzelnen IT-Ressourcen angemessen ber�cksichtigen.',
	'tImplementationGuide' => '',
),
128 => array(
	'fkContext' => '10513',
	'fkSectionBestPractice' => '10508',
	'nControlType' => '0',
	'sName' => 'DS3.5 Monitoring and Reporting (Monitoring und Reporting)',
	'tDescription' => 'Monitore laufend die Performance und Kapazit�t von IT-Ressourcen. Die gesammelten Daten dienen den beiden Zwecken:
? Die derzeitige Performance innerhalb der IT aufrecht zu erhalten und zu tunen, wobei Ausfallssicherheit, Zwischenf�lle,
derzeitige und k�nftige Auslastung, Speicherpl�ne und Beschaffung von Ressourcen behandelt werden.
? �ber Verf�gbarkeit der erbrachten Services an die Gesch�ftsbereiche zu berichten, wie dies in SLAs festgehalten ist. Mit
allen Ausnahmeberichten werden entsprechende Empfehlungen f�r korrektive Handlungen abgegeben.',
	'tImplementationGuide' => '',
),
129 => array(
	'fkContext' => '10515',
	'fkSectionBestPractice' => '10514',
	'nControlType' => '0',
	'sName' => 'DS4.1 IT Continuity Framework (Framework f�r IT-Kontinuit�t)',
	'tDescription' => 'Entwickle ein Framework f�r IT-Kontinuit�t zur Unterst�tzung eines unternehmensweiten Management der Gesch�ftskontinuit�t
durch einen konsistenten Prozess. Das Ziel des Frameworks ist die Unterst�tzung bei der Bestimmung der notwendigen
Ausfallssicherheit der Infrastruktur und das Vorantreiben der Entwicklung von Wiederanlauf- und IT-Kontinuit�tspl�nen (engl.:
disaster recovery and IT contingency plans). Das Framework sollte die Organisationsstruktur f�r Kontinuit�tsmanagement
behandeln, mit den Rollen, Aufgaben und Verantwortlichkeiten von internen und externen Dienstleistern, ihrem Management
und ihren Kunden, und die Rollen und Strukturen f�r Dokumentation, Test und Ausf�hrung der Wiederanlauf- und ITKontinuit�tspl�nen.
Der Plan sollte Einzelheiten wie die Identifikation kritischer Ressourcen, das Monitoring und Reporting der
Verf�gbarkeit kritischer Ressourcen, alternative Verarbeitung und die Grundprinzipien f�r Backup und Wiederherstellung
umfassen.',
	'tImplementationGuide' => '',
),
130 => array(
	'fkContext' => '10516',
	'fkSectionBestPractice' => '10514',
	'nControlType' => '0',
	'sName' => 'DS4.2 IT Continuity Plans (IT-Kontinuit�tspl�ne)',
	'tDescription' => 'Entwickle basierend auf dem Framework IT-Kontinuit�tspl�ne, die auf die Reduktion der Auswirkungen einer wesentlichen
Unterbrechung auf die Schl�ssel-Gesch�ftsfunktionen und -Prozesse ausgelegt sind. Die Pl�ne sollten die Anforderungen f�r
Ausfallsicherheit, alternative Verarbeitung und Wiederherstellungstauglichkeit f�r alle kritischen IT-Services behandeln. Sie
sollten auch Gebrauchsanleitungen, Rollen und Verantwortlichkeiten, Verfahren, Kommunikationsprozesse und das
Testvorgehen abdecken.',
	'tImplementationGuide' => '',
),
131 => array(
	'fkContext' => '10517',
	'fkSectionBestPractice' => '10514',
	'nControlType' => '0',
	'sName' => 'DS4.3 Critical IT Resources (Kritische IT-Ressourcen)',
	'tDescription' => 'Lenke die Aufmerksamkeit auf die im IT-Kontinuit�tsplan als am kritischsten definierten Elemente, um Ausfallsicherheit
einzubauen und um Priorit�ten f�r den Wiederanlauf festzulegen. Vermeide die Ablenkung der Wiederherstellung weniger
kritischer Elemente und stelle Reaktion und Wiederanlauf entsprechend den priorisierten Unternehmensbed�rfnissen sicher, unter
Wahrung der Kosten auf einem akzeptablen Niveau gehalten werden und der Einhaltung regulatorischer und vertraglicher
Anforderungen. Beachte Anforderungen f�r Ausfallsicherheit, Reaktion und Wiederherstellung f�r verschiedene Abstufungen,
zB eine bis vier Stunden, vier bis 24 Stunden, mehr als 24 Stunden und kritische gesch�ftliche Betriebszeiten.',
	'tImplementationGuide' => '',
),
132 => array(
	'fkContext' => '10518',
	'fkSectionBestPractice' => '10514',
	'nControlType' => '0',
	'sName' => 'DS4.4 Maintenance of the IT Continuity Plan (Wartung des IT-Kontinuit�tsplans)',
	'tDescription' => 'Unterst�tze das IT-Management bei der Festlegung und Anwendung von Verfahren zur Steuerunv von Changes, um
sicherzustellen, dass der IT-Kontinuit�tsplan aktuell gehalten wird und fortw�hrend die aktuellen Gesch�ftsanforderungen
widerspiegelt. Es ist wichtig, dass Ver�nderungen der Verfahren und Verantwortlichkeiten klar und rechtzeitig kommuniziert
werden.',
	'tImplementationGuide' => '',
),
133 => array(
	'fkContext' => '10519',
	'fkSectionBestPractice' => '10514',
	'nControlType' => '0',
	'sName' => 'DS4.5 Testing of the IT Continuity Plan (Test des IT-Kontinuit�tsplans)',
	'tDescription' => 'Teste den IT-Kontinuit�tsplan regelm��ig, um sicherzustellen, dass alle IT-Systeme wirksam wiederhergestellt werden k�nnen,
M�ngel behandelt werden und die Pl�ne zweckm��ig bleiben. Dies verlangt eine sorgf�ltige Vorbereitung, Dokumentation,
Berichterstattung �ber Testergebnisse und ? abh�ngig von den Ergebnissen ? die Umsetzung einer Ma�nahmenplanung. Erw�ge
den Umfang f�r Wiederherstellungstests von einzelnen Anwendungen, integrierten Test-Szenarios bis hin zu durchgehenden
Tests und integrierten Anbieter-Tests.',
	'tImplementationGuide' => '',
),
134 => array(
	'fkContext' => '10520',
	'fkSectionBestPractice' => '10514',
	'nControlType' => '0',
	'sName' => 'DS4.6 IT Continuity Plan Training (Schulung des IT-Kontinuit�tsplans)',
	'tDescription' => 'Stelle sicher, dass alle betroffenen Parteien regelm��ig Schulungen f�r die im Ereignis- oder Katastrophenfall anzuwendenden
Verfahren sowie ihrer Rollen und Verantwortlichkeiten erhalten. Verifiziere und erweitere Trainings entsprechend den
Ergebnissen von Kontinuit�tstests.',
	'tImplementationGuide' => '',
),
135 => array(
	'fkContext' => '10521',
	'fkSectionBestPractice' => '10514',
	'nControlType' => '0',
	'sName' => 'DS4.7 Distribution of the IT Continuity Plan (Verteilung des IT-Kontinuit�tsplans)',
	'tDescription' => 'Stelle sicher, dass eine festgelegte und gesteuerte Verteilungsstrategie besteht, um sicherzustellen, dass die Pl�ne genau und
sicher an geeignete, autorisierte Interessensgruppen verteilt werden und diesen bei Bedarf ? zeitlich und �rtlich ? zur Verf�gung
stehen. Es sollte darauf geachtet werden, dass die Pl�ne f�r alle Katastrophenszenarien verf�gbar gemacht werden.',
	'tImplementationGuide' => '',
),
136 => array(
	'fkContext' => '10522',
	'fkSectionBestPractice' => '10514',
	'nControlType' => '0',
	'sName' => 'DS4.8 IT-Services Recovery and Resumption (Wiederherstellung und Wiederanlauf von IT-Services)',
	'tDescription' => 'Plane die Aktionen f�r den Zeitraum, w�hrend die IT wiederhergestellt und die Services wieder aufgenommen werden. Dies kann
die Aktivierung von Ausweichsstandorten, die Inbetriebnahme der alternativen Verarbeitung, die Kommunikation mit Kunden
und Stakeholdern, Wiederanlaufverfahren etc beinhalten. Stelle sicher, dass die Fachbereiche die IT-Wiederherstellungszeiten
und die notwendigen technologischen Investitionen zur Unterst�tzung der gesch�ftlichen Bed�rfnisse hinsichtlich
Wiederherstellung und Wiederanlauf verstehen.',
	'tImplementationGuide' => '',
),
137 => array(
	'fkContext' => '10523',
	'fkSectionBestPractice' => '10514',
	'nControlType' => '0',
	'sName' => 'DS4.9 Offsite Backup Storage (Auslagerung von Backup)',
	'tDescription' => 'Lagere alle kritischen Backup-Medien, Dokumentationen und andere IT-Ressourcen, welche f�r den IT-Wiederanlauf und die
Gesch�ftskontinuit�tspl�ne notwendig sind, an einem entfernten Standort aus. Der Inhalt der Backup-Aufbewahrung sollte in Zusammenarbeit zwischen Gesch�ftsprozesseignern und den IT-Mitarbeiter bestimmt werden. Die Verwaltung der externen
Lagereinrichtung sollte dem Datenklassifikationsschema und Unternehmenspraktiken f�r Medienlagerung folgen. Das ITManagement
sollte sicherstellen, dass die Vorkehrungen f�r Auslagerung periodisch (mindestens j�hrlich) nach ihrem Inhalt,
dem umgebungsbezogenen Schutz und der Sicherheit beurteilt werden. Stelle die Kompatibilit�t von Hardware und Software
sicher, um die archivierten Daten wiederherzustellen, periodisch zu testen und archivierte Daten aufzufrischen.',
	'tImplementationGuide' => '',
),
138 => array(
	'fkContext' => '10524',
	'fkSectionBestPractice' => '10514',
	'nControlType' => '0',
	'sName' => 'DS4.10 Post-resumption Review (Review nach dem Wiederanlauf)',
	'tDescription' => 'Bestimme nach erfolgreichem Wiederanlauf der IT-Funktionen nach einem Ungl�ck, ob das IT-Management Verfahren f�r die
Beurteilung der Angemessenheit der Pl�ne und f�r deren dementsprechende �berarbeitung etabliert hat.',
	'tImplementationGuide' => '',
),
139 => array(
	'fkContext' => '10526',
	'fkSectionBestPractice' => '10525',
	'nControlType' => '0',
	'sName' => 'DS5.1 Management of IT Security (Management der IT-Sicherheit)',
	'tDescription' => 'Manage die IT-Sicherheit auf der h�chstm�glichen organisatorischen Ebene, so dass das Management von sicherheitsrelevanten
Aktivit�ten in Einklang steht mit den Unternehmensanforderungen.',
	'tImplementationGuide' => '',
),
140 => array(
	'fkContext' => '10527',
	'fkSectionBestPractice' => '10525',
	'nControlType' => '0',
	'sName' => 'DS5.2 IT Security Plan (IT-Security Plan)',
	'tDescription' => '�berf�hre den Informationsbedarf des Unternehmens, die IT-Konfiguration, Ma�nahmenpl�ne f�r informationsbezogene Risiken
und die Kultur der Informationssicherheit in einen umfassenden IT-Security Plan. Der Plan ist implementiert in
Sicherheitsrichtlinien und -verfahren zusammen mit angemessenen Investitionen in Services, Personal, Software und Hardware.
Sicherheitsrichtlinien und -verfahren werden an Stakeholder und Benutzer kommuniziert.',
	'tImplementationGuide' => '',
),
141 => array(
	'fkContext' => '10528',
	'fkSectionBestPractice' => '10525',
	'nControlType' => '0',
	'sName' => 'DS5.3 Identity Management (Identit�tsmanagement)',
	'tDescription' => 'Alle (internen, externen, tempor�ren) Benutzer und ihre Aktivit�ten auf IT-Systemen (Gesch�ftsanwendungen, Systembetrieb,
Entwicklung und Wartung) sollten eindeutig identifizierbar sein. Benutzerberechtigungen f�r die Systeme und Daten sollten mit
festgelegten und dokumentierten Gesch�ftsbed�rfnissen und Arbeitsplatzanforderungen �bereinstimmen.
Benutzerberechtigungen werden durch das Management des Fachbereichs angefordert, durch die Systemeigner bewilligt und
durch die sicherheitsverantwortliche Person implementiert. Benutzerkennungen und Zugriffsberechtigungen werden in einer
zentralen Datenbank gef�hrt. Kosteng�nstige technische und organisatorische Ma�nahmen werden eingesetzt und aktuell
gehalten, um die Benutzeridentifikation zu ermitteln, die Authentisierung einzurichten und Zugriffsrechte durchzusetzen.',
	'tImplementationGuide' => '',
),
142 => array(
	'fkContext' => '10529',
	'fkSectionBestPractice' => '10525',
	'nControlType' => '0',
	'sName' => 'DS5.4 User Account Management (Management von Benutzerkonten)',
	'tDescription' => 'Stelle sicher, dass Antrag, Einrichtung, Ausstellung, Aufhebung, �nderung und Schlie�ung von Benutzerkonten und zugeh�rige
Benutzerberechtigungen durch die Benutzerkontenverwaltung behandelt werden. Ein Freigabeverfahren sollte darin enthalten
sein, dass den Daten- oder Systemeigner behandelt, der die Zugriffsberechtigungen bewilligt. Diese Verfahren sollten f�r
s�mtliche Benutzer, einschlie�lich Administratoren (privilegierte Benutzer), interne und externe Benutzer, f�r normale und f�r
Notfalls-Changes G�ltigkeit haben. Rechte und Pflichten in Zusammenhang mit dem Zugriff auf Unternehmenssysteme und -
informationen sollten vertraglich f�r alle Arten von Benutzer festgelegt werden. F�hre regelm��ig Management-Reviews aller
Benutzerkonten und entsprechenden Berechtigungen durch.',
	'tImplementationGuide' => '',
),
143 => array(
	'fkContext' => '10530',
	'fkSectionBestPractice' => '10525',
	'nControlType' => '0',
	'sName' => 'DS5.5 Security Testing, Surveillance and Monitoring (Testen, Beobachtung und �berwachung der Sicherheit)',
	'tDescription' => 'Stelle sicher, dass die Umsetzungen der IT-Sicherheit getestet und proaktiv �berwacht wird. Die IT-Sicherheit sollte periodisch
neu zertifiziert werden, um sicherzustellen, dass der genehmigte Sicherheitsgrad beibehalten wird. Eine Protokollierungs- und
Monitoringfunktion erm�glicht die fr�hzeitige Erkennung von ungew�hnlichen oder abnormalen Aktivit�ten, die eventuell
behandelt werden m�ssen. Der Zugriff zur Protokollierungsinformation steht bez�glich Zugriffsrechten und
Aufbewahrungsvorschriften in Einklang mit den Gesch�ftsanforderungen.',
	'tImplementationGuide' => '',
),
144 => array(
	'fkContext' => '10531',
	'fkSectionBestPractice' => '10525',
	'nControlType' => '0',
	'sName' => 'DS5.6 Security Incident Definition (Definition von Security Incidents)',
	'tDescription' => 'Stelle sicher, dass die Charakteristika von m�glichen Security Incidents klar definiert und kommuniziert werden, so dass
Sicherheitsvorf�lle korrekt durch den Incident oder Problem-Management-Prozess behandelt werden k�nnen. Die Charakteristika
umfassen eine Beschreibung dessen, was als Security Incident verstanden wird und dem Grad der Auswirkungen. Eine begrenzte
Anzahl von Auswirkungsniveaus sind definiert und f�r alle sind die spezifisch erforderlichen Aktivit�ten und die zu
benachrichtigenden Personen identifiziert.',
	'tImplementationGuide' => '',
),
145 => array(
	'fkContext' => '10532',
	'fkSectionBestPractice' => '10525',
	'nControlType' => '0',
	'sName' => 'DS5.7 Protection of Security Technology (Schutz von Sicherheitseinrichtungen)',
	'tDescription' => 'Stelle sicher, dass wichtige Sicherheitstechnologie gegen Sabotage abgesichert wird und dass Sicherheitsdokumentation nicht
unn�tigerweise ver�ffentlicht wird, was bedeutet, dass sie nicht auff�llt. Mache jedoch die Sicherheit von Systemen nicht von der
Geheimhaltung der Sicherheitsspezifikationen abh�ngig.',
	'tImplementationGuide' => '',
),
146 => array(
	'fkContext' => '10533',
	'fkSectionBestPractice' => '10525',
	'nControlType' => '0',
	'sName' => 'DS5.8 Cryptographic Key Management (Verwaltung kryptographischer Schl�ssel)',
	'tDescription' => 'Stelle sicher, dass Richtlinien und Verfahren etabliert sind f�r die Erzeugung, �nderung, Widerrufung, Zerst�rung, Verteilung,
Zertifizierung, Speicherung, Eingabe, Verwendung und Archivierung von kryptographischen Schl�sseln, um den Schutz der
Schl�ssel gegen Ver�nderung und unberechtigte Aufdeckung sicherzustellen.',
	'tImplementationGuide' => '',
),
147 => array(
	'fkContext' => '10534',
	'fkSectionBestPractice' => '10525',
	'nControlType' => '0',
	'sName' => 'DS5.9 Malicious Software Prevention, Detection and Correction (Schutz vor, Erkennung und Korrektur von b�sartiger
Software)',
	'tDescription' => 'Stelle sicher, dass pr�ventive, detektive und korrektive Ma�nahmen (speziell aktuelle Sicherheits-Patches und Virenschutz) in der
gesamten Organisation vorhanden sind, um Informationssysteme und die Technologie vor b�sartigem Code (Viren, W�rmer,
Spionage-Software (engl.: spyware), Spam, intern entwickelte betr�gerische Software etc) zu sch�tzen.',
	'tImplementationGuide' => '',
),
148 => array(
	'fkContext' => '10535',
	'fkSectionBestPractice' => '10525',
	'nControlType' => '0',
	'sName' => 'DS5.10 Network Security (Netzwerk-Sicherheit)',
	'tDescription' => 'Stelle sicher, dass technische Sicherheitsma�nahmen und zugeh�rige Managementverfahren (zB Firewall, Sicherheits-
Appliances, Netzwerksegmentierung und Intrusionserkennung) verwendet werden, um den Zugriff zu Netzwerken zu bewilligen
und den Informationsfluss von und zu Netzwerken zu steuern.',
	'tImplementationGuide' => '',
),
149 => array(
	'fkContext' => '10536',
	'fkSectionBestPractice' => '10525',
	'nControlType' => '0',
	'sName' => 'DS5.11 Exchange of Sensitive Data (Austausch sensitiver Daten)',
	'tDescription' => 'Stelle sicher, dass sensitive Transaktionsdaten nur �ber einen vertrauensw�rdigen Pfad oder ein Medium ausgetauscht werden
mit (den notwendigen) Ma�nahmen, um die Authentizit�t des Inhalts, den Beweis der Aufgabe und des Empfangs und
Nichtabstreitbarkeit der Quelle zu bieten.',
	'tImplementationGuide' => '',
),
150 => array(
	'fkContext' => '10538',
	'fkSectionBestPractice' => '10537',
	'nControlType' => '0',
	'sName' => 'DS6.1 Definition of services (Definition von Services)',
	'tDescription' => 'Identifiziere alle IT-Kosten und lege sie auf die IT-Services um, um ein transparentes Kostenmodell zu unterst�tzen. IT-Services
sollten mit Gesch�ftsprozessen verbunden werden, so dass das Kerngesch�ft die jeweiligen Service-Rechnungsgr�ssen genau
bestimmen kann.',
	'tImplementationGuide' => '',
),
151 => array(
	'fkContext' => '10539',
	'fkSectionBestPractice' => '10537',
	'nControlType' => '0',
	'sName' => 'DS6.2 IT accounting (IT-Rechnungswesen)',
	'tDescription' => 'Zeichne die Istkosten auf und weise diese entsprechend dem definierten Kostenmodell zu. Abweichungen zwischen Plan- und
Istkosten sollten, entsprechend den unternehmensweiten Systemen zur Messung von Finanzzahlen, analysiert und berichtet
werden.',
	'tImplementationGuide' => '',
),
152 => array(
	'fkContext' => '10540',
	'fkSectionBestPractice' => '10537',
	'nControlType' => '0',
	'sName' => 'DS6.3 Cost modelling and charging (Kostenmodellierung und -verrechnung)',
	'tDescription' => 'Lege ? basierend auf den definierten Services ? ein Kostenmodell fest, das direkte, indirekte und Gemeinkosten (engl.: overhead)
f�r Services ber�cksichtigt und das die Berechnung von Verrechnungss�tzen je Service unterst�tzt. Das Kostenmodell sollte mit
den unternehmensweiten Kostenrechnungsverfahren �bereinstimmen. Das IT-Kostenmodell sollte sicherstellen, dass die
Verrechnung von Services f�r User nachvollziehbar, messbar und vorhersehbar ist, um einen angemessenen Gebrauch der
Ressourcen zu unterst�tzen. Das Fachbereichsmanagement sollte in der Lage sein, die tats�chliche Verwendung und
Verrechnung der Services zu verifizieren.',
	'tImplementationGuide' => '',
),
153 => array(
	'fkContext' => '10541',
	'fkSectionBestPractice' => '10537',
	'nControlType' => '0',
	'sName' => 'DS6.4 Cost Model Maintenance (Wartung des Kostenmodells)',
	'tDescription' => '�berpr�fe und benchmarke die Angemessenheit des Kosten(verrechnungs)modells regelm��ig, um dessen Sachlichkeit und
Angemessenheit entsprechend der sich entwickelnden Unternehmens- und IT-Aktivit�ten zu erhalten.',
	'tImplementationGuide' => '',
),
154 => array(
	'fkContext' => '10543',
	'fkSectionBestPractice' => '10542',
	'nControlType' => '0',
	'sName' => 'DS7.1 Identification of education and training needs (Identifikation von Schulungs- und Trainingsbedarf)',
	'tDescription' => 'Entwickle und aktualisiere regelm��ig ein Curriculum f�r alle Zielgruppen von Mitarbeiter unter Ber�cksichtigung von:
? Derzeitige und k�nftige Unternehmenserfordernisse und -strategie
? Unternehmensweite Werte (ethische Werte, Control, Sicherheitskultur, etc)
? Einf�hrung neuer IT-Infrastruktur und Software (Pakete und Anwendungen)
? Derzeitige Fertigkeiten, Kompetenzprofile, Bedarf an Zertifizierung und/oder Berechtigungsnachweisen
? Schulungsmethoden (zB Klassenraum, Web-basierend), Gr��e der Zielgruppe, Erreichbarkeit und Zeitvorgaben',
	'tImplementationGuide' => '',
),
155 => array(
	'fkContext' => '10544',
	'fkSectionBestPractice' => '10542',
	'nControlType' => '0',
	'sName' => 'DS7.2 Delivery of training and education (Abhaltung von Trainings und Schulungen)',
	'tDescription' => 'Identifiziere, basierend auf dem festgestellten Schulungs- und Trainingsbedarf, Zielgruppen und deren Mitglieder, wirksame
Schulungsmethoden, Lehrkr�fte, Trainer und Mentoren. Ernenne Trainer und organisiere zeitgerecht Schulungseinheiten.
Anmeldung (inklusive Voraussetzungen), Teilnahme und Leistungsbewertungen sollten festgehalten werden.',
	'tImplementationGuide' => '',
),
156 => array(
	'fkContext' => '10545',
	'fkSectionBestPractice' => '10542',
	'nControlType' => '0',
	'sName' => 'DS7.3 Evaluation of training received (Evaluierung von besuchten Trainings)',
	'tDescription' => 'Beurteile die Vermittlung der Schulungs- und Trainingsinhalte nach dem Abschluss hinsichtlich Relevanz, Qualit�t,
Wirksamkeit, Erfassen und Behalten des Wissens, Kosten und Nutzen. Die Ergebnisse dieser Beurteilung sollten als Input f�r die
k�nftige Festlegung von Curricula und Trainingseinheiten dienen.',
	'tImplementationGuide' => '',
),
157 => array(
	'fkContext' => '10547',
	'fkSectionBestPractice' => '10546',
	'nControlType' => '0',
	'sName' => 'DS8.1 Service Desk (Service Desk)',
	'tDescription' => 'Richte eine Service Desk-Funktion als Schnittstelle von Usern zur IT, zur Aufnahme, Kommunikation, Weitergabe und Analyse
aller Anrufe, gemeldeten Incidents, Service- und Informationsanfragen ein. Basierend auf den vereinbarten Service Levels gem��
dem geeigneten SLA sollten Verfahren f�r die �berwachung und die Eskalation umgesetzt werden, welche die Klassifikation
und Priorisierung aller gemeldeten Vorf�lle als Incident, Serviceanfrage oder Informationsanfrage erlauben. Messe die
Zufriedenheit des Endbenutzers mit der Qualit�t des Service Desk und der IT-Services.',
	'tImplementationGuide' => '',
),
158 => array(
	'fkContext' => '10548',
	'fkSectionBestPractice' => '10546',
	'nControlType' => '0',
	'sName' => 'DS8.2 Registration of customer queries (Registrierung von Kundenanfragen)',
	'tDescription' => 'Etabliere eine Funktion und ein System zur Aufzeichnung und Verfolgung von Anrufen, Incidents, Serviceanfragen und
Informationsbed�rfnissen. Es sollte in Prozesse wie Incident-Management, Problem-Management, Change-Management,
Kapazit�ts- und Verf�gbarkeitsmanagement eng eingebunden sein. Incidentssollten entsprechend einer Gesch�fts- und Service-
Priorit�t klassifiziert und dem geeigneten Team zur Problembehandlung �bergeben werden und die Kunden sollten �ber den
Status ihrer Anfragen informiert bleiben.',
	'tImplementationGuide' => '',
),
159 => array(
	'fkContext' => '10549',
	'fkSectionBestPractice' => '10546',
	'nControlType' => '0',
	'sName' => 'DS8.3 Incident escalation (Eskalation von Incidents)',
	'tDescription' => 'Erstelle Verfahren f�r den Service Desk, so dass nicht sofort l�sbare Incidentsangemessen, entsprechend den in den SLAs
definierten Grenzen eskaliert, und ? wo anwendbar ? entsprechende Workarounds angeboten werden. Stelle sicher, dass die
Eigent�merschaft von Incidentsund deren �berwachung w�hrend des gesamten Lebenszyklus der durch Benutzer initiierten
Incidentsbeim Service Desk verbleibt, unabh�ngig davon, welche Gruppe der IT an der L�sung arbeitet.',
	'tImplementationGuide' => '',
),
160 => array(
	'fkContext' => '10550',
	'fkSectionBestPractice' => '10546',
	'nControlType' => '0',
	'sName' => 'DS8.4 Incident closure (Schlie�en von Incidents)',
	'tDescription' => 'Erstelle Verfahren f�r das zeitnahe Monitoring der Erledigung von Kundenanfragen. Wenn ein Incidentgel�st wird, sollte der
Service Desk die zugrunde liegende Ursache (falls bekannt) aufzeichnen und best�tigen, dass die getroffene Handlung vom
Kunden akzeptiert wurde.',
	'tImplementationGuide' => '',
),
161 => array(
	'fkContext' => '10551',
	'fkSectionBestPractice' => '10546',
	'nControlType' => '0',
	'sName' => 'DS8.5 Trend analysis (Trendanalyse)',
	'tDescription' => 'Erstelle Berichte der Aktivit�ten des Service Desks, um dem Management zu erm�glichen, die Leistungserbringung und
Antwortzeiten zu messen und Trends oder wiederkehrende Probleme zu identifizieren, so dass das Service kontinuierlich
verbessert werden kann.',
	'tImplementationGuide' => '',
),
162 => array(
	'fkContext' => '10553',
	'fkSectionBestPractice' => '10552',
	'nControlType' => '0',
	'sName' => 'DS9.1 Configuration repository and baseline (Konfigurationsinformation und Referenzversionen)',
	'tDescription' => 'Erstelle eine zentrale Sammlung (engl.: repository) aller relevanten Informationen �ber Configuration Items. Dieses Repository
umfasst Hardware, Anwendungssoftware, Middleware, Parameter, Dokumentation, Verfahren und Werkzeuge f�r Betrieb,
Zugriff und Verwendung der Systeme und Services. Ber�cksichtigt werden sollten Informationen wie Benennung,
Versionsnummern und Lizenzierungsdetails. Eine Referenzversion der Configuration Items sollte f�r jedes System und alle
Services aufbewahrt werden, um nach Changes wieder dazu zur�ckkehren zu k�nnen.',
	'tImplementationGuide' => '',
),
163 => array(
	'fkContext' => '10554',
	'fkSectionBestPractice' => '10552',
	'nControlType' => '0',
	'sName' => 'DS9.2 Identification and maintenance of configuration items (Identifikation und Wartung von Configuration Items)',
	'tDescription' => 'Erstelle Verfahren f�r:
? Identifikation von Configuration Items und deren Attribute
? Aufzeichnung neuer, modifizierter und gel�schter Configuration Items
? Identifikation und Wartung der Beziehungen zwischen Configuration Items im Configuration Repository
? Update bestehender Configuration Items im Konfigurations-Repository
? Verhinderung der Ber�cksichtigung nichtautorisierter Software
Diese Verfahren sollten eine angemessene Autorisierung und Aufzeichnung aller Aktionen am Konfigurations-Repository
erm�glichen und in die Verfahren des Change-Management und Problem-Management integriert sein.',
	'tImplementationGuide' => '',
),
164 => array(
	'fkContext' => '10555',
	'fkSectionBestPractice' => '10552',
	'nControlType' => '0',
	'sName' => 'DS9.3 Configuration integrity review (Review der Integrit�t der Konfiguration)',
	'tDescription' => '�berpr�fe und verifiziere regelm��ig, wo notwendig unter Verwendung von entsprechenden Werkzeugen, den Status der
Configuration Items, um die Integrit�t der derzeitigen und historischen Konfigurationsdaten zu best�tigen und mit der effektiven
Situation zu vergleichen. �berpr�fe periodisch anhand der Policy f�r die Verwendung von Software die Existenz von privater
oder nichtlizenzierter Software oder anderer Software, die g�ltigen Lizenzvereinbarungen widerspricht. Fehler und
Abweichungen sollten berichtet, verfolgt und korrigiert werden.',
	'tImplementationGuide' => '',
),
165 => array(
	'fkContext' => '10557',
	'fkSectionBestPractice' => '10556',
	'nControlType' => '0',
	'sName' => 'DS10.1 Identification and classification of problems (Identifikation und Klassifikation von Problemen)',
	'tDescription' => 'Erstelle Prozesse zur Meldung und Klassifikation von Problemen, welche als Teil des Incident-Management identifiziert wurden.
Die Schritte einer Problemklassifizierung sind �hnlich zu den Schritten f�r die Klassifizierung von Incidents; sie sollten
Kategorie, Auswirkungen, Dringlichkeit und Priorit�t bestimmen. Probleme sollten sinnvoll in zusammenh�ngende Gruppen oder
Dom�nen kategorisiert werden (zB Hardware, Software, unterst�tzende Software). Diese Gruppen k�nnen den organisatorischen
Verantwortlichkeiten, dem User- oder dem Kundenkreis entsprechen und sind die Grundlage f�r die Zuweisung von Problemen
an Support-Personal.',
	'tImplementationGuide' => '',
),
166 => array(
	'fkContext' => '10558',
	'fkSectionBestPractice' => '10556',
	'nControlType' => '0',
	'sName' => 'DS10.2 Problem tracking and resolution (Problemverfolgung und -l�sung)',
	'tDescription' => 'Das Problemmanagementsystem sollte angemessene Pr�fspur-Aufzeichnungen bieten, welche die Nachverfolgung, Analyse und
Bestimmung der zugrunde liegenden Ursache (engl.: root cause) aller gemeldeten Probleme erm�glichen, unter Beachtung von
? allen verbundenen Konfigurationselementen
? ungel�sten Problemen und Ereignissen
? bekannten und vermuteten Fehlern
Identifiziere und initialisiere anhaltende L�sungen, welche die zu Grunde liegende Ursache angehen und Change-Requests an
den etablierten Change-Management-Prozess stellen. W�hrend des gesamten L�sungsprozesses sollte das Problem-Management
regelm��ig vom Change-Management Berichte �ber den Fortschritt in der L�sung von Problemen und Fehlern erhalten. Das
Problem-Management sollte die andauernden Auswirkungen von Problemen und bekannten Fehlern (engl.: known errors) auf die
User Services erhalten. F�r den Fall, dass die Auswirkungen wesentlich werden, sollte das Problem-Management das Problem
eskalieren, allenfalls an ein entsprechendes Gremium verweisen, um die Priorit�t der �nderungsanfrage (engl.: request for
change = RFC) zu erh�hen oder um ? falls notwendig ? einen dringenden Change zu implementieren. Der Fortschritt der
Probleml�sung sollte gegen das SLA gemonitort werden.',
	'tImplementationGuide' => '',
),
167 => array(
	'fkContext' => '10559',
	'fkSectionBestPractice' => '10556',
	'nControlType' => '0',
	'sName' => 'DS10.3 Problem closure (DS10.3 Abschluss von Problemen)',
	'tDescription' => 'Setze ein Verfahren ein zum Abschluss von Problemaufzeichnungen entweder nach der Best�tigung einer erfolgreichen
Beseitigung des bekannten Fehlers oder nach einer �bereinkunft mit dem Fachbereich, wie man das Problem alternativ l�sen
k�nnte.',
	'tImplementationGuide' => '',
),
168 => array(
	'fkContext' => '10560',
	'fkSectionBestPractice' => '10556',
	'nControlType' => '0',
	'sName' => 'DS10.4 Integration of change, configuration and problem management (Integration von Change-, Configuration- und
Problem-Management)',
	'tDescription' => 'Um ein wirksames Management von Problemen und Incidents sicherzustellen, integriere die in Beziehung stehenden Prozesse
Change-, Configuration- und Problem-Management. Verfolge wie viel Aufwand in notfallartige Korrekturen an Stelle von
Ma�nahmen zur Verbesserung des Kerngesch�fts gesteckt wird, und verbessere diese Prozesse, um Probleme zu minimieren.',
	'tImplementationGuide' => '',
),
169 => array(
	'fkContext' => '10562',
	'fkSectionBestPractice' => '10561',
	'nControlType' => '0',
	'sName' => 'DS11.1 Business requirements for data management (Unternehmensanforderungen an Datenmanagement)',
	'tDescription' => 'Erstelle Vorkehrungen, um sicherzustellen, dass vom Kerngesch�ft erwartete Quelldokumente erhalten werden, alle vom
Kerngesch�ft erhaltene Daten verarbeitet werden, der gesamte vom Kerngesch�ft ben�tigte Output vorbereitet und abgeliefert
wird und dass Anforderungen f�r Wiederanlauf und nochmalige Verarbeitung unterst�tzt werden',
	'tImplementationGuide' => '',
),
170 => array(
	'fkContext' => '10563',
	'fkSectionBestPractice' => '10561',
	'nControlType' => '0',
	'sName' => 'DS11.2 Storage and retention arrangements (Speicherungs- und Aufbewahrungsvorkehrungen)',
	'tDescription' => 'Definiere und setze Verfahren f�r die Datenspeicherung und -archivierung ein, dass Daten im Zugriff und verwendbar bleiben.
Die Verfahren sollten Anforderungen hinsichtlich Wiederauffindung, Kosteng�nstigkeit, kontinuierliche Integrit�t und
Sicherheit ber�cksichtigen. Entwickle Speicherungs- und Aufbewahrungsvorkehrungen, um gesetzliche, regulatorische und
Unternehmenserfordernisse f�r Dokumente, Daten, Archive, Programme, Berichte und (eingehende und ausgehende) Meldungen
sowie die f�r deren Verschl�sselung und Authentifikation verwendeten Daten einzuhalten.',
	'tImplementationGuide' => '',
),
171 => array(
	'fkContext' => '10564',
	'fkSectionBestPractice' => '10561',
	'nControlType' => '0',
	'sName' => 'DS11.3 Media library management system (Medien-Bibliotheksmanagementsystem)',
	'tDescription' => 'Definiere und setze Verfahren zum Unterhalt eines Inventars von lokal vorhandenen Datentr�gern ein, und stelle deren
Verwendbarkeit und Integrit�t sicher. Verfahren sollten f�r ein zeitgerechtes Review und Abkl�rung aller gefundenen
Differenzen sorgen.',
	'tImplementationGuide' => '',
),
172 => array(
	'fkContext' => '10565',
	'fkSectionBestPractice' => '10561',
	'nControlType' => '0',
	'sName' => 'DS11.4 Disposal (Entsorgung)',
	'tDescription' => 'Definiere und setze Verfahren ein, die den Zugriff auf sensitive Informationen und Software von Ger�ten oder Datentr�gern
verhindern, wenn diese entsorgt oder einem anderen Zweck �bertragen werden. Solche Verfahren sollten sicherstellen, dass als
gel�scht markierte oder zur Entsorgung bestimmte Daten nicht wiedergewonnen werden k�nnen.',
	'tImplementationGuide' => '',
),
173 => array(
	'fkContext' => '10566',
	'fkSectionBestPractice' => '10561',
	'nControlType' => '0',
	'sName' => 'DS11.5 Backup and restoration (Backup und Wiederherstellung)',
	'tDescription' => 'Definiere und setze Verfahren f�r Sicherung und Wiederherstellung von Anwendungen, Daten und Dokumentation in
�bereinstimmung mit den Gesch�ftsanforderungen und dem Kontinuit�tsplan ein. Verifiziere die Einhaltung von
Backupverfahren, die F�higkeit zu sowie die notwendige Zeit f�r eine erfolgreiche und komplette Wiederherstellung. Teste
Backup-Medien und den Wiederherstellungsprozess.',
	'tImplementationGuide' => '',
),
174 => array(
	'fkContext' => '10567',
	'fkSectionBestPractice' => '10561',
	'nControlType' => '0',
	'sName' => 'DS11.6 Security requirements for data management (Sicherheitsanforderungen f�r Management der Daten)',
	'tDescription' => 'Entwickle Vorkehrungen, um Sicherheitsanforderungen in Bezug auf Empfang, Verarbeitung, physischer Speicherung und
Ausgabe von Daten und sensitiven Meldungen zu identifizieren und umzusetzen. Dies umfasst physische Aufzeichnungen,
Daten�bermittlung und alle ausgelagerte Datenspeicherung.',
	'tImplementationGuide' => '',
),
175 => array(
	'fkContext' => '10569',
	'fkSectionBestPractice' => '10568',
	'nControlType' => '0',
	'sName' => 'DS12.1 Site selection and layout (Standortwahl und Layout von Einrichtungen)',
	'tDescription' => 'Definiere und w�hle die physischen Standorte f�r IT-Ausr�stungen aus, um die mit der Unternehmensstrategie verbundene
Technologiestrategie zu unterst�tzen. Auswahl und Entwurf des Layout eines Standortes sollten die Risiken von nat�rlichen und
durch Menschen hervorgerufene Katastrophen einbeziehen und relevante Gesetze und Bestimmungen wie f�r Betriebsgesundheit
und Safetybestimmungen ber�cksichtigen.',
	'tImplementationGuide' => '',
),
176 => array(
	'fkContext' => '10570',
	'fkSectionBestPractice' => '10568',
	'nControlType' => '0',
	'sName' => 'DS12.2 Physical security measures (Physische Sicherheitsma�nahmen)',
	'tDescription' => 'Definiere und implementiere den Unternehmenserfordernissen entsprechende Ma�nahmen zur physischen Sicherheit.
Ma�nahmen sollten unter anderem Layout und Perimeter des Sicherheitsbereichs, Sicherheitszonen, Standort kritischer
Ausr�stung sowie Versand- und Anlieferungszonen umfassen. Halte insbesondere ein unauf�lliges Profil bez�glich der Pr�senz
des kritischen IT-Betriebs. Verantwortlichkeiten f�r die �berwachung und Verfahren f�r Berichterstattung und L�sung von
Incidents der physischen Sicherheit m�ssen aufgestellt werden.',
	'tImplementationGuide' => '',
),
177 => array(
	'fkContext' => '10571',
	'fkSectionBestPractice' => '10568',
	'nControlType' => '0',
	'sName' => 'DS12.3 Physical access (Physischer Zugang)',
	'tDescription' => 'Entwickle und implementiere Verfahren f�r die dem Unternehmensbedarf inklusive Notf�llen entsprechende Erteilung,
Einschr�nkung und Zur�cknahme von Zutritt zu Gel�nde, Geb�uden und Arbeitsbereichen. Der Zugang zu Gel�nde, Geb�uden
und Arbeitsbereichen sollte begr�ndet, genehmigt, protokolliert und �berwacht werden. Dies gilt f�r alle Personen, die das
Gel�nde betreten, inklusive Personal, tempor�res Personal, Kunden, Lieferanten, Besucher oder andere Drittparteien.',
	'tImplementationGuide' => '',
),
178 => array(
	'fkContext' => '10572',
	'fkSectionBestPractice' => '10568',
	'nControlType' => '0',
	'sName' => 'DS12.4 Protection against environmental factors (Schutz gegen Umwelteinfl�sse)',
	'tDescription' => 'Entwickle und implementiere Ma�nahmen zum Schutz gegen Umweltfaktoren. Spezielle Ausr�stung und Ger�te zur
�berwachung und Steuerung der Umwelt sollten installiert sein.',
	'tImplementationGuide' => '',
),
179 => array(
	'fkContext' => '10573',
	'fkSectionBestPractice' => '10568',
	'nControlType' => '0',
	'sName' => 'DS12.5 Physical facilities management (Management von physischen Einrichtungen)',
	'tDescription' => 'Manage Einrichtungen, inklusive Strom- und Kommunikationsausr�stung entsprechend Gesetzen und Bestimmungen,
technischen und Unternehmenserfordernissen, Spezifikationen von Anbietern und Gesundheits- und Safetyrichtlinien.',
	'tImplementationGuide' => '',
),
180 => array(
	'fkContext' => '10575',
	'fkSectionBestPractice' => '10574',
	'nControlType' => '0',
	'sName' => 'DS13.1 Operations procedures and instructions (Operative Verfahren und Anweisungen)',
	'tDescription' => 'Definiere, implementiere und unterhalte standardisierte Verfahren f�r den IT-Betrieb und stelle sicher, dass das Betriebspersonal
mit allen f�r sie relevanten Betriebsaufgaben vertraut sind. Operative Verfahren sollten Schicht�bergaben (formale �bergaben
von Aktivit�ten, Status-Aktualisierungen, operativen Problemen, Eskalationsverfahren und Berichte �ber derzeitige
Verantwortungen) abdecken, um einen kontinuierlichen Betrieb sicherzustellen.',
	'tImplementationGuide' => '',
),
181 => array(
	'fkContext' => '10576',
	'fkSectionBestPractice' => '10574',
	'nControlType' => '0',
	'sName' => 'DS13.2 Job scheduling (Job-Planung)',
	'tDescription' => 'Organisiere und plane Jobs, Prozesse und Aufgaben in der wirtschaftlichsten Reihenfolge, maximiere den Durchsatz und die
Verwendung, um die Unternehmenserfordernisse zu erf�llen. Die erstmalige Planung sowie �nderungen dieser Pl�ne sollten
autorisiert werden. Verfahren sollten vorhanden sein, um Abweichungen von normalen Job-Pl�nen zu erkennen, abzukl�ren und
freizugeben.',
	'tImplementationGuide' => '',
),
182 => array(
	'fkContext' => '10577',
	'fkSectionBestPractice' => '10574',
	'nControlType' => '0',
	'sName' => 'DS13.3 IT infrastructure monitoring (Monitoring der IT-Infrastruktur)',
	'tDescription' => 'Definiere und implementiere Verfahren zur �berwachung der IT-Infrastruktur und der damit in Zusammenhang stehenden
Vorkommnisse. Stelle sicher, dass ausreichend chronologische Informationen in Betriebsprotokollen gespeichert sind, um
Wiederherstellung, Review und die Untersuchung der zeitlichen Abfolge von Betriebs- und anderen Aktivit�ten im Umfeld oder
zur Unterst�tzung des Betriebs zu erm�glichen.',
	'tImplementationGuide' => '',
),
183 => array(
	'fkContext' => '10578',
	'fkSectionBestPractice' => '10574',
	'nControlType' => '0',
	'sName' => 'DS13.4 Sensitive documents and output devices (Sensitive Dokumente und Ausgabeger�te)',
	'tDescription' => 'Etabliere geeignete physische Absicherungen, Verrechnungs- und Inventurpraktiken f�r sensitive IT-Anlagen wie
Spezialformulare, verwertbare Einrichtungen, Spezialdrucker oder Security-Token.',
	'tImplementationGuide' => '',
),
184 => array(
	'fkContext' => '10579',
	'fkSectionBestPractice' => '10574',
	'nControlType' => '0',
	'sName' => 'DS13.5 Preventive maintenance for hardware (Pr�ventive Hardware-Wartung)',
	'tDescription' => 'Definiere und implementiere Verfahren zur Sicherstellung einer zeitgerechten Wartung der Infrastruktur, um die H�ufigkeit und
Auswirkungen von Fehlern oder Leistungsabfall zu reduzieren.',
	'tImplementationGuide' => '',
),
185 => array(
	'fkContext' => '10582',
	'fkSectionBestPractice' => '10581',
	'nControlType' => '0',
	'sName' => 'ME1.1 Monitoring Approach (Ansatz f�r das Monitoring)',
	'tDescription' => 'Stelle sicher, dass das Management ein Framework und einen Ansatz f�r ein generelles Monitoring aufstellt, welche den Scope,
die Methoden und anzuwendenden Prozesse festlegen, die befolgt werden m�ssen, um den Beitrag der IT zu den
Portfoliomanagement- und Programmmanagement-Prozessen sowie jene Prozesse zu �berwachen, die spezifisch sind f�r die
Erbringung des Potential und der Services der IT. Das Framework sollte in das unternehmensweite System zum Performance-
Monitoring integriert sein.',
	'tImplementationGuide' => '',
),
186 => array(
	'fkContext' => '10583',
	'fkSectionBestPractice' => '10581',
	'nControlType' => '0',
	'sName' => 'ME1.2 Definition and Collection of Monitoring Data (Definition und Sammlung von Monitoring-Daten)',
	'tDescription' => 'Stelle sicher, dass das IT-Management in Zusammenarbeit mit dem Kerngesch�ft ein ausgewogenes Ma� an Vorgaben,
Messgr��en, Zielen und Benchmarks f�r Performance definiert und dass diese auch durch Kerngesch�ftsverantwortliche und
andere, relevante Stakeholder freigegeben werden. Messgr��en f�r Performance sollten die folgenden enthalten:
? Beitrag zum Kerngesch�ft, der auch, aber nicht nur finanzorientierte Zahlen enth�lt
? Perfomance im Vergleich zum strategischen Gesch�fts- und IT-Plan
? Risiken aus und Einhaltung von Regulativen
? Zufriedenheit interner und externer User
? Wesentliche IT-Prozesse, inklusive Entwicklung und Service Delivery
? Zukunftsorientierte Aktivit�ten (zB neu entstehende Technologien, wieder verwendbare Infrastruktureinrichtungen,
Fertigkeiten von Gesch�fsbereichs- und IT-Personal).
Prozesse sollten erstellt werden, um zeitnahe und richtige Daten zu sammeln und um �ber den Zielerreichungsgrad berichten zu
k�nnen.',
	'tImplementationGuide' => '',
),
187 => array(
	'fkContext' => '10584',
	'fkSectionBestPractice' => '10581',
	'nControlType' => '0',
	'sName' => 'ME1.3 Monitoring Method (Methode des Monitoring)',
	'tDescription' => 'Stelle sicher, dass der Monitoring-Prozess eine Methode einsetzt (zB Balanced Scorecard), die eine pr�gnante, umfassende
�bersicht �ber die Performance der IT erm�glicht und die zum unternehmensweiten Monitoring System passt.',
	'tImplementationGuide' => '',
),
188 => array(
	'fkContext' => '10585',
	'fkSectionBestPractice' => '10581',
	'nControlType' => '0',
	'sName' => 'ME1.4 Performance Assessment (Beurteilung der Performance)',
	'tDescription' => 'Vergleiche in regelm��igen Abst�nden die Performance mit den Zielen, f�hre Ursachenanalysen (engl.: root cause analysis)
durch und ergreife Ma�nahmen, die die zugrunde liegenden Ursachen in Angriff zu nehmen.',
	'tImplementationGuide' => '',
),
189 => array(
	'fkContext' => '10586',
	'fkSectionBestPractice' => '10581',
	'nControlType' => '0',
	'sName' => 'ME1.5 Board and Executive Reporting (Berichte an gesch�ftsf�hrende Gremien)',
	'tDescription' => 'Erstelle Management-Berichte f�r den Review des Fortschritts der Organisation durch die Gesch�ftsf�hrung hinsichtlich der
identifizierten Ziele ? speziell in Bezug auf die Performance des Unternehmensportfolios von IT-gest�tzten
Investitionsprogrammen, auf die Service Levels individueller Programme und auf den Beitrag der IT zu dieser Performance.
Statusberichte sollten das Ausma� aufzeigen, wie geplante Ziele erreicht, Ergebnisse fertig gestellt, Performance-Ziele erreicht
und Risiken vermindert wurden. Nach dem Review sollten s�mtliche Abweichungen von der erwarteten Performance
identifiziert, geeignete Management-Aktivit�ten initiiert und dar�ber berichtet werden.',
	'tImplementationGuide' => '',
),
190 => array(
	'fkContext' => '10587',
	'fkSectionBestPractice' => '10581',
	'nControlType' => '0',
	'sName' => 'ME1.6 Remedial Actions (Verbesserungsma�nahmen)',
	'tDescription' => 'Identifiziere und initiiere Verbesserungsma�nahmen, welche basieren auf dem Monitoring, der Beurteilung und der
Berichterstattung �ber die Performance. Dies umfasst die Nachverfolgung aller �berwachungen, Berichterstattung und
Beurteilungen durch
? Review, Verhandlung und Herbeif�hrung von Reaktionen des Managements
? Zuweisung von Verantwortlichkeiten f�r die Verbesserung
? Verfolgung der Ergebnisse der eingeleiteten Ma�nahmen',
	'tImplementationGuide' => '',
),
191 => array(
	'fkContext' => '10589',
	'fkSectionBestPractice' => '10588',
	'nControlType' => '0',
	'sName' => 'ME2.1 Monitoring of internal control framework (Monitoring des Control Frameworks)',
	'tDescription' => 'Monitore laufend das IT Control-Umfeld und das Control Framework. Eine Bewertung unter Anwendung von Best Practices der
Industrie und Benchmarks sollte verwendet werden, um die IT-Control Umgebung und das Control Framework zu verbessern.',
	'tImplementationGuide' => '',
),
192 => array(
	'fkContext' => '10590',
	'fkSectionBestPractice' => '10588',
	'nControlType' => '0',
	'sName' => 'ME2.2 Supervisory review (�bergeordneter Review)',
	'tDescription' => 'Monitore die Wirksamkeit der Internal Controls �ber die IT durch einen �bergeordneten Review und berichte dar�ber ? unter
Einbezug von zB Einhaltung von Richtlinien und Normen, Informationssicherheit, Steuerung von Changes und in Service Level
Agreements aufgef�hrte Controls.',
	'tImplementationGuide' => '',
),
193 => array(
	'fkContext' => '10591',
	'fkSectionBestPractice' => '10588',
	'nControlType' => '0',
	'sName' => 'ME2.3 Control exceptions (Ausnahmebehandlung f�r Controls)',
	'tDescription' => 'Zeichne Informationen f�r alle Ausnahmen von Controls auf und stelle sicher, dass diese f�r die Analyse der grundlegenden
Ursachen und f�r Verbesserungsma�nahmen verwendet werden. Das Management sollte entscheiden, welche Ausnahmen an die
funktional verantwortliche Person kommuniziert werden und welche Ausnahmen eskaliert werden sollten. Das Management ist
auch f�r die Information der betroffenen Parteien verantwortlich.',
	'tImplementationGuide' => '',
),
194 => array(
	'fkContext' => '10592',
	'fkSectionBestPractice' => '10588',
	'nControlType' => '0',
	'sName' => 'ME2.4 Control self-assessment (Selbstbeurteilung der Steuerung)',
	'tDescription' => 'Evaluiere durch ein st�ndiges Programm zur Selbsteinsch�tzung die Vollst�ndigkeit und Wirksamkeit der Internal Control des
Managements �ber die IT-Prozesse, -Richtlinien und -Vertr�ge.',
	'tImplementationGuide' => '',
),
195 => array(
	'fkContext' => '10593',
	'fkSectionBestPractice' => '10588',
	'nControlType' => '0',
	'sName' => 'ME2.5 Assurance of internal control (Best�tigung der Internal Controls)',
	'tDescription' => 'Hole, wo notwendig, weitere Best�tigungen f�r die Vollst�ndigkeit und Wirksamkeit der Internal Controls durch Reviews von
Dritten ein. Solche Reviews k�nnen durch die Compliance-Funktion des Unternehmens oder ? auf Anfrage des Managements ?
durch Internal Audit oder durch extern beauftragte Pr�fer, Berater oder Zertifizierungsstellen durchgef�hrt werden. Die
Qualifikation der Personen, die diese Audits durchf�hren, muss sichergestellt sein, zB durch Zertifizierung als Certified
Information Systems Auditor? (CISA�).',
	'tImplementationGuide' => '',
),
196 => array(
	'fkContext' => '10594',
	'fkSectionBestPractice' => '10588',
	'nControlType' => '0',
	'sName' => 'ME2.6 Internal control at third parties (Internal Controls bei Dritten)',
	'tDescription' => 'Bewerte den Status der Internal Controls von s�mtlichen externen Dienstleistern. Best�tige, dass externe Dienstleister rechtliche
und regulatorische Anforderungen sowie vertragliche Verpflichtungen einhalten. Dies kann durch einen Audit durch Dritte
erfolgen oder durch ein Review der internen Audit-Funktion des Managements und den Ergebnissen der Pr�fungen.',
	'tImplementationGuide' => '',
),
197 => array(
	'fkContext' => '10595',
	'fkSectionBestPractice' => '10588',
	'nControlType' => '0',
	'sName' => 'ME2.7 Remedial actions (Verbesserungsma�nahmen)',
	'tDescription' => 'Identifiziere auf Basis von Berichten und Beurteilungen von Controls Verbesserungsma�nahmen und initiiere diese. Dies umfasst
eine Nachbearbeitung aller Beurteilungen und Berichte durch:
? Review, Verhandlung und Umsetzung von Reaktionen des Management
? Zuweisung von Verantwortung f�r die Verbesserung (kann auch die Risikoakzeptanz umfassen)
? Verfolgung der Ergebnisse der vereinbarten Aktivit�ten.',
	'tImplementationGuide' => '',
),
198 => array(
	'fkContext' => '10597',
	'fkSectionBestPractice' => '10596',
	'nControlType' => '0',
	'sName' => 'ME3.1 Identification of laws and regulations having potential impact on IT (Identifikation von Gesetzen und Regulativen
mit einer potentiellen Auswirkung auf die IT)',
	'tDescription' => 'Definiere und implementiere einen Prozess, um die zeitnahe Identifikation von lokalen und internationalen, durch Recht,
Vertr�ge, Richtlinien oder Regulative begr�ndeten Anforderungen an Informationen, Informationserbringung (inklusive der
Leistungen von Dritten) und die IT-Organisation, Prozesse und Infrastruktur, sicherzustellen. Beachte Gesetze und Vorschriften
des elektronischen Handelns, Datenfluss, Datenschutz, Internal Controls, Finanzberichterstattung, industriespezifische
Vorschriften, geistiges Eigentum und Urheberrecht sowie Gesundheit und Arbeitnehmersicherheit (engl.: Safety).',
	'tImplementationGuide' => '',
),
199 => array(
	'fkContext' => '10598',
	'fkSectionBestPractice' => '10596',
	'nControlType' => '0',
	'sName' => 'ME3.2 Optimisation of response to regulatory requirements (Optimierung der Reaktion auf regulatorische
Anforderungen)',
	'tDescription' => 'Reviewe und optimiere IT-Richtlinien, -Standards und -Verfahren, um sicherzustellen, dass rechtliche und regulatorische
Anforderungen in wirtschaftlicher Weise abgedeckt sind.',
	'tImplementationGuide' => '',
),
200 => array(
	'fkContext' => '10599',
	'fkSectionBestPractice' => '10596',
	'nControlType' => '0',
	'sName' => 'ME3.3 Evaluation of compliance with regulatory requirements (Evaluierung der Compliance mit regulatorischen
Anforderungen)',
	'tDescription' => 'Evaluiere in wirtschaftlicher Weise ? basierend auf der Governance-�bersicht und des Betriebs der Internal Controls des
Unternehmens- und IT-Managements ? die Einhaltung von IT-Richtlinien, Standards und Verfahren, inklusive rechtlicher und
regulativer Anforderungen.',
	'tImplementationGuide' => '',
),
201 => array(
	'fkContext' => '10600',
	'fkSectionBestPractice' => '10596',
	'nControlType' => '0',
	'sName' => 'ME3.4 Positive assurance of compliance (Positive Best�tigung der Compliance)',
	'tDescription' => 'Definiere und implementiere Verfahren, um eine positive Best�tigung der Compliance zu erhalten und dar�ber zu berichten ?
und, wo notwendig, �ber die rechtzeitige Einleitung von Verbesserungsma�nahmen durch die verantwortlichen Prozesseigner zur
Behandlung von Compliance-L�cken. Integriere die IT-Berichterstattung �ber den Fortschritt der Compliance und deren Status
mit �hnlichen Ergebnissen anderer Unternehmensfunktionen.',
	'tImplementationGuide' => '',
),
202 => array(
	'fkContext' => '10601',
	'fkSectionBestPractice' => '10596',
	'nControlType' => '0',
	'sName' => 'ME3.5 Integrated reporting (Integrierte Berichterstattung)',
	'tDescription' => 'Integriere die IT-Berichterstattung bez�glich regulativer Anforderungen mit �hnlichen Ergebnissen anderer
Unternehmensfunktionen.',
	'tImplementationGuide' => '',
),
203 => array(
	'fkContext' => '10603',
	'fkSectionBestPractice' => '10602',
	'nControlType' => '0',
	'sName' => 'ME4.1 Establishment of an IT governance framework (Einf�hrung eines IT-Governance-Frameworks)',
	'tDescription' => 'Arbeite mit der Gesch�ftsleitung, um ein IT-Governance Framework festzulegen und einzurichten, das F�hrung, Prozesse, Rollen
und Verantwortlichkeiten, Informationsbedarf und Organisationsstrukturen umfasst, um sicherzustellen, dass die IT-gest�tzten
Investitionsprogramme des Unternehmens an den Unternehmensstrategien und -zielen ausgerichtet sind und entsprechend diesen
arbeiten. Das Framework sollte eine klare Verbindung herstellen zwischen der Unternehmensstrategie, dem Portfolio von ITgest�tzten
Investitionsprogrammen, welche die Strategie umsetzen, den individuellen Investitionsvorhaben und den
Unternehmens- und IT-Projekten, die die Programme darstellen. Das Framework sollte unmissverst�ndliche Zust�ndigkeiten und
Praktiken unterst�tzen, um einen Zusammenbruch der Internal Controls und der Aufsicht zu vermeiden. Das Framework sollte
mit der unternehmensweiten Control Umgebung und allgemein akzeptierten Grunds�tzen f�r Control konsistent sein und auf dem
Framework f�r IT-Prozesse und Control basieren.',
	'tImplementationGuide' => '',
),
204 => array(
	'fkContext' => '10604',
	'fkSectionBestPractice' => '10602',
	'nControlType' => '0',
	'sName' => 'ME4.2 Strategic alignment (Strategische Ausrichtung)',
	'tDescription' => 'Erm�gliche der Gesch�ftsf�hrung, die strategischen IT-Belange wie die Rolle der IT, technologische Einblicke und
M�glichkeiten zu verstehen. Stelle sicher, dass ein gemeinsames Verst�ndnis zwischen dem Gesch�ftsbereich und der IT �ber
den potentiellen Beitrag der IT zur Unternehmensstrategie besteht. Stelle sicher, dass ein klares Verst�ndnis dar�ber besteht, dass
nur Wertbeitrag durch IT erzielt wird, wenn durch IT-gest�tzte Investitionen als ein Portfolio von Programmen gemanagt
werden, das den vollen Umfang der Changes ber�cksichtigt, die das Unternehmen umzusetzen hat, um den Wertbeitrag f�r die
Umsetzung der Strategie durch Potentiale der IT zu optimieren. Arbeite mit der Gesch�ftsleitung, um Governance-Gremien, wie
einen IT-Strategieausschuss festzulegen und zu implementieren, um strategische Vorgaben an das Management in Relation zur
IT zu erstellen, womit sichergestellt wird, dass die Strategie und Ziele in die Unternehmenseinheiten und die IT-Funktionen
herunter gebrochen werden und dass Zuversicht und Vertrauen zwischen dem Kerngesch�ft und der IT aufgebaut wird.
Erm�gliche die Ausrichtung der IT am Kerngesch�ft in strategischer und operativer Hinsicht durch die gemeinsame
Verantwortung von Kerngesch�ft und IT f�r das Treffen strategischer Entscheidungen und dem Erzielen von Nutzen aus ITgest�tzten
Investitionen.',
	'tImplementationGuide' => '',
),
205 => array(
	'fkContext' => '10605',
	'fkSectionBestPractice' => '10602',
	'nControlType' => '0',
	'sName' => 'ME4.3 Value delivery (Schaffen von Werten/Nutzen)',
	'tDescription' => 'Manage IT-gest�tzte Investitionsprogramme und andere Werte und Services der IT, um sicherzustellen, dass diese den
h�chstm�glichen Nutzen zur Unterst�tzung der Unternehmensstrategie und -ziele erbringen. Stelle sicher, dass der erwartete
Unternehmenserfolg von IT-gest�tzten Investitionsprogrammen und der gesamte Umfang des Aufwands, der f�r die Erreichung
dieses Erfolgs notwendig ist, verstanden wird, dass umfassende und konsistente Business-Cases und von Stakeholdern erstellt
und freigegeben werden, dass Verm�genswerte und Investitionen �ber ihren gesamten wirtschaftlichen Lebenszyklus verwaltet
werden und dass ein aktives Management der Realisierung des Nutzens vorhanden ist, wie zB der Wertbeitrag f�r neue Services,
Steigerung der Wirtschaftlichkeit und verbesserte Reaktion auf Kundenanfragen. Setze einen disziplinierten Ansatz f�r Portfolio-
, Programm- und Projektmanagement durch, bestehe darauf, dass vom Kerngesch�ft die Eigent�merschaft aller IT-gest�tzten
Investitionen �bernommen wird und dass die IT eine Optimierung der Kosten f�r die Bereitstellung von IT-Potentialen und
Services sicherstellt. Stelle sicher, dass Technologie-Investitionen so weit wie m�glich standardisiert sind, um erh�hte Kosten
und Komplexit�t eines Wildwuchses technischer L�sungen zu verhindern.',
	'tImplementationGuide' => '',
),
206 => array(
	'fkContext' => '10606',
	'fkSectionBestPractice' => '10602',
	'nControlType' => '0',
	'sName' => 'ME4.4 Resource management (Ressourcenmanagement)',
	'tDescription' => 'Optimiere die Investitionen in IT-Verm�genswerte, deren Verwendung und Belegung durch regelm��ige Beurteilungen, die
sicherstellen, dass die IT ausreichende, kompetente und f�hige Ressourcen hat, um die derzeitigen und k�nftigen strategischen
Ziele umzusetzen und mit dem Unternehmensbedarf mitzuhalten. Das Management sollte klare, konsistente und durchgesetzte
Human-Ressource- und Beschaffungs-Richtlinien einsetzen, um sicherzustellen, dass Ressourcenanforderungen wirksam und
entsprechend den Architektur-Richtlinien und Standards erf�llt werden. Die IT-Infrastruktur sollte in periodischen Abst�nden
beurteilt werden, um sicherzustellen, dass sie, wo immer m�glich, standardisiert ist und dass eine Interoperabilit�t, wo gefordert,
besteht.',
	'tImplementationGuide' => '',
),
207 => array(
	'fkContext' => '10607',
	'fkSectionBestPractice' => '10602',
	'nControlType' => '0',
	'sName' => 'ME4.5 Risk management (Risikomanagement)',
	'tDescription' => 'Arbeite mit der Gesch�ftsf�hrung, um die Risikobereitschaft des Unternehmens f�r IT-Risiken festzulegen. Kommuniziere die
IT-Risikobereitschaft im Unternehmen und vereinbare einen Plan zum IT-Risikomanagement. Bette die Verantwortlichkeiten f�r
Risikomanagement in die Organisation ein, um sicherzustellen, dass das Unternehmen und die IT regelm��ig die IT-bezogenen
Risiken und deren Auswirkungen auf das Gesch�ft beurteilt und dar�ber berichtet. Stelle sicher, dass das IT-Management
drohende Risiko-Gef�hrdungen behandelt und eine besondere Aufmerksamkeit auf das Versagen von IT-Controls und
Schwachstellen in Internal Controls und die Beaufsichtigung legt sowie auf deren tats�chliche und potentielle Auswirkungen auf
die Gesch�ftst�tigkeit. Die Haltung des Unternehmens bez�glich IT-Risiken sollte f�r alle Stakeholder transparent sein.',
	'tImplementationGuide' => '',
),
208 => array(
	'fkContext' => '10608',
	'fkSectionBestPractice' => '10602',
	'nControlType' => '0',
	'sName' => 'ME4.6 Performance measurement (Messung der Performance)',
	'tDescription' => 'Berichte dem Aufsichtsrat und der Gesch�ftsleitung rechtzeitig und genau �ber relevante Portfolios, Programme und die ITPerformance.
Die Managementberichte sollten f�r den Review der Entwicklung des Unternehmens hinsichtlich der festgelegten Ziele durch die Gesch�ftsf�hrung erstellt werden. Statusberichte sollten den Grad der Erreichung von Zielen, erstellte
Ergebnisse, erreichte Performancezahlen und verminderte Risiken umfassen. Integriere die Berichterstattung mit �hnlichen
Ergebnissen anderer Unternehmensfunktionen. Die Performance-Messung sollte durch die wichtigsten Stakeholder freigegeben
werden. Der Aufsichtsrat und die Gesch�ftsleitung sollte diese Performance-Berichte hinterfragen und dem IT-Management
sollte die Gelegenheit geboten werden, Abweichungen und Performance-Probleme zu erkl�ren. Nach dem Review sollten
geeignete Management-Ma�nahmen initiiert und gesteuert werden.',
	'tImplementationGuide' => '',
),
209 => array(
	'fkContext' => '10609',
	'fkSectionBestPractice' => '10602',
	'nControlType' => '1',
	'sName' => 'ME4.7 Independent assurance (Unabh�ngige Best�tigung)',
	'tDescription' => 'Stelle sicher, dass die Organisation eine kompetente und personell angemessen ausgestattete Funktion etabliert und betreibt
und/oder sich externer Pr�fungsleistungen bedient, um dem Aufsichtsrat ? wahrscheinlich �ber ein Audit-Committee ? eine
zeitgerechte, unabh�ngige Best�tigung der Compliance der IT mit ihren Richtlinien, Standards und Verfahren sowie mit
allgemein anerkannten Praktiken zu liefern.',
	'tImplementationGuide' => '',
),
210 => array(
	'fkContext' => '10612',
	'fkSectionBestPractice' => '10611',
	'nControlType' => '1',
	'sName' => 'Bestehen der Arbeitsbeschreibungsdokumente.',
	'tDescription' => 'Management spezifiziert das Niveau der Kompetenz ben�tigt f�r bestimmte Aufgaben und �bersetzt die gew�nschten Niveaus der Kompetenz in erforderliches Wissen und in F�higkeiten.',
	'tImplementationGuide' => 'Eine Probe von 25 Angestellten vorw�hlen und feststellen, ob Arbeitsbeschreibungen vorhanden sind.  Sicherheitszugangsaufzeichnungen f�r das Faltblatt erhalten, in dem Arbeitsbeschreibungen aufrechterhalten werden und feststellen, ob nicht HR Angestellte Nicht Personalabteilung Schreibzugriff haben.',
),
211 => array(
	'fkContext' => '10613',
	'fkSectionBestPractice' => '10611',
	'nControlType' => '1',
	'sName' => 'Beweis existiert, anzeigend, dass Angestellte das erforderliche Wissen und F�higkeiten haben.',
	'tDescription' => 'Management spezifiziert das Niveau der Kompetenz ben�tigt f�r bestimmte Aufgaben und �bersetzt die gew�nschten Niveaus der Kompetenz in erforderliches Wissen und F�higkeiten.',
	'tImplementationGuide' => 'Kopien der Einstellungsverfahren erhalten und feststellen, ob Verfahren Anleitungen zur Gew�hrleistung das Angestellte mit den erforderlichen F�higkeiten angestellt werden.  Eine Probe von 25 Angestellten vorw�hlen und merken, ob neu Auswertungen in �bereinstimmung mit FirmenFirmenpolitik vorhanden sind.  Merken, was ihre Bewertung ist und zur Glockenkurve entwerfen.  Wenn �berhaupt niedrig wurden Ausf�hrende in der Probe feststellen vorgew�hlt,,\"  wenn geeignete Schritte genommen wurden.  Unterlagen der Anforderungen f�r das j�hrliche Leistungsbeurteilungssystem beschaffen und eine Probe von 25 Angestellten vorw�hlen und feststellen, wenn Leistungsbeurteilungen in �bereinstimmung mit Firmenpolitik geleitet wurden.   \"',
),
212 => array(
	'fkContext' => '10615',
	'fkSectionBestPractice' => '10614',
	'nControlType' => '1',
	'sName' => 'Der Vorstand besteht aus den Mitgliedern, die vom Management unabh�ngig sind.',
	'tDescription' => 'Ein aktiver und wirkungsvoller Vorstand ,oder Aussch�sse,\"stellt davon eine Aufsichtsfunktion zur Verf�gung, wenn sie wirkungsvolle interne Steuerung sicherstellen.',
	'tImplementationGuide' => 'Die Verordnungen beschaffen und feststellen, ob Bericht von Unabh�ngigkeit angefordert wird.  Wenn jedermann w�hrend des laufenden Jahres zugelassen worden ist, die Unterlagen beschaffen, die diesen Bericht/Auswertung st�tzen.  Eine Probe von 5 Frageb�gen erhalten und feststellen, wenn Fragen betreffend Unabh�ngigkeit adressiert wurden.   ',
),
213 => array(
	'fkContext' => '10616',
	'fkSectionBestPractice' => '10614',
	'nControlType' => '1',
	'sName' => 'Der Vorstand ficht konstruktiv geplante strategische Initiativen der Entscheidungen des Managements,z.B.,strategische Initiativen und Haupttransaktionen an,und pr�ft die Ergebnisse vergangener Resultate.',
	'tDescription' => 'Ein aktiver und wirkungsvoller Vorstand, oder Aussch�sse, stellen eine wirkungsvolle interne Aufsichtsfunktion.',
	'tImplementationGuide' => 'Nehmen Sie eine Probe von Minuten von 10 Sitzungen des Vorstandes und der Aussch�sse (10 in der Gesamtmenge) erhalten und notieren Sie, ob Mitglieder pr�fende Fragen stellten\",\"ob Mitglieder pr�fende Fragen stellten und ob Finanzberichte (wenn angebracht)',
),
214 => array(
	'fkContext' => '10617',
	'fkSectionBestPractice' => '10614',
	'nControlType' => '1',
	'sName' => 'Vorstandsaussch�sse wie, z.B.,Audit-Ausschuss,\"Kredit-Ausschuss, werden verwendet wenn garantiert wird.',
	'tDescription' => 'Ein aktiver und wirkungsvoller Vorstand \",oder Aussch�sse,\"stellen eine Aufsichtsfunktion, um eine wirkungsvolle interne Steuerung sicherstellen.',
	'tImplementationGuide' => 'Der Vorstand hat die Aussch�sse eingef�hrt, die er als notwendig erachtet, Alamosas Notwendigkeiten  - kein Test erforderlich.',
),
215 => array(
	'fkContext' => '10618',
	'fkSectionBestPractice' => '10614',
	'nControlType' => '1',
	'sName' => 'Direktoren haben ausreichendes Wissen,Erfahrung in der Industrie,\" und Zeit fest, um effektiv zu dienen.',
	'tDescription' => 'Ein aktiver und wirkungsvoller Vorstand\",oder deren Aussch�sse,\"stellt eine Aufsichtsfunktion, um eine wirkungsvolle interne Steuerung sicherstellen.',
	'tImplementationGuide' => 'Erlangen Sie eine Probe von 5 Frageb�gen und stellen fest, ob Fragen betreffend Kompetenzen adressiert wurden.',
),
216 => array(
	'fkContext' => '10619',
	'fkSectionBestPractice' => '10614',
	'nControlType' => '1',
	'sName' => 'Der Audit-Ausschuss trifft die Finanzdirektoren,internen und externen Revisoren h�ufig.',
	'tDescription' => 'Ein aktiver und wirkungsvoller Vorstand, oder deren Aussch�sse,\" stellt eine Aufsichtsfunktion, um wirkungsvoll die interne Steuerung sicherzustellen.',
	'tImplementationGuide' => 'Erlangen Sie Protokolle von zwei Sitzungen des Jahres, um festzustellen, ob der Audit-Ausschuss den Finanzdirektor und die internen und externen Revisoren traf.',
),
217 => array(
	'fkContext' => '10620',
	'fkSectionBestPractice' => '10614',
	'nControlType' => '1',
	'sName' => 'Der Audit-Ausschuss pr�ft T�tigkeiten der internen und externen Revisoren j�hrlich.',
	'tDescription' => 'Ein aktiver und wirkungsvoller Vorstand ,oder dessen Aussch�sse,\"stellt eine Aufsichtsfunktion, um die interne Steuerung wirkungsvoll sicherzustellen.',
	'tImplementationGuide' => 'Erlangen Sie Protokole die die Befolgung der beschriebenen vorhandenen Steuerung aufzeigen.',
),
218 => array(
	'fkContext' => '10621',
	'fkSectionBestPractice' => '10614',
	'nControlType' => '1',
	'sName' => 'Der Audit-Ausschuss trifft sich privat mit dem Finanzdirektor und den internen und externen Revisoren, um das Finanzberichts-Prozesssystem zu beurteilen,das interne Steuersystem,bedeutende Anmerkungen und Empfehlungen, die Leistung des Managem',
	'tDescription' => 'Ein aktiver und wirkungsvoller Vorstand, oder dessen Aussch�sse,\" stellt eine Aufsichtsfunktion, um die interne Steuerung wirkungsvoll sicherzustellen.',
	'tImplementationGuide' => 'Erlangen Sie Protokole von zwei Sitzungen  des Jahres , um festzustellen, ob der Audit-Ausschuss sich privat mit den externen Revisoren traf.    ',
),
219 => array(
	'fkContext' => '10622',
	'fkSectionBestPractice' => '10614',
	'nControlType' => '1',
	'sName' => 'Der Vorstand erh�lt regelm��ig fristgerechte Schl�sselinformationen wie die Finanzberichte,bedeutende Vertr�ge ,und Hauptmarketing-Initiativen.',
	'tDescription' => 'Ein aktiver und wirkungsvoller Vorstand, oder dessen Aussch�sse,\"stellt eine Aufsichtsfunktion, um die interne Steuerung  wirkungsvoll sicherzustellen.',
	'tImplementationGuide' => 'erlangen Sie Protokolle von zwei Sitzungen des Jahres, um festzustellen, ob der Vorstand fristgerechte Informationen erhielt (z.B.\",Finanzberichte, etc.)',
),
220 => array(
	'fkContext' => '10623',
	'fkSectionBestPractice' => '10614',
	'nControlType' => '1',
	'sName' => 'Der Vorstand oder der Audit-Ausschuss ist zeitgleicht �ber Sensible Vorg�nge und Informationen zu unterrichten.  Ein Prozess existiert f�r das Informieren,Untersuchungen,\" und unangebrachte Vorg�nge.  ',
	'tDescription' => 'Erlangen Sie Protokolle von zwei Sitzungen des Jahres, um festzustellen, ob der Audit-Ausschuss fristgerechte Informationen �ber Betrugsf�lle\",und einen nachfolge Bericht zum aktuellen Stand',
	'tImplementationGuide' => 'Erlangen Sie Protokolle von zwei Sitzungen des Jahres, um festzustellen, ob der Audit-Ausschuss fristgerechte Informationen �ber Betrugsf�lle\",und einen nachfolge Bericht zum aktuellen Stand, etc. erhielt.',
),
221 => array(
	'fkContext' => '10624',
	'fkSectionBestPractice' => '10614',
	'nControlType' => '1',
	'sName' => 'Der Audit-Ausschuss besch�ftigt sich mit Entsch�digungen  und Einbehalten betreffend des internen Revisors.',
	'tDescription' => 'Ein aktiver und wirkungsvoller Vorstand, oder dessen Aussch�sse,\" stellt eine Aufsichtsfunktion, um die interne Steuerung wirkungsvoll sicherzustellen.',
	'tImplementationGuide' => 'Erlangen Sie Protokolle, die aufzeigen, dass der Audit-Ausschuss, Entsch�digungen und Eibehalten in Betracht gegen hat gegen�ber der von einer externen Stelle geleisteten Funktion der internen Revision.',
),
222 => array(
	'fkContext' => '10625',
	'fkSectionBestPractice' => '10614',
	'nControlType' => '1',
	'sName' => 'Der Ausgleichs-Ausschuss genehmigt alle Pr�mien f�r das Management, die an der Leistung gebunden werden.',
	'tDescription' => 'Ein aktiver und wirkungsvoller Vorstand, oder dessen Aussch�sse,\" stellt eine Aufsichtsfunktion, um die interne Steuerung wirkungsvoll sicherzustellen.',
	'tImplementationGuide' => 'Erlangen Sie Protokolle von zwei Sitzungen des Ausgleichs-Ausschusses und beurteilen sie, ob kurz- und langfristige Pr�mien richtig genehmigt wurden.        ',
),
223 => array(
	'fkContext' => '10626',
	'fkSectionBestPractice' => '10614',
	'nControlType' => '1',
	'sName' => 'Der Vorstand adressiert spezifisch die Orientierungsf�higkeit des Managements am Verhaltenskodex.',
	'tDescription' => 'Ein aktiver und wirkungsvoller Vorstand, oder dessen Aussch�sse,\"stellt eine Aufsichtsfunktion , um die interne Steuerung wirkungsvoll sicherzustellen.',
	'tImplementationGuide' => 'Erlangen Sie Protokolle, die aufzeigen, dass der Vorstand die Orientierungsf�higkeit des Managements am Verhaltenskodex im Gesch�ftsleben und Ethik adressierte.   ',
),
224 => array(
	'fkContext' => '10627',
	'fkSectionBestPractice' => '10614',
	'nControlType' => '1',
	'sName' => 'Der Vorstand und der Audit-Ausschuss werden zu gen�ge in die Beurteilung der Wirksamkeit miteinbezogen, von\\  \"\"\"\" tone at the top.\\ \"\"\"\"\"\"',
	'tDescription' => 'Ein aktiver und wirkungsvoller Vorstand, oder dessen Aussch�sse,\"stellt eine Aufsichtsfunktion, um die interne Steuerung wirkungsvoll sicherzustellen.',
	'tImplementationGuide' => ' Erlangen Sie die Protokolle, die anzeigen, dass der Vorstand den CEO und \\ \"\"\"\" tone at the top \\  \"\"\"\" auswertete, die er einstellt.',
),
225 => array(
	'fkContext' => '10628',
	'fkSectionBestPractice' => '10614',
	'nControlType' => '1',
	'sName' => 'Der Vorstand-und der Audit-Ausschuss werden zu gen�ge in die Beurteilung der Wirksamkeit miteinbezogen, von \\ \"\" tone at the top.\\ \"\"\"',
	'tDescription' => 'Ein aktiver und wirkungsvoller Vorstand, oder dessen Aussch�sse,\"stellt eine Aufsichtsfunktion, um die interne Steuerung wirkungsvoll sicherzustellen.',
	'tImplementationGuide' => ' Erlangen Sie Protokolle von zwei Sitzungen des Audit-Ausschusses und beurteilen sie, ob irgendwelche der Punkte, die \\ \"\"\"\" tone at the top \\ \"\"\"\" sich auswirken, angesprochen wurden. ',
),
226 => array(
	'fkContext' => '10629',
	'fkSectionBestPractice' => '10614',
	'nControlType' => '1',
	'sName' => 'Resultierend aus seinen Entdeckungen, ergreift der Vorstand-oder der Audit-Ausschuss Ma�nahmen einschlie�lich spezieller Untersuchungen,je nach Bedarf.',
	'tDescription' => 'Ein aktiver und wirkungsvoller Vorstand, oder dessen Aussch�sse,\"stellt eine Aufsichtsfunktion, um die interne Steuerung wirkungsvoll sicherzustellen.',
	'tImplementationGuide' => 'Erlangen Sie die Richtlinien des Leitung Ausschusses und beurteilen Sie, ob Richtlinien f�r Untersuchungen wie von Steuerung gefordert vorhanden sind.  Erlangen Sie Protokolle von zwei Sitzungen und beurteilen sie, ob die T�tigkeiten rund um die Untersuchungen (eine Untersuchung anfordern\",eine laufende Untersuchung mit verfolgen,etc.) adressiert werden.',
),
227 => array(
	'fkContext' => '10630',
	'fkSectionBestPractice' => '10614',
	'nControlType' => '1',
	'sName' => 'Der Audit-Ausschuss hat die Authorit�tsunabh�ngige Berater und andere Ratgeber zu verpflichten,so wie er es f�r richtig erachtet um seine Pflichten nachzukommen',
	'tDescription' => 'Ein aktiver und wirkungsvoller Vorstand, oder dessen Aussch�sse,\"stellt eine Aufsichtsfunktion, um die interne Steuerung wirkungsvoll sicherzustellen.',
	'tImplementationGuide' => 'Erlangen sie die gegenw�rtigen Richtlinien der Audit-Ausschusse erhalten und beurteilen Sie, ob die erforderlichen Bestimmungen enthalten sind.',
),
228 => array(
	'fkContext' => '10631',
	'fkSectionBestPractice' => '10614',
	'nControlType' => '1',
	'sName' => 'Der Audit-Ausschuss stellt eine Aufsichtsfunktion , um die interne Steuerung wirkungsvoll sicherzustellen.',
	'tDescription' => 'Der Audit-Ausschuss stellt eine Aufsichtsfunktion , um die interne Steuerung wirkungsvoll sicherzustellen.',
	'tImplementationGuide' => 'Erlangen Sie Kopien der Audit-Ausschuss-Sitzungstagesordnungen seit dem 3. Mai\", 2004 und stellen Sie fest ob der BetrugsTagesordnungspunkt enthalten ist.',
),
229 => array(
	'fkContext' => '10632',
	'fkSectionBestPractice' => '10614',
	'nControlType' => '1',
	'sName' => 'Die Firma hat jemand ernannt, um �ber Betrug und in Verbindung Stehendem dem Audit-Ausschuss zu berichten.',
	'tDescription' => 'Ein aktiver und wirkungsvoller Vorstand, oder dessen Aussch�sse,\"stellt eine Aufsichtsfunktion, um die interne Steuerung wirkungsvoll sicherzustellen.',
	'tImplementationGuide' => 'Erlangen Sie Protokolle und die Darstellungsmaterialien , die dieses aufzeigen.    ',
),
230 => array(
	'fkContext' => '10633',
	'fkSectionBestPractice' => '10614',
	'nControlType' => '1',
	'sName' => 'Es gibt formale Unterlagen �ber die Sitzungstagesordnungs,Protokolle,gemachte Pr�sentationen,etc. Unterlagen die mit Betrug in Verbindung stehendes dokumentieren ,einschlie�lich der gestellten Fragen.',
	'tDescription' => 'Ein aktiver und wirkungsvoller Vorstand, oder dessen Aussch�sse,\"stellt eine Aufsichtsfunktion , um die interne Steuerung wirkungsvoll sicherzustellen.',
	'tImplementationGuide' => ' Erlangen Sie Kopien der Audit-Ausschuss-Sitzungtagesordnungen seit dem 3. Mai\",\"2004 und stellen Sie fest, ob der BetrugsTagesordnungspunkt enthalten ist.\",,,,,,,,,,,Erlangen Sie Protokolle und die Darstellungsmaterialien, die dieses aufzeigen.
',
),
231 => array(
	'fkContext' => '10635',
	'fkSectionBestPractice' => '10634',
	'nControlType' => '1',
	'sName' => 'Management verfolgt eine Firmenpolitik, die die Risiken und Nutzen eines m�glichen Wagnisses analysiert.',
	'tDescription' => 'Management ist aktiv und wirkungsvoll, bei der Bereitstellung einer �bersicht,der Leitung und der Aufbereitung einer effektiv kontrollierbaren Umwelt.',
	'tImplementationGuide' => 'Erlange Sie die Unterlagen �ber die Praxis und die Prozesse und beurteilen Sie ob die Analyse von Nutzen und Risiko bei einem m�glichen Wagnis angesprochen werden.    ',
),
232 => array(
	'fkContext' => '10636',
	'fkSectionBestPractice' => '10634',
	'nControlType' => '1',
	'sName' => 'Das Niveau der Personalfluktuation in den Schl�sselfunktionen wie Buchhaltung,handhabung,Daten Verarbeitung,\"internes Audit ist niedrig.',
	'tDescription' => 'Management ist aktiv und wirkungsvoll, bei der Bereitstellung einer �bersicht,\",der Leitung und der Aufbereitung einer effektiv kontrollierbaren Umwelt.',
	'tImplementationGuide' => 'Keine Pr�fung notwendig.    ',
),
233 => array(
	'fkContext' => '10637',
	'fkSectionBestPractice' => '10634',
	'nControlType' => '1',
	'sName' => 'Bilanzierungsgrunds�tze werden nicht basierend auf vorteilhaften Finanzergebnissen der annehmbaren Grundregeln ausgew�hlt.',
	'tDescription' => 'Management ist aktiv und wirkungsvoll, bei der Bereitstellung einer �bersicht,, der Leitung und der Aufbereitung einer effektiv kontrollierbaren Umwelt.',
	'tImplementationGuide' => 'Keine Pr�fung notwendig.     ',
),
234 => array(
	'fkContext' => '10638',
	'fkSectionBestPractice' => '10634',
	'nControlType' => '1',
	'sName' => 'Einstellungen und T�tigkeiten gegen�ber Finanzberichten ,\"einschlie�lich Debatten �ber Anwendung der Buchhaltungbehandlungen.',
	'tDescription' => 'Management ist aktiv und wirkungsvoll, bei der Bereitstellung einer �bersicht,\",\" der Leitung und der Aufbereitung einer effektiv kontrollierbaren Umwelt.',
	'tImplementationGuide' => 'Erlangen Sie Kopien der Audit-Ausschuss-Sitzungs Protokolle von zwei Sitzungen stellen Sie fest, ob der Ausschuss die Buchhaltung und die Finanzberichtsprozesse beaufsichtigt.  ',
),
235 => array(
	'fkContext' => '10639',
	'fkSectionBestPractice' => '10634',
	'nControlType' => '1',
	'sName' => 'Das Betriebsmanagement zeichnet gemeldete Resultate ab.',
	'tDescription' => 'Management ist aktiv und wirkungsvoll, bei der Bereitstellung einer �bersicht,der Leitung und der Aufbereitung einer effektiv kontrollierbaren Umwelt.',
	'tImplementationGuide' => 'Erlangen Sie Begleitkarten von zwei viertelj�hrlichen (oder einem viertelj�hrlich und einem ganz j�hrigen) Finanzberichten,und stellen Sie fest ob es einen korrekten Abschluss gab.  ',
),
236 => array(
	'fkContext' => '10640',
	'fkSectionBestPractice' => '10634',
	'nControlType' => '1',
	'sName' => 'Einstellung des Managements gegen�ber den Datenverarbeitungs- und Buchhaltungsfunktionen,\" und Vertrauen auf Finanzbericht.',
	'tDescription' => 'Management ist aktiv und wirkungsvoll, bei der Bereitstellung einer �bersicht,\",der Leitung und der Aufbereitung einer effektiv kontrollierbaren Umwelt.',
	'tImplementationGuide' => 'Keine Pr�fung notwendig.',
),
237 => array(
	'fkContext' => '10641',
	'fkSectionBestPractice' => '10634',
	'nControlType' => '1',
	'sName' => 'Wertvolle Sachanlagen werden vor nicht autorisiertem Zugang oder Gebrauch gesch�tzt.',
	'tDescription' => 'Management ist aktiv und wirkungsvoll, bei der Bereitstellung einer �bersicht,,\"der Leitung und der Aufbereitung einer effektiv kontrollierbaren Umwelt.',
	'tImplementationGuide' => ' Beobachten Sie das Aufzeichnungssystem und erlangen Sie ein grundlegendes Verst�ndnis.  Beschreibens Sie ihre Resultate. erlangen Sie Kopien der Warenbestandreports vom Lager (5 Stichproben), um festzustellen, ob Anlageg�ter korrekt in die Best�nde aufgenomen wurden. Beschaffen Sie Systems unterlagen, die den Gebrauch von Ausweiskarten aufzeigen.',
),
238 => array(
	'fkContext' => '10642',
	'fkSectionBestPractice' => '10634',
	'nControlType' => '1',
	'sName' => 'Finanzielle Aktiva werden vor nicht autorisiertem Zugang oder Gebrauch gesch�tzt.',
	'tDescription' => 'Management ist aktiv und wirkungsvoll, bei der Bereitstellung einer �bersicht,,der Leitung und der Aufbereitung einer effektiv kontrollierbaren Umwelt.',
	'tImplementationGuide' => 'Einsehen der Pr�fung der Schatzkammer/ Forderungen.   ',
),
239 => array(
	'fkContext' => '10643',
	'fkSectionBestPractice' => '10634',
	'nControlType' => '1',
	'sName' => 'Computersysteme und Netzwerke,\"einschlie�lich intellektuelle Anlageg�ter werden vor nicht autorisiertem Zugang oder Gebrauch gesch�tzt.',
	'tDescription' => 'Management ist aktiv und wirkungsvoll, bei der Bereitstellung einer �bersicht,\",\"der Leitung und der Aufbereitung einer effektiv kontrollierbaren Umwelt.',
	'tImplementationGuide' => ' Beobachten Sie das Aufzeichnungssystem und erlangen Sie ein grundlegendes Verst�ndnis.  Beschreibens Sie ihre Resultate. erlangen Sie Kopien der Warenbestandreports vom Lager (5 Stichproben), um festzustellen, ob Anlageg�ter korrekt in die Best�nde aufgenommen und aufgezeichnet wurden. Beschaffen Sie Systems unterlagen, die den Gebrauch von Ausweiskarten aufzeigen.Sehen Sie die Informationssystemsteuerpr�fung f�r den Systemzugang ein.  NCN Pr�fung zum Schutz der physischen Unterlagen.

',
),
240 => array(
	'fkContext' => '10644',
	'fkSectionBestPractice' => '10634',
	'nControlType' => '1',
	'sName' => 'Senior Manager besuchen h�ufig die Ersatz- oder Unterabteilungen.  Sitzungen des Gruppen oder des Abteilungsmanagements finden h�ufig statt.',
	'tDescription' => 'Management ist aktiv und wirkungsvoll, bei der Bereitstellung einer �bersicht,,der Leitung und der Aufbereitung einer effektiv kontrollierbaren Umwelt.',
	'tImplementationGuide' => 'Keine Pr�fung notwendig.',
),
241 => array(
	'fkContext' => '10646',
	'fkSectionBestPractice' => '10645',
	'nControlType' => '1',
	'sName' => 'Die Organisationsstruktur erleichtert den Informationsfluss gegen den Strom,stromabw�rts,und �ber allen Gesch�ftst�tigkeiten.',
	'tDescription' => 'Die Executive ist sich ihrer Steuerungsfunktion bewusst und besitzt die erforderliche Erfahrung und den Wissensstand der der jeweiligen Position angemessen ist.',
	'tImplementationGuide' => 'Keine Pr�fung notwendig.',
),
242 => array(
	'fkContext' => '10647',
	'fkSectionBestPractice' => '10645',
	'nControlType' => '1',
	'sName' => 'Die Verantwortlichkeiten und Erwartungen f�r diese Gesch�fts Einheit werden offenbar an die f�r diese T�tigkeiten verantwortliche Executive mitgeteilt.',
	'tDescription' => 'Die Executive ist sich ihrer Steuerungsfunktion bewusst und besitzt die erforderliche Erfahrung und den Wissensstand der der jeweiligen Position angemessen ist.',
	'tImplementationGuide' => 'Beschaffen Sie Unterlagen mit den Zielsetzungen f�r den Planungsabschnitt Februar 2004  und  (wenn bereits vorhanden) f�r September 2004.                 ',
),
243 => array(
	'fkContext' => '10648',
	'fkSectionBestPractice' => '10645',
	'nControlType' => '1',
	'sName' => 'Erprobte Kommunikationswege bestehen und stellen Managern die Informationen zur Verf�gung, die zu ihren Verantwortlichkeiten und ihrem Berechtigungsfeld passend sind.  Die Exekutive diese Gesch�ftsfeldes hat Zugang zur Senior Exekutive.',
	'tDescription' => 'Die Executive ist sich ihrer Steuerungsfunktion bewusst und besitzt die erforderliche Erfahrung und den Wissensstand der der jeweiligen Position angemessen ist.;Keine Pr�fung notwendig.,,',
	'tImplementationGuide' => 'Keine Pr�fung notwendig.',
),
244 => array(
	'fkContext' => '10649',
	'fkSectionBestPractice' => '10645',
	'nControlType' => '1',
	'sName' => 'Management bewertet regelm��ig die Organisationsstruktur der Gesch�ftseinheit unter dem Aspekt der Ver�nderungen in der Gesch�ftswelt oder in der Industrie.',
	'tDescription' => 'Die Executive ist sich ihrer Steuerungsfunktion bewusst und besitzt die erforderliche Erfahrung und den Wissensstand der der jeweiligen Position angemessen ist.',
	'tImplementationGuide' => 'Keine Pr�fung notwendig.',
),
245 => array(
	'fkContext' => '10650',
	'fkSectionBestPractice' => '10645',
	'nControlType' => '1',
	'sName' => 'Manager und Aufsichtskr�fte haben gen�gende Zeit, ihre Verantwortlichkeiten effektiv durchzuf�hren.',
	'tDescription' => 'Die Executive ist sich ihrer Steuerungsfunktion bewusst und besitzt die erforderliche Erfahrung und den Wissensstand der der jeweiligen Position angemessen ist.',
	'tImplementationGuide' => 'Eine Probe von 15 Aktionspl�nen vorw�hlen und feststellen, ob Stichdaten gesetzt wurden und ob die  verantwortlichen Parteien dem Plan zugestimmt haben. ',
),
246 => array(
	'fkContext' => '10652',
	'fkSectionBestPractice' => '10651',
	'nControlType' => '1',
	'sName' => 'Verantwortlichkeit f�r Entscheidungen ist verbunden mit der Ernennung von Verantwortlichen.',
	'tDescription' => 'Zuweisung der Verantwortlichkeit und Delegation von Autorit�t, mit organisatorischen Ziel- und ZielsetzungsLinienfunktionen umzugehen,Betriebsfunktionen,und der vorgeschriebenen Anforderungen,\"einschlie�lich Verantwortlichkeit f�r Informationssysteme und Erm�chtigung f�r �nderungen.',
	'tImplementationGuide' => 'Stellen Sie fest, ob von Verantwortlichen erstellte Zeitpl�ne den Angestellten leicht zug�nglich sind\",auf dem neusten Stand sind und genehmigt wurden.',
),
247 => array(
	'fkContext' => '10653',
	'fkSectionBestPractice' => '10651',
	'nControlType' => '1',
	'sName' => '�bertragene Autorit�t ist angebracht in Verbindung mit zugewiesenen Pflichten.',
	'tDescription' => 'Angewiesene Pflicht und Authorit�ts�bertragung, f�r den Umgang mit organisatorischen Ziel- und ZielsetzungsLinien,Betriebsfunktionen, und Vorschriften,\"einschlie�lich Verantwortlichkeit f�r Informationssysteme und Erm�chtigung f�r �nderungen.',
	'tImplementationGuide' => ' Stellen Sie fest, ob ein Zeitplan der Authorit�tstr�ger f�r die  Angestellten leicht zug�ngig ist\",auf dem neusten Stand ist und genehmigt wurde.  ,,,,,,,,,,,,,,Die Korrekte Zustimmung zum Zeitplan ist Basis f�r die die korrekt autorisiert wurden.',
),
248 => array(
	'fkContext' => '10654',
	'fkSectionBestPractice' => '10651',
	'nControlType' => '1',
	'sName' => 'geeignete Informationen werden in Betrachtet gezogen, wenn man die Hierarchieebene und den Umfang der Verantwortung die einer Einzelperson zugewiesen wurden, feststellt.',
	'tDescription' => 'Angewiesene Pflicht und Authorit�ts�bertragung, f�r den Umgang mit organisatorischen Ziel- und ZielsetzungsLinien, Betriebsfunktionen,und Vorschriften,\"einschlie�lich Verantwortlichkeit f�r Informationssysteme und Erm�chtigung f�r �nderungen.',
	'tImplementationGuide' => 'Stellen Sie fest, ob ein Zeitplan der Authorit�tstr�ger f�r die  Angestellten leicht zug�ngig ist \",auf dem neusten Stand ist und genehmigt wurde.     ,,,,,,,,,,,,,,Korrekte Zustimmung zum Zeitplan ist Basis f�r die Betroffenen um geeignet autorisiert zu werden.
',
),
249 => array(
	'fkContext' => '10655',
	'fkSectionBestPractice' => '10651',
	'nControlType' => '1',
	'sName' => 'Arbeitsbeschreibungen enthalten spezifische Hinweise, um in Verbindung stehende Verpflichtungen zu steuern.',
	'tDescription' => 'Angewiesene Pflicht und Authorit�ts�bertragung, f�r den Umgang mit organisatorischen Ziel- und ZielsetzungsLinien, Betriebsfunktionen,und Vorschriften,\"einschlie�lich Verantwortlichkeit f�r Informationssysteme und Erm�chtigung f�r �nderungen.',
	'tImplementationGuide' => 'W�hlen Sie eine Probe von 10 Arbeitsbeschreibungen der Angestellten aus der Buchhaltung und Finanz, die spezifische Finanzberichtssteuerverantwortlichkeiten haben und stellen Sie fest, ob die Beschreibungen Verantwortlichkeiten enthalten der Steuerung in Zusammenhang stehen.',
),
250 => array(
	'fkContext' => '10657',
	'fkSectionBestPractice' => '10656',
	'nControlType' => '1',
	'sName' => 'Bestehen und Implementierung von Verhaltenskodexen und von anderer Firmen Firmenpolitik annehmbare Gesch�ftsverfahren betreffend, Interessenkonflikte,\"oder zu erwartender Standard des ethischen und moralischen Verhaltens.',
	'tDescription' => 'Management zeigt, dass Integrit�t und ethische Werte nicht eingeb�sst werden k�nnen.',
	'tImplementationGuide' => 'Beschaffen Sie die Richtlinien zum  Gesch�ftsverfahren und Ethik und pr�fen sie sie auf die Einbeziehung von Verhaltenskodexen und anderer Firmen Firmenpolitik betreffend annehmbare Gesch�ftsverfahren\",Interessenkonflikte,oder zu erwartenden Standards des ethischen und moralischen Verhaltens. ',
),
251 => array(
	'fkContext' => '10658',
	'fkSectionBestPractice' => '10656',
	'nControlType' => '1',
	'sName' => 'Einrichtung von \\ \"\" tone at the top,\"\\ \"\"\"\" ausdr�ckliche moralische Anleitung von, was recht und unrecht ist und Ausweitung der Kommunikation auf die Gesamtheit der Organisation .\"\"',
	'tDescription' => 'Management zeigt, dass Integrit�t und ethische Werte nicht eingeb�st werden k�nnen',
	'tImplementationGuide' => ' (1) Beschaffen Sie die Richtlinien zum  Gesch�ftsverfahren und Ethik und pr�fen sie  \\ \" tone at the top \\ \" - z.B.\",\"die Erwartung der Firma in bezug auf ethisches und moralisches Verhaltens.  (2) Beobachten sie, ob Informantenplakate f�r Denunzianten und die Alamosa Plakate, die seine Kernaussagen adressieren, In B�ros und Verkaufsst�tten angebracht worden sind(10 �rtlichkeiten).  (3) �berpr�fen Sie das Intranet und stellen Sie fest, ob Alamosas Kernaussagen angezeigt werden.  (4) erlangen und �berpr�fen die Kriterien f�r die Meister Vereine und stellen Sie fest, ob sie mit Alamosas Kernaussagen in Einklang sind. ',
),
252 => array(
	'fkContext' => '10659',
	'fkSectionBestPractice' => '10656',
	'nControlType' => '1',
	'sName' => 'Umgang mit Angestellten,Lieferanten,Kunden,Investoren, Gl�ubigern, Versicherern , Konkurrenten, Revisoren, etc.,\"finden auf einer ethisch hohen Ebene statt.',
	'tDescription' => 'Management zeigt, dass Integrit�t und ethische Werte nicht eingeb�sst werden k�nnen.',
	'tImplementationGuide' => 'Beschaffen Sie die Richtlinien bez�glich Gesch�ftsverfahren und Ethik und stellen sie fest , ob der Umgang mit Angestellten, Lieferanten ,Kunden, Investoren, Gl�ubigern,Versicherern, Konkurrenten, Revisoren, etc., adressiert, dass diese auf einer ethisch hohen Ebene geleitet werden sollten.
',
),
253 => array(
	'fkContext' => '10660',
	'fkSectionBestPractice' => '10656',
	'nControlType' => '1',
	'sName' => 'Geeignete Ma�nahmen im Falle des Abweichens von der Verhaltensrichtlinien ,FirmenFirmenpolitik,\"oder vom Verhaltenskodex.',
	'tDescription' => 'Management zeigt, dass Integrit�t und ethische Werte nicht eingeb�sst werden k�nnen.',
	'tImplementationGuide' => 'Erlangen sie das Handbuch f�r Angestellte und das Kaufsausgleichshandbuch beobachten Sie, ob die progressive Disziplin enthalten ist.
',
),
254 => array(
	'fkContext' => '10661',
	'fkSectionBestPractice' => '10656',
	'nControlType' => '1',
	'sName' => 'Managementintervention, wenn bew�hrte Kontrollen aufgehoben werden, wird dokumentiert.  Abweichungen von bew�hrter Firmenpolitik werden untersucht und dokumentiert.',
	'tDescription' => 'Management zeigt, dass Integrit�t und ethische Werte nicht eingeb�sst werden k�nnen.',
	'tImplementationGuide' => 'Beschaffen Sie die Richtlinien bez�glich Gesch�ftsverfahren, Ethik-u.das Angestelltenhandbuch und stellen Sie fest, ob spezifische F�lle zur Kontrolle Aufhebung bereitstehen und sich im Einklang mit Alamosas Kernaussagenn sowohl wie guten Gesch�ftsverfahren befinden.  Beachten Sie  ob Managementzustimmung angefordert wird.',
),
255 => array(
	'fkContext' => '10662',
	'fkSectionBestPractice' => '10656',
	'nControlType' => '1',
	'sName' => 'Leistungsziele pr�fen nicht unn�tigerweise die F�higkeit der Menschen, ethische Werte zu befolgen.',
	'tDescription' => 'Management zeigt, dass Integrit�t und ethische Werte nicht eingeb�sst werden k�nnen.',
	'tImplementationGuide' => '(1) Beschaffen Sie die Richtlinien bez�glich Gesch�ftsverfahren, Ethik und �berpr�fen ihn auf die Einrichtung von \\\"\" tone at the top \\  - d.h.,\"die Erwartung der Firma an das ethische und moralische Verhalten.  (2) Beobachten sie, ob Informantenplakate f�r Denunzianten und die Alamosa Plakate, die seine Kernaussagen adressieren, In B�ros und Verkaufsst�tten angebracht worden sind(10 �rtlichkeiten).  (3) �berpr�fen Sie das Intranet und stellen Sie fest, ob Alamosas Kernaussagen angezeigt werden.  (4) erlangen und �berpr�fen die Kriterien f�r die Meister Vereine und stellen Sie fest, ob sie mit Alamosas Kernaussagen in Einklang sind. Erlangen Sie das Angestellten Handbuch und das Verkaufsausgleichshandbuch und beachten Sie, ob die progressive Disziplin enthalten ist.

',
),
256 => array(
	'fkContext' => '10663',
	'fkSectionBestPractice' => '10656',
	'nControlType' => '1',
	'sName' => 'Der F�hrungsstab hat einen Ethik Codex angenommen.',
	'tDescription' => 'Management zeigt, dass Integrit�t und ethische Werte nicht eingeb�sst werden k�nnen.',
	'tImplementationGuide' => 'Erlangen Sie den Codex f�r Gesch�ftsverhalten und Ethik
stellen Sie fest, ob erfolgende Punkte adressiert:(1) ehrliche und ethische F�hrung,einschlie�lich der ethischen Handhabung tats�chlicher oder scheinbarer Interessenkonflikte innerhalb pers�nlicher und Beruflicher-Beziehungen
(2) volle,angemessene,genaue,fristgerechte,\"verst�ndliche Offenbarung in den periodischen Reports erfordert, von der Firma archiviert zu werden
(3) Befolgung der zutreffenden Regierungsrichtlinien und Vorschriften.

',
),
257 => array(
	'fkContext' => '10664',
	'fkSectionBestPractice' => '10656',
	'nControlType' => '1',
	'sName' => 'Die Art und Weise wie Bericht erstattet wird �ber die Verletzungen des Codex im Gesch�ftsverfahren und Ethik sind dokumentiert,\"und wem die Verletzungen berichtet werden sollten.',
	'tDescription' => 'Management zeigt, dass Integrit�t und ethische Werte nicht eingeb�sst werden k�nnen.',
	'tImplementationGuide' => 'Beschaffen Sie den Codex im Gesch�ftsverfahren und Ethik und stellen Sie fest, ob er die Art und Weise wie, �ber die Verletzungen des Codex, Bericht erstattet wird an die entsprechenden Individuen umfasst.',
),
258 => array(
	'fkContext' => '10665',
	'fkSectionBestPractice' => '10656',
	'nControlType' => '1',
	'sName' => 'Die Art und Weise wie Bericht erstattet wird �ber die Verletzungen des Codex im Gesch�ftsverfahren und Ethik sind dokumentiert,\"und wem die Verletzungen berichtet werden sollten.',
	'tDescription' => 'Management zeigt, dass Integrit�t und ethische Werte nicht eingeb�sst werden k�nnen.',
	'tImplementationGuide' => 'Beschaffen Sie den Codex im Gesch�ftsverfahren und Ethik und stellen Sie fest, ob er die Art und Weise wie, �ber die Verletzungen des Codex, Bericht erstattet wird an die entsprechenden Individuen umfasst.',
),
259 => array(
	'fkContext' => '10666',
	'fkSectionBestPractice' => '10656',
	'nControlType' => '1',
	'sName' => 'Es gibt Unterlagen �ber die Annahme des Codex im Gesch�ftsverfahren und Ethik.',
	'tDescription' => 'Management zeigt, dass Integrit�t und ethische Werte nicht eingeb�sst werden k�nnen.',
	'tImplementationGuide' => 'W�hlen Sie eine Probe von 25 Angestellten und beschaffen Sie die unterzeichneten Zur Kenntnisnahme Formulare die jene an die personal Abteilung zur�ckgaben.',
),
260 => array(
	'fkContext' => '10667',
	'fkSectionBestPractice' => '10656',
	'nControlType' => '1',
	'sName' => 'Vorsorge f�r die Korrektur-Ma�nahmen, die im Zusammenhang mit Verletzungen des Codex im Gesch�ftsverfahren und Ethik greifen, wurde getroffen.',
	'tDescription' => 'Management zeigt, da? Vollst�ndigkeit und ethische Werte nicht gekompromittiert werden k�nnen.',
	'tImplementationGuide' => 'Beschaffen Sie den Codex im Gesch�ftsverfahren und Ethik und stellen Sie fest, ob Bestimmungen f�r die Korrektur-Ma�nahmen, die im Zusammenhang mit Verletzungen des Codex greifen zur Verf�gung gestellt wurden.',
),
261 => array(
	'fkContext' => '10668',
	'fkSectionBestPractice' => '10656',
	'nControlType' => '1',
	'sName' => 'Ein anti-Betrugs Programm und entsprechende Kontrollen existieren.',
	'tDescription' => 'Management zeigt, dass Integrit�t und ethische Werte nicht eingeb�sst werden k�nnen.',
	'tImplementationGuide' => 'Erlangen Sie eine Kopie des \"Mein Sicherer Arbeitsplatz\" Vertrages.  Sehen Sie andere Tests ein, die die Vermittlung von Alamosas Werten adressieren und ihre Haltung zum Betrug. Pr�fung der Aufgabentrennung wird in den Steuermatrizen f�r die verschiedenen Zyklen gedeckt. ',
),
262 => array(
	'fkContext' => '10670',
	'fkSectionBestPractice' => '10669',
	'nControlType' => '1',
	'sName' => 'Die �berpr�fungen der Vorgeschichte der Angestellten sind vollzogen und werden als ad�quat erachtet,\"besonders hinsichtlich der vorangegangenen T�tigkeiten oder Aktivit�ten die im Wesen als inakzeptabel gelten.
',
	'tDescription' => 'Die Personal Abteilung ist in der Steuerumwelt aktiv, indem sie garantieren, dass passende Firmenpolitik und Praxis angenommen wird\",dass qualifizierte Angestellte die Integrit�t vorweisen eingestellt werden,\"dass Angestellte richtig �berwacht und und ausgebildet werden.',
	'tImplementationGuide' => 'W�hlen Sie eine Probe von 15 Angestellten, die w�hrend des laufenden Jahres eingestellt wurden und stellen Sie fest, ob �berpr�fungen (wenn als notwendig erachtet), durchgef�hrt wurden (wenn die Position Fahren erfordert\",vergewissern Sie sich dass eine F�hrerscheinuntersuchung erfolgt ist).',
),
263 => array(
	'fkContext' => '10671',
	'fkSectionBestPractice' => '10669',
	'nControlType' => '1',
	'sName' => 'PersonalFirmenpolitik und -verfahren resultieren in der Einstellung die F�rderung vertrauensw�rdiger Menschen die n�tig sind um ein funktionierendes internes Kontrollsystem zu st�tzen.',
	'tDescription' => 'Die Personalabteilung ist in der Steuerumwelt aktiv, indem sie garantieren, dass entsprechende Firmenpolitik und Praxis angewandt wird,dass qualifizierte Angestellte die Integrit�t vorweisen eingestellt werden,\"und dass Angestellte richtig �berwacht und ausgebildet werden.',
	'tImplementationGuide' => '�berpr�fen Sie Die Angestelltenrichtlinien und stellen  Sie fest, ob die passende Sprache, die auf der erwarteten Steuerung basiert, existiert.',
),
264 => array(
	'fkContext' => '10672',
	'fkSectionBestPractice' => '10669',
	'nControlType' => '1',
	'sName' => 'PersonalFirmenpolitik und -verfahren resultieren in Ausbildung,\"f�rdern und verg�ten Angestellte ein funktionierendes internes Kontrollsystem zu st�tzen.',
	'tDescription' => 'Die Personalabteilung ist in der Steuerumwelt aktiv, indem sie garantieren, dass entsprechende  Firmenpolitik und Praxis angewandt wird\",dass qualifizierte Angestellte die Integrit�t vorweisen eingestellt werden, und dass Angestellte richtig �berwacht und ausgebildet werden.',
	'tImplementationGuide' => 'Keine Pr�fung notwendig.',
),
265 => array(
	'fkContext' => '10673',
	'fkSectionBestPractice' => '10669',
	'nControlType' => '1',
	'sName' => 'Angestellte werden auf ihre Verpflichtungen und die Erwartungen des Managements an Sie aufmerksam gemacht.',
	'tDescription' => 'Die Personalabteilung ist in der Steuerumwelt aktiv, indem sie garantieren, dass entsprechende FirmenFirmenpolitik und Praxis angewandt wird,dass qualifizierte Angestellten die Integrit�t vorweisen eingestellt werden,\"und dass Angestellte richtig �berwacht und ausgebildet werden.',
	'tImplementationGuide' => 'W�hlen Sie eine Probe von 25 Angestellten und stellen Sie fest, ob Arbeitsbeschreibungen vorhanden sind. Erlangen sie Sicherheit Zugang zu den Unterlagen und dem Ordner in dem  Arbeitsbeschreibungen gehalten  werden und stellen Sie fest, ob Angestellte die nicht zur Personalabteilung geh�ren Schreibzugriff haben.',
),
266 => array(
	'fkContext' => '10674',
	'fkSectionBestPractice' => '10669',
	'nControlType' => '1',
	'sName' => 'Integriet�t und ethische Werte sind Kriterien der Leistungsbewertungen.',
	'tDescription' => 'Die Personalabteilung ist in der Steuerumwelt aktiv, indem sie garantieren, dass entsprechende FirmenFirmenpolitik und Praxis angewandt wird,dass qualifizierte Angestellte die Integrit�t vorweisen eingestellt werden,\"und dass Angestellte richtig �berwacht und ausgebildet werden.',
	'tImplementationGuide' => 'Erlangen sie einen Auswertungsbogen und stellen Sie fest, ob die Wortwahl der vorhandenen Steuerung in Form ist.',
),
267 => array(
	'fkContext' => '10675',
	'fkSectionBestPractice' => '10669',
	'nControlType' => '1',
	'sName' => 'Angestellte schlie�en die Orientierung ab und die Willf�hrigkeit mit der Orientierung sowie die Beendigung der erforderlichen Module wird von der Personalabteilung mitverfolgt.',
	'tDescription' => 'Angestellte werden in Hinsicht auf Ethik und Befolgungsverpflichtung sowie dem Wert der Befolgung der FirmenFirmenpolitik und der Verfahren ausgebildet',
	'tImplementationGuide' => 'W�hlen Sie eine Probe von 15 Angestellten, die w�hrend des laufenden Jahres eingestellt wurden und stellen Sie fest, ob Orientierung Formulare von den Angestellten unterzeichnet wurden.',
),
268 => array(
	'fkContext' => '10677',
	'fkSectionBestPractice' => '10676',
	'nControlType' => '1',
	'sName' => 'Die Einheit umfassende Zielsetzungen liefern ausreichend deutliche Aussagen und Einf�hrung in was die Einheit zu erzielen w�nscht.',
	'tDescription' => 'Damit eine Einheit einen effektiven Kontrolle Mechanismus hat,\"m�ssen die Zielsetzungen feststehen.',
	'tImplementationGuide' => 'Stellen Sie fest, ob strategische Planungsdokumente f�r vor kurzem abgehaltene Strategie Sitzungen existieren.',
),
269 => array(
	'fkContext' => '10678',
	'fkSectionBestPractice' => '10676',
	'nControlType' => '1',
	'sName' => 'Die Einheit umfassende Zielsetzungen werden effektiv an die Angestellten und an den Verwaltungsrat mitgeteilt.',
	'tDescription' => 'Damit eine Einheit einen effektiven Kontrolle Mechanismus hat,m�ssen die Zielsetzungen feststehen.',
	'tImplementationGuide' => 'Keine Pr�fung notwendig.',
),
270 => array(
	'fkContext' => '10679',
	'fkSectionBestPractice' => '10676',
	'nControlType' => '1',
	'sName' => 'Strategien sind mit den die Einheit umfassenden Zielsetzungen in Einklang.',
	'tDescription' => 'Damit eine Einheit einen effektiven Kontrolle Mechanismus hat,m�ssen die Zielsetzungen feststehen.',
	'tImplementationGuide' => 'Keine Pr�fung notwendig.',
),
271 => array(
	'fkContext' => '10680',
	'fkSectionBestPractice' => '10676',
	'nControlType' => '1',
	'sName' => 'Gesch�ftspl�ne und Budget sind mit den die Einheit umfassenden Zielsetzungen in Einklang.',
	'tDescription' => 'Damit eine Einheit einen effektiven Kontrolle Mechanismus hat,m�ssen die Zielsetzungen feststehen.',
	'tImplementationGuide' => 'Keine Pr�fung notwendig.',
),
272 => array(
	'fkContext' => '10682',
	'fkSectionBestPractice' => '10681',
	'nControlType' => '1',
	'sName' => 'Die Zielsetzung der Stufe der Betriebsamkeit wird mit den die Einheit umfassenden Zielsetzungen und den strategischen Pl�nen gekoppelt.',
	'tDescription' => 'Zielsetzungen sollten f�r jede bedeutende T�tigkeit festgesteckt werden und jene Zielsetzung der Stufe der Betriebsamkeit sollten gleich bleibend sein.',
	'tImplementationGuide' => 'Keine Pr�fung notwendig.',
),
273 => array(
	'fkContext' => '10683',
	'fkSectionBestPractice' => '10681',
	'nControlType' => '1',
	'sName' => 'Die Zielsetzung der Stufe der Betriebsamkeit ist f�r alle bedeutenden Gesch�ftsprozesse relevant.',
	'tDescription' => 'Zielsetzungen sollten f�r jede bedeutende T�tigkeit festgesteckt werden und jene Zielsetzung der Stufe der Betriebsamkeit sollten gleich bleibend sein.',
	'tImplementationGuide' => 'Keine Pr�fung notwendig.',
),
274 => array(
	'fkContext' => '10684',
	'fkSectionBestPractice' => '10681',
	'nControlType' => '1',
	'sName' => 'Betriebsmittel sind im Verh�ltnis zu den Zielsetzungen ausreichend.',
	'tDescription' => 'Zielsetzungen sollten f�r jede bedeutende T�tigkeit festgesteckt werden und jene Zielsetzung der Stufe der Betriebsamkeit sollten gleich bleibend sein.',
	'tImplementationGuide' => 'Beschafen Sie den neusten Haushaltsplan und stellen Sie fest, ob er korrekt in �bereinstimmung mit dem strategischen Planung Prozess genehmigt wurde.',
),
275 => array(
	'fkContext' => '10685',
	'fkSectionBestPractice' => '10681',
	'nControlType' => '1',
	'sName' => 'Zielsetzungen (kritische Erfolgsfaktoren) die f�r die Erreichung der die Einheit umfassenden Zielsetzungen wichtig sind, werden benannt.',
	'tDescription' => 'Zielsetzungen sollten f�r jede bedeutende T�tigkeit festgesteckt werden und jene Zielsetzung der Stufe der Betriebsamkeit sollten gleich bleibend sein.',
	'tImplementationGuide' => 'Beschaffen Sie das strategische Planungsdokument und stellen Sie fest, ob organisatorische Zielsetzungen benannt wurden.',
),
276 => array(
	'fkContext' => '10686',
	'fkSectionBestPractice' => '10681',
	'nControlType' => '1',
	'sName' => 'Alle Stufen des Managements sind an der Festsetzung von Zielsetzungen betieligt.',
	'tDescription' => 'Zielsetzungen sollten f�r jede bedeutende T�tigkeit festgesteckt werden und jene Zielsetzung der Stufe der Betriebsamkeit sollten gleich bleibend sein.',
	'tImplementationGuide' => 'Keine Pr�fung notwendig.',
),
277 => array(
	'fkContext' => '10688',
	'fkSectionBestPractice' => '10687',
	'nControlType' => '1',
	'sName' => 'Die Mechanismen um Risiken zu erkennen, die sowohl aus externen wie internen Quellen entspringen k�nnen, sind ausreichend.',
	'tDescription' => 'Der Risikobeurteilungsproze� des Unternehmens sollte die Implikationen der relevanten Risiken am des Unternehmens selbst sowohl wie  der Stufe der Betriebsamkeit beachten.',
	'tImplementationGuide' => 'Stellen Sie fest, ob ein anerkannter (durch den Audit-Ausschuss) interner Audit Plan existiert und auf einer Risikobeurteilung basiert.',
),
278 => array(
	'fkContext' => '10689',
	'fkSectionBestPractice' => '10687',
	'nControlType' => '1',
	'sName' => 'Der Risikoanalysenproze�,einschlie�lich das Festsetzen von Bedeutung und von Wahrscheinlichkeit und die Bestimmung von T�tigkeiten ist vollst�ndig und relevant.',
	'tDescription' => 'Der Risikobeurteilungsproze� des Unternehmens sollte die Implikationen der relevanten Risiken am des Unternehmens selbst sowohl wie  der Stufe der Betriebsamkeit beachten.',
	'tImplementationGuide' => 'Keine Pr�fung notwendig.',
),
279 => array(
	'fkContext' => '10690',
	'fkSectionBestPractice' => '10687',
	'nControlType' => '1',
	'sName' => 'Es gibt eine systematische,dokumentierte Ann�herung zum Festsetzen des Risikos von Betrug und der in Verbindung stehenden Risiken (Betrugsrisikofaktoren).',
	'tDescription' => 'Der Risikobeurteilungsproze� des Unternehmens sollte die Implikationen der relevanten Risiken am des Unternehmens selbst sowohl wie  der Stufe der Betriebsamkeit beachten.',
	'tImplementationGuide' => 'N/A - Kontrolle Unterlagen noch nicht an der richtigen Stelle.',
),
280 => array(
	'fkContext' => '10691',
	'fkSectionBestPractice' => '10687',
	'nControlType' => '1',
	'sName' => 'Abschw�chungkontrollen werden in Bezug auf gekennzeichnete Betrug bezogene Risiken gekennzeichnet.',
	'tDescription' => 'Der Risikobeurteilungsproze� des Unternehmens sollte die Implikationen der relevanten Risiken am des Unternehmens selbst sowohl wie  der Stufe der Betriebsamkeit beachten.',
	'tImplementationGuide' => 'N/A - Kontrolle Unterlagen noch nicht an der richtigen Stelle.',
),
281 => array(
	'fkContext' => '10692',
	'fkSectionBestPractice' => '10687',
	'nControlType' => '1',
	'sName' => 'Die Betrugs Risikobeurteilung umfasst Betrachtung von Risiken der Management�bersteuerung ,Manipulation von Sch�tzungen,Genauigkeit und Vollst�ndigkeit der Finanzberichte Offenbarungen und  MD&A.',
	'tDescription' => 'Der Risikobeurteilungsproze� des Unternehmens sollte die Implikationen der relevanten Risiken am des Unternehmens selbst sowohl wie  der Stufe der Betriebsamkeit beachten.',
	'tImplementationGuide' => 'N/A - Kontrolle Unterlagen noch nicht an der richtigen Stelle.',
),
282 => array(
	'fkContext' => '10693',
	'fkSectionBestPractice' => '10687',
	'nControlType' => '1',
	'sName' => 'Als solcher erkannter Betrug und in Verbindung stehende Ausgaben werden zur Kennzeichnung zus�tzlicher Risikobereiche herangezogen.',
	'tDescription' => 'Der Risikobeurteilungsproze� des Unternehmens sollte die Implikationen der relevanten Risiken am des Unternehmens selbst sowohl wie  der Stufe der Betriebsamkeit beachten.',
	'tImplementationGuide' => 'Beschafen Sie eine Probe von 15 IS-erzeugten Reports, die verwendet werden, um Bereiche des h�heren Risikos,und beachten Sie ob Unterlagen einer Durchsicht durch das Management bez�glich existieren.',
),
283 => array(
	'fkContext' => '10694',
	'fkSectionBestPractice' => '10687',
	'nControlType' => '1',
	'sName' => 'Es gibt eine systematische,\" dokumentierte Ann�herung zur �berpr�fung von Betrug und der in Verbindung stehenden Dinge.',
	'tDescription' => 'Der Risikobeurteilungsproze� des Unternehmens sollte die Implikationen der relevanten Risiken am des Unternehmens selbst sowohl wie  der Stufe der Betriebsamkeit beachten.',
	'tImplementationGuide' => 'Beschafen Sie die Unterlagen des Finanzdirektors und stellen Sie fest, ob die Berichterstattung des Betrugsberichts dokumentiert wird (auf einer viertelj�hrlichen Basis).',
),
284 => array(
	'fkContext' => '10696',
	'fkSectionBestPractice' => '10695',
	'nControlType' => '1',
	'sName' => 'Mechanismen existieren, um vorauszusehen,zu  identifizieren,\"und zu reagieren auf Routineereignisse oder auf Aktivit�ten, die die Leistung des Unternehmens selbst sowohl wie  die Stufe der Betriebsamkeit beeinflussen.',
	'tDescription' => 'Mechanismen m�ssen implementiert sein, um Ver�nderungen der Bedingungen zu erkennen und auf solche zu reagieren.',
	'tImplementationGuide' => 'Beschaffen Sie eine Probe von 10 Aktionspl�nen, die f�r einzelne Ziele verwendet werden.  Stellen Sie fest, ob die Unterlagen diese Ziele betreffend �berwacht wurden.   ',
),
285 => array(
	'fkContext' => '10697',
	'fkSectionBestPractice' => '10695',
	'nControlType' => '1',
	'sName' => 'Mechanismen existieren, um Ver�nderungen zu erkennen und auf sie zu reagieren die, einen drastischeren Effekt auf die Einheit haben k�nnen und diese werden an die  Spitze des Managements weitergeleitet.',
	'tDescription' => 'Mechanismen m�ssen implementiert sein, um Ver�nderungen der Bedingungen zu erkennen und auf solche zu reagieren.',
	'tImplementationGuide' => 'Beschafen Sie die Unterlagen die von Finanzdirektor gehalten werden und stellen Sie fest, ob Belange die Organisation jedes Hauptleiters betreffend  diskutiert wurden.',
),
286 => array(
	'fkContext' => '10699',
	'fkSectionBestPractice' => '10698',
	'nControlType' => '1',
	'sName' => 'Entsprechende FirmenFirmenpolitik und notwendige Prozeduren,in Anbetracht einer jeden T�tigkeit des Unternehmens,\" existieren.',
	'tDescription' => 'Entsprechende FirmenFirmenpolitik und notwendige Prozeduren,\",\"in Anbetracht einer jeden T�tigkeit des Unternehmens,\",\"existieren und werden richtig angewendet.',
	'tImplementationGuide' => 'Dieses wird in jedem der Zyklen bedacht, die durch 404 Pr�fungen erfasst werden - weitere Pr�fung hier f�hren.',
),
287 => array(
	'fkContext' => '10700',
	'fkSectionBestPractice' => '10698',
	'nControlType' => '1',
	'sName' => 'Existierende gekennzeichnete Kontrollaktivit�ten werden richtig angewendet.',
	'tDescription' => 'Entsprechende FirmenFirmenpolitik und notwendige Prozeduren, ,\"in Anbetracht einer jeden T�tigkeit des Unternehmens,\",\"existieren und werden richtig angewendet.',
	'tImplementationGuide' => 'Dieses wird in jedem der Zyklen bedacht, die durch 404 Pr�fungen erfasst werden - weitere Pr�fung hier f�hren.',
),
288 => array(
	'fkContext' => '10701',
	'fkSectionBestPractice' => '10698',
	'nControlType' => '1',
	'sName' => 'Untersuchungsprotokolle sind implementiert die, sich auf die Weiterverfolgung von Betrug und in Zusammenhang stehenden Dingen beziehen.',
	'tDescription' => 'Entsprechende FirmenFirmenpolitik und notwendige Prozeduren,,\"in Anbetracht einer jeden T�tigkeit des Unternehmens,\",existieren und werden richtig angewendet.',
	'tImplementationGuide' => 'N/A - Kontrolle Unterlagen noch nicht an der richtigen Stelle.',
),
289 => array(
	'fkContext' => '10703',
	'fkSectionBestPractice' => '10702',
	'nControlType' => '1',
	'sName' => 'Mechanismen wurden implementiert um relevante externe Informationen - �ber Marktkonditionen,Konkurrenten Programme,Entwicklung der Legislative und der Vorschriften,und �konomische �nderungen zu erlangen.',
	'tDescription' => 'Eine wirkungsvolle Informationsinfrastruktur existiert.',
	'tImplementationGuide' => 'Keine Pr�fung notwendig.',
),
290 => array(
	'fkContext' => '10704',
	'fkSectionBestPractice' => '10702',
	'nControlType' => '1',
	'sName' => 'Die intern erzeugten Informationen,\"die f�r das Erreichen der Unternehmensziele kritisch sind,\",sind erkannt und werden regelm��ig berichtet.',
	'tDescription' => 'Eine wirkungsvolle Informationsinfrastruktur existiert.',
	'tImplementationGuide' => 'Die verschiedenen Berichte und die Kontrollen �ber die erhaltenen Daten werden in den jeweiligen 404 Zyklen gepr�ft. Weitere Pr�fung hier f�hren.',
),
291 => array(
	'fkContext' => '10705',
	'fkSectionBestPractice' => '10702',
	'nControlType' => '1',
	'sName' => 'Manager erhalten analytische Informationen, die ihnen erm�glicht, zu erkennen, welche Ma�nahmen ergriffen werden m�ssen.',
	'tDescription' => 'Eine wirkungsvolle Informationsinfrastruktur existiert.',
	'tImplementationGuide' => 'Die verschiedenen Berichte und die Kontrollen �ber die erhaltenen Daten werden in den jeweiligen 404 Zyklen gepr�ft. Weitere Pr�fung hier f�hren.',
),
292 => array(
	'fkContext' => '10706',
	'fkSectionBestPractice' => '10702',
	'nControlType' => '1',
	'sName' => 'Informationen werden mit der Detailintensit�t der Stufe des Management entsprechend bereit gestellt.',
	'tDescription' => 'Eine wirkungsvolle Informationsinfrastruktur existiert.',
	'tImplementationGuide' => 'Die verschiedenen Berichte und die Kontrollen �ber die erhaltenen Daten werden in den jeweiligen 404 Zyklen gepr�ft. Weitere Pr�fung hier f�hren.',
),
293 => array(
	'fkContext' => '10707',
	'fkSectionBestPractice' => '10702',
	'nControlType' => '1',
	'sName' => 'Informationen werden Zeitnah bereitgestellt, was eine wirkungsvolle �berwachung von Ereignissen und Aktivit�ten erlaubt.',
	'tDescription' => 'Eine wirkungsvolle Informationsinfrastruktur existiert.',
	'tImplementationGuide' => 'Die verschiedenen Berichte und die Kontrollen �ber die erhaltenen Daten werden in den jeweiligen 404 Zyklen gepr�ft. Weitere Pr�fung hier f�hren.',
),
294 => array(
	'fkContext' => '10708',
	'fkSectionBestPractice' => '10702',
	'nControlType' => '1',
	'sName' => 'Hauptleiter mit ausreichend umfassenden Verantwortlichkeiten stellen Informationsbedarf und Priorit�ten fest.',
	'tDescription' => 'Eine wirkungsvolle Informationsinfrastruktur existiert.',
	'tImplementationGuide' => 'Stellen Sie fest, welche Gutachten w�hrend des laufenden Jahres erstellt wurden und w�hlen Sie eine passende Probe, um festzustellen, ob den Zielsetzungen des Gutachtens entsprochen wurde.',
),
295 => array(
	'fkContext' => '10709',
	'fkSectionBestPractice' => '10702',
	'nControlType' => '1',
	'sName' => 'Betriebsmittel werden zu gen�ge bereitgestellt, um Informationssysteme zu entwickeln bzw. zu verbessern.',
	'tDescription' => 'Eine wirkungsvolle Informationsinfrastruktur existiert.',
	'tImplementationGuide' => 'Beschafens Sei den Haushaltsplan und den gesch�ftsplan und stellen Sie fest ob der IS Bedarf angemessen bedacht wurde.',
),
296 => array(
	'fkContext' => '10711',
	'fkSectionBestPractice' => '10710',
	'nControlType' => '1',
	'sName' => 'Kommunikationstr�ger sind ausreichend zur Kommunikation beweltigung.',
	'tDescription' => 'Entsprechende Informationen werden zeitnahe und wirkungsvollen weitergeleitet.',
	'tImplementationGuide' => 'Keine Pr�fung notwendig.',
),
297 => array(
	'fkContext' => '10712',
	'fkSectionBestPractice' => '10710',
	'nControlType' => '1',
	'sName' => 'Angestellte kennen die Zielsetzungen ihrer eigenen T�tigkeiten.',
	'tDescription' => 'Passende Informationen werden zeitnahe und wirkungsvoll weitergeleitet.',
	'tImplementationGuide' => 'Von der Probe die aus der Auswertungen erlangt wurde von\\ \"\" Beweis existiert der aufzeig dass die Angestellten die N�tigen Kenntnisse und F�higkeiten t, anzeigend, dass Angestellte das erforderliche Wissen und die F�higkeiten haben \\\"\" stellen Sie fest ob einzelne Ziele adressiert wurden.',
),
298 => array(
	'fkContext' => '10713',
	'fkSectionBestPractice' => '10710',
	'nControlType' => '1',
	'sName' => 'Es gibt einen Kanal �ber den man direkt nach oben kommunizieren kann, �ber eine andere Person als seinen direkten Vorgesetzten.',
	'tDescription' => 'Passende Informationen werden zeitnahe und wirkungsvoll weitergeleitet.',
	'tImplementationGuide' => '(1) Beschaffen Sie den Codex f�r Gesch�ftsverhalten und Ethik und �berpr�fen ihn hinsichtlich der Einrichtung von \\ \"\" tone at the top \\\"\" - d.h.,\"die Erwartung der Firma an das ethische und moralische Verhalten.  (2) Beobachten Sie, ob Informantenplakate und die Alamosa Plakate, die seine Kernaussage adressieren, in B�ros und in Speichern angebracht wurden (10 Lokalit�ten).  (3) Pr�fen Sie das Intranet und stellen Sie fest, ob Alamosas Kernaussagen dirt sichtbar sind. (4) Beschaffen und �berpr�fen Sie die Kriterien f�r den Meister Verein und stellen Sie fest, ob sie mit Alamosas Kernaussagen in Einklang sind.',
),
299 => array(
	'fkContext' => '10714',
	'fkSectionBestPractice' => '10710',
	'nControlType' => '1',
	'sName' => 'Menschen die �ber verd�chtige Ungereimtheiten Bericht erstatten, erhalten Resonanz und Immunit�t vor Repressalien.',
	'tDescription' => 'Passende Informationen werden zeitnahe und wirkungsvoll weitergeleitet.',
	'tImplementationGuide' => 'Keine Pr�fung notwendig.',
),
300 => array(
	'fkContext' => '10715',
	'fkSectionBestPractice' => '10710',
	'nControlType' => '1',
	'sName' => 'Informationen werden �ber die Gesamtheit der Organisation vollst�ndig und zeitnahe weitergeleitet, um Leuten zu erm�glichen, ihre Verpflichtungen zu erf�llen.',
	'tDescription' => 'Passende Informationen werden in einer zeitnahen und wirkungsvollen Weise mitgeteilt.',
	'tImplementationGuide' => 'Keine Pr�fung notwendig.',
),
301 => array(
	'fkContext' => '10716',
	'fkSectionBestPractice' => '10710',
	'nControlType' => '1',
	'sName' => 'Kan�le mit Kunden,Lieferanten,und anderen externen Parteien um notwendige Ver�nderungen zu kommunizieren sind ge�ffnet und wirkungsvoll.',
	'tDescription' => 'Passende Informationen werden in einer zeitnahen und wirkungsvollen Weise mitgeteilt.',
	'tImplementationGuide' => 'Keine Pr�fung notwendig.',
),
302 => array(
	'fkContext' => '10717',
	'fkSectionBestPractice' => '10710',
	'nControlType' => '1',
	'sName' => 'Ausenstehende Parteien sind auf den ethischen Standard des Unternehmens aufmerksam gemacht worden.',
	'tDescription' => 'Passende Informationen werden in einer zeitnahen und wirkungsvollen Weise mitgeteilt.',
	'tImplementationGuide' => 'Sehen Sie sich die verwiesene Webseite an und stellen Sie fest, ob die Werte dort verzeichnet werden und die Seite von der Home Page aus leicht zug�nglich ist.',
),
303 => array(
	'fkContext' => '10718',
	'fkSectionBestPractice' => '10710',
	'nControlType' => '1',
	'sName' => 'Unstimmigkeiten bei Angestellten oder Au�enstehenden Parteien werden das zust�ndige Personal berichtet.',
	'tDescription' => 'Passende Informationen werden in einer zeitnahen und wirkungsvollen Weise mitgeteilt.',
	'tImplementationGuide' => 'Das Angestellten on-line-Handbuch ansehen und feststellen, ob es Dokumentation gibt die aufzeigt, wie Unstimmigkeiten adressiert werden sollen.',
),
304 => array(
	'fkContext' => '10719',
	'fkSectionBestPractice' => '10710',
	'nControlType' => '1',
	'sName' => 'Die Berichte die von Kunden erhalten werden, werden entsprechend und zeitnahe verfolgt',
	'tDescription' => 'Passende Informationen werden in einer zeitnahen und wirkungsvollen Weise mitgeteilt.',
	'tImplementationGuide' => 'Keine Pr�fung notwendig',
),
305 => array(
	'fkContext' => '10720',
	'fkSectionBestPractice' => '10710',
	'nControlType' => '1',
	'sName' => 'Den Kommunikationen, die von den Regulierern oder von anderen Au�enstehenden Parteien erhalten werden, wird passend und zeitnah nachgegangen.',
	'tDescription' => 'Passende Informationen werden zeitnah und wirkungsvoll mitgeteilt.',
	'tImplementationGuide' => 'N/A - N/A - Kontrolle Unterlagen noch nicht implementiert.',
),
306 => array(
	'fkContext' => '10721',
	'fkSectionBestPractice' => '10710',
	'nControlType' => '1',
	'sName' => 'Fehler im Kundenrechnungswesen werden behoben und der Fehler wird untersucht und behoben.',
	'tDescription' => 'Passende Informationen werden zeitnah und wirkungsvoll mitgeteilt.',
	'tImplementationGuide' => 'Der kontrollierte Teil der Korrekturen ist bedeutungslos,folglich Durchlaufpr�fung in diesem Bereich.',
),
307 => array(
	'fkContext' => '10722',
	'fkSectionBestPractice' => '10710',
	'nControlType' => '1',
	'sName' => 'Entsprechendes  Personal,\"nicht in Verbindung stehend mit denen die bei der urspr�nglichen Transaktion involviert waren,\",\"bearbeitet Beanstandungen.',
	'tDescription' => 'Passende Informationen werden zeitnahe und wirkungsvoll mitgeteilt.',
	'tImplementationGuide' => 'Beschaffen sie die Organisations�bersicht , um festzustellen, ob diese zwei Bereiche richtig getrennt werden.',
),
308 => array(
	'fkContext' => '10723',
	'fkSectionBestPractice' => '10710',
	'nControlType' => '1',
	'sName' => 'Die Bedeutsamkeit um das Wissen von betrug und in Zusammenhang stehendem,\"so wie die Ansichten der Firma zu diesen Punkten werden effektiv mitgeteilt.',
	'tDescription' => 'Passende Informationen werden zeitnah und wirkungsvoll mitgeteilt.',
	'tImplementationGuide' => 'Erlangen Sie eine Kopie des Mein Sicherer Arbeitsplatz Vertrages.  Sehen Sie weitere Test ein die Alamosas Werte und Stellung zum Betrug adressieren.  Pr�fung der Aufgabentrennung wird in den Steuermatrizen f�r die verschiedenen Zyklen behandelt. \"
',
),
309 => array(
	'fkContext' => '10724',
	'fkSectionBestPractice' => '10710',
	'nControlType' => '1',
	'sName' => 'Ein Verfahren ist implementiert um zu gew�hrleisten, dass Betrug und in Verbindungstehendes dem Audit-Ausschuss zu geh�r gebracht wird.',
	'tDescription' => 'Passende Informationen werden in zeitnah und wirkungsvoll mitgeteilt.',
	'tImplementationGuide' => 'Erlangen sie die Aufzeichnungen, die vom Manager des internen Audit bei der Sitzung gemacht werden, um festzustellen, ob Betrugsangelegenheiten angesprochen wurden.',
),
310 => array(
	'fkContext' => '10726',
	'fkSectionBestPractice' => '10725',
	'nControlType' => '1',
	'sName' => 'Personal,\"w�hrend sie ihre regelm��igen T�tigkeiten ausf�hren,\",\"erh�lt Beweise, ob das System der internen Kontrolle fortf�hrt zu arbeiten.',
	'tDescription' => 'Es gibt eine fortw�hrende �berwachung der Eignung\",\"Vollst�ndigkeit und der Wirksamkeit der Kontrollumgebung des Unternehmens.',
	'tImplementationGuide' => 'Dieses wird in jedem der Zyklen gedeckt, die durch 404 pr�fend erfasst werden - weitere Pr�fung hier f�hren.',
),
311 => array(
	'fkContext' => '10727',
	'fkSectionBestPractice' => '10725',
	'nControlType' => '1',
	'sName' => 'Mitteilungen von Au�enstehenden Parteien bekr�ftigen intern erzeugte Informationen.',
	'tDescription' => 'Es gibt eine fortw�hrende �berwachung der Eignung,\"Vollst�ndigkeit und der Wirksamkeit der Kontrollumgebung des Unternehmens.',
	'tImplementationGuide' => 'Dieses wird in den relevanten Zyklen bedeckt, die durch 404 pr�fend umfasst werden - weitere Pr�fung hier f�hren.',
),
312 => array(
	'fkContext' => '10728',
	'fkSectionBestPractice' => '10725',
	'nControlType' => '1',
	'sName' => 'KIm Allgemeinen zahlen Kunden Rechnungen, oder Kundenbeanstandungen von Rechnungen werden auf die zu Gr�nde liegenden Ursachen untersucht.',
	'tDescription' => 'Es gibt eine fortw�hrende �berwachung der Eignung,Vollst�ndigkeit und der Wirksamkeit der Kontrollumgebung des Unternehmens.',
	'tImplementationGuide' => 'Keine Pr�fung notwendig.',
),
313 => array(
	'fkContext' => '10729',
	'fkSectionBestPractice' => '10725',
	'nControlType' => '1',
	'sName' => 'Lieferanten Beanstandungen betreffend unfairer Praktiken durch Einkaufsagenten werden vollst�ndig untersucht.',
	'tDescription' => 'Es gibt eine fortw�hrende �berwachung der Eignung,\"Vollst�ndigkeit und der Wirksamkeit der Kontrollumgebung des Unternehmens.',
	'tImplementationGuide' => 'Beschaffen Sie das P&P, das durch die Einkaufsabteilung erstellt wird\",\"und stellen Sie fest, ob sie die Lieferantenbeanstandungen angebracht adressiert.',
),
314 => array(
	'fkContext' => '10730',
	'fkSectionBestPractice' => '10725',
	'nControlType' => '1',
	'sName' => 'Die Mengen, die durch das Rechnungssystem notiert werden, werden regelm��ig mit Sachanlagen verglichen.',
	'tDescription' => 'Es gibt eine fortw�hrende �berwachung der Eignung,\"Vollst�ndigkeit und der Wirksamkeit der Kontrollumgebung des Unternehmens.',
	'tImplementationGuide' => ' Beobachten Sie das Aufsp�rungssystem und erreichen ein grundlegendes Verst�ndnis.  beschreiben Sie die Resultate. Beschaffen Sie Kopien der Warenbestandreports vom Lager (eine Probe von 5), um festzustellen, ob die Anlageg�ter richtig aufgenommen u. verfolgt werden. Beschaffen Sie Systems unterlagen, die Stellen aufzeigen wo  Ausweiskarten  gebraucht werden.
',
),
315 => array(
	'fkContext' => '10731',
	'fkSectionBestPractice' => '10725',
	'nControlType' => '1',
	'sName' => 'teile der Exekutive mit der entsprechenden Berechtigung entscheiden, welchen Auditorempfehlungen folgend Mittel zur St�rkung der internen Kontrollen eingef�hrt werden.',
	'tDescription' => 'Es gibt eine fortw�hrende �berwachung der Eignung,',
	'tImplementationGuide' => 'Keine Pr�fung notwendig.',
),
316 => array(
	'fkContext' => '10732',
	'fkSectionBestPractice' => '10725',
	'nControlType' => '1',
	'sName' => 'Personal wird regelm��ig aufgefordert, Befolgung des Verhaltenskodex zu best�tigen.',
	'tDescription' => 'Es gibt eine fortw�hrende �berwachung der Eignung,Vollst�ndigkeit und der Wirksamkeit der Kontrollumgebung des Unternehmens.',
	'tImplementationGuide' => 'W�hlen Sie eine Probe von 25 Angestellten und beschaffen Sie die unterzeichneten Best�tigungsformulare die diese an die Personalabteilung abgaben.',
),
317 => array(
	'fkContext' => '10733',
	'fkSectionBestPractice' => '10725',
	'nControlType' => '1',
	'sName' => 'Der interne Audit hat eine entsprechende Position innerhalb der Organisation.',
	'tDescription' => 'Es gibt eine fortw�hrende �berwachung der Eignung,\"Vollst�ndigkeit und der Wirksamkeit der Kontrollumgebung des Unternehmens.',
	'tImplementationGuide' => 'Beschafen Sie die internen Tabellarischen Bericht des Audit und stellen Sie fest, ob die passende Wortwahl enthalten ist.',
),
318 => array(
	'fkContext' => '10734',
	'fkSectionBestPractice' => '10725',
	'nControlType' => '1',
	'sName' => 'Belegschaftsmitglieder sind erfahren und kompetent.',
	'tDescription' => 'Es gibt eine fortw�hrende �berwachung der Eignung, Vollst�ndigkeit und der Wirksamkeit der Kontrollumgebung des Unternehmens.',
	'tImplementationGuide' => 'Keine Pr�fung notwendig.',
),
319 => array(
	'fkContext' => '10735',
	'fkSectionBestPractice' => '10725',
	'nControlType' => '1',
	'sName' => 'Interner Audit hat Zugang zum Audit-Ausschuss.',
	'tDescription' => 'Es gibt eine fortw�hrende �berwachung der Eignung,\"Vollst�ndigkeit und der Wirksamkeit der Kontrollumgebung des Unternehmens.',
	'tImplementationGuide' => 'Beschafen Sie die internen Tabellarischen Bericht des Audit und stellen Sie fest, ob die passende Wortwahl enthalten ist.',
),
320 => array(
	'fkContext' => '10736',
	'fkSectionBestPractice' => '10725',
	'nControlType' => '1',
	'sName' => 'Betriebspersonal ist gefordert \\ \"\"abzuzeichnen\\ \"\"auf die Vollst�ndigkeit der Finanzinformationen ihrer Einheit und wird f�r die entdeckte Fehler verantwortlich gemacht.',
	'tDescription' => 'Es gibt eine fortw�hrende �berwachung der Eignung,Vollst�ndigkeit und der Wirksamkeit der Kontrollumgebung des Unternehmens.',
	'tImplementationGuide' => 'W�hlen Sie zwei ICQ\'s (interne Kontrollfrage b�gen) die zu jedem Quartal abschlossen werden und stellen Sie fest ob korrekte Abzeichnung von dem entsprechenden Prozessinhaber erreicht wurde.',
),
321 => array(
	'fkContext' => '10737',
	'fkSectionBestPractice' => '10725',
	'nControlType' => '1',
	'sName' => 'Unterschriften werden angefordert, um Leistung der spezifischen Kontrollt�tigkeiten/-Funktionen zu beweisen.',
	'tDescription' => 'Es gibt eine fortw�hrende �berwachung der Eignung, Vollst�ndigkeit und der Wirksamkeit der Kontrollumgebung des Unternehmens.',
	'tImplementationGuide' => 'W�hlen Sie zwei ICQ\'s (interne Kontrollfrage b�gen) die zu jedem Quartal abschlossen werden und stellen Sie fest ob korrekte Abzeichnung von dem entsprechenden Prozessinhaber erreicht wurde.',
),
322 => array(
	'fkContext' => '10739',
	'fkSectionBestPractice' => '10738',
	'nControlType' => '1',
	'sName' => 'Passende Teile der internen Kontrollsysteme werden beewertet.',
	'tDescription' => 'Es gibt eine wirkungsvolle und objektive Bewertung der Eignung,Vollst�ndigkeit und der Wirksamkeit der Kontrollumgebung des Unternehmens.',
	'tImplementationGuide' => 'Dieses wird als Teil der 404 Pr�fungen bewertet folglich Durchlaufpr�fung hier.',
),
323 => array(
	'fkContext' => '10740',
	'fkSectionBestPractice' => '10738',
	'nControlType' => '1',
	'sName' => 'Die Bewertung wurde vom Personal mit den erforderlichen F�higkeiten geleitet. ',
	'tDescription' => 'Es gibt eine wirkungsvolle und objektive Bewertung der Eignung,Vollst�ndigkeit und der Wirksamkeit der Kontrollumgebung des Unternehmens. ',
	'tImplementationGuide' => 'Dieses wird als Teil der 404 Pr�fungen bewertet folglich Durchlaufpr�fung hier',
),
324 => array(
	'fkContext' => '10741',
	'fkSectionBestPractice' => '10738',
	'nControlType' => '1',
	'sName' => 'Der Auswertungsproze� ist zutreffend.',
	'tDescription' => 'Es gibt eine wirkungsvolle und objektive Bewertung der Eignung,Vollst�ndigkeit und der Wirksamkeit der Kontrollumgebung des Unternehmens.',
	'tImplementationGuide' => 'Dieses wird als Teil der 404 pr�fen folglich Durchlaufpr�fung hier ausgewertet.',
),
325 => array(
	'fkContext' => '10742',
	'fkSectionBestPractice' => '10738',
	'nControlType' => '1',
	'sName' => 'Ein Verst�ndnis wird gewonnen, wie das System arbeiten sollte und wie es wirklich funktioniert',
	'tDescription' => 'Es gibt eine wirkungsvolle und objektive Bewertung der Eignung , Vollst�ndigkeit und der Wirksamkeit der Kontrollumgebung des Unternehmens.',
	'tImplementationGuide' => 'Dieses wird als Teil der 404 pr�fen folglich Durchlaufpr�fung hier ausgewertet.',
),
326 => array(
	'fkContext' => '10743',
	'fkSectionBestPractice' => '10738',
	'nControlType' => '1',
	'sName' => 'Die Methodenlehre zur Bewertung des Systems ist logisch und angebracht.',
	'tDescription' => 'Es gibt eine wirkungsvolle und objektive Bewertung der Eignung, Vollst�ndigkeit und der Wirksamkeit der Kontrollumgebung des Unternehmens.',
	'tImplementationGuide' => 'Dieses wird als Teil der 404 pr�fen folglich Durchlaufpr�fung hier ausgewertet.',
),
327 => array(
	'fkContext' => '10744',
	'fkSectionBestPractice' => '10738',
	'nControlType' => '1',
	'sName' => 'Die Auswertung wird ausreichend geplant und koordiniert.',
	'tDescription' => 'Es gibt eine wirkungsvolle und objektive Bewertung der Eignung ,Vollst�ndigkeit und der Wirksamkeit der Kontrollumgebung des Unternehmens.',
	'tImplementationGuide' => '',
),
328 => array(
	'fkContext' => '10745',
	'fkSectionBestPractice' => '10738',
	'nControlType' => '1',
	'sName' => 'Unterlagen sind vorhanden und angebracht.',
	'tDescription' => 'Es gibt eine wirkungsvolle und objektive Bewertung der Eignung,Vollst�ndigkeit und der Wirksamkeit der Kontrollumgebung des Unternehmens.',
	'tImplementationGuide' => 'Dieses wird als Teil der 404 pr�fen folglich Durchlaufpr�fung hier ausgewertet.',
),
329 => array(
	'fkContext' => '10747',
	'fkSectionBestPractice' => '10746',
	'nControlType' => '1',
	'sName' => 'Ein Mechanismus existiert der die erkannten internen  Kontrollschw�chen auff�ngt und berichtet.',
	'tDescription' => 'Erkannte M�ngel werden passend und zeitnah berichtet ,\"und entsprechend nachbehandelt.',
	'tImplementationGuide' => 'Beschafen Sie die Berichte �ber den derzeitigen Stand der Entdeckungen, die dem Audit-Ausschuss f�r zwei Quartale vorgelegt werden. Beschaffens Sie ebenfalls die Entdeckungsdatenbank zum  Beweis, da? der Status von Entdeckungen �berwacht wird.',
),
330 => array(
	'fkContext' => '10748',
	'fkSectionBestPractice' => '10746',
	'nControlType' => '1',
	'sName' => 'M�ngel werden der Person direkt  berichtet , die f�r die T�tigkeit verantwortlich ist und einer weiteren Person die mindestens eine Stufe h�her steht.',
	'tDescription' => 'Erkannte M�ngel   werden passend und zeitnah berichtet,\"und entsprechend nachbehandelt.',
	'tImplementationGuide' => 'Beschaffen Sie die Berichte �ber den derzeitigen Stand der Entdeckungen, die dem Audit-Ausschuss f�r zwei Quartale vorgelegt werden. Beschaffens Sie ebenfalls die Entdeckungsdatenbank zum  Beweis, da? der Status von Entdeckungen �berwacht wird. Zus�tzlich,\"erkennen Sie ob die, die f�r die Gew�hrleistung der Korrektur-Ma�nahmen verantwortlich sind, aufgef�hrt sind und ob es einen angeschlossenen Handlungsplan gibt.
',
),
331 => array(
	'fkContext' => '10749',
	'fkSectionBestPractice' => '10746',
	'nControlType' => '1',
	'sName' => 'Folgema�nahmen sind angebracht und stellen sicher, dass notwendige Ma�nahmen ergriffen werden. ',
	'tDescription' => 'Erkannte M�ngel werden passend und zeitnah berichtet,\"und entsprechend nachbehandelt.',
	'tImplementationGuide' => 'Beschaffen Sie die Berichte �ber den derzeitigen Stand der Entdeckungen, die dem Audit-Ausschuss f�r zwei Quartale vorgelegt werden. Beschaffens Sie ebenfalls die Entdeckungsdatenbank zum  Beweis, dass der Status von Entdeckungen �berwacht wird. Zus�tzlich erkennen sie, ob nachfolge Handlungen richtig sind.
',
),
332 => array(
	'fkContext' => '10750',
	'fkSectionBestPractice' => '10746',
	'nControlType' => '1',
	'sName' => 'Jeder Betrug,ob materiell oder nicht,\"in den das Management oder andere Angestellte verwickelt sind, die eine bedeutende Rolle in den internen Kontrollen oder Finanzen der Firma haben, wird dem Audit-Ausschuss berichtet.',
	'tDescription' => 'Erkannte M�ngel werden passend und zeitnah berichtet\",\"und entsprechend nachbehandelt.',
	'tImplementationGuide' => 'Beschaffen sie die Pr�sentationsunterlagen und die Protokole der Audit-Ausschuss-Sitzung.  Stellen Sie fest, ob Betrug in den das Management oder andere Angestellte verwickelt sind, die eine bedeutende Rolle in den internen Kontrollen oder Finanzen der Firma haben, dem Audit-Ausschuss berichtet wurden - ob oder ob nicht einer vorgefallen ist.
',
),
333 => array(
	'fkContext' => '10751',
	'fkSectionBestPractice' => '10746',
	'nControlType' => '1',
	'sName' => 'Jeder Betrug der signifikant ist f�r die Erfassung von Transaktionen oder die Methoden der Buchf�hrung,\"der f�r die internen Kontrollen bedeutend ist\",\"einen bedeutenden Effekt auf die Finanzberichte der Firma hat wird dem Audit-Ausschuss gemeldet.

',
	'tDescription' => 'Erkannte M�ngel werden passend und zeitnah berichtet\",und entsprechend nachbehandelt. Bedeutend f�r die Firma oder ihre T�tigkeiten,bedeutend f�r die Erfassung von Transaktionen oder die Methoden der Buchf�hrung,bedeutend f�r die internen Kontrollen , sind bedeutend f�r das mit Audit in Verbindung stehendem.',
	'tImplementationGuide' => ' Beschaffen sie die Pr�sentationsunterlagen und die Protokole der Audit-Ausschuss-Sitzung.  Stellen Sie fest, ob Betrug in den das Management oder andere Angestellte verwickelt sind, die eine bedeutende Rolle in den internen Kontrollen oder Finanzen der Firma haben, dem Audit-Ausschuss berichtet wurden - ob oder ob nicht einer vorgefallen ist. Stellen Sie fest, ob dar�ber berichtet wird - ob jegwelcher vorgefallen ist oder nicht.\\ 
',
),
334 => array(
	'fkContext' => '10752',
	'fkSectionBestPractice' => '10746',
	'nControlType' => '1',
	'sName' => 'Melden von vermutetem ethischen,Betrug,\"und/oder Befolgungsfrage auf anonymer Basis ist gegeben.
',
	'tDescription' => 'Erkannte M�ngel werden passend und zeitnah berichtet\",\"und entsprechend nachbehandelt.',
	'tImplementationGuide' => ' (1) Beschaffen Sie den Codex f�r Gesch�ftsverhalten und  Ethik und �berpr�fen ihn auf die Einrichtung von \\ \"\"\"\" tone at the top \\ \" - d.h.\",\"die Erwartung der Firma bez�glich ethischen und moralischen Verhaltens.  (2) Beachten Sie, ob Informantenplakate und die Alamosa Plakate, die ihre Kernaussagen adressieren, in den B�ror�umen und in den Verkaufsstellen angebracht worden sind (10 �rtlichkeiten).  (3) �berpr�fen Sie das Intranet und stellen Sie fest, ob Alamosas Kernaussagen angezeigt werden. (4) beschaffen und �berpr�fen S� die Kriterien f�r den Meister Verein und stellen Sie fest, ob sie mit Alamosas Kernaussagen in Einklang sind. 

',
),
)?>