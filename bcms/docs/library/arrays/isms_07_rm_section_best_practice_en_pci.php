<?php

$laSectionBestPracticeEN_PCI = 
array (
  0 => 
  array (
    'fkContext' => '6257',
    'fkParent' => 'null',
    'sName' => 'PCI',
  ),
  1 => 
  array (
    'fkContext' => '6550',
    'fkParent' => '6257',
    'sName' => 'Requirement 10: Track and monitor all access to network resources and cardholder data',
  ),
  2 => 
  array (
    'fkContext' => '6551',
    'fkParent' => '6257',
    'sName' => 'Requirement 11: Regularly test security systems and processes',
  ),
  3 => 
  array (
    'fkContext' => '6262',
    'fkParent' => '6257',
    'sName' => 'Requirement 01: Install and maintain a firewall configuration to protect cardholder data',
  ),
  4 => 
  array (
    'fkContext' => '6305',
    'fkParent' => '6257',
    'sName' => 'Requirement 02: Do not use vendor-supplied defaults for system passwords and other security parameters',
  ),
  5 => 
  array (
    'fkContext' => '6324',
    'fkParent' => '6257',
    'sName' => 'Requirement 03: Protect stored cardholder data',
  ),
  6 => 
  array (
    'fkContext' => '6365',
    'fkParent' => '6257',
    'sName' => 'Requirement 04: Encrypt transmission of cardholder data across open, public networks',
  ),
  7 => 
  array (
    'fkContext' => '6372',
    'fkParent' => '6257',
    'sName' => 'Requirement 05: Use and regularly update anti-virus software or programs',
  ),
  8 => 
  array (
    'fkContext' => '6379',
    'fkParent' => '6257',
    'sName' => 'Requirement 06: Develop and maintain secure systems and applications',
  ),
  9 => 
  array (
    'fkContext' => '6444',
    'fkParent' => '6257',
    'sName' => 'Requirement 07: Restrict access to cardholder data by business need to know',
  ),
  10 => 
  array (
    'fkContext' => '6463',
    'fkParent' => '6257',
    'sName' => 'Requirement 08: Assign a unique ID to each person with computer access',
  ),
  11 => 
  array (
    'fkContext' => '6507',
    'fkParent' => '6257',
    'sName' => 'Requirement 09: Restrict physical access to cardholder data',
  ),
  12 => 
  array (
    'fkContext' => '6616',
    'fkParent' => '6257',
    'sName' => 'Requirement 12: Maintain a policy that addresses information security for employees and contractors',
  ),
  13 => 
  array (
    'fkContext' => '6695',
    'fkParent' => '6257',
    'sName' => 'Requirement A.1: Shared hosting providers must protect the cardholder data environment',
  ),
)
?>