<?php

$laBestPracticeBR = 
array (
  0 => 
  array (
    'fkContext' => '1800',
    'fkSectionBestPractice' => '1602',
    'nControlType' => '0',
    'sName' => '5.1.1 Documento da Pol�tica de Seguran�a da Informa��o',
    'tDescription' => 'Conv�m que um documento da Pol�tica de Seguran�a da informa��o seja aprovado pela dire��o, publicado e comunicado para todos os funcion�rios e partes externas relevantes.  ',
    'tImplementationGuide' => 'Conv�m que o documento da Pol�tica de Seguran�a da informa��o declare o comprometimento da dire��o e estabele�a o enfoque da organiza��o para gerenciar a seguran�a da informa��o. Conv�m que o documento da pol�tica contenha declara��es relativas a:
a) uma defini��o de seguran�a da informa��o, suas metasglobais, escopo e import�ncia da seguran�a da informa��o como um mecanismo que habilita o compartilhamento da informa��o - ver introdu��o;
b) uma declara��o do comprometimento da dire��o, apoiando as metas e princ�pios da seguran�a da informa��o, alinhada com os objetivos e estrat�gias do neg�cio;
c) uma estrutura para estabelecer os objetivos de controle e os controles, incluindo a estrutura de an�lise/avalia��o e gerenciamento do risco;
d) breve explana��o das pol�ticas princ�pios, normas e requisitos de conformidade de seguran�a da informa��o espec�ficos para a organiza��o, incluindo:   1) conformidade com a legisla��o e com requisitos regulamentares e contratuais;
2) requisitos de conscientizar�o, treinamento e educa��o em seguran�a da informa��o;
3) gest�o da continuidade do neg�cio;
4) conseq��ncias das viola��es na Pol�tica de Seguran�a da informa��o;
e) defini��o das responsabilidades gerais e espec�ficas na gest�o da seguran�a da informa��o, incluindo o registro dos incidentes de seguran�a da informa��o;
f) refer�ncias � documenta��o que possam apoiar a pol�tica, por exemplo, pol�ticas e procedimentos de seguran�a mais detalhados de sistemas de informa��o espec�ficos ou regras de seguran�a que os usu�rios devem seguir.  Conv�m que esta Pol�tica de Seguran�a da informa��o seja comunicada atrav�s de toda a organiza��o para os usu�rios de forma que seja relevante, acess�vel e compreens�vel para o leitor em foco. Informa��es adicionais A Pol�tica de Seguran�a da informa��o pode ser uma parte de um documento da pol�tica geral. Se a Pol�tica de Seguran�a da informa��o for distribu�da fora da organiza��o, conv�m que sejam tomados cuidados para n�o revelar informa��es sens�veis. Informa��es adicionais podem ser encontradas na ISSO/IEC 13335-1:2004.',
  ),
  1 => 
  array (
    'fkContext' => '1801',
    'fkSectionBestPractice' => '1602',
    'nControlType' => '0',
    'sName' => '5.1.2 An�lise cr�tica da Pol�tica de Seguran�a da informa��o',
    'tDescription' => 'Conv�m que a Pol�tica de Seguran�a da informa��o seja analisada criticamente a intervalos planejados ou quando mudan�as significativas ocorrerem, para assegurar a sua cont�nua pertin�ncia, adequa��o e efic�cia.  ',
    'tImplementationGuide' => 'Conv�m que a Pol�tica de Seguran�a da informa��o tenha um gestor que tenha aprovado a responsabilidade pelo desenvolvimento, an�lise cr�tica e avalia��o da Pol�tica de Seguran�a da informa��o. Conv�m que a an�lise cr�tica inclua a avalia��o de oportunidades para melhoria da Pol�tica de Seguran�a da informa��o da organiza��o e tenha um enfoque para gerenciar a seguran�a da informa��o em respostas �s mudan�as ao ambiente organizacional, �s circunst�ncias do neg�cio, �s condi��es legais, ou ao ambiente t�cnico.  Conv�m que a an�lise cr�tica da Pol�tica de Seguran�a da informa��o leve em considera��o os resultados da an�lise cr�tica pela dire��o. Conv�m que sejam definidos procedimentos para an�lise cr�tica pela dire��o, incluindo uma programa��o ou um per�odo para a an�lise cr�tica.  Conv�m que as entrada para a an�lise cr�tica para a dire��o incluam informa��es sobre:
a) realimenta��o das partes interessadas;
b) resultados de an�lise cr�ticas independentes - ver 6.1.8;
c) situa��o de a��es preventivas e corretivas - ver 6.1.8 e 15.2.1;
d) resultados de an�lises cr�ticas anteriores feitas pela dire��o;
e) desempenho do processo e conformidade com a Pol�tica de Seguran�a da informa��o;
f) mudan�as que possam afetar o enfoque da organiza��o para gerenciar a seguran�a da informa��o, incluindo mudan�as no ambiente organizacional, nas circunst�ncias do neg�cio, na disponibilidade dos recursos, nas quest�es contratuais, regulamentares e de aspectos legais ou no ambiente t�cnico;
g) tend�ncias relacionadas com as amea�as e vulnerabilidades;
h) relato sobre incidentes de seguran�a da informa��o - ver 13.1;
i) recomenda��es fornecidas por autoridades relevantes - ver 6.1.6. Conv�m que as sa�das da an�lise cr�tica pela dire��o incluam quaisquer decis�es e a��es relacionadas a:  a) melhoria no enfoque da organiza��o para gerenciar a seguran�a da informa��o e seus processos;
b) melhoria dos controles e dos objetivos de controles;
c) melhoria na aloca��o de recursos e/ou de responsabilidades.  Conv�m que um registro da an�lise cr�tica pela dire��o seja mantido.  Conv�m que a aprova��o pela dire��o da Pol�tica de Seguran�a da informa��o revisada seja obtida.',
  ),
  2 => 
  array (
    'fkContext' => '1802',
    'fkSectionBestPractice' => '1604',
    'nControlType' => '0',
    'sName' => '6.1.1 Comprometimento da dire��o com a seguran�a da informa��o',
    'tDescription' => 'Conv�m que a dire��o ap�ie ativamente a seguran�a da informa��o dentro da organiza��o por meio de um claro direcionamento, demonstrando o seu comprometimento, definindo atribui��es de forma expl�cita e conhecendo as responsabilidades pela seguran�a da informa��o.  Informa��es adicionais: Outras informa��es podem ser obtidas na ISO/IEC 13335-1:2004. ',
    'tImplementationGuide' => 'Conv�m a dire��o:  a) assegure que as metas de seguran�a da informa��o est�o identificadas, atendem aos requisitos da organiza��o e est�o integradas nos processos relevantes;
b) formule, analise criticamente e aprove a Pol�tica de Seguran�a da informa��o;
c) analise criticamente a efic�cia da implementa��o da Pol�tica de Seguran�a da informa��o;
d) forne�a um claro direcionamento e apoio para as iniciativas de seguran�a da informa��o;
e) forne�a os recursos necess�rios para a seguran�a da informa��o;
f) aprove as atribui��es de tarefas e responsabilidades espec�ficas para a seguran�a da informa��o por toda a organiza��o;
g) inicie planos e programas para manter a conscientiza��o da seguran�a da informa��o;
h) assegure que a implementa��o dos controles de seguran�a da informa��o tem uma coordena��o e permeia a organiza��o - ver 6.1.2. Conv�m que a dire��o identifique as necessidades para a consultoria de um especialista interno ou externo em seguran�a da informa��o, analise criticamente e coordene os resultados desta consultoria por toda a organiza��o.  Dependendo do tamanho da organiza��o, tais responsabilidades podem ser conduzidas por um f�rum de gest�o exclusivo ou por um f�rum de gest�o existente, a exemplo do conselho de diretores. ',
  ),
  3 => 
  array (
    'fkContext' => '1803',
    'fkSectionBestPractice' => '1604',
    'nControlType' => '0',
    'sName' => '6.1.2 Coordena��o da seguran�a da informa��o',
    'tDescription' => 'Conv�m que as atividades de seguran�a da informa��o sejam coordenadas por representantes de diferentes partes da organiza��o, com fun��es e pap�is relevantes. ',
    'tImplementationGuide' => 'Conv�m que a coordena��o da seguran�a da informa��o envolva a coopera��o e colabora��o de gerentes, usu�rios, administradores, desenvolvedores, auditores, pessoal de seguran�a e especialistas com habilidades nas �reas de seguro, quest�es legais, recursos humanos, TI e gest�o de riscos.  Conv�m que esta atividade:  a) garanta que as atividades de seguran�a da informa��o s�o executadas em conformidade com as pol�ticas de seguran�a da informa��o;
b) identifique como conduzir as n�o-conformidades;
c) aprove as metodologias e processos para a seguran�a da informa��o, tais como an�lise/avalia��o de riscos e classifica��o da informa��o;
d) identifique as amea�as significativas e a exposi��o da informa��o e dos recursos de processamento da informa��o �s amea�as;
e) avalie a adequa��o e coordene a implementa��o de controles de seguran�a da informa��o;
f) promova, de forma eficaz, a educa��o, o treinamento e a conscientiza��o pela seguran�a da informa��o por toda organiza��o;
g) avalie as informa��es recebidas do monitoramento e da an�lise cr�tica dos incidentes de seguran�a da informa��o, e recomende a��es apropriadas como resposta para os incidentes de seguran�a da informa��o identificados.  Se a organiza��o n�o usa representantes das diferentes �reas, por exemplo, porque tal grupo n�o � apropriado para o tamanho da organiza��o, as a��es descritas acima devem ser conduzidas por um f�rum de gest�o adequado ou por um gestor individual. ',
  ),
  4 => 
  array (
    'fkContext' => '1804',
    'fkSectionBestPractice' => '1604',
    'nControlType' => '0',
    'sName' => '6.1.3 Atribui��o de responsabilidades para a seguran�a da informa��o',
    'tDescription' => 'Conv�m que todas as responsabilidades pela seguran�a da informa��o, estejam claramente definidas. ',
    'tImplementationGuide' => 'Conv�m que a atribui��o das responsabilidades pela seguran�a da informa��o seja feita em conformidade com as pol�ticas de seguran�a da informa��o - ver se��o 5. Conv�m que as responsabilidades pela prote��o de cada ativo e pelo cumprimento de processos de seguran�a da informa��o espec�ficos sejam claramente definidas. Conv�m que esta responsabilidade seja complementada, onde for necess�rio, com orienta��es mais detalhadas para locais espec�ficos e recursos de processamento de informa��es. Conv�m que sejam claramente definidas as responsabilidades em cada local para a prote��o dos ativos e para realizar processos de seguran�a da informa��o espec�ficos, como, por exemplo, o plano de continuidade de neg�cios.  Pessoas com responsabilidades definidas pela seguran�a da informa��o podem delegar as tarefas de seguran�a da informa��o para outros usu�rios. Todavia eles continuam respons�veis e conv�m que verifiquem se as tarefas delegadas est�o sendo executadas corretamente. Conv�m que as �reas pelas quais as pessoas sejam respons�veis, estejam claramente definidas, em particular conv�m que os seguintes itens estejam definidos:
a) os ativos e os processos de seguran�a da informa��o associados com cada sistema sejam identificados e claramente definidos;
b) gestor respons�vel por cada ativo ou processo de seguran�a da informa��o tenha atribui��es definidas e os detalhes dessa responsabilidade sejam documentados - ver 7.1.2;
c) os n�veis de autoriza��o sejam claramente definidos e documentados;
Informa��es adicionais Em muitas organiza��es um gestor de seguran�a da informa��o pode ser indicado para assumir a responsabilidade global pelo desenvolvimento e implementa��o da seguran�a da informa��o e para apoiar a identifica��o de controles;
Entretanto a responsabilidade pela obten��o dos recursos e implementa��o dos controles permanece sempre com os gestores. Uma pr�tica comum � indicar um respons�vel por cada ativo, tornando-o assim respons�vel por sua prote��o no dia a dia.',
  ),
  5 => 
  array (
    'fkContext' => '1805',
    'fkSectionBestPractice' => '1604',
    'nControlType' => '0',
    'sName' => '6.1.4 Processo de autoriza��o para os recursos de processamento da informa��o',
    'tDescription' => 'Conv�m que seja definido e implementado um processo de gest�o de autoriza��o para novos recursos de processamento da informa��o. ',
    'tImplementationGuide' => 'Conv�m que as seguintes diretrizes sejam consideradas no processo de autoriza��o:
a) os novos recursos tenham a autoriza��o adequada por parte da administra��o de usu�rios, autorizando seus prop�sitos e uso. Conv�m que a autoriza��o tamb�m seja obtida junto ao gestor respons�vel pela manuten��o do sistema de seguran�a da informa��o, para garantir que todas as pol�ticas e requisitos de seguran�a relevantes sejam atendidos;
b) hardware e o software sejam verificados para garantir que s�o compat�veis com outros componentes do sistema, onde necess�rios;
c) uso de recursos de processamento de informa��o, pessoais ou privados, como, por exemplo, notebooks, computadores pessoais ou dispositivos do tipo palm top, para processamento das informa��es do neg�cio, possa introduzir novas vulnerabilidades, e conv�m que controles necess�rios sejam identificados e implementados. ',
  ),
  6 => 
  array (
    'fkContext' => '1806',
    'fkSectionBestPractice' => '1604',
    'nControlType' => '0',
    'sName' => '6.1.5 Acordos de confidencialidade',
    'tDescription' => 'Conv�m que os requisitos para confidencialidade ou acordos de n�o divulga��o que reflitam as necessidades da organiza��o para a prote��o da informa��o sejam identificados e analisados criticamente, de forma regular. ',
    'tImplementationGuide' => 'Conv�m que os acordos de confidencialidade e de n�o divulga��o considerem os requisitos para proteger as informa��es confidenciais, usando termos que s�o obrigados do ponto de vista legal. Para identificar os requisitos para os acordos de confidencialidade ou n�o divulga��o, conv�m que sejam considerados os seguintes elementos:
a) uma defini��o da informa��o a ser protegida - por exemplo, informa��o confidencial;
b) tempo de dura��o esperado de um acordo, incluindo situa��es onde a confidencialidade tenha que ser mantida indefinidamente;
c) a��es requeridas quando um acordo est� encerrado;
d) responsabilidades e a��es dos signat�rios para evitar a divulga��o n�o autorizada da informa��o (como o conceito \'\'need to know\'\') e) propriet�rio da informa��o, segredos comerciais e de propriedade intelectual, e como isto se relaciona com a prote��o da informa��o confidencial;
f) uso permitido da informa��o confidencial e dos direitos do signat�rio para usar a informa��o;
g) direito de auditar e monitorar as atividades que envolvem as informa��es confidenciais;
h) processo para notifica��o e relato de divulga��o n�o autorizada ou viola��o das informa��es confidenciais;
i) termos para a informa��o ser retornada ou destru�da quando da suspens�o do acordo;
j) a��es esperadas a serem tomadas no caso da viola��o deste acordo.  Com base nos requisitos de seguran�a da informa��o da organiza��o, outros elementos podem ser necess�rios em um acordo de confidencialidade ou de n�o divulga��o.  Conv�m que os acordos de confidencialidade e n�o divulga��o estejam em conformidade com todas as leis e regulamenta��es aplic�veis na jurisdi��o na qual eles se aplicam - ver 15.1.1 - Conv�m que os requisitos para o acordo de confidencialidade e de n�o divulga��o sejam analisados criticamente de forma peri�dica e quando mudan�as ocorrerem que influenciem estes requisitos. Informa��es adicionais Acordos de confidencialidade e de n�o divulga��o protegem as informa��es da organiza��o e informam aos signat�rios das suas responsabilidades, para proteger, usar e divulgar a informa��o de maneira respons�vel e autorizada.  Pode haver a necessidade de uma organiza��o usar diferentes formas de acordos de confidencialidade ou de n�o divulga��o, em diferentes circunst�ncias. ',
  ),
  7 => 
  array (
    'fkContext' => '1807',
    'fkSectionBestPractice' => '1604',
    'nControlType' => '0',
    'sName' => '6.1.6 Contato com autoridades',
    'tDescription' => 'Conv�m que contatos apropriados com autoridades relevantes sejam mantidos. ',
    'tImplementationGuide' => 'Conv�m que as organiza��es tenham procedimentos em funcionamento que especifiquem quando e por quais autoridades (por exemplo, obriga��es legais, corpo de bombeiros, autoridades fiscalizadoras) devem ser contatadas e como os incidentes de seguran�a da informa��o identificados devem ser notificados em tempo h�bil, no caso de suspeita de que a lei foi violada.  Organiza��es que estejam sob ataque de internet podem precisar do apoio de partes externas � organiza��o (por exemplo, um provedor de servi�o da internet ou um operador de telecomunica��es), para tomar a��es contra a origem do ataque. Informa��es adicionais A manuten��o de tais contatos pode ser um requisito para apoiar a gest�o de incidentes de seguran�a da informa��o - ver 13.2 - ou da continuidade dos neg�cios e do processo de planejamento da conting�ncia - ver se��o 14. Contatos com organismos reguladores s�o tamb�m �teis para antecipar e preparar para as mudan�as futuras na lei ou nos regulamentos, os quais t�m que ser seguidos pela organiza��o. Contatos com outras autoridades incluem utilidades, servi�os de emerg�ncia, sa�de e seguran�a, por exemplo corpo de bombeiros (em conjunto com a continuidade do neg�cio), provedores de telecomunica��o (em conjunto com as rotas de linha e disponibilidade), fornecedor de �gua (em conjunto com as instala��es de refrigera��o para os equipamentos). ',
  ),
  8 => 
  array (
    'fkContext' => '1808',
    'fkSectionBestPractice' => '1604',
    'nControlType' => '0',
    'sName' => '6.1.7 Contato com grupos especiais',
    'tDescription' => 'Conv�m que sejam mantidos contatos apropriados com grupos de interesse especiais ou outros f�runs especializados de seguran�a da informa��o e associa��es profissionais. Informa��es adicionais: Acordos de compartilhamento de informa��es podem ser estabelecidos para melhorar a coopera��o e coordena��o de assuntos de seguran�a da informa��o. Conv�m que tais acordos identifiquem requisitos para a prote��o de informa��es sens�veis. ',
    'tImplementationGuide' => 'Conv�m que os membros de grupos de interesses especiais ou f�runs sejam considerados como forma de:
a) ampliar o conhecimento sobre as melhores pr�ticas e manter-se atualizado com as informa��es relevantes sobre seguran�a da informa��o;
b) ter o entendimento de que o ambiente de seguran�a da informa��o est� correto e completo;
c) receber previamente advert�ncias de alertas, aconselhamentos e corre��es relativos a ataques e vulnerabilidades;
d) conseguir acesso � consultoria especializada em seguran�a da informa��o;
e) compartilhar e trocar informa��es sobre novas tecnologias, produtos, amea�as ou vulnerabilidades;
f) prover relacionamentos adequados quando tratar com incidentes de seguran�a da informa��o - ver 13.2.1',
  ),
  9 => 
  array (
    'fkContext' => '1809',
    'fkSectionBestPractice' => '1604',
    'nControlType' => '0',
    'sName' => '6.1.8 An�lise cr�tica independente de seguran�a da informa��o',
    'tDescription' => 'Conv�m que o enfoque da organiza��o para gerenciar a seguran�a da informa��o e sua implementa��o (por exemplo, controles, objetivo dos controles, pol�ticas, processos e procedimentos para a seguran�a da informa��o) seja analisado criticamente, de forma independente, a intervalos planejados, ou quando ocorrerem mudan�as significativas relativas a implementa��o da seguran�a da informa��o. ',
    'tImplementationGuide' => 'Conv�m que a an�lise cr�tica independente seja iniciada pela dire��o. Tal an�lise cr�tica independente � necess�ria para assegurar a cont�nua pertin�ncia, adequa��o e efic�cia do enfoque da organiza��o para gerenciar a seguran�a da informa��o. Conv�m que a an�lise cr�tica inclua a avalia��o de oportunidades para a melhoria e a necessidade de mudan�as para o enfoque da seguran�a da informa��o, incluindo a pol�tica e os objetivos de controle.  Conv�m que a an�lise cr�tica seja executada por pessoas independentes da �rea avaliada, como, por exemplo, uma fun��o de auditoria interna, um gerente independente ou uma organiza��o de terceira parte especializada em tais an�lises cr�ticas. Conv�m que as pessoas que realizem estas an�lises cr�ticas possuam habilidade e experi�ncia apropriadas.  Conv�m que os resultados da an�lise cr�tica independente sejam registrados e relatados para a dire��o que iniciou an�lise cr�tica. Esses registros devem ser mantidos.  Se a an�lise cr�tica independente identificar que o enfoque da organiza��o e a implementa��o para gerenciar a seguran�a da informa��o s�o inadequados ou n�o-conformes com as orienta��es estabelecidas pela seguran�a da informa��o, no documento da Pol�tica de Seguran�a da informa��o - ver 5.1.1 -, conv�m que a dire��o considere a tomada de a��es corretivas. Informa��es adicionais Conv�m que as �reas onde os gerentes regularmente fazem a an�lise cr�tica - ver 15.2.1 - possam tamb�m ser analisadas criticamente de forma independente. T�cnicas para a an�lise cr�tica podem incluir entrevistas com a ger�ncia, verifica��o de registros ou an�lise critica dos documentos da Pol�tica de Seguran�a da informa��o. A ABNT NBR ISO 19011:2002, Diretrizes para auditoria de sistemas de gest�o da qualidade e/ou do meio ambiente, pode tamb�m fornecer orienta��es para se realizar a an�lise cr�tica independente, incluindo o estabelecimento e a implementa��o de um programa de an�lise cr�tica. A subse��o 15.3 especifica os controles relevantes para a an�lise cr�tica independente de sistemas de informa��es operacionais e o uso de ferramentas de auditoria de sistemas. ',
  ),
  10 => 
  array (
    'fkContext' => '1810',
    'fkSectionBestPractice' => '1605',
    'nControlType' => '0',
    'sName' => '6.2.1 Identifica��o dos riscos relacionados com partes externas',
    'tDescription' => 'Conv�m que os riscos para os recursos de processamento de informa��o e da informa��o da organiza��o oriundos de processos do neg�cio que envolva as partes externas sejam identificados e controles apropriados implementados antes de se conceder o acesso. ',
    'tImplementationGuide' => 'Conv�m que uma an�lise/avalia��o de riscos - ver se��o 4 - seja feita para identificar quaisquer requisitos de controles espec�ficos, onde existir uma necessidade que permita o acesso de uma parte externa aos recursos de processamento da informa��o ou � informa��o de uma organiza��o. Conv�m que a identifica��o de riscos relativos ao acesso da parte externa leve em considera��o os seguintes aspectos:
a) os recursos de processamento da informa��o que uma parte externa seja autorizada a acessar;
b) tipo de acesso que a parte externa ter� aos recursos de processamento da informa��o e a informa��o como, por exemplo:
1) acesso f�sico ao escrit�rio, sala de computadores, gabinetes e cabeamento;
2) acesso l�gico ao banco de dados da organiza��o e aos sistemas de informa��es;
3) rede de conex�o entre a organiza��o e a rede da parte externa, como, por exemplo, conex�o permanente, acesso remoto;
4) se o acesso vai ser dentro ou fora da organiza��o;
c) valor e a sensibilidade da informa��o envolvida, e a sua criticidade para as opera��es do neg�cio;
d) os controles necess�rios para proteger a informa��o que n�o deva ser acessada pelas partes externas;
e) as pessoas das partes externas envolvidas no manuseio das informa��es da organiza��o;
f) como a organiza��o ou o pessoal autorizado a ter acesso pode ser identificado, como a autoriza��o � verificada e com qual freq��ncia isto precisa ser reconfirmado;
g) as diferentes formas e controles empregados pela parte externa quando estiver armazenando, processando, comunicando, compartilhando e repassando informa��es;
h) as diferentes formas e controles empregados pela parte externa, quando requerido, e a entrada ou o recebimento incorreto ou por engano da informa��o;
i) pr�ticas e procedimentos para tratar com incidentes de seguran�a da informa��o e danos potenciais, e os termos e condi��es para que a parte externa continue acessando, no caso que ocorra um incidente de seguran�a da informa��o;
j) que os requisitos legais e regulamentares e outras obriga��es contratuais relevantes para a parte externa sejam levados em considera��o;
k) como os interesses de quaisquer uma das partes interessadas podem ser afetados pelos acordos.  Conv�m que o acesso �s informa��es da organiza��o pelas partes externas n�o seja fornecido at� que os controles apropriados tenham sido implementados e, onde for vi�vel, um contrato tenha sido assinado definido os termos e condi��es para a conex�o ou o acesso e os preparativos para o trabalho. Conv�m que, de uma forma geral, todos os requisitos de seguran�a da informa��o resultantes do trabalho com partes externas ou controles internos estejam refletidos por um acordo com a parte externa - ver 6.2.2 e 6.2.3.  Conv�m que seja assegurado que a parte externa est� consciente de suas obriga��es, e aceita as responsabilidades e obriga��es envolvendo o acesso, processamento, comunica��o ou o gerenciamento dos recursos da informa��o e da informa��o da organiza��o. Informa��es adicionais A informa��o pode ser colocada em risco por partes externas com uma gest�o inadequada da seguran�a da informa��o. Conv�m que os controles sejam identificados e aplicados para administrar o acesso da parte externa aos recursos de processamento da informa��o. Por exemplo, se existir uma necessidade especial para a confidencialidade da informa��o, acordos de n�o divulga��o devem ser usados.  As organiza��es podem estar sujeitas a riscos associados com processos interorganizacionais, gerenciamento e comunica��o, se um alto grau de terceiriza��o for realizado, ou onde existirem v�rias partes externas envolvidas.  Os controles de 6.2.2 e 6.2.3 cobrem diferentes situa��es para as partes externas, incluindo, por exemplo:
a) provedores de servi�o, tais como ISP, provedores de rede, servi�os de telefonia e servi�os de apoio e manuten��o;
b) gerenciamento dos servi�os de seguran�a;
c) clientes;
d) opera��es e/ou recursos de terceiriza��o, como, por exemplo, sistemas de TI, servi�os de coleta de dados, opera��o de call center
e) consultores em neg�cios e em gest�o, e auditores;
f) desenvolvedores e fornecedores, como, por exemplo, de produtos de software e sistemas de TI;
g) pessoal de limpeza, servi�os de buf�s e outros servi�os de apoio terceirizados;
h) pessoal tempor�rio, estagi�rio e outras contrata��es de curta dura��o.  Tais acordos podem ajudar a reduzir o risco associado com as partes externas.',
  ),
  11 => 
  array (
    'fkContext' => '1811',
    'fkSectionBestPractice' => '1605',
    'nControlType' => '0',
    'sName' => '6.2.2 Identificando a seguran�a da informa��o, quando tratando com os clientes',
    'tDescription' => 'Conv�m que todos os requisitos de seguran�a da informa��o identificados sejam considerados antes de conceder aos clientes o acesso aos ativos ou �s informa��es da organiza��o. ',
    'tImplementationGuide' => 'Conv�m que os seguintes termos sejam considerados para contemplar a seguran�a da informa��o antes de conceder aos clientes o acesso a quaisquer ativos da organiza��o (dependendo do tipo e extens�o do acesso concedido, nem todos os itens s�o aplic�veis):  a) prote��o dos ativos incluindo:  1) procedimentos para proteger os ativos da organiza��o, incluindo informa��o e software, e a gest�o de vulnerabilidades conhecidas;
2) procedimentos para definir a��es quando ocorrer o comprometimento de quaisquer dos ativos, por exemplo, perda ou modifica��o de dados;
3) integridade;
4) restri��es em rela��o a c�pias e divulga��o de informa��es;
b) descri��o do produto ou servi�o a ser fornecido;
c) as diferentes raz�es, requisitos e benef�cios para o acesso do cliente;
d) pol�ticas de controle de acesso cobrindo:  1) m�todos de acesso permitindo e o controle e uso de identificadores �nicos, tais como identificador de usu�rio e senhas de acesso;
2) um processo de autoriza��o para acesso dos usu�rios e privil�gios 3) uma declara��o de que todo o acesso que n�o seja explicitamente autorizado � proibido;
4) um processo para revogar os direitos de acesso ou interromper a conex�o entre sistemas;
e) procedimentos para relato, notifica��o e investiga��o de informa��es imprecisas (por exemplo, sobre pessoal), incidentes de seguran�a da informa��o e viola��o da seguran�a da informa��o;
f) descri��o de cada servi�o que deve estar dispon�vel;
g) os n�veis de servi�os acordados e os n�veis de servi�os inaceit�veis;
h) direito de monitorar e revogar qualquer atividade relacionada com os ativos da organiza��o;
i) as respectivas responsabilidades civis da organiza��o e dos clientes;
j) responsabilidades com rela��o a aspectos legais e como � assegurado que os requisitos legais s�o atendidos, por exemplo, leis de prote��o de dados, especialmente levando-se em considera��o os diferentes sistemas legais nacionais se o acordo envolver a coopera��o com clientes em outros pa�ses - ver 15.1 k) direitos de propriedade intelectual e direitos autorais - ver 15.1.2 - e prote��o de qualquer trabalho colaborativo - ver 6.1.5. Informa��es adicionais Os requisitos de seguran�a da informa��o relacionados com o acesso dos clientes aos ativos da organiza��o podem variar consideravelmente, dependendo dos recursos de processamento da informa��o e das informa��es que est�o sendo acessadas. Estes requisitos de seguran�a da informa��o podem ser contemplados, usando-se os acordos com o cliente, os quais cont�m todos os riscos identificados e os requisitos de seguran�a da informa��o - ver 6.2.1.  Acordos com partes externas podem tamb�m envolver outras partes. Conv�m que os acordos que concedam o acesso a partes externas incluam permiss�o para designa��o de outras partes autorizadas e condi��es para os seus acessos e envolvimento.',
  ),
  12 => 
  array (
    'fkContext' => '1812',
    'fkSectionBestPractice' => '1605',
    'nControlType' => '0',
    'sName' => '6.2.3 Identificando seguran�a da informa��o nos acordos com terceiros',
    'tDescription' => 'Conv�m que os acordos cm terceiros envolvendo o acesso, processamento, comunica��o ou gerenciamento dos recursos de processamento da informa��o ou da informa��o da organiza��o, ou o acr�scimo de produtos ou servi�os aos recursos de processamento da informa��o cubram todos os requisitos de seguran�a da informa��o relevantes. ',
    'tImplementationGuide' => 'Conv�m que o acordo assegure que n�o existe mal entendo entre a organiza��o e o terceiro. Conv�m que as organiza��es considerem a possibilidade de indeniza��o do terceiro.  Conv�m que os seguintes termos sejam considerados para inclus�o no acordo, com o objetivo de atender aos requisitos de seguran�a da informa��o identificados (ver 6.2.1):  a) Pol�tica de Seguran�a da informa��o;
b) controles para assegurar a prote��o do ativo, incluindo:  1) procedimentos para proteger os ativos da organiza��o, incluindo informa��o, software e hardware;
2) quaisquer mecanismos e controles para a prote��o f�sica requerida;
3) controles para assegurar prote��o contra software malicioso - ver 10.4.1;
4) procedimentos para definir a��es quando ocorrer o comprometimento de quaisquer dos ativos, por exemplo, perda ou modifica��o de informa��es, software e hardware;
5) controles para assegurar o retorno ou a destrui��o da informa��o e dos ativos no final do contrato, ou em um dado momento definido no acordo. 6) Confidencialidade, integridade, disponibilidade e qualquer outra propriedade relevante dos ativos;
7) Restri��es em rela��o a c�pias e divulga��o de informa��es, e uso dos acordos de confidencialidade - ver 6.1.5. c) treinamento dos usu�rios e administrados nos m�todos, procedimentos e seguran�a da informa��o;
d) Assegurar a conscientiza��o dos usu�rios nas quest�es e responsabilidades pela seguran�a da informa��o;
e) Provis�o para a transfer�ncia de pessoal, onde necess�rio;
f) Responsabilidades com rela��o � manuten��o e instala��o de software e hardware;
g) Uma estrutura clara de notifica��o e formatos de relat�rios acordados;
h) Um processo claro e definido de gest�o de mudan�as;
i) Pol�tica de controle de acesso, cobrindo:  1) as diferentes raz�es, requisitos e benef�cios que justificam a necessidade do acesso pelo terceiro;
2) m�todos de acesso permitido e o controle e uso de identificadores �nicos, tais como identificadores de usu�rios e senhas de acesso;
3) um processo de autoriza��o de acesso e privil�gios para os usu�rios;
4) um requisito para manter uma lista de pessoas autorizadas a usar recursos que est�o sendo disponibilizados, e quais os seus direitos e privil�gios com rela��o a tal uso;
5) uma declara��o de que todo o acesso que n�o seja explicitamente autorizado � proibido;
6) um processo para revogar os direitos de acesso ou interromper a conex�o entre sistemas;
j) dispositivos para relato, notifica��o e investiga��o de incidentes de seguran�a da informa��o e viola��o da seguran�a, bem como as viola��es dos requisitos definidos no acordo;
k) uma descri��o do produto ou servi�o que est� sendo fornecido e uma descri��o da informa��o que deve estar dispon�vel, juntamente com a sua classifica��o de seguran�a - ver 7.2.1;
l) n�veis de servi�os acordados e n�veis de servi�os inaceit�veis;
m) defini��o de crit�rios de desempenho verific�veis, seu monitoramento e relato;
n) direito de monitorar e revogar qualquer atividade relacionada com os ativos da organiza��o;
o) direito de auditar as responsabilidades definidas do acordo, para ter essas auditorias realizadas por terceira parte para enumerar os direitos regulamentares dos auditores;
p) estabelecimento de um processo escalonado para resolu��o de problemas;
q) requisitos para continuidade dos servi�os, incluindo medi��es para disponibilidade e confiabilidade, de acordo com as prioridades do neg�cio da organiza��o;
r) respectivas obriga��es das partes com o acordo;
s) responsabilidades com rela��o a aspectos legais e como � assegurado que os requisitos legais s�o atendidos, por exemplo, leis de prote��o de dados, levando-se em considera��o especialmente os diferentes sistemas legais nacionais, se o acordo envolver a coopera��o com organiza��es em outros pa�ses - ver 15.1;
t) direitos de propriedade intelectual e direitos autorais - ver 15.1.2 - e prote��o de qualquer trabalho colaborativo - ver 6.1.5;
u) envolvimento do terceiro com subfornecedores e os controles de seguran�a da informa��o que esses subfornecedores precisam implementar;
v) condi��es de renegocia��o ou encerramento de acordos;
1) um plano de conting�ncia deve ser elaborado no caso de uma das partes desejar encerrar a rela��o antes do final do acordo;
2) renegocia��o dos acordos se os requisitos de seguran�a da organiza��o mudarem;
3) listas atualizadas da documenta��o dos ativos, acordos ou direitos relacionados aos ativos. Informa��es adicionais Os acordos podem variar consideravelmente para diferentes organiza��es e para os diferentes tipos de terceiros. Conv�m, entretanto, que sejam tomados cuidados para incluir nos acordos todos os riscos identificados e os requisitos de seguran�a da informa��o - ver 6.2.1. Onde necess�rio, os procedimentos e controles requeridos podem ser inclu�dos em um plano de gest�o de seguran�a da informa��o.  Se a gest�o da seguran�a da informa��o for terceirizada, conv�m que os acordos definam como os terceiros ir�o garantir que a seguran�a da informa��o, conforme definida na an�lise/avalia��o de riscos, ser� mantida e como seguran�a da informa��o ser� adaptada para identificar e tratar com as mudan�as aos riscos.  Algumas das diferen�as entre as terceiriza��es e as outras formas de provis�o de servi�os de terceiros incluem as quest�es das obriga��es legais, o planejamento do per�odo de transi��o e de descontinuidade da opera��o neste per�odo, planejamento de conting�ncia e an�lise cr�tica de investiga��es, e coleta e gest�o de incidentes de seguran�a da informa��o. Entretanto � importante que a organiza��o planeje e gerencie a transi��o para um terceirizado e tenha processos adequados para gerenciar as mudan�as e renegociar ou encerrar os acordos.   Os procedimentos para continuar processando no caso em que o terceiro se torne incapaz de prestar o servi�o precisam ser considerados no acordo para evitar qualquer atraso nos servi�os de substitui��o.  Acordos com terceiros podem tamb�m envolver outras partes. Conv�m que os acordos que concedam o acesso a terceiros incluam permiss�o para designa��o de outras partes autorizadas e condi��es para seus acessos e envolvimento.  De um modo geral os acordos s�o geralmente elaborados pela organiza��o. Podem existir situa��es onde, em algumas circunst�ncias, um acordo possa ser elaborado e imposto pela organiza��o para o terceiro. A organiza��o precisa assegurar que a sua pr�pria seguran�a da informa��o n�o � afetada desnecessariamente pelos requisitos do terceiro, estipulados no acordo imposto. ',
  ),
  13 => 
  array (
    'fkContext' => '1813',
    'fkSectionBestPractice' => '1607',
    'nControlType' => '0',
    'sName' => '7.1.1 Invent�rio dos ativos',
    'tDescription' => 'Conv�m que todos os ativos sejam claramente identificados e um invent�rio de todos os ativos importantes seja estruturado e mantido. ',
    'tImplementationGuide' => 'Conv�m que a organiza��o identifique todos os ativos e documente a import�ncia destes ativos. Conv�m que o invent�rio do ativo inclua todas as informa��es necess�rias que permitam recuperar de um desastre, incluindo o tipo do ativo, formato, localiza��o, informa��es sobre c�pias de seguran�a, informa��es sobre licen�as e a import�ncia do ativo para o neg�cio. Conv�m que o invent�rio n�o duplique outros invent�rios desnecessariamente, por�m ele deve assegurar que o seu conte�do est� coerente.  Adicionalmente, conv�m que o propriet�rio - ver 7.1.2 - e a classifica��o da informa��o - ver 7.2 - sejam acordados e documentados para cada um dos ativos. Conv�m que, com base na import�ncia do ativo, seu valor para o neg�cio e sua classifica��o de seguran�a, n�veis de prote��o proporcionais a import�ncia dos ativos sejam identificados (mais informa��es sobre como valorar os ativos para indicar a sua import�ncia podem ser encontradas na ISO IEC TR 13335-3). Informa��es adicionais Existem v�rios tipos de ativos, incluindo: a) ativos de informa��o: base de dados e arquivos, contratos e acordos, documenta��o de sistema, informa��es sobre pesquisa, manuais de usu�rio, material de treinamento, procedimento de suporte ou opera��o, planos de continuidade do neg�cio, procedimentos de recupera��o, trilhas de auditoria e informa��es armazenadas;
b) ativos de software: aplicativos, sistemas, ferramentas de desenvolvimento e utilit�rios;
c) ativos f�sicos: equipamentos computacionais, equipamentos de comunica��o, m�dias remov�veis e outros equipamentos;
d) servi�os: servi�os de computa��o e comunica��es, utilidades gerais, por exemplo aquecimento, ilumina��o, eletricidade e refrigera��o;
e) pessoas e suas qualifica��es, habilidades e experi�ncias;
f) intang�veis, tais como a reputa��o e a imagem da organiza��o.  Os invent�rios de ativos ajudam a assegurar que a prote��o efetiva do ativo pode ser feita e tamb�m pode ser requerido para outras finalidades do neg�cio, como sa�de e seguran�a, seguro ou financeira (gest�o de ativos). O processo de compila��o de um invent�rio de ativos � um pr�-requisito importante no gerenciamento de riscos - ver se��o 4. ',
  ),
  14 => 
  array (
    'fkContext' => '1814',
    'fkSectionBestPractice' => '1607',
    'nControlType' => '0',
    'sName' => '7.1.2 Propriet�rio dos ativos',
    'tDescription' => 'Conv�m que todas as informa��es e ativos associados com os recursos de processamento da informa��o tenham um propriet�rio designado por uma parte definida da organiza��o. ',
    'tImplementationGuide' => 'Conv�m que o propriet�rio do ativo seja respons�vel por:  a) assegurar que as informa��es e os ativos associados com os recursos de processamento de informa��o estejam adequadamente classificados;
b) definir e periodicamente analisar criticamente as classifica��es e restri��es ao acesso, levando em conta as pol�ticas de controle de acesso, aplic�veis.  O propriet�rio pode ser designado para:  a) um processo do neg�cio;
b) um conjunto de atividades definidas;
c) uma aplica��o;
ou d) um conjunto de dados definido. Informa��es adicionais As tarefas de rotina podem ser delegadas, por exemplo, para um custodiante que cuida do ativo no dia a dia por�m a responsabilidade permanece com o propriet�rio.  Em sistemas de informa��o complexos pode ser �til definir grupos de ativos que atuem juntos para fornecer uma fun��o particular, como servi�os. Neste caso, o propriet�rio do servi�o � o respons�vel pela entrega do servi�o, incluindo o funcionamento dos ativos, que prov� os servi�os. ',
  ),
  15 => 
  array (
    'fkContext' => '1815',
    'fkSectionBestPractice' => '1607',
    'nControlType' => '0',
    'sName' => '7.1.3 Uso aceit�vel dos ativos',
    'tDescription' => 'Conv�m que sejam identificadas, documentadas e implementadas regras para que sejam permitidos o uso de informa��es e de ativos associados aos recursos de processamento da informa��o. ',
    'tImplementationGuide' => 'Conv�m que todos os funcion�rios, fornecedores e terceiros sigam as regras para o uso permitido de informa��es e de ativos associados aos recursos de processamento da informa��o, incluindo:  a) regras para o uso da internet e do correio eletr�nico - ver 10.8;
b) diretrizes para o uso de dispositivos m�veis, especialmente para o uso fora das instala��es da organiza��o - ver 11.7.1.  Conv�m que regras especificadas ou diretrizes sejam fornecidas pelo gestor relevante. Conv�m que funcion�rios, fornecedores e terceiros que usem ou tenham acesso aos ativos da organiza��o estejam conscientes dos limites que existem para os usos das informa��es e ativos associados da organiza��o aos recursos de processamento da informa��o. Conv�m que eles sejam respons�veis pelo uso de quaisquer recursos de processamento da informa��o e de quaisquer outros usos conduzidos sob as suas responsabilidades. ',
  ),
  16 => 
  array (
    'fkContext' => '1816',
    'fkSectionBestPractice' => '1608',
    'nControlType' => '0',
    'sName' => '7.2.1 Recomenda��es para classifica��o',
    'tDescription' => 'Conv�m que a informa��o seja classificada em termos do seu valor, requisitos legais, sensibilidade e criticidade para a organiza��o. ',
    'tImplementationGuide' => 'Conv�m que a classifica��o da informa��o e seus respectivos controles de prote��o levem em considera��o as necessidades de compartilhamento ou restri��o de informa��es e os respectivos impactos nos neg�cios, associados com tais necessidades.  Conv�m que as diretrizes para classifica��o incluam conven��es para classifica��o inicial e reclassifica��o ao longo do tempo, de acordo com algumas pol�ticas de controle de acesso predeterminadas - ver 11.1.1 . Conv�m que seja de responsabilidade do propriet�rio do ativo - ver 7.1.2 - definir a classifica��o de um ativo, analisando-o criticamente a intervalos regulares, e assegurar que ele est� atualizado e no n�vel apropriado. Conv�m que a classifica��o leve em considera��o a agrega��o do efeito mencionado em 10.7.2.  Conv�m que cuidados sejam tomados com a quantidade de categorias de classifica��o e com os benef�cios obtidos pelo seu uso. Esquemas excessivamente complexos podem tornar o uso inc�modo e ser invi�veis economicamente ou impratic�veis. Conv�m que aten��o especial seja dada na interpreta��o dos r�tulos de classifica��o sobre documentos de outras organiza��es, que podem ter defini��es diferentes para r�tulos iguais ou semelhantes aos usados.  Informa��es adicionais O n�vel de prote��o pode ser avaliado analisando a confidencialidade, a integridade e a disponibilidade da informa��o, bem como quaisquer outros requisitos que sejam considerados.  A informa��o freq�entemente deixa de ser sens�vel ou cr�tica ap�s um certo per�odo de tempo, por exemplo quando a informa��o se torna p�blica. Conv�m que estes aspectos sejam levados em considera��o, pois uma classifica��o superestimada pode levar � implementa��o de custos desnecess�rios, resultando em despesas adicionais.  Considerar, conjuntamente, documentos com requisitos de seguran�a similares, quando da atribui��o dos n�veis de classifica��o, pode ajudar a simplificar a tarefa de classifica��o.  Em geral, a classifica��o dada � informa��o � uma maneira de determinar como esta informa��o vai ser tratada e protegida. ',
  ),
  17 => 
  array (
    'fkContext' => '1817',
    'fkSectionBestPractice' => '1608',
    'nControlType' => '0',
    'sName' => '7.2.2 R�tulos e tratamento da informa��o',
    'tDescription' => 'Conv�m que um conjunto apropriado de procedimentos para rotula��o e tratamento da informa��o seja definido e implementado de acordo com o esquema de classifica��o adotado pela organiza��o. ',
    'tImplementationGuide' => 'Os procedimentos para rotula��o da informa��o precisam abranger tato os ativos de informa��o no formato f�sico como no eletr�nico.  Conv�m que as sa�das de sistemas que cont�m informa��es classificadas como sens�veis ou cr�ticas tenham o r�tulo apropriado da classifica��o da informa��o (na sa�da). Conv�m que o r�tulo reflita a classifica��o de acordo com as regras estabelecidas em 7.2.1. Itens que devem ser considerados incluem relat�rios impressos, telas, m�dias magn�ticas (fitas, discos, CD), mensagens eletr�nicas e transfer�ncias de arquivos.  Conv�m que sejam definidos, para cada n�vel de classifica��o, procedimentos para o tratamento da informa��o que contemplem o processamento seguro, a armazenagem, a transmiss�o, a reclassifica��o e a destrui��o. Conv�m que isto tamb�m inclua os procedimentos para a cadeia de cust�dia e registros de qualquer evento de seguran�a relevante.  Conv�m que acordos com outras organiza��es, que incluam o compartilhamento de informa��es, considerem procedimentos para identificar a classifica��o daquela informa��o e para interpretar os r�tulos de classifica��o de outras organiza��es. Informa��es adicionais A rotula��o e o tratamento seguro da classifica��o da informa��o � um requisito-chave para os procedimentos de compartilhamento da informa��o. Os r�tulos f�sicos s�o uma forma usual de rotula��o. Entretanto, alguns ativos de informa��o, como documentos em forma eletr�nica, n�o podem ser fisicamente rotulados, sendo necess�rio usar um r�tulo eletr�nico. Por exemplo, a notifica��o do r�tulo pode aparecer na tela ou no display. Onde a aplica��o do r�tulo n�o foi poss�vel, outras formas de definir a classifica��o da informa��o podem ser usadas, por exemplo, por meio de procedimentos ou metadados.  ',
  ),
  18 => 
  array (
    'fkContext' => '1818',
    'fkSectionBestPractice' => '1610',
    'nControlType' => '0',
    'sName' => '8.1.1 Pap�is e responsabilidades',
    'tDescription' => 'Conv�m que os pap�is e responsabilidades pela seguran�a da informa��o de funcion�rios, fornecedores e terceiros sejam definidos e documentados de acordo com a Pol�tica de Seguran�a da informa��o da organiza��o ',
    'tImplementationGuide' => 'Conv�m que os pap�is e responsabilidades pela seguran�a da informa��o incluam requisitos para:  a) implementar e agir de acordo com as pol�ticas de seguran�a da informa��o da organiza��o - ver 5.1 - b) proteger ativos contra acesso n�o autorizado, divulga��o, modifica��o, destrui��o ou interfer�ncia;
c) executar processos ou atividades particulares de seguran�a da informa��o;
d) assegurar que a responsabilidade � atribu�da � pessoa para tomada de a��es;
e) relatar eventos potenciais ou reais de seguran�a da informa��o ou outros riscos de seguran�a para a organiza��o.  Conv�m que pap�is e responsabilidades de seguran�a da informa��o sejam definidos e claramente comunicados aos candidatos a cargos, durante o processo de pr�-contrata��o. Informa��es adicionais Descri��es de cargos podem ser usadas para documentar responsabilidades e pap�is pela seguran�a da informa��o. Conv�m que pap�is e responsabilidades pela seguran�a da informa��o para pessoas que n�o est�o engajadas por meio do processo de contrata��o da organiza��o, como, por exemplo, atrav�s de uma organiza��o terceirizada, sejam claramente definidos e comunicados. ',
  ),
  19 => 
  array (
    'fkContext' => '1819',
    'fkSectionBestPractice' => '1610',
    'nControlType' => '0',
    'sName' => '8.1.2 Sele��o',
    'tDescription' => 'Conv�m que verifica��es de controle de todos os candidatos a emprego, fornecedores e terceiros sejam realizadas de acordo com as leis relevantes, regulamenta��es e �ticas, e proporcional aos requisitos do neg�cio, � classifica��o das informa��es a serem acessadas e aos riscos percebidos. ',
    'tImplementationGuide' => 'Conv�m que as verifica��es de controle levem em considera��o todos os aspectos relevantes relacionados com a privacidade, legisla��o baseada na contrata��o e/ou prote��o de dados pessoais e, onde permitido, incluam os seguintes itens:  a) disponibilidade de refer�ncia de car�ter satisfat�rias, por exemplo uma profissional e uma pessoal;
b) uma verifica��o (da exatid�o e inteireza) das informa��es do curriculum vitae do candidato;
c) confirma��o das qualifica��es acad�micas e profissionais;
d) verifica��o independente da identidade - passaporte ou documento similar;
e) verifica��es mais detalhadas, tais como verifica��es financeiras (de cr�dito) ou verifica��es de registros criminais. Conv�m que a organiza��o tamb�m fa�a verifica��es mais detalhadas, onde um trabalho envolver pessoas tanto por contrata��o como por promo��o, que tenham acesso aos recursos de processamento da informa��o em particular aqueles que tratam de informa��es sens�veis, tais como informa��es financeiras ou informa��es altamente confidenciais.  Conv�m que os procedimentos definam crit�rios e limita��es para as verifica��es de controle, por exemplo, quem est� qualificado para selecionar as pessoas, e como, quando e porque as verifica��es de controle s�o realizadas.  Conv�m que um processo de sele��o tamb�m seja feito para fornecedores e terceiros. Quando essas pessoas v�m por meio de uma ag�ncia, conv�m que o contrato especifique claramente as responsabilidades da ag�ncia pela sele��o e os procedimentos de notifica��o que devem ser seguidos se a sele��o n�o for devidamente conclu�da ou quando os resultados obtidos forem motivos de d�vidas ou preocupa��es. Do mesmo modo, conv�m que acordos com terceiros - ver 6.2.3 - especifiquem claramente todas as responsabilidades e procedimentos de notifica��o para a sele��o.  Conv�m que informa��es sobre todos os candidatos que est�o sendo considerados para certas posi��es dentro da organiza��o sejam levantadas e tratadas de acordo com qualquer legisla��o apropriada existente na jurisdi��o pertinente. Dependendo da legisla��o aplic�vel, conv�m que os candidatos sejam previamente informados sobre as atividades de sele��o. ',
  ),
  20 => 
  array (
    'fkContext' => '1820',
    'fkSectionBestPractice' => '1610',
    'nControlType' => '0',
    'sName' => '8.1.3 Termos e condi��es de contrata��o',
    'tDescription' => 'Como parte das suas obriga��es contratuais, conv�m que os funcion�rios, fornecedores e terceiros concordem e assinem os termos e condi��es de sua contrata��o para o trabalho, os quais devem declarar as suas responsabilidades e a da organiza��o para a seguran�a da informa��o. ',
    'tImplementationGuide' => 'Conv�m que os termos e condi��es de trabalho reflitam a Pol�tica de Seguran�a da organiza��o, esclarecendo e declarando:  a) que todos os funcion�rios, fornecedores e terceiros que tenham acesso a informa��es sens�veis assinem um termo de confidencialidade ou de n�o divulga��o antes de lhes ser dado o acesso aos recursos de processamento da informa��o;
b) as responsabilidades legais e direitos dos funcion�rios, fornecedores e quaisquer outros usu�rios, por exemplo, com rela��o �s leis de direito autorais ou � legisla��o de prote��o de dados - ver 15.1.1 e 15.1.2;
c) as responsabilidades pela classifica��o da informa��o e pelo gerenciamento dos ativos da organiza��o associados com os sistemas de informa��o e com os servi�os conduzidos pelos funcion�rios, fornecedores ou terceiros - ver 7.2.1 e 10.7.3;
d) as responsabilidades dos funcion�rios, fornecedores e terceiros pelo tratamento da informa��o recebida de outras companhias ou de partes externas;
e) responsabilidades da organiza��o pelo tratamento das informa��es pessoais, incluindo informa��es pessoais criadas como resultado de, ou em decorr�ncia da, contrata��o com a organiza��o - ver 15.1.4;
f) responsabilidades que se estendem para fora das depend�ncias da organiza��o e fora dos hor�rios normais de trabalho, como, por exemplo, nos casos de execu��o de trabalhos em casa - ver 9.2.5 e 11.7.1;
g) a��es a serem tomadas nos caso do funcion�rio, fornecedor ou terceiro desrespeitar os requisitos de seguran�a da informa��o da organiza��o - ver 8.2.3. Conv�m que a organiza��o assegure que os funcion�rios, fornecedores ou terceiros concordam com os termos e condi��es relativas � seguran�a da informa��o adequados � natureza e extens�o do acesso que eles ter�o aos ativos da organiza��o associados com os sistemas e servi�os de informa��o.  Conv�m que as responsabilidades contidas nos termos e condi��es de contrata��o continuem por um per�odo de tempo definido, ap�s o termino da contrata��o (ver 8.3), onde apropriado. Informa��es adicionais Um c�digo de conduta pode ser usado para contemplar as responsabilidades dos funcion�rios, fornecedores ou terceiros, em rela��o � confidencialidade, prote��o de dados, �ticas, uso apropriado dos recursos e dos equipamentos da organiza��o, bem como pr�ticas de boa conduta esperada pela organiza��o. O fornecedor ou o terceiro pode estar associado com uma organiza��o externa que possa, por sua vez, ser solicitada a participar de acordos contratuais, em nome do contratado. ',
  ),
  21 => 
  array (
    'fkContext' => '1821',
    'fkSectionBestPractice' => '1611',
    'nControlType' => '0',
    'sName' => '8.2.1 Responsabilidades da dire��o',
    'tDescription' => 'Conv�m que a dire��o solicite aos funcion�rios, fornecedores e terceiros que pratiquem a seguran�a da informa��o de acordo com o estabelecido nas pol�ticas e procedimentos da organiza��o. ',
    'tImplementationGuide' => 'Conv�m que as responsabilidades da dire��o assegurem que os funcion�rios, fornecedores e terceiros:  a) est�o adequadamente instru�dos sobre as suas responsabilidades e pap�is pela seguran�a da informa��o antes de obter acesso �s informa��es sens�veis ou aos sistemas de informa��o;
b) recebam diretrizes que definam quais as expectativas sobre a seguran�a da informa��o de suas atividades dentro da organiza��o;
c) est�o motivados para cumprir com as pol�ticas de seguran�a da informa��o da organiza��o;
d) atinjam um n�vel de conscientiza��o sobre seguran�a da informa��o que seja relevante para os seus pap�is e responsabilidades dentro da organiza��o - ver 8.2.2;
e) atendam aos termos e condi��es de contrata��o, que incluam a Pol�tica de Seguran�a da informa��o da organiza��o e m�todos apropriados de trabalho;
f) tenham as habilidades e qualifica��es apropriadas. Informa��es adicionais Se os funcion�rios, fornecedores e terceiros n�o forem conscientizados das suas responsabilidades, eles podem causar consider�veis danos para a organiza��o. Pessoas motivadas t�m uma maior probabilidade de serem mais confi�veis e de causar menos incidentes de seguran�a da informa��o.  Uma m� gest�o pode causar �s pessoas o sentimento de sub-valoriza��o, resultando em um impacto de seguran�a da informa��o negativo para a organiza��o. Por exemplo, uma m� gest�o pode levar a seguran�a da informa��o a ser negligenciada ou a um potencial mau uso dos ativos da organiza��o. ',
  ),
  22 => 
  array (
    'fkContext' => '1822',
    'fkSectionBestPractice' => '1611',
    'nControlType' => '0',
    'sName' => '8.2.2 Conscientiza��o, educa��o e treinamento em seguran�a da informa��o',
    'tDescription' => 'Conv�m que todos os funcion�rios da organiza��o e, onde pertinente, fornecedores e terceiros recebam treinamento apropriados em conscientiza��o, e atualiza��es regulares nas pol�ticas e procedimentos organizacionais, relevantes para as suas fun��es. ',
    'tImplementationGuide' => 'Conv�m que o treinamento em conscientiza��o comece com um processo formal de indu��o concebido para introduzir as pol�ticas e expectativas de seguran�a da informa��o da organiza��o, antes que seja dado o acesso �s informa��es ou servi�os.  Conv�m que os treinamentos em curso incluam requisitos de seguran�a da informa��o, responsabilidades legais e controles do neg�cio, bem como o treinamento do uso correto dos recursos de processamento da informa��o, como, por exemplo, procedimentos de log-on, o uso de pacotes de software e informa��es sobre o processo disciplinar - ver 8.2.3. Informa��es adicionais Conv�m que a conscientiza��o, educa��o e treinamento nas atividades de seguran�a da informa��o sejam adequados e relevantes para os pap�is, responsabilidades e habilidades da pessoa, e que incluam informa��es sobre conhecimento de amea�as, que devem ser contatado para orienta��es sobre seguran�a da informa��o e os canais adequados para relatar os incidentes de seguran�a da informa��o - ver 13.1 -  O treinamento para aumentar a conscientiza��o visa permitir que as pessoas reconhe�am os problemas e incidentes de seguran�a da informa��o, e respondam de acordo com as necessidades do seu trabalho. ',
  ),
  23 => 
  array (
    'fkContext' => '1823',
    'fkSectionBestPractice' => '1611',
    'nControlType' => '0',
    'sName' => '8.2.3 Processo disciplinar',
    'tDescription' => 'Conv�m que exista um processo disciplinar formal para os funcion�rios que tenham cometido uma viola��o da seguran�a da informa��o. ',
    'tImplementationGuide' => 'Conv�m que o processo disciplinar n�o inicie sem uma verifica��o pr�via de que a viola��o da seguran�a da informa��o.  Conv�m que o processo disciplinar formal assegure um tratamento justo e correto aos funcion�rios que s�o suspeitos de cometer viola��es de seguran�a da informa��o. O processo disciplinar formal deve dar uma resposta de forma gradual, que leve em considera��o fatores como a natureza e a gravidade da viola��o e o seu impacto no neg�cio, se este � ou n�o o primeiro delito, se o infrator foi ou n�o adequadamente treinado, as legisla��es relevantes, os contratos do neg�cio e outros fatores conforme requerido. Em casos s�rios de m� conduta, conv�m que o processo permita, por um certo per�odo, a remo��o das responsabilidades, dos direitos de acesso e privil�gios e, dependendo da situa��o, solicitar � pessoa, a sa�da imediata das depend�ncias da organiza��o, escoltando-a. Informa��es adicionais Conv�m que o processo disciplinar tamb�m seja usado como uma forma de dissuas�o, para evitar que os funcion�rios, fornecedores e terceiros violem os procedimentos e as pol�ticas de seguran�a da informa��o da organiza��o, e quaisquer outras viola��es na seguran�a. ',
  ),
  24 => 
  array (
    'fkContext' => '1824',
    'fkSectionBestPractice' => '1612',
    'nControlType' => '0',
    'sName' => '8.3.1 Encerramento de atividades',
    'tDescription' => 'Conv�m que responsabilidades para realizar o encerramento ou a mudan�a de um trabalho sejam claramente definidas e atribu�das. ',
    'tImplementationGuide' => 'Conv�m que a comunica��o de encerramento de atividades inclua requisitos de seguran�a e responsabilidades legais existentes e, onde apropriado, responsabilidades contidas em quaisquer acordos de confidencialidade - ver 6.1.5 - e os termos e condi��es de trabalho - ver 8.1.3 - que continuem por um per�odo definido ap�s o fim do trabalho do funcion�rio, do fornecedor ou do terceiro.  Conv�m que as responsabilidades e obriga��es contidas nos contratos dos funcion�rios, fornecedores ou terceiros permane�am validadas ap�s o encerramento das atividades.  Conv�m que as mudan�as de responsabilidades ou do trabalho sejam gerenciadas quando do encerramento da respectiva responsabilidade ou do trabalho, e que novas responsabilidades ou trabalhos sejam controladas conforme descrito em 8.1. Informa��es adicionais A fun��o de Recursos Humanos � geralmente respons�vel pelo processo global de encerramento e trabalha em conjunto com o gestor respons�vel pela pessoa que est� saindo, para gerenciar os aspectos de seguran�a da informa��o dos procedimentos pertinentes. No caso de um fornecedor, o processo de encerramento de atividades pode ser realizado por uma ag�ncia respons�vel pelo fornecedor e, no caso de outro usu�rio isto pode ser tratado pela sua organiza��o.  Pode ser necess�rio informar aos funcion�rios, clientes, fornecedores ou terceiros sobre as mudan�as de pessoal e procedimentos operacionais.',
  ),
  25 => 
  array (
    'fkContext' => '1825',
    'fkSectionBestPractice' => '1612',
    'nControlType' => '0',
    'sName' => '8.3.2 Devolu��o de ativos',
    'tDescription' => 'Conv�m que todos os funcion�rios, fornecedores e terceiros devolvam todos os ativos da organiza��o que estejam em sua posse, ap�s o encerramento de suas atividades, do contrato ou acordo. ',
    'tImplementationGuide' => 'Conv�m que o processo de encerramento de atividades seja formalizado para contemplar a devolu��o de todos os equipamentos, documentos corporativos e software entregues � pessoa. Outros ativos da organiza��o, tais como dispositivos de computa��o m�vel, cart�es de cr�dito, cart�es de acesso, software, manuais e informa��es armazenadas em m�dia eletr�nica, tamb�m precisam ser devolvidos.  No caso em que um funcion�rio, fornecedor ou terceiro compre o equipamento da organiza��o ou use o seu pr�prio equipamento pessoal, conv�m que procedimentos sejam adotados para assegurar que toda a informa��o relevante seja transferida para a organiza��o e que seja apagada de forma segura do equipamento - ver 10.7.1.  Nos casos em que o funcion�rio, fornecedor ou terceiro tenha conhecimento de que seu trabalho � importante para as atividades que s�o executadas, conv�m que este conhecimento seja documentado e transferido para a organiza��o. ',
  ),
  26 => 
  array (
    'fkContext' => '1826',
    'fkSectionBestPractice' => '1612',
    'nControlType' => '0',
    'sName' => '8.3.3 Retirada dos direitos de acesso',
    'tDescription' => 'Conv�m que os direitos de acesso de todos os funcion�rios, fornecedores e terceiros �s informa��es e aos recursos de processamento da informa��o sejam retirados ap�s o encerramento de suas atividades, contratos ou acordos, ou ajustado ap�s a mudan�a destas atividades. ',
    'tImplementationGuide' => 'Conv�m que os direitos de acesso da pessoa aos ativos associados com os sistemas de informa��o e servi�os sejam reconsiderados, ap�s o encerramento das atividades. Isto ir� determinar se � necess�rio retirar os direitos de acesso. Conv�m que mudan�as de uma atividade sejam refletidas na retirada de todos os direitos de acesso que n�o foram aprovados para o novo trabalho. Conv�m que os direitos de acesso que sejam retirados ou adaptados incluam o acesso l�gico e f�sico, chaves, cart�es de identifica��o, recursos de processamento da informa��o (ver 11.2.4), subscri��es e retirada de qualquer documenta��o que os identifiquem como um membro atual da organiza��o. Caso o funcion�rio, fornecedor ou terceiro que esteja saindo tenha conhecimento de senhas de contas que permanecem ativas, conv�m que essas sejam alteradas ap�s um encerramento das atividades, mudan�a do trabalho, contrato ou acordo.  Conv�m que os direitos de acesso aos ativos de informa��o e aos recursos de processamento da informa��o sejam reduzidos ou retirados antes que a atividade se encerre ou altere, dependendo da avalia��o de fatores de risco, tais como:  a) se o encerramento da atividade ou a mudan�a � iniciada pelo funcion�rio, fornecedor ou terceiro, ou pelo gestor e a raz�o do encerramento da atividade;
b) as responsabilidades atuais do funcion�rio, fornecedor ou qualquer outro usu�rio;
c) valor dos ativos atualmente acess�veis. Informa��es adicionais Em certas circunst�ncias os direitos de acesso podem ser alocados com base no que esta sendo disponibilizado para mais pessoas do que as que est�o saindo (funcion�rio, fornecedor ou terceiro), como, por exemplo, grupos de ID. Conv�m que, em tais casos, as pessoas que est�o saindo da organiza��o sejam retiradas de quaisquer listas de grupos  de acesso e que sejam tomadas provid�ncias para avisar aos outros funcion�rios, fornecedores e terceiros envolvidos para n�o mais compartilhar estas informa��es com a pessoa que est� saindo.  Nos casos em que o encerramento da atividade seja da iniciativa do gestor, os funcion�rios, fornecedores ou terceiros descontentes podem deliberadamente corromper a informa��o ou sabotar os recursos de processamento da informa��o. No caso de pessoas demitidas ou exoneradas, elas podem ser tentadas a coletar informa��es para uso futuro.   ',
  ),
  27 => 
  array (
    'fkContext' => '1827',
    'fkSectionBestPractice' => '1614',
    'nControlType' => '0',
    'sName' => '9.1.1 Per�metro de seguran�a f�sica',
    'tDescription' => 'Conv�m que sejam utilizados per�metros de seguran�a (barreiras tais como paredes, port�es de entrada controlados por cart�o ou balc�es de recep��o com recepcionistas) para proteger as �reas que contenham informa��es e instala��es de processamento da informa��o. ',
    'tImplementationGuide' => 'Conv�m que sejam levadas em considera��o e implementadas as seguintes diretrizes para per�metros de seguran�a f�sica, quando apropriado:  a) os per�metros de seguran�a sejam claramente definidos e que a localiza��o e a capacidade de resist�ncia de cada per�metro dependam dos requisitos de seguran�a dos ativos existentes no interior do per�metro, e dos resultados da an�lise/avalia��o de riscos;
b) os per�metros de um edif�cio ou de um local que contenha instala��es de processamento da informa��o sejam fisicamente s�lidos - ou seja, o per�metro n�o deve ter brechas nem pontos onde poderia ocorrer facilmente uma invas�o;
conv�m que as paredes externas do local sejam de constru��o robusta e todas as portas externas sejam adequadamente protegidas contra acesso n�o autorizado por meio de mecanismos de controle, por exemplo, barras, alarmes, fechaduras etc.;
conv�m que as portas e janelas sejam trancadas quando estiverem sem monitora��o, e que uma prote��o externa para as janelas seja considerada, principalmente para as que estiverem situadas no andar t�rreo;
c) seja implantada uma �rea de recep��o, ou um outro meio para controlar o acesso f�sico ao local ou ao edif�cio;
o acesso aos locais ou edif�cios deve ficar restrito somente ao pessoal autorizado;
d) sejam constru�das barreiras f�sicas, onde aplic�vel, para impedir o acesso f�sico n�o autorizado e a contamina��o do meio ambiente;
e) todas as portas corta-fogo do per�metro de seguran�a sejam providas de alarme, monitoradas e testadas juntamente com as paredes, para estabelecer o n�vel de seguran�a exigido, de acordo com as normas regionais, nacionais e internacionais aceit�veis;
elas devem funcionar de acordo com os c�digos locais de preven��o de inc�ndios e preven��o de falhas;
f) sistemas adequados de detec��o de intrusos, de acordo com normas regionais, nacionais e internacionais, sejam instalados e testados em intervalos regulares, e cubram todas as portas externas e janelas acess�veis;
as �reas n�o ocupadas devem ser protegidas por alarmes o tempo todo;
tamb�m deve ser dada prote��o a outras �reas, por exemplo, salas de computadores ou salas de comunica��es;
g) as instala��es de processamento da informa��o gerenciadas pela organiza��o devem ficar fisicamente separadas daquelas que s�o gerenciadas por terceiros. Informa��es adicionais Pode-se obter prote��o f�sica criando uma ou mais barreiras f�sicas ao redor das instala��es e dos recursos de processamento da informa��o da organiza��o. O uso de barreiras m�ltiplas proporciona uma prote��o adicional, uma vez que neste caso a falha de uma das barreiras n�o significa que a seguran�a fique comprometida imediatamente.  Uma �rea segura pode ser um escrit�rio tranc�vel ou um conjunto de salas rodeado por uma barreira f�sica interna cont�nua de seguran�a. Pode haver necessidade de barreiras e per�metros adicionais para o controle do acesso f�sico, quando existem �reas com requisitos de seguran�a diferentes dentro do per�metro de seguran�a.  Conv�m que sejam tomadas precau��es especiais para a seguran�a do acesso f�sico no caso de edif�cios que alojam diversas organiza��es. ',
  ),
  28 => 
  array (
    'fkContext' => '1828',
    'fkSectionBestPractice' => '1614',
    'nControlType' => '0',
    'sName' => '9.1.2 Controles de entrada f�sica',
    'tDescription' => 'Conv�m que as �reas seguras sejam protegidas por controles apropriados de entrada para assegurar que somente pessoas autorizadas tenham acesso. ',
    'tImplementationGuide' => 'Conv�m que sejam levadas em considera��o as seguintes diretrizes:  a) a data e hora da entrada e sa�da de visitantes sejam registradas, e todos os visitantes sejam supervisionados, a n�o ser que o seu acesso tenha sido previamente aprovado;
conv�m que as permiss�es de acesso sejam concedidas somente para finalidades espec�ficas e autorizadas, e sejam emitidas com instru��es sobre os requisitos de seguran�a da �rea e os procedimentos de emerg�ncia;
b) acesso �s �reas em que s�o processadas ou armazenadas informa��es sens�veis seja controlado e restrito �s pessoas autorizadas;
conv�m que sejam utilizados controles de autentica��o, por exemplo, cart�o de controle de acesso mais PIN (personal identification number), para autorizar e validar todos os acessos;
deve ser mantido de forma segura um registro de todos os acessos para fins de auditoria;
c) seja exigido que todos os funcion�rios, fornecedores e terceiros, e todos os visitantes, tenham alguma forma vis�vel de identifica��o, e eles devem avisar imediatamente o pessoal de seguran�a caso encontrem visitantes n�o acompanhados ou qualquer pessoa que n�o esteja usando uma identifica��o vis�vel;
d) aos terceiros que realizam servi�os de suporte, seja concedido acesso restrito �s �reas seguras ou �s instala��es de processamento da informa��o sens�vel somente quando necess�rio;
este acesso deve ser autorizado e monitorado;
e) os direitos de acesso a �reas seguras sejam revistos e atualizados em intervalos regulares, e revogados quando necess�rio - ver 8.3.3.',
  ),
  29 => 
  array (
    'fkContext' => '1829',
    'fkSectionBestPractice' => '1614',
    'nControlType' => '0',
    'sName' => '9.1.3 Seguran�a em escrit�rios, salas e instala��es',
    'tDescription' => 'Conv�m que seja projetada e aplicada seguran�a f�sica para escrit�rios, salas e instala��es. ',
    'tImplementationGuide' => 'Conv�m que sejam levadas em considera��o as seguintes diretrizes para proteger escrit�rios, salas e instala��es:  a) sejam levados em conta os regulamentos e normas de sa�de e seguran�a aplic�veis;
b) as instala��es-chave sejam localizadas de maneira a evitar o acesso do p�blico;
c) os edif�cios sejam discretos e d�em a menor indica��o poss�vel de sua finalidade, sem letreiros evidentes, fora ou dentro do edif�cio, que identifiquem a presen�a de atividades de processamento de informa��es, quando for aplic�vel;
d) as listas de funcion�rios e guias telef�nicos internos que identifiquem, a localiza��o das instala��es que processam informa��es sens�veis n�o fiquem facilmente acess�veis ao p�blico. ',
  ),
  30 => 
  array (
    'fkContext' => '1830',
    'fkSectionBestPractice' => '1614',
    'nControlType' => '0',
    'sName' => '9.1.4 Prote��o contra amea�as externas e do meio ambiente',
    'tDescription' => 'Conv�m que sejam projetadas e aplicadas prote��o f�sica contra inc�ndios, enchentes, terremotos, explos�es, perturba��es da ordem p�blica e outras formas de desastres naturais ou causados pelo homem. ',
    'tImplementationGuide' => 'Conv�m que sejam levadas em considera��o todas as amea�as � seguran�a representada por instala��es vizinhas, por exemplo, um inc�ndio em um edif�cio vizinho, vazamento de �gua do telhado ou em pisos do subsolo ou uma explos�o na rua.  Conv�m que sejam levadas em considera��o as seguintes diretrizes para evitar danos causados por inc�ndios, enchentes, terremotos, explos�es, perturba��es da ordem p�blica e outras formas de desastres naturais ou causados pelo homem:  a) os materiais perigosos ou combust�veis sejam armazenados a uma dist�ncia segura da �rea de seguran�a. Suprimentos em grande volume, como materiais de papelaria, n�o devem ser armazenados dentro de uma �rea segura;
b) os equipamentos para conting�ncia e m�dia de backup fiquem a uma dist�ncia segura, para que n�o sejam danificados por um desastre que afete o local principal;
c) os equipamentos apropriados de detec��o e combate a inc�ndios sejam providenciados e posicionados corretamente. ',
  ),
  31 => 
  array (
    'fkContext' => '1831',
    'fkSectionBestPractice' => '1614',
    'nControlType' => '0',
    'sName' => '9.1.5 Trabalhando em �reas seguras',
    'tDescription' => 'Conv�m que seja projetada e aplicada prote��o f�sica, bem como diretrizes para o trabalho em �reas seguras. ',
    'tImplementationGuide' => 'Conv�m que sejam levadas em considera��o as seguintes diretrizes:  a) pessoal s� tenha conhecimento da exist�ncia de �reas seguras ou das atividades nelas realizadas, apenas se for necess�rio;
b) seja evitado o trabalho n�o supervisionado em �reas seguras, tanto por motivos de seguran�a como para prevenir as atividades mal intencionadas;
c) as �reas seguras n�o ocupadas sejam fisicamente trancadas e periodicamente verificadas;
d) n�o seja permitido o uso de m�quinas fotogr�ficas, gravadores de v�deo ou �udio ou de outros equipamentos de grava��o, tais como c�meras em dispositivos m�veis, salvo se for autorizado.  As normas para o trabalho em �reas seguras incluem o controle dos funcion�rios, fornecedores e terceiros que trabalham em tais �reas, bem como o controle de outras atividades de terceiros nestas �reas. ',
  ),
  32 => 
  array (
    'fkContext' => '1832',
    'fkSectionBestPractice' => '1614',
    'nControlType' => '0',
    'sName' => '9.1.6 Acesso do p�blico, �reas de entrega e de carregamento',
    'tDescription' => 'Conv�m que pontos de acesso, tais como �reas de entrega e de carregamento e outros pontos em que pessoas n�o autorizadas possam entrar nas instala��es, sejam controlados e, se poss�vel, isolados das instala��es de processamento da informa��o, para evitar o acesso n�o autorizado. ',
    'tImplementationGuide' => 'Conv�m que sejam levantadas em considera��o as seguintes diretrizes:  a) acesso a uma �rea de entrega e carregamento a partir do exterior do pr�dio fique restrito ao pessoal identificado e autorizado;
b) as �reas de entrega e carregamento sejam projetadas de tal maneira que seja poss�vel descarregar suprimentos sem que os entregadores tenham acesso a outras partes do edif�cio;
c) as portas externas de uma �rea de entrega e carregamento sejam protegidas enquanto as portas externas estiverem abertas;
d) os materiais entregues sejam inspecionados para detectar amea�as potenciais - ver 9.2.1 - antes de serem transportados da �rea de entrega e carregamento para o local de utiliza��o;
e) os materiais entregues sejam registrados por ocasi�o de sua entrada no local, usando-se procedimentos de gerenciamento de ativos - ver 7.1.1;
f) as remessas entregues sejam segregadas fisicamente das remessas que saem, sempre que poss�vel. ',
  ),
  33 => 
  array (
    'fkContext' => '1833',
    'fkSectionBestPractice' => '1615',
    'nControlType' => '0',
    'sName' => '9.2.1 Instala��o e prote��o do equipamento',
    'tDescription' => 'Conv�m que os equipamentos sejam colocados no local ou protegidos para reduzir os riscos de amea�as e perigos do meio ambiente, bem como as oportunidades de acesso n�o autorizado. ',
    'tImplementationGuide' => 'Conv�m que sejam levadas em considera��o as seguintes diretrizes para proteger os equipamentos:  a) os equipamentos sejam colocados no local, a fim de minimizar o acesso desnecess�rio �s �reas de trabalho;
b) as instala��es de processamento da informa��o que manuseiem dados sens�veis sejam posicionadas de forma que o �ngulo de vis�o seja restrito, de modo a reduzir o risco de que as informa��es sejam vistas por pessoal n�o autorizado durante a sua utiliza��o, e os locais de armazenagem sejam protegidos, a fim de evitar o acesso n�o autorizado;
c) os itens que exigem prote��o especial devem ser isolados para reduzir o n�vel geral de prote��o necess�rio;
d) sejam adotados controles para minimizar o risco de amea�as f�sicas potenciais, tais como furto, inc�ndio, explosivos, fuma�a, �gua (ou falha do suprimento de �gua), poeira, vibra��o, efeitos qu�micos, interfer�ncia com o suprimento de energia el�trica, interfer�ncia com as comunica��es, radia��o eletromagn�tica e vandalismo;
e) sejam estabelecidas diretrizes quanto a comer, beber e fumar nas proximidades das instala��es de processamento da informa��o;
f) as condi��es ambientais, como temperatura e umidade, sejam monitoradas para a detec��o de condi��es que possam afetar negativamente os recursos de processamento da informa��o;
g) todos os edif�cios estejam dotados de prote��o contra raios e todas as linhas de entrada de for�a e de comunica��es tenham filtros de prote��o contra raios;
h) para equipamentos em ambientes industriais, o uso de m�todos especiais de prote��o, tais como membranas para teclados, deve ser considerado;
i) os equipamentos que processam informa��es sens�veis sejam protegidos, a fim de minimizar o risco de vazamento de informa��es em decorr�ncia de emana��es. ',
  ),
  34 => 
  array (
    'fkContext' => '1834',
    'fkSectionBestPractice' => '1615',
    'nControlType' => '0',
    'sName' => '9.2.2 Utilidades',
    'tDescription' => 'Conv�m que os equipamentos sejam protegidos contra falta de energia el�trica e outras interrup��es causadas por falhas das utilidades. ',
    'tImplementationGuide' => 'Conv�m que todas as unidades, tais como suprimento de energia el�trica, suprimento de �gua, esgotos, calefa��o/ventila��o e ar condicionado sejam adequados para os sistemas que eles suportam. Conv�m que as utilidades sejam inspecionadas em intervalos regulares e testadas de maneira apropriada para assegurar seu funcionamento correto e reduzir os riscos de defeitos ou interrup��es do funcionamento. Conv�m que seja providenciado um suprimento adequado de energia el�trica, de acordo com as especifica��es do fabricante dos equipamentos.  Recomenda-se o uso de UPS para suportar as paradas e desligamento dos equipamentos ou para manter o funcionamento cont�nuo dos equipamentos que suportam opera��es cr�ticas do neg�cio. Conv�m que hajam planos de conting�ncia de energia referentes �s provid�ncias a serem tomadas em caso de falha do UPS. Conv�m que seja considerado um gerador de emerg�ncia caso seja necess�rio que o processamento continue mesmo se houver uma interrup��o prolongada do suprimento de energia. Conv�m que esteja dispon�vel um suprimento adequado de combust�vel para garantir a opera��o prolongada do gerador. Conv�m que os equipamentos UPS e os geradores sejam verificados em intervalos regulares para assegurar que eles tenham capacidade adequada, e sejam testados de acordo com as recomenda��es do fabricante. Al�m disso, deve ser considerado o uso de m�ltiplas fontes de energia ou de uma subesta��o de for�a separada, se o local for grande.  Conv�m que as chaves de emerg�ncia para o desligamento da energia fiquem localizadas na proximidade das sa�das de emerg�ncia das salas de equipamentos, para facilitar o desligamento r�pido de energia em caso de uma emerg�ncia. Conv�m que seja providenciada ilumina��o de emerg�ncia para o caso de queda da for�a.  Conv�m que o suprimento de �gua seja est�vel e adequado para abastecer os equipamentos de ar condicionado e de umidifica��o, bem como os sistemas de extin��o de inc�ndios (quando usados). Falhas de funcionamento do abastecimento de �gua podem danificar o sistema ou impedir uma a��o eficaz de extin��o de inc�ndios. Conv�m que seja analisada a necessidade de sistemas de alarme para detectar falhas de funcionamento das utilidades, instalando os alarmes, se necess�rio.  Conv�m que os equipamentos de telecomunica��es sejam conectados � rede p�blica de energia el�trica atrav�s de pelo menos duas linhas separadas para evitar que a falha de uma das conex�es interrompa os servi�os de voz. Conv�m que os servi�os de voz sejam adequados para atender as exig�ncias legais locais relativas a comunica��es de emerg�ncia. Informa��es adicionais As op��es para assegurar a continuidade do suprimento de energia incluem m�ltiplas linhas de entrada, para evitar que uma falha em um �nico ponto comprometa o suprimento de energia. ',
  ),
  35 => 
  array (
    'fkContext' => '1835',
    'fkSectionBestPractice' => '1615',
    'nControlType' => '0',
    'sName' => '9.2.3 Seguran�a do cabeamento',
    'tDescription' => 'Conv�m que o cabeamento de energia e de telecomunica��es que transporta dados ou d� suporte aos servi�os de informa��es seja protegido contra intercepta��o ou danos. ',
    'tImplementationGuide' => 'Conv�m que sejam levadas em considera��o as seguintes diretrizes para a seguran�a do cabeamento:  a) as linhas de energia e de telecomunica��es que entram nas instala��es de processamento da informa��o sejam subterr�neas (ou fiquem abaixo do piso), sempre que poss�vel, ou recebam uma prote��o alternativa adequada;
b) cabeamento de redes seja protegido contra intercepta��o n�o autorizada ou danos, por exemplo, pelo uso de condu�tes ou evitando trajetos que passem por �reas p�blicas;
c) os cabos de energia sejam segregados dos cabos de comunica��es, para evitar interfer�ncias;
d) nos cabos e nos equipamentos, sejam utilizadas marca��es claramente identific�veis, a fim de minimizar erros de manuseio, como por exemplo, fazer de forma acidental conex�es erradas em cabos de rede;
e) seja utilizada uma lista de documenta��o das conex�es para reduzir a possibilidade de erros;
f) para sistemas sens�veis ou cr�ticos, os seguintes controles adicionais devem ser considerados;
1) instala��o de condu�tes blindados e salas ou caixas trancadas em pontos de inspe��o e pontos terminais;
2) uso de rotas alternativas e/ou meios de transmiss�o alternativos que proporcionem seguran�a adequada;
3) utiliza��o de cabeamento de fibras �pticas;
4) utiliza��o de blindagem eletromagn�tica para a prote��o dos cabos;
5) realiza��o de varreduras t�cnicas e inspe��es f�sicas para detectar a presen�a de dispositivos n�o autorizados conectados aos cabos;
6) acesso controlado aos pain�is de conex�es e �s salas de cabos.',
  ),
  36 => 
  array (
    'fkContext' => '1836',
    'fkSectionBestPractice' => '1615',
    'nControlType' => '0',
    'sName' => '9.2.4 Manuten��o dos equipamentos',
    'tDescription' => 'Conv�m que os equipamentos tenham uma manuten��o correta para assegurar sua disponibilidade e integridade permanentes.',
    'tImplementationGuide' => 'Conv�m que sejam levadas em considera��o as seguintes diretrizes para a manuten��o dos equipamentos:  a) a manuten��o dos equipamentos seja realizada nos intervalos recomendados pelo fornecedor, e de acordo com as suas especifica��es;
b) a manuten��o e os consertos dos equipamentos sejam realizados somente por pessoal de manuten��o autorizado;
c) sejam mantidos os registros de todas as falhas, suspeitas ou reais, e de todas as opera��es de manuten��o preventiva e corretiva realizadas;
d) sejam implementados controles apropriados, na �poca programada para a manuten��o do equipamento, dependendo de a manuten��o ser realizada pelo pessoal do local ou por pessoal externo � organiza��o;
onde necess�rio, as informa��es sens�veis sejam eliminadas do equipamento, ou o pessoal de manuten��o seja de absoluta confian�a;
e) sejam atendidas todas as exig�ncias estabelecidas nas ap�lices de seguro.',
  ),
  37 => 
  array (
    'fkContext' => '1837',
    'fkSectionBestPractice' => '1615',
    'nControlType' => '0',
    'sName' => '9.2.5 Seguran�a de equipamentos fora das depend�ncias da organiza��o',
    'tDescription' => 'Conv�m que sejam tomadas medidas de seguran�a para equipamentos que operem fora do local, levando em conta os diferentes riscos decorrentes do fato de se trabalhar fora das depend�ncias da organiza��o. ',
    'tImplementationGuide' => 'Conv�m que, independentemente de quem seja o propriet�rio, a utiliza��o de quaisquer equipamentos de processamento de informa��es fora das depend�ncias da organiza��o seja autorizado pela ger�ncia.  Conv�m que sejam levadas em considera��o as seguintes diretrizes para a prote��o de equipamentos usados fora das depend�ncias da organiza��o:  a) os equipamentos e suportes f�sicos de dados removidos das depend�ncias da organiza��o n�o fiquem sem supervis�o em lugares p�blicos;
os computadores port�teis sejam carregados como bagagem de m�o e disfar�ados, sempre que poss�vel, quando se viaja;
b) sejam observadas a qualquer tempo as instru��es do fabricante para a prote��o do equipamento, por exemplo, prote��o contra a exposi��o a campos eletromagn�ticos intensos;
c) os controles para o trabalho em casa sejam determinados por uma an�lise/avalia��o de riscos, sendo aplicados controles adequados para cada caso, por exemplo, arquivos tranc�veis, pol�tica de \'\'mesa limpa\'\', controles de acesso a computadores, e comunica��o segura com o escrit�rio - ver ISSO/IEC 18028 Network security;
d) haja uma cobertura adequada de seguro para proteger os equipamentos fora das depend�ncias da organiza��o.  Os riscos de seguran�a, por exemplo, de danos, furto ou espionagem, podem variar consideravelmente de um local para outro e conv�m que sejam levados em conta para determinar os controles mais apropriados. Informa��es adicionais Os equipamentos de armazenagem e processamento de informa��es incluem todas as formas de computadores pessoais, agendas eletr�nicas, telefones celulares, cart�es inteligentes, pap�is e outros tipos, utilizados no trabalho em casa, ou que s�o removidos do local normal de trabalho.  Mais informa��es sobre outros aspectos da prote��o de equipamentos m�veis podem ser encontradas em 11.7.1.',
  ),
  38 => 
  array (
    'fkContext' => '1838',
    'fkSectionBestPractice' => '1615',
    'nControlType' => '0',
    'sName' => '9.2.6 Reutiliza��o e aliena��o segura de equipamentos',
    'tDescription' => 'Conv�m que todos os equipamentos que contenham m�dias de armazenamento de dados sejam examinados antes do descarte, para assegurar que todos os dados sens�veis e softwares licenciados tenham sido removidos ou sobregravados com seguran�a. ',
    'tImplementationGuide' => 'Conv�m que os dispositivos defeituosos que contenham informa��es sens�veis, pode ser necess�ria uma an�lise/avalia��o de riscos para determinar se conv�m destruir fisicamente o dispositivo em vez de se usarem as fun��es-padr�o de apagar ou formatar.  Informa��es adicionais No caso de dispositivos defeituosos que contenham informa��es sens�veis, pode ser necess�ria uma an�lise/avalia��o de riscos para determinar se conv�m destruir fisicamente o dispositivo em vez de mand�-lo para o conserto ou descart�-lo.  As informa��es podem ser comprometidas por um descarte feito sem os devidos cuidados ou pela reutiliza��o do equipamento - ver 10.7.2. ',
  ),
  39 => 
  array (
    'fkContext' => '1839',
    'fkSectionBestPractice' => '1615',
    'nControlType' => '0',
    'sName' => '9.2.7 Remo��o de propriedade',
    'tDescription' => 'Conv�m que equipamentos, informa��es ou software n�o sejam retirados do local sem autoriza��o pr�via. ',
    'tImplementationGuide' => 'Conv�m que sejam levadas em considera��o as seguintes diretrizes:  a) os equipamentos, informa��es ou software n�o sejam retirados do local sem autoriza��o pr�via;
b) os funcion�rios, fornecedores e terceiros que tenham autoridade para permitir a remo��o de ativos para fora do local sejam claramente identificados;
c) sejam estabelecidos limites de tempo para a retirada de equipamentos do local, e a devolu��o seja controlada;
d) sempre que necess�rio ou apropriado, seja feito um registro da retirada e da devolu��o de equipamentos, quando do seu retorno. Informa��es adicionais Podem ser feitas inspe��es aleat�rias para detectar a retirada n�o autorizada de bens e a exist�ncia de equipamentos de grava��o n�o autorizados, armas etc., e impedir sua entrada no local. Conv�m que tais inspe��es aleat�rias sejam feitas de acordo com a legisla��o e as normas aplic�veis. Conv�m que as pessoas sejam avisadas da realiza��o das inspe��es, e elas s� podem ser feitas com a devida autoriza��o, levando em conta as exig�ncias legais e regulamentares. ',
  ),
  40 => 
  array (
    'fkContext' => '1840',
    'fkSectionBestPractice' => '1617',
    'nControlType' => '0',
    'sName' => '10.1.1 Documenta��o dos procedimentos de opera��o',
    'tDescription' => 'Conv�m que os procedimentos de opera��o sejam documentados, mantidos atualizados e dispon�veis a todos os usu�rios que deles necessitem. ',
    'tImplementationGuide' => 'Conv�m que procedimentos documentados sejam preparados para as atividades de sistemas associadas a recursos de processamento e comunica��o de informa��es, tais como procedimentos de inicializa��o e desligamento de computadores, gera��o de c�pias de seguran�a (backup), manuten��o de equipamentos, tratamento de m�dias, seguran�a e gest�o do tratamento das correspond�ncias e das salas de computadores.  Conv�m que os procedimentos de opera��o especifiquem as instru��es para a execu��o detalhada de cada tarefa, incluindo:  a) processamento e tratamento da informa��o;
b) backup - ver 10.5;
c) requisitos de agendamento, incluindo interdepend�ncias com outros sistemas,a primeira hora para in�cio da tarefa e a �ltima hora para o t�rmino da tarefa;
d) instru��es para tratamento de erros ou outras condi��es excepcionais, que possam ocorrer durante a execu��o de uma tarefa, incluindo restri��es de uso dos utilit�rios do sistema - ver 11.5.4;
e) dados para contatos de suporte para o caso de eventos operacionais inesperados ou dificuldades t�cnicas;
f) instru��es para tratamento de resultados especiais e m�dias, tais como o uso de formul�rios especiais ou o gerenciamento de resultados confidenciais, incluindo procedimentos para a aliena��o segura de resultados provenientes de rotinas com falhas - ver 10.7.2 e 10.7.3;
g) procedimento para o rein�cio e recupera��o em caso de falha do sistema;
h) gerenciamento de trilhas de auditoria e informa��es de registros (log) de sistemas - ver 10.10. Conv�m que procedimentos operacionais e os procedimentos documentados para atividades de sistemas sejam tratados como documentos formais e as mudan�as sejam autorizadas pela dire��o. Quando tecnicamente poss�vel, conv�m que os sistemas de informa��o sejam gerenciados uniformemente, usando os mesmo procedimentos, ferramentas e utilit�rios.  ',
  ),
  41 => 
  array (
    'fkContext' => '1841',
    'fkSectionBestPractice' => '1617',
    'nControlType' => '0',
    'sName' => '10.1.2 Gest�o de mudan�as',
    'tDescription' => 'Conv�m que modifica��es nos recursos de processamento da informa��o e sistema sejam controladas. ',
    'tImplementationGuide' => 'Conv�m que sistemas operacionais e aplicativos estejam sujeitos a r�gido controle de gest�o de mudan�as. Em particular, conv�m que os seguintes itens sejam considerados:  a) identifica��o e registro das mudan�as significativas;
b) planejamento e testes das mudan�as;
c) avalia��o de impactos potenciais, incluindo impactos de seguran�a, de tais mudan�as;
d) procedimento formal de aprova��o das mudan�as propostas;
e) comunica��o dos detalhes das mudan�as para todas as pessoas envolvidas;
f) procedimentos de recupera��o, incluindo procedimentos e responsabilidades pela interrup��o e recupera��o de mudan�as em caso de insucesso ou na ocorr�ncia de eventos inesperados.  Conv�m eu sejam estabelecidos os procedimentos e responsabilidades gerenciais formais para garantir que haja um controle satisfat�rio de todas as mudan�as em equipamentos, software ou procedimentos. Quando mudan�as forem realizadas, conv�m que seja mantido um registro de auditoria contendo todas as informa��es relevantes. Informa��es adicionais O controle inadequado de modifica��es nos sistemas e nos recursos de processamento da informa��o � uma causa comum de falhas de seguran�a ou de sistemas. Mudan�as a ambientes operacionais, especialmente quando da transfer�ncia de um sistema em desenvolvimento para o est�gio operacional, podem trazer impactos � confiabilidade de aplica��es - ver 12.5.1. Conv�m que mudan�as em sistemas operacionais sejam apenas realizadas quando houver uma raz�o de neg�cio v�lida para tal, como um aumento no risco do sistema. A atualiza��o de sistemas �s vers�es mais atuais de sistemas operacionais ou aplicativos nem sempre � do interresse do neg�cio, pois pode introduzir mais vulnerabilidades e instabilidades ao ambiente do que a vers�o corrente. Pode haver ainda a necessidade de treinamento adicional, custos de licenciamento, suporte, manuten��o e sobrecarga de administra��o, bem como a necessidade de novos equipamentos, especialmente durante a fase de migra��o. ',
  ),
  42 => 
  array (
    'fkContext' => '1842',
    'fkSectionBestPractice' => '1617',
    'nControlType' => '0',
    'sName' => '10.1.3 Segrega��o de fun��es',
    'tDescription' => 'Conv�m que fun��es e �reas de responsabilidade sejam segregadas para reduzir as oportunidades de modifica��o ou uso indevido n�o autorizado ou n�o intencional dos ativos da organiza��o ',
    'tImplementationGuide' => 'A segrega��o de fun��es � um m�todo para redu��o do risco de uso indevido acidental ou deliberado dos sistemas. Conv�m que sejam tomados certos cuidados para impedir que uma �nica pessoa possa acessar, modificar ou usar ativos sem a devida autoriza��o ou detec��o. Conv�m que o in�cio de um evento seja separado de sua autoriza��o. Conv�m que a possibilidade de exist�ncia de conluios seja considerada no projeto dos controles.  As pequenas organiza��es podem considerar a segrega��o de fun��es dif�cil de ser implantada, mas conv�m que os seu principio seja aplicado sempre que poss�vel e pratic�vel. Onde for dif�cil a segrega��o, conv�m que outros controles, como a monitora��o das atividades, trilhas de auditoria e o acompanhamento gerencial, sejam considerados. � importante que a auditoria da seguran�a permane�a como uma atividade independente. ',
  ),
  43 => 
  array (
    'fkContext' => '1843',
    'fkSectionBestPractice' => '1617',
    'nControlType' => '0',
    'sName' => '10.1.4 Separa��o dos recursos de desenvolvimento, teste e de produ��o',
    'tDescription' => 'Conv�m que recursos de desenvolvimento, teste e produ��o sejam separados para reduzir o risco de acessos ou modifica��es n�o autorizadas aos sistemas operacionais. ',
    'tImplementationGuide' => 'Conv�m que o n�vel de separa��o dos ambientes de produ��o, testes e desenvolvimento que � necess�rio para prevenir problemas operacionais seja identificado e os controles apropriados sejam implementados.  Conv�m que os seguintes itens sejam considerados:  a) as regras para transfer�ncia de software da situa��o de desenvolvimento para a de produ��o sejam definidas e documentadas;
b) software em desenvolvimento e o software em produ��o sejam, sempre que poss�vel, executados em diferentes sistemas ou processadores e em diferentes dom�nios ou diret�rios;
c) os compiladores, editores e outras ferramentas de desenvolvimento ou utilit�rios de sistemas sejam acess�veis aos sistemas operacionais, quando n�o dor necess�rio;
d) os ambientes de testes emulem o ambiente de produ��o o mais pr�ximo poss�vel;
e) os usu�rios tenham diferentes perfis para sistemas em testes e em produ��o, e que os menus mostrem mensagens apropriadas de identifica��o para reduzir o risco de erro;
f) os dados sens�veis n�o sejam copiados para os ambientes de testes - ver 12.4.2. Informa��es adicionais As atividades de desenvolvimento e teste podem causar s�rios problemas, como, por exemplo, modifica��es inesperadas em arquivos ou sistemas ou falhas de sistemas. Nesse caso, � necess�ria a manuten��o de um ambiente conhecido e est�vel, no qual possam ser executados testes significativos e que seja capaz de prevenir o acesso indevido do pessoal de desenvolvimento.  Quando o pessoal de desenvolvimento e teste possui acesso ao ambiente de produ��o e suas informa��es, eles podem introduzir c�digos n�o testados e n�o autorizados, ou mesmo alterar os dados do sistema. Em alguns sistemas essa capacidade pode ser mal utilizada para a execu��o de fraudes, introdu��o de c�digos maliciosos ou n�o testados, que podem causar s�rios problemas operacionais.  O pessoal de desenvolvimento e testes tamb�m representa uma amea�a � confidencialidade das informa��es de produ��o. As atividades de desenvolvimento e teste podem causar modifica��es n�o intencionais no software e informa��es se eles compartilharem o mesmo ambiente computacional. A separa��o dos ambientes de desenvolvimento, teste e produ��o s�o, portanto, desej�vel para reduzir o risco de modifica��es acidentais ou acessos n�o autorizados aos sistemas operacionais e aos dados do neg�cio. - ver 12.4.2 para prote��o de dados de teste.',
  ),
  44 => 
  array (
    'fkContext' => '1844',
    'fkSectionBestPractice' => '1618',
    'nControlType' => '0',
    'sName' => '10.2.1 Entrega de servi�os',
    'tDescription' => 'Conv�m que seja garantido que os controles de seguran�a, as defini��es de servi�o e os n�veis de entrega inclu�dos no acordo de entrega de servi�os terceirizados sejam implementados, executados e mantidos pelo terceiro. ',
    'tImplementationGuide' => 'Conv�m que a entrega de servi�os por um terceiro inclua os arranjos de seguran�a acordados, defini��es de servi�o e aspectos do gerenciamento de servi�os. No caso de acordos de terceiriza��o, conv�m  que a organiza��o planeje as transi��es necess�rias (de informa��o, recursos de processamento de informa��es e quaisquer outros que necessitem de movimenta��o) e garanta que a seguran�a seja mantida durante todo o per�odo de transi��o.  Conv�m que a organiza��o garanta que o terceiro mantenha capacidade de servi�o suficiente, juntamente com planos vi�veis projetados para garantir que os n�veis de continuidade de servi�os acordados sejam mantidos ap�s falhas de servi�os severas ou desastres - ver 14.1.',
  ),
  45 => 
  array (
    'fkContext' => '1845',
    'fkSectionBestPractice' => '1618',
    'nControlType' => '0',
    'sName' => '10.2.2 Monitoramento e an�lise cr�tica de servi�os terceirizados',
    'tDescription' => 'Conv�m que os servi�os, relat�rios e registros fornecidos por terceiros sejam regularmente monitorados e analisados criticamente, e que auditorias sejam executadas regularmente. ',
    'tImplementationGuide' => 'Conv�m que a monitora��o e an�lise cr�tica dos servi�os terceirizados garantam a ader�ncia entre os termos de seguran�a de informa��o e as condi��es dos acordos, e que problemas e incidentes de seguran�a da informa��o sejam gerenciados adequadamente. Conv�m que isto envolva processos e rela��es de gerenciamento de servi�o entre a organiza��o e o terceiro para:  a) monitorar n�veis de desempenho de servi�o para verificar ader�ncia aos acordos;
b) analisar criticamente os relat�rios de servi�os produzidos por terceiros e agendamento de reuni�es de progresso conforme requerido pelos acordos;
c) fornecer informa��es acerca de incidentes de seguran�a da informa��o e an�lise cr�tica de tais informa��es tanto pelo terceiro quanto pela organiza��o, como requerido pelos acordos e por quaisquer procedimentos e diretrizes que os ap�iem;
d) analisar criticamente as trilhas de auditoria do terceiro e registros de eventos de seguran�a, problemas operacionais, falhas, investiga��o de falhas e interrup��es relativas ao servi�o entregue;
e) resolver e gerenciar quaisquer problemas identificados.  Conv�m que a responsabilidade do gerenciamento de relacionamento com o terceiro seja atribu�da a um indiv�duo designado ou equipe de gerenciamento de servi�o. Adicionalmente, conv�m que a organiza��o garanta que o terceiro atribua responsabilidades pela verifica��o de conformidade e refor�o aos requisitos dos acordos. Conv�m que habilidades t�cnicas suficientes e recursos sejam disponibilizados para monitorar se os requisitos dos acordos ( ver 6.2.3), em particular os requisitos de seguran�a da informa��o, est�o sendo atendidos. Conv�m que a��es apropriadas sejam tomadas quando defici�ncias na entrega dos servi�os forem observadas.  Conv�m que a organiza��o mantenha suficiente controle geral e visibilidade em todos os aspectos de seguran�a para as informa��es sens�veis ou cr�ticas ou para os recursos de processamento da informa��o acessados, processados ou gerenciados por um terceiro. Conv�m que a organiza��o garanta a reten��o da visibilidade nas atividades de seguran�a como gerenciamento de mudan�as, identifica��o de vulnerabilidades e relat�rio/resposta de incidentes de seguran�a da informa��o atrav�s de um processo de notifica��o, formata��o e estrutura��o claramente definido. Informa��es adicionais Em caso de terceiriza��o, a organiza��o precisa estar ciente de que a responsabilidade final pela informa��o processada por um parceiro de terceiriza��o permanece com a organiza��o. ',
  ),
  46 => 
  array (
    'fkContext' => '1846',
    'fkSectionBestPractice' => '1618',
    'nControlType' => '0',
    'sName' => '10.2.3 Gerenciamento de mudan�as para servi�os terceirizados',
    'tDescription' => 'Conv�m que mudan�as no provisionamento dos servi�os, incluindo manuten��o e melhoria da Pol�tica de Seguran�a da informa��o, procedimentos e controles existentes, sejam gerenciadas levando-se em conta a criticidade dos sistemas e processos de neg�cio envolvidos e a rean�lise/avalia��o de riscos. ',
    'tImplementationGuide' => 'O processo de gerenciamento de mudan�as para servi�os terceirizados precisa levar em conta:   a) mudan�a feitas pela organiza��o para a implementa��o:  1) melhorias dos servi�os correntemente oferecidos;
2) desenvolvimento de quaisquer novas aplica��es ou sistemas;
3) modifica��es ou atualiza��es das pol�ticas e procedimentos da organiza��o;
4) novos controles para resolver os incidentes de seguran�a da informa��o e para melhorar a seguran�a;
b) mudan�as em servi�os de terceiros para implementa��o de: 1) mudan�as e melhorias em redes;
2) uso de novas tecnologias;
3) ado��o de novos produtos ou novas vers�o;
4) novas ferramentas e ambientes de desenvolvimento;
5) mudan�as de localiza��o f�sica dos recursos de servi�os;
6) mudan�as de fornecedores. ',
  ),
  47 => 
  array (
    'fkContext' => '1847',
    'fkSectionBestPractice' => '1619',
    'nControlType' => '0',
    'sName' => '10.3.1 Gest�o de capacidade',
    'tDescription' => 'Conv�m que utiliza��o dos recursos seja monitorada e sincronizada e as proje��es feitas para necessidades de capacidade futura, para garantir o desempenho requerido do sistema. ',
    'tImplementationGuide' => 'Conv�m que requisitos de capacidade sejam identificados para cada atividade nova ou em andamento. Conv�m que a sincroniza��o e monitoramento dos sistemas sejam aplicados para garantir e, quando necess�rio, melhorar a disponibilidade e efici�ncia dos sistemas. Conv�m que proje��es de capacidade futura levem em considera��o os requisitos de novos neg�cios e sistemas e as tend�ncias atuais e projetadas de capacidade de processamento de informa��o da organiza��o.  Aten��o particular precisa ser dada a qualquer recurso que possua um ciclo de renova��o ou custo maior, sendo responsabilidade dos gestores monitorar a utiliza��o dos recursos-chave dos sistemas. Conv�m que eles identifiquem as tend�ncias de utiliza��o, particularmente em rela��o as aplica��es do neg�cio ou � gest�o das ferramentas de sistemas de informa��o.  Conv�m que os gestores utilizem essas informa��es para identificar e evitar os potenciais gargalos e a depend�ncia em pessoas-chave que possam representar amea�as � seguran�a dos sistemas ou aos servi�os, e planejar a��o corretiva apropriada. ',
  ),
  48 => 
  array (
    'fkContext' => '1848',
    'fkSectionBestPractice' => '1619',
    'nControlType' => '0',
    'sName' => '10.3.2 Aceita��o dos sistemas',
    'tDescription' => 'Conv�m que sejam estabelecidos crit�rios de aceita��o para novos sistemas, atualiza��es e novas vers�es, e que sejam efetuados testes apropriados do(s) sistema(s) durante seu desenvolvimento e antes da sua aceita��o. ',
    'tImplementationGuide' => 'Conv�m que os gestores garantam que os requisitos e crit�rios para aceita��o de novos sistemas estejam claramente definidos, acordados, documentados e testados. Conv�m que novos sistemas de informa��o, atualiza��es e novas vers�es s� sejam migradas para produ��o ap�s a obten��o de aceita��o formal. Conv�m que os seguintes itens sejam considerados antes que a aceita��o formal seja emitida:  a) requisitos de desempenho e de capacidade computacional;
b) recupera��o de erros, procedimentos de reinicializa��o e planos de conting�ncia;
c) prepara��o e teste de procedimentos operacionais de rotina para o estabelecimento de padr�es;
d) concord�ncia sobre o conjunto de controles de seguran�a utilizados;
e) manuais eficazes;
f) requisitos de continuidade dos neg�cios - ver 14.1;
g) evid�ncia de que a instala��o do novo sistema n�o afetar� de forma adversa os sistemas existentes, particularmente nos per�odos de pico de processamento, como, por exemplo, em final de m�s;
h) evid�ncia de que tenha sido considerado o impacto do novo sistema na seguran�a da organiza��o como um todo;
i) treinamento na opera��o ou uso de novos sistemas;
j) facilidade de uso, uma vez que afeta o desempenho do usu�rio e evita falhas humanas.  Para os principais novos desenvolvimentos, conv�m que os usu�rios e as fun��es de opera��o sejam consultados em todos os est�gios de processo de desenvolvimento, de forma a garantir a efici�ncia que todos os crit�rios de aceita��o tenham sido plenamente satisfeitos. Informa��es adicionais A aceita��o pode incluir um processo formal de certifica��o e reconhecimento para garantir que os requisitos de seguran�a tenham sido devidamente endere�ados. ',
  ),
  49 => 
  array (
    'fkContext' => '1849',
    'fkSectionBestPractice' => '1620',
    'nControlType' => '0',
    'sName' => '10.4.1 Controles conta c�digos maliciosos',
    'tDescription' => 'Conv�m que sejam implantados controles de detec��o, preven��o e recupera��o para proteger contra c�digos maliciosos, assim como procedimentos para a devida conscientiza��o dos usu�rios. ',
    'tImplementationGuide' => 'Conv�m que a prote��o contra c�digos maliciosos seja baseada em softwares de detec��o de c�digos maliciosos e reparo, na conscientiza��o da seguran�a da informa��o, no controle de acesso autorizado e nos controles de gerenciamento de mudan�as. Conv�m que as seguintes diretrizes sejam consideradas:  a) estabelecer uma pol�tica formal proibindo o uso de softwares n�o autorizados - ver 15.1.2 - b) estabelecer uma pol�tica formal para prote��o contra os riscos associados com a importa��o de arquivos e softwares, seja de redes externas, ou por qualquer outro meio, indicando quais medidas preventivas devem ser adotadas;\'\' c) conduzir an�lises cr�ticas regulares dos softwares e dados dos sistemas que suportam processos cr�ticos de neg�cio;
conv�m que a presen�a de quaisquer arquivos n�o aprovados ou atualiza��o n�o autorizada seja formalmente investigada;
d) instalar e atualizar regularmente softwares de detec��o e remo��o de c�digos maliciosos para o exame de computadores e m�dias magn�ticas, de forma preventiva ou de forma rotineira;
conv�m que as verifica��es realizadas incluam:  1) verifica��o, antes do uso, da exist�ncia de c�digos maliciosos nos arquivos em m�dias �ticas ou eletr�nicas, bem como nos arquivos transmitidos atrav�s de redes;
2) verifica��o, antes do uso, da exist�ncia de software malicioso em qualquer arquivo recebido atrav�s de correio eletr�nico ou importado (download). Conv�m que essa avalia��o seja feita em diversos locais como, por exemplo, nos servidores de correio eletr�nico, nos computadores pessoais ou quando da sua entrada na rede da organiza��o;
3) verifica��o da exist�ncia de c�digos maliciosos em p�ginas web;
e) definir procedimentos de gerenciamento e respectivas responsabilidades para tratar da prote��o de c�digo malicioso nos sistemas, treinamento nesses procedimentos, reporte e recupera��o de ataques de c�digos maliciosos - ver 13.1 e 13.2;
f) preparar planos de continuidade do neg�cio adequado para a recupera��o em casos de ataques por c�digos maliciosos, incluindo todos os procedimentos necess�rios para a c�pia e a recupera��o dos dados e softwares - ver se��o 14;
g) implementar procedimentos para regularmente coletar informa��es, tais como, assinaturas de listas de discuss�o e visitas a sites informativos sobre novos c�digos maliciosos;
h) implementar procedimentos para a verifica��o de informa��o relacionada a c�digos maliciosos e garantia de que os boletins com alertas sejam precisos e informativos;
conv�m que os gestores garantam que fontes qualificadas, como, por exemplo, jornais com reputa��o id�nea, sites confi�veis ou fornecedores de software de prote��o contra c�digos maliciosos, sejam utilizadas para diferenciar boatos de not�cias reais sobre c�digos maliciosos;
conv�m que todos os usu�rios estejam cientes dos problemas decorrentes de boatos e capacitados a lidar com eles. Informa��es adicionais A utiliza��o de dois ou mais tipos de software de controle contra c�digos maliciosos de diferentes fornecedores no ambiente de processamento da informa��o pode aumentar a efic�cia na prote��o contra c�digos maliciosos.  Softwares de prote��o contra c�digo malicioso podem ser instalados para prover atualiza��es autom�ticas dos arquivos e mecanismos de busca, para garantir que a prote��o esteja atualizada. Adicionalmente, estes softwares podem ser instalados em todas as esta��es de trabalho para a realiza��o de verifica��es autom�ticas.  Conv�m que seja tomado cuidado quanto a poss�vel introdu��o de c�digos maliciosos durante manuten��es e quando est�o sendo realizados procedimentos de emerg�ncia. Tais procedimentos podem ignorar controles normais de prote��o contra c�digos maliciosos. ',
  ),
  50 => 
  array (
    'fkContext' => '1850',
    'fkSectionBestPractice' => '1620',
    'nControlType' => '0',
    'sName' => '10.4.2 Controle contra c�digos m�veis',
    'tDescription' => 'Onde o uso de c�digos m�veis � autorizado, conv�m que a configura��o garanta que o c�digo m�vel autorizado opere de acordo com uma Pol�tica de Seguran�a da informa��o claramente definida e c�digos m�veis n�o autorizados tenham sua execu��o impedida. ',
    'tImplementationGuide' => 'Conv�m que as seguintes a��es sejam consideradas para proteger contra a��es n�o autorizadas realizadas por c�digos m�veis:  a) executar c�digos m�veis em ambientes isolados logicamente;
b) bloquear qualquer tipo de uso de c�digo m�vel;
c) bloquear o recebimento de c�digos m�veis;
d) ativar medidas t�cnicas dispon�veis nos sistemas espec�ficos para garantir que o c�digo m�vel esteja sendo administrado;
e) controlar os recursos dispon�veis para acesso ao c�digo m�vel;
f) estabelecer controles criptogr�ficos de autentica��o exclusiva do c�digo m�vel. Informa��es adicionais C�digo m�vel � um c�digo transferido de um computador a outro executando automaticamente e realizando fun��es espec�ficas com pequena ou nenhuma intera��o por parte do usu�rio. C�digos m�veis s�o associados a uma variedade de servi�os middleware.  Al�m de garantir que os c�digos m�veis n�o carreguem c�digos maliciosos, manter o controle deles � essencial na preven��o contra o uso n�o autorizado ou interrup��o de sistemas, redes ou aplicativos, e na preven��o contra viola��es de seguran�a da informa��o. ',
  ),
  51 => 
  array (
    'fkContext' => '1851',
    'fkSectionBestPractice' => '1621',
    'nControlType' => '0',
    'sName' => '10.5.1 C�pias de seguran�a das informa��es',
    'tDescription' => 'Conv�m que as c�pias de seguran�a das informa��es e dos softwares sejam efetuadas e testadas regularmente conforme a pol�tica de gera��o de c�pias de seguran�a definida. ',
    'tImplementationGuide' => 'Conv�m que recursos adequados para a gera��o de c�pias de seguran�a disponibilizados para garantir que toda informa��o e software essenciais possam ser recuperados ap�s um desastre ou a falha de uma m�dia.  Conv�m que os seguintes itens para a gera��o das c�pias de seguran�a sejam consideradas:  a) defini��o do n�vel necess�rio das c�pias de seguran�a das informa��es;
b) produ��o de registros completos e exatos das c�pias de seguran�a e documenta��o apropriadas sobre procedimentos de restaura��o da informa��o;
c) a extens�o (por exemplo, completa ou diferencial) e a freq��ncia da gera��o das c�pias de seguran�a reflita pos requisitos de neg�cio da organiza��o, al�m dos requisitos de seguran�a da informa��o envolvidos e a criticidade da informa��o para a continuidade da opera��o da organiza��o;
d) as c�pias de seguran�a sejam armazenadas em uma localidade remota, a uma dist�ncia suficiente para escapar dos danos de um desastre ocorrido no local principal;
e) deve ser dado um n�vel apropriado de prote��o f�sica e ambiental das informa��es das c�pias de seguran�a (ver se��o 9), consistente com as normas aplicadas na instala��o principal;
os controles aplicados �s m�dias na instala��o principal sejam usados no local das c�pias de seguran�a;
f) as m�dias de c�pias de seguran�a sejam testadas regularmente para garantir que elas s�o suficientemente confi�veis para o uso de emerg�ncia, quando necess�rio;
g) os procedimentos de recupera��o sejam verificados e testados regularmente, de forma a garantir que estes s�o efetivos e que podem ser conclu�dos dentro dos prazos definidos nos procedimentos operacionais de recupera��o;
h) em situa��es onde a confidencialidade � importante, c�pias de seguran�a sejam protegidas atrav�s de encripita��o.  Conv�m que as c�pias de seguran�a de sistemas espec�ficos sejam testadas regularmente para garantir que elas est�o aderentes aos requisitos definidos nos planos de continuidade do neg�cio - ver se��o 14. Para sistemas cr�ticos, conv�m que os mecanismos de gera��o de c�pias de seguran�a abranjam todos os sistemas de informa��o, aplica��es e dados necess�rios para a completa recupera��o do sistema em um evento de desastre.  Conv�m que o per�odo de reten��o para informa��es essenciais ao neg�cio e tamb�m qualquer requisito para que c�pias de arquivos sejam permanentemente retidas seja determinado - ver 15.1.3.  Informa��es adicionais Os mecanismos de c�pias de seguran�a podem ser automatizados para facilitar os processos de gera��o e recupera��o das c�pias de seguran�a. Conv�m que tais solu��es automatizadas sejam suficientemente testadas antes da implementa��o e verificadas em intervalos regulares. ',
  ),
  52 => 
  array (
    'fkContext' => '1852',
    'fkSectionBestPractice' => '1622',
    'nControlType' => '0',
    'sName' => '10.6.1 Controles de redes',
    'tDescription' => 'Conv�m que as redes sejam adequadamente gerenciadas e controladas, de forma a proteg�-las contra amea�as e manter a seguran�a de sistemas e aplica��es que utilizam estas redes, incluindo a informa��o em tr�nsito. ',
    'tImplementationGuide' => 'Conv�m que gestores de rede implementem controles para garantir a seguran�a da informa��o nestas redes e a prote��o dos servi�os a elas conectadas, de acesso n�o autorizado. Em particular, conv�m que os seguintes itens sejam considerados: a) a responsabilidade operacional pelas redes seja separada da opera��o dos recursos computacionais onde for apropriado - ver 10.1.3;
b) as responsabilidades e procedimentos sobre gerenciamento de equipamentos remotos, incluindo equipamentos em �reas de usu�rios, sejam estabelecidos;
c) os controles especiais sejam estabelecidos para prote��o da confidencialidade e integridade dos dados trafegando sobre redes p�blicas ou sobre redes sem fio (wireless) e para proteger os sistemas e aplica��es a elas conectadas - ver 11.4 e 12.3;
controles especiais podem tamb�m ser requeridos para manter a disponibilidade dos servi�os de rede e computadores conectados;
d) os mecanismos apropriados de registro e monitora��o sejam aplicados para habilitar a grava��o das a��es relevantes de seguran�a;
e) as atividades de gerenciamento sejam coordenadas para otimizar os servi�os para a organiza��o e assegurar que os controles estejam aplicados de forma consistente sobre toda a infra-estrutura de processamento da informa��o. Informa��es adicionais Informa��es adicionais sobre seguran�a de redes podem ser encontradas na ISSO/IEC 18028, Information technology - Security techiniques - IT network security. ',
  ),
  53 => 
  array (
    'fkContext' => '1853',
    'fkSectionBestPractice' => '1622',
    'nControlType' => '0',
    'sName' => '10.6.2 Seguran�a dos servi�os de rede',
    'tDescription' => 'Conv�m que as caracter�sticas de seguran�a, neveis de servi�o e requisitos de gerenciamento dos servi�os de rede sejam identificados e inclu�dos em qualquer acordo de servi�os de rede, tanto para servi�os de rede providos internamente ou terceirizados. ',
    'tImplementationGuide' => 'Conv�m que a capacidade do provedor dos servi�os de rede gerenciar os servi�os acordados de maneira segura seja determinada e monitorada regularmente, bem como que o direito de audit�-los seja acordado.  Conv�m que as defini��es de seguran�a necess�rias para servi�os espec�ficos, como caracter�sticas de seguran�a, n�veis de servi�o e requisitos de gerenciamento, sejam identificadas. Conv�m que a organiza��o assegure que os provedores dos servi�os de rede implementam estas medidas. Informa��es adicionais Servi�os de rede incluem o fornecimento de conex�es, servi�os de rede privados, redes de valor agregado e solu��es de seguran�a de rede gerenciadas como firewalls e sistemas de detec��o de intrusos. Estes servi�os podem abranger desde o simples fornecimento de banda de rede n�o gerenciada at� complexas ofertas de solu��es de valor agregado.  Funcionalidades de seguran�a de servi�os de rede podem ser:  a) tecnologias aplicadas para seguran�a se servi�os de redes como autentica��o, encripita��o e controles de conex�es de rede;
b) par�metro t�cnico requerido para uma conex�o segura com os servi�os de rede de acordo com a seguran�a e regras de conex�o de redes;
c) procedimentos para o uso de servi�os de rede para restringir o acesso a servi�os de rede ou aplica��es, onde for necess�rio. ',
  ),
  54 => 
  array (
    'fkContext' => '1854',
    'fkSectionBestPractice' => '1623',
    'nControlType' => '0',
    'sName' => '10.7.1 Gerenciamento de m�dias remov�veis',
    'tDescription' => 'Conv�m que existam procedimentos implementados para o gerenciamento de m�dias remov�veis. ',
    'tImplementationGuide' => 'Conv�m  que as seguintes diretrizes para o gerenciamento de m�dias remov�veis sejam consideradas.  a) quando n�o for mais necess�rio, o conte�do de qualquer meio magn�tico reutiliz�vel seja destru�do, caso venha a ser retirado da organiza��o;
b) quando necess�rio e pr�tico, seja requerida a autoriza��o para remo��o de qualquer m�dia da organiza��o e mantido o registro dessa remo��o como trilha de auditoria;
c) toda m�dia seja guardada de forma segura em um ambiente protegido, de acordo com as especifica��es do fabricante;
d) informa��es armazenadas em m�dias que precisam estar dispon�veis por muito tempo (em conformidade com as especifica��es dos fabricantes) sejam tamb�m armazenadas em outro local para evitar perda de informa��es devido � deteriora��o das m�dias;
e) as m�dias remov�veis sejam registradas para limitar a oportunidade de perda de dados;
f) as unidades de m�dias remov�veis estejam habilitadas somente se houver uma necessidade do neg�cio;
Conv�m que todos os procedimentos e os n�veis de autoriza��o sejam explicitamente documentados. Informa��es adicionais M�dia remov�vel inclui fitas, discos, flash disks, discos remov�veis, CD, DVD e m�dia impressa. ',
  ),
  55 => 
  array (
    'fkContext' => '1855',
    'fkSectionBestPractice' => '1623',
    'nControlType' => '0',
    'sName' => '10.7.2 Descarte de m�dias',
    'tDescription' => 'Conv�m que as m�dias sejam descartadas de forma segura e protegida quando n�o forem mais necess�rias, por meio de procedimentos formais. ',
    'tImplementationGuide' => 'Conv�m que procedimentos formais para o descarte seguro das m�dias sejam definidos para minimizar o risco de vazamento de informa��es sens�veis para pessoas n�o autorizadas. Conv�m que os procedimentos para o descarte seguro das m�dias, contendo informa��es sens�veis, sejam relativos � sensibilidade das informa��es. Conv�m que os seguintes itens sejam considerados:  a) m�dias contendo informa��es sens�veis sejam guardadas e destru�das de forma segura e protegida, como, por exemplo, atrav�s de incinera��o ou tritura��o, ou da remo��o dos dados para uso por uma outra aplica��o dentro da organiza��o;
b) procedimentos sejam implementados para identificar os itens que requerem descarte seguro;
c) pode ser mais f�cil implementar a coleta e descarte seguro de todas as m�dias a serem inutilizadas que tentar separar apenas aquelas contendo informa��es sens�veis;
d) muitas organiza��es oferecem servi�os de coleta e descarte de papel, de equipamentos e de m�dias magn�ticas;
conv�m que se tenha o cuidado na sele��o de um fornecedor com experi�ncia e controles adequados;
e) descarte de itens sens�veis seja registrado em controles sempre que poss�vel para se manter uma trilha de auditoria.  Quando da acumula��o de m�dias para descarte, conv�m que se leve em considera��o o efeito proveniente do ac�mulo, o que pode fazer com que uma grande quantidade de informa��o n�o sens�vel torne-se sens�vel. Informa��es adicionais Informa��es sens�veis podem ser divulgadas atrav�s do descarte negligente das m�dias - ver 9.2.6 para informa��es de descarte de equipamentos.',
  ),
  56 => 
  array (
    'fkContext' => '1856',
    'fkSectionBestPractice' => '1623',
    'nControlType' => '0',
    'sName' => '10.7.3 Procedimentos para tratamento de informa��o',
    'tDescription' => 'Controle  Conv�m que sejam estabelecidos procedimentos para o tratamento e o armazenamento de informa��es , para proteger tais informa��es contra a divulga��o n�o autorizada ou uso indevido. ',
    'tImplementationGuide' => 'Conv�m que procedimentos sejam estabelecidos para o tratamento, processamento, armazenamento e transmiss�o da informa��o, de acordo com a sua classifica��o - ver 7.2. Conv�m que os seguintes itens sejam considerados:  a) tratamento e identifica��o de todos os meios magn�ticos indicando o n�vel de classifica��o;
b) restri��es de acesso para prevenir o acesso de pessoas n�o autorizadas;
c) manuten��o de um registro formal dos destinat�rios de dados autorizados;
d) garantia de que a entrada de dados seja completa , de que o processamento esteja devidamente conclu�do e de que a valida��o das sa�das seja aplicada;
e) prote��o dos dados preparados para expedi��o ou impress�o de forma consistente com a sua criticidade;
f) armazenamento das m�dias em conformidade com as especifica��es dos fabricantes;
g) manuten��o da distribui��o de dados no menor n�vel poss�vel;
h) identifica��o eficaz de todas as c�pias das m�dias, para chamar a aten��o dos destinat�rios autorizados;
i) an�lise cr�tica das listas de distribui��o e das listas de destinat�rios autorizados em intervalos regulares. Informa��es adicionais Estes procedimentos s�o aplicados para informa��es em documentos, sistemas de computadores, redes de computadores, computa��o m�vel, correio eletr�nico, correio de voz, comunica��o de voz em geral, multim�dia, servi�os postais, uso de m�quinas de fax e qualquer outro item sens�vel, como, por exemplo, cheques em branco e faturas. ',
  ),
  57 => 
  array (
    'fkContext' => '1857',
    'fkSectionBestPractice' => '1623',
    'nControlType' => '0',
    'sName' => '10.7.4 Seguran�a da documenta��o dos sistemas',
    'tDescription' => 'Conv�m que a documenta��o dos sistemas seja protegida contra acessos n�o autorizados ',
    'tImplementationGuide' => 'Para proteger a documenta��o dos sistemas, conv�m que os seguintes itens sejam considerados:
a) a documenta��o dos sistemas seja guardada de forma segura;
b) a rela��o de pessoas com acesso autorizado � documenta��o de sistemas seja a menor poss�vel e autorizada pelo propriet�rio do sistema;
c) a documenta��o de sistemas seja mantida em uma rede p�blica, ou fornecida atrav�s de uma rede p�blica , seja protegida de forma apropriada. Informa��es adicionais A documenta��o dos sistemas pode conter uma s�rie de informa��es sens�veis, como, por exemplo, descri��es de processos da aplica��o, procedimentos, estruturas de dados e processos de autoriza��o.',
  ),
  58 => 
  array (
    'fkContext' => '1858',
    'fkSectionBestPractice' => '1624',
    'nControlType' => '0',
    'sName' => '10.8.1 Pol�ticas e procedimentos para troca de informa��es',
    'tDescription' => 'Conv�m que pol�ticas, procedimentos e controles sejam estabelecidos e formalizados para proteger a troca de informa��es em todos os tipos de recursos de comunica��o ',
    'tImplementationGuide' => 'Conv�m que os procedimentos e controles estabelecidos para a troca de informa��es em recursos eletr�nicos de comunica��o considerem os t�picos a seguir.
a) procedimentos formulados para proteger a informa��o em tr�nsito contra intercepta��o, c�pia, modifica��o, desvio e destrui��o;
b) procedimentos para detec��o e prote��o contra c�digo malicioso que pode ser transmitido atrav�s do uso de recursos eletr�nicos de comunica��o - ver 10.4.1;
c) procedimentos para prote��o de informa��es eletr�nicas sens�veis que sejam transmitidas na forma de anexos;
d) pol�tica ou diretrizes que especifiquem o uso aceit�vel dos recursos eletr�nicos de comunica��o - ver 7.1.3;
e) procedimentos para o uso de comunica��o sem fio (wireless), levando em conta os riscos particulares envolvidos;
f) as responsabilidades de funcion�rios, fornecedores e quaisquer outros usu�rios n�o devem comprometer a organiza��o atrav�s de, por exemplo, difama��o, ass�dio, falsa identidade, retransmiss�o de \'\'correntes\'\', compras n�o autorizadas etc.;
g) uso de t�cnicas de criptografia para, por exemplo, proteger a confidencialidade, a integridade e a autenticidade das informa��es;
h) diretrizes de reten��o e descarte para toda a correspond�ncia de neg�cios, incluindo mensagens, de acordo com regulamenta��es e legisla��o locais e nacionais relevantes;
i) n�o deixar informa��es cr�ticas ou sens�veis em equipamentos de impress�o, tais como copiadoras, impressoras e aparelhos de fax, de tal forma que pessoas n�o autorizadas tenham acesso a elas;
j) controles e restri��es associados � retransmiss�o em recursos de comunica��o como, por exemplo, a retransmiss�o autom�tica de correios eletr�nicos para endere�os externos;
k) lembrar �s pessoas que elas devem tomar precau��es adequadas como, por exemplo, n�o revelar informa��es sens�veis, para evitar que sejam executadas ou interceptadas durante uma liga��o telef�nica por:  1) pessoas em sua vizinhan�a, especialmente quando estiver usando telefone celular;
2) grampo telef�nico e outras formas de escuta clandestina atrav�s do acesso f�sico ao aparelho telef�nico ou � linha, ou, ainda, pelo uso de rastreadores;
3) pessoas ao lado do interlocutor;
l) n�o deixar mensagens contendo informa��es sens�veis em secret�rias eletr�nicas uma vez que as mensagens podem ser reproduzidas por pessoas n�o autorizadas, gravadas em sistemas p�blicos ou gravadas indevidamente por erro de discagem;
m) lembrar as pessoas sobre os problemas do uso de aparelhos de fax, tais como:  1) acesso n�o autorizado a dispositivos para recupera��o de mensagens;
2) programa��o de aparelhos, deliberada ou acidental, para enviar mensagens para n�meros espec�ficos determinados;
3) envio de documentos e mensagens para n�mero errado, seja por falha na discagem ou uso de n�mero armazenado errado;
n) lembrar as pessoas pra que evitem o armazenamento de dados pessoais, como endere�os de correios eletr�nicos ou informa��es adicionais particulares, em qualquer software, impedindo que sejam capturados para uso n�o autorizado;
o) lembrar as pessoas sobre a exist�ncia de aparelhos de fax e copiadoras que tem dispositivos de armazenamento tempor�rio de p�ginas para o caso de falha no papel ou na transmiss�o, as quais ser�o impressas ap�s a corre��o da falha.  Adicionalmente, conv�m que as pessoas sejam lembradas de que n�o devem manter conversas confidenciais em locais p�blicos, escrit�rios abertos ou locais de reuni�o que n�o disponham de paredes � prova de som.  Conv�m que recursos utilizados para a troca de informa��es estejam de acordo com os requisitos legais pertinentes - ver se��o 15. Informa��es adicionais A troca de informa��es pode ocorrer atrav�s do uso de v�rios tipos diferentes de recursos de comunica��o, incluindo correios eletr�nico, voz, fax  v�deo.  A troca de softwares pode ocorrer de diferentes formas, incluindo a baixa (downloads) da internet ou a aquisi��o junto a fornecedores que vendem produtos em s�rie.  Conv�m que sejam consideradas as poss�veis implica��es nos neg�cios, nos aspectos legais e na seguran�a, relacionadas com a troca eletr�nica de dados, com o com�rcio eletr�nico, comunica��o eletr�nica e com os requisitos para controles.  As informa��es podem ser comprometidas devido � falta de conscientiza��o, de pol�ticas ou de procedimentos no uso de recursos de troca de informa��es, como, por exemplo, a escuta de conversas ao telefone celular em locais p�blicos, erro de endere�amento de mensagens de correio eletr�nicas, acesso n�o autorizado a sistemas de correio de voz ou o envio acidental de faxes para aparelhos errados.  As opera��es do neg�cio podem ser prejudicadas e as informa��es podem ser comprometidas se os recursos de comunica��o falharem, forem sobrecarregados ou interrompidos - ver 10.3 e se��o 14. As informa��es podem ser comprometidas se acessadas por usu�rios n�o autorizados - ver se��o 11',
  ),
  59 => 
  array (
    'fkContext' => '1859',
    'fkSectionBestPractice' => '1624',
    'nControlType' => '0',
    'sName' => '10.8.2 Acordos para trocas de informa��es',
    'tDescription' => 'Conv�m que sejam estabelecidos acordos para a troca de informa��es e softwares entre a organiza��o e entidades externas. ',
    'tImplementationGuide' => 'Conv�m que os acordos de troca de informa��es considerem as seguintes condi��es de seguran�a da informa��o:  a) responsabilidades do gestor pelo controle e notifica��o de transmiss�es, expedi��es e recep��es;
b) procedimentos para notificar o emissor da transmiss�o, expedi��o e recep��o;
c) procedimentos para assegurar a rastreabilidade dos e o n�o-rep�dio;
d) padr�es t�cnicos m�nimos para embalagem e transmiss�o;
e) acordos para procedimentos de cust�dia;
f) normas para identifica��o de portadores;
g) responsabilidades e obriga��es na ocorr�ncia de incidentes de seguran�a da informa��o, como perda de dados;
h) utiliza��o de um sistema acordado de identifica��o para informa��es cr�ticas e sens�veis, garantindo que o significado dos r�tulos seja imediatamente entendido e que a informa��o esteja devidamente protegida;
i) propriedade e responsabilidades sobre a prote��o dos dados, direitos de propriedade, conformidade com as licen�as dos softwares e considera��es afins - ver 15.1.2 e 15.1.4;
j) normas t�cnicas para a grava��o e leitura de informa��es e softwares;
k) quaisquer controles especiais que possam ser necess�rios para prote��o de itens sens�veis, tais como chaves criptogr�ficas - ver 12.3.  Conv�m que pol�ticas, procedimentos e normas para proteger as informa��es e as m�dias em tr�nsito - ver tamb�m 10.8.3 - sejam estabelecidos e mantidos, al�m de serem referenciados nos mencionados acordos para a troca de informa��es.  Conv�m que os aspectos de seguran�a contidos nos acordos reflitam a sensibilidade das informa��es envolvidas no neg�cio. Informa��es adicionais Os acordos podem ser eletr�nicos ou manuais, e podem estar no formato de contratos formais ou condi��es de contrata��o. Para informa��es sens�veis, conv�m que os mecanismos espec�ficos usados para a troca de tais informa��es sejam consistentes com todas as organiza��es e tipos de acordos. ',
  ),
  60 => 
  array (
    'fkContext' => '1860',
    'fkSectionBestPractice' => '1624',
    'nControlType' => '0',
    'sName' => '10.8.3 M�dias em tr�nsito',
    'tDescription' => 'Conv�m que m�dias contendo informa��es sejam protegidas contra acesso n�o autorizado, uso impr�prio ou altera��o indevida durante o transporte externo aos limites f�sicos da organiza��o. ',
    'tImplementationGuide' => 'Conv�m que as seguintes recomenda��es sejam consideradas, para proteger as m�dias que s�o transportadas entre localidades:  a) meio de transporte ou o servi�o de mensageiros confi�veis;
b) seja definida uma rela��o de portadores autorizados em concord�ncia com o gestor;
c) sejam estabelecidos procedimentos para a verifica��o da identifica��o dos transportadores;
d) a embalagem seja suficiente para proteger o conte�do contra qualquer dano f�sico, como os que podem ocorrer durante o transporte, e que seja feita de acordo com as especifica��es dos fabricantes (como no caso de softwares), por exemplo, protegendo contra fatores ambientais que possam reduzir a possibilidade de restaura��o dos dados como a exposi��o ao calor, umidade ou campos eletromagn�ticos;
e) sejam adotados controles, onde necess�rio, para proteger informa��es sens�veis contra divulga��o n�o autorizada ou modifica��o;
como exemplo, pode-se incluir o seguinte:  1) utiliza��o de recipientes lacrados;
2) entrega em m�os;
3) lacre expl�cito de pacotes - que revele qualquer tentativa de acesso;
4) em casos excepcionais, divis�o do conte�do em mais de uma remessa e expedi��o por rotas distintas. Informa��es adicionais As informa��es podem estar vulner�veis a acesso n�o autorizado, uso impr�prio ou altera��o indevida durante o transporte f�sico, por exemplo quando a m�dia � enviada por via postal ou sistema de mensageiros. ',
  ),
  61 => 
  array (
    'fkContext' => '1861',
    'fkSectionBestPractice' => '1624',
    'nControlType' => '0',
    'sName' => '10.8.4 Mensagens eletr�nicas',
    'tDescription' => 'Conv�m que as informa��es que trafegam em mensagens eletr�nicas sejam adequadamente protegidas. ',
    'tImplementationGuide' => 'Conv�m que as considera��es de seguran�a da informa��o sobre as mensagens eletr�nicas incluam o seguinte:  a) prote��o das mensagens contra acesso n�o autorizado, modifica��o ou nega��o de servi�o;
b) assegurar que o endere�amento e o transporte da mensagem estejam corretos;
c) confiabilidade e disponibilidade geral do servi�o;
d) aspectos legais, como, por exemplo, requisitos e assinaturas eletr�nicas;
e) aprova��o pr�via para o uso de servi�os p�blicos externos, tais como sistemas de mensagens instant�neas e compartilhamento de arquivos;
f) n�veis mais altos de autentica��o para controlar o acesso a partir de redes publicas. Informa��es adicionais Mensagens eletr�nicas como correio eletr�nico, Eletronic Data Interchange (EDI) e sistemas de mensagens instant�neas cumprem um papel cada vez mais importante nas comunica��es do neg�cio. A mensagem eletr�nica tem riscos diferentes, se comparada com a comunica��o em documentos impressos. ',
  ),
  62 => 
  array (
    'fkContext' => '1862',
    'fkSectionBestPractice' => '1624',
    'nControlType' => '0',
    'sName' => '10.8.5 Sistemas de informa��es do neg�cio',
    'tDescription' => 'Conv�m que pol�ticas e procedimentos sejam desenvolvidos e implementados para proteger as informa��es associadas com a interconex�o dos sistemas de informa��es do neg�cio. ',
    'tImplementationGuide' => 'Conv�m que as considera��es sobre seguran�a da informa��o e implica��es no neg�cio das interconex�es de sistemas incluam o seguinte:  a) vulnerabilidades conhecidas nos sistemas administrativos e cont�beis onde as informa��es s�o compartilhadas com diferentes �reas da organiza��o;
b) vulnerabilidades da informa��o nos sistemas de comunica��o do neg�cio, como, por exemplo, grava��o de chamadas telef�nicas ou teleconfer�ncias, confidencialidade das chamadas, armazenamento de faxes, abertura de correio e distribui��o de correspond�ncia;
c) pol�tica e controles apropriados para gerenciar o compartilhamento de informa��es;
d) exclus�o de categorias de informa��es sens�veis e documentos confidenciais, caso o sistema n�o forne�a o n�vel de prote��o apropriado - ver 7.2;
e) restri��o do acesso a informa��es de trabalho relacionado com indiv�duos espec�ficos, como, por exemplo, um grupo que trabalha com projetos sens�veis;
f) categorias de pessoas, fornecedores ou parceiros nos neg�cios autorizados a usar os sistemas e as localidades a partir das quais pode-se obter acesso ao sistema - ver 6.2 e 6.3;
g) restri��o aos recursos selecionados para categorias espec�ficas de usu�rios;
h) identifica��o da condi��o do usu�rio, como, por exemplo, funcion�rios da organiza��o ou fornecedores na lista de cat�logo de usu�rios em benef�cio de outros usu�rios;
i) reten��o e c�pias de seguran�a das informa��es mantidas no sistema - ver 10.5.1;
j) requisitos e procedimentos para recupera��o e conting�ncia - ver se��o 14. Informa��es adicionais Os sistemas de informa��o de escrit�rio representam uma oportunidade de r�pida dissemina��o e compartilhamento de informa��es do neg�cio atrav�s do uso de uma combina��o de: documentos, computadores, dispositivos m�veis, comunica��o sem fio, correio, correio de voz, comunica��o de voz em geral, servi�os postais e aparelhos de fax. ',
  ),
  63 => 
  array (
    'fkContext' => '1863',
    'fkSectionBestPractice' => '1625',
    'nControlType' => '0',
    'sName' => '10.9.1 Com�rcio eletr�nico',
    'tDescription' => 'Conv�m que as informa��es envolvidas em com�rcio eletr�nico transitando sobre redes p�blicas sejam protegidas de atividades fraudulentas, disputas contratuais e divulga��o e modifica��es n�o autorizadas. ',
    'tImplementationGuide' => 'Conv�m que as considera��es de seguran�a da informa��o para com�rcio eletr�nico incluam os seguintes itens:  a) n�vel de confian�a que cada parte requer na suposta identidade de outros, como, por exemplo, por meio de mecanismos de autentica��o;
b) processos de autoriza��o com que pode determinar pre�os, emitir ou assinar documentos-chave de negocia��o;
c) garantia de parceiros comerciais est�o completamente informados de suas autoriza��es;
d) determinar e atender requisitos de confidencialidade, integridade, evid�ncias de emiss�o e recebimento de documentos-chave, e a n�o repudia��o de contratos, como, por exemplo, os associados aos processos de licita��es e contrata��es;
e) n�vel de confian�a requerido na integridade das listas de pre�os anunciadas;
f) a confidencialidade de quaisquer dados ou informa��es sens�veis;
g) a confidencialidade e integridade de quaisquer transa��es de pedidos, informa��es de pagamento, detalhes de endere�o de entrega e confirma��es de recebimento;
h) grau de investiga��o apropriado para a verifica��o de informa��es de pagamento fornecidas por um cliente;
i) sele��o das formas mais apropriadas de pagamento para prote��o contra fraudes;
j) n�vel de prote��o requerida para manter a confidencialidade e integridade das informa��es de pedidos;
k) preven��o contra perda ou duplica��o de informa��o de transa��o;
l) responsabilidades associadas com quaisquer transa��es fraudulentas;
m) requisitos de seguro.  Muitas das considera��es acima podem ser endere�adas pela aplica��o de controles criptogr�ficos (ver 12.3), levando-se em conta a conformidade com os requisitos legais - ver 15.1, especialmente 15.1.6 para legisla��o sobre criptografia.  Conv�m que os procedimentos para com�rcio eletr�nico entre parceiros comerciais sejam apoiados por um acordo formal que comprometa ambas as partes aos termos da transa��o, incluindo detalhes de autoriza��o - ver b) acima. Outros acordos com fornecedores de servi�os de informa��o e redes de valor agregado podem ser necess�rios.  Conv�m que sistemas comerciais p�blicos divulguem seus termos comerciais a sues clientes.  Conv�m que sejam consideradas a capacidade de resist�ncia dos servidores utilizados para com�rcio eletr�nico contra ataques e as implica��es de seguran�a de qualquer interconex�o que seja necess�ria na rede de telecomunica��es para a sua implementa��o - ver 11.4.6. Informa��es adicionais Com�rcio eletr�nico � vulner�vel a in�meras amea�as de rede que podem resultar em atividades fraudulentas, disputas contratuais, e divulga��o ou modifica��o de informa��o.  Com�rcio eletr�nico pode utilizar m�todos seguros de autentica��o, como, por exemplo, criptografia de chave p�blica e assinaturas digitais - ver 12.3 - para reduzir os riscos. Ainda, terceiros confi�veis podem ser utilizados onde tais servi�os forem necess�rios. ',
  ),
  64 => 
  array (
    'fkContext' => '1864',
    'fkSectionBestPractice' => '1625',
    'nControlType' => '0',
    'sName' => '10.9.2 Transa��es on-line',
    'tDescription' => 'Conv�m que informa��es envolvidas em transa��es on-line sejam protegidas para prevenir transmiss�es incompletas, erros de roteamento, altera��es n�o autorizadas de mensagens, divulga��o n�o autorizada, duplica��o ou reapresenta��o de mensagens n�o autorizada. ',
    'tImplementationGuide' => 'Conv�m que as considera��es de seguran�a para transa��es on-line incluam os seguintes itens: a) uso de assinaturas eletr�nicas para cada uma das partes envolvidas na transa��o;
b) todos os aspectos da transa��o, ou seja, garantindo que:  1) credenciais de usu�rio para todas as partes s�o v�lidas e verificadas;
2) a transa��o permane�a confidencial;
e  3) a privacidade de todas as partes envolvidas seja mantida;
c) caminho de comunica��o entre todas as partes envolvidas � criptografado;
d) protocolos usados para comunica��es entre todas as partes envolvidas � seguro;
e) garantir que o armazenamento dos detalhes da transa��o est� localizado fora de qualquer ambiente publicamente acess�vel, como por exemplo, numa plataforma de armazenamento na intranet da organiza��o, e n�o retida e exposta em um dispositivo de armazenamento diretamente acess�vel pala internet;
f) onde uma autoridade confi�vel � utilizada (como, por exemplo, para prop�sitos de emiss�o e manuten��o de assinaturas e/ou certificados digitais), seguran�a � integrada a todo o processo de gerenciamento de certificados e assinaturas. Informa��es adicionais A extens�o dos controles adotados precisar� ser proporcional ao n�vel de risco associado a cada forma de transa��o on-line.  Transa��es podem precisar estar de acordo com leis, regras e regulamenta��es na jurisdi��o em que a transa��o � gerada, processada, completa ou armazenada.  Existem muitas formas de transa��es que podem ser executadas de forma on-line, como, por exemplo, contratuais, financeiras, etc. ',
  ),
  65 => 
  array (
    'fkContext' => '1865',
    'fkSectionBestPractice' => '1625',
    'nControlType' => '0',
    'sName' => '10.9.3 Informa��es publicamente dispon�veis',
    'tDescription' => 'Conv�m que a integridade das informa��es disponibilizadas em sistemas publicamente acess�veis seja protegida para prevenir modifica��es. ',
    'tImplementationGuide' => 'Conv�m que aplica��es, dados e informa��es adicionais que requeiram um alto n�vel de integridade e que sejam disponibilizados em sistemas publicamente acess�veis sejam protegidos por mecanismos apropriados, como, por exemplo, assinaturas digitais - ver 12.3. Conv�m que os sistemas acess�veis publicamente sejam testados contra fragilidades e falhas antes da informa��o estar dispon�vel.  Conv�m que haja um processo formal de aprova��o antes que uma informa��o seja publicada. Adicionalmente, conv�m que todo dado de entrada fornecido por fontes externas ao sistema seja verificado e aprovado.  Conv�m que sistemas de publica��o eletr�nica, especialmente os que permitem realimenta��o e entrada direta de informa��o, sejam cuidadosamente controlados, de forma que:  a) informa��es sejam obtidas em conformidade com qualquer legisla��o de prote��o de dados - ver 15.1.4;
b) informa��es que sejam entradas e processadas por um sistema de publica��o sejam processadas completa e corretamente em um tempo adequado;
c) informa��es sens�veis sejam protegidas durante a coleta, processamento e armazenamento;
d) acesso a sistemas de publica��o n�o permita acesso n�o intencional a redes �s quais tal sistema est� conectado. Informa��es adicionais  Informa��es em sistemas publicamente dispon�veis, como, por exemplo, informa��es em servidores web acess�veis por meio da internet, podem necessitar estar de acordo com leis, regras e regulamenta��es na jurisdi��o em que o sistema est� localizado, onde a transa��o est� ocorrendo ou onde o propriet�rio reside. Modifica��es n�o autorizadas de informa��es publicadas podem trazer preju�zos � reputa��o da organiza��o que a publica. ',
  ),
  66 => 
  array (
    'fkContext' => '1866',
    'fkSectionBestPractice' => '1626',
    'nControlType' => '0',
    'sName' => '10.10.1 Registros de Auditoria',
    'tDescription' => 'Conv�m que registros (log) de auditorias contendo atividades dos usu�rios, exce��es e outros eventos de seguran�a da informa��o sejam produzidos e mantidos por um per�odo de tempo acordado para auxiliar em futuras investiga��es e monitoramento de controle de acesso. ',
    'tImplementationGuide' => 'Conv�m que os registros (log) de auditoria interna incluam, quando relevante:  a) identifica��o dos usu�rios;
b) datas, hor�rios e detalhes de eventos-chave, como, por exemplo, hor�rio de entrada (log-on) e sa�da (log-off) sa�da;
c) registros das tentativas de acesso ao sistema aceitas e rejeitadas;
d) registros das tentativas de acesso a outros recursos e dados aceitos e rejeitados;
e) altera��es na configura��o do sistema;
f) uso de privil�gios;
g) uso de aplica��es e utilit�rios do sistema;
h) arquivos acessados e tipo de acesso;
i) endere�os e protocolos de rede;
j) alarmes provocados pelo sistema de controle de acesso;
k) ativa��o e desativa��o dos sistemas de prote��o, tais como sistemas antiv�rus e sistemas de detec��o de intrusos. Informa��es adicionais Os registros (log) de auditoria podem conter dados pessoais confidenciais e de intrusos. Conv�m que medidas apropriadas de prote��o de privacidade sejam tomadas - ver 15.1.4. Quando poss�vel, conv�m que administradores de sistemas n�o tenham permiss�o de exclus�o ou desativa��o dos registros (log) de suas pr�prias atividades - ver 10.1.3.',
  ),
  67 => 
  array (
    'fkContext' => '1867',
    'fkSectionBestPractice' => '1626',
    'nControlType' => '0',
    'sName' => '10.10.2 Monitoramento do uso do sistema',
    'tDescription' => 'Conv�m que sejam estabelecidos procedimentos para o monitoramento do usos dos recursos de processamento da informa��o e os resultados das atividades de monitoramento sejam analisados criticamente de forma regular.  ',
    'tImplementationGuide' => 'Conv�m que o n�vel de monitoramento requerido para os recursos individuais seja determinado atrav�s de uma an�lise avalia��o de riscos. Conv�m que a organiza��o esteja de acordo com todos os requisitos legais relevantes, aplic�veis para suas atividades de monitoramento. Conv�m que as seguintes �reas sejam consideradas:  a) acessos autorizados, incluindo detalhes do tipo:  1) o identificador do usu�rio - ID de usu�rio;
2) a data e hor�rio dos eventos-chave;
3) tipo do evento;
4) os arquivos acessados;
5) os programas ou utilit�rios utilizados;
b) todas as opera��es privilegiadas, tais como:  1) uso de contas privilegiadas, por exemplo: supervisor, root, administrados;
2) inicializa��o e finaliza��o do sistema;
3) a conex�o e a desconex�o de dispositivos de entrada e sa�da;
c) tentativas de acesso n�o autorizadas, tais como:  1) a��es de usu�rios com falhas ou rejeitados;
2) a��es envolvendo dados ou outros recursos com falhas ou rejeitadas;
3) viola��o de pol�ticas de acesso e notifica��es para gateways de rede e firewalls;
4) alertas dos sistemas propriet�rios de detec��o de intrusos;
d) alertas e falhas do sistema, tais como:  1) alertas ou mensagens do console;
2) registro das exce��es do sistema;
3) alarmes do gerenciamento da rede;
4) alarmes disparados pelo sistema de controle de acesso;
e) altera��es ou tentativas de altera��es nos controles e par�metros dos sistemas de seguran�a.  Conv�m que a freq��ncia da an�lise cr�tica dos resultados das atividades de monitoramento dependa dos riscos envolvidos. Conv�m que os seguintes fatores de risco sejam considerados:  a) criticidade dos processos de aplica��o;
b) valor, sensibilidade e criticidade da informa��o envolvida;
c) experi�ncia anterior com infiltra��es e uso impr�prio do sistema e da freq��ncia das vulnerabilidades sendo exploradas;
d) extens�o da interconex�o dos sistemas - particularmente com redes p�blicas;
e) desativa��o da grava��o dos registros (logs). Informa��es adicionais O uso de procedimentos de monitoramento � necess�rio para assegurar que os usu�rios est�o executando somente as atividades que foram explicitamente autorizadas.  A an�lise cr�tica dos registros (log) envolve a compreens�o das amea�as encontradas no sistema e a maneira pela qual isto pode acontecer. Exemplos de eventos que podem requerer uma maior investiga��o em casos de incidentes de seguran�a da informa��o s�o comentados em 13.1.1.',
  ),
  68 => 
  array (
    'fkContext' => '1868',
    'fkSectionBestPractice' => '1626',
    'nControlType' => '0',
    'sName' => '10.10.3 Prote��o das informa��es dos registros (log)',
    'tDescription' => 'Conv�m que os recursos e informa��es de registros (log) sejam protegidos contra falsifica��o e acesso n�o autorizado. ',
    'tImplementationGuide' => 'Diretrizes para implementa��o  Conv�m que os controles implementados objetivem a prote��o contra modifica��es n�o autorizadas e problemas operacionais com os recursos dos registros (log), tais como:  a) altera��es dos tipos de mensagens que s�o gravadas;
b) arquivos de registros (log) sendo editados ou exclu�dos;
c) capacidade de armazenamento da m�dia magn�tica do arquivo de registros (log) excedida, resultando em falhas no registro de eventos ou sobreposi��o do registro de evento anterior.  Alguns registros (log) de auditoria podem ser guardados como parte da pol�tica de reten��o de registros ou devido aos requisitos para a coleta e reten��o de evid�ncia - ver 13.2.3. Informa��es adicionais Registros (log) de sistema normalmente cont�m um grande volume de informa��es e muitos dos quais n�o dizem respeito ao monitoramento da seguran�a. Para ajudar a identificar eventos significativos para prop�sito de monitoramento de seguran�a, conv�m que a c�pia autom�tica dos tipos de mensagens para a execu��o de consulta seja considerada e/ou o uso de sistemas utilit�rios adequados ou ferramentas de auditoria para realizar a racionaliza��o e investiga��o do arquivo seja considerado.  Registros (log) de sistemas precisam ser protegidos, pois os dados podem ser modificados e exclu�dos e suas ocorr�ncias podem causar falsa impress�o de seguran�a. ',
  ),
  69 => 
  array (
    'fkContext' => '1869',
    'fkSectionBestPractice' => '1626',
    'nControlType' => '0',
    'sName' => '10.10.4 Registros (log) de administrador e operador',
    'tDescription' => 'Conv�m que as atividades dos administradores e operadores do sistema sejam registradas. ',
    'tImplementationGuide' => 'Conv�m que esses registros (log) incluam:  a) a hora em que o evento ocorreu - sucesso ou falha;
b) informa��es sobre o evento - exemplo: arquivos manuseados - ou falha - exemplo: erros ocorridos e a��es corretivas adotadas;
c) que conta e que administrador ou operador estava envolvido;
d) que processos estavam envolvidos.  Conv�m que os registros (log) de atividades dos operadores e administradores dos sistemas sejam analisados criticamente em intervalos regulares. Informa��es adicionais Um sistema de detec��o de intrusos gerenciado fora do controle dos administradores de rede e de sistemas pode ser utilizado para monitorar a conformidade das atividades dos administradores do sistema e da rede. ',
  ),
  70 => 
  array (
    'fkContext' => '1870',
    'fkSectionBestPractice' => '1626',
    'nControlType' => '0',
    'sName' => '10.10.5 Registros (log) de falhas',
    'tDescription' => 'Controle  Conv�m que as falhas ocorridas sejam registradas e analisadas, e que sejam adotadas a��es apropriadas. ',
    'tImplementationGuide' => 'Conv�m que falhas informadas pelos usu�rios ou pelos programas de sistemas relacionado a problemas com processamento da informa��o ou sistemas de comunica��o sejam registradas. Conv�m que existam regras claras para o tratamento das falhas informadas, incluindo:  a) an�lise cr�tica dos registros (log) de falha para assegurar que as falhas foram satisfatoriamente resolvidas;
b) an�lise cr�tica das medidas corretivas para assegurar que os controles n�o foram comprometidos e que a a��o tomada � completamente autorizada.  Conv�m que seja assegurada que a coleta dos registros de erros � permitida, caso essa fun��o do sistema esteja dispon�vel. Informa��es adicionais Registros de falhas e erros podem impactar o desempenho do sistema. Conv�m que cada tipo de registro a ser coletado seja permitido por pessoas competentes e que o n�vel de registro requerido para cada sistema individual seja determinado por uma an�lise/avalia��o de riscos, levando em considera��o a degrada��o do desempenho do sistema. ',
  ),
  71 => 
  array (
    'fkContext' => '1871',
    'fkSectionBestPractice' => '1626',
    'nControlType' => '0',
    'sName' => '10.10.6 Sincroniza��o dos rel�gios',
    'tDescription' => 'Conv�m que os rel�gios de todos os sistemas de processamento da informa��o relevante, dentro da organiza��o ou do dom�nio de seguran�a, sejam sincronizados de acordo com uma hora oficial. ',
    'tImplementationGuide' => 'Onde um computador ou dispositivo de comunica��o tiver a capacidade para operar um rel�gio (clock) de tempo real, conv�m que o rel�gio seja ajustado conforme o padr�o acordado, por exemplo o tempo coordenado universal (Coordinated Universal Time - UTC) ou um padr�o de tempo local. Como alguns rel�gios s�o conhecidos pela sua varia��o durante o tempo, conv�m que exista um procedimento que verifique esses tipos de inconsist�ncias e corrija qualquer varia��o significativa.  A interpreta��o correta do formato data/hora � importante para assegurar que o timestamp reflete a data/hora real. Conv�m que se levem em conta especifica��es locais (por exemplo, hor�rio de ver�o). Informa��es adicionais O estabelecimento correto dos rel�gios dos computadores � importante para assegurar a exatid�o dos registros (log) de auditoria, que podem ser requeridos por investiga��es ou como evid�ncias em casos legais ou disciplinares. Registros (log) de auditoria incorretos podem impedir tais investiga��es e causar danos � credibilidade das evid�ncias. Um rel�gio interno ligado ao rel�gio at�mico nacional via transmiss�o de r�dio pode ser utilizado como rel�gio principal para os sistemas de registros (logging). O protocolo de hora da rede pode ser utilizado para sincronizar todos os rel�gios dos servidores com o rel�gio principal. ',
  ),
  72 => 
  array (
    'fkContext' => '1872',
    'fkSectionBestPractice' => '1628',
    'nControlType' => '0',
    'sName' => '11.1.1 Pol�tica de controle de acesso',
    'tDescription' => 'Conv�m que a pol�tica de controle de acesso seja estabelecida documentada e analisada criticamente, tornando-se como base os requisitos de acesso dos neg�cios e seguran�a da informa��o. ',
    'tImplementationGuide' => 'Conv�m que as regras de controle de acesso e direitos para cada usu�rio ou grupos de usu�rios sejam expressas claramente na pol�tica de controle de acesso. Conv�m considerar os controles de acesso l�gico e f�sico - ver se��o 9 - de forma conjunta. Conv�m fornecer aos usu�rios e provedores de servi�os uma declara��o n�tida dos requisitos do neg�cio a serem atendidos pelos controles de acessos.  Conv�m que a pol�tica leve em considera��o os seguintes itens:  a) requisitos de seguran�a de aplica��es de neg�cios individuais;
b) identifica��o de todas as informa��es relacionadas �s aplica��es de neg�cios e os riscos a que as informa��es est�o expostas;
c) pol�tica para dissemina��o e autoriza��o da informa��o, por exemplo, a necessidade de conhecer princ�pios e n�veis de seguran�a e a classifica��o das informa��es - ver 7.2;
d) consist�ncia entre controle de acesso e pol�ticas de classifica��o da informa��o em diferentes sistemas e redes;
e) legisla��o pertinente e qualquer obriga��o contratual relativa � prote��o de acesso para dados ou servi�os - ver 15.1;
f) perfis de acesso de usu�rio-padr�o para trabalhos comuns na organiza��o;
g) administra��o de direitos de acesso em um ambiente distribu�do e conectado � rede que reconhece todos os tipos de conex�es dispon�veis;
h) segrega��o de regras de controle de acesso, por exemplo, pedidos de acesso, autoriza��o de acesso, administra��o de acesso;
i) requisitos para autoriza��o formal de pedidos de acesso - ver 11.2.1;
j) requisitos para an�lise cr�tica peri�dica de controles de acesso - ver 11.2.4;
k) remo��o de direitos de acesso - ver 8.3.3. Informa��es adicionais Conv�m que sejam tomados cuidados na especifica��o de regras de controle de acesso quando se considerar o seguinte:  a) diferenciar entre regras que devem ser obrigat�rias e for�adas, e diretrizes que s�o opcionais ou condicionais;
b) estabelecer regra baseada na premissa \'\'Tudo � proibido, a menos que expressamente permitido\'\' em  lugar da regra mais fraca \'\'Tudo � permitido, a menos que expressamente proibido\'\';
c) mudan�as em r�tulos de informa��es - ver 7.2 - que s�o iniciadas automaticamente atrav�s de recursos de processamento da informa��o e os que iniciaram pela pondera��o de um usu�rio;
d) mudan�as em permiss�es de usu�rios que s�o iniciadas automaticamente pelo sistema de informa��o e aqueles iniciados por um administrador;
e) regras que requerem aprova��o espec�fica antes de um decreto ou lei e as que n�o necessitam.  Conv�m que as regras para controle de acesso sejam apoiadas por procedimentos formais e responsabilidades claramente definidas - ver 6.1.3, 11.3, 10.4.1 e 11.6',
  ),
  73 => 
  array (
    'fkContext' => '1873',
    'fkSectionBestPractice' => '1629',
    'nControlType' => '0',
    'sName' => '11.2.1 Registro de usu�rio',
    'tDescription' => 'Conv�m que exista um procedimento formal de registro e cancelamento de usu�rio para garantir e revogar acessos em todos os sistemas de informa��es e servi�os. ',
    'tImplementationGuide' => 'Conv�m que os procedimentos de controle de acesso para registro e cancelamento de usu�rios incluam:  a) utilizar identificador de usu�rio (ID de usu�rio) �nico para assegurar a responsabilidade de cada usu�rio por suas a��es,;
conv�m que o uso de grupos de ID somente seja permitido onde existe a necessidade para o neg�cio ou por raz�es operacionais, e isso seja aprovado e documentado;
b) verificar se o usu�rio tem autoriza��o do propriet�rio do sistema para o uso do sistema de informa��o ou servi�o;
aprova��o separada para direitos de acesso do gestor tamb�m pode ser apropriada;
c)  verificar se o n�vel de acesso concedido � apropriado ao prop�sito do neg�cio - ver 11.1 - e � consistente com a Pol�tica de Seguran�a da organiza��o, por exemplo, n�o compromete a segrega��o de fun��o - ver 10.1.3;
d) dar para os usu�rios uma declara��o por escrito dos seus direitos de acesso;
e) requerer aos usu�rios a assinatura de uma declara��o indicando que eles entendem as condi��es de acesso;
f) assegurar aos provedores de servi�os que n�o ser�o dados acessos at� que os procedimentos de autoriza��o tenham sido conclu�dos;
g) manter um registro formal de todas as pessoas registradas para usar o servi�o;
h) remover imediatamente ou bloquear direitos de acesso de usu�rios que mudaram de cargo ou fun��es, ou deixaram a organiza��o;
i) verificar periodicamente e remover ou bloquear identificadores (ID) e contas de usu�rios redundantes - ver 11.2.4;
j) assegurar que identificadores de usu�rio (ID de usu�rio) redundantes n�o sejam atribu�dos para outros usu�rios. Informa��es adicionais Conv�m que seja considerado estabelecer perfis de acesso do usu�rio baseados nos requisitos dos neg�cios que resumam um n�mero de direitos de acesso dentro de um perfil de acesso t�pico de usu�rio. Solicita��es de acessos e an�lises cr�ticas - ver 11.2.4 - s�o mais f�ceis de gerenciar ao n�vel de tais perfis do que ao n�vel de direitos particulares.  Conv�m que seja considerada a inclus�o de cl�usulas nos contratos de usu�rios e de servi�os que especifiquem as san��es em caso de tentativa de acesso n�o autorizado pelos usu�rios ou por terceiros - ver 6.1.5, 8.1.3 e 8.2.3.',
  ),
  74 => 
  array (
    'fkContext' => '1874',
    'fkSectionBestPractice' => '1629',
    'nControlType' => '0',
    'sName' => '11.2.2 Gerenciamento de privil�gios',
    'tDescription' => 'Conv�m que a concess�o e o uso de privil�gios sejam restritos e controlados  ',
    'tImplementationGuide' => 'Conv�m que os sistemas de multiusu�rios que necessitam de prote��o contra acesso n�o autorizado tenham a concess�o de privil�gios controlada por um processo de autoriza��o formal. Conv�m que os seguintes passos sejam considerados:  a) privil�gio de acesso de cada produto de sistema, por exemplo, sistema operacional, sistemas de gerenciamento de banco de dados e cada aplica��o, e de categorias de usu�rios para os quais estes necessitam ser concedido, seja identificado;
b) os privil�gios sejam concedidos a usu�rios conforme a necessidade de uso e com base em eventos alinhados com a pol�tica de controle de acesso (ver 11.1.1), por exemplo, requisitos m�nimos para sua fun��o somente quando necess�rio;
c) um processo de autoriza��o e um registro de todos os privil�gios concedidos sejam mantidos. Conv�m que os privil�gios n�o sejam fornecidos at� que todo o processo de autoriza��o seja finalizado;
d) desenvolvimento e uso de programas que n�o necessitam funcionar com privil�gios sejam estimulados;
e) os privil�gios sejam atribu�dos para um identificador de usu�rio (ID de usu�rio) diferente daqueles usados normalmente para os neg�cios. Informa��es adicionais O uso inapropriado de privil�gios de administrador de sistema (qualquer caracter�stica ou recursos de sistemas de informa��o que habilitam usu�rios a exceder o controle de sistemas ou aplica��es) pode ser grande fator de contribui��o para falhas ou viola��es de sistemas. ',
  ),
  75 => 
  array (
    'fkContext' => '1875',
    'fkSectionBestPractice' => '1629',
    'nControlType' => '0',
    'sName' => '11.2.3 Gerenciamento de senha do usu�rio',
    'tDescription' => 'Conv�m que a concess�o de senhas seja controlada atrav�s de um processo de gerenciamento formal. ',
    'tImplementationGuide' => 'Conv�m que o processo considere os seguintes requisitos:  a) solicitar aos usu�rios a assinatura de uma declara��o, para manter a confidencialidade de sua senha pessoal e das senhas de grupos de trabalho, exclusivamente com os membros do grupo;
esta declara��o assinada pode ser inclu�da nos termos e condi��es da contrata��o - ver 8.1.3;
b) garantir, onde os usu�rios necessitam manter suas pr�prias senhas, que sejam fornecidas inicialmente senhas seguras e tempor�rias (ver 11.3.1), o que obriga o usu�rio a alter�-la imediatamente;
c) estabelecer procedimentos para verificar a identidade de um usu�rio antes de fornecer uma senha tempor�ria, de substitui��o ou nova;
d) fornecer senhas tempor�rias aos usu�rios de maneira segura;
conv�m que o uso de mensagens de correio eletr�nico de terceiros ou desprotegido (texto claro) seja evitado;
e) senhas tempor�rias sejam �nicas para uma pessoa e n�o sejam de f�cil memoriza��o;
f) usu�rios acusem o recebimento de senhas;
g) as senhas nunca sejam armazenadas nos sistemas de um computador de forma desprotegida;
h) as senhas padr�o sejam alteradas logo ap�s a instala��o de sistemas ou software. Informa��es adicionais Senhas s�o um meio comum de verificar a identidade de um usu�rio antes que acessos sejam concedidos a um sistema de informa��o ou servi�o de acordo com a autoriza��o do usu�rio. Outras tecnologias para identifica��o de usu�rio e autentica��o, como biom�trica, por exemplo, verifica��o de digitais, verifica��o de assinatura, e uso de tokens, por exemplo, e cart�es inteligentes, est�o dispon�veis, e conv�m que sejam consideradas, se apropriado. ',
  ),
  76 => 
  array (
    'fkContext' => '1876',
    'fkSectionBestPractice' => '1629',
    'nControlType' => '0',
    'sName' => '11.2.4 An�lise cr�tica dos direitos de acesso de usu�rio',
    'tDescription' => 'Conv�m que o gestor conduza a intervalos regulares a an�lise cr�tica dos direitos de acesso dos usu�rios, por meio de um processo formal. ',
    'tImplementationGuide' => 'Conv�m que a an�lise cr�tica dos direitos de acesso considere as seguintes orienta��es:  a) direitos de acesso de usu�rios sejam revisados em intervalos regulares, por exemplo, um per�odo se seis meses depois de qualquer mudan�a, como promo��o, rebaixamento ou encerramento do contrato - ver 11.2.1;
b) os direitos de acesso de usu�rios sejam analisados criticamente e realocados quando movidos de um tipo de atividade para outra na mesma organiza��o;
c) autoriza��es para direitos de acesso privilegiado especial - ver 11.2.2 - sejam analisadas criticamente em intervalos mais freq�entes, por exemplo, em um per�odo de tr�s meses;
d) as aloca��es de privil�gios sejam verificadas em intervalo de tempo regular para garantir que privil�gios n�o autorizados n�o foram obtidos;
e) as modifica��es para contas de privil�gios sejam registradas para an�lise cr�tica peri�dica. Informa��es adicionais � necess�rio analisar criticamente, a intervalos regulares, os direitos de acesso de usu�rios para manter o controle efetivo sobre os acessos de dados e servi�os de informa��o. ',
  ),
  77 => 
  array (
    'fkContext' => '1877',
    'fkSectionBestPractice' => '1630',
    'nControlType' => '0',
    'sName' => '11.3.1 Uso de senha',
    'tDescription' => 'Conv�m que os usu�rios sejam solicitados a seguir as boas praticas de seguran�a da informa��o na sele��o e uso de senhas. ',
    'tImplementationGuide' => 'Conv�m que os usu�rios sejam informados para:  a) manter a confidencialidade das senhas;
b) evitar manter anotadas senhas (por exemplo, papel, arquivos ou dispositivos m�veis), a menos que elas possam ser armazenadas de forma segura e o m�todo de armazenamento esteja aprovado;
c) alterar senha sempre que existir qualquer indica��o de poss�vel comprometimento do sistema ou da pr�pria senha;
d) selecionar senhas de qualidade com um tamanho m�nimo que sejam:  1) f�ceis de lembrar;
2) n�o baseadas em nada que algu�m facilmente possa adivinhar ou obter usando informa��es relativas � pessoa, por exemplo, nomes n�meros de telefone e datas de anivers�rio;
3) n�o vulner�veis a ataque de dicion�rio - por exemplo, n�o consistir em palavras inclusas no dicion�rio;
4) isentas de caracteres id�nticos consecutivos, todos num�ricos ou todos alfab�ticos sucessivos;
e) modificar senhas regularmente ou com base no n�mero de acessos (conv�m que senhas de acesso a contas privilegiadas sejam modificadas mais frequentemente que senhas normais) e evitar a reutiliza��o ou reutiliza��o do ciclo de senhas antigas;
f) modificar senhas tempor�rias no primeiro acesso ao sistema;
g) n�o incluir senhas em nenhum processo autom�tico de acesso ao sistema, por exemplo, armazenadas em um macro ou fun��es-chave;
h) n�o compartilhar senhas de usu�rios individuais;
i) n�o utilizar a mesma senha para uso com finalidades profissionais e pessoais.  Se os usu�rios necessitam acessar m�ltiplos servi�os, sistemas ou plataformas, e forem requeridos para manter separadamente m�ltiplas senhas conv�m que eles sejam alertados para usar uma �nica senha de qualidade - ver d) acima - para todos os servi�os, j� que o usu�rio estar� assegurado de que um razo�vel n�vel de prote��o foi estabelecido para o armazenamento da senha em cada servi�o, sistema ou plataforma. Informa��es adicionais A gest�o do sistema de help desk que trata de senhas perdidas ou esquecidas necessita de cuidado especial, pois este caminho pode ser tamb�m um dos meios de ataque ao sistema de senha. ',
  ),
  78 => 
  array (
    'fkContext' => '1878',
    'fkSectionBestPractice' => '1630',
    'nControlType' => '0',
    'sName' => '11.3.2 Equipamento de usu�rio sem monitora��o',
    'tDescription' => 'Conv�m que os usu�rios assegurem que os equipamentos n�o monitorados tenham prote��o adequada. ',
    'tImplementationGuide' => 'Conv�m que todos os usu�rios estejam cientes dos requisitos de seguran�a da informa��o e procedimentos para proteger equipamentos desacompanhados, assim como suas responsabilidades por implementar estas prote��es. Conv�m que os usu�rios sejam informados para:  a) encerrar as se��es ativas, a menos que elas possam ser protegidas por meio de um mecanismo de bloqueio, por exemplo tela de prote��o com senha;
b) efetuar a desconex�o com o computador de grande porte, servidores e computadores pessoais do escrit�rio, quando a se��o for finalizada - p� exemplo: n�o apenas desligar a tela do computador ou o terminal;
c) proteger os microcomputadores ou terminais contra o uso n�o autorizado atrav�s de tecla de bloqueio ou outro controle equivalente, por exemplo, senha de acesso, quando n�o estiver em uso - ver 11.3.3. Informa��es adicionais Equipamentos instalados em �reas de usu�rios, por exemplo, esta��es de trabalho ou servidores de arquivos, podem requerer prote��o especial contra acesso n�o autorizado, quando deixados sem monitora��o por um per�odo extenso.  ',
  ),
  79 => 
  array (
    'fkContext' => '1879',
    'fkSectionBestPractice' => '1630',
    'nControlType' => '0',
    'sName' => '11.3.3 Pol�tica de mesa limpa e tela limpa',
    'tDescription' => 'Conv�m que seja adotada uma pol�tica de mesa limpa de pap�is e m�dias de armazenamento remov�vel e pol�tica de tela limpa para os recursos de processamento da informa��o. ',
    'tImplementationGuide' => 'Conv�m que uma pol�tica de mesa limpa e tela limpa leve em considera��o a classifica��o da informa��o (ver 7.2), requisitos contratuais e legais (ver 15.1), e o risco correspondente e aspectos culturais da organiza��o. Conv�m que as seguintes diretrizes sejam consideradas:  a) informa��es do neg�cio sens�veis ou cr�ticas, por exemplo, em papel ou em m�dia de armazenamento eletr�nicas, sejam guardadas em lugar seguro (idealmente em um cofre, arm�rio ou outras formas de mob�lia de seguran�a) quando n�o em uso, especialmente quando o escrit�rio est� desocupado;
b) computadores e terminais sejam mantidos desligados ou protegidos com mecanismo de travamento de tela e teclados controlados por senha, token ou mecanismo de autentica��o similar quando sem monitora��o e protegidos por tecla de bloqueio, senhas ou outros controles, quando n�o usados;
c) pontos de entrada e sa�da de correspond�ncias e m�quinas de fac-s�mile sem monitora��o sejam protegidos;
d) sejam evitados o uso n�o autorizado de fotoc�piadoras e outra tecnologia de reprodu��o - por exemplo, scanners, m�quinas fotogr�ficas digitais;
e) documentos que cont�m informa��o sens�vel ou classificada sejam removidos de impressoras imediatamente. Informa��es adicionais Uma pol�tica de mesa limpa e tala limpa reduz o risco de acesso n�o autorizado, perda e dano da informa��o durante e fora do hor�rio normal de trabalho. Cofres e outras formas de instala��es de armazenamento seguro tamb�m podem proteger informa��es armazenadas contra desastres como inc�ndio, terremotos, enchentes ou explos�o.  Considerar o uso de impressoras com fun��o de c�digo PIN, permitindo desta forma que os requerentes sejam os �nicos que possam pegar suas impress�es, e apenas quando estiverem pr�ximos �s impressoras. ',
  ),
  80 => 
  array (
    'fkContext' => '1880',
    'fkSectionBestPractice' => '1631',
    'nControlType' => '0',
    'sName' => '11.4.1 Pol�tica de uso dos servi�os de rede',
    'tDescription' => 'Controle  Conv�m que usu�rios somente recebam acesso para os servi�os que tenham sido especificamente autorizados a usar. ',
    'tImplementationGuide' => 'Conv�m que uma pol�tica seja formulada relativamente ao uso de redes e servi�os de rede. Conv�m que esta pol�tica cubra:  a) redes e servi�os de redes que s�o permitidos de serem acessados;
b) procedimentos de autoriza��o para determinar quem tem permiss�o para acessar em quais redes e servi�os de redes;
c) gerenciamento dos controles e procedimentos para proteger acesso a conex�es e servi�os de redes;
d) os meios usados para acessar redes e servi�os de rede (por exemplo, as condi��es por permitir acesso discado para acessar o provedor de servi�o internet ou sistema remoto).  Conv�m que a pol�tica no uso de servi�os de rede seja consistente com s pol�tica de controle de acesso do neg�cio - ver 11.1. Informa��es adicionais Conex�es sem autoriza��o e inseguras nos servi�os de rede podem afetar toda organiza��o. Este controle � particularmente importante para conex�es de rede sens�veis ou aplica��es de neg�cios cr�ticos ou para usu�rios em locais de alto risco, por exemplo, �reas p�blicas ou externas que est�o fora da administra��o e controle da seguran�a da organiza��o. ',
  ),
  81 => 
  array (
    'fkContext' => '1881',
    'fkSectionBestPractice' => '1631',
    'nControlType' => '0',
    'sName' => '11.4.2 Autentica��o para conex�o externa do usu�rio',
    'tDescription' => 'Conv�m que m�todos apropriados de autentica��es sejam usados para controlar acesso de usu�rios remotos. ',
    'tImplementationGuide' => 'A autentica��o de usu�rios remotos pode ser alcan�ada usando, por exemplo, t�cnica baseada em criptografia, hardware tokens ou um protocolo de desafio/resposta. Podem ser achadas poss�veis implementa��es de tais t�cnicas em v�rias solu��es de redes privadas virtuais (VPN). Tamb�m pode ser usadas linha privadas dedicadas para prover garantia da origem de conex�es.  Os procedimentos e controles de discagem reversa (dial-back), por exemplo, usando modens com discagem reversa, podem prover prote��o contra conex�es n�o autorizadas e n�o desejadas nos recursos de processamento da informa��o de uma organiza��o. Este tipo de controle autentica usu�rios que tentam estabelecer conex�es com a rede de uma organiza��es de localidades remotas. Ao usar este controle, conv�m que uma organiza��o n�o use servi�os de rede que incluem transfer�ncia de chamadas (forward) ou, se eles fizerem, conv�m que seja desabilitado o uso de tais facilidades para evitar exposi��o a fragilidades associadas ao call forward. Conv�m que o processo de discagem reversa assegure que uma desconex�o atual no lado da organiza��o aconte�a. Caso contr�rio, o usu�rio remoto poderia reter aberta a linha simulando que a verifica��o do retorno da chamada (call back) ocorreu. Conv�m que os procedimentos e controles da discagem reversa sejam testados completamente para esta possibilidade.  Autentica��o de um n� pode servir como meio alternativo de autenticar grupos de usu�rios remotos onde eles s�o conectados a recursos de computador seguros e compartilhados. T�cnicas criptogr�ficas, por exemplo, com base em certificados de m�quinas, podem ser usadas para autentica��o de n�. Isto � parte de v�rias solu��es baseado em VPN.  Conv�m que seja implementado controle de autentica��o adicional para controlar acesso a redes sem fios. Em particular, cuidado especial � necess�rio na sele��o de controles para redes sem fios devido �s numerosas oportunidades de n�o detec��o de intercepta��o e inser��o de tr�fico de rede. Informa��es adicionais As conex�es externas proporcionam um potencial de acessos n�o autorizados para as informa��es de neg�cio, por exemplo, acesso por m�todos discados (dial-up). Existem diferentes tipos de m�todos de autentica��o, alguns deles proporcionam um maior n�vel de prote��o que outros, por exemplo, m�todos baseados em t�cnicas de criptografia que podem proporcionar autentica��o forte. � importante determinar o n�vel de prote��o requerido a partir de uma an�lise/avalia��o de riscos. Isto � necess�rio para selecionar apropriadamente um m�todo de autentica��o.  Os recursos de conex�o autom�tica para computadores remotos podem prover um caminho para ganhar acesso n�o autorizado nas aplica��es de neg�cio. Isto � particularmente importante se a conex�o usar uma rede que est� fora do controle do gerenciamento de seguran�a da informa��o da organiza��o. ',
  ),
  82 => 
  array (
    'fkContext' => '1882',
    'fkSectionBestPractice' => '1631',
    'nControlType' => '0',
    'sName' => '11.4.3 Identifica��o de equipamentos em redes',
    'tDescription' => 'Conv�m que sejam consideradas as identifica��es autom�ticas de equipamentos como um meio de autenticar conex�es vindas de localiza��es e equipamentos espec�ficos. ',
    'tImplementationGuide' => 'Uma identifica��o de equipamentos pode ser usada se for importante que a comunica��o possa somente ser iniciada de um local ou equipamento espec�fico. Um identificador no equipamento pode ser usado para indicar se este equipamento possui permiss�o para conectar-se � rede. Conv�m que estes identificadores indiquem claramente para qual rede o equipamento possui permiss�o para conectar-se, se existe mais de uma rede e particularmente se estas redes s�o de sensibilidade diferente. Pode ser necess�rio considerar prote��o f�sica do equipamento para manter a seguran�a do identificador do equipamento. Informa��es adicionais Este controle pode ser complementado com outras t�cnicas para autenticar o usu�rio do equipamento - ver 11.4.2. Pode ser aplicada identifica��o de equipamento adicionalmente � autentica��o de usu�rio.',
  ),
  83 => 
  array (
    'fkContext' => '1883',
    'fkSectionBestPractice' => '1631',
    'nControlType' => '0',
    'sName' => '11.4.4 Prote��o e configura��o de portas de diagn�stico remotas',
    'tDescription' => 'Conv�m que seja controlado o acesso f�sico e l�gico das portas de diagn�stico e configura��o. ',
    'tImplementationGuide' => 'Os controles potenciais para o acesso �s portas de diagn�stico e configura��o incluem o uso de uma tecla de bloqueio e procedimentos de suporte para controlar o acesso f�sico �s portas. Um exemplo para tal procedimento � assegurar que as portas de diagn�stico e configura��o s�o apenas acess�veis pela combina��o do acesso requerido entre o gestor dos servi�os do computador e pelo pessoal de suporte do hardware/software.  Conv�m que portas, servi�os e recursos similares instalados em um computador ou recurso de rede que n�o s�o especificamente requeridos para a funcionalidade do neg�cio sejam desabilitados ou removidos. Informa��es adicionais Muitos sistemas de computadores, sistemas de rede e sistemas de comunica��o s�o instalados com os recursos de diagn�stico ou configura��o remota para uso pelos engenheiros de manuten��o. Se desprotegidas, estas portas de diagn�stico proporcionam meios de acesso n�o autorizado. ',
  ),
  84 => 
  array (
    'fkContext' => '1884',
    'fkSectionBestPractice' => '1631',
    'nControlType' => '0',
    'sName' => '11.4.5 Segrega��o de redes',
    'tDescription' => 'Conv�m que grupos de servi�os e informa��o, usu�rios e sistemas de informa��o sejam segredados em redes. ',
    'tImplementationGuide' => 'Um m�todo de controlar a seguran�a da informa��o em grandes redes � dividir em diferentes dom�nios de redes l�gicas, por exemplo, os dom�nios de redes internas de uma organiza��o e dom�nios externos de uma rede, cada um protegido por um per�metro de seguran�a definido. Um conjunto de controles regul�veis pode ser aplicado em dom�nios de redes l�gicas diferentes para adicionar mais seguran�a aos ambientes de seguran�a de rede, por exemplo, sistemas publicamente acess�veis, redes internas e ativos cr�ticos. Conv�m que os dom�nios sejam definidos com base em uma an�lise/avalia��o de riscos e os requisitos de seguran�a diferentes dentro de cada um dos dom�nios.  Tal per�metro de rede pode ser implementado instalando um gateway seguro entre as duas redes a serem interconectadas para controlar o acesso e o fluxo de informa��o entre os dois dom�nios. Conv�m que este gateway seja configurado para filtrar tr�fego entre estes dom�nios - ver 11.4.6 e 11.4.7 - e bloquear acesso n�o autorizado conforme a pol�tica de controle de acesso da organiza��o - ver 11.1. Um exemplo deste tipo de gateway � o que geralmente chamado de firewall. Outro m�todo de segregar dom�nios l�gicos � restringir acesso de rede usando redes privadas virtuais para grupos de usu�rio dentro da organiza��o.  Podem tamb�m ser segregadas redes usando a funcionalidade de dispositivo de rede, por exemplo, IP switching. Os dom�nios separados podem ser implementados controlando os fluxos de dados de rede, usando as capacidades de routing/switching, do mesmo modo que listas de controle de acesso.  Conv�m que crit�rios para segrega��o de redes em dom�nios estejam baseados na pol�tica de controle de acesso (ver 10.1), e tamb�m levem em conta os custos relativos e impactos de desempenho em incorporar roteamento adequado � rede ou tecnologia de gateway - ver 11.4.6 e 11.4.7.  Al�m disso, conv�m que segrega��o de redes esteja baseada no valor e classifica��o de informa��es armazenadas ou processadas na rede, n�veis de confian�a ou linhas de neg�cios para reduzir o impacto total de uma interrup��o de servi�o.  Conv�m considerar � segrega��o de redes sem fios de redes internas e privadas. Como os per�metros de redes sem fio n�o s�o bem definidos, conv�m que uma an�lise/avalia��o de riscos seja realizada em tais casos para identificar os controle (por exemplo, autentica��o forte, m�todos criptogr�ficos e sele��o de freq��ncia) para manter segrega��o de rede. Informa��es adicionais As redes est�o sendo progressivamente estendidas al�m dos limites organizacionais tradicionais, tendo em vista as parcerias de neg�cio que s�o formadas e que podem requerer a interconex�o ou compartilhamento de processamento de informa��o e recursos de rede. Tais extens�es podem aumentar o risco de acesso n�o autorizado a sistemas de informa��o existentes que usam a rede e alguns dos quais podem requerer prote��o de outros usu�rios de rede devido a sensibilidade ou criticidade. ',
  ),
  85 => 
  array (
    'fkContext' => '1885',
    'fkSectionBestPractice' => '1631',
    'nControlType' => '0',
    'sName' => '11.4.6 Controle de conex�o de rede',
    'tDescription' => 'Para redes compartilhadas, especialmente essas que se estendem pelos limites da organiza��o, conv�m que a capacidade dos usu�rios para conectar-se � rede seja restrita, alinhada com a pol�tica de controle de acesso e os requisitos das aplica��es do neg�cio - ver 11.1. ',
    'tImplementationGuide' => 'Conv�m que os direitos de acesso dos usu�rios a rede sejam mantidos e atualizados conforme requerido pela pol�tica de controle de acesso - ver 11.1.1.  A capacidade de conex�o de usu�rios pode ser restrita atrav�s dos gateways que filtram tr�fico por meio de tabelas ou regras predefinidas. Conv�m que sejam aplicadas restri��es nos seguintes exemplos de aplica��es:  a) mensagens, por exemplo, correio eletr�nico;
b) transfer�ncia de arquivo;
c) acesso � aplica��o.  Conv�m que sejam considerados direitos de acesso entre redes para certo per�odo do dia ou datas. Informa��es adicionais A incorpora��o de controles para restringir a capacidade de conex�o dos usu�rios pode ser requerida pela pol�tica de controle de acesso para redes compartilhadas, especialmente aquelas que estendam os limites organizacionais. ',
  ),
  86 => 
  array (
    'fkContext' => '1886',
    'fkSectionBestPractice' => '1631',
    'nControlType' => '0',
    'sName' => '11.4.7 Controle de roteamento de redes',
    'tDescription' => 'Conv�m que seja implementado controle de roteamento na rede, para assegurar que as conex�es de computador e fluxos de informa��o n�o violem a pol�tica de controle de acesso das aplica��es do neg�cio. ',
    'tImplementationGuide' => 'Conv�m que os controles de roteamento sejam baseados no mecanismo de verifica��o positiva do endere�o de origem e destinos.  Os gateways de seguran�a podem ser usados para validar endere�os de origem e destino nos pontos de controle de rede interna ou externa se o proxy e/ou tecnologia de tradu��o de endere�o forem empregados. Conv�m que os implementadores estejam conscientes da for�a e defici�ncias de qualquer mecanismo implementado. Conv�m que os requisitos de controles de roteamento das redes sejam baseados na pol�tica de controle de acesso - ver 11.1. Informa��es adicionais As redes compartilhadas, especialmente as que estendem os limites organizacionais, podem requerer controles adicionais de roteamento. Isto se aplica particularmente onde s�o compartilhadas redes com terceiros (usu�rios que n�o pertencem a organiza��o).  ',
  ),
  87 => 
  array (
    'fkContext' => '1887',
    'fkSectionBestPractice' => '1632',
    'nControlType' => '0',
    'sName' => '11.5.1 Procedimentos seguros de entrada no sistema (log-on)',
    'tDescription' => 'Conv�m que o acesso aos sistemas operacionais seja controlado por um procedimento seguro de entrada no sistema (log-on). ',
    'tImplementationGuide' => 'Conv�m que o procedimento para entrada no sistema operacional seja configurado para minimizar a oportunidade de acessos n�o autorizados. Conv�m que o procedimento de entrada (log-on) divulgue o m�nimo de informa��es sobre o sistema, de forma a evitar o fornecimento de informa��es desnecess�rias a um usu�rio n�o autorizado. Conv�m que um bom procedimento de entrada no sistema (log-on):  a) n�o mostre identificadores de sistema ou de aplica��o at� que o processo tenha sido conclu�do com sucesso;
b) mostre um aviso geral informando que o computador seja acessado somente por usu�rios autorizados;
c) n�o forne�a mensagens de ajuda durante o procedimento de entrada (log-on) que poderiam auxiliar um usu�rio n�o autorizado;
d) valide informa��es de entrada no sistema somente quando todos os dados de entrada estiverem completos. Caso ocorra uma condi��o de erro, conv�m que o sistema n�o indique qual parte do dado de entrada est� correta ou incorreta;
e) limite o n�mero permitido de tentativas de entrada no sistema (log-on) sem sucesso, por exemplo, tr�s tentativas, e considere:  1) registro das tentativas com sucesso ou com falhas;
2) imposi��o do tempo de espera antes de permitir novas tentativas de entrada no sistema (log-on) ou rejei��o de qualquer tentativa posterior de acesso sem autoriza��o espec�fica;
3) encerramento das conex�es por data link;
4) envio de uma mensagem de alerta para o console do sistema, se o n�mero m�ximo de tentativas de entrada no sistema (log-on) for alcan�ado;
5) configura��o do n�mero de reutiliza��o de senhas alinhado com o tamanho m�nimo da senha e o valor do sistema que est� sendo protegido;
f) limite o tempo m�ximo e m�nimo permitindo para o procedimento de entrada no sistema (log-on). Se excedido, conv�m que o sistema encerre o procedimento;
g) mostre as seguintes informa��es, quando o procedimento de entrada no sistema (log-on) finalizar com sucesso:  1) data e hora da �ltima entrada no sistema (log-on) com sucesso;
2) detalhes de qualquer tentativa sem sucesso de entrada no sistema (log-on) desde o �ltimo acesso com sucesso;
h) n�o mostre a senha que est� sendo informada ou considere ocultar os caracteres da senha por s�mbolos;
i) n�o transmita senhas em texto claro pela rede.  Informa��es adicionais Se as senhas forem transmitidas em texto claro durante o procedimento de entrada no sistema (log-on) pela rede, elas podem ser capturadas por um programa de sniffer de rede, instalado nela. ',
  ),
  88 => 
  array (
    'fkContext' => '1888',
    'fkSectionBestPractice' => '1632',
    'nControlType' => '0',
    'sName' => '11.5.2 Identifica��o e autentica��o de usu�rio',
    'tDescription' => 'Controle  Conv�m que todos os usu�rios tenham um identificador �nico (ID de usu�rio) para uso pessoal e exclusivo, e conv�m que uma t�cnica adequada de autentica��o seja escolhida para validar a identidade alegada por um usu�rio. ',
    'tImplementationGuide' => 'Conv�m que este controle seja aplicado para todos os tipos de usu�rios (incluindo o pessoal de suporte t�cnico, operadores, administradores de rede, programadores de sistema e administradores de banco de dados).  Conv�m que os identificadores de usu�rios (ID de usu�rios) possam ser utilizados para rastrear atividades ao indiv�duo respons�vel. Conv�m que atividades regulares de usu�rios n�o sejam executadas atrav�s de contas privilegiadas.  Em circunst�ncias excepcionais, onde exista um claro benef�cio ao neg�cio, pode ocorrer a utiliza��o de um identificador de usu�rio (ID de usu�rio) compartilhado por um grupo de usu�rios ou para um trabalho espec�fico. Conv�m que a aprova��o pelo gestor esteja documentada nestes casos. Controles adicionais podem ser necess�rios para manter as responsabilidades.  Conv�m que identificadores de usu�rios (ID de usu�rios) gen�ricos para uso de um indiv�duo somente sejam permitidos onde as fun��es acess�veis ou as a��es executadas pelo usu�rio n�o precisam ser rastreadas (p� exemplo, acesso somente leitura), ou quando existem outros controles implementados (por exemplo, senha para identificador de usu�rio gen�rico somente fornecida para um indiv�duo por vez e registrada).  Conv�m que onde autentica��o forte e verifica��o de identidade � requerida, m�todos alternativos de autentica��o de senhas, como meios criptogr�ficos, cart�es inteligentes (smart card), tokens e meios biom�tricos sejam utilizados. Informa��es adicionais As senhas - ver 11.3.1 e 11.5.3 - s�o uma maneira muito comum de se prover identifica��o com base em um segredo que apenas o usu�rio conhece. O mesmo pode ser obtido com meios criptogr�ficos e protocolos de autentica��o. Conv�m que a for�a da identifica��o e autentica��o de usu�rio seja adequada com a sensibilidade da informa��o a ser acessada.  Objetos como tokens de mem�ria ou cart�es inteligentes (smart card) que os usu�rios possuem tamb�m podem ser usados para identifica��o e autentica��o. As tecnologias de autentica��o biom�trica que usam caracter�sticas ou atributos �nicos de um indiv�duo tamb�m podem ser usadas para autenticar a identidade de uma pessoa. Uma combina��o de tecnologias e mecanismos seguramente relacionados em uma autentica��o forte.',
  ),
  89 => 
  array (
    'fkContext' => '1889',
    'fkSectionBestPractice' => '1632',
    'nControlType' => '0',
    'sName' => '11.5.3 Sistema de gerenciamento de senha',
    'tDescription' => 'Conv�m que sistemas para gerenciamento de senhas sejam interativos e assegurem senhas de qualidade. ',
    'tImplementationGuide' => 'Conv�m que o sistema de gerenciamento de senha:  a) obrigue o uso de identificador de usu�rio (ID de usu�rio) e senha individual para manter responsabilidades;
b) permita que usu�rios selecionem e modifiquem suas pr�prias senhas, incluindo um procedimento de confirma��o para evitar erros;
c) obrigue a escolha de senhas de qualidade - ver 11.3.1;
d) obrigue a troca de senhas - ver 11.3.1;
e) obrigue os usu�rios a trocar a senha tempor�ria no primeiro acesso - ver 11.2.3;
f) mantenha um registro das senhas anteriores utilizadas e bloqueie a reutiliza��o;
g) n�o mostre as senhas na tela quando forem digitadas;
h) armazene os arquivos de senha separadamente dos dados do sistema da aplica��o;
i) armazene e transmita as senhas de forma protegida (por exemplo, criptografada ou hashed). Informa��es adicionais A senha � um dos principais meios de validar a autoridade de um usu�rio para acessar um servi�o de computador.  Algumas aplica��es requerem que senhas de usu�rio sejam atribu�das por uma autoridade independente. Em alguns casos, as al�neas b), d) e e) das diretrizes acima n�o se aplicam. Na maioria dos casos, as senhas s�o selecionadas e mantidas pelos usu�rios. Ver 11.3.1 para diretrizes do uso de senhas. ',
  ),
  90 => 
  array (
    'fkContext' => '1890',
    'fkSectionBestPractice' => '1632',
    'nControlType' => '0',
    'sName' => '11.5.4 Uso de utilit�rios de sistema',
    'tDescription' => 'Conv�m que o uso de programas utilit�rios que podem ser capazes de sobrepor os controles dos sistemas e aplica��es seja restrito e estritamente controlado. ',
    'tImplementationGuide' => 'Conv�m que as seguintes diretrizes para o uso de utilit�rios de sistemas sejam consideradas: a) uso de procedimentos de identifica��o, autentica��o e autoriza��o para utilit�rios de sistema;
b) segrega��o dos utilit�rios de sistema dos softwares de aplica��o;
c) limita��o do uso dos utilit�rios de sistema a um n�mero m�nimo de usu�rios confi�veis e autorizados - ver 11.2.2;
d) autoriza��o para uso de utilit�rios de sistema n�o previstos;
e) limita��o da disponibilidade dos utilit�rios de sistema, por exemplo para a dura��o de uma modifica��o autorizada;
f) registro de todo o uso de utilit�rios de sistemas;
g) defini��o e documenta��o dos n�veis de  autoriza��o para utilit�rios de sistema;
h) remo��o ou desabilita��o de todos os softwares utilit�rios e de sistemas desnecess�rios;
i) n�o deixar utilit�rios de sistemas dispon�veis para usu�rios que t�m acesso �s aplica��es nos sistemas onde segrega��o de fun��es � requerida. Informa��es adicionais A maioria das instala��es de computadores tem um ou mais programas utilit�rios de sistema que podem ser capazes de sobrepor os controles dos sistemas e aplica��es. ',
  ),
  91 => 
  array (
    'fkContext' => '1891',
    'fkSectionBestPractice' => '1632',
    'nControlType' => '0',
    'sName' => '11.5.5 Desconex�o de terminal por inatividade',
    'tDescription' => 'Conv�m que terminais inativos sejam desconectados ap�s um per�odo definido de inatividade. ',
    'tImplementationGuide' => 'Conv�m que esta facilidade de desconex�o por tempo preveja a limpeza da tela do terminal e o encerramento das se��es do aplicativo e da rede ap�s um certo per�odo definido de inatividade. Conv�m que o prazo de tempo para desconex�o reflita os riscos de seguran�a da �rea, a classifica��o da informa��o que est� sendo manuseada, as aplica��es que est�o sendo utilizadas e os riscos relacionados para os usu�rios do terminal do equipamento.  Uma forma limitada para o recurso de desconex�o de terminal pode ser provida por alguns sistemas, os quais limpam a tela e previnem acesso n�o autorizado, mas n�o fecham as se��es das aplica��es ou da rede. Informa��es adicionais Este controle � particularmente importante em locais de alto risco, os quais incluem �reas p�blicas ou externas fora dos limites do gerenciamento de seguran�a da organiza��o. Conv�m que estas se��es sejam desligadas para prevenir o acesso por pessoas n�o autorizadas e ataques de nega��o do servi�o ',
  ),
  92 => 
  array (
    'fkContext' => '1892',
    'fkSectionBestPractice' => '1632',
    'nControlType' => '0',
    'sName' => '11.5.6 Limita��o de hor�rio de conex�o',
    'tDescription' => 'Conv�m que restri��es nos hor�rios de conex�o sejam utilizadas para proporcionar seguran�a adicional para aplica��es de alto risco.  ',
    'tImplementationGuide' => 'Conv�m que controles de hor�rio de conex�o sejam considerados para aplica��es computacionais sens�veis, especialmente aquelas com terminais instalados em locais de alto risco, por exemplo em �reas p�blicas ou externas fora dos limites do gerenciamento de seguran�a da organiza��o. Exemplos deste tipo de restri��o incluem:  a) utiliza��o de blocos de hor�rios pr� determinados, por exemplo para lotes de transmiss�o de arquivos ou se��es regulares interativas de curta dura��o;
b) restri��o dos hor�rios de conex�o �s horas normais de expediente se n�o houver necessidades para horas extras ou trabalhos fora do hor�rio normal;
c) considerar a reautentica��o em intervalos de tempo. Informa��es adicionais Limitar o per�odo durante o qual as conex�es de terminal para os servi�os computadorizados s�o permitidas reduz a janela de oportunidade para acessos n�o autorizados. Limitar a dura��o de se��es ativas inibe os usu�rios a manter se��es abertas, para evitar reautentica��o.  ',
  ),
  93 => 
  array (
    'fkContext' => '1893',
    'fkSectionBestPractice' => '1633',
    'nControlType' => '0',
    'sName' => '11.6.1 Restri��o de acesso � informa��o',
    'tDescription' => 'Conv�m que o acesso � informa��o e as fun��es dos sistemas de aplica��es por usu�rios e pessoal de suporte seja restrito de acordo com o definido na pol�tica de controle de acesso. ',
    'tImplementationGuide' => 'Conv�m que restri��es para acesso sejam baseadas nos requisitos das aplica��es individuais do neg�cio. Conv�m que a pol�tica de controle de acesso seja consistente com a pol�tica de acesso organizacional - ver 11.1.  Conv�m que a aplica��o dos seguintes controles seja considerada de forma a suportar os requisitos de restri��o de acesso:  a) fornecer menus para controlar o acesso �s fun��es dos sistemas de aplica��o;
b) controlar os direitos de acesso dos usu�rios, por exemplo, ler, escrever, excluir e executar;
c) controlar os direitos de acesso de outras aplica��es;
d) assegurar que as sa�das dos sistemas de aplica��o que tratam informa��es sens�veis contenham apenas a informa��o relevante ao uso de tais sa�das e s�o enviadas apenas para os terminais e locais autorizados;
conv�m incluir uma an�lise cr�tica peri�dica de tais sa�das, para assegurar que informa��o desnecess�ria � removida.  ',
  ),
  94 => 
  array (
    'fkContext' => '1894',
    'fkSectionBestPractice' => '1633',
    'nControlType' => '0',
    'sName' => '11.6.2 Isolamento de sistemas sens�veis',
    'tDescription' => 'Conv�m que sistemas sens�veis tenham um ambiente computacional dedicado (isolado). ',
    'tImplementationGuide' => 'Conv�m que os seguintes pontos sejam considerados para o isolamento de um sistema sens�vel:  a) a sensibilidade de um sistema de aplica��o seja explicitamente identificada e documentada pelo propriet�rio da aplica��o - ver 7.1.2;
b) quando uma aplica��o sens�vel � executada em um ambiente compartilhado, conv�m que se identifiquem os sistemas de aplica��o com os quais ela compartilha recursos e os correspondentes riscos, e que se obtenha a concord�ncia do propriet�rio da aplica��o sens�vel. Informa��es adicionais Alguns sistemas de aplica��o s�o suficientemente sens�veis a perdas potenciais, requerendo tratamento especial. A sensibilidade pode indicar que o sistema de aplica��o:  a) seja executado a partir de um computador dedicado;
ou b) compartilha recursos somente com sistemas de aplica��o confi�veis;
O isolamento pode ser obtido utilizando-se m�todos f�sicos ou l�gicos - ver 11.4.5.  ',
  ),
  95 => 
  array (
    'fkContext' => '1895',
    'fkSectionBestPractice' => '1634',
    'nControlType' => '0',
    'sName' => '11.7.1 Computa��o e comunica��o m�vel',
    'tDescription' => 'Conv�m que uma pol�tica formal seja estabelecida e que medidas de seguran�a apropriadas sejam adotadas para a prote��o contra os riscos do uso de recursos de computa��o e comunica��o m�veis. ',
    'tImplementationGuide' => 'Quando se utilizam recursos de computa��o e comunica��o m�veis, como, por exemplo, notebooks, palmtops, laptops, cart�es inteligentes (smart cards) e telefones celulares, conv�m que cuidados especiais sejam tomados para assegurar que as informa��es do neg�cio n�o sejam comprometidas. A pol�tica de computa��o m�vel deve levar em considera��o os riscos de se trabalhar com equipamentos de computa��o m�vel em ambientes desprotegidos.  Conv�m que a pol�tica de computa��o m�vel inclua os requisitos de prote��o f�sica, controles de acesso, t�cnicas criptogr�ficas, c�pias de seguran�a e prote��o contra v�rus. Conv�m que sejam estabelecidas prote��es para evitar o acesso n�o autorizado ou a divulga��o de informa��es armazenadas processadas nestes recursos, por exemplo atrav�s da utiliza��o de t�cnicas de criptografia - ver 12.3.  Conv�m que sejam tomadas certas precau��es ao se utilizarem os recursos de computa��o m�vel em locais p�blicos, salas de reuni�es e outras �reas desprotegidas fora dos limites da organiza��o. Conv�m que sejam estabelecidas prote��es para evitar o acesso n�o autorizado ou a divulga��o de informa��es armazenadas e processadas nestes recursos, por exemplo atrav�s da utiliza��o de t�cnicas de criptografia - ver 12.3.  Conv�m que usu�rios de recursos de computa��o m�vel em locais p�blicos tomem cuidado para evitar o risco de capta��o por pessoas n�o autorizadas. Conv�m que procedimentos contra softwares maliciosos sejam estabelecidos e mantidos sempre atualizados - ver 10.4.  Conv�m que c�pias de seguran�a das informa��es cr�ticas de neg�cio sejam feitas regularmente. Conv�m que equipamentos estejam dispon�veis para possibilitar a realiza��o de c�pias de seguran�a das informa��es de forma r�pida e f�cil. Para essas c�pias de seguran�a, conv�m que sejam adotadas prote��es adequadas contra, por exemplo, roubo ou perda de informa��o.  Conv�m que prote��o adequada seja dada para o uso dos recursos de computa��o m�vel conectados em rede. Conv�m que o acesso remoto �s informa��es do neg�cio atrav�s de redes p�blicas, usando os recursos de computa��o m�vel, ocorra apenas ap�s o sucesso da identifica��o e da autentica��o, e com os apropriados mecanismos de controle de acesso implantados - ver 11.4.  Conv�m que os recursos de computa��o m�vel tamb�m estejam protegidos fisicamente contra roubo, especialmente quando deixados, por exemplo, em carros ou em outros meios de transporte, quartos de hot�is, centros de confer�ncia e locais de reuni�o. Conv�m que esteja estabelecido um procedimento espec�fico que leve em considera��o requisitos legais, securit�rios e outros requisitos de seguran�a da organiza��o para casos de roubo ou perda de recursos de computa��o m�vel. Conv�m que os equipamentos que cont�m informa��es importantes, sens�veis e/ou cr�ticas para o neg�cio n�o sejam deixados sem observa��o e, quando poss�vel, estejam fisicamente trancados, ou conv�m que travas especiais sejam utilizadas para proteger o equipamento - ver 9.2.5.  Conv�m que seja providenciado treinamento para os usu�rios de computa��o m�vel, para aumentar o n�vel de conscientiza��o a respeito dos riscos adicionais resultantes desta forma de trabalho e dos controles que devem ser implementados. Informa��es adicionais As redes de conex�o sem fio s�o similares a outros tipos de redes, mas possuem diferen�as importantes que conv�m que sejam consideradas quando da identifica��o de controles. As diferen�as t�picas s�o:  a) alguns protocolos de seguran�a de redes sem fio s�o imaturos e possuem fragilidades conhecidas;
b) pode n�o ser poss�vel efetuar c�pias de seguran�a das informa��es armazenadas em computadores m�veis devido � largura de banda limitada e/ou devido ao equipamento m�vel n�o estar conectado no momento em que as c�pias de seguran�a est�o programadas. ',
  ),
  96 => 
  array (
    'fkContext' => '1896',
    'fkSectionBestPractice' => '1634',
    'nControlType' => '0',
    'sName' => '11.7.2 Trabalho remoto',
    'tDescription' => 'Conv�m que uma pol�tica, planos operacionais e procedimentos sejam desenvolvidos e implementados para atividades de trabalho remoto. ',
    'tImplementationGuide' => 'Conv�m que as organiza��es somente autorizem atividades de trabalho remotas apenas se elas estiverem certas de que as provid�ncias apropriadas e controles de seguran�a est�o implementados e que estes est�o de acordo com a Pol�tica de Seguran�a da organiza��o.  Conv�m que a prote��o apropriada ao local de trabalho remoto seja implantada para evitar, por exemplo, o roubo do equipamento e de informa��es, a divulga��o n�o autorizada de informa��o, o acesso remoto n�o autorizado aos sistemas internos da organiza��o ou mau uso de recursos. Conv�m que o trabalho remoto seja autorizado e controlado pelo gestor e conv�m que sejam asseguradas as provid�ncias adequadas a esta forma de trabalho.  Conv�m que os seguintes pontos sejam considerados:  a) a seguran�a f�sica existente no local do trabalho remoto, levando-se em considera��o a seguran�a f�sica do pr�dio e o ambiente local;
b) o ambiente f�sico proposto para o trabalho remoto;
c) os requisitos de seguran�a nas comunica��es, levando em considera��o a necessidade do acesso remoto aos sistemas internos da organiza��o, a sensibilidade da informa��o que ser� acessada e trafegada na linha de comunica��o e a sensibilidade do sistema interno;
d) a amea�a de acesso n�o autorizado � informa��o ou aos recursos por outras pessoas que utilizam o local, por exemplo fam�lias e amigos;
e) o uso de redes dom�sticas e requisitos ou restri��es na configura��o de servi�os de rede sem fio;
f) pol�ticas e procedimentos para evitar disputas relativas a direitos de propriedade intelectual desenvolvidas em equipamentos de propriedade particular;
g) acesso a equipamentos de propriedade particular (para verificar a seguran�a da m�quina ou durante uma investiga��o), que pode ser proibido legalmente;
h) acordos de licenciamento de software que podem tornar as organiza��es respons�veis pelo licenciamento do software cliente em esta��es de trabalhos particulares de propriedade de funcion�rios, fornecedores ou terceiros;
i) requisitos de prote��o contra v�rus e requisitos de firewall.  Conv�m que as diretrizes e provid�ncias a serem consideradas incluam:  a) a provis�o de equipamento e mob�lia apropriados �s atividade de trabalho remoto, onde o uso de equipamentos de propriedade particular que n�o esteja sob controle da organiza��o n�o � permitido;
b) uma defini��o do trabalho permitido, o per�odo de trabalho, a classifica��o da informa��o que pode ser tratada e os sistemas internos e servi�os que o usu�rio do trabalho remoto est� autorizado a acessar;
c) a provis�o de equipamentos de comunica��o apropriado, incluindo m�todos para acesso remoto seguro;
d) seguran�a f�sica;
e) regras e diretrizes sobre o acesso de familiares e visitantes ao equipamento e � informa��o;
f) a provis�o de suporte e manuten��o de hardware e software;
g) a provis�o de seguro;
h) os procedimentos para c�pias de seguran�a e continuidade do neg�cio;
i) auditoria e monitoramento da seguran�a;
j) revoga��o de autoridade e direitos de acesso, e devolu��o do equipamento quando as atividades trabalho remoto cessarem. Informa��es adicionais As atividades de trabalho remoto utilizam tecnologias de comunica��o que permitem que as pessoas trabalhem remotamente de uma localidade fixa externa � sua organiza��o. ',
  ),
  97 => 
  array (
    'fkContext' => '1897',
    'fkSectionBestPractice' => '1636',
    'nControlType' => '0',
    'sName' => '12.1.1 An�lise e especifica��o dos requisitos de seguran�a',
    'tDescription' => 'Conv�m que sejam especificados os requisitos para controles de seguran�a nas especifica��es de requisitos de neg�cios, para novos sistemas de informa��o ou melhorias em sistemas existentes. ',
    'tImplementationGuide' => 'Conv�m que as especifica��es para os requisitos de controles, nos sistemas de informa��o, considerem os controles autom�ticos a serem incorporados, assim como a necessidade de apoiar controles manuais. Conv�m que considera��es similares sejam aplicadas quando da avalia��o de pacotes de softwares, desenvolvidos internamente ou comprados, para as aplica��es de neg�cios.  Conv�m que requisitos de seguran�a e controles reflitam o valor para o neg�cio dos ativos de informa��o envolvidos - ver 7.2), e os danos potenciais ao neg�cio que poderiam resultar de uma falha ou aus�ncia de seguran�a.  Conv�m que os requisitos de sistemas para a seguran�a da informa��o, bem como os processos para implement�-la sejam integrados aos est�gios iniciais dos projetos dos sistemas de informa��o. Controles introduzidos no est�gio de projeto s�o significativamente mais baratos para implementar e manter do que aqueles inclu�dos durante ou ap�s a implementa��o.  Conv�m que, no caso de produtos comprados, um processo formal de aquisi��o e testes seja seguido. Conv�m que contratos com fornecedores levem em considera��o os requisitos de seguran�a identificados. Nas situa��es em que funcionalidades de seguran�a de um produto proposto n�o satisfa�am requisitos especificados, conv�m que o risco introduzido, assim como os controles associados, sejam reconsiderados antes da compra do produto. Nas situa��es em que as funcionalidades adicionais incorporadas acarretem riscos � seguran�a, conv�m que estas sejam desativadas ou a estrutura de controles proposta seja analisada criticamente para determinar se h� vantagem na utiliza��o das funcionalidades em quest�o. Informa��es adicionais Se considerado apropriado, por exemplo, por raz�es de custos, o gestor pode considerar o uso de produtos avaliados e certificados por entidade independente. Informa��o adicional sobre crit�rios de avalia��o de produtos pode ser encontrada na ISO/IEC 15408 ou em outras normas de avalia��o ou certifica��o apropriadas.  A ISO/IEC 13335-3 possui orienta��o sobre o uso de processos de gerenciamento de riscos para a identifica��o de requisitos de controles de seguran�a. ',
  ),
  98 => 
  array (
    'fkContext' => '1898',
    'fkSectionBestPractice' => '1637',
    'nControlType' => '0',
    'sName' => '12.2.1 Valida��o dos dados de entrada',
    'tDescription' => 'Conv�m que os dados de entrada de aplica��es sejam validados para garantir que s�o corretos e apropriados. ',
    'tImplementationGuide' => 'Conv�m que sejam aplicadas checagens na entrada de transa��es de neg�cios, em dados permanentes (por exemplo, nomes e endere�os, limites de cr�dito, n�meros de refer�ncia de clientes) e em , par�metros de tabelas (por exemplo, pre�os de venda, taxas de convers�o de moedas, tarifas de impostos). Conv�m que as seguintes diretrizes sejam consideradas:  a) entrada duplicada ou outros tipos de verifica��o, tais como checagem de limites ou campos limitando as faixas especificas de dados de entrada, para detectar os seguintes erros:  1) valores fora de faixa;
2) caracteres inv�lidos em campos de dados;
3) dados incompletos ou faltantes;
4) volumes de dados excedendo limites superiores ou inferiores;
5) dados de controle inconsistentes ou n�o autorizados ;
b) verifica��o peri�dica do conte�do de campos-chave ou arquivos de dados para confirmar a sua validade e integridade;
c) inspe��o de c�pias impressas de documentos de entrada para detectar quaisquer altera��es n�o autorizadas (conv�m que todas as mudan�as em documentos de entrada sejam autorizadas;
d) procedimentos para tratar os erros de valida��o;
e) procedimentos para testar a plausabilidade dos dados de entrada;
f) defini��o das responsabilidade de todo o pessoal envolvido no processo de entrada de dados;
g) cria��o de um registro de atividades envolvendo o processo de entrada de dados - ver 10.10.1. Informa��es adicionais A verifica��o e valida��o autom�tica de dados de entrada podem ser consideradas, onde aplic�veis, para reduzir o risco de erros e prevenir ataques conhecidos como buffer overflow e inje��o de c�digo. ',
  ),
  99 => 
  array (
    'fkContext' => '1899',
    'fkSectionBestPractice' => '1637',
    'nControlType' => '0',
    'sName' => '12.2.2 Controle do processamento interno',
    'tDescription' => 'Conv�m que sejam incorporadas, nas aplica��es, checagens de valida��o com o objetivo de detectar qualquer corrup��o de informa��es, por erros ou por a��es deliberadas. ',
    'tImplementationGuide' => 'Conv�m que o projeto e a implementa��o das aplica��es garantam que os riscos de falhas de processamento que levem � perda de integridade sejam minimizados. �reas espec�ficas a serem consideradas incluem:  a) o uso das fun��es, como incluir, modificar e remover para implementa��o de altera��es nos dados. b) Procedimentos para evitar que programas rodem na ordem errada ou continuem rodando ap�s uma falha de processamento - ver 10.1.1. c) O uso de programas apropriados para recupera��o de falhas, para assegurar o processamento correto dos dados;
d) Prote��o contra ataques usando buffer overrun/overflow;
Conv�m que seja preparada uma lista de verifica��o apropriada, as atividades sejam documentadas e os resultados mantidos em seguran�a. Exemplos de verifica��es que podem ser incorporadas incluem:  a) controles de se��es ou de lotes, para reconciliar saldos de arquivos ap�s as atualiza��es de transa��es;
b) controles de saldos, para verifica��o de saldos abertos comparando com saldos previamente encerrados o batimento de saldos de abertura contra saldos de fechamento, utilizando:  1) controles run-to-run;
2) totaliza��es na atualiza��o de arquivos;
3) controles program-to-program;
c) valida��o de dados de entrada gerados pelo sistema - ver 12.2.1;
d) verifica��es de integridade, autenticidade ou qualquer outra caracter�stica de seguran�a, de dados ou softwares transferidos, ou atualizados entre computadores centrais e remotos;
e) implementa��o de t�cnicas de consist�ncia (hash) para registros e arquivos;
f) verifica��es para garantir que os programas sejam rodados no tempo correto;
g) verifica��es para garantir que os programas sejam rodados na ordem correta e terminem em caso de falha, e que qualquer processamento adicional seja estancado, at� que o problema seja resolvido;
h) cria��o de um registro das atividades envolvidas no processamento - ver 10.10.1. Informa��es adicionais Os dados que tenham sido corretamente alimentados podem ser corrompidos por falhas e hardware, erros de processamento ou por atos deliberados. As verifica��es de valida��o requeridas dependem da natureza das aplica��es e do impacto, no neg�cio, de qualquer corrup��o de dados. ',
  ),
  100 => 
  array (
    'fkContext' => '1900',
    'fkSectionBestPractice' => '1637',
    'nControlType' => '0',
    'sName' => '12.2.3 Integridade das mensagens',
    'tDescription' => 'Conv�m que requisitos para garantir a autenticidade e proteger a integridade das mensagens em aplica��es sejam identificados e os controles apropriados sejam identificados e implementados ',
    'tImplementationGuide' => 'Conv�m que seja efetuada uma an�lise/avalia��o dos riscos de seguran�a para determinar se a integridade das mensagens � requerida e para identificar o m�todo mais apropriado de implementa��o. Informa��es adicionais As t�cnicas criptogr�ficas - ver 12.3 - podem ser usadas como um meio apropriado para a implementa��o da autentica��o de mensagens. ',
  ),
  101 => 
  array (
    'fkContext' => '1901',
    'fkSectionBestPractice' => '1637',
    'nControlType' => '0',
    'sName' => '12.2.4 Valida��o dos dados de sa�da',
    'tDescription' => 'Conv�m que os dados de sa�da das aplica��es sejam validados para assegurar que o processamento das informa��es armazenadas est� correto e � apropriado �s circunst�ncias. ',
    'tImplementationGuide' => 'A valida��o de dados de sa�da pode incluir:  a) verifica��es de plausabilidade para testar se os dados de sa�da s�o razo�veis;
b) controles envolvendo contagens de reconcilia��o para garantir o processamento de todos os dados;
c) fornecimento de informa��o suficiente para que um leitor ou um sistema de processamento subseq�ente possa determinar a exatid�o, completeza, precis�o e classifica��o das informa��es;
d) procedimentos para responder aos testes de valida��o de dados de sa�da;
e) defini��o das responsabilidades de todo o pessoal envolvido no processo de dados de sa�da;
f) cria��o de um registro de atividades do processo de valida��o dos dados de sa�da. Informa��es adicionais Tipicamente, sistemas e aplica��es s�o constru�dos no pressuposto de que, tendo sido efetuadas as valida��es apropriadas, verifica��es e teste, as sa�das estar�o sempre corretas. Contudo, este pressuposto nem sempre � valido, isto �, sistemas que tenham sido testados podem ainda produzir dados de sa�da incorretos sob certas circunst�ncias. ',
  ),
  102 => 
  array (
    'fkContext' => '1902',
    'fkSectionBestPractice' => '1638',
    'nControlType' => '0',
    'sName' => '12.3.1 Pol�tica para o uso de controles criptogr�ficos',
    'tDescription' => 'Conv�m que seja desenvolvida e implementada uma pol�tica para o uso de controles criptogr�ficos para a prote��o da informa��o. ',
    'tImplementationGuide' => 'Conv�m que, quando do desenvolvimento de uma pol�tica para criptografia, sejam considerados:  a) a abordagem gerencial quanto ao uso de controles criptogr�ficos em toda a organiza��o, incluindo os princ�pios gerais sob os quais as informa��es de neg�cio sejam protegidas - ver 5.1.1;
b) a identifica��o do n�vel requerido de prote��o com base em uma an�lise/avalia��o de riscos, levando em considera��o o tipo, a for�a e a qualidade do algoritmo de criptografia requerido;
c) o uso de criptografia para a prote��o de informa��es sens�veis transportadas em celulares e PDA, m�dias remov�veis ou m�veis, dispositivos ou linhas de comunica��o;
d) a abordagem do gerenciamento de chaves, incluindo m�todos para lidar com a prote��o das chaves criptogr�ficas e a recupera��o de informa��es cifradas,  no caso de chaves perdidas, comprometidas ou danificadas;
e) pap�is e responsabilidades, por exemplo, de quem for respons�vel:  1) pela implementa��o da pol�tica;
2) pelo gerenciamento de chaves, incluindo sua gera��o - ver 12.3.2;
f) os padr�es a serem adotados para a efetiva implementa��o ao longo de toda a organiza��o - qual solu��o � usada para quais processos de neg�cios;
g) o impacto do uso de informa��es cifradas em controles que dependem da inspe��o de conte�dos (por exemplo, detec��o de v�rus).  Conv�m que sejam consideradas, na implementa��o da pol�tica criptogr�fica da organiza��o, as leis ou regulamenta��es e restri��es nacionais aplic�veis ao uso de t�cnicas criptogr�ficas, nas diferentes partes do mundo, e das quest�es relativas ao fluxo transfronteiras de informa��es cifradas - ver 15.1.6.  Controles criptogr�ficos podem ser usados para alcan�ar diversos objetivos de seguran�a, como, por exemplo:  a) confidencialidade: usando a criptogr�fica da informa��o para proteger informa��es sens�veis ou cr�ticas, armazenadas ou transmitidas;
b) integridade/autenticidade: usando assinaturas digitais ou c�digos de autentica��o de mensagens (MAC) para proteger a autenticidade e integridade de informa��es sens�veis ou cr�ticas, armazenadas ou transmitidas;
c) n�o-rep�dio: usando t�cnicas de criptografia para obter prova da ocorr�ncia ou n�o ocorr�ncia de um evento ou a��o. Informa��es adicionais Conv�m que a tomada de decis�o para verificar se uma solu��o de criptografia � apropriada seja vista como parte de um processo de an�lise/avalia��o de riscos e sele��o de controles mais amplos. Essa avalia��o pode, ent�o, ser usada para determinar se um controle criptogr�fico � apropriado, que tipo de controle seja usado e para que prop�sito e processos de neg�cios.  Uma pol�tica sobre o uso de controles criptogr�ficos � necess�ria para maximizar os benef�cios e minimizar os riscos do uso de t�cnicas, criptogr�ficas para evitar o uso incorreto ou inapropriado. Quanto ao uso de assinaturas digitais, conv�m que seja considerada toda a legisla��o relevante, em particular aquela que descreve as condi��es sob as quais uma assinatura digital � legalmente aceita - ver 15.1.  Conv�m que seja buscada a opini�o de um especialista para identificar o n�vel apropriado de prote��o e definir as especifica��es aplic�veis que proporcionar�o o n�vel requerido de prote��o e o apoio � implementa��o de um sistema seguro de gerenciamento de chaves - ver 12.3.2.  O ISO/IEC JTC1 SC27 desenvolveu diversas normas relacionadas com controles criptogr�ficos. Informa��es adicionais podem tamb�m ser encontradas na IEEE P1363 e no OECD Guidelines on Cryptography. ',
  ),
  103 => 
  array (
    'fkContext' => '1903',
    'fkSectionBestPractice' => '1638',
    'nControlType' => '0',
    'sName' => '12.3.2 Gerenciamento de chaves',
    'tDescription' => 'Conv�m que um processo de gerenciamento de chaves seja implantado para apoiar o uso de t�cnicas criptogr�ficas pela organiza��o. ',
    'tImplementationGuide' => 'Conv�m que todas as chaves criptogr�ficas sejam protegidas contra modifica��o, perda e destrui��o. Adicionalmente, chaves secretas e privadas necessitam de prote��o contra a divulga��o n�o autorizada. Conv�m que os equipamentos utilizados para gerar, armazenar e guardar as chaves sejam fisicamente protegidos.  Conv�m que um sistema de gerenciamento de chaves seja baseado em um conjunto estabelecido de normas, procedimentos e m�todos de seguran�a para:  a) gerar chaves para diferentes sistemas criptogr�ficos e diferentes aplica��es;
b) gerar e obter certificados de chaves p�blicas;
c) distribuir chaves para os usu�rios devidos, incluindo a forma como as chaves devem ser ativadas, quando recebidas;
d) armazenar chaves, incluindo a forma como os usu�rios autorizados obt�m acesso a elas;
e) mudar ou atualizar chaves, incluindo regras relativas a quando as chaves devem ser mudadas e como isto ser� feito;
f) lidar com chaves comprometidas;
g) revogar chaves, incluindo regras de como elas devem ser retiradas ou desativadas, por exemplo quando chaves tiverem sido comprometidas ou quando um usu�rio deixa a organiza��o - conv�m que, tamb�m neste caso, que as chaves sejam guardadas;
h) recuperar chaves perdidas ou corrompidas, como parte da gest�o da continuidade do neg�cio, por exemplo para recupera��o de informa��es cifradas;
i) guardar chaves, por exemplo para informa��es guardadas ou armazenadas em c�pias de seguran�a;
j) destruir chaves;
k) manter registro e auditoria das atividades relacionadas com o gerenciamento de chaves.  Conv�m para reduzira possibilidade de comprometimento, que datas de ativa��o e desativa��o de chaves sejam definidas de forma que possam ser utilizadas apenas por um per�odo de tempo limitado. Conv�m que este per�odo de tempo seja dependente das circunst�ncias sob as quais o controle criptogr�fico est� sendo usado, assim como do risco percebido. Al�m do gerenciamento seguro de chaves secretas e privadas, conv�m que a autenticidade de chaves p�blicas seja tamb�m considerada. Este processo de autentica��o pode ser conduzido utilizando-se certificados de chaves p�blicas que s�o normalmente emitidos por uma autoridade certificadora, a qual conv�m que seja uma organiza��o reconhecida, com controles adequados e procedimentos implantados com o objetivo de garantir o requerido n�vel de confian�a.  Conv�m que o conte�do dos termos dos acordos de n�vel de servi�o ou contratos com fornecedores externos de servi�os criptogr�ficos, por exemplo com uma autoridade certificadora, cubram aspectos como responsabilidades, confiabilidade dos servi�os e tempos de resposta para a execu��o dos servi�os contratados - ver 6.2.3. Informa��es adicionais O gerenciamento de chaves criptogr�ficas � essencial  para o uso efetivo de t�cnicas criptogr�ficas. A ISO/IEC 11770 fornece informa��o adicional sobre gerenciamento de chaves. Os dois tipos de t�cnicas criptogr�ficas s�o:  a) t�cnicas de chaves secretas, onde duas ou mais partes compartilham a mesma chave, a qual � utilizado tanto para cifrar quando para decifrar a informa��o;
esta chave deve ser mantida secreta, uma vez que qualquer um tenha acesso a ela ser� capaz de decifrar todas as informa��es que tenham sido cifradas com essa chave ou dela se utilizando para introduzir informa��o n�o autorizada;
b) t�cnicas de chaves p�blicas, onde cada usu�rio possui um par de chaves;
uma chave p�blica (que pode ser revelada para qualquer um) e uma chave privada - que deve ser mantida secreta;
t�cnicas de chaves p�blicas podem ser utilizadas para cifrar e para produzir assinaturas digitais - ver tamb�m ISO/IEC 9796 e ISO/IEC 14888.  Existe a amea�a de que seja forjada uma assinatura digital pela substitui��o da chave p�blica do usu�rio. Este problema � resolvido pelo uso de um certificado de chave p�blica.  T�cnicas criptogr�ficas podem ser tamb�m utilizadas para proteger chaves criptogr�ficas. Pode ser necess�rio o estabelecimento de procedimentos para a manipula��o de solicita��es legais para acesso a chaves criptogr�ficas, por exemplo, informa��o cifrada pode ser requerida em sua forma decifrada para uso como evid�ncia em um processo judicial. ',
  ),
  104 => 
  array (
    'fkContext' => '1904',
    'fkSectionBestPractice' => '1639',
    'nControlType' => '0',
    'sName' => '12.4.1 Controle de software operacional',
    'tDescription' => 'Conv�m que procedimentos para controlar a instala��o de software em sistemas operacionais sejam implementados. ',
    'tImplementationGuide' => 'Para minimizar o risco de corrup��o aos sistemas operacionais, conv�m que as seguintes diretrizes sejam consideradas para controlar a mudan�as:  a) a atualiza��o do software operacional, de aplicativos e de bibliotecas de programas seja executada somente para administradores treinados e com autoriza��o gerencial - ver 12.4.3;
b) sistemas operacionais somente contenham c�digo execut�vel e aprovado, e n�o contenham c�digos em desenvolvimento ou compiladores;
c) sistemas operacionais e aplicativos somente sejam implementados ap�s testes extensivos e bem sucedido;
� recomend�vel que os testes incluam testes sobre o uso, seguran�a, efeitos sobre outros sistemas, como tamb�m sobre o uso amig�vel, e sejam realizados em sistemas separados - ver 10.1.4;
conv�m que seja assegurado que todas as bibliotecas de programa-fonte correspondentes tenham sido atualizadas;
d) um sistema de controle de configura��o seja utilizado para manter controle da implementa��o do software assim como da documenta��o do sistema;
e) uma estrat�gia de retorno �s condi��es anteriores seja disponibilizada antes que mudan�as sejam implementadas no sistema;
f) um registro de auditoria seja mantido para todas as atualiza��es das bibliotecas dos programas operacionais;
g) vers�es anteriores dos softwares aplicativos sejam mantidas como medida de conting�ncia;
h) vers�es antigas de software sejam arquivadas, junto com todas as informa��es e par�metros requeridos, procedimentos, detalhes de configura��es e software de suporte durante um prazo igual ao prazo de reten��o de dados.  Conv�m que software adquirido de fornecedores e utilizado em sistemas operacionais seja mantido num n�vel apoiado pelo fornecedor. Ao transcorrer do tempo, fornecedores de software cessam o apoio �s vers�es antigas do software. Conv�m que a organiza��o considere os riscos associados � depend�ncia de software sem suporte.  Conv�m que qualquer decis�o de atualiza��o para uma nova vers�o considere os requisitos do neg�cio para a mudan�a e da seguran�a associada, por exemplo, a introdu��o de uma nova funcionalidade de seguran�a ou a quantidade e a gravidade dos problemas de seguran�a associados a esta vers�o. Conv�m que pacotes de corre��es de software sejam aplicados quando puderem remover ou reduzir as vulnerabilidades de seguran�a - ver 12.6.1.  Conv�m que acessos f�sicos e l�gicos sejam concedidos a fornecedores, quando necess�rio, para a finalidade de suporte e com aprova��o gerencial. Conv�m que as atividades do fornecedor seja monitorada.  Os softwares para computadores podem depender de outros softwares e m�dulos fornecidos externamente, os quais conv�m ser monitorados e controlados para evitar mudan�as n�o autorizadas, que podem introduzir fragilidades na seguran�a. Informa��es adicionais Conv�m que sistemas operacionais sejam atualizados quando existir um requisito para tal, por exemplo, se a vers�o atual do sistema operacional n�o suportar mais os requisitos do neg�cio. Conv�m que as atualiza��es n�o sejam efetivadas pela mera disponibilidade de uma vers�o nova do sistema operacional. Novas vers�es de sistemas operacionais podem ser menos seguras, com menor estabilidade, e ser menos entendidas do que os sistemas atuais. ',
  ),
  105 => 
  array (
    'fkContext' => '1905',
    'fkSectionBestPractice' => '1639',
    'nControlType' => '0',
    'sName' => '12.4.2 Prote��o dos dados de teste do sistema',
    'tDescription' => 'Conv�m que os dados de teste sejam selecionados com cuidado, protegidos e controlados. ',
    'tImplementationGuide' => 'Para prop�sitos de teste, conv�m que seja evitado o uso de bancos de dados operacionais que contenham informa��es de natureza pessoal ou qualquer outra informa��o considerada sens�vel. Se informa��o de natureza pessoal ou outras informa��es sens�veis forem utilizadas com o prop�sito de teste, conv�m que todos os detalhes e conte�do sens�vel sejam removidos ou modificados de forma a evitar reconhecimento antes do seu uso. Conv�m que sejam aplicadas as seguintes diretrizes para a prote��o de dados operacionais, quando utilizados para fins de teste:  a) os procedimentos de controle de acesso, aplic�veis aos aplicativos de sistema em ambiente operacional, sejam tamb�m aplicados aos aplicativos de sistema em ambiente de teste;
b) seja obtida autoriza��o cada vez que for utilizada uma c�pia da informa��o operacional para o uso de um aplicativo em teste;
c) a informa��o operacional seja apagada do aplicativo em teste imediatamente ap�s completar o teste;
d) a c�pia e o uso de informa��o operacional sejam registrados de forma a prover uma trilha para auditoria. Informa��es adicionais Testes de sistemas e testes de aceita��o requerem normalmente volumes significativos de dados de teste que sejam o mais pr�ximo poss�vel aos dados utilizados no ambiente operacional. ',
  ),
  106 => 
  array (
    'fkContext' => '1906',
    'fkSectionBestPractice' => '1639',
    'nControlType' => '0',
    'sName' => '12.4.3 Controle de acesso ao c�digo-fonte de programa',
    'tDescription' => 'Conv�m que o acesso ao c�digo-fonte de programa seja restrito. ',
    'tImplementationGuide' => 'Conv�m que o acesso ao c�digo-fonte de programa e de itens associados (como desenhos, especifica��es, planos de verifica��o e de valida��o) seja estritamente controlado, com a finalidade de prevenir a introdu��o de funcionalidade n�o autorizada e para evitar mudan�as n�o intencionais. Para os c�digos-fonte de programas, este controle pode ser obtido coma guarda centralizada do c�digo, de prefer�ncia utilizando bibliotecas de c�digo-fonte. Conv�m que as seguintes orienta��es sejam consideradas - ver se��o 11 - para o controle de acesso �s bibliotecas de programa-fonte, com a finalidade de reduzir o risco de corrup��o de programas de computador:  a) quando poss�vel, seja evitado manter as bibliotecas de programa-fonte no mesmo ambiente dos sistemas operacionais;
b) seja implementado o controle de c�digo-fonte de programa e das bibliotecas de programa-fonte, conforme procedimentos estabelecidos;
c) o pessoal de suporte n�o tenha acesso irrestrito �s bibliotecas de programa-fonte;
d) a atualiza��o das bibliotecas de programa-fonte e itens associados e a entrega de fontes de programas a programadores seja apenas efetuada ap�s o recebimento da autoriza��o pertinente;
e) as listagens dos programas sejam mantidas num ambiente seguro - ver 10.7.4;
f) seja mantido um registro de auditoria de todos os acessos a c�digo-fonte de programa;
g) a manuten��o e a c�pia das bibliotecas de programa-fonte estejam sujeitas a procedimentos estritos de controles de mudan�as - ver 12.5.1;
Informa��es adicionais Os c�digos-fonte de programas s�o c�digos escritos por programadores, que s�o compilados (e ligados) para criar programas execut�veis. Algumas linguagens de programa��o n�o fazem uma distin��o formal entre c�digo-fonte e execut�vel, pois os execut�veis s�o criados no momento da sua ativa��o.  A ABNT NBR ISO 10007 e ABNT ISO/IEC 12207 possuem mais informa��es sobre a gest�o de configura��o e o processo de ciclo de vida de software. ',
  ),
  107 => 
  array (
    'fkContext' => '1907',
    'fkSectionBestPractice' => '1640',
    'nControlType' => '0',
    'sName' => '12.5.1 Procedimentos e controle de mudan�as',
    'tDescription' => 'Conv�m que a implementa��o de mudan�as seja controlada utilizando procedimentos formais de controle de mudan�as. ',
    'tImplementationGuide' => 'Conv�m que os procedimentos de controle de mudan�as sejam documentados e refor�ados com a finalidade de minimizar a corrup��o dos sistemas da informa��o. Conv�m que a introdu��o de novos sistemas e mudan�as maiores em sistemas existentes sigam um processo formal de documenta��o, especifica��o, teste, controle da qualidade e gest�o da implementa��o.  Conv�m que o processo inclua uma an�lise/avalia��o de riscos, an�lise do impacto das mudan�as e a especifica��o dos controles de seguran�a requeridos. Conv�m que o processo garanta que a seguran�a e os procedimentos de controle atuais n�o sejam comprometidos, que os programadores de suporte tenham acesso somente �s partes do sistema necess�rias para o cumprimento das tarefas e que sejam obtidas concord�ncia e aprova��o formal para qualquer mudan�a obtida.  Conv�m que, quando pratic�vel, os procedimentos de controle de mudan�as sejam integrados - ver 10.1.2.  a) a manuten��o de um registro dos n�veis a�odados de autoriza��o;
b) a garantia de que as mudan�as sejam submetidas por usu�rios autorizados;
c) a an�lise cr�tica dos procedimentos de controle e integridade para assegurar que as mudan�as n�o os comprometam;
d) a identifica��o de todo software, informa��o, entidades em bancos de dados e hardware que precisam de emendas;
e) a obten��o de aprova��o formal para propostas detalhadas antes da implementa��o;
f) a garantia da aceita��o das mudan�as por usu�rios autorizados, antes da implementa��o;
g) a garantia da atualiza��o da documenta��o do sistema ap�s a conclus�o de cada mudan�a e de que a documenta��o antiga seja arquivada ou descartada;
h) a manuten��o de um controle de vers�o de todas as atualiza��es de softwares;
i) a manuten��o de uma trilha para auditoria de todas as mudan�as solicitadas;
j) a garantia de que toda a documenta��o operacional - ver 10.1.1 - e procedimentos dos usu�rios sejam alterados conforme necess�rio e que se mantenham apropriados;
k) a garantia de que as mudan�as sejam implementadas em hor�rios apropriados, sem perturba��o dos processos de neg�cios cab�veis. Informa��es adicionais A mudan�a de software pode ter impacto no ambiente operacional.  As boas pr�ticas incluem o teste de novos softwares em um ambiente segregado dos ambientes de produ��o e de desenvolvimento - ver 10.1.4. Isto fornece um meio de controle sobre o novo software e permite uma prote��o adicional � informa��o operacional que � utilizada para prop�sitos de teste. Aplica-se tamb�m �s corre��es, pacotes de servi�o e outras atualiza��es. Conv�m que atualiza��es autom�ticas n�o sejam utilizadas em sistemas cr�ticos, pois algumas atualiza��es podem causar falhas em aplica��es cr�ticas - ver 12.6.',
  ),
  108 => 
  array (
    'fkContext' => '1908',
    'fkSectionBestPractice' => '1640',
    'nControlType' => '0',
    'sName' => '12.5.2 An�lise cr�tica t�cnica das aplica��es ap�s mudan�as no sistema operacional',
    'tDescription' => 'Conv�m que aplica��es cr�ticas de neg�cios sejam analisadas criticamente e testadas quando sistemas operacionais s�o mudados, para garantir que n�o haver� nenhum impacto adverso na opera��o da organiza��o ou na seguran�a. ',
    'tImplementationGuide' => 'Conv�m que o processo compreenda:  a) uma an�lise cr�tica dos procedimentos de controle e integridade dos controles para assegurar que n�o foram comprometidos pelas mudan�as no sistema operacional;
b) a garantia de que o plano anual de suporte e o or�amento ir�o cobrir as an�lises e testes do sistema devido �s mudan�as no sistema operacional;
c) a garantia de que as mudan�as pretendidas sejam comunicadas em tempo h�bil para permitir os testes e an�lises cr�ticas antes da implementa��o das mudan�as;
d) a garantia de que as mudan�as necess�rias sejam executadas nos planos de continuidade de neg�cios - ver se��o 14.  Conv�m que seja dada responsabilidade a um grupo espec�fico ou a um indiv�duo para monitoramento da vulnerabilidades e divulga��o de emendas e corre��es dos fornecedores de software - ver 12.6. ',
  ),
  109 => 
  array (
    'fkContext' => '1909',
    'fkSectionBestPractice' => '1640',
    'nControlType' => '0',
    'sName' => '12.5.3 Restri��es sobre mudan�as em pacotes de software',
    'tDescription' => 'Conv�m que modifica��es em pacotes de software n�o sejam incentivadas e limitadas �s mudan�as necess�rias e que todas as mudan�as sejam estritamente controladas. ',
    'tImplementationGuide' => 'Quando poss�vel e pratic�vel, conv�m que pacotes de softwares providos pelos fornecedores sejam utilizados sem modifica��es. Quando um pacote de software requer modifica��o, conv�m que sejam considerados os seguintes itens:  a) risco de que controles e processos de integridade embutidos no software sejam comprometidos;
b) a obten��o do consentimento do fornecedor;
c) a possibilidade de obten��o junto ao fornecedor das mudan�as necess�rias como atualiza��o padr�o do programa;
d) o impacto resultante quando a organiza��o passa a ser respons�vel para a manuten��o futura do software como resultado das mudan�as.  Se mudan�as forem necess�rias, conv�m que o software original seja mantido e as mudan�as aplicadas numa c�pia claramente identificada. Conv�m que um processo de gest�o de atualiza��es seja implementado para assegurar a instala��o das mais recentes corre��es e atualiza��es para todos os softwares autorizados - ver 12.6. Conv�m que todas as mudan�as sejam completamente testadas e documentadas para que possam ser reaplicadas, se necess�rio, em atualiza��es futuras do software. Se requerido, conv�m que as modifica��es sejam testadas e validadas por um grupo de avalia��o independente. ',
  ),
  110 => 
  array (
    'fkContext' => '1910',
    'fkSectionBestPractice' => '1640',
    'nControlType' => '0',
    'sName' => '12.5.4 Vazamento de informa��es',
    'tDescription' => 'Conv�m que oportunidades para vazamento de informa��es sejam prevenidas.  ',
    'tImplementationGuide' => 'Conv�m que os seguintes itens sejam considerados, para limitar o risco de vazamento de informa��es, por exemplo atrav�s do uso e explora��o de covert channels:  a) a varredura do envio de m�dia e comunica��es para verificar a presen�a de informa��o oculta;
b) o mascaramento e a modula��o do comportamento dos sistemas e das comunica��es para reduzir a possibilidade de terceiros deduzirem informa��es a partir do comportamento dos sistemas;
c) a utiliza��o de sistemas e software reconhecidos como de alta integridade, por exemplo utilizando produtos avaliados - ver ISO/IEC 15408;
d) o monitoramento regular das atividades do pessoal e do sistema, quando permitido pela legisla��o ou regulamenta��o vigente;
e) o monitoramento do uso de recursos de sistemas de computa��o. Informa��es adicionais Os covert channels s�o caminhos n�o previstos para conduzir fluxo de informa��es, mas que no entanto podem existir num sistema ou rede. Por exemplo, a manipula��o de bits no protocolo de pacotes de comunica��o poderia ser utilizada como um m�todo oculto de sinaliza��o. Devido � sua natureza, seria dif�cil, se n�o imposs�vel, precaver-se contra a exist�ncia de todos os poss�veis covert channels. No entanto, a explora��o destes canais frequentemente � realizada por c�digo troiano - ver 10.4.1. A ado��o de medidas de prote��o contra c�digo troiano reduz, o risco de explora��o de covert channels.  A precau��o contra acesso n�o autorizado � rede - ver 11.4 -, como tamb�m pol�ticas e procedimentos para dissuadir o mau uso de servi�os de informa��o pelo pessoal - ver 15.1.5 -, pode ajudar a proteger contra covert channels. ',
  ),
  111 => 
  array (
    'fkContext' => '1911',
    'fkSectionBestPractice' => '1640',
    'nControlType' => '0',
    'sName' => '12.5.5 Desenvolvimento terceirizado de software',
    'tDescription' => 'Conv�m que a organiza��o supervisione e monitore o desenvolvimento terceirizado de software.',
    'tImplementationGuide' => 'Conv�m que sejam considerados os seguintes itens quando do desenvolvimento de software terceirizado:
a) acordos de licenciamento, propriedade do c�digo e direitos de propriedade - ver 15.1.2;
b) certifica��o da qualidade e exatid�o do servi�o realizado;
c) provis�o para cust�dia no caso de falha da terceira parte;
d) direitos de acesso para auditorias de qualidade e exatid�o do servi�o realizado;
e) requisitos contratuais para a qualidade e funcionalidade da seguran�a do c�digo;
f) testes antes da instala��o para detectar a presen�a de c�digo malicioso e troiano. ',
  ),
  112 => 
  array (
    'fkContext' => '1912',
    'fkSectionBestPractice' => '1641',
    'nControlType' => '0',
    'sName' => '12.6.1 Controle de vulnerabilidades t�cnicas',
    'tDescription' => 'Conv�m que seja obtida informa��o em tempo h�bil sobre vulnerabilidades t�cnicas dos sistemas de informa��o em uso, avaliada a exposi��o da organiza��o a estas vulnerabilidades e tomadas de medidas apropriadas para lidar com os riscos associados. ',
    'tImplementationGuide' => 'Um invent�rio completo e atualizado dos ativos de informa��o - ver 7.1 - � um pr�-requisito para uma gest�o efetiva de vulnerabilidade t�cnica. Informa��o espec�fica para o apoio � gest�o de vulnerabilidade t�cnica inclui o fornecedor de software, o n�mero de vers�o, o status atual de uso e distribui��o (por exemplo, que softwares est�o instalados e em quais sistemas) e a(s) pessoa(s) na organiza��o respons�vel(is) pelos softwares.  Conv�m que a a��o apropriada seja tomada, no devido tempo, como resposta �s potenciais vulnerabilidades t�cnicas identificadas. Conv�m que as seguintes diretrizes sejam seguidas para o estabelecimento de um processo de gest�o efetivo de vulnerabilidades t�cnicas:  a) a organiza��o defina e estabele�a as fun��es e responsabilidades associadas na gest�o de vulnerabilidades t�cnicas, incluindo o monitoramento de vulnerabilidades, a an�lise/avalia��o de riscos de vulnerabilidades, patches, acompanhamento dos ativos e qualquer coordena��o de responsabilidades requerida;
b) os recursos a serem usados para a identificar vulnerabilidades t�cnicas relevantes e para manter a conscientiza��o sobre eles sejam identificados para softwares e outras tecnologias - com base na lista de invent�rio dos ativos, ver 7.1.1;
conv�m que esses recursos de informa��o sejam mantidos atualizados com base nas mudan�as no invent�rio de ativos, ou quando outros recursos novos ou �teis forem encontrados;
c) seja definido um prazo para a rea��o a notifica��es de potenciais vulnerabilidades t�cnicas relevantes;
d) uma vez que uma vulnerabilidade t�cnica potencial tenha sido identificada, conv�m que a organiza��o avalie os riscos associados e as a��es a serem tomadas;
tais a��es podem requerer o uso de patches nos sistemas vulner�veis e/ou a aplica��o de outros controles;
e) dependendo da urg�ncia exigida para tratar uma vulnerabilidade t�cnica, conv�m que a a��o tomada esteja de acordo com os controles relacionados com a gest�o de mudan�as - ver 12.5.1 - ou que sejam seguidos os procedimentos de resposta a incidentes de seguran�a da informa��o - ver 13.2;
f) se um patch for disponibilizado, conv�m que sejam avaliados os riscos associados � sua instala��o - conv�m que os riscos associados � vulnerabilidade sejam comparados com os riscos de instala��o do patch;
g) patches sejam testados e avaliados antes de serem instaladas para assegurar a efetividade e que n�o tragam efeitos que n�o possam ser tolerados;
quando n�o existir a disponibilidade de um patch, conv�m considerar o uso de outros controles, tais como:  1) a desativa��o de servi�os ou potencialidades relacionadas � vulnerabilidade;
2) a adapta��o ou agrega��o de controles de acesso, por exemplo firewalls nas fronteiras da rede - ver 11.4.5;
3) o aumento do monitoramento para detectar ou prevenir ataques reais;
4) o aumento da conscientiza��o sobre a vulnerabilidade;
h) seja mantido um registro de auditoria de todos os procedimentos realizados;
i) com a finalidade de assegurar a efic�cia e a efici�ncia, conv�m que seja monitorado e avaliado regularmente o processo de gest�o de vulnerabilidades t�cnicas;
j) conv�m abordar em primeiro lugar os sistemas com altos riscos. Informa��es adicionais O correto funcionamento do processo de gest�o de vulnerabilidades t�cnicas � critico para muitas organiza��es e, portanto, conv�m que seja monitorado regularmente. Um invent�rio de ativos preciso � essencial para assegurar que vulnerabilidades potenciais relevantes sejam identificadas.  A gest�o de vulnerabilidades t�cnicas pode ser vista como uma subfun��o da gest�o de mudan�as e, como tal, pode aproveitar os procedimentos e processos da gest�o de mudan�as ( ver 10.1.2 e 12.5.1).  Os fornecedores est�o sempre sob grande press�o para liberar patches t�o logo quando poss�vel. Portanto, um patch pode n�o abordar o problema adequadamente e pode causar efeitos colaterais negativos. Tamb�m, em alguns casos, a desinstala��o de um patch pode n�o ser facilmente realiz�vel ap�s sua instala��o.  Quando testes adequados de patch n�o forem poss�veis, por exemplo, devido a custos ou falta de recursos, um atraso no uso do patch pode ser considerado para avaliar os riscos associados, baseado nas experi�ncias relatadas por outros usu�rios. ',
  ),
  113 => 
  array (
    'fkContext' => '1913',
    'fkSectionBestPractice' => '1643',
    'nControlType' => '0',
    'sName' => '13.1.1 Notifica��o de eventos de seguran�a da informa��o',
    'tDescription' => 'Conv�m que os eventos de seguran�a da informa��o sejam relatados atrav�s dos canais apropriados da dire��o, o mais r�pido poss�vel. ',
    'tImplementationGuide' => 'Conv�m que um procedimento de notifica��o formal seja estabelecido para relatar os eventos de seguran�a da informa��o, junto com um procedimento de resposta a incidente e escalonamento, estabelecendo a a��o a ser tomada ao se receber as notifica��o de um evento de seguran�a da informa��o. Conv�m que um ponto de contato seja estabelecido para receber as notifica��es dos eventos de seguran�a da informa��o. Conv�m que este ponto de contato seja de conhecimento de toda a organiza��o e esteja sempre dispon�vel e em condi��es de assegurar uma resposta adequada e oportuna.  Conv�m que todos os funcion�rios, fornecedores e terceiros sejam alertados sobre sua responsabilidade de notificar qualquer evento de seguran�a da informa��o o mais r�pido poss�vel. Conv�m que eles tamb�m estejam cientes do procedimento para notificar os eventos de seguran�a da informa��o e do ponto de contato designado para este fim. Conv�m que os procedimentos incluam:  a) processos adequados de realimenta��o para assegurar que os eventos de seguran�a da informa��o relatados sejam notificados dos resultados ap�s a quest�o ter sido conduzida e conclu�da;
b) formul�rio para apoiar a a��o e notificar um evento de seguran�a da informa��o e ajudar as pessoas a lembrar as a��es necess�rias para a notifica��o do evento;
c) o comportamento correto a ser tomado no caso de um evento de seguran�a da informa��o, como, por exemplo:  1) anotar todos os detalhes importantes imediatamente - por exemplo, tipo de n�o- conformidade ou viola��o, mau funcionamento, mensagens na tela, comportamento estranho;
2) n�o tomar nenhuma a��o pr�pria, mas informar imediatamente o evento ao ponto de contato;
d) refer�ncia para um processo disciplinar formal estabelecido para lidar com funcion�rios, fornecedores ou terceiros que cometam viola��es de seguran�a da informa��o.  Em ambientes de alto risco, podem ser fornecidos alarmes de coa��o atrav�s do qual a pessoa que est� sendo coagida possa sinalizar o que est� ocorrendo. Conv�m que os procedimentos para responder a alarmes de coa��o reflitam o alto risco que a situa��o exige. Informa��es adicionais Exemplos de eventos e incidentes de seguran�a da informa��o s�o:  a) perda de servi�o, equipamento ou recursos;
b) mau funcionamento ou sobrecarga de sistema;
c) erros humanos;
d) n�o-conformidade com pol�ticas ou diretrizes;
e) viola��es de procedimentos de seguran�a f�sica;
f) mudan�as descontroladas de sistemas;
g) mau funcionamento de software ou hardware;
h) viola��o de acesso.  Considerando os cuidados com os aspectos de confidencialidade, os incidentes de seguran�a da informa��o podem ser utilizados em treinamento de conscientiza��o - ver 8.2.2 - como exemplo do que poderia ocorrer, como responder a tais incidentes e como evit�-los futuramente. Para ser capaz de destinar os eventos e incidentes de seguran�a da informa��o adequadamente, pode ser necess�rio coletar evid�ncias t�o logo quanto poss�vel depois da ocorr�ncia - ver 13.2.3. Um mau funcionamento ou outras anomalias de comportamento de sistemas podem ser um indicador de um ataque de seguran�a ou viola��o de seguran�a e, portanto, conv�m que sempre sejam notificados como um evento de seguran�a da informa��o.  Mais informa��es sobre notifica��o de eventos de seguran�a da informa��o e gest�o de incidentes de seguran�a da informa��o podem ser encontradas na ISO/IEC TR 18044. ',
  ),
  114 => 
  array (
    'fkContext' => '1914',
    'fkSectionBestPractice' => '1643',
    'nControlType' => '0',
    'sName' => '13.1.2 Notificando fragilidades de seguran�a da informa��o',
    'tDescription' => 'Conv�m que os funcion�rios, fornecedores e terceiros de sistemas e servi�os de informa��o sejam instru�dos a registrar e notificar qualquer observa��o ou suspeita de fragilidade em sistemas ou servi�os. ',
    'tImplementationGuide' => 'Conv�m que os funcion�rios, fornecedores e terceiros notifiquem esse assunto o mais r�pido poss�vel para sua dire��o ou diretamente ao seu provedor de servi�os, de forma a prevenir incidentes de seguran�a da informa��o. Conv�m que o mecanismo de notifica��o seja f�cil, acess�vel e dispon�vel sempre que poss�vel. Conv�m que os usu�rios sejam informados que n�o podem, sob nenhuma circunst�ncia, tentar averiguar fragilidade suspeita. Informa��es adicionais Conv�m que os funcion�rios, fornecedores e terceiros sejam alertados para n�o tentarem averiguar uma fragilidade de seguran�a da informa��o suspeita. Testar fragilidades pode ser interpretado como um uso impr�prio potencial do sistema e tamb�m pode causar danos ao sistema ou servi�o danos  ao sistema ou servi�o de informa��o, resultando em responsabilidade legal ao indiv�duo que efetuar o teste. ',
  ),
  115 => 
  array (
    'fkContext' => '1915',
    'fkSectionBestPractice' => '1644',
    'nControlType' => '0',
    'sName' => '13.2.1 Responsabilidades e procedimentos',
    'tDescription' => 'Conv�m que responsabilidades e procedimentos de gest�o sejam estabelecidos para assegurar respostar r�pidas, efetivas e ordenadas a incidentes de seguran�a da informa��o. ',
    'tImplementationGuide' => 'Conv�m que, adicionalmente � notifica��o de eventos de seguran�a da informa��o e fragilidades - ver 13.1 - do monitoramento de sistemas, alertas e vulnerabilidades - 10.10.2 - seja utilizado para a detec��o de incidentes de seguran�a da informa��o. Conv�m que as seguintes diretrizes para procedimentos de gest�o de incidentes de seguran�a da informa��o sejam consideradas:  a) procedimentos sejam estabelecidos para manusear diferentes tipos de incidentes de seguran�a da informa��o, incluindo: 1) falhas de sistemas de informa��es e perda de servi�os;
2) c�digo malicioso - ver 10.4.1;
3) denial of service - nega��o de servi�o;
4) erros resultantes de dados incompletos ou inconsistentes;
5) viola��es de confidencialidade e integridade;
6) uso impr�prio de sistemas de informa��o;
b) al�m dos planos de conting�ncia - ver 14.1.3 -, conv�m que os procedimentos tamb�m considerem - ver 13.2.2;
1) an�lise e identifica��o da causa do incidente;
2) reten��o;
3) planejamento e implementa��o de a��o corretiva para prevenir a sua repeti��o, se necess�rio;
4) comunica��o com aqueles afetados ou envolvidos com a recupera��o do incidente;
5) notifica��o da a��o para a autoridade apropriada;
c) conv�m que trilhas de auditoria e evid�ncias similares sejam coletadas - ver 13.2.3 - e protegidas, como apropriado, para:  1) an�lise de problemas internos;
2) uso como evid�ncia forense para o caso de uma potencial viola��o ou de normas reguladoras ou em caso de delitos civis ou criminais, por exemplo relacionados ao uso impr�prio de computadores ou legisla��o de prote��o dos dados;
3) negocia��o para compensa��o ou ressarcimento por parte de fornecedores de software e servi�os;
d) conv�m que as a��es para recupera��o de viola��es de seguran�a e corre��o de falhas do sistema sejam cuidadosa e formalmente controladas;
conv�m que os procedimentos assegurem que: 1) apenas funcion�rios explicitamente identificados e autorizados estejam liberados para acessar sistemas e dados em produ��o - ver 6.2 para acesso externo;
2) todas as a��es de emerg�ncia sejam documentadas em detalhe;
3) as a��es de emerg�ncia sejam relatadas para a dire��o e analisadas criticamente de maneira ordenada;
4) a integridade dos sistemas do neg�cio e seus controles sejam validados na maior brevidade.  Conv�m que os objetivos da gest�o de incidentes de seguran�a da informa��o estejam em concord�ncia com a dire��o e que seja assegurado que os respons�veis pela gest�o de incidentes de seguran�a da informa��o entendam as prioridades da organiza��o no manuseio de incidentes de seguran�a da informa��o. Informa��es adicionais Os incidentes de seguran�a da informa��o podem transcender fronteiras organizacionais e nacionais. Para responder a estes incidentes, cada vez mais h� a necessidade de resposta coordenada e troca de informa��es sobre eles com organiza��es externas, quando apropriado. ',
  ),
  116 => 
  array (
    'fkContext' => '1916',
    'fkSectionBestPractice' => '1644',
    'nControlType' => '0',
    'sName' => '13.2.2 Aprendendo com os incidentes de seguran�a da informa��o',
    'tDescription' => 'Conv�m que sejam estabelecidos mecanismos para permitir que tipos, quantidades e custos dos incidentes de seguran�a da informa��o sejam quantificados e monitorados. ',
    'tImplementationGuide' => 'Conv�m que a informa��o resultante da an�lise de incidentes de seguran�a da informa��o seja usada para identificar incidentes recorrentes ou de alto impacto. Informa��es adicionais A an�lise de incidentes de seguran�a da informa��o pode indicar a necessidade de melhorias ou controles adicionais para limitar freq��ncia, danos e custos de ocorr�ncias futuras ou para ser levada em conta quando for realizado o processo de an�lise cr�tica da Pol�tica de Seguran�a da informa��o - ver 5.1.2.',
  ),
  117 => 
  array (
    'fkContext' => '1917',
    'fkSectionBestPractice' => '1644',
    'nControlType' => '0',
    'sName' => '13.2.3 Coleta de evid�ncias',
    'tDescription' => 'Nos casos em que uma a��o de acompanhamento contra uma pessoa ou organiza��o, ap�s um incidente de seguran�a da informa��o, envolver uma a��o legal (civil ou criminal), conv�m que evid�ncias sejam coletadas, armazenadas e apresentadas em conformidade com as normas de armazenamento de evid�ncias da jurisdi��o(�es) pertinentes(s). ',
    'tImplementationGuide' => 'Conv�m que procedimentos internos sejam elaborados e respeitados para as atividades de coleta e apresenta��o de evid�ncias com o prop�sito de a��o disciplinar movida em uma organiza��o.  Em geral, as normas para evid�ncia abrangem:  a) admissibilidade da evid�ncia: se a evid�ncia pode ser ou n�o utilizada na corte;
b) import�ncia da evid�ncia: qualidade e inteireza da evid�ncia.  Para obter a admissibilidade da evid�ncia, conv�m que a organiza��o assegure que seus sistemas de informa��o estejam de acordo com qualquer norma ou c�digo de pr�tica publicado para produ��o de evid�ncia admiss�vel.  Conv�m que o valor da evid�ncia esteja de acordo com algum requisito aplic�vel. Para obter o valor da evid�ncia, conv�m que a qualidade e a inteireza dos controles usados para proteger as evid�ncias de forma correta e consistente (ou seja, o processo de controle de evid�ncias) durante todo o per�odo de armazenamento e processamento da evid�ncia sejam demonstradas por uma trilha forte de evid�ncia. Em geral, essa trilha forte de evid�ncia pode ser estabelecida sob as seguintes condi��es:  a) para documentos em papel: o original � mantido de forma segura, com um registro da pessoa que o encontrou, do local e data em que foi encontrado e quem testemunhou a descoberta;
conv�m que qualquer investiga��o assegure que os originais n�o foram adulterados;
b) para forma��o em m�dia eletr�nica: conv�m que imagens espelho ou c�pias (dependendo de requisitos aplic�veis) de quaisquer m�dias remov�veis, discos r�gidos ou em mem�rias sejam providenciadas para assegurar disponibilidade;
conv�m que o registro de todas as a��es tomadas durante o processo de c�pia seja guardado e que o processo seja testemunhado;
conv�m que a m�dia original que cont�m a informa��o e o registro (ou, caso isso n�o seja poss�vel, pelo menos uma imagem espelho ou c�pia) seja mantido de forma segura e intoc�vel.  Conv�m que qualquer trabalho forense seja somente realizado em c�pias do material de evid�ncia. Conv�m que a integridade de todo material de evid�ncia seja preservada. Conv�m que o processo de c�pia de todo material de evid�ncia seja supervisionado por pessoas confi�veis e que as informa��es sobre data, local, pessoas, ferramentas e programas envolvidos no processo de c�pia sejam registradas. Informa��es adicionais Quando um evento de seguran�a da informa��o � detectado, pode n�o ser �bvio que ele resultar� num poss�vel processo jur�dico. Entretanto, existe o perigo de que a evid�ncia seja destru�da intencional ou acidentalmente antes que seja percebida a seriedade do incidente. � conveniente envolver um advogado ou a pol�cia t�o logo seja constatada a possibilidade de processo jur�dico e obter consultoria sobre as evid�ncias necess�rias.  As evid�ncias podem ultrapassar limites organizacionais e/ou de jurisdi��es. Nesses casos, conv�m assegurar que a organiza��o seja devidamente autorizada para coletar as informa��es requeridas como evid�ncias. Conv�m que os requisitos de diferentes jurisdi��es sejam tamb�m considerados para maximizar as possibilidades de admiss�o da evid�ncia em todas as jurisdi��es relevantes. ',
  ),
  118 => 
  array (
    'fkContext' => '1918',
    'fkSectionBestPractice' => '1646',
    'nControlType' => '0',
    'sName' => '14.1.1 Incluindo seguran�a da informa��o no processo de gest�o da continuidade do neg�cio',
    'tDescription' => 'Conv�m que um processo de gest�o seja desenvolvido e mantido para assegurar a continuidade do neg�cio por toda a organiza��o e que contemple os requisitos de seguran�a da informa��o necess�rios para a continuidade do neg�cio da organiza��o. ',
    'tImplementationGuide' => 'Conv�m que este processo agregue os seguintes elementos chave da gest�o da continuidade do neg�cio:  a) entendimento dos riscos a que a organiza��o est� exposta, no que diz respeito � sua probabilidade e impacto no tempo, incluindo a identifica��o e prioriza��o dos processos cr�ticos do neg�cio - ver 14.1.2;
b) identifica��o de todos os ativos envolvidos em processos cr�ticos de neg�cio - ver 7.1.1;
c) entendimento do impacto que incidentes de seguran�a da informa��o provavelmente ter�o sobre os neg�cios (� importante que as solu��es encontradas possam tratar tanto os pequenos incidentes, como os mais s�rios, que poderiam colocar em risco a continuidade da organiza��o) e estabelecimento dos objetivos do neg�cio dos recursos de processamento da informa��o;
d) considera��o de contrata��o de seguro compat�vel que possa ser parte integrante do processo de continuidade do neg�cio, bem como a parte de gest�o de risco operacional;
e) identifica��o e considera��o da implementa��o de controles preventivos e de mitiga��o;
f) identifica��o de recursos financeiros, organizacionais, t�cnicos e ambientais suficientes para identificar os requisitos de seguran�a da informa��o;
g) garantia da seguran�a de pessoal e prote��o de recursos de processamento das informa��es e bens organizacionais;
h) detalhamento e documenta��o de planos de continuidade de neg�cio que contemplem os requisitos de seguran�a da informa��o alinhados com a estrat�gia da continuidade do neg�cio estabelecida - ver 14.1.3;
i) testes e atualiza��es regulares dos planos e processos implantados - ver 14.1.5;
j) garantia de que a gest�o da continuidade do neg�cio esteja incorporada aos processos e estrutura da organiza��o. Conv�m que a responsabilidade pela coordena��o do processo de gest�o de continuidade de neg�cios seja atribu�da a um n�vel adequado dentro da organiza��o - ver 6.1.1.',
  ),
  119 => 
  array (
    'fkContext' => '1919',
    'fkSectionBestPractice' => '1646',
    'nControlType' => '0',
    'sName' => '14.1.2 Continuidade de neg�cios e an�lise/avalia��o de riscos',
    'tDescription' => 'Conv�m identificar os eventos que podem causar interrup��es aos processos de neg�cio, junto a probabilidade e impacto de tais interrup��es e as conseq��ncias para a seguran�a da informa��o.  ',
    'tImplementationGuide' => 'Conv�m que os aspectos da continuidade do neg�cios relativos � seguran�a da informa��o sejam baseados na identifica��o de eventos (ou sucess�o de eventos) que possam causar interrup��es aos processos de neg�cios das organiza��es, por exemplo falha de equipamento, erros humanos, roubo, inc�ndio, desastres naturais e atos terroristas. Em seguida, conv�m que seja feita uma an�lise/avalia��o de riscos para a determina��o da probabilidade e impacto de tais interrup��es, tanto em termos de escala de dano quanto em rela��o ao per�odo de recupera��o.  Conv�m que as an�lise/avalia��es de riscos da continuidade do neg�cio sejam realizadas com total envolvimento dos respons�veis pelos processos e recursos do neg�cio. Conv�m que a an�lise/avalia��o considere todos os processos do neg�cio e n�o esteja limitada aos recursos de processamento das informa��es, mas inclua os resultados espec�ficos da seguran�a da informa��o. � importante a jun��o de aspectos de riscos diferentes, para obter um quadro completo dos requisitos de continuidade de neg�cios da organiza��o. Conv�m que a an�lise/avalia��o identifique, quantifique e priorize os crit�rios baseados nos riscos e objetivos pertinentes � organiza��o, incluindo recursos cr�ticos, impactos de interrup��o, possibilidade de aus�ncia de tempo e prioridades de recupera��o.  Em fun��o dos resultados da an�lise/avalia��o de riscos, conv�m que um plano estrat�gico seja desenvolvido para se determinar a abordagem mais abrangente a ser adotada para a continuidade dos neg�cios. Uma vez criada a estrat�gia, conv�m que ela seja validada pela dire��o e que um plano seja elaborado e validado para implementar tal estrat�gia. ',
  ),
  120 => 
  array (
    'fkContext' => '1920',
    'fkSectionBestPractice' => '1646',
    'nControlType' => '0',
    'sName' => '14.1.3 Desenvolvimento e implementa��o de planos de continuidade relativos � seguran�a da informa��o',
    'tDescription' => 'Conv�m que os planos sejam desenvolvidos e implementados para a manuten��o ou recupera��o das opera��es e para assegurar a disponibilidade da informa��o no n�vel requerido e na escala de tempo requerida, ap�s a ocorr�ncia de interrup��es ou falhas dos processos cr�ticos do neg�cio. ',
    'tImplementationGuide' => 'Conv�m que o processo de planejamento da continuidade de neg�cios considere os seguintes itens:  a) identifica��o e concord�ncia de todas as responsabilidades e procedimentos da continuidade do neg�cio;
b) identifica��o da perda aceit�vel de informa��es e servi�os;
c) implementa��o dos procedimentos que permitam a recupera��o e restaura��o das opera��es do neg�cio e da disponibilidade da informa��o nos prazos necess�rios;
aten��o especial precisa ser dada � avalia��o de depend�ncias externas ao neg�cio e de contratos existentes;
d) procedimentos operacionais que permitam a conclus�o de restaura��o e recupera��o que estejam pendentes;
e) documenta��o dos processos e procedimentos acordados;
f) educa��o adequada de pessoas nos procedimentos e processos definidos, incluindo o gerenciamento de crise;
g) teste e atualiza��o dos planos.  Conv�m que o processo de planejamento foque os objetivos requeridos do neg�cio, por exemplo recupera��o de determinados servi�os espec�ficos para os clientes, em um per�odo de tempo aceit�vel. Conv�m identificar os servi�os e recursos que facilitam isso, prevendo a contempla��o de pessoal e recursos em geral, al�m da tecnologia de informa��o, assim como o procedimento de recupera��o dos recursos de processamento das informa��es. Tais procedimentos de recupera��o podem incluir procedimentos com terceiros na forma de um acordo de reciprocidade, ou um contrato de presta��o de servi�os.  Conv�m que o plano de continuidade do neg�cio trate as vulnerabilidades da organiza��o, que pode conter informa��es sens�veis e que necessitem de prote��o adequada. Conv�m que c�pias do plano de continuidade do neg�cio sejam guardadas em um ambiente remoto, a uma dist�ncia suficiente para escapar de qualquer dano de um desastre no local principal. Conv�m que o gestor garanta que as c�pias dos planos de continuidade do neg�cio estejam atualizadas e protegidas no mesmo n�vel de seguran�a como aplicado no ambiente principal. Conv�m que outros materiais necess�rios para a execu��o do plano de continuidade do neg�cio tamb�m sejam armazenados em local remoto.  Conv�m que, se os ambientes alternativos tempor�rios forem usados, o n�vel de controles de seguran�a implementados nestes locais seja equivalente ao ambiente principal. Informa��es adicionais Conv�m que seja destacado que as atividades e os planos de gerenciamento de crise - ver 14.1.3 f) - possam ser diferentes de gest�o de continuidade de neg�cios, isto �, uma crise pode acontecer e ser suprida atrav�s dos procedimentos normais de gest�o.  ',
  ),
  121 => 
  array (
    'fkContext' => '1921',
    'fkSectionBestPractice' => '1646',
    'nControlType' => '0',
    'sName' => '14.1.4 Estrutura do plano de continuidade do neg�cio  ',
    'tDescription' => 'Conv�m que uma estrutura b�sica dos planos de continuidade do neg�cio seja mantida para assegurar que todos os planos s�o consistentes, para contemplar os requisitos de seguran�a da informa��o e para identificar prioridades para testes e manuten��o.  ',
    'tImplementationGuide' => 'Conv�m que cada plano de continuidade do neg�cio descreva o enfoque para continuidade, por exemplo, o enfoque para assegurar a disponibilidade e seguran�a do sistema de informa��o ou da informa��o. Conv�m que cada plano tamb�m especifique o plano de escalonamento e as condi��es para sua ativa��o, assim como as responsabilidades individuais para execu��o de cada uma das atividades do plano. Quando novos requisitos s�o identificados, � importante que os procedimentos de emerg�ncia relacionados sejam ajustados de forma apropriada, por exemplo o plano de abandono ou o procedimento de recupera��o. Conv�m que os procedimentos do programa de gest�o de mudan�a da organiza��o sejam inclu�dos para assegurar que os assuntos de continuidade de neg�cios estejam sempre direcionados adequadamente. Conv�m que cada plano tenha um gestor espec�fico. Conv�m que procedimentos de emerg�ncia, de recupera��o, manual de planejamento e planos de reativa��o sejam responsabilidade dos gestores dos recursos de neg�cios ou dos processos envolvidos. Conv�m que procedimentos de recupera��o para servi�os t�cnicos alternativos, como processamento de informa��o e meios de comunica��o, sejam normalmente de responsabilidade dos provedores de servi�os.  Conv�m que uma estrutura de planejamento para continuidade de neg�cios contemple os requisitos de seguran�a da informa��o identificados e considere os seguintes itens:  a) condi��es para ativa��o dos planos, os quais descrevem os processos a serem seguidos (como se avaliar a situa��o, que deve ser acionado etc.) antes de cada plano ser ativado;
b) procedimentos de emerg�ncia que descrevam as a��es a serem tomadas ap�s a ocorr�ncia de um incidente que coloque em risco as opera��es do neg�cio;
c) procedimentos de recupera��o que descrevam as a��es necess�rias para a transfer�ncia das atividades essenciais do neg�cio ou os servi�os de infra-estrutura para localidades alternativas tempor�rias e para a reativa��o dos processos do neg�cio no prazo necess�rio;
d) procedimentos operacionais tempor�rios para seguir durante a conclus�o de recupera��o e restaura��o;
e) procedimentos de recupera��o que descrevam as a��es a serem adotadas quando do restabelecimento das opera��es;
f) uma programa��o de manuten��o que especifique quando e como o plano dever� ser testado e a forma de se proceder � manuten��o deste plano;
g) atividades de treinamento, conscientiza��o e educa��o com o prop�sito de criar o entendimento do processo da continuidade do neg�cio e de assegurar que os processos continuem a ser efetivo;
h) designa��o das responsabilidades individuais, descrevendo quem � respons�vel pela execu��o de que item do plano. Conv�m que suplentes sejam definidos quando necess�rio;
i) os ativos e recursos cr�ticos precisam estar aptos a desempenhar os procedimentos de emerg�ncia, recupera��o e reativa��o. ',
  ),
  122 => 
  array (
    'fkContext' => '1922',
    'fkSectionBestPractice' => '1646',
    'nControlType' => '0',
    'sName' => '14.1.5 Testes, manuten��o e reavalia��o dos planos de continuidade do neg�cio',
    'tDescription' => 'Conv�m que os planos de continuidade do neg�cio sejam testados e atualizados regularmente, de forma a assegurar sua permanente atualiza��o e efetividade. ',
    'tImplementationGuide' => 'Conv�m que os testes do plano de continuidade do neg�cio assegurem que todos os membros da equipe de recupera��o e outras pessoas relevantes estejam conscientes dos planos e de suas responsabilidades para a continuidade do neg�cio e a seguran�a da informa��o, e conhe�am as suas atividades quando um plano for acionado.  Conv�m que o planejamento e a programa��o dos testes do(s) plano(s) de continuidade de neg�cios indiquem como e quando cada elemento do plano seja testado. Conv�m que os componentes isolados do(s) plano(s) sejam frequentemente testados.  Conv�m que v�rias t�cnicas sejam utilizadas, de modo a assegurar a confian�a de que o(s) plano(s) ir�(�o) operar consistentemente em casos reais. Conv�m que sejam considerados:
a) testes de mesa simulando diferentes cen�rios - verbalizando os procedimentos de recupera��o para diferentes formas de interrup��o;
b) simula��es - particularmente �til para o treinamento do pessoal nas suas atividades gerenciais ap�s o incidente;
c) testes de recupera��o t�cnica - garantindo que os sistemas de informa��o possam ser efetivamente recuperados;
d) testes de recupera��o em um local alternativo - executando os processos de neg�cios em paralelo com a recupera��o das opera��es distantes do local principal;
e) testes dos recursos, servi�os e instala��es de fornecedores - assegurando que os servi�os e produtos fornecidos por terceiros atendem aos requisitos contratados;
f) ensaio geral - testando se a organiza��o, o pessoal, os equipamentos, os recursos e os processos podem enfrentar interrup��es.  Estas t�cnicas podem ser utilizadas por qualquer organiza��o. Conv�m que os resultados dos testes sejam registrados e a��es tomadas pra a melhoria dos planos, onde necess�rio.  Conv�m que a responsabilidade pelas an�lises cr�ticas peri�dicas de cada parte do plano seja definida e estabelecida. Conv�m que a identifica��o de mudan�as nas atividades do neg�cio que ainda n�o tenham sido contempladas nos planos de continuidade de neg�cio seja seguida por uma apropriada atualiza��o do plano. Conv�m que um controle formal de mudan�as assegure que os planos atualizados s�o distribu�dos e refor�ados por an�lises cr�ticas peri�dicas do plano como um todo.  Os exemplos de mudan�as onde conv�m que a atualiza��o dos planos de continuidade do neg�cio seja considerada s�o a aquisi��o de novos equipamentos, atualiza��o de sistema de mudan�as de:  a) pessoal;
b) endere�os ou n�meros telef�nicos;
c) estrat�gia de neg�cio;
d) localiza��o, instala��es e recursos;
e) legisla��o;
f) prestadores de servi�o, fornecedores e clientes-chave;
g) processos - inclus�es e exclus�es;
h) risco - operacional e financeiro.',
  ),
  123 => 
  array (
    'fkContext' => '1923',
    'fkSectionBestPractice' => '1648',
    'nControlType' => '0',
    'sName' => '15.1.1 Identifica��o da legisla��o vigente',
    'tDescription' => 'Conv�m que todos os requisitos estatut�rios, regulamentares e contratuais relevantes, e o enfoque da organiza��o para atender a esses requisitos, sejam explicitamente definidos, documentados e mantidos atualizados para cada sistema de informa��o da organiza��o. ',
    'tImplementationGuide' => 'Conv�m que os controles espec�ficos e as responsabilidades individuais para atender a estes requisitos sejam definidos e documentados de forma similar. ',
  ),
  124 => 
  array (
    'fkContext' => '1924',
    'fkSectionBestPractice' => '1648',
    'nControlType' => '0',
    'sName' => '15.1.2 Direitos de propriedade intelectual',
    'tDescription' => 'Conv�m que procedimentos apropriados sejam implementados para garantir a conformidade com os requisitos legislativos, regulamentares e contratuais no uso de material, em rela��o aos quais pode haver direitos de propriedade intelectual e sobre o uso de produtos de software propriet�rios. ',
    'tImplementationGuide' => 'Conv�m que as seguintes diretrizes sejam consideradas para proteger qualquer material possa ser considerado como propriedade intelectual:  a) divulgar uma pol�tica de conformidade com os direitos de propriedade intelectual que defina o uso legal de produtos de software e de informa��o;
b) adquirir software somente por meio de fontes conhecidas e de reputa��o, para assegurar que o direito autoral n�o est� sendo violado;
c) manter conscientiza��o das pol�ticas para proteger os direitos de propriedade intelectual e notificar a inten��o de tomar a��es disciplinares contra pessoas que violarem essas pol�ticas;
d) manter de forma adequada os registros de ativos e identificar todos os ativos com requisitos para proteger os direitos de propriedade intelectual;
e) manter provas e evid�ncias da propriedade de licen�as, discos-mestre, manuais, etc.;
f) implementar controles para assegurar que o n�mero m�ximo de usu�rios permitidos n�o excede o n�mero de licen�as adquiridas;
g) conduzir verifica��es para que somente produtos de software autorizados e licenciados sejam instalados;
h) estabelecer uma pol�tica para a manuten��o das condi��es adequadas de licen�as;
i) estabelecer uma pol�tica para disposi��o ou transfer�ncia de software para outros;
j) utilizar ferramentas de auditoria apropriadas;
k) cumprir termos e condi��es para software e informa��o obtidos a partir de redes p�blicas;
l) n�o duplicar, converter para outro formato ou extrair de registros comerciais (filme, �udio) outros que n�o os permitidos pela lei de direito autoral;
m) n�o copiar, no todo ou em partes, livros, artigos, relat�rios ou outros documentos, al�m daqueles permitidos pela lei de direito autoral. Informa��es adicionais Direitos de propriedade intelectual incluem direitos de software ou documento, direitos de projeto, marcas, patentes e licen�as de c�digo-fonte.  Produtos de software propriet�rios s�o normalmente fornecidos sob um contrato de licenciamento que especifica os termos e condi��es da licen�a, por exemplo, restringe o uso dos produtos em m�quinas especificadas ou limita a c�pia apenas para cria��o e uma c�pia de seguran�a. A quest�o relativa aos direitos de propriedade intelectual de software desenvolvido pela organiza��o precisa ser esclarecida com as pessoas.  Legisla��o, regulamenta��o e requisitos contratuais podem estabelecer restri��es para c�pia de material que tenha direitos autorais. Em particular, pode ser requerido que somente material que seja desenvolvido pela organiza��o ou que foi licenciado ou fornecido pelos desenvolvedores para a organiza��o seja utilizado. Viola��es aos direitos de propriedade intelectual podem conduzir a a��es legais, que podem envolver processos criminais.  ',
  ),
  125 => 
  array (
    'fkContext' => '1925',
    'fkSectionBestPractice' => '1648',
    'nControlType' => '0',
    'sName' => '15.1.3 Prote��o de registros organizacionais',
    'tDescription' => 'Conv�m que registros importantes sejam protegidos contra perda, destrui��o e falsifica��o, de acordo com os requisitos regulamentares, estatut�rios, contratuais e do neg�cio. ',
    'tImplementationGuide' => 'Conv�m que registros sejam categorizados em tipos de registros, tais como registros cont�beis, registros de base de dados, registros de auditoria e procedimentos operacionais, cada qual com detalhes do per�odo de reten��o e do tipo de m�dia de armazenamento, como, por exemplo, papel, microficha, meio magn�tico ou �tico. Conv�m que quaisquer chaves de criptografia relacionadas com arquivos cifrados ou assinaturas digitais - ver 12.3 - sejam tamb�m armazenadas para permitir a decifra��o de registros pelo per�odo de tempo que os registros s�o mantidos.  Conv�m que cuidados sejam tomados a respeito da possibilidade de deteriora��o das m�dias usadas no armazenamento dos registros. Conv�m que os procedimentos de armazenamento e manuseio sejam implementados de acordo com as recomenda��es dos fabricantes. Conv�m que, para o armazenamento de longo tempo, o uso de papel e microficha seja considerado.  Onde m�dias eletr�nicas armazenadas forem escolhidas, conv�m que sejam inclu�dos procedimentos para assegurar a capacidade de acesso aos dados (leitura tanto na m�dia como no formato utilizado) durante o per�odo de reten��o, para proteger contra perdas ocasionadas pelas futuras mudan�as na tecnologia.  Conv�m que sistemas de armazenamento de dados sejam escolhidos de modo que o dado solicitado possa ser recuperado de forma aceit�vel, dependendo dos requisitos a serem atendidos.  Conv�m que o sistema de armazenamento e manuseio assegure a clara identifica��o dos registros e dos seus per�odos de reten��o, conforme definido pela legisla��o nacional ou regional ou por regulamenta��es, se aplic�vel. Conv�m que seja permitida a destrui��o apropriada dos registros ap�s esse per�odo, caso n�o sejam mais necess�rios � organiza��o.  Para atender aos objetivos de prote��o dos registros, conv�m que os seguintes passos sejam tomados dentro da organiza��o:  a) emitir diretrizes gerais para reten��o, armazenamento, tratamento e disposi��o de registros e informa��es;
b) elaborar uma programa��o para reten��o, identificando os registros essenciais e o per�odo que cada uma deve ser mantido;
c) manter um investimento das fontes de informa��es-chave;
d) implementar controles apropriados para proteger registros e informa��es contra perda, destrui��o e falsifica��o. Informa��es adicionais Alguns registros podem precisar ser retidos de forma segura para atender a requisitos estatut�rios, contratuais ou regulamentares, assim como para apoiar as atividades essenciais do neg�cio. Exemplo disso s�o os registros que podem ser exigidos como evid�ncia de que uma organiza��o opera de acordo com as regras estatut�rias  e regulamentares, para assegurar a defesa adequada contra potenciais processos civis ou criminais ou confirmar a situa��o financeira de uma organiza��o perante os acionistas, partes externas e auditores. O per�odo de tempo e o conte�do da informa��o retida podem estar definidos atrav�s de leis ou regulamenta��es nacionais.  Outras informa��es sobre como gerenciar os registros organizacionais, podem ser encontradas na ISO-15489-1. ',
  ),
  126 => 
  array (
    'fkContext' => '1926',
    'fkSectionBestPractice' => '1648',
    'nControlType' => '0',
    'sName' => '15.1.4 Prote��o de dados e privacidade de informa��es pessoais',
    'tDescription' => 'Conv�m que a privacidade e prote��o de dados sejam asseguradas conforme exigido nas legisla��es relevantes, regulamenta��es e, se aplic�vel, nas cl�usulas contratuais. ',
    'tImplementationGuide' => 'Conv�m que uma pol�tica de privacidade e prote��o de dados da organiza��o seja desenvolvida e implementada. Conv�m que esta pol�tica seja comunicada a todas as pessoas envolvidas no processamento de informa��es pessoais.  A conformidade com esta pol�tica e todas as legisla��es e regulamenta��es relevantes de prote��o de dados necessita de uma estrutura de gest�o e de controles apropriados. Geralmente isto � melhor alcan�ado atrav�s de uma pessoa respons�vel, como, por exemplo, um gestor de prote��o de dados, que deve fornecer orienta��es gerais para gerentes, usu�rios e provedores de servi�o sobre as responsabilidades de cada um e sobre quais procedimentos espec�ficos recomenda-se seguir. Conv�m que a responsabilidade pelo tratamento das informa��es pessoais e a garantia de conscientiza��o dos princ�pios de prote��o dos dados sejam tratadas de acordo com as legisla��es e regulamenta��es relevantes. Conv�m que medidas organizacionais e t�cnicas apropriadas para proteger as informa��es pessoais sejam implementadas. Informa��es adicionais Alguns pa�ses tem promulgado leis que estabelecem controles na coleta, no processamento e na transmiss�o de dados pessoais (geralmente informa��o sobre indiv�duos vivos que podem ser identificados a partir de tais informa��es). Dependendo da respectiva legisla��o nacional, tais controles podem impor responsabilidades sobre aqueles que coletam, processam e disseminam informa��o pessoal, e podem restringir a capacidade de transfer�ncia desses dados para outros pa�ses. ',
  ),
  127 => 
  array (
    'fkContext' => '1927',
    'fkSectionBestPractice' => '1648',
    'nControlType' => '0',
    'sName' => '15.1.5 Preven��o de mau uso de recursos de processamento da informa��o',
    'tDescription' => 'Conv�m que os usu�rios sejam dissuadidos de usar os recursos de processamento da informa��o para prop�sito n�o autorizados ',
    'tImplementationGuide' => 'Conv�m que a dire��o aprove o uso de recursos de processamento da informa��o. Conv�m que qualquer uso destes recursos para prop�sitos n�o relacionados ao neg�cio ou n�o autorizados, sem a aprova��o da dire��o (ver 6.1.4), ou para quaisquer prop�sitos n�o autorizados, seja considerado como uso impr�prio desses recursos. Se qualquer atividade n�o autorizada for identificada por processo de monitora��o ou outros meios, conv�m que esta atividade seja levada ao conhecimento do gestor respons�vel para que sejam aplicadas as a��es disciplinares e/ou legais pertinentes.  Conv�m que se busque uma assessoria legal antes da implementa��o dos procedimentos de monitora��o.  Conv�m que todos os usu�rios estejam conscientes do escopo preciso de suas permiss�es de acesso e da monitora��o realizada para detectar o uso n�o autorizado. Isto pode ser alcan�ado pelo registro das autoriza��es dos usu�rios por escrito, conv�m que a c�pia seja assinada pelo usu�rio e armazenada de forma segura pela organiza��o. Conv�m que os funcion�rios de uma organiza��o, fornecedores e terceiros sejam informados de que nenhum acesso � permitido com exce��o daqueles que foram autorizados.  No momento da conex�o inicial, conv�m que seja apresentada uma mensagem de advert�ncia para indicar que o recurso de processamento da informa��o que est� sendo usado � de propriedade da organiza��o e que n�o s�o permitidos acessos n�o autorizados. O usu�rio tem que confirmar e reagir adequadamente � mensagem na tela para continuar com o processo de conex�o - ver 11.5.1. Informa��es adicionais Os recursos de processamento da informa��o de uma organiza��o s�o destinados b�sica ou exclusivamente para atender aos prop�sitos do neg�cio.  Ferramentas do tipo detec��o de intrusos, inspe��o de conte�do e outras formas de monitora��o podem ajudar a prevenir e detectar o mau uso dos recursos de processamento da informa��o.  Muitos pa�ses t�m a legisla��o para proteger contra o mau uso do computador. Pode ser crime usar um computador para prop�sitos n�o autorizados.  A legalidade do processo de monitora��o do uso do computador varia de pa�s para pa�s e pode requerer que a dire��o avise a todos os usu�rios dessa monitora��o e/ou que concordem formalmente com este processo. Quando o sistema estiver sendo usado para acesso p�blico (por exemplo, um servidor p�blico web) e sujeito a uma monitora��o de seguran�a, conv�m que uma mensagem seja exibida na tela informando deste processo. ',
  ),
  128 => 
  array (
    'fkContext' => '1928',
    'fkSectionBestPractice' => '1648',
    'nControlType' => '0',
    'sName' => '15.1.6 Regulamenta��o de controles de criptografia  ',
    'tDescription' => 'Conv�m que controles de criptografia sejam usados em conformidade com todas as leis, acordos e regulamenta��o relevantes ',
    'tImplementationGuide' => 'Conv�m que os seguintes itens sejam considerados para conformidade com leis, acordos e regulamenta��es relevantes:  a) restri��es � importa��o e/ou exporta��o de hardware e software de computador para execu��o de fun��es criptogr�ficas;
b) restri��es � importa��o e/ou exporta��o de hardware e software de computador que foi projetado para ter fun��es criptogr�ficas embutidas;
c) restri��es no uso de criptografia;
d) m�todos mandat�rios ou discricion�rios de acesso pelas autoridades dos pa�ses � informa��o cifrada por hardware ou software para fornecer confidencialidade ao conte�do.  Conv�m que assessoria jur�dica seja obtida para garantir a conformidade com as legisla��es e leis nacionais vigentes. Tamb�m conv�m que seja obtida assessoria jur�dica antes de se transferirem informa��es cifradas ou controles de criptografia para outros pa�ses. ',
  ),
  129 => 
  array (
    'fkContext' => '1929',
    'fkSectionBestPractice' => '1649',
    'nControlType' => '0',
    'sName' => '15.2.1 Conformidade com as pol�ticas e normas de seguran�a da informa��o',
    'tDescription' => 'Conv�m que gestores garantam que todos os procedimentos de seguran�a da informa��o dentro da sua �rea de responsabilidade est�o sendo executados corretamente para atender � conformidade com as normas e pol�ticas de seguran�a da informa��o. ',
    'tImplementationGuide' => 'Conv�m que os gestores analisem criticamente, a intervalos regulares, a conformidade do processamento da informa��o dentro da sua �rea de responsabilidade com as pol�ticas de seguran�a da informa��o, normas e quaisquer outros requisitos de seguran�a.  Se qualquer n�o-conformidade for encontrada como um resultado da an�lise cr�tica, conv�m que os gestores:  a) determinem as causas da n�o conformidade;
b) avaliem a necessidade de a��es para assegurar que a n�o-conformidade n�o se repita;
c) determinem e implementem a��o corretiva apropriada;
d) analisem criticamente a a��o corretiva tomada.  Conv�m que os resultados das an�lises cr�ticas e das a��es corretivas realizadas pelos gestores sejam registrados e que esses registros sejam mantidos. Conv�m que os gestores relatem os resultados para as pessoas que est�o realizando a an�lise cr�tica independente (ver 6.1.8), quando a an�lise cr�tica independente for realizada na �rea de sua responsabilidade. Informa��es adicionais A monitora��o operacional de sistemas em uso � apresentada em 10.10. ',
  ),
  130 => 
  array (
    'fkContext' => '1930',
    'fkSectionBestPractice' => '1649',
    'nControlType' => '0',
    'sName' => '15.2.2 Verifica��o da conformidade t�cnica',
    'tDescription' => 'Conv�m que sistemas de informa��o sejam periodicamente verificados em sua conformidade com as normas de seguran�a da informa��o implementadas. ',
    'tImplementationGuide' => 'Conv�m que a verifica��o de conformidade t�cnica seja executada manualmente (auxiliada por ferramentas de software apropriadas, se necess�rio) por um engenheiro de sistemas experiente, e/ou com a assist�ncia de ferramentas automatizadas que gerem relat�rio t�cnico para interpreta��o subseq�ente por um t�cnico especialista.  Se o teste de invas�o ou avalia��es de vulnerabilidades forem usados, conv�m que sejam tomadas precau��es, uma vez que tais atividades podem conduzir a um comprometimento da seguran�a do sistema. Conv�m que tais testes sejam planejados, documentados e repetidos.  Conv�m que qualquer verifica��o de conformidade t�cnica somente seja executada por pessoas autorizadas e competentes, ou sob a supervis�o de tais pessoas. Informa��es adicionais A verifica��o da conformidade t�cnica envolve a an�lise dos sistemas operacionais para garantir que controles de hardware e software foram corretamente implementados. Este tipo de verifica��o de conformidade requer a assist�ncia de t�cnicos especializados.  A verifica��o de conformidade tamb�m engloba, por exemplo, testes de invas�o e avalia��es de vulnerabilidades, que podem ser executados por especialistas independentes contratados especificamente para este fim. Isto pode ser �til na detec��o de vulnerabilidades do sistema e na verifica��o do quanto os controles s�o eficientes na preven��o de acessos n�o autorizados devido a estas vulnerabilidades.  Testes de invas�o e avalia��o de vulnerabilidades fornece um snapshot de um sistema em um est�gio espec�fico para um tempo espec�fico. O snapshot est� limitado para aquelas partes do sistema realmente testadas durante a etapa da invas�o. O teste de invas�o e as avalia��es de vulnerabilidades n�o s�o um substituto da an�lise/avalia��o de riscos. ',
  ),
  131 => 
  array (
    'fkContext' => '1931',
    'fkSectionBestPractice' => '1650',
    'nControlType' => '0',
    'sName' => '15.3.1 Controles de auditoria de sistemas de informa��o',
    'tDescription' => 'Controle  Conv�m que requisitos e atividades de auditoria envolvendo verifica��o nos sistemas operacionais sejam cuidadosamente planejados e acordados para minimizar os riscos de interrup��o dos processos do neg�cio. ',
    'tImplementationGuide' => 'Conv�m que as seguintes diretrizes sejam observadas:  a) requisitos de auditoria sejam acordados com o n�vel apropriado da administra��o;
b) escopo da verifica��o seja acordado e controlado;
c) a verifica��o seja limitada ao acesso somente para leitura de software e dados;
d) outros acessos diferentes de apenas leitura sejam permitidos somente atrav�s de c�pias isoladas dos arquivos do sistema, e sejam apagados ao final da auditoria, ou dada prote��o apropriada quando existir uma obriga��o para guardar tais arquivos como requisitos da documenta��o da auditoria;
e) recursos para execu��o da verifica��o sejam identificados explicitamente e tornados dispon�veis;
f) requisitos para processamento adicional ou especial sejam identificados e acordados;
g) todo acesso seja monitorado e registrado de forma a produzir uma trilha de refer�ncia;
conv�m que o uso de trilhas de refer�ncia (time stamped) seja considerado para os sistemas ou dados cr�ticos;
h) todos os procedimentos, requisitos e responsabilidades sejam documentados;
i) as pessoas que executem a auditoria sejam independentes das atividades auditadas. ',
  ),
  132 => 
  array (
    'fkContext' => '1932',
    'fkSectionBestPractice' => '1650',
    'nControlType' => '0',
    'sName' => '15.3.2 Prote��o de ferramentas de auditoria de sistemas de informa��o',
    'tDescription' => 'Conv�m que o acesso �s ferramentas de auditoria de sistema de informa��o seja protegido, para prevenir qualquer possibilidade de uso impr�prio ou comprometimento. ',
    'tImplementationGuide' => 'Conv�m que acessos �s ferramentas de auditoria de sistemas de informa��o, por exemplo, software ou arquivos de dados, sejam separados de sistemas em desenvolvimento e em opera��o e n�o sejam mantidos em fitas de biblioteca ou �reas de usu�rios, a menos que seja dado um n�vel apropriado e prote��o adicional. Informa��es adicionais Quando terceiros est�o envolvidos em uma auditoria, existe um risco de mau uso de ferramentas de auditoria por esses terceiros e da informa��o que est� sendo acessada por este terceiro. Controles, tais como em 6.2.1 (para avaliar riscos) e 9.1.2 (para restringir acesso f�sico), podem ser considerados para contemplar este risco, e conv�m que quaisquer conseq��ncias, tais como trocar imediatamente as senhas reveladas para os auditores, sejam tomadas.',
  ),
  133 => 
  array (
    'fkContext' => '1934',
    'fkSectionBestPractice' => '1653',
    'nControlType' => '0',
    'sName' => 'PO1.1 IT Value Management',
    'tDescription' => 'Work with the business to ensure that the enterprise portfolio of IT-enabled investments contains programmes that have solid business cases. Recognise that there are mandatory, sustaining and discretionary investments that differ in complexity and degree of freedom in allocating funds. IT processes should provide effective and efficient delivery of the IT components of programmes and early warning of any deviations from plan, including cost, schedule or functionality, that might impact the expected outcomes of the programmes. IT services should be executed against equitable and enforceable service level agreements (SLAs). Accountability for achieving the benefits and controlling the costs should be clearly assigned and monitored. Establish fair, transparent, repeatable and comparable evaluation of business cases, including financial worth, the risk of not delivering a capability and the risk of not realising the expected benefits.',
    'tImplementationGuide' => '',
  ),
  134 => 
  array (
    'fkContext' => '1935',
    'fkSectionBestPractice' => '1653',
    'nControlType' => '0',
    'sName' => 'PO1.2 Business-IT Alignment',
    'tDescription' => 'Establish processes of bi-directional education and reciprocal involvement in strategic planning to achieve business and IT alignment and integration. Mediate between business and IT imperatives so priorities can be mutually agreed.',
    'tImplementationGuide' => '',
  ),
  135 => 
  array (
    'fkContext' => '1936',
    'fkSectionBestPractice' => '1653',
    'nControlType' => '0',
    'sName' => 'PO1.3 Assessment of Current Capability and Performance',
    'tDescription' => 'Assess the current capability and performance of solution and service delivery to establish a baseline against which future requirements can be compared. Define performance in terms of IT\'\'s contribution to business objectives, functionality, stability, complexity, costs, strengths and weaknesses.',
    'tImplementationGuide' => '',
  ),
  136 => 
  array (
    'fkContext' => '1937',
    'fkSectionBestPractice' => '1653',
    'nControlType' => '0',
    'sName' => 'PO1.4 IT Strategic Plan',
    'tDescription' => 'Create a strategic plan that defines, in co-operation with relevant stakeholders, how IT goals will contribute to the enterprise\'\'s strategic objectives and related costs and risks. It should include how IT will support IT-enabled investment programmes, IT services and IT assets. IT should define how the objectives will be met, the measurements to be used and the procedures to obtain formal sign-off from the stakeholders. The IT strategic plan should cover investment/operational budget, funding sources, sourcing strategy, acquisition strategy, and legal and regulatory requirements. The strategic plan should be sufficiently detailed to allow for the definition of tactical IT plans.',
    'tImplementationGuide' => '',
  ),
  137 => 
  array (
    'fkContext' => '1938',
    'fkSectionBestPractice' => '1653',
    'nControlType' => '0',
    'sName' => 'PO1.5 IT Tactical Plans',
    'tDescription' => 'Create a portfolio of tactical IT plans that are derived from the IT strategic plan. The tactical plans should address IT-enabled programme investments, IT services and IT assets. The tactical plans should describe required IT initiatives, resource requirements, and how the use of resources and achievement of benefits will be monitored and managed. The tactical plans should be sufficiently detailed to allow the definition of project plans. Actively manage the set of tactical IT plans and initiatives through analysis of project and service portfolios.',
    'tImplementationGuide' => '',
  ),
  138 => 
  array (
    'fkContext' => '1939',
    'fkSectionBestPractice' => '1653',
    'nControlType' => '0',
    'sName' => 'PO1.6 IT Portfolio Management',
    'tDescription' => 'Actively manage with the business the portfolio of IT-enabled investment programmes required to achieve specific strategic business objectives by identifying, defining, evaluating, prioritising, selecting, initiating, managing and controlling programmes. This should include clarifying desired business outcomes, ensuring that programme objectives support achievement of the outcomes, understanding the full scope of effort required to achieve the outcomes, assigning clear accountability with supporting measures, defining projects within the programme, allocating resources and funding, delegating authority, and commissioning required projects at programme launch.',
    'tImplementationGuide' => '',
  ),
  139 => 
  array (
    'fkContext' => '1940',
    'fkSectionBestPractice' => '1654',
    'nControlType' => '0',
    'sName' => 'PO2.1 Enterprise Information Architecture Model',
    'tDescription' => 'Establish and maintain an enterprise information model to enable applications development and decision-supporting activities, consistent with IT plans as described in PO1. The model should facilitate the optimal creation, use and sharing of information by the business in a way that maintains integrity and is flexible, functional, cost-effective, timely, secure and resilient to failure.',
    'tImplementationGuide' => '',
  ),
  140 => 
  array (
    'fkContext' => '1941',
    'fkSectionBestPractice' => '1654',
    'nControlType' => '0',
    'sName' => 'PO2.2 Enterprise Data Dictionary and Data Syntax Rules',
    'tDescription' => 'Maintain an enterprise data dictionary that incorporates the organisation\'\'s data syntax rules. This dictionary should enable the sharing of data elements amongst applications and systems, promote a common understanding of data amongst IT and business users, and prevent incompatible data elements from being created.',
    'tImplementationGuide' => '',
  ),
  141 => 
  array (
    'fkContext' => '1942',
    'fkSectionBestPractice' => '1654',
    'nControlType' => '0',
    'sName' => 'PO2.3 Data Classification Scheme',
    'tDescription' => 'Establish a classification scheme that applies throughout the enterprise, based on the criticality and sensitivity (e.g., public, confidential, top secret) of enterprise data. This scheme should include details about data ownership;
definition of appropriate security levels and protection controls;
and a brief description of data retention and destruction requirements, criticality and sensitivity. It should be used as the basis for applying controls such as access controls, archiving or encryption.',
    'tImplementationGuide' => '',
  ),
  142 => 
  array (
    'fkContext' => '1943',
    'fkSectionBestPractice' => '1654',
    'nControlType' => '0',
    'sName' => 'PO2.4 Integrity Management',
    'tDescription' => 'Define and implement procedures to ensure the integrity and consistency of all data stored in electronic form, such as databases, data warehouses and data archives.',
    'tImplementationGuide' => '',
  ),
  143 => 
  array (
    'fkContext' => '1944',
    'fkSectionBestPractice' => '1655',
    'nControlType' => '0',
    'sName' => 'PO3.1 Technological Direction Planning',
    'tDescription' => 'Analyse existing and emerging technologies, and plan which technological direction is appropriate to realise the IT strategy and the business systems architecture. Also identify in the plan which technologies have the potential to create business opportunities. The plan should address systems architecture, technological direction, migration strategies and contingency aspects of infrastructure components.',
    'tImplementationGuide' => '',
  ),
  144 => 
  array (
    'fkContext' => '1945',
    'fkSectionBestPractice' => '1655',
    'nControlType' => '0',
    'sName' => 'PO3.2 Technology Infrastructure Plan',
    'tDescription' => 'Create and maintain a technology infrastructure plan that is in accordance with the IT strategic and tactical plans. The plan should be based on the technological direction and include contingency arrangements and direction for acquisition of technology resources. It should consider changes in the competitive environment, economies of scale for information systems staffing and investments, and improved interoperability of platforms and applications.',
    'tImplementationGuide' => '',
  ),
  145 => 
  array (
    'fkContext' => '1946',
    'fkSectionBestPractice' => '1655',
    'nControlType' => '0',
    'sName' => 'PO3.3 Monitor Future Trends and Regulations',
    'tDescription' => 'Establish a process to monitor the business sector, industry, technology, infrastructure, legal and regulatory environment trends. Incorporate the consequences of these trends into the development of the IT technology infrastructure plan.',
    'tImplementationGuide' => '',
  ),
  146 => 
  array (
    'fkContext' => '1947',
    'fkSectionBestPractice' => '1655',
    'nControlType' => '0',
    'sName' => 'PO3.4 Technology Standards',
    'tDescription' => 'To provide consistent, effective and secure technological solutions enterprisewide, establish a technology forum to provide technology guidelines, advice on infrastructure products and guidance on the selection of technology, and measure compliance with these standards and guidelines. This forum should direct technology standards and practices based on their business relevance, risks and compliance with external requirements.',
    'tImplementationGuide' => '',
  ),
  147 => 
  array (
    'fkContext' => '1948',
    'fkSectionBestPractice' => '1655',
    'nControlType' => '0',
    'sName' => 'PO3.5 IT Architecture Board',
    'tDescription' => 'Establish an IT architecture board to provide architecture guidelines and advice on their application, and to verify compliance. This entity should direct IT architecture design, ensuring that it enables the business strategy and considers regulatory compliance and continuity requirements. This is related/linked to PO2 Define the information architecture.',
    'tImplementationGuide' => '',
  ),
  148 => 
  array (
    'fkContext' => '1949',
    'fkSectionBestPractice' => '1656',
    'nControlType' => '0',
    'sName' => 'PO4.1 IT Process Framework',
    'tDescription' => 'Define an IT process framework to execute the IT strategic plan. This framework should include an IT process structure and relationships (e.g., to manage process gaps and overlaps), ownership, maturity, performance measurement, improvement, compliance, quality targets and plans to achieve them. It should provide integration amongst the processes that are specific to IT, enterprise portfolio management, business processes and business change processes. The IT process framework should be integrated into a quality management system (QMS) and the internal control framework.',
    'tImplementationGuide' => '',
  ),
  149 => 
  array (
    'fkContext' => '1950',
    'fkSectionBestPractice' => '1656',
    'nControlType' => '0',
    'sName' => 'PO4.2 IT Strategy Committee',
    'tDescription' => 'Establish an IT strategy committee at the board level. This committee should ensure that IT governance, as part of enterprise governance, is adequately addressed;
advise on strategic direction;
and review major investments on behalf of the full board.',
    'tImplementationGuide' => '',
  ),
  150 => 
  array (
    'fkContext' => '1951',
    'fkSectionBestPractice' => '1656',
    'nControlType' => '0',
    'sName' => 'PO4.3 IT Steering Committee',
    'tDescription' => 'Establish an IT steering committee (or equivalent) composed of executive, business and IT management to: - Determine prioritisation of IT-enabled investment programmes in line with the enterprise\'\'s business strategy and priorities - Track status of projects and resolve resource conflict - Monitor service levels and service improvements',
    'tImplementationGuide' => '',
  ),
  151 => 
  array (
    'fkContext' => '1952',
    'fkSectionBestPractice' => '1656',
    'nControlType' => '0',
    'sName' => 'PO4.4 Organisational Placement of the IT Function',
    'tDescription' => 'Place the IT function in the overall organisational structure with a business model contingent on the importance of IT within the enterprise, specifically its criticality to business strategy and the level of operational dependence on IT. The reporting line of the CIO should be commensurate with the importance of IT within the enterprise.',
    'tImplementationGuide' => '',
  ),
  152 => 
  array (
    'fkContext' => '1953',
    'fkSectionBestPractice' => '1656',
    'nControlType' => '0',
    'sName' => 'PO4.5 IT Organisational Structure',
    'tDescription' => 'Establish an internal and external IT organisational structure that reflects business needs. In addition, put a process in place for periodically reviewing the IT organisational structure to adjust staffing requirements and sourcing strategies to meet expected business objectives and changing circumstances.',
    'tImplementationGuide' => '',
  ),
  153 => 
  array (
    'fkContext' => '1954',
    'fkSectionBestPractice' => '1656',
    'nControlType' => '0',
    'sName' => 'PO4.6 Establishment of Roles and Responsibilities',
    'tDescription' => 'Establish and communicate roles and responsibilities for IT personnel and end users that delineate between IT personnel and end-user authority, responsibilities and accountability for meeting the organisation\'\'s needs.',
    'tImplementationGuide' => '',
  ),
  154 => 
  array (
    'fkContext' => '1955',
    'fkSectionBestPractice' => '1656',
    'nControlType' => '0',
    'sName' => 'PO4.7 Responsibility for IT Quality Assurance',
    'tDescription' => 'Assign responsibility for the performance of the quality assurance (QA) function and provide the QA group with appropriate QA systems, controls and communications expertise. Ensure that the organisational placement and the responsibilities and size of the QA group satisfy the requirements of the organisation.',
    'tImplementationGuide' => '',
  ),
  155 => 
  array (
    'fkContext' => '1956',
    'fkSectionBestPractice' => '1656',
    'nControlType' => '0',
    'sName' => 'PO4.8 Responsibility for Risk, Security and Compliance',
    'tDescription' => 'Embed ownership and responsibility for IT-related risks within the business at an appropriate senior level. Define and assign roles critical for managing IT risks, including the specific responsibility for information security, physical security and compliance. Establish risk and security management responsibility at the enterprise level to deal with organisationwide issues. Additional security management responsibilities may need to be assigned at a system-specific level to deal with related security issues. Obtain direction from senior management on the appetite for IT risk and approval of any residual IT risks.',
    'tImplementationGuide' => '',
  ),
  156 => 
  array (
    'fkContext' => '1957',
    'fkSectionBestPractice' => '1656',
    'nControlType' => '0',
    'sName' => 'PO4.9 Data and System Ownership',
    'tDescription' => 'Provide the business with procedures and tools, enabling it to address its responsibilities for ownership of data and information systems. Owners should make decisions about classifying information and systems and protecting them in line with this classification.',
    'tImplementationGuide' => '',
  ),
  157 => 
  array (
    'fkContext' => '1958',
    'fkSectionBestPractice' => '1656',
    'nControlType' => '0',
    'sName' => 'PO4.10 Supervision',
    'tDescription' => 'Implement adequate supervisory practices in the IT function to ensure that roles and responsibilities are properly exercised, to assess whether all personnel have sufficient authority and resources to execute their roles and responsibilities, and to generally review KPIs.',
    'tImplementationGuide' => '',
  ),
  158 => 
  array (
    'fkContext' => '1959',
    'fkSectionBestPractice' => '1656',
    'nControlType' => '0',
    'sName' => 'PO4.11 Segregation of Duties',
    'tDescription' => 'Implement a division of roles and responsibilities that reduces the possibility for a single individual to compromise a critical process. Make sure that personnel are performing only authorised duties relevant to their respective jobs and positions.',
    'tImplementationGuide' => '',
  ),
  159 => 
  array (
    'fkContext' => '1960',
    'fkSectionBestPractice' => '1656',
    'nControlType' => '0',
    'sName' => 'PO4.12 IT Staffing',
    'tDescription' => 'Evaluate staffing requirements on a regular basis or upon major changes to the business, operational or IT environments to ensure that the IT function has sufficient resources to adequately and appropriately support the business goals and objectives.',
    'tImplementationGuide' => '',
  ),
  160 => 
  array (
    'fkContext' => '1961',
    'fkSectionBestPractice' => '1656',
    'nControlType' => '0',
    'sName' => 'PO4.13 Key IT Personnel',
    'tDescription' => 'Define and identify key IT personnel (e.g., replacements/backup personnel), and minimise reliance on a single individual performing a critical job function.',
    'tImplementationGuide' => '',
  ),
  161 => 
  array (
    'fkContext' => '1962',
    'fkSectionBestPractice' => '1656',
    'nControlType' => '0',
    'sName' => 'PO4.14 Contracted Staff Policies and Procedures',
    'tDescription' => 'Ensure that consultants and contract personnel who support the IT function know and comply with the organisation\'\'s policies for the protection of the organisation\'\'s information assets such that they meet agreed-upon contractual requirements.',
    'tImplementationGuide' => '',
  ),
  162 => 
  array (
    'fkContext' => '1963',
    'fkSectionBestPractice' => '1656',
    'nControlType' => '0',
    'sName' => 'PO4.15 Relationships',
    'tDescription' => 'Establish and maintain an optimal co-ordination, communication and liaison structure between the IT function and various other interests inside and outside the IT function, such as the board, executives, business units, individual users, suppliers, security officers, risk managers, the corporate compliance group, outsourcers and offsite management.',
    'tImplementationGuide' => '',
  ),
  163 => 
  array (
    'fkContext' => '1964',
    'fkSectionBestPractice' => '1657',
    'nControlType' => '0',
    'sName' => 'PO5.1 Financial Management Framework',
    'tDescription' => 'Establish and maintain a financial framework to manage the investment and cost of IT assets and services through portfolios of Itenabled investments, business cases and IT budgets.',
    'tImplementationGuide' => '',
  ),
  164 => 
  array (
    'fkContext' => '1965',
    'fkSectionBestPractice' => '1657',
    'nControlType' => '0',
    'sName' => 'PO5.2 Prioritisation Within IT Budget',
    'tDescription' => 'Implement a decision-making process to prioritise the allocation of IT resources for operations, projects and maintenance to maximise IT\'\'s contribution to optimising the return on the enterprise\'\'s portfolio of IT-enabled investment programmes and other IT services and assets.',
    'tImplementationGuide' => '',
  ),
  165 => 
  array (
    'fkContext' => '1966',
    'fkSectionBestPractice' => '1657',
    'nControlType' => '0',
    'sName' => 'PO5.3 IT Budgeting',
    'tDescription' => 'Establish and implement practices to prepare a budget reflecting the priorities established by the enterprise\'\'s portfolio of IT-enabled investment programmes, and including the ongoing costs of operating and maintaining the current infrastructure. The practices should support development of an overall IT budget as well as development of budgets for individual programmes, with specific emphasis on the IT components of those programmes. The practices should allow for ongoing review, refinement and approval of the overall budget and the budgets for individual programmes.',
    'tImplementationGuide' => '',
  ),
  166 => 
  array (
    'fkContext' => '1967',
    'fkSectionBestPractice' => '1657',
    'nControlType' => '0',
    'sName' => 'PO5.4 Cost Management',
    'tDescription' => 'Implement a cost management process comparing actual costs to budgets. Costs should be monitored and reported. Where there are deviations, these should be identified in a timely manner and the impact of those deviations on programmes should be assessed. Together with the business sponsor of those programmes, appropriate remedial action should be taken and, if necessary, the programme business case should be updated.',
    'tImplementationGuide' => '',
  ),
  167 => 
  array (
    'fkContext' => '1968',
    'fkSectionBestPractice' => '1657',
    'nControlType' => '0',
    'sName' => 'PO5.5 Benefit Management',
    'tDescription' => 'Implement a process to monitor the benefits from providing and maintaining appropriate IT capabilities. IT\'\'s contribution to the business, either as a component of IT-enabled investment programmes or as part of regular operational support, should be identified and documented in a business case, agreed to, monitored and reported. Reports should be reviewed and, where there are opportunities to improve IT\'\'s contribution, appropriate actions should be defined and taken. Where changes in IT\'\'s contribution impact the programme, or where changes to other related projects impact the programme, the programme business case should be updated.',
    'tImplementationGuide' => '',
  ),
  168 => 
  array (
    'fkContext' => '1969',
    'fkSectionBestPractice' => '1658',
    'nControlType' => '0',
    'sName' => 'PO6.1 IT Policy and Control Environment',
    'tDescription' => 'Define the elements of a control environment for IT, aligned with the enterprise\'\'s management philosophy and operating style. These elements should include expectations/requirements regarding delivery of value from IT investments, appetite for risk, integrity, ethical values, staff competence, accountability and responsibility. The control environment should be based on a culture that supports value delivery whilst managing significant risks, encourages cross-divisional co-operation and teamwork, promotes compliance and continuous process improvement, and handles process deviations (including failure) well.',
    'tImplementationGuide' => '',
  ),
  169 => 
  array (
    'fkContext' => '1970',
    'fkSectionBestPractice' => '1658',
    'nControlType' => '0',
    'sName' => 'PO6.2 Enterprise IT Risk and Control Framework',
    'tDescription' => 'Develop and maintain a framework that defines the enterprise\'\'s overall approach to IT risk and control and that aligns with the IT policy and control environment and the enterprise risk and control framework.',
    'tImplementationGuide' => '',
  ),
  170 => 
  array (
    'fkContext' => '1971',
    'fkSectionBestPractice' => '1658',
    'nControlType' => '0',
    'sName' => 'PO6.3 IT Policies Management',
    'tDescription' => 'Develop and maintain a set of policies to support IT strategy. These policies should include policy intent;
roles and responsibilities;
exception process;
compliance approach;
and references to procedures, standards and guidelines. Their relevance should be confirmed and approved regularly.',
    'tImplementationGuide' => '',
  ),
  171 => 
  array (
    'fkContext' => '1972',
    'fkSectionBestPractice' => '1658',
    'nControlType' => '0',
    'sName' => 'PO6.4 Policy, Standard and Procedures Rollout',
    'tDescription' => 'Roll out and enforce IT policies to all relevant staff, so they are built into and are an integral part of enterprise operations.',
    'tImplementationGuide' => '',
  ),
  172 => 
  array (
    'fkContext' => '1973',
    'fkSectionBestPractice' => '1658',
    'nControlType' => '0',
    'sName' => 'PO6.5 Communication of IT Objectives and Direction',
    'tDescription' => 'Communicate awareness and understanding of business and IT objectives and direction to appropriate stakeholders and users throughout the enterprise.',
    'tImplementationGuide' => '',
  ),
  173 => 
  array (
    'fkContext' => '1974',
    'fkSectionBestPractice' => '1659',
    'nControlType' => '0',
    'sName' => 'PO7.1 Personnel Recruitment and Retention',
    'tDescription' => 'Maintain IT personnel recruitment processes in line with the overall organisation\'\'s personnel policies and procedures (e.g., hiring, positive work environment, orienting). Implement processes to ensure that the organisation has an appropriately deployed IT workforce with the skills necessary to achieve organisational goals.',
    'tImplementationGuide' => '',
  ),
  174 => 
  array (
    'fkContext' => '1975',
    'fkSectionBestPractice' => '1659',
    'nControlType' => '0',
    'sName' => 'PO7.2 Personnel Competencies',
    'tDescription' => 'Regularly verify that personnel have the competencies to fulfil their roles on the basis of their education, training and/or experience. Define core IT competency requirements and verify that they are being maintained, using qualification and certification programmes where appropriate.',
    'tImplementationGuide' => '',
  ),
  175 => 
  array (
    'fkContext' => '1976',
    'fkSectionBestPractice' => '1659',
    'nControlType' => '0',
    'sName' => 'PO7.3 Staffing of Roles',
    'tDescription' => 'Define, monitor and supervise roles, responsibilities and compensation frameworks for personnel, including the requirement to adhere to management policies and procedures, the code of ethics, and professional practices. The level of supervision should be in line with the sensitivity of the position and extent of responsibilities assigned.',
    'tImplementationGuide' => '',
  ),
  176 => 
  array (
    'fkContext' => '1977',
    'fkSectionBestPractice' => '1659',
    'nControlType' => '0',
    'sName' => 'PO7.4 Personnel Training',
    'tDescription' => 'Provide IT employees with appropriate orientation when hired and ongoing training to maintain their knowledge, skills, abilities, internal controls and security awareness at the level required to achieve organisational goals.',
    'tImplementationGuide' => '',
  ),
  177 => 
  array (
    'fkContext' => '1978',
    'fkSectionBestPractice' => '1659',
    'nControlType' => '0',
    'sName' => 'PO7.5 Dependence Upon Individuals',
    'tDescription' => 'Minimise the exposure to critical dependency on key individuals through knowledge capture (documentation), knowledge sharing, succession planning and staff backup.',
    'tImplementationGuide' => '',
  ),
  178 => 
  array (
    'fkContext' => '1979',
    'fkSectionBestPractice' => '1659',
    'nControlType' => '0',
    'sName' => 'PO7.6 Personnel Clearance Procedures',
    'tDescription' => 'Include background checks in the IT recruitment process. The extent and frequency of periodic reviews of these checks should depend on the sensitivity and/or criticality of the function and should be applied for employees, contractors and vendors.',
    'tImplementationGuide' => '',
  ),
  179 => 
  array (
    'fkContext' => '1980',
    'fkSectionBestPractice' => '1659',
    'nControlType' => '0',
    'sName' => 'PO7.7 Employee Job Performance Evaluation',
    'tDescription' => 'Require a timely evaluation to be performed on a regular basis against individual objectives derived from the organisation\'\'s goals, established standards and specific job responsibilities. Employees should receive coaching on performance and conduct whenever appropriate.',
    'tImplementationGuide' => '',
  ),
  180 => 
  array (
    'fkContext' => '1981',
    'fkSectionBestPractice' => '1659',
    'nControlType' => '0',
    'sName' => 'PO7.8 Job Change and Termination',
    'tDescription' => 'Take expedient actions regarding job changes, especially job terminations. Knowledge transfer should be arranged, responsibilities reassigned and access rights removed such that risks are minimised and continuity of the function is guaranteed.',
    'tImplementationGuide' => '',
  ),
  181 => 
  array (
    'fkContext' => '1982',
    'fkSectionBestPractice' => '1660',
    'nControlType' => '0',
    'sName' => 'PO8.1 Quality Management System',
    'tDescription' => 'Establish and maintain a QMS that provides a standard, formal and continuous approach regarding quality management that is aligned with business requirements. The QMS should identify quality requirements and criteria;
key IT processes and their sequence and interaction;
and the policies, criteria and methods for defining, detecting, correcting and preventing non-conformity. The QMS should define the organisational structure for quality management, covering the roles, tasks and responsibilities. All key areas should develop their quality plans in line with criteria and policies and record quality data. Monitor and measure the effectiveness and acceptance of the QMS, and improve it when needed.',
    'tImplementationGuide' => '',
  ),
  182 => 
  array (
    'fkContext' => '1983',
    'fkSectionBestPractice' => '1660',
    'nControlType' => '0',
    'sName' => 'PO8.2 IT Standards and Quality Practices',
    'tDescription' => 'Identify and maintain standards, procedures and practices for key IT processes to guide the organisation in meeting the intent of the QMS. Use industry good practices for reference when improving and tailoring the organisation\'\'s quality practices.',
    'tImplementationGuide' => '',
  ),
  183 => 
  array (
    'fkContext' => '1984',
    'fkSectionBestPractice' => '1660',
    'nControlType' => '0',
    'sName' => 'PO8.3 Development and Acquisition Standards',
    'tDescription' => 'Adopt and maintain standards for all development and acquisition that follow the life cycle of the ultimate deliverable, and include sign-off at key milestones based on agreed-upon sign-off criteria. Consider software coding standards;
naming conventions;
file formats;
schema and data dictionary design standards;
user interface standards;
interoperability;
system performance efficiency;
scalability;
standards for development and testing;
validation against requirements;
test plans;
and unit, regression and integration testing.',
    'tImplementationGuide' => '',
  ),
  184 => 
  array (
    'fkContext' => '1985',
    'fkSectionBestPractice' => '1660',
    'nControlType' => '0',
    'sName' => 'PO8.4 Customer Focus',
    'tDescription' => 'Focus quality management on customers by determining their requirements and aligning them to the IT standards and practices. Define roles and responsibilities concerning conflict resolution between the user/customer and the IT organisation.',
    'tImplementationGuide' => '',
  ),
  185 => 
  array (
    'fkContext' => '1986',
    'fkSectionBestPractice' => '1660',
    'nControlType' => '0',
    'sName' => 'PO8.5 Continuous Improvement',
    'tDescription' => 'Maintain and regularly communicate an overall quality plan that promotes continuous improvement.',
    'tImplementationGuide' => '',
  ),
  186 => 
  array (
    'fkContext' => '1987',
    'fkSectionBestPractice' => '1660',
    'nControlType' => '0',
    'sName' => 'PO8.6 Quality Measurement, Monitoring and Review',
    'tDescription' => 'Define, plan and implement measurements to monitor continuing compliance to the QMS, as well as the value the QMS provides. Measurement, monitoring and recording of information should be used by the process owner to take appropriate corrective and preventive actions.',
    'tImplementationGuide' => '',
  ),
  187 => 
  array (
    'fkContext' => '1988',
    'fkSectionBestPractice' => '1661',
    'nControlType' => '0',
    'sName' => 'PO9.1 IT Risk Management Framework',
    'tDescription' => 'Establish an IT risk management framework that is aligned to the organisation\'\'s (enterprise\'\'s) risk management framework.',
    'tImplementationGuide' => '',
  ),
  188 => 
  array (
    'fkContext' => '1989',
    'fkSectionBestPractice' => '1661',
    'nControlType' => '0',
    'sName' => 'PO9.2 Establishment of Risk Context',
    'tDescription' => 'Establish the context in which the risk assessment framework is applied to ensure appropriate outcomes. This should include determining the internal and external context of each risk assessment, the goal of the assessment, and the criteria against which risks are evaluated.',
    'tImplementationGuide' => '',
  ),
  189 => 
  array (
    'fkContext' => '1990',
    'fkSectionBestPractice' => '1661',
    'nControlType' => '0',
    'sName' => 'PO9.3 Event Identification',
    'tDescription' => 'Identify events (an important realistic threat that exploits a significant applicable vulnerability) with a potential negative impact on the goals or operations of the enterprise, including business, regulatory, legal, technology, trading partner, human resources and operational aspects. Determine the nature of the impact and maintain this information. Record and maintain relevant risks in a risk registry.',
    'tImplementationGuide' => '',
  ),
  190 => 
  array (
    'fkContext' => '1991',
    'fkSectionBestPractice' => '1661',
    'nControlType' => '0',
    'sName' => 'PO9.4 Risk Assessment',
    'tDescription' => 'Assess on a recurrent basis the likelihood and impact of all identified risks, using qualitative and quantitative methods. The likelihood and impact associated with inherent and residual risk should be determined individually, by category and on a portfolio basis.',
    'tImplementationGuide' => '',
  ),
  191 => 
  array (
    'fkContext' => '1992',
    'fkSectionBestPractice' => '1661',
    'nControlType' => '0',
    'sName' => 'PO9.5 Risk Response',
    'tDescription' => 'Develop and maintain a risk response process designed to ensure that cost-effective controls mitigate exposure to risks on a continuing basis. The risk response process should identify risk strategies such as avoidance, reduction, sharing or acceptance;
determine associated responsibilities;
and consider risk tolerance levels.',
    'tImplementationGuide' => '',
  ),
  192 => 
  array (
    'fkContext' => '1993',
    'fkSectionBestPractice' => '1661',
    'nControlType' => '0',
    'sName' => 'PO9.6 Maintenance and Monitoring of a Risk Action Plan',
    'tDescription' => 'Prioritise and plan the control activities at all levels to implement the risk responses identified as necessary, including identification of costs, benefits and responsibility for execution. Obtain approval for recommended actions and acceptance of any residual risks, and ensure that committed actions are owned by the affected process owner(s). Monitor execution of the plans, and report on any deviations to senior management.',
    'tImplementationGuide' => '',
  ),
  193 => 
  array (
    'fkContext' => '1994',
    'fkSectionBestPractice' => '1662',
    'nControlType' => '0',
    'sName' => 'PO10.1 Programme Management Framework',
    'tDescription' => 'Maintain the programme of projects, related to the portfolio of IT-enabled investment programmes, by identifying, defining, evaluating, prioritising, selecting, initiating, managing and controlling projects. Ensure that the projects support the programme\'\'s objectives. Co-ordinate the activities and interdependencies of multiple projects, manage the contribution of all the projects within the programme to expected outcomes, and resolve resource requirements and conflicts.',
    'tImplementationGuide' => '',
  ),
  194 => 
  array (
    'fkContext' => '1995',
    'fkSectionBestPractice' => '1662',
    'nControlType' => '0',
    'sName' => 'PO10.2 Project Management Framework',
    'tDescription' => 'Establish and maintain a project management framework that defines the scope and boundaries of managing projects, as well as the method to be adopted and applied to each project undertaken. The framework and supporting method should be integrated with the programme management processes.',
    'tImplementationGuide' => '',
  ),
  195 => 
  array (
    'fkContext' => '1996',
    'fkSectionBestPractice' => '1662',
    'nControlType' => '0',
    'sName' => 'PO10.3 Project Management Approach',
    'tDescription' => 'Establish a project management approach commensurate with the size, complexity and regulatory requirements of each project. The project governance structure can include the roles, responsibilities and accountabilities of the programme sponsor, project sponsors, steering committee, project office and project manager, and the mechanisms through which they can meet those responsibilities (such as reporting and stage reviews). Make sure all IT projects have sponsors with sufficient authority to own the execution of the project within the overall strategic programme.',
    'tImplementationGuide' => '',
  ),
  196 => 
  array (
    'fkContext' => '1997',
    'fkSectionBestPractice' => '1662',
    'nControlType' => '0',
    'sName' => 'PO10.4 Stakeholder Commitment',
    'tDescription' => 'Obtain commitment and participation from the affected stakeholders in the definition and execution of the project within the context of the overall IT-enabled investment programme.',
    'tImplementationGuide' => '',
  ),
  197 => 
  array (
    'fkContext' => '1998',
    'fkSectionBestPractice' => '1662',
    'nControlType' => '0',
    'sName' => 'PO10.5 Project Scope Statement',
    'tDescription' => 'Define and document the nature and scope of the project to confirm and develop amongst stakeholders a common understanding of project scope and how it relates to other projects within the overall IT-enabled investment programme. The definition should be formally approved by the programme and project sponsors before project initiation.',
    'tImplementationGuide' => '',
  ),
  198 => 
  array (
    'fkContext' => '1999',
    'fkSectionBestPractice' => '1662',
    'nControlType' => '0',
    'sName' => 'PO10.6 Project Phase Initiation',
    'tDescription' => 'Approve the initiation of each major project phase and communicate it to all stakeholders. Base the approval of the initial phase on programme governance decisions. Approval of subsequent phases should be based on review and acceptance of the deliverables of the previous phase, and approval of an updated business case at the next major review of the programme. In the event of overlapping project phases, an approval point should be established by programme and project sponsors to authorise project progression.',
    'tImplementationGuide' => '',
  ),
  199 => 
  array (
    'fkContext' => '2000',
    'fkSectionBestPractice' => '1662',
    'nControlType' => '0',
    'sName' => 'PO10.7 Integrated Project Plan',
    'tDescription' => 'Establish a formal, approved integrated project plan (covering business and information systems resources) to guide project execution and control throughout the life of the project. The activities and interdependencies of multiple projects within a programme should be understood and documented. The project plan should be maintained throughout the life of the project. The project plan, and changes to it, should be approved in line with the programme and project governance framework.',
    'tImplementationGuide' => '',
  ),
  200 => 
  array (
    'fkContext' => '2001',
    'fkSectionBestPractice' => '1662',
    'nControlType' => '0',
    'sName' => 'PO10.8 Project Resources',
    'tDescription' => 'Define the responsibilities, relationships, authorities and performance criteria of project team members, and specify the basis for acquiring and assigning competent staff members and/or contractors to the project. The procurement of products and services required for each project should be planned and managed to achieve project objectives using the organisation\'\'s procurement practices.',
    'tImplementationGuide' => '',
  ),
  201 => 
  array (
    'fkContext' => '2002',
    'fkSectionBestPractice' => '1662',
    'nControlType' => '0',
    'sName' => 'PO10.9 Project Risk Management',
    'tDescription' => 'Eliminate or minimise specific risks associated with individual projects through a systematic process of planning, identifying, analysing, responding to, monitoring and controlling the areas or events that have the potential to cause unwanted change. Risks faced by the project management process and the project deliverable should be established and centrally recorded.',
    'tImplementationGuide' => '',
  ),
  202 => 
  array (
    'fkContext' => '2003',
    'fkSectionBestPractice' => '1662',
    'nControlType' => '0',
    'sName' => 'PO10.10 Project Quality Plan',
    'tDescription' => 'Prepare a quality management plan that describes the project quality system and how it will be implemented. The plan should be formally reviewed and agreed to by all parties concerned and then incorporated into the integrated project plan.',
    'tImplementationGuide' => '',
  ),
  203 => 
  array (
    'fkContext' => '2004',
    'fkSectionBestPractice' => '1662',
    'nControlType' => '0',
    'sName' => 'PO10.11 Project Change Control',
    'tDescription' => 'Establish a change control system for each project, so all changes to the project baseline (e.g., cost, schedule, scope, quality) are appropriately reviewed, approved and incorporated into the integrated project plan in line with the programme and project governance framework.',
    'tImplementationGuide' => '',
  ),
  204 => 
  array (
    'fkContext' => '2005',
    'fkSectionBestPractice' => '1662',
    'nControlType' => '0',
    'sName' => 'PO10.12 Project Planning of Assurance Methods',
    'tDescription' => 'Identify assurance tasks required to support the accreditation of new or modified systems during project planning, and include them in the integrated project plan. The tasks should provide assurance that internal controls and security features meet the defined requirements.',
    'tImplementationGuide' => '',
  ),
  205 => 
  array (
    'fkContext' => '2006',
    'fkSectionBestPractice' => '1662',
    'nControlType' => '0',
    'sName' => 'PO10.13 Project Performance Measurement, Reporting and Monitoring',
    'tDescription' => 'Measure project performance against key project performance scope, schedule, quality, cost and risk criteria. Identify any deviations from the plan. Assess the impact of deviations on the project and overall programme, and report results to key stakeholders. Recommend, implement and monitor remedial action, when required, in line with the programme and project governance framework.',
    'tImplementationGuide' => '',
  ),
  206 => 
  array (
    'fkContext' => '2007',
    'fkSectionBestPractice' => '1662',
    'nControlType' => '0',
    'sName' => 'PO10.14 Project Closure',
    'tDescription' => 'Require that, at the end of each project, the project stakeholders ascertain whether the project delivered the planned results and benefits. Identify and communicate any outstanding activities required to achieve the planned results of the project and the benefits of the programme, and identify and document lessons learned for use on future projects and programmes.',
    'tImplementationGuide' => '',
  ),
  207 => 
  array (
    'fkContext' => '2008',
    'fkSectionBestPractice' => '1664',
    'nControlType' => '0',
    'sName' => 'AI1.1 Definition and Maintenance of Business Functional and Technical Requirements',
    'tDescription' => 'Identify, prioritise, specify and agree on business functional and technical requirements covering the full scope of all initiatives required to achieve the expected outcomes of the IT-enabled investment programme.',
    'tImplementationGuide' => '',
  ),
  208 => 
  array (
    'fkContext' => '2009',
    'fkSectionBestPractice' => '1664',
    'nControlType' => '0',
    'sName' => 'AI1.2 Risk Analysis Report',
    'tDescription' => 'Identify, document and analyse risks associated with the business requirements and solution design as part of the organisation\'\'s process for the development of requirements.',
    'tImplementationGuide' => '',
  ),
  209 => 
  array (
    'fkContext' => '2010',
    'fkSectionBestPractice' => '1664',
    'nControlType' => '0',
    'sName' => 'AI1.3 Feasibility Study and Formulation of Alternative Courses of Action',
    'tDescription' => 'Develop a feasibility study that examines the possibility of implementing the requirements. Business management, supported by the IT function, should assess the feasibility and alternative courses of action and make a recommendation to the business sponsor.',
    'tImplementationGuide' => '',
  ),
  210 => 
  array (
    'fkContext' => '2011',
    'fkSectionBestPractice' => '1664',
    'nControlType' => '0',
    'sName' => 'AI1.4 Requirements and Feasibility Decision and Approval',
    'tDescription' => 'Verify that the process requires the business sponsor to approve and sign off on business functional and technical requirements and feasibility study reports at predetermined key stages. The business sponsor should make the final decision with respect to the choice of solution and acquisition approach.',
    'tImplementationGuide' => '',
  ),
  211 => 
  array (
    'fkContext' => '2012',
    'fkSectionBestPractice' => '1665',
    'nControlType' => '0',
    'sName' => 'AI2.1 High-level Design',
    'tDescription' => 'Translate business requirements into a high-level design specification for software acquisition, taking into account the organisation\'\'s technological direction and information architecture. Have the design specifications approved by management to ensure that the high-level design responds to the requirements. Reassess when significant technical or logical discrepancies occur during development or maintenance.',
    'tImplementationGuide' => '',
  ),
  212 => 
  array (
    'fkContext' => '2013',
    'fkSectionBestPractice' => '1665',
    'nControlType' => '0',
    'sName' => 'AI2.2 Detailed Design',
    'tDescription' => 'Prepare detailed design and technical software application requirements. Define the criteria for acceptance of the requirements. Have the requirements approved to ensure that they correspond to the high-level design. Perform reassessment when significant technical or logical discrepancies occur during development or maintenance.',
    'tImplementationGuide' => '',
  ),
  213 => 
  array (
    'fkContext' => '2014',
    'fkSectionBestPractice' => '1665',
    'nControlType' => '0',
    'sName' => 'AI2.3 Application Control and Auditability',
    'tDescription' => 'Implement business controls, where appropriate, into automated application controls such that processing is accurate, complete, timely, authorised and auditable.',
    'tImplementationGuide' => '',
  ),
  214 => 
  array (
    'fkContext' => '2015',
    'fkSectionBestPractice' => '1665',
    'nControlType' => '0',
    'sName' => 'AI2.4 Application Security and Availability',
    'tDescription' => 'Address application security and availability requirements in response to identified risks and in line with the organisation\'\'s data classification, information architecture, information security architecture and risk tolerance.',
    'tImplementationGuide' => '',
  ),
  215 => 
  array (
    'fkContext' => '2016',
    'fkSectionBestPractice' => '1665',
    'nControlType' => '0',
    'sName' => 'AI2.5 Configuration and Implementation of Acquired Application Software',
    'tDescription' => 'Configure and implement acquired application software to meet business objectives.',
    'tImplementationGuide' => '',
  ),
  216 => 
  array (
    'fkContext' => '2017',
    'fkSectionBestPractice' => '1665',
    'nControlType' => '0',
    'sName' => 'AI2.6 Major Upgrades to Existing Systems',
    'tDescription' => 'In the event of major changes to existing systems that result in significant change in current designs and/or functionality, follow a similar development process as that used for the development of new systems.',
    'tImplementationGuide' => '',
  ),
  217 => 
  array (
    'fkContext' => '2018',
    'fkSectionBestPractice' => '1665',
    'nControlType' => '0',
    'sName' => 'AI2.7 Development of Application Software',
    'tDescription' => 'Ensure that automated functionality is developed in accordance with design specifications, development and documentation standards, QA requirements, and approval standards. Ensure that all legal and contractual aspects are identified and addressed for application software developed by third parties.',
    'tImplementationGuide' => '',
  ),
  218 => 
  array (
    'fkContext' => '2019',
    'fkSectionBestPractice' => '1665',
    'nControlType' => '0',
    'sName' => 'AI2.8 Software Quality Assurance',
    'tDescription' => 'Develop, resource and execute a software QA plan to obtain the quality specified in the requirements definition and the organisation\'\'s quality policies and procedures.',
    'tImplementationGuide' => '',
  ),
  219 => 
  array (
    'fkContext' => '2020',
    'fkSectionBestPractice' => '1665',
    'nControlType' => '0',
    'sName' => 'AI2.9 Applications Requirements Management',
    'tDescription' => 'Track the status of individual requirements (including all rejected requirements) during the design, development and implementation, and approve changes to requirements through an established change management process.',
    'tImplementationGuide' => '',
  ),
  220 => 
  array (
    'fkContext' => '2021',
    'fkSectionBestPractice' => '1665',
    'nControlType' => '0',
    'sName' => 'AI2.10 Application Software Maintenance',
    'tDescription' => 'Develop a strategy and plan for the maintenance of software applications.',
    'tImplementationGuide' => '',
  ),
  221 => 
  array (
    'fkContext' => '2022',
    'fkSectionBestPractice' => '1666',
    'nControlType' => '0',
    'sName' => 'AI3.1 Technological Infrastructure Acquisition Plan',
    'tDescription' => 'Produce a plan for the acquisition, implementation and maintenance of the technological infrastructure that meets established business functional and technical requirements and is in accord with the organisation\'\'s technology direction.',
    'tImplementationGuide' => '',
  ),
  222 => 
  array (
    'fkContext' => '2023',
    'fkSectionBestPractice' => '1666',
    'nControlType' => '0',
    'sName' => 'AI3.2 Infrastructure Resource Protection and Availability',
    'tDescription' => 'rastructural software to protect resources and ensure availability and integrity. Responsibilities for using sensitive infrastructure components should be clearly defined and understood by those who develop and integrate infrastructure components. Their use should be monitored and evaluated.',
    'tImplementationGuide' => '',
  ),
  223 => 
  array (
    'fkContext' => '2024',
    'fkSectionBestPractice' => '1666',
    'nControlType' => '0',
    'sName' => 'AI3.3 Infrastructure Maintenance',
    'tDescription' => 'Develop a strategy and plan for infrastructure maintenance, and ensure that changes are controlled in line with the organisation\'\'s change management procedure. Include periodic reviews against business needs, patch management, upgrade strategies, risks, vulnerabilities assessment and security requirements.',
    'tImplementationGuide' => '',
  ),
  224 => 
  array (
    'fkContext' => '2025',
    'fkSectionBestPractice' => '1666',
    'nControlType' => '0',
    'sName' => 'AI3.4 Feasibility Test Environment',
    'tDescription' => 'Establish development and test environments to support effective and efficient feasibility and integration testing of infrastructure components.',
    'tImplementationGuide' => '',
  ),
  225 => 
  array (
    'fkContext' => '2026',
    'fkSectionBestPractice' => '1667',
    'nControlType' => '0',
    'sName' => 'AI4.1 Planning for Operational Solutions',
    'tDescription' => 'Develop a plan to identify and document all technical, operational and usage aspects such that all those who will operate, use and maintain the automated solutions can exercise their responsibility.',
    'tImplementationGuide' => '',
  ),
  226 => 
  array (
    'fkContext' => '2027',
    'fkSectionBestPractice' => '1667',
    'nControlType' => '0',
    'sName' => 'AI4.2 Knowledge Transfer to Business Management',
    'tDescription' => 'Transfer knowledge to business management to allow those individuals to take ownership of the system and data, and exercise responsibility for service delivery and quality, internal control, and application administration.',
    'tImplementationGuide' => '',
  ),
  227 => 
  array (
    'fkContext' => '2028',
    'fkSectionBestPractice' => '1667',
    'nControlType' => '0',
    'sName' => 'AI4.3 Knowledge Transfer to End Users',
    'tDescription' => 'Transfer knowledge and skills to allow end users to effectively and efficiently use the system in support of business processes.',
    'tImplementationGuide' => '',
  ),
  228 => 
  array (
    'fkContext' => '2029',
    'fkSectionBestPractice' => '1667',
    'nControlType' => '0',
    'sName' => 'AI4.4 Knowledge Transfer to Operations and Support Staff',
    'tDescription' => 'Transfer knowledge and skills to enable operations and technical support staff to effectively and efficiently deliver, support and maintain the system and associated infrastructure.',
    'tImplementationGuide' => '',
  ),
  229 => 
  array (
    'fkContext' => '2030',
    'fkSectionBestPractice' => '1668',
    'nControlType' => '0',
    'sName' => 'AI5.1 Procurement Control',
    'tDescription' => 'Develop and follow a set of procedures and standards that is consistent with the business organisation\'\'s overall procurement process and acquisition strategy to acquire IT-related infrastructure, facilities, hardware, software and services needed by the business.',
    'tImplementationGuide' => '',
  ),
  230 => 
  array (
    'fkContext' => '2031',
    'fkSectionBestPractice' => '1668',
    'nControlType' => '0',
    'sName' => 'AI5.2 Supplier Contract Management',
    'tDescription' => 'Set up a procedure for establishing, modifying and terminating contracts for all suppliers. The procedure should cover, at a minimum, legal, financial, organisational, documentary, performance, security, intellectual property, and termination responsibilities and liabilities (including penalty clauses). All contracts and contract changes should be reviewed by legal advisors.',
    'tImplementationGuide' => '',
  ),
  231 => 
  array (
    'fkContext' => '2032',
    'fkSectionBestPractice' => '1668',
    'nControlType' => '0',
    'sName' => 'AI5.3 Supplier Selection',
    'tDescription' => 'Select suppliers according to a fair and formal practice to ensure a viable best fit based on specified requirements. Requirements should be optimised with input from potential suppliers.',
    'tImplementationGuide' => '',
  ),
  232 => 
  array (
    'fkContext' => '2033',
    'fkSectionBestPractice' => '1668',
    'nControlType' => '0',
    'sName' => 'AI5.4 IT Resources Acquisition',
    'tDescription' => 'Protect and enforce the organisation\'\'s interests in all acquisition contractual agreements, including the rights and obligations of all parties in the contractual terms for the acquisition of software, development resources, infrastructure and services.',
    'tImplementationGuide' => '',
  ),
  233 => 
  array (
    'fkContext' => '2034',
    'fkSectionBestPractice' => '1669',
    'nControlType' => '0',
    'sName' => 'AI6.1 Change Standards and Procedures',
    'tDescription' => 'Set up formal change management procedures to handle in a standardised manner all requests (including maintenance and patches) for changes to applications, procedures, processes, system and service parameters, and the underlying platforms.',
    'tImplementationGuide' => '',
  ),
  234 => 
  array (
    'fkContext' => '2035',
    'fkSectionBestPractice' => '1669',
    'nControlType' => '0',
    'sName' => 'AI6.2 Impact Assessment, Prioritisation and Authorisation',
    'tDescription' => 'Assess all requests for change in a structured way to determine the impact on the operational system and its functionality. Ensure that changes are categorised, prioritised and authorised.',
    'tImplementationGuide' => '',
  ),
  235 => 
  array (
    'fkContext' => '2036',
    'fkSectionBestPractice' => '1669',
    'nControlType' => '0',
    'sName' => 'AI6.3 Emergency Changes',
    'tDescription' => 'Establish a process for defining, raising, testing, documenting, assessing and authorising emergency changes that do not follow the established change process.',
    'tImplementationGuide' => '',
  ),
  236 => 
  array (
    'fkContext' => '2037',
    'fkSectionBestPractice' => '1669',
    'nControlType' => '0',
    'sName' => 'AI6.4 Change Status Tracking and Reporting',
    'tDescription' => 'Establish a tracking and reporting system to document rejected changes, communicate the status of approved and in-process changes, and complete changes. Make certain that approved changes are implemented as planned.',
    'tImplementationGuide' => '',
  ),
  237 => 
  array (
    'fkContext' => '2038',
    'fkSectionBestPractice' => '1669',
    'nControlType' => '0',
    'sName' => 'AI6.5 Change Closure and Documentation',
    'tDescription' => 'Whenever changes are implemented, update the associated system and user documentation and procedures accordingly.',
    'tImplementationGuide' => '',
  ),
  238 => 
  array (
    'fkContext' => '2039',
    'fkSectionBestPractice' => '1670',
    'nControlType' => '0',
    'sName' => 'AI7.1 Training',
    'tDescription' => 'Train the staff members of the affected user departments and the operations group of the IT function in accordance with the defined training and implementation plan and associated materials, as part of every information systems development, implementation or modification project.',
    'tImplementationGuide' => '',
  ),
  239 => 
  array (
    'fkContext' => '2040',
    'fkSectionBestPractice' => '1670',
    'nControlType' => '0',
    'sName' => 'AI7.2 Test Plan',
    'tDescription' => 'Establish a test plan based on organisationwide standards that defines roles, responsibilities, and entry and exit criteria. Ensure that the plan is approved by relevant parties.',
    'tImplementationGuide' => '',
  ),
  240 => 
  array (
    'fkContext' => '2041',
    'fkSectionBestPractice' => '1670',
    'nControlType' => '0',
    'sName' => 'AI7.3 Implementation Plan',
    'tDescription' => 'Establish an implementation and fallback/backout plan. Obtain approval from relevant parties.',
    'tImplementationGuide' => '',
  ),
  241 => 
  array (
    'fkContext' => '2042',
    'fkSectionBestPractice' => '1670',
    'nControlType' => '0',
    'sName' => 'AI7.4 Test Environment',
    'tDescription' => 'Define and establish a secure test environment representative of the planned operations environment relative to security, internal controls, operational practices, data quality and privacy requirements, and workloads.',
    'tImplementationGuide' => '',
  ),
  242 => 
  array (
    'fkContext' => '2043',
    'fkSectionBestPractice' => '1670',
    'nControlType' => '0',
    'sName' => 'AI7.5 System and Data Conversion',
    'tDescription' => 'Plan data conversion and infrastructure migration as part of the organisation\'\'s development methods, including audit trails, rollbacks and fallbacks.',
    'tImplementationGuide' => '',
  ),
  243 => 
  array (
    'fkContext' => '2044',
    'fkSectionBestPractice' => '1670',
    'nControlType' => '0',
    'sName' => 'AI7.6 Testing of Changes',
    'tDescription' => 'Test changes independently in accordance with the defined test plan prior to migration to the operational environment. Ensure that the plan considers security and performance.',
    'tImplementationGuide' => '',
  ),
  244 => 
  array (
    'fkContext' => '2045',
    'fkSectionBestPractice' => '1670',
    'nControlType' => '0',
    'sName' => 'AI7.7 Final Acceptance Test',
    'tDescription' => 'Ensure that business process owners and IT stakeholders evaluate the outcome of the testing process as determined by the test plan. Remediate significant errors identified in the testing process, having completed the suite of tests identified in the test plan and any necessary regression tests. Following evaluation, approve promotion to production.',
    'tImplementationGuide' => '',
  ),
  245 => 
  array (
    'fkContext' => '2046',
    'fkSectionBestPractice' => '1670',
    'nControlType' => '0',
    'sName' => 'AI7.8 Promotion to Production',
    'tDescription' => 'Following testing, control the handover of the changed system to operations, keeping it in line with the implementation plan. Obtain approval of the key stakeholders, such as users, system owner and operational management. Where appropriate, run the system in parallel with the old system for a while, and compare behaviour and results.',
    'tImplementationGuide' => '',
  ),
  246 => 
  array (
    'fkContext' => '2047',
    'fkSectionBestPractice' => '1670',
    'nControlType' => '0',
    'sName' => 'AI7.9 Post-implementation Review',
    'tDescription' => 'Establish procedures in line with the organisational change management standards to require a post-implementation review as set out in the implementation plan.',
    'tImplementationGuide' => '',
  ),
  247 => 
  array (
    'fkContext' => '2048',
    'fkSectionBestPractice' => '1672',
    'nControlType' => '0',
    'sName' => 'DS1.1 Service Level Management Framework',
    'tDescription' => 'Define a framework that provides a formalised service level management process between the customer and service provider. The framework should maintain continuous alignment with business requirements and priorities and facilitate common understanding between the customer and provider(s). The framework should include processes for creating service requirements, service definitions, SLAs, OLAs and funding sources. These attributes should be organised in a service catalogue. The framework should define the organisational structure for service level management, covering the roles, tasks and responsibilities of internal and external service providers and customers.',
    'tImplementationGuide' => '',
  ),
  248 => 
  array (
    'fkContext' => '2049',
    'fkSectionBestPractice' => '1672',
    'nControlType' => '0',
    'sName' => 'DS1.2 Definition of Services',
    'tDescription' => 'Base definitions of IT services on service characteristics and business requirements. Ensure that they are organised and stored centrally via the implementation of a service catalogue portfolio approach.',
    'tImplementationGuide' => '',
  ),
  249 => 
  array (
    'fkContext' => '2050',
    'fkSectionBestPractice' => '1672',
    'nControlType' => '0',
    'sName' => 'DS1.3 Service Level Agreements',
    'tDescription' => 'Define and agree to SLAs for all critical IT services based on customer requirements and IT capabilities. This should cover customer commitments;
service support requirements;
quantitative and qualitative metrics for measuring the service signed off on by the stakeholders;
funding and commercial arrangements, if applicable;
and roles and responsibilities, including oversight of the SLA. Consider items such as availability, reliability, performance, capacity for growth, levels of support, continuity planning, security and demand constraints.',
    'tImplementationGuide' => '',
  ),
  250 => 
  array (
    'fkContext' => '2051',
    'fkSectionBestPractice' => '1672',
    'nControlType' => '0',
    'sName' => 'DS1.4 Operating Level Agreements',
    'tDescription' => 'Define OLAs that explain how the services will be technically delivered to support the SLA(s) in an optimal manner. The OLAs should specify the technical processes in terms meaningful to the provider and may support several SLAs.',
    'tImplementationGuide' => '',
  ),
  251 => 
  array (
    'fkContext' => '2052',
    'fkSectionBestPractice' => '1672',
    'nControlType' => '0',
    'sName' => 'DS1.5 Monitoring and Reporting of Service Level Achievements',
    'tDescription' => 'Continuously monitor specified service level performance criteria. Reports on achievement of service levels should be provided in a format that is meaningful to the stakeholders. The monitoring statistics should be analysed and acted upon to identify negative and positive trends for individual services as well as for services overall.',
    'tImplementationGuide' => '',
  ),
  252 => 
  array (
    'fkContext' => '2053',
    'fkSectionBestPractice' => '1672',
    'nControlType' => '0',
    'sName' => 'DS1.6 Review of Service Level Agreements and Contracts',
    'tDescription' => 'Regularly review SLAs and underpinning contracts (UCs) with internal and external service providers to ensure that they are effective and up to date and that changes in requirements have been taken into account.',
    'tImplementationGuide' => '',
  ),
  253 => 
  array (
    'fkContext' => '2054',
    'fkSectionBestPractice' => '1673',
    'nControlType' => '0',
    'sName' => 'DS2.1 Identification of All Supplier Relationships',
    'tDescription' => 'Identify all supplier services, and categorise them according to supplier type, significance and criticality. Maintain formal documentation of technical and organisational relationships covering the roles and responsibilities, goals, expected deliverables, and credentials of representatives of these suppliers.',
    'tImplementationGuide' => '',
  ),
  254 => 
  array (
    'fkContext' => '2055',
    'fkSectionBestPractice' => '1673',
    'nControlType' => '0',
    'sName' => 'DS2.2 Supplier Relationship Management',
    'tDescription' => 'Formalise the supplier relationship management process for each supplier. The relationship owners should liaise on customer and supplier issues and ensure the quality of the relationship based on trust and transparency (e.g., through SLAs).',
    'tImplementationGuide' => '',
  ),
  255 => 
  array (
    'fkContext' => '2056',
    'fkSectionBestPractice' => '1673',
    'nControlType' => '0',
    'sName' => 'DS2.3 Supplier Risk Management',
    'tDescription' => 'Identify and mitigate risks relating to suppliers\'\' ability to continue effective service delivery in a secure and efficient manner on a continual basis. Ensure that contracts conform to universal business standards in accordance with legal and regulatory requirements. Risk management should further consider non-disclosure agreements (NDAs), escrow contracts, continued supplier viability, conformance with security requirements, alternative suppliers, penalties and rewards, etc.',
    'tImplementationGuide' => '',
  ),
  256 => 
  array (
    'fkContext' => '2057',
    'fkSectionBestPractice' => '1673',
    'nControlType' => '0',
    'sName' => 'DS2.4 Supplier Performance Monitoring',
    'tDescription' => 'Establish a process to monitor service delivery to ensure that the supplier is meeting current business requirements and continuing to adhere to the contract agreements and SLAs, and that performance is competitive with alternative suppliers and market conditions.',
    'tImplementationGuide' => '',
  ),
  257 => 
  array (
    'fkContext' => '2058',
    'fkSectionBestPractice' => '1674',
    'nControlType' => '0',
    'sName' => 'DS3.1 Performance and Capacity Planning',
    'tDescription' => 'Establish a planning process for the review of performance and capacity of IT resources to ensure that cost-justifiable capacity and performance are available to process the agreed-upon workloads as determined by the SLAs. Capacity and performance plans should leverage appropriate modelling techniques to produce a model of the current and forecasted performance, capacity and throughput of the IT resources.',
    'tImplementationGuide' => '',
  ),
  258 => 
  array (
    'fkContext' => '2059',
    'fkSectionBestPractice' => '1674',
    'nControlType' => '0',
    'sName' => 'DS3.2 Current Performance and Capacity',
    'tDescription' => 'Assess current performance and capacity of IT resources to determine if sufficient capacity and performance exist to deliver against agreed-upon service levels.',
    'tImplementationGuide' => '',
  ),
  259 => 
  array (
    'fkContext' => '2060',
    'fkSectionBestPractice' => '1674',
    'nControlType' => '0',
    'sName' => 'DS3.3 Future Performance and Capacity',
    'tDescription' => 'Conduct performance and capacity forecasting of IT resources at regular intervals to minimise the risk of service disruptions due to insufficient capacity or performance degradation, and identify excess capacity for possible redeployment. Identify workload trends and determine forecasts to be input to performance and capacity plans.',
    'tImplementationGuide' => '',
  ),
  260 => 
  array (
    'fkContext' => '2061',
    'fkSectionBestPractice' => '1674',
    'nControlType' => '0',
    'sName' => 'DS3.4 IT Resources Availability',
    'tDescription' => 'Provide the required capacity and performance, taking into account aspects such as normal workloads, contingencies, storage requirements and IT resource life cycles. Provisions such as prioritising tasks, fault-tolerance mechanisms and resource allocation practices should be made. Management should ensure that contingency plans properly address availability, capacity and performance of individual IT resources.',
    'tImplementationGuide' => '',
  ),
  261 => 
  array (
    'fkContext' => '2062',
    'fkSectionBestPractice' => '1674',
    'nControlType' => '0',
    'sName' => 'DS3.5 Monitoring and Reporting',
    'tDescription' => 'Continuously monitor the performance and capacity of IT resources. Data gathered should serve two purposes: - To maintain and tune current performance within IT and address such issues as resilience, contingency, current and  projected workloads, storage plans, and resource acquisition - To report delivered service availability to the business, as required by the SLAs Accompany all exception reports with recommendations for corrective action.',
    'tImplementationGuide' => '',
  ),
  262 => 
  array (
    'fkContext' => '2063',
    'fkSectionBestPractice' => '1675',
    'nControlType' => '0',
    'sName' => 'DS4.1 IT Continuity Framework',
    'tDescription' => 'Develop a framework for IT continuity to support enterprisewide business continuity management using a consistent process. The objective of the framework should be to assist in determining the required resilience of the infrastructure and to drive the development of disaster recovery and IT contingency plans. The framework should address the organisational structure for continuity management, covering the roles, tasks and responsibilities of internal and external service providers, their management and their customers, and the planning processes that create the rules and structures to document, test and execute the disaster recovery and IT contingency plans. The plan should also address items such as the identification of critical resources, noting key dependencies, the monitoring and reporting of the availability of critical resources, alternative processing, and the principles of backup and recovery.',
    'tImplementationGuide' => '',
  ),
  263 => 
  array (
    'fkContext' => '2064',
    'fkSectionBestPractice' => '1675',
    'nControlType' => '0',
    'sName' => 'DS4.2 IT Continuity Plans',
    'tDescription' => 'Develop IT continuity plans based on the framework and designed to reduce the impact of a major disruption on key business functions and processes. The plans should be based on risk understanding of potential business impacts and address requirements for resilience, alternative processing and recovery capability of all critical IT services. They should also cover usage guidelines, roles and responsibilities, procedures, communication processes, and the testing approach.',
    'tImplementationGuide' => '',
  ),
  264 => 
  array (
    'fkContext' => '2065',
    'fkSectionBestPractice' => '1675',
    'nControlType' => '0',
    'sName' => 'DS4.3 Critical IT Resources',
    'tDescription' => 'Focus attention on items specified as most critical in the IT continuity plan to build in resilience and establish priorities in recovery situations. Avoid the distraction of recovering less-critical items and ensure response and recovery in line with prioritised business needs, while ensuring that costs are kept at an acceptable level and complying with regulatory and contractual requirements. Consider resilience, response and recovery requirements for different tiers, e.g., one to four hours, four to 24 hours, more than 24 hours and critical business operational periods.',
    'tImplementationGuide' => '',
  ),
  265 => 
  array (
    'fkContext' => '2066',
    'fkSectionBestPractice' => '1675',
    'nControlType' => '0',
    'sName' => 'DS4.4 Maintenance of the IT Continuity Plan',
    'tDescription' => 'Encourage IT management to define and execute change control procedures to ensure that the IT continuity plan is kept up to date and continually reflects actual business requirements. Communicate changes in procedures and responsibilities clearly and in a timely manner.',
    'tImplementationGuide' => '',
  ),
  266 => 
  array (
    'fkContext' => '2067',
    'fkSectionBestPractice' => '1675',
    'nControlType' => '0',
    'sName' => 'DS4.5 Testing of the IT Continuity Plan',
    'tDescription' => 'Test the IT continuity plan on a regular basis to ensure that IT systems can be effectively recovered, shortcomings are addressed and the plan remains relevant. This requires careful preparation, documentation, reporting of test results and, according to the results, implementation of an action plan. Consider the extent of testing recovery of single applications to integrated testing scenarios to end to-end testing and integrated vendor testing.',
    'tImplementationGuide' => '',
  ),
  267 => 
  array (
    'fkContext' => '2068',
    'fkSectionBestPractice' => '1675',
    'nControlType' => '0',
    'sName' => 'DS4.6 IT Continuity Plan Training',
    'tDescription' => 'Provide all concerned parties with regular training sessions regarding the procedures and their roles and responsibilities in case of na incident or disaster. Verify and enhance training according to the results of the contingency tests.',
    'tImplementationGuide' => '',
  ),
  268 => 
  array (
    'fkContext' => '2069',
    'fkSectionBestPractice' => '1675',
    'nControlType' => '0',
    'sName' => 'DS4.7 Distribution of the IT Continuity Plan',
    'tDescription' => 'Determine that a defined and managed distribution strategy exists to ensure that plans are properly and securely distributed and available to appropriately authorised interested parties when and where needed. Attention should be paid to making the plans accessible under all disaster scenarios.',
    'tImplementationGuide' => '',
  ),
  269 => 
  array (
    'fkContext' => '2070',
    'fkSectionBestPractice' => '1675',
    'nControlType' => '0',
    'sName' => 'DS4.8 IT Services Recovery and Resumption',
    'tDescription' => 'Plan the actions to be taken for the period when IT is recovering and resuming services. This may include activation of backup sites, initiation of alternative processing, customer and stakeholder communication, and resumption procedures. Ensure that the business understands IT recovery times and the necessary technology investments to support business recovery and resumption needs.',
    'tImplementationGuide' => '',
  ),
  270 => 
  array (
    'fkContext' => '2071',
    'fkSectionBestPractice' => '1675',
    'nControlType' => '0',
    'sName' => 'DS4.9 Offsite Backup Storage',
    'tDescription' => 'Store offsite all critical backup media, documentation and other IT resources necessary for IT recovery and business continuity plans. Determine the content of backup storage in collaboration between business process owners and IT personnel. Management of the offsite storage facility should respond to the data classification policy and the enterprise\'\'s media storage practices. IT management should ensure that offsite arrangements are periodically assessed, at least annually, for content, environmental protection and security. Ensure compatibility of hardware and software to restore archived data, and periodically test and refresh archived data.',
    'tImplementationGuide' => '',
  ),
  271 => 
  array (
    'fkContext' => '2072',
    'fkSectionBestPractice' => '1675',
    'nControlType' => '0',
    'sName' => 'DS4.10 Post-resumption Review',
    'tDescription' => 'Determine whether IT management has established procedures for assessing the adequacy of the plan in regard to the successful resumption of the IT function after a disaster, and update the plan accordingly.',
    'tImplementationGuide' => '',
  ),
  272 => 
  array (
    'fkContext' => '2073',
    'fkSectionBestPractice' => '1676',
    'nControlType' => '0',
    'sName' => 'DS5.1 Management of IT Security',
    'tDescription' => 'Manage IT security at the highest appropriate organisational level, so the management of security actions is in line with business requirements.',
    'tImplementationGuide' => '',
  ),
  273 => 
  array (
    'fkContext' => '2074',
    'fkSectionBestPractice' => '1676',
    'nControlType' => '0',
    'sName' => 'DS5.2 IT Security Plan',
    'tDescription' => 'Translate business, risk and compliance requirements into an overall IT security plan, taking into consideration the IT infrastructure and the security culture. Ensure that the plan is implemented in security policies and procedures together with appropriate investments in services, personnel, software and hardware. Communicate security policies and procedures to stakeholders and users.',
    'tImplementationGuide' => '',
  ),
  274 => 
  array (
    'fkContext' => '2075',
    'fkSectionBestPractice' => '1676',
    'nControlType' => '0',
    'sName' => 'DS5.3 Identity Management',
    'tDescription' => 'Ensure that all users (internal, external and temporary) and their activity on IT systems (business application, IT environment, system operations, development and maintenance) are uniquely identifiable. Enable user identities via authentication mechanisms. Confirm that user access rights to systems and data are in line with defined and documented business needs and that job requirements are attached to user identities. Ensure that user access rights are requested by user management, approved by system owners and implemented by the security-responsible person. Maintain user identities and access rights in a central repository. Deploy cost-effective technical and procedural measures, and keep them current to establish user identification, implement authentication and enforce access rights.',
    'tImplementationGuide' => '',
  ),
  275 => 
  array (
    'fkContext' => '2076',
    'fkSectionBestPractice' => '1676',
    'nControlType' => '0',
    'sName' => 'DS5.4 User Account Management',
    'tDescription' => 'Address requesting, establishing, issuing, suspending, modifying and closing user accounts and related user privileges with a set of user account management procedures. Include an approval procedure outlining the data or system owner granting the access privileges. These procedures should apply for all users, including administrators (privileged users) and internal and external users, for normal and emergency cases. Rights and obligations relative to access to enterprise systems and information should be contractually arranged for all types of users. Perform regular management review of all accounts and related privileges.',
    'tImplementationGuide' => '',
  ),
  276 => 
  array (
    'fkContext' => '2077',
    'fkSectionBestPractice' => '1676',
    'nControlType' => '0',
    'sName' => 'DS5.5 Security Testing, Surveillance and Monitoring',
    'tDescription' => 'Test and monitor the IT security implementation in a proactive way. IT security should be reaccredited in a timely manner to ensure that the approved enterprise\'\'s information security baseline is maintained. A logging and monitoring function will enable the early prevention and/or detection and subsequent timely reporting of unusual and/or abnormal activities that may need to be addressed.',
    'tImplementationGuide' => '',
  ),
  277 => 
  array (
    'fkContext' => '2078',
    'fkSectionBestPractice' => '1676',
    'nControlType' => '0',
    'sName' => 'DS5.6 Security Incident Definition',
    'tDescription' => 'Clearly define and communicate the characteristics of potential security incidents so they can be properly classified and treated by the incident and problem management process.',
    'tImplementationGuide' => '',
  ),
  278 => 
  array (
    'fkContext' => '2079',
    'fkSectionBestPractice' => '1676',
    'nControlType' => '0',
    'sName' => 'DS5.7 Protection of Security Technology',
    'tDescription' => 'Make security-related technology resistant to tampering, and do not disclose security documentation unnecessarily.',
    'tImplementationGuide' => '',
  ),
  279 => 
  array (
    'fkContext' => '2080',
    'fkSectionBestPractice' => '1676',
    'nControlType' => '0',
    'sName' => 'DS5.8 Cryptographic Key Management',
    'tDescription' => 'Determine that policies and procedures are in place to organise the generation, change, revocation, destruction, distribution, certification, storage, entry, use and archiving of cryptographic keys to ensure the protection of keys against modification and unauthorised disclosure.',
    'tImplementationGuide' => '',
  ),
  280 => 
  array (
    'fkContext' => '2081',
    'fkSectionBestPractice' => '1676',
    'nControlType' => '0',
    'sName' => 'DS5.9 Malicious Software Prevention, Detection and Correction',
    'tDescription' => 'Put preventive, detective and corrective measures in place (especially up-to-date security patches and virus control) across the organisation to protect information systems and technology from malware (e.g., viruses, worms, spyware, spam).',
    'tImplementationGuide' => '',
  ),
  281 => 
  array (
    'fkContext' => '2082',
    'fkSectionBestPractice' => '1676',
    'nControlType' => '0',
    'sName' => 'DS5.10 Network Security',
    'tDescription' => 'Use security techniques and related management procedures (e.g., firewalls, security appliances, network segmentation, intrusion detection) to authorise access and control information flows from and to networks.',
    'tImplementationGuide' => '',
  ),
  282 => 
  array (
    'fkContext' => '2083',
    'fkSectionBestPractice' => '1676',
    'nControlType' => '0',
    'sName' => 'DS5.11 Exchange of Sensitive Data',
    'tDescription' => 'Exchange sensitive transaction data only over a trusted path or medium with controls to provide authenticity of content, proof of submission, proof of receipt and non-repudiation of origin.',
    'tImplementationGuide' => '',
  ),
  283 => 
  array (
    'fkContext' => '2084',
    'fkSectionBestPractice' => '1677',
    'nControlType' => '0',
    'sName' => 'DS6.1 Definition of Services',
    'tDescription' => 'Identify all IT costs, and map them to IT services to support a transparent cost model. IT services should be linked to business processes such that the business can identify associated service billing levels.',
    'tImplementationGuide' => '',
  ),
  284 => 
  array (
    'fkContext' => '2085',
    'fkSectionBestPractice' => '1677',
    'nControlType' => '0',
    'sName' => 'DS6.2 IT Accounting',
    'tDescription' => 'Capture and allocate actual costs according to the enterprise cost model. Variances between forecasts and actual costs should be analysed and reported on, in compliance with the enterprise\'\'s financial measurement systems.',
    'tImplementationGuide' => '',
  ),
  285 => 
  array (
    'fkContext' => '2086',
    'fkSectionBestPractice' => '1677',
    'nControlType' => '0',
    'sName' => 'DS6.3 Cost Modelling and Charging',
    'tDescription' => 'Establish and use an IT costing model based on the service definitions that support the calculation of chargeback rates per service. The IT cost model should ensure that charging for services is identifiable, measurable and predictable by users to encourage proper use of resources.',
    'tImplementationGuide' => '',
  ),
  286 => 
  array (
    'fkContext' => '2087',
    'fkSectionBestPractice' => '1677',
    'nControlType' => '0',
    'sName' => 'DS6.4 Cost Model Maintenance',
    'tDescription' => 'Regularly review and benchmark the appropriateness of the cost/recharge model to maintain its relevance and appropriateness to the evolving business and IT activities.',
    'tImplementationGuide' => '',
  ),
  287 => 
  array (
    'fkContext' => '2088',
    'fkSectionBestPractice' => '1678',
    'nControlType' => '0',
    'sName' => 'DS7.1 Identification of Education and Training Needs',
    'tDescription' => 'Establish and regularly update a curriculum for each target group of employees considering:  - Current and future business needs and strategy  - Value of information as an asset  - Corporate values (ethical values, control and security culture, etc.)  - Implementation of new IT infrastructure and software (i.e., packages, applications)  - Current and future skills, competence profiles, and certification and/or credentialing needs as well as required reaccreditation  - Delivery methods (e.g., classroom, web-based), target group size, accessibility and timing',
    'tImplementationGuide' => '',
  ),
  288 => 
  array (
    'fkContext' => '2089',
    'fkSectionBestPractice' => '1678',
    'nControlType' => '0',
    'sName' => 'DS7.2 Delivery of Training and Education',
    'tDescription' => 'Based on the identified education and training needs, identify target groups and their members, efficient delivery mechanisms, teachers, trainers, and mentors. Appoint trainers and organise timely training sessions. Record registration (including prerequisites), attendance and training session performance evaluations.',
    'tImplementationGuide' => '',
  ),
  289 => 
  array (
    'fkContext' => '2090',
    'fkSectionBestPractice' => '1678',
    'nControlType' => '0',
    'sName' => 'DS7.3 Evaluation of Training Received',
    'tDescription' => 'Evaluate education and training content delivery upon completion for relevance, quality, effectiveness, the retention of knowledge, cost and value. The results of this evaluation should serve as input for future curriculum definition and the delivery of training sessions.',
    'tImplementationGuide' => '',
  ),
  290 => 
  array (
    'fkContext' => '2091',
    'fkSectionBestPractice' => '1679',
    'nControlType' => '0',
    'sName' => 'DS8.1 Service Desk',
    'tDescription' => 'Establish a service desk function, which is the user interface with IT, to register, communicate, dispatch and analyse all calls, reported incidents, service requests and information demands. There should be monitoring and escalation procedures based on agreed-upon service levels relative to the appropriate SLA that allow classification and prioritisation of any reported issue as an incident, service request or information request. Measure end users\'\' satisfaction with the quality of the service desk and IT services.',
    'tImplementationGuide' => '',
  ),
  291 => 
  array (
    'fkContext' => '2092',
    'fkSectionBestPractice' => '1679',
    'nControlType' => '0',
    'sName' => 'DS8.2 Registration of Customer Queries',
    'tDescription' => 'Establish a function and system to allow logging and tracking of calls, incidents, service requests and information needs. It should work closely with such processes as incident management, problem management, change management, capacity management and availability management. Incidents should be classified according to a business and service priority and routed to the appropriate problem management team, where necessary. Customers should be kept informed of the status of their queries.',
    'tImplementationGuide' => '',
  ),
  292 => 
  array (
    'fkContext' => '2093',
    'fkSectionBestPractice' => '1679',
    'nControlType' => '0',
    'sName' => 'DS8.3 Incident Escalation',
    'tDescription' => 'Establish service desk procedures, so incidents that cannot be resolved immediately are appropriately escalated according to limits defined in the SLA and, if appropriate, workarounds are provided. Ensure that incident ownership and life cycle monitoring remain with the service desk for user-based incidents, regardless which IT group is working on resolution activities.',
    'tImplementationGuide' => '',
  ),
  293 => 
  array (
    'fkContext' => '2094',
    'fkSectionBestPractice' => '1679',
    'nControlType' => '0',
    'sName' => 'DS8.4 Incident Closure',
    'tDescription' => 'Establish procedures for the timely monitoring of clearance of customer queries. When the incident has been resolved, ensure that the service desk records the resolution steps, and confirm that the action taken has been agreed to by the customer. Also record and report unresolved incidents (known errors and workarounds) to provide information for proper problem management.',
    'tImplementationGuide' => '',
  ),
  294 => 
  array (
    'fkContext' => '2095',
    'fkSectionBestPractice' => '1679',
    'nControlType' => '0',
    'sName' => 'DS8.5 Reporting and Trend Analysis',
    'tDescription' => 'Produce reports of service desk activity to enable management to measure service performance and service response times and to identify trends or recurring problems, so service can be continually improved.',
    'tImplementationGuide' => '',
  ),
  295 => 
  array (
    'fkContext' => '2096',
    'fkSectionBestPractice' => '1680',
    'nControlType' => '0',
    'sName' => 'DS9.1 Configuration Repository and Baseline',
    'tDescription' => 'Establish a supporting tool and a central repository to contain all relevant information on configuration items. Monitor and record all assets and changes to assets. Maintain a baseline of configuration items for every system and service as a checkpoint to which to return after changes.',
    'tImplementationGuide' => '',
  ),
  296 => 
  array (
    'fkContext' => '2097',
    'fkSectionBestPractice' => '1680',
    'nControlType' => '0',
    'sName' => 'DS9.2 Identification and Maintenance of Configuration Items',
    'tDescription' => 'Establish configuration procedures to support management and logging of all changes to the configuration repository. Integrate these procedures with change management, incident management and problem management procedures.',
    'tImplementationGuide' => '',
  ),
  297 => 
  array (
    'fkContext' => '2098',
    'fkSectionBestPractice' => '1680',
    'nControlType' => '0',
    'sName' => 'DS9.3 Configuration Integrity Review',
    'tDescription' => 'Periodically review the configuration data to verify and confirm the integrity of the current and historical configuration. Periodically review installed software against the policy for software usage to identify personal or unlicensed software or any software instances in excess of current license agreements. Report, act on and correct errors and deviations.',
    'tImplementationGuide' => '',
  ),
  298 => 
  array (
    'fkContext' => '2099',
    'fkSectionBestPractice' => '1681',
    'nControlType' => '0',
    'sName' => 'DS10.1 Identification and Classification of Problems',
    'tDescription' => 'Implement processes to report and classify problems that have been identified as part of incident management. The steps involved in problem classification are similar to the steps in classifying incidents;
they are to determine category, impact, urgency and priority. Categorise problems as appropriate into related groups or domains (e.g., hardware, software, support software). These groups may match the organisational responsibilities of the user and customer base, and should be the basis for allocating problems to support staff.',
    'tImplementationGuide' => '',
  ),
  299 => 
  array (
    'fkContext' => '2100',
    'fkSectionBestPractice' => '1681',
    'nControlType' => '0',
    'sName' => 'DS10.2 Problem Tracking and Resolution',
    'tDescription' => 'Ensure that the problem management system provides for adequate audit trail facilities that allow tracking, analysing and determining the root cause of all reported problems considering:  - All associated configuration items  - Outstanding problems and incidents  - Known and suspected errors  - Tracking of problem trends  Identify and initiate sustainable solutions addressing the root cause, raising change requests via the established change management process. Throughout the resolution process, problem management should obtain regular reports from change management on progress in resolving problems and errors. Problem management should monitor the continuing impact of problems and known errors on user services. In the event that this impact becomes severe, problem management should escalate the problem, perhaps referring it to an appropriate board to increase the priority of the (RFC or to implement an urgent change as appropriate. Monitor the progress of problem resolution against SLAs.',
    'tImplementationGuide' => '',
  ),
  300 => 
  array (
    'fkContext' => '2101',
    'fkSectionBestPractice' => '1681',
    'nControlType' => '0',
    'sName' => 'DS10.3 Problem Closure',
    'tDescription' => 'Put in place a procedure to close problem records either after confirmation of successful elimination of the known error or after agreement with the business on how to alternatively handle the problem.',
    'tImplementationGuide' => '',
  ),
  301 => 
  array (
    'fkContext' => '2102',
    'fkSectionBestPractice' => '1681',
    'nControlType' => '0',
    'sName' => 'DS10.4 Integration of Configuration, Incident and Problem Management',
    'tDescription' => 'Integrate the related processes of configuration, incident and problem management to ensure effective management of problems and enable improvements.',
    'tImplementationGuide' => '',
  ),
  302 => 
  array (
    'fkContext' => '2103',
    'fkSectionBestPractice' => '1682',
    'nControlType' => '0',
    'sName' => 'DS11.1 Business Requirements for Data Management',
    'tDescription' => 'Verify that all data expected for processing are received and processed completely, accurately and in a timely manner, and all output is delivered in accordance with business requirements. Support restart and reprocessing needs.',
    'tImplementationGuide' => '',
  ),
  303 => 
  array (
    'fkContext' => '2104',
    'fkSectionBestPractice' => '1682',
    'nControlType' => '0',
    'sName' => 'DS11.2 Storage and Retention Arrangements',
    'tDescription' => 'Define and implement procedures for effective and efficient data storage, retention and archiving to meet business objectives, the organisation\'\'s security policy and regulatory requirements.',
    'tImplementationGuide' => '',
  ),
  304 => 
  array (
    'fkContext' => '2105',
    'fkSectionBestPractice' => '1682',
    'nControlType' => '0',
    'sName' => 'DS11.3 Media Library Management System',
    'tDescription' => 'Define and implement procedures to maintain na inventory of stored and archived media to ensure their usability and integrity.',
    'tImplementationGuide' => '',
  ),
  305 => 
  array (
    'fkContext' => '2106',
    'fkSectionBestPractice' => '1682',
    'nControlType' => '0',
    'sName' => 'DS11.4 Disposal',
    'tDescription' => 'Define and implement procedures to ensure that business requirements for protection of sensitive data and software are met when data and hardware are disposed or transferred.',
    'tImplementationGuide' => '',
  ),
  306 => 
  array (
    'fkContext' => '2107',
    'fkSectionBestPractice' => '1682',
    'nControlType' => '0',
    'sName' => 'DS11.5 Backup and Restoration',
    'tDescription' => 'Define and implement procedures for backup and restoration of systems, applications, data and documentation in line with business requirements and the continuity plan.',
    'tImplementationGuide' => '',
  ),
  307 => 
  array (
    'fkContext' => '2108',
    'fkSectionBestPractice' => '1682',
    'nControlType' => '0',
    'sName' => 'DS11.6 Security Requirements for Data Management',
    'tDescription' => 'Define and implement policies and procedures to identify and apply security requirements applicable to the receipt, processing, storage and output of data to meet business objectives, the organisation\'\'s security policy and regulatory requirements.',
    'tImplementationGuide' => '',
  ),
  308 => 
  array (
    'fkContext' => '2109',
    'fkSectionBestPractice' => '1683',
    'nControlType' => '0',
    'sName' => 'DS12.1 Site Selection and Layout',
    'tDescription' => 'Define and select the physical sites for IT equipment to support the technology strategy linked to the business strategy. The selection and design of the layout of a site should take into account the risk associated with natural and man-made disasters, whilst considering relevant laws and regulations, such as occupational health and safety regulations.',
    'tImplementationGuide' => '',
  ),
  309 => 
  array (
    'fkContext' => '2110',
    'fkSectionBestPractice' => '1683',
    'nControlType' => '0',
    'sName' => 'DS12.2 Physical Security Measures',
    'tDescription' => 'Define and implement physical security measures in line with business requirements to secure the location and the physical assets. Physical security measures must be capable of effectively preventing, detecting and mitigating risks relating to theft, temperature, fire, smoke, water, vibration, terror, vandalism, power outages, chemicals or explosives.',
    'tImplementationGuide' => '',
  ),
  310 => 
  array (
    'fkContext' => '2111',
    'fkSectionBestPractice' => '1683',
    'nControlType' => '0',
    'sName' => 'DS12.3 Physical Access',
    'tDescription' => 'Define and implement procedures to grant, limit and revoke access to premises, buildings and areas according to business needs, including emergencies. Access to premises, buildings and areas should be justified, authorised, logged and monitored. This should apply to all persons entering the premises, including staff, temporary staff, clients, vendors, visitors or any other third party.',
    'tImplementationGuide' => '',
  ),
  311 => 
  array (
    'fkContext' => '2112',
    'fkSectionBestPractice' => '1683',
    'nControlType' => '0',
    'sName' => 'DS12.4 Protection Against Environmental Factors',
    'tDescription' => 'Design and implement measures for protection against environmental factors. Install specialised equipment and devices to monitor and control the environment.',
    'tImplementationGuide' => '',
  ),
  312 => 
  array (
    'fkContext' => '2113',
    'fkSectionBestPractice' => '1683',
    'nControlType' => '0',
    'sName' => 'DS12.5 Physical Facilities Management',
    'tDescription' => 'Manage facilities, including power and communications equipment, in line with laws and regulations, technical and business requirements, vendor specifications, and health and safety guidelines.',
    'tImplementationGuide' => '',
  ),
  313 => 
  array (
    'fkContext' => '2114',
    'fkSectionBestPractice' => '1684',
    'nControlType' => '0',
    'sName' => 'DS13.1 Operations Procedures and Instructions',
    'tDescription' => 'Define, implement and maintain procedures for IT operations, ensuring that the operations staff members are familiar with all operations tasks relevant to them. Operational procedures should cover shift handover (formal handover of activity, status updates, operational problems, escalation procedures and reports on current responsibilities) to support agreed-upon service levels and ensure continuous operations.',
    'tImplementationGuide' => '',
  ),
  314 => 
  array (
    'fkContext' => '2115',
    'fkSectionBestPractice' => '1684',
    'nControlType' => '0',
    'sName' => 'DS13.2 Job Scheduling',
    'tDescription' => 'Organise the scheduling of jobs, processes and tasks into the most efficient sequence, maximising throughput and utilisation to meet business requirements.',
    'tImplementationGuide' => '',
  ),
  315 => 
  array (
    'fkContext' => '2116',
    'fkSectionBestPractice' => '1684',
    'nControlType' => '0',
    'sName' => 'DS13.3 IT Infrastructure Monitoring',
    'tDescription' => 'Define and implement procedures to monitor the IT infrastructure and related events. Ensure that sufficient chronological information is being stored in operations logs to enable the reconstruction, review and examination of the time sequences of operations and the other activities surrounding or supporting operations.',
    'tImplementationGuide' => '',
  ),
  316 => 
  array (
    'fkContext' => '2117',
    'fkSectionBestPractice' => '1684',
    'nControlType' => '0',
    'sName' => 'DS13.4 Sensitive Documents and Output Devices',
    'tDescription' => 'Establish appropriate physical safeguards, accounting practices and inventory management over sensitive IT assets, such as special forms, negotiable instruments, special purpose printers or security tokens.',
    'tImplementationGuide' => '',
  ),
  317 => 
  array (
    'fkContext' => '2118',
    'fkSectionBestPractice' => '1684',
    'nControlType' => '0',
    'sName' => 'DS13.5 Preventive Maintenance for Hardware',
    'tDescription' => 'Define and implement procedures to ensure timely maintenance of infrastructure to reduce the frequency and impact of failures or performance degradation.',
    'tImplementationGuide' => '',
  ),
  318 => 
  array (
    'fkContext' => '2119',
    'fkSectionBestPractice' => '1686',
    'nControlType' => '0',
    'sName' => 'ME1.1 Monitoring Approach',
    'tDescription' => 'Establish a general monitoring framework and approach to define the scope, methodology and process to be followed for measuring IT\'\'s solution and service delivery, and monitor IT\'\'s contribution to the business. Integrate the framework with the corporate performance management system.',
    'tImplementationGuide' => '',
  ),
  319 => 
  array (
    'fkContext' => '2120',
    'fkSectionBestPractice' => '1686',
    'nControlType' => '0',
    'sName' => 'ME1.2 Definition and Collection of Monitoring Data',
    'tDescription' => 'Work with the business to define a balanced set of performance targets and have them approved by the business and other relevant stakeholders. Define benchmarks with which to compare the targets, and identify available data to be collected to measure the targets. Establish processes to collect timely and accurate data to report on progress against targets.',
    'tImplementationGuide' => '',
  ),
  320 => 
  array (
    'fkContext' => '2121',
    'fkSectionBestPractice' => '1686',
    'nControlType' => '0',
    'sName' => 'ME1.3 Monitoring Method',
    'tDescription' => 'Deploy a performance monitoring method (e.g., balanced scorecard) that records targets;
captures measurements;
provides a succinct, all-around view of IT performance;
and fits within the enterprise monitoring system.',
    'tImplementationGuide' => '',
  ),
  321 => 
  array (
    'fkContext' => '2122',
    'fkSectionBestPractice' => '1686',
    'nControlType' => '0',
    'sName' => 'ME1.4 Performance Assessment',
    'tDescription' => 'Periodically review performance against targets, analyse the cause of any deviations, and initiate remedial action to address the underlying causes. At appropriate times, perform root cause analysis across deviations.',
    'tImplementationGuide' => '',
  ),
  322 => 
  array (
    'fkContext' => '2123',
    'fkSectionBestPractice' => '1686',
    'nControlType' => '0',
    'sName' => 'ME1.5 Board and Executive Reporting',
    'tDescription' => 'Develop senior management reports on IT\'\'s contribution to the business, specifically in terms of the performance of the enterprise\'\'s portfolio, IT-enabled investment programmes, and the solution and service deliverable performance of individual programmes. Include in status reports the extent to which planned objectives have been achieved, budgeted resources used,  set performance targets met and identified risks mitigated. Anticipate senior management\'\'s review by suggesting remedial actions for major deviations. Provide the report to senior management, and solicit feedback from management\'\'s review.',
    'tImplementationGuide' => '',
  ),
  323 => 
  array (
    'fkContext' => '2124',
    'fkSectionBestPractice' => '1686',
    'nControlType' => '0',
    'sName' => 'ME1.6 Remedial Actions',
    'tDescription' => 'Identify and initiate remedial actions based on performance monitoring, assessment and reporting. This includes follow-up of all monitoring, reporting and assessments through: - Review, negotiation and establishment of management responses - Assignment of responsibility for remediation - Tracking of the results of actions committed',
    'tImplementationGuide' => '',
  ),
  324 => 
  array (
    'fkContext' => '2125',
    'fkSectionBestPractice' => '1687',
    'nControlType' => '0',
    'sName' => 'ME2.1 Monitoring of Internal Control Framework',
    'tDescription' => 'Continuously monitor, benchmark and improve the IT control environment and control framework to meet organisational objectives.',
    'tImplementationGuide' => '',
  ),
  325 => 
  array (
    'fkContext' => '2126',
    'fkSectionBestPractice' => '1687',
    'nControlType' => '0',
    'sName' => 'ME2.2 Supervisory Review',
    'tDescription' => 'Monitor and evaluate the efficiency and effectiveness of internal IT managerial review controls.',
    'tImplementationGuide' => '',
  ),
  326 => 
  array (
    'fkContext' => '2127',
    'fkSectionBestPractice' => '1687',
    'nControlType' => '0',
    'sName' => 'ME2.3 Control Exceptions',
    'tDescription' => 'Identify control exceptions, and analyse and identify their underlying root causes. Escalate control exceptions and report to stakeholders appropriately. Institute necessary corrective action.',
    'tImplementationGuide' => '',
  ),
  327 => 
  array (
    'fkContext' => '2128',
    'fkSectionBestPractice' => '1687',
    'nControlType' => '0',
    'sName' => 'ME2.4 Control Self-assessment',
    'tDescription' => 'Evaluate the completeness and effectiveness of management\'\'s control over IT processes, policies and contracts through a continuing programme of self-assessment.',
    'tImplementationGuide' => '',
  ),
  328 => 
  array (
    'fkContext' => '2129',
    'fkSectionBestPractice' => '1687',
    'nControlType' => '0',
    'sName' => 'ME2.5 Assurance of Internal Control',
    'tDescription' => 'Obtain, as needed, further assurance of the completeness and effectiveness of internal controls through third-party reviews.',
    'tImplementationGuide' => '',
  ),
  329 => 
  array (
    'fkContext' => '2130',
    'fkSectionBestPractice' => '1687',
    'nControlType' => '0',
    'sName' => 'ME2.6 Internal Control at Third Parties',
    'tDescription' => 'Assess the status of external service providers\'\' internal controls. Confirm that external service providers comply with legal and regulatory requirements and contractual obligations.',
    'tImplementationGuide' => '',
  ),
  330 => 
  array (
    'fkContext' => '2131',
    'fkSectionBestPractice' => '1687',
    'nControlType' => '0',
    'sName' => 'ME2.7 Remedial Actions',
    'tDescription' => 'Identify, initiate, track and implement remedial actions arising from control assessments and reporting.',
    'tImplementationGuide' => '',
  ),
  331 => 
  array (
    'fkContext' => '2132',
    'fkSectionBestPractice' => '1688',
    'nControlType' => '0',
    'sName' => 'ME3.1 Identification of External Legal, Regulatory and Contractual Compliance Requirements',
    'tDescription' => 'Identify, on a continuous basis, local and international laws, regulations, and other external requirements that must be complied with for incorporation into the organisation\'\'s IT policies, standards, procedures and methodologies.',
    'tImplementationGuide' => '',
  ),
  332 => 
  array (
    'fkContext' => '2133',
    'fkSectionBestPractice' => '1688',
    'nControlType' => '0',
    'sName' => 'ME3.2 Optimisation of Response to External Requirements',
    'tDescription' => 'Review and adjust IT policies, standards, procedures and methodologies to ensure that legal, regulatory and contractual requirements are addressed and communicated.',
    'tImplementationGuide' => '',
  ),
  333 => 
  array (
    'fkContext' => '2134',
    'fkSectionBestPractice' => '1688',
    'nControlType' => '0',
    'sName' => 'ME3.3 Evaluation of Compliance With External Requirements',
    'tDescription' => 'Confirm compliance of IT policies, standards, procedures and methodologies with legal and regulatory requirements.',
    'tImplementationGuide' => '',
  ),
  334 => 
  array (
    'fkContext' => '2135',
    'fkSectionBestPractice' => '1688',
    'nControlType' => '0',
    'sName' => 'ME3.4 Positive Assurance of Compliance',
    'tDescription' => 'Obtain and report assurance of compliance and adherence to all internal policies derived from internal directives or external legal, regulatory or contractual requirements, confirming that any corrective actions to address any compliance gaps have been taken by the responsible process owner in a timely manner.',
    'tImplementationGuide' => '',
  ),
  335 => 
  array (
    'fkContext' => '2136',
    'fkSectionBestPractice' => '1688',
    'nControlType' => '0',
    'sName' => 'ME3.5 Integrated Reporting',
    'tDescription' => 'Integrate IT reporting on legal, regulatory and contractual requirements with similar output from other business functions.',
    'tImplementationGuide' => '',
  ),
  336 => 
  array (
    'fkContext' => '2137',
    'fkSectionBestPractice' => '1689',
    'nControlType' => '0',
    'sName' => 'ME4.1 Establishment of an IT Governance Framework',
    'tDescription' => 'Define, establish and align the IT governance framework with the overall enterprise governance and control environment. Base the framework on a suitable IT process and control model and provide for unambiguous accountability and practices to avoid a breakdown in internal control and oversight. Confirm that the IT governance framework ensures compliance with laws and regulations and is aligned with, and confirms delivery of, the enterprise\'\'s strategies and objectives. Report IT governance status and issues.',
    'tImplementationGuide' => '',
  ),
  337 => 
  array (
    'fkContext' => '2138',
    'fkSectionBestPractice' => '1689',
    'nControlType' => '0',
    'sName' => 'ME4.2 Strategic Alignment',
    'tDescription' => 'Enable board and executive understanding of strategic IT issues, such as the role of IT, technology insights and capabilities. Ensure that there is a shared understanding between the business and IT regarding the potential contribution of IT to the business strategy. Work with the board and the established governance bodies, such as an IT strategy committee, to provide strategic direction to management relative to IT, ensuring that the strategy and objectives are cascaded into business units and IT functions, and that confidence and trust are developed between the business and IT. Enable the alignment of IT to the business in strategy and operations, encouraging co-responsibility between the business and IT for making strategic decisions and obtaining benefits from IT-enabled investments.',
    'tImplementationGuide' => '',
  ),
  338 => 
  array (
    'fkContext' => '2139',
    'fkSectionBestPractice' => '1689',
    'nControlType' => '0',
    'sName' => 'ME4.3 Value Delivery',
    'tDescription' => 'Manage IT-enabled investment programmes and other IT assets and services to ensure that they deliver the greatest possible value in supporting the enterprise\'\'s strategy and objectives. Ensure that the expected business outcomes of IT-enabled investments and the full scope of effort required to achieve those outcomes are understood;
that comprehensive and consistent business cases are created and approved by stakeholders;
that assets and investments are managed throughout their economic life cycle;
and that there is active management of the realisation of benefits, such as contribution to new services, efficiency gains and improved responsiveness to customer demands. Enforce a disciplined approach to portfolio, programme and project management, insisting that the business takes ownership of all IT-enabled investments and IT ensures optimisation of the costs of delivering IT capabilities and services.',
    'tImplementationGuide' => '',
  ),
  339 => 
  array (
    'fkContext' => '2140',
    'fkSectionBestPractice' => '1689',
    'nControlType' => '0',
    'sName' => 'ME4.4 Resource Management',
    'tDescription' => 'Oversee the investment, use and allocation of IT resources through regular assessments of IT initiatives and operations to ensure appropriate resourcing and alignment with current and future strategic objectives and business imperatives.',
    'tImplementationGuide' => '',
  ),
  340 => 
  array (
    'fkContext' => '2141',
    'fkSectionBestPractice' => '1689',
    'nControlType' => '0',
    'sName' => 'ME4.5 Risk Management',
    'tDescription' => 'Work with the board to define the enterprise\'\'s appetite for IT risk, and obtain reasonable assurance that IT risk management practices are appropriate to ensure that the actual IT risk does not exceed the board\'\'s risk appetite. Embed risk management responsibilities into the organisation, ensuring that the business and IT regularly assess and report IT-related risks and their impact and that the enterprise\'\'s IT risk position is transparent to all stakeholders.',
    'tImplementationGuide' => '',
  ),
  341 => 
  array (
    'fkContext' => '2142',
    'fkSectionBestPractice' => '1689',
    'nControlType' => '0',
    'sName' => 'ME4.6 Performance Measurement',
    'tDescription' => 'Confirm that agreed-upon IT objectives have been met or exceeded, or that progress toward IT goals meets expectations. Where agreed-upon objectives have been missed or progress is not as expected, review management\'\'s remedial action. Report to the board relevant portfolios, programme and IT performance, supported by reports to enable senior management to review the enterprise\'\'s progress toward identified goals.',
    'tImplementationGuide' => '',
  ),
  342 => 
  array (
    'fkContext' => '2143',
    'fkSectionBestPractice' => '1689',
    'nControlType' => '0',
    'sName' => 'ME4.7 Independent Assurance',
    'tDescription' => 'Obtain independent assurance (internal or external) about the conformance of IT with relevant laws and regulations;
the organisation\'\'s policies, standards and procedures;generally accepted practices;
and the effective and efficient performance of IT.',
    'tImplementationGuide' => '',
  ),
  343 => 
  array (
    'fkContext' => '2145',
    'fkSectionBestPractice' => '1692',
    'nControlType' => '0',
    'sName' => '1.1 - PCI',
    'tDescription' => '1.1 Establish firewall configuration stanrds that include the following:',
    'tImplementationGuide' => '',
  ),
  344 => 
  array (
    'fkContext' => '2146',
    'fkSectionBestPractice' => '1692',
    'nControlType' => '0',
    'sName' => '1.1.1 - PCI',
    'tDescription' => '1.1.1 A formal process for approving and testing all external network connections and changes to the firewall configuration.',
    'tImplementationGuide' => '',
  ),
  345 => 
  array (
    'fkContext' => '2147',
    'fkSectionBestPractice' => '1692',
    'nControlType' => '0',
    'sName' => '1.1.2 - PCI',
    'tDescription' => '1.1.2 A current network diagram with all connections to cardholder data, including any wireless networks',
    'tImplementationGuide' => '',
  ),
  346 => 
  array (
    'fkContext' => '2148',
    'fkSectionBestPractice' => '1692',
    'nControlType' => '0',
    'sName' => '1.1.3 - PCI',
    'tDescription' => '1.1.3 Requirements for a firewall at each Internet connection and between any demilitarized zone (DMZ) and the internal network zone',
    'tImplementationGuide' => '',
  ),
  347 => 
  array (
    'fkContext' => '2149',
    'fkSectionBestPractice' => '1692',
    'nControlType' => '0',
    'sName' => '1.1.4 - PCI',
    'tDescription' => '1.1.4 Description of groups, roles, and responsibilities for logical management of network components',
    'tImplementationGuide' => '',
  ),
  348 => 
  array (
    'fkContext' => '2150',
    'fkSectionBestPractice' => '1692',
    'nControlType' => '0',
    'sName' => '1.1.5 - PCI',
    'tDescription' => '1.1.5 Documented list of services and ports necessary for business',
    'tImplementationGuide' => '',
  ),
  349 => 
  array (
    'fkContext' => '2151',
    'fkSectionBestPractice' => '1692',
    'nControlType' => '0',
    'sName' => '1.1.6 - PCI',
    'tDescription' => '1.1.6 Justification and documentation for any available protocols besides hypertext transfer protocol (HTTP), and secure sockets layer (SSL), secure shell (SSH), and virtual private network (VPN)',
    'tImplementationGuide' => '',
  ),
  350 => 
  array (
    'fkContext' => '2152',
    'fkSectionBestPractice' => '1692',
    'nControlType' => '0',
    'sName' => '1.1.7 - PCI',
    'tDescription' => '1.1.7 Justification and documentation for any risky protocols allowed (for example, file transfer protocol (FTP), which includes reason for use of protocol and security features implemented',
    'tImplementationGuide' => '',
  ),
  351 => 
  array (
    'fkContext' => '2153',
    'fkSectionBestPractice' => '1692',
    'nControlType' => '0',
    'sName' => '1.1.8 - PCI',
    'tDescription' => '1.1.8 Quarterly review of firewall and router rule sets',
    'tImplementationGuide' => '',
  ),
  352 => 
  array (
    'fkContext' => '2154',
    'fkSectionBestPractice' => '1692',
    'nControlType' => '0',
    'sName' => '1.1.9 - PCI',
    'tDescription' => '1.1.9 Configuration standards for routers.',
    'tImplementationGuide' => '',
  ),
  353 => 
  array (
    'fkContext' => '2155',
    'fkSectionBestPractice' => '1692',
    'nControlType' => '0',
    'sName' => '1.2 - PCI',
    'tDescription' => '1.2 Build a firewall configuration that denies all traffic from \'\'untrusted\'\' networks and hosts, except for protocols necessary for the cardholder data environment.',
    'tImplementationGuide' => '',
  ),
  354 => 
  array (
    'fkContext' => '2156',
    'fkSectionBestPractice' => '1692',
    'nControlType' => '0',
    'sName' => '1.3 - PCI',
    'tDescription' => '1.3 Build a firewall configuration that restricts connections between publicly accessible servers and any system component storing cardholder data, including any connections from wireless networks. This firewall configuration should include the following:',
    'tImplementationGuide' => '',
  ),
  355 => 
  array (
    'fkContext' => '2157',
    'fkSectionBestPractice' => '1692',
    'nControlType' => '0',
    'sName' => '1.3.1 - PCI',
    'tDescription' => '1.3.1 Restricting inbound Internet traffic to Internet protocol (IP) addresses within the DMZ (ingress filters)',
    'tImplementationGuide' => '',
  ),
  356 => 
  array (
    'fkContext' => '2158',
    'fkSectionBestPractice' => '1692',
    'nControlType' => '0',
    'sName' => '1.3.2 - PCI',
    'tDescription' => '1.3.2 Not allowing internal addresses to pass from the Internet into the DMZ',
    'tImplementationGuide' => '',
  ),
  357 => 
  array (
    'fkContext' => '2159',
    'fkSectionBestPractice' => '1692',
    'nControlType' => '0',
    'sName' => '1.3.3 - PCI',
    'tDescription' => '1.3.3 Implementing stateful inspection, also known as dynamic packet filtering (that is, only "established" connections are allowed into the network)',
    'tImplementationGuide' => '',
  ),
  358 => 
  array (
    'fkContext' => '2160',
    'fkSectionBestPractice' => '1692',
    'nControlType' => '0',
    'sName' => '1.3.4 - PCI',
    'tDescription' => '1.3.4 Placing the database in an internal network zone, segregated from the DMZ',
    'tImplementationGuide' => '',
  ),
  359 => 
  array (
    'fkContext' => '2161',
    'fkSectionBestPractice' => '1692',
    'nControlType' => '0',
    'sName' => '1.3.5 - PCI',
    'tDescription' => '1.3.5 Restricting inbound and outbound traffic to that which is necessary for the cardholder data environment',
    'tImplementationGuide' => '',
  ),
  360 => 
  array (
    'fkContext' => '2162',
    'fkSectionBestPractice' => '1692',
    'nControlType' => '0',
    'sName' => '1.3.6 - PCI',
    'tDescription' => '1.3.6 Securing and synchronizing router configuration files. For example, running configuration files (for normal functioning of the routers), and start-up configuration files (when machines are re-booted) should have the same secure configuration',
    'tImplementationGuide' => '',
  ),
  361 => 
  array (
    'fkContext' => '2163',
    'fkSectionBestPractice' => '1692',
    'nControlType' => '0',
    'sName' => '1.3.7 - PCI',
    'tDescription' => '1.3.7 Denying all other inbound and outbound traffic not specifically allowed',
    'tImplementationGuide' => '',
  ),
  362 => 
  array (
    'fkContext' => '2164',
    'fkSectionBestPractice' => '1692',
    'nControlType' => '0',
    'sName' => '1.3.8 - PCI',
    'tDescription' => '1.3.8 Installing perimeter firewalls between any wireless networks and the cardholder data environment, and configuring these firewalls to deny any traffic from the wireless environment or from controlling any traffic (if such traffic is necessary for business purposes)',
    'tImplementationGuide' => '',
  ),
  363 => 
  array (
    'fkContext' => '2165',
    'fkSectionBestPractice' => '1692',
    'nControlType' => '0',
    'sName' => '1.3.9 - PCI',
    'tDescription' => '1.3.9 Installing personal firewall software on any mobile and employee-owned computers with direct connectivity to the Internet (for example, laptops used by employees), which are used to access the organization\'\'s network.',
    'tImplementationGuide' => '',
  ),
  364 => 
  array (
    'fkContext' => '2166',
    'fkSectionBestPractice' => '1692',
    'nControlType' => '0',
    'sName' => '1.4 - PCI',
    'tDescription' => '1.4 Prohibit direct public access between external networks and any system component that stores cardholder data (for example, databases, logs, trace files)',
    'tImplementationGuide' => '',
  ),
  365 => 
  array (
    'fkContext' => '2167',
    'fkSectionBestPractice' => '1692',
    'nControlType' => '0',
    'sName' => '1.4.1 - PCI',
    'tDescription' => '1.4.1 Implement a DMZ to filter and screen all traffic and to prohibit direct routes for inbound and outbound Internet traffic',
    'tImplementationGuide' => '',
  ),
  366 => 
  array (
    'fkContext' => '2168',
    'fkSectionBestPractice' => '1692',
    'nControlType' => '0',
    'sName' => '1.4.2 - PCI',
    'tDescription' => '1.4.2 Restrict outbound traffic from payment card applications to IP addresses within the DMZ.',
    'tImplementationGuide' => '',
  ),
  367 => 
  array (
    'fkContext' => '2169',
    'fkSectionBestPractice' => '1692',
    'nControlType' => '0',
    'sName' => '1.5 - PCI',
    'tDescription' => '1.5 Implement IP masquerading to prevent internal addresses from being translated and revealed on the Internet. Use technologies that implement RFC 1918 address space, such as port address translation (PAT) or network address translation (NAT).',
    'tImplementationGuide' => '',
  ),
  368 => 
  array (
    'fkContext' => '2170',
    'fkSectionBestPractice' => '1693',
    'nControlType' => '0',
    'sName' => '2.1 - PCI',
    'tDescription' => '2.1 Always change vendor-supplied defaults before installing a system on the network (for example, include passwords, simple network management protocol (SNMP) community strings, and elimination of unnecessary accounts).',
    'tImplementationGuide' => '',
  ),
  369 => 
  array (
    'fkContext' => '2171',
    'fkSectionBestPractice' => '1693',
    'nControlType' => '0',
    'sName' => '2.1.1 - PCI',
    'tDescription' => '2.1.1 For wireless environments, change wireless vendor defaults, including but not limited to, wired equivalent privacy (WEP) keys, default service set identifier (SSID), passwords, and SNMP community strings. Disable SSID broadcasts. Enable WiFi protected access (WPA and WPA2) technology for encryption and authentication when WPA-capable.',
    'tImplementationGuide' => '',
  ),
  370 => 
  array (
    'fkContext' => '2172',
    'fkSectionBestPractice' => '1693',
    'nControlType' => '0',
    'sName' => '2.2 - PCI',
    'tDescription' => '2.2 Develop configuration standards for all system components. Assure that these standards address all known security vulnerabilities and are consistent with industry-accepted system hardening standards as defined, for example, by SysAdmin Audit Network Security Network (SANS), National Institute of Standards Technology (NIST), and Center for Internet Security (CIS).',
    'tImplementationGuide' => '',
  ),
  371 => 
  array (
    'fkContext' => '2173',
    'fkSectionBestPractice' => '1693',
    'nControlType' => '0',
    'sName' => '2.2.1 - PCI',
    'tDescription' => '2.2.1 Implement only one primary function per server (for example, web servers, database servers, and DNS should be implemented on separate servers)',
    'tImplementationGuide' => '',
  ),
  372 => 
  array (
    'fkContext' => '2174',
    'fkSectionBestPractice' => '1693',
    'nControlType' => '0',
    'sName' => '2.2.2 - PCI',
    'tDescription' => '2.2.2 Disable all unnecessary and insecure services and protocols (services and protocols not directly needed to perform the devices\'\' specified function)',
    'tImplementationGuide' => '',
  ),
  373 => 
  array (
    'fkContext' => '2175',
    'fkSectionBestPractice' => '1693',
    'nControlType' => '0',
    'sName' => '2.2.3 - PCI',
    'tDescription' => '2.2.3 Configure system security parameters to prevent misuse',
    'tImplementationGuide' => '',
  ),
  374 => 
  array (
    'fkContext' => '2176',
    'fkSectionBestPractice' => '1693',
    'nControlType' => '0',
    'sName' => '2.2.4 - PCI',
    'tDescription' => '2.2.4 Remove all unnecessary functionality, such as scripts, drivers, features, subsystems, file systems, and unnecessary web servers.',
    'tImplementationGuide' => '',
  ),
  375 => 
  array (
    'fkContext' => '2177',
    'fkSectionBestPractice' => '1693',
    'nControlType' => '0',
    'sName' => '2.3 - PCI',
    'tDescription' => '2.3 Encrypt all non-console administrative access. Use technologies such as SSH, VPN, or SSL/TLS (transport layer security) for web-based management and other non-console administrative access.',
    'tImplementationGuide' => '',
  ),
  376 => 
  array (
    'fkContext' => '2178',
    'fkSectionBestPractice' => '1693',
    'nControlType' => '0',
    'sName' => '2.4 - PCI',
    'tDescription' => '2.4 Hosting providers must protect each entity\'\'s hosted environment and data. These providers must meet specific requirements as detailed in Appendix A: "PCI DSS Applicability for Hosting Providers."',
    'tImplementationGuide' => '',
  ),
  377 => 
  array (
    'fkContext' => '2179',
    'fkSectionBestPractice' => '1695',
    'nControlType' => '0',
    'sName' => '3.1 - PCI',
    'tDescription' => '3.1 Keep cardholder data storage to a minimum. Develop a data retention and disposal policy. Limit storage amount and retention time to that which is required for business, legal, and/or regulatory purposes, as documented in the data retention policy.',
    'tImplementationGuide' => '',
  ),
  378 => 
  array (
    'fkContext' => '2180',
    'fkSectionBestPractice' => '1695',
    'nControlType' => '0',
    'sName' => '3.2 - PCI',
    'tDescription' => '3.2 Do not store sensitive authentication data subsequent to authorization (even if encrypted). Sensitive authentication data includes the data as cited in the following Requirements 3.2.1 through 3.2.3:',
    'tImplementationGuide' => '',
  ),
  379 => 
  array (
    'fkContext' => '2181',
    'fkSectionBestPractice' => '1695',
    'nControlType' => '0',
    'sName' => '3.2.1 - PCI',
    'tDescription' => '3.2.1 Do not store the full contents of any track from the magnetic stripe (that is on the back of a card, in a chip or elsewhere). This data is alternatively called full track, track, track 1, track 2, and magnetic stripe data In the normal course of business, the following data elements from the magnetic stripe may need to be retained: the accountholder\'\'s name, primary account number (PAN), expiration date, and service code. To minimize risk, store only those data elements needed for business. NEVER store the card verification code or value or PIN verification value data elements. Note: See \'\'Glossary\'\' for additional information.',
    'tImplementationGuide' => '',
  ),
  380 => 
  array (
    'fkContext' => '2182',
    'fkSectionBestPractice' => '1695',
    'nControlType' => '0',
    'sName' => '3.2.2 - PCI',
    'tDescription' => '3.2.2 Do not store the card-validation code or value (three-digit or four-digit number printed on the front or back of a payment card) used to verify card-not-present transactions Note: See "Glossary" for additional information.',
    'tImplementationGuide' => '',
  ),
  381 => 
  array (
    'fkContext' => '2183',
    'fkSectionBestPractice' => '1695',
    'nControlType' => '0',
    'sName' => '3.2.3 - PCI',
    'tDescription' => '3.2.3 Do not store the personal identification number (PIN) or the encrypted PIN block.',
    'tImplementationGuide' => '',
  ),
  382 => 
  array (
    'fkContext' => '2184',
    'fkSectionBestPractice' => '1695',
    'nControlType' => '0',
    'sName' => '3.3 - PCI',
    'tDescription' => '3.3 Mask PAN when displayed (the first six and last four digits are the maximum number of digits to be displayed). Note: This requirement does not apply to employees and other parties with a specific need to see the full PAN;
nor does the requirement supersede stricter requirements in place for displays of cardholder data (for example, for point of sale [POS] receipts).',
    'tImplementationGuide' => '',
  ),
  383 => 
  array (
    'fkContext' => '2185',
    'fkSectionBestPractice' => '1695',
    'nControlType' => '0',
    'sName' => '3.4 - PCI',
    'tDescription' => '3.4 Render PAN, at minimum, unreadable anywhere it is  tored (including data on portable digital media, backup media, in logs, and data received from or stored by wireless networks) by using any of the following approaches: * Strong one-way hash functions (hashed indexes) * Truncation * Index tokens and pads (pads must be securely stored) * Strong cryptography with associated key management processes and procedures. The MINIMUM account information that must be rendered unreadable is the PAN. If for some reason, a company is unable to encrypt  ardholder data, refer to Appendix B: "Compensating Controls for Encryption of Stored Data."',
    'tImplementationGuide' => '',
  ),
  384 => 
  array (
    'fkContext' => '2186',
    'fkSectionBestPractice' => '1695',
    'nControlType' => '0',
    'sName' => '3.4.1 - PCI',
    'tDescription' => '3.4.1 If disk encryption is used (rather than file- or column-level database encryption), logical access must be managed independently of native operating system access control mechanisms (for example, by not using local system or Active Directory accounts). Decryption keys must not be tied to user accounts.',
    'tImplementationGuide' => '',
  ),
  385 => 
  array (
    'fkContext' => '2187',
    'fkSectionBestPractice' => '1695',
    'nControlType' => '0',
    'sName' => '3.5 - PCI',
    'tDescription' => '3.5 Protect encryption keys used for encryption of cardholder data against both disclosure and misuse.',
    'tImplementationGuide' => '',
  ),
  386 => 
  array (
    'fkContext' => '2188',
    'fkSectionBestPractice' => '1695',
    'nControlType' => '0',
    'sName' => '3.5.1 - PCI',
    'tDescription' => '3.5.1 Restrict access to keys to the fewest number of custodians necessary',
    'tImplementationGuide' => '',
  ),
  387 => 
  array (
    'fkContext' => '2189',
    'fkSectionBestPractice' => '1695',
    'nControlType' => '0',
    'sName' => '3.5.2 - PCI',
    'tDescription' => '3.5.2 Store keys securely in the fewest possible locations and forms.',
    'tImplementationGuide' => '',
  ),
  388 => 
  array (
    'fkContext' => '2190',
    'fkSectionBestPractice' => '1695',
    'nControlType' => '0',
    'sName' => '3.6 - PCI',
    'tDescription' => '3.6 Fully document and implement all key management   ocesses and procedures for keys used for encryption of  ardholder data, including the following:',
    'tImplementationGuide' => '',
  ),
  389 => 
  array (
    'fkContext' => '2191',
    'fkSectionBestPractice' => '1695',
    'nControlType' => '0',
    'sName' => '3.6.1 - PCI',
    'tDescription' => '3.6.1 Generation of strong keys',
    'tImplementationGuide' => '',
  ),
  390 => 
  array (
    'fkContext' => '2192',
    'fkSectionBestPractice' => '1695',
    'nControlType' => '0',
    'sName' => '3.6.2 - PCI',
    'tDescription' => '3.6.2 Secure key distribution',
    'tImplementationGuide' => '',
  ),
  391 => 
  array (
    'fkContext' => '2193',
    'fkSectionBestPractice' => '1695',
    'nControlType' => '0',
    'sName' => '3.6.3 - PCI',
    'tDescription' => '3.6.3 Secure key storage',
    'tImplementationGuide' => '',
  ),
  392 => 
  array (
    'fkContext' => '2194',
    'fkSectionBestPractice' => '1695',
    'nControlType' => '0',
    'sName' => '3.6.4 - PCI',
    'tDescription' => '3.6.4 Periodic changing of keys * As deemed necessary and recommended by the associated application (for example, re-keying); preferably automatically * At least annually.',
    'tImplementationGuide' => '',
  ),
  393 => 
  array (
    'fkContext' => '2195',
    'fkSectionBestPractice' => '1695',
    'nControlType' => '0',
    'sName' => '3.6.5 - PCI',
    'tDescription' => '3.6.5 Destruction of old keys',
    'tImplementationGuide' => '',
  ),
  394 => 
  array (
    'fkContext' => '2196',
    'fkSectionBestPractice' => '1695',
    'nControlType' => '0',
    'sName' => '3.6.6 - PCI',
    'tDescription' => '3.6.6 Split knowledge and establishment of dual control of keys (so that it requires two or three people, each knowing only their part of the key, to reconstruct the whole key)',
    'tImplementationGuide' => '',
  ),
  395 => 
  array (
    'fkContext' => '2197',
    'fkSectionBestPractice' => '1695',
    'nControlType' => '0',
    'sName' => '3.6.7 - PCI',
    'tDescription' => '3.6.7 Prevention of unauthorized substitution of keys',
    'tImplementationGuide' => '',
  ),
  396 => 
  array (
    'fkContext' => '2198',
    'fkSectionBestPractice' => '1695',
    'nControlType' => '0',
    'sName' => '3.6.8 - PCI',
    'tDescription' => '3.6.8 Replacement of known or suspected compromised keys',
    'tImplementationGuide' => '',
  ),
  397 => 
  array (
    'fkContext' => '2199',
    'fkSectionBestPractice' => '1695',
    'nControlType' => '0',
    'sName' => '3.6.9 - PCI',
    'tDescription' => '3.6.9 Revocation of old or invalid keys',
    'tImplementationGuide' => '',
  ),
  398 => 
  array (
    'fkContext' => '2200',
    'fkSectionBestPractice' => '1695',
    'nControlType' => '0',
    'sName' => '3.6.10 - PCI',
    'tDescription' => '3.6.10 Requirement for key custodians to sign a form stating that they understand and accept their key-custodian responsibilities.',
    'tImplementationGuide' => '',
  ),
  399 => 
  array (
    'fkContext' => '2201',
    'fkSectionBestPractice' => '1696',
    'nControlType' => '0',
    'sName' => '4.1 - PCI',
    'tDescription' => '4.1 Use strong cryptography and security protocols such as secure sockets layer (SSL) / transport layer security (TLS) and Internet protocol security (IPSEC) to safeguard sensitive cardholder data during transmission over open, public networks. Examples of open, public networks that are in scope of the PCI DSS are the Internet, WiFi (IEEE 802.11x), global system for mobile communications (GSM), and general packet radio service (GPRS).',
    'tImplementationGuide' => '',
  ),
  400 => 
  array (
    'fkContext' => '2202',
    'fkSectionBestPractice' => '1696',
    'nControlType' => '0',
    'sName' => '4.1.1 - PCI',
    'tDescription' => '4.1.1 For wireless networks transmitting cardholder data, encrypt the transmissions by using WiFi protected access (WPA or WPA2) technology, IPSEC VPN, or SSL/TLS. Never rely exclusively on wired equivalent privacy (WEP) to protect confidentiality and access to a wireless LAN. If WEP is used, do the following: * Use with a minimum 104-bit encryption key and 24 bit-initialization value * Use ONLY in conjunction with WiFi protected access (WPA or WPA2) technology, VPN, or SSL/TLS * Rotate shared WEP keys quarterly (or automatically if the technology permits) * Rotate shared WEP keys whenever there are changes in personnel with access to keys * Restrict access based on media access code (MAC) address.',
    'tImplementationGuide' => '',
  ),
  401 => 
  array (
    'fkContext' => '2203',
    'fkSectionBestPractice' => '1696',
    'nControlType' => '0',
    'sName' => '4.2 - PCI',
    'tDescription' => '4.2 Never send unencrypted PANs by e-mail.',
    'tImplementationGuide' => '',
  ),
  402 => 
  array (
    'fkContext' => '2204',
    'fkSectionBestPractice' => '1698',
    'nControlType' => '0',
    'sName' => '5.1 - PCI',
    'tDescription' => '5.1 Deploy anti-virus software on all systems commonly affected by viruses (particularly personal computers and servers) Note: Systems commonly affected by viruses typically do not include UNIX-based operating systems or mainframes.',
    'tImplementationGuide' => '',
  ),
  403 => 
  array (
    'fkContext' => '2205',
    'fkSectionBestPractice' => '1698',
    'nControlType' => '0',
    'sName' => '5.1.1 - PCI',
    'tDescription' => '5.1.1 Ensure that anti-virus programs are capable of detecting, removing, and protecting against other forms of malicious software, including spyware and adware.',
    'tImplementationGuide' => '',
  ),
  404 => 
  array (
    'fkContext' => '2206',
    'fkSectionBestPractice' => '1698',
    'nControlType' => '0',
    'sName' => '5.2 - PCI',
    'tDescription' => '5.2 Ensure that all anti-virus mechanisms are current, actively running, and capable of generating audit logs.',
    'tImplementationGuide' => '',
  ),
  405 => 
  array (
    'fkContext' => '2207',
    'fkSectionBestPractice' => '1699',
    'nControlType' => '0',
    'sName' => '6.1 - PCI',
    'tDescription' => '6.1 Ensure that all system components and software have the latest vendor-supplied security patches installed. Install relevant security patches within one month of release.',
    'tImplementationGuide' => '',
  ),
  406 => 
  array (
    'fkContext' => '2208',
    'fkSectionBestPractice' => '1699',
    'nControlType' => '0',
    'sName' => '6.2 - PCI',
    'tDescription' => '6.2 Establish a process to identify newly discovered security vulnerabilities (for example, subscribe to alert services freely available on the Internet). Update standards to address new vulnerability issues.',
    'tImplementationGuide' => '',
  ),
  407 => 
  array (
    'fkContext' => '2209',
    'fkSectionBestPractice' => '1699',
    'nControlType' => '0',
    'sName' => '6.3 - PCI',
    'tDescription' => '6.3 Develop software applications based on industry best practices and incorporate information security throughout the software development life cycle.',
    'tImplementationGuide' => '',
  ),
  408 => 
  array (
    'fkContext' => '2210',
    'fkSectionBestPractice' => '1699',
    'nControlType' => '0',
    'sName' => '6.3.1 - PCI',
    'tDescription' => '6.3.1 Testing of all security patches and system and software configuration changes before deployment',
    'tImplementationGuide' => '',
  ),
  409 => 
  array (
    'fkContext' => '2211',
    'fkSectionBestPractice' => '1699',
    'nControlType' => '0',
    'sName' => '6.3.2 - PCI',
    'tDescription' => '6.3.2 Separate development, test, and production environments',
    'tImplementationGuide' => '',
  ),
  410 => 
  array (
    'fkContext' => '2212',
    'fkSectionBestPractice' => '1699',
    'nControlType' => '0',
    'sName' => '6.3.3 - PCI',
    'tDescription' => '6.3.3 Separation of duties between development, test, and production environments',
    'tImplementationGuide' => '',
  ),
  411 => 
  array (
    'fkContext' => '2213',
    'fkSectionBestPractice' => '1699',
    'nControlType' => '0',
    'sName' => '6.3.4 - PCI',
    'tDescription' => '6.3.4 Production data (live PANs) are not used for testing or development',
    'tImplementationGuide' => '',
  ),
  412 => 
  array (
    'fkContext' => '2214',
    'fkSectionBestPractice' => '1699',
    'nControlType' => '0',
    'sName' => '6.3.5 - PCI',
    'tDescription' => '6.3.5 Removal of test data and accounts before production systems become active',
    'tImplementationGuide' => '',
  ),
  413 => 
  array (
    'fkContext' => '2215',
    'fkSectionBestPractice' => '1699',
    'nControlType' => '0',
    'sName' => '6.3.6 - PCI',
    'tDescription' => '6.3.6 Removal of custom application accounts, usernames, and passwords before applications become active or are released to customers',
    'tImplementationGuide' => '',
  ),
  414 => 
  array (
    'fkContext' => '2216',
    'fkSectionBestPractice' => '1699',
    'nControlType' => '0',
    'sName' => '6.3.7 - PCI',
    'tDescription' => '6.3.7 Review of custom code prior to release to production or customers in order to identify any potential coding vulnerability.',
    'tImplementationGuide' => '',
  ),
  415 => 
  array (
    'fkContext' => '2217',
    'fkSectionBestPractice' => '1699',
    'nControlType' => '0',
    'sName' => '6.4 - PCI',
    'tDescription' => '6.4 Follow change control procedures for all system and software configuration changes. The procedures must include the following:',
    'tImplementationGuide' => '',
  ),
  416 => 
  array (
    'fkContext' => '2218',
    'fkSectionBestPractice' => '1699',
    'nControlType' => '0',
    'sName' => '6.4.1 - PCI',
    'tDescription' => '6.4.1 Documentation of impact',
    'tImplementationGuide' => '',
  ),
  417 => 
  array (
    'fkContext' => '2219',
    'fkSectionBestPractice' => '1699',
    'nControlType' => '0',
    'sName' => '6.4.2 - PCI',
    'tDescription' => '6.4.2 Management sign-off by appropriate parties',
    'tImplementationGuide' => '',
  ),
  418 => 
  array (
    'fkContext' => '2220',
    'fkSectionBestPractice' => '1699',
    'nControlType' => '0',
    'sName' => '6.4.3 - PCI',
    'tDescription' => '6.4.3 Testing of operational functionality',
    'tImplementationGuide' => '',
  ),
  419 => 
  array (
    'fkContext' => '2221',
    'fkSectionBestPractice' => '1699',
    'nControlType' => '0',
    'sName' => '6.4.4 - PCI',
    'tDescription' => '6.4.4 Back-out procedures',
    'tImplementationGuide' => '',
  ),
  420 => 
  array (
    'fkContext' => '2222',
    'fkSectionBestPractice' => '1699',
    'nControlType' => '0',
    'sName' => '6.5 - PCI',
    'tDescription' => '6.5 Develop all web applications based on secure coding guidelines such as the Open Web Application Security Project guidelines. Review custom application code to identify coding vulnerabilities. Cover prevention of common coding vulnerabilities in software development processes, to include the following:',
    'tImplementationGuide' => '',
  ),
  421 => 
  array (
    'fkContext' => '2223',
    'fkSectionBestPractice' => '1699',
    'nControlType' => '0',
    'sName' => '6.5.1 - PCI',
    'tDescription' => '6.5.1 Unvalidated input',
    'tImplementationGuide' => '',
  ),
  422 => 
  array (
    'fkContext' => '2224',
    'fkSectionBestPractice' => '1699',
    'nControlType' => '0',
    'sName' => '6.5.2 - PCI',
    'tDescription' => '6.5.2 Broken access control (for example, malicious use of user IDs)',
    'tImplementationGuide' => '',
  ),
  423 => 
  array (
    'fkContext' => '2225',
    'fkSectionBestPractice' => '1699',
    'nControlType' => '0',
    'sName' => '6.5.3 - PCI',
    'tDescription' => '6.5.3 Broken authentication and session management (use of account credentials and session cookies)',
    'tImplementationGuide' => '',
  ),
  424 => 
  array (
    'fkContext' => '2226',
    'fkSectionBestPractice' => '1699',
    'nControlType' => '0',
    'sName' => '6.5.4 - PCI',
    'tDescription' => '6.5.4 Cross-site scripting (XSS) attacks',
    'tImplementationGuide' => '',
  ),
  425 => 
  array (
    'fkContext' => '2227',
    'fkSectionBestPractice' => '1699',
    'nControlType' => '0',
    'sName' => '6.5.5 - PCI',
    'tDescription' => '6.5.5 Buffer overflows',
    'tImplementationGuide' => '',
  ),
  426 => 
  array (
    'fkContext' => '2228',
    'fkSectionBestPractice' => '1699',
    'nControlType' => '0',
    'sName' => '6.5.6 - PCI',
    'tDescription' => '6.5.6 Injection flaws (for example, structured query language (SQL) injection)',
    'tImplementationGuide' => '',
  ),
  427 => 
  array (
    'fkContext' => '2229',
    'fkSectionBestPractice' => '1699',
    'nControlType' => '0',
    'sName' => '6.5.7 - PCI',
    'tDescription' => '6.5.7 Improper error handling',
    'tImplementationGuide' => '',
  ),
  428 => 
  array (
    'fkContext' => '2230',
    'fkSectionBestPractice' => '1699',
    'nControlType' => '0',
    'sName' => '6.5.8 - PCI',
    'tDescription' => '6.5.8 Insecure storage',
    'tImplementationGuide' => '',
  ),
  429 => 
  array (
    'fkContext' => '2231',
    'fkSectionBestPractice' => '1699',
    'nControlType' => '0',
    'sName' => '6.5.9 - PCI',
    'tDescription' => '6.5.9 Denial of service',
    'tImplementationGuide' => '',
  ),
  430 => 
  array (
    'fkContext' => '2232',
    'fkSectionBestPractice' => '1699',
    'nControlType' => '0',
    'sName' => '6.5.10 - PCI',
    'tDescription' => '6.5.10 Insecure configuration management',
    'tImplementationGuide' => '',
  ),
  431 => 
  array (
    'fkContext' => '2233',
    'fkSectionBestPractice' => '1699',
    'nControlType' => '0',
    'sName' => '6.6 - PCI',
    'tDescription' => '6.6 Ensure that all web-facing applications are protected against known attacks by applying either of the following methods: * Having all custom application code reviewed for common vulnerabilities by an organization that specializes in application security * Installing an application layer firewall in front of web-facing applications. Note: This method is considered a best practice until June 30, 2008, after which it becomes a requirement.',
    'tImplementationGuide' => '',
  ),
  432 => 
  array (
    'fkContext' => '2234',
    'fkSectionBestPractice' => '1701',
    'nControlType' => '0',
    'sName' => '7.1 - PCI',
    'tDescription' => '7.1 Limit access to computing resources and cardholder information only to those individuals whose job requires such access.',
    'tImplementationGuide' => '',
  ),
  433 => 
  array (
    'fkContext' => '2235',
    'fkSectionBestPractice' => '1701',
    'nControlType' => '0',
    'sName' => '7.2 - PCI',
    'tDescription' => '7.2 Establish a mechanism for systems with multiple users that restricts access based on a user\'\'s need to know and is set to \'\'deny all\'\' unless specifically allowed.',
    'tImplementationGuide' => '',
  ),
  434 => 
  array (
    'fkContext' => '2236',
    'fkSectionBestPractice' => '1702',
    'nControlType' => '0',
    'sName' => '8.1 - PCI',
    'tDescription' => '8.1 Identify all users with a unique user name before  llowing them to access system components or cardholder data.',
    'tImplementationGuide' => '',
  ),
  435 => 
  array (
    'fkContext' => '2237',
    'fkSectionBestPractice' => '1702',
    'nControlType' => '0',
    'sName' => '8.2 - PCI',
    'tDescription' => '8.2 In addition to assigning a unique ID, employ at least one of the following methods to authenticate all users: * Password * Token devices (e.g., SecureID, certificates, or public key) * Biometrics.',
    'tImplementationGuide' => '',
  ),
  436 => 
  array (
    'fkContext' => '2238',
    'fkSectionBestPractice' => '1702',
    'nControlType' => '0',
    'sName' => '8.3 - PCI',
    'tDescription' => '8.3 Implement two-factor authentication for remote access to the network by employees, administrators, and third parties. Use technologies such as remote authentication and dial-in service (RADIUS) or terminal access controller access control system (TACACS) with tokens;or VPN (based on SSL/TLS or IPSEC) with individual certificates.',
    'tImplementationGuide' => '',
  ),
  437 => 
  array (
    'fkContext' => '2239',
    'fkSectionBestPractice' => '1702',
    'nControlType' => '0',
    'sName' => '8.4 - PCI',
    'tDescription' => '8.4 Encrypt all passwords during transmission and storage on all system components.',
    'tImplementationGuide' => '',
  ),
  438 => 
  array (
    'fkContext' => '2240',
    'fkSectionBestPractice' => '1702',
    'nControlType' => '0',
    'sName' => '8.5 - PCI',
    'tDescription' => '8.5 Ensure proper user authentication and password management for non-consumer users and administrators on all system components as follows:',
    'tImplementationGuide' => '',
  ),
  439 => 
  array (
    'fkContext' => '2241',
    'fkSectionBestPractice' => '1702',
    'nControlType' => '0',
    'sName' => '8.5.1 - PCI',
    'tDescription' => '8.5.1 Control addition, deletion, and modification of user IDs, credentials, and other identifier objects',
    'tImplementationGuide' => '',
  ),
  440 => 
  array (
    'fkContext' => '2242',
    'fkSectionBestPractice' => '1702',
    'nControlType' => '0',
    'sName' => '8.5.2 - PCI',
    'tDescription' => '8.5.2 Verify user identity before performing password resets',
    'tImplementationGuide' => '',
  ),
  441 => 
  array (
    'fkContext' => '2243',
    'fkSectionBestPractice' => '1702',
    'nControlType' => '0',
    'sName' => '8.5.3 - PCI',
    'tDescription' => '8.5.3 Set first-time passwords to a unique value for each user and change immediately after the first use',
    'tImplementationGuide' => '',
  ),
  442 => 
  array (
    'fkContext' => '2244',
    'fkSectionBestPractice' => '1702',
    'nControlType' => '0',
    'sName' => '8.5.4 - PCI',
    'tDescription' => '8.5.4 Immediately revoke access for any terminated users',
    'tImplementationGuide' => '',
  ),
  443 => 
  array (
    'fkContext' => '2245',
    'fkSectionBestPractice' => '1702',
    'nControlType' => '0',
    'sName' => '8.5.5 - PCI',
    'tDescription' => '8.5.5 Remove inactive user accounts at least every 90 days',
    'tImplementationGuide' => '',
  ),
  444 => 
  array (
    'fkContext' => '2246',
    'fkSectionBestPractice' => '1702',
    'nControlType' => '0',
    'sName' => '8.5.6 - PCI',
    'tDescription' => '8.5.6 Enable accounts used by vendors for remote maintenance only during the time period needed',
    'tImplementationGuide' => '',
  ),
  445 => 
  array (
    'fkContext' => '2247',
    'fkSectionBestPractice' => '1702',
    'nControlType' => '0',
    'sName' => '8.5.7 - PCI',
    'tDescription' => '8.5.7 Communicate password procedures and policies to all users who have access to cardholder data',
    'tImplementationGuide' => '',
  ),
  446 => 
  array (
    'fkContext' => '2248',
    'fkSectionBestPractice' => '1702',
    'nControlType' => '0',
    'sName' => '8.5.8 - PCI',
    'tDescription' => '8.5.8 Do not use group, shared, or generic accounts and passwords',
    'tImplementationGuide' => '',
  ),
  447 => 
  array (
    'fkContext' => '2249',
    'fkSectionBestPractice' => '1702',
    'nControlType' => '0',
    'sName' => '8.5.9 - PCI',
    'tDescription' => '8.5.9 Change user passwords at least every 90 days',
    'tImplementationGuide' => '',
  ),
  448 => 
  array (
    'fkContext' => '2250',
    'fkSectionBestPractice' => '1702',
    'nControlType' => '0',
    'sName' => '8.5.10 - PCI',
    'tDescription' => '8.5.10 Require a minimum password length of at least seven characters',
    'tImplementationGuide' => '',
  ),
  449 => 
  array (
    'fkContext' => '2251',
    'fkSectionBestPractice' => '1702',
    'nControlType' => '0',
    'sName' => '8.5.11 - PCI',
    'tDescription' => '8.5.11 Use passwords containing both numeric and alphabetic characters',
    'tImplementationGuide' => '',
  ),
  450 => 
  array (
    'fkContext' => '2252',
    'fkSectionBestPractice' => '1702',
    'nControlType' => '0',
    'sName' => '8.5.12 - PCI',
    'tDescription' => '8.5.12 Do not allow an individual to submit a new password that is the same as any of the last four passwords he or she has used',
    'tImplementationGuide' => '',
  ),
  451 => 
  array (
    'fkContext' => '2253',
    'fkSectionBestPractice' => '1702',
    'nControlType' => '0',
    'sName' => '8.5.13 - PCI',
    'tDescription' => '8.5.13 Limit repeated access attempts by locking out the user ID after not more than six attempts',
    'tImplementationGuide' => '',
  ),
  452 => 
  array (
    'fkContext' => '2254',
    'fkSectionBestPractice' => '1702',
    'nControlType' => '0',
    'sName' => '8.5.14 - PCI',
    'tDescription' => '8.5.14 Set the lockout duration to thirty minutes or until administrator enables the user ID',
    'tImplementationGuide' => '',
  ),
  453 => 
  array (
    'fkContext' => '2255',
    'fkSectionBestPractice' => '1702',
    'nControlType' => '0',
    'sName' => '8.5.15 - PCI',
    'tDescription' => '8.5.15 If a session has been idle for more than 15 minutes, require the user to re-enter the password to re-activate the terminal',
    'tImplementationGuide' => '',
  ),
  454 => 
  array (
    'fkContext' => '2256',
    'fkSectionBestPractice' => '1702',
    'nControlType' => '0',
    'sName' => '8.5.16 - PCI',
    'tDescription' => '8.5.16 Authenticate all access to any database containing cardholder data. This includes access by applications, administrators, and all other users',
    'tImplementationGuide' => '',
  ),
  455 => 
  array (
    'fkContext' => '2257',
    'fkSectionBestPractice' => '1703',
    'nControlType' => '0',
    'sName' => '9.1 - PCI',
    'tDescription' => '9.1 Use appropriate facility entry controls to limit and monitor physical access to systems that store, process, or transmit cardholder data.',
    'tImplementationGuide' => '',
  ),
  456 => 
  array (
    'fkContext' => '2258',
    'fkSectionBestPractice' => '1703',
    'nControlType' => '0',
    'sName' => '9.1.1 - PCI',
    'tDescription' => '9.1.1 Use cameras to monitor sensitive areas. Audit collected data and correlate with other entries. Store for at least three months, unless otherwise restricted by law',
    'tImplementationGuide' => '',
  ),
  457 => 
  array (
    'fkContext' => '2259',
    'fkSectionBestPractice' => '1703',
    'nControlType' => '0',
    'sName' => '9.1.2 - PCI',
    'tDescription' => '9.1.2 Restrict physical access to publicly accessible network jacks',
    'tImplementationGuide' => '',
  ),
  458 => 
  array (
    'fkContext' => '2260',
    'fkSectionBestPractice' => '1703',
    'nControlType' => '0',
    'sName' => '9.1.3 - PCI',
    'tDescription' => '9.1.3 Restrict physical access to wireless access points, gateways, and handheld devices.',
    'tImplementationGuide' => '',
  ),
  459 => 
  array (
    'fkContext' => '2261',
    'fkSectionBestPractice' => '1703',
    'nControlType' => '0',
    'sName' => '9.2 - PCI',
    'tDescription' => '9.2 Develop procedures to help all personnel easily distinguish between employees and visitors, especially in areas where cardholder data is accessible. "Employee" refers to full-time and part-time employees, temporary employees and personnel, and consultants who are "resident" on the entity\'\'s site. A "visitor" is defined as a vendor, guest of an employee, service personnel, or anyone who needs to enter the facility for a short duration, usually not more than one day.',
    'tImplementationGuide' => '',
  ),
  460 => 
  array (
    'fkContext' => '2262',
    'fkSectionBestPractice' => '1703',
    'nControlType' => '0',
    'sName' => '9.3 - PCI',
    'tDescription' => '9.3 Make sure all visitors are handled as follows:',
    'tImplementationGuide' => '',
  ),
  461 => 
  array (
    'fkContext' => '2263',
    'fkSectionBestPractice' => '1703',
    'nControlType' => '0',
    'sName' => '9.3.1 - PCI',
    'tDescription' => '9.3.1 Authorized before entering areas where cardholder data is processed or maintained',
    'tImplementationGuide' => '',
  ),
  462 => 
  array (
    'fkContext' => '2264',
    'fkSectionBestPractice' => '1703',
    'nControlType' => '0',
    'sName' => '9.3.2 - PCI',
    'tDescription' => '9.3.2 Given a physical token (for example, a badge or access device) that expires and that identifies the visitors as non-employees',
    'tImplementationGuide' => '',
  ),
  463 => 
  array (
    'fkContext' => '2265',
    'fkSectionBestPractice' => '1703',
    'nControlType' => '0',
    'sName' => '9.3.3 - PCI',
    'tDescription' => '9.3.3 Asked to surrender the physical token before leaving the facility or at the date of expiration.',
    'tImplementationGuide' => '',
  ),
  464 => 
  array (
    'fkContext' => '2266',
    'fkSectionBestPractice' => '1703',
    'nControlType' => '0',
    'sName' => '9.4 - PCI',
    'tDescription' => '9.4 Use a visitor log to maintain a physical audit trail of visitor activity. Retain this log for a minimum of three months, unless otherwise restricted by law.',
    'tImplementationGuide' => '',
  ),
  465 => 
  array (
    'fkContext' => '2267',
    'fkSectionBestPractice' => '1703',
    'nControlType' => '0',
    'sName' => '9.5 - PCI',
    'tDescription' => '9.5 Store media back-ups in a secure location, preferably in an off-site facility, such as an alternate or backup site, or a commercial storage facility.',
    'tImplementationGuide' => '',
  ),
  466 => 
  array (
    'fkContext' => '2268',
    'fkSectionBestPractice' => '1703',
    'nControlType' => '0',
    'sName' => '9.6 - PCI',
    'tDescription' => '9.6 Physically secure all paper and electronic media (including computers, electronic media, networking and communications hardware, telecommunication lines, paper receipts, paper reports, and faxes) that contain cardholder data.',
    'tImplementationGuide' => '',
  ),
  467 => 
  array (
    'fkContext' => '2269',
    'fkSectionBestPractice' => '1703',
    'nControlType' => '0',
    'sName' => '9.7 - PCI',
    'tDescription' => '9.7 Maintain strict control over the internal or external distribution of any kind of media that contains cardholder data including the following:',
    'tImplementationGuide' => '',
  ),
  468 => 
  array (
    'fkContext' => '2270',
    'fkSectionBestPractice' => '1703',
    'nControlType' => '0',
    'sName' => '9.7.1 - PCI',
    'tDescription' => '9.7.1 Classify the media so it can be identified as confidential',
    'tImplementationGuide' => '',
  ),
  469 => 
  array (
    'fkContext' => '2271',
    'fkSectionBestPractice' => '1703',
    'nControlType' => '0',
    'sName' => '9.7.2 - PCI',
    'tDescription' => '9.7.2 Send the media by secured courier or other delivery method that can be accurately tracked.',
    'tImplementationGuide' => '',
  ),
  470 => 
  array (
    'fkContext' => '2272',
    'fkSectionBestPractice' => '1703',
    'nControlType' => '0',
    'sName' => '9.8 - PCI',
    'tDescription' => '9.8 Ensure management approves any and all media that is moved from a secured area  especially when media is distributed to individuals).',
    'tImplementationGuide' => '',
  ),
  471 => 
  array (
    'fkContext' => '2273',
    'fkSectionBestPractice' => '1703',
    'nControlType' => '0',
    'sName' => '9.9 - PCI',
    'tDescription' => '9.9 Maintain strict control over the storage and accessibility of media that contains cardholder data.',
    'tImplementationGuide' => '',
  ),
  472 => 
  array (
    'fkContext' => '2274',
    'fkSectionBestPractice' => '1703',
    'nControlType' => '0',
    'sName' => '9.9.1 - PCI',
    'tDescription' => '9.9.1 Properly inventory all media and make sure it is securely stored.',
    'tImplementationGuide' => '',
  ),
  473 => 
  array (
    'fkContext' => '2275',
    'fkSectionBestPractice' => '1703',
    'nControlType' => '0',
    'sName' => '9.10 - PCI',
    'tDescription' => '9.10 Destroy media containing cardholder data when it is no longer needed for business or  legal reasons as follows:',
    'tImplementationGuide' => '',
  ),
  474 => 
  array (
    'fkContext' => '2276',
    'fkSectionBestPractice' => '1703',
    'nControlType' => '0',
    'sName' => '9.10.1 - PCI',
    'tDescription' => '9.10.1 Cross-cut shred, incinerate, or pulp hardcopy materials',
    'tImplementationGuide' => '',
  ),
  475 => 
  array (
    'fkContext' => '2277',
    'fkSectionBestPractice' => '1703',
    'nControlType' => '0',
    'sName' => '9.10.2 - PCI',
    'tDescription' => '9.10.2 Purge, degauss, shred, or otherwise destroy electronic media so that cardholder data cannot be reconstructed.',
    'tImplementationGuide' => '',
  ),
  476 => 
  array (
    'fkContext' => '2278',
    'fkSectionBestPractice' => '1705',
    'nControlType' => '0',
    'sName' => '10.1 - PCI',
    'tDescription' => '10.1 Establish a process for linking all access to system components (especially access done with administrative privileges such as root) to each individual user.',
    'tImplementationGuide' => '',
  ),
  477 => 
  array (
    'fkContext' => '2279',
    'fkSectionBestPractice' => '1705',
    'nControlType' => '0',
    'sName' => '10.2 - PCI',
    'tDescription' => '10.2 Implement automated audit trails for all system components to reconstruct the following events:',
    'tImplementationGuide' => '',
  ),
  478 => 
  array (
    'fkContext' => '2280',
    'fkSectionBestPractice' => '1705',
    'nControlType' => '0',
    'sName' => '10.2.1 - PCI',
    'tDescription' => '10.2.1 All individual user accesses to cardholder data',
    'tImplementationGuide' => '',
  ),
  479 => 
  array (
    'fkContext' => '2281',
    'fkSectionBestPractice' => '1705',
    'nControlType' => '0',
    'sName' => '10.2.2 - PCI',
    'tDescription' => '10.2.2 All actions taken by any individual with root or administrative privileges',
    'tImplementationGuide' => '',
  ),
  480 => 
  array (
    'fkContext' => '2282',
    'fkSectionBestPractice' => '1705',
    'nControlType' => '0',
    'sName' => '10.2.3 - PCI',
    'tDescription' => '10.2.3 Access to all audit trails',
    'tImplementationGuide' => '',
  ),
  481 => 
  array (
    'fkContext' => '2283',
    'fkSectionBestPractice' => '1705',
    'nControlType' => '0',
    'sName' => '10.2.4 - PCI',
    'tDescription' => '10.2.4 Invalid logical access attempts',
    'tImplementationGuide' => '',
  ),
  482 => 
  array (
    'fkContext' => '2284',
    'fkSectionBestPractice' => '1705',
    'nControlType' => '0',
    'sName' => '10.2.5 - PCI',
    'tDescription' => '10.2 5 Use of identification and authentication mechanisms',
    'tImplementationGuide' => '',
  ),
  483 => 
  array (
    'fkContext' => '2285',
    'fkSectionBestPractice' => '1705',
    'nControlType' => '0',
    'sName' => '10.2.6 - PCI',
    'tDescription' => '10.2.6 Initialization of the audit logs',
    'tImplementationGuide' => '',
  ),
  484 => 
  array (
    'fkContext' => '2286',
    'fkSectionBestPractice' => '1705',
    'nControlType' => '0',
    'sName' => '10.2.7 - PCI',
    'tDescription' => '10.2.7 Creation and deletion of system-level objects.',
    'tImplementationGuide' => '',
  ),
  485 => 
  array (
    'fkContext' => '2287',
    'fkSectionBestPractice' => '1705',
    'nControlType' => '0',
    'sName' => '10.3 - PCI',
    'tDescription' => '10.3 Record at least the following audit trail entries for all system components for each event:',
    'tImplementationGuide' => '',
  ),
  486 => 
  array (
    'fkContext' => '2288',
    'fkSectionBestPractice' => '1705',
    'nControlType' => '0',
    'sName' => '10.3.1 - PCI',
    'tDescription' => '10.3.1 User identification',
    'tImplementationGuide' => '',
  ),
  487 => 
  array (
    'fkContext' => '2289',
    'fkSectionBestPractice' => '1705',
    'nControlType' => '0',
    'sName' => '10.5.2 - PCI',
    'tDescription' => '10.5.2 Protect audit trail files from unauthorized modifications',
    'tImplementationGuide' => '',
  ),
  488 => 
  array (
    'fkContext' => '2290',
    'fkSectionBestPractice' => '1705',
    'nControlType' => '0',
    'sName' => '10.5.3 - PCI',
    'tDescription' => '10.5.3 Promptly back-up audit trail files to a centralized log server or media that is difficult to alter',
    'tImplementationGuide' => '',
  ),
  489 => 
  array (
    'fkContext' => '2291',
    'fkSectionBestPractice' => '1705',
    'nControlType' => '0',
    'sName' => '10.5.4 - PCI',
    'tDescription' => '10.5.4 Copy logs for wireless networks onto a log server on the internal LAN.',
    'tImplementationGuide' => '',
  ),
  490 => 
  array (
    'fkContext' => '2292',
    'fkSectionBestPractice' => '1705',
    'nControlType' => '0',
    'sName' => '10.5.5 - PCI',
    'tDescription' => '10.5.5 Use file integrity monitoring and change detection software on logs to ensure that existing log data cannot be changed without generating alerts (although new data being added should not cause an alert).',
    'tImplementationGuide' => '',
  ),
  491 => 
  array (
    'fkContext' => '2293',
    'fkSectionBestPractice' => '1705',
    'nControlType' => '0',
    'sName' => '10.6 - PCI',
    'tDescription' => '10.6 Review logs for all system components at least daily. Log reviews must include those servers that perform security functions like intrusion detection system (IDS) and authentication, authorization, and accounting protocol (AAA) servers (for example, RADIUS). Note: Log harvesting, parsing, and alerting tools may be used to achieve compliance with Requirement 10.6.',
    'tImplementationGuide' => '',
  ),
  492 => 
  array (
    'fkContext' => '2294',
    'fkSectionBestPractice' => '1705',
    'nControlType' => '0',
    'sName' => '10.7 - PCI',
    'tDescription' => '10.7 Retain audit trail history for at least one year, with a minimum of three months online availability.',
    'tImplementationGuide' => '',
  ),
  493 => 
  array (
    'fkContext' => '2295',
    'fkSectionBestPractice' => '1706',
    'nControlType' => '0',
    'sName' => '11.1 - PCI',
    'tDescription' => '11.1 Test security controls, limitations, network connections, and restrictions annually to assure the ability to adequately identify and to stop any unauthorized access attempts. Use a wireless analyzer at least quarterly to identify all wireless devices in use.',
    'tImplementationGuide' => '',
  ),
  494 => 
  array (
    'fkContext' => '2296',
    'fkSectionBestPractice' => '1706',
    'nControlType' => '0',
    'sName' => '11.2 - PCI',
    'tDescription' => '11.2 Run internal and external network vulnerability scans at least quarterly and after any significant change in the network (such as new system component installations, changes in network topology, firewall rule modifications, product upgrades). Note: Quarterly external vulnerability scans must be performed by a scan vendor qualified by the payment card industry. Scans conducted after network changes may be performed by the company\'\'s internal staff.',
    'tImplementationGuide' => '',
  ),
  495 => 
  array (
    'fkContext' => '2297',
    'fkSectionBestPractice' => '1706',
    'nControlType' => '0',
    'sName' => '11.3 - PCI',
    'tDescription' => '11.3 Perform penetration testing at least once a year and after any significant infrastructure or application upgrade or modification (such as an operating system upgrade, a sub-network added to the environment, or a web server added to the environment). These penetration tests must include the following:',
    'tImplementationGuide' => '',
  ),
  496 => 
  array (
    'fkContext' => '2298',
    'fkSectionBestPractice' => '1706',
    'nControlType' => '0',
    'sName' => '11.3.1 - PCI',
    'tDescription' => '11.3.1 Network-layer penetration tests',
    'tImplementationGuide' => '',
  ),
  497 => 
  array (
    'fkContext' => '2299',
    'fkSectionBestPractice' => '1706',
    'nControlType' => '0',
    'sName' => '11.3.2 - PCI',
    'tDescription' => '11.3.2 Application-layer penetration tests.',
    'tImplementationGuide' => '',
  ),
  498 => 
  array (
    'fkContext' => '2300',
    'fkSectionBestPractice' => '1706',
    'nControlType' => '0',
    'sName' => '11.4 - PCI',
    'tDescription' => '11.4 Use network intrusion detection systems, host-based intrusion detection systems, and intrusion prevention systems to monitor all network traffic and alert personnel to suspected compromises. Keep all intrusion detection and prevention engines up-to-date.',
    'tImplementationGuide' => '',
  ),
  499 => 
  array (
    'fkContext' => '2301',
    'fkSectionBestPractice' => '1706',
    'nControlType' => '0',
    'sName' => '11.5 - PCI',
    'tDescription' => '11.5 Deploy file integrity monitoring software to alert personnel to unauthorized modification of critical system or content files; and configure the software to perform critical file comparisons at least weekly. Critical files are not necessarily only those containing cardholder data. For file integrity monitoring purposes, critical files are usually those that do not regularly change, but the modification of which could indicate a system compromise or risk of compromise. File integrity monitoring products usually come pre-configured with critical files for the related operating system. Other critical files, such as those for custom applications, must be evaluated and defined by the entity (that is the merchant or service provider).',
    'tImplementationGuide' => '',
  ),
  500 => 
  array (
    'fkContext' => '2302',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.1 - PCI',
    'tDescription' => '12.1 Establish, publish, maintain, and disseminate a security policy that accomplishes the following:',
    'tImplementationGuide' => '',
  ),
  501 => 
  array (
    'fkContext' => '2303',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.1.1 - PCI',
    'tDescription' => '12.1.1 Addresses all requirements in this specification',
    'tImplementationGuide' => '',
  ),
  502 => 
  array (
    'fkContext' => '2304',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.1.2 - PCI',
    'tDescription' => '12.1.2 Includes an annual process that identifies threats and vulnerabilities, and results in a formal risk assessment',
    'tImplementationGuide' => '',
  ),
  503 => 
  array (
    'fkContext' => '2305',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.1.3 - PCI',
    'tDescription' => '12.1.3 Includes a review at least once a year and updates when the environment changes.',
    'tImplementationGuide' => '',
  ),
  504 => 
  array (
    'fkContext' => '2306',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.2 - PCI',
    'tDescription' => '12.2 Develop daily operational security procedures that are consistent with requirements in this specification (for example, user account maintenance procedures, and log review procedures).',
    'tImplementationGuide' => '',
  ),
  505 => 
  array (
    'fkContext' => '2307',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.3 - PCI',
    'tDescription' => '12.3 Develop usage policies for critical employee-facing technologies (such as modems and wireless) to define proper use of these technologies for all employees and contractors. Ensure these usage policies require the following:',
    'tImplementationGuide' => '',
  ),
  506 => 
  array (
    'fkContext' => '2308',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.3.1 - PCI',
    'tDescription' => '12.3.1 Explicit management approval',
    'tImplementationGuide' => '',
  ),
  507 => 
  array (
    'fkContext' => '2309',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.3.2 - PCI',
    'tDescription' => '12.3.2 Authentication for use of the technology',
    'tImplementationGuide' => '',
  ),
  508 => 
  array (
    'fkContext' => '2310',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.3.3 - PCI',
    'tDescription' => '12.3.3 List of all such devices and personnel with access',
    'tImplementationGuide' => '',
  ),
  509 => 
  array (
    'fkContext' => '2311',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.3.4 - PCI',
    'tDescription' => '12.3.4 Labeling of devices with owner, contact information, and purpose',
    'tImplementationGuide' => '',
  ),
  510 => 
  array (
    'fkContext' => '2312',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.3.5 - PCI',
    'tDescription' => '12.3.5 Acceptable uses of the technologies',
    'tImplementationGuide' => '',
  ),
  511 => 
  array (
    'fkContext' => '2313',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.3.6 - PCI',
    'tDescription' => '12.3.6 Acceptable network locations for the technologies',
    'tImplementationGuide' => '',
  ),
  512 => 
  array (
    'fkContext' => '2314',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.3.7 - PCI',
    'tDescription' => '12.3.7 List of company-approved products',
    'tImplementationGuide' => '',
  ),
  513 => 
  array (
    'fkContext' => '2315',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.3.8 - PCI',
    'tDescription' => '12.3.8 Automatic disconnect of modem sessions after a specific period of inactivity',
    'tImplementationGuide' => '',
  ),
  514 => 
  array (
    'fkContext' => '2316',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.3.9 - PCI',
    'tDescription' => '12.3.9 Activation of modems for vendors only when needed by vendors, with immediate deactivation after use',
    'tImplementationGuide' => '',
  ),
  515 => 
  array (
    'fkContext' => '2317',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.3.10 - PCI',
    'tDescription' => '12.3.10 When accessing cardholder data remotely via modem, prohibition of storage of cardholder data onto local hard drives, floppy disks, or other external media. Prohibition of cut-and-paste and print functions during remote access.',
    'tImplementationGuide' => '',
  ),
  516 => 
  array (
    'fkContext' => '2318',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.4 - PCI',
    'tDescription' => '12.4 Ensure that the security policy and procedures clearly define information security responsibilities for all employees and contractors.',
    'tImplementationGuide' => '',
  ),
  517 => 
  array (
    'fkContext' => '2319',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.5 - PCI',
    'tDescription' => '12.5 Assign to an individual or team the following information security management responsibilities:',
    'tImplementationGuide' => '',
  ),
  518 => 
  array (
    'fkContext' => '2320',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.5.1 - PCI',
    'tDescription' => '12.5.1 Establish, document, and distribute security policies and procedures',
    'tImplementationGuide' => '',
  ),
  519 => 
  array (
    'fkContext' => '2321',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.5.2 - PCI',
    'tDescription' => '12.5.2 Monitor and analyze security alerts and information, and distribute to appropriate personnel',
    'tImplementationGuide' => '',
  ),
  520 => 
  array (
    'fkContext' => '2322',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.5.3 - PCI',
    'tDescription' => '12.5.3 Establish, document, and distribute security incident response and escalation procedures to ensure timely and effective handling of all situations',
    'tImplementationGuide' => '',
  ),
  521 => 
  array (
    'fkContext' => '2323',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.5.4 - PCI',
    'tDescription' => '12.5.4 Administer user accounts, including additions, deletions, and modifications',
    'tImplementationGuide' => '',
  ),
  522 => 
  array (
    'fkContext' => '2324',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.5.5 - PCI',
    'tDescription' => '12.5.5 Monitor and control all access to data.',
    'tImplementationGuide' => '',
  ),
  523 => 
  array (
    'fkContext' => '2325',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.6 - PCI',
    'tDescription' => '12.6 Implement a formal security awareness program to make all employees aware of the importance of cardholder data security.',
    'tImplementationGuide' => '',
  ),
  524 => 
  array (
    'fkContext' => '2326',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.6.1 - PCI',
    'tDescription' => '12.6.1 Educate employees upon hire and at least annually (for example, by letters, posters, memos, meetings, and promotions)',
    'tImplementationGuide' => '',
  ),
  525 => 
  array (
    'fkContext' => '2327',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.6.2 - PCI',
    'tDescription' => '12.6.2 Require employees to acknowledge in writing that they have read and understood the company\'\'s security policy and procedures.',
    'tImplementationGuide' => '',
  ),
  526 => 
  array (
    'fkContext' => '2328',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.7 - PCI',
    'tDescription' => '12.7 Screen potential employees to minimize the risk of attacks from internal sources. For those employees such as store cashiers who only have access to one card number at a time when facilitating a transaction, this requirement is a recommendation only.',
    'tImplementationGuide' => '',
  ),
  527 => 
  array (
    'fkContext' => '2329',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.8 - PCI',
    'tDescription' => '12.8 If cardholder data is shared with service providers, then contractually the following is required:',
    'tImplementationGuide' => '',
  ),
  528 => 
  array (
    'fkContext' => '2330',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.8.1 - PCI',
    'tDescription' => '12.8.1 Service providers must adhere to the PCI DSS requirements',
    'tImplementationGuide' => '',
  ),
  529 => 
  array (
    'fkContext' => '2331',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.8.2 - PCI',
    'tDescription' => '12.8.2 Agreement that includes an acknowledgement that the service provider is responsible for the security of cardholder data the provider possesses.',
    'tImplementationGuide' => '',
  ),
  530 => 
  array (
    'fkContext' => '2332',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.9 - PCI',
    'tDescription' => '12.9 Implement an incident response plan. Be prepared to respond immediately to a system breach.',
    'tImplementationGuide' => '',
  ),
  531 => 
  array (
    'fkContext' => '2333',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.9.1 - PCI',
    'tDescription' => '12.9.1 Create the incident response plan to be implemented in the event of system compromise. Ensure the plan addresses, at a minimum, specific incident response procedures, business recovery and continuity procedures, data backup processes, roles and responsibilities, and communication and contact strategies (for example, informing the Acquirers and credit card associations)',
    'tImplementationGuide' => '',
  ),
  532 => 
  array (
    'fkContext' => '2334',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.9.2 - PCI',
    'tDescription' => '12.9.2 Test the plan at least annually',
    'tImplementationGuide' => '',
  ),
  533 => 
  array (
    'fkContext' => '2335',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.9.3 - PCI',
    'tDescription' => '12.9.3 Designate specific personnel to be available on a 24/7 basis to respond to alerts',
    'tImplementationGuide' => '',
  ),
  534 => 
  array (
    'fkContext' => '2336',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.9.4 - PCI',
    'tDescription' => '12.9.4 Provide appropriate training to staff with security breach response responsibilities',
    'tImplementationGuide' => '',
  ),
  535 => 
  array (
    'fkContext' => '2337',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.9.5 - PCI',
    'tDescription' => '12.9.5 Include alerts from intrusion detection, intrusion prevention, and file integrity monitoring systems',
    'tImplementationGuide' => '',
  ),
  536 => 
  array (
    'fkContext' => '2338',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.9.6 - PCI',
    'tDescription' => '12.9.6 Develop process to modify and evolve the incident response plan according to lessons learned and to incorporate industry developments.',
    'tImplementationGuide' => '',
  ),
  537 => 
  array (
    'fkContext' => '2339',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.10 - PCI',
    'tDescription' => '12.10 All processors and service providers must maintain and implement policies and procedures to manage connected entities, to include the following:',
    'tImplementationGuide' => '',
  ),
  538 => 
  array (
    'fkContext' => '2340',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.10.1 - PCI',
    'tDescription' => '12.10.1. Maintain a list of connected entities',
    'tImplementationGuide' => '',
  ),
  539 => 
  array (
    'fkContext' => '2341',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.10.2 - PCI',
    'tDescription' => '12.10.2. Ensure proper due diligence is conducted prior to connecting an entity',
    'tImplementationGuide' => '',
  ),
  540 => 
  array (
    'fkContext' => '2342',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.10.3 - PCI',
    'tDescription' => '12.10.3. Ensure the entity is PCI DSS compliant',
    'tImplementationGuide' => '',
  ),
  541 => 
  array (
    'fkContext' => '2343',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.10.4 - PCI',
    'tDescription' => '12.10.4. Connect and disconnect entities by following an established process.',
    'tImplementationGuide' => '',
  ),
  542 => 
  array (
    'fkContext' => '2344',
    'fkSectionBestPractice' => '1705',
    'nControlType' => '1',
    'sName' => '10.3.2 - PCI',
    'tDescription' => '10.3.2 Type of event',
    'tImplementationGuide' => '',
  ),
  543 => 
  array (
    'fkContext' => '2345',
    'fkSectionBestPractice' => '1705',
    'nControlType' => '1',
    'sName' => '10.3.3 - PCI',
    'tDescription' => '10.3.3 Date and time',
    'tImplementationGuide' => '',
  ),
  544 => 
  array (
    'fkContext' => '2346',
    'fkSectionBestPractice' => '1705',
    'nControlType' => '1',
    'sName' => '10.3.4 - PCI',
    'tDescription' => '10.3.4 Success or failure indication',
    'tImplementationGuide' => '',
  ),
  545 => 
  array (
    'fkContext' => '2347',
    'fkSectionBestPractice' => '1705',
    'nControlType' => '1',
    'sName' => '10.3.5 - PCI',
    'tDescription' => '10.3.5 Origination of event',
    'tImplementationGuide' => '',
  ),
  546 => 
  array (
    'fkContext' => '2348',
    'fkSectionBestPractice' => '1705',
    'nControlType' => '1',
    'sName' => '10.3.6 - PCI',
    'tDescription' => '10.3.6 Identity or system compone name of affected data, nt, or resource',
    'tImplementationGuide' => '',
  ),
  547 => 
  array (
    'fkContext' => '2349',
    'fkSectionBestPractice' => '1705',
    'nControlType' => '1',
    'sName' => '10.4 - PCI',
    'tDescription' => '10.4 Synchronize all critical system clocks times',
    'tImplementationGuide' => '',
  ),
  548 => 
  array (
    'fkContext' => '2350',
    'fkSectionBestPractice' => '1705',
    'nControlType' => '1',
    'sName' => '10.5 - PCI',
    'tDescription' => '10.5 Secure audit trails so they cannot be altered',
    'tImplementationGuide' => '',
  ),
  549 => 
  array (
    'fkContext' => '2351',
    'fkSectionBestPractice' => '1705',
    'nControlType' => '1',
    'sName' => '10.5.1 - PCI',
    'tDescription' => '10.5.1 Limit viewing of audit trails to thoswith a job-related need',
    'tImplementationGuide' => '',
  ),
)
?>