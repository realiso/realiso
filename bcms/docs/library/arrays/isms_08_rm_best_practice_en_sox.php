<?php

$laBestPracticeEN_SOX = 
array (
  0 => 
  array (
    'fkContext' => '10364',
    'fkSectionBestPractice' => '10363',
    'nControlType' => '0',
    'sName' => 'PO1.1 IT Value Management',
    'tDescription' => 'Work with the business to ensure that the enterprise portfolio of IT-enabled investments contains programmes that have solid business cases. Recognise that there are mandatory, sustaining and discretionary investments that differ in complexity and degree of freedom in allocating funds. IT processes should provide effective and efficient delivery of the IT components of programmes and early warning of any deviations from plan, including cost, schedule or functionality, that might impact the expected outcomes of the programmes. IT services should be executed against equitable and enforceable service level agreements (SLAs). Accountability for achieving the benefits and controlling the costs should be clearly assigned and monitored. Establish fair, transparent, repeatable and comparable evaluation of business cases, including financial worth, the risk of not delivering a capability and the risk of not realising the expected benefits.',
    'tImplementationGuide' => '',
  ),
  1 => 
  array (
    'fkContext' => '10365',
    'fkSectionBestPractice' => '10363',
    'nControlType' => '0',
    'sName' => 'PO1.2 Business-IT Alignment',
    'tDescription' => 'Establish processes of bi-directional education and reciprocal involvement in strategic planning to achieve business and IT alignment and integration. Mediate between business and IT imperatives so priorities can be mutually agreed.',
    'tImplementationGuide' => '',
  ),
  2 => 
  array (
    'fkContext' => '10366',
    'fkSectionBestPractice' => '10363',
    'nControlType' => '0',
    'sName' => 'PO1.3 Assessment of Current Capability and Performance',
    'tDescription' => 'Assess the current capability and performance of solution and service delivery to establish a baseline against which future requirements can be compared. Define performance in terms of IT\'s contribution to business objectives, functionality, stability, complexity, costs, strengths and weaknesses.',
    'tImplementationGuide' => '',
  ),
  3 => 
  array (
    'fkContext' => '10367',
    'fkSectionBestPractice' => '10363',
    'nControlType' => '0',
    'sName' => 'PO1.4 IT Strategic Plan',
    'tDescription' => 'Create a strategic plan that defines, in co-operation with relevant stakeholders, how IT goals will contribute to the enterprise\'s strategic objectives and related costs and risks. It should include how IT will support IT-enabled investment programmes, IT services and IT assets. IT should define how the objectives will be met, the measurements to be used and the procedures to obtain formal sign-off from the stakeholders. The IT strategic plan should cover investment/operational budget, funding sources, sourcing strategy, acquisition strategy, and legal and regulatory requirements. The strategic plan should be sufficiently detailed to allow for the definition of tactical IT plans.',
    'tImplementationGuide' => '',
  ),
  4 => 
  array (
    'fkContext' => '10368',
    'fkSectionBestPractice' => '10363',
    'nControlType' => '0',
    'sName' => 'PO1.5 IT Tactical Plans',
    'tDescription' => 'Create a portfolio of tactical IT plans that are derived from the IT strategic plan. The tactical plans should address IT-enabled programme investments, IT services and IT assets. The tactical plans should describe required IT initiatives, resource requirements, and how the use of resources and achievement of benefits will be monitored and managed. The tactical plans should be sufficiently detailed to allow the definition of project plans. Actively manage the set of tactical IT plans and initiatives through analysis of project and service portfolios.',
    'tImplementationGuide' => '',
  ),
  5 => 
  array (
    'fkContext' => '10369',
    'fkSectionBestPractice' => '10363',
    'nControlType' => '0',
    'sName' => 'PO1.6 IT Portfolio Management',
    'tDescription' => 'Actively manage with the business the portfolio of IT-enabled investment programmes required to achieve specific strategic business objectives by identifying, defining, evaluating, prioritising, selecting, initiating, managing and controlling programmes. This should include clarifying desired business outcomes, ensuring that programme objectives support achievement of the outcomes, understanding the full scope of effort required to achieve the outcomes, assigning clear accountability with supporting measures, defining projects within the programme, allocating resources and funding, delegating authority, and commissioning required projects at programme launch.',
    'tImplementationGuide' => '',
  ),
  6 => 
  array (
    'fkContext' => '10371',
    'fkSectionBestPractice' => '10370',
    'nControlType' => '0',
    'sName' => 'PO2.1 Enterprise Information Architecture Model',
    'tDescription' => 'Establish and maintain an enterprise information model to enable applications development and decision-supporting activities, consistent with IT plans as described in PO1. The model should facilitate the optimal creation, use and sharing of information by the business in a way that maintains integrity and is flexible, functional, cost-effective, timely, secure and resilient to failure.',
    'tImplementationGuide' => '',
  ),
  7 => 
  array (
    'fkContext' => '10372',
    'fkSectionBestPractice' => '10370',
    'nControlType' => '0',
    'sName' => 'PO2.2 Enterprise Data Dictionary and Data Syntax Rules',
    'tDescription' => 'Maintain an enterprise data dictionary that incorporates the organisation\'s data syntax rules. This dictionary should enable the sharing of data elements amongst applications and systems, promote a common understanding of data amongst IT and business users, and prevent incompatible data elements from being created.',
    'tImplementationGuide' => '',
  ),
  8 => 
  array (
    'fkContext' => '10373',
    'fkSectionBestPractice' => '10370',
    'nControlType' => '0',
    'sName' => 'PO2.3 Data Classification Scheme',
    'tDescription' => 'Establish a classification scheme that applies throughout the enterprise, based on the criticality and sensitivity (e.g., public, confidential, top secret) of enterprise data. This scheme should include details about data ownership; definition of appropriate security levels and protection controls; and a brief description of data retention and destruction requirements, criticality and sensitivity. It should be used as the basis for applying controls such as access controls, archiving or encryption.',
    'tImplementationGuide' => '',
  ),
  9 => 
  array (
    'fkContext' => '10374',
    'fkSectionBestPractice' => '10370',
    'nControlType' => '0',
    'sName' => 'PO2.4 Integrity Management',
    'tDescription' => 'Define and implement procedures to ensure the integrity and consistency of all data stored in electronic form, such as databases, data warehouses and data archives.',
    'tImplementationGuide' => '',
  ),
  10 => 
  array (
    'fkContext' => '10376',
    'fkSectionBestPractice' => '10375',
    'nControlType' => '0',
    'sName' => 'PO3.1 Technological Direction Planning',
    'tDescription' => 'Analyse existing and emerging technologies, and plan which technological direction is appropriate to realise the IT strategy and the business systems architecture. Also identify in the plan which technologies have the potential to create business opportunities. The plan should address systems architecture, technological direction, migration strategies and contingency aspects of infrastructure components.',
    'tImplementationGuide' => '',
  ),
  11 => 
  array (
    'fkContext' => '10377',
    'fkSectionBestPractice' => '10375',
    'nControlType' => '0',
    'sName' => 'PO3.2 Technology Infrastructure Plan',
    'tDescription' => 'Create and maintain a technology infrastructure plan that is in accordance with the IT strategic and tactical plans. The plan should be based on the technological direction and include contingency arrangements and direction for acquisition of technology resources. It should consider changes in the competitive environment, economies of scale for information systems staffing and investments, and improved interoperability of platforms and applications.',
    'tImplementationGuide' => '',
  ),
  12 => 
  array (
    'fkContext' => '10378',
    'fkSectionBestPractice' => '10375',
    'nControlType' => '0',
    'sName' => 'PO3.3 Monitor Future Trends and Regulations',
    'tDescription' => 'Establish a process to monitor the business sector, industry, technology, infrastructure, legal and regulatory environment trends. Incorporate the consequences of these trends into the development of the IT technology infrastructure plan.',
    'tImplementationGuide' => '',
  ),
  13 => 
  array (
    'fkContext' => '10379',
    'fkSectionBestPractice' => '10375',
    'nControlType' => '0',
    'sName' => 'PO3.4 Technology Standards',
    'tDescription' => 'To provide consistent, effective and secure technological solutions enterprisewide, establish a technology forum to provide technology guidelines, advice on infrastructure products and guidance on the selection of technology, and measure compliance with these standards and guidelines. This forum should direct technology standards and practices based on their business relevance, risks and compliance with external requirements.',
    'tImplementationGuide' => '',
  ),
  14 => 
  array (
    'fkContext' => '10380',
    'fkSectionBestPractice' => '10375',
    'nControlType' => '0',
    'sName' => 'PO3.5 IT Architecture Board',
    'tDescription' => 'Establish an IT architecture board to provide architecture guidelines and advice on their application, and to verify compliance. This entity should direct IT architecture design, ensuring that it enables the business strategy and considers regulatory compliance and continuity requirements. This is related/linked to PO2 Define the information architecture.',
    'tImplementationGuide' => '',
  ),
  15 => 
  array (
    'fkContext' => '10382',
    'fkSectionBestPractice' => '10381',
    'nControlType' => '0',
    'sName' => 'PO4.1 IT Process Framework',
    'tDescription' => 'Define an IT process framework to execute the IT strategic plan. This framework should include an IT process structure and relationships (e.g., to manage process gaps and overlaps), ownership, maturity, performance measurement, improvement, compliance, quality targets and plans to achieve them. It should provide integration amongst the processes that are specific to IT, enterprise portfolio management, business processes and business change processes. The IT process framework should be integrated into a quality management system (QMS) and the internal control framework.',
    'tImplementationGuide' => '',
  ),
  16 => 
  array (
    'fkContext' => '10383',
    'fkSectionBestPractice' => '10381',
    'nControlType' => '0',
    'sName' => 'PO4.2 IT Strategy Committee',
    'tDescription' => 'Establish an IT strategy committee at the board level. This committee should ensure that IT governance, as part of enterprise governance, is adequately addressed; advise on strategic direction; and review major investments on behalf of the full board.',
    'tImplementationGuide' => '',
  ),
  17 => 
  array (
    'fkContext' => '10384',
    'fkSectionBestPractice' => '10381',
    'nControlType' => '0',
    'sName' => 'PO4.3 IT Steering Committee',
    'tDescription' => 'Establish an IT steering committee (or equivalent) composed of executive, business and IT management to: - Determine prioritisation of IT-enabled investment programmes in line with the enterprise\'s business strategy and priorities - Track status of projects and resolve resource conflict - Monitor service levels and service improvements',
    'tImplementationGuide' => '',
  ),
  18 => 
  array (
    'fkContext' => '10385',
    'fkSectionBestPractice' => '10381',
    'nControlType' => '0',
    'sName' => 'PO4.4 Organisational Placement of the IT Function',
    'tDescription' => 'Place the IT function in the overall organisational structure with a business model contingent on the importance of IT within the enterprise, specifically its criticality to business strategy and the level of operational dependence on IT. The reporting line of the CIO should be commensurate with the importance of IT within the enterprise.',
    'tImplementationGuide' => '',
  ),
  19 => 
  array (
    'fkContext' => '10386',
    'fkSectionBestPractice' => '10381',
    'nControlType' => '0',
    'sName' => 'PO4.5 IT Organisational Structure',
    'tDescription' => 'Establish an internal and external IT organisational structure that reflects business needs. In addition, put a process in place for periodically reviewing the IT organisational structure to adjust staffing requirements and sourcing strategies to meet expected business objectives and changing circumstances.',
    'tImplementationGuide' => '',
  ),
  20 => 
  array (
    'fkContext' => '10387',
    'fkSectionBestPractice' => '10381',
    'nControlType' => '0',
    'sName' => 'PO4.6 Establishment of Roles and Responsibilities',
    'tDescription' => 'Establish and communicate roles and responsibilities for IT personnel and end users that delineate between IT personnel and end-user authority, responsibilities and accountability for meeting the organisation\'s needs.',
    'tImplementationGuide' => '',
  ),
  21 => 
  array (
    'fkContext' => '10388',
    'fkSectionBestPractice' => '10381',
    'nControlType' => '0',
    'sName' => 'PO4.7 Responsibility for IT Quality Assurance',
    'tDescription' => 'Assign responsibility for the performance of the quality assurance (QA) function and provide the QA group with appropriate QA systems, controls and communications expertise. Ensure that the organisational placement and the responsibilities and size of the QA group satisfy the requirements of the organisation.',
    'tImplementationGuide' => '',
  ),
  22 => 
  array (
    'fkContext' => '10389',
    'fkSectionBestPractice' => '10381',
    'nControlType' => '0',
    'sName' => 'PO4.8 Responsibility for Risk, Security and Compliance',
    'tDescription' => 'Embed ownership and responsibility for IT-related risks within the business at an appropriate senior level. Define and assign roles critical for managing IT risks, including the specific responsibility for information security, physical security and compliance. Establish risk and security management responsibility at the enterprise level to deal with organisationwide issues. Additional security management responsibilities may need to be assigned at a system-specific level to deal with related security issues. Obtain direction from senior management on the appetite for IT risk and approval of any residual IT risks.',
    'tImplementationGuide' => '',
  ),
  23 => 
  array (
    'fkContext' => '10390',
    'fkSectionBestPractice' => '10381',
    'nControlType' => '0',
    'sName' => 'PO4.9 Data and System Ownership',
    'tDescription' => 'Provide the business with procedures and tools, enabling it to address its responsibilities for ownership of data and information systems. Owners should make decisions about classifying information and systems and protecting them in line with this classification.',
    'tImplementationGuide' => '',
  ),
  24 => 
  array (
    'fkContext' => '10391',
    'fkSectionBestPractice' => '10381',
    'nControlType' => '0',
    'sName' => 'PO4.10 Supervision',
    'tDescription' => 'Implement adequate supervisory practices in the IT function to ensure that roles and responsibilities are properly exercised, to assess whether all personnel have sufficient authority and resources to execute their roles and responsibilities, and to generally review KPIs.',
    'tImplementationGuide' => '',
  ),
  25 => 
  array (
    'fkContext' => '10392',
    'fkSectionBestPractice' => '10381',
    'nControlType' => '0',
    'sName' => 'PO4.11 Segregation of Duties',
    'tDescription' => 'Implement a division of roles and responsibilities that reduces the possibility for a single individual to compromise a critical process. Make sure that personnel are performing only authorised duties relevant to their respective jobs and positions.',
    'tImplementationGuide' => '',
  ),
  26 => 
  array (
    'fkContext' => '10393',
    'fkSectionBestPractice' => '10381',
    'nControlType' => '0',
    'sName' => 'PO4.12 IT Staffing',
    'tDescription' => 'Evaluate staffing requirements on a regular basis or upon major changes to the business, operational or IT environments to ensure that the IT function has sufficient resources to adequately and appropriately support the business goals and objectives.',
    'tImplementationGuide' => '',
  ),
  27 => 
  array (
    'fkContext' => '10394',
    'fkSectionBestPractice' => '10381',
    'nControlType' => '0',
    'sName' => 'PO4.13 Key IT Personnel',
    'tDescription' => 'Define and identify key IT personnel (e.g., replacements/backup personnel), and minimise reliance on a single individual performing a critical job function.',
    'tImplementationGuide' => '',
  ),
  28 => 
  array (
    'fkContext' => '10395',
    'fkSectionBestPractice' => '10381',
    'nControlType' => '0',
    'sName' => 'PO4.14 Contracted Staff Policies and Procedures',
    'tDescription' => 'Ensure that consultants and contract personnel who support the IT function know and comply with the organisation\'s policies for the protection of the organisation\'s information assets such that they meet agreed-upon contractual requirements.',
    'tImplementationGuide' => '',
  ),
  29 => 
  array (
    'fkContext' => '10396',
    'fkSectionBestPractice' => '10381',
    'nControlType' => '0',
    'sName' => 'PO4.15 Relationships',
    'tDescription' => 'Establish and maintain an optimal co-ordination, communication and liaison structure between the IT function and various other interests inside and outside the IT function, such as the board, executives, business units, individual users, suppliers, security officers, risk managers, the corporate compliance group, outsourcers and offsite management.',
    'tImplementationGuide' => '',
  ),
  30 => 
  array (
    'fkContext' => '10398',
    'fkSectionBestPractice' => '10397',
    'nControlType' => '0',
    'sName' => 'PO5.1 Financial Management Framework',
    'tDescription' => 'Establish and maintain a financial framework to manage the investment and cost of IT assets and services through portfolios of Itenabled investments, business cases and IT budgets.',
    'tImplementationGuide' => '',
  ),
  31 => 
  array (
    'fkContext' => '10399',
    'fkSectionBestPractice' => '10397',
    'nControlType' => '0',
    'sName' => 'PO5.2 Prioritisation Within IT Budget',
    'tDescription' => 'Implement a decision-making process to prioritise the allocation of IT resources for operations, projects and maintenance to maximise IT\'s contribution to optimising the return on the enterprise\'s portfolio of IT-enabled investment programmes and other IT services and assets.',
    'tImplementationGuide' => '',
  ),
  32 => 
  array (
    'fkContext' => '10400',
    'fkSectionBestPractice' => '10397',
    'nControlType' => '0',
    'sName' => 'PO5.3 IT Budgeting',
    'tDescription' => 'Establish and implement practices to prepare a budget reflecting the priorities established by the enterprise\'s portfolio of IT-enabled investment programmes, and including the ongoing costs of operating and maintaining the current infrastructure. The practices should support development of an overall IT budget as well as development of budgets for individual programmes, with specific emphasis on the IT components of those programmes. The practices should allow for ongoing review, refinement and approval of the overall budget and the budgets for individual programmes.',
    'tImplementationGuide' => '',
  ),
  33 => 
  array (
    'fkContext' => '10401',
    'fkSectionBestPractice' => '10397',
    'nControlType' => '0',
    'sName' => 'PO5.4 Cost Management',
    'tDescription' => 'Implement a cost management process comparing actual costs to budgets. Costs should be monitored and reported. Where there are deviations, these should be identified in a timely manner and the impact of those deviations on programmes should be assessed. Together with the business sponsor of those programmes, appropriate remedial action should be taken and, if necessary, the programme business case should be updated.',
    'tImplementationGuide' => '',
  ),
  34 => 
  array (
    'fkContext' => '10402',
    'fkSectionBestPractice' => '10397',
    'nControlType' => '0',
    'sName' => 'PO5.5 Benefit Management',
    'tDescription' => 'Implement a process to monitor the benefits from providing and maintaining appropriate IT capabilities. IT\'s contribution to the business, either as a component of IT-enabled investment programmes or as part of regular operational support, should be identified and documented in a business case, agreed to, monitored and reported. Reports should be reviewed and, where there are opportunities to improve IT\'s contribution, appropriate actions should be defined and taken. Where changes in IT\'s contribution impact the programme, or where changes to other related projects impact the programme, the programme business case should be updated.',
    'tImplementationGuide' => '',
  ),
  35 => 
  array (
    'fkContext' => '10404',
    'fkSectionBestPractice' => '10403',
    'nControlType' => '0',
    'sName' => 'PO6.1 IT Policy and Control Environment',
    'tDescription' => 'Define the elements of a control environment for IT, aligned with the enterprise\'s management philosophy and operating style. These elements should include expectations/requirements regarding delivery of value from IT investments, appetite for risk, integrity, ethical values, staff competence, accountability and responsibility. The control environment should be based on a culture that supports value delivery whilst managing significant risks, encourages cross-divisional co-operation and teamwork, promotes compliance and continuous process improvement, and handles process deviations (including failure) well.',
    'tImplementationGuide' => '',
  ),
  36 => 
  array (
    'fkContext' => '10405',
    'fkSectionBestPractice' => '10403',
    'nControlType' => '0',
    'sName' => 'PO6.2 Enterprise IT Risk and Control Framework',
    'tDescription' => 'Develop and maintain a framework that defines the enterprise\'s overall approach to IT risk and control and that aligns with the IT policy and control environment and the enterprise risk and control framework.',
    'tImplementationGuide' => '',
  ),
  37 => 
  array (
    'fkContext' => '10406',
    'fkSectionBestPractice' => '10403',
    'nControlType' => '0',
    'sName' => 'PO6.3 IT Policies Management',
    'tDescription' => 'Develop and maintain a set of policies to support IT strategy. These policies should include policy intent; roles and responsibilities; exception process; compliance approach; and references to procedures, standards and guidelines. Their relevance should be confirmed and approved regularly.',
    'tImplementationGuide' => '',
  ),
  38 => 
  array (
    'fkContext' => '10407',
    'fkSectionBestPractice' => '10403',
    'nControlType' => '0',
    'sName' => 'PO6.4 Policy, Standard and Procedures Rollout',
    'tDescription' => 'Roll out and enforce IT policies to all relevant staff, so they are built into and are an integral part of enterprise operations.',
    'tImplementationGuide' => '',
  ),
  39 => 
  array (
    'fkContext' => '10408',
    'fkSectionBestPractice' => '10403',
    'nControlType' => '0',
    'sName' => 'PO6.5 Communication of IT Objectives and Direction',
    'tDescription' => 'Communicate awareness and understanding of business and IT objectives and direction to appropriate stakeholders and users throughout the enterprise.',
    'tImplementationGuide' => '',
  ),
  40 => 
  array (
    'fkContext' => '10410',
    'fkSectionBestPractice' => '10409',
    'nControlType' => '0',
    'sName' => 'PO7.1 Personnel Recruitment and Retention',
    'tDescription' => 'Maintain IT personnel recruitment processes in line with the overall organisation\'s personnel policies and procedures (e.g., hiring, positive work environment, orienting). Implement processes to ensure that the organisation has an appropriately deployed IT workforce with the skills necessary to achieve organisational goals.',
    'tImplementationGuide' => '',
  ),
  41 => 
  array (
    'fkContext' => '10411',
    'fkSectionBestPractice' => '10409',
    'nControlType' => '0',
    'sName' => 'PO7.2 Personnel Competencies',
    'tDescription' => 'Regularly verify that personnel have the competencies to fulfil their roles on the basis of their education, training and/or experience. Define core IT competency requirements and verify that they are being maintained, using qualification and certification programmes where appropriate.',
    'tImplementationGuide' => '',
  ),
  42 => 
  array (
    'fkContext' => '10412',
    'fkSectionBestPractice' => '10409',
    'nControlType' => '0',
    'sName' => 'PO7.3 Staffing of Roles',
    'tDescription' => 'Define, monitor and supervise roles, responsibilities and compensation frameworks for personnel, including the requirement to adhere to management policies and procedures, the code of ethics, and professional practices. The level of supervision should be in line with the sensitivity of the position and extent of responsibilities assigned.',
    'tImplementationGuide' => '',
  ),
  43 => 
  array (
    'fkContext' => '10413',
    'fkSectionBestPractice' => '10409',
    'nControlType' => '0',
    'sName' => 'PO7.4 Personnel Training',
    'tDescription' => 'Provide IT employees with appropriate orientation when hired and ongoing training to maintain their knowledge, skills, abilities, internal controls and security awareness at the level required to achieve organisational goals.',
    'tImplementationGuide' => '',
  ),
  44 => 
  array (
    'fkContext' => '10414',
    'fkSectionBestPractice' => '10409',
    'nControlType' => '0',
    'sName' => 'PO7.5 Dependence Upon Individuals',
    'tDescription' => 'Minimise the exposure to critical dependency on key individuals through knowledge capture (documentation), knowledge sharing, succession planning and staff backup.',
    'tImplementationGuide' => '',
  ),
  45 => 
  array (
    'fkContext' => '10415',
    'fkSectionBestPractice' => '10409',
    'nControlType' => '0',
    'sName' => 'PO7.6 Personnel Clearance Procedures',
    'tDescription' => 'Include background checks in the IT recruitment process. The extent and frequency of periodic reviews of these checks should depend on the sensitivity and/or criticality of the function and should be applied for employees, contractors and vendors.',
    'tImplementationGuide' => '',
  ),
  46 => 
  array (
    'fkContext' => '10416',
    'fkSectionBestPractice' => '10409',
    'nControlType' => '0',
    'sName' => 'PO7.7 Employee Job Performance Evaluation',
    'tDescription' => 'Require a timely evaluation to be performed on a regular basis against individual objectives derived from the organisation\'s goals, established standards and specific job responsibilities. Employees should receive coaching on performance and conduct whenever appropriate.',
    'tImplementationGuide' => '',
  ),
  47 => 
  array (
    'fkContext' => '10417',
    'fkSectionBestPractice' => '10409',
    'nControlType' => '0',
    'sName' => 'PO7.8 Job Change and Termination',
    'tDescription' => 'Take expedient actions regarding job changes, especially job terminations. Knowledge transfer should be arranged, responsibilities reassigned and access rights removed such that risks are minimised and continuity of the function is guaranteed.',
    'tImplementationGuide' => '',
  ),
  48 => 
  array (
    'fkContext' => '10419',
    'fkSectionBestPractice' => '10418',
    'nControlType' => '0',
    'sName' => 'PO8.1 Quality Management System',
    'tDescription' => 'Establish and maintain a QMS that provides a standard, formal and continuous approach regarding quality management that is aligned with business requirements. The QMS should identify quality requirements and criteria; key IT processes and their sequence and interaction; and the policies, criteria and methods for defining, detecting, correcting and preventing non-conformity. The QMS should define the organisational structure for quality management, covering the roles, tasks and responsibilities. All key areas should develop their quality plans in line with criteria and policies and record quality data. Monitor and measure the effectiveness and acceptance of the QMS, and improve it when needed.',
    'tImplementationGuide' => '',
  ),
  49 => 
  array (
    'fkContext' => '10420',
    'fkSectionBestPractice' => '10418',
    'nControlType' => '0',
    'sName' => 'PO8.2 IT Standards and Quality Practices',
    'tDescription' => 'Identify and maintain standards, procedures and practices for key IT processes to guide the organisation in meeting the intent of the QMS. Use industry good practices for reference when improving and tailoring the organisation\'s quality practices.',
    'tImplementationGuide' => '',
  ),
  50 => 
  array (
    'fkContext' => '10421',
    'fkSectionBestPractice' => '10418',
    'nControlType' => '0',
    'sName' => 'PO8.3 Development and Acquisition Standards',
    'tDescription' => 'Adopt and maintain standards for all development and acquisition that follow the life cycle of the ultimate deliverable, and include sign-off at key milestones based on agreed-upon sign-off criteria. Consider software coding standards; naming conventions; file formats; schema and data dictionary design standards; user interface standards; interoperability; system performance efficiency; scalability; standards for development and testing; validation against requirements; test plans; and unit, regression and integration testing.',
    'tImplementationGuide' => '',
  ),
  51 => 
  array (
    'fkContext' => '10422',
    'fkSectionBestPractice' => '10418',
    'nControlType' => '0',
    'sName' => 'PO8.4 Customer Focus',
    'tDescription' => 'Focus quality management on customers by determining their requirements and aligning them to the IT standards and practices. Define roles and responsibilities concerning conflict resolution between the user/customer and the IT organisation.',
    'tImplementationGuide' => '',
  ),
  52 => 
  array (
    'fkContext' => '10423',
    'fkSectionBestPractice' => '10418',
    'nControlType' => '0',
    'sName' => 'PO8.5 Continuous Improvement',
    'tDescription' => 'Maintain and regularly communicate an overall quality plan that promotes continuous improvement.',
    'tImplementationGuide' => '',
  ),
  53 => 
  array (
    'fkContext' => '10424',
    'fkSectionBestPractice' => '10418',
    'nControlType' => '0',
    'sName' => 'PO8.6 Quality Measurement, Monitoring and Review',
    'tDescription' => 'Define, plan and implement measurements to monitor continuing compliance to the QMS, as well as the value the QMS provides. Measurement, monitoring and recording of information should be used by the process owner to take appropriate corrective and preventive actions.',
    'tImplementationGuide' => '',
  ),
  54 => 
  array (
    'fkContext' => '10426',
    'fkSectionBestPractice' => '10425',
    'nControlType' => '0',
    'sName' => 'PO9.1 IT Risk Management Framework',
    'tDescription' => 'Establish an IT risk management framework that is aligned to the organisation\'s (enterprise\'s) risk management framework.',
    'tImplementationGuide' => '',
  ),
  55 => 
  array (
    'fkContext' => '10427',
    'fkSectionBestPractice' => '10425',
    'nControlType' => '0',
    'sName' => 'PO9.2 Establishment of Risk Context',
    'tDescription' => 'Establish the context in which the risk assessment framework is applied to ensure appropriate outcomes. This should include determining the internal and external context of each risk assessment, the goal of the assessment, and the criteria against which risks are evaluated.',
    'tImplementationGuide' => '',
  ),
  56 => 
  array (
    'fkContext' => '10428',
    'fkSectionBestPractice' => '10425',
    'nControlType' => '0',
    'sName' => 'PO9.3 Event Identification',
    'tDescription' => 'Identify events (an important realistic threat that exploits a significant applicable vulnerability) with a potential negative impact on the goals or operations of the enterprise, including business, regulatory, legal, technology, trading partner, human resources and operational aspects. Determine the nature of the impact and maintain this information. Record and maintain relevant risks in a risk registry.',
    'tImplementationGuide' => '',
  ),
  57 => 
  array (
    'fkContext' => '10429',
    'fkSectionBestPractice' => '10425',
    'nControlType' => '0',
    'sName' => 'PO9.4 Risk Assessment',
    'tDescription' => 'Assess on a recurrent basis the likelihood and impact of all identified risks, using qualitative and quantitative methods. The likelihood and impact associated with inherent and residual risk should be determined individually, by category and on a portfolio basis.',
    'tImplementationGuide' => '',
  ),
  58 => 
  array (
    'fkContext' => '10430',
    'fkSectionBestPractice' => '10425',
    'nControlType' => '0',
    'sName' => 'PO9.5 Risk Response',
    'tDescription' => 'Develop and maintain a risk response process designed to ensure that cost-effective controls mitigate exposure to risks on a continuing basis. The risk response process should identify risk strategies such as avoidance, reduction, sharing or acceptance; determine associated responsibilities; and consider risk tolerance levels.',
    'tImplementationGuide' => '',
  ),
  59 => 
  array (
    'fkContext' => '10431',
    'fkSectionBestPractice' => '10425',
    'nControlType' => '0',
    'sName' => 'PO9.6 Maintenance and Monitoring of a Risk Action Plan',
    'tDescription' => 'Prioritise and plan the control activities at all levels to implement the risk responses identified as necessary, including identification of costs, benefits and responsibility for execution. Obtain approval for recommended actions and acceptance of any residual risks, and ensure that committed actions are owned by the affected process owner(s). Monitor execution of the plans, and report on any deviations to senior management.',
    'tImplementationGuide' => '',
  ),
  60 => 
  array (
    'fkContext' => '10433',
    'fkSectionBestPractice' => '10432',
    'nControlType' => '0',
    'sName' => 'PO10.1 Programme Management Framework',
    'tDescription' => 'Maintain the programme of projects, related to the portfolio of IT-enabled investment programmes, by identifying, defining, evaluating, prioritising, selecting, initiating, managing and controlling projects. Ensure that the projects support the programme\'s objectives. Co-ordinate the activities and interdependencies of multiple projects, manage the contribution of all the projects within the programme to expected outcomes, and resolve resource requirements and conflicts.',
    'tImplementationGuide' => '',
  ),
  61 => 
  array (
    'fkContext' => '10434',
    'fkSectionBestPractice' => '10432',
    'nControlType' => '0',
    'sName' => 'PO10.2 Project Management Framework',
    'tDescription' => 'Establish and maintain a project management framework that defines the scope and boundaries of managing projects, as well as the method to be adopted and applied to each project undertaken. The framework and supporting method should be integrated with the programme management processes.',
    'tImplementationGuide' => '',
  ),
  62 => 
  array (
    'fkContext' => '10435',
    'fkSectionBestPractice' => '10432',
    'nControlType' => '0',
    'sName' => 'PO10.3 Project Management Approach',
    'tDescription' => 'Establish a project management approach commensurate with the size, complexity and regulatory requirements of each project. The project governance structure can include the roles, responsibilities and accountabilities of the programme sponsor, project sponsors, steering committee, project office and project manager, and the mechanisms through which they can meet those responsibilities (such as reporting and stage reviews). Make sure all IT projects have sponsors with sufficient authority to own the execution of the project within the overall strategic programme.',
    'tImplementationGuide' => '',
  ),
  63 => 
  array (
    'fkContext' => '10436',
    'fkSectionBestPractice' => '10432',
    'nControlType' => '0',
    'sName' => 'PO10.4 Stakeholder Commitment',
    'tDescription' => 'Obtain commitment and participation from the affected stakeholders in the definition and execution of the project within the context of the overall IT-enabled investment programme.',
    'tImplementationGuide' => '',
  ),
  64 => 
  array (
    'fkContext' => '10437',
    'fkSectionBestPractice' => '10432',
    'nControlType' => '0',
    'sName' => 'PO10.5 Project Scope Statement',
    'tDescription' => 'Define and document the nature and scope of the project to confirm and develop amongst stakeholders a common understanding of project scope and how it relates to other projects within the overall IT-enabled investment programme. The definition should be formally approved by the programme and project sponsors before project initiation.',
    'tImplementationGuide' => '',
  ),
  65 => 
  array (
    'fkContext' => '10438',
    'fkSectionBestPractice' => '10432',
    'nControlType' => '0',
    'sName' => 'PO10.6 Project Phase Initiation',
    'tDescription' => 'Approve the initiation of each major project phase and communicate it to all stakeholders. Base the approval of the initial phase on programme governance decisions. Approval of subsequent phases should be based on review and acceptance of the deliverables of the previous phase, and approval of an updated business case at the next major review of the programme. In the event of overlapping project phases, an approval point should be established by programme and project sponsors to authorise project progression.',
    'tImplementationGuide' => '',
  ),
  66 => 
  array (
    'fkContext' => '10439',
    'fkSectionBestPractice' => '10432',
    'nControlType' => '0',
    'sName' => 'PO10.7 Integrated Project Plan',
    'tDescription' => 'Establish a formal, approved integrated project plan (covering business and information systems resources) to guide project execution and control throughout the life of the project. The activities and interdependencies of multiple projects within a programme should be understood and documented. The project plan should be maintained throughout the life of the project. The project plan, and changes to it, should be approved in line with the programme and project governance framework.',
    'tImplementationGuide' => '',
  ),
  67 => 
  array (
    'fkContext' => '10440',
    'fkSectionBestPractice' => '10432',
    'nControlType' => '0',
    'sName' => 'PO10.8 Project Resources',
    'tDescription' => 'Define the responsibilities, relationships, authorities and performance criteria of project team members, and specify the basis for acquiring and assigning competent staff members and/or contractors to the project. The procurement of products and services required for each project should be planned and managed to achieve project objectives using the organisation\'s procurement practices.',
    'tImplementationGuide' => '',
  ),
  68 => 
  array (
    'fkContext' => '10441',
    'fkSectionBestPractice' => '10432',
    'nControlType' => '0',
    'sName' => 'PO10.9 Project Risk Management',
    'tDescription' => 'Eliminate or minimise specific risks associated with individual projects through a systematic process of planning, identifying, analysing, responding to, monitoring and controlling the areas or events that have the potential to cause unwanted change. Risks faced by the project management process and the project deliverable should be established and centrally recorded.',
    'tImplementationGuide' => '',
  ),
  69 => 
  array (
    'fkContext' => '10442',
    'fkSectionBestPractice' => '10432',
    'nControlType' => '0',
    'sName' => 'PO10.10 Project Quality Plan',
    'tDescription' => 'Prepare a quality management plan that describes the project quality system and how it will be implemented. The plan should be formally reviewed and agreed to by all parties concerned and then incorporated into the integrated project plan.',
    'tImplementationGuide' => '',
  ),
  70 => 
  array (
    'fkContext' => '10443',
    'fkSectionBestPractice' => '10432',
    'nControlType' => '0',
    'sName' => 'PO10.11 Project Change Control',
    'tDescription' => 'Establish a change control system for each project, so all changes to the project baseline (e.g., cost, schedule, scope, quality) are appropriately reviewed, approved and incorporated into the integrated project plan in line with the programme and project governance framework.',
    'tImplementationGuide' => '',
  ),
  71 => 
  array (
    'fkContext' => '10444',
    'fkSectionBestPractice' => '10432',
    'nControlType' => '0',
    'sName' => 'PO10.12 Project Planning of Assurance Methods',
    'tDescription' => 'Identify assurance tasks required to support the accreditation of new or modified systems during project planning, and include them in the integrated project plan. The tasks should provide assurance that internal controls and security features meet the defined requirements.',
    'tImplementationGuide' => '',
  ),
  72 => 
  array (
    'fkContext' => '10445',
    'fkSectionBestPractice' => '10432',
    'nControlType' => '0',
    'sName' => 'PO10.13 Project Performance Measurement, Reporting and Monitoring',
    'tDescription' => 'Measure project performance against key project performance scope, schedule, quality, cost and risk criteria. Identify any deviations from the plan. Assess the impact of deviations on the project and overall programme, and report results to key stakeholders. Recommend, implement and monitor remedial action, when required, in line with the programme and project governance framework.',
    'tImplementationGuide' => '',
  ),
  73 => 
  array (
    'fkContext' => '10446',
    'fkSectionBestPractice' => '10432',
    'nControlType' => '0',
    'sName' => 'PO10.14 Project Closure',
    'tDescription' => 'Require that, at the end of each project, the project stakeholders ascertain whether the project delivered the planned results and benefits. Identify and communicate any outstanding activities required to achieve the planned results of the project and the benefits of the programme, and identify and document lessons learned for use on future projects and programmes.',
    'tImplementationGuide' => '',
  ),
  74 => 
  array (
    'fkContext' => '10449',
    'fkSectionBestPractice' => '10448',
    'nControlType' => '0',
    'sName' => 'AI1.1 Definition and Maintenance of Business Functional and Technical Requirements',
    'tDescription' => 'Identify, prioritise, specify and agree on business functional and technical requirements covering the full scope of all initiatives required to achieve the expected outcomes of the IT-enabled investment programme.',
    'tImplementationGuide' => '',
  ),
  75 => 
  array (
    'fkContext' => '10450',
    'fkSectionBestPractice' => '10448',
    'nControlType' => '0',
    'sName' => 'AI1.2 Risk Analysis Report',
    'tDescription' => 'Identify, document and analyse risks associated with the business requirements and solution design as part of the organisation\'s process for the development of requirements.',
    'tImplementationGuide' => '',
  ),
  76 => 
  array (
    'fkContext' => '10451',
    'fkSectionBestPractice' => '10448',
    'nControlType' => '0',
    'sName' => 'AI1.3 Feasibility Study and Formulation of Alternative Courses of Action',
    'tDescription' => 'Develop a feasibility study that examines the possibility of implementing the requirements. Business management, supported by the IT function, should assess the feasibility and alternative courses of action and make a recommendation to the business sponsor.',
    'tImplementationGuide' => '',
  ),
  77 => 
  array (
    'fkContext' => '10452',
    'fkSectionBestPractice' => '10448',
    'nControlType' => '0',
    'sName' => 'AI1.4 Requirements and Feasibility Decision and Approval',
    'tDescription' => 'Verify that the process requires the business sponsor to approve and sign off on business functional and technical requirements and feasibility study reports at predetermined key stages. The business sponsor should make the final decision with respect to the choice of solution and acquisition approach.',
    'tImplementationGuide' => '',
  ),
  78 => 
  array (
    'fkContext' => '10454',
    'fkSectionBestPractice' => '10453',
    'nControlType' => '0',
    'sName' => 'AI2.1 High-level Design',
    'tDescription' => 'Translate business requirements into a high-level design specification for software acquisition, taking into account the organisation\'s technological direction and information architecture. Have the design specifications approved by management to ensure that the high-level design responds to the requirements. Reassess when significant technical or logical discrepancies occur during development or maintenance.',
    'tImplementationGuide' => '',
  ),
  79 => 
  array (
    'fkContext' => '10455',
    'fkSectionBestPractice' => '10453',
    'nControlType' => '0',
    'sName' => 'AI2.2 Detailed Design',
    'tDescription' => 'Prepare detailed design and technical software application requirements. Define the criteria for acceptance of the requirements. Have the requirements approved to ensure that they correspond to the high-level design. Perform reassessment when significant technical or logical discrepancies occur during development or maintenance.',
    'tImplementationGuide' => '',
  ),
  80 => 
  array (
    'fkContext' => '10456',
    'fkSectionBestPractice' => '10453',
    'nControlType' => '0',
    'sName' => 'AI2.3 Application Control and Auditability',
    'tDescription' => 'Implement business controls, where appropriate, into automated application controls such that processing is accurate, complete, timely, authorised and auditable.',
    'tImplementationGuide' => '',
  ),
  81 => 
  array (
    'fkContext' => '10457',
    'fkSectionBestPractice' => '10453',
    'nControlType' => '0',
    'sName' => 'AI2.4 Application Security and Availability',
    'tDescription' => 'Address application security and availability requirements in response to identified risks and in line with the organisation\'s data classification, information architecture, information security architecture and risk tolerance.',
    'tImplementationGuide' => '',
  ),
  82 => 
  array (
    'fkContext' => '10458',
    'fkSectionBestPractice' => '10453',
    'nControlType' => '0',
    'sName' => 'AI2.5 Configuration and Implementation of Acquired Application Software',
    'tDescription' => 'Configure and implement acquired application software to meet business objectives.',
    'tImplementationGuide' => '',
  ),
  83 => 
  array (
    'fkContext' => '10459',
    'fkSectionBestPractice' => '10453',
    'nControlType' => '0',
    'sName' => 'AI2.6 Major Upgrades to Existing Systems',
    'tDescription' => 'In the event of major changes to existing systems that result in significant change in current designs and/or functionality, follow a similar development process as that used for the development of new systems.',
    'tImplementationGuide' => '',
  ),
  84 => 
  array (
    'fkContext' => '10460',
    'fkSectionBestPractice' => '10453',
    'nControlType' => '0',
    'sName' => 'AI2.7 Development of Application Software',
    'tDescription' => 'Ensure that automated functionality is developed in accordance with design specifications, development and documentation standards, QA requirements, and approval standards. Ensure that all legal and contractual aspects are identified and addressed for application software developed by third parties.',
    'tImplementationGuide' => '',
  ),
  85 => 
  array (
    'fkContext' => '10461',
    'fkSectionBestPractice' => '10453',
    'nControlType' => '0',
    'sName' => 'AI2.8 Software Quality Assurance',
    'tDescription' => 'Develop, resource and execute a software QA plan to obtain the quality specified in the requirements definition and the organisation\'s quality policies and procedures.',
    'tImplementationGuide' => '',
  ),
  86 => 
  array (
    'fkContext' => '10462',
    'fkSectionBestPractice' => '10453',
    'nControlType' => '0',
    'sName' => 'AI2.9 Applications Requirements Management',
    'tDescription' => 'Track the status of individual requirements (including all rejected requirements) during the design, development and implementation, and approve changes to requirements through an established change management process.',
    'tImplementationGuide' => '',
  ),
  87 => 
  array (
    'fkContext' => '10463',
    'fkSectionBestPractice' => '10453',
    'nControlType' => '0',
    'sName' => 'AI2.10 Application Software Maintenance',
    'tDescription' => 'Develop a strategy and plan for the maintenance of software applications.',
    'tImplementationGuide' => '',
  ),
  88 => 
  array (
    'fkContext' => '10465',
    'fkSectionBestPractice' => '10464',
    'nControlType' => '0',
    'sName' => 'AI3.1 Technological Infrastructure Acquisition Plan',
    'tDescription' => 'Produce a plan for the acquisition, implementation and maintenance of the technological infrastructure that meets established business functional and technical requirements and is in accord with the organisation\'s technology direction.',
    'tImplementationGuide' => '',
  ),
  89 => 
  array (
    'fkContext' => '10466',
    'fkSectionBestPractice' => '10464',
    'nControlType' => '0',
    'sName' => 'AI3.2 Infrastructure Resource Protection and Availability',
    'tDescription' => 'rastructural software to protect resources and ensure availability and integrity. Responsibilities for using sensitive infrastructure components should be clearly defined and understood by those who develop and integrate infrastructure components. Their use should be monitored and evaluated.',
    'tImplementationGuide' => '',
  ),
  90 => 
  array (
    'fkContext' => '10467',
    'fkSectionBestPractice' => '10464',
    'nControlType' => '0',
    'sName' => 'AI3.3 Infrastructure Maintenance',
    'tDescription' => 'Develop a strategy and plan for infrastructure maintenance, and ensure that changes are controlled in line with the organisation\'s change management procedure. Include periodic reviews against business needs, patch management, upgrade strategies, risks, vulnerabilities assessment and security requirements.',
    'tImplementationGuide' => '',
  ),
  91 => 
  array (
    'fkContext' => '10468',
    'fkSectionBestPractice' => '10464',
    'nControlType' => '0',
    'sName' => 'AI3.4 Feasibility Test Environment',
    'tDescription' => 'Establish development and test environments to support effective and efficient feasibility and integration testing of infrastructure components.',
    'tImplementationGuide' => '',
  ),
  92 => 
  array (
    'fkContext' => '10470',
    'fkSectionBestPractice' => '10469',
    'nControlType' => '0',
    'sName' => 'AI4.1 Planning for Operational Solutions',
    'tDescription' => 'Develop a plan to identify and document all technical, operational and usage aspects such that all those who will operate, use and maintain the automated solutions can exercise their responsibility.',
    'tImplementationGuide' => '',
  ),
  93 => 
  array (
    'fkContext' => '10471',
    'fkSectionBestPractice' => '10469',
    'nControlType' => '0',
    'sName' => 'AI4.2 Knowledge Transfer to Business Management',
    'tDescription' => 'Transfer knowledge to business management to allow those individuals to take ownership of the system and data, and exercise responsibility for service delivery and quality, internal control, and application administration.',
    'tImplementationGuide' => '',
  ),
  94 => 
  array (
    'fkContext' => '10472',
    'fkSectionBestPractice' => '10469',
    'nControlType' => '0',
    'sName' => 'AI4.3 Knowledge Transfer to End Users',
    'tDescription' => 'Transfer knowledge and skills to allow end users to effectively and efficiently use the system in support of business processes.',
    'tImplementationGuide' => '',
  ),
  95 => 
  array (
    'fkContext' => '10473',
    'fkSectionBestPractice' => '10469',
    'nControlType' => '0',
    'sName' => 'AI4.4 Knowledge Transfer to Operations and Support Staff',
    'tDescription' => 'Transfer knowledge and skills to enable operations and technical support staff to effectively and efficiently deliver, support and maintain the system and associated infrastructure.',
    'tImplementationGuide' => '',
  ),
  96 => 
  array (
    'fkContext' => '10475',
    'fkSectionBestPractice' => '10474',
    'nControlType' => '0',
    'sName' => 'AI5.1 Procurement Control',
    'tDescription' => 'Develop and follow a set of procedures and standards that is consistent with the business organisation\'s overall procurement process and acquisition strategy to acquire IT-related infrastructure, facilities, hardware, software and services needed by the business.',
    'tImplementationGuide' => '',
  ),
  97 => 
  array (
    'fkContext' => '10476',
    'fkSectionBestPractice' => '10474',
    'nControlType' => '0',
    'sName' => 'AI5.2 Supplier Contract Management',
    'tDescription' => 'Set up a procedure for establishing, modifying and terminating contracts for all suppliers. The procedure should cover, at a minimum, legal, financial, organisational, documentary, performance, security, intellectual property, and termination responsibilities and liabilities (including penalty clauses). All contracts and contract changes should be reviewed by legal advisors.',
    'tImplementationGuide' => '',
  ),
  98 => 
  array (
    'fkContext' => '10477',
    'fkSectionBestPractice' => '10474',
    'nControlType' => '0',
    'sName' => 'AI5.3 Supplier Selection',
    'tDescription' => 'Select suppliers according to a fair and formal practice to ensure a viable best fit based on specified requirements. Requirements should be optimised with input from potential suppliers.',
    'tImplementationGuide' => '',
  ),
  99 => 
  array (
    'fkContext' => '10478',
    'fkSectionBestPractice' => '10474',
    'nControlType' => '0',
    'sName' => 'AI5.4 IT Resources Acquisition',
    'tDescription' => 'Protect and enforce the organisation\'s interests in all acquisition contractual agreements, including the rights and obligations of all parties in the contractual terms for the acquisition of software, development resources, infrastructure and services.',
    'tImplementationGuide' => '',
  ),
  100 => 
  array (
    'fkContext' => '10480',
    'fkSectionBestPractice' => '10479',
    'nControlType' => '0',
    'sName' => 'AI6.1 Change Standards and Procedures',
    'tDescription' => 'Set up formal change management procedures to handle in a standardised manner all requests (including maintenance and patches) for changes to applications, procedures, processes, system and service parameters, and the underlying platforms.',
    'tImplementationGuide' => '',
  ),
  101 => 
  array (
    'fkContext' => '10481',
    'fkSectionBestPractice' => '10479',
    'nControlType' => '0',
    'sName' => 'AI6.2 Impact Assessment, Prioritisation and Authorisation',
    'tDescription' => 'Assess all requests for change in a structured way to determine the impact on the operational system and its functionality. Ensure that changes are categorised, prioritised and authorised.',
    'tImplementationGuide' => '',
  ),
  102 => 
  array (
    'fkContext' => '10482',
    'fkSectionBestPractice' => '10479',
    'nControlType' => '0',
    'sName' => 'AI6.3 Emergency Changes',
    'tDescription' => 'Establish a process for defining, raising, testing, documenting, assessing and authorising emergency changes that do not follow the established change process.',
    'tImplementationGuide' => '',
  ),
  103 => 
  array (
    'fkContext' => '10483',
    'fkSectionBestPractice' => '10479',
    'nControlType' => '0',
    'sName' => 'AI6.4 Change Status Tracking and Reporting',
    'tDescription' => 'Establish a tracking and reporting system to document rejected changes, communicate the status of approved and in-process changes, and complete changes. Make certain that approved changes are implemented as planned.',
    'tImplementationGuide' => '',
  ),
  104 => 
  array (
    'fkContext' => '10484',
    'fkSectionBestPractice' => '10479',
    'nControlType' => '0',
    'sName' => 'AI6.5 Change Closure and Documentation',
    'tDescription' => 'Whenever changes are implemented, update the associated system and user documentation and procedures accordingly.',
    'tImplementationGuide' => '',
  ),
  105 => 
  array (
    'fkContext' => '10486',
    'fkSectionBestPractice' => '10485',
    'nControlType' => '0',
    'sName' => 'AI7.1 Training',
    'tDescription' => 'Train the staff members of the affected user departments and the operations group of the IT function in accordance with the defined training and implementation plan and associated materials, as part of every information systems development, implementation or modification project.',
    'tImplementationGuide' => '',
  ),
  106 => 
  array (
    'fkContext' => '10487',
    'fkSectionBestPractice' => '10485',
    'nControlType' => '0',
    'sName' => 'AI7.2 Test Plan',
    'tDescription' => 'Establish a test plan based on organisationwide standards that defines roles, responsibilities, and entry and exit criteria. Ensure that the plan is approved by relevant parties.',
    'tImplementationGuide' => '',
  ),
  107 => 
  array (
    'fkContext' => '10488',
    'fkSectionBestPractice' => '10485',
    'nControlType' => '0',
    'sName' => 'AI7.3 Implementation Plan',
    'tDescription' => 'Establish an implementation and fallback/backout plan. Obtain approval from relevant parties.',
    'tImplementationGuide' => '',
  ),
  108 => 
  array (
    'fkContext' => '10489',
    'fkSectionBestPractice' => '10485',
    'nControlType' => '0',
    'sName' => 'AI7.4 Test Environment',
    'tDescription' => 'Define and establish a secure test environment representative of the planned operations environment relative to security, internal controls, operational practices, data quality and privacy requirements, and workloads.',
    'tImplementationGuide' => '',
  ),
  109 => 
  array (
    'fkContext' => '10490',
    'fkSectionBestPractice' => '10485',
    'nControlType' => '0',
    'sName' => 'AI7.5 System and Data Conversion',
    'tDescription' => 'Plan data conversion and infrastructure migration as part of the organisation\'s development methods, including audit trails, rollbacks and fallbacks.',
    'tImplementationGuide' => '',
  ),
  110 => 
  array (
    'fkContext' => '10491',
    'fkSectionBestPractice' => '10485',
    'nControlType' => '0',
    'sName' => 'AI7.6 Testing of Changes',
    'tDescription' => 'Test changes independently in accordance with the defined test plan prior to migration to the operational environment. Ensure that the plan considers security and performance.',
    'tImplementationGuide' => '',
  ),
  111 => 
  array (
    'fkContext' => '10492',
    'fkSectionBestPractice' => '10485',
    'nControlType' => '0',
    'sName' => 'AI7.7 Final Acceptance Test',
    'tDescription' => 'Ensure that business process owners and IT stakeholders evaluate the outcome of the testing process as determined by the test plan. Remediate significant errors identified in the testing process, having completed the suite of tests identified in the test plan and any necessary regression tests. Following evaluation, approve promotion to production.',
    'tImplementationGuide' => '',
  ),
  112 => 
  array (
    'fkContext' => '10493',
    'fkSectionBestPractice' => '10485',
    'nControlType' => '0',
    'sName' => 'AI7.8 Promotion to Production',
    'tDescription' => 'Following testing, control the handover of the changed system to operations, keeping it in line with the implementation plan. Obtain approval of the key stakeholders, such as users, system owner and operational management. Where appropriate, run the system in parallel with the old system for a while, and compare behaviour and results.',
    'tImplementationGuide' => '',
  ),
  113 => 
  array (
    'fkContext' => '10494',
    'fkSectionBestPractice' => '10485',
    'nControlType' => '0',
    'sName' => 'AI7.9 Post-implementation Review',
    'tDescription' => 'Establish procedures in line with the organisational change management standards to require a post-implementation review as set out in the implementation plan.',
    'tImplementationGuide' => '',
  ),
  114 => 
  array (
    'fkContext' => '10497',
    'fkSectionBestPractice' => '10496',
    'nControlType' => '0',
    'sName' => 'DS1.1 Service Level Management Framework',
    'tDescription' => 'Define a framework that provides a formalised service level management process between the customer and service provider. The framework should maintain continuous alignment with business requirements and priorities and facilitate common understanding between the customer and provider(s). The framework should include processes for creating service requirements, service definitions, SLAs, OLAs and funding sources. These attributes should be organised in a service catalogue. The framework should define the organisational structure for service level management, covering the roles, tasks and responsibilities of internal and external service providers and customers.',
    'tImplementationGuide' => '',
  ),
  115 => 
  array (
    'fkContext' => '10498',
    'fkSectionBestPractice' => '10496',
    'nControlType' => '0',
    'sName' => 'DS1.2 Definition of Services',
    'tDescription' => 'Base definitions of IT services on service characteristics and business requirements. Ensure that they are organised and stored centrally via the implementation of a service catalogue portfolio approach.',
    'tImplementationGuide' => '',
  ),
  116 => 
  array (
    'fkContext' => '10499',
    'fkSectionBestPractice' => '10496',
    'nControlType' => '0',
    'sName' => 'DS1.3 Service Level Agreements',
    'tDescription' => 'Define and agree to SLAs for all critical IT services based on customer requirements and IT capabilities. This should cover customer commitments; service support requirements; quantitative and qualitative metrics for measuring the service signed off on by the stakeholders; funding and commercial arrangements, if applicable; and roles and responsibilities, including oversight of the SLA. Consider items such as availability, reliability, performance, capacity for growth, levels of support, continuity planning, security and demand constraints.',
    'tImplementationGuide' => '',
  ),
  117 => 
  array (
    'fkContext' => '10500',
    'fkSectionBestPractice' => '10496',
    'nControlType' => '0',
    'sName' => 'DS1.4 Operating Level Agreements',
    'tDescription' => 'Define OLAs that explain how the services will be technically delivered to support the SLA(s) in an optimal manner. The OLAs should specify the technical processes in terms meaningful to the provider and may support several SLAs.',
    'tImplementationGuide' => '',
  ),
  118 => 
  array (
    'fkContext' => '10501',
    'fkSectionBestPractice' => '10496',
    'nControlType' => '0',
    'sName' => 'DS1.5 Monitoring and Reporting of Service Level Achievements',
    'tDescription' => 'Continuously monitor specified service level performance criteria. Reports on achievement of service levels should be provided in a format that is meaningful to the stakeholders. The monitoring statistics should be analysed and acted upon to identify negative and positive trends for individual services as well as for services overall.',
    'tImplementationGuide' => '',
  ),
  119 => 
  array (
    'fkContext' => '10502',
    'fkSectionBestPractice' => '10496',
    'nControlType' => '0',
    'sName' => 'DS1.6 Review of Service Level Agreements and Contracts',
    'tDescription' => 'Regularly review SLAs and underpinning contracts (UCs) with internal and external service providers to ensure that they are effective and up to date and that changes in requirements have been taken into account.',
    'tImplementationGuide' => '',
  ),
  120 => 
  array (
    'fkContext' => '10504',
    'fkSectionBestPractice' => '10503',
    'nControlType' => '0',
    'sName' => 'DS2.1 Identification of All Supplier Relationships',
    'tDescription' => 'Identify all supplier services, and categorise them according to supplier type, significance and criticality. Maintain formal documentation of technical and organisational relationships covering the roles and responsibilities, goals, expected deliverables, and credentials of representatives of these suppliers.',
    'tImplementationGuide' => '',
  ),
  121 => 
  array (
    'fkContext' => '10505',
    'fkSectionBestPractice' => '10503',
    'nControlType' => '0',
    'sName' => 'DS2.2 Supplier Relationship Management',
    'tDescription' => 'Formalise the supplier relationship management process for each supplier. The relationship owners should liaise on customer and supplier issues and ensure the quality of the relationship based on trust and transparency (e.g., through SLAs).',
    'tImplementationGuide' => '',
  ),
  122 => 
  array (
    'fkContext' => '10506',
    'fkSectionBestPractice' => '10503',
    'nControlType' => '0',
    'sName' => 'DS2.3 Supplier Risk Management',
    'tDescription' => 'Identify and mitigate risks relating to suppliers\' ability to continue effective service delivery in a secure and efficient manner on a continual basis. Ensure that contracts conform to universal business standards in accordance with legal and regulatory requirements. Risk management should further consider non-disclosure agreements (NDAs), escrow contracts, continued supplier viability, conformance with security requirements, alternative suppliers, penalties and rewards, etc.',
    'tImplementationGuide' => '',
  ),
  123 => 
  array (
    'fkContext' => '10507',
    'fkSectionBestPractice' => '10503',
    'nControlType' => '0',
    'sName' => 'DS2.4 Supplier Performance Monitoring',
    'tDescription' => 'Establish a process to monitor service delivery to ensure that the supplier is meeting current business requirements and continuing to adhere to the contract agreements and SLAs, and that performance is competitive with alternative suppliers and market conditions.',
    'tImplementationGuide' => '',
  ),
  124 => 
  array (
    'fkContext' => '10509',
    'fkSectionBestPractice' => '10508',
    'nControlType' => '0',
    'sName' => 'DS3.1 Performance and Capacity Planning',
    'tDescription' => 'Establish a planning process for the review of performance and capacity of IT resources to ensure that cost-justifiable capacity and performance are available to process the agreed-upon workloads as determined by the SLAs. Capacity and performance plans should leverage appropriate modelling techniques to produce a model of the current and forecasted performance, capacity and throughput of the IT resources.',
    'tImplementationGuide' => '',
  ),
  125 => 
  array (
    'fkContext' => '10510',
    'fkSectionBestPractice' => '10508',
    'nControlType' => '0',
    'sName' => 'DS3.2 Current Performance and Capacity',
    'tDescription' => 'Assess current performance and capacity of IT resources to determine if sufficient capacity and performance exist to deliver against agreed-upon service levels.',
    'tImplementationGuide' => '',
  ),
  126 => 
  array (
    'fkContext' => '10511',
    'fkSectionBestPractice' => '10508',
    'nControlType' => '0',
    'sName' => 'DS3.3 Future Performance and Capacity',
    'tDescription' => 'Conduct performance and capacity forecasting of IT resources at regular intervals to minimise the risk of service disruptions due to insufficient capacity or performance degradation, and identify excess capacity for possible redeployment. Identify workload trends and determine forecasts to be input to performance and capacity plans.',
    'tImplementationGuide' => '',
  ),
  127 => 
  array (
    'fkContext' => '10512',
    'fkSectionBestPractice' => '10508',
    'nControlType' => '0',
    'sName' => 'DS3.4 IT Resources Availability',
    'tDescription' => 'Provide the required capacity and performance, taking into account aspects such as normal workloads, contingencies, storage requirements and IT resource life cycles. Provisions such as prioritising tasks, fault-tolerance mechanisms and resource allocation practices should be made. Management should ensure that contingency plans properly address availability, capacity and performance of individual IT resources.',
    'tImplementationGuide' => '',
  ),
  128 => 
  array (
    'fkContext' => '10513',
    'fkSectionBestPractice' => '10508',
    'nControlType' => '0',
    'sName' => 'DS3.5 Monitoring and Reporting',
    'tDescription' => 'Continuously monitor the performance and capacity of IT resources. Data gathered should serve two purposes: - To maintain and tune current performance within IT and address such issues as resilience, contingency, current and  projected workloads, storage plans, and resource acquisition - To report delivered service availability to the business, as required by the SLAs Accompany all exception reports with recommendations for corrective action.',
    'tImplementationGuide' => '',
  ),
  129 => 
  array (
    'fkContext' => '10515',
    'fkSectionBestPractice' => '10514',
    'nControlType' => '0',
    'sName' => 'DS4.1 IT Continuity Framework',
    'tDescription' => 'Develop a framework for IT continuity to support enterprisewide business continuity management using a consistent process. The objective of the framework should be to assist in determining the required resilience of the infrastructure and to drive the development of disaster recovery and IT contingency plans. The framework should address the organisational structure for continuity management, covering the roles, tasks and responsibilities of internal and external service providers, their management and their customers, and the planning processes that create the rules and structures to document, test and execute the disaster recovery and IT contingency plans. The plan should also address items such as the identification of critical resources, noting key dependencies, the monitoring and reporting of the availability of critical resources, alternative processing, and the principles of backup and recovery.',
    'tImplementationGuide' => '',
  ),
  130 => 
  array (
    'fkContext' => '10516',
    'fkSectionBestPractice' => '10514',
    'nControlType' => '0',
    'sName' => 'DS4.2 IT Continuity Plans',
    'tDescription' => 'Develop IT continuity plans based on the framework and designed to reduce the impact of a major disruption on key business functions and processes. The plans should be based on risk understanding of potential business impacts and address requirements for resilience, alternative processing and recovery capability of all critical IT services. They should also cover usage guidelines, roles and responsibilities, procedures, communication processes, and the testing approach.',
    'tImplementationGuide' => '',
  ),
  131 => 
  array (
    'fkContext' => '10517',
    'fkSectionBestPractice' => '10514',
    'nControlType' => '0',
    'sName' => 'DS4.3 Critical IT Resources',
    'tDescription' => 'Focus attention on items specified as most critical in the IT continuity plan to build in resilience and establish priorities in recovery situations. Avoid the distraction of recovering less-critical items and ensure response and recovery in line with prioritised business needs, while ensuring that costs are kept at an acceptable level and complying with regulatory and contractual requirements. Consider resilience, response and recovery requirements for different tiers, e.g., one to four hours, four to 24 hours, more than 24 hours and critical business operational periods.',
    'tImplementationGuide' => '',
  ),
  132 => 
  array (
    'fkContext' => '10518',
    'fkSectionBestPractice' => '10514',
    'nControlType' => '0',
    'sName' => 'DS4.4 Maintenance of the IT Continuity Plan',
    'tDescription' => 'Encourage IT management to define and execute change control procedures to ensure that the IT continuity plan is kept up to date and continually reflects actual business requirements. Communicate changes in procedures and responsibilities clearly and in a timely manner.',
    'tImplementationGuide' => '',
  ),
  133 => 
  array (
    'fkContext' => '10519',
    'fkSectionBestPractice' => '10514',
    'nControlType' => '0',
    'sName' => 'DS4.5 Testing of the IT Continuity Plan',
    'tDescription' => 'Test the IT continuity plan on a regular basis to ensure that IT systems can be effectively recovered, shortcomings are addressed and the plan remains relevant. This requires careful preparation, documentation, reporting of test results and, according to the results, implementation of an action plan. Consider the extent of testing recovery of single applications to integrated testing scenarios to end to-end testing and integrated vendor testing.',
    'tImplementationGuide' => '',
  ),
  134 => 
  array (
    'fkContext' => '10520',
    'fkSectionBestPractice' => '10514',
    'nControlType' => '0',
    'sName' => 'DS4.6 IT Continuity Plan Training',
    'tDescription' => 'Provide all concerned parties with regular training sessions regarding the procedures and their roles and responsibilities in case of na incident or disaster. Verify and enhance training according to the results of the contingency tests.',
    'tImplementationGuide' => '',
  ),
  135 => 
  array (
    'fkContext' => '10521',
    'fkSectionBestPractice' => '10514',
    'nControlType' => '0',
    'sName' => 'DS4.7 Distribution of the IT Continuity Plan',
    'tDescription' => 'Determine that a defined and managed distribution strategy exists to ensure that plans are properly and securely distributed and available to appropriately authorised interested parties when and where needed. Attention should be paid to making the plans accessible under all disaster scenarios.',
    'tImplementationGuide' => '',
  ),
  136 => 
  array (
    'fkContext' => '10522',
    'fkSectionBestPractice' => '10514',
    'nControlType' => '0',
    'sName' => 'DS4.8 IT Services Recovery and Resumption',
    'tDescription' => 'Plan the actions to be taken for the period when IT is recovering and resuming services. This may include activation of backup sites, initiation of alternative processing, customer and stakeholder communication, and resumption procedures. Ensure that the business understands IT recovery times and the necessary technology investments to support business recovery and resumption needs.',
    'tImplementationGuide' => '',
  ),
  137 => 
  array (
    'fkContext' => '10523',
    'fkSectionBestPractice' => '10514',
    'nControlType' => '0',
    'sName' => 'DS4.9 Offsite Backup Storage',
    'tDescription' => 'Store offsite all critical backup media, documentation and other IT resources necessary for IT recovery and business continuity plans. Determine the content of backup storage in collaboration between business process owners and IT personnel. Management of the offsite storage facility should respond to the data classification policy and the enterprise\'s media storage practices. IT management should ensure that offsite arrangements are periodically assessed, at least annually, for content, environmental protection and security. Ensure compatibility of hardware and software to restore archived data, and periodically test and refresh archived data.',
    'tImplementationGuide' => '',
  ),
  138 => 
  array (
    'fkContext' => '10524',
    'fkSectionBestPractice' => '10514',
    'nControlType' => '0',
    'sName' => 'DS4.10 Post-resumption Review',
    'tDescription' => 'Determine whether IT management has established procedures for assessing the adequacy of the plan in regard to the successful resumption of the IT function after a disaster, and update the plan accordingly.',
    'tImplementationGuide' => '',
  ),
  139 => 
  array (
    'fkContext' => '10526',
    'fkSectionBestPractice' => '10525',
    'nControlType' => '0',
    'sName' => 'DS5.1 Management of IT Security',
    'tDescription' => 'Manage IT security at the highest appropriate organisational level, so the management of security actions is in line with business requirements.',
    'tImplementationGuide' => '',
  ),
  140 => 
  array (
    'fkContext' => '10527',
    'fkSectionBestPractice' => '10525',
    'nControlType' => '0',
    'sName' => 'DS5.2 IT Security Plan',
    'tDescription' => 'Translate business, risk and compliance requirements into an overall IT security plan, taking into consideration the IT infrastructure and the security culture. Ensure that the plan is implemented in security policies and procedures together with appropriate investments in services, personnel, software and hardware. Communicate security policies and procedures to stakeholders and users.',
    'tImplementationGuide' => '',
  ),
  141 => 
  array (
    'fkContext' => '10528',
    'fkSectionBestPractice' => '10525',
    'nControlType' => '0',
    'sName' => 'DS5.3 Identity Management',
    'tDescription' => 'Ensure that all users (internal, external and temporary) and their activity on IT systems (business application, IT environment, system operations, development and maintenance) are uniquely identifiable. Enable user identities via authentication mechanisms. Confirm that user access rights to systems and data are in line with defined and documented business needs and that job requirements are attached to user identities. Ensure that user access rights are requested by user management, approved by system owners and implemented by the security-responsible person. Maintain user identities and access rights in a central repository. Deploy cost-effective technical and procedural measures, and keep them current to establish user identification, implement authentication and enforce access rights.',
    'tImplementationGuide' => '',
  ),
  142 => 
  array (
    'fkContext' => '10529',
    'fkSectionBestPractice' => '10525',
    'nControlType' => '0',
    'sName' => 'DS5.4 User Account Management',
    'tDescription' => 'Address requesting, establishing, issuing, suspending, modifying and closing user accounts and related user privileges with a set of user account management procedures. Include an approval procedure outlining the data or system owner granting the access privileges. These procedures should apply for all users, including administrators (privileged users) and internal and external users, for normal and emergency cases. Rights and obligations relative to access to enterprise systems and information should be contractually arranged for all types of users. Perform regular management review of all accounts and related privileges.',
    'tImplementationGuide' => '',
  ),
  143 => 
  array (
    'fkContext' => '10530',
    'fkSectionBestPractice' => '10525',
    'nControlType' => '0',
    'sName' => 'DS5.5 Security Testing, Surveillance and Monitoring',
    'tDescription' => 'Test and monitor the IT security implementation in a proactive way. IT security should be reaccredited in a timely manner to ensure that the approved enterprise\'s information security baseline is maintained. A logging and monitoring function will enable the early prevention and/or detection and subsequent timely reporting of unusual and/or abnormal activities that may need to be addressed.',
    'tImplementationGuide' => '',
  ),
  144 => 
  array (
    'fkContext' => '10531',
    'fkSectionBestPractice' => '10525',
    'nControlType' => '0',
    'sName' => 'DS5.6 Security Incident Definition',
    'tDescription' => 'Clearly define and communicate the characteristics of potential security incidents so they can be properly classified and treated by the incident and problem management process.',
    'tImplementationGuide' => '',
  ),
  145 => 
  array (
    'fkContext' => '10532',
    'fkSectionBestPractice' => '10525',
    'nControlType' => '0',
    'sName' => 'DS5.7 Protection of Security Technology',
    'tDescription' => 'Make security-related technology resistant to tampering, and do not disclose security documentation unnecessarily.',
    'tImplementationGuide' => '',
  ),
  146 => 
  array (
    'fkContext' => '10533',
    'fkSectionBestPractice' => '10525',
    'nControlType' => '0',
    'sName' => 'DS5.8 Cryptographic Key Management',
    'tDescription' => 'Determine that policies and procedures are in place to organise the generation, change, revocation, destruction, distribution, certification, storage, entry, use and archiving of cryptographic keys to ensure the protection of keys against modification and unauthorised disclosure.',
    'tImplementationGuide' => '',
  ),
  147 => 
  array (
    'fkContext' => '10534',
    'fkSectionBestPractice' => '10525',
    'nControlType' => '0',
    'sName' => 'DS5.9 Malicious Software Prevention, Detection and Correction',
    'tDescription' => 'Put preventive, detective and corrective measures in place (especially up-to-date security patches and virus control) across the organisation to protect information systems and technology from malware (e.g., viruses, worms, spyware, spam).',
    'tImplementationGuide' => '',
  ),
  148 => 
  array (
    'fkContext' => '10535',
    'fkSectionBestPractice' => '10525',
    'nControlType' => '0',
    'sName' => 'DS5.10 Network Security',
    'tDescription' => 'Use security techniques and related management procedures (e.g., firewalls, security appliances, network segmentation, intrusion detection) to authorise access and control information flows from and to networks.',
    'tImplementationGuide' => '',
  ),
  149 => 
  array (
    'fkContext' => '10536',
    'fkSectionBestPractice' => '10525',
    'nControlType' => '0',
    'sName' => 'DS5.11 Exchange of Sensitive Data',
    'tDescription' => 'Exchange sensitive transaction data only over a trusted path or medium with controls to provide authenticity of content, proof of submission, proof of receipt and non-repudiation of origin.',
    'tImplementationGuide' => '',
  ),
  150 => 
  array (
    'fkContext' => '10538',
    'fkSectionBestPractice' => '10537',
    'nControlType' => '0',
    'sName' => 'DS6.1 Definition of Services',
    'tDescription' => 'Identify all IT costs, and map them to IT services to support a transparent cost model. IT services should be linked to business processes such that the business can identify associated service billing levels.',
    'tImplementationGuide' => '',
  ),
  151 => 
  array (
    'fkContext' => '10539',
    'fkSectionBestPractice' => '10537',
    'nControlType' => '0',
    'sName' => 'DS6.2 IT Accounting',
    'tDescription' => 'Capture and allocate actual costs according to the enterprise cost model. Variances between forecasts and actual costs should be analysed and reported on, in compliance with the enterprise\'s financial measurement systems.',
    'tImplementationGuide' => '',
  ),
  152 => 
  array (
    'fkContext' => '10540',
    'fkSectionBestPractice' => '10537',
    'nControlType' => '0',
    'sName' => 'DS6.3 Cost Modelling and Charging',
    'tDescription' => 'Establish and use an IT costing model based on the service definitions that support the calculation of chargeback rates per service. The IT cost model should ensure that charging for services is identifiable, measurable and predictable by users to encourage proper use of resources.',
    'tImplementationGuide' => '',
  ),
  153 => 
  array (
    'fkContext' => '10541',
    'fkSectionBestPractice' => '10537',
    'nControlType' => '0',
    'sName' => 'DS6.4 Cost Model Maintenance',
    'tDescription' => 'Regularly review and benchmark the appropriateness of the cost/recharge model to maintain its relevance and appropriateness to the evolving business and IT activities.',
    'tImplementationGuide' => '',
  ),
  154 => 
  array (
    'fkContext' => '10543',
    'fkSectionBestPractice' => '10542',
    'nControlType' => '0',
    'sName' => 'DS7.1 Identification of Education and Training Needs',
    'tDescription' => 'Establish and regularly update a curriculum for each target group of employees considering:  - Current and future business needs and strategy  - Value of information as an asset  - Corporate values (ethical values, control and security culture, etc.)  - Implementation of new IT infrastructure and software (i.e., packages, applications)  - Current and future skills, competence profiles, and certification and/or credentialing needs as well as required reaccreditation  - Delivery methods (e.g., classroom, web-based), target group size, accessibility and timing',
    'tImplementationGuide' => '',
  ),
  155 => 
  array (
    'fkContext' => '10544',
    'fkSectionBestPractice' => '10542',
    'nControlType' => '0',
    'sName' => 'DS7.2 Delivery of Training and Education',
    'tDescription' => 'Based on the identified education and training needs, identify target groups and their members, efficient delivery mechanisms, teachers, trainers, and mentors. Appoint trainers and organise timely training sessions. Record registration (including prerequisites), attendance and training session performance evaluations.',
    'tImplementationGuide' => '',
  ),
  156 => 
  array (
    'fkContext' => '10545',
    'fkSectionBestPractice' => '10542',
    'nControlType' => '0',
    'sName' => 'DS7.3 Evaluation of Training Received',
    'tDescription' => 'Evaluate education and training content delivery upon completion for relevance, quality, effectiveness, the retention of knowledge, cost and value. The results of this evaluation should serve as input for future curriculum definition and the delivery of training sessions.',
    'tImplementationGuide' => '',
  ),
  157 => 
  array (
    'fkContext' => '10547',
    'fkSectionBestPractice' => '10546',
    'nControlType' => '0',
    'sName' => 'DS8.1 Service Desk',
    'tDescription' => 'Establish a service desk function, which is the user interface with IT, to register, communicate, dispatch and analyse all calls, reported incidents, service requests and information demands. There should be monitoring and escalation procedures based on agreed-upon service levels relative to the appropriate SLA that allow classification and prioritisation of any reported issue as an incident, service request or information request. Measure end users\' satisfaction with the quality of the service desk and IT services.',
    'tImplementationGuide' => '',
  ),
  158 => 
  array (
    'fkContext' => '10548',
    'fkSectionBestPractice' => '10546',
    'nControlType' => '0',
    'sName' => 'DS8.2 Registration of Customer Queries',
    'tDescription' => 'Establish a function and system to allow logging and tracking of calls, incidents, service requests and information needs. It should work closely with such processes as incident management, problem management, change management, capacity management and availability management. Incidents should be classified according to a business and service priority and routed to the appropriate problem management team, where necessary. Customers should be kept informed of the status of their queries.',
    'tImplementationGuide' => '',
  ),
  159 => 
  array (
    'fkContext' => '10549',
    'fkSectionBestPractice' => '10546',
    'nControlType' => '0',
    'sName' => 'DS8.3 Incident Escalation',
    'tDescription' => 'Establish service desk procedures, so incidents that cannot be resolved immediately are appropriately escalated according to limits defined in the SLA and, if appropriate, workarounds are provided. Ensure that incident ownership and life cycle monitoring remain with the service desk for user-based incidents, regardless which IT group is working on resolution activities.',
    'tImplementationGuide' => '',
  ),
  160 => 
  array (
    'fkContext' => '10550',
    'fkSectionBestPractice' => '10546',
    'nControlType' => '0',
    'sName' => 'DS8.4 Incident Closure',
    'tDescription' => 'Establish procedures for the timely monitoring of clearance of customer queries. When the incident has been resolved, ensure that the service desk records the resolution steps, and confirm that the action taken has been agreed to by the customer. Also record and report unresolved incidents (known errors and workarounds) to provide information for proper problem management.',
    'tImplementationGuide' => '',
  ),
  161 => 
  array (
    'fkContext' => '10551',
    'fkSectionBestPractice' => '10546',
    'nControlType' => '0',
    'sName' => 'DS8.5 Reporting and Trend Analysis',
    'tDescription' => 'Produce reports of service desk activity to enable management to measure service performance and service response times and to identify trends or recurring problems, so service can be continually improved.',
    'tImplementationGuide' => '',
  ),
  162 => 
  array (
    'fkContext' => '10553',
    'fkSectionBestPractice' => '10552',
    'nControlType' => '0',
    'sName' => 'DS9.1 Configuration Repository and Baseline',
    'tDescription' => 'Establish a supporting tool and a central repository to contain all relevant information on configuration items. Monitor and record all assets and changes to assets. Maintain a baseline of configuration items for every system and service as a checkpoint to which to return after changes.',
    'tImplementationGuide' => '',
  ),
  163 => 
  array (
    'fkContext' => '10554',
    'fkSectionBestPractice' => '10552',
    'nControlType' => '0',
    'sName' => 'DS9.2 Identification and Maintenance of Configuration Items',
    'tDescription' => 'Establish configuration procedures to support management and logging of all changes to the configuration repository. Integrate these procedures with change management, incident management and problem management procedures.',
    'tImplementationGuide' => '',
  ),
  164 => 
  array (
    'fkContext' => '10555',
    'fkSectionBestPractice' => '10552',
    'nControlType' => '0',
    'sName' => 'DS9.3 Configuration Integrity Review',
    'tDescription' => 'Periodically review the configuration data to verify and confirm the integrity of the current and historical configuration. Periodically review installed software against the policy for software usage to identify personal or unlicensed software or any software instances in excess of current license agreements. Report, act on and correct errors and deviations.',
    'tImplementationGuide' => '',
  ),
  165 => 
  array (
    'fkContext' => '10557',
    'fkSectionBestPractice' => '10556',
    'nControlType' => '0',
    'sName' => 'DS10.1 Identification and Classification of Problems',
    'tDescription' => 'Implement processes to report and classify problems that have been identified as part of incident management. The steps involved in problem classification are similar to the steps in classifying incidents; they are to determine category, impact, urgency and priority. Categorise problems as appropriate into related groups or domains (e.g., hardware, software, support software). These groups may match the organisational responsibilities of the user and customer base, and should be the basis for allocating problems to support staff.',
    'tImplementationGuide' => '',
  ),
  166 => 
  array (
    'fkContext' => '10558',
    'fkSectionBestPractice' => '10556',
    'nControlType' => '0',
    'sName' => 'DS10.2 Problem Tracking and Resolution',
    'tDescription' => 'Ensure that the problem management system provides for adequate audit trail facilities that allow tracking, analysing and determining the root cause of all reported problems considering:  - All associated configuration items  - Outstanding problems and incidents  - Known and suspected errors  - Tracking of problem trends  Identify and initiate sustainable solutions addressing the root cause, raising change requests via the established change management process. Throughout the resolution process, problem management should obtain regular reports from change management on progress in resolving problems and errors. Problem management should monitor the continuing impact of problems and known errors on user services. In the event that this impact becomes severe, problem management should escalate the problem, perhaps referring it to an appropriate board to increase the priority of the (RFC or to implement an urgent change as appropriate. Monitor the progress of problem resolution against SLAs.',
    'tImplementationGuide' => '',
  ),
  167 => 
  array (
    'fkContext' => '10559',
    'fkSectionBestPractice' => '10556',
    'nControlType' => '0',
    'sName' => 'DS10.3 Problem Closure',
    'tDescription' => 'Put in place a procedure to close problem records either after confirmation of successful elimination of the known error or after agreement with the business on how to alternatively handle the problem.',
    'tImplementationGuide' => '',
  ),
  168 => 
  array (
    'fkContext' => '10560',
    'fkSectionBestPractice' => '10556',
    'nControlType' => '0',
    'sName' => 'DS10.4 Integration of Configuration, Incident and Problem Management',
    'tDescription' => 'Integrate the related processes of configuration, incident and problem management to ensure effective management of problems and enable improvements.',
    'tImplementationGuide' => '',
  ),
  169 => 
  array (
    'fkContext' => '10562',
    'fkSectionBestPractice' => '10561',
    'nControlType' => '0',
    'sName' => 'DS11.1 Business Requirements for Data Management',
    'tDescription' => 'Verify that all data expected for processing are received and processed completely, accurately and in a timely manner, and all output is delivered in accordance with business requirements. Support restart and reprocessing needs.',
    'tImplementationGuide' => '',
  ),
  170 => 
  array (
    'fkContext' => '10563',
    'fkSectionBestPractice' => '10561',
    'nControlType' => '0',
    'sName' => 'DS11.2 Storage and Retention Arrangements',
    'tDescription' => 'Define and implement procedures for effective and efficient data storage, retention and archiving to meet business objectives, the organisation\'s security policy and regulatory requirements.',
    'tImplementationGuide' => '',
  ),
  171 => 
  array (
    'fkContext' => '10564',
    'fkSectionBestPractice' => '10561',
    'nControlType' => '0',
    'sName' => 'DS11.3 Media Library Management System',
    'tDescription' => 'Define and implement procedures to maintain na inventory of stored and archived media to ensure their usability and integrity.',
    'tImplementationGuide' => '',
  ),
  172 => 
  array (
    'fkContext' => '10565',
    'fkSectionBestPractice' => '10561',
    'nControlType' => '0',
    'sName' => 'DS11.4 Disposal',
    'tDescription' => 'Define and implement procedures to ensure that business requirements for protection of sensitive data and software are met when data and hardware are disposed or transferred.',
    'tImplementationGuide' => '',
  ),
  173 => 
  array (
    'fkContext' => '10566',
    'fkSectionBestPractice' => '10561',
    'nControlType' => '0',
    'sName' => 'DS11.5 Backup and Restoration',
    'tDescription' => 'Define and implement procedures for backup and restoration of systems, applications, data and documentation in line with business requirements and the continuity plan.',
    'tImplementationGuide' => '',
  ),
  174 => 
  array (
    'fkContext' => '10567',
    'fkSectionBestPractice' => '10561',
    'nControlType' => '0',
    'sName' => 'DS11.6 Security Requirements for Data Management',
    'tDescription' => 'Define and implement policies and procedures to identify and apply security requirements applicable to the receipt, processing, storage and output of data to meet business objectives, the organisation\'s security policy and regulatory requirements.',
    'tImplementationGuide' => '',
  ),
  175 => 
  array (
    'fkContext' => '10569',
    'fkSectionBestPractice' => '10568',
    'nControlType' => '0',
    'sName' => 'DS12.1 Site Selection and Layout',
    'tDescription' => 'Define and select the physical sites for IT equipment to support the technology strategy linked to the business strategy. The selection and design of the layout of a site should take into account the risk associated with natural and man-made disasters, whilst considering relevant laws and regulations, such as occupational health and safety regulations.',
    'tImplementationGuide' => '',
  ),
  176 => 
  array (
    'fkContext' => '10570',
    'fkSectionBestPractice' => '10568',
    'nControlType' => '0',
    'sName' => 'DS12.2 Physical Security Measures',
    'tDescription' => 'Define and implement physical security measures in line with business requirements to secure the location and the physical assets. Physical security measures must be capable of effectively preventing, detecting and mitigating risks relating to theft, temperature, fire, smoke, water, vibration, terror, vandalism, power outages, chemicals or explosives.',
    'tImplementationGuide' => '',
  ),
  177 => 
  array (
    'fkContext' => '10571',
    'fkSectionBestPractice' => '10568',
    'nControlType' => '0',
    'sName' => 'DS12.3 Physical Access',
    'tDescription' => 'Define and implement procedures to grant, limit and revoke access to premises, buildings and areas according to business needs, including emergencies. Access to premises, buildings and areas should be justified, authorised, logged and monitored. This should apply to all persons entering the premises, including staff, temporary staff, clients, vendors, visitors or any other third party.',
    'tImplementationGuide' => '',
  ),
  178 => 
  array (
    'fkContext' => '10572',
    'fkSectionBestPractice' => '10568',
    'nControlType' => '0',
    'sName' => 'DS12.4 Protection Against Environmental Factors',
    'tDescription' => 'Design and implement measures for protection against environmental factors. Install specialised equipment and devices to monitor and control the environment.',
    'tImplementationGuide' => '',
  ),
  179 => 
  array (
    'fkContext' => '10573',
    'fkSectionBestPractice' => '10568',
    'nControlType' => '0',
    'sName' => 'DS12.5 Physical Facilities Management',
    'tDescription' => 'Manage facilities, including power and communications equipment, in line with laws and regulations, technical and business requirements, vendor specifications, and health and safety guidelines.',
    'tImplementationGuide' => '',
  ),
  180 => 
  array (
    'fkContext' => '10575',
    'fkSectionBestPractice' => '10574',
    'nControlType' => '0',
    'sName' => 'DS13.1 Operations Procedures and Instructions',
    'tDescription' => 'Define, implement and maintain procedures for IT operations, ensuring that the operations staff members are familiar with all operations tasks relevant to them. Operational procedures should cover shift handover (formal handover of activity, status updates, operational problems, escalation procedures and reports on current responsibilities) to support agreed-upon service levels and ensure continuous operations.',
    'tImplementationGuide' => '',
  ),
  181 => 
  array (
    'fkContext' => '10576',
    'fkSectionBestPractice' => '10574',
    'nControlType' => '0',
    'sName' => 'DS13.2 Job Scheduling',
    'tDescription' => 'Organise the scheduling of jobs, processes and tasks into the most efficient sequence, maximising throughput and utilisation to meet business requirements.',
    'tImplementationGuide' => '',
  ),
  182 => 
  array (
    'fkContext' => '10577',
    'fkSectionBestPractice' => '10574',
    'nControlType' => '0',
    'sName' => 'DS13.3 IT Infrastructure Monitoring',
    'tDescription' => 'Define and implement procedures to monitor the IT infrastructure and related events. Ensure that sufficient chronological information is being stored in operations logs to enable the reconstruction, review and examination of the time sequences of operations and the other activities surrounding or supporting operations.',
    'tImplementationGuide' => '',
  ),
  183 => 
  array (
    'fkContext' => '10578',
    'fkSectionBestPractice' => '10574',
    'nControlType' => '0',
    'sName' => 'DS13.4 Sensitive Documents and Output Devices',
    'tDescription' => 'Establish appropriate physical safeguards, accounting practices and inventory management over sensitive IT assets, such as special forms, negotiable instruments, special purpose printers or security tokens.',
    'tImplementationGuide' => '',
  ),
  184 => 
  array (
    'fkContext' => '10579',
    'fkSectionBestPractice' => '10574',
    'nControlType' => '0',
    'sName' => 'DS13.5 Preventive Maintenance for Hardware',
    'tDescription' => 'Define and implement procedures to ensure timely maintenance of infrastructure to reduce the frequency and impact of failures or performance degradation.',
    'tImplementationGuide' => '',
  ),
  185 => 
  array (
    'fkContext' => '10582',
    'fkSectionBestPractice' => '10581',
    'nControlType' => '0',
    'sName' => 'ME1.1 Monitoring Approach',
    'tDescription' => 'Establish a general monitoring framework and approach to define the scope, methodology and process to be followed for measuring IT\'s solution and service delivery, and monitor IT\'s contribution to the business. Integrate the framework with the corporate performance management system.',
    'tImplementationGuide' => '',
  ),
  186 => 
  array (
    'fkContext' => '10583',
    'fkSectionBestPractice' => '10581',
    'nControlType' => '0',
    'sName' => 'ME1.2 Definition and Collection of Monitoring Data',
    'tDescription' => 'Work with the business to define a balanced set of performance targets and have them approved by the business and other relevant stakeholders. Define benchmarks with which to compare the targets, and identify available data to be collected to measure the targets. Establish processes to collect timely and accurate data to report on progress against targets.',
    'tImplementationGuide' => '',
  ),
  187 => 
  array (
    'fkContext' => '10584',
    'fkSectionBestPractice' => '10581',
    'nControlType' => '0',
    'sName' => 'ME1.3 Monitoring Method',
    'tDescription' => 'Deploy a performance monitoring method (e.g., balanced scorecard) that records targets; captures measurements; provides a succinct, all-around view of IT performance; and fits within the enterprise monitoring system.',
    'tImplementationGuide' => '',
  ),
  188 => 
  array (
    'fkContext' => '10585',
    'fkSectionBestPractice' => '10581',
    'nControlType' => '0',
    'sName' => 'ME1.4 Performance Assessment',
    'tDescription' => 'Periodically review performance against targets, analyse the cause of any deviations, and initiate remedial action to address the underlying causes. At appropriate times, perform root cause analysis across deviations.',
    'tImplementationGuide' => '',
  ),
  189 => 
  array (
    'fkContext' => '10586',
    'fkSectionBestPractice' => '10581',
    'nControlType' => '0',
    'sName' => 'ME1.5 Board and Executive Reporting',
    'tDescription' => 'Develop senior management reports on IT\'s contribution to the business, specifically in terms of the performance of the enterprise\'s portfolio, IT-enabled investment programmes, and the solution and service deliverable performance of individual programmes. Include in status reports the extent to which planned objectives have been achieved, budgeted resources used,  set performance targets met and identified risks mitigated. Anticipate senior management\'s review by suggesting remedial actions for major deviations. Provide the report to senior management, and solicit feedback from management\'s review.',
    'tImplementationGuide' => '',
  ),
  190 => 
  array (
    'fkContext' => '10587',
    'fkSectionBestPractice' => '10581',
    'nControlType' => '0',
    'sName' => 'ME1.6 Remedial Actions',
    'tDescription' => 'Identify and initiate remedial actions based on performance monitoring, assessment and reporting. This includes follow-up of all monitoring, reporting and assessments through: - Review, negotiation and establishment of management responses - Assignment of responsibility for remediation - Tracking of the results of actions committed',
    'tImplementationGuide' => '',
  ),
  191 => 
  array (
    'fkContext' => '10589',
    'fkSectionBestPractice' => '10588',
    'nControlType' => '0',
    'sName' => 'ME2.1 Monitoring of Internal Control Framework',
    'tDescription' => 'Continuously monitor, benchmark and improve the IT control environment and control framework to meet organisational objectives.',
    'tImplementationGuide' => '',
  ),
  192 => 
  array (
    'fkContext' => '10590',
    'fkSectionBestPractice' => '10588',
    'nControlType' => '0',
    'sName' => 'ME2.2 Supervisory Review',
    'tDescription' => 'Monitor and evaluate the efficiency and effectiveness of internal IT managerial review controls.',
    'tImplementationGuide' => '',
  ),
  193 => 
  array (
    'fkContext' => '10591',
    'fkSectionBestPractice' => '10588',
    'nControlType' => '0',
    'sName' => 'ME2.3 Control Exceptions',
    'tDescription' => 'Identify control exceptions, and analyse and identify their underlying root causes. Escalate control exceptions and report to stakeholders appropriately. Institute necessary corrective action.',
    'tImplementationGuide' => '',
  ),
  194 => 
  array (
    'fkContext' => '10592',
    'fkSectionBestPractice' => '10588',
    'nControlType' => '0',
    'sName' => 'ME2.4 Control Self-assessment',
    'tDescription' => 'Evaluate the completeness and effectiveness of management\'s control over IT processes, policies and contracts through a continuing programme of self-assessment.',
    'tImplementationGuide' => '',
  ),
  195 => 
  array (
    'fkContext' => '10593',
    'fkSectionBestPractice' => '10588',
    'nControlType' => '0',
    'sName' => 'ME2.5 Assurance of Internal Control',
    'tDescription' => 'Obtain, as needed, further assurance of the completeness and effectiveness of internal controls through third-party reviews.',
    'tImplementationGuide' => '',
  ),
  196 => 
  array (
    'fkContext' => '10594',
    'fkSectionBestPractice' => '10588',
    'nControlType' => '0',
    'sName' => 'ME2.6 Internal Control at Third Parties',
    'tDescription' => 'Assess the status of external service providers\' internal controls. Confirm that external service providers comply with legal and regulatory requirements and contractual obligations.',
    'tImplementationGuide' => '',
  ),
  197 => 
  array (
    'fkContext' => '10595',
    'fkSectionBestPractice' => '10588',
    'nControlType' => '0',
    'sName' => 'ME2.7 Remedial Actions',
    'tDescription' => 'Identify, initiate, track and implement remedial actions arising from control assessments and reporting.',
    'tImplementationGuide' => '',
  ),
  198 => 
  array (
    'fkContext' => '10597',
    'fkSectionBestPractice' => '10596',
    'nControlType' => '0',
    'sName' => 'ME3.1 Identification of External Legal, Regulatory and Contractual Compliance Requirements',
    'tDescription' => 'Identify, on a continuous basis, local and international laws, regulations, and other external requirements that must be complied with for incorporation into the organisation\'s IT policies, standards, procedures and methodologies.',
    'tImplementationGuide' => '',
  ),
  199 => 
  array (
    'fkContext' => '10598',
    'fkSectionBestPractice' => '10596',
    'nControlType' => '0',
    'sName' => 'ME3.2 Optimisation of Response to External Requirements',
    'tDescription' => 'Review and adjust IT policies, standards, procedures and methodologies to ensure that legal, regulatory and contractual requirements are addressed and communicated.',
    'tImplementationGuide' => '',
  ),
  200 => 
  array (
    'fkContext' => '10599',
    'fkSectionBestPractice' => '10596',
    'nControlType' => '0',
    'sName' => 'ME3.3 Evaluation of Compliance With External Requirements',
    'tDescription' => 'Confirm compliance of IT policies, standards, procedures and methodologies with legal and regulatory requirements.',
    'tImplementationGuide' => '',
  ),
  201 => 
  array (
    'fkContext' => '10600',
    'fkSectionBestPractice' => '10596',
    'nControlType' => '0',
    'sName' => 'ME3.4 Positive Assurance of Compliance',
    'tDescription' => 'Obtain and report assurance of compliance and adherence to all internal policies derived from internal directives or external legal, regulatory or contractual requirements, confirming that any corrective actions to address any compliance gaps have been taken by the responsible process owner in a timely manner.',
    'tImplementationGuide' => '',
  ),
  202 => 
  array (
    'fkContext' => '10601',
    'fkSectionBestPractice' => '10596',
    'nControlType' => '0',
    'sName' => 'ME3.5 Integrated Reporting',
    'tDescription' => 'Integrate IT reporting on legal, regulatory and contractual requirements with similar output from other business functions.',
    'tImplementationGuide' => '',
  ),
  203 => 
  array (
    'fkContext' => '10603',
    'fkSectionBestPractice' => '10602',
    'nControlType' => '0',
    'sName' => 'ME4.1 Establishment of an IT Governance Framework',
    'tDescription' => 'Define, establish and align the IT governance framework with the overall enterprise governance and control environment. Base the framework on a suitable IT process and control model and provide for unambiguous accountability and practices to avoid a breakdown in internal control and oversight. Confirm that the IT governance framework ensures compliance with laws and regulations and is aligned with, and confirms delivery of, the enterprise\'s strategies and objectives. Report IT governance status and issues.',
    'tImplementationGuide' => '',
  ),
  204 => 
  array (
    'fkContext' => '10604',
    'fkSectionBestPractice' => '10602',
    'nControlType' => '0',
    'sName' => 'ME4.2 Strategic Alignment',
    'tDescription' => 'Enable board and executive understanding of strategic IT issues, such as the role of IT, technology insights and capabilities. Ensure that there is a shared understanding between the business and IT regarding the potential contribution of IT to the business strategy. Work with the board and the established governance bodies, such as an IT strategy committee, to provide strategic direction to management relative to IT, ensuring that the strategy and objectives are cascaded into business units and IT functions, and that confidence and trust are developed between the business and IT. Enable the alignment of IT to the business in strategy and operations, encouraging co-responsibility between the business and IT for making strategic decisions and obtaining benefits from IT-enabled investments.',
    'tImplementationGuide' => '',
  ),
  205 => 
  array (
    'fkContext' => '10605',
    'fkSectionBestPractice' => '10602',
    'nControlType' => '0',
    'sName' => 'ME4.3 Value Delivery',
    'tDescription' => 'Manage IT-enabled investment programmes and other IT assets and services to ensure that they deliver the greatest possible value in supporting the enterprise\'s strategy and objectives. Ensure that the expected business outcomes of IT-enabled investments and the full scope of effort required to achieve those outcomes are understood; that comprehensive and consistent business cases are created and approved by stakeholders; that assets and investments are managed throughout their economic life cycle; and that there is active management of the realisation of benefits, such as contribution to new services, efficiency gains and improved responsiveness to customer demands. Enforce a disciplined approach to portfolio, programme and project management, insisting that the business takes ownership of all IT-enabled investments and IT ensures optimisation of the costs of delivering IT capabilities and services.',
    'tImplementationGuide' => '',
  ),
  206 => 
  array (
    'fkContext' => '10606',
    'fkSectionBestPractice' => '10602',
    'nControlType' => '0',
    'sName' => 'ME4.4 Resource Management',
    'tDescription' => 'Oversee the investment, use and allocation of IT resources through regular assessments of IT initiatives and operations to ensure appropriate resourcing and alignment with current and future strategic objectives and business imperatives.',
    'tImplementationGuide' => '',
  ),
  207 => 
  array (
    'fkContext' => '10607',
    'fkSectionBestPractice' => '10602',
    'nControlType' => '0',
    'sName' => 'ME4.5 Risk Management',
    'tDescription' => 'Work with the board to define the enterprise\'s appetite for IT risk, and obtain reasonable assurance that IT risk management practices are appropriate to ensure that the actual IT risk does not exceed the board\'s risk appetite. Embed risk management responsibilities into the organisation, ensuring that the business and IT regularly assess and report IT-related risks and their impact and that the enterprise\'s IT risk position is transparent to all stakeholders.',
    'tImplementationGuide' => '',
  ),
  208 => 
  array (
    'fkContext' => '10608',
    'fkSectionBestPractice' => '10602',
    'nControlType' => '0',
    'sName' => 'ME4.6 Performance Measurement',
    'tDescription' => 'Confirm that agreed-upon IT objectives have been met or exceeded, or that progress toward IT goals meets expectations. Where agreed-upon objectives have been missed or progress is not as expected, review management\'s remedial action. Report to the board relevant portfolios, programme and IT performance, supported by reports to enable senior management to review the enterprise\'s progress toward identified goals.',
    'tImplementationGuide' => '',
  ),
  209 => 
  array (
    'fkContext' => '10609',
    'fkSectionBestPractice' => '10602',
    'nControlType' => '0',
    'sName' => 'ME4.7 Independent Assurance',
    'tDescription' => 'Obtain independent assurance (internal or external) about the conformance of IT with relevant laws and regulations; the organisation\'s policies, standards and procedures;generally accepted practices; and the effective and efficient performance of IT.',
    'tImplementationGuide' => '',
  ),
  210 => 
  array (
    'fkContext' => '10612',
    'fkSectionBestPractice' => '10611',
    'nControlType' => '1',
    'sName' => 'Existence of job description documents.',
    'tDescription' => 'Management specifies the level of competence needed for particular jobs and translates the desired levels of competence into requisite knowledge and skills.',
    'tImplementationGuide' => 'Select a sample of 25 employees and determine if job descriptions are available.  Obtain security access records for the folder where job descriptions are maintained and determine whether non-HR employees have write access.',
  ),
  211 => 
  array (
    'fkContext' => '10613',
    'fkSectionBestPractice' => '10611',
    'nControlType' => '1',
    'sName' => 'Evidence exists indicating that employees have the requisite knowledge and skills.',
    'tDescription' => 'Management specifies the level of competence needed for particular jobs and translates the desired levels of competence into requisite knowledge and skills.',
    'tImplementationGuide' => 'Obtain copies of hiring procedures and determine whether procedures include guidance for ensuring employees wit requisite skills are hired.  Select a sample of 25 employees and note whether recent evaluations in accordance with policy are available.  Note what their rating is and chart to the bell curve.  If any low performers were selected in the sample, determine if appropriate action was taken.  Obtain documentation of the requirements for the annual performance review system and select a sample of 25 employees and determine if performance reviews were conducted in accordance with policy.',
  ),
  212 => 
  array (
    'fkContext' => '10615',
    'fkSectionBestPractice' => '10614',
    'nControlType' => '1',
    'sName' => 'The Board consists of members independent from management.',
    'tDescription' => 'An active and effective board, or committees thereof, provides an oversight function in ensuring effective internal control.',
    'tImplementationGuide' => 'Obtain the Bylaws and determine whether review of independence is required.  If anyone has been admitted during the current year, obtain documentation supporting this review/evaluation.  Obtain a sample of 5 questionnaires and determine if questions regarding independence were addressed.',
  ),
  213 => 
  array (
    'fkContext' => '10616',
    'fkSectionBestPractice' => '10614',
    'nControlType' => '1',
    'sName' => 'The Board constructively challenges management\'s planned decisions, e.g., strategic initiatives and major transactions, and probes for explanations of past results.',
    'tDescription' => 'An active and effective board, or committees thereof, provides an oversight function in ensuring effective internal control.',
    'tImplementationGuide' => 'Obtain a sample of minutes from 10 meetings of the Board and Committees (10 in total) and note whether members asked probing questions, and whether financial statements (where appropriate) and/or other information provided was reviewed by the members present.',
  ),
  214 => 
  array (
    'fkContext' => '10617',
    'fkSectionBestPractice' => '10614',
    'nControlType' => '1',
    'sName' => 'Board committees are used where warranted, e.g., Audit Committee, Credit Committee.',
    'tDescription' => 'An active and effective board, or committees thereof, provides an oversight function in ensuring effective internal control.',
    'tImplementationGuide' => 'The Board has implemented the committees it deems necessary to address Alamosa\'s needs - no test required.',
  ),
  215 => 
  array (
    'fkContext' => '10618',
    'fkSectionBestPractice' => '10614',
    'nControlType' => '1',
    'sName' => 'Directors have sufficient knowledge, industry experience, and time to serve effectively.',
    'tDescription' => 'An active and effective board, or committees thereof, provides an oversight function in ensuring effective internal control.',
    'tImplementationGuide' => 'Obtain a sample of 5 questionnaires and determine if questions regarding competencies were addressed.',
  ),
  216 => 
  array (
    'fkContext' => '10619',
    'fkSectionBestPractice' => '10614',
    'nControlType' => '1',
    'sName' => 'The Audit Committee meets with the CFO, internal and external auditors frequently.',
    'tDescription' => 'An active and effective board, or committees thereof, provides an oversight function in ensuring effective internal control.',
    'tImplementationGuide' => 'Obtain minutes of two meetings during the year to determine whether the Audit Committee met with the CFO and the internal and external auditors.',
  ),
  217 => 
  array (
    'fkContext' => '10620',
    'fkSectionBestPractice' => '10614',
    'nControlType' => '1',
    'sName' => 'The Audit Committee reviews the scope of activities of the internal and external auditors annually.',
    'tDescription' => 'An active and effective board, or committees thereof, provides an oversight function in ensuring effective internal control.',
    'tImplementationGuide' => 'Obtain minutes showing compliance with the description of the existing control.',
  ),
  218 => 
  array (
    'fkContext' => '10621',
    'fkSectionBestPractice' => '10614',
    'nControlType' => '1',
    'sName' => 'The Audit Committee meets privately with the CFO and internal and external auditors to discuss the reasonableness of the financial reporting process, system of internal control, significant comments and recommendations, and management\'s performance.',
    'tDescription' => 'An active and effective board, or committees thereof, provides an oversight function in ensuring effective internal control.',
    'tImplementationGuide' => 'Obtain minutes of two meetings during the year to determine whether the Audit Committee met privately with the external auditors.  ',
  ),
  219 => 
  array (
    'fkContext' => '10622',
    'fkSectionBestPractice' => '10614',
    'nControlType' => '1',
    'sName' => 'The Board regularly receives timely key information such as financial statements, significant contracts, and major marketing initiatives.',
    'tDescription' => 'An active and effective board, or committees thereof, provides an oversight function in ensuring effective internal control.',
    'tImplementationGuide' => 'Obtain minutes of two meetings during the year to determine whether the Board received timely information (e.g., financial statements, etc.)',
  ),
  220 => 
  array (
    'fkContext' => '10623',
    'fkSectionBestPractice' => '10614',
    'nControlType' => '1',
    'sName' => 'The Board or Audit Committee is apprised timely of sensitive information, investigations, and improper acts.  A process exists for informing the Board or Audit Committee of significant issues.',
    'tDescription' => 'Obtain minutes of two meetings during the year to determine whether the Audit Committee received timely information about fraud, follow-up status, etc.',
    'tImplementationGuide' => 'Obtain minutes of two meetings during the year to determine whether the Audit Committee received timely information about fraud, follow-up status, etc.',
  ),
  221 => 
  array (
    'fkContext' => '10624',
    'fkSectionBestPractice' => '10614',
    'nControlType' => '1',
    'sName' => 'The Audit Committee deals with compensation and retention issues regarding the internal auditor.',
    'tDescription' => 'An active and effective board, or committees thereof, provides an oversight function in ensuring effective internal control.',
    'tImplementationGuide' => 'Obtain minutes that indicate that the Audit Committee considered compensation and retention of the externally-provided internal audit function.',
  ),
  222 => 
  array (
    'fkContext' => '10625',
    'fkSectionBestPractice' => '10614',
    'nControlType' => '1',
    'sName' => 'The Compensation Committee approves all management incentive plans tied to performance.',
    'tDescription' => 'An active and effective board, or committees thereof, provides an oversight function in ensuring effective internal control.',
    'tImplementationGuide' => 'Obtain minutes of two meetings of the Compensation Committee and note whether short- and long-term incentives were properly approved.',
  ),
  223 => 
  array (
    'fkContext' => '10626',
    'fkSectionBestPractice' => '10614',
    'nControlType' => '1',
    'sName' => 'The Board specifically addresses management\'s adherence to the code of conduct.',
    'tDescription' => 'An active and effective board, or committees thereof, provides an oversight function in ensuring effective internal control.',
    'tImplementationGuide' => 'Obtain minutes that indicate that the Baord addressed management\'s adherence to the code of conduct in the Code of Business Conduct and Ethics.',
  ),
  224 => 
  array (
    'fkContext' => '10627',
    'fkSectionBestPractice' => '10614',
    'nControlType' => '1',
    'sName' => 'The Board and Audit Committee are involved sufficiently in monitoring the effectiveness of the \'tone at the top.\'',
    'tDescription' => 'An active and effective board, or committees thereof, provides an oversight function in ensuring effective internal control.',
    'tImplementationGuide' => 'Obtain minutes that indicate that the Board evaluated the CEO and the \'tone at the top\' that he sets.',
  ),
  225 => 
  array (
    'fkContext' => '10628',
    'fkSectionBestPractice' => '10614',
    'nControlType' => '1',
    'sName' => 'The Board and Audit Committee are involved sufficiently in monitoring the effectiveness of the \'tone at the top.\'',
    'tDescription' => 'An active and effective board, or committees thereof, provides an oversight function in ensuring effective internal control.',
    'tImplementationGuide' => 'Obtain minutes of two meetings of the Audit Committee and note whether any of the issues that impact the \'tone at the top\' were addressed.',
  ),
  226 => 
  array (
    'fkContext' => '10629',
    'fkSectionBestPractice' => '10614',
    'nControlType' => '1',
    'sName' => 'As a result of its findings, the Board or Audit Committee takes actions including special investigations, as needed.',
    'tDescription' => 'An active and effective board, or committees thereof, provides an oversight function in ensuring effective internal control.',
    'tImplementationGuide' => 'Obtain the Governance Committee charter and determine if there is provision for investigations as required by the expected control.  Obtain minutes of two meetings and determine if activities around investigations (requesting an investigation, following up on an existing investigation, etc.) are addressed.',
  ),
  227 => 
  array (
    'fkContext' => '10630',
    'fkSectionBestPractice' => '10614',
    'nControlType' => '1',
    'sName' => 'The Audit Committee has the authority to engage independent counsel and other advisers, as it determines necessary to carry out its duties.',
    'tDescription' => 'An active and effective board, or committees thereof, provides an oversight function in ensuring effective internal control.',
    'tImplementationGuide' => 'Obtain the current Audit Committee charter and determine if the required provision is included.',
  ),
  228 => 
  array (
    'fkContext' => '10631',
    'fkSectionBestPractice' => '10614',
    'nControlType' => '1',
    'sName' => 'The Audit Committee is active in inquiring about fraud and other related issues.',
    'tDescription' => 'An active and effective board, or committees thereof, provides an oversight function in ensuring effective internal control.',
    'tImplementationGuide' => 'Obtain copies of the Audit Committee meeting agendas since May 3, 2004 and determine if the fraud agenda item is included.',
  ),
  229 => 
  array (
    'fkContext' => '10632',
    'fkSectionBestPractice' => '10614',
    'nControlType' => '1',
    'sName' => 'The Company has designated someone to report fraud and related issues to the Audit Committee.',
    'tDescription' => 'An active and effective board, or committees thereof, provides an oversight function in ensuring effective internal control.',
    'tImplementationGuide' => 'Obtain minutes and presentation materials demonstating this.',
  ),
  230 => 
  array (
    'fkContext' => '10633',
    'fkSectionBestPractice' => '10614',
    'nControlType' => '1',
    'sName' => 'There is formal documentation via meeting agendas, minutes, presentations made, etc. documenting fraud related issues covered, including questions asked.',
    'tDescription' => 'An active and effective board, or committees thereof, provides an oversight function in ensuring effective internal control.',
    'tImplementationGuide' => 'Obtain copies of the Audit Committee meeting agendas since May 3, 2004 and determine if the fraud agenda item is included.


Obtain minutes and presentation materials demonstating this.',
  ),
  231 => 
  array (
    'fkContext' => '10635',
    'fkSectionBestPractice' => '10634',
    'nControlType' => '1',
    'sName' => 'Management has a policy that analyzes the risks and benefits of a potential venture.',
    'tDescription' => 'Management is active and effective in providing oversight, guidance and enhancing an effective control environment.',
    'tImplementationGuide' => 'Obtain the practices and processes documentation and determine if it addresses analyzing the risks and benefits of a potential venture.',
  ),
  232 => 
  array (
    'fkContext' => '10636',
    'fkSectionBestPractice' => '10634',
    'nControlType' => '1',
    'sName' => 'There is a low level of personnel turnover in key functions such as accounting, operating, data processing, and internal audit.',
    'tDescription' => 'Management is active and effective in providing oversight, guidance and enhancing an effective control environment.',
    'tImplementationGuide' => 'No testing necessary.',
  ),
  233 => 
  array (
    'fkContext' => '10637',
    'fkSectionBestPractice' => '10634',
    'nControlType' => '1',
    'sName' => 'Accounting principles are not selected based on the favourable financial results of acceptable principles.',
    'tDescription' => 'Management is active and effective in providing oversight, guidance and enhancing an effective control environment.',
    'tImplementationGuide' => 'No testing necessary.',
  ),
  234 => 
  array (
    'fkContext' => '10638',
    'fkSectionBestPractice' => '10634',
    'nControlType' => '1',
    'sName' => 'Attitudes and actions toward financial reporting, including disputes over application of accounting treatments.',
    'tDescription' => 'Management is active and effective in providing oversight, guidance and enhancing an effective control environment.',
    'tImplementationGuide' => 'Obtain copies of Audit Committee meeting minutes from two meetings and determine that the Committee oversees the accounting and financial reporting processes.',
  ),
  235 => 
  array (
    'fkContext' => '10639',
    'fkSectionBestPractice' => '10634',
    'nControlType' => '1',
    'sName' => 'Operating management sign-off on reported results.',
    'tDescription' => 'Management is active and effective in providing oversight, guidance and enhancing an effective control environment.',
    'tImplementationGuide' => 'Obtain route sheets from two quarterly (or one quarterly and one annual) financial statements, and determine that there was proper sign-off.',
  ),
  236 => 
  array (
    'fkContext' => '10640',
    'fkSectionBestPractice' => '10634',
    'nControlType' => '1',
    'sName' => 'Management\'s attitude toward the data processing and accounting functions, and reliance on financial reporting.',
    'tDescription' => 'Management is active and effective in providing oversight, guidance and enhancing an effective control environment.',
    'tImplementationGuide' => 'No testing necessary.',
  ),
  237 => 
  array (
    'fkContext' => '10641',
    'fkSectionBestPractice' => '10634',
    'nControlType' => '1',
    'sName' => 'Valuable physical assets are protected from unauthorized access or use.',
    'tDescription' => 'Management is active and effective in providing oversight, guidance and enhancing an effective control environment.',
    'tImplementationGuide' => 'Observe the tracking system and obtain a basic understanding.  Describe results.  Obtain copies of inventory reports from the warehouse (sample of 5) to determine that assets are properly recorded & tracked.


Obtain system documentation showing the locations where identification cards are in use.',
  ),
  238 => 
  array (
    'fkContext' => '10642',
    'fkSectionBestPractice' => '10634',
    'nControlType' => '1',
    'sName' => 'Financial assets are protected from unauthorized access or use.',
    'tDescription' => 'Management is active and effective in providing oversight, guidance and enhancing an effective control environment.',
    'tImplementationGuide' => 'See testing in Treasury/Debt.',
  ),
  239 => 
  array (
    'fkContext' => '10643',
    'fkSectionBestPractice' => '10634',
    'nControlType' => '1',
    'sName' => 'Computer systems and networks, including intellectual assets are protected from unauthorized access or use.',
    'tDescription' => 'Management is active and effective in providing oversight, guidance and enhancing an effective control environment.',
    'tImplementationGuide' => 'Observe the tracking system and obtain a basic understanding.  Describe results.  Obtain copies of inventory reports from the warehouse (sample of 5) to determine that assets are properly recorded & tracked.


Obtain system documentation showing the locations where identification cards are in use.


&


See Information Systems control testing for system access.  Testing NCN for safeguarding hard-copy documents.',
  ),
  240 => 
  array (
    'fkContext' => '10644',
    'fkSectionBestPractice' => '10634',
    'nControlType' => '1',
    'sName' => 'Senior managers frequently visit subsidiary or divisional operations.  Group or divisional management meetings are held frequently.',
    'tDescription' => 'Management is active and effective in providing oversight, guidance and enhancing an effective control environment.',
    'tImplementationGuide' => 'No testing necessary.',
  ),
  241 => 
  array (
    'fkContext' => '10646',
    'fkSectionBestPractice' => '10645',
    'nControlType' => '1',
    'sName' => 'The organizational structure facilitates the flow of information upstream, downstream, and across all business activities.',
    'tDescription' => 'Executives fully understand their control responsibilities and possess the requisite experience and levels of knowledge commensurate with their positions.',
    'tImplementationGuide' => 'No testing necessary.',
  ),
  242 => 
  array (
    'fkContext' => '10647',
    'fkSectionBestPractice' => '10645',
    'nControlType' => '1',
    'sName' => 'Responsibilities and expectations for the entity\'s business activities are communicated clearly to the executives in charge of those activities.',
    'tDescription' => 'Executives fully understand their control responsibilities and possess the requisite experience and levels of knowledge commensurate with their positions.',
    'tImplementationGuide' => 'Obtain documentation of objectives for the February 2004 planning session and objectives (if already available) for the September 2004 session.',
  ),
  243 => 
  array (
    'fkContext' => '10648',
    'fkSectionBestPractice' => '10645',
    'nControlType' => '1',
    'sName' => 'Established reporting relationships are effective and provide managers information appropriate to their responsibilities and authority.  Executives of the business activities have access to senior operating executives.',
    'tDescription' => 'Executives fully understand their control responsibilities and possess the requisite experience and levels of knowledge commensurate with their positions.',
    'tImplementationGuide' => 'No testing necessary.',
  ),
  244 => 
  array (
    'fkContext' => '10649',
    'fkSectionBestPractice' => '10645',
    'nControlType' => '1',
    'sName' => 'Management periodically evaluates the entity\'s organizational structure in light of changes in the business or industry.',
    'tDescription' => 'Executives fully understand their control responsibilities and possess the requisite experience and levels of knowledge commensurate with their positions.',
    'tImplementationGuide' => 'No testing necessary.',
  ),
  245 => 
  array (
    'fkContext' => '10650',
    'fkSectionBestPractice' => '10645',
    'nControlType' => '1',
    'sName' => 'Managers and supervisors have sufficient time to carry out their responsibilities effectively.',
    'tDescription' => 'Executives fully understand their control responsibilities and possess the requisite experience and levels of knowledge commensurate with their positions.',
    'tImplementationGuide' => 'Select a sample of 15 action plans and determine that target dates are set and responsible parties agreed to the plan.',
  ),
  246 => 
  array (
    'fkContext' => '10652',
    'fkSectionBestPractice' => '10651',
    'nControlType' => '1',
    'sName' => 'Responsibility for decisions is related to assignment of authority.',
    'tDescription' => 'Assignment of responsibility and delegation of authority to deal with organizational goals and objectives, operating functions, and regulatory requirements, including responsibility for information systems and authorization for changes.',
    'tImplementationGuide' => 'Determine whether a schedule of authority is easily accessible to employees, is updated and is properly approved.',
  ),
  247 => 
  array (
    'fkContext' => '10653',
    'fkSectionBestPractice' => '10651',
    'nControlType' => '1',
    'sName' => 'Delegated authority is appropriate in relation to assigned responsibilities.',
    'tDescription' => 'Assignment of responsibility and delegation of authority to deal with organizational goals and objectives, operating functions, and regulatory requirements, including responsibility for information systems and authorization for changes.',
    'tImplementationGuide' => 'Determine whether a schedule of authority is easily accessible to employees, is updated and is properly approved.


Proper approval of the schedule is basis for those included being properly authorized.',
  ),
  248 => 
  array (
    'fkContext' => '10654',
    'fkSectionBestPractice' => '10651',
    'nControlType' => '1',
    'sName' => 'Proper information is considered in determining the level of authority and scope of responsibility assigned to an individual.',
    'tDescription' => 'Assignment of responsibility and delegation of authority to deal with organizational goals and objectives, operating functions, and regulatory requirements, including responsibility for information systems and authorization for changes.',
    'tImplementationGuide' => 'Determine whether a schedule of authority is easily accessible to employees, is updated and is properly approved.


Proper approval of the schedule is basis for those included being properly authorized.',
  ),
  249 => 
  array (
    'fkContext' => '10655',
    'fkSectionBestPractice' => '10651',
    'nControlType' => '1',
    'sName' => 'Job descriptions contain specific references to control related responsibilities.',
    'tDescription' => 'Assignment of responsibility and delegation of authority to deal with organizational goals and objectives, operating functions, and regulatory requirements, including responsibility for information systems and authorization for changes.',
    'tImplementationGuide' => 'Select a sample of 10 job descriptions of employees within the accounting and finance function that have specific financial reporting control responsibilities and determine if the descriptions contain control-related responsibilities.',
  ),
  250 => 
  array (
    'fkContext' => '10657',
    'fkSectionBestPractice' => '10656',
    'nControlType' => '1',
    'sName' => 'Existence and implementation of codes of conduct and other policies regarding acceptable business practice, conflicts of interest, or expected standards of ethical and moral behaviour.',
    'tDescription' => 'Management demonstrates that integrity and ethical values cannot be compromised.',
    'tImplementationGuide' => 'Obtain the Code of Business Conduct and Ethics and examine it for inclusion of codes of conduct and other policies regarding acceptable business practice, conflicts of interest, or expected standards of ethical and moral behaviour.',
  ),
  251 => 
  array (
    'fkContext' => '10658',
    'fkSectionBestPractice' => '10656',
    'nControlType' => '1',
    'sName' => 'Establishment of \'tone at the top\', explicit moral guidance of what is right and wrong and extent of communication throughout the organization.',
    'tDescription' => 'Management demonstrates that integrity and ethical values cannot be compromised.',
    'tImplementationGuide' => '(1) Obtain the Code of Business Conduct and Ethics and examine it for establishment of the \'tone at the top\' - i.e., the Company\'s expectation of ethical and moral behaviour.  (2) Observe whether whistleblower posters and Alamosa posters addressing its core values are posted at office locations and stores (10 locations).  (3) Examine the intranet and determine if Alamosa\'s core values are displayed.  (4) Obtain and examine the criteria for the Champion\'s Club and determine if they are consistent with Alamosa\'s core values. ',
  ),
  252 => 
  array (
    'fkContext' => '10659',
    'fkSectionBestPractice' => '10656',
    'nControlType' => '1',
    'sName' => 'Dealings with employees, suppliers, customers, investors, creditors, insurers, competitors, auditors, etc., conducted on a high ethical plane.',
    'tDescription' => 'Management demonstrates that integrity and ethical values cannot be compromised.',
    'tImplementationGuide' => 'Obtain the Code of Business Conduct and Ethics and determine if it addresses dealings with employees, suppliers, customers, investors, creditors, insurers, competitors, auditors, etc., that should be conducted on a high ethical plane.',
  ),
  253 => 
  array (
    'fkContext' => '10660',
    'fkSectionBestPractice' => '10656',
    'nControlType' => '1',
    'sName' => 'Appropriateness of remedial action in the event of departure from procedures, policies, or code of conduct.',
    'tDescription' => 'Management demonstrates that integrity and ethical values cannot be compromised.',
    'tImplementationGuide' => 'Obtain the employee handbook and the sales compensation handbook and note whether the progressive discipline is included.  ',
  ),
  254 => 
  array (
    'fkContext' => '10661',
    'fkSectionBestPractice' => '10656',
    'nControlType' => '1',
    'sName' => 'Management intervention in overriding established controls is documented.  Deviations from established policies are investigated and documented.',
    'tDescription' => 'Management demonstrates that integrity and ethical values cannot be compromised.',
    'tImplementationGuide' => 'Obtain the Code of Business Conduct and Ethics & Employee Handbook and determine if specific instances of control override are provided for that are consistent with Alamosa\'s core values and good business practices.  Note whether management approval is required.',
  ),
  255 => 
  array (
    'fkContext' => '10662',
    'fkSectionBestPractice' => '10656',
    'nControlType' => '1',
    'sName' => 'Performance targets do not unnecessarily test people\'s ability to adhere to ethical values.',
    'tDescription' => 'Management demonstrates that integrity and ethical values cannot be compromised.',
    'tImplementationGuide' => '(1) Obtain the Code of Business Conduct and Ethics and examine it for establishment of the \'tone at the top\' - i.e., the Company\'s expectation of ethical and moral behaviour.  (2) Observe whether whistleblower posters and Alamosa posters addressing its core values are posted at office locations and stores (10 locations).  (3) Examine the intranet and determine if Alamosa\'s core values are displayed.  (4) Obtain and examine the criteria for the Champion\'s Club and determine if they are consistent with Alamosa\'s core values. 


Obtain the employee handbook and the sales compensation handbook and note whether the progressive discipline is included.  ',
  ),
  256 => 
  array (
    'fkContext' => '10663',
    'fkSectionBestPractice' => '10656',
    'nControlType' => '1',
    'sName' => 'Senior management has adopted a Code of Ethics',
    'tDescription' => 'Management demonstrates that integrity and ethical values cannot be compromised.',
    'tImplementationGuide' => 'Obtain the Code of Business Conduct and Ethics and determine if it 

addresses the following points:


(1) honest and ethical conduct, including the ethical handling of actual or apparent conflicts of interest between personal and professional relationships;

(2) full, fair, accurate, timely, and understandable disclosure in the periodic reports required to be filed by the Company;and

(3) compliance with applicable governmental rules and regulations.',
  ),
  257 => 
  array (
    'fkContext' => '10664',
    'fkSectionBestPractice' => '10656',
    'nControlType' => '1',
    'sName' => 'Methods of reporting violations of the Code of Business Conduct and Ethics are documented, and who the violations should be reported to.',
    'tDescription' => 'Management demonstrates that integrity and ethical values cannot be compromised.',
    'tImplementationGuide' => 'Obtain the Code of Business Conduct and Ethics and determine if it includes methods of reporting violations of the Code to appropriate individuals.',
  ),
  258 => 
  array (
    'fkContext' => '10665',
    'fkSectionBestPractice' => '10656',
    'nControlType' => '1',
    'sName' => 'Methods of reporting violations of the Code of Business Conduct and Ethics are documented, and who the violations should be reported to.',
    'tDescription' => 'Management demonstrates that integrity and ethical values cannot be compromised.',
    'tImplementationGuide' => 'Obtain the Code of Business Conduct and Ethics and determine if it includes methods of reporting violations of the Code to appropriate individuals.',
  ),
  259 => 
  array (
    'fkContext' => '10666',
    'fkSectionBestPractice' => '10656',
    'nControlType' => '1',
    'sName' => 'There is documentation of the acceptance of the Code of Business Conduct and Ethics.',
    'tDescription' => 'Management demonstrates that integrity and ethical values cannot be compromised.',
    'tImplementationGuide' => 'Select a sample of 25 employees and obtain the signed acknowledgement forms that they turned in to HR.',
  ),
  260 => 
  array (
    'fkContext' => '10667',
    'fkSectionBestPractice' => '10656',
    'nControlType' => '1',
    'sName' => 'Provision for corrective action related to violations of the Code of Business Conduct and Ethics is provided for.',
    'tDescription' => 'Management demonstrates that integrity and ethical values cannot be compromised.',
    'tImplementationGuide' => 'Obtain the Code of Business Conduct and Ethics and determine if provisions for corrective actions related to violations of the Code are provided for.',
  ),
  261 => 
  array (
    'fkContext' => '10668',
    'fkSectionBestPractice' => '10656',
    'nControlType' => '1',
    'sName' => 'An anti-fraud program and related controls exist.',
    'tDescription' => 'Management demonstrates that integrity and ethical values cannot be compromised.',
    'tImplementationGuide' => 'Obtain a copy of the MySafeWorkplace contract.  See other tests that address communication of Alamosa\'s values and position towards fraud.  Testing of segregation of duties is covered in the control matrices for the various cycles.',
  ),
  262 => 
  array (
    'fkContext' => '10670',
    'fkSectionBestPractice' => '10669',
    'nControlType' => '1',
    'sName' => 'Employee background checks are completed and considered to be adequate, particularly with regard to prior actions or activities considered unacceptable to the entity.',
    'tDescription' => 'Human resources is active in the control environment by ensuring that appropriate policies and practices are adopted, that qualified employees exhibiting integrity are hired, and that employees are properly supervised and trained.',
    'tImplementationGuide' => 'Select a sample of 15 employees hired during the current year and determine if background (if deemed necessary) were performed (if the position requires driving, ensure a drivers license search was done).',
  ),
  263 => 
  array (
    'fkContext' => '10671',
    'fkSectionBestPractice' => '10669',
    'nControlType' => '1',
    'sName' => 'Personnel policies and procedures result in recruiting and developing competent and trustworthy people necessary to support an effective internal control system.',
    'tDescription' => 'Human resources is active in the control environment by ensuring that appropriate policies and practices are adopted, that qualified employees exhibiting integrity are hired, and that employees are properly supervised and trained.',
    'tImplementationGuide' => 'Review the hiring guidlines and determine if appropriate language based on the expected control exists.',
  ),
  264 => 
  array (
    'fkContext' => '10672',
    'fkSectionBestPractice' => '10669',
    'nControlType' => '1',
    'sName' => 'Personnel policies and procedures result in training, promoting and compensating employees to support an effective internal control system.',
    'tDescription' => 'Human resources is active in the control environment by ensuring that appropriate policies and practices are adopted, that qualified employees exhibiting integrity are hired, and that employees are properly supervised and trained.',
    'tImplementationGuide' => 'No testing necessary.',
  ),
  265 => 
  array (
    'fkContext' => '10673',
    'fkSectionBestPractice' => '10669',
    'nControlType' => '1',
    'sName' => 'Employees are made aware of their responsibilities and management\'s expectations of them.',
    'tDescription' => 'Human resources is active in the control environment by ensuring that appropriate policies and practices are adopted, that qualified employees exhibiting integrity are hired, and that employees are properly supervised and trained.',
    'tImplementationGuide' => 'Select a sample of 25 employees and determine if job descriptions are available.  Obtain security access records for the folder where job descriptions are maintained and determine whether non-HR employees have write access.',
  ),
  266 => 
  array (
    'fkContext' => '10674',
    'fkSectionBestPractice' => '10669',
    'nControlType' => '1',
    'sName' => 'Integrity and ethical values are criteria in performance appraisals.',
    'tDescription' => 'Human resources is active in the control environment by ensuring that appropriate policies and practices are adopted, that qualified employees exhibiting integrity are hired, and that employees are properly supervised and trained.',
    'tImplementationGuide' => 'Obtain an evaluation form and determine if wording from existing control is on form.',
  ),
  267 => 
  array (
    'fkContext' => '10675',
    'fkSectionBestPractice' => '10669',
    'nControlType' => '1',
    'sName' => 'Employees complete orientation and compliance with orientation and completion of required modules is tracked by Human Resources.',
    'tDescription' => 'Employees are trained on ethical and compliance responsibilities and the importance of compliance with policies and procedures.',
    'tImplementationGuide' => 'Select a sample of 15 employees that were hired during the current year and determine if orientation forms were signed by the employees.',
  ),
  268 => 
  array (
    'fkContext' => '10677',
    'fkSectionBestPractice' => '10676',
    'nControlType' => '1',
    'sName' => 'Entity-wide objectives provide sufficiently broad statements and guidance of what the entity desires to achieve.',
    'tDescription' => 'For an entity to have effective control, it must have established objectives.',
    'tImplementationGuide' => 'Determine if strategic planning documents exist for recently held strategic meetings.',
  ),
  269 => 
  array (
    'fkContext' => '10678',
    'fkSectionBestPractice' => '10676',
    'nControlType' => '1',
    'sName' => 'Entity-wide objectives are communicated effectively to employees and Board of Directors.',
    'tDescription' => 'For an entity to have effective control, it must have established objectives.',
    'tImplementationGuide' => 'No testing necessary.',
  ),
  270 => 
  array (
    'fkContext' => '10679',
    'fkSectionBestPractice' => '10676',
    'nControlType' => '1',
    'sName' => 'Strategies are consistent with entity-wide objectives.',
    'tDescription' => 'For an entity to have effective control, it must have established objectives.',
    'tImplementationGuide' => 'No testing necessary.',
  ),
  271 => 
  array (
    'fkContext' => '10680',
    'fkSectionBestPractice' => '10676',
    'nControlType' => '1',
    'sName' => 'Business plans and budgets are consistent with entity-wide objectives.',
    'tDescription' => 'For an entity to have effective control, it must have established objectives.',
    'tImplementationGuide' => 'No testing necessary.',
  ),
  272 => 
  array (
    'fkContext' => '10682',
    'fkSectionBestPractice' => '10681',
    'nControlType' => '1',
    'sName' => 'Activity-level objectives are linked with entity-wide objectives and strategic plans.',
    'tDescription' => 'Objectives should be established for each significant activity and those activity-level objectives should be consistent.',
    'tImplementationGuide' => 'No testing necessary.',
  ),
  273 => 
  array (
    'fkContext' => '10683',
    'fkSectionBestPractice' => '10681',
    'nControlType' => '1',
    'sName' => 'Activity-level objectives are relevant to all significant business processes.',
    'tDescription' => 'Objectives should be established for each significant activity and those activity-level objectives should be consistent.',
    'tImplementationGuide' => 'No testing necessary.',
  ),
  274 => 
  array (
    'fkContext' => '10684',
    'fkSectionBestPractice' => '10681',
    'nControlType' => '1',
    'sName' => 'Resources are adequate relative to the objectives.',
    'tDescription' => 'Objectives should be established for each significant activity and those activity-level objectives should be consistent.',
    'tImplementationGuide' => 'Obtain the most recent budget and determine whether it was properly approved in accordance with the strategic planning process.',
  ),
  275 => 
  array (
    'fkContext' => '10685',
    'fkSectionBestPractice' => '10681',
    'nControlType' => '1',
    'sName' => 'Objectives (Critical Success factors) that are important to achievement of entity-wide objectives are identified.',
    'tDescription' => 'Objectives should be established for each significant activity and those activity-level objectives should be consistent.',
    'tImplementationGuide' => 'Obtain the strategic planning document and determine if organizational objectives were identified.',
  ),
  276 => 
  array (
    'fkContext' => '10686',
    'fkSectionBestPractice' => '10681',
    'nControlType' => '1',
    'sName' => 'All levels of management are involved in objective setting.',
    'tDescription' => 'Objectives should be established for each significant activity and those activity-level objectives should be consistent.',
    'tImplementationGuide' => 'No testing necessary.',
  ),
  277 => 
  array (
    'fkContext' => '10688',
    'fkSectionBestPractice' => '10687',
    'nControlType' => '1',
    'sName' => 'Mechanisms to identify risks arising from external and internal sources are adequate.',
    'tDescription' => 'The entity\'s risk assessment process should consider the implications of relevant risks at both the entity and activity level.',
    'tImplementationGuide' => 'Determine whether an approved (by the Audit Committee) internal audit plan exists and is based on a risk assessment.',
  ),
  278 => 
  array (
    'fkContext' => '10689',
    'fkSectionBestPractice' => '10687',
    'nControlType' => '1',
    'sName' => 'The risk analysis process, including assessing significance and likelihood and determining actions is thorough and relevant.',
    'tDescription' => 'The entity\'s risk assessment process should consider the implications of relevant risks at both the entity and activity level.',
    'tImplementationGuide' => 'No testing necessary.',
  ),
  279 => 
  array (
    'fkContext' => '10690',
    'fkSectionBestPractice' => '10687',
    'nControlType' => '1',
    'sName' => 'There is a systematic, documented approach to assessing the risk of fraud and related risks (fraud risk factors).',
    'tDescription' => 'The entity\'s risk assessment process should consider the implications of relevant risks at both the entity and activity level.',
    'tImplementationGuide' => 'N/A - documentation of control not yet in place.',
  ),
  280 => 
  array (
    'fkContext' => '10691',
    'fkSectionBestPractice' => '10687',
    'nControlType' => '1',
    'sName' => 'Mitigating controls are identified relating to identified fraud related risks.',
    'tDescription' => 'The entity\'s risk assessment process should consider the implications of relevant risks at both the entity and activity level.',
    'tImplementationGuide' => 'N/A - documentation of control not yet in place.',
  ),
  281 => 
  array (
    'fkContext' => '10692',
    'fkSectionBestPractice' => '10687',
    'nControlType' => '1',
    'sName' => 'The fraud risk assessment includes consideration of risks of management override, manipulation of estimates, accuracy and completeness of financial statement disclosures and MD&A.',
    'tDescription' => 'The entity\'s risk assessment process should consider the implications of relevant risks at both the entity and activity level.',
    'tImplementationGuide' => 'N/A - documentation of control not yet in place.',
  ),
  282 => 
  array (
    'fkContext' => '10693',
    'fkSectionBestPractice' => '10687',
    'nControlType' => '1',
    'sName' => 'Identified fraud and related issues are trended to identiy additional risk areas.',
    'tDescription' => 'The entity\'s risk assessment process should consider the implications of relevant risks at both the entity and activity level.',
    'tImplementationGuide' => 'Obtain as sample of 15 IS-generated reports that are used to identify areas of higher risk, and note whether documentation of management review exists.',
  ),
  283 => 
  array (
    'fkContext' => '10694',
    'fkSectionBestPractice' => '10687',
    'nControlType' => '1',
    'sName' => 'There is a systematic, documented approach to reviewing fraud and related items.',
    'tDescription' => 'The entity\'s risk assessment process should consider the implications of relevant risks at both the entity and activity level.',
    'tImplementationGuide' => 'Obtain the CFO\'s notes and determine whether communication about fraud review is documented (on a quarterly basis).',
  ),
  284 => 
  array (
    'fkContext' => '10696',
    'fkSectionBestPractice' => '10695',
    'nControlType' => '1',
    'sName' => 'Mechanisms exist to anticipate, identify, and react to routine events or activities that affect achievement of the entity or activity-level objectives.',
    'tDescription' => 'Mechanisms need to be in place to identify and react to changing conditions.',
    'tImplementationGuide' => 'Obtain a sample of 10 action plans used for individual goals.  Determine whether documentation of these goals was monitored.',
  ),
  285 => 
  array (
    'fkContext' => '10697',
    'fkSectionBestPractice' => '10695',
    'nControlType' => '1',
    'sName' => 'Mechanisms exist to identify and react to changes that may have a more dramatic effect of the entity and these are communicated to top management.',
    'tDescription' => 'Mechanisms need to be in place to identify and react to changing conditions.',
    'tImplementationGuide' => 'Obtain the notes kept by the CFO and determine whether matters pertaining to each executive\'s organization were discussed.',
  ),
  286 => 
  array (
    'fkContext' => '10699',
    'fkSectionBestPractice' => '10698',
    'nControlType' => '1',
    'sName' => 'Appropriate policies and procedures necessary, with respect to each of the entity\'s activities, exists.',
    'tDescription' => 'Appropriate policies and procedures necessary, with respect to each of the entity\'s activities, exists and are being applied properly.',
    'tImplementationGuide' => 'This is covered in each of the cycles covered by 404 testing - pass further testing here.',
  ),
  287 => 
  array (
    'fkContext' => '10700',
    'fkSectionBestPractice' => '10698',
    'nControlType' => '1',
    'sName' => 'Identified control activities in place are being applied properly.',
    'tDescription' => 'Appropriate policies and procedures necessary, with respect to each of the entity\'s activities, exists and are being applied properly.',
    'tImplementationGuide' => 'This is covered in each of the cycles covered by 404 testing - pass further testing here.',
  ),
  288 => 
  array (
    'fkContext' => '10701',
    'fkSectionBestPractice' => '10698',
    'nControlType' => '1',
    'sName' => 'Investigative protocols are in place regarding following up on fraud and related issues raised.',
    'tDescription' => 'Appropriate policies and procedures necessary, with respect to each of the entity\'s activities, exists and are being applied properly.',
    'tImplementationGuide' => 'N/A - documentation of control not yet in place.',
  ),
  289 => 
  array (
    'fkContext' => '10703',
    'fkSectionBestPractice' => '10702',
    'nControlType' => '1',
    'sName' => 'Mechanisms are in place to obtain relevant external information-on market conditions, competitors\' programs, legislative or regulatory developments, and economic changes.',
    'tDescription' => 'An effective information infrastructure is in place.',
    'tImplementationGuide' => 'No testing necessary.',
  ),
  290 => 
  array (
    'fkContext' => '10704',
    'fkSectionBestPractice' => '10702',
    'nControlType' => '1',
    'sName' => 'Internally generated information, critical to the achievement of the business objectives, is identified and regularly reported.',
    'tDescription' => 'An effective information infrastructure is in place.',
    'tImplementationGuide' => 'These various reports and the controls over the data obtained are tested in the respective 404 cycles.  Pass further testing here.',
  ),
  291 => 
  array (
    'fkContext' => '10705',
    'fkSectionBestPractice' => '10702',
    'nControlType' => '1',
    'sName' => 'Managers receive analytical information that enables them to identify what action needs to be taken.',
    'tDescription' => 'An effective information infrastructure is in place.',
    'tImplementationGuide' => 'These various reports and the controls over the data obtained are tested in the respective 404 cycles.  Pass further testing here.',
  ),
  292 => 
  array (
    'fkContext' => '10706',
    'fkSectionBestPractice' => '10702',
    'nControlType' => '1',
    'sName' => 'Information is provided at the right level of detail for different levels of management.',
    'tDescription' => 'An effective information infrastructure is in place.',
    'tImplementationGuide' => 'These various reports and the controls over the data obtained are tested in the respective 404 cycles.  Pass further testing here.',
  ),
  293 => 
  array (
    'fkContext' => '10707',
    'fkSectionBestPractice' => '10702',
    'nControlType' => '1',
    'sName' => 'Information is available on a timely basis to allow effective monitoring of events and activities.',
    'tDescription' => 'An effective information infrastructure is in place.',
    'tImplementationGuide' => 'These various reports and the controls over the data obtained are tested in the respective 404 cycles.  Pass further testing here.',
  ),
  294 => 
  array (
    'fkContext' => '10708',
    'fkSectionBestPractice' => '10702',
    'nControlType' => '1',
    'sName' => 'Executives with sufficiently broad responsibilities determine information needs and priorities.',
    'tDescription' => 'An effective information infrastructure is in place.',
    'tImplementationGuide' => 'Determine which surveys were performed during the current year and select an appropriate sample to determine whether the objectives of the surveys were met.',
  ),
  295 => 
  array (
    'fkContext' => '10709',
    'fkSectionBestPractice' => '10702',
    'nControlType' => '1',
    'sName' => 'Sufficient resources are provided to develop/enhance information systems.',
    'tDescription' => 'An effective information infrastructure is in place.',
    'tImplementationGuide' => 'Obtain the budgets and business plan and determine whether appropriate consideration of IS needs exists.',
  ),
  296 => 
  array (
    'fkContext' => '10711',
    'fkSectionBestPractice' => '10710',
    'nControlType' => '1',
    'sName' => 'Communication vehicles are sufficient in effecting communications.',
    'tDescription' => 'Appropriate information is communicated in a timely and effective manner.',
    'tImplementationGuide' => 'No testing necessary.',
  ),
  297 => 
  array (
    'fkContext' => '10712',
    'fkSectionBestPractice' => '10710',
    'nControlType' => '1',
    'sName' => 'Employees know the objectives of their own activities.',
    'tDescription' => 'Appropriate information is communicated in a timely and effective manner.',
    'tImplementationGuide' => 'From the sample of evaluations obtained in \'Evidence exists indicating that employees have the requisite knowledge and skills\' determine whether individual goals were addressed.',
  ),
  298 => 
  array (
    'fkContext' => '10713',
    'fkSectionBestPractice' => '10710',
    'nControlType' => '1',
    'sName' => 'There is a channel to communicate upstream through someone other than a direct superior.',
    'tDescription' => 'Appropriate information is communicated in a timely and effective manner.',
    'tImplementationGuide' => '(1) Obtain the Code of Business Conduct and Ethics and examine it for establishment of the \'tone at the top\' - i.e., the Company\'s expectation of ethical and moral behaviour.  (2) Observe whether whistleblower posters and Alamosa posters addressing its core values are posted at office locations and stores (10 locations).  (3) Examine the intranet and determine if Alamosa\'s core values are displayed.  (4) Obtain and examine the criteria for the Champion\'s Club and determine if they are consistent with Alamosa\'s core values. ',
  ),
  299 => 
  array (
    'fkContext' => '10714',
    'fkSectionBestPractice' => '10710',
    'nControlType' => '1',
    'sName' => 'People who report suspected improprieties are provided feedback and have immunity from reprisal.',
    'tDescription' => 'Appropriate information is communicated in a timely and effective manner.',
    'tImplementationGuide' => 'No testing necessary.',
  ),
  300 => 
  array (
    'fkContext' => '10715',
    'fkSectionBestPractice' => '10710',
    'nControlType' => '1',
    'sName' => 'Information is communicated across the organization completely and in a timely manner to enable people to discharge their responsibilities.',
    'tDescription' => 'Appropriate information is communicated in a timely and effective manner.',
    'tImplementationGuide' => 'No testing necessary.',
  ),
  301 => 
  array (
    'fkContext' => '10716',
    'fkSectionBestPractice' => '10710',
    'nControlType' => '1',
    'sName' => 'Channels with customers, suppliers, and other external parties for communicating changing needs are open and effective.',
    'tDescription' => 'Appropriate information is communicated in a timely and effective manner.',
    'tImplementationGuide' => 'No testing necessary.',
  ),
  302 => 
  array (
    'fkContext' => '10717',
    'fkSectionBestPractice' => '10710',
    'nControlType' => '1',
    'sName' => 'Outside parties have been made aware of the entity\'s ethical standards.',
    'tDescription' => 'Appropriate information is communicated in a timely and effective manner.',
    'tImplementationGuide' => 'View the referenced web page and determine if the values are listed there and the page is easily accessible from the home page.',
  ),
  303 => 
  array (
    'fkContext' => '10718',
    'fkSectionBestPractice' => '10710',
    'nControlType' => '1',
    'sName' => 'Improprieties by employees or external parties are reported to the appropriate personnel.',
    'tDescription' => 'Appropriate information is communicated in a timely and effective manner.',
    'tImplementationGuide' => 'View the online employee handbook and determine whether there is documentation of how improprieties are to be addressed.',
  ),
  304 => 
  array (
    'fkContext' => '10719',
    'fkSectionBestPractice' => '10710',
    'nControlType' => '1',
    'sName' => 'Communications received from customers are followed up appropriately and in a timely manner.',
    'tDescription' => 'Appropriate information is communicated in a timely and effective manner.',
    'tImplementationGuide' => 'No testing necessary.',
  ),
  305 => 
  array (
    'fkContext' => '10720',
    'fkSectionBestPractice' => '10710',
    'nControlType' => '1',
    'sName' => 'Communications received from regulators or other external parties are followed up appropriately and in a timely manner.',
    'tDescription' => 'Appropriate information is communicated in a timely and effective manner.',
    'tImplementationGuide' => 'N/A - documentation of control not yet in place.',
  ),
  306 => 
  array (
    'fkContext' => '10721',
    'fkSectionBestPractice' => '10710',
    'nControlType' => '1',
    'sName' => 'Errors in customer billings are corrected and the error is investigated and corrected.',
    'tDescription' => 'Appropriate information is communicated in a timely and effective manner.',
    'tImplementationGuide' => 'The controlled portion of adjustments is insignificant, therefore pass testing in this area.',
  ),
  307 => 
  array (
    'fkContext' => '10722',
    'fkSectionBestPractice' => '10710',
    'nControlType' => '1',
    'sName' => 'Appropriate personnel, independent of those involved with the original transaction, process complaints.',
    'tDescription' => 'Appropriate information is communicated in a timely and effective manner.',
    'tImplementationGuide' => 'Obtain the organization chart to determine if these two areas are properly segregated.',
  ),
  308 => 
  array (
    'fkContext' => '10723',
    'fkSectionBestPractice' => '10710',
    'nControlType' => '1',
    'sName' => 'Importance of being aware of fraud and related issues, and the Company\'s views on these issues is effectively communicated.',
    'tDescription' => 'Appropriate information is communicated in a timely and effective manner.',
    'tImplementationGuide' => 'Obtain a copy of the MySafeWorkplace contract.  See other tests that address communication of Alamosa\'s values and position towards fraud.  Testing of segregation of duties is covered in the control matrices for the various cycles.',
  ),
  309 => 
  array (
    'fkContext' => '10724',
    'fkSectionBestPractice' => '10710',
    'nControlType' => '1',
    'sName' => 'A procedure is in place to ensure that fraud and related issues are escalated to the Audit Committee.',
    'tDescription' => 'Appropriate information is communicated in a timely and effective manner.',
    'tImplementationGuide' => 'Obtain notes taken by the Manager of Internal Audit at the meeting to determine if fraud issues were addressed.',
  ),
  310 => 
  array (
    'fkContext' => '10726',
    'fkSectionBestPractice' => '10725',
    'nControlType' => '1',
    'sName' => 'Personnel, in carrying out their regular activities, obtain evidence as to whether the system of internal control continues to function.',
    'tDescription' => 'There is ongoing monitoring of the appropriateness, completeness and effectiveness of the entity\'s control environment.',
    'tImplementationGuide' => 'This is covered in each of the cycles covered by 404 testing - pass further testing here.',
  ),
  311 => 
  array (
    'fkContext' => '10727',
    'fkSectionBestPractice' => '10725',
    'nControlType' => '1',
    'sName' => 'Communications from external parties corroborate internally generated information.',
    'tDescription' => 'There is ongoing monitoring of the appropriateness, completeness and effectiveness of the entity\'s control environment.',
    'tImplementationGuide' => 'This is covered in relevant cycles covered by 404 testing - pass further testing here.',
  ),
  312 => 
  array (
    'fkContext' => '10728',
    'fkSectionBestPractice' => '10725',
    'nControlType' => '1',
    'sName' => 'Customers generally pay invoices or customer complaints on billings are investigated for their underlying causes.',
    'tDescription' => 'There is ongoing monitoring of the appropriateness, completeness and effectiveness of the entity\'s control environment.',
    'tImplementationGuide' => 'No testing necessary.',
  ),
  313 => 
  array (
    'fkContext' => '10729',
    'fkSectionBestPractice' => '10725',
    'nControlType' => '1',
    'sName' => 'Supplier complaints regarding unfair practices by purchasing agents are fully investigated.',
    'tDescription' => 'There is ongoing monitoring of the appropriateness, completeness and effectiveness of the entity\'s control environment.',
    'tImplementationGuide' => 'Obtain the P&P prepared by the purchasing department, and determine whether it appropriately addresses supplier complaints.',
  ),
  314 => 
  array (
    'fkContext' => '10730',
    'fkSectionBestPractice' => '10725',
    'nControlType' => '1',
    'sName' => 'Amounts recorded by the accounting system are periodically compared to physical assets.',
    'tDescription' => 'There is ongoing monitoring of the appropriateness, completeness and effectiveness of the entity\'s control environment.',
    'tImplementationGuide' => 'Observe the tracking system and obtain a basic understanding.  Describe results.  Obtain copies of inventory reports from the warehouse (sample of 5) to determine that assets are properly recorded & tracked.


Obtain system documentation showing the locations where identification cards are in use.',
  ),
  315 => 
  array (
    'fkContext' => '10731',
    'fkSectionBestPractice' => '10725',
    'nControlType' => '1',
    'sName' => 'Executives with proper authority decide on which auditor recommendations regarding means to strengthen internal controls are implemented.',
    'tDescription' => 'There is ongoing monitoring of the appropriateness, completeness and effectiveness of the entity\'s control environment.',
    'tImplementationGuide' => 'No testing necessary.',
  ),
  316 => 
  array (
    'fkContext' => '10732',
    'fkSectionBestPractice' => '10725',
    'nControlType' => '1',
    'sName' => 'Personnel are required periodically to acknowledge compliance with the code of conduct.',
    'tDescription' => 'There is ongoing monitoring of the appropriateness, completeness and effectiveness of the entity\'s control environment.',
    'tImplementationGuide' => 'Select a sample of 25 employees and obtain the signed acknowledgement forms that they turned in to HR.',
  ),
  317 => 
  array (
    'fkContext' => '10733',
    'fkSectionBestPractice' => '10725',
    'nControlType' => '1',
    'sName' => 'Internal audit has an appropriate position within the organization.',
    'tDescription' => 'There is ongoing monitoring of the appropriateness, completeness and effectiveness of the entity\'s control environment.',
    'tImplementationGuide' => 'Obtain the Internal Audit Charter Statement and determine whether the appropriate wording is included.',
  ),
  318 => 
  array (
    'fkContext' => '10734',
    'fkSectionBestPractice' => '10725',
    'nControlType' => '1',
    'sName' => 'Staff members are experienced and competent.',
    'tDescription' => 'There is ongoing monitoring of the appropriateness, completeness and effectiveness of the entity\'s control environment.',
    'tImplementationGuide' => 'No testing necessary.',
  ),
  319 => 
  array (
    'fkContext' => '10735',
    'fkSectionBestPractice' => '10725',
    'nControlType' => '1',
    'sName' => 'Internal audit has access to the Audit Committee.',
    'tDescription' => 'There is ongoing monitoring of the appropriateness, completeness and effectiveness of the entity\'s control environment.',
    'tImplementationGuide' => 'Obtain the Internal Audit Charter Statement and determine whether the appropriate wording is included.',
  ),
  320 => 
  array (
    'fkContext' => '10736',
    'fkSectionBestPractice' => '10725',
    'nControlType' => '1',
    'sName' => 'Operating personnel are required to \'sign-off\' on the accuracy of their units\' financial information and are held responsible for errors discovered.',
    'tDescription' => 'There is ongoing monitoring of the appropriateness, completeness and effectiveness of the entity\'s control environment.',
    'tImplementationGuide' => 'Select two ICQs (Internal Control Questionary) completed each quarter and determine if proper signoff by the appropriate process owners was obtained.',
  ),
  321 => 
  array (
    'fkContext' => '10737',
    'fkSectionBestPractice' => '10725',
    'nControlType' => '1',
    'sName' => 'Signatures are required to evidence performance of specific control activities/functions.',
    'tDescription' => 'There is ongoing monitoring of the appropriateness, completeness and effectiveness of the entity\'s control environment.',
    'tImplementationGuide' => 'Select two ICQs (Internal Control Questionary) completed each quarter and determine if proper signoff by the appropriate process owners was obtained.',
  ),
  322 => 
  array (
    'fkContext' => '10739',
    'fkSectionBestPractice' => '10738',
    'nControlType' => '1',
    'sName' => 'Appropriate portions of the internal control systems are evaluated.',
    'tDescription' => 'There is effective and objective evaluation of the appropriateness, completeness and effectiveness of the entity\'s control environment.',
    'tImplementationGuide' => 'This is being evaluated as part of the 404 testing therefore pass testing here.',
  ),
  323 => 
  array (
    'fkContext' => '10740',
    'fkSectionBestPractice' => '10738',
    'nControlType' => '1',
    'sName' => 'The evaluation was conducted by personnel with the requisite skills.',
    'tDescription' => 'There is effective and objective evaluation of the appropriateness, completeness and effectiveness of the entity\'s control environment.',
    'tImplementationGuide' => 'This is being evaluated as part of the 404 testing therefore pass testing here.',
  ),
  324 => 
  array (
    'fkContext' => '10741',
    'fkSectionBestPractice' => '10738',
    'nControlType' => '1',
    'sName' => 'The evaluation process is appropriate.',
    'tDescription' => 'There is effective and objective evaluation of the appropriateness, completeness and effectiveness of the entity\'s control environment.',
    'tImplementationGuide' => 'This is being evaluated as part of the 404 testing therefore pass testing here.',
  ),
  325 => 
  array (
    'fkContext' => '10742',
    'fkSectionBestPractice' => '10738',
    'nControlType' => '1',
    'sName' => 'An understanding is gained of how the system is supposed to work and how it actually does work.',
    'tDescription' => 'There is effective and objective evaluation of the appropriateness, completeness and effectiveness of the entity\'s control environment.',
    'tImplementationGuide' => 'This is being evaluated as part of the 404 testing therefore pass testing here.',
  ),
  326 => 
  array (
    'fkContext' => '10743',
    'fkSectionBestPractice' => '10738',
    'nControlType' => '1',
    'sName' => 'The methodology for evaluating the system is logical and appropriate.',
    'tDescription' => 'There is effective and objective evaluation of the appropriateness, completeness and effectiveness of the entity\'s control environment.',
    'tImplementationGuide' => 'This is being evaluated as part of the 404 testing therefore pass testing here.',
  ),
  327 => 
  array (
    'fkContext' => '10744',
    'fkSectionBestPractice' => '10738',
    'nControlType' => '1',
    'sName' => 'The evaluation is adequately planned and coordinated.',
    'tDescription' => 'There is effective and objective evaluation of the appropriateness, completeness and effectiveness of the entity\'s control environment.',
    'tImplementationGuide' => '',
  ),
  328 => 
  array (
    'fkContext' => '10745',
    'fkSectionBestPractice' => '10738',
    'nControlType' => '1',
    'sName' => 'Documentation is available and appropriate.',
    'tDescription' => 'There is effective and objective evaluation of the appropriateness, completeness and effectiveness of the entity\'s control environment.',
    'tImplementationGuide' => 'This is being evaluated as part of the 404 testing therefore pass testing here.',
  ),
  329 => 
  array (
    'fkContext' => '10747',
    'fkSectionBestPractice' => '10746',
    'nControlType' => '1',
    'sName' => 'A mechanism exists for capturing and reporting identified internal control weaknesses.',
    'tDescription' => 'Noted deficiencies are appropriately and timely reported, and properly followed up.',
    'tImplementationGuide' => 'Obtain findings status reports presented to the Audit Committee for two quarters.  Also obtain and review the findings database for evidence that the status of findings is being monitored.',
  ),
  330 => 
  array (
    'fkContext' => '10748',
    'fkSectionBestPractice' => '10746',
    'nControlType' => '1',
    'sName' => 'Deficiencies are reported to a person directly responsible for the activity and a person at least one level higher.',
    'tDescription' => 'Noted deficiencies are appropriately and timely reported, and properly followed up.',
    'tImplementationGuide' => 'Obtain findings status reports presented to the Audit Committee for two quarters.  Also obtain and review the findings database for evidence that the status of findings is being monitored.


Additionally, note whether those responsible for ensuring the corrective actions are taken are listed and whether there is an action plan included.',
  ),
  331 => 
  array (
    'fkContext' => '10749',
    'fkSectionBestPractice' => '10746',
    'nControlType' => '1',
    'sName' => 'Follow-up actions are appropriate and ensure necessary action is taken.',
    'tDescription' => 'Noted deficiencies are appropriately and timely reported, and properly followed up.',
    'tImplementationGuide' => 'Obtain findings status reports presented to the Audit Committee for two quarters.  Also obtain and review the findings database for evidence that the status of findings is being monitored.


Additionally, note whether follow actions are properly.',
  ),
  332 => 
  array (
    'fkContext' => '10750',
    'fkSectionBestPractice' => '10746',
    'nControlType' => '1',
    'sName' => 'Any fraud, whether or not material, that involves management or other employees who have a significant role in the Company’s internal controls or financial reporting is reported to the Audit Committee.',
    'tDescription' => 'Noted deficiencies are appropriately and timely reported, and properly followed up.',
    'tImplementationGuide' => 'Obtain the presentation materials presented at the Audit Committee meeting and the minutes.  Determine whether fraud concerning management or other employees who have a significant role in the Company’s internal controls or financial reporting is reported to the Audit Committee - whether or not any has occurred.',
  ),
  333 => 
  array (
    'fkContext' => '10751',
    'fkSectionBestPractice' => '10746',
    'nControlType' => '1',
    'sName' => 'Any fraud that is significant to the recording of transactions or accounting policies, significant to internal controls, has a significant effect on the company\'s Company\'s financial statements is reported to the Audit Committee.',
    'tDescription' => 'Noted deficiencies are appropriately and timely reported, and properly followed up.gnificant to the Company or its operations, significant to the recording of transactions or accounting policies, significant to internal controls, is significant to audit-related',
    'tImplementationGuide' => 'Obtain the presentation materials presented at the Audit Committee meeting and the minutes.  Determine whether fraud concerning management or other employees who have a significant role in the Company’s internal controls or financial reporting is reported to the Audit Committee - whether or not any has occurred.


Determine whether this is reported on - whether or not any has occurred.',
  ),
  334 => 
  array (
    'fkContext' => '10752',
    'fkSectionBestPractice' => '10746',
    'nControlType' => '1',
    'sName' => 'Reporting of suspected ethical, fraud, and/or compliance issues on an anonymous basis is available.',
    'tDescription' => 'Noted deficiencies are appropriately and timely reported, and properly followed up.',
    'tImplementationGuide' => '(1) Obtain the Code of Business Conduct and Ethics and examine it for establishment of the \'tone at the top\' - i.e., the Company\'s expectation of ethical and moral behaviour.  (2) Observe whether whistleblower posters and Alamosa posters addressing its core values are posted at office locations and stores (10 locations).  (3) Examine the intranet and determine if Alamosa\'s core values are displayed.  (4) Obtain and examine the criteria for the Champion\'s Club and determine if they are consistent with Alamosa\'s core values. ',
  ),
)
?>