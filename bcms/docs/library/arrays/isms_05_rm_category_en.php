<?php

$laCategoryEN = 
array (
  0 => 
  array (
    'fkContext' => '100',
    'fkParent' => 'null',
    'sName' => 'Information',
    'tDescription' => 'Information',
  ),
  1 => 
  array (
    'fkContext' => '101',
    'fkParent' => '100',
    'sName' => 'Digital Information',
    'tDescription' => 'Digital Information',
  ),
  2 => 
  array (
    'fkContext' => '102',
    'fkParent' => '101',
    'sName' => 'Personal (Client)',
    'tDescription' => 'Personal (Client)',
  ),
  3 => 
  array (
    'fkContext' => '103',
    'fkParent' => '101',
    'sName' => 'Personal (Employees)',
    'tDescription' => 'Personal (Employees)',
  ),
  4 => 
  array (
    'fkContext' => '104',
    'fkParent' => '101',
    'sName' => 'Financial',
    'tDescription' => 'Financial',
  ),
  5 => 
  array (
    'fkContext' => '105',
    'fkParent' => '101',
    'sName' => 'Legal',
    'tDescription' => 'Legal',
  ),
  6 => 
  array (
    'fkContext' => '106',
    'fkParent' => '101',
    'sName' => 'Research',
    'tDescription' => 'Research',
  ),
  7 => 
  array (
    'fkContext' => '107',
    'fkParent' => '101',
    'sName' => 'Strategy and Business',
    'tDescription' => 'Strategy and Business',
  ),
  8 => 
  array (
    'fkContext' => '108',
    'fkParent' => '101',
    'sName' => 'Database',
    'tDescription' => 'Database',
  ),
  9 => 
  array (
    'fkContext' => '109',
    'fkParent' => '101',
    'sName' => 'Electronic Mail',
    'tDescription' => 'Electronic Mail',
  ),
  10 => 
  array (
    'fkContext' => '110',
    'fkParent' => '101',
    'sName' => 'Record',
    'tDescription' => 'Record',
  ),
  11 => 
  array (
    'fkContext' => '111',
    'fkParent' => '101',
    'sName' => 'Manual Documentation',
    'tDescription' => 'Manual Documentation',
  ),
  12 => 
  array (
    'fkContext' => '113',
    'fkParent' => '101',
    'sName' => 'E-Commerce Data',
    'tDescription' => 'E-Commerce Data',
  ),
  13 => 
  array (
    'fkContext' => '114',
    'fkParent' => '101',
    'sName' => 'Backup Information',
    'tDescription' => 'Backup Information',
  ),
  14 => 
  array (
    'fkContext' => '115',
    'fkParent' => '100',
    'sName' => 'Paper Documents',
    'tDescription' => 'Paper Documents',
  ),
  15 => 
  array (
    'fkContext' => '116',
    'fkParent' => '115',
    'sName' => 'Personal (Client)',
    'tDescription' => 'Personal (Client)',
  ),
  16 => 
  array (
    'fkContext' => '117',
    'fkParent' => '115',
    'sName' => 'Personal (Employees)',
    'tDescription' => 'Personal (Employees)',
  ),
  17 => 
  array (
    'fkContext' => '118',
    'fkParent' => '115',
    'sName' => 'Financial',
    'tDescription' => 'Financial',
  ),
  18 => 
  array (
    'fkContext' => '119',
    'fkParent' => '115',
    'sName' => 'Legal',
    'tDescription' => 'Legal',
  ),
  19 => 
  array (
    'fkContext' => '120',
    'fkParent' => '115',
    'sName' => 'Research',
    'tDescription' => 'Research',
  ),
  20 => 
  array (
    'fkContext' => '121',
    'fkParent' => '115',
    'sName' => 'Strategy and Business',
    'tDescription' => 'Strategy and Business',
  ),
  21 => 
  array (
    'fkContext' => '123',
    'fkParent' => '115',
    'sName' => 'Mail',
    'tDescription' => 'Mail',
  ),
  22 => 
  array (
    'fkContext' => '124',
    'fkParent' => '115',
    'sName' => 'Record',
    'tDescription' => 'Record',
  ),
  23 => 
  array (
    'fkContext' => '125',
    'fkParent' => '115',
    'sName' => 'Manual Documentation',
    'tDescription' => 'Manual Documention',
  ),
  24 => 
  array (
    'fkContext' => '126',
    'fkParent' => '115',
    'sName' => 'Commercial',
    'tDescription' => 'Commercial',
  ),
  25 => 
  array (
    'fkContext' => '127',
    'fkParent' => '115',
    'sName' => 'FAX',
    'tDescription' => 'FAX',
  ),
  26 => 
  array (
    'fkContext' => '128',
    'fkParent' => 'null',
    'sName' => 'Software Assets',
    'tDescription' => 'Software Assets',
  ),
  27 => 
  array (
    'fkContext' => '129',
    'fkParent' => '128',
    'sName' => 'Operating Systems',
    'tDescription' => 'Operating Systems',
  ),
  28 => 
  array (
    'fkContext' => '130',
    'fkParent' => '129',
    'sName' => 'Windows (Desktop)',
    'tDescription' => 'Windows (Desktop)',
  ),
  29 => 
  array (
    'fkContext' => '131',
    'fkParent' => '129',
    'sName' => 'Windows (Server)',
    'tDescription' => 'Windows (Server)',
  ),
  30 => 
  array (
    'fkContext' => '132',
    'fkParent' => '129',
    'sName' => 'Windows Mobile ',
    'tDescription' => 'Windows Mobile ',
  ),
  31 => 
  array (
    'fkContext' => '133',
    'fkParent' => '129',
    'sName' => 'MacOS',
    'tDescription' => 'MacOS',
  ),
  32 => 
  array (
    'fkContext' => '134',
    'fkParent' => '129',
    'sName' => 'Linux (Desktop)',
    'tDescription' => 'Linux (Desktop)',
  ),
  33 => 
  array (
    'fkContext' => '135',
    'fkParent' => '129',
    'sName' => 'Linux (Server)',
    'tDescription' => 'Linux (Server)',
  ),
  34 => 
  array (
    'fkContext' => '136',
    'fkParent' => '129',
    'sName' => 'Unix',
    'tDescription' => 'Unix',
  ),
  35 => 
  array (
    'fkContext' => '137',
    'fkParent' => '129',
    'sName' => 'Solaris',
    'tDescription' => 'Solaris',
  ),
  36 => 
  array (
    'fkContext' => '138',
    'fkParent' => '128',
    'sName' => 'Information Systems',
    'tDescription' => 'Information Systems',
  ),
  37 => 
  array (
    'fkContext' => '139',
    'fkParent' => '138',
    'sName' => 'Database',
    'tDescription' => 'Database',
  ),
  38 => 
  array (
    'fkContext' => '140',
    'fkParent' => '139',
    'sName' => 'Oracle',
    'tDescription' => 'Oracle',
  ),
  39 => 
  array (
    'fkContext' => '141',
    'fkParent' => '139',
    'sName' => 'Postgre',
    'tDescription' => 'Postgre',
  ),
  40 => 
  array (
    'fkContext' => '142',
    'fkParent' => '139',
    'sName' => 'MySQL',
    'tDescription' => 'MySQL',
  ),
  41 => 
  array (
    'fkContext' => '143',
    'fkParent' => '139',
    'sName' => 'DB2',
    'tDescription' => 'DB2',
  ),
  42 => 
  array (
    'fkContext' => '144',
    'fkParent' => '139',
    'sName' => 'Data Warehouse',
    'tDescription' => 'Data Warehouse',
  ),
  43 => 
  array (
    'fkContext' => '152',
    'fkParent' => '138',
    'sName' => 'Collaboration Systems',
    'tDescription' => 'Collaboration Systems',
  ),
  44 => 
  array (
    'fkContext' => '153',
    'fkParent' => '152',
    'sName' => 'Lotus Notes',
    'tDescription' => 'Lotus Notes',
  ),
  45 => 
  array (
    'fkContext' => '154',
    'fkParent' => '152',
    'sName' => 'Microsoft Exchange',
    'tDescription' => 'Microsoft Exchange',
  ),
  46 => 
  array (
    'fkContext' => '155',
    'fkParent' => '152',
    'sName' => 'Corporate WIKI',
    'tDescription' => 'Corporate WIKI',
  ),
  47 => 
  array (
    'fkContext' => '156',
    'fkParent' => '152',
    'sName' => 'Portal Collaboration',
    'tDescription' => 'Portal Collaboration',
  ),
  48 => 
  array (
    'fkContext' => '157',
    'fkParent' => '138',
    'sName' => 'Corporate System',
    'tDescription' => 'Corporate System',
  ),
  49 => 
  array (
    'fkContext' => '158',
    'fkParent' => '157',
    'sName' => 'ERP (Enterprise Resource Planning)',
    'tDescription' => 'ERP (Enterprise Resource Planning)',
  ),
  50 => 
  array (
    'fkContext' => '159',
    'fkParent' => '157',
    'sName' => 'CRM (Costumer Relationship Management)',
    'tDescription' => 'CRM (Costumer Relationship Management)',
  ),
  51 => 
  array (
    'fkContext' => '160',
    'fkParent' => '157',
    'sName' => 'E-learning',
    'tDescription' => 'E-learning',
  ),
  52 => 
  array (
    'fkContext' => '161',
    'fkParent' => '157',
    'sName' => 'DMS (Document Management System)',
    'tDescription' => 'DMS (Document Management System)',
  ),
  53 => 
  array (
    'fkContext' => '162',
    'fkParent' => '157',
    'sName' => 'BI (Business Intelligence)',
    'tDescription' => 'BI (Business Intelligence)',
  ),
  54 => 
  array (
    'fkContext' => '167',
    'fkParent' => '138',
    'sName' => 'Names Resolution Software  (DNS, WINS e etc)',
    'tDescription' => 'Names Resolution Software  (DNS, WINS e etc)',
  ),
  55 => 
  array (
    'fkContext' => '168',
    'fkParent' => '138',
    'sName' => 'Security Software',
    'tDescription' => 'Security Software',
  ),
  56 => 
  array (
    'fkContext' => '169',
    'fkParent' => '168',
    'sName' => 'Anti-virus',
    'tDescription' => 'Anti-virus',
  ),
  57 => 
  array (
    'fkContext' => '170',
    'fkParent' => '168',
    'sName' => 'Firewall',
    'tDescription' => 'Firewall',
  ),
  58 => 
  array (
    'fkContext' => '171',
    'fkParent' => '168',
    'sName' => 'IDS (Intrusion Detection System)',
    'tDescription' => 'IDS (Intrusion Detection System)',
  ),
  59 => 
  array (
    'fkContext' => '172',
    'fkParent' => '168',
    'sName' => 'IPS (Intrusion Prevention System)',
    'tDescription' => 'IPS (Intrusion Prevention System)',
  ),
  60 => 
  array (
    'fkContext' => '173',
    'fkParent' => '138',
    'sName' => 'Communication Software',
    'tDescription' => 'Communication Software',
  ),
  61 => 
  array (
    'fkContext' => '174',
    'fkParent' => '173',
    'sName' => 'Telephone Exchange Software ',
    'tDescription' => 'Telephone Exchange Software ',
  ),
  62 => 
  array (
    'fkContext' => '175',
    'fkParent' => '173',
    'sName' => 'Central VoIP',
    'tDescription' => 'Central VoIP',
  ),
  63 => 
  array (
    'fkContext' => '176',
    'fkParent' => '173',
    'sName' => 'Instant Messaging',
    'tDescription' => 'Instant Messaging',
  ),
  64 => 
  array (
    'fkContext' => '177',
    'fkParent' => '138',
    'sName' => 'Web Systems',
    'tDescription' => 'Web Systems',
  ),
  65 => 
  array (
    'fkContext' => '178',
    'fkParent' => '177',
    'sName' => 'Intranet',
    'tDescription' => 'Intranet',
  ),
  66 => 
  array (
    'fkContext' => '179',
    'fkParent' => '177',
    'sName' => 'Webmail',
    'tDescription' => 'Webmail',
  ),
  67 => 
  array (
    'fkContext' => '180',
    'fkParent' => '138',
    'sName' => 'File Sharing Software',
    'tDescription' => 'File Sharing Software',
  ),
  68 => 
  array (
    'fkContext' => '181',
    'fkParent' => '138',
    'sName' => 'Management Software',
    'tDescription' => 'Management Software',
  ),
  69 => 
  array (
    'fkContext' => '182',
    'fkParent' => 'null',
    'sName' => 'Physical Assets',
    'tDescription' => 'Physical Assets',
  ),
  70 => 
  array (
    'fkContext' => '183',
    'fkParent' => '182',
    'sName' => 'Infrastructure',
    'tDescription' => 'Infrastructure',
  ),
  71 => 
  array (
    'fkContext' => '184',
    'fkParent' => '183',
    'sName' => 'Building',
    'tDescription' => 'Building',
  ),
  72 => 
  array (
    'fkContext' => '185',
    'fkParent' => '183',
    'sName' => 'Data Center',
    'tDescription' => 'Data Center',
  ),
  73 => 
  array (
    'fkContext' => '186',
    'fkParent' => '183',
    'sName' => 'Environment  Control Center',
    'tDescription' => 'Environment  Control Center',
  ),
  74 => 
  array (
    'fkContext' => '187',
    'fkParent' => '183',
    'sName' => 'Server Room',
    'tDescription' => 'Server Room',
  ),
  75 => 
  array (
    'fkContext' => '188',
    'fkParent' => '183',
    'sName' => 'Store Room',
    'tDescription' => 'Store Room',
  ),
  76 => 
  array (
    'fkContext' => '189',
    'fkParent' => '183',
    'sName' => 'Distribution Center',
    'tDescription' => 'Distribution Center',
  ),
  77 => 
  array (
    'fkContext' => '190',
    'fkParent' => '183',
    'sName' => 'Data Processing Room',
    'tDescription' => 'Data Processing Room',
  ),
  78 => 
  array (
    'fkContext' => '191',
    'fkParent' => '183',
    'sName' => 'Data Warehouse',
    'tDescription' => 'Data Warehouse',
  ),
  79 => 
  array (
    'fkContext' => '192',
    'fkParent' => '183',
    'sName' => 'Operations Room',
    'tDescription' => 'Operations Room',
  ),
  80 => 
  array (
    'fkContext' => '193',
    'fkParent' => '183',
    'sName' => 'Office',
    'tDescription' => 'Office',
  ),
  81 => 
  array (
    'fkContext' => '203',
    'fkParent' => '182',
    'sName' => 'Environmental Components',
    'tDescription' => 'Environmental Components',
  ),
  82 => 
  array (
    'fkContext' => '204',
    'fkParent' => '203',
    'sName' => 'Electric System',
    'tDescription' => 'Electric System',
  ),
  83 => 
  array (
    'fkContext' => '205',
    'fkParent' => '203',
    'sName' => 'No Break',
    'tDescription' => 'No Break',
  ),
  84 => 
  array (
    'fkContext' => '206',
    'fkParent' => '203',
    'sName' => 'Lighting',
    'tDescription' => 'Lighting',
  ),
  85 => 
  array (
    'fkContext' => '207',
    'fkParent' => '203',
    'sName' => 'Cabling',
    'tDescription' => 'Cabling',
  ),
  86 => 
  array (
    'fkContext' => '208',
    'fkParent' => '182',
    'sName' => 'Equipment/Devices',
    'tDescription' => 'Equipment/Devices',
  ),
  87 => 
  array (
    'fkContext' => '209',
    'fkParent' => '208',
    'sName' => 'Storage Devices',
    'tDescription' => 'Storage Devices',
  ),
  88 => 
  array (
    'fkContext' => '210',
    'fkParent' => '209',
    'sName' => 'CD',
    'tDescription' => 'CD',
  ),
  89 => 
  array (
    'fkContext' => '211',
    'fkParent' => '209',
    'sName' => 'Diskette',
    'tDescription' => 'Diskette',
  ),
  90 => 
  array (
    'fkContext' => '212',
    'fkParent' => '209',
    'sName' => 'HD External',
    'tDescription' => 'HD External',
  ),
  91 => 
  array (
    'fkContext' => '213',
    'fkParent' => '208',
    'sName' => 'Processing Devices',
    'tDescription' => 'Processing Devices',
  ),
  92 => 
  array (
    'fkContext' => '221',
    'fkParent' => '208',
    'sName' => 'Network/Communications Devices',
    'tDescription' => 'Network/Communications Devices',
  ),
  93 => 
  array (
    'fkContext' => '222',
    'fkParent' => '221',
    'sName' => 'Router',
    'tDescription' => 'Router',
  ),
  94 => 
  array (
    'fkContext' => '223',
    'fkParent' => '221',
    'sName' => 'Switch',
    'tDescription' => 'Switch',
  ),
  95 => 
  array (
    'fkContext' => '224',
    'fkParent' => '221',
    'sName' => 'Access Point (WiFi)',
    'tDescription' => 'Access Point (WiFi)',
  ),
  96 => 
  array (
    'fkContext' => '226',
    'fkParent' => '208',
    'sName' => 'Copier/Scanner/Printer',
    'tDescription' => 'Copier/Scanner/Printer',
  ),
  97 => 
  array (
    'fkContext' => '227',
    'fkParent' => '208',
    'sName' => 'Display Devices',
    'tDescription' => 'Display Devices',
  ),
  98 => 
  array (
    'fkContext' => '228',
    'fkParent' => 'null',
    'sName' => 'Services',
    'tDescription' => 'Services',
  ),
  99 => 
  array (
    'fkContext' => '229',
    'fkParent' => '228',
    'sName' => ' Computer Services',
    'tDescription' => ' Computer Services',
  ),
  100 => 
  array (
    'fkContext' => '235',
    'fkParent' => '228',
    'sName' => 'Communication Services',
    'tDescription' => 'Communication Services',
  ),
  101 => 
  array (
    'fkContext' => '230',
    'fkParent' => '235',
    'sName' => 'E-mail Service',
    'tDescription' => 'E-mail Service',
  ),
  102 => 
  array (
    'fkContext' => '231',
    'fkParent' => '229',
    'sName' => 'File Storage Service',
    'tDescription' => 'File Storage Service',
  ),
  103 => 
  array (
    'fkContext' => '233',
    'fkParent' => '229',
    'sName' => 'Authentication Service',
    'tDescription' => 'Authentication Service',
  ),
  104 => 
  array (
    'fkContext' => '234',
    'fkParent' => '229',
    'sName' => 'Print Service',
    'tDescription' => 'Print Service',
  ),
  105 => 
  array (
    'fkContext' => '236',
    'fkParent' => '235',
    'sName' => 'Network Service (Internal)',
    'tDescription' => 'Network Service (Internal)',
  ),
  106 => 
  array (
    'fkContext' => '237',
    'fkParent' => '235',
    'sName' => 'Remote Access Service',
    'tDescription' => 'Remote Access Service',
  ),
  107 => 
  array (
    'fkContext' => '238',
    'fkParent' => '235',
    'sName' => 'Remote Administration Service',
    'tDescription' => 'Remote Administration Service',
  ),
  108 => 
  array (
    'fkContext' => '239',
    'fkParent' => '235',
    'sName' => 'External Links',
    'tDescription' => 'External Links',
  ),
  109 => 
  array (
    'fkContext' => '240',
    'fkParent' => '235',
    'sName' => 'Telephony Service',
    'tDescription' => 'Telephony Service',
  ),
  110 => 
  array (
    'fkContext' => '241',
    'fkParent' => '228',
    'sName' => 'Management Service',
    'tDescription' => 'Management Service',
  ),
  111 => 
  array (
    'fkContext' => '244',
    'fkParent' => '228',
    'sName' => 'Transport and Maintenance Service',
    'tDescription' => 'Transport and Maintenance Service',
  ),
  112 => 
  array (
    'fkContext' => '245',
    'fkParent' => '244',
    'sName' => 'Equipment  Transport Service',
    'tDescription' => 'Equipment  Transport Service',
  ),
  113 => 
  array (
    'fkContext' => '246',
    'fkParent' => '244',
    'sName' => 'Information Transport Service',
    'tDescription' => 'Information Transport Service',
  ),
  114 => 
  array (
    'fkContext' => '247',
    'fkParent' => '228',
    'sName' => 'Accounting Service',
    'tDescription' => 'Accounting Service',
  ),
  115 => 
  array (
    'fkContext' => '248',
    'fkParent' => '228',
    'sName' => 'Marketing Service',
    'tDescription' => 'Marketing Service',
  ),
  116 => 
  array (
    'fkContext' => '249',
    'fkParent' => '228',
    'sName' => 'Law Service',
    'tDescription' => 'Law Service',
  ),
  117 => 
  array (
    'fkContext' => '250',
    'fkParent' => '228',
    'sName' => 'Cleaning Service',
    'tDescription' => 'Cleaning Service',
  ),
  118 => 
  array (
    'fkContext' => '251',
    'fkParent' => '228',
    'sName' => 'IT Services',
    'tDescription' => 'IT Services',
  ),
  119 => 
  array (
    'fkContext' => '252',
    'fkParent' => '228',
    'sName' => 'General Utilities',
    'tDescription' => 'General Utilities',
  ),
  120 => 
  array (
    'fkContext' => '253',
    'fkParent' => '252',
    'sName' => 'Electrical Energy / Electrical System',
    'tDescription' => 'Electrical Energy / Electrical System',
  ),
  121 => 
  array (
    'fkContext' => '254',
    'fkParent' => '252',
    'sName' => 'Environment Conditioning Service',
    'tDescription' => 'Environment Conditioning Service',
  ),
  122 => 
  array (
    'fkContext' => '255',
    'fkParent' => 'null',
    'sName' => 'People',
    'tDescription' => 'People',
  ),
  123 => 
  array (
    'fkContext' => '256',
    'fkParent' => '255',
    'sName' => 'Employees',
    'tDescription' => 'Employees',
  ),
  124 => 
  array (
    'fkContext' => '257',
    'fkParent' => '256',
    'sName' => 'Adviser',
    'tDescription' => 'Adviser',
  ),
  125 => 
  array (
    'fkContext' => '258',
    'fkParent' => '256',
    'sName' => 'Director',
    'tDescription' => 'Director',
  ),
  126 => 
  array (
    'fkContext' => '259',
    'fkParent' => '256',
    'sName' => 'Auditor',
    'tDescription' => 'Auditor',
  ),
  127 => 
  array (
    'fkContext' => '260',
    'fkParent' => '256',
    'sName' => ' Consultant',
    'tDescription' => ' Consultant',
  ),
  128 => 
  array (
    'fkContext' => '261',
    'fkParent' => '256',
    'sName' => 'Manager',
    'tDescription' => 'Manager',
  ),
  129 => 
  array (
    'fkContext' => '262',
    'fkParent' => '256',
    'sName' => 'Analyst',
    'tDescription' => 'Analyst',
  ),
  130 => 
  array (
    'fkContext' => '263',
    'fkParent' => '256',
    'sName' => 'Developer',
    'tDescription' => 'Developer',
  ),
  131 => 
  array (
    'fkContext' => '264',
    'fkParent' => '256',
    'sName' => 'Trainee',
    'tDescription' => 'Trainee',
  ),
  132 => 
  array (
    'fkContext' => '265',
    'fkParent' => '256',
    'sName' => 'IT Employee',
    'tDescription' => 'IT Employee',
  ),
  133 => 
  array (
    'fkContext' => '266',
    'fkParent' => '256',
    'sName' => 'Security Officer',
    'tDescription' => 'Security Officer',
  ),
  134 => 
  array (
    'fkContext' => '267',
    'fkParent' => '256',
    'sName' => 'DBA',
    'tDescription' => 'DBA',
  ),
  135 => 
  array (
    'fkContext' => '268',
    'fkParent' => '256',
    'sName' => 'Administrator (TI)',
    'tDescription' => 'Administrator (TI)',
  ),
  136 => 
  array (
    'fkContext' => '269',
    'fkParent' => '255',
    'sName' => 'Third Party',
    'tDescription' => 'Third Party',
  ),
  137 => 
  array (
    'fkContext' => '270',
    'fkParent' => '255',
    'sName' => 'Supplier',
    'tDescription' => 'Supplier',
  ),
  138 => 
  array (
    'fkContext' => '271',
    'fkParent' => '255',
    'sName' => 'Service Provider',
    'tDescription' => 'Service Provider',
  ),
  139 => 
  array (
    'fkContext' => '272',
    'fkParent' => 'null',
    'sName' => 'Intangible Assets',
    'tDescription' => 'Intangible Assets',
  ),
  140 => 
  array (
    'fkContext' => '273',
    'fkParent' => '272',
    'sName' => 'Company Image',
    'tDescription' => 'Company Image',
  ),
  141 => 
  array (
    'fkContext' => '274',
    'fkParent' => '272',
    'sName' => ' Competitive Advantage',
    'tDescription' => ' Competitive Advantage',
  ),
  142 => 
  array (
    'fkContext' => '275',
    'fkParent' => '272',
    'sName' => 'Credibility',
    'tDescription' => 'Credibility',
  ),
  143 => 
  array (
    'fkContext' => '277',
    'fkParent' => '272',
    'sName' => 'Ability to Provide Services',
    'tDescription' => 'Ability to Provide Services',
  ),
  144 => 
  array (
    'fkContext' => '279',
    'fkParent' => '272',
    'sName' => 'Organization',
    'tDescription' => 'Organization',
  ),
  145 => 
  array (
    'fkContext' => '280',
    'fkParent' => '272',
    'sName' => 'Knowledge',
    'tDescription' => 'Knowledge',
  ),
  146 => 
  array (
    'fkContext' => '281',
    'fkParent' => '213',
    'sName' => 'Development Server',
    'tDescription' => 'Development Server',
  ),
  147 => 
  array (
    'fkContext' => '282',
    'fkParent' => '213',
    'sName' => 'Mainframe',
    'tDescription' => 'Mainframe',
  ),
  148 => 
  array (
    'fkContext' => '283',
    'fkParent' => '213',
    'sName' => 'Mobile Devices',
    'tDescription' => 'Mobile Devices',
  ),
  149 => 
  array (
    'fkContext' => '284',
    'fkParent' => '213',
    'sName' => 'Production Server',
    'tDescription' => 'Production Server',
  ),
  150 => 
  array (
    'fkContext' => '285',
    'fkParent' => '213',
    'sName' => 'Workstation',
    'tDescription' => 'Workstation',
  ),
  151 => 
  array (
    'fkContext' => '286',
    'fkParent' => '221',
    'sName' => 'Modem',
    'tDescription' => 'Modem',
  ),
  152 => 
  array (
    'fkContext' => '287',
    'fkParent' => '209',
    'sName' => 'Tape',
    'tDescription' => 'Tape',
  ),
  153 => 
  array (
    'fkContext' => '288',
    'fkParent' => '228',
    'sName' => 'Security Services',
    'tDescription' => 'Security Services',
  ),
  154 => 
  array (
    'fkContext' => '289',
    'fkParent' => '229',
    'sName' => 'Data Processing Service',
    'tDescription' => 'Data Processing Service',
  ),
  155 => 
  array (
    'fkContext' => '290',
    'fkParent' => '128',
    'sName' => 'Applications',
    'tDescription' => 'Applications',
  ),
  156 => 
  array (
    'fkContext' => '291',
    'fkParent' => '290',
    'sName' => 'Browser',
    'tDescription' => 'Browser',
  ),
  157 => 
  array (
    'fkContext' => '292',
    'fkParent' => '290',
    'sName' => 'E-mail Client',
    'tDescription' => 'E-mail Client',
  ),
  158 => 
  array (
    'fkContext' => '293',
    'fkParent' => '290',
    'sName' => 'Engineering Application',
    'tDescription' => 'Engineering Application',
  ),
  159 => 
  array (
    'fkContext' => '294',
    'fkParent' => '290',
    'sName' => 'Office Application',
    'tDescription' => 'Office Application',
  ),
  160 => 
  array (
    'fkContext' => '295',
    'fkParent' => '290',
    'sName' => 'Communication Application',
    'tDescription' => 'Communication Application',
  ),
  161 => 
  array (
    'fkContext' => '296',
    'fkParent' => '290',
    'sName' => 'Development Tools',
    'tDescription' => 'Development Tools',
  ),
  162 => 
  array (
    'fkContext' => '297',
    'fkParent' => '221',
    'sName' => 'Modem',
    'tDescription' => 'Modem',
  ),
)
?>