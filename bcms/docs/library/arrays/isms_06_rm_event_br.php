<?php

$laEventBR = 
array (
  0 => 
  array (
    'fkContext' => '300',
    'fkCategory' => '279',
    'sDescription' => 'Diferentes interpreta��es sobre o significado da seguran�a da informa��o.',
  ),
  1 => 
  array (
    'fkContext' => '301',
    'fkCategory' => '279',
    'sDescription' => 'Falta de direcionamento da empresa em rela��o � seguran�a da informa��o.',
  ),
  2 => 
  array (
    'fkContext' => '302',
    'fkCategory' => '279',
    'sDescription' => 'Diverg�ncia entre os objetivos da seguran�a da informa��o e os objetivos de neg�cio',
  ),
  3 => 
  array (
    'fkContext' => '303',
    'fkCategory' => '279',
    'sDescription' => 'Inexist�ncia de um enfoque claro para o gerenciamento da seguran�a da informa��o.',
  ),
  4 => 
  array (
    'fkContext' => '304',
    'fkCategory' => '279',
    'sDescription' => 'Problemas de seguran�a causados pela inexist�ncia de uma pol�tica formal que declare a abordagem para o gerenciamento da seguran�a da informa��o.',
  ),
  5 => 
  array (
    'fkContext' => '305',
    'fkCategory' => '279',
    'sDescription' => 'Falta de compreens�o dos colaboradores sobre a import�ncia da seguran�a da informa��o.',
  ),
  6 => 
  array (
    'fkContext' => '306',
    'fkCategory' => '255',
    'sDescription' => 'Desatualiza��o da Pol�tica de Seguran�a frente �s necessidades e objetivos de neg�cio.',
  ),
  7 => 
  array (
    'fkContext' => '307',
    'fkCategory' => '279',
    'sDescription' => 'Desatualiza��o da Pol�tica de Seguran�a frente �s necessidades e objetivos de neg�cio.',
  ),
  8 => 
  array (
    'fkContext' => '308',
    'fkCategory' => '279',
    'sDescription' => 'Inefici�ncia da Pol�tica de Seguran�a como instrumento de orienta��o quanto ao processo de gerenciamento da seguran�a da informa��o.',
  ),
  9 => 
  array (
    'fkContext' => '309',
    'fkCategory' => '279',
    'sDescription' => 'Falta de comprometimento da dire��o com as iniciativas de seguran�a da informa��o.',
  ),
  10 => 
  array (
    'fkContext' => '310',
    'fkCategory' => '279',
    'sDescription' => 'Falta de conhecimento dos processos e responsabilidades pela seguran�a da informa��o por parte da dire��o.',
  ),
  11 => 
  array (
    'fkContext' => '311',
    'fkCategory' => '279',
    'sDescription' => 'Indefini��o das responsabilidades pela seguran�a da informa��o devido a falta de uma defini��o clara pela dire��o.',
  ),
  12 => 
  array (
    'fkContext' => '312',
    'fkCategory' => '279',
    'sDescription' => 'Falta de alinhamento estrat�gico entre a seguran�a da informa��o e os objetivos de neg�cio.',
  ),
  13 => 
  array (
    'fkContext' => '313',
    'fkCategory' => '279',
    'sDescription' => 'Falta de credibilidade no processo de gerenciamento da seguran�a da informa��o devido a falta de apoio da dire��o.',
  ),
  14 => 
  array (
    'fkContext' => '314',
    'fkCategory' => '279',
    'sDescription' => 'Falta de apoio da dire��o na contrata��o de consultores especializados em seguran�a da informa��o.',
  ),
  15 => 
  array (
    'fkContext' => '315',
    'fkCategory' => '279',
    'sDescription' => 'Falta de recursos para o atendimento dos requisitos de seguran�a da informa��o.',
  ),
  16 => 
  array (
    'fkContext' => '316',
    'fkCategory' => '279',
    'sDescription' => 'Dificuldade de implementa��o de controles de seguran�a devido a falta de uma coordena��o com fun��es e pap�is relevantes.',
  ),
  17 => 
  array (
    'fkContext' => '317',
    'fkCategory' => '279',
    'sDescription' => 'Desalinhamento entre as a��es de seguran�a da informa��o e a pol�tica de seguran�a.',
  ),
  18 => 
  array (
    'fkContext' => '318',
    'fkCategory' => '279',
    'sDescription' => 'Falta de apoio ao processo de seguran�a da informa��o de pap�is chave na empresa .',
  ),
  19 => 
  array (
    'fkContext' => '319',
    'fkCategory' => '279',
    'sDescription' => 'Dificuldade na condu��o e solu��o de n�o conformidades do processo de seguran�a da informa��o.',
  ),
  20 => 
  array (
    'fkContext' => '320',
    'fkCategory' => '279',
    'sDescription' => 'Incapacidade de homologar m�todos para a ger�ncia de riscos.',
  ),
  21 => 
  array (
    'fkContext' => '321',
    'fkCategory' => '279',
    'sDescription' => 'Problemas de seguran�a devido � indefini��o de responsabilidades pelos processos de seguran�a da informa��o.',
  ),
  22 => 
  array (
    'fkContext' => '322',
    'fkCategory' => '279',
    'sDescription' => 'Falha na implementa��o de controles devido a indefini��o de responsabilidades claras.',
  ),
  23 => 
  array (
    'fkContext' => '323',
    'fkCategory' => '279',
    'sDescription' => 'Dificuldade por parte das pessoas em identificar e reconhecer suas responsabilidades pela seguran�a da informa��o.',
  ),
  24 => 
  array (
    'fkContext' => '324',
    'fkCategory' => '255',
    'sDescription' => 'Dificuldade por parte das pessoas em identificar e reconhecer suas responsabilidades pela seguran�a da informa��o.',
  ),
  25 => 
  array (
    'fkContext' => '325',
    'fkCategory' => '279',
    'sDescription' => 'Falhas de seguran�a devido a diverg�ncias sobre de quem � a responsabilidade pela prote��o de determinado ativo de informa��o.',
  ),
  26 => 
  array (
    'fkContext' => '326',
    'fkCategory' => '101',
    'sDescription' => 'Processamento de informa��es em ambientes que n�o possuem controle adequado.',
  ),
  27 => 
  array (
    'fkContext' => '327',
    'fkCategory' => '229',
    'sDescription' => 'Processamento de informa��es em ambientes que n�o possuem controle adequado.',
  ),
  28 => 
  array (
    'fkContext' => '328',
    'fkCategory' => '235',
    'sDescription' => 'Processamento de informa��es em ambientes que n�o possuem controle adequado.',
  ),
  29 => 
  array (
    'fkContext' => '329',
    'fkCategory' => '241',
    'sDescription' => 'Processamento de informa��es em ambientes que n�o possuem controle adequado.',
  ),
  30 => 
  array (
    'fkContext' => '331',
    'fkCategory' => '177',
    'sDescription' => 'Processamento de informa��es em ambientes que n�o possuem controle adequado.',
  ),
  31 => 
  array (
    'fkContext' => '333',
    'fkCategory' => '139',
    'sDescription' => 'Processamento de informa��es em ambientes que n�o possuem controle adequado.',
  ),
  32 => 
  array (
    'fkContext' => '334',
    'fkCategory' => '101',
    'sDescription' => 'Processamento de informa��es em notebooks e dispositivos pessoais ou privados que n�o oferecem os n�veis de seguran�a adequados.',
  ),
  33 => 
  array (
    'fkContext' => '335',
    'fkCategory' => '229',
    'sDescription' => 'Inser��o de novos hardwares e/ou softwares incompat�veis com os demais componentes do sistema.',
  ),
  34 => 
  array (
    'fkContext' => '336',
    'fkCategory' => '235',
    'sDescription' => 'Inser��o de novos hardwares e/ou softwares incompat�veis com os demais componentes do sistema.',
  ),
  35 => 
  array (
    'fkContext' => '337',
    'fkCategory' => '241',
    'sDescription' => 'Inser��o de novos hardwares e/ou softwares incompat�veis com os demais componentes do sistema.',
  ),
  36 => 
  array (
    'fkContext' => '339',
    'fkCategory' => '177',
    'sDescription' => 'Inser��o de novos hardwares e/ou softwares incompat�veis com os demais componentes do sistema.',
  ),
  37 => 
  array (
    'fkContext' => '341',
    'fkCategory' => '139',
    'sDescription' => 'Inser��o de novos hardwares e/ou softwares incompat�veis com os demais componentes do sistema.',
  ),
  38 => 
  array (
    'fkContext' => '342',
    'fkCategory' => '100',
    'sDescription' => 'Divulga��o n�o autorizada de informa��es sens�veis durante per�odo de contrata��o.',
  ),
  39 => 
  array (
    'fkContext' => '343',
    'fkCategory' => '255',
    'sDescription' => 'Divulga��o n�o autorizada de informa��es sens�veis durante per�odo de contrata��o.',
  ),
  40 => 
  array (
    'fkContext' => '344',
    'fkCategory' => '270',
    'sDescription' => 'Quebra de sigilo por parte de forneceres.',
  ),
  41 => 
  array (
    'fkContext' => '345',
    'fkCategory' => '271',
    'sDescription' => 'Quebra de sigilo por parte de forneceres.',
  ),
  42 => 
  array (
    'fkContext' => '346',
    'fkCategory' => '100',
    'sDescription' => 'Quebra de sigilo por parte de forneceres.',
  ),
  43 => 
  array (
    'fkContext' => '347',
    'fkCategory' => '255',
    'sDescription' => 'Divulga��o n�o autorizada de informa��es ap�s encerramento de contrato.',
  ),
  44 => 
  array (
    'fkContext' => '348',
    'fkCategory' => '279',
    'sDescription' => 'Desconhecimento da necessidade de contatar autoridades quando da ocorr�ncia de incidente de seguran�a relevante.',
  ),
  45 => 
  array (
    'fkContext' => '349',
    'fkCategory' => '279',
    'sDescription' => 'Desconhecimento de quais autoridades precisam ser contactadas quando da ocorr�ncia de eventos de seguran�a da informa��o.',
  ),
  46 => 
  array (
    'fkContext' => '350',
    'fkCategory' => '279',
    'sDescription' => 'Dificuldade em contactar autoridades por desconhecimento de informa��es para contato.',
  ),
  47 => 
  array (
    'fkContext' => '351',
    'fkCategory' => '235',
    'sDescription' => 'Dificuldade na conten��o de ataques externos por dificuldade em contactar autoridades apropriadas como provedores de telecomunica��o e centros de resposta a incidentes.',
  ),
  48 => 
  array (
    'fkContext' => '352',
    'fkCategory' => '279',
    'sDescription' => 'Desconhecimento de novas vulnerabilidades por falta de contato com grupos de pesquisa e servi�os de notifica��o de vulnerabilidades.',
  ),
  49 => 
  array (
    'fkContext' => '353',
    'fkCategory' => '279',
    'sDescription' => 'Falta de conhecimento especializado em seguran�a da informa��o.',
  ),
  50 => 
  array (
    'fkContext' => '354',
    'fkCategory' => '279',
    'sDescription' => 'Desconhecimento de novas t�cnicas de ataques.',
  ),
  51 => 
  array (
    'fkContext' => '355',
    'fkCategory' => '279',
    'sDescription' => 'Incapacidade de avaliar se o processo de seguran�a da informa��o permanece pertinente, adequado e eficaz.',
  ),
  52 => 
  array (
    'fkContext' => '356',
    'fkCategory' => '279',
    'sDescription' => 'Incapacidade de identificar oportunidades de melhoria nos processos de seguran�a.',
  ),
  53 => 
  array (
    'fkContext' => '357',
    'fkCategory' => '279',
    'sDescription' => 'Incapacidade de identificar se os processos de seguran�a da informa��o est�o sendo adequadamente realizados.',
  ),
  54 => 
  array (
    'fkContext' => '358',
    'fkCategory' => '100',
    'sDescription' => 'Acesso n�o autorizado de parte externa por interconex�o �s informa��es da empresa, comprometendo a seguran�a da informa��o.',
  ),
  55 => 
  array (
    'fkContext' => '359',
    'fkCategory' => '270',
    'sDescription' => 'Acesso n�o autorizado de parte externa por interconex�o �s informa��es da empresa, comprometendo a seguran�a da informa��o.',
  ),
  56 => 
  array (
    'fkContext' => '360',
    'fkCategory' => '271',
    'sDescription' => 'Acesso n�o autorizado de parte externa por interconex�o �s informa��es da empresa, comprometendo a seguran�a da informa��o.',
  ),
  57 => 
  array (
    'fkContext' => '361',
    'fkCategory' => '281',
    'sDescription' => 'Acesso n�o autorizado de parte externa por interconex�o �s informa��es da empresa, comprometendo a seguran�a da informa��o.',
  ),
  58 => 
  array (
    'fkContext' => '362',
    'fkCategory' => '236',
    'sDescription' => 'Comprometimento da rede interna atrav�s de incidentes ocorridos em redes de partes externas que est�o conectadas � rede interna.',
  ),
  59 => 
  array (
    'fkContext' => '363',
    'fkCategory' => '100',
    'sDescription' => 'Indisponibilidade de acesso para uma parte externa quando necess�rio.',
  ),
  60 => 
  array (
    'fkContext' => '364',
    'fkCategory' => '270',
    'sDescription' => 'Indisponibilidade de acesso para uma parte externa quando necess�rio.',
  ),
  61 => 
  array (
    'fkContext' => '365',
    'fkCategory' => '271',
    'sDescription' => 'Indisponibilidade de acesso para uma parte externa quando necess�rio.',
  ),
  62 => 
  array (
    'fkContext' => '366',
    'fkCategory' => '281',
    'sDescription' => 'Indisponibilidade de acesso para uma parte externa quando necess�rio.',
  ),
  63 => 
  array (
    'fkContext' => '367',
    'fkCategory' => '236',
    'sDescription' => 'Acesso remoto n�o autorizado devido a falta de seguran�a de partes externas que acessam a rede interna.',
  ),
  64 => 
  array (
    'fkContext' => '368',
    'fkCategory' => '270',
    'sDescription' => 'Acesso remoto n�o autorizado devido a falta de seguran�a de partes externas que acessam a rede interna.',
  ),
  65 => 
  array (
    'fkContext' => '369',
    'fkCategory' => '271',
    'sDescription' => 'Acesso remoto n�o autorizado devido a falta de seguran�a de partes externas que acessam a rede interna.',
  ),
  66 => 
  array (
    'fkContext' => '371',
    'fkCategory' => '270',
    'sDescription' => 'Desconhecimento, por partes externas, de suas responsabilidades quanto � seguran�a da informa��o.',
  ),
  67 => 
  array (
    'fkContext' => '372',
    'fkCategory' => '271',
    'sDescription' => 'Desconhecimento, por partes externas, de suas responsabilidades quanto � seguran�a da informa��o.',
  ),
  68 => 
  array (
    'fkContext' => '373',
    'fkCategory' => '281',
    'sDescription' => 'Desconhecimento, por partes externas, de suas responsabilidades quanto � seguran�a da informa��o.',
  ),
  69 => 
  array (
    'fkContext' => '374',
    'fkCategory' => '270',
    'sDescription' => 'Desconhecimento dos riscos relacionados ao envolvimento com partes externas.',
  ),
  70 => 
  array (
    'fkContext' => '375',
    'fkCategory' => '271',
    'sDescription' => 'Desconhecimento dos riscos relacionados ao envolvimento com partes externas.',
  ),
  71 => 
  array (
    'fkContext' => '376',
    'fkCategory' => '281',
    'sDescription' => 'Desconhecimento dos riscos relacionados ao envolvimento com partes externas.',
  ),
  72 => 
  array (
    'fkContext' => '377',
    'fkCategory' => '100',
    'sDescription' => 'Comprometimento de informa��es devido ao acesso n�o autorizado de clientes.',
  ),
  73 => 
  array (
    'fkContext' => '378',
    'fkCategory' => '236',
    'sDescription' => 'Acesso n�o autorizado de clientes � rede interna.',
  ),
  74 => 
  array (
    'fkContext' => '380',
    'fkCategory' => '177',
    'sDescription' => 'Viola��o de requisitos legais por clientes durante o uso de sistemas internos.',
  ),
  75 => 
  array (
    'fkContext' => '382',
    'fkCategory' => '100',
    'sDescription' => 'Perda de informa��es de clientes.',
  ),
  76 => 
  array (
    'fkContext' => '383',
    'fkCategory' => '100',
    'sDescription' => 'Falta de comprometimento de terceiros com a seguran�a da informa��o.',
  ),
  77 => 
  array (
    'fkContext' => '384',
    'fkCategory' => '269',
    'sDescription' => 'Falta de comprometimento de terceiros com a seguran�a da informa��o.',
  ),
  78 => 
  array (
    'fkContext' => '385',
    'fkCategory' => '270',
    'sDescription' => 'Falta de comprometimento de terceiros com a seguran�a da informa��o.',
  ),
  79 => 
  array (
    'fkContext' => '386',
    'fkCategory' => '271',
    'sDescription' => 'Falta de comprometimento de terceiros com a seguran�a da informa��o.',
  ),
  80 => 
  array (
    'fkContext' => '387',
    'fkCategory' => '100',
    'sDescription' => 'Acesso n�o autorizado de terceiros.',
  ),
  81 => 
  array (
    'fkContext' => '388',
    'fkCategory' => '269',
    'sDescription' => 'Acesso n�o autorizado de terceiros.',
  ),
  82 => 
  array (
    'fkContext' => '389',
    'fkCategory' => '270',
    'sDescription' => 'Acesso n�o autorizado de terceiros.',
  ),
  83 => 
  array (
    'fkContext' => '390',
    'fkCategory' => '271',
    'sDescription' => 'Acesso n�o autorizado de terceiros.',
  ),
  84 => 
  array (
    'fkContext' => '391',
    'fkCategory' => '100',
    'sDescription' => 'Falta de conhecimento dos terceiros sobre os requisitos de seguran�a do seu trabalho.',
  ),
  85 => 
  array (
    'fkContext' => '392',
    'fkCategory' => '269',
    'sDescription' => 'Falta de conhecimento dos terceiros sobre os requisitos de seguran�a do seu trabalho.',
  ),
  86 => 
  array (
    'fkContext' => '393',
    'fkCategory' => '270',
    'sDescription' => 'Falta de conhecimento dos terceiros sobre os requisitos de seguran�a do seu trabalho.',
  ),
  87 => 
  array (
    'fkContext' => '394',
    'fkCategory' => '271',
    'sDescription' => 'Falta de conhecimento dos terceiros sobre os requisitos de seguran�a do seu trabalho.',
  ),
  88 => 
  array (
    'fkContext' => '395',
    'fkCategory' => '269',
    'sDescription' => 'Incapacidade dos terceiros para atender aos requisitos de seguran�a.',
  ),
  89 => 
  array (
    'fkContext' => '396',
    'fkCategory' => '270',
    'sDescription' => 'Incapacidade dos terceiros para atender aos requisitos de seguran�a.',
  ),
  90 => 
  array (
    'fkContext' => '397',
    'fkCategory' => '271',
    'sDescription' => 'Incapacidade dos terceiros para atender aos requisitos de seguran�a.',
  ),
  91 => 
  array (
    'fkContext' => '398',
    'fkCategory' => '100',
    'sDescription' => 'Comprometimento de informa��es gerenciadas por terceiros.',
  ),
  92 => 
  array (
    'fkContext' => '399',
    'fkCategory' => '269',
    'sDescription' => 'Comprometimento de informa��es gerenciadas por terceiros.',
  ),
  93 => 
  array (
    'fkContext' => '400',
    'fkCategory' => '270',
    'sDescription' => 'Comprometimento de informa��es gerenciadas por terceiros.',
  ),
  94 => 
  array (
    'fkContext' => '401',
    'fkCategory' => '271',
    'sDescription' => 'Comprometimento de informa��es gerenciadas por terceiros.',
  ),
  95 => 
  array (
    'fkContext' => '402',
    'fkCategory' => '100',
    'sDescription' => 'Comprometimento de informa��es armazenadas ou processadas em ambientes de terceiros.',
  ),
  96 => 
  array (
    'fkContext' => '403',
    'fkCategory' => '269',
    'sDescription' => 'Comprometimento de informa��es armazenadas ou processadas em ambientes de terceiros.',
  ),
  97 => 
  array (
    'fkContext' => '404',
    'fkCategory' => '270',
    'sDescription' => 'Comprometimento de informa��es armazenadas ou processadas em ambientes de terceiros.',
  ),
  98 => 
  array (
    'fkContext' => '405',
    'fkCategory' => '271',
    'sDescription' => 'Comprometimento de informa��es armazenadas ou processadas em ambientes de terceiros.',
  ),
  99 => 
  array (
    'fkContext' => '406',
    'fkCategory' => '269',
    'sDescription' => 'Incapacidade dos terceiros em lidar com incidentes de seguran�a.',
  ),
  100 => 
  array (
    'fkContext' => '407',
    'fkCategory' => '270',
    'sDescription' => 'Incapacidade dos terceiros em lidar com incidentes de seguran�a.',
  ),
  101 => 
  array (
    'fkContext' => '408',
    'fkCategory' => '271',
    'sDescription' => 'Incapacidade dos terceiros em lidar com incidentes de seguran�a.',
  ),
  102 => 
  array (
    'fkContext' => '409',
    'fkCategory' => '269',
    'sDescription' => 'Desconhecimento, por terceiros, dos controles que devem ser aplicados para prote��o das informa��es.',
  ),
  103 => 
  array (
    'fkContext' => '410',
    'fkCategory' => '270',
    'sDescription' => 'Desconhecimento, por terceiros, dos controles que devem ser aplicados para prote��o das informa��es.',
  ),
  104 => 
  array (
    'fkContext' => '411',
    'fkCategory' => '271',
    'sDescription' => 'Desconhecimento, por terceiros, dos controles que devem ser aplicados para prote��o das informa��es.',
  ),
  105 => 
  array (
    'fkContext' => '412',
    'fkCategory' => '228',
    'sDescription' => 'Incapacidade de terceiros de recuperar seus n�veis de servi�o de acordo com as estrat�gias de continuidade do neg�cio desenvolvidas.',
  ),
  106 => 
  array (
    'fkContext' => '413',
    'fkCategory' => '269',
    'sDescription' => 'Incapacidade de terceiros de recuperar seus n�veis de servi�o de acordo com as estrat�gias de continuidade do neg�cio desenvolvidas.',
  ),
  107 => 
  array (
    'fkContext' => '414',
    'fkCategory' => '270',
    'sDescription' => 'Incapacidade de terceiros de recuperar seus n�veis de servi�o de acordo com as estrat�gias de continuidade do neg�cio desenvolvidas.',
  ),
  108 => 
  array (
    'fkContext' => '415',
    'fkCategory' => '271',
    'sDescription' => 'Incapacidade de terceiros de recuperar seus n�veis de servi�o de acordo com as estrat�gias de continuidade do neg�cio desenvolvidas.',
  ),
  109 => 
  array (
    'fkContext' => '416',
    'fkCategory' => '228',
    'sDescription' => 'Problemas devido a falhas no provimento do servi�o por parte de terceiros.',
  ),
  110 => 
  array (
    'fkContext' => '417',
    'fkCategory' => '269',
    'sDescription' => 'Problemas devido a falhas no provimento do servi�o por parte de terceiros.',
  ),
  111 => 
  array (
    'fkContext' => '418',
    'fkCategory' => '270',
    'sDescription' => 'Problemas devido a falhas no provimento do servi�o por parte de terceiros.',
  ),
  112 => 
  array (
    'fkContext' => '419',
    'fkCategory' => '271',
    'sDescription' => 'Problemas devido a falhas no provimento do servi�o por parte de terceiros.',
  ),
  113 => 
  array (
    'fkContext' => '420',
    'fkCategory' => '279',
    'sDescription' => 'Desconhecimento dos ativos de informa��o existentes na organiza��o.',
  ),
  114 => 
  array (
    'fkContext' => '421',
    'fkCategory' => '279',
    'sDescription' => 'Desconhecimento da localiza��o dos ativos de informa��o cr�ticos.',
  ),
  115 => 
  array (
    'fkContext' => '422',
    'fkCategory' => '279',
    'sDescription' => 'Desconhecimento de quais ativos devem ser recuperados ap�s um desastre.',
  ),
  116 => 
  array (
    'fkContext' => '423',
    'fkCategory' => '182',
    'sDescription' => 'Incapacidade de identificar roubo ou perda dos ativos de informa��o.',
  ),
  117 => 
  array (
    'fkContext' => '424',
    'fkCategory' => '213',
    'sDescription' => 'Incapacidade de identificar mudan�as de hardware ou software em ativos de informa��o.',
  ),
  118 => 
  array (
    'fkContext' => '425',
    'fkCategory' => '279',
    'sDescription' => 'Falhas na prote��o devido ao desconhecimento da import�ncia dos ativos de informa��o.',
  ),
  119 => 
  array (
    'fkContext' => '426',
    'fkCategory' => '279',
    'sDescription' => 'Falhas na prote��o de ativos de informa��o devido a indefini��o de quem � respons�vel por essa prote��o.',
  ),
  120 => 
  array (
    'fkContext' => '427',
    'fkCategory' => '279',
    'sDescription' => 'Falhas na prote��o de ativos de informa��o devido a falta de conhecimento detalhado sobre o ativo e seu funcionamento.',
  ),
  121 => 
  array (
    'fkContext' => '428',
    'fkCategory' => '279',
    'sDescription' => 'Conflito de interesses entre equipe de seguran�a e usu�rios de determinado ativo de informa��o.',
  ),
  122 => 
  array (
    'fkContext' => '429',
    'fkCategory' => '128',
    'sDescription' => 'Uso indevido de ativos de informa��o.',
  ),
  123 => 
  array (
    'fkContext' => '430',
    'fkCategory' => '213',
    'sDescription' => 'Uso indevido de ativos de informa��o.',
  ),
  124 => 
  array (
    'fkContext' => '431',
    'fkCategory' => '229',
    'sDescription' => 'Uso indevido de ativos de informa��o.',
  ),
  125 => 
  array (
    'fkContext' => '432',
    'fkCategory' => '235',
    'sDescription' => 'Uso indevido de ativos de informa��o.',
  ),
  126 => 
  array (
    'fkContext' => '433',
    'fkCategory' => '128',
    'sDescription' => 'Uso indevido de ativos de informa��o pela inexist�ncia de regras claras sobre o uso adequado e permitido.',
  ),
  127 => 
  array (
    'fkContext' => '434',
    'fkCategory' => '213',
    'sDescription' => 'Uso indevido de ativos de informa��o pela inexist�ncia de regras claras sobre o uso adequado e permitido.',
  ),
  128 => 
  array (
    'fkContext' => '435',
    'fkCategory' => '229',
    'sDescription' => 'Uso indevido de ativos de informa��o pela inexist�ncia de regras claras sobre o uso adequado e permitido.',
  ),
  129 => 
  array (
    'fkContext' => '436',
    'fkCategory' => '235',
    'sDescription' => 'Uso indevido de ativos de informa��o pela inexist�ncia de regras claras sobre o uso adequado e permitido.',
  ),
  130 => 
  array (
    'fkContext' => '438',
    'fkCategory' => '100',
    'sDescription' => 'Incapacidade de identifica��o da sensibilidade e import�ncia de determinada informa��o.',
  ),
  131 => 
  array (
    'fkContext' => '439',
    'fkCategory' => '100',
    'sDescription' => 'Incapacidade dos usu�rios em decidir como tratar determinada informa��o.',
  ),
  132 => 
  array (
    'fkContext' => '440',
    'fkCategory' => '100',
    'sDescription' => 'Manipula��o de informa��es de forma inconsistente com seu valor e import�ncia.',
  ),
  133 => 
  array (
    'fkContext' => '441',
    'fkCategory' => '100',
    'sDescription' => 'Incapacidade dos usu�rios em identificar qual o n�vel de classifica��o de determinada informa��o.',
  ),
  134 => 
  array (
    'fkContext' => '442',
    'fkCategory' => '100',
    'sDescription' => 'Tratamento da informa��o incompat�vel com o n�vel de classifica��o atribu�do.',
  ),
  135 => 
  array (
    'fkContext' => '444',
    'fkCategory' => '177',
    'sDescription' => 'Falta de identifica��o da sensibilidade de informa��es exibidas ou geradas por sistemas de aplica��o.',
  ),
  136 => 
  array (
    'fkContext' => '446',
    'fkCategory' => '100',
    'sDescription' => 'Tratamento de informa��es de forma inadequada por terceiros.',
  ),
  137 => 
  array (
    'fkContext' => '447',
    'fkCategory' => '100',
    'sDescription' => 'Incapacidade de identificar o n�vel de import�ncia e sigilo requerido pela informa��o.',
  ),
  138 => 
  array (
    'fkContext' => '448',
    'fkCategory' => '255',
    'sDescription' => 'Falta de conhecimento de cada colaborador sobre suas responsabilidades no processo de gerenciamento de seguran�a da informa��o.',
  ),
  139 => 
  array (
    'fkContext' => '450',
    'fkCategory' => '279',
    'sDescription' => 'Falta de defini��es claras das responsabilidades por determinadas atividades.',
  ),
  140 => 
  array (
    'fkContext' => '451',
    'fkCategory' => '255',
    'sDescription' => 'Contrata��o de funcion�rios incapazes de cumprir suas responsabilidades de seguran�a da informa��o.',
  ),
  141 => 
  array (
    'fkContext' => '452',
    'fkCategory' => '255',
    'sDescription' => 'Contrata��o e concess�o de acesso a sistemas cr�ticos para potenciais fraudadores.',
  ),
  142 => 
  array (
    'fkContext' => '453',
    'fkCategory' => '255',
    'sDescription' => 'Contrata��o de pessoas com antecedentes que violam a legisla��o vigente.',
  ),
  143 => 
  array (
    'fkContext' => '454',
    'fkCategory' => '255',
    'sDescription' => 'Acesso �s informa��es pessoais dos candidatos por pessoas n�o autorizadas.',
  ),
  144 => 
  array (
    'fkContext' => '455',
    'fkCategory' => '271',
    'sDescription' => 'Concess�o de acesso �s informa��es e sistemas cr�ticos para terceiros cujos antecedentes n�o foram adequadamente avaliados pela empresa prestadora de servi�os.',
  ),
  145 => 
  array (
    'fkContext' => '456',
    'fkCategory' => '269',
    'sDescription' => 'Concess�o de acesso �s informa��es e sistemas cr�ticos para terceiros cujos antecedentes n�o foram adequadamente avaliados pela empresa prestadora de servi�os.',
  ),
  146 => 
  array (
    'fkContext' => '457',
    'fkCategory' => '255',
    'sDescription' => 'Problemas de seguran�a causados por funcion�rios ou terceiros que n�o concordam com suas responsabilidades pela seguran�a da informa��o.',
  ),
  147 => 
  array (
    'fkContext' => '458',
    'fkCategory' => '255',
    'sDescription' => 'Acesso remoto indevido � rede interna por funcion�rios e/ou terceiros.',
  ),
  148 => 
  array (
    'fkContext' => '459',
    'fkCategory' => '255',
    'sDescription' => 'Incapacidade de comprovar legalmente que o funcion�rio ou terceiro concordou com suas responsabilidades de seguran�a da informa��o.',
  ),
  149 => 
  array (
    'fkContext' => '460',
    'fkCategory' => '279',
    'sDescription' => 'M� gest�o que cause �s pessoas o sentimento de sub-valoriza��o.',
  ),
  150 => 
  array (
    'fkContext' => '461',
    'fkCategory' => '279',
    'sDescription' => 'Incapacidade para implementar as a��es de seguran�a devido a falta de apoio e compreens�o da dire��o sobre os requisitos de seguran�a.',
  ),
  151 => 
  array (
    'fkContext' => '462',
    'fkCategory' => '279',
    'sDescription' => 'Falta de participa��o de funcion�rios, fornecedores e terceiros junto ao processo de seguran�a devido a falta de uma solicita��o clara da dire��o.',
  ),
  152 => 
  array (
    'fkContext' => '463',
    'fkCategory' => '279',
    'sDescription' => 'Falta de motiva��o de funcion�rios e demais colaboradores pelo processo de seguran�a da informa��o.',
  ),
  153 => 
  array (
    'fkContext' => '464',
    'fkCategory' => '255',
    'sDescription' => 'Falta de conscientiza��o sobre a import�ncia e necessidade de garantir a seguran�a da informa��o.',
  ),
  154 => 
  array (
    'fkContext' => '465',
    'fkCategory' => '255',
    'sDescription' => 'Falta de conhecimento sobre o significado de seguran�a da informa��o.',
  ),
  155 => 
  array (
    'fkContext' => '466',
    'fkCategory' => '255',
    'sDescription' => 'Falta de conhecimento sobre a pol�tica de seguran�a da informa��o e a necessidade de sua aplica��o.',
  ),
  156 => 
  array (
    'fkContext' => '467',
    'fkCategory' => '255',
    'sDescription' => 'Problemas de seguran�a causados por novos funcion�rios ou terceiros que tiveram acesso � sistemas e informa��es antes de receber o treinamento apropriado.',
  ),
  157 => 
  array (
    'fkContext' => '468',
    'fkCategory' => '255',
    'sDescription' => 'Desconhecimento sobre as normas de seguran�a que precisam ser aplicadas para correta prote��o das informa��es.',
  ),
  158 => 
  array (
    'fkContext' => '469',
    'fkCategory' => '255',
    'sDescription' => 'Falta de compet�ncia para atender as responsabilidades e desempenhar as fun��es de seguran�a da informa��o.',
  ),
  159 => 
  array (
    'fkContext' => '470',
    'fkCategory' => '255',
    'sDescription' => 'Incapacidade para reconhecer um incidente de seguran�a da informa��o.',
  ),
  160 => 
  array (
    'fkContext' => '471',
    'fkCategory' => '255',
    'sDescription' => 'Desconhecimento de novas pol�ticas, normas e procedimentos por falta de atualiza��es dos treinamentos de seguran�a.',
  ),
  161 => 
  array (
    'fkContext' => '472',
    'fkCategory' => '279',
    'sDescription' => 'Reincid�ncia de problemas de seguran�a por falta de tratamento justo com aqueles que violam as pol�ticas e normas de seguran�a.',
  ),
  162 => 
  array (
    'fkContext' => '473',
    'fkCategory' => '279',
    'sDescription' => 'Incapacidade de tratar viola��es das pol�ticas de seguran�a por falta de um processo disciplinar formal.',
  ),
  163 => 
  array (
    'fkContext' => '474',
    'fkCategory' => '255',
    'sDescription' => 'Falta de compromisso com a seguran�a da informa��o.',
  ),
  164 => 
  array (
    'fkContext' => '475',
    'fkCategory' => '255',
    'sDescription' => 'Divulga��o de informa��es sigilosas ap�s o encerramento de contrato junto a organiza��o.',
  ),
  165 => 
  array (
    'fkContext' => '476',
    'fkCategory' => '255',
    'sDescription' => 'Falta de atualiza��o das responsabilidades de seguran�a ap�s mudan�a de fun��o ou cargo dentro da empresa.',
  ),
  166 => 
  array (
    'fkContext' => '477',
    'fkCategory' => '255',
    'sDescription' => 'Acesso n�o autorizado a sistemas e informa��es por usu�rios ou terceiros cujo contrato j� encerrou, por desconhecimento dos funcion�rios sobre o encerramento do contrato.',
  ),
  167 => 
  array (
    'fkContext' => '478',
    'fkCategory' => '255',
    'sDescription' => 'Desconhecimento de novas responsabilidades e fun��es de seguran�a quando da mudan�a de cargo ou fun��o dentro da empresa.',
  ),
  168 => 
  array (
    'fkContext' => '479',
    'fkCategory' => '255',
    'sDescription' => 'Reten��o de ativos de informa��o concedidos pela organiza��o por funcion�rios, terceiros ou prestadores de servi�o ap�s encerramento de contrato.',
  ),
  169 => 
  array (
    'fkContext' => '483',
    'fkCategory' => '255',
    'sDescription' => 'Comprometimento de informa��es ap�s demiss�o ou encerramento de contrato.',
  ),
  170 => 
  array (
    'fkContext' => '484',
    'fkCategory' => '129',
    'sDescription' => 'Acesso n�o autorizado a sistemas e informa��es por usu�rios cujo contrato com a organiza��o j� foi encerrado.',
  ),
  171 => 
  array (
    'fkContext' => '486',
    'fkCategory' => '177',
    'sDescription' => 'Acesso n�o autorizado a sistemas e informa��es por usu�rios cujo contrato com a organiza��o j� foi encerrado.',
  ),
  172 => 
  array (
    'fkContext' => '488',
    'fkCategory' => '213',
    'sDescription' => 'Acesso n�o autorizado a sistemas e informa��es por usu�rios cujo contrato com a organiza��o j� foi encerrado.',
  ),
  173 => 
  array (
    'fkContext' => '489',
    'fkCategory' => '229',
    'sDescription' => 'Acesso n�o autorizado a sistemas e informa��es por usu�rios cujo contrato com a organiza��o j� foi encerrado.',
  ),
  174 => 
  array (
    'fkContext' => '494',
    'fkCategory' => '235',
    'sDescription' => 'Acesso n�o autorizado a sistemas e informa��es por usu�rios cujo contrato com a organiza��o j� foi encerrado.',
  ),
  175 => 
  array (
    'fkContext' => '498',
    'fkCategory' => '183',
    'sDescription' => 'Acesso n�o autorizado a sistemas e informa��es por usu�rios cujo contrato com a organiza��o j� foi encerrado.',
  ),
  176 => 
  array (
    'fkContext' => '499',
    'fkCategory' => '129',
    'sDescription' => 'Acesso indevido ou n�o autorizado a sistemas e informa��es por falta de manuten��o de direitos de acesso ou privil�gios de funcion�rios que mudaram de cargo e/ou fun��o.',
  ),
  177 => 
  array (
    'fkContext' => '501',
    'fkCategory' => '177',
    'sDescription' => 'Acesso indevido ou n�o autorizado a sistemas e informa��es por falta de manuten��o de direitos de acesso ou privil�gios de funcion�rios que mudaram de cargo e/ou fun��o.',
  ),
  178 => 
  array (
    'fkContext' => '503',
    'fkCategory' => '213',
    'sDescription' => 'Acesso indevido ou n�o autorizado a sistemas e informa��es por falta de manuten��o de direitos de acesso ou privil�gios de funcion�rios que mudaram de cargo e/ou fun��o.',
  ),
  179 => 
  array (
    'fkContext' => '504',
    'fkCategory' => '229',
    'sDescription' => 'Acesso indevido ou n�o autorizado a sistemas e informa��es por falta de manuten��o de direitos de acesso ou privil�gios de funcion�rios que mudaram de cargo e/ou fun��o.',
  ),
  180 => 
  array (
    'fkContext' => '509',
    'fkCategory' => '235',
    'sDescription' => 'Acesso indevido ou n�o autorizado a sistemas e informa��es por falta de manuten��o de direitos de acesso ou privil�gios de funcion�rios que mudaram de cargo e/ou fun��o.',
  ),
  181 => 
  array (
    'fkContext' => '513',
    'fkCategory' => '183',
    'sDescription' => 'Acesso indevido ou n�o autorizado a sistemas e informa��es por falta de manuten��o de direitos de acesso ou privil�gios de funcion�rios que mudaram de cargo e/ou fun��o.',
  ),
  182 => 
  array (
    'fkContext' => '514',
    'fkCategory' => '183',
    'sDescription' => 'Acesso n�o autorizado a ambientes f�sicos atrav�s de paredes e materiais fr�geis utilizados para divis�o de ambientes.',
  ),
  183 => 
  array (
    'fkContext' => '515',
    'fkCategory' => '183',
    'sDescription' => 'Acesso n�o autorizado a ambientes f�sicos atrav�s de janelas desprotegidas.',
  ),
  184 => 
  array (
    'fkContext' => '516',
    'fkCategory' => '183',
    'sDescription' => 'Acesso n�o autorizado a ambientes f�sicos por falta de divis�o e controle entre �reas com diferentes n�veis de criticidade.',
  ),
  185 => 
  array (
    'fkContext' => '517',
    'fkCategory' => '183',
    'sDescription' => 'Acesso n�o autorizado a ambientes f�sicos por falta de capacidade em definir o n�vel de prote��o f�sica requerida para determinada �rea.',
  ),
  186 => 
  array (
    'fkContext' => '518',
    'fkCategory' => '183',
    'sDescription' => 'Acesso n�o autorizado a ambientes f�sicos atrav�s de portas de emerg�ncia.',
  ),
  187 => 
  array (
    'fkContext' => '519',
    'fkCategory' => '183',
    'sDescription' => 'Acesso n�o autorizado a �reas cr�ticas de processamento de informa��es atrav�s do compartilhamento da �rea com outras atividades n�o relacionadas.',
  ),
  188 => 
  array (
    'fkContext' => '520',
    'fkCategory' => '183',
    'sDescription' => 'Viola��o de normas de combate a inc�ndio por m� aplica��o dos controles de seguran�a.',
  ),
  189 => 
  array (
    'fkContext' => '521',
    'fkCategory' => '183',
    'sDescription' => 'Acesso n�o autorizado a �reas de seguran�a por falta de controle de acesso f�sico.',
  ),
  190 => 
  array (
    'fkContext' => '522',
    'fkCategory' => '183',
    'sDescription' => 'Incapacidade para determinar acessos por falta de registro adequado.',
  ),
  191 => 
  array (
    'fkContext' => '523',
    'fkCategory' => '183',
    'sDescription' => 'Incapacidade de identifica��o de pessoas n�o autorizadas em locais restritos.',
  ),
  192 => 
  array (
    'fkContext' => '524',
    'fkCategory' => '183',
    'sDescription' => 'Acesso n�o autorizado a ambientes f�sicos atrav�s de direitos de acessos incorretamente configurados.',
  ),
  193 => 
  array (
    'fkContext' => '525',
    'fkCategory' => '183',
    'sDescription' => 'Acesso n�o autorizado a ambientes f�sicos pela sua proximidade com locais de acesso p�blico.',
  ),
  194 => 
  array (
    'fkContext' => '526',
    'fkCategory' => '183',
    'sDescription' => 'Acesso n�o autorizado a ambientes f�sicos atrav�s de falha no fechamento de ambientes onde existem informa��es cr�ticas.',
  ),
  195 => 
  array (
    'fkContext' => '527',
    'fkCategory' => '183',
    'sDescription' => 'Comprometimento do ambiente f�sico pela ocorr�ncia de inc�ndio em locais vizinhos.',
  ),
  196 => 
  array (
    'fkContext' => '528',
    'fkCategory' => '183',
    'sDescription' => 'Comprometimento do ambiente f�sico pela ocorr�ncia de inc�ndio no local.',
  ),
  197 => 
  array (
    'fkContext' => '529',
    'fkCategory' => '183',
    'sDescription' => 'Comprometimento do ambiente f�sico pela ocorr�ncia de inunda��o.',
  ),
  198 => 
  array (
    'fkContext' => '530',
    'fkCategory' => '183',
    'sDescription' => 'Comprometimento do ambiente f�sico pela ocorr�ncia de explos�es.',
  ),
  199 => 
  array (
    'fkContext' => '531',
    'fkCategory' => '183',
    'sDescription' => 'Comprometimento do ambiente f�sico por perturba��o da ordem p�blica.',
  ),
  200 => 
  array (
    'fkContext' => '532',
    'fkCategory' => '183',
    'sDescription' => 'Comprometimento do ambiente f�sico pela ocorr�ncia de desastres naturais.',
  ),
  201 => 
  array (
    'fkContext' => '533',
    'fkCategory' => '183',
    'sDescription' => 'Comprometimento de equipamentos por vazamento de �gua ou problemas na tubula��o.',
  ),
  202 => 
  array (
    'fkContext' => '534',
    'fkCategory' => '183',
    'sDescription' => 'Comprometimento da seguran�a devido o uso de m�quinas ou celulares fotogr�ficos.',
  ),
  203 => 
  array (
    'fkContext' => '535',
    'fkCategory' => '183',
    'sDescription' => 'Comprometimento da seguran�a devido ao uso de dispositivos de grava��o de v�deo ou �udio.',
  ),
  204 => 
  array (
    'fkContext' => '536',
    'fkCategory' => '183',
    'sDescription' => 'Comprometimento da seguran�a devido a��es mal intencionadas de visitantes.',
  ),
  205 => 
  array (
    'fkContext' => '537',
    'fkCategory' => '183',
    'sDescription' => 'Problemas de seguran�a devido a falta de regras claras sobre como trabalhar em �reas seguras.',
  ),
  206 => 
  array (
    'fkContext' => '538',
    'fkCategory' => '183',
    'sDescription' => 'Acesso n�o autorizado a ambientes f�sicos atrav�s de �reas em que ocorrem entregas e/ou carregamentos.',
  ),
  207 => 
  array (
    'fkContext' => '539',
    'fkCategory' => '183',
    'sDescription' => 'Comprometimento da seguran�a atrav�s de material entregue e enviado para o interior da organiza��o.',
  ),
  208 => 
  array (
    'fkContext' => '540',
    'fkCategory' => '183',
    'sDescription' => 'Sa�da n�o autorizada de ativos de informa��o.',
  ),
  209 => 
  array (
    'fkContext' => '541',
    'fkCategory' => '213',
    'sDescription' => 'Acesso n�o autorizado a equipamento cr�tico por instala��o em local de alto risco de acesso n�o autorizado.',
  ),
  210 => 
  array (
    'fkContext' => '542',
    'fkCategory' => '209',
    'sDescription' => 'Acesso n�o autorizado a equipamento cr�tico por instala��o em local de alto risco de acesso n�o autorizado.',
  ),
  211 => 
  array (
    'fkContext' => '543',
    'fkCategory' => '207',
    'sDescription' => 'Acesso n�o autorizado a equipamento cr�tico por instala��o em local de alto risco de acesso n�o autorizado.',
  ),
  212 => 
  array (
    'fkContext' => '546',
    'fkCategory' => '100',
    'sDescription' => 'Acesso n�o autorizado a informa��es por armazenamento em locais inadequados.',
  ),
  213 => 
  array (
    'fkContext' => '547',
    'fkCategory' => '213',
    'sDescription' => 'Acesso n�o autorizado a informa��es por armazenamento em locais inadequados.',
  ),
  214 => 
  array (
    'fkContext' => '548',
    'fkCategory' => '209',
    'sDescription' => 'Acesso n�o autorizado a informa��es por armazenamento em locais inadequados.',
  ),
  215 => 
  array (
    'fkContext' => '549',
    'fkCategory' => '213',
    'sDescription' => 'Comprometimento de equipamentos pela instala��o em ambientes industriais.',
  ),
  216 => 
  array (
    'fkContext' => '550',
    'fkCategory' => '209',
    'sDescription' => 'Comprometimento de equipamentos pela instala��o em ambientes industriais.',
  ),
  217 => 
  array (
    'fkContext' => '551',
    'fkCategory' => '207',
    'sDescription' => 'Comprometimento de equipamentos pela instala��o em ambientes industriais.',
  ),
  218 => 
  array (
    'fkContext' => '552',
    'fkCategory' => '213',
    'sDescription' => 'Comprometimento de equipamentos atrav�s de descarga el�trica provocado por raio.',
  ),
  219 => 
  array (
    'fkContext' => '553',
    'fkCategory' => '209',
    'sDescription' => 'Comprometimento de equipamentos atrav�s de descarga el�trica provocado por raio.',
  ),
  220 => 
  array (
    'fkContext' => '554',
    'fkCategory' => '207',
    'sDescription' => 'Comprometimento de equipamentos atrav�s de descarga el�trica provocado por raio.',
  ),
  221 => 
  array (
    'fkContext' => '555',
    'fkCategory' => '213',
    'sDescription' => 'Comprometimento de equipamentos atrav�s de condi��es ambientais como umidade e temperatura.',
  ),
  222 => 
  array (
    'fkContext' => '556',
    'fkCategory' => '209',
    'sDescription' => 'Comprometimento de equipamentos atrav�s de condi��es ambientais como umidade e temperatura.',
  ),
  223 => 
  array (
    'fkContext' => '557',
    'fkCategory' => '207',
    'sDescription' => 'Comprometimento de equipamentos atrav�s de condi��es ambientais como umidade e temperatura.',
  ),
  224 => 
  array (
    'fkContext' => '558',
    'fkCategory' => '213',
    'sDescription' => 'Comprometimento de equipamentos atrav�s de acidentes com alimentos ou bebidas mantidas pr�ximas aos equipamentos.',
  ),
  225 => 
  array (
    'fkContext' => '559',
    'fkCategory' => '209',
    'sDescription' => 'Comprometimento de equipamentos atrav�s de acidentes com alimentos ou bebidas mantidas pr�ximas aos equipamentos.',
  ),
  226 => 
  array (
    'fkContext' => '560',
    'fkCategory' => '207',
    'sDescription' => 'Comprometimento de equipamentos atrav�s de acidentes com alimentos ou bebidas mantidas pr�ximas aos equipamentos.',
  ),
  227 => 
  array (
    'fkContext' => '561',
    'fkCategory' => '213',
    'sDescription' => 'Comprometimento do equipamentos atrav�s de efeitos qu�micos.',
  ),
  228 => 
  array (
    'fkContext' => '562',
    'fkCategory' => '209',
    'sDescription' => 'Comprometimento do equipamentos atrav�s de efeitos qu�micos.',
  ),
  229 => 
  array (
    'fkContext' => '563',
    'fkCategory' => '207',
    'sDescription' => 'Comprometimento do equipamentos atrav�s de efeitos qu�micos.',
  ),
  230 => 
  array (
    'fkContext' => '564',
    'fkCategory' => '213',
    'sDescription' => 'Furto de equipamentos por instala��o em locais de risco.',
  ),
  231 => 
  array (
    'fkContext' => '565',
    'fkCategory' => '209',
    'sDescription' => 'Furto de equipamentos por instala��o em locais de risco.',
  ),
  232 => 
  array (
    'fkContext' => '566',
    'fkCategory' => '207',
    'sDescription' => 'Furto de equipamentos por instala��o em locais de risco.',
  ),
  233 => 
  array (
    'fkContext' => '567',
    'fkCategory' => '213',
    'sDescription' => 'Comprometimento de equipamentos atrav�s de oscila��es no suprimento de energia el�trica.',
  ),
  234 => 
  array (
    'fkContext' => '568',
    'fkCategory' => '209',
    'sDescription' => 'Comprometimento de equipamentos atrav�s de oscila��es no suprimento de energia el�trica.',
  ),
  235 => 
  array (
    'fkContext' => '569',
    'fkCategory' => '207',
    'sDescription' => 'Comprometimento de equipamentos atrav�s de oscila��es no suprimento de energia el�trica.',
  ),
  236 => 
  array (
    'fkContext' => '570',
    'fkCategory' => '213',
    'sDescription' => 'Comprometimento de equipamentos atrav�s de interfer�ncia eletromagn�tica.',
  ),
  237 => 
  array (
    'fkContext' => '571',
    'fkCategory' => '209',
    'sDescription' => 'Comprometimento de equipamentos atrav�s de interfer�ncia eletromagn�tica.',
  ),
  238 => 
  array (
    'fkContext' => '572',
    'fkCategory' => '207',
    'sDescription' => 'Comprometimento de equipamentos atrav�s de interfer�ncia eletromagn�tica.',
  ),
  239 => 
  array (
    'fkContext' => '573',
    'fkCategory' => '253',
    'sDescription' => 'Indisponibilidade dos recursos por falha no suprimento de energia el�trica.',
  ),
  240 => 
  array (
    'fkContext' => '574',
    'fkCategory' => '252',
    'sDescription' => 'Comprometimento dos recursos por falha no suprimento de �gua.',
  ),
  241 => 
  array (
    'fkContext' => '575',
    'fkCategory' => '254',
    'sDescription' => 'Comprometimento dos recursos por falha do sistema de calefa��o/ventila��o.',
  ),
  242 => 
  array (
    'fkContext' => '576',
    'fkCategory' => '254',
    'sDescription' => 'Comprometimento dos recursos por falha no sistema de condicionamento de ar.',
  ),
  243 => 
  array (
    'fkContext' => '577',
    'fkCategory' => '253',
    'sDescription' => 'Suprimento de energia el�trica inferior ao necess�rio para os equipamentos instalados.',
  ),
  244 => 
  array (
    'fkContext' => '578',
    'fkCategory' => '253',
    'sDescription' => 'Falhas nos dispositivos UPS.',
  ),
  245 => 
  array (
    'fkContext' => '580',
    'fkCategory' => '252',
    'sDescription' => 'Falha na ilumina��o do ambiente.',
  ),
  246 => 
  array (
    'fkContext' => '581',
    'fkCategory' => '252',
    'sDescription' => 'Falha dos equipamentos de extin��o de inc�ndio por falta de suprimento de �gua.',
  ),
  247 => 
  array (
    'fkContext' => '582',
    'fkCategory' => '252',
    'sDescription' => 'Falha dos equipamentos de condicionamento do ambiente por falta de suprimento de �gua.',
  ),
  248 => 
  array (
    'fkContext' => '583',
    'fkCategory' => '213',
    'sDescription' => 'Falha de equipamentos devido a falta de energia el�trica.',
  ),
  249 => 
  array (
    'fkContext' => '584',
    'fkCategory' => '209',
    'sDescription' => 'Falha de equipamentos devido a falta de energia el�trica.',
  ),
  250 => 
  array (
    'fkContext' => '585',
    'fkCategory' => '235',
    'sDescription' => 'Intercepta��o de informa��es atrav�s de acesso ao cabeamento.',
  ),
  251 => 
  array (
    'fkContext' => '586',
    'fkCategory' => '253',
    'sDescription' => 'Interrup��o no fornecimento de energia el�trica por falha no cabeamento.',
  ),
  252 => 
  array (
    'fkContext' => '587',
    'fkCategory' => '235',
    'sDescription' => 'Falha no cabeamento l�gico interrompendo a comunica��o de dados.',
  ),
  253 => 
  array (
    'fkContext' => '589',
    'fkCategory' => '235',
    'sDescription' => 'Falhas nas comunica��es por interfer�ncia do cabeamento el�trico.',
  ),
  254 => 
  array (
    'fkContext' => '590',
    'fkCategory' => '253',
    'sDescription' => 'Acesso n�o autorizado a pain�is de controle de energia.',
  ),
  255 => 
  array (
    'fkContext' => '591',
    'fkCategory' => '235',
    'sDescription' => 'Acesso n�o autorizado a pain�is de controle de cabeamento.',
  ),
  256 => 
  array (
    'fkContext' => '592',
    'fkCategory' => '213',
    'sDescription' => 'Falta de manuten��o peri�dica.',
  ),
  257 => 
  array (
    'fkContext' => '593',
    'fkCategory' => '209',
    'sDescription' => 'Falta de manuten��o peri�dica.',
  ),
  258 => 
  array (
    'fkContext' => '594',
    'fkCategory' => '252',
    'sDescription' => 'Falta de manuten��o peri�dica.',
  ),
  259 => 
  array (
    'fkContext' => '595',
    'fkCategory' => '213',
    'sDescription' => 'Comprometimento de equipamentos devido a manuten��o realizada por pessoas n�o capacitadas.',
  ),
  260 => 
  array (
    'fkContext' => '596',
    'fkCategory' => '209',
    'sDescription' => 'Comprometimento de equipamentos devido a manuten��o realizada por pessoas n�o capacitadas.',
  ),
  261 => 
  array (
    'fkContext' => '597',
    'fkCategory' => '252',
    'sDescription' => 'Comprometimento de equipamentos devido a manuten��o realizada por pessoas n�o capacitadas.',
  ),
  262 => 
  array (
    'fkContext' => '598',
    'fkCategory' => '213',
    'sDescription' => 'Acesso n�o autorizado �s informa��es durante manuten��o de equipamentos.',
  ),
  263 => 
  array (
    'fkContext' => '599',
    'fkCategory' => '209',
    'sDescription' => 'Acesso n�o autorizado �s informa��es durante manuten��o de equipamentos.',
  ),
  264 => 
  array (
    'fkContext' => '600',
    'fkCategory' => '213',
    'sDescription' => 'Perda de seguro devido ao n�o atendimento das exig�ncias estabelecidas sobre manuten��o de equipamentos e/ou estrutura.',
  ),
  265 => 
  array (
    'fkContext' => '601',
    'fkCategory' => '209',
    'sDescription' => 'Perda de seguro devido ao n�o atendimento das exig�ncias estabelecidas sobre manuten��o de equipamentos e/ou estrutura.',
  ),
  266 => 
  array (
    'fkContext' => '602',
    'fkCategory' => '252',
    'sDescription' => 'Perda de seguro devido ao n�o atendimento das exig�ncias estabelecidas sobre manuten��o de equipamentos e/ou estrutura.',
  ),
  267 => 
  array (
    'fkContext' => '603',
    'fkCategory' => '213',
    'sDescription' => 'Utiliza��o externa n�o autorizada de equipamentos para o processamento de informa��es.',
  ),
  268 => 
  array (
    'fkContext' => '604',
    'fkCategory' => '209',
    'sDescription' => 'Utiliza��o externa n�o autorizada de equipamentos para o processamento de informa��es.',
  ),
  269 => 
  array (
    'fkContext' => '605',
    'fkCategory' => '213',
    'sDescription' => 'Acesso n�o autorizado �s informa��es em equipamentos utilizados fora das depend�ncias da organiza��o.',
  ),
  270 => 
  array (
    'fkContext' => '606',
    'fkCategory' => '209',
    'sDescription' => 'Acesso n�o autorizado �s informa��es em equipamentos utilizados fora das depend�ncias da organiza��o.',
  ),
  271 => 
  array (
    'fkContext' => '607',
    'fkCategory' => '213',
    'sDescription' => 'Extravio de equipamentos utilizados fora das depend�ncias da organiza��o.',
  ),
  272 => 
  array (
    'fkContext' => '608',
    'fkCategory' => '209',
    'sDescription' => 'Extravio de equipamentos utilizados fora das depend�ncias da organiza��o.',
  ),
  273 => 
  array (
    'fkContext' => '609',
    'fkCategory' => '213',
    'sDescription' => 'Exposi��o de equipamentos a campos eletromagn�ticos fora da organiza��o.',
  ),
  274 => 
  array (
    'fkContext' => '610',
    'fkCategory' => '209',
    'sDescription' => 'Exposi��o de equipamentos a campos eletromagn�ticos fora da organiza��o.',
  ),
  275 => 
  array (
    'fkContext' => '611',
    'fkCategory' => '213',
    'sDescription' => 'Acesso n�o autorizado �s informa��es atrav�s equipamentos descartados.',
  ),
  276 => 
  array (
    'fkContext' => '612',
    'fkCategory' => '209',
    'sDescription' => 'Acesso n�o autorizado �s informa��es atrav�s equipamentos descartados.',
  ),
  277 => 
  array (
    'fkContext' => '613',
    'fkCategory' => '213',
    'sDescription' => 'Acesso n�o autorizado �s informa��es atrav�s de equipamentos reutilizados.',
  ),
  278 => 
  array (
    'fkContext' => '614',
    'fkCategory' => '209',
    'sDescription' => 'Acesso n�o autorizado �s informa��es atrav�s de equipamentos reutilizados.',
  ),
  279 => 
  array (
    'fkContext' => '615',
    'fkCategory' => '213',
    'sDescription' => 'Retirada de ativos da organiza��o sem autoriza��o',
  ),
  280 => 
  array (
    'fkContext' => '616',
    'fkCategory' => '209',
    'sDescription' => 'Retirada de ativos da organiza��o sem autoriza��o',
  ),
  281 => 
  array (
    'fkContext' => '617',
    'fkCategory' => '213',
    'sDescription' => 'Movimenta��o n�o autorizada de ativos dentro da organiza��o.',
  ),
  282 => 
  array (
    'fkContext' => '618',
    'fkCategory' => '209',
    'sDescription' => 'Movimenta��o n�o autorizada de ativos dentro da organiza��o.',
  ),
  283 => 
  array (
    'fkContext' => '619',
    'fkCategory' => '100',
    'sDescription' => 'Acesso n�o autorizado a informa��es atrav�s da retirada de ativos importantes da organiza��o.',
  ),
  284 => 
  array (
    'fkContext' => '620',
    'fkCategory' => '228',
    'sDescription' => 'Execu��o incorreta de servi�os de opera��o por falta de procedimentos formais documentados.',
  ),
  285 => 
  array (
    'fkContext' => '621',
    'fkCategory' => '228',
    'sDescription' => 'Interrup��o de servi�os por mudan�as mal planejadas.',
  ),
  286 => 
  array (
    'fkContext' => '622',
    'fkCategory' => '128',
    'sDescription' => 'Execu��o de mudan�as n�o autorizadas.',
  ),
  287 => 
  array (
    'fkContext' => '623',
    'fkCategory' => '182',
    'sDescription' => 'Execu��o de mudan�as n�o autorizadas.',
  ),
  288 => 
  array (
    'fkContext' => '624',
    'fkCategory' => '228',
    'sDescription' => 'Execu��o de mudan�as n�o autorizadas.',
  ),
  289 => 
  array (
    'fkContext' => '625',
    'fkCategory' => '255',
    'sDescription' => 'Execu��o de mudan�as n�o autorizadas.',
  ),
  290 => 
  array (
    'fkContext' => '626',
    'fkCategory' => '228',
    'sDescription' => 'Incapacidade de recuperar servi�os ap�s falha em mudan�as realizadas.',
  ),
  291 => 
  array (
    'fkContext' => '627',
    'fkCategory' => '129',
    'sDescription' => 'Comprometimento das configura��es de seguran�a devido a mudan�a realizada.',
  ),
  292 => 
  array (
    'fkContext' => '628',
    'fkCategory' => '128',
    'sDescription' => 'Inser��o de novas vulnerabilidades ap�s mudan�as.',
  ),
  293 => 
  array (
    'fkContext' => '629',
    'fkCategory' => '128',
    'sDescription' => 'Uso indevido de sistemas.',
  ),
  294 => 
  array (
    'fkContext' => '630',
    'fkCategory' => '128',
    'sDescription' => 'Uso indevido acidental de sistemas.',
  ),
  295 => 
  array (
    'fkContext' => '631',
    'fkCategory' => '100',
    'sDescription' => 'Modifica��o n�o autorizada de informa��es.',
  ),
  296 => 
  array (
    'fkContext' => '632',
    'fkCategory' => '129',
    'sDescription' => 'Acesso n�o autorizado ao sistema operacional de produ��o pela equipe de desenvolvimento.',
  ),
  297 => 
  array (
    'fkContext' => '633',
    'fkCategory' => '236',
    'sDescription' => 'Acesso n�o autorizado �s informa��es do ambiente de produ��o pela equipe de desenvolvimento.',
  ),
  298 => 
  array (
    'fkContext' => '635',
    'fkCategory' => '177',
    'sDescription' => 'Falha no funcionamento dos sistemas por diferen�as entre o ambiente de produ��o e o ambiente de desenvolvimento.',
  ),
  299 => 
  array (
    'fkContext' => '637',
    'fkCategory' => '101',
    'sDescription' => 'Uso de ferramentas de desenvolvimento para o comprometimento ou acesso n�o autorizado � informa��es ou sistemas.',
  ),
  300 => 
  array (
    'fkContext' => '639',
    'fkCategory' => '177',
    'sDescription' => 'Uso de ferramentas de desenvolvimento para o comprometimento ou acesso n�o autorizado � informa��es ou sistemas.',
  ),
  301 => 
  array (
    'fkContext' => '641',
    'fkCategory' => '101',
    'sDescription' => 'Acesso n�o autorizado a informa��es sens�veis que foram compiladas para o ambiente de testes ou desenvolvimento.',
  ),
  302 => 
  array (
    'fkContext' => '643',
    'fkCategory' => '177',
    'sDescription' => 'Inser��o de c�digos n�o autorizados no ambiente de produ��o.',
  ),
  303 => 
  array (
    'fkContext' => '645',
    'fkCategory' => '139',
    'sDescription' => 'Inser��o de c�digos n�o autorizados no ambiente de produ��o.',
  ),
  304 => 
  array (
    'fkContext' => '647',
    'fkCategory' => '177',
    'sDescription' => 'Inser��o de c�digos n�o testados no ambiente de produ��o.',
  ),
  305 => 
  array (
    'fkContext' => '649',
    'fkCategory' => '139',
    'sDescription' => 'Inser��o de c�digos n�o testados no ambiente de produ��o.',
  ),
  306 => 
  array (
    'fkContext' => '650',
    'fkCategory' => '271',
    'sDescription' => 'Falha na entrega de servi�os previstos em contrato.',
  ),
  307 => 
  array (
    'fkContext' => '651',
    'fkCategory' => '269',
    'sDescription' => 'Falha na entrega de servi�os previstos em contrato.',
  ),
  308 => 
  array (
    'fkContext' => '652',
    'fkCategory' => '270',
    'sDescription' => 'Falha na entrega de servi�os previstos em contrato.',
  ),
  309 => 
  array (
    'fkContext' => '653',
    'fkCategory' => '271',
    'sDescription' => 'Incapacidade de atender �s estrat�gias de continuidade do neg�cio.',
  ),
  310 => 
  array (
    'fkContext' => '654',
    'fkCategory' => '269',
    'sDescription' => 'Incapacidade de atender �s estrat�gias de continuidade do neg�cio.',
  ),
  311 => 
  array (
    'fkContext' => '655',
    'fkCategory' => '270',
    'sDescription' => 'Incapacidade de atender �s estrat�gias de continuidade do neg�cio.',
  ),
  312 => 
  array (
    'fkContext' => '656',
    'fkCategory' => '271',
    'sDescription' => 'N�o atendimento aos requisitos necess�rios para fornecimento do servi�o de maneira segura.',
  ),
  313 => 
  array (
    'fkContext' => '657',
    'fkCategory' => '269',
    'sDescription' => 'N�o atendimento aos requisitos necess�rios para fornecimento do servi�o de maneira segura.',
  ),
  314 => 
  array (
    'fkContext' => '658',
    'fkCategory' => '270',
    'sDescription' => 'N�o atendimento aos requisitos necess�rios para fornecimento do servi�o de maneira segura.',
  ),
  315 => 
  array (
    'fkContext' => '659',
    'fkCategory' => '271',
    'sDescription' => 'Incapacidade de identificar e gerenciar incidentes ocorridos na presta��o de servi�os terceirizados.',
  ),
  316 => 
  array (
    'fkContext' => '660',
    'fkCategory' => '269',
    'sDescription' => 'Incapacidade de identificar e gerenciar incidentes ocorridos na presta��o de servi�os terceirizados.',
  ),
  317 => 
  array (
    'fkContext' => '661',
    'fkCategory' => '270',
    'sDescription' => 'Incapacidade de identificar e gerenciar incidentes ocorridos na presta��o de servi�os terceirizados.',
  ),
  318 => 
  array (
    'fkContext' => '662',
    'fkCategory' => '271',
    'sDescription' => 'Acesso n�o autorizado �s informa��es e sistemas por prestadores de servi�o.',
  ),
  319 => 
  array (
    'fkContext' => '663',
    'fkCategory' => '269',
    'sDescription' => 'Acesso n�o autorizado �s informa��es e sistemas por prestadores de servi�o.',
  ),
  320 => 
  array (
    'fkContext' => '664',
    'fkCategory' => '270',
    'sDescription' => 'Acesso n�o autorizado �s informa��es e sistemas por prestadores de servi�o.',
  ),
  321 => 
  array (
    'fkContext' => '665',
    'fkCategory' => '271',
    'sDescription' => 'N�o conformidade entre os servi�os prestados e os acordos/contratos estabelecidos.',
  ),
  322 => 
  array (
    'fkContext' => '666',
    'fkCategory' => '269',
    'sDescription' => 'N�o conformidade entre os servi�os prestados e os acordos/contratos estabelecidos.',
  ),
  323 => 
  array (
    'fkContext' => '667',
    'fkCategory' => '270',
    'sDescription' => 'N�o conformidade entre os servi�os prestados e os acordos/contratos estabelecidos.',
  ),
  324 => 
  array (
    'fkContext' => '668',
    'fkCategory' => '271',
    'sDescription' => 'N�o atendimento aos requisitos para presta��o de servi�o de maneira segura.',
  ),
  325 => 
  array (
    'fkContext' => '669',
    'fkCategory' => '269',
    'sDescription' => 'N�o atendimento aos requisitos para presta��o de servi�o de maneira segura.',
  ),
  326 => 
  array (
    'fkContext' => '670',
    'fkCategory' => '270',
    'sDescription' => 'N�o atendimento aos requisitos para presta��o de servi�o de maneira segura.',
  ),
  327 => 
  array (
    'fkContext' => '671',
    'fkCategory' => '228',
    'sDescription' => 'Mudan�as no prestador de servi�o terceirizado.',
  ),
  328 => 
  array (
    'fkContext' => '672',
    'fkCategory' => '228',
    'sDescription' => 'Mudan�as realizadas por prestadores de servi�o.',
  ),
  329 => 
  array (
    'fkContext' => '673',
    'fkCategory' => '228',
    'sDescription' => 'Incapacidade dos prestadores de servi�o em atender os requisitos de melhoria.',
  ),
  330 => 
  array (
    'fkContext' => '674',
    'fkCategory' => '228',
    'sDescription' => 'Desempenho insuficiente.',
  ),
  331 => 
  array (
    'fkContext' => '675',
    'fkCategory' => '213',
    'sDescription' => 'Desempenho insuficiente.',
  ),
  332 => 
  array (
    'fkContext' => '676',
    'fkCategory' => '209',
    'sDescription' => 'Desempenho insuficiente.',
  ),
  333 => 
  array (
    'fkContext' => '677',
    'fkCategory' => '129',
    'sDescription' => 'Desempenho insuficiente.',
  ),
  334 => 
  array (
    'fkContext' => '679',
    'fkCategory' => '177',
    'sDescription' => 'Desempenho insuficiente.',
  ),
  335 => 
  array (
    'fkContext' => '681',
    'fkCategory' => '139',
    'sDescription' => 'Desempenho insuficiente.',
  ),
  336 => 
  array (
    'fkContext' => '682',
    'fkCategory' => '239',
    'sDescription' => 'Desempenho insuficiente.',
  ),
  337 => 
  array (
    'fkContext' => '683',
    'fkCategory' => '228',
    'sDescription' => 'Incapacidade de gerenciar sistemas e servi�os devido a depend�ncia de pessoa chave.',
  ),
  338 => 
  array (
    'fkContext' => '684',
    'fkCategory' => '213',
    'sDescription' => 'Incapacidade de gerenciar sistemas e servi�os devido a depend�ncia de pessoa chave.',
  ),
  339 => 
  array (
    'fkContext' => '685',
    'fkCategory' => '209',
    'sDescription' => 'Incapacidade de gerenciar sistemas e servi�os devido a depend�ncia de pessoa chave.',
  ),
  340 => 
  array (
    'fkContext' => '686',
    'fkCategory' => '129',
    'sDescription' => 'Incapacidade de gerenciar sistemas e servi�os devido a depend�ncia de pessoa chave.',
  ),
  341 => 
  array (
    'fkContext' => '688',
    'fkCategory' => '177',
    'sDescription' => 'Incapacidade de gerenciar sistemas e servi�os devido a depend�ncia de pessoa chave.',
  ),
  342 => 
  array (
    'fkContext' => '690',
    'fkCategory' => '139',
    'sDescription' => 'Incapacidade de gerenciar sistemas e servi�os devido a depend�ncia de pessoa chave.',
  ),
  343 => 
  array (
    'fkContext' => '692',
    'fkCategory' => '209',
    'sDescription' => 'Falta de espa�o para armazenamento.',
  ),
  344 => 
  array (
    'fkContext' => '693',
    'fkCategory' => '129',
    'sDescription' => 'Baixo desempenho de sistemas devido a falta de testes e avalia��o pr�via.',
  ),
  345 => 
  array (
    'fkContext' => '695',
    'fkCategory' => '177',
    'sDescription' => 'Baixo desempenho de sistemas devido a falta de testes e avalia��o pr�via.',
  ),
  346 => 
  array (
    'fkContext' => '697',
    'fkCategory' => '139',
    'sDescription' => 'Baixo desempenho de sistemas devido a falta de testes e avalia��o pr�via.',
  ),
  347 => 
  array (
    'fkContext' => '698',
    'fkCategory' => '129',
    'sDescription' => 'Falha nos planos de continuidade devido � desatualiza��o em rela��o a sistemas novos.',
  ),
  348 => 
  array (
    'fkContext' => '700',
    'fkCategory' => '177',
    'sDescription' => 'Falha nos planos de continuidade devido � desatualiza��o em rela��o a sistemas novos.',
  ),
  349 => 
  array (
    'fkContext' => '702',
    'fkCategory' => '139',
    'sDescription' => 'Falha nos planos de continuidade devido � desatualiza��o em rela��o a sistemas novos.',
  ),
  350 => 
  array (
    'fkContext' => '703',
    'fkCategory' => '129',
    'sDescription' => 'Utiliza��o de sistemas n�o testados e n�o homologados.',
  ),
  351 => 
  array (
    'fkContext' => '705',
    'fkCategory' => '177',
    'sDescription' => 'Utiliza��o de sistemas n�o testados e n�o homologados.',
  ),
  352 => 
  array (
    'fkContext' => '707',
    'fkCategory' => '139',
    'sDescription' => 'Utiliza��o de sistemas n�o testados e n�o homologados.',
  ),
  353 => 
  array (
    'fkContext' => '708',
    'fkCategory' => '129',
    'sDescription' => 'Instala��o de sistemas sem a devida avalia��o.',
  ),
  354 => 
  array (
    'fkContext' => '710',
    'fkCategory' => '177',
    'sDescription' => 'Instala��o de sistemas sem a devida avalia��o.',
  ),
  355 => 
  array (
    'fkContext' => '712',
    'fkCategory' => '139',
    'sDescription' => 'Instala��o de sistemas sem a devida avalia��o.',
  ),
  356 => 
  array (
    'fkContext' => '713',
    'fkCategory' => '129',
    'sDescription' => 'Excesso de falhas humanas devido a instala��o de sistemas de usabilidade complexa.',
  ),
  357 => 
  array (
    'fkContext' => '715',
    'fkCategory' => '177',
    'sDescription' => 'Excesso de falhas humanas devido a instala��o de sistemas de usabilidade complexa.',
  ),
  358 => 
  array (
    'fkContext' => '717',
    'fkCategory' => '139',
    'sDescription' => 'Excesso de falhas humanas devido a instala��o de sistemas de usabilidade complexa.',
  ),
  359 => 
  array (
    'fkContext' => '718',
    'fkCategory' => '129',
    'sDescription' => 'Instala��o de sistemas que n�o atendem aos requisitos de neg�cio.',
  ),
  360 => 
  array (
    'fkContext' => '720',
    'fkCategory' => '177',
    'sDescription' => 'Instala��o de sistemas que n�o atendem aos requisitos de neg�cio.',
  ),
  361 => 
  array (
    'fkContext' => '722',
    'fkCategory' => '139',
    'sDescription' => 'Instala��o de sistemas que n�o atendem aos requisitos de neg�cio.',
  ),
  362 => 
  array (
    'fkContext' => '723',
    'fkCategory' => '128',
    'sDescription' => 'Contamina��o por v�rus de computador.',
  ),
  363 => 
  array (
    'fkContext' => '724',
    'fkCategory' => '213',
    'sDescription' => 'Contamina��o por v�rus de computador.',
  ),
  364 => 
  array (
    'fkContext' => '725',
    'fkCategory' => '209',
    'sDescription' => 'Contamina��o por v�rus de computador.',
  ),
  365 => 
  array (
    'fkContext' => '726',
    'fkCategory' => '229',
    'sDescription' => 'Contamina��o por v�rus de computador.',
  ),
  366 => 
  array (
    'fkContext' => '727',
    'fkCategory' => '235',
    'sDescription' => 'Contamina��o por v�rus de computador.',
  ),
  367 => 
  array (
    'fkContext' => '728',
    'fkCategory' => '241',
    'sDescription' => 'Contamina��o por v�rus de computador.',
  ),
  368 => 
  array (
    'fkContext' => '729',
    'fkCategory' => '129',
    'sDescription' => 'Instala��o de "cavalos de tr�ia".',
  ),
  369 => 
  array (
    'fkContext' => '730',
    'fkCategory' => '213',
    'sDescription' => 'Instala��o de "cavalos de tr�ia".',
  ),
  370 => 
  array (
    'fkContext' => '731',
    'fkCategory' => '129',
    'sDescription' => 'Recebimento de c�digo malicioso via e-mail.',
  ),
  371 => 
  array (
    'fkContext' => '732',
    'fkCategory' => '213',
    'sDescription' => 'Recebimento de c�digo malicioso via e-mail.',
  ),
  372 => 
  array (
    'fkContext' => '733',
    'fkCategory' => '129',
    'sDescription' => 'Contamina��o de recursos atrav�s da inser��o de arquivos infectados em computadores.',
  ),
  373 => 
  array (
    'fkContext' => '734',
    'fkCategory' => '213',
    'sDescription' => 'Contamina��o de recursos atrav�s da inser��o de arquivos infectados em computadores.',
  ),
  374 => 
  array (
    'fkContext' => '735',
    'fkCategory' => '129',
    'sDescription' => 'Inser��o de c�digos maliciosos durante procedimentos de emerg�ncia no qual s�o desconsiderados controles de seguran�a.',
  ),
  375 => 
  array (
    'fkContext' => '736',
    'fkCategory' => '213',
    'sDescription' => 'Inser��o de c�digos maliciosos durante procedimentos de emerg�ncia no qual s�o desconsiderados controles de seguran�a.',
  ),
  376 => 
  array (
    'fkContext' => '737',
    'fkCategory' => '129',
    'sDescription' => 'Contamina��o de ativos de informa��o por falta de atualiza��o das ferramentas de detec��o e controle de c�digo malicioso.',
  ),
  377 => 
  array (
    'fkContext' => '738',
    'fkCategory' => '213',
    'sDescription' => 'Contamina��o de ativos de informa��o por falta de atualiza��o das ferramentas de detec��o e controle de c�digo malicioso.',
  ),
  378 => 
  array (
    'fkContext' => '739',
    'fkCategory' => '213',
    'sDescription' => 'Execu��o de c�digos m�veis n�o autorizados.',
  ),
  379 => 
  array (
    'fkContext' => '740',
    'fkCategory' => '213',
    'sDescription' => 'Incapacidade de identificar a tentativa de execu��o de c�digos m�veis.',
  ),
  380 => 
  array (
    'fkContext' => '741',
    'fkCategory' => '213',
    'sDescription' => 'Manipula��o de c�digos m�veis para execu��o de atividades n�o autorizadas.',
  ),
  381 => 
  array (
    'fkContext' => '742',
    'fkCategory' => '213',
    'sDescription' => 'Utiliza��o indevida de recursos locais pelo c�digo m�vel.',
  ),
  382 => 
  array (
    'fkContext' => '743',
    'fkCategory' => '101',
    'sDescription' => 'Perda de informa��es cr�ticas.',
  ),
  383 => 
  array (
    'fkContext' => '744',
    'fkCategory' => '128',
    'sDescription' => 'Perda de softwares cr�ticos.',
  ),
  384 => 
  array (
    'fkContext' => '745',
    'fkCategory' => '101',
    'sDescription' => 'Incapacidade de recupera��o de informa��es por execu��o de c�pia de seguran�a apenas da informa��o e n�o do software necess�rio para interpreta��o dos dados.',
  ),
  385 => 
  array (
    'fkContext' => '746',
    'fkCategory' => '101',
    'sDescription' => 'Comprometimento da informa��o original e das c�pias de seguran�a durante desastres devido a proximidade de armazenamento de tais c�pias.',
  ),
  386 => 
  array (
    'fkContext' => '748',
    'fkCategory' => '101',
    'sDescription' => 'Incapacidade de recupera��o de c�pias de seguran�a por deteriora��o das m�dias de armazenamento.',
  ),
  387 => 
  array (
    'fkContext' => '749',
    'fkCategory' => '209',
    'sDescription' => 'Incapacidade de recupera��o de c�pias de seguran�a por deteriora��o das m�dias de armazenamento.',
  ),
  388 => 
  array (
    'fkContext' => '750',
    'fkCategory' => '279',
    'sDescription' => 'Pol�tica de c�pias de seguran�a incompat�vel com as estrat�gias de continuidade do neg�cio.',
  ),
  389 => 
  array (
    'fkContext' => '751',
    'fkCategory' => '236',
    'sDescription' => 'Indisponibilidade de acesso devido falha de configura��o.',
  ),
  390 => 
  array (
    'fkContext' => '752',
    'fkCategory' => '236',
    'sDescription' => 'Acesso n�o autorizado devido intercepta��o das informa��es trafegadas.',
  ),
  391 => 
  array (
    'fkContext' => '753',
    'fkCategory' => '236',
    'sDescription' => 'Acesso n�o autorizado � rede atrav�s de pontos de acesso n�o controlados.',
  ),
  392 => 
  array (
    'fkContext' => '754',
    'fkCategory' => '236',
    'sDescription' => 'Acesso n�o autorizado � rede por causa de um processo de gerenciamento de usu�rios inadequado.',
  ),
  393 => 
  array (
    'fkContext' => '755',
    'fkCategory' => '236',
    'sDescription' => 'Inser��o de equipamentos n�o homologados (recursos pessoais, notebook de terceiros...).',
  ),
  394 => 
  array (
    'fkContext' => '756',
    'fkCategory' => '236',
    'sDescription' => 'Divulga��o de informa��es a respeito da rede por falha na configura��o do DNS.',
  ),
  395 => 
  array (
    'fkContext' => '757',
    'fkCategory' => '236',
    'sDescription' => 'Problemas no acesso � rede por falha na configura��o de DNS.',
  ),
  396 => 
  array (
    'fkContext' => '758',
    'fkCategory' => '236',
    'sDescription' => 'Acesso indevido atrav�s da m� configura��o do servi�o de endere�amento de rede.',
  ),
  397 => 
  array (
    'fkContext' => '759',
    'fkCategory' => '236',
    'sDescription' => 'Problemas de acesso � rede por falhas nos seus principais servi�os (AD, DHCP, DNS, Netbios...).',
  ),
  398 => 
  array (
    'fkContext' => '760',
    'fkCategory' => '101',
    'sDescription' => 'Compartilhamento inadequado de arquivos na rede.',
  ),
  399 => 
  array (
    'fkContext' => '761',
    'fkCategory' => '236',
    'sDescription' => 'Incapacidade de identificar a��es n�o autorizadas na rede.',
  ),
  400 => 
  array (
    'fkContext' => '762',
    'fkCategory' => '236',
    'sDescription' => 'Indisponibilidade da rede devido ataques internos.',
  ),
  401 => 
  array (
    'fkContext' => '763',
    'fkCategory' => '236',
    'sDescription' => 'Sobreposi��o dos controles de seguran�a da rede atrav�s da utiliza��o de servi�os que mascaram os acessos.',
  ),
  402 => 
  array (
    'fkContext' => '764',
    'fkCategory' => '236',
    'sDescription' => 'Falta de mecanismos de seguran�a das redes externas (criptografia, VPN, ...).',
  ),
  403 => 
  array (
    'fkContext' => '765',
    'fkCategory' => '236',
    'sDescription' => 'Problemas nos servi�os de rede.',
  ),
  404 => 
  array (
    'fkContext' => '766',
    'fkCategory' => '236',
    'sDescription' => 'Incapacidade de fornecer n�veis de servi�o adequado.',
  ),
  405 => 
  array (
    'fkContext' => '767',
    'fkCategory' => '236',
    'sDescription' => 'Incapacidade de aplicar os requisitos de seguran�a adequados para cada servi�o.',
  ),
  406 => 
  array (
    'fkContext' => '768',
    'fkCategory' => '236',
    'sDescription' => 'Gerenciamento inadequado de servi�os de rede.',
  ),
  407 => 
  array (
    'fkContext' => '769',
    'fkCategory' => '236',
    'sDescription' => 'Falha na comunica��o e no tratamento de incidentes de seguran�a.',
  ),
  408 => 
  array (
    'fkContext' => '770',
    'fkCategory' => '115',
    'sDescription' => 'Acesso n�o autorizado � m�dias de armazenamento de informa��o.',
  ),
  409 => 
  array (
    'fkContext' => '771',
    'fkCategory' => '209',
    'sDescription' => 'Acesso n�o autorizado � m�dias de armazenamento de informa��o.',
  ),
  410 => 
  array (
    'fkContext' => '772',
    'fkCategory' => '115',
    'sDescription' => 'Remo��o n�o autorizada de informa��es armazenadas em m�dias para fora da organiza��o.',
  ),
  411 => 
  array (
    'fkContext' => '773',
    'fkCategory' => '209',
    'sDescription' => 'Remo��o n�o autorizada de informa��es armazenadas em m�dias para fora da organiza��o.',
  ),
  412 => 
  array (
    'fkContext' => '774',
    'fkCategory' => '115',
    'sDescription' => 'Corrup��o de dados gravados em m�dias por armazenamento inadequado.',
  ),
  413 => 
  array (
    'fkContext' => '775',
    'fkCategory' => '209',
    'sDescription' => 'Corrup��o de dados gravados em m�dias por armazenamento inadequado.',
  ),
  414 => 
  array (
    'fkContext' => '776',
    'fkCategory' => '115',
    'sDescription' => 'Perda de informa��es armazenadas por longos per�odos de tempo por deterioriza��o de m�dias.',
  ),
  415 => 
  array (
    'fkContext' => '777',
    'fkCategory' => '209',
    'sDescription' => 'Perda de informa��es armazenadas por longos per�odos de tempo por deterioriza��o de m�dias.',
  ),
  416 => 
  array (
    'fkContext' => '778',
    'fkCategory' => '209',
    'sDescription' => 'C�pia n�o autorizada de informa��es atrav�s de unidades de m�dias remov�veis habilitadas desnecessariamente.',
  ),
  417 => 
  array (
    'fkContext' => '779',
    'fkCategory' => '209',
    'sDescription' => 'C�pia n�o autorizada de informa��es atrav�s de unidades de m�dias remov�veis habilitadas por necessidade do neg�cio.',
  ),
  418 => 
  array (
    'fkContext' => '780',
    'fkCategory' => '115',
    'sDescription' => 'Acesso n�o autorizado a informa��es armazenadas em m�dias descartadas.',
  ),
  419 => 
  array (
    'fkContext' => '781',
    'fkCategory' => '209',
    'sDescription' => 'Acesso n�o autorizado a informa��es armazenadas em m�dias descartadas.',
  ),
  420 => 
  array (
    'fkContext' => '782',
    'fkCategory' => '101',
    'sDescription' => 'Tratamento de informa��es de forma incompat�vel com a criticidade da informa��o.',
  ),
  421 => 
  array (
    'fkContext' => '783',
    'fkCategory' => '100',
    'sDescription' => 'Acesso n�o autorizado a informa��es por falta de restri��o de acesso compat�vel com o n�vel de sensibilidade da informa��o.',
  ),
  422 => 
  array (
    'fkContext' => '784',
    'fkCategory' => '100',
    'sDescription' => 'Acesso a informa��es sens�veis por destinat�rios n�o autorizados.',
  ),
  423 => 
  array (
    'fkContext' => '786',
    'fkCategory' => '177',
    'sDescription' => 'Acesso n�o autorizado a descri��es de processos de neg�cio em documenta��es de sistemas.',
  ),
  424 => 
  array (
    'fkContext' => '788',
    'fkCategory' => '139',
    'sDescription' => 'Acesso n�o autorizado a descri��es de processos de neg�cio em documenta��es de sistemas.',
  ),
  425 => 
  array (
    'fkContext' => '790',
    'fkCategory' => '177',
    'sDescription' => 'Acesso n�o autorizado a procedimentos e rotinas de sistemas descritos na documenta��o dos sistemas.',
  ),
  426 => 
  array (
    'fkContext' => '792',
    'fkCategory' => '139',
    'sDescription' => 'Acesso n�o autorizado a procedimentos e rotinas de sistemas descritos na documenta��o dos sistemas.',
  ),
  427 => 
  array (
    'fkContext' => '794',
    'fkCategory' => '177',
    'sDescription' => 'Acesso n�o autorizado a estrutura de dados descritos na documenta��o dos sistemas.',
  ),
  428 => 
  array (
    'fkContext' => '796',
    'fkCategory' => '139',
    'sDescription' => 'Acesso n�o autorizado a estrutura de dados descritos na documenta��o dos sistemas.',
  ),
  429 => 
  array (
    'fkContext' => '797',
    'fkCategory' => '235',
    'sDescription' => 'Acesso n�o autorizado a informa��es atrav�s da intercepta��o de comunica��es.',
  ),
  430 => 
  array (
    'fkContext' => '798',
    'fkCategory' => '230',
    'sDescription' => 'Acesso n�o autorizado a informa��es atrav�s da intercepta��o de comunica��es.',
  ),
  431 => 
  array (
    'fkContext' => '799',
    'fkCategory' => '235',
    'sDescription' => 'Altera��o n�o autorizada de dados transmitidos ou recebidos.',
  ),
  432 => 
  array (
    'fkContext' => '800',
    'fkCategory' => '230',
    'sDescription' => 'Altera��o n�o autorizada de dados transmitidos ou recebidos.',
  ),
  433 => 
  array (
    'fkContext' => '801',
    'fkCategory' => '235',
    'sDescription' => 'Recebimento de c�digo malicioso atrav�s de meios de comunica��o.',
  ),
  434 => 
  array (
    'fkContext' => '802',
    'fkCategory' => '230',
    'sDescription' => 'Recebimento de c�digo malicioso atrav�s de meios de comunica��o.',
  ),
  435 => 
  array (
    'fkContext' => '803',
    'fkCategory' => '235',
    'sDescription' => 'Envio de c�digo malicioso atrav�s de meios de comunica��o.',
  ),
  436 => 
  array (
    'fkContext' => '804',
    'fkCategory' => '230',
    'sDescription' => 'Envio de c�digo malicioso atrav�s de meios de comunica��o.',
  ),
  437 => 
  array (
    'fkContext' => '805',
    'fkCategory' => '235',
    'sDescription' => 'Envio n�o autorizado de informa��es sens�veis na forma de anexos.',
  ),
  438 => 
  array (
    'fkContext' => '806',
    'fkCategory' => '230',
    'sDescription' => 'Envio n�o autorizado de informa��es sens�veis na forma de anexos.',
  ),
  439 => 
  array (
    'fkContext' => '807',
    'fkCategory' => '235',
    'sDescription' => 'Recebimento de informa��es n�o autorizadas atrav�s de arquivos anexos.',
  ),
  440 => 
  array (
    'fkContext' => '808',
    'fkCategory' => '230',
    'sDescription' => 'Recebimento de informa��es n�o autorizadas atrav�s de arquivos anexos.',
  ),
  441 => 
  array (
    'fkContext' => '809',
    'fkCategory' => '235',
    'sDescription' => 'Recebimento de informa��es n�o autorizadas atrav�s do download de dados.',
  ),
  442 => 
  array (
    'fkContext' => '810',
    'fkCategory' => '230',
    'sDescription' => 'Recebimento de informa��es n�o autorizadas atrav�s do download de dados.',
  ),
  443 => 
  array (
    'fkContext' => '811',
    'fkCategory' => '235',
    'sDescription' => 'Envio de informa��es n�o autorizadas atrav�s do upload de dados.',
  ),
  444 => 
  array (
    'fkContext' => '812',
    'fkCategory' => '230',
    'sDescription' => 'Envio de informa��es n�o autorizadas atrav�s do upload de dados.',
  ),
  445 => 
  array (
    'fkContext' => '813',
    'fkCategory' => '235',
    'sDescription' => 'Uso indevido de recursos de comunica��o.',
  ),
  446 => 
  array (
    'fkContext' => '814',
    'fkCategory' => '230',
    'sDescription' => 'Uso indevido de recursos de comunica��o.',
  ),
  447 => 
  array (
    'fkContext' => '815',
    'fkCategory' => '236',
    'sDescription' => 'Uso de redes sem fio sem prote��o adequada.',
  ),
  448 => 
  array (
    'fkContext' => '816',
    'fkCategory' => '235',
    'sDescription' => 'Difama��o realizada por usu�rios atrav�s de servi�os de comunica��o',
  ),
  449 => 
  array (
    'fkContext' => '817',
    'fkCategory' => '230',
    'sDescription' => 'Difama��o realizada por usu�rios atrav�s de servi�os de comunica��o',
  ),
  450 => 
  array (
    'fkContext' => '818',
    'fkCategory' => '273',
    'sDescription' => 'Difama��o realizada por usu�rios atrav�s de servi�os de comunica��o',
  ),
  451 => 
  array (
    'fkContext' => '819',
    'fkCategory' => '100',
    'sDescription' => 'Acesso n�o autorizado a informa��es cr�ticas deixadas em impressoras.',
  ),
  452 => 
  array (
    'fkContext' => '820',
    'fkCategory' => '234',
    'sDescription' => 'Acesso n�o autorizado a informa��es cr�ticas deixadas em impressoras.',
  ),
  453 => 
  array (
    'fkContext' => '821',
    'fkCategory' => '100',
    'sDescription' => 'Acesso n�o autorizado a informa��es cr�ticas deixadas em copiadoras.',
  ),
  454 => 
  array (
    'fkContext' => '822',
    'fkCategory' => '100',
    'sDescription' => 'Acesso n�o autorizado a informa��es cr�ticas deixadas em equipamentos de fax.',
  ),
  455 => 
  array (
    'fkContext' => '823',
    'fkCategory' => '235',
    'sDescription' => 'Envio n�o autorizado de informa��es atrav�s de recursos de retransmiss�o autom�tica.',
  ),
  456 => 
  array (
    'fkContext' => '824',
    'fkCategory' => '230',
    'sDescription' => 'Envio n�o autorizado de informa��es atrav�s de recursos de retransmiss�o autom�tica.',
  ),
  457 => 
  array (
    'fkContext' => '825',
    'fkCategory' => '235',
    'sDescription' => 'Acesso n�o autorizado a informa��es atrav�s de escutas telef�nicas.',
  ),
  458 => 
  array (
    'fkContext' => '826',
    'fkCategory' => '235',
    'sDescription' => 'Divulga��o acidental de informa��es a pessoas pr�ximas durante conversas telef�nicas.',
  ),
  459 => 
  array (
    'fkContext' => '827',
    'fkCategory' => '255',
    'sDescription' => 'Divulga��o acidental de informa��es a pessoas pr�ximas durante conversas telef�nicas.',
  ),
  460 => 
  array (
    'fkContext' => '828',
    'fkCategory' => '235',
    'sDescription' => 'Acesso n�o autorizado a informa��es deixadas em secret�rias eletr�nicas.',
  ),
  461 => 
  array (
    'fkContext' => '829',
    'fkCategory' => '235',
    'sDescription' => 'Envio acidental de informa��es atrav�s de fax por modifica��o n�o autorizada do equipamento para envio a outros n�meros que n�o aqueles intencionados.',
  ),
  462 => 
  array (
    'fkContext' => '830',
    'fkCategory' => '235',
    'sDescription' => 'Recebimento n�o autorizado de informa��es atrav�s de fax por incapacidade de garantir a identidade do receptor.',
  ),
  463 => 
  array (
    'fkContext' => '831',
    'fkCategory' => '230',
    'sDescription' => 'Envio acidental de informa��es atrav�s de erro no endere�amento de e-mails.',
  ),
  464 => 
  array (
    'fkContext' => '832',
    'fkCategory' => '269',
    'sDescription' => 'Perda de informa��es enviadas para entidades externas.',
  ),
  465 => 
  array (
    'fkContext' => '833',
    'fkCategory' => '281',
    'sDescription' => 'Perda de informa��es enviadas para entidades externas.',
  ),
  466 => 
  array (
    'fkContext' => '834',
    'fkCategory' => '271',
    'sDescription' => 'Perda de informa��es enviadas para entidades externas.',
  ),
  467 => 
  array (
    'fkContext' => '835',
    'fkCategory' => '235',
    'sDescription' => 'Perda de informa��es enviadas para entidades externas.',
  ),
  468 => 
  array (
    'fkContext' => '836',
    'fkCategory' => '235',
    'sDescription' => 'N�o recebimento de informa��es enviadas por entidades externas.',
  ),
  469 => 
  array (
    'fkContext' => '837',
    'fkCategory' => '235',
    'sDescription' => 'Acesso n�o autorizado a informa��es trocadas com entidades externas durante envio/recep��o.',
  ),
  470 => 
  array (
    'fkContext' => '838',
    'fkCategory' => '235',
    'sDescription' => 'Entrega de informa��es a portadores n�o autorizados durante transporte de mensagens.',
  ),
  471 => 
  array (
    'fkContext' => '839',
    'fkCategory' => '209',
    'sDescription' => 'Roubo de m�dias que s�o transportadas para fora dos limites da organiza��o.',
  ),
  472 => 
  array (
    'fkContext' => '840',
    'fkCategory' => '209',
    'sDescription' => 'Acesso n�o autorizado a m�dias que s�o transportadas para fora dos limites da organiza��o.',
  ),
  473 => 
  array (
    'fkContext' => '841',
    'fkCategory' => '209',
    'sDescription' => 'Dano a m�dias por uso de embalagens inadequadas para transporte para fora dos limites da organiza��o.',
  ),
  474 => 
  array (
    'fkContext' => '842',
    'fkCategory' => '209',
    'sDescription' => 'Comprometimento de informa��es contidas em m�dias transportadas para fora da organiza��o pelo servi�o de mensageiros.',
  ),
  475 => 
  array (
    'fkContext' => '843',
    'fkCategory' => '235',
    'sDescription' => 'Acesso n�o autorizado a mensagens enviadas eletronicamente.',
  ),
  476 => 
  array (
    'fkContext' => '844',
    'fkCategory' => '235',
    'sDescription' => 'Erro de endere�amento no envio de mensagens enviadas eletronicamente.',
  ),
  477 => 
  array (
    'fkContext' => '845',
    'fkCategory' => '235',
    'sDescription' => 'Falhas na entrega de mensagens enviadas eletronicamente.',
  ),
  478 => 
  array (
    'fkContext' => '846',
    'fkCategory' => '235',
    'sDescription' => 'Incapacidade de garantir a autenticidade de mensagens enviadas eletronicamente.',
  ),
  479 => 
  array (
    'fkContext' => '847',
    'fkCategory' => '235',
    'sDescription' => 'Comprometimento da rede interna pelo uso de servi�os de mensagens instant�neas.',
  ),
  480 => 
  array (
    'fkContext' => '848',
    'fkCategory' => '235',
    'sDescription' => 'Divulga��o n�o autorizada de informa��es atrav�s do uso de servi�os de mensagem instant�nea.',
  ),
  481 => 
  array (
    'fkContext' => '850',
    'fkCategory' => '177',
    'sDescription' => 'Divulga��o n�o autorizada de informa��es devido a interconex�o de sistemas.',
  ),
  482 => 
  array (
    'fkContext' => '852',
    'fkCategory' => '139',
    'sDescription' => 'Divulga��o n�o autorizada de informa��es devido a interconex�o de sistemas.',
  ),
  483 => 
  array (
    'fkContext' => '854',
    'fkCategory' => '177',
    'sDescription' => 'Acesso n�o autorizado a informa��es atrav�s do compartilhamento de dados entre sistemas.',
  ),
  484 => 
  array (
    'fkContext' => '856',
    'fkCategory' => '139',
    'sDescription' => 'Acesso n�o autorizado a informa��es atrav�s do compartilhamento de dados entre sistemas.',
  ),
  485 => 
  array (
    'fkContext' => '858',
    'fkCategory' => '177',
    'sDescription' => 'Comprometimento de informa��es e fun��es por falhas em outro sistema interconectado.',
  ),
  486 => 
  array (
    'fkContext' => '861',
    'fkCategory' => '177',
    'sDescription' => 'Leitura n�o autorizada das informa��es de venda de com�rcio eletr�nico que transitam em redes p�blicas.',
  ),
  487 => 
  array (
    'fkContext' => '862',
    'fkCategory' => '177',
    'sDescription' => 'Modifica��o n�o autorizada de informa��es de venda de com�rcio eletr�nico que transitam em redes p�blicas.',
  ),
  488 => 
  array (
    'fkContext' => '863',
    'fkCategory' => '177',
    'sDescription' => 'Modifica��o n�o autorizada de pre�os e informa��es no site de com�rcio eletr�nico.',
  ),
  489 => 
  array (
    'fkContext' => '864',
    'fkCategory' => '177',
    'sDescription' => 'Viola��o das leis de prote��o ao consumidor durante vendas realizadas atrav�s do site de com�rcio eletr�nico.',
  ),
  490 => 
  array (
    'fkContext' => '865',
    'fkCategory' => '177',
    'sDescription' => 'Modifica��o do site de com�rcio eletr�nico atrav�s do acesso indevido de hackers e/ou usu�rios n�o autorizados.',
  ),
  491 => 
  array (
    'fkContext' => '866',
    'fkCategory' => '177',
    'sDescription' => 'Fornecimento de informa��es incorretas para o pagamento de compras realizadas no site de com�rcio eletr�nico.',
  ),
  492 => 
  array (
    'fkContext' => '867',
    'fkCategory' => '177',
    'sDescription' => 'Indisponibilidade do site de com�rcio eletr�nico.',
  ),
  493 => 
  array (
    'fkContext' => '869',
    'fkCategory' => '177',
    'sDescription' => 'Transmiss�o incorreta de transa��es on-line.',
  ),
  494 => 
  array (
    'fkContext' => '872',
    'fkCategory' => '177',
    'sDescription' => 'Erros de roteamento no envio ou recebimento de transa��es on-line.',
  ),
  495 => 
  array (
    'fkContext' => '875',
    'fkCategory' => '177',
    'sDescription' => 'Altera��es n�o autorizadas em mensagens de transa��es on-line.',
  ),
  496 => 
  array (
    'fkContext' => '878',
    'fkCategory' => '177',
    'sDescription' => 'Leitura e divulga��o n�o autorizada de informa��es contidas em mensagens de transa��es on-line.',
  ),
  497 => 
  array (
    'fkContext' => '881',
    'fkCategory' => '177',
    'sDescription' => 'Reapresenta��o n�o autorizada de mensagens em transa��es on-line.',
  ),
  498 => 
  array (
    'fkContext' => '883',
    'fkCategory' => '273',
    'sDescription' => 'Informa��es incorretas ou n�o autorizadas em sistemas publicamente dispon�veis.',
  ),
  499 => 
  array (
    'fkContext' => '885',
    'fkCategory' => '177',
    'sDescription' => 'Modifica��o n�o autorizada de informa��es em sistemas publicamente dispon�veis.',
  ),
  500 => 
  array (
    'fkContext' => '888',
    'fkCategory' => '177',
    'sDescription' => 'Publica��o de dados n�o autorizados por utiliza��o de informa��es inseridas diretamente em sistemas publicamente dispon�veis e n�o aprovadas previamente.',
  ),
  501 => 
  array (
    'fkContext' => '891',
    'fkCategory' => '177',
    'sDescription' => 'Viola��o de leis de propriedade intelectual atrav�s da disponibiliza��o indevida de informa��es em sistemas publicamente dispon�veis.',
  ),
  502 => 
  array (
    'fkContext' => '894',
    'fkCategory' => '177',
    'sDescription' => 'Incapacidade de identificar a��es realizadas por usu�rios no sistema.',
  ),
  503 => 
  array (
    'fkContext' => '896',
    'fkCategory' => '129',
    'sDescription' => 'Incapacidade de identificar a��es realizadas por usu�rios no sistema.',
  ),
  504 => 
  array (
    'fkContext' => '897',
    'fkCategory' => '139',
    'sDescription' => 'Incapacidade de identificar a��es realizadas por usu�rios no sistema.',
  ),
  505 => 
  array (
    'fkContext' => '899',
    'fkCategory' => '177',
    'sDescription' => 'Incapacidade de identificar exce��es �s atividades normais realizadas em sistemas.',
  ),
  506 => 
  array (
    'fkContext' => '901',
    'fkCategory' => '129',
    'sDescription' => 'Incapacidade de identificar exce��es �s atividades normais realizadas em sistemas.',
  ),
  507 => 
  array (
    'fkContext' => '902',
    'fkCategory' => '139',
    'sDescription' => 'Incapacidade de identificar exce��es �s atividades normais realizadas em sistemas.',
  ),
  508 => 
  array (
    'fkContext' => '904',
    'fkCategory' => '177',
    'sDescription' => 'Incapacidade de identificar eventos de seguran�a em sistemas.',
  ),
  509 => 
  array (
    'fkContext' => '905',
    'fkCategory' => '129',
    'sDescription' => 'Incapacidade de identificar eventos de seguran�a em sistemas.',
  ),
  510 => 
  array (
    'fkContext' => '907',
    'fkCategory' => '139',
    'sDescription' => 'Incapacidade de identificar eventos de seguran�a em sistemas.',
  ),
  511 => 
  array (
    'fkContext' => '909',
    'fkCategory' => '177',
    'sDescription' => 'Incapacidade de identificar tentativas de acesso n�o autorizado em sistemas.',
  ),
  512 => 
  array (
    'fkContext' => '910',
    'fkCategory' => '129',
    'sDescription' => 'Incapacidade de identificar tentativas de acesso n�o autorizado em sistemas.',
  ),
  513 => 
  array (
    'fkContext' => '912',
    'fkCategory' => '139',
    'sDescription' => 'Incapacidade de identificar tentativas de acesso n�o autorizado em sistemas.',
  ),
  514 => 
  array (
    'fkContext' => '914',
    'fkCategory' => '177',
    'sDescription' => 'Utiliza��o indevida do sistema.',
  ),
  515 => 
  array (
    'fkContext' => '915',
    'fkCategory' => '129',
    'sDescription' => 'Utiliza��o indevida do sistema.',
  ),
  516 => 
  array (
    'fkContext' => '917',
    'fkCategory' => '139',
    'sDescription' => 'Utiliza��o indevida do sistema.',
  ),
  517 => 
  array (
    'fkContext' => '919',
    'fkCategory' => '177',
    'sDescription' => 'Realiza��o de atividades n�o autorizadas no sistema.',
  ),
  518 => 
  array (
    'fkContext' => '920',
    'fkCategory' => '129',
    'sDescription' => 'Realiza��o de atividades n�o autorizadas no sistema.',
  ),
  519 => 
  array (
    'fkContext' => '922',
    'fkCategory' => '139',
    'sDescription' => 'Realiza��o de atividades n�o autorizadas no sistema.',
  ),
  520 => 
  array (
    'fkContext' => '924',
    'fkCategory' => '177',
    'sDescription' => 'Acesso n�o autorizado ao sistema.',
  ),
  521 => 
  array (
    'fkContext' => '925',
    'fkCategory' => '129',
    'sDescription' => 'Acesso n�o autorizado ao sistema.',
  ),
  522 => 
  array (
    'fkContext' => '927',
    'fkCategory' => '139',
    'sDescription' => 'Acesso n�o autorizado ao sistema.',
  ),
  523 => 
  array (
    'fkContext' => '929',
    'fkCategory' => '177',
    'sDescription' => 'Modifica��o n�o autorizada dos registros (logs) do sistema por usu�rios externos (hackers ou intrusos).',
  ),
  524 => 
  array (
    'fkContext' => '930',
    'fkCategory' => '129',
    'sDescription' => 'Modifica��o n�o autorizada dos registros (logs) do sistema por usu�rios externos (hackers ou intrusos).',
  ),
  525 => 
  array (
    'fkContext' => '932',
    'fkCategory' => '139',
    'sDescription' => 'Modifica��o n�o autorizada dos registros (logs) do sistema por usu�rios externos (hackers ou intrusos).',
  ),
  526 => 
  array (
    'fkContext' => '934',
    'fkCategory' => '177',
    'sDescription' => 'Modifica��o n�o autorizada dos registros (logs) do sistema por administradores ou usu�rios privilegiados.',
  ),
  527 => 
  array (
    'fkContext' => '935',
    'fkCategory' => '129',
    'sDescription' => 'Modifica��o n�o autorizada dos registros (logs) do sistema por administradores ou usu�rios privilegiados.',
  ),
  528 => 
  array (
    'fkContext' => '937',
    'fkCategory' => '139',
    'sDescription' => 'Modifica��o n�o autorizada dos registros (logs) do sistema por administradores ou usu�rios privilegiados.',
  ),
  529 => 
  array (
    'fkContext' => '939',
    'fkCategory' => '177',
    'sDescription' => 'Leitura n�o autorizada de registros (logs) do sistema.',
  ),
  530 => 
  array (
    'fkContext' => '940',
    'fkCategory' => '129',
    'sDescription' => 'Leitura n�o autorizada de registros (logs) do sistema.',
  ),
  531 => 
  array (
    'fkContext' => '942',
    'fkCategory' => '139',
    'sDescription' => 'Leitura n�o autorizada de registros (logs) do sistema.',
  ),
  532 => 
  array (
    'fkContext' => '944',
    'fkCategory' => '177',
    'sDescription' => 'Incapacidade de identificar atividades n�o autorizadas realizadas por usu�rios com privil�gios especiais como administradores e operadores.',
  ),
  533 => 
  array (
    'fkContext' => '945',
    'fkCategory' => '129',
    'sDescription' => 'Incapacidade de identificar atividades n�o autorizadas realizadas por usu�rios com privil�gios especiais como administradores e operadores.',
  ),
  534 => 
  array (
    'fkContext' => '947',
    'fkCategory' => '139',
    'sDescription' => 'Incapacidade de identificar atividades n�o autorizadas realizadas por usu�rios com privil�gios especiais como administradores e operadores.',
  ),
  535 => 
  array (
    'fkContext' => '949',
    'fkCategory' => '177',
    'sDescription' => 'Incapacidade de detectar erros e falhas no funcionamento dos sistemas por falta de registro dessas falhas.',
  ),
  536 => 
  array (
    'fkContext' => '950',
    'fkCategory' => '129',
    'sDescription' => 'Incapacidade de detectar erros e falhas no funcionamento dos sistemas por falta de registro dessas falhas.',
  ),
  537 => 
  array (
    'fkContext' => '952',
    'fkCategory' => '139',
    'sDescription' => 'Incapacidade de detectar erros e falhas no funcionamento dos sistemas por falta de registro dessas falhas.',
  ),
  538 => 
  array (
    'fkContext' => '954',
    'fkCategory' => '177',
    'sDescription' => 'Demora na restaura��o de controles de seguran�a ap�s falhas no sistema por falta de registro e identifica��o destas falhas.',
  ),
  539 => 
  array (
    'fkContext' => '955',
    'fkCategory' => '129',
    'sDescription' => 'Demora na restaura��o de controles de seguran�a ap�s falhas no sistema por falta de registro e identifica��o destas falhas.',
  ),
  540 => 
  array (
    'fkContext' => '957',
    'fkCategory' => '139',
    'sDescription' => 'Demora na restaura��o de controles de seguran�a ap�s falhas no sistema por falta de registro e identifica��o destas falhas.',
  ),
  541 => 
  array (
    'fkContext' => '959',
    'fkCategory' => '177',
    'sDescription' => 'Diferen�as entre os hor�rios apresentados em registros de sistemas e a hora real em que as a��es ocorreram.',
  ),
  542 => 
  array (
    'fkContext' => '960',
    'fkCategory' => '129',
    'sDescription' => 'Diferen�as entre os hor�rios apresentados em registros de sistemas e a hora real em que as a��es ocorreram.',
  ),
  543 => 
  array (
    'fkContext' => '962',
    'fkCategory' => '139',
    'sDescription' => 'Diferen�as entre os hor�rios apresentados em registros de sistemas e a hora real em que as a��es ocorreram.',
  ),
  544 => 
  array (
    'fkContext' => '964',
    'fkCategory' => '177',
    'sDescription' => 'Incapacidade de identificar a hora em que determinada a��o foi realizada por falta de padroniza��o na hora dos sistemas.',
  ),
  545 => 
  array (
    'fkContext' => '965',
    'fkCategory' => '129',
    'sDescription' => 'Incapacidade de identificar a hora em que determinada a��o foi realizada por falta de padroniza��o na hora dos sistemas.',
  ),
  546 => 
  array (
    'fkContext' => '967',
    'fkCategory' => '139',
    'sDescription' => 'Incapacidade de identificar a hora em que determinada a��o foi realizada por falta de padroniza��o na hora dos sistemas.',
  ),
  547 => 
  array (
    'fkContext' => '969',
    'fkCategory' => '177',
    'sDescription' => 'Falta de confiabilidade nas evid�ncias utilizadas para investiga��o forense, por n�o padroniza��o nos hor�rios dos sistemas.',
  ),
  548 => 
  array (
    'fkContext' => '970',
    'fkCategory' => '129',
    'sDescription' => 'Falta de confiabilidade nas evid�ncias utilizadas para investiga��o forense, por n�o padroniza��o nos hor�rios dos sistemas.',
  ),
  549 => 
  array (
    'fkContext' => '972',
    'fkCategory' => '139',
    'sDescription' => 'Falta de confiabilidade nas evid�ncias utilizadas para investiga��o forense, por n�o padroniza��o nos hor�rios dos sistemas.',
  ),
  550 => 
  array (
    'fkContext' => '974',
    'fkCategory' => '177',
    'sDescription' => 'Defini��o inadequada de direitos de acesso e privil�gios por falta de crit�rios formais para concess�o de permiss�es.',
  ),
  551 => 
  array (
    'fkContext' => '975',
    'fkCategory' => '129',
    'sDescription' => 'Defini��o inadequada de direitos de acesso e privil�gios por falta de crit�rios formais para concess�o de permiss�es.',
  ),
  552 => 
  array (
    'fkContext' => '977',
    'fkCategory' => '139',
    'sDescription' => 'Defini��o inadequada de direitos de acesso e privil�gios por falta de crit�rios formais para concess�o de permiss�es.',
  ),
  553 => 
  array (
    'fkContext' => '978',
    'fkCategory' => '183',
    'sDescription' => 'Defini��o inadequada de direitos de acesso e privil�gios por falta de crit�rios formais para concess�o de permiss�es.',
  ),
  554 => 
  array (
    'fkContext' => '980',
    'fkCategory' => '177',
    'sDescription' => 'Viola��o dos direitos de acesso por falta de diretrizes claras que determinem o que � permitido e o que � negado de forma geral.',
  ),
  555 => 
  array (
    'fkContext' => '981',
    'fkCategory' => '129',
    'sDescription' => 'Viola��o dos direitos de acesso por falta de diretrizes claras que determinem o que � permitido e o que � negado de forma geral.',
  ),
  556 => 
  array (
    'fkContext' => '983',
    'fkCategory' => '139',
    'sDescription' => 'Viola��o dos direitos de acesso por falta de diretrizes claras que determinem o que � permitido e o que � negado de forma geral.',
  ),
  557 => 
  array (
    'fkContext' => '984',
    'fkCategory' => '183',
    'sDescription' => 'Viola��o dos direitos de acesso por falta de diretrizes claras que determinem o que � permitido e o que � negado de forma geral.',
  ),
  558 => 
  array (
    'fkContext' => '986',
    'fkCategory' => '177',
    'sDescription' => 'Incompatibilidade entre os direitos de acesso concedidos e as reais necessidades de acesso baseadas nos requisitos de neg�cio.',
  ),
  559 => 
  array (
    'fkContext' => '987',
    'fkCategory' => '129',
    'sDescription' => 'Incompatibilidade entre os direitos de acesso concedidos e as reais necessidades de acesso baseadas nos requisitos de neg�cio.',
  ),
  560 => 
  array (
    'fkContext' => '989',
    'fkCategory' => '139',
    'sDescription' => 'Incompatibilidade entre os direitos de acesso concedidos e as reais necessidades de acesso baseadas nos requisitos de neg�cio.',
  ),
  561 => 
  array (
    'fkContext' => '990',
    'fkCategory' => '183',
    'sDescription' => 'Incompatibilidade entre os direitos de acesso concedidos e as reais necessidades de acesso baseadas nos requisitos de neg�cio.',
  ),
  562 => 
  array (
    'fkContext' => '992',
    'fkCategory' => '177',
    'sDescription' => 'Usu�rios com direitos de acesso desnecess�rios por causa de seu cargo hier�rquico.',
  ),
  563 => 
  array (
    'fkContext' => '993',
    'fkCategory' => '129',
    'sDescription' => 'Usu�rios com direitos de acesso desnecess�rios por causa de seu cargo hier�rquico.',
  ),
  564 => 
  array (
    'fkContext' => '995',
    'fkCategory' => '139',
    'sDescription' => 'Usu�rios com direitos de acesso desnecess�rios por causa de seu cargo hier�rquico.',
  ),
  565 => 
  array (
    'fkContext' => '996',
    'fkCategory' => '183',
    'sDescription' => 'Usu�rios com direitos de acesso desnecess�rios por causa de seu cargo hier�rquico.',
  ),
  566 => 
  array (
    'fkContext' => '997',
    'fkCategory' => '269',
    'sDescription' => 'Desconhecimento dos direitos de acesso por parte de terceiros, fornecedores e prestadores de servi�o.',
  ),
  567 => 
  array (
    'fkContext' => '998',
    'fkCategory' => '270',
    'sDescription' => 'Desconhecimento dos direitos de acesso por parte de terceiros, fornecedores e prestadores de servi�o.',
  ),
  568 => 
  array (
    'fkContext' => '999',
    'fkCategory' => '271',
    'sDescription' => 'Desconhecimento dos direitos de acesso por parte de terceiros, fornecedores e prestadores de servi�o.',
  ),
  569 => 
  array (
    'fkContext' => '1001',
    'fkCategory' => '177',
    'sDescription' => 'Incompatibilidade entre os controles de acesso definidos para os sistemas e as diretrizes de classifica��o das informa��es.',
  ),
  570 => 
  array (
    'fkContext' => '1002',
    'fkCategory' => '129',
    'sDescription' => 'Incompatibilidade entre os controles de acesso definidos para os sistemas e as diretrizes de classifica��o das informa��es.',
  ),
  571 => 
  array (
    'fkContext' => '1004',
    'fkCategory' => '139',
    'sDescription' => 'Incompatibilidade entre os controles de acesso definidos para os sistemas e as diretrizes de classifica��o das informa��es.',
  ),
  572 => 
  array (
    'fkContext' => '1006',
    'fkCategory' => '177',
    'sDescription' => 'Concess�o inadequada de direitos de acesso.',
  ),
  573 => 
  array (
    'fkContext' => '1007',
    'fkCategory' => '129',
    'sDescription' => 'Concess�o inadequada de direitos de acesso.',
  ),
  574 => 
  array (
    'fkContext' => '1009',
    'fkCategory' => '139',
    'sDescription' => 'Concess�o inadequada de direitos de acesso.',
  ),
  575 => 
  array (
    'fkContext' => '1011',
    'fkCategory' => '177',
    'sDescription' => 'Impossibilidade de responsabilizar os usu�rios por suas atividades devido a falta de identificadores �nicos.',
  ),
  576 => 
  array (
    'fkContext' => '1012',
    'fkCategory' => '129',
    'sDescription' => 'Impossibilidade de responsabilizar os usu�rios por suas atividades devido a falta de identificadores �nicos.',
  ),
  577 => 
  array (
    'fkContext' => '1014',
    'fkCategory' => '139',
    'sDescription' => 'Impossibilidade de responsabilizar os usu�rios por suas atividades devido a falta de identificadores �nicos.',
  ),
  578 => 
  array (
    'fkContext' => '1016',
    'fkCategory' => '177',
    'sDescription' => 'Exist�ncia de usu�rios com direitos de acesso desnecess�rios por falta de um processo claro de revoga��o dos direitos em situa��es de aus�ncia ou cancelamento de contrato.',
  ),
  579 => 
  array (
    'fkContext' => '1017',
    'fkCategory' => '129',
    'sDescription' => 'Exist�ncia de usu�rios com direitos de acesso desnecess�rios por falta de um processo claro de revoga��o dos direitos em situa��es de aus�ncia ou cancelamento de contrato.',
  ),
  580 => 
  array (
    'fkContext' => '1019',
    'fkCategory' => '139',
    'sDescription' => 'Exist�ncia de usu�rios com direitos de acesso desnecess�rios por falta de um processo claro de revoga��o dos direitos em situa��es de aus�ncia ou cancelamento de contrato.',
  ),
  581 => 
  array (
    'fkContext' => '1021',
    'fkCategory' => '177',
    'sDescription' => 'Concess�o de n�veis de acesso incompat�veis com os prop�sitos de neg�cio.',
  ),
  582 => 
  array (
    'fkContext' => '1022',
    'fkCategory' => '129',
    'sDescription' => 'Concess�o de n�veis de acesso incompat�veis com os prop�sitos de neg�cio.',
  ),
  583 => 
  array (
    'fkContext' => '1024',
    'fkCategory' => '139',
    'sDescription' => 'Concess�o de n�veis de acesso incompat�veis com os prop�sitos de neg�cio.',
  ),
  584 => 
  array (
    'fkContext' => '1026',
    'fkCategory' => '177',
    'sDescription' => 'Acessos n�o autorizados por falta de formaliza��o das permiss�es dos usu�rios.',
  ),
  585 => 
  array (
    'fkContext' => '1027',
    'fkCategory' => '129',
    'sDescription' => 'Acessos n�o autorizados por falta de formaliza��o das permiss�es dos usu�rios.',
  ),
  586 => 
  array (
    'fkContext' => '1029',
    'fkCategory' => '139',
    'sDescription' => 'Acessos n�o autorizados por falta de formaliza��o das permiss�es dos usu�rios.',
  ),
  587 => 
  array (
    'fkContext' => '1031',
    'fkCategory' => '177',
    'sDescription' => 'Exist�ncia de identificadores e usu�rios duplicados.',
  ),
  588 => 
  array (
    'fkContext' => '1032',
    'fkCategory' => '129',
    'sDescription' => 'Exist�ncia de identificadores e usu�rios duplicados.',
  ),
  589 => 
  array (
    'fkContext' => '1034',
    'fkCategory' => '139',
    'sDescription' => 'Exist�ncia de identificadores e usu�rios duplicados.',
  ),
  590 => 
  array (
    'fkContext' => '1036',
    'fkCategory' => '177',
    'sDescription' => 'Uso inadequado de privil�gios de acesso.',
  ),
  591 => 
  array (
    'fkContext' => '1037',
    'fkCategory' => '129',
    'sDescription' => 'Uso inadequado de privil�gios de acesso.',
  ),
  592 => 
  array (
    'fkContext' => '1039',
    'fkCategory' => '139',
    'sDescription' => 'Uso inadequado de privil�gios de acesso.',
  ),
  593 => 
  array (
    'fkContext' => '1041',
    'fkCategory' => '177',
    'sDescription' => 'Concess�o de privil�gios a usu�rios que n�o necessitam deste perfil.',
  ),
  594 => 
  array (
    'fkContext' => '1042',
    'fkCategory' => '129',
    'sDescription' => 'Concess�o de privil�gios a usu�rios que n�o necessitam deste perfil.',
  ),
  595 => 
  array (
    'fkContext' => '1044',
    'fkCategory' => '139',
    'sDescription' => 'Concess�o de privil�gios a usu�rios que n�o necessitam deste perfil.',
  ),
  596 => 
  array (
    'fkContext' => '1046',
    'fkCategory' => '177',
    'sDescription' => 'Concess�o de privil�gios sem rela��o com identificadores pessoais, impossibilitando a identifica��o e monitoramento de atividades nos sistemas.',
  ),
  597 => 
  array (
    'fkContext' => '1047',
    'fkCategory' => '129',
    'sDescription' => 'Concess�o de privil�gios sem rela��o com identificadores pessoais, impossibilitando a identifica��o e monitoramento de atividades nos sistemas.',
  ),
  598 => 
  array (
    'fkContext' => '1049',
    'fkCategory' => '139',
    'sDescription' => 'Concess�o de privil�gios sem rela��o com identificadores pessoais, impossibilitando a identifica��o e monitoramento de atividades nos sistemas.',
  ),
  599 => 
  array (
    'fkContext' => '1051',
    'fkCategory' => '177',
    'sDescription' => 'Uso irrestrito de usu�rios superiores em atividades onde n�o seria necess�rio este tipo de privil�gio.',
  ),
  600 => 
  array (
    'fkContext' => '1052',
    'fkCategory' => '129',
    'sDescription' => 'Uso irrestrito de usu�rios superiores em atividades onde n�o seria necess�rio este tipo de privil�gio.',
  ),
  601 => 
  array (
    'fkContext' => '1054',
    'fkCategory' => '139',
    'sDescription' => 'Uso irrestrito de usu�rios superiores em atividades onde n�o seria necess�rio este tipo de privil�gio.',
  ),
  602 => 
  array (
    'fkContext' => '1056',
    'fkCategory' => '177',
    'sDescription' => 'Comprometimento de senhas durante o processo de entrega para os usu�rios.',
  ),
  603 => 
  array (
    'fkContext' => '1057',
    'fkCategory' => '129',
    'sDescription' => 'Comprometimento de senhas durante o processo de entrega para os usu�rios.',
  ),
  604 => 
  array (
    'fkContext' => '1059',
    'fkCategory' => '139',
    'sDescription' => 'Comprometimento de senhas durante o processo de entrega para os usu�rios.',
  ),
  605 => 
  array (
    'fkContext' => '1061',
    'fkCategory' => '177',
    'sDescription' => 'Acesso a sistemas atrav�s de senhas tempor�rias de conhecimento geral.',
  ),
  606 => 
  array (
    'fkContext' => '1062',
    'fkCategory' => '129',
    'sDescription' => 'Acesso a sistemas atrav�s de senhas tempor�rias de conhecimento geral.',
  ),
  607 => 
  array (
    'fkContext' => '1064',
    'fkCategory' => '139',
    'sDescription' => 'Acesso a sistemas atrav�s de senhas tempor�rias de conhecimento geral.',
  ),
  608 => 
  array (
    'fkContext' => '1066',
    'fkCategory' => '177',
    'sDescription' => 'Conhecimento das senhas dos usu�rios por respons�veis pelo gerenciamento da senha.',
  ),
  609 => 
  array (
    'fkContext' => '1067',
    'fkCategory' => '129',
    'sDescription' => 'Conhecimento das senhas dos usu�rios por respons�veis pelo gerenciamento da senha.',
  ),
  610 => 
  array (
    'fkContext' => '1069',
    'fkCategory' => '139',
    'sDescription' => 'Conhecimento das senhas dos usu�rios por respons�veis pelo gerenciamento da senha.',
  ),
  611 => 
  array (
    'fkContext' => '1071',
    'fkCategory' => '177',
    'sDescription' => 'Exist�ncia de direitos de acesso inadequados por falhas na atualiza��o ap�s mudan�as de cargo ou fun��o de usu�rios.',
  ),
  612 => 
  array (
    'fkContext' => '1072',
    'fkCategory' => '129',
    'sDescription' => 'Exist�ncia de direitos de acesso inadequados por falhas na atualiza��o ap�s mudan�as de cargo ou fun��o de usu�rios.',
  ),
  613 => 
  array (
    'fkContext' => '1074',
    'fkCategory' => '139',
    'sDescription' => 'Exist�ncia de direitos de acesso inadequados por falhas na atualiza��o ap�s mudan�as de cargo ou fun��o de usu�rios.',
  ),
  614 => 
  array (
    'fkContext' => '1076',
    'fkCategory' => '177',
    'sDescription' => 'Exist�ncia de direitos de acesso n�o autorizados concedidos a usu�rios dos sistemas.',
  ),
  615 => 
  array (
    'fkContext' => '1077',
    'fkCategory' => '129',
    'sDescription' => 'Exist�ncia de direitos de acesso n�o autorizados concedidos a usu�rios dos sistemas.',
  ),
  616 => 
  array (
    'fkContext' => '1079',
    'fkCategory' => '139',
    'sDescription' => 'Exist�ncia de direitos de acesso n�o autorizados concedidos a usu�rios dos sistemas.',
  ),
  617 => 
  array (
    'fkContext' => '1081',
    'fkCategory' => '177',
    'sDescription' => 'Exist�ncia de direitos de acesso que n�o foram removidos ap�s desligamento de usu�rios.',
  ),
  618 => 
  array (
    'fkContext' => '1082',
    'fkCategory' => '129',
    'sDescription' => 'Exist�ncia de direitos de acesso que n�o foram removidos ap�s desligamento de usu�rios.',
  ),
  619 => 
  array (
    'fkContext' => '1084',
    'fkCategory' => '139',
    'sDescription' => 'Exist�ncia de direitos de acesso que n�o foram removidos ap�s desligamento de usu�rios.',
  ),
  620 => 
  array (
    'fkContext' => '1086',
    'fkCategory' => '177',
    'sDescription' => 'Exist�ncia de privil�gios n�o autorizados concedidos a usu�rios dos sistemas.',
  ),
  621 => 
  array (
    'fkContext' => '1087',
    'fkCategory' => '129',
    'sDescription' => 'Exist�ncia de privil�gios n�o autorizados concedidos a usu�rios dos sistemas.',
  ),
  622 => 
  array (
    'fkContext' => '1089',
    'fkCategory' => '139',
    'sDescription' => 'Exist�ncia de privil�gios n�o autorizados concedidos a usu�rios dos sistemas.',
  ),
  623 => 
  array (
    'fkContext' => '1091',
    'fkCategory' => '177',
    'sDescription' => 'Divulga��o de senhas para outros usu�rios pelos propriet�rios das senhas.',
  ),
  624 => 
  array (
    'fkContext' => '1092',
    'fkCategory' => '129',
    'sDescription' => 'Divulga��o de senhas para outros usu�rios pelos propriet�rios das senhas.',
  ),
  625 => 
  array (
    'fkContext' => '1094',
    'fkCategory' => '139',
    'sDescription' => 'Divulga��o de senhas para outros usu�rios pelos propriet�rios das senhas.',
  ),
  626 => 
  array (
    'fkContext' => '1096',
    'fkCategory' => '177',
    'sDescription' => 'Divulga��o de senhas por falta de cuidado dos usu�rios.',
  ),
  627 => 
  array (
    'fkContext' => '1097',
    'fkCategory' => '129',
    'sDescription' => 'Divulga��o de senhas por falta de cuidado dos usu�rios.',
  ),
  628 => 
  array (
    'fkContext' => '1099',
    'fkCategory' => '139',
    'sDescription' => 'Divulga��o de senhas por falta de cuidado dos usu�rios.',
  ),
  629 => 
  array (
    'fkContext' => '1101',
    'fkCategory' => '177',
    'sDescription' => 'Acesso n�o autorizado aos sistemas atrav�s de usu�rios que selecionaram senhas de baixa complexidade.',
  ),
  630 => 
  array (
    'fkContext' => '1102',
    'fkCategory' => '129',
    'sDescription' => 'Acesso n�o autorizado aos sistemas atrav�s de usu�rios que selecionaram senhas de baixa complexidade.',
  ),
  631 => 
  array (
    'fkContext' => '1104',
    'fkCategory' => '139',
    'sDescription' => 'Acesso n�o autorizado aos sistemas atrav�s de usu�rios que selecionaram senhas de baixa complexidade.',
  ),
  632 => 
  array (
    'fkContext' => '1106',
    'fkCategory' => '177',
    'sDescription' => 'Utiliza��o de uma mesma senha por per�odo de tempo prolongado.',
  ),
  633 => 
  array (
    'fkContext' => '1107',
    'fkCategory' => '129',
    'sDescription' => 'Utiliza��o de uma mesma senha por per�odo de tempo prolongado.',
  ),
  634 => 
  array (
    'fkContext' => '1109',
    'fkCategory' => '139',
    'sDescription' => 'Utiliza��o de uma mesma senha por per�odo de tempo prolongado.',
  ),
  635 => 
  array (
    'fkContext' => '1111',
    'fkCategory' => '177',
    'sDescription' => 'Comprometimento de senhas armazenadas em processos autom�ticos como macros ou fun��es de sistemas.',
  ),
  636 => 
  array (
    'fkContext' => '1112',
    'fkCategory' => '129',
    'sDescription' => 'Comprometimento de senhas armazenadas em processos autom�ticos como macros ou fun��es de sistemas.',
  ),
  637 => 
  array (
    'fkContext' => '1114',
    'fkCategory' => '139',
    'sDescription' => 'Comprometimento de senhas armazenadas em processos autom�ticos como macros ou fun��es de sistemas.',
  ),
  638 => 
  array (
    'fkContext' => '1115',
    'fkCategory' => '213',
    'sDescription' => 'Acesso n�o autorizado atrav�s de equipamentos n�o monitorados.',
  ),
  639 => 
  array (
    'fkContext' => '1116',
    'fkCategory' => '213',
    'sDescription' => 'Acesso n�o autorizado a equipamentos durante curtos per�odos de aus�ncia de funcion�rios.',
  ),
  640 => 
  array (
    'fkContext' => '1117',
    'fkCategory' => '213',
    'sDescription' => 'Acesso n�o autorizado a equipamentos durante hor�rios em que n�o h� expediente.',
  ),
  641 => 
  array (
    'fkContext' => '1118',
    'fkCategory' => '213',
    'sDescription' => 'Falta de dispositivos que permitam bloquear equipamentos durante aus�ncia.',
  ),
  642 => 
  array (
    'fkContext' => '1119',
    'fkCategory' => '115',
    'sDescription' => 'Acesso n�o autorizado a informa��es sens�veis deixadas sobre mesas.',
  ),
  643 => 
  array (
    'fkContext' => '1120',
    'fkCategory' => '101',
    'sDescription' => 'Acesso n�o autorizado a informa��es sens�veis atrav�s da leitura de telas de computadores de forma indevida.',
  ),
  644 => 
  array (
    'fkContext' => '1121',
    'fkCategory' => '225',
    'sDescription' => 'Recebimento de impress�es por usu�rios n�o autorizados.',
  ),
  645 => 
  array (
    'fkContext' => '1122',
    'fkCategory' => '236',
    'sDescription' => 'Concess�o inadequada de direitos para acesso aos servi�os de rede.',
  ),
  646 => 
  array (
    'fkContext' => '1123',
    'fkCategory' => '237',
    'sDescription' => 'Concess�o inadequada de direitos para acesso aos servi�os de rede.',
  ),
  647 => 
  array (
    'fkContext' => '1124',
    'fkCategory' => '236',
    'sDescription' => 'Concess�o de acesso �s redes de forma incompat�vel com a pol�tica de controle de acesso.',
  ),
  648 => 
  array (
    'fkContext' => '1125',
    'fkCategory' => '237',
    'sDescription' => 'Concess�o de acesso �s redes de forma incompat�vel com a pol�tica de controle de acesso.',
  ),
  649 => 
  array (
    'fkContext' => '1126',
    'fkCategory' => '236',
    'sDescription' => 'Acesso discado n�o autorizado a outras redes.',
  ),
  650 => 
  array (
    'fkContext' => '1127',
    'fkCategory' => '237',
    'sDescription' => 'Acesso remoto n�o autorizado �s redes internas.',
  ),
  651 => 
  array (
    'fkContext' => '1128',
    'fkCategory' => '236',
    'sDescription' => 'Conex�o n�o autorizada de equipamentos � rede.',
  ),
  652 => 
  array (
    'fkContext' => '1129',
    'fkCategory' => '237',
    'sDescription' => 'Acesso remoto n�o autorizado por usu�rios da rede.',
  ),
  653 => 
  array (
    'fkContext' => '1130',
    'fkCategory' => '236',
    'sDescription' => 'Acesso n�o autorizado a redes sem fio.',
  ),
  654 => 
  array (
    'fkContext' => '1131',
    'fkCategory' => '237',
    'sDescription' => 'Acesso remoto n�o autorizado por atacantes (hackers).',
  ),
  655 => 
  array (
    'fkContext' => '1132',
    'fkCategory' => '236',
    'sDescription' => 'Conex�o de equipamentos n�o autorizados na rede.',
  ),
  656 => 
  array (
    'fkContext' => '1133',
    'fkCategory' => '236',
    'sDescription' => 'Acesso a rede atrav�s de falsifica��o da identidade de equipamentos espec�ficos que possuem autoriza��o para acesso.',
  ),
  657 => 
  array (
    'fkContext' => '1134',
    'fkCategory' => '213',
    'sDescription' => 'Acesso n�o autorizado a equipamentos atrav�s de acesso l�gico a portas de diagn�stico remotas.',
  ),
  658 => 
  array (
    'fkContext' => '1135',
    'fkCategory' => '213',
    'sDescription' => 'Acesso n�o autorizado a equipamentos atrav�s de acesso f�sico n�o autorizado a portas de diagn�stico.',
  ),
  659 => 
  array (
    'fkContext' => '1136',
    'fkCategory' => '213',
    'sDescription' => 'Acesso n�o autorizado durante manuten��es realizadas atrav�s de portas de diagn�stico e suporte remoto.',
  ),
  660 => 
  array (
    'fkContext' => '1137',
    'fkCategory' => '236',
    'sDescription' => 'Acesso n�o autorizado atrav�s da rede a servi�os e servidores cr�ticos.',
  ),
  661 => 
  array (
    'fkContext' => '1138',
    'fkCategory' => '236',
    'sDescription' => 'Acesso n�o autorizado de usu�rios externos � rede interna atrav�s de servi�os e servidores dispon�veis publicamente.',
  ),
  662 => 
  array (
    'fkContext' => '1139',
    'fkCategory' => '236',
    'sDescription' => 'Acesso n�o autorizado a informa��es sens�veis que trafegam em redes comuns.',
  ),
  663 => 
  array (
    'fkContext' => '1140',
    'fkCategory' => '236',
    'sDescription' => 'Acesso n�o autorizado a servi�os que processam informa��es sens�veis dispon�veis em redes comuns.',
  ),
  664 => 
  array (
    'fkContext' => '1141',
    'fkCategory' => '236',
    'sDescription' => 'Acesso a informa��es sens�veis que trafegam em redes de forma desnecess�ria.',
  ),
  665 => 
  array (
    'fkContext' => '1142',
    'fkCategory' => '236',
    'sDescription' => 'Incapacidade de filtrar informa��es que s�o transmitidas entre redes compartilhadas.',
  ),
  666 => 
  array (
    'fkContext' => '1143',
    'fkCategory' => '236',
    'sDescription' => 'Acesso n�o autorizado a rede interna atrav�s de redes p�blicas, como a Internet.',
  ),
  667 => 
  array (
    'fkContext' => '1144',
    'fkCategory' => '236',
    'sDescription' => 'Acesso n�o autorizado de usu�rios de uma rede a informa��es de outras redes.',
  ),
  668 => 
  array (
    'fkContext' => '1145',
    'fkCategory' => '236',
    'sDescription' => 'Ataques a rede interna e servidores atrav�s de redes p�blicas como a Internet.',
  ),
  669 => 
  array (
    'fkContext' => '1146',
    'fkCategory' => '236',
    'sDescription' => 'Ataques a rede interna e servidores atrav�s de redes interconectadas.',
  ),
  670 => 
  array (
    'fkContext' => '1148',
    'fkCategory' => '177',
    'sDescription' => 'Acesso externo n�o autorizado a aplica��es e servi�os dispon�veis exclusivamente a usu�rios da rede interna.',
  ),
  671 => 
  array (
    'fkContext' => '1149',
    'fkCategory' => '129',
    'sDescription' => 'Acesso externo n�o autorizado a aplica��es e servi�os dispon�veis exclusivamente a usu�rios da rede interna.',
  ),
  672 => 
  array (
    'fkContext' => '1151',
    'fkCategory' => '139',
    'sDescription' => 'Acesso externo n�o autorizado a aplica��es e servi�os dispon�veis exclusivamente a usu�rios da rede interna.',
  ),
  673 => 
  array (
    'fkContext' => '1152',
    'fkCategory' => '236',
    'sDescription' => 'Ataques externos de nega��o de servi�o.',
  ),
  674 => 
  array (
    'fkContext' => '1153',
    'fkCategory' => '236',
    'sDescription' => 'Ataques internos de nega��o de servi�o.',
  ),
  675 => 
  array (
    'fkContext' => '1154',
    'fkCategory' => '236',
    'sDescription' => 'Acesso n�o autorizado a redes atrav�s de falhas de roteamento.',
  ),
  676 => 
  array (
    'fkContext' => '1155',
    'fkCategory' => '236',
    'sDescription' => 'Falha de acesso a rede externa por falhas de roteamento.',
  ),
  677 => 
  array (
    'fkContext' => '1156',
    'fkCategory' => '236',
    'sDescription' => 'Falha de acesso a rede interna por falhas de roteamento.',
  ),
  678 => 
  array (
    'fkContext' => '1157',
    'fkCategory' => '236',
    'sDescription' => 'Acesso n�o autorizado a equipamentos na rede interna por m� configura��o do endere�amento de redes.',
  ),
  679 => 
  array (
    'fkContext' => '1158',
    'fkCategory' => '236',
    'sDescription' => 'Utiliza��o n�o autorizada de dispositivos de roteamento.',
  ),
  680 => 
  array (
    'fkContext' => '1159',
    'fkCategory' => '236',
    'sDescription' => 'Roteamento inadequado de informa��es transmitidas atrav�s de redes internas e externas.',
  ),
  681 => 
  array (
    'fkContext' => '1160',
    'fkCategory' => '129',
    'sDescription' => 'Acesso n�o autorizado aos sistemas operacionais por falhas no processo de valida��o da entrada de usu�rios.',
  ),
  682 => 
  array (
    'fkContext' => '1161',
    'fkCategory' => '129',
    'sDescription' => 'Fornecimento de informa��es sobre o sistema pelos mecanismos de controle de entrada no sistema.',
  ),
  683 => 
  array (
    'fkContext' => '1162',
    'fkCategory' => '129',
    'sDescription' => 'Captura de senhas enviadas atrav�s da rede pelos mecanismos de valida��o de entrada nos sistemas.',
  ),
  684 => 
  array (
    'fkContext' => '1164',
    'fkCategory' => '177',
    'sDescription' => 'Acesso n�o autorizado a sistemas atrav�s de falhas no processo de autentica��o.',
  ),
  685 => 
  array (
    'fkContext' => '1165',
    'fkCategory' => '129',
    'sDescription' => 'Acesso n�o autorizado a sistemas atrav�s de falhas no processo de autentica��o.',
  ),
  686 => 
  array (
    'fkContext' => '1167',
    'fkCategory' => '139',
    'sDescription' => 'Acesso n�o autorizado a sistemas atrav�s de falhas no processo de autentica��o.',
  ),
  687 => 
  array (
    'fkContext' => '1169',
    'fkCategory' => '177',
    'sDescription' => 'Falta de utiliza��o de identificadores �nicos e pessoais, n�o permitindo a atribui��o de responsabilidades.',
  ),
  688 => 
  array (
    'fkContext' => '1170',
    'fkCategory' => '129',
    'sDescription' => 'Falta de utiliza��o de identificadores �nicos e pessoais, n�o permitindo a atribui��o de responsabilidades.',
  ),
  689 => 
  array (
    'fkContext' => '1172',
    'fkCategory' => '139',
    'sDescription' => 'Falta de utiliza��o de identificadores �nicos e pessoais, n�o permitindo a atribui��o de responsabilidades.',
  ),
  690 => 
  array (
    'fkContext' => '1174',
    'fkCategory' => '177',
    'sDescription' => 'Dificuldade de rastreabilidade de atividades devido o uso de identificadores compartilhados.',
  ),
  691 => 
  array (
    'fkContext' => '1175',
    'fkCategory' => '129',
    'sDescription' => 'Dificuldade de rastreabilidade de atividades devido o uso de identificadores compartilhados.',
  ),
  692 => 
  array (
    'fkContext' => '1177',
    'fkCategory' => '139',
    'sDescription' => 'Dificuldade de rastreabilidade de atividades devido o uso de identificadores compartilhados.',
  ),
  693 => 
  array (
    'fkContext' => '1179',
    'fkCategory' => '177',
    'sDescription' => 'Utiliza��o de senhas de baixa complexidade.',
  ),
  694 => 
  array (
    'fkContext' => '1180',
    'fkCategory' => '129',
    'sDescription' => 'Utiliza��o de senhas de baixa complexidade.',
  ),
  695 => 
  array (
    'fkContext' => '1182',
    'fkCategory' => '139',
    'sDescription' => 'Utiliza��o de senhas de baixa complexidade.',
  ),
  696 => 
  array (
    'fkContext' => '1184',
    'fkCategory' => '177',
    'sDescription' => 'Divulga��o n�o autorizada de senhas aos administradores ou operadores de suporte quando necess�rio realizar a mudan�a de senhas.',
  ),
  697 => 
  array (
    'fkContext' => '1185',
    'fkCategory' => '129',
    'sDescription' => 'Divulga��o n�o autorizada de senhas aos administradores ou operadores de suporte quando necess�rio realizar a mudan�a de senhas.',
  ),
  698 => 
  array (
    'fkContext' => '1187',
    'fkCategory' => '139',
    'sDescription' => 'Divulga��o n�o autorizada de senhas aos administradores ou operadores de suporte quando necess�rio realizar a mudan�a de senhas.',
  ),
  699 => 
  array (
    'fkContext' => '1189',
    'fkCategory' => '177',
    'sDescription' => 'Impossibilidade de garantir a aplica��o de boas pr�ticas de seguran�a da informa��o pelos usu�rios da informa��o.',
  ),
  700 => 
  array (
    'fkContext' => '1190',
    'fkCategory' => '129',
    'sDescription' => 'Impossibilidade de garantir a aplica��o de boas pr�ticas de seguran�a da informa��o pelos usu�rios da informa��o.',
  ),
  701 => 
  array (
    'fkContext' => '1192',
    'fkCategory' => '139',
    'sDescription' => 'Impossibilidade de garantir a aplica��o de boas pr�ticas de seguran�a da informa��o pelos usu�rios da informa��o.',
  ),
  702 => 
  array (
    'fkContext' => '1194',
    'fkCategory' => '177',
    'sDescription' => 'Viola��o dos controles de seguran�a dos sistemas atrav�s de utilit�rios especiais.',
  ),
  703 => 
  array (
    'fkContext' => '1195',
    'fkCategory' => '129',
    'sDescription' => 'Viola��o dos controles de seguran�a dos sistemas atrav�s de utilit�rios especiais.',
  ),
  704 => 
  array (
    'fkContext' => '1197',
    'fkCategory' => '139',
    'sDescription' => 'Viola��o dos controles de seguran�a dos sistemas atrav�s de utilit�rios especiais.',
  ),
  705 => 
  array (
    'fkContext' => '1199',
    'fkCategory' => '177',
    'sDescription' => 'Uso de utilit�rios de sistema por usu�rios n�o autorizados.',
  ),
  706 => 
  array (
    'fkContext' => '1200',
    'fkCategory' => '129',
    'sDescription' => 'Uso de utilit�rios de sistema por usu�rios n�o autorizados.',
  ),
  707 => 
  array (
    'fkContext' => '1202',
    'fkCategory' => '139',
    'sDescription' => 'Uso de utilit�rios de sistema por usu�rios n�o autorizados.',
  ),
  708 => 
  array (
    'fkContext' => '1203',
    'fkCategory' => '213',
    'sDescription' => 'Acesso n�o autorizado a equipamentos durante aus�ncia do usu�rio.',
  ),
  709 => 
  array (
    'fkContext' => '1205',
    'fkCategory' => '177',
    'sDescription' => 'Acesso n�o autorizado ao sistema fora do hor�rio comercial.',
  ),
  710 => 
  array (
    'fkContext' => '1206',
    'fkCategory' => '129',
    'sDescription' => 'Acesso n�o autorizado ao sistema fora do hor�rio comercial.',
  ),
  711 => 
  array (
    'fkContext' => '1208',
    'fkCategory' => '139',
    'sDescription' => 'Acesso n�o autorizado ao sistema fora do hor�rio comercial.',
  ),
  712 => 
  array (
    'fkContext' => '1210',
    'fkCategory' => '177',
    'sDescription' => 'Acesso n�o autorizado ao sistema durante per�odo em que o seu uso � desnecess�rio ou n�o autorizado.',
  ),
  713 => 
  array (
    'fkContext' => '1211',
    'fkCategory' => '129',
    'sDescription' => 'Acesso n�o autorizado ao sistema durante per�odo em que o seu uso � desnecess�rio ou n�o autorizado.',
  ),
  714 => 
  array (
    'fkContext' => '1213',
    'fkCategory' => '139',
    'sDescription' => 'Acesso n�o autorizado ao sistema durante per�odo em que o seu uso � desnecess�rio ou n�o autorizado.',
  ),
  715 => 
  array (
    'fkContext' => '1215',
    'fkCategory' => '177',
    'sDescription' => 'Acesso n�o autorizado a informa��es e fun��es de sistemas por incapacidade de restri��o de acesso de acordo com o perfil do usu�rio e a pol�tica de controle de acesso.',
  ),
  716 => 
  array (
    'fkContext' => '1216',
    'fkCategory' => '129',
    'sDescription' => 'Acesso n�o autorizado a informa��es e fun��es de sistemas por incapacidade de restri��o de acesso de acordo com o perfil do usu�rio e a pol�tica de controle de acesso.',
  ),
  717 => 
  array (
    'fkContext' => '1219',
    'fkCategory' => '177',
    'sDescription' => 'Envio n�o autorizado de informa��es para dispositivos de sa�da pelos sistemas por incapacidade de filtrar tais informa��es.',
  ),
  718 => 
  array (
    'fkContext' => '1220',
    'fkCategory' => '129',
    'sDescription' => 'Envio n�o autorizado de informa��es para dispositivos de sa�da pelos sistemas por incapacidade de filtrar tais informa��es.',
  ),
  719 => 
  array (
    'fkContext' => '1223',
    'fkCategory' => '177',
    'sDescription' => 'Comprometimento de sistemas por falhas de outros sistemas que compartilham o mesmo ambiente computacional.',
  ),
  720 => 
  array (
    'fkContext' => '1224',
    'fkCategory' => '129',
    'sDescription' => 'Comprometimento de sistemas por falhas de outros sistemas que compartilham o mesmo ambiente computacional.',
  ),
  721 => 
  array (
    'fkContext' => '1227',
    'fkCategory' => '177',
    'sDescription' => 'Comprometimento de diversos sistemas cr�ticos por falhas do ambiente computacional onde estes sistemas est�o instalados.',
  ),
  722 => 
  array (
    'fkContext' => '1228',
    'fkCategory' => '129',
    'sDescription' => 'Comprometimento de diversos sistemas cr�ticos por falhas do ambiente computacional onde estes sistemas est�o instalados.',
  ),
  723 => 
  array (
    'fkContext' => '1230',
    'fkCategory' => '213',
    'sDescription' => 'Acesso n�o autorizado a informa��es armazenadas em computadores m�veis (notebooks, palmtops, celulares) devido roubo do equipamento.',
  ),
  724 => 
  array (
    'fkContext' => '1233',
    'fkCategory' => '213',
    'sDescription' => 'Contamina��o de computadores m�veis por c�digo malicioso.',
  ),
  725 => 
  array (
    'fkContext' => '1234',
    'fkCategory' => '213',
    'sDescription' => 'Perda de informa��es sens�veis por perda de computadores m�veis.',
  ),
  726 => 
  array (
    'fkContext' => '1235',
    'fkCategory' => '213',
    'sDescription' => 'Roubo de computadores m�veis.',
  ),
  727 => 
  array (
    'fkContext' => '1236',
    'fkCategory' => '237',
    'sDescription' => 'Comprometimento de informa��es acessadas remotamente por falta de seguran�a do ambiente remoto.',
  ),
  728 => 
  array (
    'fkContext' => '1237',
    'fkCategory' => '237',
    'sDescription' => 'Acesso remoto n�o autorizado ao ambiente operacional atrav�s de locais de trabalho remoto desprotegidos.',
  ),
  729 => 
  array (
    'fkContext' => '1238',
    'fkCategory' => '237',
    'sDescription' => 'Roubo de equipamentos e informa��es nos locais de trabalho remoto.',
  ),
  730 => 
  array (
    'fkContext' => '1239',
    'fkCategory' => '237',
    'sDescription' => 'Intercepta��o de informa��es transmitidas e recebidas durante trabalho remoto.',
  ),
  731 => 
  array (
    'fkContext' => '1240',
    'fkCategory' => '237',
    'sDescription' => 'Comprometimento de informa��es por acesso irrestrito a equipamentos utilizados para trabalho remoto.',
  ),
  732 => 
  array (
    'fkContext' => '1241',
    'fkCategory' => '237',
    'sDescription' => 'Uso de equipamentos particulares para realiza��o de trabalho remoto.',
  ),
  733 => 
  array (
    'fkContext' => '1243',
    'fkCategory' => '177',
    'sDescription' => 'Falha ou falta de especifica��o de requisitos de seguran�a quando do desenvolvimento do sistema.',
  ),
  734 => 
  array (
    'fkContext' => '1246',
    'fkCategory' => '129',
    'sDescription' => 'Aquisi��o de softwares com controles de seguran�a insuficientes.',
  ),
  735 => 
  array (
    'fkContext' => '1247',
    'fkCategory' => '163',
    'sDescription' => 'Aquisi��o de softwares com controles de seguran�a insuficientes.',
  ),
  736 => 
  array (
    'fkContext' => '1248',
    'fkCategory' => '177',
    'sDescription' => 'Aquisi��o de softwares com controles de seguran�a insuficientes.',
  ),
  737 => 
  array (
    'fkContext' => '1250',
    'fkCategory' => '129',
    'sDescription' => 'Aus�ncia de processo formal de aquisi��o e testes de softwares.',
  ),
  738 => 
  array (
    'fkContext' => '1252',
    'fkCategory' => '177',
    'sDescription' => 'Aus�ncia de processo formal de aquisi��o e testes de softwares.',
  ),
  739 => 
  array (
    'fkContext' => '1254',
    'fkCategory' => '139',
    'sDescription' => 'Aus�ncia de processo formal de aquisi��o e testes de softwares.',
  ),
  740 => 
  array (
    'fkContext' => '1255',
    'fkCategory' => '163',
    'sDescription' => 'Aus�ncia de processo formal de aquisi��o e testes de softwares.',
  ),
  741 => 
  array (
    'fkContext' => '1257',
    'fkCategory' => '177',
    'sDescription' => 'Incapacidade de integrar controles de seguran�a por especifica��o de requisitos de seguran�a ap�s o desenvolvimento do sistema e n�o durante a fase de projeto.',
  ),
  742 => 
  array (
    'fkContext' => '1260',
    'fkCategory' => '177',
    'sDescription' => 'Inser��o de dados incorretos no sistema de forma intencional.',
  ),
  743 => 
  array (
    'fkContext' => '1262',
    'fkCategory' => '139',
    'sDescription' => 'Inser��o de dados incorretos no sistema de forma intencional.',
  ),
  744 => 
  array (
    'fkContext' => '1264',
    'fkCategory' => '177',
    'sDescription' => 'Inser��o de dados incorretos por erros de digita��o.',
  ),
  745 => 
  array (
    'fkContext' => '1266',
    'fkCategory' => '139',
    'sDescription' => 'Inser��o de dados incorretos por erros de digita��o.',
  ),
  746 => 
  array (
    'fkContext' => '1268',
    'fkCategory' => '177',
    'sDescription' => 'Inser��o de dados incompletos em sistemas.',
  ),
  747 => 
  array (
    'fkContext' => '1270',
    'fkCategory' => '139',
    'sDescription' => 'Inser��o de dados incompletos em sistemas.',
  ),
  748 => 
  array (
    'fkContext' => '1271',
    'fkCategory' => '129',
    'sDescription' => 'Acesso n�o autorizado de dados atrav�s do uso de inje��o de c�digo SQL.',
  ),
  749 => 
  array (
    'fkContext' => '1273',
    'fkCategory' => '177',
    'sDescription' => 'Acesso n�o autorizado de dados atrav�s do uso de inje��o de c�digo SQL.',
  ),
  750 => 
  array (
    'fkContext' => '1275',
    'fkCategory' => '139',
    'sDescription' => 'Acesso n�o autorizado de dados atrav�s do uso de inje��o de c�digo SQL.',
  ),
  751 => 
  array (
    'fkContext' => '1276',
    'fkCategory' => '129',
    'sDescription' => 'Comprometimento do sistema atrav�s do uso de t�cnicas de buffer overflow.',
  ),
  752 => 
  array (
    'fkContext' => '1278',
    'fkCategory' => '177',
    'sDescription' => 'Comprometimento do sistema atrav�s do uso de t�cnicas de buffer overflow.',
  ),
  753 => 
  array (
    'fkContext' => '1280',
    'fkCategory' => '139',
    'sDescription' => 'Comprometimento do sistema atrav�s do uso de t�cnicas de buffer overflow.',
  ),
  754 => 
  array (
    'fkContext' => '1281',
    'fkCategory' => '129',
    'sDescription' => 'Falta de verifica��o peri�dica do conte�do de campos-chave ou arquivos de dados.',
  ),
  755 => 
  array (
    'fkContext' => '1283',
    'fkCategory' => '177',
    'sDescription' => 'Falta de verifica��o peri�dica do conte�do de campos-chave ou arquivos de dados.',
  ),
  756 => 
  array (
    'fkContext' => '1285',
    'fkCategory' => '139',
    'sDescription' => 'Falta de verifica��o peri�dica do conte�do de campos-chave ou arquivos de dados.',
  ),
  757 => 
  array (
    'fkContext' => '1286',
    'fkCategory' => '163',
    'sDescription' => 'Falta de verifica��o peri�dica do conte�do de campos-chave ou arquivos de dados.',
  ),
  758 => 
  array (
    'fkContext' => '1288',
    'fkCategory' => '177',
    'sDescription' => 'Altera��o indevida de dados por erros de processamento.',
  ),
  759 => 
  array (
    'fkContext' => '1291',
    'fkCategory' => '177',
    'sDescription' => 'Falha na detec��o de corrup��o de dados por falta de checagens de valida��o.',
  ),
  760 => 
  array (
    'fkContext' => '1294',
    'fkCategory' => '177',
    'sDescription' => 'Acesso n�o autorizado a dados por falta de uma lista de verifica��es (ex.: hash, controles run-to-run e program-to-program...).',
  ),
  761 => 
  array (
    'fkContext' => '1296',
    'fkCategory' => '129',
    'sDescription' => 'Falsifica��o de mensagens.',
  ),
  762 => 
  array (
    'fkContext' => '1298',
    'fkCategory' => '177',
    'sDescription' => 'Falsifica��o de mensagens.',
  ),
  763 => 
  array (
    'fkContext' => '1300',
    'fkCategory' => '139',
    'sDescription' => 'Falsifica��o de mensagens.',
  ),
  764 => 
  array (
    'fkContext' => '1301',
    'fkCategory' => '129',
    'sDescription' => 'Altera��o indevida de mensagens por falhas de processamento ou transmiss�o.',
  ),
  765 => 
  array (
    'fkContext' => '1303',
    'fkCategory' => '177',
    'sDescription' => 'Altera��o indevida de mensagens por falhas de processamento ou transmiss�o.',
  ),
  766 => 
  array (
    'fkContext' => '1305',
    'fkCategory' => '139',
    'sDescription' => 'Altera��o indevida de mensagens por falhas de processamento ou transmiss�o.',
  ),
  767 => 
  array (
    'fkContext' => '1306',
    'fkCategory' => '129',
    'sDescription' => 'Rep�dio de mensagens enviadas entre sistemas.',
  ),
  768 => 
  array (
    'fkContext' => '1308',
    'fkCategory' => '177',
    'sDescription' => 'Rep�dio de mensagens enviadas entre sistemas.',
  ),
  769 => 
  array (
    'fkContext' => '1310',
    'fkCategory' => '139',
    'sDescription' => 'Rep�dio de mensagens enviadas entre sistemas.',
  ),
  770 => 
  array (
    'fkContext' => '1311',
    'fkCategory' => '129',
    'sDescription' => 'Apresenta��o de resultados incorretos ou fora de contexto.',
  ),
  771 => 
  array (
    'fkContext' => '1313',
    'fkCategory' => '177',
    'sDescription' => 'Apresenta��o de resultados incorretos ou fora de contexto.',
  ),
  772 => 
  array (
    'fkContext' => '1315',
    'fkCategory' => '129',
    'sDescription' => 'Manipula��o n�o autorizada de informa��es exibidas em sistemas.',
  ),
  773 => 
  array (
    'fkContext' => '1317',
    'fkCategory' => '177',
    'sDescription' => 'Manipula��o n�o autorizada de informa��es exibidas em sistemas.',
  ),
  774 => 
  array (
    'fkContext' => '1319',
    'fkCategory' => '129',
    'sDescription' => 'Uso indevido de controles criptogr�ficos.',
  ),
  775 => 
  array (
    'fkContext' => '1320',
    'fkCategory' => '101',
    'sDescription' => 'Uso indevido de controles criptogr�ficos.',
  ),
  776 => 
  array (
    'fkContext' => '1322',
    'fkCategory' => '177',
    'sDescription' => 'Uso indevido de controles criptogr�ficos.',
  ),
  777 => 
  array (
    'fkContext' => '1324',
    'fkCategory' => '139',
    'sDescription' => 'Uso indevido de controles criptogr�ficos.',
  ),
  778 => 
  array (
    'fkContext' => '1325',
    'fkCategory' => '163',
    'sDescription' => 'Uso indevido de controles criptogr�ficos.',
  ),
  779 => 
  array (
    'fkContext' => '1326',
    'fkCategory' => '101',
    'sDescription' => 'Uso de controles criptogr�ficos em informa��es onde � desnecess�ria tal prote��o.',
  ),
  780 => 
  array (
    'fkContext' => '1327',
    'fkCategory' => '101',
    'sDescription' => 'Falta de uso de controles criptogr�ficos para prote��o de informa��es sens�veis.',
  ),
  781 => 
  array (
    'fkContext' => '1328',
    'fkCategory' => '129',
    'sDescription' => 'Utiliza��o de t�cnicas de criptografia fracas ou que possuem vulnerabilidades conhecidas.',
  ),
  782 => 
  array (
    'fkContext' => '1329',
    'fkCategory' => '101',
    'sDescription' => 'Utiliza��o de t�cnicas de criptografia fracas ou que possuem vulnerabilidades conhecidas.',
  ),
  783 => 
  array (
    'fkContext' => '1331',
    'fkCategory' => '177',
    'sDescription' => 'Utiliza��o de t�cnicas de criptografia fracas ou que possuem vulnerabilidades conhecidas.',
  ),
  784 => 
  array (
    'fkContext' => '1333',
    'fkCategory' => '139',
    'sDescription' => 'Utiliza��o de t�cnicas de criptografia fracas ou que possuem vulnerabilidades conhecidas.',
  ),
  785 => 
  array (
    'fkContext' => '1334',
    'fkCategory' => '163',
    'sDescription' => 'Utiliza��o de t�cnicas de criptografia fracas ou que possuem vulnerabilidades conhecidas.',
  ),
  786 => 
  array (
    'fkContext' => '1335',
    'fkCategory' => '129',
    'sDescription' => 'Incapacidade de recuperar chaves perdidas devido ao uso de t�cnicas de criptografia propriet�rias ou sem documenta��o.',
  ),
  787 => 
  array (
    'fkContext' => '1336',
    'fkCategory' => '101',
    'sDescription' => 'Incapacidade de recuperar chaves perdidas devido ao uso de t�cnicas de criptografia propriet�rias ou sem documenta��o.',
  ),
  788 => 
  array (
    'fkContext' => '1338',
    'fkCategory' => '177',
    'sDescription' => 'Incapacidade de recuperar chaves perdidas devido ao uso de t�cnicas de criptografia propriet�rias ou sem documenta��o.',
  ),
  789 => 
  array (
    'fkContext' => '1340',
    'fkCategory' => '139',
    'sDescription' => 'Incapacidade de recuperar chaves perdidas devido ao uso de t�cnicas de criptografia propriet�rias ou sem documenta��o.',
  ),
  790 => 
  array (
    'fkContext' => '1341',
    'fkCategory' => '129',
    'sDescription' => 'Acesso n�o autorizado a chaves de criptografia.',
  ),
  791 => 
  array (
    'fkContext' => '1343',
    'fkCategory' => '177',
    'sDescription' => 'Acesso n�o autorizado a chaves de criptografia.',
  ),
  792 => 
  array (
    'fkContext' => '1345',
    'fkCategory' => '139',
    'sDescription' => 'Acesso n�o autorizado a chaves de criptografia.',
  ),
  793 => 
  array (
    'fkContext' => '1346',
    'fkCategory' => '101',
    'sDescription' => 'Acesso n�o autorizado a chaves de criptografia.',
  ),
  794 => 
  array (
    'fkContext' => '1347',
    'fkCategory' => '129',
    'sDescription' => 'Distribui��o de chaves de criptografia para usu�rios n�o autorizados.',
  ),
  795 => 
  array (
    'fkContext' => '1349',
    'fkCategory' => '177',
    'sDescription' => 'Distribui��o de chaves de criptografia para usu�rios n�o autorizados.',
  ),
  796 => 
  array (
    'fkContext' => '1351',
    'fkCategory' => '139',
    'sDescription' => 'Distribui��o de chaves de criptografia para usu�rios n�o autorizados.',
  ),
  797 => 
  array (
    'fkContext' => '1352',
    'fkCategory' => '101',
    'sDescription' => 'Distribui��o de chaves de criptografia para usu�rios n�o autorizados.',
  ),
  798 => 
  array (
    'fkContext' => '1353',
    'fkCategory' => '129',
    'sDescription' => 'Incapacidade de recupera��o de chaves de criptografia perdidas.',
  ),
  799 => 
  array (
    'fkContext' => '1355',
    'fkCategory' => '177',
    'sDescription' => 'Incapacidade de recupera��o de chaves de criptografia perdidas.',
  ),
  800 => 
  array (
    'fkContext' => '1357',
    'fkCategory' => '139',
    'sDescription' => 'Incapacidade de recupera��o de chaves de criptografia perdidas.',
  ),
  801 => 
  array (
    'fkContext' => '1358',
    'fkCategory' => '163',
    'sDescription' => 'Incapacidade de recupera��o de chaves de criptografia perdidas.',
  ),
  802 => 
  array (
    'fkContext' => '1359',
    'fkCategory' => '129',
    'sDescription' => 'Impossibilidade de recupera��o de informa��es cifradas por falta de gerenciamento de chaves.',
  ),
  803 => 
  array (
    'fkContext' => '1361',
    'fkCategory' => '177',
    'sDescription' => 'Impossibilidade de recupera��o de informa��es cifradas por falta de gerenciamento de chaves.',
  ),
  804 => 
  array (
    'fkContext' => '1363',
    'fkCategory' => '139',
    'sDescription' => 'Impossibilidade de recupera��o de informa��es cifradas por falta de gerenciamento de chaves.',
  ),
  805 => 
  array (
    'fkContext' => '1364',
    'fkCategory' => '163',
    'sDescription' => 'Impossibilidade de recupera��o de informa��es cifradas por falta de gerenciamento de chaves.',
  ),
  806 => 
  array (
    'fkContext' => '1365',
    'fkCategory' => '129',
    'sDescription' => 'Armazenamento incorreto dos equipamentos de gera��o e cust�dia de chaves de criptografia.',
  ),
  807 => 
  array (
    'fkContext' => '1367',
    'fkCategory' => '177',
    'sDescription' => 'Armazenamento incorreto dos equipamentos de gera��o e cust�dia de chaves de criptografia.',
  ),
  808 => 
  array (
    'fkContext' => '1369',
    'fkCategory' => '139',
    'sDescription' => 'Armazenamento incorreto dos equipamentos de gera��o e cust�dia de chaves de criptografia.',
  ),
  809 => 
  array (
    'fkContext' => '1370',
    'fkCategory' => '163',
    'sDescription' => 'Armazenamento incorreto dos equipamentos de gera��o e cust�dia de chaves de criptografia.',
  ),
  810 => 
  array (
    'fkContext' => '1371',
    'fkCategory' => '129',
    'sDescription' => 'Falsifica��o de chaves criptogr�ficas.',
  ),
  811 => 
  array (
    'fkContext' => '1373',
    'fkCategory' => '177',
    'sDescription' => 'Falsifica��o de chaves criptogr�ficas.',
  ),
  812 => 
  array (
    'fkContext' => '1375',
    'fkCategory' => '139',
    'sDescription' => 'Falsifica��o de chaves criptogr�ficas.',
  ),
  813 => 
  array (
    'fkContext' => '1376',
    'fkCategory' => '163',
    'sDescription' => 'Falsifica��o de chaves criptogr�ficas.',
  ),
  814 => 
  array (
    'fkContext' => '1377',
    'fkCategory' => '129',
    'sDescription' => 'Corrup��o de ambientes do sistema operacional por instala��o inadequada de softwares.',
  ),
  815 => 
  array (
    'fkContext' => '1378',
    'fkCategory' => '129',
    'sDescription' => 'Falta de atualiza��es de software operacional por aus�ncia de contrato de suporte com o fornecedor.',
  ),
  816 => 
  array (
    'fkContext' => '1379',
    'fkCategory' => '129',
    'sDescription' => 'Indisponibilidade ou mal funcionamento de software operacional por instala��es de atualiza��es sem testes extensivos.',
  ),
  817 => 
  array (
    'fkContext' => '1380',
    'fkCategory' => '139',
    'sDescription' => 'Utiliza��o de informa��es reais no banco de dados de testes.',
  ),
  818 => 
  array (
    'fkContext' => '1382',
    'fkCategory' => '177',
    'sDescription' => 'Leitura n�o autorizada ao c�digo-fonte do sistema.',
  ),
  819 => 
  array (
    'fkContext' => '1385',
    'fkCategory' => '177',
    'sDescription' => 'Modifica��o n�o autorizada do c�digo-fonte do sistema.',
  ),
  820 => 
  array (
    'fkContext' => '1388',
    'fkCategory' => '177',
    'sDescription' => 'Acesso n�o autorizado � documenta��o de desenvolvimento do sistema.',
  ),
  821 => 
  array (
    'fkContext' => '1390',
    'fkCategory' => '129',
    'sDescription' => 'Falta de procedimento formal para controle de mudan�as.',
  ),
  822 => 
  array (
    'fkContext' => '1392',
    'fkCategory' => '177',
    'sDescription' => 'Falta de procedimento formal para controle de mudan�as.',
  ),
  823 => 
  array (
    'fkContext' => '1394',
    'fkCategory' => '139',
    'sDescription' => 'Falta de procedimento formal para controle de mudan�as.',
  ),
  824 => 
  array (
    'fkContext' => '1396',
    'fkCategory' => '177',
    'sDescription' => 'Comprometimento de aplica��es ap�s mudan�as realizadas no sistema operacional sobre o qual a aplica��o � executada.',
  ),
  825 => 
  array (
    'fkContext' => '1398',
    'fkCategory' => '139',
    'sDescription' => 'Comprometimento de aplica��es ap�s mudan�as realizadas no sistema operacional sobre o qual a aplica��o � executada.',
  ),
  826 => 
  array (
    'fkContext' => '1399',
    'fkCategory' => '163',
    'sDescription' => 'Comprometimento de aplica��es ap�s mudan�as realizadas no sistema operacional sobre o qual a aplica��o � executada.',
  ),
  827 => 
  array (
    'fkContext' => '1400',
    'fkCategory' => '129',
    'sDescription' => 'Falha na recupera��o de sistemas operacionais que est�o contigenciados por falta de garantias de controle de mudan�as nos planos de continuidade.',
  ),
  828 => 
  array (
    'fkContext' => '1401',
    'fkCategory' => '129',
    'sDescription' => 'Falta de an�lise cr�tica sobre as mudan�as realizadas no ambiente operacional.',
  ),
  829 => 
  array (
    'fkContext' => '1403',
    'fkCategory' => '177',
    'sDescription' => 'Comprometimento de controles de seguran�a e mecanismos de integridade de pacotes de software ap�s mudan�as ou customiza��es.',
  ),
  830 => 
  array (
    'fkContext' => '1405',
    'fkCategory' => '129',
    'sDescription' => 'Vazamento de informa��es atrav�s do uso e explora��o de covert channels.',
  ),
  831 => 
  array (
    'fkContext' => '1407',
    'fkCategory' => '177',
    'sDescription' => 'Vazamento de informa��es atrav�s do uso e explora��o de covert channels.',
  ),
  832 => 
  array (
    'fkContext' => '1409',
    'fkCategory' => '139',
    'sDescription' => 'Vazamento de informa��es atrav�s do uso e explora��o de covert channels.',
  ),
  833 => 
  array (
    'fkContext' => '1410',
    'fkCategory' => '235',
    'sDescription' => 'Vazamento de informa��es atrav�s do uso e explora��o de covert channels.',
  ),
  834 => 
  array (
    'fkContext' => '1411',
    'fkCategory' => '129',
    'sDescription' => 'Vazamento de informa��es atrav�s do uso de sistema de integridade duvidosa.',
  ),
  835 => 
  array (
    'fkContext' => '1413',
    'fkCategory' => '177',
    'sDescription' => 'Vazamento de informa��es atrav�s do uso de sistema de integridade duvidosa.',
  ),
  836 => 
  array (
    'fkContext' => '1415',
    'fkCategory' => '139',
    'sDescription' => 'Vazamento de informa��es atrav�s do uso de sistema de integridade duvidosa.',
  ),
  837 => 
  array (
    'fkContext' => '1416',
    'fkCategory' => '129',
    'sDescription' => 'Incapacidade de identificar o vazamento de informa��es por falta de monitoramento de sistemas.',
  ),
  838 => 
  array (
    'fkContext' => '1418',
    'fkCategory' => '177',
    'sDescription' => 'Incapacidade de identificar o vazamento de informa��es por falta de monitoramento de sistemas.',
  ),
  839 => 
  array (
    'fkContext' => '1420',
    'fkCategory' => '139',
    'sDescription' => 'Incapacidade de identificar o vazamento de informa��es por falta de monitoramento de sistemas.',
  ),
  840 => 
  array (
    'fkContext' => '1422',
    'fkCategory' => '177',
    'sDescription' => 'Acesso n�o autorizado a informa��es ou ao ambiente por desenvolvedores terceirizados.',
  ),
  841 => 
  array (
    'fkContext' => '1425',
    'fkCategory' => '177',
    'sDescription' => 'Instala��o de sistema com c�digo malicioso e/ou troiano por falta de testes antes da instala��o.',
  ),
  842 => 
  array (
    'fkContext' => '1428',
    'fkCategory' => '177',
    'sDescription' => 'Incapacidade de garantir a manuten��o do sistema ap�s impossibilidade de atendimento pelo desenvolver terceirizado.',
  ),
  843 => 
  array (
    'fkContext' => '1431',
    'fkCategory' => '177',
    'sDescription' => 'Uso indiscriminado do software desenvolvido por falta de acordos de licenciamento, propriedade do c�digo e direitos de propriedade intelectual.',
  ),
  844 => 
  array (
    'fkContext' => '1433',
    'fkCategory' => '129',
    'sDescription' => 'Comprometimento do sistema devido � exist�ncia de vulnerabilidades conhecidas.',
  ),
  845 => 
  array (
    'fkContext' => '1435',
    'fkCategory' => '177',
    'sDescription' => 'Comprometimento do sistema devido � exist�ncia de vulnerabilidades conhecidas.',
  ),
  846 => 
  array (
    'fkContext' => '1437',
    'fkCategory' => '139',
    'sDescription' => 'Comprometimento do sistema devido � exist�ncia de vulnerabilidades conhecidas.',
  ),
  847 => 
  array (
    'fkContext' => '1438',
    'fkCategory' => '163',
    'sDescription' => 'Comprometimento do sistema devido � exist�ncia de vulnerabilidades conhecidas.',
  ),
  848 => 
  array (
    'fkContext' => '1439',
    'fkCategory' => '129',
    'sDescription' => 'Vulnerabilidades t�cnicas n�o identificadas por falta de um invent�rio de ativos e de um processo de monitoramento sobre vulnerabilidades descobertas.',
  ),
  849 => 
  array (
    'fkContext' => '1441',
    'fkCategory' => '177',
    'sDescription' => 'Vulnerabilidades t�cnicas n�o identificadas por falta de um invent�rio de ativos e de um processo de monitoramento sobre vulnerabilidades descobertas.',
  ),
  850 => 
  array (
    'fkContext' => '1443',
    'fkCategory' => '139',
    'sDescription' => 'Vulnerabilidades t�cnicas n�o identificadas por falta de um invent�rio de ativos e de um processo de monitoramento sobre vulnerabilidades descobertas.',
  ),
  851 => 
  array (
    'fkContext' => '1444',
    'fkCategory' => '163',
    'sDescription' => 'Vulnerabilidades t�cnicas n�o identificadas por falta de um invent�rio de ativos e de um processo de monitoramento sobre vulnerabilidades descobertas.',
  ),
  852 => 
  array (
    'fkContext' => '1445',
    'fkCategory' => '129',
    'sDescription' => 'Instala��o de uma corre��o de seguran�a com problemas.',
  ),
  853 => 
  array (
    'fkContext' => '1447',
    'fkCategory' => '177',
    'sDescription' => 'Instala��o de uma corre��o de seguran�a com problemas.',
  ),
  854 => 
  array (
    'fkContext' => '1449',
    'fkCategory' => '139',
    'sDescription' => 'Instala��o de uma corre��o de seguran�a com problemas.',
  ),
  855 => 
  array (
    'fkContext' => '1450',
    'fkCategory' => '163',
    'sDescription' => 'Instala��o de uma corre��o de seguran�a com problemas.',
  ),
  856 => 
  array (
    'fkContext' => '1451',
    'fkCategory' => '279',
    'sDescription' => 'Incapacidade da dire��o em identificar ocorr�ncia de eventos de seguran�a da informa��o.',
  ),
  857 => 
  array (
    'fkContext' => '1452',
    'fkCategory' => '279',
    'sDescription' => 'Incapacidade de responder a eventos de seguran�a.',
  ),
  858 => 
  array (
    'fkContext' => '1453',
    'fkCategory' => '255',
    'sDescription' => 'Desconhecimento dos canais adequados para comunica��o de eventos de seguran�a.',
  ),
  859 => 
  array (
    'fkContext' => '1454',
    'fkCategory' => '255',
    'sDescription' => 'Falta de notifica��o de ocorr�ncias, por desconhecimento do que s�o considerados eventos de seguran�a da informa��o.',
  ),
  860 => 
  array (
    'fkContext' => '1455',
    'fkCategory' => '279',
    'sDescription' => 'Desconhecimento de fragilidades de seguran�a no ambiente.',
  ),
  861 => 
  array (
    'fkContext' => '1456',
    'fkCategory' => '255',
    'sDescription' => 'Incapacidade de identificar fragilidades de seguran�a no ambiente.',
  ),
  862 => 
  array (
    'fkContext' => '1457',
    'fkCategory' => '255',
    'sDescription' => 'Desconhecimento de canais adequados para comunica��o de fragilidades de seguran�a da informa��o.',
  ),
  863 => 
  array (
    'fkContext' => '1458',
    'fkCategory' => '279',
    'sDescription' => 'Fragilidades de seguran�a n�o detectadas anteriormente.',
  ),
  864 => 
  array (
    'fkContext' => '1459',
    'fkCategory' => '279',
    'sDescription' => 'Resposta inadequada a incidentes de seguran�a da informa��o.',
  ),
  865 => 
  array (
    'fkContext' => '1460',
    'fkCategory' => '279',
    'sDescription' => 'Demora na resposta a incidentes de seguran�a da informa��o.',
  ),
  866 => 
  array (
    'fkContext' => '1461',
    'fkCategory' => '279',
    'sDescription' => 'Resposta a incidentes de seguran�a da informa��o por pessoas inadequadas.',
  ),
  867 => 
  array (
    'fkContext' => '1462',
    'fkCategory' => '279',
    'sDescription' => 'Falha na resposta de incidentes de seguran�a por incapacidade de assumir responsabilidades pelos eventos ocorridos ou pela resolu��o dos problemas.',
  ),
  868 => 
  array (
    'fkContext' => '1463',
    'fkCategory' => '279',
    'sDescription' => 'Incapacidade de responder a incidentes de seguran�a.',
  ),
  869 => 
  array (
    'fkContext' => '1464',
    'fkCategory' => '279',
    'sDescription' => 'Recorr�ncia de incidentes de seguran�a.',
  ),
  870 => 
  array (
    'fkContext' => '1465',
    'fkCategory' => '279',
    'sDescription' => 'Incapacidade de quantificar os custos onerados por incidentes de seguran�a da informa��o ocorridos.',
  ),
  871 => 
  array (
    'fkContext' => '1466',
    'fkCategory' => '279',
    'sDescription' => 'Incapacidade de medir a efici�ncia dos controles de seguran�a com base nos incidentes de seguran�a ocorridos.',
  ),
  872 => 
  array (
    'fkContext' => '1467',
    'fkCategory' => '279',
    'sDescription' => 'Incapacidade de identificar a melhoria cont�nua de controles de seguran�a com base em incidentes ocorridos.',
  ),
  873 => 
  array (
    'fkContext' => '1468',
    'fkCategory' => '279',
    'sDescription' => 'Dificuldade em analisar criticamente os resultados do processo de gest�o da seguran�a da informa��o por falta de avalia��o e contabiliza��o de incidentes de seguran�a.',
  ),
  874 => 
  array (
    'fkContext' => '1469',
    'fkCategory' => '279',
    'sDescription' => 'Incapacidade de identificar causas de incidentes de seguran�a por falta de evid�ncias adequadas.',
  ),
  875 => 
  array (
    'fkContext' => '1470',
    'fkCategory' => '279',
    'sDescription' => 'Incapacidade de acionamento legal (civil ou criminal) de respons�veis por incidentes de seguran�a por falta de evid�ncias.',
  ),
  876 => 
  array (
    'fkContext' => '1471',
    'fkCategory' => '279',
    'sDescription' => 'Incapacidade de garantir a admissibilidade de evid�ncias coletadas.',
  ),
  877 => 
  array (
    'fkContext' => '1472',
    'fkCategory' => '279',
    'sDescription' => 'Comprometimento da integridade de evid�ncias.',
  ),
  878 => 
  array (
    'fkContext' => '1473',
    'fkCategory' => '279',
    'sDescription' => 'Impossibilidade de coletar evid�ncias em ambientes fora da jurisdi��o da organiza��o.',
  ),
  879 => 
  array (
    'fkContext' => '1474',
    'fkCategory' => '100',
    'sDescription' => 'Parada de processos cr�ticos de neg�cio por indisponibilidade do ativo.',
  ),
  880 => 
  array (
    'fkContext' => '1475',
    'fkCategory' => '128',
    'sDescription' => 'Parada de processos cr�ticos de neg�cio por indisponibilidade do ativo.',
  ),
  881 => 
  array (
    'fkContext' => '1476',
    'fkCategory' => '182',
    'sDescription' => 'Parada de processos cr�ticos de neg�cio por indisponibilidade do ativo.',
  ),
  882 => 
  array (
    'fkContext' => '1477',
    'fkCategory' => '255',
    'sDescription' => 'Parada de processos cr�ticos de neg�cio por indisponibilidade do ativo.',
  ),
  883 => 
  array (
    'fkContext' => '1478',
    'fkCategory' => '228',
    'sDescription' => 'Parada de processos cr�ticos de neg�cio por indisponibilidade do ativo.',
  ),
  884 => 
  array (
    'fkContext' => '1479',
    'fkCategory' => '182',
    'sDescription' => 'Parada de processos cr�ticos de neg�cio por falhas em equipamentos.',
  ),
  885 => 
  array (
    'fkContext' => '1480',
    'fkCategory' => '255',
    'sDescription' => 'Parada de processos cr�ticos de neg�cio por erros humanos.',
  ),
  886 => 
  array (
    'fkContext' => '1481',
    'fkCategory' => '273',
    'sDescription' => 'Parada dos servi�os por indisponibilidade dos ativos de informa��o.',
  ),
  887 => 
  array (
    'fkContext' => '1482',
    'fkCategory' => '279',
    'sDescription' => 'Impacto sobre a execu��o de processos de neg�cio cr�ticos ap�s incidentes de seguran�a da informa��o.',
  ),
  888 => 
  array (
    'fkContext' => '1483',
    'fkCategory' => '279',
    'sDescription' => 'Falhas de seguran�a nos processos rodando em regime de conting�ncia por n�o possu�rem os mesmos controles de seguran�a dos processos normais.',
  ),
  889 => 
  array (
    'fkContext' => '1484',
    'fkCategory' => '255',
    'sDescription' => 'Inacessibilidade aos postos de trabalho por causa de dist�rbios sociais.',
  ),
  890 => 
  array (
    'fkContext' => '1485',
    'fkCategory' => '279',
    'sDescription' => 'Desconhecimento dos eventos e incidentes envolvendo seguran�a da informa��o.',
  ),
  891 => 
  array (
    'fkContext' => '1486',
    'fkCategory' => '279',
    'sDescription' => 'Desconhecimento do impacto dos eventos e incidentes de seguran�a da informa��o sobre os processos cr�ticos de neg�cio.',
  ),
  892 => 
  array (
    'fkContext' => '1487',
    'fkCategory' => '279',
    'sDescription' => 'Aus�ncia de uma estrat�gia geral para garantir a recupera��o dos principais processos.',
  ),
  893 => 
  array (
    'fkContext' => '1488',
    'fkCategory' => '279',
    'sDescription' => 'Incapacidade de recuperar os principais processos de neg�cio de acordo com as estrat�gias de continuidade por desconhecimento das a��es necess�rias pelos envolvidos.',
  ),
  894 => 
  array (
    'fkContext' => '1489',
    'fkCategory' => '279',
    'sDescription' => 'Incapacidade de recuperar os principais processos de neg�cio em tempo h�bil por desconhecimento das a��es necess�rias pelos envolvidos.',
  ),
  895 => 
  array (
    'fkContext' => '1490',
    'fkCategory' => '279',
    'sDescription' => 'Erros ao executar as estrat�gias de continuidade por falta de documenta��o a respeito de quais a��es devem ser executadas pelos envolvidos.',
  ),
  896 => 
  array (
    'fkContext' => '1491',
    'fkCategory' => '279',
    'sDescription' => 'Incapacidade de atender aos planos e estrat�gias de continuidade desenvolvidos por falta de treinamento adequado.',
  ),
  897 => 
  array (
    'fkContext' => '1492',
    'fkCategory' => '255',
    'sDescription' => 'Incapacidade de recuperar processos de neg�cio cr�ticos por dificuldade de rea��o em momentos de crise.',
  ),
  898 => 
  array (
    'fkContext' => '1493',
    'fkCategory' => '183',
    'sDescription' => 'Comprometimento de informa��es em ambientes de conting�ncia por falha na implementa��o de requisitos de seguran�a de acordo com o ambiente original.',
  ),
  899 => 
  array (
    'fkContext' => '1494',
    'fkCategory' => '279',
    'sDescription' => 'Falta de recursos necess�rios nos sites remotos para recupera��o dos principais processos.',
  ),
  900 => 
  array (
    'fkContext' => '1495',
    'fkCategory' => '279',
    'sDescription' => 'Indisponibilidade de acesso aos planos e estrat�gias de continuidade de neg�cio por estes terem sido extraviados.',
  ),
  901 => 
  array (
    'fkContext' => '1496',
    'fkCategory' => '279',
    'sDescription' => 'Dificuldade de execu��o de Planos de Continuidade de Neg�cio por falta de padroniza��o entre os planos elaborados.',
  ),
  902 => 
  array (
    'fkContext' => '1497',
    'fkCategory' => '279',
    'sDescription' => 'Falha no acionamento de planos de continuidade de neg�cio por falta de condi��es claras nas quais o plano deve ser ativado.',
  ),
  903 => 
  array (
    'fkContext' => '1498',
    'fkCategory' => '279',
    'sDescription' => 'Falha no acionamento de planos de continuidade de neg�cio por falta de procedimentos de emerg�ncia que devem ser realizados logo ap�s confirmadas as condi��es de ativa��o.',
  ),
  904 => 
  array (
    'fkContext' => '1499',
    'fkCategory' => '279',
    'sDescription' => 'Falha no acionamento de planos de continuidade de neg�cio por falta de procedimentos que descrevem as a��es para recupera��o dos servi�os e ativos cr�ticos.',
  ),
  905 => 
  array (
    'fkContext' => '1500',
    'fkCategory' => '279',
    'sDescription' => 'Falha na execu��o de Planos de Continuidade de Neg�cio por falta de responsabilidades claramente definidas.',
  ),
  906 => 
  array (
    'fkContext' => '1501',
    'fkCategory' => '279',
    'sDescription' => 'Falha na execu��o de Planos de Continuidade de Neg�cio por desatualiza��o destes planos.',
  ),
  907 => 
  array (
    'fkContext' => '1502',
    'fkCategory' => '279',
    'sDescription' => 'Falhas na execu��o de Planos de Continuidade de Neg�cio por falta de testes garantindo que est�o corretos e funcionam conforme o previsto.',
  ),
  908 => 
  array (
    'fkContext' => '1503',
    'fkCategory' => '279',
    'sDescription' => 'Viola��o de leis, contratos, estatutos ou regulamenta��es que envolvam a seguran�a da informa��o por desconhecimento destes requisitos legais.',
  ),
  909 => 
  array (
    'fkContext' => '1504',
    'fkCategory' => '279',
    'sDescription' => 'Viola��o de leis, contratos, estatutos ou regulamenta��es que envolvam a seguran�a da informa��o por falha na comunica��o dos requisitos legais aos respons�veis pelos sistemas de informa��o.',
  ),
  910 => 
  array (
    'fkContext' => '1505',
    'fkCategory' => '128',
    'sDescription' => 'Viola��o dos direitos de propriedade intelectual no uso de softwares.',
  ),
  911 => 
  array (
    'fkContext' => '1506',
    'fkCategory' => '100',
    'sDescription' => 'Viola��o dos direitos de propriedade intelectual no uso de informa��es.',
  ),
  912 => 
  array (
    'fkContext' => '1507',
    'fkCategory' => '255',
    'sDescription' => 'Perda dos direitos de propriedade intelectual de softwares ou informa��es desenvolvidas na empresa por falta de especifica��o clara de tais direitos em contratos.',
  ),
  913 => 
  array (
    'fkContext' => '1508',
    'fkCategory' => '255',
    'sDescription' => 'Viola��o dos direitos de propriedade intelectual por usu�rios ao copiarem softwares, imagens, m�sicas ou outras informa��es para os computadores da empresa.',
  ),
  914 => 
  array (
    'fkContext' => '1509',
    'fkCategory' => '100',
    'sDescription' => 'Perda de registros importantes.',
  ),
  915 => 
  array (
    'fkContext' => '1510',
    'fkCategory' => '100',
    'sDescription' => 'Falsifica��o de registros importantes.',
  ),
  916 => 
  array (
    'fkContext' => '1511',
    'fkCategory' => '100',
    'sDescription' => 'Acesso n�o autorizado a registros importantes por aplica��o de controles de seguran�a insuficientes.',
  ),
  917 => 
  array (
    'fkContext' => '1512',
    'fkCategory' => '100',
    'sDescription' => 'Incapacidade de recuperar registros importantes armazenados em m�dias por longos per�odos de tempo.',
  ),
  918 => 
  array (
    'fkContext' => '1513',
    'fkCategory' => '100',
    'sDescription' => 'Reten��o por tempo insuficiente de registros importantes.',
  ),
  919 => 
  array (
    'fkContext' => '1514',
    'fkCategory' => '100',
    'sDescription' => 'Impossibilidade de recupera��o de registros importantes por armazenamento em formatos propriet�rios de arquivo e perda dos softwares de recupera��o.',
  ),
  920 => 
  array (
    'fkContext' => '1515',
    'fkCategory' => '102',
    'sDescription' => 'Divulga��o n�o autorizada de dados e informa��es pessoais.',
  ),
  921 => 
  array (
    'fkContext' => '1516',
    'fkCategory' => '102',
    'sDescription' => 'Acesso n�o autorizado a dados e informa��es pessoais.',
  ),
  922 => 
  array (
    'fkContext' => '1517',
    'fkCategory' => '102',
    'sDescription' => 'Capta��o de informa��es pessoais atrav�s de mecanismos ilegais.',
  ),
  923 => 
  array (
    'fkContext' => '1518',
    'fkCategory' => '102',
    'sDescription' => 'Uso indevido de informa��es pessoais.',
  ),
  924 => 
  array (
    'fkContext' => '1519',
    'fkCategory' => '128',
    'sDescription' => 'Falhas de seguran�a por uso indevido de ativos de informa��o.',
  ),
  925 => 
  array (
    'fkContext' => '1520',
    'fkCategory' => '213',
    'sDescription' => 'Falhas de seguran�a por uso indevido de ativos de informa��o.',
  ),
  926 => 
  array (
    'fkContext' => '1521',
    'fkCategory' => '229',
    'sDescription' => 'Falhas de seguran�a por uso indevido de ativos de informa��o.',
  ),
  927 => 
  array (
    'fkContext' => '1522',
    'fkCategory' => '235',
    'sDescription' => 'Falhas de seguran�a por uso indevido de ativos de informa��o.',
  ),
  928 => 
  array (
    'fkContext' => '1523',
    'fkCategory' => '128',
    'sDescription' => 'Uso indevido de ativos de informa��o por inexist�ncia de regras claras sobre o uso adequado e permitido.',
  ),
  929 => 
  array (
    'fkContext' => '1524',
    'fkCategory' => '213',
    'sDescription' => 'Uso indevido de ativos de informa��o por inexist�ncia de regras claras sobre o uso adequado e permitido.',
  ),
  930 => 
  array (
    'fkContext' => '1525',
    'fkCategory' => '229',
    'sDescription' => 'Uso indevido de ativos de informa��o por inexist�ncia de regras claras sobre o uso adequado e permitido.',
  ),
  931 => 
  array (
    'fkContext' => '1526',
    'fkCategory' => '235',
    'sDescription' => 'Uso indevido de ativos de informa��o por inexist�ncia de regras claras sobre o uso adequado e permitido.',
  ),
  932 => 
  array (
    'fkContext' => '1527',
    'fkCategory' => '279',
    'sDescription' => 'Viola��o de aspectos legais durante a monitora��o do uso de recursos de processamento da informa��o.',
  ),
  933 => 
  array (
    'fkContext' => '1528',
    'fkCategory' => '129',
    'sDescription' => 'Viola��o de aspectos legais no uso de controles de criptografia.',
  ),
  934 => 
  array (
    'fkContext' => '1530',
    'fkCategory' => '177',
    'sDescription' => 'Viola��o de aspectos legais no uso de controles de criptografia.',
  ),
  935 => 
  array (
    'fkContext' => '1532',
    'fkCategory' => '139',
    'sDescription' => 'Viola��o de aspectos legais no uso de controles de criptografia.',
  ),
  936 => 
  array (
    'fkContext' => '1533',
    'fkCategory' => '101',
    'sDescription' => 'Viola��o de aspectos legais no uso de controles de criptografia.',
  ),
  937 => 
  array (
    'fkContext' => '1534',
    'fkCategory' => '129',
    'sDescription' => 'Viola��o de aspectos legais na transmiss�o de informa��es criptografadas a outros pa�ses.',
  ),
  938 => 
  array (
    'fkContext' => '1536',
    'fkCategory' => '177',
    'sDescription' => 'Viola��o de aspectos legais na transmiss�o de informa��es criptografadas a outros pa�ses.',
  ),
  939 => 
  array (
    'fkContext' => '1538',
    'fkCategory' => '139',
    'sDescription' => 'Viola��o de aspectos legais na transmiss�o de informa��es criptografadas a outros pa�ses.',
  ),
  940 => 
  array (
    'fkContext' => '1539',
    'fkCategory' => '101',
    'sDescription' => 'Viola��o de aspectos legais na transmiss�o de informa��es criptografadas a outros pa�ses.',
  ),
  941 => 
  array (
    'fkContext' => '1540',
    'fkCategory' => '279',
    'sDescription' => 'Falhas na execu��o de procedimentos de seguran�a da informa��o.',
  ),
  942 => 
  array (
    'fkContext' => '1541',
    'fkCategory' => '128',
    'sDescription' => 'Falha na implementa��o de aspectos t�cnicos exigidos pelas normas e procedimentos de seguran�a da informa��o.',
  ),
  943 => 
  array (
    'fkContext' => '1542',
    'fkCategory' => '182',
    'sDescription' => 'Falha na implementa��o de aspectos t�cnicos exigidos pelas normas e procedimentos de seguran�a da informa��o.',
  ),
  944 => 
  array (
    'fkContext' => '1543',
    'fkCategory' => '228',
    'sDescription' => 'Falha na implementa��o de aspectos t�cnicos exigidos pelas normas e procedimentos de seguran�a da informa��o.',
  ),
  945 => 
  array (
    'fkContext' => '1544',
    'fkCategory' => '279',
    'sDescription' => 'Interrup��o de processos de neg�cio devido a auditoria de sistemas de informa��o.',
  ),
  946 => 
  array (
    'fkContext' => '1545',
    'fkCategory' => '128',
    'sDescription' => 'Uso n�o autorizado de ferramentas espec�ficas para auditorias de sistemas.',
  ),
  947 => 
  array (
    'fkContext' => '1546',
    'fkCategory' => '182',
    'sDescription' => 'Uso n�o autorizado de ferramentas espec�ficas para auditorias de sistemas.',
  ),
  948 => 
  array (
    'fkContext' => '1547',
    'fkCategory' => '228',
    'sDescription' => 'Uso n�o autorizado de ferramentas espec�ficas para auditorias de sistemas.',
  ),
)
?>