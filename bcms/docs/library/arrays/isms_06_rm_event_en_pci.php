<?php

$laEventEN_PCI = 
array (
  0 => 
  array (
    'fkContext' => '300',
    'fkCategory' => '279',
    'sDescription' => 'Different interpretations about the meaning of information security.',
  ),
  1 => 
  array (
    'fkContext' => '301',
    'fkCategory' => '279',
    'sDescription' => 'Lack of company\'s directions in relation to information security.',
  ),
  2 => 
  array (
    'fkContext' => '302',
    'fkCategory' => '279',
    'sDescription' => 'Divergence between the information security objectives and the business objectives.',
  ),
  3 => 
  array (
    'fkContext' => '303',
    'fkCategory' => '279',
    'sDescription' => 'Unclear focus on the information security management.',
  ),
  4 => 
  array (
    'fkContext' => '304',
    'fkCategory' => '279',
    'sDescription' => 'Security problems caused by the non-existence of a formal policy that defines the approach to the information security management.',
  ),
  5 => 
  array (
    'fkContext' => '305',
    'fkCategory' => '279',
    'sDescription' => 'Lack of comprehension by the staff about how important the information security is.',
  ),
  6 => 
  array (
    'fkContext' => '307',
    'fkCategory' => '279',
    'sDescription' => 'Outdated Security Policy regarding the importance and the need of business objectives.',
  ),
  7 => 
  array (
    'fkContext' => '308',
    'fkCategory' => '279',
    'sDescription' => 'Inefficiency of Security Policy as an instrument of orientation to the information security management process.',
  ),
  8 => 
  array (
    'fkContext' => '309',
    'fkCategory' => '279',
    'sDescription' => 'Lack of management commitment to initiatives of information security.',
  ),
  9 => 
  array (
    'fkContext' => '310',
    'fkCategory' => '279',
    'sDescription' => 'Lack of knowledge about information security processes and responsibilities by the management.',
  ),
  10 => 
  array (
    'fkContext' => '311',
    'fkCategory' => '279',
    'sDescription' => 'Lack of responsibilities definition for the information security due to lack of a clear definition by the management.',
  ),
  11 => 
  array (
    'fkContext' => '312',
    'fkCategory' => '279',
    'sDescription' => 'Lack of strategic alignment between the information security and the business objectives.',
  ),
  12 => 
  array (
    'fkContext' => '313',
    'fkCategory' => '279',
    'sDescription' => 'Lack of credibility in the management process of information security due to lack of support from the management.',
  ),
  13 => 
  array (
    'fkContext' => '314',
    'fkCategory' => '279',
    'sDescription' => 'Lack of people with expertise to conduct the process of information security management.',
  ),
  14 => 
  array (
    'fkContext' => '315',
    'fkCategory' => '279',
    'sDescription' => 'Lack of resources for the fulfillment of the requirements of information security.',
  ),
  15 => 
  array (
    'fkContext' => '316',
    'fkCategory' => '279',
    'sDescription' => 'Difficulty in implementation of security controls due to lack of coordination with relevant functions and roles.',
  ),
  16 => 
  array (
    'fkContext' => '317',
    'fkCategory' => '279',
    'sDescription' => 'Misalignment between actions of information security and the security policy.',
  ),
  17 => 
  array (
    'fkContext' => '318',
    'fkCategory' => '279',
    'sDescription' => 'Lack of support for the information security process by the key roles in the company.',
  ),
  18 => 
  array (
    'fkContext' => '319',
    'fkCategory' => '279',
    'sDescription' => 'Difficulty in conducting and solving non-conformities of information security processes.',
  ),
  19 => 
  array (
    'fkContext' => '320',
    'fkCategory' => '279',
    'sDescription' => 'Inability to homologate methods for risk management.',
  ),
  20 => 
  array (
    'fkContext' => '321',
    'fkCategory' => '279',
    'sDescription' => 'Security problems due to lack of definition of responsibilities for information security processes.',
  ),
  21 => 
  array (
    'fkContext' => '322',
    'fkCategory' => '279',
    'sDescription' => 'Lack of controls implementation due to lack of clear definition of responsibilities.',
  ),
  22 => 
  array (
    'fkContext' => '324',
    'fkCategory' => '255',
    'sDescription' => 'Difficulty by people in identifying and recognizing their information security responsibilities.',
  ),
  23 => 
  array (
    'fkContext' => '325',
    'fkCategory' => '279',
    'sDescription' => 'Security problems caused by divergences about who is the responsible for the information security.',
  ),
  24 => 
  array (
    'fkContext' => '326',
    'fkCategory' => '101',
    'sDescription' => 'Information processing in environments  not properly controlled.',
  ),
  25 => 
  array (
    'fkContext' => '327',
    'fkCategory' => '229',
    'sDescription' => 'Information processing in environments  not properly controlled.',
  ),
  26 => 
  array (
    'fkContext' => '328',
    'fkCategory' => '235',
    'sDescription' => 'Information processing in environments  not properly controlled.',
  ),
  27 => 
  array (
    'fkContext' => '334',
    'fkCategory' => '101',
    'sDescription' => 'Information processing in notebooks and devices, personal or private, that do not offer appropriate security levels.',
  ),
  28 => 
  array (
    'fkContext' => '335',
    'fkCategory' => '229',
    'sDescription' => 'Insertions of new hardware and/or software not compatible with the rest of system devices.',
  ),
  29 => 
  array (
    'fkContext' => '336',
    'fkCategory' => '235',
    'sDescription' => 'Insertions of new hardware and/or software not compatible with the rest of system devices.',
  ),
  30 => 
  array (
    'fkContext' => '343',
    'fkCategory' => '255',
    'sDescription' => 'Unauthorized disclosure of sensitive information during the contracting period.',
  ),
  31 => 
  array (
    'fkContext' => '344',
    'fkCategory' => '270',
    'sDescription' => 'Unauthorized information disclosure by the suppliers.',
  ),
  32 => 
  array (
    'fkContext' => '345',
    'fkCategory' => '271',
    'sDescription' => 'Unauthorized information disclosure by the suppliers.',
  ),
  33 => 
  array (
    'fkContext' => '347',
    'fkCategory' => '255',
    'sDescription' => 'Unauthorized disclosure of information after the contract is finished.',
  ),
  34 => 
  array (
    'fkContext' => '348',
    'fkCategory' => '279',
    'sDescription' => 'Lack of knowledge about the need to contact the authorities when a relevant security incident occurs.',
  ),
  35 => 
  array (
    'fkContext' => '349',
    'fkCategory' => '279',
    'sDescription' => 'Lack of knowledge about the need to contact the authorities when a relevant security incident occurs.',
  ),
  36 => 
  array (
    'fkContext' => '350',
    'fkCategory' => '279',
    'sDescription' => 'Difficulty in contacting authorities due to lack of contact information.',
  ),
  37 => 
  array (
    'fkContext' => '351',
    'fkCategory' => '235',
    'sDescription' => 'Difficulty in dealing with external attacks due to a difficulty in contacting the proper authorities, such as telecommunication providers and incident response centers.',
  ),
  38 => 
  array (
    'fkContext' => '352',
    'fkCategory' => '279',
    'sDescription' => 'Lack of knowledge of new vulnerabilities due to lack of contact with research groups and services of vulnerabilities notification.',
  ),
  39 => 
  array (
    'fkContext' => '353',
    'fkCategory' => '279',
    'sDescription' => 'Lack of specialists in information security.',
  ),
  40 => 
  array (
    'fkContext' => '354',
    'fkCategory' => '279',
    'sDescription' => 'Lack of knowledge about new techniques of attack.',
  ),
  41 => 
  array (
    'fkContext' => '355',
    'fkCategory' => '279',
    'sDescription' => 'Inability to assess whether the information security process remains relevant, appropriate and effective.',
  ),
  42 => 
  array (
    'fkContext' => '356',
    'fkCategory' => '279',
    'sDescription' => 'Inability to identify opportunities to improve the security processes.',
  ),
  43 => 
  array (
    'fkContext' => '357',
    'fkCategory' => '279',
    'sDescription' => 'Inability to identify if the information security processes are being properly executed.',
  ),
  44 => 
  array (
    'fkContext' => '359',
    'fkCategory' => '270',
    'sDescription' => 'Unauthorized access by external parties due to interconnecting to the company information, compromising the information security.',
  ),
  45 => 
  array (
    'fkContext' => '360',
    'fkCategory' => '271',
    'sDescription' => 'Unauthorized access by external parties due to interconnecting to the company information, compromising the information security.',
  ),
  46 => 
  array (
    'fkContext' => '362',
    'fkCategory' => '236',
    'sDescription' => 'Damage to the internal network through incidents in networks of external parties that are connected to the internal network.',
  ),
  47 => 
  array (
    'fkContext' => '363',
    'fkCategory' => '100',
    'sDescription' => 'Unavailability of access to external parties.',
  ),
  48 => 
  array (
    'fkContext' => '364',
    'fkCategory' => '270',
    'sDescription' => 'Unavailability of access to external parties.',
  ),
  49 => 
  array (
    'fkContext' => '365',
    'fkCategory' => '271',
    'sDescription' => 'Unavailability of access to external parties.',
  ),
  50 => 
  array (
    'fkContext' => '367',
    'fkCategory' => '236',
    'sDescription' => 'Remote access not authorized due to security failure of external parties that access the internal network.',
  ),
  51 => 
  array (
    'fkContext' => '368',
    'fkCategory' => '270',
    'sDescription' => 'Remote access not authorized due to security failure of external parties that access the internal network.',
  ),
  52 => 
  array (
    'fkContext' => '369',
    'fkCategory' => '271',
    'sDescription' => 'Remote access not authorized due to security failure of external parties that access the internal network.',
  ),
  53 => 
  array (
    'fkContext' => '371',
    'fkCategory' => '270',
    'sDescription' => 'Lack of knowledge by external parties, of their responsibilities related to the information security.',
  ),
  54 => 
  array (
    'fkContext' => '372',
    'fkCategory' => '271',
    'sDescription' => 'Lack of knowledge by external parties, of their responsibilities related to the information security.',
  ),
  55 => 
  array (
    'fkContext' => '374',
    'fkCategory' => '270',
    'sDescription' => 'Lack of knowledge of the risks related to being engaged with external parties.',
  ),
  56 => 
  array (
    'fkContext' => '375',
    'fkCategory' => '271',
    'sDescription' => 'Lack of knowledge of the risks related to being engaged with external parties.',
  ),
  57 => 
  array (
    'fkContext' => '377',
    'fkCategory' => '100',
    'sDescription' => 'Compromising of information caused by client unauthorized access.',
  ),
  58 => 
  array (
    'fkContext' => '378',
    'fkCategory' => '236',
    'sDescription' => 'Unauthorized access by clients to the internal network.',
  ),
  59 => 
  array (
    'fkContext' => '385',
    'fkCategory' => '270',
    'sDescription' => 'Lack of commitment by third party to information security.',
  ),
  60 => 
  array (
    'fkContext' => '386',
    'fkCategory' => '271',
    'sDescription' => 'Lack of commitment by third party to information security.',
  ),
  61 => 
  array (
    'fkContext' => '387',
    'fkCategory' => '100',
    'sDescription' => 'Unauthorized access by the third parties.',
  ),
  62 => 
  array (
    'fkContext' => '388',
    'fkCategory' => '269',
    'sDescription' => 'Unauthorized access by the third parties.',
  ),
  63 => 
  array (
    'fkContext' => '389',
    'fkCategory' => '270',
    'sDescription' => 'Unauthorized access by the third parties.',
  ),
  64 => 
  array (
    'fkContext' => '390',
    'fkCategory' => '271',
    'sDescription' => 'Unauthorized access by the third parties.',
  ),
  65 => 
  array (
    'fkContext' => '392',
    'fkCategory' => '269',
    'sDescription' => 'Lack of knowledge by third parties of their job\'s security requirements.',
  ),
  66 => 
  array (
    'fkContext' => '393',
    'fkCategory' => '270',
    'sDescription' => 'Lack of knowledge by third parties of their job\'s security requirements.',
  ),
  67 => 
  array (
    'fkContext' => '394',
    'fkCategory' => '271',
    'sDescription' => 'Lack of knowledge by third parties of their job\'s security requirements.',
  ),
  68 => 
  array (
    'fkContext' => '395',
    'fkCategory' => '269',
    'sDescription' => 'Inability by the third party to attend to the security requirements.',
  ),
  69 => 
  array (
    'fkContext' => '396',
    'fkCategory' => '270',
    'sDescription' => 'Inability by the third party to attend to the security requirements.',
  ),
  70 => 
  array (
    'fkContext' => '397',
    'fkCategory' => '271',
    'sDescription' => 'Inability by the third party to attend to the security requirements.',
  ),
  71 => 
  array (
    'fkContext' => '398',
    'fkCategory' => '100',
    'sDescription' => 'Disruption of information managed by third parties.',
  ),
  72 => 
  array (
    'fkContext' => '399',
    'fkCategory' => '269',
    'sDescription' => 'Disruption of information managed by third parties.',
  ),
  73 => 
  array (
    'fkContext' => '400',
    'fkCategory' => '270',
    'sDescription' => 'Disruption of information managed by third parties.',
  ),
  74 => 
  array (
    'fkContext' => '401',
    'fkCategory' => '271',
    'sDescription' => 'Disruption of information managed by third parties.',
  ),
  75 => 
  array (
    'fkContext' => '402',
    'fkCategory' => '100',
    'sDescription' => 'Disruption of information stored or processed in third parties environment.',
  ),
  76 => 
  array (
    'fkContext' => '403',
    'fkCategory' => '269',
    'sDescription' => 'Disruption of information stored or processed in third parties environment.',
  ),
  77 => 
  array (
    'fkContext' => '404',
    'fkCategory' => '270',
    'sDescription' => 'Disruption of information stored or processed in third parties environment.',
  ),
  78 => 
  array (
    'fkContext' => '405',
    'fkCategory' => '271',
    'sDescription' => 'Disruption of information stored or processed in third parties environment.',
  ),
  79 => 
  array (
    'fkContext' => '406',
    'fkCategory' => '269',
    'sDescription' => 'Inability by the third party to deal with security incidents.',
  ),
  80 => 
  array (
    'fkContext' => '407',
    'fkCategory' => '270',
    'sDescription' => 'Inability by the third party to deal with security incidents.',
  ),
  81 => 
  array (
    'fkContext' => '408',
    'fkCategory' => '271',
    'sDescription' => 'Inability by the third party to deal with security incidents.',
  ),
  82 => 
  array (
    'fkContext' => '409',
    'fkCategory' => '269',
    'sDescription' => 'Lack of knowledge by the third party, of controls that must be applied to protect information.',
  ),
  83 => 
  array (
    'fkContext' => '410',
    'fkCategory' => '270',
    'sDescription' => 'Lack of knowledge by the third party, of controls that must be applied to protect information.',
  ),
  84 => 
  array (
    'fkContext' => '411',
    'fkCategory' => '271',
    'sDescription' => 'Lack of knowledge by the third party, of controls that must be applied to protect information.',
  ),
  85 => 
  array (
    'fkContext' => '412',
    'fkCategory' => '228',
    'sDescription' => 'Inability by the third parties to recover their service level according to the Business Continuity Planning.',
  ),
  86 => 
  array (
    'fkContext' => '413',
    'fkCategory' => '269',
    'sDescription' => 'Inability by the third parties to recover their service level according to the Business Continuity Planning.',
  ),
  87 => 
  array (
    'fkContext' => '414',
    'fkCategory' => '270',
    'sDescription' => 'Inability by the third parties to recover their service level according to the Business Continuity Planning.',
  ),
  88 => 
  array (
    'fkContext' => '415',
    'fkCategory' => '271',
    'sDescription' => 'Inability by the third parties to recover their service level according to the Business Continuity Planning.',
  ),
  89 => 
  array (
    'fkContext' => '416',
    'fkCategory' => '228',
    'sDescription' => 'Problems due to faulty provisions of security by the third parties.',
  ),
  90 => 
  array (
    'fkContext' => '417',
    'fkCategory' => '269',
    'sDescription' => 'Problems due to faulty provisions of security by the third parties.',
  ),
  91 => 
  array (
    'fkContext' => '418',
    'fkCategory' => '270',
    'sDescription' => 'Problems due to faulty provisions of security by the third parties.',
  ),
  92 => 
  array (
    'fkContext' => '419',
    'fkCategory' => '271',
    'sDescription' => 'Problems due to faulty provisions of security by the third parties.',
  ),
  93 => 
  array (
    'fkContext' => '420',
    'fkCategory' => '279',
    'sDescription' => 'Lack of knowledge of information assets existent in the organization.',
  ),
  94 => 
  array (
    'fkContext' => '421',
    'fkCategory' => '279',
    'sDescription' => 'Lack of knowledge about the location of critical security assets.',
  ),
  95 => 
  array (
    'fkContext' => '422',
    'fkCategory' => '279',
    'sDescription' => 'Lack of knowledge about which assets must be restored after a disaster.',
  ),
  96 => 
  array (
    'fkContext' => '423',
    'fkCategory' => '182',
    'sDescription' => 'Inability to identify theft or loss of information assets.',
  ),
  97 => 
  array (
    'fkContext' => '424',
    'fkCategory' => '213',
    'sDescription' => 'Inability to identify hardware or software changes in information assets.',
  ),
  98 => 
  array (
    'fkContext' => '425',
    'fkCategory' => '279',
    'sDescription' => 'Failure in protection due to lack of knowledge about the importance of information assets.',
  ),
  99 => 
  array (
    'fkContext' => '426',
    'fkCategory' => '279',
    'sDescription' => 'Failure in protecting information assets due to lack of definition about who is responsible for this protection.',
  ),
  100 => 
  array (
    'fkContext' => '427',
    'fkCategory' => '279',
    'sDescription' => 'Failure in information assets protection due to lack of detailed knowledge about the assets and how they work.',
  ),
  101 => 
  array (
    'fkContext' => '428',
    'fkCategory' => '279',
    'sDescription' => 'Conflict of interests between security staff and users of certain information assets.',
  ),
  102 => 
  array (
    'fkContext' => '430',
    'fkCategory' => '213',
    'sDescription' => 'Inappropriate use of information assets.',
  ),
  103 => 
  array (
    'fkContext' => '431',
    'fkCategory' => '229',
    'sDescription' => 'Inappropriate use of information assets.',
  ),
  104 => 
  array (
    'fkContext' => '435',
    'fkCategory' => '229',
    'sDescription' => 'Misuse of information assets due to lack of clear rules about the appropriate and permitted use.',
  ),
  105 => 
  array (
    'fkContext' => '436',
    'fkCategory' => '235',
    'sDescription' => 'Misuse of information assets due to lack of clear rules about the appropriate and permitted use.',
  ),
  106 => 
  array (
    'fkContext' => '438',
    'fkCategory' => '100',
    'sDescription' => 'Inability to understand how sensitive and important are some information.',
  ),
  107 => 
  array (
    'fkContext' => '439',
    'fkCategory' => '100',
    'sDescription' => 'Inability by the users to decide how to deal with certain information.',
  ),
  108 => 
  array (
    'fkContext' => '440',
    'fkCategory' => '100',
    'sDescription' => 'Manipulation of information in a manner inconsistent with its value and importance.',
  ),
  109 => 
  array (
    'fkContext' => '441',
    'fkCategory' => '100',
    'sDescription' => 'Inability by the users to identify the level of classification of certain information.',
  ),
  110 => 
  array (
    'fkContext' => '442',
    'fkCategory' => '100',
    'sDescription' => 'Information treatment incompatible with the level of the assigned classification.',
  ),
  111 => 
  array (
    'fkContext' => '446',
    'fkCategory' => '100',
    'sDescription' => 'Inappropriate information treatment by third parties.',
  ),
  112 => 
  array (
    'fkContext' => '447',
    'fkCategory' => '100',
    'sDescription' => 'Inability by the users to identify the importance and security level required for the information.',
  ),
  113 => 
  array (
    'fkContext' => '448',
    'fkCategory' => '255',
    'sDescription' => 'Lack of knowledge of each employee about their responsibilities in the information security management process.',
  ),
  114 => 
  array (
    'fkContext' => '450',
    'fkCategory' => '279',
    'sDescription' => 'Lack of clear definitions about the responsibilities for certain activities.',
  ),
  115 => 
  array (
    'fkContext' => '451',
    'fkCategory' => '255',
    'sDescription' => 'Recruitment of employees unable to fulfill its responsibilities of information security.',
  ),
  116 => 
  array (
    'fkContext' => '452',
    'fkCategory' => '255',
    'sDescription' => 'Recruitment and access grant to critical systems for potential fraudsters.',
  ),
  117 => 
  array (
    'fkContext' => '453',
    'fkCategory' => '255',
    'sDescription' => 'Recruitment of people with a past history that violate the law.',
  ),
  118 => 
  array (
    'fkContext' => '455',
    'fkCategory' => '271',
    'sDescription' => 'Grant access of information and critical systems to third parties whose background has not been adequately evaluated by the company providing the services.',
  ),
  119 => 
  array (
    'fkContext' => '456',
    'fkCategory' => '269',
    'sDescription' => 'Granted access of information and critical systems to third parties whose background has not been adequately evaluated by the company providing the services.',
  ),
  120 => 
  array (
    'fkContext' => '457',
    'fkCategory' => '255',
    'sDescription' => 'Security problems caused by employees or by third parties that do not agree with their information security responsibilities.',
  ),
  121 => 
  array (
    'fkContext' => '459',
    'fkCategory' => '255',
    'sDescription' => 'Inability to legally prove that the employee or third party agreed with their information security responsibilities.',
  ),
  122 => 
  array (
    'fkContext' => '460',
    'fkCategory' => '279',
    'sDescription' => 'Poor management that may cause lack of engagement by people.',
  ),
  123 => 
  array (
    'fkContext' => '461',
    'fkCategory' => '279',
    'sDescription' => 'Inability to implement security actions due to lack of support and comprehension from the management about security requirements.',
  ),
  124 => 
  array (
    'fkContext' => '462',
    'fkCategory' => '279',
    'sDescription' => 'Lack of motivation of employees, suppliers and third parties for the security process due to lack of a clear request from the management.',
  ),
  125 => 
  array (
    'fkContext' => '463',
    'fkCategory' => '279',
    'sDescription' => 'Lack of motivation of employees and other collaborators for the information security process.',
  ),
  126 => 
  array (
    'fkContext' => '464',
    'fkCategory' => '255',
    'sDescription' => 'Lack of awareness on the importance and the need of ensuring the information security.',
  ),
  127 => 
  array (
    'fkContext' => '465',
    'fkCategory' => '255',
    'sDescription' => 'Lack of knowledge on the meaning of information security.',
  ),
  128 => 
  array (
    'fkContext' => '466',
    'fkCategory' => '255',
    'sDescription' => 'Lack of knowledge about the information security policy and the need of its application.',
  ),
  129 => 
  array (
    'fkContext' => '467',
    'fkCategory' => '255',
    'sDescription' => 'Security problems caused by new employees or third parties that have had access to information and systems before they have been properly trained.',
  ),
  130 => 
  array (
    'fkContext' => '468',
    'fkCategory' => '255',
    'sDescription' => 'Lack of knowledge about the security standards that need to be applied to the correct information protection.',
  ),
  131 => 
  array (
    'fkContext' => '469',
    'fkCategory' => '255',
    'sDescription' => 'Lack of ability to attend and perform the information security functions.',
  ),
  132 => 
  array (
    'fkContext' => '470',
    'fkCategory' => '255',
    'sDescription' => 'Inability to recognize an information security incident.',
  ),
  133 => 
  array (
    'fkContext' => '471',
    'fkCategory' => '255',
    'sDescription' => 'Lack of knowledge about new policies, standards and procedures caused by lack of updates of security training.',
  ),
  134 => 
  array (
    'fkContext' => '472',
    'fkCategory' => '279',
    'sDescription' => 'Recurrence of security problems due to lack of proper treatment with those that violate security policies and standards.',
  ),
  135 => 
  array (
    'fkContext' => '473',
    'fkCategory' => '279',
    'sDescription' => 'Inability to treat security polices violations due to lack of a formal disciplinary process.',
  ),
  136 => 
  array (
    'fkContext' => '474',
    'fkCategory' => '255',
    'sDescription' => 'Lack of commitment to the information security.',
  ),
  137 => 
  array (
    'fkContext' => '475',
    'fkCategory' => '255',
    'sDescription' => 'Disclosure of secret information after the contract termination with the organization.',
  ),
  138 => 
  array (
    'fkContext' => '476',
    'fkCategory' => '255',
    'sDescription' => 'Lack of updates of responsibilities for security information after a change of function or position within the company.',
  ),
  139 => 
  array (
    'fkContext' => '477',
    'fkCategory' => '255',
    'sDescription' => 'Unauthorized access to systems and information by users or third parties whose contract has expired, or by ignorance of the employees about the contract termination.',
  ),
  140 => 
  array (
    'fkContext' => '478',
    'fkCategory' => '255',
    'sDescription' => 'Lack of knowledge about new responsibilities and security functions in case of a change of function or position within the company.',
  ),
  141 => 
  array (
    'fkContext' => '479',
    'fkCategory' => '255',
    'sDescription' => 'Retention of information assets granted by the organization for employees, or third parties, or service providers, after termination of contract.',
  ),
  142 => 
  array (
    'fkContext' => '483',
    'fkCategory' => '255',
    'sDescription' => 'Compromising of information after resignation or termination of contract.',
  ),
  143 => 
  array (
    'fkContext' => '488',
    'fkCategory' => '213',
    'sDescription' => 'Unauthorized access to systems and information by users whose contract with the organization has been terminated.',
  ),
  144 => 
  array (
    'fkContext' => '489',
    'fkCategory' => '229',
    'sDescription' => 'Unauthorized access to systems and information by users whose contract with the organization has been terminated.',
  ),
  145 => 
  array (
    'fkContext' => '498',
    'fkCategory' => '183',
    'sDescription' => 'Unauthorized access to infrastructure users whose contract with the organization has been terminated.',
  ),
  146 => 
  array (
    'fkContext' => '503',
    'fkCategory' => '213',
    'sDescription' => 'Undue or unauthorized access to systems and information caused by lack of access rights maintenance or privileges of employees who changed their position and / or function.',
  ),
  147 => 
  array (
    'fkContext' => '504',
    'fkCategory' => '229',
    'sDescription' => 'Undue or unauthorized access to systems and information caused by lack of access rights maintenance or privileges of employees who changed their position and / or function.',
  ),
  148 => 
  array (
    'fkContext' => '509',
    'fkCategory' => '235',
    'sDescription' => 'Undue or unauthorized access to systems and information caused by lack of access rights maintenance or privileges of employees who changed their position and / or function.',
  ),
  149 => 
  array (
    'fkContext' => '514',
    'fkCategory' => '183',
    'sDescription' => 'Unauthorized access to physical environments through fragile walls and materials used for division of environments.',
  ),
  150 => 
  array (
    'fkContext' => '515',
    'fkCategory' => '183',
    'sDescription' => 'Unauthorized access to physical environments through unprotected windows.',
  ),
  151 => 
  array (
    'fkContext' => '516',
    'fkCategory' => '183',
    'sDescription' => 'Unauthorized access to physical environments caused by lack of division and control among areas of different levels of criticality.',
  ),
  152 => 
  array (
    'fkContext' => '517',
    'fkCategory' => '183',
    'sDescription' => 'Unauthorized access to physical environments caused by lack of ability to define the level of physical protection required for a particular area.',
  ),
  153 => 
  array (
    'fkContext' => '518',
    'fkCategory' => '183',
    'sDescription' => 'Unauthorized access to physical environments through emergency doors.',
  ),
  154 => 
  array (
    'fkContext' => '519',
    'fkCategory' => '183',
    'sDescription' => 'Unauthorized access to critical information processing areas due to the sharing of the area with other non-related activities.',
  ),
  155 => 
  array (
    'fkContext' => '520',
    'fkCategory' => '183',
    'sDescription' => 'Violations of fire combat rules by poor implementation of security controls.',
  ),
  156 => 
  array (
    'fkContext' => '521',
    'fkCategory' => '183',
    'sDescription' => 'Unauthorized access to security areas caused by lack of control of physical access.',
  ),
  157 => 
  array (
    'fkContext' => '522',
    'fkCategory' => '183',
    'sDescription' => 'Inability to determine accesses because of lack of proper registration.',
  ),
  158 => 
  array (
    'fkContext' => '523',
    'fkCategory' => '183',
    'sDescription' => 'Inability to identify unauthorized people in restricted areas.',
  ),
  159 => 
  array (
    'fkContext' => '524',
    'fkCategory' => '183',
    'sDescription' => 'Unauthorized access to physical environments through incorrectly configured access rights.',
  ),
  160 => 
  array (
    'fkContext' => '525',
    'fkCategory' => '183',
    'sDescription' => 'Unauthorized access to physical environments because of its proximity to public access.',
  ),
  161 => 
  array (
    'fkContext' => '526',
    'fkCategory' => '183',
    'sDescription' => 'Unauthorized access to physical environments due to opened environments where there are critical information.',
  ),
  162 => 
  array (
    'fkContext' => '527',
    'fkCategory' => '183',
    'sDescription' => 'Compromising of the physical environment due to the occurrence of fire in neighbor places.',
  ),
  163 => 
  array (
    'fkContext' => '528',
    'fkCategory' => '183',
    'sDescription' => 'Compromising of the physical environment due to the occurrence of fire locally.',
  ),
  164 => 
  array (
    'fkContext' => '529',
    'fkCategory' => '183',
    'sDescription' => 'Compromising of the physical environment due to the occurrence of flood.',
  ),
  165 => 
  array (
    'fkContext' => '530',
    'fkCategory' => '183',
    'sDescription' => 'Compromising of the physical environment due to the occurrence of explosions.',
  ),
  166 => 
  array (
    'fkContext' => '531',
    'fkCategory' => '183',
    'sDescription' => 'Compromising of the physical environment due to the occurrence of disturbance of public order.',
  ),
  167 => 
  array (
    'fkContext' => '532',
    'fkCategory' => '183',
    'sDescription' => 'Compromising of the physical environment due to the occurrence of natural disasters.',
  ),
  168 => 
  array (
    'fkContext' => '533',
    'fkCategory' => '183',
    'sDescription' => 'Compromising of equipment caused by water leak or pipeline problems.',
  ),
  169 => 
  array (
    'fkContext' => '534',
    'fkCategory' => '183',
    'sDescription' => 'Compromising of security because of use of cameras or cell phones.',
  ),
  170 => 
  array (
    'fkContext' => '535',
    'fkCategory' => '183',
    'sDescription' => 'Compromising of security due to the use of video or audio recording devices.',
  ),
  171 => 
  array (
    'fkContext' => '536',
    'fkCategory' => '183',
    'sDescription' => 'Compromising of security because of bad-intentioned actions of visitors.',
  ),
  172 => 
  array (
    'fkContext' => '537',
    'fkCategory' => '183',
    'sDescription' => 'Security problems caused by lack of clear rules about how to work in secure areas.',
  ),
  173 => 
  array (
    'fkContext' => '538',
    'fkCategory' => '183',
    'sDescription' => 'Unauthorized access to deliveries and / or loadings areas.',
  ),
  174 => 
  array (
    'fkContext' => '539',
    'fkCategory' => '183',
    'sDescription' => 'Compromising of security through goods delivered and loaded to the interior of the organization.',
  ),
  175 => 
  array (
    'fkContext' => '540',
    'fkCategory' => '183',
    'sDescription' => 'Unauthorized removal of information assets.',
  ),
  176 => 
  array (
    'fkContext' => '546',
    'fkCategory' => '100',
    'sDescription' => 'Unauthorized access to information by storing it in an inappropriate place.',
  ),
  177 => 
  array (
    'fkContext' => '566',
    'fkCategory' => '207',
    'sDescription' => 'Theft of equipments caused by installation in inadequate places.',
  ),
  178 => 
  array (
    'fkContext' => '573',
    'fkCategory' => '253',
    'sDescription' => 'Unavailability of the resources by failure in the electric power supply.',
  ),
  179 => 
  array (
    'fkContext' => '574',
    'fkCategory' => '252',
    'sDescription' => 'Compromising of the resources by failure in the water supply.',
  ),
  180 => 
  array (
    'fkContext' => '575',
    'fkCategory' => '254',
    'sDescription' => 'Compromising of the resources caused by failure in the heating / ventilation system.',
  ),
  181 => 
  array (
    'fkContext' => '576',
    'fkCategory' => '254',
    'sDescription' => 'Compromising of the resources caused by failure in the air-conditioning system.',
  ),
  182 => 
  array (
    'fkContext' => '577',
    'fkCategory' => '253',
    'sDescription' => 'Power supply is less than necessary for the installed equipment.',
  ),
  183 => 
  array (
    'fkContext' => '578',
    'fkCategory' => '253',
    'sDescription' => 'Failure in UPS devices.',
  ),
  184 => 
  array (
    'fkContext' => '580',
    'fkCategory' => '252',
    'sDescription' => 'Failure in environment lighting.',
  ),
  185 => 
  array (
    'fkContext' => '581',
    'fkCategory' => '252',
    'sDescription' => 'Failure of the fire extinguisher equipment caused by a failure in water supply.',
  ),
  186 => 
  array (
    'fkContext' => '582',
    'fkCategory' => '252',
    'sDescription' => 'Failure of environment conditioning equipment caused by failure in water supply.',
  ),
  187 => 
  array (
    'fkContext' => '585',
    'fkCategory' => '235',
    'sDescription' => 'Interception of information through access to the cabling.',
  ),
  188 => 
  array (
    'fkContext' => '586',
    'fkCategory' => '253',
    'sDescription' => 'Interruption of electric energy supply caused by failure in the cabling.',
  ),
  189 => 
  array (
    'fkContext' => '587',
    'fkCategory' => '235',
    'sDescription' => 'Failure in the logical cabling interrupting the data communication.',
  ),
  190 => 
  array (
    'fkContext' => '589',
    'fkCategory' => '235',
    'sDescription' => 'Failure in the communications by electrical equipment interference.',
  ),
  191 => 
  array (
    'fkContext' => '590',
    'fkCategory' => '253',
    'sDescription' => 'Unauthorized access to energy control panels.',
  ),
  192 => 
  array (
    'fkContext' => '591',
    'fkCategory' => '235',
    'sDescription' => 'Unauthorized access to cabling control panels.',
  ),
  193 => 
  array (
    'fkContext' => '594',
    'fkCategory' => '252',
    'sDescription' => 'Lack of periodic maintenance',
  ),
  194 => 
  array (
    'fkContext' => '597',
    'fkCategory' => '252',
    'sDescription' => 'Compromising of equipment caused by maintenance made by unqualified people.',
  ),
  195 => 
  array (
    'fkContext' => '602',
    'fkCategory' => '252',
    'sDescription' => 'Insurance loss due to not meeting established requirements about maintenance of equipment and/or structure.',
  ),
  196 => 
  array (
    'fkContext' => '619',
    'fkCategory' => '100',
    'sDescription' => 'Unauthorized access to information through removal of important organization\'s assets.',
  ),
  197 => 
  array (
    'fkContext' => '620',
    'fkCategory' => '228',
    'sDescription' => 'Wrong  execution  of operating services due to lack of formal documented procedures for system activities.',
  ),
  198 => 
  array (
    'fkContext' => '621',
    'fkCategory' => '228',
    'sDescription' => 'Interruption of services due to unplanned changes.',
  ),
  199 => 
  array (
    'fkContext' => '623',
    'fkCategory' => '182',
    'sDescription' => 'Execution of unauthorized changes.',
  ),
  200 => 
  array (
    'fkContext' => '624',
    'fkCategory' => '228',
    'sDescription' => 'Execution of unauthorized changes.',
  ),
  201 => 
  array (
    'fkContext' => '625',
    'fkCategory' => '255',
    'sDescription' => 'Execution of unauthorized changes.',
  ),
  202 => 
  array (
    'fkContext' => '626',
    'fkCategory' => '228',
    'sDescription' => 'Inability to recover a service after a failure in performed changes.',
  ),
  203 => 
  array (
    'fkContext' => '631',
    'fkCategory' => '100',
    'sDescription' => 'Unauthorized change of information.',
  ),
  204 => 
  array (
    'fkContext' => '633',
    'fkCategory' => '236',
    'sDescription' => 'Unauthorized access to operational facilities information by developers.',
  ),
  205 => 
  array (
    'fkContext' => '641',
    'fkCategory' => '101',
    'sDescription' => 'Unauthorized access to sensitive information that has been compiled to the test facilities or development facilities.',
  ),
  206 => 
  array (
    'fkContext' => '652',
    'fkCategory' => '270',
    'sDescription' => 'Lack of service delivery stipulated by contract.',
  ),
  207 => 
  array (
    'fkContext' => '653',
    'fkCategory' => '271',
    'sDescription' => 'Inability to attend the business continuity plans.',
  ),
  208 => 
  array (
    'fkContext' => '654',
    'fkCategory' => '269',
    'sDescription' => 'Inability to attend the business continuity plans.',
  ),
  209 => 
  array (
    'fkContext' => '655',
    'fkCategory' => '270',
    'sDescription' => 'Inability to attend the business continuity plans.',
  ),
  210 => 
  array (
    'fkContext' => '659',
    'fkCategory' => '271',
    'sDescription' => 'Inability to identify and manage incidents occurrence in services provided by third party. ',
  ),
  211 => 
  array (
    'fkContext' => '660',
    'fkCategory' => '269',
    'sDescription' => 'Inability to identify and manage incidents occurrence in services provided by third party. ',
  ),
  212 => 
  array (
    'fkContext' => '661',
    'fkCategory' => '270',
    'sDescription' => 'Inability to identify and manage incidents occurrence in services provided by third party. ',
  ),
  213 => 
  array (
    'fkContext' => '671',
    'fkCategory' => '228',
    'sDescription' => 'Changes in the third party service provider.',
  ),
  214 => 
  array (
    'fkContext' => '673',
    'fkCategory' => '228',
    'sDescription' => 'Inability of the service provider to attend improvement requirements.',
  ),
  215 => 
  array (
    'fkContext' => '683',
    'fkCategory' => '228',
    'sDescription' => 'Inability to manage systems and services due to key person dependency.',
  ),
  216 => 
  array (
    'fkContext' => '684',
    'fkCategory' => '213',
    'sDescription' => 'Inability to manage systems and services due to key person dependency.',
  ),
  217 => 
  array (
    'fkContext' => '692',
    'fkCategory' => '209',
    'sDescription' => 'Lack of space for storage.',
  ),
  218 => 
  array (
    'fkContext' => '726',
    'fkCategory' => '229',
    'sDescription' => 'Computer virus contamination.',
  ),
  219 => 
  array (
    'fkContext' => '727',
    'fkCategory' => '235',
    'sDescription' => 'Computer virus contamination.',
  ),
  220 => 
  array (
    'fkContext' => '730',
    'fkCategory' => '213',
    'sDescription' => ' Installation of Trojan horses"."',
  ),
  221 => 
  array (
    'fkContext' => '732',
    'fkCategory' => '213',
    'sDescription' => 'Receivement of malicious code by e-mail.',
  ),
  222 => 
  array (
    'fkContext' => '734',
    'fkCategory' => '213',
    'sDescription' => 'Contamination of resources caused by insertion of infected files on computers.',
  ),
  223 => 
  array (
    'fkContext' => '736',
    'fkCategory' => '213',
    'sDescription' => 'Insertion of malicious code during emergency procedures in which security controls are not considered.',
  ),
  224 => 
  array (
    'fkContext' => '738',
    'fkCategory' => '213',
    'sDescription' => 'Contamination of information assets due to updates of malicious code detection and repair software.',
  ),
  225 => 
  array (
    'fkContext' => '739',
    'fkCategory' => '213',
    'sDescription' => 'Execution of unauthorized mobile code.',
  ),
  226 => 
  array (
    'fkContext' => '740',
    'fkCategory' => '213',
    'sDescription' => 'Inability to identify an attempt to execute mobile code.',
  ),
  227 => 
  array (
    'fkContext' => '741',
    'fkCategory' => '213',
    'sDescription' => 'Mobile code manipulation to execute unauthorized activities.',
  ),
  228 => 
  array (
    'fkContext' => '742',
    'fkCategory' => '213',
    'sDescription' => 'Misuse of local resources by the mobile code.',
  ),
  229 => 
  array (
    'fkContext' => '749',
    'fkCategory' => '209',
    'sDescription' => 'Inability to restore back-up copies due to deteriorating in the storage media.',
  ),
  230 => 
  array (
    'fkContext' => '750',
    'fkCategory' => '279',
    'sDescription' => 'Policy back-up copies incompatible with business continuity plans.',
  ),
  231 => 
  array (
    'fkContext' => '753',
    'fkCategory' => '236',
    'sDescription' => 'Unauthorized access to network through not an uncontrolled access point.',
  ),
  232 => 
  array (
    'fkContext' => '754',
    'fkCategory' => '236',
    'sDescription' => 'Unauthorized access to the network caused by an inadequate user management process.',
  ),
  233 => 
  array (
    'fkContext' => '755',
    'fkCategory' => '236',
    'sDescription' => 'Insertion of non-homologated equipments (personal resources, notebook of third party ...).',
  ),
  234 => 
  array (
    'fkContext' => '756',
    'fkCategory' => '236',
    'sDescription' => 'Disclosure of information related to network due to DNS configuration failure.',
  ),
  235 => 
  array (
    'fkContext' => '757',
    'fkCategory' => '236',
    'sDescription' => 'Network access problems due to failure in DNS configuration.',
  ),
  236 => 
  array (
    'fkContext' => '758',
    'fkCategory' => '236',
    'sDescription' => 'Undue access through wrong configuration of network address service.',
  ),
  237 => 
  array (
    'fkContext' => '759',
    'fkCategory' => '236',
    'sDescription' => 'Network access problems due to failure at main services (AD, DHCP, DNS, NetBIOS...).',
  ),
  238 => 
  array (
    'fkContext' => '760',
    'fkCategory' => '101',
    'sDescription' => 'Inappropriate sharing of files in network.',
  ),
  239 => 
  array (
    'fkContext' => '761',
    'fkCategory' => '236',
    'sDescription' => 'Inability to identify unauthorized actions in the network.',
  ),
  240 => 
  array (
    'fkContext' => '762',
    'fkCategory' => '236',
    'sDescription' => 'Network unavailability due to internal attack.',
  ),
  241 => 
  array (
    'fkContext' => '763',
    'fkCategory' => '236',
    'sDescription' => 'Overlap network security controls through the use of services that mask access.',
  ),
  242 => 
  array (
    'fkContext' => '764',
    'fkCategory' => '236',
    'sDescription' => 'Lack of security in external networks (cryptography, VPN, etc) due to lack of security controls.',
  ),
  243 => 
  array (
    'fkContext' => '765',
    'fkCategory' => '236',
    'sDescription' => 'Unavailability of network services.',
  ),
  244 => 
  array (
    'fkContext' => '768',
    'fkCategory' => '236',
    'sDescription' => 'Inappropriate network service management.',
  ),
  245 => 
  array (
    'fkContext' => '771',
    'fkCategory' => '209',
    'sDescription' => 'Unauthorized access to information storage medias.',
  ),
  246 => 
  array (
    'fkContext' => '773',
    'fkCategory' => '209',
    'sDescription' => 'Unauthorized removal of information stored in medias to outside the organization.',
  ),
  247 => 
  array (
    'fkContext' => '775',
    'fkCategory' => '209',
    'sDescription' => 'Compromising of information stored on media due to inappropriate storage.',
  ),
  248 => 
  array (
    'fkContext' => '777',
    'fkCategory' => '209',
    'sDescription' => 'Loss of information stored for long periods of time due to media deterioration.',
  ),
  249 => 
  array (
    'fkContext' => '778',
    'fkCategory' => '209',
    'sDescription' => 'Unauthorized copy of information through removable media drives unnecessary enabled.',
  ),
  250 => 
  array (
    'fkContext' => '779',
    'fkCategory' => '209',
    'sDescription' => 'Unauthorized copy of information through removable media drives enabled for business reason.',
  ),
  251 => 
  array (
    'fkContext' => '781',
    'fkCategory' => '209',
    'sDescription' => 'Unauthorized access to information stored in media disposal.',
  ),
  252 => 
  array (
    'fkContext' => '783',
    'fkCategory' => '100',
    'sDescription' => 'Unauthorized access to information due to lack of access restriction to sensitive information level.',
  ),
  253 => 
  array (
    'fkContext' => '784',
    'fkCategory' => '100',
    'sDescription' => 'Access to sensitive information by an unauthorized recipients.',
  ),
  254 => 
  array (
    'fkContext' => '803',
    'fkCategory' => '235',
    'sDescription' => 'Sending malicious code through communication facilities.',
  ),
  255 => 
  array (
    'fkContext' => '805',
    'fkCategory' => '235',
    'sDescription' => 'Unauthorized transmission of sensitive information in the form of attachment.',
  ),
  256 => 
  array (
    'fkContext' => '806',
    'fkCategory' => '230',
    'sDescription' => 'Unauthorized transmission of sensitive information in the form of attachment.',
  ),
  257 => 
  array (
    'fkContext' => '807',
    'fkCategory' => '235',
    'sDescription' => 'Receiving unauthorized information through files attached.',
  ),
  258 => 
  array (
    'fkContext' => '809',
    'fkCategory' => '235',
    'sDescription' => 'Receiving of unauthorized information through download of data.',
  ),
  259 => 
  array (
    'fkContext' => '811',
    'fkCategory' => '235',
    'sDescription' => 'Sending unauthorized information through upload of data.',
  ),
  260 => 
  array (
    'fkContext' => '813',
    'fkCategory' => '235',
    'sDescription' => 'Misuse of communication facilities.',
  ),
  261 => 
  array (
    'fkContext' => '815',
    'fkCategory' => '236',
    'sDescription' => 'Use of wireless communications without appropriate protection.',
  ),
  262 => 
  array (
    'fkContext' => '818',
    'fkCategory' => '273',
    'sDescription' => 'Defamation through communication facilities.',
  ),
  263 => 
  array (
    'fkContext' => '820',
    'fkCategory' => '234',
    'sDescription' => 'Unauthorized access to sensitive information left on printers.',
  ),
  264 => 
  array (
    'fkContext' => '824',
    'fkCategory' => '230',
    'sDescription' => 'Unauthorized sending of information through automatic forwarding of electronic mail .',
  ),
  265 => 
  array (
    'fkContext' => '825',
    'fkCategory' => '235',
    'sDescription' => 'Unauthorized access to information through wiretapping and other forms of eavesdropping.',
  ),
  266 => 
  array (
    'fkContext' => '827',
    'fkCategory' => '255',
    'sDescription' => 'Accidental reveal of sensitive information to immediate vicinity when making a phone call.',
  ),
  267 => 
  array (
    'fkContext' => '831',
    'fkCategory' => '230',
    'sDescription' => 'Accidental sending of sensitive information by mistake in addressing e-mails.',
  ),
  268 => 
  array (
    'fkContext' => '832',
    'fkCategory' => '269',
    'sDescription' => 'Loss of information sent to external parties.',
  ),
  269 => 
  array (
    'fkContext' => '834',
    'fkCategory' => '271',
    'sDescription' => 'Loss of information sent to external parties.',
  ),
  270 => 
  array (
    'fkContext' => '835',
    'fkCategory' => '235',
    'sDescription' => 'Loss of information sent to external parties.',
  ),
  271 => 
  array (
    'fkContext' => '836',
    'fkCategory' => '235',
    'sDescription' => 'Not receiving information sent by external parties.',
  ),
  272 => 
  array (
    'fkContext' => '837',
    'fkCategory' => '235',
    'sDescription' => 'Unauthorized access to information shared with external parties during sending/receiving.',
  ),
  273 => 
  array (
    'fkContext' => '839',
    'fkCategory' => '209',
    'sDescription' => 'Theft of medias during transportation beyond the organization\'s physical boundaries.',
  ),
  274 => 
  array (
    'fkContext' => '841',
    'fkCategory' => '209',
    'sDescription' => 'Damages caused by the use of inappropriate package to transport medias  beyond the organization\'s physical boundaries.',
  ),
  275 => 
  array (
    'fkContext' => '842',
    'fkCategory' => '209',
    'sDescription' => 'Compromising of information contained in medias which are transported to outside the organization via the postal service or via courier.',
  ),
  276 => 
  array (
    'fkContext' => '843',
    'fkCategory' => '235',
    'sDescription' => 'Unauthorized access to electronic sent messages.',
  ),
  277 => 
  array (
    'fkContext' => '844',
    'fkCategory' => '235',
    'sDescription' => 'Incorrect addressing when sending electronic messages.',
  ),
  278 => 
  array (
    'fkContext' => '845',
    'fkCategory' => '235',
    'sDescription' => 'Failure in delivering electronic messages.',
  ),
  279 => 
  array (
    'fkContext' => '846',
    'fkCategory' => '235',
    'sDescription' => 'Inability to grant the authenticity of electronic messages.',
  ),
  280 => 
  array (
    'fkContext' => '848',
    'fkCategory' => '235',
    'sDescription' => 'Unauthorized disclosure of information through the use of services such as instant messaging or file sharing.',
  ),
  281 => 
  array (
    'fkContext' => '883',
    'fkCategory' => '273',
    'sDescription' => 'Incorrect or unauthorized information in systems directly accessible from the Internet.',
  ),
  282 => 
  array (
    'fkContext' => '978',
    'fkCategory' => '183',
    'sDescription' => 'Inadequate allocation of access rights and privileges due to lack of an access control policy.',
  ),
  283 => 
  array (
    'fkContext' => '984',
    'fkCategory' => '183',
    'sDescription' => 'Access rights violation due to lack of clear rules that establish what is permitted and what is forbidden.',
  ),
  284 => 
  array (
    'fkContext' => '990',
    'fkCategory' => '183',
    'sDescription' => 'Incompatibility between access rights granted and the real need of access based in business requirements.',
  ),
  285 => 
  array (
    'fkContext' => '996',
    'fkCategory' => '183',
    'sDescription' => 'Users with unnecessary access rights regarding their job roles.',
  ),
  286 => 
  array (
    'fkContext' => '997',
    'fkCategory' => '269',
    'sDescription' => 'Lack of knowledge about access rights by third parties, suppliers and service providers.',
  ),
  287 => 
  array (
    'fkContext' => '998',
    'fkCategory' => '270',
    'sDescription' => 'Lack of knowledge about access rights by third parties, suppliers and service providers.',
  ),
  288 => 
  array (
    'fkContext' => '999',
    'fkCategory' => '271',
    'sDescription' => 'Lack of knowledge about access rights by third parties, suppliers and service providers.',
  ),
  289 => 
  array (
    'fkContext' => '1115',
    'fkCategory' => '213',
    'sDescription' => 'Unauthorized access through unattended equipment.',
  ),
  290 => 
  array (
    'fkContext' => '1116',
    'fkCategory' => '213',
    'sDescription' => 'Unauthorized access to equipment during short periods of absence of employees.',
  ),
  291 => 
  array (
    'fkContext' => '1117',
    'fkCategory' => '213',
    'sDescription' => 'Unauthorized access to equipment during periods outside normal working hours.',
  ),
  292 => 
  array (
    'fkContext' => '1118',
    'fkCategory' => '213',
    'sDescription' => 'Lack of means to block unattended equipment.',
  ),
  293 => 
  array (
    'fkContext' => '1119',
    'fkCategory' => '115',
    'sDescription' => 'Unauthorized access to sensitive information left on desks.',
  ),
  294 => 
  array (
    'fkContext' => '1120',
    'fkCategory' => '101',
    'sDescription' => 'Unauthorized access to sensitive information through undue reading of computers\' screen.',
  ),
  295 => 
  array (
    'fkContext' => '1122',
    'fkCategory' => '236',
    'sDescription' => 'Granting inappropriate access rights to network services.',
  ),
  296 => 
  array (
    'fkContext' => '1124',
    'fkCategory' => '236',
    'sDescription' => 'Granting access rights to network services incompatible with the access controls.',
  ),
  297 => 
  array (
    'fkContext' => '1126',
    'fkCategory' => '236',
    'sDescription' => 'Unauthorized dial-up connection to other networks',
  ),
  298 => 
  array (
    'fkContext' => '1128',
    'fkCategory' => '236',
    'sDescription' => 'Unauthorized connection of equipment to network.',
  ),
  299 => 
  array (
    'fkContext' => '1129',
    'fkCategory' => '237',
    'sDescription' => 'Unauthorized remote access by network users.',
  ),
  300 => 
  array (
    'fkContext' => '1130',
    'fkCategory' => '236',
    'sDescription' => 'Unauthorized access to wireless network.',
  ),
  301 => 
  array (
    'fkContext' => '1131',
    'fkCategory' => '237',
    'sDescription' => 'Unauthorized remote access by attackers (hackers).',
  ),
  302 => 
  array (
    'fkContext' => '1133',
    'fkCategory' => '236',
    'sDescription' => 'Access to the network through identity counterfeit for specific equipment.',
  ),
  303 => 
  array (
    'fkContext' => '1134',
    'fkCategory' => '213',
    'sDescription' => 'Unauthorized access to equipment through logical access to remote diagnostic and configuration ports.',
  ),
  304 => 
  array (
    'fkContext' => '1135',
    'fkCategory' => '213',
    'sDescription' => 'Unauthorized access to equipment through physical access to remote diagnostic and configuration ports.',
  ),
  305 => 
  array (
    'fkContext' => '1136',
    'fkCategory' => '213',
    'sDescription' => 'Unauthorized access during maintenance through remote diagnostic and support ports.',
  ),
  306 => 
  array (
    'fkContext' => '1137',
    'fkCategory' => '236',
    'sDescription' => 'Unauthorized access through network services and critical servers.',
  ),
  307 => 
  array (
    'fkContext' => '1138',
    'fkCategory' => '236',
    'sDescription' => 'Unauthorized access by external users to the internal network through server and services publicly available.',
  ),
  308 => 
  array (
    'fkContext' => '1139',
    'fkCategory' => '236',
    'sDescription' => 'Unauthorized access to sensitive information that are transmitted in common networks.',
  ),
  309 => 
  array (
    'fkContext' => '1140',
    'fkCategory' => '236',
    'sDescription' => 'Unauthorized access to services that handle sensitive information available in common networks.',
  ),
  310 => 
  array (
    'fkContext' => '1141',
    'fkCategory' => '236',
    'sDescription' => 'Access to sensitive information that are unnecessarily transmitted in networks.',
  ),
  311 => 
  array (
    'fkContext' => '1142',
    'fkCategory' => '236',
    'sDescription' => 'Inability to filter information transmitted in shared networks.',
  ),
  312 => 
  array (
    'fkContext' => '1143',
    'fkCategory' => '236',
    'sDescription' => 'Unauthorized access to internal network through public networks, such as Internet.',
  ),
  313 => 
  array (
    'fkContext' => '1144',
    'fkCategory' => '236',
    'sDescription' => 'Users from a network having unauthorized access to information of another network.',
  ),
  314 => 
  array (
    'fkContext' => '1145',
    'fkCategory' => '236',
    'sDescription' => 'Attack to internal network and servers through public networks such as Internet.',
  ),
  315 => 
  array (
    'fkContext' => '1146',
    'fkCategory' => '236',
    'sDescription' => 'Attack to internal network and servers through interconnected networks.',
  ),
  316 => 
  array (
    'fkContext' => '1152',
    'fkCategory' => '236',
    'sDescription' => 'External denial-of-service attack.',
  ),
  317 => 
  array (
    'fkContext' => '1153',
    'fkCategory' => '236',
    'sDescription' => 'Internal denial-of-service attack.',
  ),
  318 => 
  array (
    'fkContext' => '1154',
    'fkCategory' => '236',
    'sDescription' => 'Unauthorized access to network through a faulty routing.',
  ),
  319 => 
  array (
    'fkContext' => '1155',
    'fkCategory' => '236',
    'sDescription' => 'Failed to access external network due to routing failures.',
  ),
  320 => 
  array (
    'fkContext' => '1156',
    'fkCategory' => '236',
    'sDescription' => 'Failed to access internal network due to routing failures.',
  ),
  321 => 
  array (
    'fkContext' => '1157',
    'fkCategory' => '236',
    'sDescription' => 'Unauthorized access to the internal network equipment by poor configuration of network addresses..',
  ),
  322 => 
  array (
    'fkContext' => '1158',
    'fkCategory' => '236',
    'sDescription' => 'Unauthorized use of routing devices.',
  ),
  323 => 
  array (
    'fkContext' => '1159',
    'fkCategory' => '236',
    'sDescription' => 'Inadequate routing of information transmitted through internal and external networks.',
  ),
  324 => 
  array (
    'fkContext' => '1203',
    'fkCategory' => '213',
    'sDescription' => 'Unauthorized access to equipment during users\' absence.',
  ),
  325 => 
  array (
    'fkContext' => '1230',
    'fkCategory' => '213',
    'sDescription' => 'Unauthorized access to information stored in mobile computers (notebooks, palmtops, cell phones) due to equipment theft.',
  ),
  326 => 
  array (
    'fkContext' => '1233',
    'fkCategory' => '213',
    'sDescription' => 'Contamination of mobile resources by malicious code.',
  ),
  327 => 
  array (
    'fkContext' => '1234',
    'fkCategory' => '213',
    'sDescription' => 'Loss of sensitive information by loss of mobile computing resources.',
  ),
  328 => 
  array (
    'fkContext' => '1235',
    'fkCategory' => '213',
    'sDescription' => 'Theft of mobile computing equipments.',
  ),
  329 => 
  array (
    'fkContext' => '1236',
    'fkCategory' => '237',
    'sDescription' => 'Compromising of information remotely accessed due to lack of security in remote environment.',
  ),
  330 => 
  array (
    'fkContext' => '1237',
    'fkCategory' => '237',
    'sDescription' => 'Unauthorized remote access to the operational environment through unprotected teleworking.',
  ),
  331 => 
  array (
    'fkContext' => '1238',
    'fkCategory' => '237',
    'sDescription' => 'Theft of equipment and information of teleworking site.',
  ),
  332 => 
  array (
    'fkContext' => '1239',
    'fkCategory' => '237',
    'sDescription' => 'Interception of traffic transmitted and received during the teleworking.',
  ),
  333 => 
  array (
    'fkContext' => '1240',
    'fkCategory' => '237',
    'sDescription' => 'Compromising of information by unrestricted access to equipment used for teleworking.',
  ),
  334 => 
  array (
    'fkContext' => '1241',
    'fkCategory' => '237',
    'sDescription' => 'Use of personal equipment for teleworking activities.',
  ),
  335 => 
  array (
    'fkContext' => '1320',
    'fkCategory' => '101',
    'sDescription' => 'Undue use of cryptographic controls.',
  ),
  336 => 
  array (
    'fkContext' => '1326',
    'fkCategory' => '101',
    'sDescription' => 'Use of cryptographic controls for information in cases that it would not be necessary.',
  ),
  337 => 
  array (
    'fkContext' => '1327',
    'fkCategory' => '101',
    'sDescription' => 'Lack of use of cryptographic controls for protection of sensitive information.',
  ),
  338 => 
  array (
    'fkContext' => '1329',
    'fkCategory' => '101',
    'sDescription' => 'Lack of strength or quality of the encryption algorithm applied.',
  ),
  339 => 
  array (
    'fkContext' => '1336',
    'fkCategory' => '101',
    'sDescription' => 'Inability to recover lost keys due to use of proprietary encryption techniques or without documentation.',
  ),
  340 => 
  array (
    'fkContext' => '1346',
    'fkCategory' => '101',
    'sDescription' => 'Unauthorized access to cryptographic keys.',
  ),
  341 => 
  array (
    'fkContext' => '1352',
    'fkCategory' => '101',
    'sDescription' => 'Distribution of cryptographic keys to unauthorized users.',
  ),
  342 => 
  array (
    'fkContext' => '1410',
    'fkCategory' => '235',
    'sDescription' => 'Leak of information through the use and exploration of covert channels.',
  ),
  343 => 
  array (
    'fkContext' => '1451',
    'fkCategory' => '279',
    'sDescription' => 'Inability of management to identify occurrence of information security events.',
  ),
  344 => 
  array (
    'fkContext' => '1452',
    'fkCategory' => '279',
    'sDescription' => 'Inability to respond security events.',
  ),
  345 => 
  array (
    'fkContext' => '1453',
    'fkCategory' => '255',
    'sDescription' => 'Lack of knowledge about the appropriate channels to communicate security events.',
  ),
  346 => 
  array (
    'fkContext' => '1454',
    'fkCategory' => '255',
    'sDescription' => 'Lack of notification of occurrences, by lack of knowledge in what should be considered security information events.',
  ),
  347 => 
  array (
    'fkContext' => '1455',
    'fkCategory' => '279',
    'sDescription' => 'Lack of knowledge about security weaknesses in the environment.',
  ),
  348 => 
  array (
    'fkContext' => '1456',
    'fkCategory' => '255',
    'sDescription' => 'Inability to identify security weaknesses in the environment.',
  ),
  349 => 
  array (
    'fkContext' => '1457',
    'fkCategory' => '255',
    'sDescription' => 'Lack of knowledge about the correct channels for the communication of information security weaknesses.',
  ),
  350 => 
  array (
    'fkContext' => '1458',
    'fkCategory' => '279',
    'sDescription' => 'Security weaknesses not previously detected.',
  ),
  351 => 
  array (
    'fkContext' => '1459',
    'fkCategory' => '279',
    'sDescription' => 'Inadequate response to information security incidents.',
  ),
  352 => 
  array (
    'fkContext' => '1460',
    'fkCategory' => '279',
    'sDescription' => 'Delay in response to information security incidents.',
  ),
  353 => 
  array (
    'fkContext' => '1461',
    'fkCategory' => '279',
    'sDescription' => 'Information security incidents responded by inadequate people.',
  ),
  354 => 
  array (
    'fkContext' => '1462',
    'fkCategory' => '279',
    'sDescription' => 'Failure in response to security incidents caused by the inability to assume the responsibilities for the occurrence of events or for problems resolution.',
  ),
  355 => 
  array (
    'fkContext' => '1463',
    'fkCategory' => '279',
    'sDescription' => 'Inability to respond to security incidents.',
  ),
  356 => 
  array (
    'fkContext' => '1464',
    'fkCategory' => '279',
    'sDescription' => 'Recurrence of security incidents.',
  ),
  357 => 
  array (
    'fkContext' => '1465',
    'fkCategory' => '279',
    'sDescription' => 'Inability to quantify the costs impact for the occurrence of information security incidents.',
  ),
  358 => 
  array (
    'fkContext' => '1466',
    'fkCategory' => '279',
    'sDescription' => 'Inability to measure the effectiveness of security controls based on the occurrence of security incidents.',
  ),
  359 => 
  array (
    'fkContext' => '1467',
    'fkCategory' => '279',
    'sDescription' => 'Inability to identify the continual improvement of security controls based on the occurrence of incidents.',
  ),
  360 => 
  array (
    'fkContext' => '1468',
    'fkCategory' => '279',
    'sDescription' => 'Difficulty in a critical analysis of the information security management process due to lack of evaluation and accounting of security incidents.',
  ),
  361 => 
  array (
    'fkContext' => '1469',
    'fkCategory' => '279',
    'sDescription' => 'Inability to identify the causes of security incidents due to lack of evidences.',
  ),
  362 => 
  array (
    'fkContext' => '1470',
    'fkCategory' => '279',
    'sDescription' => 'Inability to bring an action (civil or criminal) against the responsible (person and/or organization) for the security incidents due to lack of evidences.',
  ),
  363 => 
  array (
    'fkContext' => '1471',
    'fkCategory' => '279',
    'sDescription' => 'Inability to ensure the admissibility of evidences collected.',
  ),
  364 => 
  array (
    'fkContext' => '1473',
    'fkCategory' => '279',
    'sDescription' => 'Impossibility to collect evidences in environments outside the organization jurisdiction.',
  ),
  365 => 
  array (
    'fkContext' => '1474',
    'fkCategory' => '100',
    'sDescription' => 'Stop critical business processes due to unavailability of the asset.',
  ),
  366 => 
  array (
    'fkContext' => '1476',
    'fkCategory' => '182',
    'sDescription' => 'Stop critical business processes due to unavailability of the asset.',
  ),
  367 => 
  array (
    'fkContext' => '1477',
    'fkCategory' => '255',
    'sDescription' => 'Stop critical business processes due to unavailability of the asset.',
  ),
  368 => 
  array (
    'fkContext' => '1480',
    'fkCategory' => '255',
    'sDescription' => 'Stop critical business processes due to human errors.',
  ),
  369 => 
  array (
    'fkContext' => '1481',
    'fkCategory' => '273',
    'sDescription' => 'Stop of services by unavailability of information assets.',
  ),
  370 => 
  array (
    'fkContext' => '1482',
    'fkCategory' => '279',
    'sDescription' => 'Impact on execution of critical business process after information security incidents.',
  ),
  371 => 
  array (
    'fkContext' => '1483',
    'fkCategory' => '279',
    'sDescription' => 'Security failure in processes working in a contingency regime due to the fact of not having the same security controls of normal processes.',
  ),
  372 => 
  array (
    'fkContext' => '1484',
    'fkCategory' => '255',
    'sDescription' => 'Incapacity to reach job facilities due to social disturbance.',
  ),
  373 => 
  array (
    'fkContext' => '1485',
    'fkCategory' => '279',
    'sDescription' => 'Lack of knowledge about events and incidents involving information security.',
  ),
  374 => 
  array (
    'fkContext' => '1486',
    'fkCategory' => '279',
    'sDescription' => 'Lack of knowledge about the impact of events and incidents of information security on critical business processes.',
  ),
  375 => 
  array (
    'fkContext' => '1487',
    'fkCategory' => '279',
    'sDescription' => 'Lack of a comprehensive strategy to ensure the recovery of main processes.',
  ),
  376 => 
  array (
    'fkContext' => '1488',
    'fkCategory' => '279',
    'sDescription' => 'Inability to recover main business processes in accordance with the continuity strategy due to lack of knowledge of people involved.',
  ),
  377 => 
  array (
    'fkContext' => '1489',
    'fkCategory' => '279',
    'sDescription' => 'Inability to recover the main business processes in time due to lack of knowledge of people involved.',
  ),
  378 => 
  array (
    'fkContext' => '1490',
    'fkCategory' => '279',
    'sDescription' => 'Errors in executing the continuity strategies due to lack of documentation about what actions should be done by people involved.',
  ),
  379 => 
  array (
    'fkContext' => '1491',
    'fkCategory' => '279',
    'sDescription' => 'Inability to meet the continuity plans and strategies developed due to lack of adequate training.',
  ),
  380 => 
  array (
    'fkContext' => '1493',
    'fkCategory' => '183',
    'sDescription' => 'Compromising of information in contingency environments due to failure in the implementation of security requirements in accordance with the original environment.',
  ),
  381 => 
  array (
    'fkContext' => '1494',
    'fkCategory' => '279',
    'sDescription' => 'Lack of the necessary resources in remote sites for recovery of the main processes.',
  ),
  382 => 
  array (
    'fkContext' => '1495',
    'fkCategory' => '279',
    'sDescription' => 'Unavailability of access to the continuity plans due to unavailable plans.',
  ),
  383 => 
  array (
    'fkContext' => '1496',
    'fkCategory' => '279',
    'sDescription' => 'Difficulty to execute the business continuity plans due to lack of standardization.',
  ),
  384 => 
  array (
    'fkContext' => '1497',
    'fkCategory' => '279',
    'sDescription' => 'Failure in putting into action the business continuity plans, due to lack of clear conditions in which the plan should be activated.',
  ),
  385 => 
  array (
    'fkContext' => '1498',
    'fkCategory' => '279',
    'sDescription' => 'Failure in putting into action the business continuity plans, due to lack of emergency procedures that should be done just after the activation conditions had been confirmed.',
  ),
  386 => 
  array (
    'fkContext' => '1499',
    'fkCategory' => '279',
    'sDescription' => 'Failure in putting into action the business continuity plans, due to lack of procedures that describe the actions to recovery of critical services and assets.',
  ),
  387 => 
  array (
    'fkContext' => '1500',
    'fkCategory' => '279',
    'sDescription' => 'Failure in execution of the Business Continuity Plan due to lack of clearly defined responsibilities.',
  ),
  388 => 
  array (
    'fkContext' => '1501',
    'fkCategory' => '279',
    'sDescription' => 'Failure in execution of the Business Continuity Plan due to not updating these plans.',
  ),
  389 => 
  array (
    'fkContext' => '1502',
    'fkCategory' => '279',
    'sDescription' => 'Failure in execution of the Business Continuity Plan due to lack of tests that ensure the correct and proper function as it has been required.',
  ),
  390 => 
  array (
    'fkContext' => '1503',
    'fkCategory' => '279',
    'sDescription' => 'Violation of laws, contracts, statutes, and regulations that involve the information security due to unknown legal requirements',
  ),
  391 => 
  array (
    'fkContext' => '1504',
    'fkCategory' => '279',
    'sDescription' => 'Violation of laws, contracts, statutes, and regulations that involve the information security due to failure in the communication of legal requirements to people responsible for information systems.',
  ),
  392 => 
  array (
    'fkContext' => '1506',
    'fkCategory' => '100',
    'sDescription' => 'Violation of intellectual property rights in the use of information.',
  ),
  393 => 
  array (
    'fkContext' => '1509',
    'fkCategory' => '100',
    'sDescription' => 'Loss of important records.',
  ),
  394 => 
  array (
    'fkContext' => '1510',
    'fkCategory' => '100',
    'sDescription' => 'Counterfeit of important records.',
  ),
  395 => 
  array (
    'fkContext' => '1511',
    'fkCategory' => '100',
    'sDescription' => 'Unauthorized access to important records due to the application of insufficient security controls.',
  ),
  396 => 
  array (
    'fkContext' => '1513',
    'fkCategory' => '100',
    'sDescription' => 'Insufficient retention periods for important record.',
  ),
  397 => 
  array (
    'fkContext' => '1515',
    'fkCategory' => '102',
    'sDescription' => 'Unauthorized disclosure of data and personal information.',
  ),
  398 => 
  array (
    'fkContext' => '1516',
    'fkCategory' => '102',
    'sDescription' => 'Unauthorized access to data and personal information.',
  ),
  399 => 
  array (
    'fkContext' => '1517',
    'fkCategory' => '102',
    'sDescription' => 'Capture of personal information through illegal mechanisms.',
  ),
  400 => 
  array (
    'fkContext' => '1518',
    'fkCategory' => '102',
    'sDescription' => 'Undue use of personal information.',
  ),
  401 => 
  array (
    'fkContext' => '1521',
    'fkCategory' => '229',
    'sDescription' => 'Security fault by inappropriate use of information asset.',
  ),
  402 => 
  array (
    'fkContext' => '1527',
    'fkCategory' => '279',
    'sDescription' => 'Violation of legal aspects during the monitoring of  the use of information processing facilities.',
  ),
  403 => 
  array (
    'fkContext' => '1533',
    'fkCategory' => '101',
    'sDescription' => 'Violation of legal aspects in the use of cryptographic controls.',
  ),
  404 => 
  array (
    'fkContext' => '1539',
    'fkCategory' => '101',
    'sDescription' => 'Violation of legal aspects in the transmission of the cryptographic information to other countries.',
  ),
  405 => 
  array (
    'fkContext' => '1540',
    'fkCategory' => '279',
    'sDescription' => 'Failure in execution of information security procedures.',
  ),
  406 => 
  array (
    'fkContext' => '1542',
    'fkCategory' => '182',
    'sDescription' => 'Failure in implementation of technical aspects required by the Standards and security information procedures.',
  ),
  407 => 
  array (
    'fkContext' => '1543',
    'fkCategory' => '228',
    'sDescription' => 'Failure in implementation of technical aspects required by the Standards and security information procedures.',
  ),
  408 => 
  array (
    'fkContext' => '1544',
    'fkCategory' => '279',
    'sDescription' => 'Interruption of business processes due to the information systems audit.',
  ),
  409 => 
  array (
    'fkContext' => '5679',
    'fkCategory' => '100',
    'sDescription' => 'Compromising of information stored on media due to inappropriate storage.',
  ),
  410 => 
  array (
    'fkContext' => '5680',
    'fkCategory' => '100',
    'sDescription' => 'Loss of information stored for long periods of time due to media deterioration.',
  ),
  411 => 
  array (
    'fkContext' => '5681',
    'fkCategory' => '100',
    'sDescription' => 'Unauthorized access to information stored in media disposal.',
  ),
  412 => 
  array (
    'fkContext' => '5682',
    'fkCategory' => '100',
    'sDescription' => 'Unauthorized removal of information stored in medias to outside the organization.',
  ),
  413 => 
  array (
    'fkContext' => '5683',
    'fkCategory' => '115',
    'sDescription' => 'Unauthorized access to sensitive information left on copiers.',
  ),
  414 => 
  array (
    'fkContext' => '5684',
    'fkCategory' => '115',
    'sDescription' => 'Unauthorized access to sensitive information left on fax machines.',
  ),
  415 => 
  array (
    'fkContext' => '5685',
    'fkCategory' => '115',
    'sDescription' => 'Unauthorized access to sensitive information left on printers.',
  ),
  416 => 
  array (
    'fkContext' => '5686',
    'fkCategory' => '101',
    'sDescription' => 'Inability to recover important records due to storage in proprietary files format and unavailability of recovery software.',
  ),
  417 => 
  array (
    'fkContext' => '5687',
    'fkCategory' => '101',
    'sDescription' => 'Inability to recover important record stored in media by long period of time.',
  ),
  418 => 
  array (
    'fkContext' => '5688',
    'fkCategory' => '269',
    'sDescription' => 'Lack of commitment by third party to information security.',
  ),
  419 => 
  array (
    'fkContext' => '5690',
    'fkCategory' => '100',
    'sDescription' => 'Inadequate information treatment which conflicts with the criticality of the information.',
  ),
  420 => 
  array (
    'fkContext' => '5691',
    'fkCategory' => '100',
    'sDescription' => 'Loss of critical information.',
  ),
  421 => 
  array (
    'fkContext' => '5692',
    'fkCategory' => '110',
    'sDescription' => 'Compromising of evidence integrity.',
  ),
  422 => 
  array (
    'fkContext' => '5693',
    'fkCategory' => '124',
    'sDescription' => 'Compromising of evidence integrity.',
  ),
  423 => 
  array (
    'fkContext' => '5695',
    'fkCategory' => '226',
    'sDescription' => 'Granting print out by unauthorized users.',
  ),
  424 => 
  array (
    'fkContext' => '5717',
    'fkCategory' => '128',
    'sDescription' => 'Inappropriate use of information assets.',
  ),
  425 => 
  array (
    'fkContext' => '5718',
    'fkCategory' => '128',
    'sDescription' => 'Misuse of information assets due to lack of clear rules about the appropriate and permitted use.',
  ),
  426 => 
  array (
    'fkContext' => '5719',
    'fkCategory' => '128',
    'sDescription' => 'Execution of unauthorized changes.',
  ),
  427 => 
  array (
    'fkContext' => '5720',
    'fkCategory' => '128',
    'sDescription' => 'Insertion of new vulnerabilities after changes.',
  ),
  428 => 
  array (
    'fkContext' => '5721',
    'fkCategory' => '128',
    'sDescription' => 'Misuse of systems.',
  ),
  429 => 
  array (
    'fkContext' => '5722',
    'fkCategory' => '128',
    'sDescription' => 'Accidental misuse of systems.',
  ),
  430 => 
  array (
    'fkContext' => '5723',
    'fkCategory' => '128',
    'sDescription' => 'Computer virus contamination.',
  ),
  431 => 
  array (
    'fkContext' => '5724',
    'fkCategory' => '128',
    'sDescription' => 'Loss of critical software.',
  ),
  432 => 
  array (
    'fkContext' => '5725',
    'fkCategory' => '128',
    'sDescription' => 'Stop critical business processes due to unavailability of the asset.',
  ),
  433 => 
  array (
    'fkContext' => '5726',
    'fkCategory' => '128',
    'sDescription' => 'Violation of intellectual property rights in the use of software.',
  ),
  434 => 
  array (
    'fkContext' => '5727',
    'fkCategory' => '128',
    'sDescription' => 'Security fault by inappropriate use of information asset.',
  ),
  435 => 
  array (
    'fkContext' => '5728',
    'fkCategory' => '128',
    'sDescription' => 'Misuse of information assets due to non-existence of clear rules about the adequate and permitted use.',
  ),
  436 => 
  array (
    'fkContext' => '5729',
    'fkCategory' => '128',
    'sDescription' => 'Failure in implementation of technical aspects required by the Standards and security information procedures.',
  ),
  437 => 
  array (
    'fkContext' => '5731',
    'fkCategory' => '128',
    'sDescription' => 'Inability to manage systems and services due to key person dependency.',
  ),
  438 => 
  array (
    'fkContext' => '5732',
    'fkCategory' => '128',
    'sDescription' => 'Exceed of human faults due to installation of complex usability systems.',
  ),
  439 => 
  array (
    'fkContext' => '5733',
    'fkCategory' => '129',
    'sDescription' => 'Unauthorized access to the operating system of operational facilities by developers.',
  ),
  440 => 
  array (
    'fkContext' => '5734',
    'fkCategory' => '128',
    'sDescription' => 'Unauthorized access to systems or information by users whose contract with has been terminated.',
  ),
  441 => 
  array (
    'fkContext' => '5735',
    'fkCategory' => '128',
    'sDescription' => 'Unauthorized access to systems in abnormal working hours.',
  ),
  442 => 
  array (
    'fkContext' => '5736',
    'fkCategory' => '129',
    'sDescription' => 'Unauthorized access to operating systems due to failure in procedure to validate users log-on.',
  ),
  443 => 
  array (
    'fkContext' => '5737',
    'fkCategory' => '128',
    'sDescription' => 'Undue or unauthorized access to systems and information caused by lack of access rights or privileges of employees who changed their position and / or function.',
  ),
  444 => 
  array (
    'fkContext' => '5738',
    'fkCategory' => '129',
    'sDescription' => 'Violation of systems security controls through use of system utilities.',
  ),
  445 => 
  array (
    'fkContext' => '5739',
    'fkCategory' => '128',
    'sDescription' => 'Inadequate allocation of access rights and privileges due to lack of an access control policy.',
  ),
  446 => 
  array (
    'fkContext' => '5740',
    'fkCategory' => '129',
    'sDescription' => 'Use of system utilities by unauthorized users.',
  ),
  447 => 
  array (
    'fkContext' => '5741',
    'fkCategory' => '128',
    'sDescription' => 'Access rights violation due to lack of clear rules that establish what is permitted and what is forbidden.',
  ),
  448 => 
  array (
    'fkContext' => '5742',
    'fkCategory' => '129',
    'sDescription' => 'Corruption of operating systems caused by inadequate installation of softwares.',
  ),
  449 => 
  array (
    'fkContext' => '5743',
    'fkCategory' => '128',
    'sDescription' => 'Incompatibility between access rights granted and the real need of access based in business requirements.',
  ),
  450 => 
  array (
    'fkContext' => '5744',
    'fkCategory' => '128',
    'sDescription' => 'Users with unnecessary access rights regarding their job roles.',
  ),
  451 => 
  array (
    'fkContext' => '5745',
    'fkCategory' => '128',
    'sDescription' => 'Incompatibility between access control rules and guidelines for information classification.',
  ),
  452 => 
  array (
    'fkContext' => '5746',
    'fkCategory' => '128',
    'sDescription' => 'Inadequate grant of access rights.',
  ),
  453 => 
  array (
    'fkContext' => '5747',
    'fkCategory' => '128',
    'sDescription' => 'Unable to make users responsible for their activities due to lack of unique user IDs.',
  ),
  454 => 
  array (
    'fkContext' => '5748',
    'fkCategory' => '128',
    'sDescription' => 'Users with unnecessary access rights due to lack of a clear procedure to remove access rights in situations of inexistence or cancelation of contract.',
  ),
  455 => 
  array (
    'fkContext' => '5749',
    'fkCategory' => '128',
    'sDescription' => 'Incompatibility of granted access level and the business purposes.',
  ),
  456 => 
  array (
    'fkContext' => '5750',
    'fkCategory' => '128',
    'sDescription' => 'Unauthorized access caused by lack of formalization in users permissions.',
  ),
  457 => 
  array (
    'fkContext' => '5751',
    'fkCategory' => '128',
    'sDescription' => 'Inappropriate use of privileged access.',
  ),
  458 => 
  array (
    'fkContext' => '5752',
    'fkCategory' => '128',
    'sDescription' => 'Inadequate privilege assignment to users.',
  ),
  459 => 
  array (
    'fkContext' => '5753',
    'fkCategory' => '128',
    'sDescription' => 'Redundant user IDs issued to other users.',
  ),
  460 => 
  array (
    'fkContext' => '5754',
    'fkCategory' => '139',
    'sDescription' => 'Use of real information in test databases.',
  ),
  461 => 
  array (
    'fkContext' => '5755',
    'fkCategory' => '128',
    'sDescription' => 'Assign privileges not related to user ID, making impossible to identify or monitor system activities.',
  ),
  462 => 
  array (
    'fkContext' => '5756',
    'fkCategory' => '128',
    'sDescription' => 'Unrestricted use of privileged users in activities that it would not be necessary.',
  ),
  463 => 
  array (
    'fkContext' => '5757',
    'fkCategory' => '128',
    'sDescription' => 'Compromising of password during the process of delivery to the users.',
  ),
  464 => 
  array (
    'fkContext' => '5758',
    'fkCategory' => '128',
    'sDescription' => 'Access to systems through temporary passwords of general knowledge.',
  ),
  465 => 
  array (
    'fkContext' => '5759',
    'fkCategory' => '128',
    'sDescription' => 'Inappropriate access rights due or privileges to a faulty update after changes of users\' roles or jobs.',
  ),
  466 => 
  array (
    'fkContext' => '5760',
    'fkCategory' => '128',
    'sDescription' => 'Unauthorized access rights or privileges granted to system users.',
  ),
  467 => 
  array (
    'fkContext' => '5761',
    'fkCategory' => '128',
    'sDescription' => 'Access rights that have not been removed after termination of employment.',
  ),
  468 => 
  array (
    'fkContext' => '5762',
    'fkCategory' => '128',
    'sDescription' => 'Unauthorized external access to applications and services that are available only to internal network users.',
  ),
  469 => 
  array (
    'fkContext' => '5763',
    'fkCategory' => '128',
    'sDescription' => 'Knowledge of users\' passwords by the personnel responsible for managing passwords.',
  ),
  470 => 
  array (
    'fkContext' => '5764',
    'fkCategory' => '128',
    'sDescription' => 'Disclosure of passwords to other users by the owner of the password.',
  ),
  471 => 
  array (
    'fkContext' => '5765',
    'fkCategory' => '128',
    'sDescription' => 'Disclosure of password by lack of care from users.',
  ),
  472 => 
  array (
    'fkContext' => '5766',
    'fkCategory' => '177',
    'sDescription' => 'Cross Site Scripting (XSS)',
  ),
  473 => 
  array (
    'fkContext' => '5767',
    'fkCategory' => '128',
    'sDescription' => 'Provide information about the system during the log-in procedure, that could aid an unauthorized user to gain access.',
  ),
  474 => 
  array (
    'fkContext' => '5768',
    'fkCategory' => '128',
    'sDescription' => 'Capture of passwords transmitted through network during the log-on session over a network.',
  ),
  475 => 
  array (
    'fkContext' => '5769',
    'fkCategory' => '128',
    'sDescription' => 'Unauthorized access to systems through failure in log-on procedure.',
  ),
  476 => 
  array (
    'fkContext' => '5770',
    'fkCategory' => '128',
    'sDescription' => 'Use of the same password for a long period of time.',
  ),
  477 => 
  array (
    'fkContext' => '5771',
    'fkCategory' => '128',
    'sDescription' => 'Compromising of passwords included in an automated log-on process, such as stored in a macro or function key.',
  ),
  478 => 
  array (
    'fkContext' => '5772',
    'fkCategory' => '128',
    'sDescription' => 'Do not enforce a choice of quality passwords by users.',
  ),
  479 => 
  array (
    'fkContext' => '5773',
    'fkCategory' => '128',
    'sDescription' => 'Unauthorized disclosure of passwords to the administrators or support operators in case of a necessary change of passwords.',
  ),
  480 => 
  array (
    'fkContext' => '5774',
    'fkCategory' => '128',
    'sDescription' => 'Acquisition of software with inadequate security controls.',
  ),
  481 => 
  array (
    'fkContext' => '5775',
    'fkCategory' => '128',
    'sDescription' => 'Lack of formal change control procedures.',
  ),
  482 => 
  array (
    'fkContext' => '5776',
    'fkCategory' => '128',
    'sDescription' => 'No formal testing and acquisition process is used when products are purchased.',
  ),
  483 => 
  array (
    'fkContext' => '5777',
    'fkCategory' => '128',
    'sDescription' => 'Low systems performance due to lack of testing and evaluation.',
  ),
  484 => 
  array (
    'fkContext' => '5778',
    'fkCategory' => '177',
    'sDescription' => 'Injection Flaws',
  ),
  485 => 
  array (
    'fkContext' => '5779',
    'fkCategory' => '128',
    'sDescription' => 'Failure in the continuity plans, due to lack of updates in the new systems.',
  ),
  486 => 
  array (
    'fkContext' => '5780',
    'fkCategory' => '128',
    'sDescription' => 'Using of systems that have not been tested and homologated.',
  ),
  487 => 
  array (
    'fkContext' => '5781',
    'fkCategory' => '177',
    'sDescription' => 'Insecure Remote File Included',
  ),
  488 => 
  array (
    'fkContext' => '5782',
    'fkCategory' => '128',
    'sDescription' => 'Systems installation without proper evaluation.',
  ),
  489 => 
  array (
    'fkContext' => '5783',
    'fkCategory' => '177',
    'sDescription' => 'Insecure Direct Object Reference',
  ),
  490 => 
  array (
    'fkContext' => '5784',
    'fkCategory' => '128',
    'sDescription' => 'Systems installation that do not meet the business requirements.',
  ),
  491 => 
  array (
    'fkContext' => '5785',
    'fkCategory' => '128',
    'sDescription' => 'Compromising of security configurations due to performed changes.',
  ),
  492 => 
  array (
    'fkContext' => '5786',
    'fkCategory' => '177',
    'sDescription' => 'Cross Site Request Forgery (CSRF)',
  ),
  493 => 
  array (
    'fkContext' => '5788',
    'fkCategory' => '128',
    'sDescription' => 'Inability to identify information leakage due to lack of regular monitoring of system activities.',
  ),
  494 => 
  array (
    'fkContext' => '5790',
    'fkCategory' => '128',
    'sDescription' => 'Inability to indentify actions made by users in the system.',
  ),
  495 => 
  array (
    'fkContext' => '5791',
    'fkCategory' => '128',
    'sDescription' => 'Inability to indentify exceptions in the system normal activities.',
  ),
  496 => 
  array (
    'fkContext' => '5792',
    'fkCategory' => '177',
    'sDescription' => 'Failure to Restrict URL Access',
  ),
  497 => 
  array (
    'fkContext' => '5793',
    'fkCategory' => '128',
    'sDescription' => 'Inability to identify security events in systems.',
  ),
  498 => 
  array (
    'fkContext' => '5794',
    'fkCategory' => '128',
    'sDescription' => 'Inability to identify unauthorized system access attempts.',
  ),
  499 => 
  array (
    'fkContext' => '5795',
    'fkCategory' => '177',
    'sDescription' => 'Unauthorized access to information of e-commerce payment that are traded in public networks.',
  ),
  500 => 
  array (
    'fkContext' => '5796',
    'fkCategory' => '128',
    'sDescription' => 'Unauthorized activities in the system.',
  ),
  501 => 
  array (
    'fkContext' => '5797',
    'fkCategory' => '177',
    'sDescription' => 'Unauthorized change of information of e-commerce payment that are read in public networks.',
  ),
  502 => 
  array (
    'fkContext' => '5798',
    'fkCategory' => '128',
    'sDescription' => 'Unauthorized access to the system.',
  ),
  503 => 
  array (
    'fkContext' => '5799',
    'fkCategory' => '128',
    'sDescription' => 'Unauthorized change of system logs by external users (e.g. Hackers, intruders).',
  ),
  504 => 
  array (
    'fkContext' => '5800',
    'fkCategory' => '177',
    'sDescription' => 'Unauthorized change of prices and information in the e-commerce website.',
  ),
  505 => 
  array (
    'fkContext' => '5801',
    'fkCategory' => '128',
    'sDescription' => 'Unauthorized change of system logs by administrators or privileged users.',
  ),
  506 => 
  array (
    'fkContext' => '5802',
    'fkCategory' => '128',
    'sDescription' => 'Unauthorized reading of system logs.',
  ),
  507 => 
  array (
    'fkContext' => '5803',
    'fkCategory' => '177',
    'sDescription' => 'Violation of consumer privacy laws during the e-commerce payment.',
  ),
  508 => 
  array (
    'fkContext' => '5804',
    'fkCategory' => '128',
    'sDescription' => 'Inability to identify unauthorized activities executed by privileged users such as system administrator and system operator.',
  ),
  509 => 
  array (
    'fkContext' => '5805',
    'fkCategory' => '177',
    'sDescription' => 'Change in e-commerce website trough undue access by hackers and/or unauthorized users.',
  ),
  510 => 
  array (
    'fkContext' => '5806',
    'fkCategory' => '128',
    'sDescription' => 'Inability to detect errors and failures in working systems by lack of failure records.',
  ),
  511 => 
  array (
    'fkContext' => '5807',
    'fkCategory' => '177',
    'sDescription' => 'Provision of incorrect information for the payment of purchases made in e-commerce website.',
  ),
  512 => 
  array (
    'fkContext' => '5808',
    'fkCategory' => '128',
    'sDescription' => 'Delay in restoring controls after failure in a system due to lack of fault logging.',
  ),
  513 => 
  array (
    'fkContext' => '5809',
    'fkCategory' => '177',
    'sDescription' => 'Unavailability of the e-commerce website.',
  ),
  514 => 
  array (
    'fkContext' => '5810',
    'fkCategory' => '128',
    'sDescription' => 'Differences between the time shown in systems and the time that a real action occurred.',
  ),
  515 => 
  array (
    'fkContext' => '5811',
    'fkCategory' => '128',
    'sDescription' => 'Inability to identify the precise time that an action has been made due to lack of clock synchronization.',
  ),
  516 => 
  array (
    'fkContext' => '5812',
    'fkCategory' => '128',
    'sDescription' => 'Lack of reliability in evidences used in the forensic investigation due to lack of clock synchronization.',
  ),
  517 => 
  array (
    'fkContext' => '5813',
    'fkCategory' => '128',
    'sDescription' => 'Undue use of cryptographic controls.',
  ),
  518 => 
  array (
    'fkContext' => '5814',
    'fkCategory' => '128',
    'sDescription' => 'Lack of strength or quality of the encryption algorithm applied.',
  ),
  519 => 
  array (
    'fkContext' => '5815',
    'fkCategory' => '138',
    'sDescription' => 'Injection of unauthorized code into operational facilities.',
  ),
  520 => 
  array (
    'fkContext' => '5816',
    'fkCategory' => '128',
    'sDescription' => 'Use of proprietary encryption techniques or lack of documentation that allows the recovery of encrypted information in the case of lost, compromised or damaged keys.',
  ),
  521 => 
  array (
    'fkContext' => '5817',
    'fkCategory' => '138',
    'sDescription' => 'Insertion of untested code into operational facilities.',
  ),
  522 => 
  array (
    'fkContext' => '5818',
    'fkCategory' => '128',
    'sDescription' => 'Unauthorized access to cryptographic keys.',
  ),
  523 => 
  array (
    'fkContext' => '5819',
    'fkCategory' => '138',
    'sDescription' => 'Failure in working systems due to differences between operational facilities and development facilities.',
  ),
  524 => 
  array (
    'fkContext' => '5820',
    'fkCategory' => '128',
    'sDescription' => 'Distribution of cryptographic keys to unauthorized users.',
  ),
  525 => 
  array (
    'fkContext' => '5821',
    'fkCategory' => '138',
    'sDescription' => 'Use of development tools that facilitate the unauthorized access to the information or system.',
  ),
  526 => 
  array (
    'fkContext' => '5822',
    'fkCategory' => '128',
    'sDescription' => 'Loss of cryptographic keys.',
  ),
  527 => 
  array (
    'fkContext' => '5823',
    'fkCategory' => '128',
    'sDescription' => 'Inability to recover encrypted information due to lack of key management.',
  ),
  528 => 
  array (
    'fkContext' => '5824',
    'fkCategory' => '138',
    'sDescription' => 'Intentional input of incorrect data into the system.',
  ),
  529 => 
  array (
    'fkContext' => '5825',
    'fkCategory' => '128',
    'sDescription' => 'Incorrect storage of the equipment used to generate, store and archive cryptographic keys.',
  ),
  530 => 
  array (
    'fkContext' => '5826',
    'fkCategory' => '138',
    'sDescription' => 'Input of incorrect data due to typing errors.',
  ),
  531 => 
  array (
    'fkContext' => '5827',
    'fkCategory' => '128',
    'sDescription' => 'Falsification of cryptographic keys.',
  ),
  532 => 
  array (
    'fkContext' => '5828',
    'fkCategory' => '138',
    'sDescription' => 'Incorrect transmission of on-line transactions',
  ),
  533 => 
  array (
    'fkContext' => '5829',
    'fkCategory' => '128',
    'sDescription' => 'Violation of legal aspects in the use of cryptographic controls.',
  ),
  534 => 
  array (
    'fkContext' => '5830',
    'fkCategory' => '138',
    'sDescription' => 'Routing errors when sending or receiving on-line transactions.',
  ),
  535 => 
  array (
    'fkContext' => '5831',
    'fkCategory' => '128',
    'sDescription' => 'Violation of legal aspects in the transmission of the cryptographic information to other countries.',
  ),
  536 => 
  array (
    'fkContext' => '5832',
    'fkCategory' => '138',
    'sDescription' => 'Unauthorized changes in on-line transactions messages.',
  ),
  537 => 
  array (
    'fkContext' => '5833',
    'fkCategory' => '128',
    'sDescription' => 'Falsification of messages.',
  ),
  538 => 
  array (
    'fkContext' => '5834',
    'fkCategory' => '138',
    'sDescription' => 'Unauthorized reading and disclosure of information inside on-line transactions messages.',
  ),
  539 => 
  array (
    'fkContext' => '5835',
    'fkCategory' => '128',
    'sDescription' => 'Repudiation  of messages transmitted among systems.',
  ),
  540 => 
  array (
    'fkContext' => '5836',
    'fkCategory' => '138',
    'sDescription' => 'Unauthorized replay of messages on on-line transactions.',
  ),
  541 => 
  array (
    'fkContext' => '5837',
    'fkCategory' => '128',
    'sDescription' => 'Unauthorized transmission of information to application outputs due to inability to filter information.',
  ),
  542 => 
  array (
    'fkContext' => '5838',
    'fkCategory' => '128',
    'sDescription' => 'Presenting incorrect or out of context results.',
  ),
  543 => 
  array (
    'fkContext' => '5839',
    'fkCategory' => '138',
    'sDescription' => 'Incorrect or unauthorized information in systems directly accessible from the Internet.',
  ),
  544 => 
  array (
    'fkContext' => '5840',
    'fkCategory' => '128',
    'sDescription' => 'Compromising of the system through the use of buffer overflow technique.',
  ),
  545 => 
  array (
    'fkContext' => '5841',
    'fkCategory' => '138',
    'sDescription' => 'Unauthorized publication of data by using information unapproved and inserted in systems directly accessible from the Internet.',
  ),
  546 => 
  array (
    'fkContext' => '5843',
    'fkCategory' => '128',
    'sDescription' => 'Compromising of information and system functions caused by failure in another interconnected system.',
  ),
  547 => 
  array (
    'fkContext' => '5844',
    'fkCategory' => '138',
    'sDescription' => 'Violations of intellectual property laws through undue providing of information systems directly accessible from the Internet.',
  ),
  548 => 
  array (
    'fkContext' => '5845',
    'fkCategory' => '138',
    'sDescription' => 'Lack or failure in security requirements analysis and specification when developing a system.',
  ),
  549 => 
  array (
    'fkContext' => '5846',
    'fkCategory' => '128',
    'sDescription' => 'Insertions of new hardware and/or software not compatible with the rest of system devices.',
  ),
  550 => 
  array (
    'fkContext' => '5848',
    'fkCategory' => '128',
    'sDescription' => 'Unauthorized access to systems through users that chosen a low quality password.',
  ),
  551 => 
  array (
    'fkContext' => '5849',
    'fkCategory' => '138',
    'sDescription' => 'Inability to integrate security controls due to the fact that statements of business requirements has been made after the development of the system.',
  ),
  552 => 
  array (
    'fkContext' => '5850',
    'fkCategory' => '128',
    'sDescription' => 'Unauthorized access to business process description in system documentation.',
  ),
  553 => 
  array (
    'fkContext' => '5851',
    'fkCategory' => '128',
    'sDescription' => 'Unauthorized disclosure of information due to systems interconnection.',
  ),
  554 => 
  array (
    'fkContext' => '5852',
    'fkCategory' => '138',
    'sDescription' => 'Input of incomplete data into the system.',
  ),
  555 => 
  array (
    'fkContext' => '5853',
    'fkCategory' => '128',
    'sDescription' => 'Violation of legal requirements by clients while using internal systems.',
  ),
  556 => 
  array (
    'fkContext' => '5854',
    'fkCategory' => '138',
    'sDescription' => 'Undue alteration of data caused by processing errors.',
  ),
  557 => 
  array (
    'fkContext' => '5855',
    'fkCategory' => '128',
    'sDescription' => 'Unauthorized access to information through sharing data between systems.',
  ),
  558 => 
  array (
    'fkContext' => '5856',
    'fkCategory' => '138',
    'sDescription' => 'Failure in data corruption detection due to lack of testing the plausibility of the input data.',
  ),
  559 => 
  array (
    'fkContext' => '5857',
    'fkCategory' => '128',
    'sDescription' => 'Compromising of systems by failure in other systems that share the same computing environment.',
  ),
  560 => 
  array (
    'fkContext' => '5858',
    'fkCategory' => '138',
    'sDescription' => 'Unauthorized access to data due to lack of a verification list (e.g.: hash, controls run-to-run and program-to-program...).',
  ),
  561 => 
  array (
    'fkContext' => '5859',
    'fkCategory' => '128',
    'sDescription' => 'Compromising of several critical systems due to failure in the computing environment where those systems are installed.',
  ),
  562 => 
  array (
    'fkContext' => '5860',
    'fkCategory' => '138',
    'sDescription' => 'Unauthorized reading of system source code.',
  ),
  563 => 
  array (
    'fkContext' => '5861',
    'fkCategory' => '138',
    'sDescription' => 'Unauthorized change of the system source code.',
  ),
  564 => 
  array (
    'fkContext' => '5862',
    'fkCategory' => '128',
    'sDescription' => 'Insertion of malicious code during emergency procedures in which security controls are not considered.',
  ),
  565 => 
  array (
    'fkContext' => '5863',
    'fkCategory' => '128',
    'sDescription' => 'Contamination of resources caused by insertion of infected files on computers.',
  ),
  566 => 
  array (
    'fkContext' => '5864',
    'fkCategory' => '138',
    'sDescription' => 'Unauthorized access to the development documentation of the system.',
  ),
  567 => 
  array (
    'fkContext' => '5865',
    'fkCategory' => '128',
    'sDescription' => 'Contamination of information assets due to lack updates of malicious code detection and repair software.',
  ),
  568 => 
  array (
    'fkContext' => '5866',
    'fkCategory' => '138',
    'sDescription' => 'Compromising of applications after changes made in the operating system in which the application is executed.',
  ),
  569 => 
  array (
    'fkContext' => '5867',
    'fkCategory' => '128',
    'sDescription' => 'Lack of updates caused by absence of contract support with suppliers.',
  ),
  570 => 
  array (
    'fkContext' => '5868',
    'fkCategory' => '128',
    'sDescription' => 'System unavailability or malfunction due to installation of software updates  without extensive tests.',
  ),
  571 => 
  array (
    'fkContext' => '5869',
    'fkCategory' => '138',
    'sDescription' => 'Compromising of security controls and integrity mechanisms of software packages after changes or customizations.',
  ),
  572 => 
  array (
    'fkContext' => '5870',
    'fkCategory' => '138',
    'sDescription' => 'Unauthorized access to information or environment by outsourced developers.',
  ),
  573 => 
  array (
    'fkContext' => '5871',
    'fkCategory' => '128',
    'sDescription' => 'Lack of critical analysis on changes made in the operational environment.',
  ),
  574 => 
  array (
    'fkContext' => '5872',
    'fkCategory' => '138',
    'sDescription' => 'Inability to ensure the system maintenance after failure of attendance by outsourced developers.',
  ),
  575 => 
  array (
    'fkContext' => '5873',
    'fkCategory' => '128',
    'sDescription' => 'Leak of information through the use and exploration of covert channels.',
  ),
  576 => 
  array (
    'fkContext' => '5874',
    'fkCategory' => '138',
    'sDescription' => 'Indiscriminate use of the software developed by lack of licensing agreements, code ownership and intellectual property rights.',
  ),
  577 => 
  array (
    'fkContext' => '5875',
    'fkCategory' => '128',
    'sDescription' => 'Leak of information through the use of systems with dubious integrity.',
  ),
  578 => 
  array (
    'fkContext' => '5876',
    'fkCategory' => '128',
    'sDescription' => 'Compromise the system due to the presence of well known vulnerabilities',
  ),
  579 => 
  array (
    'fkContext' => '5877',
    'fkCategory' => '138',
    'sDescription' => 'Lack of periodic contents verification of key fields or data files.',
  ),
  580 => 
  array (
    'fkContext' => '5878',
    'fkCategory' => '128',
    'sDescription' => 'Technical vulnerabilities not identified due to lack of assets inventory and of a management process on discovered vulnerabilities.',
  ),
  581 => 
  array (
    'fkContext' => '5879',
    'fkCategory' => '138',
    'sDescription' => 'Information processing in environments  not properly controlled.',
  ),
  582 => 
  array (
    'fkContext' => '5880',
    'fkCategory' => '128',
    'sDescription' => 'Installation of a fix with security problems.',
  ),
  583 => 
  array (
    'fkContext' => '5882',
    'fkCategory' => '128',
    'sDescription' => 'Installation of Trojan horses"."',
  ),
  584 => 
  array (
    'fkContext' => '5883',
    'fkCategory' => '101',
    'sDescription' => 'Unauthorized access by external parties due to interconnecting to the company information, compromising the information security.',
  ),
  585 => 
  array (
    'fkContext' => '5884',
    'fkCategory' => '116',
    'sDescription' => 'Loss of client\'s information.',
  ),
  586 => 
  array (
    'fkContext' => '5885',
    'fkCategory' => '102',
    'sDescription' => 'Loss of client\'s information.',
  ),
  587 => 
  array (
    'fkContext' => '5886',
    'fkCategory' => '296',
    'sDescription' => 'Use of development tools that facilitate the unauthorized access to the information or system.',
  ),
  588 => 
  array (
    'fkContext' => '5887',
    'fkCategory' => '114',
    'sDescription' => 'Inability to restore information due to execution of back-up copies of information only, not of the software necessary to data interpretation.',
  ),
  589 => 
  array (
    'fkContext' => '5888',
    'fkCategory' => '116',
    'sDescription' => 'Capture of personal information through illegal mechanisms.',
  ),
  590 => 
  array (
    'fkContext' => '5889',
    'fkCategory' => '114',
    'sDescription' => 'Compromising of original information and security copies during disasters due to proximity of storage of such copies.',
  ),
  591 => 
  array (
    'fkContext' => '5890',
    'fkCategory' => '114',
    'sDescription' => 'Inability to restore back-up copies due to deteriorating in the storage media.',
  ),
  592 => 
  array (
    'fkContext' => '5891',
    'fkCategory' => '116',
    'sDescription' => 'Unauthorized access to data and personal information.',
  ),
  593 => 
  array (
    'fkContext' => '5892',
    'fkCategory' => '116',
    'sDescription' => 'Unauthorized disclosure of data and personal information.',
  ),
  594 => 
  array (
    'fkContext' => '5893',
    'fkCategory' => '116',
    'sDescription' => 'Undue use of personal information.',
  ),
  595 => 
  array (
    'fkContext' => '5894',
    'fkCategory' => '208',
    'sDescription' => 'Stop critical business processes due to failure in equipment.',
  ),
  596 => 
  array (
    'fkContext' => '5895',
    'fkCategory' => '128',
    'sDescription' => 'Unauthorized use of specific tools for systems audits.',
  ),
  597 => 
  array (
    'fkContext' => '551',
    'fkCategory' => '207',
    'sDescription' => 'Compromising of cabling by installing it in industrial environment.',
  ),
  598 => 
  array (
    'fkContext' => '569',
    'fkCategory' => '207',
    'sDescription' => 'Compromising of cabling caused by instability in the electric power supply.',
  ),
  599 => 
  array (
    'fkContext' => '563',
    'fkCategory' => '207',
    'sDescription' => 'Compromising of cabling due to chemical effects.',
  ),
  600 => 
  array (
    'fkContext' => '554',
    'fkCategory' => '207',
    'sDescription' => 'Compromising of cabling due to electric discharge provoked by lightning.',
  ),
  601 => 
  array (
    'fkContext' => '557',
    'fkCategory' => '207',
    'sDescription' => 'Compromising of cabling due to environment conditions such as humidity and temperature.',
  ),
  602 => 
  array (
    'fkContext' => '572',
    'fkCategory' => '207',
    'sDescription' => 'Compromising of cabling through electromagnetic interference.',
  ),
  603 => 
  array (
    'fkContext' => '543',
    'fkCategory' => '207',
    'sDescription' => 'Unauthorized access to critical cabling caused by installation in a high risk unauthorized location.',
  ),
  604 => 
  array (
    'fkContext' => '5896',
    'fkCategory' => '208',
    'sDescription' => 'Unauthorized access to critical equipment caused by installation in a high risk unauthorized location.',
  ),
  605 => 
  array (
    'fkContext' => '5897',
    'fkCategory' => '208',
    'sDescription' => 'Security fault by inappropriate use of information asset.',
  ),
  606 => 
  array (
    'fkContext' => '5898',
    'fkCategory' => '208',
    'sDescription' => 'Misuse of information assets due to non-existence of clear rules about the adequate and permitted use.',
  ),
  607 => 
  array (
    'fkContext' => '5900',
    'fkCategory' => '128',
    'sDescription' => 'Insufficient system performance.',
  ),
  608 => 
  array (
    'fkContext' => '5901',
    'fkCategory' => '228',
    'sDescription' => 'Stop critical business processes due to unavailability of the asset.',
  ),
  609 => 
  array (
    'fkContext' => '5903',
    'fkCategory' => '235',
    'sDescription' => 'Unauthorized access to information through communications interception.',
  ),
  610 => 
  array (
    'fkContext' => '5904',
    'fkCategory' => '235',
    'sDescription' => 'Unauthorized changes of data received or sent.',
  ),
  611 => 
  array (
    'fkContext' => '5905',
    'fkCategory' => '235',
    'sDescription' => 'Receiving malicious code through communication facilities.',
  ),
  612 => 
  array (
    'fkContext' => '5907',
    'fkCategory' => '235',
    'sDescription' => 'Receiving unauthorized information through files attached.',
  ),
  613 => 
  array (
    'fkContext' => '5908',
    'fkCategory' => '235',
    'sDescription' => 'Receiving of unauthorized information through download of data.',
  ),
  614 => 
  array (
    'fkContext' => '5909',
    'fkCategory' => '235',
    'sDescription' => 'Sending unauthorized information through upload of data.',
  ),
  615 => 
  array (
    'fkContext' => '5911',
    'fkCategory' => '235',
    'sDescription' => 'Defamation through communication facilities.',
  ),
  616 => 
  array (
    'fkContext' => '5912',
    'fkCategory' => '228',
    'sDescription' => 'Inappropriate use of information assets.',
  ),
  617 => 
  array (
    'fkContext' => '5913',
    'fkCategory' => '128',
    'sDescription' => 'Unauthorized access to systems and information by users whose contract with the organization has been terminated.',
  ),
  618 => 
  array (
    'fkContext' => '5915',
    'fkCategory' => '213',
    'sDescription' => 'Unauthorized external using of information processing equipment.',
  ),
  619 => 
  array (
    'fkContext' => '5916',
    'fkCategory' => '240',
    'sDescription' => 'Accidental reveal of sensitive information to immediate vicinity when making a phone call.',
  ),
  620 => 
  array (
    'fkContext' => '5917',
    'fkCategory' => '231',
    'sDescription' => 'Lack of storage space.',
  ),
  621 => 
  array (
    'fkContext' => '5918',
    'fkCategory' => '240',
    'sDescription' => 'Unauthorized access to information left on answering machines.',
  ),
  622 => 
  array (
    'fkContext' => '5919',
    'fkCategory' => '240',
    'sDescription' => 'Unauthorized receiving of sensitive information through fax due to inability to grant the identity of the receptor.',
  ),
  623 => 
  array (
    'fkContext' => '5920',
    'fkCategory' => '240',
    'sDescription' => 'Accidental sending of sensitive information through fax due to unauthorized changes in facsimile machines.',
  ),
  624 => 
  array (
    'fkContext' => '5922',
    'fkCategory' => '208',
    'sDescription' => 'Misuse of information assets due to lack of clear rules about the appropriate and permitted use.',
  ),
  625 => 
  array (
    'fkContext' => '5925',
    'fkCategory' => '208',
    'sDescription' => 'Unauthorized access to information by storing it in an inappropriate place.',
  ),
  626 => 
  array (
    'fkContext' => '5926',
    'fkCategory' => '208',
    'sDescription' => 'Compromising of equipment by installing it in industrial environment.',
  ),
  627 => 
  array (
    'fkContext' => '5927',
    'fkCategory' => '208',
    'sDescription' => 'Compromising of equipment due to electric discharge provoked by lightning.',
  ),
  628 => 
  array (
    'fkContext' => '5928',
    'fkCategory' => '208',
    'sDescription' => 'Compromising of equipment due to environment conditions such as humidity and temperature.',
  ),
  629 => 
  array (
    'fkContext' => '5929',
    'fkCategory' => '208',
    'sDescription' => 'Compromising of equipment due to accidents with food or drink kept near the equipment.',
  ),
  630 => 
  array (
    'fkContext' => '5930',
    'fkCategory' => '208',
    'sDescription' => 'Compromising of equipment due to chemical effects.',
  ),
  631 => 
  array (
    'fkContext' => '5931',
    'fkCategory' => '208',
    'sDescription' => 'Theft of equipments caused by installation in inadequate places.',
  ),
  632 => 
  array (
    'fkContext' => '5934',
    'fkCategory' => '244',
    'sDescription' => 'Delivery of information to unauthorized couriers during messages transport.',
  ),
  633 => 
  array (
    'fkContext' => '5935',
    'fkCategory' => '208',
    'sDescription' => 'Failure in equipment caused by failure in electric energy supply.',
  ),
  634 => 
  array (
    'fkContext' => '5937',
    'fkCategory' => '208',
    'sDescription' => 'Compromising of equipment caused by maintenance made by unqualified people.',
  ),
  635 => 
  array (
    'fkContext' => '5941',
    'fkCategory' => '236',
    'sDescription' => 'Compromising of internal network by the use of services such as instant messaging or file sharing.',
  ),
  636 => 
  array (
    'fkContext' => '5944',
    'fkCategory' => '208',
    'sDescription' => 'Unauthorized access to information trough re-use of equipment.',
  ),
  637 => 
  array (
    'fkContext' => '5946',
    'fkCategory' => '208',
    'sDescription' => 'Compromising of equipment caused by instability in the electric power supply.',
  ),
  638 => 
  array (
    'fkContext' => '5947',
    'fkCategory' => '208',
    'sDescription' => 'Unauthorized moving of assets inside the organization.',
  ),
  639 => 
  array (
    'fkContext' => '5948',
    'fkCategory' => '208',
    'sDescription' => 'Compromising of equipment through electromagnetic interference.',
  ),
  640 => 
  array (
    'fkContext' => '5949',
    'fkCategory' => '208',
    'sDescription' => 'Insufficient system performance.',
  ),
  641 => 
  array (
    'fkContext' => '5952',
    'fkCategory' => '208',
    'sDescription' => 'Lack of periodic maintenance.',
  ),
  642 => 
  array (
    'fkContext' => '5954',
    'fkCategory' => '208',
    'sDescription' => 'Unauthorized access to information during equipment maintenance.',
  ),
  643 => 
  array (
    'fkContext' => '5955',
    'fkCategory' => '208',
    'sDescription' => 'Unauthorized external use of information processing equipment.',
  ),
  644 => 
  array (
    'fkContext' => '5956',
    'fkCategory' => '208',
    'sDescription' => 'Unauthorized access to information in equipment used outside the organization\'s premises.',
  ),
  645 => 
  array (
    'fkContext' => '5957',
    'fkCategory' => '182',
    'sDescription' => 'Insurance loss due to not meeting established requirements about maintenance of equipment and/or structure.',
  ),
  646 => 
  array (
    'fkContext' => '5958',
    'fkCategory' => '208',
    'sDescription' => 'Mislay off-site equipment working outside the organization\'s premises.',
  ),
  647 => 
  array (
    'fkContext' => '5959',
    'fkCategory' => '208',
    'sDescription' => 'Exposure of off-site equipment to strong electromagnetic fields.',
  ),
  648 => 
  array (
    'fkContext' => '5960',
    'fkCategory' => '208',
    'sDescription' => 'Unauthorized access to information through disposed equipment.',
  ),
  649 => 
  array (
    'fkContext' => '5962',
    'fkCategory' => '208',
    'sDescription' => 'Take assets to off-site without prior authorization.',
  ),
  650 => 
  array (
    'fkContext' => '5966',
    'fkCategory' => '235',
    'sDescription' => 'Unavailability of access due to failure at configuration.',
  ),
  651 => 
  array (
    'fkContext' => '5968',
    'fkCategory' => '229',
    'sDescription' => 'Unavailability of access due to failure at configuration.',
  ),
  652 => 
  array (
    'fkContext' => '5969',
    'fkCategory' => '208',
    'sDescription' => 'Computer virus contamination.',
  ),
  653 => 
  array (
    'fkContext' => '5971',
    'fkCategory' => '235',
    'sDescription' => 'Unauthorized access due to interception of transmitting data.',
  ),
  654 => 
  array (
    'fkContext' => '5972',
    'fkCategory' => '236',
    'sDescription' => 'Unauthorized remote access to internal networks.',
  ),
  655 => 
  array (
    'fkContext' => '5973',
    'fkCategory' => '239',
    'sDescription' => 'Insufficient system capacity.',
  ),
  656 => 
  array (
    'fkContext' => '5975',
    'fkCategory' => '228',
    'sDescription' => 'Inability to provide appropriate service level.',
  ),
  657 => 
  array (
    'fkContext' => '5976',
    'fkCategory' => '228',
    'sDescription' => 'Inability to apply the appropriate security requirements for each service.',
  ),
  658 => 
  array (
    'fkContext' => '5978',
    'fkCategory' => '103',
    'sDescription' => 'Access to personal information of the candidates to a job by unauthorized people.',
  ),
  659 => 
  array (
    'fkContext' => '5979',
    'fkCategory' => '117',
    'sDescription' => 'Access to personal information of the candidates to a job by unauthorized people.',
  ),
  660 => 
  array (
    'fkContext' => '5980',
    'fkCategory' => '235',
    'sDescription' => 'Failure in communication and security incidents treatment.',
  ),
  661 => 
  array (
    'fkContext' => '5981',
    'fkCategory' => '237',
    'sDescription' => 'Undue remote access to internal network by employees and/or third parties.',
  ),
  662 => 
  array (
    'fkContext' => '5983',
    'fkCategory' => '128',
    'sDescription' => 'Unauthorized access to systems and information by users or third parties whose contract has expired, or by lack of knowledge of the employees about the contract termination.',
  ),
  663 => 
  array (
    'fkContext' => '5984',
    'fkCategory' => '100',
    'sDescription' => 'Unauthorized access to systems and information by users or third parties whose contract has expired, or by lack of knowledge of the employees about the contract termination.',
  ),
  664 => 
  array (
    'fkContext' => '5985',
    'fkCategory' => '128',
    'sDescription' => 'Lack of knowledge of new vulnerabilities due to lack of contact with research groups and services of vulnerabilities notification.',
  ),
  665 => 
  array (
    'fkContext' => '5986',
    'fkCategory' => '279',
    'sDescription' => 'Inability to recover critical business processes due to difficulty in crisis management.',
  ),
  666 => 
  array (
    'fkContext' => '5987',
    'fkCategory' => '128',
    'sDescription' => 'Lack of knowledge about new techniques of attack.',
  ),
  667 => 
  array (
    'fkContext' => '5988',
    'fkCategory' => '128',
    'sDescription' => 'Lack of knowledge about security weaknesses in the software.',
  ),
  668 => 
  array (
    'fkContext' => '5989',
    'fkCategory' => '100',
    'sDescription' => 'Loss of intellectual property rights or information developed in the organization due to lack of clear specification of such rights in contracts.',
  ),
  669 => 
  array (
    'fkContext' => '5990',
    'fkCategory' => '128',
    'sDescription' => 'Security weaknesses not previously detected.',
  ),
  670 => 
  array (
    'fkContext' => '5991',
    'fkCategory' => '279',
    'sDescription' => 'Violation of intellectual property rights by users when copying softwares, images, music or any other information to the company computers.',
  ),
  671 => 
  array (
    'fkContext' => '5995',
    'fkCategory' => '228',
    'sDescription' => 'Not meeting the requirements for providing services in a secure way.',
  ),
  672 => 
  array (
    'fkContext' => '5997',
    'fkCategory' => '228',
    'sDescription' => 'Lack of service delivery stipulated by contract.',
  ),
  673 => 
  array (
    'fkContext' => '5998',
    'fkCategory' => '228',
    'sDescription' => 'Not attend the necessary security requirements to provide the service.',
  ),
  674 => 
  array (
    'fkContext' => '5999',
    'fkCategory' => '228',
    'sDescription' => 'Unauthorized access to the information and systems by service providers.',
  ),
  675 => 
  array (
    'fkContext' => '6000',
    'fkCategory' => '228',
    'sDescription' => 'Nonconformity between the provided services and the agreements/contracts established.',
  ),
)
?>