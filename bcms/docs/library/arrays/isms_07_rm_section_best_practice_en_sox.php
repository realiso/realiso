<?php

$laSectionBestPracticeEN_SOX = 
array (
  0 => 
  array (
    'fkContext' => '10361',
    'fkParent' => 'null',
    'sName' => 'CobiT',
  ),
  1 => 
  array (
    'fkContext' => '10362',
    'fkParent' => '10361',
    'sName' => 'Plan and Organise',
  ),
  2 => 
  array (
    'fkContext' => '10363',
    'fkParent' => '10362',
    'sName' => 'PO1Define a Strategic IT Plan',
  ),
  3 => 
  array (
    'fkContext' => '10370',
    'fkParent' => '10362',
    'sName' => 'PO2Define the Information Architecture',
  ),
  4 => 
  array (
    'fkContext' => '10375',
    'fkParent' => '10362',
    'sName' => 'PO3 Determine Technological Direction',
  ),
  5 => 
  array (
    'fkContext' => '10381',
    'fkParent' => '10362',
    'sName' => 'PO4 Define the IT Processes, Organisation and Relationships',
  ),
  6 => 
  array (
    'fkContext' => '10397',
    'fkParent' => '10362',
    'sName' => 'PO5 Manage the IT Investment',
  ),
  7 => 
  array (
    'fkContext' => '10403',
    'fkParent' => '10362',
    'sName' => 'PO6 Communicate Management Aims and Direction',
  ),
  8 => 
  array (
    'fkContext' => '10409',
    'fkParent' => '10362',
    'sName' => 'PO7 Manage IT Human Resources',
  ),
  9 => 
  array (
    'fkContext' => '10418',
    'fkParent' => '10362',
    'sName' => 'PO8 Manage Quality',
  ),
  10 => 
  array (
    'fkContext' => '10425',
    'fkParent' => '10362',
    'sName' => 'PO9 Assess and Manage IT Risks',
  ),
  11 => 
  array (
    'fkContext' => '10432',
    'fkParent' => '10362',
    'sName' => 'PO10 Manage Projects',
  ),
  12 => 
  array (
    'fkContext' => '10447',
    'fkParent' => '10361',
    'sName' => 'Acquire and Implement',
  ),
  13 => 
  array (
    'fkContext' => '10448',
    'fkParent' => '10447',
    'sName' => 'AI1 Identify Automated Solutions',
  ),
  14 => 
  array (
    'fkContext' => '10453',
    'fkParent' => '10447',
    'sName' => 'AI2 Acquire and Maintain Application Software',
  ),
  15 => 
  array (
    'fkContext' => '10464',
    'fkParent' => '10447',
    'sName' => 'AI3 Acquire and Maintain Technology Infrastructure',
  ),
  16 => 
  array (
    'fkContext' => '10469',
    'fkParent' => '10447',
    'sName' => 'AI4 Enable Operation and Use',
  ),
  17 => 
  array (
    'fkContext' => '10474',
    'fkParent' => '10447',
    'sName' => 'AI5 Procure IT Resources',
  ),
  18 => 
  array (
    'fkContext' => '10479',
    'fkParent' => '10447',
    'sName' => 'AI6 Manage Changes',
  ),
  19 => 
  array (
    'fkContext' => '10485',
    'fkParent' => '10447',
    'sName' => 'AI7 Install and Accredit Solutions and Changes',
  ),
  20 => 
  array (
    'fkContext' => '10495',
    'fkParent' => '10361',
    'sName' => 'Deliver and Support',
  ),
  21 => 
  array (
    'fkContext' => '10496',
    'fkParent' => '10495',
    'sName' => 'DS1 Define and Manage Service Levels',
  ),
  22 => 
  array (
    'fkContext' => '10503',
    'fkParent' => '10495',
    'sName' => 'DS2 Manage Third-party Services',
  ),
  23 => 
  array (
    'fkContext' => '10508',
    'fkParent' => '10495',
    'sName' => 'DS3 Manage Performance and Capacity',
  ),
  24 => 
  array (
    'fkContext' => '10514',
    'fkParent' => '10495',
    'sName' => 'DS4 Ensure Continuous Service',
  ),
  25 => 
  array (
    'fkContext' => '10525',
    'fkParent' => '10495',
    'sName' => 'DS5 Ensure Systems Security',
  ),
  26 => 
  array (
    'fkContext' => '10537',
    'fkParent' => '10495',
    'sName' => 'DS6 Identify and Allocate Costs',
  ),
  27 => 
  array (
    'fkContext' => '10542',
    'fkParent' => '10495',
    'sName' => 'DS7 Educate and Train Users',
  ),
  28 => 
  array (
    'fkContext' => '10546',
    'fkParent' => '10495',
    'sName' => 'DS8 Manage Service Desk and Incidents',
  ),
  29 => 
  array (
    'fkContext' => '10552',
    'fkParent' => '10495',
    'sName' => 'DS9 Manage the Configuration',
  ),
  30 => 
  array (
    'fkContext' => '10556',
    'fkParent' => '10495',
    'sName' => 'DS10 Manage Problems',
  ),
  31 => 
  array (
    'fkContext' => '10561',
    'fkParent' => '10495',
    'sName' => 'DS11 Manage Data',
  ),
  32 => 
  array (
    'fkContext' => '10568',
    'fkParent' => '10495',
    'sName' => 'DS12 Manage the Physical Environment',
  ),
  33 => 
  array (
    'fkContext' => '10574',
    'fkParent' => '10495',
    'sName' => 'DS13 Manage Operations',
  ),
  34 => 
  array (
    'fkContext' => '10580',
    'fkParent' => '10361',
    'sName' => 'Monitor and Evaluate',
  ),
  35 => 
  array (
    'fkContext' => '10581',
    'fkParent' => '10580',
    'sName' => 'ME1 Monitor and Evaluate IT Performance',
  ),
  36 => 
  array (
    'fkContext' => '10588',
    'fkParent' => '10580',
    'sName' => 'ME2 Monitor and Evaluate Internal Control',
  ),
  37 => 
  array (
    'fkContext' => '10596',
    'fkParent' => '10580',
    'sName' => 'ME3 Ensure Compliance With External Requirements',
  ),
  38 => 
  array (
    'fkContext' => '10602',
    'fkParent' => '10580',
    'sName' => 'ME4 Provide IT Governance',
  ),
  39 => 
  array (
    'fkContext' => '10610',
    'fkParent' => 'null',
    'sName' => 'COSO - General Entity-Wide Controls',
  ),
  40 => 
  array (
    'fkContext' => '10611',
    'fkParent' => '10610',
    'sName' => 'B - Commitment to Competence',
  ),
  41 => 
  array (
    'fkContext' => '10614',
    'fkParent' => '10610',
    'sName' => 'C - Board of Directors or Audit Committee',
  ),
  42 => 
  array (
    'fkContext' => '10634',
    'fkParent' => '10610',
    'sName' => 'D - Management Philosophy and Operating Style',
  ),
  43 => 
  array (
    'fkContext' => '10645',
    'fkParent' => '10610',
    'sName' => 'E - Organizational Structure',
  ),
  44 => 
  array (
    'fkContext' => '10651',
    'fkParent' => '10610',
    'sName' => 'F - Assignment of Authority and Responsibility',
  ),
  45 => 
  array (
    'fkContext' => '10656',
    'fkParent' => '10610',
    'sName' => 'A - Integrity and Ethical Values',
  ),
  46 => 
  array (
    'fkContext' => '10669',
    'fkParent' => '10610',
    'sName' => 'G - Human Resource Policies and Practices',
  ),
  47 => 
  array (
    'fkContext' => '10676',
    'fkParent' => '10610',
    'sName' => 'H - Entity-Wide Objectives',
  ),
  48 => 
  array (
    'fkContext' => '10681',
    'fkParent' => '10610',
    'sName' => 'I - Activity-Wide Objectives',
  ),
  49 => 
  array (
    'fkContext' => '10687',
    'fkParent' => '10610',
    'sName' => 'J - Risk Assessment',
  ),
  50 => 
  array (
    'fkContext' => '10695',
    'fkParent' => '10610',
    'sName' => 'K - Managing Change',
  ),
  51 => 
  array (
    'fkContext' => '10698',
    'fkParent' => '10610',
    'sName' => 'L - Control Activities',
  ),
  52 => 
  array (
    'fkContext' => '10702',
    'fkParent' => '10610',
    'sName' => 'M - Information',
  ),
  53 => 
  array (
    'fkContext' => '10710',
    'fkParent' => '10610',
    'sName' => 'N - Communication',
  ),
  54 => 
  array (
    'fkContext' => '10725',
    'fkParent' => '10610',
    'sName' => 'O - Ongoing Monitoring',
  ),
  55 => 
  array (
    'fkContext' => '10738',
    'fkParent' => '10610',
    'sName' => 'P - Separate Evaluation',
  ),
  56 => 
  array (
    'fkContext' => '10746',
    'fkParent' => '10610',
    'sName' => 'Q - Reporting',
  ),
)
?>