<?php
/*
 * Script para gerar os arquivos de criação e deleção das views,
 * triggers, stored_procedures e functions.
 */
 
$msContent = "";
$maViews = file('postgres/views/order.txt');
foreach ($maViews as $msViews) $msContent .= file_get_contents('postgres/views/'.trim($msViews)).PHP_EOL.PHP_EOL;
file_put_contents('postgres/isms_postgres_views_create.sql', $msContent);
//file_put_contents('postgres/isms_postgres_views_drop.sql', file_get_contents('postgres/views/drop.sql'));

$msContent = "";
$maFunctions = file('postgres/functions/order.txt');
foreach ($maFunctions as $msFunction) $msContent .= file_get_contents('postgres/functions/'.trim($msFunction)).PHP_EOL.PHP_EOL;
file_put_contents('postgres/isms_postgres_functions_create.sql', $msContent);
//file_put_contents('postgres/isms_postgres_functions_drop.sql', file_get_contents('postgres/functions/drop.sql'));

$msContent = "";
$maSPs = file('postgres/stored_procedures/order.txt');      
foreach ($maSPs as $msSP) $msContent .= file_get_contents('postgres/stored_procedures/'.trim($msSP)).PHP_EOL.PHP_EOL;
file_put_contents('postgres/isms_postgres_stored_procedures_create.sql', $msContent);
//file_put_contents('postgres/isms_postgres_stored_procedures_drop.sql', file_get_contents('postgres/stored_procedures/drop.sql'));

$msContent = "";
$maTriggers = file('postgres/triggers/order.txt');      
foreach ($maTriggers as $msTriggers) $msContent .= file_get_contents('postgres/triggers/'.trim($msTriggers)).PHP_EOL.PHP_EOL;
file_put_contents('postgres/isms_postgres_triggers_create.sql', $msContent);
//file_put_contents('postgres/isms_postgres_triggers_drop.sql', file_get_contents('postgres/triggers/drop.sql'));
?>