<?php
/*inclui a classe de manipulação da biblioteca do ISMS*/
include_once('library/LibraryTools.php');
include_once('library/ISMSLibraryTools.php');
$base_ref =  "../../fwd/src/";
include $base_ref . "base/setup.php";  
$soWeblib = FWDWebLib::getInstance();

if(isset($_GET['dbType']) && $_GET['dbType'] && $_GET['dbName'] && $_GET['dbLogin'] && $_GET['dbLang']){
  $ssDBType = $_GET['dbType'];
  $ssLang = $_GET['dbLang'];
  if ($ssLang == 'br_ems') $foLib = new EMSLibraryTools();
  else $foLib = new ISMSLibraryTools();
  switch($ssDBType){
    case 'postgres':
      require('config_postgres.php');
      header('content-type: sql/plain');
      header('Content-disposition: attachment; filename="script.sql"');
            
      readfile("postgres/isms_postgres_tables_create.sql");
      echo "\n";
      
      readfile("postgres/isms_postgres_constraints_create.sql");
      echo "\n";
      
      readfile("postgres/isms_postgres_types_create.sql");
      echo "\n";
      
      readfile("postgres/isms_postgres_language_create.sql");
      echo "\n";
      
      $msContent = "";
      
      $maViews = file('postgres/views/order.txt');
      foreach ($maViews as $msViews) $msContent .= file_get_contents("postgres/views/".trim($msViews)).PHP_EOL.PHP_EOL;
      
      $maFunctions = file('postgres/functions/order.txt');      
      foreach ($maFunctions as $msFunction) $msContent .= file_get_contents("postgres/functions/".trim($msFunction)).PHP_EOL.PHP_EOL;
      
      $maSPs = file('postgres/stored_procedures/order.txt');      
      foreach ($maSPs as $msSP) $msContent .= file_get_contents("postgres/stored_procedures/".trim($msSP)).PHP_EOL.PHP_EOL;
      
      $maTriggers = file('postgres/triggers/order.txt');      
      foreach ($maTriggers as $msTriggers) $msContent .= file_get_contents("postgres/triggers/".trim($msTriggers)).PHP_EOL.PHP_EOL;
      
      echo $msContent."\n";      
      
      echo str_replace(array_keys($laConfig),$laConfig,file_get_contents("postgres/isms_postgres_install_populate_config_$ssLang.sql"));
      echo "\n";
      
      /*cria o sql da biblioteca a partir dos arrays*/
      echo $foLib->createSQLFromLibraryArray($ssLang,$ssDBType);
      echo "\n";
      
      readfile("postgres/isms_postgres_populate_14_alter_table_isms_context.sql");
      echo "\n";      
      readfile("postgres/isms_postgres_profile_acl.sql");
      echo "\n";
   		
      break;
    default:
      exit('Database not supported.');
  }
}else{
?>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <title></title>
  <script type="text/javascript">
  </script>
  <style type="text/css">
  </style>
</head>
<body>
  Por favor, preencha todos os campos!
  <br/><br/>
  <form method="get">
    Tipo do Banco: <select name="dbType">
      <option value="postgres">PostgreSQL</option>
    </select>
    <br/><br/>
    Nome do Banco: <input type="text" name="dbName">
    <br/><br/>
    Login: <input type="text" name="dbLogin">
    <br/><br/>
    Idioma: <select name="dbLang">
      <option value="br">Português ISMS</option>
      <option value="en">Inglês ISMS</option>
      <option value="br_ems">Português EMS</option>
    </select>
    <br/><br/>
    <input type="submit" value="OK">
  </form>
</body>
</html>
<?
}
?>