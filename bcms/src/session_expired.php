<?php
include_once "include.php";

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $msEvent = "soPopUpManager.getRootWindow().location = '".$moWebLib->getSysRefBasedOnTabMain()."login.php';";
    $moEvent = new FWDClientEvent();
    $moEvent->setAttrEvent('onClick');
    $moEvent->setAttrValue($msEvent);
    FWDWebLib::getObject("vb_ok")->addObjFWDEvent($moEvent);
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    echo "<script language='javascript'>soPopUpManager.getPopUpById('popup_session_expired').setCloseEvent( function close_popup_session_expired() { ".$msEvent." } );</script>";
  }
}

// temos que pegar a linguagem setada no cookie.
$miCookieLanguage = "";
if (isset($_COOKIE['ISMSLANGUAGE'])) {
   	$miCookieLanguage = $_COOKIE['ISMSLANGUAGE'];
   	FWDLanguage::selectLanguage($miCookieLanguage);
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("session_expired.xml");
?>