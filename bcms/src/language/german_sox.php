<?php
$gaStrings = array(

/* './about_isms.xml' */

'st_about_axur_full'=>'"Risk Resilience Gmbh ist eine Schweizer Consulting Firma spezialisiert auf Risk Management. Wir haben zahlreiche Kunden in unterschiedlichen Branchen',
'st_about_axur_title'=>'Über Risk Resilience Gmbh',
'st_about_isms_full'=>'Minimarisk ist eine Lösung für Handhabung von Risk Management informationen entsprechend ISO 31000. Minimarisk Hilfen für Firmen jeglicher Grösse.',
'st_about_isms_title'=>'Über Minimarisk',
'st_axur_information_security'=>'Risk Resilience Gmbh',
'st_axur_link'=>'www.minimarisk.com',
'st_build'=>'Bilde',
'st_copyright'=>'Urheberrecht',
'st_information_security_management_system'=>'Enterprise Risk Management System',
'st_version'=>'Version',

/* './access_denied.php' */

'st_license_file_not_found'=>'Lizenz-Akte <b>%license_name%</b> nicht gefunden',

/* './access_denied.xml' */

'tt_access_denied'=>'Zutritt verweigert',

/* './classes/admin/report/ISMSReportAuditAlertFilter.php' */

'rs_alerts_created_after'=>'Alarm erstellt nach',
'rs_alerts_created_before'=>'Alarm erstellt bevor',

/* './classes/admin/report/ISMSReportAuditLogFilter.php' */

'rs_action_cl'=>'Aktion:',
'rs_log_after'=>'Log nach',
'rs_log_before'=>'Log bevor',

/* './classes/admin/report/ISMSReportAuditTaskFilter.php' */

'rs_activity_cl'=>'Aktivität:',
'rs_and_before'=>'und bevor',
'rs_any_date'=>'Jedes Datum',
'rs_creation_date_cl'=>'Erstellungsdatum:',
'rs_receiver_cl'=>'Empfänger:',
'rs_tasks_created_after'=>'Aufgaben erstellt nach',
'rs_tasks_created_before'=>'Aufgaben erstellt bevor',
'st_all_users'=>'Alle Nutzer',

/* './classes/continual_improvement/CIActionPlan.php' */

'mx_action_plan'=>'Aktionsplan',
'mx_ap_remove_error_message'=>'Es ist nicht möglich den Aktionsplan zu verschieben . Es ist mindestens eine Nichtübereinstimmung angeschlossen.',
'mx_ap_remove_error_title'=>'Fehler beim Verschieben des Aktionsplan',

/* './classes/continual_improvement/CICategory.php' */

'mx_root'=>'Wurzel',

/* './classes/continual_improvement/CIIncident.php' */

'mx_incident'=>'Vorgang',

/* './classes/continual_improvement/CIIncidentControl.php' */

'mx_incident_control_association'=>'Vorgang zu Kontrollverbindung',

/* './classes/continual_improvement/CIIncidentProcess.php' */

'mx_incident_process_association'=>'Vorgang zu Prozess Verbindung',

/* './classes/continual_improvement/CINonConformity.php' */

'mx_non_conformity'=>'Nichtübereinstimmung',

/* './classes/continual_improvement/CINonConformitySeed.php' */

'mx_inefficient'=>'Uneffizient',
'mx_late_efficiency_revision'=>'Effizienz Revision verspätet',
'mx_late_implementation'=>'Verspätete Anwendung',
'mx_late_test'=>'Verspäteter Test',
'mx_test_failure'=>'Test Fehler',

/* './classes/continual_improvement/CIOccurrence.php' */

'mx_occurrence'=>'Auftritt',
'mx_occurrence_remove_error'=>'Fehler beim Verschieben des Auftritt',
'mx_occurrence_remove_error_message'=>'Es ist nicht möglich diesen Auftritt zu verschieben . Es ist mindestens ein Vorgang angeschlossen.',
'st_incident_module_name'=>'Kontinuierliche Verbesserung',

/* './classes/continual_improvement/CISolution.php' */

'mx_solution'=>'Lösung',

/* './classes/continual_improvement/report/ISMSReportDisciplinaryProcessFilter.php' */

'rs_user_cl'=>'Nutzer:',

/* './classes/continual_improvement/report/ISMSReportElementsFilter.php' */

'lb_elements_cl'=>'Einheiten:',

/* './classes/continual_improvement/report/ISMSReportIncidentFilter.php' */

'lb_solution_date_cl'=>'Lösungs Datum:',
'mx_immediate_disposal'=>'Sofortige Beseitigung',

/* './classes/continual_improvement/report/ISMSReportNonConformityFilter.php' */

'lb_conclusion_date_cl'=>'Fertigstellungs-Datum:',
'mx_after_initialdate'=>'Nach dem %initialDate%',
'mx_before_finaldate'=>'Vor dem %finalDate%',
'mx_between_initialdate_finaldate'=>'Zwischen %initialDate% und %finalDate%',
'mx_external_audit'=>'Externe Bilanz',
'mx_internal_audit'=>'Interne Bilanz',
'mx_no'=>'Nein',
'mx_security_control'=>'Sicherheitskontrolle',
'mx_yes'=>'Ja',

/* './classes/continual_improvement/report/ISMSReportOccurrenceFilter.php' */

'mx_all'=>'Alle',

/* './classes/ISMSActivity.php' */

'wk_action_plan_approval'=>'Aktionsplan Bewilligung',
'wk_action_plan_finish_confirm'=>'Genehmigung erwartetes Ende des Aktionsplan',
'wk_action_plan_revision'=>'Aktionsplan Anschluss',
'wk_area_approval'=>'Genehmigung Bereich',
'wk_asset_approval'=>'Genehmigung Aktiva',
'wk_best_practice_approval'=>'Genehmigung Gute Beispiele',
'wk_best_practice_section_approval'=>'Genehmigung Sektion für Gute Beispiele',
'wk_category_approval'=>'Genehmigung Kategorie',
'wk_control_approval'=>'Genehmigung Kontrolle',
'wk_control_implementation_approval'=>'Genehmigung Kontrolleinführung',
'wk_control_test_followup'=>'Beobachte Kontroll Test',
'wk_document_approval'=>'Genehmigung Dokument',
'wk_document_revision_followup'=>'Beobachte Dokument Revision',
'wk_document_template_approval'=>'Genehmigung Dokumentenentwurf',
'wk_event_approval'=>'Genehmigung Fall',
'wk_incident_approval'=>'Ereignisgenehmigung',
'wk_incident_control_induction'=>'Kontrolleffiziez beinhaltet Messung',
'wk_incident_disposal_approval'=>'Ereignis Sofortbeseitigungs Bewilligung',
'wk_incident_solution_approval'=>'Ereignislösung Bewilligung',
'wk_non_conformity_approval'=>'Nichtübereinstimmung Bewilligung',
'wk_non_conformity_data_approval'=>'Nichtübereinstimmung Daten Bewilligung',
'wk_occurrence_approval'=>'Auftritts-Zustimmung',
'wk_policy_approval'=>'Genehmigung Regelung',
'wk_process_approval'=>'Genehmigung Prozess',
'wk_process_asset_association_approval'=>'Genehmigung Aktiva Verbindung mit Prozess.',
'wk_real_efficiency_followup'=>'Beobachte reale Effizienz',
'wk_risk_acceptance_approval'=>'Genehmigung Akzeptanz der Risiken mit obigem Wert',
'wk_risk_acceptance_criteria_approval'=>'Genehmigungsdefinition der Kriterien für Risiken Akzeptanz',
'wk_risk_approval'=>'Genehmigung Risiko',
'wk_risk_control_association_approval'=>'Genehmigung Kontrollverbindung mit Risiko.',
'wk_risk_tolerance_approval'=>'Genehmigung Risiko Toleranz',
'wk_scope_approval'=>'Genehmigung Bereich',
'wk_standard_approval'=>'Genehmigung Standard',

/* './classes/ISMSAuditLog.php' */

'ad_action_bad_login'=>'versuchte mit falschem Passwort einzuloggen',
'ad_action_blocked_login'=>'ist blockiert und versuchte einzuloggen',
'ad_login'=>'Eingeloggt im System',
'ad_login_error'=>'versuchte mit einem falschen Nutzername einzuloggen',
'ad_login_failed'=>'versuchte ohne Erlaubnis einzuloggen',
'ad_logout'=>'Ausgeloggt vom System',
'ad_removed_from_system'=>'Verschoben vom System',
'ad_restored'=>'erneuert',

/* './classes/ISMSContext.php' */

'ad_log_message'=>'%action% %label% <b>%context_name%</b> von %user_name%.',
'ad_on_behalf_of'=>'im Namen',
'db_default_document_name'=>'%label_context% %context_name% %label_doc%',
'st_denied_permission_to_approve'=>'Sie haben nicht eine Erlaubnis zum Genehmigen der',
'st_denied_permission_to_delete'=>'Sie haben nicht eine Erlaubnis zum Verschieben der',
'st_denied_permission_to_execute'=>'Sie haben nicht eine Erlaubnis zum Ausfertigen der Aufgabe.',
'st_denied_permission_to_insert'=>'Sie haben nicht eine Erlaubnis zum Einfügen einer',
'st_denied_permission_to_read'=>'Sie haben nicht eine Erlaubnis zum Zeigen der',

/* './classes/ISMSContextObject.php' */

'mx_ap_acceptance_pendant'=>'Aktionsplan Akzeptanz schwebend',
'mx_approval'=>'In Bewilligung',
'mx_approved'=>'Genehmigt',
'mx_closed'=>'Geschlossen',
'mx_co_approved'=>'Genehmigung von beiden',
'mx_deleted'=>'Verschoben',
'mx_denied'=>'Abgelehnt',
'mx_directed'=>'Weitergeleitet',
'mx_finished'=>'Fertiggestellt',
'mx_measured'=>'Bemessen',
'mx_nc_treatment_pendant'=>'Nichtübereinstimmung Aufbereitung schwebend',
'mx_obsolete'=>'Veraltet',
'mx_open'=>'Offen',
'mx_pendant'=>'Schwebend',
'mx_pendant_solution'=>'Schwebende - Lösung',
'mx_pendant_solved'=>'Schwebend - Gelöst',
'mx_published'=>'Veröffentlicht',
'mx_revision'=>'In Revision',
'mx_sent'=>'Ausgegeben',
'mx_solved'=>'Gelöst',
'mx_to_be_published'=>'Noch zu veröffentlichen',
'mx_under_development'=>'In Abwicklung',
'mx_waiting_conclusion'=>'Erwartung Fertigstellung',
'mx_waiting_deadline'=>'Erwartet',
'mx_waiting_measurement'=>'Erwartung der Bemessung',
'mx_waiting_solution'=>'In Erwartung der Lösung',

/* './classes/ISMSEmailTemplates.php' */

'em_activity'=>'Aktivität',
'em_alert_messages_sent'=>'<b>Lieber Minimarisk Nutzer',
'em_asset'=>'Aktiva',
'em_at'=>'an',
'em_click_here_to_access_the_system'=>'Zugang Minimarisk',
'em_client'=>'Kunde',
'em_control'=>'Kontrolle',
'em_control_has_deadline'=>'<b>Lieber Minimarisk Nutzer',
'em_created_on'=>'erstellt am',
'em_description'=>'Beschreibung',
'em_disciplinary_process'=>'<b>Lieber disziplinarischer Prozess Manager',
'em_execute_task'=>'"<b>Lieber Minimarisk Nutzer',
'em_feedback'=>'Antwort',
'em_feedback_type'=>'Antwort Typ',
'em_incident'=>'Vorgang',
'em_incident_risk_parametrization_message'=>'<b>Lieber Minimarisk Nutzer',
'em_isms_feedback_msg'=>'Ein Nutzer schrieb eine Antwort zu Minimarisk.',
'em_isms_new_user_msg'=>'Sie wurden hinzugefügt zu Minimarisk. Unten können Sie die die Zugangsdaten sehen:',
'em_limit'=>'Limit',
'em_login'=>'Login',
'em_new_parameters'=>'Neue Parameter',
'em_new_password_generated'=>'<b>Lieber Minimarisk Nutzer',
'em_new_password_request_ignore'=>'Sollten Sie kein neues Passwort angefordert haben',
'em_observation'=>'Beobachtung',
'em_original_parameters'=>'Default Parameter',
'em_password'=>'Passwort',
'em_risk'=>'Risiko',
'em_saas_abandoned_system_msg'=>'<b>Die Minimarisk wurde von unterem Kunden verlassen.</b>',
'em_sender'=>'Sender',
'em_sent_on'=>'Gesendet an',
'em_tasks_and_alerts_transferred'=>'<b>Lieber Minimarisk Nutzer',
'em_tasks_and_alerts_transferred_message'=>'der obige Nutzer wurde vom System verschoben',
'em_user'=>'Nutzer',
'em_users'=>'Nutzer',
'link'=>'Link',

/* './classes/ISMSLib.php' */

'em_license_expired'=>'Die Lizenz des Kunden <b>%client_name%</b> erlischt am <b>%expiracy%</b>',
'em_subject_license_expired'=>'Lizenz erloschen',
'mx_asset_manager'=>'Aktiva Eigner',
'mx_chairman'=>'Management',
'mx_chinese'=>'Chinese (Beta)',
'mx_control_manager'=>'Kontroll Eigner',
'mx_created_by'=>'erstellt von:',
'mx_day'=>'Tag',
'mx_days'=>'Tage',
'mx_english'=>'English',
'mx_evidence_manager'=>'Beweis Manager',
'mx_french'=>'Français',
'mx_german'=>'Deutsch',
'mx_incident_manager'=>'Ereignis Manager',
'mx_library_manager'=>'Bibliothek Eigner',
'mx_modified_by'=>'Modifiziert von:',
'mx_month'=>'Monat',
'mx_months'=>'Monate',
'mx_non_conformity_manager'=>'Nichtübereinstimmung Management',
'mx_on'=>'an',
'mx_portuguese'=>'Portugiese',
'mx_process_manager'=>'Disciplinarisches Prozess Management',
'mx_shortdate_format'=>'%m/%d/%Y',
'mx_spanish'=>'Español (Beta!)',
'mx_week'=>'Woche',
'mx_weeks'=>'Wochen',
'st_denied_permission_to_access_incident_module'=>'Sie haben keine Erlaubnis zum Betreten des kontinuierlichen Fortschritts-Arbeitsbereichs <br><br> Ihre Lizenz hat nicht die erforderliche Erlaubnis!',
'st_denied_permission_to_access_policy_module'=>'Sie haben keine Zutrittserlaubnis zum Regelungs Management Modul. <br><br>Sie nutzen eine Erlaubnis',

/* './classes/ISMSLicense.php' */

'mx_commercial'=>'Kommerziell',
'mx_trial'=>'Versuch',

/* './classes/ISMSMailer.php' */

'em_to_execute_task_check_activity_list'=>'<b>Zum Ausführen der Aufgabe',

/* './classes/ISMSPasswordPolicy.php' */

'mx_minimum_chars'=>'Minimum Länge',
'mx_minimum_numeric_chars'=>'Minimum Anzahl mit Zahlenzeichen',
'mx_minimum_special_chars'=>'Minimum Anzahl von Sonderzeichen',
'mx_password_policy'=>'<b>Passwort Regelungen:</b><br/>',
'mx_upper_and_lower_case_chars'=>'Klein- und Grossbuchstaben',

/* './classes/ISMSPolicy.php' */

'mx_policy'=>'Regelungen',

/* './classes/ISMSProfile.php' */

'mx_profile'=>'Profile',
'st_profile_remove_error'=>'Es ist nicht möglich profile zu verschieben<b>%profile_name%</b>: Es sind Nutzer angeschlossen mit ihren Profilen.',
'tt_profile_remove_error'=>'Fehler beim Verschieben der Profile',

/* './classes/ISMSScope.php' */

'mx_scope'=>'Bereich',

/* './classes/ISMSSession.php' */

'ad_login_message'=>'%label_user% <b>%user_name%</b> %action%.',

/* './classes/ISMSUser.php' */

'mx_user'=>'Nutzer',
'st_admim_module_name'=>'Admin',
'st_user_remove_error'=>'nicht möglich den Nutzer <b>%user_name%</b> zu verschieben: Der Nutzer ist verantwortlich für ein oder mehrere Einheiten des System oder ist ein Spezial-Nutzer',
'tt_user_remove_error'=>'Fehler beim Verschieben des Nutzer',

/* './classes/nonauto/admin/ISMSDefaultConfig.php' */

'db_area_priority_high'=>'3 - Hoch',
'db_area_priority_low'=>'1 - Niedrig',
'db_area_priority_middle'=>'2 - Mittel',
'db_area_type_factory_unit'=>'Hersteller',
'db_area_type_filial'=>'Filiale',
'db_area_type_matrix'=>'Zentrale',
'db_area_type_regional_office'=>'Regional Büro',
'db_control_cost_hardware'=>'Hardware',
'db_control_cost_material'=>'Material',
'db_control_cost_others'=>'Ausbildung',
'db_control_cost_pessoal'=>'Leute',
'db_control_cost_software'=>'Software',
'db_control_type_based_on_risk_analize'=>'Basierend auf Risiko Analyse',
'db_control_type_laws_and_regulamentation'=>'Folgeausgabe',
'db_control_type_others'=>'Anderes',
'db_document_type_1'=>'Vertraulich',
'db_document_type_2'=>'Begrenzt',
'db_document_type_3'=>'Einheitlich',
'db_document_type_4'=>'? öffentlich',
'db_impact_1'=>'Sehr niedrig',
'db_impact_2'=>'Niedrig',
'db_impact_3'=>'Mittel',
'db_impact_4'=>'Hoch',
'db_impact_5'=>'Sehr hoch',
'db_impact3_1'=>'Niedrig',
'db_impact3_2'=>'Mittel',
'db_impact3_3'=>'Hoch',
'db_importance_1'=>'Unbedeutend',
'db_importance_2'=>'Niedrig',
'db_importance_3'=>'Mittel',
'db_importance_4'=>'Hoch',
'db_importance_5'=>'Kritisch',
'db_importance3_1'=>'Niedrig',
'db_importance3_2'=>'Mittel',
'db_importance3_3'=>'Hoch',
'db_incident_cost_aplicable_penalty'=>'Fein',
'db_incident_cost_direct_lost'=>'Direkter Verlust',
'db_incident_cost_enviroment'=>'Umgebungserneuerung',
'db_incident_cost_indirect_lost'=>'Indirekter Verlust',
'db_process_priority_high'=>'3 - Hoch',
'db_process_priority_low'=>'1 - Niedrig',
'db_process_priority_middle'=>'2 - Mittel',
'db_process_type_4'=>'Arbeitskräfte',
'db_process_type_5'=>'Informationstechnologie',
'db_process_type_6'=>'Finanzen & Buchung',
'db_process_type_7'=>'Umwelt',
'db_process_type_8'=>'Leistungsverbesserung',
'db_process_type_administrative'=>'Markt & Kunden',
'db_process_type_operational'=>'Neues Produkt Abwicklung',
'db_process_type_support'=>'Produktion & Lieferung',
'db_rcimpact_0'=>'Beeinflusst nicht',
'db_rcimpact_1'=>'Gleichmässige Abnahme',
'db_rcimpact_2'=>'Verstärkte Abnahme',
'db_rcimpact_3'=>'Starke Abnahme',
'db_rcimpact_4'=>'Sehr starke Abnahme',
'db_rcimpact3_0'=>'Das beeinflusst nicht',
'db_rcimpact3_1'=>'Gleichmässige Abnahme',
'db_rcimpact3_2'=>'Verstärkte Abnahme',
'db_rcprob_0'=>'Beeinflusst nicht',
'db_rcprob_1'=>'Gleichmässige Abnahme',
'db_rcprob_2'=>'verstärkte Abnahme',
'db_rcprob_3'=>'Starke Abnahme',
'db_rcprob_4'=>'Sehr starke Abnahme',
'db_rcprob3_0'=>'Beeinflusst nicht',
'db_rcprob3_1'=>'Gleichmässige Abnahme',
'db_rcprob3_2'=>'Verstärkte Abnahme',
'db_register_type_1'=>'Vertraulich',
'db_register_type_2'=>'Begrenzung',
'db_register_type_3'=>'Einheitlich',
'db_register_type_4'=>'? öffentlich',
'db_risk_confidenciality'=>'Vertraulichkeit',
'db_risk_disponibility'=>'Verfügbarkeit',
'db_risk_intregrity'=>'Integrität',
'db_risk_type_administrative'=>'Administrativ',
'db_risk_type_fisic'=>'Physikalisch',
'db_risk_type_tecnologic'=>'Technologisch',
'db_risk_type_threat'=>'Gefährdung',
'db_risk_type_vulnerability'=>'Schwachstelle',
'db_risk_type_vulnerability_x_threat'=>'Fall (Schwachstelle x Gefährdung)',
'db_riskprob_1'=>'Selten (1% - 20%)',
'db_riskprob_2'=>'Unwahrscheinlich (21% - 40%)',
'db_riskprob_3'=>'Moderat (41% - 60%)',
'db_riskprob_4'=>'Wahrscheinlich (61% - 80%)',
'db_riskprob_5'=>'Fast sicher (81% - 99%)',
'db_riskprob3_1'=>'Unwahrscheinlich',
'db_riskprob3_2'=>'Moderat',
'db_riskprob3_3'=>'wahrscheinlich',

/* './classes/nonauto/FeedbackEvent.php' */

'em_isms_feedback'=>'Minimarisk Antwort',

/* './classes/nonauto/policy_management/DocumentsDrawGrid.php' */

'to_future_publication'=>'Das Dokument ist nur verfügbar für Leser am Datum %date%',

/* './classes/policy_management/PMDocInstance.php' */

'mx_document_instance'=>'Dokument Instanz',

/* './classes/policy_management/PMDocument.php' */

'mx_document'=>'Dokument',
'st_denied_permission_to_manage_document'=>'Sie haben nicht eine Erlaubnis zum Managen dieses Dokuments',
'st_document_remove_error'=>'"nicht möglich die Dokumente   <b>%name%</b> zu verschieben. Die Dokumente enthalten Nebendokumente. Zum Verschieben von Dokumenten die Nebendokumente enthalten',
'st_policy_management'=>'Regelungen Management',
'tt_document_remove_error'=>'Fehler beim Verschieben des Dokument',

/* './classes/policy_management/PMRegister.php' */

'mx_day_days'=>'Tage',
'mx_month_months'=>'Monate',
'mx_register'=>'Bericht',
'mx_week_weeks'=>'Wochen',

/* './classes/policy_management/PMTemplate.php' */

'mx_document_template'=>'Dokumentenentwurf',

/* './classes/policy_management/report/ISMSReportAccessedDocumentsFilter.php' */

'rs_users'=>'Nutzer',

/* './classes/policy_management/report/ISMSReportDocsWithHighFrequencyRevisionFilter.php' */

'report_filter_all_userid'=>'alle',
'report_filter_high_frequency_revision_definition'=>'HOHE Frequenz von Revision ist:',
'report_filter_high_frequency_revision_definition_explanation'=>'Mehr als eine Revision in einer Woche.',

/* './classes/policy_management/report/ISMSReportDocumentAccessesFilter.php' */

'rs_accesses'=>'Zutritte',
'rs_only_last'=>'Nur den letzten',
'rs_show_all'=>'Zeige alle',

/* './classes/policy_management/report/ISMSReportDocumentDatesFilter.php' */

'rs_after_initialdate'=>'Nach dem %initialDate%',
'rs_before_finaldate'=>'Vor dem %finalDate%',
'rs_between_initialfinaldate'=>'Zwischen %initialDate% und %finalDate%',
'rs_creation_date'=>'Erstellungsdatum',
'rs_last_revision_date'=>'Letztes Revisionsdatum',

/* './classes/policy_management/report/ISMSReportDocumentsByComponentFilter.php' */

'report_filter_all_doctype'=>'Alle',
'report_filter_doctype_cl'=>'Dokument Typ:',

/* './classes/policy_management/report/ISMSReportDocumentsByStateFilter.php' */

'report_filter_showregisterdocs_cl'=>'Zeige Bericht Dokumente:',
'report_filter_status_cl'=>'Dokumenten Status',
'rs_all_status'=>'Alle',

/* './classes/policy_management/report/ISMSReportDocumentSummary.php' */

'rs_approvers_list_section'=>'Genehmigungen Liste',
'rs_author'=>'Autor',
'rs_components_list_section'=>'Komponenten Liste',
'rs_document_type_management'=>'Management',
'rs_documents_type_others'=>'Anderes',
'rs_file'=>'Kartei',
'rs_general_info_section'=>'General Information',
'rs_keywords'=>'Stichwörter',
'rs_link'=>'Link',
'rs_no_approvers_message'=>'Dieses Dokument hat keinerlei Genehmigungen.',
'rs_no_components_message'=>'Dies Dokument hat keine Komponenten.',
'rs_no_readers_message'=>'Dieses Dokument hat keine Leser.',
'rs_no_references_message'=>'Dies Dokument hat keine Hinweise.',
'rs_no_registers_message'=>'Dies Dokument hat keine Berichte.',
'rs_no_subdocs_message'=>'Dies Dokument hat keine Nebenbelege.',
'rs_published_date'=>'Veröffentlichen in',
'rs_readers_list_section'=>'Leser Liste',
'rs_references_list_section'=>'Referenzen Liste',
'rs_registers_list_section'=>'Berichte Liste',
'rs_report_not_supported'=>'BERICHT IN DIESEM FORMAT NICHT VERFÜGBAR',
'rs_subdocs_list_section'=>'Nebenbelege Liste',

/* './classes/policy_management/report/ISMSReportDocumentsWithoutRegisterFilter.php' */

'report_filter_all_document_responsible'=>'alle',
'report_filter_document_responsible_cl'=>'Verantwortlich für Dokument:',

/* './classes/policy_management/report/ISMSReportNotAccessedDocumentsFilter.php' */

'rs_expand_readers'=>'Erweitere Leser',

/* './classes/policy_management/report/ISMSReportTopRevisedDocumentsFilter.php' */

'rs_expand_documents'=>'Erweitere Dokumente',
'rs_show_only_first_cl'=>'Zeige nur den Ersten:',

/* './classes/policy_management/report/ISMSReportUserCommentsFilter.php' */

'rs_all_documents'=>'alle',
'rs_documents_cl'=>'Dokumente:',

/* './classes/policy_management/report/ISMSReportUsersByProcessFilter.php' */

'rs_no'=>'Nein',
'rs_organize_by_area'=>'Organisieren von Bereich',
'rs_yes'=>'Ja',

/* './classes/policy_management/report/ISMSReportUsersWithPendantReadFilter.php' */

'report_filter_all_document_reader_cl'=>'alle',
'report_filter_document_reader_cl'=>'Dokumenten Leser',

/* './classes/risk_management/report/ISMSReportBestPracticesFilter.php' */

'rs_only_applied'=>'Nur beantragt',

/* './classes/risk_management/report/ISMSReportConformityFilter.php' */

'rs_all'=>'Alle',
'rs_standard_filter_cl'=>'Standard Filter:',

/* './classes/risk_management/report/ISMSReportControlCostByCostCategoryFilter.php' */

'rs_costcategory_cl'=>'Kategorie:',

/* './classes/risk_management/report/ISMSReportControlPlanningFilter.php' */

'rs_control_types_cl'=>'Kontroll Typen:',

/* './classes/risk_management/report/ISMSReportControlsFilter.php' */

'rs_delayed'=>'gelöscht',
'rs_implementationstatus_cl'=>'Zeige nur:',
'rs_not_trusted_controls'=>'Unausführbare Kontrolle',
'rs_trusted_and_efficient_controls'=>'ausführbare und effiziente Kontrolle',
'rs_under_implementation'=>'Unter Einfügung',

/* './classes/risk_management/report/ISMSReportEventsByCategoryFilter.php' */

'rs_category_cl'=>'Kategorie:',
'rs_event_types_cl'=>'Fall Typen:',

/* './classes/risk_management/report/ISMSReportGenericClassifTypePrioFilter.php' */

'lb_event_types_cl'=>'Fall Typen:',

/* './classes/risk_management/report/ISMSReportParametersFilter.php' */

'rs_considered_parameters_cl'=>'Betrachtete Parameter:',

/* './classes/risk_management/report/ISMSReportProcessesByPriority.php' */

'rs_undefined_priority'=>'Undefinierbare Priorität',

/* './classes/risk_management/report/ISMSReportRiskFinancialImpactFilter.php' */

'mx_accepted'=>'Akzeptiert',
'mx_avoided'=>'Gelöschte',
'mx_not_treated'=>'nicht behandelt',
'mx_reduced'=>'Reduziert',
'mx_transferred'=>'Transferriert',
'mx_treated'=>'Behandelt',
'rs_not_treated_or_accepted'=>'nicht behandelt oder akzeptiert',
'rs_treatment_cl'=>'Aufbereitung:',

/* './classes/risk_management/report/ISMSReportRiskImpactFilter.php' */

'lb_considered_parameters_cl'=>'Wohlüberlegte Parameter:',
'lb_risk_types_cl'=>'Risiko Typen:',

/* './classes/risk_management/report/ISMSReportRisksByAreaFilter.php' */

'lb_area_priorities_cl'=>'Bereich Prioritäten:',
'lb_area_types_cl'=>'Bereich Typen:',
'rs_area_cl'=>'Bereich:',

/* './classes/risk_management/report/ISMSReportRisksByProcessFilter.php' */

'lb_process_priorities_cl'=>'Prozess Prioritäten:',
'lb_process_types_cl'=>'Prozess Typen:',
'st_all'=>'Alle',

/* './classes/risk_management/report/ISMSReportRisksByType.php' */

'rs_undefined_type'=>'Undefinierbarer Typ',

/* './classes/risk_management/report/ISMSReportRisksFilter.php' */

'rs_estimated_risks'=>'erwartete Risiken',
'rs_not_estimated'=>'nicht erwartet',
'rs_not_estimated_risks'=>'nicht erwartete Risiken',
'rs_not_treated_risks'=>'nicht behandelte Risiken',
'rs_residual_value_cl'=>'Rest Risiko:',
'rs_show_only_cl'=>'Zeige nur:',
'rs_treated_risks'=>'behandelte Risiken',
'rs_value_cl'=>'Potentielles Risiko:',

/* './classes/risk_management/report/ISMSReportRiskValuesFilter.php' */

'rs_high'=>'Hoch',
'rs_low'=>'Niedrig',
'rs_medium'=>'Mittel',
'rs_not_parameterized'=>'nicht erwartet',
'rs_risk_color_based_on_cl'=>'Risiko Farbe basierend auf:',
'rs_risk_types_cl'=>'Risiko Typen:',
'rs_risks_values_cl'=>'Risiko Werte:',

/* './classes/risk_management/report/ISMSReportStatementOfApplicabilityFilter.php' */

'lb_control_types_cl'=>'Fall Typen:',

/* './classes/risk_management/report/ISMSReportTop10AssetsWithMoreRisksFilter.php' */

'lb_risk_types'=>'Risiko Typen',

/* './classes/risk_management/riskparameters/RiskParameters.php' */

'mx_justification_bl'=>'Grund',
'mx_justifying'=>'Justifizierung',

/* './classes/risk_management/RMAcceptRisk.php' */

'mx_risk_acceptance'=>'Risiko Akzeptanz',

/* './classes/risk_management/RMArea.php' */

'mx_area'=>'Bereich',
'st_area_remove_error_message'=>'"Es ist nicht möglich den Business Bereich   <b>%area_name%</b> zu verschieben: Der Bereich enthält Unterbereiche und/oder Prozesse. Zum Verschieben eines Businessbereichs das Unterbereich oder Prozesse enthält',
'tt_area_remove_erorr'=>'Fehler beim Verschieben von Business Bereich',

/* './classes/risk_management/RMAsset.php' */

'mx_asset'=>'Aktiva',
'st_asset_remove_error_message'=>'"Es ist nicht möglich Aktiva <b>%Aktiva_name%</b> zu verschieben: Es sind Risiken angeschlossen. Zum Verschieben von Aktiva mit angeschlossen Risiken',
'tt_asset_remove_error'=>'Fehler beim Verschieben von Aktiva',

/* './classes/risk_management/RMBestPractice.php' */

'mx_best_practice'=>'Gute Beispiele',
'st_best_practice_remove_error_message'=>'"Es war nicht möglich die Gute Beispiele <b>%best_practice_name%</b> zu verschieben : Die Gute Beispiele ist angeschlossen an einer oder mehreren Kontrollen. Zum Verschieben von Gute Beispiele die an einer oder mehr Kontrollen angeschlossen sind',
'tt_best_practice_remove_error'=>'Fehler beim Verschieben von Gute Beispiele',

/* './classes/risk_management/RMBestPracticeEvent.php' */

'mx_best_practice_event_association'=>'Verbindung von Gute Beispiele zu Fall',

/* './classes/risk_management/RMBestPracticeStandard.php' */

'mx_best_practice_standard_association'=>'Verbindung von Gute Beispiele zu Standard',

/* './classes/risk_management/RMCategory.php' */

'mx_category'=>'Kategorie',
'st_category_remove_error_message'=>'"Es ist nicht möglich die Kategorie <b>%category_name%</b>  zu verschieben: Die Kategorie enthält Unterkategorien und/oder Fälle und/oder Aktiva. Zum Verschieben von Kategorien',

/* './classes/risk_management/RMControl.php' */

'mx_control'=>'Kontrolle',
'st_rm_module_name'=>'Risiko Management',

/* './classes/risk_management/RMControlBestPractice.php' */

'mx_control_best_practice_association'=>'Verbindung zu Gute Beispiele an Kontrolle',

/* './classes/risk_management/RMControlImplementationAccept.php' */

'mx_control_implementation_acceptance'=>'Akzeptanz von Kontroll Ausführung',

/* './classes/risk_management/RMEvent.php' */

'mx_event'=>'Fall',

/* './classes/risk_management/RMProcess.php' */

'mx_process'=>'Prozess',

/* './classes/risk_management/RMProcessAsset.php' */

'mx_process_asset_association'=>'Verbindung von Aktiva zu Prozess',

/* './classes/risk_management/RMRisk.php' */

'mx_risk'=>'Risiko',

/* './classes/risk_management/RMRiskControl.php' */

'mx_risk_control_association'=>'Verbindung von Kontrolle zu Risiko',

/* './classes/risk_management/RMRiskLimits.php' */

'mx_risk_tolerance'=>'Risiko Toleranz',

/* './classes/risk_management/RMSectionBestPractice.php' */

'mx_best_practice_section'=>'Gute Beispiele Sektion',
'st_home'=>'Wurzel',
'st_section_control_best_practice_remove_error_message'=>'Es ist nicht möglich die Sektion   <b>%section_name%</b> zu verschieben: Die Sektion enthält Gute Beispiele und/oder Untersektionen welche Gute Beispiele',
'st_section_remove_error_message'=>'"Es ist nicht möglich die Sektion  <b>%section_name%</b> zu verschieben: Die Sektion enthält  Gute Beispiele und/oder Untersektoren. Zum Verschieben von Sektionen die Untersektoren oder Gute Beispiele enthalten',
'tt_section_remove_error'=>'Fehler beim Verschieben der Sektion',

/* './classes/risk_management/RMStandard.php' */

'mx_standard'=>'Standard',
'st_libraries_module_name'=>'Bibliotheken',
'st_remove_standard_error'=>'Es ist nicht möglich den Standard <b>%standard_name%</b>  zu löschen',
'tt_remove_standard_error'=>'Fehler beim Verschieben von Standard',

/* './classes/WKFAlert.php' */

'em_alert_check_list'=>'<b>Zwecks Zeigen des Alarm',
'wk_ap_conclusion'=>'Der Aktionsplan %name% wurde fertiggestellt',
'wk_ap_efficiency_revision_late'=>'Die Revision der Effizienz des Aktionsplan \'%name%\' ist abgelaufen.',
'wk_ap_efficiency_revision_near'=>'Das Revisionsdatum der Effizienz des Aktionsplan \'%name%\' ist geschlossen.',
'wk_approved'=>'hatte Genehmigung',
'wk_asset_np_risk_association'=>'Ein nicht erwartetes Risiko war  an die Aktiva \'%Aktiva%\' vom vorgeschlagenen Fall \'%event%\' angeschlossen.',
'wk_control_implementation'=>'Kontrolle \'%Name%\' muss eingefügt werden.',
'wk_control_test_late'=>'Der Anschluss an den Kontrolltest \'%Name%\'  ist verspätet.',
'wk_deadline_close'=>'Der Stichtag für die Bewilligung des Dokument \'%name%\'  ist in Kürze.',
'wk_deadline_expired'=>'Der Stichtag für die Bewilligung des Dokument \'%name%\' ist abgelaufen.',
'wk_delegated'=>'wurde Ihnen geschickt',
'wk_denied'=>'war abgelehnt',
'wk_disciplinary_process'=>'Der Nutzer %users% hat  zum disziplinarischem Prozess des Ereignisses %name% hinzugefügt.',
'wk_document_comment'=>'Das Dokument \'%name%\' hat eine Stellungnahme erhalten.',
'wk_document_is_now_published'=>'Dokument \'%name%\' wurde veröffentlicht.',
'wk_document_new_version'=>'Das Dokument \'%name%\' hat eine neue Version.',
'wk_document_publication'=>'Das Dokument \'%name%\' wurde zur Publizierung gesendet',
'wk_document_revision'=>'Das Dokument \'%name%\' wurde zur Revision gesendet',
'wk_document_revision_late'=>'Die Revision des Dokuments \'%name%\' ist verspätet.',
'wk_implementation_expired'=>'Die Einfügung der Kontrolle \'%Name%\' ist abgelaufen.',
'wk_inc_asset'=>'Das Ereignis \'%incident%\' wurde relativiert zur Aktiva \'%Aktiva%\'.',
'wk_inc_control'=>'Das Ereignis \'%incident%\' wurde relativiert zur Kontrolle \'%control%\'.',
'wk_inc_disposal_app'=>'Die sofortige Beseitigung für das Ereignis %name% wurde genehmigt.',
'wk_inc_disposal_denied'=>'Die sofortige Beseitigung für das Ereignis %name% wurde abgelehnt.',
'wk_inc_evidence_required'=>'Beweis-Ansammlungs Prozess erforderlich für Ereignis  \'%name%\'.',
'wk_inc_sent_to_responsible'=>'Der Vorgang \'%name%\' wurde an Sie verschickt.',
'wk_inc_solution_app'=>'Die Lösung für das Ereignis %name% wurde genehmigt.',
'wk_inc_solution_denied'=>'Die Lösung für das Ereignis %name% wurde abgelehnt.',
'wk_incident'=>'Der Vorgang \'%name%\' wurde erstellt.',
'wk_incident_risk_parametrization'=>'Das Risiko %risk_name% wurde angeschlossen an Aktiva %Aktiva_name%',
'wk_non_conformity_waiting_conclusion'=>'Nichtübereinstimmung %name% und Ende.',
'wk_non_conformity_waiting_deadline'=>'Nichtübereinstimmung %name% und Stichtag.',
'wk_pre_approved'=>'hatte Vorgenehmigung.',
'wk_probability_update_down'=>'"Die Frequenz der Ereignisse angeschlossen zum Risiko  \'%name%\'',
'wk_probability_update_up'=>'"Die Frequenz der Vorgänge angeschlossen an das Risiko \'%name%\'',
'wk_real_efficiency_late'=>'Der Anschluss an reale Effizienz der Kontrolle \'%Name%\' ist verspätet.',
'wk_risk_created'=>'Das nicht erwartete Risiko \'%risk%\' wurde angeschlossen an die Aktiva \'%Aktiva%\'.',
'wk_warning'=>'Warnung',

/* './classes/WKFSchedule.php' */

'mx_last'=>'letzter',
'mx_second_last'=>'zweiter bis letzter',
'mx_second_wee'=>'zweiter',
'st_schedule_and'=>'und',
'st_schedule_april'=>'April',
'st_schedule_august'=>'August',
'st_schedule_daily'=>'jeden Tag',
'st_schedule_day_by_month'=>'am %day%',
'st_schedule_day_by_month_inverse'=>'sm %day% Tag',
'st_schedule_day_by_week'=>'am %week% %week_day%',
'st_schedule_december'=>'Dezember',
'st_schedule_each_n_days'=>'jeden %periodicity% Tag',
'st_schedule_each_n_months'=>'jede %periodicity% Monate',
'st_schedule_each_n_weeks'=>'an %week_days%',
'st_schedule_endless_interval'=>'Vom %start%',
'st_schedule_february'=>'Februar',
'st_schedule_friday_pl'=>'Freitags',
'st_schedule_interval'=>'Zwischen %start% und %end%',
'st_schedule_january'=>'Januar',
'st_schedule_july'=>'Juli',
'st_schedule_june'=>'Juni',
'st_schedule_march'=>'März',
'st_schedule_may'=>'Mai',
'st_schedule_monday_pl'=>'Montags',
'st_schedule_monthly'=>'%day_expression% jeden Monats',
'st_schedule_november'=>'November',
'st_schedule_october'=>'Oktober',
'st_schedule_saturday_pl'=>'Samstags',
'st_schedule_september'=>'September',
'st_schedule_specified_months'=>'%day_expression% von %months%',
'st_schedule_sunday_pl'=>'Sonntags',
'st_schedule_thursday_pl'=>'Donnerstags',
'st_schedule_tuesday_pl'=>'Dienstags',
'st_schedule_wednesday_pl'=>'Mittwochs',
'st_schedule_weekly'=>'jeden %week_days%',

/* './classes/WKFTask.php' */

'wk_task'=>'Aufgabe',
'wk_task_execution_permission'=>'Sie haben nicht eine Erlaubnis zum Ausführen der Aufgabe.',

/* './crontab.php' */

'em_saas_abandoned_system'=>'Verlassenes System',

/* './damaged_system.php' */

'st_system_corrupted'=>'Ihr System könnte korrupt sein. Bitte',

/* './damaged_system.xml' */

'tt_damaged_system'=>'System Fehler',

/* './email_sender.php' */

'em_daily_digest'=>'Täglicher Bericht',

/* './graphs/ISMSGraphAPActionType.php' */

'mx_corrective_action'=>'Berichtige Aktion',
'mx_preventive_action'=>'Vorbeugende Massnahme',

/* './graphs/ISMSGraphAssetCost.php' */

'st_assets_cost_above_1m'=>'Aktiva mit Werten über $1.000.000',
'st_assets_cost_below_100k'=>'Aktiva mit Summe unter $100.000',
'st_assets_cost_between_1m_and_500k'=>'Aktiva mit Werten zwischen $1.000.000 und $500.000',
'st_assets_cost_between_500k_and_100k'=>'Aktiva mit Summe zwischen $500.000 und $100.000',

/* './graphs/ISMSGraphAssetEstimatedCost.php' */

'st_assets_with_estimated_cost'=>'Aktiva mit erwarteten Kosten',
'st_assets_without_estimated_cost'=>'Aktiva ohne erwartete Kosten',

/* './graphs/ISMSGraphControlEfficiency.php' */

'mx_expected_efficiency'=>'Erwartete Effizienz',
'mx_real_efficiency'=>'Real Effizienz',

/* './graphs/ISMSGraphControlTest.php' */

'mx_test_not_ok'=>'Test nicht OK',
'mx_test_ok'=>'Test OK',
'mx_test_value'=>'Test Wert',

/* './graphs/ISMSGraphDocumentsAccessed.php' */

'st_accessed_documents'=>'Zutritts Dokumente',
'st_accessed_documents_cumulative'=>'Zutritts Dokumenten- Kumuliert',

/* './graphs/ISMSGraphDocumentsCreated.php' */

'st_created_documents'=>'erstellte Dokumente',
'st_created_documents_cumulative'=>'erstellte Dokumente - kumuliert',

/* './graphs/ISMSGraphDocumentsType.php' */

'mx_management'=>'Management',
'mx_others'=>'Anderes',
'mx_without_type'=>'Kein Typ',

/* './graphs/ISMSGraphIncidentCategory.php' */

'st_others'=>'Anderes',

/* './graphs/ISMSGraphIncidentLossType.php' */

'mx_direct_losses'=>'Direkte Verluste',
'mx_indirect_losses'=>'Indirekte Verluste',

/* './graphs/ISMSGraphPotentialRisk.php' */

'st_high_risks'=>'Hohe Risiken',
'st_low_risks'=>'Niedrige Risiken',
'st_medium_risks'=>'Mittlere Risiken',
'st_non_parameterized_risks'=>'nicht-erwartete Risiken',

/* './graphs/ISMSGraphResidualRisk.php' */

'st_high_risk'=>'Hohes Risiko',
'st_low_risk'=>'Niedriges Risiko',
'st_medium_risk'=>'Mitleres Risiko',
'st_non_parameterized_risk'=>'nicht-erwartetes Risiko',

/* './graphs/ISMSGraphTreatedRisk.php' */

'st_avoided_risk'=>'Annulliertes Risiko',
'st_risk_accepted_being_yellow_or_red'=>'Risiko akzeptiert',
'st_risk_accepted_for_being_green'=>'Angenommenes Risiko erscheint niedrig',
'st_risk_treated_with_user_of_control'=>'Risiko behandelt unter Gebrauch der Kontrolle',
'st_transferred_risk'=>'Übertragenes Risiko',

/* './handlers/QueryCountDocsTimeToApprove.php' */

'gs_day'=>'Tag',
'gs_days'=>'Tage',
'gs_hour'=>'Stunde',
'gs_hours'=>'Stunden',
'gs_minute'=>'Minute',
'gs_minutes'=>'Minuten',
'gs_not_applicable'=>'NA',
'gs_second'=>'Sekunde',
'gs_seconds'=>'Sekunden',

/* './invalid_activation_code.php' */

'st_invalid_activation_code'=>'Der benutzte Aktivierungscode ist unbrauchbar. Bitte',

/* './invalid_activation_code.xml' */

'tt_invalid_activation_code'=>'Ungültiger Aktivierungscode',

/* './license_corrupted.xml' */

'st_license_corrupted_message'=>'Ihr Computer verwendet falsche oder beschädigte Zugangsdaten.',
'tt_license_corrupted'=>'Unvollständige oder falsche Zugangsdaten',

/* './license_expired.php' */

'st_license_expired'=>'Ihre Lizenz ist abgelaufen <b>%expiracy_str%</b>.',

/* './license_expired.xml' */

'tt_license_expired'=>'Lizenz ist abgelaufen',

/* './license_isms.php' */

'st_incident_management'=>'Kontinuierliche Verbesserung',
'st_number_of_modules'=>'Anzahl von Module',
'st_risk_management'=>'Risiko Management',
'st_system_modules_bl'=>'<b>System Module</b>',

/* './license_limit_reached.php' */

'st_assets'=>'Aktiva',
'st_modules'=>'Module',
'st_standards'=>'Standards',
'st_users'=>'Nutzer',

/* './license_limit_reached.xml' */

'st_license_limit_reached'=>'Das Maximum von %attribute_name% wurde erreicht. Es ist nicht möglich  %attribute_name% hinzuzufügen.',
'tt_license_limit_reached'=>'Das Limit von %attribute_name% wurde erreicht',

/* './login.php' */

'ad_login_error_user_and_password_do_not_match'=>'%user_login% (%user_ip%)',
'ad_login_log_message'=>'%label_user% <b>%user_name%</b> %Action%.',
'mx_session_conflict'=>'Login fehlgeschlagen weil ein anderer Nutzer mit dem gleichen Browser sich angemeldet hat.',
'st_blocked_instance_message'=>'Blockierter Fall',
'tt_blocked_instance'=>'Blockierter Fall',
'tt_session_conflict'=>'Login fehlgeschlagen',

/* './login.xml' */

'cb_remember_password'=>'Passwort-Erinnerung',
'cb_remember_user'=>'Erinnere Nutzer',
'lb_password_cl'=>'Passwort:',
'st_login_panel'=>'Login Panel',
'vb_forgot_your_password'=>'Passwort vergessen?',
'vb_login'=>'Login',
'wn_denied_access'=>'Nutzer ohne Erlaubnis zur Nutzung des Systems!',
'wn_invalid_user_or_password'=>'Falscher Nutzername oder Passwort!',
'wn_max_users'=>'Maximale Anzahl der zeitgleichen Anmeldungen erreicht!',
'wn_user_blocked'=>'Gesperrter Nutzer!',

/* './migration/ISMS_import.php' */

'ad_created'=>'erstellt',
'ad_edited'=>'Geschrieben',
'ad_removed_from_bin'=>'Vom Papierkorb zurückgeholt',

/* './migration/ISMS_migration.php' */

'st_destination_db_error'=>'Fehler beim Anschluss an Migration\'s Quelldatenbank.',
'st_source_db_error'=>'Fehler nach Anschluss an Migration\'s Quelldatenbank.',

/* './migration/ISMS_migration.xml' */

'lb_database_name_cl'=>'Datenbank Name:',
'lb_database_password_cl'=>'Datenbank Passwort:',
'lb_database_user_name_cl'=>'Datenbank Nutzername:',
'lb_hostname_cl'=>'Hostname:',
'tt_database_copy_bl'=>'<b>Datenbank Kopie</b>',
'tt_destiny_database_bl'=>'<b>Vorgesehene Datenbank</b>',
'tt_results_bl'=>'<b>Ergebnis</b>',
'tt_source_database_bl'=>'<b>Datenbank Quelle</b>',

/* './nav_graphics.xml' */

'lb_graphic_cl'=>'Suchen Sie eine Graphik aus:',
'mx_10_assets_with_most_incidents'=>'Top 10 Aktiva mit Vorgängen',
'mx_10_assets_with_most_residual_risks_in_red'=>'Top 10 Aktiva mit den höchsten Risiken',
'mx_10_assets_with_most_risks'=>'Top 10 Aktiva mit Risiken',
'mx_10_document_with_most_registers'=>'Top 10 Dokumenten mit Berichte',
'mx_10_processes_with_most_associated_assets'=>'Top 10 Prozesse mit angeschlossene Aktiva',
'mx_10_processes_with_most_documents'=>'Top 10 Prozesse mit Dokumente',
'mx_10_processes_with_most_incidents'=>'Top 10 Prozesse mit Vorgängen',
'mx_10_processes_with_most_nc_summary'=>'Top 10 Prozesse mit mehr Nichtübereinstimmungen',
'mx_10_processes_with_most_residual_risks_in_red'=>'Top 10 Prozesse mit höchsten Risiken',
'mx_10_processes_with_most_risks'=>'Top 10 Prozesse mit Risiken',
'mx_10_processes_with_most_users'=>'Top 10 Prozesse mit beteiligten Personen',
'mx_amount_of_accessed_documents'=>'Anzahl von Zutrittsdokumenten',
'mx_amount_of_created_documents'=>'Anzahl der erstellten Dokumente',
'mx_amount_of_documents_by_classification'=>'Anzahl von Dokumente nach Klassifizierungstyp',
'mx_amount_of_documents_by_status'=>'Anzahl von Dokumente von Status',
'mx_amount_of_documents_by_type'=>'Anzahl von Dokumenten von Typ',
'mx_ap_action_type_proportion'=>'Aktionsplan Proportion und ihre Typen',
'mx_assets_with_without_estimated_costs'=>'Aktiva mit/ohne erwartetem Kostenumfang',
'mx_history_of_control_test'=>'Auflistung von Kontrolltests',
'mx_history_of_controls_efficiency_revision'=>'Auflistung der Kontrolle Effizienz Revision',
'mx_incident_category_proportion'=>'Ereignis und Kategorien Proportionen',
'mx_incident_financial_impact_summary'=>'Top 10 Finanzielle Vorgangsauswirkungen',
'mx_incident_loss_type_proportion'=>'Vorgangs- und Verlusttypen Proportionen',
'mx_incidents_created_summary'=>'Zusammenfassung der letzten 6 Monatsvorgänge',
'mx_potential_risks_summary'=>'Potentielle Risiken Zusammenfassung',
'mx_residual_risks_summary'=>'Restrisiken Zusammenfassung',
'mx_summary_of_assets_costs'=>'Auflistung von Aktiva und Kosten',
'mx_summary_of_risks_treatments'=>'Zusammenfassung von Risiko Aufbereitungen',
'si_optgroup_ci'=>'Kontinuierliche Verbesserung',
'si_optgroup_cost'=>'Kosten',
'si_optgroup_pm'=>'Regelungen Manager',
'si_optgroup_revision'=>'Revision',
'si_optgroup_rm'=>'Risiken Management',
'si_optgroup_test'=>'Test',

/* './nav_report.xml' */


/* './nav_search.xml' */

'cb_document_template'=>'Dokumenten Entwurf',
'cb_incident'=>'Vorgang',
'cb_incident_category_library'=>'Vorgangskategorie',
'cb_non_conformity'=>'Nichtübereinstimmung',
'cb_occurrence'=>'Vorgang',
'cb_user'=>'Nutzer',
'lb_new_search_bl_cl'=>'<b>Neue Suche:</b>',
'rb_action_plan'=>'Aktionsplan',
'rb_advanced'=>'Erweitert',
'rb_basic'=>'Grundlage',
'rb_soluction'=>'Lösungen',
'st_creation_cl'=>'Erstellung:',
'st_dates_filter_bl'=>'Datum Filter',
'st_modification_cl'=>'Modifizierung:',
'to_advanced_search_doc'=>'<b>erweiterte Dokumentensuche:</b><br/>Suchtyp der die Kartei beinhaltet in der Suche',
'to_template_document_advanced_help'=>'<b>Erweiterter Dokumentenentwurfs Suche</b><br/>Suchtyp der die Kartei in der Suche beinhaltet',
'tt_data_filter'=>'Daten Filter',
'tt_elements_filter'=>'Einheiten Filter',
'vb_dates_filter'=>'Datums Filter',
'vb_elements_filter'=>'Elementen Filter',

/* './nav_statistics.php' */

'mx_action_plan_efficient_and_not'=>'Anzahl von effizienten und nicht-effizienten Aktionsplänen',
'mx_action_plan_finished_late'=>'Anzahl von zu spät fertiggestellten Aktionsplänen',
'mx_action_plan_finished_on_time'=>'Anzahl der derzeit beendeten Aktionspläne',
'mx_action_plan_total'=>'Anzahl der Aktionspläne',
'mx_area_amount'=>'Anzahl der Bereiche',
'mx_area_assets_amount'=>'Anzahl der Bereichs-Aktiva',
'mx_area_assets_cost'=>'Kosten der Bereich Aktiva',
'mx_area_controls_amount'=>'Anzahl der Bereich Kontrolle',
'mx_area_controls_cost'=>'Kosten der Bereichskontrolle',
'mx_area_processes_amount'=>'Anzahl der Bereichsprozesse',
'mx_area_risks_amount'=>'Anzahl des Bereich-Risiko',
'mx_area_total_impact'=>'Total Bereichsbedeutung',
'mx_area_values'=>'Bereichsvolumen',
'mx_areas_risk_summary'=>'Zusammenfassung des Bereichs-Risiko',
'mx_areas_without_processes'=>'Bereiche ohne Prozesse',
'mx_asset_controls_amount'=>'Anzahl der Aktiva Kontrolle',
'mx_asset_controls_cost'=>'Kosten der Aktiva Kontrolle',
'mx_asset_processes_amount'=>'Anzahl der Aktiva Prozesse',
'mx_asset_risks_amount'=>'Anzahl des Aktiva Risiko',
'mx_asset_total_impact'=>'Total Aktivabedeutung',
'mx_asset_values'=>'Aktiva Werte',
'mx_assets_amount'=>'Anzahl der Aktiva',
'mx_assets_risk_summary'=>'Zusammenfassung des Aktiva Risiko',
'mx_assets_total_cost'=>'Total Kosten der Aktiva',
'mx_assets_without_risk_events'=>'Aktiva ohne Risiko Fall',
'mx_business_areas'=>'Business Bereiche',
'mx_business_processes'=>'Business Prozesse',
'mx_control_risks_amount'=>'Anzahl der Kontrollrisiken',
'mx_controls_adequacy'=>'Eignung der Kontrolle',
'mx_controls_amount'=>'Anzahl der Kontrollen',
'mx_controls_implementation'=>'Kontrollanwendung',
'mx_controls_revision'=>'Revision der Kontrolle',
'mx_controls_test'=>'Test der Kontrolle',
'mx_controls_without_associated_risks'=>'Kontrolle ohne angeschlossene Risiken',
'mx_document_total'=>'Anzahl von Dokumenten',
'mx_documents_by_state'=>'Anzahl von Dokumenten nach Status',
'mx_documents_by_type'=>'Anzahl von Dokumenten nach Typ',
'mx_implemented_controls_investment'=>'Investition von eingeführter Kontrolle',
'mx_incident_total'=>'Anzahl der Vorgänge',
'mx_incidents_by_state'=>'Anzahl der Vorgänge per Status',
'mx_nc_per_ap_average'=>'Average Anzahl von Nichtübereinstimmungen per Aktionsplan',
'mx_non_conformity_by_state'=>'Anzahl von Nichtübereinstimmungen beim Status',
'mx_non_conformity_total'=>'Anzahl von Nichtübereinstimmungen',
'mx_non_parameterized_risks'=>'Nicht-erwartete Risiken',
'mx_not_measured_implemented_controls'=>'Nicht messbare Hilfsmittelkontrolle',
'mx_occupation_documents'=>'Dokumenten Akte Okkupation (MB)',
'mx_occupation_registers'=>'Berichts Kartei Okkupation (MB)',
'mx_occupation_template'=>'Entwurfs Kartei Okkupation (MB)',
'mx_occupation_total'=>'Total Okkupation (MB)',
'mx_process_assets_amount'=>'Anzahl der Prozess Aktiva',
'mx_process_assets_cost'=>'Kosten der Prozess Aktiva',
'mx_process_controls_amount'=>'Anzahl der Prozess Kontrolle',
'mx_process_controls_cost'=>'Kosten der Prozess Kontrolle',
'mx_process_risks_amount'=>'Anzahl des Prozess Risiko',
'mx_process_total_impact'=>'Total Prozesse-Bedeutung',
'mx_process_values'=>'Prozess Werte',
'mx_processes_amount'=>'Anzahl der Prozesse',
'mx_processes_risk_summary'=>'Zusammenfassung des Prozess Risiko',
'mx_processes_without_assets'=>'Prozesse ohne Aktiva',
'mx_read_documents'=>'Lese Dokumente',
'mx_read_documents_proportion'=>'Lese Dokumenten Proportion',
'mx_register_total'=>'Anzahl der Berichte',
'mx_risk_treatment_potential_impact'=>'Mögliche Auswirkung von Risiko Aufbereitung',
'mx_risks_amount'=>'Anzahl des Risiko',
'mx_risks_potential_impact'=>'Mögliche Auswirkung der Risiken',
'mx_risks_treatment'=>'Aufbereitung des Risiko',
'mx_scheduled_documents'=>'Planmässige Revision der Dokumenten Proportion',
'mx_scheduled_revision_documents'=>'Revisionplan Dokumente',
'mx_standard_best_practice_amount'=>'Anzahl der Guten Beispiele vom Standard',
'mx_total_documents'=>'Total der Dokumente',
'mx_unread_documents'=>'Ungelesene Dokumente',
'mx_unread_documents_proportion'=>'Ungelesene Dokumenten Proportion',
'si_abnormalities'=>'Abnormalitäten',
'si_action_plan_statistics'=>'Aktionsplan Statistiken',
'si_area_statistics'=>'Bereichs Statistiken',
'si_asset_statistics'=>'Aktiva Statistiken',
'si_best_practice_summary'=>'Gute Beispiele Zusammenfassung',
'si_continual_improvement'=>'Kontinuierliche Verbesserungs Statistiken',
'si_control_summary'=>'Kontroll Zusammenfassung',
'si_financial_statistics'=>'Finanzielle Statistiken',
'si_management_level_and_range'=>'Bereiche und Ebenen des Management',
'si_policy_management'=>'Regelungs Management Statistiken',
'si_process_statistics'=>'Prozess Statistiken',
'si_risk_summary'=>'Risiko Zusammenfassung',
'si_risks_summary'=>'Risiken Zusammenfassung',
'si_system_status'=>'System Status',
'st_accepted_risk'=>'Akzeptiertes Risiko',
'st_applied_best_practices_amount'=>'Anzahl der beantragten Gute Beispiele',
'st_best_practices_amount'=>'Anzahl der Gute Beispiele',
'st_complete_areas'=>'Komplette Bereiche',
'st_complete_assets'=>'Komplette Aktiva',
'st_complete_processes'=>'Komplette Prozesse',
'st_controls_being_implemented'=>'eingeschlossene Kontrollen',
'st_controls_correctly_implemented'=>'Kontrolle korrekt eingeschlossen',
'st_controls_incorrectly_implemented'=>'Kontrollen nicht korrekt eingeschlossen',
'st_controls_with_delayed_implementation'=>'Kontrollen mit verspätetem Einschluss',
'st_denied_access_to_statistics'=>'Die Einstellungen',
'st_efficient_action_plan'=>'Effiziente Aktionspläne',
'st_medium_high_not_treated_risk'=>'Medium/ Hoch / nicht behandeltes Risiko',
'st_mitigated_risk'=>'Reduziertes Risiko',
'st_non_efficient_action_plan'=>'nicht-effiziente Aktionspläne',
'st_not_treated_risks'=>'nicht behandelte Risiken',
'st_partial_areas'=>'Teilbereiche',
'st_partial_assets'=>'Teilaktiva',
'st_partial_processes'=>'Teilprozesse',
'st_potentially_low_risk'=>'Normalerweise niedriges Risiko',
'st_successfully_revised'=>'erfolgreich überarbeitet',
'st_successfully_tested'=>'erfolgreich getestet',
'st_tranferred_risk'=>'Übertragenes Risiko',
'st_treated_risks'=>'Behandelte Risiken',
'st_unmanaged_areas'=>'Nicht gemanagte Bereiche',
'st_unmanaged_assets'=>'Nichtgemanagte Aktiva',
'st_unmanaged_processes'=>'Nichtgemanagte Prozesse',
'st_unsuccessfully_revised'=>'Nicht erfolgreich überarbeitet',
'st_unsuccessfully_tested'=>'Nicht erfolgreich getestet',

/* './nav_statistics.xml' */

'lb_finish_cl'=>'Fertigstellung:',
'lb_standard_cl'=>'Standard:',
'lb_statistics_cl'=>'Statistien:',
'rb_adequate'=>'Angemessen',
'rb_ap_efficient'=>'Effizient',
'rb_ap_non_efficient'=>'Nicht effizient',
'rb_in_implantation'=>'Unter Einfügung',
'rb_inadequate'=>'Unangemessen',
'rb_late'=>'Verspätet',
'rb_not_successfully'=>'nicht erfolgreich',
'rb_not_treated'=>'nicht behandelt',
'rb_real_risk'=>'Potentielles Risiko',
'rb_residual_risk'=>'Restrisiko',
'rb_successfully'=>'Erfolgreich',
'rb_treated'=>'Behandelt',
'st_chart_not_enough_data'=>'Es sind nicht genug Daten vorhanden um die Grafik zu generieren.',
'vb_refresh'=>'Auffrischung',

/* './nav_summary_advanced.php' */

'gc_amount_bl'=>'<b>#</b>',
'gc_applied_bl'=>'<b>beantragt</b>',
'gc_assets_bl'=>'<b>Aktiva</b>',
'gc_best_practices_bl'=>'<b># von Gute Beispiele</b>',
'gc_description_bl'=>'<b>Beschreibung</b>',
'gc_disciplinary_process_amount_bl'=>'<b># von Disziplinarische Prozesse</b>',
'gc_doc_rev_qty_bl'=>'<b>#</b>',
'gc_doc_sumary_percentage_bl'=>'<b>%</b>',
'gc_doc_sumary_qty_bl'=>'<b>#</b>',
'gc_document_bl'=>'<b>Dokument</b>',
'gc_efficiency_rates_bl'=>'<b>Effizienz Rate</b>',
'gc_financial_impact_bl'=>'<b>Finanzieller Vorgang</b>',
'gc_implemented_controls_bl'=>'<b>Eingebaute Kontrolle</b>',
'gc_incident_amount_bl'=>'<b># von Vorgängen</b>',
'gc_investment_bl'=>'<b>Investment</b>',
'gc_nc_amount'=>'<b># von nicht Ähnlichkeiten',
'gc_nc_amount_bl'=>'<b># von Nichtübereinstimmungen</b>',
'gc_planned_controls_bl'=>'<b>Geplante Kontrolle</b>',
'gc_potential_bl'=>'<b>Potential</b>',
'gc_potential_impact_bl'=>'<b>Potentieller Vorgang</b>',
'gc_processes_bl'=>'<b>Prozesse</b>',
'gc_residual_bl'=>'<b>Verbleibend</b>',
'gc_standards_bl'=>'<b>Standards</b>',
'gc_status_bl'=>'<b>Status</b>',
'gc_total_bl'=>'<b>Total</b>',
'gc_users_bl'=>'<b>Angestellte</b>',
'gs_accepted_risk'=>'Akzeptiertes Risiko',
'gs_amount_bl'=>'<b>#</b>',
'gs_applicability_statement'=>'Erklärung zur Verwendbarkeit',
'gs_avoided_risk'=>'Anulliertes Risiko',
'gs_ci_ap_without_doc'=>'Aktion Plan ohne Dokumente',
'gs_ci_cn_without_ap'=>'Nichtübereinstimmungen ohne Aktionsplan',
'gs_ci_incident_without_occurrence'=>'Vorgänge ohne Auftritte',
'gs_ci_incident_without_risks'=>'Vorgänge ohne Risiken',
'gs_ci_pending_tasks'=>'Kontinuierlich eingeführte schwebende Aufgaben',
'gs_components_without_document'=>'Komponente ohne Dokumente',
'gs_controls_without_associated_risk'=>'Kontrolle ohne angeschlossene Risiken',
'gs_delayed'=>'Verspätet',
'gs_high_risk'=>'Hohes Risiko',
'gs_impact_left_bl'=>'<b>Verbliebene Vorgänge</b>',
'gs_implemented_controls_mitigated_risks_bl'=>'Eingeführte Kontrolle/Reduzierte Risiken',
'gs_implemented_controls_protected_assets_bl'=>'Eingeführte Kontrolle/Geschätzte Aktiva',
'gs_implemented_controls_treated_risks_bl'=>'Eingeführte Kontrolle/Behandelte Risiken',
'gs_inefficient_controls'=>'uneffiziente Kontrolle',
'gs_low_risk'=>'Niedriges Risiko',
'gs_medium_risk'=>'Mittleres Risiko',
'gs_mitigated_risk'=>'Reduziertes Risiko',
'gs_non_parameterized_risks'=>'nicht erwartete Risiken',
'gs_not_measured_controls'=>'nicht messbare Kontrolle',
'gs_not_treated'=>'nicht behandelt',
'gs_not_treated_risks_high_and_medium'=>'nichtbehandelte Risiken - Hoch und Mittel',
'gs_not_trusted_controls'=>'Unbewährte Kontrolle',
'gs_pm_docs_with_pendant_read'=>'Dokument mit laufenden Lesungen',
'gs_pm_documents_without_register'=>'Dokumente ohne Berichte',
'gs_pm_incident_without_risks'=>'Dokumente mit hoher Revisionsfrequenz',
'gs_pm_never_read_documents'=>'Dokumente',
'gs_pm_pending_tasks'=>'Regelungs Management schwebende Aufgaben',
'gs_pm_users_with_pendant_read'=>'Nutzer mit offenen Lesungen',
'gs_potential_risks_low'=>'Potentielles Risiken - Niedrig',
'gs_risk_management_pending_tasks'=>'Risiko Management schwebende Aufgaben',
'gs_risk_treatment_plan'=>'Risiko Aufbereitungs-Plan',
'gs_scope_statement'=>'Geltungsbereich',
'gs_sgsi_policy_statement'=>'ERMS Grundsatzerklärung',
'gs_total_bl'=>'<b>Total</b>',
'gs_total_non_parameterized_risks_bl'=>'<b>nicht-erwartete Risiken</b>',
'gs_total_parameterized_risks_bl'=>'<b>Erwartete Risiken</b>',
'gs_total_risks_bl'=>'<b>Total</b>',
'gs_transferred_risk'=>'Transferiertes Risiko',
'gs_treated'=>'Behandelt',
'gs_trusted_and_efficient_controls'=>'Bewährte und effiziente Kontrolle',
'gs_under_implementation'=>'In Ausführung',
'tt_help_impact_left'=>'<b>Verbliebene Vorgänge</b><br/><br/>Summe der Risiko Auswirkungen',
'tt_help_implemented_controls_mitigated_risks'=>'<b>Eingeführte Kontrolle/Reduzierte Risiken</b><br/><br/>Verhältnis zwischen der Summe von Kosten der eingeführten Kontrolle und der Summe der möglichen Auswirkungen der reduzierten Risiken.<br/>Diese Werte zeigen die Kontroll-Kosten',
'tt_help_implemented_controls_protected_assets'=>'<b>Eingeführte kontrollgeschützte Aktiva</b><br/><br/>Verhältnis zwischen der Summe der Kosten der eingeführten Kontrolle und dem Wert der geschätzten Aktiva.',
'tt_help_implemented_controls_treated_risks'=>'<b>eingeführte Kontrolle/aufbereitete Risiken</b><br/><br/>Verhältnis zwischen der Summe von Kosten der eingeführten Kontrolle und der Summe der möglichen Auswirkungen von aufbereiteten Risiken.<br/>Dieser Wert zeigt die Kontroll-Kosten',
'tt_help_inefficient_controls'=>'<b>Nicht-Effizienz Kontrolle</b><br/><br/> Kontrolle dessen Effizienz Revision nicht verspätet ist',
'tt_help_not_measured_controls'=>'<b> nicht gemessene Kontrolle</b><br/><br/> Eingeführte Kontrolle ohne Effizienz Revision',
'tt_help_trusted'=>'<b>Unglaubwürdige Kontrolle</b><br/><br/> Kontrolle dessen Test nicht verspätet ist',
'tt_help_trusted_and_efficient'=>'<b> Zuverlässigkeit und Effizienz-Kontrolle</b><br/><br/> Kontrolle',

/* './nav_summary_advanced.xml' */

'mi_excel_report'=>'EXCEL Bericht',
'mi_html_report'=>'HTML Bericht',
'mi_html_report_download'=>'HTML Bericht DOWNLOAD',
'mi_pdf_report'=>'PDF Bericht',
'mi_word_report'=>'WORD Bericht',
'tt_abnormalities_pm_summary_bl'=>'<b>Richtlinien Management Regelabweichungs-Zusammenfassung</b>',
'tt_abnormalities_rm_summary_bl'=>'<b>Risiko Management Anomalitäten Zusammenfassung</b>',
'tt_abnormality_ci_summary_bl'=>'<b>Kontinuierliche Verbesserungs-Zusammenfassung</b>',
'tt_best_practices_summary_bl'=>'<b>Gute Beispiele - Willfährigkeits Anzeige</b>',
'tt_controls_summary_bl'=>'<b>Kontrollueberblick</b>',
'tt_documents_average_approval_time_bl'=>'<b>Durchschnittliche Dokumenten-Genehmigungszeit</b>',
'tt_financial_summary_bl'=>'<b>Finanzielle Zusammenfassung</b>',
'tt_grid_incident_summary_title_bl'=>'<b>Ereignis überblick</b>',
'tt_grid_incidents_per_asset_title_bl'=>'<b>Top 5 Werte mit Ereignissen</b>',
'tt_grid_incidents_per_process_title_bl'=>'<b>Top 5 Prozesse mit Ereignissen</b>',
'tt_grid_incidents_per_user_title_bl'=>'<b>Top 5 Nutzer mit Disziplinarverfahren</b>',
'tt_grid_nc_per_process_title_bl'=>'<b>Top 5 Prozesse mit mehr Unstimmigkeiten</b>',
'tt_grid_nc_summary_title_bl'=>'<b>Zusammenfassung der Unstimmigkeiten</b>',
'tt_incident_summary_bl'=>'<b>Kontinuierliche Zusammenfassung der Verbesserungen</b>',
'tt_last_documents_revision_time_period_summary_bl'=>'<b>Letzte Dokumenten Zeitabschnittszusammenfassung</b>',
'tt_parameterized_risks_summary_bl'=>'<b>Potentielles Risiko . /. Restrisiko</b>',
'tt_policy_management_summary_bl'=>'<b>Richtlinien Management Überblick</b>',
'tt_policy_management_x_risk_management_bl'=>'<b>Dokumente per Geschäftsbereich</b>',
'tt_risk_management_documentation_summary_bl'=>'<b>Risikomanagement-Unterlagen Zusammenfassung</b>',
'tt_risk_management_status_bl'=>'<b>Risiko Ebene</b>',
'tt_sgsi_documents_and_reports_bl'=>'<b>ISO 31000 ERMS Obligatorische Dokumenten</b>',
'tt_top_10_most_read_documents_bl'=>'<b>Gelesene Dokumente - Top 10</b>',

/* './nav_summary_advancedNEW.xml' */

'tt_general_risks_summary_bl'=>'<b>erwartet ./. nicht-erwartetes Risiko</b>',

/* './nav_summary_basic.php' */

'gc_documents_qty_bl'=>'<b># of Dokumente</b>',
'gc_documents_status_bl'=>'<b>Dokumenten Status</b>',
'gc_percentage_bl'=>'<b>%</b>',
'gc_qty_bl'=>'<b>#</b>',
'gc_readings_bl'=>'<b>Lesungen</b>',
'gc_time_period_smaller_or_equal_bl'=>'<b>Zeitplan (kürzer oder gleich)</b>',
'gs_area'=>'Bereich',
'gs_areas_without_processes'=>'Bereich ohne Prozesse',
'gs_asset'=>'Aktiva',
'gs_assets_without_risk_events'=>'Aktiva ohne Risikofälle',
'gs_average_time'=>'mittlere Zeit',
'gs_controls_without_risk_events_association'=>'Kontrolle ohne Verbindung zu Risikofällen',
'gs_documents_to_be_read'=>'Dokument noch zu lesen',
'gs_documents_total_bl'=>'<b>Dokumente Total</b>',
'gs_documents_with_files'=>'Dokumente mit Kartei',
'gs_documents_with_links'=>'Dokumenten mit Links',
'gs_more_than_six_months'=>'mehr als sechs Monate',
'gs_na'=>'NA',
'gs_one_month'=>'Ein Monat',
'gs_one_week'=>'Eine Woche',
'gs_process'=>'Prozess',
'gs_processes_without_assets'=>'Prozesse ohne Aktiva',
'gs_published_documents_information_bl'=>'<b>Veröffentlichte Dokumente</b>',
'gs_risk'=>'Risiko',
'gs_six_months'=>'Sechs Monate',
'gs_three_months'=>'Drei Monate',

/* './nav_summary_basic.xml' */

'gc_period'=>'Periode',
'tt_my_documents_average_approval_time_bl'=>'<b>Meine durchschnittliche Dokumenten-Genehmigungszeit</b>',
'tt_my_documents_last_revision_average_time_summary_bl'=>'<b>Meine Dokumente',
'tt_my_documents_policy_management_summary_bl'=>'<b>Richtlinien-Management (Meine Dokumente) Zusammenfassung</b>',
'tt_my_elements_bl'=>'<b>Meine Einheiten</b>',
'tt_my_pendencies_bl'=>'<b>Mein schwebendes Risikomanagement</b>',
'tt_my_risk_summary_bl'=>'<b>Meine Risiken-Zusammenfassung</b>',
'tt_policy_management_x_risk_management_my_elements_bl'=>'<b>Risiko Management (Meine Einheiten) x Richtlinien Management</b>',
'tt_policy_sumary_bl'=>'<b>Richtlinien-Management Zusammenfassung</b>',
'tt_risk_management_my_elements_documentation_summary_bl'=>'<b>Meine Risikomanagement Einzelzusammenfassung</b>',
'tt_risk_summary_bl'=>'<b>Risiko Management Zusammenfassung</b>',
'tt_top_10_most_read_my_documents_bl'=>'<b>Top 10 Meine am meisten gelesenen Dokumente </b>',

/* './nav_warnings.xml' */

'gc_inquirer'=>'Anfragender',
'gc_justif'=>'Gerade eben.',
'mi_denied'=>'ableugnen',
'mi_open_task'=>'Offene Aufgabe',
'tt_alerts_bl'=>'<b>Meine Alarme</b>',
'tt_tasks_bl'=>'<b>Meine Aufgaben</b>',
'vb_remove_alerts'=>'Verschiebe Alarme',
'wn_removal_warning'=>'Vor dem Entfernen müssen Sie eine letzte Warnung auswählen.',

/* './packages/admin/acl_tree.xml' */

'to_acl_A'=>'Erlaubnis zum \'Admin\' Bereich des System',
'to_acl_A.A'=>'Bilanz tab',
'to_acl_A.A.1'=>'Tab der Liste von Aufgaben generiert vom System',
'to_acl_A.A.2'=>'Tab der Alarmliste generiert vom System',
'to_acl_A.A.3'=>'Tab mit den Tätigkeiten die im System durchgeführt sind',
'to_acl_A.C'=>'System Kundenanpassung tab',
'to_acl_A.C.1'=>'Kundenanpassung von Namen und der Anzahl von Risikoschätzungsparameter.',
'to_acl_A.C.10'=>'Kundenanpassung an die Wichtung jeden Risikoparameters',
'to_acl_A.C.11'=>'Kundenanpassung von Systemklassifizierung',
'to_acl_A.C.2'=>'Kundenanpassung der Risiko Matrixdefinition',
'to_acl_A.C.3'=>'Kundenanpassung von Kostenschätzungsparameter',
'to_acl_A.C.4'=>'Kundenanpassung von finanzielle Auswirkungsparameter',
'to_acl_A.C.5'=>'Kundenanpassung von einigen Charakteristiken des Systems',
'to_acl_A.C.6'=>'Kundenanpassung von speziellen Nutzern',
'to_acl_A.C.7'=>'Kundenanpassung der Anzahl und der Typen und Vorränge genutzt im System',
'to_acl_A.C.7.1'=>'Konfiguration von Bereichsklassifizierung',
'to_acl_A.C.7.2'=>'Konfiguration von Prozessklassifizierung',
'to_acl_A.C.7.3'=>'Konfiguration von Risikoklassifizierung',
'to_acl_A.C.7.4'=>'Konfiguration von Fallklassifizierung',
'to_acl_A.C.7.5'=>'Kontrolle von Kontrollklassifizierung',
'to_acl_A.C.7.6'=>'Konfiguration von Dokumentklassifizierung',
'to_acl_A.C.7.7'=>'Konfiguration von Berichtklassifizierung',
'to_acl_A.C.8'=>'Kundenanpassung an Sicherheitskontrolle',
'to_acl_A.C.9'=>'Kundenanpassung an Risikoakzeptanzlevel',
'to_acl_A.L'=>'Papierkorb tab',
'to_acl_A.L.1'=>'Button zum Erneuern der ausgewählten Einheiten',
'to_acl_A.L.2'=>'Button zum Verschieben der ausgewählten Einheiten',
'to_acl_A.L.3'=>'Button zum Verschieben aller Einheiten des Papierkorbs',
'to_acl_A.MA'=>'Kontomanagement tab',
'to_acl_A.P'=>'Profile tab',
'to_acl_A.P.1'=>'Menu zum Schreiben eines bestimmten Profils',
'to_acl_A.P.2'=>'Menu zum Verschieben eines bestimmten Profils',
'to_acl_A.P.3'=>'Button zum Einfügen eines neuen Profils',
'to_acl_A.S'=>'System Einstellungen tab',
'to_acl_A.S.1'=>'Passwort Regelungen Konfiguration',
'to_acl_A.S.2'=>'System \'Lösche Kaskade\' Konfiguration',
'to_acl_A.U'=>'Nutzer tab',
'to_acl_A.U.1'=>'Menu zum Schreiben eines bestimmten Nutzers',
'to_acl_A.U.2'=>'Menu zum Verschieben eines bestimmten Nutzers',
'to_acl_A.U.3'=>'Button zum Einfügen eines neuen Nutzers',
'to_acl_A.U.4'=>'Button zum Erneuern des Passworts für alle Nutzer',
'to_acl_A.U.5'=>'Menu zum blocken von Systemnutzer',
'to_acl_M'=>'Erlaubnis vom System \'Zeige Nutzer\' Modus',
'to_acl_M.CI'=>'Kontinuierliche Verbesserung tab',
'to_acl_M.CI.1'=>'Vorkommnisse tab',
'to_acl_M.CI.1.1'=>'Erlaubnis zum Zeigen alle Vorkommnisse',
'to_acl_M.CI.1.2'=>'Button zum Eintragen von Vorkommnissen',
'to_acl_M.CI.1.3'=>'Menu zum Schreiben',
'to_acl_M.CI.1.4'=>'Menu zum Verschieben',
'to_acl_M.CI.2'=>'Begebenheit tab',
'to_acl_M.CI.2.1'=>'Erlaubnis zum Zeigen aller Begebenheiten',
'to_acl_M.CI.2.2'=>'Button zum Eintragen einer Begebenheit',
'to_acl_M.CI.2.3'=>'Menu zum Schreiben auch wenn der Nutzer nicht der Begebenheitsmanager ist',
'to_acl_M.CI.2.4'=>'Menu zum Verschieben auch wenn der Nutzer nicht der Begebenheitsmanager ist',
'to_acl_M.CI.3'=>'Nichtübereinstimmung tab',
'to_acl_M.CI.3.1'=>'Erlaubnis zum Zeigen aller Nichtübereinstimmungen',
'to_acl_M.CI.3.2'=>'Button zum Eintragen einer Nichtübereinstimmung',
'to_acl_M.CI.3.3'=>'Menu zum Schreiben auch wenn der Nutzer nicht der Manager einer Nichtübereinstimmung ist',
'to_acl_M.CI.3.4'=>'Menu zum Verschieben auch wenn der Nutzer nicht der Manager der Nichtübereinstimmung ist',
'to_acl_M.CI.5'=>'Disziplinarischer Prozess tab',
'to_acl_M.CI.5.1'=>'Erlaubnis zum Zeigen der Aktion in Beziehung zu einem Benutzer',
'to_acl_M.CI.5.2'=>'Erlaubnis zum Registrieren der Aktion in Beziehung zu einem Benutzer',
'to_acl_M.CI.5.3'=>'Erlaubnis zum Verschieben eines Nutzers von einem bestimmten disziplinarischen Prozess',
'to_acl_M.CI.6'=>'Berichte tab',
'to_acl_M.CI.6.1'=>'Vorkommnisse Berichte',
'to_acl_M.CI.6.1.1'=>'Auftreten nach Begebenheit Bericht',
'to_acl_M.CI.6.1.2'=>'Disziplinarischer Prozess Bericht',
'to_acl_M.CI.6.1.3'=>'Beweisansammlungs Bericht',
'to_acl_M.CI.6.1.4'=>'Anschluss an Vorfälle Bericht',
'to_acl_M.CI.6.1.5'=>'Anschluss an Ereignis Bericht',
'to_acl_M.CI.6.1.6'=>'Finanzielle Auswirkung von Ereignis Bericht',
'to_acl_M.CI.6.1.7'=>'Kontrollrevision verursacht von Ereignissen Bericht',
'to_acl_M.CI.6.1.8'=>'Einheiten beeinträchtigt von Ereignissen Bericht',
'to_acl_M.CI.6.1.9'=>'Automatische Kalkulation der Risiko-Wahrscheinlichkeit Bericht',
'to_acl_M.CI.6.2'=>'Nichtübereinstimmungen Berichte',
'to_acl_M.CI.6.2.1'=>'Nichtübereinstimmung von Prozess Bericht',
'to_acl_M.CI.6.2.2'=>'Nichtübereinstimmung von Kontrolle von Prozess Bericht',
'to_acl_M.CI.6.2.3'=>'Anschluss an Nichtübereinstimmung Bericht',
'to_acl_M.CI.6.2.4'=>'Aktionsplan von Nutzer Bericht',
'to_acl_M.CI.6.2.5'=>'Actionsplan von Prozess Bericht',
'to_acl_M.CI.6.3'=>'Kontinuierliche Fortschritts Berichte',
'to_acl_M.CI.6.3.1'=>'Kontinuierlicher Fortschritt schwebender Aufgaben Bericht',
'to_acl_M.CI.6.3.2'=>'Bericht von Vorfällen ohne folgende Risiken.',
'to_acl_M.CI.6.3.3'=>'Bericht von Nichtübereinstimmungen ohne angeschlossene Aktionspläne.',
'to_acl_M.CI.6.3.4'=>'Aktionsplan ohne Dokument Bericht',
'to_acl_M.CI.6.3.5'=>'Vorfälle ohne Vorgang Bericht',
'to_acl_M.CI.7'=>'Aktionsplan tab',
'to_acl_M.CI.7.1'=>'Erlaubnis zum Zeigen aller Aktionspläne',
'to_acl_M.CI.7.2'=>'Button zum Eintragen eines Aktionsplans',
'to_acl_M.CI.7.3'=>'Erlaubnis zum Schreiben von Aktionsplänen',
'to_acl_M.CI.7.4'=>'Erlaubnis zum Verschieben von Aktionsplänen',
'to_acl_M.D'=>'Anzeigetafel Tab',
'to_acl_M.D.1'=>'Zusammenfassungs Tab',
'to_acl_M.D.2'=>'Erweiterte Zusammenfassung tab',
'to_acl_M.D.3'=>'Anzeigen Tab',
'to_acl_M.D.6'=>'System Statistiken (Grafiken) tab',
'to_acl_M.D.6.1'=>'Statistiken über Geschäftsbereiche',
'to_acl_M.D.6.10'=>'Statistiken über Systemstatus (Auflistung von Risiko x Bereich',
'to_acl_M.D.6.11'=>'Statistiken über Systemregulierungsmanagement',
'to_acl_M.D.6.12'=>'Statistiken über kontinuierliche Systemverbesserung',
'to_acl_M.D.6.13'=>'Statistiken über Aktionspläne und über die Verbindung zwischen Aktionsplan und Nichtübereinstimmungen',
'to_acl_M.D.6.2'=>'Statistiken über Geschäftsbereiche',
'to_acl_M.D.6.3'=>'Statistiken über Aktiva',
'to_acl_M.D.6.4'=>'Zusammenfassung von Risiken',
'to_acl_M.D.6.5'=>'Zusammenfassung von Gute Beispiele',
'to_acl_M.D.6.6'=>'Zusammenfassung von Systemkontrollen',
'to_acl_M.D.6.7'=>'Zusammenfassung von Anomalitäten',
'to_acl_M.D.6.8'=>'Finanzielle Statistiken - Kosten',
'to_acl_M.D.6.9'=>'Statistiken für Bereiche',
'to_acl_M.L'=>'Risiko Management tab',
'to_acl_M.L.1'=>'Fall-Bibliothek tab',
'to_acl_M.L.1.1'=>'Menu zum browsen in Unterkategorien und Fälle mit bestimmter Kategorie',
'to_acl_M.L.1.2'=>'Menu zum Schreiben einer bestimmten Kategorie',
'to_acl_M.L.1.3'=>'Menu zum Verschieben einer bestimmten Kategorie',
'to_acl_M.L.1.4'=>'Menu für angeschlossene Gute Beispiele zu einem bestimmten Fall',
'to_acl_M.L.1.5'=>'Menu zum Schreiben eines bestimmten Falls',
'to_acl_M.L.1.6'=>'Menu zum Verschieben eines bestimmten Falls',
'to_acl_M.L.1.7'=>'Button zum Einfügen einer neuen Kategorie',
'to_acl_M.L.1.8'=>'Button zum Einfügen eines neuen Fall',
'to_acl_M.L.1.9'=>'Button zum Anschliessen eines Falles an die Kategorie des Nutzers',
'to_acl_M.L.2'=>'Gute Beispiele Bibliothek tab',
'to_acl_M.L.2.1'=>'Menu zum browsen in Untersektionen und Gute Beispiele einer bestimmten Sektion',
'to_acl_M.L.2.10'=>'Erlaube die Nutzer zum Entwerfen eines Dokumententwurfs für Gute Beispiele',
'to_acl_M.L.2.2'=>'Menu zum Schreiben einer bestimmten Sektion',
'to_acl_M.L.2.3'=>'Menu zum Verschieben einer bestimmten Sektion',
'to_acl_M.L.2.4'=>'Menu für angeschlossene Fälle zu bestimmten Gute Beispiele',
'to_acl_M.L.2.5'=>'Menu zum Schreiben bestimmter Gute Beispiele',
'to_acl_M.L.2.6'=>'Menu zum Verschieben bestimmter Gute Beispiele',
'to_acl_M.L.2.7'=>'Button zum Einfügen einer neuen Sektion',
'to_acl_M.L.2.8'=>'Button zum Einfügen neuer Gute Beispiele',
'to_acl_M.L.2.9'=>'Entwerfe Gute Beispiele Dokumenten-Entwürfe im Dokumenten Entwurf tab',
'to_acl_M.L.3'=>'Standards Bibliothek tab',
'to_acl_M.L.3.1'=>'Menu zum Schreiben eines bestimmten Standard',
'to_acl_M.L.3.2'=>'Menu zum Verschieben eines bestimmten Standard',
'to_acl_M.L.3.3'=>'Button zum Einfügen eines neuen Standard',
'to_acl_M.L.4'=>'Gute Beispiele Bibliothek tab',
'to_acl_M.L.4.1'=>'Menu zum Schreiben einer bestimmten Sektion',
'to_acl_M.L.4.2'=>'Menu zum Schreiben einer bestimmten Sektion',
'to_acl_M.L.5'=>'Dokument Entwurfs tab',
'to_acl_M.L.5.1'=>'Menu zum Schreiben eines Dokumententwurfs',
'to_acl_M.L.5.2'=>'Menu zum Verschieben eines Dokumentenentwurfs',
'to_acl_M.L.5.3'=>'Button zum Einfügen eines neuen Dokumentenentwurfs',
'to_acl_M.L.6'=>'Kategorien Bibliothek tab',
'to_acl_M.L.6.1'=>'Button zum Einfügen einer neuen Kategorie',
'to_acl_M.L.6.2'=>'Menu zum Schreiben einer Kategorie',
'to_acl_M.L.6.3'=>'Menu zum Verschieben einer Kategorie',
'to_acl_M.L.6.4'=>'Button zum Einfügen einer neuen Lösung',
'to_acl_M.L.6.5'=>'Menu zum Schreiben einer Lösung',
'to_acl_M.L.6.6'=>'Menu zum Verschieben einer Lösung',
'to_acl_M.PM'=>'Richtlinien Management tab',
'to_acl_M.PM.10'=>'Zeige Management von Dokumenten in Abwicklung tab',
'to_acl_M.PM.10.1'=>'Button zum eintragen eines Dokument',
'to_acl_M.PM.11'=>'Zeige Management von Dokumenten in Bewilligung tab',
'to_acl_M.PM.12'=>'Zeige Management von veröffentlichten Dokumenten tab',
'to_acl_M.PM.13'=>'Zeige Management von Dokumenten in Revision tab',
'to_acl_M.PM.14'=>'Zeige Management von veralteten Dokumenten tab',
'to_acl_M.PM.15'=>'Zeige Management von allen Dokumenten tab',
'to_acl_M.PM.16'=>'Zeige Management von Berichte tab',
'to_acl_M.PM.17'=>'Berichte tab',
'to_acl_M.PM.17.1'=>'Anderes',
'to_acl_M.PM.17.1.16'=>'Bericht der die Eigenschaften sicherer Dokumente anzeigt',
'to_acl_M.PM.17.1.17'=>'Klassifizierung Bericht',
'to_acl_M.PM.17.2'=>'General Bericht',
'to_acl_M.PM.17.2.1'=>'Dokumenten Bericht',
'to_acl_M.PM.17.2.2'=>'Dokumente nach Bereich Bericht',
'to_acl_M.PM.17.2.3'=>'Dokumente nach ERMS Komponente Bericht',
'to_acl_M.PM.17.2.4'=>'Kommentarwechsel Stellungnahmen und Revisionsbegründungsbericht',
'to_acl_M.PM.17.2.5'=>'Nutzer Kommentare Bericht',
'to_acl_M.PM.17.3'=>'Berichtsreport',
'to_acl_M.PM.17.3.1'=>'Bericht nach Dokument Bericht',
'to_acl_M.PM.17.4'=>'Zugangskontrollberichte',
'to_acl_M.PM.17.4.1'=>'Nutzerzugangs-Bilanzsbericht',
'to_acl_M.PM.17.4.2'=>'Zugang zu Dokumenten Bilanzbericht',
'to_acl_M.PM.17.4.3'=>'Dokumenten und deren Genehmigung Bericht',
'to_acl_M.PM.17.4.4'=>'Dokumenten und deren Leser Bericht',
'to_acl_M.PM.17.4.5'=>'Nutzer Angeschlossen an Prozesse Bericht',
'to_acl_M.PM.17.5'=>'Management Bericht',
'to_acl_M.PM.17.5.1'=>'Dokumenten mit schwebender Lesung Bericht',
'to_acl_M.PM.17.5.2'=>'Dokument mit den meisten Zutritten Bericht',
'to_acl_M.PM.17.5.3'=>'Dokumente ohne Zutritte Bericht',
'to_acl_M.PM.17.5.4'=>'Meist gesehene Dokumente Bericht',
'to_acl_M.PM.17.5.5'=>'Schwebende Bewilligung Bericht',
'to_acl_M.PM.17.5.6'=>'Dokumentenerstellung und Revision nach Datum Bericht',
'to_acl_M.PM.17.5.7'=>'Dokumentenrevisionszeitplan Bericht',
'to_acl_M.PM.17.5.8'=>'Dokumenten mit verspäteter Revision Bericht',
'to_acl_M.PM.17.6'=>'Abnormalitätenberichte',
'to_acl_M.PM.17.6.1'=>'Komponenten ohne Dokumente Bericht',
'to_acl_M.PM.17.6.2'=>'Richtlinienmanagement schwebender Aufgaben',
'to_acl_M.PM.17.6.3'=>'Dokumente mit hoher Revisionsfrequenz. Hochfrequenz bezieht sich auf Dokumente',
'to_acl_M.PM.17.6.4'=>'Dokumente ohne Berichte',
'to_acl_M.PM.17.6.5'=>'Bericht listet die Nutzer',
'to_acl_M.PM.18'=>'Visualisierung / Management von Dokumenten zur Veröffentlichung',
'to_acl_M.RM'=>'Risiko Management tab',
'to_acl_M.RM.1'=>'Bereichs tab',
'to_acl_M.RM.1.1'=>'Erlaubnis zum zeigen aller Bereiche',
'to_acl_M.RM.1.2'=>'Menu zum browsen in Unterbereichen von sicheren Bereichen',
'to_acl_M.RM.1.3'=>'Menu bis Zeige Prozesse in einem sicheren Bereich',
'to_acl_M.RM.1.4'=>'Menu zum Schreiben eines sicheren Bereichs',
'to_acl_M.RM.1.5'=>'Menu zum Verschieben eines sicheren Bereichs',
'to_acl_M.RM.1.6'=>'Button zum Einfügen eines neuen Bereichs',
'to_acl_M.RM.1.7'=>'Bedienknopf',
'to_acl_M.RM.1.8'=>'Erlaubnis zum Schreiben in einen Bereich',
'to_acl_M.RM.1.9'=>'Erlaubnis zum Verschieben eines Bereichs',
'to_acl_M.RM.2'=>'Prozess tab',
'to_acl_M.RM.2.1'=>'Erlaubnis zum Zeigen aller Prozesse',
'to_acl_M.RM.2.2'=>'Menu zum browsen in Aktiva eines sicheren Prozess',
'to_acl_M.RM.2.4'=>'Menu zum Schreiben sicherer Prozesse',
'to_acl_M.RM.2.5'=>'Menu zum Verschieben sicherer Prozesse',
'to_acl_M.RM.2.6'=>'Button zum Eintragen eines neuen Prozesses',
'to_acl_M.RM.2.7'=>'Erlaubnis zum Schreiben eines Prozesses',
'to_acl_M.RM.2.8'=>'Erlaubnis zum Verschieben eines Prozesses',
'to_acl_M.RM.3'=>'Aktiva tab',
'to_acl_M.RM.3.1'=>'Erlaubnis zum zeigen aller Aktiva',
'to_acl_M.RM.3.10'=>'Kontextmenu für Risiko-Einfügung zu den Aktiva von den Ereignissen einer Kategorie Ereignis-Kategorie Bibliothek',
'to_acl_M.RM.3.2'=>'Menu zum browsen in Risiken kalkulierbarer Aktiva',
'to_acl_M.RM.3.4'=>'Menu zum Erstellen eines Risiko für ein Aktiva',
'to_acl_M.RM.3.5'=>'Menu zum Schreiben eines sicheren Aktiva',
'to_acl_M.RM.3.6'=>'Menu zum Verschieben eines sicheren Aktiva',
'to_acl_M.RM.3.7'=>'Button zum Eintragen einer neuen Aktiva',
'to_acl_M.RM.3.8'=>'Erlaubnis zum Schreiben einer Aktiva',
'to_acl_M.RM.3.9'=>'Erlaubnis zum Verschieben einer Aktiva',
'to_acl_M.RM.4'=>'Risiken tab',
'to_acl_M.RM.4.1'=>'Erlaubnis zum Zeigen aller Risiken',
'to_acl_M.RM.4.10'=>'Erlaubnis zum Verschieben eines Risiko',
'to_acl_M.RM.4.2'=>'Menu zum browsen in Kontrollen eines kalkulierbaren Risiko',
'to_acl_M.RM.4.4'=>'Menu zum Schreiben eines sicheren Risiko',
'to_acl_M.RM.4.5'=>'Menu zum Verschieben eines sicheren Risiko',
'to_acl_M.RM.4.6'=>'Button zum Eintragen eines neuen Risiko',
'to_acl_M.RM.4.8'=>'Button zum Einschätzen eines Risiko',
'to_acl_M.RM.4.9'=>'Erlaubnis zum Schreiben eines Risiko',
'to_acl_M.RM.5'=>'Kontroll tab',
'to_acl_M.RM.5.1'=>'Erlaubnis zum Zeigen alle Kontrollen',
'to_acl_M.RM.5.2'=>'Menu zum browsen in den Risiken',
'to_acl_M.RM.5.3'=>'Menu zum anschliessen von Risiken an eine sichere Kontrolle',
'to_acl_M.RM.5.4'=>'Menu zum Schreiben einer sicheren Kontrolle',
'to_acl_M.RM.5.5'=>'Menu zum Verschieben einer sicheren Kontrolle',
'to_acl_M.RM.5.6'=>'Button zum Eintragen einer neuen Kontrolle',
'to_acl_M.RM.5.7'=>'"Erlaubnis zum schreiben einer Kontrolle',
'to_acl_M.RM.5.8'=>'Erlaubnis zum Verschieben einer Kontrolle',
'to_acl_M.RM.5.9'=>'Wechsle die Revision/Testauflistung der Systemkontrollen',
'to_acl_M.RM.6'=>'Berichts tab',
'to_acl_M.RM.6.1'=>'Risiko Berichte',
'to_acl_M.RM.6.1.1'=>'Risikoumfangsbericht',
'to_acl_M.RM.6.1.2'=>'Risiken von Prozess Bericht',
'to_acl_M.RM.6.1.3'=>'Risiken von Aktiva Report',
'to_acl_M.RM.6.1.4'=>'Risiken von Kontrollen Bericht',
'to_acl_M.RM.6.1.5'=>'Risikenaufwandsbericht',
'to_acl_M.RM.6.1.6'=>'Aktivabedeutungsbericht',
'to_acl_M.RM.6.1.7'=>'Risiken nach Bereichs Report',
'to_acl_M.RM.6.1.8'=>'Checkliste für Ereignisse Bericht',
'to_acl_M.RM.6.2'=>'Kontrollenberichte',
'to_acl_M.RM.6.2.1'=>'Kontrollen per Verantwortlich Bericht',
'to_acl_M.RM.6.2.10'=>'Anschluss von Kontrolle Test Bericht',
'to_acl_M.RM.6.2.11'=>'Konformitätsbericht',
'to_acl_M.RM.6.2.12'=>'Kontrollrevisionsagenda von Verantwortlichen Bericht',
'to_acl_M.RM.6.2.13'=>'Kontrolltest von Verantwortlichen Bericht',
'to_acl_M.RM.6.2.2'=>'Kontrollen von Risiko Bericht',
'to_acl_M.RM.6.2.3'=>'Planung von Kontrollen Bericht',
'to_acl_M.RM.6.2.4'=>'Kontrolleintragungsdatum Bericht',
'to_acl_M.RM.6.2.5'=>'Kontrolleffizienz Bericht',
'to_acl_M.RM.6.2.6'=>'Kontrolle Zusammenfassung Bericht',
'to_acl_M.RM.6.2.7'=>'Anschluss von Kontrollbericht',
'to_acl_M.RM.6.2.8'=>'Anschluss von Kontrolleffizienz Bericht',
'to_acl_M.RM.6.2.9'=>'nicht gemessene Kontrolle Bericht',
'to_acl_M.RM.6.3'=>'Risiko Management Berichte',
'to_acl_M.RM.6.3.1'=>'Risiko Management Schwebende Aufgaben Bericht',
'to_acl_M.RM.6.3.2'=>'Bereiche ohne Prozesse Bericht',
'to_acl_M.RM.6.3.3'=>'Aktivas ohne Risiken Bericht',
'to_acl_M.RM.6.3.4'=>'Prozesse ohne Aktiva Bericht',
'to_acl_M.RM.6.3.5'=>'nicht-schätzbare Risiken Bericht',
'to_acl_M.RM.6.3.6'=>'Kontrolle ohne Risiken Bericht',
'to_acl_M.RM.6.3.7'=>'Vervielfältige Risiken Bericht',
'to_acl_M.RM.6.4'=>'Finanzielle Berichte',
'to_acl_M.RM.6.4.4'=>'Kosten von Kontrolle Bericht',
'to_acl_M.RM.6.4.5'=>'Kosten von Kontrolle von Verantwortlichen',
'to_acl_M.RM.6.4.6'=>'Kosten von Kontrolle nach Bereich Bericht',
'to_acl_M.RM.6.4.7'=>'Kosten von Kontrolle von Prozess Bericht',
'to_acl_M.RM.6.4.8'=>'Kosten von Kontrolle von Aktiva Bericht',
'to_acl_M.RM.6.5'=>'ISO 31000 Berichte',
'to_acl_M.RM.6.5.1'=>'Aussage zur Anwendbarkeit Bericht',
'to_acl_M.RM.6.5.2'=>'Risiko Aufbereitungsbericht',
'to_acl_M.RM.6.5.3'=>'ERMS Bereichsdeklaration Bericht',
'to_acl_M.RM.6.5.4'=>'ERMS Regelungendeklaration Bericht',
'to_acl_M.RM.6.6'=>'Zusammenfassungsberichte',
'to_acl_M.RM.6.6.1'=>'Bericht: Top 10 - Aktiva mit Risiken',
'to_acl_M.RM.6.6.2'=>'Bericht: top 10 - Aktiva mit den höchsten Risiken',
'to_acl_M.RM.6.6.3'=>'Bericht: Top 10 - Risiken nach Bereich',
'to_acl_M.RM.6.6.4'=>'Bericht: Top 10 - Risiken nach Prozess',
'to_acl_M.RM.6.6.5'=>'Anzahl von Risiken nach Prozess Bericht',
'to_acl_M.RM.6.6.6'=>'Anzahl von Risiken nach Bereich Bericht',
'to_acl_M.RM.6.6.7'=>'Risiko Status nach Bereich Bericht',
'to_acl_M.RM.6.6.8'=>'Risiko nach Prozess Bericht',
'to_acl_M.RM.6.7'=>'Andere Berichte',
'to_acl_M.RM.6.7.1'=>'Nutzer Verantwortlichkeiten Bericht',
'to_acl_M.RM.6.7.2'=>'Schwebende Einheiten Bericht',
'to_acl_M.RM.6.7.3'=>'Aktiva Bericht',
'to_acl_M.RM.6.7.4'=>'Einheiten Klassifizierungsbericht',
'to_acl_M.RM.6.7.5'=>'Bericht der für jede Aktiva seine Abhängigkeiten und seine Abhängigen anzeigt',
'to_acl_M.RM.6.7.6'=>'Bericht der die Aktivarelevanz anzeigt',
'to_acl_M.S'=>'Suche tab',
'tr_acl_A'=>'Administrator',
'tr_acl_A.A'=>'BILANZ',
'tr_acl_A.A.1'=>'AUFGABEN',
'tr_acl_A.A.2'=>'ALARME',
'tr_acl_A.A.3'=>'LOG',
'tr_acl_A.C'=>'KUNDENANPASSUNG',
'tr_acl_A.C.1'=>'RISIKO SCHÄTZUNGS PARAMETER',
'tr_acl_A.C.10'=>'RISIKO PARAMETERWICHTUNG',
'tr_acl_A.C.11'=>'SYSTEM KLASSIFIZIERUNG',
'tr_acl_A.C.2'=>'RISIKO MATRIX DEFINITION',
'tr_acl_A.C.3'=>'KOSTENSCHÄTZUNGSPARAMETER',
'tr_acl_A.C.4'=>'FINANZIELLE AUSWIRKUNGSPARAMETER',
'tr_acl_A.C.5'=>'SYSTEM EIGENSCHAFTEN',
'tr_acl_A.C.6'=>'SPEZIELLE NUTZER',
'tr_acl_A.C.7'=>'TYP UND VORRANG ERSCHEINUNG',
'tr_acl_A.C.7.1'=>'BEREICH',
'tr_acl_A.C.7.2'=>'PROZESS',
'tr_acl_A.C.7.3'=>'RISIKO',
'tr_acl_A.C.7.4'=>'FALL',
'tr_acl_A.C.7.5'=>'KONTROLLE',
'tr_acl_A.C.7.6'=>'DOKUMENT',
'tr_acl_A.C.7.7'=>'BERICHT',
'tr_acl_A.C.8'=>'KONTROLL REVISION UND TEST',
'tr_acl_A.C.9'=>'RISIKO AKZEPTANZLEVEL',
'tr_acl_A.L'=>'Papierkorb',
'tr_acl_A.L.1'=>'ERNEUERE',
'tr_acl_A.L.2'=>'VERSCHIEBE',
'tr_acl_A.L.3'=>'VERSCHIEBE ALLE',
'tr_acl_A.MA'=>'MEIN KONTO',
'tr_acl_A.P'=>'PROFILE',
'tr_acl_A.P.1'=>'SCHREIBE',
'tr_acl_A.P.2'=>'VERSCHIEBE',
'tr_acl_A.P.3'=>'EINFÜGE PROFILE',
'tr_acl_A.S'=>'SYSTEM EINSTELLUNGEN',
'tr_acl_A.S.1'=>'PASSWORT Regelungen',
'tr_acl_A.S.2'=>'LÖSCHE KASKADE',
'tr_acl_A.U'=>'NUTZER',
'tr_acl_A.U.1'=>'SCHREIBE',
'tr_acl_A.U.2'=>'VERSCHIEBE',
'tr_acl_A.U.3'=>'EINFÜGE NUTZER',
'tr_acl_A.U.4'=>'ERNEUERE PASSWORT',
'tr_acl_A.U.5'=>'BLOCKIERE NUTZER',
'tr_acl_M'=>'Zeige Nutzer',
'tr_acl_M.CI'=>'KONTINUIERLICHE VERBESSERUNG',
'tr_acl_M.CI.1'=>'VORKOMMEN',
'tr_acl_M.CI.1.1'=>'LISTE',
'tr_acl_M.CI.1.2'=>'EINTRAGE VORKOMMNIS',
'tr_acl_M.CI.1.3'=>'SCHREIBE OHNE RESTRIKTION',
'tr_acl_M.CI.1.4'=>'VERSCHIEBE OHNE RESTRIKTION',
'tr_acl_M.CI.2'=>'BEGEBENHEIT',
'tr_acl_M.CI.2.1'=>'LISTE',
'tr_acl_M.CI.2.2'=>'EINTRAGE BEGEBENHEIT',
'tr_acl_M.CI.2.3'=>'SCHREIBE OHNE RESTRIKTION',
'tr_acl_M.CI.2.4'=>'VERSCHIEBE OHNE RESTRIKTION',
'tr_acl_M.CI.3'=>'NICHTÜBEREINSTIMMUNG',
'tr_acl_M.CI.3.1'=>'LISTE',
'tr_acl_M.CI.3.2'=>'EINTRAGE NICHTÜBEREINSTIMMUNGEN',
'tr_acl_M.CI.3.3'=>'SCHREIBE OHNE RESTRIKTION',
'tr_acl_M.CI.3.4'=>'VERSCHIEBE OHNE RESTRIKTION',
'tr_acl_M.CI.5'=>'DISZIPLINARISCHER PROZESS',
'tr_acl_M.CI.5.1'=>'ZEIGE AKTION',
'tr_acl_M.CI.5.2'=>'REGISTER AKTION',
'tr_acl_M.CI.5.3'=>'VERSCHIEBE',
'tr_acl_M.CI.6'=>'Berichte',
'tr_acl_M.CI.6.1'=>'VORKOMMNISSE BERICHTE',
'tr_acl_M.CI.6.1.1'=>'AUFTRETEN NACH BEGEBENHEIT',
'tr_acl_M.CI.6.1.2'=>'DISZIPLINARISCHER PROZESS',
'tr_acl_M.CI.6.1.3'=>'BEWEIS-ANSAMMLUNG',
'tr_acl_M.CI.6.1.4'=>'ANSCHLUSS AN VORFÄLLE',
'tr_acl_M.CI.6.1.5'=>'ANSCHLUSS AN EREIGNIS',
'tr_acl_M.CI.6.1.6'=>'FINANZIELLE AUSWIRKUNGEN VON EREIGNIS',
'tr_acl_M.CI.6.1.7'=>'KONTROLLREVISION VERURSACHT VON EREIGNISSEN',
'tr_acl_M.CI.6.1.8'=>'EINHEITEN BEEINTRÄCHTIGT VON EREIGNISSEN',
'tr_acl_M.CI.6.1.9'=>'AUTOMATISCHE KALKULATION DER RISIKO-WAHRSCHEINLICHKEIT',
'tr_acl_M.CI.6.2'=>'NICHTÜBEREINSTIMMUNGEN Bericht',
'tr_acl_M.CI.6.2.1'=>'NICHTÜBEREINSTIMMUNG VON PROZESS',
'tr_acl_M.CI.6.2.2'=>'NICHTÜBEREINSTIMMUNG VON KONTROLLE VON PROZESS',
'tr_acl_M.CI.6.2.3'=>'ANSCHLUSS AN NICHTÜBEREINSTIMMUNG',
'tr_acl_M.CI.6.2.4'=>'AKTIONSPLAN VON NUTZER',
'tr_acl_M.CI.6.2.5'=>'AKTIONSPLAN VON PROZESS',
'tr_acl_M.CI.6.3'=>'KONTINUIERLICHER FORTSCHRITTS Berichte',
'tr_acl_M.CI.6.3.1'=>'KONTINUIERLICHER FORTSCHRITT SCHWEBENDER AUFGABEN',
'tr_acl_M.CI.6.3.2'=>'VORFÄLLE OHNE RISIKEN',
'tr_acl_M.CI.6.3.3'=>'NICHTÜBEREINSTIMMUNG OHNE AKTIONSPLAN',
'tr_acl_M.CI.6.3.4'=>'AKTIONSPLAN OHNE DOKUMENT',
'tr_acl_M.CI.6.3.5'=>'VORFÄLLE OHNE VORGANG',
'tr_acl_M.CI.7'=>'Aktionsplan',
'tr_acl_M.CI.7.1'=>'LISTE',
'tr_acl_M.CI.7.2'=>'EINTRAGEN',
'tr_acl_M.CI.7.3'=>'SCHREIBE OHNE RESTRIKTION',
'tr_acl_M.CI.7.4'=>'VERSCHIEBE OHNE RESTRIKTION',
'tr_acl_M.D'=>'ANZEIGETAFEL',
'tr_acl_M.D.1'=>'ZUSAMMENFASSUNG',
'tr_acl_M.D.2'=>'ERWEITERTE ZUSAMMENFASSUNG',
'tr_acl_M.D.3'=>'ANZEIGEN',
'tr_acl_M.D.6'=>'STATISTIKEN',
'tr_acl_M.D.6.1'=>'BEREICHS GRAPHIKEN',
'tr_acl_M.D.6.10'=>'SYSTEM STATUS',
'tr_acl_M.D.6.11'=>'Regelungen für Managementstatistiken',
'tr_acl_M.D.6.12'=>'KONTINUIERLICHE VERBESSERUNGS-STATISTIKEN',
'tr_acl_M.D.6.13'=>'AKTIONSPLAN-STATISTIKEN',
'tr_acl_M.D.6.2'=>'PROZESS GRAPHIKEN',
'tr_acl_M.D.6.3'=>'AKTIVA GRAPHIKEN',
'tr_acl_M.D.6.4'=>'RISIKEN ZUSAMMENFASSUNG',
'tr_acl_M.D.6.5'=>'GUTE BEISPIELE ZUSAMMENFASSUNG',
'tr_acl_M.D.6.6'=>'KONTROLL ZUSAMMENFASSUNG',
'tr_acl_M.D.6.7'=>'ABNORMALITÄTEN',
'tr_acl_M.D.6.8'=>'FINANZIELLE STATISTIKEN',
'tr_acl_M.D.6.9'=>'DECKUNG IN DER FÜHRUNGSEBENE',
'tr_acl_M.L'=>'Bibliotheken',
'tr_acl_M.L.1'=>'FALL-BIBLIOTHEK',
'tr_acl_M.L.1.1'=>'ÖFFNE KATEGORIE',
'tr_acl_M.L.1.2'=>'SCHREIBE KATEGORIE',
'tr_acl_M.L.1.3'=>'VERSCHIEBE KATEGORIE',
'tr_acl_M.L.1.4'=>'ANGESCHLOSSENE GUTE BEISPIELE',
'tr_acl_M.L.1.5'=>'SCHREIBE FALL',
'tr_acl_M.L.1.6'=>'VERSCHIEBE FALL',
'tr_acl_M.L.1.7'=>'EINFÜGEN KATEGORIE',
'tr_acl_M.L.1.8'=>'EINFÜGEN FALL',
'tr_acl_M.L.1.9'=>'ANGESCHLOSSENE FÄLLE',
'tr_acl_M.L.2'=>'GUTE BEISPIELE BIBLIOTHEK',
'tr_acl_M.L.2.1'=>'?ÖFFNE SEKTION',
'tr_acl_M.L.2.10'=>'ENTWERFE GUTE BEISPIELE ENTWURF',
'tr_acl_M.L.2.2'=>'SCHREIBE SEKTION',
'tr_acl_M.L.2.3'=>'VERSCHIEBE SEKTION',
'tr_acl_M.L.2.4'=>'ANGESCHLOSSENE FÄLLE',
'tr_acl_M.L.2.5'=>'SCHREIBE GUTE BEISPIELE',
'tr_acl_M.L.2.6'=>'VERSCHIEBE GUTE BEISPIELE',
'tr_acl_M.L.2.7'=>'EINFÜGEN SEKTION',
'tr_acl_M.L.2.8'=>'EINFÜGE GUTE BEISPIELE',
'tr_acl_M.L.2.9'=>'ZEIGE GUTE BEISPIELE DOKUMENT ENTWÜRFE',
'tr_acl_M.L.3'=>'STANDARDS BIBLIOTHEK',
'tr_acl_M.L.3.1'=>'SCHREIBE STANDARDS',
'tr_acl_M.L.3.2'=>'VERSCHIEBE STANDARDS',
'tr_acl_M.L.3.3'=>'EINFÜGE STANDARD',
'tr_acl_M.L.4'=>'IMPORT / EXPORT',
'tr_acl_M.L.4.1'=>'EXPORT',
'tr_acl_M.L.4.2'=>'IMPORT',
'tr_acl_M.L.5'=>'DOKUMENT ENTWURFS BIBLIOTHEK',
'tr_acl_M.L.5.1'=>'SCHREIBE DOKUMENT ENTWURF',
'tr_acl_M.L.5.2'=>'VERSCHIEBE DOKUMENTENENTWURF',
'tr_acl_M.L.5.3'=>'EINFÜGE DOKUMENTENENTWURF',
'tr_acl_M.L.6'=>'KATEGORIEN BIBLIOTHEK',
'tr_acl_M.L.6.1'=>'EINFÜGE KATEGORIE',
'tr_acl_M.L.6.2'=>'SCHREIBE KATEGORIE',
'tr_acl_M.L.6.3'=>'VERSCHIEBE KATEGORIE',
'tr_acl_M.L.6.4'=>'EINFÜGE LÖSUNG',
'tr_acl_M.L.6.5'=>'SCHREIBE LÖSUNG',
'tr_acl_M.L.6.6'=>'VERSCHIEBE LÖSUNG',
'tr_acl_M.PM'=>'RICHTLINIEN MANAGEMENT',
'tr_acl_M.PM.10'=>'DOKUMENTE IN ABWICKLUNG',
'tr_acl_M.PM.10.1'=>'EINTRAGEN DOKUMENT',
'tr_acl_M.PM.11'=>'DOKUMENTE IN BEWILLIGUNG',
'tr_acl_M.PM.12'=>'VERÖFFENTLICHTE DOKUMENTE',
'tr_acl_M.PM.13'=>'DOKUMENTE IN REVISION',
'tr_acl_M.PM.14'=>'VERALTETE DOKUMENTE',
'tr_acl_M.PM.15'=>'ALLE DOKUMENTE',
'tr_acl_M.PM.16'=>'BERICHTE',
'tr_acl_M.PM.17'=>'Berichte',
'tr_acl_M.PM.17.1'=>'ANDERES',
'tr_acl_M.PM.17.1.16'=>'DOKUMENTENEIGENSCHAFTEN',
'tr_acl_M.PM.17.1.17'=>'KLASSIFIZIERUNG Bericht',
'tr_acl_M.PM.17.2'=>'GENERAL BERICHTE',
'tr_acl_M.PM.17.2.1'=>'DOKUMENTE',
'tr_acl_M.PM.17.2.2'=>'DOKUMENTE NACH BEREICH',
'tr_acl_M.PM.17.2.3'=>'DOKUMENTE nach ERMS Komponente',
'tr_acl_M.PM.17.2.4'=>'DOKUMENTEN KOMMENTARE UND BEGRÜNDUNGEN',
'tr_acl_M.PM.17.2.5'=>'NUTZER KOMMENTARE',
'tr_acl_M.PM.17.3'=>'BERICHTSREPORT',
'tr_acl_M.PM.17.3.1'=>'BERICHT NACH DOKUMENT',
'tr_acl_M.PM.17.4'=>'ZUGANGSKONTROLLBERICHTE',
'tr_acl_M.PM.17.4.1'=>'NUTZER ZUGANG BILANZ',
'tr_acl_M.PM.17.4.2'=>'ZUGANG ZU DOKUMENTEN BILANZ',
'tr_acl_M.PM.17.4.3'=>'DOKUMENTE UND DEREN GENEHMIGUNGEN',
'tr_acl_M.PM.17.4.4'=>'DOKUMENTE UND DEREN LESER',
'tr_acl_M.PM.17.4.5'=>'NUTZER ANGESCHLOSSEN AN PROZESSE',
'tr_acl_M.PM.17.5'=>'MANAGEMENT BERICHTE',
'tr_acl_M.PM.17.5.1'=>'DOKUMENTE MIT SCHWEBENDER LESUNG',
'tr_acl_M.PM.17.5.2'=>'DOKUMENTE MIT DEN MEISTEN ZUTRITTEN',
'tr_acl_M.PM.17.5.3'=>'DOKUMENTE OHNE ZUTRITTE',
'tr_acl_M.PM.17.5.4'=>'MEIST GESEHENE DOKUMENTE',
'tr_acl_M.PM.17.5.5'=>'SCHWEBENDE BEWILLIGUNGEN',
'tr_acl_M.PM.17.5.6'=>'DOKUMENTE ERSTELLUNG UND REVISION NACH DATUM',
'tr_acl_M.PM.17.5.7'=>'DOKUMENTEN REVISIONSZEITPLAN',
'tr_acl_M.PM.17.5.8'=>'DOKUMENTE MIT VERSPÄTETER REVISION',
'tr_acl_M.PM.17.6'=>'ABNORMALITÄTENBERICHTE',
'tr_acl_M.PM.17.6.1'=>'KOMPONENTEN OHNE DOKUMENTE',
'tr_acl_M.PM.17.6.2'=>'SCHWEBENDE AUFGABEN',
'tr_acl_M.PM.17.6.3'=>'DOKUMENTE MIT HOHER REVISIONS-FREQUENZ',
'tr_acl_M.PM.17.6.4'=>'DOKUMENTE OHNE BERICHTE',
'tr_acl_M.PM.17.6.5'=>'NUTZER MIT OFFENEN LESUNGEN',
'tr_acl_M.PM.18'=>'DOKUMENTE ZUM VERÖFFENTLICHEN',
'tr_acl_M.RM'=>'RISIKO MANAGEMENT',
'tr_acl_M.RM.1'=>'Bereich',
'tr_acl_M.RM.1.1'=>'LISTE ALLE',
'tr_acl_M.RM.1.2'=>'Zeige Unterbereiche',
'tr_acl_M.RM.1.3'=>'ZEIGE PROZESSE',
'tr_acl_M.RM.1.4'=>'SCHREIBE',
'tr_acl_M.RM.1.5'=>'VERSCHIEBE',
'tr_acl_M.RM.1.6'=>'BEREICH EINTRAGEN',
'tr_acl_M.RM.1.7'=>'EINFÜGEN EINES BEREICHS DES ERSTEN LEVELS',
'tr_acl_M.RM.1.8'=>'SCHREIBEN WENN VERANTWORTLICH',
'tr_acl_M.RM.1.9'=>'VERSCHIEBE WENN VERANTWORTLICH',
'tr_acl_M.RM.2'=>'PROZESS',
'tr_acl_M.RM.2.1'=>'LISTE ALLE',
'tr_acl_M.RM.2.2'=>'ZEIGE AKTIVA',
'tr_acl_M.RM.2.4'=>'SCHREIBE',
'tr_acl_M.RM.2.5'=>'VERSCHIEBE',
'tr_acl_M.RM.2.6'=>'EINTRAGEN PROZESS',
'tr_acl_M.RM.2.7'=>'SCHREIBEN WENN VERANTWORTLICH',
'tr_acl_M.RM.2.8'=>'VERSCHIEBE WENN VERANTWORTLICH',
'tr_acl_M.RM.3'=>'AKTIVA',
'tr_acl_M.RM.3.1'=>'LISTE ALLE',
'tr_acl_M.RM.3.10'=>'LADE RISIKEN VOM LEXIKON',
'tr_acl_M.RM.3.2'=>'ZEIGE RISIKEN',
'tr_acl_M.RM.3.4'=>'NEUE RISIKEN',
'tr_acl_M.RM.3.5'=>'SCHREIBE',
'tr_acl_M.RM.3.6'=>'VERSCHIEBE',
'tr_acl_M.RM.3.7'=>'EINTRAGEN AKTIVA',
'tr_acl_M.RM.3.8'=>'SCHREIBE WENN VERANTWORTLICH',
'tr_acl_M.RM.3.9'=>'VERSCHIEBE WENN VERANTWORTLICH',
'tr_acl_M.RM.4'=>'RISIKO',
'tr_acl_M.RM.4.1'=>'LISTE ALLE',
'tr_acl_M.RM.4.10'=>'VERSCHIEBE WENN VERANTWORTLICH',
'tr_acl_M.RM.4.2'=>'ZEIGE KONTROLLEN',
'tr_acl_M.RM.4.4'=>'SCHREIBE',
'tr_acl_M.RM.4.5'=>'VERSCHIEBE',
'tr_acl_M.RM.4.6'=>'EINTRAGE RISIKO',
'tr_acl_M.RM.4.8'=>'EINSCHÄTZUNG',
'tr_acl_M.RM.4.9'=>'SCHREIBE WENN VERANTWORTLICH',
'tr_acl_M.RM.5'=>'KONTROLLE',
'tr_acl_M.RM.5.1'=>'LISTE ALLE',
'tr_acl_M.RM.5.2'=>'ZEIGE RISIKEN',
'tr_acl_M.RM.5.3'=>'ANGESCHLOSSENE RISIKEN',
'tr_acl_M.RM.5.4'=>'SCHREIBE',
'tr_acl_M.RM.5.5'=>'VERSCHIEBE',
'tr_acl_M.RM.5.6'=>'EINTRAGE KONTROLLE',
'tr_acl_M.RM.5.7'=>'SCHREIBE WENN VERANTWORTLICH',
'tr_acl_M.RM.5.8'=>'VERSCHIEBE WENN VERANTWORTLICH',
'tr_acl_M.RM.5.9'=>'REVISION/TEST AUFLISTUNG',
'tr_acl_M.RM.6'=>'BERICHTE',
'tr_acl_M.RM.6.1'=>'VON RISIKEN',
'tr_acl_M.RM.6.1.1'=>'Risiko Umfang',
'tr_acl_M.RM.6.1.2'=>'RISIKEN VON PROZESSEN',
'tr_acl_M.RM.6.1.3'=>'RISIKEN VON AKTIVA',
'tr_acl_M.RM.6.1.4'=>'RISIKEN VON KONTROLLEN',
'tr_acl_M.RM.6.1.5'=>'RISIKO VORGANG',
'tr_acl_M.RM.6.1.6'=>'AKTIVA BEDEUTUNG',
'tr_acl_M.RM.6.1.7'=>'RISIKEN NACH BEREICH',
'tr_acl_M.RM.6.1.8'=>'CHECKLISTE FÜR EREIGNISSE',
'tr_acl_M.RM.6.2'=>'VON KONTROLLEN',
'tr_acl_M.RM.6.2.1'=>'KONTROLLEN NACH VERANTWORTLICHKEIT',
'tr_acl_M.RM.6.2.10'=>'ANSCHLUSS VON KONTROLL TEST',
'tr_acl_M.RM.6.2.11'=>'KONFORMITÄT',
'tr_acl_M.RM.6.2.12'=>'KONTROLL REVISION AGENDA VON VERANTWORTLICHEN',
'tr_acl_M.RM.6.2.13'=>'KONTROLLTEST AGENDA VON VERANTWORTLICHEN',
'tr_acl_M.RM.6.2.2'=>'KONTROLLEN VON RISIKEN',
'tr_acl_M.RM.6.2.3'=>'PLANUNG VON KONTROLLEN',
'tr_acl_M.RM.6.2.4'=>'KONTROLLEN EINTRAGUNGS DATUM',
'tr_acl_M.RM.6.2.5'=>'KONTROLLEN EFFIZIENZ',
'tr_acl_M.RM.6.2.6'=>'Kontrolle ZUSAMMENFASSUNG',
'tr_acl_M.RM.6.2.7'=>'ANSCHLUSS VON KONTROLLE',
'tr_acl_M.RM.6.2.8'=>'ANSCHLUSS VON KONTROLLEFFIZIENZ',
'tr_acl_M.RM.6.2.9'=>'NICHT GEMESSENE KONTROLLE',
'tr_acl_M.RM.6.3'=>'OF RISIKO MANAGEMENT',
'tr_acl_M.RM.6.3.1'=>'SCHWEBENDE AUFGABEN',
'tr_acl_M.RM.6.3.2'=>'BEREICHE OHNE PROZESSE',
'tr_acl_M.RM.6.3.3'=>'AKTIVA OHNE RISIKEN',
'tr_acl_M.RM.6.3.4'=>'PROZESSE OHNE AKTIVA',
'tr_acl_M.RM.6.3.5'=>'NICHT SCHÄTZBARE RISIKEN',
'tr_acl_M.RM.6.3.6'=>'KONTROLLE OHNE RISIKEN',
'tr_acl_M.RM.6.3.7'=>'VERVIELFÄLTIGE RISIKEN',
'tr_acl_M.RM.6.4'=>'FINANZIELL',
'tr_acl_M.RM.6.4.4'=>'KOSTEN VON KONTROLLE',
'tr_acl_M.RM.6.4.5'=>'KOSTEN VON KONTROLLE VON VERANTWORTLICHEN',
'tr_acl_M.RM.6.4.6'=>'KOSTEN VON KONTROLLE NACH BEREICH',
'tr_acl_M.RM.6.4.7'=>'KOSTEN VON KONTROLLE NACH PROZESS',
'tr_acl_M.RM.6.4.8'=>'KOSTEN VON KONTROLLE NACH AKTIVA',
'tr_acl_M.RM.6.5'=>'ISO 31000',
'tr_acl_M.RM.6.5.1'=>'AUSSAGE ZUR ANWENDBARKEIT',
'tr_acl_M.RM.6.5.2'=>'RISIKO AUFBEREITUNGSPLAN',
'tr_acl_M.RM.6.5.3'=>'ERMS BEREICHSDEKLARATION',
'tr_acl_M.RM.6.5.4'=>'ERMS REGELUNGEN DEKLARATION',
'tr_acl_M.RM.6.6'=>'ZUSAMMENFASSUNG',
'tr_acl_M.RM.6.6.1'=>'TOP 10 - AKTIVA MIT RISIKEN',
'tr_acl_M.RM.6.6.2'=>'TOP 10 - AKTIVA MIT DEN HÖCHSTEN RISIKEN',
'tr_acl_M.RM.6.6.3'=>'TOP 10 - RISIKEN NACH BEREICH',
'tr_acl_M.RM.6.6.4'=>'TOP 10 - RISIKEN NACH PROZESS',
'tr_acl_M.RM.6.6.5'=>'ANZAHL VON RISIKEN NACH PROZESS',
'tr_acl_M.RM.6.6.6'=>'ANZAHL VON RISIKEN NACH BEREICH',
'tr_acl_M.RM.6.6.7'=>'RISIKO STATUS PER BEREICH',
'tr_acl_M.RM.6.6.8'=>'RISIKO STATUS NACH PROZESS',
'tr_acl_M.RM.6.7'=>'ANDERES',
'tr_acl_M.RM.6.7.1'=>'NUTZER VERANTWORTLICHKEITEN',
'tr_acl_M.RM.6.7.2'=>'SCHWEBENDE EINHEITEN',
'tr_acl_M.RM.6.7.3'=>'Aktiva',
'tr_acl_M.RM.6.7.4'=>'EINHEITEN KLASSIFIZIERUNG',
'tr_acl_M.RM.6.7.5'=>'AKTIVA - ABHÄNGIGKEITEN UND ABHÄNGIGE',
'tr_acl_M.RM.6.7.6'=>'AKTIVA RELEVANZ',
'tr_acl_M.S'=>'Suche',

/* './packages/admin/cfg_general.xml' */

'lb_session_expiracy'=>'Verfallzeit der Sitzung:',
'st_minutes'=>'Minuten.',
'to_session_expiracy'=>'<b>Ablauf der Sitzung:</b><br/><br/>Definiert',
'vb_deactivate'=>'Deaktivieren',

/* './packages/admin/custom_asset_values_name.xml' */

'st_asset_parametrization_cl_bl'=>'<b>Aktiva Wertschätzung:</b>',
'st_importance'=>'Bedeutung (Aktiva)',
'tt_asset_values_name_information'=>'Wie möchten Sie Ihre Wertschätzungsparameter beschriften?',

/* './packages/admin/custom_control.xml' */

'cb_revision'=>'Revision',
'cb_test'=>'Test',
'st_revision_and_test'=>'<b>Kontrollrevision und Test</b>',
'st_revision_and_test_description'=>'Möchten Sie Ihre Sicherheit Kontrolle wiederholen?',
'to_control_revision_config_bl_cl'=>'<b>Kontrolle Revision Konfiguration:</b><br/><br/> Stellt fest',
'to_control_test_config_bl_cl'=>'<b>Test Kontrollkonfiguration:</b> <br/><br/>Stellt fest wenn das System einen Test der Kontrolle erlaubt.<br/>Die Konfiguration wird beurteilen',

/* './packages/admin/custom_control_cost_name.xml' */

'lb_cost_name_1_bl'=>'<b>Kosten Name 1</b>',
'lb_cost_name_2_bl'=>'<b>Kosten Name 2</b>',
'lb_cost_name_3_bl'=>'<b>Kosten Name 3</b>',
'lb_cost_name_4_bl'=>'<b>Kosten Name 4</b>',
'lb_cost_name_5_bl'=>'<b>Kosten Name 5</b>',
'st_costs_estimation_parameters_information'=>'Welche sind die Kostenkategorien',
'static_st_cost_estimation_parameters_bl'=>'<b> Kostenerwartung Parameter</b>',

/* './packages/admin/custom_currency.xml' */


/* './packages/admin/custom_financial_impact.xml' */


/* './packages/admin/custom_financial_impact_parameters.xml' */

'st_custom_financial_impact_parameter_note'=>'Gehen Sie zu der Kategorie',
'st_description_financial_impact_parameter'=>'Minimarisk ermöglicht es Ihnen',
'st_financial_impact_parameters'=>'<b>Finanzielles Auswirkungs-Parameter</b>',

/* './packages/admin/custom_impact_damage.xml' */


/* './packages/admin/custom_impact_type.xml' */


/* './packages/admin/custom_priority.xml' */


/* './packages/admin/custom_quantity_rm_parameters.xml' */

'lb_mode_cl'=>'Modus:',
'rb_3_three'=>'3 (drei)',
'rb_5_five'=>'5 (fünf)',
'rb_optimist'=>'Optimist',
'rb_pessimist'=>'Pessimist',
'st_custom_quantity_parameters_note'=>'Anm.: Sie können die Einstellungen künftig ändern',
'st_risk_quantity_values_bl_cl'=>'<b>Risiko Matrix Definition:</b>',
'tt_custom_quantity_rm_parameters_information'=>'"Gute Beispiele empfehlt Ihnen',

/* './packages/admin/custom_report_classification.xml' */

'lb_to_cl'=>'Zu:',
'st_report_classification'=>'<b>Bericht Klassifizierung</b>',
'st_report_classification_information'=>'Was ist die Klassifizierung (und von wem diese Klassifizierung anzuwenden ist) Möchten Sie die System Tabs benutzen?',
'st_system_classification_bl'=>'<b>System Klassifizierung</b>',
'st_system_classification_information'=>'Welche Art von Klassifizierung (und wer wendet sie an) möchten Sie im System verwenden?',
'to_general_settings_classification'=>'<b>Klassifizierung:</b><br/>Andeutung',
'to_general_settings_classification_dest'=>'<b>An:</b><br/>Zeigt an',

/* './packages/admin/custom_risk_control_values_name.xml' */

'st_rc_impact'=>'Vorgang',
'st_rc_impact5_'=>'Vorgang',
'st_rc_probability'=>'Wahrscheinlichkeit',
'st_risk_control_values_name'=>'<b>Kontrollreduzierungskriterien Label</b>',
'st_very_high'=>'<b>Sehr hoch</b>',
'st_very_low'=>'<b>Sehr niedrig</b>',
'tt_risk_control_value_name_information'=>'"Sofern Sie Kontrolle zum Reduzieren des Risiko beantragen',

/* './packages/admin/custom_risk_limits.xml' */

'lb_risk_limits_high_bl_cl'=>'<b>Risiko Toleranz 2:</b>',
'lb_risk_limits_low_bl_cl'=>'<b>Risiko Toleranz 1:</b>',
'st_risk_limits_definition'=>'Definieren Sie die Werte für die Gefahrenbehandlung.<br/><br/> Risiken deren Bewertung niedriger ist als Toleranzebene 1 ist GRÜN gekennzeichnet.<br/>Risiken deren Bewertung höher ist als Toleranzebene 2 ist ROT gekennzeichnet.<br/>Risiken die zwischen diesen Werten liegen sind GELB gekennzeichnet.',
'tt_aceptable_risk_limits'=>'<b>Akzeptable Risikoebenen</b>',
'tt_risk_limits'=>'Risiko Toleranz',
'wn_high_higher_than_max'=>'Der Grenzwert von Risiko 2 kann nicht grösser sein als das System-Konfigurationslimit.',
'wn_low_higher_than_high'=>'Der Grenzwert von Risiko 1 kann nicht grösser sein als der Grenzwert von Risiko 2.',
'wn_low_lower_than_1'=>'Der Grenzwert von Risiko 1 kann nicht kleiner sein als 1.',
'wn_save_risk_matrix_update_risk_limits'=>'Das System erzeugt eine Aufgabe des Nutzer <b>%chairman_name%</b> für die Bewilligung eines neuen Risiko Limits',

/* './packages/admin/custom_risk_parameters_names.xml' */

'st_risk_parameters_bl_cl'=>'<b>Risikoschätzungs Parameter:</b>',
'st_risk_parameters_help'=>'Tragen Sie die Namen der Eigenschaften ein',
'tt_risk_parameters_information'=>'Wie möchten Sie Ihre Risikoparameterschätzung benennen?',
'wn_risk_parameters_minimum_requirement'=>'Mindestens ein Risiko-Parameter muss genannt werden.',

/* './packages/admin/custom_risk_parameters_weight.xml' */

'st_risk_parameters_weight'=>'<b>Risiko Parameterwertung</b>',
'st_risk_parameters_weight_definition'=>'Definieren Sie die Werte für jeden Risikoparameter. Die Werte müssen grösser oder gleich als Null sein.',
'wn_at_least_one_positive'=>'Mindestens ein Stellenwert muss grösser sein als Null.',

/* './packages/admin/custom_risk_values_name.xml' */

'st_custom_risk_matrix_definition_generic_note'=>'"Anm.: wählen Sie zuerst das Risiko Matrix Grösse (3x3 oder 5x5) im definitiven Thema "" Risiko Matrix Definition"".',
'st_high'=>'<b>Hoch</b>',
'st_high_bl'=>'<b>Hoch</b>',
'st_impact'=>'Vorgang (Risiko)',
'st_impact_risk'=>'Auswirkung (Risiko)',
'st_low'=>'<b>Niedrig</b>',
'st_low_bl'=>'<b>Niedrig</b>',
'st_medium'=>'<b>Mittel</b>',
'st_medium_bl'=>'<b>Mittel</b>',
'st_probability_risk'=>'Wahrscheinlichkeit (Risiko)',
'st_risk_parametrization'=>'<b>Auswirkung & Wahrscheinlichkeits-Erwartung:</b>',
'st_riskprob'=>'Wahrscheinlichkeit (Risiko)',
'st_values'=>'Werte',
'st_very_high_bl'=>'<b>Sehr hoch</b>',
'st_very_low_bl'=>'<b>Sehr niedrig</b>',
'tt_risk_values_name_information'=>'Wie möchten Sie Ihre Auswirkungs- und Wahrscheinlichkeitserwartungskriterien beschreiben?',

/* './packages/admin/custom_special_users.xml' */

'lb_asset_manager_bl'=>'<b>Aktiva Manager</b>',
'lb_chairman_bl'=>'<b>Management</b>',
'lb_control_manager_bl'=>'<b>Kontroll Manager</b>',
'lb_library_manager_bl'=>'<b>Bibliotheks Manager</b>',
'lb_user_disciplinary_process_manager_bl'=>'<b>Disziplinarischer Prozess Manager</b>',
'lb_user_evidence_manager_bl'=>'<b>Aussagen Manager</b>',
'lb_user_incident_manager_bl'=>'<b>Vorgangs Manager</b>',
'lb_user_non_conformity_manager_bl'=>'<b>Nichtübereinstimmungs Manager</b>',
'st_custom_special_users_information'=>'Der Minimarisk Ablaufplan erfordert die Definition einiger Benutzer mit Verantwortung für spezielle Aufgaben vorbestimmt im Kontext eines ERMS. Wer besetzt die Rolle dieser Benutzer in Ihrem System ?',
'st_help'=>'[?]',
'st_special_users_bl'=>'<b>Minimarisk Spezial Nutzer</b>',
'tt_special_user'=>'"<b>Management:</b><br/><br/>Nutzer verantwortlich für die Bewilligung des ersten Geschäftsbereichs',
'tt_special_user_asset_manager'=>'<b>Aktiva Manager:</b><br/><br/>Nutzer Verantwortlich für die Kontrolle der Bestände der Aktiva',
'tt_special_user_control_manager'=>'<b>Kontroll Manager:</b><br/><br/>Nutzer Verantwortlich zum Managen der Sicherheitskontrolle',
'tt_special_user_disciplinary_process_manager'=>'<b>Disziplinarischer Prozess Manager:</b><br/><br/>Nutzer der die disziplinarischen Prozessalarme erhält',
'tt_special_user_evidence_manager'=>'<b>Aussagen Manager:</b><br/><br/>Nutzer',
'tt_special_user_incident_manager'=>'<b>Vorgangs Manager:</b><br/><br/>Nutzer ist verantwortlich für Beglaubigung',
'tt_special_user_library_manager'=>'<b>Bibliotheks Manager:</b><br/><br/>Nutzer ist verantwortlich für die Kontrolle von Minimarisk Bibliotheken',
'tt_special_user_nc_manager'=>'<b>Nichtübereinstimmungs Manager:</b><br/><br/>Nutzer ist verantwortlich für die Beglaubigungen der Nichtübereinstimmungen erstellt von anderen Minimarisk Nutzern.',

/* './packages/admin/custom_system_features.xml' */

'cb_auto_document_creation'=>'Erzeugen Sie das Dokument automatisch nach der Kreation der Elemente im System ?',
'cb_cost'=>'Nutze Kostenerwartungs-Einstellungen?',
'cb_document_manual_versioning'=>'Möchten Sie die Dokumenten-Anleitung Versionsverwaltung nutzen?',
'cb_percentual_risk'=>'Zeige Risikowerte in 0 bis 100 Skala?',
'cb_solicitor'=>'Erlaube Anwalt zum Aufteilen der Zutrittsrechte',
'st_data_collecting'=>'Ermöglicht die Datenerfassung mit täglicher Periodizität',
'st_manual_data_control'=>'Möchten Sie die Anleitung für Datumskontrolle nutzen?',
'st_risk_formula'=>'Nutzen Sie die Proportionale Risiko Formel.',
'st_system_features'=>'<b>System Eigenschaften</b>',
'to_data_collecting_settings'=>'<b>Datenerfassung-Einstellungen:</b><br/><br/>Stellt ein',
'to_delete_cascade'=>'<b>Manuelle Steuerung des Kontrolldatums:</b><br/><br/>Erlaubt dem Benutzer',
'to_document_manual_versioning'=>'<b>Dokumenten-Anleitung Versionsverwaltung:</b><br/><br/>Stellt fest ob das System die Anleitung oder die automatische Versionsverwaltung von Dokumenten nutzt.',
'to_general_settings_automatic_documents_creation'=>'<b>Automatische Dokumentenkreations-Einstellungen:</b><br/><br/>Stellt fest',
'to_percentual_risk_configuration'=>'<b>Risiko Wertskala-Konfiguration:</b><br/><br/>Stellt fest',
'to_procurator_configuration'=>'<b>Rechtsanwalts Konfiguration:</b><br/><br/>Stellt fest wenn das System die Beschaffungsmechanismen während der Abwesenheit des Nutzers erlaubt.',
'to_risk_formula'=>'<b>Proportionale Risiko Formel:</b><br/><br/>Formel',
'to_system_cost_configuration'=>'<b>Kosten Konfiguration:</b><br/><br/>Stellt fest',

/* './packages/admin/custom_type_and_priority_parametrization.xml' */

'si_document'=>'Dokument',
'si_event'=>'Fall',
'si_register'=>'Bericht',
'si_risk'=>'Risiko',
'st_description_type_and_priority_parametrization'=>'Wie beschriften Sie Ihre Elemente?',
'st_element'=>'<b>Element:</b>',
'st_priority_bl'=>'<b>Wichtigkeit</b>',
'st_type_and_priority_parametrization'=>'<b>Typ und Wichtigkeit der Parametersierung</b>',
'st_type_bl'=>'<b>Typ</b>',
'wn_classification_minimum_requirement'=>'Mindestens drei Klassifizierungen müssen pro Einheit vorhanden sein.',

/* './packages/admin/nav_audit_alert.xml' */

'gc_justification'=>'Grund',
'tt_audit_alerts_bl'=>'<b>Bilanzalarm</b>',

/* './packages/admin/nav_audit_log.xml' */

'lb_user_cl'=>'Nutzer:',
'si_all_actions'=>'Alle Aktionen',
'si_all_users'=>'Alle Nutzer',
'tt_audit_log_bl'=>'<b>Bilanz Log</b>',

/* './packages/admin/nav_audit_task.xml' */

'gc_accomplishment'=>'Durchführung',
'gc_creation'=>'Erstellung',
'gc_receiver'=>'Empfänger',
'lb_activity_cl'=>'Aktivität:',
'lb_creator_cl'=>'Autor:',
'lb_receiver_cl'=>'Empfänger:',
'si_all_activities'=>'Alle Aktivitäten',
'si_all_creators'=>'Alle Autoren',
'si_all_receivers'=>'Alle Empfänger',
'tt_audit_task_bl'=>'<b>Bilanz-Aufgabe</b>',

/* './packages/admin/nav_config.php' */

'st_check_for_correct_data'=>'Überprüfen Sie',
'st_impact_parameter_remove_message'=>'Die Verschiebung des finanziellen Vorgangparameter wurde verursacht von der Verschiebung dieses Parameter in die Vorgänge. Möchten Sie damit fortfahren?',
'st_info_saved_but'=>'Die Information wurde gespeichert. Dennoch',
'st_not_possible_to_update_data_collection_info'=>'es war nicht möglich zu aktualisieren die Datensammlungsinformation',
'st_not_possible_to_update_mail_server_info'=>'es war nicht möglich die email Server  Information zu aktualisieren',
'st_unavailable_information'=>'nicht verfügbare Information.',
'tt_alert'=>'Alarm',
'tt_deactivate_system'=>'Deaktivieren Sie das System',
'tt_impact_parameter_remove'=>'Entfernen Sie finanziellen Auswirkungs-Parameter',

/* './packages/admin/nav_config.xml' */

'si_costs_names'=>'Kosten Namen',
'si_parametrization'=>'Geschätzt',
'si_risk_parameters_values'=>'Risiken Eckwerte',
'si_syslog'=>'Syslog',
'si_system'=>'System',

/* './packages/admin/nav_customization.php' */

'st_change_amount_of_parameters_confirm'=>'Wechsel der Anzahl von Parametern wird das System verunsichern.<BR><b> Sind Sie sicher das tun zu wollen?</b>',
'st_change_amount_of_risks_confirm'=>'Wechsel der Anzahl von Risiken veranlasst das System alle Risiken nochmals neu zu berechnen!<BR><b>Sind Sie sicher das tun zu wollen?</b>',
'st_remove_classification_error'=>'Ihr Wechsel wurde gespeichert. Dennoch',
'st_risk_parameters_weight_edit_confirm'=>'Sie wechseln sensible Daten. Sofern Sie den Parameterumfang verändern',
'tt_remove_classification_error'=>'Fehler beim Verschieben Klassifizierung',
'tt_save_parameters_names'=>'Speichere Parameter Namen',
'tt_save_parameters_values'=>'Speichere Parametervolumen',
'tt_sensitive_data'=>'Sensitive Daten',

/* './packages/admin/nav_customization.xml' */

'lb_settings_cl'=>'Kundenanpassung:',
'si_aceptable_risk_limits'=>'Akzeptable Levels des Risiko',
'si_control_cost_names'=>'Kostenerwartungs-Parameter',
'si_control_revision_and_test'=>'Kontrollrevision und Test',
'si_financial_impact_parameters'=>'Finanzielle Vorgangs Parameters',
'si_impact_and_importance_dimensions'=>'Risikoerwartungsparameter',
'si_parametrization_asset'=>'Aktiva Wertschätzungs-Label',
'si_parametrization_control'=>'Kontrollreduktion Kriterien Label',
'si_parametrization_risk'=>'Vorgang& Wahrscheinlichkeitserwartungs Label',
'si_priority_and_type_parametrization'=>'Typ und Wertstellung Parameterisation',
'si_report_classification'=>'Bericht Klassifizierung',
'si_risk_parameters_weight'=>'Risiko Parameterwert',
'si_special_users'=>'Spezial Nutzer',
'si_system_classification'=>'System Klassifizierung',
'si_system_features'=>'System Einstellungen',
'si_system_rm_parameters_quantity'=>'Risiko Matrix Definition',
'wn_updating_risk_values'=>'Veränderungen',
'wn_values_successfully_changed'=>'Werte erfolgreich aktualisiert.',

/* './packages/admin/nav_my_account.php' */

'st_activation_date'=>'Aktivierungs Datum',
'st_cancel_service'=>'Anullierungsdienst',
'st_cancel_service_email_message'=>'Ein Service-Annullierung wurde angefragt von \'%client%\'.',
'st_client_name'=>'Kunden Name',
'st_confirm_cancel_service_message'=>'Sind Sie sicher den Service zu annullieren?',
'st_deactivate_account_text'=>'Zur Deaktivierung Ihres Kontos',
'st_expiracy_date'=>'Ablauf Datum',
'st_expiracy_date_never'=>'Niemals',
'st_license_type'=>'Lizenz Typ',
'st_max_simult_users'=>'Max. zeitgleicher Nutzer',
'st_number_of_users'=>'Anzahl der Nutzer',
'tt_cancel_service'=>'Anullierungsdienst',
'tt_confirm_cancel_service'=>'Service Annullierung',
'tt_deactivate_account'=>'Deaktiviere Konto',

/* './packages/admin/nav_my_account.xml' */

'lb_email_cl'=>'E-mail:',
'st_cancel_service_bl'=>'<b>Dienst-Annullierung</b>',
'st_change_email_bl'=>'<b>Administrativer email Wechsel</b>',
'st_deactivate_account_bl'=>'<b>Deaktiviere Konto</b>',
'st_license_info_bl'=>'<b>Lizenz Information:</b>',
'st_upgrade_license_bl'=>'<b>Lizenz Upgrade</b>',
'st_upgrade_license_message'=>'"Aktivieren Sie bitte Ihre Benutzersätze mit dem Aktivierungscode',
'st_upgrade_license_text'=>'"Aktivieren Sie bitte Ihren Benutzer-Satz (für 10 Benutzer) und benutzen bitte den Aktivierungscode',
'vb_activate'=>'Aktiviere',
'vb_cancel_service'=>'Dienstanullierung',
'vb_change'=>'Wechsel',
'vb_deactivate_account'=>'Deaktiviere mein Konto',
'vb_upgrade'=>'Verbesserung',
'wn_email_changed'=>'Die Email wurde erfolgreich gewechselt',
'wn_invalid_activation_code'=>'Einige Codes sind unvollständig. Bitte',
'wn_successful_activation'=>'Lizenz erfolgreich aufgewertet.',

/* './packages/admin/nav_profile.php' */

'st_profile_remove_message'=>'Sind Sie sicher Sie möchten Profile <b>%profile_name%</b> verschieben?',
'tt_remove_profile'=>'Verschiebe Profile',

/* './packages/admin/nav_profile.xml' */

'mi_visualize'=>'Zeige',
'tt_profiles_bl'=>'<b>Profile</b>',

/* './packages/admin/nav_system_configuration_on_site.xml' */

'lb_system_settings_cl'=>'System Einstellungen',
'si_delete_cascade'=>'Löschungs-Kaskade',
'si_email_setup'=>'Diese Konfiguration bestimmt welche Zeitzone das System nutzen wird.',
'si_password_policy'=>'Passwort Regelungen',
'si_server_setup'=>'Wechseln der Zeitzone:',
'si_timezone_setup'=>'<b>Wähle Zeitzone</b>',
'wn_changes_successfully_saved'=>'Veränderungen',

/* './packages/admin/nav_trash.php' */

'st_all_items_remove_confirm'=>'Alle Einheiten im Papierkorb sollen permanent verschoben werden! Fortfahren?',
'st_items_restore_confirm'=>'Möchten Sie die ausgewählten Einheiten erneuern?',
'st_not_excluded_users_and_profile_message'=>'Der ausgewählte Nutzer konnte nicht verschoben werden weil es verbundene Einheiten im Papierkorb gibt. <br/> Die ausgewählten Profile konnten nicht verschoben werden weil es existierende Nutzer im Papierkorb gibt',
'st_not_removed_profiles_message'=>'Die ausgewählten Profile konnten nicht verschoben werden weil es existierende Nutzer im Papierkorb gibt',
'st_not_restored_documents_message'=>'"Die ausgewählten Dokumente können nicht restauriert werden',
'st_not_restored_items_message'=>'Ausgewählte Einheiten konnten nicht erneuert werden da sie mit einem oder mehreren enthaltenen Nutzer verschoben wurden!',
'st_not_restored_users_message'=>'Der ausgewählte Nutzer konnte nicht verschoben werden weil verbundene Einheiten im Papierkorb sind.',
'st_not_selected_items_remove_confirm'=>'Einheiten die nicht ausgewählt sind und Untereinheiten sind oder angeschlossen an ausgewählte Einheiten sollen verschoben werden. Möchten Sie fortfahren?',
'tt_all_items_permanent_removal'=>'Permanente Verschiebung aller Einheiten',
'tt_document_not_restored'=>'Dokumente nicht erneuert',
'tt_items_not_restored'=>'Einheiten nicht erneuert',
'tt_items_permanent_removal'=>'Permanente Verschiebung von Einheiten',
'tt_items_restoral'=>'Einheiten Restoration',
'tt_profile_not_deleted'=>'Die Profile wurden nicht verschoben',
'tt_users_and_profile_not_deleted'=>'Nutzer und Profile wurden nicht verschoben',
'tt_users_not_deleted'=>'Der Nutzer wurde nicht verschoben',

/* './packages/admin/nav_trash.xml' */

'tt_bin_bl'=>'<b>Papierkorb</b>',
'vb_remove_all'=>'Verschiebe Alle',
'wn_elements_removed_successfully'=>'Einheit(en) erfolgreich verschoben.',
'wn_elements_restaured_successfully'=>'Einheit(en) erfolgreich erneuert.',

/* './packages/admin/nav_users.php' */

'em_alerts_and_tasks_transfer'=>'Transfer von Aufgaben und Alarmen',
'st_choose_user_message'=>'Im nächsten Fenster',
'st_reset_all_users_passwords_confirm'=>'Sind Sie sicher das Passwort von <b>alle users</b> zurückzusetzen und das neue Passwort zu deren Email zu senden?',
'st_user_remove_confirm'=>'Sind Sie sicher Sie möchten den Nutzer <b>%user_name%</b> verschieben ?',
'tt_choose_user_warning'=>'Warnung vor Nutzerauswahl',
'tt_remove_user'=>'Verschieben Nutzer',
'tt_reset_all_users_password'=>'Reset Passwort von allen Nutzern',

/* './packages/admin/nav_users.xml' */

'gc_email'=>'E-mail',
'gc_profile'=>'Profile',
'mi_block'=>'Blockiere',
'mi_unblock'=>'Nicht mehr blockieren',
'tt_users_bl'=>'<b>Nutzer</b>',
'vb_reset_passwords'=>'Erneuere Passwörter',

/* './packages/admin/popup_associate_elements.xml' */

'lb_associate_to_cl'=>'Angeschlossen bei:',
'tt_items_association'=>'Einheiten-Verbindung',
'vb_cancel_all'=>'Lösche alle',

/* './packages/admin/popup_configure_authentication.php' */

'st_connection_failed'=>'Verbindung fehlgeschlagen.',
'st_database_structure_wrong'=>'Datenbank hat nicht die erwartete Struktur.',
'st_successful_connection'=>'Verbindung erfolgreich.',

/* './packages/admin/popup_configure_authentication.xml' */

'lb_databse_bl_cl'=>'<b>Datenbank:</b>',
'lb_host_bl_cl'=>'<b>Host:</b>',
'lb_password_bl_cl'=>'<b>Passwort:</b>',
'st_postgres_authentication_data_needed'=>'Die folgenden Daten werden angefordert',
'tt_setup_authentication'=>'Einstellung der Authentisieren',
'vb_test'=>'Test',

/* './packages/admin/popup_deactivate.php' */

'em_deactivate_account'=>'Kontodeaktivierung',
'em_deactivate_account_email_message'=>'Kunde \'%client%\' hat eine Kontendeaktivierung angefragt.<br><b>Grund:</b><br>%deactivation_reason%',

/* './packages/admin/popup_deactivate.xml' */

'lb_deactivation_reason_cl'=>'"Bitte',
'st_confirm_deactivate_account_message'=>'Sind Sie sicher dass Sie Ihr Konto stilllegen möchten?',
'tt_confirm_deactivate_account'=>'Lege Konto still',

/* './packages/admin/popup_no_access.xml' */

'st_user_wo_system_access'=>'Sie haben keine Erlaubnis zum Systemzutritt. Klicken Sie <b>Ok</b> oder schliessen Sie dieses Fenster um auszuloggen.',
'tt_user_wo_system_access'=>'Nutzer ohne Erlaubnis zum Systemzutritt',

/* './packages/admin/popup_profile_edit.php' */

'tt_edit_profile'=>'Schreibe Profile',
'tt_insert_profile'=>'Hinzufügen Profile',
'tt_visualizae_profile'=>'Zeige Profile',

/* './packages/admin/popup_profile_edit.xml' */

'st_profile_warning'=>'* Dieses Profil kann nicht beschrieben werden.',

/* './packages/admin/popup_restore_name_conflict.xml' */

'st_name_conflicts_message'=>'"Es wurde festgestellt',
'tt_name_conflicts_found'=>'Name Konflikte gefunden',

/* './packages/admin/popup_restore_structure_and_name_conflict.xml' */

'rb_all_except_name_conflict'=>'Alle bis auf diejenige mit dem Namenskonflikt *',
'rb_all_except_structure_conflict'=>'Alle bis auf diejenige',
'st_structure_name_conflicts_message'=>'Es ist ermittelt worden',
'tt_structure_and_name_conflicts_found'=>'Strukturen und Namens-Konflikte gefunden',
'wn_not_restoring_name_conflict'=>'* keine Erneuerung von Einheiten mit Namenskonflikt kann strukturelle Konflikte verursachen.',

/* './packages/admin/popup_restore_structure_conflict.xml' */

'cb_associate_structure_conflicted_items_individually'=>'Individuell angeschlossene Einheiten mit strukturellen Konflikten zu bestehenden Einheiten im System.',
'rb_all'=>'Alle',
'rb_only_not_conflicting'=>'Nur diejenige',
'st_structure_conflicts_message'=>'"Es wurde entdeckt',
'tt_structure_conflicts_found'=>'Strukturenkonflikt gefunden',
'vb_restore'=>'Erneuere',

/* './packages/admin/popup_upgrade_license.xml' */

'lb_activation_code_cl'=>'Aktivierungscode:',
'tt_upgrade_license'=>'Lizenz-Erneuerung',

/* './packages/admin/popup_user_edit.php' */

'em_new_password'=>'Neues Passwort',
'em_new_user'=>'Willkommen bei Minimarisk',
'em_new_user_footer_bl'=>'<b>Für weitere Information',
'st_reset_user_password_confirm'=>'Sind Sie sicher das Nutzerpasswort zu erneuern und das neue Passwort an seine e-Mail zu senden?',
'tt_profile_edit_error'=>'Fehler beim Wechsel des Profils',
'tt_reset_user_password'=>'Reset Nutzer Passwort',
'tt_user_adding'=>'Nutzer hinzufügen',
'tt_user_editing'=>'Nutzer Beschreibung',
'tt_user_insert_error'=>'Nutzer limit erreicht',
'wn_users_limit_reached'=>'"Die Zahl der genehmigten Benutzern ist erreicht worden. Ab sofort können<b>Dokumente nur Benutzer mit gelesenem</b> Profil (Nutzer',

/* './packages/admin/popup_user_edit.xml' */

'cb_change_password_on_login'=>'Wechseln Sie das Passwort beim nächsten Login',
'lb_confirmation_cl'=>'Bestätigung:',
'lb_email_bl_cl'=>'<b>E-Mail:</b>',
'lb_language_bl_cl'=>'<b>Sprache:</b>',
'lb_login_bl_cl'=>'<b>Login:</b>',
'lb_new_password_cl'=>'Neues Passwort:',
'lb_profile_bl_cl'=>'<b>Profile:</b>',
'st_language_bl_cl'=>'<b>Sprache:</b>',
'vb_reset_password'=>'Erneuere Passwort',
'wn_different_passwords'=>'Unterschiedliche Passwörter',
'wn_existing_email'=>'Email gibt es bereits',
'wn_existing_login'=>'Login gibt es bereits',
'wn_invalid_email'=>'Unvollständige Email',
'wn_password_changed'=>'Passwort gewechselt',
'wn_weak_password'=>'Ihr Passwort erfüllt nicht die Bedingungen der Passwort-Regelungen. Bitte',
'wn_wrong_current_password'=>'Falsches Passwort',

/* './packages/admin/report/popup_report_audit_alert.php' */

'rs_alerts_audit'=>'Alarmbilanz',

/* './packages/admin/report/popup_report_audit_log.php' */

'rs_systems_log_audit'=>'System Log Bilanz',

/* './packages/admin/report/popup_report_audit_task.php' */

'rs_tasks_audit'=>'Aufgaben Bilanz',

/* './packages/admin/report/popup_report_audit_task.xml' */

'rs_accomplishment'=>'Durchführung',
'rs_creation'=>'Erstellung',
'rs_creator'=>'Ersteller',
'rs_receiver'=>'Empfänger',

/* './packages/admin/system_configuration_delete_cascade.xml' */

'cb_delete_cascade'=>'Lösche Kaskade',
'st_delete_cascade'=>'<b>Lösche Kaskade</b>',
'st_system_configuration_delete_cascade_information'=>'"Stellt fest',

/* './packages/admin/system_configuration_email_setup.xml' */

'lb_email_encryption_cl'=>'GMT +6',
'lb_email_host_cl'=>'GMT +2',
'lb_email_password_cl'=>'GMT +5',
'lb_email_port_cl'=>'GMT +3',
'lb_email_user_cl'=>'GMT +4',
'si_email_none'=>'GMT +7',
'si_email_ssl'=>'GMT +9',
'si_email_ssl2'=>'GMT +10',
'si_email_ssl3'=>'GMT +11',
'si_email_tls'=>'GMT +8',
'st_configuration_email_config_information'=>'GMT +1',
'st_email_config_bl_cl'=>'GMT',

/* './packages/admin/system_configuration_password_policy.xml' */

'cb_case_characters'=>'Grossbuchstaben/Kleinbuchstaben',
'cb_numeric_characters_cl'=>'Nummern:',
'cb_special_characters_cl'=>'Spezielle Zeichen:',
'lb_days_before_warning'=>'Erinne an Passwortablauf:',
'lb_label_block_limit_cl'=>'Block Begrenzung:',
'lb_minimum_characters_cl'=>'Minimum Länge:',
'lb_password_history_apply_cl'=>'Anwendung der Passwort-Geschichte:',
'lb_password_max_lifetime_cl'=>'Passwort Lebenszeit:',
'st_configuration_password_policy_information'=>'Welche Passwort-Regelung möchten Sie im System nutzen?',
'st_days_before'=>'Tage zuvor.',
'st_default_configuration'=>'Zurückstellen zur Konfigurationsgrundeinstellung',
'st_invalid_logon'=>'unzulässige Einlog-Versuche',
'st_memorized_passwords'=>'Speichere Passwörter.',
'st_password_policy_bl_cl'=>'<b>Passwort Regelung:</b>',
'wn_min_char_warning'=>'Der Minimalwert muss gleich oder grösser sein als die Anzahl der Spezialcharakter',

/* './packages/admin/system_configuration_server_setup.xml' */

'lb_server_dns_cl'=>'GMT -3',
'lb_server_gateway_cl'=>'GMT -4',
'lb_server_ip_cl'=>'GMT -2',
'lb_server_update_cl'=>'GMT -5',
'st_configuration_server_config_information'=>'GMT -1',
'st_server_config_bl_cl'=>'GMT +12',

/* './packages/admin/system_configuration_timezone_setup.xml' */

'cb_timezone_setup'=>'GMT -8',
'si_timezone_-1'=>'GMT -1',
'si_timezone_-10'=>'GMT -10',
'si_timezone_-11'=>'GMT -11',
'si_timezone_-12'=>'GMT -12',
'si_timezone_-2'=>'GMT -2',
'si_timezone_-3'=>'GMT -3',
'si_timezone_-4'=>'GMT -4',
'si_timezone_-5'=>'GMT -5',
'si_timezone_-6'=>'GMT -6',
'si_timezone_-7'=>'GMT -7',
'si_timezone_-8'=>'GMT -8',
'si_timezone_-9'=>'GMT -9',
'si_timezone_+1'=>'GMT -10',
'si_timezone_+10'=>'GMT +10',
'si_timezone_+11'=>'GMT +11',
'si_timezone_+12'=>'GMT +12',
'si_timezone_+2'=>'GMT -11',
'si_timezone_+3'=>'GMT -12',
'si_timezone_+4'=>'Wechsele Zeitzone',
'si_timezone_+5'=>'Datum',
'si_timezone_+6'=>'GMT +6',
'si_timezone_+7'=>'GMT +7',
'si_timezone_+8'=>'GMT +8',
'si_timezone_+9'=>'GMT +9',
'si_timezone_0'=>'GMT -9',
'st_system_configuration_timezone_setup_information'=>'GMT -7',
'st_timezone_setup'=>'GMT -6',

/* './packages/admin/tab_audit.xml' */

'ti_alerts'=>'Alarme',
'ti_log'=>'Log',
'ti_tasks'=>'Aufgaben',

/* './packages/admin/tab_main.php' */

'st_survey_link'=>'http://www.surveymonkey.com/s.aspx?sm=F9BFTdst8Ginn7PdgNGxFA_3d_3d',
'to_ip_cl'=>'IP:',
'to_last_login_cl'=>'Letztes Login:',

/* './packages/admin/tab_main.xml' */

'lb_feedback'=>'Nachricht',
'mx_bug'=>'Bug',
'mx_content_issue'=>'Vorschlag',
'mx_feature_request'=>'Fortschritt',
'st_about_isms_bl'=>'<b>Über</b>',
'st_buy_now_bl'=>'<b>Kaufen Sie jetzt</b>',
'st_pipe_bl'=>'<b>|</b>',
'st_preferences_bl'=>'<b>Vorzüge</b>',
'st_satisfaction_survey_bl'=>'<b>Nutzer-Erfahrungsübersicht</b>',
'st_send_us_feedback_cl'=>'Hinterlasse eine Nachricht:',
'st_software_license_bl'=>'<b>Lizenz</b>',
'ti_audit'=>'Bilanz',
'ti_bin'=>'Papierkorb',
'ti_customization'=>'Kundenanpassung',
'ti_my_account'=>'Mein Konto',
'ti_profiles'=>'Profile',
'ti_system_settings'=>'System Einstellungen',
'ti_users'=>'Nutzer',
'vb_exit'=>'Ausloggen',
'vb_feedback'=>'Antwort',
'vb_help'=>'Hilfe',
'vb_user_view'=>'Zeige Nutzer',

/* './packages/continuity/nav_asset.xml' */


/* './packages/continuity/nav_place_threat.xml' */


/* './packages/continuity/nav_places.xml' */


/* './packages/continuity/nav_plan.xml' */


/* './packages/continuity/nav_plan_action.xml' */


/* './packages/continuity/nav_plan_schedule.xml' */


/* './packages/continuity/nav_plan_test.xml' */


/* './packages/continuity/nav_process.xml' */


/* './packages/continuity/nav_process_activity_asset.xml' */


/* './packages/continuity/nav_process_scene.xml' */


/* './packages/continuity/nav_process_threat.xml' */


/* './packages/continuity/nav_providers.xml' */


/* './packages/continuity/nav_resources.xml' */


/* './packages/continuity/nav_scope.xml' */


/* './packages/continuity/nav_un.xml' */


/* './packages/continuity/popup_about_asset.xml' */


/* './packages/continuity/popup_activity_edit.xml' */


/* './packages/continuity/popup_address_locator.xml' */


/* './packages/continuity/popup_area_search.xml' */


/* './packages/continuity/popup_asset_associate_threat.xml' */


/* './packages/continuity/popup_asset_edit.xml' */


/* './packages/continuity/popup_department_edit.xml' */


/* './packages/continuity/popup_financial_impact_edit.xml' */


/* './packages/continuity/popup_group_edit.xml' */


/* './packages/continuity/popup_group_resource_single_search.xml' */

'mx_friday'=>'Freitag',

/* './packages/continuity/popup_group_search.xml' */


/* './packages/continuity/popup_group_single_search.xml' */


/* './packages/continuity/popup_people_search.xml' */


/* './packages/continuity/popup_place_asset_edit.xml' */


/* './packages/continuity/popup_place_associate_threat.xml' */


/* './packages/continuity/popup_place_edit.xml' */


/* './packages/continuity/popup_place_resource_edit.xml' */


/* './packages/continuity/popup_place_search.xml' */


/* './packages/continuity/popup_place_single_search.xml' */


/* './packages/continuity/popup_plan_action_edit.xml' */


/* './packages/continuity/popup_plan_action_search.xml' */


/* './packages/continuity/popup_plan_schedule_edit.xml' */


/* './packages/continuity/popup_plan_search.xml' */


/* './packages/continuity/popup_plan_test_edit.xml' */


/* './packages/continuity/popup_plan_type_edit.xml' */


/* './packages/continuity/popup_planRange_edit.xml' */


/* './packages/continuity/popup_process_draw.xml' */


/* './packages/continuity/popup_process_edit.xml' */


/* './packages/continuity/popup_process_search.xml' */


/* './packages/continuity/popup_provider_edit.xml' */


/* './packages/continuity/popup_provider_search.xml' */


/* './packages/continuity/popup_provider_single_search.xml' */


/* './packages/continuity/popup_resource_edit.xml' */


/* './packages/continuity/popup_resource_search.xml' */


/* './packages/continuity/popup_scene_edit.xml' */


/* './packages/continuity/popup_scene_impact_edit.xml' */


/* './packages/continuity/popup_scope_search.xml' */


/* './packages/continuity/popup_scope_view.xml' */


/* './packages/continuity/popup_vulnerability_edit.xml' */


/* './packages/continuity/tab_continuity_management.xml' */


/* './packages/improvement/nav_action_plan.php' */

'mx_corrective'=>'Berichtigt',
'mx_preventive'=>'Präventiv',
'st_ap_remove_message'=>'Sind Sie sicher Sie möchten den Aktionsplan<b>%name%</b>  verschieben?',
'tt_ap_remove'=>'verschiebe Aktionsplan',
'tt_nc_filter_grid_action_plan'=>'(gefiltert von Nichtübereinstimmung \'<b>%nc_name%</b>\')',

/* './packages/improvement/nav_action_plan.xml' */

'gc_actiontype'=>'Art der Handlung',
'gc_ap_cns'=>'Fehlende Übereinstimmungen',
'gc_dateconclusion'=>'Fertigstellungsdatum',
'gc_datedeadline'=>'Stichtag',
'gc_isefficient'=>'Effizient',
'gc_responsible_id'=>'Verantwortlich',
'mi_data_to_meassure'=>'Definieren Sie das Mass',
'mi_meassure'=>'Mass',
'tt_ap_bl'=>'<b>Handlungsplan</b>',

/* './packages/improvement/nav_disciplinary_process.xml' */

'gc_action_taken'=>'Aktion ausgeführt',
'mi_register_action'=>'Register Aktion',

/* './packages/improvement/nav_incident.php' */

'mx_direct'=>'Direkt',
'mx_indirect'=>'Indirekt',
'mx_no_category'=>'Keine Kategorie',
'st_incident_remove_confirm'=>'Sind Sie sicher Sie möchten den Vorgang <b>%incident_name%</b> verschieben?',
'tt_incident_remove_confirm'=>'verschiebe Vorgang',

/* './packages/improvement/nav_incident.xml' */

'gc_incident_date_limit'=>'geschätzt',
'gc_loss_type'=>'Verlust Typ',
'mi_disciplinary_process'=>'Disziplinarischer Prozess',
'mi_finish'=>'Fertigstellung',
'mi_immediate_disposal_approval'=>'Sofortige Beseitigungs- Bewilligung',
'mi_solution_approval'=>'Lösungs-Bewilligung',
'tt_incident'=>'<b>Vorgang</b>',

/* './packages/improvement/nav_non_conformity.php' */

'gc_control_security'=>'Sicherheits Fall',
'gc_external_audit'=>'Externe Bilanz',
'gc_internal_audit'=>'Interne Bilanz',
'gc_no'=>'Nein',
'gc_yes'=>'Ja',
'st_nc_remove_message'=>'Sind Sie sicher die Nichtübereinstimmung <b>%name%</b>  zu verschieben?',
'tt_ap_filter_grid_non_conformity'=>'(gefiltert von Aktionsplan \'<b>%ap_name%</b>\')',
'tt_nc_remove'=>'verschiebe Nichtübereinstimmung',

/* './packages/improvement/nav_non_conformity.xml' */

'gc_action_plans'=>'Aktionsplan',
'gc_capability'=>'Potentiell',
'gc_sender'=>'Sender',
'gc_state'=>'Status',
'mi_action_plan'=>'Angeschlossene Aktionsplan',
'mi_send_to_ap_pendant'=>'Sende zur AP Akzeptanz',
'mi_send_to_conclusion'=>'Sende zur Fertigstellung',
'mi_send_to_responsible'=>'Sende zu Verantwortlich',
'tt_nc_bl'=>'<b>Nichtübereinstimmung</b>',

/* './packages/improvement/nav_occurrence.php' */

'st_occurrence_remove'=>'Sind Sie sicher das Auftreten <b>%occurrence_text%</b>  zu verschieben?',
'tt_occurrence_remove'=>'Auftritt verschieben',

/* './packages/improvement/nav_occurrence.xml' */

'gc_incident'=>'Vorgang',
'mi_incident'=>'Angeschlossener Vorgang',
'tt_occurrences'=>'<b>Auftritte</b>',

/* './packages/improvement/nav_report.php' */

'mx_report_action_plan_by_process'=>'Aktionsplan von Prozess',
'mx_report_action_plan_by_user'=>'Aktionsplan von Nutzer',
'mx_report_ap_without_doc'=>'Aktionsplan ohne Dokument',
'mx_report_ci_pendant_tasks'=>'Schwebende Aufgaben in kontinuierlicher Verbesserung',
'mx_report_control_nonconformity_by_process'=>'Nichtübereinstimmung der Kontrolle von Prozess',
'mx_report_elements_affected_by_incident'=>'Einheiten beeinträchtigt von Ereignissen',
'mx_report_financial_impact_by_incident'=>'Finanzieller Vorgang von Vorgang',
'mx_report_incident_accompaniment'=>'Vorgangsanschluss',
'mx_report_incident_by_responsible'=>'Indiz Sammlung',
'mx_report_incident_control_revision'=>'Kontroll Revisions verursacht von Ereignissen',
'mx_report_incident_without_risk'=>'Vorgang ohne Risiken',
'mx_report_incidents_without_occurrence'=>'Vorgang ohne Ereignisse',
'mx_report_nc_without_ap'=>'Nichtübereinstimmung ohne Aktionsplan',
'mx_report_nonconformity_accompaniment'=>'Nichtübereinstimmung Anschluss',
'mx_report_nonconformity_by_process'=>'Nichtübereinstimmung von Prozess',
'mx_report_occurrence_accompaniment'=>'Vorfälle Anschluss',
'mx_report_occurrence_by_incident'=>'Vorfälle von Vorgang',
'mx_report_risk_auto_probability_value'=>'Automatische Risiko Wahrscheinlichkeits- Kalkulation',
'mx_report_user_by_incident'=>'Disziplinarischer Prozess',

/* './packages/improvement/nav_report.xml' */

'cb_risk'=>'Risiko',
'lb_conclusion_cl'=>'Fertigstellung:',
'lb_loss_type_cl'=>'Verlust Typ:',
'lb_show_cl'=>'Zeige:',
'si_capability_all'=>'Alle',
'si_capability_no'=>'Nein',
'si_capability_yes'=>'ja',
'si_ci_manager'=>'Kontinuierlicher Fortschrittsberichte',
'si_incident_order_by_category'=>'Kategorie',
'si_incident_order_by_responsible'=>'Verantwortlich',
'si_incident_report'=>'Vorgangsberichte',
'si_nonconformity_report'=>'Nichtübereinstimmungsberichte',
'st_action_plan_filter_bl'=>'<b>Aktionsplan Filter</b>',
'st_elements_filter_bl'=>'<b>Einheiten Filter</b>',
'st_incident_filter_bl'=>'<b>Vorgangs Filter</b>',
'st_nonconformity_filter_bl'=>'<b>Nichtübereinstimmung Filter</b>',
'st_occurrence_filter_bl'=>'<b>Auftritts Filter</b>',
'st_status'=>'Status:',
'wn_at_least_one_element_must_be_selected'=>'Mindestens eine Einheit sollte gewählt werden.',

/* './packages/improvement/popup_action_plan_associate.xml' */

'tt_action_plan_association'=>'Handlungsplan-Verbindung',
'tt_current_action_plans_bl'=>'<b>Derzeitige Handlungspläne</b>',

/* './packages/improvement/popup_action_plan_edit.xml' */

'lb_ap_actionplan_bl_cl'=>'<b>Action:</b>',
'lb_ap_actiontype_bl_cl'=>'<b>Art der Handlung:</b>',
'lb_ap_dateconclusion_cl'=>'Fertigstellungsdatum:',
'lb_ap_datedeadline_bl_cl'=>'<b>Stichtag:</b>',
'lb_ap_name_bl_cl'=>'<b>Name:</b>',
'lb_ap_responsible_id_bl_cl'=>'<b>Verantwortlich:</b>',
'rb_corrective'=>'Gegenmittel',
'rb_preventive'=>'Vorbeugungsmassnahme',
'tt_ap_edit'=>'Aktionsplan Edition',
'vb_finish'=>'Fertigstellung',
'wn_deadline'=>'Der Stichtag sollte gleich oder später als der Stichtag sein.',

/* './packages/improvement/popup_action_plan_finish_confirm.xml' */

'lb_ap_days_before_bl'=>'<b>Mitteilung bevor</b>',
'lb_date_efficiency_revision_bl_cl'=>'<b>Effizientes Revisionsdatum:</b>',
'st_action_plan_finish_confirm_message'=>'Möchten Sie den Arbeitsplan \'%name%\' beenden?',
'tt_action_plan_finish'=>'Fertigstellung Aktionsplan',
'wn_revision_date_after_conclusion_date'=>'Das effiziente Revisionsdatum sollte gleich dem Fertigstellungsdatum oder später sein!',

/* './packages/improvement/popup_action_plan_revision_task.xml' */

'lb_action_cl'=>'Aktion:',
'st_action_plan_efficient_bl'=>'<b>War der Aktionsplan effizient?</b>',
'st_data_revision_bl_cl'=>'<b>Revisionsdatum:</b>',
'tt_action_plan_efficiency_revision'=>'Aktionsplan Effizienz Revision',

/* './packages/improvement/popup_disciplinary_process_action.php' */

'st_denied_permission_to_edit_action_taken'=>'Sie haben keine Erlaubnis zum Registrieren einer Aktion in einem disziplinarischen Prozess.',

/* './packages/improvement/popup_disciplinary_process_action.xml' */

'lb_action_bl_cl'=>'<b>Aktion:</b>',
'st_incident_cl'=>'<b>Vorgang:</b>',
'st_user_cl'=>'<b>Nutzer:</b>',

/* './packages/improvement/popup_disciplinary_process_edit.php' */

'em_disciplinary_process_creation'=>'Disziplinarischer Prozess Erstellung',
'em_disciplinary_process_creation_footer'=>'Für weitere Information',
'st_incident_user_remove'=>'Sind Sie sicher den Nutzer <b>%user_name%</b>  zu verschieben vom disziplinarischen Prozess vom Vorgang <b>%incident_name%</b>?',
'tt_incident_user_remove'=>'verschiebe Nutzer',

/* './packages/improvement/popup_disciplinary_process_edit.xml' */

'gc_observation_involvement'=>'Beobachtung / Engagement',
'gc_user'=>'Nutzer',
'lb_incident_cl'=>'Ereignis:',
'lb_observation_involvement_cl'=>'Beobachtung / Engagement:',
'lb_user_bl_cl'=>'<b>Nutzer:</b>',
'tt_disciplinary_process'=>'<b>Disziplinarischer Prozess</b>',
'wn_email_dp_sent'=>'Eine e-Mail wurde bereits an den Disziplinar Prozess Manager gesendet.',

/* './packages/improvement/popup_evidence_requirement.xml' */

'lb_comments_cl'=>'Bemerkungen:',
'tt_evidence_requirement'=>'Beweisführung:',
'wn_evidence_requirement_editing'=>'Anm. Wenn oben ein Kommentar eingefügt wurde',

/* './packages/improvement/popup_financial_impact_edit.xml' */

'st_total'=>'Total',
'tt_financial_impact_parametrization'=>'Finanzielle Auswirkungs- Schätzung',

/* './packages/improvement/popup_incident_accounts_plan.xml' */

'tt_incident_accounts_plan'=>'Bilanzsplan',

/* './packages/improvement/popup_incident_affected_product_service.xml' */

'tt_affected_product_service'=>'Produkt / betroffener Service',

/* './packages/improvement/popup_incident_approval_task.xml' */

'tt_assets_per_incident'=>'<b>Werte bezogen auf ein Ereignis</b>',

/* './packages/improvement/popup_incident_disposal.xml' */

'tt_immediate_disposal'=>'Sofortige Beseitigung',

/* './packages/improvement/popup_incident_edit.xml' */

'lb_accounts_plan_cl'=>'Buchungs Plan:',
'lb_affected_product_service_cl'=>'Produkt / betroffener Service:',
'lb_date_bl_cl'=>'<b>Datum:</b>',
'lb_date_limit_bl_cl'=>'<b>Erwartete Fertigstellung:</b>',
'lb_evidences_cl'=>'Belege:',
'lb_hour_bl_cl'=>'<b>Uhr:</b>',
'lb_immediate_disposal_cl'=>'Sofortige Beseitigung:',
'lb_loss_type_bl_cl'=>'<b>Verlustart</b>',
'lb_occurrences_bl_cl'=>'<b>Vorgänge:</b>',
'lb_occurrences_cl'=>'Auftritt:',
'lb_solution_cl'=>'Lösung:',
'si_direct'=>'Direkt',
'si_indirect'=>'Indirekt',
'st_collect_evidence'=>'Gesammelte Belege?',
'st_incident_date_finish'=>'Erwartete Fertigstellung:',
'st_incident_date_finish_bl'=>'<b>Erwartete Fertigstellung:</b>',
'st_no'=>'Nein',
'st_yes'=>'Ja',
'tt_incident_edit'=>'Vorgang schreiben',
'vb_financial_impact'=>'Finanzielle Auswirkungen',
'vb_observations'=>'Beobachtungen',
'wn_future_date_not_allowed'=>'Ein zukünftiges Datum / Uhrzeit ist nicht erlaubt.',
'wn_prevision_finish'=>'Das erwartete Fertigstellungsdatum muss gleich oder später als heute sein.',

/* './packages/improvement/popup_incident_evidence.xml' */

'tt_evidence'=>'Beleg',

/* './packages/improvement/popup_incident_process_association_edit.php' */

'st_save_suggested_incident_processes'=>'Möchten Sie die vorgeschlagenen Prozesse zum Vorgang <b>%context_name% </b> relativieren?',
'tt_save_suggested_incident_processes'=>'Relativiere vorgeschlagene Prozesse',

/* './packages/improvement/popup_incident_process_association_edit.xml' */

'cb_search_only_suggested_process'=>'Suche nur bestimmte Prozesse',
'tt_incident_process_association'=>'Vorgang zur Ereignisverbindung',

/* './packages/improvement/popup_incident_risk_asset_association_edit.xml' */

'lb_assets_cl'=>'Werte:',
'lb_risks_cl'=>'Risiko:',
'st_risk_asset_association_message'=>'"Beim auswählen von Aktiva',
'tt_risk_asset_association'=>'Verbindung Risiko zu Werten',

/* './packages/improvement/popup_incident_risk_association_edit.php' */

'em_incident_risk_parametrization'=>'Risiko Erwartung angeschlossen zum Vorgang',
'em_incident_risk_parametrization_footer'=>'Für weitere Information',
'mx_non_parameterized_risk'=>'nicht-erwartetes Risiko',
'to_dependencies_bl_cl'=>'<b>Abhängigkeiten:</b> <br><br>',
'to_dependents_bl_cl'=>'<b>Abhängig:</b> <br><br>',

/* './packages/improvement/popup_incident_risk_association_edit.xml' */

'tt_associated_controls_and_assets'=>'<b>Relationskontrollen und Werte</b>',
'tt_incident_risk_association'=>'Vorgangs-Risiko Verbindung',
'tt_risks_refferring_controls_and_assets'=>'<b>Risikobezugskontrolle und Werte</b>',
'tt_risks_related_selected'=>'<b>Relativiert / Bestimmte Risiken</b>',
'vb_insert_risk'=>'Risiko einfügen',
'vb_risk_incident_parametrization'=>'Gefahrenrisiko Einschätzung',
'vb_search_assets'=>'Suche Aktiva',
'vb_search_controls'=>'Suche Kontrolle',
'wn_parameterize_incident_risk'=>'Sie müssen das Gefahrenrisiko einschätzen bevor Sie speichern können.',

/* './packages/improvement/popup_incident_risk_association_options.xml' */

'lb_associate_risk_to_assets'=>'Möchten Sie gegenwärtige Gefahren relativieren zu ausgewählten Werten? Sie können auswählen welche Risiken in Relation zu welchen Werten stehen.',
'lb_send_alert_to_other_users'=>'Möchten Sie',
'tt_incident_risk_association_options'=>'Auswahl der Risiken und Gefahrmöglichkeiten',
'vb_continue'=>'weiter',

/* './packages/improvement/popup_incident_risk_parametrization.xml' */

'tt_incident_risk_association_parametrization'=>'Schätzung der Ereignis - Gefahren Verbindung',

/* './packages/improvement/popup_incident_search.xml' */

'tt_incident_search'=>'Vorgangssuche',
'vb_relation'=>'Zuordnen',
'wn_no_incident_selected'=>'Kein Vorgang wurde ausgewählt.',

/* './packages/improvement/popup_incident_solution.xml' */

'tt_solution'=>'Lösung',

/* './packages/improvement/popup_incident_state_forward.php' */

'st_incident_finish'=>'Möchten Sie den Vorgang fertigstellen?',
'st_incident_send_to_app_disposal'=>'Möchten Sie den Vorgang zur sofortigen Beseitigungsbewilligung senden?',
'st_incident_send_to_app_solution'=>'Möchten Sie den Vorgang zur Lösungsbewilligung senden?',
'st_incident_send_to_responsible'=>'Möchten Sie den Vorgang zum Verantwortlichen senden?',
'tt_incident_finish'=>'Fertigstellung Vorgang',
'tt_incident_send_to_app_disposal'=>'Gesandt zu sofortigen Beseitigungsbewilligung',
'tt_incident_send_to_app_solution'=>'Senden zu Lösungsbewilligung',
'tt_incident_send_to_responsible'=>'Sende zu Verantwortlichem',

/* './packages/improvement/popup_incident_task_view.php' */

'lb_incident_immediate_disposal_cl'=>'Sofortige Beseitigung:',
'lb_incident_solution_cl'=>'Lösung:',

/* './packages/improvement/popup_incident_task_view.xml' */

'lb_date_limit_cl'=>'Limit Datum:',

/* './packages/improvement/popup_new_risk_parameter_approval_task.xml' */


/* './packages/improvement/popup_non_conformity_approval_task.xml' */

'lb_processes_cl'=>'Prozesse:',

/* './packages/improvement/popup_non_conformity_associate.xml' */

'st_name'=>'Name:',
'tt_current_non_conformitys_bl'=>'<b>Aktuelle Nichtübereinstimmungen</b>',
'tt_non_conformity_association'=>'Nichtübereinstimmungs-Verbindung',

/* './packages/improvement/popup_non_conformity_create.xml' */

'lb_date_sent_cl'=>'Datum von Emission:',
'lb_sender_cl'=>'Sender:',
'tt_non_conformity_record'=>'Nichtübereinstimmungsbericht',

/* './packages/improvement/popup_non_conformity_data_approval_task.php' */

'st_denied_permission_to_execute_task'=>'Sie haben keine Erlaubnis zum Ausführen der Aufgabe.',

/* './packages/improvement/popup_non_conformity_data_approval_task.xml' */

'lb_ap_responsible_cl'=>'API Resp.:',
'lb_cause_cl'=>'Ursache:',
'lb_inquirer_cl'=>'Anfragender:',
'lb_justificative_cl'=>'Grund:',
'lb_non_conformity_cl'=>'Nichtübereinstimmung:',
'lb_task_cl'=>'Aufgabe:',
'tt_task_visualization'=>'Aufgabendarstellung',

/* './packages/improvement/popup_non_conformity_edit.xml' */

'lb_action_plans_cl'=>'Handlungspläne:',
'lb_nc_capability_bl_cl'=>'Potentielles:',
'lb_nc_cause_cl'=>'Ursache:',
'lb_nc_classification_cl'=>'Klassifizierung:',
'lb_nc_datesent_cl'=>'Datum der Emission:',
'lb_nc_description_cl'=>'Beschreibung:',
'lb_nc_name_bl_cl'=>'<b>Name:</b>',
'lb_nc_responsible_id_cl'=>'Verantwortlich:',
'lb_nc_sender_id_cl'=>'Sender:',
'lb_potential_cl'=>'Potentiell:',
'lb_processes_bl_cl'=>'<b>Prozesse:</b>',
'si_external_audit'=>'Externe Bilanz',
'si_internal_audit'=>'Interne Bilanz',
'si_no'=>'Nein',
'si_security_control'=>'Sicherheitskontrolle',
'si_yes'=>'Ja',
'tt_nc_edit'=>'Nichtübereinstimmung schreiben',

/* './packages/improvement/popup_non_conformity_revision_task.xml' */

'lb_action_plan_cl'=>'Aktionsplan:',
'lb_ap_efficient_bl'=>'<b>War der Aktionsplan effizient?</b>',
'rb_no'=>'Nein',
'rb_yes'=>'Ja',
'tt_non_conformity_efficiency_revision'=>'Nichtübereinstimmung Effizienz Revision',

/* './packages/improvement/popup_non_conformity_state_forward.php' */

'st_non_conformity_send_to_ap_responsible'=>'Möchten Sie die Nichtübereinstimmung versenden zwecks Akzeptanz des Aktionsplans?',
'st_non_conformity_send_to_approve'=>'Möchten Sie die Nichtübereinstimmung versenden zwecks Aktionsplan Fertigstellung?',
'st_non_conformity_send_to_responsible'=>'Möchten Sie die Nichtübereinstimmung an den Verantwortlichen senden?',
'tt_non_conformity_send_to_ap_responsible'=>'Sende an Aktionsplan Akzeptanz',
'tt_non_conformity_send_to_approve'=>'Sende zur Aktionsplans Fertigstellung',
'tt_non_conformity_send_to_responsible'=>'Sende an Verantwortlich',

/* './packages/improvement/popup_non_conformity_state_forward.xml' */

'vb_no'=>'Nein',
'vb_yes'=>'Ja',

/* './packages/improvement/popup_occurrence_edit.xml' */

'lb_description_bl_cl'=>'<b>Beschreibung:</b>',
'st_date_bl'=>'<b>Datum:</b>',
'st_date_warning'=>'Ein zukünftiges Datum / Uhrzeit ist nicht erlaubt.',
'st_hour_bl'=>'<b>Uhr:</b>',
'tt_register_occurrence'=>'Register Auftritt',

/* './packages/improvement/popup_occurrences_search.xml' */

'tt_current_occurrences'=>'<b>Aktuelle Auftritte</b>',
'tt_occurrences_search'=>'Auftritts Suche',

/* './packages/improvement/popup_probability_calculation_config.xml' */

'lb_incidents_in_period_cl'=>'Vorgänge zwischen %initial_date% und %final_date%:',
'lb_last_check_cl'=>'Letzte Überprüfung:',
'lb_periodicity_bl_cl'=>'<b>Zeitraum:</b>',
'lb_starting_at'=>'Beginn von',
'si_days'=>'Tage',
'si_months'=>'Monate',
'si_weeks'=>'Woche(n)',
'st_total_incidents_found'=>'Gesamtzahl der gefundenen Vorgänge:',
'tt_incidents_related_to_risk_in_period'=>'<b>Vorgänge in Relation zum Risiko in einer Periode</b>',
'tt_probability_calculation_config'=>'Konfiguration der automatischen Wahrscheinlichkeitsberechnung',
'tt_probability_level_vs_incident_count'=>'<b>Wahrscheinlichkeits-Niveau<br/><br/><b>X</b><br/><br/><b>Anzahl der Ereignisse</b>',
'vb_update'=>'Update',
'wn_probability_config_not_filled'=>'Nicht oder nicht richtig ausgefüllte Abstände',

/* './packages/improvement/popup_process_search.xml' */

'tt_current_processes'=>'<b>Aktuelle Prozesse<b>',
'tt_process_search'=>'Prozess Suche',

/* './packages/improvement/popup_request_compare_impact_risks.xml' */


/* './packages/improvement/popup_risk_create.xml' */

'lb_impact_money_cl'=>'Finanzieller Vorgang:',
'si_select_one_below'=>'Wählen Sie einer der unteren Einheiten',
'st_risk_impact'=>'Vorgang:',
'tt_insert_risk'=>'Einfüge Risiko',

/* './packages/improvement/report/clock.php' */

'st_clock_wait'=>'<b>Attention:</b> Dieser Bericht wird in wenigen Minuten generiert sein.<br>Bitte warten.',

/* './packages/improvement/report/popup_report_action_plan_by_process.php' */

'tt_action_plan_by_process'=>'Aktionsplan von Prozess',

/* './packages/improvement/report/popup_report_action_plan_by_user.php' */

'tt_action_plan_by_user'=>'Aktionsplan von Nutzer',

/* './packages/improvement/report/popup_report_ap_without_doc.php' */

'rs_ap_without_docs'=>'Aktionspl?ne ohne Dokumente',

/* './packages/improvement/report/popup_report_ap_without_doc.xml' */

'rs_ap'=>'Aktionsplan',

/* './packages/improvement/report/popup_report_control_nonconformity_by_process.php' */

'mx_control_nonconformity_by_process'=>'Nichtübereinstimmung von Fall von Prozess',

/* './packages/improvement/report/popup_report_disciplinary_process.php' */

'mx_disciplinary_process'=>'Disciplinarsche Prozesse',

/* './packages/improvement/report/popup_report_disciplinary_process.xml' */

'rs_dp_name'=>'Name',

/* './packages/improvement/report/popup_report_elements_affected_by_incident.php' */

'mx_elements_affected_by_incident'=>'Einheiten beeinflusst von Vorgängen',

/* './packages/improvement/report/popup_report_elements_affected_by_incident.xml' */

'rs_incident_bl_cl'=>'<b>Vorgang:</b>',

/* './packages/improvement/report/popup_report_financial_impact_by_incident.php' */

'mx_financial_impact_by_incident'=>'Finanzielle Beeinflussung von Vorgang',

/* './packages/improvement/report/popup_report_incident_accompaniment.php' */

'mx_incident_accompaniment'=>'Vorgangs Anschluss',

/* './packages/improvement/report/popup_report_incident_by_responsible.php' */

'mx_evidence_collection'=>'Beweis-Ansammlung',

/* './packages/improvement/report/popup_report_incident_control_revision.php' */

'mx_incident_control_revision'=>'Fall Revision verursacht von Vorgängen',

/* './packages/improvement/report/popup_report_incident_control_revision.xml' */

'rs_date_todo'=>'geschätztes Datum',
'rs_er'=>'Betreff',

/* './packages/improvement/report/popup_report_incident_without_occurrence.php' */

'rs_incidents_without_occurrence'=>'Vorgänge ohne Auftritte',

/* './packages/improvement/report/popup_report_incident_without_risk.php' */

'rs_incidentes_without_risks'=>'Vorgänge ohne Risiken',

/* './packages/improvement/report/popup_report_incident_without_risk.xml' */

'rs_incident'=>'Vorgang',

/* './packages/improvement/report/popup_report_incidents.php' */

'mx_incidents'=>'Vorgänge',

/* './packages/improvement/report/popup_report_incidents.xml' */

'rs_incident_name'=>'Name',

/* './packages/improvement/report/popup_report_nc_without_ap.php' */

'rs_nc_without_ap'=>'Nichtübereinstimmungen ohne Aktionspläne',
'st_nc_manager_with_parentheses'=>'(Nichtübereinstimmungs Manager)',

/* './packages/improvement/report/popup_report_non_conformities.php' */

'mx_non_conformities'=>'Nichtübereinstimmungen',

/* './packages/improvement/report/popup_report_non_conformities.xml' */

'rs_nc_name'=>'Name',

/* './packages/improvement/report/popup_report_nonconformity_accompaniment.php' */

'mx_nonconformity_accompaniment'=>'Nichtübereinstimmungs Anschluss',
'rs_not_defined'=>'nicht definiert',

/* './packages/improvement/report/popup_report_nonconformity_accompaniment.xml' */

'rs_action_plan'=>'Aktionsplan',
'rs_conclusion'=>'Fertigstellung',
'rs_non_conformity'=>'Nichtübereinstimmung:',

/* './packages/improvement/report/popup_report_nonconformity_by_process.php' */

'mx_nonconformity_by_process'=>'Nichtübereinstimmung von Prozess',

/* './packages/improvement/report/popup_report_nonconformity_by_process.xml' */

'rs_nonconformity'=>'Nichtübereinstimmung',

/* './packages/improvement/report/popup_report_occurrence_accompaniment.php' */

'mx_occurrence_accompaniment'=>'Auftritts Anschluss',

/* './packages/improvement/report/popup_report_occurrence_accompaniment.xml' */

'rs_creator_cl'=>'Ersteller:',

/* './packages/improvement/report/popup_report_occurrence_by_incident.php' */

'mx_occurrence_by_incident'=>'Auftritte von Vorgängen',

/* './packages/improvement/report/popup_report_occurrence_by_incident.xml' */

'report_filter_userid_cl'=>'Verantwortlich:',
'rs_occurrence'=>'Auftritt',
'rs_status_cl'=>'Status:',

/* './packages/improvement/report/popup_report_risk_auto_probability_value.php' */

'mx_risk_auto_probability_value'=>'Automatische Risiko Wahrscheinlichkeitskalkulation',

/* './packages/improvement/report/popup_report_risk_auto_probability_value.xml' */

'rs_incident_amount'=>'Amt. Von Vorgängen',
'rs_last_verification'=>'Letzte Verifizierung',
'rs_periodicity'=>'Periodenzahl',

/* './packages/improvement/report/popup_report_user_by_incident.php' */

'mx_user_by_incident'=>'Disziplinarscher Prozess',

/* './packages/improvement/report/popup_report_user_by_incident.xml' */

'rs_description'=>'Beschreibung',
'rs_incident_cl'=>'Vorgang:',
'rs_user'=>'Nutzer',

/* './packages/improvement/report/report_template.xml' */

'rs_fim'=>'ENDE VON BERICHT',

/* './packages/improvement/tab_continual_improvement.xml' */

'ti_action_plan'=>'Arbeitsplan',
'ti_disciplinary_process'=>'Disziplinarischer Prozess',
'ti_incident'=>'Vorgang',
'ti_non_conformity'=>'Nicht übereinstimmend',
'ti_occurrences'=>'Vorfälle',
'ti_report'=>'Berichte',

/* './packages/libraries/nav_asset_category_threats.xml' */


/* './packages/libraries/nav_best_practices.php' */

'st_remove_best_practice_confirm'=>'Sind Sie sicher Sie möchten die  Gute Beispiele <b>%best_practice_name%</b> verschieben?',
'st_remove_section_confirm'=>'Sind Sie sicher Sie möchten the section <b>%section_name%</b> verschieben?',
'tt_remove_best_practice'=>'Verschieben Gute Beispiele',
'tt_remove_section'=>'Verschieben Sektion',

/* './packages/libraries/nav_best_practices.xml' */

'mi_associate_events'=>'Angeschlossener Fall',
'mi_open_section'=>'Offene Sektion',
'mi_templates'=>'Dokumentenentwurf',
'tt_best_practices_bl'=>'<b>Gute Beispiele</b>',
'vb_insert_best_practice'=>'Einfüge Gute Beispiele',
'vb_insert_section'=>'Eintrag Bereich',

/* './packages/libraries/nav_department.xml' */


/* './packages/libraries/nav_document_template.php' */

'st_remove_document_template_confirm'=>'Sind Sie sicher Sie möchten den Dokumentenentwurf<b>%context_name%</b> verschieben?',
'tt_remove_document_template'=>'Verschieben Dokumentenentwurf',

/* './packages/libraries/nav_document_template.xml' */

'si_best_practice'=>'Gute Beispiele',
'si_denied'=>'Abgelehnt',
'si_doc_template_control'=>'Kontrolle',
'si_pending'=>'Schwebend',
'tt_documents_templates_bl'=>'<b>Dokumentenentwürfe</b>',

/* './packages/libraries/nav_events.php' */

'st_remove_category_cascade_message'=>'Ausserdem möchten Sie Aktiva relativiert zur Kategorie löschen',
'st_remove_category_confirm'=>'Sind Sie sicher Sie möchten die Kategorie  <b>%Category_name%</b>  verschieben?',
'st_remove_event_confirm'=>'Sind Sie sicher Sie möchten den Fall  <b>%event_name%</b> verschieben ?',
'tt_remove_category'=>'Verschieben Kategorie',
'tt_remove_event'=>'verschiebe Fall',

/* './packages/libraries/nav_events.xml' */

'mi_associate_best_practices'=>'Bester Beispielteilnehmer',
'mi_open_event_category'=>'Öffne Kategorie',
'tt_events_library_bl'=>'<b>Fall Bibliotheken</b>',
'vb_associate_event'=>'Ereignis einbinden',
'vb_insert_event'=>'Ereignis eintragen',

/* './packages/libraries/nav_impact_library.xml' */


/* './packages/libraries/nav_import_export.php' */

'wn_error_invalid_file'=>'Error: abgelaufene Kartei',
'wn_extension_not_allowed'=>'Erweiterung nicht erlaubt.',
'wn_maximum_size_exceeded'=>'Maximum Grösse überschritten.',

/* './packages/libraries/nav_import_export.xml' */

'lb_client_cl'=>'Kunde:',
'lb_export_library_cl'=>'Export Lexikon:',
'lb_import_library_cl'=>'Import Lexikon:',
'lb_license_cl'=>'Lizenz:',
'si_best_practices'=>'Gute Beispiele',
'si_events_library'=>'Ereignis Lexikon',
'si_standards'=>'Standards',
'si_templates'=>'Dokumentenentwürfe',
'st_export_list_bl'=>'<b>Exportliste</b>',
'st_exporting_options_bl'=>'<b>Exportiere Optionen</b>',
'st_import_list_bl'=>'<b>Importliste</b>',
'st_or_bl'=>'<b>oder</b>',
'to_export'=>'<b>Export:</b><br/><br/>Exportiere ausgewählte Einträge.',
'to_export_list'=>'<b>List:</b><br/><br/>Liste der Bibliothekeinträge',
'to_import'=>'<b>Import</b><br/><br/>Import ausgewählter Einheiten.',
'to_import_list'=>'<b>List:</b><br/><br/>Liste der Bibliothekeinträge',
'tr_best_practices'=>'Gute Beispiele',
'tr_events_library'=>'Ereignis Lexikon',
'tr_no_items_to_import'=>'Keine importierten Einheiten vorhanden',
'tr_standards'=>'Standards',
'tr_template'=>'Dokumentenentwürfe',
'tr_templates'=>'Dokumentenentwürfe',
'vb_export'=>'Export',
'vb_import'=>'Import',
'vb_list'=>'Liste',
'vb_list_import'=>'Liste',
'wn_import_required_file'=>'Sie müssen zuerst einen Eintrag auswählen bevor Sie auf die Liste klicken.',

/* './packages/libraries/nav_incident_category.php' */

'st_category_remove'=>'Sind Sie sicher Sie möchten the Kategorie <b>%category_name%</b> verschieben?',
'st_category_remove_error'=>'Es ist nicht möglich die Kategorie <b>%category_name%</b>  zu verschieben',
'st_solution_remove'=>'Sind Sie sicher Sie möchten die Lösung verschieben?',
'tt_category_remove'=>'Verschieben Kategorie',
'tt_category_remove_error'=>'Fehler beim Verschieben von Kategorie',
'tt_solution_remove'=>'Verschiebe Löschung',

/* './packages/libraries/nav_incident_category.xml' */

'gc_keywords'=>'Schlüsselwort',
'gc_problem'=>'Problem',
'gc_solution'=>'Lösung',
'gc_solutions_bl'=>'<b>Lösung</b>',
'mi_delete'=>'Verschiebe',
'mi_open_category'=>'?Öffne Kategorien',
'tt_category_library'=>'<b>Kategorien Lexikon</b>',
'vb_solution_insert'=>'Eintrag Lösung',

/* './packages/libraries/nav_place_category_threats.xml' */


/* './packages/libraries/nav_plan_ranges.xml' */


/* './packages/libraries/nav_plan_types.xml' */


/* './packages/libraries/nav_process_category_threats.xml' */

'vb_insert_category'=>'Kategorie eintragen',

/* './packages/libraries/nav_standards.php' */

'st_remove_standard_confirm'=>'Sind Sie sicher Sie möchten den Standard <b>%standard_name%</b>  verschieben?',
'tt_remove_standard'=>'Verschieben Standard',

/* './packages/libraries/nav_standards.xml' */

'gc_creator'=>'Ersteller',
'tt_standards_bl'=>'<b>Standards</b>',

/* './packages/libraries/popup_best_practice_add.xml' */

'si_all_sections'=>'Alle Bereiche',
'tt_current_best_practices_bl'=>'<b>Aktuelle beste Beispiele</b>',

/* './packages/libraries/popup_best_practice_edit.php' */

'tt_best_practice_adding'=>'Gute Beispiele hinzufügen',
'tt_best_practice_editing'=>'Gute Beispiele bearbeiten',

/* './packages/libraries/popup_best_practice_edit.xml' */

'lb_control_objective_cl'=>'Zielsteuerung:',
'lb_control_type_cl'=>'Art der Kontrolle:',
'lb_implementation_guidelines_cl'=>'Implementierungsrichtlinie:',
'lb_metric_cl'=>'Metrisch:',
'lb_name_good_practices_bl_cl'=>'<b>Name/ Beispiele:</b>',
'lb_standards_bl_cl'=>'<b>Standards:</b>',
'si_administrative'=>'Administrative',
'si_awareness'=>'Erkenntnis',
'si_coercion'=>'Zwang',
'si_correction'=>'Berichtigung',
'si_detection'=>'Detektion',
'si_limitation'=>'Eingrenzung',
'si_monitoring'=>'Überwachung',
'si_physical'=>'Physikalisch',
'si_prevention'=>'Vorbeugung',
'si_recovery'=>'Erneuerung',
'si_technical'=>'Technische',
'tt_best_practice'=>'Gute Beispiele',

/* './packages/libraries/popup_category_edit.php' */

'tt_category_adding'=>'Kategorie hinzufügen',
'tt_category_editing'=>'Kategorie bearbeiten',

/* './packages/libraries/popup_document_template_edit.php' */

'tt_document_template_adding'=>'Dokumentenentwurf hinzufügen',
'tt_document_template_editing'=>'Dokumentenentwurf bearbeiten',

/* './packages/libraries/popup_document_template_edit.xml' */

'lb_best_practice_cl'=>'Gute Beispiele:',
'lb_type_bl_cl'=>'<b>Typ:</b>',
'si_action_plan'=>'Ablaufplan',
'si_area'=>'Bereich',
'si_asset'=>'Aktiva',
'si_control'=>'Kontrolle',
'si_management'=>'Management',
'si_policy'=>'Richtlinie',
'si_process'=>'Prozess',
'si_scope'=>'Begrenzung',
'si_select_type'=>'Wähle Typ',

/* './packages/libraries/popup_event_add.xml' */

'tt_current_events_bl'=>'<b>Aktuelles Ereignis</b>',

/* './packages/libraries/popup_event_edit.php' */

'tt_event_adding'=>'Fall hinzufügen',
'tt_event_editing'=>'Fall bearbeiten',

/* './packages/libraries/popup_event_edit.xml' */

'cb_suggested_event'=>'Geplantes Ereignis',
'lb_observation_cl'=>'Beobachtung:',
'st_event_impact_cl'=>'Auswirkung:',

/* './packages/libraries/popup_imple_guide.xml' */

'tt_implementation_guidelines'=>'Einfügungsrichtlinien',

/* './packages/libraries/popup_incident_category_edit.php' */

'tt_category_addition'=>'Kategorie hinzufügen',

/* './packages/libraries/popup_incident_category_edit.xml' */

'tt_category_edit'=>'Kategorienbeschreibung',

/* './packages/libraries/popup_incident_solution_edit.xml' */

'lb_incident_category_cl'=>'Kategorie:',
'lb_problem_bl_cl'=>'<b>Problem:</b>',
'lb_solution_bl_cl'=>'<b>Lösung:</b>',
'tt_solution_edit'=>'Kategoriebeschreibung',

/* './packages/libraries/popup_metric.xml' */

'tt_metric'=>'Metrisch',

/* './packages/libraries/popup_process_category_threat_edit.xml' */


/* './packages/libraries/popup_section_edit.php' */

'tt_section_adding'=>'Sektion hinzufügen',
'tt_section_editing'=>'Sektion bearbeiten',

/* './packages/libraries/popup_standard_add.xml' */

'tt_current_standards_bl'=>'<b>Aktuelle Standards</b>',
'tt_standards_search'=>'Standardssuche',

/* './packages/libraries/popup_standard_edit.php' */

'tt_standard_adding'=>'Standard hinzufügen',
'tt_standard_editing'=>'Standard bearbeiten',

/* './packages/libraries/popup_standard_edit.xml' */

'lb_application_cl'=>'Antrag:',
'lb_objective_cl'=>'Planziel:',

/* './packages/libraries/tab_libraries.xml' */

'ti_best_practices'=>'Gute Beispiele',
'ti_category_library'=>'Ereignis-Kategorie',
'ti_events_library'=>'Ereignis Lexikon',
'ti_export_and_import'=>'Export & Import',
'ti_standards'=>'Standards',
'ti_templates'=>'Dokumentenentwürfe',

/* './packages/policy/nav_all.xml' */

'lb_status'=>'Status',
'tt_all_documents_bl'=>'<b>Alle Dokumenten</b>',

/* './packages/policy/nav_approving.xml' */

'tt_approving_documents_bl'=>'<b>Dokumente In Bewilligung</b>',

/* './packages/policy/nav_developing.xml' */

'tt_developing_document_bl'=>'<b>Dokumente In Abwicklung</b></b>',
'vb_insert_document'=>'Einfüge Dokument',

/* './packages/policy/nav_obsolete.xml' */

'gc_file_name'=>'Kartei / Link',
'tt_obsolete_documents_bl'=>'<b>Überholte Version der Dokumente</b>',

/* './packages/policy/nav_publish.xml' */

'tt_published_documents_bl'=>'<b>Veröffentlichte Dokumente</b>',

/* './packages/policy/nav_register.php' */

'st_remove_register_confirm'=>'Sind Sie sicher Sie möchten den Bericht  <b>%register_name%</b> verschieben?',
'tt_remove_register'=>'Verschiebe Bericht',

/* './packages/policy/nav_register.xml' */

'gc_retention_time'=>'AUFBEWAHRUNGSZEIT',
'gc_storage_place'=>'Speicherplatz',
'gc_storage_type'=>'Speicherart',
'mi_read'=>'Lesen',
'mi_read_document'=>'Lese Dokument',
'mi_readers'=>'Leser',
'vb_insert_register'=>'Ablagebeleg',

/* './packages/policy/nav_report.php' */

'mx_documents_with_high_revision_frequency'=>'Dokumente mit hoher Erneuerungsfrequenz',
'mx_documents_without_registers'=>'Dokumente ohne Berichte',
'mx_pendant_task_pm'=>'Schwebende Aufgaben',
'mx_report_documents_pendent_reads'=>'Dokumente mit offenen Lesungen',
'mx_report_element_classification'=>'Klassifizierungsbericht',
'mx_users_with_pendant_read'=>'Nutzer mit noch offenen Lesungen',
'si_access_to_documents'=>'Zutritt zu Dokumenten Bilanz',
'si_accessed_documents'=>'Nutzer Zutrittsbilanz',
'si_approvers'=>'Dokumente und ihre Genehmigungen',
'si_documents_by_component'=>'Dokumente mit ERMS Komponente',
'si_documents_by_state'=>'Dokumente',
'si_documents_dates'=>'Erstellung und Revision Datum von Dokumenten',
'si_documents_per_area'=>'Dokumente nach Bereich',
'si_items_with_without_documents'=>'Komponente ohne Dokumente',
'si_most_accessed_documents'=>'Dokumente mit den meisten Zutritten',
'si_most_revised_documents'=>'Meist überarbeitete Dokumente',
'si_not_accessed_documents'=>'Dokumenten ohne Zutritt',
'si_pendant_approvals'=>'Schwebende Bewilligung',
'si_readers'=>'Dokumente und seine Leser',
'si_registers_per_document'=>'Bericht von Dokument',
'si_users_per_process'=>'Nutzer angeschlossen an Prozesse',

/* './packages/policy/nav_report.xml' */

'cb_expand_documents'=>'Vollansicht der Dokumente',
'cb_expand_readers'=>'Vollansicht Leser',
'cb_organize_by_area'=>'Ordne nach Bereich',
'cb_show_all_access'=>'Zeige alle Zutritte',
'cb_show_register_documents'=>'Zeige Berichtsdokumente',
'lb_creation_date_cl'=>'Erstellungsdatum:',
'lb_documents_bl_cl'=>'<b>Dokumenten:</b>',
'lb_final_date_cl'=>'Fertigstellungsdatum:',
'lb_initial_date_cl'=>'Ausgangsdatum:',
'lb_last_revision_date_cl'=>'Letztes Erneuerungsdatum:',
'lb_show_only_first_cl'=>'Zeige nur:',
'lb_until'=>'bis',
'lb_users_bl_cl'=>'<b>Nutzer:</b>',
'si_abnormalities_reports'=>'Abweichungs-Bericht',
'si_access_control_reports'=>'Zugangskontrollbericht',
'si_all_status'=>'Alle',
'si_approved'=>'Veröffentlicht',
'si_developing'=>'In Entwicklung',
'si_documents_by_type'=>'Dokumente nach Typ',
'si_general_reports'=>'Generalbericht',
'si_management_reports'=>'Management Berichte',
'si_obsolete'=>'Veraltet',
'si_pendant'=>'In Zulassung',
'si_register_reports'=>'Ablagenbericht',
'si_registers_by_type'=>'Berichte nach Typ',
'si_revision'=>'In Erneuerung',
'st_asset'=>'Aktiva',
'st_components'=>'<b>Komponenten:</b>',
'st_control'=>'Kontrolle',
'st_element_type_warning'=>'Bitte',
'st_process'=>'Prozess',
'st_status_bl'=>'<b>Status:</b>',
'st_type_cl'=>'Typ:',
'wn_component_filter_warning'=>'Bitte wählen Sie mindestens eine Teilart aus.',
'wn_no_filter_available'=>'Kein Filter verfügbar.',
'wn_select_a_document'=>'Bitte',

/* './packages/policy/nav_revision.xml' */

'mi_edit_author'=>'Schreibe Autor',
'tt_revision_documents_bl'=>'<b>Dokumenten in Revision</b>',

/* './packages/policy/nav_to_be_published.php' */

'st_document_confirm_remove'=>'Sind Sie sicher Sie möchten das Dokument <b>%document_name%</b> verschieben?',
'tt_remove_document'=>'Verschieben Dokument',

/* './packages/policy/nav_to_be_published.xml' */

'gc_elements'=>'Einheiten',
'gc_file_version'=>'Version',
'gc_sub_doc'=>'Nebendokument',
'lb_classification'=>'Klassifizierung',
'lb_type'=>'Typ',
'mi_approve'=>'Genehmigung',
'mi_associate_components'=>'Angeschlossene Komponenten',
'mi_comments'=>'Kommentare',
'mi_copy_readers'=>'Leser-Kopie',
'mi_details'=>'Details',
'mi_document_templates'=>'Dokumentenentwurf',
'mi_edit_approvers'=>'Schreibe Bewilligungen',
'mi_edit_readers'=>'Schreibe Leser',
'mi_previous_versions'=>'Vorige Versionen',
'mi_publish'=>'Veröffentliche',
'mi_references'=>'Hinweise',
'mi_registers'=>'Berichte',
'mi_send_to_approval'=>'Sende für Bewilligung',
'mi_send_to_revision'=>'Sende für Revision',
'mi_sub_documents'=>'Nebenbelege',
'mi_view_approvers'=>'Zeige Bewilligungen',
'mi_view_components'=>'Zeige Komponenten',
'mi_view_readers'=>'Zeige Leser',
'tt_documents_to_be_published_bl'=>'<b>Dokumenten zu veröffentlichen</b>',

/* './packages/policy/popup_approvers_edit.xml' */

'tt_approvers_editing'=>'Bewilligungen schreiben',
'tt_curret_approvers_bl'=>'<b>Aktuelle Bewilligungen</b>',

/* './packages/policy/popup_approvers_view.xml' */

'tt_approvers_visualization'=>'Bewilliger Visualisierung',

/* './packages/policy/popup_associate_users.xml' */

'gc_login'=>'Login',
'lb_login_cl'=>'Login:',
'tt_current_users_bl'=>'<b>Aktuelle Nutzer</b>',
'tt_users_search'=>'Nutzer Suche',

/* './packages/policy/popup_create_document_confirm.xml' */

'rb_creation_document_question'=>'Möchten Sie ein Dokument im System erstellen und es als Schablone für die Aufzeichnung benutzen <b>%register_name%</b>?',
'rb_document_do_nothing'=>'Nicht erstellte / angeschlossene Dokumente an Bericht <b>%register_name%</b>?',
'rb_document_relation_question'=>'Möchten Sie ein Dokument im System erstellen und es als Schablone für die Aufzeichnung benutzen <b>%register_name%</b>?',
'st_what_action_you_wish_to_do'=>'Welche Aktion möchten Sie ausführen?',
'tt_document_creation_or_association'=>'Dokument Hinzufügen/Verbindung',

/* './packages/policy/popup_document_author_edit.xml' */

'tt_author_editing'=>'Autor schreiben',

/* './packages/policy/popup_document_context_associate.php' */

'st_permission_to_associate_item_document_denied'=>'"Sie haben keine Erlaubnis eine Einheit zu einem Dokument an ""Management"" oder ""Anderes"" Typ anzuschliessen',

/* './packages/policy/popup_document_context_associate.xml' */

'tt_current_items_bl'=>'<b>Aktuelle Einheiten</b>',
'tt_risk_management_items_association'=>'Risiken Management Einheiten Verbindung',

/* './packages/policy/popup_document_context_view.xml' */

'tt_associated_risk_management_items_visualization'=>'Angeschlossene Risiko Management Einheiten Visualisierung',

/* './packages/policy/popup_document_copy_readers.xml' */

'tt_documents_search'=>'Dokumenten Suche',
'vb_copy_readers'=>'Kopie Leser',

/* './packages/policy/popup_document_create.xml' */

'lb_document_name_bl_cl'=>'<b>Dokument Name:</b>',
'tt_new_document'=>'Neues Dokument',

/* './packages/policy/popup_document_edit.php' */

'si_no_type'=>'Kein Typ',
'st_creation_date'=>'Erstellungsdatum:',
'st_document_approval_confirm'=>'Möchten Sie das Dokument zur Bewilligung versenden?',
'st_download_file'=>'Download Kartei:',
'st_publish_document_confirm'=>'Möchten Sie das Dokument veröffentlichen?',
'st_publish_document_on_date_confirm'=>'Möchten Sie das Dokument  am %date% veröffentlichen?',
'st_size'=>'Grösse:',
'to_max_file_size'=>'Maximale Kartei Grösse: %max_file_size%',
'tt_document_adding'=>'Dokument hinzufügen',
'tt_document_editing'=>'Dokument schreiben',
'tt_publish'=>'Veröffentlichen',
'tt_send_for_approval'=>'Sende zur Bewilligung',

/* './packages/policy/popup_document_edit.xml' */

'lb_author_bl_cl'=>'<b>Autor:</b>',
'lb_deadline_bl_cl'=>'<b>Stichtag:</b><br/><br/>Stichtag für Dokumentenbewilligung.',
'lb_deadline_cl'=>'Stichtag:',
'lb_inform_in_advance_of'=>'Informieren Sie sich',
'lb_publication_date_bl_cl'=>'<b>Publizierungsdatum:</b>',
'lb_revision_cl'=>'Revision:',
'si_disable'=>'Sperrung',
'si_enable'=>'Freigabe',
'si_file'=>'Kartei',
'si_link'=>'Link',
'st_days'=>'Tage.',
'st_general_information'=>'General Information',
'st_management_information'=>'Management Information',
'st_test'=>'Test',
'to_doc_instance_manual_version'=>'<b>Anleitung Version:</b><br/>Die Anleitungsversion ist nur wirksam',
'to_document_inform_in_advance'=>'<b>Vorabinformation:</b><br/><br/>es ist die Anzahl von Tagen vor dem Stichtag oder Revision in welcher eine Alarm- oder Aufgabenwarnung generiert sein muss.',
'to_document_type_modification'=>'Zur Modifizierung des Dokumententyp',
'wn_max_file_size_exceeded'=>'Maximale Kartengrüsse überschritten.',
'wn_publication_date_after_today'=>'Das Publizierungsdatum kann heute sein oder später',
'wn_upload_error'=>'Fehlermeldung beim Senden der Kartei',

/* './packages/policy/popup_document_instances.php' */

'st_copy_previous_version_confirm'=>'Sind Sie sicher Sie möchten eine Kopie der vorigen Version?',
'tt_copy_previous_version'=>'Kopie vorige Version',

/* './packages/policy/popup_document_instances.xml' */

'gc_end_date'=>'Enddatum',
'gc_modification_comment'=>'Erneuerungs-Kommentar',
'gc_revision_justification'=>'Erneuerungsgrund',
'gc_start_date'=>'Start Datum',
'gc_version'=>'Version',
'tt_previous_versions'=>'Vorige Version',
'wn_no_version_selected'=>'Es ist keine Version ausgewählt.',

/* './packages/policy/popup_document_manage.xml' */

'st_approver_list_only_when_developing'=>'Die Genehmigungsliste kann nur dann modifiziert werden',
'st_items_association_not_management_or_others'=>'"Es ist nur dann möglich die Verbindung zu wechseln',
'st_items_association_only_when_developing'=>'Es ist nur dann möglich die Verbindungen zu modifizieren',
'st_readers_list_only_when_developing'=>'Die Leserliste kann nur dann geschrieben werden',
'st_revision_for_published_document_only'=>'Nur veröffentlichte Dokumente können zur Revision versandt werden',
'tt_associate_items_bl'=>'<b>Angeschlossene Einheiten</b>',
'tt_edit_approvers_bl'=>'<b>Schreibe Genehmigung</b>',
'tt_edit_readers_bl'=>'<b>Schreibe Leser</b>',
'tt_manage_document'=>'Manage Dokument',
'tt_send_for_revision_bl'=>'<b>Sende zur Revision</b>',
'tt_view_approvers_bl'=>'<b>Zeige Genehmigungen</b>',
'tt_view_items_bl'=>'<b>Zeige Einheiten</b>',
'tt_view_readers_bl'=>'<b>Zeige Leser</b>',
'vb_copy'=>'Kopie',
'vb_send'=>'Senden',

/* './packages/policy/popup_document_read.php' */

'st_remove_comment_confirm'=>'Sind Sie sicher Sie möchten zur Stellungnahme verschieben?',
'tt_approve_document'=>'Genehmigung Dokument',
'tt_remove_comment'=>'Verschieben Stellungnahme',

/* './packages/policy/popup_document_read.xml' */

'ti_comments'=>'Kommentare',
'ti_general'=>'General',
'ti_references'=>'Belege',
'ti_sub_documents'=>'Nebendokumente',
'tt_document_reading'=>'Dokument lesen',
'vb_approve'=>'Genehmigung',
'vb_deny'=>'Verweigern',

/* './packages/policy/popup_document_read_comments.xml' */

'gc_author'=>'Autor',
'gc_comment'=>'Stellungnahme',
'gc_date'=>'Datum',
'tt_coments'=>'Kommentare',
'tt_comments_bl'=>'<b>Kommentare</b>',

/* './packages/policy/popup_document_read_details.xml' */

'lb_date_cl'=>'Datum:',
'lb_keywords_cl'=>'Stichwort:',
'lb_size_cl'=>'Grösse:',
'lb_version_cl'=>'Version:',
'tt_details'=>'Details',

/* './packages/policy/popup_document_read_subdocuments.xml' */

'tt_sub_documents'=>'Nebendokument',

/* './packages/policy/popup_document_reference.php' */

'st_remove_reference_confirm'=>'Sie Sie sicher die Referenz <b>%doc_reference_name%</b> zu verschieben?',
'tt_remove_reference'=>'Verschiebe Referenz',

/* './packages/policy/popup_document_reference.xml' */

'gc_creation_date'=>'Erstellungsdatum',
'gc_link'=>'Link',
'lb_link_bl_cl'=>'<b>Link:</b>',
'st_http'=>'http://',
'tt_references'=>'Hinweise',
'tt_references_bl'=>'<b>Hinweise</b>',

/* './packages/policy/popup_document_register.xml' */

'tt_associated_registers_bl'=>'<b>Angeschlossene Berichte</b>',
'tt_registers'=>'Berichte',
'vb_disassociate'=>'Absondern',

/* './packages/policy/popup_document_register_associate.xml' */

'gc_classification'=>'Klassifizierung',
'gc_place'=>'Ort',
'gc_time'=>'Zeit',
'gc_type'=>'Typ',
'tt_register_association'=>'Berichtsverbindung',
'tt_registers_bl'=>'<b>Berichte</b>',
'vb_associate_registers'=>'Verbinde Berichte',
'wn_no_register_selected'=>'Kein ausgewählter Bericht. Vorhanden',

/* './packages/policy/popup_document_register_edit.php' */

'tt_register_adding'=>'Bericht hinzufügen',
'tt_register_editing'=>'Bericht schreiben',

/* './packages/policy/popup_document_register_edit.xml' */

'lb_disposition_cl'=>'Einteilung:',
'lb_indexing_type_cl'=>'Index-Typ:',
'lb_origin_cl'=>'Ursprung:',
'lb_protection_requirements_cl'=>'Schutz-Anforderungen:',
'si_day_days'=>'Tage',
'si_month_months'=>'Monate',
'si_week_weeks'=>'Woche(n)',
'vb_edit'=>'Schreibe',
'vb_lookup'=>'Suche',
'wn_retention_time'=>'Die Laufzeit muss länger sein als 0',

/* './packages/policy/popup_document_register_read.xml' */

'lb_responsible'=>'Verantwortlich',
'lb_retention_time_cl'=>'<b>Laufzeit:</b>',
'lb_storage_place_cl'=>'Speicherplatz:',
'lb_storage_type_cl'=>'Speicher Typ:',
'tt_register_reading'=>'Bericht Lesung',
'vb_preferences'=>'Vorzüge',

/* './packages/policy/popup_document_revise_not_alter.xml' */

'tt_revise_document_without_altering'=>'Revisiere Dokument ohne Änderung',

/* './packages/policy/popup_document_revision_task.php' */

'mx_scheduled_revision'=>'Planmässige Revision',

/* './packages/policy/popup_document_revision_task.xml' */

'lb_author_cl'=>'Autor:',
'lb_file_cl'=>'Kartei:',
'lb_link_cl'=>'Link:',
'tt_document_revision'=>'Dokumentenerneuerung',
'vb_create_new_version'=>'Erstelle neue Version',
'vb_download'=>'Download',
'vb_revise_without_altering'=>'Revision ohne Erneuerung',
'vb_visit_link'=>'Besuche Link',
'vb_visualize'=>'Ansicht',

/* './packages/policy/popup_document_search.xml' */

'tt_document_search'=>'Dokument Suche',
'wn_no_document_selected'=>'Kein ausgewähltes Dokument vorhanden',

/* './packages/policy/popup_documents_search.xml' */

'tt_current_documents_bl'=>'<b>Aktuelle Dokumente</b>',

/* './packages/policy/popup_modification_comment.php' */

'tt_popup_modification_comment_creation'=>'Stellungnahme',
'tt_popup_modification_comment_creationfasdfawfqwerqwe'=>'Erstellung Stellungnahme',

/* './packages/policy/popup_modification_comment.xml' */

'lb_comment_bl_cl'=>'<b>Kommentar:</b>',
'tt_comment_modification'=>'Stellungnahme Modifikation',
'tt_comment_modification2'=>'Anmerkungsänderung',

/* './packages/policy/popup_readers_edit.xml' */

'si_all_processes'=>'Alle Prozesse',
'tt_current_readers_bl'=>'<b>Aktuelle Leser</b>',
'tt_readers_editing'=>'Leser Bearbeitung',

/* './packages/policy/popup_readers_view.xml' */

'tt_readers_visualization'=>'Leser Visualisierung',

/* './packages/policy/popup_register_readers_edit.xml' */

'tt_current_readers'=>'<b>Aktuelle Leser</b>',
'tt_readers_edit'=>'Lesereintrag',

/* './packages/policy/popup_revision_justification.xml' */

'tt_revision_justification'=>'Revisionsgrund',
'vb_send_for_revision'=>'Sende für Revision',

/* './packages/policy/popup_subdocuments_management.php' */

'st_create_subdocument'=>'Erstelle Unterdokument',

/* './packages/policy/popup_subdocuments_management.xml' */

'tt_manage_subdocuments'=>'Manage Nebenbelege',

/* './packages/policy/popup_template_search.xml' */

'rb_suggested_by_best_practice_only'=>'Zeige nur die Entwürfe der geschätzten Gute Beispiele',
'rb_suggested_only'=>'nur empfohlen',
'tt_document_templates_search'=>'Dokumentrntwürfe Suche',

/* './packages/policy/report/popup_report_accessed_documents.php' */

'rs_accessed_documents'=>'Nutzer Zutrittsbilanz',
'rs_management'=>'Management',
'rs_others'=>'Anderes',

/* './packages/policy/report/popup_report_accessed_documents.xml' */

'rs_type'=>'Typ',

/* './packages/policy/report/popup_report_contexts_without_documents.php' */

'rs_items_with_without_documents'=>'Komponente ohne Dokumente',

/* './packages/policy/report/popup_report_contexts_without_documents.xml' */

'rs_components'=>'Komponenten',

/* './packages/policy/report/popup_report_doc_instance_comment_and_justification.php' */

'mx_doc_instance_comment_and_justification'=>'Dokumenten Kommentare und Begründungen',

/* './packages/policy/report/popup_report_doc_instance_comment_and_justification.xml' */

'rs_header_instance_modify_comment'=>'Modifizierung Stellungnahme',
'rs_header_instance_rev_justification'=>'Revisionsgrund',

/* './packages/policy/report/popup_report_docs_with_high_frequency_revision.php' */

'tt_docs_with_high_frequency_revision'=>'Dokumente mit hoher Revisionsfrequenz',

/* './packages/policy/report/popup_report_docs_with_high_frequency_revision.xml' */

'rs_justification'=>'Grund',

/* './packages/policy/report/popup_report_document_accesses.php' */

'rs_accesses_to_documents'=>'Zutritt zu Dokumenten Bilanz',
'rs_no_type'=>'Kein Typ',

/* './packages/policy/report/popup_report_document_accesses.xml' */

'rs_date'=>'Datum',
'rs_time'=>'Zeit',

/* './packages/policy/report/popup_report_document_approvers.php' */

'rs_approvers'=>'Dokumente und ihre Genehmigungen',

/* './packages/policy/report/popup_report_document_dates.php' */

'rs_documents_dates'=>'Erstellung und Revisionsdatum von Dokumenten',

/* './packages/policy/report/popup_report_document_dates.xml' */

'rs_created_on_cl'=>'Erstellt am:',

/* './packages/policy/report/popup_report_document_readers.php' */

'rs_readers'=>'Dokumente und ihre Leser',

/* './packages/policy/report/popup_report_document_readers.xml' */

'rs_author_cl'=>'Autor:',

/* './packages/policy/report/popup_report_document_registers.php' */

'rs_registers_per_document'=>'Bericht von Dokument',

/* './packages/policy/report/popup_report_document_registers.xml' */

'rs_name'=>'Name',
'rs_retention_time'=>'Laufzeit',

/* './packages/policy/report/popup_report_document_summary.php' */

'mx_report_document_summary'=>'Dokument Einstellungen',

/* './packages/policy/report/popup_report_documents_by_area.php' */

'rs_documents_per_area'=>'Dokumente von Bereich',

/* './packages/policy/report/popup_report_documents_by_area.xml' */

'rs_document_documents'=>'Dokument(e)',

/* './packages/policy/report/popup_report_documents_by_component.php' */

'rs_document_of_component'=>'%component% Dokumente',
'tt_documents_by_component'=>'Dokumente von ERMS Komponente',

/* './packages/policy/report/popup_report_documents_by_state.php' */

'tt_documents_by_state'=>'Dokumente',

/* './packages/policy/report/popup_report_documents_by_type.php' */

'rs_documents_by_type'=>'Dokumente nach Typ',

/* './packages/policy/report/popup_report_documents_revision_late.php' */

'mx_documents_revision_late'=>'Dokumente mit verspäteter Revision',

/* './packages/policy/report/popup_report_documents_revision_late.xml' */

'rs_document_next_revision'=>'Nächste Revision',

/* './packages/policy/report/popup_report_documents_revision_schedule.php' */

'mx_documents_revision_schedule'=>'Dokumenten Revisions- Zeitplan',

/* './packages/policy/report/popup_report_documents_revision_schedule.xml' */

'rs_document_name'=>'Name',
'rs_document_schedule'=>'Zeitplan',

/* './packages/policy/report/popup_report_documents_without_register.php' */

'rs_document_main_approver_name'=>'Verantwortlich für Dokument',
'rs_document_name_bl'=>'Dokument',
'tt_documents_without_register'=>'Dokumente ohne Bericht',

/* './packages/policy/report/popup_report_not_accessed_documents.php' */

'rs_not_accessed_documents'=>'Dokumente ohne Zutritt',

/* './packages/policy/report/popup_report_pendant_approvals.php' */

'tt_pendant_approvals'=>'Schwebende Bewilligung',

/* './packages/policy/report/popup_report_pendant_approvals.xml' */

'rs_documents'=>'Dokumente',

/* './packages/policy/report/popup_report_registers_by_type.php' */

'rs_registers_by_type'=>'Berichte von Typ',

/* './packages/policy/report/popup_report_top_accessed_documents.php' */

'rs_most_accessed_documents'=>'Dokumente mit den meisten Zutritten',

/* './packages/policy/report/popup_report_top_accessed_documents.xml' */

'rs_accesses_cl'=>'Zugänge:',

/* './packages/policy/report/popup_report_top_revised_documents.php' */

'rs_most_revised_documents'=>'Meist überarbeitete Dokumente',

/* './packages/policy/report/popup_report_top_revised_documents.xml' */

'rs_revision_date'=>'Revisionsdatum',
'rs_revisions_cl'=>'Revisionen:',
'rs_version'=>'Version',

/* './packages/policy/report/popup_report_user_comments.php' */

'mx_user_comments'=>'Nutzer Kommentare',

/* './packages/policy/report/popup_report_user_comments.xml' */

'rs_comment_date'=>'Datum',
'rs_comment_text'=>'Stellungnahme',
'rs_comment_user'=>'Nutzer',

/* './packages/policy/report/popup_report_users_by_process.php' */

'rs_users_per_process'=>'Nutzer Angeschlossen an Prozesse',

/* './packages/policy/report/popup_report_users_by_process_and_area.xml' */

'rs_user_bl_cl'=>'<b>Nutzer:</b>',

/* './packages/policy/report/popup_report_users_with_pendant_read.php' */

'mx_documents_pendent_reads'=>'Nutzer mit offenen Lesungen',

/* './packages/policy/tab_policy_management.xml' */

'ti_all'=>'Alle',
'ti_developing'=>'In Abwicklung',
'ti_obsolete'=>'Veraltet',
'ti_publish'=>'Veröffentlicht',
'ti_registers'=>'Berichte',
'ti_revision'=>'In Revision',
'ti_to_approve'=>'In Bewilligung',
'ti_to_be_published'=>'Zur Veröffentlichung',

/* './packages/risk/nav_area.php' */

'st_area_remove_confirm'=>'Sind Sie sicher Sie möchten den Bereich <b>%area_name%</b> verschieben?',
'tt_remove_area'=>'verschiebe Bereich',

/* './packages/risk/nav_area.xml' */

'gc_subarea'=>'Unterbereich',
'lb_sub_areas_count_cl'=>'Anzahl der Unterbereiche:',
'mi_processes'=>'Prozesse',
'mi_sub_areas'=>'Unterbereich',
'tt_business_areas_bl'=>'<b>Business Bereich</b>',

/* './packages/risk/nav_asset.php' */

'grid_asset_filter_dependents'=>'(Abhängigkeiten von Aktiva \'<b>%Aktiva_name%</b>\')',
'st_asset_copy'=>'Kopie',
'st_asset_remove_confirm'=>'Sind Sie sicher Sie möchten  die Aktiva <b>%Aktiva_name%</b> verschieben?',
'tt_remove_asset'=>'VERSCHIEBE Aktiva',

/* './packages/risk/nav_asset.xml' */

'gc_asset_category'=>'Aktiva Kategorie',
'gc_asset_dependencies'=>'Abhängigkeit(en)',
'gc_asset_dependents'=>'Abhängige(s)',
'gc_processes'=>'Prozesse',
'lb_process_count_cl'=>'Anzahl von Prozesse:',
'mi_associate_processes'=>'Teilnehmer-Prozesse',
'mi_duplicate'=>'Duplikate',
'mi_edit_dependencies'=>'Justieren Sie Abhängigkeiten',
'mi_edit_dependents'=>'Justieren Sie Abhängige',
'mi_insert_risk'=>'Risiken eintragen',
'mi_load_risks_from_library'=>'Lade Risiken von der Bibliothek',
'tt_assets_bl'=>'<b>Aktiva</b>',

/* './packages/risk/nav_control.php' */

'st_control_remove_confirm'=>'Sind Sie sicher Sie möchten die Kontrolle <b>%control_name%</b> verschieben?',

/* './packages/risk/nav_control.xml' */

'gc_cost'=>'Kosten',
'gc_icon'=>'Icon',
'gc_risks'=>'Risiken',
'lb_risk_count_cl'=>'Anzahl von Risiken:',
'mi_associate_risks'=>'Angeschlossene Risiken',
'mi_view_risks'=>'Betrachte die Risiken',
'rb_active'=>'Aktiviert',
'rb_not_active'=>'Deaktiviert',
'tt_controls_directory_bl'=>'<b>Kontrolle Verzeichnis</b>',
'vb_revision_and_test_history'=>'Revision/Test Auflistung',

/* './packages/risk/nav_process.php' */

'grid_process_asset_filter'=>'(gefiltert mit Aktiva \'<b>%Aktiva_name%</b>\')',
'st_process_remove_confirm'=>'Sind Sie sicher Sie möchten den Prozess <b>%process_name%</b> verschieben?',
'tt_remove_process'=>'verschiebe Prozess',

/* './packages/risk/nav_process.xml' */

'cb_not_estimated'=>'Nicht erwartet',
'gc_assets'=>'Aktiva',
'gc_responsible'=>'Verantwortlich',
'gc_users'=>'Nutzer',
'lb_asset_count_cl'=>'Anzahl von Aktiva:',
'lb_responsible_cl'=>'Verantwortlich:',
'lb_value_cl'=>'Risiko:',
'mi_add_document'=>'Erstelle Dokument',
'mi_associate_assets'=>'Angeschlossene Aktiva',
'mi_documents'=>'Dokumente',
'mi_view_assets'=>'Zeige Aktiva',
'si_all_priorities'=>'Alle',
'si_all_responsibles'=>'Alle',
'si_all_types'=>'Alle',
'tt_processes_bl'=>'<b>Prozesse</b>',

/* './packages/risk/nav_report.php' */

'mx_no_priority'=>'Keine Priorität',
'mx_no_type'=>'Kein Typ',
'si_amount_of_risks_per_area'=>'Anzahl von Risiken des Bereich',
'si_amount_of_risks_per_process'=>'Anzahl von Risiken der Prozesse',
'si_areas_without_processes'=>'Bereich ohne Prozesse',
'si_assets'=>'Aktiva',
'si_assets_importance'=>'Aktiva Bedeutung',
'si_assets_without_risks'=>'Aktiva ohne Risiko Fall',
'si_checklist_per_event'=>'Checkliste von Aktiva',
'si_controls_efficiency'=>'Kontrolle Effizienz',
'si_controls_implementation_date'=>'Kontrolle Einfügungsdatum',
'si_controls_per_responsible'=>'Kontrolle von Verantwortlich',
'si_controls_per_risk'=>'Kontrolle von Risiko',
'si_controls_without_risks'=>'Kontrolle ohne angeschlossene Risiken',
'si_cost_of_controls_per_area'=>'Kosten von Kontrolle von Bereich',
'si_cost_of_controls_per_asset'=>'Kosten von Kontrolle von Aktiva',
'si_cost_of_controls_per_process'=>'Kosten von Kontrolle von Prozess',
'si_cost_of_controls_per_responsible'=>'Kosten von Kontrolle von Verantwortlich',
'si_cost_per_control'=>'Kosten von Kontrolle',
'si_duplicated_risks'=>'Dupliziere Risiken',
'si_element_classification'=>'Einheiten Klassifizierung',
'si_follow_up_of_controls'=>'Anschluss von Kontrolle',
'si_follow_up_of_controls_efficiency'=>'Anschluss von Kontroll-Effizienz',
'si_follow_up_of_controls_test'=>'Anschluss von Kontrolle Test',
'si_non_parameterized_risks'=>'nicht-erwartete Risiken',
'si_not_measured_controls'=>'nicht gemessene Kontrolle',
'si_pending_items'=>'Schwebende Einheiten',
'si_pending_tasks'=>'Schwebende Aufgaben',
'si_plan_of_risks_treatment'=>'Risiko Aufbereitungsplan',
'si_planning_of_controls'=>'Planung von Kontrolle',
'si_processes_without_assets'=>'Prozesse ohne Aktiva',
'si_report_conformity'=>'Übereinstimmung',
'si_risk_impact'=>'Risiko Vorgang',
'si_risk_status_per_area'=>'Risiko Status von Bereich',
'si_risk_status_per_process'=>'Risiko Status von Prozess',
'si_risks_per_area'=>'Risiko von Bereich',
'si_risks_per_asset'=>'Risiko von Aktiva',
'si_risks_per_control'=>'Risiko von Kontrolle',
'si_risks_per_process'=>'Risiko von Prozess',
'si_risks_values'=>'Risiko Umfang',
'si_sgsi_policy_statement'=>'ERMS\'s Richtlinienerklärung',
'si_sgsi_scope_statement'=>'ERMS\'s Bereichserklärung',
'si_statement_of_applicability'=>'Aussage der Anwendbarkeit',
'si_summary_of_controls'=>'Zusammenfassung von Kontrolle',
'si_top_10_assets_with_highest_risks'=>'Top 10 Aktiva mit den meisten hohen Risiken',
'si_top_10_assets_with_most_risks'=>'Top 10 Aktiva mit Risiken',
'si_top_10_risks_per_area'=>'Top 10 Risiken nach Bereich',
'si_top_10_risks_per_process'=>'Top 10 Risiken von Prozess',
'si_users_responsibilities'=>'Nutzer Antworten',
'st_all_standards'=>'Alle',

/* './packages/risk/nav_report.xml' */

'cb_all'=>'Alle',
'cb_area'=>'Bereich',
'cb_asset'=>'Aktiva',
'cb_asset_security'=>'Wertesicherheit',
'cb_control'=>'Kontrolle',
'cb_high'=>'Hoch',
'cb_low'=>'Niedrig',
'cb_medium'=>'Mittelmässig',
'cb_not_parameterized'=>'Nicht geschätzt',
'cb_organize_by_asset'=>'Einteilen von Vermögen',
'cb_organize_by_process'=>'Einteilen der Prozesse',
'cb_process'=>'Prozess',
'lb_classification_cl'=>'Klassifizierung:',
'lb_comment_cl'=>'Stellungnahme:',
'lb_consider_cl'=>'Berücksichtigung:',
'lb_filter_by_cl'=>'Filter von:',
'lb_filter_by_standard_cl'=>'Standardfilter:',
'lb_filter_cl'=>'Filter:',
'lb_process_cl'=>'Prozess:',
'lb_reponsible_for_cl'=>'Verantwortlich für:',
'lb_values_cl'=>'Werte:',
'rb_area'=>'Bereich',
'rb_area_process'=>'Bereich und Prozess',
'rb_asset'=>'Aktiva',
'rb_both'=>'beide',
'rb_detailed'=>'Detailliert',
'rb_efficient'=>'Effizient',
'rb_implemented'=>'Einschliesslich',
'rb_inefficient'=>'Uneffizient',
'rb_planned'=>'Geplant',
'rb_process'=>'Prozess',
'rb_real'=>'Potentiell',
'rb_residual'=>'Rest',
'rb_summarized'=>'Ausgerechnet',
'si_all_reports'=>'Alle Berichte',
'si_areas_by_priority'=>'Bereiche nach Priorität',
'si_areas_by_type'=>'Art von Bereiche',
'si_control_reports'=>'Kontroll-Berichte',
'si_controls_by_type'=>'Kontrolle von Typ',
'si_events_by_type'=>'Art von Ereignissen',
'si_financial_reports'=>'Finanzielle Berichte',
'si_iso_27001_reports'=>'ISO 31000 Berichte',
'si_others'=>'Anderes',
'si_processes_by_priority'=>'Prozesse nach Priorität',
'si_processes_by_type'=>'Art von Prozesse',
'si_risk_management_reports'=>'Risiko Management Berichte',
'si_risk_reports'=>'Risiko Berichte',
'si_risks_by_type'=>'Art von Risiken',
'si_summaries'=>'Auflistung',
'st_area_priority'=>'Bereichsprioritäten',
'st_area_type'=>'Bereichstypen',
'st_asset_cl'=>'Aktiva:',
'st_config_filter_type_priority'=>'Konfiguriere ausgewählte Filter:',
'st_control_type'=>'Art der Kontrolle',
'st_event_type'=>'Art des Ereignisses',
'st_no_filter_available'=>'Kein Filter verfügbar.',
'st_none_selected'=>'Filter auswählen',
'st_organize_by_asset'=>'Organisiere Aktiva',
'st_process_priority'=>'Prozess-Priorität',
'st_process_type'=>'Art des Prozesse',
'st_risk_type'=>'Risiko Typen',
'st_show_cl'=>'Zeige:',
'st_standard_cl'=>'Standard:',
'tt_specific_report_filters'=>'Spezieller Bereichsfilter',
'tt_type_and_priority_filter_cl'=>'Typ und Priorität Filter:',
'vb_pre_report'=>'Vorbericht',
'vb_report'=>'Report',
'wn_no_report_selected'=>'Es sind keine Berichte ausgewählt.',
'wn_select_at_least_one_filter'=>'mindestens ein Filter muss ausgewählt sein!',
'wn_select_at_least_one_parameter'=>'mindestens ein Parameter muss ausgewählt sein.',
'wn_select_risk_value'=>'Bitte',

/* './packages/risk/nav_risk.php' */

'gs_avoided'=>'gelöschte',
'gs_reduced'=>'Reduziert',
'gs_restrained'=>'Einbehaltene',
'gs_transferred'=>'transferierte',
'st_denied_permission_to_edit'=>'Sie haben keine Erlaubnis zum Schreiben von',
'st_risk_remove_confirm'=>'Sind Sie sicher Sie möchten das Risiko <b>%risk_name%</b> verschieben ?',
'st_risk_remove_has_incident'=>'<br><br><b>Achtung: Das Risiko hat angeschlossene Vorgänge .</b>',
'st_risk_remove_many_confirm'=>'Sind Sie sicher die <b>%count%</b> ausgewählten Risiken zu verschieben?',
'st_risk_remove_many_have_incident'=>'<br><br><b>Achtung: Mindestens ein Risiko hat angeschlossene Vorgänge.</b>',
'tt_incident_filter'=>'gefiltert von Vorgang \'<b>%incident_name%</b>\'',
'tt_remove_risk'=>'verschiebe Risiko',

/* './packages/risk/nav_risk.xml' */

'cb_avoided'=>'Gelöscht',
'cb_not_treated'=>'nicht behandelt',
'cb_reduced'=>'Gemindert',
'cb_restrained'=>'Behalten',
'cb_search_in_name_only'=>'nur bei Namen suchen',
'cb_transferred'=>'Übertragen',
'gc_controls'=>'Kontrolle',
'gc_residual'=>'Rest',
'gc_risk'=>'Risiko',
'gc_risk_name'=>'Name',
'gc_status'=>'Status',
'gc_treatment'=>'Aufbereitung',
'lb_control_count_cl'=>'Anzahl der Kontrollen:',
'lb_name_description_cl'=>'Name/Beschreibung',
'lb_risk_residual_value_cl'=>'Rest:',
'lb_risk_value_cl'=>'Risiko:',
'lb_status_cl'=>'Status:',
'lb_treatment_type_cl'=>'Art der Behandlung:',
'mi_avoid'=>'Gelöschtes Risiko',
'mi_cancel_treatment'=>'Lösche Aufbereitung',
'mi_hold_accept'=>'Beibehalten/Akzeptiere Risiko',
'mi_reduce_risk'=>'Reduziere Risiko',
'mi_transfer'=>'Transferiere Risiko',
'mi_view_controls'=>'Zeige Kontrolle',
'si_all_assets'=>'Alle',
'si_all_states'=>'Alle',
'st_from'=>'von',
'st_to'=>'bis',
'vb_aply'=>'Beantrage',
'vb_parameterize'=>'Schätzung',

/* './packages/risk/popup_area_edit.php' */

'tt_area_adding'=>'Bereich hinzufügen',
'tt_area_editing'=>'Bereich schreiben',

/* './packages/risk/popup_area_edit.xml' */

'vb_ok'=>'Ok',

/* './packages/risk/popup_asset_dependencies.php' */

'st_dependencies_list'=>'Abhängigkeitenliste',
'st_dependents_list'=>'Abhängigenliste',
'tt_dependencies_adjustment'=>'Abhängigkeiten Justierung',
'tt_dependents_adjustment'=>'Abhängigen Justierung',

/* './packages/risk/popup_asset_dependencies.xml' */

'tt_assets_list'=>'Aktiva Liste',

/* './packages/risk/popup_asset_edit.php' */

'tt_asset_adding'=>'Aktiva hinzufügen',
'tt_asset_duplicate'=>'Aktiva Duplizierung',
'tt_asset_editing'=>'Aktiva schreiben',

/* './packages/risk/popup_asset_edit.xml' */

'lb_category_bl_cl'=>'<b>Kategorie:</b>',
'lb_demands_attention_to_legal_conformity_cl'=>'Verlangt Aufmerksamkeit zur zugelassenen Übereinstimmung :',
'lb_justification_cl'=>'Grund:',
'lb_security_responsible_bl_cl'=>'<b>Sicherheits Verantwortlicher:</b>',
'lb_value_cl_money_symbol'=>'Kosten',
'st_asset_parametrization_bl'=>'<b>Aktiva Relevanz</b>',
'vb_adjust_dependencies'=>'Justieren Sie Abhängigkeiten',
'vb_adjust_dependents'=>'Justieren Sie Abhängige',

/* './packages/risk/popup_asset_search.xml' */

'wn_no_asset_selected'=>'Keine ausgewählte Aktiva vorhanden.',

/* './packages/risk/popup_best_practice_search.xml' */

'lb_section_cl'=>'Bereich:',
'si_all'=>'Alle',
'tt_best_practices_search'=>'Suche gute Beispiele',

/* './packages/risk/popup_control_cost.xml' */

'lb_total'=>'Total',
'tt_control_cost'=>'Kostenkontrolle',

/* './packages/risk/popup_control_edit.php' */

'st_and'=>'und',
'st_of_revision'=>'der Revision',
'st_of_test'=>'von test',
'st_remove_control_implementation_confirm'=>'Sind Sie sicher Sie möchten  die Einfügung des Falles <b>%control_name%</b> verschieben?',
'st_result_in_removing'=>'Das Entfernen wird die Folge sein',
'st_this_control'=>'diese Kontrolle',
'tt_control_adding'=>'Kontrolle hinzufügen',
'tt_remove_control_implementation'=>'verschiebe Kontrolle Einfügung',

/* './packages/risk/popup_control_edit.xml' */

'cb_enable_control_revision_cl'=>'Ermöglichen Sie Kostenrevision:',
'cb_enable_control_tests_cl'=>'Ermöglichen Sie Kontroll Tests:',
'lb_best_practices_bl_cl'=>'<b>Gute Beispiele:</b>',
'lb_control_cost_cl'=>'Kontrollkosten:',
'lb_evidence_cl'=>'Belege:',
'lb_implementation_limit_bl_cl'=>'<b>Einfügungs Limit:</b>',
'lb_implemented_on_cl'=>'Eingefährt an:',
'lb_send_alert_task_bl_cl'=>'<b>Sende Alarm/Aufgabe:</b>',
'st_day_days_before'=>'Tage zuvor',
'st_money_symbol'=>'$',
'tt_control_editing'=>'Schreibe Kontrolle',
'vb_add'=>'Hinzufügen',
'vb_control_cost'=>'Steuern Sie Kosten',
'vb_implement'=>'Hilfsmittel',
'vb_remove_implementation'=>'Entfernen Sie Implementierung',
'vb_revision'=>'Revision',
'vb_tests'=>'Tests',
'wn_implementation_cannot_be_null'=>'"Das Einfügungsdatum kann nicht Null sein',
'wn_implementation_limit_after_today'=>'Der Stichtag muss gleich oder später als das aktuelle Datum sein',

/* './packages/risk/popup_control_revision.xml' */

'lb_expected_efficiency_bl_cl'=>'<b>Erwartete Effizienz:</b>',
'lb_high_value_5_cl'=>'(High) Wert 5:',
'lb_low_value_1_cl'=>'(Low) Wert 1:',
'st_helper'=>'[?]',
'to_control_revision_expected_efficiency'=>'<b>Erwartete Leistungsfähigkeit:</b><br/><br/>Stellen Sie fest',
'to_control_revision_metric_help'=>'<b>Metrik:</b><br/><br/>Definieren Sie',
'to_control_revision_value'=>'<b>Werte:</b><br/><br/>Stellen Sie eine Skala für die möglichen Service-Niveaus dieser Steuerung in der Periode fest.<br/><br/>Sehen Sie die niedrigen Werte als schlechte Leistung',
'tt_control_efficiency_revision'=>'Kontrolleffizienz Revision',

/* './packages/risk/popup_control_revision_and_test_edit.php' */

'st_control_test_not_ok'=>'nicht Ok',
'st_control_test_ok'=>'Ok',
'st_revision_remove_confirm'=>'Sind Sie sicher die Revision mit erwartetetem Datum von <b>%deadline_date%</b>  und Durchführungsdatum von <b>%realized_date%</b> zu verschieben ?',
'st_revision_update_by_insert_confirm'=>'Es ist eine Fall Revision am erwarteteten Datum von <b>%date_to_do%</b>. Sind Sie sicher diese Kontrolle Revision auszutauschen?',
'st_test_remove_confirm'=>'Sind Sie sicher den Test mit erwartetetem Datum von <b>%deadline_date%</b> und Durchführungs Datum von <b>%realized_date%</b>  zu verschieben?',
'st_test_update_by_insert_confirm'=>'Es ist ein Fall Test am erwarteteten Datum von <b>%date_to_do%</b>. Sind Sie sicher diesen Kontrolltest auszutauschen?',
'tt_control_efficiency_history_add_title'=>'Einfüge Kontroll Revision',
'tt_control_test_history_add_title'=>'Einfüge Kontroll Test',
'tt_remove_control'=>'verschiebe Kontrolle Einfügung',
'tt_replace_control_revision'=>'Verschieben von Revisionskontrolle <b>%control_name%</b>',
'tt_replace_control_test'=>'Verlegung von Kontrolle von Fall <b>%control_name%</b>',

/* './packages/risk/popup_control_revision_and_test_edit.xml' */

'gc_date_realized'=>'Ausführungs Datum',
'gc_date_to_do'=>'erwartetes Datum',
'gc_expected_efficiency'=>'EE',
'gc_real_efficiency'=>'BETREFF',
'gc_realization_date'=>'Ausführungs Datum',
'gc_test_date_to_do'=>'erwartetes Datum',
'gc_test_value'=>'Test Wert',
'mi_edit'=>'Schreibe',
'mi_remove'=>'verschiebe',
'mi_revision_edit'=>'Schreibe Revisionen',
'mi_test_edit'=>'Schreibe Tests',
'rb_not_ok'=>'Nicht korrekt',
'rb_ok'=>'Ok',
'st_control_test_result'=>'<b>Test:</b>',
'st_date_realized'=>'<b>Vollendungsdatum:</b>',
'st_date_to_do'=>'<b>Erwartetes Datum:</b>',
'st_description'=>'Beschreibung:',
'st_er'=>'Betreff',
'test_date_to_do'=>'<b>Erwartetes Datum</b>',
'tt_add_and_edit_revision_and_test_control'=>'Hinzufügen oder ändern von Kontrolltest und Erneuerung',
'tt_control_efficiency_history_edit_title'=>'Ändern Kontroll-Erneuerung',
'tt_control_selected_revision_history'=>'Kontroll Revision Auflistung',
'tt_control_selected_test_history'=>'Kontrolltest-Ablauf',
'tt_control_test_history_edit_title'=>'Ändern Kontrolltest',
'vb_insert'=>'Einfügen',

/* './packages/risk/popup_control_revision_task.php' */

'tt_control_revision_task_incident_revision'=>'Beinhaltet reduzierte Bemessung der Kontrolleffizienz (von Inzident %incident_name%)',

/* './packages/risk/popup_control_revision_task.xml' */

'lb_from'=>'von',
'lb_start_cl'=>'Start:',
'lb_to'=>'bis',
'lb_value_1_cl'=>'Wert 1:',
'lb_value_2_cl'=>'Wert 2:',
'lb_value_3_cl'=>'Wert 3:',
'lb_value_4_cl'=>'Wert 4:',
'lb_value_5_cl'=>'Wert 5:',
'st_ee'=>'EE',
'st_er_bl'=>'<b>Betreff</b>',
'st_high_parenthesis'=>'(Hoch)',
'st_low_parenthesis'=>'(Niedrig)',
'st_metric'=>'Metrisch',
'st_total_incidents_found_on_period'=>'Gesamtanzahl von Ereignissen während der Periode:',
'st_total_incidents_on_time'=>'Gesamtanzahl von Ereignissen während des Zeitabschnitts:',
'to_expected_efficiency'=>'Erwartete Effizienz',
'to_real_efficiency'=>'Echte Effizienz',
'tt_control_revision_task'=>'Kontrolle betrachten',
'tt_incidents_occurred_from_period'=>'Ereignisse traten auf von %date_begin% bis %date_today%',
'vb_filter'=>'Filter',

/* './packages/risk/popup_control_risk_edit.php' */

'tt_risk_control_association_editing'=>'Risiko Kontrolle Verbindung schreiben',

/* './packages/risk/popup_control_risk_edit.xml' */

'tt_risks_bl'=>'<b>Risiken</b>',
'vb_associate_risk'=>'Risiken einbinden',
'vb_disassociate_risks'=>'Risiken aussondern',

/* './packages/risk/popup_control_risk_search.xml' */

'gc_asset'=>'Aktiva',
'tt_risks_search'=>'Risikensuche',
'vb_associate_risks'=>'Teilnehmer-Gefahren',
'wn_no_risk_selected'=>'Es ist kein Risiko ausgewählt.',

/* './packages/risk/popup_control_search.xml' */

'vb_associate'=>'Angeschlossen',

/* './packages/risk/popup_control_test.xml' */

'lb_schedule_bl_cl'=>'<b>Zeitplanung</b>',
'tt_control_test_revision'=>'Kontroll Test Revision',

/* './packages/risk/popup_control_test_task.xml' */

'cb_no'=>'Nein',
'cb_open_next_task'=>'?Öffne neue Aufgabe',
'cb_yes'=>'Ja',
'lb_description_test'=>'Test Beschreibung:',
'lb_observations_cl'=>'Beobachtungen:',
'lb_test_ok_bl_cl'=>'<b>Test ok:</b>',
'tt_control_test'=>'Kontrolle Test',

/* './packages/risk/popup_create_risks_from_category.xml' */

'tt_risk_creation_from_category'=>'Risiko Erstellung von einer Fall Kategorie',

/* './packages/risk/popup_create_risks_from_events.xml' */

'st_select_events_not_to_associate'=>'Wählen Sie unten die Ereignisse <b>NICHT</b> die Sie auf den Wert beziehen möchten!',
'tt_risk_creation_from_events'=>'Gefahr- Kreation von den Fällen',
'vb_link_events_later'=>'Link Ereignisse später',
'vb_relate_events_to_asset'=>'Link Ereignisse zu Werte',

/* './packages/risk/popup_process_asset_association.xml' */

'tt_assets_search'=>'Suche Aktiva',
'tt_current_assets_bl'=>'<b>Derzeitige Aktiva</b>',

/* './packages/risk/popup_process_edit.php' */

'st_no_published_documents'=>'Es sind keine veröffentlichten Dokument vorhanden',
'tt_process_adding'=>'Prozess hinzufügen',
'tt_process_editing'=>'Prozess schreiben',

/* './packages/risk/popup_process_edit.xml' */

'lb_area_bl_cl'=>'<b>Bereich:</b>',
'lb_document_cl'=>'Dokument:',
'lb_name_bl_cl'=>'<b>Name:</b>',
'lb_priority_cl'=>'Priorität:',
'lb_responsible_bl_cl'=>'<b>Verantwortlich:</b>',
'vb_associate_users'=>'Angeschlossene Nutzer',
'vb_properties'=>'Einstellungen',
'vb_view'=>'Zeige',

/* './packages/risk/popup_process_search.xml' */

'gc_area'=>'Bereich',
'lb_area_cl'=>'Bereich:',
'si_all_areas'=>'Alle Bereich',
'tt_current_processes_bl'=>'<b>Aktuelle Prozesse</b>',
'tt_processes_search'=>'Prozesse Suche',
'vb_remove'=>'Verschiebe',

/* './packages/risk/popup_risk_control_association_edit.php' */

'st_control_not_mitigating_efficiency_message'=>'Die Kontrolle verringert nicht die Risiken',
'st_control_not_mitigating_not_implemented_message'=>'Der Fall reduziert nicht die Risiken weil es ist nicht eingefügt und der Stichtag der Einfügung ist abgelaufen!',

/* './packages/risk/popup_risk_control_association_edit.xml' */

'lb_control_cl'=>'Kontrolle:',
'st_control_parametrization_bl_cl'=>'<b>Kontrollerwartung:</b>',
'st_values_to_reduce_risk_consequence_and_probability'=>'Die Werte auf der Seite zeigen',
'tt_risk_reduction'=>'Risikenminderung',

/* './packages/risk/popup_risk_control_edit.php' */

'tt_control_risk_association_editing'=>'Risiko-Kontrolle Verbindung schreiben',

/* './packages/risk/popup_risk_control_edit.xml' */

'lb_risk_cl'=>'Risiko:',
'mi_edit_association'=>'Schreibe an Gemeinschaft',
'st_residual'=>'Rest',
'st_risk'=>'Risiko',
'tt_controls_bl'=>'<b>Kontrolle</b>',
'vb_disassociate_controls'=>'Aussondern der Kontrolle',

/* './packages/risk/popup_risk_control_search.xml' */

'cb_search_only_suggested_controls'=>'Suche nur vorgeschlagene Kontrolle',
'gc_name'=>'Name',
'lb_name_cl'=>'Name:',
'tt_control_search'=>'Kontrollensuche',
'vb_associate_control'=>'Angeschlossene Kontrolle',
'wn_no_control_selected'=>'Keine ausgewählte Kontrolle vorhanden',

/* './packages/risk/popup_risk_edit.php' */

'st_sensitive_data_reapproval'=>'Sie wechseln sensible Daten',
'tt_reapproval'=>'betreff Bewilligung',
'tt_risk_editing'=>'Risiko schreiben',

/* './packages/risk/popup_risk_edit.xml' */

'lb_asset_bl_cl'=>'<b>Aktiva:</b>',
'lb_event_bl_cl'=>'<b>Fall:</b>',
'lb_impact_cl_money_symbol'=>'Finanzielle Auswirkung:',
'lb_real_risk_bl_cl'=>'<b>Potentielles Risiko:</b>',
'lb_residual_risk_bl_cl'=>'<b>Restrisiko:</b>',
'lb_type_cl'=>'Typ:',
'si_select_one_of_the_items_below'=>'Wähle eines der unteren Einheiten',
'st_risk_parametrization_bl_cl'=>'<b>Risiko Schätzung:</b>',
'static_static19'=>'Vorgang:',
'vb_calculate_risk'=>'Berechne Risiko',
'vb_config'=>'Konfiguriere',
'vb_find'=>'Finde',
'vb_probability_automatic_calculation'=>'Automatische Wahrscheinlichkeits-Berechnung',
'warning_wn_asset_desparametrized'=>'Es ist nicht möglich',
'warning_wn_control_desparametrized'=>'"Mindestens eine Gefahr kann nicht eingeschätzt werden. Bevor Sie das Restrisiko berechnen',

/* './packages/risk/popup_risk_insert_search_event_asset.xml' */

'gc_category'=>'Kategorie',
'gc_description'=>'Beschreibung',
'gc_impact'=>'Vorgänge',
'lb_asset_cl'=>'Aktiva:',
'lb_category_cl'=>'Kategorie:',
'lb_description_cl'=>'Beschreibung:',
'si_all_categories'=>'Alle Kategorien',
'tt_events_search'=>'Ereignissuche',
'vb_create_risk'=>'Erstelle Risiko',
'vb_search'=>'Suche',
'wn_no_event_selected'=>'Kein ausgewählter Fall vorhanden',

/* './packages/risk/popup_risk_insert_type.xml' */

'rb_create_independent_risk'=>'Erstelle ein eingefügtes Risiko',
'st_which_action_to_take'=>'Welche Aktion möchten Sie durchführen?',
'tt_risk_adding'=>'Risiko hinzufügen',
'vb_cancel'=>'Abbrechen',
'vb_create'=>'erstelle',
'vb_create_risk_from_event'=>'Erstelle Risiko von einem Fall (vorgeschlagen)',

/* './packages/risk/popup_risk_parameters_weight.xml' */

'tt_risk_parameters_weight'=>'Risiko Parameter Gewichtung',

/* './packages/risk/popup_risk_parametrization.php' */

'mx_probability'=>'Wahrscheinlichkeit',
'st_denied_permission_to_estimate_risks'=>'Sie haben keine Erlaubnis zum Bewerten einiger Risiken',
'to_impact_cl'=>'Vorgang:',
'to_risk_cl'=>'Risiko:',

/* './packages/risk/popup_risk_parametrization.xml' */

'lb_all_bl_cl'=>'<b>Alle:</b>',
'tt_risk_parametrization'=>'Risiko Schätzung',

/* './packages/risk/popup_risk_treatment.php' */

'tt_avoid'=>'gelöscht',
'tt_hold_accept'=>'behalten/akzeptieren',
'tt_transfer'=>'transferieren',

/* './packages/risk/popup_risk_treatment.xml' */

'lb_justification_bl_cl'=>'<b>Grund:</b>',
'st_accept_transfer_or_avoid_risk'=>'Möchten Sie <b>%treatment_type%</b> riskieren für \'<b>%risk_name%</b>\'?',
'tt_risk_treatment'=>'Risiko Aufbereitung (<b>%treatment_type%</b>)',
'vb_save'=>'Speichere',

/* './packages/risk/popup_risks_control_association_edit.xml' */

'lb_all_risks_bl_cl'=>'<b>Alle Risiken:</b>',
'lb_control_bl_cl'=>'<b>Kontrolle:</b>',
'tt_risk_control_association_parametrization'=>'Riskenminderung',

/* './packages/risk/report/clock.php' */

'st_generating_report'=>'Generiere Bericht...',
'st_operation_wait'=>'<b>Attention:</b> Dieser Bericht wird in wenigen Minuten erstellt.<br> Bitte',

/* './packages/risk/report/loading_report.php' */

'st_verifying_existing_reports'=>'Verifizierung bestehender Berichte.',

/* './packages/risk/report/popup_report_activities.xml' */


/* './packages/risk/report/popup_report_areas.php' */

'mx_areas'=>'Bereiche',

/* './packages/risk/report/popup_report_areas.xml' */

'rs_header_area_name'=>'Name',
'rs_header_area_responsible'=>'Verantwortlich',

/* './packages/risk/report/popup_report_areas_by_priority.php' */

'rs_areas_by_priority'=>'Bereich von Priorität',

/* './packages/risk/report/popup_report_areas_by_type.php' */

'rs_areas_by_type'=>'Bereich von Typ',

/* './packages/risk/report/popup_report_areas_without_processes.php' */

'rs_areas_without_processes'=>'Bereich ohne Prozesse',

/* './packages/risk/report/popup_report_asa.xml' */


/* './packages/risk/report/popup_report_asset.php' */

'rs_assets'=>'Aktiva',

/* './packages/risk/report/popup_report_asset.xml' */


/* './packages/risk/report/popup_report_asset_deps.php' */

'si_assets_deps'=>'Aktiva - Verbliebene und Verbliebenes',
'si_dependencies'=>'Abhängigkeiten',
'si_dependents'=>'Verbliebenes',

/* './packages/risk/report/popup_report_asset_importance.php' */

'rs_assets_importance'=>'Aktiva Bedeutung',

/* './packages/risk/report/popup_report_asset_relevance.php' */

'rs_asset_cl_bl'=>'<b>Aktiva:</b>',
'si_asset_relevance'=>'Aktiva Relevanz',

/* './packages/risk/report/popup_report_asset_relevance.xml' */

'rs_responsible_cl'=>'Verantwortlich:',

/* './packages/risk/report/popup_report_assets.php' */

'mx_assets'=>'Aktiva',

/* './packages/risk/report/popup_report_assets.xml' */

'rs_header_asset_name'=>'Name',
'rs_header_asset_responsible'=>'Verantwortlich',

/* './packages/risk/report/popup_report_assets_without_risks.php' */

'rs_assets_without_risks'=>'Aktiva ohne Risiko Fall',

/* './packages/risk/report/popup_report_assets_without_risks.xml' */

'rs_security_responsible'=>'Sicherheitsverantwortlicher',

/* './packages/risk/report/popup_report_best_practices.php' */

'mx_best_practices'=>'Gute Beispiele',

/* './packages/risk/report/popup_report_best_practices.xml' */

'rs_best_practice_name'=>'Name',

/* './packages/risk/report/popup_report_bia_quantitative.xml' */


/* './packages/risk/report/popup_report_conformity.php' */

'rs_applied_best_practices'=>'Gute Beispiele beantragt',
'rs_not_applied_best_practices'=>'Gute Beispiele nicht beantragt',
'rs_not_used'=>'nicht genutzt',
'rs_not_used_controls'=>'Kontrolle nicht genutzt',
'rs_report_conformity_footer'=>'Gute Beispiele Total = %total% / Gute Beispiele beantragt Total = %applied%',
'rs_used_controls'=>'Genutzte Kontrolle',
'tt_conformity'=>'Konformität',

/* './packages/risk/report/popup_report_conformity.xml' */

'rs_standard_cl'=>'Standard:',

/* './packages/risk/report/popup_report_control_accompaniment.php' */

'rs_follow_up_of_controls'=>'Anschluss von Kontrolle',

/* './packages/risk/report/popup_report_control_cost.php' */

'rs_cost_per_control'=>'Kosten von Kontrolle',

/* './packages/risk/report/popup_report_control_cost_by_area.php' */

'rs_cost_of_controls_per_area'=>'Kosten der Kontrolle von Bereich',

/* './packages/risk/report/popup_report_control_cost_by_asset.php' */

'rs_cost_of_controls_per_asset'=>'Kosten von Kontrolle von Aktiva',

/* './packages/risk/report/popup_report_control_cost_by_cost_category.php' */

'mx_control_cost_by_cost_category'=>'Kontrolle Kosten von Kosten Kategorie',

/* './packages/risk/report/popup_report_control_cost_by_cost_category.xml' */

'rs_control_cost'=>'Kosten',
'rs_control_name'=>'Name',
'rs_total_cost'=>'Total',

/* './packages/risk/report/popup_report_control_cost_by_process.php' */

'rs_cost_of_controls_per_process'=>'Kosten von Kontrolle von Prozess',

/* './packages/risk/report/popup_report_control_cost_by_responsible.php' */

'rs_cost_of_controls_per_responsible'=>'Kosten von Kontrolle von Verantwortlich',

/* './packages/risk/report/popup_report_control_cost_by_responsible.xml' */

'rs_cost'=>'Kosten',

/* './packages/risk/report/popup_report_control_efficiency.php' */

'rs_controls_efficiency'=>'Kontroll- Effizienz',
'rs_efficient_controls'=>'Effiziente Kontrolle',
'rs_inefficient_controls'=>'Uneffiziente Kontrolle',

/* './packages/risk/report/popup_report_control_efficiency_accompaniment.php' */

'rs_follow_up_of_control_efficiency'=>'Anschluss von Kontroll Effizienz',

/* './packages/risk/report/popup_report_control_efficiency_accompaniment.xml' */

'rs_ee'=>'EE',
'rs_re'=>'Betreff',

/* './packages/risk/report/popup_report_control_implementation_date.php' */

'rs_controls_implemented_outside_stated_period'=>'Kontrolle eingefügt ausserhalb angegebenem Zeitraum',
'rs_controls_implemented_within_stated_period'=>'Kontrolle eingefügt innerhalb angegebenem Zeitraum',
'rs_date_of_implementation_of_controls'=>'Kontrolle Einfügung Datum',

/* './packages/risk/report/popup_report_control_implementation_date.xml' */

'rs_implementation'=>'Einfügungen',

/* './packages/risk/report/popup_report_control_planning.php' */

'rs_implementation_date'=>'Einfügungs Datum',
'rs_implementation_deadline'=>'Einfügung Stichtag',
'rs_implemented_controls'=>'Eingefügte Kontrolle',
'rs_planned_controls'=>'Geplante Kontrolle',
'rs_planning_of_controls'=>'Planung von Kontrolle',

/* './packages/risk/report/popup_report_control_revision_agenda_by_responsible.php' */

'si_control_revision_agenda_by_responsible'=>'Kontrolle Revision Agenda von Verantwortlich',

/* './packages/risk/report/popup_report_control_revision_agenda_by_responsible.xml' */

'rs_next_revision'=>'Nächste Revision',

/* './packages/risk/report/popup_report_control_summary.xml' */

'rs_best_practice'=>'Gute Beispiele',

/* './packages/risk/report/popup_report_control_summary_pm.php' */

'rs_summary_of_controls'=>'Zusammenfassung von Kontrolle',

/* './packages/risk/report/popup_report_control_summary_pm.xml' */

'rs_accomplishment_bl_cl'=>'<b>Durchführung:</b>',
'rs_description_cl'=>'<b>Beschreibung:</b>',

/* './packages/risk/report/popup_report_control_test_accompaniment.php' */

'rs_ctrl_test_not_ok'=>'nicht OK',
'rs_ctrl_test_ok'=>'Ok',
'rs_follow_up_of_control_test'=>'Anschluss von Kontrolle Test',

/* './packages/risk/report/popup_report_control_test_accompaniment.xml' */

'rs_comment'=>'Stellungnahme',
'rs_control_cl'=>'Kontrolle:',
'rs_date_accomplishment'=>'Fertigstellungsdatum',
'rs_predicted_date'=>'Geschätztes Datum',
'rs_test'=>'Test',

/* './packages/risk/report/popup_report_control_test_agenda_by_responsible.php' */

'si_control_test_agenda_by_responsible'=>'Kontroll Test Agenda von Verantwortlich',

/* './packages/risk/report/popup_report_control_test_agenda_by_responsible.xml' */

'rs_next_test'=>'Nächster Test',
'rs_processes'=>'Prozesse',
'rs_responsibles'=>'Verantwortlich',
'rs_schedule'=>'Zeitplanung',

/* './packages/risk/report/popup_report_controls.php' */

'mx_controls'=>'Kontrolle',

/* './packages/risk/report/popup_report_controls.xml' */

'rs_header_control_name'=>'Name',
'rs_header_control_responsible'=>'Verantwortlich',

/* './packages/risk/report/popup_report_controls_by_responsible.php' */

'rs_controls_per_responsible'=>'Kontrolle von Verantwortlich',

/* './packages/risk/report/popup_report_controls_by_risk.php' */

'rs_controls_per_risk'=>'Kontrolle von Risiko',

/* './packages/risk/report/popup_report_controls_by_risk.xml' */

'rs_affected_asset_bl_cl'=>'<b>Beeinträchtigte Aktiva:</b>',
'rs_risk_and_impact_bl_cl'=>'<b>Risiko / Vorgang:</b>',
'rs_risk_bl_cl'=>'<b>Risiko:</b>',

/* './packages/risk/report/popup_report_controls_by_type.php' */

'rs_controls_by_type'=>'Kontrolle von Typ',

/* './packages/risk/report/popup_report_controls_not_measured.php' */

'rs_not_measured_controls'=>'nicht gemessene Kontrolle',

/* './packages/risk/report/popup_report_controls_not_measured.xml' */

'rs_efficiency'=>'Effizienz',

/* './packages/risk/report/popup_report_controls_without_risks.php' */

'rs_controls_without_risks'=>'Kontrolle ohne angeschlossene Risiken',

/* './packages/risk/report/popup_report_detailed_pendant_tasks.php' */

'rs_detailed_pending_tasks'=>'Detailierte schwebende Aufgaben',

/* './packages/risk/report/popup_report_duplicated_risks.php' */

'tt_duplicated_risks'=>'Dupliziere Risiken',

/* './packages/risk/report/popup_report_duplicated_risks.xml' */

'rs_risk_cl'=>'Risiko:',

/* './packages/risk/report/popup_report_duplicated_risks_by_asset.php' */

'tt_duplicated_risks_by_asset'=>'Dupliziere Risiken von Aktiva',

/* './packages/risk/report/popup_report_duplicated_risks_by_asset.xml' */

'rs_risk'=>'Risiko',

/* './packages/risk/report/popup_report_events_by_category.php' */

'rs_checklist_by_event'=>'Checkliste von Aktiva',

/* './packages/risk/report/popup_report_events_by_category.xml' */

'rs_brackets'=>'[ ]',
'rs_event'=>'Ereignis',
'rs_event_category_cl'=>'Kategorie:',
'rs_na'=>'Nicht genehmigt',
'rs_nc'=>'NC',
'rs_ok'=>'Ok',

/* './packages/risk/report/popup_report_events_by_type.php' */

'rs_events_by_type'=>'Fall nach Typ',

/* './packages/risk/report/popup_report_pendant_contexts.php' */

'rs_pending_items'=>'Schwebende Einheiten',

/* './packages/risk/report/popup_report_pendant_contexts.xml' */

'rs_approver'=>'Billiger',

/* './packages/risk/report/popup_report_places.xml' */


/* './packages/risk/report/popup_report_policy.php' */

'rs_sgsi_policy_statement'=>'ERMS\'s Richtlinien Erklärung',

/* './packages/risk/report/popup_report_policy.xml' */

'rs_system_policy'=>'System Richtlinie',

/* './packages/risk/report/popup_report_process_by_process.xml' */


/* './packages/risk/report/popup_report_processes.php' */

'mx_processes'=>'Prozesse',

/* './packages/risk/report/popup_report_processes.xml' */

'rs_header_process_name'=>'Name',
'rs_header_process_responsible'=>'Verantwortlich',

/* './packages/risk/report/popup_report_processes_by_priority.php' */

'rs_processes_by_priority'=>'Prozesse von Priorität',

/* './packages/risk/report/popup_report_processes_by_priority.xml' */

'rs_priority_cl'=>'Rangfolge:',

/* './packages/risk/report/popup_report_processes_by_type.php' */

'rs_processes_by_type'=>'Prozess von Typ',

/* './packages/risk/report/popup_report_processes_without_assets.php' */

'rs_processes_without_assets'=>'Prozesse ohne Aktiva',

/* './packages/risk/report/popup_report_risk_amount_by_area.php' */

'rs_amount_of_risks_per_area'=>'Anzahl von Risiken des Bereich',

/* './packages/risk/report/popup_report_risk_amount_by_process.php' */

'rs_amount_of_risks_per_process'=>'Anzahl von Risiken von Prozess',

/* './packages/risk/report/popup_report_risk_financial_impact.php' */

'mx_risk_financial_impact'=>'Risiko finanzieller Vorgang',

/* './packages/risk/report/popup_report_risk_financial_impact.xml' */

'rs_risk_impact'=>'Risiko Vorgang',
'rs_risk_name'=>'Name',
'rs_risk_residual_value'=>'Restrisiko',
'rs_risk_value'=>'Potentielles Risiko',
'rs_total_impact'=>'Total',

/* './packages/risk/report/popup_report_risk_status_by_area.php' */

'rs_risk_status_per_area'=>'Risiken Status von Bereich',

/* './packages/risk/report/popup_report_risk_status_by_area.xml' */

'rs_area'=>'Bereich',

/* './packages/risk/report/popup_report_risk_status_by_process.php' */

'rs_risk_status_per_process'=>'Risiko Status von Prozess',

/* './packages/risk/report/popup_report_risk_status_by_process.xml' */

'rs_process'=>'Prozess',

/* './packages/risk/report/popup_report_risk_treatment_plan.php' */

'rs_and'=>'und',
'rs_avoided'=>'gelöscht',
'rs_not_treated'=>'nicht behandelt',
'rs_plan_of_risks_treatment'=>'Risiko Aufbereitungsplan',
'rs_reduced'=>'Reduziert',
'rs_restrained'=>'einbehalten',
'rs_transferred'=>'Übertragen',

/* './packages/risk/report/popup_report_risk_treatment_plan.xml' */

'rs_asset_bl_cl'=>'<b>Aktiva:</b>',
'rs_deadline'=>'Stichtag',
'rs_status'=>'Status',

/* './packages/risk/report/popup_report_risk_values.php' */

'rs_risks_values'=>'Risiken Umfang',

/* './packages/risk/report/popup_report_risk_values.xml' */

'rs_affected_asset'=>'Betroffene Aktiva',

/* './packages/risk/report/popup_report_risks.php' */

'mx_risks'=>'Risiken',

/* './packages/risk/report/popup_report_risks_by_area.php' */

'rs_risks_per_area'=>'Risiken von Bereich',

/* './packages/risk/report/popup_report_risks_by_area_and_process_asset.xml' */

'rs_asset_cl'=>'Aktiva:',
'rs_process_cl'=>'Prozess:',

/* './packages/risk/report/popup_report_risks_by_asset.php' */

'rs_risks_per_asset'=>'Risiken von Aktiva',

/* './packages/risk/report/popup_report_risks_by_control.php' */

'rs_report_of_risk_per_control'=>'Risiken von Kontrolle',

/* './packages/risk/report/popup_report_risks_by_control.xml' */

'rs_control_bl_cl'=>'<b>Kontrolle:</b>',

/* './packages/risk/report/popup_report_risks_by_process.php' */

'rs_risks_per_process'=>'Risiken von Prozess',

/* './packages/risk/report/popup_report_risks_by_type.php' */

'rs_risks_by_type'=>'Risiken von Typ',

/* './packages/risk/report/popup_report_risks_by_type.xml' */

'rs_type_cl'=>'Typ:',

/* './packages/risk/report/popup_report_scope.php' */

'rs_sgsi_scope_statement'=>'ERMS\'s Einzelerklärung',

/* './packages/risk/report/popup_report_scope.xml' */

'rs_system_scope'=>'Systemabgrenzung',

/* './packages/risk/report/popup_report_statement_of_applicability.php' */

'rs_applied'=>'beantragt',
'rs_no_risks_application_control'=>'Bis jetzt wurden keine Risiken  identifiziert welche die Anwendung von Kontrolle rechtfertigen',
'rs_not_applied'=>'nicht beantragt',
'rs_reduction_of_risks'=>'Reduzierung von Risiken.',
'rs_statement_of_applicability'=>'Aussage über Anwendbarkeit',
'rs_statement_of_applicability_pre_report'=>'Vorbericht der Aussage über Anwendbarkeit',

/* './packages/risk/report/popup_report_statement_of_applicability.xml' */

'rs_applicability_cl'=>'Anwendbarkeit:',
'rs_best_practice_cl'=>'Gute Beispiele:',
'rs_control'=>'Kontrolle',
'rs_document'=>'Dokument',
'rs_justification_cl'=>'Grund:',

/* './packages/risk/report/popup_report_statement_of_applicability_justificative.xml' */

'tt_justification'=>'Grund',
'vb_close'=>'Geschlossen',
'vb_confirm'=>'Bestätigung',

/* './packages/risk/report/popup_report_summarized_pendant_tasks.php' */

'rs_summarized_pending_tasks'=>'Auflistung schwebender Aufgaben',

/* './packages/risk/report/popup_report_summarized_pendant_tasks.xml' */

'rs_amount'=>'Anzahl',
'rs_tasks'=>'Aufgaben',

/* './packages/risk/report/popup_report_tests.xml' */


/* './packages/risk/report/popup_report_top10_assets_with_more_risks.php' */

'rs_top_10_assets_with_highest_risks'=>'Top 10 Aktiva mit den meisten hohen Risiken',
'rs_top_10_assets_with_most_risks'=>'Top 10 Aktiva mit Risiken',

/* './packages/risk/report/popup_report_top10_assets_with_more_risks.xml' */

'rs_amount_of_risks'=>'Anzahl der Risiken',
'rs_asset'=>'Aktiva',
'rs_responsible'=>'Verantwortlich',

/* './packages/risk/report/popup_report_top10_risks_by_area.php' */

'rs_top_10_risks_per_area'=>'Top 10 Risiken von Bereich',

/* './packages/risk/report/popup_report_top10_risks_by_area.xml' */

'rs_area_bl_cl'=>'<b>Bereich:</b>',

/* './packages/risk/report/popup_report_top10_risks_by_process.php' */

'rs_top_10_risks_per_process'=>'Top 10 Risiken von Prozess',

/* './packages/risk/report/popup_report_top10_risks_by_process.xml' */

'rs_process_bl_cl'=>'<b>Prozess:</b>',
'rs_real_risk'=>'Potentielles Risiko',
'rs_residual_risk'=>'Rest Risiko',
'rs_risk_and_impact'=>'Risiko / Auswirkung',

/* './packages/risk/report/popup_report_user_responsabilities.php' */

'rs_security_resp'=>'Sicherheit Resp.',
'rs_special_user'=>'Spezial Nutzer',
'rs_users_responsibilities'=>'Nutzer antworten',

/* './packages/risk/report/popup_report_user_responsabilities.xml' */

'rs_responsible_bl_cl'=>'<b>Verantwortlich:</b>',

/* './packages/risk/report/report_template.xml' */

'rs_comment_cl'=>'Stellungnahme:',
'rs_end_of_report'=>'Ende des Bericht',

/* './packages/risk/tab_risk_management.xml' */

'ti_area'=>'Bereich',
'ti_asset'=>'Information Aktiva',
'ti_controls'=>'Kontrolle',
'ti_process'=>'Prozess',
'ti_reports'=>'Berichte',
'ti_risk'=>'Risiko',

/* './popup_choose_role.php' */

'mx_user_doesnt_have_permission_to_login'=>'Der Nutzer <b>%user_name%</b> hat keine Erlaubnis zum Betreten des System.',
'tt_login_error'=>'System Login fehlerhaft',

/* './popup_choose_role.xml' */

'lb_represent_bl_cl'=>'<b>Repräsentieren:</b>',
'st_solicitor_explanation'=>'Wenn Sie wünschen',
'tt_solicitor_mode'=>'Anwalts Modus',

/* './popup_email_preferences.xml' */

'cb_action_plan'=>'Handlungsplan',
'cb_areas'=>'Gebiete',
'cb_assets'=>'Aktiva',
'cb_best_practices'=>'Gute Beispiele',
'cb_controls'=>'Kontrollen',
'cb_documents'=>'Dokumente',
'cb_events'=>'Ereignisse',
'cb_incidents'=>'Nebensachverhalte',
'cb_non_conformities'=>'mangelnde Übereinstimmungen',
'cb_occurrences'=>'Vorkommnisse',
'cb_others'=>'Anderes',
'cb_processes'=>'Prozesse',
'cb_risks'=>'Risiken',
'cb_select_all'=>'Alle auswählen',
'cb_standards'=>'Standards',
'lb_warning_types_to_receive_cl'=>'Welche Warnung möchten Sie erhalten:',
'rb_html_email'=>'Html e-Mail',
'rb_messages_digest_complete'=>'Komplett (täglich eine e-Mail mit allen Nachrichten)',
'rb_messages_digest_none'=>'Keines (eine e-Mail pro Nachricht)',
'rb_messages_digest_subjects'=>'Bezogen (täglich eine e-Mail mit fachbezogenen Nachrichten)',
'rb_text_email'=>'Reine Text Email',
'st_email_preferences_bl'=>'<b>E-Mail Vorzüge</b>',
'st_messages_digest_type_cl'=>'Auswahl angezeigter Mitteilungen:',

/* './popup_help.xml' */

'cb_do_not_show_again'=>'Nicht wieder anzeigen',
'st_tip_text'=>'<b>Beim Zugänglichmachen von Minimarisk Tabellen',
'tt_tip_bl'=>'<b>Tip</b>',

/* './popup_password_request.xml' */

'lb_user_login_cl'=>'Nutzer:',
'st_password_request'=>'Zum erhalten eines neuen Passworts',
'st_warning_general_email_disabled'=>'Die System-Email ist zur Zeit nicht verfügbar',
'tt_password_reminder'=>'Neue Passwortanfrage',
'vb_new_password'=>'Neues Passwort',
'wn_password_sent'=>'Das Passwort wurde zum Nutzer gesendet',

/* './popup_risk_limits_task_view.xml' */

'lb_risk_limits_high_cl'=>'Risiko Toleranz 2:',
'lb_risk_limits_low_cl'=>'Risiko Toleranz 1:',
'tt_risk_limits_approval'=>'Genehmigung Risiko Limits',

/* './popup_saas_disclaimer.xml' */

'tt_disclaimer_popup_title'=>'Haftungsausschluss',
'vb_agree'=>'Ich anerkenne',
'vb_disagree'=>'Ich erkenne nicht an',

/* './popup_schedule_edit.xml' */

'lb_april'=>'April',
'lb_august'=>'August',
'lb_december'=>'Dezember',
'lb_february'=>'Februar',
'lb_january'=>'Januar',
'lb_july'=>'Juli',
'lb_june'=>'Juni',
'lb_march'=>'März',
'lb_may'=>'Mai',
'lb_november'=>'November',
'lb_october'=>'Oktober',
'lb_schedule_days_to_finish'=>'Tage zur Aufgabenvervollständigung:',
'lb_schedule_end'=>'Endet am:',
'lb_schedule_periodicity'=>'jeden',
'lb_schedule_start'=>'Beginnt am:',
'lb_schedule_type_cl'=>'Typ:',
'lb_september'=>'September',
'lb_week_days'=>'An diesem Tag:',
'mx_first_week'=>'erster',
'mx_fourth_week'=>'vierter',
'mx_last_week'=>'letzter',
'mx_monday'=>'Montag',
'mx_saturday'=>'Samstag',
'mx_second_week'=>'Sekunde',
'mx_sunday'=>'Sonntag',
'mx_third_week'=>'dritter',
'mx_thursday'=>'Donnerstag',
'mx_tuesday'=>'Dienstag',
'mx_wednesday'=>'Mittwoch',
'si_schedule_by_day'=>'täglich',
'si_schedule_by_month'=>'monatlich',
'si_schedule_by_week'=>'wöchentlich',
'si_schedule_month_day'=>'Monat Tag',
'si_schedule_week_day'=>'Wochentag',
'st_months'=>'Monate',
'st_never'=>'niemals',
'st_schedule_months'=>'Einer der folgenden Monate:',
'st_weeks'=>'Woche(n)',
'tt_schedule_edit'=>'Aufgabenplan',
'tx_1'=>'1',
'tx_schedule_days_to_finish'=>'1',

/* './popup_solicitor_conflict.xml' */

'st_solicitor_conflict'=>'Der Nutzer <b>%solicitor_name%</b> ist von Ihnen ernannt worden',
'tt_solicitor_conflict'=>'Rechtsstreit',

/* './popup_survey.xml' */

'st_survey_text'=>'"Nachdem das System ausgewertet worden ist',
'tt_survey_bl'=>'<b>Gutachten</b>',

/* './popup_unexpected_behavior.xml' */

'st_unexpected_behavior_message'=>'Ein unerwartetes Verhalten im System ist ermittelt worden. Eine E-Mail mit ausführlichen Informationen über das Geschehnis ist unsere technische Unterstützungsmannschaft gesendet worden  Wenn notwendig',
'tt_unexpected_behavior'=>'Unerwartetes Verhalten',

/* './popup_user_change_password.php' */

'st_user_password_change'=>'Sie müssen Ihr Passwort JETZT wechseln.',
'st_user_password_expired'=>'<b>Ihr Passwort ist abgelaufen!</b><br/>Sie müssen es sofort wechseln.',
'st_user_password_will_expire'=>'Ihr Passwort läuft ab in %days% Tage(n). Bitte',

/* './popup_user_change_password.xml' */

'lb_confirm_cl'=>'Bestätigen:',
'tt_change_password'=>'Passwortänderung',
'vb_change_password'=>'Wechsle Password',
'wn_unmatching_password'=>'Das Passwort stimmt nicht überein.',

/* './popup_user_dashboard_preference.php' */

'tt_average_time_for_documents_approval'=>'durchschnittliche Dokumenten- Bewilligungszeit',
'tt_average_time_for_my_documents_approval'=>'Meine mittlere Dokumentenbewilligungszeit',
'tt_financial_summary'=>'Finanzielle Zusammenfassung',
'tt_general_summary_of_risks'=>'Erwartetes ./. Nicht erwartetes Risiko',
'tt_grid_abnormality_ci_title'=>'Kontinuierliche Verbesserungs-Anomalitäten-Zusammenfassung',
'tt_grid_abnormality_pm_title'=>'Regelungen Management Anomalitäten Zusammenfassung',
'tt_grid_incident_summary_title'=>'Vorgangsüberblick',
'tt_grid_incidents_per_asset_title'=>'Top 5 Aktiva mit Vorgängen',
'tt_grid_incidents_per_process_title'=>'Top 5 Prozesse mit Vorgängen',
'tt_grid_incidents_per_user_title'=>'Top 5 Nutzer mit disziplinarischen Prozessen',
'tt_grid_nc_per_process_title'=>'Top 5 Prozesse mit Nichtübereinstimmungen',
'tt_grid_nc_summary_title'=>'Nichtübereinstimmungs Zusammenfassung',
'tt_my_items'=>'Meine Einheiten',
'tt_my_pendencies'=>'Meine offenen Risiko Management',
'tt_my_risk_summary'=>'Meine Risiko Zusammenfassung',
'tt_risk_management_my_items_x_policy_management'=>'Risiko Management (Meine Einheiten) x Regelungs Management',
'tt_risk_management_status'=>'Risiko Level',
'tt_risk_management_x_policy_management'=>'Dokumente per Business Element',
'tt_sgsi_reports_and_documents'=>'ISO 31000 ERMS erforderliche Dokumente',
'tt_summary_of_abnormalities'=>'Risiko Management Anomalitäten Zusammenfassung',
'tt_summary_of_best_practices'=>'Gute Beispiele - Übereinstimmungs Index',
'tt_summary_of_controls'=>'Kontrollüberblick',
'tt_summary_of_my_documents_last_revision_average_time'=>'Letzte Dokumenten Revision Periodische Zusammenfassung',
'tt_summary_of_parameterized_risks'=>'Potentielles Risiko ./. verbleibendes Risiko',
'tt_summary_of_policy_management'=>'Regelungen Management Überblick',
'tt_summary_of_policy_management_my_documents'=>'Regelungs Management (Meine Dokumente) Zusammenfassung',
'tt_summary_of_risk_management_documentation'=>'Zusammenfassung der Risiko Management Dokumentation',
'tt_summary_of_the_risk_management_from_my_items_documentation'=>'Zusammenfassung des Risiko Management meiner Einheiten Dokumentation',
'tt_top_10_most_read_documents'=>'Top 10 meistgelesene Dokumente',
'tt_top_10_most_read_my_documents'=>'Top 10 Meine meistgelesenen Dokumente',

/* './popup_user_dashboard_preference.xml' */

'st_all_elements_not_selected'=>'Alle Einheiten (unsortiert)',
'st_selected_elements_cl'=>'Sortierte Einheiten:',
'tt_summary_preferences_editing'=>'Zusammenfassung Vorzüge schreiben',

/* './popup_user_preferences.php' */

'si_email'=>'Email',
'si_general'=>'General',

/* './popup_user_preferences.xml' */

'lb_preferences_cl'=>'Vorzüge:',
'tt_edit_preferences'=>'Schreibe Vorzüge',

/* './popup_user_profile.xml' */

'cb_change_password'=>'Passwortänderung',
'lb_current_password_cl'=>'Gegenwärtiges Passwort:',
'lb_dashboard_preferences_cl'=>'Anzeigentafel Vorzüge:',
'lb_general_config_bl'=>'<b>Generelle Einstellungen</b>',
'lb_general_config_bl_cl'=>'<b>General Konfiguration:</b>',
'lb_language_cl'=>'Sprache:',
'lb_password_confirm_cl'=>'Bestätigung des Passworts:',
'lb_solicitor_cl'=>'Anwalt:',
'lb_solicitor_finish_date_cl'=>'Ende der Besorgung:',
'lb_solicitor_start_date_cl'=>'Start der Beschaffung:',
'st_change_password_sub_title_bl'=>'<b>Änderung des Passworts</b>',
'st_solicitor_sub_title_bl'=>'<b>Anwalt</b>',
'to_dashboard_user_preferences_help'=>'<b>Konfiguration der Anzeigentafel-Präferenzen:</b><br/><br/>Bestimmen Sie die Aufgabe und welche Art von Informationen auf der Anzeigentafel zu sehen sein sollen.',
'wn_email_incorrect'=>'Ungültige email.',
'wn_existing_password'=>'Das Passwort ist in der derzeitigen Passworttabelle',
'wn_initial_date_after_today'=>'Das Startdatum muss dasselbe oder nach dem aktuellen Datum sein',
'wn_missing_dates'=>'Die Datenfelder sind Vorschrift sofern ein Anwalt ausgewählt wurde.',
'wn_missing_solicitor'=>'Wenn eines der Daten ausgefüllt wurde',
'wn_past_date'=>'Das Abschlussdatum muss gleich dem Tagesdatum oder später sein.',
'wn_wrong_dates'=>'Das Beschaffungsdatum muss später als das Startdatum sein.',

/* './popup_user_search.xml' */

'tt_user_search'=>'Nutzer Suche',
'wn_no_user_selected'=>'Kein ausgewählter Nutzer vorhanden',

/* './reports/ISMSReportTemplate.php' */

'rs_ip_cl'=>'IP:',
'rs_manager_cl'=>'Manager:',

/* './session_expired.xml' */

'st_session_expired'=>'Ihre Sitzung ist abgelaufen. Drücken Sie <b>Ok</b> oder schliessen Sie dieses Fenster und loggen sich neu ein.',
'tt_session_expired'=>'Session abgelaufen',

/* './tab_dashboard.xml' */

'ti_graphics'=>'Bilder',
'ti_statistics'=>'Statistiken',
'ti_summary'=>'Zusammenfassungen',
'ti_warnings'=>'Meine Aufgaben u. Alarme',

/* './tab_main.xml' */

'st_search'=>'Suche',
'ti_continual_improvement'=>'ständige Verbesserung',
'ti_dashboard'=>'Hinweistafel',
'ti_libraries'=>'Bibliotheken',
'ti_policy_management'=>'Richtlinien Management',
'ti_risk_management'=>'Risiko Management',
'ti_search'=>'Suche',
'vb_admin'=>'Admin',
'vb_go'=>'Gehe',

/* './visualize.php' */

'tt_visualize'=>'Zeige',
);
?>