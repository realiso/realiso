<?php
$gaStrings = array(

/* './about_isms.xml' */

'st_about_axur_full'=>'Risk Resilience Sàrl est une entreprise leader de systèmes de gestion des risques. Nous avons nombreux clients présents dans plusieurs secteurs du marché',
'st_about_axur_title'=>'A propos de Risk Resilience Sàrl',
'st_about_isms_full'=>'Minimarisk - Enterprise Risk Management System est une suite pour la gestion des risques operationelles',
'st_about_isms_title'=>'A propos de Minimarisk',
'st_axur_information_security'=>'Risk Resilience Sàrl',
'st_axur_link'=>'www.minimarisk.com',
'st_build'=>'Build',
'st_copyright'=>'Copyright',
'st_information_security_management_system'=>'Enterprise Risk Management System',
'st_version'=>'Version',

/* './access_denied.php' */

'st_license_file_not_found'=>'Fichier de licence<b>%license_name%</b> n\'a pas été trouvé.',

/* './access_denied.xml' */

'tt_access_denied'=>'Accès Refusé',

/* './classes/admin/report/ISMSReportAuditAlertFilter.php' */

'rs_alerts_created_after'=>'Alertes créées après',
'rs_alerts_created_before'=>'Alertes créées avant',

/* './classes/admin/report/ISMSReportAuditLogFilter.php' */

'rs_action_cl'=>'Action:',
'rs_log_after'=>'Journal après',
'rs_log_before'=>'Journal avant',

/* './classes/admin/report/ISMSReportAuditTaskFilter.php' */

'rs_activity_cl'=>'Activité:',
'rs_and_before'=>'et avant',
'rs_any_date'=>'Toute date',
'rs_creation_date_cl'=>'Date de création:',
'rs_receiver_cl'=>'Récepteur:',
'rs_tasks_created_after'=>'Tâches créées après',
'rs_tasks_created_before'=>'Tâches créées avant',
'st_all_users'=>'Tous les utilisateurs',

/* './classes/continual_improvement/CIActionPlan.php' */

'mx_action_plan'=>'Plan d\'Action',
'mx_ap_remove_error_message'=>'Il n\'est pas possible de supprimer le Plan d\'Action. Il y a au moins une Non-Conformité associée à cet objet.',
'mx_ap_remove_error_title'=>'Erreur lors de la suppression du Plan d\'Action',

/* './classes/continual_improvement/CICategory.php' */

'mx_root'=>'Racine',

/* './classes/continual_improvement/CIIncident.php' */

'mx_incident'=>'Incident',

/* './classes/continual_improvement/CIIncidentControl.php' */

'mx_incident_control_association'=>'Association d\'Incident au Contrôle',

/* './classes/continual_improvement/CIIncidentProcess.php' */

'mx_incident_process_association'=>'Association d\'Incident au Processus',

/* './classes/continual_improvement/CINonConformity.php' */

'mx_non_conformity'=>'Non-Conformité',

/* './classes/continual_improvement/CINonConformitySeed.php' */

'mx_inefficient'=>'Inefficace',
'mx_late_efficiency_revision'=>'Révision d\'Efficacité Retardée',
'mx_late_implementation'=>'Mise en Oeuvre en Retard',
'mx_late_test'=>'Test Retardé',
'mx_test_failure'=>'Échec de Test',

/* './classes/continual_improvement/CIOccurrence.php' */

'mx_occurrence'=>'Occurrence',
'mx_occurrence_remove_error'=>'Erreur lors de la suppression d\' Occurrence',
'mx_occurrence_remove_error_message'=>'Il n\'est pas possible de supprimer cette Occurrence. Il y a au moins un incident associé à cet objet.',
'st_incident_module_name'=>'Amélioration Continue',

/* './classes/continual_improvement/CISolution.php' */

'mx_solution'=>'Solution',

/* './classes/continual_improvement/report/ISMSReportDisciplinaryProcessFilter.php' */

'rs_user_cl'=>'Utilisateur:',

/* './classes/continual_improvement/report/ISMSReportElementsFilter.php' */

'lb_elements_cl'=>'Objets:',

/* './classes/continual_improvement/report/ISMSReportIncidentFilter.php' */

'lb_solution_date_cl'=>'Date de Solution:',
'mx_immediate_disposal'=>'Élimination Immédiate',

/* './classes/continual_improvement/report/ISMSReportNonConformityFilter.php' */

'lb_conclusion_date_cl'=>'Date de Finition:',
'mx_after_initialdate'=>'Après %initialDate%',
'mx_before_finaldate'=>'Avant %finalDate%',
'mx_between_initialdate_finaldate'=>'Entre %initialDate% et %finalDate%',
'mx_external_audit'=>'Audit Externe',
'mx_internal_audit'=>'Audit Interne',
'mx_no'=>'Non',
'mx_security_control'=>'Contrôle de Sécurité',
'mx_yes'=>'Oui',

/* './classes/continual_improvement/report/ISMSReportOccurrenceFilter.php' */

'mx_all'=>'Tous',

/* './classes/ISMSActivity.php' */

'wk_action_plan_approval'=>'Approbation de Plan d\'Action',
'wk_action_plan_finish_confirm'=>'Approuver Finition Estimée du Plan d\'Action',
'wk_action_plan_revision'=>'Suivi de Plan d\'Action',
'wk_area_approval'=>'Approuver le Domaine',
'wk_asset_approval'=>'Approuver l\'Actif',
'wk_best_practice_approval'=>'Approuver la Meilleure Pratique',
'wk_best_practice_section_approval'=>'Approuver la Section de Meilleure Pratique',
'wk_category_approval'=>'Approuver la Catégorie',
'wk_control_approval'=>'Approuver le Contrôle',
'wk_control_implementation_approval'=>'Approuver le Contrôle de la Mise en Oeuvre',
'wk_control_test_followup'=>'Surveiller le Test de Contrôle',
'wk_document_approval'=>'Approuver le Document',
'wk_document_revision_followup'=>'Surveiller la Révision de Document',
'wk_document_template_approval'=>'Approuver le Modèle de Document',
'wk_event_approval'=>'Approuver l\'Événement',
'wk_incident_approval'=>'Approuver Incident',
'wk_incident_control_induction'=>'Mesure Provoquée d\'Efficacité de Contrôle',
'wk_incident_disposal_approval'=>'Approbation d\'Élimination Immédiate d\'Incident',
'wk_incident_solution_approval'=>'Approbation de Solution d\'Incident',
'wk_non_conformity_approval'=>'Approbation de la Non-Conformité',
'wk_non_conformity_data_approval'=>'Approbation des Données de Non-Conformité',
'wk_occurrence_approval'=>'Approbation de l\'Occurrence',
'wk_policy_approval'=>'Approuver le Règlement',
'wk_process_approval'=>'Approuver le Processus',
'wk_process_asset_association_approval'=>'Approuver Association de l\'Actif avec le Processus.',
'wk_real_efficiency_followup'=>'Surveiller l\'Efficacité Réelle',
'wk_risk_acceptance_approval'=>'Approuver l\'Acceptation des Risques au-dessus de la valeur',
'wk_risk_acceptance_criteria_approval'=>'Approuver la définition de critères pour l\'Acceptation des Risques',
'wk_risk_approval'=>'Approuver Risque',
'wk_risk_control_association_approval'=>'Approuver l\'Association du Control avec le Risque.',
'wk_risk_tolerance_approval'=>'Approuver Tolérance au Risque',
'wk_scope_approval'=>'Approuver Champ d\'Application',
'wk_standard_approval'=>'Approuver la Norme',

/* './classes/ISMSAuditLog.php' */

'ad_action_bad_login'=>'a essayé de se connecter avec un Mot de Passe non valide',
'ad_action_blocked_login'=>'est bloqué et a essayé de se connecter',
'ad_login'=>'Connecté Au Système',
'ad_login_error'=>'a essayé de se connecter avec un nom d\'utilisateur non valide',
'ad_login_failed'=>'a essayé de se connecter sans autorisation',
'ad_logout'=>'Déconnecté Du Système',
'ad_removed_from_system'=>'Enlevée du système',
'ad_restored'=>'restauré',

/* './classes/ISMSContext.php' */

'ad_log_message'=>'%action% %label%<b>%context_name%</b> by %user_name%.',
'ad_on_behalf_of'=>'au nom de',
'db_default_document_name'=>'%label_context% %context_name% %label_doc%',
'st_denied_permission_to_approve'=>'Vous n\'avez pas l\'autorisation d\'approuver le',
'st_denied_permission_to_delete'=>'Vous n\'avez pas la permission de supprimer le',
'st_denied_permission_to_execute'=>'Vous n\'avez pas l\'autorisation d\'exécuter la tâche.',
'st_denied_permission_to_insert'=>'Vous n\'avez pas la permission d\'ajouter un(e)',
'st_denied_permission_to_read'=>'Vous n\'avez pas la permission d\'afficher le',

/* './classes/ISMSContextObject.php' */

'mx_ap_acceptance_pendant'=>'Plan d\'Action En attente d\'Acceptation',
'mx_approval'=>'En cours d\'Approbation',
'mx_approved'=>'Approuvé',
'mx_closed'=>'Fermé',
'mx_co_approved'=>'Approuvé par les Deux',
'mx_deleted'=>'Supprimé',
'mx_denied'=>'Rejeté',
'mx_directed'=>'Transmis',
'mx_finished'=>'Terminé',
'mx_measured'=>'Mesuré',
'mx_nc_treatment_pendant'=>'Non-Conformité En Attente de Traitement',
'mx_obsolete'=>'Obsolète',
'mx_open'=>'Ouvert',
'mx_pendant'=>'En attente',
'mx_pendant_solution'=>'En Attente - Solution',
'mx_pendant_solved'=>'En Cours - Résolu',
'mx_published'=>'Publié',
'mx_revision'=>'En cours de Révision',
'mx_sent'=>'Délivré',
'mx_solved'=>'Résolu',
'mx_to_be_published'=>'Pour être Publié',
'mx_under_development'=>'En cours de développement',
'mx_waiting_conclusion'=>'En attente de Finition',
'mx_waiting_deadline'=>'Estimation',
'mx_waiting_measurement'=>'En Attente de Mesure',
'mx_waiting_solution'=>'En Attente de Solution',

/* './classes/ISMSEmailTemplates.php' */

'em_activity'=>'Activité',
'em_alert_messages_sent'=>'<b>Cher utilisateur Minimarisk',
'em_asset'=>'Actif',
'em_at'=>'à',
'em_click_here_to_access_the_system'=>'Accéder Minimarisk',
'em_client'=>'Client',
'em_control'=>'Contrôle',
'em_control_has_deadline'=>'<b>Cher utilisateur Minimarisk',
'em_created_on'=>'Créé le',
'em_description'=>'Description',
'em_disciplinary_process'=>'<b>Cher Gérant de Processus Disciplinaires',
'em_execute_task'=>'<b>Cher utilisateur Minimarisk',
'em_feedback'=>'Commentaire',
'em_feedback_type'=>'Type de Commentaire',
'em_incident'=>'Incident',
'em_incident_risk_parametrization_message'=>'<b>Cher utilisateur Minimarisk',
'em_isms_feedback_msg'=>'Un utilisateur a écrit un commentaire sur Minimarisk.',
'em_isms_new_user_msg'=>'Vous avez été ajouté au Minimarisk. Ci-dessous vous pouvez voir les données pour l\'accès au système:',
'em_limit'=>'Limite',
'em_login'=>'Connexion',
'em_new_parameters'=>'Nouveaux Paramètres',
'em_new_password_generated'=>'<b>Cher utilisateur Minimarisk',
'em_new_password_request_ignore'=>'Si vous n\'avez pas demandé un nouveau Mot de Passe',
'em_observation'=>'Observation',
'em_original_parameters'=>'Paramètres par défaut',
'em_password'=>'Mot de Passe',
'em_risk'=>'Risque',
'em_saas_abandoned_system_msg'=>'<b>Le Minimarisk du client ci-dessous a été abandonnée.</b>',
'em_sender'=>'Expéditeur',
'em_sent_on'=>'envoyé le',
'em_tasks_and_alerts_transferred'=>'<b>Cher utilisateur Minimarisk',
'em_tasks_and_alerts_transferred_message'=>'L\'utilisateur ci-dessus a été retiré du système',
'em_user'=>'Utilisateur',
'em_users'=>'Utilisateurs',
'link'=>'Lien',

/* './classes/ISMSLib.php' */

'em_license_expired'=>'La Licence de Client<b>%client_name%</b>a expiré le<b>%expiracy%</b>',
'em_subject_license_expired'=>'Licence Expirée',
'mx_asset_manager'=>'Propriétaire d\'Actif',
'mx_chairman'=>'Direction',
'mx_chinese'=>'Chinois (Beta)',
'mx_control_manager'=>'Propriétaire de Contrôle',
'mx_created_by'=>'Créé par:',
'mx_day'=>'jour',
'mx_days'=>'jours',
'mx_english'=>'Anglais',
'mx_evidence_manager'=>'Gérant d\'Évidence',
'mx_french'=>'Français',
'mx_german'=>'Deutsch',
'mx_incident_manager'=>'Gérant d\'Incident',
'mx_library_manager'=>'Propriétaire de Bibliothèque',
'mx_modified_by'=>'Modifié par:',
'mx_month'=>'mois',
'mx_months'=>'mois',
'mx_non_conformity_manager'=>'Gestion des Non-Conformités',
'mx_on'=>'le',
'mx_portuguese'=>'Portugais',
'mx_process_manager'=>'Gestion des Processus Disciplinaires',
'mx_shortdate_format'=>'%m/%d/%Y',
'mx_spanish'=>'Español (Beta!)',
'mx_week'=>'semaine',
'mx_weeks'=>'semaines',
'st_denied_permission_to_access_incident_module'=>'Vous n\'avez pas la permission d\'accéder à l\'Établi d\'Amélioration Continue.<br/><br/>Cette Licence n\'inclue pas cette permission!',
'st_denied_permission_to_access_policy_module'=>'Vous n\'avez pas la permission d\'accéder au module de la Gestion des Règlements<br/><br/>vous utilisez une licence qui ne vous permet pas d\'accéder à ce module!',

/* './classes/ISMSLicense.php' */

'mx_commercial'=>'Commercial',
'mx_trial'=>'Essai',

/* './classes/ISMSMailer.php' */

'em_to_execute_task_check_activity_list'=>'<b>Pour exécuter cette tâche',

/* './classes/ISMSPasswordPolicy.php' */

'mx_minimum_chars'=>'Longueur minimum',
'mx_minimum_numeric_chars'=>'Nombre minimal de caractères numériques',
'mx_minimum_special_chars'=>'Nombre minimal de caractères spéciaux',
'mx_password_policy'=>'<b>Règlements de Mot de Passe:</b><br/>',
'mx_upper_and_lower_case_chars'=>'Caractères majuscules et minuscules',

/* './classes/ISMSPolicy.php' */

'mx_policy'=>'Règlement',

/* './classes/ISMSProfile.php' */

'mx_profile'=>'Profil',
'st_profile_remove_error'=>'Impossible de supprimer le profil<b>%profile_name%</b>: Il y a des utilisateurs associés à ce profil.',
'tt_profile_remove_error'=>'Erreur lors de la suppression du profil',

/* './classes/ISMSScope.php' */

'mx_scope'=>'Champ d\'Application',

/* './classes/ISMSSession.php' */

'ad_login_message'=>'%label_user%<b>%user_name%</b> %action%.',

/* './classes/ISMSUser.php' */

'mx_user'=>'Utilisateur',
'st_admim_module_name'=>'Admin',
'st_user_remove_error'=>'Il n\'est pas possible de supprimer l\'utilisateur<b>%user_name%</b>: L\'utilisateur est responsable d\'un ou de plusieurs objets du système ou est un utilisateur spécial.',
'tt_user_remove_error'=>'Erreur lors de la suppression de l\'Utilisateur',

/* './classes/nonauto/admin/ISMSDefaultConfig.php' */

'db_area_priority_high'=>'3 - Haute',
'db_area_priority_low'=>'1 - Faible',
'db_area_priority_middle'=>'2 - Moyenne',
'db_area_type_factory_unit'=>'Fabrique',
'db_area_type_filial'=>'Filiale',
'db_area_type_matrix'=>'Siège Social',
'db_area_type_regional_office'=>'Bureau Régional',
'db_control_cost_hardware'=>'Hardware',
'db_control_cost_material'=>'Matériaux',
'db_control_cost_others'=>'Education',
'db_control_cost_pessoal'=>'Gens',
'db_control_cost_software'=>'Software',
'db_control_type_based_on_risk_analize'=>'Basé sur l\'Analyse des Risques',
'db_control_type_laws_and_regulamentation'=>'Problème de Conformité',
'db_control_type_others'=>'Autres',
'db_document_type_1'=>'Confidentiel',
'db_document_type_2'=>'Restreindre',
'db_document_type_3'=>'Institutionnel',
'db_document_type_4'=>'Public',
'db_impact_1'=>'Très faible',
'db_impact_2'=>'Faible',
'db_impact_3'=>'Moyen',
'db_impact_4'=>'Élevé',
'db_impact_5'=>'Très Élevé',
'db_impact3_1'=>'Faible',
'db_impact3_2'=>'Moyen',
'db_impact3_3'=>'Élevé',
'db_importance_1'=>'Négligeable',
'db_importance_2'=>'Faible',
'db_importance_3'=>'Moyenne',
'db_importance_4'=>'Haute',
'db_importance_5'=>'Critique',
'db_importance3_1'=>'Faible',
'db_importance3_2'=>'Moyenne',
'db_importance3_3'=>'Haute',
'db_incident_cost_aplicable_penalty'=>'Amende',
'db_incident_cost_direct_lost'=>'Perte Directe',
'db_incident_cost_enviroment'=>'Récupération de l\'Environnement',
'db_incident_cost_indirect_lost'=>'Perte Indirecte',
'db_process_priority_high'=>'3 - Haute',
'db_process_priority_low'=>'1 - Faible',
'db_process_priority_middle'=>'2 - Moyenne',
'db_process_type_4'=>'Les ressources humaines',
'db_process_type_5'=>'Technologies de l\'information',
'db_process_type_6'=>'Finances et comptabilité',
'db_process_type_7'=>'Environnement',
'db_process_type_8'=>'Amélioration de la Performance',
'db_process_type_administrative'=>'Marché et clients',
'db_process_type_operational'=>'Le développement de nouveaux produits',
'db_process_type_support'=>'Production et livraison',
'db_rcimpact_0'=>'Ne concerne pas',
'db_rcimpact_1'=>'Diminution de Un Niveau',
'db_rcimpact_2'=>'Diminution de Deux Niveaux',
'db_rcimpact_3'=>'Diminution de Trois Niveaux',
'db_rcimpact_4'=>'Diminution de Quatre Niveaux',
'db_rcimpact3_0'=>'Ne concerne pas',
'db_rcimpact3_1'=>'Diminution de Un Niveau',
'db_rcimpact3_2'=>'Diminution de Deux Niveaux',
'db_rcprob_0'=>'Ne concerne pas',
'db_rcprob_1'=>'Diminution de Un Niveau',
'db_rcprob_2'=>'Diminution de Deux Niveaux',
'db_rcprob_3'=>'Diminution de Trois Niveaux',
'db_rcprob_4'=>'Diminution de Quatre Niveaux',
'db_rcprob3_0'=>'Ne concerne pas',
'db_rcprob3_1'=>'Diminution de Un Niveau',
'db_rcprob3_2'=>'Diminution de Deux Niveaux',
'db_register_type_1'=>'Confidentiel',
'db_register_type_2'=>'Restreindre',
'db_register_type_3'=>'Institutionnel',
'db_register_type_4'=>'Public',
'db_risk_confidenciality'=>'Confidentialité',
'db_risk_disponibility'=>'Disponibilité',
'db_risk_intregrity'=>'Intégrité',
'db_risk_type_administrative'=>'Administratif',
'db_risk_type_fisic'=>'Physique',
'db_risk_type_tecnologic'=>'Technologique',
'db_risk_type_threat'=>'Danger',
'db_risk_type_vulnerability'=>'Vulnérabilité',
'db_risk_type_vulnerability_x_threat'=>'Event (Vulnérabilité x Danger)',
'db_riskprob_1'=>'Rare (1% - 20%)',
'db_riskprob_2'=>'Peu Probable (21% - 40%)',
'db_riskprob_3'=>'Modéré (41% - 60%)',
'db_riskprob_4'=>'Probable (61% - 80%)',
'db_riskprob_5'=>'Quasi Certain (81% - 99%)',
'db_riskprob3_1'=>'Peu Probable',
'db_riskprob3_2'=>'Modéré',
'db_riskprob3_3'=>'Probable',

/* './classes/nonauto/FeedbackEvent.php' */

'em_isms_feedback'=>'Commentaires sur Minimarisk',

/* './classes/nonauto/policy_management/DocumentsDrawGrid.php' */

'to_future_publication'=>'Le document ne sera disponible que pour les lecteurs à la date% date%',

/* './classes/policy_management/PMDocInstance.php' */

'mx_document_instance'=>'Échantillon de Document',

/* './classes/policy_management/PMDocument.php' */

'mx_document'=>'Document',
'st_denied_permission_to_manage_document'=>'Vous n\'avez pas la permission de gérer ce document.',
'st_document_remove_error'=>'"Impossible de supprimer le document<b>%name%</b>. Le Document contient des sous-documents. Pour supprimer les documents qui contiennent des sous-documents',
'st_policy_management'=>'Gestion des Règlements',
'tt_document_remove_error'=>'Erreur lors de la suppression du document.',

/* './classes/policy_management/PMRegister.php' */

'mx_day_days'=>'jour(s)',
'mx_month_months'=>'Mois',
'mx_register'=>'Registre',
'mx_week_weeks'=>'semaine(s)',

/* './classes/policy_management/PMTemplate.php' */

'mx_document_template'=>'Modèle de Document',

/* './classes/policy_management/report/ISMSReportAccessedDocumentsFilter.php' */

'rs_users'=>'Utilisateurs',

/* './classes/policy_management/report/ISMSReportDocsWithHighFrequencyRevisionFilter.php' */

'report_filter_all_userid'=>'tous',
'report_filter_high_frequency_revision_definition'=>'Une Haute fréquence de révision est:',
'report_filter_high_frequency_revision_definition_explanation'=>'plus d\'une révision dans un délai d\'une semaine.',

/* './classes/policy_management/report/ISMSReportDocumentAccessesFilter.php' */

'rs_accesses'=>'Accès',
'rs_only_last'=>'Seulement le dernier',
'rs_show_all'=>'Afficher tous les',

/* './classes/policy_management/report/ISMSReportDocumentDatesFilter.php' */

'rs_after_initialdate'=>'Après %initialDate%',
'rs_before_finaldate'=>'Avant %finalDate%',
'rs_between_initialfinaldate'=>'Entre %initialDate% et %finalDate%',
'rs_creation_date'=>'Date de création',
'rs_last_revision_date'=>'Dernière date de révision',

/* './classes/policy_management/report/ISMSReportDocumentsByComponentFilter.php' */

'report_filter_all_doctype'=>'Tous',
'report_filter_doctype_cl'=>'Type de Document:',

/* './classes/policy_management/report/ISMSReportDocumentsByStateFilter.php' */

'report_filter_showregisterdocs_cl'=>'Afficher les Documents du Registre:',
'report_filter_status_cl'=>'État des Documents',
'rs_all_status'=>'Tous',

/* './classes/policy_management/report/ISMSReportDocumentSummary.php' */

'rs_approvers_list_section'=>'Liste d\'Approbateurs',
'rs_author'=>'Auteur',
'rs_components_list_section'=>'Liste des Composants',
'rs_document_type_management'=>'Gestion',
'rs_documents_type_others'=>'Autres',
'rs_file'=>'Fichier',
'rs_general_info_section'=>'Informations Générales',
'rs_keywords'=>'Mots Clés',
'rs_link'=>'Lien',
'rs_no_approvers_message'=>'Ce document n\'a pas de approbateurs.',
'rs_no_components_message'=>'Ce document n\'a pas de composants.',
'rs_no_readers_message'=>'Ce document n\'a pas de lecteurs.',
'rs_no_references_message'=>'Ce document n\'a pas de références.',
'rs_no_registers_message'=>'Ce document n\'a pas de registres.',
'rs_no_subdocs_message'=>'Ce document n\'a pas de sous-documents.',
'rs_published_date'=>'Publier dans',
'rs_readers_list_section'=>'Liste des lecteurs',
'rs_references_list_section'=>'Liste des Références',
'rs_registers_list_section'=>'Liste des REGISTRES',
'rs_report_not_supported'=>'RAPPORT N\'EST PAS SUPPORTÉ DANS CE FORMAT',
'rs_subdocs_list_section'=>'Liste des sous-documents',

/* './classes/policy_management/report/ISMSReportDocumentsWithoutRegisterFilter.php' */

'report_filter_all_document_responsible'=>'tous',
'report_filter_document_responsible_cl'=>'Responsable de la Documentation:',

/* './classes/policy_management/report/ISMSReportNotAccessedDocumentsFilter.php' */

'rs_expand_readers'=>'Développer les lecteurs',

/* './classes/policy_management/report/ISMSReportTopRevisedDocumentsFilter.php' */

'rs_expand_documents'=>'Développer les documents',
'rs_show_only_first_cl'=>'Afficher seulement le premier:',

/* './classes/policy_management/report/ISMSReportUserCommentsFilter.php' */

'rs_all_documents'=>'tous',
'rs_documents_cl'=>'Documents:',

/* './classes/policy_management/report/ISMSReportUsersByProcessFilter.php' */

'rs_no'=>'Non',
'rs_organize_by_area'=>'Organiser par domaine',
'rs_yes'=>'Oui',

/* './classes/policy_management/report/ISMSReportUsersWithPendantReadFilter.php' */

'report_filter_all_document_reader_cl'=>'tous',
'report_filter_document_reader_cl'=>'Lecteurs de Documents',

/* './classes/risk_management/report/ISMSReportBestPracticesFilter.php' */

'rs_only_applied'=>'Seulement Appliqué',

/* './classes/risk_management/report/ISMSReportConformityFilter.php' */

'rs_all'=>'Tous',
'rs_standard_filter_cl'=>'Filtre de Norme:',

/* './classes/risk_management/report/ISMSReportControlCostByCostCategoryFilter.php' */

'rs_costcategory_cl'=>'Catégorie:',

/* './classes/risk_management/report/ISMSReportControlPlanningFilter.php' */

'rs_control_types_cl'=>'Types de Contrôle:',

/* './classes/risk_management/report/ISMSReportControlsFilter.php' */

'rs_delayed'=>'Retardé',
'rs_implementationstatus_cl'=>'Afficher Seulement:',
'rs_not_trusted_controls'=>'Contrôles non-fiables',
'rs_trusted_and_efficient_controls'=>'Contrôles fiables et efficaces',
'rs_under_implementation'=>'En cours de mise en oeuvre',

/* './classes/risk_management/report/ISMSReportEventsByCategoryFilter.php' */

'rs_category_cl'=>'Catégorie:',
'rs_event_types_cl'=>'Type d\'Événement:',

/* './classes/risk_management/report/ISMSReportGenericClassifTypePrioFilter.php' */

'lb_event_types_cl'=>'Types d\'Événement:',

/* './classes/risk_management/report/ISMSReportParametersFilter.php' */

'rs_considered_parameters_cl'=>'Paramètres Considérés:',

/* './classes/risk_management/report/ISMSReportProcessesByPriority.php' */

'rs_undefined_priority'=>'Priorité Indéfinie',

/* './classes/risk_management/report/ISMSReportRiskFinancialImpactFilter.php' */

'mx_accepted'=>'Accepté',
'mx_avoided'=>'Évité',
'mx_not_treated'=>'Non Traité',
'mx_reduced'=>'Réduction',
'mx_transferred'=>'Transféré',
'mx_treated'=>'Traité',
'rs_not_treated_or_accepted'=>'Non traité ou accepté',
'rs_treatment_cl'=>'Traitement:',

/* './classes/risk_management/report/ISMSReportRiskImpactFilter.php' */

'lb_considered_parameters_cl'=>'Paramètres Considérés:',
'lb_risk_types_cl'=>'Types de Risque:',

/* './classes/risk_management/report/ISMSReportRisksByAreaFilter.php' */

'lb_area_priorities_cl'=>'Domaine:',
'lb_area_types_cl'=>'Types de Domaine:',
'rs_area_cl'=>'Domaine:',

/* './classes/risk_management/report/ISMSReportRisksByProcessFilter.php' */

'lb_process_priorities_cl'=>'Priorités de Processus:',
'lb_process_types_cl'=>'Types de Processus:',
'st_all'=>'Tous',

/* './classes/risk_management/report/ISMSReportRisksByType.php' */

'rs_undefined_type'=>'Type Indéfini',

/* './classes/risk_management/report/ISMSReportRisksFilter.php' */

'rs_estimated_risks'=>'Risques Estimés',
'rs_not_estimated'=>'Non Estimé',
'rs_not_estimated_risks'=>'Risques Non-Estimés',
'rs_not_treated_risks'=>'risques non traités',
'rs_residual_value_cl'=>'Risque Résiduel:',
'rs_show_only_cl'=>'Afficher Seulement:',
'rs_treated_risks'=>'risques traités',
'rs_value_cl'=>'Potentiel de Risque:',

/* './classes/risk_management/report/ISMSReportRiskValuesFilter.php' */

'rs_high'=>'Élevé',
'rs_low'=>'Faible',
'rs_medium'=>'Moyen',
'rs_not_parameterized'=>'Non Estimé',
'rs_risk_color_based_on_cl'=>'Couleur de Risque basée sur:',
'rs_risk_types_cl'=>'Types de Risque:',
'rs_risks_values_cl'=>'Valeurs de Risque:',

/* './classes/risk_management/report/ISMSReportStatementOfApplicabilityFilter.php' */

'lb_control_types_cl'=>'Types de Contrôle:',

/* './classes/risk_management/report/ISMSReportTop10AssetsWithMoreRisksFilter.php' */

'lb_risk_types'=>'Types de Risque',

/* './classes/risk_management/riskparameters/RiskParameters.php' */

'mx_justification_bl'=>'Raison',
'mx_justifying'=>'Justifier',

/* './classes/risk_management/RMAcceptRisk.php' */

'mx_risk_acceptance'=>'Acceptation de Risque',

/* './classes/risk_management/RMArea.php' */

'mx_area'=>'Domaine',
'st_area_remove_error_message'=>'"Impossible de supprimer le Domaine d\'Activité<b>%area_name%</b>: Le Domaine contient des sous-domaines et/ou processus. Pour supprimer un Domaine d\'Activité qui contient des sous-domaines ou de processus',
'tt_area_remove_erorr'=>'Erreur lors de la suppression du Domaine d\'Activité',

/* './classes/risk_management/RMAsset.php' */

'mx_asset'=>'Actif',
'st_asset_remove_error_message'=>'"Impossible de supprimer l\'Actif<b>%asset_name%</b>: Il y a des Risques associés à l\'Actif. Pour supprimer des Actifs auxquels sont associés des Risques',
'tt_asset_remove_error'=>'Erreur lors de la suppression de l\'Actif',

/* './classes/risk_management/RMBestPractice.php' */

'mx_best_practice'=>'Meilleure Pratique',
'st_best_practice_remove_error_message'=>'"Il n\'était pas possible de supprimer la Meilleures Pratique<b>%best_practice_name%</b>: La Meilleure Pratique est associée à un ou plusieurs des Contrôles. Pour supprimer une des Meilleures Pratiques qui sont associées à un ou plusieurs Contrôles',
'tt_best_practice_remove_error'=>'Erreur lors de la suppression de Meilleure Pratique',

/* './classes/risk_management/RMBestPracticeEvent.php' */

'mx_best_practice_event_association'=>'Association de Meilleure Pratique à l\'Événement',

/* './classes/risk_management/RMBestPracticeStandard.php' */

'mx_best_practice_standard_association'=>'Association de la Meilleure Pratique à la Norme',

/* './classes/risk_management/RMCategory.php' */

'mx_category'=>'Catégorie',
'st_category_remove_error_message'=>'"Il n\'était pas possible de supprimer la Catégorie<b>%category_name%</b>:La Catégorie contient des Sous-Catégories et/ou des Événements et/ou des Actifs. Afin de supprimer les Catégories qui contiennent des Sous-Catégories',

/* './classes/risk_management/RMControl.php' */

'mx_control'=>'Contrôle',
'st_rm_module_name'=>'Gestion des Risques',

/* './classes/risk_management/RMControlBestPractice.php' */

'mx_control_best_practice_association'=>'Association des Meilleures Pratiques au Contrôle',

/* './classes/risk_management/RMControlImplementationAccept.php' */

'mx_control_implementation_acceptance'=>'Acceptation de Mise en Oeuvre de Contrôle',

/* './classes/risk_management/RMEvent.php' */

'mx_event'=>'Événement',

/* './classes/risk_management/RMProcess.php' */

'mx_process'=>'Processus',

/* './classes/risk_management/RMProcessAsset.php' */

'mx_process_asset_association'=>'Association d\'Actifs à des Processus',

/* './classes/risk_management/RMRisk.php' */

'mx_risk'=>'Risque',

/* './classes/risk_management/RMRiskControl.php' */

'mx_risk_control_association'=>'L\'Association de Contrôle au Risque',

/* './classes/risk_management/RMRiskLimits.php' */

'mx_risk_tolerance'=>'Tolérance au Risque',

/* './classes/risk_management/RMSectionBestPractice.php' */

'mx_best_practice_section'=>'Section Meilleure Pratique',
'st_home'=>'Racine',
'st_section_control_best_practice_remove_error_message'=>'Impossible de supprimer la Section<b>%section_name%</b>: Section contient des Meilleures Pratiques et/ou des Sous-Sections qui contiennent des Meilleures Pratiques liées aux Contrôles.',
'st_section_remove_error_message'=>'"Impossible de supprimer la Section<b>%section_name%</b>: Section contient des Meilleures Pratiques et/ou des Sous-Sections. Pour supprimer les Sections qui contiennent des Sous-Sections ou des Meilleures Pratiques',
'tt_section_remove_error'=>'Erreur lors de la suppression de la Section',

/* './classes/risk_management/RMStandard.php' */

'mx_standard'=>'Standard',
'st_libraries_module_name'=>'Bibliothèques',
'st_remove_standard_error'=>'Il n\'est pas possible de supprimer la norme<b>%standard_name%</b> car elle est liée à de meilleures pratiques.',
'tt_remove_standard_error'=>'Erreur lors de la suppression de la Norme',

/* './classes/WKFAlert.php' */

'em_alert_check_list'=>'<b>Afin de visualiser l\'alerte',
'wk_ap_conclusion'=>'Le Plan d\'Action %name% a été fini',
'wk_ap_efficiency_revision_late'=>'La révision de l\'efficacité du Plan d\'Action \'%name%\' a expiré.',
'wk_ap_efficiency_revision_near'=>'La date de révision de l\'efficacité du Plan d\'Action \'%name%\' s\'approche.',
'wk_approved'=>'a été approuvé.',
'wk_asset_np_risk_association'=>'Un risque non-estimé a été associé à l\'actif \'%asset%\' de l\'événement proposé \'%event%\'.',
'wk_control_implementation'=>'Contrôle \'%name%\' doit être mis en oeuvre.',
'wk_control_test_late'=>'Le Suivi du Test de Contrôle \'%name%\'  est en retard.',
'wk_deadline_close'=>'La date limite pour l\'approbation du document \'%name%\'\'  approche.',
'wk_deadline_expired'=>'La date limite pour l\'approbation du document \'%name%\' a expiré.',
'wk_delegated'=>'a été délégué à vous.',
'wk_denied'=>'a été rejeté.',
'wk_disciplinary_process'=>'L\'utilisateur(s) %users% a/ont été joints au processus disciplinaire de l\'incident %name%.',
'wk_document_comment'=>'Le Document \'%name%\' a reçu un commentaire.',
'wk_document_is_now_published'=>'Document \'%name%\' a été publié.',
'wk_document_new_version'=>'Le Document \'%name%\' a une nouvelle version.',
'wk_document_publication'=>'Le document \'%name%\' a été envoyé à la publication.',
'wk_document_revision'=>'Le Document \'%name%\' a été envoyée pour révision',
'wk_document_revision_late'=>'La révision du document \'%name%\' est retardé.',
'wk_implementation_expired'=>'La Mise en Oeuvre de Contrôle \'%name%\' a expiré.',
'wk_inc_asset'=>'L\'incident \'%incident%\' a été lié à l\'actif \'%asset%\'.',
'wk_inc_control'=>'L\'incident \'%incident%\' a été lié au contrôle \'%control%\'.',
'wk_inc_disposal_app'=>'L\'élimination immédiate de l\'incident %name% a été approuvée.',
'wk_inc_disposal_denied'=>'L\'élimination immédiate de l\'incident %name% a été rejetée.',
'wk_inc_evidence_required'=>'Processus de Collection d\'Évidence requis pour l\'incident \'%name%\'.',
'wk_inc_sent_to_responsible'=>'L\'incident \'%name%\' vous a été transmis.',
'wk_inc_solution_app'=>'La solution de l\'incident %name% a été approuvée.',
'wk_inc_solution_denied'=>'La solution de l\'incident %name% a été refusée.',
'wk_incident'=>'L\'incident \'%name%\' a été créé.',
'wk_incident_risk_parametrization'=>'Le risque %risk_name% a été associé à l\'actif %asset_name%',
'wk_non_conformity_waiting_conclusion'=>'Non-Conformité %name% en attente de finition.',
'wk_non_conformity_waiting_deadline'=>'Non-Conformité %name% en attente de délai.',
'wk_pre_approved'=>'a été pré-approuvé.',
'wk_probability_update_down'=>'La fréquence des incidents associés au risque \'%name%\'',
'wk_probability_update_up'=>'La fréquence des incidents associés au risque \'%name%\'',
'wk_real_efficiency_late'=>'Le Suivi de l\'Efficacité Réelle du Contrôle \'%name%\' est en retard.',
'wk_risk_created'=>'Le risque non-estimé \'%risk%\' a été associé à l\'actif \'%asset%\'.',
'wk_warning'=>'Avertissement',

/* './classes/WKFSchedule.php' */

'mx_last'=>'dernière',
'mx_second_last'=>'avant dernière',
'mx_second_wee'=>'deuxième semaine',
'st_schedule_and'=>'et',
'st_schedule_april'=>'Avril',
'st_schedule_august'=>'Août',
'st_schedule_daily'=>'tous les jours',
'st_schedule_day_by_month'=>'le jour %day%',
'st_schedule_day_by_month_inverse'=>'le %day% jour',
'st_schedule_day_by_week'=>'le %week% %week_day%',
'st_schedule_december'=>'Décembre',
'st_schedule_each_n_days'=>'chaque %periodicity% jours',
'st_schedule_each_n_months'=>'tous les %periodicity% mois',
'st_schedule_each_n_weeks'=>'les %week_days%',
'st_schedule_endless_interval'=>'Du %start%',
'st_schedule_february'=>'Février',
'st_schedule_friday_pl'=>'les Vendredis',
'st_schedule_interval'=>'Entre %start% et %end%',
'st_schedule_january'=>'Janvier',
'st_schedule_july'=>'Juillet',
'st_schedule_june'=>'Juin',
'st_schedule_march'=>'Mars',
'st_schedule_may'=>'Mai',
'st_schedule_monday_pl'=>'les Lundis',
'st_schedule_monthly'=>'%day_expression% de chaque mois',
'st_schedule_november'=>'Novembre',
'st_schedule_october'=>'Octobre',
'st_schedule_saturday_pl'=>'les Samedis',
'st_schedule_september'=>'Septembre',
'st_schedule_specified_months'=>'%day_expression% de %months%',
'st_schedule_sunday_pl'=>'les Dimanches',
'st_schedule_thursday_pl'=>'les Jeudis',
'st_schedule_tuesday_pl'=>'les Mardis',
'st_schedule_wednesday_pl'=>'les Mercredis',
'st_schedule_weekly'=>'tous les %week_days%',

/* './classes/WKFTask.php' */

'wk_task'=>'Tâche',
'wk_task_execution_permission'=>'Vous n\'avez pas l\'autorisation d\'exécuter la tâche.',

/* './crontab.php' */

'em_saas_abandoned_system'=>'Système Abandonné',

/* './damaged_system.php' */

'st_system_corrupted'=>'Votre système est peut-être endommagé. S\'il vous plaît',

/* './damaged_system.xml' */

'tt_damaged_system'=>'Système Endommagé',

/* './email_sender.php' */

'em_daily_digest'=>'Sommaire Quotidien',

/* './graphs/ISMSGraphAPActionType.php' */

'mx_corrective_action'=>'Mesures Correctives',
'mx_preventive_action'=>'Action Préventive',

/* './graphs/ISMSGraphAssetCost.php' */

'st_assets_cost_above_1m'=>'Actif avec des valeurs au-dessus de $1.000.000',
'st_assets_cost_below_100k'=>'Actif avec des valeurs en dessous de $100.000',
'st_assets_cost_between_1m_and_500k'=>'Actifs avec des valeurs comprises entre $1.000.000 et $500',
'st_assets_cost_between_500k_and_100k'=>'Actifs avec des valeurs comprises entre $500',

/* './graphs/ISMSGraphAssetEstimatedCost.php' */

'st_assets_with_estimated_cost'=>'Actifs avec Coût Estimé',
'st_assets_without_estimated_cost'=>'Actifs sans coût estimé',

/* './graphs/ISMSGraphControlEfficiency.php' */

'mx_expected_efficiency'=>'Efficacité Attendue',
'mx_real_efficiency'=>'Efficacité Réelle',

/* './graphs/ISMSGraphControlTest.php' */

'mx_test_not_ok'=>'Test Pas OK',
'mx_test_ok'=>'Test OK',
'mx_test_value'=>'Valeur de Test',

/* './graphs/ISMSGraphDocumentsAccessed.php' */

'st_accessed_documents'=>'Documents Consultés',
'st_accessed_documents_cumulative'=>'Documents Consultés - Nombre Cumulatif',

/* './graphs/ISMSGraphDocumentsCreated.php' */

'st_created_documents'=>'Documents Créés',
'st_created_documents_cumulative'=>'Documents Créés -  Nombre Cumulatif',

/* './graphs/ISMSGraphDocumentsType.php' */

'mx_management'=>'Gestion',
'mx_others'=>'Autres',
'mx_without_type'=>'Pas de Type',

/* './graphs/ISMSGraphIncidentCategory.php' */

'st_others'=>'Autres',

/* './graphs/ISMSGraphIncidentLossType.php' */

'mx_direct_losses'=>'Pertes Directes',
'mx_indirect_losses'=>'Pertes Indirectes',

/* './graphs/ISMSGraphPotentialRisk.php' */

'st_high_risks'=>'Risques Élevés',
'st_low_risks'=>'Risques Faibles',
'st_medium_risks'=>'Risques Moyens',
'st_non_parameterized_risks'=>'Risques Non-Estimés',

/* './graphs/ISMSGraphResidualRisk.php' */

'st_high_risk'=>'Risque Élevé',
'st_low_risk'=>'Risque Faible',
'st_medium_risk'=>'Risque Moyen',
'st_non_parameterized_risk'=>'Risque Non-Estimé',

/* './graphs/ISMSGraphTreatedRisk.php' */

'st_avoided_risk'=>'Risque Évité',
'st_risk_accepted_being_yellow_or_red'=>'Risque accepté étant moyen ou élevé',
'st_risk_accepted_for_being_green'=>'Risque accepté car faible',
'st_risk_treated_with_user_of_control'=>'Risque traité avec l\'utilisation du contrôle',
'st_transferred_risk'=>'Risque Transféré',

/* './handlers/QueryCountDocsTimeToApprove.php' */

'gs_day'=>'jour',
'gs_days'=>'jours',
'gs_hour'=>'heure',
'gs_hours'=>'heures',
'gs_minute'=>'minute',
'gs_minutes'=>'minutes',
'gs_not_applicable'=>'NA',
'gs_second'=>'deuxième',
'gs_seconds'=>'secondes',

/* './invalid_activation_code.php' */

'st_invalid_activation_code'=>'Le code d\'activation utilisée est invalide. S\'il vous plaît',

/* './invalid_activation_code.xml' */

'tt_invalid_activation_code'=>'Code d\'Activation Invalide',

/* './license_corrupted.xml' */

'st_license_corrupted_message'=>'Votre système utilise une licence invalide ou qui a été endommagée.',
'tt_license_corrupted'=>'Licence Invalide ou endommagée',

/* './license_expired.php' */

'st_license_expired'=>'Votre licence a expiré le<b>%expiracy_str%</b>.',

/* './license_expired.xml' */

'tt_license_expired'=>'La licence a expiré',

/* './license_isms.php' */

'st_incident_management'=>'Amélioration Continue',
'st_number_of_modules'=>'Nombre de Modules',
'st_risk_management'=>'Gestion des Risques',
'st_system_modules_bl'=>'<b>Modules de Système</b>',

/* './license_limit_reached.php' */

'st_assets'=>'Actifs',
'st_modules'=>'Modules',
'st_standards'=>'Normes',
'st_users'=>'Utilisateurs',

/* './license_limit_reached.xml' */

'st_license_limit_reached'=>'Le nombre maximum de %attribute_name% a été atteinte. Il n\'est pas possible d\'ajouter %attribute_name%.',
'tt_license_limit_reached'=>'La limite de %attribute_name% a été atteinte',

/* './login.php' */

'ad_login_error_user_and_password_do_not_match'=>'%user_login% (%user_ip%)',
'ad_login_log_message'=>'%label_user%<b>%user_name%</b> %action%.',
'mx_session_conflict'=>'La connexion a échoué car un autre utilisateur est branché à partir du même navigateur.',
'st_blocked_instance_message'=>'Échantillon Bloquée',
'tt_blocked_instance'=>'Échantillon Bloquée',
'tt_session_conflict'=>'Échec de la Connexion',

/* './login.xml' */

'cb_remember_password'=>'Mémorisez le Mot de Passe',
'cb_remember_user'=>'Se souvenir de l\'Utilisateur',
'lb_password_cl'=>'Mot de Passe:',
'st_login_panel'=>'Panneau de Connexion',
'vb_forgot_your_password'=>'Vous avez oublié votre Mot de Passe?',
'vb_login'=>'Connexion',
'wn_denied_access'=>'Utilisateur sans autorisation d\'accès au système!',
'wn_invalid_user_or_password'=>'Utilisateur ou Mot de Passe invalide!',
'wn_max_users'=>'Nombre maximum de connexions simultanées atteint!',
'wn_user_blocked'=>'Utilisateur bloqué!',

/* './migration/ISMS_import.php' */

'ad_created'=>'Créé',
'ad_edited'=>'Modifié',
'ad_removed_from_bin'=>'Enlevé de la Corbeille',

/* './migration/ISMS_migration.php' */

'st_destination_db_error'=>'Erreur lors de la connexion à la base de données de migration de destination. Vérifiez si les données saisies sont correctes!',
'st_source_db_error'=>'Erreur lors de la connexion à la base de données de migration de source. Vérifiez si les données saisies sont correctes!',

/* './migration/ISMS_migration.xml' */

'lb_database_name_cl'=>'Nom de Base de Données:',
'lb_database_password_cl'=>'Mot de Passe de Base de Données:',
'lb_database_user_name_cl'=>'Nom d\'Utilisateur de Base de Données:',
'lb_hostname_cl'=>'Nom d\'Hôte:',
'tt_database_copy_bl'=>'<b>Copie de Base de Données</b>',
'tt_destiny_database_bl'=>'<b>Base de Données Destination</b>',
'tt_results_bl'=>'<b>Résultats</b>',
'tt_source_database_bl'=>'<b>Base de Données Source</b>',

/* './nav_graphics.xml' */

'lb_graphic_cl'=>'Choisissez un Graphique:',
'mx_10_assets_with_most_incidents'=>'Top 10 Actifs avec le plus d\'Incidents',
'mx_10_assets_with_most_residual_risks_in_red'=>'Top 10 Actifs avec le plus de Risques Élevés',
'mx_10_assets_with_most_risks'=>'Top 10 Actifs avec le plus de risques',
'mx_10_document_with_most_registers'=>'Top 10 Documents avec le plus de Registres',
'mx_10_processes_with_most_associated_assets'=>'Top 10 Processus avec le plus d\'Actifs',
'mx_10_processes_with_most_documents'=>'Top 10 Processus avec le plus de Documents',
'mx_10_processes_with_most_incidents'=>'Top 10 Processus avec le plus d\'Incidents',
'mx_10_processes_with_most_nc_summary'=>'Top 10 Processus avec le plus de Non-Conformités',
'mx_10_processes_with_most_residual_risks_in_red'=>'Top 10 Processus avec le plus de Risques Élevés',
'mx_10_processes_with_most_risks'=>'Top 10 Processus avec le plus de risques',
'mx_10_processes_with_most_users'=>'Top 10 Processus avec le plus de personnes impliquées',
'mx_amount_of_accessed_documents'=>'Nombre de Documents Consultés',
'mx_amount_of_created_documents'=>'Nombre de Documents Créés',
'mx_amount_of_documents_by_classification'=>'Nombre de Documents par Type de Classification',
'mx_amount_of_documents_by_status'=>'Nombre de Documents par Etat',
'mx_amount_of_documents_by_type'=>'Nombre de Documents par Type',
'mx_ap_action_type_proportion'=>'Proportion de Plans d\'Action et leurs Types',
'mx_assets_with_without_estimated_costs'=>'Actifs avec/sans valeur estimée de Coût',
'mx_history_of_control_test'=>'Historique des Tests de Contrôle',
'mx_history_of_controls_efficiency_revision'=>'Historique de Révision d\'Efficacité des Contrôles',
'mx_incident_category_proportion'=>'Proportions des Incidents et Catégories',
'mx_incident_financial_impact_summary'=>'Top 10 Incidents avec Impact Financier',
'mx_incident_loss_type_proportion'=>'Incidents et Proportions de Types de Perte',
'mx_incidents_created_summary'=>'Résumé d\'Incidents des 6 derniers mois',
'mx_potential_risks_summary'=>'Résumé des Risques Potentiels',
'mx_residual_risks_summary'=>'Résumé des Risques Résiduels',
'mx_summary_of_assets_costs'=>'Résumé des Actifs et des Coûts',
'mx_summary_of_risks_treatments'=>'Résumé des Traitements de Risques',
'si_optgroup_ci'=>'Amélioration Continue',
'si_optgroup_cost'=>'Coût',
'si_optgroup_pm'=>'Gérant des Règlements',
'si_optgroup_revision'=>'Révision',
'si_optgroup_rm'=>'Gestion des Risques',
'si_optgroup_test'=>'Test',

/* './nav_report.xml' */


/* './nav_search.xml' */

'cb_document_template'=>'Modèle de Document',
'cb_incident'=>'Incident',
'cb_incident_category_library'=>'Catégories d\'Incident',
'cb_non_conformity'=>'Non-Conformité',
'cb_occurrence'=>'Occurrence',
'cb_user'=>'Utilisateur',
'lb_new_search_bl_cl'=>'<b>Nouvelle Recherche:</b>',
'rb_action_plan'=>'Plan d\'Action',
'rb_advanced'=>'Avancée',
'rb_basic'=>'Basic',
'rb_soluction'=>'Solutions',
'st_creation_cl'=>'Création:',
'st_dates_filter_bl'=>'Filtre de Date',
'st_modification_cl'=>'Modification:',
'to_advanced_search_doc'=>'<b>Recherche Avancée de Documents:</b><br/>Type de recherche qui comprend le contenu du fichier dans la recherche',
'to_template_document_advanced_help'=>'<b>Recherche Avancée de Modèle de Documents</b><br/>Type de recherche qui comprend le contenu du fichier dans la recherche',
'tt_data_filter'=>'Filtre de Données',
'tt_elements_filter'=>'Filtre d\'Objets',
'vb_dates_filter'=>'Filtre de Dates',
'vb_elements_filter'=>'Filtre d\'éléments',

/* './nav_statistics.php' */

'mx_action_plan_efficient_and_not'=>'Nombre de Plans d\'Action efficaces et non-efficaces',
'mx_action_plan_finished_late'=>'Nombre de Plans d\'Action terminés en retard',
'mx_action_plan_finished_on_time'=>'Nombre des Plans d\'Action terminés à temps',
'mx_action_plan_total'=>'Nombre des Plans d\'Action',
'mx_area_amount'=>'Nombre des Domaines',
'mx_area_assets_amount'=>'Nombre des Actifs du Domaine',
'mx_area_assets_cost'=>'Coût des Actifs de Domaine',
'mx_area_controls_amount'=>'Nombre des Contrôles du Domaine',
'mx_area_controls_cost'=>'Coût des Contrôles de Domaine',
'mx_area_processes_amount'=>'Nombre des Processus du Domaine',
'mx_area_risks_amount'=>'Nombre des Risques du Domaine',
'mx_area_total_impact'=>'Total Impact de Domaine',
'mx_area_values'=>'Valeurs de Domaine',
'mx_areas_risk_summary'=>'Résumé de Risques des Domaines',
'mx_areas_without_processes'=>'Domaines sans Processus',
'mx_asset_controls_amount'=>'Nombre des Contrôles de l\'Actif',
'mx_asset_controls_cost'=>'Coût des Contrôles de l\'Actif',
'mx_asset_processes_amount'=>'Nombre des Processus de l\'Actif',
'mx_asset_risks_amount'=>'Nombre des Risques de l\'Actif',
'mx_asset_total_impact'=>'Total Impact de l\'Actif',
'mx_asset_values'=>'Valeurs de l\'Actif',
'mx_assets_amount'=>'Nombre d\'Actifs',
'mx_assets_risk_summary'=>'Résumé de Risque des Actifs',
'mx_assets_total_cost'=>'Coût Total des Actifs',
'mx_assets_without_risk_events'=>'Actifs sans Evénements de Risque',
'mx_business_areas'=>'Domaines d\'Activités',
'mx_business_processes'=>'Processus d\'Activités',
'mx_control_risks_amount'=>'Nombre des Risques de Contrôle',
'mx_controls_adequacy'=>'Adéquation des Contrôles',
'mx_controls_amount'=>'Nombre des Contrôles',
'mx_controls_implementation'=>'Mise en Oeuvre des Contrôles',
'mx_controls_revision'=>'Révision des Contrôles',
'mx_controls_test'=>'Test des Contrôles',
'mx_controls_without_associated_risks'=>'Contrôles sans les Risques Associés',
'mx_document_total'=>'Nombre de Documents',
'mx_documents_by_state'=>'Documents par État',
'mx_documents_by_type'=>'Documents par Type',
'mx_implemented_controls_investment'=>'Les Investissements des Contrôles Mis en Oeuvre',
'mx_incident_total'=>'Nombre des Incidents',
'mx_incidents_by_state'=>'Nombre d\'Incidents par État',
'mx_nc_per_ap_average'=>'Nombre Moyen de Non-Conformités par Plan d\'Action',
'mx_non_conformity_by_state'=>'Nombre des Non-Conformités par Etat',
'mx_non_conformity_total'=>'Nombre des Non-Conformités',
'mx_non_parameterized_risks'=>'Risques Non-Estimés',
'mx_not_measured_implemented_controls'=>'Contrôles Mis en Oeuvre Non Mesurés',
'mx_occupation_documents'=>'Occupation d\'Espace des Fichiers de Documents (MB)',
'mx_occupation_registers'=>'Occupation d\'Espace des Fichiers de Registres (MB)',
'mx_occupation_template'=>'Occupation d\'Espace des Fichiers de Modèles (MB)',
'mx_occupation_total'=>'Occupation d\'Espace Totale (MB)',
'mx_process_assets_amount'=>'Nombre des Actifs de Processus',
'mx_process_assets_cost'=>'Coût des Actifs de Processus',
'mx_process_controls_amount'=>'Nombre des Contrôles de Processus',
'mx_process_controls_cost'=>'Coût des Contrôles de Processus',
'mx_process_risks_amount'=>'Nombre des Risques de Processus',
'mx_process_total_impact'=>'Total Impact de Processus',
'mx_process_values'=>'Valeurs des Processus',
'mx_processes_amount'=>'Nombre de Processus',
'mx_processes_risk_summary'=>'Résumé de Risque des Processus',
'mx_processes_without_assets'=>'Processus sans Actif',
'mx_read_documents'=>'Documents Lus',
'mx_read_documents_proportion'=>'Proportion des Documents Lus',
'mx_register_total'=>'Nombre des Registres',
'mx_risk_treatment_potential_impact'=>'Impact Potentiel de Traitement des Risque',
'mx_risks_amount'=>'Nombre de Risque',
'mx_risks_potential_impact'=>'Impact Potentiel de Risques',
'mx_risks_treatment'=>'Traitement des Risques',
'mx_scheduled_documents'=>'Proportion de Révision de Documents Prévue',
'mx_scheduled_revision_documents'=>'Révision Prévue de Documents',
'mx_standard_best_practice_amount'=>'Nombre des Meilleures Pratiques de la Norme',
'mx_total_documents'=>'Total de Documents',
'mx_unread_documents'=>'Documents Non-Lus',
'mx_unread_documents_proportion'=>'Proportion des Documents Non-Lus',
'si_abnormalities'=>'Anomalies',
'si_action_plan_statistics'=>'Statistiques de Plan d\'action',
'si_area_statistics'=>'Statistiques de Domaine',
'si_asset_statistics'=>'Statistiques d\'Actif',
'si_best_practice_summary'=>'Résumé de Meilleure Pratique',
'si_continual_improvement'=>'Statistiques d\'Amélioration Continue',
'si_control_summary'=>'Résumé de Contrôles',
'si_financial_statistics'=>'Statistiques Financières',
'si_management_level_and_range'=>'Étendue et Niveaux de Gestion',
'si_policy_management'=>'Statistiques de Gestion de Règlement',
'si_process_statistics'=>'Statistiques de Processus',
'si_risk_summary'=>'Résumé de Risque',
'si_risks_summary'=>'Résumé des Risques',
'si_system_status'=>'État du Système',
'st_accepted_risk'=>'Risque Accepté',
'st_applied_best_practices_amount'=>'Nombre de Meilleures Pratiques Employées',
'st_best_practices_amount'=>'Nombre de Meilleures Pratiques',
'st_complete_areas'=>'Domaines Complet',
'st_complete_assets'=>'Actifs Complet',
'st_complete_processes'=>'Processus Complets',
'st_controls_being_implemented'=>'Contrôles en cours de mise en oeuvre',
'st_controls_correctly_implemented'=>'Contrôles Mis en Oeuvre Correctement',
'st_controls_incorrectly_implemented'=>'Contrôles Mis en Oeuvre Incorrectement',
'st_controls_with_delayed_implementation'=>'Contrôles avec mise en oeuvre retardée',
'st_denied_access_to_statistics'=>'Les paramètres se référant à la collecte de données',
'st_efficient_action_plan'=>'Plans d\'action efficaces',
'st_medium_high_not_treated_risk'=>'Moyen/ Élevé / Risque Non-Traité',
'st_mitigated_risk'=>'des Risques Réduits',
'st_non_efficient_action_plan'=>'Plans d\'action non-efficaces',
'st_not_treated_risks'=>'Risques Non-Traités',
'st_partial_areas'=>'Domaines Partiels',
'st_partial_assets'=>'Actifs Partiels',
'st_partial_processes'=>'Processus Partiels',
'st_potentially_low_risk'=>'Risque Potentiellement Faible',
'st_successfully_revised'=>'Révisé avec succès',
'st_successfully_tested'=>'Testé avec succès',
'st_tranferred_risk'=>'Risque Transféré',
'st_treated_risks'=>'Risques Traités',
'st_unmanaged_areas'=>'Domaines Non Gérés',
'st_unmanaged_assets'=>'Actifs Non Gérés',
'st_unmanaged_processes'=>'Processus Non Gérés',
'st_unsuccessfully_revised'=>'Révisé sans succès',
'st_unsuccessfully_tested'=>'Testé sans succès',

/* './nav_statistics.xml' */

'lb_finish_cl'=>'Arrivée:',
'lb_standard_cl'=>'Norme:',
'lb_statistics_cl'=>'Statistiques:',
'rb_adequate'=>'Convenable',
'rb_ap_efficient'=>'Efficace',
'rb_ap_non_efficient'=>'Non-efficace',
'rb_in_implantation'=>'En cours de mise en oeuvre',
'rb_inadequate'=>'Pas Convenable',
'rb_late'=>'Retardé',
'rb_not_successfully'=>'Sans Succès',
'rb_not_treated'=>'Non traité',
'rb_real_risk'=>'Potentiel de Risque',
'rb_residual_risk'=>'Risque Résiduel',
'rb_successfully'=>'Avec Succès',
'rb_treated'=>'Traité',
'st_chart_not_enough_data'=>'Il n\'y a pas suffisamment de données collectionnées pour produire ce graphique.',
'vb_refresh'=>'Rafraîchir',

/* './nav_summary_advanced.php' */

'gc_amount_bl'=>'<b>#</b>',
'gc_applied_bl'=>'<b>Appliquées</b>',
'gc_assets_bl'=>'<b>Actifs</b>',
'gc_best_practices_bl'=>'<b># de Meilleures Pratiques</b>',
'gc_description_bl'=>'<b>Description</b>',
'gc_disciplinary_process_amount_bl'=>'<b># de Processus Disciplinaires</b>',
'gc_doc_rev_qty_bl'=>'<b>#</b>',
'gc_doc_sumary_percentage_bl'=>'<b>%</b>',
'gc_doc_sumary_qty_bl'=>'<b>#</b>',
'gc_document_bl'=>'<b>Document</b>',
'gc_efficiency_rates_bl'=>'<b>Taux d\'Efficacité</b>',
'gc_financial_impact_bl'=>'<b>Impact Financier</b>',
'gc_implemented_controls_bl'=>'<b>Contrôles Mis en Oeuvre</b>',
'gc_incident_amount_bl'=>'<b># d\'incidents</b>',
'gc_investment_bl'=>'<b>Investissement</b>',
'gc_nc_amount'=>'<b># de Non Conformités</b>',
'gc_nc_amount_bl'=>'<b># de Non-Conformités</b>',
'gc_planned_controls_bl'=>'<b>Contrôles Prévus</b>',
'gc_potential_bl'=>'<b>Potentiel</b>',
'gc_potential_impact_bl'=>'<b>Impact Potentiel</b>',
'gc_processes_bl'=>'<b>Processus</b>',
'gc_residual_bl'=>'<b>Résiduel</b>',
'gc_standards_bl'=>'<b>Normes</b>',
'gc_status_bl'=>'<b>État</b>',
'gc_total_bl'=>'<b>Total</b>',
'gc_users_bl'=>'<b>Employés</b>',
'gs_accepted_risk'=>'Risque Accepté',
'gs_amount_bl'=>'<b>#</b>',
'gs_applicability_statement'=>'Déclaration d\'Applicabilité',
'gs_avoided_risk'=>'Risque Évité',
'gs_ci_ap_without_doc'=>'Plan d\'action',
'gs_ci_cn_without_ap'=>'Non-Conformité',
'gs_ci_incident_without_occurrence'=>'Incidents sans Occurrences',
'gs_ci_incident_without_risks'=>'Incidents sans Risques',
'gs_ci_pending_tasks'=>'Tâches en attente de l\'Amélioration continue',
'gs_components_without_document'=>'Composants sans Documents',
'gs_controls_without_associated_risk'=>'Contrôles sans Risques Associés',
'gs_delayed'=>'Reporté',
'gs_high_risk'=>'Risque Élevé',
'gs_impact_left_bl'=>'<b>Impact Restant</b>',
'gs_implemented_controls_mitigated_risks_bl'=>'Mise en Oeuvre des Contrôles/Risques Réduits',
'gs_implemented_controls_protected_assets_bl'=>'Contrôles Mis en Oeuvre/Actifs Protégés',
'gs_implemented_controls_treated_risks_bl'=>'Contrôles Mis en Oeuvre/Risques Traités',
'gs_inefficient_controls'=>'Contrôles inefficaces',
'gs_low_risk'=>'Risque Faible',
'gs_medium_risk'=>'Risque Moyen',
'gs_mitigated_risk'=>'Réduction de Risque',
'gs_non_parameterized_risks'=>'Risques Non-estimés',
'gs_not_measured_controls'=>'Contrôles non-mesurés',
'gs_not_treated'=>'Non-Traité',
'gs_not_treated_risks_high_and_medium'=>'Risques Non Traités - Élevé et Moyen',
'gs_not_trusted_controls'=>'Contrôles Non-Fiables',
'gs_pm_docs_with_pendant_read'=>'Documents en attente de lecture',
'gs_pm_documents_without_register'=>'Documents sans Registres',
'gs_pm_incident_without_risks'=>'Documents à fréquence élevée de révision',
'gs_pm_never_read_documents'=>'Documents jamais lu',
'gs_pm_pending_tasks'=>'Tâches en attente de la Gestion des Règlements',
'gs_pm_users_with_pendant_read'=>'Utilisateurs avec des lectures en attente',
'gs_potential_risks_low'=>'Risques Potentiels - Faible',
'gs_risk_management_pending_tasks'=>'Tâches de la Gestion des Risques En Attente',
'gs_risk_treatment_plan'=>'Plan de Traitement de Risque',
'gs_scope_statement'=>'Déclaration du Champ d\'Application',
'gs_sgsi_policy_statement'=>'Énoncé du Règlement de ERMS',
'gs_total_bl'=>'<b>Total</b>',
'gs_total_non_parameterized_risks_bl'=>'<b>Risques Non-Estimés</b>',
'gs_total_parameterized_risks_bl'=>'<b>Risques Estimés</b>',
'gs_total_risks_bl'=>'<b>Total</b>',
'gs_transferred_risk'=>'Risque Transféré',
'gs_treated'=>'Traité',
'gs_trusted_and_efficient_controls'=>'Contrôles Fiables et Efficaces',
'gs_under_implementation'=>'En cours de mise en oeuvre',
'tt_help_impact_left'=>'<b>Impact Restant</b><br/><br/>Somme des impacts de risque qui sont restés après le traitement.<br/>Cette valeur représente le total de l\'impact potentiel pour la société après le traitement de risque.',
'tt_help_implemented_controls_mitigated_risks'=>'<b>Contrôles Mis en Oeuvre/Risques Réduits</b><br/><br/>Rapport entre la somme des coûts des contrôles mis en oeuvre et la somme des impacts potentiels des Risques Réduits.<br/>Cette valeur indique le coût de contrôle compte tenu de la somme de l\'impact potentiel de risque pour l\'entreprise en termes financiers. Ce rapport ne comprend que la valeur des risques qui ont été réduits par la mise en oeuvre de contrôles.',
'tt_help_implemented_controls_protected_assets'=>'<b>Contrôles Mis en Oeuvre/Actifs Protégés</b><br/><br/>Rapport entre la somme des coûts des contrôles mis en oeuvre et la valeur des actifs protégés.<br/>Il est important de ne pas dépenser plus avec les contrôles que la valeur des actifs protégés.',
'tt_help_implemented_controls_treated_risks'=>'<b>Contrôles Mis en Oeuvre/Risques Traités</b><br/><br/>Rapport entre la somme des coûts des contrôles mis en oeuvre et la somme des impacts potentiels des Risques Traités.<br/>Cette valeur indique le coût de contrôle compte tenu de la somme de l\'impact potentiel de risque pour l\'entreprise en termes financiers. Ce rapport inclut l\'ensemble des risques qui ont été traités par n\'importe quelle des formes possibles de traitement.',
'tt_help_inefficient_controls'=>'<b>Contrôles non-efficaces</b><br/><br/> Contrôles dont la révision d\'efficacité n\'est pas retardée',
'tt_help_not_measured_controls'=>'<b>Contrôles non-mesurés</b><br/><br/> Contrôles mis en oeuvre qui n\'ont ni de révision d\'efficacité ni de test',
'tt_help_trusted'=>'<b>Contrôles Non-Fiables </b><br/> contrôles dont le test n\'est pas retardée',
'tt_help_trusted_and_efficient'=>'"Contrôles Fiables et Efficaces</b><br/><br/> Contrôles qui permettent la révision de test et d\'efficacité',

/* './nav_summary_advanced.xml' */

'mi_excel_report'=>'Rapport EXCEL',
'mi_html_report'=>'Rapport HTML',
'mi_html_report_download'=>'TÉLÉCHARGER Rapport HTML',
'mi_pdf_report'=>'Rapport PDF',
'mi_word_report'=>'Rapport WORD',
'tt_abnormalities_pm_summary_bl'=>'<b>Résumé des Anomalies de Gestion des Règlements</b>',
'tt_abnormalities_rm_summary_bl'=>'<b>Résumé des Anomalies de Gestion de Risque</b>',
'tt_abnormality_ci_summary_bl'=>'<b>Résumé d\'Anomalies des Améliorations Continues</b>',
'tt_best_practices_summary_bl'=>'<b>Meilleures Pratiques - Index de Conformité</b>',
'tt_controls_summary_bl'=>'<b>Vue d\'ensemble des Contrôles</b>',
'tt_documents_average_approval_time_bl'=>'<b>Délai d\'Approbation Moyen des Documents</b>',
'tt_financial_summary_bl'=>'<b>Résumé Financier</b>',
'tt_grid_incident_summary_title_bl'=>'<b>Vue d\'Ensemble d\'Incident</b>',
'tt_grid_incidents_per_asset_title_bl'=>'<b>Top 5 Actifs avec le plus d\'Incidents</b>',
'tt_grid_incidents_per_process_title_bl'=>'<b>Top 5 Processus avec le plus d\'Incidents</b>',
'tt_grid_incidents_per_user_title_bl'=>'<b>Top 5 Utilisateurs avec le plus de Processus Disciplinaires</b>',
'tt_grid_nc_per_process_title_bl'=>'<b>Top 5 Processus avec le plus de Non-Conformités</b>',
'tt_grid_nc_summary_title_bl'=>'<b>Résumé de Non-Conformités</b>',
'tt_incident_summary_bl'=>'<b>Résumés des Améliorations Continues</b>',
'tt_last_documents_revision_time_period_summary_bl'=>'Documents<b>Résumé de la Dernière Période de Révision des Documents</b>',
'tt_parameterized_risks_summary_bl'=>'<b>Risques Potentiels vs Risques Résiduels</b>',
'tt_policy_management_summary_bl'=>'<b>Vue d\'ensemble de la Gestion des Règlements</b>',
'tt_policy_management_x_risk_management_bl'=>'<b>Documents par Élément d\'Entreprise</b>',
'tt_risk_management_documentation_summary_bl'=>'<b>Résumé de la Documentation sur la Gestion de Risque</b>',
'tt_risk_management_status_bl'=>'<b>Niveau de risque</b>',
'tt_sgsi_documents_and_reports_bl'=>'<b>ISO 31000 Documents ERMS Obligatoires</b>',
'tt_top_10_most_read_documents_bl'=>'<b>Lecture de Documents - Top 10</b>',

/* './nav_summary_advancedNEW.xml' */

'tt_general_risks_summary_bl'=>'<b>Risque Estimé vs Non-Estimé </b>',

/* './nav_summary_basic.php' */

'gc_documents_qty_bl'=>'<b>Nombre de Documents</b>',
'gc_documents_status_bl'=>'<b>État du Document</b>',
'gc_percentage_bl'=>'<b>%</b>',
'gc_qty_bl'=>'<b>#</b>',
'gc_readings_bl'=>'<b>Lectures</b>',
'gc_time_period_smaller_or_equal_bl'=>'<b>Période de Temps (de durée plus courte ou égale à)</b>',
'gs_area'=>'Domaine',
'gs_areas_without_processes'=>'Domaines sans processus',
'gs_asset'=>'Actif',
'gs_assets_without_risk_events'=>'Actif sans événements de risque',
'gs_average_time'=>'Durée Moyenne',
'gs_controls_without_risk_events_association'=>'Contrôles sans association avec des événements de risque',
'gs_documents_to_be_read'=>'Documents à lire',
'gs_documents_total_bl'=>'<b>Total Documents</b>',
'gs_documents_with_files'=>'Documents avec des Fichiers',
'gs_documents_with_links'=>'Documents avec Liens',
'gs_more_than_six_months'=>'Plus de Six Mois',
'gs_na'=>'NA',
'gs_one_month'=>'Un Mois',
'gs_one_week'=>'Une Semaine',
'gs_process'=>'Processus',
'gs_processes_without_assets'=>'Processus sans actifs',
'gs_published_documents_information_bl'=>'<b>Documents Publiés</b>',
'gs_risk'=>'Risque',
'gs_six_months'=>'Six Mois',
'gs_three_months'=>'Trois Mois',

/* './nav_summary_basic.xml' */

'gc_period'=>'Période',
'tt_my_documents_average_approval_time_bl'=>'<b>Délai d\'Approbation Moyen de Mes Documents</b>',
'tt_my_documents_last_revision_average_time_summary_bl'=>'<b>Résumé du Temps Moyen de la Dernière Révision de Mes documents</b>',
'tt_my_documents_policy_management_summary_bl'=>'<b>Résumé de Gestion des Règlements (Mes documents)</b>',
'tt_my_elements_bl'=>'<b>Mes Objets</b>',
'tt_my_pendencies_bl'=>'<b>Mes En Attente de Gestion des Risques</b>',
'tt_my_risk_summary_bl'=>'<b>Mon Résumé de Risques</b>',
'tt_policy_management_x_risk_management_my_elements_bl'=>'<b>Gestion des Risques (Mes Objets) x Gestion des Règlements</b>',
'tt_policy_sumary_bl'=>'<b>Résumés de Gestion des Règlements</b>',
'tt_risk_management_my_elements_documentation_summary_bl'=>'<b>Résumé de la Documentation sur la Gestion des Risques de Mes Objets </b>',
'tt_risk_summary_bl'=>'<b>Résumés de Gestion des Risques</b>',
'tt_top_10_most_read_my_documents_bl'=>'<b>Top 10 Mes Documents les Plus Lus</b>',

/* './nav_warnings.xml' */

'gc_inquirer'=>'Enquêteur',
'gc_justif'=>'Juste.',
'mi_denied'=>'Rejeter',
'mi_open_task'=>'Ouvrir tâche',
'tt_alerts_bl'=>'<b>Mes Alertes</b>',
'tt_tasks_bl'=>'<b>Mes Tâches</b>',
'vb_remove_alerts'=>'Supprimer Alertes',
'wn_removal_warning'=>'Vous devez sélectionner au moins un avertissement à enlever.',

/* './packages/admin/acl_tree.xml' */

'to_acl_A'=>'Les autorisations aux zones \'Admin\' du système',
'to_acl_A.A'=>'l\'onglet d\'Audit',
'to_acl_A.A.1'=>'Onglet de la liste des tâches générées par le système',
'to_acl_A.A.2'=>'Onglet de la liste des alertes générées par le système',
'to_acl_A.A.3'=>'Onglet avec les actions effectuées dans le système',
'to_acl_A.C'=>'l\'onglet de Personnalisation du système',
'to_acl_A.C.1'=>'Personnalisation des noms et du nombre des paramètres de l\'estimation de risque.',
'to_acl_A.C.10'=>'Personnalisation du poids de chaque paramètre de risque',
'to_acl_A.C.11'=>'Personnalisation de la classification du système',
'to_acl_A.C.2'=>'Personnalisation de la définition de la matrice des risques',
'to_acl_A.C.3'=>'Personnalisation des paramètres d\'estimation des coûts',
'to_acl_A.C.4'=>'Personnalisation des paramètres d\'impact financier',
'to_acl_A.C.5'=>'Personnalisation de plusieurs caractéristiques du système',
'to_acl_A.C.6'=>'Personnalisation des utilisateurs spéciaux',
'to_acl_A.C.7'=>'Personnalisation de la quantité et des noms de types et des priorités utilisés dans le système',
'to_acl_A.C.7.1'=>'Configuration de la classification des domaines',
'to_acl_A.C.7.2'=>'Configuration de la classification des processus',
'to_acl_A.C.7.3'=>'Configuration de la classification des risques en matière de type',
'to_acl_A.C.7.4'=>'Configuration de la classification de l\'événement',
'to_acl_A.C.7.5'=>'Configuration de classification de contrôle',
'to_acl_A.C.7.6'=>'Configuration de la classification de documents',
'to_acl_A.C.7.7'=>'Configuration de classification de registre',
'to_acl_A.C.8'=>'Personnalisation des contrôles de sûreté',
'to_acl_A.C.9'=>'Personnalisation du niveau d\'acceptation de risque',
'to_acl_A.L'=>'l\'onglet de la Corbeille',
'to_acl_A.L.1'=>'Bouton pour restaurer les éléments sélectionnés',
'to_acl_A.L.2'=>'Bouton pour supprimer les éléments sélectionnés',
'to_acl_A.L.3'=>'Bouton pour supprimer tous les éléments de la corbeille',
'to_acl_A.MA'=>'l\'onglet de Gestion de compte',
'to_acl_A.P'=>'l\'onglet des Profils',
'to_acl_A.P.1'=>'Menu pour modifier un certain profil',
'to_acl_A.P.2'=>'Menu pour supprimer un profil',
'to_acl_A.P.3'=>'Bouton pour insérer un profil',
'to_acl_A.S'=>'l\'onglet des Paramètres du system',
'to_acl_A.S.1'=>'Configuration du règlement de Mot de Passe',
'to_acl_A.S.2'=>'Configuration de système \'Supprimer en Cascade\'',
'to_acl_A.U'=>'l\'onglet d\'Utilisateurs',
'to_acl_A.U.1'=>'Menu pour modifier un certain utilisateur',
'to_acl_A.U.2'=>'Menu pour supprimer un utilisateur',
'to_acl_A.U.3'=>'Bouton pour insérer un utilisateur',
'to_acl_A.U.4'=>'Bouton pour réinitialiser les mots de passe de tous les utilisateurs',
'to_acl_A.U.5'=>'Menu pour bloquer les utilisateurs du système',
'to_acl_M'=>'Autorisations pour le mode de système \'Vue d\'utilisateur\'',
'to_acl_M.CI'=>'l\'onglet d\'Amélioration continue',
'to_acl_M.CI.1'=>'l\'onglet d\'Occurrences',
'to_acl_M.CI.1.1'=>'Autorisation de visualiser tous les occurrences',
'to_acl_M.CI.1.2'=>'Bouton pour insérer les occurrences',
'to_acl_M.CI.1.3'=>'Menu pour modifier même si l\'utilisateur n\'est ni le responsable de l\'incident',
'to_acl_M.CI.1.4'=>'Menu pour supprimer même si l\'utilisateur n\'est ni le responsable de l\'incident',
'to_acl_M.CI.2'=>'l\'onglet d\'Incident',
'to_acl_M.CI.2.1'=>'Autorisation d\'afficher tous les incidents',
'to_acl_M.CI.2.2'=>'Bouton pour insérer un incident',
'to_acl_M.CI.2.3'=>'Menu pour modifier',
'to_acl_M.CI.2.4'=>'Menu pour supprimer',
'to_acl_M.CI.3'=>'l\'onglet de Non-Conformité',
'to_acl_M.CI.3.1'=>'Autorisation d\'afficher toutes les non-conformités',
'to_acl_M.CI.3.2'=>'Bouton pour insérer une non-conformité',
'to_acl_M.CI.3.3'=>'Menu pour modifier',
'to_acl_M.CI.3.4'=>'Menu pour supprimer',
'to_acl_M.CI.5'=>'l\'onglet de Processus disciplinaire',
'to_acl_M.CI.5.1'=>'Autorisation d\'afficher les mesures prises à l\'égard d\'un utilisateur impliqué dans un processus disciplinaire',
'to_acl_M.CI.5.2'=>'Autorisation d\'inscrire la mesure qui doit être prise à l\'égard d\'un utilisateur impliqué dans un processus disciplinaire',
'to_acl_M.CI.5.3'=>'Autorisation de retirer un utilisateur d\'un processus disciplinaire',
'to_acl_M.CI.6'=>'l\'onglet de Rapports',
'to_acl_M.CI.6.1'=>'Rapports d\'incident',
'to_acl_M.CI.6.1.1'=>'Rapport d\'occurrences par incident',
'to_acl_M.CI.6.1.2'=>'Rapport de processus disciplinaire',
'to_acl_M.CI.6.1.3'=>'Rapport de collection d\'évidences',
'to_acl_M.CI.6.1.4'=>'Rapport de suivi des occurrences',
'to_acl_M.CI.6.1.5'=>'Rapport de suivi d\'incident',
'to_acl_M.CI.6.1.6'=>'Rapport d\'impact financier par incident',
'to_acl_M.CI.6.1.7'=>'Rapport de révision de contrôle causés par des incidents',
'to_acl_M.CI.6.1.8'=>'Rapport d\'objets touchés par des incidents',
'to_acl_M.CI.6.1.9'=>'Rapport de calcul automatique de probabilité de risque',
'to_acl_M.CI.6.2'=>'Rapports de non-conformités',
'to_acl_M.CI.6.2.1'=>'Rapport de non-conformité par processus',
'to_acl_M.CI.6.2.2'=>'Rapport de non-conformité de contrôle par processus',
'to_acl_M.CI.6.2.3'=>'Rapport du suivi de la non-conformité',
'to_acl_M.CI.6.2.4'=>'Rapport de plan d\'action par l\'utilisateur',
'to_acl_M.CI.6.2.5'=>'Rapport de plan d\'action par processus',
'to_acl_M.CI.6.3'=>'Rapports d\'amélioration continue',
'to_acl_M.CI.6.3.1'=>'Rapport des taches en attente d\'amélioration continue',
'to_acl_M.CI.6.3.2'=>'Rapport d\'incidents',
'to_acl_M.CI.6.3.3'=>'Rapport de non-conformités sans plans d\'action qui leur sont associés.',
'to_acl_M.CI.6.3.4'=>'Rapport des plans d\'action sans document',
'to_acl_M.CI.6.3.5'=>'Rapport d\'incidents sans occurrences',
'to_acl_M.CI.7'=>'l\'onglet de Plan d\'action',
'to_acl_M.CI.7.1'=>'Autorisation d\'afficher tous les plans d\'action',
'to_acl_M.CI.7.2'=>'Bouton pour insérer un plan d\'action',
'to_acl_M.CI.7.3'=>'Autorisation de modifier n\'importe quel plan d\'action',
'to_acl_M.CI.7.4'=>'Autorisation de supprimer un plan d\'action',
'to_acl_M.D'=>'l\'onglet Tableau de bord',
'to_acl_M.D.1'=>'onglet Résumé',
'to_acl_M.D.2'=>'l\'onglet de Résumé Avancé',
'to_acl_M.D.3'=>'onglet Graphiques',
'to_acl_M.D.6'=>'onglet des Statistiques du Système (graphiques)',
'to_acl_M.D.6.1'=>'Statistiques sur les domaines d\'activités gérées par le système',
'to_acl_M.D.6.10'=>'Statistiques sur l\'état du système (résumé de risque x domaine',
'to_acl_M.D.6.11'=>'Statistiques de gestion des règlements du système',
'to_acl_M.D.6.12'=>'Statistiques des améliorations continues du système',
'to_acl_M.D.6.13'=>'Statistiques sur les plans d\'action et sur la relation entre le plan d\'action et la non-conformité',
'to_acl_M.D.6.2'=>'Statistiques sur les processus des domaines d\'activité gérés par le système',
'to_acl_M.D.6.3'=>'Statistiques sur les actifs gérés par le système',
'to_acl_M.D.6.4'=>'Résumé des risques gérés par le système',
'to_acl_M.D.6.5'=>'Résumé des meilleures pratiques',
'to_acl_M.D.6.6'=>'Résumé des contrôles du système',
'to_acl_M.D.6.7'=>'Résumé des anomalies détectées par le système',
'to_acl_M.D.6.8'=>'Statistiques financières - les coûts',
'to_acl_M.D.6.9'=>'Statistiques sur le domaine',
'to_acl_M.L'=>'l\'onglet de Gestion des Risques',
'to_acl_M.L.1'=>'l\'onglet de Bibliothèque d\'Événements',
'to_acl_M.L.1.1'=>'Menu de navigation dans les sous-catégories et les événements d\'une certaine catégorie',
'to_acl_M.L.1.2'=>'Menu pour modifier une certaine catégorie',
'to_acl_M.L.1.3'=>'Menu pour supprimer une certaine catégorie',
'to_acl_M.L.1.4'=>'Menu pour associer les meilleures pratiques à un certain événement',
'to_acl_M.L.1.5'=>'Menu pour modifier un certain événement',
'to_acl_M.L.1.6'=>'Menu pour supprimer un certain événement',
'to_acl_M.L.1.7'=>'Bouton pour insérer une nouvelle catégorie',
'to_acl_M.L.1.8'=>'Bouton pour insérer un nouvel événement',
'to_acl_M.L.1.9'=>'Bouton pour associer un événement à la catégorie dans laquelle l\'utilisateur se trouve',
'to_acl_M.L.2'=>'l\'onglet Bibliothèque de Meilleures Pratiques',
'to_acl_M.L.2.1'=>'Menu de navigation dans les sous-sections et les meilleures pratiques d\'une certaine section',
'to_acl_M.L.2.10'=>'Permet à l\'utilisateur de créer un modèle de document de meilleure pratique',
'to_acl_M.L.2.2'=>'Menu pour modifier une section',
'to_acl_M.L.2.3'=>'Menu pour supprimer une section',
'to_acl_M.L.2.4'=>'Menu pour associer des événements à une certaine meilleures pratiques',
'to_acl_M.L.2.5'=>'Menu pour modifier un certain meilleures pratiques',
'to_acl_M.L.2.6'=>'Menu pour supprimer une certaine meilleure pratique',
'to_acl_M.L.2.7'=>'Bouton pour insérer une nouvelle section',
'to_acl_M.L.2.8'=>'Bouton pour insérer une nouvelle meilleure pratique',
'to_acl_M.L.2.9'=>'Affiche les modèles de document de meilleure pratique dans l\'onglet des modèles de document',
'to_acl_M.L.3'=>'l\'onglet de la bibliothèque de normes',
'to_acl_M.L.3.1'=>'Menu pour modifier une certaine norme',
'to_acl_M.L.3.2'=>'Menu pour supprimer une certaine norme',
'to_acl_M.L.3.3'=>'Bouton pour insérer une nouvelle norme',
'to_acl_M.L.4'=>'l\'onglet de la Bibliothèque de Meilleures Pratiques',
'to_acl_M.L.4.1'=>'Menu pour modifier une certaine section',
'to_acl_M.L.4.2'=>'Menu pour modifier une certaine section',
'to_acl_M.L.5'=>'l\'onglet des Modèles de Document',
'to_acl_M.L.5.1'=>'Menu pour modifier un modèle de document',
'to_acl_M.L.5.2'=>'Menu pour supprimer un modèle de document',
'to_acl_M.L.5.3'=>'Bouton pour insérer un nouveau modèle de document',
'to_acl_M.L.6'=>'l\'onglet de la Bibliothèque de catégories',
'to_acl_M.L.6.1'=>'Bouton pour insérer une catégorie',
'to_acl_M.L.6.2'=>'Menu pour modifier une catégorie',
'to_acl_M.L.6.3'=>'Menu pour supprimer une catégorie',
'to_acl_M.L.6.4'=>'Bouton pour insérer une solution',
'to_acl_M.L.6.5'=>'Menu pour modifier la solution',
'to_acl_M.L.6.6'=>'Menu pour supprimer la solution',
'to_acl_M.PM'=>'l\'onglet Gestion de Règlement',
'to_acl_M.PM.10'=>'l\'onglet d\'Affichage/gestion des documents en développement',
'to_acl_M.PM.10.1'=>'Bouton pour insérer un document',
'to_acl_M.PM.11'=>'l\'onglet d\'Affichage/gestion des documents en approbation',
'to_acl_M.PM.12'=>'l\'onglet d\'Affichage/gestion des documents publiés',
'to_acl_M.PM.13'=>'l\'onglet d\'Affichage/gestion des documents en révision',
'to_acl_M.PM.14'=>'l\'onglet d\'Affichage/gestion des documents obsolètes',
'to_acl_M.PM.15'=>'l\'onglet d\'Affichage/gestion de tous les documents',
'to_acl_M.PM.16'=>'l\'onglet d\'Affichage/gestion des registres',
'to_acl_M.PM.17'=>'onglet Rapports',
'to_acl_M.PM.17.1'=>'Autres rapports',
'to_acl_M.PM.17.1.16'=>'Rapport qui énumère les propriétés d\'un document',
'to_acl_M.PM.17.1.17'=>'Rapport de classification',
'to_acl_M.PM.17.2'=>'Rapports Généraux',
'to_acl_M.PM.17.2.1'=>'Rapport des Documents',
'to_acl_M.PM.17.2.2'=>'Rapport des documents par domaine',
'to_acl_M.PM.17.2.3'=>'Rapport de documents par composant ERMS',
'to_acl_M.PM.17.2.4'=>'Rapport des commentaires et raisons de révision des documents',
'to_acl_M.PM.17.2.5'=>'Rapport de commentaires des utilisateurs',
'to_acl_M.PM.17.3'=>'Enregistrer des rapports',
'to_acl_M.PM.17.3.1'=>'Rapport de registre par document',
'to_acl_M.PM.17.4'=>'Rapports de contrôle d\'accès',
'to_acl_M.PM.17.4.1'=>'Rapport d\'audit d\'accès des utilisateurs',
'to_acl_M.PM.17.4.2'=>'Rapport d\'audit d\'accès aux documents',
'to_acl_M.PM.17.4.3'=>'Rapport des documents et leurs approbateurs',
'to_acl_M.PM.17.4.4'=>'Rapport des documents et leurs lecteurs',
'to_acl_M.PM.17.4.5'=>'Rapport des utilisateurs associés à des processus',
'to_acl_M.PM.17.5'=>'Rapports de gestion',
'to_acl_M.PM.17.5.1'=>'Rapport des documents en attente de lecture',
'to_acl_M.PM.17.5.2'=>'Rapport des documents les plus consultés',
'to_acl_M.PM.17.5.3'=>'Rapport des documents non accédés',
'to_acl_M.PM.17.5.4'=>'Rapport des documents les plus révisés',
'to_acl_M.PM.17.5.5'=>'Rapport des approbations en attente',
'to_acl_M.PM.17.5.6'=>'Rapport des dates de création et de révision des documents',
'to_acl_M.PM.17.5.7'=>'Rapport de programme de révision des documents',
'to_acl_M.PM.17.5.8'=>'Rapport des documents à révision retardée',
'to_acl_M.PM.17.6'=>'Rapports d\'anomalie',
'to_acl_M.PM.17.6.1'=>'Rapport des composants sans documents',
'to_acl_M.PM.17.6.2'=>'Tâches en attente de la gestion des règlements',
'to_acl_M.PM.17.6.3'=>'Documents à fréquence élevée de révision. À fréquence élevée fait référence à des documents qui sont examinés plus d\'une fois par semaine en moyenne.',
'to_acl_M.PM.17.6.4'=>'Rapport de documents sans registres',
'to_acl_M.PM.17.6.5'=>'Rapport qui liste les utilisateurs qui sont des lecteurs de documents publiés',
'to_acl_M.PM.18'=>'l\'onglet d\'Affichage/gestion des documents à publier',
'to_acl_M.RM'=>'l\'onglet de la Gestion des risques',
'to_acl_M.RM.1'=>'l\'onglet des Domaines',
'to_acl_M.RM.1.1'=>'Autorisation de visualiser tous les domaines',
'to_acl_M.RM.1.2'=>'Menu de navigation dans les sous-domaines d\'un domaine',
'to_acl_M.RM.1.3'=>'Menu pour afficher les processus d\'un certain domaine',
'to_acl_M.RM.1.4'=>'Menu pour modifier un certain domaine',
'to_acl_M.RM.1.5'=>'Menu pour supprimer un certain domaine',
'to_acl_M.RM.1.6'=>'Bouton pour insérer un nouveau domaine',
'to_acl_M.RM.1.7'=>'Bouton pour insérer un nouveau domaine à la racine',
'to_acl_M.RM.1.8'=>'Autorisation de modifier un domaine',
'to_acl_M.RM.1.9'=>'Permission de supprimer un domaine',
'to_acl_M.RM.2'=>'l\'onglet Processus',
'to_acl_M.RM.2.1'=>'Permission pour afficher tous les processus',
'to_acl_M.RM.2.2'=>'Menu de navigation dans les actifs d\'un certain processus',
'to_acl_M.RM.2.4'=>'Menu pour modifier un certain processus',
'to_acl_M.RM.2.5'=>'Menu pour supprimer un certain processus',
'to_acl_M.RM.2.6'=>'Bouton pour insert un nouveau processus',
'to_acl_M.RM.2.7'=>'Autorisation de modifier un processus',
'to_acl_M.RM.2.8'=>'Permission de supprimer un processus',
'to_acl_M.RM.3'=>'onglet d\'Actif',
'to_acl_M.RM.3.1'=>'Permission pour afficher tous les actifs',
'to_acl_M.RM.3.10'=>'Menu contextuel pour l\'insertion des risques à des actifs à partir des événements d\'une catégorie de la Bibliothèque des Catégorie d\'Événement',
'to_acl_M.RM.3.2'=>'Menu de navigation dans les risques d\'un certain actif',
'to_acl_M.RM.3.4'=>'Menu pour créer un risque pour un actif',
'to_acl_M.RM.3.5'=>'Menu pour modifier un certain actif',
'to_acl_M.RM.3.6'=>'Menu pour supprimer un certain actif',
'to_acl_M.RM.3.7'=>'Bouton pour insérer un nouvel actif',
'to_acl_M.RM.3.8'=>'Autorisation de modifier un actif',
'to_acl_M.RM.3.9'=>'Permission de supprimer un actif',
'to_acl_M.RM.4'=>'onglet Risques',
'to_acl_M.RM.4.1'=>'Permission de voir tous les Risques',
'to_acl_M.RM.4.10'=>'Permission de supprimer un risque',
'to_acl_M.RM.4.2'=>'Menu de navigation dans les contrôles d\'un certain risque',
'to_acl_M.RM.4.4'=>'Menu pour modifier un certain risque',
'to_acl_M.RM.4.5'=>'Menu pour supprimer un certain risque',
'to_acl_M.RM.4.6'=>'Bouton pour insérer un nouveau risque',
'to_acl_M.RM.4.8'=>'Bouton pour estimer le Risque',
'to_acl_M.RM.4.9'=>'Autorisation de modifier un risque',
'to_acl_M.RM.5'=>'onglet Contrôle',
'to_acl_M.RM.5.1'=>'Permission de voir tous les contrôles',
'to_acl_M.RM.5.2'=>'Menu de navigation dans les risques associés à un certain contrôle',
'to_acl_M.RM.5.3'=>'Menu pour associer risques à un certain contrôle',
'to_acl_M.RM.5.4'=>'Menu pour modifier un certain contrôle',
'to_acl_M.RM.5.5'=>'Menu pour supprimer un certain contrôle',
'to_acl_M.RM.5.6'=>'Bouton pour insérer un nouveau contrôle',
'to_acl_M.RM.5.7'=>'Autorisation de modifier un contrôle',
'to_acl_M.RM.5.8'=>'Autorisation de supprimer un contrôle',
'to_acl_M.RM.5.9'=>'Changements de la révision/tester l\'historique des contrôles du système',
'to_acl_M.RM.6'=>'onglet Rapports',
'to_acl_M.RM.6.1'=>'Rapports de Risques',
'to_acl_M.RM.6.1.1'=>'Rapport de Valeurs de Risque',
'to_acl_M.RM.6.1.2'=>'Rapport de Risques par processus',
'to_acl_M.RM.6.1.3'=>'Rapport de Risques par actif',
'to_acl_M.RM.6.1.4'=>'Rapport de Risques par contrôle',
'to_acl_M.RM.6.1.5'=>'Rapport d\'impact de risque',
'to_acl_M.RM.6.1.6'=>'Rapport d\'importance d\'actifs',
'to_acl_M.RM.6.1.7'=>'Rapport de Risques par domaine',
'to_acl_M.RM.6.1.8'=>'Rapport de liste de vérification par événement',
'to_acl_M.RM.6.2'=>'Rapports de Contrôle',
'to_acl_M.RM.6.2.1'=>'Rapports de contrôles par responsable',
'to_acl_M.RM.6.2.10'=>'Rapport du suivi des tests de contrôles',
'to_acl_M.RM.6.2.11'=>'Rapport de conformité',
'to_acl_M.RM.6.2.12'=>'Rapport de l\'ordre du jour de la révision de contrôle par responsable',
'to_acl_M.RM.6.2.13'=>'Rapport de l\'ordre du jour du test de contrôle par responsable',
'to_acl_M.RM.6.2.2'=>'Rapports de contrôles par risque',
'to_acl_M.RM.6.2.3'=>'Rapport de la planification des contrôles',
'to_acl_M.RM.6.2.4'=>'Rapport de date de mise en oeuvre des contrôles',
'to_acl_M.RM.6.2.5'=>'Rapport de l\'efficacité des contrôles',
'to_acl_M.RM.6.2.6'=>'Rapport de résumé des contrôles',
'to_acl_M.RM.6.2.7'=>'Rapport du suivi des contrôles',
'to_acl_M.RM.6.2.8'=>'Rapport du suivi de l\'efficacité des contrôles',
'to_acl_M.RM.6.2.9'=>'Rapport de contrôles non mesurés',
'to_acl_M.RM.6.3'=>'Les rapports de la Gestion des Risques',
'to_acl_M.RM.6.3.1'=>'Rapport des tâches en attentes de la gestion des risques',
'to_acl_M.RM.6.3.2'=>'Rapport des domaines sans processus',
'to_acl_M.RM.6.3.3'=>'Rapport des actifs sans risques',
'to_acl_M.RM.6.3.4'=>'Rapport des processus sans actifs',
'to_acl_M.RM.6.3.5'=>'Rapport des risques non-estimés',
'to_acl_M.RM.6.3.6'=>'Rapport des contrôles sans risques',
'to_acl_M.RM.6.3.7'=>'Rapport de duplicata de risques',
'to_acl_M.RM.6.4'=>'Rapports financiers',
'to_acl_M.RM.6.4.4'=>'Rapport de coût par contrôle',
'to_acl_M.RM.6.4.5'=>'Rapport de coût des contrôles par responsable',
'to_acl_M.RM.6.4.6'=>'Rapport des coût des contrôles par domaine',
'to_acl_M.RM.6.4.7'=>'Rapport de coût des contrôles par processus',
'to_acl_M.RM.6.4.8'=>'Rapport de coût des contrôles par actif',
'to_acl_M.RM.6.5'=>'Rapports ISO 31000',
'to_acl_M.RM.6.5.1'=>'Rapport de déclaration d\'applicabilité',
'to_acl_M.RM.6.5.2'=>'Rapport de traitement des risques',
'to_acl_M.RM.6.5.3'=>'Rapport de déclaration du champ d\'application d\'ERMS',
'to_acl_M.RM.6.5.4'=>'Rapport de l\'Énoncé du Règlement de ERMS',
'to_acl_M.RM.6.6'=>'Rapports des Résumés',
'to_acl_M.RM.6.6.1'=>'Rapport: top 10 - actifs avec le plus de risques',
'to_acl_M.RM.6.6.2'=>'Rapport: top 10 - actifs avec le plus de risques élevés',
'to_acl_M.RM.6.6.3'=>'Rapport: top 10 - risques par domaine',
'to_acl_M.RM.6.6.4'=>'Rapport: top 10 - risques par processus',
'to_acl_M.RM.6.6.5'=>'Rapport de Nombre des risques par processus',
'to_acl_M.RM.6.6.6'=>'Rapport de nombre des risques par domaine',
'to_acl_M.RM.6.6.7'=>'Rapport de l\'état des risques par domaine',
'to_acl_M.RM.6.6.8'=>'Rapport de l\'état des risques par processus',
'to_acl_M.RM.6.7'=>'Autres rapports',
'to_acl_M.RM.6.7.1'=>'Rapport de responsabilités des utilisateurs',
'to_acl_M.RM.6.7.2'=>'Rapport des objets en attente',
'to_acl_M.RM.6.7.3'=>'Actifs rapport',
'to_acl_M.RM.6.7.4'=>'Rapport de classification d\'objet',
'to_acl_M.RM.6.7.5'=>'Rapport qui indique pour chaque actif ses dépendances et dépendants',
'to_acl_M.RM.6.7.6'=>'Rapport qui montre la pertinence de l\'actif',
'to_acl_M.S'=>'l\'onglet de Recherche',
'tr_acl_A'=>'Admin',
'tr_acl_A.A'=>'AUDIT',
'tr_acl_A.A.1'=>'TÂCHES',
'tr_acl_A.A.2'=>'ALERTES',
'tr_acl_A.A.3'=>'JOURNAL',
'tr_acl_A.C'=>'PERSONNALISATIONS',
'tr_acl_A.C.1'=>'PARAMÈTRES DES ESTIMATIONS DE RISK',
'tr_acl_A.C.10'=>'POIDS DE PARAMÈTRES DE RISQUE',
'tr_acl_A.C.11'=>'CLASSIFICATION DU SYSTEME',
'tr_acl_A.C.2'=>'DEFINITION DE MATRICE DES RISQUES',
'tr_acl_A.C.3'=>'PARAMÈTRES D\'ESTIMATION DES CÔUTS',
'tr_acl_A.C.4'=>'PARAMÈTRES D\'IMPACT FINANCIER',
'tr_acl_A.C.5'=>'CARACTÉRISTIQUES DU SYSTÈME',
'tr_acl_A.C.6'=>'UTILISATEURS SPECIAUX',
'tr_acl_A.C.7'=>'PARAMÉTRAGE DE TYPE ET DE PRIORITÉ',
'tr_acl_A.C.7.1'=>'DOMAINE',
'tr_acl_A.C.7.2'=>'PROCESSUS',
'tr_acl_A.C.7.3'=>'RISQUE',
'tr_acl_A.C.7.4'=>'ÉVÉNEMENT',
'tr_acl_A.C.7.5'=>'CONTRÔLE',
'tr_acl_A.C.7.6'=>'DOCUMENT',
'tr_acl_A.C.7.7'=>'REGISTRE',
'tr_acl_A.C.8'=>'RÉVISION ET TEST DE CONTRÔLE',
'tr_acl_A.C.9'=>'NIVEAU D\'ACCEPTATION DE RISQUE',
'tr_acl_A.L'=>'CORBEILLE',
'tr_acl_A.L.1'=>'RESTAURER',
'tr_acl_A.L.2'=>'SUPPRIMER',
'tr_acl_A.L.3'=>'TOUT SUPPRIMER',
'tr_acl_A.MA'=>'MON COMPTE',
'tr_acl_A.P'=>'PROFILS',
'tr_acl_A.P.1'=>'MODIFIER',
'tr_acl_A.P.2'=>'SUPPRIMER',
'tr_acl_A.P.3'=>'INSÉRER PROFIL',
'tr_acl_A.S'=>'PARAMÈTRES DU SYSTEM',
'tr_acl_A.S.1'=>'RÈGLEMENT DE MOT DE PASSE',
'tr_acl_A.S.2'=>'SUPPRIMER EN CASCADE',
'tr_acl_A.U'=>'UTILISATEURS',
'tr_acl_A.U.1'=>'MODIFIER',
'tr_acl_A.U.2'=>'SUPPRIMER',
'tr_acl_A.U.3'=>'INSÉRER UTILISATEUR',
'tr_acl_A.U.4'=>'RÉINITIALISER LES MOTS DE PASSE',
'tr_acl_A.U.5'=>'BLOQUER UTILISATEUR',
'tr_acl_M'=>'Vue d\'utilisateur',
'tr_acl_M.CI'=>'AMÉLIORATION CONTINUE',
'tr_acl_M.CI.1'=>'OCCURRENCES',
'tr_acl_M.CI.1.1'=>'LISTE',
'tr_acl_M.CI.1.2'=>'INSÉRER OCCURRENCE',
'tr_acl_M.CI.1.3'=>'MODIFIER SANS RESTRICTION',
'tr_acl_M.CI.1.4'=>'SUPPRIMER SANS RESTRICTION',
'tr_acl_M.CI.2'=>'INCIDENT',
'tr_acl_M.CI.2.1'=>'LISTE',
'tr_acl_M.CI.2.2'=>'INSÉRER UN INCIDENT',
'tr_acl_M.CI.2.3'=>'MODIFIER SANS RESTRICTION',
'tr_acl_M.CI.2.4'=>'SUPPRIMER SANS RESTRICTION',
'tr_acl_M.CI.3'=>'NON-CONFORMITÉ',
'tr_acl_M.CI.3.1'=>'LISTE',
'tr_acl_M.CI.3.2'=>'INSÉRER UNE NON-CONFORMITÉ',
'tr_acl_M.CI.3.3'=>'MODIFIER SANS RESTRICTION',
'tr_acl_M.CI.3.4'=>'SUPPRIMER SANS RESTRICTION',
'tr_acl_M.CI.5'=>'PROCESSUS DISCIPLINAIRE',
'tr_acl_M.CI.5.1'=>'VOIR MESURE',
'tr_acl_M.CI.5.2'=>'INSCRIRE LA MESURE',
'tr_acl_M.CI.5.3'=>'SUPPRIMER',
'tr_acl_M.CI.6'=>'RAPPORTS',
'tr_acl_M.CI.6.1'=>'RAPPORTS D\'INCIDENT',
'tr_acl_M.CI.6.1.1'=>'OCCURRENCES PAR INCIDENT',
'tr_acl_M.CI.6.1.2'=>'PROCESSUS DISCIPLINAIRE',
'tr_acl_M.CI.6.1.3'=>'COLLECTION D\'ÉVIDENCES',
'tr_acl_M.CI.6.1.4'=>'SUIVI DES OCCURRENCES',
'tr_acl_M.CI.6.1.5'=>'SUIVI D\'INCIDENT',
'tr_acl_M.CI.6.1.6'=>'IMPACT FINANCIER PAR INCIDENT',
'tr_acl_M.CI.6.1.7'=>'RÉVISION DE CONTRÔLE CAUSÉS PAR DES INCIDENTS',
'tr_acl_M.CI.6.1.8'=>'OBJETS TOUCHÉS PAR DES INCIDENTS',
'tr_acl_M.CI.6.1.9'=>'CALCUL AUTOMATIQUE DE PROBABILITÉ DE RISQUE',
'tr_acl_M.CI.6.2'=>'RAPPORTS DE NON-CONFORMITÉS',
'tr_acl_M.CI.6.2.1'=>'NON-CONFORMITÉ PAR PROCESSUS',
'tr_acl_M.CI.6.2.2'=>'NON-CONFORMITÉ DE CONTRÔLE PAR PROCESSUS',
'tr_acl_M.CI.6.2.3'=>'SUIVI DE LA NON-CONFORMITÉ',
'tr_acl_M.CI.6.2.4'=>'PLAN D\'ACTION PAR L\'UTILISATEUR',
'tr_acl_M.CI.6.2.5'=>'PLAN D\'ACTION PAR PROCESSUS',
'tr_acl_M.CI.6.3'=>'RAPPORTS D\'AMÉLIORATION CONTINUE',
'tr_acl_M.CI.6.3.1'=>'TACHES EN ATTENTE D\'AMÉLIORATION CONTINUE',
'tr_acl_M.CI.6.3.2'=>'INCIDENTS SANS RISQUES',
'tr_acl_M.CI.6.3.3'=>'NON-CONFORMITÉ SANS PLAN D\'ACTION',
'tr_acl_M.CI.6.3.4'=>'PLAN D\'ACTION SANS DOCUMENT',
'tr_acl_M.CI.6.3.5'=>'INCIDENTS SANS OCCURRENCES',
'tr_acl_M.CI.7'=>'PLAN D\'ACTION',
'tr_acl_M.CI.7.1'=>'LISTE',
'tr_acl_M.CI.7.2'=>'INSÉRER',
'tr_acl_M.CI.7.3'=>'MODIFIER SANS RESTRICTION',
'tr_acl_M.CI.7.4'=>'SUPPRIMER SANS RESTRICTION',
'tr_acl_M.D'=>'TABLEAU DE BORD',
'tr_acl_M.D.1'=>'RÉSUMÉS',
'tr_acl_M.D.2'=>'RESUMÉ AVANCÉ',
'tr_acl_M.D.3'=>'GRAPHIQUES',
'tr_acl_M.D.6'=>'STATISTIQUES',
'tr_acl_M.D.6.1'=>'GRAPHIQUES DE DOMAINE',
'tr_acl_M.D.6.10'=>'L\'ÉTAT DU SYSTÈME',
'tr_acl_M.D.6.11'=>'STATISTIQUES DE GESTION DES RÈGLEMENTS',
'tr_acl_M.D.6.12'=>'STATISTIQUES DES AMELIORATIONS CONTINUES',
'tr_acl_M.D.6.13'=>'STATISTIQUES DES PLANS D\'ACTION',
'tr_acl_M.D.6.2'=>'GRAPHIQUES DE PROCESSUS',
'tr_acl_M.D.6.3'=>'GRAPHIQUES ACTIF',
'tr_acl_M.D.6.4'=>'RÉSUMÉ DES RISQUES',
'tr_acl_M.D.6.5'=>'RÉSUMÉ DES MEILLEURES PRATIQUES',
'tr_acl_M.D.6.6'=>'RÉSUMÉ DES CONTRÔLES',
'tr_acl_M.D.6.7'=>'ANOMALIES',
'tr_acl_M.D.6.8'=>'STATISTIQUES FINANCIÈRES',
'tr_acl_M.D.6.9'=>'COUVERTURE DU NIVEAU DE GESTION',
'tr_acl_M.L'=>'BIBLIOTHÈQUES',
'tr_acl_M.L.1'=>'BIBLIOTHÈQUE D\'ÉVÉNEMENTS',
'tr_acl_M.L.1.1'=>'OUVRIR CATÉGORIE',
'tr_acl_M.L.1.2'=>'EDIT CATÉGORIE',
'tr_acl_M.L.1.3'=>'SUPPRIMER CATÉGORIE',
'tr_acl_M.L.1.4'=>'ASSOCIER MEILLEURES PRATIQUES',
'tr_acl_M.L.1.5'=>'MODIFIER ÉVÉNEMENT',
'tr_acl_M.L.1.6'=>'SUPPRIMER ÉVÉNEMENT',
'tr_acl_M.L.1.7'=>'INSÉRER CATÉGORIE',
'tr_acl_M.L.1.8'=>'INSÉRER ÉVÉNEMENT',
'tr_acl_M.L.1.9'=>'ASSOCIER ÉVÉNEMENT',
'tr_acl_M.L.2'=>'BIBLIOTHÈQUE DE MEILLEURES PRATIQUES',
'tr_acl_M.L.2.1'=>'OUVRIR SECTION',
'tr_acl_M.L.2.10'=>'CRÉER UN MODÈLES DE DOCUMENT DE MEILLEURE PRATIQUE',
'tr_acl_M.L.2.2'=>'MODIFIER SECTION',
'tr_acl_M.L.2.3'=>'SUPPRIMER SECTION',
'tr_acl_M.L.2.4'=>'ASSOCIER ÉVÉNEMENTS',
'tr_acl_M.L.2.5'=>'MODIFIER MEILLEURE PRATIQUE',
'tr_acl_M.L.2.6'=>'SUPPRIMER MEILLEURE PRATIQUE',
'tr_acl_M.L.2.7'=>'INSÉRER SECTION',
'tr_acl_M.L.2.8'=>'INSÉRER MEILLEURE PRATIQUE',
'tr_acl_M.L.2.9'=>'VOIR LES MODÈLES DE DOCUMENT DE MEILLEURE PRATIQUE',
'tr_acl_M.L.3'=>'BIBLIOTHÈQUE DE NORMES',
'tr_acl_M.L.3.1'=>'MODIFIER NORMES',
'tr_acl_M.L.3.2'=>'SUPPRIMER NORMES',
'tr_acl_M.L.3.3'=>'INSÉRER UNE NORME',
'tr_acl_M.L.4'=>'IMPORTATION / EXPORTATION',
'tr_acl_M.L.4.1'=>'EXPORTATION',
'tr_acl_M.L.4.2'=>'IMPORTATION',
'tr_acl_M.L.5'=>'BIBLIOTHÈQUE DE MODÈLES DE DOCUMENT',
'tr_acl_M.L.5.1'=>'MODIFIER MODÈLE DE DOCUMENT',
'tr_acl_M.L.5.2'=>'SUPPRIMER MODÈLE DE DOCUMENT',
'tr_acl_M.L.5.3'=>'INSÉRER MODÈLE DE DOCUMENT',
'tr_acl_M.L.6'=>'BIBLIOTHÈQUE DE CATÉGORIES',
'tr_acl_M.L.6.1'=>'INSÉRER CATÉGORIES',
'tr_acl_M.L.6.2'=>'MODIFIER CATÉGORIES',
'tr_acl_M.L.6.3'=>'SUPPRIMER CATÉGORIE',
'tr_acl_M.L.6.4'=>'INSÉRER SOLUTION',
'tr_acl_M.L.6.5'=>'MODIFIER SOLUTION',
'tr_acl_M.L.6.6'=>'SUPPRIMER SOLUTION',
'tr_acl_M.PM'=>'GESTION DE RÈGLEMENT',
'tr_acl_M.PM.10'=>'DOCUMENTS EN DÉVELOPPEMENT',
'tr_acl_M.PM.10.1'=>'INSÉRER DOCUMENT',
'tr_acl_M.PM.11'=>'DOCUMENTS EN APPROBATION',
'tr_acl_M.PM.12'=>'DOCUMENTS PUBLIÉS',
'tr_acl_M.PM.13'=>'DOCUMENTS EN RÉVISION',
'tr_acl_M.PM.14'=>'DOCUMENTS OBSOLÈTES',
'tr_acl_M.PM.15'=>'TOUS LES DOCUMENTS',
'tr_acl_M.PM.16'=>'REGISTRES',
'tr_acl_M.PM.17'=>'RAPPORTS',
'tr_acl_M.PM.17.1'=>'AUTRES',
'tr_acl_M.PM.17.1.16'=>'PROPRIÉTÉS DE DOCUMENT',
'tr_acl_M.PM.17.1.17'=>'RAPPORT DE CLASSIFICATION',
'tr_acl_M.PM.17.2'=>'RAPPORTS GÉNÉRAUX',
'tr_acl_M.PM.17.2.1'=>'DOCUMENTS',
'tr_acl_M.PM.17.2.2'=>'DOCUMENTS PAR DOMAINE',
'tr_acl_M.PM.17.2.3'=>'DOCUMENTS PAR COMPOSANT ERMS',
'tr_acl_M.PM.17.2.4'=>'COMMENTAIRES ET RAISONS DE RÉVISION DES DOCUMENTS',
'tr_acl_M.PM.17.2.5'=>'COMMENTAIRES DES UTILISATEURS',
'tr_acl_M.PM.17.3'=>'ENREGISTRER DES RAPPORTS',
'tr_acl_M.PM.17.3.1'=>'REGISTRE PAR DOCUMENT',
'tr_acl_M.PM.17.4'=>'RAPPORTS DE CONTRÔLE D\'ACCÈS',
'tr_acl_M.PM.17.4.1'=>'AUDIT D\'ACCÈS DES UTILISATEURS',
'tr_acl_M.PM.17.4.2'=>'AUDIT D\'ACCÈS AUX DOCUMENTS',
'tr_acl_M.PM.17.4.3'=>'DOCUMENTS ET LEURS APPROBATEURS',
'tr_acl_M.PM.17.4.4'=>'DOCUMENTS ET LEURS LECTEURS',
'tr_acl_M.PM.17.4.5'=>'Les utilisateurs associés à des processus',
'tr_acl_M.PM.17.5'=>'RAPPORTS DE GESTION',
'tr_acl_M.PM.17.5.1'=>'DOCUMENTS EN ATTENTE DE LECTURE',
'tr_acl_M.PM.17.5.2'=>'LES DOCUMENTS LES PLUS CONSULTÉS',
'tr_acl_M.PM.17.5.3'=>'DOCUMENTS NON ACCÉDÉS',
'tr_acl_M.PM.17.5.4'=>'DOCUMENTS LES PLUS RÉVISÉS',
'tr_acl_M.PM.17.5.5'=>'LES APPROBATIONS EN ATTENTE',
'tr_acl_M.PM.17.5.6'=>'DATES DE CRÉATION ET DE RÉVISION DES DOCUMENTS',
'tr_acl_M.PM.17.5.7'=>'PROGRAMME DE RÉVISION DES DOCUMENTS',
'tr_acl_M.PM.17.5.8'=>'DOCUMENTS À RÉVISION RETARDÉE',
'tr_acl_M.PM.17.6'=>'RAPPORTS D\'ANOMALIE',
'tr_acl_M.PM.17.6.1'=>'COMPOSANTS SANS DOCUMENTS',
'tr_acl_M.PM.17.6.2'=>'TÂCHES EN ATTENTE',
'tr_acl_M.PM.17.6.3'=>'DOCUMENTS À FRÉQUENCE ÉLEVÉE DE RÉVISION',
'tr_acl_M.PM.17.6.4'=>'DOCUMENTS SANS REGISTRES',
'tr_acl_M.PM.17.6.5'=>'UTILISATEURS AVEC LECTURE EN ATTENTE',
'tr_acl_M.PM.18'=>'DOCUMENTS À PUBLIER',
'tr_acl_M.RM'=>'GESTION DES RISQUES',
'tr_acl_M.RM.1'=>'DOMAINE',
'tr_acl_M.RM.1.1'=>'TOUT AFFICHER',
'tr_acl_M.RM.1.2'=>'VOIR SOUS-DOMAINE',
'tr_acl_M.RM.1.3'=>'VOIR PROCESSUS',
'tr_acl_M.RM.1.4'=>'MODIFIER',
'tr_acl_M.RM.1.5'=>'SUPPRIMER',
'tr_acl_M.RM.1.6'=>'INSÉRER DOMAINE',
'tr_acl_M.RM.1.7'=>'INSÉRER DOMAINE DE PREMIER NIVEAU',
'tr_acl_M.RM.1.8'=>'MODIFIER SI RESPONSABLE',
'tr_acl_M.RM.1.9'=>'SUPPRIMER SI RESPONSABLE',
'tr_acl_M.RM.2'=>'PROCESSUS',
'tr_acl_M.RM.2.1'=>'TOUT AFFICHER',
'tr_acl_M.RM.2.2'=>'VOIR ACTIFS',
'tr_acl_M.RM.2.4'=>'MODIFIER',
'tr_acl_M.RM.2.5'=>'SUPPRIMER',
'tr_acl_M.RM.2.6'=>'INSÉRER PROCESSUS',
'tr_acl_M.RM.2.7'=>'MODIFIER SI RESPONSABLE',
'tr_acl_M.RM.2.8'=>'SUPPRIMER SI RESPONSABLE',
'tr_acl_M.RM.3'=>'ACTIF',
'tr_acl_M.RM.3.1'=>'TOUT AFFICHER',
'tr_acl_M.RM.3.10'=>'CHARGER RISQUES DE BIBLIOTHÈQUE',
'tr_acl_M.RM.3.2'=>'VOIR RISQUES',
'tr_acl_M.RM.3.4'=>'NOUVEAU RISQUE',
'tr_acl_M.RM.3.5'=>'MODIFIER',
'tr_acl_M.RM.3.6'=>'SUPPRIMER',
'tr_acl_M.RM.3.7'=>'INSÉRER ACTIF',
'tr_acl_M.RM.3.8'=>'MODIFIER SI RESPONSABLE',
'tr_acl_M.RM.3.9'=>'SUPPRIMER SI RESPONSABLE',
'tr_acl_M.RM.4'=>'RISQUE',
'tr_acl_M.RM.4.1'=>'TOUT AFFICHER',
'tr_acl_M.RM.4.10'=>'SUPPRIMER SI RESPONSABLE',
'tr_acl_M.RM.4.2'=>'VOIR CONTRÔLES',
'tr_acl_M.RM.4.4'=>'MODIFIER',
'tr_acl_M.RM.4.5'=>'SUPPRIMER',
'tr_acl_M.RM.4.6'=>'INSÉRER RISQUE',
'tr_acl_M.RM.4.8'=>'ESTIMATION',
'tr_acl_M.RM.4.9'=>'MODIFIER SI RESPONSABLE',
'tr_acl_M.RM.5'=>'CONTRÔLE',
'tr_acl_M.RM.5.1'=>'TOUT AFFICHER',
'tr_acl_M.RM.5.2'=>'VOIR RISQUES',
'tr_acl_M.RM.5.3'=>'ASSOCIER RISQUES',
'tr_acl_M.RM.5.4'=>'MODIFIER',
'tr_acl_M.RM.5.5'=>'SUPPRIMER',
'tr_acl_M.RM.5.6'=>'INSÉRER CONTRÔLE',
'tr_acl_M.RM.5.7'=>'MODIFIER SI RESPONSABLE',
'tr_acl_M.RM.5.8'=>'SUPPRIMER SI RESPONSABLE',
'tr_acl_M.RM.5.9'=>'RÉVISION/TESTER HISTORIQUE',
'tr_acl_M.RM.6'=>'RAPPORTS',
'tr_acl_M.RM.6.1'=>'DES RISQUES',
'tr_acl_M.RM.6.1.1'=>'VALEURS DE RISQUE',
'tr_acl_M.RM.6.1.2'=>'RISQUES PAR PROCESSUS',
'tr_acl_M.RM.6.1.3'=>'RISQUES PAR ACTIF',
'tr_acl_M.RM.6.1.4'=>'RISQUES PAR CONTRÔLE',
'tr_acl_M.RM.6.1.5'=>'IMPACT DE RISQUE',
'tr_acl_M.RM.6.1.6'=>'IMPORTANCE D\'ACTIFS',
'tr_acl_M.RM.6.1.7'=>'RISQUES PAR DOMAINE',
'tr_acl_M.RM.6.1.8'=>'LISTE DE VÉRIFICATION PAR ÉVÉNEMENT',
'tr_acl_M.RM.6.2'=>'DES CONTRÔLES',
'tr_acl_M.RM.6.2.1'=>'CONTRÔLES PAR RESPONSABLE',
'tr_acl_M.RM.6.2.10'=>'SUIVI DES TESTS DE CONTRÔLES',
'tr_acl_M.RM.6.2.11'=>'CONFORMITÉ',
'tr_acl_M.RM.6.2.12'=>'ORDRE DU JOUR DE LA RÉVISION DE CONTRÔLE PAR RESPONSABLE',
'tr_acl_M.RM.6.2.13'=>'ORDRE DU JOUR DU TEST DE CONTRÔLE PAR RESPONSABLE',
'tr_acl_M.RM.6.2.2'=>'CONTRÔLES PAR RISQUE',
'tr_acl_M.RM.6.2.3'=>'PLANIFICATION DES CONTRÔLES',
'tr_acl_M.RM.6.2.4'=>'DATE DE MISE EN OEUVRE DES CONTRÔLES',
'tr_acl_M.RM.6.2.5'=>'EFFICACITÉ DES CONTRÔLES',
'tr_acl_M.RM.6.2.6'=>'RÉSUMÉ DES CONTRÔLES',
'tr_acl_M.RM.6.2.7'=>'SUIVI DES CONTRÔLES',
'tr_acl_M.RM.6.2.8'=>'SUIVI DE L\'EFFICACITÉ DES CONTRÔLES',
'tr_acl_M.RM.6.2.9'=>'CONTRÔLES NON MESURÉS',
'tr_acl_M.RM.6.3'=>'DE LA GESTION DES RISQUES',
'tr_acl_M.RM.6.3.1'=>'TÂCHES EN ATTENTE',
'tr_acl_M.RM.6.3.2'=>'DOMAINES SANS PROCESSUS',
'tr_acl_M.RM.6.3.3'=>'ACTIF SANS RISQUES',
'tr_acl_M.RM.6.3.4'=>'PROCESSUS SANS ACTIFS',
'tr_acl_M.RM.6.3.5'=>'RISQUES NON-ESTIMÉS',
'tr_acl_M.RM.6.3.6'=>'CONTRÔLES SANS RISQUES',
'tr_acl_M.RM.6.3.7'=>'DUPLICATA DE RISQUES',
'tr_acl_M.RM.6.4'=>'FINANCIER',
'tr_acl_M.RM.6.4.4'=>'COÛT PAR CONTRÔLE',
'tr_acl_M.RM.6.4.5'=>'COÛT DES CONTRÔLES PAR RESPONSABLE',
'tr_acl_M.RM.6.4.6'=>'COÛT DES CONTRÔLES PAR DOMAINE',
'tr_acl_M.RM.6.4.7'=>'COÛT DES CONTRÔLES PAR PROCESSUS',
'tr_acl_M.RM.6.4.8'=>'COÛT DES CONTRÔLES PAR ACTIF',
'tr_acl_M.RM.6.5'=>'ISO 31000',
'tr_acl_M.RM.6.5.1'=>'DÉCLARATION D\'APPLICABILITÉ',
'tr_acl_M.RM.6.5.2'=>'PLAN DE TRAITEMENT DES RISQUES',
'tr_acl_M.RM.6.5.3'=>'DÉCLARATION DU CHAMP D\'APPLICATION D\'ERMS',
'tr_acl_M.RM.6.5.4'=>'ÉNONCÉ DU RÈGLEMENT DE ERMS',
'tr_acl_M.RM.6.6'=>'RÉSUMÉS',
'tr_acl_M.RM.6.6.1'=>'TOP 10 - ACTIFS AVEC LE PLUS DE RISQUES',
'tr_acl_M.RM.6.6.2'=>'TOP 10 - ACTIFS AVEC LE PLUS DE RISQUES ÉLEVÉS',
'tr_acl_M.RM.6.6.3'=>'TOP 10 - RISQUES PAR DOMAINE',
'tr_acl_M.RM.6.6.4'=>'TOP 10 - RISQUES PAR PROCESSUS',
'tr_acl_M.RM.6.6.5'=>'NOMBRE DES RISQUES PAR PROCESSUS',
'tr_acl_M.RM.6.6.6'=>'NOMBRE DES RISQUES PAR DOMAINE',
'tr_acl_M.RM.6.6.7'=>'ÉTAT DES RISQUES PAR DOMAINE',
'tr_acl_M.RM.6.6.8'=>'ÉTAT DES RISQUES PAR PROCESSUS',
'tr_acl_M.RM.6.7'=>'AUTRES',
'tr_acl_M.RM.6.7.1'=>'RESPONSABILITÉS DES UTILISATEURS',
'tr_acl_M.RM.6.7.2'=>'OBJETS EN ATTENTE',
'tr_acl_M.RM.6.7.3'=>'ACTIFS',
'tr_acl_M.RM.6.7.4'=>'CLASSIFICATION D\'OBJET',
'tr_acl_M.RM.6.7.5'=>'ACTIFS - DEPENDANCES ET DEPENDANTS',
'tr_acl_M.RM.6.7.6'=>'PERTINENCE DE L\'ACTIF',
'tr_acl_M.S'=>'RECHERCHE',

/* './packages/admin/cfg_general.xml' */

'lb_session_expiracy'=>'Temps d\'expiration de séance:',
'st_minutes'=>'minutes.',
'to_session_expiracy'=>'<b>Temps d\'expiration de session:</b><br/><br/>Définit combien de temps d\'inactivité le système attend',
'vb_deactivate'=>'Désactiver',

/* './packages/admin/custom_asset_values_name.xml' */

'st_asset_parametrization_cl_bl'=>'<b>Étiquette d\'Estimation de l\'Importance de l\'Actif:</b>',
'st_importance'=>'Importance (Actif)',
'tt_asset_values_name_information'=>'Comment voulez-vous étiqueter vos paramètres d\'estimation de l\'importance de l\'actif?',

/* './packages/admin/custom_control.xml' */

'cb_revision'=>'Révision',
'cb_test'=>'Test',
'st_revision_and_test'=>'<b>Révision et Test de Contrôle</b>',
'st_revision_and_test_description'=>'Voulez-vous revoir vos contrôles de sécurité?',
'to_control_revision_config_bl_cl'=>'<b>Configuration de Révision de Contrôle:</b><br/><br/> Détermine si le système permettra la révision de l\'efficacité réelle des contrôles. <br/>Cette configuration vous permettra de déterminer si un contrôle est efficace ou non.',
'to_control_test_config_bl_cl'=>'<b>Configuration de tests de contrôle:</b> <br/><br/>Détermine si le système permet le test des contrôles.<br/>Cette configuration vous permettra de déterminer si un contrôle est fiable ou non.',

/* './packages/admin/custom_control_cost_name.xml' */

'lb_cost_name_1_bl'=>'<b>Nom de Coût 1</b>',
'lb_cost_name_2_bl'=>'<b>Nom de Coût 2</b>',
'lb_cost_name_3_bl'=>'<b>Nom de Coût 3</b>',
'lb_cost_name_4_bl'=>'<b>Nom de Coût 4</b>',
'lb_cost_name_5_bl'=>'<b>Nom de Coût 5</b>',
'st_costs_estimation_parameters_information'=>'Quelles sont les catégories de coûts que vous devez normalement compter dans l\'application des contrôles?',
'static_st_cost_estimation_parameters_bl'=>'<b>Paramètres d\'Estimation des Coûts</b>',

/* './packages/admin/custom_currency.xml' */

'lb_currency_cl'=>'Monnaie:',

/* './packages/admin/custom_financial_impact.xml' */


/* './packages/admin/custom_financial_impact_parameters.xml' */

'st_custom_financial_impact_parameter_note'=>'Entrez le nom des catégories sur lesquelles sera évalué l\'impact financier d\'un incident.',
'st_description_financial_impact_parameter'=>'Minimarisk vous permet de définir la source d\'éventuelles pertes financières associées à un cas de risque. Quelles sont les catégories de source de financement que vous souhaitez utiliser?',
'st_financial_impact_parameters'=>'<b>Paramètres d\'Impact Financier</b>',

/* './packages/admin/custom_impact_damage.xml' */


/* './packages/admin/custom_impact_type.xml' */


/* './packages/admin/custom_priority.xml' */


/* './packages/admin/custom_quantity_rm_parameters.xml' */

'lb_mode_cl'=>'Mode:',
'rb_3_three'=>'3 (trois)',
'rb_5_five'=>'5 (cinq)',
'rb_optimist'=>'Optimiste',
'rb_pessimist'=>'Pessimiste',
'st_custom_quantity_parameters_note'=>'OBS.: Vous pouvez changer les paramètres à l\'avenir',
'st_risk_quantity_values_bl_cl'=>'<b>Définition de la Matrice de Risque:</b>',
'tt_custom_quantity_rm_parameters_information'=>'Les meilleures pratiques recommandent que vous fournissiez l\'importance',

/* './packages/admin/custom_report_classification.xml' */

'lb_to_cl'=>'À:',
'st_report_classification'=>'<b>Classification de Rapport</b>',
'st_report_classification_information'=>'Quelle est la Classification (et à qui s\'applique cette classification) que vous voulez utiliser dans les onglets du système?',
'st_system_classification_bl'=>'<b>Classification de Système</b>',
'st_system_classification_information'=>'Quel type de classification (et à qui cette classification s\'applique) voulez-vous utiliser dans le système?',
'to_general_settings_classification'=>'<b>Classification:</b><br/>Indique sa classification',
'to_general_settings_classification_dest'=>'<b>To:</b><br/>Indique à qui le classement ci-dessus s\'applique. Ce champ ne prend effet que lorsque le classement a été rempli.',

/* './packages/admin/custom_risk_control_values_name.xml' */

'st_rc_impact'=>'Impact',
'st_rc_impact5_'=>'Impact',
'st_rc_probability'=>'Probabilité',
'st_risk_control_values_name'=>'<b>Étiquette de Critère de Réduction de Contrôle</b>',
'st_very_high'=>'<b>Très Élevé</b>',
'st_very_low'=>'<b>Très faible</b>',
'tt_risk_control_value_name_information'=>'Lorsque vous appliquez les contrôles pour réduire les risques',

/* './packages/admin/custom_risk_limits.xml' */

'lb_risk_limits_high_bl_cl'=>'<b>Tolérance au Risque 2:</b>',
'lb_risk_limits_low_bl_cl'=>'<b>Tolérance au Risque 1:</b>',
'st_risk_limits_definition'=>'Définir les valeurs pour la manipulation de risque.<br/><br/> Risques dont la valeur calculée est inférieure à la Tolérance de Niveau 1 seront mis en évidence avec la couleur verte.<br/> Risques dont la valeur calculée est supérieure à la Tolérance de Niveau 2 seront mis en évidence avec la couleur rouge.<br/> Risques dont la valeur calculée est entre ces deux Niveaux de Tolérance seront mis en évidence avec la couleur jaune.',
'tt_aceptable_risk_limits'=>'<b>Niveaux de Risque Acceptables</b>',
'tt_risk_limits'=>'Tolérance au Risque',
'wn_high_higher_than_max'=>'la valeur limite de risque 2 ne peut pas dépasser la limite spécifiée dans la configuration du système.',
'wn_low_higher_than_high'=>'La valeur limite de risque 1 ne peut pas être supérieure à la valeur limite de risque 2.',
'wn_low_lower_than_1'=>'La valeur limite de risque 1 ne peut pas être inférieur à 1.',
'wn_save_risk_matrix_update_risk_limits'=>'Le système a généré une tâche à l\'utilisateur<b>%chairman_name%</b> pour l\'approbation des nouvelles limites de risque. Jusqu\'à ce que cet utilisateur approuve les nouvelles limites',

/* './packages/admin/custom_risk_parameters_names.xml' */

'st_risk_parameters_bl_cl'=>'<b>Paramètres d\'Estimation de Risque:</b>',
'st_risk_parameters_help'=>'Entrez les noms des propriétés qui seront disponibles dans le logiciel pour l\'évaluation des risques. Nous proposons les propriétés suivantes: confidentialité',
'tt_risk_parameters_information'=>'Comment étiquetez-vous les Paramètres d\'Estimation de Risque?',
'wn_risk_parameters_minimum_requirement'=>'Au moins un paramètre de risque doit être spécifié.',

/* './packages/admin/custom_risk_parameters_weight.xml' */

'st_risk_parameters_weight'=>'<b>Poids de Paramètres de Risque</b>',
'st_risk_parameters_weight_definition'=>'Définir le poids de chaque paramètre de risque. Le poids doit être supérieur ou égal à zéro',
'wn_at_least_one_positive'=>'Au moins un poids doit être supérieur à zéro.',

/* './packages/admin/custom_risk_values_name.xml' */

'st_custom_risk_matrix_definition_generic_note'=>'"OBS.: Sélectionnez d\'abord la taille de la matrice de risque (3x3 ou 5x5) dans le sujet ""Définition de la Matrice de Risque""."',
'st_high'=>'<b>Élevé</b>',
'st_high_bl'=>'<b>Élevé</b>',
'st_impact'=>'Impact (Risque)',
'st_impact_risk'=>'Impact (Risque)',
'st_low'=>'<b>Faible</b>',
'st_low_bl'=>'<b>Faible</b>',
'st_medium'=>'<b>Moyen</b>',
'st_medium_bl'=>'<b>Moyen</b>',
'st_probability_risk'=>'Probabilité (Risque)',
'st_risk_parametrization'=>'<b>Etiquette d\'Estimation de l\'Impact & Probabilité:</b>',
'st_riskprob'=>'Probabilité (Risque)',
'st_values'=>'Valeurs',
'st_very_high_bl'=>'<b>Très Élevé</b>',
'st_very_low_bl'=>'<b>Très Faible</b>',
'tt_risk_values_name_information'=>'Comment voulez-vous étiqueter votre critère d\'estimation de l\'impact et de la probabilité?',

/* './packages/admin/custom_special_users.xml' */

'lb_asset_manager_bl'=>'<b>Gérant des Actifs</b>',
'lb_chairman_bl'=>'<b>Direction</b>',
'lb_control_manager_bl'=>'<b>Gérant des Contrôles</b>',
'lb_library_manager_bl'=>'<b>Gérant des Bibliothèques</b>',
'lb_user_disciplinary_process_manager_bl'=>'<b>Gérant des Processus Disciplinaires</b>',
'lb_user_evidence_manager_bl'=>'<b>Gérant des Évidences</b>',
'lb_user_incident_manager_bl'=>'<b>Gérant des Incidents</b>',
'lb_user_non_conformity_manager_bl'=>'<b>Gérant des Non-Conformités</b>',
'st_custom_special_users_information'=>'Le workflow Minimarisk requiert la définition de certains utilisateurs chargés de tâches spéciales prédéterminées dans le cadre d\'un ERMS. Qui occupera le rôle de ces utilisateurs dans votre système?',
'st_help'=>'[?]',
'st_special_users_bl'=>'<b>Utilisateurs Spéciaux de Minimarisk</b>',
'tt_special_user'=>'<b>Direction:</b><br/><br/>utilisateur responsable de l\'approbation des premiers domaines d\'activité',
'tt_special_user_asset_manager'=>'<b>Gérant des Actifs:</b><br/><br/>utilisateur en charge du contrôle de l\'inventaire des actifs',
'tt_special_user_control_manager'=>'<b>Gérant des Contrôles:</b><br/><br/>utilisateur responsable de la gestion des contrôles de sécurité',
'tt_special_user_disciplinary_process_manager'=>'<b>Gérant des Processus Disciplinaires:</b><br/><br/>utilisateur qui reçoit les alertes des Processus Disciplinaires',
'tt_special_user_evidence_manager'=>'<b>Gérant des Évidences:</b><br/><br/>utilisateur qui reçoit les alertes de collection des évidences',
'tt_special_user_incident_manager'=>'<b>Incident Manager:</b><br/><br/>utilisateur responsable de l\'approbation',
'tt_special_user_library_manager'=>'<b>Gérant des Bibliothèques:</b><br/><br/>utilisateur responsable du contrôle des bibliothèques de Minimarisk',
'tt_special_user_nc_manager'=>'<b>Gérant des Non-Conformités:</b><br/><br/>utilisateur responsable de l\'approbation des non-conformités créées par d\'autres utilisateurs Minimarisk.',

/* './packages/admin/custom_system_features.xml' */

'cb_auto_document_creation'=>'Générer automatiquement les documents après la création des éléments dans le système?',
'cb_cost'=>'Utiliser le Dispositif d\'Estimation des Coûts?',
'cb_document_manual_versioning'=>'Voulez-vous utiliser la Versionisation Manuelle de Document?',
'cb_percentual_risk'=>'Montrer Valeurs de Risques en échelle de 0 à 100?',
'cb_solicitor'=>'Permettre l\'avocat à partager les droits d\'accès',
'st_data_collecting'=>'Permettre la Collection Quotidienne de Données afin de pouvoir générer les statistiques Minimarisk.',
'st_manual_data_control'=>'Voulez-vous utiliser le Contrôle Manuel des Dates?',
'st_risk_formula'=>'Utilisez la Formule Proportionnelle de Risque.',
'st_system_features'=>'<b>Caractéristiques du Système</b>',
'to_data_collecting_settings'=>'<b>Paramètres de Collection de Données:</b><br/><br/>Détermine si le système va recueillir des informations des bases de données du système afin de pouvoir générer l\'historique système indiqué sur l\'onglet des statistiques.',
'to_delete_cascade'=>'<b>Contrôle Manuel des Dates du Système:</b><br/><br/>Permet à l\'utilisateur de définir des dates dans le passé et/ou dans l\'avenir dans les parties du système dans lesquelles',
'to_document_manual_versioning'=>'<b>Versionisation Manuelle de Document:</b><br/><br/>Détermine si le système utilisera la versionisation manuelle ou automatique de documents.',
'to_general_settings_automatic_documents_creation'=>'<b>Paramètres de Création Automatique de Documents:</b><br/><br/>Détermine si le système va créer automatiquement un document pour chaque objet (domaine',
'to_percentual_risk_configuration'=>'<b>Configuration de l\'échelle des valeurs de risque:</b><br/><br/>Détermine si les valeurs de risque devraient être montrées en valeur absolue (de 0 à 9 ou de 0 à 25) ou à l\'échelle de 0 à 100.',
'to_procurator_configuration'=>'<b>Configuration d\'Avocat:</b><br/><br/>Déterminer si le système permet le mécanisme d\'acquisition en l\'absence temporaire d\'un utilisateur.',
'to_risk_formula'=>'<b>Formule Proportionnelle de Risque:</b><br/><br/>Formule qui prends en considération les poids de l\'importance et de la conséquence afin d\'augmenter les valeurs finales de chaque risque.',
'to_system_cost_configuration'=>'<b>Configuration de Coût:</b><br/><br/>Détermine si le système permettra le calcul des coûts.',

/* './packages/admin/custom_type_and_priority_parametrization.xml' */

'si_document'=>'Document',
'si_event'=>'Événement',
'si_register'=>'Registre',
'si_risk'=>'Risque',
'st_description_type_and_priority_parametrization'=>'Comment étiquetez vous vos éléments?',
'st_element'=>'<b>Élément:</b>',
'st_priority_bl'=>'<b>Priorité</b>',
'st_type_and_priority_parametrization'=>'<b>Paramétrage de Type et de Priorité</b>',
'st_type_bl'=>'<b>Type</b>',
'wn_classification_minimum_requirement'=>'Au moins trois classifications doivent être précisées pour chaque objet.',

/* './packages/admin/nav_audit_alert.xml' */

'gc_justification'=>'Raison',
'tt_audit_alerts_bl'=>'<b>Alerte d\'Audit</b>',

/* './packages/admin/nav_audit_log.xml' */

'lb_user_cl'=>'Utilisateur:',
'si_all_actions'=>'Toutes les Actions',
'si_all_users'=>'Tous les Utilisateurs',
'tt_audit_log_bl'=>'<b>Journal d\'Audit</b>',

/* './packages/admin/nav_audit_task.xml' */

'gc_accomplishment'=>'Réalisation',
'gc_creation'=>'Création',
'gc_receiver'=>'Récepteur',
'lb_activity_cl'=>'Activité:',
'lb_creator_cl'=>'Créateur:',
'lb_receiver_cl'=>'Récepteur:',
'si_all_activities'=>'Toutes les Activités',
'si_all_creators'=>'Tous les créateurs',
'si_all_receivers'=>'Tous les Récepteurs',
'tt_audit_task_bl'=>'<b>Équipe de Vérification</b>',

/* './packages/admin/nav_config.php' */

'st_check_for_correct_data'=>'Vérifiez si les données ont été remplies correctement.',
'st_impact_parameter_remove_message'=>'La suppression d\'un paramètre d\'impact financier entraînera la suppression de ce paramètre dans les incidents déjà estimés. Voulez-vous quand même procéder?',
'st_info_saved_but'=>'L\'information a été enregistrée. Cependant',
'st_not_possible_to_update_data_collection_info'=>'il n\'a pas été possible de mettre à jour les informations de collection de données',
'st_not_possible_to_update_mail_server_info'=>'il n\'a pas été possible de mettre à jour les informations du serveur email',
'st_unavailable_information'=>'information non disponible.',
'tt_alert'=>'Alerte',
'tt_deactivate_system'=>'Désactiver le Système',
'tt_impact_parameter_remove'=>'Supprimer les Paramètres d\'Impact Financier',

/* './packages/admin/nav_config.xml' */

'si_costs_names'=>'Noms des Coûts',
'si_parametrization'=>'Estimation',
'si_risk_parameters_values'=>'Valeurs des Paramètres de Risque',
'si_syslog'=>'Syslog',
'si_system'=>'Système',

/* './packages/admin/nav_customization.php' */

'st_change_amount_of_parameters_confirm'=>'Modifier le nombre de paramètres rendra le système non estimé.<br/><b>Êtes-vous sûr de vouloir le faire?</b>',
'st_change_amount_of_risks_confirm'=>'Modification du nombre des risques forcera le système à refaire tous ses calculs de risque!<br/></b>Etes-vous sûr de vouloir le faire?</b>',
'st_remove_classification_error'=>'Vos modifications ont été enregistrées. Cependant',
'st_risk_parameters_weight_edit_confirm'=>'Vous changez des données sensibles. Lorsque vous modifiez le poids des paramètres',
'tt_remove_classification_error'=>'Erreur lors de l\'enlèvement de la classification',
'tt_save_parameters_names'=>'Enregistrer Noms de Paramètres',
'tt_save_parameters_values'=>'Sauvegarder les Valeurs de Paramètres',
'tt_sensitive_data'=>'Données Sensibles',

/* './packages/admin/nav_customization.xml' */

'lb_settings_cl'=>'Personnalisation:',
'si_aceptable_risk_limits'=>'Niveaux de Risque Acceptables',
'si_control_cost_names'=>'Paramètres d\'Estimation des Coûts',
'si_control_revision_and_test'=>'Révision et Test de Contrôle',
'si_financial_impact_parameters'=>'Paramètres d\'Impact Financier',
'si_impact_and_importance_dimensions'=>'Les paramètres d\'estimation de risque',
'si_parametrization_asset'=>'Étiquette d\'Estimation de l\'Importance de l\'Actif',
'si_parametrization_control'=>'Étiquette du Contrôle de critère de réduction',
'si_parametrization_risk'=>'Étiquette d\'Estimation de l\'Impact & Probabilité',
'si_priority_and_type_parametrization'=>'Paramétrage de Type et de Priorité',
'si_report_classification'=>'Classification de Rapport',
'si_risk_parameters_weight'=>'Poids des Paramètres de Risque',
'si_special_users'=>'Utilisateurs Spéciaux',
'si_system_classification'=>'Classification du Système',
'si_system_currency'=>'Monnaie',
'si_system_features'=>'Caractéristiques du système',
'si_system_rm_parameters_quantity'=>'Définition de Matrice des Risques',
'wn_updating_risk_values'=>'Modifications sauvegardées avec succès. Attendez pendant que les valeurs de risques sont recalculées.',
'wn_values_successfully_changed'=>'Les valeurs ont été mises à jour avec succès.',

/* './packages/admin/nav_my_account.php' */

'st_activation_date'=>'Date d\'Activation',
'st_cancel_service'=>'Service d\'Annulation',
'st_cancel_service_email_message'=>'Un service de demande d\'annulation a été faite par le client \'%client%\'.',
'st_client_name'=>'Nom du Client',
'st_confirm_cancel_service_message'=>'Êtes-vous sûr de vouloir annuler le service?',
'st_deactivate_account_text'=>'Pour désactiver votre compte',
'st_expiracy_date'=>'Date d\'Expiration',
'st_expiracy_date_never'=>'Ne jamais',
'st_license_type'=>'Type de Licence',
'st_max_simult_users'=>'Max. Utilisateurs simultanés',
'st_number_of_users'=>'Nombre d\'Utilisateurs',
'tt_cancel_service'=>'Service d\'Annulation',
'tt_confirm_cancel_service'=>'Annulation de Service',
'tt_deactivate_account'=>'Désactiver le Compte',

/* './packages/admin/nav_my_account.xml' */

'lb_email_cl'=>'E-mail:',
'st_cancel_service_bl'=>'<b>Annulation de Service</b>',
'st_change_email_bl'=>'<b>Changement d\'e-mail administratif</b>',
'st_deactivate_account_bl'=>'<b>Désactivation du Compte</b>',
'st_license_info_bl'=>'<b>Information de Licence:</b>',
'st_upgrade_license_bl'=>'<b>Mise à niveau de Licence</b>',
'st_upgrade_license_message'=>'"S\'il vous plaît activer vos packs d\'utilisateur en utilisant les codes d\'activation qui ont été envoyées par e-mail. Remplissez vos codes d\'activation ci-dessous',
'st_upgrade_license_text'=>'"S\'il vous plaît activez votre Pack d\'Utilisateur (pour 10 utilisateurs) en vous servant du Code d\'Activation qui a été envoyé par e-mail. Entrez votre Code d\'Activation ci-dessous',
'vb_activate'=>'Activer',
'vb_cancel_service'=>'Annuler Service',
'vb_change'=>'Changer',
'vb_deactivate_account'=>'Désactiver Mon Compte',
'vb_upgrade'=>'Mise à jour',
'wn_email_changed'=>'e-mail a été changé avec succès',
'wn_invalid_activation_code'=>'Certains codes ne sont pas valides. S\'il vous plaît',
'wn_successful_activation'=>'Licence mise à jour avec succès.',

/* './packages/admin/nav_profile.php' */

'st_profile_remove_message'=>'Etes-vous sûr de vouloir supprimer le Profil<b>%profile_name%</b>?',
'tt_remove_profile'=>'Supprimer Profil',

/* './packages/admin/nav_profile.xml' */

'mi_visualize'=>'Voir',
'tt_profiles_bl'=>'<b>Profils</b>',

/* './packages/admin/nav_system_configuration_on_site.xml' */

'lb_system_settings_cl'=>'Paramètres du Système',
'si_delete_cascade'=>'Supprimer en Cascade',
'si_email_setup'=>'Cette configuration détermine le fuseau horaire qui sera utilisé par le système.',
'si_password_policy'=>'Règlement des Mots de Passe',
'si_server_setup'=>'Choisir un fuseau horaire à utiliser:',
'si_timezone_setup'=>'<b>Sélectionner le fuseau horaire</b>',
'wn_changes_successfully_saved'=>'Les modifications ont été enregistrées avec succès.',

/* './packages/admin/nav_trash.php' */

'st_all_items_remove_confirm'=>'Tous les Objets dans la Corbeille seront définitivement supprimés! Voulez-vous continuer?',
'st_items_restore_confirm'=>'Souhaitez-vous restaurer les objets sélectionnés?',
'st_not_excluded_users_and_profile_message'=>'Les utilisateurs sélectionnés n\'ont pas pu être supprimés car ils sont liées à des objets existants se trouvant dans la corbeille. <br/> Les profils sélectionnés n\'ont pas pu être supprimés car ils sont liées à des utilisateurs existants se trouvant dans la corbeille qui n\'étaient pas sélectionnés ou qui n\'ont pas pu être supprimés.',
'st_not_removed_profiles_message'=>'Les Profils sélectionnés n\'ont pas pu être supprimés car ils sont liées à des utilisateurs existants se trouvant dans la corbeille qui n\'étaient pas sélectionnés ou qui n\'ont pas pu être supprimés.',
'st_not_restored_documents_message'=>'Les documents sélectionnés n\'ont pas pu être restaurés',
'st_not_restored_items_message'=>'Les objets sélectionnés n\'ont pas pu être restaurés car ils sont liés à un ou plusieurs utilisateurs qui sont toujours supprimés!',
'st_not_restored_users_message'=>'Les Utilisateurs sélectionnés n\'ont pas pu être supprimés car ils sont liés à des objets existants se trouvant dans la corbeille.',
'st_not_selected_items_remove_confirm'=>'Les objets non sélectionnés et qui sont des sous-objets ou sont associés aux objets sélectionnés seront supprimés. Voulez-vous continuer?',
'tt_all_items_permanent_removal'=>'Suppression définitive de tous les objets',
'tt_document_not_restored'=>'Documents non restaurés',
'tt_items_not_restored'=>'Objets non restaurés',
'tt_items_permanent_removal'=>'Suppression définitive d\'objets',
'tt_items_restoral'=>'Restauration d\'Objets',
'tt_profile_not_deleted'=>'Les Profils n\'ont pas été supprimés',
'tt_users_and_profile_not_deleted'=>'Les Utilisateurs et les profils n\'ont pas été supprimés',
'tt_users_not_deleted'=>'Les Utilisateurs n\'ont pas été supprimés',

/* './packages/admin/nav_trash.xml' */

'tt_bin_bl'=>'<b>Corbeille</b>',
'vb_remove_all'=>'Supprimer Tout',
'wn_elements_removed_successfully'=>'Objet(s) supprimé(s) avec succès.',
'wn_elements_restaured_successfully'=>'Objet(s) restauré(s) avec succès.',

/* './packages/admin/nav_users.php' */

'em_alerts_and_tasks_transfer'=>'Transfert de Tâches et Alertes',
'st_choose_user_message'=>'Dans l\'écran suivant',
'st_reset_all_users_passwords_confirm'=>'Etes-vous sûr de vouloir réinitialiser le Mot de Passe de<b>tous les utilisateurs</b> et d\'envoyer le nouveau Mot de Passe à leurs e-mails?',
'st_user_remove_confirm'=>'Etes-vous sûr de vouloir supprimer l\'Utilisateur<b>%user_name%</b>?',
'tt_choose_user_warning'=>'Avertissement de Choix d\'Utilisateur',
'tt_remove_user'=>'Supprimer Utilisateur',
'tt_reset_all_users_password'=>'Réinitialiser le Mot de Passe de Tous les Utilisateurs',

/* './packages/admin/nav_users.xml' */

'gc_email'=>'E-mail',
'gc_profile'=>'Profil',
'mi_block'=>'Bloquer',
'mi_unblock'=>'Débloquer',
'tt_users_bl'=>'<b>Utilisateurs</b>',
'vb_reset_passwords'=>'Réinitialisation des Mots de passe',

/* './packages/admin/popup_associate_elements.xml' */

'lb_associate_to_cl'=>'Associer à:',
'tt_items_association'=>'Association d\'Objets',
'vb_cancel_all'=>'Annuler tous les',

/* './packages/admin/popup_configure_authentication.php' */

'st_connection_failed'=>'La connexion a échoué.',
'st_database_structure_wrong'=>'La base de données ne dispose pas de la structure prévue.',
'st_successful_connection'=>'Connexion Réussie.',

/* './packages/admin/popup_configure_authentication.xml' */

'lb_databse_bl_cl'=>'<b>Base de Données:</b>',
'lb_host_bl_cl'=>'<b>Hôte:</b>',
'lb_password_bl_cl'=>'<b>Mot de Passe:</b>',
'st_postgres_authentication_data_needed'=>'Les données ci-dessous sont nécessaires pour permettre au système d\'accéder à la base de données contenant des informations sur le calendrier postgres.',
'tt_setup_authentication'=>'Authentification du Programme d\'Installation',
'vb_test'=>'Test',

/* './packages/admin/popup_deactivate.php' */

'em_deactivate_account'=>'Désactivation de Compte',
'em_deactivate_account_email_message'=>'Client \'%client%\' a demandé la désactivation de son compte.<br/><b>Raison: </b><br/>%deactivation_reason%',

/* './packages/admin/popup_deactivate.xml' */

'lb_deactivation_reason_cl'=>'S\'il vous plaît',
'st_confirm_deactivate_account_message'=>'Etes-vous sûr que vous voulez désactiver votre compte?',
'tt_confirm_deactivate_account'=>'Désactiver le Compte',

/* './packages/admin/popup_no_access.xml' */

'st_user_wo_system_access'=>'Vous n\'avez pas accès au système. Pressez<b>Ok</b> ou fermez cette fenêtre pour vous déconnecter.',
'tt_user_wo_system_access'=>'Utilisateur sans autorisation d\'accès au système',

/* './packages/admin/popup_profile_edit.php' */

'tt_edit_profile'=>'Modifier Profil',
'tt_insert_profile'=>'Ajouter Profil',
'tt_visualizae_profile'=>'Afficher Profil',

/* './packages/admin/popup_profile_edit.xml' */

'st_profile_warning'=>'* Ce profil ne peut pas être modifié.',

/* './packages/admin/popup_restore_name_conflict.xml' */

'st_name_conflicts_message'=>'Il a été détecté qu\'un ou plusieurs objets ont présenté un conflit de nom',
'tt_name_conflicts_found'=>'Conflits de Nom Trouvé',

/* './packages/admin/popup_restore_structure_and_name_conflict.xml' */

'rb_all_except_name_conflict'=>'Tous',
'rb_all_except_structure_conflict'=>'Tous sauf ceux avec des conflits structurels',
'st_structure_name_conflicts_message'=>'Un conflit de structure et de nom a été détecté pour un ou plusieurs objets sélectionnés. Un conflit de structure survient quand il y a des éléments non sélectionnés nécessaires pour faire la restauration de certains objets. Un conflit de nom survient quand il y a un objet actif du système avec le même type et avec le même nom qu\'un objet qui sera restauré. Quels objets voulez-vous restaurer?',
'tt_structure_and_name_conflicts_found'=>'Conflits de Structure et de Nom trouvés',
'wn_not_restoring_name_conflict'=>'* Ne pas restaurer des objets avec des conflits de nom peut causer des conflits structurels.',

/* './packages/admin/popup_restore_structure_conflict.xml' */

'cb_associate_structure_conflicted_items_individually'=>'Individuellement associer les objets avec des conflits structurels à des éléments existants dans le système.',
'rb_all'=>'Tous',
'rb_only_not_conflicting'=>'Seuls ceux qui n\'ont pas présenté de conflit',
'st_structure_conflicts_message'=>'Il a été détecté qu\'un ou plusieurs objets ont présenté des conflits de structure',
'tt_structure_conflicts_found'=>'Conflits de Structure Trouvés',
'vb_restore'=>'Restaurer',

/* './packages/admin/popup_upgrade_license.xml' */

'lb_activation_code_cl'=>'Code d\'activation:',
'tt_upgrade_license'=>'Mise à Niveau de Licence',

/* './packages/admin/popup_user_edit.php' */

'em_new_password'=>'Nouveau Mot de Passe',
'em_new_user'=>'Bienvenue sur Minimarisk',
'em_new_user_footer_bl'=>'<b>Pour plus d\'information',
'st_reset_user_password_confirm'=>'Etes-vous sûr de vouloir réinitialiser le Mot de Passe de l\'utilisateur et d\'envoyer le nouveau Mot de Passe à son e-mail?',
'tt_profile_edit_error'=>'Erreur lors du changement de profil',
'tt_reset_user_password'=>'Réinitialisation du Mot de Passe de l\'Utilisateur',
'tt_user_adding'=>'Ajout d\'Utilisateur',
'tt_user_editing'=>'Modification d\'Utilisateur',
'tt_user_insert_error'=>'Limite d\'utilisateur atteinte',
'wn_users_limit_reached'=>'"Le nombre d\'utilisateurs détenteurs de licence a été atteint. A partir de maintenant',

/* './packages/admin/popup_user_edit.xml' */

'cb_change_password_on_login'=>'Changer le Mot de Passe à la prochaine connexion',
'lb_confirmation_cl'=>'Confirmation:',
'lb_email_bl_cl'=>'<b>E-mail:</b>',
'lb_language_bl_cl'=>'<b>Langue:</b>',
'lb_login_bl_cl'=>'<b>Connexion:</b>',
'lb_new_password_cl'=>'Nouveau Mot de Passe:',
'lb_profile_bl_cl'=>'<b>Profil:</b>',
'st_language_bl_cl'=>'<b>Langue:</b>',
'vb_reset_password'=>'Réinitialiser le Mot de Passe',
'wn_different_passwords'=>'Mots de passe différents',
'wn_existing_email'=>'Email existe déjà',
'wn_existing_login'=>'Connexion existe déjà',
'wn_invalid_email'=>'E-mail invalide',
'wn_password_changed'=>'Mot de Passe modifié',
'wn_weak_password'=>'Votre Mot de Passe ne remplit pas les exigences du règlement de mots de passe. S\'il vous plaît',
'wn_wrong_current_password'=>'Mot de Passe incorrect',

/* './packages/admin/report/popup_report_audit_alert.php' */

'rs_alerts_audit'=>'Alertes d\'Audit',

/* './packages/admin/report/popup_report_audit_log.php' */

'rs_systems_log_audit'=>'Audit du Journal de Système',

/* './packages/admin/report/popup_report_audit_task.php' */

'rs_tasks_audit'=>'Audit de Tâches',

/* './packages/admin/report/popup_report_audit_task.xml' */

'rs_accomplishment'=>'Réalisation',
'rs_creation'=>'Création',
'rs_creator'=>'Créateur',
'rs_receiver'=>'Récepteur',

/* './packages/admin/system_configuration_delete_cascade.xml' */

'cb_delete_cascade'=>'Supprimer en Cascade',
'st_delete_cascade'=>'<b>Supprimer en Cascade</b>',
'st_system_configuration_delete_cascade_information'=>'Détermine si l\'élimination d\'un élément entraînera la suppression automatique des sous-éléments.<br/>Si \'Supprimer en Cascade\' est désactivé',

/* './packages/admin/system_configuration_email_setup.xml' */

'lb_email_encryption_cl'=>'GMT +6',
'lb_email_host_cl'=>'GMT +2',
'lb_email_password_cl'=>'GMT +5',
'lb_email_port_cl'=>'GMT +3',
'lb_email_user_cl'=>'GMT +4',
'si_email_none'=>'GMT +7',
'si_email_ssl'=>'GMT +9',
'si_email_ssl2'=>'GMT +10',
'si_email_ssl3'=>'GMT +11',
'si_email_tls'=>'GMT +8',
'st_configuration_email_config_information'=>'GMT +1',
'st_email_config_bl_cl'=>'GMT',

/* './packages/admin/system_configuration_password_policy.xml' */

'cb_case_characters'=>'Majuscules/Minuscules',
'cb_numeric_characters_cl'=>'Caractères Numériques:',
'cb_special_characters_cl'=>'Caractères Spéciaux:',
'lb_days_before_warning'=>'Avertir de l\'expiration du Mot de Passe:',
'lb_label_block_limit_cl'=>'Limite de Blocage:',
'lb_minimum_characters_cl'=>'Longueur Minimum:',
'lb_password_history_apply_cl'=>'Appliquer l\'histoire des mots de passe:',
'lb_password_max_lifetime_cl'=>'Durée de vie maximale du Mot de Passe:',
'st_configuration_password_policy_information'=>'Quelle est le règlement des mots de passe que vous voulez utiliser dans le système?',
'st_days_before'=>'le jour avant.',
'st_default_configuration'=>'Réinitialisation de la configuration par défaut',
'st_invalid_logon'=>'Tentatives de connexion invalides',
'st_memorized_passwords'=>'mots de passe mémorisés.',
'st_password_policy_bl_cl'=>'<b>Règlement des Mots de passe:</b>',
'wn_min_char_warning'=>'La longueur minimale doit être supérieure ou égale au nombre de caractères spéciaux',

/* './packages/admin/system_configuration_server_setup.xml' */

'lb_server_dns_cl'=>'GMT -3',
'lb_server_gateway_cl'=>'GMT -4',
'lb_server_ip_cl'=>'GMT -2',
'lb_server_update_cl'=>'GMT -5',
'st_configuration_server_config_information'=>'GMT -1',
'st_server_config_bl_cl'=>'GMT +12',

/* './packages/admin/system_configuration_timezone_setup.xml' */

'cb_timezone_setup'=>'GMT -8',
'si_timezone_-1'=>'GMT -1',
'si_timezone_-10'=>'GMT -10',
'si_timezone_-11'=>'GMT -11',
'si_timezone_-12'=>'GMT -12',
'si_timezone_-2'=>'GMT -2',
'si_timezone_-3'=>'GMT -3',
'si_timezone_-4'=>'GMT -4',
'si_timezone_-5'=>'GMT -5',
'si_timezone_-6'=>'GMT -6',
'si_timezone_-7'=>'GMT -7',
'si_timezone_-8'=>'GMT -8',
'si_timezone_-9'=>'GMT -9',
'si_timezone_+1'=>'GMT -10',
'si_timezone_+10'=>'GMT +10',
'si_timezone_+11'=>'GMT +11',
'si_timezone_+12'=>'GMT +12',
'si_timezone_+2'=>'GMT -11',
'si_timezone_+3'=>'GMT -12',
'si_timezone_+4'=>'Choisissez le fuseau horaire',
'si_timezone_+5'=>'Date',
'si_timezone_+6'=>'GMT +6',
'si_timezone_+7'=>'GMT +7',
'si_timezone_+8'=>'GMT +8',
'si_timezone_+9'=>'GMT +9',
'si_timezone_0'=>'GMT -9',
'st_system_configuration_timezone_setup_information'=>'GMT -7',
'st_timezone_setup'=>'GMT -6',

/* './packages/admin/tab_audit.xml' */

'ti_alerts'=>'Alertes',
'ti_log'=>'Journal',
'ti_tasks'=>'Tâches',

/* './packages/admin/tab_main.php' */

'st_survey_link'=>'http://www.surveymonkey.com/s.aspx?sm=F9BFTdst8Ginn7PdgNGxFA_3d_3d',
'to_ip_cl'=>'IP:',
'to_last_login_cl'=>'Dernière Connexion:',

/* './packages/admin/tab_main.xml' */

'lb_feedback'=>'Commentaires',
'mx_bug'=>'Bug',
'mx_content_issue'=>'Suggestion',
'mx_feature_request'=>'Amélioration',
'st_about_isms_bl'=>'<b>À propos de</b>',
'st_buy_now_bl'=>'<b>Acheter Maintenant</b>',
'st_pipe_bl'=>'<b>|</b>',
'st_preferences_bl'=>'<b>Préférences</b>',
'st_satisfaction_survey_bl'=>'<b>Sondage d\'Expérience des Utilisateurs</b>',
'st_send_us_feedback_cl'=>'Laisser un commentaire:',
'st_software_license_bl'=>'<b>Licence</b>',
'ti_audit'=>'Audit',
'ti_bin'=>'Corbeille',
'ti_customization'=>'Personnalisation',
'ti_my_account'=>'Mon compte',
'ti_profiles'=>'Profils',
'ti_system_settings'=>'Paramètres du Système',
'ti_users'=>'Les Utilisateurs',
'vb_exit'=>'Déconnexion',
'vb_feedback'=>'Commentaires',
'vb_help'=>'Aide',
'vb_user_view'=>'Vue d\'utilisateur',

/* './packages/continuity/nav_asset.xml' */


/* './packages/continuity/nav_place_threat.xml' */


/* './packages/continuity/nav_places.xml' */


/* './packages/continuity/nav_plan.xml' */


/* './packages/continuity/nav_plan_action.xml' */


/* './packages/continuity/nav_plan_schedule.xml' */


/* './packages/continuity/nav_plan_test.xml' */


/* './packages/continuity/nav_process.xml' */


/* './packages/continuity/nav_process_activity_asset.xml' */


/* './packages/continuity/nav_process_scene.xml' */


/* './packages/continuity/nav_process_threat.xml' */


/* './packages/continuity/nav_providers.xml' */


/* './packages/continuity/nav_resources.xml' */


/* './packages/continuity/nav_scope.xml' */


/* './packages/continuity/nav_un.xml' */


/* './packages/continuity/popup_about_asset.xml' */


/* './packages/continuity/popup_activity_edit.xml' */


/* './packages/continuity/popup_address_locator.xml' */


/* './packages/continuity/popup_area_search.xml' */


/* './packages/continuity/popup_asset_associate_threat.xml' */


/* './packages/continuity/popup_asset_edit.xml' */


/* './packages/continuity/popup_department_edit.xml' */


/* './packages/continuity/popup_financial_impact_edit.xml' */


/* './packages/continuity/popup_group_edit.xml' */


/* './packages/continuity/popup_group_resource_single_search.xml' */

'mx_friday'=>'Vendredi',

/* './packages/continuity/popup_group_search.xml' */


/* './packages/continuity/popup_group_single_search.xml' */


/* './packages/continuity/popup_people_search.xml' */


/* './packages/continuity/popup_place_asset_edit.xml' */


/* './packages/continuity/popup_place_associate_threat.xml' */


/* './packages/continuity/popup_place_edit.xml' */


/* './packages/continuity/popup_place_resource_edit.xml' */


/* './packages/continuity/popup_place_search.xml' */


/* './packages/continuity/popup_place_single_search.xml' */


/* './packages/continuity/popup_plan_action_edit.xml' */


/* './packages/continuity/popup_plan_action_search.xml' */


/* './packages/continuity/popup_plan_schedule_edit.xml' */


/* './packages/continuity/popup_plan_search.xml' */


/* './packages/continuity/popup_plan_test_edit.xml' */


/* './packages/continuity/popup_plan_type_edit.xml' */


/* './packages/continuity/popup_planRange_edit.xml' */


/* './packages/continuity/popup_process_draw.xml' */


/* './packages/continuity/popup_process_edit.xml' */


/* './packages/continuity/popup_process_search.xml' */


/* './packages/continuity/popup_provider_edit.xml' */


/* './packages/continuity/popup_provider_search.xml' */


/* './packages/continuity/popup_provider_single_search.xml' */


/* './packages/continuity/popup_resource_edit.xml' */


/* './packages/continuity/popup_resource_search.xml' */


/* './packages/continuity/popup_scene_edit.xml' */


/* './packages/continuity/popup_scene_impact_edit.xml' */


/* './packages/continuity/popup_scope_search.xml' */


/* './packages/continuity/popup_scope_view.xml' */


/* './packages/continuity/popup_vulnerability_edit.xml' */


/* './packages/continuity/tab_continuity_management.xml' */


/* './packages/improvement/nav_action_plan.php' */

'mx_corrective'=>'Correctif',
'mx_preventive'=>'Préventif',
'st_ap_remove_message'=>'Etes-vous sûr de vouloir supprimer le Plan d\'Action<b>%name%</b>?',
'tt_ap_remove'=>'Supprimer Plan d\'Action',
'tt_nc_filter_grid_action_plan'=>'(filtré par non-conformité \'<b>%nc_name%</b>\')',

/* './packages/improvement/nav_action_plan.xml' */

'gc_actiontype'=>'Type d\'Action',
'gc_ap_cns'=>'Non-Conformités',
'gc_dateconclusion'=>'Date de Finition',
'gc_datedeadline'=>'Date Limite de Délai',
'gc_isefficient'=>'Efficace',
'gc_responsible_id'=>'Responsable',
'mi_data_to_meassure'=>'Définir Mesure',
'mi_meassure'=>'Mesure',
'tt_ap_bl'=>'<b>Plan d\'Action</b>',

/* './packages/improvement/nav_disciplinary_process.xml' */

'gc_action_taken'=>'Action Exécutée',
'mi_register_action'=>'Action de Registre',

/* './packages/improvement/nav_incident.php' */

'mx_direct'=>'Direct',
'mx_indirect'=>'Indirect',
'mx_no_category'=>'Pas de Catégorie',
'st_incident_remove_confirm'=>'Etes-vous sûr de vouloir supprimer l\'incident<b>%incident_name%</b>?',
'tt_incident_remove_confirm'=>'Suppression des Incidents',

/* './packages/improvement/nav_incident.xml' */

'gc_incident_date_limit'=>'Estimée',
'gc_loss_type'=>'Type de Perte',
'mi_disciplinary_process'=>'Processus Disciplinaire',
'mi_finish'=>'Terminer',
'mi_immediate_disposal_approval'=>'Approbation d\'Élimination Immédiate',
'mi_solution_approval'=>'Solution d\'Approbation',
'tt_incident'=>'<b>Incident</b>',

/* './packages/improvement/nav_non_conformity.php' */

'gc_control_security'=>'Contrôle de Sécurité',
'gc_external_audit'=>'Audit Externe',
'gc_internal_audit'=>'Audit Interne',
'gc_no'=>'Non',
'gc_yes'=>'Oui',
'st_nc_remove_message'=>'Etes-vous sûr que vous souhaitez supprimer la non-conformité<b>%name%</b>?',
'tt_ap_filter_grid_non_conformity'=>'(filtré par le plan d\'action \'<b>%ap_name%</b>\')',
'tt_nc_remove'=>'Supprimer Non-Conformité',

/* './packages/improvement/nav_non_conformity.xml' */

'gc_action_plans'=>'Plan d\'Action',
'gc_capability'=>'Potentiel',
'gc_sender'=>'Diffuseur',
'gc_state'=>'État',
'mi_action_plan'=>'Associer Plan d\'Action',
'mi_send_to_ap_pendant'=>'Envoyer à l\'Acceptation AP',
'mi_send_to_conclusion'=>'Envoyer à Finition',
'mi_send_to_responsible'=>'Envoyer à Responsable',
'tt_nc_bl'=>'<b>Non-Conformité</b>',

/* './packages/improvement/nav_occurrence.php' */

'st_occurrence_remove'=>'Etes-vous sûr que vous souhaitez supprimer l\' occurrence<b>%occurrence_text%</b>?',
'tt_occurrence_remove'=>'Supprimer Occurrence',

/* './packages/improvement/nav_occurrence.xml' */

'gc_incident'=>'Incident',
'mi_incident'=>'Associer Incident',
'tt_occurrences'=>'<b>Occurrences</b>',

/* './packages/improvement/nav_report.php' */

'mx_report_action_plan_by_process'=>'Plan d\'Action par Processus',
'mx_report_action_plan_by_user'=>'Plan d\'Action par Utilisateur',
'mx_report_ap_without_doc'=>'Plan d\'Action sans Document',
'mx_report_ci_pendant_tasks'=>'Tâches En Attente d\'Amélioration Continuelle',
'mx_report_control_nonconformity_by_process'=>'Non-Conformité de contrôle par Processus',
'mx_report_elements_affected_by_incident'=>'Objets touchés par des Incidents',
'mx_report_financial_impact_by_incident'=>'Impact Financier par Incident',
'mx_report_incident_accompaniment'=>'Suivi des Incidents',
'mx_report_incident_by_responsible'=>'Collection d\'Évidence',
'mx_report_incident_control_revision'=>'Révisions de Contrôle causées par des Incidents',
'mx_report_incident_without_risk'=>'Incident sans Risques',
'mx_report_incidents_without_occurrence'=>'Incidents sans Occurrences',
'mx_report_nc_without_ap'=>'Non-Conformité Sans Plan d\'Action',
'mx_report_nonconformity_accompaniment'=>'Non-Conformité de suivi',
'mx_report_nonconformity_by_process'=>'Non-Conformité par processus',
'mx_report_occurrence_accompaniment'=>'Suivi d\'Occurrence',
'mx_report_occurrence_by_incident'=>'Occurrences par Incidents',
'mx_report_risk_auto_probability_value'=>'Calcul Automatique de Probabilité de Risque',
'mx_report_user_by_incident'=>'Processus Disciplinaire',

/* './packages/improvement/nav_report.xml' */

'cb_risk'=>'Risque',
'lb_conclusion_cl'=>'En Terminant:',
'lb_loss_type_cl'=>'Type de Perte:',
'lb_show_cl'=>'Afficher:',
'si_capability_all'=>'Tous',
'si_capability_no'=>'Non',
'si_capability_yes'=>'Oui',
'si_ci_manager'=>'Rapports d\'Amélioration Continue',
'si_incident_order_by_category'=>'Catégorie',
'si_incident_order_by_responsible'=>'Responsable',
'si_incident_report'=>'Rapports d\'Incident',
'si_nonconformity_report'=>'Rapports de Non-Conformité',
'st_action_plan_filter_bl'=>'<b>Filtre de Plan d\'Action</b>',
'st_elements_filter_bl'=>'<b>Filtre d\'Objets</b>',
'st_incident_filter_bl'=>'<b>Filtre d\'Incident</b>',
'st_nonconformity_filter_bl'=>'<b>Filtre de Non-Conformité</b>',
'st_occurrence_filter_bl'=>'<b>Filtre d\'Occurrence</b>',
'st_status'=>'État:',
'wn_at_least_one_element_must_be_selected'=>'Au moins un élément doit être sélectionné.',

/* './packages/improvement/popup_action_plan_associate.xml' */

'tt_action_plan_association'=>'Plan d\'Action de l\'Association',
'tt_current_action_plans_bl'=>'<b>Plans d\'Action Courants</b>',

/* './packages/improvement/popup_action_plan_edit.xml' */

'lb_ap_actionplan_bl_cl'=>'<b>Action:</b>',
'lb_ap_actiontype_bl_cl'=>'<b>Type d\'Action:</b>',
'lb_ap_dateconclusion_cl'=>'Date de Finition:',
'lb_ap_datedeadline_bl_cl'=>'<b>Date Limite de Délai:</b>',
'lb_ap_name_bl_cl'=>'<b>Nom:</b>',
'lb_ap_responsible_id_bl_cl'=>'<b>Responsable:</b>',
'rb_corrective'=>'Rectificatif',
'rb_preventive'=>'Préventif',
'tt_ap_edit'=>'Modification de Plan d\'Action',
'vb_finish'=>'Finition',
'wn_deadline'=>'La date limite de délai doit être postérieure ou égale à la date actuelle.',

/* './packages/improvement/popup_action_plan_finish_confirm.xml' */

'lb_ap_days_before_bl'=>'<b>Notifier avant</b>',
'lb_date_efficiency_revision_bl_cl'=>'<b>Date de Révision d\'Efficacité:</b>',
'st_action_plan_finish_confirm_message'=>'Voulez-vous terminer le plan d\'action \'%name% \'?',
'tt_action_plan_finish'=>'Terminer Plan d\'Action',
'wn_revision_date_after_conclusion_date'=>'La date de révision d\'efficacité doit être postérieure ou la même que la date de finition!',

/* './packages/improvement/popup_action_plan_revision_task.xml' */

'lb_action_cl'=>'Action:',
'st_action_plan_efficient_bl'=>'<b>Est-ce que le plan d\'action a été efficace?</b>',
'st_data_revision_bl_cl'=>'<b>Date de Révision:</b>',
'tt_action_plan_efficiency_revision'=>'Révision d\'Efficacité de Plan d\'Action',

/* './packages/improvement/popup_disciplinary_process_action.php' */

'st_denied_permission_to_edit_action_taken'=>'Vous n\'avez pas la permission d\'enregistrer une action dans un processus disciplinaire.',

/* './packages/improvement/popup_disciplinary_process_action.xml' */

'lb_action_bl_cl'=>'<b>Action:</b>',
'st_incident_cl'=>'<b>Incident:</b>',
'st_user_cl'=>'<b>Utilisateur:</b>',

/* './packages/improvement/popup_disciplinary_process_edit.php' */

'em_disciplinary_process_creation'=>'Création de Processus Disciplinaire',
'em_disciplinary_process_creation_footer'=>'Pour plus d\'informations',
'st_incident_user_remove'=>'Etes-vous sûr de vouloir supprimer l\'utilisateur user_name<b>%user_name%</b> du processus disciplinaire de l\'incident<b>%incident_name%</b>?',
'tt_incident_user_remove'=>'Supprimer Utilisateur',

/* './packages/improvement/popup_disciplinary_process_edit.xml' */

'gc_observation_involvement'=>'Observation / Engagement',
'gc_user'=>'Utilisateur',
'lb_incident_cl'=>'Incident:',
'lb_observation_involvement_cl'=>'Observation / Engagement:',
'lb_user_bl_cl'=>'<b>Utilisateur:</b>',
'tt_disciplinary_process'=>'<b>Processus Disciplinaire</b>',
'wn_email_dp_sent'=>'Un e-mail a déjà été envoyé au Gérant de Processus Disciplinaire.',

/* './packages/improvement/popup_evidence_requirement.xml' */

'lb_comments_cl'=>'Commentaires:',
'tt_evidence_requirement'=>'Collection d\'Évidence:',
'wn_evidence_requirement_editing'=>'Obs. Lors de l\'édition du commentaire ci-dessus',

/* './packages/improvement/popup_financial_impact_edit.xml' */

'st_total'=>'Total',
'tt_financial_impact_parametrization'=>'Estimation d\'Impact Financier',

/* './packages/improvement/popup_incident_accounts_plan.xml' */

'tt_incident_accounts_plan'=>'Plan Comptable',

/* './packages/improvement/popup_incident_affected_product_service.xml' */

'tt_affected_product_service'=>'Produit / Service Touché',

/* './packages/improvement/popup_incident_approval_task.xml' */

'tt_assets_per_incident'=>'<b>Actifs Liés à l\'Incident</b>',

/* './packages/improvement/popup_incident_disposal.xml' */

'tt_immediate_disposal'=>'Élimination Immédiate',

/* './packages/improvement/popup_incident_edit.xml' */

'lb_accounts_plan_cl'=>'Plan Comptable:',
'lb_affected_product_service_cl'=>'Produit / Service Visé:',
'lb_date_bl_cl'=>'<b>Date:</b>',
'lb_date_limit_bl_cl'=>'<b>Estimation de Finition:</b>',
'lb_evidences_cl'=>'Évidences:',
'lb_hour_bl_cl'=>'<b>Heure:</b>',
'lb_immediate_disposal_cl'=>'Élimination immédiate:',
'lb_loss_type_bl_cl'=>'<b>Type de Perte</b>',
'lb_occurrences_bl_cl'=>'<b>Occurrences:</b>',
'lb_occurrences_cl'=>'Occurrences:',
'lb_solution_cl'=>'Solution:',
'si_direct'=>'Direct',
'si_indirect'=>'Indirect',
'st_collect_evidence'=>'Collectionner Evidences?',
'st_incident_date_finish'=>'Estimation de Finition:',
'st_incident_date_finish_bl'=>'<b>Date de Finition Estimée:</b>',
'st_no'=>'Non',
'st_yes'=>'Oui',
'tt_incident_edit'=>'Modification d\'Incident',
'vb_financial_impact'=>'Impact Financier',
'vb_observations'=>'Observations',
'wn_future_date_not_allowed'=>'Une date / heure future n\'est pas autorisée.',
'wn_prevision_finish'=>'La date estimée de finition doit être égale ou postérieure à la journée en cours.',

/* './packages/improvement/popup_incident_evidence.xml' */

'tt_evidence'=>'Évidence',

/* './packages/improvement/popup_incident_process_association_edit.php' */

'st_save_suggested_incident_processes'=>'Voulez-vous relier les processus suggérés à l\'incident<b>%context_name%</b>?',
'tt_save_suggested_incident_processes'=>'Relier Processus Suggérés',

/* './packages/improvement/popup_incident_process_association_edit.xml' */

'cb_search_only_suggested_process'=>'Seulement Rechercher les Processus suggérés',
'tt_incident_process_association'=>'Association de Processus à Incident',

/* './packages/improvement/popup_incident_risk_asset_association_edit.xml' */

'lb_assets_cl'=>'Actifs:',
'lb_risks_cl'=>'Risques:',
'st_risk_asset_association_message'=>'Lors de la sélection d\'un actif',
'tt_risk_asset_association'=>'Association de Risque à l\'Actif',

/* './packages/improvement/popup_incident_risk_association_edit.php' */

'em_incident_risk_parametrization'=>'Estimation de Risque associée à l\'Incident',
'em_incident_risk_parametrization_footer'=>'Pour plus d\'information veuillez contacter l\'agent de sécurité.',
'mx_non_parameterized_risk'=>'Risque Non-Estimé',
'to_dependencies_bl_cl'=>'<b>Dépendances:</b><br/>',
'to_dependents_bl_cl'=>'<b>Dépendant:</b><br/>',

/* './packages/improvement/popup_incident_risk_association_edit.xml' */

'tt_associated_controls_and_assets'=>'<b>Contrôles et Avoirs Apparentés</b>',
'tt_incident_risk_association'=>'Association Incident-Risque',
'tt_risks_refferring_controls_and_assets'=>'<b>Risques en matière de Contrôles et d\'Actifs</b>',
'tt_risks_related_selected'=>'<b>Connexes / Risques Sélectionnés</b>',
'vb_insert_risk'=>'Insérer des Risques',
'vb_risk_incident_parametrization'=>'Estimation d\'Incident-Risque',
'vb_search_assets'=>'Recherche d\'Actifs',
'vb_search_controls'=>'Recherche de Contrôles',
'wn_parameterize_incident_risk'=>'Vous devez estimer l\'association Incident-Risque avant l\'enregistrement de votre association.',

/* './packages/improvement/popup_incident_risk_association_options.xml' */

'lb_associate_risk_to_assets'=>'Voulez-vous relier les risques courants aux actifs sélectionnés? Vous pouvez choisir quel risque à relier à quel actifs.',
'lb_send_alert_to_other_users'=>'Voulez-vous envoyer des alertes pour d\'autres utilisateurs',
'tt_incident_risk_association_options'=>'Options pour l\'association de risques et d\'incidents',
'vb_continue'=>'Continuer',

/* './packages/improvement/popup_incident_risk_parametrization.xml' */

'tt_incident_risk_association_parametrization'=>'Estimation de l\'Association des incidents et des risques',

/* './packages/improvement/popup_incident_search.xml' */

'tt_incident_search'=>'Recherche d\'Incident',
'vb_relation'=>'Lier',
'wn_no_incident_selected'=>'Aucun incident a été sélectionné.',

/* './packages/improvement/popup_incident_solution.xml' */

'tt_solution'=>'Solution',

/* './packages/improvement/popup_incident_state_forward.php' */

'st_incident_finish'=>'Voulez-vous terminer l\'incident?',
'st_incident_send_to_app_disposal'=>'Souhaitez vous envoyer l\'incident à l\'Approbation d\'Élimination Immédiate?',
'st_incident_send_to_app_solution'=>'Voulez-vous envoyer l\'incident à l\'Approbation de Solution?',
'st_incident_send_to_responsible'=>'Voulez-vous envoyer l\'incident au responsable?',
'tt_incident_finish'=>'Terminer l\'Incident',
'tt_incident_send_to_app_disposal'=>'Envoyé à l\'Approbation d\'Élimination Immédiate',
'tt_incident_send_to_app_solution'=>'Envoyer à l\'Approbation de Solution',
'tt_incident_send_to_responsible'=>'Envoyer au Responsable',

/* './packages/improvement/popup_incident_task_view.php' */

'lb_incident_immediate_disposal_cl'=>'Élimination Immédiate:',
'lb_incident_solution_cl'=>'Solution:',

/* './packages/improvement/popup_incident_task_view.xml' */

'lb_date_limit_cl'=>'Date Limite:',

/* './packages/improvement/popup_new_risk_parameter_approval_task.xml' */


/* './packages/improvement/popup_non_conformity_approval_task.xml' */

'lb_processes_cl'=>'Processus:',

/* './packages/improvement/popup_non_conformity_associate.xml' */

'st_name'=>'Nom:',
'tt_current_non_conformitys_bl'=>'<b>Non-Conformités actuelles</b>',
'tt_non_conformity_association'=>'Association de Non-Conformités',

/* './packages/improvement/popup_non_conformity_create.xml' */

'lb_date_sent_cl'=>'Date d\'Envoi:',
'lb_sender_cl'=>'Expéditeur:',
'tt_non_conformity_record'=>'Non-Conformité de Registre',

/* './packages/improvement/popup_non_conformity_data_approval_task.php' */

'st_denied_permission_to_execute_task'=>'Vous n\'avez pas l\'autorisation d\'exécuter la tâche.',

/* './packages/improvement/popup_non_conformity_data_approval_task.xml' */

'lb_ap_responsible_cl'=>'Resp. API:',
'lb_cause_cl'=>'Cause:',
'lb_inquirer_cl'=>'Enquêteur:',
'lb_justificative_cl'=>'Raison:',
'lb_non_conformity_cl'=>'Non-Conformité:',
'lb_task_cl'=>'Tâche:',
'tt_task_visualization'=>'Visualisation de Tâche',

/* './packages/improvement/popup_non_conformity_edit.xml' */

'lb_action_plans_cl'=>'Plans d\'Action:',
'lb_nc_capability_bl_cl'=>'Potentiel:',
'lb_nc_cause_cl'=>'Cause:',
'lb_nc_classification_cl'=>'Classification:',
'lb_nc_datesent_cl'=>'Date d\'Envoi:',
'lb_nc_description_cl'=>'Description:',
'lb_nc_name_bl_cl'=>'<b>Nom:</b>',
'lb_nc_responsible_id_cl'=>'Responsable:',
'lb_nc_sender_id_cl'=>'Expéditeur:',
'lb_potential_cl'=>'Potentiel:',
'lb_processes_bl_cl'=>'<b>Processus:</b>',
'si_external_audit'=>'Audit Externe',
'si_internal_audit'=>'Audit Interne',
'si_no'=>'Non',
'si_security_control'=>'Contrôle de Sécurité',
'si_yes'=>'Oui',
'tt_nc_edit'=>'Modification de Non-Conformité',

/* './packages/improvement/popup_non_conformity_revision_task.xml' */

'lb_action_plan_cl'=>'Plan d\'Action:',
'lb_ap_efficient_bl'=>'<b>Est-ce que le Plan d\'Action a été efficace?</b>',
'rb_no'=>'Non',
'rb_yes'=>'Oui',
'tt_non_conformity_efficiency_revision'=>'Révision d\'Efficacité de Non-Conformité',

/* './packages/improvement/popup_non_conformity_state_forward.php' */

'st_non_conformity_send_to_ap_responsible'=>'Voulez-vous envoyer la non-conformité afin de faire accepter ses Plans d\'Action?',
'st_non_conformity_send_to_approve'=>'Voulez-vous envoyer la non-conformité à Finition des Plans d\'Action?',
'st_non_conformity_send_to_responsible'=>'Voulez-vous envoyer la Non-Conformité au responsable?',
'tt_non_conformity_send_to_ap_responsible'=>'Envoyer à l\'Acceptation des Plans d\'Action',
'tt_non_conformity_send_to_approve'=>'Envoyer à Finition des Plans d\'Action',
'tt_non_conformity_send_to_responsible'=>'Envoyer à Responsable',

/* './packages/improvement/popup_non_conformity_state_forward.xml' */

'vb_no'=>'Non',
'vb_yes'=>'Oui',

/* './packages/improvement/popup_occurrence_edit.xml' */

'lb_description_bl_cl'=>'<b>Description:</b>',
'st_date_bl'=>'<b>Date:</b>',
'st_date_warning'=>'Une date / heure futur n\'est pas autorisée.',
'st_hour_bl'=>'<b>Heure:</b>',
'tt_register_occurrence'=>'Inscrivez l\'Occurrence',

/* './packages/improvement/popup_occurrences_search.xml' */

'tt_current_occurrences'=>'<b>Occurrences Actuelles</b>',
'tt_occurrences_search'=>'Recherche d\'Occurrences',

/* './packages/improvement/popup_probability_calculation_config.xml' */

'lb_incidents_in_period_cl'=>'Incidents entre %initial_date% and %final_date%:',
'lb_last_check_cl'=>'Dernière vérification:',
'lb_periodicity_bl_cl'=>'<b>Périodicité:</b>',
'lb_starting_at'=>'A partir de',
'si_days'=>'Jour(s)',
'si_months'=>'Mois',
'si_weeks'=>'Semaine(s)',
'st_total_incidents_found'=>'Total des incidents trouvés:',
'tt_incidents_related_to_risk_in_period'=>'<b>Incidents liés à des risques au cours de la période</b>',
'tt_probability_calculation_config'=>'Configuration du Calcul Automatique de Probabilité',
'tt_probability_level_vs_incident_count'=>'<b>Niveau de Probabilité<br/><br/><b>X</b><br/><br/><b>Nombre d\'Incidents</b>',
'vb_update'=>'Mise à jour',
'wn_probability_config_not_filled'=>'Intervalles non remplis ou avec des valeurs inconsistantes',

/* './packages/improvement/popup_process_search.xml' */

'tt_current_processes'=>'<b>Processus Actuels</b>',
'tt_process_search'=>'Recherche de Processus',

/* './packages/improvement/popup_request_compare_impact_risks.xml' */


/* './packages/improvement/popup_risk_create.xml' */

'lb_impact_money_cl'=>'Impact Financier:',
'si_select_one_below'=>'Sélectionnez l\'un des objets ci-dessous',
'st_risk_impact'=>'Impact:',
'tt_insert_risk'=>'Insérer Risque',

/* './packages/improvement/report/clock.php' */

'st_clock_wait'=>'<b>Attention:</b>Ce rapport peut prendre quelques minutes à être généré.<br/>S\'il vous plaît attendre.',

/* './packages/improvement/report/popup_report_action_plan_by_process.php' */

'tt_action_plan_by_process'=>'Plan d\'Action par Processus',

/* './packages/improvement/report/popup_report_action_plan_by_user.php' */

'tt_action_plan_by_user'=>'Plan d\'Action par Utilisateur',

/* './packages/improvement/report/popup_report_ap_without_doc.php' */

'rs_ap_without_docs'=>'Plans d\'Action sans Documents',

/* './packages/improvement/report/popup_report_ap_without_doc.xml' */

'rs_ap'=>'Plan d\'Action',

/* './packages/improvement/report/popup_report_control_nonconformity_by_process.php' */

'mx_control_nonconformity_by_process'=>'Non-Conformité de Contrôle par Processus',

/* './packages/improvement/report/popup_report_disciplinary_process.php' */

'mx_disciplinary_process'=>'Processus Disciplinaires',

/* './packages/improvement/report/popup_report_disciplinary_process.xml' */

'rs_dp_name'=>'Nom',

/* './packages/improvement/report/popup_report_elements_affected_by_incident.php' */

'mx_elements_affected_by_incident'=>'Objets touchés par des Incidents',

/* './packages/improvement/report/popup_report_elements_affected_by_incident.xml' */

'rs_incident_bl_cl'=>'<b>Incident:</b>',

/* './packages/improvement/report/popup_report_financial_impact_by_incident.php' */

'mx_financial_impact_by_incident'=>'Impact Financier par Incident',

/* './packages/improvement/report/popup_report_incident_accompaniment.php' */

'mx_incident_accompaniment'=>'Suivi d\'Incident',

/* './packages/improvement/report/popup_report_incident_by_responsible.php' */

'mx_evidence_collection'=>'Collection d\'Évidence',

/* './packages/improvement/report/popup_report_incident_control_revision.php' */

'mx_incident_control_revision'=>'Révision de Contrôle causée par Incidents',

/* './packages/improvement/report/popup_report_incident_control_revision.xml' */

'rs_date_todo'=>'Date Estimée',
'rs_er'=>'RE',

/* './packages/improvement/report/popup_report_incident_without_occurrence.php' */

'rs_incidents_without_occurrence'=>'Incidents sans Occurrences',

/* './packages/improvement/report/popup_report_incident_without_risk.php' */

'rs_incidentes_without_risks'=>'Incidents sans Risques',

/* './packages/improvement/report/popup_report_incident_without_risk.xml' */

'rs_incident'=>'Incident',

/* './packages/improvement/report/popup_report_incidents.php' */

'mx_incidents'=>'Incidents',

/* './packages/improvement/report/popup_report_incidents.xml' */

'rs_incident_name'=>'Nom',

/* './packages/improvement/report/popup_report_nc_without_ap.php' */

'rs_nc_without_ap'=>'Non-Conformités sans Plans d\'Action',
'st_nc_manager_with_parentheses'=>'(Gérant des Non-Conformités)',

/* './packages/improvement/report/popup_report_non_conformities.php' */

'mx_non_conformities'=>'Non-Conformités',

/* './packages/improvement/report/popup_report_non_conformities.xml' */

'rs_nc_name'=>'Nom',

/* './packages/improvement/report/popup_report_nonconformity_accompaniment.php' */

'mx_nonconformity_accompaniment'=>'Suivi de Non-Conformité',
'rs_not_defined'=>'Non définie',

/* './packages/improvement/report/popup_report_nonconformity_accompaniment.xml' */

'rs_action_plan'=>'Plan d\'Action',
'rs_conclusion'=>'Terminer',
'rs_non_conformity'=>'Non-Conformité:',

/* './packages/improvement/report/popup_report_nonconformity_by_process.php' */

'mx_nonconformity_by_process'=>'Non-Conformité par Processus',

/* './packages/improvement/report/popup_report_nonconformity_by_process.xml' */

'rs_nonconformity'=>'Non-Conformité',

/* './packages/improvement/report/popup_report_occurrence_accompaniment.php' */

'mx_occurrence_accompaniment'=>'Suivi d\'Occurrence',

/* './packages/improvement/report/popup_report_occurrence_accompaniment.xml' */

'rs_creator_cl'=>'Créateur:',

/* './packages/improvement/report/popup_report_occurrence_by_incident.php' */

'mx_occurrence_by_incident'=>'Occurrences par Incident',

/* './packages/improvement/report/popup_report_occurrence_by_incident.xml' */

'report_filter_userid_cl'=>'Responsable:',
'rs_occurrence'=>'Occurrence',
'rs_status_cl'=>'État:',

/* './packages/improvement/report/popup_report_risk_auto_probability_value.php' */

'mx_risk_auto_probability_value'=>'Calcul Automatique de Probabilité de Risque',

/* './packages/improvement/report/popup_report_risk_auto_probability_value.xml' */

'rs_incident_amount'=>'Nbr. d\'Incidents',
'rs_last_verification'=>'Dernière Vérification',
'rs_periodicity'=>'Périodicité',

/* './packages/improvement/report/popup_report_user_by_incident.php' */

'mx_user_by_incident'=>'Processus Disciplinaire',

/* './packages/improvement/report/popup_report_user_by_incident.xml' */

'rs_description'=>'Description',
'rs_incident_cl'=>'Incident:',
'rs_user'=>'Utilisateur',

/* './packages/improvement/report/report_template.xml' */

'rs_fim'=>'FIN DU RAPPORT',

/* './packages/improvement/tab_continual_improvement.xml' */

'ti_action_plan'=>'Plan d\'Action',
'ti_disciplinary_process'=>'Processus Disciplinaire',
'ti_incident'=>'Incident',
'ti_non_conformity'=>'Non-Conformité',
'ti_occurrences'=>'Occurrences',
'ti_report'=>'Rapports',

/* './packages/libraries/nav_asset_category_threats.xml' */


/* './packages/libraries/nav_best_practices.php' */

'st_remove_best_practice_confirm'=>'Etes-vous sûr de vouloir supprimer la Meilleure Pratique<b>%best_practice_name%</b>?',
'st_remove_section_confirm'=>'Etes-vous sûr de vouloir supprimer la section<b>%section_name%</b>?',
'tt_remove_best_practice'=>'Supprimer Meilleure Pratique',
'tt_remove_section'=>'Supprimer Section',

/* './packages/libraries/nav_best_practices.xml' */

'mi_associate_events'=>'Associer Evénements',
'mi_open_section'=>'Ouvrir Section',
'mi_templates'=>'Modèles de Document',
'tt_best_practices_bl'=>'<b>Meilleures Pratiques</b>',
'vb_insert_best_practice'=>'Insérer Meilleure Pratique',
'vb_insert_section'=>'Insérer Section',

/* './packages/libraries/nav_department.xml' */


/* './packages/libraries/nav_document_template.php' */

'st_remove_document_template_confirm'=>'Etes-vous sûr de vouloir supprimer le Modèle de Document<b>%context_name%</b>?',
'tt_remove_document_template'=>'Supprimer Modèle de Document',

/* './packages/libraries/nav_document_template.xml' */

'si_best_practice'=>'Meilleures Pratiques',
'si_denied'=>'Refusé',
'si_doc_template_control'=>'Contrôle',
'si_pending'=>'En Attente',
'tt_documents_templates_bl'=>'<b>Modèles de Documents</b>',

/* './packages/libraries/nav_events.php' */

'st_remove_category_cascade_message'=>'Vous allez également supprimer tous les Actifs liés à la Catégorie',
'st_remove_category_confirm'=>'Etes-vous sûr de vouloir supprimer la catégorie<b>%category_name%</b>?',
'st_remove_event_confirm'=>'Etes-vous sûr de vouloir supprimer l\'événement<b>%event_name%</b>?',
'tt_remove_category'=>'Supprimer Catégorie',
'tt_remove_event'=>'Supprimer Événement',

/* './packages/libraries/nav_events.xml' */

'mi_associate_best_practices'=>'Associer les Meilleures Pratiques',
'mi_open_event_category'=>'Ouvrir Catégorie',
'tt_events_library_bl'=>'<b>Bibliothèque</b>',
'vb_associate_event'=>'Associer Événement',
'vb_insert_event'=>'Insérer Événement',

/* './packages/libraries/nav_impact_library.xml' */


/* './packages/libraries/nav_import_export.php' */

'wn_error_invalid_file'=>'Erreur: fichier non valide.',
'wn_extension_not_allowed'=>'Extension pas autorisée.',
'wn_maximum_size_exceeded'=>'Taille maximale dépassée.',

/* './packages/libraries/nav_import_export.xml' */

'lb_client_cl'=>'Client:',
'lb_export_library_cl'=>'Bibliothèque d\'Exportations:',
'lb_import_library_cl'=>'Bibliothèque d\'Importation:',
'lb_license_cl'=>'Licence:',
'si_best_practices'=>'Meilleures Pratiques',
'si_events_library'=>'Bibliothèque d\'Événements',
'si_standards'=>'Normes',
'si_templates'=>'Modèles de documents',
'st_export_list_bl'=>'<b>Liste des Exportations</b>',
'st_exporting_options_bl'=>'<b>Options d\'Exportation</b>',
'st_import_list_bl'=>'<b>Liste des Importations</b>',
'st_or_bl'=>'<b>Ou</b>',
'to_export'=>'<b>Export:</b><br/><br/>Exportation des éléments sélectionnés.',
'to_export_list'=>'<b>Liste:</b><br/><br/>Produit la liste d\'objets de bibliothèque',
'to_import'=>'<b>Importer</b><br/><br/>Objets d\'importation sélectionnés.',
'to_import_list'=>'<b>Liste:</b><br/><br/>Produit la liste d\'objets de bibliothèque',
'tr_best_practices'=>'Meilleures Pratiques',
'tr_events_library'=>'Bibliothèque d\'Événements',
'tr_no_items_to_import'=>'Il n\'y a pas d\'objets à importer',
'tr_standards'=>'Normes',
'tr_template'=>'Modèles de Documents',
'tr_templates'=>'Modèles de Documents',
'vb_export'=>'Exporter',
'vb_import'=>'Importation',
'vb_list'=>'Liste',
'vb_list_import'=>'Liste',
'wn_import_required_file'=>'Vous devez choisir un fichier avant de cliquer sur la liste.',

/* './packages/libraries/nav_incident_category.php' */

'st_category_remove'=>'Etes-vous sûr de vouloir supprimer la catégorie<b>%category_name%</b>?',
'st_category_remove_error'=>'Il n\'est pas possible de supprimer la catégorie<b>%category_name%</b> car elle est liée à un ou plusieurs incidents!',
'st_solution_remove'=>'Etes-vous sûr de vouloir supprimer la solution?',
'tt_category_remove'=>'Enlever Catégorie',
'tt_category_remove_error'=>'Erreur lors de la suppression de la Catégorie',
'tt_solution_remove'=>'Supprimer Solution',

/* './packages/libraries/nav_incident_category.xml' */

'gc_keywords'=>'Mots Clés',
'gc_problem'=>'Problème',
'gc_solution'=>'Solution',
'gc_solutions_bl'=>'<b>Solutions</b>',
'mi_delete'=>'Supprimer',
'mi_open_category'=>'Ouvrir Catégorie',
'tt_category_library'=>'<b>Bibliothèque de Catégories</b>',
'vb_solution_insert'=>'Insérer Solution',

/* './packages/libraries/nav_place_category_threats.xml' */


/* './packages/libraries/nav_plan_ranges.xml' */


/* './packages/libraries/nav_plan_types.xml' */


/* './packages/libraries/nav_process_category_threats.xml' */

'vb_insert_category'=>'Insérer Catégorie',

/* './packages/libraries/nav_standards.php' */

'st_remove_standard_confirm'=>'Etes-vous sûr de vouloir supprimer la norme<b>%standard_name%</b>?',
'tt_remove_standard'=>'Suppression de Norme',

/* './packages/libraries/nav_standards.xml' */

'gc_creator'=>'Créateur',
'tt_standards_bl'=>'<b>Normes</b>',

/* './packages/libraries/popup_best_practice_add.xml' */

'si_all_sections'=>'Toutes les Sections',
'tt_current_best_practices_bl'=>'<b>Meilleures Pratiques Actuelles</b>',

/* './packages/libraries/popup_best_practice_edit.php' */

'tt_best_practice_adding'=>'Ajout de Meilleure Pratique',
'tt_best_practice_editing'=>'Modification de Meilleure Pratique',

/* './packages/libraries/popup_best_practice_edit.xml' */

'lb_control_objective_cl'=>'Objectif de Contrôle:',
'lb_control_type_cl'=>'Type de Contrôle:',
'lb_implementation_guidelines_cl'=>'Lignes Directrices de Mise en oeuvre:',
'lb_metric_cl'=>'Métrique:',
'lb_name_good_practices_bl_cl'=>'<b>Nom/ Bonnes Pratiques:</b>',
'lb_standards_bl_cl'=>'<b>Normes:</b>',
'si_administrative'=>'Administratif',
'si_awareness'=>'Sensibilisation',
'si_coercion'=>'Contrainte',
'si_correction'=>'Correction',
'si_detection'=>'Détection',
'si_limitation'=>'Limitation',
'si_monitoring'=>'Suivi',
'si_physical'=>'Physique',
'si_prevention'=>'Prévention',
'si_recovery'=>'Rétablissement',
'si_technical'=>'Technique',
'tt_best_practice'=>'Meilleure Pratique',

/* './packages/libraries/popup_category_edit.php' */

'tt_category_adding'=>'Ajout de Catégorie',
'tt_category_editing'=>'Modification de Catégorie',

/* './packages/libraries/popup_document_template_edit.php' */

'tt_document_template_adding'=>'Ajout de Modèle de Document',
'tt_document_template_editing'=>'Modification de Modèle de Document',

/* './packages/libraries/popup_document_template_edit.xml' */

'lb_best_practice_cl'=>'Meilleure Pratique:',
'lb_type_bl_cl'=>'<b>Type:</b>',
'si_action_plan'=>'Plan d\'Action',
'si_area'=>'Domaine',
'si_asset'=>'Actif',
'si_control'=>'Contrôle',
'si_management'=>'Gestion',
'si_policy'=>'Règlement',
'si_process'=>'Processus',
'si_scope'=>'Champ d\'Application',
'si_select_type'=>'Sélectionnez le Type',

/* './packages/libraries/popup_event_add.xml' */

'tt_current_events_bl'=>'<b>Événements Courants</b>',

/* './packages/libraries/popup_event_edit.php' */

'tt_event_adding'=>'Ajout d\'Événement',
'tt_event_editing'=>'Modification d\'Événement',

/* './packages/libraries/popup_event_edit.xml' */

'cb_suggested_event'=>'Proposition d\'Événement',
'lb_observation_cl'=>'Observation:',
'st_event_impact_cl'=>'Impact:',

/* './packages/libraries/popup_imple_guide.xml' */

'tt_implementation_guidelines'=>'Directives de Mise en Oeuvre',

/* './packages/libraries/popup_incident_category_edit.php' */

'tt_category_addition'=>'Ajout de Catégorie',

/* './packages/libraries/popup_incident_category_edit.xml' */

'tt_category_edit'=>'Modification de Catégorie',

/* './packages/libraries/popup_incident_solution_edit.xml' */

'lb_incident_category_cl'=>'Catégorie:',
'lb_problem_bl_cl'=>'<b>Problème:</b>',
'lb_solution_bl_cl'=>'<b>Solution:</b>',
'tt_solution_edit'=>'Modification de Catégorie',

/* './packages/libraries/popup_metric.xml' */

'tt_metric'=>'Métrique',

/* './packages/libraries/popup_process_category_threat_edit.xml' */


/* './packages/libraries/popup_section_edit.php' */

'tt_section_adding'=>'Ajout de Section',
'tt_section_editing'=>'Modification de Section',

/* './packages/libraries/popup_standard_add.xml' */

'tt_current_standards_bl'=>'<b>Normes Courantes</b>',
'tt_standards_search'=>'Recherche de Normes',

/* './packages/libraries/popup_standard_edit.php' */

'tt_standard_adding'=>'Ajout de Norme',
'tt_standard_editing'=>'Modification de Norme',

/* './packages/libraries/popup_standard_edit.xml' */

'lb_application_cl'=>'Application:',
'lb_objective_cl'=>'Objectif:',

/* './packages/libraries/tab_libraries.xml' */

'ti_best_practices'=>'Meilleures Pratiques',
'ti_category_library'=>'Catégories d\'Incident',
'ti_events_library'=>'Bibliothèque d\'Événements',
'ti_export_and_import'=>'Exportation et Importation',
'ti_standards'=>'Normes',
'ti_templates'=>'Modèles de Documents',

/* './packages/policy/nav_all.xml' */

'lb_status'=>'État',
'tt_all_documents_bl'=>'<b>Tous les Documents</b>',

/* './packages/policy/nav_approving.xml' */

'tt_approving_documents_bl'=>'<b>Documents en cours d\'Approbation</b>',

/* './packages/policy/nav_developing.xml' */

'tt_developing_document_bl'=>'<b>Documents En Cours de Développement</b>',
'vb_insert_document'=>'Insérer Document',

/* './packages/policy/nav_obsolete.xml' */

'gc_file_name'=>'Fichier / Lien',
'tt_obsolete_documents_bl'=>'<b>Versions Obsolètes de Documents</b>',

/* './packages/policy/nav_publish.xml' */

'tt_published_documents_bl'=>'<b>Documents Publiés</b>',

/* './packages/policy/nav_register.php' */

'st_remove_register_confirm'=>'Etes-vous sûr de vouloir supprimer le Registre<b>%register_name%</b>?',
'tt_remove_register'=>'Suppression des Registres',

/* './packages/policy/nav_register.xml' */

'gc_retention_time'=>'Temps de Rétention',
'gc_storage_place'=>'Lieu de Stockage',
'gc_storage_type'=>'Type de Stockage',
'mi_read'=>'Lire',
'mi_read_document'=>'Lire Document',
'mi_readers'=>'Lecteurs',
'vb_insert_register'=>'Insérer Registre',

/* './packages/policy/nav_report.php' */

'mx_documents_with_high_revision_frequency'=>'Documents avec une fréquence élevée de révision',
'mx_documents_without_registers'=>'Documents sans Registres',
'mx_pendant_task_pm'=>'Tâches en attente',
'mx_report_documents_pendent_reads'=>'Documents En Attente de Lecture',
'mx_report_element_classification'=>'Rapport de Classification',
'mx_users_with_pendant_read'=>'Utilisateurs avec des lectures en attente',
'si_access_to_documents'=>'Audit d\'Accès aux Documents',
'si_accessed_documents'=>'Audit d\'Accès d\'Utilisateurs',
'si_approvers'=>'Les Documents et leurs Approbateurs',
'si_documents_by_component'=>'Documents par ERMS Composant',
'si_documents_by_state'=>'Documents',
'si_documents_dates'=>'Dates de Création et de Révision de Documents',
'si_documents_per_area'=>'Documents par Domaine',
'si_items_with_without_documents'=>'Composants sans Documents',
'si_most_accessed_documents'=>'Documents les plus Consultés',
'si_most_revised_documents'=>'Documents les plus Révisés',
'si_not_accessed_documents'=>'Documents non consultés',
'si_pendant_approvals'=>'En Attente d\'Approbations',
'si_readers'=>'Les Documents et leurs Lecteurs',
'si_registers_per_document'=>'Registre par Document',
'si_users_per_process'=>'Les Utilisateurs associés à des Processus',

/* './packages/policy/nav_report.xml' */

'cb_expand_documents'=>'Développer Documents',
'cb_expand_readers'=>'Développer Lecteurs',
'cb_organize_by_area'=>'Organiser par domaine',
'cb_show_all_access'=>'Afficher tous les accès',
'cb_show_register_documents'=>'Montrer Documents du Registre',
'lb_creation_date_cl'=>'Date de Création:',
'lb_documents_bl_cl'=>'<b>Documents:</b>',
'lb_final_date_cl'=>'Date Finale:',
'lb_initial_date_cl'=>'Date Initiale:',
'lb_last_revision_date_cl'=>'Date de la Dernière Révision:',
'lb_show_only_first_cl'=>'Afficher uniquement les premiers:',
'lb_until'=>'jusqu\'à',
'lb_users_bl_cl'=>'<b>Utilisateurs:</b>',
'si_abnormalities_reports'=>'Rapports d\'Anomalies',
'si_access_control_reports'=>'Rapports de Contrôle d\'Accès',
'si_all_status'=>'Tous',
'si_approved'=>'Publié',
'si_developing'=>'En développement',
'si_documents_by_type'=>'Documents par Type',
'si_general_reports'=>'Rapports Généraux',
'si_management_reports'=>'Rapports de Gestion',
'si_obsolete'=>'Obsolète',
'si_pendant'=>'En approbation',
'si_register_reports'=>'Rapports de Registre',
'si_registers_by_type'=>'Registres par Type',
'si_revision'=>'En révision',
'st_asset'=>'Actif',
'st_components'=>'<b>Composants:</b>',
'st_control'=>'Contrôle',
'st_element_type_warning'=>'S\'il vous plaît',
'st_process'=>'Processus',
'st_status_bl'=>'<b>État:</b>',
'st_type_cl'=>'Type:',
'wn_component_filter_warning'=>'S\'il vous plaît sélectionner au moins un type de composant.',
'wn_no_filter_available'=>'Pas de filtre disponible.',
'wn_select_a_document'=>'S\'il vous plaît',

/* './packages/policy/nav_revision.xml' */

'mi_edit_author'=>'Modifier Auteur',
'tt_revision_documents_bl'=>'<b>Documents en cours de Révision</b>',

/* './packages/policy/nav_to_be_published.php' */

'st_document_confirm_remove'=>'Etes-vous sûr de vouloir supprimer le document<b>%document_name%</b>?',
'tt_remove_document'=>'Supprimer Document',

/* './packages/policy/nav_to_be_published.xml' */

'gc_elements'=>'Objets',
'gc_file_version'=>'Version',
'gc_sub_doc'=>'Sousdoc',
'lb_classification'=>'Classification',
'lb_type'=>'Type',
'mi_approve'=>'Approuver',
'mi_associate_components'=>'Associer Composants',
'mi_comments'=>'Commentaires',
'mi_copy_readers'=>'Copie de Lecteurs',
'mi_details'=>'Détails',
'mi_document_templates'=>'Modèles de documents',
'mi_edit_approvers'=>'Modifier Approbateurs',
'mi_edit_readers'=>'Modification de Lecteurs',
'mi_previous_versions'=>'Versions Antérieures',
'mi_publish'=>'Publier',
'mi_references'=>'Références',
'mi_registers'=>'Registres',
'mi_send_to_approval'=>'Envoyer pour Approbation',
'mi_send_to_revision'=>'Envoyer pour la Révision',
'mi_sub_documents'=>'Sous-Documents',
'mi_view_approvers'=>'Voir Approbateurs',
'mi_view_components'=>'Voir Composants',
'mi_view_readers'=>'Voir Lecteurs',
'tt_documents_to_be_published_bl'=>'<b>Documents à Publier</b>',

/* './packages/policy/popup_approvers_edit.xml' */

'tt_approvers_editing'=>'Modification d\'Approbateurs',
'tt_curret_approvers_bl'=>'<b>Approbateurs Actuels</b>',

/* './packages/policy/popup_approvers_view.xml' */

'tt_approvers_visualization'=>'Visualisation d\'Approbateurs',

/* './packages/policy/popup_associate_users.xml' */

'gc_login'=>'Connexion',
'lb_login_cl'=>'Connexion:',
'tt_current_users_bl'=>'<b>Utilisateurs Actuels</b>',
'tt_users_search'=>'Recherche d\'Utilisateurs',

/* './packages/policy/popup_create_document_confirm.xml' */

'rb_creation_document_question'=>'Voulez-vous créer un document dans le système et l\'utiliser comme un modèle pour le registre<b>%register_name%</b>?',
'rb_document_do_nothing'=>'Ne pas créer/associer le document au registre<b>%register_name%</b>?',
'rb_document_relation_question'=>'Voulez-vous associer un document existant dans le système et l\'utiliser comme un modèle pour registre<b>%register_name%</b>?',
'st_what_action_you_wish_to_do'=>'Quelles action désirez-vous exécuter?',
'tt_document_creation_or_association'=>'Ajout/Association de Document',

/* './packages/policy/popup_document_author_edit.xml' */

'tt_author_editing'=>'Modification d\'Auteur',

/* './packages/policy/popup_document_context_associate.php' */

'st_permission_to_associate_item_document_denied'=>'"Vous n\'avez pas la permission d\'associer un objet à un document de type ""Gestion"" ou ""Autres""',

/* './packages/policy/popup_document_context_associate.xml' */

'tt_current_items_bl'=>'<b>Objets Actuels</b>',
'tt_risk_management_items_association'=>'Association d\'Objets à la Gestion des Risques',

/* './packages/policy/popup_document_context_view.xml' */

'tt_associated_risk_management_items_visualization'=>'Visualisation des Objets Associés de la Gestion des Risques',

/* './packages/policy/popup_document_copy_readers.xml' */

'tt_documents_search'=>'Recherche de Documents',
'vb_copy_readers'=>'Copier Lecteurs',

/* './packages/policy/popup_document_create.xml' */

'lb_document_name_bl_cl'=>'<b>Nom du Document:</b>',
'tt_new_document'=>'Nouveau Document',

/* './packages/policy/popup_document_edit.php' */

'si_no_type'=>'Pas de Type',
'st_creation_date'=>'Date de Création:',
'st_document_approval_confirm'=>'Souhaitez-vous envoyer le document pour l\'approbation?',
'st_download_file'=>'Télécharger le fichier:',
'st_publish_document_confirm'=>'Souhaitez-vous publier le document?',
'st_publish_document_on_date_confirm'=>'Souhaitez-vous publier le document le %date%?',
'st_size'=>'Taille:',
'to_max_file_size'=>'Taille maximum de fichier: %max_file_size%',
'tt_document_adding'=>'Ajout de Document',
'tt_document_editing'=>'Modification de Document',
'tt_publish'=>'Publier',
'tt_send_for_approval'=>'Envoyer pour Approbation',

/* './packages/policy/popup_document_edit.xml' */

'lb_author_bl_cl'=>'<b>Auteur:</b>',
'lb_deadline_bl_cl'=>'<b>Délai:</b><br/><br/>Date limite du délai d\'approbation de document.',
'lb_deadline_cl'=>'Date limite:',
'lb_inform_in_advance_of'=>'Informez à l\'avance de',
'lb_publication_date_bl_cl'=>'<b>Date de Publication:</b>',
'lb_revision_cl'=>'Révision:',
'si_disable'=>'Désactiver',
'si_enable'=>'Activer',
'si_file'=>'Fichier',
'si_link'=>'Lien',
'st_days'=>'jours.',
'st_general_information'=>'Information Générale',
'st_management_information'=>'Gestion de l\'Information',
'st_test'=>'test',
'to_doc_instance_manual_version'=>'<b>Version Manuelle:</b><br/>La version manuelle ne prendra effet que lorsque le document a un lien ou un fichier associé.',
'to_document_inform_in_advance'=>'<b>Informer à l\'avance:</b><br/><br/>C\'est le nombre de jours avant la date limite ou de révision dans laquelle une alerte ou avertissement de tâche doit être générée.',
'to_document_type_modification'=>'Pour modifier le type de document',
'wn_max_file_size_exceeded'=>'La taille maximale de fichier est dépassée.',
'wn_publication_date_after_today'=>'La date de publication doit être la même ou après la date d\'aujourd\'hui.',
'wn_upload_error'=>'Erreur de l\'envoi de fichier.',

/* './packages/policy/popup_document_instances.php' */

'st_copy_previous_version_confirm'=>'Etes-vous sûr de vouloir copier la version précédente?',
'tt_copy_previous_version'=>'Copier version précédente',

/* './packages/policy/popup_document_instances.xml' */

'gc_end_date'=>'Date de Fin',
'gc_modification_comment'=>'Commentaire de Modification',
'gc_revision_justification'=>'Raison de Révision',
'gc_start_date'=>'Date de Début',
'gc_version'=>'Version',
'tt_previous_versions'=>'Versions Antérieures',
'wn_no_version_selected'=>'Il n\'y a pas de version sélectionnée.',

/* './packages/policy/popup_document_manage.xml' */

'st_approver_list_only_when_developing'=>'La liste des approbateurs ne peut être modifiée que lorsque le document est en cours de développement.',
'st_items_association_not_management_or_others'=>'"Il est seulement possible de changer les associations d\'un document qui n\'est pas de type ""Gestion"" ou ""Autres""."',
'st_items_association_only_when_developing'=>'Il est seulement possible de modifier les associations d\'un document qui est en cours de développement.',
'st_readers_list_only_when_developing'=>'La liste des lecteurs ne peut être modifiée que lorsque le document est en cours de développement.',
'st_revision_for_published_document_only'=>'Seulement un document publié peut être envoyé à la Révision',
'tt_associate_items_bl'=>'<b>Associer Objets</b>',
'tt_edit_approvers_bl'=>'<b>Modifier Approbateurs</b>',
'tt_edit_readers_bl'=>'<b>Modifier les Lecteurs</b>',
'tt_manage_document'=>'Gestion de Document',
'tt_send_for_revision_bl'=>'<b>Envoyer à la Révision</b>',
'tt_view_approvers_bl'=>'<b>Voir Approbateurs</b>',
'tt_view_items_bl'=>'<b>Voir Objets</b>',
'tt_view_readers_bl'=>'<b>Voir Lecteurs</b>',
'vb_copy'=>'Copier',
'vb_send'=>'Envoyer',

/* './packages/policy/popup_document_read.php' */

'st_remove_comment_confirm'=>'Etes-vous sûr de vouloir supprimer le commentaire en question?',
'tt_approve_document'=>'Approuver Document',
'tt_remove_comment'=>'Supprimer Commentaire',

/* './packages/policy/popup_document_read.xml' */

'ti_comments'=>'Commentaires',
'ti_general'=>'Général',
'ti_references'=>'Références',
'ti_sub_documents'=>'Sous-Documents',
'tt_document_reading'=>'Lecture de Document',
'vb_approve'=>'Approuver',
'vb_deny'=>'Rejeter',

/* './packages/policy/popup_document_read_comments.xml' */

'gc_author'=>'Auteur',
'gc_comment'=>'Commentaire',
'gc_date'=>'Date',
'tt_coments'=>'Commentaires',
'tt_comments_bl'=>'<b>Commentaires</b>',

/* './packages/policy/popup_document_read_details.xml' */

'lb_date_cl'=>'Date:',
'lb_keywords_cl'=>'Mots Clés:',
'lb_size_cl'=>'Taille:',
'lb_version_cl'=>'Version:',
'tt_details'=>'Détails',

/* './packages/policy/popup_document_read_subdocuments.xml' */

'tt_sub_documents'=>'Sous-Documents',

/* './packages/policy/popup_document_reference.php' */

'st_remove_reference_confirm'=>'Etes-vous sûr de vouloir supprimer la référence<b>%doc_reference_name%</b>?',
'tt_remove_reference'=>'Supprimer Référence',

/* './packages/policy/popup_document_reference.xml' */

'gc_creation_date'=>'Date de Création',
'gc_link'=>'Lien',
'lb_link_bl_cl'=>'<b>Lien:</b>',
'st_http'=>'http://',
'tt_references'=>'Références',
'tt_references_bl'=>'<b>Références</b>',

/* './packages/policy/popup_document_register.xml' */

'tt_associated_registers_bl'=>'<b>Registres Associés</b>',
'tt_registers'=>'Registres',
'vb_disassociate'=>'Dissocier',

/* './packages/policy/popup_document_register_associate.xml' */

'gc_classification'=>'Classification',
'gc_place'=>'Place',
'gc_time'=>'Temps',
'gc_type'=>'Type',
'tt_register_association'=>'Association de Registre',
'tt_registers_bl'=>'<b>Registres</b>',
'vb_associate_registers'=>'Associer Registres',
'wn_no_register_selected'=>'Il n\'y a pas des registres sélectionnés.',

/* './packages/policy/popup_document_register_edit.php' */

'tt_register_adding'=>'Ajout de Registre',
'tt_register_editing'=>'Modification de Registre',

/* './packages/policy/popup_document_register_edit.xml' */

'lb_disposition_cl'=>'Élimination:',
'lb_indexing_type_cl'=>'Type d\'Indexation:',
'lb_origin_cl'=>'Origine:',
'lb_protection_requirements_cl'=>'Exigences de Protection:',
'si_day_days'=>'Jour(s)',
'si_month_months'=>'Mois',
'si_week_weeks'=>'Semaine(s)',
'vb_edit'=>'Modifier',
'vb_lookup'=>'Recherche',
'wn_retention_time'=>'Le temps de rétention doit être plus que zéro',

/* './packages/policy/popup_document_register_read.xml' */

'lb_responsible'=>'Responsable',
'lb_retention_time_cl'=>'<b>Temps de Rétention:</b>',
'lb_storage_place_cl'=>'Lieu de Stockage:',
'lb_storage_type_cl'=>'Type de Stockage:',
'tt_register_reading'=>'Lecture de Registre',
'vb_preferences'=>'Préférences',

/* './packages/policy/popup_document_revise_not_alter.xml' */

'tt_revise_document_without_altering'=>'Réviser le Document sans Modifier',

/* './packages/policy/popup_document_revision_task.php' */

'mx_scheduled_revision'=>'Révision Prévue',

/* './packages/policy/popup_document_revision_task.xml' */

'lb_author_cl'=>'Auteur:',
'lb_file_cl'=>'Fichier:',
'lb_link_cl'=>'Lien:',
'tt_document_revision'=>'Révision de Document',
'vb_create_new_version'=>'Créer une Nouvelle Version',
'vb_download'=>'Télécharger',
'vb_revise_without_altering'=>'Réviser sans Modifier',
'vb_visit_link'=>'Lien de Visite',
'vb_visualize'=>'Voir',

/* './packages/policy/popup_document_search.xml' */

'tt_document_search'=>'Recherche de Document',
'wn_no_document_selected'=>'Il n\'y a pas de document sélectionné.',

/* './packages/policy/popup_documents_search.xml' */

'tt_current_documents_bl'=>'<b>Documents Courants</b>',

/* './packages/policy/popup_modification_comment.php' */

'tt_popup_modification_comment_creation'=>'Création de Commentaire',
'tt_popup_modification_comment_creationfasdfawfqwerqwe'=>'Création de Commentaire',

/* './packages/policy/popup_modification_comment.xml' */

'lb_comment_bl_cl'=>'<b>Commentaire:</b>',
'tt_comment_modification'=>'Modification de Commentaire',
'tt_comment_modification2'=>'Modification de Commentaire',

/* './packages/policy/popup_readers_edit.xml' */

'si_all_processes'=>'Tous les Processus',
'tt_current_readers_bl'=>'<b>Lecteurs Actuels</b>',
'tt_readers_editing'=>'Modification de Lecteurs',

/* './packages/policy/popup_readers_view.xml' */

'tt_readers_visualization'=>'Visualisation des Lecteurs',

/* './packages/policy/popup_register_readers_edit.xml' */

'tt_current_readers'=>'<b>Lecteurs Actuels</b>',
'tt_readers_edit'=>'Modification de Lecteurs',

/* './packages/policy/popup_revision_justification.xml' */

'tt_revision_justification'=>'Raison de Révision',
'vb_send_for_revision'=>'Envoyer à la Révision',

/* './packages/policy/popup_subdocuments_management.php' */

'st_create_subdocument'=>'Créer Sous-Document',

/* './packages/policy/popup_subdocuments_management.xml' */

'tt_manage_subdocuments'=>'Gérer les Sous-Documents',

/* './packages/policy/popup_template_search.xml' */

'rb_suggested_by_best_practice_only'=>'Afficher uniquement les modèles de meilleures pratiques proposées',
'rb_suggested_only'=>'Seulement Suggéré',
'tt_document_templates_search'=>'Recherche de Modèles de Documents',

/* './packages/policy/report/popup_report_accessed_documents.php' */

'rs_accessed_documents'=>'Audit d\'Accès d\'Utilisateurs',
'rs_management'=>'Gestion',
'rs_others'=>'Autres',

/* './packages/policy/report/popup_report_accessed_documents.xml' */

'rs_type'=>'Type',

/* './packages/policy/report/popup_report_contexts_without_documents.php' */

'rs_items_with_without_documents'=>'Composants sans Documents',

/* './packages/policy/report/popup_report_contexts_without_documents.xml' */

'rs_components'=>'Composants',

/* './packages/policy/report/popup_report_doc_instance_comment_and_justification.php' */

'mx_doc_instance_comment_and_justification'=>'Commentaires et Raisons de Documents',

/* './packages/policy/report/popup_report_doc_instance_comment_and_justification.xml' */

'rs_header_instance_modify_comment'=>'Modifier Commentaire',
'rs_header_instance_rev_justification'=>'Raison de Révision',

/* './packages/policy/report/popup_report_docs_with_high_frequency_revision.php' */

'tt_docs_with_high_frequency_revision'=>'Documents avec une fréquence élevée de révision',

/* './packages/policy/report/popup_report_docs_with_high_frequency_revision.xml' */

'rs_justification'=>'Raison',

/* './packages/policy/report/popup_report_document_accesses.php' */

'rs_accesses_to_documents'=>'Audit d\'Accès aux Documents',
'rs_no_type'=>'Pas de Type',

/* './packages/policy/report/popup_report_document_accesses.xml' */

'rs_date'=>'Date',
'rs_time'=>'Temps',

/* './packages/policy/report/popup_report_document_approvers.php' */

'rs_approvers'=>'Les Documents et leurs Approbateurs',

/* './packages/policy/report/popup_report_document_dates.php' */

'rs_documents_dates'=>'Dates de Création et de Révision des Documents',

/* './packages/policy/report/popup_report_document_dates.xml' */

'rs_created_on_cl'=>'Créée le:',

/* './packages/policy/report/popup_report_document_readers.php' */

'rs_readers'=>'Les Documents et leurs Lecteurs',

/* './packages/policy/report/popup_report_document_readers.xml' */

'rs_author_cl'=>'Auteur:',

/* './packages/policy/report/popup_report_document_registers.php' */

'rs_registers_per_document'=>'Registre par Document',

/* './packages/policy/report/popup_report_document_registers.xml' */

'rs_name'=>'Nom',
'rs_retention_time'=>'Temps de Rétention',

/* './packages/policy/report/popup_report_document_summary.php' */

'mx_report_document_summary'=>'Propriétés de Document',

/* './packages/policy/report/popup_report_documents_by_area.php' */

'rs_documents_per_area'=>'Documents par Domaine',

/* './packages/policy/report/popup_report_documents_by_area.xml' */

'rs_document_documents'=>'document(s)',

/* './packages/policy/report/popup_report_documents_by_component.php' */

'rs_document_of_component'=>'%component% documents',
'tt_documents_by_component'=>'Documents par Composant ERMS',

/* './packages/policy/report/popup_report_documents_by_state.php' */

'tt_documents_by_state'=>'Documents',

/* './packages/policy/report/popup_report_documents_by_type.php' */

'rs_documents_by_type'=>'Documents par Type',

/* './packages/policy/report/popup_report_documents_revision_late.php' */

'mx_documents_revision_late'=>'Documents avec Retard de Révision',

/* './packages/policy/report/popup_report_documents_revision_late.xml' */

'rs_document_next_revision'=>'Prochaine Révision',

/* './packages/policy/report/popup_report_documents_revision_schedule.php' */

'mx_documents_revision_schedule'=>'Calendrier de Révision de Documents',

/* './packages/policy/report/popup_report_documents_revision_schedule.xml' */

'rs_document_name'=>'Nom',
'rs_document_schedule'=>'Planification',

/* './packages/policy/report/popup_report_documents_without_register.php' */

'rs_document_main_approver_name'=>'Responsable de document',
'rs_document_name_bl'=>'Document',
'tt_documents_without_register'=>'Documents sans Registre',

/* './packages/policy/report/popup_report_not_accessed_documents.php' */

'rs_not_accessed_documents'=>'Documents Non Consultés',

/* './packages/policy/report/popup_report_pendant_approvals.php' */

'tt_pendant_approvals'=>'Approbations En Attente',

/* './packages/policy/report/popup_report_pendant_approvals.xml' */

'rs_documents'=>'Documents',

/* './packages/policy/report/popup_report_registers_by_type.php' */

'rs_registers_by_type'=>'Registre par Type',

/* './packages/policy/report/popup_report_top_accessed_documents.php' */

'rs_most_accessed_documents'=>'Les Documents les plus Consultés',

/* './packages/policy/report/popup_report_top_accessed_documents.xml' */

'rs_accesses_cl'=>'Accès:',

/* './packages/policy/report/popup_report_top_revised_documents.php' */

'rs_most_revised_documents'=>'Documents les plus Révisés',

/* './packages/policy/report/popup_report_top_revised_documents.xml' */

'rs_revision_date'=>'Date de Révision',
'rs_revisions_cl'=>'Révisions:',
'rs_version'=>'Version',

/* './packages/policy/report/popup_report_user_comments.php' */

'mx_user_comments'=>'Commentaires d\'Utilisateurs',

/* './packages/policy/report/popup_report_user_comments.xml' */

'rs_comment_date'=>'Date',
'rs_comment_text'=>'Commentaire',
'rs_comment_user'=>'Utilisateur',

/* './packages/policy/report/popup_report_users_by_process.php' */

'rs_users_per_process'=>'Utilisateurs associés à des Processus',

/* './packages/policy/report/popup_report_users_by_process_and_area.xml' */

'rs_user_bl_cl'=>'<b>Utilisateur:</b>',

/* './packages/policy/report/popup_report_users_with_pendant_read.php' */

'mx_documents_pendent_reads'=>'Les utilisateurs avec des lectures en attente',

/* './packages/policy/tab_policy_management.xml' */

'ti_all'=>'Tous',
'ti_developing'=>'En cours de Développement',
'ti_obsolete'=>'Obsolète',
'ti_publish'=>'Publié',
'ti_registers'=>'Registres',
'ti_revision'=>'En Révision',
'ti_to_approve'=>'En cours d\'Approbation',
'ti_to_be_published'=>'À être publié',

/* './packages/risk/nav_area.php' */

'st_area_remove_confirm'=>'Etes-vous sûr de vouloir supprimer le domaine<b>%area_name%</b>?',
'tt_remove_area'=>'Supprimer Domaine',

/* './packages/risk/nav_area.xml' */

'gc_subarea'=>'Sous-Domaines',
'lb_sub_areas_count_cl'=>'Nombre de Sous-Domaines:',
'mi_processes'=>'Processus',
'mi_sub_areas'=>'Sous-Domaines',
'tt_business_areas_bl'=>'<b>Domaines d\'Activité</b>',

/* './packages/risk/nav_asset.php' */

'grid_asset_filter_dependents'=>'(dépendances de l\'actif \'<b>%asset_name%</b>\')',
'st_asset_copy'=>'Copier',
'st_asset_remove_confirm'=>'Etes-vous sûr de vouloir supprimer l\'Actif<b>%asset_name%</b>?',
'tt_remove_asset'=>'Suppression d\'Actif',

/* './packages/risk/nav_asset.xml' */

'gc_asset_category'=>'Catégorie d\'Actifs',
'gc_asset_dependencies'=>'Dép.(nces)',
'gc_asset_dependents'=>'Dép.(nts)',
'gc_processes'=>'Processus',
'lb_process_count_cl'=>'Nombre de processus:',
'mi_associate_processes'=>'Associer Processus',
'mi_duplicate'=>'Dupliquer',
'mi_edit_dependencies'=>'Ajuster les Dépendances',
'mi_edit_dependents'=>'Ajuster les Dépendants',
'mi_insert_risk'=>'Insérer des Risques',
'mi_load_risks_from_library'=>'Charger des risques de la bibliothèque',
'tt_assets_bl'=>'<b>Actifs</b>',

/* './packages/risk/nav_control.php' */

'st_control_remove_confirm'=>'Etes-vous sûr de vouloir supprimer le Contrôle<b>%control_name%</b>?',

/* './packages/risk/nav_control.xml' */

'gc_cost'=>'Coût',
'gc_icon'=>'Icône',
'gc_risks'=>'Risques',
'lb_risk_count_cl'=>'Nombre de Risques:',
'mi_associate_risks'=>'Associer Risques',
'mi_view_risks'=>'Voir Risques',
'rb_active'=>'Activé',
'rb_not_active'=>'Désactivé',
'tt_controls_directory_bl'=>'<b>Contrôles Répertoire</b>',
'vb_revision_and_test_history'=>'Historique de Révision/Test',

/* './packages/risk/nav_process.php' */

'grid_process_asset_filter'=>'(filtré par l\'actif \'<b>%asset_name%</b>\')',
'st_process_remove_confirm'=>'Etes-vous sûr de vouloir supprimer le Processus<b>%process_name%</b>?',
'tt_remove_process'=>'Supprimer Processus',

/* './packages/risk/nav_process.xml' */

'cb_not_estimated'=>'Pas Estimé',
'gc_assets'=>'Actifs',
'gc_responsible'=>'Responsable',
'gc_users'=>'Utilisateurs',
'lb_asset_count_cl'=>'Nombre d\'Actifs:',
'lb_responsible_cl'=>'Responsable:',
'lb_value_cl'=>'Risque:',
'mi_add_document'=>'Créer Document',
'mi_associate_assets'=>'Associer Actifs',
'mi_documents'=>'Documents',
'mi_view_assets'=>'Voir Actifs',
'si_all_priorities'=>'Tous',
'si_all_responsibles'=>'Tous',
'si_all_types'=>'Tous',
'tt_processes_bl'=>'<b>Processus</b>',

/* './packages/risk/nav_report.php' */

'mx_no_priority'=>'Pas de Priorité',
'mx_no_type'=>'Pas de Type',
'si_amount_of_risks_per_area'=>'Nombre de Risques par Domaine',
'si_amount_of_risks_per_process'=>'Nombre de Risques par Processus',
'si_areas_without_processes'=>'Domaines sans Processus',
'si_assets'=>'Actifs',
'si_assets_importance'=>'Importance des Actifs',
'si_assets_without_risks'=>'Actifs sans Evénements de Risque',
'si_checklist_per_event'=>'Liste de Vérification par Actif',
'si_controls_efficiency'=>'Efficacité des Contrôles',
'si_controls_implementation_date'=>'Date de Mise en Oeuvre des Contrôles',
'si_controls_per_responsible'=>'Contrôles par Responsable',
'si_controls_per_risk'=>'Contrôles par Risque',
'si_controls_without_risks'=>'Contrôles sans Risques Associés',
'si_cost_of_controls_per_area'=>'Coût des Contrôles par Domaine',
'si_cost_of_controls_per_asset'=>'Coût des Contrôles par Actif',
'si_cost_of_controls_per_process'=>'Coût des Contrôles par Processus',
'si_cost_of_controls_per_responsible'=>'Coût des Contrôles par Responsable',
'si_cost_per_control'=>'Coût par Contrôle',
'si_duplicated_risks'=>'Duplicata de Risques',
'si_element_classification'=>'Classification d\'Objets',
'si_follow_up_of_controls'=>'Suivi des Contrôles',
'si_follow_up_of_controls_efficiency'=>'Suivi de l\'Efficacité des Contrôles',
'si_follow_up_of_controls_test'=>'Suivi de Test de Contrôles',
'si_non_parameterized_risks'=>'Risques Non-Estimés',
'si_not_measured_controls'=>'Contrôles Non Mesurés',
'si_pending_items'=>'Objets En Attente',
'si_pending_tasks'=>'Tâches En Attente',
'si_plan_of_risks_treatment'=>'Plan de Traitement de Risque',
'si_planning_of_controls'=>'Planification des Contrôles',
'si_processes_without_assets'=>'Processus sans Actifs',
'si_report_conformity'=>'Conformité',
'si_risk_impact'=>'Impact de Risque',
'si_risk_status_per_area'=>'État de Risque par Domaine',
'si_risk_status_per_process'=>'État de Risque par Processus',
'si_risks_per_area'=>'Risques par Domaine',
'si_risks_per_asset'=>'Risques par Actif',
'si_risks_per_control'=>'Risques par Contrôle',
'si_risks_per_process'=>'Risques par Processus',
'si_risks_values'=>'Valeurs de Risques',
'si_sgsi_policy_statement'=>'Énoncé du Règlement de ERMS',
'si_sgsi_scope_statement'=>'Déclaration du Champ d\'Application de ERMS',
'si_statement_of_applicability'=>'Déclaration d\'Applicabilité',
'si_summary_of_controls'=>'Résumé des Contrôles',
'si_top_10_assets_with_highest_risks'=>'Top 10 Actifs avec le plus de Risques Élevés',
'si_top_10_assets_with_most_risks'=>'Top 10 Actifs avec le plus de Risques',
'si_top_10_risks_per_area'=>'Top 10 Risques par Domaine',
'si_top_10_risks_per_process'=>'Top 10 Risques par Processus',
'si_users_responsibilities'=>'Responsabilités d\'Utilisateurs',
'st_all_standards'=>'Tous',

/* './packages/risk/nav_report.xml' */

'cb_all'=>'Tous',
'cb_area'=>'Domaine',
'cb_asset'=>'Actif',
'cb_asset_security'=>'Sécurité d\'Actif',
'cb_control'=>'Contrôle',
'cb_high'=>'Élevé',
'cb_low'=>'Faible',
'cb_medium'=>'Moyen',
'cb_not_parameterized'=>'Non évalué',
'cb_organize_by_asset'=>'Organisation par Actif',
'cb_organize_by_process'=>'Organisation par Processus',
'cb_process'=>'Processus',
'lb_classification_cl'=>'Classification:',
'lb_comment_cl'=>'Commentaire:',
'lb_consider_cl'=>'Considérer:',
'lb_filter_by_cl'=>'Filtrer par:',
'lb_filter_by_standard_cl'=>'Filtrer par Standard:',
'lb_filter_cl'=>'Filtre:',
'lb_process_cl'=>'Processus:',
'lb_reponsible_for_cl'=>'Responsable:',
'lb_values_cl'=>'Valeurs:',
'rb_area'=>'Domaine',
'rb_area_process'=>'Domaine et Processus',
'rb_asset'=>'Actif',
'rb_both'=>'Les deux',
'rb_detailed'=>'Détaillé',
'rb_efficient'=>'Efficace',
'rb_implemented'=>'Exécuté',
'rb_inefficient'=>'Inefficace',
'rb_planned'=>'Planifié',
'rb_process'=>'Processus',
'rb_real'=>'Potentiel',
'rb_residual'=>'Résiduel',
'rb_summarized'=>'Résumé',
'si_all_reports'=>'Tous les Rapports',
'si_areas_by_priority'=>'Domaines par Priorité',
'si_areas_by_type'=>'Domaines par Type',
'si_control_reports'=>'Rapports de Contrôle',
'si_controls_by_type'=>'Contrôles par Type',
'si_events_by_type'=>'Événements par Types',
'si_financial_reports'=>'Rapports Financiers',
'si_iso_27001_reports'=>'Rapports ISO 31000',
'si_others'=>'Autres',
'si_processes_by_priority'=>'Processus par Priorité',
'si_processes_by_type'=>'Processus par Type',
'si_risk_management_reports'=>'Rapports de Gestion de Risque',
'si_risk_reports'=>'Rapport de Risque',
'si_risks_by_type'=>'Risques par Type',
'si_summaries'=>'Résumés',
'st_area_priority'=>'Priorités de Domaine',
'st_area_type'=>'Types de Domaine',
'st_asset_cl'=>'Actif:',
'st_config_filter_type_priority'=>'Configurer Filtre Sélectionné:',
'st_control_type'=>'Types de Contrôles',
'st_event_type'=>'Types d\'Événements',
'st_no_filter_available'=>'Pas de filtres disponibles.',
'st_none_selected'=>'Sélectionnez un Filtre',
'st_organize_by_asset'=>'Organiser par Actifs',
'st_process_priority'=>'Priorités de Processus',
'st_process_type'=>'Types de Processus',
'st_risk_type'=>'Types de Risque',
'st_show_cl'=>'Montrer:',
'st_standard_cl'=>'Norme:',
'tt_specific_report_filters'=>'Filtres Spécifiques à Rapport',
'tt_type_and_priority_filter_cl'=>'Filtres de Type et de Priorité:',
'vb_pre_report'=>'Pré-Rapport',
'vb_report'=>'Rapport',
'wn_no_report_selected'=>'Il n\'y a pas de rapport sélectionné.',
'wn_select_at_least_one_filter'=>'Au moins un filtre doit être choisi!',
'wn_select_at_least_one_parameter'=>'Au moins un paramètre doit être sélectionné.',
'wn_select_risk_value'=>'S\'il vous plaît',

/* './packages/risk/nav_risk.php' */

'gs_avoided'=>'Évité',
'gs_reduced'=>'Réduit',
'gs_restrained'=>'Retenu',
'gs_transferred'=>'Transféré',
'st_denied_permission_to_edit'=>'Vous n\'avez pas la permission de modifier le',
'st_risk_remove_confirm'=>'Etes-vous sûr de vouloir supprimer le risque<b>%risk_name%</b>?',
'st_risk_remove_has_incident'=>'<br/><br/><b>Attention: Un ou des incident(s) sont associés au risque.</b>',
'st_risk_remove_many_confirm'=>'Êtes-vous sûr de vouloir supprimer les<b>%count%</b> risques sélectionnés?',
'st_risk_remove_many_have_incident'=>'<br/><b>Attention: Un ou des incident(s) sont associés à au moins un risque.</b>',
'tt_incident_filter'=>'filtré par incident \'<b>%incident_name%</b>\'',
'tt_remove_risk'=>'Supprimer Risque',

/* './packages/risk/nav_risk.xml' */

'cb_avoided'=>'Évité',
'cb_not_treated'=>'Non Traité',
'cb_reduced'=>'Réduit',
'cb_restrained'=>'Retenu',
'cb_search_in_name_only'=>'ne rechercher que dans le nom',
'cb_transferred'=>'Transféré',
'gc_controls'=>'Contrôles',
'gc_residual'=>'Résiduel',
'gc_risk'=>'Risque',
'gc_risk_name'=>'Nom',
'gc_status'=>'État',
'gc_treatment'=>'Traitement',
'lb_control_count_cl'=>'Nombre de Contrôles:',
'lb_name_description_cl'=>'Nom / Description',
'lb_risk_residual_value_cl'=>'Résiduelle:',
'lb_risk_value_cl'=>'Risque:',
'lb_status_cl'=>'État:',
'lb_treatment_type_cl'=>'Type de Traitement:',
'mi_avoid'=>'Éviter le Risque',
'mi_cancel_treatment'=>'Annuler le Traitement',
'mi_hold_accept'=>'Conserver / Accepter le Risque',
'mi_reduce_risk'=>'Réduire le Risque',
'mi_transfer'=>'Transfert de Risque',
'mi_view_controls'=>'Voir Contrôles',
'si_all_assets'=>'Tous',
'si_all_states'=>'Tous',
'st_from'=>'à partir de',
'st_to'=>'à',
'vb_aply'=>'Appliquer',
'vb_parameterize'=>'Estimation',

/* './packages/risk/popup_area_edit.php' */

'tt_area_adding'=>'Ajouter Domaine',
'tt_area_editing'=>'Modifier Domaine',

/* './packages/risk/popup_area_edit.xml' */

'vb_ok'=>'Ok',

/* './packages/risk/popup_asset_dependencies.php' */

'st_dependencies_list'=>'Liste des Dépendances',
'st_dependents_list'=>'Liste de Dépendants',
'tt_dependencies_adjustment'=>'Ajustement de Dépendances',
'tt_dependents_adjustment'=>'Ajustement de Dépendants',

/* './packages/risk/popup_asset_dependencies.xml' */

'tt_assets_list'=>'Liste des Actifs',

/* './packages/risk/popup_asset_edit.php' */

'tt_asset_adding'=>'Ajout d\'Actif',
'tt_asset_duplicate'=>'Duplication d\'Actif',
'tt_asset_editing'=>'Modification d\'Actif',

/* './packages/risk/popup_asset_edit.xml' */

'lb_category_bl_cl'=>'<b>Catégorie:</b>',
'lb_demands_attention_to_legal_conformity_cl'=>'Demande l\'attention à la Conformité Juridique:',
'lb_justification_cl'=>'Raison:',
'lb_security_responsible_bl_cl'=>'<b>Responsable de Sécurité:</b>',
'lb_value_cl_money_symbol'=>'Coût',
'st_asset_parametrization_bl'=>'<b>Pertinence d\'Actif</b>',
'vb_adjust_dependencies'=>'Ajuster les Dépendances',
'vb_adjust_dependents'=>'Ajuster les Dépendants',

/* './packages/risk/popup_asset_search.xml' */

'wn_no_asset_selected'=>'Il n\'y a pas d\'actif sélectionné.',

/* './packages/risk/popup_best_practice_search.xml' */

'lb_section_cl'=>'Section:',
'si_all'=>'Tous',
'tt_best_practices_search'=>'Recherche des Meilleures Pratiques',

/* './packages/risk/popup_control_cost.xml' */

'lb_total'=>'Total',
'tt_control_cost'=>'Coût de Contrôle',

/* './packages/risk/popup_control_edit.php' */

'st_and'=>'et',
'st_of_revision'=>'de révision',
'st_of_test'=>'de test',
'st_remove_control_implementation_confirm'=>'Etes-vous sûr de vouloir supprimer la mise en oeuvre du contrôle<b>%control_name%</b>?',
'st_result_in_removing'=>'Cela se traduira par la suppression',
'st_this_control'=>'ce contrôle',
'tt_control_adding'=>'Ajout de Contrôle',
'tt_remove_control_implementation'=>'Supprimer la mise en uvre de contrôle',

/* './packages/risk/popup_control_edit.xml' */

'cb_enable_control_revision_cl'=>'Activer la Révision de Contrôle:',
'cb_enable_control_tests_cl'=>'Activer les Tests de Contrôle:',
'lb_best_practices_bl_cl'=>'<b>Meilleures Pratiques:</b>',
'lb_control_cost_cl'=>'Coût de Contrôle:',
'lb_evidence_cl'=>'Évidence:',
'lb_implementation_limit_bl_cl'=>'<b>Limite de Mise en Oeuvre:</b>',
'lb_implemented_on_cl'=>'Mise en oeuvre le:',
'lb_send_alert_task_bl_cl'=>'<b>Envoyer Alerte/Tâche:</b>',
'st_day_days_before'=>'jour(s) avant',
'st_money_symbol'=>'$',
'tt_control_editing'=>'Modification de Contrôle',
'vb_add'=>'Ajouter',
'vb_control_cost'=>'Coût de Contrôle',
'vb_implement'=>'Mettre en oeuvre',
'vb_remove_implementation'=>'Supprimer la Mise en oeuvre',
'vb_revision'=>'Révision',
'vb_tests'=>'Tests',
'wn_implementation_cannot_be_null'=>'La date de mise en oeuvre ne peut pas être nulle',
'wn_implementation_limit_after_today'=>'La date limite doit être égale ou postérieure à la date actuelle.',

/* './packages/risk/popup_control_revision.xml' */

'lb_expected_efficiency_bl_cl'=>'<b>Efficacité Attendue:</b>',
'lb_high_value_5_cl'=>'(Haute) Valeur 5:',
'lb_low_value_1_cl'=>'(Basse) Valeur 1:',
'st_helper'=>'[?]',
'to_control_revision_expected_efficiency'=>'<b>Efficacité attendue:</b><br/><br/>Déterminer le niveau de service qui est attendu pour le contrôle dans la période spécifiée.<br/><br/>Envisager la performance nécessaire pour réduire les risques associés à ce contrôle.',
'to_control_revision_metric_help'=>'<b>Métrique:</b><br/><br/>Préciser la manière de mesurer ce contrôle.',
'to_control_revision_value'=>'<b>Valeurs:</b><br/><br/>Établir une échelle pour la mesure des niveaux de service possibles de ce contrôle au cours de la période.<br/><br/>Considérer les faibles valeurs comme une mauvaise performance',
'tt_control_efficiency_revision'=>'Révision de l\'Efficacité des Contrôles',

/* './packages/risk/popup_control_revision_and_test_edit.php' */

'st_control_test_not_ok'=>'Pas Ok',
'st_control_test_ok'=>'Ok',
'st_revision_remove_confirm'=>'Êtes-vous sûr de vouloir supprimer la révision avec la Date Estimée le<b>%deadline_date%</b> et la Date de Réalisation le<b>%realized_date%</b> ?',
'st_revision_update_by_insert_confirm'=>'Il y a une révision de contrôle à la date estimée le<b>%date_to_do%</b>. Etes-vous sûr de vouloir substituer cette révision de contrôle?',
'st_test_remove_confirm'=>'Êtes-vous sûr de vouloir supprimer le test avec la Date Estimée le<b>%deadline_date%</b> et la Date de Réalisation le<b>%realized_date%</b> ?',
'st_test_update_by_insert_confirm'=>'Il y a un test de contrôle à la Date Estimée le<b>%date_to_do%</b>. Etes-vous sûr de vouloir substituer ce test de contrôle?',
'tt_control_efficiency_history_add_title'=>'Insérer la Révision de Contrôle',
'tt_control_test_history_add_title'=>'Insérer Test de Contrôle',
'tt_remove_control'=>'Supprimer la Mise en Oeuvre de Contrôle',
'tt_replace_control_revision'=>'Remplacement de la Révision du Contrôle<b>%control_name%</b>',
'tt_replace_control_test'=>'Remplacement de Test de Contrôle<b>%control_name%</b>',

/* './packages/risk/popup_control_revision_and_test_edit.xml' */

'gc_date_realized'=>'Date de Réalisation',
'gc_date_to_do'=>'Date Estimée',
'gc_expected_efficiency'=>'EE',
'gc_real_efficiency'=>'RE',
'gc_realization_date'=>'Date de Réalisation',
'gc_test_date_to_do'=>'Date Estimée',
'gc_test_value'=>'Valeur de Test',
'mi_edit'=>'Modifier',
'mi_remove'=>'Supprimer',
'mi_revision_edit'=>'Modifier Révisions',
'mi_test_edit'=>'Modifier Tests',
'rb_not_ok'=>'Pas Ok',
'rb_ok'=>'Ok',
'st_control_test_result'=>'<b>Test:</b>',
'st_date_realized'=>'<b>Date de Réalisation:</b>',
'st_date_to_do'=>'<b>Date Estimée:</b>',
'st_description'=>'Description:',
'st_er'=>'RE',
'test_date_to_do'=>'<b>Date Estimée </b>',
'tt_add_and_edit_revision_and_test_control'=>'Ajout ou Modification de Contrôle Test et Révision',
'tt_control_efficiency_history_edit_title'=>'Modification de Révision de Contrôle',
'tt_control_selected_revision_history'=>'Historique des Révisions de Contrôle',
'tt_control_selected_test_history'=>'Histoire de test de contrôle',
'tt_control_test_history_edit_title'=>'Modification de Test de Contrôle',
'vb_insert'=>'Insérer',

/* './packages/risk/popup_control_revision_task.php' */

'tt_control_revision_task_incident_revision'=>'Mesure d\'Efficacité de Contrôle Provoquée (par l\'Incident %incident_name%)',

/* './packages/risk/popup_control_revision_task.xml' */

'lb_from'=>'à partir de',
'lb_start_cl'=>'Début:',
'lb_to'=>'à',
'lb_value_1_cl'=>'Valeur 1:',
'lb_value_2_cl'=>'Valeur 2:',
'lb_value_3_cl'=>'Valeur 3:',
'lb_value_4_cl'=>'Valeur 4:',
'lb_value_5_cl'=>'Valeur 5:',
'st_ee'=>'EE',
'st_er_bl'=>'<b>RE</b>',
'st_high_parenthesis'=>'(Élevé)',
'st_low_parenthesis'=>'(Faible)',
'st_metric'=>'Métrique',
'st_total_incidents_found_on_period'=>'Total d\'incidents trouvés dans la période:',
'st_total_incidents_on_time'=>'Total d\'incidents dans la période courante:',
'to_expected_efficiency'=>'Efficacité Attendue',
'to_real_efficiency'=>'Efficacité Réelle',
'tt_control_revision_task'=>'Révision de Contrôle',
'tt_incidents_occurred_from_period'=>'Incidents survenus de %date_begin% à %date_today%',
'vb_filter'=>'Filtre',

/* './packages/risk/popup_control_risk_edit.php' */

'tt_risk_control_association_editing'=>'Modifier Association Risque-Contrôle',

/* './packages/risk/popup_control_risk_edit.xml' */

'tt_risks_bl'=>'<b>Risques</b>',
'vb_associate_risk'=>'Associer Risque',
'vb_disassociate_risks'=>'Dissocier les Risques',

/* './packages/risk/popup_control_risk_search.xml' */

'gc_asset'=>'Actif',
'tt_risks_search'=>'Recherche de Risques',
'vb_associate_risks'=>'Risques Associés',
'wn_no_risk_selected'=>'Il n\'y a pas de risque sélectionné.',

/* './packages/risk/popup_control_search.xml' */

'vb_associate'=>'Associer',

/* './packages/risk/popup_control_test.xml' */

'lb_schedule_bl_cl'=>'<b>Planification</b>',
'tt_control_test_revision'=>'Révision de Test de Contrôle',

/* './packages/risk/popup_control_test_task.xml' */

'cb_no'=>'Non',
'cb_open_next_task'=>'Ouvrir prochaine tâche',
'cb_yes'=>'Oui',
'lb_description_test'=>'Description de Test:',
'lb_observations_cl'=>'Observations:',
'lb_test_ok_bl_cl'=>'<b>Test ok:</b>',
'tt_control_test'=>'Test de Contrôle',

/* './packages/risk/popup_create_risks_from_category.xml' */

'tt_risk_creation_from_category'=>'Création de Risque à partir d\'une catégorie d\'événement',

/* './packages/risk/popup_create_risks_from_events.xml' */

'st_select_events_not_to_associate'=>'Ne sélectionnez ci-dessous que les événements que vous<b>NE</b>voulez<b>PAS</b> relier à l\'actif!',
'tt_risk_creation_from_events'=>'Création de Risque à partir d\'Événements',
'vb_link_events_later'=>'Relier les Événements Plus Tard',
'vb_relate_events_to_asset'=>'Relier des Événements à l\'Actif',

/* './packages/risk/popup_process_asset_association.xml' */

'tt_assets_search'=>'Recherche d\'Actifs',
'tt_current_assets_bl'=>'<b>Actifs Courants</b>',

/* './packages/risk/popup_process_edit.php' */

'st_no_published_documents'=>'Il n\'y a pas de documents publiés',
'tt_process_adding'=>'Ajouter Processus',
'tt_process_editing'=>'Modifier Processus',

/* './packages/risk/popup_process_edit.xml' */

'lb_area_bl_cl'=>'<b>Domaine:</b>',
'lb_document_cl'=>'Document:',
'lb_name_bl_cl'=>'<b>Nom:</b>',
'lb_priority_cl'=>'Priorité:',
'lb_responsible_bl_cl'=>'<b>Responsable:</b>',
'vb_associate_users'=>'Associer des Utilisateurs',
'vb_properties'=>'Propriétés',
'vb_view'=>'Voir',

/* './packages/risk/popup_process_search.xml' */

'gc_area'=>'Domaine',
'lb_area_cl'=>'Domaine:',
'si_all_areas'=>'Toutes les Domaines',
'tt_current_processes_bl'=>'<b>Processus en Cours</b>',
'tt_processes_search'=>'Recherche de Processus',
'vb_remove'=>'Supprimer',

/* './packages/risk/popup_risk_control_association_edit.php' */

'st_control_not_mitigating_efficiency_message'=>'Le Contrôle ne réduit pas de Risques parce que l\'Efficacité Réelle est plus faible que prévue!',
'st_control_not_mitigating_not_implemented_message'=>'Le contrôle ne réduit pas de risques car il n\'est pas mis en oeuvre et la date limite de mise en oeuvre a expiré!',

/* './packages/risk/popup_risk_control_association_edit.xml' */

'lb_control_cl'=>'Contrôle:',
'st_control_parametrization_bl_cl'=>'<b>Estimation de Control:</b>',
'st_values_to_reduce_risk_consequence_and_probability'=>'Les valeurs sur le côté définiront par quelle quantité ce contrôle permettra de réduire les conséquences et la probabilité des risque réel.',
'tt_risk_reduction'=>'Réduction de Risque',

/* './packages/risk/popup_risk_control_edit.php' */

'tt_control_risk_association_editing'=>'Modifier Association Contrôle-Risque',

/* './packages/risk/popup_risk_control_edit.xml' */

'lb_risk_cl'=>'Risque:',
'mi_edit_association'=>'Modifier Association',
'st_residual'=>'Résiduel',
'st_risk'=>'Risque',
'tt_controls_bl'=>'<b>Contrôles</b>',
'vb_disassociate_controls'=>'Dissocier Contrôles',

/* './packages/risk/popup_risk_control_search.xml' */

'cb_search_only_suggested_controls'=>'Rechercher seulement les Contrôles suggérés',
'gc_name'=>'Nom',
'lb_name_cl'=>'Nom:',
'tt_control_search'=>'Recherche de Contrôle',
'vb_associate_control'=>'Associer Contrôle',
'wn_no_control_selected'=>'Il n\'y a pas de contrôle sélectionné.',

/* './packages/risk/popup_risk_edit.php' */

'st_sensitive_data_reapproval'=>'Vous changez des données sensibles',
'tt_reapproval'=>'Ré-Approbation',
'tt_risk_editing'=>'Modification de Risque',

/* './packages/risk/popup_risk_edit.xml' */

'lb_asset_bl_cl'=>'<b>Actif:</b>',
'lb_event_bl_cl'=>'<b>Événement:</b>',
'lb_impact_cl_money_symbol'=>'Impact Financier:',
'lb_real_risk_bl_cl'=>'<b>Risque Potentiel:</b>',
'lb_residual_risk_bl_cl'=>'<b>Risque Résiduel:</b>',
'lb_type_cl'=>'Type:',
'si_select_one_of_the_items_below'=>'Sélectionnez l\'un des objets ci-dessous',
'st_risk_parametrization_bl_cl'=>'<b>Estimation de Risque:</b>',
'static_static19'=>'Impact:',
'vb_calculate_risk'=>'Calcul du Risque',
'vb_config'=>'Configurer',
'vb_find'=>'Trouver',
'vb_probability_automatic_calculation'=>'Calcul automatique de la probabilité',
'warning_wn_asset_desparametrized'=>'Il n\'est pas possible de calculer la valeur du risque',
'warning_wn_control_desparametrized'=>'au moins un contrôle n\'est pas encore évalué. Avant de calculer le risque résiduel',

/* './packages/risk/popup_risk_insert_search_event_asset.xml' */

'gc_category'=>'Catégorie',
'gc_description'=>'Description',
'gc_impact'=>'Impact',
'lb_asset_cl'=>'Actif:',
'lb_category_cl'=>'Catégorie:',
'lb_description_cl'=>'Description:',
'si_all_categories'=>'Toutes les Catégories',
'tt_events_search'=>'Recherche d\'Evénements',
'vb_create_risk'=>'Créer Risque',
'vb_search'=>'Recherche',
'wn_no_event_selected'=>'Il n\'y a pas d\'élément sélectionné.',

/* './packages/risk/popup_risk_insert_type.xml' */

'rb_create_independent_risk'=>'Créer un risque indépendant',
'st_which_action_to_take'=>'Quelle action souhaitez-vous prendre?',
'tt_risk_adding'=>'Ajout des Risque',
'vb_cancel'=>'Annuler',
'vb_create'=>'Créer',
'vb_create_risk_from_event'=>'Créer risque à partir d\'un événement (recommandé)',

/* './packages/risk/popup_risk_parameters_weight.xml' */

'tt_risk_parameters_weight'=>'Poids des paramètres de risque',

/* './packages/risk/popup_risk_parametrization.php' */

'mx_probability'=>'Probabilité',
'st_denied_permission_to_estimate_risks'=>'Vous n\'avez pas la permission d\'évaluer plusieurs risques',
'to_impact_cl'=>'Impact:',
'to_risk_cl'=>'Risque:',

/* './packages/risk/popup_risk_parametrization.xml' */

'lb_all_bl_cl'=>'<b>Tous:</b>',
'tt_risk_parametrization'=>'Estimation de Risque',

/* './packages/risk/popup_risk_treatment.php' */

'tt_avoid'=>'éviter',
'tt_hold_accept'=>'conserver/accepter',
'tt_transfer'=>'transférer',

/* './packages/risk/popup_risk_treatment.xml' */

'lb_justification_bl_cl'=>'<b>Raison:</b>',
'st_accept_transfer_or_avoid_risk'=>'Voulez-vous<b>%treatment_type%</b> Risque \'<b>%risk_name%</b>\'?',
'tt_risk_treatment'=>'Traitement de Risque (<b>%treatment_type%</b>)',
'vb_save'=>'Enregistrer',

/* './packages/risk/popup_risks_control_association_edit.xml' */

'lb_all_risks_bl_cl'=>'<b>Tous Risques:</b>',
'lb_control_bl_cl'=>'<b>Control:</b>',
'tt_risk_control_association_parametrization'=>'Réduction de Risque',

/* './packages/risk/report/clock.php' */

'st_generating_report'=>'Génération de Rapport...',
'st_operation_wait'=>'<b>Attention:</b> Ce rapport peut prendre quelques minutes à être créé.<br/> S\'il vous plaît',

/* './packages/risk/report/loading_report.php' */

'st_verifying_existing_reports'=>'Vérifier les Rapports Existants.',

/* './packages/risk/report/popup_report_activities.xml' */


/* './packages/risk/report/popup_report_areas.php' */

'mx_areas'=>'Domaines',

/* './packages/risk/report/popup_report_areas.xml' */

'rs_header_area_name'=>'Nom',
'rs_header_area_responsible'=>'Responsable',

/* './packages/risk/report/popup_report_areas_by_priority.php' */

'rs_areas_by_priority'=>'Domaines par Priorité',

/* './packages/risk/report/popup_report_areas_by_type.php' */

'rs_areas_by_type'=>'Domaines par Type',

/* './packages/risk/report/popup_report_areas_without_processes.php' */

'rs_areas_without_processes'=>'Domaines sans Processus',

/* './packages/risk/report/popup_report_asa.xml' */


/* './packages/risk/report/popup_report_asset.php' */

'rs_assets'=>'Actifs',

/* './packages/risk/report/popup_report_asset.xml' */


/* './packages/risk/report/popup_report_asset_deps.php' */

'si_assets_deps'=>'Actifs - Dépendances et Dépendants',
'si_dependencies'=>'Dépendances',
'si_dependents'=>'Dépendants',

/* './packages/risk/report/popup_report_asset_importance.php' */

'rs_assets_importance'=>'Importance d\'Actifs',

/* './packages/risk/report/popup_report_asset_relevance.php' */

'rs_asset_cl_bl'=>'<b>Actif:</b>',
'si_asset_relevance'=>'Pertinence d\'Actif',

/* './packages/risk/report/popup_report_asset_relevance.xml' */

'rs_responsible_cl'=>'Responsable:',

/* './packages/risk/report/popup_report_assets.php' */

'mx_assets'=>'Actifs',

/* './packages/risk/report/popup_report_assets.xml' */

'rs_header_asset_name'=>'Nom',
'rs_header_asset_responsible'=>'Responsable',

/* './packages/risk/report/popup_report_assets_without_risks.php' */

'rs_assets_without_risks'=>'Actifs sans Événements de Risque',

/* './packages/risk/report/popup_report_assets_without_risks.xml' */

'rs_security_responsible'=>'Responsable de Sécurité',

/* './packages/risk/report/popup_report_best_practices.php' */

'mx_best_practices'=>'Meilleures Pratiques',

/* './packages/risk/report/popup_report_best_practices.xml' */

'rs_best_practice_name'=>'Nom',

/* './packages/risk/report/popup_report_bia_quantitative.xml' */


/* './packages/risk/report/popup_report_conformity.php' */

'rs_applied_best_practices'=>'Meilleures Pratiques Employées',
'rs_not_applied_best_practices'=>'Meilleures Pratiques Non-Employées',
'rs_not_used'=>'Pas Utilisé',
'rs_not_used_controls'=>'Contrôles Non Utilisés',
'rs_report_conformity_footer'=>'Total des Meilleures Pratiques = %total% / Total des Meilleures Pratiques Employées = %applied%',
'rs_used_controls'=>'Contrôles Utilisés',
'tt_conformity'=>'Conformité',

/* './packages/risk/report/popup_report_conformity.xml' */

'rs_standard_cl'=>'Norme:',

/* './packages/risk/report/popup_report_control_accompaniment.php' */

'rs_follow_up_of_controls'=>'Suivi des Contrôles',

/* './packages/risk/report/popup_report_control_cost.php' */

'rs_cost_per_control'=>'Coût par Contrôle',

/* './packages/risk/report/popup_report_control_cost_by_area.php' */

'rs_cost_of_controls_per_area'=>'Coût des Contrôles par Domaine',

/* './packages/risk/report/popup_report_control_cost_by_asset.php' */

'rs_cost_of_controls_per_asset'=>'Coût des Contrôles par Actif',

/* './packages/risk/report/popup_report_control_cost_by_cost_category.php' */

'mx_control_cost_by_cost_category'=>'Coût de Contrôle par Catégorie',

/* './packages/risk/report/popup_report_control_cost_by_cost_category.xml' */

'rs_control_cost'=>'Coût',
'rs_control_name'=>'Nom',
'rs_total_cost'=>'Total',

/* './packages/risk/report/popup_report_control_cost_by_process.php' */

'rs_cost_of_controls_per_process'=>'Coût de Contrôles par Processus',

/* './packages/risk/report/popup_report_control_cost_by_responsible.php' */

'rs_cost_of_controls_per_responsible'=>'Coût des Contrôles par Responsable',

/* './packages/risk/report/popup_report_control_cost_by_responsible.xml' */

'rs_cost'=>'Coût',

/* './packages/risk/report/popup_report_control_efficiency.php' */

'rs_controls_efficiency'=>'Efficacité des Contrôles',
'rs_efficient_controls'=>'Contrôles Efficaces',
'rs_inefficient_controls'=>'Contrôles Inefficaces',

/* './packages/risk/report/popup_report_control_efficiency_accompaniment.php' */

'rs_follow_up_of_control_efficiency'=>'Suivi de l\'Efficacité des Contrôles',

/* './packages/risk/report/popup_report_control_efficiency_accompaniment.xml' */

'rs_ee'=>'EE',
'rs_re'=>'RE',

/* './packages/risk/report/popup_report_control_implementation_date.php' */

'rs_controls_implemented_outside_stated_period'=>'Contrôles mis en oeuvre en dehors de la Période Indiquée',
'rs_controls_implemented_within_stated_period'=>'Contrôles mis en oeuvre dans la période indiquée',
'rs_date_of_implementation_of_controls'=>'Date de Mise en Oeuvre des Contrôles',

/* './packages/risk/report/popup_report_control_implementation_date.xml' */

'rs_implementation'=>'Mise en Oeuvre',

/* './packages/risk/report/popup_report_control_planning.php' */

'rs_implementation_date'=>'Date de Mise en Oeuvre',
'rs_implementation_deadline'=>'Délai de Mise en Oeuvre',
'rs_implemented_controls'=>'Contrôles Mis en Oeuvre',
'rs_planned_controls'=>'Contrôles Prévus',
'rs_planning_of_controls'=>'Planification des Contrôles',

/* './packages/risk/report/popup_report_control_revision_agenda_by_responsible.php' */

'si_control_revision_agenda_by_responsible'=>'Ordre du Jour de Révision de Contrôle par Responsable',

/* './packages/risk/report/popup_report_control_revision_agenda_by_responsible.xml' */

'rs_next_revision'=>'Prochaine Révision',

/* './packages/risk/report/popup_report_control_summary.xml' */

'rs_best_practice'=>'Meilleures Pratiques',

/* './packages/risk/report/popup_report_control_summary_pm.php' */

'rs_summary_of_controls'=>'Résumé de Contrôles',

/* './packages/risk/report/popup_report_control_summary_pm.xml' */

'rs_accomplishment_bl_cl'=>'<b>Réalisation:</b>',
'rs_description_cl'=>'<b>Description:</b>',

/* './packages/risk/report/popup_report_control_test_accompaniment.php' */

'rs_ctrl_test_not_ok'=>'Pas OK',
'rs_ctrl_test_ok'=>'Ok',
'rs_follow_up_of_control_test'=>'Suivi des Tests de Contrôles',

/* './packages/risk/report/popup_report_control_test_accompaniment.xml' */

'rs_comment'=>'Commentaire',
'rs_control_cl'=>'Contrôle:',
'rs_date_accomplishment'=>'Date de Réalisation',
'rs_predicted_date'=>'Date Prévue',
'rs_test'=>'Test',

/* './packages/risk/report/popup_report_control_test_agenda_by_responsible.php' */

'si_control_test_agenda_by_responsible'=>'Ordre du Jour de Test de Contrôle par Responsable',

/* './packages/risk/report/popup_report_control_test_agenda_by_responsible.xml' */

'rs_next_test'=>'Prochain Test',
'rs_processes'=>'Processus',
'rs_responsibles'=>'Responsables',
'rs_schedule'=>'Planification',

/* './packages/risk/report/popup_report_controls.php' */

'mx_controls'=>'Contrôles',

/* './packages/risk/report/popup_report_controls.xml' */

'rs_header_control_name'=>'Nom',
'rs_header_control_responsible'=>'Responsable',

/* './packages/risk/report/popup_report_controls_by_responsible.php' */

'rs_controls_per_responsible'=>'Contrôles par Responsable',

/* './packages/risk/report/popup_report_controls_by_risk.php' */

'rs_controls_per_risk'=>'Contrôles par Risque',

/* './packages/risk/report/popup_report_controls_by_risk.xml' */

'rs_affected_asset_bl_cl'=>'<b>Actif Touché:</b>',
'rs_risk_and_impact_bl_cl'=>'<b>Risque / Impact:</b>',
'rs_risk_bl_cl'=>'<b>Risque:</b>',

/* './packages/risk/report/popup_report_controls_by_type.php' */

'rs_controls_by_type'=>'Contrôles par Type',

/* './packages/risk/report/popup_report_controls_not_measured.php' */

'rs_not_measured_controls'=>'Contrôles Non Mesuré',

/* './packages/risk/report/popup_report_controls_not_measured.xml' */

'rs_efficiency'=>'Efficacité:',

/* './packages/risk/report/popup_report_controls_without_risks.php' */

'rs_controls_without_risks'=>'Contrôles sans Risques Associés',

/* './packages/risk/report/popup_report_detailed_pendant_tasks.php' */

'rs_detailed_pending_tasks'=>'Tâches En Attente Détaillées',

/* './packages/risk/report/popup_report_duplicated_risks.php' */

'tt_duplicated_risks'=>'Duplicata de Risques',

/* './packages/risk/report/popup_report_duplicated_risks.xml' */

'rs_risk_cl'=>'Risque:',

/* './packages/risk/report/popup_report_duplicated_risks_by_asset.php' */

'tt_duplicated_risks_by_asset'=>'Duplicata de Risques par Actif',

/* './packages/risk/report/popup_report_duplicated_risks_by_asset.xml' */

'rs_risk'=>'Risque',

/* './packages/risk/report/popup_report_events_by_category.php' */

'rs_checklist_by_event'=>'Liste de Vérification par Actif',

/* './packages/risk/report/popup_report_events_by_category.xml' */

'rs_brackets'=>'[ ]',
'rs_event'=>'Événement',
'rs_event_category_cl'=>'Catégorie:',
'rs_na'=>'NA',
'rs_nc'=>'NC',
'rs_ok'=>'Ok',

/* './packages/risk/report/popup_report_events_by_type.php' */

'rs_events_by_type'=>'Événements par Type',

/* './packages/risk/report/popup_report_pendant_contexts.php' */

'rs_pending_items'=>'Objets en Attente',

/* './packages/risk/report/popup_report_pendant_contexts.xml' */

'rs_approver'=>'Approbateur',

/* './packages/risk/report/popup_report_places.xml' */


/* './packages/risk/report/popup_report_policy.php' */

'rs_sgsi_policy_statement'=>'Énoncé du Règlement de ERMS',

/* './packages/risk/report/popup_report_policy.xml' */

'rs_system_policy'=>'Règlement du Système',

/* './packages/risk/report/popup_report_process_by_process.xml' */


/* './packages/risk/report/popup_report_processes.php' */

'mx_processes'=>'Processus',

/* './packages/risk/report/popup_report_processes.xml' */

'rs_header_process_name'=>'Nom',
'rs_header_process_responsible'=>'Responsable',

/* './packages/risk/report/popup_report_processes_by_priority.php' */

'rs_processes_by_priority'=>'Processus par Priorité',

/* './packages/risk/report/popup_report_processes_by_priority.xml' */

'rs_priority_cl'=>'Priorité:',

/* './packages/risk/report/popup_report_processes_by_type.php' */

'rs_processes_by_type'=>'Processus par Type',

/* './packages/risk/report/popup_report_processes_without_assets.php' */

'rs_processes_without_assets'=>'Processus sans Actifs',

/* './packages/risk/report/popup_report_risk_amount_by_area.php' */

'rs_amount_of_risks_per_area'=>'Nombre des Risques par Domaine',

/* './packages/risk/report/popup_report_risk_amount_by_process.php' */

'rs_amount_of_risks_per_process'=>'Nombre de Risques par Processus',

/* './packages/risk/report/popup_report_risk_financial_impact.php' */

'mx_risk_financial_impact'=>'Impact Financier de Risque',

/* './packages/risk/report/popup_report_risk_financial_impact.xml' */

'rs_risk_impact'=>'Impact de Risque',
'rs_risk_name'=>'Nom',
'rs_risk_residual_value'=>'Risque Résiduel',
'rs_risk_value'=>'Potentiel de Risque',
'rs_total_impact'=>'Total',

/* './packages/risk/report/popup_report_risk_status_by_area.php' */

'rs_risk_status_per_area'=>'État de Risque par Domaine',

/* './packages/risk/report/popup_report_risk_status_by_area.xml' */

'rs_area'=>'Domaine',

/* './packages/risk/report/popup_report_risk_status_by_process.php' */

'rs_risk_status_per_process'=>'État des Risques par Processus',

/* './packages/risk/report/popup_report_risk_status_by_process.xml' */

'rs_process'=>'Processus',

/* './packages/risk/report/popup_report_risk_treatment_plan.php' */

'rs_and'=>'et',
'rs_avoided'=>'Évité',
'rs_not_treated'=>'Non Traité',
'rs_plan_of_risks_treatment'=>'Plan de Traitement de Risque',
'rs_reduced'=>'Réduction',
'rs_restrained'=>'Retenu',
'rs_transferred'=>'Transféré',

/* './packages/risk/report/popup_report_risk_treatment_plan.xml' */

'rs_asset_bl_cl'=>'<b>Actif:</b>',
'rs_deadline'=>'Date Limite de Délai',
'rs_status'=>'État',

/* './packages/risk/report/popup_report_risk_values.php' */

'rs_risks_values'=>'Valeurs de Risques',

/* './packages/risk/report/popup_report_risk_values.xml' */

'rs_affected_asset'=>'Actif Visé',

/* './packages/risk/report/popup_report_risks.php' */

'mx_risks'=>'Risques',

/* './packages/risk/report/popup_report_risks_by_area.php' */

'rs_risks_per_area'=>'Risques par Domaine',

/* './packages/risk/report/popup_report_risks_by_area_and_process_asset.xml' */

'rs_asset_cl'=>'Actif:',
'rs_process_cl'=>'Processus:',

/* './packages/risk/report/popup_report_risks_by_asset.php' */

'rs_risks_per_asset'=>'Risques par Actif',

/* './packages/risk/report/popup_report_risks_by_control.php' */

'rs_report_of_risk_per_control'=>'Risques par Contrôle',

/* './packages/risk/report/popup_report_risks_by_control.xml' */

'rs_control_bl_cl'=>'<b>Contrôle:</b>',

/* './packages/risk/report/popup_report_risks_by_process.php' */

'rs_risks_per_process'=>'Risques par Processus',

/* './packages/risk/report/popup_report_risks_by_type.php' */

'rs_risks_by_type'=>'Risques par Type',

/* './packages/risk/report/popup_report_risks_by_type.xml' */

'rs_type_cl'=>'Type:',

/* './packages/risk/report/popup_report_scope.php' */

'rs_sgsi_scope_statement'=>'Énoncé du Champ d\'Application de ERMS',

/* './packages/risk/report/popup_report_scope.xml' */

'rs_system_scope'=>'Champ d\'Application du Système',

/* './packages/risk/report/popup_report_statement_of_applicability.php' */

'rs_applied'=>'Appliqué',
'rs_no_risks_application_control'=>'Jusqu\'à présent',
'rs_not_applied'=>'Non Appliqué',
'rs_reduction_of_risks'=>'Réduction des Risques.',
'rs_statement_of_applicability'=>'Déclaration d\'Applicabilité',
'rs_statement_of_applicability_pre_report'=>'Pré-Rapport de Déclaration d\'Applicabilité',

/* './packages/risk/report/popup_report_statement_of_applicability.xml' */

'rs_applicability_cl'=>'Applicabilité:',
'rs_best_practice_cl'=>'Meilleure Pratique:',
'rs_control'=>'Contrôle',
'rs_document'=>'Document',
'rs_justification_cl'=>'Raison:',

/* './packages/risk/report/popup_report_statement_of_applicability_justificative.xml' */

'tt_justification'=>'Raison',
'vb_close'=>'Fermer',
'vb_confirm'=>'Confirmer',

/* './packages/risk/report/popup_report_summarized_pendant_tasks.php' */

'rs_summarized_pending_tasks'=>'Résumé de Tâches en Attente',

/* './packages/risk/report/popup_report_summarized_pendant_tasks.xml' */

'rs_amount'=>'Montant',
'rs_tasks'=>'Tâches',

/* './packages/risk/report/popup_report_tests.xml' */


/* './packages/risk/report/popup_report_top10_assets_with_more_risks.php' */

'rs_top_10_assets_with_highest_risks'=>'Top 10 Actifs avec le plus de Risque Élevés',
'rs_top_10_assets_with_most_risks'=>'Top 10 Actifs avec le plus de Risques',

/* './packages/risk/report/popup_report_top10_assets_with_more_risks.xml' */

'rs_amount_of_risks'=>'Nombre de Risques',
'rs_asset'=>'Actif',
'rs_responsible'=>'Responsable',

/* './packages/risk/report/popup_report_top10_risks_by_area.php' */

'rs_top_10_risks_per_area'=>'Top 10 Risques par Domaine',

/* './packages/risk/report/popup_report_top10_risks_by_area.xml' */

'rs_area_bl_cl'=>'<b>Domaine:</b>',

/* './packages/risk/report/popup_report_top10_risks_by_process.php' */

'rs_top_10_risks_per_process'=>'Top 10 Risques par Processus',

/* './packages/risk/report/popup_report_top10_risks_by_process.xml' */

'rs_process_bl_cl'=>'<b>Processus:</b>',
'rs_real_risk'=>'Risque Potentiel',
'rs_residual_risk'=>'Risque Résiduel',
'rs_risk_and_impact'=>'Risque / Impact',

/* './packages/risk/report/popup_report_user_responsabilities.php' */

'rs_security_resp'=>'Resp. de Sécu.',
'rs_special_user'=>'Utilisateurs Spéciaux',
'rs_users_responsibilities'=>'Responsabilités d\'Utilisateurs',

/* './packages/risk/report/popup_report_user_responsabilities.xml' */

'rs_responsible_bl_cl'=>'<b>Responsable:</b>',

/* './packages/risk/report/report_template.xml' */

'rs_comment_cl'=>'Commentaire:',
'rs_end_of_report'=>'Fin du Rapport',

/* './packages/risk/tab_risk_management.xml' */

'ti_area'=>'Domaine',
'ti_asset'=>'Actif',
'ti_controls'=>'Contrôles',
'ti_process'=>'Processus',
'ti_reports'=>'Rapports',
'ti_risk'=>'Risque',

/* './popup_choose_role.php' */

'mx_user_doesnt_have_permission_to_login'=>'L\'utilisateur<b>%user_name%</b> n\'a pas la permission d\'accéder au système.',
'tt_login_error'=>'Échec de la Connexion Système',

/* './popup_choose_role.xml' */

'lb_represent_bl_cl'=>'<b>Représenter:</b>',
'st_solicitor_explanation'=>'Si vous souhaitez représenter l\'un de ces utilisateurs',
'tt_solicitor_mode'=>'Mode Avocat',

/* './popup_email_preferences.xml' */

'cb_action_plan'=>'Plan d\'Action',
'cb_areas'=>'Domaines',
'cb_assets'=>'Actifs',
'cb_best_practices'=>'Meilleures Pratiques',
'cb_controls'=>'Contrôles',
'cb_documents'=>'Documents',
'cb_events'=>'Evènements',
'cb_incidents'=>'Incidents',
'cb_non_conformities'=>'Non-Conformités',
'cb_occurrences'=>'Occurrences',
'cb_others'=>'Autres',
'cb_processes'=>'Processus',
'cb_risks'=>'Risques',
'cb_select_all'=>'Tout Sélectionner',
'cb_standards'=>'Les Normes',
'lb_warning_types_to_receive_cl'=>'Sélectionnez les alertes que vous souhaitez recevoir:',
'rb_html_email'=>'E-mail Html',
'rb_messages_digest_complete'=>'Complet (un e-mail quotidien avec messages pleins)',
'rb_messages_digest_none'=>'Aucun (un e-mail pour chaque message)',
'rb_messages_digest_subjects'=>'Sujets (un e-mail quotidien avec des sujets de messages uniquement)',
'rb_text_email'=>'Email de Texte Seulement',
'st_email_preferences_bl'=>'<b>Préférences E-mail</b>',
'st_messages_digest_type_cl'=>'Type de Recueil de Messages:',

/* './popup_help.xml' */

'cb_do_not_show_again'=>'Ne plus afficher ceci',
'st_tip_text'=>'<b>Lors de l\'accès de tables Minimarisk',
'tt_tip_bl'=>'<b>Conseil</b>',

/* './popup_password_request.xml' */

'lb_user_login_cl'=>'Utilisateur:',
'st_password_request'=>'Pour obtenir un nouveau Mot de Passe',
'st_warning_general_email_disabled'=>'Le système e-mail est désactivé',
'tt_password_reminder'=>'Demande de nouveau Mot de Passe',
'vb_new_password'=>'Nouveau Mot de Passe',
'wn_password_sent'=>'Le Mot de Passe a été envoyé à l\'utilisateur',

/* './popup_risk_limits_task_view.xml' */

'lb_risk_limits_high_cl'=>'Tolérance au Risque 2:',
'lb_risk_limits_low_cl'=>'Tolérance au Risque 1:',
'tt_risk_limits_approval'=>'Approuver les Limites de Risque',

/* './popup_saas_disclaimer.xml' */

'tt_disclaimer_popup_title'=>'Clause de Non Responsabilité',
'vb_agree'=>'Je suis d\'accord',
'vb_disagree'=>'Je ne suis pas d\'accord',

/* './popup_schedule_edit.xml' */

'lb_april'=>'Avril',
'lb_august'=>'Août',
'lb_december'=>'Décembre',
'lb_february'=>'Février',
'lb_january'=>'Janvier',
'lb_july'=>'Juillet',
'lb_june'=>'Juin',
'lb_march'=>'Mars',
'lb_may'=>'Mai',
'lb_november'=>'Novembre',
'lb_october'=>'Octobre',
'lb_schedule_days_to_finish'=>'Jours pour accomplir tâche:',
'lb_schedule_end'=>'Fini le:',
'lb_schedule_periodicity'=>'Tous les',
'lb_schedule_start'=>'Commence le:',
'lb_schedule_type_cl'=>'Type:',
'lb_september'=>'Septembre',
'lb_week_days'=>'Au cours des jours suivants:',
'mx_first_week'=>'première semaine',
'mx_fourth_week'=>'quatrième semaine',
'mx_last_week'=>'dernière',
'mx_monday'=>'Lundi',
'mx_saturday'=>'Samedi',
'mx_second_week'=>'deuxième',
'mx_sunday'=>'Dimanche',
'mx_third_week'=>'troisième semaine',
'mx_thursday'=>'Jeudi',
'mx_tuesday'=>'Mardi',
'mx_wednesday'=>'Mercredi',
'si_schedule_by_day'=>'quotidien',
'si_schedule_by_month'=>'Mensuel',
'si_schedule_by_week'=>'Hebdomadaire',
'si_schedule_month_day'=>'Mois jour',
'si_schedule_week_day'=>'Jour de semaine',
'st_months'=>'mois',
'st_never'=>'jamais',
'st_schedule_months'=>'Les mois suivants:',
'st_weeks'=>'semaines',
'tt_schedule_edit'=>'Etablir l\'Horaire de Tâche',
'tx_1'=>'1',
'tx_schedule_days_to_finish'=>'1',

/* './popup_solicitor_conflict.xml' */

'st_solicitor_conflict'=>'L\'utilisateur<b>%solicitor_name%</b> a été nommé par vous pour vous représenter lors de votre absence. Souhaitez-vous mettre fin à ce lien?',
'tt_solicitor_conflict'=>'Conflit d\'Avocat',

/* './popup_survey.xml' */

'st_survey_text'=>'"Après l\'évaluation du système',
'tt_survey_bl'=>'<b>Sondage</b>',

/* './popup_unexpected_behavior.xml' */

'st_unexpected_behavior_message'=>'Un comportement inattendu dans le système a été détecté. Un e-mail avec des informations détaillées sur ce qui s\'est passé a été envoyé à notre équipe de Support Technique. Si nécessaire',
'tt_unexpected_behavior'=>'Comportement Inattendu',

/* './popup_user_change_password.php' */

'st_user_password_change'=>'Vous devez modifier votre Mot de Passe maintenant.',
'st_user_password_expired'=>'<b>Votre Mot de Passe a expiré!</b><br/>Vous devez le changer maintenant.',
'st_user_password_will_expire'=>'Votre Mot de Passe expire dans %days% jour(s). Si vous le souhaitez',

/* './popup_user_change_password.xml' */

'lb_confirm_cl'=>'Confirmer:',
'tt_change_password'=>'Changer Mot de Passe',
'vb_change_password'=>'Changer Mot de Passe',
'wn_unmatching_password'=>'Les mots de passe ne correspondent pas.',

/* './popup_user_dashboard_preference.php' */

'tt_average_time_for_documents_approval'=>'Délai d\'Approbation Moyen des Documents',
'tt_average_time_for_my_documents_approval'=>'Temps d\'Approbation Moyen de Mes documents',
'tt_financial_summary'=>'Résumé Financier',
'tt_general_summary_of_risks'=>'Risque Estimé vs Non-Estimé',
'tt_grid_abnormality_ci_title'=>'Résumé des Anomalies de l\'Amélioration Continue',
'tt_grid_abnormality_pm_title'=>'Résumé des Anomalies de la Gestion des Règlements',
'tt_grid_incident_summary_title'=>'Vue d\'Ensemble des Incidents',
'tt_grid_incidents_per_asset_title'=>'Top 5 Actifs avec le plus d\'Incidents',
'tt_grid_incidents_per_process_title'=>'Top 5 des Processus avec le plus d\'Incidents',
'tt_grid_incidents_per_user_title'=>'Top 5 Utilisateurs avec le plus de Processus Disciplinaires',
'tt_grid_nc_per_process_title'=>'Top 5 des Processus avec le plus de Non-Conformités',
'tt_grid_nc_summary_title'=>'Résumé des Non-Conformités',
'tt_my_items'=>'Mes Objets',
'tt_my_pendencies'=>'Mes En Attente de la Gestion des Risques',
'tt_my_risk_summary'=>'Mon Résumé de Risque',
'tt_risk_management_my_items_x_policy_management'=>'Gestion des Risques (Mes Objets) x Gestion des Règlements',
'tt_risk_management_status'=>'Niveau de Risque',
'tt_risk_management_x_policy_management'=>'Documents par Élément d\'Activité',
'tt_sgsi_reports_and_documents'=>'ISO 31000 Documents ERMS Obligatoires',
'tt_summary_of_abnormalities'=>'Résumé des Anomalies de Gestion de Risque',
'tt_summary_of_best_practices'=>'Meilleures Pratiques - Index de Conformité',
'tt_summary_of_controls'=>'Vue d\'Ensemble des Contrôles',
'tt_summary_of_my_documents_last_revision_average_time'=>'Résumé de Période de la Dernière Révision des Documents',
'tt_summary_of_parameterized_risks'=>'Risque Potentiel vs Résiduel',
'tt_summary_of_policy_management'=>'Vue d\'Ensemble de la Gestion des Règlements',
'tt_summary_of_policy_management_my_documents'=>'Résumé de la Gestion des Règlements (Mes Documents)',
'tt_summary_of_risk_management_documentation'=>'Résumé de la Documentation de la Gestion des Risques',
'tt_summary_of_the_risk_management_from_my_items_documentation'=>'Résumé de la Documentation de la Gestion des Risques pour Mes Objets',
'tt_top_10_most_read_documents'=>'Top 10 des Documents les plus lus',
'tt_top_10_most_read_my_documents'=>'Top 10 de Mes Documents Les Plus Lus',

/* './popup_user_dashboard_preference.xml' */

'st_all_elements_not_selected'=>'Tous les objets (non sélectionnés)',
'st_selected_elements_cl'=>'Objets sélectionnés:',
'tt_summary_preferences_editing'=>'Résumé de Modification de Préférences',

/* './popup_user_preferences.php' */

'si_email'=>'Email',
'si_general'=>'Général',

/* './popup_user_preferences.xml' */

'lb_preferences_cl'=>'Préférences:',
'tt_edit_preferences'=>'Modifier les Préférences',

/* './popup_user_profile.xml' */

'cb_change_password'=>'Changer le Mot de Passe',
'lb_current_password_cl'=>'Mot de Passe actuel:',
'lb_dashboard_preferences_cl'=>'Préférences de Tableau de bord:',
'lb_general_config_bl'=>'<b>Paramètres Généraux</b>',
'lb_general_config_bl_cl'=>'<b>Configuration Générale:</b>',
'lb_language_cl'=>'Langue:',
'lb_password_confirm_cl'=>'Confirmation du Mot de Passe:',
'lb_solicitor_cl'=>'Avocat:',
'lb_solicitor_finish_date_cl'=>'Fin d\'Acquisition:',
'lb_solicitor_start_date_cl'=>'Début d\'Acquisition:',
'st_change_password_sub_title_bl'=>'<b>Changement de Mot de Passe</b>',
'st_solicitor_sub_title_bl'=>'<b>Avocat</b>',
'to_dashboard_user_preferences_help'=>'<b>Configuration des préférences du tableau de bord:</b><br/><br/>Détermine l\'ordre et les grilles d\'information qui seront affichées dans votre tableau de bord.',
'wn_email_incorrect'=>'E-mail Invalide.',
'wn_existing_password'=>'Le Mot de Passe est dans la récente histoire de la table des mots de passe',
'wn_initial_date_after_today'=>'La date de début doit être la même ou postérieur à la date actuelle.',
'wn_missing_dates'=>'Les champs de date sont obligatoires lorsqu\'un avocat est sélectionné.',
'wn_missing_solicitor'=>'Il est obligatoire de choisir un avocat lorsque l\'une des dates a été remplie.',
'wn_past_date'=>'La date finale doit être postérieure ou égale à la date actuelle.',
'wn_wrong_dates'=>'La date finale d\'acquisition doit être plus tard que sa date de début.',

/* './popup_user_search.xml' */

'tt_user_search'=>'Recherche d\'Utilisateur',
'wn_no_user_selected'=>'Il n\'y a pas d\'utilisateur sélectionné.',

/* './reports/ISMSReportTemplate.php' */

'rs_ip_cl'=>'IP:',
'rs_manager_cl'=>'Responsable:',

/* './session_expired.xml' */

'st_session_expired'=>'Votre session a expiré. Pressez<b>Ok</b> ou fermez cette fenêtre pour vous connecter à nouveau.',
'tt_session_expired'=>'Session Expirée',

/* './tab_dashboard.xml' */

'ti_graphics'=>'Graphiques',
'ti_statistics'=>'Statistiques',
'ti_summary'=>'Résumés',
'ti_warnings'=>'Mes Tâches et Alertes',

/* './tab_main.xml' */

'st_search'=>'Recherche',
'ti_continual_improvement'=>'Amélioration continue',
'ti_dashboard'=>'Tableau de bord',
'ti_libraries'=>'Bibliothèques',
'ti_policy_management'=>'Gestion des Règlements',
'ti_risk_management'=>'Gestion des risques',
'ti_search'=>'Recherche',
'vb_admin'=>'Admin',
'vb_go'=>'Aller',

/* './visualize.php' */

'tt_visualize'=>'Voir',
);
?>