<?php
$gaStrings = array(

/* './about_isms.xml' */

'st_about_axur_full'=>'Realiso Corp. is an on-demand management systems leading company. We have hundreds of clients present in several market sectors, including financial, telecom, industry, government, retail, energy, mining, dot-com, services and oil & gas. We use worldwide requirements and provide you with high technology solutions to reduce organizational risk, measure and demonstrate controls efficiency regarding the organization\'s information assets protection.',
'st_about_axur_title'=>'About Realiso Corp.',
'st_about_isms_full'=>'Real PCI – Payment Card Industry Management System is a suite for managing controls according to PCI. Real PCI helps corporations of any size to understand their compliance level with the regulations.',
'st_about_isms_title'=>'About Real PCI',
'st_axur_information_security'=>'Realiso Corp.',
'st_axur_link'=>'www.realiso.com',
'st_build'=>'Build',
'st_copyright'=>'Copyright',
'st_information_security_management_system'=>'Information Security Management System',
'st_version'=>'Version',

/* './access_denied.php' */

'st_license_file_not_found'=>'License file <b>%license_name%</b> not found.',

/* './access_denied.xml' */

'tt_access_denied'=>'Access Denied',

/* './classes/admin/report/ISMSReportAuditAlertFilter.php' */

'rs_alerts_created_after'=>'Alerts created after',
'rs_alerts_created_before'=>'Alerts created before',

/* './classes/admin/report/ISMSReportAuditLogFilter.php' */

'rs_action_cl'=>'Action:',
'rs_log_after'=>'Log after',
'rs_log_before'=>'Log before',

/* './classes/admin/report/ISMSReportAuditTaskFilter.php' */

'rs_activity_cl'=>'Activity:',
'rs_and_before'=>'and before',
'rs_any_date'=>'Any date',
'rs_creation_date_cl'=>'Creation date:',
'rs_receiver_cl'=>'Receiver:',
'rs_tasks_created_after'=>'Tasks created after',
'rs_tasks_created_before'=>'Tasks created before',
'st_all_users'=>'All users',

/* './classes/continual_improvement/CIActionPlan.php' */

'mx_action_plan'=>'Action Plan',
'mx_ap_remove_error_message'=>'It is not possible to remove the Action Plan. There is at least one Nonconformity associated to this item.',
'mx_ap_remove_error_title'=>'Error removing Action Plan',

/* './classes/continual_improvement/CICategory.php' */

'mx_root'=>'Root',

/* './classes/continual_improvement/CIIncident.php' */

'mx_incident'=>'Incident',

/* './classes/continual_improvement/CIIncidentControl.php' */

'mx_incident_control_association'=>'Incident to Control association',

/* './classes/continual_improvement/CIIncidentProcess.php' */

'mx_incident_process_association'=>'Incident to Process association',

/* './classes/continual_improvement/CINonConformity.php' */

'mx_non_conformity'=>'Nonconformity',

/* './classes/continual_improvement/CINonConformitySeed.php' */

'mx_inefficient'=>'Inefficient',
'mx_late_efficiency_revision'=>'Efficiency Revision Delayed',
'mx_late_implementation'=>'Late Implementation',
'mx_late_test'=>'Delayed Test',
'mx_test_failure'=>'Test Failure',

/* './classes/continual_improvement/CIOccurrence.php' */

'mx_occurrence'=>'Occurrence',
'mx_occurrence_remove_error'=>'Error removing Occurrence',
'mx_occurrence_remove_error_message'=>'It is not possible to remove this Occurrence. There is at least one Incident associated to this item.',
'st_incident_module_name'=>'Continual Improvement',

/* './classes/continual_improvement/CISolution.php' */

'mx_solution'=>'Solution',

/* './classes/continual_improvement/report/ISMSReportDisciplinaryProcessFilter.php' */

'rs_user_cl'=>'User:',

/* './classes/continual_improvement/report/ISMSReportElementsFilter.php' */

'lb_elements_cl'=>'Items:',

/* './classes/continual_improvement/report/ISMSReportIncidentFilter.php' */

'lb_solution_date_cl'=>'Solution Date:',
'mx_immediate_disposal'=>'Immediate Disposal',

/* './classes/continual_improvement/report/ISMSReportNonConformityFilter.php' */

'lb_conclusion_date_cl'=>'Finish Date:',
'mx_after_initialdate'=>'After %initialDate%',
'mx_before_finaldate'=>'Before %finalDate%',
'mx_between_initialdate_finaldate'=>'Between %initialDate% and %finalDate%',
'mx_external_audit'=>'External Audit',
'mx_internal_audit'=>'Internal Audit',
'mx_no'=>'No',
'mx_security_control'=>'Security Control',
'mx_yes'=>'Yes',

/* './classes/continual_improvement/report/ISMSReportOccurrenceFilter.php' */

'mx_all'=>'All',

/* './classes/ISMSActivity.php' */

'wk_action_plan_approval'=>'Action Plan Approval',
'wk_action_plan_finish_confirm'=>'Approve Estimated Finish of Action Plan',
'wk_action_plan_revision'=>'Action Plan Follow Up',
'wk_area_approval'=>'Approve Area',
'wk_asset_approval'=>'Approve Asset',
'wk_best_practice_approval'=>'Approve Requirement',
'wk_best_practice_section_approval'=>'Approve Section of Requirement',
'wk_category_approval'=>'Approve Category',
'wk_control_approval'=>'Approve Control',
'wk_control_implementation_approval'=>'Approve Control Implementation',
'wk_control_test_followup'=>'Monitor Control Test',
'wk_document_approval'=>'Approve Document',
'wk_document_revision_followup'=>'Monitor Document Revision',
'wk_document_template_approval'=>'Approve Document Template',
'wk_event_approval'=>'Approve Event',
'wk_incident_approval'=>'Approve Incident',
'wk_incident_control_induction'=>'Control Eficiency Induced Measuring',
'wk_incident_disposal_approval'=>'Incident Immediate Disposal Approval',
'wk_incident_solution_approval'=>'Incident Solution Approval',
'wk_non_conformity_approval'=>'Nonconformity Approval',
'wk_non_conformity_data_approval'=>'Nonconformity Data Approval',
'wk_occurrence_approval'=>'Occurrence Approval',
'wk_policy_approval'=>'Approve Policy',
'wk_process_approval'=>'Approve Process',
'wk_process_asset_association_approval'=>'Approve Asset Association with Process.',
'wk_real_efficiency_followup'=>'Monitor Real Efficiency',
'wk_risk_acceptance_approval'=>'Approve Acceptance of Risks above value',
'wk_risk_acceptance_criteria_approval'=>'Approve definition of criteria for Risks Acceptance',
'wk_risk_approval'=>'Approve Risk',
'wk_risk_control_association_approval'=>'Approve Control Association with Risk.',
'wk_risk_tolerance_approval'=>'Approve Risk Tolerance',
'wk_scope_approval'=>'Approve Scope',
'wk_standard_approval'=>'Approve Standard',

/* './classes/ISMSAuditLog.php' */

'ad_action_bad_login'=>'has tried to log in with an invalid password',
'ad_action_blocked_login'=>'is blocked and tried to log in',
'ad_login'=>'Logged In System',
'ad_login_error'=>'has tried to log in with an invalid username',
'ad_login_failed'=>'has tried to log in without permission',
'ad_logout'=>'Logged Off System',
'ad_removed_from_system'=>'Removed from the system',
'ad_restored'=>'restored',

/* './classes/ISMSContext.php' */

'ad_log_message'=>'%action% %label% <b>%context_name%</b> by %user_name%.',
'ad_on_behalf_of'=>'on behalf of',
'db_default_document_name'=>'%label_context% %context_name% %label_doc%',
'st_denied_permission_to_approve'=>'You do not have permission to approve the',
'st_denied_permission_to_delete'=>'You do not have permission to remove the',
'st_denied_permission_to_execute'=>'You do not have permission to execute the task.',
'st_denied_permission_to_insert'=>'You do not have permission to insert a(n)',
'st_denied_permission_to_read'=>'You do not have permission to view the',

/* './classes/ISMSContextObject.php' */

'mx_ap_acceptance_pendant'=>'Action Plan Acceptance Pending',
'mx_approval'=>'In Approval',
'mx_approved'=>'Approved',
'mx_closed'=>'Closed',
'mx_co_approved'=>'Approved by Both',
'mx_deleted'=>'Removed',
'mx_denied'=>'Denied',
'mx_directed'=>'Forwarded',
'mx_finished'=>'Finished',
'mx_measured'=>'Measured',
'mx_nc_treatment_pendant'=>'Nonconformity Treatment Pending',
'mx_obsolete'=>'Obsolete',
'mx_open'=>'Open',
'mx_pendant'=>'Pending',
'mx_pendant_solution'=>'Pending - Solution',
'mx_pendant_solved'=>'Pending - Solved',
'mx_published'=>'Published',
'mx_revision'=>'In Revision',
'mx_sent'=>'Issued',
'mx_solved'=>'Solved',
'mx_to_be_published'=>'To be Published',
'mx_under_development'=>'In development',
'mx_waiting_conclusion'=>'Awaiting Finish',
'mx_waiting_deadline'=>'Estimated',
'mx_waiting_measurement'=>'Waiting Measurement',
'mx_waiting_solution'=>'Waiting Solution',

/* './classes/ISMSEmailTemplates.php' */

'em_activity'=>'Activity',
'em_alert_messages_sent'=>'<b>Dear Real PCI user,<b> <br> The following alert messages were sent to you.<br>',
'em_asset'=>'Asset',
'em_at'=>'at',
'em_click_here_to_access_the_system'=>'Access Real PCI',
'em_client'=>'Client',
'em_control'=>'Control',
'em_control_has_deadline'=>'<b>Dear Real PCI user,<b> <br> the Control below has a deadline for implementation.',
'em_created_on'=>'Created on',
'em_description'=>'Description',
'em_disciplinary_process'=>'<b>Dear Disciplinary Process Manager, </b><br>The  following system user(s) was/were added to a disciplinary process.',
'em_execute_task'=>'<b>Dear Real PCI user,<b> <br>we ask you to execute the activity(ies) below.<br>',
'em_feedback'=>'Feedback',
'em_feedback_type'=>'Feedback Type',
'em_incident'=>'Incident',
'em_incident_risk_parametrization_message'=>'<b>Dear Real PCI user,</b><br>the following risk was associated to an asset of your responsability.',
'em_isms_feedback_msg'=>'A user wrote a feedback about Real PCI.',
'em_isms_new_user_msg'=>'You were added to Real PCI. Below you can see the data for system access:',
'em_limit'=>'Limit',
'em_login'=>'Login',
'em_new_parameters'=>'New Parameters',
'em_new_password_generated'=>'<b>Dear Real PCI user,<b> <br> a new password has been created for you.<br> Your new password is',
'em_new_password_request_ignore'=>'If you haven\'t asked for a new password, please ignore this e-mail. Your previous credentials are still valid and, if you use them, this new password will be discarded.',
'em_observation'=>'Observation',
'em_original_parameters'=>'Default Parameters',
'em_password'=>'Password',
'em_risk'=>'Risk',
'em_saas_abandoned_system_msg'=>'<b>The Real PCI of the client below was abandoned.</b>',
'em_sender'=>'Sender',
'em_sent_on'=>'Sent on',
'em_tasks_and_alerts_transferred'=>'<b>Dear Real PCI user,<b> <br> A system user was removed, and his tasks and alerts have been transferred to you.',
'em_tasks_and_alerts_transferred_message'=>'The user above has been removed from the system, and his tasks and alerts have been transferred to your name.',
'em_user'=>'User',
'em_users'=>'Users',
'link'=>'Link',

/* './classes/ISMSLib.php' */

'em_license_expired'=>'The License of Client <b>%client_name%</b> expired on <b>%expiracy%</b>',
'em_subject_license_expired'=>'License Expired',
'mx_asset_manager'=>'Asset Owner',
'mx_chairman'=>'Management',
'mx_chinese'=>'Chinese (Beta)',
'mx_control_manager'=>'Control Owner',
'mx_created_by'=>'Created by:',
'mx_day'=>'day',
'mx_days'=>'days',
'mx_english'=>'English',
'mx_evidence_manager'=>'Evidence Manager',
'mx_french'=>'Français',
'mx_german'=>'Deutsch',
'mx_incident_manager'=>'Incident Manager',
'mx_library_manager'=>'Library Owner',
'mx_modified_by'=>'Modified by:',
'mx_month'=>'month',
'mx_months'=>'months',
'mx_non_conformity_manager'=>'Nonconformity Management',
'mx_on'=>'on',
'mx_portuguese'=>'Portuguese',
'mx_process_manager'=>'Disciplinary Process Management',
'mx_shortdate_format'=>'%m/%d/%Y',
'mx_spanish'=>'Español (Beta!)',
'mx_week'=>'week',
'mx_weeks'=>'weeks',
'st_denied_permission_to_access_incident_module'=>'You don´t have permission to access Continual Improvement Workbench. <br><br> This license doesn´t have this permission!',
'st_denied_permission_to_access_policy_module'=>'You don\'t have permission to access Policy Management module. <br><br>you are using a license that doesn\'t allow you to access this module!',

/* './classes/ISMSLicense.php' */

'mx_commercial'=>'Commercial',
'mx_trial'=>'Trial',

/* './classes/ISMSMailer.php' */

'em_to_execute_task_check_activity_list'=>'<b>To execute this Task, access the system and verify your Activity List. </b><br> For further information about this task, please contact the security officer.',

/* './classes/ISMSPasswordPolicy.php' */

'mx_minimum_chars'=>'Minimum length',
'mx_minimum_numeric_chars'=>'Minimum number of numeric characters',
'mx_minimum_special_chars'=>'Minimum number of special characters',
'mx_password_policy'=>'<b>Password Policy:</b><br/>',
'mx_upper_and_lower_case_chars'=>'Upper case and lower case characters',

/* './classes/ISMSPolicy.php' */

'mx_policy'=>'Policy',

/* './classes/ISMSProfile.php' */

'mx_profile'=>'Profile',
'st_profile_remove_error'=>'Not possible to remove profile <b>%profile_name%</b>: There are users associated with this profile.',
'tt_profile_remove_error'=>'Error when removing Profile',

/* './classes/ISMSScope.php' */

'mx_scope'=>'Scope',

/* './classes/ISMSSession.php' */

'ad_login_message'=>'%label_user% <b>%user_name%</b> %action%.',

/* './classes/ISMSUser.php' */

'mx_user'=>'User',
'st_admim_module_name'=>'Admin',
'st_user_remove_error'=>'Not possible to remove user <b>%user_name%</b>: The user is responsible for one or more items of the system or is a special user.',
'tt_user_remove_error'=>'Error when removing User',

/* './classes/nonauto/admin/ISMSDefaultConfig.php' */

'db_area_priority_high'=>'3 - High',
'db_area_priority_low'=>'1 - Low',
'db_area_priority_middle'=>'2 - Medium',
'db_area_type_factory_unit'=>'Factory',
'db_area_type_filial'=>'Subsidiary',
'db_area_type_matrix'=>'Headquarter',
'db_area_type_regional_office'=>'Regional Office',
'db_control_cost_hardware'=>'Hardware',
'db_control_cost_material'=>'Material',
'db_control_cost_others'=>'Education',
'db_control_cost_pessoal'=>'People',
'db_control_cost_software'=>'Software',
'db_control_type_based_on_risk_analize'=>'Based on Risk Analysis',
'db_control_type_laws_and_regulamentation'=>'Compliance Issue',
'db_control_type_others'=>'Others',
'db_document_type_1'=>'Confidential',
'db_document_type_2'=>'Restrict',
'db_document_type_3'=>'Institutional',
'db_document_type_4'=>'Public',
'db_impact_1'=>'Very Low',
'db_impact_2'=>'Low',
'db_impact_3'=>'Medium',
'db_impact_4'=>'High',
'db_impact_5'=>'Very High',
'db_impact3_1'=>'Low',
'db_impact3_2'=>'Medium',
'db_impact3_3'=>'High',
'db_importance_1'=>'Negligible',
'db_importance_2'=>'Low',
'db_importance_3'=>'Medium',
'db_importance_4'=>'High',
'db_importance_5'=>'Critical',
'db_importance3_1'=>'Low',
'db_importance3_2'=>'Medium',
'db_importance3_3'=>'High',
'db_incident_cost_aplicable_penalty'=>'Fine',
'db_incident_cost_direct_lost'=>'Direct Loss',
'db_incident_cost_enviroment'=>'Environment Recovery',
'db_incident_cost_indirect_lost'=>'Indirect Loss',
'db_process_priority_high'=>'3 - High',
'db_process_priority_low'=>'1 - Low',
'db_process_priority_middle'=>'2 - Medium',
'db_process_type_4'=>'Human resources',
'db_process_type_5'=>'Information technology',
'db_process_type_6'=>'Finance & accounting',
'db_process_type_7'=>'Environment, health & safety',
'db_process_type_8'=>'Performance improvement',
'db_process_type_administrative'=>'Market & customers',
'db_process_type_operational'=>'New product development',
'db_process_type_support'=>'Production & delivery',
'db_rcimpact_0'=>'Does not affect',
'db_rcimpact_1'=>'One Level Decrease',
'db_rcimpact_2'=>'Two Level Decrease',
'db_rcimpact_3'=>'Three Level Decrease',
'db_rcimpact_4'=>'Four Level Decrease',
'db_rcimpact3_0'=>'Does not affect',
'db_rcimpact3_1'=>'One Level Decrease',
'db_rcimpact3_2'=>'Two Level Decrease',
'db_rcprob_0'=>'Does not affect',
'db_rcprob_1'=>'One Level Decrease',
'db_rcprob_2'=>'Two Level Decrease',
'db_rcprob_3'=>'Three Level Decrease',
'db_rcprob_4'=>'Four Level Decrease',
'db_rcprob3_0'=>'Does not affect',
'db_rcprob3_1'=>'One Level Decrease',
'db_rcprob3_2'=>'Two Level Decrease',
'db_register_type_1'=>'Confidential',
'db_register_type_2'=>'Restrict',
'db_register_type_3'=>'Institutional',
'db_register_type_4'=>'Public',
'db_risk_confidenciality'=>'Confidentiality',
'db_risk_disponibility'=>'Availability',
'db_risk_intregrity'=>'Integrity',
'db_risk_type_administrative'=>'Administrative',
'db_risk_type_fisic'=>'Physical',
'db_risk_type_tecnologic'=>'Technological',
'db_risk_type_threat'=>'Threat',
'db_risk_type_vulnerability'=>'Vulnerability',
'db_risk_type_vulnerability_x_threat'=>'Event (Vulnerability x Threat)',
'db_riskprob_1'=>'Rare (1% - 20%)',
'db_riskprob_2'=>'Unlikely (21% - 40%)',
'db_riskprob_3'=>'Moderate (41% - 60%)',
'db_riskprob_4'=>'Likely (61% - 80%)',
'db_riskprob_5'=>'Almost certain (81% - 99%)',
'db_riskprob3_1'=>'Unlikely',
'db_riskprob3_2'=>'Moderate',
'db_riskprob3_3'=>'Likely',

/* './classes/nonauto/FeedbackEvent.php' */

'em_isms_feedback'=>'Real PCI Feedback',

/* './classes/nonauto/policy_management/DocumentsDrawGrid.php' */

'to_future_publication'=>'The document will only be avaliable to readers on date %date%',

/* './classes/policy_management/PMDocInstance.php' */

'mx_document_instance'=>'Document Instance',

/* './classes/policy_management/PMDocument.php' */

'mx_document'=>'Document',
'st_denied_permission_to_manage_document'=>'You do not have permission to manage this document.',
'st_document_remove_error'=>'Not possible to remove document <b>%name%</b>. The Document contains sub-documents. To remove documents that contain sub-documents, activate the option "Delete Cascade" in  "Admin -> Configuration".',
'st_policy_management'=>'Policy Management',
'tt_document_remove_error'=>'Error when removing document.',

/* './classes/policy_management/PMRegister.php' */

'mx_day_days'=>'day(s)',
'mx_month_months'=>'Month(s)',
'mx_register'=>'Record',
'mx_week_weeks'=>'week(s)',

/* './classes/policy_management/PMTemplate.php' */

'mx_document_template'=>'Document Template',

/* './classes/policy_management/report/ISMSReportAccessedDocumentsFilter.php' */

'rs_users'=>'Users',

/* './classes/policy_management/report/ISMSReportDocsWithHighFrequencyRevisionFilter.php' */

'report_filter_all_userid'=>'all',
'report_filter_high_frequency_revision_definition'=>'High frequency of revision is:',
'report_filter_high_frequency_revision_definition_explanation'=>'more than one revision in a one-week period.',

/* './classes/policy_management/report/ISMSReportDocumentAccessesFilter.php' */

'rs_accesses'=>'Accesses',
'rs_only_last'=>'Only the last one',
'rs_show_all'=>'Show all',

/* './classes/policy_management/report/ISMSReportDocumentDatesFilter.php' */

'rs_after_initialdate'=>'After %initialDate%',
'rs_before_finaldate'=>'Before %finalDate%',
'rs_between_initialfinaldate'=>'Between %initialDate% and %finalDate%',
'rs_creation_date'=>'Creation date',
'rs_last_revision_date'=>'Last revision date',

/* './classes/policy_management/report/ISMSReportDocumentsByComponentFilter.php' */

'report_filter_all_doctype'=>'All',
'report_filter_doctype_cl'=>'Document Type:',

/* './classes/policy_management/report/ISMSReportDocumentsByStateFilter.php' */

'report_filter_showregisterdocs_cl'=>'Show Record Documents:',
'report_filter_status_cl'=>'Documents\' Status',
'rs_all_status'=>'All',

/* './classes/policy_management/report/ISMSReportDocumentSummary.php' */

'rs_approvers_list_section'=>'Approvers List',
'rs_author'=>'Author',
'rs_components_list_section'=>'Components List',
'rs_document_type_management'=>'Management',
'rs_documents_type_others'=>'Others',
'rs_file'=>'File',
'rs_general_info_section'=>'General Information',
'rs_keywords'=>'Keywords',
'rs_link'=>'Link',
'rs_no_approvers_message'=>'This document doesn\'t have any approvers.',
'rs_no_components_message'=>'This document doesn\'t have any components.',
'rs_no_readers_message'=>'This document doesn\'t have any readers.',
'rs_no_references_message'=>'This document doesn\'t have any references.',
'rs_no_registers_message'=>'This document doesn\'t have any records.',
'rs_no_subdocs_message'=>'This document doesn\'t have any subdocuments.',
'rs_published_date'=>'Publish in',
'rs_readers_list_section'=>'Readers List',
'rs_references_list_section'=>'References List',
'rs_registers_list_section'=>'Records List',
'rs_report_not_supported'=>'REPORT NOT SUPPORTED IN THIS FORMAT',
'rs_subdocs_list_section'=>'Subdocuments List',

/* './classes/policy_management/report/ISMSReportDocumentsWithoutRegisterFilter.php' */

'report_filter_all_document_responsible'=>'all',
'report_filter_document_responsible_cl'=>'Responsible for Document:',

/* './classes/policy_management/report/ISMSReportNotAccessedDocumentsFilter.php' */

'rs_expand_readers'=>'Expand readers',

/* './classes/policy_management/report/ISMSReportTopRevisedDocumentsFilter.php' */

'rs_expand_documents'=>'Expand documents',
'rs_show_only_first_cl'=>'Show only the first:',

/* './classes/policy_management/report/ISMSReportUserCommentsFilter.php' */

'rs_all_documents'=>'all',
'rs_documents_cl'=>'Documents:',

/* './classes/policy_management/report/ISMSReportUsersByProcessFilter.php' */

'rs_no'=>'No',
'rs_organize_by_area'=>'Organize by area',
'rs_yes'=>'Yes',

/* './classes/policy_management/report/ISMSReportUsersWithPendantReadFilter.php' */

'report_filter_all_document_reader_cl'=>'all',
'report_filter_document_reader_cl'=>'Documents Readers',

/* './classes/risk_management/report/ISMSReportBestPracticesFilter.php' */

'rs_only_applied'=>'Only Applied',

/* './classes/risk_management/report/ISMSReportConformityFilter.php' */

'rs_all'=>'All',
'rs_standard_filter_cl'=>'Standard Filter:',

/* './classes/risk_management/report/ISMSReportControlCostByCostCategoryFilter.php' */

'rs_costcategory_cl'=>'Category:',

/* './classes/risk_management/report/ISMSReportControlPlanningFilter.php' */

'rs_control_types_cl'=>'Control Types:',

/* './classes/risk_management/report/ISMSReportControlsFilter.php' */

'rs_delayed'=>'Delayed',
'rs_implementationstatus_cl'=>'Show Only:',
'rs_not_trusted_controls'=>'Unreliable controls',
'rs_trusted_and_efficient_controls'=>'Realiable and efficient controls',
'rs_under_implementation'=>'Under implementation',

/* './classes/risk_management/report/ISMSReportEventsByCategoryFilter.php' */

'rs_category_cl'=>'Category:',
'rs_event_types_cl'=>'Event Types:',

/* './classes/risk_management/report/ISMSReportGenericClassifTypePrioFilter.php' */

'lb_event_types_cl'=>'Event Types:',

/* './classes/risk_management/report/ISMSReportParametersFilter.php' */

'rs_considered_parameters_cl'=>'Considered Parameters:',

/* './classes/risk_management/report/ISMSReportProcessesByPriority.php' */

'rs_undefined_priority'=>'Undefined Priority',

/* './classes/risk_management/report/ISMSReportRiskFinancialImpactFilter.php' */

'mx_accepted'=>'Accepted',
'mx_avoided'=>'Avoided',
'mx_not_treated'=>'Not Treated',
'mx_reduced'=>'Reduced',
'mx_transferred'=>'Transferred',
'mx_treated'=>'Treated',
'rs_not_treated_or_accepted'=>'Not treated or accepted',
'rs_treatment_cl'=>'Treatment:',

/* './classes/risk_management/report/ISMSReportRiskImpactFilter.php' */

'lb_considered_parameters_cl'=>'Considered Parameters:',
'lb_risk_types_cl'=>'Risk Types:',

/* './classes/risk_management/report/ISMSReportRisksByAreaFilter.php' */

'lb_area_priorities_cl'=>'Area Priorities:',
'lb_area_types_cl'=>'Area Types:',
'rs_area_cl'=>'Area:',

/* './classes/risk_management/report/ISMSReportRisksByProcessFilter.php' */

'lb_process_priorities_cl'=>'Process Priorities:',
'lb_process_types_cl'=>'Process Types:',
'st_all'=>'All',

/* './classes/risk_management/report/ISMSReportRisksByType.php' */

'rs_undefined_type'=>'Undefined Type',

/* './classes/risk_management/report/ISMSReportRisksFilter.php' */

'rs_estimated_risks'=>'estimated risks',
'rs_not_estimated'=>'Not Estimated',
'rs_not_estimated_risks'=>'non-estimated risks',
'rs_not_treated_risks'=>'non-treated risks',
'rs_residual_value_cl'=>'Residual Risk:',
'rs_show_only_cl'=>'Show Only:',
'rs_treated_risks'=>'treated risks',
'rs_value_cl'=>'Potential Risk:',

/* './classes/risk_management/report/ISMSReportRiskValuesFilter.php' */

'rs_high'=>'High',
'rs_low'=>'Low',
'rs_medium'=>'Medium',
'rs_not_parameterized'=>'Not Estimated',
'rs_risk_color_based_on_cl'=>'Risk color based on:',
'rs_risk_types_cl'=>'Risk Types:',
'rs_risks_values_cl'=>'Risk Values:',

/* './classes/risk_management/report/ISMSReportStatementOfApplicabilityFilter.php' */

'lb_control_types_cl'=>'Control Types:',

/* './classes/risk_management/report/ISMSReportTop10AssetsWithMoreRisksFilter.php' */

'lb_risk_types'=>'Risk Types',

/* './classes/risk_management/riskparameters/RiskParameters.php' */

'mx_justification_bl'=>'Reason',
'mx_justifying'=>'Justifying',

/* './classes/risk_management/RMAcceptRisk.php' */

'mx_risk_acceptance'=>'Risk Acceptance',

/* './classes/risk_management/RMArea.php' */

'mx_area'=>'Area',
'st_area_remove_error_message'=>'Not possible to remove Business Area <b>%area_name%</b>: The Area contains sub-areas and/or Processes. To remove a Business Area that contains sub-areas or processes, activate the option "Delete Cascade" in "Admin -> Settings".',
'tt_area_remove_erorr'=>'Error when removing Business Area',

/* './classes/risk_management/RMAsset.php' */

'mx_asset'=>'Asset',
'st_asset_remove_error_message'=>'Not possible to remove Asset <b>%asset_name%</b>: There are Risks associated with the Asset. To remove Assets to which there are associated Risks, activate the option "Delete Cascade" in "Admin -> Settings"',
'tt_asset_remove_error'=>'Error when removing Asset',

/* './classes/risk_management/RMBestPractice.php' */

'mx_best_practice'=>'Requirement',
'st_best_practice_remove_error_message'=>'It was not possible to remove the Requirement <b>%best_practice_name%</b>: The Requirement is associated with one or more Controls. To remove a Requirement that are associated with one or more Controls, user the option "Delete Cascade" in "Admin -> Settings"',
'tt_best_practice_remove_error'=>'Error while removing Requirement',

/* './classes/risk_management/RMBestPracticeEvent.php' */

'mx_best_practice_event_association'=>'Association of Requirement to Event',

/* './classes/risk_management/RMBestPracticeStandard.php' */

'mx_best_practice_standard_association'=>'Association of Requirement to Standard',

/* './classes/risk_management/RMCategory.php' */

'mx_category'=>'Category',
'st_category_remove_error_message'=>'It was not possible to remove Category <b>%category_name%</b>: The Category contains Subcategories and/or Events and/or Assets. In order to remove Categories that contain Subcategories, Events or Assets, you must activate option "Delete Cascade" in tab "Admin -> Configuration".',

/* './classes/risk_management/RMControl.php' */

'mx_control'=>'Control',
'st_rm_module_name'=>'Risk Assessment',

/* './classes/risk_management/RMControlBestPractice.php' */

'mx_control_best_practice_association'=>'Association of Requirements to Control',

/* './classes/risk_management/RMControlImplementationAccept.php' */

'mx_control_implementation_acceptance'=>'Acceptance of Control Implementation',

/* './classes/risk_management/RMEvent.php' */

'mx_event'=>'Event',

/* './classes/risk_management/RMProcess.php' */

'mx_process'=>'Process',

/* './classes/risk_management/RMProcessAsset.php' */

'mx_process_asset_association'=>'Association of Asset to Process',

/* './classes/risk_management/RMRisk.php' */

'mx_risk'=>'Risk',

/* './classes/risk_management/RMRiskControl.php' */

'mx_risk_control_association'=>'Association of Control to Risk',

/* './classes/risk_management/RMRiskLimits.php' */

'mx_risk_tolerance'=>'Risk Tolerance',

/* './classes/risk_management/RMSectionBestPractice.php' */

'mx_best_practice_section'=>'Requirement Section',
'st_home'=>'Root',
'st_section_control_best_practice_remove_error_message'=>'Not possible to remove Section <b>%section_name%</b>: Section contains Requirements and/or Sub-Sections which contain Requirements related with Controls.',
'st_section_remove_error_message'=>'Not possible to remove Section <b>%section_name%</b>: Section contains Requirements and/or Sub-Sections. To remove Sections which contain Sub-Sections or Requirements, activate the option "Delete Cascade" in "Admin -> Settings".',
'tt_section_remove_error'=>'Error when removing Section',

/* './classes/risk_management/RMStandard.php' */

'mx_standard'=>'Standard',
'st_libraries_module_name'=>'PCI DSS',
'st_remove_standard_error'=>'It is not possible to delete the standard <b>%standard_name%</b> because it is related to requirements.',
'tt_remove_standard_error'=>'Error while removing Standard',

/* './classes/WKFAlert.php' */

'em_alert_check_list'=>'<b>In order to view the alert, access the system and check your activities list.</b><br>For further information about this alert, get in contact with the security officer.',
'wk_ap_conclusion'=>'The Action Plan %name% has been finished, in other words, it has been measured.',
'wk_ap_efficiency_revision_late'=>'The revision of the efficiency of the Action Plan \'%name%\' has expired.',
'wk_ap_efficiency_revision_near'=>'The revision date of the efficiency of the Action Plan \'%name%\' is close.',
'wk_approved'=>'has been approved.',
'wk_asset_np_risk_association'=>'A non-estimated risk was associated with the asset \'%asset%\' from the suggested event \'%event%\'.',
'wk_control_implementation'=>'Control \'%name%\' must be implemented.',
'wk_control_test_late'=>'The Follow-up of the Control Test \'%name%\'  is late.',
'wk_deadline_close'=>'The deadline for the approval of the document \'%name%\'  is approaching.',
'wk_deadline_expired'=>'The deadline for the approval of the document \'%name%\' has expired.',
'wk_delegated'=>'has been delegated to you.',
'wk_denied'=>'has been denied.',
'wk_disciplinary_process'=>'The user(s) %users% has/have been added to the disciplinary process of incident %name%.',
'wk_document_comment'=>'The Document \'%name%\' has received a comment.',
'wk_document_is_now_published'=>'Document \'%name%\' was published.',
'wk_document_new_version'=>'The Document \'%name%\' has a new version.',
'wk_document_publication'=>'The document \'%name%\' was sent to publication.',
'wk_document_revision'=>'The Document \'%name%\' was sent for revision',
'wk_document_revision_late'=>'The revision of document \'%name%\' is delayed.',
'wk_implementation_expired'=>'The Implementation of Control \'%name%\' has expired.',
'wk_inc_asset'=>'The incident \'%incident%\' has been related to the asset \'%asset%\'.',
'wk_inc_control'=>'The incident \'%incident%\' has been related to the control \'%control%\'.',
'wk_inc_disposal_app'=>'The immediate disposal for the incident %name% was approved.',
'wk_inc_disposal_denied'=>'The immediate disposal for the incident %name% was denied.',
'wk_inc_evidence_required'=>'Evidence Collection Process required for incident \'%name%\'.',
'wk_inc_sent_to_responsible'=>'The incident \'%name%\' was forwarded to you.',
'wk_inc_solution_app'=>'The solution to the incident %name% has been approved.',
'wk_inc_solution_denied'=>'The solution to the incident %name% has been denied.',
'wk_incident'=>'The incident \'%name%\' has been created.',
'wk_incident_risk_parametrization'=>'The risk %risk_name% has been associated to asset %asset_name%, which you are responsible for. The risk parameters are different from the original risk.',
'wk_non_conformity_waiting_conclusion'=>'Nonconformity %name% awaiting finishing.',
'wk_non_conformity_waiting_deadline'=>'Nonconformity %name% awaiting deadline.',
'wk_pre_approved'=>'has been pre-approved.',
'wk_probability_update_down'=>'The frequency of incidents associated to the risk \'%name%\', in the last period, indicates that the likelihood is lower than what was estimated.',
'wk_probability_update_up'=>'The frequency of incidents associated to the risk \'%name%\', in the last period, indicates that the likelihood is larger than what was specified.',
'wk_real_efficiency_late'=>'The Follow-Up of Real Efficiency of Control \'%name%\' is late.',
'wk_risk_created'=>'The non-estimated risk \'%risk%\' has been associated to the asset \'%asset%\'.',
'wk_warning'=>'Warning',

/* './classes/WKFSchedule.php' */

'mx_last'=>'last',
'mx_second_last'=>'second to last',
'mx_second_wee'=>'second',
'st_schedule_and'=>'and',
'st_schedule_april'=>'April',
'st_schedule_august'=>'August',
'st_schedule_daily'=>'every day',
'st_schedule_day_by_month'=>'on day %day%',
'st_schedule_day_by_month_inverse'=>'on %day% day',
'st_schedule_day_by_week'=>'on %week% %week_day%',
'st_schedule_december'=>'December',
'st_schedule_each_n_days'=>'every %periodicity% days',
'st_schedule_each_n_months'=>'every %periodicity% months, %day_expression% of the month',
'st_schedule_each_n_weeks'=>'on %week_days%, every %periodicity% weeks',
'st_schedule_endless_interval'=>'From %start%',
'st_schedule_february'=>'February',
'st_schedule_friday_pl'=>'Fridays',
'st_schedule_interval'=>'Between %start% and %end%',
'st_schedule_january'=>'January',
'st_schedule_july'=>'July',
'st_schedule_june'=>'June',
'st_schedule_march'=>'March',
'st_schedule_may'=>'May',
'st_schedule_monday_pl'=>'Mondays',
'st_schedule_monthly'=>'%day_expression% of every month',
'st_schedule_november'=>'November',
'st_schedule_october'=>'October',
'st_schedule_saturday_pl'=>'Saturdays',
'st_schedule_september'=>'September',
'st_schedule_specified_months'=>'%day_expression% of %months%',
'st_schedule_sunday_pl'=>'Sundays',
'st_schedule_thursday_pl'=>'Thursdays',
'st_schedule_tuesday_pl'=>'Tuesdays',
'st_schedule_wednesday_pl'=>'Wednesdays',
'st_schedule_weekly'=>'every %week_days%',

/* './classes/WKFTask.php' */

'wk_task'=>'Task',
'wk_task_execution_permission'=>'You do not have permission to execute the task.',

/* './crontab.php' */

'em_saas_abandoned_system'=>'Abandoned System',

/* './damaged_system.php' */

'st_system_corrupted'=>'You system may be corrupted. Please, repeat the installation or contact our Support in Realiso\'s website.',

/* './damaged_system.xml' */

'tt_damaged_system'=>'System Corrupted',

/* './email_sender.php' */

'em_daily_digest'=>'Daily Digest',

/* './graphs/ISMSGraphAPActionType.php' */

'mx_corrective_action'=>'Corrective Action',
'mx_preventive_action'=>'Preventive Action',

/* './graphs/ISMSGraphAssetCost.php' */

'st_assets_cost_above_1m'=>'Assets with values above $1.000.000',
'st_assets_cost_below_100k'=>'Assets with values below $100.000',
'st_assets_cost_between_1m_and_500k'=>'Assets with values between $1.000.000 and $500.000',
'st_assets_cost_between_500k_and_100k'=>'Assets with values between $500.000 e $100.000',

/* './graphs/ISMSGraphAssetEstimatedCost.php' */

'st_assets_with_estimated_cost'=>'Assets with estimated cost',
'st_assets_without_estimated_cost'=>'Assets without estimated cost',

/* './graphs/ISMSGraphControlEfficiency.php' */

'mx_expected_efficiency'=>'Expected Efficiency',
'mx_real_efficiency'=>'Real Efficiency',

/* './graphs/ISMSGraphControlTest.php' */

'mx_test_not_ok'=>'Test Not OK',
'mx_test_ok'=>'Test OK',
'mx_test_value'=>'Test Value',

/* './graphs/ISMSGraphDocumentsAccessed.php' */

'st_accessed_documents'=>'Accessed Documents',
'st_accessed_documents_cumulative'=>'Accessed Documents - Cumulative',

/* './graphs/ISMSGraphDocumentsCreated.php' */

'st_created_documents'=>'Created Documents',
'st_created_documents_cumulative'=>'Created Documents - Cumulative',

/* './graphs/ISMSGraphDocumentsType.php' */

'mx_management'=>'Management',
'mx_others'=>'Others',
'mx_without_type'=>'No Type',

/* './graphs/ISMSGraphIncidentCategory.php' */

'st_others'=>'Others',

/* './graphs/ISMSGraphIncidentLossType.php' */

'mx_direct_losses'=>'Direct Losses',
'mx_indirect_losses'=>'Indirect Losses',

/* './graphs/ISMSGraphPotentialRisk.php' */

'st_high_risks'=>'High Risks',
'st_low_risks'=>'Low Risks',
'st_medium_risks'=>'Medium Risks',
'st_non_parameterized_risks'=>'Non-Estimated Risks',

/* './graphs/ISMSGraphResidualRisk.php' */

'st_high_risk'=>'High Risk',
'st_low_risk'=>'Low Risk',
'st_medium_risk'=>'Medium Risk',
'st_non_parameterized_risk'=>'Non-Estimated Risk',

/* './graphs/ISMSGraphTreatedRisk.php' */

'st_avoided_risk'=>'Avoided Risk',
'st_risk_accepted_being_yellow_or_red'=>'Risk accepted, being medium or high',
'st_risk_accepted_for_being_green'=>'Risk Accepted for being low',
'st_risk_treated_with_user_of_control'=>'Risk treated with the use of control',
'st_transferred_risk'=>'Transferred Risk',

/* './handlers/QueryCountDocsTimeToApprove.php' */

'gs_day'=>'day',
'gs_days'=>'days',
'gs_hour'=>'hour',
'gs_hours'=>'hours',
'gs_minute'=>'minute',
'gs_minutes'=>'minutes',
'gs_not_applicable'=>'NA',
'gs_second'=>'second',
'gs_seconds'=>'seconds',

/* './invalid_activation_code.php' */

'st_invalid_activation_code'=>'The activation code used is invalid. Please, revise it or contact our Support Team in Realiso\'s website.',

/* './invalid_activation_code.xml' */

'tt_invalid_activation_code'=>'Invalid Activation Code',

/* './license_corrupted.xml' */

'st_license_corrupted_message'=>'Your system is using an invalid license or one which has been corrupted.',
'tt_license_corrupted'=>'Invalid or Corrupted License',

/* './license_expired.php' */

'st_license_expired'=>'Your license expired on <b>%expiracy_str%</b>.',

/* './license_expired.xml' */

'tt_license_expired'=>'License is expired',

/* './license_isms.php' */

'st_incident_management'=>'Continual Improvement',
'st_number_of_modules'=>'Number of Modules',
'st_risk_management'=>'Risk Assessment',
'st_system_modules_bl'=>'<b>System Modules</b>',

/* './license_limit_reached.php' */

'st_assets'=>'Assets',
'st_modules'=>'Modules',
'st_standards'=>'Standards',
'st_users'=>'Users',

/* './license_limit_reached.xml' */

'st_license_limit_reached'=>'The maximum number of %attribute_name% has been reached. It is not possible to add %attribute_name%.',
'tt_license_limit_reached'=>'The limit of %attribute_name% has been reached',

/* './login.php' */

'ad_login_error_user_and_password_do_not_match'=>'%user_login% (%user_ip%)',
'ad_login_log_message'=>'%label_user% <b>%user_name%</b> %action%.',
'mx_session_conflict'=>'Login failed because there is another user signed in from the same browser.',
'st_blocked_instance_message'=>'Blocked Instance',
'tt_blocked_instance'=>'Blocked Instance',
'tt_session_conflict'=>'Login Failed',

/* './login.xml' */

'cb_remember_password'=>'Remember Password',
'cb_remember_user'=>'Remember User',
'lb_password_cl'=>'Password:',
'st_login_panel'=>'Login Panel',
'vb_forgot_your_password'=>'Forgot your password?',
'vb_login'=>'Login',
'wn_denied_access'=>'User without permission to access the system!',
'wn_invalid_user_or_password'=>'Invalid user or password!',
'wn_max_users'=>'Maximum number of simultaneous logins reached!',
'wn_user_blocked'=>'Blocked user!',

/* './migration/ISMS_import.php' */

'ad_created'=>'Created',
'ad_edited'=>'Edited',
'ad_removed_from_bin'=>'Removed from Recycle Bin',

/* './migration/ISMS_migration.php' */

'st_destination_db_error'=>'Error when connecting to migration\'s destination Database. 

 Verify if the data entered is correct!',
'st_source_db_error'=>'Error when connecting to migration\'s source database.

 Verify if the data entered is correct!',

/* './migration/ISMS_migration.xml' */

'lb_database_name_cl'=>'Database Name:',
'lb_database_password_cl'=>'Database Password:',
'lb_database_user_name_cl'=>'Database User Name:',
'lb_hostname_cl'=>'Hostname:',
'tt_database_copy_bl'=>'<b>Database Copy</b>',
'tt_destiny_database_bl'=>'<b>Destiny Database</b>',
'tt_results_bl'=>'<b>Results</b>',
'tt_source_database_bl'=>'<b>Source Database</b>',

/* './nav_graphics.xml' */

'lb_graphic_cl'=>'Choose a Graphic:',
'mx_10_assets_with_most_incidents'=>'Top 10 Assets with more Incidents',
'mx_10_assets_with_most_residual_risks_in_red'=>'Top 10 Assets with the most high risks',
'mx_10_assets_with_most_risks'=>'Top 10 Assets with more risks',
'mx_10_document_with_most_registers'=>'Top 10 Documents with more Records',
'mx_10_processes_with_most_associated_assets'=>'Top 10 Processes with more associated assets',
'mx_10_processes_with_most_documents'=>'Top 10 Processes with more Documents',
'mx_10_processes_with_most_incidents'=>'Top 10 Processes with more Incidents',
'mx_10_processes_with_most_nc_summary'=>'Top 10 Processes with more Nonconformities',
'mx_10_processes_with_most_residual_risks_in_red'=>'Top 10 Processes with the most high risks',
'mx_10_processes_with_most_risks'=>'Top 10 Processes with more risks',
'mx_10_processes_with_most_users'=>'Top 10 Processes with more involved people',
'mx_amount_of_accessed_documents'=>'Amount of Accessed Documents',
'mx_amount_of_created_documents'=>'Amount of Created Documents',
'mx_amount_of_documents_by_classification'=>'Amount of Documents by Classification Type',
'mx_amount_of_documents_by_status'=>'Amount of Documents by Status',
'mx_amount_of_documents_by_type'=>'Amount of Documents by Type',
'mx_ap_action_type_proportion'=>'Action Plans Proportion and their Types',
'mx_assets_with_without_estimated_costs'=>'Assets with/without Estimated Cost Value',
'mx_history_of_control_test'=>'History of Control Tests',
'mx_history_of_controls_efficiency_revision'=>'History of Controls Efficiency Revision',
'mx_incident_category_proportion'=>'Incidents and Categories Proportions',
'mx_incident_financial_impact_summary'=>'Top 10 Financial Impact Incidents',
'mx_incident_loss_type_proportion'=>'Incidents and Loss Types Proportions',
'mx_incidents_created_summary'=>'Summary of the last 6 months Incidents',
'mx_potential_risks_summary'=>'Potential Risks Summary',
'mx_residual_risks_summary'=>'Residual Risks Summary',
'mx_summary_of_assets_costs'=>'Summary of Assets and Costs',
'mx_summary_of_risks_treatments'=>'Summary of Risks Treatments',
'si_optgroup_ci'=>'Continual Improvement',
'si_optgroup_cost'=>'Cost',
'si_optgroup_pm'=>'Policies Manager',
'si_optgroup_revision'=>'Revision',
'si_optgroup_rm'=>'Risks Management',
'si_optgroup_test'=>'Test',

/* './nav_report.xml' */


/* './nav_search.xml' */

'cb_document_template'=>'Document Template',
'cb_incident'=>'Incident',
'cb_incident_category_library'=>'Incident Categories',
'cb_non_conformity'=>'Nonconformity',
'cb_occurrence'=>'Occurrence',
'cb_user'=>'User',
'lb_new_search_bl_cl'=>'<b>New Search:</b>',
'rb_action_plan'=>'Action Plan',
'rb_advanced'=>'Advanced',
'rb_basic'=>'Basic',
'rb_soluction'=>'Solutions',
'st_creation_cl'=>'Creation:',
'st_dates_filter_bl'=>'Date Filter',
'st_modification_cl'=>'Modification:',
'to_advanced_search_doc'=>'<b>Documents advanced search:</b><br/>Search type that includes the file content in the search, besides the basic search fields.',
'to_template_document_advanced_help'=>'<b>Advanced document template search</b><br/>Search type that includes the file content in the search, as well as the basic search fields.',
'tt_data_filter'=>'Date Filter',
'tt_elements_filter'=>'Items Filter',
'vb_dates_filter'=>'Date Filter',
'vb_elements_filter'=>'Elements Filter',

/* './nav_statistics.php' */

'mx_action_plan_efficient_and_not'=>'Amount of efficient and non-efficient Action Plans',
'mx_action_plan_finished_late'=>'Amount of Action Plans finished late',
'mx_action_plan_finished_on_time'=>'Amount of Action Plans finished on time',
'mx_action_plan_total'=>'Amount of Action Plans',
'mx_area_amount'=>'Amount of Areas',
'mx_area_assets_amount'=>'Amount of Area Assets',
'mx_area_assets_cost'=>'Cost of Area Assets',
'mx_area_controls_amount'=>'Amount of Area Controls',
'mx_area_controls_cost'=>'Cost of Area Controls',
'mx_area_processes_amount'=>'Amount of Area Processes',
'mx_area_risks_amount'=>'Amount of Area Risks',
'mx_area_total_impact'=>'Total Area Impact',
'mx_area_values'=>'Area Values',
'mx_areas_risk_summary'=>'Summary of Areas Risks',
'mx_areas_without_processes'=>'Areas without Processes',
'mx_asset_controls_amount'=>'Amount of Asset Controls',
'mx_asset_controls_cost'=>'Cost of Asset Controls',
'mx_asset_processes_amount'=>'Amount of Asset Processes',
'mx_asset_risks_amount'=>'Amount of Asset Risks',
'mx_asset_total_impact'=>'Total Asset Impact',
'mx_asset_values'=>'Asset Values',
'mx_assets_amount'=>'Amount of Assets',
'mx_assets_risk_summary'=>'Summary of Assets Risk',
'mx_assets_total_cost'=>'Total Cost of Assets',
'mx_assets_without_risk_events'=>'Assets without Risk Events',
'mx_business_areas'=>'Business Areas',
'mx_business_processes'=>'Business Processes',
'mx_control_risks_amount'=>'Amount of Control Risks',
'mx_controls_adequacy'=>'Adequacy of Controls',
'mx_controls_amount'=>'Amount of Controls',
'mx_controls_implementation'=>'Controls Implementation',
'mx_controls_revision'=>'Revision of Controls',
'mx_controls_test'=>'Test of Controls',
'mx_controls_without_associated_risks'=>'Controls without Associated Risks',
'mx_document_total'=>'Amount of Documents',
'mx_documents_by_state'=>'Amount of Documents by Status',
'mx_documents_by_type'=>'Amount of Documents by Type',
'mx_implemented_controls_investment'=>'Investment of Implemented Controls',
'mx_incident_total'=>'Amount of Incidents',
'mx_incidents_by_state'=>'Amount of Incidents per Status',
'mx_nc_per_ap_average'=>'Average amount of Nonconformities per Action Plan',
'mx_non_conformity_by_state'=>'Amount of Nonconformities by Status',
'mx_non_conformity_total'=>'Amount of Nonconformities',
'mx_non_parameterized_risks'=>'Non-Estimated Risks',
'mx_not_measured_implemented_controls'=>'Not Measured Implemented Controls',
'mx_occupation_documents'=>'Documents Files Ocupation (MB)',
'mx_occupation_registers'=>'Record Files Ocupation (MB)',
'mx_occupation_template'=>'Template Files Ocupation (MB)',
'mx_occupation_total'=>'Total Occupation (MB)',
'mx_process_assets_amount'=>'Amount of Process Assets',
'mx_process_assets_cost'=>'Cost of Process Assets',
'mx_process_controls_amount'=>'Amount of Process Controls',
'mx_process_controls_cost'=>'Cost of Process Controls',
'mx_process_risks_amount'=>'Amount of Process Risks',
'mx_process_total_impact'=>'Total Process Impact',
'mx_process_values'=>'Process Values',
'mx_processes_amount'=>'Amount of Processes',
'mx_processes_risk_summary'=>'Summary of Processes Risk',
'mx_processes_without_assets'=>'Processes without Assets',
'mx_read_documents'=>'Read Documents',
'mx_read_documents_proportion'=>'Read Documents Proportion',
'mx_register_total'=>'Amount of Records',
'mx_risk_treatment_potential_impact'=>'Potential Impact of Risk Treatment',
'mx_risks_amount'=>'Amount of Risks',
'mx_risks_potential_impact'=>'Potential Impact of Risks',
'mx_risks_treatment'=>'Treatment of Risks',
'mx_scheduled_documents'=>'Scheduled Revision Documents Proportion',
'mx_scheduled_revision_documents'=>'Scheduled Revision Documents',
'mx_standard_best_practice_amount'=>'Amount of Requirements of the Standard',
'mx_total_documents'=>'Total of Documents',
'mx_unread_documents'=>'Non Read Documents',
'mx_unread_documents_proportion'=>'Non Read Documents Proportion',
'si_abnormalities'=>'Abnormalities',
'si_action_plan_statistics'=>'Action Plan Statistics',
'si_area_statistics'=>'Area Statistics',
'si_asset_statistics'=>'Asset Statistics',
'si_best_practice_summary'=>'Requirement Summary',
'si_continual_improvement'=>'Continual Improvement Statistics',
'si_control_summary'=>'Control Summary',
'si_financial_statistics'=>'Financial Statistics',
'si_management_level_and_range'=>'Range and Levels of Management',
'si_policy_management'=>'Policy Management Statistics',
'si_process_statistics'=>'Process Statistics',
'si_risk_summary'=>'Risk Summary',
'si_risks_summary'=>'Risks Summary',
'si_system_status'=>'System Status',
'st_accepted_risk'=>'Accepted Risk',
'st_applied_best_practices_amount'=>'Amount of Applied Requirements',
'st_best_practices_amount'=>'Amount of Requirements',
'st_complete_areas'=>'Complete Areas',
'st_complete_assets'=>'Complete Assets',
'st_complete_processes'=>'Complete Processes',
'st_controls_being_implemented'=>'Controls being implemented',
'st_controls_correctly_implemented'=>'Controls Correctly Implemented',
'st_controls_incorrectly_implemented'=>'Controls Incorrectly Implemented',
'st_controls_with_delayed_implementation'=>'Controls with delayed implementation',
'st_denied_access_to_statistics'=>'The settings referring to the data collection are disabled, therefore, it is not possible to view this screen.',
'st_efficient_action_plan'=>'Efficient action plans',
'st_medium_high_not_treated_risk'=>'Medium/ High / Not Treated Risk',
'st_mitigated_risk'=>'Reduced Risk',
'st_non_efficient_action_plan'=>'Non-efficient action plans',
'st_not_treated_risks'=>'Not Treated Risks',
'st_partial_areas'=>'Partial Areas',
'st_partial_assets'=>'Partial Assets',
'st_partial_processes'=>'Partial Processes',
'st_potentially_low_risk'=>'Potentially Low Risk',
'st_successfully_revised'=>'Successfully Revised',
'st_successfully_tested'=>'Successfully tested',
'st_tranferred_risk'=>'Transferred Risk',
'st_treated_risks'=>'Treated Risks',
'st_unmanaged_areas'=>'Unmanaged Areas',
'st_unmanaged_assets'=>'Unmanaged Assets',
'st_unmanaged_processes'=>'Unmanaged Processes',
'st_unsuccessfully_revised'=>'Unsuccessfully revised',
'st_unsuccessfully_tested'=>'Unsuccessfully tested',

/* './nav_statistics.xml' */

'lb_finish_cl'=>'Finish:',
'lb_standard_cl'=>'Standard:',
'lb_statistics_cl'=>'Statistics:',
'rb_adequate'=>'Suitable',
'rb_ap_efficient'=>'Efficient',
'rb_ap_non_efficient'=>'Non-efficient',
'rb_in_implantation'=>'Under implementation',
'rb_inadequate'=>'Unsuitable',
'rb_late'=>'Delayed',
'rb_not_successfully'=>'Not Successfully',
'rb_not_treated'=>'Not Treated',
'rb_real_risk'=>'Potential Risk',
'rb_residual_risk'=>'Residual Risk',
'rb_successfully'=>'Successfully',
'rb_treated'=>'Treated',
'st_chart_not_enough_data'=>'There is not enough data collected to generate that graph.',
'vb_refresh'=>'Refresh',

/* './nav_summary_advanced.php' */

'gc_amount_bl'=>'<b>#</b>',
'gc_applied_bl'=>'<b>Applied</b>',
'gc_assets_bl'=>'<b>Assets</b>',
'gc_best_practices_bl'=>'<b># of Requirements</b>',
'gc_description_bl'=>'<b>Description</b>',
'gc_disciplinary_process_amount_bl'=>'<b># of Disciplinary Processes</b>',
'gc_doc_rev_qty_bl'=>'<b>#</b>',
'gc_doc_sumary_percentage_bl'=>'<b>%</b>',
'gc_doc_sumary_qty_bl'=>'<b>#</b>',
'gc_document_bl'=>'<b>Document</b>',
'gc_efficiency_rates_bl'=>'<b>Efficiency Rates</b>',
'gc_financial_impact_bl'=>'<b>Financial Impact</b>',
'gc_implemented_controls_bl'=>'<b>Implemented Controls</b>',
'gc_incident_amount_bl'=>'<b># of Incidents</b>',
'gc_investment_bl'=>'<b>Investment</b>',
'gc_nc_amount'=>'<b># of Non Conformities</b>',
'gc_nc_amount_bl'=>'<b># of Nonconformities</b>',
'gc_planned_controls_bl'=>'<b>Planned Controls</b>',
'gc_potential_bl'=>'<b>Potential</b>',
'gc_potential_impact_bl'=>'<b>Potential Impact</b>',
'gc_processes_bl'=>'<b>Processes</b>',
'gc_residual_bl'=>'<b>Residual</b>',
'gc_standards_bl'=>'<b>Standards</b>',
'gc_status_bl'=>'<b>Status</b>',
'gc_total_bl'=>'<b>Total</b>',
'gc_users_bl'=>'<b>Employee</b>',
'gs_accepted_risk'=>'Accepted Risk',
'gs_amount_bl'=>'<b>#</b>',
'gs_applicability_statement'=>'Statement of Aplicability',
'gs_avoided_risk'=>'Avoided Risk',
'gs_ci_ap_without_doc'=>'Action Plan without Documents',
'gs_ci_cn_without_ap'=>'Nonconformity without Action Plans',
'gs_ci_incident_without_occurrence'=>'Incidents without Occurrences',
'gs_ci_incident_without_risks'=>'Incidents without Risks',
'gs_ci_pending_tasks'=>'Continual Improvement pending tasks',
'gs_components_without_document'=>'Components without Documents',
'gs_controls_without_associated_risk'=>'Controls without Associated Risks',
'gs_delayed'=>'Delayed',
'gs_high_risk'=>'High Risk',
'gs_impact_left_bl'=>'<b>Impact Left</b>',
'gs_implemented_controls_mitigated_risks_bl'=>'Implemented Controls/Reduced Risks',
'gs_implemented_controls_protected_assets_bl'=>'Implemented Controls/Protected Assets',
'gs_implemented_controls_treated_risks_bl'=>'Implemented Controls/Treated Risks',
'gs_inefficient_controls'=>'Non-efficient controls',
'gs_low_risk'=>'Low Risk',
'gs_medium_risk'=>'Medium Risk',
'gs_mitigated_risk'=>'Reduced Risk',
'gs_non_parameterized_risks'=>'Non-estimated risks',
'gs_not_measured_controls'=>'Not measured controls',
'gs_not_treated'=>'Not Treated',
'gs_not_treated_risks_high_and_medium'=>'Not Treated Risks - High and Medium',
'gs_not_trusted_controls'=>'Unreliable controls',
'gs_pm_docs_with_pendant_read'=>'Documents with pending reading',
'gs_pm_documents_without_register'=>'Documents without Records',
'gs_pm_incident_without_risks'=>'Documents with high frequency of revision',
'gs_pm_never_read_documents'=>'Documents never read',
'gs_pm_pending_tasks'=>'Policy Management pending tasks',
'gs_pm_users_with_pendant_read'=>'Users with pending reading',
'gs_potential_risks_low'=>'Potential Risks - Low',
'gs_risk_management_pending_tasks'=>'Risk Assessment Pending Tasks',
'gs_risk_treatment_plan'=>'Risk Treatment Plan',
'gs_scope_statement'=>'Scope Statement',
'gs_sgsi_policy_statement'=>'PCI Policy Statement',
'gs_total_bl'=>'<b>Total</b>',
'gs_total_non_parameterized_risks_bl'=>'<b>Non-Estimated Risks</b>',
'gs_total_parameterized_risks_bl'=>'<b>Estimated Risks</b>',
'gs_total_risks_bl'=>'<b>Total</b>',
'gs_transferred_risk'=>'Transferred Risk',
'gs_treated'=>'Treated',
'gs_trusted_and_efficient_controls'=>'Reliable and efficient controls',
'gs_under_implementation'=>'Under Implementation',
'tt_help_impact_left'=>'<b>Impact Left</b><br/><br/>Sum of the risk impacts that remained after the treatment.<br/>This value represents the total of potential impact to which the company is subject after the risk treatment.',
'tt_help_implemented_controls_mitigated_risks'=>'<b>Implemented Controls/Reduced Risks</b><br/><br/>Ratio between the sum of costs of the implementd controls and the sum of potential impacts of the Reduced Risks.<br/>This value shows the control cost considering the sum of potential risk impact to the company in financial terms. This ratio includes only the value of risks that were reduced by the application of controls.',
'tt_help_implemented_controls_protected_assets'=>'<b>Implemented Controls/Protected Assets</b><br/><br/>Ratio between the sum of the costs of the implemented controls and the value of the protected assets.<br/>It is important not to spend more with the controls than the value of the protected assets.',
'tt_help_implemented_controls_treated_risks'=>'<b>Implemented Controls/Treated Risks</b><br/><br/>Ratio between the sum of costs of the implemented controls and the sum of potential impacts of the Treated Risks.<br/>This value shows the control cost considering the sum of potential risk impact to the company in financial terms. This ratio includes all risks that were treated by any of the possible forms of treatment.',
'tt_help_inefficient_controls'=>'<b>Non-efficient controls</b><br/><br/> Controls whose efficiency revision is not delayed, and whose last revision has been considered as non-efficient (the control real efficiency is lower than the expected efficiency)',
'tt_help_not_measured_controls'=>'<b> Not measured controls</b><br/><br/> Implemented controls that do not have efficiency revision, nor test, or whose measure, efficiency revision, or test has expired.',
'tt_help_trusted'=>'<b>Unreliable Controls</b><br/><br/> Controls whose test is not delayed, and whose last test has failed.',
'tt_help_trusted_and_efficient'=>'<b> Reliable and Efficient Controls</b><br/><br/> Controls which have test and efficiency revision enabled, and both have not been delayed; and the last test performed was reliable, and the last revision was efficient (when control real efficiency is equal or bigger than the control expected efficiency)',

/* './nav_summary_advanced.xml' */

'mi_excel_report'=>'EXCEL Report',
'mi_html_report'=>'HTML Report',
'mi_html_report_download'=>'HTML Report DOWNLOAD',
'mi_pdf_report'=>'PDF Report',
'mi_word_report'=>'WORD Report',
'tt_abnormalities_pm_summary_bl'=>'<b>Policy Management Abnormalities Summary</b>',
'tt_abnormalities_rm_summary_bl'=>'<b>Risk Assessment Abnormalities Summary</b>',
'tt_abnormality_ci_summary_bl'=>'<b>Continual Improvement Abnormalities Summary</b>',
'tt_best_practices_summary_bl'=>'<b>Requirements - Compliance Index</b>',
'tt_controls_summary_bl'=>'<b>Controls Overview</b>',
'tt_documents_average_approval_time_bl'=>'<b>Documents Average Approval Time</b>',
'tt_financial_summary_bl'=>'<b>Financial Summary</b>',
'tt_grid_incident_summary_title_bl'=>'<b>Incident Overview</b>',
'tt_grid_incidents_per_asset_title_bl'=>'<b>Top 5 Assets with more Incidents</b>',
'tt_grid_incidents_per_process_title_bl'=>'<b>Top 5 Processes with more Incidents</b>',
'tt_grid_incidents_per_user_title_bl'=>'<b>Top 5 Users with more Disciplinary Processes</b>',
'tt_grid_nc_per_process_title_bl'=>'<b>Top 5 Processes with more Nonconformities</b>',
'tt_grid_nc_summary_title_bl'=>'<b>Nonconformities Summary</b>',
'tt_incident_summary_bl'=>'<b>Continual Improvement Summaries</b>',
'tt_last_documents_revision_time_period_summary_bl'=>'<b>Last Documents Revision Time Period Summary</b>',
'tt_parameterized_risks_summary_bl'=>'<b>Potential Risk vs. Residual Risk</b>',
'tt_policy_management_summary_bl'=>'<b>Policy Management Overview</b>',
'tt_policy_management_x_risk_management_bl'=>'<b>Documents per Business Element</b>',
'tt_risk_management_documentation_summary_bl'=>'<b>Risk Assessment Documentation Summary</b>',
'tt_risk_management_status_bl'=>'<b>Risk Level</b>',
'tt_sgsi_documents_and_reports_bl'=>'<b>Compliance Mandatory Documents</b>',
'tt_top_10_most_read_documents_bl'=>'<b>Reading Documents - Top 10</b>',

/* './nav_summary_advancedNEW.xml' */

'tt_general_risks_summary_bl'=>'<b>Estimated vs. Non-Estimated Risk</b>',

/* './nav_summary_basic.php' */

'gc_documents_qty_bl'=>'<b># of Documents</b>',
'gc_documents_status_bl'=>'<b>Documents Status</b>',
'gc_percentage_bl'=>'<b>%</b>',
'gc_qty_bl'=>'<b>#</b>',
'gc_readings_bl'=>'<b>Readings</b>',
'gc_time_period_smaller_or_equal_bl'=>'<b>Time Period (shorter or equal than)</b>',
'gs_area'=>'Area',
'gs_areas_without_processes'=>'Areas without processes',
'gs_asset'=>'Asset',
'gs_assets_without_risk_events'=>'Assets without risk events',
'gs_average_time'=>'Average Time',
'gs_controls_without_risk_events_association'=>'Controls without association with risk events',
'gs_documents_to_be_read'=>'Documents to be read',
'gs_documents_total_bl'=>'<b>Documents Total</b>',
'gs_documents_with_files'=>'Documents with Files',
'gs_documents_with_links'=>'Documents with Links',
'gs_more_than_six_months'=>'More than Six Months',
'gs_na'=>'NA',
'gs_one_month'=>'One Month',
'gs_one_week'=>'One Week',
'gs_process'=>'Process',
'gs_processes_without_assets'=>'Processes without assets',
'gs_published_documents_information_bl'=>'<b>Published Documents</b>',
'gs_risk'=>'Risk',
'gs_six_months'=>'Six Months',
'gs_three_months'=>'Three Months',

/* './nav_summary_basic.xml' */

'gc_period'=>'Period',
'tt_my_documents_average_approval_time_bl'=>'<b>My Documents Average Approval Time</b>',
'tt_my_documents_last_revision_average_time_summary_bl'=>'<b>My Documents Last Revision Average Time Summary</b>',
'tt_my_documents_policy_management_summary_bl'=>'<b>Policy Management (My Documents) Summary</b>',
'tt_my_elements_bl'=>'<b>My Items</b>',
'tt_my_pendencies_bl'=>'<b>My Pendings from Risk Assessment</b>',
'tt_my_risk_summary_bl'=>'<b>My Risk Summary</b>',
'tt_policy_management_x_risk_management_my_elements_bl'=>'<b>Risk Assessment (My Items) x Policy Management</b>',
'tt_policy_sumary_bl'=>'<b>Policy Management Summaries</b>',
'tt_risk_management_my_elements_documentation_summary_bl'=>'<b>Risk Assessment My Items Documentation Summary</b>',
'tt_risk_summary_bl'=>'<b>Risk Assessment Summaries</b>',
'tt_top_10_most_read_my_documents_bl'=>'<b>Top 10 My Documents Most Read</b>',

/* './nav_warnings.xml' */

'gc_inquirer'=>'Inquirer',
'gc_justif'=>'Just.',
'mi_denied'=>'Deny',
'mi_open_task'=>'Open task',
'tt_alerts_bl'=>'<b>My Alerts</b>',
'tt_tasks_bl'=>'<b>My Tasks</b>',
'vb_remove_alerts'=>'Remove Alerts',
'wn_removal_warning'=>'You must select at least one warning to be removed.',

/* './packages/admin/acl_tree.xml' */

'to_acl_A'=>'Permissions to \'Admin\' area of the system',
'to_acl_A.A'=>'Audit tab',
'to_acl_A.A.1'=>'Tab with the list of tasks generated by the system',
'to_acl_A.A.2'=>'Tab with the list of alerts generated by the system',
'to_acl_A.A.3'=>'Tab with the actions performed in the system',
'to_acl_A.C'=>'System customization tab',
'to_acl_A.C.1'=>'Customization of names and of the amount of risk estimation parameters.',
'to_acl_A.C.10'=>'Customization of the weight of each risk parameter',
'to_acl_A.C.11'=>'Customization of system classification',
'to_acl_A.C.2'=>'Customization of the risk matrix definition, of the asset importance estimation label, impact and likelihood, and of the control reduction criteria',
'to_acl_A.C.3'=>'Customization of costs estimation parameters',
'to_acl_A.C.4'=>'Customization of financial impact parameters',
'to_acl_A.C.5'=>'Customization of several characteristics of the system',
'to_acl_A.C.6'=>'Customization of special users',
'to_acl_A.C.7'=>'Customization of the amount and the names of types and priorities used in the system',
'to_acl_A.C.7.1'=>'Configuration of area classification, in relation to type and priority',
'to_acl_A.C.7.2'=>'Configuration of process classification, in relation to type and priority',
'to_acl_A.C.7.3'=>'Configuration of risk classification, in relation to type',
'to_acl_A.C.7.4'=>'Configuration of event classification, in relation to type',
'to_acl_A.C.7.5'=>'Configuration of control classification, in relation to type',
'to_acl_A.C.7.6'=>'Configuration of document classification, in relation to type',
'to_acl_A.C.7.7'=>'Configuration of record classification, in relation to type',
'to_acl_A.C.8'=>'Customization of the security controls',
'to_acl_A.C.9'=>'Customization of the risk acceptance level',
'to_acl_A.L'=>'Recycle Bin tab',
'to_acl_A.L.1'=>'Button to restore the selected items',
'to_acl_A.L.2'=>'Button to remove the selected items',
'to_acl_A.L.3'=>'Button to remove all items from the recycle bin',
'to_acl_A.MA'=>'Account management tab',
'to_acl_A.P'=>'Profiles tab',
'to_acl_A.P.1'=>'Menu to edit a certain profile',
'to_acl_A.P.2'=>'Menu to remove a certain profile',
'to_acl_A.P.3'=>'Button to insert a profile',
'to_acl_A.S'=>'System settings tab',
'to_acl_A.S.1'=>'Password policy configuration',
'to_acl_A.S.2'=>'System \'Delete Cascade\' configuration',
'to_acl_A.U'=>'Users tab',
'to_acl_A.U.1'=>'Menu to edit a certain user',
'to_acl_A.U.2'=>'Menu to remove a certain user',
'to_acl_A.U.3'=>'Button to insert a user',
'to_acl_A.U.4'=>'Button to reset the passwords of all users',
'to_acl_A.U.5'=>'Menu to block system users',
'to_acl_M'=>'Permissions for system \'User view\' mode',
'to_acl_M.CI'=>'Continual improvement tab',
'to_acl_M.CI.1'=>'Occurrences tab',
'to_acl_M.CI.1.1'=>'Permission to view all occurrences',
'to_acl_M.CI.1.2'=>'Button to insert occurrences',
'to_acl_M.CI.1.3'=>'Menu to edit even if the user is neither the incident manager nor the occurrence creator',
'to_acl_M.CI.1.4'=>'Menu to remove even if the user is neither the incident manager nor the occurrence creator',
'to_acl_M.CI.2'=>'Incident tab',
'to_acl_M.CI.2.1'=>'Permission to view all incidents',
'to_acl_M.CI.2.2'=>'Button to insert an incident',
'to_acl_M.CI.2.3'=>'Menu to edit even if the user is not the incident manager',
'to_acl_M.CI.2.4'=>'Menu to remove even if the user is not the incident manager',
'to_acl_M.CI.3'=>'Nonconformity tab',
'to_acl_M.CI.3.1'=>'Permission to view all nonconformities',
'to_acl_M.CI.3.2'=>'Button to insert a nonconformity',
'to_acl_M.CI.3.3'=>'Menu to edit even if the user is not the manager of the nonconformity',
'to_acl_M.CI.3.4'=>'Menu to remove even if the user is not the manager of the nonconformity',
'to_acl_M.CI.5'=>'Disciplinary process tab',
'to_acl_M.CI.5.1'=>'Permission to view the action taken in relation to a user involved in a certain disciplinary process',
'to_acl_M.CI.5.2'=>'Permission to register the action that must be taken in relation to a user involved in a certain disciplinary process',
'to_acl_M.CI.5.3'=>'Permission to remove a user from a certain disciplinary process',
'to_acl_M.CI.6'=>'Reports tab',
'to_acl_M.CI.6.1'=>'Incident reports',
'to_acl_M.CI.6.1.1'=>'Occurrences by incident report',
'to_acl_M.CI.6.1.2'=>'Disciplinary process report',
'to_acl_M.CI.6.1.3'=>'Evidence collection report',
'to_acl_M.CI.6.1.4'=>'Follow up of occurrences report',
'to_acl_M.CI.6.1.5'=>'Follow up of incident report',
'to_acl_M.CI.6.1.6'=>'Financial impact by incident report',
'to_acl_M.CI.6.1.7'=>'Control revisions caused by incidents report',
'to_acl_M.CI.6.1.8'=>'Items affected by incidents report',
'to_acl_M.CI.6.1.9'=>'Automatic calculation of risk likelihood report',
'to_acl_M.CI.6.2'=>'Nonconformities reports',
'to_acl_M.CI.6.2.1'=>'Nonconformity by process report',
'to_acl_M.CI.6.2.2'=>'Nonconformity of control by process report',
'to_acl_M.CI.6.2.3'=>'Follow up of nonconformity report',
'to_acl_M.CI.6.2.4'=>'Action plan by user report',
'to_acl_M.CI.6.2.5'=>'Action plan by process report',
'to_acl_M.CI.6.3'=>'Continual improvement reports',
'to_acl_M.CI.6.3.1'=>'Continual Improvement pending tasks report',
'to_acl_M.CI.6.3.2'=>'Report of incidents with no risks associated to them.',
'to_acl_M.CI.6.3.3'=>'Report of nonconformities without action plans associated to them.',
'to_acl_M.CI.6.3.4'=>'Action Plan without Document report',
'to_acl_M.CI.6.3.5'=>'Incidents without occurrences report',
'to_acl_M.CI.7'=>'Action plan tab',
'to_acl_M.CI.7.1'=>'Permission to view all action plans',
'to_acl_M.CI.7.2'=>'Button to insert an action plan',
'to_acl_M.CI.7.3'=>'Permission to edit any action plan',
'to_acl_M.CI.7.4'=>'Permission to remove any action plan',
'to_acl_M.D'=>'Dashboard tab',
'to_acl_M.D.1'=>'Summary tab',
'to_acl_M.D.2'=>'Advanced Summary tab',
'to_acl_M.D.3'=>'Graphics tab',
'to_acl_M.D.6'=>'System Statistics (graphics) tab',
'to_acl_M.D.6.1'=>'Statistics about business areas managed by the system',
'to_acl_M.D.6.10'=>'Statistics about system status (summary of risk x area, risk x process, risk x asset, summary of risks)',
'to_acl_M.D.6.11'=>'Statistics about system policy management',
'to_acl_M.D.6.12'=>'Statistics about system continual improvement',
'to_acl_M.D.6.13'=>'Statistics about action plans and about the relationship between action plan and nonconformity',
'to_acl_M.D.6.2'=>'Statistics about business areas processes managed by the system',
'to_acl_M.D.6.3'=>'Statistics about assets managed by the system',
'to_acl_M.D.6.4'=>'Summary of risks managed by the system',
'to_acl_M.D.6.5'=>'Summary of requirements',
'to_acl_M.D.6.6'=>'Summary of system controls',
'to_acl_M.D.6.7'=>'Summary of abnormalities detected by the system',
'to_acl_M.D.6.8'=>'Financial statistics - costs, financial impact and investment',
'to_acl_M.D.6.9'=>'Statistics about area, process and asset coverage and management',
'to_acl_M.L'=>'Risk Assessment tab',
'to_acl_M.L.1'=>'Events Library tab',
'to_acl_M.L.1.1'=>'Menu to browse in sub-categories and events of a certain category',
'to_acl_M.L.1.2'=>'Menu to edit a certain category',
'to_acl_M.L.1.3'=>'Menu to remove a certain category',
'to_acl_M.L.1.4'=>'Menu to associate requirements to a certain event',
'to_acl_M.L.1.5'=>'Menu to edit a certain event',
'to_acl_M.L.1.6'=>'Menu to remove a certain event',
'to_acl_M.L.1.7'=>'Button to insert a new category',
'to_acl_M.L.1.8'=>'Button to insert a new event',
'to_acl_M.L.1.9'=>'Button to associate an event to the category in which the user is',
'to_acl_M.L.2'=>'Requirements Library tab',
'to_acl_M.L.2.1'=>'Menu to browse in sub-sections and requirements of a certain section',
'to_acl_M.L.2.10'=>'Allows the user to create a document template for the requirement',
'to_acl_M.L.2.2'=>'Menu to edit a certain section',
'to_acl_M.L.2.3'=>'Menu to remove a certain section',
'to_acl_M.L.2.4'=>'Menu to associate events to a certain requirement',
'to_acl_M.L.2.5'=>'Menu to edit a certain requirement',
'to_acl_M.L.2.6'=>'Menu to remove a certain requirement',
'to_acl_M.L.2.7'=>'Button to insert a new section',
'to_acl_M.L.2.8'=>'Button to insert a new requirement',
'to_acl_M.L.2.9'=>'Displays the requirement document templates in the document templates tab',
'to_acl_M.L.3'=>'Standards Library tab',
'to_acl_M.L.3.1'=>'Menu to edit a certain standard',
'to_acl_M.L.3.2'=>'Menu to remove a certain standard',
'to_acl_M.L.3.3'=>'Button to insert a new standard',
'to_acl_M.L.4'=>'Requirements Library tab',
'to_acl_M.L.4.1'=>'Menu to edit a certain section',
'to_acl_M.L.4.2'=>'Menu to edit a certain section',
'to_acl_M.L.5'=>'Document Templates tab',
'to_acl_M.L.5.1'=>'Menu to edit a document template',
'to_acl_M.L.5.2'=>'Menu to remove a document template',
'to_acl_M.L.5.3'=>'Button to insert a new document template',
'to_acl_M.L.6'=>'Categories library tab',
'to_acl_M.L.6.1'=>'Button to insert a category',
'to_acl_M.L.6.2'=>'Menu to edit a category',
'to_acl_M.L.6.3'=>'Menu to remove a category',
'to_acl_M.L.6.4'=>'Button to insert a solution',
'to_acl_M.L.6.5'=>'Menu to edit solution',
'to_acl_M.L.6.6'=>'Menu to remove solution',
'to_acl_M.PM'=>'Policy Managment tab',
'to_acl_M.PM.10'=>'Viewing/management of documents in development tab',
'to_acl_M.PM.10.1'=>'Button to insert a document',
'to_acl_M.PM.11'=>'Viewing/management of documents in approval tab',
'to_acl_M.PM.12'=>'Viewing/management of published documents tab',
'to_acl_M.PM.13'=>'Viewing/management of documents in revision tab',
'to_acl_M.PM.14'=>'Viewing/management of obsolete documents tab',
'to_acl_M.PM.15'=>'Viewing/management of all documents tab',
'to_acl_M.PM.16'=>'Viewing/management of records tab',
'to_acl_M.PM.17'=>'Reports tab',
'to_acl_M.PM.17.1'=>'Other reports',
'to_acl_M.PM.17.1.16'=>'Report that lists the properties of a certain document',
'to_acl_M.PM.17.1.17'=>'Classification report',
'to_acl_M.PM.17.2'=>'General reports',
'to_acl_M.PM.17.2.1'=>'Documents report',
'to_acl_M.PM.17.2.2'=>'Documents by area report',
'to_acl_M.PM.17.2.3'=>'Documents by PCI component report',
'to_acl_M.PM.17.2.4'=>'Documents changing comments and revision reasons report',
'to_acl_M.PM.17.2.5'=>'User comments report',
'to_acl_M.PM.17.3'=>'Record reports',
'to_acl_M.PM.17.3.1'=>'Record by document report',
'to_acl_M.PM.17.4'=>'Access Control reports',
'to_acl_M.PM.17.4.1'=>'User access audit report',
'to_acl_M.PM.17.4.2'=>'Access to documents audit report',
'to_acl_M.PM.17.4.3'=>'Documents and their approvers report',
'to_acl_M.PM.17.4.4'=>'Documents and their readers report',
'to_acl_M.PM.17.4.5'=>'Users associated to processes report',
'to_acl_M.PM.17.5'=>'Management Reports',
'to_acl_M.PM.17.5.1'=>'Documents with pending reading report',
'to_acl_M.PM.17.5.2'=>'Most accessed documents report',
'to_acl_M.PM.17.5.3'=>'Documents not accessed report',
'to_acl_M.PM.17.5.4'=>'Most reviewed documents report',
'to_acl_M.PM.17.5.5'=>'Pending approvals report',
'to_acl_M.PM.17.5.6'=>'Documents creation and revision date report',
'to_acl_M.PM.17.5.7'=>'Documents revision schedule report',
'to_acl_M.PM.17.5.8'=>'Documents with delayed revision report',
'to_acl_M.PM.17.6'=>'Abnormality reports',
'to_acl_M.PM.17.6.1'=>'Components without documents report',
'to_acl_M.PM.17.6.2'=>'Policy management pending tasks',
'to_acl_M.PM.17.6.3'=>'Documents with a high frequency of revision. High frequency refers to documents that are reviewed more than once a week on average.',
'to_acl_M.PM.17.6.4'=>'Documents without records report',
'to_acl_M.PM.17.6.5'=>'Report that lists the users who are readers of documents that are published, but who haven\'t read those documents.',
'to_acl_M.PM.18'=>'Visualization / Management of documents to be published',
'to_acl_M.RM'=>'Risk Assessment tab',
'to_acl_M.RM.1'=>'Area tab',
'to_acl_M.RM.1.1'=>'Permission to view all areas',
'to_acl_M.RM.1.2'=>'Menu to browse in sub-areas of a certain area',
'to_acl_M.RM.1.3'=>'Menu to view processes of a certain area',
'to_acl_M.RM.1.4'=>'Menu to edit a certain area',
'to_acl_M.RM.1.5'=>'Menu to remove a certain area',
'to_acl_M.RM.1.6'=>'Button to insert a new area',
'to_acl_M.RM.1.7'=>'Button to insert a new area in the root',
'to_acl_M.RM.1.8'=>'Permission to edit an area, provided that you area responsible for it',
'to_acl_M.RM.1.9'=>'Permission to remove an Area, as long as you are responsible for it',
'to_acl_M.RM.2'=>'Process tab',
'to_acl_M.RM.2.1'=>'Permission to view all processes',
'to_acl_M.RM.2.2'=>'Menu to browse in assets of a certain process',
'to_acl_M.RM.2.4'=>'Menu to edit a certain process',
'to_acl_M.RM.2.5'=>'Menu to remove a certain process',
'to_acl_M.RM.2.6'=>'Button to insert a new process',
'to_acl_M.RM.2.7'=>'Permission to edit a process, provided that you are responsible for it',
'to_acl_M.RM.2.8'=>'Permission to remove a Process, as long as you are responsible for it',
'to_acl_M.RM.3'=>'Asset tab',
'to_acl_M.RM.3.1'=>'Permission to view all assets',
'to_acl_M.RM.3.10'=>'Context menu for risk insertion to assets from events of a category of the Event Category Library',
'to_acl_M.RM.3.2'=>'Menu to browse in risks of a certain asset',
'to_acl_M.RM.3.4'=>'Menu to create a risk for an asset, based on an event',
'to_acl_M.RM.3.5'=>'Menu to edit a certain asset',
'to_acl_M.RM.3.6'=>'Menu to remove a certain asset',
'to_acl_M.RM.3.7'=>'Button to insert a new asset',
'to_acl_M.RM.3.8'=>'Permission to edit an asset, provided that you are responsible for it',
'to_acl_M.RM.3.9'=>'Permission to remove an Asset, as long as you are responsible for it',
'to_acl_M.RM.4'=>'Risks tab',
'to_acl_M.RM.4.1'=>'Permission to view all risks',
'to_acl_M.RM.4.10'=>'Permission to remove a Risk, as long as you are responsible for it',
'to_acl_M.RM.4.2'=>'Menu to browse in controls of a certain risk',
'to_acl_M.RM.4.4'=>'Menu to edit a certain risk',
'to_acl_M.RM.4.5'=>'Menu to remove a certain risk',
'to_acl_M.RM.4.6'=>'Button to insert a new risk',
'to_acl_M.RM.4.8'=>'Button to estimate the risk',
'to_acl_M.RM.4.9'=>'Permission to edit a risk, provided that you are responsible for it',
'to_acl_M.RM.5'=>'Control tab',
'to_acl_M.RM.5.1'=>'Permission to view all controls',
'to_acl_M.RM.5.2'=>'Menu to browse in the risks associated to a certain control',
'to_acl_M.RM.5.3'=>'Menu to associate risks to a certain control',
'to_acl_M.RM.5.4'=>'Menu to edit a certain control',
'to_acl_M.RM.5.5'=>'Menu to remove a certain control',
'to_acl_M.RM.5.6'=>'Button to insert a new control',
'to_acl_M.RM.5.7'=>'Permission to edit a control, provided that you are responsible for it',
'to_acl_M.RM.5.8'=>'Permission to remove a Control, as long as you are responsible for it',
'to_acl_M.RM.5.9'=>'Change in the revision/test history of the controls of the system',
'to_acl_M.RM.6'=>'Reports tab',
'to_acl_M.RM.6.1'=>'Risk Reports',
'to_acl_M.RM.6.1.1'=>'Risk values report',
'to_acl_M.RM.6.1.2'=>'Risks by process report',
'to_acl_M.RM.6.1.3'=>'Risks by asset report',
'to_acl_M.RM.6.1.4'=>'Risks by control report',
'to_acl_M.RM.6.1.5'=>'Risk impact report',
'to_acl_M.RM.6.1.6'=>'Assets importance report',
'to_acl_M.RM.6.1.7'=>'Risks by Area report',
'to_acl_M.RM.6.1.8'=>'Checklist by Event report',
'to_acl_M.RM.6.2'=>'Control Reports',
'to_acl_M.RM.6.2.1'=>'Controls per responsible report',
'to_acl_M.RM.6.2.10'=>'Follow up of controls test report',
'to_acl_M.RM.6.2.11'=>'Conformity report',
'to_acl_M.RM.6.2.12'=>'Control\'s revision agenda by responsible report',
'to_acl_M.RM.6.2.13'=>'Control\'s test agenda by responsible report',
'to_acl_M.RM.6.2.2'=>'Controls by risk report',
'to_acl_M.RM.6.2.3'=>'Planning of controls report',
'to_acl_M.RM.6.2.4'=>'Controls implementention date report',
'to_acl_M.RM.6.2.5'=>'Controls efficiency report',
'to_acl_M.RM.6.2.6'=>'Controls summary report',
'to_acl_M.RM.6.2.7'=>'Follow up of controls report',
'to_acl_M.RM.6.2.8'=>'Follow up of controls efficiency report',
'to_acl_M.RM.6.2.9'=>'Not measured controls report',
'to_acl_M.RM.6.3'=>'Risk Assessment reports',
'to_acl_M.RM.6.3.1'=>'Risk assessment pending tasks report',
'to_acl_M.RM.6.3.2'=>'Areas without processes report',
'to_acl_M.RM.6.3.3'=>'Assets without risks report',
'to_acl_M.RM.6.3.4'=>'Processes without assets report',
'to_acl_M.RM.6.3.5'=>'Non-estimated risks report',
'to_acl_M.RM.6.3.6'=>'Controls without risks report',
'to_acl_M.RM.6.3.7'=>'Duplicate risks report',
'to_acl_M.RM.6.4'=>'Financial reports',
'to_acl_M.RM.6.4.4'=>'Cost by control report',
'to_acl_M.RM.6.4.5'=>'Cost of controls by responsible report',
'to_acl_M.RM.6.4.6'=>'Cost of controls by area report',
'to_acl_M.RM.6.4.7'=>'Cost of controls by process report',
'to_acl_M.RM.6.4.8'=>'Cost of controls by asset report',
'to_acl_M.RM.6.5'=>'Compliance reports',
'to_acl_M.RM.6.5.1'=>'Statement of aplicability report',
'to_acl_M.RM.6.5.2'=>'Risk treatment report',
'to_acl_M.RM.6.5.3'=>'PCI scope declaration report',
'to_acl_M.RM.6.5.4'=>'PCI policy declaration report',
'to_acl_M.RM.6.6'=>'Summaries reports',
'to_acl_M.RM.6.6.1'=>'Report: top 10 - assets with more risks',
'to_acl_M.RM.6.6.2'=>'Report: top 10 - assets with the most high risks',
'to_acl_M.RM.6.6.3'=>'Report: top 10 - risks by area',
'to_acl_M.RM.6.6.4'=>'Report: top 10 - risks by process',
'to_acl_M.RM.6.6.5'=>'Amount of risks by process report',
'to_acl_M.RM.6.6.6'=>'Amount of risks by area report',
'to_acl_M.RM.6.6.7'=>'Risk status by area report',
'to_acl_M.RM.6.6.8'=>'Risk by process report',
'to_acl_M.RM.6.7'=>'Other reports',
'to_acl_M.RM.6.7.1'=>'Users responsabilities report',
'to_acl_M.RM.6.7.2'=>'Pending items report',
'to_acl_M.RM.6.7.3'=>'Assets report',
'to_acl_M.RM.6.7.4'=>'Item classification report',
'to_acl_M.RM.6.7.5'=>'Report that indicates for each asset its dependencies and its dependents',
'to_acl_M.RM.6.7.6'=>'Report that shows the asset relevance',
'to_acl_M.S'=>'Search tab',
'tr_acl_A'=>'Admin',
'tr_acl_A.A'=>'AUDIT',
'tr_acl_A.A.1'=>'TASKS',
'tr_acl_A.A.2'=>'ALERTS',
'tr_acl_A.A.3'=>'LOG',
'tr_acl_A.C'=>'CUSTOMIZATIONS',
'tr_acl_A.C.1'=>'RISK ESTIMATION PARAMETERS',
'tr_acl_A.C.10'=>'RISK PARAMETERS WEIGHT',
'tr_acl_A.C.11'=>'SYSTEM CLASSIFICATION',
'tr_acl_A.C.2'=>'RISK MATRIX DEFINITION',
'tr_acl_A.C.3'=>'COSTS ESTIMATION PARAMETERS',
'tr_acl_A.C.4'=>'FINANCIAL IMPACT PARAMETERS',
'tr_acl_A.C.5'=>'SYSTEM FEATURES',
'tr_acl_A.C.6'=>'SPECIAL USERS',
'tr_acl_A.C.7'=>'TYPE AND PRIORITY PARAMETERIZATION',
'tr_acl_A.C.7.1'=>'AREA',
'tr_acl_A.C.7.2'=>'PROCESS',
'tr_acl_A.C.7.3'=>'RISK',
'tr_acl_A.C.7.4'=>'EVENT',
'tr_acl_A.C.7.5'=>'CONTROL',
'tr_acl_A.C.7.6'=>'DOCUMENT',
'tr_acl_A.C.7.7'=>'RECORD',
'tr_acl_A.C.8'=>'CONTROL REVISION AND TEST',
'tr_acl_A.C.9'=>'RISK ACCEPTANCE LEVEL',
'tr_acl_A.L'=>'RECYCLE BIN',
'tr_acl_A.L.1'=>'RESTORE',
'tr_acl_A.L.2'=>'REMOVE',
'tr_acl_A.L.3'=>'REMOVE ALL',
'tr_acl_A.MA'=>'MY ACCOUNT',
'tr_acl_A.P'=>'PROFILES',
'tr_acl_A.P.1'=>'EDIT',
'tr_acl_A.P.2'=>'REMOVE',
'tr_acl_A.P.3'=>'INSERT PROFILE',
'tr_acl_A.S'=>'SYSTEM SETTINGS',
'tr_acl_A.S.1'=>'PASSWORD POLICY',
'tr_acl_A.S.2'=>'DELETE CASCADE',
'tr_acl_A.U'=>'USERS',
'tr_acl_A.U.1'=>'EDIT',
'tr_acl_A.U.2'=>'REMOVE',
'tr_acl_A.U.3'=>'INSERT USER',
'tr_acl_A.U.4'=>'RESET PASSWORDS',
'tr_acl_A.U.5'=>'BLOCK USER',
'tr_acl_M'=>'User view',
'tr_acl_M.CI'=>'CONTINUAL IMPROVEMENT',
'tr_acl_M.CI.1'=>'OCCURRENCES',
'tr_acl_M.CI.1.1'=>'LIST',
'tr_acl_M.CI.1.2'=>'INSERT OCCURRENCE',
'tr_acl_M.CI.1.3'=>'EDIT WITHOUT RESTRICTION',
'tr_acl_M.CI.1.4'=>'REMOVE WITHOUT RESTRICTION',
'tr_acl_M.CI.2'=>'INCIDENT',
'tr_acl_M.CI.2.1'=>'LIST',
'tr_acl_M.CI.2.2'=>'INSERT INCIDENT',
'tr_acl_M.CI.2.3'=>'EDIT WITHOUT RESTRICTION',
'tr_acl_M.CI.2.4'=>'REMOVE WITHOUT RESTRICTION',
'tr_acl_M.CI.3'=>'NONCONFORMITY',
'tr_acl_M.CI.3.1'=>'LIST',
'tr_acl_M.CI.3.2'=>'INSERT NONCONFORMITIES',
'tr_acl_M.CI.3.3'=>'EDIT WITHOUT RESTRICTION',
'tr_acl_M.CI.3.4'=>'REMOVE WITHOUT RESTRICTION',
'tr_acl_M.CI.5'=>'DISCIPLINARY PROCESS',
'tr_acl_M.CI.5.1'=>'VIEW ACTION',
'tr_acl_M.CI.5.2'=>'REGISTER ACTION',
'tr_acl_M.CI.5.3'=>'REMOVE',
'tr_acl_M.CI.6'=>'REPORTS',
'tr_acl_M.CI.6.1'=>'INCIDENT REPORTS',
'tr_acl_M.CI.6.1.1'=>'OCCURRENCES BY INCIDENT',
'tr_acl_M.CI.6.1.2'=>'DISCIPLINARY PROCESS',
'tr_acl_M.CI.6.1.3'=>'EVIDENCE COLLECTION',
'tr_acl_M.CI.6.1.4'=>'FOLLOW UP OF OCCURRENCES',
'tr_acl_M.CI.6.1.5'=>'FOLLOW UP OF INCIDENT',
'tr_acl_M.CI.6.1.6'=>'FINANCIAL IMPACT BY INCIDENT',
'tr_acl_M.CI.6.1.7'=>'CONTROL REVISION CAUSED BY INCIDENTS',
'tr_acl_M.CI.6.1.8'=>'ITEMS AFFECTED BY INCIDENTS',
'tr_acl_M.CI.6.1.9'=>'AUTOMATIC CALCULATION OF RISK LIKELIHOOD',
'tr_acl_M.CI.6.2'=>'NONCONFORMITIES REPORT',
'tr_acl_M.CI.6.2.1'=>'NONCONFORMITY BY PROCESS',
'tr_acl_M.CI.6.2.2'=>'NONCONFORMITY OF CONTROL BY PROCESS',
'tr_acl_M.CI.6.2.3'=>'FOLLOW UP OF NONCONFORMITY',
'tr_acl_M.CI.6.2.4'=>'ACTION PLAN BY USER',
'tr_acl_M.CI.6.2.5'=>'ACTION PLAN BY PROCESS',
'tr_acl_M.CI.6.3'=>'CONTINUAL IMPROVEMENT REPORTS',
'tr_acl_M.CI.6.3.1'=>'CONTINUAL IMPROVEMENT PENDING TASKS',
'tr_acl_M.CI.6.3.2'=>'INCIDENTS WITHOUT RISKS',
'tr_acl_M.CI.6.3.3'=>'NONCONFORMITY WITHOUT ACTION PLAN',
'tr_acl_M.CI.6.3.4'=>'ACTION PLAN WITHOUT DOCUMENT',
'tr_acl_M.CI.6.3.5'=>'INCIDENTS WITHOUT OCCURRENCES',
'tr_acl_M.CI.7'=>'ACTION PLAN',
'tr_acl_M.CI.7.1'=>'LIST',
'tr_acl_M.CI.7.2'=>'INSERT',
'tr_acl_M.CI.7.3'=>'EDIT WITHOUT RESTRICTION',
'tr_acl_M.CI.7.4'=>'REMOVE WITHOUT RESTRICTION',
'tr_acl_M.D'=>'DASHBOARD',
'tr_acl_M.D.1'=>'SUMMARIES',
'tr_acl_M.D.2'=>'ADVANCED SUMMARY',
'tr_acl_M.D.3'=>'GRAPHICS',
'tr_acl_M.D.6'=>'STATISTICS',
'tr_acl_M.D.6.1'=>'AREA GRAPHICS',
'tr_acl_M.D.6.10'=>'SYSTEM STATUS',
'tr_acl_M.D.6.11'=>'POLICY MANAGEMENT STATISTICS',
'tr_acl_M.D.6.12'=>'CONTINUAL IMPROVEMENT STATISTICS',
'tr_acl_M.D.6.13'=>'ACTION PLAN STATISTICS',
'tr_acl_M.D.6.2'=>'PROCESS GRAPHICS',
'tr_acl_M.D.6.3'=>'ASSET GRAPHICS',
'tr_acl_M.D.6.4'=>'RISKS SUMMARY',
'tr_acl_M.D.6.5'=>'REQUIREMENT SUMMARY',
'tr_acl_M.D.6.6'=>'CONTROL SUMMARY',
'tr_acl_M.D.6.7'=>'ABNORMALITIES',
'tr_acl_M.D.6.8'=>'FINANCIAL STATISTICS',
'tr_acl_M.D.6.9'=>'COVERAGE IN MANAGEMENT LEVEL',
'tr_acl_M.L'=>'PCI DSS',
'tr_acl_M.L.1'=>'EVENTS LIBRARY',
'tr_acl_M.L.1.1'=>'OPEN CATEGORY',
'tr_acl_M.L.1.2'=>'EDIT CATEGORY',
'tr_acl_M.L.1.3'=>'REMOVE CATEGORY',
'tr_acl_M.L.1.4'=>'ASSOCIATE REQUIREMENTS',
'tr_acl_M.L.1.5'=>'EDIT EVENT',
'tr_acl_M.L.1.6'=>'REMOVE EVENT',
'tr_acl_M.L.1.7'=>'INSERT CATEGORY',
'tr_acl_M.L.1.8'=>'INSERT EVENT',
'tr_acl_M.L.1.9'=>'ASSOCIATE EVENT',
'tr_acl_M.L.2'=>'REQUIREMENTS LIBRARY',
'tr_acl_M.L.2.1'=>'OPEN SECTION',
'tr_acl_M.L.2.10'=>'CREATE REQUIREMENT TEMPLATE',
'tr_acl_M.L.2.2'=>'EDIT SECTION',
'tr_acl_M.L.2.3'=>'REMOVE SECTION',
'tr_acl_M.L.2.4'=>'ASSOCIATE EVENTS',
'tr_acl_M.L.2.5'=>'EDIT REQUIREMENT',
'tr_acl_M.L.2.6'=>'REMOVE REQUIREMENT',
'tr_acl_M.L.2.7'=>'INSERT SECTION',
'tr_acl_M.L.2.8'=>'INSERT REQUIREMENT',
'tr_acl_M.L.2.9'=>'VIEW REQUIREMENT DOCUMENT TEMPLATES',
'tr_acl_M.L.3'=>'STANDARDS LIBRARY',
'tr_acl_M.L.3.1'=>'EDIT STANDARDS',
'tr_acl_M.L.3.2'=>'REMOVE STANDARDS',
'tr_acl_M.L.3.3'=>'INSERT STANDARD',
'tr_acl_M.L.4'=>'IMPORT / EXPORT',
'tr_acl_M.L.4.1'=>'EXPORT',
'tr_acl_M.L.4.2'=>'IMPORT',
'tr_acl_M.L.5'=>'DOCUMENT TEMPLATES LIBRARY',
'tr_acl_M.L.5.1'=>'EDIT DOCUMENT TEMPLATE',
'tr_acl_M.L.5.2'=>'REMOVE DOCUMENT TEMPLATE',
'tr_acl_M.L.5.3'=>'INSERT DOCUMENT TEMPLATE',
'tr_acl_M.L.6'=>'CATEGORIES LIBRARY',
'tr_acl_M.L.6.1'=>'INSERT CATEGORY',
'tr_acl_M.L.6.2'=>'EDIT CATEGORY',
'tr_acl_M.L.6.3'=>'REMOVE CATEGORY',
'tr_acl_M.L.6.4'=>'INSERT SOLUTION',
'tr_acl_M.L.6.5'=>'EDIT SOLUTION',
'tr_acl_M.L.6.6'=>'REMOVE SOLUTION',
'tr_acl_M.PM'=>'POLICY MANAGEMENT',
'tr_acl_M.PM.10'=>'DOCUMENTS IN DEVELOPMENT',
'tr_acl_M.PM.10.1'=>'INSERT DOCUMENT',
'tr_acl_M.PM.11'=>'DOCUMENTS IN APPROVAL',
'tr_acl_M.PM.12'=>'PUBLISHED DOCUMENTS',
'tr_acl_M.PM.13'=>'DOCUMENTS IN REVISION',
'tr_acl_M.PM.14'=>'OBSOLETE DOCUMENTS',
'tr_acl_M.PM.15'=>'ALL DOCUMENTS',
'tr_acl_M.PM.16'=>'RECORDS',
'tr_acl_M.PM.17'=>'REPORTS',
'tr_acl_M.PM.17.1'=>'OTHERS',
'tr_acl_M.PM.17.1.16'=>'DOCUMENT PROPERTIES',
'tr_acl_M.PM.17.1.17'=>'CLASSIFICATION REPORT',
'tr_acl_M.PM.17.2'=>'GENERAL REPORTS',
'tr_acl_M.PM.17.2.1'=>'DOCUMENTS',
'tr_acl_M.PM.17.2.2'=>'DOCUMENTS BY AREA',
'tr_acl_M.PM.17.2.3'=>'DOCUMENTS BY PCI COMPONENT',
'tr_acl_M.PM.17.2.4'=>'DOCUMENTS COMMENTS AND REASONS',
'tr_acl_M.PM.17.2.5'=>'USER COMMENTS',
'tr_acl_M.PM.17.3'=>'RECORD REPORTS',
'tr_acl_M.PM.17.3.1'=>'RECORD BY DOCUMENT',
'tr_acl_M.PM.17.4'=>'ACCESS CONTROL REPORTS',
'tr_acl_M.PM.17.4.1'=>'USER ACCESS AUDIT',
'tr_acl_M.PM.17.4.2'=>'ACCESS TO DOCUMENTS AUDIT',
'tr_acl_M.PM.17.4.3'=>'DOCUMENTS AND THEIR APPROVERS',
'tr_acl_M.PM.17.4.4'=>'DOCUMENTS AND THEIR READERS',
'tr_acl_M.PM.17.4.5'=>'USERS ASSOCIATED TO PROCESSES',
'tr_acl_M.PM.17.5'=>'MANAGEMENT REPORTS',
'tr_acl_M.PM.17.5.1'=>'DOCUMENTS WITH PENDING READING',
'tr_acl_M.PM.17.5.2'=>'MOST ACCESSED DOCUMENTS',
'tr_acl_M.PM.17.5.3'=>'DOCUMENTS NOT ACCESSED',
'tr_acl_M.PM.17.5.4'=>'MOST REVIEWED DOCUMENTS',
'tr_acl_M.PM.17.5.5'=>'PENDING APPROVALS',
'tr_acl_M.PM.17.5.6'=>'DOCUMENTS CREATION AND REVISION DATE',
'tr_acl_M.PM.17.5.7'=>'DOCUMENTS REVISION SCHEDULE',
'tr_acl_M.PM.17.5.8'=>'DOCUMENTS WITH DELAYED REVISION',
'tr_acl_M.PM.17.6'=>'ABNORMALITY REPORTS',
'tr_acl_M.PM.17.6.1'=>'COMPONENTS WITHOUT DOCUMENTS',
'tr_acl_M.PM.17.6.2'=>'PENDING TASKS',
'tr_acl_M.PM.17.6.3'=>'DOCUMENTS WITH A HIGH FREQUENCY OF REVISION',
'tr_acl_M.PM.17.6.4'=>'DOCUMENTS WITHOUT RECORDS',
'tr_acl_M.PM.17.6.5'=>'USERS WITH PENDING READING',
'tr_acl_M.PM.18'=>'DOCUMENTS TO BE PUBLISHED',
'tr_acl_M.RM'=>'RISK ASSESSMENT',
'tr_acl_M.RM.1'=>'AREA',
'tr_acl_M.RM.1.1'=>'LIST ALL',
'tr_acl_M.RM.1.2'=>'VIEW SUB-AREA',
'tr_acl_M.RM.1.3'=>'VIEW PROCESSES',
'tr_acl_M.RM.1.4'=>'EDIT',
'tr_acl_M.RM.1.5'=>'REMOVE',
'tr_acl_M.RM.1.6'=>'INSERT AREA',
'tr_acl_M.RM.1.7'=>'INSERT FIRST LEVEL AREA',
'tr_acl_M.RM.1.8'=>'EDIT IF RESPONSIBLE',
'tr_acl_M.RM.1.9'=>'REMOVE IF RESPONSIBLE',
'tr_acl_M.RM.2'=>'PROCESS',
'tr_acl_M.RM.2.1'=>'LIST ALL',
'tr_acl_M.RM.2.2'=>'VIEW ASSETS',
'tr_acl_M.RM.2.4'=>'EDIT',
'tr_acl_M.RM.2.5'=>'REMOVE',
'tr_acl_M.RM.2.6'=>'INSERT PROCESS',
'tr_acl_M.RM.2.7'=>'EDIT IF RESPONSIBLE',
'tr_acl_M.RM.2.8'=>'REMOVE IF RESPONSIBLE',
'tr_acl_M.RM.3'=>'ASSET',
'tr_acl_M.RM.3.1'=>'LIST ALL',
'tr_acl_M.RM.3.10'=>'LOAD RISKS FROM LIBRARY',
'tr_acl_M.RM.3.2'=>'VIEW RISKS',
'tr_acl_M.RM.3.4'=>'NEW RISK',
'tr_acl_M.RM.3.5'=>'EDIT',
'tr_acl_M.RM.3.6'=>'REMOVE',
'tr_acl_M.RM.3.7'=>'INSERT ASSET',
'tr_acl_M.RM.3.8'=>'EDIT IF RESPONSIBLE',
'tr_acl_M.RM.3.9'=>'REMOVE IF RESPONSIBLE',
'tr_acl_M.RM.4'=>'RISK',
'tr_acl_M.RM.4.1'=>'LIST ALL',
'tr_acl_M.RM.4.10'=>'REMOVE IF RESPONSIBLE',
'tr_acl_M.RM.4.2'=>'VIEW CONTROLS',
'tr_acl_M.RM.4.4'=>'EDIT',
'tr_acl_M.RM.4.5'=>'REMOVE',
'tr_acl_M.RM.4.6'=>'INSERT RISK',
'tr_acl_M.RM.4.8'=>'ESTIMATE',
'tr_acl_M.RM.4.9'=>'EDIT IF RESPONSIBLE',
'tr_acl_M.RM.5'=>'CONTROL',
'tr_acl_M.RM.5.1'=>'LIST ALL',
'tr_acl_M.RM.5.2'=>'VIEW RISKS',
'tr_acl_M.RM.5.3'=>'ASSOCIATE RISKS',
'tr_acl_M.RM.5.4'=>'EDIT',
'tr_acl_M.RM.5.5'=>'REMOVE',
'tr_acl_M.RM.5.6'=>'INSERT CONTROL',
'tr_acl_M.RM.5.7'=>'EDIT IF RESPONSIBLE',
'tr_acl_M.RM.5.8'=>'REMOVE IF RESPONSIBLE',
'tr_acl_M.RM.5.9'=>'REVISION/TEST HISTORY',
'tr_acl_M.RM.6'=>'REPORTS',
'tr_acl_M.RM.6.1'=>'OF RISKS',
'tr_acl_M.RM.6.1.1'=>'RISK VALUES',
'tr_acl_M.RM.6.1.2'=>'RISKS BY PROCESS',
'tr_acl_M.RM.6.1.3'=>'RISKS BY ASSET',
'tr_acl_M.RM.6.1.4'=>'RISKS BY CONTROL',
'tr_acl_M.RM.6.1.5'=>'RISK IMPACT',
'tr_acl_M.RM.6.1.6'=>'ASSETS IMPORTANCE',
'tr_acl_M.RM.6.1.7'=>'RISKS BY AREA',
'tr_acl_M.RM.6.1.8'=>'CHECKLIST BY EVENT',
'tr_acl_M.RM.6.2'=>'OF CONTROLS',
'tr_acl_M.RM.6.2.1'=>'CONTROLS BY RESPONSIBLE',
'tr_acl_M.RM.6.2.10'=>'FOLLOW UP OF CONTROLS TESTING',
'tr_acl_M.RM.6.2.11'=>'CONFORMITY',
'tr_acl_M.RM.6.2.12'=>'CONTROL\'S REVISION AGENDA BY RESPONSIBLE',
'tr_acl_M.RM.6.2.13'=>'CONTROL\'S TEST AGENDA BY RESPONSIBLE',
'tr_acl_M.RM.6.2.2'=>'CONTROLS BY RISK',
'tr_acl_M.RM.6.2.3'=>'PLANNING OF CONTROLS',
'tr_acl_M.RM.6.2.4'=>'CONTROLS IMPLEMENTATION DATE',
'tr_acl_M.RM.6.2.5'=>'CONTROLS EFFICIENCY',
'tr_acl_M.RM.6.2.6'=>'CONTROLS SUMMARY',
'tr_acl_M.RM.6.2.7'=>'FOLLOW UP OF CONTROLS',
'tr_acl_M.RM.6.2.8'=>'FOLLOW UP OF CONTROLS EFFICIENCY',
'tr_acl_M.RM.6.2.9'=>'NOT MEASURED CONTROLS',
'tr_acl_M.RM.6.3'=>'OF RISK ASSESSMENT',
'tr_acl_M.RM.6.3.1'=>'PENDING TASKS',
'tr_acl_M.RM.6.3.2'=>'AREAS WITHOUT PROCESSES',
'tr_acl_M.RM.6.3.3'=>'ASSETS WITHOUT RISKS',
'tr_acl_M.RM.6.3.4'=>'PROCESSES WITHOUT ASSETS',
'tr_acl_M.RM.6.3.5'=>'NON-ESTIMATED RISKS',
'tr_acl_M.RM.6.3.6'=>'CONTROLS WITHOUT RISKS',
'tr_acl_M.RM.6.3.7'=>'DUPLICATE RISKS',
'tr_acl_M.RM.6.4'=>'FINANCIAL',
'tr_acl_M.RM.6.4.4'=>'COST BY CONTROL',
'tr_acl_M.RM.6.4.5'=>'COST OF CONTROLS BY RESPONSIBLE',
'tr_acl_M.RM.6.4.6'=>'COST OF CONTROLS BY AREA',
'tr_acl_M.RM.6.4.7'=>'COST OF CONTROLS BY PROCESS',
'tr_acl_M.RM.6.4.8'=>'COST OF CONTROLS BY ASSET',
'tr_acl_M.RM.6.5'=>'COMPLIANCE',
'tr_acl_M.RM.6.5.1'=>'STATEMENT OF APLICABILITY',
'tr_acl_M.RM.6.5.2'=>'RISK TREATMENT PLAN',
'tr_acl_M.RM.6.5.3'=>'PCI SCOPE DECLARATION',
'tr_acl_M.RM.6.5.4'=>'PCI POLICY DECLARATION',
'tr_acl_M.RM.6.6'=>'SUMMARIES',
'tr_acl_M.RM.6.6.1'=>'TOP 10 - ASSETS WITH MORE RISKS',
'tr_acl_M.RM.6.6.2'=>'TOP 10 - ASSETS WITH THE MOST HIGH RISKS',
'tr_acl_M.RM.6.6.3'=>'TOP 10 - RISKS BY AREA',
'tr_acl_M.RM.6.6.4'=>'TOP 10 - RISKS BY PROCESS',
'tr_acl_M.RM.6.6.5'=>'AMOUNT OF RISKS BY PROCESS',
'tr_acl_M.RM.6.6.6'=>'AMOUNT OF RISKS BY AREA',
'tr_acl_M.RM.6.6.7'=>'RISK STATUS PER AREA',
'tr_acl_M.RM.6.6.8'=>'RISK STATUS BY PROCESS',
'tr_acl_M.RM.6.7'=>'OTHERS',
'tr_acl_M.RM.6.7.1'=>'USERS RESPONSABILITIES',
'tr_acl_M.RM.6.7.2'=>'PENDING ITEMS',
'tr_acl_M.RM.6.7.3'=>'ASSETS',
'tr_acl_M.RM.6.7.4'=>'ITEM CLASSIFICATION',
'tr_acl_M.RM.6.7.5'=>'ASSETS - DEPENDENCIES AND DEPENDENTS',
'tr_acl_M.RM.6.7.6'=>'ASSET RELEVANCE',
'tr_acl_M.S'=>'SEARCH',

/* './packages/admin/cfg_general.xml' */

'lb_session_expiracy'=>'Session expiration time:',
'st_minutes'=>'minutes.',
'to_session_expiracy'=>'<b>Session expiration time:</b><br/><br/>Defines how much inactivity time the system waits, before the session of the logged user is expired.',
'vb_deactivate'=>'Deactivate',

/* './packages/admin/custom_asset_values_name.xml' */

'st_asset_parametrization_cl_bl'=>'<b>Asset Importance Estimation Label:</b>',
'st_importance'=>'Importance (Asset)',
'tt_asset_values_name_information'=>'How do you want to label your asset importance estimation parameters?',

/* './packages/admin/custom_control.xml' */

'cb_revision'=>'Revision',
'cb_test'=>'Test',
'st_revision_and_test'=>'<b>Control Revision and Test</b>',
'st_revision_and_test_description'=>'Do you want to review your security controls?',
'to_control_revision_config_bl_cl'=>'<b>Control Revision Configuration:</b><br/><br/> Determines if the system will allow the revision of the real efficiency of controls.<br/>This configuration will let you evaluate whether a control is efficient or not.',
'to_control_test_config_bl_cl'=>'<b>Tests Control Configuration:</b> <br/><br/>Determines if the system will allow the test of the controls.<br/>This configuration will let you evaluate whether a control is reliable or not.',

/* './packages/admin/custom_control_cost_name.xml' */

'lb_cost_name_1_bl'=>'<b>Cost Name 1</b>',
'lb_cost_name_2_bl'=>'<b>Cost Name 2</b>',
'lb_cost_name_3_bl'=>'<b>Cost Name 3</b>',
'lb_cost_name_4_bl'=>'<b>Cost Name 4</b>',
'lb_cost_name_5_bl'=>'<b>Cost Name 5</b>',
'st_costs_estimation_parameters_information'=>'What are the categories of costs that you normally count in the application of controls?',
'static_st_cost_estimation_parameters_bl'=>'<b> Costs Estimation Parameters</b>',

/* './packages/admin/custom_currency.xml' */

'lb_currency_cl'=>'Currency:',
'si_dollar'=>'United States, US Dollar, USD',
'si_euro'=>'Europe, Euro, EUR',
'si_pesos'=>'Argentina, Pesos, ARS',
'si_pounds'=>'England, Pounds, GBP',
'si_reais'=>'Brazil, Reais, BRL',
'si_yen'=>'Japan, Yen, JPY',
'si_yuan'=>'China, Yuan Renminbi, CNY',
'st_system_currency_bl'=>'<b>System Currency</b>',
'st_system_currency_information'=>'Which currency should be used in the system?',
'to_general_settings_currency'=>'<b>Currency:</b> <br/> <br/> Configures the currency shown in the system screens. Please note that no conversion is made when the currency is changed.',

/* './packages/admin/custom_financial_impact.xml' */


/* './packages/admin/custom_financial_impact_parameters.xml' */

'st_custom_financial_impact_parameter_note'=>'Enter the name of the categories on which will be assessed the financial impact of an incident.',
'st_description_financial_impact_parameter'=>'Real PCI enables you to define the source of possible financial losses associated with an event of risk. What are the categories for financial source that you want to use?',
'st_financial_impact_parameters'=>'<b>Financial Impact Parameters</b>',

/* './packages/admin/custom_impact_damage.xml' */


/* './packages/admin/custom_impact_type.xml' */


/* './packages/admin/custom_priority.xml' */


/* './packages/admin/custom_quantity_rm_parameters.xml' */

'lb_mode_cl'=>'Mode:',
'rb_3_three'=>'3 (three)',
'rb_5_five'=>'5 (five)',
'rb_optimist'=>'Optimist',
'rb_pessimist'=>'Pessimist',
'st_custom_quantity_parameters_note'=>'OBS.: You can change the settings in the future, but it will be necessary to revaluate some risk values.',
'st_risk_quantity_values_bl_cl'=>'<b>Risk Matrix Definition:</b>',
'tt_custom_quantity_rm_parameters_information'=>'Requirements recommend that you give importance, impact and likelihood in a 3 x 3 matrix.<br/> Some organizations choose to use a 5 x 5 matrix.<br/> This is a decision you must take before starting the risk assessment.<br/> Using a 3 x 3 matrix, the risks will have values between 1 and 9. If you choose the 5 x 5 matrix, the risks will have values between 1 and 25.<br/><br/> How do you want to scale your risk matrix?',

/* './packages/admin/custom_report_classification.xml' */

'lb_to_cl'=>'To:',
'st_report_classification'=>'<b>Report Classification</b>',
'st_report_classification_information'=>'What is the Classification (and to whom this classification applies) you want to use in the system tabs?',
'st_system_classification_bl'=>'<b>System Classification</b>',
'st_system_classification_information'=>'What kind of classification (and to whom this classification applies) do you want to use in the system?',
'to_general_settings_classification'=>'<b>Classification:</b><br/>Indicates, in the footer of the system, its classification. E.g.: Confidential, Secret, etc.',
'to_general_settings_classification_dest'=>'<b>To:</b><br/>Indicates to whom the classification above applies. This field will take effect only when the classification has been filled.',

/* './packages/admin/custom_risk_control_values_name.xml' */

'st_rc_impact'=>'Impact',
'st_rc_impact5_'=>'Impact',
'st_rc_probability'=>'Likelihood',
'st_risk_control_values_name'=>'<b>Control Reduction Criteria Label</b>',
'st_very_high'=>'<b>Very High</b>',
'st_very_low'=>'<b>Very Low</b>',
'tt_risk_control_value_name_information'=>'When you apply controls to reduce risk, they reduce the likelihood or the impact related to these risks. How do you want to label these reductions in impact and likelihood?',

/* './packages/admin/custom_risk_limits.xml' */

'lb_risk_limits_high_bl_cl'=>'<b>Risk Tolerance 2:</b>',
'lb_risk_limits_low_bl_cl'=>'<b>Risk Tolerance 1:</b>',
'st_risk_limits_definition'=>'Define the values for risk handling.<br/><br/> Risks whose calculated value is lower than Tolerance Level 1 will be highlighted with green color.<br/>Risks whose calculated value is higher than Tolerance Level 2 will be highlighted with red color.<br/>Risks whose calculated value is between these two Tolerance Levels will be highlighted with yellow color.',
'tt_aceptable_risk_limits'=>'<b>Acceptable Levels of Risk</b>',
'tt_risk_limits'=>'Risk Tolerance',
'wn_high_higher_than_max'=>'The limit value of risk 2 cannot exceed the system configuration limit.',
'wn_low_higher_than_high'=>'The limit value of risk 1 cannot be greater than the limit value of risk 2.',
'wn_low_lower_than_1'=>'The limit value of risk 1 cannot be lower than 1.',
'wn_save_risk_matrix_update_risk_limits'=>'The system generated a task to user <b>%chairman_name%</b> for the approval of the new risk limits. Until this user approves the new limits, the system will use the old risk limits.',

/* './packages/admin/custom_risk_parameters_names.xml' */

'st_risk_parameters_bl_cl'=>'<b>Risk Estimation Parameters:</b>',
'st_risk_parameters_help'=>'Enter the names of the properties that will become available in the software for risk evaluation. We suggest the following properties: confidentiality, integrity and availability; and yet other properties such as authenticity, responsibility, non-repudiation and reliability. Or as you prefer.',
'tt_risk_parameters_information'=>'How do you label your Risk Estimation Parameters?',
'wn_risk_parameters_minimum_requirement'=>'At least one Risk Parameter must be specified.',

/* './packages/admin/custom_risk_parameters_weight.xml' */

'st_risk_parameters_weight'=>'<b>Risk Parameters Weight</b>',
'st_risk_parameters_weight_definition'=>'Define the weights for each risk parameter. The weights must be greater than or equal to zero, and at least one weight must be positive. The values of risks and assets will be updated according to the changes in the current weights.',
'wn_at_least_one_positive'=>'At least one weight must be greater than zero.',

/* './packages/admin/custom_risk_values_name.xml' */

'st_custom_risk_matrix_definition_generic_note'=>'OBS.: first select the risk matrix size (3x3 or 5x5) in topic "Risk Matrix Definition".',
'st_high'=>'<b>High</b>',
'st_high_bl'=>'<b>High</b>',
'st_impact'=>'Impact (Risk)',
'st_impact_risk'=>'Impact (Risk)',
'st_low'=>'<b>Low</b>',
'st_low_bl'=>'<b>Low</b>',
'st_medium'=>'<b>Medium</b>',
'st_medium_bl'=>'<b>Medium</b>',
'st_probability_risk'=>'Likelihood (Risk)',
'st_risk_parametrization'=>'<b>Impact & Likelihood Estimation Label:</b>',
'st_riskprob'=>'Likelihood (Risk)',
'st_values'=>'Values',
'st_very_high_bl'=>'<b>Very High</b>',
'st_very_low_bl'=>'<b>Very Low</b>',
'tt_risk_values_name_information'=>'How do you want to label your Impact and Likelihood estimation criteria?',

/* './packages/admin/custom_special_users.xml' */

'lb_asset_manager_bl'=>'<b>Asset Manager</b>',
'lb_chairman_bl'=>'<b>Management</b>',
'lb_control_manager_bl'=>'<b>Control Manager</b>',
'lb_library_manager_bl'=>'<b>PCI DSS Manager</b>',
'lb_user_disciplinary_process_manager_bl'=>'<b>Disciplinary Process Manager</b>',
'lb_user_evidence_manager_bl'=>'<b>Evidence Manager</b>',
'lb_user_incident_manager_bl'=>'<b>Incident Manager</b>',
'lb_user_non_conformity_manager_bl'=>'<b>Nonconformity Manager</b>',
'st_custom_special_users_information'=>'The Real PCI workflow requires the definition of some users responsible for special tasks pre-determined in the context of a PCI. Who will occupy the role of these users in your system?',
'st_help'=>'[?]',
'st_special_users_bl'=>'<b>Real PCI Special Users</b>',
'tt_special_user'=>'<b>Management:</b><br/><br/>user responsible for the approval of the first business areas, risk acceptance, and other aspects that require the Management\'s approval.',
'tt_special_user_asset_manager'=>'<b>Asset Manager:</b><br/><br/>user responsible for the control of the inventory of assets',
'tt_special_user_control_manager'=>'<b>Control Manager:</b><br/><br/>user responsible for managing the security controls, approving the creation of new controls, and organizing controls in Real PCI.',
'tt_special_user_disciplinary_process_manager'=>'<b>Disciplinary Process Manager:</b><br/><br/>user who receives Disciplinary Process alerts',
'tt_special_user_evidence_manager'=>'<b>Evidence Manager:</b><br/><br/>user who receives evidence collection alerts',
'tt_special_user_incident_manager'=>'<b>Incident Manager:</b><br/><br/>user responsible for approving, editing and removing occurrences, as well as for creating, editing and removing incidents<br/><br/>Only this user will be able to create disciplinary processes, as well as associate risks or processes to the incident.',
'tt_special_user_library_manager'=>'<b>PCI DSS Manager:</b><br/><br/>user responsible for the control of Real PCI libraries, by verifying new events, new requirements and security standards',
'tt_special_user_nc_manager'=>'<b>Nonconformity Manager:</b><br/><br/>user responsible for approving the nonconformities created by other Real PCI users.',

/* './packages/admin/custom_system_features.xml' */

'cb_auto_document_creation'=>'Generate the documents automatically after the creation of elements in the system?',
'cb_cost'=>'Use Costs Estimation Features?',
'cb_document_manual_versioning'=>'Do you want to use Document Manual Versioning?',
'cb_percentual_risk'=>'Show Risks Values in 0 to 100 scale?',
'cb_solicitor'=>'Allow attorney to share access rights, privileges and responsibilities?',
'st_data_collecting'=>'Enable Data Collection with daily periodicity to generate Real PCI statistics.',
'st_manual_data_control'=>'Do you want to use Manual Control of Dates?',
'st_risk_formula'=>'Use Proportional Risk Formula.',
'st_system_features'=>'<b>System Features</b>',
'to_data_collecting_settings'=>'<b>Data Collecting Settings:</b><br/><br/>Sets whether the system will collect database information to generate system history shown on statistics tab.',
'to_delete_cascade'=>'<b>Manual Control of System Dates:</b><br/><br/>Allows the user to define dates in the past and/or in the future in parts of system in which, by default, this kind of flexibility is not permitted.',
'to_document_manual_versioning'=>'<b>Document Manual Versioning:</b><br/><br/>Determines whether the system will use manual or automatic versioning of documents.',
'to_general_settings_automatic_documents_creation'=>'<b>Automatic Documents Creation Settings:</b><br/><br/>Sets whether the system will automatically create a document for each item (area, process, asset and control) created by the user.',
'to_percentual_risk_configuration'=>'<b>Risk values scale configuration:</b><br/><br/>Determines if the risk values should be exhibited in absolute value (0 to 9 or 0 to 25) or in scale from 0 to 100.',
'to_procurator_configuration'=>'<b>Attorney Configuration:</b><br/><br/>Determine if the system will allow the Procurement mechanism in the temporary absence of a user.',
'to_risk_formula'=>'<b>Proportional Risk Formula:</b><br/><br/>Formula that considers the weights of importance and consequence to increase the final values for each risk.',
'to_system_cost_configuration'=>'<b>Cost Configuration:</b><br/><br/>Determine if the system will enable costs calculation.',

/* './packages/admin/custom_type_and_priority_parametrization.xml' */

'si_document'=>'Document',
'si_event'=>'Event',
'si_register'=>'Record',
'si_risk'=>'Risk',
'st_description_type_and_priority_parametrization'=>'How do you label your elements?',
'st_element'=>'<b>Element:</b>',
'st_priority_bl'=>'<b>Priority</b>',
'st_type_and_priority_parametrization'=>'<b>Type and Priority Parameterization</b>',
'st_type_bl'=>'<b>Type</b>',
'wn_classification_minimum_requirement'=>'At least three classifications must be specified for each item.',

/* './packages/admin/nav_audit_alert.xml' */

'gc_justification'=>'Reason',
'tt_audit_alerts_bl'=>'<b>Audit Alerts</b>',

/* './packages/admin/nav_audit_log.xml' */

'lb_user_cl'=>'User:',
'si_all_actions'=>'All Actions',
'si_all_users'=>'All Users',
'tt_audit_log_bl'=>'<b>Audit Log</b>',

/* './packages/admin/nav_audit_task.xml' */

'gc_accomplishment'=>'Accomplishment',
'gc_creation'=>'Creation',
'gc_receiver'=>'Receiver',
'lb_activity_cl'=>'Activity:',
'lb_creator_cl'=>'Creator:',
'lb_receiver_cl'=>'Receiver:',
'si_all_activities'=>'All Activities',
'si_all_creators'=>'All Creators',
'si_all_receivers'=>'All Receivers',
'tt_audit_task_bl'=>'<b>Audit Task</b>',

/* './packages/admin/nav_config.php' */

'st_check_for_correct_data'=>'Check if the data were filled out correctly.',
'st_impact_parameter_remove_message'=>'The removal of a financial impact parameter will cause the removal of this parameter in the incidents already estimated. Do you wish to proceed anyway?',
'st_info_saved_but'=>'The information has been saved. However,',
'st_not_possible_to_update_data_collection_info'=>'it was not possible to update the data collection information',
'st_not_possible_to_update_mail_server_info'=>'it was not possible to update the email server  information',
'st_unavailable_information'=>'unavailable information.',
'tt_alert'=>'Alert',
'tt_deactivate_system'=>'Deactivate System',
'tt_impact_parameter_remove'=>'Remove Financial Impact Parameter',

/* './packages/admin/nav_config.xml' */

'si_costs_names'=>'Costs Names',
'si_parametrization'=>'Estimation',
'si_risk_parameters_values'=>'Risk Parameters Values',
'si_syslog'=>'Syslog',
'si_system'=>'System',

/* './packages/admin/nav_customization.php' */

'st_change_amount_of_parameters_confirm'=>'Changing the amount of parameters will make the system become not estimated.<BR><b> Are you sure you wish to do this?</b>',
'st_change_amount_of_risks_confirm'=>'Changing the amount of risks will cause the system to redo all its risk calculations!<BR><b>Are you sure you wish to do this?</b>',
'st_remove_classification_error'=>'Your changes have been saved. However, it was not possible to remove the following classifications, as they are being used by one or more items: <b>%not_deletable%</b>.',
'st_risk_parameters_weight_edit_confirm'=>'You are changing sensible data. When you change the parameters weight, the values of all risks and assets of the system will be changed accordingly. Do you wish to confirm the changes?',
'tt_remove_classification_error'=>'Error when removing Classification',
'tt_save_parameters_names'=>'Save Parameters Names',
'tt_save_parameters_values'=>'Save Parameters Values',
'tt_sensitive_data'=>'Sensitive Data',

/* './packages/admin/nav_customization.xml' */

'lb_settings_cl'=>'Customization:',
'si_aceptable_risk_limits'=>'Acceptable levels of Risk',
'si_control_cost_names'=>'Costs Estimation Parameters',
'si_control_revision_and_test'=>'Control Revision and Test',
'si_financial_impact_parameters'=>'Financial Impact Parameters',
'si_impact_and_importance_dimensions'=>'Risk estimation parameters',
'si_parametrization_asset'=>'Asset Importance Estimation Label',
'si_parametrization_control'=>'Control reduction criteria label',
'si_parametrization_risk'=>'Impact & Likelihood Estimation Label',
'si_priority_and_type_parametrization'=>'Type and Priority Parameterization',
'si_report_classification'=>'Report Classification',
'si_risk_parameters_weight'=>'Risk Parameters Weight',
'si_special_users'=>'Special Users',
'si_system_classification'=>'System Classification',
'si_system_currency'=>'Currency',
'si_system_features'=>'System Features',
'si_system_rm_parameters_quantity'=>'Risk Matrix Definition',
'wn_updating_risk_values'=>'Changes successfully saved. Wait while the risks values are being recalculated.',
'wn_values_successfully_changed'=>'Values successfully updated.',

/* './packages/admin/nav_my_account.php' */

'st_activation_date'=>'Activation Date',
'st_cancel_service'=>'Service Cancellation',
'st_cancel_service_email_message'=>'A service cancellation request has been made by client \'%client%\'.',
'st_client_name'=>'Client\'s Name',
'st_confirm_cancel_service_message'=>'Are you sure you want to cancel the service?',
'st_deactivate_account_text'=>'To deactivate your account, click on the button below.<br/>Your database will be maintained for 30 days. After that, all data will be fully deleted. To receive this database, contact us at %email%',
'st_expiracy_date'=>'Expiry Date',
'st_expiracy_date_never'=>'Never',
'st_license_type'=>'License Type',
'st_max_simult_users'=>'Max. Simultaneous Users',
'st_number_of_users'=>'Number of Users',
'tt_cancel_service'=>'Service Cancellation',
'tt_confirm_cancel_service'=>'Service Cancellation',
'tt_deactivate_account'=>'Deactivate Account',

/* './packages/admin/nav_my_account.xml' */

'lb_email_cl'=>'E-mail:',
'st_cancel_service_bl'=>'<b>Service cancellation</b>',
'st_change_email_bl'=>'<b>Administrative email change</b>',
'st_deactivate_account_bl'=>'<b>Deactivate Account</b>',
'st_license_info_bl'=>'<b>License Information:</b>',
'st_upgrade_license_bl'=>'<b>License Upgrade</b>',
'st_upgrade_license_message'=>'Please activate your user packs using the activation codes that were sent via e-mail. Put your activation codes below, and press "Activate". It will be delivered instantly!',
'st_upgrade_license_text'=>'Please activate your User Pack (for 10 users) using the Activation Code that was sent via e-mail. Put your Activation Code below, and press "Activate". It will be delivered instantly!',
'vb_activate'=>'Activate',
'vb_cancel_service'=>'Cancel Service',
'vb_change'=>'Change',
'vb_deactivate_account'=>'Deactivate My Account',
'vb_upgrade'=>'Upgrade',
'wn_email_changed'=>'email was changed successfully',
'wn_invalid_activation_code'=>'Some codes are invalid. Please, contact %email%.',
'wn_successful_activation'=>'License successfully updated.',

/* './packages/admin/nav_profile.php' */

'st_profile_remove_message'=>'Are you sure you wish to remove Profile <b>%profile_name%</b>?',
'tt_remove_profile'=>'Remove Profile',

/* './packages/admin/nav_profile.xml' */

'mi_visualize'=>'View',
'tt_profiles_bl'=>'<b>Profiles</b>',

/* './packages/admin/nav_system_configuration_on_site.xml' */

'lb_system_settings_cl'=>'System settings',
'si_delete_cascade'=>'Delete Cascade',
'si_email_setup'=>'E-mail configuration',
'si_password_policy'=>'Password Policy',
'si_server_setup'=>'Server Maintenance',
'si_timezone_setup'=>'Select time zone',
'wn_changes_successfully_saved'=>'Changes were successfully saved.',

/* './packages/admin/nav_trash.php' */

'st_all_items_remove_confirm'=>'All Items in the Recycle Bin will be permanently removed! Do you wish to Continue?',
'st_items_restore_confirm'=>'Do you wish to restore the selected items?',
'st_not_excluded_users_and_profile_message'=>'The selected users could not be removed because they are related to existent itens in the recycle bin. <br/> The selected Profiles could not be removed because they are related to existent users in the recycle bin that were not selected or could not be removed.',
'st_not_removed_profiles_message'=>'The selected Profiles could not be removed because they are related to existent users in the recycle bin that were not selected or could not be removed.',
'st_not_restored_documents_message'=>'The selected documents could not be restored, because they are related to one or more items that remain removed!',
'st_not_restored_items_message'=>'Selected items could not be restored as they are related to one or more users wich remain removed!',
'st_not_restored_users_message'=>'The selected Users could not be removed because they are related to existing itens in the recycle bin.',
'st_not_selected_items_remove_confirm'=>'Items which were not selected and are sub-items or associated to selected items will be removed. Do you wish to continue?',
'tt_all_items_permanent_removal'=>'Permanent removal of all items',
'tt_document_not_restored'=>'Documents non restored',
'tt_items_not_restored'=>'Items not restored',
'tt_items_permanent_removal'=>'Permanent removal of items',
'tt_items_restoral'=>'Items Restoration',
'tt_profile_not_deleted'=>'The profiles were not removed',
'tt_users_and_profile_not_deleted'=>'Users and profiles were not removed',
'tt_users_not_deleted'=>'The Users were not removed',

/* './packages/admin/nav_trash.xml' */

'tt_bin_bl'=>'<b>Recycle Bin</b>',
'vb_remove_all'=>'Remove All',
'wn_elements_removed_successfully'=>'Item(s) removed successfully.',
'wn_elements_restaured_successfully'=>'Item(s) restored successfully.',

/* './packages/admin/nav_users.php' */

'em_alerts_and_tasks_transfer'=>'Transfer of Tasks and Alerts',
'st_choose_user_message'=>'In the next screen, choose an user who will receive the tasks and pending warnings of this user.',
'st_reset_all_users_passwords_confirm'=>'Are you sure you wish to reset the password from  <b>all users</b> and send the new password to their e-mails?',
'st_user_remove_confirm'=>'Are you sure you wish to remove the User <b>%user_name%</b>?',
'tt_choose_user_warning'=>'Warning of User Choice',
'tt_remove_user'=>'Remove User',
'tt_reset_all_users_password'=>'Reset Password of All Users',

/* './packages/admin/nav_users.xml' */

'gc_email'=>'E-mail',
'gc_profile'=>'Profile',
'mi_block'=>'Block',
'mi_unblock'=>'Unblock',
'tt_users_bl'=>'<b>Users</b>',
'vb_reset_passwords'=>'Reset Passwords',

/* './packages/admin/popup_associate_elements.xml' */

'lb_associate_to_cl'=>'Associate to:',
'tt_items_association'=>'Items Association',
'vb_cancel_all'=>'Cancel All',

/* './packages/admin/popup_configure_authentication.php' */

'st_connection_failed'=>'Connection failed.',
'st_database_structure_wrong'=>'Database does not have expected structure.',
'st_successful_connection'=>'Connection Successful.',

/* './packages/admin/popup_configure_authentication.xml' */

'lb_databse_bl_cl'=>'<b>Database:</b>',
'lb_host_bl_cl'=>'<b>Host:</b>',
'lb_password_bl_cl'=>'<b>Password:</b>',
'st_postgres_authentication_data_needed'=>'The data below is required to allow the system to access the database containing information regarding postgres schedule.',
'tt_setup_authentication'=>'Setup Authentication',
'vb_test'=>'Test',

/* './packages/admin/popup_deactivate.php' */

'em_deactivate_account'=>'Account Deactivation',
'em_deactivate_account_email_message'=>'Client \'%client%\' has requested account deactivation.<br><b>Reason:</b><br>%deactivation_reason%',

/* './packages/admin/popup_deactivate.xml' */

'lb_deactivation_reason_cl'=>'Please, let us know why you are deactivating:',
'st_confirm_deactivate_account_message'=>'Are you sure you want to deactivate your account?',
'tt_confirm_deactivate_account'=>'Deactivate Account',

/* './packages/admin/popup_no_access.xml' */

'st_user_wo_system_access'=>'You don\'t have access to the system. Press <b>Ok</b> or close this window to logout.',
'tt_user_wo_system_access'=>'User without permission to access the system',

/* './packages/admin/popup_profile_edit.php' */

'tt_edit_profile'=>'Edit Profile',
'tt_insert_profile'=>'Add Profile',
'tt_visualizae_profile'=>'Show Profile',

/* './packages/admin/popup_profile_edit.xml' */

'st_profile_warning'=>'* This profile can not be edited.',

/* './packages/admin/popup_restore_name_conflict.xml' */

'st_name_conflicts_message'=>'It\'s been detected that one or more selected items presented name conflict, meaning there is an active item on the system that has the same type and name. Which items do you wish to restore?',
'tt_name_conflicts_found'=>'Name Conflicts Found',

/* './packages/admin/popup_restore_structure_and_name_conflict.xml' */

'rb_all_except_name_conflict'=>'All except the ones with name conflict *',
'rb_all_except_structure_conflict'=>'All except the ones with structural conflict',
'st_structure_name_conflicts_message'=>'It has been detected that there is a conflict of structure and name with one or more selected items. A structure conflict occurs when there are unselected items necessary to make the restore of selected items. A name conflict occurs when there is a active system item with the same type and with the same name of a item that will be restored. Which items do you wish to restore?',
'tt_structure_and_name_conflicts_found'=>'Structure and Name Conflicts found',
'wn_not_restoring_name_conflict'=>'* Not restoring items with name conflict may cause structural conflicts.',

/* './packages/admin/popup_restore_structure_conflict.xml' */

'cb_associate_structure_conflicted_items_individually'=>'Individually associate items with structural conflicts to existing items in the system.',
'rb_all'=>'All',
'rb_only_not_conflicting'=>'Only the ones that did not present conflict',
'st_structure_conflicts_message'=>'It\'s been detected that one or more selected items presented structure conflict, which means that to restore them, you would have to restore a structure of unselected items. Which items do you wish to restore?',
'tt_structure_conflicts_found'=>'Structure Conflicts Found',
'vb_restore'=>'Restore',

/* './packages/admin/popup_upgrade_license.xml' */

'lb_activation_code_cl'=>'Activation code:',
'tt_upgrade_license'=>'License Upgrade',

/* './packages/admin/popup_user_edit.php' */

'em_new_password'=>'New Password',
'em_new_user'=>'Welcome to Real PCI',
'em_new_user_footer_bl'=>'<b>For further information, contact the security officer.</b>',
'st_reset_user_password_confirm'=>'Are you sure you wish to reset the user password and send the new password to his e-mail?',
'tt_profile_edit_error'=>'Error while changing the profile',
'tt_reset_user_password'=>'Reset User Password',
'tt_user_adding'=>'User Adding',
'tt_user_editing'=>'User Editing',
'tt_user_insert_error'=>'User limit reached',
'wn_users_limit_reached'=>'The number of licensed users has been reached. From now on only users with the <b>Document Reader</b> profile (users that only have permission to read documents) can be used. Click <a target="_blank" href=%link%>here</a> in order to buy more licenses.',

/* './packages/admin/popup_user_edit.xml' */

'cb_change_password_on_login'=>'Change password on next login',
'lb_confirmation_cl'=>'Confirmation:',
'lb_email_bl_cl'=>'<b>E-mail:</b>',
'lb_language_bl_cl'=>'<b>Language:</b>',
'lb_login_bl_cl'=>'<b>Login:</b>',
'lb_new_password_cl'=>'New Password:',
'lb_profile_bl_cl'=>'<b>Profile:</b>',
'st_language_bl_cl'=>'<b>Language:</b>',
'vb_reset_password'=>'Reset Password',
'wn_different_passwords'=>'Different Passwords',
'wn_existing_email'=>'Email already exists',
'wn_existing_login'=>'Login already exists',
'wn_invalid_email'=>'Invalid e-mail',
'wn_password_changed'=>'Password changed',
'wn_weak_password'=>'Your password doesn\'t fulfill the requirements of the password policy. Please, choose another one.',
'wn_wrong_current_password'=>'Wrong password',

/* './packages/admin/report/popup_report_audit_alert.php' */

'rs_alerts_audit'=>'Alerts Audit',

/* './packages/admin/report/popup_report_audit_log.php' */

'rs_systems_log_audit'=>'System Log Audit',

/* './packages/admin/report/popup_report_audit_task.php' */

'rs_tasks_audit'=>'Tasks Audit',

/* './packages/admin/report/popup_report_audit_task.xml' */

'rs_accomplishment'=>'Accomplishment',
'rs_creation'=>'Creation',
'rs_creator'=>'Creator',
'rs_receiver'=>'Receiver',

/* './packages/admin/system_configuration_delete_cascade.xml' */

'cb_delete_cascade'=>'Delete Cascade',
'st_delete_cascade'=>'<b>Delete Cascade</b>',
'st_system_configuration_delete_cascade_information'=>'Determines if an item removal will cause automatic subitems removal.<br/>If \'Delete Cascade\' is disabled, it will not be possible to remove items that have subitems.<br/><br/>Do you want to use \'Delete Cascade\' in the system?',

/* './packages/admin/system_configuration_email_setup.xml' */

'lb_email_encryption_cl'=>'Encryption type:',
'lb_email_host_cl'=>'SMTP Server Address:',
'lb_email_password_cl'=>'Password:',
'lb_email_port_cl'=>'Port:',
'lb_email_user_cl'=>'User:',
'si_email_none'=>'None',
'si_email_ssl'=>'SSL',
'si_email_ssl2'=>'SSLv2',
'si_email_ssl3'=>'SSLv3',
'si_email_tls'=>'TLS',
'st_configuration_email_config_information'=>'Please configure the e-mail server that will be used by the system.',
'st_email_config_bl_cl'=>'<b>E-mail configuration:</b>',

/* './packages/admin/system_configuration_password_policy.xml' */

'cb_case_characters'=>'Upper/Lowercase Characters',
'cb_numeric_characters_cl'=>'Numeric Characters:',
'cb_special_characters_cl'=>'Special Characters:',
'lb_days_before_warning'=>'Notify password expiration:',
'lb_label_block_limit_cl'=>'Block Limit:',
'lb_minimum_characters_cl'=>'Minimum Length:',
'lb_password_history_apply_cl'=>'Apply password history:',
'lb_password_max_lifetime_cl'=>'Password max lifetime:',
'st_configuration_password_policy_information'=>'What is the password policy that you want to use in the system?',
'st_days_before'=>'day before.',
'st_default_configuration'=>'Reset to default configuration',
'st_invalid_logon'=>'invalid logon attempts',
'st_memorized_passwords'=>'memorized passwords.',
'st_password_policy_bl_cl'=>'<b>Password Policy:</b>',
'wn_min_char_warning'=>'The minimum length must be greater than or equal to the number of special caracters, plus 2 (at least a lower case letter and a capital letter), plus the number of numeric caracters.<br/><br/>Obs: Only the selected options are counted.',

/* './packages/admin/system_configuration_server_setup.xml' */

'lb_server_dns_cl'=>'DNS Server Address:',
'lb_server_gateway_cl'=>'Server Gateway:',
'lb_server_ip_cl'=>'Server IP Address:',
'lb_server_update_cl'=>'Update file:',
'st_configuration_server_config_information'=>'Server parameters setup.',
'st_server_config_bl_cl'=>'<b>Server Maintenance:</b>',

/* './packages/admin/system_configuration_timezone_setup.xml' */

'cb_timezone_setup'=>'Choose time zone:',
'si_timezone_-1'=>'GMT -1',
'si_timezone_-10'=>'GMT -10',
'si_timezone_-11'=>'GMT -11',
'si_timezone_-12'=>'GMT -12',
'si_timezone_-2'=>'GMT -2',
'si_timezone_-3'=>'GMT -3',
'si_timezone_-4'=>'GMT -4',
'si_timezone_-5'=>'GMT -5',
'si_timezone_-6'=>'GMT -6',
'si_timezone_-7'=>'GMT -7',
'si_timezone_-8'=>'GMT -8',
'si_timezone_-9'=>'GMT -9',
'si_timezone_+1'=>'GMT +1',
'si_timezone_+10'=>'GMT +10',
'si_timezone_+11'=>'GMT +11',
'si_timezone_+12'=>'GMT +12',
'si_timezone_+2'=>'GMT +2',
'si_timezone_+3'=>'GMT +3',
'si_timezone_+4'=>'GMT +4',
'si_timezone_+5'=>'GMT +5',
'si_timezone_+6'=>'GMT +6',
'si_timezone_+7'=>'GMT +7',
'si_timezone_+8'=>'GMT +8',
'si_timezone_+9'=>'GMT +9',
'si_timezone_0'=>'GMT',
'st_system_configuration_timezone_setup_information'=>'Determines which time zone will be used in the system.',
'st_timezone_setup'=>'<b>Select Time zone</b>',

/* './packages/admin/tab_audit.xml' */

'ti_alerts'=>'Alerts',
'ti_log'=>'Log',
'ti_tasks'=>'Tasks',

/* './packages/admin/tab_main.php' */

'st_survey_link'=>'http://www.surveymonkey.com/s.aspx?sm=F9BFTdst8Ginn7PdgNGxFA_3d_3d',
'to_ip_cl'=>'IP:',
'to_last_login_cl'=>'Last Login:',

/* './packages/admin/tab_main.xml' */

'lb_feedback'=>'Feedback',
'mx_bug'=>'Bug',
'mx_content_issue'=>'Suggestion',
'mx_feature_request'=>'Improvement',
'st_about_isms_bl'=>'<b>About</b>',
'st_buy_now_bl'=>'<b>Buy Now</b>',
'st_pipe_bl'=>'<b>|</b>',
'st_preferences_bl'=>'<b>Preferences</b>',
'st_satisfaction_survey_bl'=>'<b>User Experience Survey</b>',
'st_send_us_feedback_cl'=>'Leave a feedback:',
'st_software_license_bl'=>'<b>License</b>',
'ti_audit'=>'Audit',
'ti_bin'=>'Recycle Bin',
'ti_customization'=>'Customization',
'ti_my_account'=>'My Account',
'ti_profiles'=>'Profiles',
'ti_system_settings'=>'System Settings',
'ti_users'=>'Users',
'vb_exit'=>'Logout',
'vb_feedback'=>'Feedback',
'vb_help'=>'Help',
'vb_user_view'=>'User view',

/* './packages/continuity/nav_asset.xml' */


/* './packages/continuity/nav_place_threat.xml' */


/* './packages/continuity/nav_places.xml' */


/* './packages/continuity/nav_plan.xml' */


/* './packages/continuity/nav_plan_action.xml' */


/* './packages/continuity/nav_plan_schedule.xml' */


/* './packages/continuity/nav_plan_test.xml' */


/* './packages/continuity/nav_process.xml' */


/* './packages/continuity/nav_process_activity_asset.xml' */


/* './packages/continuity/nav_process_scene.xml' */


/* './packages/continuity/nav_process_threat.xml' */


/* './packages/continuity/nav_providers.xml' */


/* './packages/continuity/nav_resources.xml' */


/* './packages/continuity/nav_scope.xml' */


/* './packages/continuity/nav_un.xml' */


/* './packages/continuity/popup_about_asset.xml' */


/* './packages/continuity/popup_activity_edit.xml' */


/* './packages/continuity/popup_address_locator.xml' */


/* './packages/continuity/popup_area_search.xml' */


/* './packages/continuity/popup_asset_associate_threat.xml' */


/* './packages/continuity/popup_asset_edit.xml' */


/* './packages/continuity/popup_department_edit.xml' */


/* './packages/continuity/popup_financial_impact_edit.xml' */


/* './packages/continuity/popup_group_edit.xml' */


/* './packages/continuity/popup_group_resource_single_search.xml' */

'mx_friday'=>'Friday',

/* './packages/continuity/popup_group_search.xml' */


/* './packages/continuity/popup_group_single_search.xml' */


/* './packages/continuity/popup_people_search.xml' */


/* './packages/continuity/popup_place_asset_edit.xml' */


/* './packages/continuity/popup_place_associate_threat.xml' */


/* './packages/continuity/popup_place_edit.xml' */


/* './packages/continuity/popup_place_resource_edit.xml' */


/* './packages/continuity/popup_place_search.xml' */


/* './packages/continuity/popup_place_single_search.xml' */


/* './packages/continuity/popup_plan_action_edit.xml' */


/* './packages/continuity/popup_plan_action_search.xml' */


/* './packages/continuity/popup_plan_schedule_edit.xml' */


/* './packages/continuity/popup_plan_search.xml' */


/* './packages/continuity/popup_plan_test_edit.xml' */


/* './packages/continuity/popup_plan_type_edit.xml' */


/* './packages/continuity/popup_planRange_edit.xml' */


/* './packages/continuity/popup_process_draw.xml' */


/* './packages/continuity/popup_process_edit.xml' */


/* './packages/continuity/popup_process_search.xml' */


/* './packages/continuity/popup_provider_edit.xml' */


/* './packages/continuity/popup_provider_search.xml' */


/* './packages/continuity/popup_provider_single_search.xml' */


/* './packages/continuity/popup_resource_edit.xml' */


/* './packages/continuity/popup_resource_search.xml' */


/* './packages/continuity/popup_scene_edit.xml' */


/* './packages/continuity/popup_scene_impact_edit.xml' */


/* './packages/continuity/popup_scope_search.xml' */


/* './packages/continuity/popup_scope_view.xml' */


/* './packages/continuity/popup_vulnerability_edit.xml' */


/* './packages/continuity/tab_continuity_management.xml' */


/* './packages/improvement/nav_action_plan.php' */

'mx_corrective'=>'Corrective',
'mx_preventive'=>'Preventive',
'st_ap_remove_message'=>'Are you sure you wish to remove the Action Plan<b>%name%</b>?',
'tt_ap_remove'=>'Remove Action Plan',
'tt_nc_filter_grid_action_plan'=>'(filtered by nonconformity \'<b>%nc_name%</b>\')',

/* './packages/improvement/nav_action_plan.xml' */

'gc_actiontype'=>'Action Type',
'gc_ap_cns'=>'Nonconformities',
'gc_dateconclusion'=>'Finishing Date',
'gc_datedeadline'=>'Deadline',
'gc_isefficient'=>'Efficient',
'gc_responsible_id'=>'Responsible',
'mi_data_to_meassure'=>'Define Measure',
'mi_meassure'=>'Measure',
'tt_ap_bl'=>'<b>Action Plan</b>',

/* './packages/improvement/nav_disciplinary_process.xml' */

'gc_action_taken'=>'Action Performed',
'mi_register_action'=>'Register Action',

/* './packages/improvement/nav_incident.php' */

'mx_direct'=>'Direct',
'mx_indirect'=>'Indirect',
'mx_no_category'=>'No Category',
'st_incident_remove_confirm'=>'Are you sure you wish to remove the incident <b>%incident_name%</b>?',
'tt_incident_remove_confirm'=>'Remove Incident',

/* './packages/improvement/nav_incident.xml' */

'gc_incident_date_limit'=>'Estimated',
'gc_loss_type'=>'Loss Type',
'mi_disciplinary_process'=>'Disciplinary Process',
'mi_finish'=>'Finish',
'mi_immediate_disposal_approval'=>'Immediate Disposal approval',
'mi_solution_approval'=>'Solution Approval',
'tt_incident'=>'<b>Incident</b>',

/* './packages/improvement/nav_non_conformity.php' */

'gc_control_security'=>'Security Control',
'gc_external_audit'=>'External Audit',
'gc_internal_audit'=>'Internal Audit',
'gc_no'=>'No',
'gc_yes'=>'Yes',
'st_nc_remove_message'=>'Are you sure that you want to remove the nonconformity <b>%name%</b>?',
'tt_ap_filter_grid_non_conformity'=>'(filtered by action plan \'<b>%ap_name%</b>\')',
'tt_nc_remove'=>'Remove Nonconformity',

/* './packages/improvement/nav_non_conformity.xml' */

'gc_action_plans'=>'Action Plan',
'gc_capability'=>'Potential',
'gc_sender'=>'Emitter',
'gc_state'=>'Status',
'mi_action_plan'=>'Associate Action Plan',
'mi_send_to_ap_pendant'=>'Send to AP Acceptance',
'mi_send_to_conclusion'=>'Send to Finish',
'mi_send_to_responsible'=>'Send to Responsible',
'tt_nc_bl'=>'<b>Nonconformity</b>',

/* './packages/improvement/nav_occurrence.php' */

'st_occurrence_remove'=>'Are you sure that you want to remove the occurrence <b>%occurrence_text%</b>?',
'tt_occurrence_remove'=>'Remove Occurrence',

/* './packages/improvement/nav_occurrence.xml' */

'gc_incident'=>'Incident',
'mi_incident'=>'Associate Incident',
'tt_occurrences'=>'<b>Occurrences</b>',

/* './packages/improvement/nav_report.php' */

'mx_report_action_plan_by_process'=>'Action Plan by Process',
'mx_report_action_plan_by_user'=>'Action Plan by User',
'mx_report_ap_without_doc'=>'Action Plan without Document',
'mx_report_ci_pendant_tasks'=>'Pending tasks in Continual Improvment',
'mx_report_control_nonconformity_by_process'=>'Nonconformity of Control by Process',
'mx_report_elements_affected_by_incident'=>'Items affected by Incidents',
'mx_report_financial_impact_by_incident'=>'Financial Impact by Incident',
'mx_report_incident_accompaniment'=>'Incident Follow Up',
'mx_report_incident_by_responsible'=>'Evidence Collection',
'mx_report_incident_control_revision'=>'Control Revisions caused by Incidents',
'mx_report_incident_without_risk'=>'Incident without Risks',
'mx_report_incidents_without_occurrence'=>'Incidents without Occurrences',
'mx_report_nc_without_ap'=>'Nonconformity Without Action Plan',
'mx_report_nonconformity_accompaniment'=>'Nonconformity follow up',
'mx_report_nonconformity_by_process'=>'Nonconformity by Process',
'mx_report_occurrence_accompaniment'=>'Occurrence Follow Up',
'mx_report_occurrence_by_incident'=>'Occurrences by Incident',
'mx_report_risk_auto_probability_value'=>'Automatic Risk Likelihood Calculation',
'mx_report_user_by_incident'=>'Disciplinary Process',

/* './packages/improvement/nav_report.xml' */

'cb_risk'=>'Risk',
'lb_conclusion_cl'=>'Finishing:',
'lb_incident_order_by'=>'Order by:',
'lb_loss_type_cl'=>'Loss Type:',
'lb_show_cl'=>'Show:',
'si_capability_all'=>'All',
'si_capability_no'=>'No',
'si_capability_yes'=>'Yes',
'si_ci_manager'=>'Continual Improvement Reports',
'si_incident_order_by_category'=>'Category',
'si_incident_order_by_date'=>'Incident Date',
'si_incident_order_by_name'=>'Name',
'si_incident_order_by_occurrence_qt'=>'Occurrence Quantity',
'si_incident_order_by_responsible'=>'Responsible',
'si_incident_report'=>'Incident Reports',
'si_nonconformity_report'=>'Nonconformity Reports',
'st_action_plan_filter_bl'=>'<b>Action Plan Filter</b>',
'st_elements_filter_bl'=>'<b>Items Filter</b>',
'st_incident_filter_bl'=>'<b>Incident Filter</b>',
'st_nonconformity_filter_bl'=>'<b>Nonconformity Filter</b>',
'st_occurrence_filter_bl'=>'<b>Occurrence Filter</b>',
'st_status'=>'Status:',
'wn_at_least_one_element_must_be_selected'=>'At least one item should be selected.',

/* './packages/improvement/popup_action_plan_associate.xml' */

'tt_action_plan_association'=>'Action Plan Association',
'tt_current_action_plans_bl'=>'<b>Current Action Plans</b>',

/* './packages/improvement/popup_action_plan_edit.xml' */

'lb_ap_actionplan_bl_cl'=>'<b>Action:</b>',
'lb_ap_actiontype_bl_cl'=>'<b>Action Type:</b>',
'lb_ap_dateconclusion_cl'=>'Finishing Date:',
'lb_ap_datedeadline_bl_cl'=>'<b>Deadline:</b>',
'lb_ap_name_bl_cl'=>'<b>Name:</b>',
'lb_ap_responsible_id_bl_cl'=>'<b>Responsible:</b>',
'rb_corrective'=>'Corrective',
'rb_preventive'=>'Preventive',
'tt_ap_edit'=>'Action Plan Edition',
'vb_finish'=>'Finish',
'wn_deadline'=>'The deadline should be later than or equal to the current date.',

/* './packages/improvement/popup_action_plan_finish_confirm.xml' */

'lb_ap_days_before_bl'=>'<b>Notify before</b>',
'lb_date_efficiency_revision_bl_cl'=>'<b>Efficiency Revision Date:</b>',
'st_action_plan_finish_confirm_message'=>'Do you want to finish the Action Plan \'%name%\'?',
'tt_action_plan_finish'=>'Finish Action Plan',
'wn_revision_date_after_conclusion_date'=>'The efficiency revision date should be later than or the same as the finishing date!',

/* './packages/improvement/popup_action_plan_revision_task.xml' */

'lb_action_cl'=>'Action:',
'st_action_plan_efficient_bl'=>'<b>Was the Action Plan efficient?</b>',
'st_data_revision_bl_cl'=>'<b>Revision Date:</b>',
'tt_action_plan_efficiency_revision'=>'Action Plan Eficiency Revision',

/* './packages/improvement/popup_disciplinary_process_action.php' */

'st_denied_permission_to_edit_action_taken'=>'You don\'t have permission to register an action in a disciplinary process.',

/* './packages/improvement/popup_disciplinary_process_action.xml' */

'lb_action_bl_cl'=>'<b>Action:</b>',
'st_incident_cl'=>'<b>Incident:</b>',
'st_user_cl'=>'<b>User:</b>',

/* './packages/improvement/popup_disciplinary_process_edit.php' */

'em_disciplinary_process_creation'=>'Disciplinary Process Creation',
'em_disciplinary_process_creation_footer'=>'For further information, contact the security officer.',
'st_incident_user_remove'=>'Are you sure you want to remove user <b>%user_name%</b> from the disciplinary process of the incident <b>%incident_name%</b>?',
'tt_incident_user_remove'=>'Remove User',

/* './packages/improvement/popup_disciplinary_process_edit.xml' */

'gc_observation_involvement'=>'Observation / Engagement',
'gc_user'=>'User',
'lb_incident_cl'=>'Incident:',
'lb_observation_involvement_cl'=>'Observation / Engagement:',
'lb_user_bl_cl'=>'<b>User:</b>',
'tt_disciplinary_process'=>'<b>Disciplinary Process</b>',
'wn_email_dp_sent'=>'An e-mail was already sent to the Disciplinary Process Manager.',

/* './packages/improvement/popup_evidence_requirement.xml' */

'lb_comments_cl'=>'Comments:',
'tt_evidence_requirement'=>'Evidence Colection:',
'wn_evidence_requirement_editing'=>'Obs. When editing the comment above, an alert with the comment content will be sent to the Evidence Colector Manager.',

/* './packages/improvement/popup_financial_impact_edit.xml' */

'st_total'=>'Total',
'tt_financial_impact_parametrization'=>'Financial Impact Estimation',

/* './packages/improvement/popup_incident_accounts_plan.xml' */

'tt_incident_accounts_plan'=>'Accounting Plan',

/* './packages/improvement/popup_incident_affected_product_service.xml' */

'tt_affected_product_service'=>'Product / Affected Service',

/* './packages/improvement/popup_incident_approval_task.xml' */

'tt_assets_per_incident'=>'<b>Assets Related to the Incident</b>',

/* './packages/improvement/popup_incident_disposal.xml' */

'tt_immediate_disposal'=>'Immediate Disposal',

/* './packages/improvement/popup_incident_edit.xml' */

'lb_accounts_plan_cl'=>'Accounting Plan:',
'lb_affected_product_service_cl'=>'Product / Affected Service:',
'lb_date_bl_cl'=>'<b>Date:</b>',
'lb_date_limit_bl_cl'=>'<b>Estimated Finishing:</b>',
'lb_evidences_cl'=>'Evidences:',
'lb_hour_bl_cl'=>'<b>Hour:</b>',
'lb_immediate_disposal_cl'=>'Immediate Disposal:',
'lb_loss_type_bl_cl'=>'<b>Loss Type</b>',
'lb_occurrences_bl_cl'=>'<b>Occurrences:</b>',
'lb_occurrences_cl'=>'Occurrences:',
'lb_solution_cl'=>'Solution:',
'si_direct'=>'Direct',
'si_indirect'=>'Indirect',
'st_collect_evidence'=>'Collect Evidences?',
'st_incident_date_finish'=>'Estimated Finishing:',
'st_incident_date_finish_bl'=>'<b>Estimated Finishing:</b>',
'st_no'=>'No',
'st_yes'=>'Yes',
'tt_incident_edit'=>'Incident Editing',
'vb_financial_impact'=>'Financial Impact',
'vb_observations'=>'Observations',
'wn_future_date_not_allowed'=>'A future date / hour is not allowed.',
'wn_prevision_finish'=>'The estimated finishing date must be equal to or later than the current day.',

/* './packages/improvement/popup_incident_evidence.xml' */

'tt_evidence'=>'Evidence',

/* './packages/improvement/popup_incident_process_association_edit.php' */

'st_save_suggested_incident_processes'=>'Do you want to relate the suggested processes to the incident <b>%context_name% </b>?',
'tt_save_suggested_incident_processes'=>'Relate Suggested Processes',

/* './packages/improvement/popup_incident_process_association_edit.xml' */

'cb_search_only_suggested_process'=>'Search only suggested processes',
'tt_incident_process_association'=>'Process to Incident Association',

/* './packages/improvement/popup_incident_risk_asset_association_edit.xml' */

'lb_assets_cl'=>'Assets:',
'lb_risks_cl'=>'Risks:',
'st_risk_asset_association_message'=>'When selecting an asset, please select the risks that you want to be associated to it. It\'s possible to associate more than one risk by selecting them with CRTL key.',
'tt_risk_asset_association'=>'Risk to Assets Association',

/* './packages/improvement/popup_incident_risk_association_edit.php' */

'em_incident_risk_parametrization'=>'Risk Estimation associated to Incident',
'em_incident_risk_parametrization_footer'=>'For further information, contact the security officer.',
'mx_non_parameterized_risk'=>'Non-Estimated Risk',
'to_dependencies_bl_cl'=>'<b>Dependences:</b> <br><br>',
'to_dependents_bl_cl'=>'<b>Dependent:</b> <br><br>',

/* './packages/improvement/popup_incident_risk_association_edit.xml' */

'tt_associated_controls_and_assets'=>'<b>Related Controls and Assets</b>',
'tt_incident_risk_association'=>'Incident-Risk Association',
'tt_risks_refferring_controls_and_assets'=>'<b>Risks Regarding Controls and Assets</b>',
'tt_risks_related_selected'=>'<b>Related / Selected Risks</b>',
'vb_insert_risk'=>'Insert Risk',
'vb_risk_incident_parametrization'=>'Incident-Risk Estimation',
'vb_search_assets'=>'Search Assets',
'vb_search_controls'=>'Search Controls',
'wn_parameterize_incident_risk'=>'You must estimate the Incident-Risk association before saving your association.',

/* './packages/improvement/popup_incident_risk_association_options.xml' */

'lb_associate_risk_to_assets'=>'Do you want to relate current risks to the selected assets? You can choose which risks want to relate the one which assets.',
'lb_send_alert_to_other_users'=>'Do you want to send alerts for other users besides the responsible for the security of the assets and controls related to the risk %new_risks%?',
'tt_incident_risk_association_options'=>'Options to the risks and incidents association',
'vb_continue'=>'Continue',

/* './packages/improvement/popup_incident_risk_parametrization.xml' */

'tt_incident_risk_association_parametrization'=>'Estimation of the Incident-Risk Associaton',

/* './packages/improvement/popup_incident_search.xml' */

'tt_incident_search'=>'Incident Search',
'vb_relation'=>'Relate',
'wn_no_incident_selected'=>'No incident was selected.',

/* './packages/improvement/popup_incident_solution.xml' */

'tt_solution'=>'Solution',

/* './packages/improvement/popup_incident_state_forward.php' */

'st_incident_finish'=>'Do you want to finish the incident?',
'st_incident_send_to_app_disposal'=>'Do you wish to send the incident to Immediate Disposal approval?',
'st_incident_send_to_app_solution'=>'Do you want to send the incident to Solution approval?',
'st_incident_send_to_responsible'=>'Do you want to send the incident to responsible?',
'tt_incident_finish'=>'Finish Incident',
'tt_incident_send_to_app_disposal'=>'Sent to Immediate Disposal Approval',
'tt_incident_send_to_app_solution'=>'Send to Solution Approval',
'tt_incident_send_to_responsible'=>'Send to Responsible',

/* './packages/improvement/popup_incident_task_view.php' */

'lb_incident_immediate_disposal_cl'=>'Immediate Disposal:',
'lb_incident_solution_cl'=>'Solution:',

/* './packages/improvement/popup_incident_task_view.xml' */

'lb_date_limit_cl'=>'Limit Date:',

/* './packages/improvement/popup_new_risk_parameter_approval_task.xml' */


/* './packages/improvement/popup_non_conformity_approval_task.xml' */

'lb_processes_cl'=>'Processes:',

/* './packages/improvement/popup_non_conformity_associate.xml' */

'st_name'=>'Name:',
'tt_current_non_conformitys_bl'=>'<b>Current Nonconformities</b>',
'tt_non_conformity_association'=>'Nonconformities Association',

/* './packages/improvement/popup_non_conformity_create.xml' */

'lb_date_sent_cl'=>'Date of Emission:',
'lb_sender_cl'=>'Sender:',
'tt_non_conformity_record'=>'Nonconformity Record',

/* './packages/improvement/popup_non_conformity_data_approval_task.php' */

'st_denied_permission_to_execute_task'=>'You do not have permission to execute the task.',

/* './packages/improvement/popup_non_conformity_data_approval_task.xml' */

'lb_ap_responsible_cl'=>'API Resp.:',
'lb_cause_cl'=>'Cause:',
'lb_inquirer_cl'=>'Inquirer:',
'lb_justificative_cl'=>'Reason:',
'lb_non_conformity_cl'=>'Nonconformity:',
'lb_task_cl'=>'Task:',
'tt_task_visualization'=>'Task Visualization',

/* './packages/improvement/popup_non_conformity_edit.xml' */

'lb_action_plans_cl'=>'Action Plans:',
'lb_nc_capability_bl_cl'=>'Potential:',
'lb_nc_cause_cl'=>'Cause:',
'lb_nc_classification_cl'=>'Classification:',
'lb_nc_datesent_cl'=>'Date of Emission:',
'lb_nc_description_cl'=>'Description:',
'lb_nc_name_bl_cl'=>'<b>Name:</b>',
'lb_nc_responsible_id_cl'=>'Responsible:',
'lb_nc_sender_id_cl'=>'Sender:',
'lb_potential_cl'=>'Potential:',
'lb_processes_bl_cl'=>'<b>Processes:</b>',
'si_external_audit'=>'External Audit',
'si_internal_audit'=>'Internal Audit',
'si_no'=>'No',
'si_security_control'=>'Security Control',
'si_yes'=>'Yes',
'tt_nc_edit'=>'Nonconformity Editing',

/* './packages/improvement/popup_non_conformity_revision_task.xml' */

'lb_action_plan_cl'=>'Action Plan:',
'lb_ap_efficient_bl'=>'<b>Was the Action Plan efficient?</b>',
'rb_no'=>'No',
'rb_yes'=>'Yes',
'tt_non_conformity_efficiency_revision'=>'Nonconformity Eficiency Revision',

/* './packages/improvement/popup_non_conformity_state_forward.php' */

'st_non_conformity_send_to_ap_responsible'=>'Do you wish to send the Nonconformity in order to have its Action Plans accepted?',
'st_non_conformity_send_to_approve'=>'Do you wish to send the Nonconformity to the Action Plans Finishing?',
'st_non_conformity_send_to_responsible'=>'Do you wish to send the Nonconformity to the responsible?',
'tt_non_conformity_send_to_ap_responsible'=>'Send to Action Plans Acceptance',
'tt_non_conformity_send_to_approve'=>'Send to Action Plans Finishing',
'tt_non_conformity_send_to_responsible'=>'Send to Responsible',

/* './packages/improvement/popup_non_conformity_state_forward.xml' */

'vb_no'=>'No',
'vb_yes'=>'Yes',

/* './packages/improvement/popup_occurrence_edit.xml' */

'lb_description_bl_cl'=>'<b>Description:</b>',
'st_date_bl'=>'<b>Date:</b>',
'st_date_warning'=>'A future date / hour is not allowed.',
'st_hour_bl'=>'<b>Hour:</b>',
'tt_register_occurrence'=>'Register Occurrence',

/* './packages/improvement/popup_occurrences_search.xml' */

'tt_current_occurrences'=>'<b>Current Occurrences</b>',
'tt_occurrences_search'=>'Occurrences Search',

/* './packages/improvement/popup_probability_calculation_config.xml' */

'lb_incidents_in_period_cl'=>'Incidents between %initial_date% and %final_date%:',
'lb_last_check_cl'=>'Last verification:',
'lb_periodicity_bl_cl'=>'<b>Periodicity:</b>',
'lb_starting_at'=>'Starting by',
'si_days'=>'Day(s)',
'si_months'=>'Month(s)',
'si_weeks'=>'Week(s)',
'st_total_incidents_found'=>'Total of incidents found:',
'tt_incidents_related_to_risk_in_period'=>'<b>Incidents related to risks in the period</b>',
'tt_probability_calculation_config'=>'Configuration of Automatic Likelihood Calculation',
'tt_probability_level_vs_incident_count'=>'<b>Likelihood Level<br/><br/><b>X</b><br/><br/><b>Amount of Incidents</b>',
'vb_update'=>'Update',
'wn_probability_config_not_filled'=>'Intervals not filled out or with inconsistent  values',

/* './packages/improvement/popup_process_search.xml' */

'tt_current_processes'=>'<b>Current Processes<b>',
'tt_process_search'=>'Process Search',

/* './packages/improvement/popup_request_compare_impact_risks.xml' */


/* './packages/improvement/popup_risk_create.xml' */

'lb_impact_money_cl'=>'Financial Impact:',
'si_select_one_below'=>'Select one of the items below',
'st_risk_impact'=>'Impact:',
'tt_insert_risk'=>'Insert Risk',

/* './packages/improvement/report/clock.php' */

'st_clock_wait'=>'<b>Attention:</b> This report may take a few minutes to be generated.<br>Please wait.',

/* './packages/improvement/report/popup_report_action_plan_by_process.php' */

'tt_action_plan_by_process'=>'Action Plan by Process',

/* './packages/improvement/report/popup_report_action_plan_by_user.php' */

'tt_action_plan_by_user'=>'Action Plan by User',

/* './packages/improvement/report/popup_report_ap_without_doc.php' */

'rs_ap_without_docs'=>'Action Plans without Documents',

/* './packages/improvement/report/popup_report_ap_without_doc.xml' */

'rs_ap'=>'Action Plan',

/* './packages/improvement/report/popup_report_control_nonconformity_by_process.php' */

'mx_control_nonconformity_by_process'=>'Nonconformity of Control by Process',

/* './packages/improvement/report/popup_report_disciplinary_process.php' */

'mx_disciplinary_process'=>'Disciplinary Processes',

/* './packages/improvement/report/popup_report_disciplinary_process.xml' */

'rs_dp_name'=>'Name',

/* './packages/improvement/report/popup_report_elements_affected_by_incident.php' */

'mx_elements_affected_by_incident'=>'Items affected by Incidents',

/* './packages/improvement/report/popup_report_elements_affected_by_incident.xml' */

'rs_incident_bl_cl'=>'<b>Incident:</b>',

/* './packages/improvement/report/popup_report_financial_impact_by_incident.php' */

'mx_financial_impact_by_incident'=>'Financial Impact by Incident',

/* './packages/improvement/report/popup_report_incident_accompaniment.php' */

'mx_incident_accompaniment'=>'Incident Follow Up',

/* './packages/improvement/report/popup_report_incident_by_responsible.php' */

'mx_evidence_collection'=>'Evidence Collection',

/* './packages/improvement/report/popup_report_incident_control_revision.php' */

'mx_incident_control_revision'=>'Control Revision caused by Incidents',

/* './packages/improvement/report/popup_report_incident_control_revision.xml' */

'rs_date_todo'=>'Estimated Date',
'rs_er'=>'RE',

/* './packages/improvement/report/popup_report_incident_without_occurrence.php' */

'rs_incidents_without_occurrence'=>'Incidents without Occurrences',

/* './packages/improvement/report/popup_report_incident_without_risk.php' */

'rs_incidentes_without_risks'=>'Incidents without Risks',

/* './packages/improvement/report/popup_report_incident_without_risk.xml' */

'rs_incident'=>'Incident',

/* './packages/improvement/report/popup_report_incidents.php' */

'mx_incidents'=>'Incidents',

/* './packages/improvement/report/popup_report_incidents.xml' */

'rs_incident_name'=>'Name',

/* './packages/improvement/report/popup_report_nc_without_ap.php' */

'rs_nc_without_ap'=>'Nonconformities without Action Plans',
'st_nc_manager_with_parentheses'=>'(Nonconformity Manager)',

/* './packages/improvement/report/popup_report_non_conformities.php' */

'mx_non_conformities'=>'Nonconformities',

/* './packages/improvement/report/popup_report_non_conformities.xml' */

'rs_nc_name'=>'Name',

/* './packages/improvement/report/popup_report_nonconformity_accompaniment.php' */

'mx_nonconformity_accompaniment'=>'Nonconformity Follow Up',
'rs_not_defined'=>'Not defined',

/* './packages/improvement/report/popup_report_nonconformity_accompaniment.xml' */

'rs_action_plan'=>'Action Plan',
'rs_conclusion'=>'Finishing',
'rs_non_conformity'=>'Nonconformity:',

/* './packages/improvement/report/popup_report_nonconformity_by_process.php' */

'mx_nonconformity_by_process'=>'Nonconformity by Process',

/* './packages/improvement/report/popup_report_nonconformity_by_process.xml' */

'rs_nonconformity'=>'Nonconformity',

/* './packages/improvement/report/popup_report_occurrence_accompaniment.php' */

'mx_occurrence_accompaniment'=>'Occurrence Follow Up',

/* './packages/improvement/report/popup_report_occurrence_accompaniment.xml' */

'rs_creator_cl'=>'Creator:',

/* './packages/improvement/report/popup_report_occurrence_by_incident.php' */

'mx_occurrence_by_incident'=>'Occurrences by Incident',

/* './packages/improvement/report/popup_report_occurrence_by_incident.xml' */

'report_filter_userid_cl'=>'Responsible:',
'rs_date_cl'=>'Date',
'rs_occurrence'=>'Occurrence',
'rs_status_cl'=>'Status:',

/* './packages/improvement/report/popup_report_risk_auto_probability_value.php' */

'mx_risk_auto_probability_value'=>'Automatic Risk Likelihood Calculation',

/* './packages/improvement/report/popup_report_risk_auto_probability_value.xml' */

'rs_incident_amount'=>'Amt. of Incidents',
'rs_last_verification'=>'Last Verification',
'rs_periodicity'=>'Periodicity',

/* './packages/improvement/report/popup_report_user_by_incident.php' */

'mx_user_by_incident'=>'Disciplinary Process',

/* './packages/improvement/report/popup_report_user_by_incident.xml' */

'rs_description'=>'Description',
'rs_incident_cl'=>'Incident:',
'rs_user'=>'User',

/* './packages/improvement/report/report_template.xml' */

'rs_fim'=>'END OF REPORT',

/* './packages/improvement/tab_continual_improvement.xml' */

'ti_action_plan'=>'Action Plan',
'ti_disciplinary_process'=>'Disciplinary Process',
'ti_incident'=>'Incident',
'ti_non_conformity'=>'Nonconformity',
'ti_occurrences'=>'Occurrences',
'ti_report'=>'Reports',

/* './packages/libraries/nav_asset_category_threats.xml' */


/* './packages/libraries/nav_best_practices.php' */

'st_remove_best_practice_confirm'=>'Are you sure you wish to remove the  Requirement <b>%best_practice_name%</b>?',
'st_remove_section_confirm'=>'Are you sure you wish to remove the section <b>%section_name%</b>?',
'tt_remove_best_practice'=>'Remove Requirement',
'tt_remove_section'=>'Remove Section',

/* './packages/libraries/nav_best_practices.xml' */

'mi_associate_events'=>'Associate Events',
'mi_open_section'=>'Open Section',
'mi_templates'=>'Document Templates',
'tt_best_practices_bl'=>'<b>Requirements</b>',
'vb_insert_best_practice'=>'Insert Requirement',
'vb_insert_section'=>'Insert Section',

/* './packages/libraries/nav_department.xml' */


/* './packages/libraries/nav_document_template.php' */

'st_remove_document_template_confirm'=>'Are you sure you wish to remove the Document Template <b>%context_name%</b>?',
'tt_remove_document_template'=>'Remove Document Template',

/* './packages/libraries/nav_document_template.xml' */

'si_best_practice'=>'Requirement',
'si_denied'=>'Denied',
'si_doc_template_control'=>'Control',
'si_pending'=>'Pending',
'tt_documents_templates_bl'=>'<b>Documents Templates</b>',

/* './packages/libraries/nav_events.php' */

'st_remove_category_cascade_message'=>'You will also be deleting any Assets related to the Category, as well as its Risks and their associations with other items of the system.',
'st_remove_category_confirm'=>'Are you sure you wish to remove the category  <b>%category_name%</b>?',
'st_remove_event_confirm'=>'Are you sure you wish to remove the event  <b>%event_name%</b>?',
'tt_remove_category'=>'Remove Category',
'tt_remove_event'=>'Remove Event',

/* './packages/libraries/nav_events.xml' */

'mi_associate_best_practices'=>'Associate Requirements',
'mi_open_event_category'=>'Open Category',
'tt_events_library_bl'=>'<b>Events Library</b>',
'vb_associate_event'=>'Associate Event',
'vb_insert_event'=>'Insert Event',

/* './packages/libraries/nav_impact_library.xml' */


/* './packages/libraries/nav_import_export.php' */

'wn_error_invalid_file'=>'Error: invalid file.',
'wn_extension_not_allowed'=>'Extention not allowed.',
'wn_maximum_size_exceeded'=>'Maximum size exceeded.',

/* './packages/libraries/nav_import_export.xml' */

'lb_client_cl'=>'Client:',
'lb_export_library_cl'=>'Export Library:',
'lb_import_library_cl'=>'Import Library:',
'lb_license_cl'=>'License:',
'si_best_practices'=>'Requirements',
'si_events_library'=>'Events Library',
'si_standards'=>'Standards',
'si_templates'=>'Documents Templates',
'st_export_list_bl'=>'<b>List of Export</b>',
'st_exporting_options_bl'=>'<b>Export Options</b>',
'st_import_list_bl'=>'<b>List of Import</b>',
'st_or_bl'=>'<b>Or</b>',
'to_export'=>'<b>Export:</b><br/><br/>Exports selected items.',
'to_export_list'=>'<b>List:</b><br/><br/>Lists library items, which will be exported.',
'to_import'=>'<b>Import</b><br/><br/>Import selected items.',
'to_import_list'=>'<b>List:</b><br/><br/>Lists library items, which will be imported.',
'tr_best_practices'=>'Requirements',
'tr_events_library'=>'Events Library',
'tr_no_items_to_import'=>'There are no items to be imported',
'tr_standards'=>'Standards',
'tr_template'=>'Documents Templates',
'tr_templates'=>'Documents Templates',
'vb_export'=>'Export',
'vb_import'=>'Import',
'vb_list'=>'List',
'vb_list_import'=>'List',
'wn_import_required_file'=>'You must choose a file before clicking on List.',

/* './packages/libraries/nav_incident_category.php' */

'st_category_remove'=>'Are you sure you wish to remove the category <b>%category_name%</b>?',
'st_category_remove_error'=>'It is not possible to remove the category <b>%category_name%</b> because it is related to one or more incidents!',
'st_solution_remove'=>'Are you sure you wish to remove the solution?',
'tt_category_remove'=>'Remove Category',
'tt_category_remove_error'=>'Error when removing Category',
'tt_solution_remove'=>'Remove Solution',

/* './packages/libraries/nav_incident_category.xml' */

'gc_keywords'=>'Keywords',
'gc_problem'=>'Problem',
'gc_solution'=>'Solution',
'gc_solutions_bl'=>'<b>Solutions</b>',
'mi_delete'=>'Remove',
'mi_open_category'=>'Open Category',
'tt_category_library'=>'<b>Categories Library</b>',
'vb_solution_insert'=>'Insert Solution',

/* './packages/libraries/nav_place_category_threats.xml' */


/* './packages/libraries/nav_plan_ranges.xml' */


/* './packages/libraries/nav_plan_types.xml' */


/* './packages/libraries/nav_process_category_threats.xml' */

'vb_insert_category'=>'Insert Category',

/* './packages/libraries/nav_standards.php' */

'st_remove_standard_confirm'=>'Are you sure you wish to remove the standard <b>%standard_name%</b>?',
'tt_remove_standard'=>'Remove Standard',

/* './packages/libraries/nav_standards.xml' */

'gc_creator'=>'Creator',
'tt_standards_bl'=>'<b>Standards</b>',

/* './packages/libraries/popup_best_practice_add.xml' */

'si_all_sections'=>'All Sections',
'tt_current_best_practices_bl'=>'<b>Current Requirements</b>',

/* './packages/libraries/popup_best_practice_edit.php' */

'tt_best_practice_adding'=>'Requirement Adding',
'tt_best_practice_editing'=>'Requirement Editing',

/* './packages/libraries/popup_best_practice_edit.xml' */

'lb_control_objective_cl'=>'Control Objective:',
'lb_control_type_cl'=>'Control Type:',
'lb_implementation_guidelines_cl'=>'Testing Procedures:',
'lb_metric_cl'=>'Metric:',
'lb_name_good_practices_bl_cl'=>'<b>Name/ Good Practices:</b>',
'lb_standards_bl_cl'=>'<b>Standards:</b>',
'si_administrative'=>'Administrative',
'si_awareness'=>'Awareness',
'si_coercion'=>'Coercion',
'si_correction'=>'Correction',
'si_detection'=>'Detection',
'si_limitation'=>'Limitation',
'si_monitoring'=>'Monitoring',
'si_physical'=>'Physical',
'si_prevention'=>'Prevention',
'si_recovery'=>'Recovery',
'si_technical'=>'Technical',
'tt_best_practice'=>'Requirement',

/* './packages/libraries/popup_category_edit.php' */

'tt_category_adding'=>'Category Adding',
'tt_category_editing'=>'Category Editing',

/* './packages/libraries/popup_document_template_edit.php' */

'tt_document_template_adding'=>'Document Template Adding',
'tt_document_template_editing'=>'Document Template Editing',

/* './packages/libraries/popup_document_template_edit.xml' */

'lb_best_practice_cl'=>'Requirement:',
'lb_type_bl_cl'=>'<b>Type:</b>',
'si_action_plan'=>'Action Plan',
'si_area'=>'Area',
'si_asset'=>'Asset',
'si_control'=>'Control',
'si_management'=>'Management',
'si_policy'=>'Policy',
'si_process'=>'Process',
'si_scope'=>'Scope',
'si_select_type'=>'Select Type',

/* './packages/libraries/popup_event_add.xml' */

'tt_current_events_bl'=>'<b>Current Events</b>',

/* './packages/libraries/popup_event_edit.php' */

'tt_event_adding'=>'Event Adding',
'tt_event_editing'=>'Event Editing',

/* './packages/libraries/popup_event_edit.xml' */

'cb_suggested_event'=>'Suggested Event',
'lb_observation_cl'=>'Observation:',
'st_event_impact_cl'=>'Impact:',

/* './packages/libraries/popup_imple_guide.xml' */

'tt_implementation_guidelines'=>'Testing Procedures',

/* './packages/libraries/popup_incident_category_edit.php' */

'tt_category_addition'=>'Category Adding',

/* './packages/libraries/popup_incident_category_edit.xml' */

'tt_category_edit'=>'Category Edition',

/* './packages/libraries/popup_incident_solution_edit.xml' */

'lb_incident_category_cl'=>'Category:',
'lb_problem_bl_cl'=>'<b>Problem:</b>',
'lb_solution_bl_cl'=>'<b>Solution:</b>',
'tt_solution_edit'=>'Category Editing',

/* './packages/libraries/popup_metric.xml' */

'tt_metric'=>'Metric',

/* './packages/libraries/popup_process_category_threat_edit.xml' */


/* './packages/libraries/popup_section_edit.php' */

'tt_section_adding'=>'Section Adding',
'tt_section_editing'=>'Section Editing',

/* './packages/libraries/popup_standard_add.xml' */

'tt_current_standards_bl'=>'<b>Current Standards</b>',
'tt_standards_search'=>'Standards Search',

/* './packages/libraries/popup_standard_edit.php' */

'tt_standard_adding'=>'Standard Adding',
'tt_standard_editing'=>'Standard Editing',

/* './packages/libraries/popup_standard_edit.xml' */

'lb_application_cl'=>'Application:',
'lb_objective_cl'=>'Objective:',

/* './packages/libraries/tab_libraries.xml' */

'ti_best_practices'=>'Requirements',
'ti_category_library'=>'Incident Categories',
'ti_events_library'=>'Events Library',
'ti_export_and_import'=>'Export & Import',
'ti_standards'=>'Standards',
'ti_templates'=>'Documents Templates',

/* './packages/policy/nav_all.xml' */

'lb_status'=>'Status',
'tt_all_documents_bl'=>'<b>All Documents</b>',

/* './packages/policy/nav_approving.xml' */

'tt_approving_documents_bl'=>'<b>Documents In Approval</b>',

/* './packages/policy/nav_developing.xml' */

'tt_developing_document_bl'=>'<b>Documents In Development</b>',
'vb_insert_document'=>'Insert Document',

/* './packages/policy/nav_obsolete.xml' */

'gc_file_name'=>'File / Link',
'tt_obsolete_documents_bl'=>'<b>Obsolete Versions of Documents</b>',

/* './packages/policy/nav_publish.xml' */

'tt_published_documents_bl'=>'<b>Published Documents</b>',

/* './packages/policy/nav_register.php' */

'st_remove_register_confirm'=>'Are you sure you wish to remove the Record  <b>%register_name%</b>?',
'tt_remove_register'=>'Remove Record',

/* './packages/policy/nav_register.xml' */

'gc_retention_time'=>'Retention Time',
'gc_storage_place'=>'Storage Place',
'gc_storage_type'=>'Storage Type',
'mi_read'=>'Read',
'mi_read_document'=>'Read Document',
'mi_readers'=>'Readers',
'vb_insert_register'=>'Insert Record',

/* './packages/policy/nav_report.php' */

'mx_documents_with_high_revision_frequency'=>'Documents with a high frequency of revision',
'mx_documents_without_registers'=>'Documents without Records',
'mx_pendant_task_pm'=>'Pending tasks',
'mx_report_documents_pendent_reads'=>'Documents with Reading Pending',
'mx_report_element_classification'=>'Classification Report',
'mx_users_with_pendant_read'=>'Users with pending readings',
'si_access_to_documents'=>'Access to Documents Audit',
'si_accessed_documents'=>'Users Access Audit',
'si_approvers'=>'Documents and their Approvers',
'si_documents_by_component'=>'Documents by PCI Component',
'si_documents_by_state'=>'Documents',
'si_documents_dates'=>'Creation and Revision Dates of Documents',
'si_documents_per_area'=>'Documents by Area',
'si_items_with_without_documents'=>'Components without Documents',
'si_most_accessed_documents'=>'Most Accessed Documents',
'si_most_revised_documents'=>'Most Revised Documents',
'si_not_accessed_documents'=>'Documents not accessed',
'si_pendant_approvals'=>'Pending Approvals',
'si_readers'=>'Documents and their Readers',
'si_registers_per_document'=>'Record by Document',
'si_users_per_process'=>'Users associated to Processes',

/* './packages/policy/nav_report.xml' */

'cb_expand_documents'=>'Expand Documents',
'cb_expand_readers'=>'Expand Readers',
'cb_organize_by_area'=>'Organize by area',
'cb_show_all_access'=>'Show all accesses',
'cb_show_register_documents'=>'Show Record Documents',
'lb_creation_date_cl'=>'Creation Date:',
'lb_documents_bl_cl'=>'<b>Documents:</b>',
'lb_final_date_cl'=>'Final Date:',
'lb_initial_date_cl'=>'Initial Date:',
'lb_last_revision_date_cl'=>'Last Revision Date:',
'lb_show_only_first_cl'=>'Show only first:',
'lb_until'=>'until',
'lb_users_bl_cl'=>'<b>Users:</b>',
'si_abnormalities_reports'=>'Abnormality Reports',
'si_access_control_reports'=>'Access Control Reports',
'si_all_status'=>'All',
'si_approved'=>'Published',
'si_developing'=>'In development',
'si_documents_by_type'=>'Documents by Type',
'si_general_reports'=>'General Reports',
'si_management_reports'=>'Management Reports',
'si_obsolete'=>'Obsolete',
'si_pendant'=>'In approval',
'si_register_reports'=>'Record Reports',
'si_registers_by_type'=>'Records by Type',
'si_revision'=>'In revision',
'st_asset'=>'Asset',
'st_components'=>'<b>Components:</b>',
'st_control'=>'Control',
'st_element_type_warning'=>'Please, select at least one type.',
'st_process'=>'Process',
'st_status_bl'=>'<b>Status:</b>',
'st_type_cl'=>'Type:',
'wn_component_filter_warning'=>'Please select at least one component type.',
'wn_no_filter_available'=>'No filter available.',
'wn_select_a_document'=>'Please, select a document.',

/* './packages/policy/nav_revision.xml' */

'mi_edit_author'=>'Edit Author',
'tt_revision_documents_bl'=>'<b>Documents in Revision</b>',

/* './packages/policy/nav_to_be_published.php' */

'st_document_confirm_remove'=>'Are you sure you wish to remove the document <b>%document_name%</b>?',
'tt_remove_document'=>'Remove Document',

/* './packages/policy/nav_to_be_published.xml' */

'gc_elements'=>'Items',
'gc_file_version'=>'Version',
'gc_sub_doc'=>'Subdoc',
'lb_classification'=>'Classification',
'lb_type'=>'Type',
'mi_approve'=>'Approve',
'mi_associate_components'=>'Associate Components',
'mi_comments'=>'Comments',
'mi_copy_readers'=>'Copy Readers',
'mi_details'=>'Details',
'mi_document_templates'=>'Document Templates',
'mi_edit_approvers'=>'Edit Approvers',
'mi_edit_readers'=>'Edit Readers',
'mi_previous_versions'=>'Previous Versions',
'mi_publish'=>'Publish',
'mi_references'=>'References',
'mi_registers'=>'Records',
'mi_send_to_approval'=>'Send for Approval',
'mi_send_to_revision'=>'Send for Revision',
'mi_sub_documents'=>'Subdocuments',
'mi_view_approvers'=>'View Approvers',
'mi_view_components'=>'View Components',
'mi_view_readers'=>'View Readers',
'tt_documents_to_be_published_bl'=>'<b>Documents to be Published</b>',

/* './packages/policy/popup_approvers_edit.xml' */

'tt_approvers_editing'=>'Approvers Editing',
'tt_curret_approvers_bl'=>'<b>Current Approvers</b>',

/* './packages/policy/popup_approvers_view.xml' */

'tt_approvers_visualization'=>'Approvers Visualization',

/* './packages/policy/popup_associate_users.xml' */

'gc_login'=>'Login',
'lb_login_cl'=>'Login:',
'tt_current_users_bl'=>'<b>Current Users</b>',
'tt_users_search'=>'Users Search',

/* './packages/policy/popup_create_document_confirm.xml' */

'rb_creation_document_question'=>'Do you want to create a document in the system and use it as a template for the record <b>%register_name%</b>?',
'rb_document_do_nothing'=>'Do not create / associate document to record <b>%register_name%</b>?',
'rb_document_relation_question'=>'Do you want to associate an existing document in the system and use it as a template for record <b>%register_name%</b>?',
'st_what_action_you_wish_to_do'=>'What action do you wish to perform?',
'tt_document_creation_or_association'=>'Document Adding/Association',

/* './packages/policy/popup_document_author_edit.xml' */

'tt_author_editing'=>'Author Edition',

/* './packages/policy/popup_document_context_associate.php' */

'st_permission_to_associate_item_document_denied'=>'You do not have permission to associate an item to a document of the "Management" or "Others" Type',

/* './packages/policy/popup_document_context_associate.xml' */

'tt_current_items_bl'=>'<b>Current Items</b>',
'tt_risk_management_items_association'=>'Risk Assessment Items Association',

/* './packages/policy/popup_document_context_view.xml' */

'tt_associated_risk_management_items_visualization'=>'Associated Risk Assessment Items Visualization',

/* './packages/policy/popup_document_copy_readers.xml' */

'tt_documents_search'=>'Documents Search',
'vb_copy_readers'=>'Copy Readers',

/* './packages/policy/popup_document_create.xml' */

'lb_document_name_bl_cl'=>'<b>Document Name:</b>',
'tt_new_document'=>'New Document',

/* './packages/policy/popup_document_edit.php' */

'si_no_type'=>'No Type',
'st_creation_date'=>'Creation Date:',
'st_document_approval_confirm'=>'Do you wish to send the document for approval?',
'st_download_file'=>'Download file:',
'st_publish_document_confirm'=>'Do you wish to publish the document?',
'st_publish_document_on_date_confirm'=>'Do you wish to publish the document on %date%?',
'st_size'=>'Size:',
'to_max_file_size'=>'Maximum file size: %max_file_size%',
'tt_document_adding'=>'Document Adding',
'tt_document_editing'=>'Document Editing',
'tt_publish'=>'Publish',
'tt_send_for_approval'=>'Send for Approval',

/* './packages/policy/popup_document_edit.xml' */

'lb_author_bl_cl'=>'<b>Author:</b>',
'lb_deadline_bl_cl'=>'<b>Deadline:</b><br/><br/>Deadline for document approval.',
'lb_deadline_cl'=>'Deadline:',
'lb_inform_in_advance_of'=>'Inform in advance of',
'lb_publication_date_bl_cl'=>'<b>Publication Date:</b>',
'lb_revision_cl'=>'Revision:',
'si_disable'=>'Disable',
'si_enable'=>'Enable',
'si_file'=>'File',
'si_link'=>'Link',
'st_days'=>'days.',
'st_general_information'=>'General Information',
'st_management_information'=>'Management Information',
'st_test'=>'test',
'to_doc_instance_manual_version'=>'<b>Manual Version:</b><br/>The manual version will only take effect when the document has a file or a link associated to it.',
'to_document_inform_in_advance'=>'<b>Inform in advance:</b><br/><br/>It is the number of days before deadline or revision in which an alert or task warning must be generated.',
'to_document_type_modification'=>'To modify the document type, you must remove the relationships between the document and the Risk Assessment Items.',
'wn_max_file_size_exceeded'=>'Maximum file size exceeded.',
'wn_publication_date_after_today'=>'The publication date must be the same or after today\'s date.',
'wn_upload_error'=>'Error trying to send file.',

/* './packages/policy/popup_document_instances.php' */

'st_copy_previous_version_confirm'=>'Are you sure you wish to copy the previous version?',
'tt_copy_previous_version'=>'Copy previous version',

/* './packages/policy/popup_document_instances.xml' */

'gc_end_date'=>'End Date',
'gc_modification_comment'=>'Modification Comment',
'gc_revision_justification'=>'Revision Reason',
'gc_start_date'=>'Start Date',
'gc_version'=>'Version',
'tt_previous_versions'=>'Previous Versions',
'wn_no_version_selected'=>'There is no selected version.',

/* './packages/policy/popup_document_manage.xml' */

'st_approver_list_only_when_developing'=>'The approvers list can only be modified when the document is in development.',
'st_items_association_not_management_or_others'=>'It is only possible to change the associations of a document which is not of "Management" or "Others" type.',
'st_items_association_only_when_developing'=>'It is only possible to modify the associations of a document which is in development.',
'st_readers_list_only_when_developing'=>'The readers list can only be edited when the document is in development.',
'st_revision_for_published_document_only'=>'Only a published document can be sent for revision',
'tt_associate_items_bl'=>'<b>Associate Items</b>',
'tt_edit_approvers_bl'=>'<b>Edit Approvers</b>',
'tt_edit_readers_bl'=>'<b>Edit Readers</b>',
'tt_manage_document'=>'Manage Document',
'tt_send_for_revision_bl'=>'<b>Send for Revision</b>',
'tt_view_approvers_bl'=>'<b>View Approvers</b>',
'tt_view_items_bl'=>'<b>View Items</b>',
'tt_view_readers_bl'=>'<b>View Readers</b>',
'vb_copy'=>'Copy',
'vb_send'=>'Send',

/* './packages/policy/popup_document_read.php' */

'st_remove_comment_confirm'=>'Are you sure you wish to remove the the referred comment?',
'tt_approve_document'=>'Approve Document',
'tt_remove_comment'=>'Remove Comment',

/* './packages/policy/popup_document_read.xml' */

'ti_comments'=>'Comments',
'ti_general'=>'General',
'ti_references'=>'References',
'ti_sub_documents'=>'Sub-Documents',
'tt_document_reading'=>'Document Reading',
'vb_approve'=>'Approve',
'vb_deny'=>'Deny',

/* './packages/policy/popup_document_read_comments.xml' */

'gc_author'=>'Author',
'gc_comment'=>'Comment',
'gc_date'=>'Date',
'tt_coments'=>'Comments',
'tt_comments_bl'=>'<b>Comments</b>',

/* './packages/policy/popup_document_read_details.xml' */

'lb_date_cl'=>'Date:',
'lb_keywords_cl'=>'Keywords:',
'lb_size_cl'=>'Size:',
'lb_version_cl'=>'Version:',
'tt_details'=>'Details',

/* './packages/policy/popup_document_read_subdocuments.xml' */

'tt_sub_documents'=>'Sub-Documents',

/* './packages/policy/popup_document_reference.php' */

'st_remove_reference_confirm'=>'Are you sure you wish to remove the reference <b>%doc_reference_name%</b>?',
'tt_remove_reference'=>'Remove Reference',

/* './packages/policy/popup_document_reference.xml' */

'gc_creation_date'=>'Creation Date',
'gc_link'=>'Link',
'lb_link_bl_cl'=>'<b>Link:</b>',
'st_http'=>'http://',
'tt_references'=>'References',
'tt_references_bl'=>'<b>References</b>',

/* './packages/policy/popup_document_register.xml' */

'tt_associated_registers_bl'=>'<b>Associated Records</b>',
'tt_registers'=>'Records',
'vb_disassociate'=>'Disassociate',

/* './packages/policy/popup_document_register_associate.xml' */

'gc_classification'=>'Classification',
'gc_place'=>'Place',
'gc_time'=>'Time',
'gc_type'=>'Type',
'tt_register_association'=>'Record Association',
'tt_registers_bl'=>'<b>Records</b>',
'vb_associate_registers'=>'Associate Records',
'wn_no_register_selected'=>'There is no selected record.',

/* './packages/policy/popup_document_register_edit.php' */

'tt_register_adding'=>'Record Adding',
'tt_register_editing'=>'Record Editing',

/* './packages/policy/popup_document_register_edit.xml' */

'lb_disposition_cl'=>'Disposition:',
'lb_indexing_type_cl'=>'Type of Indexing:',
'lb_origin_cl'=>'Origin:',
'lb_protection_requirements_cl'=>'Protection Requirements:',
'si_day_days'=>'Day(s)',
'si_month_months'=>'Month(s)',
'si_week_weeks'=>'Week(s)',
'vb_edit'=>'Edit',
'vb_lookup'=>'Search',
'wn_retention_time'=>'The retention time must be longer than zero',

/* './packages/policy/popup_document_register_read.xml' */

'lb_responsible'=>'Responsible',
'lb_retention_time_cl'=>'<b>Retention Time:</b>',
'lb_storage_place_cl'=>'Storage Place:',
'lb_storage_type_cl'=>'Storage Type:',
'tt_register_reading'=>'Record Reading',
'vb_preferences'=>'Preferences',

/* './packages/policy/popup_document_revise_not_alter.xml' */

'tt_revise_document_without_altering'=>'Revise Document without Altering',

/* './packages/policy/popup_document_revision_task.php' */

'mx_scheduled_revision'=>'Scheduled Revision',

/* './packages/policy/popup_document_revision_task.xml' */

'lb_author_cl'=>'Author:',
'lb_file_cl'=>'File:',
'lb_link_cl'=>'Link:',
'tt_document_revision'=>'Document Revision',
'vb_create_new_version'=>'Create New Version',
'vb_download'=>'Download',
'vb_revise_without_altering'=>'Revise without Altering',
'vb_visit_link'=>'Visit Link',
'vb_visualize'=>'View',

/* './packages/policy/popup_document_search.xml' */

'tt_document_search'=>'Document Search',
'wn_no_document_selected'=>'There is no selected document.',

/* './packages/policy/popup_documents_search.xml' */

'tt_current_documents_bl'=>'<b>Current Documents</b>',

/* './packages/policy/popup_modification_comment.php' */

'tt_popup_modification_comment_creation'=>'Creation Comment',
'tt_popup_modification_comment_creationfasdfawfqwerqwe'=>'Creation Comment',

/* './packages/policy/popup_modification_comment.xml' */

'lb_comment_bl_cl'=>'<b>Comment:</b>',
'tt_comment_modification'=>'Comment Modification',
'tt_comment_modification2'=>'Comment Modification',

/* './packages/policy/popup_readers_edit.xml' */

'si_all_processes'=>'All Processes',
'tt_current_readers_bl'=>'<b>Current Readers</b>',
'tt_readers_editing'=>'Readers Editing',

/* './packages/policy/popup_readers_view.xml' */

'tt_readers_visualization'=>'Readers Visualization',

/* './packages/policy/popup_register_readers_edit.xml' */

'tt_current_readers'=>'<b>Current Readers</b>',
'tt_readers_edit'=>'Readers Editing',

/* './packages/policy/popup_revision_justification.xml' */

'tt_revision_justification'=>'Revision Reason',
'vb_send_for_revision'=>'Send for Revision',

/* './packages/policy/popup_subdocuments_management.php' */

'st_create_subdocument'=>'Create Subdocument',

/* './packages/policy/popup_subdocuments_management.xml' */

'tt_manage_subdocuments'=>'Manage Subdocuments',

/* './packages/policy/popup_template_search.xml' */

'rb_suggested_by_best_practice_only'=>'show only the templates of suggested requirements',
'rb_suggested_only'=>'Suggested Only',
'tt_document_templates_search'=>'Document Templates Search',

/* './packages/policy/report/popup_report_accessed_documents.php' */

'rs_accessed_documents'=>'User Access Audit',
'rs_management'=>'Management',
'rs_others'=>'Others',

/* './packages/policy/report/popup_report_accessed_documents.xml' */

'rs_type'=>'Type',

/* './packages/policy/report/popup_report_contexts_without_documents.php' */

'rs_items_with_without_documents'=>'Components without Documents',

/* './packages/policy/report/popup_report_contexts_without_documents.xml' */

'rs_components'=>'Components',

/* './packages/policy/report/popup_report_doc_instance_comment_and_justification.php' */

'mx_doc_instance_comment_and_justification'=>'Documents\' Comments and Reasons',

/* './packages/policy/report/popup_report_doc_instance_comment_and_justification.xml' */

'rs_header_instance_modify_comment'=>'Modification Comment',
'rs_header_instance_rev_justification'=>'Revision Reason',

/* './packages/policy/report/popup_report_docs_with_high_frequency_revision.php' */

'tt_docs_with_high_frequency_revision'=>'Documents with a high frequency of revision',

/* './packages/policy/report/popup_report_docs_with_high_frequency_revision.xml' */

'rs_justification'=>'Reason',

/* './packages/policy/report/popup_report_document_accesses.php' */

'rs_accesses_to_documents'=>'Access to Documents Audit',
'rs_no_type'=>'No Type',

/* './packages/policy/report/popup_report_document_accesses.xml' */

'rs_date'=>'Date',
'rs_time'=>'Time',

/* './packages/policy/report/popup_report_document_approvers.php' */

'rs_approvers'=>'Documents and their Approvers',

/* './packages/policy/report/popup_report_document_dates.php' */

'rs_documents_dates'=>'Creation and Revision Dates of Documents',

/* './packages/policy/report/popup_report_document_dates.xml' */

'rs_created_on_cl'=>'Created on:',

/* './packages/policy/report/popup_report_document_readers.php' */

'rs_readers'=>'Documents and their Readers',

/* './packages/policy/report/popup_report_document_readers.xml' */

'rs_author_cl'=>'Author:',

/* './packages/policy/report/popup_report_document_registers.php' */

'rs_registers_per_document'=>'Record by Document',

/* './packages/policy/report/popup_report_document_registers.xml' */

'rs_name'=>'Name',
'rs_retention_time'=>'Retention Time',

/* './packages/policy/report/popup_report_document_summary.php' */

'mx_report_document_summary'=>'Document Properties',

/* './packages/policy/report/popup_report_documents_by_area.php' */

'rs_documents_per_area'=>'Documents by Area',

/* './packages/policy/report/popup_report_documents_by_area.xml' */

'rs_document_documents'=>'document(s)',

/* './packages/policy/report/popup_report_documents_by_component.php' */

'rs_document_of_component'=>'%component% documents',
'tt_documents_by_component'=>'Documents by PCI Component',

/* './packages/policy/report/popup_report_documents_by_state.php' */

'tt_documents_by_state'=>'Documents',

/* './packages/policy/report/popup_report_documents_by_type.php' */

'rs_documents_by_type'=>'Documents by Type',

/* './packages/policy/report/popup_report_documents_revision_late.php' */

'mx_documents_revision_late'=>'Documents with Delayed Revision',

/* './packages/policy/report/popup_report_documents_revision_late.xml' */

'rs_document_next_revision'=>'Next Revision',

/* './packages/policy/report/popup_report_documents_revision_schedule.php' */

'mx_documents_revision_schedule'=>'Documents Revision Schedule',

/* './packages/policy/report/popup_report_documents_revision_schedule.xml' */

'rs_document_name'=>'Name',
'rs_document_schedule'=>'Scheduling',

/* './packages/policy/report/popup_report_documents_without_register.php' */

'rs_document_main_approver_name'=>'Responsible for document',
'rs_document_name_bl'=>'Document',
'tt_documents_without_register'=>'Documents without Record',

/* './packages/policy/report/popup_report_not_accessed_documents.php' */

'rs_not_accessed_documents'=>'Documents Not Accessed',

/* './packages/policy/report/popup_report_pendant_approvals.php' */

'tt_pendant_approvals'=>'Pending Approvals',

/* './packages/policy/report/popup_report_pendant_approvals.xml' */

'rs_documents'=>'Documents',

/* './packages/policy/report/popup_report_registers_by_type.php' */

'rs_registers_by_type'=>'Records by Type',

/* './packages/policy/report/popup_report_top_accessed_documents.php' */

'rs_most_accessed_documents'=>'Most Accessed Documents',

/* './packages/policy/report/popup_report_top_accessed_documents.xml' */

'rs_accesses_cl'=>'Accesses:',

/* './packages/policy/report/popup_report_top_revised_documents.php' */

'rs_most_revised_documents'=>'Most Revised Documents',

/* './packages/policy/report/popup_report_top_revised_documents.xml' */

'rs_revision_date'=>'Revision Date',
'rs_revisions_cl'=>'Revisions:',
'rs_version'=>'Version',

/* './packages/policy/report/popup_report_user_comments.php' */

'mx_user_comments'=>'Users Comments',

/* './packages/policy/report/popup_report_user_comments.xml' */

'rs_comment_date'=>'Date',
'rs_comment_text'=>'Comment',
'rs_comment_user'=>'User',

/* './packages/policy/report/popup_report_users_by_process.php' */

'rs_users_per_process'=>'Users associated to Processes',

/* './packages/policy/report/popup_report_users_by_process_and_area.xml' */

'rs_user_bl_cl'=>'<b>User:</b>',

/* './packages/policy/report/popup_report_users_with_pendant_read.php' */

'mx_documents_pendent_reads'=>'Users with pending readings',

/* './packages/policy/tab_policy_management.xml' */

'ti_all'=>'All',
'ti_developing'=>'In Development',
'ti_obsolete'=>'Obsolete',
'ti_publish'=>'Published',
'ti_registers'=>'Records',
'ti_revision'=>'In Revision',
'ti_to_approve'=>'In Approval',
'ti_to_be_published'=>'To be Published',

/* './packages/risk/nav_area.php' */

'st_area_remove_confirm'=>'Are you sure you wish to remove the area <b>%area_name%</b>?',
'tt_remove_area'=>'Remove Area',

/* './packages/risk/nav_area.xml' */

'gc_subarea'=>'Sub-Areas',
'lb_sub_areas_count_cl'=>'Number of Sub-Areas:',
'mi_processes'=>'Processes',
'mi_sub_areas'=>'Sub-Areas',
'tt_business_areas_bl'=>'<b>Business Areas</b>',

/* './packages/risk/nav_asset.php' */

'grid_asset_filter_dependents'=>'(dependencies of asset \'<b>%asset_name%</b>\')',
'st_asset_copy'=>'Copy',
'st_asset_remove_confirm'=>'Are you sure you wish to remove the Asset <b>%asset_name%</b>?',
'tt_remove_asset'=>'Remove Asset',

/* './packages/risk/nav_asset.xml' */

'gc_asset_category'=>'Asset category',
'gc_asset_dependencies'=>'Dep.(cies)',
'gc_asset_dependents'=>'Dep.(ents)',
'gc_processes'=>'Processes',
'lb_process_count_cl'=>'Number of Processes:',
'mi_associate_processes'=>'Associate Processes',
'mi_duplicate'=>'Duplicate',
'mi_edit_dependencies'=>'Adjust Dependencies',
'mi_edit_dependents'=>'Adjust Dependents',
'mi_insert_risk'=>'Insert Risk',
'mi_load_risks_from_library'=>'Load risks from library',
'tt_assets_bl'=>'<b>Assets</b>',

/* './packages/risk/nav_control.php' */

'st_control_remove_confirm'=>'Are you sure you wish to remove the Control <b>%control_name%</b>?',

/* './packages/risk/nav_control.xml' */

'gc_cost'=>'Cost',
'gc_icon'=>'Icon',
'gc_risks'=>'Risks',
'lb_risk_count_cl'=>'Number of Risks:',
'mi_associate_risks'=>'Associate Risks',
'mi_view_risks'=>'View Risks',
'rb_active'=>'Activated',
'rb_not_active'=>'Deactivated',
'tt_controls_directory_bl'=>'<b>Controls Directory</b>',
'vb_revision_and_test_history'=>'Revision/Test History',

/* './packages/risk/nav_process.php' */

'grid_process_asset_filter'=>'(filtered by asset \'<b>%asset_name%</b>\')',
'st_process_remove_confirm'=>'Are you sure you wish to remove the Process <b>%process_name%</b>?',
'tt_remove_process'=>'Remove Process',

/* './packages/risk/nav_process.xml' */

'cb_not_estimated'=>'Not Estimated',
'gc_assets'=>'Assets',
'gc_responsible'=>'Responsible',
'gc_users'=>'Users',
'lb_asset_count_cl'=>'Number of Assets:',
'lb_responsible_cl'=>'Responsible:',
'lb_value_cl'=>'Risk:',
'mi_add_document'=>'Create Document',
'mi_associate_assets'=>'Associate Assets',
'mi_documents'=>'Documents',
'mi_view_assets'=>'View Assets',
'si_all_priorities'=>'All',
'si_all_responsibles'=>'All',
'si_all_types'=>'All',
'tt_processes_bl'=>'<b>Processes</b>',

/* './packages/risk/nav_report.php' */

'mx_no_priority'=>'No Priority',
'mx_no_type'=>'No Type',
'si_amount_of_risks_per_area'=>'Amount of Risks by Area',
'si_amount_of_risks_per_process'=>'Amount of Risks by Process',
'si_areas_without_processes'=>'Areas without Processes',
'si_assets'=>'Assets',
'si_assets_importance'=>'Assets Importance',
'si_assets_without_risks'=>'Assets without Risk Events',
'si_checklist_per_event'=>'Checklist by Asset',
'si_controls_efficiency'=>'Controls Efficiency',
'si_controls_implementation_date'=>'Controls Implementation Date',
'si_controls_per_responsible'=>'Controls by Responsible',
'si_controls_per_risk'=>'Controls by Risk',
'si_controls_without_risks'=>'Controls without Associated Risks',
'si_cost_of_controls_per_area'=>'Cost of Controls by Area',
'si_cost_of_controls_per_asset'=>'Cost of Controls by Asset',
'si_cost_of_controls_per_process'=>'Cost of Controls by Process',
'si_cost_of_controls_per_responsible'=>'Cost of Controls by Responsible',
'si_cost_per_control'=>'Cost by Control',
'si_duplicated_risks'=>'Duplicate Risks',
'si_element_classification'=>'Items Classification',
'si_follow_up_of_controls'=>'Follow Up of Controls',
'si_follow_up_of_controls_efficiency'=>'Follow Up of Controls Efficiency',
'si_follow_up_of_controls_test'=>'Follow Up of Controls Testing',
'si_non_parameterized_risks'=>'Non-Estimated Risks',
'si_not_measured_controls'=>'Not Measured Controls',
'si_pending_items'=>'Pending Items',
'si_pending_tasks'=>'Pending Tasks',
'si_plan_of_risks_treatment'=>'Risk Treatment Plan',
'si_planning_of_controls'=>'Planning of Controls',
'si_processes_without_assets'=>'Processes without Assets',
'si_report_conformity'=>'Conformity',
'si_risk_impact'=>'Risk Impact',
'si_risk_status_per_area'=>'Risk Status by Area',
'si_risk_status_per_process'=>'Risk Status by Process',
'si_risks_per_area'=>'Risks by Area',
'si_risks_per_asset'=>'Risks by Asset',
'si_risks_per_control'=>'Risks by Control',
'si_risks_per_process'=>'Risks by Process',
'si_risks_values'=>'Risks Values',
'si_sgsi_policy_statement'=>'PCI\'s Policy Statement',
'si_sgsi_scope_statement'=>'PCI\'s Scope Statements',
'si_statement_of_applicability'=>'Statement of Applicability',
'si_summary_of_controls'=>'Summary of Controls',
'si_top_10_assets_with_highest_risks'=>'Top 10 Assets with the most High Risks',
'si_top_10_assets_with_most_risks'=>'Top 10 Assets with more Risks',
'si_top_10_risks_per_area'=>'Top 10 Risks by Area',
'si_top_10_risks_per_process'=>'Top 10 Risks by Process',
'si_users_responsibilities'=>'Users Responsibilities',
'st_all_standards'=>'All',
'st_clear_justificatives_confirm'=>'Are you sure you want to erase the Reasons?',
'tt_clear_justificatives'=>'Confirm deletion of Reasons',

/* './packages/risk/nav_report.xml' */

'cb_all'=>'All',
'cb_area'=>'Area',
'cb_asset'=>'Asset',
'cb_asset_security'=>'Asset Security',
'cb_control'=>'Control',
'cb_high'=>'High',
'cb_low'=>'Low',
'cb_medium'=>'Medium',
'cb_not_parameterized'=>'Not estimated',
'cb_organize_by_asset'=>'Organize by Asset',
'cb_organize_by_process'=>'Organize by Process',
'cb_process'=>'Process',
'lb_classification_cl'=>'Classification:',
'lb_comment_cl'=>'Comment:',
'lb_consider_cl'=>'Consider:',
'lb_filter_by_cl'=>'Filter by:',
'lb_filter_by_standard_cl'=>'Filter by Standard:',
'lb_filter_cl'=>'Filter:',
'lb_process_cl'=>'Process:',
'lb_reponsible_for_cl'=>'Responsible for:',
'lb_values_cl'=>'Values:',
'rb_area'=>'Area',
'rb_area_process'=>'Area and Process',
'rb_asset'=>'Asset',
'rb_both'=>'Both',
'rb_detailed'=>'Detailed',
'rb_efficient'=>'Efficient',
'rb_implemented'=>'Implemented',
'rb_inefficient'=>'Inefficient',
'rb_planned'=>'Planned',
'rb_process'=>'Process',
'rb_real'=>'Potential',
'rb_residual'=>'Residual',
'rb_summarized'=>'Summarized',
'si_all_reports'=>'All Reports',
'si_areas_by_priority'=>'Areas by Priority',
'si_areas_by_type'=>'Areas by Type',
'si_control_reports'=>'Control Reports',
'si_controls_by_type'=>'Controls by Type',
'si_events_by_type'=>'Events by Type',
'si_financial_reports'=>'Financial Reports',
'si_iso_27001_reports'=>'Compliance Reports',
'si_others'=>'Others',
'si_processes_by_priority'=>'Processes by Priority',
'si_processes_by_type'=>'Processes by Type',
'si_risk_management_reports'=>'Risk Assessment Reports',
'si_risk_reports'=>'Risk Reports',
'si_risks_by_type'=>'Risks by Type',
'si_summaries'=>'Summaries',
'st_area_priority'=>'Area Priorities',
'st_area_type'=>'Area Types',
'st_asset_cl'=>'Asset:',
'st_config_filter_type_priority'=>'Configure Selected Filter:',
'st_control_type'=>'Control Types',
'st_event_type'=>'Event Types',
'st_no_filter_available'=>'No filters available.',
'st_none_selected'=>'Select a Filter',
'st_organize_by_asset'=>'Organize by Asset',
'st_process_priority'=>'Process Priorities',
'st_process_type'=>'Process Types',
'st_risk_type'=>'Risk Types',
'st_show_cl'=>'Show:',
'st_standard_cl'=>'Standard:',
'tt_specific_report_filters'=>'Report Specific Filters',
'tt_type_and_priority_filter_cl'=>'Type and Priority Filters:',
'vb_clear_pre_report'=>'Erase Reasons',
'vb_pre_report'=>'Pre-Report',
'vb_report'=>'Report',
'wn_no_report_selected'=>'There is no selected report.',
'wn_select_at_least_one_filter'=>'At least one filter must be selected!',
'wn_select_at_least_one_parameter'=>'At least a parameter should be selected.',
'wn_select_risk_value'=>'Please, select a risk value to be displayed.',

/* './packages/risk/nav_risk.php' */

'gs_avoided'=>'Avoided',
'gs_reduced'=>'Reduced',
'gs_restrained'=>'Retained',
'gs_transferred'=>'Transferred',
'st_denied_permission_to_edit'=>'You do not have permission to edit the',
'st_risk_remove_confirm'=>'Are you sure you wish to remove the risk <b>%risk_name%</b>?',
'st_risk_remove_has_incident'=>'<br><br><b>Atention: The risk has incident(s) associated to it.</b>',
'st_risk_remove_many_confirm'=>'Are you sure you want to remove the <b>%count%</b> selected risks?',
'st_risk_remove_many_have_incident'=>'<br><br><b>Attention: At least one risk has incident(s) associated to it.</b>',
'tt_incident_filter'=>'filtered by incident \'<b>%incident_name%</b>\'',
'tt_remove_risk'=>'Remove Risk',

/* './packages/risk/nav_risk.xml' */

'cb_avoided'=>'Avoided',
'cb_not_treated'=>'Not Treated',
'cb_reduced'=>'Reduced',
'cb_restrained'=>'Retained',
'cb_search_in_name_only'=>'search only in name',
'cb_transferred'=>'Transfered',
'gc_controls'=>'Controls',
'gc_residual'=>'Residual',
'gc_risk'=>'Risk',
'gc_risk_name'=>'Name',
'gc_status'=>'Status',
'gc_treatment'=>'Treatment',
'lb_control_count_cl'=>'Number of Controls:',
'lb_name_description_cl'=>'Name/Description',
'lb_risk_residual_value_cl'=>'Residual:',
'lb_risk_value_cl'=>'Risk:',
'lb_status_cl'=>'Status:',
'lb_treatment_type_cl'=>'Treatment Type:',
'mi_avoid'=>'Avoid Risk',
'mi_cancel_treatment'=>'Cancel Treatment',
'mi_hold_accept'=>'Retain/Accept Risk',
'mi_reduce_risk'=>'Reduce Risk',
'mi_transfer'=>'Transfer Risk',
'mi_view_controls'=>'View Controls',
'si_all_assets'=>'All',
'si_all_states'=>'All',
'st_from'=>'from',
'st_to'=>'to',
'vb_aply'=>'Apply',
'vb_parameterize'=>'Estimate',

/* './packages/risk/popup_area_edit.php' */

'tt_area_adding'=>'Area Adding',
'tt_area_editing'=>'Area Editing',

/* './packages/risk/popup_area_edit.xml' */

'vb_ok'=>'Ok',

/* './packages/risk/popup_asset_dependencies.php' */

'st_dependencies_list'=>'Dependencies List',
'st_dependents_list'=>'Dependents List',
'tt_dependencies_adjustment'=>'Dependencies Adjustment',
'tt_dependents_adjustment'=>'Dependents Adjustment',

/* './packages/risk/popup_asset_dependencies.xml' */

'tt_assets_list'=>'Assets List',

/* './packages/risk/popup_asset_edit.php' */

'tt_asset_adding'=>'Asset Adding',
'tt_asset_duplicate'=>'Asset Duplication',
'tt_asset_editing'=>'Asset Editing',

/* './packages/risk/popup_asset_edit.xml' */

'lb_category_bl_cl'=>'Category',
'lb_demands_attention_to_legal_conformity_cl'=>'Demands attention to Legal Conformity:',
'lb_justification_cl'=>'Reason:',
'lb_security_responsible_bl_cl'=>'<b>Security Responsible:</b>',
'lb_value_cl_money_symbol'=>'Cost',
'st_asset_parametrization_bl'=>'<b>Asset Relevance</b>',
'vb_adjust_dependencies'=>'Adjust Dependencies',
'vb_adjust_dependents'=>'Adjust Dependents',

/* './packages/risk/popup_asset_search.xml' */

'wn_no_asset_selected'=>'There is no selected asset.',

/* './packages/risk/popup_best_practice_search.xml' */

'lb_section_cl'=>'Section:',
'si_all'=>'All',
'tt_best_practices_search'=>'Requirements Search',

/* './packages/risk/popup_control_cost.xml' */

'lb_total'=>'Total',
'tt_control_cost'=>'Control Cost',

/* './packages/risk/popup_control_edit.php' */

'st_and'=>'and',
'st_of_revision'=>'of revision',
'st_of_test'=>'of test',
'st_remove_control_implementation_confirm'=>'Are you sure you wish to remove the implementation of control <b>%control_name%</b>?',
'st_result_in_removing'=>'This will result in removing',
'st_this_control'=>'this control',
'tt_control_adding'=>'Control Adding',
'tt_remove_control_implementation'=>'Remove control implementation',

/* './packages/risk/popup_control_edit.xml' */

'cb_enable_control_revision_cl'=>'Enable Control Revision:',
'cb_enable_control_tests_cl'=>'Enable Control Tests:',
'lb_best_practices_bl_cl'=>'<b>Requirements:</b>',
'lb_control_cost_cl'=>'Control Cost:',
'lb_evidence_cl'=>'Evidence:',
'lb_implementation_limit_bl_cl'=>'<b>Implementation Limit:</b>',
'lb_implemented_on_cl'=>'Implemented on:',
'lb_send_alert_task_bl_cl'=>'<b>Send Alert/Task:</b>',
'st_day_days_before'=>'day(s) before',
'st_money_symbol'=>'$',
'tt_control_editing'=>'Control Editing',
'vb_add'=>'Add',
'vb_control_cost'=>'Control Cost',
'vb_implement'=>'Implement',
'vb_remove_implementation'=>'Remove Implementation',
'vb_revision'=>'Revision',
'vb_tests'=>'Tests',
'wn_implementation_cannot_be_null'=>'The implementation date cannot be null, if you want to remove the implementation, use the appropriate button to execute this event.',
'wn_implementation_limit_after_today'=>'The deadline must be the same or after current date.',

/* './packages/risk/popup_control_revision.xml' */

'lb_expected_efficiency_bl_cl'=>'<b>Expected Efficiency:</b>',
'lb_high_value_5_cl'=>'(High) Value 5:',
'lb_low_value_1_cl'=>'(Low) Value 1:',
'st_helper'=>'[?]',
'to_control_revision_expected_efficiency'=>'<b>Expected Efficiency:</b><br/><br/>Determine which service level is expected for the control in the specified period.<br/><br/>Consider the necessary performance to reduce risks associated to this control.',
'to_control_revision_metric_help'=>'<b>Metric:</b><br/><br/>Define how to measure this control.',
'to_control_revision_value'=>'<b>Values:</b><br/><br/>Determine a scale for the possible service levels of this control in the period.<br/><br/>Consider the low values as a bad performance, and for higher values, the best possible performance for the control.',
'tt_control_efficiency_revision'=>'Control Efficiency Revision',

/* './packages/risk/popup_control_revision_and_test_edit.php' */

'st_control_test_not_ok'=>'Not Ok',
'st_control_test_ok'=>'Ok',
'st_revision_remove_confirm'=>'Are you sure you want to remove the revision with Estimated Date on <b>%deadline_date%</b> and Accomplishment Date on <b>%realized_date%</b> ?',
'st_revision_update_by_insert_confirm'=>'There is a control revision on the Estimated Date on <b>%date_to_do%</b>. Are you sure you want to substitute this control revision?',
'st_test_remove_confirm'=>'Are you sure you want to remove the test with Estimated Date on <b>%deadline_date%</b> and Accomplishment Date on <b>%realized_date%</b> ?',
'st_test_update_by_insert_confirm'=>'There is a control test on the Estimated Date on <b>%date_to_do%</b>. Are you sure you want to substitute this control test?',
'tt_control_efficiency_history_add_title'=>'Insert Control Revision',
'tt_control_test_history_add_title'=>'Insert Control Test',
'tt_remove_control'=>'Remove Control Implementation',
'tt_replace_control_revision'=>'Replacement of Revision of Control <b>%control_name%</b>',
'tt_replace_control_test'=>'Replacement of Test of Control <b>%control_name%</b>',

/* './packages/risk/popup_control_revision_and_test_edit.xml' */

'gc_date_realized'=>'Accomplishment Date',
'gc_date_to_do'=>'Estimated Date',
'gc_expected_efficiency'=>'EE',
'gc_real_efficiency'=>'RE',
'gc_realization_date'=>'Accomplishment Date',
'gc_test_date_to_do'=>'Estimated Date',
'gc_test_value'=>'Test Value',
'mi_edit'=>'Edit',
'mi_remove'=>'Remove',
'mi_revision_edit'=>'Edit Revisions',
'mi_test_edit'=>'Edit Tests',
'rb_not_ok'=>'Not Ok',
'rb_ok'=>'Ok',
'st_control_test_result'=>'<b>Test:</b>',
'st_date_realized'=>'<b>Accomplishment Date:</b>',
'st_date_to_do'=>'<b>Estimated Date:</b>',
'st_description'=>'Description:',
'st_er'=>'RE',
'test_date_to_do'=>'<b>Estimated Date</b>',
'tt_add_and_edit_revision_and_test_control'=>'Adding or Editing Control Test and Revision',
'tt_control_efficiency_history_edit_title'=>'Edit Control Revision',
'tt_control_selected_revision_history'=>'Control revision history',
'tt_control_selected_test_history'=>'Control test history',
'tt_control_test_history_edit_title'=>'Edit Control Test',
'vb_insert'=>'Insert',

/* './packages/risk/popup_control_revision_task.php' */

'tt_control_revision_task_incident_revision'=>'Induced Measurement of Control Efficiency (by Incident %incident_name%)',

/* './packages/risk/popup_control_revision_task.xml' */

'lb_from'=>'from',
'lb_start_cl'=>'Start:',
'lb_to'=>'to',
'lb_value_1_cl'=>'Value 1:',
'lb_value_2_cl'=>'Value 2:',
'lb_value_3_cl'=>'Value 3:',
'lb_value_4_cl'=>'Value 4:',
'lb_value_5_cl'=>'Value 5:',
'st_ee'=>'EE',
'st_er_bl'=>'<b>RE</b>',
'st_high_parenthesis'=>'(High)',
'st_low_parenthesis'=>'(Low)',
'st_metric'=>'Metric',
'st_total_incidents_found_on_period'=>'Total of incidents found in the period:',
'st_total_incidents_on_time'=>'Total of incidents in the current time frame:',
'to_expected_efficiency'=>'Expected Efficiency',
'to_real_efficiency'=>'Real Efficiency',
'tt_control_revision_task'=>'Control review',
'tt_incidents_occurred_from_period'=>'Incidents occurred from %date_begin% to %date_today%',
'vb_filter'=>'Filter',

/* './packages/risk/popup_control_risk_edit.php' */

'tt_risk_control_association_editing'=>'Risk Control Association Editing',

/* './packages/risk/popup_control_risk_edit.xml' */

'tt_risks_bl'=>'<b>Risks</b>',
'vb_associate_risk'=>'Associate Risk',
'vb_disassociate_risks'=>'Disassociate Risks',

/* './packages/risk/popup_control_risk_search.xml' */

'gc_asset'=>'Asset',
'tt_risks_search'=>'Risks Search',
'vb_associate_risks'=>'Associate Risks',
'wn_no_risk_selected'=>'There is no selected risk.',

/* './packages/risk/popup_control_search.xml' */

'vb_associate'=>'Associate',

/* './packages/risk/popup_control_test.xml' */

'lb_schedule_bl_cl'=>'<b>Scheduling</b>',
'tt_control_test_revision'=>'Control Test Revision',

/* './packages/risk/popup_control_test_task.xml' */

'cb_no'=>'No',
'cb_open_next_task'=>'Open next task',
'cb_yes'=>'Yes',
'lb_description_test'=>'Test description:',
'lb_observations_cl'=>'Observations:',
'lb_test_ok_bl_cl'=>'<b>Test ok:</b>',
'tt_control_test'=>'Control Test',

/* './packages/risk/popup_create_risks_from_category.xml' */

'tt_risk_creation_from_category'=>'Risk Creation from an event category',

/* './packages/risk/popup_create_risks_from_events.xml' */

'st_select_events_not_to_associate'=>'Select below only the Events that you <b>DON\'T</b> want to relate to asset!',
'tt_risk_creation_from_events'=>'Risk Creation from Events',
'vb_link_events_later'=>'Link Events Later',
'vb_relate_events_to_asset'=>'Link Events to Asset',

/* './packages/risk/popup_process_asset_association.xml' */

'tt_assets_search'=>'Assets Search',
'tt_current_assets_bl'=>'<b>Current Assets</b>',

/* './packages/risk/popup_process_edit.php' */

'st_no_published_documents'=>'There are no published documents',
'tt_process_adding'=>'Process Adding',
'tt_process_editing'=>'Process Editing',

/* './packages/risk/popup_process_edit.xml' */

'lb_area_bl_cl'=>'<b>Area:</b>',
'lb_document_cl'=>'Document:',
'lb_name_bl_cl'=>'<b>Name:</b>',
'lb_priority_cl'=>'Priority:',
'lb_responsible_bl_cl'=>'<b>Responsible:</b>',
'vb_associate_users'=>'Associate Users',
'vb_properties'=>'Properties',
'vb_view'=>'View',

/* './packages/risk/popup_process_search.xml' */

'gc_area'=>'Area',
'lb_area_cl'=>'Area:',
'si_all_areas'=>'All Areas',
'tt_current_processes_bl'=>'<b>Current Processes</b>',
'tt_processes_search'=>'Processes Search',
'vb_remove'=>'Remove',

/* './packages/risk/popup_risk_control_association_edit.php' */

'st_control_not_mitigating_efficiency_message'=>'The Control is not reducing Risks because the Real Efficiency is lower than the Expected Efficiency!',
'st_control_not_mitigating_not_implemented_message'=>'The Control is not reducing Risks because it is not implemented and the deadline date of implementation has expired!',

/* './packages/risk/popup_risk_control_association_edit.xml' */

'lb_control_cl'=>'Control:',
'st_control_parametrization_bl_cl'=>'<b>Control Estimation:</b>',
'st_values_to_reduce_risk_consequence_and_probability'=>'The values on the side will define by how much this control will reduce the consequence and likelihood of the actual risk.',
'tt_risk_reduction'=>'Risk Reduction',

/* './packages/risk/popup_risk_control_edit.php' */

'tt_control_risk_association_editing'=>'Risk-Control Association Editing',

/* './packages/risk/popup_risk_control_edit.xml' */

'lb_risk_cl'=>'Risk:',
'mi_edit_association'=>'Edit Association',
'st_residual'=>'Residual',
'st_risk'=>'Risk',
'tt_controls_bl'=>'<b>Controls</b>',
'vb_disassociate_controls'=>'Disassociate Controls',

/* './packages/risk/popup_risk_control_search.xml' */

'cb_search_only_suggested_controls'=>'Search only suggested Controls',
'gc_name'=>'Name',
'lb_name_cl'=>'Name:',
'tt_control_search'=>'Control Search',
'vb_associate_control'=>'Associate Control',
'wn_no_control_selected'=>'There is no selected control.',

/* './packages/risk/popup_risk_edit.php' */

'st_sensitive_data_reapproval'=>'You are changing sensitive data which needs reapproval. Do you wish to confirm changes?',
'tt_reapproval'=>'Reapproval',
'tt_risk_editing'=>'Risk Editing',

/* './packages/risk/popup_risk_edit.xml' */

'lb_asset_bl_cl'=>'<b>Asset:</b>',
'lb_event_bl_cl'=>'<b>Event:</b>',
'lb_impact_cl_money_symbol'=>'Financial Impact:',
'lb_real_risk_bl_cl'=>'<b>Potential Risk:</b>',
'lb_residual_risk_bl_cl'=>'<b>Residual Risk:</b>',
'lb_type_cl'=>'Type:',
'si_select_one_of_the_items_below'=>'Select one of the items below',
'st_risk_parametrization_bl_cl'=>'<b>Risk Estimation:</b>',
'static_static19'=>'Impact:',
'vb_calculate_risk'=>'Calculate Risk',
'vb_config'=>'Configure',
'vb_find'=>'Find',
'vb_probability_automatic_calculation'=>'Automatic likelihood calculation',
'warning_wn_asset_desparametrized'=>'It is not possible to calculate the risk value, because the asset related to the risk is not estimated!',
'warning_wn_control_desparametrized'=>'at least one control is not estimated. Before calculating the residual risk, estimate the control(s) related to this risk!',

/* './packages/risk/popup_risk_insert_search_event_asset.xml' */

'gc_category'=>'Category',
'gc_description'=>'Description',
'gc_impact'=>'Impact',
'lb_asset_cl'=>'Asset:',
'lb_category_cl'=>'Category:',
'lb_description_cl'=>'Description:',
'si_all_categories'=>'All Categories',
'tt_events_search'=>'Events Search',
'vb_create_risk'=>'Create Risk',
'vb_search'=>'Search',
'wn_no_event_selected'=>'There is no selected event.',

/* './packages/risk/popup_risk_insert_type.xml' */

'rb_create_independent_risk'=>'Create an independent risk',
'st_which_action_to_take'=>'Which action do you wish to take?',
'tt_risk_adding'=>'Risk Adding',
'vb_cancel'=>'Cancel',
'vb_create'=>'Create',
'vb_create_risk_from_event'=>'Create risk from an event (recommended)',

/* './packages/risk/popup_risk_parameters_weight.xml' */

'tt_risk_parameters_weight'=>'Risk Parameters Weight',

/* './packages/risk/popup_risk_parametrization.php' */

'mx_probability'=>'Likelihood',
'st_denied_permission_to_estimate_risks'=>'You don\'t have permission to estimate several risks',
'to_impact_cl'=>'Impact:',
'to_risk_cl'=>'Risk:',

/* './packages/risk/popup_risk_parametrization.xml' */

'lb_all_bl_cl'=>'<b>All:</b>',
'tt_risk_parametrization'=>'Risk Estimation',

/* './packages/risk/popup_risk_treatment.php' */

'tt_avoid'=>'avoid',
'tt_hold_accept'=>'retain/accept',
'tt_transfer'=>'transfer',

/* './packages/risk/popup_risk_treatment.xml' */

'lb_justification_bl_cl'=>'<b>Reason:</b>',
'st_accept_transfer_or_avoid_risk'=>'Do you wish to <b>%treatment_type%</b> Risk \'<b>%risk_name%</b>\'?',
'tt_risk_treatment'=>'Risk Treatment (<b>%treatment_type%</b>)',
'vb_save'=>'Save',

/* './packages/risk/popup_risks_control_association_edit.xml' */

'lb_all_risks_bl_cl'=>'<b>All Risks:</b>',
'lb_control_bl_cl'=>'<b>Control:</b>',
'tt_risk_control_association_parametrization'=>'Risk Reduction',

/* './packages/risk/report/clock.php' */

'st_generating_report'=>'Generating Report...',
'st_operation_wait'=>'<b>Attention:</b> This report may take a few minutes to be created.<br> Please, wait.',

/* './packages/risk/report/loading_report.php' */

'st_verifying_existing_reports'=>'Verifying Exisiting Reports.',

/* './packages/risk/report/popup_report_activities.xml' */


/* './packages/risk/report/popup_report_areas.php' */

'mx_areas'=>'Areas',

/* './packages/risk/report/popup_report_areas.xml' */

'rs_header_area_name'=>'Name',
'rs_header_area_responsible'=>'Responsible',

/* './packages/risk/report/popup_report_areas_by_priority.php' */

'rs_areas_by_priority'=>'Areas by Priority',

/* './packages/risk/report/popup_report_areas_by_type.php' */

'rs_areas_by_type'=>'Areas by Type',

/* './packages/risk/report/popup_report_areas_without_processes.php' */

'rs_areas_without_processes'=>'Areas without Processes',

/* './packages/risk/report/popup_report_asa.xml' */


/* './packages/risk/report/popup_report_asset.php' */

'rs_assets'=>'Assets',

/* './packages/risk/report/popup_report_asset.xml' */


/* './packages/risk/report/popup_report_asset_deps.php' */

'si_assets_deps'=>'Assets - Dependencies and Dependents',
'si_dependencies'=>'Dependencies',
'si_dependents'=>'Dependents',

/* './packages/risk/report/popup_report_asset_importance.php' */

'rs_assets_importance'=>'Assets Importance',

/* './packages/risk/report/popup_report_asset_relevance.php' */

'rs_asset_cl_bl'=>'<b>Asset:</b>',
'si_asset_relevance'=>'Asset Relevance',

/* './packages/risk/report/popup_report_asset_relevance.xml' */

'rs_responsible_cl'=>'Responsible:',

/* './packages/risk/report/popup_report_assets.php' */

'mx_assets'=>'Assets',

/* './packages/risk/report/popup_report_assets.xml' */

'rs_header_asset_name'=>'Name',
'rs_header_asset_responsible'=>'Responsible',

/* './packages/risk/report/popup_report_assets_without_risks.php' */

'rs_assets_without_risks'=>'Assets without Risk Events',

/* './packages/risk/report/popup_report_assets_without_risks.xml' */

'rs_security_responsible'=>'Security Responsible',

/* './packages/risk/report/popup_report_best_practices.php' */

'mx_best_practices'=>'Requirements',

/* './packages/risk/report/popup_report_best_practices.xml' */

'rs_best_practice_name'=>'Name',

/* './packages/risk/report/popup_report_bia_quantitative.xml' */


/* './packages/risk/report/popup_report_conformity.php' */

'rs_applied_best_practices'=>'Requirements Applied',
'rs_not_applied_best_practices'=>'Requirements Not Applied',
'rs_not_used'=>'Not Used',
'rs_not_used_controls'=>'Controls Not Used',
'rs_report_conformity_footer'=>'Requirements Total = %total% / Requirements Applied Total = %applied%',
'rs_used_controls'=>'Used Controls',
'tt_conformity'=>'Conformity',

/* './packages/risk/report/popup_report_conformity.xml' */

'rs_standard_cl'=>'Standard:',

/* './packages/risk/report/popup_report_control_accompaniment.php' */

'rs_follow_up_of_controls'=>'Follow Up of Controls',

/* './packages/risk/report/popup_report_control_cost.php' */

'rs_cost_per_control'=>'Cost by Control',

/* './packages/risk/report/popup_report_control_cost_by_area.php' */

'rs_cost_of_controls_per_area'=>'Cost of Controls by Area',

/* './packages/risk/report/popup_report_control_cost_by_asset.php' */

'rs_cost_of_controls_per_asset'=>'Cost of Controls by Asset',

/* './packages/risk/report/popup_report_control_cost_by_cost_category.php' */

'mx_control_cost_by_cost_category'=>'Control Cost by Cost Category',

/* './packages/risk/report/popup_report_control_cost_by_cost_category.xml' */

'rs_control_cost'=>'Cost',
'rs_control_name'=>'Name',
'rs_total_cost'=>'Total',

/* './packages/risk/report/popup_report_control_cost_by_process.php' */

'rs_cost_of_controls_per_process'=>'Cost of Controls by Process',

/* './packages/risk/report/popup_report_control_cost_by_responsible.php' */

'rs_cost_of_controls_per_responsible'=>'Cost of Controls by Responsible',

/* './packages/risk/report/popup_report_control_cost_by_responsible.xml' */

'rs_cost'=>'Cost',

/* './packages/risk/report/popup_report_control_efficiency.php' */

'rs_controls_efficiency'=>'Controls Efficiency',
'rs_efficient_controls'=>'Efficient Controls',
'rs_inefficient_controls'=>'Inefficient Controls',

/* './packages/risk/report/popup_report_control_efficiency_accompaniment.php' */

'rs_follow_up_of_control_efficiency'=>'Follow Up of Controls Efficiency',

/* './packages/risk/report/popup_report_control_efficiency_accompaniment.xml' */

'rs_ee'=>'EE',
'rs_re'=>'RE',

/* './packages/risk/report/popup_report_control_implementation_date.php' */

'rs_controls_implemented_outside_stated_period'=>'Controls implemented Outside Stated Period',
'rs_controls_implemented_within_stated_period'=>'Controls implemented within stated period',
'rs_date_of_implementation_of_controls'=>'Controls Implementation Date',

/* './packages/risk/report/popup_report_control_implementation_date.xml' */

'rs_implementation'=>'Implementation',

/* './packages/risk/report/popup_report_control_planning.php' */

'rs_implementation_date'=>'Implementation Date',
'rs_implementation_deadline'=>'Implementation Deadline',
'rs_implemented_controls'=>'Implemented Controls',
'rs_planned_controls'=>'Planned Controls',
'rs_planning_of_controls'=>'Planning of Controls',

/* './packages/risk/report/popup_report_control_revision_agenda_by_responsible.php' */

'si_control_revision_agenda_by_responsible'=>'Control\'s Revision Agenda by Responsible',

/* './packages/risk/report/popup_report_control_revision_agenda_by_responsible.xml' */

'rs_next_revision'=>'Next Revision',

/* './packages/risk/report/popup_report_control_summary.xml' */

'rs_best_practice'=>'Requirement',

/* './packages/risk/report/popup_report_control_summary_pm.php' */

'rs_summary_of_controls'=>'Summary of Controls',

/* './packages/risk/report/popup_report_control_summary_pm.xml' */

'rs_accomplishment_bl_cl'=>'<b>Accomplishment:</b>',
'rs_description_cl'=>'<b>Description:</b>',

/* './packages/risk/report/popup_report_control_test_accompaniment.php' */

'rs_ctrl_test_not_ok'=>'Not OK',
'rs_ctrl_test_ok'=>'Ok',
'rs_follow_up_of_control_test'=>'Follow Up of Controls Testing',

/* './packages/risk/report/popup_report_control_test_accompaniment.xml' */

'rs_comment'=>'Comment',
'rs_control_cl'=>'Control:',
'rs_date_accomplishment'=>'Accomplishment Date',
'rs_predicted_date'=>'Predicted Date',
'rs_test'=>'Test',

/* './packages/risk/report/popup_report_control_test_agenda_by_responsible.php' */

'si_control_test_agenda_by_responsible'=>'Control\'s Test Agenda by Responsible',

/* './packages/risk/report/popup_report_control_test_agenda_by_responsible.xml' */

'rs_next_test'=>'Next Test',
'rs_processes'=>'Processes',
'rs_responsibles'=>'Responsibles',
'rs_schedule'=>'Scheduling',

/* './packages/risk/report/popup_report_controls.php' */

'mx_controls'=>'Controls',

/* './packages/risk/report/popup_report_controls.xml' */

'rs_header_control_name'=>'Name',
'rs_header_control_responsible'=>'Responsible',

/* './packages/risk/report/popup_report_controls_by_responsible.php' */

'rs_controls_per_responsible'=>'Controls by Responsible',

/* './packages/risk/report/popup_report_controls_by_risk.php' */

'rs_controls_per_risk'=>'Controls by Risk',

/* './packages/risk/report/popup_report_controls_by_risk.xml' */

'rs_affected_asset_bl_cl'=>'<b>Affected Asset:</b>',
'rs_risk_and_impact_bl_cl'=>'<b>Risk / Impact:</b>',
'rs_risk_bl_cl'=>'<b>Risk:</b>',

/* './packages/risk/report/popup_report_controls_by_type.php' */

'rs_controls_by_type'=>'Controls by Type',

/* './packages/risk/report/popup_report_controls_not_measured.php' */

'rs_not_measured_controls'=>'Not Measured Controls',

/* './packages/risk/report/popup_report_controls_not_measured.xml' */

'rs_efficiency'=>'Efficiency',

/* './packages/risk/report/popup_report_controls_without_risks.php' */

'rs_controls_without_risks'=>'Controls without Associated Risks',

/* './packages/risk/report/popup_report_detailed_pendant_tasks.php' */

'rs_detailed_pending_tasks'=>'Detailed Pending tasks',

/* './packages/risk/report/popup_report_duplicated_risks.php' */

'tt_duplicated_risks'=>'Duplicate Risks',

/* './packages/risk/report/popup_report_duplicated_risks.xml' */

'rs_risk_cl'=>'Risk:',

/* './packages/risk/report/popup_report_duplicated_risks_by_asset.php' */

'tt_duplicated_risks_by_asset'=>'Duplicate Risks by Asset',

/* './packages/risk/report/popup_report_duplicated_risks_by_asset.xml' */

'rs_risk'=>'Risk',

/* './packages/risk/report/popup_report_events_by_category.php' */

'rs_checklist_by_event'=>'Checklist by Asset',

/* './packages/risk/report/popup_report_events_by_category.xml' */

'rs_brackets'=>'[ ]',
'rs_event'=>'Event',
'rs_event_category_cl'=>'Category:',
'rs_na'=>'NA',
'rs_nc'=>'NC',
'rs_ok'=>'Ok',

/* './packages/risk/report/popup_report_events_by_type.php' */

'rs_events_by_type'=>'Events by Type',

/* './packages/risk/report/popup_report_pendant_contexts.php' */

'rs_pending_items'=>'Pending Items',

/* './packages/risk/report/popup_report_pendant_contexts.xml' */

'rs_approver'=>'Approver',

/* './packages/risk/report/popup_report_places.xml' */


/* './packages/risk/report/popup_report_policy.php' */

'rs_sgsi_policy_statement'=>'PCI\'s Policy Statement',

/* './packages/risk/report/popup_report_policy.xml' */

'rs_system_policy'=>'System Policy',

/* './packages/risk/report/popup_report_process_by_process.xml' */


/* './packages/risk/report/popup_report_processes.php' */

'mx_processes'=>'Processes',

/* './packages/risk/report/popup_report_processes.xml' */

'rs_header_process_name'=>'Name',
'rs_header_process_responsible'=>'Responsible',

/* './packages/risk/report/popup_report_processes_by_priority.php' */

'rs_processes_by_priority'=>'Processes by Priority',

/* './packages/risk/report/popup_report_processes_by_priority.xml' */

'rs_priority_cl'=>'Priority:',

/* './packages/risk/report/popup_report_processes_by_type.php' */

'rs_processes_by_type'=>'Processes by Type',

/* './packages/risk/report/popup_report_processes_without_assets.php' */

'rs_processes_without_assets'=>'Processes without Assets',

/* './packages/risk/report/popup_report_risk_amount_by_area.php' */

'rs_amount_of_risks_per_area'=>'Amount of Risks by Area',

/* './packages/risk/report/popup_report_risk_amount_by_process.php' */

'rs_amount_of_risks_per_process'=>'Amount of Risks by Process',

/* './packages/risk/report/popup_report_risk_financial_impact.php' */

'mx_risk_financial_impact'=>'Risk Financial Impact',

/* './packages/risk/report/popup_report_risk_financial_impact.xml' */

'rs_risk_impact'=>'Risk Impact',
'rs_risk_name'=>'Name',
'rs_risk_residual_value'=>'Residual Risk',
'rs_risk_value'=>'Potential Risk',
'rs_total_impact'=>'Total',

/* './packages/risk/report/popup_report_risk_status_by_area.php' */

'rs_risk_status_per_area'=>'Risk Status by Area',

/* './packages/risk/report/popup_report_risk_status_by_area.xml' */

'rs_area'=>'Area',

/* './packages/risk/report/popup_report_risk_status_by_process.php' */

'rs_risk_status_per_process'=>'Risk Status by Process',

/* './packages/risk/report/popup_report_risk_status_by_process.xml' */

'rs_process'=>'Process',

/* './packages/risk/report/popup_report_risk_treatment_plan.php' */

'rs_and'=>'and',
'rs_avoided'=>'Avoided',
'rs_not_treated'=>'Not Treated',
'rs_plan_of_risks_treatment'=>'Risk Treatment Plan',
'rs_reduced'=>'Reduced',
'rs_restrained'=>'Retained',
'rs_transferred'=>'Transferred',

/* './packages/risk/report/popup_report_risk_treatment_plan.xml' */

'rs_asset_bl_cl'=>'<b>Asset:</b>',
'rs_deadline'=>'Deadline',
'rs_status'=>'Status',

/* './packages/risk/report/popup_report_risk_values.php' */

'rs_risks_values'=>'Risks Values',

/* './packages/risk/report/popup_report_risk_values.xml' */

'rs_affected_asset'=>'Affected Asset',

/* './packages/risk/report/popup_report_risks.php' */

'mx_risks'=>'Risks',

/* './packages/risk/report/popup_report_risks_by_area.php' */

'rs_risks_per_area'=>'Risks by Area',

/* './packages/risk/report/popup_report_risks_by_area_and_process_asset.xml' */

'rs_asset_cl'=>'Asset:',
'rs_process_cl'=>'Process:',

/* './packages/risk/report/popup_report_risks_by_asset.php' */

'rs_risks_per_asset'=>'Risks by Asset',

/* './packages/risk/report/popup_report_risks_by_control.php' */

'rs_report_of_risk_per_control'=>'Risks by Control',

/* './packages/risk/report/popup_report_risks_by_control.xml' */

'rs_control_bl_cl'=>'<b>Control:</b>',

/* './packages/risk/report/popup_report_risks_by_process.php' */

'rs_risks_per_process'=>'Risks by Process',

/* './packages/risk/report/popup_report_risks_by_type.php' */

'rs_risks_by_type'=>'Risks by Type',

/* './packages/risk/report/popup_report_risks_by_type.xml' */

'rs_type_cl'=>'Type:',

/* './packages/risk/report/popup_report_scope.php' */

'rs_sgsi_scope_statement'=>'PCI\'s Scope Statement',

/* './packages/risk/report/popup_report_scope.xml' */

'rs_system_scope'=>'System Scope',

/* './packages/risk/report/popup_report_statement_of_applicability.php' */

'rs_applied'=>'Applied',
'rs_no_risks_application_control'=>'Until now no Risks have been identified which justify the application of Control',
'rs_not_applied'=>'Not Applied',
'rs_reduction_of_risks'=>'Reduction of Risks.',
'rs_statement_of_applicability'=>'Statement of Applicability',
'rs_statement_of_applicability_pre_report'=>'Pre-Report of Statement of Applicability',

/* './packages/risk/report/popup_report_statement_of_applicability.xml' */

'rs_applicability_cl'=>'Applicability:',
'rs_best_practice_cl'=>'Requirement:',
'rs_control'=>'Control',
'rs_document'=>'Document',
'rs_justification_cl'=>'Reason:',

/* './packages/risk/report/popup_report_statement_of_applicability_justificative.xml' */

'tt_justification'=>'Reason',
'vb_close'=>'Close',
'vb_confirm'=>'Confirm',

/* './packages/risk/report/popup_report_summarized_pendant_tasks.php' */

'rs_summarized_pending_tasks'=>'Summarized Pending tasks',

/* './packages/risk/report/popup_report_summarized_pendant_tasks.xml' */

'rs_amount'=>'Amount',
'rs_tasks'=>'Tasks',

/* './packages/risk/report/popup_report_tests.xml' */


/* './packages/risk/report/popup_report_top10_assets_with_more_risks.php' */

'rs_top_10_assets_with_highest_risks'=>'Top 10 Assets with the most High Risks',
'rs_top_10_assets_with_most_risks'=>'Top 10 Assets with more Risks',

/* './packages/risk/report/popup_report_top10_assets_with_more_risks.xml' */

'rs_amount_of_risks'=>'Amount of Risks',
'rs_asset'=>'Asset',
'rs_responsible'=>'Responsible',

/* './packages/risk/report/popup_report_top10_risks_by_area.php' */

'rs_top_10_risks_per_area'=>'Top 10 Risks by Area',

/* './packages/risk/report/popup_report_top10_risks_by_area.xml' */

'rs_area_bl_cl'=>'<b>Area:</b>',

/* './packages/risk/report/popup_report_top10_risks_by_process.php' */

'rs_top_10_risks_per_process'=>'Top 10 Risks by Process',

/* './packages/risk/report/popup_report_top10_risks_by_process.xml' */

'rs_process_bl_cl'=>'<b>Process:</b>',
'rs_real_risk'=>'Potential Risk',
'rs_residual_risk'=>'Residual Risk',
'rs_risk_and_impact'=>'Risk / Impact',

/* './packages/risk/report/popup_report_user_responsabilities.php' */

'rs_security_resp'=>'Security Resp.',
'rs_special_user'=>'Special User',
'rs_users_responsibilities'=>'Users Responsibilities',

/* './packages/risk/report/popup_report_user_responsabilities.xml' */

'rs_responsible_bl_cl'=>'<b>Responsible:</b>',

/* './packages/risk/report/report_template.xml' */

'rs_comment_cl'=>'Comment:',
'rs_end_of_report'=>'End of Report',

/* './packages/risk/tab_risk_management.xml' */

'ti_area'=>'Area',
'ti_asset'=>'Information Asset',
'ti_controls'=>'Controls',
'ti_process'=>'Process',
'ti_reports'=>'Reports',
'ti_risk'=>'Risk',

/* './popup_choose_role.php' */

'mx_user_doesnt_have_permission_to_login'=>'The user <b>%user_name%</b> does not have permission to access the system.',
'tt_login_error'=>'System Login Failed',

/* './popup_choose_role.xml' */

'lb_represent_bl_cl'=>'<b>Represent:</b>',
'st_solicitor_explanation'=>'If you wish to represent any of these users, press OK.',
'tt_solicitor_mode'=>'Attorney Mode',

/* './popup_email_preferences.xml' */

'cb_action_plan'=>'Action Plan',
'cb_areas'=>'Areas',
'cb_assets'=>'Assets',
'cb_best_practices'=>'Requirements',
'cb_controls'=>'Controls',
'cb_documents'=>'Documents',
'cb_events'=>'Events',
'cb_incidents'=>'Incidents',
'cb_non_conformities'=>'Nonconformities',
'cb_occurrences'=>'Occurrences',
'cb_others'=>'Others',
'cb_processes'=>'Processes',
'cb_risks'=>'Risks',
'cb_select_all'=>'Select All',
'cb_standards'=>'Standards',
'lb_warning_types_to_receive_cl'=>'Select which warnings you want to receive:',
'rb_html_email'=>'Html e-mail',
'rb_messages_digest_complete'=>'Complete (one daily e-mail with full messages)',
'rb_messages_digest_none'=>'None (one e-mail for each message)',
'rb_messages_digest_subjects'=>'Subjects (one daily e-mail with only messages subjects)',
'rb_text_email'=>'Plain Text Email',
'st_email_preferences_bl'=>'<b>E-mail Preferences</b>',
'st_messages_digest_type_cl'=>'Messages digest type:',

/* './popup_help.xml' */

'cb_do_not_show_again'=>'Do not show this again',
'st_tip_text'=>'<b>When accessing Real PCI tables, try our context-menu.</b><br/><br/>To edit, remove or link to other Real PCI components, click with the right mouse button on a result, in order to view the popup menu with the available actions.',
'tt_tip_bl'=>'<b>Tip</b>',

/* './popup_password_request.xml' */

'lb_user_login_cl'=>'User:',
'st_password_request'=>'To obtain a new password, type your User. A new password will be sent to your e-mail.',
'st_warning_general_email_disabled'=>'The system e-mail is disabled, so it isn\'t possible to generate a new password.',
'tt_password_reminder'=>'New Password Request',
'vb_new_password'=>'New Password',
'wn_password_sent'=>'The password was sent to the user, as long as the user exists.',

/* './popup_risk_limits_task_view.xml' */

'lb_risk_limits_high_cl'=>'Risk Tolerance 2:',
'lb_risk_limits_low_cl'=>'Risk Tolerance 1:',
'tt_risk_limits_approval'=>'Approve Risk Limits',

/* './popup_saas_disclaimer.xml' */

'tt_disclaimer_popup_title'=>'Disclaimer',
'vb_agree'=>'I agree',
'vb_disagree'=>'I disagree',

/* './popup_schedule_edit.xml' */

'lb_april'=>'April',
'lb_august'=>'August',
'lb_december'=>'December',
'lb_february'=>'February',
'lb_january'=>'January',
'lb_july'=>'July',
'lb_june'=>'June',
'lb_march'=>'March',
'lb_may'=>'May',
'lb_november'=>'November',
'lb_october'=>'October',
'lb_schedule_days_to_finish'=>'Days to accomplish task:',
'lb_schedule_end'=>'Finishes on:',
'lb_schedule_periodicity'=>'Every',
'lb_schedule_start'=>'Starts on:',
'lb_schedule_type_cl'=>'Type:',
'lb_september'=>'September',
'lb_week_days'=>'On the following days:',
'mx_first_week'=>'first',
'mx_fourth_week'=>'forth',
'mx_last_week'=>'last',
'mx_monday'=>'Monday',
'mx_saturday'=>'Saturday',
'mx_second_week'=>'second',
'mx_sunday'=>'Sunday',
'mx_third_week'=>'third',
'mx_thursday'=>'Thursday',
'mx_tuesday'=>'Tuesday',
'mx_wednesday'=>'Wednesday',
'si_schedule_by_day'=>'daily',
'si_schedule_by_month'=>'Monthly',
'si_schedule_by_week'=>'Weekly',
'si_schedule_month_day'=>'Month day',
'si_schedule_week_day'=>'Week day',
'st_months'=>'months',
'st_never'=>'never',
'st_schedule_months'=>'On the following months:',
'st_weeks'=>'weeks',
'tt_schedule_edit'=>'Task Scheduling',
'tx_1'=>'1',
'tx_schedule_days_to_finish'=>'1',

/* './popup_solicitor_conflict.xml' */

'st_solicitor_conflict'=>'The user <b>%solicitor_name%</b> has been nominated by you to be your Attorney on your absency period. Would you like to end this link?',
'tt_solicitor_conflict'=>'Attorney Conflict',

/* './popup_survey.xml' */

'st_survey_text'=>'After evaluating the system, don\'t forget to answer our quality survey. You can access it through link "User Experience Survey" in system\'s footer. Do you want to answer it now?',
'tt_survey_bl'=>'<b>Survey</b>',

/* './popup_unexpected_behavior.xml' */

'st_unexpected_behavior_message'=>'An unexpected behavior in the system has been detected. An e-mail with detailed information about what happened has been sent to our Technical Support team. If necessary, close the web browser and log into the system again.',
'tt_unexpected_behavior'=>'Unexpected Behaviour',

/* './popup_user_change_password.php' */

'st_user_password_change'=>'You must change your password now.',
'st_user_password_expired'=>'<b>Your password has expired!</b><br/>You must change it now.',
'st_user_password_will_expire'=>'Your password will expire in %days% day(s). If you want to, you may change it now.',

/* './popup_user_change_password.xml' */

'lb_confirm_cl'=>'Confirm:',
'tt_change_password'=>'Change Password',
'vb_change_password'=>'Change Password',
'wn_unmatching_password'=>'The passwords don\'t match.',

/* './popup_user_dashboard_preference.php' */

'tt_average_time_for_documents_approval'=>'Documents Average Approval Time',
'tt_average_time_for_my_documents_approval'=>'My Documents Average Approval Time',
'tt_financial_summary'=>'Financial Summary',
'tt_general_summary_of_risks'=>'Estimated vs. Non-Estimated Risk',
'tt_grid_abnormality_ci_title'=>'Continual Improvement Abnormalities Summary',
'tt_grid_abnormality_pm_title'=>'Policy Management Abnormalities Summary',
'tt_grid_incident_summary_title'=>'Incident Overview',
'tt_grid_incidents_per_asset_title'=>'Top 5 Assets with more Incidents',
'tt_grid_incidents_per_process_title'=>'Top 5 Processes with more Incidents',
'tt_grid_incidents_per_user_title'=>'Top 5 Users with more Disciplinary Processes',
'tt_grid_nc_per_process_title'=>'Top 5 Processes with more Nonconformities',
'tt_grid_nc_summary_title'=>'Nonconformities Summary',
'tt_my_items'=>'My Items',
'tt_my_pendencies'=>'My Pendings from Risk Assessment',
'tt_my_risk_summary'=>'My Risk Summary',
'tt_risk_management_my_items_x_policy_management'=>'Risk Assessment (My Items) x Policy Management',
'tt_risk_management_status'=>'Risk Level',
'tt_risk_management_x_policy_management'=>'Documents per Business Element',
'tt_sgsi_reports_and_documents'=>'Compliance Mandatory Documents',
'tt_summary_of_abnormalities'=>'Risk Assessment Abnormalities Summary',
'tt_summary_of_best_practices'=>'Requirements - Compliance Index',
'tt_summary_of_controls'=>'Controls Overview',
'tt_summary_of_my_documents_last_revision_average_time'=>'Last Documents Revision Time Period Summary',
'tt_summary_of_parameterized_risks'=>'Potential Risk vs. Residual Risk',
'tt_summary_of_policy_management'=>'Policy Management Overview',
'tt_summary_of_policy_management_my_documents'=>'Policy Management (My Documents) Summary',
'tt_summary_of_risk_management_documentation'=>'Summary of the Risk Assessment Documentation',
'tt_summary_of_the_risk_management_from_my_items_documentation'=>'Summary of the Risk Assessment from My Items Documentation',
'tt_top_10_most_read_documents'=>'Top 10 most read Documents',
'tt_top_10_most_read_my_documents'=>'Top 10 My Documents Most Read',

/* './popup_user_dashboard_preference.xml' */

'st_all_elements_not_selected'=>'All items (not selected)',
'st_selected_elements_cl'=>'Selected items:',
'tt_summary_preferences_editing'=>'Summary Preferences Editing',

/* './popup_user_preferences.php' */

'si_email'=>'Email',
'si_general'=>'General',

/* './popup_user_preferences.xml' */

'lb_preferences_cl'=>'Preferences:',
'tt_edit_preferences'=>'Edit Preferences',

/* './popup_user_profile.xml' */

'cb_change_password'=>'Change password',
'lb_current_password_cl'=>'Current Password:',
'lb_dashboard_preferences_cl'=>'Dashboard Preferences:',
'lb_general_config_bl'=>'<b>General Settings</b>',
'lb_general_config_bl_cl'=>'<b>General Configuration:</b>',
'lb_language_cl'=>'Language:',
'lb_password_confirm_cl'=>'Confirmation of Password:',
'lb_solicitor_cl'=>'Attorney:',
'lb_solicitor_finish_date_cl'=>'End of Procurement:',
'lb_solicitor_start_date_cl'=>'Start of Procurement:',
'st_change_password_sub_title_bl'=>'<b>Password Change</b>',
'st_solicitor_sub_title_bl'=>'<b>Attorney</b>',
'to_dashboard_user_preferences_help'=>'<b>Configuration of dashboard preferences:</b><br/><br/>Determines the order and which grids of information will be shown in your dashboard.',
'wn_email_incorrect'=>'Invalid email.',
'wn_existing_password'=>'The password is in the recent passwords history table',
'wn_initial_date_after_today'=>'The start date must be the same or after the current date.',
'wn_missing_dates'=>'The date fields are mandatory when an attorney is selected.',
'wn_missing_solicitor'=>'It is mandatory to select an attorney when one of the dates was filled.',
'wn_past_date'=>'The final date must be later or equal to the current date.',
'wn_wrong_dates'=>'The final date of the procurement must be later than its start date.',

/* './popup_user_search.xml' */

'tt_user_search'=>'User Search',
'wn_no_user_selected'=>'There is no selected user.',

/* './reports/ISMSReportTemplate.php' */

'rs_ip_cl'=>'IP:',
'rs_manager_cl'=>'Manager:',

/* './session_expired.xml' */

'st_session_expired'=>'Your session has expired. Press <b>Ok</b> or close this window to login again.',
'tt_session_expired'=>'Session Expired',

/* './tab_dashboard.xml' */

'ti_graphics'=>'Graphics',
'ti_statistics'=>'Statistics',
'ti_summary'=>'Summaries',
'ti_warnings'=>'My Tasks & Alerts',

/* './tab_main.xml' */

'st_search'=>'Search',
'ti_continual_improvement'=>'Continual Improvement',
'ti_dashboard'=>'Dashboard',
'ti_libraries'=>'PCI DSS',
'ti_policy_management'=>'Policy Management',
'ti_risk_management'=>'Risk Assessment',
'ti_search'=>'Search',
'vb_admin'=>'Admin',
'vb_go'=>'Go',

/* './visualize.php' */

'tt_visualize'=>'View',
);
?>