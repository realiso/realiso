<?php
$gaStrings = array(

/* './about_isms.xml' */

'st_about_axur_full'=>'axur信息安全（ www.axur.net ）是一个信息安全管理解决方案的领先公司。我们是一个全球性的球员的ISO 27001解决方案市场。我们所提供的高科技解决方案，以减少组织风险，衡量和demonstrat',
'st_about_axur_title'=>'约axur',
'st_about_isms_full'=>'axur主义-信息安全管理解决方案是一套为管理一个信息安全管理体系按照I SO2 7001。 axur主义有利于公司的任何规模的，其中有几位议员所涉及的资产的保管，新里',
'st_about_isms_title'=>'约axur主义',
'st_axur_information_security'=>'axur信息安全',
'st_axur_link'=>'www.realiso.com',
'st_build'=>'建造',
'st_copyright'=>'版权',
'st_information_security_management_system'=>'信息安全管理解决方案',
'st_version'=>'版本',

/* './access_denied.php' */

'st_license_file_not_found'=>'许可文件的<b>％\'license_name\'％</b>没有发现。',

/* './access_denied.xml' */

'tt_access_denied'=>'拒绝访问',

/* './classes/admin/report/ISMSReportAuditAlertFilter.php' */

'rs_alerts_created_after'=>'警报后所创建的',
'rs_alerts_created_before'=>'快讯之前创建',

/* './classes/admin/report/ISMSReportAuditLogFilter.php' */

'rs_action_cl'=>'行动：',
'rs_log_after'=>'登录后',
'rs_log_before'=>'日志之前',

/* './classes/admin/report/ISMSReportAuditTaskFilter.php' */

'rs_activity_cl'=>'活动：',
'rs_and_before'=>'和前',
'rs_any_date'=>'任何日期',
'rs_creation_date_cl'=>'创建日期：',
'rs_receiver_cl'=>'接收机：',
'rs_tasks_created_after'=>'任务后所创建的',
'rs_tasks_created_before'=>'任务之前创建',
'st_all_users'=>'所有用户',

/* './classes/continual_improvement/CIActionPlan.php' */

'mx_action_plan'=>'行动计划',
'mx_ap_remove_error_message'=>'这是不可能删除的行动计划。有至少有一个不符合相关的这个项目。',
'mx_ap_remove_error_title'=>'消除错误的行动计划',

/* './classes/continual_improvement/CICategory.php' */

'mx_root'=>'根',

/* './classes/continual_improvement/CIIncident.php' */

'mx_incident'=>'事件',

/* './classes/continual_improvement/CIIncidentControl.php' */

'mx_incident_control_association'=>'事件控制协会',

/* './classes/continual_improvement/CIIncidentProcess.php' */

'mx_incident_process_association'=>'事件的过程中协会',

/* './classes/continual_improvement/CINonConformity.php' */

'mx_non_conformity'=>'不符合',

/* './classes/continual_improvement/CINonConformitySeed.php' */

'mx_inefficient'=>'效率低下',
'mx_late_efficiency_revision'=>'效率修订延迟',
'mx_late_implementation'=>'年底实施',
'mx_late_test'=>'延迟测试',
'mx_test_failure'=>'试验失败',

/* './classes/continual_improvement/CIOccurrence.php' */

'mx_occurrence'=>'发生',
'mx_occurrence_remove_error'=>'消除错误的发生',
'mx_occurrence_remove_error_message'=>'这是不可能删除此发生。有至少有一个事件相关的这个项目。',
'st_incident_module_name'=>'不断改善',

/* './classes/continual_improvement/CISolution.php' */

'mx_solution'=>'解决方案',

/* './classes/continual_improvement/report/ISMSReportDisciplinaryProcessFilter.php' */

'rs_user_cl'=>'用户名：',

/* './classes/continual_improvement/report/ISMSReportElementsFilter.php' */

'lb_elements_cl'=>'项目：',

/* './classes/continual_improvement/report/ISMSReportIncidentFilter.php' */

'lb_solution_date_cl'=>'解决日期：',
'mx_immediate_disposal'=>'立即处置',

/* './classes/continual_improvement/report/ISMSReportNonConformityFilter.php' */

'lb_conclusion_date_cl'=>'完成日期：',
'mx_after_initialdate'=>'后％ initialdate ％',
'mx_before_finaldate'=>'前％ finaldate ％',
'mx_between_initialdate_finaldate'=>'之间％ initialdate ％和％ finaldate ％',
'mx_external_audit'=>'外部审计',
'mx_internal_audit'=>'内部审计',
'mx_no'=>'否',
'mx_security_control'=>'安全控制',
'mx_yes'=>'是',

/* './classes/continual_improvement/report/ISMSReportOccurrenceFilter.php' */

'mx_all'=>'全部',

/* './classes/ISMSActivity.php' */

'wk_action_plan_approval'=>'行动计划的审批',
'wk_action_plan_finish_confirm'=>'批准，估计完成的行动计划',
'wk_action_plan_revision'=>'行动计划跟进',
'wk_area_approval'=>'批准面积',
'wk_asset_approval'=>'批准资产',
'wk_best_practice_approval'=>'批准的最佳做法',
'wk_best_practice_section_approval'=>'批准节的最佳做法',
'wk_category_approval'=>'批准类别',
'wk_control_approval'=>'批准控制',
'wk_control_implementation_approval'=>'批准控制的执行情况',
'wk_control_test_followup'=>'监控测试',
'wk_document_approval'=>'批准文件',
'wk_document_revision_followup'=>'监测文件的修改',
'wk_document_template_approval'=>'批准文件范本',
'wk_event_approval'=>'批准事件',
'wk_incident_approval'=>'批准事件',
'wk_incident_control_induction'=>'控制eficiency诱导测量',
'wk_incident_disposal_approval'=>'事件立即处置审批',
'wk_incident_solution_approval'=>'事件的解决方案审批',
'wk_non_conformity_approval'=>'不符合审批',
'wk_non_conformity_data_approval'=>'不符合的数据批准',
'wk_occurrence_approval'=>'发生批准',
'wk_policy_approval'=>'核准政策',
'wk_process_approval'=>'批准过程',
'wk_process_asset_association_approval'=>'批准资产协会与过程。',
'wk_real_efficiency_followup'=>'监察真正的效率',
'wk_risk_acceptance_approval'=>'批准接受的风险，上述价值',
'wk_risk_acceptance_criteria_approval'=>'批准标准的定义为接受的风险',
'wk_risk_approval'=>'批准的风险',
'wk_risk_control_association_approval'=>'批准控制协会与风险。',
'wk_risk_tolerance_approval'=>'批准风险承受能力',
'wk_scope_approval'=>'批准范围',
'wk_standard_approval'=>'批准标准',

/* './classes/ISMSAuditLog.php' */

'ad_action_bad_login'=>'已尝试登录在同一个错误的密码',
'ad_action_blocked_login'=>'是封锁，并试图在日志',
'ad_login'=>'记录在系统',
'ad_login_error'=>'已尝试登录在与用户名无效',
'ad_login_failed'=>'已尝试登入，在未经允许',
'ad_logout'=>'注销系统',
'ad_removed_from_system'=>'由系统中移除',
'ad_restored'=>'恢复',

/* './classes/ISMSContext.php' */

'ad_log_message'=>'％行动％ ％ ％标签的<b> ％ context_name ％ < / b >由百分之用户名％ 。',
'ad_on_behalf_of'=>'对代表',
'db_default_document_name'=>'％ label_context ％ ％ context_name ％ ％ label_doc ％',
'st_denied_permission_to_approve'=>'您没有权限批准',
'st_denied_permission_to_delete'=>'您没有权限删除',
'st_denied_permission_to_execute'=>'您没有权限去执行任务。',
'st_denied_permission_to_insert'=>'您没有权限插入（ n ）的',
'st_denied_permission_to_read'=>'您没有权限，以查看',

/* './classes/ISMSContextObject.php' */

'mx_ap_acceptance_pendant'=>'行动计划，接受等候',
'mx_approval'=>'在审批',
'mx_approved'=>'批准',
'mx_closed'=>'封闭',
'mx_co_approved'=>'批准这两个',
'mx_deleted'=>'删除',
'mx_denied'=>'否认',
'mx_directed'=>'转发',
'mx_finished'=>'完成',
'mx_measured'=>'测量',
'mx_nc_treatment_pendant'=>'不符合的治疗有待',
'mx_obsolete'=>'已过时',
'mx_open'=>'打开',
'mx_pendant'=>'紧迫的',
'mx_pendant_solution'=>'等候-解决方案',
'mx_pendant_solved'=>'等候-解决',
'mx_published'=>'出版',
'mx_revision'=>'在修订',
'mx_sent'=>'发出',
'mx_solved'=>'解决',
'mx_to_be_published'=>'即将出版',
'mx_under_development'=>'在发展',
'mx_waiting_conclusion'=>'等待完成',
'mx_waiting_deadline'=>'估计',
'mx_waiting_measurement'=>'等待测量',
'mx_waiting_solution'=>'等待解决方案',

/* './classes/ISMSEmailTemplates.php' */

'em_activity'=>'活动',
'em_alert_messages_sent'=>'亲爱的<b>主义的用户中， <b> <br>随即以下警告消息被发送给您。 <br>',
'em_asset'=>'资产',
'em_at'=>'在',
'em_click_here_to_access_the_system'=>'访问axur主义',
'em_client'=>'客户端',
'em_control'=>'控制',
'em_control_has_deadline'=>'亲爱的<b>主义的用户中， <b> <br>随即控制下面有一个截止日期为执行。',
'em_created_on'=>'上创建',
'em_description'=>'描述',
'em_disciplinary_process'=>'亲爱的<b>纪律处分程序经理， < / b > <br>随即以下系统的用户（ ） /被添加到一个纪律处分程序。',
'em_execute_task'=>'亲爱的<b>主义的用户中， <b> <br>我们请你来执行活动（每小时）以下。 <br>',
'em_feedback'=>'反馈',
'em_feedback_type'=>'反馈类型',
'em_incident'=>'事件',
'em_incident_risk_parametrization_message'=>'主义的<b>亲爱的用户， < / b > <br>随即以下的风险是相关联的资产，您的responsability 。',
'em_isms_feedback_msg'=>'用户写了反馈意见axur主义。',
'em_isms_new_user_msg'=>'您添加到axur主义。下面你可以看到的数据系统的访问：',
'em_limit'=>'限制',
'em_login'=>'注册',
'em_new_parameters'=>'新参数',
'em_new_password_generated'=>'亲爱的<b>主义的用户中， <b> <br>随即新密码已为您创建。 <br>您的新密码',
'em_new_password_request_ignore'=>'如果您有没有要求一个新的密码，请忽略这条电子邮件。您以前的全权证书仍然有效，如果您使用他们来说，这新的密码将被丢弃。',
'em_observation'=>'观察',
'em_original_parameters'=>'默认参数',
'em_password'=>'密码',
'em_risk'=>'风险',
'em_saas_abandoned_system_msg'=>'<b>已axur主义saas的客户端下面是被遗弃的。 < / b >',
'em_sender'=>'发件人',
'em_sent_on'=>'发出的',
'em_tasks_and_alerts_transferred'=>'亲爱的<b>主义的用户中， <b> <br>随即系统用户已被删除，他的任务和警报已移交给您。',
'em_tasks_and_alerts_transferred_message'=>'用户上述已被移除，从制度，他的任务和警报已转移到您的姓名。',
'em_user'=>'用户',
'em_users'=>'用户',
'link'=>'链接',

/* './classes/ISMSLib.php' */

'em_license_expired'=>'授权的客户端的<b> ％ client_name ％ < / b >过期的<b> ％ expiracy ％ < / b >',
'em_subject_license_expired'=>'许可证过期',
'mx_asset_manager'=>'资产所有者',
'mx_chairman'=>'管理',
'mx_chinese'=>'中国（试用版）',
'mx_control_manager'=>'控制业主',
'mx_created_by'=>'创建者：',
'mx_day'=>'白天',
'mx_days'=>'天',
'mx_english'=>'英语',
'mx_evidence_manager'=>'证据经理',
'mx_incident_manager'=>'事件管理',
'mx_library_manager'=>'图书馆的主人',
'mx_modified_by'=>'修改：',
'mx_month'=>'一个月',
'mx_months'=>'个月',
'mx_non_conformity_manager'=>'不符合的管理',
'mx_on'=>'在…之上',
'mx_portuguese'=>'葡萄牙语',
'mx_process_manager'=>'纪律处分程序管理',
'mx_shortdate_format'=>'%m/%d/%Y',
'mx_spanish'=>'Español (Beta!)',
'mx_week'=>'周',
'mx_weeks'=>'周',
'st_denied_permission_to_access_incident_module'=>'您没有权限访问的不断提高工作台。 <br> <br>此许可并没有此权限！',
'st_denied_permission_to_access_policy_module'=>'您没有权限访问的政策管理模块。 <br> <br>您正在使用的许可证，不允许您要访问此模块！',

/* './classes/ISMSLicense.php' */

'mx_commercial'=>'商业',
'mx_trial'=>'试验',

/* './classes/ISMSMailer.php' */

'em_to_execute_task_check_activity_list'=>'<b>要执行这项任务，进入系统，并确认您的活动清单。 < / b > <br>如需进一步资料，这项工作，请联络的保安人员。',

/* './classes/ISMSPasswordPolicy.php' */

'mx_minimum_chars'=>'最低长度',
'mx_minimum_numeric_chars'=>'的最低数目，数字字符',
'mx_minimum_special_chars'=>'最低数目的特殊字符',
'mx_password_policy'=>'<b>密码政策： < / b > <br/>',
'mx_upper_and_lower_case_chars'=>'大写和小写字符',

/* './classes/ISMSPolicy.php' */

'mx_policy'=>'政策',

/* './classes/ISMSProfile.php' */

'mx_profile'=>'外形',
'st_profile_remove_error'=>'不可能删除的个人资料的<b> ％ profile_name ％ < / b > ：有用户与此相关的个人资料。',
'tt_profile_remove_error'=>'时发生错误，消除简介',

/* './classes/ISMSScope.php' */

'mx_scope'=>'范围',

/* './classes/ISMSSession.php' */

'ad_login_message'=>'％ label_user ％的<b> ％用户名％ < / b > ％行动％ 。',

/* './classes/ISMSUser.php' */

'mx_user'=>'用户',
'st_admim_module_name'=>'政府当局',
'st_user_remove_error'=>'不可能删除用户的<b> ％的用户名％ < / b > ：用户是负责的一个或多个项目的系统或是一个特殊的用户。',
'tt_user_remove_error'=>'时发生错误，删除用户',

/* './classes/nonauto/admin/ISMSDefaultConfig.php' */

'db_area_priority_high'=>'3 -高',
'db_area_priority_low'=>'1 -低',
'db_area_priority_middle'=>'2 -中等',
'db_area_type_factory_unit'=>'工厂',
'db_area_type_filial'=>'附属',
'db_area_type_matrix'=>'总部',
'db_area_type_regional_office'=>'区域办事处',
'db_control_cost_hardware'=>'硬件',
'db_control_cost_material'=>'材料',
'db_control_cost_others'=>'教育',
'db_control_cost_pessoal'=>'人们',
'db_control_cost_software'=>'软件',
'db_control_type_based_on_risk_analize'=>'基于风险分析',
'db_control_type_laws_and_regulamentation'=>'遵约问题',
'db_control_type_others'=>'其他',
'db_document_type_1'=>'机密',
'db_document_type_2'=>'限制',
'db_document_type_3'=>'体制',
'db_document_type_4'=>'市民',
'db_impact_1'=>'非常低',
'db_impact_2'=>'低的',
'db_impact_3'=>'中等',
'db_impact_4'=>'高的',
'db_impact_5'=>'非常高',
'db_impact3_1'=>'低的',
'db_impact3_2'=>'中等',
'db_impact3_3'=>'高的',
'db_importance_1'=>'微不足道',
'db_importance_2'=>'低的',
'db_importance_3'=>'中等',
'db_importance_4'=>'高的',
'db_importance_5'=>'关键',
'db_importance3_1'=>'低的',
'db_importance3_2'=>'中等',
'db_importance3_3'=>'高的',
'db_incident_cost_aplicable_penalty'=>'美好的',
'db_incident_cost_direct_lost'=>'直接损失',
'db_incident_cost_enviroment'=>'环境恢复',
'db_incident_cost_indirect_lost'=>'间接损失',
'db_process_priority_high'=>'3 -高',
'db_process_priority_low'=>'1 -低',
'db_process_priority_middle'=>'2 -中等',
'db_process_type_4'=>'人力资源',
'db_process_type_5'=>'信息技术',
'db_process_type_6'=>'财务与会计',
'db_process_type_7'=>'环保，卫生及安全',
'db_process_type_8'=>'性能改进',
'db_process_type_administrative'=>'市场与客户',
'db_process_type_operational'=>'新产品开发',
'db_process_type_support'=>'生产及交货',
'db_rcimpact_0'=>'不影响',
'db_rcimpact_1'=>'一水平下降',
'db_rcimpact_2'=>'2水平下降',
'db_rcimpact_3'=>'3水平下降',
'db_rcimpact_4'=>'4水平下降',
'db_rcimpact3_0'=>'不影响',
'db_rcimpact3_1'=>'一水平下降',
'db_rcimpact3_2'=>'2水平下降',
'db_rcprob_0'=>'不影响',
'db_rcprob_1'=>'一水平下降',
'db_rcprob_2'=>'2水平下降',
'db_rcprob_3'=>'3水平下降',
'db_rcprob_4'=>'4水平下降',
'db_rcprob3_0'=>'不影响',
'db_rcprob3_1'=>'一水平下降',
'db_rcprob3_2'=>'2水平下降',
'db_register_type_1'=>'机密',
'db_register_type_2'=>'限制',
'db_register_type_3'=>'体制',
'db_register_type_4'=>'市民',
'db_risk_confidenciality'=>'保密',
'db_risk_disponibility'=>'供货',
'db_risk_intregrity'=>'完整性',
'db_risk_type_administrative'=>'行政',
'db_risk_type_fisic'=>'物理',
'db_risk_type_tecnologic'=>'科技',
'db_risk_type_threat'=>'威胁',
'db_risk_type_vulnerability'=>'脆弱性',
'db_risk_type_vulnerability_x_threat'=>'事件（ x脆弱性的威胁）',
'db_riskprob_1'=>'罕见（ 1 ％ -2 0％ ）',
'db_riskprob_2'=>'不大可能（ 21 ％ -4 0％ ）',
'db_riskprob_3'=>'中度（ 41 ％ -6 0％ ）',
'db_riskprob_4'=>'可能（ 61 ％ -8 0％ ）',
'db_riskprob_5'=>'几乎可以肯定， （ 81 ％ -9 9％ ）',
'db_riskprob3_1'=>'不大可能',
'db_riskprob3_2'=>'温和',
'db_riskprob3_3'=>'可能',

/* './classes/nonauto/FeedbackEvent.php' */

'em_isms_feedback'=>'axur主义反馈',

/* './classes/nonauto/policy_management/DocumentsDrawGrid.php' */

'to_future_publication'=>'该文件将只提供给读者关于日期％日期％',

/* './classes/policy_management/PMDocInstance.php' */

'mx_document_instance'=>'文件，例如',

/* './classes/policy_management/PMDocument.php' */

'mx_document'=>'文件',
'st_denied_permission_to_manage_document'=>'您没有权限来管理这个文件。',
'st_document_remove_error'=>'不可能删除文件的<b> ％名称％ < / b > 。该文件载有小组的文件。要删除的文件包含子文件，启动选项“删除级联”在“政府当局-> ”配置“ 。',
'st_policy_management'=>'政策管理',
'tt_document_remove_error'=>'时发生错误删除的文件。',

/* './classes/policy_management/PMRegister.php' */

'mx_day_days'=>'天（ ）',
'mx_month_months'=>'本月（ ）',
'mx_register'=>'记录',
'mx_week_weeks'=>'周（ ）',

/* './classes/policy_management/PMTemplate.php' */

'mx_document_template'=>'文件范本',

/* './classes/policy_management/report/ISMSReportAccessedDocumentsFilter.php' */

'rs_users'=>'用户',

/* './classes/policy_management/report/ISMSReportDocsWithHighFrequencyRevisionFilter.php' */

'report_filter_all_userid'=>'全部',
'report_filter_high_frequency_revision_definition'=>'高频率的修订是：',
'report_filter_high_frequency_revision_definition_explanation'=>'一个以上的修订，在一个星期内。',

/* './classes/policy_management/report/ISMSReportDocumentAccessesFilter.php' */

'rs_accesses'=>'通道',
'rs_only_last'=>'只有最后一',
'rs_show_all'=>'显示所有',

/* './classes/policy_management/report/ISMSReportDocumentDatesFilter.php' */

'rs_after_initialdate'=>'后％ initialdate ％',
'rs_before_finaldate'=>'前％ finaldate ％',
'rs_between_initialfinaldate'=>'之间％ initialdate ％和％ finaldate ％',
'rs_creation_date'=>'作品创作日期',
'rs_last_revision_date'=>'最近修订日期',

/* './classes/policy_management/report/ISMSReportDocumentsByComponentFilter.php' */

'report_filter_all_doctype'=>'全部',
'report_filter_doctype_cl'=>'文件类型：',

/* './classes/policy_management/report/ISMSReportDocumentsByStateFilter.php' */

'report_filter_showregisterdocs_cl'=>'查看记录文件：',
'report_filter_status_cl'=>'文件\'的地位',
'rs_all_status'=>'全部',

/* './classes/policy_management/report/ISMSReportDocumentSummary.php' */

'rs_approvers_list_section'=>'批准者名单',
'rs_author'=>'作者',
'rs_components_list_section'=>'组件列表',
'rs_document_type_management'=>'管理',
'rs_documents_type_others'=>'其他',
'rs_file'=>'文件',
'rs_general_info_section'=>'一般资料',
'rs_keywords'=>'关键词',
'rs_link'=>'链接',
'rs_no_approvers_message'=>'这份文件并没有任何批准者。',
'rs_no_components_message'=>'这份文件并没有任何元件。',
'rs_no_readers_message'=>'这份文件并没有任何以飨读者。',
'rs_no_references_message'=>'这份文件并没有任何参考。',
'rs_no_registers_message'=>'这份文件并没有任何纪录。',
'rs_no_subdocs_message'=>'这份文件并没有任何子文档。',
'rs_published_date'=>'发表在',
'rs_readers_list_section'=>'读者名单',
'rs_references_list_section'=>'参考名单',
'rs_registers_list_section'=>'记录清单',
'rs_report_not_supported'=>'报告中不支持此格式',
'rs_subdocs_list_section'=>'子文档列表',

/* './classes/policy_management/report/ISMSReportDocumentsWithoutRegisterFilter.php' */

'report_filter_all_document_responsible'=>'全部',
'report_filter_document_responsible_cl'=>'负责文件：',

/* './classes/policy_management/report/ISMSReportNotAccessedDocumentsFilter.php' */

'rs_expand_readers'=>'扩大读者',

/* './classes/policy_management/report/ISMSReportTopRevisedDocumentsFilter.php' */

'rs_expand_documents'=>'扩大文件',
'rs_show_only_first_cl'=>'只显示第一：',

/* './classes/policy_management/report/ISMSReportUserCommentsFilter.php' */

'rs_all_documents'=>'全部',
'rs_documents_cl'=>'文件：',

/* './classes/policy_management/report/ISMSReportUsersByProcessFilter.php' */

'rs_no'=>'否',
'rs_organize_by_area'=>'组织由区',
'rs_yes'=>'是',

/* './classes/policy_management/report/ISMSReportUsersWithPendantReadFilter.php' */

'report_filter_all_document_reader_cl'=>'全部',
'report_filter_document_reader_cl'=>'文件读者',

/* './classes/risk_management/report/ISMSReportBestPracticesFilter.php' */

'rs_only_applied'=>'只适用于',

/* './classes/risk_management/report/ISMSReportConformityFilter.php' */

'rs_all'=>'全部',
'rs_standard_filter_cl'=>'标准过滤器：',

/* './classes/risk_management/report/ISMSReportControlCostByCostCategoryFilter.php' */

'rs_costcategory_cl'=>'类别：',

/* './classes/risk_management/report/ISMSReportControlPlanningFilter.php' */

'rs_control_types_cl'=>'控制类型：',

/* './classes/risk_management/report/ISMSReportControlsFilter.php' */

'rs_delayed'=>'延迟',
'rs_implementationstatus_cl'=>'只显示：',
'rs_not_trusted_controls'=>'不可靠的控制',
'rs_trusted_and_efficient_controls'=>'realiable和有效率的管制',
'rs_under_implementation'=>'正在执行',

/* './classes/risk_management/report/ISMSReportEventsByCategoryFilter.php' */

'rs_category_cl'=>'类别：',
'rs_event_types_cl'=>'事件类型：',

/* './classes/risk_management/report/ISMSReportGenericClassifTypePrioFilter.php' */

'lb_event_types_cl'=>'事件类型：',

/* './classes/risk_management/report/ISMSReportParametersFilter.php' */

'rs_considered_parameters_cl'=>'考虑参数：',

/* './classes/risk_management/report/ISMSReportProcessesByPriority.php' */

'rs_undefined_priority'=>'未定义优先',

/* './classes/risk_management/report/ISMSReportRiskFinancialImpactFilter.php' */

'mx_accepted'=>'接受',
'mx_avoided'=>'避免',
'mx_not_treated'=>'不治疗',
'mx_reduced'=>'减少',
'mx_transferred'=>'转移',
'mx_treated'=>'治疗',
'rs_not_treated_or_accepted'=>'不治疗或接受',
'rs_treatment_cl'=>'治疗：',

/* './classes/risk_management/report/ISMSReportRiskImpactFilter.php' */

'lb_considered_parameters_cl'=>'考虑参数：',
'lb_risk_types_cl'=>'风险类型：',

/* './classes/risk_management/report/ISMSReportRisksByAreaFilter.php' */

'lb_area_priorities_cl'=>'地区优先事项：',
'lb_area_types_cl'=>'面积类型：',
'rs_area_cl'=>'地区：',

/* './classes/risk_management/report/ISMSReportRisksByProcessFilter.php' */

'lb_process_priorities_cl'=>'过程中的优先事项：',
'lb_process_types_cl'=>'过程类型：',
'st_all'=>'全部',

/* './classes/risk_management/report/ISMSReportRisksByType.php' */

'rs_undefined_type'=>'未定义类型',

/* './classes/risk_management/report/ISMSReportRisksFilter.php' */

'rs_estimated_risks'=>'估计风险',
'rs_not_estimated'=>'未估计',
'rs_not_estimated_risks'=>'非估计风险',
'rs_not_treated_risks'=>'非治疗的风险',
'rs_residual_value_cl'=>'剩余的风险：',
'rs_show_only_cl'=>'只显示：',
'rs_treated_risks'=>'治疗的风险',
'rs_value_cl'=>'潜在风险：',

/* './classes/risk_management/report/ISMSReportRiskValuesFilter.php' */

'rs_high'=>'高的',
'rs_low'=>'低的',
'rs_medium'=>'中等',
'rs_not_parameterized'=>'未估计',
'rs_risk_color_based_on_cl'=>'风险颜色的基础上：',
'rs_risk_types_cl'=>'风险类型：',
'rs_risks_values_cl'=>'风险值：',

/* './classes/risk_management/report/ISMSReportStatementOfApplicabilityFilter.php' */

'lb_control_types_cl'=>'控制类型：',

/* './classes/risk_management/report/ISMSReportTop10AssetsWithMoreRisksFilter.php' */

'lb_risk_types'=>'风险类型',

/* './classes/risk_management/riskparameters/RiskParameters.php' */

'mx_justification_bl'=>'原因',
'mx_justifying'=>'理由',

/* './classes/risk_management/RMAcceptRisk.php' */

'mx_risk_acceptance'=>'接受风险',

/* './classes/risk_management/RMArea.php' */

'mx_area'=>'面积',
'st_area_remove_error_message'=>'不可能的，以消除业务领域的<b> ％ area_name ％ < / b > ：该地区蕴含的分领域和/或进程。要删除业务领域包含的分领域或进程，启动选项“删除级联”在“政府当局-> ”设置“ 。',
'tt_area_remove_erorr'=>'时发生错误，消除业务领域',

/* './classes/risk_management/RMAsset.php' */

'mx_asset'=>'资产',
'st_asset_remove_error_message'=>'不可能的，以消除资产的<b> ％ asset_name ％ < / b > ：有相关的风险与资产。要删除的资产，其中有相关的风险，启动选项“删除级联”在“政府当局-> ”设置“',
'tt_asset_remove_error'=>'时发生错误，消除资产',

/* './classes/risk_management/RMBestPractice.php' */

'mx_best_practice'=>'最佳做法',
'st_best_practice_remove_error_message'=>'这是不可能消除的最佳做法的<b> ％best_practice_name％ </b>：最好的做法是与一个或一个以上的管制。要删除的最佳做法是与一个或一个以上的管制，用户可以选择“删除级联”在“政府当局-> \' s',
'tt_best_practice_remove_error'=>'错误时，删除的最佳做法',

/* './classes/risk_management/RMBestPracticeEvent.php' */

'mx_best_practice_event_association'=>'协会的最佳实践活动',

/* './classes/risk_management/RMBestPracticeStandard.php' */

'mx_best_practice_standard_association'=>'协会的最佳做法标准',

/* './classes/risk_management/RMCategory.php' */

'mx_category'=>'类别',

/* './classes/risk_management/RMControl.php' */

'mx_control'=>'控制',
'st_rm_module_name'=>'风险管理',

/* './classes/risk_management/RMControlBestPractice.php' */

'mx_control_best_practice_association'=>'协会的最佳做法，以控制',

/* './classes/risk_management/RMControlImplementationAccept.php' */

'mx_control_implementation_acceptance'=>'接受控制的执行情况',

/* './classes/risk_management/RMEvent.php' */

'mx_event'=>'事件',

/* './classes/risk_management/RMProcess.php' */

'mx_process'=>'过程',

/* './classes/risk_management/RMProcessAsset.php' */

'mx_process_asset_association'=>'协会的资产过程中',

/* './classes/risk_management/RMRisk.php' */

'mx_risk'=>'风险',

/* './classes/risk_management/RMRiskControl.php' */

'mx_risk_control_association'=>'协会控制风险',

/* './classes/risk_management/RMRiskLimits.php' */

'mx_risk_tolerance'=>'风险容忍度',

/* './classes/risk_management/RMSectionBestPractice.php' */

'mx_best_practice_section'=>'最佳practice节',
'st_home'=>'家',
'st_section_control_best_practice_remove_error_message'=>'不可能删除的<b>第％的Section_Name ％ < / b > ：区段包含的最佳做法和/或分节，其中包含相关的最佳做法与管制。',
'st_section_remove_error_message'=>'不可能删除的<b>第％的Section_Name ％ < / b > ：区段包含的最佳做法和/或小节。删除条文，包含小节或最佳做法，启动选项“删除级联”在“政府当局-> ”设置“',
'tt_section_remove_error'=>'时发生错误，消除节',

/* './classes/risk_management/RMStandard.php' */

'mx_standard'=>'标准',
'st_libraries_module_name'=>'图书馆',
'st_remove_standard_error'=>'这是不可能的删除标准的 <b>％standard_name</b>, 因为它是有关最佳做法。',
'tt_remove_standard_error'=>'错误，而删除标准',

/* './classes/WKFAlert.php' */

'em_alert_check_list'=>'<b>在以检视提高警觉，进入系统并检查您的活动清单。 < / b > <br>如需进一步资料，有关此警报，取得联系，与保安人员。',
'wk_ap_conclusion'=>'该行动计划％名称％已经完成，换言之，它已被测量。',
'wk_ap_efficiency_revision_late'=>'修改的效率行动计划 \' ％名称％  』已过期。',
'wk_ap_efficiency_revision_near'=>'修订日期效率行动计划\' ％名称％ \'是密切。',
'wk_approved'=>'已得到批准。',
'wk_asset_np_risk_association'=>'非估计风险与资产 \' ％资产％  \' ，从建议的事件 \' ％事件％  \' 。',
'wk_control_implementation'=>'控制 \' ％名称％  \'必须得到执行。',
'wk_control_test_late'=>'后续行动的控制测试 \' ％名称％  \'是晚了。',
'wk_deadline_close'=>'截止日期为批准该文件的 \' ％姓名％  \'正在到来。',
'wk_deadline_expired'=>'截止日期为批准文件 \' ％名称％  \'已经过期。',
'wk_delegated'=>'已授予给您。',
'wk_denied'=>'已被拒绝。',
'wk_disciplinary_process'=>'用户（ ） ％的用户％ /已添加到纪律的过程中的事件％名称％ 。',
'wk_document_comment'=>'文件 \' ％名称％  \'收到了评论。',
'wk_document_is_now_published'=>'文件\' ％名称％ \'出版。',
'wk_document_new_version'=>'文件 \' ％名称％  \'有了新的版本。',
'wk_document_publication'=>'该文件 \' ％名称％  \' ，被送往出版。',
'wk_document_revision'=>'文件 \' ％名称％  \' ，被送往修改',
'wk_document_revision_late'=>'修改文件\' ％名称％ \'是延迟。',
'wk_implementation_expired'=>'实施控制 \' ％名称％  \'已经过期。',
'wk_inc_asset'=>'事件 \' ％事件％  \'已涉及到资产 \' ％资产％  \' 。',
'wk_inc_control'=>'事件 \' ％事件％  \'已涉及到控制 \' ％控制％  \' 。',
'wk_inc_disposal_app'=>'立即处置这一事件％名称％获得通过。',
'wk_inc_disposal_denied'=>'立即处置这一事件％名称％ ，被拒绝。',
'wk_inc_evidence_required'=>'收集证据的过程中所需的事件\' ％名称％ \' 。',
'wk_inc_sent_to_responsible'=>'事件 \' ％名称％  \'是转发给您。',
'wk_inc_solution_app'=>'要解决这一事件％名称％ ，已得到批准。',
'wk_inc_solution_denied'=>'要解决这一事件％名称％ ，已被拒绝。',
'wk_incident'=>'事件 \' ％名称％  \'已经建立。',
'wk_incident_risk_parametrization'=>'风险％ risk_name ％ ，已相关的资产％ asset_name ％ ，您有责任。风险参数是不同的从原来的风险。',
'wk_non_conformity_waiting_conclusion'=>'不符合％名称％ ，等待整理。',
'wk_non_conformity_waiting_deadline'=>'不符合％名称％等待的最后期限。',
'wk_pre_approved'=>'已预先核准。',
'wk_probability_update_down'=>'频率事件相关的风险\' ％名称％ \' ，在过去的时期，显示的可能性是低于估计。',
'wk_probability_update_up'=>'频率事件相关的风险\' ％名称％ \' ，在过去的时期，表明的可能性大于是什么指定。',
'wk_real_efficiency_late'=>'后续的实质管制效率 \' ％名称％  \'是晚了。',
'wk_risk_created'=>'非估计风险 \' ％风险％  \'已被相关的资产 \' ％资产％  \' 。',
'wk_warning'=>'警告',

/* './classes/WKFSchedule.php' */

'mx_last'=>'最后的',
'mx_second_last'=>'第二，以去年',
'mx_second_wee'=>'第二',
'st_schedule_and'=>'和',
'st_schedule_april'=>'4月',
'st_schedule_august'=>'8月',
'st_schedule_daily'=>'每一天',
'st_schedule_day_by_month'=>'关于天％天％',
'st_schedule_day_by_month_inverse'=>'对％天％天',
'st_schedule_day_by_week'=>'对周％ ％ ％ week_day ％',
'st_schedule_december'=>'12月',
'st_schedule_each_n_days'=>'每一个周期％ ％天',
'st_schedule_each_n_months'=>'每一个周期％ ％个月， ％ day_expression ％的一个月',
'st_schedule_each_n_weeks'=>'对％ week_days ％ ，每一个周期％ ％周',
'st_schedule_endless_interval'=>'从％开始％',
'st_schedule_february'=>'二月',
'st_schedule_friday_pl'=>'五',
'st_schedule_interval'=>'％之间开始％和％年底％',
'st_schedule_january'=>'一月',
'st_schedule_july'=>'7月',
'st_schedule_june'=>'6月',
'st_schedule_march'=>'三月',
'st_schedule_may'=>'可能',
'st_schedule_monday_pl'=>'星期一',
'st_schedule_monthly'=>'％ day_expression ％ ，每个月都有',
'st_schedule_november'=>'十一月',
'st_schedule_october'=>'10月',
'st_schedule_saturday_pl'=>'星期六',
'st_schedule_september'=>'9月',
'st_schedule_specified_months'=>'％ day_expression ％ ％个月％',
'st_schedule_sunday_pl'=>'星期日',
'st_schedule_thursday_pl'=>'星期四',
'st_schedule_tuesday_pl'=>'星期二',
'st_schedule_wednesday_pl'=>'星期三',
'st_schedule_weekly'=>'每％ week_days ％',

/* './classes/WKFTask.php' */

'wk_task'=>'任务',
'wk_task_execution_permission'=>'您没有权限去执行任务。',

/* './crontab.php' */

'em_saas_abandoned_system'=>'被遗弃的系统',

/* './damaged_system.php' */

'st_system_corrupted'=>'你的系统可能被损坏。请，重复安装或与我们的支持，在axur的网站。',

/* './damaged_system.xml' */

'tt_damaged_system'=>'系统损坏',

/* './email_sender.php' */

'em_daily_digest'=>'每日摘要',

/* './graphs/ISMSGraphAPActionType.php' */

'mx_corrective_action'=>'纠正行动',
'mx_preventive_action'=>'预防行动',

/* './graphs/ISMSGraphAssetCost.php' */

'st_assets_cost_above_1m'=>'资产与价值观美元以上1.000.000',
'st_assets_cost_below_100k'=>'资产价值低于一百点○○○美元',
'st_assets_cost_between_1m_and_500k'=>'资产与价值观美元之间1.000.000和五百点零零零美元',
'st_assets_cost_between_500k_and_100k'=>'资产价值之间的五百点〇 〇 〇美元e一百点〇 〇 〇美元',

/* './graphs/ISMSGraphAssetEstimatedCost.php' */

'st_assets_with_estimated_cost'=>'资产的估计费用',
'st_assets_without_estimated_cost'=>'资产的估计费用',

/* './graphs/ISMSGraphControlEfficiency.php' */

'mx_expected_efficiency'=>'预期效益',
'mx_real_efficiency'=>'真正的效率',

/* './graphs/ISMSGraphControlTest.php' */

'mx_test_not_ok'=>'测试没有确定',
'mx_test_ok'=>'试验确定',
'mx_test_value'=>'测试值',

/* './graphs/ISMSGraphDocumentsAccessed.php' */

'st_accessed_documents'=>'访问文件',
'st_accessed_documents_cumulative'=>'访问的文件-累积',

/* './graphs/ISMSGraphDocumentsCreated.php' */

'st_created_documents'=>'创建文件',
'st_created_documents_cumulative'=>'创建的文件-累积',

/* './graphs/ISMSGraphDocumentsType.php' */

'mx_management'=>'管理',
'mx_others'=>'其他',
'mx_without_type'=>'没有类型',

/* './graphs/ISMSGraphIncidentCategory.php' */

'st_others'=>'其他',

/* './graphs/ISMSGraphIncidentLossType.php' */

'mx_direct_losses'=>'直接损失',
'mx_indirect_losses'=>'间接损失',

/* './graphs/ISMSGraphPotentialRisk.php' */

'st_high_risks'=>'高风险',
'st_low_risks'=>'低风险',
'st_medium_risks'=>'中等风险',
'st_non_parameterized_risks'=>'非估计风险',

/* './graphs/ISMSGraphResidualRisk.php' */

'st_high_risk'=>'高风险',
'st_low_risk'=>'低风险',
'st_medium_risk'=>'中等风险',
'st_non_parameterized_risk'=>'非估计风险',

/* './graphs/ISMSGraphTreatedRisk.php' */

'st_avoided_risk'=>'避免风险',
'st_risk_accepted_being_yellow_or_red'=>'风险接受，中或高',
'st_risk_accepted_for_being_green'=>'风险接受低',
'st_risk_treated_with_user_of_control'=>'风险处理与使用控制',
'st_transferred_risk'=>'转移风险',

/* './handlers/QueryCountDocsTimeToApprove.php' */

'gs_day'=>'白天',
'gs_days'=>'天',
'gs_hour'=>'小时',
'gs_hours'=>'小时',
'gs_minute'=>'分钟',
'gs_minutes'=>'分钟',
'gs_not_applicable'=>'娜',
'gs_second'=>'第二',
'gs_seconds'=>'秒',

/* './invalid_activation_code.php' */

'st_invalid_activation_code'=>'激活代码使用无效。请，修改或联络我们的支援小组在axur的网站。',

/* './invalid_activation_code.xml' */

'tt_invalid_activation_code'=>'无效的激活代码',

/* './license_corrupted.xml' */

'st_license_corrupted_message'=>'您的系统是使用无效许可或其中已损坏。',
'tt_license_corrupted'=>'无效或损坏的许可',

/* './license_expired.php' */

'st_license_expired'=>'您的许可证过期的<b> ％ expiracy_str ％ < / b > 。',

/* './license_expired.xml' */

'tt_license_expired'=>'许可证过期',

/* './license_isms.php' */

'st_incident_management'=>'不断改善',
'st_number_of_modules'=>'若干模块',
'st_risk_management'=>'风险管理',
'st_system_modules_bl'=>'<b>系统模块< / b >',

/* './license_limit_reached.php' */

'st_assets'=>'资产',
'st_modules'=>'模块',
'st_standards'=>'标准',
'st_users'=>'用户',

/* './license_limit_reached.xml' */

'st_license_limit_reached'=>'的最大数目％属性％ ，已达成共识。是不可能的添加％属性％ 。',
'tt_license_limit_reached'=>'极限％属性％ ，已达成',

/* './login.php' */

'ad_login_error_user_and_password_do_not_match'=>'％ user_login ％ （ ％ user_ip ％ ）',
'ad_login_log_message'=>'％ label_user ％的<b> ％用户名％ < / b > ％行动％ 。',
'mx_session_conflict'=>'登录失败是因为另一个用户签署的从同一个浏览器。',
'st_blocked_instance_message'=>'例如封锁',
'tt_blocked_instance'=>'例如封锁',
'tt_session_conflict'=>'登录失败',

/* './login.xml' */

'cb_remember_password'=>'记住密码',
'cb_remember_user'=>'记住用户',
'lb_password_cl'=>'密码：',
'st_login_panel'=>'登录面板',
'vb_forgot_your_password'=>'忘记您的密码？',
'vb_login'=>'注册',
'wn_denied_access'=>'用户未经允许进入系统！',
'wn_invalid_user_or_password'=>'无效的用户或密码！',
'wn_max_users'=>'人数最多的同时登录达成！',
'wn_user_blocked'=>'阻止用户！',

/* './migration/ISMS_import.php' */

'ad_created'=>'创建',
'ad_edited'=>'编辑',
'ad_removed_from_bin'=>'删除回收站',

/* './migration/ISMS_migration.php' */

'st_destination_db_error'=>'错误当连接到移民的目的地数据库。  n  n已确认，如果输入的数据是正确的！',
'st_source_db_error'=>'错误当连接到移民的来源资料库。  n  n已确认，如果输入的数据是正确的！',

/* './migration/ISMS_migration.xml' */

'lb_database_name_cl'=>'数据库名称：',
'lb_database_password_cl'=>'数据库密码：',
'lb_database_user_name_cl'=>'数据库的用户名：',
'lb_hostname_cl'=>'主机：',
'tt_database_copy_bl'=>'<b>数据库副本< / b >',
'tt_destiny_database_bl'=>'命运的<b>数据库< / b >',
'tt_results_bl'=>'的<b>结果< / b >',
'tt_source_database_bl'=>'<b>来源数据库< / b >',

/* './nav_graphics.xml' */

'lb_graphic_cl'=>'选择一个图：',
'mx_10_assets_with_most_incidents'=>'前10名的资产与更多事件',
'mx_10_assets_with_most_residual_risks_in_red'=>'前10名的资产与最高风险',
'mx_10_assets_with_most_risks'=>'前10名的资产与更多的风险',
'mx_10_document_with_most_registers'=>'前10名的文件，更记录',
'mx_10_processes_with_most_associated_assets'=>'前10名的进程与更多的相关资产',
'mx_10_processes_with_most_documents'=>'前10名的进程与更多的文件',
'mx_10_processes_with_most_incidents'=>'前10名的过程有更多事件',
'mx_10_processes_with_most_nc_summary'=>'前10名的过程更不符合',
'mx_10_processes_with_most_residual_risks_in_red'=>'前10名的进程与最高风险',
'mx_10_processes_with_most_risks'=>'前10名的进程与更多的风险',
'mx_10_processes_with_most_users'=>'前10名的进程与人民更多地参与',
'mx_amount_of_accessed_documents'=>'数额存取文件',
'mx_amount_of_created_documents'=>'数额创建文件',
'mx_amount_of_documents_by_classification'=>'数额的文件分类类型',
'mx_amount_of_documents_by_status'=>'数额的文件状态',
'mx_amount_of_documents_by_type'=>'数额的文件类型',
'mx_ap_action_type_proportion'=>'行动计划的比例和他们的类型',
'mx_assets_with_without_estimated_costs'=>'资产/无费用估计价值',
'mx_history_of_control_test'=>'历史的控制测试',
'mx_history_of_controls_efficiency_revision'=>'历史上的管制效率修订',
'mx_incident_category_proportion'=>'事件和类别比例',
'mx_incident_financial_impact_summary'=>'十大金融事件的影响',
'mx_incident_loss_type_proportion'=>'事故和损失类型的比例',
'mx_incidents_created_summary'=>'总结最近6个月内事件',
'mx_potential_risks_summary'=>'潜在风险概要',
'mx_residual_risks_summary'=>'剩余的风险概要',
'mx_summary_of_assets_costs'=>'简要的资产和成本',
'mx_summary_of_risks_treatments'=>'综述风险的治疗',
'si_optgroup_ci'=>'不断改善',
'si_optgroup_cost'=>'成本',
'si_optgroup_pm'=>'政策经理',
'si_optgroup_revision'=>'修订',
'si_optgroup_rm'=>'风险经理',
'si_optgroup_test'=>'测试',

/* './nav_report.xml' */


/* './nav_search.xml' */

'cb_document_template'=>'文件范本',
'cb_incident'=>'事件',
'cb_incident_category_library'=>'事件类别',
'cb_non_conformity'=>'不符合',
'cb_occurrence'=>'发生',
'cb_user'=>'用户',
'lb_new_search_bl_cl'=>'<b>新搜索： < / b >',
'rb_action_plan'=>'行动计划',
'rb_advanced'=>'先进',
'rb_basic'=>'基本',
'rb_soluction'=>'解决方案',
'st_creation_cl'=>'创新：',
'st_dates_filter_bl'=>'的日期“过滤器',
'st_modification_cl'=>'修改：',
'to_advanced_search_doc'=>'文件的<b>高级搜索： < / b > <br/>搜索类型，包括档案内容，在搜索，除了基本的搜索领域。',
'to_template_document_advanced_help'=>'<b>高级文件范本搜索< / b > <br/>搜索类型，包括档案内容，在搜索，以及作为基本的搜索领域。',
'tt_data_filter'=>'数据滤波',
'tt_elements_filter'=>'项目筛选',
'vb_dates_filter'=>'的日期“过滤器',
'vb_elements_filter'=>'项目筛选',

/* './nav_statistics.php' */

'mx_action_plan_efficient_and_not'=>'金额效率和非效率的行动计划',
'mx_action_plan_finished_late'=>'金额行动计划年底完成',
'mx_action_plan_finished_on_time'=>'金额行动计划完成时间',
'mx_action_plan_total'=>'金额行动计划',
'mx_area_amount'=>'数额领域',
'mx_area_assets_amount'=>'金额方面的资产',
'mx_area_assets_cost'=>'成本方面的资产',
'mx_area_controls_amount'=>'金额方面的管制',
'mx_area_controls_cost'=>'成本方面的控制',
'mx_area_processes_amount'=>'金额方面的进程',
'mx_area_risks_amount'=>'金额方面的风险',
'mx_area_total_impact'=>'总面积的影响',
'mx_area_values'=>'区价值',
'mx_areas_risk_summary'=>'综述领域的风险',
'mx_areas_without_processes'=>'地区无进程',
'mx_asset_controls_amount'=>'数额的资产管制',
'mx_asset_controls_cost'=>'资产的成本控制',
'mx_asset_processes_amount'=>'数额资产的进程',
'mx_asset_risks_amount'=>'数额的资产风险',
'mx_asset_total_impact'=>'总资产的影响',
'mx_asset_values'=>'资产价值',
'mx_assets_amount'=>'资产总额',
'mx_assets_risk_summary'=>'综述资产风险',
'mx_assets_total_cost'=>'总资产的成本',
'mx_assets_without_risk_events'=>'资产风险事件',
'mx_business_areas'=>'业务领域',
'mx_business_processes'=>'业务流程',
'mx_control_risks_amount'=>'数额控制风险',
'mx_controls_adequacy'=>'有足够的控制',
'mx_controls_amount'=>'数量管制',
'mx_controls_implementation'=>'管制的执行情况',
'mx_controls_revision'=>'修订管制',
'mx_controls_test'=>'试验控制',
'mx_controls_without_associated_risks'=>'管制没有相关的风险',
'mx_document_total'=>'数额的文件',
'mx_documents_by_state'=>'数额的文件状态',
'mx_documents_by_type'=>'数额的文件类型',
'mx_implemented_controls_investment'=>'投资实施管制',
'mx_incident_total'=>'数额事件',
'mx_incidents_by_state'=>'数额的事件，每地位',
'mx_nc_per_ap_average'=>'平均金额不符合％行动计划',
'mx_non_conformity_by_state'=>'数额不符合状态',
'mx_non_conformity_total'=>'数额不符合',
'mx_non_parameterized_risks'=>'非估计风险',
'mx_not_measured_implemented_controls'=>'没有衡量实施管制',
'mx_occupation_documents'=>'文件档案ocupation组（ MB ）',
'mx_occupation_registers'=>'记录档案ocupation组（ MB ）',
'mx_occupation_template'=>'模板文件ocupation组（ MB ）',
'mx_occupation_total'=>'总占领组（ MB ）',
'mx_process_assets_amount'=>'金额过程中的资产',
'mx_process_assets_cost'=>'成本的过程中的资产',
'mx_process_controls_amount'=>'金额过程控制',
'mx_process_controls_cost'=>'成本过程控制',
'mx_process_risks_amount'=>'金额过程中的风险',
'mx_process_total_impact'=>'全过程的影响',
'mx_process_values'=>'过程中的价值观',
'mx_processes_amount'=>'数额进程',
'mx_processes_risk_summary'=>'摘要进程的风险',
'mx_processes_without_assets'=>'过程中没有资产',
'mx_read_documents'=>'阅读文件',
'mx_read_documents_proportion'=>'阅读文件的比例',
'mx_register_total'=>'金额纪录',
'mx_risk_treatment_potential_impact'=>'潜在影响的风险治疗',
'mx_risks_amount'=>'数额的风险',
'mx_risks_potential_impact'=>'潜在影响的风险',
'mx_risks_treatment'=>'治疗的风险',
'mx_scheduled_documents'=>'定修订文件的比例',
'mx_scheduled_revision_documents'=>'定修订文件',
'mx_standard_best_practice_amount'=>'数额的最佳做法标准',
'mx_total_documents'=>'总的文件',
'mx_unread_documents'=>'非读文件',
'mx_unread_documents_proportion'=>'非阅读文件的比例',
'si_abnormalities'=>'异常',
'si_action_plan_statistics'=>'行动计划统计',
'si_area_statistics'=>'地区统计',
'si_asset_statistics'=>'资产统计',
'si_best_practice_summary'=>'最佳做法摘要',
'si_continual_improvement'=>'不断改进统计',
'si_control_summary'=>'控制摘要',
'si_financial_statistics'=>'金融统计',
'si_management_level_and_range'=>'范围和层次的管理',
'si_policy_management'=>'政策管理统计',
'si_process_statistics'=>'进程统计',
'si_risk_summary'=>'风险摘要',
'si_risks_summary'=>'风险概要',
'si_system_status'=>'系统状态',
'st_accepted_risk'=>'接受风险',
'st_applied_best_practices_amount'=>'金额适用的最佳做法',
'st_best_practices_amount'=>'数额的最佳做法',
'st_complete_areas'=>'完整的地区',
'st_complete_assets'=>'完成资产',
'st_complete_processes'=>'完成进程',
'st_controls_being_implemented'=>'管制正在实施',
'st_controls_correctly_implemented'=>'管制的正确实施',
'st_controls_incorrectly_implemented'=>'管制措施不当实施',
'st_controls_with_delayed_implementation'=>'控制与延迟执行',
'st_denied_access_to_statistics'=>'设置指的是数据收集是残疾人士，因此，这是不可能的，以查看此画面。',
'st_efficient_action_plan'=>'高效率的行动计划',
'st_medium_high_not_treated_risk'=>'中/高/不治疗的风险',
'st_mitigated_risk'=>'降低风险',
'st_non_efficient_action_plan'=>'非有效率的行动计划',
'st_not_treated_risks'=>'不治疗的风险',
'st_partial_areas'=>'部分地区',
'st_partial_assets'=>'部分资产',
'st_partial_processes'=>'部分过程',
'st_potentially_low_risk'=>'潜在风险低',
'st_successfully_revised'=>'成功修订',
'st_successfully_tested'=>'试验成功',
'st_tranferred_risk'=>'转移风险',
'st_treated_risks'=>'治疗的风险',
'st_unmanaged_areas'=>'非托管领域',
'st_unmanaged_assets'=>'非托管资产',
'st_unmanaged_processes'=>'非托管进程',
'st_unsuccessfully_revised'=>'不成功的修订',
'st_unsuccessfully_tested'=>'成功测试',

/* './nav_statistics.xml' */

'lb_finish_cl'=>'完成：',
'lb_standard_cl'=>'标准：',
'lb_statistics_cl'=>'统计：',
'rb_adequate'=>'适当的',
'rb_ap_efficient'=>'效率',
'rb_ap_non_efficient'=>'非效率',
'rb_in_implantation'=>'正在执行',
'rb_inadequate'=>'不适合',
'rb_late'=>'延迟',
'rb_not_successfully'=>'没有成功',
'rb_not_treated'=>'不治疗',
'rb_real_risk'=>'潜在风险',
'rb_residual_risk'=>'残余风险',
'rb_successfully'=>'成功',
'rb_treated'=>'治疗',
'st_chart_not_enough_data'=>'有没有足够的数据收集产生图表。',
'vb_refresh'=>'刷新',

/* './nav_summary_advanced.php' */

'gc_amount_bl'=>'的<b> ＃ < / b >',
'gc_applied_bl'=>'的<b>适用< / b >',
'gc_assets_bl'=>'资产的<b> < / b >',
'gc_best_practices_bl'=>'＃的<b>的最佳做法< / b >',
'gc_description_bl'=>'<b>说明< / b >',
'gc_disciplinary_process_amount_bl'=>'＃的<b>的纪律程序< / b >',
'gc_doc_rev_qty_bl'=>'的<b> ＃ < / b >',
'gc_doc_sumary_percentage_bl'=>'的<b> ％ < / b >',
'gc_doc_sumary_qty_bl'=>'的<b> ＃ < / b >',
'gc_document_bl'=>'的<b>文件< / b >',
'gc_efficiency_rates_bl'=>'的<b>效率利率< / b >',
'gc_financial_impact_bl'=>'的<b>的财务影响< / b >',
'gc_implemented_controls_bl'=>'实施管制的<b> < / b >',
'gc_incident_amount_bl'=>'＃的<b>事件的< / b >',
'gc_investment_bl'=>'的<b>投资< / b >',
'gc_nc_amount'=>'的<b> ＃非符合< / b >',
'gc_nc_amount_bl'=>'＃的<b>不符合< / b >',
'gc_planned_controls_bl'=>'计划控制的<b> < / b >',
'gc_potential_bl'=>'<b>可能的< / b >',
'gc_potential_impact_bl'=>'<b>可能的影响， < / b >',
'gc_processes_bl'=>'的<b>进程< / b >',
'gc_residual_bl'=>'剩余的<b> < / b >',
'gc_standards_bl'=>'的<b>标准< / b >',
'gc_status_bl'=>'[状态< / b >',
'gc_total_bl'=>'<b>总< / b >',
'gc_users_bl'=>'雇员的<b> < / b >',
'gs_accepted_risk'=>'接受风险',
'gs_amount_bl'=>'的<b> ＃ < / b >',
'gs_applicability_statement'=>'声明aplicability',
'gs_avoided_risk'=>'避免风险',
'gs_ci_ap_without_doc'=>'行动计划的无证件',
'gs_ci_cn_without_ap'=>'不符合的，没有行动计划',
'gs_ci_incident_without_occurrence'=>'事件没有发生',
'gs_ci_incident_without_risks'=>'事件无风险',
'gs_ci_pending_tasks'=>'不断改进尚未完成的任务',
'gs_components_without_document'=>'组件无证件',
'gs_controls_without_associated_risk'=>'管制没有相关的风险',
'gs_delayed'=>'延迟',
'gs_high_risk'=>'高风险',
'gs_impact_left_bl'=>'的<b>的影响，左< / b >',
'gs_implemented_controls_mitigated_risks_bl'=>'实施管制/减少风险',
'gs_implemented_controls_protected_assets_bl'=>'实施管制/受保护的资产',
'gs_implemented_controls_treated_risks_bl'=>'实施管制/治疗的风险',
'gs_inefficient_controls'=>'非有效率的管制',
'gs_low_risk'=>'低风险',
'gs_medium_risk'=>'中等风险',
'gs_mitigated_risk'=>'降低风险',
'gs_non_parameterized_risks'=>'非估计风险',
'gs_not_measured_controls'=>'没有计量控制',
'gs_not_treated'=>'不治疗',
'gs_not_treated_risks_high_and_medium'=>'不治疗的风险-高，中',
'gs_not_trusted_controls'=>'不可靠的控制',
'gs_pm_docs_with_pendant_read'=>'文件之前，读',
'gs_pm_documents_without_register'=>'文件记录',
'gs_pm_incident_without_risks'=>'文件与高频率的修订',
'gs_pm_never_read_documents'=>'文件从未读过',
'gs_pm_pending_tasks'=>'政策，管理尚未完成的任务',
'gs_pm_users_with_pendant_read'=>'用户等候读',
'gs_potential_risks_low'=>'潜在风险-低',
'gs_risk_management_pending_tasks'=>'风险管理尚未完成的任务',
'gs_risk_treatment_plan'=>'风险处理计划',
'gs_scope_statement'=>'范围声明',
'gs_sgsi_policy_statement'=>'主义政策声明',
'gs_total_bl'=>'<b>总< / b >',
'gs_total_non_parameterized_risks_bl'=>'的<b>非估计的风险， < / b >',
'gs_total_parameterized_risks_bl'=>'<b>估算的风险， < / b >',
'gs_total_risks_bl'=>'<b>总< / b >',
'gs_transferred_risk'=>'转移风险',
'gs_treated'=>'治疗',
'gs_trusted_and_efficient_controls'=>'可靠和高效率的管制',
'gs_under_implementation'=>'正在执行',
'tt_help_impact_left'=>'的<b>的影响，左< / b > <br/> <br/>的总和风险的影响仍然是治疗后。 <br/>这个值代表总数的潜在影响，以该公司是受后的风险治疗。',
'tt_help_implemented_controls_mitigated_risks'=>'实施管制的<b> /减少风险， </b> <br/> <br/>之间的比率的总和成本的implementd控制和总和的潜在影响，减少风险。 <br/>此值显示控制考虑到成本的总和的潜在危险，影响到合作',
'tt_help_implemented_controls_protected_assets'=>'实施管制的<b> /保护资产的< / b > <br/> <br/>之间的比率总和的成本，实施的管制和价值保护的资产。 <br/> ，这是重要的是不要花更多与对照比的价值，保护资产。',
'tt_help_implemented_controls_treated_risks'=>'实施管制的<b> /治疗的风险， </b> <br/> <br/>之间的比率的总和成本的控制和实施的总和的潜在影响治疗的风险。 <br/>此值显示控制考虑到成本的总和的潜在危险，影响到C',
'tt_help_inefficient_controls'=>'的<b>非有效率的管制< / b > <br/> <br/>管制的效率，修订，是不推迟，其最后修订已经被视为非效率（控制真正的效率是低于预期的效益）',
'tt_help_not_measured_controls'=>'<b>这不是衡量管制< / b > <br/> <br/>实施管制，没有效率的修订，也不测试，或其措施，效率，修订，测试或已过期。',
'tt_help_trusted'=>'不可靠的<b>管制< / b > <br/> <br/>管制的测试是不推迟，其最后测试已告失败。',
'tt_help_trusted_and_efficient'=>'的<b>可靠和高效率的管制</b> <br/> <br/>管制，其中有测试和效率，修订启用后，都没有受到延误;和最后测试的表现是可靠的，和上次调整是有效率的（当控制真正的效率是平等的)',

/* './nav_summary_advanced.xml' */

'mi_excel_report'=>'Excel报表',
'mi_html_report'=>'HTML报告',
'mi_html_report_download'=>'HTML报告下载',
'mi_pdf_report'=>'PDF报告',
'mi_word_report'=>'字的报告',
'tt_abnormalities_pm_summary_bl'=>'政策管理的<b>异常摘要< / b >',
'tt_abnormalities_rm_summary_bl'=>'风险管理的<b>异常摘要< / b >',
'tt_abnormality_ci_summary_bl'=>'不断改善的<b>异常摘要< / b >',
'tt_best_practices_summary_bl'=>'的<b>的最佳做法-遵守指数< / b >',
'tt_controls_summary_bl'=>'<b>控制概述< / b >',
'tt_documents_average_approval_time_bl'=>'文件的<b>平均审批时间< / b >',
'tt_financial_summary_bl'=>'的<b>财务概要< / b >',
'tt_grid_incident_summary_title_bl'=>'的<b>事件概述< / b >',
'tt_grid_incidents_per_asset_title_bl'=>'<b>最常见的5资产与更多的事件， < / b >',
'tt_grid_incidents_per_process_title_bl'=>'<b>最常见的5进程与更多的事件， < / b >',
'tt_grid_incidents_per_user_title_bl'=>'<b>最常见的5个用户更多的纪律程序< / b >',
'tt_grid_nc_per_process_title_bl'=>'<b>最常见的5进程与更多的不符合< / b >',
'tt_grid_nc_summary_title_bl'=>'不符合简易程序的<b> < / b >',
'tt_incident_summary_bl'=>'不断改善的<b>摘要< / b >',
'tt_last_documents_revision_time_period_summary_bl'=>'<b>上次文件的修改时间内摘要< / b >',
'tt_parameterized_risks_summary_bl'=>'<b>可能的风险与残余风险< / b >',
'tt_policy_management_summary_bl'=>'的<b>政策管理概述< / b >',
'tt_policy_management_x_risk_management_bl'=>'的<b>份文件的商业元素， < / b >',
'tt_risk_management_documentation_summary_bl'=>'的<b>风险管理文件的摘要< / b >',
'tt_risk_management_status_bl'=>'风险水平的<b> < / b >',
'tt_sgsi_documents_and_reports_bl'=>'的<b>的ISO 27001主义强制性文件< / b >',
'tt_top_10_most_read_documents_bl'=>'的<b>读文件-十大< / b >',

/* './nav_summary_advancedNEW.xml' */

'tt_general_risks_summary_bl'=>'<b>估算与非估计风险< / b >',

/* './nav_summary_basic.php' */

'gc_documents_qty_bl'=>'的<b> ＃文件< / b >',
'gc_documents_status_bl'=>'的<b>文件的地位， < / b >',
'gc_percentage_bl'=>'的<b> ％ < / b >',
'gc_qty_bl'=>'的<b> ＃ < / b >',
'gc_readings_bl'=>'的<b>读< / b >',
'gc_time_period_smaller_or_equal_bl'=>'<b>时间期限（短，或相等于比） < / b >',
'gs_area'=>'面积',
'gs_areas_without_processes'=>'地区无进程',
'gs_asset'=>'资产',
'gs_assets_without_risk_events'=>'资产风险事件',
'gs_average_time'=>'平均时间',
'gs_controls_without_risk_events_association'=>'没有协会的管制与风险事件',
'gs_documents_to_be_read'=>'文件阅读',
'gs_documents_total_bl'=>'总的<b>文件< / b >',
'gs_documents_with_files'=>'文件与档案',
'gs_documents_with_links'=>'文件链接',
'gs_more_than_six_months'=>'6个月以上',
'gs_na'=>'娜',
'gs_one_month'=>'一个月',
'gs_one_week'=>'一周',
'gs_process'=>'过程',
'gs_processes_without_assets'=>'过程中没有资产',
'gs_published_documents_information_bl'=>'的<b>发表的文件< / b >',
'gs_risk'=>'风险',
'gs_six_months'=>'六个月',
'gs_three_months'=>'三个月',

/* './nav_summary_basic.xml' */

'gc_period'=>'期',
'tt_my_documents_average_approval_time_bl'=>'<b>我的文件的平均审批时间< / b >',
'tt_my_documents_last_revision_average_time_summary_bl'=>'<b>我的文件的最后修改时间平均概要< / b >',
'tt_my_documents_policy_management_summary_bl'=>'的<b>政策管理（我的文档）摘要< / b >',
'tt_my_elements_bl'=>'<b>我的项目< / b >',
'tt_my_pendencies_bl'=>'<b>我pendings从风险管理< / b >',
'tt_my_risk_summary_bl'=>'<b>我的风险摘要< / b >',
'tt_policy_management_x_risk_management_my_elements_bl'=>'的<b>风险管理（我的项目） ×政策管理< / b >',
'tt_policy_sumary_bl'=>'政策管理的<b>摘要< / b >',
'tt_risk_management_my_elements_documentation_summary_bl'=>'的<b>风险管理我的项目文件的摘要< / b >',
'tt_risk_summary_bl'=>'风险管理的<b>摘要< / b >',
'tt_top_10_most_read_my_documents_bl'=>'<b>最常见的10个文件，我最读< / b >',

/* './nav_warnings.xml' */

'gc_inquirer'=>'询问者',
'gc_justif'=>'仅仅。',
'mi_denied'=>'拒绝',
'mi_open_task'=>'打开任务',
'tt_alerts_bl'=>'<b>我快讯< / b >',
'tt_tasks_bl'=>'<b>我的任务< / b >',
'vb_remove_alerts'=>'删除警报',
'wn_removal_warning'=>'你必须至少选择一个警告，被删除。',

/* './packages/admin/acl_tree.xml' */

'to_acl_A'=>'权限\'管理\'方面的制度',
'to_acl_A.A'=>'审计标签',
'to_acl_A.A.1'=>'标签的工作清单中所产生的系统',
'to_acl_A.A.2'=>'标签名单警报所产生的系统',
'to_acl_A.A.3'=>'标签与行动，表现在制度',
'to_acl_A.C'=>'系统定制标签',
'to_acl_A.C.1'=>'定制的姓名和金额的风险估计参数。',
'to_acl_A.C.10'=>'定制的重量，每个风险参数',
'to_acl_A.C.11'=>'个性化的系统分类',
'to_acl_A.C.2'=>'定制的风险矩阵的定义，该资产的重要性，估计标签，影响和可能性，以及控制减少准则',
'to_acl_A.C.3'=>'定制的成本估算参数',
'to_acl_A.C.4'=>'定制的财务影响参数',
'to_acl_A.C.5'=>'定制的几个系统的特点',
'to_acl_A.C.6'=>'定制特殊用户',
'to_acl_A.C.7'=>'定制的数量和名称，类型和优先事项，所用的系统',
'to_acl_A.C.7.1'=>'配置方面的分类，在有关的类型和优先',
'to_acl_A.C.7.2'=>'配置过程中的分类，在有关的类型和优先',
'to_acl_A.C.7.3'=>'配置风险分类，在有关类型',
'to_acl_A.C.7.4'=>'配置事件的分类，在有关类型',
'to_acl_A.C.7.5'=>'配置控制的分类，在有关类型',
'to_acl_A.C.7.6'=>'配置文件的分类，在有关类型',
'to_acl_A.C.7.7'=>'配置记录的分类，在有关类型',
'to_acl_A.C.8'=>'定制的安全管制',
'to_acl_A.C.9'=>'定制的风险接受水平',
'to_acl_A.L'=>'回收站标签',
'to_acl_A.L.1'=>'按钮来恢复选定的项目',
'to_acl_A.L.2'=>'按钮可以删除选定项目',
'to_acl_A.L.3'=>'按钮移除所有项目，从回收站',
'to_acl_A.MA'=>'帐户管理标签',
'to_acl_A.P'=>'配置文件选项卡',
'to_acl_A.P.1'=>'菜单修改某简介',
'to_acl_A.P.2'=>'菜单上删除某简介',
'to_acl_A.P.3'=>'按钮，插入一个简介',
'to_acl_A.S'=>'系统设置标签',
'to_acl_A.S.1'=>'密码策略配置',
'to_acl_A.S.2'=>'系统\'删除级联\'配置',
'to_acl_A.U'=>'用户标签',
'to_acl_A.U.1'=>'菜单修改某一个用户',
'to_acl_A.U.2'=>'菜单上删除某一个用户',
'to_acl_A.U.3'=>'按钮，插入一个用户',
'to_acl_A.U.4'=>'按钮来重置密码的所有用户',
'to_acl_A.U.5'=>'菜单上的闭塞系统用户',
'to_acl_M'=>'权限系统的用户视图\'模式',
'to_acl_M.CI'=>'不断改进标签',
'to_acl_M.CI.1'=>'发生标签',
'to_acl_M.CI.1.1'=>'许可，以查看所有的发生',
'to_acl_M.CI.1.2'=>'按钮插入发生',
'to_acl_M.CI.1.3'=>'菜单修改，甚至如果用户既不是经理人或事件的发生，创作者',
'to_acl_M.CI.1.4'=>'菜单上删除，即使用户既不是经理人或事件的发生，创作者',
'to_acl_M.CI.2'=>'事件标签',
'to_acl_M.CI.2.1'=>'权限查看所有事件',
'to_acl_M.CI.2.2'=>'按钮，插入一个事件',
'to_acl_M.CI.2.3'=>'菜单修改，甚至如果用户没有事件经理',
'to_acl_M.CI.2.4'=>'菜单上删除，甚至如果用户没有事件经理',
'to_acl_M.CI.3'=>'不符合标签',
'to_acl_M.CI.3.1'=>'许可，以查看所有的不符合',
'to_acl_M.CI.3.2'=>'按钮，插入一个不符合',
'to_acl_M.CI.3.3'=>'菜单修改，甚至如果用户没有经理人的不符合',
'to_acl_M.CI.3.4'=>'菜单上删除，甚至如果用户没有经理人的不符合',
'to_acl_M.CI.5'=>'纪律处分程序标签',
'to_acl_M.CI.5.1'=>'许可，以查看所采取的行动，在与使用者所涉及的某一个纪律处分程序',
'to_acl_M.CI.5.2'=>'许可登记的行动，必须采取与使用者所涉及的某一个纪律处分程序',
'to_acl_M.CI.5.3'=>'允许删除用户从某一个纪律处分程序',
'to_acl_M.CI.6'=>'报告标签',
'to_acl_M.CI.6.1'=>'事件报告',
'to_acl_M.CI.6.1.1'=>'由发生事故报告',
'to_acl_M.CI.6.1.2'=>'纪律处分程序的报告',
'to_acl_M.CI.6.1.3'=>'收集证据的报告',
'to_acl_M.CI.6.1.4'=>'跟进事件报告',
'to_acl_M.CI.6.1.5'=>'跟进事件报告',
'to_acl_M.CI.6.1.6'=>'财政方面的影响，由事故报告',
'to_acl_M.CI.6.1.7'=>'控制的修改而导致的事故报告',
'to_acl_M.CI.6.1.8'=>'项目受影响的事件报告',
'to_acl_M.CI.6.1.9'=>'自动计算风险的可能性报告',
'to_acl_M.CI.6.2'=>'不符合报告',
'to_acl_M.CI.6.2.1'=>'不符合的，由过程的报告',
'to_acl_M.CI.6.2.2'=>'不符合的控制过程的报告',
'to_acl_M.CI.6.2.3'=>'跟进不符合的报告',
'to_acl_M.CI.6.2.4'=>'行动计划由用户报告',
'to_acl_M.CI.6.2.5'=>'行动计划的过程中的报告',
'to_acl_M.CI.6.3'=>'不断改进报告',
'to_acl_M.CI.6.3.1'=>'不断改进尚未完成的任务的报告',
'to_acl_M.CI.6.3.2'=>'报告的事件，没有相关的风险给他们。',
'to_acl_M.CI.6.3.3'=>'报告不符合，没有行动计划相关的给他们。',
'to_acl_M.CI.6.3.4'=>'行动计划，没有文件报告',
'to_acl_M.CI.6.3.5'=>'事件没有发生的报告',
'to_acl_M.CI.7'=>'行动计划的标签',
'to_acl_M.CI.7.1'=>'允许查看所有行动计划',
'to_acl_M.CI.7.2'=>'按钮，插入一个行动计划',
'to_acl_M.CI.7.3'=>'编辑的权限，任何行动计划',
'to_acl_M.CI.7.4'=>'许可，以消除任何行动计划',
'to_acl_M.D'=>'仪表板标签',
'to_acl_M.D.1'=>'摘要选项卡',
'to_acl_M.D.2'=>'先进的摘要选项卡',
'to_acl_M.D.3'=>'图形选项卡',
'to_acl_M.D.6'=>'系统的统计资料（图形）标签',
'to_acl_M.D.6.1'=>'统计业务范围，管理制度',
'to_acl_M.D.6.10'=>'统计系统状态（摘要风险X领域，风险x的过程中，风险x的资产，总结风险）',
'to_acl_M.D.6.11'=>'统计系统的政策管理',
'to_acl_M.D.6.12'=>'统计制度不断改进',
'to_acl_M.D.6.13'=>'统计约的行动计划和有关之间的关系的行动计划和不符合',
'to_acl_M.D.6.2'=>'统计业务领域过程管理系统',
'to_acl_M.D.6.3'=>'统计资料资产管理系统',
'to_acl_M.D.6.4'=>'简要的风险管理系统',
'to_acl_M.D.6.5'=>'简要的最佳做法',
'to_acl_M.D.6.6'=>'摘要系统控制',
'to_acl_M.D.6.7'=>'摘要异常检测系统',
'to_acl_M.D.6.8'=>'金融统计-成本，影响金融和投资',
'to_acl_M.D.6.9'=>'统计领域，过程和资产的范围和管理',
'to_acl_M.L'=>'风险管理标签',
'to_acl_M.L.1'=>'活动图书馆标签',
'to_acl_M.L.1.1'=>'菜单浏览，在子类别和事件的某一类',
'to_acl_M.L.1.2'=>'菜单修改某一类',
'to_acl_M.L.1.3'=>'菜单上删除某一类',
'to_acl_M.L.1.4'=>'菜单协理最佳做法，以某一个事件',
'to_acl_M.L.1.5'=>'菜单修改某一个事件',
'to_acl_M.L.1.6'=>'菜单上删除某一个事件',
'to_acl_M.L.1.7'=>'按钮，插入一个新的类别',
'to_acl_M.L.1.8'=>'按钮，插入一个新的事件',
'to_acl_M.L.1.9'=>'按钮联想到事件的类别，其中用户',
'to_acl_M.L.2'=>'最佳做法图书馆标签',
'to_acl_M.L.2.1'=>'菜单浏览，在分节和最佳做法的某一个节',
'to_acl_M.L.2.10'=>'允许用户创建一个文档模板的最佳做法',
'to_acl_M.L.2.2'=>'菜单编辑某些路段',
'to_acl_M.L.2.3'=>'菜单上删除某些路段',
'to_acl_M.L.2.4'=>'菜单副事件，到某一个最佳做法',
'to_acl_M.L.2.5'=>'菜单修改某一个最佳做法',
'to_acl_M.L.2.6'=>'菜单上删除某一个最佳做法',
'to_acl_M.L.2.7'=>'按钮，插入一个新节',
'to_acl_M.L.2.8'=>'按钮，插入一个新的最佳做法',
'to_acl_M.L.2.9'=>'显示最佳做法文件范本，在文件范本标签',
'to_acl_M.L.3'=>'标准图书馆标签',
'to_acl_M.L.3.1'=>'菜单编辑一定的水准',
'to_acl_M.L.3.2'=>'菜单上删除某一个标准',
'to_acl_M.L.3.3'=>'按钮，插入一个新的标准',
'to_acl_M.L.4'=>'最佳做法图书馆标签',
'to_acl_M.L.4.1'=>'菜单编辑某些路段',
'to_acl_M.L.4.2'=>'菜单编辑某些路段',
'to_acl_M.L.5'=>'文件范本标签',
'to_acl_M.L.5.1'=>'菜单编辑一个文件范本',
'to_acl_M.L.5.2'=>'菜单上删除文件范本',
'to_acl_M.L.5.3'=>'按钮，插入一个新的文件范本',
'to_acl_M.L.6'=>'各类图书馆标签',
'to_acl_M.L.6.1'=>'按钮，插入一个类别',
'to_acl_M.L.6.2'=>'菜单编辑一类',
'to_acl_M.L.6.3'=>'菜单上删除一个类别',
'to_acl_M.L.6.4'=>'按钮，插入一个解决方案',
'to_acl_M.L.6.5'=>'菜单编辑解决方案',
'to_acl_M.L.6.6'=>'菜单上删除的解决办法',
'to_acl_M.PM'=>'政策管理标签',
'to_acl_M.PM.10'=>'查看/管理文件在发展标签',
'to_acl_M.PM.10.1'=>'按钮，插入一个文件',
'to_acl_M.PM.11'=>'查看/管理的文件，批准标签',
'to_acl_M.PM.12'=>'查看/管理发表的文件标签',
'to_acl_M.PM.13'=>'查看/管理文件在修订标签',
'to_acl_M.PM.14'=>'查看/管理过时的文件标签',
'to_acl_M.PM.15'=>'查看/管理的所有文件标签',
'to_acl_M.PM.16'=>'查看/管理记录标签',
'to_acl_M.PM.17'=>'报告标签',
'to_acl_M.PM.17.1'=>'其他报告',
'to_acl_M.PM.17.1.16'=>'报告列出了性能的某一个文件',
'to_acl_M.PM.17.1.17'=>'分类报告',
'to_acl_M.PM.17.2'=>'一般报告',
'to_acl_M.PM.17.2.1'=>'文件报告',
'to_acl_M.PM.17.2.2'=>'文件区的报告',
'to_acl_M.PM.17.2.3'=>'文件由主义的组成部分报告',
'to_acl_M.PM.17.2.4'=>'改变文件的意见和修改的原因，报告',
'to_acl_M.PM.17.2.5'=>'评论用户报告',
'to_acl_M.PM.17.3'=>'记录报告',
'to_acl_M.PM.17.3.1'=>'纪录文件报告',
'to_acl_M.PM.17.4'=>'访问控制的报告',
'to_acl_M.PM.17.4.1'=>'用户访问的审计报告',
'to_acl_M.PM.17.4.2'=>'存取文件的审计报告',
'to_acl_M.PM.17.4.3'=>'文件及其批准者的报告',
'to_acl_M.PM.17.4.4'=>'文件和读者的报告',
'to_acl_M.PM.17.4.5'=>'用户相关的进程报告',
'to_acl_M.PM.17.5'=>'管理报告',
'to_acl_M.PM.17.5.1'=>'文件之前，读报告',
'to_acl_M.PM.17.5.2'=>'最存取文件的报告',
'to_acl_M.PM.17.5.3'=>'文件没有访问报告',
'to_acl_M.PM.17.5.4'=>'最文件审查报告',
'to_acl_M.PM.17.5.5'=>'等待审批的报告',
'to_acl_M.PM.17.5.6'=>'文件的建立和修订日期报告',
'to_acl_M.PM.17.5.7'=>'文件修订附表报告',
'to_acl_M.PM.17.5.8'=>'文件延迟修订报告',
'to_acl_M.PM.17.6'=>'异常报告',
'to_acl_M.PM.17.6.1'=>'组件无证件的报告',
'to_acl_M.PM.17.6.2'=>'政策，管理尚未完成的任务',
'to_acl_M.PM.17.6.3'=>'文件与高频率的修订。高频率是指文件审查多于一次，每星期平均水平。',
'to_acl_M.PM.17.6.4'=>'文件记录的报告',
'to_acl_M.PM.17.6.5'=>'报告列出了用户谁是读者的文件公布，但谁没有看过这些文件。',
'to_acl_M.PM.18'=>'可视化/管理文件即将出版',
'to_acl_M.RM'=>'风险管理标签',
'to_acl_M.RM.1'=>'区标签',
'to_acl_M.RM.1.1'=>'许可，以查看所有领域',
'to_acl_M.RM.1.2'=>'菜单浏览，在分地区的某地区',
'to_acl_M.RM.1.3'=>'菜单，以查看进程的某一地区',
'to_acl_M.RM.1.4'=>'菜单修改某一地区',
'to_acl_M.RM.1.5'=>'菜单上删除某地区',
'to_acl_M.RM.1.6'=>'按钮，插入一个新的领域',
'to_acl_M.RM.1.7'=>'按钮，插入一个新的领域中的根',
'to_acl_M.RM.1.8'=>'编辑的权限，一个地区，只要您面积负责，它',
'to_acl_M.RM.1.9'=>'许可，以消除一个地区，只要您有责任为它',
'to_acl_M.RM.2'=>'过程标签',
'to_acl_M.RM.2.1'=>'许可，以查看所有进程',
'to_acl_M.RM.2.2'=>'菜单浏览，在资产的某一个过程',
'to_acl_M.RM.2.4'=>'菜单修改某一个进程',
'to_acl_M.RM.2.5'=>'菜单上删除某一个进程',
'to_acl_M.RM.2.6'=>'按钮，插入一个新的进程',
'to_acl_M.RM.2.7'=>'编辑的权限，一个过程，只要你是负责这',
'to_acl_M.RM.2.8'=>'许可，以消除一个过程，只要你是负责这',
'to_acl_M.RM.3'=>'资产标签',
'to_acl_M.RM.3.1'=>'许可，以查看所有资产',
'to_acl_M.RM.3.10'=>'上下文菜单中的风险插入资产从事件一类的事件类别图书馆',
'to_acl_M.RM.3.2'=>'菜单浏览，在风险一定的资产',
'to_acl_M.RM.3.4'=>'菜单上建立一个风险资产的基础上，一个事件',
'to_acl_M.RM.3.5'=>'菜单修改某资产',
'to_acl_M.RM.3.6'=>'菜单上删除某些资产',
'to_acl_M.RM.3.7'=>'按钮，插入一个新的资产',
'to_acl_M.RM.3.8'=>'编辑的权限，资产，只要您有责任为它',
'to_acl_M.RM.3.9'=>'许可，以消除资产，只要您有责任为它',
'to_acl_M.RM.4'=>'风险标签',
'to_acl_M.RM.4.1'=>'许可，以查看所有的风险',
'to_acl_M.RM.4.10'=>'许可，以消除风险，只要你是负责这',
'to_acl_M.RM.4.2'=>'菜单浏览，在控制一定风险',
'to_acl_M.RM.4.4'=>'菜单编辑一定的风险',
'to_acl_M.RM.4.5'=>'菜单上删除一定的风险',
'to_acl_M.RM.4.6'=>'按钮，插入一个新的风险',
'to_acl_M.RM.4.8'=>'按钮来估计的风险',
'to_acl_M.RM.4.9'=>'编辑的权限，一种风险，只要你是负责这',
'to_acl_M.RM.5'=>'控制标签',
'to_acl_M.RM.5.1'=>'许可，以查看所有的管制',
'to_acl_M.RM.5.2'=>'菜单浏览，在相关的风险有一定的控制',
'to_acl_M.RM.5.3'=>'菜单上的风险，联想到某一个控制',
'to_acl_M.RM.5.4'=>'菜单修改某控制',
'to_acl_M.RM.5.5'=>'菜单上删除某控制',
'to_acl_M.RM.5.6'=>'按钮，插入一个新的控制',
'to_acl_M.RM.5.7'=>'编辑的权限控制，只要您有责任为它',
'to_acl_M.RM.5.8'=>'许可，以消除控制，只要您有责任为它',
'to_acl_M.RM.5.9'=>'改变在修订/测试历史的控制系统',
'to_acl_M.RM.6'=>'报告标签',
'to_acl_M.RM.6.1'=>'风险报告',
'to_acl_M.RM.6.1.1'=>'风险值报告',
'to_acl_M.RM.6.1.2'=>'风险进程报告',
'to_acl_M.RM.6.1.3'=>'风险资产的报告',
'to_acl_M.RM.6.1.4'=>'风险控制报告',
'to_acl_M.RM.6.1.5'=>'风险影响报告书',
'to_acl_M.RM.6.1.6'=>'资产的重要性的报告',
'to_acl_M.RM.6.1.7'=>'风险区的报告',
'to_acl_M.RM.6.1.8'=>'清单由事件报告',
'to_acl_M.RM.6.2'=>'控制报告',
'to_acl_M.RM.6.2.1'=>'控制％负责报告',
'to_acl_M.RM.6.2.10'=>'跟进的管制测试报告',
'to_acl_M.RM.6.2.11'=>'整合报告',
'to_acl_M.RM.6.2.12'=>'控制的修订议程，负责报告',
'to_acl_M.RM.6.2.13'=>'控制的测试议程负责报告',
'to_acl_M.RM.6.2.2'=>'控制风险评估报告',
'to_acl_M.RM.6.2.3'=>'规划控制报告',
'to_acl_M.RM.6.2.4'=>'控制implementention最新报告',
'to_acl_M.RM.6.2.5'=>'管制效率的报告',
'to_acl_M.RM.6.2.6'=>'控制的简要报告',
'to_acl_M.RM.6.2.7'=>'跟进的管制报告',
'to_acl_M.RM.6.2.8'=>'跟进的管制效率的报告',
'to_acl_M.RM.6.2.9'=>'没有计量控制报告',
'to_acl_M.RM.6.3'=>'风险管理的报告',
'to_acl_M.RM.6.3.1'=>'风险管理尚未完成的任务的报告',
'to_acl_M.RM.6.3.2'=>'地区无进程报告',
'to_acl_M.RM.6.3.3'=>'资产的无风险报告',
'to_acl_M.RM.6.3.4'=>'过程中没有资产的报告',
'to_acl_M.RM.6.3.5'=>'非估计风险报告',
'to_acl_M.RM.6.3.6'=>'控制无风险报告',
'to_acl_M.RM.6.3.7'=>'重复的风险报告',
'to_acl_M.RM.6.4'=>'财务报告',
'to_acl_M.RM.6.4.4'=>'成本控制报告',
'to_acl_M.RM.6.4.5'=>'成本控制，由负责报告',
'to_acl_M.RM.6.4.6'=>'成本控制方面的报告',
'to_acl_M.RM.6.4.7'=>'成本控制过程的报告',
'to_acl_M.RM.6.4.8'=>'成本控制，资产报告',
'to_acl_M.RM.6.5'=>'的ISO 27001的报告',
'to_acl_M.RM.6.5.1'=>'声明aplicability报告',
'to_acl_M.RM.6.5.2'=>'风险处理报告',
'to_acl_M.RM.6.5.3'=>'主义的范围宣言的报告',
'to_acl_M.RM.6.5.4'=>'主义的政策宣言的报告',
'to_acl_M.RM.6.6'=>'摘要报告',
'to_acl_M.RM.6.6.1'=>'报告：前10名-资产与更多的风险',
'to_acl_M.RM.6.6.2'=>'报告：前10名-资产与最高风险',
'to_acl_M.RM.6.6.3'=>'报告：前10名-风险区',
'to_acl_M.RM.6.6.4'=>'报告：前10名-风险进程',
'to_acl_M.RM.6.6.5'=>'数额的风险进程报告',
'to_acl_M.RM.6.6.6'=>'数额的风险区的报告',
'to_acl_M.RM.6.6.7'=>'风险状况按地区报告',
'to_acl_M.RM.6.6.8'=>'风险进程报告',
'to_acl_M.RM.6.7'=>'其他报告',
'to_acl_M.RM.6.7.1'=>'用户responsabilities报告',
'to_acl_M.RM.6.7.2'=>'未决事项的报告',
'to_acl_M.RM.6.7.3'=>'资产报告',
'to_acl_M.RM.6.7.4'=>'项目分类报告',
'to_acl_M.RM.6.7.5'=>'报告表明每个资产相依及其家属',
'to_acl_M.RM.6.7.6'=>'报告显示相关的资产',
'to_acl_M.S'=>'搜索标签',
'tr_acl_A'=>'政府当局',
'tr_acl_A.A'=>'审计',
'tr_acl_A.A.1'=>'任务',
'tr_acl_A.A.2'=>'快讯',
'tr_acl_A.A.3'=>'日志',
'tr_acl_A.C'=>'自定义',
'tr_acl_A.C.1'=>'风险估计参数',
'tr_acl_A.C.10'=>'风险参数重量',
'tr_acl_A.C.11'=>'系统分类',
'tr_acl_A.C.2'=>'风险矩阵的定义',
'tr_acl_A.C.3'=>'成本估算参数',
'tr_acl_A.C.4'=>'财政影响参数',
'tr_acl_A.C.5'=>'系统特点',
'tr_acl_A.C.6'=>'特殊用户',
'tr_acl_A.C.7'=>'类型和优先参数',
'tr_acl_A.C.7.1'=>'面积',
'tr_acl_A.C.7.2'=>'过程',
'tr_acl_A.C.7.3'=>'风险',
'tr_acl_A.C.7.4'=>'事件',
'tr_acl_A.C.7.5'=>'控制',
'tr_acl_A.C.7.6'=>'文件',
'tr_acl_A.C.7.7'=>'记录',
'tr_acl_A.C.8'=>'控制的修改和测试',
'tr_acl_A.C.9'=>'接受风险水平',
'tr_acl_A.L'=>'回收站',
'tr_acl_A.L.1'=>'恢复',
'tr_acl_A.L.2'=>'删除',
'tr_acl_A.L.3'=>'移除所有',
'tr_acl_A.MA'=>'我的帐户',
'tr_acl_A.P'=>'概况',
'tr_acl_A.P.1'=>'编辑',
'tr_acl_A.P.2'=>'删除',
'tr_acl_A.P.3'=>'插入简介',
'tr_acl_A.S'=>'系统设置',
'tr_acl_A.S.1'=>'密码策略',
'tr_acl_A.S.2'=>'删除级联',
'tr_acl_A.U'=>'用户',
'tr_acl_A.U.1'=>'编辑',
'tr_acl_A.U.2'=>'删除',
'tr_acl_A.U.3'=>'插入用户',
'tr_acl_A.U.4'=>'重设密码',
'tr_acl_A.U.5'=>'阻止用户',
'tr_acl_M'=>'用户视图',
'tr_acl_M.CI'=>'不断改善',
'tr_acl_M.CI.1'=>'发生',
'tr_acl_M.CI.1.1'=>'名单',
'tr_acl_M.CI.1.2'=>'插入发生',
'tr_acl_M.CI.1.3'=>'编辑不受任何限制',
'tr_acl_M.CI.1.4'=>'删除不受任何限制',
'tr_acl_M.CI.2'=>'事件',
'tr_acl_M.CI.2.1'=>'名单',
'tr_acl_M.CI.2.2'=>'插入事件',
'tr_acl_M.CI.2.3'=>'编辑不受任何限制',
'tr_acl_M.CI.2.4'=>'删除不受任何限制',
'tr_acl_M.CI.3'=>'不符合',
'tr_acl_M.CI.3.1'=>'名单',
'tr_acl_M.CI.3.2'=>'插入不符合',
'tr_acl_M.CI.3.3'=>'编辑不受任何限制',
'tr_acl_M.CI.3.4'=>'删除不受任何限制',
'tr_acl_M.CI.5'=>'纪律处分程序',
'tr_acl_M.CI.5.1'=>'鉴于行动',
'tr_acl_M.CI.5.2'=>'注册行动',
'tr_acl_M.CI.5.3'=>'删除',
'tr_acl_M.CI.6'=>'报告',
'tr_acl_M.CI.6.1'=>'事件报告',
'tr_acl_M.CI.6.1.1'=>'由事件发生',
'tr_acl_M.CI.6.1.2'=>'纪律处分程序',
'tr_acl_M.CI.6.1.3'=>'收集证据',
'tr_acl_M.CI.6.1.4'=>'跟进事件',
'tr_acl_M.CI.6.1.5'=>'跟进事件',
'tr_acl_M.CI.6.1.6'=>'财政影响事件',
'tr_acl_M.CI.6.1.7'=>'控制调整而导致的事故',
'tr_acl_M.CI.6.1.8'=>'项目受影响的事件',
'tr_acl_M.CI.6.1.9'=>'自动计算风险的可能性',
'tr_acl_M.CI.6.2'=>'不符合报告',
'tr_acl_M.CI.6.2.1'=>'不符合的，由过程',
'tr_acl_M.CI.6.2.2'=>'不符合的控制过程',
'tr_acl_M.CI.6.2.3'=>'跟进不符合',
'tr_acl_M.CI.6.2.4'=>'行动计划由用户',
'tr_acl_M.CI.6.2.5'=>'行动计划的过程中',
'tr_acl_M.CI.6.3'=>'不断改进报告',
'tr_acl_M.CI.6.3.1'=>'不断改进尚未完成的任务',
'tr_acl_M.CI.6.3.2'=>'事件无风险',
'tr_acl_M.CI.6.3.3'=>'不符合的，没有行动计划',
'tr_acl_M.CI.6.3.4'=>'行动计划没有文件',
'tr_acl_M.CI.6.3.5'=>'事件没有发生',
'tr_acl_M.CI.7'=>'行动计划',
'tr_acl_M.CI.7.1'=>'名单',
'tr_acl_M.CI.7.2'=>'插入',
'tr_acl_M.CI.7.3'=>'编辑不受任何限制',
'tr_acl_M.CI.7.4'=>'删除不受任何限制',
'tr_acl_M.D'=>'仪表板',
'tr_acl_M.D.1'=>'摘要',
'tr_acl_M.D.2'=>'先进的概要',
'tr_acl_M.D.3'=>'图形',
'tr_acl_M.D.6'=>'统计',
'tr_acl_M.D.6.1'=>'面积图形',
'tr_acl_M.D.6.10'=>'系统状态',
'tr_acl_M.D.6.11'=>'政策管理统计',
'tr_acl_M.D.6.12'=>'不断改进统计',
'tr_acl_M.D.6.13'=>'行动计划统计',
'tr_acl_M.D.6.2'=>'过程中的图形',
'tr_acl_M.D.6.3'=>'资产图形',
'tr_acl_M.D.6.4'=>'风险概要',
'tr_acl_M.D.6.5'=>'最佳做法摘要',
'tr_acl_M.D.6.6'=>'控制摘要',
'tr_acl_M.D.6.7'=>'异常',
'tr_acl_M.D.6.8'=>'金融统计',
'tr_acl_M.D.6.9'=>'覆盖在管理水平',
'tr_acl_M.L'=>'图书馆',
'tr_acl_M.L.1'=>'活动图书馆',
'tr_acl_M.L.1.1'=>'公开组',
'tr_acl_M.L.1.2'=>'编辑类别',
'tr_acl_M.L.1.3'=>'删除类别',
'tr_acl_M.L.1.4'=>'副的最佳做法',
'tr_acl_M.L.1.5'=>'编辑活动',
'tr_acl_M.L.1.6'=>'删除事件',
'tr_acl_M.L.1.7'=>'插入类',
'tr_acl_M.L.1.8'=>'插入事件',
'tr_acl_M.L.1.9'=>'副事件',
'tr_acl_M.L.2'=>'最佳做法图书馆',
'tr_acl_M.L.2.1'=>'公开组',
'tr_acl_M.L.2.10'=>'创建最佳实践模板',
'tr_acl_M.L.2.2'=>'编辑节',
'tr_acl_M.L.2.3'=>'删除第',
'tr_acl_M.L.2.4'=>'副事件',
'tr_acl_M.L.2.5'=>'编辑的最佳做法',
'tr_acl_M.L.2.6'=>'删除的最佳做法',
'tr_acl_M.L.2.7'=>'插入分节',
'tr_acl_M.L.2.8'=>'插入的最佳做法',
'tr_acl_M.L.2.9'=>'鉴于最佳做法文件范本',
'tr_acl_M.L.3'=>'标准图书馆',
'tr_acl_M.L.3.1'=>'编辑标准',
'tr_acl_M.L.3.2'=>'删除标准',
'tr_acl_M.L.3.3'=>'插入标准',
'tr_acl_M.L.4'=>'进口/出口',
'tr_acl_M.L.4.1'=>'出口',
'tr_acl_M.L.4.2'=>'进口',
'tr_acl_M.L.5'=>'文件范本图书馆',
'tr_acl_M.L.5.1'=>'编辑文件范本',
'tr_acl_M.L.5.2'=>'删除文件范本',
'tr_acl_M.L.5.3'=>'插入文件范本',
'tr_acl_M.L.6'=>'各类图书馆',
'tr_acl_M.L.6.1'=>'插入类',
'tr_acl_M.L.6.2'=>'编辑类别',
'tr_acl_M.L.6.3'=>'删除类别',
'tr_acl_M.L.6.4'=>'插入解决方案',
'tr_acl_M.L.6.5'=>'编辑解决方案',
'tr_acl_M.L.6.6'=>'删除的解决办法',
'tr_acl_M.PM'=>'政策管理',
'tr_acl_M.PM.10'=>'文件在发展',
'tr_acl_M.PM.10.1'=>'插入文件',
'tr_acl_M.PM.11'=>'文件在审批',
'tr_acl_M.PM.12'=>'公开文件',
'tr_acl_M.PM.13'=>'文件在修订',
'tr_acl_M.PM.14'=>'过时的文件',
'tr_acl_M.PM.15'=>'所有文件',
'tr_acl_M.PM.16'=>'记录',
'tr_acl_M.PM.17'=>'报告',
'tr_acl_M.PM.17.1'=>'其他',
'tr_acl_M.PM.17.1.16'=>'文件属性',
'tr_acl_M.PM.17.1.17'=>'分类报告',
'tr_acl_M.PM.17.2'=>'一般报告',
'tr_acl_M.PM.17.2.1'=>'文件',
'tr_acl_M.PM.17.2.2'=>'文件区',
'tr_acl_M.PM.17.2.3'=>'文件由主义的组成部分',
'tr_acl_M.PM.17.2.4'=>'文件的意见和理由',
'tr_acl_M.PM.17.2.5'=>'用户评论',
'tr_acl_M.PM.17.3'=>'记录报告',
'tr_acl_M.PM.17.3.1'=>'纪录文件',
'tr_acl_M.PM.17.4'=>'访问控制的报告',
'tr_acl_M.PM.17.4.1'=>'用户访问审计',
'tr_acl_M.PM.17.4.2'=>'查阅文件审计',
'tr_acl_M.PM.17.4.3'=>'文件及其批准者',
'tr_acl_M.PM.17.4.4'=>'文件和他们的读者',
'tr_acl_M.PM.17.4.5'=>'用户相关的进程',
'tr_acl_M.PM.17.5'=>'管理报告',
'tr_acl_M.PM.17.5.1'=>'文件之前，读',
'tr_acl_M.PM.17.5.2'=>'最存取文件',
'tr_acl_M.PM.17.5.3'=>'文件没有访问',
'tr_acl_M.PM.17.5.4'=>'最检讨文件',
'tr_acl_M.PM.17.5.5'=>'待批准',
'tr_acl_M.PM.17.5.6'=>'文件的建立和修订日期',
'tr_acl_M.PM.17.5.7'=>'文件修订附表',
'tr_acl_M.PM.17.5.8'=>'文件延迟修订',
'tr_acl_M.PM.17.6'=>'异常报告',
'tr_acl_M.PM.17.6.1'=>'组件无证件',
'tr_acl_M.PM.17.6.2'=>'尚未完成的任务',
'tr_acl_M.PM.17.6.3'=>'文件与高频率的修订',
'tr_acl_M.PM.17.6.4'=>'文件记录',
'tr_acl_M.PM.17.6.5'=>'用户等候读',
'tr_acl_M.PM.18'=>'文件公布',
'tr_acl_M.RM'=>'风险管理',
'tr_acl_M.RM.1'=>'面积',
'tr_acl_M.RM.1.1'=>'列出所有',
'tr_acl_M.RM.1.2'=>'鉴于次级领域',
'tr_acl_M.RM.1.3'=>'鉴于进程',
'tr_acl_M.RM.1.4'=>'编辑',
'tr_acl_M.RM.1.5'=>'删除',
'tr_acl_M.RM.1.6'=>'插入区',
'tr_acl_M.RM.1.7'=>'插入第一层面积',
'tr_acl_M.RM.1.8'=>'编辑如果负责',
'tr_acl_M.RM.1.9'=>'删除，如果负责',
'tr_acl_M.RM.2'=>'过程',
'tr_acl_M.RM.2.1'=>'列出所有',
'tr_acl_M.RM.2.2'=>'鉴于资产',
'tr_acl_M.RM.2.4'=>'编辑',
'tr_acl_M.RM.2.5'=>'删除',
'tr_acl_M.RM.2.6'=>'插入进程',
'tr_acl_M.RM.2.7'=>'编辑如果负责',
'tr_acl_M.RM.2.8'=>'删除，如果负责',
'tr_acl_M.RM.3'=>'资产',
'tr_acl_M.RM.3.1'=>'列出所有',
'tr_acl_M.RM.3.10'=>'负载的风险，从图书馆',
'tr_acl_M.RM.3.2'=>'鉴于风险',
'tr_acl_M.RM.3.4'=>'新的风险',
'tr_acl_M.RM.3.5'=>'编辑',
'tr_acl_M.RM.3.6'=>'删除',
'tr_acl_M.RM.3.7'=>'插入资产',
'tr_acl_M.RM.3.8'=>'编辑如果负责',
'tr_acl_M.RM.3.9'=>'删除，如果负责',
'tr_acl_M.RM.4'=>'风险',
'tr_acl_M.RM.4.1'=>'列出所有',
'tr_acl_M.RM.4.10'=>'删除，如果负责',
'tr_acl_M.RM.4.2'=>'鉴于管制',
'tr_acl_M.RM.4.4'=>'编辑',
'tr_acl_M.RM.4.5'=>'删除',
'tr_acl_M.RM.4.6'=>'插入风险',
'tr_acl_M.RM.4.8'=>'估计',
'tr_acl_M.RM.4.9'=>'编辑如果负责',
'tr_acl_M.RM.5'=>'控制',
'tr_acl_M.RM.5.1'=>'列出所有',
'tr_acl_M.RM.5.2'=>'鉴于风险',
'tr_acl_M.RM.5.3'=>'副风险',
'tr_acl_M.RM.5.4'=>'编辑',
'tr_acl_M.RM.5.5'=>'删除',
'tr_acl_M.RM.5.6'=>'插入控制',
'tr_acl_M.RM.5.7'=>'编辑如果负责',
'tr_acl_M.RM.5.8'=>'删除，如果负责',
'tr_acl_M.RM.5.9'=>'修订/测试历史',
'tr_acl_M.RM.6'=>'报告',
'tr_acl_M.RM.6.1'=>'风险',
'tr_acl_M.RM.6.1.1'=>'风险值',
'tr_acl_M.RM.6.1.2'=>'风险进程',
'tr_acl_M.RM.6.1.3'=>'风险资产',
'tr_acl_M.RM.6.1.4'=>'风险控制',
'tr_acl_M.RM.6.1.5'=>'风险影响',
'tr_acl_M.RM.6.1.6'=>'资产的重要性',
'tr_acl_M.RM.6.1.7'=>'风险区',
'tr_acl_M.RM.6.1.8'=>'核对事件',
'tr_acl_M.RM.6.2'=>'管制',
'tr_acl_M.RM.6.2.1'=>'管制由负责',
'tr_acl_M.RM.6.2.10'=>'跟进的管制测试',
'tr_acl_M.RM.6.2.11'=>'符合',
'tr_acl_M.RM.6.2.12'=>'控件的负责修订议程',
'tr_acl_M.RM.6.2.13'=>'控制的测试议程负责',
'tr_acl_M.RM.6.2.2'=>'控制风险',
'tr_acl_M.RM.6.2.3'=>'规划管制',
'tr_acl_M.RM.6.2.4'=>'管制的实施日期',
'tr_acl_M.RM.6.2.5'=>'管制效率',
'tr_acl_M.RM.6.2.6'=>'控制摘要',
'tr_acl_M.RM.6.2.7'=>'跟进的管制',
'tr_acl_M.RM.6.2.8'=>'跟进的管制效率',
'tr_acl_M.RM.6.2.9'=>'没有计量控制',
'tr_acl_M.RM.6.3'=>'风险管理',
'tr_acl_M.RM.6.3.1'=>'尚未完成的任务',
'tr_acl_M.RM.6.3.2'=>'地区无进程',
'tr_acl_M.RM.6.3.3'=>'资产的无风险',
'tr_acl_M.RM.6.3.4'=>'过程中没有资产',
'tr_acl_M.RM.6.3.5'=>'非估计风险',
'tr_acl_M.RM.6.3.6'=>'控制无风险',
'tr_acl_M.RM.6.3.7'=>'重复的风险',
'tr_acl_M.RM.6.4'=>'金融',
'tr_acl_M.RM.6.4.4'=>'成本控制',
'tr_acl_M.RM.6.4.5'=>'成本控制负责',
'tr_acl_M.RM.6.4.6'=>'成本控制区',
'tr_acl_M.RM.6.4.7'=>'成本控制的过程',
'tr_acl_M.RM.6.4.8'=>'成本控制的资产',
'tr_acl_M.RM.6.5'=>'国际标准化组织27001',
'tr_acl_M.RM.6.5.1'=>'声明aplicability',
'tr_acl_M.RM.6.5.2'=>'风险处理计划',
'tr_acl_M.RM.6.5.3'=>'主义的范围宣言',
'tr_acl_M.RM.6.5.4'=>'主义的政策宣言',
'tr_acl_M.RM.6.6'=>'摘要',
'tr_acl_M.RM.6.6.1'=>'前10名-资产与更多的风险',
'tr_acl_M.RM.6.6.2'=>'前10名-资产与最高风险',
'tr_acl_M.RM.6.6.3'=>'前10名-风险区',
'tr_acl_M.RM.6.6.4'=>'前10名-风险进程',
'tr_acl_M.RM.6.6.5'=>'数额的风险进程',
'tr_acl_M.RM.6.6.6'=>'数额的风险区',
'tr_acl_M.RM.6.6.7'=>'风险状况，每个地区',
'tr_acl_M.RM.6.6.8'=>'风险地位的过程中',
'tr_acl_M.RM.6.7'=>'其他',
'tr_acl_M.RM.6.7.1'=>'用户responsabilities',
'tr_acl_M.RM.6.7.2'=>'未决事项',
'tr_acl_M.RM.6.7.3'=>'资产',
'tr_acl_M.RM.6.7.4'=>'项目分类',
'tr_acl_M.RM.6.7.5'=>'资产-依赖性和家属',
'tr_acl_M.RM.6.7.6'=>'资产相关性',
'tr_acl_M.S'=>'搜索',

/* './packages/admin/cfg_general.xml' */

'lb_session_expiracy'=>'会议到期时间：',
'st_minutes'=>'分钟。',
'to_session_expiracy'=>'会议的<b>到期时间： </b> <br/> <br/>的定义有多少闲置时间系统的等待，会议之前登录的用户已过期。',
'vb_deactivate'=>'停用',

/* './packages/admin/custom_asset_values_name.xml' */

'st_asset_parametrization_cl_bl'=>'的<b>资产的重要性，估计标签： < / b >',
'st_importance'=>'的重要性（资产）',
'tt_asset_values_name_information'=>'你怎么想的标签您的资产的重要性，估计参数？',

/* './packages/admin/custom_control.xml' */

'cb_revision'=>'修订',
'cb_test'=>'测试',
'st_revision_and_test'=>'<b>控制的修改和测试< / b >',
'st_revision_and_test_description'=>'你想不想检讨您的安全管制？',
'to_control_revision_config_bl_cl'=>'<b>控制修改配置： < / b > <br/> <br/>决定，如果该系统将允许修改的真正效率的管制。 <br/>此配置将可让您评估是否是有效的控制或不。',
'to_control_test_config_bl_cl'=>'的<b>测试，控制组态： < / b > <br/> <br/>决定，如果该系统将允许试验的管制。 <br/>此配置将可让您评估是否控制是可靠的或没有。',

/* './packages/admin/custom_control_cost_name.xml' */

'lb_cost_name_1_bl'=>'<b>费用名称1 < / b >',
'lb_cost_name_2_bl'=>'<b>成本姓名2 < / b >',
'lb_cost_name_3_bl'=>'<b>费用名称3 < / b >',
'lb_cost_name_4_bl'=>'<b>费用名称4 < / b >',
'lb_cost_name_5_bl'=>'<b>成本的名字5 < / b >',
'st_costs_estimation_parameters_information'=>'究竟是什么类别的费用，通常您指望在应用程序的管制？',
'static_st_cost_estimation_parameters_bl'=>'<b>费用估算参数< / b >',

/* './packages/admin/custom_currency.xml' */


/* './packages/admin/custom_financial_impact.xml' */


/* './packages/admin/custom_financial_impact_parameters.xml' */

'st_custom_financial_impact_parameter_note'=>'输入姓名的类别上，将评估的财务影响的事件。',
'st_description_financial_impact_parameter'=>'axur主义使您能够界定的来源可能的财政损失，相关事件的风险。什么是分类为财政来源您想要使用的呢？',
'st_financial_impact_parameters'=>'的<b>的财务影响参数< / b >',

/* './packages/admin/custom_impact_damage.xml' */


/* './packages/admin/custom_impact_type.xml' */


/* './packages/admin/custom_priority.xml' */


/* './packages/admin/custom_quantity_rm_parameters.xml' */

'lb_mode_cl'=>'模式：',
'rb_3_three'=>'3 （ 3 ）',
'rb_5_five'=>'5 （ 5 ）',
'rb_optimist'=>'乐观主义者',
'rb_pessimist'=>'悲观',
'st_custom_quantity_parameters_note'=>'开放式保税仓系统。 ：您可以更改设置，在未来的日子，但将有必要revaluate一些风险，价值观。',
'st_risk_quantity_values_bl_cl'=>'的<b>风险矩阵的定义是： < / b >',
'tt_custom_quantity_rm_parameters_information'=>'最佳做法建议您给予的重要性，影响和可能性，在一个3 × 3矩阵。 <br/>一些组织选择使用5 × 5矩阵。 <br/>这是一个决定，你必须考虑出发之前的风险管理。 <br/>使用的3 × 3矩阵，风险',

/* './packages/admin/custom_report_classification.xml' */

'lb_to_cl'=>'到：',
'st_report_classification'=>'<b>报告分类< / b >',
'st_report_classification_information'=>'什么是分类（和向谁这种分类适用）您想要使用的系统标签？',
'st_system_classification_bl'=>'<b>系统分类</b>',
'st_system_classification_information'=>'什么样的分类（和向谁这种分类适用）你想要使用的系统？',
'to_general_settings_classification'=>'的<b>分类： < / b > <br/>表明，在页脚的系统，其分类。例如：机密，秘密等。',
'to_general_settings_classification_dest'=>'<b>要： < / b > <br/>表明，人的分类上述适用。这一领域将采取的效果，只有当分类已被填补。',

/* './packages/admin/custom_risk_control_values_name.xml' */

'st_rc_impact'=>'影响',
'st_rc_impact5_'=>'影响',
'st_rc_probability'=>'的可能性',
'st_risk_control_values_name'=>'<b>控制标准，减少标签< / b >',
'st_very_high'=>'的<b>非常高的< / b >',
'st_very_low'=>'的<b>非常低， < / b >',
'tt_risk_control_value_name_information'=>'当您申请管制，以降低风险，减少他们的可能性，或影响相关的这些风险。你怎么想的标签在这些削减的影响和可能性呢？',

/* './packages/admin/custom_risk_limits.xml' */

'lb_risk_limits_high_bl_cl'=>'的<b>风险承受能力2 ： < / b >',
'lb_risk_limits_low_bl_cl'=>'的<b>风险承受能力1 ： < / b >',
'st_risk_limits_definition'=>'界定值为风险的处理。 <br/> <br/>风险，其计算值是低于容忍的第1级将突出绿色的颜色。 <br/>风险，其计算值高于公差第2级将突出与红颜色。 <br/>风险',
'tt_aceptable_risk_limits'=>'<B>可接受的风险程度< / b >',
'tt_risk_limits'=>'风险容忍度',
'wn_high_higher_than_max'=>'限值的风险2不能超过系统配置的限制。',
'wn_low_higher_than_high'=>'限值的风险一不能大于限值的风险2 。',
'wn_low_lower_than_1'=>'限值的风险一不能低于1 。',
'wn_save_risk_matrix_update_risk_limits'=>'该系统所产生的一项任务，以用户的<b> ％ chairman_name ％ < / b >为批准新的风险限额。直到此用户批准新的限制，该系统将使用旧的风险限额。',

/* './packages/admin/custom_risk_parameters_names.xml' */

'st_risk_parameters_bl_cl'=>'的<b>风险估计参数： < / b >',
'st_risk_parameters_help'=>'输入的名称属性将成为可在软件的风险评估。我们建议下列内容：保密性，完整性和可用性;但其他属性，如真实性，责任，非repudiati',
'tt_risk_parameters_information'=>'你怎么标签，您的风险估计参数？',
'wn_risk_parameters_minimum_requirement'=>'至少有一个风险参数必须指定。',

/* './packages/admin/custom_risk_parameters_weight.xml' */

'st_risk_parameters_weight'=>'的<b>风险参数重量< / b >',
'st_risk_parameters_weight_definition'=>'确定的权数，每个风险参数。度量衡必须大于或等于零的，至少有一个重量必须是正面的。价值风险和资产将被更新根据的变化，在目前的权重。',
'wn_at_least_one_positive'=>'至少有一个重量必须大于零。',

/* './packages/admin/custom_risk_values_name.xml' */

'st_custom_risk_matrix_definition_generic_note'=>'开放式保税仓系统。 ：一是选择风险矩阵大小（ 3x3或5x5 ）在主题为“风险矩阵的定义” 。',
'st_high'=>'<b>高< / b >',
'st_high_bl'=>'<b>高< / b >',
'st_impact'=>'影响（风险）',
'st_impact_risk'=>'影响（风险）',
'st_low'=>'低的<b> < / b >',
'st_low_bl'=>'低的<b> < / b >',
'st_medium'=>'<b>媒介< / b >',
'st_medium_bl'=>'<b>媒介< / b >',
'st_probability_risk'=>'的可能性（风险）',
'st_risk_parametrization'=>'的<b>影响＆似然估计标签： < / b >',
'st_riskprob'=>'的可能性（风险）',
'st_values'=>'价值观',
'st_very_high_bl'=>'的<b>非常高的< / b >',
'st_very_low_bl'=>'的<b>非常低， < / b >',
'tt_risk_values_name_information'=>'你怎么想的标签您的影响和似然估计的准则？',

/* './packages/admin/custom_special_users.xml' */

'lb_asset_manager_bl'=>'资产管理公司的<b> < / b >',
'lb_chairman_bl'=>'的<b>管理< / b >',
'lb_control_manager_bl'=>'<b>控制经理< / b >',
'lb_library_manager_bl'=>'<b>图书馆经理< / b >',
'lb_user_disciplinary_process_manager_bl'=>'的<b>纪律处分程序经理< / b >',
'lb_user_evidence_manager_bl'=>'的<b>证据经理< / b >',
'lb_user_incident_manager_bl'=>'事件经理的<b> < / b >',
'lb_user_non_conformity_manager_bl'=>'经理的<b>不符合< / b >',
'st_custom_special_users_information'=>'该axur主义的工作流程需要定义一些用户负责特殊任务预先确定的，在一个主义。谁就会去占领的作用，这些用户在您的系统呢？',
'st_help'=>'[ ？ ]',
'st_special_users_bl'=>'的<b> axur主义的特殊用户< / b >',
'tt_special_user'=>'的<b>管理： < / b > <br/> <br/>用户负责批准第一个业务领域，接受风险，及其他方面的需要管理部门的批准。',
'tt_special_user_asset_manager'=>'资产管理公司的<b> ： < / b > <br/> <br/>用户负责控制的资产盘存',
'tt_special_user_control_manager'=>'<b>控制经理： < / b > <br/> <br/>用户负责管理的安全管制，批准建立新的管制措施，并组织控制的主义。',
'tt_special_user_disciplinary_process_manager'=>'的<b>纪律处分程序经理： < / b > <br/> <br/>用户谁收到的纪律处分程序快讯',
'tt_special_user_evidence_manager'=>'的<b>证据经理： < / b > <br/> <br/>用户谁收到的收集证据的警示',
'tt_special_user_incident_manager'=>'事件的<b>经理： </b> <br/> <br/>用户负责审批，编辑和删除发生，以及为创建，编辑和删除事件<br/> <br/>只有这样，用户将能够创造出纪律程序，以及作为准的风险或.',
'tt_special_user_library_manager'=>'<b>图书馆经理： < / b > <br/> <br/>用户负责管制主义图书馆，通过验证的新事件，新的最佳做法和安全标准',
'tt_special_user_nc_manager'=>'不符合的<b>经理： < / b > <br/> <br/>用户负责批准不符合所造成的其他验证用户。',

/* './packages/admin/custom_system_features.xml' */

'cb_auto_document_creation'=>'生成的文件后，就会自动建立系统中元素的呢？',
'cb_cost'=>'使用成本估计的功能？',
'cb_document_manual_versioning'=>'你要使用的文件，手册版本？',
'cb_percentual_risk'=>'显示的风险值在0到100的规模？',
'cb_solicitor'=>'允许检察分享获得的权利，特权和责任呢？',
'st_data_collecting'=>'使数据收集与每日周期产生axur主义的统计数字。',
'st_manual_data_control'=>'你是否想使用手动控制的日期？',
'st_risk_formula'=>'使用比例风险的公式。',
'st_system_features'=>'<b>系统“功能< / b >',
'to_data_collecting_settings'=>'<b>数据采集设置： < / b > <br/> <br/>套是否该系统将收集的数据库信息生成系统的历史显示，对统计信息标签。',
'to_delete_cascade'=>'<b>手动控制系统日期： < / b > <br/> <br/>允许用户定义的日期，在过去和/或在未来的部分系统，其中，默认情况下，这种灵活性是不能容许的。',
'to_document_manual_versioning'=>'文件的<b>手册版本： < / b > <br/> <br/>确定是否系统将使用手动或自动版本的文件。',
'to_general_settings_automatic_documents_creation'=>'<b>自动创建的文件设置： < / b > <br/> <br/>套是否系统会自动创建一个文档，为每个项目（面积，过程中，资产及管制）所产生的用户。',
'to_percentual_risk_configuration'=>'风险值的<b>规模配置： < / b > <br/> <br/>决定，如果风险值应表现在绝对值（ 0到9或0至25日） ，或在规模，从0到100 。',
'to_procurator_configuration'=>'的<b>检察配置： < / b > <br/> <br/>确定，如果该系统将允许采购的机制，暂时缺席，一个用户。',
'to_risk_formula'=>'<b>风险比例公式:</b><br/><br/>公式，认为权重的重要性和后果，以增加最终价值为每个风险。',
'to_system_cost_configuration'=>'<b>成本配置： < / b > <br/> <br/>确定，如果该系统将使成本的计算方法。',

/* './packages/admin/custom_type_and_priority_parametrization.xml' */

'si_document'=>'文件',
'si_event'=>'事件',
'si_register'=>'记录',
'si_risk'=>'风险',
'st_description_type_and_priority_parametrization'=>'你怎么标签您的元素？',
'st_element'=>'的<b>元素： < / b >',
'st_priority_bl'=>'的<b>优先< / b >',
'st_type_and_priority_parametrization'=>'<b>类型和优先参数< / b >',
'st_type_bl'=>'<b>类型< / b >',
'wn_classification_minimum_requirement'=>'至少有三个分类，必须指定为每个项目。',

/* './packages/admin/nav_audit_alert.xml' */

'gc_justification'=>'原因',
'tt_audit_alerts_bl'=>'审计的<b>快讯< / b >',

/* './packages/admin/nav_audit_log.xml' */

'lb_user_cl'=>'用户名：',
'si_all_actions'=>'一切行动',
'si_all_users'=>'所有用户',
'tt_audit_log_bl'=>'的<b>审核日志< / b >',

/* './packages/admin/nav_audit_task.xml' */

'gc_accomplishment'=>'成绩',
'gc_creation'=>'创作',
'gc_receiver'=>'接收机',
'lb_activity_cl'=>'活动：',
'lb_creator_cl'=>'作者：',
'lb_receiver_cl'=>'接收机：',
'si_all_activities'=>'所有活动',
'si_all_creators'=>'所有创作者',
'si_all_receivers'=>'所有接收器',
'tt_audit_task_bl'=>'的<b>审计工作< / b >',

/* './packages/admin/nav_config.php' */

'st_check_for_correct_data'=>'检查，如果数据填写正确。',
'st_impact_parameter_remove_message'=>'取消金融的影响参数将导致取消此参数在事件已经估计。你是否想仍要继续进行？',
'st_info_saved_but'=>'该信息已保存。但是，',
'st_not_possible_to_update_data_collection_info'=>'它是不可能的，以更新的数据收集信息',
'st_not_possible_to_update_mail_server_info'=>'它是不可能的，以更新的电子邮件伺服器的资料',
'st_unavailable_information'=>'不可用的信息。',
'tt_alert'=>'提高警觉',
'tt_deactivate_system'=>'关闭系统',
'tt_impact_parameter_remove'=>'删除的财务影响参数',

/* './packages/admin/nav_config.xml' */

'si_costs_names'=>'费用名称',
'si_parametrization'=>'估计',
'si_risk_parameters_values'=>'风险参数值',
'si_syslog'=>'系统记录',
'si_system'=>'系统',

/* './packages/admin/nav_customization.php' */

'st_change_amount_of_parameters_confirm'=>'改变数额的参数，使系统成为没有估计。 <br>的<b>您是否确实这样做？ < / b >',
'st_change_amount_of_risks_confirm'=>'改变数额的风险，将导致系统重做其所有风险计算！ <br>的<b>您是否确实这样做？ < / b >',
'st_remove_classification_error'=>'您的更改已保存。不过，这是不可能删除以下分类，因为他们正在使用的一个或多个项目： not_deletable的<b> ％ ％ < / b > 。',
'st_risk_parameters_weight_edit_confirm'=>'你正在发生变化明智的数据。当您更改参数的重量，价值，所有的风险和资产的该系统将相应改变。你是否想以确认您的变更？',
'tt_remove_classification_error'=>'时发生错误，消除分类',
'tt_save_parameters_names'=>'保存参数的名称',
'tt_save_parameters_values'=>'保存参数值',
'tt_sensitive_data'=>'敏感数据',

/* './packages/admin/nav_customization.xml' */

'lb_settings_cl'=>'定制：',
'si_aceptable_risk_limits'=>'可接受的风险程度',
'si_control_cost_names'=>'成本估算参数',
'si_control_revision_and_test'=>'控制的修改和测试',
'si_financial_impact_parameters'=>'财政影响参数',
'si_impact_and_importance_dimensions'=>'风险估计参数',
'si_parametrization_asset'=>'资产的重要性，估计标签',
'si_parametrization_control'=>'控制减少标准标签',
'si_parametrization_risk'=>'影响＆似然估计标签',
'si_priority_and_type_parametrization'=>'类型和优先参数',
'si_report_classification'=>'报告分类',
'si_risk_parameters_weight'=>'风险参数重量',
'si_special_users'=>'特殊用户',
'si_system_classification'=>'系统分类',
'si_system_features'=>'系统特点',
'si_system_rm_parameters_quantity'=>'风险矩阵的定义',
'wn_updating_risk_values'=>'变化成功地保存。等待的风险价值正在重新计算。',
'wn_values_successfully_changed'=>'价值观成功更新。',

/* './packages/admin/nav_my_account.php' */

'st_activation_date'=>'启用日期',
'st_cancel_service'=>'取消服务',
'st_cancel_service_email_message'=>'1取消服务的要求，已取得了客户端\' ％的客户％ \' 。',
'st_client_name'=>'客户的姓名',
'st_confirm_cancel_service_message'=>'您确定要取消服务？',
'st_deactivate_account_text'=>'要停用您的帐户，点击下面的按钮。 <br/>您的数据库将保持为30天。之后，所有资料都会被完全删除。能获得这个数据库，请与我们联络的电子邮件％ ％',
'st_expiracy_date'=>'届满日期',
'st_expiracy_date_never'=>'从不',
'st_license_type'=>'授权类型',
'st_max_simult_users'=>'最高。同时用户',
'st_number_of_users'=>'用户数目',
'tt_cancel_service'=>'取消服务',
'tt_confirm_cancel_service'=>'取消服务',
'tt_deactivate_account'=>'停用的帐户',

/* './packages/admin/nav_my_account.xml' */

'lb_email_cl'=>'电子邮箱：',
'st_cancel_service_bl'=>'<b>服务取消< / b >',
'st_change_email_bl'=>'的<b>行政电子邮件变更< / b >',
'st_deactivate_account_bl'=>'的<b>停用帐户“ < / b >',
'st_license_info_bl'=>'<b>许可信息： < / b >',
'st_upgrade_license_bl'=>'<b>许可升级< / b >',
'st_upgrade_license_message'=>'请激活您的用户包使用激活代码都是通过电子邮件发送。把您的激活代码下面，并按下“激活” 。这将提供即时！',
'st_upgrade_license_text'=>'请激活您的用户包（ 10用户）使用激活代码，这是通过电子邮件发送。把您的激活下面的代码，并按下“激活” 。这将提供即时！',
'vb_activate'=>'激活',
'vb_cancel_service'=>'取消服务',
'vb_change'=>'更改',
'vb_deactivate_account'=>'停用我的帐户',
'vb_upgrade'=>'升级',
'wn_email_changed'=>'电子邮件已成功更改',
'wn_invalid_activation_code'=>'一些守则是无效的。请联系，电子邮件％ ％ 。',
'wn_successful_activation'=>'授权成功更新。',

/* './packages/admin/nav_profile.php' */

'st_profile_remove_message'=>'您是否确实要删除的个人资料的<b> ％ profile_name ％ < / b > ？',
'tt_remove_profile'=>'删除个人资料',

/* './packages/admin/nav_profile.xml' */

'mi_visualize'=>'查看',
'tt_profiles_bl'=>'的<b>概况< / b >',

/* './packages/admin/nav_system_configuration_on_site.xml' */

'lb_system_settings_cl'=>'系统设置',
'si_delete_cascade'=>'删除级联',
'si_password_policy'=>'密码策略',
'wn_changes_successfully_saved'=>'的变化，成功保存。',

/* './packages/admin/nav_trash.php' */

'st_all_items_remove_confirm'=>'所有项目在回收站将被永久删除！你是否打算继续？',
'st_items_restore_confirm'=>'你是否想恢复选定的项目呢？',
'st_not_excluded_users_and_profile_message'=>'选定的用户可不会被删除，因为他们是相关的存在itens在回收站。 <br/>到选择的配置文件不能被删除，因为他们是相关存在的用户在回收站被认为是不选定，或无法物权',
'st_not_removed_profiles_message'=>'选择的配置文件不能被删除，因为他们是相关存在的用户在回收站被认为是不选定的或可能不会被删除。',
'st_not_restored_documents_message'=>'选定的文件不能恢复，因为他们是与一个或多个项目仍然删除！',
'st_not_restored_items_message'=>'选定的项目不能恢复，因为他们都与一个或多个用户仍然删除！',
'st_not_restored_users_message'=>'选定的用户可不会被删除，因为他们是与现有itens在回收站。',
'st_not_selected_items_remove_confirm'=>'项目不被选中，并分项或相关选定的项目将被删除。你是否打算继续？',
'tt_all_items_permanent_removal'=>'永久删除所有项目',
'tt_document_not_restored'=>'文件非恢复',
'tt_items_not_restored'=>'项目无法恢复',
'tt_items_permanent_removal'=>'永久删除项目',
'tt_items_restoral'=>'恢复项目',
'tt_profile_not_deleted'=>'配置文件不删除',
'tt_users_and_profile_not_deleted'=>'用户和配置文件不删除',
'tt_users_not_deleted'=>'用户不拆除',

/* './packages/admin/nav_trash.xml' */

'tt_bin_bl'=>'的<b>回收站< / b >',
'vb_remove_all'=>'移除所有',
'wn_elements_removed_successfully'=>'（某些）项目成功移除。',
'wn_elements_restaured_successfully'=>'（某些）项目成功还原。',

/* './packages/admin/nav_users.php' */

'em_alerts_and_tasks_transfer'=>'转移的任务和警报',
'st_choose_user_message'=>'在下一屏幕上，选择一个用户会收到谁的任务和之前的警告，此用户。',
'st_reset_all_users_passwords_confirm'=>'您是否确实要重设密码从<b>所有用户< / b >和发送新密码，以他们的电子邮件？',
'st_user_remove_confirm'=>'您是否确定删除用户的<b> ％的用户名％ < / b > ？',
'tt_choose_user_warning'=>'警告用户选择',
'tt_remove_user'=>'删除用户',
'tt_reset_all_users_password'=>'重设密码的所有用户',

/* './packages/admin/nav_users.xml' */

'gc_email'=>'电子邮件',
'gc_profile'=>'外形',
'mi_block'=>'座',
'mi_unblock'=>'解除封锁',
'tt_users_bl'=>'<b>用户< / b >',
'vb_reset_passwords'=>'重设密码',

/* './packages/admin/popup_associate_elements.xml' */

'lb_associate_to_cl'=>'联想到：',
'tt_items_association'=>'项目协会',
'vb_cancel_all'=>'取消所有',

/* './packages/admin/popup_configure_authentication.php' */

'st_connection_failed'=>'连接失败。',
'st_database_structure_wrong'=>'数据库中没有预期的结构。',
'st_successful_connection'=>'连接成功。',

/* './packages/admin/popup_configure_authentication.xml' */

'lb_databse_bl_cl'=>'<b>资料库： < / b >',
'lb_host_bl_cl'=>'<b>主机： < / b >',
'lb_password_bl_cl'=>'<b>密码： < / b >',
'st_postgres_authentication_data_needed'=>'数据以下是需要让系统存取资料库的资料，载有关于postgres附表。',
'tt_setup_authentication'=>'安装验证',
'vb_test'=>'测试',

/* './packages/admin/popup_deactivate.php' */

'em_deactivate_account'=>'帐户的失活',
'em_deactivate_account_email_message'=>'客户端\' ％的客户％ \'已要求帐户的失活。 <br>的<b>的原因是： < / b > <br> ％ deactivation_reason ％',

/* './packages/admin/popup_deactivate.xml' */

'lb_deactivation_reason_cl'=>'请让我们知道你为什么失能：',
'st_confirm_deactivate_account_message'=>'您确定要关闭您的帐户吗？',
'tt_confirm_deactivate_account'=>'停用的帐户',

/* './packages/admin/popup_no_access.xml' */

'st_user_wo_system_access'=>'您没有进入该系统。新闻<b>确定< / b >或关闭该窗口以注销。',
'tt_user_wo_system_access'=>'用户未经允许进入系统',

/* './packages/admin/popup_profile_edit.php' */

'tt_edit_profile'=>'编辑个人资料',
'tt_insert_profile'=>'添加配置文件',
'tt_visualizae_profile'=>'显示个人资料',

/* './packages/admin/popup_profile_edit.xml' */

'st_profile_warning'=>'*此配置文件无法修改。',

/* './packages/admin/popup_restore_name_conflict.xml' */

'st_name_conflicts_message'=>'它的发现有一个或多个选定的项目提出了名称冲突，意思是有积极的项目，该系统具有相同的类型和名称。哪些项目你是否想还原吗？',
'tt_name_conflicts_found'=>'名称冲突发现',

/* './packages/admin/popup_restore_structure_and_name_conflict.xml' */

'rb_all_except_name_conflict'=>'除那些名称冲突*',
'rb_all_except_structure_conflict'=>'除那些与结构性矛盾',
'st_structure_name_conflicts_message'=>'它已发现有冲突的结构和名称与一个或多个选定项目。结构发生冲突，当有未选中的项目要还原选定的项目。一个名称发生冲突时，是有积极',
'tt_structure_and_name_conflicts_found'=>'结构和名称冲突发现',
'wn_not_restoring_name_conflict'=>'*没有恢复的项目名称冲突可能导致的结构性冲突。',

/* './packages/admin/popup_restore_structure_conflict.xml' */

'cb_associate_structure_conflicted_items_individually'=>'个别准项目与结构性冲突，以现有项目在系统中。',
'rb_all'=>'全部',
'rb_only_not_conflicting'=>'只有那些没有目前的冲突',
'st_structure_conflicts_message'=>'它的发现有一个或多个选定的项目提出了结构冲突，即以恢复他们，你将不得不恢复结构未选中的项目。哪些项目你是否想还原吗？',
'tt_structure_conflicts_found'=>'结构冲突发现',
'vb_restore'=>'恢复',

/* './packages/admin/popup_upgrade_license.xml' */

'lb_activation_code_cl'=>'激活代码：',
'tt_upgrade_license'=>'授权升级',

/* './packages/admin/popup_user_edit.php' */

'em_new_password'=>'新密码',
'em_new_user'=>'欢迎axur主义',
'em_new_user_footer_bl'=>'<b>对于进一步资料，请联络的警卫人员。 < / b >',
'st_reset_user_password_confirm'=>'您是否确定要重置用户密码和发送新密码，以他的电子邮件？',
'tt_profile_edit_error'=>'错误而改变形象',
'tt_reset_user_password'=>'重置用户密码',
'tt_user_adding'=>'用户加入',
'tt_user_editing'=>'用户编辑',
'tt_user_insert_error'=>'用户达到限额',
'wn_users_limit_reached'=>'数量授权的用户已经达到。从现在起，只有用户的<b>文件阅读器“ / b ”个人资料（用户，只有有权限读取文件）都可以使用。按一下的 <a target="_blank" href=%link%>这里</a> 以便购买更多的许可证。',

/* './packages/admin/popup_user_edit.xml' */

'cb_change_password_on_login'=>'更改密码在下次登录',
'lb_confirmation_cl'=>'确认：',
'lb_email_bl_cl'=>'<b>电子邮件： < / b >',
'lb_language_bl_cl'=>'<b>语言： < / b >',
'lb_login_bl_cl'=>'<b>登录： < / b >',
'lb_new_password_cl'=>'新密码：',
'lb_profile_bl_cl'=>'<b>配置文件： < / b >',
'st_language_bl_cl'=>'<b>语言： < / b >',
'vb_reset_password'=>'重设密码',
'wn_different_passwords'=>'不同的密码',
'wn_existing_email'=>'电子邮件已经存在',
'wn_existing_login'=>'登录已经存在',
'wn_invalid_email'=>'无效电子邮件',
'wn_password_changed'=>'密码已变更',
'wn_weak_password'=>'您的密码不履行要求的密码政策。请，请选择另一个1 。',
'wn_wrong_current_password'=>'密码错误',

/* './packages/admin/report/popup_report_audit_alert.php' */

'rs_alerts_audit'=>'快讯审计',

/* './packages/admin/report/popup_report_audit_log.php' */

'rs_systems_log_audit'=>'系统日志审计',

/* './packages/admin/report/popup_report_audit_task.php' */

'rs_tasks_audit'=>'审计任务',

/* './packages/admin/report/popup_report_audit_task.xml' */

'rs_accomplishment'=>'成绩',
'rs_creation'=>'创作',
'rs_creator'=>'创作者',
'rs_receiver'=>'接收机',

/* './packages/admin/system_configuration_delete_cascade.xml' */

'cb_delete_cascade'=>'删除级联',
'st_delete_cascade'=>'<b>删除级联</b>',
'st_system_configuration_delete_cascade_information'=>'决定如果某个项目的搬迁将导致自动分项目罢免权。 <br/>如果\'删除级联\'是残疾人士，将不可能删除项目已分项目。 <br/> <br/>您要使用\'删除级联\'在该系统呢？',

/* './packages/admin/system_configuration_email_setup.xml' */

'si_email_ssl2'=>'SSLv2',

/* './packages/admin/system_configuration_password_policy.xml' */

'cb_case_characters'=>'上/小写字符',
'cb_numeric_characters_cl'=>'数字字符：',
'cb_special_characters_cl'=>'特殊字符：',
'lb_days_before_warning'=>'通知密码到期：',
'lb_label_block_limit_cl'=>'座限制：',
'lb_minimum_characters_cl'=>'最低长度：',
'lb_password_history_apply_cl'=>'申请密码的历史：',
'lb_password_max_lifetime_cl'=>'密码最高寿命：',
'st_configuration_password_policy_information'=>'什么是密码政策，您想要使用的系统？',
'st_days_before'=>'前一天。',
'st_default_configuration'=>'重置为默认配置',
'st_invalid_logon'=>'无效登录尝试',
'st_memorized_passwords'=>'背密码。',
'st_password_policy_bl_cl'=>'<b>密码政策： < / b >',
'wn_min_char_warning'=>'最低长度必须大于或等于人数特别caracters ，加2 （至少在较低的情况下的信和一个大写字母） ，再加上人数的数字caracters 。 <br/> <br/>开放式保税仓系统：只选定的选项也将计入。',

/* './packages/admin/system_configuration_server_setup.xml' */


/* './packages/admin/system_configuration_timezone_setup.xml' */

'si_timezone_-11'=>'GMT -11',

/* './packages/admin/tab_audit.xml' */

'ti_alerts'=>'快讯',
'ti_log'=>'日志',
'ti_tasks'=>'任务',

/* './packages/admin/tab_main.php' */

'st_survey_link'=>'http://www.surveymonkey.com/s.aspx?sm=F9BFTdst8Ginn7PdgNGxFA_3d_3d',
'to_ip_cl'=>'叶：',
'to_last_login_cl'=>'上次登录：',

/* './packages/admin/tab_main.xml' */

'lb_feedback'=>'反馈',
'mx_bug'=>'千年虫',
'mx_content_issue'=>'建议',
'mx_feature_request'=>'改善',
'st_about_isms_bl'=>'<b>关于< / b >',
'st_buy_now_bl'=>'<b>购买“ < / b >',
'st_pipe_bl'=>'<b>|</b>',
'st_preferences_bl'=>'<b>偏好设定< / b >',
'st_satisfaction_survey_bl'=>'<b>使用者经验调查< / b >',
'st_send_us_feedback_cl'=>'留下的反馈意见：',
'st_software_license_bl'=>'<b>许可< / b >',
'ti_audit'=>'审计',
'ti_bin'=>'回收站',
'ti_customization'=>'定制',
'ti_my_account'=>'我的帐户',
'ti_profiles'=>'概况',
'ti_system_settings'=>'系统设置',
'ti_users'=>'用户',
'vb_exit'=>'注销',
'vb_feedback'=>'反馈',
'vb_help'=>'帮助',
'vb_user_view'=>'用户视图',

/* './packages/continuity/nav_asset.xml' */


/* './packages/continuity/nav_place_threat.xml' */


/* './packages/continuity/nav_places.xml' */


/* './packages/continuity/nav_plan.xml' */


/* './packages/continuity/nav_plan_action.xml' */


/* './packages/continuity/nav_plan_schedule.xml' */


/* './packages/continuity/nav_plan_test.xml' */


/* './packages/continuity/nav_process.xml' */


/* './packages/continuity/nav_process_activity_asset.xml' */


/* './packages/continuity/nav_process_scene.xml' */


/* './packages/continuity/nav_process_threat.xml' */


/* './packages/continuity/nav_providers.xml' */


/* './packages/continuity/nav_resources.xml' */


/* './packages/continuity/nav_scope.xml' */


/* './packages/continuity/nav_un.xml' */


/* './packages/continuity/popup_about_asset.xml' */


/* './packages/continuity/popup_activity_edit.xml' */


/* './packages/continuity/popup_address_locator.xml' */


/* './packages/continuity/popup_area_search.xml' */


/* './packages/continuity/popup_asset_associate_threat.xml' */


/* './packages/continuity/popup_asset_edit.xml' */


/* './packages/continuity/popup_department_edit.xml' */


/* './packages/continuity/popup_financial_impact_edit.xml' */


/* './packages/continuity/popup_group_edit.xml' */


/* './packages/continuity/popup_group_resource_single_search.xml' */

'mx_friday'=>'星期五',

/* './packages/continuity/popup_group_search.xml' */


/* './packages/continuity/popup_group_single_search.xml' */


/* './packages/continuity/popup_people_search.xml' */


/* './packages/continuity/popup_place_asset_edit.xml' */


/* './packages/continuity/popup_place_associate_threat.xml' */


/* './packages/continuity/popup_place_edit.xml' */


/* './packages/continuity/popup_place_resource_edit.xml' */


/* './packages/continuity/popup_place_search.xml' */


/* './packages/continuity/popup_place_single_search.xml' */


/* './packages/continuity/popup_plan_action_edit.xml' */


/* './packages/continuity/popup_plan_action_search.xml' */


/* './packages/continuity/popup_plan_schedule_edit.xml' */


/* './packages/continuity/popup_plan_search.xml' */


/* './packages/continuity/popup_plan_test_edit.xml' */


/* './packages/continuity/popup_plan_type_edit.xml' */


/* './packages/continuity/popup_planRange_edit.xml' */


/* './packages/continuity/popup_process_draw.xml' */


/* './packages/continuity/popup_process_edit.xml' */


/* './packages/continuity/popup_process_search.xml' */


/* './packages/continuity/popup_provider_edit.xml' */


/* './packages/continuity/popup_provider_search.xml' */


/* './packages/continuity/popup_provider_single_search.xml' */


/* './packages/continuity/popup_resource_edit.xml' */


/* './packages/continuity/popup_resource_search.xml' */


/* './packages/continuity/popup_scene_edit.xml' */


/* './packages/continuity/popup_scene_impact_edit.xml' */


/* './packages/continuity/popup_scope_search.xml' */


/* './packages/continuity/popup_scope_view.xml' */


/* './packages/continuity/popup_vulnerability_edit.xml' */


/* './packages/continuity/tab_continuity_management.xml' */


/* './packages/improvement/nav_action_plan.php' */

'mx_corrective'=>'纠正',
'mx_preventive'=>'预防',
'st_ap_remove_message'=>'您是否确实要删除该行动计划的<b> ％名称％ < / b > ？',
'tt_ap_remove'=>'移除行动计划',
'tt_nc_filter_grid_action_plan'=>'（筛选条件不符合\'的<b> ％ nc_name ％ < / b > “ ）',

/* './packages/improvement/nav_action_plan.xml' */

'gc_actiontype'=>'动作类型',
'gc_ap_cns'=>'不符合',
'gc_dateconclusion'=>'整理日期',
'gc_datedeadline'=>'截止日期',
'gc_isefficient'=>'效率',
'gc_responsible_id'=>'负责',
'mi_data_to_meassure'=>'界定措施',
'mi_meassure'=>'措施',
'tt_ap_bl'=>'行动计划的<b> < / b >',

/* './packages/improvement/nav_disciplinary_process.xml' */

'gc_action_taken'=>'行动表现',
'mi_register_action'=>'注册行动',

/* './packages/improvement/nav_incident.php' */

'mx_direct'=>'直接',
'mx_indirect'=>'间接',
'mx_no_category'=>'任何一类',
'st_incident_remove_confirm'=>'您是否确实要删除事件的<b> ％ incident_name ％ < / b > ？',
'tt_incident_remove_confirm'=>'删除事件',

/* './packages/improvement/nav_incident.xml' */

'gc_incident_date_limit'=>'估计',
'gc_loss_type'=>'损失类型',
'mi_disciplinary_process'=>'纪律处分程序',
'mi_finish'=>'完成',
'mi_immediate_disposal_approval'=>'即时处置审批',
'mi_solution_approval'=>'解决方案审批',
'tt_incident'=>'事件的<b> < / b >',

/* './packages/improvement/nav_non_conformity.php' */

'gc_control_security'=>'安全控制',
'gc_external_audit'=>'外部审计',
'gc_internal_audit'=>'内部审计',
'gc_no'=>'否',
'gc_yes'=>'是',
'st_nc_remove_message'=>'您确定您要删除不符合的<b> ％名称％ < / b > ？',
'tt_ap_filter_grid_non_conformity'=>'（过滤行动计划\'的<b> ％ ap_name ％ < / b > “ ）',
'tt_nc_remove'=>'删除不符合',

/* './packages/improvement/nav_non_conformity.xml' */

'gc_action_plans'=>'行动计划',
'gc_capability'=>'潜力',
'gc_sender'=>'辐射源',
'gc_state'=>'身份',
'mi_action_plan'=>'副行动计划',
'mi_send_to_ap_pendant'=>'发送给鸭验收',
'mi_send_to_conclusion'=>'发送完成',
'mi_send_to_responsible'=>'发送给负责',
'tt_nc_bl'=>'的<b>不符合< / b >',

/* './packages/improvement/nav_occurrence.php' */

'st_occurrence_remove'=>'您确定您想要移除发生的<b> ％ occurrence_text ％ < / b > ？',
'tt_occurrence_remove'=>'删除发生',

/* './packages/improvement/nav_occurrence.xml' */

'gc_incident'=>'事件',
'mi_incident'=>'副事件',
'tt_occurrences'=>'发生的<b> < / b >',

/* './packages/improvement/nav_report.php' */

'mx_report_action_plan_by_process'=>'行动计划的过程中',
'mx_report_action_plan_by_user'=>'行动计划由用户',
'mx_report_ap_without_doc'=>'行动计划没有文件',
'mx_report_ci_pendant_tasks'=>'尚未完成的任务在不断改善',
'mx_report_control_nonconformity_by_process'=>'不符合的控制过程',
'mx_report_elements_affected_by_incident'=>'项目受影响的事件',
'mx_report_financial_impact_by_incident'=>'财政影响事件',
'mx_report_incident_accompaniment'=>'事件跟进',
'mx_report_incident_by_responsible'=>'收集证据',
'mx_report_incident_control_revision'=>'控制的修改而导致的事故',
'mx_report_incident_without_risk'=>'事件中无风险',
'mx_report_incidents_without_occurrence'=>'事件没有发生',
'mx_report_nc_without_ap'=>'不符合的，没有行动计划',
'mx_report_nonconformity_accompaniment'=>'不符合的跟进',
'mx_report_nonconformity_by_process'=>'不符合的，由过程',
'mx_report_occurrence_accompaniment'=>'发生跟进',
'mx_report_occurrence_by_incident'=>'由事件发生',
'mx_report_risk_auto_probability_value'=>'自动计算风险的可能性',
'mx_report_user_by_incident'=>'纪律处分程序',

/* './packages/improvement/nav_report.xml' */

'cb_risk'=>'风险',
'lb_conclusion_cl'=>'整理：',
'lb_loss_type_cl'=>'损失类型：',
'lb_show_cl'=>'显示：',
'si_capability_all'=>'全部',
'si_capability_no'=>'否',
'si_capability_yes'=>'是',
'si_ci_manager'=>'不断改进报告',
'si_incident_report'=>'事件报告',
'si_nonconformity_report'=>'不符合的报告',
'st_action_plan_filter_bl'=>'的<b>行动计划“过滤器< / b >',
'st_elements_filter_bl'=>'<b>项目“过滤器< / b >',
'st_incident_filter_bl'=>'的<b>事件“过滤器< / b >',
'st_nonconformity_filter_bl'=>'的<b>不符合“过滤器< / b >',
'st_occurrence_filter_bl'=>'的<b>发生的“过滤器< / b >',
'st_status'=>'状态：',
'wn_at_least_one_element_must_be_selected'=>'至少有一个项目应被选中。',

/* './packages/improvement/popup_action_plan_associate.xml' */

'tt_action_plan_association'=>'行动计划协会',
'tt_current_action_plans_bl'=>'<b>目前的行动计划< / b >',

/* './packages/improvement/popup_action_plan_edit.xml' */

'lb_ap_actionplan_bl_cl'=>'<b>操作： < / b >',
'lb_ap_actiontype_bl_cl'=>'<b>操作类型： < / b >',
'lb_ap_dateconclusion_cl'=>'整理日期：',
'lb_ap_datedeadline_bl_cl'=>'的<b>截止日期： < / b >',
'lb_ap_name_bl_cl'=>'<b>名称： < / b >',
'lb_ap_responsible_id_bl_cl'=>'的<b>负责人： < / b >',
'rb_corrective'=>'纠正',
'rb_preventive'=>'预防',
'tt_ap_edit'=>'行动计划版',
'vb_finish'=>'完成',
'wn_deadline'=>'截止日期应不迟于或等于到目前的日期。',

/* './packages/improvement/popup_action_plan_finish_confirm.xml' */

'lb_ap_days_before_bl'=>'的<b>通知之前， < / b >',
'lb_date_efficiency_revision_bl_cl'=>'的<b>效率修订日期： < / b >',
'st_action_plan_finish_confirm_message'=>'你想不想完成行动计划\' ％名称％ \' ？',
'tt_action_plan_finish'=>'完成行动计划',
'wn_revision_date_after_conclusion_date'=>'效率修订日期应不迟于或同时作为整理日期！',

/* './packages/improvement/popup_action_plan_revision_task.xml' */

'lb_action_cl'=>'行动：',
'st_action_plan_efficient_bl'=>'的<b>是有效率的行动计划？ < / b >',
'st_data_revision_bl_cl'=>'的<b>修订日期： < / b >',
'tt_action_plan_efficiency_revision'=>'行动计划eficiency修订',

/* './packages/improvement/popup_disciplinary_process_action.php' */

'st_denied_permission_to_edit_action_taken'=>'您没有权限登记的行动，在纪律处分程序。',

/* './packages/improvement/popup_disciplinary_process_action.xml' */

'lb_action_bl_cl'=>'<b>操作： < / b >',
'st_incident_cl'=>'的<b>事件： < / b >',
'st_user_cl'=>'<b>用户： < / b >',

/* './packages/improvement/popup_disciplinary_process_edit.php' */

'em_disciplinary_process_creation'=>'纪律处分程序创造',
'em_disciplinary_process_creation_footer'=>'如需进一步资料，联络保安人员。',
'st_incident_user_remove'=>'您确定要删除用户的<b> ％的用户名％ < / b > ，从纪律处分程序，该事件的<b> ％ incident_name ％ < / b > ？',
'tt_incident_user_remove'=>'删除用户',

/* './packages/improvement/popup_disciplinary_process_edit.xml' */

'gc_observation_involvement'=>'观察/接触',
'gc_user'=>'用户',
'lb_incident_cl'=>'事件：',
'lb_observation_involvement_cl'=>'观察/参与：',
'lb_user_bl_cl'=>'<b>用户： < / b >',
'tt_disciplinary_process'=>'的<b>纪律处分程序< / b >',
'wn_email_dp_sent'=>'电子邮件已经发送给纪律处分程序经理。',

/* './packages/improvement/popup_evidence_requirement.xml' */

'lb_comments_cl'=>'评论：',
'tt_evidence_requirement'=>'证据colection ：',
'wn_evidence_requirement_editing'=>'开放式保税仓系统。当编辑的评论以上，警报与评论的内容将被发送到的证据colector经理。',

/* './packages/improvement/popup_financial_impact_edit.xml' */

'st_total'=>'总',
'tt_financial_impact_parametrization'=>'财务影响的估计',

/* './packages/improvement/popup_incident_accounts_plan.xml' */

'tt_incident_accounts_plan'=>'会计计划',

/* './packages/improvement/popup_incident_affected_product_service.xml' */

'tt_affected_product_service'=>'产品/服务受影响',

/* './packages/improvement/popup_incident_approval_task.xml' */

'tt_assets_per_incident'=>'的<b>资产与事件有关< / b >',

/* './packages/improvement/popup_incident_disposal.xml' */

'tt_immediate_disposal'=>'立即处置',

/* './packages/improvement/popup_incident_edit.xml' */

'lb_accounts_plan_cl'=>'会计计划：',
'lb_affected_product_service_cl'=>'产品/受影响的服务：',
'lb_date_bl_cl'=>'<b>日期： < / b >',
'lb_date_limit_bl_cl'=>'<b>估算的整理： < / b >',
'lb_evidences_cl'=>'证据：',
'lb_hour_bl_cl'=>'的<b>小时： < / b >',
'lb_immediate_disposal_cl'=>'即时处理：',
'lb_loss_type_bl_cl'=>'的<b>损失类型< / b >',
'lb_occurrences_bl_cl'=>'的<b>发生： < / b >',
'lb_occurrences_cl'=>'事件发生：',
'lb_solution_cl'=>'解决方案：',
'si_direct'=>'直接',
'si_indirect'=>'间接',
'st_collect_evidence'=>'收集的证据呢？',
'st_incident_date_finish'=>'估计整理：',
'st_incident_date_finish_bl'=>'<b>估算的整理： < / b >',
'st_no'=>'否',
'st_yes'=>'是',
'tt_incident_edit'=>'事件编辑',
'vb_financial_impact'=>'财政影响',
'vb_observations'=>'意见',
'wn_future_date_not_allowed'=>'一个未来的日期/小时是不允许的。',
'wn_prevision_finish'=>'估计整理日期必须等于或不迟于当前天。',

/* './packages/improvement/popup_incident_evidence.xml' */

'tt_evidence'=>'证据',

/* './packages/improvement/popup_incident_process_association_edit.php' */

'st_save_suggested_incident_processes'=>'你想不想与建议的过程，以事件的<b> ％ context_name ％ < / b > ？',
'tt_save_suggested_incident_processes'=>'有关建议的过程',

/* './packages/improvement/popup_incident_process_association_edit.xml' */

'cb_search_only_suggested_process'=>'仅搜索建议进程',
'tt_incident_process_association'=>'进程的事件协会',

/* './packages/improvement/popup_incident_risk_asset_association_edit.xml' */

'lb_assets_cl'=>'资产：',
'lb_risks_cl'=>'风险：',
'st_risk_asset_association_message'=>'当选择一项资产，请选择的风险，你想成为相关。它的可能联想到一个以上的风险，选择他们crtl的关键。',
'tt_risk_asset_association'=>'风险资产协会',

/* './packages/improvement/popup_incident_risk_association_edit.php' */

'em_incident_risk_parametrization'=>'风险估计相关的事件',
'em_incident_risk_parametrization_footer'=>'如需进一步资料，联络保安人员。',
'mx_non_parameterized_risk'=>'非估计风险',
'to_dependencies_bl_cl'=>'的<b>的依赖性： < / b > <br> <br>',
'to_dependents_bl_cl'=>'依赖的<b> ： < / b > <br> <br>',

/* './packages/improvement/popup_incident_risk_association_edit.xml' */

'tt_associated_controls_and_assets'=>'<b>相关管制和资产< / b >',
'tt_incident_risk_association'=>'事件风险协会',
'tt_risks_refferring_controls_and_assets'=>'的<b>的风险就控制和资产< / b >',
'tt_risks_related_selected'=>'<b>相关/选定的风险， < / b >',
'vb_insert_risk'=>'插入风险',
'vb_risk_incident_parametrization'=>'事件的风险估计',
'vb_search_assets'=>'搜索资产',
'vb_search_controls'=>'搜索控制',
'wn_parameterize_incident_risk'=>'您必须估计事件风险协会前节省您的协会。',

/* './packages/improvement/popup_incident_risk_association_options.xml' */

'lb_associate_risk_to_assets'=>'你想不想与目前的风险到选定的资产？您可以选择其中的风险要涉及其中的资产。',
'lb_send_alert_to_other_users'=>'你想不想发送警报为其他用户除了负责安全的资产和控制有关的风险％ new_risks ％ ？',
'tt_incident_risk_association_options'=>'选项风险和事故的协会',
'vb_continue'=>'继续',

/* './packages/improvement/popup_incident_risk_parametrization.xml' */

'tt_incident_risk_association_parametrization'=>'估计这一事件的风险associaton',

/* './packages/improvement/popup_incident_search.xml' */

'tt_incident_search'=>'事件搜索',
'vb_relation'=>'涉及',
'wn_no_incident_selected'=>'没有事件是选定的。',

/* './packages/improvement/popup_incident_solution.xml' */

'tt_solution'=>'解决方案',

/* './packages/improvement/popup_incident_state_forward.php' */

'st_incident_finish'=>'你想不想完成事件？',
'st_incident_send_to_app_disposal'=>'你是否想发送该事件的即时处置审批？',
'st_incident_send_to_app_solution'=>'你要发送的事件的解决方案审批？',
'st_incident_send_to_responsible'=>'你要发送的事件负责呢？',
'tt_incident_finish'=>'完成事件',
'tt_incident_send_to_app_disposal'=>'发送即时处置审批',
'tt_incident_send_to_app_solution'=>'发送到解决方案的批准',
'tt_incident_send_to_responsible'=>'发送给负责',

/* './packages/improvement/popup_incident_task_view.php' */

'lb_incident_immediate_disposal_cl'=>'即时处理：',
'lb_incident_solution_cl'=>'解决方案：',

/* './packages/improvement/popup_incident_task_view.xml' */

'lb_date_limit_cl'=>'限制日期：',

/* './packages/improvement/popup_new_risk_parameter_approval_task.xml' */


/* './packages/improvement/popup_non_conformity_approval_task.xml' */

'lb_processes_cl'=>'过程：',

/* './packages/improvement/popup_non_conformity_associate.xml' */

'st_name'=>'名称：',
'tt_current_non_conformitys_bl'=>'<b>目前不符合< / b >',
'tt_non_conformity_association'=>'不符合协会',

/* './packages/improvement/popup_non_conformity_create.xml' */

'lb_date_sent_cl'=>'日期的排放量：',
'lb_sender_cl'=>'发件人：',
'tt_non_conformity_record'=>'不符合的纪录',

/* './packages/improvement/popup_non_conformity_data_approval_task.php' */

'st_denied_permission_to_execute_task'=>'您没有权限去执行任务。',

/* './packages/improvement/popup_non_conformity_data_approval_task.xml' */

'lb_ap_responsible_cl'=>'空气污染指数分别为：',
'lb_cause_cl'=>'原因：',
'lb_inquirer_cl'=>'询问者：',
'lb_justificative_cl'=>'原因是：',
'lb_non_conformity_cl'=>'不符合的：',
'lb_task_cl'=>'任务：',
'tt_task_visualization'=>'任务的可视化',

/* './packages/improvement/popup_non_conformity_edit.xml' */

'lb_action_plans_cl'=>'行动计划：',
'lb_nc_capability_bl_cl'=>'潜力：',
'lb_nc_cause_cl'=>'原因：',
'lb_nc_classification_cl'=>'分类：',
'lb_nc_datesent_cl'=>'日期的排放量：',
'lb_nc_description_cl'=>'描述：',
'lb_nc_name_bl_cl'=>'<b>名称： < / b >',
'lb_nc_responsible_id_cl'=>'负责人：',
'lb_nc_sender_id_cl'=>'发件人：',
'lb_potential_cl'=>'潜力：',
'lb_processes_bl_cl'=>'的<b>进程： < / b >',
'si_external_audit'=>'外部审计',
'si_internal_audit'=>'内部审计',
'si_no'=>'否',
'si_security_control'=>'安全控制',
'si_yes'=>'是',
'tt_nc_edit'=>'不符合的编辑',

/* './packages/improvement/popup_non_conformity_revision_task.xml' */

'lb_action_plan_cl'=>'行动计划：',
'lb_ap_efficient_bl'=>'的<b>是有效率的行动计划？ < / b >',
'rb_no'=>'否',
'rb_yes'=>'是',
'tt_non_conformity_efficiency_revision'=>'不符合eficiency修订',

/* './packages/improvement/popup_non_conformity_state_forward.php' */

'st_non_conformity_send_to_ap_responsible'=>'你是否想发送不符合的，以便有其行动计划接纳？',
'st_non_conformity_send_to_approve'=>'你是否想发送不符合该行动计划的整理？',
'st_non_conformity_send_to_responsible'=>'你是否想发送不符合该负责？',
'tt_non_conformity_send_to_ap_responsible'=>'传送到行动计划验收',
'tt_non_conformity_send_to_approve'=>'传送到行动计划整理',
'tt_non_conformity_send_to_responsible'=>'发送给负责',

/* './packages/improvement/popup_non_conformity_state_forward.xml' */

'vb_no'=>'否',
'vb_yes'=>'是',

/* './packages/improvement/popup_occurrence_edit.xml' */

'lb_description_bl_cl'=>'<b>说明： < / b >',
'st_date_bl'=>'<b>日期： < / b >',
'st_date_warning'=>'一个未来的日期/小时是不允许的。',
'st_hour_bl'=>'的<b>小时： < / b >',
'tt_register_occurrence'=>'登记的发生',

/* './packages/improvement/popup_occurrences_search.xml' */

'tt_current_occurrences'=>'<b>当前出现的< / b >',
'tt_occurrences_search'=>'发生搜索',

/* './packages/improvement/popup_probability_calculation_config.xml' */

'lb_incidents_in_period_cl'=>'事件之间的％ initial_date ％和％ final_date ％ ：',
'lb_last_check_cl'=>'最后验证：',
'lb_periodicity_bl_cl'=>'周期性的<b> ： < / b >',
'lb_starting_at'=>'入手',
'si_days'=>'天（ ）',
'si_months'=>'本月（ ）',
'si_weeks'=>'周（ ）',
'st_total_incidents_found'=>'总的事件，发现：',
'tt_incidents_related_to_risk_in_period'=>'的<b>有关的事件的风险，在这段期间< / b >',
'tt_probability_calculation_config'=>'配置自动计算的可能性',
'tt_probability_level_vs_incident_count'=>'的<b>的可能性水平<br/> <br/>的<b> X < / b > <br/> <br/>的<b>数额的事件， < / b >',
'vb_update'=>'更新',
'wn_probability_config_not_filled'=>'间隔没有填写或与不一致的价值观',

/* './packages/improvement/popup_process_search.xml' */

'tt_current_processes'=>'<b>当前进程的<b>',
'tt_process_search'=>'过程中搜索',

/* './packages/improvement/popup_request_compare_impact_risks.xml' */


/* './packages/improvement/popup_risk_create.xml' */

'lb_impact_money_cl'=>'财政方面的影响：',
'si_select_one_below'=>'选择的项目之一，下面',
'st_risk_impact'=>'影响：',
'tt_insert_risk'=>'插入风险',

/* './packages/improvement/report/clock.php' */

'st_clock_wait'=>'的<b>注意事项： < / b >这份报告可能需要几分钟的时间，以产生的。 <br>请等待。',

/* './packages/improvement/report/popup_report_action_plan_by_process.php' */

'tt_action_plan_by_process'=>'行动计划的过程中',

/* './packages/improvement/report/popup_report_action_plan_by_user.php' */

'tt_action_plan_by_user'=>'行动计划由用户',

/* './packages/improvement/report/popup_report_ap_without_doc.php' */

'rs_ap_without_docs'=>'行动计划的无证件',

/* './packages/improvement/report/popup_report_ap_without_doc.xml' */

'rs_ap'=>'行动计划',

/* './packages/improvement/report/popup_report_control_nonconformity_by_process.php' */

'mx_control_nonconformity_by_process'=>'不符合的控制过程',

/* './packages/improvement/report/popup_report_disciplinary_process.php' */

'mx_disciplinary_process'=>'纪律程序',

/* './packages/improvement/report/popup_report_disciplinary_process.xml' */

'rs_dp_name'=>'名字',

/* './packages/improvement/report/popup_report_elements_affected_by_incident.php' */

'mx_elements_affected_by_incident'=>'项目受影响的事件',

/* './packages/improvement/report/popup_report_elements_affected_by_incident.xml' */

'rs_incident_bl_cl'=>'的<b>事件： < / b >',

/* './packages/improvement/report/popup_report_financial_impact_by_incident.php' */

'mx_financial_impact_by_incident'=>'财政影响事件',

/* './packages/improvement/report/popup_report_incident_accompaniment.php' */

'mx_incident_accompaniment'=>'事件跟进',

/* './packages/improvement/report/popup_report_incident_by_responsible.php' */

'mx_evidence_collection'=>'收集证据',

/* './packages/improvement/report/popup_report_incident_control_revision.php' */

'mx_incident_control_revision'=>'控制调整而导致的事故',

/* './packages/improvement/report/popup_report_incident_control_revision.xml' */

'rs_date_todo'=>'估计日期',
'rs_er'=>'转口',

/* './packages/improvement/report/popup_report_incident_without_occurrence.php' */

'rs_incidents_without_occurrence'=>'事件没有发生',

/* './packages/improvement/report/popup_report_incident_without_risk.php' */

'rs_incidentes_without_risks'=>'事件无风险',

/* './packages/improvement/report/popup_report_incident_without_risk.xml' */

'rs_incident'=>'事件',

/* './packages/improvement/report/popup_report_incidents.php' */

'mx_incidents'=>'事件',

/* './packages/improvement/report/popup_report_incidents.xml' */

'rs_incident_name'=>'名字',

/* './packages/improvement/report/popup_report_nc_without_ap.php' */

'rs_nc_without_ap'=>'不符合无行动计划',
'st_nc_manager_with_parentheses'=>'（不符合的经理）',

/* './packages/improvement/report/popup_report_non_conformities.php' */

'mx_non_conformities'=>'不符合',

/* './packages/improvement/report/popup_report_non_conformities.xml' */

'rs_nc_name'=>'名字',

/* './packages/improvement/report/popup_report_nonconformity_accompaniment.php' */

'mx_nonconformity_accompaniment'=>'不符合的跟进',
'rs_not_defined'=>'没有界定',

/* './packages/improvement/report/popup_report_nonconformity_accompaniment.xml' */

'rs_action_plan'=>'行动计划',
'rs_conclusion'=>'整理',
'rs_non_conformity'=>'不符合的：',

/* './packages/improvement/report/popup_report_nonconformity_by_process.php' */

'mx_nonconformity_by_process'=>'不符合的，由过程',

/* './packages/improvement/report/popup_report_nonconformity_by_process.xml' */

'rs_nonconformity'=>'不符合',

/* './packages/improvement/report/popup_report_occurrence_accompaniment.php' */

'mx_occurrence_accompaniment'=>'发生跟进',

/* './packages/improvement/report/popup_report_occurrence_accompaniment.xml' */

'rs_creator_cl'=>'作者：',

/* './packages/improvement/report/popup_report_occurrence_by_incident.php' */

'mx_occurrence_by_incident'=>'由事件发生',

/* './packages/improvement/report/popup_report_occurrence_by_incident.xml' */

'report_filter_userid_cl'=>'负责人：',
'rs_occurrence'=>'发生',
'rs_status_cl'=>'状态：',

/* './packages/improvement/report/popup_report_risk_auto_probability_value.php' */

'mx_risk_auto_probability_value'=>'自动计算风险的可能性',

/* './packages/improvement/report/popup_report_risk_auto_probability_value.xml' */

'rs_incident_amount'=>'AMT的。事件',
'rs_last_verification'=>'上次验证',
'rs_periodicity'=>'周期',

/* './packages/improvement/report/popup_report_user_by_incident.php' */

'mx_user_by_incident'=>'纪律处分程序',

/* './packages/improvement/report/popup_report_user_by_incident.xml' */

'rs_description'=>'描述',
'rs_incident_cl'=>'事件：',
'rs_user'=>'用户',

/* './packages/improvement/report/report_template.xml' */

'rs_fim'=>'年底的报告',

/* './packages/improvement/tab_continual_improvement.xml' */

'ti_action_plan'=>'行动计划',
'ti_disciplinary_process'=>'纪律处分程序',
'ti_incident'=>'事件',
'ti_non_conformity'=>'不符合',
'ti_occurrences'=>'发生',
'ti_report'=>'报告',

/* './packages/libraries/nav_asset_category_threats.xml' */


/* './packages/libraries/nav_best_practices.php' */

'st_remove_best_practice_confirm'=>'您是否确实要删除的最佳做法的<b> ％ best_practice_name ％ < / b > ？',
'st_remove_section_confirm'=>'您是否确实要删除该科的<b> ％的Section_Name ％ < / b > ？',
'tt_remove_best_practice'=>'删除的最佳做法',
'tt_remove_section'=>'删除第',

/* './packages/libraries/nav_best_practices.xml' */

'mi_associate_events'=>'副事件',
'mi_open_section'=>'公开组',
'mi_templates'=>'文件范本',
'tt_best_practices_bl'=>'最佳做法的<b> < / b >',
'vb_insert_best_practice'=>'插入的最佳做法',
'vb_insert_section'=>'插入分节',

/* './packages/libraries/nav_department.xml' */


/* './packages/libraries/nav_document_template.php' */

'st_remove_document_template_confirm'=>'您是否确实要删除文档模板的<b> ％ context_name ％ < / b > ？',
'tt_remove_document_template'=>'删除文件范本',

/* './packages/libraries/nav_document_template.xml' */

'si_best_practice'=>'最佳做法',
'si_denied'=>'否认',
'si_doc_template_control'=>'控制',
'si_pending'=>'紧迫的',
'tt_documents_templates_bl'=>'的<b>文件范本< / b >',

/* './packages/libraries/nav_events.php' */

'st_remove_category_cascade_message'=>'您也将删去任何有关的资产类别，以及其风险和他们的协会与其他项目的制度。',
'st_remove_category_confirm'=>'您是否确实要删除类别的<b> ％ category_name ％ < / b > ？',
'st_remove_event_confirm'=>'您是否确实要删除事件的<b> ％ event_name ％ < / b > ？',
'tt_remove_category'=>'删除类别',
'tt_remove_event'=>'删除事件',

/* './packages/libraries/nav_events.xml' */

'mi_associate_best_practices'=>'副的最佳做法',
'mi_open_event_category'=>'公开组',
'tt_events_library_bl'=>'事件的<b>图书馆< / b >',
'vb_associate_event'=>'副事件',
'vb_insert_event'=>'插入事件',

/* './packages/libraries/nav_impact_library.xml' */


/* './packages/libraries/nav_import_export.php' */

'wn_error_invalid_file'=>'错误：无效的档案。',
'wn_extension_not_allowed'=>'延伸不允许的。',
'wn_maximum_size_exceeded'=>'最大尺寸超过。',

/* './packages/libraries/nav_import_export.xml' */

'lb_client_cl'=>'客户：',
'lb_export_library_cl'=>'出口图书馆：',
'lb_import_library_cl'=>'导入库：',
'lb_license_cl'=>'许可：',
'si_best_practices'=>'最佳做法',
'si_events_library'=>'活动图书馆',
'si_standards'=>'标准',
'si_templates'=>'文件范本',
'st_export_list_bl'=>'<b>列出的出口< / b >',
'st_exporting_options_bl'=>'<b>导出选项< / b >',
'st_import_list_bl'=>'<b>列出进口< / b >',
'st_or_bl'=>'<b>或< / b >',
'to_export'=>'<b>汇出： < / b > <br/> <br/>出口选定项目。',
'to_export_list'=>'<b>列出： < / b > <br/> <br/>名单图书馆项目，这将是出口。',
'to_import'=>'<b>导入< / b > <br/> <br/>进口选定项目。',
'to_import_list'=>'<b>列出： < / b > <br/> <br/>名单图书馆项目，这将是进口。',
'tr_best_practices'=>'最佳做法',
'tr_events_library'=>'活动图书馆',
'tr_no_items_to_import'=>'有没有项目，拟进口',
'tr_standards'=>'标准',
'tr_template'=>'文件范本',
'tr_templates'=>'文件范本',
'vb_export'=>'出口',
'vb_import'=>'进口',
'vb_list'=>'名单',
'vb_list_import'=>'名单',
'wn_import_required_file'=>'您必须选择一个文件，然后按一下清单。',

/* './packages/libraries/nav_incident_category.php' */

'st_category_remove'=>'您是否确实要删除类别的<b> ％ category_name ％ < / b > ？',
'st_category_remove_error'=>'它是不可能的，以消除类的<b> ％ category_name ％ < / b > ，因为它是与一个或一个以上的事件！',
'st_solution_remove'=>'您是否确定删除的解决办法？',
'tt_category_remove'=>'删除类别',
'tt_category_remove_error'=>'时发生错误，消除类别',
'tt_solution_remove'=>'删除的解决办法',

/* './packages/libraries/nav_incident_category.xml' */

'gc_keywords'=>'关键词',
'gc_problem'=>'问题',
'gc_solution'=>'解决方案',
'gc_solutions_bl'=>'解决方案的<b> < / b >',
'mi_delete'=>'删除',
'mi_open_category'=>'公开组',
'tt_category_library'=>'图书馆的<b>类别< / b >',
'vb_solution_insert'=>'插入解决方案',

/* './packages/libraries/nav_place_category_threats.xml' */


/* './packages/libraries/nav_plan_ranges.xml' */


/* './packages/libraries/nav_plan_types.xml' */


/* './packages/libraries/nav_process_category_threats.xml' */

'vb_insert_category'=>'插入类',

/* './packages/libraries/nav_standards.php' */

'st_remove_standard_confirm'=>'您是否确实要删除标准的<b> ％ standard_name ％ < / b > ？',
'tt_remove_standard'=>'删除标准',

/* './packages/libraries/nav_standards.xml' */

'gc_creator'=>'创作者',
'tt_standards_bl'=>'的<b>标准< / b >',

/* './packages/libraries/popup_best_practice_add.xml' */

'si_all_sections'=>'所有各款',
'tt_current_best_practices_bl'=>'<b>当前的最佳做法< / b >',

/* './packages/libraries/popup_best_practice_edit.php' */

'tt_best_practice_adding'=>'最佳实践加入',
'tt_best_practice_editing'=>'最佳做法编辑',

/* './packages/libraries/popup_best_practice_edit.xml' */

'lb_control_objective_cl'=>'控制的目的：',
'lb_control_type_cl'=>'控制类型：',
'lb_implementation_guidelines_cl'=>'实施准则：',
'lb_metric_cl'=>'公制：',
'lb_name_good_practices_bl_cl'=>'<b>名称/良好做法： < / b >',
'lb_standards_bl_cl'=>'的<b>标准： < / b >',
'si_administrative'=>'行政',
'si_awareness'=>'意识',
'si_coercion'=>'胁迫',
'si_correction'=>'更正',
'si_detection'=>'检测',
'si_limitation'=>'限制',
'si_monitoring'=>'监测',
'si_physical'=>'物理',
'si_prevention'=>'预防',
'si_recovery'=>'复苏',
'si_technical'=>'技术',
'tt_best_practice'=>'最佳做法',

/* './packages/libraries/popup_category_edit.php' */

'tt_category_adding'=>'分类加入',
'tt_category_editing'=>'分类编辑',

/* './packages/libraries/popup_document_template_edit.php' */

'tt_document_template_adding'=>'文件范本加入',
'tt_document_template_editing'=>'文件模板编辑',

/* './packages/libraries/popup_document_template_edit.xml' */

'lb_best_practice_cl'=>'最佳做法：',
'lb_type_bl_cl'=>'<b>类型： < / b >',
'si_action_plan'=>'行动计划',
'si_area'=>'面积',
'si_asset'=>'资产',
'si_control'=>'控制',
'si_management'=>'管理',
'si_policy'=>'政策',
'si_process'=>'过程',
'si_scope'=>'范围',
'si_select_type'=>'选择类型',

/* './packages/libraries/popup_event_add.xml' */

'tt_current_events_bl'=>'<b>当前事件的< / b >',

/* './packages/libraries/popup_event_edit.php' */

'tt_event_adding'=>'事件加入',
'tt_event_editing'=>'事件编辑',

/* './packages/libraries/popup_event_edit.xml' */

'cb_suggested_event'=>'建议活动',
'lb_observation_cl'=>'观察：',
'st_event_impact_cl'=>'影响：',

/* './packages/libraries/popup_imple_guide.xml' */

'tt_implementation_guidelines'=>'实施准则',

/* './packages/libraries/popup_incident_category_edit.php' */

'tt_category_addition'=>'分类加入',

/* './packages/libraries/popup_incident_category_edit.xml' */

'tt_category_edit'=>'分类版',

/* './packages/libraries/popup_incident_solution_edit.xml' */

'lb_incident_category_cl'=>'类别：',
'lb_problem_bl_cl'=>'的<b>问题： < / b >',
'lb_solution_bl_cl'=>'<b>解决方案： < / b >',
'tt_solution_edit'=>'分类编辑',

/* './packages/libraries/popup_metric.xml' */

'tt_metric'=>'公制',

/* './packages/libraries/popup_process_category_threat_edit.xml' */


/* './packages/libraries/popup_section_edit.php' */

'tt_section_adding'=>'第加入',
'tt_section_editing'=>'第编辑',

/* './packages/libraries/popup_standard_add.xml' */

'tt_current_standards_bl'=>'<b>当前标准< / b >',
'tt_standards_search'=>'标准搜索',

/* './packages/libraries/popup_standard_edit.php' */

'tt_standard_adding'=>'标准加入',
'tt_standard_editing'=>'标准编辑',

/* './packages/libraries/popup_standard_edit.xml' */

'lb_application_cl'=>'应用：',
'lb_objective_cl'=>'目的：',

/* './packages/libraries/tab_libraries.xml' */

'ti_best_practices'=>'最佳做法',
'ti_category_library'=>'事件类别',
'ti_events_library'=>'活动图书馆',
'ti_export_and_import'=>'出口及进口',
'ti_standards'=>'标准',
'ti_templates'=>'文件范本',

/* './packages/policy/nav_all.xml' */

'lb_status'=>'身份',
'tt_all_documents_bl'=>'<b>所有文件< / b >',

/* './packages/policy/nav_approving.xml' */

'tt_approving_documents_bl'=>'的<b>文件，在批准< / b >',

/* './packages/policy/nav_developing.xml' */

'tt_developing_document_bl'=>'文件的<b>在发展< / b >',
'vb_insert_document'=>'插入文件',

/* './packages/policy/nav_obsolete.xml' */

'gc_file_name'=>'文件/链接',
'tt_obsolete_documents_bl'=>'的<b>过时版本的文件< / b >',

/* './packages/policy/nav_publish.xml' */

'tt_published_documents_bl'=>'的<b>发表的文件< / b >',

/* './packages/policy/nav_register.php' */

'st_remove_register_confirm'=>'您是否确实要删除记录的<b> ％ register_name ％ < / b > ？',
'tt_remove_register'=>'删除纪录',

/* './packages/policy/nav_register.xml' */

'gc_retention_time'=>'保留时间',
'gc_storage_place'=>'储存地',
'gc_storage_type'=>'储水式',
'mi_read'=>'阅读',
'mi_read_document'=>'阅读文件',
'mi_readers'=>'读者',
'vb_insert_register'=>'插入记录',

/* './packages/policy/nav_report.php' */

'mx_documents_with_high_revision_frequency'=>'文件与高频率的修订',
'mx_documents_without_registers'=>'文件记录',
'mx_pendant_task_pm'=>'尚未完成的任务',
'mx_report_documents_pendent_reads'=>'文件读等候',
'mx_report_element_classification'=>'分类报告',
'mx_users_with_pendant_read'=>'用户等候读',
'si_access_to_documents'=>'查阅文件审计',
'si_accessed_documents'=>'用户访问审计',
'si_approvers'=>'文件及其批准者',
'si_documents_by_component'=>'文件由主义的组成部分',
'si_documents_by_state'=>'文件',
'si_documents_dates'=>'建立和修改日期的文件',
'si_documents_per_area'=>'文件区',
'si_items_with_without_documents'=>'组件无证件',
'si_most_accessed_documents'=>'最存取文件',
'si_most_revised_documents'=>'最修订的文件',
'si_not_accessed_documents'=>'文件没有访问',
'si_pendant_approvals'=>'待批准',
'si_readers'=>'文件和他们的读者',
'si_registers_per_document'=>'纪录文件',
'si_users_per_process'=>'用户相关的进程',

/* './packages/policy/nav_report.xml' */

'cb_expand_documents'=>'扩大文件',
'cb_expand_readers'=>'扩大读者',
'cb_organize_by_area'=>'组织由区',
'cb_show_all_access'=>'显示所有通道',
'cb_show_register_documents'=>'查看记录文件',
'lb_creation_date_cl'=>'创建日期：',
'lb_documents_bl_cl'=>'的<b>文件： < / b >',
'lb_final_date_cl'=>'最后日期：',
'lb_initial_date_cl'=>'初步日期：',
'lb_last_revision_date_cl'=>'最近修订日期：',
'lb_show_only_first_cl'=>'只显示第一：',
'lb_until'=>'直到',
'lb_users_bl_cl'=>'<b>用户： < / b >',
'si_abnormalities_reports'=>'异常报告',
'si_access_control_reports'=>'访问控制的报告',
'si_all_status'=>'全部',
'si_approved'=>'出版',
'si_developing'=>'在发展',
'si_documents_by_type'=>'文件类型',
'si_general_reports'=>'一般报告',
'si_management_reports'=>'管理报告',
'si_obsolete'=>'已过时',
'si_pendant'=>'在审批',
'si_register_reports'=>'记录报告',
'si_registers_by_type'=>'记录类型',
'si_revision'=>'在修订',
'st_asset'=>'资产',
'st_components'=>'的<b>部分组成： < / b >',
'st_control'=>'控制',
'st_element_type_warning'=>'请，至少选择一个类型。',
'st_process'=>'过程',
'st_status_bl'=>'[状态： < / b >',
'st_type_cl'=>'类型：',
'wn_component_filter_warning'=>'请至少选择一个组件类型。',
'wn_no_filter_available'=>'没有任何过滤功能。',
'wn_select_a_document'=>'请，选择一个文件。',

/* './packages/policy/nav_revision.xml' */

'mi_edit_author'=>'编辑作者',
'tt_revision_documents_bl'=>'的<b>文件在修订< / b >',

/* './packages/policy/nav_to_be_published.php' */

'st_document_confirm_remove'=>'您是否确实要删除文档的<b> ％ document_name ％ < / b > ？',
'tt_remove_document'=>'删除文件',

/* './packages/policy/nav_to_be_published.xml' */

'gc_elements'=>'项目',
'gc_file_version'=>'版本',
'gc_sub_doc'=>'subdoc',
'lb_classification'=>'分类',
'lb_type'=>'类型',
'mi_approve'=>'批准',
'mi_associate_components'=>'副组件',
'mi_comments'=>'评论',
'mi_copy_readers'=>'复制读者',
'mi_details'=>'详情',
'mi_document_templates'=>'文件范本',
'mi_edit_approvers'=>'编辑批准者',
'mi_edit_readers'=>'编辑读者',
'mi_previous_versions'=>'先前版本',
'mi_publish'=>'发布',
'mi_references'=>'参考',
'mi_registers'=>'记录',
'mi_send_to_approval'=>'发送审批',
'mi_send_to_revision'=>'发送修订',
'mi_sub_documents'=>'子文档',
'mi_view_approvers'=>'鉴于批准者',
'mi_view_components'=>'鉴于组件',
'mi_view_readers'=>'鉴于读者',
'tt_documents_to_be_published_bl'=>'文件的<b>即将出版的</b>',

/* './packages/policy/popup_approvers_edit.xml' */

'tt_approvers_editing'=>'批准者编辑',
'tt_curret_approvers_bl'=>'<b>目前批准者< / b >',

/* './packages/policy/popup_approvers_view.xml' */

'tt_approvers_visualization'=>'批准者可视化',

/* './packages/policy/popup_associate_users.xml' */

'gc_login'=>'注册',
'lb_login_cl'=>'登录：',
'tt_current_users_bl'=>'<b>当前用户< / b >',
'tt_users_search'=>'用户搜索',

/* './packages/policy/popup_create_document_confirm.xml' */

'rb_creation_document_question'=>'你是否想创建一个文档在系统中，并用它作为范本，以便记录在案的<b> ％ register_name ％ < / b > ？',
'rb_document_do_nothing'=>'不创建/副文件记录的<b> ％ register_name ％ < / b > ？',
'rb_document_relation_question'=>'你想不想副现有的文件在系统中，并用它为模板作记录的<b> ％ register_name ％ < / b > ？',
'st_what_action_you_wish_to_do'=>'什么行动，你想不想执行？',
'tt_document_creation_or_association'=>'文件添加/协会',

/* './packages/policy/popup_document_author_edit.xml' */

'tt_author_editing'=>'作者版',

/* './packages/policy/popup_document_context_associate.php' */

'st_permission_to_associate_item_document_denied'=>'您没有权限联想到一个项目的文件“管理”或“其他”类型',

/* './packages/policy/popup_document_context_associate.xml' */

'tt_current_items_bl'=>'<b>当前的项目< / b >',
'tt_risk_management_items_association'=>'风险管理项目协会',

/* './packages/policy/popup_document_context_view.xml' */

'tt_associated_risk_management_items_visualization'=>'相关的风险管理项目的可视化',

/* './packages/policy/popup_document_copy_readers.xml' */

'tt_documents_search'=>'文件搜索',
'vb_copy_readers'=>'复制读者',

/* './packages/policy/popup_document_create.xml' */

'lb_document_name_bl_cl'=>'的<b>文件的名称： < / b >',
'tt_new_document'=>'新文件',

/* './packages/policy/popup_document_edit.php' */

'si_no_type'=>'没有类型',
'st_creation_date'=>'创建日期：',
'st_document_approval_confirm'=>'你是否想发送文件批准？',
'st_download_file'=>'下载档案：',
'st_publish_document_confirm'=>'你是否想公布的文件呢？',
'st_publish_document_on_date_confirm'=>'你是否想发布的文件， ％日期％ ？',
'st_size'=>'大小：',
'to_max_file_size'=>'档案大小上限： ％ max_file_size ％',
'tt_document_adding'=>'文件中加入',
'tt_document_editing'=>'文档编辑',
'tt_publish'=>'发布',
'tt_send_for_approval'=>'发送审批',

/* './packages/policy/popup_document_edit.xml' */

'lb_author_bl_cl'=>'<b>作者： < / b >',
'lb_deadline_bl_cl'=>'的<b>截止日期： < / b > <br/> <br/>的截止日期为文件的批准。',
'lb_deadline_cl'=>'截止日期：',
'lb_inform_in_advance_of'=>'可否告知在前进',
'lb_publication_date_bl_cl'=>'<b>发布日期： < / b >',
'lb_revision_cl'=>'修订：',
'si_disable'=>'丧失能力',
'si_enable'=>'使能够',
'si_file'=>'文件',
'si_link'=>'链接',
'st_days'=>'天。',
'st_general_information'=>'一般资料',
'st_management_information'=>'管理信息',
'st_test'=>'测试',
'to_doc_instance_manual_version'=>'<b>手动版本： < / b > <br/>到手册版本才生效时，该文件有一个文件或一个链接相关的。',
'to_document_inform_in_advance'=>'的<b>在预先告知： < / b > <br/> <br/> ，这是天数之前的最后期限或修订，其中一个警示或警告的任务，必须产生。',
'to_document_type_modification'=>'要修改的文件类型，您必须先移除之间的关系的文件和风险管理项目。',
'wn_max_file_size_exceeded'=>'档案大小上限超出。',
'wn_publication_date_after_today'=>'出版日期必须是相同的或经过今天的日期。',
'wn_upload_error'=>'错误尝试传送的文件。',

/* './packages/policy/popup_document_instances.php' */

'st_copy_previous_version_confirm'=>'您是否确定要复制以前的版本？',
'tt_copy_previous_version'=>'复制以前的版本',

/* './packages/policy/popup_document_instances.xml' */

'gc_end_date'=>'结束日期',
'gc_modification_comment'=>'改性评论',
'gc_revision_justification'=>'修订的原因',
'gc_start_date'=>'开始日期',
'gc_version'=>'版本',
'tt_previous_versions'=>'先前版本',
'wn_no_version_selected'=>'是没有选择的版本。',

/* './packages/policy/popup_document_manage.xml' */

'st_approver_list_only_when_developing'=>'该批准者名单，只能修改文件时，是在发展。',
'st_items_association_not_management_or_others'=>'这是唯一可能的改变协会的一份文件，这是不是“管理”或“其他”类型。',
'st_items_association_only_when_developing'=>'这是唯一可能的修改协会的一份文件，这是在发展。',
'st_readers_list_only_when_developing'=>'读者清单，只能编辑时，该文件是在发展。',
'st_revision_for_published_document_only'=>'只是一个发布的文件能够被修改',
'tt_associate_items_bl'=>'副项目的<b> < / b >',
'tt_edit_approvers_bl'=>'<b>修改批准者< / b >',
'tt_edit_readers_bl'=>'<b>编辑读者< / b >',
'tt_manage_document'=>'管理文件',
'tt_send_for_revision_bl'=>'<b>发送修订< / b >',
'tt_view_approvers_bl'=>'<b>查看批准者< / b >',
'tt_view_items_bl'=>'<b>查看项目< / b >',
'tt_view_readers_bl'=>'<b>查看读者< / b >',
'vb_copy'=>'复制',
'vb_send'=>'发送',

/* './packages/policy/popup_document_read.php' */

'st_remove_comment_confirm'=>'您是否确定删除转介评论？',
'tt_approve_document'=>'批准文件',
'tt_remove_comment'=>'删除评论',

/* './packages/policy/popup_document_read.xml' */

'ti_comments'=>'评论',
'ti_general'=>'一般',
'ti_references'=>'参考',
'ti_sub_documents'=>'小组文件',
'tt_document_reading'=>'文件读',
'vb_approve'=>'批准',
'vb_deny'=>'拒绝',

/* './packages/policy/popup_document_read_comments.xml' */

'gc_author'=>'作者',
'gc_comment'=>'评论',
'gc_date'=>'日期',
'tt_coments'=>'评论',
'tt_comments_bl'=>'<b>意见< / b >',

/* './packages/policy/popup_document_read_details.xml' */

'lb_date_cl'=>'日期：',
'lb_keywords_cl'=>'关键词：',
'lb_size_cl'=>'大小：',
'lb_version_cl'=>'版本：',
'tt_details'=>'详情',

/* './packages/policy/popup_document_read_subdocuments.xml' */

'tt_sub_documents'=>'小组文件',

/* './packages/policy/popup_document_reference.php' */

'st_remove_reference_confirm'=>'您是否确实要删除参考的<b> ％ doc_reference_name ％ < / b > ？',
'tt_remove_reference'=>'删除参考',

/* './packages/policy/popup_document_reference.xml' */

'gc_creation_date'=>'作品创作日期',
'gc_link'=>'链接',
'lb_link_bl_cl'=>'<b>链接： < / b >',
'st_http'=>'的http://',
'tt_references'=>'参考',
'tt_references_bl'=>'的<b>参考< / b >',

/* './packages/policy/popup_document_register.xml' */

'tt_associated_registers_bl'=>'的<b>相关记录< / b >',
'tt_registers'=>'记录',
'vb_disassociate'=>'不赞同',

/* './packages/policy/popup_document_register_associate.xml' */

'gc_classification'=>'分类',
'gc_place'=>'地点',
'gc_time'=>'时间',
'gc_type'=>'类型',
'tt_register_association'=>'记录协会',
'tt_registers_bl'=>'记录的<b> < / b >',
'vb_associate_registers'=>'副纪录',
'wn_no_register_selected'=>'是没有选定的纪录。',

/* './packages/policy/popup_document_register_edit.php' */

'tt_register_adding'=>'记录加入',
'tt_register_editing'=>'记录编辑',

/* './packages/policy/popup_document_register_edit.xml' */

'lb_disposition_cl'=>'处理方式：',
'lb_indexing_type_cl'=>'类型的索引：',
'lb_origin_cl'=>'资料来源：',
'lb_protection_requirements_cl'=>'保护要求：',
'si_day_days'=>'天（ ）',
'si_month_months'=>'本月（ ）',
'si_week_weeks'=>'周（ ）',
'vb_edit'=>'编辑',
'vb_lookup'=>'搜索',
'wn_retention_time'=>'的保留时间要长于零',

/* './packages/policy/popup_document_register_read.xml' */

'lb_responsible'=>'负责',
'lb_retention_time_cl'=>'的<b>保留时间： < / b >',
'lb_storage_place_cl'=>'储存地点：',
'lb_storage_type_cl'=>'存储类型：',
'tt_register_reading'=>'记录读',
'vb_preferences'=>'偏好',

/* './packages/policy/popup_document_revise_not_alter.xml' */

'tt_revise_document_without_altering'=>'修改文件，而不改变',

/* './packages/policy/popup_document_revision_task.php' */

'mx_scheduled_revision'=>'定修订',

/* './packages/policy/popup_document_revision_task.xml' */

'lb_author_cl'=>'作者：',
'lb_file_cl'=>'档案：',
'lb_link_cl'=>'链接：',
'tt_document_revision'=>'文件修订',
'vb_create_new_version'=>'创建新版本',
'vb_download'=>'下载',
'vb_revise_without_altering'=>'修改而不改变',
'vb_visit_link'=>'访问链接',
'vb_visualize'=>'查看',

/* './packages/policy/popup_document_search.xml' */

'tt_document_search'=>'文件搜索',
'wn_no_document_selected'=>'是没有选定的文件。',

/* './packages/policy/popup_documents_search.xml' */

'tt_current_documents_bl'=>'<b>当前文件< / b >',

/* './packages/policy/popup_modification_comment.php' */

'tt_popup_modification_comment_creation'=>'创作评论',
'tt_popup_modification_comment_creationfasdfawfqwerqwe'=>'创作评论',

/* './packages/policy/popup_modification_comment.xml' */

'lb_comment_bl_cl'=>'的<b>评论： < / b >',
'tt_comment_modification'=>'评论改性',
'tt_comment_modification2'=>'评论改性',

/* './packages/policy/popup_readers_edit.xml' */

'si_all_processes'=>'所有进程',
'tt_current_readers_bl'=>'<b>当前读者< / b >',
'tt_readers_editing'=>'读者编辑',

/* './packages/policy/popup_readers_view.xml' */

'tt_readers_visualization'=>'读者可视化',

/* './packages/policy/popup_register_readers_edit.xml' */

'tt_current_readers'=>'<b>当前读者< / b >',
'tt_readers_edit'=>'读者编辑',

/* './packages/policy/popup_revision_justification.xml' */

'tt_revision_justification'=>'修订的原因',
'vb_send_for_revision'=>'发送修订',

/* './packages/policy/popup_subdocuments_management.php' */

'st_create_subdocument'=>'创建子文档',

/* './packages/policy/popup_subdocuments_management.xml' */

'tt_manage_subdocuments'=>'子文档管理',

/* './packages/policy/popup_template_search.xml' */

'rb_suggested_by_best_practice_only'=>'只显示模板的建议的最佳做法',
'rb_suggested_only'=>'建议只',
'tt_document_templates_search'=>'文件范本搜索',

/* './packages/policy/report/popup_report_accessed_documents.php' */

'rs_accessed_documents'=>'用户访问审计',
'rs_management'=>'管理',
'rs_others'=>'其他',

/* './packages/policy/report/popup_report_accessed_documents.xml' */

'rs_type'=>'类型',

/* './packages/policy/report/popup_report_contexts_without_documents.php' */

'rs_items_with_without_documents'=>'组件无证件',

/* './packages/policy/report/popup_report_contexts_without_documents.xml' */

'rs_components'=>'组件',

/* './packages/policy/report/popup_report_doc_instance_comment_and_justification.php' */

'mx_doc_instance_comment_and_justification'=>'文件的意见和理由',

/* './packages/policy/report/popup_report_doc_instance_comment_and_justification.xml' */

'rs_header_instance_modify_comment'=>'改性评论',
'rs_header_instance_rev_justification'=>'修订的原因',

/* './packages/policy/report/popup_report_docs_with_high_frequency_revision.php' */

'tt_docs_with_high_frequency_revision'=>'文件与高频率的修订',

/* './packages/policy/report/popup_report_docs_with_high_frequency_revision.xml' */

'rs_justification'=>'原因',

/* './packages/policy/report/popup_report_document_accesses.php' */

'rs_accesses_to_documents'=>'查阅文件审计',
'rs_no_type'=>'没有类型',

/* './packages/policy/report/popup_report_document_accesses.xml' */

'rs_date'=>'日期',
'rs_time'=>'时间',

/* './packages/policy/report/popup_report_document_approvers.php' */

'rs_approvers'=>'文件及其批准者',

/* './packages/policy/report/popup_report_document_dates.php' */

'rs_documents_dates'=>'建立和修改日期的文件',

/* './packages/policy/report/popup_report_document_dates.xml' */

'rs_created_on_cl'=>'创建于：',

/* './packages/policy/report/popup_report_document_readers.php' */

'rs_readers'=>'文件和他们的读者',

/* './packages/policy/report/popup_report_document_readers.xml' */

'rs_author_cl'=>'作者：',

/* './packages/policy/report/popup_report_document_registers.php' */

'rs_registers_per_document'=>'纪录文件',

/* './packages/policy/report/popup_report_document_registers.xml' */

'rs_name'=>'名字',
'rs_retention_time'=>'保留时间',

/* './packages/policy/report/popup_report_document_summary.php' */

'mx_report_document_summary'=>'文件属性',

/* './packages/policy/report/popup_report_documents_by_area.php' */

'rs_documents_per_area'=>'文件区',

/* './packages/policy/report/popup_report_documents_by_area.xml' */

'rs_document_documents'=>'文件（ ）',

/* './packages/policy/report/popup_report_documents_by_component.php' */

'rs_document_of_component'=>'％组成％文件',
'tt_documents_by_component'=>'文件由主义的组成部分',

/* './packages/policy/report/popup_report_documents_by_state.php' */

'tt_documents_by_state'=>'文件',

/* './packages/policy/report/popup_report_documents_by_type.php' */

'rs_documents_by_type'=>'文件类型',

/* './packages/policy/report/popup_report_documents_revision_late.php' */

'mx_documents_revision_late'=>'文件延迟修订',

/* './packages/policy/report/popup_report_documents_revision_late.xml' */

'rs_document_next_revision'=>'明年修订',

/* './packages/policy/report/popup_report_documents_revision_schedule.php' */

'mx_documents_revision_schedule'=>'文件修订附表',

/* './packages/policy/report/popup_report_documents_revision_schedule.xml' */

'rs_document_name'=>'名字',
'rs_document_schedule'=>'调度',

/* './packages/policy/report/popup_report_documents_without_register.php' */

'rs_document_main_approver_name'=>'负责文件',
'rs_document_name_bl'=>'文件',
'tt_documents_without_register'=>'文件记录',

/* './packages/policy/report/popup_report_not_accessed_documents.php' */

'rs_not_accessed_documents'=>'文件没有访问',

/* './packages/policy/report/popup_report_pendant_approvals.php' */

'tt_pendant_approvals'=>'待批准',

/* './packages/policy/report/popup_report_pendant_approvals.xml' */

'rs_documents'=>'文件',

/* './packages/policy/report/popup_report_registers_by_type.php' */

'rs_registers_by_type'=>'记录类型',

/* './packages/policy/report/popup_report_top_accessed_documents.php' */

'rs_most_accessed_documents'=>'最存取文件',

/* './packages/policy/report/popup_report_top_accessed_documents.xml' */

'rs_accesses_cl'=>'通道：',

/* './packages/policy/report/popup_report_top_revised_documents.php' */

'rs_most_revised_documents'=>'最修订的文件',

/* './packages/policy/report/popup_report_top_revised_documents.xml' */

'rs_revision_date'=>'修订日期',
'rs_revisions_cl'=>'修改：',
'rs_version'=>'版本',

/* './packages/policy/report/popup_report_user_comments.php' */

'mx_user_comments'=>'用户评论',

/* './packages/policy/report/popup_report_user_comments.xml' */

'rs_comment_date'=>'日期',
'rs_comment_text'=>'评论',
'rs_comment_user'=>'用户',

/* './packages/policy/report/popup_report_users_by_process.php' */

'rs_users_per_process'=>'用户相关的进程',

/* './packages/policy/report/popup_report_users_by_process_and_area.xml' */

'rs_user_bl_cl'=>'<b>用户： < / b >',

/* './packages/policy/report/popup_report_users_with_pendant_read.php' */

'mx_documents_pendent_reads'=>'用户等候读',

/* './packages/policy/tab_policy_management.xml' */

'ti_all'=>'全部',
'ti_developing'=>'在发展',
'ti_obsolete'=>'已过时',
'ti_publish'=>'出版',
'ti_registers'=>'记录',
'ti_revision'=>'在修订',
'ti_to_approve'=>'在审批',
'ti_to_be_published'=>'即将出版',

/* './packages/risk/nav_area.php' */

'st_area_remove_confirm'=>'您是否确实要删除该地区的<b> ％ area_name ％ < / b > ？',
'tt_remove_area'=>'删除区',

/* './packages/risk/nav_area.xml' */

'gc_subarea'=>'分地区',
'lb_sub_areas_count_cl'=>'数量的分领域：',
'mi_processes'=>'进程',
'mi_sub_areas'=>'分地区',
'tt_business_areas_bl'=>'的<b>业务领域< / b >',

/* './packages/risk/nav_asset.php' */

'grid_asset_filter_dependents'=>'（相依性资产的<b> ％ asset_name ％ < / b > “ ）',
'st_asset_copy'=>'复制',
'st_asset_remove_confirm'=>'您是否确实要删除资产的<b> ％ asset_name ％ < / b > ？',
'tt_remove_asset'=>'删除资产',

/* './packages/risk/nav_asset.xml' */

'gc_asset_category'=>'资产类别',
'gc_asset_dependencies'=>'环保署署长。 （ cies ）',
'gc_asset_dependents'=>'环保署署长。 （经济需求测试）',
'gc_processes'=>'进程',
'lb_process_count_cl'=>'的进程数目：',
'mi_associate_processes'=>'副进程',
'mi_duplicate'=>'重复',
'mi_edit_dependencies'=>'调整相依',
'mi_edit_dependents'=>'调整家属',
'mi_insert_risk'=>'插入风险',
'mi_load_risks_from_library'=>'负载的风险，从图书馆',
'tt_assets_bl'=>'资产的<b> < / b >',

/* './packages/risk/nav_control.php' */

'st_control_remove_confirm'=>'您是否确实要删除控制的<b> ％ control_name ％ < / b > ？',

/* './packages/risk/nav_control.xml' */

'gc_cost'=>'成本',
'gc_icon'=>'图标',
'gc_risks'=>'风险',
'lb_risk_count_cl'=>'一些风险：',
'mi_associate_risks'=>'副风险',
'mi_view_risks'=>'鉴于风险',
'rb_active'=>'激活',
'rb_not_active'=>'停用',
'tt_controls_directory_bl'=>'<b>控制目录< / b >',
'vb_revision_and_test_history'=>'修订/测试历史',

/* './packages/risk/nav_process.php' */

'grid_process_asset_filter'=>'（过滤资产的<b> ％ asset_name ％ < / b > “ ）',
'st_process_remove_confirm'=>'您是否确实要删除的过程中的<b> ％ process_name ％ < / b > ？',
'tt_remove_process'=>'删除过程',

/* './packages/risk/nav_process.xml' */

'cb_not_estimated'=>'未估计',
'gc_assets'=>'资产',
'gc_responsible'=>'负责',
'gc_users'=>'用户',
'lb_asset_count_cl'=>'有多少资产：',
'lb_responsible_cl'=>'负责人：',
'lb_value_cl'=>'风险：',
'mi_add_document'=>'创建文件',
'mi_associate_assets'=>'副资产',
'mi_documents'=>'文件',
'mi_view_assets'=>'鉴于资产',
'si_all_priorities'=>'全部',
'si_all_responsibles'=>'全部',
'si_all_types'=>'全部',
'tt_processes_bl'=>'的<b>进程< / b >',

/* './packages/risk/nav_report.php' */

'mx_no_priority'=>'没有优先',
'mx_no_type'=>'没有类型',
'si_amount_of_risks_per_area'=>'数额的风险区',
'si_amount_of_risks_per_process'=>'数额的风险进程',
'si_areas_without_processes'=>'地区无进程',
'si_assets'=>'资产',
'si_assets_importance'=>'资产的重要性',
'si_assets_without_risks'=>'资产风险事件',
'si_checklist_per_event'=>'核对资产',
'si_controls_efficiency'=>'管制效率',
'si_controls_implementation_date'=>'管制的实施日期',
'si_controls_per_responsible'=>'管制由负责',
'si_controls_per_risk'=>'控制风险',
'si_controls_without_risks'=>'管制没有相关的风险',
'si_cost_of_controls_per_area'=>'成本控制区',
'si_cost_of_controls_per_asset'=>'成本控制的资产',
'si_cost_of_controls_per_process'=>'成本控制的过程',
'si_cost_of_controls_per_responsible'=>'成本控制负责',
'si_cost_per_control'=>'成本控制',
'si_duplicated_risks'=>'重复的风险',
'si_element_classification'=>'项目分类',
'si_follow_up_of_controls'=>'跟进的管制',
'si_follow_up_of_controls_efficiency'=>'跟进的管制效率',
'si_follow_up_of_controls_test'=>'跟进的管制测试',
'si_non_parameterized_risks'=>'非估计风险',
'si_not_measured_controls'=>'没有计量控制',
'si_pending_items'=>'未决事项',
'si_pending_tasks'=>'尚未完成的任务',
'si_plan_of_risks_treatment'=>'风险处理计划',
'si_planning_of_controls'=>'规划管制',
'si_processes_without_assets'=>'过程中没有资产',
'si_report_conformity'=>'符合',
'si_risk_impact'=>'风险影响',
'si_risk_status_per_area'=>'风险状况按地区',
'si_risk_status_per_process'=>'风险地位的过程中',
'si_risks_per_area'=>'风险区',
'si_risks_per_asset'=>'风险资产',
'si_risks_per_control'=>'风险控制',
'si_risks_per_process'=>'风险进程',
'si_risks_values'=>'风险值',
'si_sgsi_policy_statement'=>'主义的政策声明',
'si_sgsi_scope_statement'=>'主义的范围报表',
'si_statement_of_applicability'=>'声明适用性',
'si_summary_of_controls'=>'综述管制',
'si_top_10_assets_with_highest_risks'=>'前10名的资产与最高风险',
'si_top_10_assets_with_most_risks'=>'前10名的资产与更多的风险',
'si_top_10_risks_per_area'=>'十大风险区',
'si_top_10_risks_per_process'=>'十大风险进程',
'si_users_responsibilities'=>'使用者的责任',
'st_all_standards'=>'全部',

/* './packages/risk/nav_report.xml' */

'cb_all'=>'全部',
'cb_area'=>'面积',
'cb_asset'=>'资产',
'cb_asset_security'=>'资产安全',
'cb_control'=>'控制',
'cb_high'=>'高的',
'cb_low'=>'低的',
'cb_medium'=>'中等',
'cb_not_parameterized'=>'未估计',
'cb_organize_by_asset'=>'组织资产',
'cb_organize_by_process'=>'组织由进程',
'cb_process'=>'过程',
'lb_classification_cl'=>'分类：',
'lb_comment_cl'=>'评论：',
'lb_consider_cl'=>'考虑：',
'lb_filter_by_cl'=>'过滤条件：',
'lb_filter_by_standard_cl'=>'筛选标准：',
'lb_filter_cl'=>'过滤器：',
'lb_process_cl'=>'过程：',
'lb_reponsible_for_cl'=>'负责：',
'lb_values_cl'=>'值：',
'rb_area'=>'面积',
'rb_area_process'=>'面积和过程',
'rb_asset'=>'资产',
'rb_both'=>'两者',
'rb_detailed'=>'详细',
'rb_efficient'=>'效率',
'rb_implemented'=>'实施',
'rb_inefficient'=>'效率低下',
'rb_planned'=>'计划',
'rb_process'=>'过程',
'rb_real'=>'潜力',
'rb_residual'=>'剩余',
'rb_summarized'=>'总结',
'si_all_reports'=>'所有报告',
'si_areas_by_priority'=>'地区优先',
'si_areas_by_type'=>'地区类型',
'si_control_reports'=>'控制报告',
'si_controls_by_type'=>'控制类型',
'si_events_by_type'=>'活动类型',
'si_financial_reports'=>'财务报告',
'si_iso_27001_reports'=>'的ISO 27001的报告',
'si_others'=>'其他',
'si_processes_by_priority'=>'程序优先',
'si_processes_by_type'=>'过程类型',
'si_risk_management_reports'=>'风险管理的报告',
'si_risk_reports'=>'风险报告',
'si_risks_by_type'=>'风险类型',
'si_summaries'=>'摘要',
'st_area_priority'=>'地区优先事项',
'st_area_type'=>'区类型',
'st_asset_cl'=>'资产:',
'st_config_filter_type_priority'=>'配置选定的过滤器：',
'st_control_type'=>'控制类型',
'st_event_type'=>'事件类型',
'st_no_filter_available'=>'没有过滤器可用。',
'st_none_selected'=>'选择一个过滤器',
'st_organize_by_asset'=>'组织资产',
'st_process_priority'=>'过程中的优先事项',
'st_process_type'=>'过程类型',
'st_risk_type'=>'风险类型',
'st_show_cl'=>'显示：',
'st_standard_cl'=>'标准：',
'tt_specific_report_filters'=>'报告具体的过滤器',
'tt_type_and_priority_filter_cl'=>'类型和优先过滤器：',
'vb_pre_report'=>'前报告',
'vb_report'=>'报告',
'wn_no_report_selected'=>'是没有选定的报告。',
'wn_select_at_least_one_filter'=>'至少有一个过滤器，必须选择！',
'wn_select_at_least_one_parameter'=>'至少有一个参数应被选中。',
'wn_select_risk_value'=>'请选择一个风险值，以显示。',

/* './packages/risk/nav_risk.php' */

'gs_avoided'=>'避免',
'gs_reduced'=>'减少',
'gs_restrained'=>'保留',
'gs_transferred'=>'转移',
'st_denied_permission_to_edit'=>'您没有权限编辑',
'st_risk_remove_confirm'=>'您是否确实要删除的风险的<b> ％ risk_name ％ < / b > ？',
'st_risk_remove_has_incident'=>'<br> <br>的<b> atention ：风险事件（ ）相关联。 < / b >',
'st_risk_remove_many_confirm'=>'您确定要删除的<b> ％计数％ < / b >选定的风险？',
'st_risk_remove_many_have_incident'=>'<br> <br>的<b>注意事项：至少有一个风险事件（ ）相关联。 < / b >',
'tt_incident_filter'=>'过滤事件\'的<b> ％ incident_name ％ < / b > “',
'tt_remove_risk'=>'删除的风险',

/* './packages/risk/nav_risk.xml' */

'cb_avoided'=>'避免',
'cb_not_treated'=>'不治疗',
'cb_reduced'=>'减少',
'cb_restrained'=>'保留',
'cb_search_in_name_only'=>'搜索只在名称',
'cb_transferred'=>'转移',
'gc_controls'=>'管制',
'gc_residual'=>'剩余',
'gc_risk'=>'风险',
'gc_risk_name'=>'名字',
'gc_status'=>'身份',
'gc_treatment'=>'治疗',
'lb_control_count_cl'=>'数量管制：',
'lb_name_description_cl'=>'名称/描述',
'lb_risk_residual_value_cl'=>'剩余：',
'lb_risk_value_cl'=>'风险：',
'lb_status_cl'=>'状态：',
'lb_treatment_type_cl'=>'治疗类型：',
'mi_avoid'=>'避免风险',
'mi_cancel_treatment'=>'取消治疗',
'mi_hold_accept'=>'保留/接受的风险',
'mi_reduce_risk'=>'降低风险',
'mi_transfer'=>'转移风险',
'mi_view_controls'=>'鉴于管制',
'si_all_assets'=>'全部',
'si_all_states'=>'全部',
'st_from'=>'自',
'st_to'=>'至',
'vb_aply'=>'应用',
'vb_parameterize'=>'估计',

/* './packages/risk/popup_area_edit.php' */

'tt_area_adding'=>'地区加入',
'tt_area_editing'=>'区编辑',

/* './packages/risk/popup_area_edit.xml' */

'vb_ok'=>'好的',

/* './packages/risk/popup_asset_dependencies.php' */

'st_dependencies_list'=>'相依名单',
'st_dependents_list'=>'随军家属名单',
'tt_dependencies_adjustment'=>'相依性调整',
'tt_dependents_adjustment'=>'家属调整',

/* './packages/risk/popup_asset_dependencies.xml' */

'tt_assets_list'=>'资产清单',

/* './packages/risk/popup_asset_edit.php' */

'tt_asset_adding'=>'资产加入',
'tt_asset_duplicate'=>'资产重复',
'tt_asset_editing'=>'资产编辑',

/* './packages/risk/popup_asset_edit.xml' */

'lb_demands_attention_to_legal_conformity_cl'=>'要求注意的法律符合：',
'lb_justification_cl'=>'原因是：',
'lb_security_responsible_bl_cl'=>'<b>安全负责： < / b >',
'lb_value_cl_money_symbol'=>'费用',
'st_asset_parametrization_bl'=>'资产相关的<b> < / b >',
'vb_adjust_dependencies'=>'调整相依',
'vb_adjust_dependents'=>'调整家属',

/* './packages/risk/popup_asset_search.xml' */

'wn_no_asset_selected'=>'是没有选择的资产。',

/* './packages/risk/popup_best_practice_search.xml' */

'lb_section_cl'=>'区段：',
'si_all'=>'全部',
'tt_best_practices_search'=>'最佳做法搜索',

/* './packages/risk/popup_control_cost.xml' */

'lb_total'=>'总',
'tt_control_cost'=>'控制成本',

/* './packages/risk/popup_control_edit.php' */

'st_and'=>'和',
'st_of_revision'=>'修订',
'st_of_test'=>'测试',
'st_remove_control_implementation_confirm'=>'您是否确实要删除执行控制的<b> ％ control_name ％ < / b > ？',
'st_result_in_removing'=>'这将导致取消',
'st_this_control'=>'这种控制',
'tt_control_adding'=>'控制加入',
'tt_remove_control_implementation'=>'移除控制的执行情况',

/* './packages/risk/popup_control_edit.xml' */

'cb_enable_control_revision_cl'=>'使能控制修订：',
'cb_enable_control_tests_cl'=>'使能控制的测试：',
'lb_best_practices_bl_cl'=>'的<b>的最佳做法： < / b >',
'lb_control_cost_cl'=>'控制成本：',
'lb_evidence_cl'=>'证据：',
'lb_implementation_limit_bl_cl'=>'的<b>实施限制： < / b >',
'lb_implemented_on_cl'=>'实施的：',
'lb_send_alert_task_bl_cl'=>'<b>发送警报/任务： < / b >',
'st_day_days_before'=>'天（ ）之前，',
'st_money_symbol'=>'元',
'tt_control_editing'=>'控制编辑',
'vb_add'=>'添加',
'vb_control_cost'=>'控制成本',
'vb_implement'=>'落实',
'vb_remove_implementation'=>'删除的执行情况',
'vb_revision'=>'修订',
'vb_tests'=>'测试',
'wn_implementation_cannot_be_null'=>'实施日期不能空，如果您要删除的执行情况，使用适当的按钮来执行此事件。',
'wn_implementation_limit_after_today'=>'截止日期必须相同或之后目前的日期。',

/* './packages/risk/popup_control_revision.xml' */

'lb_expected_efficiency_bl_cl'=>'的<b>预计效率： < / b >',
'lb_high_value_5_cl'=>'（高）值为5 ：',
'lb_low_value_1_cl'=>'（低）值1 ：',
'st_helper'=>'[ ？ ]',
'to_control_revision_expected_efficiency'=>'的<b>预计效率： < / b > <br/> <br/>确定哪些服务水平，预计为控制在指定期间内。 <br/> <br/>考虑必要的性能，以减少相关的风险本控制。',
'to_control_revision_metric_help'=>'公制的<b> ： < / b > <br/> <br/>界定如何衡量这种控制。',
'to_control_revision_value'=>'<b>值： < / b > <br/> <br/>确定一个规模为可能的服务水平的控制，这在该期限内。 <br/> <br/>考虑低的价值作为一个坏的表现，以及更高的价值观，尽可能最佳的表现为控制。',
'tt_control_efficiency_revision'=>'控制效率修订',

/* './packages/risk/popup_control_revision_and_test_edit.php' */

'st_control_test_not_ok'=>'不能确定',
'st_control_test_ok'=>'好的',
'st_revision_remove_confirm'=>'您确定要删除该修订估计日期的<b> ％ deadline_date ％ < / b >和成绩之日起的<b> ％ realized_date ％ < / b > ？',
'st_revision_update_by_insert_confirm'=>'是有控制的修订估计日期的<b> ％ date_to_do ％ < / b > 。您确定要以这种控制修改？',
'st_test_remove_confirm'=>'您确定要删除的测试与估计日期的<b> ％ deadline_date ％ < / b >和成绩之日起的<b> ％ realized_date ％ < / b > ？',
'st_test_update_by_insert_confirm'=>'是有控制的试验研究，估计日期的<b> ％ date_to_do ％ < / b > 。您确定要以这种控制测试？',
'tt_control_efficiency_history_add_title'=>'插入控制修订',
'tt_control_test_history_add_title'=>'插入控制测试',
'tt_remove_control'=>'移除控制的执行情况',
'tt_replace_control_revision'=>'更换修改控制的<b> ％ control_name ％ < / b >',
'tt_replace_control_test'=>'更换试验控制的<b> ％ control_name ％ < / b >',

/* './packages/risk/popup_control_revision_and_test_edit.xml' */

'gc_date_realized'=>'完成日期',
'gc_date_to_do'=>'估计日期',
'gc_expected_efficiency'=>'电子工程专辑',
'gc_real_efficiency'=>'转口',
'gc_realization_date'=>'完成日期',
'gc_test_date_to_do'=>'估计日期',
'gc_test_value'=>'测试值',
'mi_edit'=>'编辑',
'mi_remove'=>'删除',
'mi_revision_edit'=>'编辑修改',
'mi_test_edit'=>'编辑测试',
'rb_not_ok'=>'不能确定',
'rb_ok'=>'好的',
'st_control_test_result'=>'<b>测试： < / b >',
'st_date_realized'=>'成绩的<b>日期： < / b >',
'st_date_to_do'=>'<b>估算的日期： < / b >',
'st_description'=>'描述：',
'st_er'=>'转口',
'test_date_to_do'=>'<b>估算的日期< / b >',
'tt_add_and_edit_revision_and_test_control'=>'添加或修改控制测试和修改',
'tt_control_efficiency_history_edit_title'=>'编辑控制修订',
'tt_control_selected_revision_history'=>'控制修订历史',
'tt_control_selected_test_history'=>'控制测试的历史',
'tt_control_test_history_edit_title'=>'编辑控制测试',
'vb_insert'=>'插入',

/* './packages/risk/popup_control_revision_task.php' */

'tt_control_revision_task_incident_revision'=>'诱导的测量控制效率（ ％事件incident_name ％ ）',

/* './packages/risk/popup_control_revision_task.xml' */

'lb_from'=>'自',
'lb_start_cl'=>'开始：',
'lb_to'=>'至',
'lb_value_1_cl'=>'价值1 ：',
'lb_value_2_cl'=>'价值2 ：',
'lb_value_3_cl'=>'价值三：',
'lb_value_4_cl'=>'价值4 ：',
'lb_value_5_cl'=>'价值5 ：',
'st_ee'=>'电子工程专辑',
'st_er_bl'=>'的<b>重新< / b >',
'st_high_parenthesis'=>'（高）',
'st_low_parenthesis'=>'（低）',
'st_metric'=>'公制',
'st_total_incidents_found_on_period'=>'总的事件，发现在这段期间：',
'st_total_incidents_on_time'=>'总的事件，在目前的时间框架：',
'to_expected_efficiency'=>'预期效益',
'to_real_efficiency'=>'真正的效率',
'tt_control_revision_task'=>'控制检讨',
'tt_incidents_occurred_from_period'=>'事件发生从％ date_begin ％至％ date_today ％',
'vb_filter'=>'过滤器',

/* './packages/risk/popup_control_risk_edit.php' */

'tt_risk_control_association_editing'=>'风险控制协会编辑',

/* './packages/risk/popup_control_risk_edit.xml' */

'tt_risks_bl'=>'风险的<b> < / b >',
'vb_associate_risk'=>'副风险',
'vb_disassociate_risks'=>'不赞同的风险',

/* './packages/risk/popup_control_risk_search.xml' */

'gc_asset'=>'资产',
'tt_risks_search'=>'风险搜索',
'vb_associate_risks'=>'副风险',
'wn_no_risk_selected'=>'是没有选择的风险。',

/* './packages/risk/popup_control_search.xml' */

'vb_associate'=>'副',

/* './packages/risk/popup_control_test.xml' */

'lb_schedule_bl_cl'=>'<b>排程< / b >',
'tt_control_test_revision'=>'控制测试版',

/* './packages/risk/popup_control_test_task.xml' */

'cb_no'=>'否',
'cb_open_next_task'=>'开放的下一个任务',
'cb_yes'=>'是',
'lb_description_test'=>'测试说明：',
'lb_observations_cl'=>'意见：',
'lb_test_ok_bl_cl'=>'<b>测试确定： < / b >',
'tt_control_test'=>'控制测试',

/* './packages/risk/popup_create_risks_from_category.xml' */

'tt_risk_creation_from_category'=>'风险创造，从一事件类别',

/* './packages/risk/popup_create_risks_from_events.xml' */

'st_select_events_not_to_associate'=>'选择下面只有你们组织的活动<b>不要< / b >要涉及到的资产！',
'tt_risk_creation_from_events'=>'风险创造活动',
'vb_relate_events_to_asset'=>'链接活动，以资产',

/* './packages/risk/popup_process_asset_association.xml' */

'tt_assets_search'=>'资产搜索',
'tt_current_assets_bl'=>'<b>当前资产< / b >',

/* './packages/risk/popup_process_edit.php' */

'st_no_published_documents'=>'有没有发表的文件',
'tt_process_adding'=>'过程中加入',
'tt_process_editing'=>'编辑过程',

/* './packages/risk/popup_process_edit.xml' */

'lb_area_bl_cl'=>'的<b>地区： < / b >',
'lb_document_cl'=>'文件：',
'lb_name_bl_cl'=>'<b>名称： < / b >',
'lb_priority_cl'=>'优先考虑：',
'lb_responsible_bl_cl'=>'的<b>负责人： < / b >',
'vb_associate_users'=>'副用户',
'vb_properties'=>'性能',
'vb_view'=>'查看',

/* './packages/risk/popup_process_search.xml' */

'gc_area'=>'面积',
'lb_area_cl'=>'地区：',
'si_all_areas'=>'所有领域',
'tt_current_processes_bl'=>'<b>当前进程< / b >',
'tt_processes_search'=>'搜索过程',
'vb_remove'=>'删除',

/* './packages/risk/popup_risk_control_association_edit.php' */

'st_control_not_mitigating_efficiency_message'=>'控制是不会减少的风险，因为真正的效率是低于预期的效率！',
'st_control_not_mitigating_not_implemented_message'=>'控制是不会减少的风险，因为它是没有执行，并限期实施日期已经过期！',

/* './packages/risk/popup_risk_control_association_edit.xml' */

'lb_control_cl'=>'控制：',
'st_control_parametrization_bl_cl'=>'<b>控制估算： < / b >',
'st_values_to_reduce_risk_consequence_and_probability'=>'价值观对的一方将确定多少，这将减少控制的后果和可能性的实际风险。',
'tt_risk_reduction'=>'减少风险',

/* './packages/risk/popup_risk_control_edit.php' */

'tt_control_risk_association_editing'=>'风险控制协会编辑',

/* './packages/risk/popup_risk_control_edit.xml' */

'lb_risk_cl'=>'风险：',
'mi_edit_association'=>'编辑协会',
'st_residual'=>'剩余',
'st_risk'=>'风险',
'tt_controls_bl'=>'<b>控制< / b >',
'vb_disassociate_controls'=>'解除管制',

/* './packages/risk/popup_risk_control_search.xml' */

'cb_search_only_suggested_controls'=>'仅搜索建议管制',
'gc_name'=>'名字',
'lb_name_cl'=>'名称：',
'tt_control_search'=>'控制搜索',
'vb_associate_control'=>'副控制',
'wn_no_control_selected'=>'是没有选定的控制。',

/* './packages/risk/popup_risk_edit.php' */

'st_sensitive_data_reapproval'=>'你正在发生变化敏感的数据，需要reapproval 。你是否想确认更改？',
'tt_reapproval'=>'reapproval',
'tt_risk_editing'=>'风险编辑',

/* './packages/risk/popup_risk_edit.xml' */

'lb_asset_bl_cl'=>'资产的<b> ： < / b >',
'lb_event_bl_cl'=>'<b>活动： < / b >',
'lb_impact_cl_money_symbol'=>'财政方面的影响：',
'lb_real_risk_bl_cl'=>'<b>可能的风险： < / b >',
'lb_residual_risk_bl_cl'=>'的<b>残余风险： < / b >',
'lb_type_cl'=>'类型：',
'si_select_one_of_the_items_below'=>'选择的项目之一，下面',
'st_risk_parametrization_bl_cl'=>'的<b>风险估计： < / b >',
'static_static19'=>'影响：',
'vb_calculate_risk'=>'计算风险',
'vb_config'=>'配置',
'vb_find'=>'查找',
'vb_probability_automatic_calculation'=>'自动计算的可能性',
'warning_wn_asset_desparametrized'=>'这是不可能计算风险价值，因为资产相关的风险是没有估计！',
'warning_wn_control_desparametrized'=>'至少有一个控制，是不是估计。之前，计算残余风险，估计管制（ ）与此相关的风险！',

/* './packages/risk/popup_risk_insert_search_event_asset.xml' */

'gc_category'=>'类别',
'gc_description'=>'描述',
'gc_impact'=>'影响',
'lb_asset_cl'=>'资产：',
'lb_category_cl'=>'类别：',
'lb_description_cl'=>'描述：',
'si_all_categories'=>'所有类别',
'tt_events_search'=>'活动搜索',
'vb_create_risk'=>'建立风险',
'vb_search'=>'搜索',
'wn_no_event_selected'=>'是没有选定的事件。',

/* './packages/risk/popup_risk_insert_type.xml' */

'rb_create_independent_risk'=>'建立一个独立的风险',
'st_which_action_to_take'=>'行动你是否打算走？',
'tt_risk_adding'=>'风险加入',
'vb_cancel'=>'取消',
'vb_create'=>'创建',
'vb_create_risk_from_event'=>'建立风险从一个事件（推荐）',

/* './packages/risk/popup_risk_parameters_weight.xml' */

'tt_risk_parameters_weight'=>'风险参数重量',

/* './packages/risk/popup_risk_parametrization.php' */

'mx_probability'=>'的可能性',
'st_denied_permission_to_estimate_risks'=>'您没有权限估计数的风险',
'to_impact_cl'=>'影響',
'to_risk_cl'=>'風險',

/* './packages/risk/popup_risk_parametrization.xml' */

'lb_all_bl_cl'=>'<b>所有： < / b >',
'tt_risk_parametrization'=>'风险估计',

/* './packages/risk/popup_risk_treatment.php' */

'tt_avoid'=>'避免',
'tt_hold_accept'=>'保留/接受',
'tt_transfer'=>'转让',

/* './packages/risk/popup_risk_treatment.xml' */

'lb_justification_bl_cl'=>'的<b>的原因是： < / b >',
'st_accept_transfer_or_avoid_risk'=>'你是否打算到<b> ％ treatment_type ％ </b>风险\'的<b> ％ risk_name ％ < / b > “ ？',
'tt_risk_treatment'=>'风险治疗（ treatment_type的<b> ％ ％ < / b > ）',
'vb_save'=>'保存',

/* './packages/risk/popup_risks_control_association_edit.xml' */

'lb_all_risks_bl_cl'=>'<b>所有的风险： < / b >',
'lb_control_bl_cl'=>'<b>控制： < / b >',
'tt_risk_control_association_parametrization'=>'减少风险',

/* './packages/risk/report/clock.php' */

'st_generating_report'=>'生成的报告...',
'st_operation_wait'=>'的<b>注意事项： < / b >这份报告可能需要几分钟的时间去创造。 <br>请，请等待。',

/* './packages/risk/report/loading_report.php' */

'st_verifying_existing_reports'=>'核实现有的报告。',

/* './packages/risk/report/popup_report_activities.xml' */


/* './packages/risk/report/popup_report_areas.php' */

'mx_areas'=>'地区',

/* './packages/risk/report/popup_report_areas.xml' */

'rs_header_area_name'=>'名字',
'rs_header_area_responsible'=>'负责',

/* './packages/risk/report/popup_report_areas_by_priority.php' */

'rs_areas_by_priority'=>'地区优先',

/* './packages/risk/report/popup_report_areas_by_type.php' */

'rs_areas_by_type'=>'地区类型',

/* './packages/risk/report/popup_report_areas_without_processes.php' */

'rs_areas_without_processes'=>'地区无进程',

/* './packages/risk/report/popup_report_asa.xml' */


/* './packages/risk/report/popup_report_asset.php' */

'rs_assets'=>'资产',

/* './packages/risk/report/popup_report_asset.xml' */


/* './packages/risk/report/popup_report_asset_deps.php' */

'si_assets_deps'=>'资产-属地及其家属',
'si_dependencies'=>'相依',
'si_dependents'=>'家属',

/* './packages/risk/report/popup_report_asset_importance.php' */

'rs_assets_importance'=>'资产的重要性',

/* './packages/risk/report/popup_report_asset_relevance.php' */

'rs_asset_cl_bl'=>'<b>资产:</b>',
'si_asset_relevance'=>'资产关联',

/* './packages/risk/report/popup_report_asset_relevance.xml' */

'rs_responsible_cl'=>'负责人：',

/* './packages/risk/report/popup_report_assets.php' */

'mx_assets'=>'资产',

/* './packages/risk/report/popup_report_assets.xml' */

'rs_header_asset_name'=>'名字',
'rs_header_asset_responsible'=>'负责',

/* './packages/risk/report/popup_report_assets_without_risks.php' */

'rs_assets_without_risks'=>'资产风险事件',

/* './packages/risk/report/popup_report_assets_without_risks.xml' */

'rs_security_responsible'=>'安全负责',

/* './packages/risk/report/popup_report_best_practices.php' */

'mx_best_practices'=>'最佳做法',

/* './packages/risk/report/popup_report_best_practices.xml' */

'rs_best_practice_name'=>'名字',

/* './packages/risk/report/popup_report_bia_quantitative.xml' */


/* './packages/risk/report/popup_report_conformity.php' */

'rs_applied_best_practices'=>'最佳做法的应用',
'rs_not_applied_best_practices'=>'最佳做法并不适用',
'rs_not_used'=>'不使用',
'rs_not_used_controls'=>'管制不使用',
'rs_report_conformity_footer'=>'最佳做法的总= ％总计％的最佳做法适用于总= ％应用％',
'rs_used_controls'=>'使用管制',
'tt_conformity'=>'符合',

/* './packages/risk/report/popup_report_conformity.xml' */

'rs_standard_cl'=>'标准：',

/* './packages/risk/report/popup_report_control_accompaniment.php' */

'rs_follow_up_of_controls'=>'跟进的管制',

/* './packages/risk/report/popup_report_control_cost.php' */

'rs_cost_per_control'=>'成本控制',

/* './packages/risk/report/popup_report_control_cost_by_area.php' */

'rs_cost_of_controls_per_area'=>'成本控制区',

/* './packages/risk/report/popup_report_control_cost_by_asset.php' */

'rs_cost_of_controls_per_asset'=>'成本控制的资产',

/* './packages/risk/report/popup_report_control_cost_by_cost_category.php' */

'mx_control_cost_by_cost_category'=>'控制成本费用类',

/* './packages/risk/report/popup_report_control_cost_by_cost_category.xml' */

'rs_control_cost'=>'成本',
'rs_control_name'=>'名字',
'rs_total_cost'=>'总',

/* './packages/risk/report/popup_report_control_cost_by_process.php' */

'rs_cost_of_controls_per_process'=>'成本控制的过程',

/* './packages/risk/report/popup_report_control_cost_by_responsible.php' */

'rs_cost_of_controls_per_responsible'=>'成本控制负责',

/* './packages/risk/report/popup_report_control_cost_by_responsible.xml' */

'rs_cost'=>'成本',

/* './packages/risk/report/popup_report_control_efficiency.php' */

'rs_controls_efficiency'=>'管制效率',
'rs_efficient_controls'=>'高效率的管制',
'rs_inefficient_controls'=>'低效率的管制',

/* './packages/risk/report/popup_report_control_efficiency_accompaniment.php' */

'rs_follow_up_of_control_efficiency'=>'跟进的管制效率',

/* './packages/risk/report/popup_report_control_efficiency_accompaniment.xml' */

'rs_ee'=>'电子工程专辑',
'rs_re'=>'转口',

/* './packages/risk/report/popup_report_control_implementation_date.php' */

'rs_controls_implemented_outside_stated_period'=>'管制措施实施期间以外的说明',
'rs_controls_implemented_within_stated_period'=>'管制的实施，说明期',
'rs_date_of_implementation_of_controls'=>'管制的实施日期',

/* './packages/risk/report/popup_report_control_implementation_date.xml' */

'rs_implementation'=>'执行',

/* './packages/risk/report/popup_report_control_planning.php' */

'rs_implementation_date'=>'实施日期',
'rs_implementation_deadline'=>'实施期限',
'rs_implemented_controls'=>'实施管制',
'rs_planned_controls'=>'计划控制',
'rs_planning_of_controls'=>'规划管制',

/* './packages/risk/report/popup_report_control_revision_agenda_by_responsible.php' */

'si_control_revision_agenda_by_responsible'=>'控制的修订议程负责',

/* './packages/risk/report/popup_report_control_revision_agenda_by_responsible.xml' */

'rs_next_revision'=>'明年修订',

/* './packages/risk/report/popup_report_control_summary.xml' */

'rs_best_practice'=>'最佳做法',

/* './packages/risk/report/popup_report_control_summary_pm.php' */

'rs_summary_of_controls'=>'综述管制',

/* './packages/risk/report/popup_report_control_summary_pm.xml' */

'rs_accomplishment_bl_cl'=>'的<b>成绩： < / b >',
'rs_description_cl'=>'<b>说明： < / b >',

/* './packages/risk/report/popup_report_control_test_accompaniment.php' */

'rs_ctrl_test_not_ok'=>'不能确定',
'rs_ctrl_test_ok'=>'好的',
'rs_follow_up_of_control_test'=>'跟进的管制测试',

/* './packages/risk/report/popup_report_control_test_accompaniment.xml' */

'rs_comment'=>'评论',
'rs_control_cl'=>'控制：',
'rs_date_accomplishment'=>'完成日期',
'rs_predicted_date'=>'预测日期',
'rs_test'=>'测试',

/* './packages/risk/report/popup_report_control_test_agenda_by_responsible.php' */

'si_control_test_agenda_by_responsible'=>'控制的试验议程负责',

/* './packages/risk/report/popup_report_control_test_agenda_by_responsible.xml' */

'rs_next_test'=>'下一个考验',
'rs_processes'=>'进程',
'rs_responsibles'=>'负责',
'rs_schedule'=>'调度',

/* './packages/risk/report/popup_report_controls.php' */

'mx_controls'=>'管制',

/* './packages/risk/report/popup_report_controls.xml' */

'rs_header_control_name'=>'名字',
'rs_header_control_responsible'=>'负责',

/* './packages/risk/report/popup_report_controls_by_responsible.php' */

'rs_controls_per_responsible'=>'管制由负责',

/* './packages/risk/report/popup_report_controls_by_risk.php' */

'rs_controls_per_risk'=>'控制风险',

/* './packages/risk/report/popup_report_controls_by_risk.xml' */

'rs_affected_asset_bl_cl'=>'受影响的<b>资产： < / b >',
'rs_risk_and_impact_bl_cl'=>'<b>风险/影响</b>',
'rs_risk_bl_cl'=>'的<b>风险： < / b >',

/* './packages/risk/report/popup_report_controls_by_type.php' */

'rs_controls_by_type'=>'控制类型',

/* './packages/risk/report/popup_report_controls_not_measured.php' */

'rs_not_measured_controls'=>'没有计量控制',

/* './packages/risk/report/popup_report_controls_not_measured.xml' */

'rs_efficiency'=>'效率',

/* './packages/risk/report/popup_report_controls_without_risks.php' */

'rs_controls_without_risks'=>'管制没有相关的风险',

/* './packages/risk/report/popup_report_detailed_pendant_tasks.php' */

'rs_detailed_pending_tasks'=>'详细尚未完成的任务',

/* './packages/risk/report/popup_report_duplicated_risks.php' */

'tt_duplicated_risks'=>'重复的风险',

/* './packages/risk/report/popup_report_duplicated_risks.xml' */

'rs_risk_cl'=>'风险：',

/* './packages/risk/report/popup_report_duplicated_risks_by_asset.php' */

'tt_duplicated_risks_by_asset'=>'重复的风险资产',

/* './packages/risk/report/popup_report_duplicated_risks_by_asset.xml' */

'rs_risk'=>'风险',

/* './packages/risk/report/popup_report_events_by_category.php' */

'rs_checklist_by_event'=>'核对资产',

/* './packages/risk/report/popup_report_events_by_category.xml' */

'rs_brackets'=>'[ ]',
'rs_event'=>'事件',
'rs_event_category_cl'=>'类别：',
'rs_na'=>'娜',
'rs_nc'=>'数控',
'rs_ok'=>'好的',

/* './packages/risk/report/popup_report_events_by_type.php' */

'rs_events_by_type'=>'活动类型',

/* './packages/risk/report/popup_report_pendant_contexts.php' */

'rs_pending_items'=>'未决事项',

/* './packages/risk/report/popup_report_pendant_contexts.xml' */

'rs_approver'=>'审批',

/* './packages/risk/report/popup_report_places.xml' */


/* './packages/risk/report/popup_report_policy.php' */

'rs_sgsi_policy_statement'=>'主义的政策声明',

/* './packages/risk/report/popup_report_policy.xml' */

'rs_system_policy'=>'制度政策',

/* './packages/risk/report/popup_report_process_by_process.xml' */


/* './packages/risk/report/popup_report_processes.php' */

'mx_processes'=>'进程',

/* './packages/risk/report/popup_report_processes.xml' */

'rs_header_process_name'=>'名字',
'rs_header_process_responsible'=>'负责',

/* './packages/risk/report/popup_report_processes_by_priority.php' */

'rs_processes_by_priority'=>'程序优先',

/* './packages/risk/report/popup_report_processes_by_priority.xml' */

'rs_priority_cl'=>'优先考虑：',

/* './packages/risk/report/popup_report_processes_by_type.php' */

'rs_processes_by_type'=>'过程类型',

/* './packages/risk/report/popup_report_processes_without_assets.php' */

'rs_processes_without_assets'=>'过程中没有资产',

/* './packages/risk/report/popup_report_risk_amount_by_area.php' */

'rs_amount_of_risks_per_area'=>'数额的风险区',

/* './packages/risk/report/popup_report_risk_amount_by_process.php' */

'rs_amount_of_risks_per_process'=>'数额的风险进程',

/* './packages/risk/report/popup_report_risk_financial_impact.php' */

'mx_risk_financial_impact'=>'风险的金融影响',

/* './packages/risk/report/popup_report_risk_financial_impact.xml' */

'rs_risk_impact'=>'风险影响',
'rs_risk_name'=>'名字',
'rs_risk_residual_value'=>'残余风险',
'rs_risk_value'=>'潜在风险',
'rs_total_impact'=>'总',

/* './packages/risk/report/popup_report_risk_status_by_area.php' */

'rs_risk_status_per_area'=>'风险状况按地区',

/* './packages/risk/report/popup_report_risk_status_by_area.xml' */

'rs_area'=>'面积',

/* './packages/risk/report/popup_report_risk_status_by_process.php' */

'rs_risk_status_per_process'=>'风险地位的过程中',

/* './packages/risk/report/popup_report_risk_status_by_process.xml' */

'rs_process'=>'过程',

/* './packages/risk/report/popup_report_risk_treatment_plan.php' */

'rs_and'=>'和',
'rs_avoided'=>'避免',
'rs_not_treated'=>'不治疗',
'rs_plan_of_risks_treatment'=>'风险处理计划',
'rs_reduced'=>'减少',
'rs_restrained'=>'保留',
'rs_transferred'=>'转移',

/* './packages/risk/report/popup_report_risk_treatment_plan.xml' */

'rs_asset_bl_cl'=>'资产的<b> ： < / b >',
'rs_deadline'=>'截止日期',
'rs_status'=>'身份',

/* './packages/risk/report/popup_report_risk_values.php' */

'rs_risks_values'=>'风险值',

/* './packages/risk/report/popup_report_risk_values.xml' */

'rs_affected_asset'=>'受影响的资产',

/* './packages/risk/report/popup_report_risks.php' */

'mx_risks'=>'风险',

/* './packages/risk/report/popup_report_risks_by_area.php' */

'rs_risks_per_area'=>'风险区',

/* './packages/risk/report/popup_report_risks_by_area_and_process_asset.xml' */

'rs_asset_cl'=>'资产：',
'rs_process_cl'=>'过程：',

/* './packages/risk/report/popup_report_risks_by_asset.php' */

'rs_risks_per_asset'=>'风险资产',

/* './packages/risk/report/popup_report_risks_by_control.php' */

'rs_report_of_risk_per_control'=>'风险控制',

/* './packages/risk/report/popup_report_risks_by_control.xml' */

'rs_control_bl_cl'=>'<b>控制： < / b >',

/* './packages/risk/report/popup_report_risks_by_process.php' */

'rs_risks_per_process'=>'风险进程',

/* './packages/risk/report/popup_report_risks_by_type.php' */

'rs_risks_by_type'=>'风险类型',

/* './packages/risk/report/popup_report_risks_by_type.xml' */

'rs_type_cl'=>'类型：',

/* './packages/risk/report/popup_report_scope.php' */

'rs_sgsi_scope_statement'=>'主义的范围声明',

/* './packages/risk/report/popup_report_scope.xml' */

'rs_system_scope'=>'系统范围',

/* './packages/risk/report/popup_report_statement_of_applicability.php' */

'rs_applied'=>'应用',
'rs_no_risks_application_control'=>'直至目前为止没有任何风险，已经确定了其中的理由的应用控制',
'rs_not_applied'=>'不适用',
'rs_reduction_of_risks'=>'减少风险。',
'rs_statement_of_applicability'=>'声明适用性',
'rs_statement_of_applicability_pre_report'=>'前报告的声明的适用性',

/* './packages/risk/report/popup_report_statement_of_applicability.xml' */

'rs_applicability_cl'=>'适用于：',
'rs_best_practice_cl'=>'最佳做法：',
'rs_control'=>'控制',
'rs_document'=>'文件',
'rs_justification_cl'=>'原因是：',

/* './packages/risk/report/popup_report_statement_of_applicability_justificative.xml' */

'tt_justification'=>'原因',
'vb_close'=>'关闭',
'vb_confirm'=>'确认',

/* './packages/risk/report/popup_report_summarized_pendant_tasks.php' */

'rs_summarized_pending_tasks'=>'总结了尚未完成的任务',

/* './packages/risk/report/popup_report_summarized_pendant_tasks.xml' */

'rs_amount'=>'数量',
'rs_tasks'=>'任务',

/* './packages/risk/report/popup_report_tests.xml' */


/* './packages/risk/report/popup_report_top10_assets_with_more_risks.php' */

'rs_top_10_assets_with_highest_risks'=>'前10名的资产与最高风险',
'rs_top_10_assets_with_most_risks'=>'前10名的资产与更多的风险',

/* './packages/risk/report/popup_report_top10_assets_with_more_risks.xml' */

'rs_amount_of_risks'=>'数额的风险',
'rs_asset'=>'资产',
'rs_responsible'=>'负责',

/* './packages/risk/report/popup_report_top10_risks_by_area.php' */

'rs_top_10_risks_per_area'=>'十大风险区',

/* './packages/risk/report/popup_report_top10_risks_by_area.xml' */

'rs_area_bl_cl'=>'的<b>地区： < / b >',

/* './packages/risk/report/popup_report_top10_risks_by_process.php' */

'rs_top_10_risks_per_process'=>'十大风险进程',

/* './packages/risk/report/popup_report_top10_risks_by_process.xml' */

'rs_process_bl_cl'=>'的<b>过程： < / b >',
'rs_real_risk'=>'潜在风险',
'rs_residual_risk'=>'残余风险',
'rs_risk_and_impact'=>'风险/影响',

/* './packages/risk/report/popup_report_user_responsabilities.php' */

'rs_security_resp'=>'RESP的安全。',
'rs_special_user'=>'特殊用户',
'rs_users_responsibilities'=>'使用者的责任',

/* './packages/risk/report/popup_report_user_responsabilities.xml' */

'rs_responsible_bl_cl'=>'的<b>负责人： < / b >',

/* './packages/risk/report/report_template.xml' */

'rs_comment_cl'=>'评论：',
'rs_end_of_report'=>'年底的报告',

/* './packages/risk/tab_risk_management.xml' */

'ti_area'=>'面积',
'ti_asset'=>'资产',
'ti_controls'=>'管制',
'ti_process'=>'过程',
'ti_reports'=>'报告',
'ti_risk'=>'风险',

/* './popup_choose_role.php' */

'mx_user_doesnt_have_permission_to_login'=>'用户的<b> ％用户名％ “ / b ”没有权限来存取系统。',
'tt_login_error'=>'系统登录失败',

/* './popup_choose_role.xml' */

'lb_represent_bl_cl'=>'的<b>代表： < / b >',
'st_solicitor_explanation'=>'如果你想代表任何这些用户，按下OK 。',
'tt_solicitor_mode'=>'检察模式',

/* './popup_email_preferences.xml' */

'cb_action_plan'=>'行动计划',
'cb_areas'=>'地区',
'cb_assets'=>'资产',
'cb_best_practices'=>'最佳做法',
'cb_controls'=>'管制',
'cb_documents'=>'文件',
'cb_events'=>'事件',
'cb_incidents'=>'事件',
'cb_non_conformities'=>'不符合',
'cb_occurrences'=>'发生',
'cb_others'=>'其他',
'cb_processes'=>'进程',
'cb_risks'=>'风险',
'cb_select_all'=>'选择所有',
'cb_standards'=>'标准',
'lb_warning_types_to_receive_cl'=>'选择警告您想要接收的：',
'rb_html_email'=>'HTML电子邮件',
'rb_messages_digest_complete'=>'完成（ 1每日电子邮件与充分的讯息）',
'rb_messages_digest_none'=>'无（ 1电子邮件每封邮件）',
'rb_messages_digest_subjects'=>'科目（一每日电子邮件与讯息，只有科目）',
'rb_text_email'=>'纯文字电子邮件',
'st_email_preferences_bl'=>'<b>电子邮件偏好< / b >',
'st_messages_digest_type_cl'=>'邮件消化类型：',

/* './popup_help.xml' */

'cb_do_not_show_again'=>'不要再显示此再次',
'st_tip_text'=>'<b>何时访问axur主义表，请尝试我们的上下文菜单。 < / b > <br/> <br/>编辑，删除或链接到其他axur主义组件，单击与权利滑鼠键，结果，在为了查看弹出式菜单中与现有的行动。',
'tt_tip_bl'=>'<b>提示< / b >',

/* './popup_password_request.xml' */

'lb_user_login_cl'=>'用户名：',
'st_password_request'=>'获取一个新的密码时，请输入您的用户。一个新的密码将发送到您的电子邮箱。',
'st_warning_general_email_disabled'=>'该系统的电子邮件是残疾人士，因此它是不可能产生一个新密码。',
'tt_password_reminder'=>'新的密码要求',
'vb_new_password'=>'新密码',
'wn_password_sent'=>'密码被发送给用户，只要用户存在。',

/* './popup_risk_limits_task_view.xml' */

'lb_risk_limits_high_cl'=>'风险承受能力2 ：',
'lb_risk_limits_low_cl'=>'风险承受能力1 ：',
'tt_risk_limits_approval'=>'批准的风险限额',

/* './popup_saas_disclaimer.xml' */

'tt_disclaimer_popup_title'=>'免责声明',
'vb_agree'=>'我同意',
'vb_disagree'=>'我不同意',

/* './popup_schedule_edit.xml' */

'lb_april'=>'4月',
'lb_august'=>'8月',
'lb_december'=>'12月',
'lb_february'=>'二月',
'lb_january'=>'一月',
'lb_july'=>'7月',
'lb_june'=>'6月',
'lb_march'=>'三月',
'lb_may'=>'可能',
'lb_november'=>'十一月',
'lb_october'=>'10月',
'lb_schedule_days_to_finish'=>'天要完成的任务：',
'lb_schedule_end'=>'完成于：',
'lb_schedule_periodicity'=>'每个的',
'lb_schedule_start'=>'开始：',
'lb_schedule_type_cl'=>'类型：',
'lb_september'=>'9月',
'lb_week_days'=>'对下列天：',
'mx_first_week'=>'首先',
'mx_fourth_week'=>'提出',
'mx_last_week'=>'最后的',
'mx_monday'=>'星期一',
'mx_saturday'=>'周六',
'mx_second_week'=>'第二',
'mx_sunday'=>'星期日',
'mx_third_week'=>'第三',
'mx_thursday'=>'星期四',
'mx_tuesday'=>'星期二',
'mx_wednesday'=>'周三',
'si_schedule_by_day'=>'每日',
'si_schedule_by_month'=>'每月',
'si_schedule_by_week'=>'每周',
'si_schedule_month_day'=>'一个月天',
'si_schedule_week_day'=>'周天',
'st_months'=>'个月',
'st_never'=>'从不',
'st_schedule_months'=>'在随后的几个月里：',
'st_weeks'=>'周',
'tt_schedule_edit'=>'任务调度',
'tx_1'=>'1',
'tx_schedule_days_to_finish'=>'1',

/* './popup_solicitor_conflict.xml' */

'st_solicitor_conflict'=>'用户的<b> ％ solicitor_name ％ < / b >已被提名你成为您的检察长对您的absency时期。你是否想结束这种联系呢？',
'tt_solicitor_conflict'=>'检察冲突',

/* './popup_survey.xml' */

'st_survey_text'=>'后评估制度，不要忘了，回答我们的质量调查。您是否可以访问它通过链接“的用户体验调查”在系统的注脚。你想不想回答，现在呢？',
'tt_survey_bl'=>'调查的<b> < / b >',

/* './popup_unexpected_behavior.xml' */

'st_unexpected_behavior_message'=>'意想不到的行为，在该系统已被侦破。 1电子邮件与详细资料，所发生的事情已发送到我们的技术支持小组。如有必要，关闭网页浏览器，并登录到该系统。',
'tt_unexpected_behavior'=>'意想不到的行为',

/* './popup_user_change_password.php' */

'st_user_password_change'=>'你必须改变你的密码。',
'st_user_password_expired'=>'<b>您的密码已过期！ < / b > <br/>你必须改变现在。',
'st_user_password_will_expire'=>'您的密码将届满， ％天％天（ ） 。如果你想要，你可以改变现在。',

/* './popup_user_change_password.xml' */

'lb_confirm_cl'=>'确认：',
'tt_change_password'=>'更改密码',
'vb_change_password'=>'更改密码',
'wn_unmatching_password'=>'该密码不匹配。',

/* './popup_user_dashboard_preference.php' */

'tt_average_time_for_documents_approval'=>'文件的平均审批时间',
'tt_average_time_for_my_documents_approval'=>'我的文件的平均审批时间',
'tt_financial_summary'=>'财务概要',
'tt_general_summary_of_risks'=>'估计与非估计风险',
'tt_grid_abnormality_ci_title'=>'不断改善异常摘要',
'tt_grid_abnormality_pm_title'=>'政策管理异常摘要',
'tt_grid_incident_summary_title'=>'事件概述',
'tt_grid_incidents_per_asset_title'=>'最常见的5个资产与更多事件',
'tt_grid_incidents_per_process_title'=>'最常见的5个流程，更多的事件',
'tt_grid_incidents_per_user_title'=>'最常见的5个用户更多的纪律程序',
'tt_grid_nc_per_process_title'=>'最常见的5个流程，更不符合',
'tt_grid_nc_summary_title'=>'不符合简易程序',
'tt_my_items'=>'我的项目',
'tt_my_pendencies'=>'我pendings从风险管理',
'tt_my_risk_summary'=>'我的风险摘要',
'tt_risk_management_my_items_x_policy_management'=>'风险管理（我的项目） ×政策管理',
'tt_risk_management_status'=>'风险水平',
'tt_risk_management_x_policy_management'=>'份文件的商业元素',
'tt_sgsi_reports_and_documents'=>'国际标准化组织27001主义强制性文件',
'tt_summary_of_abnormalities'=>'风险管理异常摘要',
'tt_summary_of_best_practices'=>'最佳做法-遵守指数',
'tt_summary_of_controls'=>'控件概述',
'tt_summary_of_my_documents_last_revision_average_time'=>'最后文件的修改时间内摘要',
'tt_summary_of_parameterized_risks'=>'潜在风险与残余风险',
'tt_summary_of_policy_management'=>'政策管理概述',
'tt_summary_of_policy_management_my_documents'=>'政策管理（我的文档）摘要',
'tt_summary_of_risk_management_documentation'=>'摘要风险管理文件',
'tt_summary_of_the_risk_management_from_my_items_documentation'=>'摘要风险管理，从我的项目文件',
'tt_top_10_most_read_documents'=>'10大最读取文件',
'tt_top_10_most_read_my_documents'=>'十大最我的文档阅读',

/* './popup_user_dashboard_preference.xml' */

'st_all_elements_not_selected'=>'所有项目（不选中）',
'st_selected_elements_cl'=>'选定项目：',
'tt_summary_preferences_editing'=>'摘要喜好编辑',

/* './popup_user_preferences.php' */

'si_email'=>'电子邮件',
'si_general'=>'一般',

/* './popup_user_preferences.xml' */

'lb_preferences_cl'=>'偏好：',
'tt_edit_preferences'=>'编辑偏好',

/* './popup_user_profile.xml' */

'cb_change_password'=>'更改密码',
'lb_current_password_cl'=>'目前的密码：',
'lb_dashboard_preferences_cl'=>'仪表板偏好：',
'lb_general_config_bl'=>'<b>一般设置< / b >',
'lb_general_config_bl_cl'=>'<b>一般的配置： < / b >',
'lb_language_cl'=>'语言：',
'lb_password_confirm_cl'=>'确认密码：',
'lb_solicitor_cl'=>'检察长：',
'lb_solicitor_finish_date_cl'=>'年底采购：',
'lb_solicitor_start_date_cl'=>'开始采购：',
'st_change_password_sub_title_bl'=>'<b>密码变更< / b >',
'st_solicitor_sub_title_bl'=>'检察长的<b> < / b >',
'to_dashboard_user_preferences_help'=>'的<b>配置的仪表板偏好： < / b > <br/> <br/>决定，以便和网格的信息将显示在您的控制台。',
'wn_email_incorrect'=>'无效的电子邮件。',
'wn_existing_password'=>'密码是在最近的历史密码表',
'wn_initial_date_after_today'=>'开始日期必须同时或之后，目前的日期。',
'wn_missing_dates'=>'日期字段是强制性的，当检察长被选中。',
'wn_missing_solicitor'=>'它是强制性的选择一个检察长时，其中一个日期，被填补。',
'wn_past_date'=>'最后的日期必须稍后或相等于目前的日期。',
'wn_wrong_dates'=>'最后日期的采购，必须不迟于它的开始日期。',

/* './popup_user_search.xml' */

'tt_user_search'=>'用户搜索',
'wn_no_user_selected'=>'有没有选取的使用者。',

/* './reports/ISMSReportTemplate.php' */

'rs_ip_cl'=>'叶：',
'rs_manager_cl'=>'经理：',

/* './session_expired.xml' */

'st_session_expired'=>'您的会话已过期。新闻<b>确定< / b >或关闭该窗口以再次登录。',
'tt_session_expired'=>'会议过期',

/* './tab_dashboard.xml' */

'ti_graphics'=>'图形',
'ti_statistics'=>'统计',
'ti_summary'=>'摘要',
'ti_warnings'=>'我的任务与快讯',

/* './tab_main.xml' */

'st_search'=>'搜索',
'ti_continual_improvement'=>'不断改善',
'ti_dashboard'=>'仪表板',
'ti_libraries'=>'图书馆',
'ti_policy_management'=>'政策管理',
'ti_risk_management'=>'风险管理',
'ti_search'=>'搜索',
'vb_admin'=>'政府当局',
'vb_go'=>'离去',

/* './visualize.php' */

'tt_visualize'=>'查看',
);
?>