<?php

include_once 'include.php';
include_once $classes_isms_ref . 'nonauto/FeedbackEvent.php';
include_once $handlers_ref . 'QueryGetProfileFromUser.php';

define('SECURITY_OFFICER_PROFILE', 1);


class LoadModalASaaS extends FWDRunnable {
  public function run(){
    echo "show_asaas_popup();";
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){

    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new FeedbackEvent('feedback_event'));
    $moStartEvent->addAjaxEvent(new LoadModalASaaS('load_modal_asaas'));
    
    if(!ISMSLib::getAdminPermission()){
      $moHeaderChangeMode = FWDWebLib::getObject('header_change_mode');
      $moHeaderUserPrefBox = FWDWebLib::getObject('header_user_pref')->getObjFWDBox();

      $moHeaderChangeMode->setShouldDraw(false);

      $miNewLeft = $moHeaderUserPrefBox->getAttrLeft() + $moHeaderChangeMode->getObjFWDBox()->getAttrWidth();
      $moHeaderUserPrefBox->setAttrLeft($miNewLeft);
    }
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $moISMSLib = ISMSLib::getInstance();

    $moConfig = new ISMSConfig();
    if($moConfig->getConfigFromDB(CLIENT_ID_ASAAS)) {
        $moISMSASaaS = new ISMSASaaS();
        $moISMSASaaS->setNewCheckPointASaaS(utf8_encode("Logou no sistema - Vis�o do Usu�rio"), utf8_encode("M�dulo Login"));
    }

    /* Warning no TOP indicando quanto tempo falta para terminar o per�odo do ASaaS */
    $moConfig = new ISMSConfig();
    $moHandlerProfileFromUser = new QueryGetProfileFromUser(FWDWebLib::getConnection());
    $moHandlerProfileFromUser->setUserId(ISMSLib::getCurrentUserId());
    $moHandlerProfileFromUser->makeQuery();
    $moHandlerProfileFromUser->executeQuery();
    $userProfileId = $moHandlerProfileFromUser->getProfileId();

    if($userProfileId==SECURITY_OFFICER_PROFILE  && $moConfig->getConfigFromDB(CLIENT_ID_ASAAS)) {
        $isTrialInstance = $moConfig->getConfigFromDB(TRIAL_INSTANCE);
        $bodyForm = $moWebLib->getObject('bodyBCMS');
        if($userProfileId == 1){
            $moISMSASaaS = new ISMSASaaS();
            $planCode = $moISMSASaaS->getPlanCode();
            if($isTrialInstance==0 || $planCode != 'TRIAL'){
                $moWebLib->getObject('warningAsaas')->setAttrDisplay("false");
                $bodyForm->getObjFWDBox()->setAttrTop(0);
            }
            else{
                $bodyForm->getObjFWDBox()->setAttrTop(40);
                $dataASaaS = $moISMSASaaS->getExpirationDate();
                $dataNow = time();
                $secs = strtotime($dataASaaS) - $dataNow;
                $days = ceil($secs / 86400);
                $moWebLib->getObject('asaas_day')->concatValue("<b> $days</b>");
                if($days > 15){
                    $moWebLib->getObject('closeAsaas')->setAttrDisplay("false");
?>

<script type="text/javascript">
var timer = setInterval( removeTopWarning, 15000);
function removeTopWarning(){
    js_hide('warningAsaas');
    js_reposition('bodyBCMS', 0, 0);
}
</script>
<?
                }

            }
        }
    }else{
        $bodyForm = $moWebLib->getObject('bodyBCMS');
        $moWebLib->getObject('warningAsaas')->setAttrDisplay("false");
        $bodyForm->getObjFWDBox()->setAttrTop(0);
    }
    /* FIM do Warning*/

    $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    $moSession->setMode(MANAGER_MODE);

    $moWebLib->getObject('user_name')->concatValue($moSession->getUserName(true));

    //  seta o link de help
    $msLink = 'http://support.realiso.com';
    $moHelpEvent = new FWDClientEvent();
    $moHelpEvent->setAttrEvent('onClick');
    $moHelpEvent->setAttrValue("goto_url('{$msLink}',true);");
    FWDWebLib::getObject('help_button')->addObjFWDEvent($moHelpEvent);

    if(in_array(ISMSSaaS::getConfig(ISMSSaaS::LICENSE_TYPE), ISMSLicense::getTrialLicenseTypes())){
      $moSaasAdmin = new ISMSSaaSAdmin();
      $msUri = ISMSLib::getBuyNowUrl();
      $moEvent = new FWDClientEvent();
      $moEvent->setAttrEvent('onClick');
      $moEvent->setAttrValue("goto_url('{$msUri}',true);");
      FWDWebLib::getObject('buy_now_link')->addObjFWDEvent($moEvent);
    }else{
      $moBuyNow = FWDWebLib::getObject('buy_now');
//      $moSurveyBox = FWDWebLib::getObject('survey')->getObjFWDBox();
      
      $moBuyNow->setShouldDraw(false);
      
//      $miNewLeft = $moSurveyBox->getAttrLeft() - $moBuyNow->getObjFWDBox()->getAttrWidth();
//      $moSurveyBox->setAttrLeft($miNewLeft);
    }
    if (in_array(ISMSSaaS::getConfig(ISMSSaaS::LICENSE_TYPE), ISMSLicense::getMinimaRiskLicenseTypes())){
//    	$moSurveyBox = FWDWebLib::getObject('survey_group');
//    	$moSurveyBox->setShouldDraw(false);
    }
    
    $msClassification = ISMSLib::getConfigById(GENERAL_SYSTEM_CLASSIFICATION);
    $msClassificationDest = ISMSLib::getConfigById(GENERAL_SYSTEM_CLASSIFICATION_DEST);
    if(trim($msClassification) && trim($msClassificationDest)){
      FWDWebLib::getObject('system_classification')->setValue("<b>{$msClassification}</b>: {$msClassificationDest}");
    }
    
    $msLastLogin = $moSession->getLastLogin();
    $msLastIP = $moSession->getLastIP();
    $msLabelLastLogin = FWDLanguage::getPHPStringValue('to_last_login_cl','�ltimo Login:');
    $msLabelIP = FWDLanguage::getPHPStringValue('to_ip_cl','IP:');
    if($msLastLogin){
      $msTooltipLastLogin = "$msLabelLastLogin $msLastLogin &nbsp; $msLabelIP $msLastIP";
      FWDWebLib::getObject('last_login')->setValue($msTooltipLastLogin);
    }else{
      FWDWebLib::getObject('last_login')->setShouldDraw(false);
    }
    
    $soLicense = new ISMSLicense();
//    $moEvent = new FWDClientEvent();
//    $moEvent->setAttrEvent('onClick');
    $msLinkConcat = str_replace(' ','_',$soLicense->getClientName()) . "_" . ISMSLib::ISMSTime();
    //XXX criar aqui uma parametriza��o para escolher a url
    $msEvent = "goto_url('" . FWDLanguage::getPHPStringValue('st_survey_link','http://www.surveymonkey.com/s.aspx?sm=Iv5Lu2xahyHtsNrd2ShHBA_3d_3d') . "?$msLinkConcat" ."',true);";
    
//    $moEvent->setAttrValue($msEvent);
//    FWDWebLib::getObject('survey')->addObjFWDEvent($moEvent);
    
    //instala a seguran�a de ACL na p�gina
    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());

    if(!FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(POLICY_MODE)){
     FWDWebLib::getObject('tab_policy_management')->setShouldDraw(false); 
    }
    
    if(!FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(INCIDENT_MODE)){
     FWDWebLib::getObject('tab_continual_improvement')->setShouldDraw(false); 
    }
    
    FWDWebLib::getObject('dialog')->setAttrDisplay('false');
    $moWebLib->dump_html(FWDWebLib::getObject('dialog'));
?>

<link href="lib/css/nonauto/realiso.css" type="text/css" rel="stylesheet">
<link href="lib/css/nonauto/reveal.css" type="text/css" rel="stylesheet">
<script src="lib/javascript/nonauto/jquery-1.8.3.min.js"  type="text/javascript"></script>
<script src="lib/javascript/nonauto/jquery.reveal.js" type="text/javascript"></script>

<div id="asaas_block_popup" class="reveal-modal"></div>
<script language="javascript">


  setISMSMode(MANAGER_MODE);
  window.onresize = center_dialog_div;
  center_dialog_div();
  js_show('dialog');
  document.body.className = 'BODYBackground';
  
  function refresh(){
    document.forms[0].submit();
  }  

  function show_asaas_popup(){
      $('#asaas_block_popup').load('asaas_block_popup.php');
      $('#asaas_block_popup').reveal(
        {closeonbackgroundclick: true}
      );
    }
</script>
<?

  }
}


FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("tab_main.xml");
?>
<!--[if lt IE 7]>
<script defer type='text/javascript' language='javascript'>
  correct_png();
</script>
<![endif]-->