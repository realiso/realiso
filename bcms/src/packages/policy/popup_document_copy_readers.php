<?php
include_once "include.php";
include_once $handlers_ref . "grid/policy/QueryGridDocumentSearch.php";

class GridDocumentSearch extends FWDDrawGrid {
  public function DrawItem(){
    switch($this->ciColumnIndex){
      case 2:
        $this->coCellBox->setAttrStringNoEscape("true");
        $this->coCellBox->setValue("<a href='javascript:open_visualize(".$this->caData[1].",".CONTEXT_DOCUMENT.",\"".uniqid()."\")'>{$this->caData[2]}</a>");
        return $this->coCellBox->draw();
        break;
      
      default:
        return parent::drawItem();
      break;
    }
  }
}

class SearchDocumentEvent extends FWDRunnable {
  public function run() {
    $moGrid = FWDWebLib::getObject('grid_document_search');
    $moGrid->execEventPopulate();
  }
}

class CopyReadersEvent extends FWDRunnable {
  public function run() {
    $miDocumentId = FWDWebLib::getObject('document_id')->getValue();
    $msDocuments = FWDWebLib::getObject('selected_documents')->getValue();
    $maDocuments = explode(",",$msDocuments);
    $maUsers=array();
    
    $moPMDocument = new PMDocument();
    $moPMDocument->testPermissionToManage($miDocumentId);
    
    foreach($maDocuments as $miSelectedDocumentId) {
      $moPMDocumentReader = new PMDocumentReader();
      $moPMDocumentReader->createFilter($miSelectedDocumentId,'document_id');
      $moPMDocumentReader->select();
      while($moPMDocumentReader->fetch()) {
        $miUserId=$moPMDocumentReader->getFieldValue('user_id');
        $mbDenied=$moPMDocumentReader->getFieldValue('denied');
        if (!$mbDenied && !in_array($miUserId,$maUsers)) {
          $maUsers[]=$miUserId;
        }
      }
    }
    foreach($maUsers as $miUserId) {
      $moPMDocumentReader = new PMDocumentReader();
      $moPMDocumentReader->createFilter($miDocumentId,'document_id');
      $moPMDocumentReader->createFilter($miUserId,'user_id');
      $moPMDocumentReader->select();
      if (!$moPMDocumentReader->fetch()) {
        $moPMDocumentReader->setFieldValue('document_id',$miDocumentId);
        $moPMDocumentReader->setFieldValue('user_id',$miUserId);
        $moPMDocumentReader->setFieldValue('manual',true);
        $moPMDocumentReader->insert();
      }
    }
    echo "soWindow = soPopUpManager.getPopUpById('popup_document_copy_readers').getOpener();
          if (soWindow.refresh_grid) soWindow.refresh_grid();
          soWindow = null;
          soPopUpManager.closePopUp('popup_document_copy_readers');";
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new SearchDocumentEvent("search_document"));
    $moStartEvent->addAjaxEvent(new CopyReadersEvent("copy_readers"));
    
    $miDocumentId = FWDWebLib::getObject("param_document_id")->getValue();
    
    FWDWebLib::getObject("document_id")->setValue($miDocumentId);
    
    $moGrid = FWDWebLib::getObject("grid_document_search");
    $moHandler = new QueryGridDocumentSearch(FWDWebLib::getConnection());
    $moHandler->setName(FWDWebLib::getObject("var_document_name")->getValue());
    $moHandler->setDocumentId(FWDWebLib::getObject("param_document_id")->getValue());
    $moHandler->showAllDocuments(true);
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new GridDocumentSearch());
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $miDocumentId = FWDWebLib::getObject("document_id")->getValue();
    $moPMDocument = new PMDocument();
    $moPMDocument->testPermissionToManage($miDocumentId);
    
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
?>
<script language="javascript">
  gebi('document_name').focus();
  
  function refresh_grid() {
    js_refresh_grid('grid_document_search');
  }
</script>
<?
  }
}

//testa se a licen�a utilizada permite acessar o m�dulo de documenta��o
ISMSLib::testUserPermissionToPoliceMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_document_copy_readers.xml");
?>