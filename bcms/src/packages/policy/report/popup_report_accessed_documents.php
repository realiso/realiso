<?php

set_time_limit(3600);
include_once 'include.php';

class UserLevelIterator extends FWDReportLevelIterator {

  public function __construct(){
    parent::__construct("user_id");
  }

  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msGfxRef = $moWebLib->getGfxRef();
    $msUserName = $poDataSet->getFieldByAlias("user_name")->getValue();
    $msIcon = $msGfxRef . 'icon-user.png';
    $moWebLib->getObject('user_icon')->setAttrSrc($msIcon);
    $moWebLib->getObject('user_name')->setValue($msUserName);
  }
}

class DocumentLevelIterator extends FWDReportLevelIterator {

  protected $csTypelessString;
  protected $csGfxRef;
  protected $caCacheLabels = array();

  public function __construct(){
    parent::__construct('document_id');
    $this->csTypelessString = FWDLanguage::getPHPStringValue('rs_no_type','Sem tipo');
    $this->csGfxRef = FWDWebLib::getInstance()->getGfxRef();
  }

 /**
  * Verifica se mudou o n�vel.
  *
  * <p>M�todo para verificar se houve mudan�a de n�vel.</p>
  * @access public
  * @return boolean Verdadeiro ou falso
  */
  public function changedLevel(FWDDBDataSet $poDataSet){
    return true;
  }

  protected function getTypeLabel($piType){
    if(!isset($this->caCacheLabels[$piType])){
      if( $piType == DOCUMENT_TYPE_MANAGEMENT ) {
        $this->caCacheLabels[$piType] = FWDLanguage::getPHPStringValue('rs_management','Gest�o');
      } elseif (defined('DOCUMENT_TYPE_OTHERS') && $piType==DOCUMENT_TYPE_OTHERS) {
        $this->caCacheLabels[$piType] = FWDLanguage::getPHPStringValue('rs_others','Outros');
      } else {
        $moContext = ISMSContextObject::getContextObject($piType);
        if($moContext){
          $this->caCacheLabels[$piType] = $moContext->getLabel();
        }else{
          $this->caCacheLabels[$piType] = $this->csTypelessString;
        }
      }
    }

    return $this->caCacheLabels[$piType];
  }

  public function fetch(FWDDBDataSet $poDataSet){
    $msName = $poDataSet->getFieldByAlias('document_name')->getValue();
    $mbIsLink = $poDataSet->getFieldByAlias('doc_instance_is_link')->getValue();
    $msFileName = $poDataSet->getFieldByAlias('doc_instance_file_name')->getValue();
    $miType = $poDataSet->getFieldByAlias('document_type')->getValue();
    $msDate = $poDataSet->getFieldByAlias('access_date')->getValue();
    $msTime = date('H:i:s',strtotime($msDate));
    $msDate = ISMSLib::getISMSShortDate($msDate);
    
    if($mbIsLink){
      $msIcon = 'icon-link.png';
    }else{
      $msIcon = ISMSLib::getIcon($msFileName);
    }
    FWDWebLib::getObject('document_icon')->setAttrSrc($this->csGfxRef.$msIcon);
    FWDWebLib::getObject('document_name')->setValue($msName);
    FWDWebLib::getObject('document_type')->setValue($this->getTypeLabel($miType));
    FWDWebLib::getObject('access_date')->setValue($msDate);
    FWDWebLib::getObject('access_time')->setValue($msTime);
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    FWDWebLib::getObject('level_user')->setLevelIterator(new UserLevelIterator());
    FWDWebLib::getObject('level_document')->setLevelIterator(new DocumentLevelIterator());
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport(FWDWebLib::getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    $moComponent->setReportFilter(FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter());
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('rs_accessed_documents', "Auditoria de Acesso dos Usu�rios"));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_report_accessed_documents.xml');

?>