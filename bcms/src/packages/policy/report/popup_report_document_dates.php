<?php

set_time_limit(3600);
include_once 'include.php';

class DocumentLevelIterator extends FWDReportLevelIterator {

  protected $csGfxRef;

  public function __construct(){
    parent::__construct('doc_id');
    $this->csGfxRef = FWDWebLib::getInstance()->getGfxRef();
  }

  public function fetch(FWDDBDataSet $poDataSet){
    $msName = $poDataSet->getFieldByAlias('doc_name')->getValue();
    $mbIsLink = $poDataSet->getFieldByAlias('doc_instance_is_link')->getValue();
    $msFileName = $poDataSet->getFieldByAlias('doc_instance_file_name')->getValue();
    $msCreationDate = ISMSLib::getISMSShortDate($poDataSet->getFieldByAlias('doc_date_crated')->getValue());

    if($mbIsLink){
      $msIcon = 'icon-link.png';
    }else{
      $msIcon = ISMSLib::getIcon($msFileName);
    }
    FWDWebLib::getObject('document_icon')->setAttrSrc($this->csGfxRef.$msIcon);
    FWDWebLib::getObject('document_name')->setValue($msName);
    FWDWebLib::getObject('document_creation_date')->setValue($msCreationDate);
  }

}

class RevisionLevelIterator extends FWDReportLevelIterator {

  public function __construct(){
    parent::__construct('doc_date_revision');
  }

  public function fetch(FWDDBDataSet $poDataSet){
    $msDateRevision = ISMSLib::getISMSShortDate($poDataSet->getFieldByAlias('doc_date_revision')->getValue());
    //$msJustification = $poDataSet->getFieldByAlias('doc_revision_justification')->getValue();
    if (ISMSLib::getConfigById(GENERAL_DOCUMENTS_MANUAL_VERSIONING) && $poDataSet->getFieldByAlias('document_version')->getValue()) {
      $msVersion = $poDataSet->getFieldByAlias('document_version')->getValue();
    } else {
      $msVersion = $poDataSet->getFieldByAlias('document_major_version')->getValue().".".$poDataSet->getFieldByAlias('document_revision_version')->getValue();
    }
    FWDWebLib::getObject('revision_date')->setValue($msDateRevision);
    FWDWebLib::getObject('revision_version')->setValue($msVersion);
  }

}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    FWDWebLib::getObject('level_document')->setLevelIterator(new DocumentLevelIterator());
    FWDWebLib::getObject('level_revision')->setLevelIterator(new RevisionLevelIterator());
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport(FWDWebLib::getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    
    $moComponent->setReportFilter(FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter());
    
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('rs_documents_dates','Data de Cria��o e Revis�o dos Documentos'));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_report_document_dates.xml');

?>