<?php

set_time_limit(3600);
include_once 'include.php';

class DocumentLevelIterator extends FWDReportLevelIterator {
  
  protected $csGfxRef;
  
  public function __construct(){
    parent::__construct('document_id');
    
    $this->csGfxRef = FWDWebLib::getInstance()->getGfxRef();
  }
  
  public function fetch(FWDDBDataSet $poDataSet){
    FWDWebLib::getObject('document_name')->setValue($poDataSet->getFieldByAlias('document_name')->getValue());
    FWDWebLib::getObject('responsible_name')->setValue($poDataSet->getFieldByAlias('responsible_name')->getValue());

    $mbIsLink = $poDataSet->getFieldByAlias('doc_instance_is_link')->getValue();
    $msFileName = $poDataSet->getFieldByAlias('doc_instance_file_name')->getValue();
    if($mbIsLink){
      $msIcon = 'icon-link.png';
    }else{
      $msIcon = ISMSLib::getIcon($msFileName);
    }
    FWDWebLib::getObject('document_id')->setAttrSrc($this->csGfxRef.$msIcon);
    FWDWebLib::getObject('responsible_id')->setAttrSrc($this->csGfxRef .'icon-user.gif');
  }
  
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moFilter = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter();
    
    FWDWebLib::getObject('document_level')->setLevelIterator(new DocumentLevelIterator());
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport(FWDWebLib::getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    
    $moComponent->setReportFilter($moFilter);

    FWDWebLib::getObject('header_document_name')->setValue(FWDLanguage::getPHPStringValue('rs_document_name_bl','Documento'));
    FWDWebLib::getObject('header_responsible_name')->setValue(FWDLanguage::getPHPStringValue('rs_document_main_approver_name','Responsável pelo documento'));
    
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('tt_documents_without_register','Documentos sem Registro'));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_report_documents_without_register.xml');

?>