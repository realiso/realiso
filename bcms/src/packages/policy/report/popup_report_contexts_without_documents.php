<?php

set_time_limit(3600);
include_once 'include.php';


class ContextLevelIterator extends FWDReportLevelIterator {

  protected $csGfxRef;

  public function __construct(){
    parent::__construct('context_id');
    $this->csGfxRef = FWDWebLib::getInstance()->getGfxRef();
  }

  public function fetch(FWDDBDataSet $poDataSet){
    $msContextName = $poDataSet->getFieldByAlias('context_name')->getValue();
    $miType = $poDataSet->getFieldByAlias('context_type')->getValue();
    
    $moContext = ISMSContextObject::getContextObject($miType);
    $msIcon = $moContext->getIcon();
    
    FWDWebLib::getObject('context_icon')->setAttrSrc($this->csGfxRef.$msIcon);
    FWDWebLib::getObject('context_name')->setValue($msContextName);
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moFilter = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter();
    
    FWDWebLib::getObject('level_context')->setLevelIterator(new ContextLevelIterator());
 
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport(FWDWebLib::getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    $moComponent->setReportFilter(FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter());
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('rs_items_with_without_documents','Componentes sem Documentos'));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_report_contexts_without_documents.xml');

?>