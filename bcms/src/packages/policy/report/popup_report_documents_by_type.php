<?php
set_time_limit(3600);
include_once "include.php";

class TypeLevelIterator extends FWDReportLevelIterator {  

  public function __construct(){
    parent::__construct("class_id");
  }
	
  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();    
    $moWebLib->getObject('type_name')->setValue($poDataSet->getFieldByAlias("class_name")->getValue());
  }
}

class DocumentLevelIterator extends FWDReportLevelIterator {

  public function __construct(){
    parent::__construct("doc_id");
  }

  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msGfxRef = $moWebLib->getGfxRef();
    		
		if($poDataSet->getFieldByAlias("doc_is_link")->getValue()) $msIcon = 'icon-link.png';
    else $msIcon = ISMSLib::getIcon($poDataSet->getFieldByAlias("doc_version_name")->getValue());
    
    $moWebLib->getObject('document_icon')->setAttrSrc($msGfxRef.$msIcon);
    $moWebLib->getObject('document_name')->setValue($poDataSet->getFieldByAlias("doc_name")->getValue());
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();    
    $moWebLib->getObject("level_type")->setLevelIterator(new TypeLevelIterator());
    $moWebLib->getObject("level_document")->setLevelIterator(new DocumentLevelIterator());
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport($moWebLib->getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    $moComponent->setReportFilter($moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter());
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('rs_documents_by_type', "Documentos por Tipo"));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_report_documents_by_type.xml");
?>