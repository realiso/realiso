<?php

set_time_limit(3600);
include_once 'include.php';

class DocumentLevelIterator extends FWDReportLevelIterator {

  protected $csGfxRef;

  public function __construct(){
    parent::__construct('document_id');
    $this->csGfxRef = FWDWebLib::getInstance()->getGfxRef();
  }

  public function fetch(FWDDBDataSet $poDataSet){
    $msName = $poDataSet->getFieldByAlias('document_name')->getValue();
    $mbIsLink = $poDataSet->getFieldByAlias('doc_instance_is_link')->getValue();
    $msFileName = $poDataSet->getFieldByAlias('doc_instance_file_name')->getValue();
    
    if($mbIsLink){
      $msIcon = 'icon-link.png';
    }else{
      $msIcon = ISMSLib::getIcon($msFileName);
    }
    FWDWebLib::getObject('document_icon')->setAttrSrc($this->csGfxRef.$msIcon);
    FWDWebLib::getObject('document_name')->setValue($msName);
  }
}

class RegisterLevelIterator extends FWDReportLevelIterator {

  public function __construct(){
    parent::__construct('register_id');
  }

  public function fetch(FWDDBDataSet $poDataSet){
    $moRegister = new PMRegister();
    $msName = $poDataSet->getFieldByAlias('register_name')->getValue();
    $miRegisterPeriod = $poDataSet->getFieldByAlias('register_period')->getValue();
    $miRegisterValue = $poDataSet->getFieldByAlias('register_value')->getValue();
    $msRetentionTime = $moRegister->getRetentionTimeAsString($miRegisterValue,$miRegisterPeriod);
    
    FWDWebLib::getObject('register_name')->setValue($msName);
    FWDWebLib::getObject('retention_time')->setValue($msRetentionTime);
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    FWDWebLib::getObject('level_document')->setLevelIterator(new DocumentLevelIterator());
    FWDWebLib::getObject('level_register')->setLevelIterator(new RegisterLevelIterator());
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport(FWDWebLib::getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    
    $moComponent->setReportFilter(FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter());
    
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('rs_registers_per_document','Registro por Documento'));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_report_document_registers.xml');

?>