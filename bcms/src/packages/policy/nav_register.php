<?php

include_once "include.php";
include_once $handlers_ref . "grid/policy/QueryGridRegister.php";
include_once $handlers_ref . "select/policy/QuerySelectRegisterType.php";

class FilterEvent extends FWDRunnable {
  public function run() {
    $moGrid = FWDWebLib::getObject('grid_register');
    $moGrid->execEventPopulate(); 
  }
}

class GridRegister extends FWDDrawGrid {
  public function drawItem(){
    switch($this->ciColumnIndex){
      case 3:
        $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
        $maMenuACLs = array('edit','delete','read','doc_read');
        $maAllowed = array();
        
        $mbIsResponsible = ($this->caData[8]==$miUserId);
        $mbCanRead = $this->caData[10];
        $mbCanEdit = $this->caData[9];
        if($mbIsResponsible && $mbCanEdit){
          $maAllowed[] = 'edit';
          $maAllowed[] = 'delete';
        }
        elseif($mbIsResponsible) {
          $maAllowed[] = 'delete';
          $maAllowed[] = 'read';
          if ($this->caData[11])  $maAllowed[] = 'doc_read';
        }
        elseif($mbCanRead) {
          $maAllowed[] = 'read';
          if ($this->caData[11]) 
            $maAllowed[] = 'doc_read';
        }
        
        if($mbIsResponsible) $maAllowed[] = 'edit_readers';
        
        $moACL = FWDACLSecurity::getInstance();
        $moACL->setNotAllowed(array_diff($maMenuACLs,$maAllowed));
        $moMenu = FWDWebLib::getObject('menu');
        $moGrid = FWDWebLib::getObject('grid_register');
        $msLine = $moGrid->getAttrName().'_acl_'.$this->ciRowIndex;
        FWDGridMenuACLSecurity::getInstance()->installGridMenuACL($moMenu,$moACL,$moGrid,$msLine);
      
        $miValue = $this->coCellBox->getValue();
        $miPeriod = $this->caData[7];
        $moRegister = new PMRegister();
        $this->coCellBox->setValue($moRegister->getRetentionTimeAsString($miValue,$miPeriod));
        return $this->coCellBox->draw();
      break;
      case 6:
        $moContextClassification = new ISMSContextClassification();
        $moContextClassification->fetchById($this->coCellBox->getValue());
        $msContextClassification = $moContextClassification->getFieldValue('classif_name');
        $this->coCellBox->setValue($msContextClassification);
        return $this->coCellBox->draw();
      break;
      case 7:
        $moContext = new ISMSContextObject();
        $msStatus = $moContext->getContextStateAsString($this->coCellBox->getValue());
        $this->coCellBox->setValue($msStatus);
        return $this->coCellBox->draw();
      break;
      default:
        return parent::drawItem();
        break;
    }
  }
}

class RegisterConfirmRemove extends FWDRunnable {
  public function run(){
    $msTitle = FWDLanguage::getPHPStringValue('tt_remove_register','Remover Registro');
    $msMessage = FWDLanguage::getPHPStringValue('st_remove_register_confirm',"Voc� tem certeza de que deseja remover o registro <b>%register_name%</b>?");
    
    $moPMRegister = new PMRegister();
    $moPMRegister->fetchById(FWDWebLib::getObject('selected_register_id')->getValue());
    $msRegisterName = ISMSLib::truncateString($moPMRegister->getFieldValue('register_name'), 70);
    $msMessage = str_replace("%register_name%",substr(str_replace('\\','\\\\',$msRegisterName),0,100),$msMessage);
    
    $msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
                    "soWindow = soPopUp.getOpener();" .
                    "soWindow.remove_register();";
    
    ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,50);
  }
}

class RemoveRegisterEvent extends FWDRunnable {
  public function run(){
    $miRegisterId = FWDWebLib::getObject('selected_register_id')->getValue();
    $moPMRegister = new PMRegister();
    $moPMRegister->delete($miRegisterId);
  }
}


class RegisterDocView extends FWDRunnable {
  public function run() {
    $moWebLib = FWDWebLib::getInstance();
    $miDocumentId = $moWebLib->getObject('selected_register_doc_id')->getValue();
    $miUserId = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    
    $moDocument = new PMDocument();
    $moDocument->testPermissionToRead($miDocumentId);
    $moDocument->fetchById($miDocumentId);
    $moPMDocInstance = $moDocument->getPublishedInstance();
    $miDocInstanceId = $moPMDocInstance->getFieldValue('doc_instance_id');
    
    $moReader = new PMDocumentReader();
    $moReader->createFilter($miDocumentId,'document_id');
    $moReader->createFilter($miUserId,'user_id');
    $moReader->select();
    if ($moReader->fetch()) {
      $moReader->setFieldValue('user_has_read_document',1);
      $moReader->update();
    }
    
    $moPMDocReadHistory = new PMDocReadHistory();
    $moPMDocReadHistory->setFieldValue('user_id',$miUserId);
    $moPMDocReadHistory->setFieldValue('document_id',$miDocInstanceId);
    $moPMDocReadHistory->setFieldValue('read_date',ISMSLib::ISMSTime());
    $moPMDocReadHistory->insert();
    
    $mbIsLink = (bool)$moPMDocInstance->getFieldValue('doc_instance_is_link');
    if ($mbIsLink) {
      $msURL = $moPMDocInstance->getFieldValue('doc_instance_link');
      if ($msURL)
        echo "js_open('{$msURL}','document_link',740,502,'center_screen');";
    } else {
      echo "js_submit('register_doc_download','ajax');";
    }
  }
}

class RegisterDocDownload extends FWDRunnable {
  public function run() {
    $moWebLib = FWDWebLib::getInstance();
    $miDocumentId = $moWebLib->getObject('selected_register_doc_id')->getValue();
    $miUserId = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    
    $moDocument = new PMDocument();
    $moDocument->testPermissionToRead($miDocumentId);
    $moDocument->fetchById($miDocumentId);
    $moPMDocInstance = $moDocument->getPublishedInstance();
    
    $msPath = $moWebLib->getSysRef()."packages/policy/".$moPMDocInstance->getFieldValue('doc_instance_path');
    
    if(!file_exists($msPath)) { // Nao deveria ser assim mas na base de dados tem entradas com o path completo, nao sei por que.. - Piero
    	$msPath = $moPMDocInstance->getFieldValue('doc_instance_path');
    }
    
    $msFileName = $moPMDocInstance->getFieldValue('doc_instance_file_name');
    header('Cache-Control: ');// leave blank to avoid IE errors
    header('Pragma: ');// leave blank to avoid IE errors
    header('Content-type: application/octet-stream');
    header('Content-Disposition: attachment; filename="'.$msFileName.'"');
    $moCrypt = new FWDCrypt();
    $moFP = fopen($msPath,'rb');
    $moCrypt->setIV(fread($moFP,strlen($moCrypt->getIV())));
    while(!feof($moFP)) {
      echo $moCrypt->decryptNoBase64(fread($moFP,16384));
    }
    fclose($moFP);
  }
}

class SaveRegDoc extends FWDRunnable {
  public function run(){
    $miRegisterId = FWDWebLib::getObject('selected_register_id')->getValue();
    $miDocumentId = FWDWebLib::getObject('selected_register_doc_id')->getValue();
    $moPMRegister = new PMRegister();

    if($miRegisterId && $miDocumentId){
      $moPMDocRegister = new PMDocRegisters();
      $moPMDocRegister->createFilter($miRegisterId,'register_id');
      $moPMDocRegister->createFilter($miDocumentId,'document_id');
      $moPMDocRegister->select();
      if(!$moPMDocRegister->fetch()){
        $moPMDocRegister->setFieldValue('register_id',$miRegisterId);
        $moPMDocRegister->setFieldValue('document_id',$miDocumentId);
        $moPMDocRegister->insert();
        
        $moRegister = new PMRegister();
        $moRegister->setFieldValue("document_id",$miDocumentId);
        $moRegister->update($miRegisterId);
      }
      
      $moRegisterBd = new PMRegister();
      $moRegisterBd->fetchById($miRegisterId);
      /*seta o tipo do documento para ser do tipo registro e o autor como o respons�vel pelo registro*/

      $moDocument = new PMDocument();
      $moDocument->setFieldValue('document_type',CONTEXT_REGISTER);
      $moDocument->setFieldValue('document_author',$moRegisterBd->getFieldValue('register_responsible'));
      $moDocument->update($miDocumentId);
    }
    //echo "soPopUpManager.getPopUpById('popup_create_document_confirm').close();";
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new RegisterConfirmRemove('register_confirm_remove'));
    $moStartEvent->addAjaxEvent(new RemoveRegisterEvent('remove_register'));
    $moStartEvent->addAjaxEvent(new FilterEvent('filter_register'));
    $moStartEvent->addAjaxEvent(new RegisterDocView('register_doc_view'));
    $moStartEvent->addSubmitEvent(new RegisterDocDownload('register_doc_download'));
    $moStartEvent->addAjaxEvent(new SaveRegDoc("save_reg_doc"));
        
    //instala a seguran�a de ACL na p�gina
    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
    FWDMenuACLSecurity::getInstance()->installMenuACL(FWDWebLib::getObject('menu'),FWDACLSecurity::getInstance());
    
    $moGrid = FWDWebLib::getObject("grid_register");
    $moHandler = new QueryGridRegister(FWDWebLib::getConnection());
    $moHandler->setRegisterClassification((int) FWDWebLib::getObject('filter_classification')->getValue());
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new GridRegister());
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moHandler = new QuerySelectRegisterType(FWDWebLib::getConnection());
    $moSelect = FWDWebLib::getObject('select_register_classification');
    $moSelect->setQueryHandler($moHandler);
    $moSelect->populate();
    
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        function refresh_grid(){
          js_refresh_grid('grid_register');
        }
        function remove_register(){
          trigger_event('remove_register',3);
          refresh_grid();
        }
        
        function openLookupDocument(piRegisterId){
          gebi('selected_register_id').value = piRegisterId;
          isms_open_popup('popup_document_search','packages/policy/popup_document_search.php?reg_id='+piRegisterId,'','true');
        }
        
        function set_document(piDocumentId){
          gebi('selected_register_doc_id').value = piDocumentId;
          setTimeout("trigger_event('save_reg_doc',3);",600);
          soPopUpManager.getPopUpById('popup_create_document_confirm').close();
        }
      </script>
    <?
  }
}

//testa se a licen�a utilizada permite acessar o m�dulo de documenta��o
ISMSLib::testUserPermissionToPoliceMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_register.xml");

?>
