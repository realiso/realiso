<?php

include_once "include.php";
include_once $handlers_ref . "select/policy/QuerySelectRegisterType.php";

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $miRegisterId = FWDWebLib::getObject('param_register_id')->getValue();
    
    $moPMRegister = new PMRegister();
    $moPMRegister->testPermissionToRead($miRegisterId);
    
    // Preenche os campos com os dados do registro.
    if($miRegisterId && $moPMRegister->fetchById($miRegisterId)){
      $moContextClassification = new ISMSContextClassification();
      $moContextClassification->fetchById($moPMRegister->getFieldValue("register_classification"));
      FWDWebLib::getObject('name')->setValue($moPMRegister->getFieldValue("register_name"));
      FWDWebLib::getObject('classification')->setValue($moContextClassification->getFieldValue('classif_name'));
      FWDWebLib::getObject('retention_time')->setValue($moPMRegister->getRetentionTimeAsString());
      FWDWebLib::getObject('storage_place')->setValue($moPMRegister->getFieldValue("register_storage_place"));
      FWDWebLib::getObject('storage_type')->setValue($moPMRegister->getFieldValue("register_storage_type"));
      
      $miRegisterDocumentId = $moPMRegister->getFieldValue("document_id");
      $miResponsibleId = $moPMRegister->getFieldValue('register_responsible');
      $moUser = new ISMSUser();
      $moUser->fetchById($miResponsibleId);
      FWDWebLib::getObject('responsible_name')->setValue($moUser->getFieldValue('user_name'));
      
      $moPMDocument = new PMDocument();
      if($miRegisterDocumentId && $moPMDocument->fetchById($miRegisterDocumentId)){
        $msDocumentName = $moPMDocument->getFieldValue("document_name");
        FWDWebLib::getObject('document_name')->setValue($msDocumentName);
        FWDWebLib::getObject('document_id')->setValue($miRegisterDocumentId);
        if(!$moPMDocument->userCanRead($miRegisterDocumentId)){
          FWDWebLib::getObject('viewgroup_document')->setShouldDraw(false);
        }
      }else{
        FWDWebLib::getObject('viewgroup_document')->setShouldDraw(false);
      }
    }
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
  }
}

//testa se a licen�a utilizada permite acessar o m�dulo de documenta��o
ISMSLib::testUserPermissionToPoliceMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_document_register_read.xml');

?>