<?php
include_once "include.php";
include_once $handlers_ref . "grid/policy/QueryGridDocReference.php";

class GridDocReference extends FWDDrawGrid {
  public function drawItem(){
    switch($this->ciColumnIndex){
      case 4:
        $this->coCellBox->setValue(ISMSLib::getISMSShortDate($this->coCellBox->getValue()));
        return $this->coCellBox->draw();
      break;
      default:
        return $this->coCellBox->draw();
      break;
    }
  }
}

class InsertEvent extends FWDRunnable {
  public function run() {
    $moWebLib = FWDWebLib::getInstance();
    $miDocumentId = $moWebLib->getObject('document_id')->getValue();
    
    $moPMDocument = new PMDocument();
    $moPMDocument->testPermissionToEdit($miDocumentId);
    
    $msDocReferenceName = $moWebLib->getObject('text_doc_reference_name')->getValue();
    $msDocReferenceLink = $moWebLib->getObject('text_doc_reference_link')->getValue();
    $msDocReferenceCreationDate = ISMSLib::ISMSTime();
    
    $moPMDocReference = new PMDocReference();
    $moPMDocReference->setFieldValue("document_id",$miDocumentId);
    $moPMDocReference->setFieldValue("doc_reference_name",$msDocReferenceName);
    $moPMDocReference->setFieldValue("doc_reference_link",$msDocReferenceLink);
    $moPMDocReference->setFieldValue("doc_reference_creation_date",$msDocReferenceCreationDate);
    
    $moPMDocReference->insert();
    
    echo "gebi('text_doc_reference_name').value = '';";
    echo "gebi('text_doc_reference_link').value = 'http://';";
    echo "refresh_grid();"; 
  }
}

class EditEvent extends FWDRunnable {
  public function run() {
    $moWebLib = FWDWebLib::getInstance();
    $moPMDocReference = new PMDocReference();
    $moPMDocReference->fetchById($moWebLib->getObject("doc_reference_id")->getValue());
    $msDocReferenceName = $moPMDocReference->getFieldValue("doc_reference_name");
    $msDocReferenceLink = $moPMDocReference->getFieldValue("doc_reference_link");
    echo "gebi('text_doc_reference_name').value='".$msDocReferenceName."';
          gebi('text_doc_reference_link').value='".$msDocReferenceLink."';
          js_hide('bt_insert');
          gobi('bt_save').show();
          js_show('bt_cancel');
         ";
    
  }
}

class SaveEvent extends FWDRunnable {
  public function run() {
    $miDocumentId = FWDWebLib::getObject('document_id')->getValue();
    $moPMDocument = new PMDocument();
    $moPMDocument->testPermissionToEdit($miDocumentId);
    
    $moWebLib = FWDWebLib::getInstance();
    $miDocReferenceId = $moWebLib->getObject("doc_reference_id")->getValue();
    $moPMDocReference = new PMDocReference();
    $moPMDocReference->setFieldValue("doc_reference_name",$moWebLib->getObject("text_doc_reference_name")->getValue());
    $moPMDocReference->setFieldValue("doc_reference_link",$moWebLib->getObject("text_doc_reference_link")->getValue());
    $moPMDocReference->update($miDocReferenceId);
    
    echo "gebi('text_doc_reference_name').value='';
          gebi('text_doc_reference_link').value='http://';
          js_hide('bt_save');
          js_hide('bt_cancel');
          js_show('bt_insert');
         ";
    echo "refresh_grid();";         
  }
}

class DocReferenceConfirmRemove extends FWDRunnable {
  public function run(){
    $msTitle = FWDLanguage::getPHPStringValue('tt_remove_reference','Remover Refer�ncia');
    $msMessage = FWDLanguage::getPHPStringValue('st_remove_reference_confirm',"Voc� tem certeza de que deseja remover a refer�ncia <b>%doc_reference_name%</b>?");
    
    $moPMDocReference = new PMDocReference();
    $moPMDocReference->fetchById(FWDWebLib::getObject('doc_reference_id')->getValue());
    $msDocReferenceName = ISMSLib::truncateString($moPMDocReference->getFieldValue('doc_reference_name'), 70);
    $msMessage = str_replace("%doc_reference_name%",$msDocReferenceName,$msMessage);
    
    $msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
                    "soWindow = soPopUp.getOpener();" .
                    "soWindow.doc_reference_remove();";
    
    ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,50);
  }
}

class DocReferenceRemoveEvent extends FWDRunnable {
  public function run(){    
    $miDocumentId = FWDWebLib::getObject('document_id')->getValue();
    $moPMDocument = new PMDocument();
    $moPMDocument->testPermissionToEdit($miDocumentId);
    
    $miDocReferenceId = FWDWebLib::getObject('doc_reference_id')->getValue();   
    $moPMDocReference = new PMDocReference();
    $moPMDocReference->delete($miDocReferenceId);
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new InsertEvent("insert"));
    $moStartEvent->addAjaxEvent(new EditEvent("edit"));
    $moStartEvent->addAjaxEvent(new SaveEvent("save"));
    $moStartEvent->addAjaxEvent(new DocReferenceConfirmRemove("doc_reference_confirm_remove"));
    $moStartEvent->addAjaxEvent(new DocReferenceRemoveEvent("doc_reference_remove"));
        
    $moGrid = FWDWebLib::getObject("grid_doc_reference");
    $moHandler = new QueryGridDocReference(FWDWebLib::getConnection());
    $moHandler->setDocumentId(FWDWebLib::getObject('document_id')->getValue());
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new GridDocReference());
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $miDocumentId = FWDWebLib::getObject('document_id')->getValue();
    $moPMDocument = new PMDocument();
    $moPMDocument->testPermissionToEdit($miDocumentId);
    
    $moWebLib = FWDWebLib::getInstance();
    $moWebLib->dump_html($moWebLib->getObject('dialog'));
    ?>
    <script language="javascript">
    gebi('text_doc_reference_name').focus();
    
    function refresh_grid() {
      js_refresh_grid('grid_doc_reference');    
    }
    
    function doc_reference_remove() {
      trigger_event('doc_reference_remove', 3);
      refresh_grid();       
    }
    </script>
  <?
  }
}

//testa se a licen�a utilizada permite acessar o m�dulo de documenta��o
ISMSLib::testUserPermissionToPoliceMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_document_reference.xml");
?>
