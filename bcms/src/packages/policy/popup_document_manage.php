<?php

include_once "include.php";

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $miDocumentId = FWDWebLib::getObject('param_document_id')->getValue();
    $moDocument = new PMDocument();
    $moDocument->fetchById($miDocumentId);
    
    $moDocument->testPermissionToManage($miDocumentId);
    
    $miDocumentType = $moDocument->getFieldValue('document_type');
    
    $miState = $moDocument->getContextState($miDocumentId);
    if($miState==CONTEXT_STATE_DOC_DEVELOPING){
      FWDWebLib::getObject('view_approvers')->setAttrDisplay('false');
      FWDWebLib::getObject('view_readers')->setAttrDisplay('false');
      FWDWebLib::getObject('view_elements')->setAttrDisplay('false');
      FWDWebLib::getObject('static_cant_edit_approvers')->setAttrDisplay('false');
      FWDWebLib::getObject('static_cant_edit_readers')->setAttrDisplay('false');
      FWDWebLib::getObject('static_cant_associate_elements')->setAttrDisplay('false');
      FWDWebLib::getObject('static_cant_associate_elements_on_type')->setAttrDisplay('false');
    }else{
      FWDWebLib::getObject('edit_approvers')->setAttrDisplay('false');
      FWDWebLib::getObject('edit_readers')->setAttrDisplay('false');
      FWDWebLib::getObject('associate_elements')->setAttrDisplay('false');
    }
    
    if ($miDocumentType == DOCUMENT_TYPE_MANAGEMENT || $miDocumentType == DOCUMENT_TYPE_OTHERS) {
      FWDWebLib::getObject('static_cant_associate_elements_on_type')->setAttrDisplay('true');
      FWDWebLib::getObject('viewbutton_associate_elements')->setAttrDisplay('false');
    }
    
    if($miState==CONTEXT_STATE_DOC_APPROVED){
      FWDWebLib::getObject('static_cant_send_to_revision')->setAttrDisplay('false');
    }else{
      FWDWebLib::getObject('viewbutton_send_to_revision')->setAttrDisplay('false');
    }
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
  }
}

//testa se a licen�a utilizada permite acessar o m�dulo de documenta��o
ISMSLib::testUserPermissionToPoliceMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_document_manage.xml');

?>