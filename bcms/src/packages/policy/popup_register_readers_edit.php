<?php

include_once "include.php";
include_once $handlers_ref."grid/QueryGridUserSearch.php";
include_once $handlers_ref."grid/policy/QueryGridCurrentUsers.php";

class GridUserSearch extends FWDDrawGrid {
  public function DrawItem(){
    switch($this->ciColumnIndex){
      case 1:
        $msGfxRef = FWDWebLib::getInstance()->getGfxRef();
        $moIcon = new FWDIcon(new FWDBox(2,4));
        $moIcon->setAttrSrc("{$msGfxRef}icon-user.gif");
        return $moIcon->draw();
      default:
        return parent::drawItem();
    }
  }
}

class AssociateReadersEvent extends FWDRunnable {
  public function run(){
    $miRegisterId = FWDWebLib::getObject('register_id')->getValue();
    
    $moRegister = new PMRegister();
    $moRegister->testPermissionToEdit($miRegisterId);
    
    $msNew = FWDWebLib::getObject('current_readers_ids')->getValue();
    if($msNew){
      $maNew = explode(':',$msNew);
    }else{
      $maNew = array();
    }
    
    $moRegisterUser = new PMRegisterReader();
    $maOld = $moRegisterUser->getAllowedUsers($miRegisterId);

    $maToInsert = array_diff($maNew,$maOld);
    $maToDelete = array_diff($maOld,$maNew);
    
    $moRegisterUser = new PMRegisterReader();
    if(count($maToInsert)) $moRegisterUser->insertUsers($miRegisterId,$maToInsert);
    if(count($maToDelete)) $moRegisterUser->denyUsers($miRegisterId,$maToDelete);
    
    echo "soWindow = soPopUpManager.getPopUpById('popup_register_readers_edit').getOpener();
          if (soWindow.refresh_grid) soWindow.refresh_grid();
          soWindow = null;
          soPopUpManager.closePopUp('popup_register_readers_edit');";
  }
}

class SearchUserEvent extends FWDRunnable {
  public function run(){
    $moGrid = FWDWebLib::getObject('grid_readers_search');
    $moGrid->execEventPopulate();
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
    $moStartEvent->addAjaxEvent(new SearchUserEvent('search_user_event'));
    $moStartEvent->addAjaxEvent(new AssociateReadersEvent('associate_readers_event'));
    
    $moSearchGrid = FWDWebLib::getObject('grid_readers_search');
    $moSearchHandler = new QueryGridUserSearch(FWDWebLib::getConnection());
    $moSearchGrid->setObjFwdDrawGrid(new GridUserSearch());
    
    $moCurrentGrid = FWDWebLib::getObject('grid_current_readers');
    $moCurrentHandler = new QueryGridCurrentUsers(FWDWebLib::getConnection());
    $moCurrentGrid->setObjFwdDrawGrid(new GridUserSearch());
    
    $moSearchHandler->setName(FWDWebLib::getObject('name_filter')->getValue());
    
    $miRegisterId = FWDWebLib::getObject('param_register_id')->getValue();
    if($miRegisterId){
      FWDWebLib::getObject('register_id')->setValue($miRegisterId);
    }else{
      $miRegisterId = FWDWebLib::getObject('register_id')->getValue();
    }
    
    $msCurrentUsers = FWDWebLib::getObject('current_readers_ids')->getValue();
    if($msCurrentUsers){
      $maUsers = explode(':',$msCurrentUsers);
      $moSearchHandler->setExludedUsers($maUsers);
      $moCurrentHandler->showUsers($maUsers);
    }else{
      $moCurrentHandler->setEmpty(true);
    }
    
    $moSearchGrid->setQueryHandler($moSearchHandler);
    $moCurrentGrid->setQueryHandler($moCurrentHandler);
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $miRegisterId = FWDWebLib::getObject('param_register_id')->getValue();
    $moRegister = new PMRegister();
    $moRegister->testPermissionToEdit($miRegisterId);
        
    $moRegisterUser = new PMRegisterReader();
    $maUsers = $moRegisterUser->getAllowedUsers(FWDWebLib::getObject('param_register_id')->getValue());
    FWDWebLib::getObject('current_readers_ids')->setValue(implode(':',$maUsers));
    
    $moCurrentHandler = new QueryGridCurrentUsers(FWDWebLib::getConnection());
    $moCurrentHandler->showUsers($maUsers);
    FWDWebLib::getObject('grid_current_readers')->setQueryHandler($moCurrentHandler);
    
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        gebi('reader_name').focus();
      </script>
    <?
  }
}

//testa se a licen�a utilizada permite acessar o m�dulo de documenta��o
ISMSLib::testUserPermissionToPoliceMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_register_readers_edit.xml');

?>