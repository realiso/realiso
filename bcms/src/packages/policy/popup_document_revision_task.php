<?php
include_once "include.php";
include_once $handlers_ref . "grid/QueryGridUserTasks.php";

class DownloadEvent extends FWDRunnable {
  public function run() {
    $moWebLib = FWDWebLib::getInstance();
    $miDocInstanceId = $moWebLib->getObject('var_instance_id')->getValue();

    $moPMDocInstance = new PMDocInstance();
    $moPMDocInstance->testPermissionToRead($miDocInstanceId);

    if ($moPMDocInstance->fetchById($miDocInstanceId)) {
      $msPath = $moPMDocInstance->getFieldValue('doc_instance_path');
      $msFileName = $moPMDocInstance->getFieldValue('doc_instance_file_name');
      header('Cache-Control: ');// leave blank to avoid IE errors
      header('Pragma: ');// leave blank to avoid IE errors
      header('Content-type: application/octet-stream');
      header('Content-Disposition: attachment; filename="'.$msFileName.'"');
      $moCrypt = new FWDCrypt();
      $moFP = fopen($msPath,'rb');
      $moCrypt->setIV(fread($moFP,strlen($moCrypt->getIV())));
      while(!feof($moFP)) {
        echo $moCrypt->decryptNoBase64(fread($moFP,16384));
      }
      fclose($moFP);
    }
  }
}

class VisitLinkEvent extends FWDRunnable {
  public function run() {
    $moWebLib = FWDWebLib::getInstance();
    $miDocInstanceId = $moWebLib->getObject('var_instance_id')->getValue();

    $moPMDocInstance = new PMDocInstance();
    $moPMDocInstance->testPermissionToRead($miDocInstanceId);

    if ($moPMDocInstance->fetchById($miDocInstanceId))
      $msURL = $moPMDocInstance->getFieldValue('doc_instance_link');
    if ($msURL)
      echo "js_open('{$msURL}','document_link',740,502,'center_screen');";
    else
      echo "soPopUpManager.closePopUp('popup_document_revision_task');";
  }
}

class CreateNewVersionEvent extends FWDRunnable {
  public function run(){
    $miDocumentId = FWDWebLib::getObject("var_document_id")->getValue();
    $miTaskId = FWDWebLib::getObject('var_task_id')->getValue();
    $miTimestamp = ISMSLib::ISMSTime();

    $moTask = new WKFTask();
    $moTask->testPermissionToExecute($miTaskId);

    $moPMDocumentRevisionHistory = new PMDocumentRevisionHistory();
    $moPMDocumentRevisionHistory->setFieldValue("document_id",$miDocumentId);
    $moPMDocumentRevisionHistory->setFieldValue("date_revision",$miTimestamp);
    $moPMDocumentRevisionHistory->setFieldValue("new_version",true);
    $moPMDocumentRevisionHistory->insert();

    $moDocument = new PMDocument();
    $moDocument->fetchById($miDocumentId);
    $moDocument->stateForward(WKF_ACTION_DOC_REVISION,FWDLanguage::getPHPStringValue('mx_scheduled_revision','Revis�o agendada.'));

    $moTask->createFilter($miDocumentId,"task_context_id");
    $moTask->createFilter(ACT_DOCUMENT_REVISION,"task_activity");
    $moTask->createFilter(true,"task_is_visible");
    $moTask->select();
    while($moTask->fetch()){
      $moTask2 = new WKFTask();
      if($moTask->getFieldValue('task_id')==$miTaskId){
        $moTask2->setFieldValue('task_date_accomplished',$miTimestamp);
      }
      $moTask2->setFieldValue('task_is_visible',false);
      $moTask2->update($moTask->getFieldValue("task_id"));
    }

    $msNextTaskJS = "";
		$mbOpenNextTask = (bool)array_filter(explode(':',FWDWebLib::getObject('next_task_controller')->getValue()));
		if ($mbOpenNextTask) {
		  $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
		  $moHandlerTask = new QueryGridUserTasks(FWDWebLib::getConnection());
      $moHandlerTask->setUser($miUserId);
      $moHandlerTask->setOrderBy('task_date_created','-');
      $moHandlerTask->makeQuery();
      $moHandlerTask->executeQuery();
      if ($moHandlerTask->fetch()) {
  		  $msNextTaskJS = 'soWindow.setTimeout("'.ISMSLib::getTaskPopup($moHandlerTask->getFieldValue('task_id'),
  		                                        $moHandlerTask->getFieldValue('task_activity'),
  		                                        $moHandlerTask->getFieldValue('task_context_id'),
  		                                        $moHandlerTask->getFieldValue('task_context_type'),
  		                                        2).',1");';
      }
		}
    echo "soWindow = soPopUpManager.getPopUpById('popup_document_revision_task').getOpener();"
        ."if(soWindow.refresh_grids) soWindow.refresh_grids();"
        .$msNextTaskJS
        ."soPopUpManager.closePopUp('popup_document_revision_task');";
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addSubmitEvent(new DownloadEvent("download"));
    $moStartEvent->addAjaxEvent(new VisitLinkEvent("visit_link"));
    $moStartEvent->addAjaxEvent(new CreateNewVersionEvent("create_new_version"));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $miAutoTaskOpen = FWDWebLib::getObject('auto_task_open')->getValue();
    if (!$miAutoTaskOpen) FWDWebLib::getObject('next_task')->setShouldDraw(false);
    if ($miAutoTaskOpen==2) FWDWebLib::getObject('next_task_controller')->checkItem(1);

    $moWebLib = FWDWebLib::getInstance();
    $miDocumentId = $moWebLib->getObject("document_id")->getValue();
    $moWebLib->getObject("var_document_id")->setValue($miDocumentId);
    $miTaskId = $moWebLib->getObject("task_id")->getValue();
    $moWebLib->getObject("var_task_id")->setValue($miTaskId);

    $moTask = new WKFTask();
    $moTask->testPermissionToExecute($miTaskId);

    $moPMDocument = new PMDocument();
    if ($moPMDocument->fetchById($miDocumentId)) {
      $moWebLib->getObject('name')->setValue($moPMDocument->getFieldValue('document_name'));
      $moWebLib->getObject('description')->setValue($moPMDocument->getFieldValue('document_description'));
      $miAuthorId = $moPMDocument->getFieldValue('document_author');
      if($miAuthorId){
        $moUser = new ISMSUser();
        if($moUser->fetchById($miAuthorId)) {
          $moWebLib->getObject('author')->setValue($moUser->getName());
        }
      }
    }
    $miCurrentVersion = $moPMDocument->getFieldValue('document_current_version');
    $moPMDocInstance = new PMDocInstance();
    if ($miCurrentVersion && $moPMDocInstance->fetchById($miCurrentVersion)) {
      $moWebLib->getObject('var_instance_id')->setValue($miCurrentVersion);
      $mbIsLink = $moPMDocInstance->getFieldValue('doc_instance_is_link');
      if ($mbIsLink) {
        $moWebLib->getObject('file_name')->setValue($moPMDocInstance->getFieldValue('doc_instance_link'));
        $moWebLib->getObject('label_file_name')->setAttrDisplay("false");
        $moWebLib->getObject('label_link')->setAttrDisplay("true");
        $moWebLib->getObject('download_button')->setAttrDisplay("false");
        $moWebLib->getObject('visit_link_button')->setAttrDisplay("true");
      } else {
        $msPath = $moPMDocInstance->getFieldValue('doc_instance_path');
        if ($msPath && is_file($msPath)) {
          $moWebLib->getObject('file_name')->setValue($moPMDocInstance->getFieldValue('doc_instance_file_name'));
          $moWebLib->getObject('label_link')->setAttrDisplay("false");
          $moWebLib->getObject('label_file_name')->setAttrDisplay("true");
          $moWebLib->getObject('visit_link_button')->setAttrDisplay("false");
          $moWebLib->getObject('download_button')->setAttrDisplay("true");
        }
      }
    }
    $moWebLib->dump_html($moWebLib->getObject('dialog'));
  }
}

//testa se a licen�a utilizada permite acessar o m�dulo de documenta��o
ISMSLib::testUserPermissionToPoliceMode();

	FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
	FWDWebLib::getInstance()->xml_load("popup_document_revision_task.xml");
?>