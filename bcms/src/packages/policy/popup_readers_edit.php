<?php

include_once "include.php";
include_once $handlers_ref."grid/QueryGridUserSearch.php";
include_once $handlers_ref."grid/policy/QueryGridCurrentUsers.php";
include_once $handlers_ref."select/QuerySelectProcess.php";
include_once $handlers_ref."QueryContextHierarchicalStructure.php";

class GridUserSearch extends FWDDrawGrid {
  public function DrawItem(){
    switch($this->ciColumnIndex){
      case 1:
        $msGfxRef = FWDWebLib::getInstance()->getGfxRef();
        $moIcon = new FWDIcon(new FWDBox(2,4));
        $moIcon->setAttrSrc("{$msGfxRef}icon-user.gif");
        return $moIcon->draw();
      default:
        return parent::drawItem();
    }
  }
}

class AssociateReadersEvent extends FWDRunnable {
  public function run(){
    $miDocumentId = FWDWebLib::getObject('document_id')->getValue();
    
    $moDocument = new PMDocument();
    $moDocument->testPermissionToManage($miDocumentId);
    
    $msNew = FWDWebLib::getObject('current_readers_ids')->getValue();
    if($msNew){
      $maNew = explode(':',$msNew);
    }else{
      $maNew = array();
    }
    
    $moDocumentUser = new PMDocumentReader();
    $maOld = $moDocumentUser->getAllowedUsers($miDocumentId);
    
    $maToInsert = array_diff($maNew,$maOld);
    $maToDelete = array_diff($maOld,$maNew);
    
    $moDocumentUser = new PMDocumentReader();
    if(count($maToInsert)) $moDocumentUser->insertUsers($miDocumentId,$maToInsert);
    if(count($maToDelete)) $moDocumentUser->denyUsers($miDocumentId,$maToDelete);
    
    echo "soWindow = soPopUpManager.getPopUpById('popup_readers_edit').getOpener();
          if (soWindow.refresh_grid) soWindow.refresh_grid();
          soWindow = null;
          soPopUpManager.closePopUp('popup_readers_edit');";
  }
}

class PopulateProcessEvent extends FWDRunnable {
  public function run(){
    $miAreaId = FWDWebLib::getObject('readers_area_id')->getValue();
    $moHandler = new QuerySelectProcess(FWDWebLib::getConnection());
    $moHandler->setArea($miAreaId);
    
    $moSelect = FWDWebLib::getObject('readers_process_id');
    $moSelect = FWDWebLib::getObject('readers_process_id');
    $moSelect->setQueryHandler($moHandler);
    $moSelect->populate();
    $moSelect->execEventPopulate();
  }
}

class SearchUserEvent extends FWDRunnable {
  public function run(){
    $moGrid = FWDWebLib::getObject('grid_readers_search');
    $moGrid->execEventPopulate();
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
    $moStartEvent->addAjaxEvent(new SearchUserEvent('search_user_event'));
    $moStartEvent->addAjaxEvent(new PopulateProcessEvent('populate_process'));
    $moStartEvent->addAjaxEvent(new AssociateReadersEvent('associate_readers_event'));
    
    $moSearchGrid = FWDWebLib::getObject('grid_readers_search');
    $moSearchHandler = new QueryGridUserSearch(FWDWebLib::getConnection());
    $moSearchGrid->setObjFwdDrawGrid(new GridUserSearch());
    
    $moCurrentGrid = FWDWebLib::getObject('grid_current_readers');
    $moCurrentHandler = new QueryGridCurrentUsers(FWDWebLib::getConnection());
    $moCurrentGrid->setObjFwdDrawGrid(new GridUserSearch());
    
    $moSearchHandler->setName(FWDWebLib::getObject('name_filter')->getValue());
    $moSearchHandler->setArea(FWDWebLib::getObject('area_filter')->getValue());
    $moSearchHandler->setProcess(FWDWebLib::getObject('process_filter')->getValue());
    
    $miDocumentId = FWDWebLib::getObject('param_document_id')->getValue();
    if($miDocumentId){
      FWDWebLib::getObject('document_id')->setValue($miDocumentId);
    }else{
      $miDocumentId = FWDWebLib::getObject('document_id')->getValue();
    }
    
    $msCurrentUsers = FWDWebLib::getObject('current_readers_ids')->getValue();
    if($msCurrentUsers){
      $maUsers = explode(':',$msCurrentUsers);
      $moSearchHandler->setExludedUsers($maUsers);
      $moCurrentHandler->showUsers($maUsers);
    }else{
      $moCurrentHandler->setEmpty(true);
    }
    
    $moSearchGrid->setQueryHandler($moSearchHandler);
    $moCurrentGrid->setQueryHandler($moCurrentHandler);
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $miDocumentId = FWDWebLib::getObject('param_document_id')->getValue();
    $moDocument = new PMDocument();
    $moDocument->testPermissionToManage($miDocumentId);
    
    $moSelect = FWDWebLib::getObject('readers_area_id');
    $moQuery = new QueryContextHierarchicalStructure(FWDWebLib::getConnection());
    $moQuery->setContextType(CONTEXT_AREA);
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    foreach($moQuery->getContexts() as $maContext){
      list($miContextId,$msHierarchicalName) = $maContext;
      $moSelect->setItemValue($miContextId,$msHierarchicalName);
    }
    
    $moDocumentUser = new PMDocumentReader();
    $maUsers = $moDocumentUser->getAllowedUsers(FWDWebLib::getObject('param_document_id')->getValue());
    FWDWebLib::getObject('current_readers_ids')->setValue(implode(':',$maUsers));
    
    $moCurrentHandler = new QueryGridCurrentUsers(FWDWebLib::getConnection());
    $moCurrentHandler->showUsers($maUsers);
    FWDWebLib::getObject('grid_current_readers')->setQueryHandler($moCurrentHandler);
    
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        gebi('reader_name').focus();
      </script>
    <?
  }
}

//testa se a licen�a utilizada permite acessar o m�dulo de documenta��o
ISMSLib::testUserPermissionToPoliceMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_readers_edit.xml');

?>