<?php
include_once "include.php";
include_once $handlers_ref . "select/policy/QuerySelectCurrentDocuments.php";
include_once $handlers_ref . "select/policy/QuerySelectCurrentUsers.php";
include_once $handlers_ref . "select/policy/QuerySelectDocumentType.php";
include_once $handlers_ref . "select/policy/QuerySelectRegisterType.php";
include_once $handlers_ref . "QueryGetReportClassification.php";

/*
 * Colocar aqui todos os relat�rios que possuem filtros.
 * Para cada linha (relat�rio) da grid � criado um evento
 * que faz com que quando um relat�rio seja selecionado,
 * ele esconda todos os filtros da tela e mostre somente
 * os filtros pertinentes ao relat�rio clicado.
 */
class GridReports extends FWDDrawGrid {
  public function drawItem(){
    switch($this->ciColumnIndex){
      case 2:{
        $moEvent = new FWDClientEvent();
        $moEvent->setAttrEvent('onClick');
        $moEvent->setAttrValue("FilterManager.showFilters('{$this->caData[1]}');");
        $this->coCellBox->addObjFWDEvent($moEvent);
        return parent::drawItem();
      }
      default:{
        return parent::drawItem();
      }
    }
  }
}

class PopulateFilterDocumentListDocumentsSelectEvent extends FWDRunnable {
  public function run(){
    $msIds = FWDWebLib::getObject('var_filter_document_list_documents')->getValue();
    $maIds = array_filter(explode(':',$msIds));
    
    $moHandler = new QuerySelectCurrentDocuments(FWDWebLib::getConnection());
    $moHandler->setIds($maIds);
    
    $moSelect = FWDWebLib::getObject('filter_document_list_documents');
    $moSelect->setQueryHandler($moHandler);
    $moSelect->populate();
    $moSelect->execEventPopulate();
  }
}

class PopulateFilterUserListUsersSelectEvent extends FWDRunnable {
  public function run(){
    $msIds = FWDWebLib::getObject('var_filter_user_list_users')->getValue();
    $maIds = array_filter(explode(':',$msIds));
    
    $moHandler = new QuerySelectCurrentUsers(FWDWebLib::getConnection());
    $moHandler->setIds($maIds);
    
    $moSelect = FWDWebLib::getObject('filter_user_list_users');
    $moSelect->setQueryHandler($moHandler);
    $moSelect->populate();
    $moSelect->execEventPopulate();
  }
}

class PopulateFilterDocumentEvent extends FWDRunnable {
  public function run(){
    $miDoc = FWDWebLib::getObject('doc_id')->getValue();    
    $moDoc = new PMDocument();
    $moDoc->fetchById($miDoc);    
    echo "gobi('doc_name').setValue('" . $moDoc->getFieldValue('document_name') . "')";
  }
}

class ISMSReportEvent extends FWDRunnable {

  private function getReportDocumentAccessesFilter(){
    $moFilter = new ISMSReportDocumentAccessesFilter();
    $miInitialDate = FWDWebLib::getObject('filter_access_initial_date')->getTimestamp();
    $miFinalDate = FWDWebLib::getObject('filter_access_final_date')->getTimestamp();
    $mbShowAll = (strpos(FWDWebLib::getObject('filter_access_show_all_accesses')->getValue(),'1')!==false);
    $maDocuments = array_filter(explode(':',FWDWebLib::getObject('var_filter_document_list_documents')->getValue()));
    
    $moFilter->setDocuments($maDocuments);
    $moFilter->setShowAllReads($mbShowAll);
    $moFilter->setInitialDate($miInitialDate);
    $moFilter->setFinalDate($miFinalDate);
    return $moFilter;
  }

  private function getReportAccessedDocumentsFilter(){
    $moFilter = new ISMSReportAccessedDocumentsFilter();
    $miInitialDate = FWDWebLib::getObject('filter_access_initial_date')->getTimestamp();
    $miFinalDate = FWDWebLib::getObject('filter_access_final_date')->getTimestamp();
    $mbShowAll = (strpos(FWDWebLib::getObject('filter_access_show_all_accesses')->getValue(),'1')!==false);
    $maUsers = array_filter(explode(':',FWDWebLib::getObject('var_filter_user_list_users')->getValue()));
    
    $moFilter->setUsers($maUsers);
    $moFilter->setShowAllReads($mbShowAll);
    $moFilter->setInitialDate($miInitialDate);
    $moFilter->setFinalDate($miFinalDate);
    return $moFilter;
  }

  private function getReportDocumentRegistersFilter(){
    $moFilter = new ISMSReportDocumentRegistersFilter();
    $maDocuments = array_filter(explode(':',FWDWebLib::getObject('var_filter_document_list_documents')->getValue()));
    $moFilter->setDocuments($maDocuments);
    return $moFilter;
  }
  
  private function getReportDocumentApproversFilter(){
    $moFilter = new ISMSReportDocumentApproversFilter();
    $maDocuments = array_filter(explode(':',FWDWebLib::getObject('var_filter_document_list_documents')->getValue()));
    $moFilter->setDocuments($maDocuments);
    return $moFilter;
  }
  
  private function getReportDocumentReadersFilter(){
    $moFilter = new ISMSReportDocumentReadersFilter();
    $maDocuments = array_filter(explode(':',FWDWebLib::getObject('var_filter_document_list_documents')->getValue()));
    $moFilter->setDocuments($maDocuments);
    return $moFilter;
  }
  
  private function getReportTopRevisedDocumentsFilter(){
    $moFilter = new ISMSReportTopRevisedDocumentsFilter();
    $miTruncateNumber = FWDWebLib::getObject('filter_truncate_number')->getValue();
    $mbExpand = (strpos(FWDWebLib::getObject('filter_expand_documents_expand')->getValue(),'1')!==false);
    $moFilter->setTruncateNumber($miTruncateNumber);
    $moFilter->setExpand($mbExpand);
    return $moFilter;
  }
  
  private function getReportTopAccessedDocumentsFilter(){
    $moFilter = new ISMSReportTopAccessedDocumentsFilter();
    $miTruncateNumber = FWDWebLib::getObject('filter_truncate_number')->getValue();
    $moFilter->setTruncateNumber($miTruncateNumber);
    return $moFilter;
  }

  private function getReportNotAccessedDocumentsFilter(){
    $moFilter = new ISMSReportNotAccessedDocumentsFilter();
    $mbExpand = (strpos(FWDWebLib::getObject('filter_expand_readers_expand')->getValue(),'1')!==false);
    $moFilter->setExpand($mbExpand);
    return $moFilter;
  }

  private function getReportDocumentsPendentReadsFilter(){
    $moFilter = new ISMSReportDocumentsPendentReadsFilter();
    $mbExpand = (strpos(FWDWebLib::getObject('filter_expand_readers_expand')->getValue(),'1')!==false);
    $moFilter->setExpand($mbExpand);
    return $moFilter;
  }

  private function getReportUsersByProcessFilter(){
    $moFilter = new ISMSReportUsersByProcessFilter();
    $mbOrganize = (strpos(FWDWebLib::getObject('filter_organize_by_area_organize')->getValue(),'1')!==false);
    $moFilter->setOrganizeByArea($mbOrganize);
    return $moFilter;
  }

  private function getReportDocumentDatesFilter(){
    $moFilter = new ISMSReportDocumentDatesFilter();
    
    $miCreationDateStart = FWDWebLib::getObject('filter_dates_creation_date_start')->getTimestamp();
    $miCreationDateEnd = FWDWebLib::getObject('filter_dates_creation_date_end')->getTimestamp();
    $miRevisionDateStart = FWDWebLib::getObject('filter_dates_revision_date_start')->getTimestamp();
    $miRevisionDateEnd = FWDWebLib::getObject('filter_dates_revision_date_end')->getTimestamp();

    $moFilter->setCreationDateStart($miCreationDateStart);
    $moFilter->setCreationDateEnd($miCreationDateEnd);
    $moFilter->setRevisionDateStart($miRevisionDateStart);
    $moFilter->setRevisionDateEnd($miRevisionDateEnd);
    
    return $moFilter; 
  }  

  private function getReportDocumentsByAreaFilter(){
    $moFilter = new ISMSReportDocumentsByAreaFilter();
    $mbExpand = (strpos(FWDWebLib::getObject('filter_expand_documents_expand')->getValue(),'1')!==false);
    $moFilter->setExpand($mbExpand);
    return $moFilter;
  }
  
  private function getReportDocumentSummaryFilter(){
    $moFilter = new ISMSReportDocumentSummaryFilter();    
    $moFilter->setDocument(FWDWebLib::getObject('doc_id')->getValue());
    return $moFilter;
  }
  
  private function getReportElementClassificationFilter(){
    $moFilter = new ISMSReportElementClassificationFilter();    
    $moFilter->setDocumentsType(FWDWebLib::getObject('select_document_type')->getValue());
    $moFilter->setRegistersType(FWDWebLib::getObject('select_register_type')->getValue());
    return $moFilter;
  }
  
  private function getReportDocumentResponsibleFilter(){
    $moFilter = new ISMSReportDocumentsWithoutRegisterFilter();
    $maUsers = array_filter(explode(':',FWDWebLib::getObject('var_filter_user_list_users')->getValue()));
    $moFilter->setUserId($maUsers);
    return $moFilter;
  }

  private function getReportUsersWithPendantReadFilter(){
    $moFilter = new ISMSReportUsersWithPendantReadFilter();
    $maUsers = array_filter(explode(':',FWDWebLib::getObject('var_filter_user_list_users')->getValue()));
    $moFilter->setUserId($maUsers);
    return $moFilter;
  }

  private function getReportDocsWithHighFrequencyRevisionFilter(){
    $moFilter = new ISMSReportDocsWithHighFrequencyRevisionFilter();
    $maUsers = array_filter(explode(':',FWDWebLib::getObject('var_filter_user_list_users')->getValue()));
    $moFilter->setUserId($maUsers);
    return $moFilter;
  }
  
  private function getReportDocumentsByComponentFilter() {
    $moFilter = new ISMSReportDocumentsByComponentFilter();
    $msValue = FWDWebLib::getObject('component_controller')->getValue();
    $msValue = str_replace(array('process', 'asset', 'control'), array(CONTEXT_PROCESS, CONTEXT_ASSET, CONTEXT_CONTROL), $msValue);    
    $maType = array_filter(explode(':', $msValue));
    $moFilter->setDocType($maType);
    return $moFilter;
  }
  
  private function getReportDocumentsByStateFilter() {
    $moFilter = new ISMSReportDocumentsByStateFilter();
    $msController = FWDWebLib::getObject('show_register_controller')->getValue();
    if ($msController) $moFilter->setShowRegisterDocs(true);    
    $msValue = FWDWebLib::getObject('select_status')->getValue();
    $miState = 0;
    switch($msValue) {
      case 'developing':
        $miState = CONTEXT_STATE_DOC_DEVELOPING;
      break;
      case 'approved':
        $miState = CONTEXT_STATE_DOC_APPROVED;
      break;
      case 'pendant':
        $miState = CONTEXT_STATE_DOC_PENDANT;
      break;
      case 'revision':
        $miState = CONTEXT_STATE_DOC_REVISION;
      break;
      case 'obsolete':
        $miState = CONTEXT_STATE_DOC_OBSOLETE;
      break;
    }    
    if ($miState) $moFilter->setStatus($miState);    
    return $moFilter;
  }
  
  private function getReportDocInstanceCommentAndJustificationFilter(){
    $moFilter = new ISMSReportDocInstanceCommentAndJustificationFilter();
    $maDocuments = array_filter(explode(':',FWDWebLib::getObject('var_filter_document_list_documents')->getValue()));
    
    $moFilter->setDocuments($maDocuments);
    return $moFilter;
  }
  
  private function getReportUserCommentsFilter(){
    $moFilter = new ISMSReportUserCommentsFilter();
    $maDocuments = array_filter(explode(':',FWDWebLib::getObject('var_filter_document_list_documents')->getValue()));
    
    $moFilter->setDocuments($maDocuments);
    return $moFilter;
  }
  
  /*
   * Essa fun��o serve para retornar o filtro do relat�rio.
   * De acordo com o relat�rio selecionado, chama um fun��o
   * espec�fica para montar o filtro do relat�rio em quest�o.
   */
  private function getFilter() {
    $maValue = FWDWebLib::getObject('grid_reports')->getValue();
    $moReportFilter = null;
    switch($maValue[0]) {
      case 'report_accessed_documents':                     $moReportFilter = $this->getReportAccessedDocumentsFilter();                  break;
      case 'report_document_accesses':                      $moReportFilter = $this->getReportDocumentAccessesFilter();                   break;
      case 'report_document_registers':                     $moReportFilter = $this->getReportDocumentRegistersFilter();                  break;      
      case 'report_document_approvers':                     $moReportFilter = $this->getReportDocumentApproversFilter();                  break;
      case 'report_document_readers':                       $moReportFilter = $this->getReportDocumentReadersFilter();                    break;
      case 'report_top_revised_documents':                  $moReportFilter = $this->getReportTopRevisedDocumentsFilter();                break;
      case 'report_top_accessed_documents':                 $moReportFilter = $this->getReportTopAccessedDocumentsFilter();               break;
      case 'report_not_accessed_documents':                 $moReportFilter = $this->getReportNotAccessedDocumentsFilter();               break;
      case 'report_documents_pendent_reads':                $moReportFilter = $this->getReportDocumentsPendentReadsFilter();              break;
      case 'report_users_by_process':                       $moReportFilter = $this->getReportUsersByProcessFilter();                     break;
      case 'report_document_dates':                         $moReportFilter = $this->getReportDocumentDatesFilter();                      break;      
      case 'report_documents_by_area':                      $moReportFilter = $this->getReportDocumentsByAreaFilter();                    break;
      case 'report_document_summary':	                      $moReportFilter = $this->getReportDocumentSummaryFilter();                    break;
      case 'report_element_classification':		              $moReportFilter = $this->getReportElementClassificationFilter();              break;
      case 'report_documents_without_register':             $moReportFilter = $this->getReportDocumentResponsibleFilter();                break;
      case 'report_users_with_pendant_read':                $moReportFilter = $this->getReportUsersWithPendantReadFilter();               break;
      case 'report_docs_with_high_frequency_revision':      $moReportFilter = $this->getReportDocsWithHighFrequencyRevisionFilter();      break;
      case 'report_documents_by_component':                 $moReportFilter = $this->getReportDocumentsByComponentFilter();               break;
      case 'report_documents_by_state':                     $moReportFilter = $this->getReportDocumentsByStateFilter();                   break;
      case 'report_doc_instance_comment_and_justification': $moReportFilter = $this->getReportDocInstanceCommentAndJustificationFilter(); break;
      case 'report_user_comments':                          $moReportFilter = $this->getReportUserCommentsFilter();                       break;
      default:                                              $moReportFilter = new FWDReportFilter();                                      break;
    }
    $moReportFilter->setManualClassification(FWDWebLib::getObject('select_report_classification')->getValue());
    $moReportFilter->setFileType(FWDWebLib::getObject('select_report_type')->getValue());
    
    FWDWebLib::getObject('report_comment')->getObjFWDString()->setAttrNoEscape('true');
    
    $moReportFilter->setComment(FWDWebLib::convertToISO(FWDWebLib::getObject('report_comment')->getValue(), true));
    return $moReportFilter;
  }

  /*
   * Fun��o para retornar o relat�rio que deve ser gerado.
   * � utilizada quando o relat�rio que foi clicado deve
   * chamar um outro relat�rio.
   */
  private function getReport(){
    $maGridValue = FWDWebLib::getObject('grid_reports')->getValue();
    
    switch ($maGridValue[0]) {
    	case 'report_element_classification':      	      	
      	$maItem = FWDWebLib::getObject('select_element_type')->getItems();
        foreach($maItem as $moFilter){
          if($moFilter->getAttrCheck()==true){
            switch ($moFilter->getAttrKey()) {
      				case 'document_type':			return 'report_documents_by_type';      				
      				case 'register_type':			return 'report_registers_by_type';
      			}
          }
        }
     	break;
      case 'report_pendant_task_pm':
        return str_replace(':','',FWDWebLib::getObject('pendant_tasks_filter_radiobox')->getValue());
      break;
    	default:
     		$msReport = $maGridValue[0];
     	break;
    }    
    
    return $msReport;
  }

  /*
   * Verifica se o filtro do relat�rio foi preenchido corretamente.
   */
  public function validateFilters(){
    $maValue = FWDWebLib::getObject('grid_reports')->getValue();
    switch($maValue[0]) {    	
    	case 'report_document_summary':      
        if (FWDWebLib::getObject('doc_id')->getValue()) return true;
        else echo "js_show('report_document_summary_filter_warning');";
      break;
      
      /*case 'report_element_classification':
      	$maItem = FWDWebLib::getObject('select_element_type')->getItems();
        foreach($maItem as $moFilter){
          if($moFilter->getAttrCheck()==true){
          	switch ($moFilter->getAttrKey()) {
          		case 'document_type':
         				$miValue = FWDWebLib::getObject('select_document_type')->getValue();         				
          		break;          		
          		case 'register_type':
          			$miValue = FWDWebLib::getObject('select_register_type')->getValue();         		
          		break;
          	}
          	if ($miValue) return true;
         		else {
         			echo "gobi('element_type_filter_warning').show();";
         			return false;
         		}
          }
        }
        return false;
      break;*/
      
      case 'report_documents_by_component':
        $msValue = FWDWebLib::getObject('component_controller')->getValue();
        if (!$msValue) {
          echo "gobi('component_filter_warning').show();";
          return false;
        }
        else return true;
      break;
    	
      default:{
        return true;
      }
    }
  }

  public function run(){
    $maValue = FWDWebLib::getObject('grid_reports')->getValue();
    if(isset($maValue[0])){
      if($this->validateFilters()){
        set_time_limit(3000);

        $mbDontForceDownload = (FWDWebLib::getObject("select_report_type")->getValue()==REPORT_FILETYPE_HTML);

        $moWindowReport = new ISMSReportWindow(POLICY_MODE);
        if($mbDontForceDownload) $moWindowReport->open();

        $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
        $moSession->deleteAttribute("filter");
        $moSession->addAttribute("filter");
        $moSession->setAttrFilter($this->getFilter());

        $msReport = $this->getReport();

        if($mbDontForceDownload){
          $moWindowReport->setWaitLoadingComplete(true);
          $moWindowReport->loadReport($msReport);
        }else{
          $moWindowReport->forceReportDownload($msReport);
        }
      }
    }else{
      echo "js_show('warning');";
    }
  }
}

/*
 * Fun��o que retorna um array associativo do tipo array('report_category' => array('report1', 'report2', ...))
 * para mapear quais relat�rios pertencem a quais categorias
 */
function getReportCategoryArray(){
  return array(
    'general_reports' =>
      array(
        'report_documents_by_state' => array(FWDLanguage::getPHPStringValue('si_documents_by_state','Documentos'),'M.PM.17.2.1'),
        'report_documents_by_area' => array(FWDLanguage::getPHPStringValue('si_documents_per_area','Documentos por �rea'),'M.PM.17.2.2'),
        'report_documents_by_component' => array(FWDLanguage::getPHPStringValue('si_documents_by_component','Documentos por Componente do SGSI'),'M.PM.17.2.3'),
        'report_doc_instance_comment_and_justification' => array(FWDLanguage::getPHPStringValue('mx_doc_instance_comment_and_justification','Coment�rios e Justificativas de Documentos'),'M.PM.17.2.4'),
        'report_user_comments' => array(FWDLanguage::getPHPStringValue('mx_user_comments','Coment�rios de Usu�rios'),'M.PM.17.2.5'),
      ),
    'register_reports' =>
      array(
        'report_document_registers' => array(FWDLanguage::getPHPStringValue('si_registers_per_document','Registro por Documento'),'M.PM.17.3.1')
      ),
    'access_control_reports' =>
      array(
        'report_accessed_documents' => array(FWDLanguage::getPHPStringValue('si_accessed_documents','Auditoria de Acesso dos Usu�rios'),'M.PM.17.4.1'),
        'report_document_accesses' => array(FWDLanguage::getPHPStringValue('si_access_to_documents','Auditoria de Acesso aos Documentos'),'M.PM.17.4.2'),
        'report_document_approvers' => array(FWDLanguage::getPHPStringValue('si_approvers','Documentos e seus Aprovadores'),'M.PM.17.4.3'),
        'report_document_readers' => array(FWDLanguage::getPHPStringValue('si_readers','Documentos e seus Leitores'),'M.PM.17.4.4'),
        'report_users_by_process' => array(FWDLanguage::getPHPStringValue('si_users_per_process','Usu�rios associados aos Processos'),'M.PM.17.4.5')        
      ),
    'management_reports' =>
      array(
        'report_documents_pendent_reads' => array(FWDLanguage::getPHPStringValue('mx_report_documents_pendent_reads','Documentos com Leitura Pendente'),'M.PM.17.5.1'),
        'report_top_accessed_documents' => array(FWDLanguage::getPHPStringValue('si_most_accessed_documents','Documentos mais acessados'),'M.PM.17.5.2'),
        'report_not_accessed_documents' => array(FWDLanguage::getPHPStringValue('si_not_accessed_documents','Documentos n�o acessados'),'M.PM.17.5.3'),
        'report_top_revised_documents' => array(FWDLanguage::getPHPStringValue('si_most_revised_documents','Documentos mais revisados'),'M.PM.17.5.4'),
        'report_pendant_approvals' => array(FWDLanguage::getPHPStringValue('si_pendant_approvals','Aprova��es Pendentes'),'M.PM.17.5.5'),
        'report_document_dates' => array(FWDLanguage::getPHPStringValue('si_documents_dates','Data de Cria��o e Revis�o dos Documentos'),'M.PM.17.5.6'),
        'report_documents_revision_schedule' => array(FWDLanguage::getPHPStringValue('mx_documents_revision_schedule','Cronograma de Revis�o de Documentos'),'M.PM.17.5.7'),
        'report_documents_revision_late' => array(FWDLanguage::getPHPStringValue('mx_documents_revision_late','Documentos com Revis�o Atrasada'),'M.PM.17.5.8'),
      ),
    'abnormalities_reports' =>
      array(
        'report_contexts_without_documents' => array(FWDLanguage::getPHPStringValue('si_items_with_without_documents','Componentes sem Documentos'),'M.PM.17.6.1'),
        'report_pendant_task_pm' => array(FWDLanguage::getPHPStringValue('mx_pendant_task_pm','Tarefas pendentes'),'M.PM.17.6.2'),
        'report_docs_with_high_frequency_revision' => array(FWDLanguage::getPHPStringValue('mx_documents_with_high_revision_frequency','Documentos com uma alta freq��ncia de revis�o'),'M.PM.17.6.3'),
        'report_documents_without_register' => array(FWDLanguage::getPHPStringValue('mx_documents_without_registers','Documentos sem Registros'),'M.PM.17.6.4'),
        'report_users_with_pendant_read' => array(FWDLanguage::getPHPStringValue('mx_users_with_pendant_read','Usu�rios com leitura pendente'),'M.PM.17.6.5')
      ),
    'others' =>
      array(
        'report_document_summary' => array(FWDLanguage::getPHPStringValue('mx_report_document_summary','Propriedades do Documento'),'M.PM.17.1.16'),
        'report_element_classification' => array(FWDLanguage::getPHPStringValue('mx_report_element_classification','Relat�rio de Classifica��o'),'M.PM.17.1.17')
      )
  );
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new ISMSReportEvent("function_report"));
    $moStartEvent->addAjaxEvent(new ShowReportsEvent("show_reports"));
    $moStartEvent->addAjaxEvent(new PopulateFilterDocumentListDocumentsSelectEvent('populate_filter_document_list_documents_select_event'));
    $moStartEvent->addAjaxEvent(new PopulateFilterUserListUsersSelectEvent('populate_filter_user_list_users_select_event'));
    $moStartEvent->addAjaxEvent(new PopulateFilterDocumentEvent('populate_filter_document_event'));
  }
}

// popula a grid com os relat�rios de acordo com a op��o do filtro de relat�rios
class ShowReportsEvent extends FWDRunnable {
  public function run(){
    echo "FilterManager.showFilters('no_filter_available');";
  
    $moGrid = FWDWebLib::getObject('grid_reports');
    $moGrid->setObjFwdDrawGrid(new GridReports());
    $msReportCategory = FWDWebLib::getObject("report_category")->getValue();
    $maReportCategory = getReportCategoryArray();
    $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
  
    if ($msReportCategory != 'all_reports') {
      $maReports = $maReportCategory[$msReportCategory];
      $miLineCount = 0;
      foreach ($maReports as $msReportKey => $maReportACL) {
        if(!in_array($maReportACL[1] , $maACLs)){
          $miLineCount++;
          $moGrid->setItem(1, $miLineCount, $msReportKey);
          $moGrid->setItem(2, $miLineCount, $maReportACL[0]);
        }
      }
    }else{
      $miLineCount = 0;
      foreach ($maReportCategory as $maReports) {
        foreach ($maReports as $msReportKey => $maReportACL) {
          if(!in_array($maReportACL[1] , $maACLs)){
            $miLineCount++;
            $moGrid->setItem(1, $miLineCount, $msReportKey);
            $moGrid->setItem(2, $miLineCount, $maReportACL[0]);
          }
        }
      }
    }
    $moGrid->execEventPopulate();
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){    
    $moWebLib = FWDWebLib::getInstance();
    $moGrid = $moWebLib->getObject('grid_reports');
    $moGrid->setObjFwdDrawGrid(new GridReports());
    
    /*
     * Popula o select com tipos de documento
     */    
    $moHandler = new QuerySelectDocumentType(FWDWebLib::getConnection());
    $moSelect = FWDWebLib::getObject('select_document_type');
    $moSelect->setQueryHandler($moHandler);
    $moSelect->populate();
    
    /*
     * Popula o select com tipos de registro
     */    
    $moHandler = new QuerySelectRegisterType(FWDWebLib::getConnection());
    $moSelect = FWDWebLib::getObject('select_register_type');
    $moSelect->setQueryHandler($moHandler);
    $moSelect->populate();

    /*
     * Popula o select com os tipos de formato de relat�rios e
     * o select com os tipos de classifica��o de relat�rios.
     */
    $moFormatSelect = FWDWebLib::getObject('select_report_type');
    $maAvailableFormats = FWDReportGenerator::getAvailableFormats(ISMSLib::getSupportedReportFormats());
    foreach ($maAvailableFormats as $miKey => $msValue) {
      $moFormatSelect->setItemValue($miKey, $msValue);
    }
    $moHandler = new QueryGetReportClassification(FWDWebLib::getConnection());
    $moHandler->makeQuery();
    $maAvailableClassifications = $moHandler->executeQuery();
    $moClassificationSelect = FWDWebLib::getObject('select_report_classification');
    foreach ($maAvailableClassifications as $miKey => $msValue) {
      $moClassificationSelect->setItemValue($msValue, $msValue);
    }
    
    //instala a seguran�a de ACL na p�gina
    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());

    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
     ?>
      <script language="javascript">
        gobi('grid_reports').setPopulate(true);
        trigger_event('show_reports',3);
        
        FilterManager = {
          'selectedReport': '',
          'visibleFilters': ['no_filter_available'],
          'showFilter': function(psFilterId){
            this.visibleFilters.push(psFilterId);
            js_show(psFilterId);
          },
          'showFilters': function(psReportId){
            for(var i=0;i<this.visibleFilters.length;i++){
              js_hide(this.visibleFilters[i]);
            }
            this.visibleFilters = [];
            if(psReportId==this.selectedReport){
              this.showFilter('no_filter_available');
              this.selectedReport = '';
            }else{
              this.selectedReport = psReportId;
              switch(psReportId){
                case 'report_accessed_documents':{
                  this.showFilter('filter_user_list');
                  this.showFilter('filter_access');
                  break;
                }
                case 'report_document_accesses':{
                  this.showFilter('filter_document_list');
                  this.showFilter('filter_access');
                  break;
                }
                case 'report_document_registers':{
                  this.showFilter('filter_document_list');
                  break;
                }                
                case 'report_document_dates':{
                  this.showFilter('filter_dates');
                  break;
                }
                case 'report_documents_by_area':{
                  this.showFilter('filter_expand_documents');
                  break;
                }                
                case 'report_top_accessed_documents':{
                  this.showFilter('filter_truncate');
                  break;
                }
                case 'report_not_accessed_documents':{
                  this.showFilter('filter_expand_readers');
                  break;
                }
                case 'report_documents_pendent_reads':{
                  this.showFilter('filter_expand_readers');
                  break;
                }
                case 'report_top_revised_documents':{
                  this.showFilter('filter_truncate');
                  this.showFilter('filter_expand_documents');
                  break;
                }                
                case 'report_document_approvers':
                case 'report_doc_instance_comment_and_justification':
                case 'report_user_comments':
                case 'report_document_readers':{
                  this.showFilter('filter_document_list');
                  break;
                }
                case 'report_users_by_process':{
                  this.showFilter('filter_organize_by_area');
                  break;
                }
                case 'report_document_summary':{
                  this.showFilter('filter_document');
                  break;
                }
                case 'report_element_classification':{
                  this.showFilter('report_element_classification_filter');
                  break;
                }
                case 'report_pendant_task_pm':{
                  this.showFilter('report_pendant_tasks_filter');
                  break;
                }
                case 'report_docs_with_high_frequency_revision':
                case 'report_users_with_pendant_read':
                case 'report_documents_without_register':{
                  this.showFilter('filter_user_list');
                  break;
                }
                case 'report_documents_by_component':{
                  this.showFilter('report_documents_by_component_filter');
                  break;
                }
                case 'report_documents_by_state':{
                  this.showFilter('report_documents_by_state_filter');
                  break;
                }
                default: this.showFilter('no_filter_available');
              }
            }
          },
          'hideWarnings': function(){
            js_hide('warning');
            js_hide('report_document_summary_filter_warning');
            js_hide('element_type_filter_warning');
            js_hide('component_filter_warning');
          }
        };
        
        generic_set_documents = function(psReport,psDocuments){
          gebi('var_'+psReport+'_documents').value = psDocuments;
          trigger_event('populate_'+psReport+'_documents_select_event',3);
        }
        
        set_documents = function(psDocuments){generic_set_documents('filter_document_list',psDocuments);};
        
        set_users = function(psUsers){
          gebi('var_filter_user_list_users').value = psUsers;
          trigger_event('populate_filter_user_list_users_select_event',3);
        }
        
        set_document = function(piDocId){
          gebi('doc_id').value = piDocId;          
          trigger_event('populate_filter_document_event',3);
        }
        
        var soSelector = new FWDSelector();       
        soSelector.select('select_document_type');      
        
        gebi('doc_id').object = {
          'onRequiredCheckNotOk': function(){
            gobi('report_document_summary_filter_warning').show();
          }
        }
        
        gobi('component_controller').onRequiredCheckNotOk = function(){
          gobi('component_filter_warning').show();
        }
        
        gobi('select_element_type').onRequiredCheckNotOk = 
        gobi('select_document_type').onRequiredCheckNotOk = 
        gobi('select_register_type').onRequiredCheckNotOk = function(){
          gobi('element_type_filter_warning').show();
        }
        
        gobi('grid_reports').onRequiredCheckNotOk = function(){
          gobi('warning').show();
        }
        
      </script>
    <?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_report.xml");
?>