<?php
include_once "include.php";
include_once $handlers_ref . "grid/policy/QueryGridDocObsolete.php";
include_once $handlers_ref . "select/policy/QuerySelectDocumentType.php";


class ObsoleteDocumentsGrid extends FWDDrawGrid {

  protected $csGridName;
  
  public function __construct($psGridName){
    $this->csGridName = $psGridName;
  }
  
  public function drawItem(){
    switch($this->ciColumnIndex){
      case $this->getIndexByAlias('document_id'):{
        $msIcon = PMDocument::getDocumentIcon($this->getFieldValue('file_name'),$this->getFieldValue('is_link'));
        $this->coCellBox->setIconSrc($msIcon);
        return $this->coCellBox->draw();
      }
      case $this->getIndexByAlias('document_name'):{
        $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
        $miDocumentId = $this->getFieldValue('document_id');
        $mbIsLink = $this->getFieldValue('is_link');
        $msFilePath = $this->getFieldValue('file_path');
        $miDocInstance =  $this->getFieldValue('instance_id');
        if($mbIsLink){
          $this->coCellBox->setValue("<a href=javascript:read('".$miDocumentId."','".$miDocInstance."','link');>".$this->coCellBox->getValue()."</a>");
        }elseif($msFilePath && is_file($msFilePath)){
          $this->coCellBox->setValue("<a href=javascript:read('".$miDocumentId."','".$miDocInstance."','file');>".$this->coCellBox->getValue()."</a>");
        }
        return $this->coCellBox->draw();
      }
      case $this->getIndexByAlias('file_name'):{
        if($this->getFieldValue('is_link')){
          $this->coCellBox->setValue($this->getFieldValue('document_link'));
        }
        return parent::drawItem();
      }
      case $this->getIndexByAlias('document_classification'):{
        if($this->coCellBox->getValue()){
          $moContextClassification = new ISMSContextClassification();
          $moContextClassification->fetchById($this->coCellBox->getValue());
          $msContextClassification = $moContextClassification->getFieldValue('classif_name');
          $this->coCellBox->setValue($msContextClassification);
        }
        return $this->coCellBox->draw();
      }
      case $this->getIndexByAlias('document_version'):{
        if (!ISMSLib::getConfigById(GENERAL_DOCUMENTS_MANUAL_VERSIONING) || !$this->coCellBox->getValue()) {
          $this->coCellBox->setValue($this->getFieldValue('document_major_version').".".$this->getFieldValue('document_revision_version'));
        }
        return  parent::drawItem();
      }
      default:{
        return parent::drawItem();
      }
    }
  }
}

class FilterEvent extends FWDRunnable {
  public function run() {
    $moGrid = FWDWebLib::getObject('grid_obsolete');
    $moGrid->execEventPopulate(); 
  }
}

class DownloadFileEvent extends FWDRunnable {
  public function run() {
    $moWebLib = FWDWebLib::getInstance();
    $miDocumentId = $moWebLib->getObject('read_document_id')->getValue();
    $miUserId = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    $miDocInstanceId = $moWebLib->getObject('read_doc_instance_id')->getValue();
    
    $moDocument = new PMDocument();
    $moPMDocReadHistory = new PMDocReadHistory();
    $moPMDocReadHistory->setFieldValue('user_id',$miUserId);
    $moPMDocReadHistory->setFieldValue('document_id',$miDocInstanceId);
    $moPMDocReadHistory->setFieldValue('read_date',ISMSLib::ISMSTime());
    $moPMDocReadHistory->insert();
    
    $moPMDocument = new PMDocument();
    $moPMDocInstance = new PMDocInstance();
    if ($moPMDocInstance->fetchById($miDocInstanceId)) {
      $msPath = $moPMDocInstance->getFieldValue('doc_instance_path');
      $msFileName = $moPMDocInstance->getFieldValue('doc_instance_file_name');
      header('Cache-Control: ');// leave blank to avoid IE errors
      header('Pragma: ');// leave blank to avoid IE errors
      header('Content-type: application/octet-stream');
      header('Content-Disposition: attachment; filename="'.$msFileName.'"');
      $moCrypt = new FWDCrypt();
      $moFP = fopen($msPath,'rb');
      $moCrypt->setIV(fread($moFP,strlen($moCrypt->getIV())));
      while(!feof($moFP)) {
        echo $moCrypt->decryptNoBase64(fread($moFP,16384));
      }
      fclose($moFP);
    }
  }
}

class VisitLinkEvent extends FWDRunnable {
  public function run() {
    $moWebLib = FWDWebLib::getInstance();
    $moReader = new PMDocumentReader();
    $miDocInstanceId = $moWebLib->getObject('read_doc_instance_id')->getValue();
    
    $miDocumentId = $moWebLib->getObject('read_document_id')->getValue();
    $miUserId = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    
    $moPMDocReadHistory = new PMDocReadHistory();
    $moPMDocReadHistory->setFieldValue('user_id',$miUserId);
    $moPMDocReadHistory->setFieldValue('document_id',$miDocInstanceId);
    $moPMDocReadHistory->setFieldValue('read_date',ISMSLib::ISMSTime());
    $moPMDocReadHistory->insert();
    
    $moPMDocInstance = new PMDocInstance();
    if ($moPMDocInstance->fetchById($miDocInstanceId))
      $msURL = $moPMDocInstance->getFieldValue('doc_instance_link');
    if ($msURL)
      echo "js_open('{$msURL}','document_link',740,502,'center_screen');";
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new FilterEvent('filter_all'));
    $moStartEvent->addSubmitEvent(new DownloadFileEvent('download_file'));
    $moStartEvent->addAjaxEvent(new VisitLinkEvent('visit_link_event'));
    
    //instala a seguran�a de ACL na p�gina
    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
    
    $moGrid = FWDWebLib::getObject("grid_obsolete");
    $moHandler = new QueryGridDocObsolete(FWDWebLib::getConnection());
    $miType = (int) FWDWebLib::getObject('filter_type')->getValue();
    if($miType) $moHandler->setTypesToShow($miType);

    $moHandler->setTypesToHide(CONTEXT_REGISTER);
    $moHandler->setDocumentClassification((int) FWDWebLib::getObject('filter_classification')->getValue());

    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new ObsoleteDocumentsGrid('grid_obsolete'));
  }
}


class ScreenEvent extends FWDRunnable {
  public function run(){
    $moHandler = new QuerySelectDocumentType(FWDWebLib::getConnection());    
    $moSelect = FWDWebLib::getObject('select_document_classification');
    $moSelect->setQueryHandler($moHandler);
    $moSelect->populate();
    
    $moContext = new ISMSContextObject();   
    $moSelect = FWDWebLib::getObject('select_document_type');
    $moSelect->setItemValue(CONTEXT_NONE             ,FWDLanguage::getPHPStringValue('si_no_type','Sem Tipo'));
    $moSelect->setItemValue(CONTEXT_AREA             ,ISMSContextObject::getContextObject(CONTEXT_AREA)->getLabel());
    $moSelect->setItemValue(CONTEXT_PROCESS          ,ISMSContextObject::getContextObject(CONTEXT_PROCESS)->getLabel());
    $moSelect->setItemValue(CONTEXT_ASSET            ,ISMSContextObject::getContextObject(CONTEXT_ASSET)->getLabel());
    $moSelect->setItemValue(CONTEXT_CONTROL          ,ISMSContextObject::getContextObject(CONTEXT_CONTROL)->getLabel());
    $moSelect->setItemValue(CONTEXT_SCOPE            ,ISMSContextObject::getContextObject(CONTEXT_SCOPE)->getLabel());
    $moSelect->setItemValue(CONTEXT_POLICY           ,ISMSContextObject::getContextObject(CONTEXT_POLICY)->getLabel());
    $moSelect->setItemValue(DOCUMENT_TYPE_MANAGEMENT ,FWDLanguage::getPHPStringValue('si_management','Gest�o'));
    $moSelect->setItemValue(DOCUMENT_TYPE_OTHERS     ,FWDLanguage::getPHPStringValue('si_others','Outros'));
    if (FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(INCIDENT_MODE))
      $moSelect->setItemValue(CONTEXT_CI_ACTION_PLAN   ,ISMSContextObject::getContextObject(CONTEXT_CI_ACTION_PLAN)->getLabel());
                                                 
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        function refresh_grid(){
          js_refresh_grid('grid_obsolete');
        }
        function remove_document(){
          trigger_event('remove_document',3);
          refresh_grid();
        }
        function read(piDocumentId,piDocInstanceId,psType) {
          gebi('read_document_id').value=piDocumentId;
          gebi('read_doc_instance_id').value=piDocInstanceId;
          switch(psType) {
            case "link":
              trigger_event('visit_link_event',3);
              break;
            case "file":
              js_submit('download_file','ajax');
              break;
            default:
              gebi('read_document_id').value=0;
              break;
          }
        }
      </script>
    <?
  }
}
//testa se a licen�a utilizada permite acessar o m�dulo de documenta��o
  ISMSLib::testUserPermissionToPoliceMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_obsolete.xml");

?>