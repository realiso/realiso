<?php
include_once "include.php";
include_once $handlers_ref . "QueryTreeDocument.php";
include_once $handlers_ref . "QueryDocumentRevision.php";
include_once $handlers_ref . "grid/policy/QueryGridDocReference.php";
include_once $handlers_ref . "grid/policy/QueryGridInstanceComments.php";
include_once $handlers_ref . "grid/QueryGridUserTasks.php";

class TaskEvent extends FWDRunnable {
  public function run(){
    $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    $miDocumentId = FWDWebLib::getObject('document_id')->getValue();
    $miAction = FWDWebLib::getObject('action')->getValue();

    $moTask = new WKFTask();
    $moTask->createFilter($miDocumentId,'task_context_id');
    $moTask->createFilter($miUserId,'task_receiver_id');
    $moTask->createFilter(ACT_DOCUMENT_APPROVAL,'task_activity');
    $moTask->createFilter(true,'task_is_visible');
    $moTask->createFilter(null,'task_date_accomplished','null');
    $moTask->select();
    $moTask->fetch();
    $miTaskId = $moTask->getFieldValue('task_id');

    $moTask->testPermissionToExecute($miTaskId);

    $moDocument = new PMDocument();
    $moDocument->fetchById($miDocumentId);
    $moDocument->stateForward($miAction);

    $moTask = new WKFTask();
    $moTask->createFilter($miDocumentId,'task_context_id');
    $moTask->createFilter($miUserId,'task_receiver_id');
    $moTask->createFilter(ACT_DOCUMENT_APPROVAL,'task_activity');
    $moTask->createFilter(null,'task_date_accomplished','null');
    $moTask->setFieldValue('task_date_accomplished', ISMSLib::ISMSTime());
    $moTask->setFieldValue('task_is_visible', 0);
    $moTask->update($miTaskId);
    
    $msNextTaskJS = "";
		$mbOpenNextTask = (bool)array_filter(explode(':',FWDWebLib::getObject('next_task_controller')->getValue()));
		if ($mbOpenNextTask) {
		  $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
		  $moHandlerTask = new QueryGridUserTasks(FWDWebLib::getConnection());
      $moHandlerTask->setUser($miUserId);
      $moHandlerTask->setOrderBy('task_date_created','-');
      $moHandlerTask->makeQuery();
      $moHandlerTask->executeQuery();
      if ($moHandlerTask->fetch()) {
  		  $msNextTaskJS = 'soWindow.setTimeout("'.ISMSLib::getTaskPopup($moHandlerTask->getFieldValue('task_id'),
  		                                        $moHandlerTask->getFieldValue('task_activity'),
  		                                        $moHandlerTask->getFieldValue('task_context_id'),
  		                                        $moHandlerTask->getFieldValue('task_context_type'),
  		                                        2).',1");';
      }
		}
		
    echo "var soWindow = soPopUpManager.getPopUpById('popup_document_read').getOpener();"
        ."if(soWindow.refresh_grids) soWindow.refresh_grids();"
        ."else if(soWindow.refresh_grid) soWindow.refresh_grid();"
        .$msNextTaskJS
        ."soWindow=null;"
        ."soPopUpManager.closePopUp('popup_document_read');";
  }
}

class DownloadEvent extends FWDRunnable {
  public function run() {
    $moWebLib = FWDWebLib::getInstance();
    $miDocumentId = $moWebLib->getObject('document_id')->getValue();
    $miUserId = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    //$miDocInstanceId = $moWebLib->getObject('read_doc_instance_id')->getValue();

    $moDocument = new PMDocument();
    $moDocument->testPermissionToRead($miDocumentId);

    $moReader = new PMDocumentReader();
    $moReader->createFilter($miDocumentId,'document_id');
    $moReader->createFilter($miUserId,'user_id');
    $moReader->select();
    if ($moReader->fetch()) {
      $moReader->setFieldValue('user_has_read_document',1);
      $moReader->update();
    } else {
      //o usuario nao esta na lista de leitores
    }

    $moPMDocument = new PMDocument();
    if($moPMDocument->fetchById($miDocumentId)){
      $miCurrentVersion = $moPMDocument->getFieldValue('document_current_version');
      if(FWDWebLib::getObject('param_is_approve')->getValue()){
        $moDocInstance = $moPMDocument->getDeveloppingInstance();
      }else{
        $moDocInstance = $moPMDocument->getPublishedInstance();
      }
      if($moDocInstance){
        $moPMDocReadHistory = new PMDocReadHistory();
        $moPMDocReadHistory->setFieldValue('user_id',$miUserId);
        $moPMDocReadHistory->setFieldValue('document_id',$moDocInstance->getFieldValue('doc_instance_id'));
        $moPMDocReadHistory->setFieldValue('read_date',ISMSLib::ISMSTime());
        $moPMDocReadHistory->insert();



        $msPath = $moDocInstance->getFieldValue('doc_instance_path');
        $msFileName = $moDocInstance->getFieldValue('doc_instance_file_name');
        header('Cache-Control: ');// leave blank to avoid IE errors
        header('Pragma: ');// leave blank to avoid IE errors
        header('Content-type: application/octet-stream');
        header('Content-Disposition: attachment; filename="'.$msFileName.'"');
        $moCrypt = new FWDCrypt();
        $moFP = fopen($msPath,'rb');
        $moCrypt->setIV(fread($moFP,strlen($moCrypt->getIV())));
        while(!feof($moFP)) {
          echo $moCrypt->decryptNoBase64(fread($moFP,16384));
        }
        fclose($moFP);
      }
    }
  }
}

class SubDocumentNodeBuilder extends FWDNodeBuilder {
  public function buildNode($psId, $psValue) {
    $moNode = new FWDTree();
    $moNode->setAttrName($psId);

    // �cone do tipo do documento
		$msIcon = '';
		$moPMDocument = new PMDocument();
		$moPMDocument->fetchById($psId);
		$moIcon = new FWDIcon();
		$moIcon->setAttrSrc(FWDWebLib::getInstance()->getGfxRef().$moPMDocument->getIcon());

    // Link para ler subdocumento
    $moLink = new FWDStatic();
    $moLink->setAttrStringNoEscape("true");
    $msId = uniqid();
    $moLink->setValue("<a href='javascript:read_subdocument($psId,\"$msId\");'>$psValue</a>");

    // Adiciona objetos no nodo
    $moNode->addObjFWDView($moIcon);
    $moNode->addObjFWDView($moLink);

    return $moNode;
  }
}

class GridDocReference extends FWDDrawGrid {
  public function drawItem(){
    switch($this->ciColumnIndex){
    	case 2:
        $this->coCellBox->setValue(ISMSLib::getISMSDate($this->coCellBox->getValue()));
        return $this->coCellBox->draw();
      break;
      default:
        return $this->coCellBox->draw();
      break;
    }
  }
}

class GridDocComment extends FWDDrawGrid {
  public function drawItem(){
    switch($this->ciColumnIndex){
      case 2:
        $moUser = new ISMSUser();
        $moUser->fetchById($this->caData[4]);
        $this->coCellBox->setValue(ISMSLib::getISMSDate($this->coCellBox->getValue()));

        $moToolTip = new FWDToolTip();
        $moToolTip->setAttrShowDelay(1000);
        //$msToolTipText = str_replace('\\','\\\\',$moUser->getName());
        //$msToolTipText = str_replace('"','\"',$msToolTipText);
        $moToolTip->setValue($moUser->getName());
        $this->coCellBox->addObjFWDEvent($moToolTip);

        return $this->coCellBox->draw();
      break;
      default:
        return $this->coCellBox->draw();
      break;
    }
  }
}

class InstanceCommentInsertEvent extends FWDRunnable {
  public function run() {
    $moWebLib = FWDWebLib::getInstance();
    $miDocumentId = $moWebLib->getObject('document_id')->getValue();
    $miDocInstanceId = $moWebLib->getObject('doc_instance_id')->getValue();
    $miUserId = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    $msInstanceComment = $moWebLib->getObject('text_instance_comment')->getValue();

    $moPMDocument = new PMDocument();
    $moPMDocument->testPermissionToRead($miDocumentId);

    $moPMInstanceComment = new PMInstanceComment();
    $moPMInstanceComment->setFieldValue("instance_id",$miDocInstanceId);
    $moPMInstanceComment->setFieldValue("user_id",$miUserId);
    $moPMInstanceComment->setFieldValue("instance_comment",$msInstanceComment);
    $moPMInstanceComment->setFieldValue("instance_comment_date",ISMSLib::ISMSTime());
    $moPMInstanceComment->insert(true);

    $moPMDocument->fetchById($miDocumentId);
    $maAlertReceivers = array();
    $maAlertReceivers[] = $moPMDocument->getFieldValue('document_author');
    $maAlertReceivers = array_unique(array_merge($maAlertReceivers,$moPMDocument->getApprovers()));
    
    foreach($maAlertReceivers as $miReceiver) {
      if ($miReceiver == $miUserId) continue;
      $moAlert = new WKFAlert();
      $moAlert->setFieldValue('alert_receiver_id',$miReceiver);
      $moAlert->setFieldValue('alert_date',ISMSLib::ISMSTime());
      $moAlert->setFieldValue('alert_type',WKF_ALERT_DOCUMENT_COMMENT_CREATED);
      $moAlert->setFieldValue('alert_justification',' ');
      $moAlert->setFieldValue('alert_creator_id',$miUserId);
      $moAlert->setFieldValue('alert_context_id',$miDocumentId);
      $moAlert->insert();
    }
    echo "gebi('text_instance_comment').value = '';";
    echo "refresh_comment_grid();";
  }
}

class InstanceCommentConfirmRemove extends FWDRunnable {
  public function run(){
    $msTitle = FWDLanguage::getPHPStringValue('tt_remove_comment','Remover Coment�rio');
    $msMessage = FWDLanguage::getPHPStringValue('st_remove_comment_confirm',"Voc� tem certeza de que deseja excluir o coment�rio referido?");

    $moPMInstanceComment = new PMInstanceComment();
    $moPMInstanceComment->fetchById(FWDWebLib::getObject('instance_comment_id')->getValue());

    $msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
                    "soWindow = soPopUp.getOpener();" .
                    "soWindow.instance_comment_remove();";

    ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,50);
  }
}

class InstanceCommentRemoveEvent extends FWDRunnable {
  public function run(){
    $miDocumentId = FWDWebLib::getObject('document_id')->getValue();
    $moDocument = new PMDocument();
    $moDocument->testPermissionToRead($miDocumentId);

    $miInstanceCommentId = FWDWebLib::getObject('instance_comment_id')->getValue();
    $moPMInstanceComment = new PMInstanceComment();
    $moPMInstanceComment->delete($miInstanceCommentId);
  }
}

class VisitLinkEvent extends FWDRunnable {
  public function run() {
    $moWebLib = FWDWebLib::getInstance();
    $moReader = new PMDocumentReader();
    //$miDocInstanceId = $moWebLib->getObject('read_doc_instance_id')->getValue();

    $miDocumentId = $moWebLib->getObject('document_id')->getValue();
    $miUserId = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();

    $moDocument = new PMDocument();
    $moDocument->testPermissionToRead($miDocumentId);

    $moReader->createFilter($miDocumentId,'document_id');
    $moReader->createFilter($miUserId,'user_id');
    $moReader->select();
    if ($moReader->fetch()) {
      $moReader->setFieldValue('user_has_read_document',1);
      $moReader->update();
    } else {
      //o usuario nao esta na lista de leitores
    }

    $moPMDocument = new PMDocument();
    if($moPMDocument->fetchById($miDocumentId)){
      $miCurrentVersion = $moPMDocument->getFieldValue('document_current_version');
      if(FWDWebLib::getObject('param_is_approve')->getValue()){
        $moDocInstance = $moPMDocument->getDeveloppingInstance();
      }else{
        $moDocInstance = $moPMDocument->getPublishedInstance();
      }
      if($moDocInstance){
        $moPMDocReadHistory = new PMDocReadHistory();
        $moPMDocReadHistory->setFieldValue('user_id',$miUserId);
        $moPMDocReadHistory->setFieldValue('document_id',$moDocInstance->getFieldValue('doc_instance_id'));
        $moPMDocReadHistory->setFieldValue('read_date',ISMSLib::ISMSTime());
        $moPMDocReadHistory->insert();

        $moPMDocInstance = new PMDocInstance();
        if ($moPMDocInstance->fetchById($moDocInstance->getFieldValue('doc_instance_id')))
          $msURL = $moPMDocInstance->getFieldValue('doc_instance_link');
        if ($msURL)
          echo "js_open('{$msURL}','document_link',740,502,'center_screen');";
      }
    }
  }
}
class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();

    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addSubmitEvent(new DownloadEvent("download"));
    $moStartEvent->addAjaxEvent(new InstanceCommentInsertEvent("instance_comment_insert"));
    $moStartEvent->addAjaxEvent(new InstanceCommentConfirmRemove("instance_comment_confirm_remove"));
    $moStartEvent->addAjaxEvent(new InstanceCommentRemoveEvent("instance_comment_remove"));
    $moStartEvent->addAjaxEvent(new VisitLinkEvent("visit_link"));
    $moStartEvent->addAjaxEvent(new TaskEvent("task_event"));

    $moDialog = $moWebLib->getObject("dialog");
    $moWebLib->xml_load($moWebLib->getSysRef().'/packages/policy/document_read_general.xml',false);
    $moDialog->addObjFWDView($moWebLib->getObject("emutab1_viewgroup"));
    $moWebLib->xml_load($moWebLib->getSysRef().'/packages/policy/document_read_subdocuments.xml',false);
    $moDialog->addObjFWDView($moWebLib->getObject("emutab2_viewgroup"));
    $moWebLib->xml_load($moWebLib->getSysRef().'/packages/policy/document_read_references.xml',false);
    $moDialog->addObjFWDView($moWebLib->getObject("emutab3_viewgroup"));
    $moWebLib->xml_load($moWebLib->getSysRef().'/packages/policy/document_read_comments.xml',false);
    $moDialog->addObjFWDView($moWebLib->getObject("emutab4_viewgroup"));

    /*** REFERENCES ***/
    $moGrid = FWDWebLib::getObject("grid_doc_reference");
    $moHandler = new QueryGridDocReference(FWDWebLib::getConnection());
    $moHandler->setDocumentId(FWDWebLib::getObject('document_id')->getValue());
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new GridDocReference());

    /*** COMMENTS ***/
    $moGrid = FWDWebLib::getObject("grid_instance_comments");
    $moHandler = new QueryGridInstanceComments(FWDWebLib::getConnection());
    $moHandler->setInstanceId(FWDWebLib::getObject('doc_instance_id')->getValue());
    //$moHandler->setUserId($moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getUserId());
    $miUserResponsible = FWDWebLib::getObject('var_responsible')->getValue();
    if(!$miUserResponsible){
      $moDocument = new PMDocument();
      $moDocument->fetchById(FWDWebLib::getObject('doc_instance_id')->getValue());
      $miUserResponsible = $moDocument->getFieldValue('document_author');
      FWDWebLib::getObject('var_responsible')->setValue($miUserResponsible);
    }
    if($moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getUserId()==$miUserResponsible){
      $moHandler->setDocResponsible($miUserResponsible);
      $moGrid->setObjFwdDrawGrid(new GridDocComment());
    }else{
      $moGrid->setObjFwdDrawGrid(new GridDocReference());
    }

    $moGrid->setQueryHandler($moHandler);

  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $miAutoTaskOpen = FWDWebLib::getObject('auto_task_open')->getValue();
    if (!$miAutoTaskOpen) FWDWebLib::getObject('next_task')->setShouldDraw(false);
    if ($miAutoTaskOpen==2) FWDWebLib::getObject('next_task_controller')->checkItem(1);
    $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    $moWebLib = FWDWebLib::getInstance();
    $miDocumentId = $moWebLib->getObject('param_document_id')->getValue();
    $moWebLib->getObject('document_id')->setValue($miDocumentId);

    /*
     * Armazena o id que serve de sufixo para o identificador da popup.
     * Necess�rio para que essa janela n�o seja sobreescrita por uma
     * outra 'inst�ncia' dessa mesma janela.
     */
    $msPopupId = $moWebLib->getObject('param_popupid')->getValue();
    $moWebLib->getObject('popupid')->setValue($msPopupId);

    /*** GENERAL ***/
    if ($miDocumentId) {
      $moPMDocument = new PMDocument();
      $moPMDocument->fetchById($miDocumentId);

      $moPMDocument->testPermissionToRead($miDocumentId);

      $moWebLib->getObject('name')->setValue($moPMDocument->getFieldValue('document_name'));
      $moWebLib->getObject('description')->setValue($moPMDocument->getFieldValue('document_description'));
      $moWebLib->getObject('keywords')->setValue($moPMDocument->getFieldValue('document_keywords'));
      $moContextClassification = new ISMSContextClassification();
      $moContextClassification->fetchById($moPMDocument->getFieldValue('document_classification'));
      $moWebLib->getObject('classification')->setValue($moContextClassification->getFieldValue('classif_name'));
      $miAuthorId = $moPMDocument->getFieldValue('document_author');
      if($miAuthorId){
        //salva o id do autor do documento
        FWDWebLib::getObject('var_responsible')->setValue($miAuthorId);
        $moUser = new ISMSUser();
        if($moUser->fetchById($miAuthorId))
          $moWebLib->getObject('author')->setValue($moUser->getName());
      }
      $miCurrentVersion = $moPMDocument->getFieldValue('document_current_version');

      $msVersion = '0.0';
      $msFileName = '';
      $msFinalFileSize = '';
      $msFileDate = '';

      if(FWDWebLib::getObject('param_is_approve')->getValue()){
        $moDocInstance = $moPMDocument->getDeveloppingInstance();
      }else{
        $moDocInstance = $moPMDocument->getPublishedInstance();
      }

      if($moDocInstance){
        $moWebLib->getObject('doc_instance_id')->setValue($moDocInstance->getFieldValue('doc_instance_id'));
        $msVersion = $moDocInstance->getVersion();

        $mbIsLink = $moDocInstance->getFieldValue('doc_instance_is_link');
        if ($mbIsLink) {
          $msFileName = $moDocInstance->getFieldValue('doc_instance_link');
          $moWebLib->getObject('label_link')->setAttrDisplay("true");
          $moWebLib->getObject('label_file_name')->setAttrDisplay("false");
          $moWebLib->getObject('label_file_size')->setAttrDisplay("false");
          $moWebLib->getObject('file_size')->setAttrDisplay("false");
          $moWebLib->getObject('label_file_date')->setAttrDisplay("false");
          $moWebLib->getObject('file_date')->setAttrDisplay("false");
          $moWebLib->getObject('visit_link_button')->setAttrDisplay("true");
          $moWebLib->getObject('download_button')->setAttrDisplay("false");
        } else {
          $msPath = $moDocInstance->getFieldValue('doc_instance_path');
          if ($msPath && is_file($msPath)) {
            $msFileName = $moDocInstance->getFieldValue('doc_instance_file_name');
            $miFileSize = $moDocInstance->getFieldValue('doc_instance_file_size');
            $maSizeName = array("GB", "MB", "KB", "B");
            $msFinalFileSize = number_format($miFileSize,2)."&nbsp;".array_pop($maSizeName);
            while (($miFileSize/1024)>1) {
             $miFileSize=$miFileSize/1024;
             $msFinalFileSize = number_format($miFileSize,2)."&nbsp;".array_pop($maSizeName);
            }
            $msFileDate=date("d/m/Y",filectime($msPath));
          } else {
            $moWebLib->getObject('download_button')->setAttrDisplay("false");
          }
        }
        $moWebLib->getObject('version')->setValue($msVersion);
        $moWebLib->getObject('file_name')->setValue($msFileName);
        $moWebLib->getObject('file_size')->setValue($msFinalFileSize);
        $moWebLib->getObject('file_date')->setValue($msFileDate);
      }else{
        $moWebLib->getObject('download_button')->setAttrDisplay("false");
      }
    }

    /*** SUBDOCUMENTS ***/
    $moDocument = new PMDocument();
    if ($miDocumentId && $moDocument->fetchById($miDocumentId)) {
      $moHandler = new QueryTreeDocument($moWebLib->getConnection());
      $moHandler->setRootDocument($miDocumentId);
      $moHandler->setUserId($miUserId);
      $moHandler->makeQuery();
      $moHandler->executeQuery();
      $moDbTree = $moWebLib->getObject('subdocuments_tree');

      // �cone do tipo do documento
			$moIcon = new FWDIcon();
		  $moIcon->setAttrSrc(FWDWebLib::getInstance()->getGfxRef().$moDocument->getIcon());
      $moDbTree->addObjFWDView($moIcon);

      // Nome do documento
      $moStatic = new FWDStatic();
      $moStatic->setValue($moDocument->getFieldValue('document_name'));
      $moDbTree->addObjFWDView($moStatic);

      $moDbTree->setDataSet($moHandler->getDataSet());
      $moDbTree->setNodeBuilder(new SubDocumentNodeBuilder());
      $moDbTree->buildTree();
    }

    /*** REFERENCES - ScreenBeforeEvent***/
    /*** COMMENTS - ScreenBeforeEvent***/

    if(FWDWebLib::getObject('param_is_approve')->getValue()){
      $msTitle = FWDLanguage::getPHPStringValue('tt_approve_document','Aprovar Documento');
      FWDWebLib::getObject('popup_title')->setValue($msTitle);
      FWDWebLib::getObject('viewbutton_approve')->setAttrDisplay('true');
      FWDWebLib::getObject('viewbutton_deny')->setAttrDisplay('true');
    }

    $moWebLib->dump_html($moWebLib->getObject('dialog'));
    ?>
      <script language="javascript">
        ssSelectedEmuTab='emutab1';
        function change_emutab(emutab) {
          js_change_class(ssSelectedEmuTab,'FWDSubTabItem_notSelected');
          if (gebi(ssSelectedEmuTab+'_viewgroup'))
            js_hide(ssSelectedEmuTab + '_viewgroup');
          ssSelectedEmuTab=emutab;
          if (gebi(ssSelectedEmuTab+'_viewgroup'))
            js_show(ssSelectedEmuTab + '_viewgroup');
          js_change_class(ssSelectedEmuTab,'FWDSubTabItem_selected');
        }

        function read_subdocument(piSubDocumentId, psPopupId) {
          isms_open_popup('popup_document_read'+psPopupId,'packages/policy/popup_document_read.php?document='+piSubDocumentId+'&popupid='+psPopupId,'','true',452,500);
        }

        function refresh_comment_grid() {
          js_refresh_grid('grid_instance_comments');
        }

        function instance_comment_remove() {
          trigger_event('instance_comment_remove', 3);
          refresh_comment_grid();
        }
      </script>
    <?
  }
}

//testa se a licen�a utilizada permite acessar o m�dulo de documenta��o
ISMSLib::testUserPermissionToPoliceMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_document_read.xml");
?>