<?php
include_once "include.php";
include_once $handlers_ref . "QueryTreeDocument.php";

class SubDocumentNodeBuilder extends FWDNodeBuilder {
  public function buildNode($psId, $psValue) {
    $moNode = new FWDTree();
    $moNode->setAttrName($psId);
    
    // �cone do tipo do documento
    $msIcon = '';
    $moPMDocument = new PMDocument();
    $moPMDocument->fetchById($psId);  
    $moIcon = new FWDIcon();
    $moIcon->setAttrSrc(FWDWebLib::getInstance()->getGfxRef().$moPMDocument->getIcon());

    $moPMDocInstance = $moPMDocument->getPublishedInstance();
    $miDocInstanceId = $moPMDocInstance->getFieldValue('doc_instance_id');
    $msType = $moPMDocInstance->getFieldValue('doc_instance_is_link')?"link":"file";
    
    // Link para ler subdocumento
    $moLink = new FWDStatic();
    $moLink->setAttrStringNoEscape("true");
    $moLink->setValue("<a href='javascript:read($psId,$miDocInstanceId,\"$msType\");'>$psValue</a>");
    
    // Adiciona objetos no nodo    
    $moNode->addObjFWDView($moIcon);
    $moNode->addObjFWDView($moLink);
    
    return $moNode;
  }
}

class DownloadFileEvent extends FWDRunnable {
  public function run() {
    $moWebLib = FWDWebLib::getInstance();
    $miDocumentId = $moWebLib->getObject('read_document_id')->getValue();
    $miUserId = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    $miDocInstanceId = $moWebLib->getObject('read_doc_instance_id')->getValue();
    
    $moDocument = new PMDocument();
    $moDocument->testPermissionToRead($miDocumentId);
    
    $moReader = new PMDocumentReader();
    $moReader->createFilter($miDocumentId,'document_id');
    $moReader->createFilter($miUserId,'user_id');
    $moReader->select();
    if ($moReader->fetch()) {
      $moReader->setFieldValue('user_has_read_document',1);
      $moReader->update();
    } else {
      //o usuario nao esta na lista de leitores
    }
    
    $moPMDocReadHistory = new PMDocReadHistory();
    $moPMDocReadHistory->setFieldValue('user_id',$miUserId);
    $moPMDocReadHistory->setFieldValue('document_id',$miDocInstanceId);
    $moPMDocReadHistory->setFieldValue('read_date',ISMSLib::ISMSTime());
    $moPMDocReadHistory->insert();
    
    $moPMDocument = new PMDocument();
    
    if($moPMDocument->fetchById($miDocumentId)){
      $moPMDocInstance = new PMDocInstance();
      if ($moPMDocInstance->fetchById($miDocInstanceId)) {
        $msPath = $moPMDocInstance->getFieldValue('doc_instance_path');
        $msFileName = $moPMDocInstance->getFieldValue('doc_instance_file_name');
        header('Cache-Control: ');// leave blank to avoid IE errors
        header('Pragma: ');// leave blank to avoid IE errors
        header('Content-type: application/octet-stream');
        header('Content-Disposition: attachment; filename="'.$msFileName.'"');
        $moCrypt = new FWDCrypt();
        $moFP = fopen($msPath,'rb');
        $moCrypt->setIV(fread($moFP,strlen($moCrypt->getIV())));
        while(!feof($moFP)) {
          echo $moCrypt->decryptNoBase64(fread($moFP,16384));
        }
        fclose($moFP);
      }
    }
  }
}

class VisitLinkEvent extends FWDRunnable {
  public function run() {
    $moWebLib = FWDWebLib::getInstance();
    $moReader = new PMDocumentReader();
    $miDocInstanceId = $moWebLib->getObject('read_doc_instance_id')->getValue();
    
    $miDocumentId = $moWebLib->getObject('read_document_id')->getValue();
    $miUserId = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    
    $moDocument = new PMDocument();
    $moDocument->testPermissionToRead($miDocumentId);
    
    $moReader->createFilter($miDocumentId,'document_id');
    $moReader->createFilter($miUserId,'user_id');
    $moReader->select();
    if ($moReader->fetch()) {
      $moReader->setFieldValue('user_has_read_document',1);
      $moReader->update();
    } else {
      //o usuario nao esta na lista de leitores
    }
    
    $moPMDocReadHistory = new PMDocReadHistory();
    $moPMDocReadHistory->setFieldValue('user_id',$miUserId);
    $moPMDocReadHistory->setFieldValue('document_id',$miDocInstanceId);
    $moPMDocReadHistory->setFieldValue('read_date',ISMSLib::ISMSTime());
    $moPMDocReadHistory->insert();
    
    $moPMDocInstance = new PMDocInstance();
    if ($moPMDocInstance->fetchById($miDocInstanceId))
      $msURL = $moPMDocInstance->getFieldValue('doc_instance_link');
    if ($msURL)
      echo "js_open('{$msURL}','document_link',740,502,'center_screen');";
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addSubmitEvent(new DownloadFileEvent('download_file'));
    $moStartEvent->addAjaxEvent(new VisitLinkEvent('visit_link_event'));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $miDocumentId = FWDWebLib::getObject('document_id')->getValue();
    $moPMDocument = new PMDocument();
    $moPMDocument->fetchById($miDocumentId);
    
    if($moPMDocument->getFieldValue('document_type')!=CONTEXT_BEST_PRACTICE){
      $moPMDocument->testPermissionToRead($miDocumentId);
    }
    
    $moWebLib = FWDWebLib::getInstance();
    $moDocument = new PMDocument();
    if ($miDocumentId && $moDocument->fetchById($miDocumentId)) {      
      $moHandler = new QueryTreeDocument($moWebLib->getConnection());
      $moHandler->setRootDocument($miDocumentId);
      $moHandler->setPublishedDocuments(true);
      $moHandler->setUserId($moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getUserId());
      $moHandler->makeQuery();
      $moHandler->executeQuery();
      $moDbTree = $moWebLib->getObject('subdocuments_tree');
      
      // �cone do tipo do documento       
      $moIcon = new FWDIcon();
      $moIcon->setAttrSrc(FWDWebLib::getInstance()->getGfxRef().$moDocument->getIcon());
      $moDbTree->addObjFWDView($moIcon);
      
      // Nome do documento
      $moStatic = new FWDStatic();
      $moStatic->setValue($moDocument->getFieldValue('document_name'));
      $moDbTree->addObjFWDView($moStatic);
      
      $moDbTree->setDataSet($moHandler->getDataSet());
      $moDbTree->setNodeBuilder(new SubDocumentNodeBuilder());
      $moDbTree->buildTree();
    }
    $moWebLib->dump_html($moWebLib->getObject('dialog'));
    ?>
    <script language="javascript">
    function refresh_grid() {
      js_refresh_grid('grid_doc_reference');    
    }
    
    function read_subdocument(piSubDocumentId, psPopupId) {
      isms_open_popup('popup_document_read'+psPopupId,'packages/policy/popup_document_read.php?document='+piSubDocumentId+'&popupid='+psPopupId,'','true',452,500);
    }
    
    function read(piDocumentId,piDocInstanceId,psType) {
      gebi('read_document_id').value=piDocumentId;
      gebi('read_doc_instance_id').value=piDocInstanceId;
      switch(psType) {
        case "link":
          trigger_event('visit_link_event',3);
          break;
        case "file":
          js_submit('download_file','ajax');
          break;
        default:
          gebi('read_document_id').value=0;
          break;
      }
    }
    </script>
  <?
  }
}

//testa se a licen�a utilizada permite acessar o m�dulo de documenta��o
ISMSLib::testUserPermissionToPoliceMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_document_read_subdocuments.xml");
?>
