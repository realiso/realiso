<?php

include_once 'include.php';
include_once $handlers_ref.'grid/policy/QueryGridDocInstance.php';

class DocInstancesDrawGrid extends FWDDrawGrid {
  public function drawItem(){
    switch($this->ciColumnIndex){
      case 2:
        if (ISMSLib::getConfigById(GENERAL_DOCUMENTS_MANUAL_VERSIONING) && $this->getFieldValue('doc_instance_manual_version')) {
          $this->coCellBox->setValue($this->getFieldValue('doc_instance_manual_version'));
        } else {
          $this->coCellBox->setValue($this->caData[2].'.'.$this->caData[3]);
        }
        return $this->coCellBox->draw();
      case 4:
      case 5:
        $msDate = $this->caData[$this->ciColumnIndex];
        if($msDate){
          $this->coCellBox->setValue(ISMSLib::getISMSShortDate($msDate));
        }
        return $this->coCellBox->draw();
      default:
        return parent::drawItem();
    }
  }
}

class CopyConfirmEvent extends FWDRunnable {
  public function run(){
    $msEventValue = "soPopUpManager.getPopUpById('popup_document_instances').getWindow().trigger_event('copy',3);";
    $msTitle = FWDLanguage::getPHPStringValue('tt_copy_previous_version','Copiar vers�o anterior');
    $msMessage = FWDLanguage::getPHPStringValue('st_copy_previous_version_confirm','Tem certeza de que deseja copiar a vers�o anterior?');
    ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,50);
  }
}

class CopyEvent extends FWDRunnable {
  public function run(){
    $miDocInstanceId = FWDWebLib::getObject('doc_instance')->getValue();
    $moOldInstance = new PMDocInstance();
    $moOldInstance->fetchById($miDocInstanceId);
    
    $miDocumentId = $moOldInstance->getFieldValue('document_id');

    $moDocument = new PMDocument();
    $moDocument->testPermissionToEdit($miDocumentId);
    
    $moDevInstance = $moDocument->getDeveloppingInstance($miDocumentId);
    $miDevInstanceId = $moDevInstance->getFieldValue('doc_instance_id');
    
    $msCurrentFilePath = $moDevInstance->getFieldValue('doc_instance_path');
    if(file_exists($msCurrentFilePath)){
      unlink($msCurrentFilePath);
    }
    
    $moDevInstance = new PMDocInstance();
    $msOldFilePath = $moOldInstance->getFieldValue('doc_instance_path');
    if(file_exists($msOldFilePath)){
      $msFilesRef = ISMSLib::isCompiled() ? $GLOBALS['files_ref'] : 'files/';
      $msFilePath = $msFilesRef.md5(uniqid()).'.aef';
      copy($msOldFilePath,$msFilePath);
    }else{
      $msFilePath = '';
    }
    
    $moDevInstance->setFieldValue('doc_instance_is_link',$moOldInstance->getFieldValue('doc_instance_is_link'));
    $moDevInstance->setFieldValue('doc_instance_link',$moOldInstance->getFieldValue('doc_instance_link'));
    $moDevInstance->setFieldValue('doc_instance_path',$msFilePath);
    $moDevInstance->setFieldValue('doc_instance_file_name',$moOldInstance->getFieldValue('doc_instance_file_name'));
    $moDevInstance->update($miDevInstanceId);
    
    $moInstanceContent = new PMInstanceContent();
    if($moInstanceContent->fetchById($miDocInstanceId)){
      $moDevInstanceContent = new PMInstanceContent();
      if($moDevInstanceContent->fetchById($miDevInstanceId)){
        $moDevInstanceContent->setFieldValue('instance_content',$moInstanceContent->getFieldValue('instance_content'));
        $moDevInstanceContent->update($miDevInstanceId);
      }else{
        $moDevInstanceContent->setFieldValue('instance_id',$miDevInstanceId);
        $moDevInstanceContent->setFieldValue('instance_content',$moInstanceContent->getFieldValue('instance_content'));
        $moDevInstanceContent->insert();
      }
    }
    echo "soPopUpManager.getPopUpById('popup_document_instances').getOpener().refresh_grid();"
        ."soPopUpManager.closePopUp('popup_document_instances');";
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
    $moStartEvent->addAjaxEvent(new CopyConfirmEvent('copy_confirm'));
    $moStartEvent->addAjaxEvent(new CopyEvent('copy'));
    
    $moGrid = FWDWebLib::getObject('grid_doc_instances');
    $moHandler = new QueryGridDocInstance(FWDWebLib::getConnection());
    $miDocumentId = FWDWebLib::getObject('param_document_id')->getValue();
    $moHandler->setDocumentId($miDocumentId);
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new DocInstancesDrawGrid());
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $miDocumentId = FWDWebLib::getObject('param_document_id')->getValue();
    $moDocument = new PMDocument();
    $moDocument->testPermissionToEdit($miDocumentId);
  
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        gobi('grid_doc_instances').setSelectionRequired(true);
      </script>
    <?
  }
}
//testa se a licen�a utilizada permite acessar o m�dulo de documenta��o
ISMSLib::testUserPermissionToPoliceMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_document_instances.xml');

?>