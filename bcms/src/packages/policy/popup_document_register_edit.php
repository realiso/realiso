<?php

include_once "include.php";
include_once $handlers_ref . "select/policy/QuerySelectRegisterType.php";

class SaveEvent extends FWDRunnable {
  public function run() {
    /*nao deixa salvar com o tempo de reten��o ='0'*/
    if( (FWDWebLib::getObject('retention_value')->getValue()<=0)){
      echo " gobi('warning_retention_time').show();";
      exit();
    }

    $miRegisterId = FWDWebLib::getObject('register_id')->getValue();
    $miDocumentId = FWDWebLib::getObject('register_document_id')->getValue();
    
    $mbEdit = ($miRegisterId?true:false);

    $msName = FWDWebLib::getObject('name_register')->getValue();
    $miResponsibleId = FWDWebLib::getObject('register_responsible_id')->getValue();
    $miClassification = FWDWebLib::getObject('classification')->getValue();
    $miPeriod = FWDWebLib::getObject('retention_period')->getValue();
    $miValue = FWDWebLib::getObject('retention_value')->getValue();
    $msStoragePlace = FWDWebLib::getObject('storage_place')->getValue();
    $msStorageType = FWDWebLib::getObject('storage_type')->getValue();
    $msIndexingType = FWDWebLib::getObject('indexing_type')->getValue();
    $msDisposition = FWDWebLib::getObject('disposition')->getValue();
    $msProtectionRequirements = FWDWebLib::getObject('protection_requirements')->getValue();
    $msOrigin = FWDWebLib::getObject('origin')->getValue();

    $moPMRegister = new PMRegister();
    $moPMRegister->setFieldValue('register_name',$msName);
    $moPMRegister->setFieldValue('register_responsible',$miResponsibleId);
    if($miClassification){
      $moPMRegister->setFieldValue('register_classification',$miClassification);
    }else{
      $moPMRegister->setFieldValue('register_classification','null'); 
    }
    $moPMRegister->setFieldValue('register_period',$miPeriod);
    $moPMRegister->setFieldValue('register_value',$miValue);
    $moPMRegister->setFieldValue('register_storage_place',$msStoragePlace);
    $moPMRegister->setFieldValue('register_storage_type',$msStorageType);
    $moPMRegister->setFieldValue('register_indexing_type',$msIndexingType);
    $moPMRegister->setFieldValue('register_disposition',$msDisposition);
    $moPMRegister->setFieldValue('register_prot_requirements',$msProtectionRequirements);
    $moPMRegister->setFieldValue('register_origin',$msOrigin);

    $moPMRegisterSelect = new PMRegister();
    if($moPMRegisterSelect->fetchById($miRegisterId)){
      $moPMRegister->update($miRegisterId);
    }else{
      $moPMDocument = new PMDocument();
      if($miDocumentId){
        $moPMDocument->testPermissionToEdit($miDocumentId);
      }else{
        $moPMRegister->testPermissionToInsert();
      }
      $miRegisterId = $moPMRegister->insert(true);
    }

    if($miRegisterId && $miDocumentId){
      $moPMDocRegister = new PMDocRegisters();
      $moPMDocRegister->createFilter($miRegisterId,'register_id');
      $moPMDocRegister->createFilter($miDocumentId,'document_id');
      $moPMDocRegister->select();
      if(!$moPMDocRegister->fetch()){
        $moPMDocRegister->setFieldValue('register_id',$miRegisterId);
        $moPMDocRegister->setFieldValue('document_id',$miDocumentId);
        $moPMDocRegister->insert();
        
        $moRegister = new PMRegister();
        $moRegister->setFieldValue("document_id",$miDocumentId);
        $moRegister->update($miRegisterId);
      }
      
      /*seta o tipo do documento para ser do tipo registro*/
      $moDocument = new PMDocument();
      $moDocument->setFieldValue('document_type',CONTEXT_REGISTER);
      
      /*se foi trocado o respons�vel pelo registro, deve-se alterar o author do documento*/
      if( FWDWebLib::getObject('pog_user_id')->getValue() != $miResponsibleId){
        $moDocument->setFieldValue('document_author',$miResponsibleId);
      }
      $moDocument->update($miDocumentId);
    }

    if(!$mbEdit){
      /*
      o documento do registro deve possuir como autor o mesmo usu�rio que o respons�vel pelo  registro
        foi adicionado o teste abaixo porque, se ao criar um registro, o respons�vel pelo registro n�o
        for o usu�rio logado iria dar erro de permiss�o ao abrir a popup de edi��o de documentos

        caso estaja em edi��o, e for trocado o respons�vel pelo registro, deve-se trocar tamb�m o respons�vel pelo documento
        pois os 2 devem ser a mesma pessoa!
      */

      if($miResponsibleId == FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId() && !FWDWebLib::getObject('param_document_id')->getValue()){
        echo "isms_open_popup('popup_create_document_confirm','packages/policy/popup_create_document_confirm.php?register='+$miRegisterId,'','true');";
      }else{
        echo "
              soWindow = soPopUpManager.getPopUpById('popup_document_register_edit').getOpener();
              if(soWindow.refresh_grid) soWindow.refresh_grid();
              soPopUpManager.closePopUp('popup_document_register_edit');
             ";
      }
    }else{
      echo "
            soWindow = soPopUpManager.getPopUpById('popup_document_register_edit').getOpener();
            if(soWindow.refresh_grid) soWindow.refresh_grid();
            soPopUpManager.closePopUp('popup_document_register_edit');
           ";
    }
  }
}

class ChangeDocumentInformation extends FWDRunnable {
  public function run(){
  $msDisabledEdit ='';
	$miRegisterId = FWDWebLib::getObject('register_id')->getValue();
  $miDocumentId = FWDWebLib::getObject('register_document_id')->getValue();
  $miDocumentId2 = FWDWebLib::getObject('document_id')->getValue();
  
  $miDocId = $miDocumentId? $miDocumentId : $miDocumentId2;
  
	$moDocument = new PMDocument();
	if($moDocument->fetchById($miDocId)){
	  $msDisabledEdit .= " if (gobi('vb_doc_lookup')) gobi('vb_doc_lookup').hide();";
    
	  $msDocName = $moDocument->getName();
	  $mbHasApproved = $moDocument->getFieldByAlias('document_has_approved')->getValue();
	  if($mbHasApproved){
	   $msDisabledEdit .= " if (gobi('vb_doc_edit')) gobi('vb_doc_edit').hide();";
	   $msDisabledEdit .= " gobi('vb_doc_read').show();";
	  }else{
	   $msDisabledEdit .= " if (gobi('vb_doc_edit')) gobi('vb_doc_edit').show();";
	   $msDisabledEdit .= " gobi('vb_doc_read').hide();";
	  }
	  $msDisabledEdit .= " gobi('label_document_create_edit').setValue('". FWDLanguage::getPHPStringValue("vb_edit","Editar") ."'); ";
	  echo " gobi('document_name').setValue('$msDocName'); $msDisabledEdit";
	}
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new SaveEvent("save"));
    $moStartEvent->addAjaxEvent(new ChangeDocumentInformation("change_document_information"));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $miRegisterId = FWDWebLib::getObject('param_register_id')->getValue();
    FWDWebLib::getObject('register_id')->setValue($miRegisterId);
    $miDocumentId = FWDWebLib::getObject('param_document_id')->getValue();
    if($miDocumentId)
    	FWDWebLib::getObject('register_document_id')->setValue($miDocumentId);

    $moPMRegister = new PMRegister();
    if($miRegisterId){
      $moPMRegister->testPermissionToEdit($miRegisterId);
    }else{
      $moPMDocument = new PMDocument();
      if($miDocumentId){
        $moPMDocument->testPermissionToEdit($miDocumentId);
      }else{
        $moPMRegister->testPermissionToInsert();
      }
    }

    $msPopupId = uniqid();
    FWDWebLib::getObject('popupid')->setValue($msPopupId);

    /*
     * Popula o select com os tipos de classifica��o de um registro.
     */
    $moHandler = new QuerySelectRegisterType(FWDWebLib::getConnection());
    $moSelect = FWDWebLib::getObject('classification');
    $moSelect->setQueryHandler($moHandler);
    $moSelect->populate();

    // Preenche os campos com os dados do registro.
    if($miRegisterId && $moPMRegister->fetchById($miRegisterId)){
      FWDWebLib::getObject('title')->setValue(FWDLanguage::getPHPStringValue("tt_register_editing","Edi��o de Registro"));
      $msName = $moPMRegister->getFieldValue("register_name");
      $miClassification = $moPMRegister->getFieldValue("register_classification");
      $miPeriod = $moPMRegister->getFieldValue("register_period");
      $miValue = $moPMRegister->getFieldValue("register_value");
      $msStoragePlace = $moPMRegister->getFieldValue("register_storage_place");
      $msStorageType = $moPMRegister->getFieldValue("register_storage_type");
      $miRegisterDocumentId = $moPMRegister->getFieldValue("document_id");
      $msIndexingType = $moPMRegister->getFieldValue("register_indexing_type");
      $msDisposition = $moPMRegister->getFieldValue("register_disposition");
      $msProtectionRequirements = $moPMRegister->getFieldValue("register_prot_requirements");
      $msOrigin = $moPMRegister->getFieldValue("register_origin");

      /*
      guarda o id do usu�rio resons�vel pelo registro. se for alterado o respons�vel,
      deve-se alterar o author dos documentos relacionados ao registro
     */
      FWDWebLib::getObject('pog_user_id')->setValue($moPMRegister->getFieldValue('register_responsible'));

      FWDWebLib::getObject('name_register')->setValue($msName);
      FWDWebLib::getObject('classification')->checkItem($miClassification);
      FWDWebLib::getObject('retention_value')->setValue($miValue);
      FWDWebLib::getObject('retention_period')->checkItem($miPeriod);
      FWDWebLib::getObject('storage_place')->setValue($msStoragePlace);
      FWDWebLib::getObject('storage_type')->setValue($msStorageType);
      FWDWebLib::getObject('indexing_type')->setValue($msIndexingType);
      FWDWebLib::getObject('disposition')->setValue($msDisposition);
      FWDWebLib::getObject('protection_requirements')->setValue($msProtectionRequirements);
      FWDWebLib::getObject('origin')->setValue($msOrigin);

      $miResponsibleId = $moPMRegister->getFieldValue('register_responsible');
      $moUser = new ISMSUser();
      $moUser->fetchById($miResponsibleId);
      FWDWebLib::getObject('register_responsible_id')->setValue($miResponsibleId);
      FWDWebLib::getObject('register_responsible_name')->setValue($moUser->getFieldValue('user_name'));

      $moPMDocument = new PMDocument();
      if($miRegisterDocumentId && $moPMDocument->fetchById($miRegisterDocumentId)){
        $msDocumentName = $moPMDocument->getFieldValue("document_name");
        FWDWebLib::getObject('document_name')->setValue($msDocumentName);
        FWDWebLib::getObject('register_document_id')->setValue($miRegisterDocumentId);
        FWDWebLib::getObject('vb_doc_lookup')->setShouldDraw(false);
        
        if($moPMDocument->getFieldValue('document_has_approved')){
          FWDWebLib::getObject('vb_doc_edit')->setShouldDraw(false);
          FWDWebLib::getObject('vb_doc_read')->setAttrDisplay('true');
        }else{
          FWDWebLib::getObject('vb_doc_read')->setAttrDisplay('false');
        }
      }else{
        FWDWebLib::getObject('label_document_create_edit')->setValue(FWDLanguage::getPHPStringValue("vb_create","Criar"));
        FWDWebLib::getObject('vb_doc_read')->setAttrDisplay('false');
      }
      FWDWebLib::getObject('viewgroup_document')->setAttrDisplay("true");
    }else{
      $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
      $moUser = new ISMSUser();
      $moUser->fetchById($miUserId);
      FWDWebLib::getObject('register_responsible_id')->setValue($miUserId);
      FWDWebLib::getObject('register_responsible_name')->setValue($moUser->getFieldValue('user_name'));
      FWDWebLib::getObject('title')->setValue(FWDLanguage::getPHPStringValue("tt_register_adding","Adi��o de Registro"));
    }
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        gebi('name_register').focus();
      
        function set_user(piId,psName){
          gebi('register_responsible_id').value = piId;
          gobi('register_responsible_name').setValue(psName);
        }
        
        //Comentei a funcao abaixo pois ela fazia o mesmo da funcao set_document.
        /*function change_doc_info(piDocId){
          gebi('register_document_id').value = piDocId;
          setTimeout("trigger_event('change_document_information',3);",600);
        }*/
        
        set_document = change_doc_info = function(piDocumentId){
          gebi('register_document_id').value = piDocumentId;
          setTimeout("trigger_event('change_document_information',3);",600);
        }
      </script>
    <?
  }
}

//testa se a licen�a utilizada permite acessar o m�dulo de documenta��o
ISMSLib::testUserPermissionToPoliceMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_document_register_edit.xml');

?>