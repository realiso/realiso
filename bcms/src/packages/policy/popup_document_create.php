<?php
include_once "include.php";

class SaveDocumentEvent extends FWDRunnable {
  public function run() {
    $moWebLib = FWDWebLib::getInstance();
    
    $miContextId = $moWebLib->getObject('associated_context_id')->getValue();
    
    $miType = CONTEXT_NONE;
    if ($miContextId) {
      $moContextObject = new ISMSContextObject();
      $moContext = $moContextObject->getContextObjectByContextId($miContextId);
      $miType = $moContext->getContextType();
    }

    $moPMDocument = new PMDocument();
    $moPMDocument->testPermissionToInsert();
    
    $miAuthorId = $miMainApproverId = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    $msName = $moWebLib->getObject('document_name')->getValue();
    $msDescription = $moWebLib->getObject('document_description')->getValue();
    $msKeywords = $moWebLib->getObject('document_keywords')->getValue();
    
    $moPMDocument->setFieldValue('document_author',$miAuthorId);
    $moPMDocument->setFieldValue('document_main_approver',$miMainApproverId);
    $moPMDocument->setFieldValue('document_name',$msName);
    $moPMDocument->setFieldValue('document_description',$msDescription);
    $moPMDocument->setFieldValue('document_type',$miType);
    $moPMDocument->setFieldValue('document_keywords',$msKeywords);
    
    $miDocumentId = $moPMDocument->insert(true);
    
    if ($miContextId) {
      $moDocContext = new PMDocContext();
      $moDocContext->setFieldValue('document_id',$miDocumentId);
      $moDocContext->setFieldValue('context_id',$miContextId);
      $moDocContext->insert();
    }
    
    echo "soWindow = soPopUpManager.getPopUpById('popup_document_create').getOpener();"
      ."if(soWindow.refresh_grid) soWindow.refresh_grid();"
      ."soPopUpManager.closePopUp('popup_document_create');";
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new SaveDocumentEvent("save_document_event"));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        gebi('document_name').focus();
      </script>
    <?
  }
}

//testa se a licen�a utilizada permite acessar o m�dulo de documenta��o
ISMSLib::testUserPermissionToPoliceMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_document_create.xml");
?>