<?php
include_once "include.php";
include_once $handlers_ref . "grid/policy/QueryGridDocReference.php";

class GridDocReference extends FWDDrawGrid {
  public function drawItem(){
    switch($this->ciColumnIndex){
      case 4:
        $this->coCellBox->setValue(ISMSLib::getISMSShortDate($this->coCellBox->getValue()));
        return $this->coCellBox->draw();
      break;
      default:
        return $this->coCellBox->draw();
      break;
    }
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    
    $moGrid = FWDWebLib::getObject("grid_doc_reference");
    $moHandler = new QueryGridDocReference(FWDWebLib::getConnection());
    $moHandler->setDocumentId(FWDWebLib::getObject('document_id')->getValue());
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new GridDocReference());
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $miDocumentId = FWDWebLib::getObject('document_id')->getValue();
    $moPMDocument = new PMDocument();
    $moPMDocument->fetchById($miDocumentId);
    
    if($moPMDocument->getFieldValue('document_type')!=CONTEXT_BEST_PRACTICE){
      $moPMDocument->testPermissionToRead($miDocumentId);
    }
    
    $moWebLib = FWDWebLib::getInstance();
    $moWebLib->dump_html($moWebLib->getObject('dialog'));
    ?>
    <script language="javascript">
    function refresh_grid() {
      js_refresh_grid('grid_doc_reference');    
    }
    </script>
  <?
  }
}

//testa se a licen�a utilizada permite acessar o m�dulo de documenta��o
ISMSLib::testUserPermissionToPoliceMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_document_read_reference.xml");
?>
