<?php
include_once "include.php";
include_once $handlers_ref . "grid/policy/QueryGridInstanceComments.php";

class GridDocComment extends FWDDrawGrid {
  public function drawItem(){
    switch($this->ciColumnIndex){
      case 2:         
        $moUser = new ISMSUser();
        $moUser->fetchById($this->caData[4]);
        $this->coCellBox->setValue(ISMSLib::getISMSDate($this->coCellBox->getValue()));
        
        $moToolTip = new FWDToolTip();
        $moToolTip->setAttrShowDelay(1000);
        //$msToolTipText = str_replace('\\','\\\\',$moUser->getName());
        //$msToolTipText = str_replace('"','\"',$msToolTipText);
        $moToolTip->setValue($moUser->getName());
        $this->coCellBox->addObjFWDEvent($moToolTip);
        
        return $this->coCellBox->draw();
      break;      
      default:
        return $this->coCellBox->draw();
      break;
    }
  }
}

class InsertCommentEvent extends FWDRunnable {
  public function run() {
    $moWebLib = FWDWebLib::getInstance();
    $miDocumentId = $moWebLib->getObject('document_id')->getValue();
    $miDocInstanceId = $moWebLib->getObject('doc_instance_id')->getValue();
    $miUserId = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    $msComment = $moWebLib->getObject('comment')->getValue();

    $moPMDocument = new PMDocument();
    $moPMDocument->testPermissionToRead($miDocumentId);
    
    $moPMInstanceComment = new PMInstanceComment();
    $moPMInstanceComment->setFieldValue("instance_id",$miDocInstanceId);
    $moPMInstanceComment->setFieldValue("user_id",$miUserId);
    $moPMInstanceComment->setFieldValue("instance_comment",$msComment);
    $moPMInstanceComment->setFieldValue("instance_comment_date",ISMSLib::ISMSTime());
    $moPMInstanceComment->insert(true);

    $moPMDocument->fetchById($miDocumentId);
    $maAlertReceivers = array();
    $maAlertReceivers[] = $moPMDocument->getFieldValue('document_author');
    $maAlertReceivers = array_unique(array_merge($maAlertReceivers,$moPMDocument->getApprovers()));

    foreach($maAlertReceivers as $miReceiver) {
      if ($miReceiver == $miUserId) continue;
      $moAlert = new WKFAlert();
      $moAlert->setFieldValue('alert_receiver_id',$miReceiver);
      $moAlert->setFieldValue('alert_date',ISMSLib::ISMSTime());
      $moAlert->setFieldValue('alert_type',WKF_ALERT_DOCUMENT_COMMENT_CREATED);
      $moAlert->setFieldValue('alert_justification',' ');
      $moAlert->setFieldValue('alert_creator_id',$miUserId);
      $moAlert->setFieldValue('alert_context_id',$miDocumentId);
      $moAlert->insert();
    }
    echo "gebi('comment').value = '';";
    echo "refresh_grid();";
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new InsertCommentEvent("insert_comment_event"));
    
    $moWebLib = FWDWebLib::getInstance();
    $moGrid = FWDWebLib::getObject("grid_instance_comments");
    $moHandler = new QueryGridInstanceComments(FWDWebLib::getConnection());
    $moHandler->setInstanceId(FWDWebLib::getObject('doc_instance_id')->getValue());
    //$moHandler->setUserId($moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getUserId());
    $moDocument = new PMDocument();
    $moDocument->fetchById(FWDWebLib::getObject('document_id')->getValue());
    $miUserResponsible = $moDocument->getFieldValue('document_author');
    if($moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getUserId()==$miUserResponsible){
      $moHandler->setDocResponsible($miUserResponsible);
      $moGrid->setObjFwdDrawGrid(new GridDocComment());
    }else{
      //$moGrid->setObjFwdDrawGrid(new GridDocReference());
    }
    
    $moGrid->setQueryHandler($moHandler);
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $miDocumentId = FWDWebLib::getObject('document_id')->getValue();
    $moPMDocument = new PMDocument();
    $moPMDocument->fetchById($miDocumentId);
    
    if($moPMDocument->getFieldValue('document_type')!=CONTEXT_BEST_PRACTICE){
      $moPMDocument->testPermissionToRead($miDocumentId);
    }
    
    $moWebLib = FWDWebLib::getInstance();
    $miDocumentId = FWDWebLib::getObject('document_id')->getValue();
    $moPMDocument = new PMDocument();
    $moPMDocument->fetchById($miDocumentId);
    $moDocInstance = $moPMDocument->getPublishedInstance();
    $moWebLib->getObject('doc_instance_id')->setValue($moDocInstance->getFieldValue('doc_instance_id'));
    
    $moWebLib->dump_html($moWebLib->getObject('dialog'));
    ?>
    <script language="javascript">
    function refresh_grid() {
      js_refresh_grid('grid_instance_comments');    
    }
    </script>
  <?
  }
}

//testa se a licen�a utilizada permite acessar o m�dulo de documenta��o
ISMSLib::testUserPermissionToPoliceMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_document_read_comments.xml");
?>
