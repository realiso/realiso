<?php
include_once "include.php";


class DownloadEvent extends FWDRunnable {
  public function run() {
    $moWebLib = FWDWebLib::getInstance();
    $miDocumentId = $moWebLib->getObject('document_id')->getValue();
    $miUserId = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    //$miDocInstanceId = $moWebLib->getObject('read_doc_instance_id')->getValue();
    
    $moDocument = new PMDocument();
    $moDocument->testPermissionToRead($miDocumentId);
    
    $moReader = new PMDocumentReader();
    $moReader->createFilter($miDocumentId,'document_id');
    $moReader->createFilter($miUserId,'user_id');
    $moReader->select();
    if ($moReader->fetch()) {
      $moReader->setFieldValue('user_has_read_document',1);
      $moReader->update();
    } else {
      //o usuario nao esta na lista de leitores
    }
   
    $moPMDocument = new PMDocument();
    if($moPMDocument->fetchById($miDocumentId)){
      $miCurrentVersion = $moPMDocument->getFieldValue('document_current_version');
      $moDocInstance = $moPMDocument->getPublishedInstance();
      if($moDocInstance){
        $moPMDocReadHistory = new PMDocReadHistory();
        $moPMDocReadHistory->setFieldValue('user_id',$miUserId);
        $moPMDocReadHistory->setFieldValue('document_id',$moDocInstance->getFieldValue('doc_instance_id'));
        $moPMDocReadHistory->setFieldValue('read_date',ISMSLib::ISMSTime());
        $moPMDocReadHistory->insert();



        $msPath = $moDocInstance->getFieldValue('doc_instance_path');
        $msFileName = $moDocInstance->getFieldValue('doc_instance_file_name');
        header('Cache-Control: ');// leave blank to avoid IE errors
        header('Pragma: ');// leave blank to avoid IE errors
        header('Content-type: application/octet-stream');
        header('Content-Disposition: attachment; filename="'.$msFileName.'"');
        $moCrypt = new FWDCrypt();
        $moFP = fopen($msPath,'rb');
        $moCrypt->setIV(fread($moFP,strlen($moCrypt->getIV())));
        while(!feof($moFP)) {
          echo $moCrypt->decryptNoBase64(fread($moFP,16384));
        }
        fclose($moFP);
      }
    }
  }
}


class VisitLinkEvent extends FWDRunnable {
  public function run() {
    $moWebLib = FWDWebLib::getInstance();
    $moReader = new PMDocumentReader();
    //$miDocInstanceId = $moWebLib->getObject('read_doc_instance_id')->getValue();
    
    $miDocumentId = $moWebLib->getObject('document_id')->getValue();
    $miUserId = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    
    $moDocument = new PMDocument();
    $moDocument->testPermissionToRead($miDocumentId);
    
    $moReader->createFilter($miDocumentId,'document_id');
    $moReader->createFilter($miUserId,'user_id');
    $moReader->select();
    if ($moReader->fetch()) {
      $moReader->setFieldValue('user_has_read_document',1);
      $moReader->update();
    } else {
      //o usuario nao esta na lista de leitores
    }

    $moPMDocument = new PMDocument();
    if($moPMDocument->fetchById($miDocumentId)){
      $miCurrentVersion = $moPMDocument->getFieldValue('document_current_version');
      $moDocInstance = $moPMDocument->getPublishedInstance();
      if($moDocInstance){
        $moPMDocReadHistory = new PMDocReadHistory();
        $moPMDocReadHistory->setFieldValue('user_id',$miUserId);
        $moPMDocReadHistory->setFieldValue('document_id',$moDocInstance->getFieldValue('doc_instance_id'));
        $moPMDocReadHistory->setFieldValue('read_date',ISMSLib::ISMSTime());
        $moPMDocReadHistory->insert();
        
        $moPMDocInstance = new PMDocInstance();
        if ($moPMDocInstance->fetchById($moDocInstance->getFieldValue('doc_instance_id')))
          $msURL = $moPMDocInstance->getFieldValue('doc_instance_link');
        if ($msURL)
          echo "js_open('{$msURL}','document_link',740,502,'center_screen');";
      }
    }
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addSubmitEvent(new DownloadEvent("download"));
    $moStartEvent->addAjaxEvent(new VisitLinkEvent("visit_link"));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    
    $miDocumentId = FWDWebLib::getObject('document_id')->getValue();
    $moPMDocument = new PMDocument();
    $moPMDocument->fetchById($miDocumentId);
    
    if($moPMDocument->getFieldValue('document_type')!=CONTEXT_BEST_PRACTICE){
      $moPMDocument->testPermissionToRead($miDocumentId);
    }
    
    $moWebLib->getObject('name')->setValue($moPMDocument->getFieldValue('document_name'));
    $moWebLib->getObject('description')->setValue($moPMDocument->getFieldValue('document_description'));
    $moWebLib->getObject('keywords')->setValue($moPMDocument->getFieldValue('document_keywords'));
    $moContextClassification = new ISMSContextClassification();
    $moContextClassification->fetchById($moPMDocument->getFieldValue('document_classification'));
    $moWebLib->getObject('classification')->setValue($moContextClassification->getFieldValue('classif_name'));
    $miAuthorId = $moPMDocument->getFieldValue('document_author');
    if($miAuthorId){
      $moUser = new ISMSUser();
      if($moUser->fetchById($miAuthorId))
        $moWebLib->getObject('author')->setValue($moUser->getName());
    }
    $miCurrentVersion = $moPMDocument->getFieldValue('document_current_version');
    
    $msVersion = '0.0';
    $msFileName = '';
    $msFinalFileSize = '';
    $msFileDate = '';
    
    $moDocInstance = $moPMDocument->getPublishedInstance();
    
    if($moDocInstance){
      $msVersion = $moDocInstance->getVersion();
      
      $mbIsLink = $moDocInstance->getFieldValue('doc_instance_is_link');
      if ($mbIsLink) {
        $msFileName = $moDocInstance->getFieldValue('doc_instance_link');
        $moWebLib->getObject('label_link')->setAttrDisplay("true");
        $moWebLib->getObject('label_file_name')->setAttrDisplay("false");
        $moWebLib->getObject('label_file_size')->setAttrDisplay("false");
        $moWebLib->getObject('file_size')->setAttrDisplay("false");
        $moWebLib->getObject('label_file_date')->setAttrDisplay("false");
        $moWebLib->getObject('file_date')->setAttrDisplay("false");
        $moWebLib->getObject('visit_link_button')->setAttrDisplay("true");
        $moWebLib->getObject('download_button')->setAttrDisplay("false");
      } else {
        $msPath = $moDocInstance->getFieldValue('doc_instance_path');
        if ($msPath && is_file($msPath)) {
          $msFileName = $moDocInstance->getFieldValue('doc_instance_file_name');
          $miFileSize = $moDocInstance->getFieldValue('doc_instance_file_size');
          $maSizeName = array("GB", "MB", "KB", "B");
          $msFinalFileSize = number_format($miFileSize,2)."&nbsp;".array_pop($maSizeName);
          while (($miFileSize/1024)>1) {
           $miFileSize=$miFileSize/1024;
           $msFinalFileSize = number_format($miFileSize,2)."&nbsp;".array_pop($maSizeName);
          }
          $msFileDate=date("d/m/Y",filectime($msPath));
        } else {
          $moWebLib->getObject('download_button')->setAttrDisplay("false");
        }
      }
      $moWebLib->getObject('version')->setValue($msVersion);
      $moWebLib->getObject('file_name')->setValue($msFileName);
      $moWebLib->getObject('file_size')->setValue($msFinalFileSize);
      $moWebLib->getObject('file_date')->setValue($msFileDate);
    }else{
      $moWebLib->getObject('download_button')->setAttrDisplay("false");
    }
    
    $moWebLib = FWDWebLib::getInstance();
    $moWebLib->dump_html($moWebLib->getObject('dialog'));
    ?>
    <script language="javascript">
    function refresh_grid() {
      js_refresh_grid('grid_doc_reference');    
    }
    </script>
  <?
  }
}

//testa se a licen�a utilizada permite acessar o m�dulo de documenta��o
ISMSLib::testUserPermissionToPoliceMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_document_read_details.xml");
?>
