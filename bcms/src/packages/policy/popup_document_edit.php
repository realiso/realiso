<?php
include_once "include.php";
include_once $handlers_ref . "select/policy/QuerySelectDocumentType.php";
include_once $handlers_ref . "QueryDocumentRevision.php";
include_once $handlers_ref . "QueryDocInstances.php";

function closePopUp($piDocumentId,$psPopupId){
	echo "soTabManager.unlockTabs();";
	$moHandler = new QueryDocInstances(FWDWebLib::getConnection());
	$moHandler->setDocumentId($piDocumentId);
	$moHandler->addStatus(CONTEXT_STATE_DOC_DEVELOPING);
	$moHandler->addStatus(CONTEXT_STATE_DOC_REVISION);
	$moHandler->makeQuery();
	$moHandler->executeQuery();
	$moHandler->fetch();
	$moDocument = new PMDocument();
	$moDocument->fetchById($piDocumentId);
	$miType = $moDocument->getFieldValue('document_type');

	$miUserId = ISMSLib::getCurrentUserId();
	$mbIsMainApprover = ($moHandler->getFieldValue('document_main_approver')==$miUserId);
	$miCountApprovers = $moHandler->getFieldValue('count_approvers');
	$mbIsLink = $moHandler->getFieldValue('document_is_link');
	$msFilePath = $moHandler->getFieldValue('document_file_path');
	$msLink = $moHandler->getFieldValue('document_link');
	$msProductionDate = $moHandler->getFieldValue('document_date_production');

	$msEventValue = "var soPopUp=soPopUpManager.getPopUpById('popup_confirm');"
	."soPopUp.setCloseEvent(function(){});" // se a resposta for sim, deixa a popup pai aberta por causa do evento ajax
	."isms_open_popup('popup_modification_comment','packages/policy/popup_modification_comment.php?document=$piDocumentId&popupid=$psPopupId','','true');"
	."soWindow = soPopUpManager.getPopUpById('popup_document_edit{$psPopupId}').getOpener();"
	."if(soWindow.change_doc_info) soWindow.change_doc_info({$piDocumentId});"
	."soPopUp=null;";

	// se a resposta for n�o, fecha a popup pai
	$msEchoClose = "soPopUpManager.getPopUpById('popup_confirm').setCloseEvent(function(){"
	."soWindow = soPopUpManager.getPopUpById('popup_document_edit{$psPopupId}').getOpener();"
	."if(soWindow.change_doc_info) soWindow.change_doc_info({$piDocumentId});"
	."soPopUpManager.closePopUp('popup_document_edit{$psPopupId}');"
	."});";

	if( ($mbIsLink && $msLink &&  $miType!=CONTEXT_NONE) || (!$mbIsLink && is_file($msFilePath) && $miType!=CONTEXT_NONE) ){
		if($miCountApprovers==0 && $mbIsMainApprover){
			// Pode publicar
			$msTitle = FWDLanguage::getPHPStringValue('tt_publish','Publicar');
			if (ISMSLib::getTimestamp($msProductionDate) > ISMSLib::ISMSTime()) {
				$msMessage = FWDLanguage::getPHPStringValue('st_publish_document_on_date_confirm',"Deseja publicar o documento no dia %date%?");
				$msMessage = str_replace('%date%',ISMSLib::getISMSShortDate($msProductionDate),$msMessage);
			} else {
				$msMessage = FWDLanguage::getPHPStringValue('st_publish_document_confirm',"Deseja publicar o documento?");
			}
			ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,35);
			echo $msEchoClose;
			return;
		}else{
			// Pode mandar para aprova��o
			$msTitle = FWDLanguage::getPHPStringValue('tt_send_for_approval','Enviar para aprova��o');
			$msMessage = FWDLanguage::getPHPStringValue('st_document_approval_confirm',"Deseja enviar o documento para aprova��o?");
			ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,35);
			echo $msEchoClose;
			return;
		}
	}

	$msName = FWDWebLib::getObject('document_name')->getValue();
	$msNameEditDocRegister = FWDWebLib::convertToISO(FWDWebLib::getObject('document_name')->getValue(),true);

	echo "soWindow = soPopUpManager.getPopUpById('popup_document_edit{$psPopupId}').getOpener();
        if(soWindow.refresh_grid) soWindow.refresh_grid();
        if(soWindow.refresh_tree) soWindow.refresh_tree();
        if(soWindow.set_document) soWindow.set_document({$piDocumentId},'{$msName}');
        if(soWindow.set_document_name) soWindow.set_document_name('{$msNameEditDocRegister}');
        soPopUpManager.closePopUp('popup_document_edit{$psPopupId}');";
}

class RefreshEvent extends FWDRunnable {
	public function run(){
		$miDocumentId = FWDWebLib::getObject('document_id')->getValue();

		$moPMDocument = new PMDocument();
		$moPMDocument->testPermissionToEdit($miDocumentId);

		$moHandler = new QueryDocumentRevision(FWDWebLib::getConnection());
		$moHandler->setDocument($miDocumentId);
		$moHandler->makeQuery();
		$moHandler->executeQuery();
		$moHandler->fetch();

		if($moHandler->getFieldValue('doc_instance_is_link')){
			$msLink = $moHandler->getFieldValue('doc_instance_link');
			$msIsLink = 'true';
		}else{
			$msLink = 'http://';
			$msIsLink = 'false';
		}
		echo "setIsLink($msIsLink,true);"
		."gebi('text_link').value = '$msLink';";
	}
}

class SubmitEvent extends FWDRunnable {
	public function run(){
		$moWebLib = FWDWebLib::getInstance();
		$moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
		$miDocumentId = $moSession->attributeExists("documentContextId")?$moSession->getAttrDocumentContextId():$moWebLib->getObject('document_id')->getValue();
		$miParentDocumentId = $moWebLib->getObject('parent_document_id')->getValue();

		$moFile = FWDWebLib::getObject('doc_instance_file');
		$mbUploadedFile = $moFile->isUploaded();
		$miFileError = 0;
		if($mbUploadedFile){
			$miFileError = $moFile->getErrorCode();
			if($miFileError==FWDFile::E_NONE){
				// Upload sem erros


				/* EXTRA��O DE TEXTO DOS ARQUIVOS DESABILITADA POR CAUSA DO PROBLEMA DE CODIFICA��O
				 $moTextExtractor = new FWDTextExtractor();
				 $msFileExtension = strtolower(substr($moFile->getFileName(),strrpos($moFile->getFileName(),'.')+1));
				 $msFileContent = $moTextExtractor->extractTextFromFile($moFile->getTempFileName(),$msFileExtension);
				 /*/
				$msFileContent = '';
				//*/


				$moCrypt = new FWDCrypt();
				$msDirectory = (ISMSLib::isCompiled() ? $GLOBALS['files_ref'] : 'files/');
				if(is_writable($msDirectory)){
					$msFileName=$moFile->getFileName();
					$msHashFileName = md5($msFileName.uniqid());
					$msPath = "{$msDirectory}{$msHashFileName}.aef";
					$miFileSize = filesize($moFile->getTempFileName());
					$moWriteFP = fopen($msPath,'wb');
					fwrite($moWriteFP,$moCrypt->getIV());
					$moFP = fopen($moFile->getTempFileName(),'rb');
					while(!feof($moFP)){
						fwrite($moWriteFP,$moCrypt->encryptNoBase64(fread($moFP,16384)));
					}
					fclose($moFP);
					fclose($moWriteFP);

					if(!$moSession->attributeExists("documentFilePath")){
						$moSession->addAttribute("documentFilePath");
					}
					$moSession->setAttrDocumentFilePath($msPath);
					if(!$moSession->attributeExists("documentFileName")){
						$moSession->addAttribute("documentFileName");
					}
					$moSession->setAttrDocumentFileName($msFileName);
					if(!$moSession->attributeExists("documentFileSize")){
						$moSession->addAttribute("documentFileSize");
					}
					$moSession->setAttrDocumentFileSize($miFileSize);
					if(!$moSession->attributeExists("documentFileContent")){
						$moSession->addAttribute("documentFileContent");
					}
					$moSession->setAttrDocumentFileContent($msFileContent);
				}else{
					//O diret�rio n�o p�de ser escrito!
					trigger_error("Directory '{$msDirectory}' is not writable.",E_USER_ERROR);
				}
			}else{
				// Erro no upload -> armazena c�digo do erro na sess�o
				if(!$moSession->attributeExists('uploadErrorCode')){
					$moSession->addAttribute('uploadErrorCode');
				}
				$moSession->setAttrUploadErrorCode($miFileError);
			}
		}
		// Sinaliza que, com ou sem arquivo, com ou sem erro, acabou o submit
		if(!$moSession->attributeExists("policySaveComplete")){
			$moSession->addAttribute("policySaveComplete");
		}
		$moSession->setAttrPolicySaveComplete(true);
	}
}

class SaveCompleteEvent extends FWDRunnable {
	public function run(){
		$moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
		if($moSession->attributeExists("policySaveComplete")){
			if($moSession->getAttrPolicySaveComplete()){
				$moSession->deleteAttribute("policySaveComplete");

				if($moSession->attributeExists('uploadErrorCode') && $moSession->getAttrUploadErrorCode()){
					// Trata erro no upload
					$miErrorCode = $moSession->getAttrUploadErrorCode();
					$moSession->deleteAttribute('uploadErrorCode');

					if($miErrorCode & FWDFile::E_MAX_SIZE){
						echo "gobi('warning_max_size').show();";
					}else{
						echo "gobi('warning_upload_error').show();";
					}
					echo "soTabManager.unlockTabs();";
				}else{
					// Upload feito com sucesso
					$moWebLib = FWDWebLib::getInstance();
					$miDocumentId = $moSession->attributeExists("documentContextId")?$moSession->getAttrDocumentContextId():$moWebLib->getObject('document_id')->getValue();
					$moPMDocument = new PMDocument();

					$moPMDocument->fetchById($miDocumentId);
					$msDocumentName = $moPMDocument->getFieldValue('document_name');

					$miCurrentVersion = FWDWebLib::getObject('doc_instance_id')->getValue();

					if($moSession->attributeExists("documentFilePath")){

						$msPath = $moSession->getAttrDocumentFilePath();
						$msFileName = $moSession->attributeExists("documentFileName")?$moSession->getAttrDocumentFileName():"";
						$miFileSize = $moSession->attributeExists("documentFileSize")?$moSession->getAttrDocumentFileSize():0;

						$miParentDocumentId = $moWebLib->getObject('parent_document_id')->getValue();
						$msOldPath="";
						if ($miCurrentVersion) {
							$moPMDocInstance = new PMDocInstance();
							$moPMDocInstance->fetchById($miCurrentVersion);
							$msOldPath = $moPMDocInstance->getFieldValue('doc_instance_path');
						}
						$moPMDocInstance = new PMDocInstance();
						$moPMDocInstance->setFieldValue('doc_instance_is_link',0);
						$moPMDocInstance->setFieldValue('doc_instance_link','');
						$moPMDocInstance->setFieldValue('doc_instance_path',$msPath);
						$moPMDocInstance->setFieldValue('doc_instance_file_size',$miFileSize);
						$moPMDocInstance->setFieldValue('doc_instance_file_name',$msFileName);
						if (ISMSLib::getConfigById(GENERAL_DOCUMENTS_MANUAL_VERSIONING)) {
							$moPMDocInstance->setFieldValue('doc_instance_manual_version',$moWebLib->getObject('doc_instance_manual_version')->getValue());
						}
						if ($msOldPath && is_file($msOldPath)) unlink($msOldPath);
						$moPMDocInstance->setFieldValue('document_id',$miDocumentId);
						if($miCurrentVersion){
							$moPMDocInstance->update($miCurrentVersion);
						}else{
							$miCurrentVersion = $moPMDocInstance->insert(true);
							$moPMDocument = new PMDocument();
							$moPMDocument->setFieldValue('document_current_version',$miCurrentVersion);
							$moPMDocument->update($miDocumentId);
						}

						$msFileContent = $moSession->attributeExists("documentFileContent")?$moSession->getAttrDocumentFileContent():"";
						$msFileContent = $msFileContent;
						$moPMInstanceContent = new PMInstanceContent();
						if ($moPMInstanceContent->fetchById($miCurrentVersion)) {
							$moPMInstanceContent->setFieldValue('instance_content',$msFileContent);
							$moPMInstanceContent->update($miCurrentVersion);
						} else {
							$moPMInstanceContent->setFieldValue('instance_id',$miCurrentVersion);
							$moPMInstanceContent->setFieldValue('instance_content',$msFileContent);
							$moPMInstanceContent->insert();
						}
					}

					if ($moSession->attributeExists("documentFilePath")) $moSession->deleteAttribute("documentFilePath");
					if ($moSession->attributeExists("documentFileName")) $moSession->deleteAttribute("documentFileName");
					if ($moSession->attributeExists("documentFileSize")) $moSession->deleteAttribute("documentFileSize");
					if ($moSession->attributeExists("documentFileContent")) $moSession->deleteAttribute("documentFileContent");
					if ($moSession->attributeExists("documentContextId")) $moSession->deleteAttribute("documentContextId");

					$msPopupId = FWDWebLib::getObject('popupid')->getValue();
					closePopUp($miDocumentId,$msPopupId);
				}

			}else{
				echo "setTimeout('trigger_event(\"save_complete_event\",3)',200);";
			}
		}
	}
}

class SaveEvent extends FWDRunnable {
	public function run() {
		$moWebLib = FWDWebLib::getInstance();
		$mbCanSave = true;

		if(!ISMSLib::getConfigById(GENERAL_MANUAL_DATA_CONTROL)){
			//testa se a data para produ�ao eh menor que a data atual
			$miDateProduction = FWDWebLib::getObject("date_production")->getTimestamp();
			$miTimestamp = ISMSLib::ISMSTime();
			$miCurrentDate = mktime (0, 0, 0, date("m",$miTimestamp), date("d",$miTimestamp),  date("Y",$miTimestamp));
			$mbManualVersioning = ISMSLib::getConfigById(GENERAL_DOCUMENTS_MANUAL_VERSIONING);
			if ($miDateProduction < $miCurrentDate) {
				$mbCanSave = false;
				echo "soTabManager.unlockTabs();js_show('label_date_production_warning');";
			}
		}

		if($mbCanSave) {
			// Previne bugs por lixo deixado na sess�o
			$moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
			if($moSession->attributeExists('policySaveComplete'))  $moSession->deleteAttribute('policySaveComplete');
			if($moSession->attributeExists('uploadErrorCode'))     $moSession->deleteAttribute('uploadErrorCode');
			if($moSession->attributeExists('documentFilePath'))    $moSession->deleteAttribute('documentFilePath');
			if($moSession->attributeExists('documentFileName'))    $moSession->deleteAttribute('documentFileName');
			if($moSession->attributeExists('documentFileSize'))    $moSession->deleteAttribute('documentFileSize');
			if($moSession->attributeExists('documentFileContent')) $moSession->deleteAttribute('documentFileContent');
			if($moSession->attributeExists('documentContextId'))   $moSession->deleteAttribute('documentContextId');

			$miContextId = FWDWebLib::getObject('associated_context_id')->getValue();
			$miParentDocumentId = $moWebLib->getObject('parent_document_id')->getValue();
			$miDocumentId = $moWebLib->getObject('document_id')->getValue();
			$msName = $moWebLib->getObject('document_name')->getValue();
			$msDescription = $moWebLib->getObject('document_description')->getValue();
			$msKeywords = $moWebLib->getObject('document_keywords')->getValue();
			$miClassification = $moWebLib->getObject('select_document_classification')->getValue();
			$miAuthorId = $moWebLib->getObject('author_id')->getValue();
			$miDeadline = $moWebLib->getObject('deadline')->getTimestamp();
			$msPreviousDeadline = $moWebLib->getObject('previous_deadline')->getValue();
			$miDateProduction = $moWebLib->getObject('date_production')->getTimestamp();
			$miDaysBefore = $moWebLib->getObject('days_before')->getValue();

			if($moWebLib->getObject('select_document_type'))
			$miDocumentType = $moWebLib->getObject('select_document_type')->getValue();
			else
			$miDocumentType = 0;

			$msInsert = '';
			$moPMDocument = new PMDocument();
			if($miDocumentId){
				$moPMDocument->testPermissionToEdit($miDocumentId);
				$moDocument = new PMDocument();
				$moDocument->fetchById($miDocumentId);
				$miScheduleId = $moDocument->getFieldValue('document_schedule');
			}else{
				$moPMDocument->testPermissionToInsert();
				$miScheduleId = 0;
			}

			$mbRevisionIsEnabled = (bool)FWDWebLib::getObject('select_revision')->getValue();
			$msSerializedSchedule = FWDWebLib::getObject('var_schedule')->getValue();

			// Schedule de revis�o
			$mbDeleteSchedule = false;
			if($mbRevisionIsEnabled && $msSerializedSchedule){ // revis�o habilitada
				$moSchedule = new WKFSchedule();
				$maSchedule = FWDWebLib::unserializeString($msSerializedSchedule);
				unset($maSchedule['schedule_id']);
				$moSchedule->fillFieldsFromArray($maSchedule);
				$miScheduleId = $moSchedule->insertOrUpdate($miScheduleId);
				$moPMDocument->setFieldValue('document_schedule',$miScheduleId);
			}elseif($miScheduleId){ // revis�o existia, mas foi desabilitada
				$mbDeleteSchedule = true;
				$moPMDocument->setFieldValue('document_schedule','null');
			}

			$moPMDocument->setFieldValue('document_name',$msName);
			$moPMDocument->setFieldValue('document_description',$msDescription);
			$moPMDocument->setFieldValue('document_keywords',$msKeywords);
			$moPMDocument->setFieldValue('document_author',$miAuthorId);
			$moPMDocument->setFieldValue('document_deadline',$miDeadline);
			$moPMDocument->setFieldValue('document_date_production',$miDateProduction);
			$moPMDocument->setFieldValue('days_before',$miDaysBefore);
			if($miDocumentType){
				$moPMDocument->setFieldValue('document_type',$miDocumentType);
			}
			if($miDeadline != strtotime($msPreviousDeadline)) {
				$moPMDocument->setFieldValue('flag_deadline_near',false);
				$moPMDocument->setFieldValue('flag_deadline_late',false);
			}
			if($miClassification)
			$moPMDocument->setFieldValue('document_classification',$miClassification);
			else
			$moPMDocument->setFieldValue('document_classification','null');

			if($miParentDocumentId) {
				$moParentDoc = new PMDocument();
				$moParentDoc->fetchById($miParentDocumentId);
				$moPMDocument->setFieldValue('document_parent',$miParentDocumentId);
				//$moPMDocument->setFieldValue('document_type',$moParentDoc->getFieldValue('document_type'));
			}

			$mbIsRegisterDocument = false;
			$mbHasContext = false;
			if($miContextId){
				$moContextObject = new ISMSContextObject();
				$moContext = $moContextObject->getContextObjectByContextId($miContextId);
				$miContextType = $moContext->getContextType();
				if($miContextType==CONTEXT_REGISTER){
					$mbIsRegisterDocument = true;
					$moContext->testPermissionToEdit($miContextId);
				}else{
					//trigger_error("Invalid register id.",E_USER_ERROR);
					$mbHasContext = true;
				}
				$miMainApproverId = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
				$moPMDocument->setFieldValue('document_author',$miAuthorId);
				$moPMDocument->setFieldValue('document_main_approver',$miMainApproverId);
				$moPMDocument->setFieldValue('document_type',$miContextType);
			}

			$mbInsert = false;
			if ($miDocumentId) {
				$moPMDocument->update($miDocumentId);
			}else {
				$mbInsert = true;
				$moPMDocument->setFieldValue('document_main_approver',$moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getUserId());
				$miDocumentId = $moPMDocument->insert(true);
				$msInsert .=" gebi('document_id').value={$miDocumentId};";
			}

			// Deleta o schedule de revis�o
			if($mbDeleteSchedule){
				$moSchedule = new WKFSchedule();
				$moSchedule->delete($miScheduleId);
			}

			if($mbIsRegisterDocument){ // documento de registro
				$moRegister = new PMRegister();
				$moRegister->setFieldValue('document_id',$miDocumentId);
				$moRegister->update($miContextId);

				$moPMDocRegister = new PMDocRegisters();
				$moPMDocRegister->createFilter($miContextId,'register_id');
				$moPMDocRegister->createFilter($miDocumentId,'document_id');
				$moPMDocRegister->select();
				if(!$moPMDocRegister->fetch()){
					$moPMDocRegister->setFieldValue('register_id',$miContextId);
					$moPMDocRegister->setFieldValue('document_id',$miDocumentId);
					$moPMDocRegister->insert();
				}
			}elseif($miParentDocumentId){ // subdocumento
				$moParentPMDocContext = new PMDocContext();
				$moParentPMDocContext->createFilter($miParentDocumentId,'document_id');
				$moParentPMDocContext->select();
				while($moParentPMDocContext->fetch()) {
					$moPMDocContext = new PMDocContext();
					$moPMDocContext->setFieldValue('document_id',$miDocumentId);
					$moPMDocContext->setFieldValue('context_id',$moParentPMDocContext->getFieldValue('context_id'));
					$moPMDocContext->insert();
				}

				$moParentPMDocumentReader = new PMDocumentReader();
				$moParentPMDocumentReader->createFilter($miParentDocumentId,'document_id');
				$moParentPMDocumentReader->select();
				while($moParentPMDocumentReader->fetch()) {
					$moPMDocumentReader = new PMDocumentReader();
					$moPMDocumentReader->setFieldValue('document_id',$miDocumentId);
					$moPMDocumentReader->setFieldValue('user_id',$moParentPMDocumentReader->getFieldValue('user_id'));
					$moPMDocumentReader->setFieldValue('user_has_read_document',$moParentPMDocumentReader->getFieldValue('user_has_read_document'));
					$moPMDocumentReader->setFieldValue('denied',$moParentPMDocumentReader->getFieldValue('denied'));
					$moPMDocumentReader->setFieldValue('manual',$moParentPMDocumentReader->getFieldValue('manual'));
					$moPMDocumentReader->insert();
				}
			}elseif($mbHasContext && $mbInsert) { //Somente associa a um contexto se for uma insercao de documento.
				$moDocument = new PMDocument();
				$moDocument->setFieldValue('document_type',$miContextType);
				$moDocument->update($miDocumentId);
				$moDocContext = new PMDocContext();
				$moDocContext->setFieldValue('document_id',$miDocumentId);
				$moDocContext->setFieldValue('context_id',$miContextId);
				$moDocContext->insert();
				$moDocContext = new PMDocContext();
				$moDocContext->updateAutoApprovers($miDocumentId);
				$maUsers = array();
				$moDocContext = new PMDocContext();
				foreach($moDocContext->getCurrentContexts($miDocumentId) as $miContextId) {
					$moContextObject  = new ISMSContextObject();
					$moContext = $moContextObject->getContextObjectByContextId($miContextId);
					
					if ($moContext->hasDocument()) {
					  $readerAndResponsible = $moContext->getReadersAndResponsible($miContextId);
					  
					  if($readerAndResponsible)
					    $maUsers = array_merge($moContext->getReadersAndResponsible($miContextId),$maUsers);
					}
				}
				if($maUsers){
  				$maUsers = array_unique($maUsers);
  				$moDocumentReader = new PMDocumentReader();
  				$moDocumentReader->updateUsers($miDocumentId,$maUsers);
				}
			}

			$moPMDocument = new PMDocument();
			$moPMDocument->fetchById($miDocumentId);
			$miCurrentVersion = FWDWebLib::getObject('doc_instance_id')->getValue();
			$msOldPath="";
			if ($miCurrentVersion) {
				$moPMDocInstance = new PMDocInstance();
				$moPMDocInstance->fetchById($miCurrentVersion);
				$msOldPath = $moPMDocInstance->getFieldValue('doc_instance_path');
			}
			if ($moWebLib->getObject('document_type')->getValue()=='link') {
				$msLink = $moWebLib->getObject('text_link')->getValue();
				$moPMDocInstance = new PMDocInstance();
				$moPMDocInstance->setFieldValue('doc_instance_is_link',1);
				$moPMDocInstance->setFieldValue('doc_instance_link',$msLink);
				$moPMDocInstance->setFieldValue('doc_instance_path','');
				$moPMDocInstance->setFieldValue('doc_instance_file_name','');
				if (ISMSLib::getConfigById(GENERAL_DOCUMENTS_MANUAL_VERSIONING)) {
					$moPMDocInstance->setFieldValue('doc_instance_manual_version',$moWebLib->getObject('doc_instance_manual_version')->getValue());
				}
				if ($msOldPath && is_file($msOldPath))
				unlink($msOldPath);
				$moPMDocInstance->setFieldValue('document_id',$miDocumentId);
				if($miCurrentVersion){
					$moPMDocInstance->update($miCurrentVersion);
				}else{
					$miCurrentVersion = $moPMDocInstance->insert(true);
					$moPMDocument = new PMDocument();
					$moPMDocument->setFieldValue('document_current_version',$miCurrentVersion);
					$moPMDocument->update($miDocumentId);
				}
				$msPopupId = FWDWebLib::getObject('popupid')->getValue();
				closePopUp($miDocumentId,$msPopupId);
			} else {
				$moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
				if (!$moSession->attributeExists("policySaveComplete"))
				$moSession->addAttribute("policySaveComplete");
				$moSession->setAttrPolicySaveComplete(false);
				if (!$moSession->attributeExists("documentContextId"))
				$moSession->addAttribute("documentContextId");
				$moSession->setAttrDocumentContextId($miDocumentId);
				echo "$msInsert js_submit('upload','ajax');setTimeout('trigger_event(\"save_complete_event\",3)',200);";
			}
		}
	}
}

class DownloadEvent extends FWDRunnable {
	public function run() {
		$moWebLib = FWDWebLib::getInstance();
		$miDocumentId = $moWebLib->getObject('document_id')->getValue();
		$moPMDocument = new PMDocument();

		$moPMDocument->testPermissionToEdit($miDocumentId);

		if ($moPMDocument->fetchById($miDocumentId)) {
			$miCurrentVersion = FWDWebLib::getObject('doc_instance_id')->getValue();
			$moPMDocInstance = new PMDocInstance();
			if ($moPMDocInstance->fetchById($miCurrentVersion)) {
				$msPath = $moPMDocInstance->getFieldValue('doc_instance_path');
				$msFileName = $moPMDocInstance->getFieldValue('doc_instance_file_name');
				header('Cache-Control: ');// leave blank to avoid IE errors
				header('Pragma: ');// leave blank to avoid IE errors
				header('Content-type: application/octet-stream');
				header('Content-Disposition: attachment; filename="'.$msFileName.'"');
				$moCrypt = new FWDCrypt();
				$moFP = fopen($msPath,'rb');
				$moCrypt->setIV(fread($moFP,strlen($moCrypt->getIV())));
				while(!feof($moFP)) {
					echo $moCrypt->decryptNoBase64(fread($moFP,16384));
				}
				fclose($moFP);
			}
		}
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));
		$moStartEvent->addSubmitEvent(new SubmitEvent("upload"));
		$moStartEvent->addAjaxEvent(new SaveCompleteEvent("save_complete_event"));
		$moStartEvent->addAjaxEvent(new SaveEvent("save_event"));
		$moStartEvent->addAjaxEvent(new RefreshEvent("refresh"));
		$moStartEvent->addSubmitEvent(new DownloadEvent("download"));
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$moWebLib = FWDWebLib::getInstance();

		$msTooltipText = FWDLanguage::getPHPStringValue('to_max_file_size',"Tamanho m�ximo de arquivo: %max_file_size%");
		$msMaxFileSize = ISMSLib::formatFileSize(FWDWebLib::getObject('doc_instance_file')->getAttrMaxFileSize());
		$msTooltipText = str_replace('%max_file_size%',$msMaxFileSize,$msTooltipText);
		FWDWebLib::getObject('tooltip_max_file_size')->setValue($msTooltipText);

		/*
		 * Armazena o id que serve de sufixo para o identificador da popup.
		 * Necess�rio para que essa janela n�o seja sobreescrita por uma
		 * outra 'inst�ncia' dessa mesma janela.
		 */
		$msPopupId = $moWebLib->getObject('param_popupid')->getValue();
		$moWebLib->getObject('popupid')->setValue($msPopupId);

		/*
		 * Armazena o identificador do pai, caso exista.
		 * Esse identificador ir� existir quando essa janela for
		 * chamada a partir da janela de gerenciamento de subdocumentos.
		 */
		$miParentDocumentId = $moWebLib->getObject('param_parent_document_id')->getValue();
		$moWebLib->getObject('parent_document_id')->setValue($miParentDocumentId);

		/*
		 * Armazena o identificador do documento, caso exista.
		 * Esse identificador ir� existir quando essa janela for
		 * chamada a partir de uma grid de visualiza��o de documentos,
		 * por exemplo.
		 */
		$miDocumentId = $moWebLib->getObject('param_document_id')->getValue();
		$moWebLib->getObject('document_id')->setValue($miDocumentId);

		/*
		 * Armazena o identificador do contexto ao qual o documento ser� associado.
		 * Por enquanto, s� � usado quando essa janela for chamada a partir dos
		 * registros.
		 */
		$miContextId = FWDWebLib::getObject('param_associated_context_id')->getValue();
		if ($miContextId) FWDWebLib::getObject('associated_context_id')->setValue($miContextId);

		$maContextLabel = array();
		$maContextLabel[CONTEXT_NONE             ] = FWDLanguage::getPHPStringValue('si_no_type','Sem Tipo');
		$maContextLabel[CONTEXT_AREA             ] = ISMSContextObject::getContextObject(CONTEXT_AREA)->getLabel();
		$maContextLabel[CONTEXT_PROCESS          ] = ISMSContextObject::getContextObject(CONTEXT_PROCESS)->getLabel();
		$maContextLabel[CONTEXT_ASSET            ] = ISMSContextObject::getContextObject(CONTEXT_ASSET)->getLabel();
		$maContextLabel[CONTEXT_CONTROL          ] = ISMSContextObject::getContextObject(CONTEXT_CONTROL)->getLabel();
		$maContextLabel[CONTEXT_SCOPE            ] = ISMSContextObject::getContextObject(CONTEXT_SCOPE)->getLabel();
		$maContextLabel[CONTEXT_POLICY           ] = ISMSContextObject::getContextObject(CONTEXT_POLICY)->getLabel();
		$maContextLabel[DOCUMENT_TYPE_MANAGEMENT ] = FWDLanguage::getPHPStringValue('si_management','Gest�o');
		$maContextLabel[DOCUMENT_TYPE_OTHERS     ] = FWDLanguage::getPHPStringValue('si_others','Outros');

		$moContext = new ISMSContextObject();
		$moSelect = FWDWebLib::getObject('select_document_type');
		$moSelect->setItemValue(CONTEXT_NONE             ,$maContextLabel[CONTEXT_NONE             ]);
		$moSelect->setItemValue(CONTEXT_AREA             ,$maContextLabel[CONTEXT_AREA             ]);
		$moSelect->setItemValue(CONTEXT_PROCESS          ,$maContextLabel[CONTEXT_PROCESS          ]);
		$moSelect->setItemValue(CONTEXT_ASSET            ,$maContextLabel[CONTEXT_ASSET            ]);
		$moSelect->setItemValue(CONTEXT_CONTROL          ,$maContextLabel[CONTEXT_CONTROL          ]);
		$moSelect->setItemValue(CONTEXT_SCOPE            ,$maContextLabel[CONTEXT_SCOPE            ]);
		$moSelect->setItemValue(CONTEXT_POLICY           ,$maContextLabel[CONTEXT_POLICY           ]);
		$moSelect->setItemValue(DOCUMENT_TYPE_MANAGEMENT ,$maContextLabel[DOCUMENT_TYPE_MANAGEMENT ]);
		$moSelect->setItemValue(DOCUMENT_TYPE_OTHERS     ,$maContextLabel[DOCUMENT_TYPE_OTHERS     ]);

		if (FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(INCIDENT_MODE)) {
			$maContextLabel[CONTEXT_CI_ACTION_PLAN] = ISMSContextObject::getContextObject(CONTEXT_CI_ACTION_PLAN)->getLabel();
			$moSelect->setItemValue(CONTEXT_CI_ACTION_PLAN,$maContextLabel[CONTEXT_CI_ACTION_PLAN]);
		}
		$moPMDocument = new PMDocument();
		if($miDocumentId){ // edicao
			$moPMDocument->testPermissionToEdit($miDocumentId);
			//FWDWebLib::getObject('viewbutton_old_versions')->setAttrDisplay('true');
		}elseif($miParentDocumentId){ // insercao de subdocumentos
			$moPMDocument->testPermissionToEdit($miParentDocumentId);
		}
		// insercao/edi��o de documento de registro
		if ($miContextId) {
			$moContextObject = new ISMSContextObject();
			$moContext = $moContextObject->getContextObjectByContextId($miContextId);
			if($moContext->getContextType()==CONTEXT_REGISTER){
				$moContext->testPermissionToEdit($miContextId);
				FWDWebLib::getObject("vb_author")->setShouldDraw(false);
			}
		}

		/*
		 * Popula o select com os tipos de classifica��o de um documento.
		 */
		$moHandler = new QuerySelectDocumentType(FWDWebLib::getConnection());
		$moSelect = FWDWebLib::getObject('select_document_classification');
		$moSelect->setQueryHandler($moHandler);
		$moSelect->populate();

		$mbManualVersioning = ISMSLib::getConfigById(GENERAL_DOCUMENTS_MANUAL_VERSIONING);
		if ($mbManualVersioning) {
			$moWebLib->getObject('doc_instance_manual_version')->setAttrDisplay('true');
			$moWebLib->getObject('doc_instance_manual_version_helper')->setAttrDisplay('true');
			$moWebLib->getObject('doc_instance_version')->setAttrDisplay('false');
		}

		/*
		 * Quando for edi��o de um documento.
		 */
		if ($miDocumentId) {
			FWDWebLib::getObject('title')->setValue(FWDLanguage::getPHPStringValue('tt_document_editing',"Edi��o de Documento"));

			$moPMDocument->fetchById($miDocumentId);
			$moPMDocInstance = new PMDocInstance();
			$moWebLib->getObject('document_name')->setValue($moPMDocument->getFieldValue('document_name'));
			$moWebLib->getObject('document_description')->setValue($moPMDocument->getFieldValue('document_description'));
			$moWebLib->getObject('document_keywords')->setValue($moPMDocument->getFieldValue('document_keywords'));
			$moWebLib->getObject('select_document_classification')->checkItem($moPMDocument->getFieldValue('document_classification'));
			if ($moPMDocument->getFieldValue('document_date_production')) $moWebLib->getObject('date_production')->setValue(ISMSLib::getISMSShortDate($moPMDocument->getFieldValue('document_date_production')));
			if ($moPMDocument->getFieldValue('document_deadline')) $moWebLib->getObject('deadline')->setValue(ISMSLib::getISMSShortDate($moPMDocument->getFieldValue('document_deadline')));
			$moWebLib->getObject('previous_deadline')->setValue($moPMDocument->getFieldValue('document_deadline'));
			$moWebLib->getObject('days_before')->setValue($moPMDocument->getFieldValue('days_before'));

			/*
			 * Armazena os ids dos contextos aos quais o documento est� associado.
			 */
			$moPMDocContext = new PMDocContext();
			$moPMDocContext->createFilter($miDocumentId,'document_id');
			$moPMDocContext->select();
			$maContexts = array();
			while($moPMDocContext->fetch()) {
				$maContexts[] = $moPMDocContext->getFieldValue('context_id');
			}
			if(!count($maContexts)){
				FWDWebLib::getObject('select_document_type')->setValue($moPMDocument->getFieldValue('document_type'));
				//desabilita o help do tipo de documentos
				FWDWebLib::getObject('st_document_type')->setShouldDraw(false);
				FWDWebLib::getObject('st_document_type_help')->setShouldDraw(false);
			}else{
				//desabilita o help do tipo de documentos
				FWDWebLib::getObject('select_document_type')->setShouldDraw(false);

				FWDWebLib::getObject('st_document_type')->setValue($maContextLabel[$moPMDocument->getFieldValue('document_type')]);

			}
			$msContexts = serialize($maContexts);
			FWDWebLib::getObject('document_context_id')->setValue($msContexts);

			$miAuthorId = $moPMDocument->getFieldValue('document_author');
			if($miAuthorId){
				$moUser = new ISMSUser();
				if($moUser->fetchById($miAuthorId)) {
					$moWebLib->getObject('document_author')->setValue($moUser->getName());
					$moWebLib->getObject('author_id')->setValue($miAuthorId);
				}
			}
			//se miUserId != miMainApprover, desabilita busca de autor.
			$miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
			$miMainApproverId = $moPMDocument->getFieldValue('document_main_approver');
			if ($miUserId != $miMainApproverId) {
				$moWebLib->getObject('vb_author')->setShouldDraw(false);
			}
			$miParentId = $moPMDocument->getFieldValue('document_parent');

			// P�e o schedule de revis�o serializado numa variable
			$miScheduleId = $moPMDocument->getFieldValue('document_schedule');
			if($miScheduleId){
				$moRevisionSelect = $moWebLib->getObject('select_revision');
				$moRevisionSelect->setValue(1);
				FWDWebLib::getObject('btn_revision')->setAttrDisplay('true');
				$moSchedule = new WKFSchedule();
				$moSchedule->fetchById($miScheduleId);
				$msSerializedSchedule = serialize($moSchedule->getFieldValues());
				FWDWebLib::getObject('var_schedule')->setValue($msSerializedSchedule);
			}

			$moDocInstance = $moPMDocument->getDeveloppingInstance();

			if($moDocInstance){
				$msVersion = $moDocInstance->getVersion();
				if (!$mbManualVersioning && !$msVersion) $msVersion="0.0";
				$moWebLib->getObject($mbManualVersioning?'doc_instance_manual_version':'doc_instance_version')->setValue($msVersion);
				$moWebLib->getObject('doc_instance_id')->setValue($moDocInstance->getFieldValue('doc_instance_id'));

				$mbIsLink = $moDocInstance->getFieldValue('doc_instance_is_link');
				if ($mbIsLink) {
					$msLink = $moDocInstance->getFieldValue('doc_instance_link');
					$moWebLib->getObject('text_link')->setValue($msLink);
					//$moFileAsLinkController = $moWebLib->getObject('IsLinkController');
					//$moFileAsLinkController->checkItem(1);
					$moDocumentTypeSelect = $moWebLib->getObject('document_type');
					$moDocumentTypeSelect->setValue('link');
					$moWebLib->getObject('text_link')->setAttrDisplay("true");
					$moWebLib->getObject('label_link')->setAttrDisplay("true");
					$moWebLib->getObject('label_file')->setAttrDisplay("false");
					$moWebLib->getObject('doc_instance_file')->setAttrDisplay("false");
					$moWebLib->getObject('max_file_size_helper')->setAttrDisplay("false");
				} else {
					$msPath = $moDocInstance->getFieldValue('doc_instance_path');
					if ($msPath && is_file($msPath)) {
						$msFileName = $moDocInstance->getFieldValue('doc_instance_file_name');
						$miFileSize = $moDocInstance->getFieldValue('doc_instance_file_size');
						$maSizeName = array("GB", "MB", "KB", "B");
						$msFinalFileSize = number_format($miFileSize,2)."&nbsp;".array_pop($maSizeName);
						while (($miFileSize/1024)>1) {
							$miFileSize=$miFileSize/1024;
							$msFinalFileSize = number_format($miFileSize,2)."&nbsp;".array_pop($maSizeName);
						}

						$msFileDate=date("d/m/Y",filectime($msPath));
						$moIcon = $moWebLib->getObject('icon_download');
						$moIcon->setAttrDisplay("true");
						$moToolTipDownload = $moWebLib->getObject('tooltip_download');
						$moToolTipDownload->setAttrWidth(300);
						$msMsgDownload = FWDLanguage::getPHPStringValue('st_download_file','Fazer download do arquivo:');
						$msMsgSize = FWDLanguage::getPHPStringValue('st_size','Tamanho:');
						$msMsgCreationDate = FWDLanguage::getPHPStringValue('st_creation_date','Data de Cria��o:');
						$moToolTipDownload->setValue("{$msMsgDownload} {$msFileName}<BR/>{$msMsgSize} {$msFinalFileSize}<BR/>{$msMsgCreationDate} {$msFileDate}",true);
					}
				}
			} else {
				if (!$mbManualVersioning) $moWebLib->getObject('doc_instance_version')->setValue("0.0");
			}
		}else{
			/*
			 * Desabilita o bot�o de 'refer�ncias' e 'subdocumentos' quando for
			 * uma inser��o. Tamb�m mostra o usu�rio corrente como o autor padr�o.
			 */
			FWDWebLib::getObject('title')->setValue(FWDLanguage::getPHPStringValue('tt_document_adding',"Adi��o de Documento"));

			//desabilita o help do tipo de documentos
			FWDWebLib::getObject('st_document_type')->setShouldDraw(false);
			FWDWebLib::getObject('st_document_type_help')->setShouldDraw(false);

			$miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
			$moUser = new ISMSUser();
			$moUser->fetchById($miUserId);
			FWDWebLib::getObject('author_id')->setValue($miUserId);
			FWDWebLib::getObject('document_author')->setValue($moUser->getFieldValue('user_name'));
		}
		//setTimeout("soWindow.change_doc_info(gebi('document_id').value);",500);
		if(FWDWebLib::getObject('popup_register_edit')->getValue()){
			FWDWebLib::getObject('revision_viewgroup')->setShouldDraw(false);
			FWDWebLib::getObject('select_document_type')->setShouldDraw(false);
			FWDWebLib::getObject('st_document_type')->setShouldDraw(true);
			FWDWebLib::getObject('st_document_type')->setValue(FWDLanguage::getPHPStringValue('si_register','Registro'));
		}

		$moWebLib->dump_html($moWebLib->getObject('dialog'));
		?>
<script language="javascript">
        gebi('document_name').focus();
        
        soPopUpManager.getPopUpById('popup_document_edit'+gebi('popupid').value).setCloseEvent(
          function(){
            msPopup = 'popup_document_edit'+gebi('popupid').value;
            soWindow = soPopUpManager.getPopUpById(msPopup).getOpener();
            if(soWindow.refresh_grid){
              soWindow.refresh_grid();
            }
            if(soWindow.refresh_tree){
              soWindow.refresh_tree();
            } else if(soWindow.closePopUps){
              soWindow.closePopUps();
            }
            if(soWindow.change_doc_info){ 
                var docId= gebi('document_id').value;
                soWindow.change_doc_info(docId);
            }
            msPopup = soWindow = null;
          }
        );
        function set_user(id, name){
          gebi('author_id').value = id;
          gobi('document_author').setValue(name);
          id = name = null;
        }
        function refresh(){
          trigger_event('refresh',3);
        }
        function setIsLink(pbIsLink,pbCheck){
          if(pbCheck) gfx_check('IsLinkController','1');
          if(pbIsLink){
            js_hide('doc_instance_file');
            js_hide('max_file_size_helper');
            js_hide('label_file');
            js_show('label_link');
            js_show('text_link');
          }else{
            js_hide('text_link');
            js_hide('label_link');
            js_show('label_file');
            js_show('doc_instance_file');
            js_show('max_file_size_helper');
          }
        }
        
        function set_schedule(psSerializedSchedule){
          gebi('var_schedule').value = psSerializedSchedule;
        }
        
      </script>
		<?
	}
}
//testa se a licen�a utilizada permite acessar o m�dulo de documenta��o
ISMSLib::testUserPermissionToPoliceMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_document_edit.xml");
?>