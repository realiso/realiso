<?php

include_once "include.php";

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    // Testa se o usu�rio tem permiss�o para inserir um documento
    $moDocument = new PMDocument();
    $moDocument->testPermissionToInsert();
    
    $moWebLib = FWDWebLib::getInstance();
    $msPopupId = uniqid();
    $moWebLib->getObject('popupid')->setValue($msPopupId);
    
    $miRegisterId = $moWebLib->getObject('param_register_id')->getValue();
    $moWebLib->getObject('register_id')->setValue($miRegisterId);
    $moPMRegister = new PMRegister();
    
    if ($moPMRegister->fetchById($miRegisterId)) {
      $msRegisterName = $moPMRegister->getName();
      FWDWebLib::getObject('document_creation')->setValue(str_replace('%register_name%', $msRegisterName, FWDWebLib::getObject('document_creation')->getValue()));
      FWDWebLib::getObject('document_relation')->setValue(str_replace('%register_name%', $msRegisterName, FWDWebLib::getObject('document_relation')->getValue()));
      FWDWebLib::getObject('document_do_nothing')->setValue(str_replace('%register_name%', $msRegisterName, FWDWebLib::getObject('document_do_nothing')->getValue()));
    }
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        
        function closePopUps(){
          soPopUpManager.closePopUp('popup_create_document_confirm');
        }
        soPopUpManager.getPopUpById('popup_create_document_confirm').setCloseEvent(
          function(){
            soWindow = soPopUpManager.getPopUpById('popup_document_register_edit').getOpener();
            if(soWindow.refresh_grid)
              soWindow.refresh_grid();
            soWindow = null;
            soPopUpManager.closePopUp('popup_document_register_edit');
          }
        );
      </script>
    <?
  }
}
//testa se a licen�a utilizada permite acessar o m�dulo de documenta��o
ISMSLib::testUserPermissionToPoliceMode();
    
FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_create_document_confirm.xml');

?>