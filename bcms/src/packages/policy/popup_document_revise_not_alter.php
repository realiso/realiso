<?php
include_once "include.php";
include_once $handlers_ref . "grid/QueryGridUserTasks.php";

class SaveEvent extends FWDRunnable {
	public function run() {
		$moWebLib = FWDWebLib::getInstance();
    $miDocumentId = $moWebLib->getObject("var_document_id")->getValue();
    $miTimestamp = ISMSLib::ISMSTime();
    $msJustification = $moWebLib->getObject("justification")->getValue();

    $miTaskId = FWDWebLib::getObject('var_task_id')->getValue();

    $moTask = new WKFTask();
    $moTask->testPermissionToExecute($miTaskId);

    $moPMDocumentRevisionHistory = new PMDocumentRevisionHistory();
    $moPMDocumentRevisionHistory->setFieldValue("document_id",$miDocumentId);
    $moPMDocumentRevisionHistory->setFieldValue("date_revision",$miTimestamp);
    $moPMDocumentRevisionHistory->setFieldValue("justification",$msJustification);
    $moPMDocumentRevisionHistory->setFieldValue("new_version",false);
    $moPMDocumentRevisionHistory->insert();

    $moTask->createFilter($miDocumentId,"task_context_id");
    $moTask->createFilter(ACT_DOCUMENT_REVISION,"task_activity");
    $moTask->createFilter(true,"task_is_visible");
    $moTask->select();
    while($moTask->fetch()){
      $moTask2 = new WKFTask();
      if($moTask->getFieldValue('task_id')==$miTaskId){
        $moTask2->setFieldValue('task_date_accomplished',$miTimestamp);
      }
      $moTask2->setFieldValue('task_is_visible',false);
      $moTask2->update($moTask->getFieldValue("task_id"));
    }

    $msNextTaskJS = "";
		$mbOpenNextTask = (bool)trim(FWDWebLib::getObject('auto_task_open')->getValue(),':');
		if ($mbOpenNextTask) {
		  $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
		  $moHandlerTask = new QueryGridUserTasks(FWDWebLib::getConnection());
      $moHandlerTask->setUser($miUserId);
      $moHandlerTask->setOrderBy('task_date_created','-');
      $moHandlerTask->makeQuery();
      $moHandlerTask->executeQuery();
      if ($moHandlerTask->fetch()) {
  		  $msNextTaskJS = 'soWindow.setTimeout("'.ISMSLib::getTaskPopup($moHandlerTask->getFieldValue('task_id'),
  		                                        $moHandlerTask->getFieldValue('task_activity'),
  		                                        $moHandlerTask->getFieldValue('task_context_id'),
  		                                        $moHandlerTask->getFieldValue('task_context_type'),
  		                                        2).',1");';
      }
		}
		echo "soWindow = soPopUpManager.getPopUpById('popup_document_revision_task').getOpener();"
        ."if (soWindow.refresh_grids)"
        ."  soWindow.refresh_grids();"
        .$msNextTaskJS
        ."soPopUpManager.closePopUp('popup_document_revision_task');"
        ."soPopUpManager.closePopUp('popup_document_revise_not_alter');";
	}
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new SaveEvent("save"));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $miDocumentId = $moWebLib->getObject("document_id")->getValue();
    $moWebLib->getObject("var_document_id")->setValue($miDocumentId);
    $miTaskId = $moWebLib->getObject("task_id")->getValue();
    $moWebLib->getObject("var_task_id")->setValue($miTaskId);
    $moWebLib->dump_html($moWebLib->getObject('dialog'));
    ?>
      <script language="javascript">
        gebi('justification').focus();
      </script>
    <?
  }
}

//testa se a licen�a utilizada permite acessar o m�dulo de documenta��o
ISMSLib::testUserPermissionToPoliceMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_document_revise_not_alter.xml");

?>