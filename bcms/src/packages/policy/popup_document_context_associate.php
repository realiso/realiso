<?php
include_once "include.php";
include_once $handlers_ref."grid/policy/QueryGridContextNames.php";

function getSearchContexts(){
  $maReturn = array();
  $maReturn[CONTEXT_AREA]=    ISMSContextObject::getContextObject(CONTEXT_AREA)->getLabel();
  $maReturn[CONTEXT_PROCESS]= ISMSContextObject::getContextObject(CONTEXT_PROCESS)->getLabel();
  $maReturn[CONTEXT_ASSET]=   ISMSContextObject::getContextObject(CONTEXT_ASSET)->getLabel();
  $maReturn[CONTEXT_CONTROL]= ISMSContextObject::getContextObject(CONTEXT_CONTROL)->getLabel();
  if (FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(INCIDENT_MODE))
    $maReturn[CONTEXT_CI_ACTION_PLAN]=  ISMSContextObject::getContextObject(CONTEXT_CI_ACTION_PLAN)->getLabel();
  return $maReturn;
}

class GridContextSearch extends FWDDrawGrid {
  public function DrawItem(){
    switch($this->ciColumnIndex){
      case 1:
        switch($this->caData[3]) {
          case CONTEXT_AREA:
            $this->coCellBox->setIconSrc("icon-area_gray.gif");
          break;
          case CONTEXT_PROCESS:
            $this->coCellBox->setIconSrc("icon-process_gray.gif");
          break;
          case CONTEXT_ASSET:
            $this->coCellBox->setIconSrc("icon-asset_gray.gif");
          break;
          case CONTEXT_CONTROL:
            $this->coCellBox->setIconSrc("icon-control.gif");
          break;
          default:
            $this->coCellBox->setIconSrc("icon-wrong.gif");
            $this->coCellBox->setValue('');
          break;
        }
        return parent::drawItem();
      default:
        return parent::drawItem();
    }
  }
}

class SearchContextEvent extends FWDRunnable {
  public function run(){
    $moGrid = FWDWebLib::getObject('grid_context_search');
    $moGrid->execEventPopulate();
  }
}

class PopulateGridSelected extends FWDRunnable {
  public function run(){
    $moGrid = FWDWebLib::getObject('grid_current_contexts');
    $moGrid->execEventPopulate();
  }
}

class AssociateContextEvent extends FWDRunnable {
  public function run() {
    $miDocumentId = FWDWebLib::getObject('document_id')->getValue();
    $moDocument = new PMDocument();
    $moDocument->fetchById($miDocumentId);
    $miDocumentType = $moDocument->getFieldValue('document_type');
    //teste de permiss�o  
    $moDocument = new PMDocument();
    $moDocument->testPermissionToManage($miDocumentId);
    $miCurrentType = FWDWebLib::getObject('type_filter')->getValue();
    //contextos 'atuais' relacionados com o documento
    if(FWDWebLib::getObject('current_context_ids')->getValue())
      $maCurrentIds = array_unique(array_filter(explode(':',FWDWebLib::getObject('current_context_ids')->getValue())));
    else
      $maCurrentIds = array();

    $moDocContext = new PMDocContext();
    $maOldIds = $moDocContext->getCurrentContexts($miDocumentId);
    if($miCurrentType == $miDocumentType){
      //tipo da rela��o do documento = tipo atual dos elementos selecionados
      $maToInsert = array_diff($maCurrentIds,$maOldIds);
      $maToDelete = array_diff($maOldIds,$maCurrentIds);
    }else{
      //tipos diferentes, deletar as rela��es anteriores e inserir as novas
      $maToDelete = $maOldIds;
      $maToInsert = $maCurrentIds;
      if($miCurrentType){
        $moDocument = new PMDocument();
        $moDocument->setFieldValue('document_type',$miCurrentType);
        $moDocument->update($miDocumentId);
      }
    }
    //delete as rela��es anteriores que foram excluidas pelo usu�rio
    foreach($maToDelete as $miContextId) {
      $moDocContext = new PMDocContext();
      $moDocContext->createFilter($miDocumentId,'document_id');
      $moDocContext->createFilter($miContextId,'context_id');
      $moDocContext->delete();
    }
    //insere as novas rela��es
    foreach($maToInsert as $miContextId) {
      $moDocContext = new PMDocContext();
      $moDocContext->setFieldValue('document_id',$miDocumentId);
      $moDocContext->setFieldValue('context_id',$miContextId);
      $moDocContext->insert();
    }
    //Atualiza aprovadores autom�ticos
    $moDocContext = new PMDocContext();
    $moDocContext->updateAutoApprovers($miDocumentId);

    $maUsers = array();
    $moDocContext = new PMDocContext();
    foreach($moDocContext->getCurrentContexts($miDocumentId) as $miContextId) {
      $moContextObject  = new ISMSContextObject();
      $moContext = $moContextObject->getContextObjectByContextId($miContextId);
      if ($moContext->hasDocument()) $maUsers = array_merge($moContext->getReadersAndResponsible($miContextId),$maUsers);
    }
    $maUsers = array_unique($maUsers);
    $moDocumentReader = new PMDocumentReader();
    $moDocumentReader->updateUsers($miDocumentId,$maUsers);
    
    echo "soPopUpManager.getRootWindow().soTabSubManager.getSelectedWindow().soTabSubManager.getSelectedWindow().refresh_grid();"
        ."soPopUpManager.closePopUp('popup_document_context_associate');";
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    /*eventos*/
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
    $moStartEvent->addAjaxEvent(new SearchContextEvent('search_event'));
    $moStartEvent->addAjaxEvent(new PopulateGridSelected('refresh_selected'));
    $moStartEvent->addAjaxEvent(new AssociateContextEvent('associate_context_event'));
    /*variaveis*/
    $miDocumentId = FWDWebLib::getObject('document_id')->getValue();
    $miContextType = FWDWebLib::getObject('type_filter')->getValue();
    $msSearchString = FWDWebLib::getObject('name_filter')->getValue();
    $maContexts = array_unique(array_filter(explode(':',FWDWebLib::getObject('current_context_ids')->getValue())));
    $maSearchContexts = getSearchContexts();
    $maSelect = array();
    /*objetos*/
    $moSearchGrid = FWDWebLib::getObject('grid_context_search');
    $moCurrentGrid = FWDWebLib::getObject('grid_current_contexts');
    $moSearchHandler = new QueryGridContextNames(FWDWebLib::getConnection());
    $moCurrentHandler = new QueryGridContextNames(FWDWebLib::getConnection());
    $moSearchGrid->setObjFwdDrawGrid(new GridContextSearch());
    $moCurrentGrid->setObjFwdDrawGrid(new GridContextSearch());
    
    if(!$miContextType){
      //o select n�o tem tipo, pegar o tipo do documento
       $moDocument = new PMDocument();
       $moDocument->fetchById($miDocumentId);
       $miContextType = $moDocument->getFieldValue('document_type');
    }
    if(count($maContexts)){
      if(in_array($miContextType,array_keys($maSearchContexts))){
        //tipo do select = tipo do documento ou = ao tipo do select do evento anterior
        $moSearchHandler->setExcludedContexts($maContexts);
        $moCurrentHandler->setContextIds($maContexts);
        $maSelect[] = array($miContextType,$maSearchContexts[$miContextType]);
      }else{
        //tipo do documento n�o consta no filtro de tipos do select, mostrar todos os filtros do select
         foreach ($maSearchContexts as $miKey=>$msValue){
          $maSelect[] = array($miKey,$msValue);
         }
      }
    }else{
      if(!$miContextType)
        $miContextType = CONTEXT_AREA;
      //nao tem contextos selecionados, mostrar todos os possiveis filtros no select
      foreach ($maSearchContexts as $miKey=>$msValue){
        $maSelect[] = array($miKey,$msValue);
      }
    }
    //popula o select do filtro de tipos
    if(trim($moStartEvent->getAjaxEventName())!=''){
      $msSelect = serialize($maSelect);
      echo " gobi('sel_context_type').populate('$msSelect');";
      if($miContextType){
        echo " gobi('sel_context_type').setValue('$miContextType');";
      }
    }
    //seta os filtros da query da grid de search
    $moSearchHandler->setContextType(($miContextType?$miContextType:0));
    $moSearchHandler->setSearchString($msSearchString);
    $moCurrentHandler->setCurrentOnly(true);
    $moSearchGrid->setQueryHandler($moSearchHandler);
    $moCurrentGrid->setQueryHandler($moCurrentHandler);
  }
}
class ScreenEvent extends FWDRunnable {
  public function run(){
    $miDocumentId = FWDWebLib::getObject('param_document_id')->getValue();
    
    $moDocument = new PMDocument();
    $moDocument->testPermissionToManage($miDocumentId);
    
    $maSearchContext = getSearchContexts();
    
    FWDWebLib::getObject('document_id')->setValue($miDocumentId);
    $moDocument = new PMDocument();
    $moDocument->fetchById($miDocumentId);
    $miDocumentType = $moDocument->getFieldValue('document_type');
    
    $moSelect = FWDWebLib::getObject('sel_context_type');
    
    if ($miDocumentType == DOCUMENT_TYPE_MANAGEMENT || $miDocumentType == DOCUMENT_TYPE_OTHERS) { 
      //Verifica��o do tipo. N�o pode ser 'gest�o' ou 'outros'
      trigger_error(FWDLanguage::getPHPStringValue('st_permission_to_associate_item_document_denied','Voc� n�o tem permiss�o para associar um elemento a um documento do tipo "Gest�o" ou "Outros"'), E_USER_ERROR);
    }elseif($miDocumentType == CONTEXT_NONE){ 
      //Se o documento nao tiver tipo, ent�o popula select com todos os tipos.
      foreach ($maSearchContext as $miContextType=>$msContextName) {
        $moSelect->setItemValue($miContextType,$msContextName);
      }
      $moSelect->checkItem(CONTEXT_AREA);
    } else { 
      //Se tiver tipo, popula com aquele tipo apenas.
      $moSelect->setItemValue($miDocumentType,$maSearchContext[$miDocumentType]);
    }
    //elementos atuais relacionados ao documento
    $moDocumentContext = new PMDocContext();
    $maContexts = $moDocumentContext->getCurrentContexts($miDocumentId);
    FWDWebLib::getObject('current_context_ids')->setValue(implode(':',$maContexts));
    //popula a grid dos documentos relacionados
    $moCurrentGrid = FWDWebLib::getObject('grid_current_contexts');
    $moCurrentHandler = new QueryGridContextNames(FWDWebLib::getConnection());
    $moCurrentGrid->setObjFwdDrawGrid(new GridContextSearch());
    $moCurrentHandler->setCurrentOnly(true);
    $moCurrentHandler->setContextIds($maContexts);
    $moCurrentGrid->setQueryHandler($moCurrentHandler);
    
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
    <script language="javascript">
      gebi('context_name').focus();
      
      function save_suggested_processes(psValues){
        gebi('var_ids_suggested').value=psValues;
        trigger_event('save_suggested_event',3);
      }
      function refresh_grids(){
         gobi('grid_context_search').setPopulate(true);
         gobi('grid_current_contexts').setPopulate(true);
         trigger_event('search_event',3);
         trigger_event('refresh_selected',3);
      }
      function search(){
        gebi('name_filter').value = gebi('context_name').value;
        gebi('type_filter').value = gobi('sel_context_type').getValue();
        gobi('grid_context_search').setPopulate(true);
        trigger_event('search_event',3);
      }
      function select_grid_lines(){
         var moIds = gebi('current_context_ids');
         if(moIds.value)
             moIds.value = moIds.value + ':' + gobi('grid_context_search').getValue().join(':');
         else
             moIds.value = gobi('grid_context_search').getValue().join(':');
         refresh_grids();
      }
      function remove_selected(){
         var maNewCtx = new Array();
         var moNewCtx = gebi('current_context_ids');
         if(moNewCtx.value){
            var maCtx = moNewCtx.value.split(':');
            var maGridSelected = gobi('grid_current_contexts').getValue();
            for(i=0;i < maCtx.length; i++){
               if(!in_array(maCtx[i], maGridSelected)){
                  maNewCtx.push(maCtx[i]);
               }
            }
            moNewCtx.value = maNewCtx.join(':');
            refresh_grids();
          }
      }
      function enter_search_event(e)
      {
        if (!e) e = window.event;
        if(e['keyCode']==13){
          search();
        }
      }
      FWDEventManager.addEvent(gebi('context_name'), 'keydown', enter_search_event);
    </script>
    <?
  }
}

//testa se a licen�a utilizada permite acessar o m�dulo de documenta��o
ISMSLib::testUserPermissionToPoliceMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_document_context_associate.xml');

?>