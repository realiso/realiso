<?php
include_once "include.php";
include_once $handlers_ref . "grid/policy/QueryGridDocuments.php";
include_once $handlers_ref . "select/policy/QuerySelectDocumentType.php";
//include_once $classes_isms_ref . "nonauto/policy_management/DocumentsDrawGrid.php";

class AllDocumentsGrid extends FWDDrawGrid {
  protected $csGridName;
  
  public function __construct($psGridName){
    $this->csGridName = $psGridName;
  }
  
  public function drawItem(){
    switch($this->ciColumnIndex){
      case $this->getIndexByAlias('document_id'):{
        $msIcon = PMDocument::getDocumentIcon($this->getFieldValue('document_file_name'),$this->getFieldValue('document_is_link'));
        $this->coCellBox->setIconSrc($msIcon);
        return $this->coCellBox->draw();
      }
      case $this->getIndexByAlias('document_name'):{
        $miDocumentId = $this->getFieldValue('document_id');
        $msPath = '';
        $mbIsLink = false;
        $miDocInstance = $this->getFieldValue('document_instance_id');
        if($this->getFieldValue('document_is_link')){
          $mbIsLink = true;
          $msPath = $this->getFieldValue('document_link');
        }else{
          $msPath = $this->getFieldValue('document_file_path');
        }
        
        if($miDocInstance && trim($msPath)!=''){
          if($mbIsLink){
            $this->coCellBox->setValue("<a href=javascript:read('".$miDocumentId."','".$miDocInstance."','link');>".$this->coCellBox->getValue()."</a>");
          }else{
            if($msPath && is_file($msPath)){
              $this->coCellBox->setValue("<a href=javascript:read('".$miDocumentId."','".$miDocInstance."','file');>".$this->coCellBox->getValue()."</a>");
            }
          }
        }
        
        return $this->coCellBox->draw();
      }
      case $this->getIndexByAlias('document_file_name'):{
        if($this->getFieldValue('document_is_link')){
          $this->coCellBox->setValue($this->getFieldValue('document_link'));
        }
        return parent::drawItem();
      }
      case $this->getIndexByAlias('document_classification'):{
        if($this->coCellBox->getValue()){
          $moContextClassification = new ISMSContextClassification();
          $moContextClassification->fetchById($this->coCellBox->getValue());
          $msContextClassification = $moContextClassification->getFieldValue('classif_name');
          $this->coCellBox->setValue($msContextClassification);
        }
        return $this->coCellBox->draw();
      }
      case $this->getIndexByAlias('document_version'):{
        if (!ISMSLib::getConfigById(GENERAL_DOCUMENTS_MANUAL_VERSIONING) || !$this->coCellBox->getValue()) {
          $miMajorVersion = $this->getFieldValue('document_major_version');
          $miRevisionVersion = $this->getFieldValue('document_revision_version');
          if(!$miMajorVersion) $miMajorVersion = 0;
          if(!$miRevisionVersion) $miRevisionVersion = 0;
          $this->coCellBox->setValue("{$miMajorVersion}.{$miRevisionVersion}");
        }
        return  parent::drawItem();
      }
      case $this->getIndexByAlias('doc_instance_status'):{
        $moContext = new ISMSContextObject();

        $msStatus = $moContext->getContextStateAsString($this->coCellBox->getValue());
        $this->coCellBox->setValue($msStatus);

        return parent::drawItem();
      }
      case $this->getIndexByAlias('context_name'):{
        $msIconName = '';
        switch($this->getFieldValue('document_type')){
          case CONTEXT_AREA:          $msIconName="icon-area_gray.gif";     break;
          case CONTEXT_PROCESS:       $msIconName="icon-process_gray.gif";  break;
          case CONTEXT_ASSET:         $msIconName="icon-asset_gray.gif";    break;
          case CONTEXT_CONTROL:       $msIconName="icon-control.gif";       break;
          case CONTEXT_BEST_PRACTICE: $msIconName="icon-best_practice.gif"; break;
          case CONTEXT_POLICY:        $msIconName="icon-policy.gif";
              $this->coCellBox->setValue(str_replace("%context_policy%",ISMSContextObject::getContextObject(CONTEXT_POLICY)->getLabel(),$this->coCellBox->getValue()));
          break;
          case CONTEXT_SCOPE:         $msIconName="icon-scope.gif";
              $this->coCellBox->setValue(str_replace("%context_scope%",ISMSContextObject::getContextObject(CONTEXT_SCOPE)->getLabel(),$this->coCellBox->getValue()));
          break;
          case CONTEXT_CI_ACTION_PLAN: $msIconName="icon-ci_action_plan.gif"; break;
          default:                    $msIconName="";                       break;
        }
        
        $msValue = trim($this->coCellBox->getValue());
        if($msIconName){
          $msToolTipText = $msValue;
          $moToolTip = new FWDToolTip();
          $moToolTip->setAttrShowDelay(1000);
          //$msToolTipText = str_replace('\\','\\\\',$msToolTipText);
          //$msToolTipText = str_replace('"','\"',$msToolTipText);
          $moStr = new FWDString();
          $moStr->setAttrValue($msToolTipText);
          $moToolTip->setObjFWDString($moStr);
          $this->coCellBox->setObjFWDToolTip($moToolTip);
          $moIcon = new FWDIcon();
          $moIcon->setAttrSrc(FWDWebLib::getInstance()->getGfxRef().$msIconName);
          $this->coCellBox->setAttrStringNoEscape('true');
          $this->coCellBox->setValue($moIcon->draw().'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$msValue);
        }
        return $this->coCellBox->draw();
      }
      default:{
        return parent::drawItem();
      }
    }
  }
}

class DownloadFileEvent extends FWDRunnable {
  public function run() {
    $moWebLib = FWDWebLib::getInstance();
    $miDocumentId = $moWebLib->getObject('read_document_id')->getValue();
    $miUserId = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    $miDocInstanceId = $moWebLib->getObject('read_doc_instance_id')->getValue();
    
    $moPMDocReadHistory = new PMDocReadHistory();
    $moPMDocReadHistory->setFieldValue('user_id',$miUserId);
    $moPMDocReadHistory->setFieldValue('document_id',$miDocInstanceId);
    $moPMDocReadHistory->setFieldValue('read_date',ISMSLib::ISMSTime());
    $moPMDocReadHistory->insert();
    
    $moPMDocument = new PMDocument();
    
    if($moPMDocument->fetchById($miDocumentId)){
      $moPMDocInstance = new PMDocInstance();
      if ($moPMDocInstance->fetchById($miDocInstanceId)) {
        $msPath = $moPMDocInstance->getFieldValue('doc_instance_path');
        $msFileName = $moPMDocInstance->getFieldValue('doc_instance_file_name');
        header('Cache-Control: ');// leave blank to avoid IE errors
        header('Pragma: ');// leave blank to avoid IE errors
        header('Content-type: application/octet-stream');
        header('Content-Disposition: attachment; filename="'.$msFileName.'"');
        $moCrypt = new FWDCrypt();
        $moFP = fopen($msPath,'rb');
        $moCrypt->setIV(fread($moFP,strlen($moCrypt->getIV())));
        while(!feof($moFP)) {
          echo $moCrypt->decryptNoBase64(fread($moFP,16384));
        }
        fclose($moFP);
      }
    }
  }
}

class VisitLinkEvent extends FWDRunnable {
  public function run() {
    $moWebLib = FWDWebLib::getInstance();
    $moReader = new PMDocumentReader();
    $miDocInstanceId = $moWebLib->getObject('read_doc_instance_id')->getValue();
    
    $miDocumentId = $moWebLib->getObject('read_document_id')->getValue();
    $miUserId = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    
    $moPMDocReadHistory = new PMDocReadHistory();
    $moPMDocReadHistory->setFieldValue('user_id',$miUserId);
    $moPMDocReadHistory->setFieldValue('document_id',$miDocInstanceId);
    $moPMDocReadHistory->setFieldValue('read_date',ISMSLib::ISMSTime());
    $moPMDocReadHistory->insert();
    
    $moPMDocInstance = new PMDocInstance();
    if ($moPMDocInstance->fetchById($miDocInstanceId))
      $msURL = $moPMDocInstance->getFieldValue('doc_instance_link');
    if ($msURL)
      echo "js_open('{$msURL}','document_link',740,502,'center_screen');";
  }
}

class FilterEvent extends FWDRunnable {
  public function run() {
    $moGrid = FWDWebLib::getObject('grid_all');
    $moGrid->execEventPopulate(); 
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new FilterEvent('filter_all'));
    $moStartEvent->addSubmitEvent(new DownloadFileEvent('download_file'));
    $moStartEvent->addAjaxEvent(new VisitLinkEvent('visit_link_event'));
    
    //instala a seguran�a de ACL na p�gina
    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
    //FWDMenuACLSecurity::getInstance()->installMenuACL(FWDWebLib::getObject('menu'),FWDACLSecurity::getInstance());
    
    $moGrid = FWDWebLib::getObject("grid_all");
    $moHandler = new QueryGridDocuments(FWDWebLib::getConnection());
    
    $miType = (int) FWDWebLib::getObject('filter_type')->getValue();
    if($miType) $moHandler->setTypesToShow($miType);

    $moHandler->setTypesToHide(CONTEXT_REGISTER);
    $moHandler->setDocumentClassification((int) FWDWebLib::getObject('filter_classification')->getValue());

    $moHandler->setDocumentStatus(FWDWebLib::getObject('filter_status')->getValue());    
    
    $moGrid->setQueryHandler($moHandler);
    //$moGrid->setObjFwdDrawGrid(new DocumentsDrawGrid('grid_all'));
    $moGrid->setObjFwdDrawGrid(new AllDocumentsGrid('grid_all'));
  }
}


class ScreenEvent extends FWDRunnable {
  public function run(){
    $moHandler = new QuerySelectDocumentType(FWDWebLib::getConnection());    
    $moSelect = FWDWebLib::getObject('select_document_classification');
    $moSelect->setQueryHandler($moHandler);
    $moSelect->populate();
    
    $moContext = new ISMSContextObject();   
    $moSelect = FWDWebLib::getObject('select_document_type');
    $moSelect->setItemValue(CONTEXT_NONE             ,FWDLanguage::getPHPStringValue('si_no_type','Sem Tipo')                );
    $moSelect->setItemValue(CONTEXT_AREA             ,ISMSContextObject::getContextObject(CONTEXT_AREA)->getLabel()          );
    $moSelect->setItemValue(CONTEXT_PROCESS          ,ISMSContextObject::getContextObject(CONTEXT_PROCESS)->getLabel()       );
    $moSelect->setItemValue(CONTEXT_ASSET            ,ISMSContextObject::getContextObject(CONTEXT_ASSET)->getLabel()         );
    $moSelect->setItemValue(CONTEXT_CONTROL          ,ISMSContextObject::getContextObject(CONTEXT_CONTROL)->getLabel()       );
    $moSelect->setItemValue(CONTEXT_SCOPE            ,ISMSContextObject::getContextObject(CONTEXT_SCOPE)->getLabel()         );
    $moSelect->setItemValue(CONTEXT_POLICY           ,ISMSContextObject::getContextObject(CONTEXT_POLICY)->getLabel()        );
    $moSelect->setItemValue(DOCUMENT_TYPE_MANAGEMENT ,FWDLanguage::getPHPStringValue('si_management','Gest�o')               );
    $moSelect->setItemValue(DOCUMENT_TYPE_OTHERS     ,FWDLanguage::getPHPStringValue('si_others','Outros')                   );
    if (FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(INCIDENT_MODE))
      $moSelect->setItemValue(CONTEXT_CI_ACTION_PLAN   ,ISMSContextObject::getContextObject(CONTEXT_CI_ACTION_PLAN)->getLabel());
    
    $moContext = new ISMSContextObject();		
    $moSelect = FWDWebLib::getObject('select_document_status');
    $moSelect->setItemValue(CONTEXT_STATE_DOC_DEVELOPING,$moContext->getContextStateAsString(CONTEXT_STATE_DOC_DEVELOPING));
    $moSelect->setItemValue(CONTEXT_STATE_DOC_PENDANT,$moContext->getContextStateAsString(CONTEXT_STATE_DOC_PENDANT));
    $moSelect->setItemValue(CONTEXT_STATE_DOC_TO_BE_PUBLISHED,$moContext->getContextStateAsString(CONTEXT_STATE_DOC_TO_BE_PUBLISHED));
    $moSelect->setItemValue(CONTEXT_STATE_DOC_PUBLISHED,$moContext->getContextStateAsString(CONTEXT_STATE_DOC_PUBLISHED));
    $moSelect->setItemValue(CONTEXT_STATE_DOC_REVISION,$moContext->getContextStateAsString(CONTEXT_STATE_DOC_REVISION));
    $moSelect->setItemValue(CONTEXT_STATE_DOC_OBSOLETE,$moContext->getContextStateAsString(CONTEXT_STATE_DOC_OBSOLETE));
    
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        function refresh_grid(){
          js_refresh_grid('grid_all');
        }
        function read(piDocumentId,piDocInstanceId,psType) {
          gebi('read_document_id').value=piDocumentId;
          gebi('read_doc_instance_id').value=piDocInstanceId;
          switch(psType) {
            case "link":
              trigger_event('visit_link_event',3);
              break;
            case "file":
              js_submit('download_file','ajax');
              break;
            default:
              gebi('read_document_id').value=0;
              break;
          }
        }
      </script>
    <?
  }
}
//testa se a licen�a utilizada permite acessar o m�dulo de documenta��o
  ISMSLib::testUserPermissionToPoliceMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_all.xml");

?>