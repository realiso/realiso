<?php

include_once 'include.php';

function getSectionFromFields(){
  $moSection = new RMSectionBestPractice();

  $msSectionName = FWDWebLib::getObject('section_name')->getValue();
  $miSectionParent = FWDWebLib::getObject('root_section_id')->getValue();
  if($miSectionParent) $moSection->setFieldValue('section_parent_id', $miSectionParent);
  $moSection->setFieldValue('section_name', $msSectionName);

  return $moSection;
}

function save($poContext,$pbHasSensitiveChanges,$piContextId=0){
  if($piContextId){
    //teste para verificar se o sistema n�o est� sendo hakeado no evento de update
    $moCtxUserTest = new RMSectionBestPractice();
    $moCtxUserTest->testPermissionToEdit($piContextId);
    $poContext->update($piContextId,true,$pbHasSensitiveChanges);
  }else{
    $poContext->insert();
  }
  echo "soWindow = soPopUpManager.getPopUpById('popup_section_edit').getOpener();"
      ."if(soWindow.refresh_grid) soWindow.refresh_grid();"
      ."soPopUpManager.closePopUp('popup_section_edit');";
}

class ConfirmEditEvent extends FWDRunnable {
  public function run(){
    $miSectionId = FWDWebLib::getObject('section_id')->getValue();
    $moSection = getSectionFromFields();
    save($moSection,true,$miSectionId);
  }
}

class SaveSectionEvent extends FWDRunnable {
  public function run() {
    $miSectionId = FWDWebLib::getObject('section_id')->getValue();
    $moSection = getSectionFromFields();
    $moSection->setHash(FWDWebLib::getObject('hash')->getValue());
    $mbHasSensitiveChanges = $moSection->hasSensitiveChanges();
    $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    if($miSectionId && $mbHasSensitiveChanges && $miUserId != $moSection->getApprover()){
      $msTitle = FWDLanguage::getPHPStringValue('tt_reapproval','Reaprova��o');
      $msMessage = FWDLanguage::getPHPStringValue('st_sensitive_data_reapproval','Voc� est� alterando dados sens�veis, que necessitam passar por uma reaprova��o. Deseja confirmar as altera��es?');
      $msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
                      "soWindow = soPopUp.getOpener();" .
                      "soWindow.trigger_event('confirm_edit',3);";
      ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,75);
    }else{
      save($moSection,$mbHasSensitiveChanges,$miSectionId);
    }
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
    $moStartEvent->addAjaxEvent(new SaveSectionEvent('save_section_event'));
    $moStartEvent->addAjaxEvent(new ConfirmEditEvent('confirm_edit'));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $miSectionId = FWDWebLib::getObject('section_id')->getValue();
    $moWindowTitle = FWDWebLib::getObject('window_title');
    $moIcon = FWDWebLib::getObject('tooltip_icon');

    if($miSectionId){
      //teste para verificar se o sistema n�o est� sendo hakeado
      $moCtxUserTest = new RMSectionBestPractice();
      $moCtxUserTest->testPermissionToEdit($miSectionId);
      
      $moSection = new RMSectionBestPractice();
      $moSection->fetchById($miSectionId);
      FWDWebLib::getObject('section_name')->setValue($moSection->getFieldValue('section_name'));
      $moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_section_editing', 'Edi��o de Se��o'));

      $moToolTip = ISMSLib::getInstance()->getToolTipCreateModifyById($miSectionId);
      $moToolTip->setAttrWidth(350);
      $moIcon->addObjFWDEvent($moToolTip);
      
      FWDWebLib::getObject('hash')->setValue($moSection->getHash());
    }else{
      //teste para verificar se o sistema n�o est� sendo hakeado
      $moCtxUserTest = new RMSectionBestPractice();
      $moCtxUserTest->testPermissionToInsert();

      $moIcon->setAttrDisplay('false');
      $moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_section_adding', 'Adi��o de Se��o'));
    }

    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        gebi('section_name').focus();
      </script>
    <?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_section_edit.xml');

?>