<?php

include_once 'include.php';
include_once $handlers_ref . "select/QuerySelectEventType.php";

function getEventFromFields(){
  $moEvent = new RMEvent();

  $msEventDescription = FWDWebLib::getObject('event_description')->getValue();
  $msEventObservation = FWDWebLib::getObject('event_observation')->getValue();
  $miEventType = FWDWebLib::getObject('select_event_type')->getValue();
  $miEventPropagate = FWDWebLib::getObject('check_controller')->getValue();
  
  if($miEventPropagate)
    $moEvent->setFieldValue('event_propagate',1);
  else
    $moEvent->setFieldValue('event_propagate',0);
    
  $moEvent->setFieldValue('event_description', $msEventDescription);
  $moEvent->setFieldValue('event_observation', $msEventObservation);
  if($miEventType)
    $moEvent->setFieldValue('event_type', $miEventType);
  else
    $moEvent->setFieldValue('event_type', 'null');

  $moEvent->setFieldValue('event_impact',FWDWebLib::getObject('event_impact')->getValue());
  
  return $moEvent;
}

function save($poContext,$pbHasSensitiveChanges,$piContextId=0){
  if($piContextId){
    //teste para verificar se o sistema n�o est� sendo hakeado no evento de update
    $moCtxUserTest = new RMEvent();
    $moCtxUserTest->testPermissionToEdit($piContextId);
    
    $poContext->update($piContextId,true,$pbHasSensitiveChanges);
  }else{
    $miEventCategory = FWDWebLib::getObject('root_category_id')->getValue();
    $poContext->setFieldValue('event_category_id', $miEventCategory);
    $miPropagate = FWDWebLib::getObject('check_controller')->getValue() ? 1:0;
    $poContext->setFieldValue('event_propagate', $miPropagate);
    $poContext->insert();
  }
  echo "soWindow = soPopUpManager.getPopUpById('popup_event_edit').getOpener();"
      ."if(soWindow.refresh_grid) soWindow.refresh_grid();"
      ."soPopUpManager.closePopUp('popup_event_edit');";
}

class ConfirmEditEvent extends FWDRunnable {
  public function run(){
    $miEventId = FWDWebLib::getObject('event_id')->getValue();
    $moEvent = getEventFromFields();
    save($moEvent,true,$miEventId);
  }
}

class SaveEventEvent extends FWDRunnable {
  public function run(){
    $miEventId = FWDWebLib::getObject('event_id')->getValue();
    $moEvent = getEventFromFields();
    $moEvent->setHash(FWDWebLib::getObject('hash')->getValue());
    $mbHasSensitiveChanges = $moEvent->hasSensitiveChanges();
    $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    if($miEventId && $mbHasSensitiveChanges && $miUserId != $moEvent->getApprover()){
      $msTitle = FWDLanguage::getPHPStringValue('tt_reapproval','Reaprova��o');
      $msMessage = FWDLanguage::getPHPStringValue('st_sensitive_data_reapproval','Voc� est� alterando dados sens�veis, que necessitam passar por uma reaprova��o. Deseja confirmar as altera��es?');
      $msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
                      "soWindow = soPopUp.getOpener();" .
                      "soWindow.trigger_event('confirm_edit',3);";
      ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,75);
    }else{
      save($moEvent,$mbHasSensitiveChanges,$miEventId);
    }
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
    $moStartEvent->addAjaxEvent(new SaveEventEvent('save_event_event'));
    $moStartEvent->addAjaxEvent(new ConfirmEditEvent('confirm_edit'));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $miEventId = FWDWebLib::getObject('event_id')->getValue();
    $moWindowTitle = FWDWebLib::getObject('window_title');
    $moIcon = FWDWebLib::getObject('tooltip_icon');

    $moHandler = new QuerySelectEventType(FWDWebLib::getConnection());    
    $moSelect = FWDWebLib::getObject('select_event_type');
    $moSelect->setQueryHandler($moHandler);
    $moSelect->populate();
    
    if($miEventId){
      //teste para verificar se o sistema n�o est� sendo hakeado
      $moCtxUserTest = new RMEvent();
      $moCtxUserTest->testPermissionToEdit($miEventId);

      $moEvent = new RMEvent();
      $moEvent->fetchById($miEventId);
      
      FWDWebLib::getObject('event_description')->setValue($moEvent->getFieldValue('event_description'));
      FWDWebLib::getObject('event_observation')->setValue($moEvent->getFieldValue('event_observation'));
      FWDWebLib::getObject('select_event_type')->checkItem($moEvent->getFieldValue('event_type'));
      FWDWebLib::getObject('event_impact')->setValue($moEvent->getFieldValue('event_impact'));
      if($moEvent->getFieldValue('event_propagate')==1)
        FWDWebLib::getObject('check_controller')->setValue('suggested_event');

      
      $moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_event_editing', 'Edi��o de Evento'));
      
      // DESABILITAR O CHECKBOX DE EVENTO SUGERIDO
      FWDWebLib::getObject('label_suggested_event')->setShouldDraw(false);
      FWDWebLib::getObject('ckbox_suggested_event')->setShouldDraw(false);
      
      $moToolTip = ISMSLib::getInstance()->getToolTipCreateModifyById($miEventId);
      $moToolTip->setAttrWidth(350);
      $moIcon->addObjFWDEvent($moToolTip);
      
      FWDWebLib::getObject('hash')->setValue($moEvent->getHash());
    }else{
      //teste para verificar se o sistema n�o est� sendo hakeado
      $moCtxUserTest = new RMEvent();
      $moCtxUserTest->testPermissionToInsert();

      $moIcon->setAttrDisplay('false');
      $moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_event_adding', 'Adi��o de Evento'));
    }

    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        gebi('event_description').focus();
      </script>
    <?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_event_edit.xml');

?>