<?php
include_once "include.php";
include_once $handlers_ref . "grid/QueryGridStandardSearch.php";
include_once $handlers_ref . "grid/QueryGridCurrentStandards.php";

function getCurrentStandardIds() {
	$msValues = FWDWebLib::getObject('current_standards_ids')->getValue();	
	$maIds = explode(':', $msValues);
	unset($maIds[0]);
	return $maIds;
}

class GridStandards extends FWDDrawGrid {
  public function drawItem(){
    switch($this->ciColumnIndex){
      case 1:
        $this->coCellBox->setIconSrc("icon-standard.gif");
        return parent::drawItem();
        break;
      
      case 2:
        $this->coCellBox->setAttrStringNoEscape("true");
        $this->coCellBox->setValue("<a href='javascript:open_visualize(".$this->caData[1].",".CONTEXT_STANDARD.",\"".uniqid()."\")'>{$this->caData[2]}</a>");
        return $this->coCellBox->draw();
        break;
      
      default:
        return parent::drawItem();
        break;
    }
  }
}

class SearchStandardEvent extends FWDRunnable {
	public function run() {		
		$moGrid = FWDWebLib::getObject('grid_standard_search');
		$moGrid->execEventPopulate();
	}
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new SearchStandardEvent('search_standard_event'));    
    
    $moGrid = FWDWebLib::getObject('grid_standard_search');
    $moHandler = new QueryGridStandardSearch(FWDWebLib::getConnection());
    $moHandler->setName(FWDWebLib::getObject('var_standard_name')->getValue());
    $moHandler->dontShowStandards(getCurrentStandardIds());        	
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new GridStandards());    
    
    $moGrid = FWDWebLib::getObject('grid_current_standards');
    $moHandler = new QueryGridCurrentStandards(FWDWebLib::getConnection());
    $moHandler->showStandards(getCurrentStandardIds());
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new GridStandards());      		    
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    //teste para verificar se o sistema n�o est� sendo hakeado
    $moCtxUserTest = new RMBestPracticeStandard();
    $moCtxUserTest->testPermissionToInsert();    
    
  	FWDWebLib::getObject('current_standards_ids')->setValue(FWDWebLib::getObject('current_standards')->getValue());
  	
  	$moGrid = FWDWebLib::getObject('grid_current_standards');
    $moHandler = new QueryGridCurrentStandards(FWDWebLib::getConnection());
    $moHandler->showStandards(getCurrentStandardIds());
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new GridStandards());
        
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
?>
<script language="javascript">
  gebi('standard_name').focus();
  
  function refresh_grid() {
    js_refresh_grid('grid_standard_search');
    js_refresh_grid('grid_current_standards');
  }
  function enter_standard_search_event(e)
	{
		if (!e) e = window.event;
		if(e['keyCode']==13) {
      gebi('var_standard_name').value = gebi('standard_name').value;
      gobi('grid_standard_search').setPopulate(true);
	    trigger_event("search_standard_event","3");
		}
	}
	FWDEventManager.addEvent(gebi('standard_name'), 'keydown', enter_standard_search_event);
</script>
<?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_standard_add.xml");
?>