<?php
include_once "include.php";
include_once $handlers_ref . "grid/QueryGridPlanRange.php";
set_time_limit(3000);

class ConfirmRemove extends FWDRunnable {
	public function run(){
		$planRangeId = FWDWebLib::getObject('id')->getValue();

		$planRange = new CMPlanRange();
		$planRange->fetchById($planRangeId);
		$planRangeName = ISMSLib::truncateString($planRange->getFieldValue('description'), 70);

		$miPopupSize = 50;
		if (strlen($planRangeName) > 100) {
			$miPopupSize = 100;
		}
		if (strlen($planRangeName) > 200) {
			$miPopupSize = 150;
		}

		$msTitle = FWDLanguage::getPHPStringValue('tt_remove_planRange','Remover PlanRangeo');
		$msMessage = FWDLanguage::getPHPStringValue('st_remove_planRange_confirm',"Voc� tem certeza de que deseja remover o Intervalo de plano <b>%planRange_name%</b>?");
		$msMessage = str_replace('%planRange_name%', $planRangeName, $msMessage);

		$msPlanRangeValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
		    				"soWindow = soPopUp.getOpener();" .
		    				"soWindow.remove_planRange();";

		ISMSLib::openConfirm($msTitle,$msMessage,$msPlanRangeValue,$miPopupSize);
	}
}


class RemovePlanRangeEvent extends FWDRunnable {
	public function run(){
		$planRangeId = FWDWebLib::getObject('id')->getValue();
		$planRange = new CMPlanRange();
		$planRange->delete($planRangeId);
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));
		$moStartEvent->addAjaxEvent(new ConfirmRemove('confirm_remove'));
		$moStartEvent->addAjaxEvent(new RemovePlanRangeEvent('remove_planRange_event'));

		//instala a seguran�a de ACL na p�gina
		FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
		FWDMenuACLSecurity::getInstance()->installMenuACL(FWDWebLib::getObject('menu'),FWDACLSecurity::getInstance());

		$moGrid = FWDWebLib::getObject("grid_planRange");

		$moHandler = new QueryGridPlanRange(FWDWebLib::getConnection());
		$moGrid->setObjFwdDrawGrid(new GridPlanRange());
		$moGrid->setQueryHandler($moHandler);
	}
}

class GridPlanRange extends FWDDrawGrid {
	public function drawItem(){
		switch($this->ciColumnIndex){
			case $this->getIndexByAlias('id'):
				continue;
			case $this->getIndexByAlias('description'):
				$desc = $this->getFieldValue('description');
				if(strstr($desc,"%1%")){
					$desc = str_replace("%1%", " Minutos", $desc);
				}
				if(strstr($desc,"%2%")){
					$desc = str_replace("%2%", " Horas", $desc);
				}
				if(strstr($desc,"%3%")){
					$desc = str_replace("%3%", " Dias", $desc);
				}
				if(strstr($desc,"%4%")){
					$desc = str_replace("%4%", " Meses", $desc);
				}
				$this->coCellBox->setValue($desc);
				return parent::drawItem();
			default:
				return parent::drawItem();
				break;
		}
	}
}

class ScreenEvent extends FWDRunnable {
	public function run() {
		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>

<script language="javascript">	

		function refresh_grid() {
			js_refresh_grid('grid_planRange');		
		}

		function remove_planRange() {
			trigger_event('remove_planRange_event', 2);
			refresh_grid();
		}

</script>
		<?
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_plan_ranges.xml");
?>