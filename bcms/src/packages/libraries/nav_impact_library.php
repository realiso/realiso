<?php
include_once "include.php";
include_once $handlers_ref . "grid/QueryGridImpactLibrary.php";
set_time_limit(3000);

class ConfirmRemove extends FWDRunnable {
	public function run(){
		$impactId = FWDWebLib::getObject('impact_id')->getValue();

		$impact = new CMImpact();
		$impact->fetchById($impactId);
		$impactName = ISMSLib::truncateString($impact->getFieldValue('impact_name'), 70);

		$miPopupSize = 50;
		if (strlen($impactName) > 100) {
			$miPopupSize = 100;
		}
		if (strlen($impactName) > 200) {
			$miPopupSize = 150;
		}

		$msTitle = FWDLanguage::getPHPStringValue('tt_remove_impact','Remover Impacto');
		$msMessage = FWDLanguage::getPHPStringValue('st_remove_impact_confirm',"Voc� tem certeza de que deseja remover o impacto <b>%impact_name%</b>? (Isso tamb�m ir� remover este impacto de todos os cen�rios)");
		$msMessage = str_replace('%impact_name%', $impactName, $msMessage);

		$msImpactValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
		    				"soWindow = soPopUp.getOpener();" .
		    				"soWindow.remove_impact();";

		ISMSLib::openConfirm($msTitle,$msMessage,$msImpactValue,$miPopupSize);
	}
}


class RemoveImpactEvent extends FWDRunnable {
	public function run(){
		$impactId = FWDWebLib::getObject('impact_id')->getValue();
		$impact = new CMImpact();
		$impact->delete($impactId);

		$impactDamage = new CMImpactDamage();
		$impactDamage->createFilter($impactId, 'impact_id');
		$impactDamage->select();
		while($impactDamage->fetch()){
			$impactDamage->delete($impactDamage->getFieldValue('impact_damage_id'));
		}
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));
		$moStartEvent->addAjaxEvent(new ConfirmRemove('confirm_remove'));
		$moStartEvent->addAjaxEvent(new RemoveImpactEvent('remove_impact_event'));

		//instala a seguran�a de ACL na p�gina
		FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
		FWDMenuACLSecurity::getInstance()->installMenuACL(FWDWebLib::getObject('menu'),FWDACLSecurity::getInstance());

		$moGrid = FWDWebLib::getObject("grid_impact");

		$moHandler = new QueryGridImpactLibrary(FWDWebLib::getConnection());
		$moGrid->setObjFwdDrawGrid(new GridImpact());
		$moGrid->setQueryHandler($moHandler);
	}
}

class GridImpact extends FWDDrawGrid {
	public function drawItem(){
		switch($this->ciColumnIndex){
			case $this->getIndexByAlias('impact_id'):
				continue;
			default:
				return parent::drawItem();
				break;
		}
	}
}

class ScreenEvent extends FWDRunnable {
	public function run() {
		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
<script language="javascript">			
		function refresh_grid() {
			js_refresh_grid('grid_impact');		
		}
		function remove_impact() {
			trigger_event('remove_impact_event', 2);
			refresh_grid();
		}

		</script>
		<?
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_impact_library.xml");
?>