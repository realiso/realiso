<?php

include_once 'include.php';
include_once $handlers_ref . "select/QuerySelectEventType.php";
include_once $handlers_ref . "grid/QueryGridBestPracticeSearchById.php";
include_once $handlers_ref . "QueryTemplateBestPractice.php";

class SubmitEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    $miContextId = $moSession->attributeExists("documentTemplateContextId")?$moSession->getAttrDocumentTemplateContextId():$moWebLib->getObject('context_id')->getValue();
    $moContext = new PMTemplate();
    $moContext->testPermissionToEdit($miContextId);
    
    $moFile = FWDWebLib::getObject('file');
    $mbUploadedFile = $moFile->isUploaded();
    $miFileError = 0;
    
    if($mbUploadedFile){
      $miFileError = $moFile->getErrorCode();
      if($miFileError==FWDFile::E_NONE){
        // Upload sem erros
        
        
        /* EXTRA��O DE TEXTO DOS ARQUIVOS DESABILITADA POR CAUSA DO PROBLEMA DE CODIFICA��O
        $moTextExtractor = new FWDTextExtractor();
        $msFileExtension = strtolower(substr($moFile->getFileName(),strrpos($moFile->getFileName(),'.')+1));
        $msFileContent = $moTextExtractor->extractTextFromFile($moFile->getTempFileName(),$msFileExtension);
        /*/
        $msFileContent = '';
        //*/
        
        
        $moCrypt = new FWDCrypt();
        $msDirectory = (ISMSLib::isCompiled() ? $GLOBALS['files_ref'] : '../policy/files/');
        if(is_writable($msDirectory)){
          $msFileName=$moFile->getFileName();
          $msHashFileName = md5($msFileName.uniqid());
          $msPath = "{$msDirectory}{$msHashFileName}.aef";
          $miFileSize = filesize($moFile->getTempFileName());
          $moWriteFP = fopen($msPath,'wb');
          fwrite($moWriteFP,$moCrypt->getIV());
          $moFP = fopen($moFile->getTempFileName(),'rb');
          while(!feof($moFP)) {
            fwrite($moWriteFP,$moCrypt->encryptNoBase64(fread($moFP,16384)));
          }
          fclose($moFP);
          fclose($moWriteFP);
          $miDiskFileSize = filesize($msPath);
          
          if(!$moSession->attributeExists('TemplateDocFilePath')){
            $moSession->addAttribute('TemplateDocFilePath');
          }
          $moSession->setAttrTemplateDocFilePath($msPath);
          if(!$moSession->attributeExists('TemplateDocFileName')){
            $moSession->addAttribute('TemplateDocFileName');
          }
          $moSession->setAttrTemplateDocFileName($msFileName);
          if(!$moSession->attributeExists('TemplateFileSize')){
            $moSession->addAttribute('TemplateFileSize');
          }
          $moSession->setAttrTemplateFileSize($miFileSize);
          if(!$moSession->attributeExists('TemplateDiskFileSize')){
            $moSession->addAttribute('TemplateDiskFileSize');
          }
          $moSession->setAttrTemplateDiskFileSize($miDiskFileSize);
          if(!$moSession->attributeExists('TemplateDocFileContent')){
            $moSession->addAttribute('TemplateDocFileContent');
          }
          $moSession->setAttrTemplateDocFileContent($msFileContent);
        }else{
          //O diret�rio n�o p�de ser escrito!
          trigger_error("Directory '{$msDirectory}' is not writable.",E_USER_ERROR);
        }
      }else{
        // Erro no upload -> armazena c�digo do erro na sess�o
        if(!$moSession->attributeExists('uploadErrorCode')){
          $moSession->addAttribute('uploadErrorCode');
        }
        $moSession->setAttrUploadErrorCode($miFileError);
      }
    }
    // Sinaliza que, com ou sem arquivo, com ou sem erro, acabou o submit
    if(!$moSession->attributeExists('policySaveCompleteTemplateDoc')){
      $moSession->addAttribute('policySaveCompleteTemplateDoc');
    }
    $moSession->setAttrPolicySaveCompleteTemplateDoc(true);
  }
}

class SaveCompleteEvent extends FWDRunnable {
  public function run(){
    $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    if($moSession->attributeExists("policySaveCompleteTemplateDoc")){
      if($moSession->getAttrPolicySaveCompleteTemplateDoc()){
        $moSession->deleteAttribute("policySaveCompleteTemplateDoc");
        
        if($moSession->attributeExists('uploadErrorCode') && $moSession->getAttrUploadErrorCode()){
          // Trata erro no upload
          $miErrorCode = $moSession->getAttrUploadErrorCode();
          $moSession->deleteAttribute('uploadErrorCode');
          
          if($miErrorCode & FWDFile::E_MAX_SIZE){
            echo "gobi('warning_max_size').show();";
          }else{
            echo "gobi('warning_upload_error').show();";
          }
          echo "soTabManager.unlockTabs();";
        }else{
          // Upload feito com sucesso
          $moWebLib = FWDWebLib::getInstance();
          $miContextId = $moSession->attributeExists("documentTemplateContextId")?$moSession->getAttrDocumentTemplateContextId():$moWebLib->getObject('context_id')->getValue();
          $moPMTemplate = new PMTemplate();
          $moPMTemplate->testPermissionToEdit($miContextId);
          if ($moSession->attributeExists("TemplateDocFilePath")) {
            //delete o arquivo antigo se ele existir
            $moPMTemplateOldPath = new PMTemplate();
            $moPMTemplateOldPath->fetchById($miContextId);
            $msOldPath = $moPMTemplateOldPath->getFieldValue('template_path');
            if ($msOldPath && is_file($msOldPath)) {
              unlink($msOldPath);
            }
            $msPath = $moSession->getAttrTemplateDocFilePath("TemplateDocFilePath");
            $msFileName = $moSession->attributeExists("TemplateDocFileName")?$moSession->getAttrTemplateDocFileName():"";
            $miFileSize = $moSession->attributeExists("TemplateFileSize")?$moSession->getAttrTemplateFileSize():0;
            $miDiskFileSize = $moSession->attributeExists("TemplateDiskFileSize")?$moSession->getAttrTemplateDiskFileSize():0;
            
            $moPMTemplatePath = new PMTemplate();
            $moPMTemplatePath->setFieldValue('template_path',$msPath);
            $moPMTemplatePath->setFieldValue('template_file_name',$msFileName);
            $moPMTemplatePath->setFieldValue('template_file_size',$miFileSize);
            $moPMTemplatePath->update($miContextId);
            
            $msFileContent = $moSession->attributeExists("TemplateDocFileContent")?$moSession->getAttrTemplateDocFileContent():"";
            $moTemplateContentAux = new PMTemplateContent();
            $moTemplateContent = new PMTemplateContent();
            /*feita a convers�o para ir os caracteres corretamente para o banco*/
            $msFileContent = $msFileContent;
            if($moTemplateContentAux->fetchById($miContextId)){
              $moTemplateContent->setFieldValue('template_content',$msFileContent);
              $moTemplateContent->update($miContextId);
            }else{
              $moTemplateContent->setFieldValue('template_content',$msFileContent);
              $moTemplateContent->setFieldValue('template_content_id',$miContextId);
              $moTemplateContent->insert();
            }
          }
          if($moSession->attributeExists("TemplateDocFilePath"))       $moSession->deleteAttribute("TemplateDocFilePath");
          if($moSession->attributeExists("TemplateDocFileName"))       $moSession->deleteAttribute("TemplateDocFileName");
          if($moSession->attributeExists("TemplateFileSize"))          $moSession->deleteAttribute("TemplateFileSize");
          if($moSession->attributeExists("TemplateDiskFileSize"))      $moSession->deleteAttribute("TemplateDiskFileSize");
          if($moSession->attributeExists("TemplateDocFileContent"))    $moSession->deleteAttribute("TemplateDocFileContent");
          if($moSession->attributeExists("documentTemplateContextId")) $moSession->deleteAttribute("documentTemplateContextId");
          echo "soWindow = soPopUpManager.getPopUpById('popup_document_template_edit').getOpener();"
              ."if(soWindow.refresh_grid) soWindow.refresh_grid();"
              ."soTabManager.unlockTabs();"
              ."self.close();";
        }
      }else{
        echo "setTimeout('trigger_event(\"save_complete_event\",3)',200);";
      }
    }
  }
}

class SaveEvent extends FWDRunnable {
  public function run() {
    $moWebLib = FWDWebLib::getInstance();
    $miContextId = FWDWebLib::getObject('var_context_id')->getValue();
    $msInsert = '';
    $moPMTemplate = new PMTemplate();
    if($miContextId){
      $moPMTemplate->testPermissionToEdit($miContextId);
    }else{
      $moPMTemplate->testPermissionToInsert();
    }
    
    // Previne bugs por lixo deixado na sess�o
    $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    if($moSession->attributeExists('policySaveCompleteTemplateDoc')) $moSession->deleteAttribute('policySaveCompleteTemplateDoc');
    if($moSession->attributeExists('uploadErrorCode'))               $moSession->deleteAttribute('uploadErrorCode');
    if($moSession->attributeExists('TemplateDocFilePath'))           $moSession->deleteAttribute('TemplateDocFilePath');
    if($moSession->attributeExists('TemplateDocFileName'))           $moSession->deleteAttribute('TemplateDocFileName');
    if($moSession->attributeExists('TemplateFileSize'))              $moSession->deleteAttribute('TemplateFileSize');
    if($moSession->attributeExists('TemplateDiskFileSize'))          $moSession->deleteAttribute('TemplateDiskFileSize');
    if($moSession->attributeExists('TemplateDocFileContent'))        $moSession->deleteAttribute('TemplateDocFileContent');
    if($moSession->attributeExists('documentTemplateContextId'))     $moSession->deleteAttribute('documentTemplateContextId');
    
    $moTemplate = new PMTemplate();
    $moTemplate->setFieldValue('template_name'       ,FWDWebLib::getObject('template_name')->getValue());
    $moTemplate->setFieldValue('template_type'       ,FWDWebLib::getObject('sel_template_type')->getValue());
    $moTemplate->setFieldValue('template_description',FWDWebLib::getObject('document_template_description')->getValue());
    $moTemplate->setFieldValue('template_key_words'  ,FWDWebLib::getObject('document_template_key_words')->getValue());
    if($miContextId){
      $moContextObject = new ISMSContextObject();
      $moContext = $moContextObject->getContextObjectByContextId($miContextId);
      $moContext->testPermissionToEdit($miContextId);
      $moTemplate->update($miContextId);
    }else {      
      $miContextId = $moTemplate->insert(true);
      $msInsert .=" gebi('var_context_id').value={$miContextId};";
    }

    $msNewBestPractices = $moWebLib->getObject('var_bp_ids')->getValue();
    if($msNewBestPractices){
      $maNewBestPractices = array_filter(explode(':',$msNewBestPractices));
    }else{
      $maNewBestPractices = array();
    }
    $msOldBestPractices = $moWebLib->getObject('var_orig_bp_ids')->getValue();
    if($msOldBestPractices){
      $maOldBestPractices = array_filter(explode(':',$msOldBestPractices));
    }else{
      $maOldBestPractices = array();
    }
    $moTemplateBP = new PMTemplateBestPractice();
    if(FWDWebLib::getObject('sel_template_type')->getValue()==2805){
    //� de melhor pr�tica, inserir as novas rela��es
      $maToInsert = array_diff($maNewBestPractices,$maOldBestPractices);
      $maToDelete = array_diff($maOldBestPractices,$maNewBestPractices);
      if(count($maToInsert)){
        $moTemplateBP->insertRelations($miContextId,$maToInsert);
      }
      if(count($maToDelete)){
        $moTemplateBP->deleteRelations($miContextId,$maToDelete);
      }
    }elseif(count($maOldBestPractices)){
        $moTemplateBP->deleteRelations($miContextId,$maOldBestPractices);
    }
  
    $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    if (!$moSession->attributeExists("policySaveCompleteTemplateDoc"))
      $moSession->addAttribute("policySaveCompleteTemplateDoc");  
    $moSession->setAttrPolicySaveCompleteTemplateDoc(false);
    if (!$moSession->attributeExists("documentTemplateContextId"))
      $moSession->addAttribute("documentTemplateContextId");
    $moSession->setAttrDocumentTemplateContextId($miContextId);
    echo "$msInsert js_submit('upload','ajax');setTimeout('trigger_event(\"save_complete_event\",3)',200);";
  }
}

class DownloadEvent extends FWDRunnable {
  public function run() {
    $moWebLib = FWDWebLib::getInstance();
    $miContextId = $moWebLib->getObject('context_id')->getValue();
    $moPMTemplate = new PMTemplate();
    
    $moPMTemplate->testPermissionToEdit($miContextId);
    
    if ($moPMTemplate->fetchById($miContextId)) {
      $msPath = $moPMTemplate->getFieldValue('template_path');
      $msFileName = $moPMTemplate->getFieldValue('template_file_name');
      header('Cache-Control: ');// leave blank to avoid IE errors
      header('Pragma: ');// leave blank to avoid IE errors
      header('Content-type: application/octet-stream');
      header('Content-Disposition: attachment; filename="'.$msFileName.'"');
      $moCrypt = new FWDCrypt();
      $moFP = fopen($msPath,'rb');
      $moCrypt->setIV(fread($moFP,strlen($moCrypt->getIV())));
      while(!feof($moFP)) {
        echo $moCrypt->decryptNoBase64(fread($moFP,16384));
      }
      fclose($moFP);
    }
  }
}

class PopulateSelBestPractice extends FWDRunnable{
  public function run(){
    $moHandler = new QueryGridBestPracticeSearchById(FWDWebLib::getConnection());
    $msIds = FWDWebLib::getObject('var_bp_ids')->getValue();
    $moHandler->setSectionsIds($msIds);
    $maAux = $moHandler->getSelectSessionInfo();
    $msAux = serialize($maAux);
    echo ISMSLib::getJavascriptPopulateSelect('sel_bp', $msAux);
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
    
    $moStartEvent->addSubmitEvent(new SubmitEvent("upload"));
    $moStartEvent->addAjaxEvent(new SaveCompleteEvent("save_complete_event"));
    $moStartEvent->addAjaxEvent(new SaveEvent("save_event"));
    $moStartEvent->addSubmitEvent(new DownloadEvent("download"));
    $moStartEvent->addAjaxEvent(new PopulateSelBestPractice("sel_best_practice"));
    
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $miContextId = FWDWebLib::getObject('context_id')->getValue();
    FWDWebLib::getObject("var_context_id")->setValue($miContextId);
    $moWindowTitle = FWDWebLib::getObject('template_document_edit_title');
    $msExecScript = "";
    
    $msTooltipText = FWDLanguage::getPHPStringValue('to_max_file_size',"Tamanho m�ximo de arquivo: %max_file_size%");
    $msMaxFileSize = ISMSLib::formatFileSize(ISMSSaaS::getConfig(ISMSSaaS::MAX_UPLOAD_FILE_SIZE));
    $msTooltipText = str_replace('%max_file_size%',$msMaxFileSize,$msTooltipText);
    FWDWebLib::getObject('tooltip_max_file_size')->setValue($msTooltipText);
    
    if($miContextId){
        //teste para verificar se o sistema n�o est� sendo hakeado
        $moCtxUserTest = new PMTemplate();
        $moCtxUserTest->testPermissionToEdit($miContextId);
        $moContext = new PMTemplate();
        if($moContext->fetchById($miContextId)){
        FWDWebLib::getObject('template_name')->setValue($moContext->getFieldValue('template_name'));
        FWDWebLib::getObject('sel_template_type')->setValue($moContext->getFieldValue('template_type'));
        
        FWDWebLib::getObject('document_template_description')->setValue($moContext->getFieldValue('template_description'));
        FWDWebLib::getObject('document_template_key_words')->setValue($moContext->getFieldValue('template_key_words'));        
        $moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_document_template_editing', 'Edi��o de Modelo de Documento'));
        
        $msPath = $moContext->getFieldValue('template_path');
        if ($msPath && is_file($msPath)) {
          $msFileName = $moContext->getFieldValue('template_file_name');
          $miFileSize = $moContext->getFieldValue('template_file_size');
          $maSizeName = array("GB", "MB", "KB", "B");
          $msFinalFileSize = number_format($miFileSize,2)."&nbsp;".array_pop($maSizeName);
          while (($miFileSize/1024)>1) {
             $miFileSize=$miFileSize/1024;
             $msFinalFileSize = number_format($miFileSize,2)."&nbsp;".array_pop($maSizeName);
          }
          $msFileDate=date("d/m/Y",filectime($msPath));
          FWDWebLib::getObject('icon_download')->setAttrDisplay("true");
          FWDWebLib::getObject('tooltip_download')->setAttrWidth(300);
          FWDWebLib::getObject('tooltip_download')->setValue("Fazer download do arquivo: {$msFileName}<BR>Tamanho: {$msFinalFileSize}<BR>Data de Cria��o: {$msFileDate}",true);
        }else{
          FWDWebLib::getObject('icon_download')->setShouldDraw(false);
        }
      }else{
        FWDWebLib::getObject('context_id')->setValue('');
      }
      
      if($moContext->getFieldValue('template_type')==CONTEXT_CONTROL){
        $msExecScript .=" gobi('vb_add_bp').enable();gobi('vb_remove_bp').enable();gobi('sel_bp').enable();";

        $moQuery = new QueryTemplateBestPractice(FWDWebLib::getConnection());
        $moQuery->setTemplateId($miContextId);
        $maValues = $moQuery->getValues();
        if(count($maValues)){
          $maAuxIds = array();
          foreach($maValues as $miId){
            $maAuxIds[] = $miId[0];
          }
          $msAuxIds = implode(':',array_unique(array_filter($maAuxIds)));
          FWDWebLib::getObject('var_bp_ids')->setValue($msAuxIds);
          FWDWebLib::getObject('var_orig_bp_ids')->setValue($msAuxIds);
          $msAux = serialize($maValues);
          $msExecScript .= ISMSLib::getJavascriptPopulateSelect('sel_bp', $msAux);
        }
      }
      
    }else{
      //teste para verificar se o sistema n�o est� sendo hakeado
      $moCtxUserTest = new PMTemplate();
      $moCtxUserTest->testPermissionToInsert();
      $moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_document_template_adding', 'Adi��o de Modelo de Documento'));
      
      //for�a um relacionamento entre o template sendo criado e a melhor pr�tica passada por par�metro
      //utilizando pelo item de menu criar modelo de documento na nav_best_practice
      if(FWDWebLib::getObject('get_context_type')->getValue()==CONTEXT_BEST_PRACTICE){
        $maIds = explode(':',FWDWebLib::getObject('get_context_id')->getValue());
        $miId = $maIds[0];
        FWDWebLib::getObject('var_bp_ids')->setValue($miId);
        FWDWebLib::getObject('sel_template_type')->setValue(CONTEXT_BEST_PRACTICE);
        FWDWebLib::getObject('sel_template_type')->setAttrDisabled("true");
        FWDWebLib::getObject('vb_add_bp')->setShouldDraw(false);
        FWDWebLib::getObject('vb_remove_bp')->setShouldDraw(false);
        $moContextObject = new ISMSContextObject();
        $moContext = $moContextObject->getContextObjectByContextId($miId);
        $moContext->fetchById($miId);
        $msAux = serialize(array(array($miId,str_replace( array("\n","\r"), array(' ','') ,$moContext->getName()) ) ) );
        $msExecScript .= ISMSLib::getJavascriptPopulateSelect('sel_bp', $msAux);
      }
      
    }
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        gebi('template_name').focus();
        
        <? echo $msExecScript ?>
        function setBestPracticeIds(psBestPracticesIds){
          gebi('var_bp_ids').value = psBestPracticesIds;
          psBestPracticesIds=null;
          trigger_event('sel_best_practice',3);
        }
      </script>
      <?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_document_template_edit.xml');
?>