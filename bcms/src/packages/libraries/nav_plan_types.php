<?php
include_once "include.php";
include_once $handlers_ref . "grid/QueryGridPlanType.php";
set_time_limit(3000);

class ConfirmRemove extends FWDRunnable {
	public function run(){
		$plan_typeId = FWDWebLib::getObject('id')->getValue();

		$plan_type = new CMPlanRangeType();
		$plan_type->fetchById($plan_typeId);
		$plan_typeName = ISMSLib::truncateString($plan_type->getFieldValue('description'), 70);

		$miPopupSize = 50;
		if (strlen($plan_typeName) > 100) {
			$miPopupSize = 100;
		}
		if (strlen($plan_typeName) > 200) {
			$miPopupSize = 150;
		}

		$msTitle = FWDLanguage::getPHPStringValue('tt_remove_plan_type','Remover Tipo');
		$msMessage = FWDLanguage::getPHPStringValue('st_remove_plan_type_confirm',"Voc� tem certeza de que deseja remover o Tipo de plano <b>%plan_type_name%</b>?");
		$msMessage = str_replace('%plan_type_name%', $plan_typeName, $msMessage);

		$msPlanTypeValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
		    				"soWindow = soPopUp.getOpener();" .
		    				"soWindow.remove_plan_type();";

		ISMSLib::openConfirm($msTitle,$msMessage,$msPlanTypeValue,$miPopupSize);
	}
}


class RemovePlanTypeEvent extends FWDRunnable {
	public function run(){
		$plan_typeId = FWDWebLib::getObject('id')->getValue();
		$plan_type = new CMPlanRangeType();
		$plan_type->delete($plan_typeId);
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));
		$moStartEvent->addAjaxEvent(new ConfirmRemove('confirm_remove'));
		$moStartEvent->addAjaxEvent(new RemovePlanTypeEvent('remove_plan_type_event'));

		//instala a seguran�a de ACL na p�gina
		FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
		FWDMenuACLSecurity::getInstance()->installMenuACL(FWDWebLib::getObject('menu'),FWDACLSecurity::getInstance());

		$moGrid = FWDWebLib::getObject("grid_plan_type");

		$moHandler = new QueryGridPlanType(FWDWebLib::getConnection());
		$moGrid->setObjFwdDrawGrid(new GridPlanType());
		$moGrid->setQueryHandler($moHandler);
	}
}

class GridPlanType extends FWDDrawGrid {
	public function drawItem(){
		switch($this->ciColumnIndex){
			case $this->getIndexByAlias('id'):
				continue;
			default:
				return parent::drawItem();
				break;
		}
	}
}

class ScreenEvent extends FWDRunnable {
	public function run() {
		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
<script language="javascript">

		function refresh_grid() {
			js_refresh_grid('grid_plan_type');		
		}

		function remove_plan_type() {
			trigger_event('remove_plan_type_event', 2);
			refresh_grid();
		}

</script>
		<?
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_plan_types.xml");
?>