<?php
include_once "include.php";
include_once $handlers_ref . "grid/QueryGridAssetCategory.php";
set_time_limit(3000);
class GridAssets extends FWDDrawGrid {
	public function drawItem(){
		switch($this->ciColumnIndex){
			case $this->getIndexByAlias('context_id'):
				$msIconRef = '';

				if($this->getFieldValue('context_type') == CONTEXT_CM_THREAT){
					$msIconRef="icon-risk_gray.gif";
					$maNotAllowed = array("M.L.1.1","M.L.1.2","M.L.1.3","M.L.1.7");
				}else{
					$msIconRef="icon-category.gif";
					$maNotAllowed = array("M.L.1.4","M.L.1.5","M.L.1.6","M.L.1.8","M.L.1.9");
				}

				$moACL = FWDACLSecurity::getInstance();
				$moACL->setNotAllowed($maNotAllowed);
				$moMenu = FWDWebLib::getObject('menu');
				$moGrid = FWDWebLib::getObject('grid_Assets');
				$msLine = $moGrid->getAttrName().'_acl_'.$this->ciRowIndex;
				FWDGridMenuACLSecurity::getInstance()->installGridMenuACL($moMenu,$moACL,$moGrid,$msLine);
				$this->coCellBox->setIconSrc($msIconRef);
				return parent::drawItem();
				break;
			case $this->getIndexByAlias('context_name'):
				if($this->getFieldValue('context_type') == CONTEXT_CM_ASSET_CATEGORY){
					if(ISMSLib::userHasACL('M.L.1.1')){
						$this->coCellBox->setValue("<a href=javascript:enter_category(".$this->getFieldValue('context_id').");>".$this->getFieldValue('context_name')."</a>");
					}else{
						$this->coCellBox->setValue("<a href='javascript:open_visualize(".$this->getFieldValue('context_id').",".$this->getFieldValue('context_type').",\"".uniqid()."\");'>".$this->getFieldValue('context_name')."</a>");
					}
				}else{
					if(ISMSLib::userHasACL('M.L.1.5')){
						$this->coCellBox->setValue("<a href=javascript:open_threat_edit(".$this->getFieldValue('context_id').");>".$this->getFieldValue('context_name')."</a>");
					}else{
						$this->coCellBox->setValue("<a href='javascript:open_visualize(".$this->getFieldValue('context_id').",".$this->getFieldValue('context_type').",\"".uniqid()."\");'>".$this->getFieldValue('context_name')."</a>");
					}
				}
				return $this->coCellBox->draw();
				break;
			default:
				return parent::drawItem();
				break;
		}
	}
}

class CategoryConfirmRemove extends FWDRunnable {
	public function run(){
		$miCategoryId = FWDWebLib::getObject('selected_context_id')->getValue();
			
		$moCategory = new CMAssetCategory();
		$moCategory->fetchById($miCategoryId);
		$msCategoryName = ISMSLib::truncateString($moCategory->getFieldValue('name'), 70);
			
		$msTitle = FWDLanguage::getPHPStringValue('tt_remove_category','Remover Categoria');
		$msMessage = FWDLanguage::getPHPStringValue('st_remove_category_confirm',"Voc� tem certeza de que deseja remover a categoria <b>%category_name%</b>?");
		$msMessage = str_replace('%category_name%', $msCategoryName, $msMessage);

		if (ISMSLib::getConfigById(GENERAL_CASCADE_ON)) {
			$msMessage .= " ".FWDLanguage::getPHPStringValue('st_remove_category_cascade_message',"Voc� estar� deletando tamb�m qualquer Ativo relacionado � Categoria, bem como seus Riscos e associa��o dos mesmos com outros elementos do sistema.");
		}

		$msThreatValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
    				"soWindow = soPopUp.getOpener();" .
    				"soWindow.remove_category();";
			
		ISMSLib::openConfirm($msTitle,$msMessage,$msThreatValue,80);
	}
}

class ThreatConfirmRemove extends FWDRunnable {
	public function run(){
		$miThreatId = FWDWebLib::getObject('selected_context_id')->getValue();
			
		$moThreat = new CMThreat();
		$moThreat->fetchById($miThreatId);
		$msThreatName = ISMSLib::truncateString($moThreat->getFieldValue('threat_name'), 70);
			
		$miPopupSize = 50;
		if (strlen($msThreatName) > 100) {
			$miPopupSize = 100;
		}
		if (strlen($msThreatName) > 200) {
			$miPopupSize = 150;
		}

		$msTitle = FWDLanguage::getPHPStringValue('tt_remove_threat','Remover Amea�a');
		$msMessage = FWDLanguage::getPHPStringValue('st_remove_threat_confirm',"Voc� tem certeza de que deseja remover a amea�a <b>%threat_name%</b>?");
		$msMessage = str_replace('%threat_name%', $msThreatName, $msMessage);
			
		$msThreatValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
    				"soWindow = soPopUp.getOpener();" .
    				"soWindow.remove_threat();";
			
		ISMSLib::openConfirm($msTitle,$msMessage,$msThreatValue,$miPopupSize);
	}
}

class RemoveCategoryThreat extends FWDRunnable {
	public function run(){
		$miCategoryId = FWDWebLib::getObject('selected_context_id')->getValue();
		$moCategory = new CMAssetCategory();
		$moCategory->delete($miCategoryId);
	}
}

class RemoveThreatEvent extends FWDRunnable {
	public function run(){
		$miThreatId = FWDWebLib::getObject('selected_context_id')->getValue();
		$moThreat = new CMThreat();
		$moThreat->delete($miThreatId);
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));
		$moStartEvent->addAjaxEvent(new CategoryConfirmRemove('category_confirm_remove'));
		$moStartEvent->addAjaxEvent(new RemoveCategoryThreat('remove_category_threat'));
		$moStartEvent->addAjaxEvent(new ThreatConfirmRemove('threat_confirm_remove'));
		$moStartEvent->addAjaxEvent(new RemoveThreatEvent('remove_threat_event'));
		$moStartEvent->addAjaxEvent(new ChangeScrollingPath('change_scrolling_path'));
		//instala a seguran�a de ACL na p�gina
		FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
		FWDMenuACLSecurity::getInstance()->installMenuACL(FWDWebLib::getObject('menu'),FWDACLSecurity::getInstance());

		$moGrid = FWDWebLib::getObject("grid_Assets");
		$moHandler = new QueryGridAssetCategory(FWDWebLib::getConnection());

		if (!FWDWebLib::getObject("root_category_id")->getValue())
		FWDWebLib::getObject("root_category_id")->setValue(FWDWebLib::getObject("category_id")->getValue());
		elseif(FWDWebLib::getObject("par_scrolling_filter")->getValue()){
			FWDWebLib::getObject("root_category_id")->setValue(FWDWebLib::getObject("par_scrolling_filter")->getValue());
		}
		$moHandler->setRootCategory(FWDWebLib::getObject("root_category_id")->getValue());
		$moGrid->setQueryHandler($moHandler);
		$moGrid->setObjFwdDrawGrid(new GridAssets());
	}
}

class ChangeScrollingPath extends FWDRunnable{
	public function run(){
		echo ISMSLib::getPathScrollCode(LIBRARIES_MODE,CONTEXT_CM_ASSET_CATEGORY,FWDWebLib::getObject('root_category_id')->getValue(),true,"enter_category");
	}
}

class ScreenEvent extends FWDRunnable {
	public function run() {
		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
<script language="javascript">			
		function refresh_grid() {
			js_refresh_grid('grid_Assets');		
		}
		
		function enter_category(category_id) {
			category_id = (""+category_id).split(":")[0];
			gebi('root_category_id').value = category_id;
			if (category_id > 0) {
				//js_show('button_associate_threat');
				js_show('button_insert_threat');
			}
			else {
				//js_hide('button_associate_threat');
				js_hide('button_insert_threat');
			}			
				
			refresh_grid();
			trigger_event('change_scrolling_path',3);
		}
		
		function remove_category() {
			trigger_event('remove_category_threat', 3);
			refresh_grid();				
		}
		
		function remove_threat() {
			trigger_event('remove_threat_event', 3);
			refresh_grid();				
		}
		
		if (gebi('root_category_id').value) {
			js_show('button_associate_threat');
			js_show('button_insert_threat');	
		}
    
    function open_threat_edit(piId){
      isms_open_popup('popup_asset_category_threat_edit','packages/libraries/popup_asset_category_threat_edit.php?threat='+piId,'','true');
    }
		</script>
		<?
	 ISMSLib::getPathScrollCode(LIBRARIES_MODE,CONTEXT_CATEGORY,FWDWebLib::getObject('root_category_id')->getValue(),false,'enter_category');
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_asset_category_threats.xml");
?>