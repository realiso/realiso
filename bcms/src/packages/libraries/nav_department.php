<?php
include_once "include.php";
include_once $handlers_ref . "grid/QueryGridDepartment.php";
set_time_limit(3000);

class ConfirmRemove extends FWDRunnable {
	public function run(){
		$departmentId = FWDWebLib::getObject('id')->getValue();

		$department = new CMDepartment();
		$department->fetchById($departmentId);
		$departmentName = ISMSLib::truncateString($department->getFieldValue('name'), 70);

		$miPopupSize = 50;
		if (strlen($departmentName) > 100) {
			$miPopupSize = 100;
		}
		if (strlen($departmentName) > 200) {
			$miPopupSize = 150;
		}

		$msTitle = FWDLanguage::getPHPStringValue('tt_remove_department','Remover Departamento');
		$msMessage = FWDLanguage::getPHPStringValue('st_remove_department_confirm',"Voc� tem certeza de que deseja remover o departamento <b>%department_name%</b>?");
		$msMessage = str_replace('%department_name%', $departmentName, $msMessage);

		$msDepartmentValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
		    				"soWindow = soPopUp.getOpener();" .
		    				"soWindow.remove_department();";

		ISMSLib::openConfirm($msTitle,$msMessage,$msDepartmentValue,$miPopupSize);
	}
}


class RemoveDepartmentEvent extends FWDRunnable {
	public function run(){
		$departmentId = FWDWebLib::getObject('id')->getValue();
		$department = new CMDepartment();
		$department->delete($departmentId);
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));
		$moStartEvent->addAjaxEvent(new ConfirmRemove('confirm_remove'));
		$moStartEvent->addAjaxEvent(new RemoveDepartmentEvent('remove_department_event'));

		//instala a seguran�a de ACL na p�gina
		FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
		FWDMenuACLSecurity::getInstance()->installMenuACL(FWDWebLib::getObject('menu'),FWDACLSecurity::getInstance());

		$moGrid = FWDWebLib::getObject("grid_department");

		$moHandler = new QueryGridDepartment(FWDWebLib::getConnection());
		$moGrid->setObjFwdDrawGrid(new GridDepartment());
		$moGrid->setQueryHandler($moHandler);
	}
}

class GridDepartment extends FWDDrawGrid {
	public function drawItem(){
		switch($this->ciColumnIndex){
			case $this->getIndexByAlias('id'):
				continue;
			default:
				return parent::drawItem();
				break;
		}
	}
}

class ScreenEvent extends FWDRunnable {
	public function run() {
		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
<script language="javascript">

		function refresh_grid() {
			js_refresh_grid('grid_department');		
		}
		
		function remove_department() {
			trigger_event('remove_department_event', 2);
			refresh_grid();
		}

		</script>
		<?
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_department.xml");
?>