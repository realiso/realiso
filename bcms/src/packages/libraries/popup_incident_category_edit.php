<?php

include_once "include.php";

class SaveEvent extends FWDRunnable {
  public function run(){
    $miCategoryId = FWDWebLib::getObject('category_id')->getValue();
    
    $moCtxUserTest = new CICategory();
    $moCategory = new CICategory();
    $moCategory->setFieldValue("category_name",FWDWebLib::getObject('category_name')->getValue());
    if ($miCategoryId) {
      $moCtxUserTest->testPermissionToEdit($miCategoryId);
      $moCategory->update($miCategoryId);
    }
    else {
      $moCtxUserTest->testPermissionToInsert();
      $miCategoryId = $moCategory->insert(true);
    }
    echo "soWindow = soPopUpManager.getPopUpById('popup_incident_category_edit').getOpener();;
          if (soWindow.refresh_grid) soWindow.refresh_grid();
          soPopUpManager.closePopUp('popup_incident_category_edit');";
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
    $moStartEvent->addAjaxEvent(new SaveEvent('save_event'));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $miCategoryId = intval(FWDWebLib::getObject('category_id')->getValue());
    $moCtxUserTest = new CICategory();
    if ($miCategoryId) {
      $moCtxUserTest->testPermissionToEdit($miCategoryId);
      $moCategory = new CICategory();
      $moCategory->fetchById($miCategoryId);
      FWDWebLib::getObject('category_name')->setValue($moCategory->getFieldValue('category_name'));
    } else {
      $moCtxUserTest->testPermissionToInsert();
      FWDWebLib::getObject('label_title_category')->setValue(FWDLanguage::getPHPStringValue('tt_category_addition', "Adi��o de Categoria"));
    }
    
    //instala a seguran�a de ACL na p�gina
    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        gebi('category_name').focus();
      </script>
    <?
  }
}

//testa se a licen�a utilizada permite acessar o m�dulo de incidente
ISMSLib::testUserPermissionToIncidentMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_incident_category_edit.xml');

?>