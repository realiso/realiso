<?php
set_time_limit(3000);
include_once "include.php";

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$moWebLib = FWDWebLib::getInstance();
		$msLibraryExport = $moWebLib->getObject('library_export')->getValue(); //Pega a lista de classes que estava na se��o
		$maLibraryExport = FWDWebLib::unserializeString($msLibraryExport);
		
    if (!is_array($maLibraryExport)) exit;
    
		$moLibraryExport = new ISMSLibraryExport();
		foreach($maLibraryExport as $msType=>$mmIds) {
			$moLibraryExport->addXML($msType,$mmIds); //escreve cada se��o do XML
		}
		$moLibraryExport->addBestPracticeEventXML();
		$moLibraryExport->addBestPracticeStandardXML();
    $moLibraryExport->addTemplateBestPracticeXML();
    $moLibraryExport->addTemplateFileXML();
		@header('Cache-Control: ');// leave blank to avoid IE errors
		@header('Pragma: ');// leave blank to avoid IE errors
		@header('Content-type: text/enriched');
		@header('Content-Disposition: attachment; filename="library.aol"');
		echo $moLibraryExport->getEncrypt();
    $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    $moSession->setAttrLibraryExportDone(true);
	}
}
FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("export.xml");
?>