<?php

include_once 'include.php';
include_once $handlers_ref . 'select/QuerySelectStandard.php';

function getBestPracticeFromFields(){
  $moBestPractice = new RMBestPractice();

  $moBestPractice->setFieldValue('best_practice_name',ISMSLib::convertString(FWDWebLib::getObject('best_practice_name')->getValue()));
  $moBestPractice->setFieldValue('best_practice_description',ISMSLib::convertString(FWDWebLib::getObject('description')->getValue()));
  $moBestPractice->setFieldValue('best_practice_control_type',FWDWebLib::getObject('control_type')->getValue());
  $moBestPractice->setFieldValue('best_practice_classification',FWDWebLib::getObject('classification_check_controller')->getValue());
  $moBestPractice->setFieldValue('best_practice_imple_guide',FWDWebLib::getObject('pog_implementation_guidance')->getValue());
  $moBestPractice->setFieldValue('best_practice_metric',FWDWebLib::getObject('pog_metric')->getValue());
  $moBestPractice->setFieldValue('best_practice_document',FWDWebLib::getObject('document')->getValue());
  
  return $moBestPractice;
}

function save($poContext,$pbHasSensitiveChanges,$piContextId=0){
  $miBestPracticeId = $piContextId;
  
  $poContext->setFieldValue('best_practice_imple_guide',str_replace('<br>',"\n\r",$poContext->getFieldValue('best_practice_imple_guide')));
  $poContext->setFieldValue('best_practice_metric',str_replace('<br>',"\n\r",$poContext->getFieldValue('best_practice_metric')));
  
  if($miBestPracticeId){
    //teste para verificar se o sistema n�o est� sendo hakeado no evento de update
    $moCtxUserTest = new RMBestPractice();
    $moCtxUserTest->testPermissionToEdit($piContextId);
    
    $poContext->update($miBestPracticeId,true,$pbHasSensitiveChanges);
  }else{
    $miSectionBestPracticeId = FWDWebLib::getObject('section_best_practice_id')->getValue();
    $poContext->setFieldValue('section_best_practice_id',$miSectionBestPracticeId);
    $miBestPracticeId = $poContext->insert(true);
  }
  $moBestPracticeStandard = new RMBestPracticeStandard();
  
  $maInitialStandardValues = explode(':', FWDWebLib::getObject('initial_standard_ids')->getValue());
  $maCurrentStandardValues = explode(':', FWDWebLib::getObject('current_standard_ids')->getValue());
  unset($maInitialStandardValues[0], $maCurrentStandardValues[0]);
  $maInsertStandard = array_diff($maCurrentStandardValues,$maInitialStandardValues);
  $maDeleteStandard = array_diff($maInitialStandardValues,$maCurrentStandardValues);

  if(count($maInsertStandard)) $moBestPracticeStandard->insertStandards($miBestPracticeId, $maInsertStandard);
  if(count($maDeleteStandard)) $moBestPracticeStandard->deleteStandards($miBestPracticeId, $maDeleteStandard);

  echo "soWindow = soPopUpManager.getPopUpById('popup_best_practice_edit').getOpener();"
      ."if(soWindow.refresh_grid) soWindow.refresh_grid();"
      ."soPopUpManager.closePopUp('popup_best_practice_edit');";
}

class ConfirmEditEvent extends FWDRunnable {
  public function run(){
    $miBestPracticeId = FWDWebLib::getObject('best_practice_id')->getValue();
    $moBestPractice = getBestPracticeFromFields();
    save($moBestPractice,true,$miBestPracticeId);
  }
}

class SaveBestPracticeEvent extends FWDRunnable {
  public function run(){
    $miBestPracticeId = FWDWebLib::getObject('best_practice_id')->getValue();
    $moBestPractice = getBestPracticeFromFields();

    $maInitialStandardValues = explode(':', FWDWebLib::getObject('initial_standard_ids')->getValue());
    $maCurrentStandardValues = explode(':', FWDWebLib::getObject('current_standard_ids')->getValue());
    unset($maInitialStandardValues[0], $maCurrentStandardValues[0]);
    $maInsertStandard = array_diff($maCurrentStandardValues,$maInitialStandardValues);
    $maDeleteStandard = array_diff($maInitialStandardValues,$maCurrentStandardValues);
    
    $moBestPractice->setHash(FWDWebLib::getObject('hash')->getValue());
    
    if(count($maInsertStandard) || count($maDeleteStandard)){
      $mbHasSensitiveChanges = true;
    }else{
      $mbHasSensitiveChanges = $moBestPractice->hasSensitiveChanges();
    }
    $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    if($miBestPracticeId && $mbHasSensitiveChanges && $miUserId != $moBestPractice->getApprover()){
      $msTitle = FWDLanguage::getPHPStringValue('tt_reapproval','Reaprova��o');
      $msMessage = FWDLanguage::getPHPStringValue('st_sensitive_data_reapproval','Voc� est� alterando dados sens�veis, que necessitam passar por uma reaprova��o. Deseja confirmar as altera��es?');
      $msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
                      "soWindow = soPopUp.getOpener();" .
                      "soWindow.trigger_event('confirm_edit',3);";
      ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,75);
    }else{
      save($moBestPractice,$mbHasSensitiveChanges,$miBestPracticeId);
    }
  }
}

class OpenImpleGuidePopupEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $msPopUpId = 'popup_imple_guide';
    $msXmlFile = $moWebLib->getSysRef().'packages/libraries/popup_imple_guide.xml';
    $moWebLib->xml_load($msXmlFile,false);

    $moWebLib->getObject('popup_imple_guide_memo')->setValue(str_replace('<br/>','\n',FWDWebLib::convertToISO(FWDWebLib::getObject('pog_implementation_guidance')->getValue(), true)));

    $moDialog = $moWebLib->getObject('popup_imple_guide_dialog');

    // Evento do close
    $moEvent = new FWDClientEvent();
    $moEvent->setAttrEvent('onClick');
    $moEvent->setAttrValue("soPopUpManager.closePopUp('popup_imple_guide');");
    $moWebLib->getObject('popup_imple_guide_viewbutton_close')->addObjFWDEvent($moEvent);

    // Evento do save
    $psConfirmEvent = "soPopUp = soPopUpManager.getPopUpById('popup_imple_guide'); soWindow = soPopUp.getOpener(); soWindow.setInnerHTML('implementation_guidance',gebi('popup_imple_guide_memo').value);";
    $moEvent = new FWDClientEvent();
    $moEvent->setAttrEvent('onClick');
    $moEvent->setAttrValue($psConfirmEvent."soPopUpManager.closePopUp('popup_imple_guide');");
    $moWebLib->getObject('popup_imple_guide_viewbutton_save')->addObjFWDEvent($moEvent);

    $msXml = str_replace(array("\n","\r",'"'),array(' ','','\"'),$moDialog->draw());

    echo "isms_open_popup('popup_imple_guide','','','true',412,440);"
        ."soPopUpManager.getPopUpById('popup_imple_guide').setHtmlContent(\"$msXml\");";
  }
}

class OpenMetricPopupEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $msPopUpId = 'popup_metric';
    $msXmlFile = $moWebLib->getSysRef().'packages/libraries/popup_metric.xml';
    $moWebLib->xml_load($msXmlFile,false);

    $moWebLib->getObject('popup_metric_memo')->setValue(str_replace('<br/>','\n',FWDWebLib::convertToISO(FWDWebLib::getObject('pog_metric')->getValue(), true)));

    $moDialog = $moWebLib->getObject('popup_metric_dialog');

    // Evento do close
    $moEvent = new FWDClientEvent();
    $moEvent->setAttrEvent('onClick');
    $moEvent->setAttrValue("soPopUpManager.closePopUp('popup_metric');");
    $moWebLib->getObject('popup_metric_viewbutton_close')->addObjFWDEvent($moEvent);

    // Evento do save
    $psConfirmEvent = "soPopUp = soPopUpManager.getPopUpById('popup_metric'); soWindow = soPopUp.getOpener(); soWindow.setInnerHTML('metric',gebi('popup_metric_memo').value);";
    $moEvent = new FWDClientEvent();
    $moEvent->setAttrEvent('onClick');
    $moEvent->setAttrValue($psConfirmEvent."soPopUpManager.closePopUp('popup_metric');");
    $moWebLib->getObject('popup_metric_viewbutton_save')->addObjFWDEvent($moEvent);

    $msXml = str_replace(array("\n","\r",'"'),array(' ','','\"'),$moDialog->draw());

    echo "isms_open_popup('popup_metric','','','true',412,440);"
        ."soPopUpManager.getPopUpById('popup_metric').setHtmlContent(\"$msXml\");";
  }
}

class PopulateStandardsEvent extends FWDRunnable {
  public function run(){
    $msStandardsIds = FWDWebLib::getObject('current_standard_ids')->getValue();
    $maStandards = explode(':', $msStandardsIds);
    unset($maStandards[0]);
    $moHandler = new QuerySelectStandard(FWDWebLib::getConnection());
    $moHandler->setStandards($maStandards);
    $moHandler->makeQuery();
    $moHandler->executeQuery();
    $maValues = $moHandler->getValues();
    $miCount = count($maValues);
    if ($miCount){
      $msValues = serialize($maValues);
      echo ISMSLib::getJavascriptPopulateSelect('select_standard', $msValues);
    }
    else {
      echo "js_clear_select('select_standard');";
    }

  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
    $moStartEvent->addAjaxEvent(new SaveBestPracticeEvent('save_best_practice_event'));
    $moStartEvent->addAjaxEvent(new OpenImpleGuidePopupEvent('open_imple_guide_popup_event'));
    $moStartEvent->addAjaxEvent(new OpenMetricPopupEvent('open_metric_popup_event'));
    $moStartEvent->addAjaxEvent(new PopulateStandardsEvent('populate_standards_event'));
    $moStartEvent->addAjaxEvent(new ConfirmEditEvent('confirm_edit'));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $miBestPracticeId = FWDWebLib::getObject('best_practice_id')->getValue();
    $moWindowTitle = FWDWebLib::getObject('window_title');
    
    $moIcon = FWDWebLib::getObject('tooltip_icon');
    if($miBestPracticeId){
      //teste para verificar se o sistema n�o est� sendo hakeado
      $moCtxUserTest = new RMBestPractice();
      $moCtxUserTest->testPermissionToEdit($miBestPracticeId);
      
      $moBestPractice = new RMBestPractice();
      $moBestPractice->fetchById($miBestPracticeId);

      FWDWebLib::getObject('best_practice_name')->setValue($moBestPractice->getFieldValue('best_practice_name'));
      FWDWebLib::getObject('description')->setValue($moBestPractice->getFieldValue('best_practice_description'));
      FWDWebLib::getObject('control_type')->setValue($moBestPractice->getFieldValue('best_practice_control_type'));
      FWDWebLib::getObject('classification_check_controller')->setValue($moBestPractice->getFieldValue('best_practice_classification'));

      $msEscapedImplGuid = str_replace("\n",'<br/>',$moBestPractice->getFieldValue('best_practice_imple_guide'));
      FWDWebLib::getObject('implementation_guidance')->setValue($msEscapedImplGuid);
      FWDWebLib::getObject('pog_implementation_guidance')->setValue(str_replace("''", "\"", $msEscapedImplGuid));

      $msEscapedMetric = str_replace("\n",'<br/>',$moBestPractice->getFieldValue('best_practice_metric'));
      FWDWebLib::getObject('metric')->setValue($msEscapedMetric);
      FWDWebLib::getObject('pog_metric')->setValue(str_replace("''", "\"", $msEscapedMetric));

      if(ISMSLib::hasModule(POLICY_MODE)){
        FWDWebLib::getObject('document')->setShouldDraw(false);
        FWDWebLib::getObject('st_document')->setShouldDraw(false);
      }else{
        FWDWebLib::getObject('document')->setValue($moBestPractice->getFieldValue('best_practice_document'));  
      }
      
      
      /* popula o select manualmente para guardar os ids das normas */
      $moHandler = new QuerySelectStandard(FWDWebLib::getConnection());
      $moHandler->setBestPractice($miBestPracticeId);
      $moHandler->makeQuery();
      $moHandler->executeQuery();
      $maValues = $moHandler->getValues();
      $moSelect = FWDWebLib::getObject('select_standard');
      $msValues = '';
      foreach ($maValues as $maOption){
        $moItem = new FWDItem();
        $moItem->setAttrKey($maOption[0]);
        $moItem->setValue($maOption[1]);
        $moSelect->addObjFWDItem($moItem);
        unset($moItem);
        $msValues.=":$maOption[0]";
      }
      FWDWebLib::getObject('initial_standard_ids')->setValue($msValues);
      FWDWebLib::getObject('current_standard_ids')->setValue($msValues);
      /* ---------------------------------------------------------- */

      $moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_best_practice_editing', 'Edi��o de Melhor Pr�tica'));

      $moToolTip = ISMSLib::getInstance()->getToolTipCreateModifyById($miBestPracticeId);
      $moIcon->addObjFWDEvent($moToolTip);
      
      FWDWebLib::getObject('hash')->setValue($moBestPractice->getHash());
    }else{
      //teste para verificar se o sistema n�o est� sendo hakeado
      $moCtxUserTest = new RMBestPractice();
      $moCtxUserTest->testPermissionToInsert($miBestPracticeId);      
      
      if(ISMSLib::hasModule(POLICY_MODE)){
        FWDWebLib::getObject('document')->setShouldDraw(false);
        FWDWebLib::getObject('st_document')->setShouldDraw(false);
      }

      $moIcon->setAttrDisplay('false');
      $moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_best_practice_adding', 'Adi��o de Melhor Pr�tica'));
    }

    $miSectionBestPracticeId = FWDWebLib::getObject('section_best_practice_id')->getValue();
    $moSection = new RMSectionBestPractice();
    if (!$miSectionBestPracticeId) $miSectionBestPracticeId = $moBestPractice->getFieldValue('section_best_practice_id');
    $moSection->fetchById($miSectionBestPracticeId);
    FWDWebLib::getObject('section')->setValue($moSection->getFieldValue('section_name'));
  ?>
  <script language="javascript">
    function setInnerHTML(psElement,psValue){
      psValue = psValue.replace(/\n/g,'<br/>');
      gobi(psElement).setValue(psValue);
      gebi('pog_'+psElement).value = psValue;
    }

    function set_standards(paStandards){
      if (paStandards.length)  gebi('current_standard_ids').value = ':' + paStandards.join(':');
      else gebi('current_standard_ids').value = '';
      trigger_event('populate_standards_event', 3);
    }
  </script>
  <?
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        gebi('best_practice_name').focus();
      </script>
    <?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_best_practice_edit.xml');

?>