<?php
include_once 'include.php';
include_once $handlers_ref . "QueryEventFromBestPractice.php";
include_once $handlers_ref . 'grid/QueryGridEventSearch.php';
include_once $handlers_ref . "QueryContextHierarchicalStructure.php";

class GridEventSearch extends FWDDrawGrid {
  public function drawItem(){
    switch($this->ciColumnIndex){
      case 1:
        $msGfxRef = FWDWebLib::getInstance()->getGfxRef();
        $moIcon = new FWDIcon(new FWDBox(4,4));
        $moIcon->setAttrSrc("{$msGfxRef}icon-event.gif");
        return $moIcon->draw();
        break;
      
      case 2:
        $this->coCellBox->setAttrStringNoEscape("true");
        $this->coCellBox->setValue("<a href='javascript:open_visualize(".$this->caData[1].",".CONTEXT_EVENT.",\"".uniqid()."\")'>{$this->caData[2]}</a>");
        return $this->coCellBox->draw();
        break;
      
      case 4:
        $this->coCellBox->setAttrStringNoEscape("true");
        $this->coCellBox->setValue("<a href='javascript:open_visualize(".$this->caData[5].",".CONTEXT_CATEGORY.",\"".uniqid()."\")'>{$this->caData[4]}</a>");
        return $this->coCellBox->draw();
        break;
      
      default:
        return parent::drawItem();
        break;
    }
  }
}

class AssociateEvent extends FWDRunnable {
  public function run(){
    //teste para verificar se o sistema n�o est� sendo hakeado no evento de update
    $moCtxUserTest = new RMBestPracticeEvent();
    $moCtxUserTest->testPermissionToInsert();
    
    $moWebLib = FWDWebLib::getInstance();
    $miBestPracticeId = $moWebLib->getObject('best_practice_id')->getValue();
    $msNewEvents = $moWebLib->getObject('current_events_ids')->getValue();
    if($msNewEvents){
      $maNewEvents = explode(':',$msNewEvents);
    }else{
      $maNewEvents = array();
    }
    
    $moQuery = new QueryEventFromBestPractice(FWDWebLib::getConnection());
    $moQuery->setBestPracticeId($miBestPracticeId);
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    $maOldEvents = $moQuery->getEvents();
    
    $maToInsert = array_diff($maNewEvents,$maOldEvents);
    $maToDelete = array_diff($maOldEvents,$maNewEvents);
    $moBestPracticeEvent = new RMBestPracticeEvent();
    if(count($maToInsert)) $moBestPracticeEvent->insertEvents($miBestPracticeId,$maToInsert);
    if(count($maToDelete)) $moBestPracticeEvent->deleteEvents($miBestPracticeId,$maToDelete);
    
    echo "soPopUpManager.closePopUp('popup_event_add');";
  }
}

class SearchEventsEvent extends FWDRunnable {
  public function run(){
    $moGrid = FWDWebLib::getObject('grid_event_search');
    $moGrid->execEventPopulate();
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
    $moStartEvent->addAjaxEvent(new SearchEventsEvent('search_events_event'));
    $moStartEvent->addAjaxEvent(new AssociateEvent('associate_event'));
    
    $moSearchGrid = $moWebLib->getObject('grid_event_search');
    $moSearchGrid->setObjFwdDrawGrid(new GridEventSearch());
    $moSearchHandler = new QueryGridEventSearch($moWebLib->getConnection());
    
    $moCurrentGrid = $moWebLib->getObject('grid_current_events');
    $moCurrentGrid->setObjFwdDrawGrid(new GridEventSearch());
    $moCurrentHandler = new QueryGridEventSearch($moWebLib->getConnection());
    
    $moSearchHandler->setDescription($moWebLib->getObject('description_filter')->getValue());
    $moSearchHandler->setCategory($moWebLib->getObject('category_filter')->getValue());
    
    $miBestPracticeId = $moWebLib->getObject('best_practice')->getValue();
    if($miBestPracticeId){
      $moWebLib->getObject('best_practice_id')->setValue($miBestPracticeId);
    }else{
      $miBestPracticeId = $moWebLib->getObject('best_practice_id')->getValue();
    }
    
    $msCurrentIds = $moWebLib->getObject('current_events_ids')->getValue();
    if($msCurrentIds){
      $maCurrentIds = explode(':',$msCurrentIds);
      $moSearchHandler->dontShowEvents($maCurrentIds);
      $moCurrentHandler->setEvents($maCurrentIds);
    }
    else {
      $moCurrentHandler->setEmpty(true);
    }
    
    $moSearchGrid->setQueryHandler($moSearchHandler);
    $moCurrentGrid->setQueryHandler($moCurrentHandler);
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    //teste para verificar se o sistema n�o est� sendo hakeado
    $moCtxUserTest = new RMEvent();
    $moCtxUserTest->testPermissionToInsert();

    $moSelect = FWDWebLib::getObject('event_category_id');
    $moQuery = new QueryContextHierarchicalStructure(FWDWebLib::getConnection());
    $moQuery->setContextType(CONTEXT_CATEGORY);
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    foreach ($moQuery->getContexts() as $maContext) {
      list($miContextId,$msHierarchicalName) = $maContext;
      $moSelect->setItemValue($miContextId,$msHierarchicalName);
    }
    
    $moQuery = new QueryEventFromBestPractice(FWDWebLib::getConnection());
    $moQuery->setBestPracticeId(FWDWebLib::getObject('best_practice')->getValue());
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    FWDWebLib::getObject('current_events_ids')->setValue(implode(":",$moQuery->getEvents()));
    
    $moWebLib->dump_html($moWebLib->getObject('dialog'));
?>
<script language="javascript">
  gebi('event_description').focus();
  
  function refresh_grid() {    
    gobi('grid_current_events').setPopulate(true);
    gobi('grid_current_events').refresh();
    //gobi('grid_event_search').setPopulate(true);
    //gobi('grid_event_search').refresh();
  }
  function enter_event_search_event(e)
	{
		if (!e) e = window.event;
		if(e['keyCode']==13) {
			  gebi('description_filter').value = gebi('event_description').value;
        gebi('category_filter').value = gebi('event_category_id').value;
        gebi('current_events_ids').value = gobi('grid_current_events').getAllIds().join(':');
        gobi('grid_event_search').setPopulate(true);
	      trigger_event("search_events_event","3");
		}
	}
	FWDEventManager.addEvent(gebi('event_description'), 'keydown', enter_event_search_event);
  refresh_grid();
</script>
<?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_event_add.xml');

?>