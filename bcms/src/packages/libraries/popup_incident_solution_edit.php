<?php

include_once "include.php";

class SaveEvent extends FWDRunnable {
  public function run() {
    $miSolutionId = FWDWebLib::getObject('solution_id')->getValue();
    $moSolution = new CISolution();
    $moSolution->fillFieldsFromForm();
    $moSolution->insertOrUpdate($miSolutionId); //ja executa os testes de permiss�o de edi��o e inser��o
    
    echo "soWindow = soPopUpManager.getPopUpById('popup_incident_solution_edit').getOpener();;
          if (soWindow.refresh_grid) soWindow.refresh_grid();
          soPopUpManager.closePopUp('popup_incident_solution_edit');";
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
    $moStartEvent->addAjaxEvent(new SaveEvent('save_event'));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $miSolutionId = FWDWebLib::getObject('solution_id')->getValue();
    $moCtxUserTest = new CISolution();
    if ($miSolutionId) {
      $moCtxUserTest->testPermissionToEdit($miSolutionId);
      $moSolution = new CISolution();
      $moSolution->fetchById($miSolutionId);
      $moSolution->fillFormFromFields();
      $miCategoryId = $moSolution->getFieldValue('category_id');
    } else {
      $moCtxUserTest->testPermissionToInsert();
    }
    
    if (!isset($miCategoryId)) $miCategoryId = FWDWebLib::getObject('param_category_id')->getValue();
    if ($miCategoryId) {
      FWDWebLib::getObject('category_id')->setValue($miCategoryId);
      $moCategory = new CICategory();
      $moCategory->fetchById($miCategoryId);
      FWDWebLib::getObject('category')->setValue($moCategory->getFieldValue('category_name'));
    }
        
    
    //instala a seguran�a de ACL na p�gina
    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        gebi('solution_problem').focus();
      </script>
    <?
  }
}

//testa se a licen�a utilizada permite acessar o m�dulo de incidente
ISMSLib::testUserPermissionToIncidentMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_incident_solution_edit.xml');

?>