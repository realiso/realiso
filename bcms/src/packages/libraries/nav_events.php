<?php
include_once "include.php";
include_once $handlers_ref . "grid/QueryGridEvents.php";
set_time_limit(3000);
class GridEvents extends FWDDrawGrid {
  public function drawItem(){    
    switch($this->ciColumnIndex){    
      case $this->getIndexByAlias('context_id'):
        $msIconRef = '';
        if($this->getFieldValue('context_type') == CONTEXT_CATEGORY){
          $msIconRef="icon-category.gif";
          $maNotAllowed = array("M.L.1.4","M.L.1.5","M.L.1.6","M.L.1.8","M.L.1.9");
        }else{
          $msIconRef="icon-event.gif";
          $maNotAllowed = array("M.L.1.1","M.L.1.2","M.L.1.3","M.L.1.7");
        }
        
        $moACL = FWDACLSecurity::getInstance();
        $moACL->setNotAllowed($maNotAllowed);
        $moMenu = FWDWebLib::getObject('menu');
        $moGrid = FWDWebLib::getObject('grid_events');
        $msLine = $moGrid->getAttrName().'_acl_'.$this->ciRowIndex;
        FWDGridMenuACLSecurity::getInstance()->installGridMenuACL($moMenu,$moACL,$moGrid,$msLine);
        
        $this->coCellBox->setIconSrc($msIconRef);
        $this->coCellBox->setObjFWDToolTip(ISMSLib::getInstance()->getToolTipCreateModify($this->getFieldValue('user_create_name'), $this->getFieldValue('date_create'), $this->getFieldValue('user_edit_name'), $this->getFieldValue('date_edit')) );
        return parent::drawItem();
      break;
      case $this->getIndexByAlias('context_name'):
        $this->coCellBox->setAttrStringNoEscape("true");
        if($this->getFieldValue('context_type') == CONTEXT_CATEGORY){
          if(ISMSLib::userHasACL('M.L.1.1')){
            $this->coCellBox->setValue("<a href='javascript:enter_category(".$this->getFieldValue('context_id').");'>".$this->getFieldValue('context_name')."</a>");
          }else{
            $this->coCellBox->setValue("<a href='javascript:open_visualize(".$this->getFieldValue('context_id').",".$this->getFieldValue('context_type').",\"".uniqid()."\");'>".$this->getFieldValue('context_name')."</a>");
          }
        }else{
          if(ISMSLib::userHasACL('M.L.1.5')){
            $this->coCellBox->setValue("<a href='javascript:open_event_edit(".$this->getFieldValue('context_id').");'>".$this->getFieldValue('context_name')."</a>");
          }else{
            $this->coCellBox->setValue("<a href='javascript:open_visualize(".$this->getFieldValue('context_id').",".$this->getFieldValue('context_type').",\"".uniqid()."\");'>".$this->getFieldValue('context_name')."</a>");
          }
        }
        return $this->coCellBox->draw();
        break;
       case $this->getIndexByAlias('date_create'):
        $this->coCellBox->setValue(ISMSLib::getISMSShortDate($this->coCellBox->getValue()));
        return $this->coCellBox->draw();
       break;
      default:
        return parent::drawItem();
      break;
    }
  }
}

class CategoryConfirmRemove extends FWDRunnable {
  public function run(){
  	$miCategoryId = FWDWebLib::getObject('selected_context_id')->getValue();  	
  	
  	$moCategory = new RMCategory();
  	$moCategory->fetchById($miCategoryId);
  	$msCategoryName = ISMSLib::truncateString($moCategory->getFieldValue('category_name'), 70);
  	
    $msTitle = FWDLanguage::getPHPStringValue('tt_remove_category','Remover Categoria');
    $msMessage = FWDLanguage::getPHPStringValue('st_remove_category_confirm',"Voc� tem certeza de que deseja remover a categoria <b>%category_name%</b>?");
    $msMessage = str_replace('%category_name%', $msCategoryName, $msMessage);
    
    if (ISMSLib::getConfigById(GENERAL_CASCADE_ON)) {
      $msMessage .= " ".FWDLanguage::getPHPStringValue('st_remove_category_cascade_message',"Voc� estar� deletando tamb�m qualquer Ativo relacionado � Categoria, bem como seus Riscos e associa��o dos mesmos com outros elementos do sistema."); 
    }
    
    $msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
    				"soWindow = soPopUp.getOpener();" .
    				"soWindow.remove_category();";
			  		
    ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,80);
  }
}

class EventConfirmRemove extends FWDRunnable {
  public function run(){
  	$miEventId = FWDWebLib::getObject('selected_context_id')->getValue();  	
  	
  	$moEvent = new RMEvent();
  	$moEvent->fetchById($miEventId);
  	$msEventName = ISMSLib::truncateString($moEvent->getFieldValue('event_description'), 70);
  	
    $miPopupSize = 50;
    if (strlen($msEventName) > 100) {
      $miPopupSize = 100;
    }
    if (strlen($msEventName) > 200) {
      $miPopupSize = 150;
    }
    
    $msTitle = FWDLanguage::getPHPStringValue('tt_remove_event','Remover Evento');
    $msMessage = FWDLanguage::getPHPStringValue('st_remove_event_confirm',"Voc� tem certeza de que deseja remover o evento <b>%event_name%</b>");
    $msMessage = str_replace('%event_name%', $msEventName, $msMessage);
           
    $msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
    				"soWindow = soPopUp.getOpener();" .
    				"soWindow.remove_event();";
			  		
    ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,$miPopupSize);
  }
}

class RemoveCategoryEvent extends FWDRunnable {
  public function run(){  	
  	$miCategoryId = FWDWebLib::getObject('selected_context_id')->getValue();  	
  	$moCategory = new RMCategory();
  	$moCategory->delete($miCategoryId);
  }
}

class RemoveEventEvent extends FWDRunnable {
  public function run(){  	
  	$miEventId = FWDWebLib::getObject('selected_context_id')->getValue();  	
  	$moEvent = new RMEvent();
  	$moEvent->delete($miEventId);
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));  	
    $moStartEvent->addAjaxEvent(new CategoryConfirmRemove('category_confirm_remove'));
    $moStartEvent->addAjaxEvent(new RemoveCategoryEvent('remove_category_event'));
    $moStartEvent->addAjaxEvent(new EventConfirmRemove('event_confirm_remove'));
    $moStartEvent->addAjaxEvent(new RemoveEventEvent('remove_event_event'));
    $moStartEvent->addAjaxEvent(new ChangeScrollingPath('change_scrolling_path'));
    //instala a seguran�a de ACL na p�gina
		FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
		FWDMenuACLSecurity::getInstance()->installMenuACL(FWDWebLib::getObject('menu'),FWDACLSecurity::getInstance());
    
    $moGrid = FWDWebLib::getObject("grid_events");
    $moHandler = new QueryGridEvents(FWDWebLib::getConnection());
    
    if (!FWDWebLib::getObject("root_category_id")->getValue())
    	FWDWebLib::getObject("root_category_id")->setValue(FWDWebLib::getObject("category_id")->getValue());    
    elseif(FWDWebLib::getObject("par_scrolling_filter")->getValue()){
    	FWDWebLib::getObject("root_category_id")->setValue(FWDWebLib::getObject("par_scrolling_filter")->getValue());
    }    
    $moHandler->setRootCategory(FWDWebLib::getObject("root_category_id")->getValue());    	
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new GridEvents());    
  }
}

class ChangeScrollingPath extends FWDRunnable{
	public function run(){
		echo ISMSLib::getPathScrollCode(LIBRARIES_MODE,CONTEXT_CATEGORY,FWDWebLib::getObject('root_category_id')->getValue(),true,"enter_category");
	}
}

class ScreenEvent extends FWDRunnable {
  public function run() {
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));    
	?>		
		<script language="javascript">			
		function refresh_grid() {
			js_refresh_grid('grid_events');		
		}
		
		function enter_category(category_id) {									
			gebi('root_category_id').value = category_id;
			if (category_id > 0) {
				js_show('button_associate_event');
				js_show('button_insert_event');
			}
			else {
				js_hide('button_associate_event');
				js_hide('button_insert_event');
			}			
				
			refresh_grid();						
			trigger_event('change_scrolling_path',3);
		}
		
		function remove_category() {
			trigger_event('remove_category_event', 3);
			refresh_grid();				
		}
		
		function remove_event() {
			trigger_event('remove_event_event', 3);
			refresh_grid();				
		}
		
		if (gebi('root_category_id').value) {
			js_show('button_associate_event');
			js_show('button_insert_event');	
		}
    
    function open_event_edit(piId){
      isms_open_popup('popup_event_edit','packages/libraries/popup_event_edit.php?event='+piId,'','true');
    }
		</script>
	<? 
	 ISMSLib::getPathScrollCode(LIBRARIES_MODE,CONTEXT_CATEGORY,FWDWebLib::getObject('root_category_id')->getValue(),false,'enter_category');          
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_events.xml");
?>