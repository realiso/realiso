<?php
include_once "include.php";

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){

    $moConfig = new ISMSConfig();
    if($moConfig->getConfigFromDB(CLIENT_ID_ASAAS)) {
        $moISMSASaaS = new ISMSASaaS();
        $moISMSASaaS->setNewCheckPointASaaS(utf8_encode("Acessou M�dulo Bibliotecas"), utf8_encode("M�dulo Bibliotecas"));
    }
    $miContextType = FWDWebLib::getObject('context_type')->getValue();
    $miContextId = FWDWebLib::getObject('context_id')->getValue();
    if ($miContextType) {
      switch ($miContextType) {
        case CONTEXT_EVENT: case CONTEXT_CATEGORY:
          FWDWebLib::getObject('tab_group')->setAttrLoadTabItemIndex(1);
          $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
          $moSession->addAttribute('categoryid');  
          $moSession->setAttrCategoryId($miContextId);
        break;
        case CONTEXT_BEST_PRACTICE: 
        case CONTEXT_SECTION_BEST_PRACTICE:
          FWDWebLib::getObject('tab_group')->setAttrLoadTabItemIndex(2);
          $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
          $moSession->addAttribute('sectionid');  
          $moSession->setAttrSectionId($miContextId);
        break;
        case CONTEXT_STANDARD:
          FWDWebLib::getObject('tab_group')->setAttrLoadTabItemIndex(3);
        break;
        case CONTEXT_DOCUMENT_TEMPLATE:
        case CONTEXT_DOCUMENT_TEMPLATE_WITH_CONTENT:
          if(FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(POLICY_MODE)){
            FWDWebLib::getObject('tab_group')->setAttrLoadTabItemIndex(5);
          }
        break;
        case CONTEXT_CI_CATEGORY:
            FWDWebLib::getObject('tab_group')->setAttrLoadTabItemIndex(6);
            $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
        break;
        case CONTEXT_CI_SOLUTION:
          if( ISMSLib::hasModule(INCIDENT_MODE) || ISMSLib::userHasACL('M.CI') || ISMSLib::userHasACL('M.L.6') ){
            FWDWebLib::getObject('tab_group')->setAttrLoadTabItemIndex(6);
            $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
            $moSession->deleteAttribute('categoryid');
            $moSession->addAttribute('categoryid');
            $moSession->setAttrCategoryId($miContextId);
          }
        break;
      }    
    }
    
    if( (!ISMSLib::hasModule(INCIDENT_MODE) && !ISMSLib::userHasACL('M.CI') ) || !ISMSLib::userHasACL('M.L.6') ){
      FWDWebLib::getObject('tab_incident_category')->setShouldDraw(false);
    }
    
    if(!FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(POLICY_MODE)){
      FWDWebLib::getObject('tab_document_template')->setShouldDraw(false);
    }
    //instala a seguran�a de ACL na p�gina
    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
    if(in_array(ISMSSaaS::getConfig(ISMSSaaS::LICENSE_TYPE), ISMSLicense::getTrialLicenseTypes())){
      FWDWebLib::getObject('nav_import_export')->setShouldDraw(false);
    }
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("tab_libraries.xml");
?>