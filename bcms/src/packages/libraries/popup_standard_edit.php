<?php

include_once 'include.php';
include_once $handlers_ref . 'QueryTotalStandards.php';

function getStandardFromFields(){
  $moStandard = new RMStandard();

  $msStandardName = FWDWebLib::getObject('standard_name')->getValue();
  $msStandardDescription = FWDWebLib::getObject('standard_description')->getValue();
  $msStandardApplication = FWDWebLib::getObject('standard_application')->getValue();
  $msStandardObjective = FWDWebLib::getObject('standard_objective')->getValue();

  $moStandard->setFieldValue('standard_name', $msStandardName);
  $moStandard->setFieldValue('standard_description', $msStandardDescription);
  $moStandard->setFieldValue('standard_application', $msStandardApplication);
  $moStandard->setFieldValue('standard_objective', $msStandardObjective);
  
  return $moStandard;
}

function save($poContext,$pbHasSensitiveChanges,$piContextId=0){
  if($piContextId){
    //teste para verificar se o sistema n�o est� sendo hakeado no evento de update
    $moCtxUserTest = new RMStandard();
    $moCtxUserTest->testPermissionToEdit($piContextId);
    
    $poContext->update($piContextId,true,$pbHasSensitiveChanges);
  }else{
    $poContext->insert();
  }
  echo "soWindow = soPopUpManager.getPopUpById('popup_standard_edit').getOpener();"
      ."if(soWindow.refresh_grid) soWindow.refresh_grid();"
      ."soPopUpManager.closePopUp('popup_standard_edit');";
}

class ConfirmEditEvent extends FWDRunnable {
  public function run(){
    $miStandardId = FWDWebLib::getObject('standard_id')->getValue();
    $moStandard = getStandardFromFields();
    save($moStandard,true,$miStandardId);
  }
}

class SaveStandardEvent extends FWDRunnable {
  public function run(){
    $miStandardId = FWDWebLib::getObject('standard_id')->getValue();
    $moStandard = getStandardFromFields();
    $moStandard->setHash(FWDWebLib::getObject('hash')->getValue());
    
    $mbHasSensitiveChanges = $moStandard->hasSensitiveChanges();
    $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    if($miStandardId && $mbHasSensitiveChanges && $miUserId != $moStandard->getApprover()){
      $msTitle = FWDLanguage::getPHPStringValue('tt_reapproval','Reaprova��o');
      $msMessage = FWDLanguage::getPHPStringValue('st_sensitive_data_reapproval','Voc� est� alterando dados sens�veis, que necessitam passar por uma reaprova��o. Deseja confirmar as altera��es?');
      $msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
                      "soWindow = soPopUp.getOpener();" .
                      "soWindow.trigger_event('confirm_edit',3);";
      ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,75);
    }else{
      save($moStandard,$mbHasSensitiveChanges,$miStandardId);
    }
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
    $moStartEvent->addAjaxEvent(new SaveStandardEvent('save_standard_event'));
    $moStartEvent->addAjaxEvent(new ConfirmEditEvent('confirm_edit'));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $miStandardId = FWDWebLib::getObject('standard_id')->getValue();
    $moWindowTitle = FWDWebLib::getObject('window_title');
    $moIcon = FWDWebLib::getObject('tooltip_icon');
    if($miStandardId){
      //teste para verificar se o sistema n�o est� sendo hakeado
      $moCtxUserTest = new RMStandard();
      $moCtxUserTest->testPermissionToEdit($miStandardId);
      
      $moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_standard_editing', 'Edi��o de Norma'));
      $moStandard = new RMStandard();
      $moStandard->fetchById($miStandardId);

      FWDWebLib::getObject('standard_name')->setValue($moStandard->getFieldValue('standard_name'));
      FWDWebLib::getObject('standard_description')->setValue($moStandard->getFieldValue('standard_description'));
      FWDWebLib::getObject('standard_application')->setValue($moStandard->getFieldValue('standard_application'));
      FWDWebLib::getObject('standard_objective')->setValue($moStandard->getFieldValue('standard_objective'));
      $moToolTip = ISMSLib::getInstance()->getToolTipCreateModifyById($miStandardId);
      $moToolTip->setAttrWidth(385);
      $moIcon->addObjFWDEvent($moToolTip);
      
      FWDWebLib::getObject('hash')->setValue($moStandard->getHash());
    }else{
      //teste para verificar se o sistema n�o est� sendo hakeado
      $moCtxUserTest = new RMStandard();
      $moCtxUserTest->testPermissionToInsert();

      $moIcon->setAttrDisplay('false');
      $moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_standard_adding', 'Adi��o de Norma'));
    }

    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        gebi('standard_name').focus();
      </script>
    <?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_standard_edit.xml');

?>