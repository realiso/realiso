<?php
include_once "include.php";
include_once $handlers_ref . "grid/QueryGridBestPractice.php";

class SectionConfirmRemove extends FWDRunnable {
  public function run(){
    $miSectionId = FWDWebLib::getObject('section_id')->getValue();
    
    $moSection = new RMSectionBestPractice();
    $moSection->fetchById($miSectionId);
    $msSectionName = ISMSLib::truncateString($moSection->getFieldValue('section_name'), 70);
    
    $msTitle = FWDLanguage::getPHPStringValue('tt_remove_section','Remover Se��o');
    $msMessage = FWDLanguage::getPHPStringValue('st_remove_section_confirm',"Voc� tem certeza de que deseja remover a se��o <b>%section_name%</b>?");
    $msMessage = str_replace('%section_name%', $msSectionName, $msMessage);
    
    $msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
            "soWindow = soPopUp.getOpener();" .
            "soWindow.remove_section();";
    
    ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,50);
  }
}

class RemoveSectionEvent extends FWDRunnable {
  public function run(){
    $miSectionId = FWDWebLib::getObject('section_id')->getValue();
    $moSection = new RMSectionBestPractice();
    $moSection->delete($miSectionId);
  }
}

class BestPracticeConfirmRemove extends FWDRunnable {
  public function run(){
    $miBestPracticeId = FWDWebLib::getObject('best_practice_id')->getValue();
    
    $moBestPractice = new RMBestPractice();
    $moBestPractice->fetchById($miBestPracticeId);
    $msBestPracticeName = ISMSLib::truncateString($moBestPractice->getFieldValue('best_practice_name'), 70);
    
    $msTitle = FWDLanguage::getPHPStringValue('tt_remove_best_practice','Remover Melhor Pr�tica');
    $msMessage = FWDLanguage::getPHPStringValue('st_remove_best_practice_confirm',"Voc� tem certeza de que deseja remover a melhor pr�tica <b>%best_practice_name%</b>?");
    $msMessage = str_replace('%best_practice_name%', $msBestPracticeName, $msMessage);
    
    $msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
            "soWindow = soPopUp.getOpener();" .
            "soWindow.remove_best_practice();";
    
    ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,50);
  }
}

class RemoveBestPracticeEvent extends FWDRunnable {
  public function run(){
    $miBestPracticeId = FWDWebLib::getObject('best_practice_id')->getValue();
    $moBestPractice = new RMBestPractice();
    $moBestPractice->delete($miBestPracticeId);
  }
}

class GridBestPractice extends FWDDrawGrid {
  public function drawItem(){
    switch($this->ciColumnIndex){
      case $this->getIndexByAlias('context_id'):
        $msIconSrc='';
        if($this->getFieldValue('context_type') == CONTEXT_SECTION_BEST_PRACTICE){
          $msGfxRef='icon-section.gif';
          $maNotAllowed = array("M.L.2.4","M.L.2.5","M.L.2.6","M.L.2.8","M.L.2.9","M.L.2.10");
        }else{
          $msGfxRef = 'icon-best_practice.gif';
          $maNotAllowed = array("M.L.2.1","M.L.2.2","M.L.2.3","M.L.2.7");
        }
        
        $moACL = FWDACLSecurity::getInstance();
        $moACL->setNotAllowed($maNotAllowed);
        $moMenu = FWDWebLib::getObject('menu');
        $moGrid = FWDWebLib::getObject('grid_best_practice');
        $msLine = $moGrid->getAttrName().'_acl_'.$this->ciRowIndex;
        FWDGridMenuACLSecurity::getInstance()->installGridMenuACL($moMenu,$moACL,$moGrid,$msLine);
        
        $this->coCellBox->setIconSrc($msGfxRef);
        $this->coCellBox->setObjFWDToolTip(ISMSLib::getInstance()->getToolTipCreateModify($this->getFieldValue('user_create_name'),$this->getFieldValue('date_create'),$this->getFieldValue('user_edit_name'),$this->getFieldValue('date_edit')) );
        return parent::drawItem();
      break;
      
      case $this->getIndexByAlias('context_name'):
        $this->coCellBox->setAttrStringNoEscape("true");
        if($this->getFieldValue('context_type') == CONTEXT_SECTION_BEST_PRACTICE){
          if(ISMSLib::userHasACL('M.L.2.2')){
            $this->coCellBox->setValue("<a href='javascript:enter_section(".$this->getFieldValue('context_id').");'>".$this->getFieldValue('context_name')."</a>");
          }else{
            $this->coCellBox->setValue("<a href='javascript:open_visualize(".$this->getFieldValue('context_id').",".$this->getFieldValue('context_type').",\"".uniqid()."\");'>".$this->getFieldValue('context_name')."</a>");
          }
        }else{
          if(ISMSLib::userHasACL('M.L.2.5')){
            $this->coCellBox->setValue("<a href='javascript:open_bp_edit(".$this->getFieldValue('context_id').");'>".$this->getFieldValue('context_name')."</a>");
          }else{
            $this->coCellBox->setValue("<a href='javascript:open_visualize(".$this->getFieldValue('context_id').",".$this->getFieldValue('context_type').",\"".uniqid()."\");'>".$this->getFieldValue('context_name')."</a>");
          }
        }
        return $this->coCellBox->draw();
      break;
       case $this->getIndexByAlias('date_create'):
       	$this->coCellBox->setValue(ISMSLib::getISMSShortDate($this->caData[5]));
       	return $this->coCellBox->draw();
       break;
      default:
        return parent::drawItem();
      break;
    }
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new SectionConfirmRemove('section_confirm_remove'));
    $moStartEvent->addAjaxEvent(new RemoveSectionEvent('remove_section_event'));
    $moStartEvent->addAjaxEvent(new BestPracticeConfirmRemove('best_practice_confirm_remove'));
    $moStartEvent->addAjaxEvent(new RemoveBestPracticeEvent('remove_best_practice_event'));
    $moStartEvent->addAjaxEvent(new ChangeScrollingPath('change_scrolling_path'));
    //instala a seguran�a de ACL na p�gina
		FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
		FWDMenuACLSecurity::getInstance()->installMenuACL(FWDWebLib::getObject('menu'),FWDACLSecurity::getInstance());
    
    $moGrid = FWDWebLib::getObject("grid_best_practice");
    $moHandler = new QueryGridBestPractice(FWDWebLib::getConnection());
    
    if (!FWDWebLib::getObject("root_section_id")->getValue())
    	FWDWebLib::getObject("root_section_id")->setValue(FWDWebLib::getObject("session_section_id")->getValue());
    elseif(FWDWebLib::getObject("par_scrolling_filter")->getValue()){
    	FWDWebLib::getObject("root_section_id")->setValue(FWDWebLib::getObject("par_scrolling_filter")->getValue());
    }    
    
    $moHandler->setRootSection(FWDWebLib::getObject("root_section_id")->getValue());    	
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new GridBestPractice());    
  }
}

class ChangeScrollingPath extends FWDRunnable{
	public function run(){
		echo ISMSLib::getPathScrollCode(LIBRARIES_MODE,CONTEXT_SECTION_BEST_PRACTICE,FWDWebLib::getObject('root_section_id')->getValue(),true,"enter_section");
	}
}

class ScreenEvent extends FWDRunnable {
  public function run(){        
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
			<script language="javascript">
			function refresh_grid() {
				js_refresh_grid('grid_best_practice');		
			}
			
			function enter_section(section_id) {
				gebi('root_section_id').value = section_id;
				if (section_id > 0) js_show('button_insert_best_practice');
				else js_hide('button_insert_best_practice');
				refresh_grid();
				trigger_event('change_scrolling_path',3);					
			}
	    
		    function remove_section() {
		      trigger_event('remove_section_event', 3);
		      refresh_grid();
		    }
		    
		    function remove_best_practice() {
		      trigger_event('remove_best_practice_event', 3);
		      refresh_grid();
		    }
		    
        function open_bp_edit(piId){
          isms_open_popup('popup_best_practice_edit','packages/libraries/popup_best_practice_edit.php?best_practice='+piId,'','true');
        }
        
		    if (gebi('root_section_id').value) {
				js_show('button_insert_best_practice');	
			}
			</script>
		<?
		 ISMSLib::getPathScrollCode(LIBRARIES_MODE,CONTEXT_SECTION_BEST_PRACTICE,FWDWebLib::getObject('root_section_id')->getValue(),false,'enter_section');          
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_best_practices.xml");
?>