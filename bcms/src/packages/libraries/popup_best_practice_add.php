<?php
include_once 'include.php';
include_once $handlers_ref . "QueryBestPracticeFromEvent.php";
include_once $handlers_ref . 'grid/QueryGridBestPracticeSearch.php';
include_once $handlers_ref . "QueryContextHierarchicalStructure.php";

class GridBestPracticesSearch extends FWDDrawGrid {
  public function DrawItem(){
    switch($this->ciColumnIndex){
      case 1:
        $msGfxRef = FWDWebLib::getInstance()->getGfxRef();
        $moIcon = new FWDIcon(new FWDBox(4,4));
        $moIcon->setAttrSrc("{$msGfxRef}icon-best_practice.gif");
        return $moIcon->draw();
        break;
      
      case 2:
        $this->coCellBox->setAttrStringNoEscape("true");
        $this->coCellBox->setValue("<a href='javascript:open_visualize(".$this->caData[1].",".CONTEXT_BEST_PRACTICE.",\"".uniqid()."\")'>{$this->caData[2]}</a>");
        return $this->coCellBox->draw();
        break;
      
      default:
        return parent::drawItem();
        break;
    }
  }
}

class AssociateEvent extends FWDRunnable {
  public function run(){
    //teste para verificar se o sistema n�o est� sendo hakeado
    $moCtxUserTest = new RMBestPracticeEvent();
    $moCtxUserTest->testPermissionToInsert();
    
    $moWebLib = FWDWebLib::getInstance();
    $miEventId = $moWebLib->getObject('event_id')->getValue();
    $msNewBestPractices = $moWebLib->getObject('current_best_practices_ids')->getValue();
    if($msNewBestPractices){
      $maNewBestPractices = explode(':',$msNewBestPractices);
    }else{
      $maNewBestPractices = array();
    }
    
    $moQuery = new QueryBestPracticeFromEvent(FWDWebLib::getConnection());
    $moQuery->setEventId($miEventId);
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    $maOldBestPractices = $moQuery->getBestPractices();
    
    $maToInsert = array_diff($maNewBestPractices,$maOldBestPractices);
    $maToDelete = array_diff($maOldBestPractices,$maNewBestPractices);
    $moBestPracticeEvent = new RMBestPracticeEvent();
    if(count($maToInsert)) $moBestPracticeEvent->insertBestPractices($miEventId,$maToInsert);
    if(count($maToDelete)) $moBestPracticeEvent->deleteBestPractices($miEventId,$maToDelete);
    
    echo "soPopUpManager.closePopUp('popup_best_practice_add');";
  }
}

class SearchBestPracticesEvent extends FWDRunnable {
  public function run(){
    $moGrid = FWDWebLib::getObject('grid_best_practice_search');
    $moGrid->execEventPopulate();
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
    $moStartEvent->addAjaxEvent(new SearchBestPracticesEvent('search_best_practices_event'));
    $moStartEvent->addAjaxEvent(new AssociateEvent('associate_event'));
    
    $moSearchGrid = $moWebLib->getObject('grid_best_practice_search');
    $moSearchGrid->setObjFwdDrawGrid(new GridBestPracticesSearch());
    $moSearchHandler = new QueryGridBestPracticeSearch($moWebLib->getConnection());
    
    $moCurrentGrid = $moWebLib->getObject('grid_current_best_practices');
    $moCurrentGrid->setObjFwdDrawGrid(new GridBestPracticesSearch());
    $moCurrentHandler = new QueryGridBestPracticeSearch($moWebLib->getConnection());
    
    $moSearchHandler->setName($moWebLib->getObject('name_filter')->getValue());
    $moSearchHandler->setSectionID($moWebLib->getObject('section_filter')->getValue());
    
    $miEventId = $moWebLib->getObject('event')->getValue();
    if($miEventId){
      $moWebLib->getObject('event_id')->setValue($miEventId);
    }else{
      $miEventId = $moWebLib->getObject('event_id')->getValue();
    }
    
    $msCurrentIds = $moWebLib->getObject('current_best_practices_ids')->getValue();
    if($msCurrentIds){
      $maCurrentIds = explode(':',$msCurrentIds);
      $moSearchHandler->setExcludedIds($maCurrentIds);
      $moCurrentHandler->setIds($maCurrentIds);
    }
    else{
      $moCurrentHandler->setEmpty(true);
    }
    
    $moSearchGrid->setQueryHandler($moSearchHandler);
    $moCurrentGrid->setQueryHandler($moCurrentHandler);
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    //teste para verificar se o sistema n�o est� sendo hakeado
    $moCtxUserTest = new RMBestPracticeEvent();
    $moCtxUserTest->testPermissionToInsert();
    
    $moWebLib = FWDWebLib::getInstance();
    $moSelect = FWDWebLib::getObject('best_practice_section_id');
    $moQuery = new QueryContextHierarchicalStructure(FWDWebLib::getConnection());
    $moQuery->setContextType(CONTEXT_SECTION_BEST_PRACTICE);
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    foreach ($moQuery->getContexts() as $maContext) {
      list($miContextId,$msHierarchicalName) = $maContext;
      $moSelect->setItemValue($miContextId,$msHierarchicalName);
    }
    
    $moQuery = new QueryBestPracticeFromEvent(FWDWebLib::getConnection());
    $moQuery->setEventId(FWDWebLib::getObject('event')->getValue());
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    FWDWebLib::getObject('current_best_practices_ids')->setValue(implode(":",$moQuery->getBestPractices()));
    
    $moWebLib->dump_html($moWebLib->getObject('dialog'));
?>
<script language="javascript">
  gebi('best_practice_name').focus();
  function refresh_grid() {    
    gobi('grid_current_best_practices').setPopulate(true);
    gobi('grid_current_best_practices').refresh();
    //gobi('grid_best_practice_search').setPopulate(true);
    //gobi('grid_best_practice_search').refresh();
  }
	function enter_bestpractice_search_event(e)
	{
		if (!e) e = window.event;
		if(e['keyCode']==13) {
			gebi('name_filter').value = gebi('best_practice_name').value;
	    gebi('section_filter').value = gebi('best_practice_section_id').value;
	    gebi('current_best_practices_ids').value = gobi('grid_current_best_practices').getAllIds().join(':');
      gobi('grid_best_practice_search').setPopulate(true);
	    trigger_event("search_best_practices_event","3");
		}
	}
	FWDEventManager.addEvent(gebi('best_practice_name'), 'keydown', enter_bestpractice_search_event);
  refresh_grid();
</script>
<?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_best_practice_add.xml');

?>