<?php

include_once 'include.php';
include_once $handlers_ref . "select/continuity/QuerySelectThreatProbability.php";
include_once $handlers_ref . "select/continuity/QuerySelectThreatTypes.php";
include_once $handlers_ref . "select/QuerySelectReadableDocumentByContext.php";

function save($poContext,$pbHasSensitiveChanges,$piContextId=0){
	if($piContextId){
		$poContext->update($piContextId,true,$pbHasSensitiveChanges);
	}else{
		$piContextId = $poContext->insert(true);
		$moPlaceCategoryThreat = new CMPlaceCategoryThreat();
		$moPlaceCategoryThreat->setFieldValue('placecategory_id', FWDWebLib::getObject('place_category_id')->getValue());

		$moPlaceCategoryThreat->setFieldValue('threat', $piContextId);
		$moPlaceCategoryThreat->insert();
	}

	echo "soWindow = soPopUpManager.getPopUpById('popup_place_category_threat_edit').getOpener();"
	."if(soWindow.refresh_grid) soWindow.refresh_grid();"
	."soPopUpManager.closePopUp('popup_place_category_threat_edit');";
}

function getThreatFromFields(){
	$moThreat = new CMThreat();

	$msThreatName = FWDWebLib::getObject('threat_name')->getValue();

	$msThreatDescription = FWDWebLib::getObject('threat_description')->getValue();
	//	$miThreatProbability = FWDWebLib::getObject('select_probability')->getValue();
	$miThreatType = FWDWebLib::getObject('select_type')->getValue();

	$moThreat->setFieldValue('threat_name', $msThreatName);
	$moThreat->setFieldValue('threat_description', $msThreatDescription);
	//	$moThreat->setFieldValue('threat_probability', $miThreatProbability);
	$moThreat->setFieldValue('threat_type', $miThreatType);
	$moThreat->setFieldValue('threat_class', CONTEXT_CM_PLACE_CATEGORY);

	return $moThreat;
}

class ConfirmEditEvent extends FWDRunnable {
	public function run(){
		$miAreaId = FWDWebLib::getObject('area_id')->getValue();
		$moArea = getAreaFromFields();
		save($moArea,true,$miAreaId);
	}
}

class SaveThreatEvent extends FWDRunnable {
	public function run(){
		$miThreatId = FWDWebLib::getObject('threat_id')->getValue();
		$moThreat = getThreatFromFields();
		save($moThreat,true,$miThreatId);
	}
}

class DocumentViewEvent extends FWDRunnable {
	public function run() {
		$moWebLib = FWDWebLib::getInstance();
		$miDocumentId = $moWebLib->getObject('select_area_document')->getValue();
		$miUserId = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();

		$moDocument = new PMDocument();
		$moDocument->testPermissionToRead($miDocumentId);
		$moDocument->fetchById($miDocumentId);
		$moPMDocInstance = $moDocument->getPublishedInstance();
		$miDocInstanceId = $moPMDocInstance->getFieldValue('doc_instance_id');

		$moReader = new PMDocumentReader();
		$moReader->createFilter($miDocumentId,'document_id');
		$moReader->createFilter($miUserId,'user_id');
		$moReader->select();
		if ($moReader->fetch()) {
			$moReader->setFieldValue('user_has_read_document',1);
			$moReader->update();
		}

		$moPMDocReadHistory = new PMDocReadHistory();
		$moPMDocReadHistory->setFieldValue('user_id',$miUserId);
		$moPMDocReadHistory->setFieldValue('document_id',$miDocInstanceId);
		$moPMDocReadHistory->setFieldValue('read_date',ISMSLib::ISMSTime());
		$moPMDocReadHistory->insert();

		$mbIsLink = (bool)$moPMDocInstance->getFieldValue('doc_instance_is_link');
		if ($mbIsLink) {
			$msURL = $moPMDocInstance->getFieldValue('doc_instance_link');
			if ($msURL)
			echo "js_open('{$msURL}','document_link',740,502,'center_screen');";
		} else {
			echo "js_submit('download_file','ajax');";
		}
	}
}

class DownloadFileEvent extends FWDRunnable {
	public function run() {
		$moWebLib = FWDWebLib::getInstance();
		$miDocumentId = $moWebLib->getObject('select_area_document')->getValue();
		$miUserId = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();

		$moDocument = new PMDocument();
		$moDocument->testPermissionToRead($miDocumentId);
		$moDocument->fetchById($miDocumentId);
		$moPMDocInstance = $moDocument->getPublishedInstance();

		$msPath = $moWebLib->getSysRef()."packages/policy/".$moPMDocInstance->getFieldValue('doc_instance_path');
		$msFileName = $moPMDocInstance->getFieldValue('doc_instance_file_name');
		header('Cache-Control: ');// leave blank to avoid IE errors
		header('Pragma: ');// leave blank to avoid IE errors
		header('Content-type: application/octet-stream');
		header('Content-Disposition: attachment; filename="'.$msFileName.'"');
		$moCrypt = new FWDCrypt();
		$moFP = fopen($msPath,'rb');
		$moCrypt->setIV(fread($moFP,strlen($moCrypt->getIV())));
		while(!feof($moFP)) {
			echo $moCrypt->decryptNoBase64(fread($moFP,16384));
		}
		fclose($moFP);
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(''));
		$moStartEvent->addAjaxEvent(new SaveThreatEvent('save_threat_event'));
		$moStartEvent->addAjaxEvent(new ConfirmEditEvent('confirm_edit'));
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$miThreatId = FWDWebLib::getObject('param_threat_id')->getValue();
		FWDWebLib::getObject('threat_id')->setValue($miThreatId);

		$miPlaceCategoryId = FWDWebLib::getObject('param_place_category_id')->getValue();
		FWDWebLib::getObject('place_category_id')->setValue($miPlaceCategoryId);

		$moWindowTitle = FWDWebLib::getObject('window_title');
		$moIcon = FWDWebLib::getObject('tooltip_icon');

		//		$moHandler = new QuerySelectThreatProbability(FWDWebLib::getConnection());
		//		$moSelect = FWDWebLib::getObject('select_probability');
		//		$moSelect->setQueryHandler($moHandler);
		//		$moSelect->populate();

		$moHandler = new QuerySelectThreatTypes(FWDWebLib::getConnection());
		$moSelect = FWDWebLib::getObject('select_type');
		$moSelect->setQueryHandler($moHandler);
		$moSelect->populate();


		if($miThreatId){
			$moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_threat_editing', 'Edi��o de Amea�a'));

			$cmThreat = new CMThreat();
			$cmThreat->fetchById($miThreatId);
			FWDWebLib::getObject('threat_name')->setValue($cmThreat->getFieldValue('threat_name'));
			FWDWebLib::getObject('threat_description')->setValue($cmThreat->getFieldValue('threat_description'));
			//			FWDWebLib::getObject('select_probability')->checkItem($cmThreat->getFieldValue('threat_probability'));
			FWDWebLib::getObject('select_type')->checkItem($cmThreat->getFieldValue('threat_type'));
		}else{
			//teste para verificar se o sistema n�o est� sendo hakeado
			$moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_threat_adding', 'Adi��o de Amea�a'));
		}

		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
<script language="javascript">
        gebi('threat_name').focus();
        
      </script>
		<?
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_place_category_threat_edit.xml');

?>