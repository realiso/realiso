<?php
set_time_limit(3600);
include_once "include.php";


class LocalLevelIterator extends FWDReportLevelIterator {

	public function __construct(){
		parent::__construct("place");
	}

	public function fetch(FWDDBDataSet $poDataSet){
		$moWebLib = FWDWebLib::getInstance();
		
		$place = $poDataSet->getFieldByAlias("place")->getValue();
		$moWebLib->getObject('place')->setValue($place);
		
	}
}


class AssetLevelIterator extends FWDReportLevelIterator {

	public function __construct(){
		parent::__construct("process");
	}

	public function fetch(FWDDBDataSet $poDataSet){
		$moWebLib = FWDWebLib::getInstance();

		$asset = $poDataSet->getFieldByAlias("asset")->getValue();
		//$place = $poDataSet->getFieldByAlias("place")->getValue();
		$process = $poDataSet->getFieldByAlias("process")->getValue();
		$tra = $poDataSet->getFieldByAlias("tra")->getValue();
		$tac = $poDataSet->getFieldByAlias("tac")->getValue();
		$isa = $poDataSet->getFieldByAlias("isa")->getValue();
		//$plans = $poDataSet->getFieldByAlias("plans")->getValue();
		$rto = $poDataSet->getFieldByAlias("rto")->getValue();
		
		$moWebLib->getObject('asset')->setValue($asset);
		//$moWebLib->getObject('place')->setValue($place);
		$moWebLib->getObject('process')->setValue($process);
		$moWebLib->getObject('tra')->setValue($tra);
		$moWebLib->getObject('tac')->setValue($tac);
		$moWebLib->getObject('isa')->setValue($isa);
		//$moWebLib->getObject('plans')->setValue($plans);
		$moWebLib->getObject('rto')->setValue($rto);

		if($isa <= 1){
			//$msIcon = FWDWebLib::getInstance()->getGfxRef() . 'icon-asset_red.png';
			$moWebLib->getObject('isa')->setAttrBgColor('ff0a00');
			//$moWebLib->getObject('status')->setAttrBgColor('ff0a00');
			//$moWebLib->getObject('status')->setValue('Alto'); //traducao?
		}else if($isa > 1 && $isa <= 1.25){
			//$msIcon = FWDWebLib::getInstance()->getGfxRef() . 'icon-asset_yellow.png';
			$moWebLib->getObject('isa')->setAttrBgColor('fcff00');
			//$moWebLib->getObject('status')->setAttrBgColor('fcff00');
			//$moWebLib->getObject('status')->setValue('M�dio');
		}else if($isa > 1.25){
			//$msIcon = FWDWebLib::getInstance()->getGfxRef() . 'icon-asset_green.png';
			$moWebLib->getObject('isa')->setAttrBgColor('00f300');
			//$moWebLib->getObject('status')->setAttrBgColor('00f300');
			//$moWebLib->getObject('status')->setValue('Baixo');
		}

		//FWDWebLib::getObject('asset_icon')->setAttrSrc($msIcon);
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$moWebLib = FWDWebLib::getInstance();
		$moFilter = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter();

		$moWebLib->xml_load("popup_report_asa.xml", false);
		
		$moWebLib->getObject("level_local")->setLevelIterator(new LocalLevelIterator());
		
		$moWebLib->getObject("level_asset")->setLevelIterator(new AssetLevelIterator());

		$moComponent = new FWDReportGenerator();
		$moComponent->setReport($moWebLib->getObject('report'));
		$moComponent->setReportTemplate(new ISMSReportTemplate());
		$moComponent->setReportFilter($moFilter);
		$moComponent->setReportName(FWDLanguage::getPHPStringValue('asa', "An�lise de Sustentabilidade de Ativos"));
		$moComponent->generate();
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDStartEvent::getInstance()->start();
?>