<?php
set_time_limit(3600);
include_once "include.php";

class PriorityLevelIterator extends FWDReportLevelIterator {  

  public function __construct(){
    parent::__construct("class_id");
  }
	
  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();    
    $moWebLib->getObject('priority_name')->setValue($poDataSet->getFieldByAlias("class_name")->getValue());
  }
}

class ProcessLevelIterator extends FWDReportLevelIterator {

  public function __construct(){
    parent::__construct("process_id");
  }

  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msGfxRef = $moWebLib->getGfxRef();

    $miProcessValue = $poDataSet->getFieldByAlias("process_value")->getValue();		
		$msIcon = $msGfxRef . 'icon-process_' . RMRiskConfig::getRiskColor($miProcessValue) . '.png';   

    $moWebLib->getObject('process_icon')->setAttrSrc($msIcon);
    $moWebLib->getObject('process_name')->setValue($poDataSet->getFieldByAlias("process_name")->getValue());
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();    
    $moWebLib->getObject("level_priority")->setLevelIterator(new PriorityLevelIterator());
    $moWebLib->getObject("level_process")->setLevelIterator(new ProcessLevelIterator());
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport($moWebLib->getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    $moComponent->setReportFilter($moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter());
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('rs_processes_by_priority', "Processos por Prioridade"));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_report_processes_by_priority.xml");
?>