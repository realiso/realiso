<?php
set_time_limit(3600);
include_once "include.php";

class ProcessLevelIterator extends FWDReportLevelIterator {  

  public function __construct(){
    parent::__construct("process_id");
  }
	
  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msGfxRef = $moWebLib->getGfxRef();

    $msProcessName = $poDataSet->getFieldByAlias("process_name")->getValue();    
    $miProcessValue = $poDataSet->getFieldByAlias("process_value")->getValue();
    
    $msIcon = $msGfxRef . 'icon-process_' . RMRiskConfig::getRiskColor($miProcessValue) . '.png';
        
    $moWebLib->getObject('process_icon')->setAttrSrc($msIcon);
    $moWebLib->getObject('process_name')->setValue($msProcessName);    
  }
}

class AssetLevelIterator extends FWDReportLevelIterator {  

  public function __construct(){
    parent::__construct("asset_id");
  }
	
  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msGfxRef = $moWebLib->getGfxRef();

    $msAssetName = $poDataSet->getFieldByAlias("asset_name")->getValue();    
    $miAssetValue = $poDataSet->getFieldByAlias("asset_value")->getValue();
    
    $msIcon = $msGfxRef . 'icon-asset_' . RMRiskConfig::getRiskColor($miAssetValue) . '.png';
        
    $moWebLib->getObject('asset_icon')->setAttrSrc($msIcon);
    $moWebLib->getObject('asset_name')->setValue($msAssetName);    
  }
}

class RiskLevelIterator extends FWDReportLevelIterator {

  public function __construct(){
    parent::__construct("risk_id");
  }

  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();

    $msGfxRef = $moWebLib->getGfxRef();

    $msRiskName = $poDataSet->getFieldByAlias("risk_name")->getValue();
    $msRiskImpact = $poDataSet->getFieldByAlias("risk_impact")->getValue() ? ' / '.$poDataSet->getFieldByAlias("risk_impact")->getValue() : '';
    $miRiskValue = $poDataSet->getFieldByAlias("risk_value")->getValue();		        
		$miRiskResidualValue = $poDataSet->getFieldByAlias("risk_residual_value")->getValue();
		$miRiskIsAccepted = $poDataSet->getFieldByAlias("risk_accept_mode")->getValue();
		
    $moFilter = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter();
    $miValueRiskColor = ($moFilter->getRiskValueType() == RISK_VALUE_TYPE_REAL_VALUE) ? $miRiskValue : $miRiskResidualValue;
		
		if ($miRiskIsAccepted) $msIcon = $msGfxRef . 'icon-risk_green.png';
    else $msIcon = $msGfxRef . 'icon-risk_' . RMRiskConfig::getRiskColor($miValueRiskColor) . '.png';

    $moWebLib->getObject('risk_icon')->setAttrSrc($msIcon);    
    $moWebLib->getObject('risk_name')->setValue($msRiskName.$msRiskImpact);
    $moWebLib->getObject('risk_value')->setValue(RMRiskConfig::getRiskValueFormat($miRiskValue));
    $moWebLib->getObject('risk_residual_value')->setValue(RMRiskConfig::getRiskValueFormat($miRiskResidualValue));    
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $moFilter = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter();
    
    if ($moFilter->mustOrganizeByAsset()) {
    	FWDWebLib::getInstance()->xml_load("popup_report_risks_by_process_and_asset.xml", false);
    	$moWebLib->getObject("level_process")->setLevelIterator(new ProcessLevelIterator());
    	$moWebLib->getObject("level_asset")->setLevelIterator(new AssetLevelIterator());
    	$moWebLib->getObject("level_risk")->setLevelIterator(new RiskLevelIterator());
    }
    else {
    	FWDWebLib::getInstance()->xml_load("popup_report_risks_by_process.xml", false);
    	$moWebLib->getObject("level_process")->setLevelIterator(new ProcessLevelIterator());
    	$moWebLib->getObject("level_risk")->setLevelIterator(new RiskLevelIterator());
    }
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport($moWebLib->getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    $moComponent->setReportFilter($moFilter);
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('rs_risks_per_process', "Riscos por Processo"));
    $moComponent->generate();    
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
/*
 * Ao inv�s do xml_load ser chamado aqui, ele � chamado
 * baseado no filtro do relat�rio que o usu�rio escolheu.
 * Com isso, � necess�rio chamar a fun��o start() manualmente.
 */
FWDStartEvent::getInstance()->start();
?>