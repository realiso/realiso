<?php

set_time_limit(3600);
include_once 'include.php';

class AssetLevelIterator extends FWDReportLevelIterator {
  
  public function __construct(){
    parent::__construct('asset_id');
  }
  
  public function fetch(FWDDBDataSet $poDataSet){
    $miValue = $poDataSet->getFieldByAlias('asset_value')->getValue();
    $msIcon = FWDWebLib::getInstance()->getGfxRef() . 'icon-asset_' . RMRiskConfig::getRiskColor($miValue) . '.png';
    FWDWebLib::getObject('asset_icon')->setAttrSrc($msIcon);
    FWDWebLib::getObject('asset_name')->setValue($poDataSet->getFieldByAlias('asset_name')->getValue());    
  }
  
}
class RiskLevelIterator extends FWDReportLevelIterator {
  
  public function __construct(){
    parent::__construct('risk_name');
  }
  
  public function fetch(FWDDBDataSet $poDataSet){
    $msIcon = FWDWebLib::getInstance()->getGfxRef() . 'icon-risk_gray.png';
    FWDWebLib::getObject('risk_icon')->setAttrSrc($msIcon);
    FWDWebLib::getObject('risk_name')->setValue($poDataSet->getFieldByAlias('risk_name')->getValue());
    FWDWebLib::getObject('risk_count')->setValue($poDataSet->getFieldByAlias('risk_count')->getValue());
  }
  
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moFilter = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter();
    
    FWDWebLib::getObject('asset_level')->setLevelIterator(new AssetLevelIterator());
    
    FWDWebLib::getObject('risk_level')->setLevelIterator(new RiskLevelIterator());
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport(FWDWebLib::getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    
    $moComponent->setReportFilter($moFilter);
    
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('tt_duplicated_risks_by_asset','Riscos Duplicados por Ativo'));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_report_duplicated_risks_by_asset.xml');

?>