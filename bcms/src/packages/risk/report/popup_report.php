<?php
set_time_limit(3000);
include_once "include.php";

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$moWebLib = FWDWebLib::getInstance();

		$msLink = '';
		$msPopup = $moWebLib->getObject('popup')->getValue();
		switch($msPopup) {
			case "report_risks_by_control":
				$msLink = "popup_report_risks_by_control.php";
			break;
			
			case "report_risk_treatment_plan":
				$msLink = "popup_report_risk_treatment_plan.php";
			break;
			
			case "report_control_summary":
				$msLink = "popup_report_control_summary.php";
			break;
			
			case "report_control_accompaniment":
				$msLink = "popup_report_control_accompaniment.php";
			break;
			
			case "report_control_efficiency_accompaniment":
				$msLink = "popup_report_control_efficiency_accompaniment.php";
			break;
			
			case "report_areas_without_processes":
				$msLink = "popup_report_areas_without_processes.php";
			break;
			
			case "report_processes_without_assets":
				$msLink = "popup_report_processes_without_assets.php";
			break;
			
			case "report_assets_without_risks":
				$msLink = "popup_report_assets_without_risks.php";
			break;
			
			case "report_controls_not_measured":
				$msLink = "popup_report_controls_not_measured.php";
			break;
			
			case "report_risk_values":
				$msLink = "popup_report_risk_values.php";
			break;
			
			case "report_user_responsabilities":
				$msLink = "popup_report_user_responsabilities.php";
			break;
			
			case "report_detailed_pendant_tasks":
				$msLink = "popup_report_detailed_pendant_tasks.php";
			break;
			
			case "report_summarized_pendant_tasks":
				$msLink = "popup_report_summarized_pendant_tasks.php";
			break;
			
			case "report_pendant_contexts":
				$msLink = "popup_report_pendant_contexts.php";
			break;
			
			case "report_asset":
				$msLink = "popup_report_asset.php";
			break;
			
			case "report_risks_by_asset":
				$msLink = "popup_report_risks_by_asset.php";
			break;
			
			case "report_risks_by_process":
				$msLink = "popup_report_risks_by_process.php";
			break;
			
			case "report_controls_by_risk":
				$msLink = "popup_report_controls_by_risk.php";
			break;
			
			case "report_controls_by_responsible":
				$msLink = "popup_report_controls_by_responsible.php";
			break;
			
			case "report_control_efficiency":
				$msLink = "popup_report_control_efficiency.php";
			break;
			
			case "report_control_implementation_date":
				$msLink = "popup_report_control_implementation_date.php";
			break;
			
			case "report_control_planning":
				$msLink = "popup_report_control_planning.php";
			break;
			
			case "report_control_cost_by_responsible":
				$msLink = "popup_report_control_cost_by_responsible.php";
			break;
			
			case "report_control_cost_by_asset":
				$msLink = "popup_report_control_cost_by_asset.php";
			break;
			
			case "report_control_cost_by_process":
				$msLink = "popup_report_control_cost_by_process.php";
			break;
			
			case "report_control_cost_by_area":
				$msLink = "popup_report_control_cost_by_area.php";
			break;
			
			case "report_top10_assets_with_more_risks":
				$msLink = "popup_report_top10_assets_with_more_risks.php";
			break;
			
			case "report_top10_assets_with_more_high_risks":
				$msLink = "popup_report_top10_assets_with_more_risks.php";
			break;
			
			case "report_control_cost":
				$msLink = "popup_report_control_cost.php";
			break;
			
			case "report_risk_amount_by_process":
				$msLink = "popup_report_risk_amount_by_process.php";
			break;
			
			case "report_risk_amount_by_area":
				$msLink = "popup_report_risk_amount_by_area.php";
			break;
			
			case "report_risk_status_by_area":
				$msLink = "popup_report_risk_status_by_area.php";
			break;
			
			case "report_risk_status_by_process":
				$msLink = "popup_report_risk_status_by_process.php";
			break;
			
			case "report_top10_risks_by_process":
				$msLink = "popup_report_top10_risks_by_process.php";
			break;
			
			case "report_top10_risks_by_area":
				$msLink = "popup_report_top10_risks_by_area.php";
			break;
			
			case "report_statement_of_applicability":
				$msLink = "popup_report_statement_of_applicability.php";
			break;
			
			case "report_risk_impact":
				$msLink = "popup_report_risk_impact.php";
			break;
			
			case "report_asset_importance":
				$msLink = "popup_report_asset_importance.php";
			break;
			
			case "report_risks_by_area":
				$msLink = "popup_report_risks_by_area.php";
			break;		
			
			case "report_events_by_category":
				$msLink = "popup_report_events_by_category.php";
			break;
			
			case "report_controls_without_risks":
				$msLink = "popup_report_controls_without_risks.php";
			break;
			
			default:
				$msLink = "popup_" . $msPopup . ".php";
			break;
		}
		
		$msIframe = '<iframe name="report" framed="0" frameborder=0 autoScroll="VSCROLLBAR" style="cursor:wait;margin-left:10px;" height="100%" width="100%" src="clock.php?'.$msLink.'" ></iframe>';
		
		$moReportDiv = new FWDDiv(new FWDBox(0,0,500,730));
		$moReportDiv->setName("report_div");
		$moReportDiv->setValue($msIframe);
		
		$moWebLib->getObject('dialog')->addObjFWDView($moReportDiv);
		$moWebLib->dump_html($moWebLib->getObject('dialog'));
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_report.xml");
?>