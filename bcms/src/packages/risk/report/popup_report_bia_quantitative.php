<?php
set_time_limit(3600);
include_once "include.php";
include_once $handlers_ref . "select/QueryCheckFinancialLoss.php";


class PlaceLevelIterator extends FWDReportLevelIterator {

	public function __construct(){
		parent::__construct("place_name");
	}

	public function fetch(FWDDBDataSet $poDataSet){
		$moWebLib = FWDWebLib::getInstance();
		$placeName= $poDataSet->getFieldByAlias("place_name")->getValue();
		$moWebLib->getObject('place_name')->setValue($placeName);
	}
}


class ProcessLevelIterator extends FWDReportLevelIterator {

	public function __construct(){
		parent::__construct("name");
		$moWebLib = FWDWebLib::getInstance();
		$query = new FWDDBDataSet(FWDWebLib::getConnection());
		$query->setQuery("select fkcontext as id, sname as name from cm_damage_matrix order by id");
		$query->addFWDDBField(new FWDDBField('id','id'  ,DB_NUMBER));
		$query->addFWDDBField(new FWDDBField('name','name'  ,DB_STRING));
		$query->execute();

		/*$i = 1;
		while($query->fetch()){
			
			$name = $query->getFieldByAlias("name")->getValue();
			$moWebLib->getObject('f'.$i)->setValue($name);

			$i++;
		}*/
	}

	public function fetch(FWDDBDataSet $poDataSet){
		$moWebLib = FWDWebLib::getInstance();

		$processName = $poDataSet->getFieldByAlias("name")->getValue();
		$area = $poDataSet->getFieldByAlias("area")->getValue();
		
		/*$fl1 = $poDataSet->getFieldByAlias("f1");
		$fl2 = $poDataSet->getFieldByAlias("f2");
		$fl3 = $poDataSet->getFieldByAlias("f3");
		$fl4 = $poDataSet->getFieldByAlias("f4");
		$fl5 = $poDataSet->getFieldByAlias("f5");
		$fl6 = $poDataSet->getFieldByAlias("f6");
		$fl7 = $poDataSet->getFieldByAlias("f7");*/

		$processFinancialLoss = $poDataSet->getFieldByAlias("financialloss")->getValue();

		$moWebLib->getObject('process_name')->setValue($processName);
		$moWebLib->getObject('area')->setValue($area);

		/*if($fl1)
			$moWebLib->getObject('fl1')->setValue($fl1->getValue());
		
		if($fl2)
			$moWebLib->getObject('fl2')->setValue($fl2->getValue());

		if($fl3)
			$moWebLib->getObject('fl3')->setValue($fl3->getValue());
		
		if($fl4)
			$moWebLib->getObject('fl4')->setValue($fl4->getValue());

		if($fl5)
			$moWebLib->getObject('fl5')->setValue($fl5->getValue());

		if($fl6)
			$moWebLib->getObject('fl6')->setValue($fl6->getValue());

		if($fl7)
			$moWebLib->getObject('fl7')->setValue($fl7->getValue());*/

		// Setar a cor do financialloss.
		$moQueryFinancialLoss = new QueryCheckFinancialLoss(FWDWebLib::getConnection());
		$moQueryFinancialLoss->makeQuery();
		$moQueryFinancialLoss->fetchFinancialLoss();
		$financialLossValues = $moQueryFinancialLoss->getValuesFinancialLoss();

		if ($processFinancialLoss <= $financialLossValues[1]) $cor = "<font color=green>";
		else if ($processFinancialLoss > $financialLossValues[2] && $processFinancialLoss <= $financialLossValues[3]) $cor = "<font color=#c9b52e>";
		else if ($processFinancialLoss > $financialLossValues[4] && $processFinancialLoss <= $financialLossValues[5]) $cor = "<font color=orange>";
		else if ($processFinancialLoss > $financialLossValues[6] && $processFinancialLoss <= $financialLossValues[7]) $cor = "<font color=red>";
		else $cor = "<font color=purple>";
		
		$moWebLib->getObject('financialloss')->setValue($cor . $processFinancialLoss . "</font>");
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$moWebLib = FWDWebLib::getInstance();
		$moFilter = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter();

		FWDWebLib::getInstance()->xml_load("popup_report_bia_quantitative.xml", false);
		$moWebLib->getObject("level_place")->setLevelIterator(new PlaceLevelIterator());
		$moWebLib->getObject("level_process")->setLevelIterator(new ProcessLevelIterator());

		$moComponent = new FWDReportGenerator();
		$moComponent->setReport($moWebLib->getObject('report'));
		$moComponent->setReportTemplate(new ISMSReportTemplate());
		$moComponent->setReportFilter($moFilter);
		$moComponent->setReportName(FWDLanguage::getPHPStringValue('bia_quantitative', "BIA Quantitativo"));
		$moComponent->generate();
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
/*
 * Ao inv�s do xml_load ser chamado aqui, ele � chamado
 * baseado no filtro do relat�rio que o usu�rio escolheu.
 * Com isso, � necess�rio chamar a fun��o start() manualmente.
 */
FWDStartEvent::getInstance()->start();
?>