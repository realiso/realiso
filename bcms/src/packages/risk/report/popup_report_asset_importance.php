<?php
set_time_limit(3600);
include_once "include.php";

class AssetLevelIterator extends FWDReportLevelIterator {  

  public function __construct(){
    parent::__construct("asset_id");
  }
	
  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msGfxRef = $moWebLib->getGfxRef();
   
    $msAssetName = $poDataSet->getFieldByAlias("asset_name")->getValue();    		
		$miAssetValue = $poDataSet->getFieldByAlias("asset_value")->getValue();
		
    $msIcon = $msGfxRef . 'icon-asset_' . RMRiskConfig::getRiskColor($miAssetValue) . '.png';

    $moWebLib->getObject('asset_icon')->setAttrSrc($msIcon);    
    $moWebLib->getObject('asset_name')->setValue($msAssetName);    
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();    
    $moWebLib->getObject("level_asset")->setLevelIterator(new AssetLevelIterator());    
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport($moWebLib->getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    $moComponent->setReportFilter($moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter());
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('rs_assets_importance',"Importāncia dos Ativos"));
    $moComponent->generate();  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_report_asset_importance.xml");
?>