<?php
set_time_limit(3600);
include_once "include.php";

class PlaceLevelIterator extends FWDReportLevelIterator {

	public function __construct(){
		parent::__construct("place_name");
	}

	public function fetch(FWDDBDataSet $poDataSet){
		$moWebLib = FWDWebLib::getInstance();	
		
		$placeName = $poDataSet->getFieldByAlias("place_name")->getValue();
		$moWebLib->getObject('place_name')->setValue($placeName);
	}
	
}

class ProcessLevelIterator extends FWDReportLevelIterator {

	public function __construct(){
		parent::__construct("process_name");
	}

	public function fetch(FWDDBDataSet $poDataSet){
		$moWebLib = FWDWebLib::getInstance();	
		
		$processName = $poDataSet->getFieldByAlias("process_name")->getValue();
		$processDescription = $poDataSet->getFieldByAlias("process_description")->getValue();
		$processRTO = $poDataSet->getFieldByAlias("process_rto")->getValue();

		$moWebLib->getObject('process_name')->setValue($processName);
		$moWebLib->getObject('process_description')->setValue($processDescription);
		

		$rtoFlag = $poDataSet->getFieldByAlias("process_rtoflag")->getValue();

	  $rtoMessage = '';
	  
	  if($rtoFlag == 1){
	    $rtoMessage = FWDLanguage::getPHPStringValue('rtoMinutes','minutos');
	  } else if($rtoFlag == 2){
	    $rtoMessage = FWDLanguage::getPHPStringValue('rtoHours','horas');
	  } else {
	    $rtoMessage = FWDLanguage::getPHPStringValue('rtoDays','dias');
	  }		
		
		$moWebLib->getObject('process_rto')->setValue($processRTO . ' ' . $rtoMessage);
	}
	
}

class AssetLevelIterator extends FWDReportLevelIterator {

	public function __construct(){
		parent::__construct("asset_name");
	}

	public function fetch(FWDDBDataSet $poDataSet){
		$moWebLib = FWDWebLib::getInstance();	
		
		$assetName = $poDataSet->getFieldByAlias("asset_name")->getValue();
		$assetTAC = $poDataSet->getFieldByAlias("asset_tac")->getValue();
		$assetTRA = $poDataSet->getFieldByAlias("asset_tra")->getValue();
		
		$moWebLib->getObject('asset_name')->setValue($assetName);
		$moWebLib->getObject('asset_tac')->setValue($assetTAC);
		$moWebLib->getObject('asset_tra')->setValue($assetTRA);		
	}
	
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){	  
    $moWebLib = FWDWebLib::getInstance();
		$moFilter = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter();

		FWDWebLib::getInstance()->xml_load("popup_report_process_by_asset.xml", false);
		$moWebLib->getObject("place_level")->setLevelIterator(new PlaceLevelIterator());
    $moWebLib->getObject("process_level")->setLevelIterator(new ProcessLevelIterator());
    $moWebLib->getObject("asset_level")->setLevelIterator(new AssetLevelIterator());		
		
		$moComponent = new FWDReportGenerator();
		$moComponent->setReport($moWebLib->getObject('report'));
		$moComponent->setReportTemplate(new ISMSReportTemplate());
		$moComponent->setReportFilter($moFilter);
		$moComponent->setReportName(FWDLanguage::getPHPStringValue('process_by_assets', "Processos X Ativos"));
		$moComponent->generate();	  
	  
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
/*
 * Ao inv�s do xml_load ser chamado aqui, ele � chamado
 * baseado no filtro do relat�rio que o usu�rio escolheu.
 * Com isso, � necess�rio chamar a fun��o start() manualmente.
 */
FWDStartEvent::getInstance()->start();
?>