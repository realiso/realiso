<?php
set_time_limit(3600);
include_once "include.php";

class ProcessLevelIterator extends FWDReportLevelIterator {

	public function __construct(){
		parent::__construct("processId");
	}

	public function fetch(FWDDBDataSet $poDataSet){
		$moWebLib = FWDWebLib::getInstance();
		$processName = $poDataSet->getFieldByAlias("processName")->getValue();
		$moWebLib->getObject('processName')->setValue($processName);
	}
}

class ActivityLevelIterator extends FWDReportLevelIterator {

	public function __construct(){
		parent::__construct("activityId");
	}

	public function fetch(FWDDBDataSet $poDataSet){
		$moWebLib = FWDWebLib::getInstance();

		$activityName = $poDataSet->getFieldByAlias("activityName")->getValue();
		$activityDescription = $poDataSet->getFieldByAlias("activityDescription")->getValue();
		$responsible = $poDataSet->getFieldByAlias("responsible")->getValue();
		$importance = $poDataSet->getFieldByAlias("importance")->getValue();

		$moWebLib->getObject('activityName')->setValue($activityName);
		$moWebLib->getObject('activityDescription')->setValue($activityDescription);
		$moWebLib->getObject('responsible')->setValue($responsible);
		$moWebLib->getObject('importance')->setValue($importance);
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$moWebLib = FWDWebLib::getInstance();
		$moFilter = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter();

		FWDWebLib::getInstance()->xml_load("popup_report_activities.xml", false);
		$moWebLib->getObject("level_process")->setLevelIterator(new ProcessLevelIterator());
		$moWebLib->getObject("level_activity")->setLevelIterator(new ActivityLevelIterator());

		$moComponent = new FWDReportGenerator();
		$moComponent->setReport($moWebLib->getObject('report'));
		$moComponent->setReportTemplate(new ISMSReportTemplate());
		$moComponent->setReportFilter($moFilter);
		$moComponent->setReportName(FWDLanguage::getPHPStringValue('reportActivityTitle', "Atividades"));
		$moComponent->generate();
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
/*
 * Ao inv�s do xml_load ser chamado aqui, ele � chamado
 * baseado no filtro do relat�rio que o usu�rio escolheu.
 * Com isso, � necess�rio chamar a fun��o start() manualmente.
 */
FWDStartEvent::getInstance()->start();
?>