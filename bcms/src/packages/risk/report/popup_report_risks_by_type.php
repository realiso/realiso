<?php
set_time_limit(3600);
include_once "include.php";

class TypeLevelIterator extends FWDReportLevelIterator {  

  public function __construct(){
    parent::__construct("class_id");
  }
	
  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();    
    $moWebLib->getObject('type_name')->setValue($poDataSet->getFieldByAlias("class_name")->getValue());
  }
}

class RiskLevelIterator extends FWDReportLevelIterator {

  public function __construct(){
    parent::__construct("risk_id");
  }

  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msGfxRef = $moWebLib->getGfxRef();

    $miAreaValue = $poDataSet->getFieldByAlias("risk_value")->getValue();		
		$msIcon = $msGfxRef . 'icon-risk_' . RMRiskConfig::getRiskColor($miAreaValue) . '.png';   

    $moWebLib->getObject('risk_icon')->setAttrSrc($msIcon);
    $moWebLib->getObject('risk_name')->setValue($poDataSet->getFieldByAlias("risk_name")->getValue());
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();    
    $moWebLib->getObject("level_type")->setLevelIterator(new TypeLevelIterator());
    $moWebLib->getObject("level_risk")->setLevelIterator(new RiskLevelIterator());
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport($moWebLib->getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    $moComponent->setReportFilter($moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter());
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('rs_risks_by_type', "Riscos por Tipo"));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_report_risks_by_type.xml");
?>