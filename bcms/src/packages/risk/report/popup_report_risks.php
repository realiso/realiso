<?php

set_time_limit(3600);
include_once 'include.php';

class RiskLevelIterator extends FWDReportLevelIterator {
  
  public function __construct(){
    parent::__construct('risk_id');
  }
  
  public function fetch(FWDDBDataSet $poDataSet){
    $miRiskValue = $poDataSet->getFieldByAlias('risk_value')->getValue();
    $miRiskResidualValue = $poDataSet->getFieldByAlias('risk_residual_value')->getValue();
    $msIconSrc = FWDWebLib::getInstance()->getGfxRef()."icon-risk_".RMRiskConfig::getRiskColor($miRiskResidualValue).".gif";
    
    FWDWebLib::getObject('risk_id')->setAttrSrc($msIconSrc);
    FWDWebLib::getObject('risk_name')->setValue($poDataSet->getFieldByAlias('risk_name')->getValue());
    FWDWebLib::getObject('risk_value')->setValue(RMRiskConfig::getRiskValueFormat($miRiskValue));
    FWDWebLib::getObject('risk_residual_value')->setValue((RMRiskConfig::getRiskValueFormat($miRiskResidualValue)));
  }
  
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moFilter = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter();
    
    FWDWebLib::getObject('risk_level')->setLevelIterator(new RiskLevelIterator());
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport(FWDWebLib::getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    
    $moComponent->setReportFilter($moFilter);
    
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('mx_risks','Riscos'));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_report_risks.xml');

?>