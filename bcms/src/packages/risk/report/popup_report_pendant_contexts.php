<?php
set_time_limit(3600);
include_once "include.php";

class TypeLevelIterator extends FWDReportLevelIterator {  

  public function __construct(){
    parent::__construct("context_type");
  }
  
  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msGfxRef = $moWebLib->getGfxRef();

    $miContextType = $poDataSet->getFieldByAlias("context_type")->getValue();
    
    $moContextObject = new ISMSContextObject();
    $moContext = $moContextObject->getContextObject($miContextType);
    $msContextLabel = FWDWebLib::convertToISO($moContext->getLabel());
    
    $moWebLib->getObject('context_label')->setValue($msContextLabel);    
  }
}

class ContextLevelIterator extends FWDReportLevelIterator {

  protected $csUserChairman = '';

  public function __construct(){
    parent::__construct("context_id");
    
    $moChairman = new ISMSUser();
    $moChairman->fetchById(ISMSLib::getConfigById(USER_CHAIRMAN));
    $this->csUserChairman = $moChairman->getFieldValue('user_name');
  }

  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msGfxRef = $moWebLib->getGfxRef();

    $miContextType = $poDataSet->getFieldByAlias("context_type")->getValue();
    $msContextName = $poDataSet->getFieldByAlias("context_name")->getValue();    
    $miContextValue = $poDataSet->getFieldByAlias("context_value")->getValue();
    $miContextApproverName = $poDataSet->getFieldByAlias("context_approver_name")->getValue();
    
    /*
     * Array para identificar os contextos que necessitam ter seu �cone
     * mudado de acordo com seu valor
     */
    $maContextsWithValues = array(CONTEXT_AREA,CONTEXT_PROCESS,CONTEXT_ASSET,CONTEXT_RISK,CONTEXT_CONTROL);
    $msContextIcon = '';    
    switch ($miContextType) {
      case CONTEXT_AREA:
        $msContextIcon = 'area';
        if (!$miContextApproverName) $miContextApproverName = $this->csUserChairman;
      break;      
      case CONTEXT_PROCESS:
        $msContextIcon = 'process';
      break;      
      case CONTEXT_ASSET:
        $msContextIcon = 'asset';
      break;      
      case CONTEXT_RISK:
        $msContextIcon = 'risk';
      break;      
      case CONTEXT_CONTROL:
        $msContextIcon = 'control';
      break;      
      case CONTEXT_CATEGORY:
        $msContextIcon = 'category';
      break;      
      case CONTEXT_EVENT:
        $msContextIcon = 'event';
      break;      
      case CONTEXT_BEST_PRACTICE:
        $msContextIcon = 'best_practice';
      break;      
      case CONTEXT_SECTION_BEST_PRACTICE:
        $msContextIcon = 'section';
      break;      
      case CONTEXT_STANDARD:
        $msContextIcon = 'standard';
      break;
    }
    
    if ($miContextType == CONTEXT_CONTROL) {
      if ($miContextValue) $msIcon = $msGfxRef . 'icon-control.png';
      else $msIcon = $msGfxRef . 'icon-control_red.png';
    }  
    else if (in_array($miContextType, $maContextsWithValues)) {
      $msIcon = $msGfxRef . "icon-{$msContextIcon}_" . RMRiskConfig::getRiskColor($miContextValue) . '.png';
    }
    else {
      $msIcon = $msGfxRef . "icon-{$msContextIcon}.png";
    }
    $moWebLib->getObject('context_icon')->setAttrSrc($msIcon);
    $moWebLib->getObject('context_name')->setValue($msContextName);
    $moWebLib->getObject('context_approver')->setValue($miContextApproverName);
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $moWebLib->getObject("level_type")->setLevelIterator(new TypeLevelIterator());
    $moWebLib->getObject("level_context")->setLevelIterator(new ContextLevelIterator());
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport($moWebLib->getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    $moComponent->setReportFilter($moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter());
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('rs_pending_items', "Elementos Pendentes"));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_report_pendant_contexts.xml");
?>