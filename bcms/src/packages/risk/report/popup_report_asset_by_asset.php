<?php
set_time_limit(3600);
include_once "include.php";

class AssetLevelIterator extends FWDReportLevelIterator {
	public function __construct(){
		parent::__construct("asset_name");
	}
	public function fetch(FWDDBDataSet $poDataSet){
		$moWebLib = FWDWebLib::getInstance();	
		
		$assetName = $poDataSet->getFieldByAlias("asset_name")->getValue();
		$assetRanking = $poDataSet->getFieldByAlias("asset_ranking")->getValue();
		$moWebLib->getObject('asset_name')->setValue($assetName);
		$moWebLib->getObject('asset_ranking')->setValue($assetRanking);
	}
}

class AssetDepLevelIterator extends FWDReportLevelIterator {

	public function __construct(){
		parent::__construct("asset_dep_name");
	}
	public function fetch(FWDDBDataSet $poDataSet){
		$moWebLib = FWDWebLib::getInstance();	
		$assetDepName = $poDataSet->getFieldByAlias("asset_dep_name")->getValue();
		$assetDepRanking = $poDataSet->getFieldByAlias("asset_dep_ranking")->getValue();
		$moWebLib->getObject('asset_dep_name')->setValue($assetDepName);
		$moWebLib->getObject('asset_dep_ranking')->setValue($assetDepRanking);
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){	  
    $moWebLib = FWDWebLib::getInstance();
		$moFilter = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter();

		FWDWebLib::getInstance()->xml_load("popup_report_asset_by_asset.xml", false);
    	$moWebLib->getObject("asset_level")->setLevelIterator(new AssetLevelIterator());
    	$moWebLib->getObject("asset_dep_level")->setLevelIterator(new AssetDepLevelIterator());		
		
		$moComponent = new FWDReportGenerator();
		$moComponent->setReport($moWebLib->getObject('report'));
		$moComponent->setReportTemplate(new ISMSReportTemplate());
		$moComponent->setReportFilter($moFilter);
		$moComponent->setReportName(FWDLanguage::getPHPStringValue('asset_by_asset', "Ativo X Ativo"));
		$moComponent->generate();	  
	  
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
/*
 * Ao inv�s do xml_load ser chamado aqui, ele � chamado
 * baseado no filtro do relat�rio que o usu�rio escolheu.
 * Com isso, � necess�rio chamar a fun��o start() manualmente.
 */
FWDStartEvent::getInstance()->start();
?>