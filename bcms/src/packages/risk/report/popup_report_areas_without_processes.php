<?php
set_time_limit(3600);
include_once "include.php";

class AreaLevelIterator extends FWDReportLevelIterator {  

  public function __construct(){
    parent::__construct("area_id");
  }
	
  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msGfxRef = $moWebLib->getGfxRef();
   
    $msAreaName = $poDataSet->getFieldByAlias("area_name")->getValue();				
		$msAreaResponsibleName = $poDataSet->getFieldByAlias("area_responsible_name")->getValue();
		$miAreaValue = $poDataSet->getFieldByAlias("area_value")->getValue();
		
		$msIcon = $msGfxRef . 'icon-area_' . RMRiskConfig::getRiskColor($miAreaValue) . '.png';    

    $moWebLib->getObject('area_icon')->setAttrSrc($msIcon);    
    $moWebLib->getObject('area_name')->setValue($msAreaName);
    $moWebLib->getObject('area_responsible_name')->setValue($msAreaResponsibleName);
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $moWebLib->getObject("level_area")->setLevelIterator(new AreaLevelIterator());

    $moComponent = new FWDReportGenerator();
    $moComponent->setReport($moWebLib->getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    $moComponent->setReportFilter($moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter());
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('rs_areas_without_processes', "�reas sem Processos"));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_report_areas_without_processes.xml");
?>