<?php
set_time_limit(3600);
include_once "include.php";


class ProcessLevelIterator extends FWDReportLevelIterator {

	public function __construct(){
		parent::__construct("process_name");
	}

	public function fetch(FWDDBDataSet $poDataSet){
		$moWebLib = FWDWebLib::getInstance();	
		
		$processName = $poDataSet->getFieldByAlias("process_name")->getValue();
		$processDescription = $poDataSet->getFieldByAlias("process_ranking")->getValue();
		$processRTO = $poDataSet->getFieldByAlias("process_rto")->getValue();
		$rtoFlag = $poDataSet->getFieldByAlias("process_rtoflag")->getValue();
		
		$moWebLib->getObject('process_name')->setValue($processName);
		$moWebLib->getObject('process_ranking')->setValue($processDescription);
				
		$rtoMessage = '';
		if($rtoFlag == 1){
			$rtoMessage = FWDLanguage::getPHPStringValue('rtoMinutes','minutos');
		} else if($rtoFlag == 2){
			$rtoMessage = FWDLanguage::getPHPStringValue('rtoHours','horas');
		} else if($rtoFlag == 3){
			$rtoMessage = FWDLanguage::getPHPStringValue('rtoDays','dias');
		} else{
			$traMessage = FWDLanguage::getPHPStringValue('rtoMonths','meses');
		}		
		$moWebLib->getObject('process_rto')->setValue($processRTO . ' ' . $rtoMessage);
	}
}

class AssetLevelIterator extends FWDReportLevelIterator {
	public function __construct(){
		parent::__construct("asset_name");
	}

	public function fetch(FWDDBDataSet $poDataSet){
		$moWebLib = FWDWebLib::getInstance();	
		
		$assetName = $poDataSet->getFieldByAlias("asset_name")->getValue();
		$assetTAC = $poDataSet->getFieldByAlias("asset_tac")->getValue();
		$assetTRA = $poDataSet->getFieldByAlias("asset_tra")->getValue();
		$traFlag = $poDataSet->getFieldByAlias("asset_traflag")->getValue();
		$tacFlag = $poDataSet->getFieldByAlias("asset_tacflag")->getValue();
		$isaTra = $poDataSet->getFieldByAlias("asset_isa_tra")->getValue();
		$isaTac = $poDataSet->getFieldByAlias("asset_isa_tac")->getValue();
				
		$moWebLib->getObject('asset_name')->setValue($assetName);
		
		$traMessage = '';
		if($assetTRA != 0){
			if($traFlag == 1){
				$traMessage = FWDLanguage::getPHPStringValue('rtoMinutes','minutos');
			} else if($traFlag == 2){
				$traMessage = FWDLanguage::getPHPStringValue('rtoHours','horas');
			} else if($traFlag == 3){
				$traMessage = FWDLanguage::getPHPStringValue('rtoDays','dias');
			} else{
				$traMessage = FWDLanguage::getPHPStringValue('rtoMonths','meses');
			}
		}else{
			$assetTRA = '-';
			$traMessage = '-';
		}
		// Regra de Cores
		if ($isaTra < 1){
			$cor = "<font color=#FF0000>";
		} else if ($isaTra > 1){
			$cor = "<font color=#66CC00>";
		} else {
			$cor = "<font color=#FFCC00>";
		}
		$moWebLib->getObject('asset_tra')->setValue($cor . $isaTra . ' ' . '('.$assetTRA . ' ' . $traMessage . ')' . "</font>");
		
		$tacMessage = '';
		if($assetTAC != 0){
			if($tacFlag == 1){
				$tacMessage = FWDLanguage::getPHPStringValue('rtoMinutes','minutos');
			} else if($tacFlag == 2){
				$tacMessage = FWDLanguage::getPHPStringValue('rtoHours','horas');
			} else if($tacFlag == 3){
				$tacMessage = FWDLanguage::getPHPStringValue('rtoDays','dias');
			} else{
				$tacMessage = FWDLanguage::getPHPStringValue('rtoMonths','meses');
			}
			
		}else{
			$assetTAC = '-';
			$tacMessage = '-';
			if ($isaTac == ''){
				$isaTac = '-';
			}
		}
		
		// Regra de Cores
		if ($isaTac < 1){
			$cor = "<font color=#FF0000>";
		} else if ($isaTac > 1){
			$cor = "<font color=#66CC00>";
		} else {
			$cor = "<font color=#FFCC00>";
		}
		$moWebLib->getObject('asset_tac')->setValue($cor . $isaTac . ' ' . '('.$assetTAC . ' ' . $tacMessage . ')' . "</font>");
		
		
	}
	
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){	  
    $moWebLib = FWDWebLib::getInstance();
		$moFilter = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter();

		FWDWebLib::getInstance()->xml_load("popup_report_asa_by_processes.xml", false);
		$moWebLib->getObject("process_level")->setLevelIterator(new ProcessLevelIterator());
    	$moWebLib->getObject("asset_level")->setLevelIterator(new AssetLevelIterator());		
		
		$moComponent = new FWDReportGenerator();
		$moComponent->setReport($moWebLib->getObject('report'));
		$moComponent->setReportTemplate(new ISMSReportTemplate());
		$moComponent->setReportFilter($moFilter);
		$moComponent->setReportName(FWDLanguage::getPHPStringValue('asa_by_process', "ASA X Processos"));
		$moComponent->generate();	  
	  
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
/*
 * Ao inv�s do xml_load ser chamado aqui, ele � chamado
 * baseado no filtro do relat�rio que o usu�rio escolheu.
 * Com isso, � necess�rio chamar a fun��o start() manualmente.
 */
FWDStartEvent::getInstance()->start();
?>