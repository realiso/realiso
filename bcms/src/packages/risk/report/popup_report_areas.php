<?php

set_time_limit(3600);
include_once 'include.php';

class AreaLevelIterator extends FWDReportLevelIterator {

	public function __construct(){
		parent::__construct('area_name');
	}

	public function fetch(FWDDBDataSet $poDataSet){
		FWDWebLib::getObject('area_name')->setValue($poDataSet->getFieldByAlias('area_name')->getValue());
	}

}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(''));
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$moFilter = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter();

		FWDWebLib::getObject('area_level')->setLevelIterator(new AreaLevelIterator());

		$moComponent = new FWDReportGenerator();
		$moComponent->setReport(FWDWebLib::getObject('report'));
		$moComponent->setReportTemplate(new ISMSReportTemplate());

		$moComponent->setReportFilter($moFilter);

		$moComponent->setReportName(FWDLanguage::getPHPStringValue('reportAreas','Unidades de Neg�cio'));
		$moComponent->generate();
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_report_areas.xml');

?>