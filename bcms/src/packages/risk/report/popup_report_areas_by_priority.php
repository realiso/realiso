<?php
set_time_limit(3600);
include_once "include.php";

class PriorityLevelIterator extends FWDReportLevelIterator {  

  public function __construct(){
    parent::__construct("class_id");
  }
	
  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();    
    $moWebLib->getObject('priority_name')->setValue($poDataSet->getFieldByAlias("class_name")->getValue());
  }
}

class AreaLevelIterator extends FWDReportLevelIterator {

  public function __construct(){
    parent::__construct("area_id");
  }

  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msGfxRef = $moWebLib->getGfxRef();

    $miAreaValue = $poDataSet->getFieldByAlias("area_value")->getValue();		
		$msIcon = $msGfxRef . 'icon-area_' . RMRiskConfig::getRiskColor($miAreaValue) . '.png';   

    $moWebLib->getObject('area_icon')->setAttrSrc($msIcon);
    $moWebLib->getObject('area_name')->setValue($poDataSet->getFieldByAlias("area_name")->getValue());
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();    
    $moWebLib->getObject("level_priority")->setLevelIterator(new PriorityLevelIterator());
    $moWebLib->getObject("level_area")->setLevelIterator(new AreaLevelIterator());
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport($moWebLib->getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    $moComponent->setReportFilter($moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter());
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('rs_areas_by_priority', "�reas por Prioridade"));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_report_areas_by_priority.xml");
?>