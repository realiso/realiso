<?php
set_time_limit(3600);
include_once "include.php";


class ProcessLevelIterator extends FWDReportLevelIterator {

	public function __construct(){
		parent::__construct("process_name");
	}

	public function fetch(FWDDBDataSet $poDataSet){
		$moWebLib = FWDWebLib::getInstance();	
		
		$processName = $poDataSet->getFieldByAlias("process_name")->getValue();
		$processUN = $poDataSet->getFieldByAlias("area_name")->getValue();
		
		$moWebLib->getObject('process_name')->setValue($processName);
		$moWebLib->getObject('area_name')->setValue($processUN);
		
	}
}

class ProcessesDependenceLevelIterator extends FWDReportLevelIterator {
	public function __construct(){
		parent::__construct("process_input_name");
	}

	public function fetch(FWDDBDataSet $poDataSet){
		$moWebLib = FWDWebLib::getInstance();	
		
		$ProcessDependence = $poDataSet->getFieldByAlias("process_input_name")->getValue();				
		$moWebLib->getObject('process_input_name')->setValue($ProcessDependence);
	}
	
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){	  
    $moWebLib = FWDWebLib::getInstance();
		$moFilter = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter();

		FWDWebLib::getInstance()->xml_load("popup_report_processes_dependence.xml", false);
		$moWebLib->getObject("process_level")->setLevelIterator(new ProcessLevelIterator());
    	$moWebLib->getObject("processes_dependence")->setLevelIterator(new ProcessesDependenceLevelIterator());		
		
		$moComponent = new FWDReportGenerator();
		$moComponent->setReport($moWebLib->getObject('report'));
		$moComponent->setReportTemplate(new ISMSReportTemplate());
		$moComponent->setReportFilter($moFilter);
		$moComponent->setReportName(FWDLanguage::getPHPStringValue('process_dependence', "Depend�ncia de Processos"));
		$moComponent->generate();	  
	  
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
/*
 * Ao inv�s do xml_load ser chamado aqui, ele � chamado
 * baseado no filtro do relat�rio que o usu�rio escolheu.
 * Com isso, � necess�rio chamar a fun��o start() manualmente.
 */
FWDStartEvent::getInstance()->start();
?>