<?php
set_time_limit(3600);
include_once "include.php";


class AssetLevelIterator extends FWDReportLevelIterator {

	public function __construct(){
		parent::__construct("assetName");
	}

	public function fetch(FWDDBDataSet $poDataSet){
		$moWebLib = FWDWebLib::getInstance();

		$assetName = $poDataSet->getFieldByAlias("assetName")->getValue();
		$assetAlias = $poDataSet->getFieldByAlias("assetAlias")->getValue();
		$assetModel = $poDataSet->getFieldByAlias("assetModel")->getValue();
		$assetManufacturer = $poDataSet->getFieldByAlias("assetManufacturer")->getValue();
		$assetVersion = $poDataSet->getFieldByAlias("assetVersion")->getValue();
		$assetBuild = $poDataSet->getFieldByAlias("assetBuild")->getValue();
		$assetSerial = $poDataSet->getFieldByAlias("assetSerial")->getValue();
		$assetIp = $poDataSet->getFieldByAlias("assetIp")->getValue();
		$assetMac = $poDataSet->getFieldByAlias("assetMac")->getValue();
		$providerName = $poDataSet->getFieldByAlias("providerName")->getValue();

		$moWebLib->getObject('assetName')->setValue($assetName);
		$moWebLib->getObject('assetAlias')->setValue($assetAlias);
		$moWebLib->getObject('assetModel')->setValue($assetModel);
		$moWebLib->getObject('assetManufacturer')->setValue($assetManufacturer);
		$moWebLib->getObject('assetVersion')->setValue($assetVersion);
		$moWebLib->getObject('assetBuild')->setValue($assetBuild);
		$moWebLib->getObject('assetSerial')->setValue($assetSerial);
		$moWebLib->getObject('assetIp')->setValue($assetIp);
		$moWebLib->getObject('assetMac')->setValue($assetMac);
		$moWebLib->getObject('providerName')->setValue($providerName);


	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$moWebLib = FWDWebLib::getInstance();
		$moFilter = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter();

		FWDWebLib::getInstance()->xml_load("popup_report_asset.xml", false);
		$moWebLib->getObject("level_asset")->setLevelIterator(new AssetLevelIterator());

		$moComponent = new FWDReportGenerator();
		$moComponent->setReport($moWebLib->getObject('report'));
		$moComponent->setReportTemplate(new ISMSReportTemplate());
		$moComponent->setReportFilter($moFilter);
		$moComponent->setReportName(FWDLanguage::getPHPStringValue('reportAssetTitle', "Atividades"));
		$moComponent->generate();
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
/*
 * Ao inv�s do xml_load ser chamado aqui, ele � chamado
 * baseado no filtro do relat�rio que o usu�rio escolheu.
 * Com isso, � necess�rio chamar a fun��o start() manualmente.
 */
FWDStartEvent::getInstance()->start();
?>