<?php
set_time_limit(3600);
include_once "include.php";

class AssetLevelIterator extends FWDReportLevelIterator {

	public function __construct(){
		parent::__construct("asset_name");
	}

	public function fetch(FWDDBDataSet $poDataSet){
		$moWebLib = FWDWebLib::getInstance();
		
		$assetName = $poDataSet->getFieldByAlias("asset_name")->getValue();
		$assetDescription = $poDataSet->getFieldByAlias("asset_description")->getValue();
	  $assetTRA = $poDataSet->getFieldByAlias("asset_tra")->getValue();
		$assetTAC = $poDataSet->getFieldByAlias("asset_tac")->getValue();
		$processRTO = $poDataSet->getFieldByAlias("process_rto")->getValue();
		$processRanking = $poDataSet->getFieldByAlias("process_ranking")->getValue();

		$moWebLib->getObject('asset_name')->setValue($assetName);
		$moWebLib->getObject('asset_description')->setValue($assetDescription);
		$moWebLib->getObject('asset_tra')->setValue($assetTRA);
		$moWebLib->getObject('asset_tac')->setValue($assetTAC);
		$moWebLib->getObject('process_rto')->setValue($processRTO);
		$moWebLib->getObject('process_ranking')->setValue($processRanking);	
		
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$moWebLib = FWDWebLib::getInstance();
		$moFilter = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter();

		FWDWebLib::getInstance()->xml_load("popup_report_asset_matrix.xml", false);
		$moWebLib->getObject("level_asset")->setLevelIterator(new AssetLevelIterator());

		$moComponent = new FWDReportGenerator();
		$moComponent->setReport($moWebLib->getObject("report"));
		$moComponent->setReportTemplate(new ISMSReportTemplate());
		$moComponent->setReportFilter($moFilter);
		$moComponent->setReportName(FWDLanguage::getPHPStringValue('reportAssetMatrixName',"Matriz de Recupera��o de Ativos"));
		
		$moComponent->generate();
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
/*
 * Ao inv�s do xml_load ser chamado aqui, ele � chamado
 * baseado no filtro do relat�rio que o usu�rio escolheu.
 * Com isso, � necess�rio chamar a fun��o start() manualmente.
 */
FWDStartEvent::getInstance()->start();
?>