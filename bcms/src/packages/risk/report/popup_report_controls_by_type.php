<?php
set_time_limit(3600);
include_once "include.php";

class TypeLevelIterator extends FWDReportLevelIterator {  

  public function __construct(){
    parent::__construct("class_id");
  }
	
  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();    
    $moWebLib->getObject('type_name')->setValue($poDataSet->getFieldByAlias("class_name")->getValue());
  }
}

class ControlLevelIterator extends FWDReportLevelIterator {

  public function __construct(){
    parent::__construct("control_id");
  }

  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msGfxRef = $moWebLib->getGfxRef();
    		
		$mbControlIsActive = $poDataSet->getFieldByAlias("control_value")->getValue();		
		$msIcon = $msGfxRef;    
    if ($mbControlIsActive) $msIcon .= 'icon-control.png';
    else $msIcon .= 'icon-control_red.png';		

    $moWebLib->getObject('control_icon')->setAttrSrc($msIcon);
    $moWebLib->getObject('control_name')->setValue($poDataSet->getFieldByAlias("control_name")->getValue());
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();    
    $moWebLib->getObject("level_type")->setLevelIterator(new TypeLevelIterator());
    $moWebLib->getObject("level_control")->setLevelIterator(new ControlLevelIterator());
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport($moWebLib->getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    $moComponent->setReportFilter($moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter());
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('rs_controls_by_type', "Controles por Tipo"));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_report_controls_by_type.xml");
?>