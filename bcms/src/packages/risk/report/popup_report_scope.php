<?php
set_time_limit(3600);
include_once "include.php";

class ScopeLevelIterator extends FWDReportLevelIterator {

	public function __construct(){
		parent::__construct("scopeName");
	}

	public function fetch(FWDDBDataSet $poDataSet){
		$moWebLib = FWDWebLib::getInstance();

		$scopeName = $poDataSet->getFieldByAlias("scopeName")->getValue();
		$scopeDescription = $poDataSet->getFieldByAlias("scopeDescription")->getValue();

		$moWebLib->getObject('scopeDescription')->setValue($scopeDescription);
		$moWebLib->getObject('scopeName')->setValue($scopeName);
	}
}

class PlaceLevelIterator extends FWDReportLevelIterator {

	public function __construct(){
		parent::__construct("placeName");
	}

	public function fetch(FWDDBDataSet $poDataSet){
		$moWebLib = FWDWebLib::getInstance();

		$placeName = $poDataSet->getFieldByAlias("placeName")->getValue();
		$placeId = $poDataSet->getFieldByAlias("placeId")->getValue();

		$placeLable = FWDLanguage::getPHPStringValue('scopeReportPlace', "Local: ");
		$value = "<b>{$placeLable}</b>".$placeName;

		/*
		 * 	left join view_cm_place_resource_active cmtPlace on cmtPlace.cmt = 1 and cmtPlace.fkplace = place.fkcontext
			left join view_cm_group_resource_active cmtGroupResource on cmtGroupResource.fkgroup = cmtPlace.fkgroup
			left join view_cm_resource_active cmtResources on cmtResources.fkcontext = cmtGroupResource.fkresource
			*/

		$query = new FWDDBDataSet(FWDWebLib::getConnection());
		$query->setQuery("	select
								cmtResources.name as cmtResourceName,
								comercialNumber,
								celNumber,
								homeNumber,
					 			nextelNumber,
					 			email
							from 
								view_cm_place_resource_active cmtPlace
								join view_cm_group_resource_active cmtGroupResource on cmtGroupResource.fkgroup = cmtPlace.fkgroup
								join view_cm_resource_active cmtResources on cmtResources.fkcontext = cmtGroupResource.fkresource
							where
								cmtPlace.cmt = 1 and cmtPlace.fkplace = {$placeId} ");
		$query->addFWDDBField(new FWDDBField('cmtResourceName'	,'cmtResourceName'  ,DB_STRING));
		$query->addFWDDBField(new FWDDBField('comercialNumber'	,'comercialNumber'  ,DB_STRING));
		$query->addFWDDBField(new FWDDBField('celNumber'		,'celNumber'  		,DB_STRING));
		$query->addFWDDBField(new FWDDBField('homeNumber'		,'homeNumber'  		,DB_STRING));
		$query->addFWDDBField(new FWDDBField('nextelNumber'		,'nextelNumber'  	,DB_STRING));
		$query->addFWDDBField(new FWDDBField('email'			,'email'  			,DB_STRING));

		$query->execute();
		$first = true;
		while($query->fetch()){
			if($first){
				$value .= " <br/><b>CMT</b>";
				$first = false;
			}
			$name = $query->getFieldByAlias("cmtResourceName")->getValue();
			$comercialNumber = $query->getFieldByAlias("comercialNumber")->getValue();
			$celNumber = $query->getFieldByAlias("celNumber")->getValue();
			$homeNumber = $query->getFieldByAlias("homeNumber")->getValue();
			$nextelNumber = $query->getFieldByAlias("nextelNumber")->getValue();
			$email = $query->getFieldByAlias("email")->getValue();

			$value .= " <br/>".$name;

			if($comercialNumber){
				$value .= ", ".$comercialNumber;
			}

			if($celNumber){
				$value .= ", ".$celNumber;
			}

			if($homeNumber){
				$value .= ", ".$homeNumber;
			}

			if($nextelNumber){
				$value .= ", ".$nextelNumber;
			}

			if($email){
				$value .= ", ".$email;
			}

		}

		$query = new FWDDBDataSet(FWDWebLib::getConnection());
		$query->setQuery("	select
								resources.name as resourceName,
								comercialNumber,
								celNumber,
								homeNumber,
					 			nextelNumber,
					 			email
							from 
								view_cm_place_resource_active placeResource
								join view_cm_resource_active resources on resources.fkcontext = placeResource.fkresource
							where
								placeResource.cmt = 0 and placeResource.fkplace = {$placeId} ");

		$query->addFWDDBField(new FWDDBField('resourceName'		,'resourceName'  	,DB_STRING));
		$query->addFWDDBField(new FWDDBField('comercialNumber'	,'comercialNumber'  ,DB_STRING));
		$query->addFWDDBField(new FWDDBField('celNumber'		,'celNumber'  		,DB_STRING));
		$query->addFWDDBField(new FWDDBField('homeNumber'		,'homeNumber'  		,DB_STRING));
		$query->addFWDDBField(new FWDDBField('nextelNumber'		,'nextelNumber'  	,DB_STRING));
		$query->addFWDDBField(new FWDDBField('email'			,'email'  			,DB_STRING));

		$query->execute();
		$first = true;
		while($query->fetch()){
			if($first){
				$value .= " <br/><b>Contatos do Local</b>";
				$first = false;
			}
			$name = $query->getFieldByAlias("resourceName")->getValue();
			$comercialNumber = $query->getFieldByAlias("comercialNumber")->getValue();
			$celNumber = $query->getFieldByAlias("celNumber")->getValue();
			$homeNumber = $query->getFieldByAlias("homeNumber")->getValue();
			$nextelNumber = $query->getFieldByAlias("nextelNumber")->getValue();
			$email = $query->getFieldByAlias("email")->getValue();

			$value .= " <br/>".$name;

			if($comercialNumber){
				$value .= ", ".$comercialNumber;
			}

			if($celNumber){
				$value .= ", ".$celNumber;
			}

			if($homeNumber){
				$value .= ", ".$homeNumber;
			}

			if($nextelNumber){
				$value .= ", ".$nextelNumber;
			}

			if($email){
				$value .= ", ".$email;
			}

		}

		$query = new FWDDBDataSet(FWDWebLib::getConnection());
		$query->setQuery("	select
								resources.name as resourceName,
								provider.name as provider,
								comercialNumber,
								celNumber,
								homeNumber,
					 			nextelNumber,
					 			email
							from 
								view_cm_place_provider_active placeProvider
								join view_cm_provider_resource_active providerResource on providerResource.fkprovider = placeProvider.fkprovider 
								join view_cm_provider_active provider on provider.fkcontext = providerResource.fkprovider
								join view_cm_resource_active resources on resources.fkcontext = providerResource.fkresource
							where
								placeProvider.fkplace = {$placeId} ");

		$query->addFWDDBField(new FWDDBField('resourceName'		,'resourceName'  	,DB_STRING));
		$query->addFWDDBField(new FWDDBField('provider'			,'provider'  		,DB_STRING));
		$query->addFWDDBField(new FWDDBField('comercialNumber'	,'comercialNumber'  ,DB_STRING));
		$query->addFWDDBField(new FWDDBField('celNumber'		,'celNumber'  		,DB_STRING));
		$query->addFWDDBField(new FWDDBField('homeNumber'		,'homeNumber'  		,DB_STRING));
		$query->addFWDDBField(new FWDDBField('nextelNumber'		,'nextelNumber'  	,DB_STRING));
		$query->addFWDDBField(new FWDDBField('email'			,'email'  			,DB_STRING));

		$query->execute();
		$lastProvider = "";
		while($query->fetch()){
			$provider = $query->getFieldByAlias("provider")->getValue();
			if($provider != $lastProvider){
				$lastProvider = $provider;
				$value .= " <br/><b>Contatos Externos: </b>".$lastProvider;
			}
			$name = $query->getFieldByAlias("resourceName")->getValue();
			$comercialNumber = $query->getFieldByAlias("comercialNumber")->getValue();
			$celNumber = $query->getFieldByAlias("celNumber")->getValue();
			$homeNumber = $query->getFieldByAlias("homeNumber")->getValue();
			$nextelNumber = $query->getFieldByAlias("nextelNumber")->getValue();
			$email = $query->getFieldByAlias("email")->getValue();

			$value .= " <br/>".$name;

			if($comercialNumber){
				$value .= ", ".$comercialNumber;
			}

			if($celNumber){
				$value .= ", ".$celNumber;
			}

			if($homeNumber){
				$value .= ", ".$homeNumber;
			}

			if($nextelNumber){
				$value .= ", ".$nextelNumber;
			}

			if($email){
				$value .= ", ".$email;
			}

		}

		//		$cmtResourceName = $poDataSet->getFieldByAlias("cmtResourceName")->getValue();
		//		if($cmtResourceName){
		//			//			$responsible = FWDLanguage::getPHPStringValue('scopeReportAreaResponsible', "Respons�vel: ");
		//			$value .= " <br/>".$cmtResourceName;
		//		}
		//
		//		$areaSubstitute = $poDataSet->getFieldByAlias("areaSubstitute")->getValue();
		//		if($areaSubstitute){
		//			$substitute = FWDLanguage::getPHPStringValue('scopeReportAreaSubstitute', "Substituto: ");
		//			$value .= " <br/>{$substitute}".$areaSubstitute;
		//		}
		//
		//		$moWebLib->getObject('areaName')->setValue($value);

		$moWebLib->getObject('placeName')->setValue($value);
	}
}

class ProcessLevelIterator extends FWDReportLevelIterator {

	public function __construct(){
		parent::__construct("processName");
	}

	public function fetch(FWDDBDataSet $poDataSet){
		$moWebLib = FWDWebLib::getInstance();

		$processName = $poDataSet->getFieldByAlias("processName")->getValue();

		$moWebLib->getObject('processName')->setValue($processName);
	}
}

class AreaLevelIterator extends FWDReportLevelIterator {

	public function __construct(){
		parent::__construct("areaName");
	}

	public function fetch(FWDDBDataSet $poDataSet){
		$moWebLib = FWDWebLib::getInstance();

		$areaName = $poDataSet->getFieldByAlias("areaName")->getValue();

		$un = FWDLanguage::getPHPStringValue('scopeReportArea', "Unidade de Neg�cio: ");
		$value = "<b>{$un}</b>".$areaName;

		$areaResponsible = $poDataSet->getFieldByAlias("areaResponsible")->getValue();
		if($areaResponsible){
			$responsible = FWDLanguage::getPHPStringValue('scopeReportAreaResponsible', "Respons�vel: ");
			$value .= " <br/>{$responsible}".$areaResponsible;
		}

		$areaSubstitute = $poDataSet->getFieldByAlias("areaSubstitute")->getValue();
		if($areaSubstitute){
			$substitute = FWDLanguage::getPHPStringValue('scopeReportAreaSubstitute', "Substituto: ");
			$value .= " <br/>{$substitute}".$areaSubstitute;
		}

		$moWebLib->getObject('areaName')->setValue($value);
	}
}

class AssetPlanLevelIterator extends FWDReportLevelIterator {
	public function __construct(){
		parent::__construct("assetPlan");
	}
	public function fetch(FWDDBDataSet $poDataSet){
		$moWebLib = FWDWebLib::getInstance();
		$planName = $poDataSet->getFieldByAlias("assetPlan")->getValue();
		$moWebLib->getObject('assetPlanName')->setValue($planName);
	}
}

class ProcessPlanLevelIterator extends FWDReportLevelIterator {
	public function __construct(){
		parent::__construct("processPlan");
	}
	public function fetch(FWDDBDataSet $poDataSet){
		$moWebLib = FWDWebLib::getInstance();
		$planName = $poDataSet->getFieldByAlias("processPlan")->getValue();
		$moWebLib->getObject('processPlanName')->setValue($planName);
	}
}

class AssetLevelIterator extends FWDReportLevelIterator {

	public function __construct(){
		parent::__construct("assetName");
	}

	public function fetch(FWDDBDataSet $poDataSet){
		$moWebLib = FWDWebLib::getInstance();

		$assetName = $poDataSet->getFieldByAlias("assetName")->getValue();
		$assetResponsible = $poDataSet->getFieldByAlias("assetResponsible")->getValue();

		$moWebLib->getObject('assetResponsible')->setValue($assetResponsible);
		$moWebLib->getObject('assetName')->setValue($assetName);
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$moWebLib = FWDWebLib::getInstance();
		$moFilter = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter();

		FWDWebLib::getInstance()->xml_load("popup_report_scope.xml", false);
		$moWebLib->getObject("level_scope")->setLevelIterator(new ScopeLevelIterator());
		$moWebLib->getObject("level_place")->setLevelIterator(new PlaceLevelIterator());
		$moWebLib->getObject("level_area")->setLevelIterator(new AreaLevelIterator());
		//		$moWebLib->getObject("level_asset_plan")->setLevelIterator(new AssetPlanLevelIterator());
		//		$moWebLib->getObject("level_process_plan")->setLevelIterator(new ProcessPlanLevelIterator());
		$moWebLib->getObject("level_process")->setLevelIterator(new ProcessLevelIterator());
		$moWebLib->getObject("level_asset")->setLevelIterator(new AssetLevelIterator());

		$moComponent = new FWDReportGenerator();
		$moComponent->setReport($moWebLib->getObject('report'));
		$moComponent->setReportTemplate(new ISMSReportTemplate());
		$moComponent->setReportFilter($moFilter);
		$moComponent->setReportName(FWDLanguage::getPHPStringValue('scopeReportName', "Declara��o de Escopo"));
		$moComponent->generate();
	}
} 

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
/*
 * Ao inv�s do xml_load ser chamado aqui, ele � chamado
 * baseado no filtro do relat�rio que o usu�rio escolheu.
 * Com isso, � necess�rio chamar a fun��o start() manualmente.
 */
FWDStartEvent::getInstance()->start();
?>