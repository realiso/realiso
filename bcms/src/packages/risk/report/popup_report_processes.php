<?php
set_time_limit(3600);
include_once "include.php";

class ProcessLevelIterator extends FWDReportLevelIterator {

	public function __construct(){
		parent::__construct("name");
	}

	public function fetch(FWDDBDataSet $poDataSet){
		$moWebLib = FWDWebLib::getInstance();

		$name = $poDataSet->getFieldByAlias("name")->getValue();
		$type = $poDataSet->getFieldByAlias("type")->getValue();
		$priority = $poDataSet->getFieldByAlias("priority")->getValue();

		$moWebLib->getObject('name')->setValue($name);
		$moWebLib->getObject('type')->setValue($type);
		$moWebLib->getObject('priority')->setValue($priority);
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$moWebLib = FWDWebLib::getInstance();
		$moFilter = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter();

		$moWebLib->xml_load("popup_report_processes.xml", false);
		$moWebLib->getObject("level_process")->setLevelIterator(new ProcessLevelIterator());

		$moComponent = new FWDReportGenerator();
		$moComponent->setReport($moWebLib->getObject('report'));
		$moComponent->setReportTemplate(new ISMSReportTemplate());
		$moComponent->setReportFilter($moFilter);
		$moComponent->setReportName(FWDLanguage::getPHPStringValue('reportProcesses', "Processos"));
		$moComponent->generate();
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDStartEvent::getInstance()->start();
?>