<?php

set_time_limit(3600);
include_once "include.php";

class ControlLevelIterator extends FWDReportLevelIterator {

  public function __construct(){
    parent::__construct("control_id");
  }
  
  public function fetch(FWDDBDataSet $poDataSet){
    $msControlName = $poDataSet->getFieldByAlias('control_name')->getValue();
    $mbControlIsActive = $poDataSet->getFieldByAlias('control_is_active')->getValue();
    $miScheduleId = $poDataSet->getFieldByAlias('schedule_id')->getValue();
    
    $moSchedule = new WKFSchedule();
    $moSchedule->fetchById($miScheduleId);
    
    $msControlNextRevision = FWDWebLib::getShortDate($moSchedule->getFieldValue('schedule_next_occurrence'));
    $msSchedule = $moSchedule->getTextDescription();
    
    if($mbControlIsActive){
      $msIcon = FWDWebLib::getInstance()->getGfxRef().'icon-control.png';
    }else{
      $msIcon = FWDWebLib::getInstance()->getGfxRef().'icon-control_red.png';
    }
    
    FWDWebLib::getObject('control_icon')->setAttrSrc($msIcon);
    FWDWebLib::getObject('control_name')->setValue($msControlName);
    FWDWebLib::getObject('control_next_revision')->setValue($msControlNextRevision);
    FWDWebLib::getObject('control_schedule')->setValue($msSchedule);
  }

}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $moWebLib->getObject("level_control")->setLevelIterator(new ControlLevelIterator());    
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport($moWebLib->getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    $moComponent->setReportFilter($moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter());
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('rs_follow_up_of_controls', "Acompanhamento de Controles"));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_report_control_accompaniment.xml");

?>