<?php

set_time_limit(3600);
include_once 'include.php';

class ControlLevelIterator extends FWDReportLevelIterator {
  
  protected $ciTotal = 0;
  
  public function __construct(){
    parent::__construct('control_id');
  }
  
  public function fetch(FWDDBDataSet $poDataSet){
  	$moConfig = new ISMSConfig();
    $msCurrency = constant($moConfig->getConfig(GENERAL_CURRENCY_IDENTIFIER));
    if (!$msCurrency) $msCurrency = '$';
    if($poDataSet->getFieldByAlias('control_is_active')->getValue()){
      $msIconSrc = FWDWebLib::getInstance()->getGfxRef()."icon-control.gif";
    }else{
      $msIconSrc = FWDWebLib::getInstance()->getGfxRef()."icon-control_red.gif";
    }
    $miControlCost = $poDataSet->getFieldByAlias('control_cost')->getValue();
    $this->ciTotal+= $miControlCost;
    
    FWDWebLib::getObject('control_id')->setAttrSrc($msIconSrc);
    FWDWebLib::getObject('control_name')->setValue($poDataSet->getFieldByAlias('control_name')->getValue());
    FWDWebLib::getObject('control_cost')->setValue("$msCurrency ".number_format($miControlCost,2,'.',','));
    
    FWDWebLib::getObject('total_cost')->setValue("$msCurrency ".number_format($this->ciTotal,2,'.',','));
  }
  
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moFilter = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter();
    
    FWDWebLib::getObject('control_level')->setLevelIterator(new ControlLevelIterator());
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport(FWDWebLib::getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    
    $moComponent->setReportFilter($moFilter);
    
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('mx_control_cost_by_cost_category','Custos de Controles por Categoria de Custo'));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_report_control_cost_by_cost_category.xml');

?>