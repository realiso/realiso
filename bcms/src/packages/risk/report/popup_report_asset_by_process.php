<?php
set_time_limit(3600);
include_once "include.php";

class PlaceLevelIterator extends FWDReportLevelIterator {

	public function __construct(){
		parent::__construct("asset_place");
	}

	public function fetch(FWDDBDataSet $poDataSet){
		$moWebLib = FWDWebLib::getInstance();

		$assetPlace = $poDataSet->getFieldByAlias("asset_place")->getValue();
		
		$moWebLib->getObject('place')->setValue($assetPlace);
	}
}

class AreaLevelIterator extends FWDReportLevelIterator {

	public function __construct(){
		parent::__construct("area");
	}

	public function fetch(FWDDBDataSet $poDataSet){
		$moWebLib = FWDWebLib::getInstance();

		$assetArea = $poDataSet->getFieldByAlias("area")->getValue();
		
		$moWebLib->getObject('area')->setValue($assetArea);
	}
}

class AssetLevelIterator extends FWDReportLevelIterator {

	public function __construct(){
		parent::__construct("asset_name");
	}

	public function fetch(FWDDBDataSet $poDataSet){
		$moWebLib = FWDWebLib::getInstance();

		$assetName = $poDataSet->getFieldByAlias("asset_name")->getValue();
		
		$moWebLib->getObject('asset')->setValue($assetName);
	}
}

class ProcessLevelIterator extends FWDReportLevelIterator {

	public function __construct(){
		parent::__construct("process_name");
	}

	public function fetch(FWDDBDataSet $poDataSet){
		$moWebLib = FWDWebLib::getInstance();

		$assetName = $poDataSet->getFieldByAlias("asset_name")->getValue();
		$processName = $poDataSet->getFieldByAlias("process_name")->getValue();
		$processImportance = $poDataSet->getFieldByAlias("importance")->getValue();
		$processRanking = $poDataSet->getFieldByAlias("process_ranking")->getValue();
		$processRTO = $poDataSet->getFieldByAlias("process_rto")->getValue();
		
		$moWebLib->getObject('asset')->setValue($assetName);
		$moWebLib->getObject('process')->setValue($processName);
		$moWebLib->getObject('processImportance')->setValue($processImportance);
		$moWebLib->getObject('processRanking')->setValue($processRanking);
		$moWebLib->getObject('processRTO')->setValue($processRTO);
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$moWebLib = FWDWebLib::getInstance();
		$moFilter = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter();

		$moWebLib->xml_load("popup_report_asset_by_process.xml", false);
		
		$moWebLib->getObject("level_place")->setLevelIterator(new PlaceLevelIterator());
		$moWebLib->getObject("level_area")->setLevelIterator(new AreaLevelIterator());
		$moWebLib->getObject("level_asset")->setLevelIterator(new AssetLevelIterator());
		$moWebLib->getObject("level_process")->setLevelIterator(new ProcessLevelIterator());

		$moComponent = new FWDReportGenerator();
		$moComponent->setReport($moWebLib->getObject("report"));
		$moComponent->setReportTemplate(new ISMSReportTemplate());
		$moComponent->setReportFilter($moFilter);
		$moComponent->setReportName(FWDLanguage::getPHPStringValue("assetByProcess", "Ativos X Processos"));
		$moComponent->generate();
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDStartEvent::getInstance()->start();
?>