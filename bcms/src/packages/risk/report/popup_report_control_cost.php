<?php
set_time_limit(3600);
include_once "include.php";

class ControlLevelIterator extends FWDReportLevelIterator {  

  public function __construct(){
    parent::__construct("control_id");
  }
	
  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msGfxRef = $moWebLib->getGfxRef();
   
    $msControlName = $poDataSet->getFieldByAlias("control_name")->getValue();    
		$mbControlIsActive = $poDataSet->getFieldByAlias("control_is_active")->getValue();	
		
		$msIcon = $msGfxRef;    
    if ($mbControlIsActive) $msIcon .= 'icon-control.png';
    else $msIcon .= 'icon-control_red.png';

    $moWebLib->getObject('control_icon')->setAttrSrc($msIcon);    
    $moWebLib->getObject('control_name')->setValue($msControlName);    
  }
}

class CostLevelIterator extends FWDReportLevelIterator {

  public function __construct(){
    parent::__construct("control_cost_id");
  }

  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msGfxRef = $moWebLib->getGfxRef();
    $moConfig = new ISMSConfig();
    $msCurrency = constant($moConfig->getConfig(GENERAL_CURRENCY_IDENTIFIER));
    if (!$msCurrency) $msCurrency = '$';

    $miControlCostId = $poDataSet->getFieldByAlias("control_cost_id")->getValue();		
		$miControlCost = $poDataSet->getFieldByAlias("control_cost")->getValue();
		$msControlCostName = ISMSLib::getConfigById($miControlCostId);
		
    $moWebLib->getObject('control_cost_name')->setValue($msControlCostName.":");    
    $moWebLib->getObject('control_cost')->setValue($msCurrency.number_format($miControlCost,2,'.',','));
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();    
    $moWebLib->getObject("level_control")->setLevelIterator(new ControlLevelIterator());
    $moWebLib->getObject("level_cost")->setLevelIterator(new CostLevelIterator());
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport($moWebLib->getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    $moComponent->setReportFilter($moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter());
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('rs_cost_per_control',"Custo por Controle"));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_report_control_cost.xml");
?>