<?php
set_time_limit(3600);
include_once "include.php";

class PlaceLevelIterator extends FWDReportLevelIterator {

	public function __construct(){
		parent::__construct("place_name");
	}

	public function fetch(FWDDBDataSet $poDataSet){
		$moWebLib = FWDWebLib::getInstance();
		$placeName= $poDataSet->getFieldByAlias("place_name")->getValue();
		$moWebLib->getObject('placeName')->setValue($placeName);
	}
}

class ProcessLevelIterator extends FWDReportLevelIterator {

	public function __construct(){
		parent::__construct("name");
	}

	public function fetch(FWDDBDataSet $poDataSet){
		$moWebLib = FWDWebLib::getInstance();

		$processName = $poDataSet->getFieldByAlias("name")->getValue();
		$ranking = $poDataSet->getFieldByAlias("ranking")->getValue();
		//$financialloss = $poDataSet->getFieldByAlias("financialloss")->getValue();
		$rto = $poDataSet->getFieldByAlias("rto")->getValue();
		$area= $poDataSet->getFieldByAlias("area")->getValue();
		

		$moWebLib->getObject('process_name')->setValue($processName);
		$moWebLib->getObject('ranking')->setValue($ranking);
		$moWebLib->getObject('rto')->setValue($rto);
		$moWebLib->getObject('area')->setValue($area);
		//$moWebLib->getObject('financialLoss')->setValue($financialloss);
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$moWebLib = FWDWebLib::getInstance();
		$moFilter = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter();

		FWDWebLib::getInstance()->xml_load("popup_report_bia_qualitative.xml", false);
		$moWebLib->getObject("level_place")->setLevelIterator(new PlaceLevelIterator());
		$moWebLib->getObject("level_process")->setLevelIterator(new ProcessLevelIterator());
		
		

		$moComponent = new FWDReportGenerator();
		$moComponent->setReport($moWebLib->getObject('report'));
		$moComponent->setReportTemplate(new ISMSReportTemplate());
		$moComponent->setReportFilter($moFilter);
		$moComponent->setReportName(FWDLanguage::getPHPStringValue('bia_qualitative', "BIA Qualitativo"));
		$moComponent->generate();
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
/*
 * Ao inv�s do xml_load ser chamado aqui, ele � chamado
 * baseado no filtro do relat�rio que o usu�rio escolheu.
 * Com isso, � necess�rio chamar a fun��o start() manualmente.
 */
FWDStartEvent::getInstance()->start();
?>