<?php
set_time_limit(3600);
include_once "include.php";

class TestLevelIterator extends FWDReportLevelIterator {

	public function __construct(){
		parent::__construct("testId");
	}

	public function fetch(FWDDBDataSet $poDataSet){
		$moWebLib = FWDWebLib::getInstance();

		$planName = $poDataSet->getFieldByAlias("planName")->getValue();
		$testObservation = $poDataSet->getFieldByAlias("testObservation")->getValue();
		$testStart = $poDataSet->getFieldByAlias("testStart")->getValue();
		$testEnd = $poDataSet->getFieldByAlias("testEnd")->getValue();

		if($testStart){
			$testStart = ISMSLib::getISMSDate($testStart);
		}

		if($testEnd){
			$testEnd = ISMSLib::getISMSDate($testEnd);
		}

		$moWebLib->getObject('planName')->setValue($planName);
		$moWebLib->getObject('testObservation')->setValue($testObservation);
		$moWebLib->getObject('testStart')->setValue($testStart);
		$moWebLib->getObject('testEnd')->setValue($testEnd);
	}
}

class ActionLevelIterator extends FWDReportLevelIterator {

	public function __construct(){
		parent::__construct("actionId");
	}

	public function fetch(FWDDBDataSet $poDataSet){
		$moWebLib = FWDWebLib::getInstance();

		$actionResume = $poDataSet->getFieldByAlias("actionResume")->getValue();
		$actionGroup = $poDataSet->getFieldByAlias("actionGroup")->getValue();
		$actionStart = $poDataSet->getFieldByAlias("actionStart")->getValue();
		$actionEnd = $poDataSet->getFieldByAlias("actionEnd")->getValue();
		$actionObservation = $poDataSet->getFieldByAlias("actionObservation")->getValue();

		$actionEstimate = $poDataSet->getFieldByAlias("actionEstimate")->getValue();
		$actionEstimateFlag = $poDataSet->getFieldByAlias("actionEstimateFlag")->getValue();
		$multiplier = 1;

		if($actionEstimateFlag == 1){
			$actionEstimateFlag = " Minuto(s)";
		}else if($actionEstimateFlag == 2){
			$multiplier = 60;
			$actionEstimateFlag = " Hora(s)";
		}else if($actionEstimateFlag == 3){
			$multiplier = 60*24;
			$actionEstimateFlag = " Dia(s)";
		}

		if($actionStart && $actionEnd){
			$time = $actionEstimate * $multiplier;
			$diference = (ISMSLib::getTimestamp($actionEnd) - ISMSLib::getTimestamp($actionStart)) / 60;
			if($time > $diference){
				FWDWebLib::getObject('actionIcon')->setAttrSrc(FWDWebLib::getInstance()->getGfxRef() . 'icon-asset_green.png');
			}else{
				FWDWebLib::getObject('actionIcon')->setAttrSrc(FWDWebLib::getInstance()->getGfxRef() . 'icon-asset_red.png');
			}
		}else{
			FWDWebLib::getObject('actionIcon')->setAttrSrc(FWDWebLib::getInstance()->getGfxRef() . 'icon-asset_yellow.png');
		}

		if($actionStart){
			$actionStart = ISMSLib::getISMSDate($actionStart);
		}

		if($actionEnd){
			$actionEnd = ISMSLib::getISMSDate($actionEnd);
		}

		$moWebLib->getObject('actionResume')->setValue($actionResume);
		$moWebLib->getObject('actionGroup')->setValue($actionGroup);
		$moWebLib->getObject('actionStart')->setValue($actionStart);
		$moWebLib->getObject('actionEnd')->setValue($actionEnd);
		$moWebLib->getObject('actionEstimate')->setValue($actionEstimate.$actionEstimateFlag);
		$moWebLib->getObject('actionObservation')->setValue($actionObservation);
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$moWebLib = FWDWebLib::getInstance();
		$moFilter = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter();

		FWDWebLib::getInstance()->xml_load("popup_report_tests.xml", false);
		$moWebLib->getObject("level_test")->setLevelIterator(new TestLevelIterator());
		$moWebLib->getObject("level_action")->setLevelIterator(new ActionLevelIterator());

		$moComponent = new FWDReportGenerator();
		$moComponent->setReport($moWebLib->getObject('report'));
		$moComponent->setReportTemplate(new ISMSReportTemplate());
		$moComponent->setReportFilter($moFilter);
		$moComponent->setReportName(FWDLanguage::getPHPStringValue('reportTestTitle', "Testes"));
		$moComponent->generate();
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
/*
 * Ao inv�s do xml_load ser chamado aqui, ele � chamado
 * baseado no filtro do relat�rio que o usu�rio escolheu.
 * Com isso, � necess�rio chamar a fun��o start() manualmente.
 */
FWDStartEvent::getInstance()->start();
?>