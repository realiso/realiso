<?php
set_time_limit(3600);
include_once "include.php";

class ControlLevelIterator extends FWDReportLevelIterator {  

  public function __construct(){
    parent::__construct("control_id");
  }
	
  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msGfxRef = $moWebLib->getGfxRef();
   
    $msControlName = $poDataSet->getFieldByAlias("control_name")->getValue();    
		$mbControlIsActive = $poDataSet->getFieldByAlias("control_is_active")->getValue();		
		$msControlAccomplishment = $poDataSet->getFieldByAlias("control_date_implemented")->getValue();
		$msControlDescription = $poDataSet->getFieldByAlias("control_description")->getValue();
		$msIcon = $msGfxRef;    
    if ($mbControlIsActive) $msIcon .= 'icon-control.png';
    else $msIcon .= 'icon-control_red.png';

    $moWebLib->getObject('control_icon')->setAttrSrc($msIcon);    
    $moWebLib->getObject('control_name')->setValue($msControlName);
    if($msControlAccomplishment){
      $moWebLib->getObject('control_accomplishment')->setValue(ISMSLib::getISMSShortDate($msControlAccomplishment));
    }else{
      $moWebLib->getObject('control_accomplishment')->setValue('--');
    }
    $moWebLib->getObject('control_description')->setValue($msControlDescription);
    
  }
}


class TypeLevelIterator extends FWDReportLevelIterator {

  public function __construct(){
    parent::__construct("row_type");
  }

  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msGfxRef = $moWebLib->getGfxRef();
    
		if($poDataSet->getFieldByAlias("row_type")->getValue()=="best_practice"){
      FWDWebLib::getObject('level_type_name')->setValue(FWDLanguage::getPHPStringValue('rs_best_practice','Melhor Pr�tica'));
    }else{
      FWDWebLib::getObject('level_type_name')->setValue(FWDLanguage::getPHPStringValue('rs_document','Documento'));
    }
  }
}


class ContextLevelIterator extends FWDReportLevelIterator {

  public function __construct(){
    parent::__construct("context_id");
  }

  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msGfxRef = $moWebLib->getGfxRef();

    $msBestPracticeName = $poDataSet->getFieldByAlias("context_name")->getValue();		
		if($poDataSet->getFieldByAlias("row_type")->getValue()=="best_practice"){
      $msIcon = $msGfxRef . 'icon-standard.png';
    }else{
      $moDocument = new PMDocument();
      $msIcon = $msGfxRef . $moDocument->getIcon($poDataSet->getFieldByAlias("context_id")->getValue(),true);
    }

    $moWebLib->getObject('context_icon')->setAttrSrc($msIcon);    
    $moWebLib->getObject('context_name')->setValue($msBestPracticeName);    

  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();        
    $moWebLib->getObject("level_control")->setLevelIterator(new ControlLevelIterator());
    $moWebLib->getObject("level_context")->setLevelIterator(new ContextLevelIterator());
    $moWebLib->getObject("level_type")->setLevelIterator(new TypeLevelIterator());
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport($moWebLib->getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    $moComponent->setReportFilter($moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter());
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('rs_summary_of_controls', "Resumo de Controles"));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_report_control_summary_pm.xml");
?>