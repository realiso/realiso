<?php

set_time_limit(3600);
include_once 'include.php';

class ControlLevelIterator extends FWDReportLevelIterator {
  
  public function __construct(){
    parent::__construct('control_id');
  }
  
  public function fetch(FWDDBDataSet $poDataSet){
    $mbIsActive = $poDataSet->getFieldByAlias('control_is_active')->getValue();
    if($mbIsActive){
      FWDWebLib::getObject('control_id')->setAttrSrc(FWDWebLib::getInstance()->getGfxRef().'icon-control.gif');
    }else{
      FWDWebLib::getObject('control_id')->setAttrSrc(FWDWebLib::getInstance()->getGfxRef().'icon-control_red.gif');
    }
    FWDWebLib::getObject('control_name')->setValue($poDataSet->getFieldByAlias('control_name')->getValue());
    FWDWebLib::getObject('control_responsible')->setValue($poDataSet->getFieldByAlias('control_responsible')->getValue());
  }
  
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moFilter = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter();
    
    FWDWebLib::getObject('control_level')->setLevelIterator(new ControlLevelIterator());
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport(FWDWebLib::getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    
    $moComponent->setReportFilter($moFilter);
    
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('mx_controls','Controles'));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_report_controls.xml');

?>