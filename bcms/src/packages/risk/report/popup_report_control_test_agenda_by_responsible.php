<?php
set_time_limit(3600);
include_once "include.php";

class ResponsibleLevelIterator extends FWDReportLevelIterator {  

  public function __construct(){
    parent::__construct("responsible_id");
  }
  
  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msGfxRef = $moWebLib->getGfxRef();

    $msResponsibleName = $poDataSet->getFieldByAlias("responsible_name")->getValue();    
    
    $msIcon = $msGfxRef . 'icon-user.png';    
    
    $moWebLib->getObject('responsible_icon')->setAttrSrc($msIcon);
    $moWebLib->getObject('responsible_name')->setValue($msResponsibleName);    
  }
}

class ControlLevelIterator extends FWDReportLevelIterator {  

  public function __construct(){
    parent::__construct("control_id");
  }  
  
  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msGfxRef = $moWebLib->getGfxRef();

    $msControlName = $poDataSet->getFieldByAlias("control_name")->getValue();    
    $mbControlIsActive = $poDataSet->getFieldByAlias("control_is_active")->getValue();
    $miScheduleId = $poDataSet->getFieldByAlias('schedule_id')->getValue();
    
    $moSchedule = new WKFSchedule();
    $moSchedule->fetchById($miScheduleId);
    
    $msControlNextRevision = FWDWebLib::getShortDate($moSchedule->getFieldValue('schedule_next_occurrence'));
    $msSchedule = $moSchedule->getTextDescription();
    
    $msIcon = $msGfxRef;
    
    if ($mbControlIsActive) $msIcon .= 'icon-control.png';
    else $msIcon .= 'icon-control_red.png';

    $moWebLib->getObject('control_icon')->setAttrSrc($msIcon);
    $moWebLib->getObject('control_name')->setValue($msControlName);
    $moWebLib->getObject('control_next_revision')->setValue($msControlNextRevision);
    $moWebLib->getObject('control_schedule')->setValue($msSchedule);
  }
}

class ProcessLevelIterator extends FWDReportLevelIterator {  

  public function __construct(){
    parent::__construct("process_id");
  }
	
  public function changedLevel(FWDDBDataSet $poDataSet){
    if (!$poDataSet->getFieldByAlias("process_id")->getValue()) return false;
    else return parent::changedLevel($poDataSet);
  }
  
  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msGfxRef = $moWebLib->getGfxRef();

    $msProcessName = $poDataSet->getFieldByAlias("process_name")->getValue();				
		$msProcessResponsibleName = $poDataSet->getFieldByAlias("process_responsible")->getValue();
		$miProcessValue = $poDataSet->getFieldByAlias("process_value")->getValue();

		$msIcon = $msGfxRef . 'icon-process_' . RMRiskConfig::getRiskColor($miProcessValue) . '.png';    

    $moWebLib->getObject('process_icon')->setAttrSrc($msIcon);
    $moWebLib->getObject('process_name')->setValue($msProcessName);
    $moWebLib->getObject('process_responsible')->setValue($msProcessResponsibleName);
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $moWebLib->getObject("level_responsible")->setLevelIterator(new ResponsibleLevelIterator());
    $moWebLib->getObject("level_control")->setLevelIterator(new ControlLevelIterator());
    $moWebLib->getObject("level_process")->setLevelIterator(new ProcessLevelIterator());
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport($moWebLib->getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    $moComponent->setReportFilter($moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter());
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('si_control_test_agenda_by_responsible',"Agenda de Teste dos Controles por Responsável"));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_report_control_test_agenda_by_responsible.xml");
?>