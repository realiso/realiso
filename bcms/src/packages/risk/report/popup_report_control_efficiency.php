<?php
set_time_limit(3600);
include_once "include.php";

class ControlEfficiencyLevelIterator extends FWDReportLevelIterator {  

  public function __construct(){
    parent::__construct("is_efficient");
  }
	
  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();    
   
    $miIsEfficient = $poDataSet->getFieldByAlias("is_efficient")->getValue();
    
    if ($miIsEfficient == CONTROL_IS_EFFICIENT) $moWebLib->getObject('control_efficiency_status')->setValue(FWDLanguage::getPHPStringValue('rs_efficient_controls', 'Controles Eficientes'));
    else $moWebLib->getObject('control_efficiency_status')->setValue(FWDLanguage::getPHPStringValue('rs_inefficient_controls', 'Controles Ineficientes'));
  }
}

class ControlLevelIterator extends FWDReportLevelIterator {  

  public function __construct(){
    parent::__construct("control_id");
  }
	
  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msGfxRef = $moWebLib->getGfxRef();
   
    $msControlName = $poDataSet->getFieldByAlias("control_name")->getValue();    
		$mbControlIsActive = $poDataSet->getFieldByAlias("control_is_active")->getValue();		
		$msControlResponsible = $poDataSet->getFieldByAlias("control_responsible")->getValue();
		
		$msIcon = $msGfxRef;    
    if ($mbControlIsActive) $msIcon .= 'icon-control.png';
    else $msIcon .= 'icon-control_red.png';

    $moWebLib->getObject('control_icon')->setAttrSrc($msIcon);    
    $moWebLib->getObject('control_name')->setValue($msControlName);
    $moWebLib->getObject('control_responsible')->setValue($msControlResponsible);
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();        
    $moWebLib->getObject("level_control_efficiency")->setLevelIterator(new ControlEfficiencyLevelIterator());
    $moWebLib->getObject("level_control")->setLevelIterator(new ControlLevelIterator());    
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport($moWebLib->getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    $moComponent->setReportFilter($moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter());
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('rs_controls_efficiency', "EficiÍncia dos Controles"));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_report_control_efficiency.xml");
?>