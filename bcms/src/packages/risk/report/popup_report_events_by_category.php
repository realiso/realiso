<?php
set_time_limit(3600);
include_once "include.php";

class CategoryLevelIterator extends FWDReportLevelIterator {  

  public function __construct(){
    parent::__construct("category_id");
  }
	
  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msGfxRef = $moWebLib->getGfxRef();

    $msCategoryName = $poDataSet->getFieldByAlias("category_name")->getValue();    
    
    $msIcon = $msGfxRef . 'icon-category.png';
        
    $moWebLib->getObject('category_icon')->setAttrSrc($msIcon);
    $moWebLib->getObject('category_name')->setValue($msCategoryName);
  }
}

class EventLevelIterator extends FWDReportLevelIterator {

  public function __construct(){
    parent::__construct("event_id");
  }

  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msGfxRef = $moWebLib->getGfxRef();

    $msEventDescription = $poDataSet->getFieldByAlias("event_description")->getValue();
    
    $msIcon = $msGfxRef . 'icon-event.png';

    $moWebLib->getObject('event_icon')->setAttrSrc($msIcon);    
    $moWebLib->getObject('event_description')->setValue($msEventDescription);    
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $moWebLib->getObject("level_category")->setLevelIterator(new CategoryLevelIterator());
    $moWebLib->getObject("level_event")->setLevelIterator(new EventLevelIterator());
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport($moWebLib->getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    $moComponent->setReportFilter($moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter());
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('rs_checklist_by_event',"Checklist por Evento"));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_report_events_by_category.xml");
?>