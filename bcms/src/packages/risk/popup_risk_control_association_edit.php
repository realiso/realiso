<?php

set_time_limit(300);


include_once "include.php";
include_once $handlers_ref . "QueryAssetValues.php";
include_once $handlers_ref . "QueryParametersValues.php";
include_once $handlers_ref . "QueryParameterControlRiskPropabylityReduction.php";
include_once $handlers_ref . "QueryRiskProbability.php";
include_once $handlers_ref . "QueryParameterControlRiskPropabylityReduction.php";
include_once $handlers_ref . "QueryParametersRiskControlValues.php";
include_once $handlers_ref . "QueryParameterControlValue.php";


function getRiskControlFromFields(){
  $moRiskControl = new RMRiskControl();

  $miRiskId = FWDWebLib::getObject('param_risk_id')->getValue();
  $miControlId = FWDWebLib::getObject('param_control_id')->getValue();
  $miRiskControlId = FWDWebLib::getObject('param_rc_id')->getValue();

  $moRiskParameter = FWDWebLib::getObject('rc_parameters');
  $maValues = $moRiskParameter->getValues();
  $maJustfications = $moRiskParameter->getJustifications();

  $moRiskControl->setFieldValue('rc_probability',$maValues['prob']);
  $moRiskControl->setFieldValue('rc_risk_id', $miRiskId);
  $moRiskControl->setFieldValue('rc_control_id', $miControlId);
  $moRiskControl->setFieldValue('rc_justification',$maJustfications['prob']);

  return $moRiskControl;
}

function save($poContext,$pbHasSensitiveChanges,$piContextId=0){
  $miRiskControlId = $piContextId;


  // armazena a string "risk_control" ou "control_risk"
  $msRC_CR = FWDWebLib::getObject('param_rc_cr')->getValue();

  $moRiskParameter = FWDWebLib::getObject('rc_parameters');
  $msVariableHash = FWDWebLib::getObject('riskparameters_hash')->getValue();
  $msRiskParameterHash = $moRiskParameter->getHash();

  if($miRiskControlId){
    //teste para verificar se o sistema n�o est� sendo hakeado
    $moCtxUserTest = new RMRiskControl();
    $moRisk = new RMRisk();
    $moRisk->fetchById(FWDWebLib::getObject("param_risk_id")->getValue());
    $moCtxUserTest->testPermissionToEdit($miRiskControlId,$moRisk->getResponsibleToACLPermition(FWDWebLib::getObject("param_risk_id")->getValue()));

    $poContext->update($miRiskControlId,true,$pbHasSensitiveChanges);
    $msCloseSearchPopup = '';
  }else{
    $miRiskControlId = $poContext->insert(true);
    /*
     * Se possuir o m�dulo de documenta��o, deve atualizar os
     * leitores do documento que est� sendo associado ao risco.
     */
    if (FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(POLICY_MODE)) {
    	$moControl = new RMControl();
    	$moControl->updateReaders($poContext->getFieldValue('rc_control_id'));
    }
    $msCloseSearchPopup = "soPopUpManager.closePopUp('popup_".$msRC_CR."_search');";
  }

  $moRiskParametersValue = new RiskParametersValue($moRiskParameter,RISKPARAMETER_RISKCONTROL);

  if($msVariableHash != $msRiskParameterHash){
    $moRiskParametersValue->setValues($miRiskControlId);
  }else{
    $moRiskParametersValue->updateJustification($miRiskControlId);
  }

  if($msRC_CR=='risk_control'){
    echo "soPopUpManager.getPopUpById('popup_risk_control_edit').getWindow().update_risk_values();";
  }

  echo "soPopUpManager.getPopUpById('popup_".$msRC_CR."_edit').getWindow().refresh_grid();" .
       "soPopUpManager.getPopUpById('popup_".$msRC_CR."_edit').getOpener().refresh_grid();" .
       "$msCloseSearchPopup" .
       "soPopUpManager.closePopUp('popup_risk_control_association_edit');";
}

class ConfirmEditEvent extends FWDRunnable {
  public function run(){
    $miRiskControlId = FWDWebLib::getObject('param_rc_id')->getValue();
    $moRiskControl = getRiskControlFromFields();
    save($moRiskControl,true,$miRiskControlId);
  }
}


class AssociateRiskControlEvent extends FWDRunnable {
  public function run(){
    $miRiskControlId = FWDWebLib::getObject('param_rc_id')->getValue();
    $moRiskControl = getRiskControlFromFields();

    $moRiskParameter = FWDWebLib::getObject('rc_parameters');
    $msVariableHash = FWDWebLib::getObject('riskparameters_hash')->getValue();
    $msRiskParameterHash = $moRiskParameter->getHash();

    if($msVariableHash != $msRiskParameterHash){
      $mbHasSensitiveChanges = true;
    }else{
      $mbHasSensitiveChanges = $moRiskControl->hasSensitiveChanges();
    }
    $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    if($miRiskControlId && $mbHasSensitiveChanges && $miUserId != $moRiskControl->getApprover()){
      $msTitle = FWDLanguage::getPHPStringValue('tt_reapproval','Reaprova��o');
      $msMessage = FWDLanguage::getPHPStringValue('st_sensitive_data_reapproval','Voc� est� alterando dados sens�veis, que necessitam passar por uma reaprova��o. Deseja confirmar as altera��es?');
      $msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
                      "soWindow = soPopUp.getOpener();" .
                      "soWindow.trigger_event('confirm_edit',3);";
      ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,75);
    }else{
      save($moRiskControl,$mbHasSensitiveChanges,$miRiskControlId);
    }
  }
}

class CalcRiskValue extends FWDRunnable{
	public function run(){
    $moWebLib = FWDWebLib::getInstance();
		$mfValueResidual = 0.00;
		$mfValueReal = FWDWebLib::getObject('st_risk_real')->getValue();

		$moHandleParameterRCValue = new QueryParametersRiskControlValues(FWDWebLib::getConnection());
		$maParRCValue = $moHandleParameterRCValue->getParameterRCValues();

		//valores dos parametros de parametriza��o do sistema
		$moHandleParameterValue = new QueryParametersValues(FWDWebLib::getConnection());
		$maParValue = $moHandleParameterValue->getParameterValues();
		//obtem os valores dos parametros dos ativos
		$moAssetHandler = new QueryAssetValues(FWDWebLib::getConnection());
		$moAssetHandler->setElementId(FWDWebLib::getObject("var_asset_id")->getValue());
	  $moAssetHandler->makeQuery();
	 	$maAssetValues = array();
    $maParWeight = array();
		if($moAssetHandler->executeQuery()){
			while($moAssetHandler->fetch()){
				$maAssetValues[$moAssetHandler->getFieldValue("parameter_id")] = $maParValue[$moAssetHandler->getFieldValue("value_id")];
        $maParWeight[$moAssetHandler->getFieldValue("parameter_id")] = $moAssetHandler->getFieldValue("parameter_weight");
			}
		}
    $maControlValues = array();
		//ids dos valores dos parametros do controle

		$moControlPar = $moWebLib->getObject('rc_parameters');
	  $maControlValuesAux = $moControlPar->getValues();
	  foreach($maControlValuesAux as $miKey => $miValue){
			if(FWDWebLib::getObject('controlActive')->getValue())
				$maControlValues[$miKey] = $maParRCValue[$miValue];
			else
				$maControlValues[$miKey] = 0;
	  }

		$miRiskId = FWDWebLib::getObject("param_risk_id")->getValue();
		$miControlId = FWDWebLib::getObject("param_control_id")->getValue();
		if(($miRiskId)&&($miControlId)){
			//adiciona o valor dos outros controles nos valores do controle que est� sendo editado
		  $moHandlerOtherControls = new QueryParameterControlValue(FWDWebLib::getConnection());
		  $moHandlerOtherControls->setControlId($miControlId);
      $moHandlerOtherControls->setRiskId($miRiskId);
		  $maOtherControls = $moHandlerOtherControls->getParameterValues();
	  	foreach ($maOtherControls as $miKey => $miValue){
	  		$maControlValues[$miKey] = $maControlValues[$miKey]+ $miValue;
	  	}
			//obtem os valores dos parametros do risco
			$moHandleRiskValues = new QueryRiskValues(FWDWebLib::getConnection());
		  $moHandleRiskValues->setElementId($miRiskId);
		  $moHandleRiskValues->makeQuery();
		 	$maRiskParValues = array();
			if($moHandleRiskValues->executeQuery()){
				$moDataset = $moHandleRiskValues->getDataset();
				while($moDataset->fetch()){
          $miParameterId = $moDataset->getFieldByAlias("parameter_id")->getValue();
          $miValueId = $moDataset->getFieldByAlias("value_id")->getValue();
					$maRiskParValues[$miParameterId] = $maParValue[$miValueId];
				}
			}
			//obtem a probabilidade do risco
			$moHandleRiskProb = new QueryRiskProbability(FWDWebLib::getConnection());
			$moHandleRiskProb->setElementId($miRiskId);
		  $moHandleRiskProb->makeQuery();
			if($moHandleRiskProb->executeQuery()){
				$moDataset = $moHandleRiskProb->getDataset();
				while($moDataset->fetch()){
          $miParameterId = $moDataset->getFieldByAlias("parameter_id")->getValue();
          $miValueId = $moDataset->getFieldByAlias("value_id")->getValue();
					$maRiskParValues[$miParameterId] = $maParValue[$miValueId];
				}
			}

			//pega a probabilidade de redu��o dos controles relacionados ao risco exceto a do controle que est� sendo parametrizado
			$moHandlerControlProb = new QueryParameterControlRiskPropabylityReduction(FWDWebLib::getConnection());
			$moHandlerControlProb->setRiskId($miRiskId);
			$moHandlerControlProb->setControlId($miControlId);
			$miProbCtrlReduction = $moHandlerControlProb->getControlRiskParameterProbabylity();
			$miProbReduction =	 $maRiskParValues['prob']  - 	$miProbCtrlReduction - $maControlValues['prob'];
			if($miProbReduction<1.00)
				$miProbReduction = 1.00;
			$maReducedValues = array();
			$miContNumPar = 0;
			foreach($maControlValues as $miKey => $miValue){
				if($miKey!='prob'){
          $miParWeight = $maParWeight[$miKey];
          $miContNumPar += $miParWeight;
					$miDifRCvalues = $maRiskParValues[$miKey] - $miValue;
					if($miDifRCvalues<1.00)
						$maReducedValues[$miKey] = 1.00;
					else
						$maReducedValues[$miKey] = $miDifRCvalues;
				}
			}

			if($miContNumPar>0){
				foreach($maReducedValues as $miKey => $miValue){
					if($miKey!='prob'){
							$mfValueResidual =$mfValueResidual + ($miValue + $maAssetValues[$miKey])*$maParWeight[$miKey];
					}
				}
				$mfValueResidual = ($mfValueResidual*$miProbReduction)/($miContNumPar*2);
			}else{
				$mfValueResidual = $mfValueReal;
			}
		}else{
			$mfValueResidual = $mfValueReal;
		}
		$mfValueResidual=RMRiskConfig::getRiskValueFormat($mfValueResidual);
		echo"
		 gobi('st_risk_residual').setValue(\"{$mfValueResidual}\");
		";
	}
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new AssociateRiskControlEvent("associate_risk_control_event"));
    $moStartEvent->addAjaxEvent(new CalcRiskValue('calc_risk_value'));
    $moStartEvent->addAjaxEvent(new ConfirmEditEvent('confirm_edit'));

    $moRiskParameter = FWDWebLib::getObject('rc_parameters');
    $moRiskParametersValue = new RiskParametersValue($moRiskParameter,RISKPARAMETER_RISKCONTROL);
    $moRiskParameter->populate();
    
    $moControl = new RMControl();
    $moControl->fetchById(FWDWebLib::getObject('param_control_id')->getValue());
    FWDWebLib::getObject('controlActive')->setValue($moControl->getFieldValue('control_active'));

  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    //teste para verificar se o sistema n�o est� sendo hakeado
    $moCtxUserTest = new RMRiskControl();
    $moCtxUserTest->testPermissionToInsert();



    $moRisk = new RMRisk();
    $moRisk->fetchById(FWDWebLib::getObject("param_risk_id")->getValue());
    $moRisk->getFieldValue("risk_name");
    FWDWebLib::getObject("risk")->setValue($moRisk->getFieldValue("risk_name"));
    FWDWebLib::getObject("var_asset_id")->setValue($moRisk->getFieldValue("risk_asset_id"));

    $moControl = new RMControl();
    $moControl->fetchById(FWDWebLib::getObject('param_control_id')->getValue());
    FWDWebLib::getObject('control')->setValue($moControl->getFieldValue('control_name'));
    if(!FWDWebLib::getObject('controlActive')->getValue()){
    		$msMessage = '';
    		if((ISMSLib::getTimestamp($moControl->getFieldValue('control_date_deadline')) < time()) && ( $moControl->getFieldValue('control_date_implemented')==null) )
    			$msMessage .= FWDLanguage::getPHPStringValue('st_control_not_mitigating_not_implemented_message','O Controle n�o est� reduzindo Riscos, pois n�o est� implementado e a data de deadline de implementa��o expirou!');
    		else{
    		$moCtrlEff = new WKFControlEfficiency();
	    		if($moCtrlEff->fetchById($moControl->getFieldValue('control_id') ) )
    			if((!$moCtrlEff->getFieldValue('real_efficiency'))&&($moCtrlEff->getFieldValue('expected_efficiency'))&&( $moCtrlEff->getFieldValue('real_efficiency') < $moCtrlEff->getFieldValue('expected_efficiency')) )
    				$msMessage .= FWDLanguage::getPHPStringValue('st_control_not_mitigating_efficiency_message','O Controle n�o est� reduzindo Riscos, pois a efici�ncia real est� menor que a efici�ncia esperada!');
    		}
    	  FWDWebLib::getObject('st_warning_control_not_active')->setValue($msMessage);
    }

    $miRCid = FWDWebLib::getObject('param_rc_id')->getValue();
    if ($miRCid) {
      $moRiskParameter = FWDWebLib::getObject('rc_parameters');
      $moRiskParametersValue = new RiskParametersValue($moRiskParameter,RISKPARAMETER_RISKCONTROL);
      $moRiskParametersValue->getValues($miRCid);
      $msHash = $moRiskParameter->getHash();
      FWDWebLib::getObject('riskparameters_hash')->setValue($msHash);
    }
    else {}
    FWDWebLib::getObject('st_risk_real')->setValue(RMRiskConfig::getRiskValueFormat($moRisk->getFieldValue('risk_value')));
    FWDWebLib::getObject('st_risk_residual')->setValue(RMRiskConfig::getRiskValueFormat($moRisk->getFieldValue('risk_residual_value')));

    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_risk_control_association_edit.xml');

?>