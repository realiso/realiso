<?php
include_once "include.php";
include_once $handlers_ref . "QueryAssetFromProcess.php";
include_once $handlers_ref . "grid/QueryGridAssetSearch.php";
include_once $handlers_ref . "QueryContextHierarchicalStructure.php";

class GridAssetSearch extends FWDDrawGrid {
  public function DrawItem(){
    switch($this->ciColumnIndex){
      case 1:
        $miAssetValue = $this->caData[1];
        $msAssetColor = RMRiskConfig::getRiskColor($miAssetValue);
        $this->coCellBox->setIconSrc("icon-asset_".$msAssetColor.".gif");
        return parent::drawItem();
        break;
      case 2:
        $miAssetValue = $this->caData[1];
        $msLinkColor = RMRiskConfig::getRiskColor($miAssetValue,true);
        $msLink = "<font color=".$msLinkColor.">".$this->caData[2]."</a>";
        $this->coCellBox->setAttrStringNoEscape("true");
        $this->coCellBox->setValue("<a href='javascript:open_visualize(".$this->caData[4].",".CONTEXT_ASSET.",\"".uniqid()."\")'>{$msLink}</a>");
        return $this->coCellBox->draw();
        break;
      case 3:
        $this->coCellBox->setAttrStringNoEscape("true");
        $this->coCellBox->setValue("<a href='javascript:open_visualize(".$this->caData[5].",".CONTEXT_CATEGORY.",\"".uniqid()."\")'>{$this->caData[3]}</a>");
        return $this->coCellBox->draw();
        break;
      default:
        return parent::drawItem();
        break;
    }
  }
}

class AssociateAssetsEvent extends FWDRunnable {
  public function run(){    
    $moWebLib = FWDWebLib::getInstance();
    $miProcessId = $moWebLib->getObject('process_id')->getValue();

    //teste para verificar se o sistema n�o est� sendo hakeado
    $moCtxUserTest = new RMProcessAsset();
    $moCtxTest = new RMProcess();
    $moCtxTest->fetchById($miProcessId);
    $moCtxUserTest->testPermissionToEdit($miProcessId,$moCtxTest->getResponsible());
    
    $msNewAssets = $moWebLib->getObject('current_assets_ids')->getValue();
    if($msNewAssets){
      $maNewAssets = explode(':',$msNewAssets);
    }else{
      $maNewAssets = array();
    }
    
    $moQuery = new QueryAssetFromProcess(FWDWebLib::getConnection());
    $moQuery->setProcessId($miProcessId);
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    $maOldAssets = $moQuery->getAssets();
    
    $maToInsert = array_diff($maNewAssets,$maOldAssets);
    $maToDelete = array_diff($maOldAssets,$maNewAssets);
    $moProcessAsset = new RMProcessAsset();
    if(count($maToInsert)) $moProcessAsset->insertAssets($miProcessId,$maToInsert);
    if(count($maToDelete)) $moProcessAsset->deleteAssets($miProcessId,$maToDelete);
    
    /*
     * Se possuir o m�dulo de documenta��o, deve atualizar os leitores dos ativos inseridos/deletados 
     */
    if (FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(POLICY_MODE)) {
    	$maAssets = array_merge($maToInsert,$maToDelete);
    	$moAsset= new RMAsset();
    	foreach ($maAssets as $miAssetId) {    		
    		$moAsset->updateReaders($miAssetId);
    	}   	
    }
    
    echo "soWindow = soPopUpManager.getPopUpById('popup_process_asset_association').getOpener();"
        ."if (soWindow.refresh_grid)"
        ."  soWindow.refresh_grid();"
        ."soPopUpManager.closePopUp('popup_process_asset_association');";
  }
}

class SearchAssetEvent extends FWDRunnable {
  public function run(){
    $moGrid = FWDWebLib::getObject('grid_asset_search');
    $moGrid->execEventPopulate();
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new SearchAssetEvent('search_asset_event'));    
    $moStartEvent->addAjaxEvent(new AssociateAssetsEvent('associate_assets_event'));
    
    $moSearchGrid = FWDWebLib::getObject('grid_asset_search');
    $moSearchGrid->setObjFwdDrawGrid(new GridAssetSearch());
    $moSearchHandler = new QueryGridAssetSearch(FWDWebLib::getConnection());
    
    $moCurrentGrid = FWDWebLib::getObject('grid_current_assets');
    $moCurrentGrid->setObjFwdDrawGrid(new GridAssetSearch());
    $moCurrentHandler = new QueryGridAssetSearch(FWDWebLib::getConnection());
    
    $moSearchHandler->setName(FWDWebLib::getObject('name_filter')->getValue());
    $moSearchHandler->setCategory(FWDWebLib::getObject('category_filter')->getValue());
    
    $miProcessId = FWDWebLib::getObject('param_process_id')->getValue();
    if($miProcessId){
      FWDWebLib::getObject('process_id')->setValue($miProcessId);
    }else{
      $miProcessId = FWDWebLib::getObject('process_id')->getValue();
    }
    
    $msCurrentIds = FWDWebLib::getObject('current_assets_ids')->getValue();
    if($msCurrentIds){
      $maCurrentIds = explode(':',$msCurrentIds);
      $moSearchHandler->setExcludedIds($maCurrentIds);
      $moCurrentHandler->setIds($maCurrentIds);
    }
    else {
      $moCurrentHandler->setEmpty(true);
    }
    
    $moSearchGrid->setQueryHandler($moSearchHandler);
    $moCurrentGrid->setQueryHandler($moCurrentHandler);
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    //teste para verificar se o sistema n�o est� sendo hakeado
    $moCtxUserTest = new RMProcessAsset();
    $moCtxUserTest->testPermissionToInsert();
    
    $moSelect = FWDWebLib::getObject('asset_category_id');
    $moQuery = new QueryContextHierarchicalStructure(FWDWebLib::getConnection());
    $moQuery->setContextType(CONTEXT_CATEGORY);
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    foreach ($moQuery->getContexts() as $maContext) {
      list($miContextId,$msHierarchicalName) = $maContext;
      $moSelect->setItemValue($miContextId,$msHierarchicalName);
    }
    
    $moQuery = new QueryAssetFromProcess(FWDWebLib::getConnection());
    $moQuery->setProcessId(FWDWebLib::getObject('param_process_id')->getValue());
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    FWDWebLib::getObject('current_assets_ids')->setValue(implode(":",$moQuery->getAssets()));
    
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
?>
<script language="javascript">
  gebi('asset_name').focus(); 

  function refresh_grid() {
    gobi('grid_current_assets').setPopulate(true);
    gobi('grid_current_assets').refresh();
    //gobi('grid_asset_search').setPopulate(true);
    //gobi('grid_asset_search').refresh();    
  }
  function enter_asset_search_event(e)
  {
    if (!e) e = window.event;
    if(e['keyCode']==13) {
      gebi('name_filter').value = gebi('asset_name').value;
      gebi('category_filter').value = gebi('asset_category_id').value;
      gebi('current_assets_ids').value = gobi('grid_current_assets').getAllIds().join(':');
      gobi('grid_asset_search').setPopulate(true);
      trigger_event("search_asset_event","3");
    }
  }
  FWDEventManager.addEvent(gebi('asset_name'), 'keydown', enter_asset_search_event);
  refresh_grid();
</script>
<?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load('popup_process_asset_association.xml');

?>