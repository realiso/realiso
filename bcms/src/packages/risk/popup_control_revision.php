<?php

include_once 'include.php';

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $msRevisionSensitive = FWDWebLib::getObject('par_revision_sensitive')->getValue();
    $msRevision = FWDWebLib::getObject('par_revision')->getValue();
    
    if($msRevisionSensitive){      
      $maRevision = array_merge(
        FWDWebLib::unserializeString($msRevision, false),
        FWDWebLib::unserializeString($msRevisionSensitive, false)
      );
      
      for($i=1;$i<=5;$i++){
        FWDWebLib::getObject('revision_value_'.$i)->setValue($maRevision['revision_value_'.$i]);
      }
      FWDWebLib::getObject('revision_control_metric')->setValue($maRevision['revision_metric']);
      
      FWDWebLib::getObject('controller_ee')->setValue($maRevision['controller_ee']);
      FWDWebLib::getObject('var_schedule')->setValue($maRevision['schedule']);
    }
    
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        
        function set_schedule(psSerializedSchedule){
          gebi('var_schedule').value = psSerializedSchedule;
        }
        
        function confirm(){
          var maSensitiveFields = {};
          var maFields = {};
          
          maSensitiveFields['schedule'] = gebi('var_schedule').value;
          maSensitiveFields['controller_ee'] = gebi('controller_ee').value;
          for(var i=1;i<=5;i++){
            maFields['revision_value_'+i] = gebi('revision_value_'+i).value;
          }
          maFields['revision_metric'] = gebi('revision_control_metric').value;
          
          self.getOpener().set_revision(soUnSerializer.serialize(maSensitiveFields),soUnSerializer.serialize(maFields));
          self.close();
        }
        
      </script>
    <?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_control_revision.xml');

?>