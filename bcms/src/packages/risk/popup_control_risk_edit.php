<?php
include_once "include.php";
include_once $handlers_ref . "grid/QueryGridControlRisk.php";

class DissociateRisksEvent extends FWDRunnable {
  public function run(){
    //teste para verificar se o sistema n�o est� sendo hakeado
    $moCtxUserTest = new RMRiskControl();
    $moCtxResponsibleTest = new RMControl();
    $moCtxResponsibleTest->fetchById(FWDWebLib::getObject('control_id')->getValue());
    $moCtxUserTest->testPermissionToEdit(FWDWebLib::getObject('control_id')->getValue(),$moCtxResponsibleTest->getResponsible());
    
    $maGridValues = FWDWebLib::getObject("grid_control_risk")->getValue();
    
    $moRiskControl = new RMRiskControl();
    foreach ($maGridValues as $maGridValue)
      $moRiskControl->delete($maGridValue[4],true);
    
    /*
     * Se possui o m�dulo de documenta��o, deve atualizar
     * os leitores do documento do controle.
     */
    if (FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(POLICY_MODE)) {
    	$moControl = new RMControl();
    	$moControl->updateReaders(FWDWebLib::getObject("control_id")->getValue());
    }
    
    echo "soPopUpManager.getPopUpById('popup_control_risk_edit').getWindow().refresh_grid();" .
         "soPopUpManager.getPopUpById('popup_control_risk_edit').getOpener().refresh_grid();";
  }
}

class GridControlRisk extends FWDDrawGrid {
  public function drawItem(){
    switch($this->ciColumnIndex){
      case 1:
        $msRiskColor = RMRiskConfig::getRiskColor($this->caData[1]);
        $msGfxRef = FWDWebLib::getInstance()->getGfxRef();
        $moIcon = new FWDIcon(new FWDBox(2,4));
        $moIcon->setAttrSrc("{$msGfxRef}icon-risk_".$msRiskColor.".gif");
        
        //risk_zero_value
        if(!$this->caData[1]){
          $moACL = FWDACLSecurity::getInstance();
          $maDenied = array();
          $maDenied[] = 'risk_zero_value';
          $moACL->setNotAllowed($maDenied);
          $moMenu = FWDWebLib::getObject('menu');
          $moGrid = FWDWebLib::getObject('grid_control_risk');
          $msLine = $moGrid->getAttrName().'_acl_'.$this->ciRowIndex;
          FWDGridMenuACLSecurity::getInstance()->installGridMenuACL($moMenu,$moACL,$moGrid,$msLine);
        }
        return $moIcon->draw();
        break;
      
      case 2:
        $this->coCellBox->setAttrStringNoEscape("true");
        $this->coCellBox->setValue("<a href='javascript:open_visualize(".$this->caData[3].",".CONTEXT_RISK.",\"".uniqid()."\")'>{$this->caData[2]}</a>");
        return $this->coCellBox->draw();
        break;
      
      default:
        return parent::drawItem();
        break;
    }
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new DissociateRisksEvent("dissociate_risks_event"));
    
    $moGrid = FWDWebLib::getObject("grid_control_risk");
    $moHandler = new QueryGridControlRisk(FWDWebLib::getConnection());
    $moHandler->setControlId(FWDWebLib::getObject("control_id")->getValue());
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new GridControlRisk());
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $miControlId = FWDWebLib::getObject("param_control_id")->getValue();
    $moWindowTitle = FWDWebLib::getObject("window_title");

    //teste para verificar se o sistema n�o est� sendo hakeado
    $moCtxUserTest = new RMRiskControl();
    $moCtxUserTest->testPermissionToInsert();

    if($miControlId){
      $moWindowTitle->setValue(FWDLanguage::getPHPStringValue("tt_risk_control_association_editing", "Edi��o de Associa��o Controle-Risco"));
      
      FWDWebLib::getObject("control_id")->setValue($miControlId);
      
      $moControl = new RMControl();
      $moControl->fetchById($miControlId);
      FWDWebLib::getObject("control_name")->setValue($moControl->getFieldValue("control_name"));
    }
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
?>
<script language="javascript">
  function refresh_grid() {
    js_refresh_grid('grid_control_risk');   
  }
</script>
<?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_control_risk_edit.xml');
?>