<?php

include_once 'include.php';
include_once $handlers_ref . "QueryContextHierarchicalStructure.php";
include_once $handlers_ref . 'QueryTotalAssets.php';
include_once $handlers_ref . "select/QuerySelectReadableDocumentByContext.php";

function getAssetFromFields(){
  $moAsset = new RMAsset();

  $miAssetSecurityResponsible = FWDWebLib::getObject('asset_security_responsible_id')->getValue();
  $miAssetResponsible = FWDWebLib::getObject('asset_responsible_id')->getValue();
  $miAssetCategoryId = FWDWebLib::getObject('asset_category_id')->getValue();
  $msAssetName = FWDWebLib::getObject('asset_name')->getValue();
  $mAssetDescription = FWDWebLib::getObject('asset_description')->getValue();
  $mAssetCost = str_replace(',','',FWDWebLib::getObject('asset_cost')->getValue());
  //$mAssetValue = FWDWebLib::getObject('asset_value')->getValue();
  $mAssetLegality = trim(FWDWebLib::getObject('legality')->getValue(),':');
  $mAssetJustification = FWDWebLib::getObject('asset_justification')->getValue();

  $moAsset->setFieldValue('asset_security_responsible_id', $miAssetSecurityResponsible);
  $moAsset->setFieldValue('asset_responsible_id', $miAssetResponsible);
  $moAsset->setFieldValue('asset_category_id', $miAssetCategoryId);
  $moAsset->setFieldValue('asset_name', $msAssetName);
  $moAsset->setFieldValue('asset_description', $mAssetDescription);
  $moAsset->setFieldValue('asset_cost', $mAssetCost);
  //$moAsset->setFieldValue('asset_value', $mAssetValue);
  $moAsset->setFieldValue('asset_legality', $mAssetLegality);
  $moAsset->setFieldValue('asset_justification', $mAssetJustification);
  
  $mbHasPMModule = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(POLICY_MODE);
  if (!$mbHasPMModule) {
    $msAssetDocument = FWDWebLib::getObject('asset_document')->getValue();
    $moAsset->setFieldValue('asset_document', $msAssetDocument);
  }
  return $moAsset;
}

function save($poContext,$pbHasSensitiveChanges,$piContextId=0){
  $moAsset = $poContext;
  $miAssetId = $piContextId;
  $mbOpenPopup = true;
  if($miAssetId){
    //teste para verificar se o sistema n�o est� sendo hakeado no evento de update
    $moCtxUserTest = new RMAsset();
    $moCtxTest = new RMAsset();
    $moCtxTest->fetchById($miAssetId);
    $moCtxUserTest->testPermissionToEdit($piContextId,$moCtxTest->getResponsible());
    
    $moAsset->update($miAssetId,true,$pbHasSensitiveChanges);
    $miInitialCatId = FWDWebLib::getObject('initial_category_id')->getValue();
    $miCurrentCatId = FWDWebLib::getObject('asset_category_id')->getValue();
    if($miInitialCatId == $miCurrentCatId) $mbOpenPopup = false;
  }else{
    $miAssetId = $moAsset->insert(true);
    
    $miProcessId = FWDWebLib::getObject('param_process_id')->getValue();
    if($miProcessId){
      $moAsset = new RMAsset();
      $moAsset->setFieldValue('asset_id',$miAssetId);
      $moAsset->updateRelation(new RMProcessAsset(),'process_id',array($miProcessId));
    }
  }

  //Salva valores do ativo, caso sejam diferentes dos originais
  $moRiskParameter = FWDWebLib::getObject('parameters');
  $msVariableHash = FWDWebLib::getObject('riskparameters_hash')->getValue();
  $msRiskParameterHash = $moRiskParameter->getHash();
  $moRiskParameterValues = new RiskParametersValue($moRiskParameter,RISKPARAMETER_ASSET);
  if($msVariableHash != $msRiskParameterHash){
    $moRiskParameterValues->setValues($miAssetId);
  }else{
    $moRiskParameterValues->updateJustification($miAssetId);
  }
  
  $mbHasPMModule = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(POLICY_MODE);
  if ($mbHasPMModule) {
    $moDocContext = new PMDocContext();
    $moDocContext->createFilter($miAssetId,'context_id');
    $moDocContext->select();
    while($moDocContext->fetch()) {
      $moDocContext->updateAutoApprovers($moDocContext->getFieldValue('document_id'));
    }
  }

  echo "soWindow = soPopUpManager.getPopUpById('popup_asset_edit').getOpener();"
      ."if(soWindow.refresh_grid) soWindow.refresh_grid();"
      .($mbOpenPopup ?
           "if(soWindow.nav_asset_open_popup)"
          ."  soWindow.nav_asset_open_popup('popup_create_risks_from_events','packages/risk/popup_create_risks_from_events.php?asset=$miAssetId&category=".FWDWebLib::getObject('asset_category_id')->getValue()."','','true');"
        : ""
       )
      ."soPopUpManager.closePopUp('popup_asset_edit');";
}

class ConfirmEditEvent extends FWDRunnable {
  public function run(){
    $miAssetId = FWDWebLib::getObject('asset_id')->getValue();
    $moAsset = getAssetFromFields();
    save($moAsset,true,$miAssetId);
  }
}

class SaveAssetEvent extends FWDRunnable {
  public function run(){

    $miAssetId = FWDWebLib::getObject('asset_id')->getValue();
    $moAsset = getAssetFromFields();
    $moAsset->setHash(FWDWebLib::getObject('hash')->getValue());

    $moRiskParameter = FWDWebLib::getObject('parameters');
    $msVariableHash = FWDWebLib::getObject('riskparameters_hash')->getValue();
    $msRiskParameterHash = $moRiskParameter->getHash();
    
    if($msVariableHash != $msRiskParameterHash){
      $mbHasSensitiveChanges = true;
    }else{
      $mbHasSensitiveChanges = $moAsset->hasSensitiveChanges();
    }
    $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    if($miAssetId && $mbHasSensitiveChanges && $miUserId != $moAsset->getApprover()){
      $msTitle = FWDLanguage::getPHPStringValue('tt_reapproval','Reaprova��o');
      $msMessage = FWDLanguage::getPHPStringValue('st_sensitive_data_reapproval','Voc� est� alterando dados sens�veis, que necessitam passar por uma reaprova��o. Deseja confirmar as altera��es?');
      $msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
                      "soWindow = soPopUp.getOpener();" .
                      "soWindow.trigger_event('confirm_edit',3);";
      ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,75);
    }else{
      save($moAsset,$mbHasSensitiveChanges,$miAssetId);
    }
  }
}

class DocumentViewEvent extends FWDRunnable {
  public function run() {
    $moWebLib = FWDWebLib::getInstance();
    $miDocumentId = $moWebLib->getObject('select_asset_document')->getValue();
    $miUserId = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    
    $moDocument = new PMDocument();
    $moDocument->testPermissionToRead($miDocumentId);
    $moDocument->fetchById($miDocumentId);
    $moPMDocInstance = $moDocument->getPublishedInstance();
    $miDocInstanceId = $moPMDocInstance->getFieldValue('doc_instance_id');
    
    $moReader = new PMDocumentReader();
    $moReader->createFilter($miDocumentId,'document_id');
    $moReader->createFilter($miUserId,'user_id');
    $moReader->select();
    if ($moReader->fetch()) {
      $moReader->setFieldValue('user_has_read_document',1);
      $moReader->update();
    }
    
    $moPMDocReadHistory = new PMDocReadHistory();
    $moPMDocReadHistory->setFieldValue('user_id',$miUserId);
    $moPMDocReadHistory->setFieldValue('document_id',$miDocInstanceId);
    $moPMDocReadHistory->setFieldValue('read_date',ISMSLib::ISMSTime());
    $moPMDocReadHistory->insert();
    
    $mbIsLink = (bool)$moPMDocInstance->getFieldValue('doc_instance_is_link');
    if ($mbIsLink) {
      $msURL = $moPMDocInstance->getFieldValue('doc_instance_link');
      if ($msURL)
        echo "js_open('{$msURL}','document_link',740,502,'center_screen');";
    } else {
      echo "js_submit('download_file','ajax');";
    }
  }
}

class DownloadFileEvent extends FWDRunnable {
  public function run() {
    $moWebLib = FWDWebLib::getInstance();
    $miDocumentId = $moWebLib->getObject('select_asset_document')->getValue();
    $miUserId = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    
    $moDocument = new PMDocument();
    $moDocument->testPermissionToRead($miDocumentId);
    $moDocument->fetchById($miDocumentId);
    $moPMDocInstance = $moDocument->getPublishedInstance();
    
    $msPath = $moWebLib->getSysRef()."packages/policy/".$moPMDocInstance->getFieldValue('doc_instance_path');
    $msFileName = $moPMDocInstance->getFieldValue('doc_instance_file_name');
    header('Cache-Control: ');// leave blank to avoid IE errors
    header('Pragma: ');// leave blank to avoid IE errors
    header('Content-type: application/octet-stream');
    header('Content-Disposition: attachment; filename="'.$msFileName.'"');
    $moCrypt = new FWDCrypt();
    $moFP = fopen($msPath,'rb');
    $moCrypt->setIV(fread($moFP,strlen($moCrypt->getIV())));
    while(!feof($moFP)) {
      echo $moCrypt->decryptNoBase64(fread($moFP,16384));
    }
    fclose($moFP);
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $miAssetId = FWDWebLib::getObject('param_asset_id')->getValue();
    FWDWebLib::getObject('asset_id')->setValue($miAssetId);

    $moSelect = FWDWebLib::getObject('asset_category_id');
    $moQuery = new QueryContextHierarchicalStructure(FWDWebLib::getConnection());
    $moQuery->setContextType(CONTEXT_CATEGORY);
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    foreach ($moQuery->getContexts() as $maContext) {
      list($miContextId,$msHierarchicalName) = $maContext;
      $moSelect->setItemValue($miContextId,$msHierarchicalName);
    }

    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
    $moStartEvent->addAjaxEvent(new SaveAssetEvent('save_asset'));
    $moStartEvent->addAjaxEvent(new ConfirmEditEvent('confirm_edit'));
    $moStartEvent->addAjaxEvent(new DocumentViewEvent('document_view_event'));
    $moStartEvent->addSubmitEvent(new DownloadFileEvent('download_file'));

    $moWebLib = FWDWebLib::getInstance();
    $moRiskParameter = $moWebLib->getObject('parameters');
    $moRiskParameter->populate();
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $miAssetId = FWDWebLib::getObject('asset_id')->getValue();
    $moWindowTitle = FWDWebLib::getObject('window_title');
    $moIcon = FWDWebLib::getObject('tooltip_icon');
    
    /*
     * Se possui o m�dulo de documenta��o, troca o campo texto
     * por select, e mostra um bot�o para o usu�rio visualizar
     * o documento selecionado. Se n�o houver documento associado,
     * mostrar mensagem indicando tal situa��o.
     */
    $mbHasPMModule = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(POLICY_MODE);
    if ($mbHasPMModule) {
      if($miAssetId){
        FWDWebLib::getObject('document_without_policy')->setShouldDraw(false);
        FWDWebLib::getObject('document_with_policy')->setShouldDraw(true);
        
        $moHandler = new QuerySelectReadableDocumentByContext(FWDWebLib::getConnection());
        $moHandler->setContextId($miAssetId);
        $moSelect = FWDWebLib::getObject('select_asset_document');
        $moSelect->setQueryHandler($moHandler);
        $moSelect->populate();
        
        if (!$moSelect->getValue()) {
          FWDWebLib::getObject('no_published_documents')->setValue(FWDLanguage::getPHPStringValue('st_no_published_documents','N�o h� documentos publicados'));
          FWDWebLib::getObject('no_published_documents')->setAttrDisplay('true');
          FWDWebLib::getObject('asset_document_create')->setAttrDisplay('true');
          FWDWebLib::getObject('select_asset_document')->setAttrDisplay('false');
          FWDWebLib::getObject('asset_document_view')->setShouldDraw(false);
          FWDWebLib::getObject('asset_document_properties')->setShouldDraw(false);
        }
      }else{
        FWDWebLib::getObject('document_without_policy')->setShouldDraw(false);
        FWDWebLib::getObject('document_with_policy')->setShouldDraw(false);
        FWDWebLib::getObject('document')->setShouldDraw(false);
      }
      
      /* Provisoriamente, como os documentos passaram a ser acessados
       * a partir do menu de contexto, estou removendo a linha de
       * documento da tela de edi��o
       */
       FWDWebLib::getObject('document')->setShouldDraw(false);
       FWDWebLib::getObject('document_with_policy')->setShouldDraw(false);
       
    } else {
      FWDWebLib::getObject('document_without_policy')->setShouldDraw(true);
      FWDWebLib::getObject('document_with_policy')->setShouldDraw(false);
    }
    
    if($miAssetId){
      $moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_asset_editing', 'Edi��o de Ativo'));
      $moAsset = new RMAsset();
      $moAsset->fetchById($miAssetId);
      
      //teste para verificar se o sistema n�o est� sendo hakeado
      $moCtxUserTest = new RMAsset();
      $moCtxUserTest->testPermissionToEdit($miAssetId,$moAsset->getFieldValue('asset_responsible_id'));
      
      FWDWebLib::getObject('asset_name')->setValue($moAsset->getFieldValue('asset_name'));
      $moResponsibleUser = new ISMSUser();
      $miResponsibleId = $moAsset->getFieldValue('asset_responsible_id');
      $moResponsibleUser->fetchById($miResponsibleId);
      FWDWebLib::getObject('asset_responsible_id')->setValue($miResponsibleId);
      FWDWebLib::getObject('asset_responsible_name')->setValue($moResponsibleUser->getFieldValue('user_name'));
      $miSecurityResponsibleId = $moAsset->getFieldValue('asset_security_responsible_id');
      $moSecurityResponsibleUser = new ISMSUser();
      $moSecurityResponsibleUser->fetchById($miSecurityResponsibleId);

      FWDWebLib::getObject('asset_security_responsible_id')->setValue($miSecurityResponsibleId);
      FWDWebLib::getObject('asset_security_responsible_name')->setValue($moSecurityResponsibleUser->getFieldValue('user_name'));
      FWDWebLib::getObject('asset_category_id')->checkItem($moAsset->getFieldValue('asset_category_id'));
      FWDWebLib::getObject('asset_document')->setValue($moAsset->getFieldValue('asset_document'));
      FWDWebLib::getObject('asset_description')->setValue($moAsset->getFieldValue('asset_description'));
      FWDWebLib::getObject('asset_cost')->setValue($moAsset->getFieldValue('asset_cost'));
      FWDWebLib::getObject('legality')->setValue($moAsset->getFieldValue('asset_legality'));
      FWDWebLib::getObject('asset_justification')->setValue($moAsset->getFieldValue('asset_justification'));

      FWDWebLib::getObject('initial_category_id')->setValue($moAsset->getFieldValue('asset_category_id'));

      FWDWebLib::getObject('hash')->setValue($moAsset->getHash());

      $moRiskParameter = FWDWebLib::getObject('parameters');
      $moRiskParameterValues = new RiskParametersValue($moRiskParameter,RISKPARAMETER_ASSET);
      $moRiskParameterValues->getValues($miAssetId);

      $msHash = $moRiskParameter->getHash();
      FWDWebLib::getObject('riskparameters_hash')->setValue($msHash);

      $moToolTip = ISMSLib::getInstance()->getToolTipCreateModifyById($miAssetId);
      $moIcon->addObjFWDEvent($moToolTip);
    }else{
      FWDWebLib::getObject('riskparameters_hash')->setValue(FWDWebLib::getObject('parameters')->getHash());
    
      //teste para verificar se o sistema n�o est� sendo hakeado
      $moCtxUserTest = new RMAsset();
      $moCtxUserTest->testPermissionToInsert();

      FWDWebLib::getObject('asset_dependents')->setShouldDraw(false);
      FWDWebLib::getObject('asset_dependencies')->setShouldDraw(false);
      
      $moIcon->setAttrDisplay('false');
      $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
      $miUserId = $moSession->getUserId();
      $moUser = new ISMSUser();
      $moUser->fetchById($miUserId);
      FWDWebLib::getObject('asset_responsible_id')->setValue($miUserId);
      FWDWebLib::getObject('asset_responsible_name')->setValue($moUser->getFieldValue('user_name'));
      FWDWebLib::getObject('asset_security_responsible_id')->setValue($miUserId);
      FWDWebLib::getObject('asset_security_responsible_name')->setValue($moUser->getFieldValue('user_name'));
      
      $moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_asset_adding', 'Adi��o de Ativo'));
    }
    
    //teste para ver se o custo esta abilitado no sistema
    $moConfig = new ISMSConfig();
    if(!$moConfig->getConfig(GENERAL_COST_ENABLED)){
      FWDWebLib::getObject('cost')->setShouldDraw(false);
      FWDWebLib::getObject('asset_cost')->setShouldDraw(false);
      if(FWDWebLib::getObject('st_money_symbol'))
      FWDWebLib::getObject('st_money_symbol')->setShouldDraw(false);
    }
    
    // coloca o simbolo de moeda
    $msCurrency = constant($moConfig->getConfig(GENERAL_CURRENCY_IDENTIFIER));
	if (!$msCurrency) $msCurrency = '$';
	$msCost = FWDLanguage::getPHPStringValue('lb_value_cl_money_symbol', 'Valor');
    FWDWebLib::getObject('cost')->setValue($msCost . " ($msCurrency):");
    
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        gebi('asset_name').focus();
        
        function set_responsible(id, name){
          gebi('asset_responsible_id').value = id;
          gobi('asset_responsible_name').setValue(name);
        }
        function set_security_responsible(id, name){
          gebi('asset_security_responsible_id').value = id;
          gobi('asset_security_responsible_name').setValue(name);
        }
        if(gebi('asset_cost'))
          gebi('asset_cost').value = format_currency(gebi('asset_cost').value);
      </script>
    <?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_asset_edit.xml');

?>