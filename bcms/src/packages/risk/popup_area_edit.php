<?php

include_once 'include.php';
include_once $handlers_ref . "select/QueryGridGroupResourceSearch.php";
include_once $handlers_ref . "grid/QueryGridPlaceSearch.php";

include_once $handlers_ref . "select/QuerySelectPlaceUN.php";

function save($poContext,$pbHasSensitiveChanges,$piContextId=0){
	if($piContextId){
		$poContext->update($piContextId,true,$pbHasSensitiveChanges);
	}else{
		$piContextId = $poContext->insert(true);
	}

	echo "soWindow = soPopUpManager.getPopUpById('popup_area_edit').getOpener();"
	."if(soWindow.refresh_grid) soWindow.refresh_grid();"
	."soPopUpManager.closePopUp('popup_area_edit');";
}

function getAreaFromFields(){
	$moArea = new RMArea();

	$msAreaName = FWDWebLib::getObject('area_name')->getValue();
	$msAreaDescription = FWDWebLib::getObject('area_description')->getValue();
	$miAreaParent = FWDWebLib::getObject('root_area_id')->getValue();

	if($miAreaParent) $moArea->setFieldValue('area_parent_id', $miAreaParent);
	$moArea->setFieldValue('area_name', $msAreaName);
	$moArea->setFieldValue('area_description', $msAreaDescription);

	$place = FWDWebLib::getObject('placeId')->getValue();
	$place_id = FWDWebLib::getObject('place')->getValue();
	
	$substitute = FWDWebLib::getObject('substituteId')->getValue();
	$group = FWDWebLib::getObject('groupId')->getValue();
	
	$moArea->setGroup($group);
	$moArea->setSubstitute($substitute);

	if($place)
		$moArea->setFieldValue('area_place', $place);
	else
		$moArea->setFieldValue('area_place', "null");
	
	if($place_id)
		$moArea->setFieldValue('area_place', $place_id);
	else
		$moArea->setFieldValue('area_place', "null");
		
	return $moArea;
}

class ConfirmEditEvent extends FWDRunnable {
	public function run(){
		$miAreaId = FWDWebLib::getObject('area_id')->getValue();
		$moArea = getAreaFromFields();
		save($moArea,true,$miAreaId);
	}
}

class SaveAreaEvent extends FWDRunnable {
	public function run(){
		$miAreaId = FWDWebLib::getObject('area_id')->getValue();
		$moArea = getAreaFromFields();
		$moArea->setHash(FWDWebLib::getObject('hash')->getValue());

		$mbHasSensitiveChanges = $moArea->hasSensitiveChanges();
		$miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
		
	  if($moArea->getGroup() == null || $moArea->getGroup() == "null"){
		  echo "gobi('warning_invalid_group').show();";
		}
		else if($moArea->getSubstitute() && $moArea->getGroup() && $moArea->getSubstitute() != "null" && $moArea->getGroup() != "null" && $moArea->getSubstitute() == $moArea->getGroup()){
			echo "gobi('warning_substitute_asset').show();";
		} 
		 
		else {
			save($moArea,$mbHasSensitiveChanges,$miAreaId);
		}
	}
}

class DownloadFileEvent extends FWDRunnable {
	public function run() {
		$moWebLib = FWDWebLib::getInstance();
		$miUserId = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();

		$msPath = $moWebLib->getSysRef()."packages/policy/".$moPMDocInstance->getFieldValue('doc_instance_path');
		$msFileName = $moPMDocInstance->getFieldValue('doc_instance_file_name');
		header('Cache-Control: ');// leave blank to avoid IE errors
		header('Pragma: ');// leave blank to avoid IE errors
		header('Content-type: application/octet-stream');
		header('Content-Disposition: attachment; filename="'.$msFileName.'"');
		$moCrypt = new FWDCrypt();
		$moFP = fopen($msPath,'rb');
		$moCrypt->setIV(fread($moFP,strlen($moCrypt->getIV())));
		while(!feof($moFP)) {
			echo $moCrypt->decryptNoBase64(fread($moFP,16384));
		}
		fclose($moFP);
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(''));
		$moStartEvent->addAjaxEvent(new SaveAreaEvent('save_area_event'));
		$moStartEvent->addAjaxEvent(new ConfirmEditEvent('confirm_edit'));
		$moStartEvent->addSubmitEvent(new DownloadFileEvent('download_file'));

		$defaultFinderSubstitute = new DefaultFinder('search_substitute');
		$defaultFinderSubstitute->set('substitute', new QueryGridGroupResourceSearch(FWDWebLib::getConnection()));
		$moStartEvent->addAjaxEvent($defaultFinderSubstitute);

		$defaultFinderGroup = new DefaultFinder('search_group');
		$defaultFinderGroup->set('group', new QueryGridGroupResourceSearch(FWDWebLib::getConnection()));
		$moStartEvent->addAjaxEvent($defaultFinderGroup);
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$miAreaId = FWDWebLib::getObject('param_area_id')->getValue();
		FWDWebLib::getObject('area_id')->setValue($miAreaId);
		$moWindowTitle = FWDWebLib::getObject('window_title');

		$moHandler = new QuerySelectPlaceUN(FWDWebLib::getConnection());
		$moSelect = FWDWebLib::getObject('place');
		$moSelect->setQueryHandler($moHandler);
		$moSelect->populate();

		if($miAreaId){
			$moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_area_editing', 'Edi��o de Unidade de Neg�cio'));

			$moArea = new RMArea();
			$moArea->fetchById($miAreaId);
			FWDWebLib::getObject('area_name')->setValue($moArea->getFieldValue('area_name'));
			
			FWDWebLib::getObject('area_description')->setValue($moArea->getFieldValue('area_description'));
			FWDWebLib::getObject('hash')->setValue($moArea->getHash());

			FWDWebLib::getObject('substituteId')->setValue($moArea->getSubstitute());
			FWDWebLib::getObject('substitute')->setValue($moArea->getSubstituteName());
			FWDWebLib::getObject('groupId')->setValue($moArea->getGroup());
			FWDWebLib::getObject('group')->setValue($moArea->getGroupName());

			if($moArea->getFieldValue('area_place')){
				$place = new CMPlace();
				$place->fetchById($moArea->getFieldValue('area_place'));
				FWDWebLib::getObject('placeId')->setValue($moArea->getFieldValue('area_place'));
				FWDWebLib::getObject('place')->checkItem($place->getFieldValue('place_id'));
			}
		}else{
			//teste para verificar se o sistema n�o est� sendo hakeado
			$moCtxUserTest = new RMArea();
			$moCtxUserTest->testPermissionToInsert();

			$moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
			$miUserId = $moSession->getUserId();
			$moUser = new ISMSUser();
			$moUser->fetchById($miUserId);
			$moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_un_adding', 'Adi��o de Unidade de Neg�cio'));		
			
			/* Adicionar automaticamente o Local, caso uma nova Unidade de Neg�cio for adicionada a partir dos Locais */
			$place_id = FWDWebLib::getObject('place_id')->getValue();
			
			$cmPlace = new CMPlace();
			$cmPlace->fetchById($place_id);
			
			if($place_id)
				FWDWebLib::getObject('place')->checkItem($cmPlace->getFieldValue('place_id'));
			else
				FWDWebLib::getObject('place')->setValue("");

		}
		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
<script language="javascript">
        gebi('area_name').focus();
        
        var subs = false;

	   		function setGroup(id, name, acronym){
     	 		if(subs){
     	 			gebi("substituteId").value = id;
     	         	gebi("substitute").value = name;
     	 		}else{
     	 			gebi("groupId").value = id;
     	         	gebi("group").value = name;
     	 		}
      	}
  
   	   	function setPlace(id, name, desc){
  	 		gebi("placeId").value = id;
  	        gebi("place").value = name;
       	}

      	function setSubstitute(sub){
      		subs = sub;
      	}
      </script>
		<?
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_area_edit.xml');

?>