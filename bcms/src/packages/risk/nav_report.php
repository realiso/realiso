<?php
include_once "include.php";
include_once $handlers_ref . "QueryContextHierarchicalStructure.php";
include_once $handlers_ref . "select/QuerySelectProcess.php";
include_once $handlers_ref . "select/QuerySelectParameters.php";
include_once $handlers_ref . "QueryContextClassification.php";
include_once $handlers_ref . "select/QuerySelectStandard.php";
include_once $handlers_ref . "select/QuerySelectAsset.php";
include_once $handlers_ref . "QueryGetReportClassification.php";

/*
 * Colocar aqui todos os relat�rios que possuem filtros.
 * Para cada linha (relat�rio) da grid � criado um evento
 * que faz com que quando um relat�rio seja selecionado,
 * ele esconda todos os filtros da tela e mostre somente
 * os filtros pertinentes ao relat�rio clicado.
 */
class GridReports extends FWDDrawGrid {
  public function drawItem(){    
    switch($this->ciColumnIndex){
      case 2:
        $moEvent = new FWDClientEvent();
        $moEvent->setAttrEvent('onClick');
        $msEvent = "hide_all_filters();hide_all_warnings();";
        switch ($this->caData[1]) {
          case 'report_risk_values':
            $msEvent .= "show_filter('report_risk_filter');";
          break;
          case 'report_risks_by_control':
            $msEvent .= "show_filter_risk_control('report_risk_filter');";
          break;
          
          case 'report_pendant_tasks':
            $msEvent .= "show_filter('report_pendant_tasks_filter');";
          break;
          
          case 'report_asset':
            $msEvent .= "show_filter('report_asset_filter');";
          break;
          
          case 'report_risks_by_asset':
            $msEvent .= "show_filter('report_risk_filter');";
            $msEvent .= "show_filter('report_risks_by_asset_filter');";
          break;
          
          case 'report_risks_by_process':
            $msEvent .= "show_filter('report_risk_filter');";
            $msEvent .= "show_filter('report_risks_by_process_filter');";
          break;
          
          case 'report_control_efficiency':
            $msEvent .= "show_filter('report_control_effiency_filter');";            
          break;
          
          case 'report_control_planning':
            $msEvent .= "show_filter('report_control_planning_filter');";            
          break;
          
          case 'report_statement_of_applicability':
            $msEvent .= "show_filter('report_statement_of_applicability_filter');";            
          break;
          
          case 'report_risk_impact':
          case 'report_asset_importance':
            $msEvent .= "show_filter('report_parameters_filter');";            
          break;
          
          case 'report_risks_by_area':
            $msEvent .= "show_filter('report_risk_filter');";
            $msEvent .= "show_filter('report_risks_by_area_filter');";
          break;
          
          case 'report_events_by_category':
            $msEvent .= "show_filter('report_events_by_category_filter');";
          break;
          
          case 'report_user_responsabilities':
            $msEvent .= "show_filter('report_user_responsabilities_filter');";
          break;
          
          case 'report_element_classification':
            $msEvent .= "show_filter('report_element_classification_filter');";
          break;
          
          case 'report_conformity':
            $msEvent .= "show_filter('report_conformity_filter');";
          break;
          
          case 'report_asset_deps':
            $msEvent .= "show_filter('report_asset_deps_filter');";
          break;
          
          case 'report_duplicated_risks':
            $msEvent .= "show_filter('report_duplicated_risks_filter');";
          break;
          
          default:
            $msEvent .= "js_show('no_filter_available');";
          break;
        }
        $moEvent->setAttrValue($msEvent);
        $this->coCellBox->addObjFWDEvent($moEvent);
        return parent::drawItem();
      break;
      
      default:
        return parent::drawItem();
      break;
    }
  }
}

class ISMSReportEvent extends FWDRunnable {

  private function getTypePriorityFilter($psClassif){
    //values of $psClassif
    //area_type, process_type, risk_type, control_type, event_type, area_priority, process_priority
    $maSelectedItens = FWDWebLib::getObject('ctrl_type_prio')->getAllItemsCheck();
    $maValues = array();
    $mbHasFilter = false;
    foreach($maSelectedItens as $moItem){
      if($moItem->getAttrKey()=="chk_".$psClassif){
        //filtro para esse tipo esta abilitado
        $maFiltersSelect = FWDWebLib::getObject('sel_'.$psClassif)->getItems();
        foreach($maFiltersSelect as $moFilter){
          if($moFilter->getAttrCheck()==true){
            $mbHasFilter = true;
            $moParameterName = new ISMSContextClassification();
            $moParameterName->fetchById($moFilter->getAttrKey());
            $maValues[$moFilter->getAttrKey()] = $moParameterName->getFieldValue('classif_name');
          }
        }
        if(!$mbHasFilter){
          $maValues['null'] = (strstr($psClassif,"prio") ?  FWDLanguage::getPHPStringValue('mx_no_priority', 'Sem Prioridade')
                                                         :  FWDLanguage::getPHPStringValue('mx_no_type', 'Sem Tipo') );
           ;
        }
        break;
      }
    }
    return $maValues;
  }

  private function getReportRiskValuesFilter($poFilterObject) {
    $maCheckboxValue = FWDWebLib::getObject('risk_filter_checkbox')->getAllItemsCheck();
    $msRadioboxValue = FWDWebLib::getObject('risk_filter_radiobox')->getValue();
    
    if ($msRadioboxValue == ':residual') $poFilterObject->setRiskValueType(RISK_VALUE_TYPE_RESIDUAL_VALUE);
    else if ($msRadioboxValue == ':real') $poFilterObject->setRiskValueType(RISK_VALUE_TYPE_REAL_VALUE);
    
    foreach($maCheckboxValue as $moItem) {
      switch ($moItem->getAttrKey()) {
        case 'high':
          $poFilterObject->showHighRisk(true);
        break;
        case 'medium':
          $poFilterObject->showMediumRisk(true);
        break;
        case 'low':
          $poFilterObject->showLowRisk(true);
        break;
        case 'no_values':
          //criado esse teste pois o relat�rio de riscos por controle n�o deve ter o filtro
          //de riscos desparametrizados, pois esse caso n�o deve ocorrer no sistema
          //jah q o sistema somente deixa os riscos parametrizados serem relacionados 
          //com os controles
          if(FWDWebLib::getObject('var_pog_disable_risk_no_value')->getValue()=='false')
             $poFilterObject->showRiskWithNoValues(true);
        break;
      }
    }
    $poFilterObject->setRiskClassifType($this->getTypePriorityFilter('risk_type'));
    
    return $poFilterObject;
  }

  private function getReportAssetFilter() {
    $msRadioboxValue = FWDWebLib::getObject('asset_filter_radiobox')->getValue();
    $msReportType = substr( $msRadioboxValue , 1 , strlen($msRadioboxValue)-1 );
    $moReportFilter = new ISMSReportAssetFilter();
    $moReportFilter->setReportType($msReportType);
    switch ($msReportType) {
      case 'report_asset_by_area_and_process':
          $moReportFilter->setAreaClassifType($this->getTypePriorityFilter('area_type'));
          $moReportFilter->setProcessClassifType($this->getTypePriorityFilter('process_type'));
          $moReportFilter->setAreaClassifPriority($this->getTypePriorityFilter('area_priority'));
          $moReportFilter->setProcessClassifPriority($this->getTypePriorityFilter('process_priority'));
      break;
      case 'report_asset_by_area':
          $moReportFilter->setAreaClassifType($this->getTypePriorityFilter('area_type'));
          $moReportFilter->setAreaClassifPriority($this->getTypePriorityFilter('area_priority'));
      break;
      case 'report_asset_by_process':
          $moReportFilter->setProcessClassifType($this->getTypePriorityFilter('process_type'));
          $moReportFilter->setProcessClassifPriority($this->getTypePriorityFilter('process_priority'));
      break;
    }
    return $moReportFilter;
  }
  
  private function getReportRisksWithNoValuesFilter() {
    $moReportFilter = new ISMSReportRiskValuesFilter();
    $moReportFilter->setRiskValueType(RISK_VALUE_TYPE_RESIDUAL_VALUE);
    $moReportFilter->showRiskWithNoValues(true);
    $moReportFilter->setRiskClassifType($this->getTypePriorityFilter('risk_type'));
    return $moReportFilter;
  }

  private function getReportRisksByAssetFilter() {
    $moReportFilter = $this->getReportRiskValuesFilter(new ISMSReportRisksByAssetFilter());
    $miCategory = FWDWebLib::getObject('select_category')->getValue();
    if ($miCategory) $moReportFilter->setCategory($miCategory);
    return $moReportFilter;
  }

  private function getReportRisksByControlFilter() {
    $moReportFilter = $this->getReportRiskValuesFilter(new ISMSReportRisksByControlFilter());
    $moReportFilter->setControlClassifType($this->getTypePriorityFilter('control_type'));
    return $moReportFilter;
  }

  private function getReportEventsByCategoryFilter() {
    $moReportFilter = new ISMSReportEventsByCategoryFilter();
    $moReportFilter->setEventClassifType($this->getTypePriorityFilter('event_type'));
    $miCategory = FWDWebLib::getObject('select_category2')->getValue();   
    if ($miCategory) $moReportFilter->setCategory($miCategory);
    return $moReportFilter;
  }

  private function getReportRisksByAreaFilter() {
    $moReportFilter = $this->getReportRiskValuesFilter(new ISMSReportRisksByAreaFilter());
    
    $moReportFilter->setAreaClassifPriority($this->getTypePriorityFilter('area_priority'));
    $moReportFilter->setAreaClassifType($this->getTypePriorityFilter('area_type'));
    
    $miArea = FWDWebLib::getObject('select_area')->getValue();
    if ($miArea) $moReportFilter->setArea($miArea);
    $maCheckboxValue = FWDWebLib::getObject('risks_by_area_filter_checkbox')->getAllItemsCheck();    
    foreach($maCheckboxValue as $moItem) {
      switch ($moItem->getAttrKey()) {
        case 'organize_by_process':
          $moReportFilter->organizeByProcess();
          $moReportFilter->setProcessClassifPriority($this->getTypePriorityFilter('process_priority'));
          $moReportFilter->setProcessClassifType($this->getTypePriorityFilter('process_type'));
        break;
        case 'organize_by_asset':
          $moReportFilter->organizeByAsset();
        break;
      }
    }    
    return $moReportFilter;
  }

  private function getReportRisksByProcessFilter() {
    $moReportFilter = $this->getReportRiskValuesFilter(new ISMSReportRisksByProcessFilter());
    $moReportFilter->setProcessClassifType($this->getTypePriorityFilter('process_type'));
    $moReportFilter->setProcessClassifPriority($this->getTypePriorityFilter('process_priority'));
    
    $miProcess = FWDWebLib::getObject('select_process')->getValue();
    if ($miProcess) $moReportFilter->setProcess($miProcess);
    $maCheckboxValue = FWDWebLib::getObject('report_risks_by_process_checkbox')->getAllItemsCheck();    
    if(count($maCheckboxValue)) $moReportFilter->organizeByAsset();    
    return $moReportFilter;
  }

  private function getReportControlEfficiencyFilter() {
    $msRadioboxValue = FWDWebLib::getObject('control_effiency_filter_radiobox')->getValue();    
    $msReportType = substr($msRadioboxValue,1,strlen($msRadioboxValue)-1);    
    $moReportFilter = new ISMSReportControlEfficiencyFilter();
    $moReportFilter->setControlClassifType($this->getTypePriorityFilter('control_type'));
    switch ($msReportType) {
      case 'only_efficient':
        $moReportFilter->showOnlyEfficient();
      break;
      
      case 'only_not_efficient':
        $moReportFilter->showOnlyNotEfficient();
      break;      
    }    
    return $moReportFilter;  
  }

  private function getReportControlTestFilter() {
    $msRadioboxValue = FWDWebLib::getObject('control_test_filter_radiobox')->getValue();
    $msReportType = substr($msRadioboxValue,1,strlen($msRadioboxValue)-1);    
    $moReportFilter = new ISMSReportControlTestFilter();
    $moReportFilter->setControlClassifType($this->getTypePriorityFilter('control_type'));
    switch ($msReportType) {
      case 'only_test_no':
        $moReportFilter->showOnlyTestOk();
      break;
      
      case 'only_test_yes':
        $moReportFilter->showOnlyTestNotOk();
      break;      
    }    
    return $moReportFilter;  
  }

  private function getReportControlPlanningFilter() {
    $msRadioboxValue = FWDWebLib::getObject('control_planning_filter_radiobox')->getValue();    
    $msReportType = substr($msRadioboxValue,1,strlen($msRadioboxValue)-1);    
    $moReportFilter = new ISMSReportControlPlanningFilter();
    $moReportFilter->setControlClassifType($this->getTypePriorityFilter('control_type'));
    switch ($msReportType) {
      case 'only_planned':
        $moReportFilter->showOnlyPlanned();
      break;
      
      case 'only_implemented':
        $moReportFilter->showOnlyImplemented();
      break;      
    }    
    return $moReportFilter;  
  }

  private function getReportTop10AssetsWithMoreRisksFilter() {
    $moReportFilter = new ISMSReportTop10AssetsWithMoreRisksFilter();
    $moReportFilter->setRiskClassifType($this->getTypePriorityFilter('risk_type'));
    $moReportFilter->filterOnlyByHighRisks();
    return $moReportFilter;
  }
  
  private function getReportParametersFilter() {
    $maParameters = FWDWebLib::getObject('select_parameters')->getValue();
    $moReportFilter = new ISMSReportParametersFilter();
    $moReportFilter->setParameters($maParameters);
    return $moReportFilter;
  }
  
  private function getReportRiskImpactFilter() {
    $moReportFilter = new ISMSReportRiskImpactFilter();
    $moReportFilter->setRiskClassifType($this->getTypePriorityFilter('risk_type'));
    $moReportFilter->setParameters(FWDWebLib::getObject('select_parameters')->getValue());
    return $moReportFilter;
  }
  
  private function getReportUserResponsabilitiesFilter() {
    $msType = FWDWebLib::getObject('report_user_responsabilities_filter_controller')->getValue();
    $moReportFilter = new ISMSReportUserResponsabilitiesFilter();
    $moReportFilter->setResponsibleType($msType);
    $moReportFilter->setAreaClassifType($this->getTypePriorityFilter('area_type'));
    $moReportFilter->setProcessClassifType($this->getTypePriorityFilter('process_type'));
    $moReportFilter->setRiskClassifType($this->getTypePriorityFilter('risk_type'));
    $moReportFilter->setControlClassifType($this->getTypePriorityFilter('control_type'));
    $moReportFilter->setAreaClassifPriority($this->getTypePriorityFilter('area_priority'));
    $moReportFilter->setProcessClassifPriority($this->getTypePriorityFilter('process_priority'));
    return $moReportFilter;
  }
  
  private function getReportControlsByRiskFilter(){
    $moReportFilter = new ISMSReportGenericClassifTypePrioFilter();
    $moReportFilter->setControlClassifType($this->getTypePriorityFilter('control_type'));
    $moReportFilter->setRiskClassifType($this->getTypePriorityFilter('risk_type'));
    return $moReportFilter;
  }
  
  private function getReportControlGenericFilter() {
    $moReportFilter = new ISMSReportGenericClassifTypePrioFilter();
    $moReportFilter->setControlClassifType($this->getTypePriorityFilter('control_type'));
    return $moReportFilter;
  }
  
  private function getReportAreasWithoutProcessFilter() {
    $moReportFilter = new ISMSReportGenericClassifTypePrioFilter();
    $moReportFilter->setAreaClassifPriority($this->getTypePriorityFilter('area_priority'));
    $moReportFilter->setAreaClassifType($this->getTypePriorityFilter('area_type'));
    return $moReportFilter;
  }
  
  private function getReportProcessesWithoutAssets() {
    $moReportFilter = new ISMSReportGenericClassifTypePrioFilter();
    $moReportFilter->setProcessClassifType($this->getTypePriorityFilter('process_type'));
    $moReportFilter->setProcessClassifPriority($this->getTypePriorityFilter('process_priority'));
    return $moReportFilter;
  }
  private function getReportTotalImpactByArea(){
    $moReportFilter = new ISMSReportGenericClassifTypePrioFilter();
    $moReportFilter->setAreaClassifType($this->getTypePriorityFilter('area_type'));
    $moReportFilter->setAreaClassifPriority($this->getTypePriorityFilter('area_priority'));
    return $moReportFilter;
  }
  private function getReportTotalImpactByProcess(){
    $moReportFilter = new ISMSReportGenericClassifTypePrioFilter();
    $moReportFilter->setProcessClassifType($this->getTypePriorityFilter('process_type'));
    $moReportFilter->setProcessClassifPriority($this->getTypePriorityFilter('process_priority'));
    return $moReportFilter;
  }
  private function getControlCostByProcessFilter(){
    $moReportFilter = new ISMSReportGenericClassifTypePrioFilter();
    $moReportFilter->setControlClassifType($this->getTypePriorityFilter('control_type'));
    $moReportFilter->setProcessClassifType($this->getTypePriorityFilter('process_type'));
    $moReportFilter->setProcessClassifPriority($this->getTypePriorityFilter('process_priority'));
    return $moReportFilter;
  }
  private function getControlCostByAreaFilter(){
    $moReportFilter = new ISMSReportGenericClassifTypePrioFilter();
    $moReportFilter->setControlClassifType($this->getTypePriorityFilter('control_type'));
    $moReportFilter->setAreaClassifType($this->getTypePriorityFilter('area_type'));
    $moReportFilter->setAreaClassifPriority($this->getTypePriorityFilter('area_priority'));
    return $moReportFilter;
  }
  private function getRiskTreatmentPlanFilter(){
    $moReportFilter = new ISMSReportGenericClassifTypePrioFilter();
    $moReportFilter->setRiskClassifType($this->getTypePriorityFilter('risk_type'));
    $moReportFilter->setControlClassifType($this->getTypePriorityFilter('control_type'));
    return $moReportFilter;
  }
  private function getReportTop10AssetsWithRisksFilter(){
    $moReportFilter = new ISMSReportGenericClassifTypePrioFilter();
    $moReportFilter->setRiskClassifType($this->getTypePriorityFilter('risk_type'));
    return $moReportFilter;
  }
  private function getTop10RisksByArea(){
    $moReportFilter = new ISMSReportGenericClassifTypePrioFilter();
    $moReportFilter->setRiskClassifType($this->getTypePriorityFilter('risk_type'));
    $moReportFilter->setAreaClassifType($this->getTypePriorityFilter('area_type'));
    $moReportFilter->setAreaClassifPriority($this->getTypePriorityFilter('area_priority'));
    return $moReportFilter;
  }
  private function getTop10RisksByProcess(){
    $moReportFilter = new ISMSReportGenericClassifTypePrioFilter();
    $moReportFilter->setRiskClassifType($this->getTypePriorityFilter('risk_type'));
    $moReportFilter->setProcessClassifType($this->getTypePriorityFilter('process_type'));
    $moReportFilter->setProcessClassifPriority($this->getTypePriorityFilter('process_priority'));
    return $moReportFilter;
  }
  private function getRiskAmountByProcessFilter(){
    $moReportFilter = new ISMSReportGenericClassifTypePrioFilter();
    $moReportFilter->setRiskClassifType($this->getTypePriorityFilter('risk_type'));
    $moReportFilter->setProcessClassifType($this->getTypePriorityFilter('process_type'));
    $moReportFilter->setProcessClassifPriority($this->getTypePriorityFilter('process_priority'));
    return $moReportFilter;
  }
  private function getRiskAmountByAreaFilter(){
    $moReportFilter = new ISMSReportGenericClassifTypePrioFilter();
    $moReportFilter->setRiskClassifType($this->getTypePriorityFilter('risk_type'));
    $moReportFilter->setAreaClassifType($this->getTypePriorityFilter('area_type'));
    $moReportFilter->setAreaClassifPriority($this->getTypePriorityFilter('area_priority'));
    return $moReportFilter;
  }
  private function getRiskStatusByAreaFilter(){
    $moReportFilter = new ISMSReportGenericClassifTypePrioFilter();
    $moReportFilter->setAreaClassifType($this->getTypePriorityFilter('area_type'));
    $moReportFilter->setAreaClassifPriority($this->getTypePriorityFilter('area_priority'));
    return $moReportFilter;
  }
  private function getRiskStatusByProcessFilter(){
    $moReportFilter = new ISMSReportGenericClassifTypePrioFilter();
    $moReportFilter->setProcessClassifType($this->getTypePriorityFilter('process_type'));
    $moReportFilter->setProcessClassifPriority($this->getTypePriorityFilter('process_priority'));
    return $moReportFilter;
  }
  private function getPendantContextsFilter(){
    $moReportFilter = new ISMSReportGenericClassifTypePrioFilter();
    $moReportFilter->setAreaClassifType($this->getTypePriorityFilter('area_type'));
    $moReportFilter->setProcessClassifType($this->getTypePriorityFilter('process_type'));
    $moReportFilter->setRiskClassifType($this->getTypePriorityFilter('risk_type'));
    $moReportFilter->setControlClassifType($this->getTypePriorityFilter('control_type'));
    $moReportFilter->setAreaClassifPriority($this->getTypePriorityFilter('area_priority'));
    $moReportFilter->setProcessClassifPriority($this->getTypePriorityFilter('process_priority'));
    $moReportFilter->setEventClassifType($this->getTypePriorityFilter('event_type'));
    
    return $moReportFilter;
  }
  private function getSOAFilter(){
    $moReportFilter = new ISMSReportStatementOfApplicabilityFilter();
    FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->deleteAttribute("isPreReport");
    $miId = FWDWebLib::getObject('var_stardard_filter_id')->getValue();
    if($miId){
      $moStd = new RMStandard();
      $moStd->fetchById($miId);
      $moReportFilter->setStandardFilter($miId);
      $moReportFilter->setStandardFilterName($moStd->getFieldValue('standard_name'));
    }else{
      $moReportFilter->setStandardFilterName(FWDLanguage::getPHPStringValue('st_all_standards', 'Todas'));
    }
    $moReportFilter->setControlClassifType($this->getTypePriorityFilter('control_type'));
    return $moReportFilter;
  }
  
  private function getElementClassificationFilter() {
  	$moReportFilter = new ISMSReportGenericClassifTypePrioFilter();
    $moReportFilter->setAreaClassifType($this->getTypePriorityFilter('area_type'));
    $moReportFilter->setProcessClassifType($this->getTypePriorityFilter('process_type'));
    $moReportFilter->setRiskClassifType($this->getTypePriorityFilter('risk_type'));
    $moReportFilter->setControlClassifType($this->getTypePriorityFilter('control_type'));
    $moReportFilter->setAreaClassifPriority($this->getTypePriorityFilter('area_priority'));
    $moReportFilter->setProcessClassifPriority($this->getTypePriorityFilter('process_priority'));
    $moReportFilter->setEventClassifType($this->getTypePriorityFilter('event_type'));    
    return $moReportFilter;
  }
  
  private function getConformityFilter() {
    $moReportFilter = new ISMSReportConformityFilter();    
    $moReportFilter->setControlClassifType($this->getTypePriorityFilter('control_type'));
    $moReportFilter->setStandard(FWDWebLib::getObject('select_conformity')->getValue());    
    return $moReportFilter;
  }
  
  private function getReportAssetDepsFilter() {
    $moReportFilter = new ISMSReportAssetDependenciesAndDependentsFilter();
    $moReportFilter->setAssetId(FWDWebLib::getObject('select_asset')->getValue());
    return $moReportFilter;
  }
  
  /*
   * Essa fun��o serve para retornar o filtro do relat�rio.
   * De acordo com o relat�rio selecionado, chama um fun��o
   * espec�fica para montar o filtro do relat�rio em quest�o.
   */
  private function getFilter() {
    $maValue = FWDWebLib::getObject('grid_reports')->getValue();
    $moReportFilter = null;
    switch($maValue[0]) {
      case 'report_risk_values':
        $moReportFilter = $this->getReportRiskValuesFilter(new ISMSReportRiskValuesFilter());
      
      break;
      case 'report_risks_by_control':
        $moReportFilter = $this->getReportRisksByControlFilter();
      break;
      
      case 'report_risks_with_no_values':
        $moReportFilter = $this->getReportRisksWithNoValuesFilter();
      break;
      
      case 'report_asset':
        $moReportFilter = $this->getReportAssetFilter();
      break;      
      
      case 'report_risks_by_asset':
        $moReportFilter = $this->getReportRisksByAssetFilter();
      break;
      
      case 'report_risks_by_process':
        $moReportFilter = $this->getReportRisksByProcessFilter();
      break;
      
      case 'report_control_efficiency':
        $moReportFilter = $this->getReportControlEfficiencyFilter();
      break;
      
      case 'report_control_planning':
        $moReportFilter = $this->getReportControlPlanningFilter();
      break;
      
      case 'report_top10_assets_with_more_high_risks':
        $moReportFilter = $this->getReportTop10AssetsWithMoreRisksFilter();
      break;      
      /*feito*/
      case 'report_top10_assets_with_more_risks':
        $moReportFilter = new ISMSReportTop10AssetsWithMoreRisksFilter();
        $moReportFilter->setRiskClassifType($this->getTypePriorityFilter('risk_type'));
      break;
      
      case 'report_risk_impact':
        $moReportFilter = $this->getReportRiskImpactFilter();
      break;
      
      case 'report_asset_importance':
        $moReportFilter = $this->getReportParametersFilter();
      break;
      
      case 'report_risks_by_area':
        $moReportFilter = $this->getReportRisksByAreaFilter();
      break;
      
      case 'report_events_by_category':
        $moReportFilter = $this->getReportEventsByCategoryFilter();
      break;
      
      case 'report_user_responsabilities':
        $moReportFilter = $this->getReportUserResponsabilitiesFilter();
      break;      
      case 'report_controls_by_risk':
        $moReportFilter = $this->getReportControlsByRiskFilter();
      break;
      case 'report_controls_not_measured':
        $moReportFilter = $this->getReportControlGenericFilter();
      break;
      case 'report_areas_without_processes':
        $moReportFilter = $this->getReportAreasWithoutProcessFilter();
      break;
      case 'report_processes_without_assets':
        $moReportFilter = $this->getReportProcessesWithoutAssets();
      break;
      case 'report_total_impact_by_area':
        $moReportFilter = $this->getReportTotalImpactByArea();
      break;
      case 'report_total_impact_by_process':
        $moReportFilter = $this->getReportTotalImpactByProcess();
      break;
      case 'report_control_cost_by_process':
        $moReportFilter = $this->getControlCostByProcessFilter();
      break;
      case 'report_control_cost_by_area':
        $moReportFilter = $this->getControlCostByAreaFilter();
      break;
      case 'report_risk_treatment_plan':
        $moReportFilter = $this->getRiskTreatmentPlanFilter();
      break;
      case 'report_top10_risks_by_area':
        $moReportFilter = $this->getTop10RisksByArea();
      break;
      case 'report_top10_risks_by_process':
        $moReportFilter = $this->getTop10RisksByProcess();
      break;
      case 'report_risk_amount_by_area':
        $moReportFilter = $this->getRiskAmountByAreaFilter();
      break;
      case 'report_risk_amount_by_process':
        $moReportFilter = $this->getRiskAmountByProcessFilter();
      break;
      case 'report_risk_status_by_area':
        $moReportFilter = $this->getRiskStatusByAreaFilter();
      break;
      case 'report_risk_status_by_process':
        $moReportFilter = $this->getRiskStatusByProcessFilter();
      break;
      case 'report_pendant_contexts':
        $moReportFilter = $this->getPendantContextsFilter();
      break;
      case 'report_statement_of_applicability':
        $moReportFilter = $this->getSOAFilter();
      break;
      case 'report_control_cost_by_responsible':
      case 'report_control_cost':
      case 'report_controls_without_risks':
      case 'report_control_efficiency_accompaniment':
      case 'report_control_test_accompaniment':
      case 'report_controls_by_responsible':
      case 'report_control_implementation_date':
      case 'report_control_summary':
      case 'report_control_summary_pm':
      case 'report_control_accompaniment':
      case 'report_control_revision_agenda_by_responsible':
      case 'report_control_test_agenda_by_responsible':
        $moReportFilter = $this->getReportControlGenericFilter();
      break;
      
      case 'report_element_classification':
      	$moReportFilter = $this->getElementClassificationFilter();
      break;
      
      case 'report_conformity':
        $moReportFilter = $this->getConformityFilter();
      break;
      
      case 'report_asset_deps':
        $moReportFilter = $this->getReportAssetDepsFilter();
      break;

      default:
        $moReportFilter = new FWDReportFilter();
      break;

    }
    $moReportFilter->setManualClassification(FWDWebLib::getObject('select_report_classification')->getValue());
    $moReportFilter->setFileType(FWDWebLib::getObject('select_report_type')->getValue());
    
    FWDWebLib::getObject('report_comment')->getObjFWDString()->setAttrNoEscape('true');
    
    $moReportFilter->setComment(FWDWebLib::convertToISO(FWDWebLib::getObject('report_comment')->getValue(), true));    
    return $moReportFilter;
  }
  
  /*
   * Fun��o para retornar o relat�rio que deve ser gerado.
   * � utilizada quando o relat�rio que foi clicado deve
   * chamar um outro relat�rio.
   */
  private function getReport() {
    $maGridValue = FWDWebLib::getObject('grid_reports')->getValue();      
    $msReport = $maGridValue[0];
    
    switch ($msReport) {
      case 'report_pendant_tasks':
        $msRadioboxValue = FWDWebLib::getObject('pendant_tasks_filter_radiobox')->getValue();
        return substr($msRadioboxValue,1,strlen($msRadioboxValue)-1);
      break;
      
      case 'report_risks_with_no_values':
        return 'report_risk_values';
      break;
      
      case 'report_top10_assets_with_more_high_risks':
        return 'report_top10_assets_with_more_risks';
      break;
      
      case 'report_element_classification':
      	$maItem = FWDWebLib::getObject('classification_type')->getItems();
        foreach($maItem as $moFilter){
          if($moFilter->getAttrCheck()==true){
            switch ($moFilter->getAttrKey()) {
      				case 'area_type': 				return 'report_areas_by_type';
      				case 'area_priority': 		return 'report_areas_by_priority';
      				case 'process_type': 			return 'report_processes_by_type';
      				case 'process_priority': 	return 'report_processes_by_priority';
      				case 'risk_type': 				return 'report_risks_by_type';
      				case 'event_type': 				return 'report_events_by_type';
      				case 'control_type': 			return 'report_controls_by_type';
      			}
          }
        }
      break;
      
      case 'report_duplicated_risks':
        $msValue = FWDWebLib::getObject('dup_risks_by_asset')->getValue();
        if ($msValue) return 'report_duplicated_risks_by_asset';
        else return 'report_duplicated_risks';
      break;
      
      default:
        return $msReport;
      break;
    }
  }
  
  /*
   * Verifica se o filtro do relat�rio foi preenchido corretamente.
   */
  public function validateFilters() {
    $maValue = FWDWebLib::getObject('grid_reports')->getValue();    
    switch($maValue[0]) {      
      case 'report_risk_values':
      case 'report_risks_by_control':
      case 'report_risks_by_asset':
      case 'report_risks_by_process':
      case 'report_risks_by_area':
        if (count(FWDWebLib::getObject('risk_filter_checkbox')->getAllItemsCheck())) return true;
        else echo "js_show('report_risk_filter_warning');";
      break;
      
      case 'report_risk_impact':
      case 'report_asset_importance':
        if (count(FWDWebLib::getObject('select_parameters')->getValue())) return true;
        else echo "js_show('report_parameters_filter_warning');";
      break;
      
      case 'report_user_responsabilities':
        $msType = FWDWebLib::getObject('report_user_responsabilities_filter_controller')->getValue();
        if ($msType) return true;
        else echo "js_show('report_responsabilities_filter_warning');";
      break;
      
      default:
        return true;
      break;
    }  
  }
  
  public function run(){
    $maValue = FWDWebLib::getObject('grid_reports')->getValue();
    if (isset($maValue[0])) {
      if ($this->validateFilters()) {
        set_time_limit(3000);
    
        $mbDontForceDownload = (FWDWebLib::getObject("select_report_type")->getValue()==REPORT_FILETYPE_HTML);
        
        $moWindowReport = new ISMSReportWindow(RISK_MANAGEMENT_MODE);
        if($mbDontForceDownload) $moWindowReport->open();
    
        $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
        $moSession->deleteAttribute("filter");
        $moSession->addAttribute("filter");
        $moSession->setAttrFilter($this->getFilter());
        $msReport = $this->getReport();
        if($mbDontForceDownload){
          $moWindowReport->setWaitLoadingComplete(true);
          $moWindowReport->loadReport($msReport);
        }
        else $moWindowReport->forceReportDownload($msReport);
      }
    }
    else echo "js_show('warning');";
  }
}

class ISMSPreReportEvent extends FWDRunnable {
  public function run(){
    set_time_limit(3000); 

    $moWindowReport = new ISMSReportWindow(RISK_MANAGEMENT_MODE);
    $moWindowReport->open();

    $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    $moSession->deleteAttribute("filter");
    $moSession->addAttribute("filter");
    $moSession->deleteAttribute("isPreReport");
    $moSession->addAttribute("isPreReport");
    $moSession->setAttrIsPreReport("true2");

    //filtro de norma
    $moReportFilter = new ISMSReportStatementOfApplicabilityFilter();
    $miId = FWDWebLib::getObject('var_stardard_filter_id')->getValue();
    if($miId){
      $moStd = new RMStandard();
      $moStd->fetchById($miId);
      $moReportFilter->setStandardFilter($miId);
      $moReportFilter->setStandardFilterName($moStd->getFieldValue('standard_name'));
    }else{
      $moReportFilter->setStandardFilterName(FWDLanguage::getPHPStringValue('st_all_standards', 'Todas'));
    }
    $moSession->setAttrFilter($moReportFilter);
    
    $moWindowReport->setWaitLoadingComplete(true);
    $moWindowReport->loadReport('report_statement_of_applicability');
  }
}

class ISMSConfirmClearPreReportEvent extends FWDRunnable {
	public function run () {
		$msTitle = FWDLanguage::getPHPStringValue('tt_clear_justificatives','Confirmar limpeza de justificativas');
		$msMessage = FWDLanguage::getPHPStringValue('st_clear_justificatives_confirm',"Voc� tem certeza que deseja limpar as justificativas?");
		$msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
                    "soWindow = soPopUp.getOpener().trigger_event('clear_pre_report_confirmed', 1);";
		ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue);
	}
}

class ISMSClearPreReportEvent extends FWDRunnable {
	public function run() {
		$moBestPractice = new RMBestPractice();
	  	$moBestPractice->cleanJustifications();
	}
}

/*
 * Fun��o que retorna um array associativo do tipo array('report_category' => array('report1', 'report2', ...))
 * para mapear quais relat�rios pertencem a quais categorias 
 */
function getReportCategoryArray() {

  $moConfig = new ISMSConfig();
  $mbHasRevision = $moConfig->getConfig(GENERAL_REVISION_ENABLED);
  $mbHasCost = $moConfig->getConfig(GENERAL_COST_ENABLED);
  $mbHasTests = $moConfig->getConfig(GENERAL_TEST_ENABLED);
  $maCostReports = array();
  if($mbHasCost){
    $maCostReports = array('report_control_cost' => array(FWDLanguage::getPHPStringValue('si_cost_per_control',"Custo por Controle"),"M.RM.6.4.4"),
                          'report_control_cost_by_responsible' => array(FWDLanguage::getPHPStringValue('si_cost_of_controls_per_responsible',"Custo dos Controles por Respons�vel"),"M.RM.6.4.5"),
                          'report_control_cost_by_area' => array(FWDLanguage::getPHPStringValue('si_cost_of_controls_per_area',"Custo dos Controles por �rea"),"M.RM.6.4.6"),
                          'report_control_cost_by_process' => array(FWDLanguage::getPHPStringValue('si_cost_of_controls_per_process',"Custo dos Controles por Processo"),"M.RM.6.4.7"),
                          'report_control_cost_by_asset' => array(FWDLanguage::getPHPStringValue('si_cost_of_controls_per_asset',"Custo dos Controles por Ativo"),"M.RM.6.4.8")    
                      );
  }else{
    $maCostReports = array();
  }

  $maControlReports = array('report_controls_by_responsible' => array(FWDLanguage::getPHPStringValue('si_controls_per_responsible',"Controles por Respons�vel"),"M.RM.6.2.1"),
            'report_controls_by_risk' => array(FWDLanguage::getPHPStringValue('si_controls_per_risk',"Controles por Risco"),"M.RM.6.2.2"),
            'report_control_planning' => array(FWDLanguage::getPHPStringValue('si_planning_of_controls',"Planejamento dos Controles"),"M.RM.6.2.3")
            );
  if($mbHasRevision){
    $maRevAux = array('report_control_implementation_date' => array(FWDLanguage::getPHPStringValue('si_controls_implementation_date',"Data de Implementa��o dos Controles"),"M.RM.6.2.4"),
                      'report_control_efficiency' => array(FWDLanguage::getPHPStringValue('si_controls_efficiency',"Efici�ncia dos Controles"),"M.RM.6.2.5")
                     );
    $maControlReports = array_merge($maControlReports,$maRevAux);
  }
  
  
  if(ISMSLib::hasModule(POLICY_MODE))
    $maControlReports = array_merge($maControlReports,array('report_control_summary_pm' => array(FWDLanguage::getPHPStringValue('si_summary_of_controls',"Resumo de Controles"),"M.RM.6.2.6")));
  else
    $maControlReports = array_merge($maControlReports,array('report_control_summary' => array(FWDLanguage::getPHPStringValue('si_summary_of_controls',"Resumo de Controles"),"M.RM.6.2.6")));  
  
  
  if($mbHasRevision){
      $maRevAux = array( 'report_control_accompaniment' => array(FWDLanguage::getPHPStringValue('si_follow_up_of_controls',"Acompanhamento de Controles"),"M.RM.6.2.7"),
                         'report_control_efficiency_accompaniment' => array(FWDLanguage::getPHPStringValue('si_follow_up_of_controls_efficiency',"Acompanhamento da Efici�ncia dos Controles"),"M.RM.6.2.8"),
                         'report_control_revision_agenda_by_responsible' => array(FWDLanguage::getPHPStringValue('si_control_revision_agenda_by_responsible',"Agenda de Revis�o dos Controles por Respons�vel"),"M.RM.6.2.12")
                       );
     $maControlReports = array_merge($maControlReports,$maRevAux);
  }
  if($mbHasTests){
    $maControlReports = array_merge($maControlReports, array('report_control_test_accompaniment' => array(FWDLanguage::getPHPStringValue('si_follow_up_of_controls_test',"Acompanhamento do Teste dos Controles"),"M.RM.6.2.10") ));  
    $maControlReports = array_merge($maControlReports, array('report_control_test_agenda_by_responsible' => array(FWDLanguage::getPHPStringValue('si_control_test_agenda_by_responsible',"Agenda de Teste dos Controles por Respons�vel"),"M.RM.6.2.13") ));
    
  }
  if(($mbHasRevision)||($mbHasTests)){
      $maControlReports = array_merge($maControlReports,array('report_controls_not_measured' => array(FWDLanguage::getPHPStringValue('si_not_measured_controls',"Controles n�o Medidos"),"M.RM.6.2.9")));
  }

  $maControlReports = array_merge($maControlReports,array('report_conformity' => array(FWDLanguage::getPHPStringValue('si_report_conformity',"Conformidade"),"M.RM.6.2.11")));

	return array(
		'risk_reports' =>
			array('report_risk_values' => array(FWDLanguage::getPHPStringValue('si_risks_values',"Valores dos Riscos"),"M.RM.6.1.1"),						
						'report_risks_by_process' => array(FWDLanguage::getPHPStringValue('si_risks_per_process',"Riscos por Processo"),"M.RM.6.1.2"),						
						'report_risks_by_asset' => array(FWDLanguage::getPHPStringValue('si_risks_per_asset',"Riscos por Ativo"),"M.RM.6.1.3"),
						'report_risks_by_control' => array(FWDLanguage::getPHPStringValue('si_risks_per_control',"Riscos por Controle"),"M.RM.6.1.4"),
						'report_risk_impact' => array(FWDLanguage::getPHPStringValue('si_risk_impact',"Impacto do Risco"),"M.RM.6.1.5"),
						'report_asset_importance' => array(FWDLanguage::getPHPStringValue('si_assets_importance',"Import�ncia dos Ativos"),"M.RM.6.1.6"),
						'report_risks_by_area' => array(FWDLanguage::getPHPStringValue('si_risks_per_area',"Riscos por �rea"),"M.RM.6.1.7"),
						'report_events_by_category' => array(FWDLanguage::getPHPStringValue('si_checklist_per_event',"Checklist por Evento"),"M.RM.6.1.8")
			),
		'control_reports' => $maControlReports
    ,
		'risk_management_reports' =>
			array('report_pendant_tasks' => array(FWDLanguage::getPHPStringValue('si_pending_tasks',"Tarefas Pendentes"),"M.RM.6.3.1"),
						'report_areas_without_processes' => array(FWDLanguage::getPHPStringValue('si_areas_without_processes',"�reas sem Processos"),"M.RM.6.3.2"),
						'report_assets_without_risks' => array(FWDLanguage::getPHPStringValue('si_assets_without_risks',"Ativos sem Eventos de Risco"),"M.RM.6.3.3"),
						'report_processes_without_assets' => array(FWDLanguage::getPHPStringValue('si_processes_without_assets',"Processos sem Ativos"),"M.RM.6.3.4"),
						'report_risks_with_no_values' => array(FWDLanguage::getPHPStringValue('si_non_parameterized_risks',"Riscos n�o Estimados"),"M.RM.6.3.5"),
						'report_controls_without_risks' => array(FWDLanguage::getPHPStringValue('si_controls_without_risks',"Controles sem Riscos"),"M.RM.6.3.6"),
            'report_duplicated_risks' => array(FWDLanguage::getPHPStringValue('si_duplicated_risks',"Riscos Duplicados"),"M.RM.6.3.7")
			),
		'financial_reports' => $maCostReports
    ,
		'iso_27001_reports' =>
			array('report_statement_of_applicability' => array(FWDLanguage::getPHPStringValue('si_statement_of_applicability',"Declara��o de Aplicabilidade"),"M.RM.6.5.1"),
						'report_risk_treatment_plan' => array(FWDLanguage::getPHPStringValue('si_plan_of_risks_treatment',"Plano de Tratamento dos Riscos"),"M.RM.6.5.2"),
			),
		'summaries' =>
			array('report_top10_assets_with_more_risks' => array(FWDLanguage::getPHPStringValue('si_top_10_assets_with_most_risks',"Top 10 Ativos com Mais Riscos"),"M.RM.6.6.1"),
						'report_top10_assets_with_more_high_risks' => array(FWDLanguage::getPHPStringValue('si_top_10_assets_with_highest_risks',"Top 10 Ativos com Mais Riscos Altos"),"M.RM.6.6.2"),
						'report_top10_risks_by_area' => array(FWDLanguage::getPHPStringValue('si_top_10_risks_per_area',"Top 10 Riscos por �rea"),"M.RM.6.6.3"),
						'report_top10_risks_by_process' => array(FWDLanguage::getPHPStringValue('si_top_10_risks_per_process',"Top 10 Riscos por Processo"),"M.RM.6.6.4"),
						'report_risk_amount_by_process' => array(FWDLanguage::getPHPStringValue('si_amount_of_risks_per_process',"Quantidade de Riscos por Processo"),"M.RM.6.6.5"),
						'report_risk_amount_by_area' => array(FWDLanguage::getPHPStringValue('si_amount_of_risks_per_area',"Quantidade de Riscos por �rea"),"M.RM.6.6.6"),
						'report_risk_status_by_area' => array(FWDLanguage::getPHPStringValue('si_risk_status_per_area',"Status de Risco por �rea"),"M.RM.6.6.7"),
						'report_risk_status_by_process' => array(FWDLanguage::getPHPStringValue('si_risk_status_per_process',"Status de Risco por Processo"),"M.RM.6.6.8")
			),
		'others' =>
			array('report_user_responsabilities' => array(FWDLanguage::getPHPStringValue('si_users_responsibilities',"Responsabilidades dos Usu�rios"),"M.RM.6.7.1"),
						'report_pendant_contexts' => array(FWDLanguage::getPHPStringValue('si_pending_items',"Elementos Pendentes"),"M.RM.6.7.2"),
						'report_asset' => array(FWDLanguage::getPHPStringValue('si_assets',"Ativos"),"M.RM.6.7.3"),
            'report_asset_deps' => array(FWDLanguage::getPHPStringValue('si_assets_deps',"Ativos - Depend�ncias e Dependentes"),"M.RM.6.7.5"),
            'report_asset_relevance' => array(FWDLanguage::getPHPStringValue('si_asset_relevance',"Relev�ncia dos Ativos"),"M.RM.6.7.6"),
						'report_element_classification' => array(FWDLanguage::getPHPStringValue('si_element_classification',"Classifica��o dos Elementos"),"M.RM.6.7.4")
			)
	);
}

/*
 * Fun��o que popula a grid com os relat�rios de 
 * acordo com a op��o do filtro de relat�rios
 */
function showReports() {  
  echo "hide_all_filters();hide_all_warnings();js_show('no_filter_available');";
  
  $moGrid = FWDWebLib::getObject('grid_reports');
  $moGrid->setObjFwdDrawGrid(new GridReports());   
  $msReportCategory = FWDWebLib::getObject("report_category")->getValue();  
  $maReportCategory = getReportCategoryArray();
  $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
       
  if ($msReportCategory != 'all_reports') {
    $maReports = $maReportCategory[$msReportCategory];
    $miLineCount = 0;
    foreach ($maReports as $msReportKey => $maReportACL) {
      if(!in_array($maReportACL[1] , $maACLs)){
        $miLineCount++;
        $moGrid->setItem(1, $miLineCount, $msReportKey);
        $moGrid->setItem(2, $miLineCount, $maReportACL[0]);
      }
    }
  }
  else {    
    $miLineCount = 0;
    foreach ($maReportCategory as $maReports) {      
      foreach ($maReports as $msReportKey => $maReportACL) {
        if(!in_array($maReportACL[1] , $maACLs)){
          $miLineCount++;
          $moGrid->setItem(1, $miLineCount, $msReportKey);
          $moGrid->setItem(2, $miLineCount, $maReportACL[0]);
        } 
      }
    }
  }
  $moGrid->execEventPopulate();
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent("")); 
    $moStartEvent->addAjaxEvent(new ISMSReportEvent("function_report"));
    $moStartEvent->addAjaxEvent(new ISMSPreReportEvent("function_pre_report"));
    $moStartEvent->addAjaxEvent(new ISMSConfirmClearPreReportEvent("function_clear_pre_report"));
    $moStartEvent->addAjaxEvent(new ISMSClearPreReportEvent("clear_pre_report_confirmed"));
    $moStartEvent->addAjaxEvent(new ShowReportsEvent("show_reports"));
  }
}

class ShowReportsEvent extends FWDRunnable {
  public function run(){       
    showReports();    
  }
}

function getSelClassifPrio($psKey){

  $moReturn =
      strstr($psKey,"type_area") ? FWDWebLib::getObject('sel_area_type') :
      (strstr($psKey,"type_process") ? FWDWebLib::getObject('sel_process_type') :
        (strstr($psKey,"type_risk") ? FWDWebLib::getObject('sel_risk_type') :
          (strstr($psKey,"type_event") ? FWDWebLib::getObject('sel_event_type') :
            (strstr($psKey,"type_control") ? FWDWebLib::getObject('sel_control_type') :
              (strstr($psKey,"prio_area") ? FWDWebLib::getObject('sel_area_priority') : 
                (strstr($psKey,"prio_process") ? FWDWebLib::getObject('sel_process_priority') : null))))));

  return $moReturn;
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();    
    $moGrid = $moWebLib->getObject('grid_reports');
    $moGrid->setObjFwdDrawGrid(new GridReports());   
    
    /*
     * Popula o select com as categorias para o filtro do relat�rio de riscos por ativo
     */
    $moSelect = FWDWebLib::getObject('select_category');
    $moSelect2 = FWDWebLib::getObject('select_category2');
    $moQuery = new QueryContextHierarchicalStructure(FWDWebLib::getConnection());
    $moQuery->setContextType(CONTEXT_CATEGORY);
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    foreach ($moQuery->getContexts() as $maContext) {
      list($miContextId,$msHierarchicalName) = $maContext;
      $moSelect->setItemValue($miContextId,$msHierarchicalName);
      $moSelect2->setItemValue($miContextId,$msHierarchicalName);
    }
    
    /*
     * Popula o select com os processos para o filtro do relat�rio de riscos por processo
     */    
    $moHandler = new QuerySelectProcess(FWDWebLib::getConnection());
    $moSelect = FWDWebLib::getObject('select_process');
    $moSelect->setQueryHandler($moHandler);
    $moSelect->populate();
    
    //popula o select de filtro por norma do relat�rio de declara��o de aplicabilidade
    $moSelSection = FWDWebLib::getObject("sel_filter_standard_bp");
    $moSelect = FWDWebLib::getObject('select_conformity');
    $moQuery = new QuerySelectStandard(FWDWebLib::getConnection());
    $moQuery->setAllStandards(true);
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    $maValues = $moQuery->getValues();
    foreach($maValues as $maValue){
      $moItem = new FWDItem();
      $moItem->setAttrKey($maValue[0]);
      $moItem->setValue($maValue[1]);
      $moSelSection->addObjFWDItem($moItem);
      $moSelect->addObjFWDItem($moItem);      
    }
    
    /*
     * Popula o select com os par�metros do risco
     */    
    $moHandler = new QuerySelectParameters(FWDWebLib::getConnection());
    $moSelect = FWDWebLib::getObject('select_parameters');
    $moSelect->setQueryHandler($moHandler);
    $moSelect->populate();
    
    /*
     * Popula o select com as �rea para o filtro do relat�rio de riscos por �rea
     */   
    $moSelect = FWDWebLib::getObject('select_area');
    $moQuery = new QueryContextHierarchicalStructure(FWDWebLib::getConnection());
    $moQuery->setContextType(CONTEXT_AREA);
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    foreach ($moQuery->getContexts() as $maContext) {
      list($miContextId,$msHierarchicalName) = $maContext;
      $moSelect->setItemValue($miContextId,$msHierarchicalName);      
    }
    
    /*
    * Popula o select com os ativos do sistema para o relar�rio de depend�ncias e dependentes dos ativos
    */
    $moHandler = new QuerySelectAsset(FWDWebLib::getConnection());
    $moSelect = FWDWebLib::getObject('select_asset');
    $moSelect->setQueryHandler($moHandler);
    $moSelect->populate();
    
    /*
     * Popula o select com os tipos de formato de relat�rios e
     * o select com os tipos de classifica��o de relat�rios.
     */
    $moFormatSelect = FWDWebLib::getObject('select_report_type');
    $maAvailableFormats = FWDReportGenerator::getAvailableFormats(ISMSLib::getSupportedReportFormats());
    foreach ($maAvailableFormats as $miKey => $msValue) {
      $moFormatSelect->setItemValue($miKey, $msValue);      
    }    
    $moHandler = new QueryGetReportClassification(FWDWebLib::getConnection());
    $moHandler->makeQuery();
    $maAvailableClassifications = $moHandler->executeQuery();
    $moClassificationSelect = FWDWebLib::getObject('select_report_classification');
    foreach ($maAvailableClassifications as $miKey => $msValue) {
      $moClassificationSelect->setItemValue($msValue, $msValue);
    }

    //popula os filtros de classifica��o
    $moClassifHandle = new QueryContextClassification(FWDWebLib::getConnection());
    $moClassifHandle->makeQuery();
    $moClassifHandle->executeQuery();
    $maClassifIds = $moClassifHandle->getClassificationIds();
    $maClassifNames = $moClassifHandle->getClassificationNames();
    foreach($maClassifIds as $msKey=>$miValue){
      $moSel = getSelClassifPrio($msKey);
      $moItem = new FWDItem();
      $moItem->setAttrKey($miValue);
      $moItem->setValue($maClassifNames[$msKey]);
      if($moSel){
        $moSel->addObjFWDItem($moItem);
      }
    }

            
    //instala a seguran�a de ACL na p�gina
    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
    
    //desabilita o item do select referente aos relat�rios de custo caso o sistema
    //n�o esteja gerenciando os custos
    $moConfig = new ISMSConfig();
    if(!$moConfig->getConfig(GENERAL_COST_ENABLED)){
       FWDWebLib::getObject('financial_reports_item')->setShouldDraw(false);
    }
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
     ?>
      <script language="javascript">
        gobi('grid_reports').setPopulate(true);
        trigger_event('show_reports',3);
        var ssTypePrioSelId = '';
        function show_sel_type_prio_by_checkbox(psSelId){
          if(gobi('ctrl_type_prio_chk_'+psSelId).isChecked()){
            gobi('sel_'+psSelId).hide();
            if(ssTypePrioSelId){
              gobi('sel_'+ssTypePrioSelId).hide();
              gobi('st_'+ssTypePrioSelId).hide();
            }
            ssTypePrioSelId = '';
          }else{
            gobi('sel_'+psSelId).show();
            gobi('st_'+psSelId).show();
            if(ssTypePrioSelId){
              gobi('sel_'+ssTypePrioSelId).hide();
              gobi('st_'+ssTypePrioSelId).hide();
            }
            ssTypePrioSelId = psSelId;
          }
        }
        
        function check_if_not_checked(psId){
          if(!gobi('ctrl_type_prio_chk_'+psId).isChecked()){
            gfx_check('ctrl_type_prio','chk_'+psId);
          }
        }
        
        function show_sel_type_prio(psId){
          if(ssTypePrioSelId){
            if(ssTypePrioSelId != psId){
              gobi('sel_'+ssTypePrioSelId).hide();
              gobi('st_'+ssTypePrioSelId).hide();
              gobi('sel_'+psId).show();
              gobi('st_'+psId).show();
              check_if_not_checked(psId);
            }
          }else{
            gobi('sel_'+psId).show();
            gobi('st_'+psId).show();
            check_if_not_checked(psId);
          }
          ssTypePrioSelId = psId;
        }
        
        function hide_all_filters() {
          js_hide('report_risk_filter');
          js_hide('report_pendant_tasks_filter');
          js_hide('report_asset_filter');
          js_hide('report_risks_by_asset_filter');
          js_hide('report_risks_by_process_filter');
          js_hide('report_control_effiency_filter');
          js_hide('report_control_planning_filter');
          js_hide('report_statement_of_applicability_filter');
          js_hide('report_parameters_filter');
          js_hide('report_risks_by_area_filter');
          js_hide('report_events_by_category_filter');
          js_hide('report_user_responsabilities_filter');
          js_hide('report_element_classification_filter');
          js_hide('report_conformity_filter');
          js_hide('report_asset_deps_filter');
          js_hide('report_duplicated_risks_filter');
          js_hide('no_filter_available');
        }
        
        function hide_all_warnings() {
          js_hide('warning');
          js_hide('report_risk_filter_warning');
          js_hide('report_parameters_filter_warning');
          js_hide('report_responsabilities_filter_warning');
        }

        function show_filter(psId) {
          gobi('st_risk_no_values').show();
          gobi('risk_filter_checkbox_no_values').show();
          gebi('var_pog_disable_risk_no_value').value=false;
          
          if (gobi('grid_reports').getValue()) js_show(psId);
          else js_show('no_filter_available');
        }
        
        function show_filter_risk_control(psId) {
          gobi('st_risk_no_values').hide();
          gobi('risk_filter_checkbox_no_values').hide();
          
          gebi('var_pog_disable_risk_no_value').value=true;
          if (gobi('grid_reports').getValue()) 
            js_show(psId);
          else 
            js_show('no_filter_available');
        }
        
        gobi('risk_filter_checkbox').onRequiredCheckNotOk = function(){
          gobi('report_risk_filter_warning').show();
        }
        
        gobi('select_parameters').onRequiredCheckNotOk = function(){
          gobi('report_parameters_filter_warning').show();
        }
        
        gobi('report_user_responsabilities_filter_controller').onRequiredCheckNotOk = function(){
          gobi('report_responsabilities_filter_warning').show();
        }
        
        gobi('grid_reports').onRequiredCheckNotOk = function(){
          gobi('warning').show();
        }
        
      </script>
    <?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_report.xml");
?>