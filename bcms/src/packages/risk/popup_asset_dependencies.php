<?php
include_once "include.php";
include_once $handlers_ref . "QueryAssetPossibleDependencies.php";
include_once $handlers_ref . "QueryAssetPossibleDependents.php";
include_once $handlers_ref . "QueryAssetDependencies.php";
include_once $handlers_ref . "QueryAssetDependents.php";

class SaveDependenciesEvent extends FWDRunnable {
	public function run() {

		$moWebLib = FWDWebLib::getInstance();
		$miAssetId = $moWebLib->getObject("asset_id")->getValue();
		$miDependency = $moWebLib->getObject("param_dependency")->getValue();
		$mbDependency = $miDependency==1?true:false;
		$moSelect = $moWebLib->getObject('asset_dep');
		$maSelectItems = $moSelect->getItemsKeys();

		$moDependency = new RMAssetDependency();

		$msAliasId = $mbDependency?"dependent_id":"asset_id";
		$moDependency->setAliasId($msAliasId);
		$moDependency->delete($miAssetId);
		$msNotAliasId = $mbDependency?"asset_id":"dependent_id";
		foreach($maSelectItems as $miSelectAssetId) {
			$moDependency = new RMAssetDependency();
			$moDependency->setFieldValue($msAliasId, $miAssetId);
			$moDependency->setFieldValue($msNotAliasId, $miSelectAssetId);
			$moDependency->insert();
		}
		echo "soPopUpManager.closePopUp('popup_asset_dependencies');";
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moWebLib = FWDWebLib::getInstance();
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));
		$moStartEvent->addAjaxEvent(new SaveDependenciesEvent("save_asset_dependencies"));

		$miAssetId = $moWebLib->getObject("param_asset_id")->getValue();
		$moWebLib->getObject("asset_id")->setValue($miAssetId);

	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$moWebLib = FWDWebLib::getInstance();
		$moWindowTitle = $moWebLib->getObject('window_title');
		$moListName = $moWebLib->getObject('list_name');

		$miAssetId = $moWebLib->getObject("asset_id")->getValue();

		$miDependency = $moWebLib->getObject("param_dependency")->getValue();
		$mbDependency = $miDependency==1?true:false;
		$moSelectAll = $moWebLib->getObject('asset_all');
		$moSelectDep = $moWebLib->getObject('asset_dep');

		if ($mbDependency) {
			$moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_dependencies_adjustment',"Ajuste de Dependências"));
			$moListName->setValue(FWDLanguage::getPHPStringValue('st_dependencies_list',"Lista de Dependências"));
		}
		else {
			$moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_dependents_adjustment',"Ajuste de Dependentes"));
			$moListName->setValue(FWDLanguage::getPHPStringValue('st_dependents_list',"Lista de Dependentes"));
		}

		/*
		 * Popula o select com as possíveis dependências ou possíveis dependentes do ativo
		 */
		$moQuery = $mbDependency ? new QueryAssetPossibleDependencies($moWebLib->getConnection()) : new QueryAssetPossibleDependents($moWebLib->getConnection());
		$moQuery->setAsset($miAssetId);
		$moQuery->makeQuery();
		$moQuery->executeQuery();
		$maSelectAllItems = $mbDependency ? $moQuery->getAssetDependencies() : $moQuery->getAssetDependents();
		if (count($maSelectAllItems) > 0) {
			foreach($maSelectAllItems as $maItem) {
				$moItem = new FWDItem();
				$moItem->setAttrKey($maItem['asset_id']);
				$moItem->setValue($maItem['asset_name']);
				$moItem->setAttrClass('asset_'.RMRiskConfig::getRiskColor($maItem['asset_value']));
				$moSelectAll->addObjFWDItem($moItem);
			}
		}

		/*
		 * Popula o select com as dependências ou dependentes diretos do ativo
		 */
		$moQuery = $mbDependency ? new QueryAssetDependencies($moWebLib->getConnection()) : new QueryAssetDependents($moWebLib->getConnection());
		$moQuery->setAsset($miAssetId);
		$moQuery->makeQuery();
		$moQuery->executeQuery();
		$maSelectDepItems = $mbDependency ? $moQuery->getAssetDependencies() : $moQuery->getAssetDependents();
		if (count($maSelectDepItems) > 0) {
			foreach($maSelectDepItems as $maItem) {
				$moItem = new FWDItem();
				$moItem->setAttrKey($maItem['asset_id']);
				$moItem->setValue($maItem['asset_name']);
				$moItem->setAttrClass('asset_'.RMRiskConfig::getRiskColor($maItem['asset_value']));
				$moSelectDep->addObjFWDItem($moItem);
			}
		}

		$moWebLib->dump_html($moWebLib->getObject('dialog'));
		?>
<script>
	    function move_option(from,to) {
				for (i=0;(i<from.length);i++){
					if (from.options[i].selected){
						nova = new Option(from.options[i].text,from.options[i].value);
						nova.className = from.options[i].className;
						to.options[to.length]=nova;			
					}
				}
				i=0;
				while (i<from.length) {
					if (from.options[i].selected) {
						from.options[i]=null;
					}
					else
						i++;
			}	
			BubbleSort(to);
		}
	
		function BubbleSort(listbox) { 
		  var x, y, auxiliar_text,auxiliar_value; 
		  for(x = 0; x < listbox.length; x++) { 
		    for(y = 0; y < (listbox.length-1); y++) { 
		    palavra1 = listbox[y].text.toLowerCase(); 
		    palavra2 = listbox[y+1].text.toLowerCase(); 
		      if(palavra1 > palavra2) { 
		        auxiliar_text = listbox[y+1].text; 
		        auxiliar_value = listbox[y+1].value;
		        auxiliar_color = listbox[y+1].className;  
		        listbox[y+1].text = listbox[y].text; 
		        listbox[y+1].value = listbox[y].value; 
		        listbox[y+1].className = listbox[y].className;
		        listbox[y].text = auxiliar_text; 
		        listbox[y].value = auxiliar_value; 
		        listbox[y].className = auxiliar_color;
		      } 
		    } 
		  }
		} 
	
		function SelectAll(listbox) {
			for (i=0;(i<listbox.length);i++){
				listbox.options[i].selected = true;
			}	
		}
	</script>
		<?
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_asset_dependencies.xml");
?>