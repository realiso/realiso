<?php

include_once 'include.php';

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $msTestSensitive = FWDWebLib::getObject('par_test_sensitive')->getValue();
    $msTest = FWDWebLib::getObject('par_test')->getValue();
    
    if($msTestSensitive){
      $maTest = array_merge(
        FWDWebLib::unserializeString($msTest, false),
        FWDWebLib::unserializeString($msTestSensitive, false)
      );
      
      FWDWebLib::getObject('description')->setValue($maTest['description']);
      FWDWebLib::getObject('var_schedule')->setValue($maTest['schedule']);
    }
    
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        
        function set_schedule(psSerializedSchedule){
          gebi('var_schedule').value = psSerializedSchedule;
        }
        
        function confirm(){
          var maSensitiveFields = {};
          var maFields = {};
          
          maSensitiveFields['schedule'] = gebi('var_schedule').value;
          maFields['description'] = gebi('description').value;
          
          self.getOpener().set_test(soUnSerializer.serialize(maSensitiveFields),soUnSerializer.serialize(maFields));
          self.close();
        }
        
      </script>
    <?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_control_test.xml');

?>