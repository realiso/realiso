<?php
include_once "include.php";

set_time_limit(300);

class SaveEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $maRisksIds = explode(':',$moWebLib->getObject('risks_ids')->getValue());
    
    if(!ISMSLib::userHasACL('M.RM.4.8')){
      echo FWDLanguage::getPHPStringValue('st_denied_permission_to_estimate_risks','Voc� n�o tem permiss�o para estimar v�rios riscos');
      exit();
    }

    foreach($maRisksIds as $miRiskId){
      $moRiskParameter = $moWebLib->getObject("risk_parameters_$miRiskId");
      if($moRiskParameter->getHash() != $moWebLib->getPOST("risk_hash_$miRiskId")){               
        $maRiskParameters = $moRiskParameter->getValues();
                
        $moRisk = new RMRisk();
        $moRisk->setFieldValue('risk_probability', $maRiskParameters['prob']);
        
        $moRisk->update($miRiskId);
        $moRiskParameterValues = new RiskParametersValue($moRiskParameter,RISKPARAMETER_RISK);
        $moRiskParameterValues->setValues($miRiskId);
      }
    }
    
    echo "var soWindow = soPopUpManager.getPopUpById('popup_risk_parametrization').getOpener();"
        ."if(soWindow.refresh_grid) soWindow.refresh_grid();"
        ."soPopUpManager.closePopUp('popup_risk_parametrization');"
        ."soWindow = null;";
  }
}

function getParametersNames(){
/*
  return array(
    '1' => '1� Parametro',
    '2' => '2� Parametro',
    '3' => '3� Parametro',
    '4' => '4� Parametro',
    '5' => '5� Parametro',
    '6' => '6� Parametro',
    '7' => '7� Parametro',
    '8' => '8� Parametro',
  );
*/
  $maParameters = FWDWebLib::getObject('ParametersNames');
  if($maParameters===null){
    $maParameters = array();
    $moHandler = new QueryParameterNames(FWDWebLib::getConnection());
    $moHandler->makeQuery();
    $moHandler->executeQuery();
    $moDataset = $moHandler->getDataset();
    while($moDataset->fetch()){
      $miId = $moDataset->getFieldByAlias('parametername_id')->getValue();
      $msName = $moDataset->getFieldByAlias('parametername_name')->getValue();
      $maParameters[$miId] = $msName;
    }
    FWDWebLib::addObject('ParametersNames',$maParameters);
  }
  return $maParameters;
}

function getValuesNames(){
/*
  return array(
    '1' => 'valor grande 1',
    '2' => 'valor grande 2',
    '3' => 'valor grande 3',
  );
*/
  $maValuesNames = FWDWebLib::getObject('ValuesNames');
  if($maValuesNames===null){
    $maValuesNames = array();
    $moHandler = new QueryParameterValueNames(FWDWebLib::getConnection());
    $moHandler->setType(RISKPARAMETER_RISK);
    $moHandler->makeQuery();
    $moHandler->executeQuery();
    $moDataset = $moHandler->getDataset();
    while($moDataset->fetch()){
      $miId = $moDataset->getFieldByAlias('value_id')->getValue();
      $msName = $moDataset->getFieldByAlias('value_name')->getValue();
      $maValuesNames[$miId] = $msName;
    }
    FWDWebLib::addObject('ValuesNames',$maValuesNames);
  }
  return $maValuesNames;
}

function getProbabilityValues(){
/*
  return array(
    '1' => 'valor grande 1',
    '2' => 'valor grande 2',
    '3' => 'valor grande 3',
  );
*/
  $maProbabilityValues = FWDWebLib::getObject('ProbabilityValues');
  if($maProbabilityValues===null){
    $maProbabilityValues = array();
    $moHandler = new QueryProbabilityValueNames(FWDWebLib::getConnection());
    $moHandler->setType(RISKPARAMETER_RISK);
    $moHandler->makeQuery();
    $moHandler->executeQuery();
    $moDataset = $moHandler->getDataset();
    while($moDataset->fetch()){
      $miId = $moDataset->getFieldByAlias('value_id')->getValue();
      $msName = $moDataset->getFieldByAlias('value_name')->getValue();
      $maProbabilityValues[$miId] = $msName;
    }
    FWDWebLib::addObject('ProbabilityValues',$maProbabilityValues);
  }
  return $maProbabilityValues;
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
    $moStartEvent->addAjaxEvent(new SaveEvent('save_event'));
    
    // Busca no banco os nomes dos par�metros
    $maParameters = getParametersNames();
    // Busca no banco os nomes dos valores
    $maValues = getValuesNames();
    // Busca no banco os nomes dos valores de probabilidade
    $maProbabilityValues = getProbabilityValues();
    
    $moWebLib = FWDWebLib::getInstance();
    $moRisksIds = $moWebLib->getObject('risks_ids');
    $msRisks = $moRisksIds->getValue();
    if($msRisks){
      $maRisksIds = explode(':',$msRisks);
    }else{
      $maRisksIds = array_unique(explode(':',$moWebLib->getObject('risks')->getValue()));
      $moRisksIds->setValue(implode(':',$maRisksIds));
    }
    
    foreach($maRisksIds as $miRiskId){
      $moRiskParameter = new RiskParameters();
      $moRiskParameter->setAttrName("risk_parameters_$miRiskId");
      $moWebLib->addObject("risk_parameters_$miRiskId",$moRiskParameter);
      $moRiskParameter->setAttrType('risk');
      $moRiskParameter->setObjFWDBox(new FWDBox());
      foreach($maParameters as $miId=>$msName){
        $moRiskParameter->addParameter($miId,$msName);
      }
      foreach($maValues as $miId=>$msName){
        $moRiskParameter->addValue($miId,$msName);
      }
      foreach($maProbabilityValues as $miId=>$msName){
        $moRiskParameter->addProbabilityValue($miId,$msName);
      }
    }
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $maRisksIds = explode(':',$moWebLib->getObject('risks_ids')->getValue());
    
    if(!ISMSLib::userHasACL('M.RM.4.8')){
      trigger_error(FWDLanguage::getPHPStringValue('st_denied_permission_to_estimate_risks','Voc� n�o tem permiss�o para estimar v�rios riscos'), E_USER_ERROR);
    }

    // Busca no banco os nomes dos par�metros
    $maParameters = getParametersNames();
    // Busca no banco os nomes dos valores
    $maValues = getValuesNames();
    // Busca no banco os nomes dos valores de probabilidade
    $maProbabilityValues = getProbabilityValues();
    
    $moOuterViewGroup = $moWebLib->getObject('risk_parameters');
    $moInnerViewGroup = $moWebLib->getObject('lines');
    
    $moInnerViewGroup->setAttrClass('simple_border');
    
    $miMaxSelectWidth = 150;
    $miMaxStaticWidth = 250;
    $miHorizontalMargin = 10;
    $miStaticWidth = 160;
    $miLineHeight = 36;
    $miStaticHeight = 32;
    $miRiskParameterHeight = 25;
    $miHeaderHeight = 25;
    $miHorizontalOffset = floor($miHorizontalMargin/2);
    
    $miStaticVerticalOffset = floor(($miLineHeight - $miStaticHeight)/2);
    $miRiskParameterVerticalOffset = floor(($miLineHeight - $miRiskParameterHeight)/2);
    
    $miViewGroupWidth = $moInnerViewGroup->getObjFWDBox()->getAttrWidth();
    $miColumns = count($maParameters) + 1;
    $miColumnsTotalWidth = $miViewGroupWidth - $miStaticWidth - 20;
    if($miColumnsTotalWidth/$miColumns > $miMaxSelectWidth){
      $miStaticWidth = $miViewGroupWidth - $miColumns * $miMaxSelectWidth;
      if($miStaticWidth > $miMaxStaticWidth){
        $miStaticWidth = $miMaxStaticWidth;
        $miColumnsTotalWidth = $miColumns * $miMaxSelectWidth;
      }else{
        $miColumnsTotalWidth = $miViewGroupWidth - $miStaticWidth - 20;
      }
    }
    $miSelectWidth = $miColumnsTotalWidth/$miColumns - $miHorizontalMargin;

    $miLeft = $miStaticWidth;
    $maParameters['prob'] = FWDLanguage::getPHPStringValue('mx_probability','Probabilidade');
    foreach($maParameters as $msName){
      $moStatic = new FWDStatic();
      $moStatic->setObjFWDBox(new FWDBox($miLeft+$miHorizontalOffset,0,$miHeaderHeight,$miSelectWidth));
      $moStatic->setValue($msName);
      $moOuterViewGroup->addObjFWDView($moStatic);
      $miLeft+= $miSelectWidth + $miHorizontalMargin;
      
      $moToolTip = new FWDToolTip();
      $moToolTip->setAttrShowDelay(1000);
      $moToolTip->setValue($msName);
      $moStatic->addObjFWDEvent($moToolTip);
    }
    unset($maParameters['prob']);

    $moRiskParameter = new RiskParameters();
    $moRiskParameter->setAttrName("risk_parameters_all");
    $moRiskParameter->setAttrType('risk');
    $moRiskParameter->setAttrClass('');
    $moRiskParameter->setAttrHorizontal('true');
    $moRiskParameter->setAttrValueNameWidth($miSelectWidth);
    $moRiskParameter->setAttrHorizontalMargin($miHorizontalMargin);
    $moRiskParameter->setObjFWDBox(new FWDBox($miStaticWidth,25,0,0));
    $moOuterViewGroup->addObjFWDView($moRiskParameter);
    foreach($maParameters as $miId=>$msName){
      $moRiskParameter->addParameter($miId,$msName);
    }
    foreach($maValues as $miId=>$msName){
      $moRiskParameter->addValue($miId,$msName);
    }
    foreach($maProbabilityValues as $miId=>$msName){
      $moRiskParameter->addProbabilityValue($miId,$msName);
    }
    $moRiskParameter->build();

    $miTop = 0;
    $mbEven = false;
    foreach($maRisksIds as $miRiskId){
      
      $moRisk = new RMRisk();
      $moRisk->fetchById($miRiskId);
      $msRiskName = $moRisk->getFieldValue('risk_name');
      $msRiskImpact = $moRisk->getFieldValue('risk_impact') ? $moRisk->getFieldValue('risk_impact') : '---';
      
      $moLine = new FWDViewGroup();
      $moLine->setObjFWDBox(new FWDBox(0,$miTop,$miLineHeight,$miViewGroupWidth-20));
      $moLine->setAttrClass($mbEven?'BdZ0':'BdZ1');
      $moInnerViewGroup->addObjFWDView($moLine);
      
      $moStatic = new FWDMemoStatic();
      $moStatic->setAttrScrollbar('true');
      $moStatic->setAttrName("label_risk_parameters_$miRiskId");
      $moStatic->setAttrClass('MemoStatic_RiskName');
      $moStatic->setObjFWDBox(new FWDBox(0,$miStaticVerticalOffset,$miStaticHeight,$miStaticWidth));
      $moStatic->setValue($msRiskName);
      $moLine->addObjFWDView($moStatic);
      
      $moToolTip = new FWDToolTip();
      $moToolTip->setAttrShowDelay(100);
      $moToolTip->setValue('<b>'.FWDLanguage::getPHPStringValue('to_risk_cl','Risco:').'</b> '.$msRiskName.'<br/><br/><b>'.FWDLanguage::getPHPStringValue('to_impact_cl','Impacto:').'</b> '.$msRiskImpact);
      $moStatic->addObjFWDEvent($moToolTip);
      
      $moRiskParameter = $moWebLib->getObject("risk_parameters_$miRiskId");
      $moRiskParameter->setAttrClass('');
      $moRiskParameter->setAttrHorizontal('true');
      $moRiskParameter->setAttrValueNameWidth($miSelectWidth);
      $moRiskParameter->setAttrHorizontalMargin($miHorizontalMargin);
      $moRiskParameter->setObjFWDBox(new FWDBox($miStaticWidth,$miRiskParameterVerticalOffset,0,0));
      //*
      $moRiskParameterValues = new RiskParametersValue($moRiskParameter,RISKPARAMETER_RISK);
      $moRiskParameterValues->getValues($miRiskId);
      /*/
      $moRiskParameter->build();
      //*/
      $moLine->addObjFWDView($moRiskParameter);
      
      $moRiskHash = new FWDVariable();
      $moRiskHash->setAttrName("risk_hash_$miRiskId");
      $moRiskHash->setValue($moRiskParameter->getHash());
      $moLine->addObjFWDView($moRiskHash);
      
      $miTop+= $miLineHeight;
      $mbEven = !$mbEven;
    }
    
    FWDWebLib::getInstance()->dump_html($moWebLib->getObject('dialog'));
    ?>
      <script language="javascript">
        gebi('risk_parameters').object = {
          'getRiskParameters': function(){
            if(!this.caRiskParameters){
              var maIds = gebi('risks_ids').value.split(':');
              this.caRiskParameters = [];
              for(var i=0;i<maIds.length;i++){
                this.caRiskParameters.push(gebi('risk_parameters_'+maIds[i]));
                this.caRiskParameters[i].object.onRequiredCheckOk = null;
                this.caRiskParameters[i].object.onRequiredCheckNotOk = null;
              }
              maIds = null;
            }
            return this.caRiskParameters;
          },
          'isFilled': function(){
            var maOk = [];
            var maNotOk = [];
            var maRiskParameters = this.getRiskParameters();
            for(var i=0;i<maRiskParameters.length;i++){
              if(maRiskParameters[i].object.isFilled()){
                maOk.push(maRiskParameters[i].id);
              }else{
                maNotOk.push(maRiskParameters[i].id);
              }
            }
            js_required_check_feedback(maOk,maNotOk);
            var mbReturn = (maNotOk.length==0);
            maOk = maNotOk = maRiskParameters = null;
            return mbReturn;
          },
          'onRequiredCheckOk': function(){},
          'onRequiredCheckNotOk': function(){},
          'setParameterValue': function(psParamId,psValueId){
            var maRiskParameters = this.getRiskParameters();
            for(var i=0;i<maRiskParameters.length;i++){
              maRiskParameters[i].object.setParameterValue(psParamId,psValueId);
            }
            maRiskParameters = null;
          }
        };
        
        gobi('risk_parameters_all').setParameterValue = function(psParamId,psValueId){
          var moSelect = gebi(this.csName+'_'+psParamId);
          moSelect.value = psValueId;
          moSelect.className = moSelect.options[moSelect.selectedIndex].className;
          gobi('risk_parameters').setParameterValue(psParamId,psValueId);
          moSelect = psParamId = psValueId = null;
        }
        
      </script>
    <?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_risk_parametrization.xml');
?>