<?php
include_once "include.php";

class SaveRiskTreatmentEvent extends FWDRunnable {
  public function run(){
    $miRiskId = FWDWebLib::getObject("param_risk_id")->getValue();
    
    //teste para verificar se o sistema n�o est� sendo hakeado
    $moCtxUserTest = new RMAcceptRisk();
    $moCtxUserTest->testPermissionToEdit($miRiskId);
    
    $miAcceptState = FWDWebLib::getObject("type_of_treatment")->getValue();
    $msAcceptJustification = FWDWebLib::getObject("justification")->getValue();
    if(in_array($miAcceptState, array(RISK_ACCEPT_MODE_ACCEPT,RISK_ACCEPT_MODE_TRANSFER,RISK_ACCEPT_MODE_AVOID))){
      $moAcceptRisk = new RMAcceptRisk();
      $moAcceptRisk->setFieldValue("risk_accept_mode", $miAcceptState);
      $moAcceptRisk->setFieldValue("risk_accept_justification", $msAcceptJustification);
      $moAcceptRisk->update($miRiskId,true,true);
    }else{
      trigger_error("Invalid risk accept state ($miAcceptState) !",E_USER_ERROR);
    }
    echo "soPopUpManager.getPopUpById('popup_risk_treatment').getOpener().refresh_grid();" .
         "soPopUpManager.closePopUp('popup_risk_treatment');";
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new SaveRiskTreatmentEvent("save_risk_treatment_event"));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    
    $miRiskId = $moWebLib->getObject("param_risk_id")->getValue();
    
    //teste para verificar se o sistema n�o est� sendo hakeado
    $moCtxUserTest = new RMAcceptRisk();
    $moCtxUserTest->testPermissionToEdit($miRiskId);
  
    $moRisk = new RMRisk();
    $moRisk->fetchById($miRiskId);
    $miTreatment = FWDWebLib::getObject('type_of_treatment')->getValue();
    $msTreatmentMsg = '';

    switch($miTreatment){
      case RISK_ACCEPT_MODE_ACCEPT:
        $msTreatmentMsg = FWDLanguage::getPHPStringValue('tt_hold_accept','reter/aceitar');
      break;
      case RISK_ACCEPT_MODE_TRANSFER:
        $msTreatmentMsg = FWDLanguage::getPHPStringValue('tt_transfer','transferir');
      break;
      case RISK_ACCEPT_MODE_AVOID:
        $msTreatmentMsg = FWDLanguage::getPHPStringValue('tt_avoid','evitar');
      break;
      default:
        trigger_error("Invalid risk accept state ($miTreatment) !",E_USER_ERROR);
      break;
    }
    $msWindowTitle = str_replace("%treatment_type%",$msTreatmentMsg,FWDWebLib::getObject('window_title')->getValue());
    $msMessage = str_replace("%treatment_type%",$msTreatmentMsg,FWDWebLib::getObject('risk_treatment_message')->getValue());
    $msMessage = str_replace("%risk_name%", $moRisk->getFieldValue("risk_name"), $msMessage);
    
    FWDWebLib::getObject('window_title')->setValue($msWindowTitle);
    FWDWebLib::getObject("risk_treatment_message")->setValue($msMessage);
    
    FWDWebLib::getInstance()->dump_html($moWebLib->getObject('dialog'));
    ?>
      <script language="javascript">
        gebi('justification').focus();
      </script>
    <?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_risk_treatment.xml');
?>