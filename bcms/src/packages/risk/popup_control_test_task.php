<?php
include_once "include.php";
include_once $handlers_ref . "grid/QueryGridUserTasks.php";

class SaveEvent extends FWDRunnable {
	public function run() {
		$moWebLib = FWDWebLib::getInstance();

    $moTaskTest = new WKFTask();
    $moTaskTest->fetchById($moWebLib->getObject('var_task_id')->getValue());
    //teste para verificar se o sistema n�o est� sendo hakeado
    $moTaskTest->testPermissionToExecute($moWebLib->getObject('var_task_id')->getValue());

    $miControlId = $moWebLib->getObject('var_control_id')->getValue();

		$moTest = new WKFControlTest();
		$moTest->fetchById($miControlId);
		$moTask = new WKFTask();
		$moTask->fetchById($moWebLib->getObject('var_task_id')->getValue());
		//atualiza o hist�rico dos testes
		$msTestValue = '';
		$msAuxTestValue = $moWebLib->getObject('controller_test')->getValue();
		if(substr($msAuxTestValue,0,1)==':')
			$msTestValue = substr($msAuxTestValue,1);
		else
			$msTestValue = $msAuxTestValue;

    $moHist = new RMControlTestHistory();
    $moHist->createFilter(FWDWebLib::getObject('var_control_id')->getValue(),'control_id');
    $moHist->createFilter($moTask->getFieldValue('task_date_created'),'control_date_todo');
    $moHist->delete();

		$moHist = new RMControlTestHistory();
		$moHist->setFieldValue('control_date_todo',$moTask->getFieldValue('task_date_created'));
		$moHist->setFieldValue('control_id',$moWebLib->getObject('var_control_id')->getValue());
		$moHist->setFieldValue('control_date_realized',ISMSLib::ISMSTime());
		$moHist->setFieldValue('control_test_value', $msTestValue);
		$moHist->setFieldValue('control_test_description',$moWebLib->getObject('test_observation')->getValue());
		$moHist->insert();

		//atualiza o estado da task
		$moTask2 = new WKFTask();
		$moTask2->setFieldValue('task_date_accomplished',ISMSLib::ISMSTime());
		$moTask2->setFieldValue('task_is_visible',false);
		$moTask2->update($moWebLib->getObject('var_task_id')->getValue());

    // atualiza o controle
    $moControl = new RMControl();
    $moControl->setFieldValue('control_id',$miControlId);
    $moControl->updateActive();

    $msNextTaskJS = "";
		$mbOpenNextTask = (bool)array_filter(explode(':',FWDWebLib::getObject('next_task_controller')->getValue()));
		if ($mbOpenNextTask) {
		  $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
		  $moHandlerTask = new QueryGridUserTasks(FWDWebLib::getConnection());
      $moHandlerTask->setUser($miUserId);
      $moHandlerTask->setOrderBy('task_date_created','-');
      $moHandlerTask->makeQuery();
      $moHandlerTask->executeQuery();
      if ($moHandlerTask->fetch()) {
  		  $msNextTaskJS = 'soWindow.setTimeout("'.ISMSLib::getTaskPopup($moHandlerTask->getFieldValue('task_id'),
  		                                        $moHandlerTask->getFieldValue('task_activity'),
  		                                        $moHandlerTask->getFieldValue('task_context_id'),
  		                                        $moHandlerTask->getFieldValue('task_context_type'),
  		                                        2).',1");';
      }
		}

		echo "soWindow = soPopUpManager.getPopUpById('popup_control_test_task').getOpener();"
        ."if (soWindow.refresh_grids)"
        ."  soWindow.refresh_grids();"
        .$msNextTaskJS
        ."soPopUpManager.closePopUp('popup_control_test_task');";
	}
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new SaveEvent("save_event"));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
		$miAutoTaskOpen = FWDWebLib::getObject('auto_task_open')->getValue();
    if (!$miAutoTaskOpen) FWDWebLib::getObject('next_task')->setShouldDraw(false);
    if ($miAutoTaskOpen==2) FWDWebLib::getObject('next_task_controller')->checkItem(1);

    $moWebLib = FWDWebLib::getInstance();
		$moWebLib->getObject('var_control_id')->setValue($moWebLib->getObject('control_id')->getValue());
		$moWebLib->getObject('var_task_id')->setValue($moWebLib->getObject('task_id')->getValue());

    $moTask = new WKFTask();
    $moTask->fetchById($moWebLib->getObject('var_task_id')->getValue());
    //teste para verificar se o sistema n�o est� sendo hakeado
    $moTask->testPermissionToExecute($moWebLib->getObject('var_task_id')->getValue());

		$moControl = new RMControl();
		$moControl->fetchById($moWebLib->getObject('control_id')->getValue());
		$moTest = new WKFControlTest();
		$moTest->fetchById($moWebLib->getObject('control_id')->getValue());
    FWDWebLib::getObject('control_test_desc')->setValue($moTest->getFieldValue('description'));

		$moWebLib->getObject('control_name')->setValue($moControl->getFieldValue('control_name'));
		$moWebLib->getObject('control_desc')->setValue($moControl->getFieldValue('control_description'));
    $moWebLib->getObject('control_test_desc')->setValue($moTest->getFieldValue('description'));
		//cria o tooltip para o campo de nome
		$msToolTipText = trim($moControl->getFieldValue('control_name'));
		if($msToolTipText!=''){
			$moToolTip = new FWDToolTip();
			$moToolTip->setAttrShowDelay(1000);
			$moToolTip->setValue($msToolTipText);
			$moWebLib->getObject('control_name')->addObjFWDEvent($moToolTip);
		}

		$moWebLib->dump_html($moWebLib->getObject('dialog'));
    ?>
      <script language="javascript">
        gebi('test_observation').focus();
      </script>
    <?
  }
}
	FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
	FWDWebLib::getInstance()->xml_load("popup_control_test_task.xml");
?>