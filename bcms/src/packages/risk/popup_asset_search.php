<?php
include_once "include.php";
include_once $handlers_ref . "grid/QueryGridAssetSearch.php";
include_once $handlers_ref . "QueryContextHierarchicalStructure.php";
include_once $handlers_ref . "QueryAssetDependents.php";

class GridAssetSearch extends FWDDrawGrid {
  public function drawItem(){
    switch($this->ciColumnIndex){
      case 1:
        $miAssetValue = $this->caData[1];
        $msAssetColor = RMRiskConfig::getRiskColor($miAssetValue);
        $this->coCellBox->setIconSrc("icon-asset_".$msAssetColor.".gif");
        return parent::drawItem();
        break;
      
      
      default:
        return parent::drawItem();
        break;
    }
  }
}

class SearchAssetEvent extends FWDRunnable {
  public function run(){
    $moGrid = FWDWebLib::getObject('grid_asset_search');
    $moGrid->execEventPopulate();
  }
}

class AssociateAssetEvent extends FWDRunnable {
  public function run(){
    $maGridValue = FWDWebLib::getObject('grid_asset_search')->getValue();
    if(count($maGridValue)){
      $maAsset = $maGridValue[0];
      echo "soPopUp = soPopUpManager.getPopUpById('popup_asset_search');
            soWindow = soPopUp.getOpener();
            soWindow.set_asset({$maAsset[4]},'{$maAsset[2]}');
            soPopUpManager.closePopUp('popup_asset_search');";
    }else{
      echo "js_show('warning');";
    }
  }
}

class AssociateAssetsEvent extends FWDRunnable {
  public function run(){
    $maGridValue = FWDWebLib::getObject('grid_asset_search')->getValue();
    if(count($maGridValue)){
      echo "soPopUp = soPopUpManager.getPopUpById('popup_asset_search');
            soWindow = soPopUp.getOpener();
            soWindow.set_assets(js_get_grid_value('grid_asset_search'));
            soPopUpManager.closePopUp('popup_asset_search');";
    }else{
      echo "js_show('warning');";
    }
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $mbExcludeRelated = FWDWebLib::getObject('par_exclude_related')->getValue();
    
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
    $moStartEvent->addAjaxEvent(new SearchAssetEvent('search_asset_event'));
    $moStartEvent->addAjaxEvent(new AssociateAssetEvent('associate_asset_event'));
    $moStartEvent->addAjaxEvent(new AssociateAssetsEvent('associate_assets_event'));

    $moGrid = $moWebLib->getObject('grid_asset_search');
    $moGrid->setObjFwdDrawGrid(new GridAssetSearch());
    $moHandler = new QueryGridAssetSearch($moWebLib->getConnection());
    $moHandler->setName($moWebLib->getObject('name_filter')->getValue());
    $moHandler->setProcess($moWebLib->getObject('process_id')->getValue());
    $moHandler->setCategory($moWebLib->getObject('category_filter')->getValue());
    $moHandler->setExcludedIds(array_filter(explode(':',FWDWebLib::getObject('var_asset_ids')->getValue())));
    $moHandler->setExcludeRelated($mbExcludeRelated);
    $moGrid->setQueryHandler($moHandler);
    
    if(FWDWebLib::getObject('is_multiple')->getValue()){
      FWDWebLib::getObject('grid_asset_search')->setAttrSelectType('multiple');
      FWDWebLib::getObject('grid_asset_search')->setAttrSelectColumn('4');
    }
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    
    $moSelect = FWDWebLib::getObject('asset_category_id');
    $moQuery = new QueryContextHierarchicalStructure(FWDWebLib::getConnection());
    $moQuery->setContextType(CONTEXT_CATEGORY);
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    foreach ($moQuery->getContexts() as $maContext) {
      list($miContextId,$msHierarchicalName) = $maContext;
      $moSelect->setItemValue($miContextId,$msHierarchicalName);
    }
    
    if($moWebLib->getObject('multiple_selection')->getValue()){
      FWDWebLib::getObject('grid_asset_search')->setAttrSelectType('multiple');
      FWDWebLib::getObject('grid_asset_search')->setAttrSelectColumn('4');
      $moWebLib->getObject('is_multiple')->setValue(1);
    }
    FWDWebLib::getObject('var_asset_ids')->setValue(FWDWebLib::getObject('par_asset_ids')->getValue());
    $moWebLib->dump_html(FWDWebLib::getObject('dialog'));
?>
<script language="javascript">
  gebi('asset_name_filter').focus();
  
  function refresh_grid() {
    gobi('grid_asset_search').setPopulate(true);
    js_refresh_grid('grid_asset_search');
  }
  function enter_asset_search_event(e)
	{
		if (!e) e = window.event;
		if(e['keyCode']==13) {
			gebi('name_filter').value = gebi('asset_name_filter').value;
      gebi('category_filter').value = gebi('asset_category_id').value;
      gobi('grid_asset_search').setPopulate(true);
	    trigger_event("search_asset_event","3");
		}
	}
	FWDEventManager.addEvent(gebi('asset_name_filter'), 'keydown', enter_asset_search_event);
</script>
<?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_asset_search.xml');

?>