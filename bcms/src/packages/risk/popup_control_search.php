<?php
include_once "include.php";
include_once $handlers_ref . "grid/improvement/QueryGridControlSearchIncident.php";

class GridRiskControlSearch extends FWDDrawGrid {
  public function DrawItem(){
    switch($this->ciColumnIndex){
      case 1:
        $msGfxRef = FWDWebLib::getInstance()->getGfxRef();
        $moIcon = new FWDIcon(new FWDBox(2,4));
        if($this->caData[3]==1)
          $moIcon->setAttrSrc("{$msGfxRef}icon-control.gif");
        else
          $moIcon->setAttrSrc("{$msGfxRef}icon-control_red.gif");
        return $moIcon->draw();
        break;
      
      case 2:
        $this->coCellBox->setAttrStringNoEscape("true");
        $this->coCellBox->setValue("<a href='javascript:open_visualize(".$this->caData[1].",".CONTEXT_CONTROL.",\"".uniqid()."\")'>{$this->caData[2]}</a>");
        return $this->coCellBox->draw();
        break;
      
      default:
        return parent::drawItem();
      break;
    }
  }
}

class SearchControlEvent extends FWDRunnable {
  public function run() {
    $moGrid = FWDWebLib::getObject('grid_control_search');
    $moGrid->setAttrPopulate("true");
    $moGrid->execEventPopulate();
  }
}

class AssociateControlsEvent extends FWDRunnable {
  public function run(){
    $maGridValue = FWDWebLib::getObject('grid_control_search')->getValue();
    if(count($maGridValue)){
      echo "soPopUp = soPopUpManager.getPopUpById('popup_control_search');
            soWindow = soPopUp.getOpener();
            soWindow.set_controls(js_get_grid_value('grid_control_search'));
            soPopUpManager.closePopUp('popup_control_search');";
    }else{
      echo "js_show('warning');";
    }
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new SearchControlEvent("search_control_event"));
    $moStartEvent->addAjaxEvent(new AssociateControlsEvent("associate_control_event"));

    $moGrid = FWDWebLib::getObject("grid_control_search");
    $moHandler = new QueryGridControlSearchIncident(FWDWebLib::getConnection());
    $moHandler->setName(FWDWebLib::getObject("var_control_name")->getValue());
    $moHandler->setIdsNot(FWDWebLib::getObject('var_control_ids')->getValue());
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new GridRiskControlSearch());
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    //teste para verificar se o sistema n�o est� sendo hakeado
    $moCtxUserTest = new RMRiskControl();
    $moCtxUserTest->testPermissionToInsert();
    FWDWebLib::getObject('var_control_ids')->setValue(FWDWebLib::getObject('par_control_ids')->getValue());
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
?>
<script language="javascript">
  gebi('control_name').focus();
  
  function refresh_grid() {
    js_refresh_grid('grid_control_search');
  }
</script>
<?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_control_search.xml");
?>