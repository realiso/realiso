<?php
include_once "include.php";
include_once $handlers_ref . "QueryParametersValues.php";
include_once $handlers_ref . "QueryAssetValues.php";
include_once $handlers_ref . "QueryParameterControlRiskPropabylityReduction.php";
include_once $handlers_ref . "select/QuerySelectRiskType.php";
include_once $handlers_ref . "QueryParameterControlValue.php";

function getRiskFromFields(){
  $moRisk = new RMRisk();
  
  $msRiskName = FWDWebLib::getObject('risk_name')->getValue();
  $miRiskAssetId = FWDWebLib::getObject('risk_asset_id')->getValue();
  $msRiskDescription = FWDWebLib::getObject('risk_description')->getValue();
  $msRiskCost = FWDWebLib::getObject('risk_cost')->getValue();
  $miRiskType = FWDWebLib::getObject('select_risk_type')->getValue();

  $moRisk->setFieldValue('risk_name', $msRiskName);
  $moRisk->setFieldValue('risk_asset_id', $miRiskAssetId);
  $moRisk->setFieldValue('risk_description', $msRiskDescription);
  $moRisk->setFieldValue('risk_cost', str_replace(',','',$msRiskCost));
  $moRisk->setFieldValue('risk_impact',FWDWebLib::getObject('risk_impact')->getValue());
  if($miRiskType) 
    $moRisk->setFieldValue('risk_type', $miRiskType);
  else
    $moRisk->setFieldValue('risk_type', 'null');
  return $moRisk;
}

function save($poContext,$pbHasSensitiveChanges,$piContextId=0){
  $miRiskId = FWDWebLib::getObject('risk_id')->getValue();
  $moRisk = getRiskFromFields();

  if($miRiskId){
    //teste para verificar se o sistema n�o est� sendo hakeado
    $moCtxUserTest = new RMRisk();
    $moRiskTest = new RMRisk();
    $moRiskTest->fetchById($miRiskId);
    $moCtxUserTest->testPermissionToEdit($miRiskId,$moRiskTest->getApprover());
    $moRisk->update($miRiskId,true,$pbHasSensitiveChanges);
  }else{
  	$miEventId = FWDWebLib::getObject('event_id')->getValue();
  	if($miEventId) {
  		$moRisk->setFieldValue('risk_event_id', $miEventId);
  	} else {
  		$moRisk->setFieldValue('risk_event_id', 'null');
  	}
  	$miRiskId = $moRisk->insert(true);
  }

  //Salva valores do risco, caso sejam diferentes dos originais
  $moRiskParameter = FWDWebLib::getObject('risk_parameters');
  $msVariableHash = FWDWebLib::getObject('riskparameters_hash')->getValue();
  $msRiskParameterHash = $moRiskParameter->getHash();
  $moRiskParameterValues = new RiskParametersValue($moRiskParameter,RISKPARAMETER_RISK);
  $maRiskParameters = $moRiskParameter->getValues();
  $maRiskParametersJustifications = $moRiskParameter->getJustifications();
  
  if($msVariableHash != $msRiskParameterHash){
    $moRisk->setFieldValue('risk_probability', $maRiskParameters['prob']);
    $moRisk->setFieldValue('risk_justification', $maRiskParametersJustifications['prob']);
    $moRisk->update($miRiskId);
    $moRiskParameterValues->setValues($miRiskId);
  }else{
    $moRisk->setFieldValue('risk_justification', $maRiskParametersJustifications['prob']);
    $moRisk->update($miRiskId);
    $moRiskParameterValues->updateJustification($miRiskId);
  }
  
  /*
   * Se possui o m�dulo de documenta��o, verifica se o ativo
   * associado ao risco mudou. Se mudou, deve atualizar os
   * documentos do controles do ativo antigo e do ativo novo.
   */
  if (FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(POLICY_MODE)) {
	  $miOldAssetId = FWDWebLib::getObject('current_asset_id')->getValue();
	  $miNewAssetId = $poContext->getFieldValue('risk_asset_id');	  
	  if ($miOldAssetId && ($miOldAssetId != $miNewAssetId)) {
	  	$maAssets = array_merge(array($miOldAssetId),array($miNewAssetId));	  	
	  	foreach ($maAssets as $miAssetId) {	  		
		  	$moQuery = new QueryControlsByAsset(FWDWebLib::getConnection());
		    $moQuery->setAsset($miAssetId);
		    $moQuery->makeQuery();
		    $moQuery->executeQuery();
		    $maControls = $moQuery->getControls();	  	
		  	$moControl = new RMControl();
		  	foreach ($maControls as $miControlId) $moControl->updateReaders($miControlId);
	  	}
	  }	  
  }
  
  if(FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(INCIDENT_MODE)){
    if(FWDWebLib::getObject('probability_configuration')->getValue()==':1:'){
      // inserir/atualizar
      $miPeriodUnit = FWDWebLib::getObject('period_unit')->getValue();
      if($miPeriodUnit){ // se n�o t� setado, n�o fo modificado/preenchido
        $miPeriodSize = FWDWebLib::getObject('period_size')->getValue();
        $maProbabilityValues = explode(':',FWDWebLib::getObject('probability_values')->getValue());
        
        $moRiskProbabilities = new CIRiskProbability();
        $moRiskProbabilities->createFilter($miRiskId,'risk_id');
        $moRiskProbabilities->delete();
        
        $moSchedule = new CIRiskSchedule();
        $moSchedule->createFilter($miRiskId,'risk_id');
        $moSchedule->select();
        if($moSchedule->fetch()){
          $moSchedule = new CIRiskSchedule();
          $moSchedule->setFieldValue('schedule_period',$miPeriodUnit);
          $moSchedule->setFieldValue('schedule_value',$miPeriodSize);
          $moSchedule->update($miRiskId);
        }else{
          $moSchedule = new CIRiskSchedule();
          $moSchedule->setFieldValue('schedule_period',$miPeriodUnit);
          $moSchedule->setFieldValue('schedule_value',$miPeriodSize);
          $moSchedule->setFieldValue('risk_id',$miRiskId);
          $moSchedule->insert();
        }
        
        $moParameterValueName = new RMParameterValueName();
        $moParameterValueName->setOrderBy('parametervalue_value','+');
        $moParameterValueName->select();
        $maValueNames = array();
        for($i=0;$i<count($maProbabilityValues);$i++){
          $moParameterValueName->fetch();
          $miValueId = $moParameterValueName->getFieldValue('parametervalue_id');
          $moRiskProbabilities = new CIRiskProbability();
          $moRiskProbabilities->setFieldValue('risk_id',$miRiskId);
          $moRiskProbabilities->setFieldValue('value_name_id',$miValueId);
          $moRiskProbabilities->setFieldValue('incident_amount',$maProbabilityValues[$i]);
          $moRiskProbabilities->insert();
        }
      }
    }else{
      // deletar
      $moRiskProbabilities = new CIRiskProbability();
      $moRiskProbabilities->createFilter($miRiskId,'risk_id');
      $moRiskProbabilities->delete();
      
      $moSchedule = new CIRiskSchedule();
      $moSchedule->delete($miRiskId);
    }
    
  }

  echo "soWindow = soPopUpManager.getPopUpById('popup_risk_edit').getOpener();"
      ."if(soWindow.refresh_grid) soWindow.refresh_grid();"
      ."soPopUpManager.closePopUp('popup_risk_edit');";
}

class ConfirmEditEvent extends FWDRunnable {
  public function run(){
    $miRiskId = FWDWebLib::getObject('risk_id')->getValue();
    $moRisk = getRiskFromFields();
    save($moRisk,true,$miRiskId);
  }
}

class SaveRiskEvent extends FWDRunnable {
  public function run(){
    $miRiskId = FWDWebLib::getObject('risk_id')->getValue();
    $moRisk = getRiskFromFields();
    $moRisk->setHash(FWDWebLib::getObject('hash')->getValue());

    $moRiskParameter = FWDWebLib::getObject('risk_parameters');
    $msVariableHash = FWDWebLib::getObject('riskparameters_hash')->getValue();
    $msRiskParameterHash = $moRiskParameter->getHash();
    
   if($msVariableHash != $msRiskParameterHash){
      $mbHasSensitiveChanges = true;
    }else{
      $mbHasSensitiveChanges = $moRisk->hasSensitiveChanges();
    }
    $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    if($miRiskId && $mbHasSensitiveChanges && $miUserId != $moRisk->getApprover()){
      $msTitle = FWDLanguage::getPHPStringValue('tt_reapproval','Reaprova��o');
      $msMessage = FWDLanguage::getPHPStringValue('st_sensitive_data_reapproval','Voc� est� alterando dados sens�veis, que necessitam passar por uma reaprova��o. Deseja confirmar as altera��es?');
      $msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
                      "soWindow = soPopUp.getOpener();" .
                      "soWindow.trigger_event('confirm_edit',3);";
      ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,75);
    }else{
      save($moRisk,$mbHasSensitiveChanges,$miRiskId);
    }
  }
}

class CalcRiskValue extends FWDRunnable{
	public function run(){
    $moWebLib = FWDWebLib::getInstance();

		$moHandleParameterValue = new QueryParametersValues(FWDWebLib::getConnection());
		$maParValue = $moHandleParameterValue->getParameterValues();
    $maParWeight = array();
 	 	$maAssetValues = array();
    $miCountParAsset = 0;
    $miCountParRisk = 0;
    $miCountParControl = 0;
    $mfValueReal = 0.00;
		$mfValueResidual = 0.00;
		$miParCont = 0.00;
		$miRiskId = $moWebLib->getObject('var_risk_id')->getValue();
    $miRiskCount = ISMSLib::getConfigById(RISK_VALUE_COUNT);
    $msFormulaType = ISMSLib::getConfigById(RISK_FORMULA_TYPE);
    
    //pega os parametros do risco que est� sendo editado
    $moRiskPar = $moWebLib->getObject('risk_parameters');
    $maRiskParValuesAux = $moRiskPar->getValues();
    foreach($maRiskParValuesAux as $miKey => $miValue){
      $maRiskParValues[$miKey] = $maParValue[$miValue];
    }
    $miCountParRisk = count($maRiskParValues);
      
		//obtem os valores dos parametros dos ativos
		$moAssetHandler = new QueryAssetValues(FWDWebLib::getConnection());
		$moAssetHandler->setElementId($moWebLib->getObject('risk_asset_id')->getValue());
	  $moAssetHandler->makeQuery();
		if($moAssetHandler->executeQuery()){
			while($moAssetHandler->fetch()){
				$maAssetValues[$moAssetHandler->getFieldValue("parameter_id")] = $maParValue[$moAssetHandler->getFieldValue("value_id")];
        $maParWeight[$moAssetHandler->getFieldValue("parameter_id")] = $moAssetHandler->getFieldValue("parameter_weight");
			}
		}
    $miCountParAsset = count($maAssetValues);
    //se o ativo esta desparametrizado n�o calcula o risco real nem o residual
    if($miCountParAsset+1 != $miCountParRisk){
      echo "gobi('wn_asset_desparametrized').show(); setTimeout('gobi(\\'wn_asset_desparametrized\\').hide()',5000);";
    }else{
      //calculo do Risco Potencial      
      if ($msFormulaType == RISK_FORMULA_TYPE_1) {
        foreach($maAssetValues as $miKey => $miValue){
          $miParCont += $maParWeight[$miKey];
          $mfValueReal = $mfValueReal + ($miValue+$maRiskParValues[$miKey])*$maParWeight[$miKey];
        }
        if($miParCont==0){
          $miParCont=1;
        }
        $mfValueReal = ($mfValueReal * $maRiskParValues['prob']) / ($miParCont*2);
      }
      else {
        $miRes1 = 0;
        $miRes2 = 0;
        foreach($maAssetValues as $miKey => $miValue){
          $miParCont += $maParWeight[$miKey];
          $miRes1 += ($miValue * $maRiskParValues[$miKey]) * $maParWeight[$miKey];
          $miRes2 += ($miValue + $maRiskParValues[$miKey]) * $maParWeight[$miKey];
        }
        if($miParCont == 0){
          $miParCont = 1;
        }
        $miRes1 = $miRes1 / $miParCont;
        $miRes2 = $miRes2 / $miParCont / 2;        
        $mfValueReal = ($miRes1 / $miRes2) * $maRiskParValues['prob'];
      }
      
      if($miRiskId){
        //obtem os valores dos parametros dos controles
        $moHandlerControl = new QueryParameterControlValue(FWDWebLib::getConnection());
        $moHandlerControl->setRiskId($miRiskId);
        $maControlValues = $moHandlerControl->getParameterValues();
        $miCountParControl = count($maControlValues);
        //verifica se os controles est�o desparametrizados
        if( ($miCountParControl + 1 != $miCountParRisk) && ($miCountParControl != 0) ){
          echo "gobi('wn_control_desparametrized').show(); setTimeout('gobi(\\'wn_control_desparametrized\\').hide()',5000);";
        }else{
          //pega a probabilidade de redu��o dos controles relacionados ao risco
          $moHandlerControlProb = new QueryParameterControlRiskPropabylityReduction(FWDWebLib::getConnection());
          $moHandlerControlProb->setRiskId($miRiskId);
          $miProbCtrlReduction = $moHandlerControlProb->getControlRiskParameterProbabylity();
          $miProbReduction =	$maRiskParValues['prob'] - 	$miProbCtrlReduction;				
          if($miProbReduction<1.00)
            $miProbReduction = 1.00;
          $maReducedValues = array();
          $miContNumPar = 0;
          foreach($maControlValues as $miKey => $miValue){
            $miContNumPar += $maParWeight[$miKey];
            $miAuxDif = $maRiskParValues[$miKey] - $miValue; 
            if($miAuxDif<1.00)
              $maReducedValues[$miKey] = 1.00;
            else
              $maReducedValues[$miKey] = $miAuxDif;
          }
          if($miContNumPar>0){          
            if ($msFormulaType == RISK_FORMULA_TYPE_1) {
              foreach($maReducedValues as $miKey => $miValue){
                $mfValueResidual = $mfValueResidual + ($miValue + $maAssetValues[$miKey])*$maParWeight[$miKey];
              }
              $mfValueResidual = ($mfValueResidual*$miProbReduction)/($miContNumPar*2);
            }
            else {
              $miRes1 = 0;
              $miRes2 = 0;
              foreach($maReducedValues as $miKey => $miValue){
                $miRes1 += ($miValue * $maAssetValues[$miKey]) * $maParWeight[$miKey];
                $miRes2 += ($miValue + $maAssetValues[$miKey]) * $maParWeight[$miKey];
              }
              $miRes1 = $miRes1 / $miContNumPar;
              $miRes2 = $miRes2 / $miContNumPar / 2;
              $mfValueResidual = ($miRes1 / $miRes2) * $miProbReduction;
            }
          }else{
            $mfValueResidual = $mfValueReal;
          }
        }
      }else{
        $mfValueResidual = $mfValueReal;
      }
    }

    $mfValueReal = RMRiskConfig::getRiskValueFormat($mfValueReal);
		$mfValueResidual = RMRiskConfig::getRiskValueFormat($mfValueResidual);
    echo" 
     gobi('risk_real_value').setValue(\"{$mfValueReal}\");
     gobi('risk_residual_value').setValue(\"{$mfValueResidual}\");
    ";
	}
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
    $moStartEvent->addAjaxEvent(new SaveRiskEvent('save_risk'));
    $moStartEvent->addAjaxEvent(new CalcRiskValue('calc_risk_value'));
    $moStartEvent->addAjaxEvent(new ConfirmEditEvent('confirm_edit'));

    $moWebLib = FWDWebLib::getInstance();
    $moRiskParameter = $moWebLib->getObject('risk_parameters');
    $moRiskParameter->populate();
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $miRiskId = FWDWebLib::getObject('risk_id')->getValue();
    $moWindowTitle = FWDWebLib::getObject('window_title');
    $moIcon = FWDWebLib::getObject('tooltip_icon');

    $moHandler = new QuerySelectRiskType(FWDWebLib::getConnection());    
    $moSelect = FWDWebLib::getObject('select_risk_type');
    $moSelect->setQueryHandler($moHandler);
    $moSelect->populate();

    //teste para ver se o custo esta habilitado no sistema
    $moConfig = new ISMSConfig();
    if(!$moConfig->getConfig(GENERAL_COST_ENABLED)){
      FWDWebLib::getObject('cost')->setShouldDraw(false);
      FWDWebLib::getObject('risk_cost')->setShouldDraw(false);
      FWDWebLib::getObject('st_money_symbol')->setShouldDraw(false);
    }
    $msCurrency = constant($moConfig->getConfig(GENERAL_CURRENCY_IDENTIFIER));
	if (!$msCurrency) $msCurrency = '$';
    FWDWebLib::getObject('st_money_symbol')->setValue($msCurrency);

    if($miRiskId){
      $moWebLib->getObject('var_risk_id')->setValue($miRiskId);
      
      $moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_risk_editing', 'Edi��o de Risco'));
      $moRisk = new RMRisk();
      $moRisk->fetchById($miRiskId);
      //teste para verificar se o sistema n�o est� sendo hakeado
      $moCtxUserTest = new RMRisk();
      $moCtxUserTest->testPermissionToEdit($miRiskId,$moRisk->getApprover());
      
      
      $moWebLib->getObject('risk_name')->setValue($moRisk->getFieldValue('risk_name'));
      $moWebLib->getObject('risk_residual_value')->setValue(RMRiskConfig::getRiskValueFormat($moRisk->getFieldValue('risk_residual_value')));
      $moWebLib->getObject('risk_real_value')->setValue(RMRiskConfig::getRiskValueFormat($moRisk->getFieldValue('risk_value')));
      $moWebLib->getObject('risk_description')->setValue($moRisk->getFieldValue('risk_description'));
      $moWebLib->getObject('risk_cost')->setValue(number_format($moRisk->getFieldValue('risk_cost'),2));
      FWDWebLib::getObject("select_risk_type")->checkItem($moRisk->getFieldValue('risk_type'));
      FWDWebLib::getObject('risk_impact')->setValue($moRisk->getFieldValue('risk_impact'));
      $moAsset = new RMAsset();
      $miAssetId = $moRisk->getFieldValue('risk_asset_id');
      $moAsset->fetchById($miAssetId);
      FWDWebLib::getObject('risk_asset_id')->setValue($miAssetId);
      FWDWebLib::getObject('risk_asset_name')->setValue($moAsset->getFieldValue('asset_name'));

      FWDWebLib::getObject('hash')->setValue($moRisk->getHash());

      $moRiskParameter = FWDWebLib::getObject('risk_parameters');
      $moRiskParameterValues = new RiskParametersValue($moRiskParameter,RISKPARAMETER_RISK);
      $moRiskParameterValues->getValues($miRiskId);
      
      //Verifica se os parametros ja foram inseridos. Em caso positivo, nao pode desparametrizar o risco.
      if (count(array_filter($moRiskParameter->getValues()))) {
        FWDWebLib::getObject('parameter_allow_empty')->setValue("0");
      }
      $msHash = $moRiskParameter->getHash();
      FWDWebLib::getObject('riskparameters_hash')->setValue($msHash);

      $moToolTip = ISMSLib::getInstance()->getToolTipCreateModifyById($miRiskId);
      $moIcon->addObjFWDEvent($moToolTip);
      
      /*
       * Se possui o m�dulo de documenta��o, armazena os id do ativo atual
       * para verificar se na hora de salvar n�o mudou.
       */
      if (FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(POLICY_MODE))       
        FWDWebLib::getObject('current_asset_id')->setValue($miAssetId);
      
    }else{
      FWDWebLib::getObject('riskparameters_hash')->setValue(FWDWebLib::getObject('risk_parameters')->getHash());
      
      //teste para verificar se o sistema n�o est� sendo hakeado
      $moCtxUserTest = new RMRisk();
      $moCtxUserTest->testPermissionToInsert();
      
      $moIcon->setAttrDisplay('false');
      $moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_risk_adding', 'Adi��o de Risco'));
      $miEventId = FWDWebLib::getObject('event_id')->getValue();
      if($miEventId){
        $moEvent = new RMEvent();
        $moEvent->fetchById($miEventId);
        FWDWebLib::getObject('risk_name')->setValue($moEvent->getFieldValue('event_description'));
        FWDWebLib::getObject('risk_impact')->setValue($moEvent->getFieldValue('event_impact'));
      }
      $miAssetId = FWDWebLib::getObject('asset_id')->getValue();
      if($miAssetId){
        $moAsset = new RMAsset();
        $moAsset->fetchById($miAssetId);
        FWDWebLib::getObject('risk_asset_id')->setValue($miAssetId);
        FWDWebLib::getObject('risk_asset_name')->setValue($moAsset->getFieldValue('asset_name'));
        FWDWebLib::getObject('vb_search_asset')->setAttrDisabled('true');
      }
    }

    if(FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(INCIDENT_MODE)){
      $moSchedule = new CIRiskSchedule();
      $moSchedule->createFilter($miRiskId,'risk_id');
      $moSchedule->select();
      if($moSchedule->fetch()){
        FWDWebLib::getObject('probability_configuration')->setValue(':1');
        FWDWebLib::getObject('config_probability')->setAttrDisplay(true);
      }
    }else{
      // Se n�o possui o m�dulo de incidente, n�o desenha a viewgroup de c�lculo de probabilidade
      FWDWebLib::getObject('vg_probability_calculation')->setShouldDraw(false);
    }

    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        gebi('risk_name').focus();
        
        function set_asset(id, name){
          gebi('risk_asset_id').value = id;
          gobi('risk_asset_name').setValue(name);
        }
        
        function setProbabilityConfig(piPeriodSize,piPeriodUnit,paValues){
          gebi('period_size').value = piPeriodSize;
          gebi('period_unit').value = piPeriodUnit;
          gebi('probability_values').value = paValues.join(':');
        }
        
      </script>
    <?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_risk_edit.xml');

?>