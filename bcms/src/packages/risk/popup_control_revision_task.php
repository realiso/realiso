<?php
include_once "include.php";
include_once $handlers_ref . "/grid/improvement/QueryGridIncidentByControlId.php";
include_once $handlers_ref . "QueryLastControlRevision.php";
include_once $handlers_ref . "grid/QueryGridUserTasks.php";

class IncidentDrawGrid extends FWDDrawGrid {
  public function drawItem(){
    switch($this->ciColumnIndex){
      case $this->getIndexByAlias('context_id'):
        $this->coCellBox->setIconSrc("icon-ci_incident.gif");
        return $this->coCellBox->draw();
      default:
        return parent::drawItem();
    }
  }
}

class SaveEvent extends FWDRunnable {
	public function run() {
		$moWebLib = FWDWebLib::getInstance();

	  $moTaskTest = new WKFTask();
    $moTaskTest->fetchById($moWebLib->getObject('var_task_id')->getValue());
    //teste para verificar se o sistema n�o est� sendo hakeado
    $moTaskTest->testPermissionToExecute($moWebLib->getObject('var_task_id')->getValue());

    $miControlId = $moWebLib->getObject('var_control_id')->getValue();

		$moRevision = new WKFControlEfficiency();
    $moRevisionUp = new WKFControlEfficiency();
		$moRevision->fetchById($miControlId);
		$moTask = new WKFTask();
		$moTask->fetchById($moWebLib->getObject('var_task_id')->getValue());
    $moHist = new RMControlEfficiencyHistory();
    $mbTaskInduction = false;
    if($moTask->getFieldValue('task_activity')==ACT_INC_CONTROL_INDUCTION){
      $mbTaskInduction = true;
    }


 		//atualiza a efici�ncia real do controle
 		$msREAux = $moWebLib->getObject('controller_er')->getValue();
 		$msRE = "";
 		if($msREAux!=":")
 				$msRE = substr($msREAux,1);
 		$moRevisionUp->setFieldValue('real_efficiency',$msRE);
 		$moRevisionUp->update($moWebLib->getObject('var_control_id')->getValue());

		//atualiza o hist�rico das mudan�as na revis�o do controle
    $moHist = new RMControlEfficiencyHistory();
    $moHist->createFilter(FWDWebLib::getObject('var_control_id')->getValue(),'control_id');
    $moHist->createFilter($moTask->getFieldValue('task_date_created'),'control_date_todo');
    $moHist->delete();

		$moHist = new RMControlEfficiencyHistory();
		$moHist->setFieldValue('control_date_todo',$moTask->getFieldValue('task_date_created'));
		$moHist->setFieldValue('control_id',$moWebLib->getObject('var_control_id')->getValue());
		$moHist->setFieldValue('control_date_realized',ISMSLib::ISMSTime());
		$moHist->setFieldValue('control_real_eff', substr($moWebLib->getObject('controller_er')->getValue(),1));
		$moHist->setFieldValue('control_expected_eff',$moRevision->getFieldValue('expected_efficiency'));
		$moHist->insert();

		//atualiza o estado da task
		$moTask2 = new WKFTask();
		$moTask2->setFieldValue('task_date_accomplished',ISMSLib::ISMSTime());
		$moTask2->setFieldValue('task_is_visible',false);
		$moTask2->update($moWebLib->getObject('var_task_id')->getValue());

    // atualiza o controle
    $moControl = new RMControl();
    $moControl->setFieldValue('control_id',$miControlId);
    $moControl->updateActive();

    $msNextTaskJS = "";
		$mbOpenNextTask = (bool)array_filter(explode(':',FWDWebLib::getObject('next_task_controller')->getValue()));
		if ($mbOpenNextTask) {
		  $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
		  $moHandlerTask = new QueryGridUserTasks(FWDWebLib::getConnection());
      $moHandlerTask->setUser($miUserId);
      $moHandlerTask->setOrderBy('task_date_created','-');
      $moHandlerTask->makeQuery();
      $moHandlerTask->executeQuery();
      if ($moHandlerTask->fetch()) {
  		  $msNextTaskJS = 'soWindow.setTimeout("'.ISMSLib::getTaskPopup($moHandlerTask->getFieldValue('task_id'),
  		                                        $moHandlerTask->getFieldValue('task_activity'),
  		                                        $moHandlerTask->getFieldValue('task_context_id'),
  		                                        $moHandlerTask->getFieldValue('task_context_type'),
  		                                        2).',1");';
      }
		}
    echo "soWindow = soPopUpManager.getPopUpById('popup_control_revision_task').getOpener();"
        ."if (soWindow.refresh_grids)"
        ."  soWindow.refresh_grids();"
        .$msNextTaskJS
        ."soPopUpManager.closePopUp('popup_control_revision_task');";
  }
}

class FilterEvent extends FWDRunnable{
  public function run(){
    if(FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(INCIDENT_MODE)){
      $moGrid = FWDWebLib::getObject('grid_incident_control');
      $moHandler = new QueryGridIncidentByControlId(FWDWebLib::getConnection());
      $miDateBegin = FWDWebLib::getObject('filter_begin_date')->getTimestamp();
      $miDateEnd = FWDWebLib::getObject('filter_end_date')->getTimestamp();

      $moHandler->setControlId(FWDWebLib::getObject('var_control_id')->getValue());
      $moHandler->setDateBegin($miDateBegin);
      $moHandler->setDateEnd($miDateEnd);
      $moGrid->setQueryHandler($moHandler);
      $moGrid->setObjFwdDrawGrid(new IncidentDrawGrid());
      $moGrid->execEventPopulate();

      $moHandler2 = new QueryGridIncidentByControlId(FWDWebLib::getConnection());
      $moHandler2->setControlId(FWDWebLib::getObject('var_control_id')->getValue());
      $moHandler2->setDateBegin($miDateBegin);
      $moHandler2->setDateEnd($miDateEnd);
      $miCount = $moHandler2->getCountQuery();
      echo "
        gebi('var_date_begin_filter').value = $miDateBegin;
        gebi('var_date_end_filter').value = $miDateEnd;
        gobi('total_incidents_period').setValue($miCount);
      ";
    }
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new SaveEvent("save_event"));
    $moStartEvent->addAjaxEvent(new FilterEvent("filter_event"));
    if(FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(INCIDENT_MODE)){
      $moGrid = FWDWebLib::getObject('grid_incident_control');
      $moHandler = new QueryGridIncidentByControlId(FWDWebLib::getConnection('var_control_id'));
      $moHandler->setControlId(FWDWebLib::getObject('var_control_id')->getValue());
      if(FWDWebLib::getObject('var_date_begin_filter')->getValue())
        $moHandler->setDateBegin(FWDWebLib::getObject('var_date_begin_filter')->getValue());
      if(FWDWebLib::getObject('var_date_end_filter')->getValue())
        $moHandler->setDateEnd(FWDWebLib::getObject('var_date_end_filter')->getValue());
      $moGrid->setQueryHandler($moHandler);
      $moGrid->setObjFwdDrawGrid(new IncidentDrawGrid());
    }
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $miAutoTaskOpen = FWDWebLib::getObject('auto_task_open')->getValue();
    if (!$miAutoTaskOpen) FWDWebLib::getObject('next_task')->setShouldDraw(false);
    if ($miAutoTaskOpen==2) FWDWebLib::getObject('next_task_controller')->checkItem(1);

    $moWebLib = FWDWebLib::getInstance();
    $moWebLib->getObject('var_task_id')->setValue($moWebLib->getObject('task_id')->getValue());

    $moTask = new WKFTask();
    $moTask->fetchById($moWebLib->getObject('var_task_id')->getValue());
    //teste para verificar se o sistema n�o est� sendo hakeado
    $moTask->testPermissionToExecute($moWebLib->getObject('var_task_id')->getValue());
    $miControlId = $moWebLib->getObject('control_id')->getValue();

    if(!FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(INCIDENT_MODE)){
      FWDWebLib::getObject('panel_incident')->setShouldDraw(false);
      FWDWebLib::getObject('vb_save')->getObjFWDBox()->setAttrLeft(225);
      FWDWebLib::getObject('vb_close')->getObjFWDBox()->setAttrLeft(310);
      FWDWebLib::getObject('horizontal_bar')->getObjFWDBox()->setAttrWidth(371);
      FWDWebLib::getObject('dialog')->getObjFWDBox()->setAttrWidth(390);
      FWDWebLib::getObject('popup_title')->getObjFWDBox()->setAttrWidth(392);
    }elseif($moTask->getFieldValue('task_activity')==ACT_REAL_EFFICIENCY){
      FWDWebLib::getObject('st_begin_filter')->setShouldDraw(false);
      FWDWebLib::getObject('st_filter_of')->setShouldDraw(false);
      FWDWebLib::getObject('vg_calendar_begin')->setShouldDraw(false);
      FWDWebLib::getObject('st_filter_to')->setShouldDraw(false);
      FWDWebLib::getObject('vg_calendar_end')->setShouldDraw(false);
      FWDWebLib::getObject('vb_filter')->setShouldDraw(false);
      FWDWebLib::getObject('st_period_incident_total')->setShouldDraw(false);
      FWDWebLib::getObject('total_incidents_period')->setShouldDraw(false);
      FWDWebLib::getObject('st_actual_window_total_incident')->setShouldDraw(false);
      FWDWebLib::getObject('st_actual_window_total_incident')->setShouldDraw(false);
      FWDWebLib::getObject('total_incidents_window')->setShouldDraw(false);
      FWDWebLib::getObject('grid_incident_control')->getObjFWDBox()->setAttrTop(32);
      FWDWebLib::getObject('grid_incident_control')->getObjFWDBox()->setAttrHeight(210);
    }else{
      FWDWebLib::getObject('st_panel_incident_title')->setShouldDraw(false);
      $moIncidentControl  = new CIIncidentControl();
      $moIncidentControl->fetchById($moTask->getFieldValue('task_context_id'));
      $moIncident = new CIIncident();
      $moIncident->fetchById($moIncidentControl->getFieldValue('incident_id'));
      $msTitle = FWDLanguage::getPHPStringValue('tt_control_revision_task_incident_revision',"Medi��o Induzida de Efici�ncia do Controle (pelo Incidente '%incident_name%')");
      $mstitle2 = str_replace("%incident_name%",$moIncident->getName(),$msTitle);
      FWDWebLib::getObject('popup_title')->setValue($mstitle2);
      $miControlId = $moIncidentControl->getFieldValue('control_id');
    }
    //revis�o de efficiencia do controle
    $moControlEff = new WKFControlEfficiency();
    $moControlEff->fetchById($miControlId);
    $moWebLib->getObject('var_control_id')->setValue($miControlId);

    if(FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(INCIDENT_MODE)){
      $moQuery = new QueryLastControlRevision();
      $miTimeStamp = FWDWebLib::dateToTimestamp($moQuery->getNextRevision($miControlId));
      $miTimeStampBefore = FWDWebLib::dateToTimestamp($moQuery->getLastRevision($miControlId));

      FWDWebLib::getObject('var_date_begin')->setValue($miTimeStampBefore);
      FWDWebLib::getObject('var_date_end')->setValue($miTimeStamp);
      FWDWebLib::getObject('var_date_begin_filter')->setValue($miTimeStampBefore);
      FWDWebLib::getObject('var_date_end_filter')->setValue($miTimeStamp);
      //Como in�cio do intervalo teremos ent�o a data atual menos a periodicidade da revis�o do controle.
      if(FWDWebLib::getObject('st_panel_incident_title')->getShouldDraw()){
        $msStValue = FWDWebLib::getObject('st_panel_incident_title')->getValue();
        $msStValue = str_replace("%date_begin%",ISMSLib::getISMSShortDate($miTimeStampBefore,true),$msStValue);
        $msStValue = str_replace("%date_today%",ISMSLib::getISMSShortDate($miTimeStamp,true),$msStValue);
        FWDWebLib::getObject('st_panel_incident_title')->setValue($msStValue);
      }else{
        //setar nos variables e tb nos calend�rios de filtro
        FWDWebLib::getObject('filter_begin_date')->setTimestamp($miTimeStampBefore);
        FWDWebLib::getObject('filter_end_date')->setTimestamp($miTimeStamp);
      }

      $moHandler = new QueryGridIncidentByControlId(FWDWebLib::getConnection('var_control_id'));
      $moHandler->setControlId(FWDWebLib::getObject('var_control_id')->getValue());
      $moHandler->setDateBegin(FWDWebLib::getObject('var_date_begin_filter')->getValue());
      $moHandler->setDateEnd(FWDWebLib::getObject('var_date_end_filter')->getValue());
      $miValue = $moHandler->getCountQuery();
      FWDWebLib::getObject('total_incidents_period')->setValue($miValue);
      FWDWebLib::getObject('total_incidents_window')->setValue($miValue);
      FWDWebLib::getObject('grid_incident_control')->setQueryHandler($moHandler);

    }

    $moControl = new RMControl();
    $moControl->fetchById($miControlId);

    $moWebLib->getObject('control_name')->setValue($moControl->getFieldValue('control_name'));
    $moWebLib->getObject('controller_ee')->setValue(':' . $moControlEff->getFieldValue('expected_efficiency'));
    if($moControlEff->getFieldValue('real_efficiency')=='')
      $moWebLib->getObject('controller_er')->setValue(':' . $moControlEff->getFieldValue('expected_efficiency'));
    else
      $moWebLib->getObject('controller_er')->setValue(':' . $moControlEff->getFieldValue('real_efficiency'));
    $moWebLib->getObject('tx_1')->setValue($moControlEff->getFieldValue('value_1'));
    $moWebLib->getObject('tx_2')->setValue($moControlEff->getFieldValue('value_2'));
    $moWebLib->getObject('tx_3')->setValue($moControlEff->getFieldValue('value_3'));
    $moWebLib->getObject('tx_4')->setValue($moControlEff->getFieldValue('value_4'));
    $moWebLib->getObject('tx_5')->setValue($moControlEff->getFieldValue('value_5'));
    $moWebLib->getObject('revision_metric')->setValue($moControlEff->getFieldValue('metric'));

    //tooltips da tela
    $msToolTipText = trim($moControl->getFieldValue('control_name'));
    if($msToolTipText!=''){
      $moToolTip = new FWDToolTip();
      $moToolTip->setAttrShowDelay(1000);
      $moToolTip->setValue($msToolTipText);
      $moWebLib->getObject('control_name')->addObjFWDEvent($moToolTip);
    }

    $msToolTipText = trim($moControlEff->getFieldValue('value_1'));
    if($msToolTipText!=''){
      $moToolTip = new FWDToolTip();
      $moToolTip->setAttrShowDelay(1000);
      $moToolTip->setValue($msToolTipText);
      $moWebLib->getObject('tx_1')->addObjFWDEvent($moToolTip);
    }

    $msToolTipText = trim($moControlEff->getFieldValue('value_2'));
    if($msToolTipText!=''){
      $moToolTip = new FWDToolTip();
      $moToolTip->setAttrShowDelay(1000);
      $moToolTip->setValue($msToolTipText);
      $moWebLib->getObject('tx_2')->addObjFWDEvent($moToolTip);
    }

    $msToolTipText = trim($moControlEff->getFieldValue('value_3'));
    if($msToolTipText!=''){
      $moToolTip = new FWDToolTip();
      $moToolTip->setAttrShowDelay(1000);
      $moToolTip->setValue($msToolTipText);
      $moWebLib->getObject('tx_3')->addObjFWDEvent($moToolTip);
    }

    $msToolTipText = trim($moControlEff->getFieldValue('value_4'));
    if($msToolTipText!=''){
      $moToolTip = new FWDToolTip();
      $moToolTip->setAttrShowDelay(1000);
      $moToolTip->setValue($msToolTipText);
      $moWebLib->getObject('tx_4')->addObjFWDEvent($moToolTip);
    }

    $msToolTipText = trim($moControlEff->getFieldValue('value_5'));
    if($msToolTipText!=''){
      $moToolTip = new FWDToolTip();
      $moToolTip->setAttrShowDelay(1000);
      $moToolTip->setValue($msToolTipText);
      $moWebLib->getObject('tx_5')->addObjFWDEvent($moToolTip);
    }

    $moWebLib->dump_html($moWebLib->getObject('dialog'));
  }
}
  FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
  FWDWebLib::getInstance()->xml_load("popup_control_revision_task.xml");
?>