<?php
include_once "include.php";
include_once $handlers_ref . "grid/QueryGridBestPracticeSearch.php";
include_once $handlers_ref . "QueryContextHierarchicalStructure.php";
include_once $handlers_ref . "grid/QueryGridBestPracticeSearchById.php";

class GridControl extends FWDDrawGrid {
  public function drawItem(){
    switch($this->ciColumnIndex){
      case 1:
        $this->coCellBox->setIconSrc("icon-best_practice.gif");
        return parent::drawItem();
        break;
      
      case 2:
        $this->coCellBox->setAttrStringNoEscape("true");
        $this->coCellBox->setValue("<a href='javascript:open_visualize(".$this->caData[1].",".CONTEXT_BEST_PRACTICE.",\"".uniqid()."\")'>{$this->caData[2]}</a>");
        return $this->coCellBox->draw();
        break;
      
      default:
        return parent::drawItem();
        break;
    }
  }
}

class SearchEvent extends FWDRunnable {
  public function run(){
    $moGrid = FWDWebLib::getObject('best_practiceGd');
    $moGrid->execEventPopulate();
	}
}

class RemoveEvent extends FWDRunnable {
	public function run() {		
		// atualiza grid de usu�rios selecionados
		$moGridSelected = FWDWebLib::getObject('best_practiceGdName');
		$moGridSelectedHandler = $moGridSelected->getQueryHandler();

		$maSelectAux = array();
		$maGridValueAux = array();
		$maSelectIds = array_unique(array_filter(explode(':',FWDWebLib::getObject('selected_ids')->getValue())));
		$maGridSel = array_unique(array_filter($moGridSelected->getValue() ));
		$maSelectAux = array_diff($maSelectIds,$maGridSel);

		$msAuxSelAux = implode(':',$maSelectAux);
		$moJsSetContent = new FWDJsEvent(JS_SET_CONTENT,'selected_ids',$msAuxSelAux);
		echo $moJsSetContent->render();
	
		$moGridSelectedHandler->setSectionsIds($msAuxSelAux);
		$moGridSelected->setQueryHandler($moGridSelectedHandler);
		echo "trigger_event('best_practiceGdNamerefresh',3);";
	
		$msIds =  FWDWebLib::getObject('selected_ids')->getValue();
		$moGrid = FWDWebLib::getObject("best_practiceGd");
		$moGrid->getQueryHandler()->setSectionsIds($msAuxSelAux);
		echo "trigger_event('best_practiceGdrefresh',3);";
	}
}

class InsertEvent extends FWDRunnable {
	public function run() {
		$msIds =  FWDWebLib::getObject('selected_ids')->getValue();
		$moGrid = FWDWebLib::getObject("best_practiceGd");
		$msIds = $msIds . ':' . implode(':',$moGrid->getValue());
		
		$moGridSelected = FWDWebLib::getObject('best_practiceGdName');
		$moGridSelectedHandler = $moGridSelected->getQueryHandler();
		$moGridSelectedHandler->setSectionsIds($msIds);
		$msContent = implode(':',$moGridSelectedHandler->getIds());
		$moGridSelected->setQueryHandler($moGridSelectedHandler);
		echo "trigger_event('best_practiceGdNamerefresh',3);";

		$moJsSetContent = new FWDJsEvent(JS_SET_CONTENT,'selected_ids',$msIds);
		echo $moJsSetContent->render();

		$msIds =  FWDWebLib::getObject('selected_ids')->getValue();
		$moGrid->getQueryHandler()->setSectionsIds($msIds);
		echo "trigger_event('best_practiceGdrefresh',3);";
	}
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $msIds = FWDWebLib::getObject('selected_ids')->getValue();
    
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new SearchEvent("control_search_event"));
    $moStartEvent->addAjaxEvent(new InsertEvent("control_insert_event"));
    $moStartEvent->addAjaxEvent(new RemoveEvent("control_remove_event"));
    
    $moGrid = FWDWebLib::getObject("best_practiceGd");
    $moGrid->setObjFWDDrawGrid(new GridControl());
    $moHandler = new QueryGridBestPracticeSearch(FWDWebLib::getConnection());
    $moHandler->setSectionsIds($msIds);
    $moHandler->setSectionID(FWDWebLib::getObject("section_id_filter")->getValue());
    $moHandler->setName(FWDWebLib::getObject("search_filter_name")->getValue());
    $moGrid->setQueryHandler($moHandler);
    
    $moGridName = FWDWebLib::getObject("best_practiceGdName");
    $moGridName->setObjFWDDrawGrid(new GridControl());
    $moHandlerName = new QueryGridBestPracticeSearchById(FWDWebLib::getConnection());
    $moHandlerName->setSectionsIds($msIds);
    $moGridName->setQueryHandler($moHandlerName);
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    //teste para verificar se o sistema n�o est� sendo hakeado
    $moCtxUserTest = new RMControlBestPractice();
    $moCtxUserTest->testPermissionToInsert();
    
    $moWebLib = FWDWebLib::getInstance();

    $msIds = FWDWebLib::getObject('selected_ids')->getValue();
		$msAuxIds = FWDWebLib::getObject('best_practice_ids')->getValue();
		if($msAuxIds){
			$msIds .= ':' . $msAuxIds;
			FWDWebLib::getObject('selected_ids')->setValue($msIds);
		}
    
    $moGridName = FWDWebLib::getObject("best_practiceGdName");
    $moHandlerName = new QueryGridBestPracticeSearchById(FWDWebLib::getConnection());
    $moHandlerName->setSectionsIds($msIds);
    $moGridName->setQueryHandler($moHandlerName);
    
    $moSelect = FWDWebLib::getObject('selBestPractice');
    $moQuery = new QueryContextHierarchicalStructure(FWDWebLib::getConnection());
    $moQuery->setContextType(CONTEXT_SECTION_BEST_PRACTICE);
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    foreach ($moQuery->getContexts() as $maContext) {
      list($miContextId,$msHierarchicalName) = $maContext;
      $moSelect->setItemValue($miContextId,$msHierarchicalName);
    }
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
?>
<script language="javascript">
  gebi('tp_filter_name').focus();
  
  function refresh_grid() {
    gobi('best_practiceGd').setPopulate(true);
    js_refresh_grid('best_practiceGd');
    gobi('best_practiceGdName').setPopulate(true);
    js_refresh_grid('best_practiceGdName');
  }
  function enter_bestpractice_search_event(e)
	{
		if (!e) e = window.event;
		if(e['keyCode']==13) {
			gebi('search_filter_name').value = gebi('tp_filter_name').value;
      gebi('section_id_filter').value = gebi('selBestPractice').value;
      gobi('best_practiceGd').setPopulate(true);
	    trigger_event("control_search_event","3");
		}
	}
	FWDEventManager.addEvent(gebi('tp_filter_name'), 'keydown', enter_bestpractice_search_event);
</script>
<?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_best_practice_search.xml");
?>