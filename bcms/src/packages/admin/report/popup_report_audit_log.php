<?php
set_time_limit(3600);
include_once "include.php";

class LogLevelIterator extends FWDReportLevelIterator {  

  public function __construct(){
    parent::__construct("log_date");
  }
	
	/*
	 * Necess�rio pois esse relat�rio n�o possui
	 * um identificador �nico para as linhas
	 */
	public function changedLevel(FWDDBDataSet $poDataSet){
		return true;
	}
	
  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msDate = ISMSLib::getISMSDate($poDataSet->getFieldByAlias("log_date")->getValue());    
    $msDescription = $poDataSet->getFieldByAlias("log_description")->getValue();	
    
    $moWebLib->getObject('date')->setValue($msDate);
    $moWebLib->getObject('log_description')->setValue($msDescription);    
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $moWebLib->getObject("level_log")->setLevelIterator(new LogLevelIterator());    
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport($moWebLib->getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    $moComponent->setReportFilter($moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter());
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('rs_systems_log_audit',"Auditoria do Log do Sistema"));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_report_audit_log.xml");
?>