<?php
set_time_limit(3600);
include_once "include.php";

class AlertLevelIterator extends FWDReportLevelIterator {  

  public function __construct(){
    parent::__construct("alert_id");
  }
	
  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msDateCreated = $poDataSet->getFieldByAlias("alert_date")->getValue();   
    $msDateCreated = $msDateCreated ? ISMSLib::getISMSDate($msDateCreated) : '';
        
    $msCreator = $poDataSet->getFieldByAlias("alert_creator")->getValue(); 
    $msReceiver = $poDataSet->getFieldByAlias("alert_receiver")->getValue();
    
    $msJustification = $poDataSet->getFieldByAlias("alert_justification")->getValue();
    $miType = $poDataSet->getFieldByAlias("alert_type")->getValue();
    $msContext = $poDataSet->getFieldByAlias("alert_context_name")->getValue();
    $miContextType = $poDataSet->getFieldByAlias("alert_context_type")->getValue();
    $miContextId = $poDataSet->getFieldByAlias("alert_context_id")->getValue();
    
    $moAlert = new WKFAlert();
    $msDescription = $moAlert->getDescription($miType, $miContextType, $msContext, $miContextId);  	
    
    $moWebLib->getObject('date_created')->setValue($msDateCreated);    
    $moWebLib->getObject('alert_creator')->setValue($msCreator);
    $moWebLib->getObject('alert_receiver')->setValue($msReceiver);
    $moWebLib->getObject('alert_description')->setValue($msDescription);
    $moWebLib->getObject('alert_justification')->setValue($msJustification);
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $moWebLib->getObject("level_alert")->setLevelIterator(new AlertLevelIterator());    
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport($moWebLib->getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    $moComponent->setReportFilter($moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter());
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('rs_alerts_audit',"Auditoria de Alertas"));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_report_audit_alert.xml");
?>