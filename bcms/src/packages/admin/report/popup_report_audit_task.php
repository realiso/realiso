<?php
set_time_limit(3600);
include_once "include.php";

class TaskLevelIterator extends FWDReportLevelIterator {  

  public function __construct(){
    parent::__construct("task_id");
  }
	
  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msDateCreated = $poDataSet->getFieldByAlias("task_creation_date")->getValue();   
    $msDateCreated = $msDateCreated ? ISMSLib::getISMSDate($msDateCreated) : '';
    $msDateAccomplished = $poDataSet->getFieldByAlias("task_accomplished_date")->getValue(); 
    $msDateAccomplished = $msDateAccomplished ? ISMSLib::getISMSDate($msDateAccomplished) : '';
    $msCreator = $poDataSet->getFieldByAlias("task_creator")->getValue(); 
    $msReceiver = $poDataSet->getFieldByAlias("task_receiver")->getValue();
    $msContext = $poDataSet->getFieldByAlias("task_context_name")->getValue();
    $miActivity = $poDataSet->getFieldByAlias("task_activity")->getValue();    
    $msDescription = ISMSActivity::getDescription($miActivity) . ": " . $msContext;
  	
    
    $moWebLib->getObject('date_created')->setValue($msDateCreated);    
    $moWebLib->getObject('date_accomplished')->setValue($msDateAccomplished);
    $moWebLib->getObject('task_creator')->setValue($msCreator);
    $moWebLib->getObject('task_receiver')->setValue($msReceiver);
    $moWebLib->getObject('task_description')->setValue($msDescription);
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $moWebLib->getObject("level_task")->setLevelIterator(new TaskLevelIterator());    
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport($moWebLib->getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    $moComponent->setReportFilter($moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter());
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('rs_tasks_audit',"Auditoria de Tarefas"));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_report_audit_task.xml");
?>