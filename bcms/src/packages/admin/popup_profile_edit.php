<?php
include_once "include.php";

class ReloadProfileACLs extends FWDRunnable {
  public function run(){
      $miProfileId = FWDWebLib::getObject('profile_id')->getValue();
      $moProfileAux = new ISMSProfile();
      $moProfileAux->setFieldValue('profile_id', $miProfileId);
      $moProfileAux->getDeniedAcls(true);
      
      echo "soWindow = soPopUpManager.getPopUpById('popup_profile_edit').getOpener();"
          ."if(soWindow.refresh_grid) soWindow.refresh_grid();"
          ."if(soPopUpManager.getRootWindow().refresh) soPopUpManager.getRootWindow().refresh();"
          ."soPopUpManager.closePopUp('popup_profile_edit');";
  }
}

class SaveEvent extends FWDRunnable {
  public function run() {    
      $msProfileCheck = FWDWebLib::getObject('var_checkController')->getValue();
      $msProfileName = FWDWebLib::getObject('tx_profile_name')->getValue();
      $miProfileId = FWDWebLib::getObject('profile_id')->getValue();
      $moProfile = new ISMSProfile();
      $mbRefreshWindow = false;

      //teste para verificar se o sistema n�o est� sendo hakeado em um update
      if($miProfileId){
        $moCtxUserTest = new ISMSProfile();
        $moCtxUserTest->testPermissionToEdit($miProfileId);
      }

      if($miProfileId){
        $moProfile->setFieldValue('profile_name',$msProfileName);
        $moProfile->update($miProfileId);

        $miCurrentUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
        $moUser = new ISMSUser();
        $moUser->fetchById($miCurrentUserId);

        if($miProfileId == $moUser->getFieldValue('user_profile_id')){
          $mbRefreshWindow = true;
          $moProfileAux = new ISMSProfile();
          $moProfileAux->setFieldValue('profile_id', $miProfileId);
          $moProfileAux->getDeniedAcls(true);
        }

      }else{
        $miProfileId = $moProfile->insert($msProfileName);
      }
      if($miProfileId){
        $moProfile->insertAcls($miProfileId,$msProfileCheck);
      }

    if($mbRefreshWindow){
      echo "trigger_event('reload_profile_acls_event',3);";
    }else{
      echo "soWindow = soPopUpManager.getPopUpById('popup_profile_edit').getOpener();"
          ."if(soWindow.refresh_grid) soWindow.refresh_grid();"
          ."soPopUpManager.closePopUp('popup_profile_edit');";
    }
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new SaveEvent("save_event"));  
    $moStartEvent->addAjaxEvent(new ReloadProfileACLs("reload_profile_acls_event"));
    
    FWDWebLib::getInstance()->xml_load('acl_tree.xml',false);
    
    $moLicense = new ISMSLicense();
    $mbHasPolicyMode = in_array(POLICY_MODE, $moLicense->getModules());  
    $mbHasIncidentMode = in_array(INCIDENT_MODE, $moLicense->getModules());
    
    if(!$mbHasPolicyMode){
      FWDWebLib::getObject('tree_M.PM')->setShouldDraw(false);
      FWDWebLib::getObject('tree_M.L.5')->setShouldDraw(false);
      FWDWebLib::getObject('tree_M.D.6.11')->setShouldDraw(false);
    }
    
    if(!$mbHasIncidentMode){
      FWDWebLib::getObject('tree_M.CI')->setShouldDraw(false);
      FWDWebLib::getObject('tree_M.D.6.12')->setShouldDraw(false);
    }
      
    FWDWebLib::getObject('profile_panel_tree')->addObjFWDView(FWDWebLib::getObject('ACL_profile_tree_base'));
    
    if(in_array(ISMSSaaS::getConfig(ISMSSaaS::LICENSE_TYPE), ISMSLicense::getTrialLicenseTypes())){
      FWDWebLib::getObject('tree_M.L.4')->setShouldDraw(false);
      FWDWebLib::getObject('tree_M.L.4.1')->setShouldDraw(false);
      FWDWebLib::getObject('tree_M.L.4.2')->setShouldDraw(false);
    }

  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $miProfileId = FWDWebLib::getObject('par_profile_id')->getValue();
    FWDWebLib::getObject('profile_id')->setValue($miProfileId);
    //teste para verificar se o sistema n�o est� sendo hakeado
    $moCtxUserTest = new ISMSProfile();
    if($miProfileId){
      $moProfile = new ISMSProfile();
      $moProfile->fetchById($miProfileId);
      if($moProfile->getFieldValue('profile_identifier')){
        FWDWebLib::getObject('tx_profile_name')->setAttrDisabled('true');
        FWDWebLib::getObject('popup_title')->setValue(FWDLanguage::getPHPStringValue('tt_visualizae_profile','Visualizar Perfil'));
        FWDWebLib::getObject('save_button')->setShouldDraw(false);
      }else{
        $moCtxUserTest->testPermissionToEdit($miProfileId);
        FWDWebLib::getObject('popup_title')->setValue(FWDLanguage::getPHPStringValue('tt_edit_profile','Editar Perfil'));
        FWDWebLib::getObject('profile_warning')->setShouldDraw(false);
      }
    }else{
      $moCtxUserTest->testPermissionToInsert();
      FWDWebLib::getObject('popup_title')->setValue(FWDLanguage::getPHPStringValue('tt_insert_profile','Inserir Perfil'));
      FWDWebLib::getObject('profile_warning')->setShouldDraw(false);
    }
    
    $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    if($miProfileId){
      FWDWebLib::getObject('tx_profile_name')->setValue($moProfile->getFieldValue('profile_name'));
      FWDWebLib::getObject('ACL_profile_check_controller')->setValue(implode(':',$moProfile->getAcls()));
    }
    
      
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));  
    ?>
      <script language="javascript">
        gebi('tx_profile_name').focus();
      </script>
    <?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_profile_edit.xml");

?>