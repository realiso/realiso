<?php

include_once 'include.php';

class UpgradeLicenseEvent extends FWDRunnable {
  public function run(){
    $msActivationCodeText = FWDWebLib::getObject('activation_code')->getValue();
    
    $maCodes = explode(PHP_EOL, $msActivationCodeText);
    $maFilteredCodes = array();
    foreach ($maCodes as $msCode) {
      $msCode = trim($msCode);
      if ($msCode) $maFilteredCodes[] = trim($msCode);
    }

    $moSaasAdmin = new ISMSSaaSAdmin();
    $moISMSSaaS = new ISMSSaaS();
    $msAlias = $moISMSSaaS->getConfig(ISMSSaaS::LICENSE_CLIENT_NAME);
    $miInstanceId = $moSaasAdmin->getInstanceId($msAlias);
    
    $miInvalidCodes = 0;
    foreach ($maFilteredCodes as $msActivationCode) {
      $miActivationCodeId = $moSaasAdmin->getActivationId($msActivationCode);
      if($miActivationCodeId){
        // Atualiza a ativa��o pra associar com a inst�ncia e armazenar a data em que foi usada
        if ($miInstanceId) $moSaasAdmin->updateActivationId($miActivationCodeId,$miInstanceId);
        
        // Modifica a licen�a pra aceitar mais 1 usu�rio, por�m desconsidera 1 caso o cliente esteja desbloqueando a licen�a.
        // Isso foi alterado pois antes o cliente sempre ganhava um usu�rio extra ap�s o desbloqueio da licen�a. Agora o cliente n�o ganha mais usu�rio gr�tis.
        $moLicense = new ISMSLicense();
        if ($moISMSSaaS->getConfig(ISMSSaaS::LICENSE_EXPIRACY)) {
          $moLicense->unlockLicense();
        }
        else {
          $moLicense->setMaxUsers($moLicense->getMaxUsers() + 1);
          $moLicense->setMaxSimultaneousUsers($moLicense->getMaxSimultaneousUsers() + 1);
          $moLicense->updateHash();
        }
      }else{
        $miInvalidCodes++;
      }
    }

    if (!$miInvalidCodes) {
      echo "gobi('license_grid').refresh();"
              ."gebi('activation_code').value='';"
              ."gobi('warning_successful_activation').show();";
    }else{
      echo "gobi('license_grid').refresh();"
              ."gobi('warning_invalid_code').show();";
    }
  }
}

class ChangeEmailEvent extends FWDRunnable {
  public function run(){
    $moISMSSaaS = new ISMSSaaS();
    $moISMSSaaS->setConfig(ISMSSaaS::EMAIL_ADMIN,FWDWebLib::getObject('email')->getValue());
    echo "gobi('warning_email').show();";
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->addAjaxEvent(new ChangeEmailEvent('change_email'));
    $moStartEvent->addAjaxEvent(new UpgradeLicenseEvent('upgrade_license'));
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
    
    // Popula a grid
    $soLicense = new ISMSLicense();
    $miExpiracy = $soLicense->getExpiracy();
    if($miExpiracy){
      $msExpiracy = ISMSLib::getISMSShortDate($miExpiracy,true);
    }else{
      $msExpiracy = FWDLanguage::getPHPStringValue('st_expiracy_date_never',"Nunca");
    }
    
    $siRow = 1;
    $soGrid = FWDWebLib::getObject('license_grid');
    
    $soGrid->setItem(1, $siRow, FWDLanguage::getPHPStringValue('st_client_name', "Nome do Cliente"));
    $soGrid->setItem(2, $siRow++, $soLicense->getClientName());
    
    $soGrid->setItem(1, $siRow, FWDLanguage::getPHPStringValue('st_number_of_users', "N�mero de Usu�rios"));
    $soGrid->setItem(2, $siRow++, $soLicense->getMaxUsers());
    
    $soGrid->setItem(1, $siRow, FWDLanguage::getPHPStringValue('st_activation_date', "Data de Ativa��o"));
    $soGrid->setItem(2, $siRow++, ISMSLib::getISMSShortDate($soLicense->getActivationDate(),true));
    
    $soGrid->setItem(1, $siRow, FWDLanguage::getPHPStringValue('st_expiracy_date', "Data de Expira��o"));
    $soGrid->setItem(2, $siRow++, $msExpiracy);
    
    $soGrid->setItem(1, $siRow, FWDLanguage::getPHPStringValue('st_max_simult_users', "M�x. Usu�rios Simult�neos"));
    $soGrid->setItem(2, $siRow++, $soLicense->getMaxSimultaneousUsers());
    
    $soGrid->setItem(1, $siRow, FWDLanguage::getPHPStringValue('st_license_type', "Tipo de Licen�a"));
    $soGrid->setItem(2, $siRow++, $soLicense->getLicenseType());
    
    // N�o desenha o panel de upgrade de licen�a se for trial
    if(in_array(ISMSSaaS::getConfig(ISMSSaaS::LICENSE_TYPE), ISMSLicense::getTrialLicenseTypes())){
      FWDWebLib::getObject('pn_upgrade_license')->setShouldDraw(false);
      $miLeft = FWDWebLib::getObject('pn_upgrade_license')->getObjFWDBox()->getAttrLeft();
      FWDWebLib::getObject('pn_deactivate_account')->getObjFWDBox()->setAttrLeft($miLeft);
    }
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moISMSSaaS = new ISMSSaaS();
    FWDWebLib::getObject('email')->setValue($moISMSSaaS->getConfig(ISMSSaaS::EMAIL_ADMIN));
    
    $msAxurEmail = $moISMSSaaS->getConfig(ISMSSaaS::SAAS_REPORT_EMAIL);
    $msDeactivateAccountText = str_replace(
      '%email%',
      $msAxurEmail,
      FWDLanguage::getPHPStringValue('st_deactivate_account_text',"Para desativar sua conta, clique no bot�o abaixo.<br/>Seu banco de dados ser� mantido por 30 dias. Depois disso, todos os dados ser�o deletados.Para receber o banco, contate-nos em %email%")
    );
    FWDWebLib::getObject('deactivate_account_text')->setValue($msDeactivateAccountText);
    
    $msErrorEmail = ISMSLib::getConfigById(ERROR_REPORT_EMAIL);
    $msMessage = FWDWebLib::getObject('warning_invalid_code')->getValue();
    $msMessage = str_replace('%email%', $msErrorEmail, $msMessage);
    FWDWebLib::getObject('warning_invalid_code')->setValue($msMessage);
    
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    if(!in_array(ISMSSaaS::getConfig(ISMSSaaS::LICENSE_TYPE), ISMSLicense::getTrialLicenseTypes())){
      ?>
        <script language="javascript">
          gobi('activation_code').setMaskRegExp(/[0-9a-zA-Z+\/=]{34}/);
          gobi('activation_code').onRequiredCheckNotOk = function(){ gobi('warning_invalid_code').show(); };
        </script>
      <?php
    }
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('nav_my_account.xml');

?>
<!--[if lt IE 7]>
<script defer type='text/javascript' language='javascript'>
  correct_png();
</script>
<![endif]-->