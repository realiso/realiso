<?php
include_once "include.php";
include_once $handlers_ref . "grid/QueryGridProfiles.php";

class ProfileDrawGrid extends FWDDrawGrid {

	public function drawItem(){
		switch($this->ciColumnIndex){
			case $this->getIndexByAlias('profile_name'):
				$maMenuACLs = array('A.P.1','A.P.2','visualize');
				$maAllowed = array();
				$maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();

				if (!in_array('A.P.1', $maACLs) && !$this->caData[3]) $maAllowed[] =  'A.P.1';
				if (!in_array('A.P.2', $maACLs) && !$this->caData[3])	$maAllowed[] =  'A.P.2';
				if ($this->caData[3] > 0) $maAllowed[] =  'visualize';

				$moACL = FWDACLSecurity::getInstance();
				$moACL->setNotAllowed(array_diff($maMenuACLs,$maAllowed));
				$moMenu = FWDWebLib::getObject('menu');
				$moGrid = FWDWebLib::getObject('grid_profiles');
				$msLine = $moGrid->getAttrName().'_acl_'.$this->ciRowIndex;
				FWDGridMenuACLSecurity::getInstance()->installGridMenuACL($moMenu,$moACL,$moGrid,$msLine);

				return parent::drawItem();
				break;
					
			default:
				return parent::drawItem();
				break;
		}
	}

}

class ProfileRemoveConfirm extends FWDRunnable {
	public function run(){
		$msTitle = FWDLanguage::getPHPStringValue('tt_remove_profile','Remover Perfil');
		$msMessage = FWDLanguage::getPHPStringValue('st_profile_remove_message',"Voc� tem certeza que deseja remover o perfil <b>%profile_name%</b>?");

		$moProfile = new ISMSProfile();
		$moProfile->fetchById(FWDWebLib::getObject('profile_id')->getValue());
		$msProfileName = ISMSLib::truncateString($moProfile->getFieldValue('profile_name'), 70);
		$msMessage = str_replace("%profile_name%",$msProfileName,$msMessage);

		$msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
                    "soWindow = soPopUp.getOpener();" .
                    "soWindow.remove_profile();";

		ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,50);
	}
}

class RemoveProfileEvent extends FWDRunnable {
	public function run(){
		$miProfilelId = FWDWebLib::getObject('profile_id')->getValue();
		$moProfile = new ISMSProfile();
		$moProfile->delete($miProfilelId);
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));
		$moStartEvent->addAjaxEvent(new ProfileRemoveConfirm('profile_remove_confirm'));
		$moStartEvent->addAjaxEvent(new RemoveProfileEvent("remove_profile_event"));
		//instala a seguran�a de ACL na p�gina
		FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
		FWDMenuACLSecurity::getInstance()->installMenuACL(FWDWebLib::getObject('menu'),FWDACLSecurity::getInstance());

		$moGrid = FWDWebLib::getObject("grid_profiles");
		$moHandler = new QueryGridProfiles(FWDWebLib::getConnection());
		$moGrid->setQueryHandler($moHandler);
		$moGrid->setObjFwdDrawGrid(new ProfileDrawGrid());
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
<script language="javascript">
			function refresh_grid() {
				js_refresh_grid('grid_profiles');		
			}
			function remove_profile() {
				trigger_event('remove_profile_event', 3);
				refresh_grid();				
			}
		</script>
		<?
		 
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_profile.xml");
?>