<?php

include_once "include.php";
include_once $handlers_ref . "QueryContextNames.php";

class LoadElementEvent extends FWDRunnable {
  public function run() {
    $maElements = FWDWebLib::unserializeString(FWDWebLib::getObject("contexts")->getValue());
    if (count($maElements)==0) {
      echo "soPopUp = soPopUpManager.getPopUpById('popup_associate_elements');" .
           "soWindow = soPopUp.getOpener();" .
           "soWindow.refresh_grid();" .
           "if(gebi('has_restored_element').value) soWindow.show_restored_event();".
           "soPopUpManager.closePopUp('popup_associate_elements');";
      return;
    }
    
    $miContextId = 0;
    $miContextType = 0;
    $miParentType = 0;
    
    $mbFirst = true;
    $maNewElements = array();
    foreach ($maElements as $miElementId => $miElementType) {
      if ($mbFirst) {
        $miContextId = $miElementId;
        echo "gebi('current_id').value = '$miElementId';";
        $miContextType = $miElementType;
        echo "gebi('current_type').value = '$miElementType';";
        $mbFirst = false;
      }
      else
        $maNewElements[$miElementId] = $miElementType;
    }
    
    echo "gebi('contexts').value = '".serialize($maNewElements)."';";
    
    $moContextObject = new ISMSContextObject();
    $moContext = $moContextObject->getContextObject($miContextType);
    $moContext->fetchById($miContextId);
    
    $msContextTypeName = "";
    switch($miContextType) {
      case CONTEXT_AREA:
        $moContextObj = new RMArea();
        $msContextTypeName = $moContextObj->getLabel();
        $miParentType = CONTEXT_AREA;
        break;
      case CONTEXT_PROCESS:
        $moContextObj = new RMProcess();
        $msContextTypeName = $moContextObj->getLabel();
        $miParentType = CONTEXT_AREA;
        break;
      case CONTEXT_RISK:
        $moContextObj = new RMRisk();
        $msContextTypeName = $moContextObj->getLabel();
       
        $miParentType = CONTEXT_ASSET;
        break;
      case CONTEXT_CATEGORY:
        $moContextObj = new RMCategory();
        $msContextTypeName = $moContextObj->getLabel();
        $miParentType = CONTEXT_CATEGORY;
        break;
      case CONTEXT_EVENT:
        $moContextObj = new RMEvent();
        $msContextTypeName = $moContextObj->getLabel();
        $miParentType = CONTEXT_CATEGORY;
        break;
      case CONTEXT_BEST_PRACTICE:
        $moContextObj = new RMBestPractice();
        $msContextTypeName = $moContextObj->getLabel();
        $miParentType = CONTEXT_SECTION_BEST_PRACTICE;
        break;
      case CONTEXT_SECTION_BEST_PRACTICE:
        $moContextObj = new RMSectionBestPractice();
        $msContextTypeName = $moContextObj->getLabel();
        $miParentType = CONTEXT_SECTION_BEST_PRACTICE;
        break;
      case CONTEXT_USER:
        $moContextObj = new ISMSUser();
        $msContextTypeName = $moContextObj->getLabel();
        $miParentType = CONTEXT_PROFILE;
        break;
      case CONTEXT_CI_SOLUTION:
        $moContextObj = new CISolution();
        $msContextTypeName = $moContextObj->getLabel();
        $miParentType = CONTEXT_CI_CATEGORY;
        break;
      case CONTEXT_CI_OCCURRENCE:
        $moContextObj = new CIOccurrence();
        $msContextTypeName = $moContextObj->getLabel();
        $miParentType = CONTEXT_CI_INCIDENT;
      break;
      default : 
        $msContextTypeName = "CONTEXT_UNKNOWN"; 
      break;
    }
    
    echo "gebi('element_type_td').innerHTML = '".$msContextTypeName."';";
    echo "gebi('element_name_td').innerHTML = '".$moContext->getName()."';";
    
    $moGrid = FWDWebLib::getObject("grid_associate");
    
    $moQuery = new QueryContextNames(FWDWebLib::getConnection());
    $moQuery->setState(CONTEXT_STATE_DELETED);
    $moQuery->setType($miParentType);
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    $miRow = 1;
    foreach ($moQuery->getContexts() as $miContextId => $msContextName) {
      $moGrid->setItem(1, $miRow, $miContextId);
      $moGrid->setItem(2, $miRow++, $msContextName);
    }
    $moDrawItem = new GridAssociate();
    $moDrawItem->setType($miParentType);
    $moGrid->setObjFwdDrawGrid($moDrawItem);
    $moGrid->execEventPopulate();
  }
}

class GridAssociate extends FWDDrawGrid {
  
  protected $ciType = 0;
  
  public function setType($piType) {
    $this->ciType = $piType;
  }
  
  public function drawItem(){    
    switch($this->ciColumnIndex){    
      case 1:
        switch($this->ciType) {
          case CONTEXT_AREA: $this->coCellBox->setIconSrc("icon-area_gray.gif"); break;
          case CONTEXT_ASSET: $this->coCellBox->setIconSrc("icon-asset_gray.gif"); break;
          case CONTEXT_CATEGORY: $this->coCellBox->setIconSrc("icon-category.gif"); break;
          case CONTEXT_SECTION_BEST_PRACTICE: $this->coCellBox->setIconSrc("icon-section.gif"); break;
          case CONTEXT_CI_CATEGORY: $this->coCellBox->setIconSrc("icon-ci_category.gif"); break;
          case CONTEXT_CI_INCIDENT: $this->coCellBox->setIconSrc("icon-ci_incident.gif"); break;
          default:break;
        }
        return parent::drawItem();    
        break;
      default:
        return parent::drawItem();
        break;
    }
  }
}

class AssociateElementEvent extends FWDRunnable {
  public function run() {
    $miContextId = FWDWebLib::getObject("current_id")->getValue();
    $miContextType = FWDWebLib::getObject("current_type")->getValue();
    $miNewParentId = FWDWebLib::getObject("new_parent_id")->getValue();
    
    $moContextObject  = new ISMSContextObject();
    $moContext = $moContextObject->getContextObject($miContextType);
    $moContext->setFieldValue($moContext->getDependenceAliasId(),$miNewParentId);
    $moContext->update($miContextId);
    $moContext->restore($miContextId);
    /*
     * Se possuir o m�dulo de documenta��o e o contexto tiver
     * documento, atualiza os leitores desse documento.
     * Tamb�m existe a possibilidade de ser um Risco. Nessa caso,
     * deve-se pegar todos os controles do risco e atualizar seus
     * leitores tamb�m.
     */      
    if (FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(POLICY_MODE)) { 
      if ($moContext->hasDocument()) $moContext->updateReaders($miContextId);
      elseif($moContext->getContextType() == CONTEXT_RISK) {
      	$moRC = new RMRiskControl();
  			$maControls = $moRC->getControlsFromRisk($miContextId);
  			$moControl = new RMControl();
    		foreach ($maControls as $miControlId) $moControl->updateReaders($miControlId);
      }
    }
    
    echo "gebi('has_restored_element').value=1;trigger_event('load_element_event','3');";
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run() {
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    
    $moStartEvent->addAjaxEvent(new LoadElementEvent("load_element_event"));
    $moStartEvent->addAjaxEvent(new AssociateElementEvent("associate_element_event"));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    FWDWebLib::getObject('contexts')->setValue(FWDWebLib::getObject('elems')->getValue());
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        trigger_event('load_element_event','3');
      </script>
    <?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_associate_elements.xml");
?>