<?php
include_once "include.php";
include_once $handlers_ref . "grid/QueryGridAuditLog.php";
include_once $handlers_ref . "select/QuerySelectAuditLogUser.php";
include_once $handlers_ref . "QueryAuditLogAction.php";
include_once $handlers_ref . "QueryGetReportClassification.php";

class FilterAuditLogEvent extends FWDRunnable {
	public function run() {
		$moWebLib = FWDWebLib::getInstance();
		$moGrid = FWDWebLib::getObject('grid_audit_log');
		$moGrid->execEventPopulate();	
	}
}

class GridAuditLog extends FWDDrawGrid {
  public function drawItem(){    
    switch($this->ciColumnIndex){
      case 1:
      	$this->coCellBox->setValue(ISMSLib::getISMSDate($this->coCellBox->getValue()));        
        return $this->coCellBox->draw();
      break;      
      default:
        return parent::drawItem();
      break;
    }
  }
}

class ISMSReportEvent extends FWDRunnable {
	
	private function getFilter() {
		$moReportFilter = new ISMSReportAuditLogFilter();
		
		$moReportFilter->setDateStart(FWDWebLib::getObject("date_start")->getTimestamp());
		$moReportFilter->setDateFinish(FWDWebLib::getObject("date_end")->getTimestamp());
		$moReportFilter->setUser(FWDWebLib::getObject("filter_user")->getValue());
		$moReportFilter->setAction(FWDWebLib::getObject("filter_action")->getValue());		
		
		$moReportFilter->setManualClassification(FWDWebLib::getObject('select_report_classification')->getValue());
	  $moReportFilter->setFileType(FWDWebLib::getObject('select_report_type')->getValue());
    
    FWDWebLib::getObject('report_comment')->getObjFWDString()->setAttrNoEscape('true');
    
	  $moReportFilter->setComment(FWDWebLib::convertToISO(FWDWebLib::getObject('report_comment')->getValue(), true));
	  
	  return $moReportFilter;
	}
	
  public function run(){  	
  	set_time_limit(3000);   

    $mbDontForceDownload = (FWDWebLib::getObject("select_report_type")->getValue()==REPORT_FILETYPE_HTML);

    $moWindowReport = new ISMSReportWindow(ADMIN_MODE);
    if($mbDontForceDownload) $moWindowReport->open();

    $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    $moSession->deleteAttribute("filter");
    $moSession->addAttribute("filter");    
    $moSession->setAttrFilter($this->getFilter());
    
    $msReport = "report_audit_log";
    	
    if($mbDontForceDownload){
      $moWindowReport->setWaitLoadingComplete(true);	      
      $moWindowReport->loadReport($msReport);
    }
    else $moWindowReport->forceReportDownload($msReport);  	  	
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
  	$moWebLib = FWDWebLib::getInstance();
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));   
		$moStartEvent->addAjaxEvent(new FilterAuditLogEvent("filter_audit_log"));
		$moStartEvent->addAjaxEvent(new ISMSReportEvent("function_report"));
		
    $moGrid = FWDWebLib::getObject("grid_audit_log");
    $moGridHandler = new QueryGridAuditLog(FWDWebLib::getConnection());
		$moGridHandler->setDateStart(FWDWebLib::getObject("date_start")->getTimestamp());
		$moGridHandler->setDateEnd(FWDWebLib::getObject("date_end")->getTimestamp());
		$moGridHandler->setAction(FWDWebLib::getObject("filter_action")->getValue());
		$moGridHandler->setUser(FWDWebLib::getObject("filter_user")->getValue());
		$moGrid->setQueryHandler($moGridHandler);
    $moGrid->setObjFwdDrawGrid(new GridAuditLog());       
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
  	$moAuditLog = new ISMSAuditLog();
  	$moSelect = new QueryAuditLogAction(FWDWebLib::getConnection());
  	$moSelectAction = FWDWebLib::getObject('select_filter_action');
    $moSelect->makeQuery();
  	$moSelect->executeQuery();
  	while($moSelect->getDataset()->fetch()) {
  		$moSelectDataset = $moSelect->getDataset();
  		$miSelectActionId = $moSelectDataset->getFieldByAlias('action_id')->getValue();
  		$msSelectActionValue = $moAuditLog->getAction($miSelectActionId);
  		$moItem = new FWDItem();
  		$moItem->setAttrKey($miSelectActionId);
  		$moItem->setValue($msSelectActionValue);
  		$moSelectAction->addObjFWDItem($moItem);
  	}
  	
  	$moHandler = new QuerySelectAuditLogUser(FWDWebLib::getConnection());    
    $moSelect = FWDWebLib::getObject('select_filter_user');
    $moSelect->setQueryHandler($moHandler);
    $moSelect->populate();
    
    /*
     * Popula o select com os tipos de formato de relatórios e
     * o select com os tipos de classificação de relatórios.
     */
    $moFormatSelect = FWDWebLib::getObject('select_report_type');
    $maAvailableFormats = FWDReportGenerator::getAvailableFormats(ISMSLib::getSupportedReportFormats());
    foreach ($maAvailableFormats as $miKey => $msValue) {
    	$moFormatSelect->setItemValue($miKey, $msValue);    	
    }
    $moHandler = new QueryGetReportClassification(FWDWebLib::getConnection());
    $moHandler->makeQuery();
    $maAvailableClassifications = $moHandler->executeQuery();
    $moClassificationSelect = FWDWebLib::getObject('select_report_classification');
    foreach ($maAvailableClassifications as $miKey => $msValue) {
      $moClassificationSelect->setItemValue($msValue, $msValue);
    }

    //instala a segurança de acls do sistema nos elementos da tela
    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
    
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));	           
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_audit_log.xml");
?>