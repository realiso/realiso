<?php
include_once 'include.php';
include_once $classes_isms_ref . "nonauto/admin/ISMSDefaultConfig.php";

class SavePasswdPolicyEvent extends FWDRunnable {
  public function run() {    
    $moPasswordController = FWDWebLib::getObject("password_controller");
    $maSelectedItens = $moPasswordController->getAllItemsCheck();
    
    $miMin = FWDWebLib::getObject('char_num')->getValue();
    $miTotal = 0;
    foreach ($maSelectedItens as $moItem) {
      if ($moItem->getAttrCheck()) {
        switch($moItem->getAttrKey()) {
          case 'password1':
            $miValue = FWDWebLib::getObject('special_chars_num')->getValue();
            $miTotal += (is_numeric($miValue) ? $miValue : 0);
          break;
          case 'password2':
            $miTotal += 2;
          break;
          case 'password3':
            $miValue = FWDWebLib::getObject('numeric_chars_num')->getValue();
            $miTotal += (is_numeric($miValue) ? $miValue : 0);
          break;  
        } 
      }
    }
    
    if ($miTotal <= $miMin) {    
      $maPasswordCheckboxFields = array("password2"=>PASSWORD_CASE_CHARS);
      $maPasswordTextboxFields = 
        array(
          "char_num"=>PASSWORD_CHAR_NUM,
          "block"=>PASSWORD_BLOCK_TRIES,
          "frequency"=>PASSWORD_CHANGE_FREQUENCY,
          "history"=>PASSWORD_HISTORY_NUM,
          "days_before_warning"=>PASSWORD_DAYS_BEFORE_WARNING,
        );
      $maPasswordCheckTextBoxFields =
        array(
          "password1" => array("special_chars_num"=>PASSWORD_SPECIAL_CHARS),
          "password3" => array("numeric_chars_num"=>PASSWORD_NUMERIC_CHARS)
        );      
      $maPasswordAssociation = FWDWebLib::unserializeString(FWDWebLib::getObject("password_association")->getValue());
      $maPasswordAssociationNew = array(); 
      //Atualiza os campos texto
      foreach ($maPasswordTextboxFields as $msField=>$msConfig) {
        ISMSLib::setConfigById($msConfig,FWDWebLib::getObject($msField)->getValue());
      }
      //Atualiza os itens marcados
      foreach ($maSelectedItens as $moItem) { 
        $msKey = $moItem->getAttrKey();
        $maPasswordAssociationNew[$msKey] = '1';
        unset($maPasswordAssociation[$msKey]);
        if (isset($maPasswordCheckboxFields[$msKey])) {
          ISMSLib::setConfigById($maPasswordCheckboxFields[$msKey],"1");
        }
        elseif (isset($maPasswordCheckTextBoxFields[$msKey])) {
          $maField = $maPasswordCheckTextBoxFields[$msKey];
          foreach($maField as $msField=>$msConfig){
            ISMSLib::setConfigById($msConfig,FWDWebLib::getObject($msField)->getValue());
          }
        }
      }
      // Deleta os itens desmarcados
      $msCleanFields = "";
      if (count($maPasswordAssociation) > 0){
        foreach ($maPasswordAssociation as $msKey => $msValue) {
          if (isset($maPasswordCheckboxFields[$msKey])) {
            ISMSLib::setConfigById($maPasswordCheckboxFields[$msKey],"");
          }
          elseif (isset($maPasswordCheckTextBoxFields[$msKey])) {
            $maField = $maPasswordCheckTextBoxFields[$msKey];
            foreach ($maField as $msField=>$msConfig) {
              ISMSLib::setConfigById($msConfig,"");
            $moJs = new FWDJsEvent(JS_SET_CONTENT,$msField,"");
            $msCleanFields .= $moJs->render();
            }
          }
        }
      }
      $msPasswordAssociation = serialize($maPasswordAssociationNew);
      $msPasswordAssociation = str_replace('\\','\\\\"',$msPasswordAssociation);
      $msPasswordAssociation = str_replace('"','\"',$msPasswordAssociation);
      echo $msCleanFields;
      echo "gebi('password_association').value = '$msPasswordAssociation';";
      echo "gobi('save_warning').show(); setTimeout('gobi(\\'save_warning\\').hide()',5000);";    
    }
    else {
      echo "gobi('min_chars_warning').show();"; 
    }
  }
}

class SaveDeleteCascade extends FWDRunnable{
  public function run(){
    $msConfig = trim(str_replace(':','',FWDWebLib::getObject("delete_cascade_controller")->getValue()));
    if($msConfig && $msConfig == 'delete_cascade'){
      ISMSLib::setConfigById(GENERAL_CASCADE_ON,'1');
    }else{
      ISMSLib::setConfigById(GENERAL_CASCADE_ON,'0');
    }
    echo "gobi('save_warning').show(); setTimeout('gobi(\\'save_warning\\').hide()',5000);";
  }
}

class SaveTimezoneSetup extends FWDRunnable{
	public function run(){
		$moConfig = new ISMSConfig();
		$msConfig = FWDWebLib::getPOST('timezone_setup');
		if ($msConfig){
			$moConfig->setConfig(TIMEZONE, $msConfig);
		}
		echo "gobi('save_warning').show(); setTimeout('gobi(\\'save_warning\\').hide()',5000);";
	}
}

class SaveEmailConfig extends FWDRunnable{
	public function run(){
		$moConfig = new ISMSConfig();
		$msUser = FWDWebLib::getPOST('user_setup');
		$msPass = FWDWebLib::getPOST('pass_setup');
		$msHost = FWDWebLib::getPOST('host_setup');
		$msPort = (int) FWDWebLib::getPOST('port_setup');
		$msEnc = FWDWebLib::getPOST('encryption_setup');
		if ($msUser) $moConfig->setConfig(EXTERNAL_MAIL_USER, $msUser);
		if ($msPass) $moConfig->setConfig(EXTERNAL_MAIL_PASSWORD, $msPass);
		if ($msHost) $moConfig->setConfig(GENERAL_SMTP_SERVER, $msHost);
		if ($msPort) $moConfig->setConfig(EXTERNAL_MAIL_PORT, $msPort);
		if ($msEnc) $moConfig->setConfig(EXTERNAL_MAIL_ENCRYPTION, $msEnc);
		echo "gobi('save_warning').show(); setTimeout('gobi(\\'save_warning\\').hide()',5000);";
	}
}

class SaveServerSetup extends FWDRunnable{
	public function run(){
		$moConfig = new ISMSConfig();
		$msIp = FWDWebLib::getPOST('ip_setup');
		$msDNS = FWDWebLib::getPOST('DNS_setup');
		$msGateway = FWDWebLib::getPOST('gateway_setup');
		if ($msIp) $moConfig->setConfig(EXTERNAL_SERVER_IP, $msIp);
		if ($msDNS) $moConfig->setConfig(EXTERNAL_SERVER_DNS, $msDNS);
		if ($msGateway) $moConfig->setConfig(EXTERNAL_SERVER_GATEWAY, $msGateway);
		if (FWDWebLib::getPOST('update_file')){
			echo "js_submit('upload','ajax'); setTimeout('trigger_event(\"save_complete_event\",3)',200);";
		} else {
			echo "gobi('save_warning').show(); setTimeout('gobi(\\'save_warning\\').hide()',5000);";
		}		
	}
}

class SaveUpdateFileEvent extends FWDRunnable{
	public function run(){
		// upload de arquivo de configura��o
		$moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
		$moFile = FWDWebLib::getObject('update_file');
		$mbUploadedFile = $moFile->isUploaded();
		$miFileError = 0;
		if($mbUploadedFile){
			$miFileError = $moFile->getErrorCode();
			if($miFileError==FWDFile::E_NONE){
				$msDirectory =  "c:\\tmp\\";
				if(is_writable($msDirectory)){
					$msFileName=$moFile->getFileName();
					$msPath = "{$msDirectory}{$msFileName}";
					$miFileSize = filesize($moFile->getTempFileName());
					$moWriteFP = fopen($msPath,'wb');
					$moFP = fopen($moFile->getTempFileName(),'rb');
					while(!feof($moFP)){
						fwrite($moWriteFP,fread($moFP,16384));
					}
					fclose($moFP);
					fclose($moWriteFP);
				} else{
					//O diret�rio n�o p�de ser escrito!
					trigger_error("Directory '{$msDirectory}' is not writable.",E_USER_ERROR);
				}
			} else{
				// Erro no upload -> armazena c�digo do erro na sess�o
				trigger_error("Upload error: $miFileError", E_USER_ERROR);
				if(!$moSession->attributeExists('uploadErrorCode')){
					$moSession->addAttribute('uploadErrorCode');
				}
				$moSession->setAttrUploadErrorCode($miFileError);
			}
		}
		if(!$moSession->attributeExists("policySaveComplete")){
			$moSession->addAttribute("policySaveComplete");
		}
		$moSession->setAttrPolicySaveComplete(true);
	}
}

class SaveCompleteEvent extends FWDRunnable{
	public function run(){
		$moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
		if($moSession->attributeExists("policySaveComplete")){
			if($moSession->getAttrPolicySaveComplete()){
				$moSession->deleteAttribute("policySaveComplete");

				if($moSession->attributeExists('uploadErrorCode') && $moSession->getAttrUploadErrorCode()){
					// Trata erro no upload
					$miErrorCode = $moSession->getAttrUploadErrorCode();
					$moSession->deleteAttribute('uploadErrorCode');
					echo "gobi('warning_upload_error').show();";
					echo "soTabManager.unlockTabs();";
				}else{
					// Upload feito com sucesso
					echo "gobi('save_warning').show();
						gebi('update_file').value = ''; 
						setTimeout('gobi(\\'save_warning\\').hide()',5000);";
				}
			}
		} else {
			echo "setTimeout('trigger_event(\"save_complete_event\",3)',200);";
		}
	}
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
    $moStartEvent->addAjaxEvent(new SavePasswdPolicyEvent('save_password_policy_event'));
    $moStartEvent->addAjaxEvent(new SaveDeleteCascade('save_system_configuration_delete_cascade_event'));
    $moStartEvent->addAjaxEvent(new SaveTimezoneSetup('save_system_configuration_timezone_setup_event'));
    if (ISMSLib::getConfigById(EXTERNAL_SERVER)) {
    	$moStartEvent->addAjaxEvent(new SaveEmailConfig('save_system_configuration_email_setup_event'));
    	$moStartEvent->addAjaxEvent(new SaveServerSetup('save_system_configuration_server_setup_event'));
    	$moStartEvent->addAjaxEvent(new SaveCompleteEvent('save_complete_event'));
    	$moStartEvent->addSubmitEvent(new SaveUpdateFileEvent('upload'));
    }
    $moWebLib = FWDWebLib::getInstance();
    $moDialog = FWDWebLib::getObject("dialog");

    $moWebLib->xml_load($moWebLib->getSysRef().'/packages/admin/system_configuration_password_policy.xml',false);
    $moDialog->addObjFWDView(FWDWebLib::getObject("system_configuration_password_policy"));

    $moWebLib->xml_load($moWebLib->getSysRef().'/packages/admin/system_configuration_delete_cascade.xml',false);
    $moDialog->addObjFWDView(FWDWebLib::getObject("system_configuration_delete_cascade"));
    
    $moWebLib->xml_load($moWebLib->getSysRef().'/packages/admin/system_configuration_timezone_setup.xml',false);
    $moDialog->addObjFWDView(FWDWebLib::getObject("system_configuration_timezone_setup"));

    if (ISMSLib::getConfigById(EXTERNAL_SERVER)) {
    	$moWebLib->xml_load($moWebLib->getSysRef().'/packages/admin/system_configuration_email_setup.xml',false);
    	$moDialog->addObjFWDView(FWDWebLib::getObject("system_configuration_email_setup"));
    	
    	$moWebLib->xml_load($moWebLib->getSysRef().'/packages/admin/system_configuration_server_setup.xml',false);
    	$moDialog->addObjFWDView(FWDWebLib::getObject("system_configuration_server_setup"));
    }
    
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){

  /********** Procedimentos relacionados �s configura��es de Pol�tica de Senhas **********/
  $maPasswordCheckboxFields = array("password2" => PASSWORD_CASE_CHARS);
  $maPasswordTextboxFields = 
    array(
      "char_num"=>PASSWORD_CHAR_NUM,
      "block"=>PASSWORD_BLOCK_TRIES,
      "frequency"=>PASSWORD_CHANGE_FREQUENCY,
      "history"=>PASSWORD_HISTORY_NUM,
      "days_before_warning"=>PASSWORD_DAYS_BEFORE_WARNING,
    );
  $maPasswordCheckTextBoxFields =
    array(
      "password1" => array("special_chars_num"=>PASSWORD_SPECIAL_CHARS),
      "password3" => array("numeric_chars_num"=>PASSWORD_NUMERIC_CHARS)
    );
  $moPasswordController = FWDWebLib::getObject("password_controller");
  $maPasswordAssociation = array();
  foreach($maPasswordCheckboxFields as $msField=>$msConfig) {
    $msConfigValue = ISMSLib::getConfigById($msConfig);
    if (trim($msConfigValue)) {
      $moPasswordController->checkItem($msField);
      $maPasswordAssociation[$msField]='1';
    }
  }
  foreach($maPasswordTextboxFields as $msField=>$msConfig) {
    FWDWebLib::getObject($msField)->setValue(ISMSLib::getConfigById($msConfig));
  }
  foreach($maPasswordCheckTextBoxFields as $msCheckboxField=>$maTextboxFields) {
    foreach($maTextboxFields as $msField=>$msConfig) {
      $msConfigValue = ISMSLib::getConfigById($msConfig);
      if (trim($msConfigValue)) {
        FWDWebLib::getObject($msField)->setValue($msConfigValue);
        $moPasswordController->checkItem($msCheckboxField);
        $maPasswordAssociation[$msCheckboxField]='1';
      }
      else {
        FWDWebLib::getObject($msField)->setAttrDisabled("true");
      }
    }
  }
  FWDWebLib::getObject("password_association")->setValue(serialize($maPasswordAssociation));  
  
    /********** Procedimentos relacionados ao fuso horario ************/
 
  $moConfig = new ISMSConfig();
  $msTimezone = $moConfig->getConfig(TIMEZONE);
  FWDWebLib::getObject("timezone_setup")->setValue($msTimezone);
  
  /********** Procedimentos relacionados ao email externo e servidor ************/
  if (ISMSLib::getConfigById(EXTERNAL_SERVER)) {
  	$moConfig = new ISMSConfig();
  	$msIp = $moConfig->getConfig(EXTERNAL_SERVER_IP);
  	FWDWebLib::getObject("ip_setup")->setValue($msIp);
  	$msDNS = $moConfig->getConfig(EXTERNAL_SERVER_DNS);
  	FWDWebLib::getObject("DNS_setup")->setValue($msDNS);
  	$msGT = $moConfig->getConfig(EXTERNAL_SERVER_GATEWAY);
  	FWDWebLib::getObject("gateway_setup")->setValue($msGT);
  	
  	$msHost = $moConfig->getConfig(GENERAL_SMTP_SERVER);
  	FWDWebLib::getObject("host_setup")->setValue($msHost);
  	$msPort = $moConfig->getConfig(EXTERNAL_MAIL_PORT);
  	FWDWebLib::getObject("port_setup")->setValue($msPort);
  	$msUsr = $moConfig->getConfig(EXTERNAL_MAIL_USER);
  	FWDWebLib::getObject("user_setup")->setValue($msUsr);
  	$msPass = $moConfig->getConfig(EXTERNAL_MAIL_PASSWORD);
  	FWDWebLib::getObject("pass_setup")->setValue($msPass);
  	$msEnc = $moConfig->getConfig(EXTERNAL_MAIL_ENCRYPTION);
  	FWDWebLib::getObject("encryption_setup")->setValue($msEnc);
  }
  
  /********** Procedimentos relacionados ao delete cascade **********/
  if(ISMSLib::getConfigById(GENERAL_CASCADE_ON)){
    FWDWebLib::getObject("delete_cascade_controller")->checkItem('delete_cascade');
  }
  
    //instala a seguran�a de ACL na p�gina
  FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());

  // exibe a primeira tela de configura��o que o usu�rio tem permiss�o para ver
  if (FWDWebLib::getObject("system_configuration_password_policy")->getShouldDraw())
    FWDWebLib::getObject("system_configuration_password_policy")->setAttrDisplay("true");
  elseif (FWDWebLib::getObject("system_configuration_delete_cascade")->getShouldDraw())
    FWDWebLib::getObject("system_configuration_delete_cascade")->setAttrDisplay("true");

    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
      function change_system_configuration () {
        //alert(gebi('var_selected_panel').value);
        if(gebi('var_selected_panel').value && gebi('var_selected_panel').value!=undefined){
          if(gebi(gebi('var_selected_panel').value)){
            js_hide(gebi('var_selected_panel').value);
          }
        }
        gebi('var_selected_panel').value = gebi('system_configuration_select').value;
        
        if (gebi(gebi('var_selected_panel').value)){
          js_show(gebi('var_selected_panel').value);
        }
      }
      <? 
        $moDefaultConfig = new ISMSDefaultConfig();
        echo $moDefaultConfig->getPasswordPolicyCode();
      ?>

      </script>
    <?   
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
if (ISMSLib::getConfigById(EXTERNAL_SERVER)){
	FWDWebLib::getInstance()->xml_load('nav_system_configuration_on_site.xml');
} else {
	FWDWebLib::getInstance()->xml_load('nav_system_configuration.xml');
}
?>