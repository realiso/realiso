<?php
include_once 'include.php';

include_once $handlers_ref . "QueryParameterNames.php";
include_once $handlers_ref . "QueryAllParameterValuesName.php";
include_once $handlers_ref . "QueryRCParameterValueNames.php";
include_once $handlers_ref . "QuerySpecialUsers.php";
include_once $handlers_ref . "QueryRiskUpdate.php";
include_once $handlers_ref . "QueryParameterValueCountUpdate.php";
include_once $handlers_ref . "QueryRiskProbabilityUpdate.php";
include_once $handlers_ref . "QueryContextClassification.php";
include_once $handlers_ref . "QueryControlCostNames.php";
include_once $handlers_ref . "QueryGetAllRiskControlIds.php";
include_once $handlers_ref . "QueryUpdateAllValues.php";
include_once $handlers_ref . "QueryImpactsIntervalInUse.php";
include_once $classes_isms_ref . "nonauto/admin/ISMSDefaultConfig.php";

include_once $handlers_ref . "QueryLinkAssetsToProcesses.php";

class SaveCurrencyEvent extends FWDRunnable {
	public function run() {
		$msConfig = FWDWebLib::getPOST('currency');

		if(!ISMSLib::getConfigById(CURRENCY_SET)){
			if($msConfig){
				ISMSLib::setConfigById(GENERAL_CURRENCY_IDENTIFIER, $msConfig);
				ISMSLib::setConfigById(CURRENCY_SET, 1);
			}
		}

		echo "gebi('currency').disabled = true;";
		echo "gobi('save_btn').disable();";

		echo "gobi('save_warning').show(); setTimeout('gobi(\\'save_warning\\').hide()',5000);";
	}
}

class SaveClassificationEvent extends FWDRunnable {
	public function run() {
		$mbError = false;
		$maSelectedItems = FWDWebLib::getObject("classification_controller")->getAllItemsCheck();
		$miTotalAreaTypes = $miTotalAreaPrios = $miTotalProcessTypes = $miTotalProcessPrios = $miTotalRiskTypes = $miTotalEventTypes = $miTotalControlTypes = $miTotalDocumentTypes = $miTotalRegisterTypes = 0;
		foreach ($maSelectedItems as $moItem) {
			switch(substr($moItem->getAttrKey(),0,12)) {
				case "cl_type_area" : $miTotalAreaTypes++; break;
				case "cl_prio_area" : $miTotalAreaPrios++; break;
				case "cl_type_proc" : $miTotalProcessTypes++; break;
				case "cl_prio_proc" : $miTotalProcessPrios++; break;
				case "cl_type_risk" : $miTotalRiskTypes++; break;
				case "cl_type_even" : $miTotalEventTypes++; break;
				case "cl_type_cont" : $miTotalControlTypes++; break;
				case "cl_type_docu" : $miTotalDocumentTypes++; break;
				case "cl_type_regi" : $miTotalRegisterTypes++; break;
			}
		}

		if ( (($miTotalAreaTypes<3)||($miTotalAreaPrios<3) ) && (FWDWebLib::getObject("cl_area")->getShouldDraw()) ) {
			echo "gebi('classification_select').selectedIndex = 0;"
			."hack_change_classification ('cl_area');";
			$mbError = true;
		}
		elseif ( (($miTotalProcessTypes<3)||($miTotalProcessPrios<3) ) && (FWDWebLib::getObject("cl_process")->getShouldDraw()) ) {
			echo "gebi('classification_select').selectedIndex = 1;"
			."hack_change_classification ('cl_process');";
			$mbError = true;
		}
		elseif ( ($miTotalRiskTypes<3) && (FWDWebLib::getObject("cl_risk")->getShouldDraw()) ) {
			echo "gebi('classification_select').selectedIndex = 2;"
			."hack_change_classification ('cl_risk');";
			$mbError = true;
		}
		elseif ( ($miTotalEventTypes<3) && (FWDWebLib::getObject("cl_event")->getShouldDraw()) ) {
			echo "gebi('classification_select').selectedIndex = 3;"
			."hack_change_classification ('cl_event');";
			$mbError = true;
		}
		elseif ( ($miTotalControlTypes<3) && (FWDWebLib::getObject("cl_control")->getShouldDraw()) ) {
			echo "gebi('classification_select').selectedIndex = 4;"
			."hack_change_classification ('cl_control');";
			$mbError = true;
		}
		elseif ( ($miTotalDocumentTypes<3) && (FWDWebLib::getObject("cl_document")->getShouldDraw()) ) {
			echo "gebi('classification_select').selectedIndex = 5;"
			."hack_change_classification ('cl_document');";
			$mbError = true;
		}
		elseif ( ($miTotalRegisterTypes<3) && (FWDWebLib::getObject("cl_register")->getShouldDraw()) ) {
			echo "gebi('classification_select').selectedIndex = 6;"
			."hack_change_classification ('cl_register');";
			$mbError = true;
		}
		if ($mbError) {
			echo "gobi('classification_error').show();";
			return;
		}

		$moQueryContextClassification = new QueryContextClassification(FWDWebLib::getConnection());
		$moQueryContextClassification->makeQuery();
		$moQueryContextClassification->executeQuery();
		$maClassificationNames = $moQueryContextClassification->getClassificationNames();
		$maClassificationIds = $moQueryContextClassification->getClassificationIds();

		$maNewClassifications = array();
		$maSelectedItems = FWDWebLib::getObject("classification_controller")->getAllItemsCheck();
		foreach ($maSelectedItems as $moItem) {
			$msKey = $moItem->getAttrKey();
			$msName = trim(FWDWebLib::getObject($msKey)->getValue());
			if ($msName) {
				$maNewClassifications[$msKey] = $msName;
				$moISMSContextClassification = new ISMSContextClassification();
				$moISMSContextClassification->setFieldValue('classif_name',$msName);

				// atualiza classifica��o
				if (isset($maClassificationIds[$msKey])) {
					$moISMSContextClassification->update($maClassificationIds[$msKey]);
				}
				// insere classifica��o
				else {
					$miClassifContextType =
					strstr($msKey,"area") ? CONTEXT_AREA :
					(strstr($msKey,"process") ? CONTEXT_PROCESS :
					(strstr($msKey,"risk") ? CONTEXT_RISK :
					(strstr($msKey,"event") ? CONTEXT_EVENT :
					(strstr($msKey,"control") ? CONTEXT_CONTROL :
					(strstr($msKey,"document") ? CONTEXT_DOCUMENT :
					(strstr($msKey,"register") ? CONTEXT_REGISTER : -1))))));

					$miClassifType =
					strstr($msKey,"type") ? CONTEXT_CLASSIFICATION_TYPE :
					(strstr($msKey,"prio") ? CONTEXT_CLASSIFICATION_PRIORITY : -1);

					if ($miClassifContextType<0 || $miClassifType<0)
					trigger_error("Invalid Classification Field: $msKey",E_USER_ERROR);

					$moISMSContextClassification->setFieldValue('classif_context_type',$miClassifContextType);
					$moISMSContextClassification->setFieldValue('classif_type',$miClassifType);
					$moISMSContextClassification->insert();
				}
				unset($moISMSContextClassification);
			}
		}
		/* php 5.0.x nao possui a fun��o array_diff_key */
		// deleta classifica��es
		$maDeleteIdsByFieldName = FWDWebLib::fwd_array_diff_key($maClassificationIds,$maNewClassifications);
		$msCleanFields = "";
		if (count($maDeleteIdsByFieldName)>0) {
			$moISMSContextClassification = new ISMSContextClassification();
			$maNotDeletable = array();
			foreach ($maDeleteIdsByFieldName as $msFieldName => $miClassificationId) {
				if (strstr($msFieldName,"area")) {
					$moContextType = new RMArea();
					$msPrefix = "area_";
				} elseif (strstr($msFieldName,"process")) {
					$moContextType = new RMProcess();
					$msPrefix = "process_";
				} elseif (strstr($msFieldName,"risk")) {
					$moContextType = new RMRisk();
					$msPrefix = "risk_";
				} elseif (strstr($msFieldName,"event")) {
					$moContextType = new RMEvent();
					$msPrefix = "event_";
				} elseif (strstr($msFieldName,"control")) {
					$moContextType = new RMControl();
					$msPrefix = "control_";
				} elseif (strstr($msFieldName,"document")) {
					$moContextType = new PMDocument();
					$msPrefix = "document_";
				} elseif (strstr($msFieldName,"register")) {
					$moContextType = new PMRegister();
					$msPrefix = "register_";
				}
				$msField =
				strstr($msFieldName,"type") ? $msPrefix."type" :
				(strstr($msFieldName,"prio") ? $msPrefix."priority" : "");

				//<POG>
				// Os alias dos campos de documentos e registros n�o seguem o padr�o, at� porque tipo de documento � outra coisa
				if($msField=='document_type'){
					$msField = 'document_classification';
				}elseif($msField=='register_type'){
					$msField = 'register_classification';
				}
				//</POG>

				$moContextType->createFilter($miClassificationId,$msField);
				$moContextType->select();
				if ($moContextType->fetch()) {
					$maNotDeletable[] = $maClassificationNames[$msFieldName];
					continue;
				}
				$moISMSContextClassification->delete($miClassificationId);
				$moJs = new FWDJsEvent(JS_SET_CONTENT,$msFieldName,"");
				$msCleanFields .= $moJs->render();
			}
			if (count($maNotDeletable) > 0) {
				$msTitle = FWDLanguage::getPHPStringValue('tt_remove_classification_error','Erro ao remover Classifica��o');
				$msMessage = FWDLanguage::getPHPStringValue('st_remove_classification_error',"Suas altera��es foram salvas. No entanto, n�o foi poss�vel remover as seguintes classifica��es, pois est�o sendo usadas por um ou mais elementos: <b>%not_deletable%</b>.");
				$msMessage = str_replace("%not_deletable%",implode(", ",$maNotDeletable),$msMessage);
				ISMSLib::openOk($msTitle,$msMessage,"",60);
			}
		}
		echo $msCleanFields;
		echo "gobi('save_warning').show();"
		."setTimeout('gobi(\\'save_warning\\').hide();',5000);";
	}
}

Class OpenRiskParametersNamesConfirm extends FWDRunnable {
	public function run() {
		$maPNameAssociation = FWDWebLib::unserializeString(FWDWebLib::getObject("risk_par_association")->getValue());
		$maSelectedItems = FWDWebLib::getObject("risk_par_controller")->getAllItemsCheck();
		if (count($maSelectedItems)<1) {
			echo "gobi('risk_parameters_names_error').show();";
			return;
		}
		if (count($maPNameAssociation) != count($maSelectedItems)) {
			$msTitle = FWDLanguage::getPHPStringValue('tt_save_parameters_names','Salvar Nomes de Par�metros');
			$msMessage = FWDLanguage::getPHPStringValue('st_change_amount_of_parameters_confirm',"Alterar a quantidade de par�metros resultar� na desparametriza��o do sistema.<BR><b>Voc� tem certeza que deseja fazer isso?</b>");
			$msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm').getOpener().save_pname();";
			ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,50);
		}else {
			echo " save_pname(); ";
		}
	}
}

class SaveRiskParametersNamesEvent extends FWDRunnable {
	public function run() {
		$maPNameAssociation = FWDWebLib::unserializeString(FWDWebLib::getObject("risk_par_association")->getValue());
		$maPNameAssociationNew = array();
		$moControllerPName = FWDWebLib::getObject("risk_par_controller");
		$maSelectedItems = $moControllerPName->getAllItemsCheck();
		$maNewRiskParItens = array();

		foreach ($maSelectedItems as $moItem) {
			$msKey = $moItem->getAttrKey();
			$msPNameName = trim(FWDWebLib::getObject($msKey)->getValue());
			if ($msPNameName) {
				$moPName = new RMParameterName();
				$moPName->setFieldValue('parametername_name', $msPNameName);
				if (isset($maPNameAssociation[$msKey])) {
					$moPName->update($maPNameAssociation[$msKey]);
					$maPNameAssociationNew[$msKey] = $maPNameAssociation[$msKey];
					unset($maPNameAssociation[$msKey]);
				}else {
					$miPNameID = $moPName->insert(true);
					$maPNameAssociationNew[$msKey] = $miPNameID;
					$maNewRiskParItens[] = $miPNameID;
				}
			}
		}
		// Deleta os parametros deselecionados
		$msCleanFields = "";
		if (count($maPNameAssociation) != 0) {
			foreach ($maPNameAssociation as $msMey => $miId) {
				$moPName = new RMParameterName();
				$moPName->delete($miId);
				$moJs = new FWDJsEvent(JS_SET_CONTENT,$msMey,"");
				$msCleanFields .= $moJs->render();
			}
		}

		//cria os novos parametros para a rela��o de risco x controle com o valor 'Do Not Affect'
		if(count($maNewRiskParItens)){
			$moRCQuery = new QueryGetAllRiskControlIds(FWDWebLib::getConnection());
			$maValues = $moRCQuery->getValue();
			$moRiskControl = new RMRiskControl();
			$moRiskControl->select();
			foreach($maValues as $miRCId){
				foreach($maNewRiskParItens as $miValue){
					$moRCValue = new RMRiskControlValue();
					$moRCValue->setFieldValue('rcvalue_parameter_name_id',$miValue);
					$moRCValue->setFieldValue('rcvalue_value_name_id', 1);
					$moRCValue->setFieldValue('rcvalue_rc_id', $miRCId);
					$moRCValue->insert();
				}
			}
		}

		//Atualiza os valores de risco
		$moQueryRiskUpdate = new QueryRiskUpdate(FWDWebLib::getConnection());
		$moQueryRiskUpdate->makeQuery();
		$moQueryRiskUpdate->executeQuery();

		$msPNameAssociation = serialize($maPNameAssociationNew);
		$msPNameAssociation = str_replace('\\','\\\\"',$msPNameAssociation);
		$msPNameAssociation = str_replace('"','\"',$msPNameAssociation);
		echo $msCleanFields;
		echo "gebi('risk_par_association').value = '$msPNameAssociation';";
		echo "gobi('save_warning').show();"
		."setTimeout('gobi(\\'save_warning\\').hide()',5000);";
	}
}

class OpenNumberOfRiskParametersConfirm extends FWDRunnable {
	public function run() {
		$miPreviousRiskValueCount = ISMSLib::getConfigById(RISK_VALUE_COUNT);
		$miRiskValueCount = ltrim(FWDWebLib::getObject("risk_value_count")->getValue(),":");
		if ($miPreviousRiskValueCount != $miRiskValueCount) {
			$msTitle = FWDLanguage::getPHPStringValue('tt_save_parameters_values','Salvar Valores de Par�metros');
			$msMessage = FWDLanguage::getPHPStringValue('st_change_amount_of_risks_confirm',"Alterar a quantidade de riscos do sistema resultar� na necessidade de refazer todos os c�lculos de risco no sistema!<BR><b>Voc� tem certeza que deseja fazer isso?</b>");

			$msEventValue = " soPopUpManager.getPopUpById('popup_confirm').getOpener().save_number_of_risk_parameters_event();";
			ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,75);
		}
		else {
			echo " save_number_of_risk_parameters_event(); ";
		}
	}
}

class SaveNumberOfRiskParameters extends FWDRunnable {
	public function run() {
		set_time_limit(3000);
		$miRiskCount = ltrim(FWDWebLib::getObject("risk_value_count")->getValue(),":");
		$miPreviousRiskValueCount = ISMSLib::getConfigById(RISK_VALUE_COUNT);
		$maRiskControlIds = FWDWebLib::unserializeString(FWDWebLib::getObject('var_risk_control_ids')->getValue());
		$maAssetRiskIds = FWDWebLib::unserializeString(FWDWebLib::getObject('var_asset_risk_ids')->getValue());
		$maAssetRiskIdsNew = array();
		$maRiskControlIdsNew = array();
		$miOptimist = ltrim(FWDWebLib::getObject("optimist_choice")->getValue(),":");
		$mbOptimist = $miOptimist==1?true:false;
		$msEcho = "";
		for ($miI=1; $miI<=$miRiskCount; $miI++){
			//Salva a tela Ativo e Risco
			$moPValue = new RMParameterValueName();
			$moPValue->setFieldValue('parametervalue_value', $miI);
			$moPValue->setFieldValue('parametervalue_importance', FWDWebLib::getObject("importance".$miRiskCount."_".$miI)->getValue());
			$moPValue->setFieldValue('parametervalue_impact', FWDWebLib::getObject("impact".$miRiskCount."_".$miI)->getValue());
			$moPValue->setFieldValue('parametervalue_rprob', FWDWebLib::getObject("riskprob".$miRiskCount."_".$miI)->getValue());
			if(isset($maAssetRiskIds[$miI])){
				$moPValue->update($maAssetRiskIds[$miI]);
				$maAssetRiskIdsNew[$miI] = $maAssetRiskIds[$miI];
			}else{
				$miNewId = $moPValue->insert(true);
				$maAssetRiskIdsNew[$miI] = $miNewId;
			}
			//Salva a tela Risco x Controle
			$moRCPValue = new RMRCParameterValueName();
			$moRCPValue->setFieldValue('rcparametervalue_value', ($miI-1));
			$moRCPValue->setFieldValue('rcparametervalue_impact', FWDWebLib::getObject("rc_impact".$miRiskCount."_".($miI-1))->getValue());
			$moRCPValue->setFieldValue('rcparametervalue_prob',FWDWebLib::getObject("rc_prob".$miRiskCount."_".($miI-1))->getValue());
			if(isset($maRiskControlIds[$miI-1])){
				$moRCPValue->update($maRiskControlIds[$miI-1]);
				$maRiskControlIdsNew[$miI-1] = $maRiskControlIds[$miI-1];
			}else{
				$miNewId = $moRCPValue->insert(true);
				$maRiskControlIdsNew[$miI-1] = $miNewId;
			}
		}

		//Atualiza os valores de risco, caso o numero de parametros seja diferente.
		if ($miRiskCount != $miPreviousRiskValueCount) {
			$moQueryParameterValueCountUpdate = new QueryParameterValueCountUpdate(FWDWebLib::getConnection());
			$moQueryParameterValueCountUpdate->setValueCount($miRiskCount);
			$moQueryParameterValueCountUpdate->setOptimist($mbOptimist);
			$moQueryParameterValueCountUpdate->makeQuery();
			$moQueryParameterValueCountUpdate->executeQuery(0,0,true);
			//atualiza o ISMSConfig
			$miRiskLow = ISMSLib::getConfigById(RISK_LOW);
			$miRiskHigh = ISMSLib::getConfigById(RISK_HIGH);

			$miPreviousPow = $miPreviousRiskValueCount * $miPreviousRiskValueCount;
			$miAtualPow = $miRiskCount * $miRiskCount;
			$miNewRiskHigh = min(number_format( ( $miRiskHigh * $miAtualPow ) / $miPreviousPow,0), $miAtualPow );
			$miNewRiskLow = min(number_format( ( $miRiskLow * $miAtualPow ) / $miPreviousPow,0), $miAtualPow );
			//garante que o valor minimo dos limites de risco seja 1
			if($miNewRiskHigh <1){$miNewRiskHigh =1;}
			if($miNewRiskLow <1){$miNewRiskLow =1 ;}

			$moRiskLimits = new RMRiskLimits();
			$moRiskLimits->setFieldValue('risk_limits_low' ,$miNewRiskLow);
			$moRiskLimits->setFieldValue('risk_limits_high',$miNewRiskHigh);
			$moRiskLimits->insert();

			ISMSLib::setConfigById(RISK_LOW, $miNewRiskLow);
			ISMSLib::setConfigById(RISK_HIGH,$miNewRiskHigh);
			if($moRiskLimits->getApprover() == ISMSLib::getCurrentUserId()){
				$msEcho .="gebi('risk_limits_low').value = $miNewRiskLow;";
				$msEcho .="gebi('risk_limits_high').value = $miNewRiskHigh;";
			}else{
				$moUser = new ISMSUser();
				$moUser->fetchById($moRiskLimits->getApprover());
				$msWarning = FWDWebLib::getObject('wn_user_is_not_chairman')->getValue();
				$msWarning = str_replace("%chairman_name%", $moUser->getName(), $msWarning);
				$msEcho .= "gobi('wn_user_is_not_chairman').setValue('$msWarning');";
				$msEcho .= "gobi('wn_user_is_not_chairman').show();";
				$msEcho .= "hack_change_customize('custom_quantity_rm_parameters');";
			}

			//Atualiza os valores de risco
			$moQueryRiskUpdate = new QueryRiskUpdate(FWDWebLib::getConnection());
			$moQueryRiskUpdate->makeQuery();
			$moQueryRiskUpdate->executeQuery();
			// Atualiza os limites de quantidade de incidente para o Calculo Autom�tico de probabilidade
			if(ISMSLib::hasModule(INCIDENT_MODE)){
				$moQueryRiskProbabilityUpdate = new QueryRiskProbabilityUpdate();
				$moQueryRiskProbabilityUpdate->setNewValuesCount($miRiskCount);
				$moQueryRiskProbabilityUpdate->makeQuery();
				$moQueryRiskProbabilityUpdate->executeQuery();
			}
		}
		//Apaga 4 e 5 quando passar de 5 para 3
		if ( ($miRiskCount==3) && (ISMSLib::getConfigById(RISK_VALUE_COUNT)==5) ) {
			$moPValue = new RMParameterValueName();
			$moRCPValue = new RMRCParameterValueName();
			for ($miI=4; $miI<=5; $miI++) {
				if ($maAssetRiskIds[$miI]) {
					$moPValue->delete($maAssetRiskIds[$miI]);
				}
				if ($maRiskControlIds[$miI-1]) {
					$moRCPValue->delete($maRiskControlIds[$miI-1]);
				}
			}
		}


		$maRiskControlIds = serialize($maRiskControlIdsNew);
		$maRiskControlIds = str_replace('\\','\\\\"',$maRiskControlIds);
		$maRiskControlIds = str_replace('"','\"',$maRiskControlIds);
		echo "gebi('var_risk_control_ids').value = '$maRiskControlIds';";
		$maAssetRiskIds = serialize($maAssetRiskIdsNew);
		$maAssetRiskIds = str_replace('\\','\\\\"',$maAssetRiskIds);
		$maAssetRiskIds = str_replace('"','\"',$maAssetRiskIds);
		echo "gebi('var_asset_risk_ids').value = '$maAssetRiskIds';";
		//seta o novo valor da quantidade de riscos do sistema
		ISMSLib::setConfigById(RISK_VALUE_COUNT,$miRiskCount);



		echo $msEcho;
		echo "gebi('var_risk_value_count').value = '$miRiskCount';";
		echo "gobi('save_warning').show(); setTimeout('gobi(\\'save_warning\\').hide()',5000);";
	}
}

class SaveControlCostNamesEvent extends FWDRunnable {
	public function run() {
		ISMSLib::setConfigById(CONTROL_COST_1,FWDWebLib::getObject("cname1")->getValue());
		ISMSLib::setConfigById(CONTROL_COST_2,FWDWebLib::getObject("cname2")->getValue());
		ISMSLib::setConfigById(CONTROL_COST_3,FWDWebLib::getObject("cname3")->getValue());
		ISMSLib::setConfigById(CONTROL_COST_4,FWDWebLib::getObject("cname4")->getValue());
		ISMSLib::setConfigById(CONTROL_COST_5,FWDWebLib::getObject("cname5")->getValue());
		echo "gobi('save_warning').show(); setTimeout('gobi(\\'save_warning\\').hide()',5000);";
	}
}

class SaveImpactTypeEvent extends FWDRunnable {


	public function save($index){
		$impactName = FWDWebLib::getObject("impact_".$index)->getValue();
		$impactW = FWDWebLib::getObject("impactW_".$index)->getValue();
		$impactid = FWDWebLib::getObject("impactid_".$index)->getValue();
		if($impactName != NULL && $impactName != ""){
			$impact = new CMImpactType();
			$impact->setFieldValue('impact_type_name', $impactName);
			$impact->setFieldValue('impact_type_weight', $impactW);
			if($impactid){
				$impact->update($impactid);
			}else{
				$impactid = $impact->insert(true);
				echo "gebi('impactid_".$index."').value = '".$impactid."';";
			}
		}else if($impactid){
			$impact = new CMImpactType();
			$impact->setFieldValue('impact_type_name', "");
			$impact->setFieldValue('impact_type_weight', 0);
			$impact->update($impactid);
		}
	}

	public function run() {
		$this->save(1);
		$this->save(2);
		$this->save(3);
		$this->save(4);
		$this->save(5);
		echo "gobi('save_warning').show(); setTimeout('gobi(\\'save_warning\\').hide()',5000);";
	}
}

class SavePriorityEvent extends FWDRunnable {

	public function run() {
		$i = 1;
		while($i <= 6){
			$priorityName = FWDWebLib::getObject("priority_".$i)->getValue();
			$priorityWeight = FWDWebLib::getObject("priority_weight_".$i)->getValue();
			if(!$priorityWeight){
				$priorityWeight = 0;
			}
			$priorityId = FWDWebLib::getObject("priorityId_".$i)->getValue();

			$priority = new CMPriority();
			if($priorityName != NULL && $priorityName != ""){
				$priority->setFieldValue('priority_name', $priorityName);
				$priority->setFieldValue('weight', $priorityWeight);
				if($priorityId){
					$priority->update($priorityId);
				}else{
					$priorityId = $priority->insert(true);
					echo "gebi('priorityId_".$i."').value = '".$priorityId."';";
				}
			}else if($priorityId){
				$priority->setFieldValue('priority_name', "");
				$priority->setFieldValue('priority_weight', "0");
				$priority->update($priorityId);
			}
			$i++;
		}
		echo "gobi('save_warning').show(); setTimeout('gobi(\\'save_warning\\').hide()',5000);";
	}
}

/*
 * A��o do bot�o salvar da tela 'N�vel de dano dos Impactos'
 */
class SaveImpactDamageEvent extends FWDRunnable {

	public function save($index){
		$impactName = FWDWebLib::getObject("impactDamage_".$index)->getValue();
		$impactW = FWDWebLib::getObject("impactDamageW_".$index)->getValue();
		$impactid = FWDWebLib::getObject("impactDamageid_".$index)->getValue();

		if($impactName != NULL && $impactName != ""){

			$impact = new CMDamageMatrixParameter();
			$impact->setFieldValue('damage_matrix_parameter_name', $impactName);
			$impact->setFieldValue('damage_matrix_parameter_weight', $impactW);

			if($impactid){
				$impact->update($impactid);
			}else{
				$impactid = $impact->insert(true);
				echo "gebi('impactDamageid_".$index."').value = '".$impactid."';";
			}

		}else if($impactid){
			$impact = new CMDamageMatrixParameter();
			$impact->setFieldValue('damage_matrix_parameter_name', "");
			$impact->setFieldValue('damage_matrix_parameter_weight', 0);
			$impact->update($impactid);
		}
	}

	public function run() {
		$this->save(1);
		$this->save(2);
		$this->save(3);
		$this->save(4);
		$this->save(5);
		$this->save(6);

		echo "gobi('save_warning').show(); ";
		echo "setTimeout('gobi(\\'save_warning\\').hide()',5000); ";
	}
}

class SaveFinancialImpactEvent extends FWDRunnable {

	public function save($index){
		$impactName = FWDWebLib::getObject("financial_impact_".$index)->getValue();
		$startImpact = FWDWebLib::getObject("start_impact_".$index)->getValue();
		if($index != 6)
		$endImpact = FWDWebLib::getObject("end_impact_".$index)->getValue();
		$impactid = FWDWebLib::getObject("financial_impactid_".$index)->getValue();
		if($impactName != NULL && $impactName != ""){
			$impact = new CMFinancialImpact();
			$impact->setFieldValue('financial_impact_name', $impactName);
			$impact->setFieldValue('start_range', $startImpact);
			if($index != 6)
			$impact->setFieldValue('end_range', $endImpact);
			if($impactid){
				$impact->update($impactid);
			}else{
				$impactid = $impact->insert(true);
				echo "gebi('financial_impactid_".$index."').value = '".$impactid."';";
			}
		}else if($impactid){
			$impact = new CMFinancialImpact();
			$impact->setFieldValue('financial_impact_name', "");
			$impact->setFieldValue('start_range', 0);
			$impact->setFieldValue('end_range', 0);
			$impact->update($impactid);
		}
	}

	public function run() {
		$this->save(1);
		$this->save(2);
		$this->save(3);
		$this->save(4);
		$this->save(5);
		//$this->save(6);
		echo "gobi('save_warning').show(); setTimeout('gobi(\\'save_warning\\').hide()',5000);";
	}
}

/*
 * Classe de "evento" que salva a tela Intervalo de Tempo dos Impactos.
 */
class SaveImpactsIntervalEvent extends FWDRunnable {

	public function save($index){
		$damageName = FWDWebLib::getObject("dmgname{$index}")->getValue();
		$damageId = FWDWebLib::getObject("damageId_{$index}")->getValue();

		if($damageName != NULL && $damageName != ""){
			$dmg = new CMDamageMatrix();
			$dmg->setFieldValue('damage_matrix_name', $damageName);

			if($damageId){
				$dmg->update($damageId);
			}else{
				$damageId = $dmg->insert(true);
				echo "gebi('damageId_{$index}').value = '{$damageId}';";
			}

		} else if($damageId){
			$dmg = new CMDamageMatrix();
			$dmg->setFieldValue('damage_matrix_name', "");
			$dmg->update($damageId);
		}
	}

	public function run() {
    $queryImpactsIntervalInUse = new QueryImpactsIntervalInUse(FWDWebLib::getConnection());
		$impactsIntervalIsInUse = $queryImpactsIntervalInUse->isInUse();
		
		// Impede save feito a for�a.
		if($impactsIntervalIsInUse)
		  return;
	  
		$this->save(1);
		$this->save(2);
		$this->save(3);
		$this->save(4);
		$this->save(5);
		$this->save(6);
		$this->save(7);

		$msConfig = new ISMSConfig();
		$msConfig->setConfig(DAMAGE_MATRIX_SET, 1);

		echo "gobi('save_warning').show(); ";
		echo "setTimeout('gobi(\\'save_warning\\').hide()',5000); ";
	}

}

class SaveFinancialImpactParametersNamesEvent extends FWDRunnable {
	public function run() {
		$msScript = '';
		if(ISMSLib::hasModule(INCIDENT_MODE)){
			$moContextClassification = new ISMSContextClassification();
			$moContextClassification->createFilter(CONTEXT_CLASSIFICATION_FINANCIAL_IMPACT,'classif_type');
			$moContextClassification->select();

			for($i=1;$i<=8;$i++){
				$miId = FWDWebLib::getObject("fin_imp_id_$i")->getValue();
				$msName = FWDWebLib::getObject("fin_imp_text_$i")->getValue();
				$mbChecked = FWDWebLib::getObject("fin_imp_checkbox_$i")->getAttrCheck() && $msName;
				if($miId){
					if($mbChecked){
						// update
						$moContextClassification = new ISMSContextClassification();
						$moContextClassification->setFieldValue('classif_name',$msName);
						$moContextClassification->update($miId);
					}else{
						// delete
						$moIncFinancialImpact = new CIIncidentFinancialImpact();
						$moIncFinancialImpact->createFilter($miId,'classification_id');
						$moIncFinancialImpact->delete();

						$moContextClassification = new ISMSContextClassification();
						$moContextClassification->delete($miId);
						$msScript.= "gebi('fin_imp_id_$i').value='';";
						$msScript.= "gebi('fin_imp_text_$i').value='';";
					}
				}elseif($mbChecked){
					// insert
					$moContextClassification = new ISMSContextClassification();
					$moContextClassification->setFieldValue('classif_name',$msName);
					$moContextClassification->setFieldValue('classif_context_type',CONTEXT_CI_INCIDENT);
					$moContextClassification->setFieldValue('classif_type',CONTEXT_CLASSIFICATION_FINANCIAL_IMPACT);
					$miId = $moContextClassification->insert(true);
					$msScript.= "gebi('fin_imp_id_$i').value=$miId;";
				}
			}

			$miCounter = 0;
			while($moContextClassification->fetch()){
				$miId = $moContextClassification->getFieldByAlias('classif_id')->getValue();
				$msName = $moContextClassification->getFieldByAlias('classif_name')->getValue();
				$miCounter++;
				FWDWebLib::getObject('fin_imp_id_'.$miCounter)->setValue($miId);
				FWDWebLib::getObject('fin_imp_text_'.$miCounter)->setValue($msName);
				FWDWebLib::getObject('fin_imp_text_'.$miCounter)->setAttrDisabled('false');
				FWDWebLib::getObject('fin_imp_checkbox_'.$miCounter)->setAttrCheck('true');
			}
		}

		echo "{$msScript} gobi('save_warning').show(); setTimeout('gobi(\\'save_warning\\').hide()',2000);";
	}
}



class SaveSpecialUsersEvent extends FWDRunnable {
	public function run() {
		ISMSLib::setConfigById(USER_CHAIRMAN,           FWDWebLib::getObject('user_chairman_id')->getValue());
		ISMSLib::setConfigById(USER_ASSET_CONTROLLER,   FWDWebLib::getObject('user_asset_controller_id')->getValue());
		ISMSLib::setConfigById(USER_CONTROL_CONTROLLER, FWDWebLib::getObject('user_control_controller_id')->getValue());
		ISMSLib::setConfigById(USER_LIBRARIAN,          FWDWebLib::getObject('user_librarian_id')->getValue());

		if (ISMSLib::hasModule(INCIDENT_MODE)) {
			ISMSLib::setConfigById(USER_INCIDENT_MANAGER,              FWDWebLib::getObject('user_incident_manager_id')->getValue());
			ISMSLib::setConfigById(USER_DISCIPLINARY_PROCESS_MANAGER,  FWDWebLib::getObject('user_disciplinary_process_manager_id')->getValue());
			ISMSLib::setConfigById(USER_NON_CONFORMITY_MANAGER,        FWDWebLib::getObject('user_non_conformity_manager_id')->getValue());
			ISMSLib::setConfigById(USER_EVIDENCE_MANAGER,              FWDWebLib::getObject('user_evidence_manager_id')->getValue());
		}
		echo "gobi('save_warning').show(); setTimeout('gobi(\\'save_warning\\').hide()',5000);";
	}
}


class SaveControlEvent extends FWDRunnable {
	public function run() {
		$maControlFields = array("control_revision"=>GENERAL_REVISION_ENABLED,"control_test"=>GENERAL_TEST_ENABLED);
		$maSelectedItems = FWDWebLib::getObject("control_controller")->getAllItemsCheck();
		//Atualiza os itens marcados
		$maCheckedItens = array();
		foreach ($maSelectedItems as $moItem) {
			$maCheckedItens[] = $moItem->getAttrKey();
		}
		foreach ($maControlFields as $miKey=>$miValue){
			if(in_array($miKey,$maCheckedItens)){
				ISMSLib::setConfigById($miValue,"1");
			}else{
				ISMSLib::setConfigById($miValue,"0");
			}
		}
		echo "gobi('save_warning').show();"
		."setTimeout('gobi(\\'save_warning\\').hide()',5000);";
	}
}

Class SaveFeaturesEvent extends FWDRunnable {
	public function run() {

		if(ISMSLib::getConfigById(ASSET_KEY))
			return;

		$maFeatureslFields = array( "asset_key"           => ASSET_KEY);

		$maFeatures = FWDWebLib::getObject('system_features_controller')->getAllItemsCheck();
		//Atualiza os itens marcados
		$maCheckedItens = array();
		foreach ($maFeatures as $moItem) {
			$maCheckedItens[] = $moItem->getAttrKey();
		}
		$mbChangedToType1 = false;
		$mbChangedToType2 = false;
		foreach ($maFeatureslFields as $miKey=>$miValue){
			if(in_array($miKey,$maCheckedItens)){
				if ($miKey == "risk_formula") {
					if (ISMSLib::getConfigById(RISK_FORMULA_TYPE) != RISK_FORMULA_TYPE_2) {
						$mbChangedToType2 = true;
					}
					ISMSLib::setConfigById($miValue, RISK_FORMULA_TYPE_2);
				}
				else {

					if($miKey == "asset_key"){
						$linkAssetsToProcesses = new QueryLinkAssetsToProcesses(FWDWebLib::getConnection());
						$linkAssetsToProcesses->executeQuery();
						
						echo "gobi('save_features_btn').disable();";		
					}

					ISMSLib::setConfigById($miValue,"1");
				}
			}else{
				if ($miKey == "risk_formula") {
					if (ISMSLib::getConfigById(RISK_FORMULA_TYPE) != RISK_FORMULA_TYPE_1) {
						$mbChangedToType1 = true;
					}
					ISMSLib::setConfigById($miValue, RISK_FORMULA_TYPE_1);
				}
				else {
					ISMSLib::setConfigById($miValue,"0");
				}
			}
		}

		if ($mbChangedToType1 || $mbChangedToType2) {
			echo "gobi('risk_values_updating').show(); updateAllValues();";
		}
		else {
			echo "gobi('save_warning').show(); setTimeout('gobi(\\'save_warning\\').hide()',5000);";
		}
	}
}

Class UpdateAllValuesEvent extends FWDRunnable {
	public function run() {
		//Atualiza os valores de risco
		set_time_limit(3600);
		$moQueryRiskUpdate = new QueryUpdateAllValues(FWDWebLib::getConnection());
		$moQueryRiskUpdate->makeQuery();
		$moQueryRiskUpdate->executeQuery();
		echo "gobi('save_warning').hide();";
		echo "gobi('risk_values_updating').hide();";
		echo "gobi('risk_values_updated').show(); setTimeout('gobi(\\'risk_values_updated\\').hide()',5000);";
	}
}

function save_risk_limits($poContext){
	//teste para verificar se o sistema n�o est� sendo hakeado
	$moCtxUserTest = new RMRiskLimits();
	$moCtxUserTest->testPermissionToInsert();

	$poContext->insert();
	ISMSLib::setConfigById(RISK_LOW, $poContext->getFieldValue('risk_limits_low'));
	ISMSLib::setConfigById(RISK_HIGH,$poContext->getFieldValue('risk_limits_high'));

	$msEcho = "";
	$moRiskLimits = new RMRiskLimits();
	if($moRiskLimits->getApprover() != ISMSLib::getCurrentUserId()){
		$moUser = new ISMSUser();
		$moUser->fetchById($moRiskLimits->getApprover());
		$msWarning = FWDWebLib::getObject('wn_user_is_not_chairman_risk_limits')->getValue();
		$msWarning = str_replace("%chairman_name%", $moUser->getName(), $msWarning);
		$msEcho .= "gobi('wn_user_is_not_chairman_risk_limits').setValue('$msWarning');";
		$msEcho .= "gobi('wn_user_is_not_chairman_risk_limits').show();";
	}

	echo $msEcho;
	echo "gobi('save_warning').show(); setTimeout('gobi(\\'save_warning\\').hide()',5000);";
}

class ConfirmEditEvent extends FWDRunnable {
	public function run(){
		$moRiskLimits = new RMRiskLimits();
		$moRiskLimits->setFieldValue('risk_limits_low' ,FWDWebLib::getObject('risk_limits_low')->getValue());
		$moRiskLimits->setFieldValue('risk_limits_high',FWDWebLib::getObject('risk_limits_high')->getValue());
		save_risk_limits($moRiskLimits);
	}
}

class SaveRiskLimitsEvent extends FWDRunnable {
	public function run(){
		$moRiskLimits = new RMRiskLimits();
		$moRiskLimits->setFieldValue('risk_limits_low' ,FWDWebLib::getObject('risk_limits_low')->getValue());
		$moRiskLimits->setFieldValue('risk_limits_high',FWDWebLib::getObject('risk_limits_high')->getValue());

		$miRiskHighMax = ISMSLib::getConfigById(RISK_VALUE_COUNT);
		$miRiskHighMax*= $miRiskHighMax;

		$miRiskLow = $moRiskLimits->getFieldValue('risk_limits_low');
		$miRiskHigh = $moRiskLimits->getFieldValue('risk_limits_high');

		if($miRiskLow > $miRiskHigh){
			echo "js_hide('high_gt_max_warning'); js_hide('low_limit_warning'); js_show('low_gt_high_warning');";
			return;
		}

		if($miRiskHigh > $miRiskHighMax){
			echo "js_hide('low_gt_high_warning'); js_hide('low_limit_warning'); js_show('high_gt_max_warning');";
			return;
		}

		if($miRiskLow < 1) {
			echo "js_hide('low_gt_high_warning'); js_hide('high_gt_max_warning'); js_show('low_limit_warning');";
			return;
		}

		$moRiskLimits->setHash(FWDWebLib::getObject('hash_risk_limits')->getValue());
		if($moRiskLimits->hasSensitiveChanges()){
			$miUserId = ISMSLib::getCurrentUserId();
			if($miUserId==$moRiskLimits->getApprover()){
				save_risk_limits($moRiskLimits);
			}else{
				$msTitle = FWDLanguage::getPHPStringValue('tt_reapproval','Reaprova��o');
				$msMessage = FWDLanguage::getPHPStringValue('st_sensitive_data_reapproval','Voc� est� alterando dados sens�veis, que necessitam passar por uma reaprova��o. Deseja confirmar as altera��es?');
				$msEventValue = " soPopUpManager.getPopUpById('popup_confirm').getOpener().trigger_event('confirm_risk_limits_edit',3);";
				ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,75);
			}
		} //else{}
	}
}

class ConfirmRiskParWeightSave extends FWDRunnable {
	public function run() {
		$maAssociation = FWDWebLib::unserializeString(FWDWebLib::getObject('association')->getValue());
		$mbAllZero = true;
		$mbChange = false;
		for($miI=1;$miI<=count($maAssociation);$miI++) {
			$miWeight = $maAssociation[$miI][1];
			$miNewWeight = FWDWebLib::getObject("parameter_weight_{$miI}")->getValue();
			if ($miNewWeight) {
				$mbAllZero = false;
			}
			if ($miWeight != $miNewWeight) {
				$mbChange = true;
			}
		}
		if ($mbAllZero) {
			echo "js_show('at_least_one_positive_warning');";
			exit;
		} else {
			echo "js_hide('at_least_one_positive_warning');";
		}
		if ($mbChange){
			$msTitle = FWDLanguage::getPHPStringValue('tt_sensitive_data','Dados Sens�veis');
			$msMessage = FWDLanguage::getPHPStringValue('st_risk_parameters_weight_edit_confirm','Voc� est� alterando dados sens�veis. Ao alterar o peso dos par�metros os valores de todos os riscos e ativos do sistema ser�o alterados de acordo. Deseja confirmar as altera��es?');
			$msEventValue = "soPopUpManager.getPopUpById('popup_confirm').getOpener().trigger_event('save_risk_par_weight',3);";
			ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,75);
		}
	}
}

class SaveRiskParWeight extends FWDRunnable {
	public function run(){
		$maAssociation = FWDWebLib::unserializeString(FWDWebLib::getObject('association')->getValue());
		foreach($maAssociation as $miIndex=>$maParameter) {
			$moPName = new RMParameterName();
			$moPName->setFieldValue('parametername_weight',FWDWebLib::getObject("parameter_weight_{$miIndex}")->getValue());
			$moPName->createFilter($maParameter[0],'parametername_id');
			$moPName->update();
		}
		$moQueryRiskUpdate = new QueryRiskUpdate(FWDWebLib::getConnection());
		$moQueryRiskUpdate->makeQuery();
		$moQueryRiskUpdate->executeQuery();
		echo "gobi('save_warning').show(); setTimeout('gobi(\\'save_warning\\').hide()',5000);";
	}
}

class SaveReportClassificationEvent extends FWDRunnable {
	public function run(){
		ISMSLib::setConfigById(GENERAL_SYSTEM_CLASSIFICATION, FWDWebLib::getObject('classification')->getValue());
		ISMSLib::setConfigById(GENERAL_SYSTEM_CLASSIFICATION_DEST,FWDWebLib::getObject('classification_dest')->getValue());
		echo "gobi('save_warning').show(); setTimeout('gobi(\\'save_warning\\').hide()',5000);";
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));
		$moStartEvent->addAjaxEvent(new OpenRiskParametersNamesConfirm("open_risk_parameters_names_confirm"));
		$moStartEvent->addAjaxEvent(new SaveRiskParametersNamesEvent("save_risk_parameters_names_event"));
		$moStartEvent->addAjaxEvent(new SaveControlEvent("save_control_event"));
		$moStartEvent->addAjaxEvent(new SaveSpecialUsersEvent("save_special_users_event"));
		$moStartEvent->addAjaxEvent(new SaveClassificationEvent("save_classification_event"));
		$moStartEvent->addAjaxEvent(new SaveFeaturesEvent("save_features_event"));
		$moStartEvent->addAjaxEvent(new SaveImpactsIntervalEvent("save_impacts_interval_event"));
		$moStartEvent->addAjaxEvent(new SaveControlCostNamesEvent("save_control_cost_names_event_event"));
		$moStartEvent->addAjaxEvent(new SaveFinancialImpactParametersNamesEvent("save_financial_impact_parameters_names_event"));
		$moStartEvent->addAjaxEvent(new SaveNumberOfRiskParameters("save_number_of_risk_parameters_event"));
		$moStartEvent->addAjaxEvent(new OpenNumberOfRiskParametersConfirm("open_number_of_risk_parameters_confirm"));
		$moStartEvent->addAjaxEvent(new SaveImpactTypeEvent("SaveImpactTypeEvent"));
		$moStartEvent->addAjaxEvent(new SaveImpactDamageEvent("SaveImpactDamageEvent"));
		$moStartEvent->addAjaxEvent(new SavePriorityEvent("SavePriorityEvent"));
		$moStartEvent->addAjaxEvent(new SaveFinancialImpactEvent("SaveFinancialImpactEvent"));
		$moStartEvent->addAjaxEvent(new SaveRiskLimitsEvent('save_risk_limits_event'));
		$moStartEvent->addAjaxEvent(new ConfirmEditEvent('confirm_risk_limits_edit'));
		$moStartEvent->addAjaxEvent(new ConfirmRiskParWeightSave('confirm_risk_par_weight_save'));
		$moStartEvent->addAjaxEvent(new SaveRiskParWeight('save_risk_par_weight'));
		$moStartEvent->addAjaxEvent(new SaveReportClassificationEvent('save_report_classification_event'));
		$moStartEvent->addAjaxEvent(new UpdateAllValuesEvent('update_all_values_event'));
		$moStartEvent->addAjaxEvent(new SaveCurrencyEvent('save_currency_event'));

		$moWebLib = FWDWebLib::getInstance();
		$moDialog = FWDWebLib::getObject("dialog");
		$moWebLib->xml_load($moWebLib->getSysRef().'/packages/admin/custom_impacts_interval.xml',false);
		$moDialog->addObjFWDView(FWDWebLib::getObject("custom_impacts_interval"));


		$moWebLib->xml_load($moWebLib->getSysRef().'/packages/admin/custom_asset_values_name.xml',false);
		$moDialog->addObjFWDView(FWDWebLib::getObject("custom_asset_values_name"));

		$moWebLib->xml_load($moWebLib->getSysRef().'/packages/admin/custom_impact_type.xml',false);
		$moDialog->addObjFWDView(FWDWebLib::getObject("custom_impact_type"));

		$moWebLib->xml_load($moWebLib->getSysRef().'/packages/admin/custom_priority.xml',false);
		$moDialog->addObjFWDView(FWDWebLib::getObject("custom_priority"));

		$moWebLib->xml_load($moWebLib->getSysRef().'/packages/admin/custom_impact_damage.xml',false);
		$moDialog->addObjFWDView(FWDWebLib::getObject("custom_impact_damage"));

		$moWebLib->xml_load($moWebLib->getSysRef().'/packages/admin/custom_financial_impact.xml',false);
		$moDialog->addObjFWDView(FWDWebLib::getObject("custom_financial_impact"));

		$moWebLib->xml_load($moWebLib->getSysRef().'/packages/admin/custom_risk_control_values_name.xml',false);
		$moDialog->addObjFWDView(FWDWebLib::getObject("custom_risk_control_values_name"));

		$moWebLib->xml_load($moWebLib->getSysRef().'/packages/admin/custom_control_cost_name.xml',false);
		$moDialog->addObjFWDView(FWDWebLib::getObject("custom_control_cost_name"));

		$moWebLib->xml_load($moWebLib->getSysRef().'/packages/admin/custom_financial_impact_parameters.xml',false);
		$moDialog->addObjFWDView(FWDWebLib::getObject("custom_financial_impact_parameters"));

		$moWebLib->xml_load($moWebLib->getSysRef().'/packages/admin/custom_system_features.xml',false);
		$moDialog->addObjFWDView(FWDWebLib::getObject("custom_system_features"));

		$moWebLib->xml_load($moWebLib->getSysRef().'/packages/admin/custom_report_classification.xml',false);
		$moDialog->addObjFWDView(FWDWebLib::getObject("custom_report_classification"));

		$moWebLib->xml_load($moWebLib->getSysRef().'/packages/admin/custom_currency.xml',false);
		$moDialog->addObjFWDView(FWDWebLib::getObject("custom_currency"));		
		
		/* Se n�o possuir o m�dulo de documenta��o, n�o deve mostrar os tipos de classifica��o de documento e registro. */
		if(!ISMSLib::hasModule(POLICY_MODE)){
			FWDWebLib::getObject('item_document')->setShouldDraw(false);
			FWDWebLib::getObject('item_register')->setShouldDraw(false);
			FWDWebLib::getObject('cl_document')->setShouldDraw(false);
			FWDWebLib::getObject('cl_register')->setShouldDraw(false);
			FWDWebLib::getObject('has_pm_module')->setValue('0');
		}
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		//for�a o carregamento das configura��es do banco na sess�o, importante para pegar as informa��es
		//que o ACT possivelmente alterou no banco
		$moConfigForce = new ISMSConfig();
		$moConfigForce->loadConfigForced();

		/********** Procedimentos relacionados a configuracao: Intervalo de Tempo dos Impactos **********/
		$moDamageMatrix = new CMDamageMatrix();
		$moMatrix = $moDamageMatrix->getDamageMatrix();
		$i = 1;
		
    $queryImpactsIntervalInUse = new QueryImpactsIntervalInUse(FWDWebLib::getConnection());
		$impactsIntervalIsInUse = $queryImpactsIntervalInUse->isInUse();
		
		foreach($moMatrix as $matrixId=>$matrixValue) {
			FWDWebLib::getObject("dmgname".$i)->setValue($matrixValue);
			FWDWebLib::getObject("damageId_".$i)->setValue($matrixId);
			
			if($impactsIntervalIsInUse){
			  FWDWebLib::getObject("dmgname".$i)->setAttrDisabled('true');
			}			
			
			$i = $i + 1;
		}
		
		if($impactsIntervalIsInUse){
		  FWDWebLib::getObject("vb_save_impacts_interval")->setAttrDisabled('true');
		  FWDWebLib::getObject("lb_impacts_inteval_blocked")->setAttrDisplay('true');
		}
		
		/********** Procedimentos relacionados ao tipo de impacto**********/
		$impact = new CMImpactType();
		$impact->select();
		$i = 1;
		while($impact->fetch() && $i <= 5){
			FWDWebLib::getObject("impactid_".$i)->setValue($impact->getFieldValue("impact_type_id"));
			FWDWebLib::getObject("impact_".$i)->setValue($impact->getFieldValue("impact_type_name"));
			FWDWebLib::getObject("impactW_".$i)->setValue($impact->getFieldValue("impact_type_weight"));
			$i = $i + 1;
		}

		/********** Procedimentos relacionados as prioridades**********/
		$priority = new CMPriority();
		$priority->select();
		$i = 1;
		while($priority->fetch() && $i <= 6){
			FWDWebLib::getObject("priorityId_".$i)->setValue($priority->getFieldValue("priority_id"));
			FWDWebLib::getObject("priority_".$i)->setValue($priority->getFieldValue("priority_name"));
			FWDWebLib::getObject("priority_weight_".$i)->setValue($priority->getFieldValue("weight"));
			$i = $i + 1;
		}

		/********** Procedimentos relacionados ao dano de impacto**********/
		$impactDamage = new CMDamageMatrixParameter();
		$impactDamage->select();
		$i = 1;
		while($impactDamage->fetch() && $i <= 6){
			FWDWebLib::getObject("impactDamageid_".$i)->setValue($impactDamage->getFieldValue("damage_matrix_parameter_id"));
			FWDWebLib::getObject("impactDamage_".$i)->setValue($impactDamage->getFieldValue("damage_matrix_parameter_name"));
			FWDWebLib::getObject("impactDamageW_".$i)->setValue($impactDamage->getFieldValue("damage_matrix_parameter_weight"));
			$i = $i + 1;
		}

		/********** Procedimentos relacionados ao impacto financeiro **********/
		$financialImpact = new CMFinancialImpact();
		$financialImpact->select();
		$i = 1;
		while($financialImpact->fetch() && $i <= 5){
			FWDWebLib::getObject("financial_impactid_".$i)->setValue($financialImpact->getFieldValue("financial_impact_id"));
			FWDWebLib::getObject("financial_impact_".$i)->setValue($financialImpact->getFieldValue("financial_impact_name"));
			FWDWebLib::getObject("start_impact_".$i)->setValue($financialImpact->getFieldValue("start_range"));
			if($i != 6)
			FWDWebLib::getObject("end_impact_".$i)->setValue($financialImpact->getFieldValue("end_range"));
			$i = $i + 1;
		}

		/********** Procedimentos relacionados � moeda  **********/
		$msCurrency = $moConfigForce->getConfig(GENERAL_CURRENCY_IDENTIFIER);
		FWDWebLib::getObject('currency')->setValue($msCurrency);

		$currencyIsSet = $moConfigForce->getConfig(CURRENCY_SET);
		
		if($currencyIsSet){
			FWDWebLib::getObject('currency')->setAttrDisabled("true");
			FWDWebLib::getObject('save_btn')->setAttrDisabled("true");
		}

		/********** Procedimentos relacionados aos par�metros de impacto financeiro **********/
		$moContextClassification = new ISMSContextClassification();
		$moContextClassification->createFilter(CONTEXT_CLASSIFICATION_FINANCIAL_IMPACT,'classif_type');
		$moContextClassification->select();

		$miCounter = 0;
		while($moContextClassification->fetch()){
			$miCounter++;
			FWDWebLib::getObject('fin_imp_id_'.$miCounter)->setValue($moContextClassification->getFieldByAlias('classif_id')->getValue());
			FWDWebLib::getObject('fin_imp_text_'.$miCounter)->setValue($moContextClassification->getFieldByAlias('classif_name')->getValue());
			FWDWebLib::getObject('fin_imp_text_'.$miCounter)->setAttrDisabled('false');
			FWDWebLib::getObject('fin_imp_checkbox_'.$miCounter)->setAttrCheck('true');
		}

		/********** Procedimentos relacionados �s Features do sistema **********/
		$moFeaturesController = FWDWebLib::getObject('system_features_controller');
		if(ISMSLib::getConfigById(ASSET_KEY)) {
			$moFeaturesController->checkItem("asset_key");
			FWDWebLib::getObject('save_features_btn')->setAttrDisabled("true");
		}

		/********** Procedimentos relacionados a classifica��o dos relat�rios **********/
		FWDWebLib::getObject('classification')->setValue(ISMSLib::getConfigById(GENERAL_SYSTEM_CLASSIFICATION));
		FWDWebLib::getObject('classification_dest')->setValue(ISMSLib::getConfigById(GENERAL_SYSTEM_CLASSIFICATION_DEST));

		//instala a seguran�a de ACL na p�gina
		FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());

		if (FWDWebLib::getObject("custom_impact_type")->getShouldDraw()){
			FWDWebLib::getObject("custom_impact_type")->setAttrDisplay("true");
		}
		else if (FWDWebLib::getObject("custom_impacts_interval")->getShouldDraw()){
			FWDWebLib::getObject("custom_impacts_interval")->setAttrDisplay("true");
		}
		else if (FWDWebLib::getObject("custom_impact_damage")->getShouldDraw()){
			FWDWebLib::getObject("custom_impact_damage")->setAttrDisplay("true");
		}
		else if (FWDWebLib::getObject("custom_priority")->getShouldDraw()){
			FWDWebLib::getObject("custom_priority")->setAttrDisplay("true");
		}
		else if (FWDWebLib::getObject("custom_financial_impact")->getShouldDraw()){
			FWDWebLib::getObject("custom_financial_impact")->setAttrDisplay("true");
		}
		else if (FWDWebLib::getObject("custom_system_features")->getShouldDraw()){
			FWDWebLib::getObject("custom_system_features")->setAttrDisplay("true");
		}
		else if (FWDWebLib::getObject("custom_report_classification")->getShouldDraw()){
			FWDWebLib::getObject("custom_report_classification")->setAttrDisplay("true");
		}
		else if (FWDWebLib::getObject("custom_currency")->getShouldDraw()){
			FWDWebLib::getObject("custom_currency")->setAttrDisplay("true");
		}

		// teste para retirar campos referentes ao m�dulo de documenta��o
		if(!ISMSLib::hasModule(POLICY_MODE)){
			FWDWebLib::getObject('auto_doc_creation')->setShouldDraw(false);
			FWDWebLib::getObject('auto_doc_creation_help')->setShouldDraw(false);
			FWDWebLib::getObject('auto_doc_creation_checkbox')->setShouldDraw(false);
		}

		// teste para retirar campos referentes ao m�dulo de incidentes
		if(!ISMSLib::hasModule(INCIDENT_MODE)){
			FWDWebLib::getObject('custom_financial_impact_parameters')->setShouldDraw(false);
			FWDWebLib::getObject('name_custom_financial_impact_parameters')->setShouldDraw(false);
			FWDWebLib::getObject('has_ci_module')->setValue('0');

		}

		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
<script language="javascript">

    if(gebi('financial_impact_1')){
    	gebi('financial_impact_1').style.color="green";
    	gebi('financial_impact_1').style.fontWeight="bold";
    }
    if(gebi('financial_impact_2')){
    	gebi('financial_impact_2').style.color="#c9b52e";
    	gebi('financial_impact_2').style.fontWeight="bold";
    }
    if(gebi('financial_impact_3')){
    	gebi('financial_impact_3').style.color="orange";
    	gebi('financial_impact_3').style.fontWeight="bold";
    }
    if(gebi('financial_impact_4')){
    	gebi('financial_impact_4').style.color="red";
    	gebi('financial_impact_4').style.fontWeight="bold";
    }
    if(gebi('financial_impact_5')){
    	gebi('financial_impact_5').style.color="purple";
    	gebi('financial_impact_5').style.fontWeight="bold";
    }

    if(!gebi('rc_impact3_0').value) gebi('rc_impact3_0').value = '0';
    if(!gebi('rc_impact5_0').value) gebi('rc_impact5_0').value = '0';
    if(!gebi('rc_prob3_0').value) gebi('rc_prob3_0').value = '0';
    if(!gebi('rc_prob5_0').value) gebi('rc_prob5_0').value = '0';
    
    js_show = function(id){
      gebi(id).style.display='block';
      gebi(id).style.visibility='visible';
    }
    
    js_hide = function(id){
      gebi(id).style.display='none';
      gebi(id).style.visibility='hidden';
    }

    for(var siI=1;siI<=8;siI++){
      if(gebi('parameter_weight_'+siI)){
        if(!gebi('parameter_weight_'+siI).value){
          gebi('parameter_weight_'+siI).value='0';
        }
      }
    }
    
    function changeIsFillable(paFields,psPanel,psPanelToShow){
      for (var i=0; i < paFields.length; i++){
        if(gebi(paFields[i])){
          gobi(paFields[i]).isFillable = function(){
            var msElement = paFields[i];
            var msPanelToShow = psPanelToShow;
            var msPanel = psPanel;
            if(gebi(msPanel).style.display=='none'){
              return false;
            }else{
              if(!this.isFilled()){
                hack_change_customize(msPanelToShow);
              }
              return true;
            }
          }
        }
      }
    }

    var maAsset3   = new Array('importance3_1','importance3_2','importance3_3');
    var maAsset5   = new Array('importance5_1','importance5_2','importance5_3','importance5_4','importance5_5');
   // var maRisk3   = new Array('impact3_1','impact3_2','impact3_3','riskprob3_1','riskprob3_2','riskprob3_3');
  //  var maRisk5   = new Array('impact5_1','impact5_2','impact5_3','impact5_4','impact5_5','riskprob5_1','riskprob5_2','riskprob5_3','riskprob5_4','riskprob5_5');
    var maRiskControl3 = new Array('rc_impact3_0','rc_impact3_1','rc_impact3_2','rc_prob3_0','rc_prob3_1','rc_prob3_2');
    var maRiskControl5 = new Array('rc_impact5_0','rc_impact5_1','rc_impact5_2','rc_impact5_3','rc_impact5_4','rc_prob5_0','rc_prob5_1','rc_prob5_2','rc_prob5_3','rc_prob5_4');
    changeIsFillable(maAsset3       ,'vg_asset_val_3'                       ,'custom_asset_values_name');
    //changeIsFillable(maRisk3        ,'vg_risk_val_3'                        ,'custom_risk_values_name');
    changeIsFillable(maRiskControl3 ,'vg_custom_risk_control_values_name_3' ,'custom_risk_control_values_name');
    changeIsFillable(maRiskControl5 ,'vg_custom_risk_control_values_name_5' ,'custom_risk_control_values_name');
    changeIsFillable(maAsset5       ,'vg_asset_val_5'                       ,'custom_asset_values_name');
    //changeIsFillable(maRisk5        ,'vg_risk_val_5'                        ,'custom_risk_values_name');
    maAsset3 = maRisk3 = maRiskControl3 = maAssetRisk5 = maRisk5 = maRiskControl5 = null;

    function hack_change_customize(psSelectId){
      if(gebi(gebi('var_selected_panel').value)){
        js_hide(gebi('var_selected_panel').value)
      }
      gebi('var_selected_panel').value = psSelectId;
      if (gebi(gebi('var_selected_panel').value)){
        js_show(gebi('var_selected_panel').value);
        gebi('customization_select').value = gebi('var_selected_panel').value;
      }
    }
    
    function change_customize () {
      if(gebi('var_selected_panel').value && gebi('var_selected_panel').value!=undefined){
        if(gebi(gebi('var_selected_panel').value)){
          js_hide(gebi('var_selected_panel').value)
        }
      }
      gebi('var_selected_panel').value = gebi('customization_select').value;
      if (gebi(gebi('var_selected_panel').value)){
        js_show(gebi('var_selected_panel').value);
      }
    }
    
    function change_classification () {
      if(gebi('var_select_classification_panel').value && gebi('var_select_classification_panel').value!=undefined){
        if(gebi(gebi('var_select_classification_panel').value)){
          js_hide(gebi('var_select_classification_panel').value)
        }
      }
      gebi('var_select_classification_panel').value = gebi('classification_select').value;
      if (gebi(gebi('classification_select').value)){
        js_show(gebi('classification_select').value);
      }
    }
    
    function hack_change_classification (psPanel) {
      if(gebi(gebi('var_select_classification_panel').value)){
        js_hide(gebi('var_select_classification_panel').value);
      }
      gebi('classification_select').value = psPanel;
      gebi('var_select_classification_panel').value = psPanel;
      if (gebi(gebi('classification_select').value)){
        js_show(gebi('classification_select').value);
      }
    }

    function save_number_of_risk_parameters_event(){
      trigger_event('save_number_of_risk_parameters_event',3);
    }
    
    function save_parametrization(){
      var maFieldNames = new Array(
        "cl_type_area1","cl_type_area2","cl_type_area3","cl_type_area4","cl_type_area5","cl_type_area6","cl_type_area7","cl_type_area8",
        "cl_prio_area1","cl_prio_area2","cl_prio_area3","cl_prio_area4","cl_prio_area5","cl_prio_area6","cl_prio_area7","cl_prio_area8",
        "cl_type_process1","cl_type_process2","cl_type_process3","cl_type_process4","cl_type_process5","cl_type_process6","cl_type_process7","cl_type_process8",
        "cl_prio_process1","cl_prio_process2","cl_prio_process3","cl_prio_process4","cl_prio_process5","cl_prio_process6","cl_prio_process7","cl_prio_process8",
        "cl_type_risk1","cl_type_risk2","cl_type_risk3","cl_type_risk4","cl_type_risk5","cl_type_risk6","cl_type_risk7","cl_type_risk8",
        "cl_type_event1","cl_type_event2","cl_type_event3","cl_type_event4","cl_type_event5","cl_type_event6","cl_type_event7","cl_type_event8",
        "cl_type_control1","cl_type_control2","cl_type_control3","cl_type_control4","cl_type_control5","cl_type_control6","cl_type_control7","cl_type_control8"
      );
      if (gebi('has_pm_module').value == '1') {
        maFieldNames.concat(
          new Array(
            "cl_type_document1","cl_type_document2","cl_type_document3","cl_type_document4","cl_type_document5","cl_type_document6","cl_type_document7","cl_type_document8",
            "cl_type_register1","cl_type_register2","cl_type_register3","cl_type_register4","cl_type_register5","cl_type_register6","cl_type_register7","cl_type_register8"
          )
        );
      }
      for (var i=0; i < maFieldNames.length; i++) {
        if (!trim(gebi(maFieldNames[i]).value) && (gebi(maFieldNames[i]).disabled==false))
          disable_checkbox_clean_text_field('classification_controller',maFieldNames[i]);
      }
      maFieldNames = i = null;
    }
    
    function set_chairman(piId, psName){
      gebi('user_chairman_id').value = piId;
      gobi('user_chairman_name').setValue(psName);
    }
    
    function set_asset_controller(piId, psName){
      gebi('user_asset_controller_id').value = piId;
      gobi('user_asset_controller_name').setValue(psName);
    }
    
    function set_control_controller(piId, psName){
      gebi('user_control_controller_id').value = piId;
      gobi('user_control_controller_name').setValue(psName);
    }
    
    function set_librarian(piId, psName){
      gebi('user_librarian_id').value = piId;
      gobi('user_librarian_name').setValue(psName);
    }
    
    function set_incident_manager(piId, psName){
      gebi('user_incident_manager_id').value = piId;
      gobi('user_incident_manager_name').setValue(psName);
    }
    
    function set_disciplinary_process_manager(piId, psName){
      gebi('user_disciplinary_process_manager_id').value = piId;
      gobi('user_disciplinary_process_manager_name').setValue(psName);
    }
    
    function set_non_conformity_manager(piId, psName){
      gebi('user_non_conformity_manager_id').value = piId;
      gobi('user_non_conformity_manager_name').setValue(psName);
    }
    
    function set_evidence_manager(piId, psName){
      gebi('user_evidence_manager_id').value = piId;
      gobi('user_evidence_manager_name').setValue(psName);
    }

    ISMSFinancialConfigManager = {
      'csSelected': 'cost_names',
      'changeSelection': function(psSelection){
        if(psSelection!=this.csSelected){
          gebi('financial_config_selector').value = psSelection;
          js_hide('vg_'+this.csSelected);
          this.csSelected = psSelection;
          js_show('vg_'+this.csSelected);
        }
      },
      'toggleCheck':function(piIndex){
        gebi('fin_imp_text_'+piIndex).disabled = !gebi('fin_imp_text_'+piIndex).disabled;
      },
      'isDeleting': function(){
        if(gebi('has_ci_module').value){
          for(var i=1;i<=8;i++){
            if(gebi('fin_imp_id_'+i).value){
              if(!gebi('fin_imp_text_'+i).value || !gobi('financial_impact_controller_'+i).isChecked()){
                return true;
              }
            }
          }
        }
        return false;
      }
    }
    
    function updateAllValues() {
      trigger_event('update_all_values_event', 3);
    }
    
    function change_risk_value(piNum) {
      if (!(piNum == 3 || piNum == 5)) return false;
      var miOtherNum = (piNum == 3) ? 5 : 3;
      var maAssetRiskFields = new Array('importance','impact','riskprob');
      var maRiskControlFields = new Array('rc_impact','rc_prob');
      var miKbegin = 0;
      var miKend = 0;

      if(piNum==3){
        miKend = 1;
        js_hide('vg_asset_val_5');
        js_hide('vg_risk_val_5');
        js_hide('vg_custom_risk_control_values_name_5');
        js_show('vg_asset_val_3');
        js_show('vg_risk_val_3');
        js_show('vg_custom_risk_control_values_name_3');
        if ((gebi('risk_value_count').value == 5)&&(gebi('var_risk_value_count').value == 5)){
          js_show('optimist_group');
        }
      }else{
        miKbegin = 1;
        js_hide('vg_asset_val_3');
        js_hide('vg_risk_val_3');
        js_hide('vg_custom_risk_control_values_name_3');
        js_show('vg_asset_val_5');
        js_show('vg_risk_val_5');
        js_show('vg_custom_risk_control_values_name_5');
        js_hide('optimist_group');
        //setDefaultRiskParValue3_to_5_change();
      }
      
      for (var miI=0; miI<maAssetRiskFields.length; miI++) {
        for(var miK=1;miK<=3;miK++){
          gebi(maAssetRiskFields[miI] + piNum + '_'+ (miK + miKbegin)).value = gebi(maAssetRiskFields[miI] + miOtherNum + '_' + (miK + miKend)).value;
        }
      }
      for (var miI=0; miI<maRiskControlFields.length; miI++) {
        for(var miK=0;miK<3;miK++){
          gebi(maRiskControlFields[miI] + piNum + '_' + (miK + miKbegin)).value = gebi(maRiskControlFields[miI] + miOtherNum + '_' + (miK + miKend)).value;
        }
      }
      
      piNum = maAssetRiskFields = maRiskControlFields = miOtherNum = miI = miK = miKbegin = miKend = null;
    }
    
    function restore_risk_value_on_change(paValues,piDefaultValue){
      for (var miI=0; miI<paValues.length; miI++) {
        if(!gebi(paValues[miI]).value){
          gebi(paValues[miI]).value = piDefaultValue;
        }
      }
    }
  
    function save_pname() {
      trigger_event('save_risk_parameters_names_event','3');
    }

    function disable_checkbox_clean_text_field(psControllerName,psCheckBoxKey) {
      if (!trim(gebi(psCheckBoxKey).value)) {
        gebi(psCheckBoxKey).value = '';
        gebi(psCheckBoxKey).disabled = true;
        gfx_check(psControllerName,psCheckBoxKey);
      }
    }

      <? 
        $moDefaultConfig = new ISMSDefaultConfig();
        echo $moDefaultConfig->getJSCode();
      ?>

      </script>
      <?
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('nav_customization.xml');
?>