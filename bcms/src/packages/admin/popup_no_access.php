<?php
include_once "include.php";

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $msEvent = "soPopUpManager.getRootWindow().location = '".$moWebLib->getSysRefBasedOnTabMain()."login.php';";
    $moEvent = new FWDClientEvent();
    $moEvent->setAttrEvent('onClick');
    $moEvent->setAttrValue($msEvent);
    FWDWebLib::getObject("vb_ok")->addObjFWDEvent($moEvent);
    $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    $moSession->logout();
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    echo "<script language='javascript'>soPopUpManager.getPopUpById('popup_no_access').setCloseEvent( function close_popup_no_access() { ".$msEvent." } );</script>";
    
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_no_access.xml");
?>