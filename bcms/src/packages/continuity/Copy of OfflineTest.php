<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Testes: Plano offline</title>
		<style type="text/css">
			div.observacao{
				height: 150px; 
				width:250px;
			}
			div.data{
				height: 100px; 
				width:100px; 
			}
			
			body{
				font-family: arial;
				font-size: 14px;
				page: landscape;
			}
			
			td{
				border: 1px solid #B64B08;
				border-left: none;
				border-bottom: none;
			}
			
			th{
				text-align: left;
				color: black;
				border-right: 1px solid #B64B08;
			}
			
			table{
				border: 1px #B64B08 solid ;
				border-right: none;
				margin: 10px;
				margin-top: 0px;
			}
			
			td.sigla{
				text-align: center;
			}
			tr{
				background-color: #FEF0E2;
				vertical-align: top;
			}
			tr.cor{
				background-color: #FEE7D0;
			}
			
			div.a4{
				width: 1122px;
				height: 210mm;
			}
		
			div.tableHeader{
				width: 1100px;
				border: 1px solid #B64B08;
				border-bottom: none;
				font-weight: bold;
				font-size: 25px;
				margin-left: 10px;
				margin-top: 10px;
				padding-bottom: 5px;
				padding-top: 5px;
				text-align: center;
			}
				
		</style>
	</head>
	<body>
		<div class="a4">
			<div class="tableHeader">Imediatamente</div>
			<table cellpadding="10px" cellspacing="0px">
				<tr >
					<th>Plano</th>
					<th>Grupo Respons�vel</th>
					<th>Descri��o</th>
					<th>Tempo Estimado</th>
					<th>Hora In�cio</th>
					<th>Hora Fim</th>
					<th>Observa��o</th>
				</tr>
				<tr>
					<td>Call for external agencies</td>
					<td class="sigla">ERT</td>
					<td>Get assistance from external emergency agencies if necessary</td>
					<td>3 Minutos</td>
					<td><div class="data"></div></td>
					<td><div class="data"></div></td>
					<td><div class="observacao"></div></td>
				</tr>
				
				<tr class="cor">
					<td>Collect threat info</td>
					<td class="sigla">SEC</td>
					<td>Receive information from various fonts (phone, mail, direct contact, etc)</td>
					<td>1 Minuto</td>
					<td><div class="data"></div></td>
					<td><div class="data"></div></td>
					<td><div class="observacao"></div></td>
				</tr>
				
				<tr>
					<td>Inform ERT</td>
					<td class="sigla">SEC</td>
					<td>Pass detailed information to Emergency Response Team's leader for appropriate action</td>
					<td>2 Minutos</td>
					<td><div class="data"></div></td>
					<td><div class="data"></div></td>
					<td><div class="observacao"></div></td>
				</tr>
				
				<tr class="cor">
					<td>Contact CMT</td>
					<td class="sigla">ERT</td>
					<td>Inform CMT leaders about incident and actions taken</td>
					<td>2 Minutos</td>
					<td><div class="data"></div></td>
					<td><div class="data"></div></td>
					<td><div class="observacao"></div></td>
				</tr>
				
				<tr>
					<td>Start Evacuation Plan</td>
					<td class="sigla">SEC</td>
					<td>Start evacuation plan if necessary</td>
					<td>3 Minutos</td>
					<td><div class="data"></div></td>
					<td><div class="data"></div></td>
					<td><div class="observacao"></div></td>
				</tr>
			</table>
			
			<div class="tableHeader">De 2 Horas � 6 Horas</div>
			<table cellpadding="10px" cellspacing="0px">
				<tr >
					<th>Plano</th>
					<th>Grupo Respons�vel</th>
					<th>Descri��o</th>
					<th>Tempo Estimado</th>
					<th>Hora In�cio</th>
					<th>Hora Fim</th>
					<th>Observa��o</th>
				</tr>
				<tr>
					<td>Assist external Agencies</td>
					<td class="sigla">SEC</td>
					<td>Assist external Emergency Acencies as required</td>
					<td>2 Horas</td>
					<td><div class="data"></div></td>
					<td><div class="data"></div></td>
					<td><div class="observacao"></div></td>
				</tr>
			</table>
		</div>
	</body>
</html>