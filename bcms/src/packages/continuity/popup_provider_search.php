<?php
include_once "include.php";
include_once $handlers_ref . "select/QueryPlaceProvider.php";
include_once $handlers_ref . "select/QueryGridProviderSearch.php";

class AssociateProviderEvent extends FWDRunnable {
	public function run(){
		$moWebLib = FWDWebLib::getInstance();
		$place = $moWebLib->getObject('placeId')->getValue();

		$msNewProviders = $moWebLib->getObject('current_providers_ids')->getValue();
		if($msNewProviders){
			$maNewProviders = explode(':',$msNewProviders);
		}else{
			$maNewProviders = array();
		}

		if($place) {
			$moQuery = new QueryPlaceProvider(FWDWebLib::getConnection());
			$moQuery->setPlaceId($place);
			$moQuery->makeQuery();
			$moQuery->fetchProviders();
			$maOldProviders = $moQuery->getProviders();

			$maToInsert = array_diff($maNewProviders,$maOldProviders);
			$maToDelete = array_diff($maOldProviders,$maNewProviders);
			$placeProvider = new CMPlaceProvider();
			if(count($maToInsert)) $placeProvider->insertProviders($place, $maToInsert);
			if(count($maToDelete)) $placeProvider->deleteProviders($place, $maToDelete);
		}

		echo "soWindow = soPopUpManager.getPopUpById('popup_provider_search').getOpener();"
		."if (soWindow.refresh_grid)"
		." soWindow.refresh_grid();"
		."if(soWindow.set_providers) soWindow.set_providers(gebi('current_providers_ids').value);"
		."soPopUpManager.closePopUp('popup_provider_search');";
	}
}

class SearchProviderEvent extends FWDRunnable {
	public function run(){
		$moGrid = FWDWebLib::getObject('grid_provider_search');
		$moGrid->execEventPopulate();
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));
		$moStartEvent->addAjaxEvent(new SearchProviderEvent('search_provider_event'));
		$moStartEvent->addAjaxEvent(new AssociateProviderEvent('associate_provider_event'));

		$moSearchGrid = FWDWebLib::getObject('grid_provider_search');
		$moSearchHandler = new QueryGridProviderSearch(FWDWebLib::getConnection());

		$moCurrentGrid = FWDWebLib::getObject('grid_current_providers');
		$moCurrentHandler = new QueryGridProviderSearch(FWDWebLib::getConnection());

		$moSearchHandler->setNameFilter(FWDWebLib::getObject('name_filter')->getValue());

		$removed = FWDWebLib::getObject('removed')->getValue();

		$msCurrentIds = FWDWebLib::getObject('current_providers_ids')->getValue();
		$placeId = FWDWebLib::getObject('param_place_id')->getValue();
		FWDWebLib::getObject('placeId')->setValue($placeId);
		if(!$msCurrentIds && $removed == 'false'){
			if($placeId){
				$msCurrentIds = '0';
				$query = new FWDDBDataSet(FWDWebLib::getConnection());

				$query->setQuery("select fkprovider as id from view_cm_place_provider_active where fkprovider is not null and fkplace = {$placeId}");

				$query->addFWDDBField(new FWDDBField('id','id'  ,DB_NUMBER));
				$query->execute();
				while($query->fetch()){
					$id = $query->getFieldByAlias("id")->getValue();
					$msCurrentIds .= ":".$id;
				}
				FWDWebLib::getObject('current_providers_ids')->setValue($msCurrentIds);
			}
		}

		if($msCurrentIds){
			$maCurrentIds = explode(':',$msCurrentIds);
			$moSearchHandler->setExcludedIds($maCurrentIds);
			$moCurrentHandler->setIds($maCurrentIds);
		}
		else {
			$moCurrentHandler->setEmpty(true);
		}

		$moSearchGrid->setQueryHandler($moSearchHandler);
		$moCurrentGrid->setQueryHandler($moCurrentHandler);
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$paramProvidersIds = FWDWebLib::getObject('param_providers_ids')->getValue();
		if($paramProvidersIds) {
	 	FWDWebLib::getObject('current_providers_ids')->setValue($paramProvidersIds);

		}

		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
<script language="javascript">
  gebi('provider_name').focus();
  function refresh_grid() {    
    gobi('grid_current_providers').setPopulate(true);
    gobi('grid_current_providers').refresh();
  }
	function remove(){
		if(gobi('grid_current_providers').getValue()!=null) {
			if(gobi('grid_current_providers').getValue().join(':').length > 0) {
				var soGridCurrent = gobi('grid_current_providers');
				var soGridSearch = gobi('grid_provider_search');
				var saAll = soGridCurrent.getAllIds();
				var saSel = soGridCurrent.getValue();
				gebi('current_providers_ids').value = saAll.diff(saSel).join(':');
				gebi("removed").value = true;
				soGridCurrent.refresh();
				soGridSearch.refresh();
				soGridCurrent = soGridSearch = saAll = saSel = null;
			}
		}
	}
  function enter_provider_search_event(e)
	{
		if (!e) e = window.event;
		if(e['keyCode']==13) {
      gebi('name_filter').value = gebi('provider_name').value;
      gebi('current_providers_ids').value = gobi('grid_current_providers').getAllIds().join(':');
      gobi('grid_providers_search').setPopulate(true);
	    trigger_event("search_provider_event","3");
		}
	}
	FWDEventManager.addEvent(gebi('provider_name'), 'keydown', enter_provider_search_event);
	
  refresh_grid();
</script>
		<?
	}
}
FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load('popup_provider_search.xml');

?>