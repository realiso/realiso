<?php

include_once 'include.php';
include_once $handlers_ref . "select/QuerySelectAreaType.php";
include_once $handlers_ref . "select/QuerySelectReadableDocumentByContext.php";
include_once $handlers_ref . "grid/QueryGridScopeViewPlaces.php";
include_once $handlers_ref . "grid/QueryGridScopeViewPlans.php";
include_once $handlers_ref . "grid/QueryGridScopeViewAreas.php";
include_once $handlers_ref . "grid/QueryGridScopeViewProcesses.php";
include_once $handlers_ref . "grid/QueryGridScopeViewAssets.php";

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();

		$scopeId = FWDWebLib::getObject('scope')->getValue();
		if($scopeId){
			FWDWebLib::getObject('scopeId')->setValue($scopeId);
		}

		$placeHandler = new QueryGridScopeViewPlaces(FWDWebLib::getConnection());
		$planHandler = new QueryGridScopeViewPlans(FWDWebLib::getConnection());
		$areaHandler = new QueryGridScopeViewAreas(FWDWebLib::getConnection());
		$processHandler = new QueryGridScopeViewProcesses(FWDWebLib::getConnection());
		$assetHandler = new QueryGridScopeViewAssets(FWDWebLib::getConnection());

		$placeHandler->setScope($scopeId);
		$planHandler->setScope($scopeId);
		$areaHandler->setScope($scopeId);
		$processHandler->setScope($scopeId);
		$assetHandler->setScope($scopeId);

		$gridPlaces = FWDWebLib::getObject('gridPlaces');
		$gridPlans = FWDWebLib::getObject('gridPlans');
		$gridAreas = FWDWebLib::getObject('gridAreas');
		$gridProcesses = FWDWebLib::getObject('gridProcesses');
		$gridAssets = FWDWebLib::getObject('gridAssets');

		$gridPlaces->setQueryHandler($placeHandler);
		$gridPlans->setQueryHandler($planHandler);
		$gridAreas->setQueryHandler($areaHandler);
		$gridProcesses->setQueryHandler($processHandler);
		$gridAssets->setQueryHandler($assetHandler);

		$gridPlaces->populate();
		$gridPlans->populate();
		$gridAreas->populate();
		$gridProcesses->populate();
		$gridAssets->populate();

		$moStartEvent->setScreenEvent(new ScreenEvent(''));
	}
}

class GridScope extends FWDDrawGrid {
	public function DrawItem(){
		switch($this->ciColumnIndex){
			default:
				return parent::drawItem();
				break;
		}
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$moWindowTitle = FWDWebLib::getObject('window_title');
		$moWindowTitle->setValue(FWDLanguage::getPHPStringValue('scopeViewTitle', 'Visualizašao de Escopo'));

		$scopeId = FWDWebLib::getObject('scope')->getValue();
		if($scopeId){
			$moScope = new CMScope();
			$moScope->fetchById($scopeId);
			FWDWebLib::getObject('scopeName')->setValue($moScope->getFieldValue('scope_name'));
			FWDWebLib::getObject('scopeDescription')->setValue($moScope->getFieldValue('scope_description'));
		}

		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
<script language="javascript">
      	</script>
		<?
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_scope_view.xml');

?>