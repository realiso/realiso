<?php

include_once 'include.php';
include_once $handlers_ref . "select/QuerySelectResource.php";

class SaveProviderEvent extends FWDRunnable {
	public function run(){
		$providerName = FWDWebLib::getObject('providerName')->getValue();
		$providerDescription = FWDWebLib::getObject('providerDescription')->getValue();
		$providerContract = FWDWebLib::getObject('providerContract')->getValue();
		$providerContractType = FWDWebLib::getObject('providerContractType')->getValue();
		$sla = FWDWebLib::getObject('sla')->getValue();
		$providerId = FWDWebLib::getObject('provider_id')->getValue();
    
		$provider = new CMProvider();
		$provider->setFieldValue('name', $providerName);
		$provider->setFieldValue('description', $providerDescription);
		$provider->setFieldValue('contract', $providerContract);
		$provider->setFieldValue('contractType', $providerContractType);
		$provider->setFieldValue('sla', $sla);

		if($providerId){
			$provider->update($providerId, true, true);
		}else{
			$providerId = $provider->insert(true);
			
			$unId = FWDWebLib::getObject('un_id')->getValue();
		  if($unId){
				$r = new RMAreaProvider();
				$r->setFieldValue('provider_id',$providerId);
				$r->setFieldValue('area_id', $unId); // area = un, corrigir os nomes.
				$r->insert();
			}			
			
		}

		echo "soWindow = soPopUpManager.getPopUpById('popup_provider_edit').getOpener();"
		."if(soWindow.refresh_grid) soWindow.refresh_grid();"
		."soPopUpManager.closePopUp('popup_provider_edit');";
	}
}

class RefreshResourceEvent extends FWDRunnable {
	public function run(){
		$moHandler = new QuerySelectResource(FWDWebLib::getConnection());
		$resourceIds = FWDWebLib::getObject('resources_ids')->getValue();
		if($resourceIds){
			$moHandler->setResourceIds($resourceIds);

			$moSelect = FWDWebLib::getObject('resources');
			$moSelect->setQueryHandler($moHandler);
			$moSelect->populate();
			$moSelect->execEventPopulate();
		}
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(''));
		$moStartEvent->addAjaxEvent(new SaveProviderEvent('SaveProviderEvent'));
		$moStartEvent->addAjaxEvent(new RefreshResourceEvent('refresh_resources'));
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$moWebLib = FWDWebLib::getInstance();
		$moWindowTitle = $moWebLib->getObject('window_title');
    $providerId = $moWebLib->getObject('provider_id')->getValue();
			
		if(!$providerId) {
			$moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_provider_adding', 'Adicionando Fornecedor'));
		} else {
			$moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_provider_editing', 'Editando Fornecedor'));
			$provider = new CMProvider();
			$provider->fetchById($providerId);

			FWDWebLib::getObject('providerName')->setValue($provider->getFieldValue('name'));
			FWDWebLib::getObject('providerDescription')->setValue($provider->getFieldValue('Description'));
			FWDWebLib::getObject('providerContract')->setValue($provider->getFieldValue('contract'));
			FWDWebLib::getObject('providerContractType')->setValue($provider->getFieldValue('contractType'));
			FWDWebLib::getObject('sla')->setValue($provider->getFieldValue('sla'));
		}

		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
<script language="javascript">

			function set_resources(resourceIds) {
			  	gebi('resources_ids').value = resourceIds;
			  	resourceIds=null;
			  	trigger_event('refresh_resources',3);
			}	

        gebi('providerName').focus();
        var soListEditor  = new FWDSelect('resources');
    </script>
		<?
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_provider_edit.xml');

?>