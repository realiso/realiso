<?php
include_once "include.php";
include_once $handlers_ref . "select/QueryGridGroupSearch.php";

class GridSearch extends FWDDrawGrid {
  public function DrawItem(){
    switch($this->ciColumnIndex){
      default:
        return parent::drawItem();
      break;
    }
  }
}

class SearchControlEvent extends FWDRunnable {
  public function run() {
    $moGrid = FWDWebLib::getObject('grid_search');
    $moGrid->execEventPopulate();
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new SearchControlEvent("search_event"));
    
    $miGroupId = FWDWebLib::getObject("param_group_id")->getValue();
    
    FWDWebLib::getObject("group_id")->setValue($miGroupId);
    
    $groupIdFilter = FWDWebLib::getObject("groupIdFilter")->getValue();
    
    $moGrid = FWDWebLib::getObject("grid_search");
    $moHandler = new QueryGridGroupSearch(FWDWebLib::getConnection());
    
    if($groupIdFilter){
    	$ids = array();
    	$ids[] = $groupIdFilter;
    	$moHandler->setIds($ids);
    }else{
    	$moHandler->setNameFilter(FWDWebLib::getObject("var_group_name")->getValue());
    }
    
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new GridSearch());
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
?>
<script language="javascript">
  gebi('group_name').focus();
  
	function refresh_grid(id) {
		if(id){
			gebi("groupIdFilter").value = id;
		}
    	gobi('grid_search').setPopulate(true);
    	js_refresh_grid('grid_search');
    	if(id){
			setTimeout(function(){gebi("groupIdFilter").value = null;}, 2000);
		}
  }
    function enter_group_search_event(e)
	{
		if (!e) e = window.event;
		if(e['keyCode']==13) {
	      	gebi('var_group_name').value = gebi('group_name').value;
	      	gobi('grid_search').setPopulate(true);
	    	trigger_event("search_event","3");
		}
	}
	FWDEventManager.addEvent(gebi('group_name'), 'keydown', enter_group_search_event);
</script>
<?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_group_single_search.xml");
?>