<?php 
	require_once('include.php');
	require_once('../../handlers/grid/QueryOfflinePlanTest.php');
	
	$planId = $_GET["plan"];
	$planTestId = $_GET["planTest"];

	$planTest = new CMPlanTest();
	$planTest->fetchById($planTestId);
	
	$query = new QueryOfflinePlanTest(FWDWebLib::getConnection());
	$query->setPlan($planId);
	$query->makeQuery();
	$query->executeQuery();
	
	$plan = null;
	$planDescription = null;
	$actions = null;
	$ranges = array();
	$rangeNames = array();
	$currentRange = null;
	while($query->fetch()){
		$range = $query->getFieldValue("range");
		if($currentRange != $range){
			$currentRange = $range;
			if($actions){
				$ranges[] = $actions;
			}
			$rangeNames[] = QueryOfflinePlanTest::replacePatterns($range);
			$actions = array();
		}
		if(!$plan){
			$plan = $query->getFieldValue("name");
		}
		if(!$planId){
			$planId = $query->getFieldValue("planId");
		}
		if(!$planDescription){
			$planDescription = $query->getFieldValue("planDescription");
		}

		$arr = array();
		$arr[] = $query->getFieldValue("resume");
		$arr[] = $query->getFieldValue("acronym");
		$arr[] = $query->getFieldValue("description");
		$arr[] = $query->getFieldValue("estimate");
		$arr[] = $query->getFieldValue("planActionId");
		$arr[] = $query->getFieldValue("dependsOn");
		$arr[] = $query->getFieldValue("dependsOnId");
		$actions[] = $arr;
	}
	if($actions){
		$ranges[] = $actions;
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
		<meta content="no-cache" http-equiv="Pragma">
		<meta content="Realiso Corp." name="keywords">
		<meta content="noindex, nofollow" name="robots">
		<script>

			var ids = new Array();
			
		</script>
		<title>Preencher resultado dos testes</title>
		<style type="text/css">
			div.remaining{
			}
			div.btn{
				background: none repeat scroll 0pt 0pt rgb(216, 216, 216); 
				border: 1px solid rgb(168, 168, 168); 
				height: 20px; 
				margin-top: 15px;
				cursor: pointer;
			}
			
			div.submit{
				width: 40px;
				margin-left: 1070px;
				margin-bottom: 20px; 
				
			}
			
			div.btn div{
				font-family: Verdana,tahoma,arial,sans-serif; 
				font-size: 8pt;
				text-align: center;
				margin-top: 3px;
			}
			
			div.observacao{
				height: 150px; 
				width:250px;
			}
			
			textarea{
				height: 140px;
				width: 245px;
			}
			
			div.data{
				height: 100px; 
				width:106px; 
			}
			
			div.data input{
				width: 101px; 
				margin-top: 5px;
				background-color: #E1E1E1;
			}
			
			body{
				font-family: arial;
				font-size: 14px;
				page: landscape;
				background-color: #F8F8F8;
			}
			
			td{
				border: 1px solid #B64B08;
				border-left: none;
				border-bottom: none;
			}
			
			th{
				text-align: left;
				color: black;
				border-right: 1px solid #B64B08;
			}
			
			table{
				border: 1px #B64B08 solid ;
				border-right: none;
				margin: 10px;
				margin-top: 0px;
			}
			
			td.sigla{
				text-align: center;
			}
			tr{
				background-color: #FEF0E2;
				vertical-align: top;
			}
			tr.cor{
				background-color: #FEE7D0;
			}
			
			div.a4{
				width: 1122px;
				height: 210mm;
			}
		
			div.tableHeader{
				width: 1090px;
				border: 1px solid #B64B08;
				border-bottom: none;
				font-weight: bold;
				font-size: 25px;
				margin-left: 10px;
				margin-top: 10px;
				padding-bottom: 5px;
				padding-top: 5px;
				padding-left: 10px;
			}
				
			#atualizando{
				position: fixed	; 
				background-color: red;
				font-size: 12pt;
				font-weight: bold;
				color: white;
			}
		</style>
	</head>
	<body>
		<div id="atualizando" style="display:none;">Salvando...</div>
		<div class="a4">
			<form id="form" action="OnlineTestSave.php" method="post">
				<input type="hidden" value="<?php echo $planId;?>" name="plan"></input>
				<input type="hidden" value="<?php echo $planTestId;?>" name="planTest"></input>
				<?php if(count($ranges) > 0){ for($i=0;$i < count($ranges);$i++){ ?>
					<div class="tableHeader"><?php echo $rangeNames[$i]; $data = $ranges[$i]; ?></div>
					<table cellpadding="10px" cellspacing="0px">
						<tr>
							<th>Plano</th>
							<th>Grupo Responsável</th>
							<th>Descrição</th>
							<th>Depende de</th>
							<th>Tempo Estimado</th>
							<th>Hora Início</th>
							<th>Hora Fim</th>
							<th>Observação</th>
						</tr>
						<?php 
							$cor = false;
							foreach($data as $row){
								
								$planActionTest = new CMPlanActionTest();

								$planActionId = $row[4];
							
								$planActionTest->createFilter($planTestId, "planTest");
								$planActionTest->createFilter($planActionId, "planAction");
								$planActionTest->select();
								$found = false;
								if($planActionTest->fetch()){
									$found = true;
								}
								$observation = "";
								$startDate = "";
								$startTime = "";
								$endDate = "";
								$endTime = "";
								if($found){
									if($planActionTest->getFieldValue('startTime')){
										$msDateTime = explode(' ', ISMSLib::getISMSDate($planActionTest->getFieldValue('startTime')));
										$startDate = $msDateTime[0];
										$startTime = $msDateTime[1];
									}
									
									if($planActionTest->getFieldValue('endTime')){
										$msDateTime = explode(' ', ISMSLib::getISMSDate($planActionTest->getFieldValue('endTime')));
										$endDate = $msDateTime[0];
										$endTime = $msDateTime[1];
									}
									if($planActionTest->getFieldValue('observation')){
										$observation = $planActionTest->getFieldValue('observation');
									}
								}
								
						?>
						<script>
							ids.push(<?php echo $row[4];?>);
						</script>
						<tr <?php if($cor) echo " class=\"cor\""; $cor = !$cor;?>>
							<td><?php echo $row[0];?></td> 
							<td class="sigla"><?php echo $row[1];?></td>
							<td><?php echo $row[2];?></td>
							<td><?php echo $row[5];?> <input type="hidden" id="dependsOn_<?php echo $row[4];?>" value="<?php echo $row[6];?>"></input> </td>
							<td><?php echo $row[3];?>
							<br/><br/>
								<div class="remaining" style="display:none;" id="remaining_<?php echo $row[4];?>">
									
									Decorrido:<br/><b>
									<div id="remainingValue_<?php echo $row[4];?>">
										2 horas e 15 minutos
									</div>
									</b>
								</div>
							</td>
							<td><div class="data">
								Data
								<input readonly="readonly" maxlength="10" onkeyup="Formatadata(this,event)" type="text" name="startDate_<?php echo $row[4];?>" id="startDate_<?php echo $row[4];?>" <?php if($found) echo "value=\"{$startDate}\"";?>></input>
								Hora
								<input readonly="readonly" OnKeyPress="formatar('##:##', this)" maxlength="5" type="text" name="startHour_<?php echo $row[4];?>" id="startHour_<?php echo $row[4];?>" <?php if($found) echo "value=\"{$startTime}\"";?>></input>
								<?php if(!$startDate){ ?>
									<div class="btn start" onclick="javascript: start(<?php echo $row[4];?>, this);" id="startButton_<?php echo $row[4];?>">
										<div>Iniciar</div>
									</div>
								<?php }?>
							</div></td>
							<td><div class="data">
								Data
								<input readonly="readonly" maxlength="10" onkeyup="Formatadata(this,event)" type="text" name="endDate_<?php echo $row[4];?>" id="endDate_<?php echo $row[4];?>" <?php if($found) echo "value=\"{$endDate}\"";?>></input>
								Hora
								<input readonly="readonly" OnKeyPress="formatar('##:##', this)" maxlength="5" type="text" name="endHour_<?php echo $row[4];?>" id="endHour_<?php echo $row[4];?>" <?php if($found) echo "value=\"{$endTime}\"";?>></input>
								<?php if(!$endDate){ ?>
									<div class="btn end" onclick="javascript: stop(<?php echo $row[4];?>, this);" id="stopButton_<?php echo $row[4];?>">
										<div>Parar</div>
									</div>
								<?php }?>
							</div></td>
							<td><div class="observacao"><textarea name="observation_<?php echo $row[4];?>"><?php if($found) echo $observation;?></textarea></div></td>
						</tr>
						<?php }?>				
					</table>
				<?php }
				}else{
					echo "Nenhuma ação para este plano.";
				}
				?>
				<div style="margin-right: 10px; margin-top: 10px; text-align: right;">
					<span style="vertical-align: top; margin: 10px"><b>Observações gerais</b></span><textarea name="generalObservation"><?php echo $planTest->getFieldValue("observation");?></textarea>
				</div>
				<div class="btn submit" onclick="$get('atualizando').style.display = ''; document.getElementById('form').submit();">
					<div>OK</div>
				</div>
			</form>
		</div>
		<script type="text/javascript">
	
			$get = function (id){
				return document.getElementById(id);
			}
		
			function start(id, el){
				if($get("startDate_"+id).value == ""){
					var now = new Date();
					var date = now.formatDate("dd/MM/yyyy");
					var time = now.formatDate("HH:mm");
	
					$get("startDate_"+id).value = date;
					$get("startHour_"+id).value = time;

					updateRemainingValue(id);

					el.style.display = "none";
				}
			}

			function updateRemainingValue(id){
				var date = $get("startDate_"+id).value;
				if(date != ""){
					var endDateStr = $get("endDate_"+id).value;
					var now = new Date();
					if(endDateStr != ""){
						var hourEnd = $get("endHour_"+id).value;
						var mesFim = endDateStr.substring(3,5);
						if(mesFim.substring(0,1) == "0"){
							mesFim = parseInt(mesFim.substring(1,2))-1;
						}else{
							mesFim = parseInt(mesFim)-1;
						}
						now = new Date(endDateStr.substring(6,10), mesFim, endDateStr.substring(0,2), hourEnd.substring(0,2), hourEnd.substring(3,5));
					}
					
					$get("remaining_"+id).style.display = "";
					var hour = $get("startHour_"+id).value;
					var mes = date.substring(3,5);
					if(mes.substring(0,1) == "0"){
						mes = parseInt(mes.substring(1,2))-1;
					}else{
						mes = parseInt(mes)-1;
					}
					var startDate = new Date(date.substring(6,10), mes, date.substring(0,2), hour.substring(0,2), hour.substring(3,5));
					
					var dif = (now.getTime() - startDate.getTime()) / 1000 / 60;

					var time = getTime(dif);
					
					$get("remainingValue_"+id).innerHTML = time;
				}
			}

			var ultimaAtualizacao = new Date();
			updateDates();

			hideStartButton4dependents();
			
			function hideStartButton4dependents(){
				for(var i = 0; i < ids.length; i++){
					var innerId = ids[i];
					var dependsOn = $get("dependsOn_"+innerId).value;
					if(dependsOn != ""){
						var date = $get("endDate_"+dependsOn).value;
						if(date == ""){
							var start = $get("startButton_"+innerId);
							if(start)
								start.style.display="none";
							var end = $get("stopButton_"+innerId);
							if(end)
								end.style.display="none";
						}
					}
				}
			}
			
			function updateDates(){
				for(var i = 0; i < ids.length; i++){
					var id = ids[i];
					updateRemainingValue(id);
				}

				var now = new Date();
				var dif = (now.getTime() - ultimaAtualizacao.getTime()) / 1000;
				if(dif > 60){
					//Atualiza a página e salva os valores
					$get("atualizando").style.display = "";
					$get("form").submit();
				}else{
					setTimeout(updateDates, 5000);
				}
			}

			function getTime(time){
				var result = "";
				if(time > 1440){
					//Dias
					var dia = Math.floor(time / 1440);
					if(dia == 1)
						result += horas + " dia, ";
					else
						result += dia + " dias, ";
					
					var resto = time % 1440;
					if(resto > 60){
						var horas = Math.floor(resto / 60);
						if(horas == 1)
							result += horas + " hora, ";
						else
							result += horas + " horas, ";
						resto = time % 60;
						var min = Math.floor(resto);
						if(min == 1)
							result += min+" minuto";
						else
							result += min+" minutos";
					}else{
						var min = Math.floor(resto);
						if(min == 1)
							result += min+" minuto";
						else
							result += min+" minutos";
					}
				}else{
					if(time > 60){
						var horas = Math.floor(time / 60);
						if(horas == 1)
							result += horas + " hora, ";
						else
							result += horas + " horas, ";
						resto = time % 60;
						var min = Math.floor(resto);
						if(min == 1)
							result += min+" minuto";
						else
							result += min+" minutos";
					}else{
						var min = Math.floor(time);
						if(min == 1)
							result += min+" minuto";
						else
							result += min+" minutos";
						
					}
				}
				return result;
			}

			function stop(id, el){
				if($get("startDate_"+id).value != "" && $get("endDate_"+id).value == ""){
					var now = new Date();
					var date = now.formatDate("dd/MM/yyyy");
					var time = now.formatDate("HH:mm");
	
					$get("endDate_"+id).value = date;
					$get("endHour_"+id).value = time;

					el.style.display = "none";

					for(var i = 0; i < ids.length; i++){
						var innerId = ids[i];
						var dependsOn = $get("dependsOn_"+innerId).value;
						if(dependsOn == id){
							var start = $get("startButton_"+innerId);
							if(start)
								start.style.display="";
							var end = $get("stopButton_"+innerId);
							if(end)
								end.style.display="";
						}
					}
				}	
			}
		
			String.repeat = function(chr,count)
			{    
			    var str = ""; 
			    for(var x=0;x<count;x++) {str += chr}; 
			    return str;
			};
			String.prototype.padL = function(width,pad)
			{
			    if (!width ||width<1)
			        return this;   
			 
			    if (!pad) pad=" ";        
			    var length = width - this.length
			    if (length < 1) return this.substr(0,width);
			 
			    return (String.repeat(pad,length) + this).substr(0,width);    
			}; 
			String.prototype.padR = function(width,pad)
			{
			    if (!width || width<1)
			        return this;        
			 
			    if (!pad) pad=" ";
			    var length = width - this.length
			    if (length < 1) this.substr(0,width);
			 
			    return (this + String.repeat(pad,length)).substr(0,width);
			};
	
			Date.prototype.formatDate = function(format)
			{
			    var date = this;
			    if (!format)
			      format="dd/MM/yyyy";               
			 
			    var month = date.getMonth() + 1;
			    var year = date.getFullYear();    
			 
			    format = format.replace("MM",month.toString().padL(2,"0"));        
			 
			    if (format.indexOf("yyyy") > -1)
			        format = format.replace("yyyy",year.toString());
			    else if (format.indexOf("yy") > -1)
			        format = format.replace("yy",year.toString().substr(2,2));
			 
			    format = format.replace("dd",date.getDate().toString().padL(2,"0"));
			 
			    var hours = date.getHours();       
			    if (format.indexOf("HH") > -1)
			        format = format.replace("HH",hours.toString().padL(2,"0"));
			    if (format.indexOf("mm") > -1)
			       format = format.replace("mm",date.getMinutes().toString().padL(2,"0"));
			    return format;
			};
		
			function Formatadata(Campo, teclapres)
			{
				var tecla = teclapres.keyCode;
				var vr = new String(Campo.value);
				vr = vr.replace("/", "");
				vr = vr.replace("/", "");
				vr = vr.replace("/", "");
				tam = vr.length + 1;
				if (tecla != 8 && tecla != 8)
				{
					if (tam > 0 && tam < 2)
						Campo.value = vr.substr(0, 2) ;
					if (tam > 2 && tam < 4)
						Campo.value = vr.substr(0, 2) + '/' + vr.substr(2, 2);
					if (tam > 4 && tam < 7)
						Campo.value = vr.substr(0, 2) + '/' + vr.substr(2, 2) + '/' + vr.substr(4, 7);
				}
			}

			function formatar(mascara, documento){
				var i = documento.value.length;
				var saida = mascara.substring(0,1);
		  		var texto = mascara.substring(i);
		  		if (texto.substring(0,1) != saida){
		        	documento.value += texto.substring(0,1);
		  		}
			}	
		</script>
	</body>
</html>