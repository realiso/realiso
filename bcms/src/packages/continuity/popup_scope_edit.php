<?php

include_once 'include.php';
include_once $handlers_ref . "select/QuerySelectAreaType.php";
include_once $handlers_ref . "select/QuerySelectReadableDocumentByContext.php";

function save($poContext,$pbHasSensitiveChanges,$piContextId=0){

	$mbHasPMModule = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(POLICY_MODE);
	if ($mbHasPMModule) {
		$moDocContext = new PMDocContext();
		$moDocContext->createFilter($piContextId,'context_id');
		$moDocContext->select();
		while($moDocContext->fetch()) {
			$moDocContext->updateAutoApprovers($moDocContext->getFieldValue('document_id'));
		}
	}
}

function getScopeFromFields(){
	$moScope = new CMScope();

	$msScopeName = FWDWebLib::getObject('scope_name')->getValue();
	$msAreaDescription = FWDWebLib::getObject('scope_description')->getValue();

	$moScope->setFieldValue('scope_name',$msScopeName);
	$moScope->setFieldValue('scope_description',$msAreaDescription);

	return $moScope;
}

class ConfirmEditEvent extends FWDRunnable {
	public function run(){
		$miScopeId = FWDWebLib::getObject('scope_id')->getValue();
		$moScope = getScopeFromFields();
	}
}

class SaveScopeEvent extends FWDRunnable {
	public function run(){
		$miScopeId = FWDWebLib::getObject('scope_id')->getValue();
		$moScope = getScopeFromFields();

		$search = new CMScope();
		$search->execute("SELECT fkContext as scope_id, sname as scope_name, sdescription as scope_description FROM view_cm_scope_active WHERE lower(sname) = '"
		.strtolower($moScope->getFieldValue('scope_name'))."'");

		while($search->fetch()) {
			if($miScopeId){
				if($search->getFieldValue('scope_id') != $miScopeId){
					echo "gobi('warning_duplicated_scope').show();";
					return;
				}
			}else{
				echo "gobi('warning_duplicated_scope').show();";
				return;
			}
		}

		if($miScopeId){
			$moScope->update($miScopeId,true,true);
		}else{
			$piContextId = $moScope->insert(true);
		}

		$moConfig = new ISMSConfig();
		if($moConfig->getConfigFromDB(CLIENT_ID_ASAAS)) {
		     $moISMSASaaS = new ISMSASaaS();
		     $moISMSASaaS->setNewCheckPointASaaS(utf8_encode("Inseriu Novo Escopo"), utf8_encode("M�dulo BCMS"));
		}

		echo "soWindow = soPopUpManager.getPopUpById('popup_scope_edit').getOpener();"
		."if(soWindow.refresh_grid) soWindow.refresh_grid();"
		."soPopUpManager.closePopUp('popup_scope_edit');";
	}
}


class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(''));
		$moStartEvent->addAjaxEvent(new SaveScopeEvent('save_scope_event'));
		//    $moStartEvent->addAjaxEvent(new ConfirmEditEvent('confirm_edit'));
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$miScopeId = FWDWebLib::getObject('param_scope_id')->getValue();
		FWDWebLib::getObject('scope_id')->setValue($miScopeId);
		$moWindowTitle = FWDWebLib::getObject('window_title');

		if($miScopeId){
			$moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_scope_editing', 'Edi��o de Escopo'));

			$moScope = new CMScope();
			$moScope->fetchById($miScopeId);
			FWDWebLib::getObject('scope_name')->setValue($moScope->getFieldValue('scope_name'));

			FWDWebLib::getObject('scope_description')->setValue($moScope->getFieldValue('scope_description'));

		}else{
			//teste para verificar se o sistema n�o est� sendo hakeado
			$moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_scope_adding', 'Adi��o de Escopo'));
		}

		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
<script language="javascript">
        gebi('scope_name').focus();
        
      </script>
		<?
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_scope_edit.xml');

?>