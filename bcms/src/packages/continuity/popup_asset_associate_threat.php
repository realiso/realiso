<?php
include_once "include.php";
include_once $handlers_ref . "QueryThreatsByClass.php";

set_time_limit(300);

class GridThreats extends FWDDrawGrid {
  public function DrawItem(){
    switch($this->ciColumnIndex){
      case 1:
        $msGfxRef = FWDWebLib::getInstance()->getGfxRef();
        $moIcon = new FWDIcon(new FWDBox(4,12));
        $moIcon->setAttrSrc("{$msGfxRef}icon-event.gif");
        return $moIcon->draw();
        break;
      
/*      case 2:
        $this->coCellBox->setAttrStringNoEscape("true");
        $msUniqId = uniqid();
        $this->coCellBox->setValue("<a href='javascript:open_visualize(".$this->caData[1].",".CONTEXT_EVENT.",\"".uniqid()."\")'>{$this->caData[2]}</a>");
        return $this->coCellBox->draw();
        break; */
      default:
        return parent::drawItem();
        break;
    }
  }
}

class AssociateThreatsEvent extends FWDRunnable {
  public function run() {
    $moGrid = FWDWebLib::getObject('grid_threats');
    
    //
/*    $moCtxUserTest = new RMAsset();
    $moCtxTest = new RMAsset();
    $moCtxTest->fetchById(FWDWebLib::getObject("param_asset_id")->getValue());
    //
    $moCtxUserTest->testPermissionToEdit(FWDWebLib::getObject("param_asset_id")->getValue(),$moCtxTest->getResponsible()); */
    
    $moQuery = new QueryThreatsByClass(FWDWebLib::getConnection());
    $moQuery->setClass(CONTEXT_ASSET);
    $moQuery->makeQuery();
    $moQuery->executeQuery();
  
    $maThreats = FWDWebLib::fwd_array_diff_key($moQuery->getThreats(),array_flip($moGrid->getValue()));
    
    $miAssetId = FWDWebLib::getObject("param_asset_id")->getValue();
    
    foreach ($maThreats as $miThreatId => $maValues) {
      $moThreat = new CMAssetThreat();
      $moThreat->setFieldValue("asset_id", $miAssetId);
      $moThreat->setFieldValue("threat_id", $miThreatId);
      $moThreat->insert();
    }
    
    echo "soWindow = soPopUpManager.getPopUpById('popup_asset_associate_threat').getOpener();"
        ."if (soWindow) {"
        ."  if (soWindow.refresh_grid)"
        ."    soWindow.refresh_grid();"
        ."}"
        ."soPopUpManager.closePopUp('popup_asset_associate_threat');";
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent("")); 
    $moStartEvent->addAjaxEvent(new AssociateThreatsEvent("associate_threats_event"));

    $miAssetId = FWDWebLib::getObject("var_asset_id")->getValue();
    if(!$miAssetId)
      $miAssetId = FWDWebLib::getObject("param_asset_id")->getValue();  
    
    $moGrid = FWDWebLib::getObject("grid_threats");
    $moGrid->setObjFwdDrawGrid(new GridThreats());
    
    $moQuery = new QueryThreatsByClass(FWDWebLib::getConnection());
    $moQuery->setClass(CONTEXT_ASSET);
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    $miRow = 1;
    foreach ($moQuery->getThreats() as $miThreatId => $maValues) {
      $moGrid->setItem(1,$miRow,$miThreatId);
      $moGrid->setItem(2,$miRow,$maValues['threat_name']);
      $moGrid->setItem(3,$miRow++,$maValues['threat_description']);
    }


  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $miAssetId = FWDWebLib::getObject("param_asset_id")->getValue();
    if($miAssetId)
      FWDWebLib::getObject('var_asset_id')->setValue($miAssetId);
    
    $moCtxUserTest = new RMAsset();
    $moAsset = new RMAsset();
    $moAsset->fetchById($miAssetId);
    
    $moCategory = new RMCategory();
    $moCategory->fetchById($moAsset->getFieldValue("asset_category_id"));

    $moCtxUserTest->testPermissionToEdit($miAssetId,$moAsset->getResponsible());

    FWDWebLib::getObject("asset_name")->setValue($moAsset->getFieldValue("asset_name"));
    FWDWebLib::getObject("category_name")->setValue($moCategory->getFieldValue("category_name"));
    
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
?>
<script language="javascript">
  function refresh_grid() {
    js_refresh_grid('grid_threats');
  }
</script>
<?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_asset_associate_threat.xml");
?>