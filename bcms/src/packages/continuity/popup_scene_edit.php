<?php

include_once 'include.php';
include_once $handlers_ref . "select/QuerySelectAreaType.php";
include_once $handlers_ref . "select/QuerySelectActiveImpacts.php";
include_once $handlers_ref . "select/QuerySelectPriority.php";
include_once $handlers_ref . "select/QuerySelectReadableDocumentByContext.php";

include_once $handlers_ref . "QueryCheckSceneName.php";
include_once $handlers_ref . "select/QueryGridSceneNameSearch.php";

/* A funcionalidade deste est� em SaveSceneEvent, para verificar se o nome j� existe! */
/*function save($poContext,$pbHasSensitiveChanges,$piContextId=0){
	if($piContextId){
		$poContext->update($piContextId,true,$pbHasSensitiveChanges);
	}else{
		$piContextId = $poContext->insert(true);
		$poContext->executeDefaultInsert($piContextId);
	}

	echo "soWindow = soPopUpManager.getPopUpById('popup_scene_edit').getOpener();"
	."if(soWindow.refresh_grid) soWindow.refresh_grid();"
	."soPopUpManager.closePopUp('popup_scene_edit');";
}*/

function getSceneFromFields(){
	$moScene = new CMScene();

	$msSceneName = FWDWebLib::getObject('scene_name')->getValue();
	$msSceneDescription = FWDWebLib::getObject('scene_description')->getValue();
	$msSceneConsequence = FWDWebLib::getObject('scene_consequence')->getValue();

	$probability  = FWDWebLib::getObject('probability')->getValue();
	$miSceneRto  = FWDWebLib::getObject('rto_value')->getValue();
	$miSceneRpo  = FWDWebLib::getObject('rpo_value')->getValue();
	$miSceneMtpd = FWDWebLib::getObject('mtpd_value')->getValue();

	$miSceneRtoFlags = FWDWebLib::getObject('rto_radiocontroller')->getValue();
	$miSceneRpoFlags = FWDWebLib::getObject('rpo_radiocontroller')->getValue();
	$miSceneMtpdFlags = FWDWebLib::getObject('mtpd_radiocontroller')->getValue();

	if(substr($miSceneRtoFlags,0,1)==':') $miSceneRtoFlags = substr($miSceneRtoFlags,1);
	if(substr($miSceneRpoFlags,0,1)==':') $miSceneRpoFlags = substr($miSceneRpoFlags,1);
	if(substr($miSceneMtpdFlags,0,1)==':') $miSceneMtpdFlags = substr($miSceneMtpdFlags,1);

	$moScene->setFieldValue('scene_name', $msSceneName);
		
	$moScene->setFieldValue('scene_description', $msSceneDescription);
	$moScene->setFieldValue('scene_consequence', $msSceneConsequence);
	$moScene->setFieldValue('scene_rto', $miSceneRto);
	$moScene->setFieldValue('scene_rpo', $miSceneRpo);
	$moScene->setFieldValue('scene_mtpd', $miSceneMtpd);

	if($probability){
		$moScene->setFieldValue('probability', $probability);
	}else{
		$moScene->setFieldValue('probability', 'null');
	}

	$moScene->setFieldValue('scene_rto_flags', $miSceneRtoFlags);
	$moScene->setFieldValue('scene_rpo_flags', $miSceneRpoFlags);
	$moScene->setFieldValue('scene_mtpd_flags', $miSceneMtpdFlags);

	if(FWDWebLib::getObject('process_id')->getValue())
	$moScene->setFieldValue('scene_process', FWDWebLib::getObject('process_id')->getValue());

	$miScheduleId = $moScene->getFieldValue('seasonality');

	$msSerializedSchedule = FWDWebLib::getObject('var_schedule')->getValue();

	if($msSerializedSchedule){
		$moSchedule = new WKFSchedule();
		$maSchedule = FWDWebLib::unserializeString($msSerializedSchedule);
		unset($maSchedule['schedule_id']);
		$moSchedule->fillFieldsFromArray($maSchedule);
		$miScheduleId = $moSchedule->insertOrUpdate($miScheduleId);
		$moScene->setFieldValue('seasonality', $miScheduleId);
	}		
	return $moScene;
}

class SaveSceneEvent extends FWDRunnable {
	public function run(){
		$miSceneId = FWDWebLib::getObject('scene_id')->getValue();
		$msSceneName = FWDWebLib::getObject('scene_name')->getValue();

		$moScene = getSceneFromFields();
		$pbHasSensitiveChanges = false;

		/* N�o permite que o usu�rio salve com o mesmo nome */
		$moQuery = new QueryCheckSceneName(FWDWebLib::getConnection());
		$moQuery->setSceneName($msSceneName);
		$moQuery->makeQuery();
		$moQuery->fetchSceneName();
		$duplicateName = $moQuery->getSceneName();

		//edicao
		if($miSceneId){

			$msSceneNameTmp = FWDWebLib::getObject('scene_name_tmp')->getValue();

			if($duplicateName == true && $msSceneNameTmp != $msSceneName){
				echo "gobi('warning_scene_name_exists').show();";
				return;
			}

			$moScene->update($miSceneId,true,$pbHasSensitiveChanges);
		}else{
			//echo "$msSceneName $duplicateName";
			if($duplicateName == true){
				echo "gobi('warning_scene_name_exists').show();";
				return;
			}

			$miSceneId = $moScene->insert(true);
			$moScene->executeDefaultInsert($miSceneId);
		}
		

		/*
		if($duplicateName == true){
			echo "gobi('warning_scene_name_exists').show();";
		}else{
			$moScene->setFieldValue('scene_name', $msSceneName);
			//save($moScene,false,$miSceneId);
		*/	

		echo "soWindow = soPopUpManager.getPopUpById('popup_scene_edit').getOpener();"
		."if(soWindow.refresh_grid) soWindow.refresh_grid();"
		."soPopUpManager.closePopUp('popup_scene_edit');";
		
		/*}
		*/
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(''));
		$moStartEvent->addAjaxEvent(new SaveSceneEvent('save_scene_event'));
				
		$defaultFinderSceneName = new DefaultFinder('search_scene_name');
		$defaultFinderSceneName->set('scene_name', new QueryGridSceneNameSearch(FWDWebLib::getConnection()));
		$moStartEvent->addAjaxEvent($defaultFinderSceneName);
		
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$moConfig = new ISMSConfig();
		$moWindowTitle = FWDWebLib::getObject('window_title');
		$miSceneId = FWDWebLib::getObject('param_scene_id')->getValue();
		$miProcessId = FWDWebLib::getObject('param_process_id')->getValue();
		FWDWebLib::getObject('process_id')->setValue($miProcessId);
			
		$moHandler = new QuerySelectPriority(FWDWebLib::getConnection());
		$moSelect = FWDWebLib::getObject('probability');
		$moSelect->setQueryHandler($moHandler);
		$moSelect->populate();
		$moScene = new CMScene();
		$moScene->setFieldValue('scene_process', $miProcessId);

		// Edi��o
		if($miSceneId) {
			FWDWebLib::getObject('scene_id')->setValue($miSceneId);
			$moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_scene_editing', 'Edi��o de Cen�rio'));

			$moScene->fetchById($miSceneId);

			FWDWebLib::getObject('probability')->checkItem($moScene->getFieldValue('probability'));
			FWDWebLib::getObject('scene_name_tmp')->setValue($moScene->getFieldValue('scene_name'));
			FWDWebLib::getObject('scene_name')->setValue($moScene->getFieldValue('scene_name'));
			FWDWebLib::getObject('scene_description')->setValue($moScene->getFieldValue('scene_description'));
			FWDWebLib::getObject('scene_consequence')->setValue($moScene->getFieldValue('scene_consequence'));
			FWDWebLib::getObject('rto_value')->setValue($moScene->getFieldValue('scene_rto'));
			FWDWebLib::getObject('rpo_value')->setValue($moScene->getFieldValue('scene_rpo'));
			FWDWebLib::getObject('mtpd_value')->setValue($moScene->getFieldValue('scene_mtpd'));

			FWDWebLib::getObject('rto_radiocontroller')->setValue($moScene->getFieldValue('scene_rto_flags'));
			FWDWebLib::getObject('rpo_radiocontroller')->setValue($moScene->getFieldValue('scene_rpo_flags'));
			FWDWebLib::getObject('mtpd_radiocontroller')->setValue($moScene->getFieldValue('scene_mtpd_flags'));

		} else {
			$moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_scene_adding', 'Adi��o de Cen�rio'));
			FWDWebLib::getObject('seasonalityIcon')->setAttrSrc(FWDWebLib::getInstance()->getGfxRef()."icon-risk_red.png");
		}

		$msSerializedSchedule = $moScene->getSerializedSeasonality();
		if($msSerializedSchedule){
			FWDWebLib::getObject('var_schedule')->setValue($msSerializedSchedule);
		}else{
			FWDWebLib::getObject('seasonalityIcon')->setAttrSrc(FWDWebLib::getInstance()->getGfxRef()."icon-risk_red.png");
		}
			
		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
<script language="javascript">
        gebi('scene_name').focus();
        function set_schedule(psSerializedSchedule){
			gebi('var_schedule').value = psSerializedSchedule;
		}        
      </script>
		<?
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_scene_edit.xml');

?>