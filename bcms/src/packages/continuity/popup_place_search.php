<?php
include_once "include.php";
include_once $handlers_ref . "QueryPlaceFromProcess.php";
include_once $handlers_ref . "QueryPlaceFromScope.php";
include_once $handlers_ref . "grid/QueryGridScopeSearch.php";
include_once $handlers_ref . "grid/QueryGridPlaceSearch.php";

class GridPlaceSearch extends FWDDrawGrid {
	public function DrawItem(){
		switch($this->ciColumnIndex){
			case 1:
				break;
			case 2:
				return $this->coCellBox->draw();
				break;
			default:
				return parent::drawItem();
				break;
		}
	}
}

class AssociatePlaceEvent extends FWDRunnable {
	public function run(){
		$moWebLib = FWDWebLib::getInstance();
		$miProcessId = $moWebLib->getObject('process_id')->getValue();
		$scopeId = $moWebLib->getObject('scope_id')->getValue();

		$msNewPlaces = $moWebLib->getObject('current_places_ids')->getValue();
		if($msNewPlaces){
			$maNewPlaces = explode(':',$msNewPlaces);
		}else{
			$maNewPlaces = array();
		}
		if($miProcessId) {

			$moQuery = new QueryPlaceFromProcess(FWDWebLib::getConnection());
			$moQuery->setProcessId($miProcessId);
			$moQuery->makeQuery();
			$moQuery->fetchPlaces();
			$maOldPlaces = $moQuery->getPlaces();

			$maToInsert = array_diff($maNewPlaces,$maOldPlaces);
			$maToDelete = array_diff($maOldPlaces,$maNewPlaces);
			$moPlaceProcess = new CMPlaceProcess();
			if(count($maToInsert)) $moPlaceProcess->insertPlaces($miProcessId,$maToInsert);
			if(count($maToDelete)) $moPlaceProcess->deletePlaces($miProcessId,$maToDelete);

		}else if($scopeId) {

			$moQuery = new QueryPlaceFromScope(FWDWebLib::getConnection());
			$moQuery->setScopeId($scopeId);
			$moQuery->makeQuery();
			$moQuery->fetchPlaces();
			$maOldPlaces = $moQuery->getPlaces();

			$maToInsert = array_diff($maNewPlaces,$maOldPlaces);
			$maToDelete = array_diff($maOldPlaces,$maNewPlaces);
			$moScopePlace = new CMScopePlace();
			if(count($maToInsert)) $moScopePlace->insertPlaces($scopeId,$maToInsert);
			if(count($maToDelete)) $moScopePlace->deletePlaces($scopeId,$maToDelete);

		}

		echo "soWindow = soPopUpManager.getPopUpById('popup_place_search').getOpener();"
		."if (soWindow.refresh_grid)"
		." soWindow.refresh_grid();"
		."if(soWindow.set_places) soWindow.set_places(gebi('current_places_ids').value);"
		."soPopUpManager.closePopUp('popup_place_search');";
	}
}

class SearchPlaceEvent extends FWDRunnable {
	public function run(){
		$moGrid = FWDWebLib::getObject('grid_place_search');
		$moGrid->execEventPopulate();
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));
		$moStartEvent->addAjaxEvent(new SearchPlaceEvent('search_place_event'));
		$moStartEvent->addAjaxEvent(new AssociatePlaceEvent('associate_place_event'));

		$moSearchGrid = FWDWebLib::getObject('grid_place_search');
		$moSearchGrid->setObjFwdDrawGrid(new GridPlaceSearch());
		$moSearchHandler = new QueryGridPlaceSearch(FWDWebLib::getConnection());

		$moCurrentGrid = FWDWebLib::getObject('grid_current_places');
		$moCurrentGrid->setObjFwdDrawGrid(new GridPlaceSearch());
		$moCurrentHandler = new QueryGridPlaceSearch(FWDWebLib::getConnection());

		$scopeId = FWDWebLib::getObject('param_scope_id')->getValue();

		$moSearchHandler->setNameFilter(FWDWebLib::getObject('name_filter')->getValue());
		$msCurrentIds = FWDWebLib::getObject('current_places_ids')->getValue();
		$removed = FWDWebLib::getObject('removed')->getValue();
		if(!$msCurrentIds && $removed == 'false'){
			if($scopeId){
				$msCurrentIds = '0';
				$query = new FWDDBDataSet(FWDWebLib::getConnection());
				$query->setQuery("select fkplace as id from cm_scope_place where fkscope = {$scopeId}");
				$query->addFWDDBField(new FWDDBField('id','id'  ,DB_NUMBER));
				$query->execute();
				while($query->fetch()){
					$id = $query->getFieldByAlias("id")->getValue();
					$msCurrentIds .= ":".$id;
				}
				FWDWebLib::getObject('current_places_ids')->setValue($msCurrentIds);
			}
		}
		if($msCurrentIds){
			$maCurrentIds = explode(':',$msCurrentIds);
			$moSearchHandler->setExcludedIds($maCurrentIds);
			$moCurrentHandler->setIds($maCurrentIds);
		}
		else {
			$moCurrentHandler->setEmpty(true);
		}

		$moSearchGrid->setQueryHandler($moSearchHandler);
		$moCurrentGrid->setQueryHandler($moCurrentHandler);
		$moCurrentGrid->populate();
		$moSearchGrid->populate();
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$paramPlacesIds = FWDWebLib::getObject('param_places_ids')->getValue();
		if($paramPlacesIds) {
			FWDWebLib::getObject('current_places_ids')->setValue($paramPlacesIds);
		}
		$miProcessId = FWDWebLib::getObject('param_process_id')->getValue();
		FWDWebLib::getObject('process_id')->setValue($miProcessId);
		$scopeId = FWDWebLib::getObject('param_scope_id')->getValue();
		FWDWebLib::getObject('scope_id')->setValue($scopeId);

		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
<script language="javascript">
	gebi('place_name').focus();
	function refresh_grid() {
		gobi('grid_current_places').setPopulate(true);
		gobi('grid_current_places').refresh();
	}
	function enter_place_search_event(e)
	{
		if (!e) e = window.event;
		if(e['keyCode']==13) {
			gebi('name_filter').value = gebi('place_name').value;
			gebi('current_places_ids').value = gobi('grid_current_places').getAllIds().join(':');
			gobi('grid_places_search').setPopulate(true);
			trigger_event("search_place_event","3");
		}
	}
	function remove(){
		if (gobi('grid_current_places').getValue()!=null) {
			if (gobi('grid_current_places').getValue().join(':').length > 0) {
				var soGridCurrent = gobi('grid_current_places');
				var soGridSearch = gobi('grid_place_search');
				var saAll = soGridCurrent.getAllIds();
				var saSel = soGridCurrent.getValue();
				gebi('current_places_ids').value = saAll.diff(saSel).join(':');
				gebi("removed").value = true;
				soGridCurrent.refresh();
				soGridSearch.refresh();
				soGridCurrent = soGridSearch = saAll = saSel = null;
			}
		}
	}
	function insert(){
		if(gobi('grid_place_search').getValue()!=null) {
			if(gobi('grid_place_search').getValue().join(':').length > 0) {
				var soGridCurrent = gobi('grid_current_places');
				var soGridSearch = gobi('grid_place_search');
				var saNewCurrents = soGridCurrent.getAllIds();
				saNewCurrents = saNewCurrents.concat(soGridSearch.getValue());
				gebi('current_places_ids').value = saNewCurrents.join(':');
				soGridSearch.refresh();
				soGridCurrent.refresh();
				soGridCurrent = soGridSearch = saNewCurrents = null;
			}
		}
	}
	FWDEventManager.addEvent(gebi('place_name'), 'keydown', enter_place_search_event);
</script>
		<?
	}
}
FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load('popup_place_search.xml');
?>