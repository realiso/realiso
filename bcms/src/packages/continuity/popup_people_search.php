<?php
include_once "include.php";
include_once $handlers_ref . "QueryScopeFromProcess.php";
include_once $handlers_ref . "grid/QueryGridPeopleSearch.php";
include_once $handlers_ref . "QueryContextHierarchicalStructure.php";

class GridPeopleSearch extends FWDDrawGrid {
  public function DrawItem(){
    switch($this->ciColumnIndex){
      case 1:
/*        $miProcessValue = $this->caData[1];
        $msProcessColor = RMRiskConfig::getRiskColor($miProcessValue);
        $this->coCellBox->setIconSrc("icon-process_".$msProcessColor.".gif");
        return parent::drawItem(); */
        break;
      case 2:
/*        $miProcessValue = $this->caData[1];
        $msLinkColor = RMRiskConfig::getRiskColor($miProcessValue,true);
        $msLink = "<font color=".$msLinkColor.">".$this->caData[2]."</font>";
        $this->coCellBox->setAttrStringNoEscape("true");
        $this->coCellBox->setValue("<a href='javascript:open_visualize(".$this->caData[4].",".CONTEXT_PROCESS.",\"".uniqid()."\")'>{$msLink}</a>"); */
        return $this->coCellBox->draw();
        break;
      default:
        return parent::drawItem();
        break;
    }
  }
}

class AssociatePeopleEvent extends FWDRunnable {
  public function run(){    
    $moWebLib = FWDWebLib::getInstance();
    $miActivityId = $moWebLib->getObject('activity_id')->getValue();
    
    if($miActivityId) { // se ja foi salvo no banco, temos o ID, entao podemos ja aproveitar e salvar
    
    	$msNewPeople = $moWebLib->getObject('current_people_ids')->getValue();
    	if($msNewPeople){
      		$maNewPeople = explode(':',$msNewPeople);
    	}else{
      		$maNewPeople = array();
    	}
    
    	$moQuery = new QueryPeopleFromActivity(FWDWebLib::getConnection());
    	$moQuery->setActivityId($miActivityId);
    	$moQuery->makeQuery();
    	$moQuery->fetchScopes();
    	$maOldPeople = $moQuery->getPeople();
    
    	$maToInsert = array_diff($maNewPeople,$maOldPeople);
    	$maToDelete = array_diff($maOldPeople,$maNewPeople);
    	$moActivityPeople = new CMActivityPeople();
    	if(count($maToInsert)) $moActivityPeople->insertPeople($miActivityId,$maToInsert);
    	if(count($maToDelete)) $moActivityPeople->deletePeople($miActivityId,$maToDelete);

    }
    
    // senao so setamos a array, para que na hora do save do popup de baixo, seja persistida a lista tambem
    echo "soWindow = soPopUpManager.getPopUpById('popup_people_search').getOpener();"
        ."if (soWindow.refresh_grid)"
        ." soWindow.refresh_grid();"
        ."soWindow.set_people(gebi('current_people_ids').value);"
        ."soPopUpManager.closePopUp('popup_people_search');";
        
  }
}

class SearchPeopleEvent extends FWDRunnable {
  public function run(){
    $moGrid = FWDWebLib::getObject('grid_people_search');
    $moGrid->execEventPopulate();
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new SearchPeopleEvent('search_people_event'));    
    $moStartEvent->addAjaxEvent(new AssociatePeopleEvent('associate_people_event'));
    
    $moSearchGrid = FWDWebLib::getObject('grid_people_search');
    $moSearchGrid->setObjFwdDrawGrid(new GridPeopleSearch());
    $moSearchHandler = new QueryGridPeopleSearch(FWDWebLib::getConnection());
    
    $moCurrentGrid = FWDWebLib::getObject('grid_current_people');
    $moCurrentGrid->setObjFwdDrawGrid(new GridPeopleSearch());
    $moCurrentHandler = new QueryGridPeopleSearch(FWDWebLib::getConnection());
    
    $moSearchHandler->setNameFilter(FWDWebLib::getObject('name_filter')->getValue());

    $msCurrentIds = FWDWebLib::getObject('current_people_ids')->getValue();
    
    	
    
    if($msCurrentIds){
      $maCurrentIds = explode(':',$msCurrentIds);
      $moSearchHandler->setExcludedIds($maCurrentIds);
      $moCurrentHandler->setIds($maCurrentIds);
    }
    else {
      $moCurrentHandler->setEmpty(true);
    }
    
    $moSearchGrid->setQueryHandler($moSearchHandler);
    $moCurrentGrid->setQueryHandler($moCurrentHandler);
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
  	$paramPeopleIds = FWDWebLib::getObject('param_people_ids')->getValue();
  	if($paramPeopleIds) {
//  		 $msCurrentIds = FWDWebLib::getObject('current_scopes_ids')->getValue();
//  		 if(!$msCurrentIds) {
	 	FWDWebLib::getObject('current_people_ids')->setValue($paramPeopleIds);
//  		 }
  		 
  	}
//    $miProcessId = FWDWebLib::getObject('param_process_id')->getValue();
//    FWDWebLib::getObject('process_id')->setValue($miProcessId);   	
    //teste para verificar se o sistema n�o est� sendo hakeado
    
/*    $moQuery = new QueryScopeFromProcess(FWDWebLib::getConnection());
    $moQuery->setProcessId(FWDWebLib::getObject('param_process_id')->getValue());
    $moQuery->makeQuery();
    $moQuery->fetchScopes();
    FWDWebLib::getObject('current_scopes_ids')->setValue(implode(":",$moQuery->getScopes())); */
    
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
?>
<script language="javascript">
  gebi('people_name').focus();
  function refresh_grid() {    
    gobi('grid_current_people').setPopulate(true);
    gobi('grid_current_people').refresh();
    //gobi('grid_process_search').setPopulate(true);
    //gobi('grid_process_search').refresh();
  }
  function enter_people_search_event(e)
	{
		if (!e) e = window.event;
		if(e['keyCode']==13) {
      gebi('name_filter').value = gebi('people_name').value;
      gebi('current_people_ids').value = gobi('grid_current_people').getAllIds().join(':');
      gobi('grid_people_search').setPopulate(true);
	    trigger_event("search_people_event","3");
		}
	}
	FWDEventManager.addEvent(gebi('people_name'), 'keydown', enter_people_search_event);
  refresh_grid();
</script>
<?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load('popup_people_search.xml');

?>