<?php

include_once 'include.php';
include_once $handlers_ref . "select/QuerySelectImpactTypes.php";
include_once $handlers_ref . "select/QuerySelectActiveScenes.php";

function saveImpactFromFields($piContext){

	$msImpactName = FWDWebLib::getObject('impact_name')->getValue();
	$miImpactType = FWDWebLib::getObject('select_impact_type')->getValue();

	$moImpact = new CMImpact();
	$moImpact->setFieldValue('impact_name', $msImpactName);
	$moImpact->setFieldValue('impact_type', $miImpactType);
	if($piContext) {
		$moImpact->update($piContext, true, true);
	} else {
		$piContext = $moImpact->insert(true);

		$damageMatrix = new CMDamageMatrix();
		$matrix = $damageMatrix->getDamageMatrix();

		$damageMatrixParameter = new CMDamageMatrixParameter();
		$lowestParameter = $damageMatrixParameter->getLowestDamageParameter();
		
		$financialImpact = new CMFinancialImpact();
		$lowestFinancialImpact = $financialImpact->getLowestFinancialImpact();

		$scene = new QuerySelectActiveScenes();
		$scene->makeQuery();
		$scene->executeQuery();

		while($scene->fetch()){
			$impactDamage = new CMImpactDamage();
			$impactDamage->setFieldValue('impact_id', $piContext);
			$impactDamage->setFieldValue('scene_id', $scene->getFieldValue('fkcontext'));
				
			$idImpactDamage = $impactDamage->insert(true);
			foreach($matrix as $matrixId=>$matrixValue) {
				$damageRange = new CMImpactDamageRange();
				$damageRange->setFieldValue('impact_damage_id', $idImpactDamage);
				$damageRange->setFieldValue('impact_damage_damage_id', $matrixId);
				$damageRange->setFieldValue('impact_damage_parameter_id', $lowestParameter);
				$damageRange->setFieldValue('financial_impact', $lowestFinancialImpact);
				$damageRange->insert(true);
			}
		}
	}
	return $moImpact;
}


class SaveImpactEvent extends FWDRunnable {
	public function run(){
		$miImpactId = FWDWebLib::getObject('impact_id')->getValue();
		saveImpactFromFields($miImpactId);
		echo "soWindow = soPopUpManager.getPopUpById('popup_impact_edit').getOpener();"
		."if(soWindow.refresh_grid) soWindow.refresh_grid();"
		."soPopUpManager.closePopUp('popup_impact_edit');";
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(''));
		$moStartEvent->addAjaxEvent(new SaveImpactEvent('save_impact_event'));
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$moWebLib = FWDWebLib::getInstance();
		$moWindowTitle = $moWebLib->getObject('window_title');
		$miImpactId = $moWebLib->getObject('param_impact_id')->getValue();
			
		$moHandler = new QuerySelectImpactTypes(FWDWebLib::getConnection());
		$moSelect = FWDWebLib::getObject('select_impact_type');
		$moSelect->setQueryHandler($moHandler);

		if(!$miImpactId) {
			$moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_impact_adding', 'Adicionando Impacto'));
		} else {
			$moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_impact_editing', 'Editando Impacto'));
			FWDWebLib::getObject('impact_id')->setValue($miImpactId);
			$moCMImpact = new CMImpact();
			$moCMImpact->fetchById($miImpactId);

			FWDWebLib::getObject('impact_name')->setValue($moCMImpact->getFieldValue('impact_name'));
			$moSelectChecked = new FWDItem();
			$moSelectChecked->setAttrKey($moCMImpact->getFieldValue('impact_type'));

			$moSelect->addObjFWDItem($moSelectChecked);
		}

		$moSelect->populate();

		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
<script language="javascript">
        gebi('impact_name').focus();
    </script>
		<?
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_impact_edit.xml');

?>