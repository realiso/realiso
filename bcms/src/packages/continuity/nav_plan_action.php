<?php
include_once "include.php";
include_once $handlers_ref . "grid/QueryGridPlanAction.php";
include_once $handlers_ref . "select/QuerySelectCategory.php";

class PlanActionConfirmRemove extends FWDRunnable {
	public function run(){
		$msTitle = FWDLanguage::getPHPStringValue('tt_remove_planAction','Remover A��o');
		$msMessage = FWDLanguage::getPHPStringValue('st_planAction_remove_confirm',"Voc� tem certeza que deseja remover a a��o <b>%planAction_name%</b>?");

		$moPlanAction = new CMPlanAction();
		$moPlanAction->fetchById(FWDWebLib::getObject('selected_planAction_id')->getValue());
		$msPlanActionName = ISMSLib::truncateString($moPlanAction->getFieldValue('resume'), 70);
		$msMessage = str_replace("%planAction_name%",$msPlanActionName,$msMessage);

		$msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
                    "soWindow = soPopUp.getOpener();" .
                    "soWindow.removePlanAction();";

		ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue);
	}
}

class RemovePlanActionEvent extends FWDRunnable {
	public function run(){
		$miSelectedId = FWDWebLib::getObject('selected_planAction_id')->getValue();
		$moPlanAction = new CMPlanAction();
		$moPlanAction->delete($miSelectedId);
	}
}

class GridPlanAction extends FWDDrawGrid {
	public function drawItem(){
		switch($this->ciColumnIndex){
			case $this->getIndexByAlias('resume'):
				$desc = $this->getFieldValue('resume');
				$this->coCellBox->setValue("<a href=javascript:openEdit({$this->getFieldValue('id')},{$this->getFieldValue('plan')})><font color='#0000ff'>".$desc."</font></a>");
				return $this->coCellBox->draw();
				break;
			case $this->getIndexByAlias('groupAcronym'):
				$desc = $this->getFieldValue('groupAcronym');
				$this->coCellBox->setValue("<a href=javascript:openEdit({$this->getFieldValue('id')},{$this->getFieldValue('plan')})><font color='#0000ff'>".$desc."</font></a>");
				return $this->coCellBox->draw();
				break;
			case $this->getIndexByAlias('type_order'):
				$desc = $this->getFieldValue('type');
				$this->coCellBox->setValue("<a href=javascript:openEdit({$this->getFieldValue('id')},{$this->getFieldValue('plan')})><font color='#0000ff'>".$desc."</font></a>");
				return $this->coCellBox->draw();
				break;
			case $this->getIndexByAlias('range'):
				$desc = $this->getFieldValue('range');
				if(strstr($desc,"%1%")){
					$desc = str_replace("%1%", " Minutos", $desc);
				}
				if(strstr($desc,"%2%")){
					$desc = str_replace("%2%", " Horas", $desc);
				}
				if(strstr($desc,"%3%")){
					$desc = str_replace("%3%", " Dias", $desc);
				}
				if(strstr($desc,"%4%")){
					$desc = str_replace("%4%", " Meses", $desc);
				}
				$to = FWDLanguage::getPHPStringValue('to',' � ');
				$from = FWDLanguage::getPHPStringValue('from',' De ');
				
				$desc = str_replace("{\$from}", $from, $desc);
				$desc = str_replace("{\$to}", $to, $desc);
				
				$this->coCellBox->setValue("<a href=javascript:openEdit({$this->getFieldValue('id')},{$this->getFieldValue('plan')})><font color='#0000ff'>".$desc."</font></a>");
				return parent::drawItem();
				break;
			default:
				return parent::drawItem();
				break;
		}
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(''));
		$moStartEvent->addAjaxEvent(new PlanActionConfirmRemove('planActionConfirmRemove'));
		$moStartEvent->addAjaxEvent(new RemovePlanActionEvent('remove_planAction_event'));
		$moStartEvent->addAjaxEvent(new ChangeScrollingPath('change_scrolling_path'));

		$plan = FWDWebLib::getObject("plan")->getValue();

		FWDWebLib::getObject("planId")->setValue($plan);

		$grid = FWDWebLib::getObject('grid_planAction');
		$moHandler = new QueryGridPlanAction(FWDWebLib::getConnection());
		$moHandler->setPlan($plan);
		$grid->setQueryHandler($moHandler);
		$grid->setObjFwdDrawGrid(new GridPlanAction());
	}
}

class ChangeScrollingPath extends FWDRunnable{
	public function run(){
		echo ISMSLib::getPathScrollCode(RISK_MANAGEMENT_MODE,CONTEXT_CM_GROUP,FWDWebLib::getObject('root_planAction_id')->getValue(), '', true,'enterPlanAction');
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){

		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
<script language="javascript">

        function refresh_grid(){
            js_refresh_grid('grid_planAction');
        }

       	function removePlanAction(){
  			trigger_event('remove_planAction_event',3);
  			refresh_grid();
  		}
          
        function goToProcess() {
        	parent.soTabSubManager.changeTab(TAB_PROCESS,'nav_process.php');
        }

        function goToArea() {
        	parent.soTabSubManager.changeTab(TAB_AREA,'nav_un.php');
        }

        function goToAsset(id) {
        	parent.soTabSubManager.changeTab(TAB_ASSET,'nav_asset.php');
        }

        function goToPlace() {
        	parent.soTabSubManager.changeTab(TAB_PLACE,'nav_places.php');
        }

        function goToPlan() {
        	parent.soTabSubManager.changeTab(TAB_PLAN,'nav_plan.php');
        }

        function openEdit(id,plan){
        	isms_open_popup("popup_plan_action_edit","packages/continuity/popup_plan_action_edit.php?planAction="+id+"&plan="+plan,"","true");
        }

</script>
		<?
		$plan = FWDWebLib::getObject("plan")->getValue();
		ISMSLib::getPathScrollCode(RISK_MANAGEMENT_MODE, CONTEXT_CM_PLAN_ACTION, $plan, '', false, '');
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_plan_action.xml");
?>