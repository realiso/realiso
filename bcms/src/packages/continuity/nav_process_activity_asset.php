<?php
include_once "include.php";
include_once $handlers_ref . "grid/QueryGridActivityAsset.php";
include_once $handlers_ref . "select/QuerySelectArea.php";
include_once $handlers_ref . "select/QuerySelectProcessPriority.php";
include_once $handlers_ref . "select/QuerySelectProcessType.php";

class SearchEvent extends FWDRunnable {
  public function run(){
    $moGrid = FWDWebLib::getObject('grid_asset');
    $moGrid->execEventPopulate();
  }
}

class GridActivity extends FWDDrawGrid {
  public function drawItem(){
    switch($this->ciColumnIndex){
    	case $this->getIndexByAlias('activity_asset_id'):
    		continue;

      default:
        return parent::drawItem();
        break;
    }
  }
}

class AssetConfirmRemove extends FWDRunnable {
  public function run(){
    $msTitle = FWDLanguage::getPHPStringValue('tt_remove_asset','Remover Ativo');
    $msMessage = FWDLanguage::getPHPStringValue('st_asset_remove_confirm',"Voc� tem certeza que deseja remover o ativo <b>%asset_name%</b> da atividade?");
    
    $moActivityAsset = new CMProcessActivityAsset();
    $moActivityAsset->fetchById(FWDWebLib::getObject('selected_asset_id')->getValue());
    
    $moAsset = new RMAsset();
    $moAsset->fetchById($moActivityAsset->getFieldValue('asset_id'));
    
    $msAssetName = ISMSLib::truncateString($moAsset->getFieldValue('asset_name'), 70);
    $msMessage = str_replace("%asset_name%",$msAssetName,$msMessage);
    
    $msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
                    "soWindow = soPopUp.getOpener();" .
                    "soWindow.remove_asset();";
    
    ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,50);
  }
}

class RemoveAssetEvent extends FWDRunnable {
  public function run(){    
    $miAssetId = FWDWebLib::getObject('selected_asset_id')->getValue();    
    $moActivityAsset = new CMProcessActivityAsset();
    $moActivityAsset->delete($miAssetId);
  }
}


class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new AssetConfirmRemove('asset_confirm_remove'));
    $moStartEvent->addAjaxEvent(new RemoveAssetEvent('remove_asset_event'));
//    $moStartEvent->addAjaxEvent(new AssociateAssetsToProcessEvent('associate_assets_to_process_event'));
    $moStartEvent->addAjaxEvent(new SearchEvent('search'));
//    $moStartEvent->addAjaxEvent(new RedirectToProcessArea('redirect_to_process_area'));
    //instala a seguran�a de ACL na p�gina
    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
    FWDMenuACLSecurity::getInstance()->installMenuACL(FWDWebLib::getObject('menu'),FWDACLSecurity::getInstance());
    
    $moGrid = FWDWebLib::getObject("grid_asset");
    $moHandler = new QueryGridActivityAsset(FWDWebLib::getConnection());
    
    if(!ISMSLib::userHasACL('M.RM.2.1')){
      $moHandler->setUserId(ISMSLib::getCurrentUserId());
    }
    
    $miActivityId = FWDWebLib::getObject('par_asset_activity_id')->getValue();
    if($miActivityId){
      $moHandler->setActivity($miActivityId);
//      FWDWebLib::getObject('area')->setValue($miAreaId);
//      FWDWebLib::getObject('area')->setAttrDisabled('true');
    }else{
//      $moHandler->setProcess(FWDWebLib::getObject('area')->getValue());
    }
    
//    $moHandler->setAsset(FWDWebLib::getObject('par_filter_by_asset')->getValue());
    
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new GridActivity());    
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
  	$miActivityId = FWDWebLib::getObject('par_asset_activity_id')->getValue();
  	FWDWebLib::getObject('asset_activity_id')->setValue($miActivityId);

  	$miProcessId = FWDWebLib::getObject('par_asset_process_id')->getValue();
  	FWDWebLib::getObject('asset_process_id')->setValue($miProcessId);
  	
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ISMSLib::getPathScrollCode(RISK_MANAGEMENT_MODE,CONTEXT_CM_PROCESS_ACTIVITY_ASSET,"teste",false,'enter_area');
    ?>
      <script language="javascript">
      
        function refresh_grid() {
          js_refresh_grid('grid_asset');
        }
        
        function set_assets(assets) {
          gebi('selected_assets_ids').value = assets;
          trigger_event('associate_assets_to_process_event', 3);
          refresh_grid();
        }
        
        function remove_asset() {
          trigger_event('remove_asset_event', 3);
          refresh_grid();
        }
        
        function go_to_nav_asset(piId){
          if(gebi('filter_scrolling').value!=0){
            isms_change_to_sibling(3,'nav_asset.php?scrollfilter='+piId+':'+gebi('filter_scrolling').value);
          }else{
            isms_change_to_sibling(3,'nav_asset.php?scrollfilter='+piId);
          }
        }
        
        function go_to_nav_area(piId){
          gebi('selected_process_area').value = piId;
          trigger_event('redirect_to_process_area',3);
        }

        function go_to_nav_process() {
        	parent.soTabSubManager.changeTab(TAB_PROCESS,'nav_process.php');
        }

        function go_to_nav_activity() {
        	parent.soTabSubManager.changeTab(TAB_PROCESS,'nav_process_activity.php?activity_process_id='+gebi('asset_process_id').value);
        }

        

        function go_to_nav_activity_asset(piActivityId){
            isms_change_to_sibling(TAB_PROCESS,'nav_process_activity_asset.php?activity_id='+piActivityId);
        }
        
        
      </script>
    <?
          
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_process_activity_asset.xml");
?>