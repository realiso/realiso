<?php
include_once "include.php";
include_once $handlers_ref . "grid/QueryGridPlace.php";
include_once $handlers_ref . "QueryThreatsAssociatedByPlace.php";

class SearchEvent extends FWDRunnable {
	public function run(){
		$moGrid = FWDWebLib::getObject('grid_asset');
		$moGrid->execEventPopulate();
	}
}

class ConfirmRemove extends FWDRunnable {
	public function run(){
		$msTitle = FWDLanguage::getPHPStringValue('tt_remove_place','Remover Lugar');
		$msMessage = FWDLanguage::getPHPStringValue('st_place_remove_confirm',"Voc� tem certeza que deseja remover o local <b>%place_name%</b>?");

		$moPlace = new CMPlace();
		$moPlace->fetchById(FWDWebLib::getObject('selected_asset_id')->getValue());
		$msPlaceName = ISMSLib::truncateString($moPlace->getFieldValue('place_name'), 70);
		$msMessage = str_replace("%place_name%",$msPlaceName,$msMessage);

		$msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
                    "soWindow = soPopUp.getOpener();" .
                    "soWindow.remove();";

		ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue);
	}
}

class RemoveEvent extends FWDRunnable {
	public function run(){
		$miSelectedId = FWDWebLib::getObject('selected_asset_id')->getValue();

		$cmPlace = new CMPlace();
    	$cmPlace->delete($miSelectedId);
	}
}

class GridPlace extends FWDDrawGrid {
	public function drawItem(){
		switch($this->ciColumnIndex){
			case $this->getIndexByAlias('place_id'):
				break;
			default:
				return parent::drawItem();
				break;
		}
	}
}

class DuplicateEvent extends FWDRunnable {
	public function run() {
		$miAssetId = FWDWebLib::getObject('selected_asset_id')->getValue();
		$moAsset = new RMAsset();
		$moAsset->fetchById($miAssetId);
		$msNewAssetName = $moAsset->getFieldValue('asset_name') .' - '.FWDLanguage::getPHPStringValue('st_asset_copy','C�pia');

		$moNewAsset = new RMAsset();
		$moNewAsset->setFieldValue('asset_security_responsible_id',$moAsset->getFieldValue('asset_security_responsible_id'));
		$moNewAsset->setFieldValue('asset_responsible_id',$moAsset->getFieldValue('asset_responsible_id'));
		$moNewAsset->setFieldValue('asset_category_id',$moAsset->getFieldValue('asset_category_id'));
		$moNewAsset->setFieldValue('asset_name',$msNewAssetName,false);
		$moNewAsset->setFieldValue('asset_description',$moAsset->getFieldValue('asset_description'),false);
		$moNewAsset->setFieldValue('asset_document',$moAsset->getFieldValue('asset_document'));
		$moNewAsset->setFieldValue('asset_cost',$moAsset->getFieldValue('asset_cost'));
		$moNewAsset->setFieldValue('asset_legality',$moAsset->getFieldValue('asset_legality'));
		$moNewAsset->setFieldValue('asset_justification',$moAsset->getFieldValue('asset_justification'),false);
		$miNewAssetId = $moNewAsset->insert(true);

		$moRisk = new RMRisk();
		$moRisk->createFilter($miAssetId,'risk_asset_id');
		$moRisk->select();
		while($moRisk->fetch()) {
			if ($moRisk->getContextState($moRisk->getFieldValue('risk_id'))!=CONTEXT_STATE_DELETED) {
				$moRisk->duplicateRiskOnAsset($miNewAssetId);
			}
		}
		echo "refresh_grid(); isms_open_popup('popup_asset_edit','packages/risk/popup_asset_edit.php?asset={$miNewAssetId}','','true');";
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(''));
		$moStartEvent->addAjaxEvent(new ConfirmRemove('confirm_remove'));
		$moStartEvent->addAjaxEvent(new RemoveEvent('remove_event'));
		$moStartEvent->addAjaxEvent(new SearchEvent('search'));
		$moStartEvent->addAjaxEvent(new DuplicateEvent('duplicate_event'));

		FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
		FWDMenuACLSecurity::getInstance()->installMenuACL(FWDWebLib::getObject('menu_place'),FWDACLSecurity::getInstance());

		$moGrid = FWDWebLib::getObject('grid_place');
		$moHandler = new QueryGridPlace(FWDWebLib::getConnection());

		$miSessionProcessId = FWDWebLib::getObject("session_process_id")->getValue();
		if ($miSessionProcessId){
			FWDWebLib::getObject('process_id')->setValue($miSessionProcessId);
			$moProcess = new RMProcess();
			if($moProcess->fetchById($miSessionProcessId)){
				FWDWebLib::getObject('filter_scrolling')->setValue($miSessionProcessId);
				$miAreaId = $moProcess->getFieldValue('process_area_id');
				if($miAreaId)
				FWDWebLib::getObject('filter_scrolling')->setValue($miSessionProcessId.':'.$miAreaId);
			}
			else
			FWDWebLib::getObject('filter_scrolling')->setValue($miSessionProcessId);

		}elseif(FWDWebLib::getObject('par_scrolling_filter')->getValue()){
			$maIds = explode(':',FWDWebLib::getObject('par_scrolling_filter')->getValue());
			if(isset($maIds[0]))
			FWDWebLib::getObject('process_id')->setValue($maIds[0]);
			FWDWebLib::getObject('filter_scrolling')->setValue(FWDWebLib::getObject('par_scrolling_filter')->getValue());
		}

		$rootId = FWDWebLib::getObject('root')->getValue();
		if($rootId){
			$moHandler->setParent($rootId);
			FWDWebLib::getObject('root_place_id')->setValue($rootId);
		}

		$moGrid->setQueryHandler($moHandler);
		$moGrid->setObjFwdDrawGrid(new GridPlace());
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
<script language="javascript">
        function refresh_grid(){
            js_refresh_grid('grid_place');
        }

        function remove(){
            trigger_event('remove_event',3);
            refresh_grid();
        }

        function nav_place_open_popup(psId,psPath,psDummy,psModal,piHeight,piWidth){
          setTimeout(
            function(){
              isms_open_popup(psId,psPath,'',psModal,piHeight,piWidth);
              psId = psPath = psDummy = psModal = piHeight = piWidth = null;
            },
            1
          );
        }
          
        function nav_risk_open_popup(psId,psPath,psDummy,psModal,piHeight,piWidth){
          isms_open_popup(psId,psPath,'',psModal,piHeight,piWidth);
          psId = psPath = psDummy = psModal = piHeight = piWidth = null;
        }

        function go_to_nav_risk(piId){
            isms_change_to_sibling(TAB_PLACE,'nav_place_threat.php?threat_place_id='+piId);
        }

        function go_to_nav_process(piId){
          isms_change_to_sibling(TAB_PROCESS,'nav_process.php?asset_id='+piId);
        }

        function goToPlans(place){
            isms_change_to_sibling(TAB_PLAN,'nav_plan.php?place='+place);
      	}

        function goToScope(){
            isms_change_to_sibling(TAB_SCOPE,'nav_scope.php');
      	}

        function goToResources(place, cmt){
            var param = '';
            if(cmt){
				param = '&cmt=1';
            }
            isms_change_to_sibling(TAB_RESOURCE,'nav_resources.php?place='+place+param);
      	}

        function goToUN(placeId){
            isms_change_to_sibling(TAB_AREA,'nav_un.php?place='+placeId);
      	}
        
        function go_to_nav_asset_dependents(piId){
          isms_change_to_sibling(3,'nav_asset.php?asset_id='+piId+'&asset_filter_type=dependents');
        }
        
        function go_to_nav_asset_depencencies(piId){
          isms_change_to_sibling(3,'nav_asset.php?asset_id='+piId+'&asset_filter_type=dependencies');
        }
        
        function goToAssets(place){
        	isms_change_to_sibling(TAB_ASSET,'nav_asset.php?place='+place);
		}
		
        function enterPlace(place_id) {          
        	isms_change_to_sibling(TAB_PLACE, 'nav_places.php?root='+place_id);
        }

        function associatePlaces(){
        	isms_open_popup('popup_place_search','packages/continuity/popup_place_search.php?scope='+gebi('scopeId').value,'','true');
        }

        function set_places(ids){
        }

        /*if(gebi("scopeId").value != ""){
        	js_show("associate_scope");
        	js_hide("vb_insert");
        }else{*/
        	js_hide("associate_scope");
        	js_show("vb_insert");
        /*}*/

        function associateAreas(id){
        	isms_open_popup('popup_area_search','packages/continuity/popup_area_search.php?place='+id,'','true');
        }        
      </script>
		<?
		$rootId = FWDWebLib::getObject('root')->getValue();
		if($rootId){
			ISMSLib::getPathScrollCode(RISK_MANAGEMENT_MODE, CONTEXT_CM_PLACE, $rootId, "Sub-Locais", false, "");
		}else{
			ISMSLib::getPathScrollCode(RISK_MANAGEMENT_MODE, CONTEXT_CM_PLACE, 0, '', false, '');
		}
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_places.xml");
?>
