<?php
require_once('include.php');
require_once('../../handlers/grid/QueryOfflinePlanTest.php');

class OfflineTestPDF extends FPDF
{

	function printReport($planId){

		$query = new QueryOfflinePlanTest(FWDWebLib::getConnection());
		$query->setPlan($planId);
		$query->makeQuery();
		$query->executeQuery();

		$plan = null;
		$planDescription = null;
		$actions = null;
		$ranges = array();
		$rangeNames = array();
		$currentRange = null;
		while($query->fetch()){
			$range = $query->getFieldValue("range");
			if($currentRange != $range){
				$currentRange = $range;
				if($actions){
					$ranges[] = $actions;
				}
				$rangeNames[] = QueryOfflinePlanTest::replacePatterns($range);
				$actions = array();
			}
			if(!$plan){
				$plan = $query->getFieldValue("name");
			}
			if(!$planDescription){
				$planDescription = $query->getFieldValue("planDescription");
			}

			$arr = array();
			$arr[] = $query->getFieldValue("resume");
			$arr[] = $query->getFieldValue("acronym");
			$arr[] = $query->getFieldValue("description");
			$arr[] = $query->getFieldValue("estimate");
			$arr[] = "";
			$arr[] = "";
			$arr[] = "espacoEmBranco espacoEmBranco espacoEmBranco espacoEmBranco espacoEmBranco espacoEmBranco espacoEmBranco espacoEmBranco espacoEmBranco espacoEmBranco espacoEmBranco
			espacoEmBranco espacoEmBranco espacoEmBranco espacoEmBranco espacoEmBranco espacoEmBranco ";
			$actions[] = $arr;
		}
		$ranges[] = $actions;
		$pdf=new OfflineTestPDF('l');
		$pdf->Open();
		$pdf->AddPage();

		$header=array('A��o','Grupo Respons�vel','Descri��o','Tempo Estimado', 'Hora in�cio', 'Hora fim', 'Observa��o');

		$pdf->SetFillColor(213,213,213);
		$pdf->SetDrawColor(128,0,0);
		$pdf->SetLineWidth(.3);

		$pdf->SetFont('Arial','B',20);
		$pdf->Cell(280, 10, $plan);
		$pdf->Ln(10);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(280, 10, $planDescription);
		$pdf->Ln(10);

		$pdf->Ln(10);
		$pdf->SetFont('Arial','B',20);
		$pdf->Cell(150, 10, "In�cio: "."_____________________________");
		$pdf->Cell(150, 10, "Fim: "."_____________________________");
		$pdf->Ln(20);

		for($i=0;$i<count($ranges);$i++){
			$pdf->SetFont('Arial','B',17);
			$pdf->SetTextColor(0);
			$pdf->Cell(280, 10, $rangeNames[$i], 1);
			$pdf->Ln(10);

			$data = $ranges[$i];
			$pdf->createTable($data, $header);
			//			$pdf->Cell(350,35);
			$pdf->Ln(15);
		}

		$pdf->SetTextColor(0);
		$pdf->SetDrawColor(128,0,0);
		$pdf->SetLineWidth(.3);
		$pdf->SetFont('Arial','B',20);
		$pdf->Cell(50, 10, "Observa��es: ");
		$pdf->Cell(150, 10, "___________________________________________________________");
		$pdf->Ln(13);
		$pdf->Cell(150, 10, "_______________________________________________________________________");
		$pdf->Ln(13);
		$pdf->Cell(150, 10, "_______________________________________________________________________");
		$pdf->Ln(13);
		$pdf->Cell(150, 10, "_______________________________________________________________________");
		$pdf->Ln(13);
		$pdf->Cell(150, 10, "_______________________________________________________________________");

		$pdf->Output('Teste Offline.pdf', 'D');
	}

	function createTable($data, $header){
		$this->SetFont('Arial','',12);
		$this->SetFont('','B', 10);
		//Header
		$w=array(40, 35, 50, 35, 25, 25, 70,70);
		$this->SetWidths($w);
		$this->SetAligns(array('J', 'C', 'J','J','J','J','J'));
		$this->SetTextColor(0);
		for($i=0;$i<count($header);$i++){
			$this->Cell($w[$i],7,$header[$i],1,0,'C',true);
		}
		$this->Ln();

		$this->SetFont('Arial','',9);
		//Data
		$dataArr = array();
		foreach($data as $row){
			$this->Row($row);
		}
	}

	function LoadData($file)
	{
		//Read file lines
		$lines=file($file);
		$data=array();
		foreach($lines as $line)
		$data[]=explode(';',chop($line));
		return $data;
	}

	var $widths;
	var $aligns;

	function SetWidths($w)
	{
		//Set the array of column widths
		$this->widths=$w;
	}

	function SetAligns($a)
	{
		//Set the array of column alignments
		$this->aligns=$a;
	}

	function Row($data)
	{
		//Calculate the height of the row
		$nb=0;
		for($i=0;$i< count($data);$i++)
		$nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
		$h=5*$nb;
		//Issue a page break first if needed
		$this->CheckPageBreak($h);
		//Draw the cells of the row
		for($i=0;$i< count($data);$i++)
		{
			$w=$this->widths[$i];
			$a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
			//Save the current position
			$x=$this->GetX();
			$y=$this->GetY();
			//Draw the border
			$this->Rect($x,$y,$w,$h);
			if($i == 6){
				$this->SetTextColor(255);
			}else{
				$this->SetTextColor(0);
			}
			//Print the text
			$this->MultiCell($w,5,$data[$i],0,$a);
			//Put the position to the right of the cell
			$this->SetXY($x+$w,$y);
		}
		//Go to the next line
		$this->Ln($h);
	}

	function CheckPageBreak($h)
	{
		//If the height h would cause an overflow, add a new page immediately
		if($this->GetY()+$h>$this->PageBreakTrigger)
		$this->AddPage($this->CurOrientation);
	}

	function NbLines($w,$txt)
	{
		//Computes the number of lines a MultiCell of width w will take
		$cw=&$this->CurrentFont['cw'];
		if($w==0)
		$w=$this->w-$this->rMargin-$this->x;
		$wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
		$s=str_replace("\r",'',$txt);
		$nb=strlen($s);
		if($nb>0 and $s[$nb-1]=="\n")
		$nb--;
		$sep=-1;
		$i=0;
		$j=0;
		$l=0;
		$nl=1;
		while($i<$nb)
		{
			$c=$s[$i];
			if($c=="\n")
			{
				$i++;
				$sep=-1;
				$j=$i;
				$l=0;
				$nl++;
				continue;
			}
			if($c==' ')
			$sep=$i;
			$l+=$cw[$c];
			if($l>$wmax)
			{
				if($sep==-1)
				{
					if($i==$j)
					$i++;
				}
				else
				$i=$sep+1;
				$sep=-1;
				$j=$i;
				$l=0;
				$nl++;
			}
			else
			$i++;
		}
		return $nl;
	}
}

$idSchedule = isset($_GET["schedule"]) ? $_GET["schedule"] : null;
$id = isset($_GET["plan"]) ? $_GET["plan"] : null;
if(!$id && $idSchedule){
	$schedule = new CMPlanSchedule();
	$schedule->fetchById($idSchedule);
	$id = $schedule->getFieldValue("plan");
}

if($id){
	$p = new OfflineTestPDF();
	$p->printReport($id);
}
?>