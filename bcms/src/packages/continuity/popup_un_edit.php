<?php

include_once 'include.php';
include_once $handlers_ref . "select/QuerySelectAreaType.php";
include_once $handlers_ref . "select/QuerySelectPriority.php";
include_once $handlers_ref . "select/QuerySelectReadableDocumentByContext.php";

function save($poContext,$pbHasSensitiveChanges,$piContextId=0){
	if($piContextId){
		//teste para verificar se o sistema n�o est� sendo hakeado no evento de update
		$moCtxUserTest = new RMArea();
		$moCtxResponsibleTest = new RMArea();
		$moCtxResponsibleTest->fetchById($piContextId);
		$moCtxUserTest->testPermissionToEdit($piContextId,$moCtxResponsibleTest->getResponsible());

		$poContext->update($piContextId,true,$pbHasSensitiveChanges);
	}else{
		$piContextId = $poContext->insert(true);
	}

	$mbHasPMModule = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(POLICY_MODE);
	if ($mbHasPMModule) {
		$moDocContext = new PMDocContext();
		$moDocContext->createFilter($piContextId,'context_id');
		$moDocContext->select();
		while($moDocContext->fetch()) {
			$moDocContext->updateAutoApprovers($moDocContext->getFieldValue('document_id'));
		}
	}

	echo "soWindow = soPopUpManager.getPopUpById('popup_un_edit').getOpener();"
	."if(soWindow.refresh_grid) soWindow.refresh_grid();"
	."soPopUpManager.closePopUp('popup_un_edit');";
}

function getAreaFromFields(){
	$moArea = new RMArea();

	$msAreaName = FWDWebLib::getObject('area_name')->getValue();
	$miAreaResponsible = FWDWebLib::getObject('responsible_id')->getValue();
	$msAreaDescription = FWDWebLib::getObject('area_description')->getValue();
	$miAreaParent = FWDWebLib::getObject('root_area_id')->getValue();
	$miAreaType = FWDWebLib::getObject('select_area_type')->getValue();
	$miAreaPriority = FWDWebLib::getObject('select_area_priority')->getValue();

	if($miAreaParent) $moArea->setFieldValue('area_parent_id', $miAreaParent);
	$moArea->setFieldValue('area_name', $msAreaName);

	$moArea->setFieldValue('area_responsible_id', $miAreaResponsible);
	$moArea->setFieldValue('area_description', $msAreaDescription);
	if($miAreaType)
	$moArea->setFieldValue('area_type', $miAreaType);
	else
	$moArea->setFieldValue('area_type', 'null');

	if($miAreaPriority)
	$moArea->setFieldValue('area_priority', $miAreaPriority);
	else
	$moArea->setFieldValue('area_priority', 'null');

	$mbHasPMModule = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(POLICY_MODE);
	if (!$mbHasPMModule) {
		$msAreaDocument = FWDWebLib::getObject('area_document')->getValue();
		$moArea->setFieldValue('area_document', $msAreaDocument);
	}

	return $moArea;
}

class ConfirmEditEvent extends FWDRunnable {
	public function run(){
		$miAreaId = FWDWebLib::getObject('area_id')->getValue();
		$moArea = getAreaFromFields();
		save($moArea,true,$miAreaId);
	}
}

class SaveAreaEvent extends FWDRunnable {
	public function run(){
		$miAreaId = FWDWebLib::getObject('area_id')->getValue();
		$moArea = getAreaFromFields();
		$moArea->setHash(FWDWebLib::getObject('hash')->getValue());

		$mbHasSensitiveChanges = $moArea->hasSensitiveChanges();
		$miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
		if($miAreaId && $mbHasSensitiveChanges && $miUserId != $moArea->getApprover()){
			$msTitle = FWDLanguage::getPHPStringValue('tt_reapproval','Reaprova��o');
			$msMessage = FWDLanguage::getPHPStringValue('st_sensitive_data_reapproval','Voc� est� alterando dados sens�veis, que necessitam passar por uma reaprova��o. Deseja confirmar as altera��es?');
			$msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
                      "soWindow = soPopUp.getOpener();" .
                      "soWindow.trigger_event('confirm_edit',3);";
			ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,75);
		}else{
			save($moArea,$mbHasSensitiveChanges,$miAreaId);
		}
	}
}

class DocumentViewEvent extends FWDRunnable {
	public function run() {
		$moWebLib = FWDWebLib::getInstance();
		$miDocumentId = $moWebLib->getObject('select_area_document')->getValue();
		$miUserId = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();

		$moDocument = new PMDocument();
		$moDocument->testPermissionToRead($miDocumentId);
		$moDocument->fetchById($miDocumentId);
		$moPMDocInstance = $moDocument->getPublishedInstance();
		$miDocInstanceId = $moPMDocInstance->getFieldValue('doc_instance_id');

		$moReader = new PMDocumentReader();
		$moReader->createFilter($miDocumentId,'document_id');
		$moReader->createFilter($miUserId,'user_id');
		$moReader->select();
		if ($moReader->fetch()) {
			$moReader->setFieldValue('user_has_read_document',1);
			$moReader->update();
		}

		$moPMDocReadHistory = new PMDocReadHistory();
		$moPMDocReadHistory->setFieldValue('user_id',$miUserId);
		$moPMDocReadHistory->setFieldValue('document_id',$miDocInstanceId);
		$moPMDocReadHistory->setFieldValue('read_date',ISMSLib::ISMSTime());
		$moPMDocReadHistory->insert();

		$mbIsLink = (bool)$moPMDocInstance->getFieldValue('doc_instance_is_link');
		if ($mbIsLink) {
			$msURL = $moPMDocInstance->getFieldValue('doc_instance_link');
			if ($msURL)
			echo "js_open('{$msURL}','document_link',740,502,'center_screen');";
		} else {
			echo "js_submit('download_file','ajax');";
		}
	}
}

class DownloadFileEvent extends FWDRunnable {
	public function run() {
		$moWebLib = FWDWebLib::getInstance();
		$miDocumentId = $moWebLib->getObject('select_area_document')->getValue();
		$miUserId = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();

		$moDocument = new PMDocument();
		$moDocument->testPermissionToRead($miDocumentId);
		$moDocument->fetchById($miDocumentId);
		$moPMDocInstance = $moDocument->getPublishedInstance();

		$msPath = $moWebLib->getSysRef()."packages/policy/".$moPMDocInstance->getFieldValue('doc_instance_path');
		$msFileName = $moPMDocInstance->getFieldValue('doc_instance_file_name');
		header('Cache-Control: ');// leave blank to avoid IE errors
		header('Pragma: ');// leave blank to avoid IE errors
		header('Content-type: application/octet-stream');
		header('Content-Disposition: attachment; filename="'.$msFileName.'"');
		$moCrypt = new FWDCrypt();
		$moFP = fopen($msPath,'rb');
		$moCrypt->setIV(fread($moFP,strlen($moCrypt->getIV())));
		while(!feof($moFP)) {
			echo $moCrypt->decryptNoBase64(fread($moFP,16384));
		}
		fclose($moFP);
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(''));
		$moStartEvent->addAjaxEvent(new SaveAreaEvent('save_area_event'));
		$moStartEvent->addAjaxEvent(new ConfirmEditEvent('confirm_edit'));
		$moStartEvent->addAjaxEvent(new DocumentViewEvent('document_view_event'));
		$moStartEvent->addSubmitEvent(new DownloadFileEvent('download_file'));
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$miAreaId = FWDWebLib::getObject('param_area_id')->getValue();
		FWDWebLib::getObject('area_id')->setValue($miAreaId);
		$moWindowTitle = FWDWebLib::getObject('window_title');
		$moIcon = FWDWebLib::getObject('tooltip_icon');

		$moHandler = new QuerySelectAreaType(FWDWebLib::getConnection());
		$moSelect = FWDWebLib::getObject('select_area_type');
		$moSelect->setQueryHandler($moHandler);
		$moSelect->populate();

		$moHandler = new QuerySelectPriority(FWDWebLib::getConnection());
		$moSelect = FWDWebLib::getObject('select_area_priority');
		$moSelect->setQueryHandler($moHandler);
		$moSelect->populate();

		/*
		 * Se possui o m�dulo de documenta��o, troca o campo texto
		 * por select, e mostra um bot�o para o usu�rio visualizar
		 * o documento selecionado. Se n�o houver documento associado,
		 * mostrar mensagem indicando tal situa��o.
		 */
		$mbHasPMModule = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(POLICY_MODE);
		if ($mbHasPMModule) {
			if($miAreaId){
				FWDWebLib::getObject('document_without_policy')->setShouldDraw(false);
				FWDWebLib::getObject('document_with_policy')->setShouldDraw(true);

				$moHandler = new QuerySelectReadableDocumentByContext(FWDWebLib::getConnection());
				$moHandler->setContextId($miAreaId);
				$moSelect = FWDWebLib::getObject('select_area_document');
				$moSelect->setQueryHandler($moHandler);
				$moSelect->populate();

				if (!$moSelect->getValue()) {
					FWDWebLib::getObject('no_published_documents')->setValue(FWDLanguage::getPHPStringValue('st_no_published_documents','N�o h� documentos publicados'));
					FWDWebLib::getObject('no_published_documents')->setAttrDisplay('true');
					FWDWebLib::getObject('area_document_create')->setAttrDisplay('true');
					FWDWebLib::getObject('select_area_document')->setAttrDisplay('false');
					FWDWebLib::getObject('area_document_view')->setShouldDraw(false);
					FWDWebLib::getObject('area_document_properties')->setShouldDraw(false);
				}
			}else{
				FWDWebLib::getObject('document_without_policy')->setShouldDraw(false);
				FWDWebLib::getObject('document_with_policy')->setShouldDraw(false);
				FWDWebLib::getObject('st_area_document')->setShouldDraw(false);
			}

			/* Provisoriamente, como os documentos passaram a ser acessados
			 * a partir do menu de contexto, estou removendo a linha de
			 * documento da tela de edi��o
			 */
			FWDWebLib::getObject('st_area_document')->setShouldDraw(false);
			FWDWebLib::getObject('document_with_policy')->setShouldDraw(false);

		} else {
			FWDWebLib::getObject('document_without_policy')->setShouldDraw(true);
			FWDWebLib::getObject('document_with_policy')->setShouldDraw(false);
		}

		if($miAreaId){
			$moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_un_editing', 'Edi��o de Unidade de neg�cio'));

			$moArea = new RMArea();
			$moArea->fetchById($miAreaId);
			FWDWebLib::getObject('area_name')->setValue($moArea->getFieldValue('area_name'));
			$miResponsibleId = $moArea->getFieldValue('area_responsible_id');

			//teste para verificar se o sistema n�o est� sendo hakeado
			$moCtxUserTest = new RMArea();
			$moCtxUserTest->testPermissionToEdit($miAreaId,$miResponsibleId);

			$moUser = new ISMSUser();
			$moUser->fetchById($miResponsibleId);
			FWDWebLib::getObject('responsible_id')->setValue($miResponsibleId);
			FWDWebLib::getObject('area_responsible')->setValue($moUser->getFieldValue('user_name'));
			FWDWebLib::getObject('area_document')->setValue($moArea->getFieldValue('area_document'));
			FWDWebLib::getObject('area_description')->setValue($moArea->getFieldValue('area_description'));
			FWDWebLib::getObject('select_area_type')->checkItem($moArea->getFieldValue('area_type'));
			FWDWebLib::getObject('select_area_priority')->checkItem($moArea->getFieldValue('area_priority'));

			FWDWebLib::getObject('hash')->setValue($moArea->getHash());

			$moToolTip = ISMSLib::getInstance()->getToolTipCreateModifyById($miAreaId);
			$moToolTip->setAttrWidth(375);
			$moIcon->addObjFWDEvent($moToolTip);
		}else{
			//teste para verificar se o sistema n�o est� sendo hakeado
			$moCtxUserTest = new RMArea();
			$moCtxUserTest->testPermissionToInsert();

			$moIcon->setAttrDisplay('false');
			$moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
			$miUserId = $moSession->getUserId();
			$moUser = new ISMSUser();
			$moUser->fetchById($miUserId);
			FWDWebLib::getObject('responsible_id')->setValue($miUserId);
			FWDWebLib::getObject('area_responsible')->setValue($moUser->getFieldValue('user_name'));
			$moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_un_adding', 'Adi��o de Unidade de Neg�cio'));
		}

		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
<script language="javascript">
        gebi('area_name').focus();
        
        function set_user(id, name){
          gebi('responsible_id').value = id;
          gobi('area_responsible').setValue(name);
        }
       
      </script>
		<?
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_un_edit.xml');

?>