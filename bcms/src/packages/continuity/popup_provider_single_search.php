<?php
include_once "include.php";
include_once $handlers_ref . "select/QueryGridProviderSearch.php";

class GridSearch extends FWDDrawGrid {
  public function DrawItem(){
    switch($this->ciColumnIndex){
      default:
        return parent::drawItem();
      break;
    }
  }
}

class SearchControlEvent extends FWDRunnable {
  public function run() {
    $moGrid = FWDWebLib::getObject('grid_search');
    $moGrid->execEventPopulate();
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new SearchControlEvent("search_event"));
    
    $miProviderId = FWDWebLib::getObject("param_provider_id")->getValue();
    
    FWDWebLib::getObject("provider_id")->setValue($miProviderId);
    
    $moGrid = FWDWebLib::getObject("grid_search");
    $moHandler = new QueryGridProviderSearch(FWDWebLib::getConnection());
    $moHandler->setNameFilter(FWDWebLib::getObject("var_provider_name")->getValue());
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new GridSearch());
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
?>
<script language="javascript">
  gebi('provider_name').focus();
  
  function refresh_grid() {
    gobi('grid_search').setPopulate(true);
    js_refresh_grid('grid_search');
  }
    function enter_provider_search_event(e)
	{
		if (!e) e = window.event;
		if(e['keyCode']==13) {
	      	gebi('var_provider_name').value = gebi('provider_name').value;
	      	gobi('grid_search').setPopulate(true);
	    	trigger_event("search_event","3");
		}
	}
	FWDEventManager.addEvent(gebi('provider_name'), 'keydown', enter_provider_search_event);
</script>
<?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_provider_single_search.xml");
?>