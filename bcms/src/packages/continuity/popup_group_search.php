<?php
include_once "include.php";
include_once $handlers_ref . "select/QueryResourceFromGroup.php";
include_once $handlers_ref . "select/QueryGridGroupSearch.php";

class AssociateGroupEvent extends FWDRunnable {
  public function run(){    
    $moWebLib = FWDWebLib::getInstance();
    $miResourceId = $moWebLib->getObject('resource_id')->getValue();
    
    $msNewGroups = $moWebLib->getObject('current_groups_ids')->getValue();
    if($msNewGroups){
      $maNewGroups = explode(':',$msNewGroups);
    }else{
      $maNewGroups = array();
    }
    
    if($miResourceId) {
    
    	$moQuery = new QueryResourceFromGroup(FWDWebLib::getConnection());
    	$moQuery->setResourceId($miResourceId);
    	$moQuery->makeQuery();
    	$moQuery->fetchGroups();
    	$maOldGroups = $moQuery->getResources();
    
    	$maToInsert = array_diff($maNewGroups,$maOldGroups);
    	$maToDelete = array_diff($maOldGroups,$maNewGroups);
    	$resourceGroup = new CMGroupResource();
    	if(count($maToInsert)) $resourceGroup->insertResources($miResourceId,$maToInsert);
    	if(count($maToDelete)) $resourceGroup->deleteResources($miResourceId,$maToDelete);
    }
    
    echo "soWindow = soPopUpManager.getPopUpById('popup_group_search').getOpener();"
        ."if (soWindow.refresh_grid)"
        ." soWindow.refresh_grid();"
        ."soWindow.set_groups(gebi('current_groups_ids').value);"
        ."soPopUpManager.closePopUp('popup_group_search');";
  }
}

class SearchGroupEvent extends FWDRunnable {
  public function run(){
    $moGrid = FWDWebLib::getObject('grid_group_search');
    $moGrid->execEventPopulate();
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new SearchGroupEvent('search_group_event'));    
    $moStartEvent->addAjaxEvent(new AssociateGroupEvent('associate_group_event'));
    
    $moSearchGrid = FWDWebLib::getObject('grid_group_search');
    $moSearchHandler = new QueryGridGroupSearch(FWDWebLib::getConnection());
    
    $moCurrentGrid = FWDWebLib::getObject('grid_current_groups');
    $moCurrentHandler = new QueryGridGroupSearch(FWDWebLib::getConnection());
    
    $moSearchHandler->setNameFilter(FWDWebLib::getObject('name_filter')->getValue());

    $msCurrentIds = FWDWebLib::getObject('current_groups_ids')->getValue();
    
    if($msCurrentIds){
      $maCurrentIds = explode(':',$msCurrentIds);
      $moSearchHandler->setExcludedIds($maCurrentIds);
      $moCurrentHandler->setIds($maCurrentIds);
    }
    else {
      $moCurrentHandler->setEmpty(true);
    }
    
    $moSearchGrid->setQueryHandler($moSearchHandler);
    $moCurrentGrid->setQueryHandler($moCurrentHandler);
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
  	$paramGroupsIds = FWDWebLib::getObject('param_groups_ids')->getValue();
  	if($paramGroupsIds) {
	 	FWDWebLib::getObject('current_groups_ids')->setValue($paramGroupsIds);
  		 
  	}
    $miResourceId = FWDWebLib::getObject('param_resource_id')->getValue();
    FWDWebLib::getObject('resource_id')->setValue($miResourceId);   	
    
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
?>
<script language="javascript">
  gebi('group_name').focus();
  function refresh_grid() {    
    gobi('grid_current_groups').setPopulate(true);
    gobi('grid_current_groups').refresh();
  }
  function enter_group_search_event(e)
	{
		if (!e) e = window.event;
		if(e['keyCode']==13) {
      gebi('name_filter').value = gebi('group_name').value;
      gebi('current_groups_ids').value = gobi('grid_current_groups').getAllIds().join(':');
      gobi('grid_groups_search').setPopulate(true);
	    trigger_event("search_group_event","3");
		}
	}
	FWDEventManager.addEvent(gebi('group_name'), 'keydown', enter_group_search_event);
  refresh_grid();
</script>
<?
  }
}
FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load('popup_group_search.xml');

?>