<?php
include_once "include.php";
include_once $handlers_ref . "select/QueryGridPlanSearch.php";

class GridSearch extends FWDDrawGrid {
  public function DrawItem(){
    switch($this->ciColumnIndex){
      default:
        return parent::drawItem();
      break;
    }
  }
}

class SearchControlEvent extends FWDRunnable {
  public function run() {
    $moGrid = FWDWebLib::getObject('grid_search');
    $moGrid->execEventPopulate();
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new SearchControlEvent("search_event"));
    
    $miPlanId = FWDWebLib::getObject("param_plan")->getValue();
    
    FWDWebLib::getObject("plan_id")->setValue($miPlanId);
    
    $moGrid = FWDWebLib::getObject("grid_search");
    $moHandler = new QueryGridPlanSearch(FWDWebLib::getConnection());
    $moHandler->setNameFilter(FWDWebLib::getObject("var_plan_name")->getValue());
    $a = array();
    $a[] = $miPlanId;
    $moHandler->setExcludedIds($a);
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new GridSearch());
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
?>
<script language="javascript">
  gebi('plan_name').focus();
  
  function refresh_grid() {
    gobi('grid_search').setPopulate(true);
    js_refresh_grid('grid_search');
  }
    function enter_plan_search_event(e)
	{
		if (!e) e = window.event;
		if(e['keyCode']==13) {
	      	gebi('var_plan_name').value = gebi('plan_name').value;
	      	gobi('grid_search').setPopulate(true);
	    	trigger_event("search_event","3");
		}
	}
	FWDEventManager.addEvent(gebi('plan_name'), 'keydown', enter_plan_search_event);
</script>
<?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_plan_search.xml");
?>