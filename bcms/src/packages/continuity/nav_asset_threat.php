<?php
include_once "include.php";
include_once $handlers_ref . "grid/QueryGridAssetThreat.php";
include_once $handlers_ref . "select/QuerySelectArea.php";

class SearchEvent extends FWDRunnable {
	public function run(){
		$moGrid = FWDWebLib::getObject('grid_asset');
		$moGrid->execEventPopulate();
	}
}

class GridThreat extends FWDDrawGrid {
	public function drawItem(){
		switch($this->ciColumnIndex){
			case $this->getIndexByAlias('threat_id'):
				continue;
			case $this->getIndexByAlias('threat_probability'):
				$moProbability = new CMThreatProbability();
				$moProbability->fetchById($this->getFieldValue('threat_probability'));
				$this->coCellBox->setValue($moProbability->getFieldValue('probability_name'));
				return parent::drawItem();
				break;
			case $this->getIndexByAlias('origin'):
				$origin = $this->getFieldValue('origin');

				if($origin != "asset"){
					$maNotAllowed = array("allMenus");
					$moACL = FWDACLSecurity::getInstance();
					$moACL->setNotAllowed($maNotAllowed);
					$moMenu = FWDWebLib::getObject('menu');
					$moGrid = FWDWebLib::getObject('grid_threat');
					$msLine = $moGrid->getAttrName().'_acl_'.$this->ciRowIndex;
					FWDGridMenuACLSecurity::getInstance()->installGridMenuACL($moMenu,$moACL,$moGrid,$msLine);
				}

				if($origin == "asset"){
					$origin = FWDLanguage::getPHPStringValue('asset','Ativo');
				}else if($origin == "category"){
					$origin = FWDLanguage::getPHPStringValue('asset_library','Biblioteca de ativos');
				}else if($origin == "unknown"){
					$origin = FWDLanguage::getPHPStringValue('asset_unknown','Desconhecido');
				}
				$this->coCellBox->setValue($origin);

				return parent::drawItem();
				break;
			default:
				return parent::drawItem();
				break;
		}
	}
}

class ThreatConfirmRemove extends FWDRunnable {
	public function run(){
		$msTitle = FWDLanguage::getPHPStringValue('remove_threat','Remover Amea�a');
		$msMessage = FWDLanguage::getPHPStringValue('threat_remove_confirm',"Voc� tem certeza que deseja remover a amea�a <b>%threat_name%</b>?");

		$cmThreat = new CMThreat();
		$cmThreat->fetchById(FWDWebLib::getObject('selected_threat_id')->getValue());
		$msThreatName = ISMSLib::truncateString($cmThreat->getFieldValue('threat_name'), 70);
		$msMessage = str_replace("%threat_name%",$msThreatName,$msMessage);

		$msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
                    "soWindow = soPopUp.getOpener();" .
                    "soWindow.remove_threat();";

		ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,50);
	}
}

class AssociateAssetsToAssetEvent extends FWDRunnable {
	public function run(){
		$miAssetId = FWDWebLib::getObject('selected_asset_id')->getValue();
		$maAssets = FWDWebLib::getObject('selected_assets_ids')->getValue();

		$maAssets = explode(",", $maAssets);

		$moAssetAsset = new RMAssetAsset();
		$moAssetAsset->setFieldValue('asset_id', $miAssetId);
		foreach ($maAssets as $miAssetId) {
			$moAssetAsset->setFieldValue('asset_id', $miAssetId);
			$moAssetAsset->insert();
		}
	}
}

class RemoveThreatEvent extends FWDRunnable {
	public function run(){
		$miThreatId = FWDWebLib::getObject('selected_threat_id')->getValue();
		$miAssetId = FWDWebLib::getObject('threat_asset_id')->getValue();

		$cmThreat = new CMThreat();
		$cmThreat->delete($miThreatId);
		$cmAssetThreat = new CMAssetThreat();
		$cmAssetThreat->deleteAssetsThreat($miThreatId, $miAssetId);
	}
}

class RedirectToAssetArea extends FWDRunnable {
	public function run(){
		$moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
		$moSession->deleteAttribute('areaid');
		$moSession->addAttribute('areaid');
		$moSession->setAttrAreaId(FWDWebLib::getObject('selected_asset_area')->getValue());
		echo "isms_change_to_sibling(TAB_SCOPE,'nav_area.php?uniqid=".uniqid()."');";
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));
		$moStartEvent->addAjaxEvent(new ThreatConfirmRemove('threat_confirm_remove'));
		$moStartEvent->addAjaxEvent(new RemoveThreatEvent('remove_threat_event'));
		$moStartEvent->addAjaxEvent(new SearchEvent('search'));
		//instala a seguran�a de ACL na p�gina
		FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
		FWDMenuACLSecurity::getInstance()->installMenuACL(FWDWebLib::getObject('menu'),FWDACLSecurity::getInstance());

		$moGrid = FWDWebLib::getObject("grid_threat");
		$moHandler = new QueryGridAssetThreat(FWDWebLib::getConnection());

		if(!ISMSLib::userHasACL('M.RM.2.1')){
			$moHandler->setUserId(ISMSLib::getCurrentUserId());
		}

		$miAssetId = FWDWebLib::getObject('par_threat_asset_id')->getValue();
		if($miAssetId){
			$moHandler->setAsset($miAssetId);
		}

		$moGrid->setQueryHandler($moHandler);
		$moGrid->setObjFwdDrawGrid(new GridThreat());
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$miAssetId = FWDWebLib::getObject('par_threat_asset_id')->getValue();
			
		FWDWebLib::getObject('threat_asset_id')->setValue($miAssetId);
			
		//grid_asset_name
		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		ISMSLib::getPathScrollCode(RISK_MANAGEMENT_MODE, CONTEXT_CM_ASSET_THREAT, "teste", false, 'enter_area');
		?>
<script language="javascript">
      
        function refresh_grid() {
          js_refresh_grid('grid_threat');
        }
        
        function set_assets(assets) {
          gebi('selected_assets_ids').value = assets;
          trigger_event('associate_assets_to_asset_event', 3);
          refresh_grid();
        }
        
        function remove_threat() {
          trigger_event('remove_threat_event', 3);
          refresh_grid();
        }
        
        function go_to_nav_asset(piId){
          if(gebi('filter_scrolling').value!=0){
            isms_change_to_sibling(3,'nav_asset.php?scrollfilter='+piId+':'+gebi('filter_scrolling').value);
          }else{
            isms_change_to_sibling(3,'nav_asset.php?scrollfilter='+piId);
          }
        }
        
        function go_to_nav_area(piId){
          gebi('selected_asset_area').value = piId;
          trigger_event('redirect_to_asset_area',3);
        }

        function go_to_nav_asset() {
        	parent.soTabSubManager.changeTab('5','nav_asset.php');
        }

        function go_to_nav_impact(piSceneId,piAssetId){
            isms_change_to_sibling(TAB_PROCESS,'nav_asset_impact.php?scene_id='+piSceneId+'&asset_id='+piAssetId);
        }
        
        
      </script>
		<?
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_asset_threat.xml");
?>