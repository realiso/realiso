<?php
include_once "include.php";
include_once $handlers_ref . "grid/QueryGridPlaceThreat.php";
include_once $handlers_ref . "select/QuerySelectArea.php";
include_once $handlers_ref . "select/continuity/QuerySelectPlaceType.php";

class SearchEvent extends FWDRunnable {
	public function run(){
		$moGrid = FWDWebLib::getObject('grid_place');
		$moGrid->execEventPopulate();
	}
}

class GridThreat extends FWDDrawGrid {
	public function drawItem(){
		switch($this->ciColumnIndex){
			case $this->getIndexByAlias('threat_id'):
				continue;
			case $this->getIndexByAlias('origin'):
				$origin = $this->getFieldValue('origin');

				if($origin != "place"){
					$maNotAllowed = array("allMenus");
					$moACL = FWDACLSecurity::getInstance();
					$moACL->setNotAllowed($maNotAllowed);
					$moMenu = FWDWebLib::getObject('menu');
					$moGrid = FWDWebLib::getObject('grid_threat');
					$msLine = $moGrid->getAttrName().'_acl_'.$this->ciRowIndex;
					FWDGridMenuACLSecurity::getInstance()->installGridMenuACL($moMenu,$moACL,$moGrid,$msLine);
				}

				if($origin == "place"){
					$origin = FWDLanguage::getPHPStringValue('place','Local');
				}else if($origin == "category"){
					$origin = FWDLanguage::getPHPStringValue('place_library','Biblioteca de Locais');
				}else if($origin == "inheritance"){
					$origin = FWDLanguage::getPHPStringValue('inheritance','Heran�a');
				}else if($origin == "unknown"){
					$origin = FWDLanguage::getPHPStringValue('place_unknown','Desconhecido');
				}
				$this->coCellBox->setValue($origin);

				return parent::drawItem();
				break;
			default:
				return parent::drawItem();
				break;
		}
	}
}

class ThreatConfirmRemove extends FWDRunnable {
	public function run(){
		$msTitle = FWDLanguage::getPHPStringValue('tt_remove_threat','Remover Amea�a');
		$msMessage = FWDLanguage::getPHPStringValue('st_threat_remove_confirm',"Voc� tem certeza que deseja remover a amea�a <b>%threat_name%</b>?");

		$cmThreat = new CMThreat();
		$cmThreat->fetchById(FWDWebLib::getObject('selected_threat_id')->getValue());
		$msThreatName = ISMSLib::truncateString($cmThreat->getFieldValue('threat_name'), 70);
		$msMessage = str_replace("%threat_name%",$msThreatName,$msMessage);

		$msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
                    "soWindow = soPopUp.getOpener();" .
                    "soWindow.remove_threat();";

		ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,50);
	}
}

class AssociateAssetsToPlaceEvent extends FWDRunnable {
	public function run(){
		$miPlaceId = FWDWebLib::getObject('selected_place_id')->getValue();
		$maAssets = FWDWebLib::getObject('selected_assets_ids')->getValue();

		$maAssets = explode(",", $maAssets);

		$moPlaceAsset = new RMPlaceAsset();
		$moPlaceAsset->setFieldValue('place_id', $miPlaceId);
		foreach ($maAssets as $miAssetId) {
			$moPlaceAsset->setFieldValue('asset_id', $miAssetId);
			$moPlaceAsset->insert();
		}
	}
}

class RemoveThreatEvent extends FWDRunnable {
	public function run(){
		$miThreatId = FWDWebLib::getObject('selected_threat_id')->getValue();
		$miPlaceId = FWDWebLib::getObject('threat_place_id')->getValue();

		$cmThreat = new CMThreat();
		$cmThreat->delete($miThreatId);
		$cmPlaceThreat = new CMPlaceThreat();
		$cmPlaceThreat->deletePlacesThreat($miThreatId, $miPlaceId);
	}
}

class RedirectToPlaceArea extends FWDRunnable {
	public function run(){
		$moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
		$moSession->deleteAttribute('areaid');
		$moSession->addAttribute('areaid');
		$moSession->setAttrAreaId(FWDWebLib::getObject('selected_place_area')->getValue());
		echo "isms_change_to_sibling(TAB_SCOPE,'nav_area.php?uniqid=".uniqid()."');";
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));
		$moStartEvent->addAjaxEvent(new ThreatConfirmRemove('threat_confirm_remove'));
		$moStartEvent->addAjaxEvent(new RemoveThreatEvent('remove_threat_event'));
		$moStartEvent->addAjaxEvent(new SearchEvent('search'));
		//instala a seguran�a de ACL na p�gina
		FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
		FWDMenuACLSecurity::getInstance()->installMenuACL(FWDWebLib::getObject('menu'),FWDACLSecurity::getInstance());

		$moGrid = FWDWebLib::getObject("grid_threat");
		$moHandler = new QueryGridPlaceThreat(FWDWebLib::getConnection());

		if(!ISMSLib::userHasACL('M.RM.2.1')){
			$moHandler->setUserId(ISMSLib::getCurrentUserId());
		}

		$miPlaceId = FWDWebLib::getObject('par_threat_place_id')->getValue();
		if($miPlaceId){
			$moHandler->setPlace($miPlaceId);
		}

		$moGrid->setQueryHandler($moHandler);
		$moGrid->setObjFwdDrawGrid(new GridThreat());
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$miPlaceId = FWDWebLib::getObject('par_threat_place_id')->getValue();
		FWDWebLib::getObject('threat_place_id')->setValue($miPlaceId);
		
		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));

		if($miPlaceId){
			$threatString = FWDLanguage::getPHPStringValue('threats',"Amea�as");
			ISMSLib::getPathScrollCode(RISK_MANAGEMENT_MODE, CONTEXT_CM_PLACE, $miPlaceId, $threatString, false, '');
		}

		?>
<script language="javascript">
      
        function refresh_grid() {
          js_refresh_grid('grid_threat');
        }
        
        function set_assets(assets) {
          gebi('selected_assets_ids').value = assets;
          trigger_event('associate_assets_to_place_event', 3);
          refresh_grid();
        }
        
        function remove_threat() {
          trigger_event('remove_threat_event', 3);
          refresh_grid();
        }
        
        function go_to_nav_asset(piId){
          if(gebi('filter_scrolling').value!=0){
            isms_change_to_sibling(3,'nav_asset.php?scrollfilter='+piId+':'+gebi('filter_scrolling').value);
          }else{
            isms_change_to_sibling(3,'nav_asset.php?scrollfilter='+piId);
          }
        }
        
        function go_to_nav_area(piId){
          gebi('selected_place_area').value = piId;
          trigger_event('redirect_to_place_area',3);
        }

        function go_to_nav_place() {
        	parent.soTabSubManager.changeTab(TAB_PLACE,'nav_places.php');
        }

        function go_to_nav_impact(piSceneId,piPlaceId){
            isms_change_to_sibling(TAB_PROCESS,'nav_place_impact.php?scene_id='+piSceneId+'&place_id='+piPlaceId);
        }

        function openVulnerability(id){
        	isms_open_popup('popup_vulnerability_edit','packages/continuity/popup_vulnerability_edit.php?threat='+id+'&place='+gebi('threat_place_id').value,'','true');
        }

        function enterPlace(place_id) {          
        	parent.soTabSubManager.changeTab(TAB_PLACE,'nav_places.php?root='+place_id);
        }

      </script>
		<?
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_place_threat.xml");
?>