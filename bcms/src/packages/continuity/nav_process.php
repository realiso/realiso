<?php
include_once "include.php";
include_once $handlers_ref . "grid/QueryGridProcess.php";
include_once $handlers_ref . "select/QuerySelectArea.php";
include_once $handlers_ref . "select/QuerySelectProcessPriority.php";
include_once $handlers_ref . "select/QuerySelectProcessType.php";

class GridProcess extends FWDDrawGrid {
  
	private $msCurrency;
	  
	public function __construct(){
	    $this->msCurrency = ISMSLib::getCurrencyString();
	}
    
	public function drawItem(){
		switch($this->ciColumnIndex){
			case $this->getIndexByAlias('process_value'):
				$mbHideEdit = false;
				$mbHideRemove = false;
				$mbHideAssociateAsset = false;

				if ($mbHideEdit || $mbHideRemove || $mbHideAssociateAsset ) {
					$moACL = FWDACLSecurity::getInstance();
					$maDenied = array();
					if($mbHideEdit)
					$maDenied[] = 'perm_to_edit';
					if($mbHideRemove)
					$maDenied[] = 'perm_to_remove';
					if($mbHideAssociateAsset)
					$maDenied[] = 'perm_to_associate_asset';

					$moACL->setNotAllowed($maDenied);
					$moMenu = FWDWebLib::getObject('menu');
					$moGrid = FWDWebLib::getObject('grid_process');
					$msLine = $moGrid->getAttrName().'_acl_'.$this->ciRowIndex;
					FWDGridMenuACLSecurity::getInstance()->installGridMenuACL($moMenu,$moACL,$moGrid,$msLine);
				}

				$miProcessValue = $this->getFieldValue('process_value');
				$msProcessColor = RMRiskConfig::getRiskColor($miProcessValue);
				$this->coCellBox->setIconSrc("icon-process_".$msProcessColor.".gif");
				$this->coCellBox->setObjFWDToolTip(ISMSLib::getInstance()->getToolTipCreateModify($this->getFieldValue('user_create_name'),$this->getFieldValue('date_create'),$this->getFieldValue('user_edit_name'),$this->getFieldValue('date_edit')));
				return parent::drawItem();
				break;

			case $this->getIndexByAlias('process_name'):
				$this->coCellBox->setAttrStringNoEscape("true");
				$this->coCellBox->setValue($this->getNameContent());
				return $this->coCellBox->draw();
				break;

			case $this->getIndexByAlias('financialLoss'):
				$this->coCellBox->setValue($this->msCurrency . ' ' . $this->getFieldValue('financialLoss'));
				return $this->coCellBox->draw();
				break;				
				
			case $this->getIndexByAlias('rto_order'):
			  
    		$minutes = FWDLanguage::getPHPStringValue('rtoMinutes','minutos');
    		$hours = FWDLanguage::getPHPStringValue('rtoHours','horas');
    		$days = FWDLanguage::getPHPStringValue('rtoDays','dias');						  
		
			  $rtoFlag = $this->getFieldValue('rto_flag');

			  $rtoMessage = '';
			  
			  if($rtoFlag == 1){
			    $rtoMessage = FWDLanguage::getPHPStringValue('rtoMinutes','minutos');
			  } else if($rtoFlag == 2){
			    $rtoMessage = FWDLanguage::getPHPStringValue('rtoHours','horas');
			  } else {
			    $rtoMessage = FWDLanguage::getPHPStringValue('rtoDays','dias');
			  }

				$this->coCellBox->setValue($this->getFieldValue('rto') . ' ' . $rtoMessage);
				return $this->coCellBox->draw();
				break;							
				
			default:
				return parent::drawItem();
				break;
		}
	}

	public function getNameContent(){
		$miProcessValue = $this->getFieldValue('process_value');
		$msLinkColor = RMRiskConfig::getRiskColor($miProcessValue,true);
		$msLink = "<font color=".$msLinkColor.">".$this->getFieldValue('process_name')."</a>";

		if(ISMSLib::userHasACL('M.RM.2.2') && ISMSLib::userHasACL('M.RM.3')){
			return "<a href='javascript:isms_open_popup(\"popup_process_edit\",\"packages/continuity/popup_process_edit.php?process={$this->getFieldValue('process_id')}\",\"\",\"true\");'>{$msLink}</a>";
		}
	}
}

class ProcessConfirmRemove extends FWDRunnable {
	public function run(){
		$msTitle = FWDLanguage::getPHPStringValue('tt_remove_process','Remover Processo');
		$msMessage = FWDLanguage::getPHPStringValue('st_process_remove_confirm',"Voc� tem certeza que deseja remover o processo <b>%process_name%</b>?");

		$moProcess = new RMProcess();
		$moProcess->fetchById(FWDWebLib::getObject('selected_process_id')->getValue());
		$msProcessName = ISMSLib::truncateString($moProcess->getFieldValue('process_name'), 70);
		$msMessage = str_replace("%process_name%",$msProcessName,$msMessage);

		$msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
                    "soWindow = soPopUp.getOpener();" .
                    "soWindow.remove_process();";

		ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,50);
	}
}

class AssociateAssetsToProcessEvent extends FWDRunnable {
	public function run(){
		$miProcessId = FWDWebLib::getObject('selected_process_id')->getValue();
		$maAssets = FWDWebLib::getObject('selected_assets_ids')->getValue();

		$maAssets = explode(",", $maAssets);

		$moProcessAsset = new RMProcessAsset();
		$moProcessAsset->setFieldValue('process_id', $miProcessId);
		foreach ($maAssets as $miAssetId) {
			$moProcessAsset->setFieldValue('asset_id', $miAssetId);
			$moProcessAsset->insert();
		}
	}
}

class RemoveProcessEvent extends FWDRunnable {
	public function run(){
		$miProcessId = FWDWebLib::getObject('selected_process_id')->getValue();
		$moProcess = new RMProcess();
		$moProcess->delete($miProcessId);
		/*
		 * Se possuir o m�dulo de documenta��o atualiza os leitores do documento do processo.
		 */
		//if (FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(POLICY_MODE))
		//$moProcess->updateReaders($miProcessId);
	}
}

class RedirectToProcessArea extends FWDRunnable {
	public function run(){
		$moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
		$moSession->deleteAttribute('areaid');
		$moSession->addAttribute('areaid');
		$moSession->setAttrAreaId(FWDWebLib::getObject('selected_process_area')->getValue());
		echo "isms_change_to_sibling(TAB_SCOPE,'nav_area.php?uniqid=".uniqid()."');";
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));
		$moStartEvent->addAjaxEvent(new ProcessConfirmRemove('process_confirm_remove'));
		$moStartEvent->addAjaxEvent(new RemoveProcessEvent('remove_process_event'));
		$moStartEvent->addAjaxEvent(new AssociateAssetsToProcessEvent('associate_assets_to_process_event'));
		$moStartEvent->addAjaxEvent(new RedirectToProcessArea('redirect_to_process_area'));

		$maNotAllowed = array();

		if(ISMSConfig::getConfigFromDB(ASSET_KEY)){
			$moProcessActivityCountColumn = FWDWebLib::getObject('process_activity_count_column');
			$moProcessActivityCountColumn->setAttrDisplay("false");

			$maNotAllowed = array("M.RM.2.2"); //Atividades
		} else {
			$moProcessActivityCountColumn = FWDWebLib::getObject('process_asset_count_column');
			$moProcessActivityCountColumn->setAttrDisplay("false");
			
			$maNotAllowed = array("asset");				
		}

		$moACL = FWDACLSecurity::getInstance();
		$moACL->setNotAllowed($maNotAllowed);
		$moACL->installSecurity(FWDWebLib::getObject('dialog'), $maNotAllowed);

		$scopeId = FWDWebLib::getObject("scope")->getValue();
		FWDWebLib::getObject("scopeId")->setValue($scopeId);

		$moHandler = new QueryGridProcess(FWDWebLib::getConnection());

		if($scopeId){
			$moHandler->setScope($scopeId);
		}

		$area =  FWDWebLib::getObject('area')->getValue();
		$miArea = explode(":", $area);
		$miAreaId = $miArea[0];

		if($miAreaId){
			$moHandler->setArea($miAreaId);
			FWDWebLib::getObject('areaId')->setValue($area);
		}

		$moGrid = FWDWebLib::getObject("grid_process");

		$moGrid->setQueryHandler($moHandler);
		$moGrid->setObjFwdDrawGrid(new GridProcess());
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
<script language="javascript">
        function refresh_grid() {
          js_refresh_grid('grid_process');
        }
        
        function set_assets(assets) {
          gebi('selected_assets_ids').value = assets;
          trigger_event('associate_assets_to_process_event', 3);
          refresh_grid();
        }
        
        function remove_process() {
          trigger_event('remove_process_event', 3);
          refresh_grid();
        }
        
        function go_to_nav_asset(piId){
            //isms_change_to_sibling(TAB_ASSET,'nav_asset.php?process='+piId);

	      if(gebi('areaId').value!=0){
	      	isms_change_to_sibling(TAB_ASSET,'nav_asset.php?process='+piId+':'+gebi('areaId').value);
	      }else{
	      	isms_change_to_sibling(TAB_ASSET,'nav_asset.php?process='+piId);
	      }

        }

        function go_to_nav_scene(piId){
	      if(gebi('areaId').value!=0){
	      	isms_change_to_sibling(TAB_PROCESS,'nav_process_scene.php?scene_process_id='+piId+':'+gebi('areaId').value);
	      }else{
	      	isms_change_to_sibling(TAB_PROCESS,'nav_process_scene.php?scene_process_id='+piId);
	      }
      	}

        function goToPlans(process){
 	     if(gebi('areaId').value!=0){
	      	isms_change_to_sibling(TAB_PLAN,'nav_plan.php?process='+process+':'+gebi('areaId').value);
	      }else{
	      	isms_change_to_sibling(TAB_PLAN,'nav_plan.php?process='+process);
	      }     
      	}

        function enterArea(){
            isms_change_to_sibling(TAB_AREA,'nav_un.php');
      	}

        function enterPlace(place_id) {          
        	isms_change_to_sibling(TAB_PLACE, 'nav_places.php?root='+place_id);
        }

	    function enter_area(area_id) {
	    	isms_change_to_sibling(TAB_AREA, 'nav_un.php?root='+area_id);
	    }      	

        function go_to_nav_activity(piId){
	      if(gebi('areaId').value!=0){
	      	isms_change_to_sibling(TAB_PROCESS,'nav_process_activity.php?activity_process_id='+piId+':'+gebi('areaId').value);
	      }else{
	      	isms_change_to_sibling(TAB_PROCESS,'nav_process_activity.php?activity_process_id='+piId);
	      }            
    	}

        function go_to_nav_threat(piId){
            isms_change_to_sibling(TAB_PROCESS,'nav_process_threat.php?threat_process_id='+piId);
     	}

        function goToScope(){
            isms_change_to_sibling(TAB_SCOPE,'nav_scope.php');
      	}

        function associateProcess(){
        	isms_open_popup('popup_process_search','packages/continuity/popup_process_search.php?scope='+gebi('scopeId').value,'','true');
        }
        
        function go_to_nav_area(piId){
          gebi('selected_process_area').value = piId;
          trigger_event('redirect_to_process_area',3);
        }

        if(gebi("scopeId").value != ""){
        	js_show("associate_scope");
        	js_hide("vb_insert");
        }else{
        	js_hide("associate_scope");
        	js_show("vb_insert");
        }
</script>

		<?
		$area = FWDWebLib::getObject('area')->getValue();
		if($area){
			$processString = FWDLanguage::getPHPStringValue('tt_processes_bl','Processos');
			ISMSLib::getPathScrollCode(RISK_MANAGEMENT_MODE, CONTEXT_AREA, $area, $processString, false, '');
		} else {
			ISMSLib::getPathScrollCode(RISK_MANAGEMENT_MODE, CONTEXT_PROCESS, 0, '', false, '');
		}
		
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_process.xml");
?>