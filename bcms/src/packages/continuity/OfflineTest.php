<?php 
	require_once('include.php');
	require_once('../../handlers/grid/QueryOfflinePlanTest.php');
	
	$planId = $_GET["plan"];
	$planTestId = $_GET["planTest"];
	
	$query = new QueryOfflinePlanTest(FWDWebLib::getConnection());
	$query->setPlan($planId);
	$query->makeQuery();
	$query->executeQuery();
	
	$plan = null;
	$planId = null;
	$planDescription = null;
	$actions = null;
	$ranges = array();
	$rangeNames = array();
	$currentRange = null;
	while($query->fetch()){
		$range = $query->getFieldValue("range");
		if($currentRange != $range){
			$currentRange = $range;
			if($actions){
				$ranges[] = $actions;
			}
			$rangeNames[] = QueryOfflinePlanTest::replacePatterns($range);
			$actions = array();
		}
		if(!$plan){
			$plan = $query->getFieldValue("name");
		}
		if(!$planId){
			$planId = $query->getFieldValue("planId");
		}
		if(!$planDescription){
			$planDescription = $query->getFieldValue("planDescription");
		}

		$arr = array();
		$arr[] = $query->getFieldValue("resume");
		$arr[] = $query->getFieldValue("acronym");
		$arr[] = $query->getFieldValue("description");
		$arr[] = $query->getFieldValue("estimate");
		$arr[] = $query->getFieldValue("planActionId");
		$actions[] = $arr;
	}
	$ranges[] = $actions;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
		<meta content="no-cache" http-equiv="Pragma">
		<meta content="Realiso Corp." name="keywords">
		<meta content="noindex, nofollow" name="robots">
		
		<title>Preencher resultado dos testes</title>
		<script type="text/javascript">
			function Formatadata(Campo, teclapres)
			{
				var tecla = teclapres.keyCode;
				var vr = new String(Campo.value);
				vr = vr.replace("/", "");
				vr = vr.replace("/", "");
				vr = vr.replace("/", "");
				tam = vr.length + 1;
				if (tecla != 8 && tecla != 8)
				{
					if (tam > 0 && tam < 2)
						Campo.value = vr.substr(0, 2) ;
					if (tam > 2 && tam < 4)
						Campo.value = vr.substr(0, 2) + '/' + vr.substr(2, 2);
					if (tam > 4 && tam < 7)
						Campo.value = vr.substr(0, 2) + '/' + vr.substr(2, 2) + '/' + vr.substr(4, 7);
				}
			}

			function formatar(mascara, documento){
				var i = documento.value.length;
				var saida = mascara.substring(0,1);
		  		var texto = mascara.substring(i);
		  		if (texto.substring(0,1) != saida){
		        	documento.value += texto.substring(0,1);
		  		}
			}	
		</script>
		<style type="text/css">
			div.btn{
				background: none repeat scroll 0pt 0pt rgb(216, 216, 216); 
				border: 1px solid rgb(168, 168, 168); 
				height: 20px; 
				width: 40px;
				margin-left: 1070px;
				margin-top: 15px;
				cursor: pointer;
			}
			
			div.btn div{
				margin-left: 11px; 
				margin-top: 3px; 
				font-family: Verdana,tahoma,arial,sans-serif; 
				font-size: 8pt;
			}
			
			div.observacao{
				height: 150px; 
				width:250px;
			}
			
			div.observacao textarea{
				height: 140px;
				width: 245px;
			}
			
			div.data{
				height: 100px; 
				width:106px; 
			}
			
			div.data input{
				width: 101px; 
				margin-top: 5px;
			}
			
			body{
				font-family: arial;
				font-size: 14px;
				page: landscape;
				background-color: #F8F8F8;
			}
			
			td{
				border: 1px solid #B64B08;
				border-left: none;
				border-bottom: none;
			}
			
			th{
				text-align: left;
				color: black;
				border-right: 1px solid #B64B08;
			}
			
			table{
				border: 1px #B64B08 solid ;
				border-right: none;
				margin: 10px;
				margin-top: 0px;
			}
			
			td.sigla{
				text-align: center;
			}
			tr{
				background-color: #FEF0E2;
				vertical-align: top;
			}
			tr.cor{
				background-color: #FEE7D0;
			}
			
			div.a4{
				width: 1122px;
				height: 210mm;
			}
		
			div.tableHeader{
				width: 1100px;
				border: 1px solid #B64B08;
				border-bottom: none;
				font-weight: bold;
				font-size: 25px;
				margin-left: 10px;
				margin-top: 10px;
				padding-bottom: 5px;
				padding-top: 5px;
				padding-left: 10px;
			}
				
		</style>
	</head>
	<body>
		<div class="a4">
			<form id="form" action="OfflineTestSave.php" method="post">
				<input type="hidden" value="<?php echo $planId;?>" name="plan"></input>
				<input type="hidden" value="<?php echo $planTestId;?>" name="planTest"></input>
				<?php for($i=0;$i<count($ranges);$i++){ ?>
					<div class="tableHeader"><?php echo $rangeNames[$i]; $data = $ranges[$i]; ?></div>
					<table cellpadding="10px" cellspacing="0px">
						<tr>
							<th>Plano</th>
							<th>Grupo Responsável</th>
							<th>Descrição</th>
							<th>Tempo Estimado</th>
							<th>Hora Início</th>
							<th>Hora Fim</th>
							<th>Observação</th>
						</tr>
						<?php 
							$cor = false;
							foreach($data as $row){
								
								$planActionTest = new CMPlanActionTest();

								$planActionId = $row[4];
							
								$planActionTest->createFilter($planTestId, "planTest");
								$planActionTest->createFilter($planActionId, "planAction");
								$planActionTest->select();
								$found = false;
								if($planActionTest->fetch()){
									$found = true;
								}
								$observation = "";
								$startDate = "";
								$startTime = "";
								$endDate = "";
								$endTime = "";
								if($found){
									if($planActionTest->getFieldValue('startTime')){
										$msDateTime = explode(' ', ISMSLib::getISMSDate($planActionTest->getFieldValue('startTime')));
										$startDate = $msDateTime[0];
										$startTime = $msDateTime[1];
									}
									
									if($planActionTest->getFieldValue('endTime')){
										$msDateTime = explode(' ', ISMSLib::getISMSDate($planActionTest->getFieldValue('endTime')));
										$endDate = $msDateTime[0];
										$endTime = $msDateTime[1];
									}
									if($planActionTest->getFieldValue('observation')){
										$observation = $planActionTest->getFieldValue('observation');
									}
								}
								
						?>
						<tr <?php if($cor) echo " class=\"cor\""; $cor = !$cor;?>>
							<td><?php echo $row[0];?></td> 
							<td class="sigla"><?php echo $row[1];?></td>
							<td><?php echo $row[2];?></td>
							<td><?php echo $row[3];?>
							</td>
							<td><div class="data">
								Data
								<input maxlength="10" onkeyup="Formatadata(this,event)" type="text" name="startDate_<?php echo $row[4];?>" <?php if($found) echo "value=\"{$startDate}\"";?>></input>
								Hora
								<input OnKeyPress="formatar('##:##', this)" maxlength="5" type="text" name="startHour_<?php echo $row[4];?>" <?php if($found) echo "value=\"{$startTime}\"";?>></input>
							</div></td>
							<td><div class="data">
								Data
								<input maxlength="10" onkeyup="Formatadata(this,event)" type="text" name="endDate_<?php echo $row[4];?>" <?php if($found) echo "value=\"{$endDate}\"";?>></input>
								Hora
								<input OnKeyPress="formatar('##:##', this)" maxlength="5" type="text" name="endHour_<?php echo $row[4];?>" <?php if($found) echo "value=\"{$endTime}\"";?>></input>
							</div></td>
							<td><div class="observacao"><textarea name="observation_<?php echo $row[4];?>"><?php if($found) echo $observation;?></textarea></div></td>
						</tr>
						<?php }?>				
					</table>
				<?php } ?>
				<div class="btn" onclick="javascript: document.getElementById('form').submit();"><div>OK</div></div>
			</form>
		</div>
	</body>
</html>