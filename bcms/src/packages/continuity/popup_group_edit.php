<?php

include_once 'include.php';
include_once $handlers_ref . "select/QuerySelectPriority.php";
include_once $handlers_ref . "select/QueryResourceFromGroup.php";
include_once $handlers_ref . "select/QuerySelectResource.php";

class SaveGroupEvent extends FWDRunnable {
	public function run(){
		$groupName = FWDWebLib::getObject('groupName')->getValue();
		$groupAcronym = FWDWebLib::getObject('groupAcronym')->getValue();
		$groupId = FWDWebLib::getObject('groupId')->getValue();
		$groupPriority = FWDWebLib::getObject('groupPriority')->getValue();
		$place = FWDWebLib::getObject('placeId')->getValue();
		$area = FWDWebLib::getObject('areaId')->getValue();

		$group = new CMGroup();
		$group->setFieldValue('group_name', $groupName);
		$group->setFieldValue('group_acronym', $groupAcronym);
		$group->setFieldValue('group_priority', $groupPriority);

		if($groupId){
			$group->update($groupId, true, true);
		}else{
			$groupId = $group->insert(true);
		}

		if($place){
			$r = new CMGroup();
			$r->setFieldValue('group_id',$groupId);
			$r->updateRelation(new CMPlaceResource(),'place_id',array($place));
		}

		if($area){
			$r = new CMGroup();
			$r->setFieldValue('group_id',$groupId);
			$r->updateRelation(new CMAreaResource(),'area_id',array($area));
		}

		$msNewResources = FWDWebLib::getObject('resources_ids')->getValue();
		if($msNewResources){
			$maNewResources = explode(':',$msNewResources);
		}else{
			$maNewResources = array();
		}

		$moQuery = new QueryResourceFromGroup(FWDWebLib::getConnection());
		$moQuery->setGroupId($groupId);
		$moQuery->makeQuery();
		$moQuery->fetchResources();
		$maOldResources = $moQuery->getResources();

		$maToInsert = array_diff($maNewResources,$maOldResources);
		$maToDelete = array_diff($maOldResources,$maNewResources);
		$moResourceGroup = new CMGroupResource();
		if(count($maToInsert)) $moResourceGroup->insertResources($groupId,$maToInsert);
		if(count($maToDelete)) $moResourceGroup->deleteResources($groupId,$maToDelete);

		$moResourceGroup->orderResources($maNewResources,$groupId);

		echo "soWindow = soPopUpManager.getPopUpById('popup_group_edit').getOpener();"
		."if(soWindow.refresh_grid) soWindow.refresh_grid(".$groupId.");"
		."soPopUpManager.closePopUp('popup_group_edit');";
	}
}

class RefreshResourceEvent extends FWDRunnable {
	public function run(){
		$moHandler = new QuerySelectResource(FWDWebLib::getConnection());
		$resourceIds = FWDWebLib::getObject('resources_ids')->getValue();
		if($resourceIds){
			$moHandler->setResourceIds($resourceIds);

			$moSelect = FWDWebLib::getObject('resources');
			$moSelect->setQueryHandler($moHandler);
			$moSelect->populate();
			$moSelect->execEventPopulate();
		}
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(''));
		$moStartEvent->addAjaxEvent(new SaveGroupEvent('SaveGroupEvent'));
		$moStartEvent->addAjaxEvent(new RefreshResourceEvent('refresh_resources'));
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$moWebLib = FWDWebLib::getInstance();
		$moWindowTitle = $moWebLib->getObject('window_title');
		$miGroupId = $moWebLib->getObject('param_group_id')->getValue();
			
		$moHandler = new QuerySelectPriority(FWDWebLib::getConnection());
		$moSelect = FWDWebLib::getObject('groupPriority');
		$moSelect->setQueryHandler($moHandler);
		$moSelect->populate();

		$place = $moWebLib->getObject('place')->getValue();
		if($place){
			$moWebLib->getObject('placeId')->setValue($place);
		}
		$area = $moWebLib->getObject('area')->getValue();
		if($area){
			$moWebLib->getObject('areaId')->setValue($area);
		}

		if(!$miGroupId) {
			$moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_group_adding', 'Adicionando Grupo'));
		} else {
			$moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_group_editing', 'Editando Grupo'));
			FWDWebLib::getObject('groupId')->setValue($miGroupId);
			$moCMGroup = new CMGroup();
			$moCMGroup->fetchById($miGroupId);

			FWDWebLib::getObject('groupName')->setValue($moCMGroup->getFieldValue('group_name'));
			FWDWebLib::getObject('groupAcronym')->setValue($moCMGroup->getFieldValue('group_acronym'));
			$priority = $moCMGroup->getFieldValue('group_priority');
			if($priority){
				$moSelect->setAttrValue($priority);
			}
		}

		$moResourceSelect = FWDWebLib::getObject('resources');

		if($miGroupId) {
			$moQuery = new QueryResourceFromGroup(FWDWebLib::getConnection());
			$moQuery->setGroupId($miGroupId);
			$moQuery->makeQuery();
			$moQuery->executeQuery();
			$moResources = array();
			while($moQuery->fetch()) {
				$moResourceSelect->setItemValue($moQuery->getFieldValue('resource_id'), $moQuery->getFieldValue('resource_name'));
				$moResources[] = $moQuery->getFieldValue('resource_id');
			}
			FWDWebLib::getObject('resources_ids')->setValue(implode(':',$moResources));
		}

		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
<script language="javascript">

			function set_resources(resourceIds) {
			  	gebi('resources_ids').value = resourceIds;
			  	resourceIds=null;
			  	trigger_event('refresh_resources',3);
			}	

        gebi('groupName').focus();
        var soListEditor  = new FWDSelect('resources');
    </script>
		<?
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_group_edit.xml');

?>