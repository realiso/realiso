<?php
include_once "include.php";
include_once $handlers_ref . "QueryProcessFromScope.php";
include_once $handlers_ref . "grid/QueryGridScopeSearch.php";
include_once $handlers_ref . "grid/QueryGridProcessSearch.php";

class GridProcessSearch extends FWDDrawGrid {
	public function DrawItem(){
		switch($this->ciColumnIndex){
			case 1:
				break;
			case 2:
				return $this->coCellBox->draw();
				break;
			default:
				return parent::drawItem();
				break;
		}
	}
}

class AssociateProcessEvent extends FWDRunnable {
	public function run(){
		$moWebLib = FWDWebLib::getInstance();
		$scopeId = $moWebLib->getObject('scope_id')->getValue();

		$msNewProcess = $moWebLib->getObject('current_process_ids')->getValue();
		if($msNewProcess){
			$maNewProcess = explode(':',$msNewProcess);
		}else{
			$maNewProcess = array();
		}
		if($scopeId) {

			$moQuery = new QueryProcessFromScope(FWDWebLib::getConnection());
			$moQuery->setScopeId($scopeId);
			$moQuery->makeQuery();
			$moQuery->fetchProcess();
			$maOldProcess = $moQuery->getProcess();

			$maToInsert = array_diff($maNewProcess,$maOldProcess);
			$maToDelete = array_diff($maOldProcess,$maNewProcess);
			$moScopeProcess = new CMScopeProcess();
			if(count($maToInsert)) $moScopeProcess->insertProcess($scopeId,$maToInsert);
			if(count($maToDelete)) $moScopeProcess->deleteProcess($scopeId,$maToDelete);

		}

		echo "soWindow = soPopUpManager.getPopUpById('popup_process_search').getOpener();"
		."if (soWindow.refresh_grid)"
		." soWindow.refresh_grid();"
		."if(soWindow.set_process) soWindow.set_process(gebi('current_process_ids').value);"
		."soPopUpManager.closePopUp('popup_process_search');";
	}
}

class SearchProcessEvent extends FWDRunnable {
	public function run(){
		$moGrid = FWDWebLib::getObject('grid_process_search');
		$moGrid->execEventPopulate();
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));
		$moStartEvent->addAjaxEvent(new SearchProcessEvent('search_process_event'));
		$moStartEvent->addAjaxEvent(new AssociateProcessEvent('associate_process_event'));

		$moSearchGrid = FWDWebLib::getObject('grid_process_search');
		$moSearchGrid->setObjFwdDrawGrid(new GridProcessSearch());
		$moSearchHandler = new QueryGridProcessSearch(FWDWebLib::getConnection());

		$moCurrentGrid = FWDWebLib::getObject('grid_current_process');
		$moCurrentGrid->setObjFwdDrawGrid(new GridProcessSearch());
		$moCurrentHandler = new QueryGridProcessSearch(FWDWebLib::getConnection());

		$scopeId = FWDWebLib::getObject('param_scope_id')->getValue();

		$moSearchHandler->setNameFilter(FWDWebLib::getObject('name_filter')->getValue());
		$moSearchHandler->setScope($scopeId);

		$title = FWDLanguage::getPHPStringValue('tt_process_search', "Busca de Processos");

		$msCurrentIds = FWDWebLib::getObject('current_process_ids')->getValue();
		$removed = FWDWebLib::getObject('removed')->getValue();
		if(!$msCurrentIds && $removed == 'false'){
			if($scopeId){

				$title .= FWDLanguage::getPHPStringValue('processLimitedByScope', " - Limitado �s Unidades de Neg�cio do escopo");

				$msCurrentIds = '0';
				$query = new FWDDBDataSet(FWDWebLib::getConnection());
				$query->setQuery("select fkprocess as id from cm_scope_process where fkscope = {$scopeId}");
				$query->addFWDDBField(new FWDDBField('id','id'  ,DB_NUMBER));
				$query->execute();
				while($query->fetch()){
					$id = $query->getFieldByAlias("id")->getValue();
					$msCurrentIds .= ":".$id;
				}
				FWDWebLib::getObject('current_process_ids')->setValue($msCurrentIds);
			}
		}

		FWDWebLib::getObject('window_title')->setValue($title);

		if($msCurrentIds){
			$maCurrentIds = explode(':',$msCurrentIds);
			$moSearchHandler->setExcludedIds($maCurrentIds);
			$moCurrentHandler->setIds($maCurrentIds);
		}
		else {
			$moCurrentHandler->setEmpty(true);
		}

		$moSearchGrid->setQueryHandler($moSearchHandler);
		$moCurrentGrid->setQueryHandler($moCurrentHandler);
		$moCurrentGrid->populate();
		$moSearchGrid->populate();
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$paramProcessIds = FWDWebLib::getObject('param_process_ids')->getValue();
		if($paramProcessIds) {
			FWDWebLib::getObject('current_process_ids')->setValue($paramProcessIds);
		}
		$scopeId = FWDWebLib::getObject('param_scope_id')->getValue();
		FWDWebLib::getObject('scope_id')->setValue($scopeId);

		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
<script language="javascript">
	gebi('process_name').focus();
	function refresh_grid() {
		gobi('grid_current_process').setPopulate(true);
		gobi('grid_current_process').refresh();
	}
	function enter_process_search_event(e)
	{
		if (!e) e = window.event;
		if(e['keyCode']==13) {
			gebi('name_filter').value = gebi('process_name').value;
			gebi('current_process_ids').value = gobi('grid_current_process').getAllIds().join(':');
			gobi('grid_process_search').setPopulate(true);
			trigger_event("search_process_event","3");
		}
	}
	FWDEventManager.addEvent(gebi('process_name'), 'keydown', enter_process_search_event);
</script>
		<?
	}
}
FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load('popup_process_search.xml');
?>