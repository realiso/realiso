<?php

include_once 'include.php';
include_once $handlers_ref . "select/QuerySelectActiveScenes.php";

function savePlanRangeFromFields($piContext){

	$msPlanRangeName = FWDWebLib::getObject('planRange_name')->getValue();

	$moPlanRange = new CMPlanRange();
	if(!$msPlanRangeName || $msPlanRangeName == ''){
		$moPlanRange->setFieldValue('description', null);
	}else{
		$moPlanRange->setFieldValue('description', $msPlanRangeName);
	}

	$startRange  = FWDWebLib::getObject('startRange')->getValue();
	$endRange  = FWDWebLib::getObject('endRange')->getValue();
	$startRangeFlag = FWDWebLib::getObject('startRangeController')->getValue();
	$endRangeFlag = FWDWebLib::getObject('endRangeController')->getValue();

	if(substr($startRangeFlag,0,1)==':') $startRangeFlag = substr($startRangeFlag,1);
	if(substr($endRangeFlag,0,1)==':') $endRangeFlag = substr($endRangeFlag,1);

	$moPlanRange->setFieldValue('startRange', $startRange);
	$moPlanRange->setFieldValue('startRangeFlag', $startRangeFlag);
	$moPlanRange->setFieldValue('endRange', $endRange);
	$moPlanRange->setFieldValue('endRangeFlag', $endRangeFlag);

	if($piContext) {
		$moPlanRange->update($piContext, true, true);
	} else {
		$piContext = $moPlanRange->insert(true);
	}
	return $moPlanRange;
}


class SavePlanRangeEvent extends FWDRunnable {
	public function run(){
		$miPlanRangeId = FWDWebLib::getObject('planRange_id')->getValue();
		savePlanRangeFromFields($miPlanRangeId);
		echo "soWindow = soPopUpManager.getPopUpById('popup_planRange_edit').getOpener();"
		."if(soWindow.refresh_grid) soWindow.refresh_grid();"
		."soPopUpManager.closePopUp('popup_planRange_edit');";
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(''));
		$moStartEvent->addAjaxEvent(new SavePlanRangeEvent('save_planRange_event'));
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$moWebLib = FWDWebLib::getInstance();
		$moWindowTitle = $moWebLib->getObject('window_title');
		$miPlanRangeId = $moWebLib->getObject('param_planRange_id')->getValue();
			
		if(!$miPlanRangeId) {
			$moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_planRange_adding', 'Adicionando Intervalo de Plano'));
		} else {
			$moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_planRange_editing', 'Editando Intervalo de Plano'));
			FWDWebLib::getObject('planRange_id')->setValue($miPlanRangeId);
			$moCMPlanRange = new CMPlanRange();
			$moCMPlanRange->fetchById($miPlanRangeId);
			FWDWebLib::getObject('planRange_name')->setValue($moCMPlanRange->getFieldValue('description'));
			FWDWebLib::getObject('startRange')->setValue($moCMPlanRange->getFieldValue('startRange'));
			FWDWebLib::getObject('endRange')->setValue($moCMPlanRange->getFieldValue('endRange'));
			FWDWebLib::getObject('startRangeController')->setValue($moCMPlanRange->getFieldValue('startRangeFlag'));
			FWDWebLib::getObject('endRangeController')->setValue($moCMPlanRange->getFieldValue('endRangeFlag'));
		}

		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
<script language="javascript">
        gebi('planRange_name').focus();
    </script>
		<?
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_planRange_edit.xml');

?>