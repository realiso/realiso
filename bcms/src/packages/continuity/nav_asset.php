<?php
include_once "include.php";
include_once $handlers_ref . "grid/QueryGridAsset.php";
include_once $handlers_ref . "QueryTotalAssets.php";
include_once $handlers_ref . "select/QuerySelectCategory.php";

class ConfirmRemove extends FWDRunnable {
	public function run(){
		$msTitle = FWDLanguage::getPHPStringValue('tt_remove_asset','Remover Ativo');
		$msMessage = FWDLanguage::getPHPStringValue('st_asset_remove_confirm',"Voc� tem certeza que deseja remove o ativo <b>%asset_name%</b>?");

		$moAsset = new RMAsset();
		$moAsset->fetchById(FWDWebLib::getObject('selected_asset_id')->getValue());
		$msAssetName = ISMSLib::truncateString($moAsset->getFieldValue('asset_name'), 70);
		$msMessage = str_replace("%asset_name%",$msAssetName,$msMessage);

		$msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
                    "soWindow = soPopUp.getOpener();" .
                    "soWindow.remove();";

		ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue);
	}
}

class RemoveEvent extends FWDRunnable {
	public function run(){
		$miSelectedId = FWDWebLib::getObject('selected_asset_id')->getValue();
		$moAsset = new RMAsset();
		$moAsset->delete($miSelectedId);
		/*
		 * Se possuir o m�dulo de documenta��o atualiza os leitores do documento do ativo.
		 */
		if (FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(POLICY_MODE))
		$moAsset->updateReaders($miSelectedId);
	}
}

class GridAsset extends FWDDrawGrid {
	public function drawItem(){
		switch($this->ciColumnIndex){
			case $this->getIndexByAlias('asset_name'):
				$miAssetValue = $this->caData[1];
				$msLinkColor = RMRiskConfig::getRiskColor($miAssetValue,true);
				$msLink = "<font color=".$msLinkColor.">".$this->caData[2]."</a>";
				$this->coCellBox->setAttrStringNoEscape("true");
				return $this->coCellBox->draw();
				break;
			default:
				return parent::drawItem();
				break;
		}
	}
}

class DuplicateEvent extends FWDRunnable {
	public function run() {
		$miAssetId = FWDWebLib::getObject('selected_asset_id')->getValue();
		$moAsset = new RMAsset();
		$moAsset->fetchById($miAssetId);

		$moNewAsset = new RMAsset();
		$moNewAsset->cloneEntity($moAsset);
		
		$miNewAssetId = $moNewAsset->insert(true);

		echo "refresh_grid(); editAsset({$miNewAssetId});";
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(''));
		$moStartEvent->addAjaxEvent(new ConfirmRemove('confirm_remove'));
		$moStartEvent->addAjaxEvent(new RemoveEvent('remove_event'));
		$moStartEvent->addAjaxEvent(new DuplicateEvent('duplicate_event'));

		FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
		FWDMenuACLSecurity::getInstance()->installMenuACL(FWDWebLib::getObject('menu'),FWDACLSecurity::getInstance());

		$moGrid = FWDWebLib::getObject('grid_asset');
		$moHandler = new QueryGridAsset(FWDWebLib::getConnection());

		$scopeId = FWDWebLib::getObject("scope")->getValue();
		FWDWebLib::getObject("scopeId")->setValue($scopeId);

		if(FWDWebLib::getObject('activity')->getValue()){
			FWDWebLib::getObject('activityId')->setValue(FWDWebLib::getObject('activity')->getValue());
		}

		if(FWDWebLib::getObject('place')->getValue()){
			FWDWebLib::getObject('placeId')->setValue(FWDWebLib::getObject('place')->getValue());
		}

		if(FWDWebLib::getObject('process')->getValue()){
			FWDWebLib::getObject('process_ids')->setValue(FWDWebLib::getObject('process')->getValue());
		}		

		if($scopeId){
			$moHandler->setScope($scopeId);
		}

		$activity =  FWDWebLib::getObject('activityId')->getValue();
		$activityIds = explode(":", $activity);

		$activityId = 0;
		if(count($activityIds)){
			$activityId = $activityIds[0];
		} else {
			$activityId = $activity;
		}

		if($activityId){
			$moHandler->setActivity($activityId);
		}

		if(FWDWebLib::getObject('placeId')->getValue()){
			$moHandler->setPlace(FWDWebLib::getObject('placeId')->getValue());
		}

		//$processId = FWDWebLib::getObject("process")->getValue();
		//FWDWebLib::getObject("process_id")->setValue($processId);
		//$miProcessId = FWDWebLib::getObject('process_id')->getValue();

		$process = FWDWebLib::getObject('process_ids')->getValue();
		$processIds = explode(":", $process);

		$miProcessId = 0;
		if(count($processIds)){
			$miProcessId = $processIds[0];
		} else {
			$miProcessId = $process;
		}

		FWDWebLib::getObject('process_id')->setValue($miProcessId);

		if($miProcessId){
			$moHandler->setProcess($miProcessId);
		}else{
			if(!ISMSLib::userHasACL('M.RM.3.1')){
				$moHandler->setUserId(ISMSLib::getCurrentUserId());
			}
		}

		$moGrid->setQueryHandler($moHandler);
		$moGrid->setObjFwdDrawGrid(new GridAsset());
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		if(FWDWebLib::getObject('par_asset_id')->getValue() && FWDWebLib::getObject('par_asset_filter_type')->getValue()){
			$moAsset = new RMAsset();
			$moAsset->fetchById(FWDWebLib::getObject('par_asset_id')->getValue());
			if( ISMSLib::userHasACL('M.RM.3.6')
			|| ( ISMSLib::userHasACL('M.RM.3.9') && $moAsset->getResponsible() == ISMSLib::getCurrentUserId() )
			){
				$msGridName = FWDWebLib::getObject('asset_grid_name')->getValue();
				switch(FWDWebLib::getObject('par_asset_filter_type')->getValue()){
					case 'dependents':
						$msGridName .=" ".FWDLanguage::getPHPStringValue('grid_asset_filter_dependents',"(dependentes do ativo '<b>%asset_name%</b>')");
						break;
					case 'dependencies':
						$msGridName .=" ".FWDLanguage::getPHPStringValue('grid_asset_filter_dependence',"(depend�ncias do ativo '<b>%asset_name%</b>')");
						break;
					default:
						break;
				}
				FWDWebLib::getObject('asset_grid_name')->setValue(str_replace('%asset_name%',$moAsset->getName(),$msGridName));
			}
		}

		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));

		?>
<script language="javascript">
        function refresh_grid(){
            js_refresh_grid('grid_asset');
        }

        function remove(){
            trigger_event('remove_event',3);
            refresh_grid();
        }

        function nav_asset_open_popup(psId,psPath,psDummy,psModal,piHeight,piWidth){
          setTimeout(
            function(){
              isms_open_popup(psId,psPath,'',psModal,piHeight,piWidth);
              psId = psPath = psDummy = psModal = piHeight = piWidth = null;
            },
            1
          );
        }
          
        function nav_risk_open_popup(psId,psPath,psDummy,psModal,piHeight,piWidth){
          isms_open_popup(psId,psPath,'',psModal,piHeight,piWidth);
          psId = psPath = psDummy = psModal = piHeight = piWidth = null;
        }

        function go_to_nav_place(piId){
            if(gebi('filter_scrolling').value!=0){
				isms_change_to_sibling(TAB_PLACE,'nav_places.php?scrollfilter='+piId+':'+gebi('filter_scrolling').value);
            }else{
				isms_change_to_sibling(TAB_PLACE,'nav_places.php?scrollfilter='+piId);
            }
        }

        function go_to_nav_process(piId){
            var url = 'nav_process.php';
            if(piId){
            	url += '?asset_id='+piId;
            }
			isms_change_to_sibling(TAB_PROCESS,url);
        }

        function goToPlans(asset){

           if(gebi('placeId').value!=0){
				isms_change_to_sibling(TAB_PLAN,'nav_plan.php?asset='+asset+':'+gebi('placeId').value);

			} else if (gebi('activityId').value!=0){

				isms_change_to_sibling(TAB_PLAN,'nav_plan.php?asset='+asset+':'+gebi('activityId').value);
				
			} else if (gebi('process_ids').value!=0){

				isms_change_to_sibling(TAB_PLAN,'nav_plan.php?asset='+asset+':'+gebi('process_ids').value);
				
            }else{
				isms_change_to_sibling(TAB_PLAN,'nav_plan.php?asset='+asset);
            }

      	}
        
        function go_to_nav_asset_dependents(piId){
			isms_change_to_sibling(3,'nav_asset.php?asset_id='+piId+'&asset_filter_type=dependents');
        }
        
        function go_to_nav_asset_depencencies(piId){
			isms_change_to_sibling(3,'nav_asset.php?asset_id='+piId+'&asset_filter_type=dependencies');
        }

        function openActivity(id){
        	isms_change_to_sibling(TAB_PROCESS,'nav_process_activity.php?activity_process_id='+id);
        }

        function openPlace(){
        	isms_change_to_sibling(TAB_PLACE,'nav_places.php');
        }

        function enterPlace(id){
        	isms_change_to_sibling(TAB_PLACE,'nav_places.php?root='+id);
        }        

		if(gebi("placeId").value == 0 && gebi("process_id").value == 0 && gebi("activityId").value == 0 ){
			js_hide("associate");
		}

		function associateAssets(){
			var filterScope = "&scope="+gebi('scopeId').value;
			var filterPlace = "&place="+gebi('placeId').value;
			var filterProcess = "&process="+gebi('process_id').value;
			var filterActivity = "&activity="+gebi('activityId').value;
			isms_open_popup('popup_asset_search','packages/continuity/popup_asset_search.php?a=1'+filterPlace+filterScope+filterProcess+filterActivity,'','true');
	    }

		function goToScope(){
            isms_change_to_sibling(TAB_SCOPE,'nav_scope.php');
      	}

		function goToProcess(){
            isms_change_to_sibling(TAB_PROCESS,'nav_process.php');
      	}

		function aboutAsset(asset){
			isms_open_popup('popup_about_asset','packages/continuity/popup_about_asset.php?asset='+asset,'','true');
		}
	    
	    function set_assets(ids){
	    }
	    
	    if(gebi("scopeId").value != ""){
	    	js_show("associate_scope");
	    	js_hide("vb_insert");
	    }else{
	    	js_hide("associate_scope");
	    	js_show("vb_insert");
	    }

	    function editAsset(id){
	    	var filterProcess = "&process="+gebi('process_id').value;
	    	var filterActivity = "&activity="+gebi('activityId').value;
	    	isms_open_popup('popup_asset_edit','packages/continuity/popup_asset_edit.php?asset='+id+filterProcess+filterActivity,'','true');
	    }
	    
      </script>
		<?
		$scopeId = FWDWebLib::getObject("scope")->getValue();
		$process = FWDWebLib::getObject('process')->getValue();

		$placeId = FWDWebLib::getObject('place')->getValue();
		$activityId = FWDWebLib::getObject('activity')->getValue();

		if($activityId){
			$assetString = FWDLanguage::getPHPStringValue('tt_assets_bl','Ativos');
			ISMSLib::getPathScrollCode(RISK_MANAGEMENT_MODE, CONTEXT_CM_PROCESS_ACTIVITY, $activityId, $assetString, false, '');
		} else if($placeId) {
			$assetString = FWDLanguage::getPHPStringValue('tt_assets_bl','Ativos');
			ISMSLib::getPathScrollCode(RISK_MANAGEMENT_MODE, CONTEXT_CM_PLACE, $placeId, $assetString, false, '');
		} else if($process) {
			$assetString = FWDLanguage::getPHPStringValue('tt_assets_bl','Ativos');
			ISMSLib::getPathScrollCode(RISK_MANAGEMENT_MODE, CONTEXT_PROCESS, $process, $assetString, false, '');			
		} else {
			ISMSLib::getPathScrollCode(RISK_MANAGEMENT_MODE, CONTEXT_ASSET, 0, '', false, '');
		}

	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_asset.xml");
?>