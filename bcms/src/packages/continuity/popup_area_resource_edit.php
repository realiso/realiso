<?php

include_once 'include.php';
include_once $handlers_ref . "select/QueryAreaResource.php";
include_once $handlers_ref . "select/QuerySelectResource.php";
include_once $handlers_ref . "select/QuerySelectGroup.php";


class SaveEvent extends FWDRunnable {
	public function run(){
		$area = FWDWebLib::getObject('areaId')->getValue();

		$msNewResources = FWDWebLib::getObject('resources_ids')->getValue();
		$msNewGroups = FWDWebLib::getObject('groups_ids')->getValue();
		if($msNewResources){
			$maNewResources = explode(':',$msNewResources);
		}else{
			$maNewResources = array();
		}
		if($msNewGroups){
			$maNewGroups = explode(':',$msNewGroups);
		}else{
			$maNewGroups = array();
		}

		$moQuery = new QueryAreaResource(FWDWebLib::getConnection());
		$moQuery->setAreaId($area);
		$moQuery->setResource(true);
		$moQuery->makeQuery();
		$moQuery->fetchResources();
		$maOldResources = $moQuery->getResources();

		$moQuery = new QueryAreaResource(FWDWebLib::getConnection());
		$moQuery->setAreaId($area);
		$moQuery->setResource(false);
		$moQuery->makeQuery();
		$moQuery->fetchResources();
		$maOldGroups = $moQuery->getResources();

		$maToInsert = array_diff($maNewResources,$maOldResources);
		$maToDelete = array_diff($maOldResources,$maNewResources);
		$areaResource = new CMAreaResource();
		if(count($maToInsert)) $areaResource->insertResources($area,$maToInsert);
		if(count($maToDelete)) $areaResource->deleteResources($area,$maToDelete);

		$maToInsert = array_diff($maNewGroups,$maOldGroups);
		$maToDelete = array_diff($maOldGroups,$maNewGroups);
		$areaResource = new CMAreaResource();
		if(count($maToInsert)) $areaResource->insertGroup($area,$maToInsert);
		if(count($maToDelete)) $areaResource->deleteGroup($area,$maToDelete);


		echo "soWindow = soPopUpManager.getPopUpById('popup_area_resource_edit').getOpener();"
		."if(soWindow.refresh_grid) soWindow.refresh_grid();"
		."soPopUpManager.closePopUp('popup_area_resource_edit');";
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(''));
		$moStartEvent->addAjaxEvent(new SaveEvent('SaveEvent'));
		$moStartEvent->addAjaxEvent(new RefreshGroupEvent('refresh_groups'));
		$moStartEvent->addAjaxEvent(new RefreshResourceEvent('refresh_resources'));
	}
}

class RefreshResourceEvent extends FWDRunnable {
	public function run(){
		$moHandler = new QuerySelectResource(FWDWebLib::getConnection());
		$resourceIds = FWDWebLib::getObject('resources_ids')->getValue();
		if($resourceIds){
			$moHandler->setResourceIds($resourceIds);

			$moSelect = FWDWebLib::getObject('resources');
			$moSelect->setQueryHandler($moHandler);
			$moSelect->populate();
			$moSelect->execEventPopulate();
		}
	}
}

class RefreshGroupEvent extends FWDRunnable {
	public function run(){
		$moHandler = new QuerySelectGroup(FWDWebLib::getConnection());
		$groupsIds = FWDWebLib::getObject('groups_ids')->getValue();
		if($groupsIds){
			$moHandler->setGroupIds($groupsIds);

			$moSelect = FWDWebLib::getObject('groups');
			$moSelect->setQueryHandler($moHandler);
			$moSelect->populate();
			$moSelect->execEventPopulate();
		}
	}
}


class ScreenEvent extends FWDRunnable {
	public function run(){
		$moWebLib = FWDWebLib::getInstance();
		$moWindowTitle = $moWebLib->getObject('window_title');
		$area = $moWebLib->getObject('area')->getValue();
			
		$area = $moWebLib->getObject('area')->getValue();
		if($area){
			$moWebLib->getObject('areaId')->setValue($area);
		}

		$moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_group_editing', 'Editando Contatos'));
		FWDWebLib::getObject('areaId')->setValue($area);

		$moResourceSelect = FWDWebLib::getObject('resources');
		$moGroupSelect = FWDWebLib::getObject('groups');

		if($area) {
			$moQuery = new QueryAreaResource(FWDWebLib::getConnection());
			$moQuery->setAreaId($area);
			$moQuery->setResource(true);
			$moQuery->makeQuery();
			$moQuery->executeQuery();
				
			$moResources = array();
			while($moQuery->fetch()) {
				$moResourceSelect->setItemValue($moQuery->getFieldValue('id'), $moQuery->getFieldValue('name'));
				$moResources[] = $moQuery->getFieldValue('id');
			}
			FWDWebLib::getObject('resources_ids')->setValue(implode(':',$moResources));
				
			$moQuery = new QueryAreaResource(FWDWebLib::getConnection());
			$moQuery->setAreaId($area);
			$moQuery->setResource(false);
			$moQuery->makeQuery();
			$moQuery->executeQuery();
				
			$moGroups = array();
			while($moQuery->fetch()) {
				$moGroupSelect->setItemValue($moQuery->getFieldValue('id'), $moQuery->getFieldValue('name'));
				$moGroups[] = $moQuery->getFieldValue('id');
			}
			FWDWebLib::getObject('groups_ids')->setValue(implode(':',$moGroups));
		}

		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
		<script language="javascript">

			function set_groups(groupIds) {
			  	gebi('groups_ids').value = groupIds;
			  	groupIds = null;
			  	trigger_event('refresh_groups',3);
			}
			function set_resources(resourceIds) {
			  	gebi('resources_ids').value = resourceIds;
			  	resourceIds=null;
			  	trigger_event('refresh_resources',3);
			}

        	gebi('groupName').focus();
		</script>
		<?
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_area_resource_edit.xml');

?>