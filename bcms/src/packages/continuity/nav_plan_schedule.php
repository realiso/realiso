<?php
include_once "include.php";
include_once $handlers_ref . "grid/QueryGridPlanSchedule.php";

class PlanConfirmRemove extends FWDRunnable {
	public function run(){
		$msTitle = FWDLanguage::getPHPStringValue('tt_remove_plan','Remover Agendamento');
		$msMessage = FWDLanguage::getPHPStringValue('st_plan_remove_confirm',"Voc� tem certeza que deseja remover o agendamento do plano <b>%plan_name%</b>?");

		$selectedScheduleId = FWDWebLib::getObject('selected_plan_schedule_id')->getValue();
		$schedule = new CMPlanSchedule();
		$schedule->fetchById($selectedScheduleId);

		$planId = $schedule->getFieldValue("plan");

		$moPlan = new CMPlan();
		$moPlan->fetchById($planId);
		$msPlanName = ISMSLib::truncateString($moPlan->getFieldValue('name'), 70);
		$msMessage = str_replace("%plan_name%",$msPlanName,$msMessage);

		$msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
                    "soWindow = soPopUp.getOpener();" .
                    "soWindow.removePlan();";

		ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue);
	}
}

class StartOnlineTest extends FWDRunnable{
	public function run(){
		$selectedScheduleId = FWDWebLib::getObject('selectedScheduleId')->getValue();
		$schedule = new CMPlanSchedule();
		$schedule->fetchById($selectedScheduleId);
		$id = $schedule->getFieldValue("test");
		$planForTestId = $schedule->getFieldValue("plan");
		if(!$id){
			$test = new CMPlanTest();
			$test->setFieldValue("plan", $planForTestId);
			$test->setFieldValue("startTime", time());
			$id = $test->insert(true);
			$schedule->setFieldValue("test", $id);
			$schedule->update($selectedScheduleId);
		}

		echo "window.open(\"OnlineTest.php?planTest={$id}&plan={$planForTestId}\", null, \"height = 808, width = 1148, status=no, location=0,locationbar=0,toolbar=0, menubar=no, scrollbars=yes\");";
	}
}

class RemovePlanEvent extends FWDRunnable {
	public function run(){
		$miSelectedId = FWDWebLib::getObject('selected_plan_schedule_id')->getValue();

		$schedule = new CMPlanSchedule();
		$schedule->delete($miSelectedId);
	}
}

class GridPlan extends FWDDrawGrid {
	public function drawItem(){
		switch($this->ciColumnIndex){
			case $this->getIndexByAlias('date'):
				$next = $this->getFieldValue('date');
				if($next){
					$msDateTime = explode(' ', ISMSLib::getISMSDate($next));
					$next = $msDateTime[0]." ".$msDateTime[1];
					$this->coCellBox->setValue($next);
				}
				return parent::drawItem();
				break;
			case $this->getIndexByAlias('status'):
				$status = $this->getFieldValue('status');

				if($status == 1){
					$status = FWDLanguage::getPHPStringValue('statusPlanned','Programado');
				} else if($status == 2){
					$status = FWDLanguage::getPHPStringValue('statusRealized','Realizado');
				} else if($status == 3){
					$status = FWDLanguage::getPHPStringValue('statusPending','Pendente');
				} else if($status == 4){
					$status = FWDLanguage::getPHPStringValue('statusRunning','Em Andamento');
				}

				$this->coCellBox->setValue($status);
				return parent::drawItem();
				break;
			default:
				return parent::drawItem();
				break;
		}
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(''));
		$moStartEvent->addAjaxEvent(new PlanConfirmRemove('planConfirmRemove'));
		$moStartEvent->addAjaxEvent(new RemovePlanEvent('remove_plan_event'));
		$moStartEvent->addAjaxEvent(new StartOnlineTest('startOnlineTest'));

		$planId = FWDWebLib::getObject('plan')->getValue();
		FWDWebLib::getObject('planId')->setValue($planId);

		$grid = FWDWebLib::getObject('grid_plan');

		$schedule = new CMPlanSchedule();
		$schedule->updateStatus();

		$moHandler = new QueryGridPlanSchedule(FWDWebLib::getConnection());
		if($planId){
			$moHandler->setPlan($planId);
		}
		$grid->setQueryHandler($moHandler);
		$grid->setObjFwdDrawGrid(new GridPlan());

	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
<script language="javascript">
        function refresh_grid(){
            js_refresh_grid('grid_plan');
        }
        
        function remove(){
              trigger_event('remove_event',3);
              refresh_grid();
        }

		function removePlan(){
			trigger_event('remove_plan_event',3);
			refresh_grid();
		}
		
		function enterAction(planId) {
			isms_change_to_sibling(TAB_PLAN,'nav_plan_action.php?plan='+planId);
		}

		function enterTests(planId) {
			isms_change_to_sibling(TAB_PLAN, 'nav_plan_test.php?plan='+planId);
		}

        function goToProcess() {
        	parent.soTabSubManager.changeTab(TAB_PROCESS,'nav_process.php');
        }

        function goToArea() {
        	parent.soTabSubManager.changeTab(TAB_AREA,'nav_un.php');
        }

        function goToAsset(id) {
        	parent.soTabSubManager.changeTab(TAB_ASSET,'nav_asset.php');
        }

        function goToPlace() {
        	parent.soTabSubManager.changeTab(TAB_PLACE,'nav_places.php');
        }

		function printOfflineTest(id){
			
			window.open("OfflineTestPDF.php?schedule="+id, null, "height = 808, width = 1148, status=no, location=0,locationbar=0,toolbar=0, menubar=no, scrollbars=yes");
		}

		function startTest(id){
			gebi("selectedScheduleId").value = id;
			trigger_event('startOnlineTest', 3);
		}

		function fillOfflineTest(schedule){
			isms_open_popup('popup_plan_test_edit','packages/continuity/popup_plan_test_edit.php?schedule='+schedule,'','true');
		}
		
      </script>
		<?

	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_plan_schedule.xml");
?>