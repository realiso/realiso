<?php

include_once 'include.php';
include_once $handlers_ref . "select/QuerySelectActiveScenes.php";

function saveDepartmentFromFields($piContext){

	$msDepartmentName = FWDWebLib::getObject('department_name')->getValue();
	$description = FWDWebLib::getObject('description')->getValue();

	$moDepartment = new CMDepartment();
	if(!$msDepartmentName || $msDepartmentName == ''){
		$moDepartment->setFieldValue('name', null);
	}else{
		$moDepartment->setFieldValue('name', $msDepartmentName);
	}
	$moDepartment->setFieldValue('description', $description);

	if($piContext) {
		$moDepartment->update($piContext, true, true);
	} else {
		$piContext = $moDepartment->insert(true);
	}
	return $moDepartment;
}


class SaveDepartmentEvent extends FWDRunnable {
	public function run(){
		$miDepartmentId = FWDWebLib::getObject('department_id')->getValue();
		saveDepartmentFromFields($miDepartmentId);
		echo "soWindow = soPopUpManager.getPopUpById('popup_department_edit').getOpener();"
		."if(soWindow.refresh_grid) soWindow.refresh_grid();"
		."soPopUpManager.closePopUp('popup_department_edit');";
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(''));
		$moStartEvent->addAjaxEvent(new SaveDepartmentEvent('save_department_event'));
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$moWebLib = FWDWebLib::getInstance();
		$moWindowTitle = $moWebLib->getObject('window_title');
		$miDepartmentId = $moWebLib->getObject('param_department_id')->getValue();
			
		if(!$miDepartmentId) {
			$moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_department_adding', 'Adicionando Departamento'));
		} else {
			$moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_department_editing', 'Editando Departamento'));
			FWDWebLib::getObject('department_id')->setValue($miDepartmentId);
			$moCMDepartment = new CMDepartment();
			$moCMDepartment->fetchById($miDepartmentId);
			FWDWebLib::getObject('department_name')->setValue($moCMDepartment->getFieldValue('name'));
			FWDWebLib::getObject('description')->setValue($moCMDepartment->getFieldValue('description'));
		}

		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
<script language="javascript">
        gebi('department_name').focus();
    </script>
		<?
	}
}
FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_department_edit.xml');
?>