<?php
include_once "include.php";
include_once $handlers_ref . "QueryScopeFromProcess.php";
include_once $handlers_ref . "grid/QueryGridScopeSearch.php";
include_once $handlers_ref . "QueryContextHierarchicalStructure.php";

class GridScopeSearch extends FWDDrawGrid {
  public function DrawItem(){
    switch($this->ciColumnIndex){
      case 1:
/*        $miProcessValue = $this->caData[1];
        $msProcessColor = RMRiskConfig::getRiskColor($miProcessValue);
        $this->coCellBox->setIconSrc("icon-process_".$msProcessColor.".gif");
        return parent::drawItem(); */
        break;
      case 2:
/*        $miProcessValue = $this->caData[1];
        $msLinkColor = RMRiskConfig::getRiskColor($miProcessValue,true);
        $msLink = "<font color=".$msLinkColor.">".$this->caData[2]."</font>";
        $this->coCellBox->setAttrStringNoEscape("true");
        $this->coCellBox->setValue("<a href='javascript:open_visualize(".$this->caData[4].",".CONTEXT_PROCESS.",\"".uniqid()."\")'>{$msLink}</a>"); */
        return $this->coCellBox->draw();
        break;
      default:
        return parent::drawItem();
        break;
    }
  }
}

class AssociateScopeEvent extends FWDRunnable {
  public function run(){    
    $moWebLib = FWDWebLib::getInstance();
    $miProcessId = $moWebLib->getObject('process_id')->getValue();
    
    $msNewScopes = $moWebLib->getObject('current_scopes_ids')->getValue();
    if($msNewScopes){
      $maNewScopes = explode(':',$msNewScopes);
    }else{
      $maNewScopes = array();
    }
    
    if($miProcessId) {
    	$moQuery = new QueryScopeFromProcess(FWDWebLib::getConnection());
    	$moQuery->setProcessId($miProcessId);
    	$moQuery->makeQuery();
    	$moQuery->fetchScopes();
    	$maOldScopes = $moQuery->getScopes();
    
    	$maToInsert = array_diff($maNewScopes,$maOldScopes);
    	$maToDelete = array_diff($maOldScopes,$maNewScopes);
    	$moScopeProcess = new CMScopeProcess();
    	if(count($maToInsert)) $moScopeProcess->insertScopes($miProcessId,$maToInsert);
    	if(count($maToDelete)) $moScopeProcess->deleteScopes($miProcessId,$maToDelete);
    }
    
    /*
     * Se possuir o m�dulo de documenta��o, deve atualizar os leitores do documento do ativo 
     */
/*    if (FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(POLICY_MODE)) {     
      $moAsset= new RMAsset();              
      $moAsset->updateReaders($miAssetId);          
    } */
    
    echo "soWindow = soPopUpManager.getPopUpById('popup_scope_search').getOpener();"
        ."if (soWindow.refresh_grid)"
        ." soWindow.refresh_grid();"
        ."soWindow.set_scopes(gebi('current_scopes_ids').value);"
        ."soPopUpManager.closePopUp('popup_scope_search');";
        
  }
}

class SearchScopeEvent extends FWDRunnable {
  public function run(){
    $moGrid = FWDWebLib::getObject('grid_scope_search');
    $moGrid->execEventPopulate();
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new SearchScopeEvent('search_scope_event'));    
    $moStartEvent->addAjaxEvent(new AssociateScopeEvent('associate_scope_event'));
    
    $moSearchGrid = FWDWebLib::getObject('grid_scope_search');
    $moSearchGrid->setObjFwdDrawGrid(new GridScopeSearch());
    $moSearchHandler = new QueryGridScopeSearch(FWDWebLib::getConnection());
    
    $moCurrentGrid = FWDWebLib::getObject('grid_current_scopes');
    $moCurrentGrid->setObjFwdDrawGrid(new GridScopeSearch());
    $moCurrentHandler = new QueryGridScopeSearch(FWDWebLib::getConnection());
    
    $moSearchHandler->setNameFilter(FWDWebLib::getObject('name_filter')->getValue());

    $msCurrentIds = FWDWebLib::getObject('current_scopes_ids')->getValue();
    
    	
    
    if($msCurrentIds){
      $maCurrentIds = explode(':',$msCurrentIds);
      $moSearchHandler->setExcludedIds($maCurrentIds);
      $moCurrentHandler->setIds($maCurrentIds);
    }
    else {
      $moCurrentHandler->setEmpty(true);
    }
    
    $moSearchGrid->setQueryHandler($moSearchHandler);
    $moCurrentGrid->setQueryHandler($moCurrentHandler);
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
  	$paramScopesIds = FWDWebLib::getObject('param_scope_ids')->getValue();
  	if($paramScopesIds) {
//  		 $msCurrentIds = FWDWebLib::getObject('current_scopes_ids')->getValue();
//  		 if(!$msCurrentIds) {
	 	FWDWebLib::getObject('current_scopes_ids')->setValue($paramScopesIds);
//  		 }
  		 
  	}
    $miProcessId = FWDWebLib::getObject('param_process_id')->getValue();
    FWDWebLib::getObject('process_id')->setValue($miProcessId);   	
    //teste para verificar se o sistema n�o est� sendo hakeado
    
/*    $moQuery = new QueryScopeFromProcess(FWDWebLib::getConnection());
    $moQuery->setProcessId(FWDWebLib::getObject('param_process_id')->getValue());
    $moQuery->makeQuery();
    $moQuery->fetchScopes();
    FWDWebLib::getObject('current_scopes_ids')->setValue(implode(":",$moQuery->getScopes())); */
    
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
?>
<script language="javascript">
  gebi('scope_name').focus();
  function refresh_grid() {    
    gobi('grid_current_scopes').setPopulate(true);
    gobi('grid_current_scopes').refresh();
    //gobi('grid_process_search').setPopulate(true);
    //gobi('grid_process_search').refresh();
  }
  function enter_scope_search_event(e)
	{
		if (!e) e = window.event;
		if(e['keyCode']==13) {
      gebi('name_filter').value = gebi('scope_name').value;
      gebi('current_scopes_ids').value = gobi('grid_current_scopes').getAllIds().join(':');
      gobi('grid_scopes_search').setPopulate(true);
	    trigger_event("search_scope_event","3");
		}
	}
	FWDEventManager.addEvent(gebi('scope_name'), 'keydown', enter_scope_search_event);
  refresh_grid();
</script>
<?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load('popup_scope_search.xml');

?>