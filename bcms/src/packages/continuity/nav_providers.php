<?php
include_once "include.php";
include_once $handlers_ref . "grid/QueryGridResource.php";
include_once $handlers_ref . "select/QuerySelectCategory.php";

class ProviderConfirmRemove extends FWDRunnable {
	public function run(){
		$msTitle = FWDLanguage::getPHPStringValue('tt_remove_provider','Remover Fornecedor');
		$msMessage = FWDLanguage::getPHPStringValue('st_provider_remove_confirm',"Voc� tem certeza que deseja remover o fornecedor <b>%provider_name%</b>?");

		$moProvider = new CMProvider();
		$moProvider->fetchById(FWDWebLib::getObject('selected_provider_id')->getValue());
		$msProviderName = ISMSLib::truncateString($moProvider->getFieldValue('name'), 70);
		$msMessage = str_replace("%provider_name%",$msProviderName,$msMessage);

		$msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
                    "soWindow = soPopUp.getOpener();" .
                    "soWindow.remove();";

		ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue);
	}
}

class ResourceConfirmRemove extends FWDRunnable {
	public function run(){
		$msTitle = FWDLanguage::getPHPStringValue('tt_remove_resource','Remover Recurso');
		$msMessage = FWDLanguage::getPHPStringValue('st_resource_remove_confirm',"Voc� tem certeza que deseja remover o recurso <b>%resource_name%</b>?");

		$moResource = new CMResource();
		$moResource->fetchById(FWDWebLib::getObject('selected_resource_id')->getValue());
		$msResourceName = ISMSLib::truncateString($moResource->getFieldValue('resource_name'), 70);
		$msMessage = str_replace("%resource_name%",$msResourceName,$msMessage);

		$msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
                    "soWindow = soPopUp.getOpener();" .
                    "soWindow.removeResource();";

		ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue);
	}
}

class RemoveResourceEvent extends FWDRunnable {
	public function run(){
		$miSelectedId = FWDWebLib::getObject('selected_resource_id')->getValue();
		$moResource = new CMResource();
		$moResource->delete($miSelectedId);

		$providerResource = new CMProviderResource();
		$providerResource->createFilter($miSelectedId, 'resource_id');
		$providerResource->select();
		while($providerResource->fetch()){
			$providerResource->delete($providerResource->getFieldValue('provider_resource_id'));
		}
	}
}

class RemoveEvent extends FWDRunnable {
	public function run(){
		$miSelectedId = FWDWebLib::getObject('selected_provider_id')->getValue();

		$providerResource = new CMProviderResource();
		$providerResource->createFilter($miSelectedId, 'id');
		$providerResource->select();
		while($providerResource->fetch()){
			$resource = new CMResource();
			$resource->createFilter($providerResource->getFieldValue('resource_id'), 'resource_id');
			$resource->select();
			while($resource->fetch()){
				$resource->delete($resource->getFieldValue('resource_id'));
			}
			$providerResource->delete($providerResource->getFieldValue('provider_resource_id'));
		}

		$moProvider = new CMProvider();
		$moProvider->delete($miSelectedId);
	}
}

class MarkResourceAsDefaultEvent extends FWDRunnable {
	public function run(){
		$rootProviderid = FWDWebLib::getObject("rootProviderId")->getValue();

    	$providerResource = new CMProviderResource();
		$providerResource->createFilter(true, 'is_default');
    	$providerResource->createFilter($rootProviderid, 'id');
		$providerResource->select();
		
		// Limpa os possiveis marcados
		while($providerResource->fetch()){
			$providerResourceMarked = new CMProviderResource();
			$providerResourceMarked->setFieldValue('is_default', false);
		  
			$id = $providerResource->getFieldValue('provider_resource_id');
			$providerResourceMarked->update($id);
		}
		
    	$miSelectedId = FWDWebLib::getObject('selected_resource_id')->getValue();
		
		$providerResource = null;
		$providerResource = new CMProviderResource();
		$providerResource->createFilter($miSelectedId, 'resource_id');
		$providerResource->select();
		
		while($providerResource->fetch()){
			$providerResourceMarked = new CMProviderResource();
			$providerResourceMarked->setFieldValue('is_default', true);
		  
			$id = $providerResource->getFieldValue('provider_resource_id');
			$providerResourceMarked->update($id);		  
		}
	}
}

class GridResource extends FWDDrawGrid {
	public function drawItem(){
		switch($this->ciColumnIndex){
			case $this->getIndexByAlias('resource_img'):
				$msIconRef = '';
				$maNotAllowed=array();
				if($this->getFieldValue('resource_type') == 'resource'){
					$msIconRef="icon-risk_gray.gif";
					$maNotAllowed = array("provider");
				}else if($this->getFieldValue('resource_type') == 'provider'){
					$msIconRef="icon-category.gif";
					$maNotAllowed = array("user");
				}

				$moACL = FWDACLSecurity::getInstance();
				$moACL->setNotAllowed($maNotAllowed);
				$moMenu = FWDWebLib::getObject('menu');
				$moGrid = FWDWebLib::getObject('grid_resource');
				$msLine = $moGrid->getAttrName().'_acl_'.$this->ciRowIndex;
				FWDGridMenuACLSecurity::getInstance()->installGridMenuACL($moMenu,$moACL,$moGrid,$msLine);
				$this->coCellBox->setIconSrc($msIconRef);
				return parent::drawItem();
				break;
				
			case $this->getIndexByAlias('resource_name'):
				if($this->getFieldValue('resource_type') == 'provider'){
					if(ISMSLib::userHasACL('M.L.1.1')){
						$this->coCellBox->setValue("<a href=javascript:enterProvider(".$this->getFieldValue('resource_id').");>".$this->getFieldValue('resource_name')."</a>");
					}
				}
				else{
				  if($this->getFieldValue('resource_is_default')){
					  $this->coCellBox->setValue("<b>".$this->getFieldValue('resource_name')."</b>");
				  }
				}
				return $this->coCellBox->draw();
				break;
				
			case $this->getIndexByAlias('default_contact_info'):
			    $resourceId = $this->getFieldValue('resource_id');
			    $contactId = $this->getFieldValue('contact_id');
			    $contactName = $this->getFieldValue('contact_name');
			    $contactEmail = $this->getFieldValue('contact_email');
			    $contactPhone = $this->getFieldValue('contact_phone');
			    
			    if($resourceId && $contactId && $contactName){
            $this->coCellBox->setValue("<a href=javascript:editResource($resourceId,$contactId);>{$contactName}</a> ({$contactEmail}) $contactPhone");
			    }
			  return $this->coCellBox->draw();
			  break;
			  
			default:
				return parent::drawItem();
				break;
		}
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(''));
		$moStartEvent->addAjaxEvent(new ProviderConfirmRemove('providerConfirmRemove'));
		$moStartEvent->addAjaxEvent(new ResourceConfirmRemove('resourceConfirmRemove'));
		$moStartEvent->addAjaxEvent(new RemoveEvent('remove_event'));
		$moStartEvent->addAjaxEvent(new RemoveResourceEvent('remove_resource_event'));
		$moStartEvent->addAjaxEvent(new MarkResourceAsDefaultEvent('mark_as_default_event'));
		//$moStartEvent->addAjaxEvent(new ChangeScrollingPath('change_scrolling_path'));

		FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
		FWDMenuACLSecurity::getInstance()->installMenuACL(FWDWebLib::getObject('menu'),FWDACLSecurity::getInstance());

		$grid = FWDWebLib::getObject('grid_resource');
		$moHandler = new QueryGridResource(FWDWebLib::getConnection());
		$moHandler->setProvider(1);

		$provider = FWDWebLib::getObject("root_provider_id")->getValue();
		if($provider){
			$moHandler->setProviderId($provider);
			FWDWebLib::getObject("rootProviderId")->setValue($provider);
		}

		$ids = FWDWebLib::getObject("un_id")->getValue();

    	$idsArray = explode(":", $ids);

    	$unId = 0;
    	if(count($idsArray))
    		$unId = $idsArray[0];
    	else
    		$unId = $ids;

		if($provider){
			$cols = $grid->getColumns();
			foreach($cols as $col){
				if(
				$col->getAttrAlias() == "comercialNumber"
				|| 	$col->getAttrAlias() == "celNumber" //telefone alternativo
			  || 	$col->getAttrAlias() == "email"
				|| 	$col->getAttrAlias() == "nextelNumber" // outro
				){
					$col->setAttrDisplay("true");
				}

				if($col->getAttrAlias() == "resource_acronym"
				|| $col->getAttrAlias() == "resource_detail"
				|| $col->getAttrAlias() == "default_contact_info"){
					$col->setAttrDisplay("false");
				}
			}
		}
		
		$moHandler->setUN($unId);
		FWDWebLib::getObject("unId")->setValue($unId);		

		$grid->setQueryHandler($moHandler);
		$grid->setObjFwdDrawGrid(new GridResource());
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){

		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
		
<script language="javascript">

		function refresh_grid(){
			js_refresh_grid('grid_resource');
		}

		function remove(){
     		trigger_event('remove_event',3);
			refresh_grid();
		}

    	function removeResource(){
      		trigger_event('remove_resource_event',3);
			refresh_grid();
		}

		function insertProvider(){
			var unId = gebi('unId').value;
			isms_open_popup('popup_provider_edit','packages/continuity/popup_provider_edit.php?un_id='+unId, '', 'true');
		}

		function goToPlaces(){
			isms_change_to_sibling(TAB_PLACE,'nav_places.php');
		}

		function editResource(provider,resource){
			isms_open_popup('popup_resource_edit','packages/continuity/popup_resource_edit.php?provider='+provider+'&resource='+resource,'','true');
		}
		
		function enterProvider(resourceId) {
			gebi('rootProviderId').value = resourceId;
			isms_change_to_sibling(TAB_PROVIDER,'nav_providers.php?root_provider_id='+resourceId);
		}

	    function enter_area(area_id) {
	    	isms_change_to_sibling(TAB_AREA, 'nav_un.php?root='+area_id);
	    }     

		if(gebi('rootProviderId').value == 0){
			js_hide("button_insert_resource");
			js_show("button_insert_provider");
		}else{
			js_hide("button_insert_provider");
			js_show("button_insert_resource");
		}

		if(gebi('unId').value != 0 || gebi('unId').value != ""){
			js_hide("button_insert_resource");
		}

      </script>
		<?
		$unId = FWDWebLib::getObject("un_id")->getValue();
		$rootId = FWDWebLib::getObject("rootProviderId")->getValue();

		if($unId){
			$providersString = FWDLanguage::getPHPStringValue('providers','Fornecedores');
			ISMSLib::getPathScrollCode(RISK_MANAGEMENT_MODE, CONTEXT_AREA, $unId, $providersString, false, '');
		} else {
			ISMSLib::getPathScrollCode(RISK_MANAGEMENT_MODE, CONTEXT_CM_PROVIDER, $rootId, '', false, '');
		}
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_providers.xml");
?>