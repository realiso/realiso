<?php

include_once 'include.php';
include_once $handlers_ref . "select/QuerySelectPlan.php";
include_once $handlers_ref . "select/QueryGridGroupResourceSearch.php";

function getPlanFromFields(){
	$planSchedule = new CMPlanSchedule();

	$plan = FWDWebLib::getObject('select_plan')->getValue();
	if($plan){

		$date = FWDWebLib::getObject('date');
		$maTime = explode(":", FWDWebLib::getObject('dateHour')->getValue());
		$timeStamp = mktime(isset($maTime[0])?$maTime[0]:0, isset($maTime[1])?$maTime[1]:0, 0, $date->getMonth(), $date->getDay(), $date->getYear());

		$planSchedule->setFieldValue('date', $timeStamp);
		$planSchedule->setFieldValue('plan', $plan);

		$responsible = FWDWebLib::getObject('responsibleId')->getValue();
		if($responsible){
			$planSchedule->setGroup($responsible);
		}

		return $planSchedule;
	}
	return null;
}

class SavePlanScheduleEvent extends FWDRunnable {
	public function run(){
		$planScheduleId = FWDWebLib::getObject('planScheduleId')->getValue();
		$planSchedule = getPlanFromFields();
		if($planSchedule){
			if($planScheduleId){
				$sch = new CMPlanSchedule();
				$sch->fetchById($planScheduleId);
				if($sch->getFieldValue("status") != 2){
					$now = time();
					if($now > $planSchedule->getFieldValue("date")){
						$planSchedule->setFieldValue('status', 3);
					}else{
						$planSchedule->setFieldValue('status', 1);
					}
				}
				if(!$sch->getFieldValue("test")){
					$planSchedule->setFieldValue("test", 'null');
				}
				$planSchedule->update($planScheduleId,true,true);
			}else{
				$plan = FWDWebLib::getObject('select_plan')->getValue();
				$planSchedule->setPlanResponsible($plan);

				$now = time();

				if($now > $planSchedule->getFieldValue("date")){
					$planSchedule->setFieldValue('status', 3);
				}else{
					$planSchedule->setFieldValue('status', 1);
				}

				if(!$planSchedule->getFieldValue("test")){
					$planSchedule->setFieldValue("test", 'null');
				}

				$planScheduleId = $planSchedule->insert(true);
			}
		}

		echo "soWindow = soPopUpManager.getPopUpById('popup_plan_schedule_edit').getOpener();"
		."if(soWindow.refresh_grid) soWindow.refresh_grid();"
		."soPopUpManager.closePopUp('popup_plan_schedule_edit');";
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(''));
		$moStartEvent->addAjaxEvent(new SavePlanScheduleEvent('save_plan_schedule_event'));

		$defaultFinderGroup = new DefaultFinder('search_responsible');
		$defaultFinderGroup->set('responsible', new QueryGridGroupResourceSearch(FWDWebLib::getConnection()));
		$moStartEvent->addAjaxEvent($defaultFinderGroup);

	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$planScheduleId = FWDWebLib::getObject('param_plan_schedule_id')->getValue();
		$planId = FWDWebLib::getObject('plan')->getValue();
		FWDWebLib::getObject('planId')->setValue($planId);

		FWDWebLib::getObject('planScheduleId')->setValue($planScheduleId);

		$moWindowTitle = FWDWebLib::getObject('window_title');

		$moHandler = new QuerySelectPlan(FWDWebLib::getConnection());
		$moSelect = FWDWebLib::getObject('select_plan');
		$moSelect->setQueryHandler($moHandler);
		$moSelect->populate();

		if($planScheduleId){
			$moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_plan_schedule_editing', 'Edi��o de Compromisso'));

			$planSchedule = new CMPlanSchedule();
			$planSchedule->fetchById($planScheduleId);

			FWDWebLib::getObject('responsibleId')->setValue($planSchedule->getGroup());
			FWDWebLib::getObject('responsible')->setValue($planSchedule->getGroupName());

			$moSelect->checkItem($planSchedule->getFieldValue('plan'));

			$msDateTime = explode(' ', ISMSLib::getISMSDate($planSchedule->getFieldValue('date')));
			FWDWebLib::getObject('date')->setValue($msDateTime[0]);
			FWDWebLib::getObject('dateHour')->setValue($msDateTime[1]);

		}else{
			if($planId){
				$moSelect->checkItem($planId);
			}

			FWDWebLib::getObject('responsible')->setAttrDisplay("false");
			FWDWebLib::getObject('label_responsible')->setAttrDisplay("false");

			$moWindowTitle->setValue(FWDLanguage::getPHPStringValue('newPlanSchedule', 'Novo Compromisso'));
		}

		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
<script language="javascript">
        		gebi('startTime').focus();
        	  	function setGroup(id, name, acronym){
          			gebi("responsibleId").value = id;
                  	gebi("responsible").value = name;
              	}

      		</script>
		<?
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_plan_schedule_edit.xml');

?>