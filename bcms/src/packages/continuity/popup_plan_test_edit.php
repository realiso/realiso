<?php

include_once 'include.php';

function getPlanFromFields(){
	$planTest = new CMPlanTest();

	$plan = FWDWebLib::getObject('planId')->getValue();
	if($plan){
		$observation = FWDWebLib::getObject('observation')->getValue();

		$startTime = FWDWebLib::getObject('startTime');
		$maTime = explode(":", FWDWebLib::getObject('startHour')->getValue());
		$startTimeStamp = mktime(isset($maTime[0])?$maTime[0]:0, isset($maTime[1])?$maTime[1]:0, 0, $startTime->getMonth(), $startTime->getDay(), $startTime->getYear());
		$planTest->setFieldValue('startTime', $startTimeStamp);


		$endTime = FWDWebLib::getObject('endTime');
		$maTime = explode(":",FWDWebLib::getObject('endHour')->getValue());
		$endTimeStamp = mktime(isset($maTime[0])?$maTime[0]:0, isset($maTime[1])?$maTime[1]:0, 0, $endTime->getMonth(), $endTime->getDay(), $endTime->getYear());
		$planTest->setFieldValue('endTime', $endTimeStamp);

		$planTest->setFieldValue('observation', $observation);
		$planTest->setFieldValue('plan', $plan);

		return $planTest;
	}
	return null;
}

class SavePlanTestEvent extends FWDRunnable {
	public function run(){
		$planTestId = FWDWebLib::getObject('planTestId')->getValue();
		$scheduleId = FWDWebLib::getObject('scheduleId')->getValue();
		$planTest = getPlanFromFields();
		$plan = FWDWebLib::getObject('planId')->getValue();
		if($planTest){
			if($planTestId){
				$planTest->update($planTestId,true,true);
			}else{
				$planTestId = $planTest->insert(true);
			}
		}

		if($scheduleId){
			$schedule = new CMPlanSchedule();
			$schedule->fetchById($scheduleId);
			$schedule->setFieldValue("test", $planTestId);
			$schedule->update($scheduleId);
		}

		$msTitle = FWDLanguage::getPHPStringValue('fillResults','Preencher Resultados');
		$msMessage = FWDLanguage::getPHPStringValue('confirmFillResults',"Voc� deseja preencher o resultado de cada a��o agora? (Tamb�m poder� fazer isso depois se desejar).");

		$msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
                    "soWindow = soPopUp.getOpener();" .
                    "soWindow.fillOfflineTest({$planTestId}, {$plan});";

		$eventClose = "soWindow = soPopUpManager.getPopUpById('popup_plan_test_edit').getOpener();"
		."if(soWindow.refresh_grid) soWindow.refresh_grid();"
		."soPopUpManager.closePopUp('popup_plan_test_edit');";

		ISMSLib::openConfirm($msTitle, $msMessage, $msEventValue.$eventClose, 0, $eventClose);

	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(''));
		$moStartEvent->addAjaxEvent(new SavePlanTestEvent('save_plan_test_event'));
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$planTestId = FWDWebLib::getObject('param_plan_test_id')->getValue();
		$planId = FWDWebLib::getObject('plan')->getValue();

		$idSchedule = FWDWebLib::getObject('schedule')->getValue();
		FWDWebLib::getObject('scheduleId')->setValue($idSchedule);
		if(!$planId && $idSchedule){
			$schedule = new CMPlanSchedule();
			$schedule->fetchById($idSchedule);
			$planId = $schedule->getFieldValue("plan");
			if(!$planTestId){
				$planTestId = $schedule->getFieldValue("test");
			}
		}

		FWDWebLib::getObject('planTestId')->setValue($planTestId);
		FWDWebLib::getObject('planId')->setValue($planId);
		
		$moWindowTitle = FWDWebLib::getObject('window_title');

		if($planTestId){
			$moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_plan_test_editing', 'Edi��o de Teste'));

			$planTest = new CMPlanTest();
			$planTest->fetchById($planTestId);

			if(!$planId){
				FWDWebLib::getObject('planId')->setValue($planTest->getFieldValue('plan'));
			}

			$msDateTime = explode(' ', ISMSLib::getISMSDate($planTest->getFieldValue('startTime')));
			FWDWebLib::getObject('startTime')->setValue($msDateTime[0]);
			FWDWebLib::getObject('startHour')->setValue($msDateTime[1]);

			$msDateTime = explode(' ', ISMSLib::getISMSDate($planTest->getFieldValue('endTime')));
			FWDWebLib::getObject('endTime')->setValue($msDateTime[0]);
			FWDWebLib::getObject('endHour')->setValue($msDateTime[1]);

			FWDWebLib::getObject('observation')->setValue($planTest->getFieldValue('observation'));
		}else{
			$moWindowTitle->setValue(FWDLanguage::getPHPStringValue('newPlanTest', 'Novo Teste'));
		}

		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
<script language="javascript">
        		gebi('startTime').focus();

				function fillOfflineTest(planTestId, plan){
					window.open("packages/continuity/OfflineTest.php?plan="+plan+"&planTest="+planTestId, 
							null, "height = 808, width = 1148, location=no,locationbar=no,status=no, toolbar = no, menubar=no, scrollbars=yes");
				}
        
      		</script>
		<?
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_plan_test_edit.xml');

?>