<?php
include_once "include.php";
include_once $handlers_ref . "grid/QueryGridProcessActivity.php";
include_once $handlers_ref . "select/QuerySelectArea.php";
include_once $handlers_ref . "select/QuerySelectProcessPriority.php";
include_once $handlers_ref . "select/QuerySelectProcessType.php";

class SearchEvent extends FWDRunnable {
	public function run(){
		$moGrid = FWDWebLib::getObject('grid_process');
		$moGrid->execEventPopulate();
	}
}

class GridActivity extends FWDDrawGrid {
	public function drawItem(){
		switch($this->ciColumnIndex){
			case $this->getIndexByAlias('activity_id'):
				continue;
			default:
				return parent::drawItem();
				break;
		}
	}
}

class ActivityConfirmRemove extends FWDRunnable {
	public function run(){
		$msTitle = FWDLanguage::getPHPStringValue('tt_remove_activity','Remover Atividade');
		$msMessage = FWDLanguage::getPHPStringValue('st_activity_remove_confirm',"Voc� tem certeza que deseja remover a atividade <b>%activity_name%</b>?");

		$moActivity = new CMProcessActivity();
		$moActivity->fetchById(FWDWebLib::getObject('selected_activity_id')->getValue());
		$msActivityName = ISMSLib::truncateString($moActivity->getFieldValue('activity_name'), 70);
		$msMessage = str_replace("%activity_name%",$msActivityName,$msMessage);

		$msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
                    "soWindow = soPopUp.getOpener();" .
                    "soWindow.remove_activity();";

		ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,50);
	}
}

class RemoveActivityEvent extends FWDRunnable {
	public function run(){
		$miActivityId = FWDWebLib::getObject('selected_activity_id')->getValue();
		$moActivity = new CMProcessActivity();
		$moActivity->delete($miActivityId);
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));
		$moStartEvent->addAjaxEvent(new ActivityConfirmRemove('activity_confirm_remove'));
		$moStartEvent->addAjaxEvent(new RemoveActivityEvent('remove_activity_event'));
		$moStartEvent->addAjaxEvent(new SearchEvent('search'));
		FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
		FWDMenuACLSecurity::getInstance()->installMenuACL(FWDWebLib::getObject('menu'),FWDACLSecurity::getInstance());
		$moGrid = FWDWebLib::getObject("grid_activity");
		$maNotAllowed = array();
		if(ISMSLib::getConfigById(ASSET_KEY)){
			$maNotAllowed = array("asset");
			$cols = $moGrid->getColumns();
			foreach($cols as $col){
				if($col->getAttrAlias() == "activity_asset_count"){
					$col->setAttrDisplay("false");
					break;
				}
			}
		}
		$moACL = FWDACLSecurity::getInstance();
		$moACL->setNotAllowed($maNotAllowed);
		$moACL->installSecurity(FWDWebLib::getObject('dialog'), $maNotAllowed);


		$moHandler = new QueryGridProcessActivity(FWDWebLib::getConnection());

		if(!ISMSLib::userHasACL('M.RM.2.1')){
			$moHandler->setUserId(ISMSLib::getCurrentUserId());
		}

		/*
		$miProcessId = FWDWebLib::getObject('par_activity_process_id')->getValue();
		if($miProcessId){
			$moHandler->setProcess($miProcessId);
		}
		*/

		$process = FWDWebLib::getObject('par_activity_process_id')->getValue();
		$processIds = explode(":", $process);
		$processId = $processIds[0];

		if($processId){
			$moHandler->setProcess($processId);
		}

		/*
		if($miAreaId){
			$moHandler->setArea($miAreaId);
			FWDWebLib::getObject('areaId')->setValue($area);
		}
		*/

		$moGrid->setQueryHandler($moHandler);
		$moGrid->setObjFwdDrawGrid(new GridActivity());
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$miProcessId = FWDWebLib::getObject('par_activity_process_id')->getValue();
		FWDWebLib::getObject('activity_process_id')->setValue($miProcessId);

		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));

		if($miProcessId) {
			$activityString = FWDLanguage::getPHPStringValue('tt_activities_bl','Atividades');
			ISMSLib::getPathScrollCode(RISK_MANAGEMENT_MODE, CONTEXT_PROCESS, $miProcessId, $activityString, false, '');
		}

		?>
<script language="javascript">
      
        function refresh_grid() {
          js_refresh_grid('grid_activity');
        }
        
        function set_assets(assets) {
          gebi('selected_assets_ids').value = assets;
          trigger_event('associate_assets_to_process_event', 3);
          refresh_grid();
        }
        
        function remove_activity() {
          trigger_event('remove_activity_event', 3);
          refresh_grid();
        }
        
        function go_to_nav_asset(piId){
          if(gebi('filter_scrolling').value!=0){
            isms_change_to_sibling(3,'nav_asset.php?scrollfilter='+piId+':'+gebi('filter_scrolling').value);
          }else{
            isms_change_to_sibling(3,'nav_asset.php?scrollfilter='+piId);
          }
        }
        
        function go_to_nav_area(piId){
          gebi('selected_process_area').value = piId;
          trigger_event('redirect_to_process_area',3);
        }

        function go_to_nav_process() {
        	parent.soTabSubManager.changeTab(TAB_PROCESS,'nav_process.php');
        }

        function go_to_nav_activity_asset(piActivityId, piProcessId){
            isms_change_to_sibling(TAB_PROCESS,'nav_process_activity_asset.php?activity_id='+piActivityId+'&process_id='+piProcessId);
        }

        function openAssets(piId){
          if(gebi('activity_process_id').value!=0){
            isms_change_to_sibling(TAB_ASSET,'nav_asset.php?activity='+piId+':'+gebi('activity_process_id').value);
          }else{
            isms_change_to_sibling(TAB_ASSET,'nav_asset.php?activity='+piId);
          }
		}

        function enterPlace(place_id) {          
        	isms_change_to_sibling(TAB_PLACE, 'nav_places.php?root='+place_id);
        }          
        
      </script>
		<?

	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_process_activity.xml");
?>