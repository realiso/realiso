<?php

include_once 'include.php';
include_once $handlers_ref . "select/QuerySelectAreaType.php";
include_once $handlers_ref . "select/QuerySelectReadableDocumentByContext.php";

class SavePlanEvent extends FWDRunnable {

	function getPlanFromFields(){
		$moPlan = new CMPlan();

		$msPlanName = FWDWebLib::getObject('name')->getValue();
		$msPlanDescription = FWDWebLib::getObject('description')->getValue();

		$process = FWDWebLib::getObject('processId')->getValue();
		$area = FWDWebLib::getObject('areaId')->getValue();
		$asset = FWDWebLib::getObject('assetId')->getValue();
		$place = FWDWebLib::getObject('placeId')->getValue();

		if($process){
			$moPlan->setFieldValue('process',$process);
		}else if($area){
			$moPlan->setFieldValue('area',$area);
		}else if($asset){
			$moPlan->setFieldValue('asset',$asset);
		}else if($place){
			$moPlan->setFieldValue('place',$place);
		}

		$moPlan->setFieldValue('name',$msPlanName);
		$moPlan->setFieldValue('description',$msPlanDescription);

		return $moPlan;
	}

	public function run(){
		$miPlanId = FWDWebLib::getObject('plan_id')->getValue();

		$moPlan = $this->getPlanFromFields();

		if($miPlanId){
			$moPlan->update($miPlanId,true,true);
		}else{
			$piContextId = $moPlan->insert(true);
		}

		$process = FWDWebLib::getObject("process")->getValue();
		$area = FWDWebLib::getObject("area")->getValue();
		$place = FWDWebLib::getObject("place")->getValue();
		$asset = FWDWebLib::getObject("asset")->getValue();

		if($process){
			FWDWebLib::getObject("processId")->setValue($process);
		}
		if($area){
			FWDWebLib::getObject("areaId")->setValue($area);
		}
		if($place){
			FWDWebLib::getObject("placeId")->setValue($place);
		}
		if($asset){
			FWDWebLib::getObject("assetId")->setValue($asset);
		}

		echo "soWindow = soPopUpManager.getPopUpById('popup_plan_edit').getOpener();"
		."if(soWindow.refresh_grid) soWindow.refresh_grid();"
		."soPopUpManager.closePopUp('popup_plan_edit');";
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(''));
		$moStartEvent->addAjaxEvent(new SavePlanEvent('save_plan_event'));
	}
}

class ScreenEvent extends FWDRunnable {

	private function getProcessName($id){
		$rmProcess = new RMProcess();
		$rmProcess->fetchById($id);
		return $rmProcess->getFieldValue("process_name");
	}

	private function getAreaName($id){
		$rmArea = new RMArea();
		$rmArea->fetchById($id);
		return $rmArea->getFieldValue("area_name");
	}

	private function getAssetName($id){
		$rmAsset = new RMAsset();
		$rmAsset->fetchById($id);
		return $rmAsset->getFieldValue("asset_name");
	}

	private function getPlaceName($id){
		$rmPlace = new CMPlace();
		$rmPlace->fetchById($id);
		return $rmPlace->getFieldValue("place_name");
	}		

	public function run(){
		$miPlanId = FWDWebLib::getObject('param_plan_id')->getValue();
		FWDWebLib::getObject('plan_id')->setValue($miPlanId);
		$moWindowTitle = FWDWebLib::getObject('window_title');

		if($miPlanId){
			$moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_plan_editing', 'Edi��o de Plano'));

			$moPlan = new CMPlan();
			$moPlan->fetchById($miPlanId);
			FWDWebLib::getObject('name')->setValue($moPlan->getFieldValue('name'));
			FWDWebLib::getObject('description')->setValue($moPlan->getFieldValue('description'));

		}else{
			//teste para verificar se o sistema n�o est� sendo hakeado
			$moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_plan_adding', 'Adi��o de Plano'));

			FWDWebLib::getObject("label_context")->setAttrDisplay("true");
			FWDWebLib::getObject("label_context_name")->setAttrDisplay("true");
		}

		$process = FWDWebLib::getObject("process")->getValue();
		$area = FWDWebLib::getObject('area')->getValue();
		$asset = FWDWebLib::getObject('asset')->getValue();
		$place = FWDWebLib::getObject('place')->getValue();

		if($process){
			FWDWebLib::getObject("processId")->setValue($process);

			FWDWebLib::getObject("label_context")->setValue( FWDLanguage::getPHPStringValue('rs_process_cl', 'Processo:') );
			FWDWebLib::getObject("label_context_name")->setValue($this->getProcessName($process));

		}else if($area){
			FWDWebLib::getObject("areaId")->setValue($area);

			FWDWebLib::getObject("label_context")->setValue( FWDLanguage::getPHPStringValue('ti_un', 'Unidade de Neg�cio:') );
			FWDWebLib::getObject("label_context_name")->setValue($this->getAreaName($area));

		}else if($asset){
			FWDWebLib::getObject("assetId")->setValue($asset);

			FWDWebLib::getObject("label_context")->setValue( FWDLanguage::getPHPStringValue('rs_asset_cl', 'Ativo:') );
			FWDWebLib::getObject("label_context_name")->setValue($this->getAssetName($asset));

		}else if($place){
			FWDWebLib::getObject("placeId")->setValue($place);

			FWDWebLib::getObject("label_context")->setValue( FWDLanguage::getPHPStringValue('gc_place', 'Local:') );
			FWDWebLib::getObject("label_context_name")->setValue($this->getPlaceName($place));			
		}

		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
	
		<script language="javascript">
        	gebi('name').focus();
    	</script>

		<?
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_plan_edit.xml');

?>