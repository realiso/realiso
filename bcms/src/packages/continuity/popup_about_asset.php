<?php

include_once 'include.php';
include_once $handlers_ref . "QueryContextHierarchicalStructure.php";
include_once $handlers_ref . 'QueryTotalAssets.php';
include_once $handlers_ref . "select/QuerySelectReadableDocumentByContext.php";
include_once $handlers_ref . "select/continuity/QuerySelectPlace.php";
include_once $handlers_ref . "QueryPlaceFromAsset.php";
include_once $handlers_ref . "select/QueryGridProviderSearch.php";

include_once $handlers_ref . "QueryCheckAssetSerial.php";

function getAssetFromFields(){
	$asset = new RMAsset();
	
	$alias = FWDWebLib::getObject('asset_alias')->getValue();
	$model = FWDWebLib::getObject('asset_model')->getValue();
	$manufacturer = FWDWebLib::getObject('asset_manufacturer')->getValue();
	$version = FWDWebLib::getObject('asset_version')->getValue();
	$build = FWDWebLib::getObject('asset_build')->getValue();
	$serial = FWDWebLib::getObject('asset_serial')->getValue();
	$ip = FWDWebLib::getObject('asset_ip')->getValue();
	$mac = FWDWebLib::getObject('asset_mac')->getValue();
	$providerId = FWDWebLib::getObject('providerId')->getValue();
	$provider = FWDWebLib::getObject('provider')->getValue();
	
	$asset->setFieldValue('serial', $serial);
	$asset->setFieldValue('alias', $alias);
	$asset->setFieldValue('model', $model);
	$asset->setFieldValue('manufacturer', $manufacturer);
	$asset->setFieldValue('version', $version);
	$asset->setFieldValue('build', $build);
	$asset->setFieldValue('ip', $ip);
	$asset->setFieldValue('mac', $mac);

	if(!$providerId){
		$asset->setFieldValue('provider', 'null');
	} else {
		$asset->setFieldValue('provider', $providerId);
	}

	return $asset;
}

class SearchProvider extends FWDRunnable {
	public function run(){
		$provider = FWDWebLib::getObject('provider')->getValue();
		$providerId = FWDWebLib::getObject('providerId')->getValue();
		$providerOld = FWDWebLib::getPOST('providerOld');

		if($provider != $providerOld && trim($provider) != ""){
			$query = new QueryGridProviderSearch(FWDWebLib::getConnection());
			$query->setNameFilter($provider);
			$query->setLimit(5);
			$query->makeQuery();
			$query->executeQuery();

			$providerTop = FWDWebLib::getObject('provider')->getAttrTop()+22;
			$providerLeft = FWDWebLib::getObject('provider')->getAttrLeft();

			echo "var position = {top: {$providerTop}, left:{$providerLeft}}; ";
			echo "var listElements = new Array(); ";
			$i = 0;
			$found = false;
			while($query->fetch()){
				$found = true;
				$name = $query->getFieldValue("provider_name");
				$id = $query->getFieldValue("provider_id");
				echo "listElements[{$i}] = {name:'{$name}', id:'{$id}'}; ";
				echo "gebi('providerId').value = '0';";
				$i++;
			}

			if(!$found){
				echo "listElements[{$i}] = {name:'', id:''}; ";
				echo "gebi('providerId').value = '0';";
			}

			echo "createAutoCompleteDiv(listElements, 'provider', 'providerId', position);";
		}
		echo "gebi('providerOld').value = '{$provider}';";
	}
}

class SaveAssetEvent extends FWDRunnable {
	public function run(){
		$provider = FWDWebLib::getObject('provider')->getValue();
		$miAssetId = FWDWebLib::getObject('assetId')->getValue();
		$serial = FWDWebLib::getObject('asset_serial')->getValue();
		
		$providerId = FWDWebLib::getObject('providerId')->getValue();
		
		if($miAssetId){
			$asset = getAssetFromFields();
			$asset->update($miAssetId);
		}
		
		/* N�o permite que o usu�rio salve com o mesmo n�mero de s�rie */
		$moQuery = new QueryCheckAssetSerial(FWDWebLib::getConnection());
		$moQuery->setAssetSerial($serial);
		$moQuery->makeQuery();
		$moQuery->fetchAssetSerial();
		$duplicateAssetSerial = $moQuery->getAssetSerial();
		
		$asset = getAssetFromFields();
		if($serial){
			if($duplicateAssetSerial == true){
				
				echo "gobi('warning_serial_exists').show();";				
			}else{
				$asset->setFieldValue('serial', $serial);

				if($provider == '' || $providerId > 0){
					//$asset->setFieldValue('provider', $providerId);
					echo "soWindow = soPopUpManager.getPopUpById('popup_about_asset').getOpener();"
					."soPopUpManager.closePopUp('popup_about_asset');";
				} else if ($provider != '' && $providerId == 0){
					echo "gobi('warning_no_provider').show();";
				}
			}
		}else{
			$asset->setFieldValue('serial', $serial);
			if($provider != '' && $providerId == 0){
				echo "gobi('warning_no_provider').show();";
			}else{
				$asset->setFieldValue('provider', $provider);
				echo "soWindow = soPopUpManager.getPopUpById('popup_about_asset').getOpener();"
				."soPopUpManager.closePopUp('popup_about_asset');";
			}
		}
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$miAssetId = FWDWebLib::getObject('param_asset_id')->getValue();
		FWDWebLib::getObject('assetId')->setValue($miAssetId);

		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(''));
		$moStartEvent->addAjaxEvent(new SaveAssetEvent('save_asset'));
		$moStartEvent->addAjaxEvent(new SearchProvider('search_provider'));
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$miAssetId = FWDWebLib::getObject('assetId')->getValue();
		$moWindowTitle = FWDWebLib::getObject('window_title');

		if($miAssetId){
			$asset = new RMAsset();
			$asset->fetchById($miAssetId);

			FWDWebLib::getObject('asset_alias')->setValue($asset->getFieldValue('alias'));
			FWDWebLib::getObject('asset_model')->setValue($asset->getFieldValue('model'));
			FWDWebLib::getObject('asset_manufacturer')->setValue($asset->getFieldValue('manufacturer'));
			FWDWebLib::getObject('asset_version')->setValue($asset->getFieldValue('version'));
			FWDWebLib::getObject('asset_build')->setValue($asset->getFieldValue('build'));
			FWDWebLib::getObject('asset_serial')->setValue($asset->getFieldValue('serial'));
			FWDWebLib::getObject('asset_ip')->setValue($asset->getFieldValue('ip'));
			FWDWebLib::getObject('asset_mac')->setValue($asset->getFieldValue('mac'));
			
			FWDWebLib::getObject('providerId')->setValue($asset->getFieldValue('provider'));

			$providerId = $asset->getFieldValue('provider');

			if($providerId){
				$provider = new CMProvider();
				$provider->fetchById($providerId);
				FWDWebLib::getObject('provider')->setValue($provider->getFieldValue("name"));
				FWDWebLib::getObject('provider')->setIdValue($providerId);
			}

			$moWindowTitle->setValue(FWDLanguage::getPHPStringValue('about_asset_edit', 'Sobre este Ativo'));
		}

		// coloca o simbolo de moeda

		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
<script language="javascript">
        gebi('asset_alias').focus();
      </script>
		<?
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_about_asset.xml');

?>