<?php
include_once 'include.php';
include_once $handlers_ref . "select/improvement/QuerySelectNCProcess.php";

class SaveProcessDrawEvent extends FWDRunnable {
  public function run(){
  $msInput = FWDWebLib::getObject('area_input')->getValue();
  $msOutput = FWDWebLib::getObject('area_output')->getValue();
  $msDescription = FWDWebLib::getObject('area_description')->getValue();
  
  $msProcessInputs = FWDWebLib::getObject('process_input_ids')->getValue();
  $msProcessOutputs = FWDWebLib::getObject('process_output_ids')->getValue();
  
  $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
  
  if($moSession->attributeExists("processDescription")) {
  	$moSession->deleteAttribute("processDescription");
  }
  
  if($msDescription) {
  	$moSession->addAttribute("processDescription");
  	$moSession->setAttrProcessDescription(serialize($msDescription));
  }

  
  if($moSession->attributeExists("processInput")) {
  	$moSession->deleteAttribute("processInput");
  }
  
  if($msInput) {
  	$moSession->addAttribute("processInput");
  	$moSession->setAttrProcessInput(serialize($msInput));
  }

  if($moSession->attributeExists("processOutput")) {
  	$moSession->deleteAttribute("processOutput");
  }
  
  if($msOutput) {
  	$moSession->addAttribute("processOutput");
  	$moSession->setAttrProcessOutput(serialize($msOutput));
  }
  
  if(!$moSession->attributeExists("processInputIds")) {
    	$moSession->addAttribute("processInputIds");
  }
  $moSession->setAttrProcessInputIds(serialize(FWDWebLib::getObject('process_input_ids')->getValue()));
  
  if(!$moSession->attributeExists("processOutputIds")) {
  	 	$moSession->addAttribute("processOutputIds");
  }
  $moSession->setAttrProcessOutputIds(serialize(FWDWebLib::getObject('process_output_ids')->getValue()));
  
      echo "soPopUpManager.closePopUp('popup_process_draw');";
  }
}


class RefreshInputProcessesEvent extends FWDRunnable {
  public function run(){
    $moHandler = new QuerySelectNcProcess();
    $moHandler->setProcessIds(FWDWebLib::getObject('process_input_ids')->getValue());
    $moSelect = FWDWebLib::getObject('nc_inputs');
    $moSelect->setQueryHandler($moHandler);
    $moSelect->populate();
    $moSelect->execEventPopulate();
    
    $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    
    if(!$moSession->attributeExists("processInputIds")) {
    	$moSession->addAttribute("processInputIds");
    }
    $moSession->setAttrProcessInputIds(serialize(FWDWebLib::getObject('process_input_ids')->getValue()));
    
  }
}

class RefreshOutputProcessesEvent extends FWDRunnable {
  public function run(){
    $moHandler = new QuerySelectNcProcess();
    $moHandler->setProcessIds(FWDWebLib::getObject('process_output_ids')->getValue());    
    $moSelect = FWDWebLib::getObject('nc_outputs');
    $moSelect->setQueryHandler($moHandler);
    $moSelect->populate();
    $moSelect->execEventPopulate();
    
    $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    
    if(!$moSession->attributeExists("processOutputIds")) {
    	$moSession->addAttribute("processOutputIds");
    }
    $moSession->setAttrProcessOutputIds(serialize(FWDWebLib::getObject('process_output_ids')->getValue()));
    
  }
}


class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
    $moStartEvent->addAjaxEvent(new RefreshInputProcessesEvent('refresh_input_processes'));
    $moStartEvent->addAjaxEvent(new RefreshOutputProcessesEvent('refresh_output_processes'));
    $moStartEvent->addAjaxEvent(new SaveProcessDrawEvent('save_process_draw_event'));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
  	$moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
  	if($moSession->attributeExists("processDescription")) {
  		$moDescription = unserialize($moSession->getAttrProcessDescription());
  		FWDWebLib::getObject('area_description')->setValue($moDescription);
  	}
  	
  	if($moSession->attributeExists("processInput")) {
  		$moInput = unserialize($moSession->getAttrProcessInput());
  		FWDWebLib::getObject('area_input')->setValue($moInput);
  	}

  	
  	if($moSession->attributeExists("processOutput")) {  	
  		$moOutput = unserialize($moSession->getAttrProcessOutput());
  		FWDWebLib::getObject('area_output')->setValue($moOutput);
  	}

  	if($moSession->attributeExists("processInputIds")) {  	
  		$moInputIds = unserialize($moSession->getAttrProcessInputIds());
  		FWDWebLib::getObject('process_input_ids')->setValue($moInputIds);
  	}

  	if($moSession->attributeExists("processOutputIds")) {  	
  		$moOutputIds = unserialize($moSession->getAttrProcessOutputIds());
  		FWDWebLib::getObject('process_output_ids')->setValue($moOutputIds);
  	}  	
  	
  	?>
  	<script language="javascript">
  	var changing = null;
  	</script>
  	<?
  	  	
	FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
	
	?>
	<script language="javascript">
	   function set_processes(psCurrentIds) {
		   if(changing == 1) {
          		gebi('process_input_ids').value = psCurrentIds;
          		psCurrentIds=null;
          		trigger_event('refresh_input_processes',3);
		   } else {
         		gebi('process_output_ids').value = psCurrentIds;
          		psCurrentIds=null;
          		trigger_event('refresh_output_processes',3);
		   }			   
        }
    </script>
	<?
	
	if($moSession->attributeExists("processOutputIds")) {
  	?>
  	<script language="javascript">
  		trigger_event('refresh_output_processes',3);
  	</script>
  	<?
	}
	
	if($moSession->attributeExists("processInputIds")) {
  	?>
  	<script language="javascript">
  		trigger_event('refresh_input_processes',3);
  	</script>
  	<?
	}
	
	
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_process_draw.xml');

?>