<?php
include_once "include.php";
include_once $handlers_ref . "grid/QueryGridPlanTest.php";
include_once $handlers_ref . "select/QuerySelectCategory.php";

class PlanTestConfirmRemove extends FWDRunnable {
	public function run(){
		$msTitle = FWDLanguage::getPHPStringValue('tt_remove_test','Remover Teste');
		$msMessage = FWDLanguage::getPHPStringValue('st_test_remove_confirm',"Voc� tem certeza que deseja remover o teste?");

		$msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
                    "soWindow = soPopUp.getOpener();" .
                    "soWindow.removePlanTest();";

		ISMSLib::openConfirm($msTitle, $msMessage, $msEventValue);
	}
}

class RemovePlanTestEvent extends FWDRunnable {
	public function run(){
		$miSelectedId = FWDWebLib::getObject('selected_plan_test_id')->getValue();

		$planActions = new CMPlanActionTest();
		$planActions->createFilter($miSelectedId, 'planTest');
		$planActions->select();
		while($planActions->fetch()){
			$planActions->delete($planActions->getFieldValue("id"));
		}
		$moPlanTest = new CMPlanTest();
		$moPlanTest->delete($miSelectedId);
	}
}

class StartOnlineTest extends FWDRunnable{
	public function run(){
		$planForTestId = FWDWebLib::getObject('planForTestId')->getValue();
		
		$plan = new CMPlan();
		$plan->fetchById($planForTestId);
		$responsible = $plan->getResponsibleId();
		
		$test = new CMPlanTest();
		$test->setFieldValue("plan", $planForTestId);
		$test->setFieldValue("startTime", time());
		$test->setFieldValue("responsible", $responsible);
		$id = $test->insert(true);
		
		echo "window.open(\"OnlineTest.php?planTest={$id}&plan={$planForTestId}\", null, \"height = 808, width = 1148, status=no, location=0,locationbar=0,toolbar=0, menubar=no, scrollbars=yes\");refresh_grid();";
	}
}

class GridPlanTest extends FWDDrawGrid {
	public function drawItem(){
		switch($this->ciColumnIndex){
			default:
				return parent::drawItem();
				break;
		}
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(''));
		$moStartEvent->addAjaxEvent(new PlanTestConfirmRemove('planTestConfirmRemove'));
		$moStartEvent->addAjaxEvent(new RemovePlanTestEvent('remove_plan_test_event'));
		$moStartEvent->addAjaxEvent(new ChangeScrollingPath('change_scrolling_path'));
		$moStartEvent->addAjaxEvent(new StartOnlineTest('startOnlineTest'));

		$grid = FWDWebLib::getObject('grid_plan');

		$plan = FWDWebLib::getObject('plan')->getValue();
		FWDWebLib::getObject('planId')->setValue($plan);

		$moHandler = new QueryGridPlanTest(FWDWebLib::getConnection());
		$moHandler->setPlan($plan);
		$grid->setQueryHandler($moHandler);
		$grid->setObjFwdDrawGrid(new GridPlanTest());
	}
}

class ChangeScrollingPath extends FWDRunnable{
	public function run(){
		echo ISMSLib::getPathScrollCode(RISK_MANAGEMENT_MODE,CONTEXT_CM_GROUP,FWDWebLib::getObject('root_plan_id')->getValue(),true,'enterPlanTest');
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){

		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
<script language="javascript">
        function refresh_grid(){
          js_refresh_grid('grid_plan');
        }
		function remove(){
            trigger_event('remove_event',3);
			refresh_grid();
		}

		function removePlanTest(){
			trigger_event('remove_plan_test_event',3);
			refresh_grid();
		}

        function goToPlan() {
        	parent.soTabSubManager.changeTab(TAB_PLAN,'nav_plan.php');
        }

        function fillOfflineTest(planTestId){
			window.open("OfflineTest.php?plan="+gebi("planId").value+"&planTest="+planTestId, 
					null, "height = 808, width = 1148, status=no, toolbar = no, menubar=no, scrollbars=yes");
		}

        function fillOnlineTest(planTestId){
			window.open("OnlineTest.php?plan="+gebi("planId").value+"&planTest="+planTestId, 
					null, "height = 808, width = 1148, status=no, toolbar = no, menubar=no, scrollbars=yes");
		}

        function startTest(id){
			gebi("planForTestId").value = id;
			trigger_event('startOnlineTest', 3);
		}
        
      </script>
		<?
		$plan = FWDWebLib::getObject("planId")->getValue();
		ISMSLib::getPathScrollCode(CONTEXT_CM_PLAN, CONTEXT_CM_PLAN_TEST, $plan, '', false, '');
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_plan_test.xml");
?>