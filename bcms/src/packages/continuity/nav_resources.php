<?php
include_once "include.php";
include_once $handlers_ref . "grid/QueryGridResource.php";
include_once $handlers_ref . "select/QuerySelectCategory.php";

class GroupConfirmRemove extends FWDRunnable {
	public function run(){
		$msTitle = FWDLanguage::getPHPStringValue('tt_remove_group','Remover Grupo');
		$msMessage = FWDLanguage::getPHPStringValue('st_group_remove_confirm',"Voc� tem certeza que deseja remover o grupo <b>%group_name%</b>?");

		$moGroup = new CMGroup();
		$moGroup->fetchById(FWDWebLib::getObject('selected_group_id')->getValue());
		$msGroupName = ISMSLib::truncateString($moGroup->getFieldValue('group_name'), 70);
		$msMessage = str_replace("%group_name%",$msGroupName,$msMessage);

		$msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
                    "soWindow = soPopUp.getOpener();" .
                    "soWindow.remove();";

		ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue);
	}
}

class ResourceConfirmRemove extends FWDRunnable {
	public function run(){
		$msTitle = FWDLanguage::getPHPStringValue('tt_remove_resource','Remover Recurso');
		$msMessage = FWDLanguage::getPHPStringValue('st_resource_remove_confirm',"Voc� tem certeza que deseja remover o recurso <b>%resource_name%</b>?");
		$resourceId = FWDWebLib::getObject('selected_resource_id')->getValue();

		$moResource = new CMResource();
		$moResource->fetchById($resourceId);
		$msResourceName = ISMSLib::truncateString($moResource->getFieldValue('resource_name'), 70);
		$msMessage = str_replace("%resource_name%",$msResourceName,$msMessage);

		$groups = "";
		$query = new FWDDBDataSet(FWDWebLib::getConnection());
		$query->setQuery("select g.name as name from view_cm_group_resource_active gr join view_cm_group_active g on g.fkcontext = gr.fkgroup where gr.fkresource = {$resourceId}");
		$query->addFWDDBField(new FWDDBField('name','name'  ,DB_STRING));
		$query->execute();
		while($query->fetch()){
			$name = $query->getFieldByAlias("name")->getValue();
			$groups .= $name.", ";
		}
		$height = 0;

		if(strlen($groups) > 0){
			$groups = substr($groups, 0, strlen($groups) - 2);

			$msMessageGroup = FWDLanguage::getPHPStringValue('currentGroupsRemove',"<br/>Este recurso faz parte do(s) grupo(s) <b>%groups%</b>.");

			$msMessageGroup = str_replace("%groups%", $groups, $msMessageGroup);

			$msMessage .= $msMessageGroup;
			$height = 70;
		}

		$msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
                    "soWindow = soPopUp.getOpener();" .
                    "soWindow.removeResource();";

		ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue, $height);
	}
}

class ResourceConfirmRemoveFromGroup extends FWDRunnable {
	public function run(){
		$msTitle = FWDLanguage::getPHPStringValue('tt_remove_resource','Remover Recurso');
		$msMessage = FWDLanguage::getPHPStringValue('st_resource_remove_from_group_confirm',"Voc� tem certeza que deseja remover o recurso <b>%resource_name%</b> do grupo <b>%group_name%</b>?");

		$resourceId = FWDWebLib::getObject('selected_resource_id')->getValue();
		$groupId = FWDWebLib::getObject('root_group_id')->getValue();

		$group = new CMGroup();
		$group->fetchById($groupId);

		$moResource = new CMResource();
		$moResource->fetchById($resourceId);

		$msResourceName = ISMSLib::truncateString($moResource->getFieldValue('resource_name'), 70);
		$msMessage = str_replace("%resource_name%",$msResourceName,$msMessage);

		$groupName = ISMSLib::truncateString($group->getFieldValue('group_name'), 70);
		$msMessage = str_replace("%group_name%", $groupName, $msMessage);

		$msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
                    "soWindow = soPopUp.getOpener();" .
                    "soWindow.removeResourceFromGroup();";

		ISMSLib::openConfirm($msTitle, $msMessage, $msEventValue);
	}
}

class RemoveResourceFromGroupEvent extends FWDRunnable {
	public function run(){

		$resourceId = FWDWebLib::getObject('selected_resource_id')->getValue();
		$groupId = FWDWebLib::getObject('root_group_id')->getValue();

		$groupResource = new CMGroupResource();
		$groupResource->createFilter($resourceId, "resource_id");
		$groupResource->createFilter($groupId, "group_id");
		$groupResource->select();
		while($groupResource->fetch()){
			$groupResource->delete($groupResource->getFieldValue("group_resource_id"), true);
		}
	}
}

class RemoveResourceEvent extends FWDRunnable {
	public function run(){
		$miSelectedId = FWDWebLib::getObject('selected_resource_id')->getValue();
		$moResource = new CMResource();
		$moResource->delete($miSelectedId);

		$groupResource = new CMGroupResource();
		$groupResource->createFilter($miSelectedId, 'resource_id');
		$groupResource->select();
		while($groupResource->fetch()){
			$groupResource->delete($groupResource->getFieldValue('group_resource_id'));
		}

		$placeResource = new CMPlaceResource();
		$placeResource->createFilter($miSelectedId, 'resource_id');
		$placeResource->select();
		while($placeResource->fetch()){
			$placeResource->delete($placeResource->getFieldValue('id'));
		}

		$areaResource = new CMAreaResource();
		$areaResource->createFilter($miSelectedId, 'resource_id');
		$areaResource->select();
		while($areaResource->fetch()){
			$areaResource->delete($areaResource->getFieldValue('id'));
		}
	}
}

class RemoveEvent extends FWDRunnable {
	public function run(){
		$miSelectedId = FWDWebLib::getObject('selected_group_id')->getValue();
		$moGroup = new CMGroup();
		$moGroup->delete($miSelectedId);

		$groupResource = new CMGroupResource();
		$groupResource->createFilter($miSelectedId, 'group_id');
		$groupResource->select();
		while($groupResource->fetch()){
			$groupResource->delete($groupResource->getFieldValue('group_resource_id'));
		}

		$placeResource = new CMPlaceResource();
		$placeResource->createFilter($miSelectedId, 'group_id');
		$placeResource->select();
		while($placeResource->fetch()){
			$placeResource->delete($placeResource->getFieldValue('id'));
		}

		$areaResource = new CMAreaResource();
		$areaResource->createFilter($miSelectedId, 'group_id');
		$areaResource->select();
		while($areaResource->fetch()){
			$areaResource->delete($areaResource->getFieldValue('id'));
		}
	}
}

class GridResource extends FWDDrawGrid {
	public function drawItem(){
		switch($this->ciColumnIndex){
			case $this->getIndexByAlias('resource_img'):
				$msIconRef = '';
				$maNotAllowed=array();
				if($this->getFieldValue('resource_type') == 'resource'){
					$msIconRef="icon-risk_gray.gif";
					$maNotAllowed[] = "group";
				}else if($this->getFieldValue('resource_type') == 'group'){
					$msIconRef="icon-category.gif";
					$maNotAllowed[] = "user";
				}

				$group = FWDWebLib::getObject("root_group_id")->getValue();
				if(!$group){
					$maNotAllowed[] = "userInGroup";
				}

				$moACL = FWDACLSecurity::getInstance();
				$moACL->setNotAllowed($maNotAllowed);
				$moMenu = FWDWebLib::getObject('menu');
				$moGrid = FWDWebLib::getObject('grid_resource');
				$msLine = $moGrid->getAttrName().'_acl_'.$this->ciRowIndex;
				FWDGridMenuACLSecurity::getInstance()->installGridMenuACL($moMenu,$moACL,$moGrid,$msLine);
				$this->coCellBox->setIconSrc($msIconRef);
				return parent::drawItem();
				break;
			case $this->getIndexByAlias('resource_name'):

				$detail = $this->getFieldValue('resource_detail');

				$value = $this->coCellBox->getValue();

				if($this->getFieldValue('resource_type') == 'group'){
					if(ISMSLib::userHasACL('M.L.1.1')){
						$value = "<a href=javascript:enterGroup(".$this->getFieldValue('resource_id').");>".$this->getFieldValue('resource_name')."</a>";
					}
				}
				if(strpos($detail, "0 ") === 0){
					$value = "<font color=#F58021>".$value."</font>";
				}
				$this->coCellBox->setValue($value);
				return $this->coCellBox->draw();
				break;
			case $this->getIndexByAlias('resource_detail'):

				$detail = $this->getFieldValue('resource_detail');

				$value = $this->coCellBox->getValue();

				if(strpos($detail, "0 ") === 0){
					$value = "<font color=#F58021>".$value."</font>";
				}
				$this->coCellBox->setValue($value);
				return $this->coCellBox->draw();
				break;
			case $this->getIndexByAlias('resource_acronym'):

				$detail = $this->getFieldValue('resource_detail');

				$value = $this->coCellBox->getValue();

				if(strpos($detail, "0 ") === 0){
					$value = "<font color=#F58021>".$value."</font>";
				}
				$this->coCellBox->setValue($value);
				return $this->coCellBox->draw();
				break;

			default:
				return parent::drawItem();
				break;
		}
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(''));
		$moStartEvent->addAjaxEvent(new GroupConfirmRemove('groupConfirmRemove'));
		$moStartEvent->addAjaxEvent(new ResourceConfirmRemove('resourceConfirmRemove'));
		$moStartEvent->addAjaxEvent(new ResourceConfirmRemoveFromGroup('resourceConfirmRemoveFromGroup'));
		$moStartEvent->addAjaxEvent(new RemoveEvent('remove_event'));
		$moStartEvent->addAjaxEvent(new RemoveResourceEvent('remove_resource_event'));
		$moStartEvent->addAjaxEvent(new RemoveResourceFromGroupEvent('remove_resource_from_group_event'));

		$moStartEvent->addAjaxEvent(new ChangeScrollingPath('change_scrolling_path'));

		FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
		FWDMenuACLSecurity::getInstance()->installMenuACL(FWDWebLib::getObject('menu'),FWDACLSecurity::getInstance());

		$grid = FWDWebLib::getObject('grid_resource');
		$moHandler = new QueryGridResource(FWDWebLib::getConnection());
    
		$nameFilter = FWDWebLib::getObject("nameFilter")->getValue();
		if($nameFilter){
			$moHandler->setNameFilter($nameFilter);
		}

		$group = FWDWebLib::getObject("root_group_id")->getValue();
		if($group){
			$moHandler->setGroup($group);
		}

		/*
		 * Mantido para mostrar CMT de um local.
		 */
		$place = FWDWebLib::getObject("place")->getValue();

		$cmt = FWDWebLib::getObject("cmt")->getValue();
		if($place){
			if($cmt){
				FWDWebLib::getObject("associatePlaceStr")->setValue(FWDLanguage::getPHPStringValue('associatePlaceStr','Associar Recursos'));
				$moHandler->setCMT(1);
				FWDWebLib::getObject("isCMT")->setValue(1);
			}
			
			$cols = $grid->getColumns();
			foreach($cols as $col){
				if($col->getAttrAlias() == "department"
				|| $col->getAttrAlias() == "func"
				|| $col->getAttrAlias() == "comercialNumber"
				|| $col->getAttrAlias() == "celNumber"
				//|| $col->getAttrAlias() == "homeNumber"
				|| $col->getAttrAlias() == "nextelNumber"
				|| $col->getAttrAlias() == "email")
				{
					$col->setAttrDisplay("true");
				}

				if($col->getAttrAlias() == "resource_acronym"
				|| $col->getAttrAlias() == "resource_detail")
				{
					$col->setAttrDisplay("false");
				}
			}
			
			$moHandler->setPlace($place);
			FWDWebLib::getObject("placeId")->setValue($place);
		}

		/*
		 * Unidade de negocio
		 */
    	$ids = FWDWebLib::getObject("un_id")->getValue();

    	$idsArray = explode(":", $ids);

    	$unId = 0;
    	if(count($idsArray))
    		$unId = $idsArray[0];
    	else
    		$unId = $ids;

		if($unId){
		  $cols = $grid->getColumns();
		  
		  foreach($cols as $col){
		    
			  if($col->getAttrAlias() == "department"
			    || 	$col->getAttrAlias() == "func"
			    || 	$col->getAttrAlias() == "comercialNumber"
			    || 	$col->getAttrAlias() == "celNumber"
			    || 	$col->getAttrAlias() == "nextelNumber"
			    || 	$col->getAttrAlias() == "email")
			  {	
				  $col->setAttrDisplay("true");
			  }

			  if($col->getAttrAlias() == "resource_acronym"
			    || $col->getAttrAlias() == "resource_detail")
			  {
				  $col->setAttrDisplay("false");
			  }
			  
		  }
		  
  		$moHandler->setUN($unId);
  		FWDWebLib::getObject("unId")->setValue($unId);
	  }
    

		$grid->setQueryHandler($moHandler);
		$grid->setObjFwdDrawGrid(new GridResource());
	}
}

class ChangeScrollingPath extends FWDRunnable{
	public function run(){
		$ids = "";
		$place = FWDWebLib::getObject("place")->getValue();
		$area = FWDWebLib::getObject("area")->getValue();
		$cmt = FWDWebLib::getObject("cmt")->getValue();

		$ids .= FWDWebLib::getObject('root_group_id')->getValue();
		if($place){
			$ids .= ":". 0;
			$ids .= ":". $place;
			$ids .= ":". $cmt;
		}else if($area){
			$ids .= ":". $area;
		}
		echo ISMSLib::getPathScrollCode(RISK_MANAGEMENT_MODE, CONTEXT_CM_GROUP, $ids, '', true, 'enterGroup');
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){

		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
<script language="javascript">

		function refresh_grid(){
			js_refresh_grid('grid_resource');
		}
		
		function remove(){
			trigger_event('remove_event',3);
			refresh_grid();
		}
		
		function removeResource(){
			trigger_event('remove_resource_event',3);
			refresh_grid();
		}

		function removeResourceFromGroup(){
			trigger_event('remove_resource_from_group_event',3);
			refresh_grid();
		}

		function enterGroup(resourceId) {
			gebi('root_group_id').value = resourceId;
			if (resourceId > 0) {
				js_hide('button_insert_group');
				js_show('button_associate_resources');
			}
			else {
				js_show('button_insert_group');
				js_hide('button_associate_resources');
			}			
			refresh_grid();
			trigger_event('change_scrolling_path',2);
		}
		
		function goToArea(){
			isms_change_to_sibling(TAB_AREA,'nav_un.php');
		}
		
		function goToPlaces(){
			isms_change_to_sibling(TAB_PLACE,'nav_places.php');
		}

		function associateResources(){
			isms_open_popup('popup_resource_search','packages/continuity/popup_resource_search.php?group='+gebi('root_group_id').value,'','true');
		}
		
		function associatePlaceResource(){
			isms_open_popup('popup_resource_search','packages/continuity/popup_resource_search.php?place='+gebi('placeId').value+'&cmt='+gebi('isCMT').value,'','true');
		}

		function insertResource(){
			var placeId = gebi('placeId').value;
			var isCMT = gebi('isCMT').value;
			var groupId = gebi('root_group_id').value;
			var unId = gebi('unId').value;

			isms_open_popup('popup_resource_edit','packages/continuity/popup_resource_edit.php?cmt='+isCMT+'&un_id='+unId+'&place='+placeId+'&group='+groupId, '', 'true');
		}

        function enterPlace(place_id) {          
        	isms_change_to_sibling(TAB_PLACE, 'nav_places.php?root='+place_id);
        }

	    function enter_area(area_id) {
	    	isms_change_to_sibling(TAB_AREA, 'nav_un.php?root='+area_id);
	    }      		

		if(gebi('placeId').value == 0){
			js_hide('button_insert_place_resource');
		}

		if(gebi('root_group_id').value == 0){
			js_hide('button_associate_resources');
		}

		if(gebi('areaId').value == 0){
			js_hide('button_insert_area_resource');
		}

		if(gebi('isCMT').value == 1){
			js_hide('button_insert_area_resource');
			js_hide('button_insert_group');
		}

      </script>
		<?
		$unId = FWDWebLib::getObject("un_id")->getValue();

		$place = FWDWebLib::getObject("place")->getValue();
		$cmt = FWDWebLib::getObject("cmt")->getValue();

		if($place && $cmt){
			$cmtString = FWDLanguage::getPHPStringValue('CMT','CMT');
			ISMSLib::getPathScrollCode(RISK_MANAGEMENT_MODE, CONTEXT_CM_PLACE, $place, $cmtString, false, '');
		} elseif($unId){

			$resourceString = FWDLanguage::getPHPStringValue('resources','Recursos');
			ISMSLib::getPathScrollCode(RISK_MANAGEMENT_MODE, CONTEXT_AREA, $unId, $resourceString, false, '');

		} else {
			ISMSLib::getPathScrollCode(RISK_MANAGEMENT_MODE, CONTEXT_CM_RESOURCE, 0, '', false, '');
		}
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_resources.xml");
?>