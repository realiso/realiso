<?php
include_once "include.php";
include_once $handlers_ref . "grid/QueryGridPlan.php";
include_once $handlers_ref . "select/QuerySelectCategory.php";

class PlanConfirmRemove extends FWDRunnable {
	public function run(){
		$msTitle = FWDLanguage::getPHPStringValue('tt_remove_plan','Remover Plano');
		$msMessage = FWDLanguage::getPHPStringValue('st_plan_remove_confirm',"Voc� tem certeza que deseja remover o plano <b>%plan_name%</b>?");

		$moPlan = new CMPlan();
		$moPlan->fetchById(FWDWebLib::getObject('selected_plan_id')->getValue());
		$msPlanName = ISMSLib::truncateString($moPlan->getFieldValue('name'), 70);
		$msMessage = str_replace("%plan_name%",$msPlanName,$msMessage);

		$msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
                    "soWindow = soPopUp.getOpener();" .
                    "soWindow.removePlan();";

		ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue);
	}
}

class StartOnlineTest extends FWDRunnable{
	public function run(){
		$planForTestId = FWDWebLib::getObject('planForTestId')->getValue();

		$test = new CMPlanTest();
		$test->setFieldValue("plan", $planForTestId);
		$test->setFieldValue("startTime", time());
		$id = $test->insert(true);

		echo "window.open(\"OnlineTest.php?planTest={$id}&plan={$planForTestId}\", null, \"height = 808, width = 1148, status=no, location=0,locationbar=0,toolbar=0, menubar=no, scrollbars=yes\");enterTests({$planForTestId});";
	}
}

class RemovePlanEvent extends FWDRunnable {
	public function run(){
		$miSelectedId = FWDWebLib::getObject('selected_plan_id')->getValue();
		$plan = new CMPlan();
		$plan->removePlan($miSelectedId);
	}
}

class GridPlan extends FWDDrawGrid {
	public function drawItem(){
		switch($this->ciColumnIndex){
			case $this->getIndexByAlias('nextTest'):
				$next = $this->getFieldValue('nextTest');
				if($next){
					$msDateTime = explode(' ', ISMSLib::getISMSDate($next));
					$next = $msDateTime[0]." ".$msDateTime[1];
					$this->coCellBox->setValue($next);
				}
				return parent::drawItem();
				break;
			case $this->getIndexByAlias('origin'):
				$origin = $this->getFieldValue('origin');

				if($origin == "process"){
					$origin = FWDLanguage::getPHPStringValue('process','Processo');
				} else if($origin == "place"){
					$origin = FWDLanguage::getPHPStringValue('place','Local');
				} else if($origin == "asset"){
					$origin = FWDLanguage::getPHPStringValue('asset','Ativo');
				} else if($origin == "area"){
					$origin = FWDLanguage::getPHPStringValue('area','Unidade de Neg�cio');
				} else{
					$origin = FWDLanguage::getPHPStringValue('other','Outro');
				}

				$this->coCellBox->setValue($origin);
				return parent::drawItem();
				break;
			default:
				return parent::drawItem();
				break;
		}
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(''));
		$moStartEvent->addAjaxEvent(new PlanConfirmRemove('planConfirmRemove'));
		$moStartEvent->addAjaxEvent(new RemovePlanEvent('remove_plan_event'));
		$moStartEvent->addAjaxEvent(new ChangeScrollingPath('change_scrolling_path'));
		$moStartEvent->addAjaxEvent(new StartOnlineTest('startOnlineTest'));

		$grid = FWDWebLib::getObject('grid_plan');
		$moHandler = new QueryGridPlan(FWDWebLib::getConnection());

		// Process
		$processIds = FWDWebLib::getObject("process")->getValue();
		$processIdsArray = explode(":", $processIds);

		$process = 0;
		if(count($processIdsArray))
			$process = $processIdsArray[0];
		else
			$process = $processIds;

		// Area
		$areaIds = FWDWebLib::getObject('area')->getValue();
		$areaIdsArray = explode(":", $areaIds);

		$area = 0;
		if(count($areaIdsArray))
			$area = $areaIdsArray[0];
		else
			$area = $areaIds;

		// Asset
		$assetIds = FWDWebLib::getObject('asset')->getValue();
		$assetIdsArray = explode(":", $assetIds);

		$asset = 0;
		if(count($assetIdsArray))
			$asset = $assetIdsArray[0];
		else
			$asset = $assetIds;

		// Place
		$place = FWDWebLib::getObject('place')->getValue();

		$maNotAllowed = array();

		if($process){
			$maNotAllowed = array("asset", "area", "place");
			$moHandler->setProcess($process);
			FWDWebLib::getObject("processId")->setValue($process);
		}else if($area){
			$maNotAllowed = array("asset", "process", "place");
			$moHandler->setArea($area);
			FWDWebLib::getObject("areaId")->setValue($area);
		}else if($asset){
			$maNotAllowed = array("process", "area", "place");
			$moHandler->setAsset($asset);
			FWDWebLib::getObject("assetId")->setValue($asset);
		}else if($place){
			$maNotAllowed = array("asset", "area", "process");
			$moHandler->setPlace($place);
			FWDWebLib::getObject("placeId")->setValue($place);
		}else{
			$maNotAllowed = array("asset", "area", "place", "process");
		}
		$grid->setQueryHandler($moHandler);
		$grid->setObjFwdDrawGrid(new GridPlan());

		$moACL = FWDACLSecurity::getInstance();
		$moACL->setNotAllowed($maNotAllowed);
		$moACL->installSecurity(FWDWebLib::getObject('dialog'),$maNotAllowed);
	}
}

class ChangeScrollingPath extends FWDRunnable{
	public function run(){
		echo ISMSLib::getPathScrollCode(RISK_MANAGEMENT_MODE,CONTEXT_CM_GROUP,FWDWebLib::getObject('root_plan_id')->getValue(),true,'enterPlan');
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){

		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
<script language="javascript">
        function refresh_grid(){
            js_refresh_grid('grid_plan');
        }

        function remove(){
              trigger_event('remove_event',3);
              refresh_grid();
        }

		function removePlan(){
			trigger_event('remove_plan_event',3);
			refresh_grid();
		}
		
		function enterAction(planId) {
			isms_change_to_sibling(TAB_PLAN,'nav_plan_action.php?plan='+planId);
		}

		function enterTests(planId) {
			isms_change_to_sibling(TAB_PLAN, 'nav_plan_test.php?plan='+planId);
		}

        function goToProcess() {
        	parent.soTabSubManager.changeTab(TAB_PROCESS,'nav_process.php');
        }

        function goToArea() {
        	parent.soTabSubManager.changeTab(TAB_AREA,'nav_un.php');
        }

        function goToAsset(id) {
        	parent.soTabSubManager.changeTab(TAB_ASSET,'nav_asset.php');
        }
        
        function goToPlace() {
        	parent.soTabSubManager.changeTab(TAB_PLACE,'nav_places.php');
        }

		function startTest(id){
			gebi("planForTestId").value = id;
			trigger_event('startOnlineTest', 3);
		}

        function enterPlace(place_id) {          
        	isms_change_to_sibling(TAB_PLACE, 'nav_places.php?root='+place_id);
        }

	    function enter_area(area_id) {
	    	isms_change_to_sibling(TAB_AREA, 'nav_un.php?root='+area_id);
	    }              
        
      </script>
		<?
		$process = FWDWebLib::getObject("process")->getValue();
		$place = FWDWebLib::getObject("place")->getValue();
		$asset = FWDWebLib::getObject("asset")->getValue();
		$area = FWDWebLib::getObject("area")->getValue();
		
		if($process){
			$planString = FWDLanguage::getPHPStringValue('tt_plan_bl','Planos');
			ISMSLib::getPathScrollCode(RISK_MANAGEMENT_MODE, CONTEXT_PROCESS, $process, $planString, false, '');

		}else if($place){
			$planString = FWDLanguage::getPHPStringValue('tt_plan_bl','Planos');
			ISMSLib::getPathScrollCode(RISK_MANAGEMENT_MODE, CONTEXT_CM_PLACE, $place, $planString, false, '');

		}else if($area){
			$planString = FWDLanguage::getPHPStringValue('tt_plan_bl','Planos');
			ISMSLib::getPathScrollCode(RISK_MANAGEMENT_MODE, CONTEXT_AREA, $area, $planString, false, '');

		}else if($asset){
			$planString = FWDLanguage::getPHPStringValue('tt_plan_bl','Planos');
			ISMSLib::getPathScrollCode(RISK_MANAGEMENT_MODE, CONTEXT_ASSET, $asset, $planString, false, '');

		}else {
			ISMSLib::getPathScrollCode(RISK_MANAGEMENT_MODE, CONTEXT_CM_PLAN, 0, '', false, '');

		}
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_plan.xml");
?>