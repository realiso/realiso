<?php

include_once 'include.php';
include_once $handlers_ref . "select/QuerySelectActiveScenes.php";

function savePlanTypeFromFields($piContext){

	$msPlanTypeName = FWDWebLib::getObject('plan_type_name')->getValue();
	$msPlanTypeDetail = FWDWebLib::getObject('plan_type_detail')->getValue();

	$moPlanType = new CMPlanRangeType();
	if(!$msPlanTypeName || $msPlanTypeName == ''){
		$moPlanType->setFieldValue('description', null);
	}else{
		$moPlanType->setFieldValue('description', $msPlanTypeName);
	}
	
	$moPlanType->setFieldValue('detail', $msPlanTypeDetail);

	if($piContext) {
		$moPlanType->update($piContext, true, true);
	} else {
		$piContext = $moPlanType->insert(true);
	}
	return $moPlanType;
}


class SavePlanTypeEvent extends FWDRunnable {
	public function run(){
		$miPlanTypeId = FWDWebLib::getObject('plan_type_id')->getValue();
		savePlanTypeFromFields($miPlanTypeId);
		echo "soWindow = soPopUpManager.getPopUpById('popup_plan_type_edit').getOpener();"
		."if(soWindow.refresh_grid) soWindow.refresh_grid();"
		."soPopUpManager.closePopUp('popup_plan_type_edit');";
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(''));
		$moStartEvent->addAjaxEvent(new SavePlanTypeEvent('save_plan_type_event'));
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$moWebLib = FWDWebLib::getInstance();
		$moWindowTitle = $moWebLib->getObject('window_title');
		$miPlanTypeId = $moWebLib->getObject('param_plan_type_id')->getValue();
			
		if(!$miPlanTypeId) {
			$moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_plan_type_adding', 'Adicionando Tipo de Plano'));
		} else {
			$moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_plan_type_editing', 'Editando Tipo de Plano'));
			FWDWebLib::getObject('plan_type_id')->setValue($miPlanTypeId);
			$moCMPlanType = new CMPlanRangeType();
			$moCMPlanType->fetchById($miPlanTypeId);
			FWDWebLib::getObject('plan_type_name')->setValue($moCMPlanType->getFieldValue('description'));
			FWDWebLib::getObject('plan_type_detail')->setValue($moCMPlanType->getFieldValue('detail'));
		}

		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
<script language="javascript">
        gebi('plan_type_name').focus();
    </script>
		<?
	}
}
FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_plan_type_edit.xml');
?>