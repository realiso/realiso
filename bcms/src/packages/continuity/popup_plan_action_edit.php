<?php

include_once 'include.php';
include_once $handlers_ref . "select/QuerySelectPlanRangeTypes.php";
include_once $handlers_ref . "select/QuerySelectPlanRange.php";
include_once $handlers_ref . "select/QueryGridGroupSearch.php";

include_once $handlers_ref . "select/QueryGridPlanActionSearch.php";
include_once $handlers_ref . "select/QueryGridPlanSearch.php";

function getPlanActionFromFields(){
	$moPlanAction = new CMPlanAction();

	$msPlanActionresume = FWDWebLib::getObject('resume')->getValue();
	$plan = FWDWebLib::getObject("planId")->getValue();
	$group = FWDWebLib::getObject('groupId')->getValue();
	$msPlanActionDescription = FWDWebLib::getObject('description')->getValue();
	$type = FWDWebLib::getObject('type')->getValue();
	$range = FWDWebLib::getObject('range')->getValue();
	$estimate  = FWDWebLib::getObject('estimate')->getValue();
	$estimateFlag = FWDWebLib::getObject('estimateController')->getValue();
	if(substr($estimateFlag,0,1)==':') $estimateFlag = substr($estimateFlag, 1);

	$previousAction = FWDWebLib::getObject('previousActionId')->getValue();
	$otherPlan = FWDWebLib::getObject('otherPlanId')->getValue();

	if($otherPlan){
		$moPlanAction->setFieldValue('otherPlan',$otherPlan);
	}
	if($previousAction){
		$moPlanAction->setFieldValue('previousAction',$previousAction);
	}
	$moPlanAction->setFieldValue('resume', 		$msPlanActionresume);
	$moPlanAction->setFieldValue('plan', 		$plan);
	$moPlanAction->setFieldValue('description', $msPlanActionDescription);
	$moPlanAction->setFieldValue('group', $group );
	$moPlanAction->setFieldValue('planType', $type );
	$moPlanAction->setFieldValue('estimateFlag', $estimateFlag );
	$moPlanAction->setFieldValue('estimate', $estimate );
	$moPlanAction->setFieldValue('planRange', $range);

	return $moPlanAction;
}

//class SearchGroup extends FWDRunnable {
//	public function run(){
//		$group = FWDWebLib::getObject('group')->getValue();
//		$groupOld = FWDWebLib::getObject('groupOld')->getValue();
//
//		if($group != $groupOld && trim($group) != ""){
//			$query = new QueryGridGroupSearch(FWDWebLib::getConnection());
//			$query->setNameFilter($group);
//			$query->setLimit(5);
//			$query->makeQuery();
//			$query->executeQuery();
//				
//			$groupHeight = FWDWebLib::getObject('group')->getObjFWDBox()->getAttrHeight();
//			$groupTop = FWDWebLib::getObject('group')->getObjFWDBox()->getAttrTop()+$groupHeight;
//			$groupLeft = FWDWebLib::getObject('group')->getObjFWDBox()->getAttrLeft();
//
//			echo "var position = {top: {$groupTop}, left:{$groupLeft}}; ";
//			echo "var listElements = new Array(); ";
//			$i = 0;
//			while($query->fetch()){
//				$name = $query->getFieldValue("group_name");
//				$id = $query->getFieldValue("group_id");
//				echo "listElements[{$i}] = {name:'{$name}', id:'{$id}'}; ";
//				$i++;
//			}
//			echo "createAutoCompleteDiv(listElements, 'group', 'groupId', position);";
//		}
//		echo "gebi('groupOld').value = '{$group}';";
//	}
//}

class SavePlanActionEvent extends FWDRunnable {
	public function run(){
		$miPlanActionId = FWDWebLib::getObject('planAction_id')->getValue();

		$moPlanAction = getPlanActionFromFields();

		if($miPlanActionId){
			$moPlanAction->update($miPlanActionId, true, true);
		}else{
			$piContextId = $moPlanAction->insert(true);
		}

		$plan = FWDWebLib::getObject("plan")->getValue();

		if($plan){
			FWDWebLib::getObject("planId")->setValue($plan);
		}
		
		//$this->process(null, $plan);

		echo "soWindow = soPopUpManager.getPopUpById('popup_plan_action_edit').getOpener();"
		."if(soWindow.refresh_grid) soWindow.refresh_grid();"
		."soPopUpManager.closePopUp('popup_plan_action_edit');";
	}

	protected function process($parent,$plan){
		$filter = new CMPlanAction();
		$filter->createFilter($plan, "plan");
		if(!$parent){
			$filter->createFilter("null", "previousAction", "is");
		}else{
			$filter->createFilter($parent->getFieldValue("id"), "previousAction");
		}
		$filter->select();
		$i = 10;
		while($filter->fetch()){
			$this->process($filter);
			$i = $i + 10;
		}
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(''));
		$moStartEvent->addAjaxEvent(new SavePlanActionEvent('save_planAction_event'));
		//$moStartEvent->addAjaxEvent(new SearchGroup('search_group'));
		
		$defaultFinderGroup = new DefaultFinder('search_group');
		$defaultFinderGroup->set('group', new QueryGridGroupSearch(FWDWebLib::getConnection()));
		$moStartEvent->addAjaxEvent($defaultFinderGroup);
		
		// Busca acao antecessora
		$defaultFinderPreviousAction = new DefaultFinder('search_previousAction');
		$queryGridPlanActionSearch = new QueryGridPlanActionSearch(FWDWebLib::getConnection());

		$plan = FWDWebLib::getObject("plan")->getValue();

		if($plan)
			$queryGridPlanActionSearch->setPlanFilter($plan);

		$miPlanActionId = FWDWebLib::getObject('planAction')->getValue();
		if($miPlanActionId){
			$excludeIds[] = $miPlanActionId;
			$queryGridPlanActionSearch->setExcludedIds($excludeIds);
		}

		$defaultFinderPreviousAction->set('previousAction', $queryGridPlanActionSearch);
		$moStartEvent->addAjaxEvent($defaultFinderPreviousAction);
		
		// Busca para associar um plano
		$defaultFinderOtherPlan = new DefaultFinder('search_otherPlan');
		$queryGridPlanSearch = new QueryGridPlanSearch(FWDWebLib::getConnection());

		if($plan){
			$excludeIds[] = $plan;
			$queryGridPlanSearch->setExcludedIds($excludeIds);
		}

		$defaultFinderOtherPlan->set('otherPlan', $queryGridPlanSearch);
		$moStartEvent->addAjaxEvent($defaultFinderOtherPlan);
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){

		$miPlanActionId = FWDWebLib::getObject('planAction')->getValue();
		FWDWebLib::getObject('planAction_id')->setValue($miPlanActionId);
		$moWindowTitle = FWDWebLib::getObject('window_title');

		$moHandler = new QuerySelectPlanRangeTypes(FWDWebLib::getConnection());
		$moHandlerRange = new QuerySelectPlanRange(FWDWebLib::getConnection());
		$moSelect = FWDWebLib::getObject('type');
		$moSelect->setQueryHandler($moHandler);

		$moSelectRange = FWDWebLib::getObject('range');
		$moSelectRange->setQueryHandler($moHandlerRange);

		if($miPlanActionId){
			$moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_planAction_editing', 'Edi��o de A��o'));

			$moPlanAction = new CMPlanAction();
			$moPlanAction->fetchById($miPlanActionId);
			FWDWebLib::getObject('resume')->setValue($moPlanAction->getFieldValue('resume'));
			FWDWebLib::getObject('description')->setValue($moPlanAction->getFieldValue('description'));

			$group = new CMGroup();
			$group->fetchById($moPlanAction->getFieldValue('group'));

			FWDWebLib::getObject('group')->setValue($group->getFieldValue('group_acronym'));
			FWDWebLib::getObject('groupId')->setValue($group->getFieldValue('group_id'));

			if($moPlanAction->getFieldValue('previousAction')){
				$prev = new CMPlanAction();
				$prev->fetchById($moPlanAction->getFieldValue('previousAction'));

				FWDWebLib::getObject('previousAction')->setValue($prev->getFieldValue('resume'));
				FWDWebLib::getObject('previousActionId')->setValue($prev->getFieldValue('id'));
			}

			if($moPlanAction->getFieldValue('otherPlan')){
				$prev = new CMPlan();
				$prev->fetchById($moPlanAction->getFieldValue('otherPlan'));

				FWDWebLib::getObject('otherPlan')->setValue($prev->getFieldValue('name'));
				FWDWebLib::getObject('otherPlanId')->setValue($prev->getFieldValue('id'));
			}

			FWDWebLib::getObject('estimateController')->setValue($moPlanAction->getFieldValue('estimateFlag'));
			FWDWebLib::getObject('estimate')->setValue($moPlanAction->getFieldValue('estimate'));

			$moSelectChecked = new FWDItem();
			$moSelectChecked->setAttrKey($moPlanAction->getFieldValue('planType'));
			$moSelectRangeChecked = new FWDItem();
			$moSelectRangeChecked->setAttrKey($moPlanAction->getFieldValue('planRange'));
			$moSelect->addObjFWDItem($moSelectChecked);
			$moSelectRange->addObjFWDItem($moSelectRangeChecked);

		}else{
			$moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_planAction_adding', 'Inser��o de A��o'));

			$query = new FWDDBDataSet(FWDWebLib::getConnection());
			$query->setQuery("select fkgroup as group from view_cm_plan_action_active where fkcontext = (select max(fkcontext) from view_cm_plan_action_active)");
			$query->addFWDDBField(new FWDDBField('group','group'  ,DB_NUMBER));
			$query->execute();
			while($query->fetch()){
				$idGroup = $query->getFieldByAlias("group")->getValue();
				if($idGroup){
					$group = new CMGroup();
					$group->fetchById($idGroup);

					FWDWebLib::getObject('group')->setValue($group->getFieldValue('group_acronym'));
					FWDWebLib::getObject('groupId')->setValue($group->getFieldValue('group_id'));
				}
			}
		}

		$moSelect->populate();
		$moSelectRange->populate();
		foreach($moSelectRange->getItems() as $item){
			$desc = $item->getValue();
			if(strstr($desc,"%1%")){
				$desc = str_replace("%1%", " Minutos", $desc);
			}
			if(strstr($desc,"%2%")){
				$desc = str_replace("%2%", " Horas", $desc);
			}
			if(strstr($desc,"%3%")){
				$desc = str_replace("%3%", " Dias", $desc);
			}
			if(strstr($desc,"%4%")){
				$desc = str_replace("%4%", " Meses", $desc);
			}
			$item->setValue($desc);
		}

		$plan = FWDWebLib::getObject("plan")->getValue();
		if($plan){
			FWDWebLib::getObject("planId")->setValue($plan);
		}


		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
<script language="javascript">
        gebi('resume').focus();

        function setGroup(id, name, acronym){
            gebi("groupId").value = id;
            gebi("group").value = acronym;
        }

        function setPlanAction(id, group, resume){
            gebi("previousActionId").value = id;
            gebi("previousAction").value = resume;
        }

        function setPlan(id, name, description){
            gebi("otherPlanId").value = id;
            gebi("otherPlan").value = name;
        }
        
      </script>
		<?
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_plan_action_edit.xml');
?>