<?php

include_once 'include.php';
include_once $handlers_ref . "grid/QueryGridUnOfProvider.php";

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();

		$providerId = FWDWebLib::getObject("provider_id")->getValue();			
		
		$unOfProviderHandler = new QueryGridUnOfProvider(FWDWebLib::getConnection());
		$unOfProviderHandler->setProviderId($providerId);
		
		$gridUn = FWDWebLib::getObject('gridUn');

		$gridUn->setQueryHandler($unOfProviderHandler);
		$gridUn->populate();

		$moStartEvent->setScreenEvent(new ScreenEvent(''));
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
?>

<script language="javascript">
</script>

<?
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_un_of_provider_view.xml');

?>