<?php
include_once "include.php";
include_once $handlers_ref . "QueryAreaFromScope.php";
include_once $handlers_ref . "QueryAreaFromPlace.php";
include_once $handlers_ref . "grid/QueryGridAreaSearch.php";

class GridAreaSearch extends FWDDrawGrid {
	public function DrawItem(){
		switch($this->ciColumnIndex){
			case 1:
				break;
			case 2:
				return $this->coCellBox->draw();
				break;
			default:
				return parent::drawItem();
				break;
		}
	}
}

class AssociateAreaEvent extends FWDRunnable {
	public function run(){
		$moWebLib = FWDWebLib::getInstance();
		$scopeId = $moWebLib->getObject('scope_id')->getValue();
		$placeId = $moWebLib->getObject('placeId')->getValue();

		$msNewAreas = $moWebLib->getObject('current_areas_ids')->getValue();
		if($msNewAreas){
			$maNewAreas = explode(':',$msNewAreas);
		}else{
			$maNewAreas = array();
		}
		if($scopeId) {

			$moQuery = new QueryAreaFromScope(FWDWebLib::getConnection());
			$moQuery->setScopeId($scopeId);
			$moQuery->makeQuery();
			$moQuery->fetchAreas();
			$maOldAreas = $moQuery->getAreas();

			$maToInsert = array_diff($maNewAreas,$maOldAreas);
			$maToDelete = array_diff($maOldAreas,$maNewAreas);
			$moScopeArea = new CMScopeArea();
			if(count($maToInsert)) $moScopeArea->insertAreas($scopeId,$maToInsert);
			if(count($maToDelete)) $moScopeArea->deleteAreas($scopeId,$maToDelete);

		}

		if($placeId) {
			$moQuery = new QueryAreaFromPlace(FWDWebLib::getConnection());
			$moQuery->setPlaceId($placeId);
			$moQuery->makeQuery();
			$moQuery->fetchAreas();
			$maOldAreas = $moQuery->getAreas();

			$maToInsert = array_diff($maNewAreas,$maOldAreas);
			$maToDelete = array_diff($maOldAreas,$maNewAreas);

			foreach($maToInsert as $areaId){
				$area = new RMArea();
				$area->fetchById($areaId);
				$area->setFieldValue('area_place', $placeId);
				$area->setDefaultValues();

				$area->update($areaId);
			}

			foreach($maToDelete as $areaId){
				$area = new RMArea();
				$area->fetchById($areaId);
				$area->setFieldValue('area_place', 'null');
				$area->setDefaultValues();
				$area->update($areaId);
			}
		}

		echo "soWindow = soPopUpManager.getPopUpById('popup_area_search').getOpener();"
		."if (soWindow.refresh_grid)"
		." soWindow.refresh_grid();"
		."if(soWindow.set_areas) soWindow.set_areas(gebi('current_areas_ids').value);"
		."soPopUpManager.closePopUp('popup_area_search');";
	}
}

class SearchAreaEvent extends FWDRunnable {
	public function run(){
		$moGrid = FWDWebLib::getObject('grid_area_search');
		$moGrid->execEventPopulate();
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));
		$moStartEvent->addAjaxEvent(new SearchAreaEvent('search_area_event'));
		$moStartEvent->addAjaxEvent(new AssociateAreaEvent('associate_area_event'));

		$moSearchGrid = FWDWebLib::getObject('grid_area_search');
		$moSearchGrid->setObjFwdDrawGrid(new GridAreaSearch());
		$moSearchHandler = new QueryGridAreaSearch(FWDWebLib::getConnection());

		$moCurrentGrid = FWDWebLib::getObject('grid_current_areas');
		$moCurrentGrid->setObjFwdDrawGrid(new GridAreaSearch());
		$moCurrentHandler = new QueryGridAreaSearch(FWDWebLib::getConnection());

		$scopeId = FWDWebLib::getObject('param_scope_id')->getValue();
		$placeId = FWDWebLib::getObject('place')->getValue();

		$moSearchHandler->setNameFilter(FWDWebLib::getObject('name_filter')->getValue());
		$moSearchHandler->setScope($scopeId);
		$msCurrentIds = FWDWebLib::getObject('current_areas_ids')->getValue();

		$title = FWDLanguage::getPHPStringValue('tt_areas_search', "Busca de Unidades de Neg�cio");

		$removed = FWDWebLib::getObject('removed')->getValue();
		if(!$msCurrentIds && $removed == 'false'){
			if($placeId){
				$msCurrentIds = '0';
				$query = new FWDDBDataSet(FWDWebLib::getConnection());
				$query->setQuery("select fkcontext as id from rm_area where fkplace = {$placeId}");
				$query->addFWDDBField(new FWDDBField('id','id'  ,DB_NUMBER));
				$query->execute();
				while($query->fetch()){
					$id = $query->getFieldByAlias("id")->getValue();
					$msCurrentIds .= ":".$id;
				}
				FWDWebLib::getObject('current_areas_ids')->setValue($msCurrentIds);
			}

			if($scopeId){

				$title .= FWDLanguage::getPHPStringValue('areaLimitedByScope', " - Limitado aos locais do escopo");

				$msCurrentIds = '0';
				$query = new FWDDBDataSet(FWDWebLib::getConnection());
				$query->setQuery("select fkarea as id from cm_scope_area where fkscope = {$scopeId}");
				$query->addFWDDBField(new FWDDBField('id','id'  ,DB_NUMBER));
				$query->execute();
				while($query->fetch()){
					$id = $query->getFieldByAlias("id")->getValue();
					$msCurrentIds .= ":".$id;
				}
				FWDWebLib::getObject('current_areas_ids')->setValue($msCurrentIds);
			}
		}

		FWDWebLib::getObject('window_title')->setValue($title);

		if($msCurrentIds){
			$maCurrentIds = explode(':',$msCurrentIds);
			$moSearchHandler->setExcludedIds($maCurrentIds);
			$moCurrentHandler->setIds($maCurrentIds);
		}
		else {
			$moCurrentHandler->setEmpty(true);
		}

		$moSearchGrid->setQueryHandler($moSearchHandler);
		$moCurrentGrid->setQueryHandler($moCurrentHandler);
		$moCurrentGrid->populate();
		$moSearchGrid->populate();
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$paramAreasIds = FWDWebLib::getObject('param_areas_ids')->getValue();
		if($paramAreasIds) {
			FWDWebLib::getObject('current_areas_ids')->setValue($paramAreasIds);
		}

		$scopeId = FWDWebLib::getObject('param_scope_id')->getValue();
		FWDWebLib::getObject('scope_id')->setValue($scopeId);

		$placeId = FWDWebLib::getObject('place')->getValue();
		FWDWebLib::getObject('placeId')->setValue($placeId);

		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
<script language="javascript">
	gebi('area_name').focus();
	function refresh_grid() {
		gobi('grid_current_areas').setPopulate(true);
		gobi('grid_current_areas').refresh();
	}
	function enter_area_search_event(e)
	{
		if (!e) e = window.event;
		if(e['keyCode']==13) {
			gebi('name_filter').value = gebi('area_name').value;
			gebi('current_areas_ids').value = gobi('grid_current_areas').getAllIds().join(':');
			gobi('grid_areas_search').setPopulate(true);
			trigger_event("search_area_event","3");
		}
	}
	FWDEventManager.addEvent(gebi('area_name'), 'keydown', enter_area_search_event);
</script>
		<?
	}
}
FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load('popup_area_search.xml');
?>