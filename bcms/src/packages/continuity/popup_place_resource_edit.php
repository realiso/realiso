<?php

include_once 'include.php';
include_once $handlers_ref . "select/QueryPlaceResource.php";
include_once $handlers_ref . "select/QuerySelectResource.php";
include_once $handlers_ref . "select/QuerySelectGroup.php";


class SaveEvent extends FWDRunnable {
	public function run(){
		$place = FWDWebLib::getObject('placeId')->getValue();

		$msNewResources = FWDWebLib::getObject('resources_ids')->getValue();
		$msNewGroups = FWDWebLib::getObject('groups_ids')->getValue();
		if($msNewResources){
			$maNewResources = explode(':',$msNewResources);
		}else{
			$maNewResources = array();
		}
		if($msNewGroups){
			$maNewGroups = explode(':',$msNewGroups);
		}else{
			$maNewGroups = array();
		}

		$moQuery = new QueryPlaceResource(FWDWebLib::getConnection());
		$moQuery->setPlaceId($place);
		$moQuery->setResource(true);
		$moQuery->makeQuery();
		$moQuery->fetchResources();
		$maOldResources = $moQuery->getResources();

		$moQuery = new QueryPlaceResource(FWDWebLib::getConnection());
		$moQuery->setPlaceId($place);
		$moQuery->setResource(false);
		$moQuery->makeQuery();
		$moQuery->fetchResources();
		$maOldGroups = $moQuery->getResources();

		$maToInsert = array_diff($maNewResources,$maOldResources);
		$maToDelete = array_diff($maOldResources,$maNewResources);
		$placeResource = new CMPlaceResource();
		if(count($maToInsert)) $placeResource->insertResources($place,$maToInsert);
		if(count($maToDelete)) $placeResource->deleteResources($place,$maToDelete);

		$maToInsert = array_diff($maNewGroups,$maOldGroups);
		$maToDelete = array_diff($maOldGroups,$maNewGroups);
		$placeResource = new CMPlaceResource();
		if(count($maToInsert)) $placeResource->insertGroup($place,$maToInsert);
		if(count($maToDelete)) $placeResource->deleteGroup($place,$maToDelete);


		echo "soWindow = soPopUpManager.getPopUpById('popup_place_resource_edit').getOpener();"
		."if(soWindow.refresh_grid) soWindow.refresh_grid();"
		."soPopUpManager.closePopUp('popup_place_resource_edit');";
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(''));
		$moStartEvent->addAjaxEvent(new SaveEvent('SaveEvent'));
		$moStartEvent->addAjaxEvent(new RefreshGroupEvent('refresh_groups'));
		$moStartEvent->addAjaxEvent(new RefreshResourceEvent('refresh_resources'));
	}
}

class RefreshResourceEvent extends FWDRunnable {
	public function run(){
		$moHandler = new QuerySelectResource(FWDWebLib::getConnection());
		$resourceIds = FWDWebLib::getObject('resources_ids')->getValue();
		if($resourceIds){
			$moHandler->setResourceIds($resourceIds);

			$moSelect = FWDWebLib::getObject('resources');
			$moSelect->setQueryHandler($moHandler);
			$moSelect->populate();
			$moSelect->execEventPopulate();
		}
	}
}

class RefreshGroupEvent extends FWDRunnable {
	public function run(){
		$moHandler = new QuerySelectGroup(FWDWebLib::getConnection());
		$groupsIds = FWDWebLib::getObject('groups_ids')->getValue();
		if($groupsIds){
			$moHandler->setGroupIds($groupsIds);

			$moSelect = FWDWebLib::getObject('groups');
			$moSelect->setQueryHandler($moHandler);
			$moSelect->populate();
			$moSelect->execEventPopulate();
		}
	}
}


class ScreenEvent extends FWDRunnable {
	public function run(){
		$moWebLib = FWDWebLib::getInstance();
		$moWindowTitle = $moWebLib->getObject('window_title');
			
		$place = $moWebLib->getObject('place')->getValue();
		if($place){
			$moWebLib->getObject('placeId')->setValue($place);
		}

		$moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_group_editing', 'Editando Contatos'));

		$moResourceSelect = FWDWebLib::getObject('resources');
		$moGroupSelect = FWDWebLib::getObject('groups');

		if($place) {
			$moQuery = new QueryPlaceResource(FWDWebLib::getConnection());
			$moQuery->setPlaceId($place);
			$moQuery->setResource(true);
			$moQuery->makeQuery();
			$moQuery->executeQuery();

			$moResources = array();
			while($moQuery->fetch()) {
				$moResourceSelect->setItemValue($moQuery->getFieldValue('id'), $moQuery->getFieldValue('name'));
				$moResources[] = $moQuery->getFieldValue('id');
			}
			FWDWebLib::getObject('resources_ids')->setValue(implode(':',$moResources));

			$moQuery = new QueryPlaceResource(FWDWebLib::getConnection());
			$moQuery->setPlaceId($place);
			$moQuery->setResource(false);
			$moQuery->makeQuery();
			$moQuery->executeQuery();

			$moGroups = array();
			while($moQuery->fetch()) {
				$moGroupSelect->setItemValue($moQuery->getFieldValue('id'), $moQuery->getFieldValue('name'));
				$moGroups[] = $moQuery->getFieldValue('id');
			}
			FWDWebLib::getObject('groups_ids')->setValue(implode(':',$moGroups));
		}

		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
<script language="javascript">

			function set_groups(groupIds) {
			  	gebi('groups_ids').value = groupIds;
			  	groupIds = null;
			  	trigger_event('refresh_groups',3);
			}
			function set_resources(resourceIds) {
			  	gebi('resources_ids').value = resourceIds;
			  	resourceIds=null;
			  	trigger_event('refresh_resources',3);
			}

        	gebi('groupName').focus();
		</script>
		<?
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_place_resource_edit.xml');

?>