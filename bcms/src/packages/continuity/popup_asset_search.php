<?php
include_once "include.php";
include_once $handlers_ref . "QueryAssetFromScope.php";
include_once $handlers_ref . "QueryAssetFromPlace.php";
include_once $handlers_ref . "QueryAssetFromProcess.php";
include_once $handlers_ref . "QueryAssetFromActivity.php";
include_once $handlers_ref . "grid/QueryGridAssetSearch.php";

class GridAssetSearch extends FWDDrawGrid {
	public function DrawItem(){
		switch($this->ciColumnIndex){
			case 1:
				break;
			case 2:
				return $this->coCellBox->draw();
				break;
			default:
				return parent::drawItem();
				break;
		}
	}
}

class AssociateAssetEvent extends FWDRunnable {
	public function run(){
		$moWebLib = FWDWebLib::getInstance();
		$scopeId = $moWebLib->getObject('scope_id')->getValue();
		$placeId = $moWebLib->getObject('place_id')->getValue();
		$processId = $moWebLib->getObject('process_id')->getValue();
		$activityId = $moWebLib->getObject('activity_id')->getValue();

		$msNewAssets = $moWebLib->getObject('current_assets_ids')->getValue();
		if($msNewAssets){
			$maNewAssets = explode(':',$msNewAssets);
		}else{
			$maNewAssets = array();
		}
		if($scopeId) {
			$moQuery = new QueryAssetFromScope(FWDWebLib::getConnection());
			$moQuery->setScopeId($scopeId);
			$moQuery->makeQuery();
			$moQuery->fetchAssets();
			$maOldAssets = $moQuery->getAssets();

			$maToInsert = array_diff($maNewAssets,$maOldAssets);
			$maToDelete = array_diff($maOldAssets,$maNewAssets);
			$moScopeAsset = new CMScopeAsset();
			if(count($maToInsert)) $moScopeAsset->insertAssets($scopeId,$maToInsert);
			if(count($maToDelete)) $moScopeAsset->deleteAssets($scopeId,$maToDelete);
		} else if($placeId){

			$moQuery = new QueryAssetFromPlace(FWDWebLib::getConnection());
			$moQuery->setPlaceId($placeId);
			$moQuery->makeQuery();
			$moQuery->executeQuery();
			$maOldAssets = $moQuery->getAssets();

			$maToInsert = array_diff($maNewAssets,$maOldAssets);
			$maToDelete = array_diff($maOldAssets,$maNewAssets);
			$place = new CMPlace();

			if(count($maToInsert)) $place->associateAssets($placeId,$maToInsert);
			if(count($maToDelete)) $place->removeAssets($maToDelete);
		}else if($processId){

			$moQuery = new QueryAssetFromProcess(FWDWebLib::getConnection());
			$moQuery->setProcessId($processId);
			$moQuery->makeQuery();
			$moQuery->executeQuery();
			$maOldAssets = $moQuery->getAssets();

			$maToInsert = array_diff($maNewAssets,$maOldAssets);
			$maToDelete = array_diff($maOldAssets,$maNewAssets);

			$processAsset = new RMProcessAsset();
			if(count($maToInsert)) $processAsset->insertAssets($processId, $maToInsert);
			if(count($maToDelete)) $processAsset->deleteAssets($processId, $maToDelete);
		}else if($activityId){

			$moQuery = new QueryAssetFromActivity(FWDWebLib::getConnection());
			$moQuery->setActivityId($activityId);
			$moQuery->makeQuery();
			$moQuery->executeQuery();
			$maOldAssets = $moQuery->getAssets();

			$maToInsert = array_diff($maNewAssets,$maOldAssets);
			$maToDelete = array_diff($maOldAssets,$maNewAssets);

			$activityAsset = new CMProcessActivityAsset();
			if(count($maToInsert)) $activityAsset->insertAssets($activityId, $maToInsert);
			if(count($maToDelete)) $activityAsset->deleteAssets($activityId, $maToDelete);
		}

		echo "soWindow = soPopUpManager.getPopUpById('popup_asset_search').getOpener();"
		."if (soWindow.refresh_grid)"
		." soWindow.refresh_grid();"
		."if(soWindow.set_assets) soWindow.set_assets(gebi('current_assets_ids').value);"
		."soPopUpManager.closePopUp('popup_asset_search');";
	}
}

class SearchAssetEvent extends FWDRunnable {
	public function run(){
		$moGrid = FWDWebLib::getObject('grid_asset_search');
		$moGrid->execEventPopulate();
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));
		$moStartEvent->addAjaxEvent(new SearchAssetEvent('search_asset_event'));
		$moStartEvent->addAjaxEvent(new AssociateAssetEvent('associate_asset_event'));

		$moSearchGrid = FWDWebLib::getObject('grid_asset_search');
		$moSearchGrid->setObjFwdDrawGrid(new GridAssetSearch());
		$moSearchHandler = new QueryGridAssetSearch(FWDWebLib::getConnection());

		$moCurrentGrid = FWDWebLib::getObject('grid_current_assets');
		$moCurrentGrid->setObjFwdDrawGrid(new GridAssetSearch());
		$moCurrentHandler = new QueryGridAssetSearch(FWDWebLib::getConnection());
		$moWindowTitle = FWDWebLib::getObject('window_title');
		$title = FWDLanguage::getPHPStringValue('asset_search', "Busca de Ativos");

		$scopeId = FWDWebLib::getObject('param_scope_id')->getValue();
		if($scopeId){
			$moSearchHandler->setScope($scopeId);
			$moCurrentHandler->setScope($scopeId);
			$title .= FWDLanguage::getPHPStringValue('assetLimitedByScope', " - Limitado aos processos do escopo");
		}

		$processId = FWDWebLib::getObject('param_process_id')->getValue();
		$activityId = FWDWebLib::getObject('param_activity_id')->getValue();

		$moWindowTitle->setValue($title);
		$moSearchHandler->setNameFilter(FWDWebLib::getObject('name_filter')->getValue());
		$msCurrentIds = FWDWebLib::getObject('current_assets_ids')->getValue();
		$removed = FWDWebLib::getObject('removed')->getValue();
		if(!$msCurrentIds && $removed == 'false'){
			if($scopeId){
				$msCurrentIds = '0';
				$query = new FWDDBDataSet(FWDWebLib::getConnection());
				$query->setQuery("select fkasset as id from cm_scope_asset where fkscope = {$scopeId}");
				$query->addFWDDBField(new FWDDBField('id','id'  ,DB_NUMBER));
				$query->execute();
				while($query->fetch()){
					$id = $query->getFieldByAlias("id")->getValue();
					$msCurrentIds .= ":".$id;
				}
				FWDWebLib::getObject('current_assets_ids')->setValue($msCurrentIds);
			}

			if($processId){
				$msCurrentIds = '0';
				$query = new FWDDBDataSet(FWDWebLib::getConnection());
				$query->setQuery("select fkasset as id from view_rm_process_asset_active where fkprocess = {$processId}");
				$query->addFWDDBField(new FWDDBField('id','id'  ,DB_NUMBER));
				$query->execute();
				while($query->fetch()){
					$id = $query->getFieldByAlias("id")->getValue();
					$msCurrentIds .= ":".$id;
				}
				FWDWebLib::getObject('current_assets_ids')->setValue($msCurrentIds);
			}
				
			if($activityId){
				$msCurrentIds = '0';
				$query = new FWDDBDataSet(FWDWebLib::getConnection());
				$query->setQuery("select fkasset as id from view_cm_process_activity_asset_active where fkactivity = {$activityId}");
				$query->addFWDDBField(new FWDDBField('id','id'  ,DB_NUMBER));
				$query->execute();
				while($query->fetch()){
					$id = $query->getFieldByAlias("id")->getValue();
					$msCurrentIds .= ":".$id;
				}
				FWDWebLib::getObject('current_assets_ids')->setValue($msCurrentIds);
			}
		}
		if($msCurrentIds){
			$maCurrentIds = explode(':',$msCurrentIds);
			$moSearchHandler->setExcludedIds($maCurrentIds);
			$moCurrentHandler->setIds($maCurrentIds);
		}
		else {
			$moCurrentHandler->setEmpty(true);
		}

		$moSearchGrid->setQueryHandler($moSearchHandler);
		$moCurrentGrid->setQueryHandler($moCurrentHandler);
		$moCurrentGrid->populate();
		$moSearchGrid->populate();
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$placeId = FWDWebLib::getObject('param_place_id')->getValue();
		FWDWebLib::getObject('place_id')->setValue($placeId);

		$paramAssetsIds = FWDWebLib::getObject('param_assets_ids')->getValue();
		if($paramAssetsIds) {
			FWDWebLib::getObject('current_assets_ids')->setValue($paramAssetsIds);
		}else if($placeId){
			$asset = new RMAsset();
			$asset->createFilter($placeId, 'place');
			$asset->select();
			$ids = '';
			while($asset->fetch()){
				$ids .= $asset->getFieldValue('asset_id').':';
			}
			if(count($ids)){
				$ids = substr($ids, 0, count($ids)-2);
				FWDWebLib::getObject('current_assets_ids')->setValue($ids);
			}

		}
		$scopeId = FWDWebLib::getObject('param_scope_id')->getValue();
		FWDWebLib::getObject('scope_id')->setValue($scopeId);

		$processId = FWDWebLib::getObject('param_process_id')->getValue();
		FWDWebLib::getObject('process_id')->setValue($processId);
		
		$activityId = FWDWebLib::getObject('param_activity_id')->getValue();
		FWDWebLib::getObject('activity_id')->setValue($activityId);

		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
<script language="javascript">
	gebi('asset_name').focus();
	function refresh_grid() {
		gobi('grid_current_assets').setPopulate(true);
		gobi('grid_current_assets').refresh();
	}
	function enter_asset_search_event(e)
	{
		if (!e) e = window.event;
		if(e['keyCode']==13) {
			gebi('name_filter').value = gebi('asset_name').value;
			gebi('current_assets_ids').value = gobi('grid_current_assets').getAllIds().join(':');
			gobi('grid_assets_search').setPopulate(true);
			trigger_event("search_asset_event","3");
		}
	}
	FWDEventManager.addEvent(gebi('asset_name'), 'keydown', enter_asset_search_event);
</script>
		<?
	}
}
FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load('popup_asset_search.xml');
?>