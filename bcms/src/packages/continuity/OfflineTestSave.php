<?php
require_once('include.php');
require_once('../../handlers/grid/QueryOfflinePlanTest.php');

function getTimestamp($value) {
	$miTimestamp = 0;
	if ($value) {
		$maDate = explode("/", $value);
		$miTimestamp = mktime(0, 0, 0, $maDate[1], $maDate[0], $maDate[2]);
	}
	return $miTimestamp;
}

$planId = $_POST["plan"];
$planTestId = $_POST["planTest"];

$query = new QueryOfflinePlanTest(FWDWebLib::getConnection());
$query->setPlan($planId);
$query->makeQuery();
$query->executeQuery();

while($query->fetch()){
	$planActionTest = new CMPlanActionTest();

	$planActionId = $query->getFieldValue("planActionId");

	$planActionTest->createFilter($planTestId, "planTest");
	$planActionTest->createFilter($planActionId, "planAction");
	$planActionTest->select();
	$planActionTest->fetch();


	$startHour = $_POST["startHour_".$planActionId];
	$startDate = $_POST["startDate_".$planActionId];
	$endDate = $_POST["endDate_".$planActionId];
	$endHour = $_POST["endHour_".$planActionId];
	$observation = $_POST["observation_".$planActionId];

	if($startHour && $startDate){
		$maTime = explode(":", $startHour);
		$maDate = explode("/", $startDate);
		$timeStamp = mktime(isset($maTime[0])?$maTime[0]:0, isset($maTime[1])?$maTime[1]:0, 0, $maDate[1], $maDate[0], $maDate[2]);

		$planActionTest->setFieldValue("startTime", $timeStamp);
	}else{
		$planActionTest->setFieldValue("startTime", 'null');
	}

	if($endHour && $endDate){
		$maTime = explode(":", $endHour);
		$maDate = explode("/", $endDate);
		$timeStamp = mktime(isset($maTime[0])?$maTime[0]:0, isset($maTime[1])?$maTime[1]:0, 0, $maDate[1], $maDate[0], $maDate[2]);
		$planActionTest->setFieldValue("endTime", $timeStamp);
	}else{
		$planActionTest->setFieldValue("endTime", 'null');
	}

	$planActionTest->setFieldValue("planAction", $planActionId);
	$planActionTest->setFieldValue("planTest", $planTestId);
	if($observation){
		$planActionTest->setFieldValue("observation", $observation);
	}else{
		$planActionTest->setFieldValue("observation", '');
	}
	if($planActionTest->getFieldValue("id")){
		$planActionTest->update($planActionTest->getFieldValue("id"));
	}else{
		$planActionTest->insert();
	}

}

?>
<html>
<head>
<style type="text/css">
body {
	font-weight: bold;
	font-size: 25px;
	margin-left: 10px;
	margin-top: 10px;
}
</style>
</head>
<body>
	Resultados guardados com sucesso!
</body>
<script>
	setTimeout(function(){self.close();}, 4000);
</script>
</html>
