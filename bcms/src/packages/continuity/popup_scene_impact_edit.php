<?php

include_once 'include.php';
include_once $handlers_ref . "select/QuerySelectImpactTypes.php";
include_once $handlers_ref . "select/QuerySelectFinancialImpact.php";
include_once $handlers_ref . "select/QuerySelectReadableDocumentByContext.php";

function save($poContext, $pbHasSensitiveChanges, $piContextId = 0){
	if($piContextId){
		$poContext->update($piContextId, true, $pbHasSensitiveChanges);
	}else{
		$piContextId = $poContext->insert(true);
	}
}

function saveImpactFromFields(){

	$impactId = FWDWebLib::getObject('impact_id')->getValue();

	$moDamageMatrix = new CMDamageMatrix();
	$moMatrix = $moDamageMatrix->getDamageMatrix();

	foreach($moMatrix as $matrixId=>$matrixValue) {
		$miImpactParameter = intval($_POST['impact_' . $matrixId]);
		$miFinancialImpactParameter = intval($_POST['financial_impact_id_' . $matrixId]);
		$miImpactFinancial = $_POST['financial_impact_' . $matrixId];
		$impactDamageRange = new CMImpactDamageRange();
		$impactDamageRange->createFilter($impactId, 'impact_damage_id');
		$impactDamageRange->createFilter($matrixId, 'impact_damage_damage_id');
		$impactDamageRange->select();
		while($impactDamageRange->fetch()){
			$impactDamageRange->setFieldValue('impact_damage_parameter_id', $miImpactParameter);
			$impactDamageRange->setFieldValue('financial_impact', $miFinancialImpactParameter);
			$impactDamageRange->setFieldValue('impact_damage_financial_impact', $miImpactFinancial);
			$impactDamageRange->update($impactDamageRange->getFieldValue('impact_damage_range_id'),true,true);
		}
	}
}


class SaveImpactEvent extends FWDRunnable {
	public function run(){
		saveImpactFromFields();
		echo "soWindow = soPopUpManager.getPopUpById('popup_scene_impact_edit').getOpener();"
		."if(soWindow.refresh_grid) soWindow.refresh_grid();"
		."soPopUpManager.closePopUp('popup_scene_impact_edit');";
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(''));
		$moStartEvent->addAjaxEvent(new SaveImpactEvent('save_impact_event'));
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$moWebLib = FWDWebLib::getInstance();
		$moWindowTitle = $moWebLib->getObject('window_title');
		$miImpactId = $moWebLib->getObject('param_impact_id')->getValue();
			
		$miSceneId = $moWebLib->getObject('param_scene_id')->getValue();

		if($miImpactId) {
			$moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_impact_editing', 'Editando Impacto'));
			FWDWebLib::getObject('impact_id')->setValue($miImpactId);
			$impactDamage = new CMImpactDamage();
			$impactDamage->fetchById($miImpactId);
			$moCMImpact = new CMImpact();
			$moCMImpact->fetchById($impactDamage->getFieldValue('impact_id'));

			$impactType = new CMImpactType();
			$impactType->fetchById($moCMImpact->getFieldValue('impact_type'));

			FWDWebLib::getObject('impact_name')->setValue($moCMImpact->getFieldValue('impact_name'));
			FWDWebLib::getObject('impact_type')->setValue($impactType->getFieldValue('impact_type_name'));
		}

		if($miSceneId){

			$moStoredDamages = array();
			$storedFinancialImpacts = array();

			if($miImpactId) {
				$moCMImpactDamage = new CMImpactDamageRange();
				$moStoredDamages = $moCMImpactDamage->getDamages($miImpactId);
				$storedFinancialImpacts = $moCMImpactDamage->getFinancialImpacts($miImpactId);
			}

			FWDWebLib::getObject('scene_id')->setValue($miSceneId);

			$moDamageMatrix = new CMDamageMatrix();
			$moMatrix = $moDamageMatrix->getDamageMatrix();

			$moDamageMatrixParameter = new CMDamageMatrixParameter();
			$moParameters = $moDamageMatrixParameter->getDamageParameters();

			$moOuterViewGroup = $moWebLib->getObject('damage_matrix');
			$moOuterViewGroup->setAttrClass('simple_border');

			$queryFinancialImpact = new QuerySelectFinancialImpact(FWDWebLib::getConnection());
			$queryFinancialImpact->makeQuery();
			$queryFinancialImpact->executeQuery();
			$financialImpacts = array();
			while($queryFinancialImpact->fetch()){
				$financialImpacts[$queryFinancialImpact->getFieldValue('select_id')] = $queryFinancialImpact->getFieldValue('select_value');
			}

			$i = 10;
			$offset = 950 / count($moMatrix);
			foreach($moMatrix as $matrixId=>$matrixValue) {
				$moStatic = new FWDStatic();
				$moStatic->setAttrName('static_'.$matrixId);
				$moStatic->setAttrHorizontal('center');
				$moStatic->setObjFWDBox(new FWDBox($i,10,20,130));
				$moStatic->setValue('<b>' . $matrixValue . '</b>');
				$moOuterViewGroup->addObjFWDView($moStatic);

				$moSelectVariable = new FWDVariable();
				$moSelectVariable->setAttrName('impact_criticity_' . $matrixId);

				$moOuterViewGroup->addObjFWDView($moSelectVariable);

				$selectFinancialImpact = new FWDText();
				$selectFinancialImpact->setAttrName("financial_impact_id_".$matrixId);
				$selectFinancialImpact->setObjFWDBox(new FWDBox($i,80,20,125));

				$financialImpactMask = new FWDMask();
				$financialImpactMask->setType(MASK_INTEGER);
				$selectFinancialImpact->addObjFWDMask($financialImpactMask);

				$moOuterViewGroup->addObjFWDView($selectFinancialImpact);

				$selectFinancialImpact->setValue($storedFinancialImpacts[$matrixId]);

				$moSelect = new FWDSelect();
				$moSelect->setAttrName("impact_".$matrixId);
				$moSelect->setObjFWDBox(new FWDBox($i,40,20,125));

				foreach($moParameters as $miparId=>$msParName){
					$moItem = new FWDItem();
					$moItem->setAttrKey($miparId);
					$moItem->setValue($msParName);

					if(array_key_exists($matrixId, $moStoredDamages)) {
						if($moStoredDamages[$matrixId] == $miparId) {
							$moItem->setAttrCheck(true);
						}
					}
					$moSelect->addObjFWDItem($moItem);
				}


				$moEvent = new FWDClientEvent();
				$moEvent->setAttrEvent('onChange');
				$moEvent->setAttrValue("gebi('impact_criticity_". $matrixId . "').value = this.value;");
				$moSelect->addObjFWDEvent($moEvent);

				$moOuterViewGroup->addObjFWDView($moSelect);

				$moViewButton = new FWDViewButton(new FWDBox($i-1,110,20,125));

				$moStatic = new FWDStatic();
				$moStatic->setObjFWDBox(new FWDBox(0,0,20,130));
				$msStaticText = FWDLanguage::getPHPStringValue('cm_financial_impact','&nbsp;Mem�ria de C�lculo');
				$moStatic->setValue("$msStaticText");
				$moViewButton->addObjFWDView($moStatic);

				$moEvent = new FWDClientEvent();
				$moEvent->setAttrEvent('onClick');
				if($miImpactId) {
					$moEvent->setAttrValue("isms_open_popup('popup_financial_impact_edit','packages/continuity/popup_financial_impact_edit.php?matrix=". $matrixId . "&impact=" . $miImpactId."','','true');");
				} else {
					$moEvent->setAttrValue("isms_open_popup('popup_financial_impact_edit','packages/continuity/popup_financial_impact_edit.php?matrix=". $matrixId . "','','true');");
				}
				$moViewButton->addObjFWDEvent($moEvent);

				$moFinancialImpactVariable = new FWDVariable();
				$moFinancialImpactVariable->setAttrName('financial_impact_' . $matrixId);

				if($miImpactId) {
					$moCMImpactDamage = new CMImpactDamageRange();
					$moCMImpactDamage->createFilter($miImpactId, 'impact_damage_id');
					$moCMImpactDamage->createFilter($matrixId, 'impact_damage_damage_id');
					$moCMImpactDamage->select();

					if($moCMImpactDamage->fetch()) {
						$moFinancialImpactVariable->setValue($moCMImpactDamage->getFieldValue('impact_damage_financial_impact'));
					}
				}

				$moOuterViewGroup->addObjFWDView($moFinancialImpactVariable);

				$moOuterViewGroup->addObjFWDView($moViewButton);

				$i+=$offset;
					
			}
		}
		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
<script language="javascript">
        gebi('impact_name').focus();

        function set_financial_impact(id, value){
            gebi('financial_impact_' + id).value = value;
            var select = gebi('impact_'+ id);
            if(select.options[0].value == ""){
  				select.remove(0);
            }
        }

        function get_financial_impact(id){
            return gebi('financial_impact_' + id).value;
        }
                
      </script>
		<?
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_scene_impact_edit.xml');

?>