<?php

include_once 'include.php';
include_once $handlers_ref . "QueryContextHierarchicalStructure.php";
include_once $handlers_ref . 'QueryTotalAssets.php';
include_once $handlers_ref . "select/QuerySelectReadableDocumentByContext.php";
include_once $handlers_ref . "select/QuerySelectPriority.php";
include_once $handlers_ref . "select/QueryGridGroupSearch.php";
include_once $handlers_ref . "grid/QueryGridPlaceSearch.php";

include_once $handlers_ref . "select/QuerySelectPlaceUN.php";
include_once $handlers_ref . "select/QueryCheckTacTraAsset.php";

class SubmitEvent extends FWDRunnable {
	public function run(){


		$moWebLib = FWDWebLib::getInstance();
		$moSession = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId());

		$moFile = FWDWebLib::getObject('file');
		$miFileError = 0;
		if($moFile->isUploaded()){
			$miFileError = $moFile->getErrorCode();
			if($miFileError==FWDFile::E_NONE){
				// Upload sem erros

				$moCrypt = new FWDCrypt();
				$msDirectory = (ISMSLib::isCompiled() ? $GLOBALS['files_ref'] : '/files/');
				if(is_writable($msDirectory)){
					$msFileName=$moFile->getFileName();
					$msHashFileName = md5($msFileName.uniqid());
					$msPath = "{$msDirectory}{$msHashFileName}.aef";
					$miFileSize = filesize($moFile->getTempFileName());
					$moWriteFP = fopen($msPath,'wb');
					fwrite($moWriteFP,$moCrypt->getIV());
					$moFP = fopen($moFile->getTempFileName(),'rb');
					while(!feof($moFP)){
						fwrite($moWriteFP,$moCrypt->encryptNoBase64(fread($moFP,16384)));
					}
					fclose($moFP);
					fclose($moWriteFP);

					if(!$moSession->attributeExists("documentFilePath")){
						$moSession->addAttribute("documentFilePath");
					}
					$moSession->setAttrDocumentFilePath($msPath);
					if(!$moSession->attributeExists("documentFileName")){
						$moSession->addAttribute("documentFileName");
					}
					$moSession->setAttrDocumentFileName($msFileName);
					if(!$moSession->attributeExists("documentFileSize")){
						$moSession->addAttribute("documentFileSize");
					}
					$moSession->setAttrDocumentFileSize($miFileSize);
					if(!$moSession->attributeExists("documentFileContent")){
						$moSession->addAttribute("documentFileContent");
					}
				}else{
					//O diret�rio n�o p�de ser escrito!
					trigger_error("Directory '{$msDirectory}' is not writable.",E_USER_ERROR);
				}
			}else{
				// Erro no upload -> armazena c�digo do erro na sess�o
				if(!$moSession->attributeExists('uploadErrorCode')){
					$moSession->addAttribute('uploadErrorCode');
				}
				$moSession->setAttrUploadErrorCode($miFileError);
			}
		}
		// Sinaliza que, com ou sem arquivo, com ou sem erro, acabou o submit
		if(!$moSession->attributeExists("policySaveComplete")){
			$moSession->addAttribute("policySaveComplete");
		}
		$moSession->setAttrPolicySaveComplete(true);

	}
}

class SaveCompleteEvent extends FWDRunnable {
	public function run(){
		$moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
		$id = $moSession->attributeExists("documentContextId")?$moSession->getAttrDocumentContextId():0;
		$asset = new RMAsset();

		if(!$moSession->attributeExists("policySaveComplete")){
			echo "setTimeout('trigger_event(\"save_complete_event\",3)',200);";
			return;
		}

		if($asset->fetchById($id) && $moSession->attributeExists("policySaveComplete")){
			if($moSession->getAttrPolicySaveComplete()){
				$moSession->deleteAttribute("policySaveComplete");

				if($moSession->attributeExists('uploadErrorCode') && $moSession->getAttrUploadErrorCode()){
					// Trata erro no upload
					$miErrorCode = $moSession->getAttrUploadErrorCode();
					$moSession->deleteAttribute('uploadErrorCode');

					if($miErrorCode & FWDFile::E_MAX_SIZE){
						echo "gobi('warning_max_size').show();";
					}else{
						echo "gobi('warning_upload_error').show();";
					}
					echo "soTabManager.unlockTabs();";
				}else{
					// Upload feito com sucesso
					$moWebLib = FWDWebLib::getInstance();

					if($moSession->attributeExists("documentFilePath")){

						$msPath = $moSession->getAttrDocumentFilePath();
						$msFileName = $moSession->attributeExists("documentFileName")?$moSession->getAttrDocumentFileName():"";

						$asset->setFieldValue('file_path',$msPath);
						$asset->setFieldValue('file_name',$msFileName);
					}

					$asset->setDefaultValues();

					$asset->update($id);

					if ($moSession->attributeExists("documentFilePath")) $moSession->deleteAttribute("documentFilePath");
					if ($moSession->attributeExists("documentFileName")) $moSession->deleteAttribute("documentFileName");
					if ($moSession->attributeExists("documentFileSize")) $moSession->deleteAttribute("documentFileSize");
					if ($moSession->attributeExists("documentFileContent")) $moSession->deleteAttribute("documentFileContent");
					if ($moSession->attributeExists("documentContextId")) $moSession->deleteAttribute("documentContextId");
				}
			}
		}
		echo "	var soWindow = soPopUpManager.getPopUpById('popup_asset_edit').getOpener();
						if(soWindow.refresh_grid){
							soWindow.refresh_grid();
						}
						soPopUpManager.closePopUp('popup_asset_edit');
				";
	}
}
class DownloadEvent extends FWDRunnable {
	public function run() {
		$moWebLib = FWDWebLib::getInstance();
		$assetId = $moWebLib->getObject('asset_id')->getValue();
		$asset = new RMAsset();

		if ($asset->fetchById($assetId)) {
			$msPath = $asset->getFieldValue('file_path');
			$msFileName = $asset->getFieldValue('file_name');
			header('Cache-Control: ');// leave blank to avoid IE errors
			header('Pragma: ');// leave blank to avoid IE errors
			if(strpos(strtolower($msFileName),"jpg") !== false || strpos(strtolower($msFileName),"jpeg" !== false)){
				header('Content-type: image/jpeg');
				header('Content-Disposition: inline; filename="'.$msFileName.'"');
			}else if(strpos(strtolower($msFileName),"gif") !== false){
				header('Content-type: image/gif');
				header('Content-Disposition: inline; filename="'.$msFileName.'"');
			}else if(strpos(strtolower($msFileName),"tiff") !== false || strpos(strtolower($msFileName),"tif") !== false){
				header('Content-type: image/tiff');
				header('Content-Disposition: inline; filename="'.$msFileName.'"');
			}else if(strpos(strtolower($msFileName),"bmp") !== false){
				header('Content-type: image/x-ms-bmp');
				header('Content-Disposition: inline; filename="'.$msFileName.'"');
			}else{
				header('Content-type: application/octet-stream');
				header('Content-Disposition: attachment; filename="'.$msFileName.'"');
			}

			$moCrypt = new FWDCrypt();
			$moFP = fopen($msPath,'rb');
			$moCrypt->setIV(fread($moFP,strlen($moCrypt->getIV())));
			while(!feof($moFP)) {
				echo $moCrypt->decryptNoBase64(fread($moFP,16384));
			}
			fclose($moFP);
		}
	}
}
function getAssetFromFields(){
	$moAsset = new RMAsset();
	$miAssetCategoryId = FWDWebLib::getObject('asset_category_id')->getValue();
	$msAssetName = FWDWebLib::getObject('asset_name')->getValue();
	$mAssetDescription = FWDWebLib::getObject('asset_description')->getValue();
	$mAssetContingency = trim(FWDWebLib::getObject('contingency')->getValue(),':');
	$mAssetContingencyDesc = trim(FWDWebLib::getObject('asset_contingency')->getValue(),':');
	$group = FWDWebLib::getObject('groupId')->getValue();

	$traFlag = FWDWebLib::getObject('traController')->getValue();
	if(substr($traFlag,0,1)==':') $traFlag = substr($traFlag, 1);
	
	$tacFlag = FWDWebLib::getObject('tacController')->getValue();
	
	if(substr($tacFlag,0,1)==':') $tacFlag = substr($tacFlag, 1);	
	

	$mAssetTac = FWDWebLib::getObject('asset_tac')->getValue();
	$mAssetRecovery = FWDWebLib::getObject('asset_recovery')->getValue();
	$place = FWDWebLib::getObject('placeId')->getValue();
	$place_id = FWDWebLib::getObject('place')->getValue();
	
	$moAsset->setFieldValue('asset_category_id', $miAssetCategoryId);
	$moAsset->setFieldValue('asset_name', $msAssetName);
	$moAsset->setFieldValue('asset_description', $mAssetDescription);
	$moAsset->setFieldValue('asset_contingency', $mAssetContingency);
	$moAsset->setFieldValue('asset_contingency_desc', $mAssetContingencyDesc);
	
	if($group){
		$moAsset->setFieldValue('group', $group);
	}else{
		$moAsset->setFieldValue('group', 'null');
	}
	
	if($place){
		$moAsset->setFieldValue('place', $place);
	}else{
		$moAsset->setFieldValue('place', "null");
	}
	
	if($place_id){
		$moAsset->setFieldValue('place', $place_id);
	}else{
		$moAsset->setFieldValue('place', "null");
	}

	$moAsset->setFieldValue('asset_tac', $mAssetTac);
	$moAsset->setFieldValue('asset_recovery', $mAssetRecovery);
	$moAsset->setFieldValue('asset_tra_flag', $traFlag);
	$moAsset->setFieldValue('asset_tac_flag', $tacFlag);

	return $moAsset;
}

function save($poContext,$pbHasSensitiveChanges,$piContextId=0){
	$moAsset = $poContext;
	$miAssetId = $piContextId;
	$msInsert = "";
	if($miAssetId){
		$moAsset->update($miAssetId,true,$pbHasSensitiveChanges);
	}else{
		$miAssetId = $moAsset->insert(true);
		$msInsert .=" gebi('asset_id').value={$miAssetId};";
	}

	$activityId = FWDWebLib::getObject('activityId')->getValue();
	if($activityId){
		$activityAsset = new CMProcessActivityAsset();
		$activityAsset->createFilter($miAssetId, "asset_id");
		$activityAsset->createFilter($activityId, "activity_id");
		$activityAsset->select();
		while($activityAsset->fetch()){
			$activityAsset->delete($activityAsset->getFieldValue("activity_asset_id"), true);
		}

		$activityAsset = new CMProcessActivityAsset();
		$importance = FWDWebLib::getObject('importance')->getValue();
		if($importance){
			$activityAsset->setFieldValue('asset_importance', $importance);
		}
		$activityAsset->setFieldValue('asset_id', $miAssetId);
		$activityAsset->setFieldValue('activity_id', $activityId);
		$activityAsset->insert();
	}

	$processId = FWDWebLib::getObject('processId')->getValue();
	if($processId){
		$processAsset = new RMProcessAsset();
		$processAsset->createFilter($miAssetId, "asset_id");
		$processAsset->createFilter($processId, "process_id");
		$processAsset->select();
		while($processAsset->fetch()){
			$processAsset->delete($processAsset->getFieldValue("process_asset_id"), true);
		}

		$processAsset = new RMProcessAsset();
		$importance = FWDWebLib::getObject('importance')->getValue();
		if($importance){
			$processAsset->setFieldValue('importance', $importance);
		}
		$processAsset->setFieldValue('asset_id', $miAssetId);
		$processAsset->setFieldValue('process_id', $processId);
		$processAsset->insert();
	}

	$moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
	$moSession->addAttribute("documentContextId");
	$moSession->setAttrDocumentContextId($piContextId);

	// Verifica se o TAC � inferior ao TRA
	$moQueryTacTraAsset = new QueryCheckTacTraAsset(FWDWebLib::getConnection());
	$moQueryTacTraAsset->setAssetId($miAssetId);
	$moQueryTacTraAsset->makeQuery();
	$moQueryTacTraAsset->fetchTacTraValue();
	$tacIsGreaterThanTra = $moQueryTacTraAsset->checkTacTraValue();
	if($tacIsGreaterThanTra == true){
		echo "gobi('warning_tac_tra_asset').show();";
		return;
	}
	
	echo "$msInsert js_submit('upload','ajax');setTimeout('trigger_event(\"save_complete_event\",3)',800);";
}

class SearchGroup extends FWDRunnable {
	public function run(){
		$group = FWDWebLib::getObject('group')->getValue();
		$groupOld = FWDWebLib::getObject('groupOld')->getValue();

		if($group != $groupOld && trim($group) != ""){
			$query = new QueryGridGroupSearch(FWDWebLib::getConnection());
			$query->setNameFilter($group);
			$query->setLimit(5);
			$query->makeQuery();
			$query->executeQuery();

			$groupHeight = FWDWebLib::getObject('group')->getObjFWDBox()->getAttrHeight();
			$groupTop = FWDWebLib::getObject('group')->getObjFWDBox()->getAttrTop()+$groupHeight;
			$groupLeft = FWDWebLib::getObject('group')->getObjFWDBox()->getAttrLeft();

			echo "var position = {top: {$groupTop}, left:{$groupLeft}}; ";
			echo "var listElements = new Array(); ";
			$i = 0;
			while($query->fetch()){
				$name = $query->getFieldValue("group_name");
				$id = $query->getFieldValue("group_id");
				echo "listElements[{$i}] = {name:'{$name}', id:'{$id}'}; ";
				$i++;
			}
			echo "createAutoCompleteDiv(listElements, 'group', 'groupId', position);";
		}
		echo "gebi('groupOld').value = '{$group}';";
	}
}

class ConfirmEditEvent extends FWDRunnable {
	public function run(){
		$miAssetId = FWDWebLib::getObject('asset_id')->getValue();
		$moAsset = getAssetFromFields();

		save($moAsset,true,$miAssetId);
	}
}

class SaveAssetEvent extends FWDRunnable {
	public function run(){
		$groupName = FWDWebLib::getObject('group')->getValue();
		$group = FWDWebLib::getObject('groupId')->getValue();
		
		$moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
		if($moSession->attributeExists('policySaveComplete'))  $moSession->deleteAttribute('policySaveComplete');
		if($moSession->attributeExists('uploadErrorCode'))     $moSession->deleteAttribute('uploadErrorCode');
		if($moSession->attributeExists('documentFilePath'))    $moSession->deleteAttribute('documentFilePath');
		if($moSession->attributeExists('documentFileName'))    $moSession->deleteAttribute('documentFileName');
		if($moSession->attributeExists('documentFileSize'))    $moSession->deleteAttribute('documentFileSize');
		if($moSession->attributeExists('documentFileContent')) $moSession->deleteAttribute('documentFileContent');
		if($moSession->attributeExists('documentContextId'))   $moSession->deleteAttribute('documentContextId');

		$miAssetId = FWDWebLib::getObject('asset_id')->getValue();
		$moAsset = getAssetFromFields();
		$search = new RMAsset();
		$search->execute("SELECT fkContext as asset_id FROM view_rm_asset_active WHERE lower(sname) = lower('".$moAsset->getFieldValue('asset_name')."')");
		
		// Caso o groupo respons�vel seja vazio
		if($groupName == ''){
			$moAsset->setFieldValue('group', 'null');
		}else{
			$moAsset->setFieldValue('group', $group);
		}
				
		// Verifica se o ativo j� existe
		while($search->fetch()) {
			if($miAssetId){
				if($search->getFieldValue('asset_id') != $miAssetId){
					echo "gobi('warning_duplicated_asset').show();";
					return;
				}
			}else{
				echo "gobi('warning_duplicated_asset').show();";
				return;
			}
		}
		
		$miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
		
		save($moAsset,true,$miAssetId);
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$miAssetId = FWDWebLib::getObject('param_asset_id')->getValue();
		FWDWebLib::getObject('asset_id')->setValue($miAssetId);

		$moSelect = FWDWebLib::getObject('asset_category_id');
		$moQuery = new QueryContextHierarchicalStructure(FWDWebLib::getConnection());
		$moQuery->setContextType(CONTEXT_CATEGORY);

		$moQuery->makeQuery();
		$moQuery->executeQuery();
		foreach ($moQuery->getContexts() as $maContext) {
			list($miContextId,$msHierarchicalName) = $maContext;
			$moSelect->setItemValue($miContextId,$msHierarchicalName);
		}

		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(''));
		//		$moStartEvent->addAjaxEvent(new SearchGroup('search_group'));

		$moStartEvent->addAjaxEvent(new SaveAssetEvent("save_asset"));
		$moStartEvent->addAjaxEvent(new SaveCompleteEvent("save_complete_event"));
		$moStartEvent->addSubmitEvent(new SubmitEvent("upload"));
		$moStartEvent->addSubmitEvent(new DownloadEvent("download"));

		$defaultFinderGroup = new DefaultFinder('search_group');
		$defaultFinderGroup->set('group', new QueryGridGroupSearch(FWDWebLib::getConnection()));
		$moStartEvent->addAjaxEvent($defaultFinderGroup);

		//$defaultFinderPlace = new DefaultFinder('search_place');
		//$defaultFinderPlace->set('place', new QueryGridPlaceSearch(FWDWebLib::getConnection()));
		//$moStartEvent->addAjaxEvent($defaultFinderPlace);
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$miAssetId = FWDWebLib::getObject('asset_id')->getValue();

		$placeId = 0;
		$process = FWDWebLib::getObject('process')->getValue();
		$activity = FWDWebLib::getObject('activity')->getValue();

		FWDWebLib::getObject('processId')->setValue($process);
		FWDWebLib::getObject('activityId')->setValue($activity);

		$moWindowTitle = FWDWebLib::getObject('window_title');
		
		$moHandler = new QuerySelectPlaceUN(FWDWebLib::getConnection());
		$moSelect = FWDWebLib::getObject('place');
		$moSelect->setQueryHandler($moHandler);
		$moSelect->populate();

		if($process || $activity){
			$moHandler = new QuerySelectPriority(FWDWebLib::getConnection());
			$moSelect = FWDWebLib::getObject('importance');
			$moSelect->setQueryHandler($moHandler);
			$moSelect->populate();

			$pId = $process;

			if($activity){
				$a = new CMProcessActivity();
				$a->fetchById($activity);
				$pId = $a->getFieldValue("activity_process");
			}
			if($pId){
				$p = new CMPlaceProcess();
				$p->createFilter($pId, 'process_id');
				$p->select();
				while($p->fetch()){
					$placeId = $p->getFieldValue('place_id');
					break;
				}
			}

		}else{
			FWDWebLib::getObject('importance')->setAttrDisplay('false');
			FWDWebLib::getObject('label_importance')->setAttrDisplay('false');
		}

		if($miAssetId){
			$moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_asset_editing', 'Edi��o de Ativo'));
			$moAsset = new RMAsset();
			$moAsset->fetchById($miAssetId);

			FWDWebLib::getObject('asset_name')->setValue($moAsset->getFieldValue('asset_name'));
			FWDWebLib::getObject('asset_category_id')->checkItem($moAsset->getFieldValue('asset_category_id'));
			FWDWebLib::getObject('asset_description')->setValue($moAsset->getFieldValue('asset_description'));

			if($moAsset->getFieldValue('place')){
				$place = new CMPlace();
				$place->fetchById($moAsset->getFieldValue('place'));
				FWDWebLib::getObject('placeId')->setValue($moAsset->getFieldValue('place'));
				FWDWebLib::getObject('place')->checkItem($place->getFieldValue('place_id'));
			}

			FWDWebLib::getObject('asset_contingency')->setValue($moAsset->getFieldValue('asset_contingency_desc'));
			FWDWebLib::getObject('contingency')->setValue($moAsset->getFieldValue('asset_contingency'));

			FWDWebLib::getObject('asset_tac')->setValue($moAsset->getFieldValue('asset_tac'));
			FWDWebLib::getObject('asset_recovery')->setValue($moAsset->getFieldValue('asset_recovery'));
			FWDWebLib::getObject('traController')->setValue($moAsset->getFieldValue('asset_tra_flag'));
			FWDWebLib::getObject('tacController')->setValue($moAsset->getFieldValue('asset_tac_flag'));
			FWDWebLib::getObject('initial_category_id')->setValue($moAsset->getFieldValue('asset_category_id'));

			if($moAsset->getFieldValue('group')){
				$group = new CMGroup();
				$group->fetchById($moAsset->getFieldValue('group'));
				FWDWebLib::getObject('group')->setValue($group->getFieldValue('group_name'));
				FWDWebLib::getObject('groupId')->setValue($group->getFieldValue('group_id'));
			}

			//File
			$moWebLib = FWDWebLib::getInstance();
			$msPath = $moAsset->getFieldValue('file_path');
			if ($msPath && is_file($msPath)) {
				$msFileName = $moAsset->getFieldValue('file_name');

				$link = "<a href=javascript:try{js_submit('download');}catch(e){debugException(e)}>";
				$closeLink = "</a>";

				$fileName = $moWebLib->getObject('file_name');
				$fileName->setValue($link.$msFileName.$closeLink);
				$fileName->setAttrDisplay("true");
				$moWebLib->getObject('file')->setAttrDisplay("false");
				$moWebLib->getObject('changeFile')->setAttrDisplay("true");
			}

			if($process){
				$processAsset = new RMProcessAsset();
				$processAsset->createFilter($miAssetId, "asset_id");
				$processAsset->createFilter($process, "process_id");
				$processAsset->select();
				if($processAsset->fetch()){
					$moSelect = FWDWebLib::getObject('importance');
					$moSelect->checkItem($processAsset->getFieldValue('importance'));
				}
			}

			if($activity){
				$activityAsset = new CMProcessActivityAsset();
				$activityAsset->createFilter($miAssetId, "asset_id");
				$activityAsset->createFilter($activity, "activity_id");
				$activityAsset->select();
				if($activityAsset->fetch()){
					$moSelect = FWDWebLib::getObject('importance');
					$moSelect->checkItem($activityAsset->getFieldValue('asset_importance'));
				}
			}

		}else{
			$placeId = FWDWebLib::getObject('param_place_id')->getValue();
				
			if($placeId)
				FWDWebLib::getObject('place')->checkItem($placeId);
			else
				FWDWebLib::getObject('place')->setValue("");

			$moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_asset_adding', 'Adi��o de Ativo'));
		}

		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>

<script language="javascript">
        gebi('asset_name').focus();
        
        function set_security_responsible(id, name){
          gebi('asset_security_responsible_id').value = id;
          gobi('asset_security_responsible_name').setValue(name);
        }
        
        if(gebi('asset_cost'))
          gebi('asset_cost').value = format_currency(gebi('asset_cost').value);

        if(gebi('contingency').value == 0){
        	hideContigency();
        }

        function hideContigency(){
        	gobi("asset_tac").hide();
            gobi("tacLabel").hide();
            //gobi("asset_tac").resetField();
            gobi("asset_contingency").hide();
			//gobi("asset_contingency").resetField();
			gobi("contingencyLabel").hide();
			gobi("helper").hide();
			gobi("st_minutos_tac").hide();
			gobi("st_horas_tac").hide();
			gobi("st_dias_tac").hide();
			gobi("st_meses_tac").hide();
			gobi("tacController").hide();
        }

        function setPlace(id, name, desc){
	 		gebi("placeId").value = id;
	        gebi("place").value = name;
     	}
            
        function setGroup(id, name, acronym){
            gebi("groupId").value = id;
            gebi("group").value = name;
        } 

        function changeFile(){
    		gebi("file").style.display="";
    		gebi("changeFile").style.display="none";
    		gebi("file_name").style.display="none";
    	}
        
      </script>
		<?
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_asset_edit.xml');

?>