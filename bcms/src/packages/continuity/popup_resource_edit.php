<?php

include_once 'include.php';
include_once $handlers_ref . "select/QueryResourceFromGroup.php";
include_once $handlers_ref . "select/QuerySelectDepartment.php";
include_once $handlers_ref . "select/QuerySelectResource.php";
include_once $handlers_ref . "select/QuerySelectGroup.php";

class SaveResourceEvent extends FWDRunnable {
	public function run(){
		$resourceName = FWDWebLib::getObject('resourceName')->getValue();
		$resourceFunction = FWDWebLib::getObject('resourceFunction')->getValue();
		$resourceId = FWDWebLib::getObject('resourceId')->getValue();
		$department = FWDWebLib::getObject('departments')->getValue();
		$place = FWDWebLib::getObject('placeId')->getValue();
		$provider = FWDWebLib::getObject('providerId')->getValue();
		
		//Remover
		//$area = FWDWebLib::getObject('areaId')->getValue();

		$comercialNumber = FWDWebLib::getObject('comercialNumber')->getValue();
		$celNumber = FWDWebLib::getObject('celNumber')->getValue();
		$nextelNumber = FWDWebLib::getObject('nextelNumber')->getValue();
		$email = FWDWebLib::getObject('email')->getValue();

		$resource = new CMResource();
		if($department){
			$resource->setFieldValue('department', $department);
		}else{
			$resource->setFieldValue('department', 'null');
		}

		$resource->setFieldValue('resource_name', $resourceName);
		$resource->setFieldValue('resource_function', $resourceFunction);
		if($provider){
			$resource->setFieldValue('provider', '1');
		}else{
			$resource->setFieldValue('provider', '0');
		}

		$resource->setFieldValue('comercialNumber', $comercialNumber);
		$resource->setFieldValue('celNumber', $celNumber);
		$resource->setFieldValue('nextelNumber', $nextelNumber);

		$resource->setFieldValue('email', $email);

		$msNewGroups = FWDWebLib::getObject('groups_ids')->getValue();
		if($msNewGroups){
			$maNewGroups = explode(':',$msNewGroups);
		}else{
			$maNewGroups = array();
		}

		if($resourceId){
			$resource->update($resourceId, true, true);
		}else{
			$resourceId = $resource->insert(true);
			if($place){

				$cmt = FWDWebLib::getObject('cmt')->getValue();
				if($cmt){
					$groupCMT = new CMGroup();
					$groupCMT = $groupCMT->getCMT($place);
					$groupId = $groupCMT->getFieldValue('group_id');
					$maNewGroups[] = $groupId;
				}else{
					$r = new CMResource();
					$r->setFieldValue('resource_id',$resourceId);
					$r->updateRelation(new CMPlaceResource(),'place_id',array($place));
				}
			}

			if($provider){
				/*$r = new CMResource();
				$r->setFieldValue('resource_id',$resourceId);
				$r->updateRelation(new CMProviderResource(),'id',array($provider));*/
			  
			  $providerResource = new CMProviderResource();
        $providerResource->setFieldValue('id', $provider);
        $providerResource->setFieldValue('resource_id', $resourceId);
        
        $pr = new CMProviderResource();
        $pr->createFilter($provider,'id');
        $pr->select();
        
        if(!$pr->fetch())
          $providerResource->setFieldValue('is_default', 1);
        
        $providerResource->insert();
			}

			// Remover
			/*if($area){
				$r = new CMResource();
				$r->setFieldValue('resource_id',$resourceId);
				$r->updateRelation(new CMAreaResource(),'area_id',array($area));
			}*/
			
			$unId = FWDWebLib::getObject('un_id')->getValue();
			
      if($unId){
				$r = new CMResource();
				$r->setFieldValue('resource_id',$resourceId);
				$r->updateRelation(new CMAreaResource(),'area_id',array($unId));			
      }
		}

		if(!$provider){
			$moQuery = new QueryResourceFromGroup(FWDWebLib::getConnection());
			$moQuery->setResourceId($resourceId);
			$moQuery->makeQuery();
			$moQuery->fetchResources();
			$maOldGroups = $moQuery->getResources();

			$maToInsert = array_diff($maNewGroups,$maOldGroups);
			$maToDelete = array_diff($maOldGroups,$maNewGroups);
			$moResourceGroup = new CMGroupResource();
			if(count($maToInsert)) $moResourceGroup->insertGroup($resourceId,$maToInsert);
			if(count($maToDelete)) $moResourceGroup->deleteGroup($resourceId,$maToDelete);
		}
		echo "soWindow = soPopUpManager.getPopUpById('popup_resource_edit').getOpener();"
		."if(soWindow.refresh_grid) soWindow.refresh_grid();"
		."soPopUpManager.closePopUp('popup_resource_edit');";
	}
}

class RefreshGroupEvent extends FWDRunnable {
	public function run(){
		$moHandler = new QuerySelectGroup(FWDWebLib::getConnection());
		$groupsIds = FWDWebLib::getObject('groups_ids')->getValue();
		if($groupsIds){
			$moHandler->setGroupIds($groupsIds);

			$moSelect = FWDWebLib::getObject('groups');
			$moSelect->setQueryHandler($moHandler);
			$moSelect->populate();
			$moSelect->execEventPopulate();
		}
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(''));
		$moStartEvent->addAjaxEvent(new SaveResourceEvent('SaveResourceEvent'));
		$moStartEvent->addAjaxEvent(new RefreshGroupEvent('refresh_groups'));
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$moWebLib = FWDWebLib::getInstance();
		$moWindowTitle = $moWebLib->getObject('window_title');
		$miResourceId = $moWebLib->getObject('param_resource_id')->getValue();
		$miGroupId = $moWebLib->getObject('param_group_id')->getValue();

		$place = $moWebLib->getObject('place')->getValue();
		if($place){
			$moWebLib->getObject('placeId')->setValue($place);
		}

		$provider = $moWebLib->getObject('provider')->getValue();
		if($provider){
			$moWebLib->getObject('providerId')->setValue($provider);
		}

		/*$area = $moWebLib->getObject('area')->getValue();
		if($area){
			$moWebLib->getObject('areaId')->setValue($area);
		}
		*/

		$moHandler = new QuerySelectDepartment(FWDWebLib::getConnection());
		$moSelect = FWDWebLib::getObject('departments');
		$moSelect->setQueryHandler($moHandler);
		$moSelect->populate();
			
		if(!$miResourceId) {
			$moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_resource_adding', 'Adicionar Contato'));
		} else {
			$moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_resource_editing', 'Editar Contato'));
			FWDWebLib::getObject('resourceId')->setValue($miResourceId);
			$resource = new CMResource();
			$resource->fetchById($miResourceId);

			FWDWebLib::getObject('resourceName')->setValue($resource->getFieldValue('resource_name'));
			FWDWebLib::getObject('resourceFunction')->setValue($resource->getFieldValue('resource_function'));
			FWDWebLib::getObject('departments')->checkItem($resource->getFieldValue('department'));
			
			FWDWebLib::getObject('comercialNumber')->setValue($resource->getFieldValue('comercialNumber'));
			FWDWebLib::getObject('celNumber')->setValue($resource->getFieldValue('celNumber'));
			FWDWebLib::getObject('nextelNumber')->setValue($resource->getFieldValue('nextelNumber'));
			
			FWDWebLib::getObject('email')->setValue($resource->getFieldValue('email'));
		}

		$groups = FWDWebLib::getObject('groups');
		if($miResourceId) {
			$moQuery = new QueryResourceFromGroup(FWDWebLib::getConnection());
			$moQuery->setResourceId($miResourceId);
			$moQuery->makeQuery();
			$moQuery->executeQuery();
			$moGroups = array();
			while($moQuery->fetch()) {
				$groups->setItemValue($moQuery->getFieldValue('resource_id'), $moQuery->getFieldValue('resource_name'));
				$moGroups[] = $moQuery->getFieldValue('resource_id');
			}
			FWDWebLib::getObject('groups_ids')->setValue(implode(':',$moGroups));
		}else if($miGroupId){
			$moGroups = array();
			$group = new CMGroup();
			$group->fetchById($miGroupId);
			$groups->setItemValue($group->getFieldValue('group_id'), $group->getFieldValue('group_name'));
			$moGroups[] = $miGroupId;
			FWDWebLib::getObject('groups_ids')->setValue(implode(':',$moGroups));
		}

		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
<script language="javascript">

			function set_groups(groupIds) {
			  	gebi('groups_ids').value = groupIds;
			  	groupIds=null;
			  	trigger_event('refresh_groups',3);
			}	

        gebi('resourceName').focus();
        if(gebi('providerId').value != 0){
			js_hide("label_groups");
			js_hide("groups");
			js_hide("add_groups");
			js_hide("removeGroups");
			js_hide("add_groups_label");
        }
    </script>
		<?
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_resource_edit.xml');

?>