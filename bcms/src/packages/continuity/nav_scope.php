<?php
include_once "include.php";
include_once $handlers_ref . "grid/QueryGridScope.php";

class GridScope extends FWDDrawGrid {
	public function drawItem(){
		switch($this->ciColumnIndex){
			default:
				return parent::drawItem();
				break;
		}
	}
}

class AreaConfirmRemove extends FWDRunnable {
	public function run(){
		$msTitle = FWDLanguage::getPHPStringValue('tt_remove_scope','Remover Escopo');
		$msMessage = FWDLanguage::getPHPStringValue('st_scope_remove_confirm',"Voc� tem certeza de que deseja remover o escopo <b>%scope_name%</b>?");

		$moScope = new CMScope();
		$moScope->fetchById(FWDWebLib::getObject('selected_area_id')->getValue());
		$msScopeName = ISMSLib::truncateString($moScope->getFieldValue('scope_name'), 70);
		$msMessage = str_replace("%scope_name%",$msScopeName,$msMessage);

		$msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
                    "soWindow = soPopUp.getOpener();" .
                    "soWindow.remove_scope();";

		ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,50);
	}
}

class RemoveScopeEvent extends FWDRunnable {
	public function run(){
		$miScopeId = FWDWebLib::getObject('selected_area_id')->getValue();
		$moScope = new CMScope();
		$moScope->delete($miScopeId);
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));
		$moStartEvent->addAjaxEvent(new AreaConfirmRemove('area_confirm_remove'));
		$moStartEvent->addAjaxEvent(new RemoveScopeEvent('remove_scope_event'));

		//instala a seguran�a de ACL na p�gina
		FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
		FWDMenuACLSecurity::getInstance()->installMenuACL(FWDWebLib::getObject('menu'),FWDACLSecurity::getInstance());

		$moGrid = FWDWebLib::getObject("grid_scope");
		$moHandler = new QueryGridScope(FWDWebLib::getConnection());

		if (!FWDWebLib::getObject("root_area_id")->getValue())
		FWDWebLib::getObject("root_area_id")->setValue(FWDWebLib::getObject("area_id")->getValue());

		if ((!FWDWebLib::getObject("root_area_id")->getValue())&&(FWDWebLib::getObject("par_area_id")->getValue())){
			FWDWebLib::getObject("root_area_id")->setValue(FWDWebLib::getObject("par_area_id")->getValue());
		}

		$moGrid->setQueryHandler($moHandler);
		$moGrid->setObjFwdDrawGrid(new GridScope());

		$maDeniedACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();

		if(FWDWebLib::getObject("root_area_id")->getValue()){
			if(!ISMSLib::userHasACL('M.RM.1.7') && !ISMSLib::userHasACL('M.RM.1.6')){
				FWDWebLib::getObject('viewbutton_insert')->setAttrDisplay("false");
			}
		}else{
			if(!ISMSLib::userHasACL('M.RM.1.7')){
				FWDWebLib::getObject('viewbutton_insert')->setAttrDisplay("false");
			}
		}
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>

<script language="javascript">

    function refresh_grid() {
    	js_refresh_grid('grid_scope');    
    }
    
    function remove_scope() {
	    trigger_event('remove_scope_event', 3);
	    refresh_grid();        
    }
    
    function goToProcesses(piId){
    	isms_open_popup('popup_process_search','packages/continuity/popup_process_search.php?scope='+piId,'','true');
    }

    function goToPlaces(id){
		isms_open_popup('popup_place_search','packages/continuity/popup_place_search.php?scope='+id,'','true');
    }

    function goToAreas(id){
    	isms_open_popup('popup_area_search','packages/continuity/popup_area_search.php?scope='+id,'','true');
    }

    function goToAssets(id){
    	var filterScope = "&scope="+id;
		isms_open_popup('popup_asset_search','packages/continuity/popup_asset_search.php?a=1'+filterScope,'','true');
    }

    function viewScope(id){
    	isms_open_popup('popup_scope_view','packages/continuity/popup_scope_view.php?scope='+id,'','true');
    }
    
    </script>
		<?
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_scope.xml");
?>