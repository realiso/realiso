<?php

include_once 'include.php';
include_once $handlers_ref . "select/continuity/QuerySelectAssetImportance.php";
//include_once $handlers_ref . "select/continuity/QuerySelectActivityPeople.php";
include_once $handlers_ref . "select/QuerySelectReadableDocumentByContext.php";

function saveAssetFromFields($piContext){
	$miActivityId = FWDWebLib::getObject('activity_id')->getValue();
	$moProcessActivityAsset = new CMProcessActivityAsset();

	$miAssetId = FWDWebLib::getObject('asset_id')->getValue();
	$miAssetImportance = FWDWebLib::getObject('importance')->getValue();
    
	$moProcessActivityAsset->setFieldValue('activity_id',$miActivityId);
	$moProcessActivityAsset->setFieldValue('asset_id', $miAssetId);
	$moProcessActivityAsset->setFieldValue('asset_importance', $miAssetImportance);
	
	$fd = fopen("/tmp/activity.txt","a+");
	fwrite($fd,"context: " . $piContext . "\n");
	
  
	if($piContext) {
		$moProcessActivityAsset->update($piContext,true,true);
	} else {
		$piContext = $moProcessActivityAsset->insert(true);
	}
	
	
	return $moProcessActivityAsset;
}


class SaveAssetEvent extends FWDRunnable {
  public function run(){
    $miActivityAssetId = FWDWebLib::getObject('activity_asset_id')->getValue();
    saveAssetFromFields($miActivityAssetId);
  echo "soWindow = soPopUpManager.getPopUpById('popup_activity_asset_edit').getOpener();"
      ."if(soWindow.refresh_grid) soWindow.refresh_grid();"
      ."soPopUpManager.closePopUp('popup_activity_asset_edit');";
    
  }
}


class DocumentViewEvent extends FWDRunnable {
  public function run() {
    $moWebLib = FWDWebLib::getInstance();
    $miDocumentId = $moWebLib->getObject('select_area_document')->getValue();
    $miUserId = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    
    $moDocument = new PMDocument();
    $moDocument->testPermissionToRead($miDocumentId);
    $moDocument->fetchById($miDocumentId);
    $moPMDocInstance = $moDocument->getPublishedInstance();
    $miDocInstanceId = $moPMDocInstance->getFieldValue('doc_instance_id');
    
    $moReader = new PMDocumentReader();
    $moReader->createFilter($miDocumentId,'document_id');
    $moReader->createFilter($miUserId,'user_id');
    $moReader->select();
    if ($moReader->fetch()) {
      $moReader->setFieldValue('user_has_read_document',1);
      $moReader->update();
    }
    
    $moPMDocReadHistory = new PMDocReadHistory();
    $moPMDocReadHistory->setFieldValue('user_id',$miUserId);
    $moPMDocReadHistory->setFieldValue('document_id',$miDocInstanceId);
    $moPMDocReadHistory->setFieldValue('read_date',ISMSLib::ISMSTime());
    $moPMDocReadHistory->insert();
    
    $mbIsLink = (bool)$moPMDocInstance->getFieldValue('doc_instance_is_link');
    if ($mbIsLink) {
      $msURL = $moPMDocInstance->getFieldValue('doc_instance_link');
      if ($msURL)
        echo "js_open('{$msURL}','document_link',740,502,'center_screen');";
    } else {
      echo "js_submit('download_file','ajax');";
    }
  }
}

class DownloadFileEvent extends FWDRunnable {
  public function run() {
    $moWebLib = FWDWebLib::getInstance();
    $miDocumentId = $moWebLib->getObject('select_area_document')->getValue();
    $miUserId = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    
    $moDocument = new PMDocument();
    $moDocument->testPermissionToRead($miDocumentId);
    $moDocument->fetchById($miDocumentId);
    $moPMDocInstance = $moDocument->getPublishedInstance();
    
    $msPath = $moWebLib->getSysRef()."packages/policy/".$moPMDocInstance->getFieldValue('doc_instance_path');
    $msFileName = $moPMDocInstance->getFieldValue('doc_instance_file_name');
    header('Cache-Control: ');// leave blank to avoid IE errors
    header('Pragma: ');// leave blank to avoid IE errors
    header('Content-type: application/octet-stream');
    header('Content-Disposition: attachment; filename="'.$msFileName.'"');
    $moCrypt = new FWDCrypt();
    $moFP = fopen($msPath,'rb');
    $moCrypt->setIV(fread($moFP,strlen($moCrypt->getIV())));
    while(!feof($moFP)) {
      echo $moCrypt->decryptNoBase64(fread($moFP,16384));
    }
    fclose($moFP);
  }
}



class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
    $moStartEvent->addAjaxEvent(new SaveAssetEvent('save_asset_event'));
    $moStartEvent->addAjaxEvent(new DocumentViewEvent('document_view_event'));
    $moStartEvent->addSubmitEvent(new DownloadFileEvent('download_file'));
    
//    $moStartEvent->addAjaxEvent(new RefreshPeopleEvent('refresh_people_event'));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $miActivityId = FWDWebLib::getObject('param_activity_id')->getValue();
    FWDWebLib::getObject('activity_id')->setValue($miActivityId);

    $miActivityAssetId = FWDWebLib::getObject('param_asset_id')->getValue();
    FWDWebLib::getObject('activity_asset_id')->setValue($miActivityAssetId);
    
    
    $moWindowTitle = FWDWebLib::getObject('window_title');
    $moIcon = FWDWebLib::getObject('tooltip_icon');
    
    $moHandler = new QuerySelectAssetImportance(FWDWebLib::getConnection());
    $moSelect = FWDWebLib::getObject('importance');
    $moSelect->setQueryHandler($moHandler);
    
    
    
    if($miActivityAssetId){
      $moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_asset_editing', 'Edi��o de Ativo'));

      $moProcessActivityAsset = new CMProcessActivityAsset();
      $moProcessActivityAsset->fetchById($miActivityAssetId);
      $moAsset = new RMAsset();
      $moAsset->fetchById($moProcessActivityAsset->getFieldValue('asset_id'));
      
      $moProcessActivityAsset->fetchById($miActivityAssetId);
      FWDWebLib::getObject('asset_name')->setValue($moAsset->getFieldValue('asset_name'));
      FWDWebLib::getObject('asset_id')->setValue($moProcessActivityAsset->getFieldValue('asset_id'));

	  $moSelectChecked = new FWDItem();
	  $moSelectChecked->setAttrKey($moProcessActivityAsset->getFieldValue('asset_importance'));
		
	  $moSelect->addObjFWDItem($moSelectChecked);
      

    }else{
    	
      $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
      $moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_asset_adding', 'Adi��o de Ativo'));
    }
    
    $moSelect->populate();

    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        gebi('asset_name').focus();
        
        function set_asset(id, name){
          gebi('asset_id').value = id;
          gobi('asset_name').setValue(name);
        }
        
      </script>      
      
    <?
    
    
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_activity_asset_edit.xml');

?>