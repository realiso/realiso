<?php
include_once "include.php";
include_once $handlers_ref . "grid/QueryGridScene.php";
include_once $handlers_ref . "select/QuerySelectArea.php";
include_once $handlers_ref . "select/QuerySelectProcessPriority.php";
include_once $handlers_ref . "select/QuerySelectProcessType.php";

class SearchEvent extends FWDRunnable {
	public function run(){
		$moGrid = FWDWebLib::getObject('grid_process');
		$moGrid->execEventPopulate();
	}
}

class GridProcess extends FWDDrawGrid {
	protected $processId = 0;
	public function setProcess($id){
		$this->processId = $id;
	}
	public function drawItem(){
		switch($this->ciColumnIndex){
			case $this->getIndexByAlias('scene_id'):
				continue;
			case $this->getIndexByAlias('scene_name'):

				$miProcessValue = $this->getFieldValue('scene_name');

				$this->coCellBox->setAttrStringNoEscape("true");
				$this->coCellBox->setValue("<a href='javascript:go_to_nav_impact({$this->getFieldValue('scene_id')},{$this->processId});'><font color='#0000ff'>{$miProcessValue}</font></a>");

				return $this->coCellBox->draw();
				break;
			default:
				return parent::drawItem();
				break;
		}
	}
}

class DuplicateEvent extends FWDRunnable {
	public function run() {
		$miSceneId = FWDWebLib::getObject('selected_scene_id')->getValue();
		$moScene = new CMScene();
		$moScene->fetchById($miSceneId);

		$moNewScene = new CMScene();
		$moNewScene->cloneEntity($moScene);

		$miNewSceneId = $moNewScene->insert(true);
		$moNewScene->executeDefaultInsert($miNewSceneId);

		echo "refresh_grid(); editScene({$miNewSceneId});";
	}
}

class SceneConfirmRemove extends FWDRunnable {
	public function run(){
		$msTitle = FWDLanguage::getPHPStringValue('tt_remove_scene','Remover Cen�rio');
		$msMessage = FWDLanguage::getPHPStringValue('st_scene_remove_confirm',"Voc� tem certeza que deseja remover o cen�rio <b>%scene_name%</b>?");

		$moScene = new CMScene();
		$moScene->fetchById(FWDWebLib::getObject('selected_scene_id')->getValue());
		$msSceneName = ISMSLib::truncateString($moScene->getFieldValue('scene_name'), 70);
		$msMessage = str_replace("%scene_name%",$msSceneName,$msMessage);

		$msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
                    "soWindow = soPopUp.getOpener();" .
                    "soWindow.remove_scene();";

		ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,50);
	}
}

class AssociateAssetsToProcessEvent extends FWDRunnable {
	public function run(){
		$miProcessId = FWDWebLib::getObject('selected_process_id')->getValue();
		$maAssets = FWDWebLib::getObject('selected_assets_ids')->getValue();

		$maAssets = explode(",", $maAssets);

		$moProcessAsset = new RMProcessAsset();
		$moProcessAsset->setFieldValue('process_id', $miProcessId);
		foreach ($maAssets as $miAssetId) {
			$moProcessAsset->setFieldValue('asset_id', $miAssetId);
			$moProcessAsset->insert();
		}
	}
}

class RemoveSceneEvent extends FWDRunnable {
	public function run(){
		$miSceneId = FWDWebLib::getObject('selected_scene_id')->getValue();
		$moScene = new CMScene();
		$moScene->delete($miSceneId);
	}
}

class RedirectToProcessArea extends FWDRunnable {
	public function run(){
		$moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
		$moSession->deleteAttribute('areaid');
		$moSession->addAttribute('areaid');
		$moSession->setAttrAreaId(FWDWebLib::getObject('selected_process_area')->getValue());
		echo "isms_change_to_sibling(TAB_SCOPE,'nav_area.php?uniqid=".uniqid()."');";
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));
		$moStartEvent->addAjaxEvent(new SceneConfirmRemove('scene_confirm_remove'));
		$moStartEvent->addAjaxEvent(new RemoveSceneEvent('remove_scene_event'));
		$moStartEvent->addAjaxEvent(new SearchEvent('search'));
		$moStartEvent->addAjaxEvent(new DuplicateEvent('duplicate_event'));
		
		//instala a seguran�a de ACL na p�gina
		FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
		FWDMenuACLSecurity::getInstance()->installMenuACL(FWDWebLib::getObject('menu'),FWDACLSecurity::getInstance());

		$moGrid = FWDWebLib::getObject("grid_scene");
		$moHandler = new QueryGridScene(FWDWebLib::getConnection());

		if(!ISMSLib::userHasACL('M.RM.2.1')){
			$moHandler->setUserId(ISMSLib::getCurrentUserId());
		}

		$miProcess = explode(":", FWDWebLib::getObject('par_scene_process_id')->getValue() );
		$miProcessId = $miProcess[0];

		if($miProcessId){
			$moHandler->setProcess($miProcessId);
		}

		$moGrid->setQueryHandler($moHandler);
		$g = new GridProcess();
		$g->setProcess($miProcessId);
		$moGrid->setObjFwdDrawGrid($g);
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$miProcessId = FWDWebLib::getObject('par_scene_process_id')->getValue();

		FWDWebLib::getObject('scene_process_id')->setValue($miProcessId);
		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));

		if($miProcessId){
			$sceneString = FWDLanguage::getPHPStringValue('tt_scenes_bl','Cen�rios');
			ISMSLib::getPathScrollCode(RISK_MANAGEMENT_MODE, CONTEXT_PROCESS, $miProcessId, $sceneString, false, '');
		}

		?>
<script language="javascript">
      
        function refresh_grid() {
          js_refresh_grid('grid_scene');
        }
        
        function set_assets(assets) {
          gebi('selected_assets_ids').value = assets;
          trigger_event('associate_assets_to_process_event', 3);
          refresh_grid();
        }
        
        function remove_scene() {
          trigger_event('remove_scene_event', 3);
          refresh_grid();
        }
        
        function go_to_nav_asset(piId){
          if(gebi('filter_scrolling').value!=0){
            isms_change_to_sibling(3,'nav_asset.php?scrollfilter='+piId+':'+gebi('filter_scrolling').value);
          }else{
            isms_change_to_sibling(3,'nav_asset.php?scrollfilter='+piId);
          }
        }
        
        function go_to_nav_area(piId){
          gebi('selected_process_area').value = piId;
          trigger_event('redirect_to_process_area',3);
        }

        function go_to_nav_process() {
        	parent.soTabSubManager.changeTab(TAB_PROCESS,'nav_process.php');
        }

        function go_to_nav_impact(piSceneId){
	      if(gebi('scene_process_id').value!=0){
	      	isms_change_to_sibling(TAB_PROCESS,'nav_process_impact.php?scene_id='+piSceneId+':'+gebi('scene_process_id').value);
	      }else{
	      	isms_change_to_sibling(TAB_PROCESS,'nav_process_impact.php?scene_id='+piSceneId);
	      }
        }

        function editScene(id){
        	isms_open_popup('popup_scene_edit','packages/continuity/popup_scene_edit.php?scene='+id,'','true');
        }

        function enterPlace(place_id) {          
        	isms_change_to_sibling(TAB_PLACE, 'nav_places.php?root='+place_id);
        }        
        
      </script>
		<?

	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_process_scene.xml");
?>