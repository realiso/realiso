<?php

include_once 'include.php';
include_once $handlers_ref . "QueryAssetFromPlace.php";
include_once $handlers_ref . "select/QuerySelectAsset.php";


class SaveEvent extends FWDRunnable {
	public function run(){
		$place = FWDWebLib::getObject('placeId')->getValue();

		$msNewAssets = FWDWebLib::getObject('assets_ids')->getValue();
		if($msNewAssets){
			$maNewAssets = explode(':',$msNewAssets);
		}else{
			$maNewAssets = array();
		}

		$moQuery = new QueryAssetFromPlace(FWDWebLib::getConnection());
		$moQuery->setPlaceId($place);
		$moQuery->makeQuery();
		$moQuery->executeQuery();
		$maOldAssets = $moQuery->getAssets();

		$maToInsert = array_diff($maNewAssets,$maOldAssets);
		$maToDelete = array_diff($maOldAssets,$maNewAssets);
		$placeAsset = new CMPlaceAsset();
		if(count($maToInsert)) $placeAsset->insertAssets($place,$maToInsert);
		if(count($maToDelete)) $placeAsset->deleteAssets($place,$maToDelete);

		echo "soWindow = soPopUpManager.getPopUpById('popup_place_asset_edit').getOpener();"
		."if(soWindow.refresh_grid) soWindow.refresh_grid();"
		."soPopUpManager.closePopUp('popup_place_asset_edit');";
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(''));
		$moStartEvent->addAjaxEvent(new SaveEvent('SaveEvent'));
		$moStartEvent->addAjaxEvent(new RefreshAssetEvent('refresh_assets'));
	}
}

class RefreshAssetEvent extends FWDRunnable {
	public function run(){
		$moHandler = new QuerySelectAsset(FWDWebLib::getConnection());
		$assetIds = FWDWebLib::getObject('assets_ids')->getValue();
		if($assetIds){
			$moHandler->setAssetIds($assetIds);

			$moSelect = FWDWebLib::getObject('assets');
			$moSelect->setQueryHandler($moHandler);
			$moSelect->populate();
			$moSelect->execEventPopulate();
		}
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$moWebLib = FWDWebLib::getInstance();
		$moWindowTitle = $moWebLib->getObject('window_title');
			
		$place = $moWebLib->getObject('place')->getValue();
		if($place){
			$moWebLib->getObject('placeId')->setValue($place);
		}

		$moWindowTitle->setValue(FWDLanguage::getPHPStringValue('associateAssets', 'Associar Ativos'));

		$moAssetSelect = FWDWebLib::getObject('assets');
		if($place) {
			$moQuery = new QueryAssetFromPlace(FWDWebLib::getConnection());
			$moQuery->setPlaceId($place);
			$moQuery->makeQuery();
			$moQuery->executeQuery();

			$moAssets = array();
			
			while($moQuery->fetch()) {
				$moAssetSelect->setItemValue($moQuery->getFieldValue('id'), $moQuery->getFieldValue('name'));
				
				$moAssets[] = $moQuery->getFieldValue('id');
			}
			FWDWebLib::getObject('assets_ids')->setValue(implode(':',$moAssets));
		}
		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
<script language="javascript">
			function set_asset(assetIds) {
			  	gebi('assets_ids').value = assetIds;
			  	assetIds=null;
			  	trigger_event('refresh_assets', 3);
			}
		</script>
		<?
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_place_asset_edit.xml');
?>