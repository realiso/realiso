<?php
include_once "include.php";
include_once $handlers_ref . "grid/QueryGridImpact.php";
include_once $handlers_ref . "select/QuerySelectArea.php";
include_once $handlers_ref . "select/QuerySelectProcessPriority.php";
include_once $handlers_ref . "select/QuerySelectProcessType.php";
include_once $handlers_ref . "QueryDamagesFromImpact.php";

class SearchEvent extends FWDRunnable {
	public function run(){
		$moGrid = FWDWebLib::getObject('grid_impact');
		$moGrid->execEventPopulate();
	}
}

class GridImpact extends FWDDrawGrid {
	protected $sceneId = 0;

	private $damages;

	private function getDamageColor($value){

		$colors[0] = '';
		$colors[1] = 'green';
		$colors[2] = 'yellow';
		$colors[3] = 'orange';
		$colors[4] = 'red';
		$colors[5] = 'purple';

		if($value > count($colors) - 1){
			return $colors[0];
		} else {
			return $colors[$value];
		}

	}

	private function createDamageColumnValue($id){
		$columnName = FWDWebLib::getObject("lb_interval_impact_{$id}")->getValue();

		$damageValue = $this->damages["{$columnName}"];
		$color = $this->getDamageColor($damageValue);

		return "<div style=background-color:{$color};height:18px;text-align:center;line-height:17px;font-weight:bold;>" . $damageValue . "</div>";
	}

	public function setScene($id){
		$this->sceneId = $id;
	}

	public function drawItem(){

		$queryDamagesFromImpact = new QueryDamagesFromImpact();

		$this->damages = 0;
		unset($this->damages); 

		$this->damages = $queryDamagesFromImpact->getDamages( $this->getFieldValue('impact_id') );

		switch($this->ciColumnIndex){
			case $this->getIndexByAlias('impact_id'):
				continue;

			case $this->getIndexByAlias('impact_name'):

				$miProcessValue = $this->getFieldValue('impact_name');

				$this->coCellBox->setAttrStringNoEscape("true");
				$this->coCellBox->setValue("<a href='javascript:isms_open_popup(\"popup_scene_impact_edit\",\"packages/continuity/popup_scene_impact_edit.php?scene={$this->sceneId}
				&impact={$this->getFieldValue('impact_id')}\",\"\",\"true\");'><font color='#0000ff'>{$miProcessValue}</font></a>");

				return $this->coCellBox->draw();
				break;

			case $this->getIndexByAlias('interval_impact_0'):
				$this->coCellBox->setValue( $this->createDamageColumnValue(0) );
				return $this->coCellBox->draw();
				break;

			case $this->getIndexByAlias('interval_impact_1'):
				$this->coCellBox->setValue( $this->createDamageColumnValue(1) );
				return $this->coCellBox->draw();
				break;

			case $this->getIndexByAlias('interval_impact_2'):
				$this->coCellBox->setValue( $this->createDamageColumnValue(2) );
				return $this->coCellBox->draw();
				break;

			case $this->getIndexByAlias('interval_impact_3'):
				$this->coCellBox->setValue( $this->createDamageColumnValue(3) );
				return $this->coCellBox->draw();
				break;

			case $this->getIndexByAlias('interval_impact_4'):
				$this->coCellBox->setValue( $this->createDamageColumnValue(4) );
				return $this->coCellBox->draw();
				break;

			case $this->getIndexByAlias('interval_impact_5'):
				$this->coCellBox->setValue( $this->createDamageColumnValue(5) );
				return $this->coCellBox->draw();
				break;

			case $this->getIndexByAlias('interval_impact_6'):
				$this->coCellBox->setValue( $this->createDamageColumnValue(6) );
				return $this->coCellBox->draw();
				break;

			default:
				return parent::drawItem();
				break;
		}
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));
		$moStartEvent->addAjaxEvent(new SearchEvent('search'));
		FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
		FWDMenuACLSecurity::getInstance()->installMenuACL(FWDWebLib::getObject('menu'),FWDACLSecurity::getInstance());

		$moGrid = FWDWebLib::getObject("grid_impact");
		$moHandler = new QueryGridImpact(FWDWebLib::getConnection());

		if(!ISMSLib::userHasACL('M.RM.2.1')){
			$moHandler->setUserId(ISMSLib::getCurrentUserId());
		}

		$miScene = explode(":", FWDWebLib::getObject('par_scene_id')->getValue());
		$miSceneId = $miScene[0];

		if($miSceneId){
			$moHandler->setScene($miSceneId);
		}
		
		$moGrid->setQueryHandler($moHandler);
		
		$g = new GridImpact();
		$g->setScene($miSceneId);
		$moGrid->setObjFwdDrawGrid($g);

		/* constroi colunas de intervalos */
		$cmDamageMatrix = new CMDamageMatrix();
		$damageMatrix = $cmDamageMatrix->getDamageMatrix();

		$interval_count = 0;
		foreach ($damageMatrix as $key => $value){

			if($value != ''){
				FWDWebLib::getObject('lb_interval_impact_' . $interval_count)->setValue($value);
				FWDWebLib::getObject('interval_impact_' . $interval_count)->setAttrDisplay('true');
			}

			$interval_count++;
		}

	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$miSceneId = FWDWebLib::getObject('par_scene_id')->getValue();
		FWDWebLib::getObject('scene_id')->setValue($miSceneId);

		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));

		if($miSceneId){
			ISMSLib::getPathScrollCode(RISK_MANAGEMENT_MODE, CONTEXT_CM_SCENE, $miSceneId, "Impactos", false, '');
		}

		?>
<script language="javascript">
      
        function refresh_grid() {
          js_refresh_grid('grid_impact');
        }
        
        function remove_impact() {
          trigger_event('remove_impact_event', 3);
          refresh_grid();
        }

        function go_to_nav_scenes(piId) {
        	parent.soTabSubManager.changeTab(TAB_PROCESS,'nav_process_scene.php?scene_process_id='+piId);
        }

        function go_to_nav_process() {
        	parent.soTabSubManager.changeTab(TAB_PROCESS,'nav_process.php');
        }

        function enterPlace(place_id) {          
        	isms_change_to_sibling(TAB_PLACE, 'nav_places.php?root='+place_id);
        }        
        
      </script>
		<?
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_process_impact.xml");
?>