<?php

include_once 'include.php';
include_once $handlers_ref . "select/QuerySelectReadableDocumentByContext.php";
include_once $handlers_ref . "select/QuerySelectPriority.php";
include_once $handlers_ref . "select/QueryGridGroupSearch.php";

function saveActivityFromFields($piContext){
	$miProcessId = FWDWebLib::getObject('process_id')->getValue();
	$moProcessActivity = new CMProcessActivity();

	$msActivityName = FWDWebLib::getObject('activity_name')->getValue();
	$msActivityDescription = FWDWebLib::getObject('activity_description')->getValue();
	$importance = FWDWebLib::getObject('select_importance')->getValue();
	$group = FWDWebLib::getObject('groupId')->getValue();

	$moProcessActivity->setFieldValue('activity_name',$msActivityName);
	$moProcessActivity->setFieldValue('activity_description', $msActivityDescription);
	$moProcessActivity->setFieldValue('activity_process', $miProcessId);
	if($importance){
		$moProcessActivity->setFieldValue('importance', $importance);
	}

	$moProcessActivity->setFieldValue('group', $group );

	if($piContext) {
		$moProcessActivity->update($piContext,true,true);
	} else {
		$piContext = $moProcessActivity->insert(true);
	}

	return $moProcessActivity;
}


class SaveActivityEvent extends FWDRunnable {
	public function run(){
		$miActivityId = FWDWebLib::getObject('activity_id')->getValue();
		saveActivityFromFields($miActivityId);
		echo "soWindow = soPopUpManager.getPopUpById('popup_activity_edit').getOpener();"
		."if(soWindow.refresh_grid) soWindow.refresh_grid();"
		."soPopUpManager.closePopUp('popup_activity_edit');";

	}
}


class DocumentViewEvent extends FWDRunnable {
	public function run() {
		$moWebLib = FWDWebLib::getInstance();
		$miDocumentId = $moWebLib->getObject('select_area_document')->getValue();
		$miUserId = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();

		$moDocument = new PMDocument();
		$moDocument->testPermissionToRead($miDocumentId);
		$moDocument->fetchById($miDocumentId);
		$moPMDocInstance = $moDocument->getPublishedInstance();
		$miDocInstanceId = $moPMDocInstance->getFieldValue('doc_instance_id');

		$moReader = new PMDocumentReader();
		$moReader->createFilter($miDocumentId,'document_id');
		$moReader->createFilter($miUserId,'user_id');
		$moReader->select();
		if ($moReader->fetch()) {
			$moReader->setFieldValue('user_has_read_document',1);
			$moReader->update();
		}

		$moPMDocReadHistory = new PMDocReadHistory();
		$moPMDocReadHistory->setFieldValue('user_id',$miUserId);
		$moPMDocReadHistory->setFieldValue('document_id',$miDocInstanceId);
		$moPMDocReadHistory->setFieldValue('read_date',ISMSLib::ISMSTime());
		$moPMDocReadHistory->insert();

		$mbIsLink = (bool)$moPMDocInstance->getFieldValue('doc_instance_is_link');
		if ($mbIsLink) {
			$msURL = $moPMDocInstance->getFieldValue('doc_instance_link');
			if ($msURL)
			echo "js_open('{$msURL}','document_link',740,502,'center_screen');";
		} else {
			echo "js_submit('download_file','ajax');";
		}
	}
}

class DownloadFileEvent extends FWDRunnable {
	public function run() {
		$moWebLib = FWDWebLib::getInstance();
		$miDocumentId = $moWebLib->getObject('select_area_document')->getValue();
		$miUserId = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();

		$moDocument = new PMDocument();
		$moDocument->testPermissionToRead($miDocumentId);
		$moDocument->fetchById($miDocumentId);
		$moPMDocInstance = $moDocument->getPublishedInstance();

		$msPath = $moWebLib->getSysRef()."packages/policy/".$moPMDocInstance->getFieldValue('doc_instance_path');
		$msFileName = $moPMDocInstance->getFieldValue('doc_instance_file_name');
		header('Cache-Control: ');// leave blank to avoid IE errors
		header('Pragma: ');// leave blank to avoid IE errors
		header('Content-type: application/octet-stream');
		header('Content-Disposition: attachment; filename="'.$msFileName.'"');
		$moCrypt = new FWDCrypt();
		$moFP = fopen($msPath,'rb');
		$moCrypt->setIV(fread($moFP,strlen($moCrypt->getIV())));
		while(!feof($moFP)) {
			echo $moCrypt->decryptNoBase64(fread($moFP,16384));
		}
		fclose($moFP);
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(''));
		$moStartEvent->addAjaxEvent(new SaveActivityEvent('save_activity_event'));
		$moStartEvent->addAjaxEvent(new DocumentViewEvent('document_view_event'));
		$moStartEvent->addSubmitEvent(new DownloadFileEvent('download_file'));
		
		$defaultFinderGroup = new DefaultFinder('search_group');
		$defaultFinderGroup->set('group', new QueryGridGroupSearch(FWDWebLib::getConnection()));
		$moStartEvent->addAjaxEvent($defaultFinderGroup);
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$miActivityId = FWDWebLib::getObject('param_activity_id')->getValue();
		FWDWebLib::getObject('activity_id')->setValue($miActivityId);

		$miProcessId = FWDWebLib::getObject('param_process_id')->getValue();
		FWDWebLib::getObject('process_id')->setValue($miProcessId);

		$moWindowTitle = FWDWebLib::getObject('window_title');
		$moIcon = FWDWebLib::getObject('tooltip_icon');

		$moHandler = new QuerySelectPriority(FWDWebLib::getConnection());
		$moSelect = FWDWebLib::getObject('select_importance');
		$moSelect->setQueryHandler($moHandler);
		$moSelect->populate();

		if($miActivityId){
			$moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_activity_editing', 'Edi��o de Atividade'));

			$moProcessActivity = new CMProcessActivity();

			$moProcessActivity->fetchById($miActivityId);
			FWDWebLib::getObject('activity_name')->setValue($moProcessActivity->getFieldValue('activity_name'));

			//teste para verificar se o sistema n�o est� sendo hakeado
			FWDWebLib::getObject('activity_description')->setValue($moProcessActivity->getFieldValue('activity_description'));

			FWDWebLib::getObject('select_importance')->checkItem($moProcessActivity->getFieldValue('importance'));
			if($moProcessActivity->getFieldValue('group')){
				$group = new CMGroup();
				$group->fetchById($moProcessActivity->getFieldValue('group'));
				FWDWebLib::getObject('group')->setValue($group->getFieldValue('group_name'));
				FWDWebLib::getObject('groupId')->setValue($group->getFieldValue('group_id'));
			}
		}else{
			$moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
			$moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_activity_adding', 'Adi��o de Atividade'));
		}

		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
<script language="javascript">
        gebi('activity_name').focus();
        
        function setGroup(id, name, acronym){
            gebi("groupId").value = id;
            gebi("group").value = name;
        }
      </script>

		<?
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_activity_edit.xml');

?>