<?php
include_once "include.php";
include_once $handlers_ref . "select/QueryGridGroupResourceSearch.php";

class GridSearch extends FWDDrawGrid {
	public function DrawItem(){
		switch($this->ciColumnIndex){
			case $this->getIndexByAlias('group_name'):
				$this->coCellBox->setAttrStringNoEscape("true");
				$this->coCellBox->setValue($this->getNameContent());
				return $this->coCellBox->draw();
				break;
			default:
				return parent::drawItem();
				break;
		}
	}
	public function getNameContent(){
		$type = $this->getFieldValue('type');
		$name = $this->getFieldValue('group_name');
		if($type == 'group'){
			$id = $this->getFieldValue('group_id');
			return "<a href='javascript:enterGroup(".$id.");'><font color='#0000ff'>".$name."</font></a>";
		}else{
			return $name;
		}
	}
}

class SearchControlEvent extends FWDRunnable {
	public function run() {
		$moGrid = FWDWebLib::getObject('grid_search');
		$moGrid->execEventPopulate();
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));
		$moStartEvent->addAjaxEvent(new SearchControlEvent("search_event"));

		$showResources = FWDWebLib::getObject('showResources')->getValue();
		if(substr($showResources,0,1)==':') $showResources = substr($showResources, 1);
		
		$group = FWDWebLib::getObject('groupId')->getValue();
		$moHandler = new QueryGridGroupResourceSearch(FWDWebLib::getConnection());
		$moHandler->setNameFilter(FWDWebLib::getObject("var_group_name")->getValue());
		$moHandler->setShowResourcesFilter($showResources);
		$moHandler->setGroupFilter($group);

		$moGrid = FWDWebLib::getObject("grid_search");
		$moGrid->setQueryHandler($moHandler);
		$moGrid->setObjFwdDrawGrid(new GridSearch());
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
<script language="javascript">

	gebi('group_name').focus();
  
	function refresh_grid() {
		gobi('grid_search').setPopulate(true);
    	js_refresh_grid('grid_search');
	}

  	function enterGroup(id){
  		gebi('var_group_name').value = "";
  		gebi("groupId").value = id;
      	gobi('grid_search').setPopulate(true);
    	trigger_event("search_event","3");
	}

    function enter_group_search_event(e)
	{
		if (!e) e = window.event;
		if(e['keyCode']==13) {
			gebi('var_group_name').value = gebi('group_name').value;
	      	gobi('grid_search').setPopulate(true);
	    	trigger_event("search_event","3");
		}
	}

	function testPopupName(){
		var popupName = 'popup_group_single_search';
		var popup = soPopUpManager.getPopUpById(popupName);
		if(popup == null){
			popupName = 'popup_group_resource_single_search';
			popup = soPopUpManager.getPopUpById(popupName);
			if(popup == null){
				popupName = 'popup_responsible_single_search';
				popup = soPopUpManager.getPopUpById(popupName);
				if(popup == null){
					popupName = 'popup_substitute_single_search';
					popup = soPopUpManager.getPopUpById(popupName);
				}
			}

		}

		return popupName;
	}
	
	FWDEventManager.addEvent(gebi('group_name'), 'keydown', enter_group_search_event);
</script>
		<?
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_group_resource_single_search.xml");
?>