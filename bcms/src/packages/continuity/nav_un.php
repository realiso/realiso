<?php
include_once "include.php";
include_once $handlers_ref . "grid/QueryGridArea.php";
include_once $handlers_ref . "select/QuerySelectPriority.php";
include_once $handlers_ref . "select/QuerySelectAreaType.php";

class GridArea extends FWDDrawGrid {
	public function drawItem(){
		switch($this->ciColumnIndex){
			case $this->getIndexByAlias('subarea_count'):
				$parent = $this->getFieldValue('parent');
				if($parent){
					$maNotAllowed = array("subUN");
					$moACL = FWDACLSecurity::getInstance();
					$moACL->setNotAllowed($maNotAllowed);
					$moMenu = FWDWebLib::getObject('menu');
					$moGrid = FWDWebLib::getObject('grid_area');
					$msLine = $moGrid->getAttrName().'_acl_'.$this->ciRowIndex;
					FWDGridMenuACLSecurity::getInstance()->installGridMenuACL($moMenu,$moACL,$moGrid,$msLine);
				}
				if (!$this->coCellBox->getValue()) $this->coCellBox->setValue(0);
				$this->coCellBox->setAttrStringNoEscape("true");
				if(ISMSLib::userHasACL('M.RM.1.2')){
					$this->coCellBox->setValue("<a href='javascript:enter_area(".$this->getFieldValue('area_id').");'>".$this->coCellBox->getValue()."</a>");
				}
				return $this->coCellBox->draw();
				break;

			case $this->getIndexByAlias('area_process_count'):
				if (!$this->coCellBox->getValue()) $this->coCellBox->setValue(0);
				$this->coCellBox->setAttrStringNoEscape("true");
				if(ISMSLib::userHasACL('M.RM.1.3') && ISMSLib::userHasACL('M.RM.2') ){
					$this->coCellBox->setValue("<a href='javascript:go_to_nav_process(".$this->getFieldValue('area_id').");'>".$this->coCellBox->getValue()."</a>");
				}
				return $this->coCellBox->draw();
				break;

			case $this->getIndexByAlias('area_id'):
				break;
			case $this->getIndexByAlias('parent'):

				break;
			default:
				return parent::drawItem();
				break;
		}
	}
}

class AreaConfirmRemove extends FWDRunnable {
	public function run(){
		$msTitle = FWDLanguage::getPHPStringValue('tt_remove_area','Remover Unidade de Neg�cio');
		$msMessage = FWDLanguage::getPHPStringValue('st_area_remove_confirm',"Voc� tem certeza de que deseja remover a unidade de neg�cio <b>%area_name%</b>?");

		$moArea = new RMArea();
		$moArea->fetchById(FWDWebLib::getObject('selected_area_id')->getValue());
		$msAreaName = ISMSLib::truncateString($moArea->getFieldValue('area_name'), 70);
		$msMessage = str_replace("%area_name%",$msAreaName,$msMessage);

		$msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
                    "soWindow = soPopUp.getOpener();" .
                    "soWindow.remove_area();";

		ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,50);
	}
}

class RemoveAreaEvent extends FWDRunnable {
	public function run(){
		$miAreaId = FWDWebLib::getObject('selected_area_id')->getValue();
		$moArea = new RMArea();
		$moArea->delete($miAreaId);
		/*
		 * Se possuir o m�dulo de documenta��o atualiza os leitores do documento da �rea.
		 */
		if (FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(POLICY_MODE))
		$moArea->updateReaders($miAreaId);
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));
		$moStartEvent->addAjaxEvent(new AreaConfirmRemove('area_confirm_remove'));
		$moStartEvent->addAjaxEvent(new RemoveAreaEvent('remove_area_event'));

		//instala a seguran�a de ACL na p�gina
		FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
		FWDMenuACLSecurity::getInstance()->installMenuACL(FWDWebLib::getObject('menu'),FWDACLSecurity::getInstance());
		
		$placeId = FWDWebLib::getObject("place")->getValue();
		if($placeId)
			FWDWebLib::getObject("placeId")->setValue($placeId);

		$moGrid = FWDWebLib::getObject("grid_area");
		$moHandler = new QueryGridArea(FWDWebLib::getConnection());

		$rootId = FWDWebLib::getObject("root")->getValue();
		if($rootId){
			$moHandler->setRootArea($rootId);
			FWDWebLib::getObject("root_area_id")->setValue($rootId);
		}

		if($placeId){
			$moHandler->setPlace($placeId);
		}

		$moGrid->setQueryHandler($moHandler);
		$moGrid->setObjFwdDrawGrid(new GridArea());

		$maDeniedACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();

		if(FWDWebLib::getObject("root_area_id")->getValue()){
			if(!ISMSLib::userHasACL('M.RM.1.7') && !ISMSLib::userHasACL('M.RM.1.6')){
				FWDWebLib::getObject('viewbutton_insert')->setAttrDisplay("false");
			}
		}else{
			if(!ISMSLib::userHasACL('M.RM.1.7')){
				FWDWebLib::getObject('viewbutton_insert')->setAttrDisplay("false");
			}
		}
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		
		?>

<script language="javascript">

    function refresh_grid() {
    	js_refresh_grid('grid_area');    
    }

    function goToPlans(area){
		if(gebi('placeId').value!=0){
			isms_change_to_sibling(TAB_PLAN,'nav_plan.php?area='+area+':'+gebi('placeId').value);
		}else{
			isms_change_to_sibling(TAB_PLAN,'nav_plan.php?area='+area);
		}
  	}

    function remove_area() {
      trigger_event('remove_area_event', 3);
      refresh_grid();        
    }
		
    function goToResourcesContacts(un_id){
		if(gebi('placeId').value!=0){
			isms_change_to_sibling(TAB_RESOURCE,'nav_resources.php?un_id='+un_id+':'+gebi('placeId').value);
		}else{
			isms_change_to_sibling(TAB_RESOURCE,'nav_resources.php?un_id='+un_id);
		}
  	}  	  	
  	
    function enter_area(area_id) {
    	isms_change_to_sibling(TAB_AREA, 'nav_un.php?root='+area_id);
    }

    function goToExternalContacts(unId){
		if(gebi('placeId').value!=0){
			isms_change_to_sibling(TAB_PROVIDER,'nav_providers.php?un_id='+unId+':'+gebi('placeId').value);
		}else{
			isms_change_to_sibling(TAB_PROVIDER,'nav_providers.php?un_id='+unId);
		}
  	}
    
    function go_to_nav_process(piId){
      if(gebi('placeId').value!=0){
      	isms_change_to_sibling(TAB_PROCESS,'nav_process.php?area='+piId+':'+gebi('placeId').value);
      }else{
      	isms_change_to_sibling(TAB_PROCESS,'nav_process.php?area='+piId);
      }
    }

    function associateAreas(){
    	isms_open_popup('popup_area_search','packages/continuity/popup_area_search.php?scope='+gebi('scopeId').value,'','true');
    }
    
    function set_areas(ids){
    }

    function goToScope(){
        isms_change_to_sibling(TAB_SCOPE,'nav_scope.php');
  	}

    function enterArea(){
        isms_change_to_sibling(TAB_AREA,'nav_un.php');
  	}

    function enterPlace(place_id) {          
    	isms_change_to_sibling(TAB_PLACE, 'nav_places.php?root='+place_id);
    }
 
    //if(gebi("scopeId").value != ""){
    //	js_show("associate_scope");
    //	js_hide("viewbutton_insert");
    //}else{
    	js_hide("associate_scope");
    	js_show("viewbutton_insert");
    //}
    </script>
		<?
		
		$placeId = FWDWebLib::getObject("placeId")->getValue();
		$rootAreaId = FWDWebLib::getObject("root_area_id")->getValue();

		if($placeId){
			$unString = FWDLanguage::getPHPStringValue('tt_area_bl','Unidades de Neg�cio');
			ISMSLib::getPathScrollCode(RISK_MANAGEMENT_MODE, CONTEXT_CM_PLACE, $placeId, $unString, false, '');
		}
		else if($rootAreaId){

			ISMSLib::getPathScrollCode(RISK_MANAGEMENT_MODE, CONTEXT_AREA, $rootAreaId, 'Sub-UNs', false, "");

		} else {

			ISMSLib::getPathScrollCode(RISK_MANAGEMENT_MODE, CONTEXT_AREA, 0, '', false, '');
		}
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_un.xml");
?>