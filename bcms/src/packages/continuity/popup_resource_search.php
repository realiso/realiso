<?php
include_once "include.php";
include_once $handlers_ref . "select/QuerySelectDepartment.php";
include_once $handlers_ref . "select/QueryResourceFromGroup.php";
include_once $handlers_ref . "select/QueryPlaceResource.php";
include_once $handlers_ref . "select/QueryGridResourceSearch.php";

class AssociateResourceEvent extends FWDRunnable {
	public function run(){
		$moWebLib = FWDWebLib::getInstance();
		$miGroupId = $moWebLib->getObject('group_id')->getValue();
		$place = $moWebLib->getObject('placeId')->getValue();
		$cmt = $moWebLib->getObject('cmt')->getValue();

		$msNewResources = $moWebLib->getObject('current_resources_ids')->getValue();
		if($msNewResources){
			$maNewResources = explode(':',$msNewResources);
		}else{
			$maNewResources = array();
		}

		if($miGroupId) {
			$moQuery = new QueryResourceFromGroup(FWDWebLib::getConnection());
			$moQuery->setGroupId($miGroupId);
			$moQuery->makeQuery();
			$moQuery->fetchResources();
			$maOldResources = $moQuery->getResources();

			$maToInsert = array_diff($maNewResources,$maOldResources);
			$maToDelete = array_diff($maOldResources,$maNewResources);
			$groupResource = new CMGroupResource();
			if(count($maToInsert)) $groupResource->insertResources($miGroupId,$maToInsert);
			if(count($maToDelete)) $groupResource->deleteResources($miGroupId,$maToDelete);
		}

		if($place) {
			if(!$cmt){
				$moQuery = new QueryPlaceResource(FWDWebLib::getConnection());
				$moQuery->setPlaceId($place);
				$moQuery->setResource(true);
				$moQuery->makeQuery();
				$moQuery->fetchResources();
				$maOldResources = $moQuery->getResources();

				$maToInsert = array_diff($maNewResources,$maOldResources);
				$maToDelete = array_diff($maOldResources,$maNewResources);
				$placeResource = new CMPlaceResource();
				if(count($maToInsert)) $placeResource->insertResources($place, $maToInsert);
				if(count($maToDelete)) $placeResource->deleteResources($place, $maToDelete);

			}else{
				$g = new CMGroup();
				$groupCMT = $g->getCMT($place);
				$groupId = $groupCMT->getFieldValue('group_id');

				//Associa os recursos com o grupo CMT
				$moQuery = new QueryResourceFromGroup(FWDWebLib::getConnection());
				$moQuery->setGroupId($groupId);
				$moQuery->makeQuery();
				$moQuery->fetchResources();
				$maOldResources = $moQuery->getResources();

				$maToInsert = array_diff($maNewResources,$maOldResources);
				$maToDelete = array_diff($maOldResources,$maNewResources);
				$moResourceGroup = new CMGroupResource();
				if(count($maToInsert)) $moResourceGroup->insertResources($groupId,$maToInsert);
				if(count($maToDelete)) $moResourceGroup->deleteResources($groupId,$maToDelete);
				$moResourceGroup->orderResources($maNewResources,$groupId);

			}
		}

		echo "soWindow = soPopUpManager.getPopUpById('popup_resource_search').getOpener();"
		."if (soWindow.refresh_grid)"
		." soWindow.refresh_grid();"
		."if(soWindow.set_resources) soWindow.set_resources(gebi('current_resources_ids').value);"
		."soPopUpManager.closePopUp('popup_resource_search');";
	}
}

class SearchResourceEvent extends FWDRunnable {
	public function run(){
		$moGrid = FWDWebLib::getObject('grid_resource_search');
		$moGrid->execEventPopulate();
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));
		$moStartEvent->addAjaxEvent(new SearchResourceEvent('search_resource_event'));
		$moStartEvent->addAjaxEvent(new AssociateResourceEvent('associate_resource_event'));

		$moSearchGrid = FWDWebLib::getObject('grid_resource_search');
		$moSearchHandler = new QueryGridResourceSearch(FWDWebLib::getConnection());

		$moCurrentGrid = FWDWebLib::getObject('grid_current_resources');
		$moCurrentHandler = new QueryGridResourceSearch(FWDWebLib::getConnection());

		$moHandler = new QuerySelectDepartment(FWDWebLib::getConnection());
		$moSelect = FWDWebLib::getObject('departments');
		$moSelect->setQueryHandler($moHandler);
		$moSelect->populate();

		$moSearchHandler->setNameFilter(FWDWebLib::getObject('name_filter')->getValue());
		$departmentId = FWDWebLib::getObject('departments')->getValue();
		if($departmentId){
			$moSearchHandler->setDepartment($departmentId);
		}
		$msCurrentIds = FWDWebLib::getObject('current_resources_ids')->getValue();
		$placeId = FWDWebLib::getObject('param_place_id')->getValue();

		$groupId = FWDWebLib::getObject('param_group_id')->getValue();
		FWDWebLib::getObject('placeId')->setValue($placeId);
		$cmt = FWDWebLib::getObject('cmt')->getValue();
		$removed = FWDWebLib::getObject('removed')->getValue();
		if(!$msCurrentIds && $removed == 'false'){
			if($placeId){
				$msCurrentIds = '0';
				$query = new FWDDBDataSet(FWDWebLib::getConnection());

				if($cmt){
					$query->setQuery("select fkresource as id from view_cm_group_resource_active where fkgroup in (select fkgroup from cm_place_resource where cmt = 1 and fkplace = {$placeId} order by escalation)");
				}else{
					$query->setQuery("select fkresource as id from view_cm_place_resource_active where fkresource is not null and fkplace = {$placeId}");
				}

				$query->addFWDDBField(new FWDDBField('id','id'  ,DB_NUMBER));
				$query->execute();
				while($query->fetch()){
					$id = $query->getFieldByAlias("id")->getValue();
					$msCurrentIds .= ":".$id;
				}
				FWDWebLib::getObject('current_resources_ids')->setValue($msCurrentIds);
			}else if($groupId) {
				$msCurrentIds = '0';
				$query = new FWDDBDataSet(FWDWebLib::getConnection());
				$query->setQuery("select fkresource as id from view_cm_group_resource_active where fkresource is not null and fkgroup= {$groupId}");
				$query->addFWDDBField(new FWDDBField('id','id'  ,DB_NUMBER));
				$query->execute();
				while($query->fetch()){
					$id = $query->getFieldByAlias("id")->getValue();
					$msCurrentIds .= ":".$id;
				}
				FWDWebLib::getObject('current_resources_ids')->setValue($msCurrentIds);
			}
		}

		if($msCurrentIds){
			$maCurrentIds = explode(':',$msCurrentIds);
			$moSearchHandler->setExcludedIds($maCurrentIds);
			$moCurrentHandler->setIds($maCurrentIds);
		}
		else {
			$moCurrentHandler->setEmpty(true);
		}

		$moSearchGrid->setQueryHandler($moSearchHandler);
		$moCurrentGrid->setQueryHandler($moCurrentHandler);
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$paramResourcesIds = FWDWebLib::getObject('param_resources_ids')->getValue();
		if($paramResourcesIds) {
	 	FWDWebLib::getObject('current_resources_ids')->setValue($paramResourcesIds);

		}
		$miGroupId = FWDWebLib::getObject('param_group_id')->getValue();
		FWDWebLib::getObject('group_id')->setValue($miGroupId);

		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
<script language="javascript">
  gebi('resource_name').focus();
  function refresh_grid() {    
    gobi('grid_current_resources').setPopulate(true);
    gobi('grid_current_resources').refresh();
  }
  function enter_resource_search_event(e)
	{
		if (!e) e = window.event;
		if(e['keyCode']==13) {
      gebi('name_filter').value = gebi('resource_name').value;
      gebi('current_resources_ids').value = gobi('grid_current_resources').getAllIds().join(':');
      gobi('grid_resources_search').setPopulate(true);
	    trigger_event("search_resource_event","3");
		}
	}
	FWDEventManager.addEvent(gebi('resource_name'), 'keydown', enter_resource_search_event);
  refresh_grid();
</script>
		<?
	}
}
FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load('popup_resource_search.xml');

?>