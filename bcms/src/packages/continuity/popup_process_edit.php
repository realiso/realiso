<?php

include_once 'include.php';
include_once $handlers_ref . "QueryContextHierarchicalStructure.php";
include_once $handlers_ref . "select/QuerySelectProcessType.php";
include_once $handlers_ref . "select/QuerySelectPriority.php";
include_once $handlers_ref . "select/QuerySelectReadableDocumentByContext.php";
include_once $handlers_ref . "select/QueryGridGroupResourceSearch.php";
include_once $handlers_ref . "select/continuity/CMQuerySelectInputProcess.php";
include_once $handlers_ref . "select/continuity/CMQuerySelectOutputProcess.php";

include_once $handlers_ref . "select/continuity/QuerySelectPlace.php";

include_once $handlers_ref . "QueryPlaceFromProcess.php";

function getProcessFromFields(){
	$moProcess = new RMProcess();

	$msProcessName = FWDWebLib::getObject('process_name')->getValue();
	$miArea = FWDWebLib::getObject('select_area')->getValue();
	$miProcessType = FWDWebLib::getObject('select_process_type')->getValue();
	$substitute = FWDWebLib::getObject('substituteId')->getValue();
	$group = FWDWebLib::getObject('groupId')->getValue();
	$miProcessPriority = FWDWebLib::getObject('select_process_priority')->getValue();

	$moProcess->setFieldValue('process_name', $msProcessName);
	$moProcess->setFieldValue('process_area_id', $miArea);

	$moProcess->setGroup($group);
	$moProcess->setSubstitute($substitute);

	$moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());


	if($moSession->attributeExists("processDescription")) {
		$moProcess->setFieldValue('process_description', unserialize($moSession->getAttrProcessDescription()));
	}

	if($moSession->attributeExists("processInput")) {
		$moProcess->setFieldValue('process_input', unserialize($moSession->getAttrProcessInput()));
	}

	if($moSession->attributeExists("processOutput")) {
		$moProcess->setFieldValue('process_output', unserialize($moSession->getAttrProcessOutput()));
	}

	//$moProcess->setFieldValue('process_document', $msProcessDocument);
	if($miProcessType){
		$moProcess->setFieldValue('process_type', $miProcessType);
	}else{
		$moProcess->setFieldValue('process_type', 'null');
	}
	if($miProcessPriority)
	$moProcess->setFieldValue('process_priority', $miProcessPriority);
	else
	$moProcess->setFieldValue('process_priority', 'null');

	$mbHasPMModule = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(POLICY_MODE);
	if (!$mbHasPMModule) {
		$msProcessDocument = FWDWebLib::getObject('process_document')->getValue();
		$moProcess->setFieldValue('process_document', $msProcessDocument);
	}

	$miScheduleId = $moProcess->getFieldValue('seasonality');

	$msSerializedSchedule = FWDWebLib::getObject('var_schedule')->getValue();

	if($msSerializedSchedule){
		$moSchedule = new WKFSchedule();
		$maSchedule = FWDWebLib::unserializeString($msSerializedSchedule);
		unset($maSchedule['schedule_id']);
		$moSchedule->fillFieldsFromArray($maSchedule);
		$miScheduleId = $moSchedule->insertOrUpdate($miScheduleId);
		$moProcess->setFieldValue('seasonality', $miScheduleId);
	}

	return $moProcess;
}

function save($poContext,$pbHasSensitiveChanges,$piContextId=0){
	$miId = $piContextId;
	if($miId){
		$poContext->update($miId,true,$pbHasSensitiveChanges);
	}else{
		$miId = $poContext->insert(true);
	}

	$moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
	if($moSession->attributeExists("processInputIds")) { // se temos IDs de processos de input, vamos relaciona-los
		$moInputs = explode(':',unserialize($moSession->getAttrProcessInputIds()));
		$moProcessInputs = new QuerySelectProcessInputs(FWDWebLib::getConnection());
		$moProcessInputs->setProcess($miId);
		$moProcessInputs->makeQuery();
		$moProcessInputs->executeQuery();
		while($moProcessInputs->fetch()){
			$moInputId = $moProcessInputs->getFieldValue('input_id');
			$moId = $moProcessInputs->getFieldValue('process_input_id');
			if($offset = array_search($moInputId, $moInputs)) { // se o id do banco foi encontrado na lista
				unset($moInputs[$offset]); // remvemos ele da array
			} else { // se nao foi encontrado, significa que precisamos remover
				$moProcessInput = new CMProcessInput();
				$moProcessInput->delete($moId,true);
			}
		}
		if(count($moInputs)) { // sobraram ids pra serem adicionados
			foreach($moInputs as $key=>$value) {
				if($value) {
					$moProcessInput = new CMProcessInput();
					$moProcessInput->setFieldValue('process_id',$miId);
					$moProcessInput->setFieldValue('input_id',$value);
					$moProcessInput->insert();
				}
			}
		}
	}

	if($moSession->attributeExists("processOutputIds")) { // se temos IDs de processos de output, vamos relaciona-los
			
		$moOutputs = explode(':',unserialize($moSession->getAttrProcessOutputIds()));
			
		$moProcessOutputs = new QuerySelectProcessOutputs(FWDWebLib::getConnection());
		$moProcessOutputs->setProcess($miId);
		$moProcessOutputs->makeQuery();
		$moProcessOutputs->executeQuery();
		while($moProcessOutputs->fetch()){
			$moOutputId = $moProcessOutputs->getFieldValue('output_id');
			$moId = $moProcessOutputs->getFieldValue('process_output_id');
			if($offset = array_search($moOutputId, $moOutputs)) { // se o id do banco foi encontrado na lista
				unset($moOutputs[$offset]); // remvemos ele da array
			} else { // se nao foi encontrado, significa que precisamos remover

				$moProcessOutput = new CMProcessOutput();
				$moProcessOutput->delete($moId,true);
			}
		}
		if(count($moOutputs)) { // sobraram ids pra serem adicionados
			foreach($moOutputs as $key=>$value) {
				if($value) {
					$moProcessOutput = new CMProcessOutput();
					$moProcessOutput->setFieldValue('process_id',$miId);
					$moProcessOutput->setFieldValue('output_id',$value);
					$moProcessOutput->insert();
				}
			}
		}
	}

	/*
	 * Se possui o m�dulo de documenta��o, verifica se ocorreu
	 * alguma mudan�a na associa��o dos usu�rios com o processo
	 * e, caso necess�rio, grava as altera��es.
	 *
	 * Atualiza tamb�m os aprovadores autom�ticos dos documentos
	 * associados a esse contexto.
	 */
	$mbHasPMModule = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(POLICY_MODE);
	if ($mbHasPMModule) {
		$moProcessUser = new PMProcessUser();
		$maInitialUserValues = explode(':', FWDWebLib::getObject('initial_users_ids')->getValue());
		$maCurrentUserValues = explode(':', FWDWebLib::getObject('current_users_ids')->getValue());
		unset($maInitialUserValues[0], $maCurrentUserValues[0]);
		$maInsertUser = array_unique(array_diff($maCurrentUserValues,$maInitialUserValues));
		$maDeleteUser = array_unique(array_diff($maInitialUserValues,$maCurrentUserValues));

		if(count($maInsertUser)) $moProcessUser->insertUsers($miId, $maInsertUser);
		if(count($maDeleteUser)) $moProcessUser->deleteUsers($miId, $maDeleteUser);
			
		/*
		 * Atualiza os leitores dos documentos dos elementos associados ao processo
		 * caso ocorra mudan�a nos usu�rios associados ao processo.
		 */
		if (count($maInsertUser) || count($maDeleteUser)) $poContext->updateReaders($miId);
			
		/*
		 * Caso o processo mude de �rea, deve-se atualizar os leitores
		 * dos documentos da �rea antiga e da nova �rea
		 */
		$miOldAreaId = FWDWebLib::getObject('current_area_id')->getValue();
		$miNewAreaId = $poContext->getFieldValue('process_area_id');
		if ($miOldAreaId && ($miOldAreaId != $miNewAreaId)) {
			$moArea = new RMArea();
			$moArea->updateReaders($miOldAreaId);
			$moArea->updateReaders($miNewAreaId);
		}

		$moDocContext = new PMDocContext();
		$moDocContext->createFilter($miId,'context_id');
		$moDocContext->select();
		while($moDocContext->fetch()) {
			$moDocContext->updateAutoApprovers($moDocContext->getFieldValue('document_id'));
		}
	}

	$msNewPlaces = FWDWebLib::getObject('process_places_ids')->getValue();
	if($msNewPlaces){
		$maNewPlaces = explode(':',$msNewPlaces);
	}else{
		$maNewPlaces = array();
	}

	$moQuery = new QueryPlaceFromProcess(FWDWebLib::getConnection());
	$moQuery->setProcessId($miId);
	$moQuery->makeQuery();
	$moQuery->fetchPlaces();
	$maOldPlaces = $moQuery->getPlaces();

	$maToInsert = array_diff($maNewPlaces,$maOldPlaces);
	$maToDelete = array_diff($maOldPlaces,$maNewPlaces);
	$moPlaceProcess = new CMPlaceProcess();
	if(count($maToInsert)) $moPlaceProcess->insertPlaces($miId,$maToInsert);
	if(count($maToDelete)) $moPlaceProcess->deletePlaces($miId,$maToDelete);

	echo "soWindow = soPopUpManager.getPopUpById('popup_process_edit').getOpener();"
	."if(soWindow.refresh_grid) soWindow.refresh_grid();"
	."soPopUpManager.closePopUp('popup_process_edit');";
}

class ConfirmEditEvent extends FWDRunnable {
	public function run(){
		$miProcessId = FWDWebLib::getObject('process_id')->getValue();
		$moProcess = getProcessFromFields();

	}
}

class SaveProcessEvent extends FWDRunnable {
	public function run(){
		$miProcessId = FWDWebLib::getObject('process_id')->getValue();
		$moProcess = getProcessFromFields();
		$moProcess->setHash(FWDWebLib::getObject('hash')->getValue());
		$mbHasSensitiveChanges = $moProcess->hasSensitiveChanges();
		$miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();

		if($moProcess->getSubstitute() && $moProcess->getGroup() && $moProcess->getSubstitute() != "null" && $moProcess->getGroup() != "null" && $moProcess->getSubstitute() == $moProcess->getGroup()){
			echo "gobi('warning_substitute_asset').show();";
		} else {
			save($moProcess,$mbHasSensitiveChanges,$miProcessId);
		}
	}
}

class DocumentViewEvent extends FWDRunnable {
	public function run() {
		$moWebLib = FWDWebLib::getInstance();
		$miDocumentId = $moWebLib->getObject('select_process_document')->getValue();
		$miUserId = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();

		$moDocument = new PMDocument();
		$moDocument->testPermissionToRead($miDocumentId);
		$moDocument->fetchById($miDocumentId);
		$moPMDocInstance = $moDocument->getPublishedInstance();
		$miDocInstanceId = $moPMDocInstance->getFieldValue('doc_instance_id');

		$moReader = new PMDocumentReader();
		$moReader->createFilter($miDocumentId,'document_id');
		$moReader->createFilter($miUserId,'user_id');
		$moReader->select();
		if ($moReader->fetch()) {
			$moReader->setFieldValue('user_has_read_document',1);
			$moReader->update();
		}

		$moPMDocReadHistory = new PMDocReadHistory();
		$moPMDocReadHistory->setFieldValue('user_id',$miUserId);
		$moPMDocReadHistory->setFieldValue('document_id',$miDocInstanceId);
		$moPMDocReadHistory->setFieldValue('read_date',ISMSLib::ISMSTime());
		$moPMDocReadHistory->insert();

		$mbIsLink = (bool)$moPMDocInstance->getFieldValue('doc_instance_is_link');
		if ($mbIsLink) {
			$msURL = $moPMDocInstance->getFieldValue('doc_instance_link');
			if ($msURL)
			echo "js_open('{$msURL}','document_link',740,502,'center_screen');";
		} else {
			echo "js_submit('download_file','ajax');";
		}
	}
}

class DownloadFileEvent extends FWDRunnable {
	public function run() {
		$moWebLib = FWDWebLib::getInstance();
		$miDocumentId = $moWebLib->getObject('select_process_document')->getValue();
		$miUserId = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();

		$moDocument = new PMDocument();
		$moDocument->testPermissionToRead($miDocumentId);
		$moDocument->fetchById($miDocumentId);
		$moPMDocInstance = $moDocument->getPublishedInstance();

		$msPath = $moWebLib->getSysRef()."packages/policy/".$moPMDocInstance->getFieldValue('doc_instance_path');
		$msFileName = $moPMDocInstance->getFieldValue('doc_instance_file_name');
		header('Cache-Control: ');// leave blank to avoid IE errors
		header('Pragma: ');// leave blank to avoid IE errors
		header('Content-type: application/octet-stream');
		header('Content-Disposition: attachment; filename="'.$msFileName.'"');
		$moCrypt = new FWDCrypt();
		$moFP = fopen($msPath,'rb');
		$moCrypt->setIV(fread($moFP,strlen($moCrypt->getIV())));
		while(!feof($moFP)) {
			echo $moCrypt->decryptNoBase64(fread($moFP,16384));
		}
		fclose($moFP);
	}
}

class RefreshPlaceEvent extends FWDRunnable {
	public function run(){
		$moHandler = new QuerySelectPlace(FWDWebLib::getConnection());
		$placeIds = FWDWebLib::getObject('process_places_ids')->getValue();
		if($placeIds){
			$moHandler->setPlaceIds($placeIds);
		}else{
			$moHandler->setPlaceIds("1:1");
		}
		$moSelect = FWDWebLib::getObject('nc_places');
		$moSelect->setQueryHandler($moHandler);
		$moSelect->populate();
		$moSelect->execEventPopulate();
	}
}


class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(''));
		$moStartEvent->addAjaxEvent(new SaveProcessEvent('save_process_event'));
		//		$moStartEvent->addAjaxEvent(new SearchGroup('search_group'));
		//		$moStartEvent->addAjaxEvent(new SearchSubstitute('search_substitute'));
		$moStartEvent->addAjaxEvent(new ConfirmEditEvent('confirm_edit'));
		$moStartEvent->addAjaxEvent(new DocumentViewEvent('document_view_event'));
		$moStartEvent->addAjaxEvent(new RefreshPlaceEvent('refresh_places'));
		$moStartEvent->addSubmitEvent(new DownloadFileEvent('download_file'));

		$defaultFinderGroup = new DefaultFinder('search_group');
		$defaultFinderGroup->set('group', new QueryGridGroupResourceSearch(FWDWebLib::getConnection()));
		$moStartEvent->addAjaxEvent($defaultFinderGroup);

		$defaultFinderSubstitute = new DefaultFinder('search_substitute');
		$defaultFinderSubstitute->set('substitute', new QueryGridGroupResourceSearch(FWDWebLib::getConnection()));
		$moStartEvent->addAjaxEvent($defaultFinderSubstitute);

		$moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
		/*
		 * Verifica se possui o m�dulo de documenta��o para mostrar o bot�o de associar usu�rios.
		 */
		$mbHasPMModule = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(POLICY_MODE);
		if (!$mbHasPMModule){
			FWDWebLib::getObject('btn_associate_users')->setShouldDraw(false);
		}
		$moSelect = FWDWebLib::getObject('select_area');
		$moQuery = new QueryContextHierarchicalStructure(FWDWebLib::getConnection());
		$moQuery->setContextType(CONTEXT_AREA);
		$moQuery->makeQuery();
		$moQuery->executeQuery();
		foreach ($moQuery->getContexts() as $maContext) {
			list($miContextId,$msHierarchicalName) = $maContext;
			$moSelect->setItemValue($miContextId,$msHierarchicalName);
		}

		$miProcessId = FWDWebLib::getObject('param_process_id')->getValue();
		FWDWebLib::getObject('process_id')->setValue($miProcessId);
		$miAreaId = FWDWebLib::getObject('area_id')->getValue();
		$moWindowTitle = FWDWebLib::getObject('window_title');
		$moIcon  = FWDWebLib::getObject('tooltip_icon');

		$moPlaceSelect = FWDWebLib::getObject('nc_places');

		if($miProcessId) {
			$moQuery = new QueryPlaceFromProcess(FWDWebLib::getConnection());
			$moQuery->setProcessId($miProcessId);
			$moQuery->makeQuery();
			$moQuery->executeQuery();
			$moPlaces = array();
			while($moQuery->fetch()) {
				$moPlaceSelect->setItemValue($moQuery->getFieldValue('place_id'), $moQuery->getFieldValue('place_name'));
				$moPlaces[] = $moQuery->getFieldValue('place_id');
			}
			FWDWebLib::getObject('process_places_ids')->setValue(implode(':',$moPlaces));
		}

		if($moSession->attributeExists("processDescription")) {
			$moSession->deleteAttribute("processDescription");
		}

		if($moSession->attributeExists("processOutput")) {
			$moSession->deleteAttribute("processOutput");
		}

		if($moSession->attributeExists("processInput")) {
			$moSession->deleteAttribute("processInput");
		}

		if($moSession->attributeExists("processInputIds")) {
			$moSession->deleteAttribute("processInputIds");
		}

		if($moSession->attributeExists("processOutputIds")) {
			$moSession->deleteAttribute("processOutputIds");
		}

		$moHandler = new QuerySelectProcessType(FWDWebLib::getConnection());
		$moSelect = FWDWebLib::getObject('select_process_type');
		$moSelect->setQueryHandler($moHandler);
		$moSelect->populate();

		$moHandler = new QuerySelectPriority(FWDWebLib::getConnection());
		$moSelect = FWDWebLib::getObject('select_process_priority');
		$moSelect->setQueryHandler($moHandler);
		$moSelect->populate();

		if($miProcessId){
			$moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_process_editing', 'Edi��o de Processo'));

			$moProcess = new RMProcess();
			$moProcess->fetchById($miProcessId);

			FWDWebLib::getObject('substituteId')->setValue($moProcess->getSubstitute());
			FWDWebLib::getObject('substitute')->setValue($moProcess->getSubstituteName());

			FWDWebLib::getObject('groupId')->setValue($moProcess->getGroup());
			FWDWebLib::getObject('group')->setValue($moProcess->getGroupName());

			FWDWebLib::getObject('process_name')->setValue($moProcess->getFieldValue('process_name'));
			FWDWebLib::getObject('select_area')->checkItem($moProcess->getFieldValue('process_area_id'));
			FWDWebLib::getObject('select_process_type')->checkItem($moProcess->getFieldValue('process_type'));
			FWDWebLib::getObject('select_process_priority')->checkItem($moProcess->getFieldValue('process_priority'));
			FWDWebLib::getObject('hash')->setValue($moProcess->getHash());

			if(!$moSession->attributeExists("processDescription")) {
				$moSession->addAttribute("processDescription");
			}
			$moSession->setAttrProcessDescription(serialize($moProcess->getFieldValue('process_description')));

			if(!$moSession->attributeExists("processOutput")) {
				$moSession->addAttribute("processOutput");
			}
			$moSession->setAttrProcessOutput(serialize($moProcess->getFieldValue('process_output')));

			if(!$moSession->attributeExists("processInput")) {
				$moSession->addAttribute("processInput");
			}
			$moSession->setAttrProcessInput(serialize($moProcess->getFieldValue('process_input')));

			$moProcessInputs = new QuerySelectProcessInputs(FWDWebLib::getConnection());
			$moProcessInputs->setProcess($miProcessId);
			$moProcessInputs->makeQuery();
			$moProcessInputs->executeQuery();
			$miProcessInputIds = "";
			while($moProcessInputs->fetch()){
				$miProcessInputIds .= $moProcessInputs->getFieldValue('input_id') . ":";
			}

			if(!$moSession->attributeExists("processInputIds")) {
				$moSession->addAttribute("processInputIds");
			}
			$moSession->setAttrProcessInputIds(serialize($miProcessInputIds));

			$moProcessOutputs = new QuerySelectProcessOutputs(FWDWebLib::getConnection());
			$moProcessOutputs->setProcess($miProcessId);
			$moProcessOutputs->makeQuery();
			$moProcessOutputs->executeQuery();
			$miProcessOutputIds = "";
			while($moProcessOutputs->fetch()){
				$miProcessOutputIds .= $moProcessOutputs->getFieldValue('output_id') . ":";
			}

			if(!$moSession->attributeExists("processOutputIds")) {
				$moSession->addAttribute("processOutputIds");
			}
			$moSession->setAttrProcessOutputIds(serialize($miProcessOutputIds));

			$miScheduleId = $moProcess->getFieldValue('seasonality');
			if($miScheduleId){
				$moSchedule = new WKFSchedule();
				$moSchedule->fetchById($miScheduleId);
				$msSerializedSchedule = serialize($moSchedule->getFieldValues());
				FWDWebLib::getObject('var_schedule')->setValue($msSerializedSchedule);
			}else{
				FWDWebLib::getObject('seasonalityIcon')->setAttrSrc(FWDWebLib::getInstance()->getGfxRef()."icon-risk_red.png");
			}

			/*
			 * Se possui o m�dulo de documenta��o, armazena os ids dos
			 * usu�rios associados ao processo e da �rea atual.
			 */
			if ($mbHasPMModule) {
				$moProcessUser = new PMProcessUser();
				$moProcessUser->createFilter($miProcessId, 'process_id');
				$moProcessUser->select();
				$msUserIds = '';
				while($moProcessUser->fetch()) $msUserIds .= ':' . $moProcessUser->getFieldValue('user_id');
				FWDWebLib::getObject('initial_users_ids')->setValue($msUserIds);
				FWDWebLib::getObject('current_users_ids')->setValue($msUserIds);
				FWDWebLib::getObject('current_area_id')->setValue($moProcess->getFieldValue('process_area_id'));
			}
		}else{
			if($miAreaId) FWDWebLib::getObject('select_area')->checkItem($miAreaId);

			$moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_process_adding', 'Adi��o de Processo'));
			FWDWebLib::getObject('seasonalityIcon')->setAttrSrc(FWDWebLib::getInstance()->getGfxRef()."icon-risk_red.png");
		}

		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
<script language="javascript">
        gebi('process_name').focus();
      
 	   function set_places(psPlacesIds) {
          	gebi('process_places_ids').value = psPlacesIds;	
          	psPlacesIds=null;
          	trigger_event('refresh_places',3);
        }

		var subs = false;
 	   	function setGroup(id, name, acronym){
 	 		if(subs){
 	 			gebi("substituteId").value = id;
 	         	gebi("substitute").value = name;
 	 		}else{
 	 			gebi("groupId").value = id;
 	         	gebi("group").value = name;
 	 		}
      	}

      	function setSubstitute(sub){
      		subs = sub;
      	}

      	function set_schedule(psSerializedSchedule){
			gebi('var_schedule').value = psSerializedSchedule;
		}	   
      </script>
		<?
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_process_edit.xml');

?>