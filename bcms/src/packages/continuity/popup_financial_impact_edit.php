<?php
include_once 'include.php';

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(''));
		//    $moStartEvent->addAjaxEvent(new SaveImpactEvent('save_impact_event'));
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$moWebLib = FWDWebLib::getInstance();
		$moWindowTitle = $moWebLib->getObject('window_title');

		$miMatrixId = FWDWebLib::getObject('param_matrix_id')->getValue();

		FWDWebLib::getObject('matrix_id')->setValue($miMatrixId);

		$miImpactId = FWDWebLib::getObject('param_impact_id')->getValue();

		$moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_impact_financial', 'Impacto Financeiro'));

		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
<script language="javascript">
		soWindow = soPopUpManager.getPopUpById('popup_financial_impact_edit').getOpener();
		var text = soWindow.get_financial_impact(gebi('matrix_id').value);
		gebi('financial_impact').value = text;
        gebi('financial_impact').focus();

      </script>
		<?
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_financial_impact_edit.xml');

?>