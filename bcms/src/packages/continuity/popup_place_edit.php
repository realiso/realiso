<?php

include_once 'include.php';
include_once $handlers_ref . "select/QuerySelectReadableDocumentByContext.php";
//include_once $handlers_ref . "select/QueryGridGroupResourceSearch.php";

include_once $handlers_ref . "select/QueryGridPlaceEditResourceSearch.php";
include_once $handlers_ref . "QueryCheckPlaceEditResourceSearch.php";

function save($poContext,$pbHasSensitiveChanges,$piContextId=0){
	$msInsert = "";
	if($piContextId){
		$poContext->update($piContextId,true,$pbHasSensitiveChanges);
	}else{
		$piContextId = $poContext->insert(true);
		$msInsert .=" gebi('place_id').value={$piContextId};";
	}

	$moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
	$moSession->addAttribute("documentContextId");
	$moSession->setAttrDocumentContextId($piContextId);

	echo "$msInsert js_submit('upload','ajax');setTimeout('trigger_event(\"save_complete_event\",3)',200);";
}

function getPlaceFromFields(){
	$moPlace = new CMPlace();

	$msPlaceName = FWDWebLib::getObject('place_name')->getValue();
	$msPlaceDescription = FWDWebLib::getObject('place_description')->getValue();
	
	$substitute = FWDWebLib::getObject('substituteId')->getValue();
	$group = FWDWebLib::getObject('groupId')->getValue();
	$parent = FWDWebLib::getObject('parentId')->getValue();
	
	$address = FWDWebLib::getObject('address_name')->getValue();
	$moPlace->setFieldValue('place_address', $address);

	$moPlace->setGroup($group);
	$moPlace->setSubstitute($substitute);

	if($parent){
		$moPlace->setFieldValue('parent', $parent);
	}else{
		$moPlace->setFieldValue('parent', "null");
	}

	$moPlace->setFieldValue('place_name', $msPlaceName);
	$moPlace->setFieldValue('place_description', $msPlaceDescription);
		
	return $moPlace;
}

class SubmitEvent extends FWDRunnable {
	public function run(){
		$moWebLib = FWDWebLib::getInstance();
		$moSession = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId());

		$moFile = FWDWebLib::getObject('file');
		$miFileError = 0;
		if($moFile->isUploaded()){
			$miFileError = $moFile->getErrorCode();
			if($miFileError==FWDFile::E_NONE){
				// Upload sem erros

				$moCrypt = new FWDCrypt();
				$msDirectory = (ISMSLib::isCompiled() ? $GLOBALS['files_ref'] : 'files/');
				if(is_writable($msDirectory)){
					$msFileName=$moFile->getFileName();
					$msHashFileName = md5($msFileName.uniqid());
					$msPath = "{$msDirectory}{$msHashFileName}.aef";
					$miFileSize = filesize($moFile->getTempFileName());
					$moWriteFP = fopen($msPath,'wb');
					fwrite($moWriteFP,$moCrypt->getIV());
					$moFP = fopen($moFile->getTempFileName(),'rb');
					while(!feof($moFP)){
						fwrite($moWriteFP,$moCrypt->encryptNoBase64(fread($moFP,16384)));
					}
					fclose($moFP);
					fclose($moWriteFP);

					if(!$moSession->attributeExists("documentFilePath")){
						$moSession->addAttribute("documentFilePath");
					}
					$moSession->setAttrDocumentFilePath($msPath);
					if(!$moSession->attributeExists("documentFileName")){
						$moSession->addAttribute("documentFileName");
					}
					$moSession->setAttrDocumentFileName($msFileName);
					if(!$moSession->attributeExists("documentFileSize")){
						$moSession->addAttribute("documentFileSize");
					}
					$moSession->setAttrDocumentFileSize($miFileSize);
					if(!$moSession->attributeExists("documentFileContent")){
						$moSession->addAttribute("documentFileContent");
					}
				}else{
					//O diret�rio n�o p�de ser escrito!
					trigger_error("Directory '{$msDirectory}' is not writable.",E_USER_ERROR);
				}
			}else{
				// Erro no upload -> armazena c�digo do erro na sess�o
				if(!$moSession->attributeExists('uploadErrorCode')){
					$moSession->addAttribute('uploadErrorCode');
				}
				$moSession->setAttrUploadErrorCode($miFileError);
			}
		}
		// Sinaliza que, com ou sem arquivo, com ou sem erro, acabou o submit
		if(!$moSession->attributeExists("policySaveComplete")){
			$moSession->addAttribute("policySaveComplete");
		}
		$moSession->setAttrPolicySaveComplete(true);

	}
}
class SaveCompleteEvent extends FWDRunnable {
	public function run(){
		$moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
		$id = $moSession->attributeExists("documentContextId")?$moSession->getAttrDocumentContextId():0;
		$place = new CMPlace();

		if(!$moSession->attributeExists("policySaveComplete")){
			echo "setTimeout('trigger_event(\"save_complete_event\",3)',200);";
			return;
		}

		if($place->fetchById($id) && $moSession->attributeExists("policySaveComplete")){
			if($moSession->getAttrPolicySaveComplete()){
				$moSession->deleteAttribute("policySaveComplete");

				if($moSession->attributeExists('uploadErrorCode') && $moSession->getAttrUploadErrorCode()){
					// Trata erro no upload
					$miErrorCode = $moSession->getAttrUploadErrorCode();
					$moSession->deleteAttribute('uploadErrorCode');

					if($miErrorCode & FWDFile::E_MAX_SIZE){
						echo "gobi('warning_max_size').show();";
					}else{
						echo "gobi('warning_upload_error').show();";
					}
					echo "soTabManager.unlockTabs();";
				}else{
					// Upload feito com sucesso
					$moWebLib = FWDWebLib::getInstance();

					if($moSession->attributeExists("documentFilePath")){

						$msPath = $moSession->getAttrDocumentFilePath();
						$msFileName = $moSession->attributeExists("documentFileName")?$moSession->getAttrDocumentFileName():"";

						$place->setFieldValue('file_path',$msPath);
						$place->setFieldValue('file_name',$msFileName);
					}

					if(!$place->getFieldValue('resource')){
						$place->setFieldValue('resource', 'null');
					}
					if(!$place->getFieldValue('resourceSubstitute')){
						$place->setFieldValue('resourceSubstitute', 'null');
					}
					if(!$place->getFieldValue('groupSubstitute')){
						$place->setFieldValue('groupSubstitute', 'null');
					}
					if(!$place->getFieldValue('group')){
						$place->setFieldValue('group', 'null');
					}
					if(!$place->getFieldValue('parent')){
						$place->setFieldValue('parent', 'null');
					}
					$place->update($id);

					if ($moSession->attributeExists("documentFilePath")) $moSession->deleteAttribute("documentFilePath");
					if ($moSession->attributeExists("documentFileName")) $moSession->deleteAttribute("documentFileName");
					if ($moSession->attributeExists("documentFileSize")) $moSession->deleteAttribute("documentFileSize");
					if ($moSession->attributeExists("documentFileContent")) $moSession->deleteAttribute("documentFileContent");
					if ($moSession->attributeExists("documentContextId")) $moSession->deleteAttribute("documentContextId");
				}
			}
		}
		echo "soWindow = soPopUpManager.getPopUpById('popup_place_edit').getOpener();"
		."if(soWindow.refresh_grid) soWindow.refresh_grid();"
		."soPopUpManager.closePopUp('popup_place_edit');";
	}
}
class DownloadEvent extends FWDRunnable {
	public function run() {
		$moWebLib = FWDWebLib::getInstance();
		$placeId = $moWebLib->getObject('place_id')->getValue();
		$place = new CMPlace();

		if ($place->fetchById($placeId)) {
			$msPath = $place->getFieldValue('file_path');
			$msFileName = $place->getFieldValue('file_name');

			header('Cache-Control: ');// leave blank to avoid IE errors
			header('Pragma: ');// leave blank to avoid IE errors

			if(strpos(strtolower($msFileName),"jpg") !== false || strpos(strtolower($msFileName),"jpeg" !== false)){
				header('Content-type: image/jpeg');
				header('Content-Disposition: inline; filename="'.$msFileName.'"');
			}else if(strpos(strtolower($msFileName),"gif") !== false){
				header('Content-type: image/gif');
				header('Content-Disposition: inline; filename="'.$msFileName.'"');
			}else if(strpos(strtolower($msFileName),"tiff") !== false || strpos(strtolower($msFileName),"tif") !== false){
				header('Content-type: image/tiff');
				header('Content-Disposition: inline; filename="'.$msFileName.'"');
			}else if(strpos(strtolower($msFileName),"bmp") !== false){
				header('Content-type: image/x-ms-bmp');
				header('Content-Disposition: inline; filename="'.$msFileName.'"');
			}else{
				header('Content-type: application/octet-stream');
				header('Content-Disposition: attachment; filename="'.$msFileName.'"');
			}

			$moCrypt = new FWDCrypt();
			$moFP = fopen($msPath,'rb');
			$moCrypt->setIV(fread($moFP,strlen($moCrypt->getIV())));
			while(!feof($moFP)) {
				echo $moCrypt->decryptNoBase64(fread($moFP,16384));
			}
			fclose($moFP);
		}
	}
}


class ConfirmEditEvent extends FWDRunnable {
	public function run(){
		$miAreaId = FWDWebLib::getObject('area_id')->getValue();
		$place = getAreaFromFields();

		save($place,true,$miAreaId);
	}
}

class SaveAreaEvent extends FWDRunnable {
	public function run(){
		$substituteName = FWDWebLib::getObject('substitute')->getValue();
		$groupName = FWDWebLib::getObject('group')->getValue();
				
		$moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
		if($moSession->attributeExists('policySaveComplete'))  $moSession->deleteAttribute('policySaveComplete');
		if($moSession->attributeExists('uploadErrorCode'))     $moSession->deleteAttribute('uploadErrorCode');
		if($moSession->attributeExists('documentFilePath'))    $moSession->deleteAttribute('documentFilePath');
		if($moSession->attributeExists('documentFileName'))    $moSession->deleteAttribute('documentFileName');
		if($moSession->attributeExists('documentFileSize'))    $moSession->deleteAttribute('documentFileSize');
		if($moSession->attributeExists('documentFileContent')) $moSession->deleteAttribute('documentFileContent');
		if($moSession->attributeExists('documentContextId'))   $moSession->deleteAttribute('documentContextId');

		$miPlaceId = FWDWebLib::getObject('place_id')->getValue();
		
		$moPlace = getPlaceFromFields();
		
		$moPlace->setHash(FWDWebLib::getObject('hash')->getValue());
		$mbHasSensitiveChanges = $moPlace->hasSensitiveChanges();
		
		// Verifica se o Substituto existe.
		if($substituteName != ''){
			$moQuerySubstitute = new QueryCheckPlaceEditResourceSearch(FWDWebLib::getConnection());
			$moQuerySubstitute->setResourceName($substituteName);
			$moQuerySubstitute->makeQuery();
			$moQuerySubstitute->fetchResourceName();
			$existsSubstitute = $moQuerySubstitute->checkResourceName();
		}else{
			$existsSubstitute = true;
			$moPlace->setSubstitute('null');
		}
		// Verifica se o Respons�vel existe.
		if($groupName != ''){
			$moQueryGroup = new QueryCheckPlaceEditResourceSearch(FWDWebLib::getConnection());
			$moQueryGroup->setResourceName($groupName);
			$moQueryGroup->makeQuery();
			$moQueryGroup->fetchResourceName();
			$existsGroup = $moQueryGroup->checkResourceName();
		}else{
			$existsGroup = true;
		}
		
		if($existsGroup == false){
			echo "gobi('warning_group_exists').show();";
		}elseif($existsSubstitute == false){
			echo "gobi('warning_substitute_exists').show();";
		}elseif($moPlace->getSubstitute() && $moPlace->getGroup() && $moPlace->getSubstitute() != "null" && $moPlace->getGroup() != "null" && $moPlace->getSubstitute() == $moPlace->getGroup()){
			echo "gobi('warning_substitute_asset').show();";
		}else{
			save($moPlace,$mbHasSensitiveChanges,$miPlaceId);
		}
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(''));

		$defaultFinderGroup = new DefaultFinder('search_group');
		$defaultFinderGroup->set('group', new QueryGridPlaceEditResourceSearch(FWDWebLib::getConnection()));
		$moStartEvent->addAjaxEvent($defaultFinderGroup);

		$defaultFinderSubstitute = new DefaultFinder('search_substitute');
		$defaultFinderSubstitute->set('substitute', new QueryGridPlaceEditResourceSearch(FWDWebLib::getConnection()));
		$moStartEvent->addAjaxEvent($defaultFinderSubstitute);

		$moStartEvent->addAjaxEvent(new SaveAreaEvent("save_area_event"));
		$moStartEvent->addAjaxEvent(new SaveCompleteEvent("save_complete_event"));
		$moStartEvent->addSubmitEvent(new SubmitEvent("upload"));
		$moStartEvent->addSubmitEvent(new DownloadEvent("download"));


		//		$moStartEvent->addAjaxEvent(new ConfirmEditEvent('confirm_edit'));
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$parent = FWDWebLib::getObject('parent')->getValue();
		if($parent){
			FWDWebLib::getObject('parentId')->setValue($parent);
		}
		$miPlaceId = FWDWebLib::getObject('param_place_id')->getValue();
		FWDWebLib::getObject('place_id')->setValue($miPlaceId);
		$moWindowTitle = FWDWebLib::getObject('window_title');
    
		if($miPlaceId){
			$moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_place_editing', 'Edi��o de Local'));

			$moPlace = new CMPlace();
			$moPlace->fetchById($miPlaceId);
			FWDWebLib::getObject('place_name')->setValue($moPlace->getFieldValue('place_name'));
						
			FWDWebLib::getObject('place_description')->setValue($moPlace->getFieldValue('place_description'));
						
			FWDWebLib::getObject('substituteId')->setValue($moPlace->getSubstitute());
			FWDWebLib::getObject('substitute')->setValue($moPlace->getSubstituteName());
			FWDWebLib::getObject('groupId')->setValue($moPlace->getGroup());
			FWDWebLib::getObject('group')->setValue($moPlace->getGroupName());

			$address = $moPlace->getFieldValue('place_address');
			$complete = $address;

			FWDWebLib::getObject('address_name')->setValue($complete);

			if(!$parent){
				FWDWebLib::getObject('parentId')->setValue($moPlace->getFieldValue('parent'));
			}

			FWDWebLib::getObject('hash')->setValue($moPlace->getHash());


			//File
			$moWebLib = FWDWebLib::getInstance();
			$msPath = $moPlace->getFieldValue('file_path');
			if ($msPath && is_file($msPath)) {
				$msFileName = $moPlace->getFieldValue('file_name');

				$link = "<a href=javascript:try{js_submit('download');}catch(e){debugException(e)}>";
				$closeLink = "</a>";

				$fileName = $moWebLib->getObject('file_name');
				$fileName->setValue($link.$msFileName.$closeLink);
				$fileName->setAttrDisplay("true");
				$moWebLib->getObject('changeFile')->setAttrDisplay("true");
				$moWebLib->getObject('file')->setAttrDisplay("false");
			}
		}else{
			$moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
			$moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_place_adding', 'Adi��o de Local'));
		}

		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
<script language="javascript">
        gebi('place_name').focus();
        
        var subs = false;
 	   	function setGroup(id, name, acronym){
				if(subs){
	 	 			gebi("substituteId").value = id;
	 	         	gebi("substitute").value = name;
	 	 		}else{
	 	 			gebi("groupId").value = id;
	 	         	gebi("group").value = name;
	 	 		}
      	}

 	   			
 	  	function setSubstitute(sub){
    		subs = sub;
    	}

    	function changeFile(){
    		gebi("file").style.display="";
    		gebi("changeFile").style.display="none";
    		gebi("file_name").style.display="none";
    	}
</script>
		<?
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_place_edit.xml');

?>