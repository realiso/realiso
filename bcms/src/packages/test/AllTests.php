<?php
require_once('simpletest/unit_tester.php');
require_once('simpletest/reporter.php');
require_once('../continuity/include.php');

class AllTests extends TestSuite {
	function AllTests() {
		$this->TestSuite('Todos os testes');
		
		$this->addTestFile('TestCMScene.php');
	}
}

$test = &new AllTests();
$test->run(new HtmlReporter());

?>