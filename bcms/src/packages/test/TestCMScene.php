<?php
require_once('simpletest/unit_tester.php');
require_once('simpletest/autorun.php');

Mock::generate('CMScene');

class TestCMScene extends UnitTestCase {

	public $scene;

	private $areaId;

	private $processId;
	
	function createProcess() {
		$area = new RMArea();
		$area->setFieldValue('area_id', 2);
		$area->setFieldValue('area_name', 'Area Teste');
		$this->areaId = $area->insert(true);

		$process = new RMProcess();
		$process->setFieldValue('process_id', 1);
		$process->setFieldValue('process_area_id', $this->areaId);
		$process->setFieldValue('process_name', 'Processo Teste');
		$this->processId = $process->insert(true);
	}

	function removeProcess(){
		$process = new RMProcess();
		$process->delete($this->processId, true);

		$area = new RMArea();
		$area->delete($this->areaId, true);
	}

	function test_getProcess_semIdProcesso() {
		$scene = new CMScene();
		$this->assertEqual(null, $scene->getProcess());
	}

	function test_getProcess_comIdProcesso() {
		$this->createProcess();

		$scene = new CMScene();
		$scene->setFieldValue('scene_process', $this->processId);
		$process = $scene->getProcess();
		$this->assertNotNull($process);
		$this->assertEqual($this->processId, $process->getFieldValue('process_id'));

		$this->removeProcess();
	}

	function test_getSerializedSeasonality_idNull() {
		$scene = new CMScene();
		$this->assertEqual(null, $scene->getSerializedSeasonality());
	}

	function test_getSerializedSeasonality_idPreenchidoSemProcesso() {
		$scene = new CMScene();
		$scene->setFieldValue('scene_id', 1);
		$this->assertEqual(null, $scene->getSerializedSeasonality());
	}
}
?>