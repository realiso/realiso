<?php
include_once "include.php";
include_once $handlers_ref . "QueryRisksAndAssetsByIncidentId.php";

class SaveEvent extends FWDRunnable {
  public function run() {
    $maRiskAssetAssociation = FWDWebLib::unserializeString(FWDWebLib::getObject('risk_asset_association')->getValue());
    $maRiskAsset = array();
    foreach($maRiskAssetAssociation as $msAssetRisk) {
      list($miAssetId,$msRiskIds) = split(':',$msAssetRisk);
      $maRiskAsset[$miAssetId] = array_filter(split(',',$msRiskIds));
      if (!count($maRiskAsset[$miAssetId])) unset($maRiskAsset[$miAssetId]);
    }
    foreach($maRiskAsset as $miAssetId=>$maRisks) {
      foreach($maRisks as $miRiskId) {
        $moRisk = new RMRisk();
        if ($moRisk->fetchById($miRiskId)) {
          $miEventId = $moRisk->getFieldValue('risk_event_id');
          $moNewRisk = new RMRisk();
          $moNewRisk->setFieldValue("risk_value",0);
          $moNewRisk->setFieldValue("risk_residual_value",0);
          if ($miEventId) $moNewRisk->setFieldValue("risk_event_id",$miEventId);
          $moNewRisk->setFieldValue("risk_asset_id",$miAssetId);
          $moNewRisk->setFieldValue("risk_name",$moRisk->getFieldValue('risk_name'),false);
          $moNewRisk->insert();
        }
      }
    }
    echo "self.close();";
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
    $moStartEvent->addAjaxEvent(new SaveEvent('save_event'));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $miIncidentId = FWDWebLib::getObject('incident_id')->getValue();
    $moSelectAsset = FWDWebLib::getObject('assets');
    $msJsRiskAsset = "assets_ids = new Array();";
    $moIncident = new CIIncident();
    if ($moIncident->fetchById($miIncidentId)) {
      $moQueryRisksAndAssets = new QueryRisksAndAssetsByIncidentId(FWDWebLib::getConnection());
      $moQueryRisksAndAssets->setIncidentId($miIncidentId);
      $maValues = $moQueryRisksAndAssets->getValues();
      $maAssetsIds = array_unique(array_values($maValues));
      $miAssetCount = 0;
      foreach($maAssetsIds as $miAssetId) {
        $moAsset = new RMAsset();
        if ($moAsset->fetchById($miAssetId)) {
          $mbInsertAsset = false;
          $moSelect = new FWDSelect();
          $moSelect->setAttrName("select_risk_".$miAssetId);
          $moSelect->setAttrMultiple("true");
          $moSelect->setAttrDisplay("false");
          $moSelect->setObjFWDBox(new FWDBox(160,120,150,130));
          foreach($maValues as $miRiskId=>$miRiskAssetId) {
            if ($miRiskAssetId==$miAssetId) continue;
            $moRisk = new RMRisk();
            if ($moRisk->fetchById($miRiskId)) {
              $moCompareRisk = new RMRisk();
              $miEventId = $moRisk->getFieldValue('risk_event_id');
              if ($miEventId) {
                $moCompareRisk->createFilter($miAssetId,'risk_asset_id');
                $moCompareRisk->createFilter($miEventId,'risk_event_id');
                $moCompareRisk->select();
                if ($moCompareRisk->fetch()) continue;
              }
              $moCompareRisk->removeAllFilters();
              $moCompareRisk->createFilter($miAssetId,'risk_asset_id');
              $moCompareRisk->createFilter($moRisk->getFieldValue('risk_name'),'risk_name');
              $moCompareRisk->select();
              if ($moCompareRisk->fetch()) continue;
              
              $mbInsertAsset = true;
              $moItem = new FWDItem();
              $moItem->setAttrKey($miRiskId);
              $moItem->setValue($moRisk->getFieldValue('risk_name'));
              $moSelect->addObjFWDItem($moItem);
            }
          }
          if ($mbInsertAsset) {
            $msJsRiskAsset .= "assets_ids[$miAssetCount] = $miAssetId;";
            $miAssetCount++;
            $moSelectAsset->setItemValue($miAssetId,$moAsset->getFieldValue('asset_name'));
          }
          FWDWebLib::getObject('dialog')->addObjFWDView($moSelect);
        }
      }
      if ($miAssetCount > 0) {
        $moSelectAsset->checkItem($maAssetsIds[0]);
        $msJsRiskAsset .= "js_show('select_risk_".$maAssetsIds[0]."');";
      }
    }
    
    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        <?=$msJsRiskAsset?>
        
        function change_asset(piAssetId) {
          hide_risks();
          js_show('select_risk_'+piAssetId);
        }
        
        function hide_risks() {
          for(miI=0;miI<assets_ids.length;miI++) {
            js_hide('select_risk_'+assets_ids[miI]);
          }
          miI = null;
        }
        
        function save() {
          maAssetRisk = new Array();
          for(miI=0;miI<assets_ids.length;miI++) {
            maAssetRisk[miI] = assets_ids[miI] + ':' + gobi('select_risk_'+assets_ids[miI]).getValue();
          }
          
          objSerialize = new fwdObjSerialize();
          gebi('risk_asset_association').value = objSerialize.serializeArray(maAssetRisk);
          miI = maAssetRisk = objSerialize = null;
        }
      </script>
    <?
  }
}
//testa se a licen�a utilizada permite acessar o m�dulo de incidente
ISMSLib::testUserPermissionToIncidentMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_incident_risk_asset_association_edit.xml");
?>