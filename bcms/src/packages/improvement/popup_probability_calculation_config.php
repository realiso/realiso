<?php

include_once "include.php";
include_once $handlers_ref."grid/improvement/QueryGridIncidentWindow.php";
include_once $handlers_ref."QueryCountIncidentWindow.php";

class IncidentWindowDrawGrid extends FWDDrawGrid {
  public function drawItem(){
    switch($this->ciColumnIndex){
      case 1:{
        $this->coCellBox->setIconSrc("icon-ci_incident.gif");
        return $this->coCellBox->draw();
      }
      default:{
        return parent::drawItem();
      }
    }
  }
}

class UpdatePeriodEvent extends FWDRunnable {
  public function run(){
    $miRiskId = FWDWebLib::getObject('risk_id')->getValue();
    $miPeriodSize = FWDWebLib::getObject('period_size')->getValue();
    $miPeriodUnit = FWDWebLib::getObject('period_unit')->getValue();
    
    $miFinalDate = ISMSLib::ISMSTime();
    $miInitialDate = ISMSLib::subtractPeriodFromTime($miPeriodSize,$miPeriodUnit,$miFinalDate);
    
    $moCountHandler = new QueryCountIncidentWindow(FWDWebLib::getConnection());
    $moCountHandler->setRiskId($miRiskId);
    $moCountHandler->setInitialDate($miInitialDate);
    $moCountHandler->setFinalDate($miFinalDate);
    $moCountHandler->makeQuery();
    $moCountHandler->executeQuery();
    $moCountHandler->fetch();
    
    $msInitialDate = ISMSLib::getISMSShortDate($miInitialDate,true);
    $msFinalDate = ISMSLib::getISMSShortDate($miFinalDate,true);
    
    $msLabel = FWDWebLib::getObject('st_incidents_in_period')->getValue();
    $msLabel = str_replace(array('%initial_date%','%final_date%'),array($msInitialDate,$msFinalDate),$msLabel);
    echo "gobi('st_incidents_in_period').setValue('$msLabel');";
    echo "gobi('incidents_in_period').setValue('".$moCountHandler->getFieldValue('incident_count')."');";
  }
}

class UpdateGridEvent extends FWDRunnable {
  public function run(){
    $moGrid = FWDWebLib::getObject('grid_window');
    $moGrid->setAttrPopulate('true');
    $moGrid->execEventPopulate();
    $miRowsCount = $moGrid->getAttrRowsCount();
    echo "gobi('total_incidents').setValue($miRowsCount);";
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
    $moStartEvent->addAjaxEvent(new UpdateGridEvent('update_grid'));
    $moStartEvent->addAjaxEvent(new UpdatePeriodEvent('update_period'));
    
    $moGrid = FWDWebLib::getObject('grid_window');
    $moHandler = new QueryGridIncidentWindow(FWDWebLib::getConnection());
    
    $miRiskId = FWDWebLib::getObject('risk_id')->getValue();
    if(FWDWebLib::getObject('initial_date')->getValue())
       $moHandler->setInitialDate(FWDWebLib::getObject('initial_date')->getTimestamp());
    
    if(FWDWebLib::getObject('final_date')->getValue())
      $moHandler->setFinalDate(FWDWebLib::getObject('final_date')->getTimestamp());

    $moHandler->setRiskId($miRiskId);
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new IncidentWindowDrawGrid('grid_window'));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $miRiskId = FWDWebLib::getObject('risk_id')->getValue();
    $miPeriodUnit = FWDWebLib::getObject('par_period_unit')->getValue();
    $miOffset = 5 - ISMSLib::getConfigById(RISK_VALUE_COUNT);
    $miPeriodSize = '';
    $maValues = array();
    
    $miFinalDate = ISMSLib::ISMSTime();
    $miInitialDate = $miFinalDate - 86400;
    
    if($miPeriodUnit){ // tem valores nos parametros
      $miPeriodSize = FWDWebLib::getObject('par_period_size')->getValue();
      $maValuesFromParams = explode(':',FWDWebLib::getObject('par_probability_values')->getValue());
      for($i=0;$i<count($maValuesFromParams);$i++){
        FWDWebLib::getObject('max_value_'.($i+1+$miOffset))->setValue($maValuesFromParams[$i]);
        FWDWebLib::getObject('min_value_'.($i+2+$miOffset))->setValue($maValuesFromParams[$i]+1);
      }
    }else{
      $moSchedule = new CIRiskSchedule();
      $moSchedule->createFilter($miRiskId,'risk_id');
      $moSchedule->select();
      if($moSchedule->fetch()){ // tem valores no BD
        $miPeriodUnit = $moSchedule->getFieldValue('schedule_period');
        $miPeriodSize = $moSchedule->getFieldValue('schedule_value');
        $miDateLastCheck = ISMSLib::getTimestamp($moSchedule->getFieldValue('schedule_date_last_check'));
        if($miDateLastCheck){
          FWDWebLib::getObject('date_last_check')->setValue(ISMSLib::getISMSShortDate($miDateLastCheck,true));
        }
        
        $moRiskProbabilities = new CIRiskProbability();
        $moRiskProbabilities->createFilter($miRiskId,'risk_id');
        $moRiskProbabilities->select();
        while($moRiskProbabilities->fetch()){
          $miValueNameId = $moRiskProbabilities->getFieldValue('value_name_id');
          $miIncidentAmount = $moRiskProbabilities->getFieldValue('incident_amount');
          $maValues[$miValueNameId] = $miIncidentAmount;
        }
      }
    }
    
    if($miPeriodUnit && $miPeriodSize){
      $miInitialDate = ISMSLib::subtractPeriodFromTime($miPeriodSize,$miPeriodUnit,$miFinalDate);
    }
    $msInitialDate = ISMSLib::getISMSShortDate($miInitialDate,true);
    $msFinalDate = ISMSLib::getISMSShortDate($miFinalDate,true);
    
    $moLabel = FWDWebLib::getObject('st_incidents_in_period');
    $msLabel = str_replace(array('%initial_date%','%final_date%'),array($msInitialDate,$msFinalDate),$moLabel->getValue());
    $moLabel->setValue($msLabel);
    
    $moCountHandler = new QueryCountIncidentWindow(FWDWebLib::getConnection());
    $moCountHandler->setRiskId($miRiskId);
    $moCountHandler->setInitialDate($miInitialDate);
    $moCountHandler->setFinalDate($miFinalDate);
    $moCountHandler->makeQuery();
    $moCountHandler->executeQuery();
    $moCountHandler->fetch();
    FWDWebLib::getObject('incidents_in_period')->setValue($moCountHandler->getFieldValue('incident_count'));
    
    FWDWebLib::getObject('period_size')->setValue($miPeriodSize);
    FWDWebLib::getObject('period_unit')->setValue($miPeriodUnit);
    
    if(ISMSLib::getConfigById(RISK_VALUE_COUNT)==5){
      $miValueCount = 5;
    }else{
      $miValueCount = 3;
      $moLowerValues = FWDWebLib::getObject('vg_lower_values');
      $moLowerValues->setShouldDraw(false);
      
      $moHigherValuesBox = FWDWebLib::getObject('vg_higher_values')->getObjFWDBox();
      $moHigherValuesBox->setAttrTop($moHigherValuesBox->getAttrTop() - $moLowerValues->getObjFWDBox()->getAttrHeight());
    }
    $miOffset = 5 - $miValueCount;
    FWDWebLib::getObject('min_value_'.(1+$miOffset))->setValue(0);
    
    $moParameterValueName = new RMParameterValueName();
    $moParameterValueName->setOrderBy('parametervalue_value','+');
    $moParameterValueName->select();
    $maValueNames = array();
    $miCounter = 1;
    while($moParameterValueName->fetch()){
      $miValueId = $moParameterValueName->getFieldValue('parametervalue_id');
      $msValueName = $moParameterValueName->getFieldValue('parametervalue_rprob');
      FWDWebLib::getObject('value_name_'.($miCounter+$miOffset))->setValue("<b>$msValueName</b>");
      if(isset($maValues[$miValueId])){
        FWDWebLib::getObject('max_value_'.($miCounter+$miOffset))->setValue($maValues[$miValueId]);
        FWDWebLib::getObject('min_value_'.($miCounter+$miOffset+1))->setValue($maValues[$miValueId]+1);
      }
      $miCounter++;
    }
    
    //instala a seguran�a de ACL na p�gina
    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        
        function save(){
          var moParent = soPopUpManager.getPopUpById('popup_probability_calculation_config').getOpener();
          var miPeriodSize = gebi('period_size').value;
          var miPeriodUnit = gebi('period_unit').value;
          moParent.setProbabilityConfig(miPeriodSize,miPeriodUnit,ValuesManager.getValues());
          moParent = null;
          soPopUpManager.closePopUp('popup_probability_calculation_config');
        }
        
        ValuesManager = {
          'ciValueCount': <?=$miValueCount?>,
          'updateValue': function(piIndex){
            var miValue = parseInt(gebi('max_value_'+piIndex).value);
            if(!isNaN(miValue)){
              gobi('min_value_'+(piIndex+1)).setValue(miValue+1);
            }
          },
          'getValues': function(){
            var maValues = [];
            var i = 6 - this.ciValueCount;
            var moValue;
            while(moValue = gebi('max_value_'+i++)){
              maValues.push(moValue.value);
            }
            return maValues;
          },
          'validateValues': function(){
            var i,miValue,miLastValue;
            i = 6 - this.ciValueCount;
            miLastValue = Number(gebi('max_value_'+i).value);
            if(!miLastValue) return false;
            for(i++;i<5;i++){
              miValue = Number(gebi('max_value_'+i).value);
              if(!miValue || miValue<=miLastValue) return false;
              miLastValue = miValue;
            }
            return true;
          }
        }
        
        gebi('vg_values').object = {
          'isFilled': function(){
            return ValuesManager.validateValues();
          },
          'onRequiredCheckOk': function(){
            gebi('vg_values').style.border = 'none';
            gobi('warning').hide();
          },
          'onRequiredCheckNotOk': function(){
            gobi('warning').show();
          }
        }
      </script>
    <?
  }
}

//testa se a licen�a utilizada permite acessar o m�dulo de incidente
ISMSLib::testUserPermissionToIncidentMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_probability_calculation_config.xml');

?>