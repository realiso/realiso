<?php
include_once "include.php";
include_once $handlers_ref . "grid/improvement/QueryGridNonConformity.php";
include_once $classes_isms_ref . 'nonauto/ISMSContextDrawGrid.php';
include_once $handlers_ref . 'select/improvement/QuerySelectNcActionPlan.php';

class NcDrawGrid extends ISMSContextDrawGrid {

  public function __construct(){
    parent::__construct('icon-ci_nonconformity.gif',CONTEXT_CI_NON_CONFORMITY);
    $this->setupToolTipCreateModify('creator_name','date_created','modifier_name','date_modified');
    $this->setColumnType('nc_datesent', ISMSContextDrawGrid::TYPE_DATE);
    $this->setColumnType('nc_state', ISMSContextDrawGrid::TYPE_STATE);
  }
 public function drawItem(){
   switch($this->ciColumnIndex){
      case $this->getIndexByAlias('nc_name'):
        $this->coCellBox->setAttrStringNoEscape("true");
        $moNc = new CINonConformity();
        if( $moNc->userCanEdit($this->getFieldValue('nc_id')) ){
          $this->coCellBox->setValue("<a href='javascript:open_edit(".$this->getFieldValue('nc_id').");'>".$this->coCellBox->getValue()."</a>");
        }else{
          $this->coCellBox->setValue("<a href='javascript:open_visualize(".$this->getFieldValue('nc_id').",".CONTEXT_CI_NON_CONFORMITY .",\"".uniqid()."\");'>".$this->coCellBox->getValue()."</a>");
        }
        return $this->coCellBox->draw();

      break;
      case $this->getIndexByAlias('nc_classification'):
          $msStr = '';
          switch($this->coCellBox->getValue()){
              case NC_ACTION_CLASSIFICATION_INTERNAL_AUDIT:
                $msStr = FWDLanguage::getPHPStringValue('gc_internal_audit', 'Auditoria Interna');
              break;
              case NC_ACTION_CLASSIFICATION_EXTERNAL_AUDIT:
                $msStr = FWDLanguage::getPHPStringValue('gc_external_audit', 'Auditoria Externa');
              break;
              case NC_ACTION_CLASSIFICATION_SECURITY_CONTROL:
                $msStr = FWDLanguage::getPHPStringValue('gc_control_security', 'Controle de Seguran�a');
              break;
              default:
                $msStr = '';
              break;
          }

          $miUserId = ISMSLib::getCurrentUserId();
          $msReturn = "";
          if ($this->getFieldValue('task_receiver')==$miUserId) {
            $moVariable = new FWDVariable();
            $moVariable->setAttrNoEscape('false');
            $moVariable->setAttrName('task_id_'.$this->getFieldValue('nc_id'));
            $moVariable->setValue($this->getFieldValue('task_id'));
            $msReturn .= $moVariable->drawBody();
          }

          $this->coCellBox->setValue($msStr);
          return $this->coCellBox->draw() . " " . $msReturn . " ";
      break;
      case $this->getIndexByAlias('nc_capability'):
          $msStr = '';
          switch($this->coCellBox->getValue()){
              case NC_NO_CAPABILITY:
                $msStr = FWDLanguage::getPHPStringValue('gc_no', 'N�o');
              break;
              case NC_CAPABILITY:
                $msStr = FWDLanguage::getPHPStringValue('gc_yes', 'Sim');
                break;
              default:
                $msStr = '';
              break;
          }
          $this->coCellBox->setValue($msStr);
          return $this->coCellBox->draw();
      break;
      
      case $this->getIndexByAlias('count_action_plan'):
        $this->coCellBox->setAttrStringNoEscape("true");
        if(ISMSLib::userHasACL('M.CI.7')){
          $this->coCellBox->setValue("<a href='javascript:go_to_nav_ap(".$this->getFieldValue('nc_id').");'>".$this->coCellBox->getValue()."</a>");
        }
        return $this->coCellBox->draw();
      break;
      
      default:
        return parent::drawItem();
      break;
    }
  }
  
  protected function setupMenu(){
    $miUserId = ISMSLib::getCurrentUserId();
    $miTaskReceiver = $this->getFieldValue('task_receiver');
    $miNcId = $this->coCellBox->getValue();
    $moNc = new CINonConformity();
    $moNc->fetchById($miNcId);
    /*obtem o proximo estado da NC*/
    $maStates = $moNc->getStateForwardAction();
    $maAllStates = array();
    if(isset($maStates['confirm'])){
      $maAllStates[] = $maStates['confirm'];
    }
    
    if(isset($maStates['direct'])){
      $maAllStates[] = $maStates['direct'];
    }
    
    $maMenuACLs = array('edit','advanced_edit','delete','send_to_responsible','send_to_ap_pendant','waiting_conclusion','approve','associate_ap');
    $maAllowed = array();
    if($moNc->userCanEdit($miNcId)){ // teste se o usuario pode editar
      $maAllowed[] = 'advanced_edit';
    }
    if($moNc->userCanDelete($miNcId)){ // teste se o usuario pode remover
      $maAllowed[] = 'delete';
    }
    
    foreach($maAllStates as $miState) {
      switch($miState){
        case WKF_ACTION_APPROVE:{
          if ($miUserId == $miTaskReceiver) $maAllowed[] = 'approve';
          break;
        }
        case WKF_ACTION_CI_NC_SEND_TO_RESPONSIBLE:{        $maAllowed[] = 'send_to_responsible';  break;}
        case WKF_ACTION_SEND_NC_TO_APPROVE:{               $maAllowed[] = 'send_to_ap_pendant';   break;}
        case WKF_ACTION_CI_NC_SEND_TO_WAITING_CONCLUSION:{ $maAllowed[] = 'waiting_conclusion';   break;}
        default:{ break; }
      }
    }
    
    //associate_ap
    if ($moNc->getContextState() == CONTEXT_STATE_CI_DIRECTED){
      $maAllowed[] = 'associate_ap';
    }
    
    $moACL = FWDACLSecurity::getInstance();
    $moACL->setNotAllowed(array_diff($maMenuACLs,$maAllowed));
    $moMenu = FWDWebLib::getObject('menu');
    $moGrid = FWDWebLib::getObject('grid_nc');
    $msLine = $moGrid->getAttrName().'_acl_'.$this->ciRowIndex;
    FWDGridMenuACLSecurity::getInstance()->installGridMenuACL($moMenu,$moACL,$moGrid,$msLine);
  }
}

class ConfirmRemove extends FWDRunnable {
  public function run() {
    $msTitle = FWDLanguage::getPHPStringValue('tt_nc_remove','Remover N�o Conformidade');
    $msMessage = FWDLanguage::getPHPStringValue('st_nc_remove_message','Voc� tem certeza de que deseja remover a n�o conformidade <b>%name%</b>?');

    $moNc = new CINonConformity();
    $miNcId = intval(FWDWebLib::getObject('selected_nc_id')->getValue());
    $moNc->fetchById($miNcId);
    $msName = ISMSLib::truncateString($moNc->getFieldValue('nc_name'), 70);
    $msMessage = str_replace('%name%',$msName,$msMessage);

    $msEventValue = "soPopUpManager.getPopUpById('popup_confirm').getOpener().remove();";

    ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,50);
  }
}

class RemoveEvent extends FWDRunnable {
  public function run() {
    $miNcId = intval(FWDWebLib::getObject('selected_nc_id')->getValue());

    $moCtxUserTest = new CINonConformity();
    $moCtxUserTest->testPermissionToDelete($miNcId);

    $moNc = new CINonConformity();
    if($moNc->isDeletable($miNcId)){
      $moNc->delete($miNcId);
      FWDWebLib::getObject('grid_nc')->execEventPopulate();
      echo 'refresh_grid();';
    }else{
      $moNc->showDeleteError();
    }
  }
}

class StateForwardEvent extends FWDRunnable {
  public function run() {
    $miNCId = FWDWebLib::getObject('selected_nc_id')->getValue();
    $moNonConformity = new CINonConformity();
    $moNonConformity->getStateForwardConfirmation($miNCId);
  }
}

class SaveNCApsEvent extends FWDRunnable {
  public function run() {
    $moNc = new CINonConformity();
    $moNc->fetchById(FWDWebLib::getObject('selected_nc_id')->getValue());
    $moNc->setFieldValue('nc_id',FWDWebLib::getObject('selected_nc_id')->getValue());

    $maActionPlans = array_filter(explode(':',FWDWebLib::getObject('var_action_plans')->getValue()));
    if(!is_array($maActionPlans)){
      $maActionPlans = array($maActionPlans);
    }
    $moNc->updateRelation(new CINcActionPlan(),'ap_id',$maActionPlans,true);

    echo 'refresh_grids();';
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
    $moStartEvent->addAjaxEvent(new ConfirmRemove('confirm_remove'));
    $moStartEvent->addAjaxEvent(new RemoveEvent('remove_event'));
    $moStartEvent->addAjaxEvent(new StateForwardEvent('state_forward_event'));
    $moStartEvent->addAjaxEvent(new SaveNCApsEvent('save_nc_aps'));
    
    //instala a seguran�a de ACL na p�gina
    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
    FWDMenuACLSecurity::getInstance()->installMenuACL(FWDWebLib::getObject('menu'),FWDACLSecurity::getInstance());

    $moGrid = FWDWebLib::getObject('grid_nc');
    $moHandler = new QueryGridNonConformity(FWDWebLib::getConnection());
    $moHandler->setActionPlan(FWDWebLib::getObject('ap_id_filter')->getValue());
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new NcDrawGrid());
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moNc = new CINonConformity();
    if(!$moNc->userCanInsert()){
      FWDWebLib::getObject('insert')->setShouldDraw(false);
    }
    
    if(FWDWebLib::getObject('ap_id_filter')->getValue()){
      $moAp = new CIActionPlan();
      $moAp->fetchById(FWDWebLib::getObject('ap_id_filter')->getValue());
      FWDWebLib::getObject('nc_grid_title')->setValue(FWDWebLib::getObject('nc_grid_title')->getValue() . " " .str_replace('%ap_name%',$moAp->getName(),FWDLanguage::getPHPStringValue('tt_ap_filter_grid_non_conformity',"(filtrada pelo plano de a��o '<b>%ap_name%</b>')")));
    }

    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language='javascript'>
        function refresh_grids(){
          gobi('grid_nc').refresh();
        }
        function refresh_grid(){
          gobi('grid_nc').refresh();
        }

        function remove(){
          trigger_event('remove_event',3);
          refresh_grid();
        }
        
        function open_edit(piId){
          isms_open_popup('popup_non_conformity_edit','packages/improvement/popup_non_conformity_edit.php?nc='+piId,'','true');
        }
        
        function go_to_nav_ap(piId){
          isms_change_to_sibling(TAB_PLACE,'nav_action_plan.php?nc_id='+piId);
        }
        
        function open_ap_nc(piId){
          isms_open_popup('popup_action_plan_associate','packages/improvement/popup_action_plan_associate.php?getapids=1&nc='+piId,'','true');
        }
        
        function set_action_plans(psIds){
          gebi('var_action_plans').value = psIds;
          trigger_event('save_nc_aps',3);
        }
      </script>
    <?
  }
}

// Testa se a licen�a utilizada permite acessar este m�dulo
ISMSLib::testUserPermissionToIncidentMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('nav_non_conformity.xml');


?>