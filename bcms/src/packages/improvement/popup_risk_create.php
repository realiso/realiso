<?php

include_once "include.php";

include_once $handlers_ref . "select/QuerySelectRiskType.php";

class SaveEvent extends FWDRunnable {
  public function run(){
    $moRisk = new RMRisk();
    $moRisk->fillFieldsFromForm();
    $moRisk->setFieldValue('risk_cost', str_replace(',','',FWDWebLib::getObject('risk_cost')->getValue()));
    $miRiskId = $moRisk->insert(true);
    
    $moAsset = new RMAsset();
    $moAsset->fetchById($moRisk->getFieldValue('risk_asset_id'));
    $miReceiverId = $moAsset->getFieldValue('asset_security_responsible_id');
    
    $moAlert = new WKFAlert();
    $moAlert->setFieldValue('alert_type', WKF_ALERT_RISK_CREATED);
    $moAlert->setFieldValue('alert_context_id', $miRiskId);
    $moAlert->setFieldValue('alert_creator_id', ISMSLib::getCurrentUserId());
    $moAlert->setFieldValue('alert_receiver_id', $miReceiverId);
    $moAlert->setFieldValue('alert_justification', '');
    $moAlert->setFieldValue('alert_date', ISMSLib::ISMSTime());
    $moAlert->insert();
    
    echo "soPopUpManager.getPopUpById('popup_risk_create').getOpener().refresh_risks();"
        ."soPopUpManager.closePopUp('popup_risk_create');";
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
    $moStartEvent->addAjaxEvent(new SaveEvent('save'));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    //teste para ver se o custo esta abilitado no sistema
    $moConfig = new ISMSConfig();
    if(!$moConfig->getConfig(GENERAL_COST_ENABLED)){
      FWDWebLib::getObject('cost')->setShouldDraw(false);
      FWDWebLib::getObject('risk_cost')->setShouldDraw(false);
    }
    
    $moHandler = new QuerySelectRiskType(FWDWebLib::getConnection());
    $moSelect = FWDWebLib::getObject('risk_type');
    $moSelect->setQueryHandler($moHandler);
    $moSelect->populate();

    //instala a seguran�a de ACL na p�gina
    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        gebi('risk_name').focus();
        
        function set_asset(piId,psName){
          gebi('risk_asset_id').value = piId;
          gobi('risk_asset_name').setValue(psName);
        }
      </script>
    <?
  }
}

//testa se a licen�a utilizada permite acessar o m�dulo de incidente
ISMSLib::testUserPermissionToIncidentMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_risk_create.xml');

?>