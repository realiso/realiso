<?php
include_once "include.php";
include_once $handlers_ref . "grid/improvement/QueryGridNCProcess.php";
include_once $handlers_ref . "select/improvement/QuerySelectNCProcess.php";

class UpdateCurrentProcessesEvent extends FWDRunnable {
  public function run() {
    $moHandler = new QueryGridNCProcess(FWDWebLib::getConnection());
    $maIds = array_unique(array_filter(explode(":",FWDWebLib::getObject('processes_ids')->getValue())));
    $moHandler->showProcesses($maIds);
    $maCurrentProcesses = $moHandler->getSelectProcesses();
    $msCurrentProcesses = serialize($maCurrentProcesses);
    echo ISMSLib::getJavascriptPopulateSelect('nc_processes', $msCurrentProcesses);
  }
}

class SaveEvent extends FWDRunnable {
  public function run() {
    $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();

    $moNonConformity = new CINonConformity();
    $moNonConformity->setFieldValue("nc_name",FWDWebLib::getObject('nc_name')->getValue());
    $moNonConformity->setFieldValue("nc_sender_id",$miUserId);
    
    if(ISMSLib::getConfigById(GENERAL_MANUAL_DATA_CONTROL)){
      $moNonConformity->setFieldValue("nc_datesent",FWDWebLib::getObject('date_create_nc')->getTimestamp());
    }else{
      $moNonConformity->setFieldValue("nc_datesent",ISMSLib::ISMSTime());
    }
    
    $msDescription = FWDWebLib::getObject('nc_description')->getValue();
    if ($msDescription) $moNonConformity->setFieldValue("nc_description",$msDescription);
    
    $moCtxUserTest = new CINonConformity();
    $moCtxUserTest->testPermissionToInsert();
    $miNCId = $moNonConformity->insert(true);
    
    $maInitialProcesses = array_unique(array_filter(explode(':',FWDWebLib::getObject('initial_processes_ids')->getValue())));
    $maProcesses = array_unique(array_filter(explode(':',FWDWebLib::getObject('processes_ids')->getValue())));
    $maAssociateProcesses = array_diff($maProcesses,$maInitialProcesses);
    $maDisassociateProcesses = array_diff($maInitialProcesses,$maProcesses);
    foreach($maAssociateProcesses as $miProcessId) {
      $moNCProcess = new CINonConformityProcess();
      $moNCProcess->setFieldValue('nc_id',$miNCId);
      $moNCProcess->setFieldValue('process_id',$miProcessId);
      $moNCProcess->insert();
    }
    foreach($maDisassociateProcesses as $miProcessId) {
      $moNCProcess = new CINonConformityProcess();
      $moNCProcess->createFilter($miNCId,'nc_id');
      $moNCProcess->createFilter($miProcessId,'process_id');
      $moNCProcess->delete();
    }
    
    echo "soWindow = soPopUpManager.getPopUpById('popup_non_conformity_create').getOpener();;
          if (soWindow.refresh_grid) soWindow.refresh_grid();
          soPopUpManager.closePopUp('popup_non_conformity_create');";
  }
  
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new UpdateCurrentProcessesEvent("update_current_processes"));
    $moStartEvent->addAjaxEvent(new SaveEvent("save_event"));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moNonConformity = new CINonConformity();

    $moCtxUserTest = new CINonConformity();
    $moCtxUserTest->testPermissionToInsert();
    
    $miSenderId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    $moUser = new ISMSUser();
    $moUser->fetchById($miSenderId);
    $msUserName = $moUser->getFieldValue('user_name');
    
    FWDWebLib::getObject('nc_sender')->setValue($msUserName);
    FWDWebLib::getObject('nc_date_sent')->setValue(ISMSLib::getISMSShortDate());
    if(ISMSLib::getConfigById(GENERAL_MANUAL_DATA_CONTROL)){
      FWDWebLib::getObject('nc_date_sent')->setShouldDraw(false);
      FWDWebLib::getObject('vg_date_create_nc')->setAttrDisplay("true");
      FWDWebLib::getObject('label_nc_date_sent')->setAttrMustFill("true");
      FWDWebLib::getObject('label_nc_date_sent')->setValue("<b>".FWDWebLib::getObject('label_nc_date_sent')->getValue()."</b>");
      FWDWebLib::getObject('date_create_nc')->setValue(ISMSLib::getISMSShortDate());
      
      
      FWDWebLib::getObject('req_save')->setAttrElements(FWDWebLib::getObject('req_save')->getAttrElements().':date_create_nc');
    }


    //instala a seguran�a de ACL na p�gina
    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        gebi('nc_name').focus();
        var sbHasManualData = <? echo ISMSLib::getConfigById(GENERAL_MANUAL_DATA_CONTROL); ?>
        
        function set_processes(psCurrentIds) {
          gebi('processes_ids').value = psCurrentIds;
          psCurrentIds=null;
          trigger_event('update_current_processes',3);
        }
        
        function set_user(piId, psName) {
          gebi('sender_id').value = piId;
          gobi('nc_sender').setValue(psName);
        }
      </script>
    <?
  }
}
//testa se a licen�a utilizada permite acessar o m�dulo de incidente
ISMSLib::testUserPermissionToIncidentMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_non_conformity_create.xml");

?>