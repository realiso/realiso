<?php
include_once "include.php";
include_once $handlers_ref . "QueryAssetsByIncidentId.php";
include_once $handlers_ref . "QueryAssetAndControlResponsiblesByRisk.php";

class SelectOptionsEvent extends FWDRunnable {
  public function run() {
    $miIncidentId = FWDWebLib::getObject('incident_id')->getValue();
    $mbSendUserAlerts = (bool)trim(FWDWebLib::getObject('user_alerts_controller')->getValue(),':');
    $mbAssociateRisks = (bool)trim(FWDWebLib::getObject('risk_asset_association_controller')->getValue(),':');
    if ($mbSendUserAlerts) {
      echo "isms_open_popup('popup_associate_users','packages/policy/popup_associate_users.php','','true',402,700);
            soWindow = soPopUpManager.getPopUpById('popup_associate_users');
            soWindow.setCloseEvent(
              function(){ 
                soWindow = soPopUpManager.getPopUpById('popup_incident_risk_association_options').getWindow();
                if (soWindow.check_risk_association) soWindow.check_risk_association();
              }
            );
          ";
    } elseif($mbAssociateRisks) {
      echo "isms_open_popup('popup_incident_risk_asset_association_edit','packages/improvement/popup_incident_risk_asset_association_edit.php?incident=".$miIncidentId."','','true',312,300);
            soWindow = soPopUpManager.getPopUpById('popup_incident_risk_asset_association_edit');
            soWindow.setCloseEvent(
              function(){ 
                soPopUpManager.closePopUp('popup_incident_risk_association_options');
              }
            );
            ";
    } else {
      echo "self.close();";
    } 
  }
}

class DispatchAlertsEvent extends FWDRunnable {
  public function run(){
    $msDebug = '';

    $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    $miIncidentId = FWDWebLib::getObject('incident_id')->getValue();

    $moHandler = new QueryAssetAndControlResponsiblesByRisk(FWDWebLib::getConnection());
    $moHandler->setIncidentId($miIncidentId);
    $moHandler->setRiskIds(FWDWebLib::getObject('new_risks')->getValue());
    $moHandler->makeQuery();
    $moHandler->executeQuery();

    while($moHandler->fetch()){
      // envia alerta
      $miAlertType = ($moHandler->getFieldValue('type')==CONTEXT_ASSET?WKF_ALERT_INC_ASSET:WKF_ALERT_INC_CONTROL);
      $moAlert = new WKFAlert();
      $moAlert->setFieldValue('alert_type', $miAlertType);
      $moAlert->setFieldValue('alert_context_id', $moHandler->getFieldValue('relation_id'));
      $moAlert->setFieldValue('alert_creator_id', $miUserId);
      $moAlert->setFieldValue('alert_receiver_id', $moHandler->getFieldValue('responsible'));
      $moAlert->setFieldValue('alert_justification', '');
      $moAlert->setFieldValue('alert_date', ISMSLib::ISMSTime());
      $moAlert->insert();
    }

    $maUsers = array_filter(explode(':',FWDWebLib::getObject('users_ids')->getValue()));
    foreach($maUsers as $miReceiverId){
      // envia alerta
      $moAlert = new WKFAlert();
      $moAlert->setFieldValue('alert_type', WKF_ALERT_INCIDENT);
      $moAlert->setFieldValue('alert_context_id', $miIncidentId);
      $moAlert->setFieldValue('alert_creator_id', $miUserId);
      $moAlert->setFieldValue('alert_receiver_id', $miReceiverId);
      $moAlert->setFieldValue('alert_justification', '');
      $moAlert->setFieldValue('alert_date', ISMSLib::ISMSTime());
      $moAlert->insert();
    }
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
    $moStartEvent->addAjaxEvent(new SelectOptionsEvent('select_options_event'));
    $moStartEvent->addAjaxEvent(new DispatchAlertsEvent("dispatch_alerts"));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $miIncidentId = FWDWebLib::getObject('par_incident_id')->getValue();
    FWDWebLib::getObject('incident_id')->setValue($miIncidentId);
    $maNewRisks = array_unique(array_filter(explode(':',FWDWebLib::getObject('new_risks')->getValue())));
    
    if (count($maNewRisks)) {
      $msAlertMessage = FWDWebLib::getObject('send_alert_to_other_users_message')->getValue();
      $msAlertMessage = str_replace('%new_risks%',count($maNewRisks),$msAlertMessage);
      FWDWebLib::getObject('send_alert_to_other_users_message')->setValue($msAlertMessage,true);
    } else {
      FWDWebLib::getObject('user_alerts_viewgroup')->setShouldDraw(false);
      FWDWebLib::getObject('risk_asset_association_viewgroup')->getObjFWDBox()->setAttrTop(30);
      FWDWebLib::getObject('continue')->getObjFWDBox()->setAttrTop(120);
      FWDWebLib::getObject('dialog')->getObjFWDBox()->setAttrHeight(150);
    }
    
    $moIncident = new CIIncident();
    if ($moIncident->fetchById($miIncidentId)) {
      $moQueryAssets = new QueryAssetsByIncidentId(FWDWebLib::getConnection());
      $moQueryAssets->setIncidentId($miIncidentId);
      $maAssetsIds = $moQueryAssets->getValues();
      if (count($maAssetsIds) > 1) {
        
      } else {
        FWDWebLib::getObject('risk_asset_association_viewgroup')->setShouldDraw(false);
        FWDWebLib::getObject('continue')->getObjFWDBox()->setAttrTop(110);
        FWDWebLib::getObject('dialog')->getObjFWDBox()->setAttrHeight(140);
      }
    }
    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        function set_users(psUsers){
          gebi('users_ids').value = psUsers;
          trigger_event('dispatch_alerts',3);
        }
        
        function check_risk_association() {
          if (gebi('risk_asset_association_controller') && gebi('risk_asset_association_controller').value==1) {
            isms_open_popup('popup_incident_risk_asset_association_edit','packages/improvement/popup_incident_risk_asset_association_edit.php?incident='+gebi('incident_id').value,'','true',312,300);
            soWindow = soPopUpManager.getPopUpById('popup_incident_risk_asset_association_edit');
            soWindow.setCloseEvent(
              function(){ 
                soPopUpManager.closePopUp('popup_incident_risk_association_options');
              }
            );
          } else {
            self.close();
          }
        }
      </script>
    <?
  }
}
//testa se a licen�a utilizada permite acessar o m�dulo de incidente
ISMSLib::testUserPermissionToIncidentMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_incident_risk_association_options.xml");

?>