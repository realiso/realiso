<?php

include_once "include.php";

class SaveEvent extends FWDRunnable {
  public function run(){
    $miOccurrenceId = FWDWebLib::getObject('occurrence_id')->getValue();
    $moOccurrence = new CIOccurrence();
    $mbEdit = $moOccurrence->fetchById($miOccurrenceId); 
    $moOccurrence = new CIOccurrence();
    $moOccurrence->setFieldValue("occurrence_description",FWDWebLib::getObject('occurrence_description')->getValue());
    
    $moDate = FWDWebLib::getObject('date');
    $maTime = explode(":",FWDWebLib::getObject('hour')->getValue());
    $msDateTimestamp = mktime(isset($maTime[0])?$maTime[0]:0, isset($maTime[1])?$maTime[1]:0, 0, $moDate->getMonth(), $moDate->getDay(), $moDate->getYear());
    
    if ($msDateTimestamp <= ISMSLib::ISMSTime()) {    
	    $moOccurrence->setFieldValue("occurrence_date", $msDateTimestamp);
	    
	    if ($miOccurrenceId) {
	      $moCtxUserTest = new CIOccurrence();
	      $moCtxUserTest->testPermissionToEdit($miOccurrenceId); 
	      $moOccurrence->update($miOccurrenceId);
	    }
	    else {
	      $moCtxUserTest = new CIOccurrence();
	      $moCtxUserTest->testPermissionToInsert(); 
	      $miOccurrenceId = $moOccurrence->insert(true);
	    }
	    echo "soWindow = soPopUpManager.getPopUpById('popup_occurrence_edit').getOpener();;
	          if (soWindow.refresh_grid) soWindow.refresh_grid();
	          soPopUpManager.closePopUp('popup_occurrence_edit');";
    }
    else {
    	echo "gobi('date_warning').show();";
    }
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
    $moStartEvent->addAjaxEvent(new SaveEvent('save_event'));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $miOccurrenceId = intval(FWDWebLib::getObject('occurrence_id')->getValue());
    if ($miOccurrenceId) {
      $moCtxUserTest = new CIOccurrence();
      $moCtxUserTest->testPermissionToEdit($miOccurrenceId);

      $moOccurrence = new CIOccurrence();
      $moOccurrence->fetchById($miOccurrenceId);
      FWDWebLib::getObject('occurrence_description')->setValue($moOccurrence->getFieldValue('occurrence_description'));
            
      $msDateTime = explode(' ', ISMSLib::getISMSDate($moOccurrence->getFieldValue('occurrence_date')));      
      FWDWebLib::getObject('date')->setValue($msDateTime[0]);
      FWDWebLib::getObject('hour')->setValue($msDateTime[1]);
    } else {
      $moCtxUserTest = new CIOccurrence();
      $moCtxUserTest->testPermissionToInsert();
      
      // popula data e hora automaticamente
      FWDWebLib::getObject('date')->setTimestamp(ISMSLib::ISMSTime());
      FWDWebLib::getObject('hour')->setValue(date("H", ISMSLib::ISMSTime()) . ":" . date("i", ISMSLib::ISMSTime()));
    }

    //instala a seguran�a de ACL na p�gina
    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        gebi('occurrence_description').focus();
      </script>
    <?
  }
}

//testa se a licen�a utilizada permite acessar o m�dulo de incidente
ISMSLib::testUserPermissionToIncidentMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_occurrence_edit.xml');

?>