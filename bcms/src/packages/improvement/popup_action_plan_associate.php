<?php

include_once 'include.php';
include_once $handlers_ref . 'grid/improvement/QueryGridActionPlanSearch.php';
include_once $classes_isms_ref . 'nonauto/ISMSContextDrawGrid.php';


function getCurrentIds(){
  $maValues = array_filter(explode(':', FWDWebLib::getObject('var_current_ids')->getValue()));
  if(count($maValues)==0) $maValues = array(0);
  return $maValues;
}

class SearchEvent extends FWDRunnable {
  public function run(){
    $moGrid = FWDWebLib::getObject('grid_search');
    $moGrid->execEventPopulate();
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
    $moStartEvent->addAjaxEvent(new SearchEvent('search_event'));

    $maCurrentIds = getCurrentIds();
    $moGrid = FWDWebLib::getObject('grid_search');
    if(FWDWebLib::getObject('var_must_search')->getValue()){
      $moHandler = new QueryGridActionPlanSearch();
      $miNcId = FWDWebLib::getObject('non_conformity_id')->getValue();
      if($miNcId){
        $moNc = new CINonConformity();
        $moNc->fetchById($miNcId);
        switch($moNc->getFieldValue('nc_capability')){
          case NC_NO_CAPABILITY:{
            $moHandler->setTypeAc(ACTION_PLAN_CORRECTIVE_TYPE);
            break;
          }
          case NC_CAPABILITY:{
            $moHandler->setTypeAc(ACTION_PLAN_PREVENTIVE_TYPE);
            break;
          }
        }
      }
      $moHandler->setName(FWDWebLib::getObject('var_name')->getValue());
      $moHandler->setExcludedIds($maCurrentIds);
      $moGrid->setQueryHandler($moHandler);
      $moGrid->setAttrPopulate('true');
      $moGrid->setObjFwdDrawGrid(new ISMSContextDrawGrid('icon-ci_action_plan.gif'));
    }else{
      $moGrid->setAttrPopulate('false');
    }
    $moGrid = FWDWebLib::getObject('grid_current');
    $moHandler = new QueryGridActionPlanSearch();
    $moHandler->setIds($maCurrentIds);
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new ISMSContextDrawGrid('icon-ci_action_plan.gif'));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    FWDWebLib::getObject('var_current_ids')->setValue(FWDWebLib::getObject('current_ids')->getValue());

    if(FWDWebLib::getObject('get_action_plan_ids')->getValue()){
      $moNCAp = new CINcActionPlan();
      $moNCAp->getFieldByAlias('nc_id')->addFilter(new FWDDBFilter('=',FWDWebLib::getObject('non_conformity_id')->getValue()));
      $moNCAp->select();
      $maValues = array();
      while($moNCAp->fetch()){
        $maValues[]=$moNCAp->getFieldValue('ap_id');
      }
      FWDWebLib::getObject('var_current_ids')->setValue(implode(':',$maValues));
    }
    
    $moGrid = FWDWebLib::getObject('grid_current');
    $moHandler = $moGrid->getQueryHandler();
    $moHandler->setIds(getCurrentIds());
    $moGrid->setQueryHandler($moHandler);

    //instala a seguran�a de ACL na p�gina
    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">

        function search(){
          gebi('var_must_search').value = 1;
          gebi('var_name').value = gebi('name').value;
          gobi('grid_search').setPopulate(true);
          trigger_event('search_event',3);
        }

        var soContextsAssociator = new ISMSContextsAssociator('grid_search','grid_current','var_current_ids','grids_pack');
        gebi('name').focus();

      </script>
    <?
  }
}

// Testa se a licen�a utilizada permite acessar este m�dulo
//ISMSLib::testUserPermissionTo%module%Mode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_action_plan_associate.xml');

?>