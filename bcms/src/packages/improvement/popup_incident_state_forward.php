<?php
include_once "include.php";

class StateForwardEvent extends FWDRunnable {
  public function run() {
    $miIncidentId = FWDWebLib::getObject('incident_id')->getValue();
    $moCtxUserTest = new CIIncident();
    $moCtxUserTest->testPermissionToEdit($miIncidentId);
    
    $moIncident = new CIIncident();
    $moIncident->fetchById($miIncidentId);
    
    $maWKFAction = $moIncident->getStateForwardAction();
    $miWKFAction = isset($maWKFAction['confirm'])?$maWKFAction['confirm']:0;
    $moIncident->stateForward($miWKFAction?$miWKFAction:WKF_ACTION_NONE);
    
    echo "self.close();";
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new StateForwardEvent('state_forward_event'));

    //instala a seguran�a de ACL na p�gina
    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
  }
}


class ScreenEvent extends FWDRunnable {
  public function run(){
    $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    $miIncidentId = FWDWebLib::getObject('incident_id')->getValue();
    $moIncident = new CIIncident();
    $moIncident->fetchById($miIncidentId);
    $mbIsApprover = ($miUserId == $moIncident->getApprover());

    $maWKFAction = $moIncident->getStateForwardAction($miIncidentId);
    $miWKFAction = isset($maWKFAction['confirm'])?$maWKFAction['confirm']:0;
    switch($miWKFAction) {
      case WKF_ACTION_SEND_TO_APPROVAL:{
        $msTitle = FWDLanguage::getPHPStringValue('tt_incident_send_to_responsible','Enviar para aprova��o');
        $msMessage = FWDLanguage::getPHPStringValue('st_incident_send_to_responsible',"Deseja enviar o incidente para aprova��o?");
        break;
      }
      case WKF_ACTION_INC_SEND_TO_RESPONSIBLE:{
        $msTitle = FWDLanguage::getPHPStringValue('tt_incident_send_to_responsible','Enviar para Respons�vel');
        $msMessage = FWDLanguage::getPHPStringValue('st_incident_send_to_responsible',"Deseja enviar o incidente para o respons�vel?");
      break;
      }
      case WKF_ACTION_INC_SEND_TO_APP_DISPOSAL:{
        $msTitle = FWDLanguage::getPHPStringValue('tt_incident_send_to_app_disposal','Enviar para Aprova��o de Disposi��o Imediata');
        $msMessage = FWDLanguage::getPHPStringValue('st_incident_send_to_app_disposal',"Deseja enviar o incidente para aprova��o de Disposi��o Imediata?");
      break;
      }
      case WKF_ACTION_INC_SEND_TO_APP_SOLUTION:{
        if($mbIsApprover){
          $msTitle = FWDLanguage::getPHPStringValue('tt_incident_finish','Finalizar o Incidente');
          $msMessage = FWDLanguage::getPHPStringValue('st_incident_finish',"Deseja finalizar o incidente?");
        }else{
          $msTitle = FWDLanguage::getPHPStringValue('tt_incident_send_to_app_solution','Enviar para Aprova��o de Solu��o');
          $msMessage = FWDLanguage::getPHPStringValue('st_incident_send_to_app_solution',"Deseja enviar o incidente para aprova��o de Solu��o?");
        }
      break;
      }
      default:
        return false;
      break;
    }
    FWDWebLib::getObject('confirm_title')->setValue($msTitle);
    FWDWebLib::getObject('confirm_message')->setValue($msMessage);
    
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
  }
}
//testa se a licen�a utilizada permite acessar o m�dulo de incidente
ISMSLib::testUserPermissionToIncidentMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_incident_state_forward.xml");

?>