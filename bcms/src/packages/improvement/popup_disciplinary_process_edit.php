<?php
include_once "include.php";
include_once $handlers_ref . "grid/improvement/QueryGridIncidentUser.php";

class InsertEvent extends FWDRunnable {
  public function run(){
    $moIncidentUser = new CIIncidentUser();
    $miIncidentId = intval(FWDWebLib::getObject('incident_id')->getValue());
    $miUserId = intval(FWDWebLib::getObject('user_id')->getValue());
    
    $msDescription = FWDWebLib::getObject('description')->getValue();
    $moIncidentUser->setFieldValue('incident_id',$miIncidentId);
    $moIncidentUser->setFieldValue('user_id',$miUserId);
    $moIncidentUser->setFieldValue('description',$msDescription);
    $moIncidentUser->insert();
    
    $moHandler = new QueryGridIncidentUser(FWDWebLib::getConnection());
    $moHandler->setIncident(FWDWebLib::getObject('incident_id')->getValue());
    $maCurrentUsers = $moHandler->getCurrentUsers();
    $msCurrentUsers = serialize($maCurrentUsers);
    
    echo "gebi('user_id').value='';
          gobi('user').setValue('');
          gebi('description').value='';
          gebi('exclude_users').value='{$msCurrentUsers}';
          refresh_grid();";
  }
}

class IncidentUserConfirmRemove extends FWDRunnable {
  public function run() {
    $msTitle = FWDLanguage::getPHPStringValue('tt_incident_user_remove','Remover Usu�rio');
    $msMessage = FWDLanguage::getPHPStringValue('st_incident_user_remove',"Voc� tem certeza que deseja remover o usu�rio <b>%user_name%</b> do processo disciplinar do incidente <b>%incident_name%</b>?");
    
    list($miIncidentId,$miUserId) = split(":",FWDWebLib::getObject('selected_incident_user_id')->getValue());
    
    $moIncident = new CIIncident();
    $moIncident->fetchById($miIncidentId);
    $msIncidentName = ISMSLib::truncateString($moIncident->getFieldValue('incident_name'), 45);
    
    $moUser = new ISMSUser();
    $moUser->fetchById($miUserId);
    $msUserName = ISMSLib::truncateString($moUser->getFieldValue('user_name'), 25);

    $msMessage = str_replace('%user_name%',$msUserName,$msMessage);
    $msMessage = str_replace('%incident_name%',$msIncidentName,$msMessage);
    
    $msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
                    "soWindow = soPopUp.getOpener();" .
                    "soWindow.remove_incident_user();";
    
    ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,60);
  }
}

class RemoveIncidentUserEvent extends FWDRunnable {
  public function run() {
    $moIncidentUser = new CIIncidentUser();
    list($miIncidentId,$miUserId) = split(":",FWDWebLib::getObject('selected_incident_user_id')->getValue());
    $moIncidentUser->createFilter($miIncidentId,'incident_id');
    $moIncidentUser->createFilter($miUserId,'user_id');
    $moIncidentUser->delete();
    
    $moHandler = new QueryGridIncidentUser(FWDWebLib::getConnection());
    $moHandler->setIncident(FWDWebLib::getObject('incident_id')->getValue());
    $maCurrentUsers = $moHandler->getCurrentUsers();
    $msCurrentUsers = serialize($maCurrentUsers);
    
    echo "gebi('exclude_users').value='{$msCurrentUsers}';
          refresh_grid();";
  }
}

class SendEmailEvent extends FWDRunnable {
  public function run() {
    $miIncidentId = FWDWebLib::getObject('incident_id')->getValue();
    
    $moHandler = new QueryGridIncidentUser(FWDWebLib::getConnection());
    $moHandler->setIncident($miIncidentId);
    $maCurrentUsers = $moHandler->getCurrentUsers();
    $maCurrentUserNames = array();
    foreach($maCurrentUsers as $miUserId) {
      $moUser = new ISMSUser();
      $moUser->fetchById($miUserId);
      $msUserName = $moUser->getFieldValue('user_name');
      $maCurrentUserNames[] = $msUserName;
    }
    
    if (count($maCurrentUserNames)) {
      $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
      $moAlert = new WKFAlert();
      $moAlert->setFieldValue('alert_context_id', $miIncidentId);
      $moAlert->setFieldValue('alert_creator_id', $miUserId);
      $moAlert->setFieldValue('alert_receiver_id', ISMSLib::getConfigById(USER_DISCIPLINARY_PROCESS_MANAGER));
      $moAlert->setFieldValue('alert_type', WKF_ALERT_DISCIPLINARY_PROCESS);
      $moAlert->setFieldValue('alert_date', ISMSLib::ISMSTime());
      $moAlert->insert(false, false);
      
      if(ISMSLib::getConfigById(GENERAL_EMAIL_ENABLED)){
        $moIncident = new CIIncident();
        $moIncident->fetchById($miIncidentId);
        $msIncidentName = $moIncident->getFieldValue('incident_name');
        
        $msDate = ISMSLib::getISMSDate();
        $moReceiver = new ISMSUser();
        $moReceiver->fetchById(ISMSLib::getConfigById(USER_DISCIPLINARY_PROCESS_MANAGER));
        $msReceiverEmail = $moReceiver->getFieldValue('user_email');
        $moEmailPreferences = new ISMSEmailPreferences();
        $msFormat = $moEmailPreferences->getEmailFormat($moReceiver->getFieldValue('user_id'));
        $msSystemName = ISMSLib::getSystemName();
        $moMailer = new ISMSMailer();
        $moMailer->setFormat($msFormat);
        $moMailer->setTo($msReceiverEmail);
        $moMailer->setSubject("[$msSystemName] " . date('d/m/Y') . ' - '.FWDLanguage::getPHPStringValue('em_disciplinary_process_creation','Cria��o de Processo Disciplinar'));
        $moMailer->addContent('disciplinaryProcess');
        $moMailer->setParameter('users',implode(", ",$maCurrentUserNames));
        $moMailer->setParameter('incident',$msIncidentName);
        $moMailer->setFooterMessage(FWDLanguage::getPHPStringValue('em_disciplinary_process_creation_footer','Para maiores informa��es entre em contato com o security officer.'));
        $moMailer->send();       

        $moIncident = new CIIncident();
        $moIncident->setFieldValue('incident_email_dp_sent',true);
        $moIncident->update($miIncidentId);
      }
    }
    echo "soPopUpManager.closePopUp('popup_disciplinary_process_edit');";
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
    $moStartEvent->addAjaxEvent(new InsertEvent('insert_event'));
    $moStartEvent->addAjaxEvent(new IncidentUserConfirmRemove('incident_user_confirm_remove'));
    $moStartEvent->addAjaxEvent(new RemoveIncidentUserEvent('remove_incident_user_event'));
    $moStartEvent->addAjaxEvent(new SendEmailEvent('send_email_event'));
    
    $moGrid = FWDWebLib::getObject("grid_incident_user");
    $moHandler = new QueryGridIncidentUser(FWDWebLib::getConnection());
    $moHandler->setIncident(FWDWebLib::getObject('incident_id')->getValue());
    $moGrid->setQueryHandler($moHandler);
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $miIncidentId = FWDWebLib::getObject('incident_id')->getValue();
    $moIncident = new CIIncident();
    if (!$moIncident->fetchById($miIncidentId)) return false;
    
    $moHandler = new QueryGridIncidentUser(FWDWebLib::getConnection());
    $moHandler->setIncident(FWDWebLib::getObject('incident_id')->getValue());
    $maCurrentUsers = $moHandler->getCurrentUsers();
    $msCurrentUsers = serialize($maCurrentUsers);
    FWDWebLib::getObject('exclude_users')->setValue($msCurrentUsers);
    
    FWDWebLib::getObject('incident')->setValue($moIncident->getFieldValue('incident_name'));
    if ($moIncident->getFieldValue('incident_email_dp_sent')) {
      FWDWebLib::getObject('warning_email_dp_sent')->setAttrDisplay('true');
    }
    
    //instala a seguran�a de ACL na p�gina
    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        gebi('description').focus();
        
        function refresh_grid() {
          js_refresh_grid('grid_incident_user');
        }

        function set_user(piId, psName) {
          gebi('user_id').value = piId;
          gobi('user').setValue(psName);
        }
        
        function remove_incident_user() {
          trigger_event('remove_incident_user_event',3);
          refresh_grid();
        }
      </script>
    <?
  }
}

//testa se a licen�a utilizada permite acessar o m�dulo de incidente
ISMSLib::testUserPermissionToIncidentMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_disciplinary_process_edit.xml');

?>