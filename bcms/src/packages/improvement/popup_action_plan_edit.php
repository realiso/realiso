<?php

include_once 'include.php';

function get_user_name($piId){
  $moUser = new ISMSUser();
  $moUser->fetchById($piId);
  return $moUser->getName();
}

class GetUserNameEvent extends FWDRunnable {
  public function run(){
    $msUserField = FWDWebLib::getObject('var_user_field')->getValue();
    $miUserId = FWDWebLib::getObject($msUserField)->getValue();
    $msUserName = get_user_name($miUserId);
    echo "gobi('static_$msUserField').setValue('$msUserName');";
  }
}

class SaveEvent extends FWDRunnable {
  public function run() {
    /*teste de consist�ncia nas datas de deadline e de conclus�o do plano de a��o*/
    $miActionPlanId = FWDWebLib::getObject('ap_id')->getValue();
    
    $moAP = new CIActionPlan();
    $moAP->fetchById($miActionPlanId);
    $miState = $moAP->getContextState();
    if(!in_array($miState,array(CONTEXT_STATE_AP_WAITING_MEASUREMENT,CONTEXT_STATE_AP_MEASURED,CONTEXT_STATE_AP_WAITING_CONCLUSION))
        &&
       !ISMSLib::getConfigById(GENERAL_MANUAL_DATA_CONTROL)
    ){
      $miDateTest = ISMSLib::ISMSTime();
      $miDate = mktime (0, 0, 0, date("m",$miDateTest), date("d",$miDateTest),  date("Y",$miDateTest));
      $miDeadLine =  FWDWebLib::getObject('ap_datedeadline')->getTimestamp();
      //transformar para timestamp
      $miConclusion = FWDWebLib::getObject('var_ap_dateconclusion')->getValue();
      if($miDeadLine < $miDate){
        echo "gobi('warning_deadline').show();";
        return;
      }
    }

    

    $moActionPlan = new CIActionPlan();
    $moActionPlan->fillFieldsFromForm();
    
    
    
    
    if(ISMSLib::getConfigById(GENERAL_MANUAL_DATA_CONTROL)){
      if(FWDWebLib::getObject('ap_dateconclusion')->getValue()){
        $moActionPlan->setFieldValue('ap_dateconclusion',FWDWebLib::getObject('ap_dateconclusion')->getTimestamp());
      }
    }else{
      $moActionPlan->setFieldValue('ap_dateconclusion',FWDWebLib::getObject('var_ap_dateconclusion')->getValue());
    }

    $moActionPlan->setFieldValue('ap_actiontype',str_replace(':','',FWDWebLib::getObject('ap_actiontype')->getValue()) );
    $miAPid = $moActionPlan->insertOrUpdate($miActionPlanId); //ja executa os testes de permiss�o de edi��o e inser��o

    /*SE ESTA SENDO CRIADO POR UMA N�O CONFORMIDADE, CRIAR O RELACIONAMENTO*/
     $miNCId = FWDWebLib::getObject('non_conformity_id')->getValue();
     if(($miNCId) && !($miActionPlanId)){
        $moNcAp = new CINcActionPlan();
        $moNcAp->setFieldValue('nc_id',$miNCId);
        $moNcAp->setFieldValue('ap_id',$miAPid);
        $moNcAp->insert();
     }

    /** MUDAN�A DE ESTADO DE CONTEXTO **/
    $moAP = new CIActionPlan();

    echo 'soWindow = self.getOpener();'
        .'if(soWindow.refresh_grid) soWindow.refresh_grid();';

    if (!$moAP->getStateForwardConfirmation($miAPid)) {
      echo 'self.close();';
    }
  }
}

class FinishPAEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $miDateTest = ISMSLib::ISMSTime();
    $miDate = mktime (0, 0, 0, date("m",$miDateTest), date("d",$miDateTest),  date("Y",$miDateTest));
    $msDate = ISMSLib::getISMSShortDate($miDate,true);
    $msDateDeadLine = FWDWebLib::getObject('ap_datedeadline')->getValue();
    echo "
    gebi('var_ap_dateconclusion').value = $miDate;
    gobi('st_data_conclusion').setValue('".$msDate."');
    gobi('st_data_deadline').setValue('".$msDateDeadLine."');
    gobi('vb_finish').hide();
    gebi('vg_data_deadline').style.display= 'none';
    gobi('st_data_deadline').show();
    ";
  }
}


class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
    $moStartEvent->addAjaxEvent(new SaveEvent('save_event'));
    $moStartEvent->addAjaxEvent(new GetUserNameEvent('get_user_name'));
    $moStartEvent->addAjaxEvent(new FinishPAEvent('finish_event'));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $miActionPlanId = FWDWebLib::getObject('ap_id')->getValue();
    $moActionPlan = new CIActionPlan();

    if($miActionPlanId){
      $moActionPlan->fetchById($miActionPlanId);
      $moActionPlan->testPermissionToEdit($miActionPlanId);
      $moActionPlan->fillFormFromFields();

      FWDWebLib::getObject('static_ap_responsible_id')->setValue(get_user_name($moActionPlan->getFieldValue('ap_responsible_id')));
      FWDWebLib::getObject('st_data_deadline')->setValue(FWDWebLib::getObject('ap_datedeadline')->getValue());

      if(ISMSLib::getConfigById(GENERAL_MANUAL_DATA_CONTROL)){
        FWDWebLib::getObject('vg_dateconclusion')->setAttrDisplay("true");
        FWDWebLib::getObject('st_data_conclusion')->setAttrDisplay("false");
        FWDWebLib::getObject('st_data_deadline')->setAttrDisplay("false");
        FWDWebLib::getObject('vg_data_deadline')->setAttrDisplay("true");
        FWDWebLib::getObject('vb_finish')->setShouldDraw(false);
        if($moActionPlan->getFieldValue('ap_dateconclusion')){
          FWDWebLib::getObject('ap_dateconclusion')->setValue(ISMSLib::getISMSShortDate($moActionPlan->getFieldValue('ap_dateconclusion')));
          FWDWebLib::getObject('st_data_conclusion')->setValue(ISMSLib::getISMSShortDate($moActionPlan->getFieldValue('ap_dateconclusion')));
        }
        FWDWebLib::getObject('ap_datedeadline')->setValue(ISMSLib::getISMSShortDate($moActionPlan->getFieldValue('ap_datedeadline')));
      }else{
        if($moActionPlan->getFieldValue('ap_dateconclusion')){
          FWDWebLib::getObject('var_ap_dateconclusion')->setValue(ISMSLib::getTimestamp($moActionPlan->getFieldValue('ap_dateconclusion')));
          FWDWebLib::getObject('st_data_conclusion')->setValue(ISMSLib::getISMSShortDate(FWDWebLib::getObject('var_ap_dateconclusion')->getValue(),true));
          FWDWebLib::getObject('vb_finish')->setShouldDraw(false);
          FWDWebLib::getObject('st_data_deadline')->setAttrDisplay('true');
          FWDWebLib::getObject('vg_data_deadline')->setAttrDisplay('false');
        }
      }
/*

label_ap_dateconclusion
st_data_conclusion
vb_finish

vg_dateconclusion
ap_dateconclusion

*/
    /* SESS�O PARA ABILITAR E DESABILITAR OS CAMPOS DE EDI��O */
        $miState = $moActionPlan->getContextState();
        switch($miState){
          case CONTEXT_STATE_DENIED:
          case CONTEXT_STATE_AP_MEASURED:
          case CONTEXT_STATE_AP_WAITING_MEASUREMENT:{
              if(ISMSLib::getConfigById(GENERAL_MANUAL_DATA_CONTROL)){
                if($moActionPlan->getFieldValue('ap_dateefficiencyrevision')){
                  FWDWebLib::getObject('st_data_deadline')->setAttrDisplay('true');
                  FWDWebLib::getObject('vg_data_deadline')->setAttrDisplay('false');
                  FWDWebLib::getObject('st_data_conclusion')->setAttrDisplay('true');
                  FWDWebLib::getObject('vg_dateconclusion')->setAttrDisplay('false');
                }
              }
              if($miState !=CONTEXT_STATE_AP_WAITING_MEASUREMENT){
                FWDWebLib::getObject('vb_save')->setShouldDraw(false);
              }
              FWDWebLib::getObject('vb_finish')->setShouldDraw(false);
              FWDWebLib::getObject('ap_actiontype_4780')->setAttrDisabled('true');
              FWDWebLib::getObject('ap_actiontype_4781')->setAttrDisabled('true');
              FWDWebLib::getObject('ap_name')->setAttrDisabled('true');
              FWDWebLib::getObject('ap_actionplan')->setAttrDisabled('true');
              FWDWebLib::getObject('vb_responsible')->setShouldDraw(false);
              
            break;
          }
          case CONTEXT_STATE_AP_WAITING_CONCLUSION:
            FWDWebLib::getObject('ap_actiontype_4780')->setAttrDisabled('true');
            FWDWebLib::getObject('ap_actiontype_4781')->setAttrDisabled('true');
            
            if(!ISMSLib::getConfigById(GENERAL_MANUAL_DATA_CONTROL)){
              FWDWebLib::getObject('st_data_deadline')->setAttrDisplay('true');
              FWDWebLib::getObject('vg_data_deadline')->setAttrDisplay('false');
            }
            
            if($moActionPlan->getResponsible() != ISMSLib::getCurrentUserId()){
               FWDWebLib::getObject('vb_finish')->setShouldDraw(false);
            }
          break;
          case CONTEXT_STATE_AP_WAITING_CONCLUSION:{
          }
          default:{
            /*independente do estado, se o plano de a��o estiver relacionado a pelo menos uma NC, 
              desabilitar a altera��o do tipo do plano de a��o*/
              $moNcAp = new CINcActionPlan();
              $moNcAp->createFilter($miActionPlanId, 'ap_id');
              $moNcAp->select();
              if($moNcAp->fetch()){
                FWDWebLib::getObject('ap_actiontype_4780')->setAttrDisabled('true');
                FWDWebLib::getObject('ap_actiontype_4781')->setAttrDisabled('true');
              }
              if($moActionPlan->getResponsible() != ISMSLib::getCurrentUserId()){
                FWDWebLib::getObject('vb_finish')->setShouldDraw(false);
              }
          break;
          }
        }
      /* FIM DA SESS�O PARA ABILITAR E DESABILITAR OS CAMPOS DE EDI��O */

    }else{
      $moActionPlan->testPermissionToInsert();
      $moUser = new ISMSUser();
      $moUser->fetchById(ISMSLib::getCurrentUserId());
      FWDWebLib::getObject('ap_responsible_id')->setValue(ISMSLib::getCurrentUserId());
      FWDWebLib::getObject('static_ap_responsible_id')->setValue($moUser->getName());

      FWDWebLib::getObject('vb_finish')->setShouldDraw(false);

      $miNCId = FWDWebLib::getObject('non_conformity_id')->getValue();
      if($miNCId){
        $moNC = new NonConformity();
        $moNC->fetchById($miNCId);

        switch($moNC->getFieldValue('ncapability')){
          case NC_NO_CAPABILITY:{
            FWDWebLib::getObject('ap_actiontype_4780')->setAttrDisabled('true');
            FWDWebLib::getObject('ap_actiontype_4781')->setAttrDisabled('true');
            FWDWebLib::getObject('ap_actiontype_4780')->setAttrCheck('true');
            break;
          }
          case NC_CAPABILITY:{
            FWDWebLib::getObject('ap_actiontype_4780')->setAttrDisabled('true');
            FWDWebLib::getObject('ap_actiontype_4781')->setAttrDisabled('true');
            FWDWebLib::getObject('ap_actiontype_4781')->setAttrCheck('true');
            break;
          }
        }
      }
    }
    
    if (FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(POLICY_MODE)) { //Se tiver o m�dulo de pol�tica, esconde o campo de documento. 
      FWDWebLib::getObject('policy_mode')->setShouldDraw(false);
      FWDWebLib::getObject('footer')->getObjFWDBox()->setAttrTop(290);
      FWDWebLib::getObject('dialog')->getObjFWDBox()->setAttrHeight(345);
    }
    
    //instala a seguran�a de ACL na p�gina
    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        gebi('ap_name').focus();

        function set_user(piId){
          gebi(gebi('var_user_field').value).value = piId;
          trigger_event('get_user_name',3);
        }
      </script>
    <?
  }
}

//testa se a licen�a utilizada permite acessar o m�dulo de incidente
ISMSLib::testUserPermissionToIncidentMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_action_plan_edit.xml');

?>