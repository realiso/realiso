<?php
include_once "include.php";

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){ 
    $miContextType = FWDWebLib::getObject('context_type')->getValue();
    $miContextId = FWDWebLib::getObject('context_id')->getValue();
    if ($miContextType) {
      switch ($miContextType) {
        case CONTEXT_CI_OCCURRENCE:
          FWDWebLib::getObject('tab_group')->setAttrLoadTabItemIndex(1);
          $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
        break;
        case CONTEXT_CI_INCIDENT:
          FWDWebLib::getObject('tab_group')->setAttrLoadTabItemIndex(2);
          $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
        break;
        case CONTEXT_CI_NON_CONFORMITY:
          FWDWebLib::getObject('tab_group')->setAttrLoadTabItemIndex(3);
          $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
          $moSession->addAttribute('processid');
          $moSession->setAttrProcessId($miContextId);
        break;  
        case CONTEXT_CI_ACTION_PLAN:
          FWDWebLib::getObject('tab_group')->setAttrLoadTabItemIndex(4);
        break;
        
      }
    }
       
    //instala a seguran�a de ACL na p�gina
    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
  }
}

//testa se a licen�a utilizada permite acessar o m�dulo de incidente
ISMSLib::testUserPermissionToIncidentMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("tab_continual_improvement.xml");
?>