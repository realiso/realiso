<?php
include_once "include.php";
include_once $handlers_ref . "select/improvement/QuerySelectNCProcess.php";

class ContextEvent extends FWDRunnable {
  public function run(){
  	$miTaskId = FWDWebLib::getObject('task_id')->getValue();

    //teste para verificar se o sistema n�o est� sendo hakeado
    $moTaskTest = new WKFTask();
    $moTaskTest->fetchById($miTaskId);
    $moTaskTest->testPermissionToExecute($miTaskId);
    $miContextId = $moTaskTest->getFieldValue('task_context_id');
    $msJustification = FWDWebLib::getObject('task_justification')->getValue();    
    $miAction = FWDWebLib::getObject('action')->getValue(); 
  	
  	$moContextObject = new ISMSContextObject();
  	$moContext = $moContextObject->getContextObjectByContextId($miContextId); 	
  	$moContext->fetchById($miContextId);	
		$moContext->stateForward($miAction, $msJustification);	
		
		$moTask = new WKFTask();		
		$moTask->setFieldValue('task_date_accomplished', ISMSLib::ISMSTime());		
		$moTask->setFieldValue('task_is_visible', 0);	
		$moTask->update($miTaskId);
		
		echo "soWindow = soPopUpManager.getPopUpById('popup_non_conformity_ap_approval_task').getOpener();"
	        ."if (soWindow.refresh_grids)"
	        ."  soWindow.refresh_grids();"
	        ."soPopUpManager.closePopUp('popup_non_conformity_ap_approval_task');";
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new ContextEvent('context_event'));
  }
}

function truncate($psString, $piChars) {
  if (strlen($psString) > $piChars)
    return substr($psString,0,$piChars-3)."...";
  else
    return $psString;
}

class ScreenEvent extends FWDRunnable {
  public function run(){    
    $mbDumpHTML = true;
    $miTaskId = FWDWebLib::getObject('task_id')->getValue();
    $moTask = new WKFTask();
    $moTask->fetchById($miTaskId);    
    
    //teste para verificar se o sistema n�o est� sendo hakeado
    $moTask->testPermissionToExecute($miTaskId);
    
    FWDWebLib::getObject('task_date')->setValue(ISMSLib::getISMSDate($moTask->getFieldValue('task_date_created')));
    
    $moUser = new ISMSUser();
    $moUser->fetchById($moTask->getFieldValue('task_creator_id'));    
    FWDWebLib::getObject('task_inquirer')->setValue($moUser->getFieldValue('user_name'));
    
    FWDWebLib::getObject('task_name')->setValue(ISMSActivity::getDescription($moTask->getFieldValue('task_activity')));
    
    $miNCId = FWDWebLib::getObject('nc_id')->getValue();
    $moNonConformity = new CINonConformity();
    if($moNonConformity->fetchById($moTask->getFieldValue('task_context_id'))){
  		$msNonConformityName = truncate($moNonConformity->getFieldValue('nc_name'),35);
      $msCause = truncate($moNonConformity->getFieldValue('nc_cause'),35);
      $msActionPlan = truncate($moNonConformity->getFieldValue('nc_action_plan'),35);
      $msDeadline = ISMSLib::getISMSShortDate($moNonConformity->getFieldValue('nc_date_deadline'));
      
      $msNCLink = "<a href='javascript:open_visualize({$miNCId},".CONTEXT_CI_NON_CONFORMITY.",\"".uniqid()."\")'>".$msNonConformityName."</a>";

      FWDWebLib::getObject('nc_name')->setAttrStringNoEscape(true);
      FWDWebLib::getObject('nc_name')->setValue($msNCLink);
      FWDWebLib::getObject('nc_cause')->setAttrStringNoEscape(true);
      FWDWebLib::getObject('nc_cause')->setValue($msCause);
      FWDWebLib::getObject('nc_action_plan')->setAttrStringNoEscape(true);
      FWDWebLib::getObject('nc_action_plan')->setValue($msActionPlan);
      FWDWebLib::getObject('nc_deadline')->setAttrStringNoEscape(true);
      FWDWebLib::getObject('nc_deadline')->setValue($msDeadline);      
    }else{
      $mbDumpHTML = false;
    }
    if($mbDumpHTML){
        FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    }else{
        return FWDLanguage::getPHPStringValue('st_denied_permission_to_execute_task','Voc� n�o tem permiss�o para executar a tarefa ');
    }
    ?>
      <script language="javascript">
        gebi('task_justification').focus();
      </script>
    <?   
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_non_conformity_ap_approval_task.xml");
?>