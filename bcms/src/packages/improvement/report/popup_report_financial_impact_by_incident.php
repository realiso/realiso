<?php
set_time_limit(3600);
include_once "include.php";

class IncidentLevelIterator extends FWDReportLevelIterator {  

  public function __construct(){
    parent::__construct('incident_id');
  }
	
  public function fetch(FWDDBDataSet $poDataSet){
    $msIcon = 'icon-ci_incident.png';
    $msName = $poDataSet->getFieldByAlias('incident_name')->getValue();        
    FWDWebLib::getObject('incident_icon')->setAttrSrc(FWDWebLib::getInstance()->getGfxRef().$msIcon);
    FWDWebLib::getObject('incident_name')->setValue($msName);   
  }
}

class ClassificationLevelIterator extends FWDReportLevelIterator {

  public function __construct(){
    parent::__construct("classification_id");
  }

  public function fetch(FWDDBDataSet $poDataSet){
    $msCostName = $poDataSet->getFieldByAlias("classification_name")->getValue();		
		$miValue = $poDataSet->getFieldByAlias("classification_value")->getValue();		
		
	$moConfig = new ISMSConfig();
    $msCurrency = constant($moConfig->getConfig(GENERAL_CURRENCY_IDENTIFIER));
    if (!$msCurrency) $msCurrency = '$';
    FWDWebLib::getObject('classification_name')->setValue($msCostName.":");    
    FWDWebLib::getObject('classification_value')->setValue($msCurrency.number_format($miValue,2,'.',','));
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();    
    $moWebLib->getObject("level_incident")->setLevelIterator(new IncidentLevelIterator());
    $moWebLib->getObject("level_classification")->setLevelIterator(new ClassificationLevelIterator());
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport($moWebLib->getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    $moComponent->setReportFilter($moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter());
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('mx_financial_impact_by_incident','Impacto Financeiro por Incidente'));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_report_financial_impact_by_incident.xml");
?>