<?php
set_time_limit(3600);
include_once 'include.php';

class ResponsibleLevelIterator extends FWDReportLevelIterator {  
	public function __construct(){
    parent::__construct('responsible_id');
  }

  public function fetch(FWDDBDataSet $poDataSet){
    $msIcon = 'icon-user.png';
    $msName = $poDataSet->getFieldByAlias('responsible_name')->getValue();        
    FWDWebLib::getObject('responsible_icon')->setAttrSrc(FWDWebLib::getInstance()->getGfxRef().$msIcon);
    FWDWebLib::getObject('responsible_name')->setValue($msName);
  }
}

class IncidentLevelIterator extends FWDReportLevelIterator {
  public function __construct(){
    parent::__construct('incident_id');
  }

  public function fetch(FWDDBDataSet $poDataSet){
  	$msIcon = 'icon-ci_incident.png';
    $msName = $poDataSet->getFieldByAlias('incident_name')->getValue();        
    FWDWebLib::getObject('incident_icon')->setAttrSrc(FWDWebLib::getInstance()->getGfxRef().$msIcon);
    FWDWebLib::getObject('incident_name')->setValue($msName);
  }
}


class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    FWDWebLib::getObject('level_responsible')->setLevelIterator(new ResponsibleLevelIterator());
    FWDWebLib::getObject('level_incident')->setLevelIterator(new IncidentLevelIterator());
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport(FWDWebLib::getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());    
    $moComponent->setReportFilter(FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter());    
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('mx_evidence_collection','Coleta de EvidÍncias'));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_report_incident_by_responsible.xml');
?>