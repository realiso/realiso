<?php
set_time_limit(3600);
include_once "include.php";

class IncidentLevelIterator extends FWDReportLevelIterator {  

  public function __construct(){
    parent::__construct("incident_id");
  }
	
  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msGfxRef = $moWebLib->getGfxRef();
    $msIncidentName = $poDataSet->getFieldByAlias("incident_name")->getValue();
    $msIcon = $msGfxRef . 'icon-ci_incident.png';
    $moWebLib->getObject('incident_icon')->setAttrSrc($msIcon);
    $moWebLib->getObject('incident_name')->setValue($msIncidentName);
  }
}

class ContextLevelIterator extends FWDReportLevelIterator {

	protected $ciContextNameWidth = 0;

  public function __construct(){
    parent::__construct("context_id");
    $this->ciContextNameWidth = FWDWebLib::getObject('context_name')->getAttrWidth();
  }
  
  public function changedLevel(FWDDBDataSet $poDataSet){
    return true;
  }

  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msGfxRef = $moWebLib->getGfxRef();

		$miContextType = $poDataSet->getFieldByAlias("context_type")->getValue();		
    $msContextName = $poDataSet->getFieldByAlias("context_name")->getValue();
    $miContextValue = $poDataSet->getFieldByAlias("context_value")->getValue();    
		
		$msContextIcon = '';
		$miContextTypeWidth = 0;		
		switch ($miContextType) {
			
			case CONTEXT_PROCESS:
				$msContextIcon = 'process';
				$miContextTypeWidth = 62;
			break;
			
			case CONTEXT_ASSET:
				$msContextIcon = 'asset';
				$miContextTypeWidth = 38;
			break;
			
			case CONTEXT_RISK:
				$msContextIcon = 'risk';
				$miContextTypeWidth = 40;
			break;
			
			case CONTEXT_CONTROL:				
				$msContextIcon = 'control';
				$miContextTypeWidth = 59;
			break;
		}
		
		if ($miContextType == CONTEXT_CONTROL) {
			if ($miContextValue) $msIcon = $msGfxRef . 'icon-control.png';
    	else $msIcon = $msGfxRef . 'icon-control_red.png';
		}	
    else {
    	$msIcon = $msGfxRef . "icon-{$msContextIcon}_" . RMRiskConfig::getRiskColor($miContextValue) . '.png';
    }
            
    $moContextObject = new ISMSContextObject();
    $moContext = $moContextObject->getContextObject($miContextType);
		$msContextLabel = FWDWebLib::convertToISO($moContext->getLabel());  
		
		/*
		 * Ajusta a largura devido ao fato dos labels dos
		 * contextos possuirem tamanhos diferentes
		 */
		$moWebLib->getObject('context_type')->setAttrWidth($miContextTypeWidth);
		$moWebLib->getObject('context_name')->setAttrWidth($this->ciContextNameWidth-$miContextTypeWidth);
		
    $moWebLib->getObject('context_icon')->setAttrSrc($msIcon);
    $moWebLib->getObject('context_type')->setValue($msContextLabel.":");        
    $moWebLib->getObject('context_name')->setValue($msContextName);    
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $moWebLib->getObject("level_incident")->setLevelIterator(new IncidentLevelIterator());
    $moWebLib->getObject("level_context")->setLevelIterator(new ContextLevelIterator());
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport($moWebLib->getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    $moComponent->setReportFilter($moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter());
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('mx_elements_affected_by_incident','Elementos afetados por Incidentes'));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_report_elements_affected_by_incident.xml");
?>