<?php
set_time_limit(3600);
include_once 'include.php';

class OccurrenceLevelIterator extends FWDReportLevelIterator {  
	public function __construct(){
    parent::__construct('occur_id');
  }

  public function fetch(FWDDBDataSet $poDataSet){
    $msIcon = 'icon-ci_occurrence.png';
    $msDescription = $poDataSet->getFieldByAlias('occur_description')->getValue();        
    FWDWebLib::getObject('occur_icon')->setAttrSrc(FWDWebLib::getInstance()->getGfxRef().$msIcon);
    FWDWebLib::getObject('occur_description')->setValue($msDescription);
  }
}

class InfoLevelIterator extends FWDReportLevelIterator {  
	public function __construct(){
    parent::__construct('occur_state');
  }

  public function fetch(FWDDBDataSet $poDataSet){    
    $miState = $poDataSet->getFieldByAlias('occur_state')->getValue();
    $msState = ISMSContextObject::getContextStateAsString($miState);        
    $msCreator = $poDataSet->getFieldByAlias('occur_creator')->getValue();
    FWDWebLib::getObject('occur_creator')->setValue($msCreator);
    FWDWebLib::getObject('occur_state')->setValue($msState);
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    FWDWebLib::getObject('level_occurrence')->setLevelIterator(new OccurrenceLevelIterator());
    FWDWebLib::getObject('level_info')->setLevelIterator(new InfoLevelIterator());
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport(FWDWebLib::getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());    
    $moComponent->setReportFilter(FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter());    
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('mx_occurrence_accompaniment','Acompanhamento de Ocorrência'));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_report_occurrence_accompaniment.xml');
?>