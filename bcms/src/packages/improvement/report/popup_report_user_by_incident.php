<?php
set_time_limit(3600);
include_once 'include.php';

class IncidentLevelIterator extends FWDReportLevelIterator {
  public function __construct(){
    parent::__construct('incident_id');
  }

  public function fetch(FWDDBDataSet $poDataSet){
  	$msIcon = 'icon-ci_incident.png';
    $msName = $poDataSet->getFieldByAlias('incident_name')->getValue();        
    FWDWebLib::getObject('incident_icon')->setAttrSrc(FWDWebLib::getInstance()->getGfxRef().$msIcon);
    FWDWebLib::getObject('incident_name')->setValue($msName);
  }
}

class UserLevelIterator extends FWDReportLevelIterator {  
	public function __construct(){
    parent::__construct('user_id');
  }

  public function fetch(FWDDBDataSet $poDataSet){
    $msIcon = 'icon-user.png';
    $msName = $poDataSet->getFieldByAlias('user_name')->getValue();
    $msDescription = $poDataSet->getFieldByAlias('incident_user_description')->getValue();    
    FWDWebLib::getObject('user_icon')->setAttrSrc(FWDWebLib::getInstance()->getGfxRef().$msIcon);
    FWDWebLib::getObject('user_name')->setValue($msName);
    FWDWebLib::getObject('user_description')->setValue($msDescription);
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    FWDWebLib::getObject('level_incident')->setLevelIterator(new IncidentLevelIterator());
    FWDWebLib::getObject('level_user')->setLevelIterator(new UserLevelIterator());
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport(FWDWebLib::getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());    
    $moComponent->setReportFilter(FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter());    
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('mx_user_by_incident','Processo Disciplinar'));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_report_user_by_incident.xml');
?>