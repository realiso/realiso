<?php
set_time_limit(3600);
include_once 'include.php';

class ProcessLevelIterator extends FWDReportLevelIterator {
  public function __construct(){
    parent::__construct('process_id');
  }

  public function fetch(FWDDBDataSet $poDataSet){
  	$miProcessValue = $poDataSet->getFieldByAlias("process_value")->getValue();		
		$msIcon = FWDWebLib::getInstance()->getGfxRef() . 'icon-process_' . RMRiskConfig::getRiskColor($miProcessValue) . '.png'; 
    $msName = $poDataSet->getFieldByAlias('process_name')->getValue();        
    FWDWebLib::getObject('process_icon')->setAttrSrc($msIcon);
    FWDWebLib::getObject('process_name')->setValue($msName);
  }
}

class NCLevelIterator extends FWDReportLevelIterator {  
	public function __construct(){
    parent::__construct('nc_id');
  }

  public function fetch(FWDDBDataSet $poDataSet){
  	$msNCName = $poDataSet->getFieldByAlias('nc_name')->getValue();
  	$msControlName = $poDataSet->getFieldByAlias("control_name")->getValue();    
    $mbControlIsActive = $poDataSet->getFieldByAlias("control_is_active")->getValue();
    
    if ($mbControlIsActive) $msControlIcon = 'icon-control.png';
    else $msControlIcon = 'icon-control_red.png';  	
    $msNCIcon = 'icon-ci_nonconformity.png';
        
    FWDWebLib::getObject('nc_icon')->setAttrSrc(FWDWebLib::getInstance()->getGfxRef().$msNCIcon);
    FWDWebLib::getObject('nc_name')->setValue($msNCName);
    FWDWebLib::getObject('control_icon')->setAttrSrc(FWDWebLib::getInstance()->getGfxRef().$msControlIcon);
    FWDWebLib::getObject('control_name')->setValue($msControlName);
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    FWDWebLib::getObject('level_process')->setLevelIterator(new ProcessLevelIterator());
    FWDWebLib::getObject('level_nc')->setLevelIterator(new NCLevelIterator());
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport(FWDWebLib::getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());    
    $moComponent->setReportFilter(FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter());    
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('mx_control_nonconformity_by_process','N�o Conformidade de Controle por Processo'));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_report_control_nonconformity_by_process.xml');
?>