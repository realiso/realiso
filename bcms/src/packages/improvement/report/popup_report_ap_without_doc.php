<?php
set_time_limit(3600);
include_once "include.php";

class APLevelIterator extends FWDReportLevelIterator {

  public function __construct(){
    parent::__construct("ap_id");
  }
  
  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msGfxRef = $moWebLib->getGfxRef();
   
    $msAPName = $poDataSet->getFieldByAlias("ap_name")->getValue();
    $msAPResponsibleName = $poDataSet->getFieldByAlias("responsible_name")->getValue();
    $msIcon = $msGfxRef . 'icon-ci_action_plan.png';

    $moWebLib->getObject('ap_icon')->setAttrSrc($msIcon);
    $moWebLib->getObject('ap_name')->setValue($msAPName);
    $moWebLib->getObject('ap_responsible_name')->setValue($msAPResponsibleName);
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $moWebLib->getObject("level_ap")->setLevelIterator(new APLevelIterator());
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport($moWebLib->getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    $moComponent->setReportFilter($moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter());
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('rs_ap_without_docs', "Planos de A��o sem Documentos"));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_report_ap_without_doc.xml");
?>