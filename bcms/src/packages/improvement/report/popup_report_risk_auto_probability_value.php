<?php
set_time_limit(3600);
include_once 'include.php';

class RiskLevelIterator extends FWDReportLevelIterator {  
	public function __construct(){
    parent::__construct('risk_id');
  }

  public function fetch(FWDDBDataSet $poDataSet){    
    $msRiskName = $poDataSet->getFieldByAlias("risk_name")->getValue();    
		$miRiskIsAccepted = $poDataSet->getFieldByAlias("risk_accept_mode")->getValue();		
		$miRiskResidualValue = $poDataSet->getFieldByAlias("risk_residual_value")->getValue();
		$miPeriod = $poDataSet->getFieldByAlias("schedule_period")->getValue();
		$miValue = $poDataSet->getFieldByAlias("schedule_value")->getValue();
				    
    if ($miRiskIsAccepted) $msIcon = 'icon-risk_green.png';
    else $msIcon = 'icon-risk_' . RMRiskConfig::getRiskColor($miRiskResidualValue) . '.png';

		$msPeriodicity = ISMSLib::getPeriodicity($miValue, $miPeriod);

    FWDWebLib::getObject('risk_icon')->setAttrSrc(FWDWebLib::getInstance()->getGfxRef().$msIcon);    
    FWDWebLib::getObject('risk_name')->setValue($msRiskName);
    FWDWebLib::getObject('incident_amount')->setValue($poDataSet->getFieldByAlias("incident_count")->getValue());
    if ($poDataSet->getFieldByAlias("schedule_date")->getValue()) FWDWebLib::getObject('last_check')->setValue(ISMSLib::getISMSShortDate($poDataSet->getFieldByAlias("schedule_date")->getValue()));
    else FWDWebLib::getObject('last_check')->setValue('---');
    FWDWebLib::getObject('periodicity')->setValue($msPeriodicity);
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    FWDWebLib::getObject('level_risk')->setLevelIterator(new RiskLevelIterator()); 
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport(FWDWebLib::getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());    
    $moComponent->setReportFilter(FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter());    
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('mx_risk_auto_probability_value','C�lculo Autom�tico de Probabilidade dos Riscos'));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_report_risk_auto_probability_value.xml');
?>