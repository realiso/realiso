<?php
set_time_limit(3000);
include_once "include.php";

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();

    $msLink = '';
    $msPopup = $moWebLib->getObject('popup')->getValue();
    $msLink = "popup_{$msPopup}.php";

    $msIframe = '<iframe name="report" framed="0" frameborder=0 autoScroll="VSCROLLBAR" style="cursor:wait;margin-left:10px;" height="100%" width="100%" src="clock.php?'.$msLink.'" ></iframe>';

    $moReportDiv = new FWDDiv(new FWDBox(0,0,500,730));
    $moReportDiv->setName("report_div");
    $moReportDiv->setValue($msIframe);

    $moWebLib->getObject('dialog')->addObjFWDView($moReportDiv);
    $moWebLib->dump_html($moWebLib->getObject('dialog'));
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_report.xml");
?>