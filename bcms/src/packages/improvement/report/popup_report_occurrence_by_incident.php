<?php
set_time_limit(3600);
include_once 'include.php';

class IncidentLevelIterator extends FWDReportLevelIterator {
  public function __construct(){
    parent::__construct('incident_id');
  }

  public function fetch(FWDDBDataSet $poDataSet){
  	$moContext = new ISMSContextObject();
  	$msIcon = 'icon-ci_incident.png';
  	$msName = $poDataSet->getFieldByAlias('incident_name')->getValue();
  	$msResponsible = $poDataSet->getFieldByAlias('responsible_name')->getValue();
  	$msIncDate = ISMSLib::getISMSDate($poDataSet->getFieldByAlias('incident_date')->getValue());
  	$msIncStatus = $moContext->getContextStateAsString($poDataSet->getFieldByAlias('incident_status')->getValue());
       
  	FWDWebLib::getObject('incident_icon')->setAttrSrc(FWDWebLib::getInstance()->getGfxRef().$msIcon);
  	FWDWebLib::getObject('incident_name')->setValue($msName);
  	FWDWebLib::getObject('responsible_name')->setValue($msResponsible);
  	FWDWebLib::getObject('incident_date')->setValue($msIncDate);
  	FWDWebLib::getObject('incident_status')->setValue($msIncStatus);
  }
}

class OccurrenceLevelIterator extends FWDReportLevelIterator {  
	public function __construct(){
    parent::__construct('occurrence_id');
  }

  public function fetch(FWDDBDataSet $poDataSet){
    $msIcon = 'icon-ci_occurrence.png';
    $msDescription = $poDataSet->getFieldByAlias('occurrence_description')->getValue();    
    $msOcDate = ISMSLib::getISMSDate($poDataSet->getFieldByAlias('occurrence_date')->getValue());
    FWDWebLib::getObject('occurrence_icon')->setAttrSrc(FWDWebLib::getInstance()->getGfxRef().$msIcon);
    FWDWebLib::getObject('occurrence_description')->setValue($msDescription);
    FWDWebLib::getObject('occurrence_date')->setValue($msOcDate);
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    FWDWebLib::getObject('level_incident')->setLevelIterator(new IncidentLevelIterator());
    FWDWebLib::getObject('level_occurrence')->setLevelIterator(new OccurrenceLevelIterator());
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport(FWDWebLib::getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());    
    $moComponent->setReportFilter(FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter());    
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('mx_occurrence_by_incident','Ocorrências por Incidente'));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_report_occurrence_by_incident.xml');
?>