<?php
set_time_limit(3600);
include_once 'include.php';

class NCLevelIterator extends FWDReportLevelIterator {  
	public function __construct(){
    parent::__construct('nc_id');
  }

  public function fetch(FWDDBDataSet $poDataSet){
    $msIcon = 'icon-ci_nonconformity.png';
    $msName = $poDataSet->getFieldByAlias('nc_name')->getValue();        
    $msResponsible = $poDataSet->getFieldByAlias('nc_responsible_name')->getValue() ? $poDataSet->getFieldByAlias('nc_responsible_name')->getValue() : FWDLanguage::getPHPStringValue('rs_not_defined','N�o definido');
    FWDWebLib::getObject('nc_icon')->setAttrSrc(FWDWebLib::getInstance()->getGfxRef().$msIcon);
    FWDWebLib::getObject('nc_name')->setValue($msName);
    FWDWebLib::getObject('nc_responsible_name')->setValue($msResponsible);
  }
}

class APLevelIterator extends FWDReportLevelIterator {  
	public function __construct(){
    parent::__construct('action_plan_id');
  }

  public function fetch(FWDDBDataSet $poDataSet){
    $msIcon = 'icon-ci_action_plan.png';
    $miState = $poDataSet->getFieldByAlias('action_plan_state')->getValue();
    $msState = ISMSContextObject::getContextStateAsString($miState);
    $msName = $poDataSet->getFieldByAlias('action_plan_name')->getValue();
    $msResp = $poDataSet->getFieldByAlias('action_plan_responsible_name')->getValue();
    
    if ($poDataSet->getFieldByAlias('date_deadline')->getValue()) $msDeadline = ISMSLib::getISMSShortDate($poDataSet->getFieldByAlias('date_deadline')->getValue());
    else $msDeadline = '---';
    if ($poDataSet->getFieldByAlias('date_conclusion')->getValue()) $msConclusion = ISMSLib::getISMSShortDate($poDataSet->getFieldByAlias('date_conclusion')->getValue());
    else $msConclusion = '---';
    
    FWDWebLib::getObject('ap_icon')->setAttrSrc(FWDWebLib::getInstance()->getGfxRef().$msIcon);
    FWDWebLib::getObject('ap_state')->setValue($msState);
    FWDWebLib::getObject('action_plan_name')->setValue($msName);
    FWDWebLib::getObject('plan_responsible_name')->setValue($msResp);    
    FWDWebLib::getObject('deadline')->setValue($msDeadline);
    FWDWebLib::getObject('conclusion')->setValue($msConclusion);
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    FWDWebLib::getObject('level_nc')->setLevelIterator(new NCLevelIterator());
    FWDWebLib::getObject('level_ap')->setLevelIterator(new APLevelIterator());
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport(FWDWebLib::getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());    
    $moComponent->setReportFilter(FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter());    
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('mx_nonconformity_accompaniment','Acompanhamento de N�o Conformidade'));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_report_nonconformity_accompaniment.xml');
?>