<?php
include_once 'include.php';
include_once $handlers_ref . "grid/QueryGridUserTasks.php";

class ExecuteTaskEvent extends FWDRunnable {

  protected function getAction($piIncidentState){
    return 0;
  }

  public function run(){
    $miTaskId = FWDWebLib::getObject('task_id')->getValue();
    $moTask = new WKFTask();
    $moTask->fetchById($miTaskId);
   
    //teste para verificar se o sistema n�o est� sendo hakeado
    $moTask->testPermissionToExecute($miTaskId);
    
    $miIncidentId = $moTask->getFieldValue('task_context_id');
    $moIncident = new CIIncident();
    $moIncident->fetchById($miIncidentId);
    $miState = $moIncident->getContextState();
    $moIncident->stateForward($this->getAction($miState));
    
    $moTask = new WKFTask();
    $moTask->setFieldValue('task_date_accomplished', ISMSLib::ISMSTime());
    $moTask->setFieldValue('task_is_visible', 0);
    $moTask->update($miTaskId);

    $msNextTaskJS = "";
		$mbOpenNextTask = (bool)array_filter(explode(':',FWDWebLib::getObject('next_task_controller')->getValue()));
		if ($mbOpenNextTask) {
		  $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
		  $moHandlerTask = new QueryGridUserTasks(FWDWebLib::getConnection());
      $moHandlerTask->setUser($miUserId);
      $moHandlerTask->setOrderBy('task_date_created','-');
      $moHandlerTask->makeQuery();
      $moHandlerTask->executeQuery();
      if ($moHandlerTask->fetch()) {
  		  $msNextTaskJS = 'soWindow.setTimeout("'.ISMSLib::getTaskPopup($moHandlerTask->getFieldValue('task_id'),
  		                                        $moHandlerTask->getFieldValue('task_activity'),
  		                                        $moHandlerTask->getFieldValue('task_context_id'),
  		                                        $moHandlerTask->getFieldValue('task_context_type'),
  		                                        2).',1");';
      }
		}
		
    echo "soWindow = soPopUpManager.getPopUpById('popup_incident_task_view').getOpener();"
        ."if(soWindow.refresh_grids) soWindow.refresh_grids();"
        .$msNextTaskJS
        ."soPopUpManager.closePopUp('popup_incident_task_view');"
        ."soWindow=null;";
  }

}

class ApproveEvent extends ExecuteTaskEvent {
  protected function getAction($piIncidentState){
    return ($piIncidentState==CONTEXT_STATE_INC_PENDANT_DISPOSAL?WKF_ACTION_INC_APP_DISPOSAL:WKF_ACTION_INC_APP_SOLUTION);
  }
}

class DenyEvent extends ExecuteTaskEvent {
  protected function getAction($piIncidentState){
    return ($piIncidentState==CONTEXT_STATE_INC_PENDANT_DISPOSAL?WKF_ACTION_INC_DENY_DISPOSAL:WKF_ACTION_INC_DENY_SOLUTION);
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
    $moStartEvent->addAjaxEvent(new ApproveEvent('approve'));
    $moStartEvent->addAjaxEvent(new DenyEvent('deny'));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $miAutoTaskOpen = FWDWebLib::getObject('auto_task_open')->getValue();
    if (!$miAutoTaskOpen) FWDWebLib::getObject('next_task')->setShouldDraw(false);
    if ($miAutoTaskOpen==2) FWDWebLib::getObject('next_task_controller')->checkItem(1);
    
    $miTaskId = FWDWebLib::getObject('task_id')->getValue();
    $moTask = new WKFTask();
    $moTask->fetchById($miTaskId);
    
    //teste para verificar se o sistema n�o est� sendo hakeado
    $moTask->testPermissionToExecute($miTaskId);
    
    FWDWebLib::getObject('task_date')->setValue(ISMSLib::getISMSDate($moTask->getFieldValue('task_date_created')));
    $moUser = new ISMSUser();
    $moUser->fetchById($moTask->getFieldValue('task_creator_id'));
    FWDWebLib::getObject('task_inquirer')->setValue($moUser->getFieldValue('user_name'));
    FWDWebLib::getObject('task_name')->setValue(ISMSActivity::getDescription($moTask->getFieldValue('task_activity')));
    
    $miIncidentId = $moTask->getFieldValue('task_context_id');
    $moIncident = new CIIncident();
    $moIncident->fetchById($miIncidentId);
    $miState = $moIncident->getContextState();
    
    if($miState==CONTEXT_STATE_INC_PENDANT_DISPOSAL){
      $msDateField = ISMSLib::getISMSShortDate($moIncident->getFieldValue('incident_date_limit'));
      $msTextField = str_replace("\n",'<br/>',$moIncident->getFieldValue('incident_disposal_description'));
      $msLabelTextField = FWDLanguage::getPHPStringValue('lb_incident_immediate_disposal_cl','Disposi��o Imediata:');
    }else{
      $msDateField = ISMSLib::getISMSShortDate($moIncident->getFieldValue('incident_date_finish'));
      $msTextField = str_replace("\n",'<br/>',$moIncident->getFieldValue('incident_solution_description'));
      $msLabelTextField = FWDLanguage::getPHPStringValue('lb_incident_solution_cl','Solu��o:');
    }
    FWDWebLib::getObject('date_field')->setValue($msDateField);
    FWDWebLib::getObject('text_field')->setValue($msTextField);
    FWDWebLib::getObject('label_text_field')->setValue($msLabelTextField);
    
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_incident_task_view.xml');

?>