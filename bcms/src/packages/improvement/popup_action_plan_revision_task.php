<?php
include_once "include.php";
include_once $handlers_ref . "grid/QueryGridUserTasks.php";

class SaveEvent extends FWDRunnable {
  public function run() {    
    $miTaskId = FWDWebLib::getObject('task_id')->getValue();
    $moTask = new WKFTask();
    $moTask->fetchById($miTaskId);
    $moTask->testPermissionToExecute($miTaskId);

    $miAPId = FWDWebLib::getObject('ap_id')->getValue();
    
    $moCtxUserTest = new CIActionPlan();
    $moCtxUserTest->fetchById($miAPId);
    $moCtxUserTest->testPermissionToEdit($miAPId); 

    $moTask = new WKFTask();
    $moActionPlan = new CIActionPlan();
    $miIsEfficient = trim(FWDWebLib::getObject('efficiencyController')->getValue(),':');
    $moActionPlan->setFieldValue('ap_isefficient',$miIsEfficient?true:false);
    
    if(ISMSLib::getConfigById(GENERAL_MANUAL_DATA_CONTROL)){
      $moActionPlan->setFieldValue('ap_dateefficiencymeasured', FWDWebLib::getObject('ap_revision_task_date')->getTimestamp());
      $moTask->setFieldValue('task_date_accomplished', FWDWebLib::getObject('ap_revision_task_date')->getTimestamp());
    }else{
      $moActionPlan->setFieldValue('ap_dateefficiencymeasured', ISMSLib::ISMSTime());
      $moTask->setFieldValue('task_date_accomplished', ISMSLib::ISMSTime());
    }


    $moActionPlan->update($miAPId);
    
    $moActionPlan->stateForward(WKF_ACTION_AP_SEND_TO_WAINTING_MEASUREMENT);
    
    
    
    $moTask->setFieldValue('task_is_visible',false);
    $moTask->update($miTaskId);
    
    $msNextTaskJS = "";
		$mbOpenNextTask = (bool)array_filter(explode(':',FWDWebLib::getObject('next_task_controller')->getValue()));
		if ($mbOpenNextTask) {
		  $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
		  $moHandlerTask = new QueryGridUserTasks(FWDWebLib::getConnection());
      $moHandlerTask->setUser($miUserId);
      $moHandlerTask->setOrderBy('task_date_created','-');
      $moHandlerTask->makeQuery();
      $moHandlerTask->executeQuery();
      if ($moHandlerTask->fetch()) {
  		  $msNextTaskJS = 'soWindow.setTimeout("'.ISMSLib::getTaskPopup($moHandlerTask->getFieldValue('task_id'),
  		                                        $moHandlerTask->getFieldValue('task_activity'),
  		                                        $moHandlerTask->getFieldValue('task_context_id'),
  		                                        $moHandlerTask->getFieldValue('task_context_type'),
  		                                        2).',1");';
      }
		}
    echo "soWindow = soPopUpManager.getPopUpById('popup_action_plan_revision_task').getOpener();"
        ."if (soWindow.refresh_grids)"
        ."  soWindow.refresh_grids();"
        .$msNextTaskJS
        ."soPopUpManager.closePopUp('popup_action_plan_revision_task');";
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new SaveEvent("save_event"));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $miAutoTaskOpen = FWDWebLib::getObject('auto_task_open')->getValue();
    if (!$miAutoTaskOpen) FWDWebLib::getObject('next_task')->setShouldDraw(false);
    if ($miAutoTaskOpen==2) FWDWebLib::getObject('next_task_controller')->checkItem(1);
    
    $miAPId = FWDWebLib::getObject('ap_id')->getValue();
    $miTaskId = FWDWebLib::getObject('task_id')->getValue();

    $moTask = new WKFTask();
    $moTask->fetchById($miTaskId);
    $moTask->testPermissionToExecute($miTaskId);

    $moActionPlan = new CIActionPlan();
    $moActionPlan->fetchById($miAPId);

    FWDWebLib::getObject('name')->setValue($moActionPlan->getFieldValue('ap_name'));
    FWDWebLib::getObject('action_plan')->setValue($moActionPlan->getFieldValue('ap_actionplan'));

    $msToolTipText = trim($moActionPlan->getFieldValue('ap_name'));
    if($msToolTipText!=''){
      $moToolTip = new FWDToolTip();
      $moToolTip->setAttrShowDelay(1000);
      $moToolTip->setValue($msToolTipText);
      FWDWebLib::getObject('name')->addObjFWDEvent($moToolTip);
    }

    if(ISMSLib::getConfigById(GENERAL_MANUAL_DATA_CONTROL)){
      FWDWebLib::getObject('requiredCheck')->setAttrElements(FWDWebLib::getObject('requiredCheck')->getAttrElements().":ap_revision_task_date");
      FWDWebLib::getObject('ap_revision_task_date')->setValue(ISMSLib::getISMSShortDate());
    }else{
      FWDWebLib::getObject('vg_data_revision_group')->setShouldDraw(false);
      FWDWebLib::getObject('vg_buttons_popup')->getObjFWDBox()->setAttrTop(186);
      FWDWebLib::getObject('dialog')->getObjFWDBox()->setAttrHeight(245);
    }

    //instala a seguran�a de ACL na p�gina
    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
  }
}
  FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
  FWDWebLib::getInstance()->xml_load("popup_action_plan_revision_task.xml");
?>