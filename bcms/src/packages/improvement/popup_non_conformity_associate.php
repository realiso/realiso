<?php

include_once 'include.php';
include_once $handlers_ref . 'grid/improvement/QueryGridNonConformitySearch.php';
include_once $classes_isms_ref . 'nonauto/ISMSContextDrawGrid.php';

class GridCurrentNcs extends ISMSContextDrawGrid{

  public function __construct(){
    parent::__construct('icon-ci_nonconformity.gif');
  }
  
  public function drawItem(){
    if($this->getFieldValue('nc_state') != CONTEXT_STATE_CI_DIRECTED){
        $this->bpRowEventBlock=true;
        $this->csDivUnitClass = 'GridLineGray';
    }
    return parent::drawItem();
  }
}

function getCurrentIds(){
  $maValues = array_filter(explode(':', FWDWebLib::getObject('var_current_ids')->getValue()));
  if(count($maValues)==0) $maValues = array(0);
  return $maValues;
}

class SearchEvent extends FWDRunnable {
  public function run(){
    $moGrid = FWDWebLib::getObject('grid_search');
    $moGrid->execEventPopulate();
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
    $moStartEvent->addAjaxEvent(new SearchEvent('search_event'));
    $maCurrentIds = getCurrentIds();
    $moGrid = FWDWebLib::getObject('grid_search');
    if(FWDWebLib::getObject('var_must_search')->getValue()){
      $moHandler = new QueryGridNonConformitySearch();
      $miApId = FWDWebLib::getObject('par_ap_id')->getValue();
      if($miApId){
        $moAp = new CIActionPlan();
        $moAp->fetchById($miApId);
        switch($moAp->getFieldValue('ap_actiontype')){
          case ACTION_PLAN_CORRECTIVE_TYPE:{
            $moHandler->setTypeNc(NC_NO_CAPABILITY);
            break;
          }
          case ACTION_PLAN_PREVENTIVE_TYPE:{
            $moHandler->setTypeNc(NC_CAPABILITY);
            break;
          }
        }
      }

      $moHandler->setName(utf8_decode(FWDWebLib::getObject('var_name')->getValue()));
      $moHandler->setExcludedIds($maCurrentIds);
      $moGrid->setQueryHandler($moHandler);
      $moGrid->setAttrPopulate('true');
      $moGrid->setObjFwdDrawGrid(new ISMSContextDrawGrid('icon-ci_nonconformity.gif'));
    }else{
      $moGrid->setAttrPopulate('false');
    }
    $moGrid = FWDWebLib::getObject('grid_current');
    $moHandler = new QueryGridNonConformitySearch();
    $moHandler->setIds($maCurrentIds);
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new GridCurrentNcs());
  }
  
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    if(FWDWebLib::getObject('par_ap_id')->getValue()){
      $moNCAp = new CINcActionPlan();
      $moNCAp->getFieldByAlias('ap_id')->addFilter(new FWDDBFilter('=',FWDWebLib::getObject('par_ap_id')->getValue()));
      $moNCAp->select();
      $maValues = array();
      while($moNCAp->fetch()){
        $maValues[]=$moNCAp->getFieldValue('nc_id');
      }
      FWDWebLib::getObject('var_current_ids')->setValue(implode(':',$maValues));
    }
    $moGrid = FWDWebLib::getObject('grid_current');
    $moHandler = $moGrid->getQueryHandler();
    $moHandler->setIds(getCurrentIds());
    $moGrid->setQueryHandler($moHandler);
    
    //instala a seguran�a de ACL na p�gina
    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
       function search(){
          gebi('var_must_search').value = 1;
          gebi('var_name').value = gebi('name').value;
          gobi('grid_search').setPopulate(true);
          trigger_event('search_event',3);
        }
        var soContextsAssociator = new ISMSContextsAssociator('grid_search','grid_current','var_current_ids','grids_pack');
        gebi('name').focus();
      </script>
    <?
  }
}

// Testa se a licen�a utilizada permite acessar este m�dulo
//ISMSLib::testUserPermissionTo%module%Mode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_non_conformity_associate.xml');

?>