<?php
include_once "include.php";

include_once $handlers_ref . "QueryIncidentRiskParameter.php";
include_once $handlers_ref . "QueryIncidentRiskParameterUpdate.php";

function getRiskParameters() {
	$riskParameters = array();
    foreach($GLOBALS['_POST'] as $key=>$value) {
    	if(strstr($key,"risk_parameters_all")!=FALSE) {
    		$risk_array = explode("_",$key);
    		$riskParameters[$risk_array[3]] = $value;
    	}    		
	}
	return($riskParameters);
}

class SaveEvent extends FWDRunnable {
  public function run(){

    $moWebLib = FWDWebLib::getInstance();
    $maRisksIds = explode(':',$moWebLib->getObject('risks_ids')->getValue());
//    $moIncidentRiskParameter = new CIIncidentRiskParameter();
    //$moIncidentParameter = $moWebLib->getObject("risk_parameters_all");
    
    //$maIncidentParameters = $moIncidentParameter->getValues();
    
    $maRiskParameters = getRiskParameters();
    $miIncidentId = $moWebLib->getObject('incident_id')->getValue();
    
    $maReturn = array();
   	foreach($maRiskParameters as $parameter=>$value) {
   		if(!$value) {
   			continue;
   		}
   		
   		$queryIncident = new QueryIncidentRiskParameter(FWDWebLib::getConnection());
   		$queryIncident->setIncidentId($miIncidentId);
   		$queryIncident->setParameterName($parameter);
   		$queryIncident->makeQuery();
    	$queryIncident->executeQuery();
    	$queryReturn = $queryIncident->getDataset();
    	if($queryIncident->fetch()){
    		$queryUpdate = new QueryIncidentRiskParameterUpdate(FWDWebLib::getConnection());
    		$queryUpdate->setIncidentId($miIncidentId);
    		$queryUpdate->setParameterName($parameter);
    		$queryUpdate->setParameterValue($value);
   			$queryUpdate->makeQuery();
    		$queryUpdate->executeQuery();
    	} else {
    		$moIncidentRiskParameter = new CIIncidentRiskParameter();
    		$moIncidentRiskParameter->setFieldValue('incident_id',$miIncidentId);
    		$moIncidentRiskParameter->setFieldValue('parameter_name_id',$parameter);
    		$moIncidentRiskParameter->setFieldValue('parameter_value_id',$value);
    		$moIncidentRiskParameter->insert(false);
    	}
   	}
   	//$fd = fopen("/tmp/a.txt","a+");
   	
   		
    
    
   	$updateRiskTree = array();
   	
   	//fwrite($fd,print_r($maRisksIds,true));
    foreach($maRisksIds as $offset=>$miRiskId){
    	//fwrite($fd,"passa aqui: $miRiskId\n");
       	$query = new QueryRiskValues(FWDWebLib::getConnection());
       	$query->setElementId($miRiskId);
       	$query->makeQuery();
       	$query->executeQuery();
       	while($query->fetch()) { // enquanto existirem riscos
       		$data = $query->getDataset();
       		$riskParameter = $data->getFieldByAlias('parameter_id')->getValue();
       		$riskValue = $data->getFieldByAlias('value_id')->getValue();
       		//fwrite($fd,"riskParameter $riskParameter riskValue: $riskValue\n");
       		if($maRiskParameters[$riskParameter] != $riskValue) {
       			$updateRiskTree[$miRiskId][$riskParameter] = $maRiskParameters[$riskParameter];
       		}
       	}
    }
    //fwrite($fd,print_r($updateRiskTree,true));
    //fclose($fd);
   
	//$miIncidentId = $moWebLib->getObject('incident')->getValue();
	
    // aqui tem que procurar se ja nao existe, caso contrario tem que inserir
/*   	foreach($maIncidentParameters as $parameter=>$value) {
   		if(!$value) {
   			continue;
   		}
    	$moIncidentRiskParameter = new CIIncidentRiskParameter();
    	$moIncidentRiskParameter->setFieldValue('incident_id',$miIncidentId);
    	$moIncidentRiskParameter->setFieldValue('parameter_name_id',$parameter);
    	$moIncidentRiskParameter->setFieldValue('parameter_value_id',$value);
    	$moIncidentRiskParameter->insert(false);
   	} */	
    
    
//    $moIncidentRiskParameter->setFieldValue('incident_id',$miIncidentId);
    $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    $moSession->deleteAttribute("incidentRiskParameters");
    $moSession->addAttribute("incidentRiskParameters");
    $moSession->setAttrIncidentRiskParameters(serialize($maRiskParameters));
    
    $moSession->deleteAttribute("updateRiskTree");
    $moSession->addAttribute("updateRiskTree");
    $moSession->setAttrUpdateRiskTree(serialize($updateRiskTree));
    
    $moSession->deleteAttribute("incidentId");
    $moSession->addAttribute("incidentId");
    $moSession->setAttrIncidentId(serialize($miIncidentId));
    
    
/*    $moSession->deleteAttribute("riskParameterList");
    $moSession->addAttribute("riskParameterList");
    $moSession->setAttrRiskParameterList(serialize($maRiskParametersList)); */
    
    if(count($updateRiskTree)) {
    	echo "isms_open_popup('popup_request_compare_impact_risks','packages/improvement/popup_request_compare_impact_risks.php','','true');";
    } else {
    echo "soPopUpManager.closePopUp('popup_incident_risk_parametrization');";
    }
  }
}

function getParametersNames(){
/*
  return array(
    '1' => '1� Parametro',
    '2' => '2� Parametro',
    '3' => '3� Parametro',
    '4' => '4� Parametro',
    '5' => '5� Parametro',
    '6' => '6� Parametro',
    '7' => '7� Parametro',
    '8' => '8� Parametro',
  );
*/
  $maParameters = FWDWebLib::getObject('ParametersNames');
  if($maParameters===null){
    $maParameters = array();
    $moHandler = new QueryParameterNames(FWDWebLib::getConnection());
    $moHandler->makeQuery();
    $moHandler->executeQuery();
    $moDataset = $moHandler->getDataset();
    while($moDataset->fetch()){
      $miId = $moDataset->getFieldByAlias('parametername_id')->getValue();
      $msName = $moDataset->getFieldByAlias('parametername_name')->getValue();
      $maParameters[$miId] = $msName;
    }
    FWDWebLib::addObject('ParametersNames',$maParameters);
  }
  return $maParameters;
}

function getValuesNames(){
/*
  return array(
    '1' => 'valor grande 1',
    '2' => 'valor grande 2',
    '3' => 'valor grande 3',
  );
*/
  $maValuesNames = FWDWebLib::getObject('ValuesNames');
  if($maValuesNames===null){
    $maValuesNames = array();
    $moHandler = new QueryParameterValueNames(FWDWebLib::getConnection());
    $moHandler->setType(RISKPARAMETER_RISK);
    $moHandler->makeQuery();
    $moHandler->executeQuery();
    $moDataset = $moHandler->getDataset();
    while($moDataset->fetch()){
      $miId = $moDataset->getFieldByAlias('value_id')->getValue();
      $msName = $moDataset->getFieldByAlias('value_name')->getValue();
      $maValuesNames[$miId] = $msName;
    }
    FWDWebLib::addObject('ValuesNames',$maValuesNames);
  }
  return $maValuesNames;
}

/*function getProbabilityValues(){

  return array(
    '1' => 'valor grande 1',
    '2' => 'valor grande 2',
    '3' => 'valor grande 3',
  );

  $maProbabilityValues = FWDWebLib::getObject('ProbabilityValues');
  if($maProbabilityValues===null){
    $maProbabilityValues = array();
    $moHandler = new QueryProbabilityValueNames(FWDWebLib::getConnection());
    $moHandler->setType(RISKPARAMETER_RISK);
    $moHandler->makeQuery();
    $moHandler->executeQuery();
    $moDataset = $moHandler->getDataset();
    while($moDataset->fetch()){
      $miId = $moDataset->getFieldByAlias('value_id')->getValue();
      $msName = $moDataset->getFieldByAlias('value_name')->getValue();
      $maProbabilityValues[$miId] = $msName;
    }
    FWDWebLib::addObject('ProbabilityValues',$maProbabilityValues);
  }
  return $maProbabilityValues;
}*/

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
    $moStartEvent->addAjaxEvent(new SaveEvent('save_event'));
    
    // Busca no banco os nomes dos par�metros
    $maParameters = getParametersNames();
    // Busca no banco os nomes dos valores
    $maValues = getValuesNames();
    /*// Busca no banco os nomes dos valores de probabilidade
    $maProbabilityValues = getProbabilityValues();*/
    
    $moWebLib = FWDWebLib::getInstance();
    $moRisksIds = $moWebLib->getObject('risks_ids');
    $msRisks = $moRisksIds->getValue();
    if($msRisks){
      $maRisksIds = explode(':',$msRisks);
    }else{
      $maRisksIds = array_unique(explode(',',$moWebLib->getObject('risks')->getValue()));
      $moRisksIds->setValue(implode(':',$maRisksIds));
    }
    
    foreach($maRisksIds as $miRiskId){
      $moRiskParameter = new RiskParameters();
      $moRiskParameter->setAttrName("risk_parameters_$miRiskId");
      $moWebLib->addObject("risk_parameters_$miRiskId",$moRiskParameter);
      $moRiskParameter->setAttrType('incident_risk');
      $moRiskParameter->setObjFWDBox(new FWDBox());
      foreach($maParameters as $miId=>$msName){
        $moRiskParameter->addParameter($miId,$msName);
      }
      foreach($maValues as $miId=>$msName){
        $moRiskParameter->addValue($miId,$msName);
      }
      /*foreach($maProbabilityValues as $miId=>$msName){
        $moRiskParameter->addProbabilityValue($miId,$msName);
      }*/
    }
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $maRisksIds = array_filter(explode(':',$moWebLib->getObject('risks_ids')->getValue()));
    $miIncidentId = $moWebLib->getObject('incident_id')->getValue();
    
    /*//teste para verificar se o sistema n�o est� sendo hakeado
    foreach($maRisksIds as $miId){
      $moCtxUserTest = new RMRisk();
      $moRiskTest = new RMRisk();
      $moRiskTest->fetchById($miId);
      $moCtxUserTest->testPermissionToEdit($miId,$moRiskTest->getApprover());
    }*/
    
    // Busca no banco os nomes dos par�metros
    $maParameters = getParametersNames();
    // Busca no banco os nomes dos valores
    $maValues = getValuesNames();
    /*// Busca no banco os nomes dos valores de probabilidade
    $maProbabilityValues = getProbabilityValues();*/
    
    $moOuterViewGroup = $moWebLib->getObject('risk_parameters');
    //$moInnerViewGroup = $moWebLib->getObject('lines');
    
    //$moInnerViewGroup->setAttrClass('simple_border');
    
    $miMaxSelectWidth = 200;
    $miMaxStaticWidth = 250;
    $miHorizontalMargin = 10;
    $miStaticWidth = 160;
    $miLineHeight = 36;
    $miStaticHeight = 32;
    $miRiskParameterHeight = 25;
    $miHeaderHeight = 25;
    $miHorizontalOffset = floor($miHorizontalMargin/2);
    
    $miStaticVerticalOffset = floor(($miLineHeight - $miStaticHeight)/2);
    $miRiskParameterVerticalOffset = floor(($miLineHeight - $miRiskParameterHeight)/2);
    
    $miViewGroupWidth = $moOuterViewGroup->getObjFWDBox()->getAttrWidth();
    $miColumns = count($maParameters);
    $miColumnsTotalWidth = $miViewGroupWidth - $miStaticWidth - 20;
    if($miColumnsTotalWidth/$miColumns > $miMaxSelectWidth){
      $miStaticWidth = $miViewGroupWidth - $miColumns * $miMaxSelectWidth;
      if($miStaticWidth > $miMaxStaticWidth){
        $miStaticWidth = $miMaxStaticWidth;
        $miColumnsTotalWidth = $miColumns * $miMaxSelectWidth;
      }else{
        $miColumnsTotalWidth = $miViewGroupWidth - $miStaticWidth - 20;
      }
    }
    $miSelectWidth = $miColumnsTotalWidth/$miColumns - $miHorizontalMargin;

    $miLeft = $miStaticWidth;
    /*$maParameters['prob'] = FWDLanguage::getPHPStringValue('mx_probability','Probabilidade');*/
    foreach($maParameters as $msName){
      $moStatic = new FWDStatic();
      $moStatic->setObjFWDBox(new FWDBox($miLeft+$miHorizontalOffset,0,$miHeaderHeight,$miSelectWidth));
      $moStatic->setValue($msName);
      $moOuterViewGroup->addObjFWDView($moStatic);
      $miLeft+= $miSelectWidth + $miHorizontalMargin;
      
      $moToolTip = new FWDToolTip();
      $moToolTip->setAttrShowDelay(1000);
      $moToolTip->setValue($msName);
      $moStatic->addObjFWDEvent($moToolTip);
    }
    //unset($maParameters['prob']);

    $moRiskParameter = new RiskParameters();
    $moRiskParameter->setAttrName("risk_parameters_all");
    $moRiskParameter->setAttrType('incident_risk');
    $moRiskParameter->setAttrClass('');
    $moRiskParameter->setAttrHorizontal('true');
    $moRiskParameter->setAttrValueNameWidth($miSelectWidth);
    $moRiskParameter->setAttrHorizontalMargin($miHorizontalMargin);
    $moRiskParameter->setObjFWDBox(new FWDBox($miStaticWidth,25,0,0));
    $moOuterViewGroup->addObjFWDView($moRiskParameter);
    
    

    foreach($maParameters as $miId=>$msName){
    	$query = new QueryIncidentRiskParameter(FWDWebLib::getConnection());
    	$query->setIncidentId($miIncidentId);
    	$query->setParameterName($miId);
   		$query->makeQuery();
    	if($query->executeQuery()) {
    		$queryReturn = $query->getDataset();
    		$queryReturn->fetch();
    		$parameterValue = $queryReturn->getFieldByAlias('ci_parameter_value')->getValue();
    		$moRiskParameter->addSelectedValue($miId, $parameterValue);
    	}
    }
    
    
    
    foreach($maParameters as $miId=>$msName){
      $moRiskParameter->addParameter($miId,$msName);
    }
    foreach($maValues as $miId=>$msName){
      $moRiskParameter->addValue($miId,$msName);
    }
    /*foreach($maProbabilityValues as $miId=>$msName){
      $moRiskParameter->addProbabilityValue($miId,$msName);
    }*/
    $moRiskParameter->build();

/*    $miTop = 0;
    $mbEven = false;
    foreach($maRisksIds as $miRiskId){
      if ($miIncidentId) {
        $moIncidentRisk = new CIIncidentRisk();
        $moIncidentRisk->createFilter($miIncidentId,'incident_id');
        $moIncidentRisk->createFilter($miRiskId,'risk_id');
        $moIncidentRisk->select();
        if ($moIncidentRisk->fetch()) continue;
      }
      $moRisk = new RMRisk();
      $moRisk->fetchById($miRiskId);
      $msRiskName = $moRisk->getFieldValue('risk_name');
      
      $moLine = new FWDViewGroup();
      $moLine->setObjFWDBox(new FWDBox(0,$miTop,$miLineHeight,$miViewGroupWidth-20));
      $moLine->setAttrClass($mbEven?'BdZ0':'BdZ1');
      $moInnerViewGroup->addObjFWDView($moLine);
      
      $moStatic = new FWDMemoStatic();
      $moStatic->setAttrScrollbar('true');
      $moStatic->setAttrName("label_risk_parameters_$miRiskId");
      $moStatic->setAttrClass('MemoStatic_RiskName');
      $moStatic->setObjFWDBox(new FWDBox(0,$miStaticVerticalOffset,$miStaticHeight,$miStaticWidth));
      $moStatic->setValue($msRiskName);
      $moLine->addObjFWDView($moStatic);
      
      $moToolTip = new FWDToolTip();
      $moToolTip->setAttrShowDelay(1000);
      $moToolTip->setValue($msRiskName);
      $moStatic->addObjFWDEvent($moToolTip);
      
      $moRiskParameter = $moWebLib->getObject("risk_parameters_$miRiskId");
      $moRiskParameter->setAttrClass('');
      $moRiskParameter->setAttrHorizontal('true');
      $moRiskParameter->setAttrValueNameWidth($miSelectWidth);
      $moRiskParameter->setAttrHorizontalMargin($miHorizontalMargin);
      $moRiskParameter->setObjFWDBox(new FWDBox($miStaticWidth,$miRiskParameterVerticalOffset,0,0));
      //*
      $moRiskParameterValues = new RiskParametersValue($moRiskParameter,RISKPARAMETER_INCIDENTRISK);
      $moRiskParameterValues->getValues($miRiskId);
      
      $moLine->addObjFWDView($moRiskParameter);
      
      $moRiskHash = new FWDVariable();
      $moRiskHash->setAttrName("risk_hash_$miRiskId");
      $moRiskHash->setValue($moRiskParameter->getHash());
      $moLine->addObjFWDView($moRiskHash);
      
      $miTop+= $miLineHeight;
      $mbEven = !$mbEven;
    } */
    
    FWDWebLib::getInstance()->dump_html($moWebLib->getObject('dialog'));
    
?>    <script language="javascript">
        gebi('risk_parameters').object = {
          'getRiskParameters': function(){
            if(!this.caRiskParameters){
              var maIds = gebi('risks_ids').value.split(':');
              this.caRiskParameters = [];
              var index=0;
              for(var i=0;i<maIds.length;i++){
                if (gebi('risk_parameters_'+maIds[i])) {
                  this.caRiskParameters.push(gebi('risk_parameters_'+maIds[i]));
                  this.caRiskParameters[index].object.onRequiredCheckOk = null;
                  this.caRiskParameters[index].object.onRequiredCheckNotOk = null;
                  index++;
                }
              }
              maIds = index = i = null;
            }
            return this.caRiskParameters;
          },
          'isFilled': function(){
            var maOk = [];
            var maNotOk = [];
            var maRiskParameters = this.getRiskParameters();
            for(var i=0;i<maRiskParameters.length;i++){
              if(maRiskParameters[i].object.isFilled()){
                maOk.push(maRiskParameters[i].id);
              }else{
                maNotOk.push(maRiskParameters[i].id);
              }
            }
            js_required_check_feedback(maOk,maNotOk);
            var mbReturn = (maNotOk.length==0);
            maOk = maNotOk = maRiskParameters = null;
            return mbReturn;
          },
          'onRequiredCheckOk': function(){},
          'onRequiredCheckNotOk': function(){},
          'setParameterValue': function(psParamId,psValueId){
            var maRiskParameters = this.getRiskParameters();
            for(var i=0;i<maRiskParameters.length;i++){
              maRiskParameters[i].object.setParameterValue(psParamId,psValueId);
            }
            maRiskParameters = null;
          }
        };
        
        gobi('risk_parameters_all').setParameterValue = function(psParamId,psValueId){
          var moSelect = gebi(this.csName+'_'+psParamId);
          moSelect.value = psValueId;
          moSelect.className = moSelect.options[moSelect.selectedIndex].className;
          gobi('risk_parameters').setParameterValue(psParamId,psValueId);
          moSelect = psParamId = psValueId = null;
        }
        
      </script>
<?    
      
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_incident_risk_parametrization.xml');
?>