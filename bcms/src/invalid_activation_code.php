<?php
include_once "include.php";

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $msMessage = FWDLanguage::getPHPStringValue('st_invalid_activation_code', "O c�digo de ativa��o inserido � inv�lido. Por favor, revise-o ou contate nosso Suporte no site da Realiso.");
    FWDWebLib::getObject("invalid_activation_code_message")->setValue($msMessage);
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("invalid_activation_code.xml");
?>