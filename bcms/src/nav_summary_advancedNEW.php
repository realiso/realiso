<?php
include_once "include.php";
include_once $handlers_ref . "QueryResume.php";
include_once $handlers_ref . "QueryTop10Qualitative.php";

function getReportLink($psValue,$psReportName,$psModule,$pmFilter='',$psPrefix='',$psSufix=''){
	if($psValue){
		if(is_array($pmFilter)){
			if(count($pmFilter)){
				$msFilter = str_replace("\"","\\'",serialize($pmFilter));
			}else{
				$msFilter = '';
			}
		}else{
			$msFilter = $pmFilter;
		}
		return "<a href=\"javascript:generateReport('{$psReportName}','{$psModule}','{$msFilter}')\">{$psPrefix}{$psValue}{$psSufix}</a>";
	}else{
		return $psPrefix.$psValue.$psSufix;
	}
}

class ISMSReportEvent extends FWDRunnable {
	public function run(){
		set_time_limit(3000);

		$msReport = FWDWebLib::getObject('report_name')->getValue();
		$msFormat = FWDWebLib::getObject('report_format')->getValue();

		$mbDontForceDownload = ($msFormat==REPORT_FILETYPE_HTML);

		$moWindowReport = new ISMSReportWindow(RISK_MANAGEMENT_MODE);
		if($mbDontForceDownload) $moWindowReport->open();

		$moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
		$moSession->deleteAttribute("filter");
		$moSession->addAttribute("filter");
		switch($msReport) {
			case "report_risk_values":
				$moFilter = new ISMSReportRiskValuesFilter();
				$moFilter->setRiskValueType(RISK_VALUE_TYPE_RESIDUAL_VALUE);
				$moFilter->showRiskWithNoValues(true);
				break;
			case 'report_statement_of_applicability':
				$moFilter  = new ISMSReportStatementOfApplicabilityFilter();
				FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->deleteAttribute("isPreReport");
				$moFilter->setStandardFilterName(FWDLanguage::getPHPStringValue('st_all_standards', 'Todas'));
				break;
			case 'report_risk_treatment_plan':
			case 'report_areas_without_processes':
			case 'report_processes_without_assets':
			case 'report_controls_without_risks':
				$moFilter = new ISMSReportGenericClassifTypePrioFilter();
				break;
			case 'report_areas':{
				$moFilter = new ISMSReportAreasFilter();
				$moFilter->setValue(FWDWebLib::getObject('report_filter')->getValue());
				break;
			}
			case 'report_processes':{
				$moFilter = new ISMSReportProcessesFilter();
				$moFilter->setValue(FWDWebLib::getObject('report_filter')->getValue());
				break;
			}
			case 'report_assets':{
				$moFilter = new ISMSReportAssetsFilter();
				$moFilter->setValue(FWDWebLib::getObject('report_filter')->getValue());
				break;
			}
			case 'report_risks':{
				$moFilter = new ISMSReportRisksFilter();
				$msFilter = FWDWebLib::getObject('report_filter')->getValue();
				if($msFilter){
					$maFilter = FWDWebLib::unserializeString(str_replace("'",'"',$msFilter));
					if(isset($maFilter['rv'])){
						$moFilter->setResidualValue($maFilter['rv']);
					}
					if(isset($maFilter['v'])){
						$moFilter->setValue($maFilter['v']);
					}
					if(isset($maFilter['t'])){
						$moFilter->setShowTreated($maFilter['t']);
					}
					if(isset($maFilter['e'])){
						$moFilter->setShowEstimated($maFilter['e']);
					}
				}
				break;
			}
			case 'report_controls':{
				$moFilter = new ISMSReportControlsFilter();
				$moFilter->setImplementationStatus(FWDWebLib::getObject('report_filter')->getValue());
				break;
			}
			case 'report_control_cost':{
				$moFilter = new ISMSReportGenericClassifTypePrioFilter();
				break;
			}
			case 'report_control_cost_by_cost_category':{
				$moFilter = new ISMSReportControlCostByCostCategoryFilter();
				$msFilter = FWDWebLib::getObject('report_filter')->getValue();
				if($msFilter){
					$maFilter = unserialize(str_replace("'",'"',$msFilter));
					if(isset($maFilter['cc'])){
						$moFilter->setCostCategory($maFilter['cc']);
					}
				}
				break;
			}
			case 'report_risk_financial_impact':{
				$moFilter = new ISMSReportRiskFinancialImpactFilter();
				$msFilter = FWDWebLib::getObject('report_filter')->getValue();
				if($msFilter){
					$maFilter = unserialize(str_replace("'",'"',$msFilter));
					if(isset($maFilter['t'])){
						$moFilter->setTreatment($maFilter['t']);
					}
					if(isset($maFilter['v'])){
						$moFilter->setValues($maFilter['v']);
					}
					if(isset($maFilter['nta'])){
						$moFilter->setNotTreatedOrAccepted(true);
					}
				}
				break;
			}
			case 'report_best_practices':{
				$moFilter = new ISMSReportBestPracticesFilter();
				$msFilter = FWDWebLib::getObject('report_filter')->getValue();
				if($msFilter){
					$maFilter = unserialize(str_replace("'",'"',$msFilter));
					if(isset($maFilter['s'])){
						$moFilter->setStandard($maFilter['s']);
					}
					if(isset($maFilter['a'])){
						$moFilter->setOnlyApplied(true);
					}
				}
				break;
			}
			default:
				$moFilter = new FWDReportFilter();
				break;
		}
		$moFilter->setFileType($msFormat);
		$moSession->setAttrFilter($moFilter);

		if($mbDontForceDownload){
			$moWindowReport->setWaitLoadingComplete(true);
			$moWindowReport->loadReport($msReport);
		}
		else $moWindowReport->forceReportDownload($msReport);
	}
}

class GridTop10Qualitative extends FWDDrawGrid {
	public function drawItem(){
		if ($this->ciRowIndex==1) {
			$this->csDivUnitClass = 'GridLineGray';
		} else {
			$this->coCellBox->setIconSrc("");
		}
		$this->coCellBox->setAttrStringNoEscape("true");
		return parent::drawItem();
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent("event_screen"));
		$moStartEvent->addAjaxEvent(new ISMSReportEvent("function_report"));
		FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
	}
}

function getArrayGrids(){
	$moConfig = new ISMSConfig();
	$mbHasRMModule = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(RISK_MANAGEMENT_MODE);
	$maGrids = array();
	if ($mbHasRMModule) {
		$maGrids['grid_resume_summary'             	] = array('module'=>'rm', 'height'=>FWDWebLib::getObject('grid_resume_summary')->getObjFWDBox()->getAttrHeight());
		$maGrids['grid_top10qualitative'           	] = array('module'=>'rm', 'height'=>FWDWebLib::getObject('grid_top10qualitative')->getObjFWDBox()->getAttrHeight());
	}

	return $maGrids;
}

function populate_grid_resume_summary(){
	$moQuery = new QueryResume(FWDWebLib::getConnection());
	$moQuery->makeQuery();
	$moQuery->executeQuery();
	$result = $moQuery->getRiskSummary();

	$gridResume = FWDWebLib::getObject("grid_resume_summary");
	$gridResume->setItem(1,1,FWDLanguage::getPHPStringValue('places','<b>Locais</b>'));
	$gridResume->setItem(1,2,FWDLanguage::getPHPStringValue('subplaces','<b>Sub-Locais</b>'));
	$gridResume->setItem(1,3,FWDLanguage::getPHPStringValue('areas','<b>Unidades de Neg�cio</b>'));
	$gridResume->setItem(1,4,FWDLanguage::getPHPStringValue('subareas','<b>Sub-Unidades de Neg�cio</b>'));
	$gridResume->setItem(1,5,FWDLanguage::getPHPStringValue('plans','<b>Planos</b>'));
	$gridResume->setItem(1,6,FWDLanguage::getPHPStringValue('scenes','<b>Cen�rios</b>'));
	$gridResume->setItem(1,7,FWDLanguage::getPHPStringValue('activities','<b>Atividades</b>'));
	$gridResume->setItem(1,8,FWDLanguage::getPHPStringValue('assets','<b>Ativos</b>'));

	$gridResume->setItem(2,1,$result["places"]);
	$gridResume->setItem(2,2,$result["subplaces"]);
	$gridResume->setItem(2,3,$result["areas"]);
	$gridResume->setItem(2,4,$result["subareas"]);
	$gridResume->setItem(2,5,$result["plans"]);
	$gridResume->setItem(2,6,$result["scenes"]);
	$gridResume->setItem(2,7,$result["activities"]);
	$gridResume->setItem(2,8,$result["assets"]);
}

function populate_grid_top10qualitative(){
	$moQuery = new QueryTop10Qualitative(FWDWebLib::getConnection());
	$moQuery->makeQuery();
	$moQuery->executeQuery();
	$grid = FWDWebLib::getObject("grid_top10qualitative");
	$grid->setObjFWDDrawGrid(new GridTop10Qualitative());

	$grid->setItem(1,1,FWDLanguage::getPHPStringValue('process','<b>Processo</b>'));
	$grid->setItem(2,1,FWDLanguage::getPHPStringValue('plans','<b>Planos</b>'));

	$line = 2;
	while($moQuery->fetch()){
		$grid->setItem(1,$line, "<b>{$moQuery->getFieldValue('name')}</b>");
		$grid->setItem(2,$line, $moQuery->getFieldValue('plans'));
		$line = $line + 1;
	}
}

function getModules(){
	return array('RM');
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$moWebLib = FWDWebLib::getInstance();

		$moPreference = new ISMSUserPreferences();
		if($moPreference->preferenceUserExistes("dashboard_sumary_pref")){
			$maPrefUser = FWDWebLib::unserializeString($moPreference->getPreference("dashboard_sumary_pref"));
			$maPrefUserKeys = array();
			if(is_array($maPrefUser)){
				$maPrefUserKeys = array_flip($maPrefUser);
			}
		}else{
			$maPrefUserKeys = getArrayGrids();
			$maValuesKeys ="";
			foreach($maPrefUserKeys as $miKey=>$maValue){
				$maValuesKeys[] = $miKey;
			}
			$moPreference->setPreference("dashboard_sumary_pref",serialize($maValuesKeys));
		}
		$maGrids = getArrayGrids();

		$mbColumnOneRM = true;
		$mbColumnOnePM = true;
		$mbColumnOneCI = true;
		$miTopCommon = 30;

		$miTopOneRM = $miTopCommon;
		$miTopTwoRM = $miTopCommon;
		$miTopOnePM = $miTopCommon;
		$miTopTwoPM = $miTopCommon;
		$miTopOneCI = $miTopCommon;
		$miTopTwoCI = $miTopCommon;

		$miTopFirstPanel = 15;
		$miLeftOne = 10;
		$miLeftTwo = 485;
		$miSpaceBetweenElements = 30;

		if(count($maPrefUserKeys)){
			foreach($maGrids as $miKey=>$maValues){
				FWDWebLib::getObject($miKey)->setShouldDraw(false);
			}
			foreach ($maPrefUserKeys as $miKey=>$maValues){
				$moGrid = FWDWebLib::getObject($miKey);
				if(isset($maGrids[$miKey])){
					$msModule = strtoupper($maGrids[$miKey]['module']);
					if(${"mbColumnOne".$msModule}){
						$moGrid->getObjFWDBox()->setAttrLeft($miLeftOne);
						$moGrid->getObjFWDBox()->setAttrTop(${"miTopOne".$msModule});
						${"miTopOne".$msModule} = ${"miTopOne".$msModule} + $maGrids[$miKey]['height'] + $miSpaceBetweenElements;
					}else{
						$moGrid->getObjFWDBox()->setAttrLeft($miLeftTwo);
						$moGrid->getObjFWDBox()->setAttrTop(${"miTopTwo".$msModule});
						${"miTopTwo".$msModule} = ${"miTopTwo".$msModule} + $maGrids[$miKey]['height'] + $miSpaceBetweenElements;
					}
					${"mbColumnOne".$msModule} = !${"mbColumnOne".$msModule};

					/*executa a fun��o que popula a grid correspondente, fun��o no formato "populate_"
					 * mais o nome da grid definido no xml */
					$msFuctionName = "populate_".$miKey;
					$msFuctionName();

					$moGrid->setShouldDraw(true);
				}
			}
		}else{
			foreach ($maPrefUserKeys as $miKey=>$maValues){
				$moGrid = FWDWebLib::getObject($miKey);
				$msModule = strtoupper($maGrids[$miKey]['module']);
				if(${"mbColumnOne".$msModule}){
					$moGrid->getObjFWDBox()->setAttrLeft($miLeftOne);
					$moGrid->getObjFWDBox()->setAttrTop(${"miTopOne".$msModule});
					${"miTopOne".$msModule} = ${"miTopOne".$msModule} + $maGrids[$miKey]['height'] + $miSpaceBetweenElements;
				}else{
					$moGrid->getObjFWDBox()->setAttrLeft($miLeftTwo);
					$moGrid->getObjFWDBox()->setAttrTop(${"miTopTwo".$msModule});
					${"miTopTwo".$msModule} = ${"miTopTwo".$msModule} + $maGrids[$miKey]['height'] + $miSpaceBetweenElements;
				}
				${"mbColumnOne".$msModule} = !${"mbColumnOne".$msModule};

				/*executa a fun��o que popula a grid correspondente, fun��o no formato "populate_"
				 * mais o nome da grid definido no xml */
				$msFuctionName = "populate_".$miKey;
				$msFuctionName();
				$moGrid->setShouldDraw(true);
			}
		}

		/*atualiza a altura dos panels dos m�dulos*/
		$miValuePast = $miTopFirstPanel;
		foreach (getModules() as $strModule){
			$miValue = max(${"miTopOne".$strModule},${"miTopTwo".$strModule});
			FWDWebLib::getObject(strtolower($strModule).'_sumary')->getObjFWDBox()->setAttrTop($miValuePast);
			if($miValue > $miTopCommon){
				FWDWebLib::getObject(strtolower($strModule).'_sumary')->getObjFWDBox()->setAttrHeight( $miValue + $miSpaceBetweenElements);
				$miValuePast = $miValuePast + $miValue + 2*$miSpaceBetweenElements;
			}else{
				FWDWebLib::getObject(strtolower($strModule).'_sumary')->setShouldDraw(false);
			}
		}

		$miTopOne = $miTopOneRM + $miTopOnePM + $miTopOneCI;
		$miTopTwo = $miTopTwoRM + $miTopTwoPM + $miTopTwoCI;
		FWDWebLib::getObject('dialog')->getObjFWDBox()->setAttrHeight(max($miTopOne,$miTopTwo,430));

		$mbSurveyDisplayed = false;
		$msSurvey = "";
		$moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
		$miUserId = $moSession->getUserId(true);
		$moUser = new ISMSUser();
		if ($moUser->fetchById($miUserId)){
			$mbSurveyDisplayed = (bool)$moUser->getFieldValue("user_answered_survey");
		}
		if (!$mbSurveyDisplayed && $moSession->attributeExists("surveyDisplayed")) {
			$mbSurveyDisplayed = ($moSession->getAttrSurveyDisplayed() === true);
		}
		if (!$mbSurveyDisplayed && ISMSSaaS::getConfig(ISMSSaaS::LICENSE_TYPE)==ISMSLicense::LICENSE_TYPE_TRIAL_ISMS) {
			$msSurvey = "isms_open_popup('popup_survey','popup_survey.php','','true');";
			if (!$moSession->attributeExists("surveyDisplayed")) {
				$moSession->addAttribute('surveyDisplayed');
			}
			$moSession->setAttrSurveyDisplayed(true);
		}

		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
<script language="javascript">
      function refresh(){
       showLoading();
       document.forms[0].submit();
       
      }
      
      function generateReport(psName,psModule,psFilter){
        gebi('report_name').value = psName;
        gebi('report_format').value = REPORT_FILETYPE_HTML;
        if(psFilter){
          gebi('report_filter').value = psFilter;
        }else{
          gebi('report_filter').value = '';
        }
        if(psModule){
          trigger_event('function_report_'+psModule,3);
        }else{
          trigger_event('function_report',3);
        }
      }
      
      <?=$msSurvey?>
    </script>
      <?
	}
}
FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_summary_advanced.xml");
?>