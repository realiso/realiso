<head>
	<link href='http://fonts.googleapis.com/css?family=Courgette' rel='stylesheet' type='text/css'>
</head>

<script type="text/javascript">

	$('#bus5').on('click', function (e) {
		plan = $('#bus5').val();
		redirectToPaypal(plan);
	});

	$('#bus10').on('click', function (e) {
		plan = $('#bus10').val();
		redirectToPaypal(plan);
	});

	$('#bus20').on('click', function (e) {
		plan = $('#bus20').val();
		redirectToPaypal(plan);
	});


	function redirectToPaypal(plan){
		$("#warning").hide();
		$("#wait").show();
	   
		$.get("asaas_block.php", { planId: plan })
		.done(function(data) {
		  window.location = data;
		});
	}


	$.ajax({
		type: 'post',
		url:'asaas_block_popup_language.php',
        dataType: "html",
        success: function(data) {
	        var retorno = jQuery.parseJSON(data);
	        console.log(retorno);
			if (retorno.status == "success") {
	            $('#title').text(retorno.dados.title);
	            $('#body_title').html(retorno.dados.body_title);
	            $('#choose_plan').text(retorno.dados.choose_plan);
	            $('#users5').html(retorno.dados.users);
	            $('#month5').html(retorno.dados.month);
	            $('#users10').html(retorno.dados.users);
	            $('#month10').html(retorno.dados.month);
	            $('#users20').html(retorno.dados.users);
	            $('#month20').html(retorno.dados.month);
	            $('#bus5').html(retorno.dados.buy);
	            $('#bus10').html(retorno.dados.buy);
	            $('#bus20').html(retorno.dados.buy);
	            $('#wait_title').html(retorno.dados.wait_title);
	            $('#wait_body').html(retorno.dados.wait_body);
			}
        }
        });


</script>

<div id="warning" class="font-family-users-month">
	<h3><div id="title"></div></h3>
	<p id="body_title">
	</p>
	<h4 style="font-weight: bold"><div id="choose_plan"></div></h4>

<table class="table fix-table">
	<thead>
		<tr>
			<th>
	      		<span class="label label-success size-label-success">
	      			<div class="font-users-price pos-users-5">5</div>
	      			<div id="users5" class="font-family-users-month pos-users-name"></div>
	      			<div class="color-price-month font-users-price pos-price-5">$400</div>
	      			<div id="month5" class="color-price-month font-family-users-month pos-month-5"></div>
	      		</span>
			</th>
			<th>
	      		<span class="label label-success size-label-success">
	      			<div class="font-users-price pos-users-10">10</div>
	      			<div id="users10" class="font-family-users-month pos-users-name"></div>
	      			<div class="color-price-month font-users-price pos-price-10">$720</div>
	      			<div id="month10" class="color-price-month font-family-users-month pos-month-10 "></div>
	      		</span>
			</th>
			<th>
	      		<span class="label label-success size-label-success">
	      			<div class="font-users-price pos-users-20">20</div>
	      			<div id="users20" class="font-family-users-month pos-users-name"></div>
	      			<div class="color-price-month font-users-price pos-price-20">$1280</div>
	      			<div id="month20" class="color-price-month font-family-users-month pos-month-20 "></div>
	      		</span>
			</th>
	</tr>
	</thead>
              <tbody>
                <tr>
                  <td><button id="bus5" value="BUS5" class="btn btn-primary btn-info btn-mini" type="button"></button></td>
                  <td><button id="bus10" value="BUS10" class="btn btn-primary btn-info btn-mini" type="button"></button></td>
                  <td><button id="bus20" value="BUS20" class="btn btn-primary btn-info btn-mini" type="button"></button></td>
                </tr>
              </tbody>
            </table>
</div>

<div id="wait" class="font-family-users-month hide-wait">
	<h3><div id="wait_title"></div></h3>
	<div id="wait_body"></div><br><br>
	<div align="center"><img class="icon" src="gfx/loading_asaas.gif"></div>
</div>



