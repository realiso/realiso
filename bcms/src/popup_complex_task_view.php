<?php
include_once "include.php";
include_once $handlers_ref . "grid/QueryGridUserTasks.php";

class ContextEvent extends FWDRunnable {
  public function run(){
  	$miTaskId = FWDWebLib::getObject('task_id')->getValue();
  	
    //teste para verificar se o sistema n�o est� sendo hakeado
    $moTaskTest = new WKFTask();
    $moTaskTest->fetchById($miTaskId);
    $moTaskTest->testPermissionToExecute($miTaskId);
    
  	$msJustification = FWDWebLib::getObject('task_justification')->getValue();  	
  	$miAction = FWDWebLib::getObject('action')->getValue(); 
  	
  	$moContextObject = new ISMSContextObject();
  	$moContext = $moContextObject->getContextObjectByContextId($moTaskTest->getFieldValue('task_context_id')); 	
  	$moContext->fetchById($moTaskTest->getFieldValue('task_context_id'));	
		$moContext->stateForward($miAction, $msJustification);	
		
		$moTask = new WKFTask();		
		$moTask->setFieldValue('task_date_accomplished', ISMSLib::ISMSTime());		
		$moTask->setFieldValue('task_is_visible', 0);	
		$moTask->update($miTaskId);
		
		$msNextTaskJS = "";
		$mbOpenNextTask = (bool)array_filter(explode(':',FWDWebLib::getObject('next_task_controller')->getValue()));
		if ($mbOpenNextTask) {
		  $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
		  $moHandlerTask = new QueryGridUserTasks(FWDWebLib::getConnection());
      $moHandlerTask->setUser($miUserId);
      $moHandlerTask->setOrderBy('task_date_created','-');
      $moHandlerTask->makeQuery();
      $moHandlerTask->executeQuery();
      if ($moHandlerTask->fetch()) {
  		  $msNextTaskJS = 'soWindow.setTimeout("'.ISMSLib::getTaskPopup($moHandlerTask->getFieldValue('task_id'),
  		                                        $moHandlerTask->getFieldValue('task_activity'),
  		                                        $moHandlerTask->getFieldValue('task_context_id'),
  		                                        $moHandlerTask->getFieldValue('task_context_type'),
  		                                        2).',1");';
      }
		}
		
		echo "soWindow = soPopUpManager.getPopUpById('popup_complex_task_view').getOpener();"
	        ."if (soWindow.refresh_grids)"
	        ."  soWindow.refresh_grids();"
	        .$msNextTaskJS
	        ."soPopUpManager.closePopUp('popup_complex_task_view');";
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new ContextEvent('context_event'));    
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){    
    $miAutoTaskOpen = FWDWebLib::getObject('auto_task_open')->getValue();
    if (!$miAutoTaskOpen) FWDWebLib::getObject('next_task')->setShouldDraw(false);
    if ($miAutoTaskOpen==2) FWDWebLib::getObject('next_task_controller')->checkItem(1);
    
    $mbDumpHTML = true;  
    $miTaskId = FWDWebLib::getObject('task_id')->getValue();
    
    $moTask = new WKFTask();
    $moTask->fetchById($miTaskId); 

    //teste para verificar se o sistema n�o est� sendo hakeado
    $moTask->testPermissionToExecute($miTaskId);
       
    FWDWebLib::getObject('task_date')->setValue(ISMSLib::getISMSDate($moTask->getFieldValue('task_date_created')));
    
    $moUser = new ISMSUser();
    $moUser->fetchById($moTask->getFieldValue('task_creator_id'));    
    FWDWebLib::getObject('task_inquirer')->setValue($moUser->getFieldValue('user_name'));
    
    FWDWebLib::getObject('task_name')->setValue(ISMSActivity::getDescription($moTask->getFieldValue('task_activity')));
    
    $moContextObject = new ISMSContextObject();
    //$moContextAssociation = $moContextObject->getContextObject(FWDWebLib::getObject('task_context_type')->getValue());
    $moContextAssociation = $moContextObject->getContextObjectByContextId($moTask->getFieldValue('task_context_id'));
    if($moContextAssociation){
      if($moContextAssociation->fetchById($moTask->getFieldValue('task_context_id'))){  
        // pega o objeto do primeiro contexto da associa��o
        $moContextObject1 = new ISMSContextObject();
        $miContext1Id = $moContextAssociation->getFirstContextId();
        $moContext1 = $moContextObject1->getContextObjectByContextId($miContext1Id);
        $moContext1->fetchById($miContext1Id);
        FWDWebLib::getObject('context1_type')->setValue($moContext1->getLabel().":");
        
    		$msContext1Name = $moContext1->getName();
    		if (strlen($msContext1Name) > 35) {
    			$msContext1Name = substr($msContext1Name,0,32)."...";
    		}
        $msContext1Link = "<a href='javascript:open_visualize(".$moContext1->getId().",".$moContext1->getContextType().",\"".uniqid()."\")'>".$msContext1Name."</a>";
        FWDWebLib::getObject('context1_name')->setAttrStringNoEscape(true);
        FWDWebLib::getObject('context1_name')->setValue($msContext1Link);
        
        // pega o objeto do segundo contexto da associa��o
        $moContextObject2 = new ISMSContextObject();
        $miContext2Id = $moContextAssociation->getSecondContextId();
        $moContext2 = $moContextObject2->getContextObjectByContextId($miContext2Id);
        $moContext2->fetchById($miContext2Id);
        FWDWebLib::getObject('context2_type')->setValue($moContext2->getLabel().":");
        
        $msContext2Name = $moContext2->getName();
    		if (strlen($msContext2Name) > 35) {
    			$msContext2Name = substr($msContext2Name,0,32)."...";
    		}
        $msContext2Link = "<a href='javascript:open_visualize(".$moContext2->getId().",".$moContext2->getContextType().",\"".uniqid()."\")'>".$msContext2Name."</a>";
        FWDWebLib::getObject('context2_name')->setAttrStringNoEscape(true);
        FWDWebLib::getObject('context2_name')->setValue($msContext2Link);
      }else{
        $mbDumpHTML = false;
      }
    }else{
      $mbDumpHTML = false;
    }
    if($mbDumpHTML){
        FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    }else{
        return FWDLanguage::getPHPStringValue('st_denied_permission_to_execute_task','Voc� n�o tem permiss�o para executar a tarefa! ');
    }
    ?>
      <script language="javascript">
        gebi('task_justification').focus();
      </script>
    <? 
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_complex_task_view.xml");
?>