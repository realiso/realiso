<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportWindow.
 *
 * <p>Classe que implementa a popup report.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportWindow {

	/**
	 * Indica se o loading ja foi completado ou n�o
	 * @var boolean
	 * @access protected
	 */
	protected $cbWaitLoadingComplete = false;

	/**
	 * Constante para indicar o path dos relat�rios
	 * @var boolean
	 * @access protected
	 */
	protected $csReportPath = '';

	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe ISMSReportWindow.</p>
	 * @access public
	 */
	public function __construct($piReportFilesRef) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		if($this->cbWaitLoadingComplete) echo "soTabManager.lockTabs();";
		switch ($piReportFilesRef) {
			case ADMIN_MODE:
				$this->csReportPath = 'packages/admin/report/';
				break;

			case RISK_MANAGEMENT_MODE:
				$this->csReportPath = 'packages/risk/report/';
				break;
			case POLICY_MODE:
				$this->csReportPath = 'packages/policy/report/';
				break;
			case INCIDENT_MODE:
				$this->csReportPath = 'packages/improvement/report/';
				break;
		}
		FWDStartEvent::getInstance()->addBeforeAjax(new ISMSReportHideLoadingEvent("hide_loading_report_isms"),HIGH);
	}

	/**
	 * Seta o valor booleano do atributo cbWaitLoadingComplete.
	 *
	 * <p>Seta o valor booleano do atributo cbWaitLoadingComplete.</p>
	 * @access public
	 * @param boolean $pbWaitLoadingComplete Define se deve esperar o loading terminar
	 */
	public function setWaitLoadingComplete($pbWaitLoadingComplete) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		$this->cbWaitLoadingComplete = $pbWaitLoadingComplete;
	}

	/**
	 * Abre a popup de relat�rio.
	 *
	 * <p>Retorna o c�digo javascript para abrir a popup de relat�rio.</p>
	 * @access public
	 * @return string C�digo javascript para abrir a popup de relat�rio
	 */
	public function open() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		/*
		 * Relat�rio em uma popup normal
		 */
		//$msOut = "win = js_open('{$this->csReportPath}loading_report.phploading_report.php','popup_report',740,502,'center_screen');";

		/*
		 * Relat�rio em uma div popup
		 */
		if (FWDWEbLIb::browserIsIE()) $msOut = "isms_open_popup('popup_report','{$this->csReportPath}loading_report.php','','true',502,732);";
		else $msOut = "isms_open_popup('popup_report','{$this->csReportPath}loading_report.php','','true',502,740);";
		$msOut .= "win = soPopUpManager.getPopUpById('popup_report').getWindow();";

		echo $msOut;
	}

	/**
	 * Carrega o relat�rio.
	 *
	 * <p>Carrega o relat�rio.</p>
	 * @access public
	 * @param string $psPopup Popup
	 * @return string C�digo javascript para carregar o relat�rio
	 */
	public function loadReport($psPopup) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		$msOut = 'if ((win)&&(win!="undefined")){ ';

		/*
		 * TEM QUE VERIFICAR ISSO DEPOIS!
		 * NO FIREFOX N�O FUNCIONA E NO IE ENTRA EM LOOP!
		 */
		//$msOut.= "alert('Gerando Relat�rio!');";
		//if($this->cbWaitLoadingComplete)
		//$msOut .= 'while(win.document.readyState == "loading") ;';

		$msSysRef = FWDWebLib::getInstance()->getSysRef();
		$msOut .= 'win.location = "'.$msSysRef.$this->csReportPath.'popup_report.php?popup='.$psPopup.'";}';
		echo $msOut;
	}

	/**
	 * For�a o download do relat�rio.
	 *
	 * <p>M�todo para for�ar o download do relat�rio.</p>
	 * @access public
	 * @param string $psPopup Popup
	 * @return string C�digo javascript para carregar o relat�rio
	 */
	public function forceReportDownload($psPopup){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		$moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
		$msId = sprintf("#%x",crc32("report_has_finished" . $_SERVER['SCRIPT_FILENAME']));
		if($moSession->attributeExists($msId)){
			$miCont = $moSession->{'getAttr'.$msId}();
			$miCont=$miCont+2;
			$moSession->{'setAttr'.$msId}($miCont);
		}else{
			$moSession->addAttribute($msId);
			$moSession->{'setAttr'.$msId}('2');
		}
		$moSession->commit();

		$msSysRef = FWDWebLib::getInstance()->getSysRef();
		echo "soTabManager.lockTabs();gebi('force_report_download_iframe').src = '{$msSysRef}{$this->csReportPath}popup_report.php?popup=$psPopup';window.setTimeout(\"gebi('FWDLoading').style.display = 'block'\",1);window.setTimeout(\"trigger_event('hide_loading_report_isms',1)\",2000);";
	}

	/**
	 * Muda a p�gina do relat�rio.
	 *
	 * <p>Muda a p�gina do relat�rio.</p>
	 * @access public
	 * @return string C�digo javascript para mudar a p�gina do relat�rio
	 */
	public function changeWindow() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		$msOut = 'if(parent.win){';
		if ($this->cbWaitLoadingComplete)
		$msOut .= 'while(parent.win.document.readyState == "loading");';
		$msOut .= '}';
		echo $msOut;
	}

	/**
	 * Retorna o logo do sistema para coloar na janela que abre o relat�rio.
	 *
	 * <p>M�todo para retornar o logo do sistema para coloar na janela que abre o relat�rio.</p>
	 * @access public
	 * @return string Imagem do Logo
	 */
	public static function getReportSystemLogo() {
		$moLicense = new ISMSLicense();
		$msSrc = '';
		if ($moLicense->isBCMS()) $msSrc = 'realBCMS_no_bg.png';
		else if ($moLicense->isISMS()) $msSrc = 'realISMS_no_bg.png';
		else if ($moLicense->isEMS()) $msSrc = 'realEMS_no_bg.png';
		else if ($moLicense->isOHS()) $msSrc = 'realOHS_no_bg.png';
		else if ($moLicense->isSOX() && $moLicense->isMinima()) $msSrc = 'minimaSOX_no_bg.png';
		else if ($moLicense->isSOX()) $msSrc = 'realSOX_no_bg.png';
		else if ($moLicense->isPCI()) $msSrc = 'realPCI_no_bg.png';
		return $msSrc;
	}
}
?>