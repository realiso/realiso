<?php
include_once "include.php";
include_once $handlers_ref . "QueryContextHierarchicalStructure.php";
include_once $handlers_ref . "select/QuerySelectProcess.php";
include_once $handlers_ref . "select/continuity/QuerySelectPlace.php";
include_once $handlers_ref . "select/QuerySelectParameters.php";
include_once $handlers_ref . "QueryContextClassification.php";
include_once $handlers_ref . "select/continuity/QuerySelectPlaceType.php";
include_once $handlers_ref . "select/QuerySelectStandard.php";
include_once $handlers_ref . "select/QuerySelectAsset.php";
include_once $handlers_ref . "QueryGetReportClassification.php";
include_once $handlers_ref . "select/QuerySelectArea.php";
include_once $handlers_ref . "select/QuerySelectPlan.php";
include_once $handlers_ref . "select/QuerySelectPriority.php";
include_once $handlers_ref . "grid/policy/QueryGridDocuments.php";


/*
 * Colocar aqui todos os relat�rios que possuem filtros.
 * Para cada linha (relat�rio) da grid � criado um evento
 * que faz com que quando um relat�rio seja selecionado,
 * ele esconda todos os filtros da tela e mostre somente
 * os filtros pertinentes ao relat�rio clicado.
 */
class GridReports extends FWDDrawGrid {
	public function drawItem(){
		switch($this->ciColumnIndex){
			case 2:
				$moEvent = new FWDClientEvent();
				$moEvent->setAttrEvent('onClick');
				$msEvent = "hide_all_filters();hide_all_warnings();";
				switch ($this->caData[1]) {
					case 'report_risks_by_process':
						$msEvent .= "show_filter('report_risk_filter');";
						$msEvent .= "show_filter('report_risks_by_process_filter');";
						break;
					case 'report_bia_quantitative':
						$msEvent .= "show_filter('report_bia_quantitative_filter');";
						break;
					case 'report_bia_qualitative':
						$msEvent .= "show_filter('report_bia_qualitative_filter');";
						break;
					case 'report_asa':
						$msEvent .= "show_filter('report_asa_filter');";
						break;
					case 'report_asa_by_processes':
						$msEvent .= "show_filter('report_asa_by_processes_filter');";
						break;
					case 'report_asset_by_asset':
						$msEvent .= "show_filter('report_asset_by_asset_filter');";
						break;
					case 'report_asset_by_process':
						$msEvent .= "show_filter('report_asset_by_process_filter');";
						break;						
					case 'report_tests':
						$msEvent .= "show_filter('report_tests_filter');";
						break;
					case 'report_activities':
						$msEvent .= "show_filter('report_activities_filter');";
						break;
					case 'report_asset':
						$msEvent .= "show_filter('report_asset_filter');";
						break;
					case 'report_scope':
						$msEvent .= "show_filter('report_scope_filter');";
						break;
					//case 'report_process_by_process':
					//	$msEvent .= "show_filter('report_process_by_process_filter');";
					//	break;
					case 'report_process_by_asset':
						$msEvent .= "show_filter('report_process_by_asset_filter');";
						break;
					
					case 'report_asset_matrix':
						$msEvent .= "show_filter('report_asset_matrix_filter');";
						break;
					case 'report_places':
						$msEvent .= "show_filter('report_places_filter');";
						break;
					case 'report_areas':
						$msEvent .= "show_filter('report_areas_filter');";
						break;
					case 'report_processes':
						$msEvent .= "show_filter('report_processes_filter');";
						break;
					default:
						$msEvent .= "js_show('no_filter_available');";
						break;
				}
				$moEvent->setAttrValue($msEvent);
				$this->coCellBox->addObjFWDEvent($moEvent);
				return parent::drawItem();
				break;

			default:
				return parent::drawItem();
				break;
		}
	}
}

class ISMSReportEvent extends FWDRunnable {
	private function getReportRisksByProcessFilter() {
		$moReportFilter = $this->getReportRiskValuesFilter(new ISMSReportRisksByProcessFilter());

		$miProcess = FWDWebLib::getObject('select_process')->getValue();
		if ($miProcess) $moReportFilter->setProcess($miProcess);
		$maCheckboxValue = FWDWebLib::getObject('report_risks_by_process_checkbox')->getAllItemsCheck();
		if(count($maCheckboxValue)) $moReportFilter->organizeByAsset();
		return $moReportFilter;
	}

	private function getReportBiaQuantitativeFilter() {
		$moReportFilter = new ISMSReportBiaQuantitativeFilter();
		$place = FWDWebLib::getObject('select_place')->getValue();
		if ($place){
			$moReportFilter->setPlace($place);
		}
		return $moReportFilter;
	}

	private function getReportBiaQualitativeFilter() {
		$moReportFilter = new ISMSReportBiaQualitativeFilter();
		$place = FWDWebLib::getObject('select_place_qualitative')->getValue();
		if ($place){
			$moReportFilter->setPlace($place);
		}
		return $moReportFilter;
	}

	private function getReportTestsFilter() {
		$moReportFilter = new ISMSReportTestsFilter();
		return $moReportFilter;
	}

	private function getReportActivitiesFilter() {
		$moReportFilter = new ISMSReportActivitiesFilter();
		return $moReportFilter;
	}

	private function getReportAssetFilter() {
		$moReportFilter = new ISMSReportAssetFilter();
		return $moReportFilter;
	}

	private function getReportScopeFilter() {
		$moReportFilter = new ISMSReportScopeFilter();
		return $moReportFilter;
	}

	private function getReportProcessByProcessFilter() {
		$moReportFilter = new ISMSReportProcessByProcessFilter();
		return $moReportFilter;
	}

	private function getReportProcessByAssetFilter() {
		$moReportFilter = new ISMSReportProcessByAssetFilter();
		return $moReportFilter;
	}

	private function getReportProcessesDependenceFilter() {
		$moReportFilter = new ISMSReportProcessesDependenceFilter();
		return $moReportFilter;
	}
	
	private function getReportASAByAssetsFilter() {
		$moReportFilter = new ISMSReportASAByAssetsFilter();
		return $moReportFilter;
	}
	
	private function getReportAssetMatrixFilter() {
		$moReportFilter = new ISMSReportAssetMatrixFilter();
		return $moReportFilter;
	}

	private function getReportPlacesFilter() {
		$moReportFilter = new ISMSReportPlacesFilter();
		return $moReportFilter;
	}

	private function getReportAreasFilter() {
		$moReportFilter = new ISMSReportAreasFilter();
		return $moReportFilter;
	}


	private function getReportProcessesFilter() {
		$moReportFilter = new ISMSReportProcessesFilter();
		return $moReportFilter;
	}
	
	private function getReportASAFilter() {
		$moReportFilter = new ISMSReportASAFilter();

		$place = FWDWebLib::getObject('select_place_asa')->getValue();
		if ($place){
			$moReportFilter->setPlace($place);
		}

		$area = FWDWebLib::getObject('select_area_asa')->getValue();
		if ($area){
			$moReportFilter->setArea($area);
		}

		$process = FWDWebLib::getObject('select_process_asa')->getValue();
		if ($process){
			$moReportFilter->setProcess($process);
		}

		return $moReportFilter;
	}
	
	private function getReportASAByProcessesFilter() {
		$moReportFilter = new ISMSReportASAByProcessesFilter();
		
		$place = FWDWebLib::getObject('select_place_asa_processes')->getValue();
		if ($place){
			$moReportFilter->setPlace($place);
		}
		
		$area = FWDWebLib::getObject('select_area_asa_processes')->getValue();
		if ($area){
			$moReportFilter->setArea($area);
		}
		
		$process = FWDWebLib::getObject('select_process_asa_processes')->getValue();
		if ($process){
			$moReportFilter->setProcess($process);
		}
		
		return $moReportFilter;
	}
	
	private function getReportAssetByAssetFilter() {
		$moReportFilter = new ISMSReportAssetByAssetFilter();
		
		
		$place = FWDWebLib::getObject('select_place_asset_by_asset')->getValue();
		if ($place){
			$moReportFilter->setPlace($place);
		}
		
		$area = FWDWebLib::getObject('select_area_asset_by_asset')->getValue();
		if ($area){
			$moReportFilter->setArea($area);
		}
		
		$process = FWDWebLib::getObject('select_process_asset_by_asset')->getValue();
		if ($process){
			$moReportFilter->setProcess($process);
		}
		
		return $moReportFilter;
	}
	
	private function getReportAssetByProcessFilter() {
		$moReportFilter = new ISMSReportAssetByProcessFilter();

		$place = FWDWebLib::getObject('select_place_asset_by_process')->getValue();
		if ($place){
			$moReportFilter->setPlace($place);
		}

		$area = FWDWebLib::getObject('select_area_asset_by_process')->getValue();
		if ($area){
			$moReportFilter->setArea($area);
		}

		return $moReportFilter;
	}	

	private function getReportRiskValuesFilter($poFilterObject) {
		$maCheckboxValue = FWDWebLib::getObject('risk_filter_checkbox')->getAllItemsCheck();
		$msRadioboxValue = FWDWebLib::getObject('risk_filter_radiobox')->getValue();

		if ($msRadioboxValue == ':residual') $poFilterObject->setRiskValueType(RISK_VALUE_TYPE_RESIDUAL_VALUE);
		else if ($msRadioboxValue == ':real') $poFilterObject->setRiskValueType(RISK_VALUE_TYPE_REAL_VALUE);

		foreach($maCheckboxValue as $moItem) {
			switch ($moItem->getAttrKey()) {
				case 'high':
					$poFilterObject->showHighRisk(true);
					break;
				case 'medium':
					$poFilterObject->showMediumRisk(true);
					break;
				case 'low':
					$poFilterObject->showLowRisk(true);
					break;
				case 'no_values':
					//criado esse teste pois o relat�rio de riscos por controle n�o deve ter o filtro
					//de riscos desparametrizados, pois esse caso n�o deve ocorrer no sistema
					//jah q o sistema somente deixa os riscos parametrizados serem relacionados
					//com os controles
					if(FWDWebLib::getObject('var_pog_disable_risk_no_value')->getValue()=='false')
					$poFilterObject->showRiskWithNoValues(true);
					break;
			}
		}

		return $poFilterObject;
	}

	/*
	 * Essa fun��o serve para retornar o filtro do relat�rio.
	 * De acordo com o relat�rio selecionado, chama um fun��o
	 * espec�fica para montar o filtro do relat�rio em quest�o.
	 */
	private function getFilter() {
		$maValue = FWDWebLib::getObject('grid_reports')->getValue();
		$moReportFilter = null;

		switch($maValue[0]) {
			case 'report_risks_by_process':
				$moReportFilter = $this->getReportRisksByProcessFilter();
				break;
			case 'report_bia_quantitative':
				$moReportFilter = $this->getReportBiaQuantitativeFilter();
				break;
			case 'report_bia_qualitative':
				$moReportFilter = $this->getReportBiaQualitativeFilter();
				break;
			case 'report_asa':
				$moReportFilter = $this->getReportASAFilter();
				break;
			case 'report_asa_by_processes':
				$moReportFilter = $this->getReportASAByProcessesFilter();
				break;
			case 'report_asset_by_asset':
				$moReportFilter = $this->getReportAssetByAssetFilter();
				break;
			case 'report_asset_by_process':
				$moReportFilter = $this->getReportAssetByProcessFilter();
				break;
			case 'report_tests':
				$moReportFilter = $this->getReportTestsFilter();
				break;
			case 'report_activities':
				$moReportFilter = $this->getReportActivitiesFilter();
				break;
			case 'report_asset':
				$moReportFilter = $this->getReportAssetFilter();
				break;
			case 'report_scope':
				$moReportFilter = $this->getReportScopeFilter();
				break;
			//case 'report_process_by_process':
			//	$moReportFilter = $this->getReportProcessByProcessFilter();
			//	break;
			case 'report_process_by_asset':
				$moReportFilter = $this->getReportProcessByAssetFilter();
				break;
			case 'report_processes_dependence':
				$moReportFilter = $this->getReportProcessesDependenceFilter();
				break;
			case 'report_asa_by_assets':
				$moReportFilter = $this->getReportASAByAssetsFilter();
				break;
			case 'report_places':
				$moReportFilter = $this->getReportPlacesFilter();
				break;
			case 'report_areas':
				$moReportFilter = $this->getReportAreasFilter();
				break;
			case 'report_processes':
				$moReportFilter = $this->getReportProcessesFilter();
				break;
			case 'report_asset_matrix':
				$moReportFilter = $this->getReportAssetMatrixFilter();
				break;				
			default:
				$moReportFilter = new FWDReportFilter();
				break;

		}

		$moReportFilter->setManualClassification(FWDWebLib::getObject('select_report_classification')->getValue());
		$moReportFilter->setFileType(FWDWebLib::getObject('select_report_type')->getValue());

		$placeType = FWDWebLib::getObject('sel_area_type')->getValue();
		$processType = FWDWebLib::getObject('sel_process_type')->getValue();
		$processPriority = FWDWebLib::getObject('sel_process_priority')->getValue();

		$filters = FWDWebLib::getObject('ctrl_type_prio')->getValue();

		$pos = strpos($filters,"chk_area_type");
		if($pos !== false) {
			if($placeType){
				$moReportFilter->setPlaceType($placeType);
			}
		}

		$pos = strpos($filters,"chk_process_type");
		if($pos !== false) {
			if($processType){
				$moReportFilter->setProcessType($processType);
			}
		}
		$pos = strpos($filters,"chk_process_priority");
		if($pos !== false) {
			if($processPriority){
				$moReportFilter->setProcessPriority($processPriority);
			}
		}

		FWDWebLib::getObject('report_comment')->getObjFWDString()->setAttrNoEscape('true');

		$moReportFilter->setComment(FWDWebLib::convertToISO(FWDWebLib::getObject('report_comment')->getValue(), true));
		return $moReportFilter;
	}

	/*
	 * Fun��o para retornar o relat�rio que deve ser gerado.
	 * � utilizada quando o relat�rio que foi clicado deve
	 * chamar um outro relat�rio.
	 */
	private function getReport() {
		$maGridValue = FWDWebLib::getObject('grid_reports')->getValue();
		$msReport = $maGridValue[0];

		switch ($msReport) {
			default:
				return $msReport;
				break;
		}
	}

	/*
	 * Verifica se o filtro do relat�rio foi preenchido corretamente.
	 */
	public function validateFilters() {
		$maValue = FWDWebLib::getObject('grid_reports')->getValue();
		switch($maValue[0]) {
			case 'report_risks_by_process':
				if (count(FWDWebLib::getObject('risk_filter_checkbox')->getAllItemsCheck())) return true;
				else echo "js_show('report_risk_filter_warning');";
				break;
			default:
				return true;
				break;
		}
	}

	public function run(){
		$maValue = FWDWebLib::getObject('grid_reports')->getValue();
		if (isset($maValue[0])) {
			if ($this->validateFilters()) {
				set_time_limit(3000);

				$mbDontForceDownload = (FWDWebLib::getObject("select_report_type")->getValue()==REPORT_FILETYPE_HTML);

				$moWindowReport = new ISMSReportWindow(RISK_MANAGEMENT_MODE);
				if($mbDontForceDownload) $moWindowReport->open();

				$moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
				$moSession->deleteAttribute("filter");
				$moSession->addAttribute("filter");
				$moSession->setAttrFilter($this->getFilter());
				$msReport = $this->getReport();
				if($mbDontForceDownload){
					$moWindowReport->setWaitLoadingComplete(true);
					$moWindowReport->loadReport($msReport);
				}
				else $moWindowReport->forceReportDownload($msReport);
			}
		}
		else echo "js_show('warning');";
	}
}

class ISMSPreReportEvent extends FWDRunnable {
	public function run(){
		set_time_limit(3000);

		$moWindowReport = new ISMSReportWindow(RISK_MANAGEMENT_MODE);
		$moWindowReport->open();

		$moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
		$moSession->deleteAttribute("filter");
		$moSession->addAttribute("filter");
		$moSession->deleteAttribute("isPreReport");
		$moSession->addAttribute("isPreReport");
		$moSession->setAttrIsPreReport("true2");

		//filtro de norma
		$moReportFilter = new ISMSReportStatementOfApplicabilityFilter();
		$miId = FWDWebLib::getObject('var_stardard_filter_id')->getValue();
		if($miId){
			$moStd = new RMStandard();
			$moStd->fetchById($miId);
			$moReportFilter->setStandardFilter($miId);
			$moReportFilter->setStandardFilterName($moStd->getFieldValue('standard_name'));
		}else{
			$moReportFilter->setStandardFilterName(FWDLanguage::getPHPStringValue('st_all_standards', 'Todas'));
		}
		$moSession->setAttrFilter($moReportFilter);

		$moWindowReport->setWaitLoadingComplete(true);
		$moWindowReport->loadReport('report_statement_of_applicability');
	}
}

class ISMSConfirmClearPreReportEvent extends FWDRunnable {
	public function run () {
		$msTitle = FWDLanguage::getPHPStringValue('tt_clear_justificatives','Confirmar limpeza de justificativas');
		$msMessage = FWDLanguage::getPHPStringValue('st_clear_justificatives_confirm',"Voc� tem certeza que deseja limpar as justificativas?");
		$msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
                    "soWindow = soPopUp.getOpener().trigger_event('clear_pre_report_confirmed', 1);";
		ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue);
	}
}

class ISMSClearPreReportEvent extends FWDRunnable {
	public function run() {
		$moBestPractice = new RMBestPractice();
		$moBestPractice->cleanJustifications();
	}
}

/*
 * Fun��o que retorna um array associativo do tipo array('report_category' => array('report1', 'report2', ...))
 * para mapear quais relat�rios pertencem a quais categorias
 */
function getReportCategoryArray() {

	$reportNames = array();

	$reportNames['report_bia_quantitative'] = array(FWDLanguage::getPHPStringValue('bia_quantitative',"BIA Quantitativo"),"");
	$reportNames['report_bia_qualitative'] = array(FWDLanguage::getPHPStringValue('bia_qualitative',"BIA Qualitativo"),"");
	$reportNames['report_asa'] = array(FWDLanguage::getPHPStringValue('asa',"An�lise de Sustentabilidade de Ativos"),"");
	$reportNames['report_asa_by_processes'] = array(FWDLanguage::getPHPStringValue('reportASAByProcesses',"ASA X Processos"),"");
	$reportNames['report_asset_by_asset'] = array(FWDLanguage::getPHPStringValue('reportAssetByAsset',"Ativo X Ativo"),"");
	$reportNames['report_asset_by_process'] = array(FWDLanguage::getPHPStringValue('reportAssetByProcess',"Ativos X Processos"),"");
	$reportNames['report_tests'] = array(FWDLanguage::getPHPStringValue('reportTestsName',"Testes"),"");

	if(!ISMSConfig::getConfigFromDB(ASSET_KEY))
		$reportNames['report_activities'] = array(FWDLanguage::getPHPStringValue('reportActivitiesName',"Atividades"),"");

	$reportNames['report_asset'] = array(FWDLanguage::getPHPStringValue('reportAssetName',"Ativos"),"");
	$reportNames['report_scope'] = array(FWDLanguage::getPHPStringValue('reportScopeName',"Declara��o de Escopo"),"");
	//$reportNames['report_process_by_process'] = array(FWDLanguage::getPHPStringValue('reportProcessByProcessName',"Processos X Processos"),"");
	$reportNames['report_process_by_asset'] = array(FWDLanguage::getPHPStringValue('reportProcessByAssetName',"Processos X Ativos"),"");
	$reportNames['report_processes_dependence'] = array(FWDLanguage::getPHPStringValue('reportProcessesDependence',"Depend�ncia de Processos"),"");
	$reportNames['report_asa_by_assets'] = array(FWDLanguage::getPHPStringValue('reportASAByAssets',"ASA x Ativos"),"");
	$reportNames['report_asset_matrix'] = array(FWDLanguage::getPHPStringValue('reportAssetMatrixName',"Matriz de Recupera��o de Ativos"),"");
	$reportNames['report_places'] = array(FWDLanguage::getPHPStringValue('reportPlacesName',"Locais"),"");
	$reportNames['report_processes'] = array(FWDLanguage::getPHPStringValue('reportProcessesName',"Processos"),"");
	$reportNames['report_areas'] = array(FWDLanguage::getPHPStringValue('reportAreasName',"Unidades de Neg�cio"),"");

	return array('risk_reports' => $reportNames);
	
}

/*
 * Fun��o que popula a grid com os relat�rios de
 * acordo com a op��o do filtro de relat�rios
 */
function showReports() {
	echo "hide_all_filters();hide_all_warnings();js_show('no_filter_available');";

	$moGrid = FWDWebLib::getObject('grid_reports');
	$moGrid->setObjFwdDrawGrid(new GridReports());
	//	$msReportCategory = FWDWebLib::getObject("report_category")->getValue();
	$maReportCategory = getReportCategoryArray();
	$maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();

	//	if ($msReportCategory != 'all_reports') {
	//		$maReports = $maReportCategory[$msReportCategory];
	//		$miLineCount = 0;
	//		foreach ($maReports as $msReportKey => $maReportACL) {
	//			if(!in_array($maReportACL[1] , $maACLs)){
	//				$miLineCount++;
	//				$moGrid->setItem(1, $miLineCount, $msReportKey);
	//				$moGrid->setItem(2, $miLineCount, $maReportACL[0]);
	//			}
	//		}
	//	}
	//	else {
	$miLineCount = 0;
	foreach ($maReportCategory as $maReports) {
		foreach ($maReports as $msReportKey => $maReportACL) {
			if(!in_array($maReportACL[1] , $maACLs)){
				$miLineCount++;
				$moGrid->setItem(1, $miLineCount, $msReportKey);
				$moGrid->setItem(2, $miLineCount, $maReportACL[0]);
			}
		}
	}
	//	}
	$moGrid->execEventPopulate();
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));
		$moStartEvent->addAjaxEvent(new ISMSReportEvent("function_report"));
		$moStartEvent->addAjaxEvent(new ISMSPreReportEvent("function_pre_report"));
		$moStartEvent->addAjaxEvent(new ISMSConfirmClearPreReportEvent("function_clear_pre_report"));
		$moStartEvent->addAjaxEvent(new ISMSClearPreReportEvent("clear_pre_report_confirmed"));
		$moStartEvent->addAjaxEvent(new ShowReportsEvent("show_reports"));
		$moStartEvent->addAjaxEvent(new PopulateProcess3Event("populate_process3"));
		$moStartEvent->addAjaxEvent(new PopulateAsset3Event("populate_asset3"));
	}
}

class PopulateProcess3Event extends FWDRunnable {
	public function run(){
		$moSelectArea3 = FWDWebLib::getObject('select_area3');
		$areaId = $moSelectArea3->getValue();

		$moQuery = new QuerySelectProcess(FWDWebLib::getConnection());
		$moQuery->setArea($areaId);

		$moSelect = FWDWebLib::getObject('select_process3');
		$moSelect->setQueryHandler($moQuery);
		$moSelect->populate();
		$moSelect->execEventPopulate();

		$moSelectProcess3 = FWDWebLib::getObject('select_process3');
		$processId = $moSelectProcess3->getValue();
	}
}

class PopulateAsset3Event extends FWDRunnable {
	public function run(){
		$moSelectProcess3 = FWDWebLib::getObject('select_process3');
		$processId = $moSelectProcess3->getValue();

		$moSelectArea3 = FWDWebLib::getObject('select_area3');
		$areaId = $moSelectArea3->getValue();
	}
}

class ShowReportsEvent extends FWDRunnable {
	public function run(){
		showReports();
	}
}

function getSelClassifPrio($psKey){

	$moReturn =
	(strstr($psKey,"type_process") ? FWDWebLib::getObject('sel_process_type') :
	(strstr($psKey,"prio_area") ? FWDWebLib::getObject('sel_area_priority') : null));

	return $moReturn;
}

class ScreenEvent extends FWDRunnable {
	public function run(){

		$moConfig = new ISMSConfig();
		if($moConfig->getConfigFromDB(CLIENT_ID_ASAAS)) {
			$moISMSASaaS = new ISMSASaaS();
			$moISMSASaaS->setNewCheckPointASaaS(utf8_encode("Acessou M�dulo Relat�rios"), utf8_encode("M�dulo Dashboard"));
		}

		$moWebLib = FWDWebLib::getInstance();
		$moGrid = $moWebLib->getObject('grid_reports');
		$moGrid->setObjFwdDrawGrid(new GridReports());

		/*
		 * Popula o select com os processos para o filtro do relat�rio de riscos por processo
		 */
		$moHandler = new QuerySelectProcess(FWDWebLib::getConnection());
		$moSelect = FWDWebLib::getObject('select_process');
		$moSelect->setQueryHandler($moHandler);
		$moSelect->populate();

		/*
		 * Popula o select com os Locais
		 */
		$moHandler = new QuerySelectPlace(FWDWebLib::getConnection());
		$moSelect = FWDWebLib::getObject('select_place');
		$moSelect->setQueryHandler($moHandler);
		$moSelect->populate();

		$moSelect = FWDWebLib::getObject('select_place_asa');
		$moSelect->setQueryHandler($moHandler);
		$moSelect->populate();
		
		$moSelect = FWDWebLib::getObject('select_place_asa_processes');
		$moSelect->setQueryHandler($moHandler);
		$moSelect->populate();
		
		$moSelect = FWDWebLib::getObject('select_place_asset_by_asset');
		$moSelect->setQueryHandler($moHandler);
		$moSelect->populate();

		$moSelect = FWDWebLib::getObject('select_place_qualitative');
		$moSelect->setQueryHandler($moHandler);
		$moSelect->populate();
		
		$moSelect = FWDWebLib::getObject('select_place_asset_by_process');
		$moSelect->setQueryHandler($moHandler);
		$moSelect->populate();		

		$moHandler = new QuerySelectProcess(FWDWebLib::getConnection());
		$moSelect = FWDWebLib::getObject('select_process_asa');
		$moSelect->setQueryHandler($moHandler);
		$moSelect->populate();
		
		$moHandler = new QuerySelectProcess(FWDWebLib::getConnection());
		$moSelect = FWDWebLib::getObject('select_process_asa_processes');
		$moSelect->setQueryHandler($moHandler);
		$moSelect->populate();
		
		$moHandler = new QuerySelectProcess(FWDWebLib::getConnection());
		$moSelect = FWDWebLib::getObject('select_process_asset_by_asset');
		$moSelect->setQueryHandler($moHandler);
		$moSelect->populate();
		
		$moHandler = new QuerySelectArea(FWDWebLib::getConnection());
		$moSelect = FWDWebLib::getObject('select_area_asa');
		$moSelect->setQueryHandler($moHandler);
		$moSelect->populate();
		
		$moHandler = new QuerySelectArea(FWDWebLib::getConnection());
		$moSelect = FWDWebLib::getObject('select_area_asa_processes');
		$moSelect->setQueryHandler($moHandler);
		$moSelect->populate();
		
		$moHandler = new QuerySelectArea(FWDWebLib::getConnection());
		$moSelect = FWDWebLib::getObject('select_area_asset_by_asset');
		$moSelect->setQueryHandler($moHandler);
		$moSelect->populate();
		
		$moHandler = new QuerySelectArea(FWDWebLib::getConnection());
		$moSelect = FWDWebLib::getObject('select_area_asset_by_process');
		$moSelect->setQueryHandler($moHandler);
		$moSelect->populate();		

		/*
		 * Popula o select com os par�metros do risco
		 */
		$moHandler = new QuerySelectParameters(FWDWebLib::getConnection());
		$moSelect = FWDWebLib::getObject('select_parameters');
		$moSelect->setQueryHandler($moHandler);
		$moSelect->populate();

		/*
		 * Popula o select com os tipos de formato de relat�rios e
		 * o select com os tipos de classifica��o de relat�rios.
		 */
		$moFormatSelect = FWDWebLib::getObject('select_report_type');
		$maAvailableFormats = FWDReportGenerator::getAvailableFormats(ISMSLib::getSupportedReportFormats());
		foreach ($maAvailableFormats as $miKey => $msValue) {
			$moFormatSelect->setItemValue($miKey, $msValue);
		}
		$moHandler = new QueryGetReportClassification(FWDWebLib::getConnection());
		$moHandler->makeQuery();
		$maAvailableClassifications = $moHandler->executeQuery();
		$moClassificationSelect = FWDWebLib::getObject('select_report_classification');
		foreach ($maAvailableClassifications as $miKey => $msValue) {
			$moClassificationSelect->setItemValue($msValue, $msValue);
		}

		//popula os filtros de classifica��o
		$moClassifHandle = new QueryContextClassification(FWDWebLib::getConnection());
		$moClassifHandle->makeQuery();
		$moClassifHandle->executeQuery();
		$maClassifIds = $moClassifHandle->getClassificationIds();
		$maClassifNames = $moClassifHandle->getClassificationNames();
		foreach($maClassifIds as $msKey=>$miValue){
			$moSel = getSelClassifPrio($msKey);
			$moItem = new FWDItem();
			$moItem->setAttrKey($miValue);
			$moItem->setValue($maClassifNames[$msKey]);
			if($moSel){
				$moSel->addObjFWDItem($moItem);
			}
		}

		//Popula o filtro de prioridade
		$selectPriority = FWDWebLib::getObject('sel_process_priority');
		$query = new QuerySelectPriority(FWDWebLib::getConnection());
		$query->makeQuery();
		$query->executeQuery();
		while($query->fetch()){
			$moItem = new FWDItem();
			$moItem->setAttrKey($query->getFieldValue('select_id'));
			$moItem->setValue($query->getFieldValue('select_value'));
			$selectPriority->addObjFWDItem($moItem);
		}

		//Popula o filtro de tipo de local
		$selectAreaType = FWDWebLib::getObject('sel_area_type');
		$query = new QuerySelectPlaceType(FWDWebLib::getConnection());
		$query->makeQuery();
		$query->executeQuery();
		foreach ($query->getContexts() as $type) {
			list($miContextId,$msHierarchicalName) = $type;
			$moItem = new FWDItem();
			$moItem->setAttrKey($miContextId);
			$moItem->setValue($msHierarchicalName);
			$selectAreaType->addObjFWDItem($moItem);
		}

		//instala a seguran�a de ACL na p�gina
		FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());

		//desabilita o item do select referente aos relat�rios de custo caso o sistema
		//n�o esteja gerenciando os custos
		$moConfig = new ISMSConfig();
		if(!$moConfig->getConfig(GENERAL_COST_ENABLED)){
			//FWDWebLib::getObject('financial_reports_item')->setShouldDraw(false);
		}
		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
<script language="javascript">
        gobi('grid_reports').setPopulate(true);
        trigger_event('show_reports',3);
        var ssTypePrioSelId = '';
        function show_sel_type_prio_by_checkbox(psSelId){
          if(gobi('ctrl_type_prio_chk_'+psSelId).isChecked()){
            gobi('sel_'+psSelId).hide();
            if(ssTypePrioSelId){
              gobi('sel_'+ssTypePrioSelId).hide();
              gobi('st_'+ssTypePrioSelId).hide();
            }
            ssTypePrioSelId = '';
          }else{
            gobi('sel_'+psSelId).show();
            gobi('st_'+psSelId).show();
            if(ssTypePrioSelId){
              gobi('sel_'+ssTypePrioSelId).hide();
              gobi('st_'+ssTypePrioSelId).hide();
            }
            ssTypePrioSelId = psSelId;
          }
        }
        
        function check_if_not_checked(psId){
          if(!gobi('ctrl_type_prio_chk_'+psId).isChecked()){
            gfx_check('ctrl_type_prio','chk_'+psId);
          }
        }
        
        function show_sel_type_prio(psId){
          if(ssTypePrioSelId){
            if(ssTypePrioSelId != psId){
              gobi('sel_'+ssTypePrioSelId).hide();
              gobi('st_'+ssTypePrioSelId).hide();
              gobi('sel_'+psId).show();
              gobi('st_'+psId).show();
              check_if_not_checked(psId);
            }
          }else{
            gobi('sel_'+psId).show();
            gobi('st_'+psId).show();
            check_if_not_checked(psId);
          }
          ssTypePrioSelId = psId;
        }
        
        function hide_all_filters() {
          js_hide('report_risks_by_process_filter');
          js_hide('report_risk_filter');
          js_hide('report_bia_quantitative_filter');
          js_hide('report_bia_qualitative_filter');
          js_hide('report_asa_filter');
		  js_hide('report_asa_by_processes_filter');
		  js_hide('report_asset_by_asset_filter');
          js_hide('report_asset_by_process_filter');
          js_hide('report_tests_filter');
          js_hide('report_activities_filter');
          js_hide('report_asset_filter');
          js_hide('report_scope_filter');
          //js_hide('report_process_by_process_filter');
          js_hide('report_process_by_asset_filter');
          js_hide('report_asset_matrix_filter');
          js_hide('report_places_filter');
          js_hide('report_areas_filter');
          js_hide('report_processes_filter');
          js_hide('no_filter_available');
        }
        
        function hide_all_warnings() {
          js_hide('warning');
          js_hide('report_risk_filter_warning');
          js_hide('report_parameters_filter_warning');
          js_hide('report_responsabilities_filter_warning');
        }

        function show_filter(psId) {
          gobi('st_risk_no_values').show();
          gobi('risk_filter_checkbox_no_values').show();
          gebi('var_pog_disable_risk_no_value').value=false;
          
          if (gobi('grid_reports').getValue()) js_show(psId);
          else js_show('no_filter_available');
        }
        
        function show_filter_risk_control(psId) {
          gobi('st_risk_no_values').hide();
          gobi('risk_filter_checkbox_no_values').hide();
          
          gebi('var_pog_disable_risk_no_value').value=true;
          if (gobi('grid_reports').getValue()) 
            js_show(psId);
          else 
            js_show('no_filter_available');
        }
        
        gobi('risk_filter_checkbox').onRequiredCheckNotOk = function(){
          gobi('report_risk_filter_warning').show();
        }
        
        gobi('select_parameters').onRequiredCheckNotOk = function(){
          gobi('report_parameters_filter_warning').show();
        }
        
        gobi('report_user_responsabilities_filter_controller').onRequiredCheckNotOk = function(){
          gobi('report_responsabilities_filter_warning').show();
        }
        
        gobi('grid_reports').onRequiredCheckNotOk = function(){
          gobi('warning').show();
        }
        
      </script>
		<?
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_report.xml");
?>