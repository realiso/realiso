<?php
include_once "include.php";
include_once $handlers_ref . "grid/QueryGridUserTasks.php";
include_once $handlers_ref . "grid/QueryGridUserAlerts.php";

class GridUserTasks extends FWDDrawGrid {
  public function drawItem(){
    switch($this->ciColumnIndex){
      case $this->getIndexByAlias('task_date_created'):
        $maMenuACLs = array('automatic','manual');
        $maAllowed = array();
        $moTaskManager = new ISMSTaskManager();
        if(in_array($this->getFieldValue('task_activity'),$moTaskManager->getManualExec())){
          $maAllowed[] = 'manual';
        }else{
          $maAllowed[] = 'automatic';
        }
        $moACL = FWDACLSecurity::getInstance();
        $moACL->setNotAllowed(array_diff($maMenuACLs,$maAllowed));
        $moMenu = FWDWebLib::getObject('menu_task');
        $moGrid = FWDWebLib::getObject('grid_user_tasks');
        $msLine = $moGrid->getAttrName().'_acl_'.$this->ciRowIndex;
        FWDGridMenuACLSecurity::getInstance()->installGridMenuACL($moMenu,$moACL,$moGrid,$msLine);
      
        $this->coCellBox->setValue(ISMSLib::getISMSDate($this->coCellBox->getValue()));
        return $this->coCellBox->draw();
        break;
      case $this->getIndexByAlias('task_activity'):
      // monta descri��o da tarefa
        if(trim($this->getFieldValue('context_name'))){
          $msDescription = ISMSActivity::getDescription($this->getFieldValue('task_activity')) . " - ".$this->getFieldValue('context_name');
        }else{
          $msDescription = ISMSActivity::getDescription($this->getFieldValue('task_activity'));
        }
        $miTaskId = $this->getFieldValue('task_id');
        $miActivity = $this->getFieldValue('task_activity');
        $miContextId = $this->getFieldValue('task_context_id');
        $miContextType = $this->getFieldValue('task_context_type');
        $this->coCellBox->setAttrStringNoEscape("true");
        $this->coCellBox->setValue("<a href='javascript:".htmlentities(ISMSLib::getTaskPopup($miTaskId,$miActivity,$miContextId,$miContextType,1),ENT_QUOTES).";'>$msDescription</a>");
        return $this->coCellBox->draw();
        break;
      default:
        return parent::drawItem();
    }
  }
}

class GridUserAlerts extends FWDDrawGrid {
  public function drawItem(){
    switch($this->ciColumnIndex){
      case $this->getIndexByAlias('alert_date'):
        $this->coCellBox->setValue(ISMSLib::getISMSDate($this->coCellBox->getValue()));
        return $this->coCellBox->draw();
      break;

      case $this->getIndexByAlias('alert_type'):
        $moAlert = new WKFAlert();
        $msDescription = $moAlert->getDescription($this->getFieldValue('alert_type'), $this->getFieldValue('alert_context_type'), $this->getFieldValue('alert_context_name'), $this->getFieldValue('alert_context_id'));
        $this->coCellBox->setValue($msDescription);
        return $this->coCellBox->draw();
      break;

      case $this->getIndexByAlias('alert_justification'):
        if (trim($this->coCellBox->getValue())) $this->coCellBox->setIconSrc("icon-detail.gif");
        else $this->coCellBox->setIconSrc("");
        return $this->coCellBox->draw();
      break;

      default:
        return parent::drawItem();
      break;
    }
  }
}

class RemoveAlertsEvent extends FWDRunnable {
  public function run(){
    $moGrid = FWDWebLib::getObject('grid_user_alerts');
    $maAlerts = $moGrid->getValue();
    if (count($maAlerts)) {
      $moAlert = new WKFAlert();
      foreach ($maAlerts as $miAlert) {
        $moAlert->createFilter($miAlert, 'alert_id', 'in');
      }
      $moAlert->delete();
      $moGrid->execEventPopulate();
    }
    else echo "gobi('removal_warning').show();";
  }
}

class RemoveEvent extends FWDRunnable {
  public function run(){
    if (FWDWebLib::getObject('selected_id')->getValue()) {
      $moAlert = new WKFAlert();
      $moAlert->createFilter(FWDWebLib::getObject('selected_id')->getValue(), 'alert_id', '=');
      $moAlert->delete();
      FWDWebLib::getObject('grid_user_alerts')->execEventPopulate();
    }else{
      echo "gobi('removal_warning').show();";
    }
  }
}

class ApproveEvent extends FWDRunnable {
  public function run(){
    if (FWDWebLib::getObject('selected_task_id')->getValue()) {
      $moTaskMgr = new ISMSTaskManager();
      $moTaskMgr->execTask(FWDWebLib::getObject('selected_task_id')->getValue(),WKF_ACTION_APPROVE);
    }
  }
}

class DeniedEvent extends FWDRunnable {
  public function run(){
    if (FWDWebLib::getObject('selected_task_id')->getValue()) {
      $moTaskMgr = new ISMSTaskManager();
      $moTaskMgr->execTask(FWDWebLib::getObject('selected_task_id')->getValue(),WKF_ACTION_DENY);
    }
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new RemoveAlertsEvent('remove_alerts_event'));
    $moStartEvent->addAjaxEvent(new RemoveEvent('remove_event'));
    $moStartEvent->addAjaxEvent(new ApproveEvent('approve_event'));
    $moStartEvent->addAjaxEvent(new DeniedEvent('denied_event'));
    
    $moGridTask = FWDWebLib::getObject("grid_user_tasks");
    $moHandlerTask = new QueryGridUserTasks(FWDWebLib::getConnection());
    $moHandlerTask->setUser(ISMSLib::getCurrentUserId());
    $moGridTask->setQueryHandler($moHandlerTask);
    $moGridTask->setObjFwdDrawGrid(new GridUserTasks());
    
    $moGridAlert = FWDWebLib::getObject("grid_user_alerts");
    $moHandlerAlert = new QueryGridUserAlerts(FWDWebLib::getConnection());
    $moHandlerAlert->setUser(ISMSLib::getCurrentUserId());
    $moGridAlert->setQueryHandler($moHandlerAlert);
    $moGridAlert->setObjFwdDrawGrid(new GridUserAlerts());
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){

   //instala a seguran�a de ACL na p�gina
   FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
   FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
   ?>
  <script language="javascript">
    function refresh_grids() {
      js_refresh_grid('grid_user_tasks');
      js_refresh_grid('grid_user_alerts');
    }
  </script>
  <?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_warnings.xml");
?>
