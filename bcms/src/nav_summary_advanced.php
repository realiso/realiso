<?php
include_once "include.php";
include_once $handlers_ref . "QuerySummaryResidualRisk.php";
include_once $handlers_ref . "QuerySummaryRisk.php";
include_once $handlers_ref . "QuerySummaryControl.php";
include_once $handlers_ref . "QuerySummaryBestPractice.php";
include_once $handlers_ref . "QuerySummaryAbnormalitys.php";
include_once $handlers_ref . "QuerySummaryFinancial.php";
include_once $handlers_ref . "QuerySummaryIncident.php";
include_once $handlers_ref . "QuerySummaryNC.php";
include_once $handlers_ref . "QueryTop5IncidentsPerUser.php";
include_once $handlers_ref . "QueryTop5IncidentsPerAsset.php";
include_once $handlers_ref . "QueryTop5IncidentsPerProcess.php";
include_once $handlers_ref . "QueryTop5NCPerProcess.php";

include_once $handlers_ref . "QueryCountAllDocuments.php";
include_once $handlers_ref . "QueryCountDocumentsByType.php";
include_once $handlers_ref . "QueryCountDocumentsWithFiles.php";
include_once $handlers_ref . "QueryCountDocumentsWithLinks.php";
include_once $handlers_ref . "QueryCountDocumentsWithoutReads.php";
include_once $handlers_ref . "QueryTop10DocumentsRead.php";
include_once $handlers_ref . "QueryCountDocumentsByState.php";
include_once $handlers_ref . "QueryCountDocsRevisionPeriod.php";
include_once $handlers_ref . "QueryCountDocsTimeToApprove.php";
include_once $handlers_ref . "QuerySummaryAbnormalityPM.php";
include_once $handlers_ref . "QuerySummaryAbnormalityCI.php";

include_once $handlers_ref . "QueryResume.php";
include_once $handlers_ref . "QueryTop10Qualitative.php";
include_once $handlers_ref . "QueryTop10Asa.php";
include_once $handlers_ref . "QueryDashboardScope.php";
include_once $handlers_ref . "QueryDashboardCurrentTests.php";
include_once $handlers_ref . "QueryTop10QualitativeFinancial.php";

class GridTop10Qualitative extends FWDDrawGrid {
	public function drawItem(){
		if ($this->ciRowIndex==1) {
			$this->csDivUnitClass = 'GridLineGray';
		} else {
			$this->coCellBox->setIconSrc("");
		}
		$this->coCellBox->setAttrStringNoEscape("true");
		return parent::drawItem();
	}
}

class GridTop10Asa extends FWDDrawGrid {
	public function drawItem(){
		if ($this->ciRowIndex==1) {
			$this->csDivUnitClass = 'GridLineGray';
		} else {
			$this->coCellBox->setIconSrc("");
		}
		$this->coCellBox->setAttrStringNoEscape("true");
		return parent::drawItem();
	}
}

class GridDashboardScope extends FWDDrawGrid {
	public function drawItem(){
		if ($this->ciRowIndex==1) {
			$this->csDivUnitClass = 'GridLineGray';
		} else {
			$this->coCellBox->setIconSrc("");
		}
		$this->coCellBox->setAttrStringNoEscape("true");
		return parent::drawItem();
	}
}

class GridDashboardCurrentTests extends FWDDrawGrid {
	public function drawItem(){
		if ($this->ciRowIndex==1) {
			$this->csDivUnitClass = 'GridLineGray';
		} else {
			$this->coCellBox->setIconSrc("");
		}

		switch($this->ciColumnIndex){
			//			case 4: if ($this->ciRowIndex==1) $this->coCellBox->setIconSrc("icon-risk_red.gif"); break;
			//			case 3: if ($this->ciRowIndex==1) $this->coCellBox->setIconSrc("icon_correct.gif"); break;
			case 1:
				if ($this->ciRowIndex != 1){
					$status = $this->getFieldValue('status');

					if($status == 1){
						$this->coCellBox->setIconSrc("icon-risk_blue.gif");
					} else if($status == 2){
						$this->coCellBox->setIconSrc("icon-risk_green.gif");
					} else if($status == 3){
						$this->coCellBox->setIconSrc("icon-risk_red.gif");
					} else if($status == 4){
						$this->coCellBox->setIconSrc("icon-risk_yellow.gif");
					}
				}
				break;

			default: break;
		}

		$this->coCellBox->setAttrStringNoEscape("true");
		return parent::drawItem();
	}
}

class GridResume extends FWDDrawGrid {
	public function drawItem(){
		if ($this->ciRowIndex==1) {
			$this->csDivUnitClass = 'GridLineGray';
		} else {
			$this->coCellBox->setIconSrc("");
		}

		switch($this->ciColumnIndex){
			//			case 4: if ($this->ciRowIndex==1) $this->coCellBox->setIconSrc("icon-risk_red.gif"); break;
			//			case 3: if ($this->ciRowIndex==1) $this->coCellBox->setIconSrc("icon_correct.gif"); break;
			case 1:
				switch($this->ciRowIndex) {
					case 4  : $this->coCellBox->setIconSrc("icon-area_gray.gif"); break;
					case 5  : $this->coCellBox->setIconSrc("icon-area_gray.gif"); break;
					case 2  : $this->coCellBox->setIconSrc("icon-home.gif"); break;
					case 3  : $this->coCellBox->setIconSrc("icon-home.gif"); break;
					case 6  : $this->coCellBox->setIconSrc("icon-process_gray.gif"); break;
					case 7  : $this->coCellBox->setIconSrc("icon-asset_gray.gif"); break;
					case 8  : $this->coCellBox->setIconSrc("icon-asset_gray.gif"); break;
					case 9  : $this->coCellBox->setIconSrc("icon-asset_gray.gif"); break;
					default : $this->coCellBox->setIconSrc(""); break;
				}
			default: break;
		}
		$this->coCellBox->setAttrStringNoEscape("true");
		return parent::drawItem();
	}
}



function populate_grid_resume_summary(){
	$moQuery = new QueryResume(FWDWebLib::getConnection());
	$moQuery->makeQuery();
	$moQuery->executeQuery();
	$result = $moQuery->getRiskSummary();

	$grid = FWDWebLib::getObject("grid_resume_summary");

	$grid->setObjFWDDrawGrid(new GridResume());

	$grid->setItem(2,1,FWDLanguage::getPHPStringValue('descriptionDash','<b>Descri��o</b>'));
	$grid->setItem(3,1,FWDLanguage::getPHPStringValue('itemsDash','<b>�tens</b>'));
	$grid->setItem(4,1,FWDLanguage::getPHPStringValue('withPlansDash','<b>Planos</b>'));
	//	$grid->setItem(3,1,FWDLanguage::getPHPStringValue('withPlan','<b>C/ Plano</b>'));
	//	$grid->setItem(4,1,FWDLanguage::getPHPStringValue('withoutPlan','<b>S/ Plano</b>'));
	$grid->setItem(5,1,FWDLanguage::getPHPStringValue('%','<b>%</b>'));

	$index = 2;
	$grid->setItem(2,$index++,FWDLanguage::getPHPStringValue('placesDash','<b>Locais</b>'));
	$grid->setItem(2,$index++,FWDLanguage::getPHPStringValue('subplacesDash','<b>Sub-Locais</b>'));
	$grid->setItem(2,$index++,FWDLanguage::getPHPStringValue('areasDash','<b>Unidades de Neg�cio</b>'));
	$grid->setItem(2,$index++,FWDLanguage::getPHPStringValue('subareasDash','<b>Sub-Unidades de Neg�cio</b>'));
	$grid->setItem(2,$index++,FWDLanguage::getPHPStringValue('processesDash','<b>Processos</b>'));
	$grid->setItem(2,$index++,FWDLanguage::getPHPStringValue('scenesDash','<b>Cen�rios</b>'));
	$grid->setItem(2,$index++,FWDLanguage::getPHPStringValue('activitiesDash','<b>Atividades</b>'));
	$grid->setItem(2,$index++,FWDLanguage::getPHPStringValue('assetsDash','<b>Ativos</b>'));

	$index = 2;
	$grid->setItem(3,$index,$result["places"][0]);
	$grid->setItem(4,$index,$result["places"][1]);
	$grid->setItem(5,$index,"<b>".$result["places"][2]."</b>");
	$index++;
	$grid->setItem(3,$index,$result["subplaces"][0]);
	$grid->setItem(4,$index,$result["subplaces"][1]);
	$grid->setItem(5,$index,"<b>".$result["subplaces"][2]."</b>");
	$index++;
	$grid->setItem(3,$index,$result["areas"][0]);
	$grid->setItem(4,$index,$result["areas"][1]);
	$grid->setItem(5,$index,"<b>".$result["areas"][2]."</b>");
	$index++;
	$grid->setItem(3,$index,$result["subareas"][0]);
	$grid->setItem(4,$index,$result["subareas"][1]);
	$grid->setItem(5,$index,"<b>".$result["subareas"][2]."</b>");
	$index++;
	$grid->setItem(3,$index,$result["processes"][0]);
	$grid->setItem(4,$index,$result["processes"][1]);
	$grid->setItem(5,$index,"<b>".$result["processes"][2]."</b>");
	$index++;
	$grid->setItem(3,$index,$result["scenes"][0]);
	$grid->setItem(4,$index,$result["scenes"][1]);
	$grid->setItem(5,$index,"<b>".$result["scenes"][2]."</b>");
	$index++;
	$grid->setItem(3,$index,$result["activities"][0]);
	$grid->setItem(4,$index,$result["activities"][1]);
	$grid->setItem(5,$index,"<b>".$result["activities"][2]."</b>");
	$index++;
	$grid->setItem(3,$index,$result["assets"][0]);
	$grid->setItem(4,$index,$result["assets"][1]);
	$grid->setItem(5,$index,"<b>".$result["assets"][2]."</b>");
}

function populate_grid_top10qualitative(){
	$moQuery = new QueryTop10Qualitative(FWDWebLib::getConnection());
	$moQuery->makeQuery();
	$moQuery->executeQuery();
	$grid = FWDWebLib::getObject("grid_top10qualitative");
	$grid->setObjFWDDrawGrid(new GridTop10Qualitative());

	$grid->setItem(1,1,FWDLanguage::getPHPStringValue('processDash','<b>Processo</b>'));
	$grid->setItem(2,1,FWDLanguage::getPHPStringValue('ranking','<b>Ranking</b>'));
	$grid->setItem(3,1,FWDLanguage::getPHPStringValue('plansDash','<b>Planos</b>'));

	$line = 2;
	while($moQuery->fetch()){
		$grid->setItem(1,$line, "<b>{$moQuery->getFieldValue('name')}</b>");
		$grid->setItem(2,$line, $moQuery->getFieldValue('ranking'));
		$grid->setItem(3,$line, $moQuery->getFieldValue('plans'));
		$line = $line + 1;
	}
}

function populate_grid_top10qualitativeFinancial(){
	$moQuery = new QueryTop10QualitativeFinancial(FWDWebLib::getConnection());
	$moQuery->makeQuery();
	$moQuery->executeQuery();
	$grid = FWDWebLib::getObject("grid_top10qualitativeFinancial");
	$grid->setObjFWDDrawGrid(new GridTop10Qualitative());

	$grid->setItem(1,1,FWDLanguage::getPHPStringValue('processDash','<b>Processo</b>'));
	$grid->setItem(2,1,FWDLanguage::getPHPStringValue('financialLoss','<b>Perda Financeira</b>'));
	$grid->setItem(3,1,FWDLanguage::getPHPStringValue('plansDash','<b>Planos</b>'));

	$line = 2;
	while($moQuery->fetch()){
		$grid->setItem(1,$line, "<b>{$moQuery->getFieldValue('name')}</b>");
		$grid->setItem(2,$line, $moQuery->getFieldValue('financialLoss'));
		$grid->setItem(3,$line, $moQuery->getFieldValue('plans'));
		$line = $line + 1;
	}
}

function populate_grid_top10asa(){
	$moQuery = new QueryTop10Asa(FWDWebLib::getConnection());
	$moQuery->makeQuery();
	$moQuery->executeQuery();
	$grid = FWDWebLib::getObject("grid_top10asa");
	$grid->setObjFWDDrawGrid(new GridTop10Asa());

	$grid->setItem(1,1,FWDLanguage::getPHPStringValue('processDash','<b>Processo</b>'));
	$grid->setItem(2,1,FWDLanguage::getPHPStringValue('assetDash','<b>Ativo</b>'));
	$grid->setItem(3,1,FWDLanguage::getPHPStringValue('asa','<b>ASA</b>'));
	$grid->setItem(4,1,FWDLanguage::getPHPStringValue('plansDash','<b>Planos</b>'));

	$line = 2;
	while($moQuery->fetch()){
		$grid->setItem(1,$line, "<b>{$moQuery->getFieldValue('process')}</b>");
		$grid->setItem(2,$line, $moQuery->getFieldValue('asset'));
		$grid->setItem(3,$line, $moQuery->getFieldValue('asa'));
		$grid->setItem(4,$line, $moQuery->getFieldValue('plans'));
		$line = $line + 1;
	}
}

function populate_grid_dashboardScope(){
	$moQuery = new QueryDashboardScope(FWDWebLib::getConnection());
	$moQuery->makeQuery();
	$moQuery->executeQuery();
	$grid = FWDWebLib::getObject("grid_dashboardScope");
	$grid->setObjFWDDrawGrid(new GridDashboardScope());

	$grid->setItem(1,1,FWDLanguage::getPHPStringValue('dashboard_scopeDash','<b>Escopo</b>'));
	$grid->setItem(2,1,FWDLanguage::getPHPStringValue('dashboard_processesDash','<b>Processos</b>'));
	$grid->setItem(3,1,FWDLanguage::getPHPStringValue('dashboard_placesDash','<b>Locais</b>'));
	$grid->setItem(4,1,FWDLanguage::getPHPStringValue('dashboard_assetsDash','<b>Ativos</b>'));
	$grid->setItem(5,1,FWDLanguage::getPHPStringValue('dashboard_areasDash','<b>UNs</b>'));

	$line = 2;
	while($moQuery->fetch()){
		$grid->setItem(1,$line, "<b>{$moQuery->getFieldValue('name')}</b>");
		$grid->setItem(2,$line, $moQuery->getFieldValue('processes'));
		$grid->setItem(3,$line, $moQuery->getFieldValue('places'));
		$grid->setItem(4,$line, $moQuery->getFieldValue('assets'));
		$grid->setItem(5,$line, $moQuery->getFieldValue('areas'));
		$line = $line + 1;
	}
}

function populate_grid_dashboardCurrentTests(){
	$schedule = new CMPlanSchedule();
	$schedule->updateStatus();
	$moQuery = new QueryDashboardCurrentTests(FWDWebLib::getConnection());
	$moQuery->makeQuery();
	$moQuery->executeQuery();
	$grid = FWDWebLib::getObject("grid_dashboardCurrentTests");
	$grid->setObjFWDDrawGrid(new GridDashboardCurrentTests());

	$grid->setItem(2,1,FWDLanguage::getPHPStringValue('dashboard_currentTestsPlanNameDash','<b>Plano</b>'));
	$grid->setItem(3,1,FWDLanguage::getPHPStringValue('dashboard_currentTestsDateDash','<b>Data</b>'));
	$grid->setItem(4,1,FWDLanguage::getPHPStringValue('dashboard_currentTestsResponsibleDash','<b>Respons�vel</b>'));

	$line = 2;
	while($moQuery->fetch()){
		$date = $moQuery->getFieldValue('date');
		if($date){
			$date = ISMSLib::getISMSDate($date);
		}
		$grid->setItem(1, $line, $moQuery->getFieldValue('status'));
		$grid->setItem(2, $line, "<b>{$moQuery->getFieldValue('planName')}</b>");
		$grid->setItem(3, $line, $date);
		$grid->setItem(4, $line, $moQuery->getFieldValue('responsible'));

		$line = $line + 1;
	}
}

function getReportLink($psValue,$psReportName,$psModule,$pmFilter='',$psPrefix='',$psSufix=''){
	if($psValue){
		if(is_array($pmFilter)){
			if(count($pmFilter)){
				$msFilter = str_replace("\"","\\'",serialize($pmFilter));
			}else{
				$msFilter = '';
			}
		}else{
			$msFilter = $pmFilter;
		}
		return "<a href=\"javascript:generateReport('{$psReportName}','{$psModule}','{$msFilter}')\">{$psPrefix}{$psValue}{$psSufix}</a>";
	}else{
		return $psPrefix.$psValue.$psSufix;
	}
}

class ISMSReportEvent extends FWDRunnable {
	public function run(){
		set_time_limit(3000);

		$msReport = FWDWebLib::getObject('report_name')->getValue();
		$msFormat = FWDWebLib::getObject('report_format')->getValue();

		$mbDontForceDownload = ($msFormat==REPORT_FILETYPE_HTML);

		$moWindowReport = new ISMSReportWindow(RISK_MANAGEMENT_MODE);
		if($mbDontForceDownload) $moWindowReport->open();

		$moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
		$moSession->deleteAttribute("filter");
		$moSession->addAttribute("filter");
		switch($msReport) {
			case "report_risk_values":
				$moFilter = new ISMSReportRiskValuesFilter();
				$moFilter->setRiskValueType(RISK_VALUE_TYPE_RESIDUAL_VALUE);
				$moFilter->showRiskWithNoValues(true);
				break;
			case 'report_statement_of_applicability':
				$moFilter  = new ISMSReportStatementOfApplicabilityFilter();
				FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->deleteAttribute("isPreReport");
				$moFilter->setStandardFilterName(FWDLanguage::getPHPStringValue('st_all_standards', 'Todas'));
				break;
			case 'report_risk_treatment_plan':
			case 'report_areas_without_processes':
			case 'report_processes_without_assets':
			case 'report_controls_without_risks':
				$moFilter = new ISMSReportGenericClassifTypePrioFilter();
				break;
			case 'report_areas':{
				$moFilter = new ISMSReportAreasFilter();
				$moFilter->setValue(FWDWebLib::getObject('report_filter')->getValue());
				break;
			}
			case 'report_processes':{
				$moFilter = new ISMSReportProcessesFilter();
				$moFilter->setValue(FWDWebLib::getObject('report_filter')->getValue());
				break;
			}
			case 'report_assets':{
				$moFilter = new ISMSReportAssetsFilter();
				$moFilter->setValue(FWDWebLib::getObject('report_filter')->getValue());
				break;
			}
			case 'report_risks':{
				$moFilter = new ISMSReportRisksFilter();
				$msFilter = FWDWebLib::getObject('report_filter')->getValue();
				if($msFilter){
					$maFilter = FWDWebLib::unserializeString(str_replace("'",'"',$msFilter));
					if(isset($maFilter['rv'])){
						$moFilter->setResidualValue($maFilter['rv']);
					}
					if(isset($maFilter['v'])){
						$moFilter->setValue($maFilter['v']);
					}
					if(isset($maFilter['t'])){
						$moFilter->setShowTreated($maFilter['t']);
					}
					if(isset($maFilter['e'])){
						$moFilter->setShowEstimated($maFilter['e']);
					}
				}
				break;
			}
			case 'report_controls':{
				$moFilter = new ISMSReportControlsFilter();
				$moFilter->setImplementationStatus(FWDWebLib::getObject('report_filter')->getValue());
				break;
			}
			case 'report_control_cost':{
				$moFilter = new ISMSReportGenericClassifTypePrioFilter();
				break;
			}
			case 'report_control_cost_by_cost_category':{
				$moFilter = new ISMSReportControlCostByCostCategoryFilter();
				$msFilter = FWDWebLib::getObject('report_filter')->getValue();
				if($msFilter){
					$maFilter = unserialize(str_replace("'",'"',$msFilter));
					if(isset($maFilter['cc'])){
						$moFilter->setCostCategory($maFilter['cc']);
					}
				}
				break;
			}
			case 'report_risk_financial_impact':{
				$moFilter = new ISMSReportRiskFinancialImpactFilter();
				$msFilter = FWDWebLib::getObject('report_filter')->getValue();
				if($msFilter){
					$maFilter = unserialize(str_replace("'",'"',$msFilter));
					if(isset($maFilter['t'])){
						$moFilter->setTreatment($maFilter['t']);
					}
					if(isset($maFilter['v'])){
						$moFilter->setValues($maFilter['v']);
					}
					if(isset($maFilter['nta'])){
						$moFilter->setNotTreatedOrAccepted(true);
					}
				}
				break;
			}
			case 'report_best_practices':{
				$moFilter = new ISMSReportBestPracticesFilter();
				$msFilter = FWDWebLib::getObject('report_filter')->getValue();
				if($msFilter){
					$maFilter = unserialize(str_replace("'",'"',$msFilter));
					if(isset($maFilter['s'])){
						$moFilter->setStandard($maFilter['s']);
					}
					if(isset($maFilter['a'])){
						$moFilter->setOnlyApplied(true);
					}
				}
				break;
			}
			default:
				$moFilter = new FWDReportFilter();
				break;
		}
		$moFilter->setFileType($msFormat);
		$moSession->setAttrFilter($moFilter);

		if($mbDontForceDownload){
			$moWindowReport->setWaitLoadingComplete(true);
			$moWindowReport->loadReport($msReport);
		}
		else $moWindowReport->forceReportDownload($msReport);
	}
}

class ISMSReportEventCI extends FWDRunnable {
	public function run(){
		set_time_limit(3000);

		$msReport = FWDWebLib::getObject('report_name')->getValue();
		$msFormat = FWDWebLib::getObject('report_format')->getValue();

		$mbDontForceDownload = ($msFormat==REPORT_FILETYPE_HTML);

		$moWindowReport = new ISMSReportWindow(INCIDENT_MODE);
		if($mbDontForceDownload) $moWindowReport->open();

		$moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
		$moSession->deleteAttribute("filter");
		$moSession->addAttribute("filter");

		switch($msReport){
			case 'report_incidents':{
				$moFilter = new ISMSReportIncidentsFilter();
				$msFilter = FWDWebLib::getObject('report_filter')->getValue();
				if($msFilter){
					$maFilter = unserialize(str_replace("'",'"',$msFilter));
					if(isset($maFilter['s'])){
						$moFilter->setStatus($maFilter['s']);
					}
					if(isset($maFilter['a'])){
						$moFilter->setAsset($maFilter['a']);
					}
					if(isset($maFilter['p'])){
						$moFilter->setProcess($maFilter['p']);
					}
				}
				break;
			}
			case 'report_non_conformities':{
				$moFilter = new ISMSReportNonConformitiesFilter();
				$msFilter = FWDWebLib::getObject('report_filter')->getValue();
				if($msFilter){
					$maFilter = unserialize(str_replace("'",'"',$msFilter));
					if(isset($maFilter['s'])){
						$moFilter->setStatus($maFilter['s']);
					}
					if(isset($maFilter['p'])){
						$moFilter->setProcess($maFilter['p']);
					}
				}
				break;
			}
			case 'report_disciplinary_process':{
				$moFilter = new ISMSReportDisciplinaryProcessFilter();
				$msFilter = FWDWebLib::getObject('report_filter')->getValue();
				if($msFilter){
					$maFilter = unserialize(str_replace("'",'"',$msFilter));
					if(isset($maFilter['u'])){
						$moFilter->setUser($maFilter['u']);
					}
				}
				break;
			}
			default:{
				$moFilter = new FWDReportFilter();
				break;
			}
		}

		$moFilter->setFileType($msFormat);
		$moSession->setAttrFilter($moFilter);

		if($mbDontForceDownload){
			$moWindowReport->setWaitLoadingComplete(true);
			$moWindowReport->loadReport($msReport);
		}
		else $moWindowReport->forceReportDownload($msReport);
	}
}

class ISMSReportEventPM extends FWDRunnable {
	public function run(){
		set_time_limit(3000);

		$msReport = FWDWebLib::getObject('report_name')->getValue();
		$msFormat = FWDWebLib::getObject('report_format')->getValue();

		$mbDontForceDownload = ($msFormat==REPORT_FILETYPE_HTML);

		$moWindowReport = new ISMSReportWindow(POLICY_MODE);
		if($mbDontForceDownload) $moWindowReport->open();

		$moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
		$moSession->deleteAttribute("filter");
		$moSession->addAttribute("filter");
		switch($msReport){
			case 'report_documents_without_register':{
				$moFilter = new ISMSReportDocumentsWithoutRegisterFilter();
				$moFilter->setFileType($msFormat);
				$moSession->setAttrFilter($moFilter);
				break;
			}
			case 'report_documents_pendent_reads':{
				$moFilter = new ISMSReportDocumentsPendentReadsFilter();
				$moFilter->setExpand(true);
				break;
			}
			case 'report_not_accessed_documents':{
				$moFilter = new ISMSReportNotAccessedDocumentsFilter();
				$moFilter->setExpand(true);
				break;
			}
			case 'report_users_with_pendant_read':{
				$moFilter = new ISMSReportUsersWithPendantReadFilter();
				break;
			}
			case 'report_docs_with_high_frequency_revision':{
				$moFilter = new ISMSReportDocsWithHighFrequencyRevisionFilter();
				break;
			}
			case 'report_documents_by_state':{
				$moFilter = new ISMSReportDocumentsByStateFilter();
				$msFilter = FWDWebLib::getObject('report_filter')->getValue();
				$moFilter->setStatus($msFilter);
				/*
				 if($msFilter){
				 $maFilter = unserialize(str_replace("'",'"',$msFilter));
				 if(isset($maFilter['e'])){
				 $moFilter->setShowEstimated($maFilter['e']);
				 }
				 }
				 */
				break;
			}
			default:{
				$moFilter = new FWDReportFilter();
				break;
			}
		}
		$moFilter->setFileType($msFormat);
		$moSession->setAttrFilter($moFilter);

		if($mbDontForceDownload){
			$moWindowReport->setWaitLoadingComplete(true);
			$moWindowReport->loadReport($msReport);
		}
		else $moWindowReport->forceReportDownload($msReport);
	}
}

class GridResidualRiskSummary extends FWDDrawGrid {
	public function drawItem(){
		if ($this->ciRowIndex==1) {
			$this->csDivUnitClass = 'GridLineGray';
		} else {
			$this->coCellBox->setIconSrc("");
		}
		switch($this->ciColumnIndex){
			case 1:
				switch($this->ciRowIndex) {
					case 2  : $this->coCellBox->setIconSrc("icon-area_gray.gif"); break;
					case 3  : $this->coCellBox->setIconSrc("icon-process_gray.gif"); break;
					case 4  : $this->coCellBox->setIconSrc("icon-asset_gray.gif"); break;
					case 5  : $this->coCellBox->setIconSrc("icon-risk_gray.gif"); break;
					default : $this->coCellBox->setIconSrc(""); break;
				}
				break;
			case 3: if ($this->ciRowIndex==1) $this->coCellBox->setIconSrc("icon-risk_red.gif"); break;
			case 4: if ($this->ciRowIndex==1) $this->coCellBox->setIconSrc("icon-risk_yellow.gif"); break;
			case 5: if ($this->ciRowIndex==1) $this->coCellBox->setIconSrc("icon-risk_green.gif"); break;
			case 6: if ($this->ciRowIndex==1) $this->coCellBox->setIconSrc("icon-risk_blue.gif"); break;
			default: break;
		}
		$this->coCellBox->setAttrStringNoEscape("true");
		return parent::drawItem();
	}
}

class GridGeneralRiskSummary extends FWDDrawGrid {
	public function drawItem(){
		switch($this->ciRowIndex) {
			case 1 : case 2: $this->csDivUnitClass = 'GridLineLightYellow'; break;
			case 5 : $this->csDivUnitClass = 'GridLineGray'; break;
		}
		$this->coCellBox->setAttrStringNoEscape("true");
		return parent::drawItem();
		break;
	}
}

class GridParameterizedRiskSummary extends FWDDrawGrid {
	public function drawItem(){
		switch($this->ciColumnIndex){
			case 1:
				switch($this->ciRowIndex) {
					case 2: $this->coCellBox->setIconSrc("icon-risk_red.gif"); break;
					case 3: $this->coCellBox->setIconSrc("icon-risk_yellow.gif"); break;
					case 4: $this->coCellBox->setIconSrc("icon-risk_green.gif"); break;
					default: $this->coCellBox->setIconSrc(""); break;
				}
				break;
			default:
				break;
		}
		switch($this->ciRowIndex) {
			case 1 : case 5: $this->csDivUnitClass = 'GridLineGray'; break;
		}
		$this->coCellBox->setAttrStringNoEscape("true");
		return parent::drawItem();
	}
}

class GridBestPracticeSummary extends FWDDrawGrid {
	public function drawItem(){
		if($this->ciRowIndex==1){
			$this->csDivUnitClass = 'GridLineGray';
		}else{
			$this->csDivUnitClass = 'GridLineYellow';
			if($this->ciColumnIndex==1){
				$this->coCellBox->setValue('<b>'.$this->coCellBox->getValue().'</b>');
			}
		}
		$this->coCellBox->setAttrStringNoEscape('true');
		return parent::drawItem();
	}
}

class GridAbnormalitysSummary extends FWDDrawGrid {
	public function drawItem(){
		switch($this->ciRowIndex) {
			case 1: $this->csDivUnitClass = 'GridLineGray'; break;
			default:
				switch($this->ciColumnIndex){
					case 1:
						$this->coCellBox->setIconSrc("icon-wrong.gif");
						return parent::drawItem();
						break;
					default: break;
				}
				break;
		}
		return parent::drawItem();
	}
}

class GridFinancialSummary extends FWDDrawGrid {
	public function drawItem(){
		switch($this->ciRowIndex) {
			case 1:
			case 8:
			case 14:
			case 18:
				$this->csDivUnitClass = 'GridLinePink'; break;
			default:break;
		}
		$this->coCellBox->setAttrStringNoEscape("true");
		return parent::drawItem();
	}
}

class GridControlSummary extends FWDDrawGrid {
	public function drawItem(){
		switch($this->ciRowIndex){
			case 1:
			case 5:
				$this->csDivUnitClass = 'GridLineGray'; break;
		}
		$this->coCellBox->setAttrStringNoEscape("true");
		return parent::drawItem();
	}
}

class GridDocumentSumary extends FWDDrawGrid {
	public function drawItem(){
		switch($this->ciRowIndex) {
			case 1:
			case 8:
				$this->csDivUnitClass = 'GridLineGray';
				break;
		}
		$this->coCellBox->setAttrStringNoEscape("true");
		return parent::drawItem();
	}
}

class GridDiferentClassRowOne extends FWDDrawGrid {
	public function drawItem() {
		if($this->ciRowIndex==1){
			$this->csDivUnitClass = 'GridLineGray';
		}
		$this->coCellBox->setAttrStringNoEscape('true');
		return parent::drawItem();
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent("event_screen"));
		$moStartEvent->addAjaxEvent(new ISMSReportEvent("function_report"));
		$moStartEvent->addAjaxEvent(new ISMSReportEventCI("function_report_ci"));
		$moStartEvent->addAjaxEvent(new ISMSReportEventPM("function_report_pm"));
		FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
//		FWDMenuACLSecurity::getInstance()->installMenuACL(FWDWebLib::getObject('menu_abnormalitys_summary'),FWDACLSecurity::getInstance());
	}
}

function getArrayGrids(){
	$moConfig = new ISMSConfig();
	$mbHasRMModule = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(RISK_MANAGEMENT_MODE);
	$mbHasPMModule = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(POLICY_MODE);
	$mbHasCIModule = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(INCIDENT_MODE);
	$mbHasRevision = $moConfig->getConfig(GENERAL_REVISION_ENABLED);
	$mbHasTests = $moConfig->getConfig(GENERAL_TEST_ENABLED);
	$maGrids = array();
	$maGrids['grid_top10qualitative'           	] = array('module'=>'rm', 'height'=>FWDWebLib::getObject('grid_top10qualitative')->getObjFWDBox()->getAttrHeight());
	$maGrids['grid_top10qualitativeFinancial'  	] = array('module'=>'rm', 'height'=>FWDWebLib::getObject('grid_top10qualitativeFinancial')->getObjFWDBox()->getAttrHeight());
	$maGrids['grid_top10asa'  					] = array('module'=>'rm', 'height'=>FWDWebLib::getObject('grid_top10asa')->getObjFWDBox()->getAttrHeight());
	$maGrids['grid_dashboardScope'				] = array('module'=>'rm', 'height'=>FWDWebLib::getObject('grid_dashboardScope')->getObjFWDBox()->getAttrHeight());
	$maGrids['grid_dashboardCurrentTests'		] = array('module'=>'rm', 'height'=>FWDWebLib::getObject('grid_dashboardCurrentTests')->getObjFWDBox()->getAttrHeight());
	$maGrids['grid_resume_summary'             	] = array('module'=>'rm', 'height'=>FWDWebLib::getObject('grid_resume_summary')->getObjFWDBox()->getAttrHeight());
	return $maGrids;
}
function populate_grid_residual_risk_summary(){
	/*** POPULA GRID 'SUM�RIO DE RISCO RESIDUAL' ***/
	$moGridMyResidualRisk = FWDWebLib::getObject("grid_residual_risk_summary");
	$moGridMyResidualRisk->setObjFWDDrawGrid(new GridResidualRiskSummary());

	$moGridMyResidualRisk->setItem(7,1,FWDLanguage::getPHPStringValue('gc_total_bl','<b>Total</b>'));

	$moGridMyResidualRisk->setItem(2,2,FWDLanguage::getPHPStringValue('gs_area','�rea'));
	$moGridMyResidualRisk->setItem(2,3,FWDLanguage::getPHPStringValue('processDash','Processo'));
	$moGridMyResidualRisk->setItem(2,4,FWDLanguage::getPHPStringValue('gs_asset','Ativo'));
	$moGridMyResidualRisk->setItem(2,5,FWDLanguage::getPHPStringValue('gs_risk','Risco'));

	$moQuery = new QuerySummaryResidualRisk(FWDWebLib::getConnection());
	$moQuery->makeQuery();
	$moQuery->executeQuery();
	$maResidualRiskSummary = $moQuery->getResidualRiskSummary();
	foreach ($maResidualRiskSummary as $msName => $miValue) {
		$miColumn =
		strstr($msName,"high") ? 3 :
		(strstr($msName,"mid") ? 4 :
		(strstr($msName,"low") ? 5 :
		(strstr($msName,"np") ? 6 :
		(strstr($msName,"total") ? 7 : -1))));
		$miLine =
		strstr($msName,"area") ? 2 :
		(strstr($msName,"process") ? 3 :
		(strstr($msName,"asset") ? 4 :
		(strstr($msName,"risk") ? 5 : -1)));
		if ($miColumn<0 || $miLine<0)
		trigger_error("Invalid Residual Risk Summary Query Name: $msName",E_USER_ERROR);

		if($miColumn==7){
			$msPrefix = "<b>";
			$msSufix = "</b>";
		}else{
			$msPrefix = $msSufix = "";
		}

		if($miLine>1 && $miColumn>2){
			if($miLine==5){
				$maFilter = array();
				switch($miColumn){
					case 3: $maFilter['rv'] = 'high'; break;
					case 4: $maFilter['rv'] = 'mid';  break;
					case 5: $maFilter['rv'] = 'low';  break;
					case 6: $maFilter['rv'] = 'np';   break;
				}
				$msReportName = 'report_risks';
				$msValue = getReportLink($miValue,$msReportName,'',$maFilter,$msPrefix,$msSufix);
			}else{
				switch($miColumn){
					case 3 : $msFilter = 'high'; break;
					case 4 : $msFilter = 'mid';  break;
					case 5 : $msFilter = 'low';  break;
					case 6 : $msFilter = 'np';   break;
					default: $msFilter = '';     break;
				}
				switch($miLine){
					case 2: $msReportName = 'report_areas';     break;
					case 3: $msReportName = 'report_processes'; break;
					case 4: $msReportName = 'report_assets';    break;
				}
				$msValue = getReportLink($miValue,$msReportName,'',$msFilter,$msPrefix,$msSufix);
			}
		}else{
			$msValue = $miValue;
		}

		$moGridMyResidualRisk->setItem($miColumn,$miLine,$msValue);
	}
}
function populate_grid_best_practice_summary(){
	/*** POPULA GRID 'SUM�RIO DE MELHORES PR�TICAS' ***/
	$moGridBestPractice = FWDWebLib::getObject("grid_best_practice_summary");
	$moGridBestPractice->setObjFWDDrawGrid(new GridBestPracticeSummary());

	$moGridBestPractice->setItem(1,1,FWDLanguage::getPHPStringValue('gc_standards_bl','<b>Normas</b>'));
	$moGridBestPractice->setItem(2,1,FWDLanguage::getPHPStringValue('gc_best_practices_bl','<b># de Melhores Pr�ticas</b>'));
	$moGridBestPractice->setItem(3,1,FWDLanguage::getPHPStringValue('gc_applied_bl','<b>Aplicadas</b>'));
	$moGridBestPractice->setItem(4,1,FWDLanguage::getPHPStringValue('gc_percentage_bl','<b>%</b>'));

	$moQuery = new QuerySummaryBestPractice();
	$moQuery->makeQuery();
	$moQuery->executeQuery();
	$miLine = 2;
	foreach($moQuery->getBestPracticeSummary() as $miId => $maData){
		if(!isset($maData['applied'])) $maData['applied'] = 0;
		if(!isset($maData['total'])) $maData['total'] = 0;

		$moGridBestPractice->setItem(1,$miLine,$maData['name']);

		$maFilter = array('s'=>$miId);
		$msLink = getReportLink($maData['total'],'report_best_practices','',$maFilter);
		$moGridBestPractice->setItem(2,$miLine,$msLink);

		$maFilter = array('s'=>$miId,'a'=>true);
		$msLink = getReportLink($maData['applied'],'report_best_practices','',$maFilter);
		$moGridBestPractice->setItem(3,$miLine,$msLink);

		if($maData['total'] && $maData['applied']){
			$miPercent = round(($maData['applied']/$maData['total'])*100);
			$moGridBestPractice->setItem(4,$miLine,$miPercent.'%');
		}else{
			$moGridBestPractice->setItem(4,$miLine,'0%');
		}

		$miLine++;
	}
}
function populate_grid_abnormalitys_summary(){
	/*** POPULA GRID 'SUM�RIO DE ANORMALIDADES' ***/
	$moGridAbnormalitysSummary = FWDWebLib::getObject("grid_abnormalitys_summary");
	$moGridAbnormalitysSummary->setObjFWDDrawGrid(new GridAbnormalitysSummary());

	$moGridAbnormalitysSummary->setItem(2,1,FWDLanguage::getPHPStringValue('gc_description_bl','<b>Descri��o</b>'));
	$moGridAbnormalitysSummary->setItem(3,1,FWDLanguage::getPHPStringValue('gc_amount_bl','<b>Qtde.</b>'));

	$moGridAbnormalitysSummary->setItem(2,2,FWDLanguage::getPHPStringValue('gs_risk_management_pending_tasks','Tarefas pendentes da Gest�o de Riscos'));
	$moGridAbnormalitysSummary->setItem(4,2,'report_summarized_pendant_tasks');
	$moGridAbnormalitysSummary->setItem(2,3,FWDLanguage::getPHPStringValue('gs_areas_without_processes','�reas sem processos'));
	$moGridAbnormalitysSummary->setItem(4,3,'report_areas_without_processes');
	$moGridAbnormalitysSummary->setItem(2,4,FWDLanguage::getPHPStringValue('gs_processes_without_assets','Processos sem ativos'));
	$moGridAbnormalitysSummary->setItem(4,4,'report_processes_without_assets');
	$moGridAbnormalitysSummary->setItem(2,5,FWDLanguage::getPHPStringValue('gs_assets_without_risk_events','Ativos sem eventos de risco'));
	$moGridAbnormalitysSummary->setItem(4,5,'report_assets_without_risks');
	$moGridAbnormalitysSummary->setItem(2,6,FWDLanguage::getPHPStringValue('gs_non_parameterized_risks','Riscos n�o estimados'));
	$moGridAbnormalitysSummary->setItem(4,6,'report_risk_values');
	$moGridAbnormalitysSummary->setItem(2,7,FWDLanguage::getPHPStringValue('gs_controls_without_associated_risk','Controles sem risco associado'));
	$moGridAbnormalitysSummary->setItem(4,7,'report_controls_without_risks');

	$moQuery = new QuerySummaryAbnormalitys(FWDWebLib::getConnection());
	$moQuery->makeQuery();
	$moQuery->executeQuery();
	$maAbnormalitysSummary = $moQuery->getAbnormalitysSummary();

	$moGridAbnormalitysSummary->setItem(3,2,$maAbnormalitysSummary["pendant_tasks"]);
	$moGridAbnormalitysSummary->setItem(3,3,$maAbnormalitysSummary["areas_without_processes"]);
	$moGridAbnormalitysSummary->setItem(3,4,$maAbnormalitysSummary["processes_without_assets"]);
	$moGridAbnormalitysSummary->setItem(3,5,$maAbnormalitysSummary["assets_without_risks"]);
	$moGridAbnormalitysSummary->setItem(3,6,$maAbnormalitysSummary["non_parametrized_risks"]);
	$moGridAbnormalitysSummary->setItem(3,7,$maAbnormalitysSummary["controls_without_risks"]);
}
function populate_grid_isms_reports_and_documents(){
	/*** POPULA GRID 'RELAT�RIOS E DOCUMENTOS DO SGSI' ***/
	$moGridISMSReportsAndDocuments = FWDWebLib::getObject("grid_isms_reports_and_documents");
	$moGridISMSReportsAndDocuments->setObjFWDDrawGrid(new GridDiferentClassRowOne());

	$moGridISMSReportsAndDocuments->setItem(2,1,FWDLanguage::getPHPStringValue('gc_document_bl','<b>Documento</b>'));
	$moGridISMSReportsAndDocuments->setItem(1,2,'report_statement_of_applicability');
	$moGridISMSReportsAndDocuments->setItem(2,2,FWDLanguage::getPHPStringValue('gs_applicability_statement','Declara��o de Aplicabilidade'));
	$moGridISMSReportsAndDocuments->setItem(1,3,'report_risk_treatment_plan');
	$moGridISMSReportsAndDocuments->setItem(2,3,FWDLanguage::getPHPStringValue('gs_risk_treatment_plan','Plano de Tratamento do Risco'));

}
function populate_grid_financial_summary(){
	$moGridFinancialSummary = FWDWebLib::getObject("grid_financial_summary");
	$moGridFinancialSummary->setObjFWDDrawGrid(new GridFinancialSummary());
	$moConfig = new ISMSConfig();
	$msCurrency = constant($moConfig->getConfig(GENERAL_CURRENCY_IDENTIFIER));

	$moQuery = new QuerySummaryFinancial(FWDWebLib::getConnection());
	$moQuery->makeQuery();
	$moQuery->executeQuery();
	$maFinancialSummary = $moQuery->getFinancialSummary();

	$miAssetTotal = $maFinancialSummary["total_asset"];
	$miImplementedControlsTotal = $maFinancialSummary["cost1_control"] + $maFinancialSummary["cost2_control"] + $maFinancialSummary["cost3_control"] + $maFinancialSummary["cost4_control"] + $maFinancialSummary["cost5_control"];
	$miRiskTreatmentTotal1 = $maFinancialSummary["mitigated_risk"] + $maFinancialSummary["avoided_risk"] + $maFinancialSummary["transfered_risk"] + $maFinancialSummary["accepted_risk"];
	$miRiskTreatmentTotal2 = $maFinancialSummary["potentially_low_risk"] + $maFinancialSummary["not_treated_and_medium_high_risk"];

	if (!$miImplementedControlsTotal || !$miAssetTotal)
	$miControlProtectEfficiency = 0;
	else
	$miControlProtectEfficiency = round((1-($miImplementedControlsTotal/$miAssetTotal))*100,2);

	if (!$miImplementedControlsTotal || !$maFinancialSummary["mitigated_risk"])
	$miControlMitigateEfficiency = 0;
	else
	$miControlMitigateEfficiency = round((1-($miImplementedControlsTotal/$maFinancialSummary["mitigated_risk"]))*100,2);

	$miTotalTreatment = $maFinancialSummary["avoided_risk"] + $maFinancialSummary["transfered_risk"] + $maFinancialSummary["accepted_risk"];
	if (!$miImplementedControlsTotal || !$miTotalTreatment)
	$miControlTreatmentEfficiency = 0;
	else
	$miControlTreatmentEfficiency = round((1-($miImplementedControlsTotal/$miTotalTreatment))*100,2);

	$miImpactLeft = $maFinancialSummary["accepted_risk"] + $maFinancialSummary["not_treated_and_medium_high_risk"] + $maFinancialSummary["potentially_low_risk"];

	$moGridFinancialSummary->setItem(1,1,FWDLanguage::getPHPStringValue('gc_implemented_controls_bl','<b>Controles Implementados</b>'));
	$moGridFinancialSummary->setItem(2,1,FWDLanguage::getPHPStringValue('gc_investment_bl','<b>Investimento</b>'));

	$miLine = 2;
	for($i=1;$i<=5;$i++){
		$moGridFinancialSummary->setItem(1,$miLine,ISMSLib::getConfigById(constant("CONTROL_COST_{$i}")));
		$maFilter = array('cc'=>$i);
		$msValue = $msCurrency . " " . number_format($maFinancialSummary["cost{$i}_control"],2,'.',',');
		$msLink = getReportLink($msValue,'report_control_cost_by_cost_category','',$maFilter);
		$moGridFinancialSummary->setItem(2,$miLine,$msLink);
		$miLine++;
	}
	$moGridFinancialSummary->setItem(1,7,FWDLanguage::getPHPStringValue('gs_total_bl','<b>Total</b>'));

	$miValue = number_format($miImplementedControlsTotal,2,'.',',');
	$msLink = getReportLink($miValue,'report_control_cost','','',"<b>" . $msCurrency ,'</b>');
	$moGridFinancialSummary->setItem(2,7,$msLink);

	$moGridFinancialSummary->setItem(1,8,FWDLanguage::getPHPStringValue('gc_potential_impact_bl','<b>Impacto Potencial</b>'));
	$moGridFinancialSummary->setItem(2,8,FWDLanguage::getPHPStringValue('gc_financial_impact_bl','<b>Impacto Financeiro</b>'));

	$moGridFinancialSummary->setItem(1,9,FWDLanguage::getPHPStringValue('gs_mitigated_risk','Risco Reduzido'));
	$maFilter = array('t'=>ISMSReportRiskFinancialImpactFilter::REDUCED);
	$miValue = number_format($maFinancialSummary["mitigated_risk"],2,'.',',');
	$msLink = getReportLink($miValue,'report_risk_financial_impact','',$maFilter,"\$ ");
	$moGridFinancialSummary->setItem(2,9,$msLink);

	$moGridFinancialSummary->setItem(1,10,FWDLanguage::getPHPStringValue('gs_avoided_risk','Risco Evitado'));
	$maFilter = array('t'=>ISMSReportRiskFinancialImpactFilter::AVOIDED);
	$miValue = number_format($maFinancialSummary["avoided_risk"],2,'.',',');
	$msLink = getReportLink($miValue,'report_risk_financial_impact','',$maFilter,"\$ ");
	$moGridFinancialSummary->setItem(2,10,$msLink);

	$moGridFinancialSummary->setItem(1,11,FWDLanguage::getPHPStringValue('gs_transferred_risk','Risco Transferido'));
	$maFilter = array('t'=>ISMSReportRiskFinancialImpactFilter::TRANSFERRED);
	$miValue = number_format($maFinancialSummary["transfered_risk"],2,'.',',');
	$msLink = getReportLink($miValue,'report_risk_financial_impact','',$maFilter,"\$ ");
	$moGridFinancialSummary->setItem(2,11,$msLink);

	$moGridFinancialSummary->setItem(1,12,FWDLanguage::getPHPStringValue('gs_accepted_risk','Risco Aceito'));
	$maFilter = array('t'=>ISMSReportRiskFinancialImpactFilter::ACCEPTED);
	$miValue = number_format($maFinancialSummary["accepted_risk"],2,'.',',');
	$msLink = getReportLink($miValue,'report_risk_financial_impact','',$maFilter,"\$ ");
	$moGridFinancialSummary->setItem(2,12,$msLink);

	$moGridFinancialSummary->setItem(1,13,FWDLanguage::getPHPStringValue('gs_total_bl','<b>Total</b>'));
	$maFilter = array('t'=>ISMSReportRiskFinancialImpactFilter::TREATED);
	$miValue = number_format($miRiskTreatmentTotal1,2,'.',',');
	$msLink = getReportLink($miValue,'report_risk_financial_impact','',$maFilter,"\$ ");
	$moGridFinancialSummary->setItem(2,13,$msLink);

	$moGridFinancialSummary->setItem(1,14,FWDLanguage::getPHPStringValue('gc_potential_impact_bl','<b>Impacto Potencial</b>'));
	$moGridFinancialSummary->setItem(2,14,FWDLanguage::getPHPStringValue('gc_financial_impact_bl','<b>Impacto Financeiro</b>'));

	$moGridFinancialSummary->setItem(1,15,FWDLanguage::getPHPStringValue('gs_potential_risks_low','Riscos Potenciais - Baixo'));
	$maFilter = array(
    't' => ISMSReportRiskFinancialImpactFilter::NOT_TREATED,
    'v' => ISMSReportRiskFinancialImpactFilter::LOW
	);
	$miValue = number_format($maFinancialSummary['potentially_low_risk'],2,'.',',');
	$msLink = getReportLink($miValue,'report_risk_financial_impact','',$maFilter,"\$ ");
	$moGridFinancialSummary->setItem(2,15,$msLink);

	$moGridFinancialSummary->setItem(1,16,FWDLanguage::getPHPStringValue('gs_not_treated_risks_high_and_medium','Riscos N�o Tratados - Alto e M�dio'));
	$maFilter = array(
    't' => ISMSReportRiskFinancialImpactFilter::NOT_TREATED,
    'v' => ISMSReportRiskFinancialImpactFilter::MIDDLE
	|ISMSReportRiskFinancialImpactFilter::HIGH
	);
	$miValue = number_format($maFinancialSummary['not_treated_and_medium_high_risk'],2,'.',',');
	$msLink = getReportLink($miValue,'report_risk_financial_impact','',$maFilter,"\$ ");
	$moGridFinancialSummary->setItem(2,16,$msLink);

	$moGridFinancialSummary->setItem(1,17,FWDLanguage::getPHPStringValue('gs_total_bl','<b>Total</b>'));
	$maFilter = array(
    't' => ISMSReportRiskFinancialImpactFilter::NOT_TREATED,
    'v' => ISMSReportRiskFinancialImpactFilter::LOW
	|ISMSReportRiskFinancialImpactFilter::MIDDLE
	|ISMSReportRiskFinancialImpactFilter::HIGH
	);
	$miValue = number_format($miRiskTreatmentTotal2,2,'.',',');
	$msLink = getReportLink($miValue,'report_risk_financial_impact','',$maFilter,"\$ ");
	$moGridFinancialSummary->setItem(2,17,$msLink);

	$moGridFinancialSummary->setItem(1,18,FWDLanguage::getPHPStringValue('gc_efficiency_rates_bl','<b>Taxas de Efici�ncia</b>'));
	$moGridFinancialSummary->setItem(2,18,FWDLanguage::getPHPStringValue('gc_percentage_bl','<b>%</b>'));

	$miOldLeft = FWDWebLib::getObject('st_template_help')->getObjFWDBox()->getAttrLeft();
	FWDWebLib::getObject('st_template_help')->getObjFWDBox()->setAttrLeft(270);

	FWDWebLib::getObject('st_template_help_tt')->setValue(FWDLanguage::getPHPStringValue('tt_help_implemented_controls_protected_assets','<b>Controles Implementados/Ativos Protegidos</b><br/><br/>Raz�o entre a soma de custos dos controles implementados e o valor dos ativos protegidos.<br/>� importante n�o gastar mais com controles do que o valor dos ativos protegidos.'),true);
	$moGridFinancialSummary->setItem(1,19,FWDLanguage::getPHPStringValue('gs_implemented_controls_protected_assets_bl','Controles Implementados/Ativos Protegidos') . FWDWebLib::getObject('st_template_help')->draw() );
	$moGridFinancialSummary->setItem(2,19,"".$miControlProtectEfficiency."%");

	FWDWebLib::getObject('st_template_help_tt')->setValue(FWDLanguage::getPHPStringValue('tt_help_implemented_controls_mitigated_risks','<b>Controles Implementados/Riscos Reduzidos</b><br/><br/>Raz�o entre a soma de custos dos controles implementados e a soma dos impactos potenciais dos Riscos Reduzidos.<br/>Este valor demonstra qual o custo de controle frente a soma do potencial impacto de risco para a empresa em termos financeiros. Esta raz�o inclui apenas o valor dos riscos que foram reduzidos pela aplica��o de controles.'),true);
	$moGridFinancialSummary->setItem(1,20,FWDLanguage::getPHPStringValue('gs_implemented_controls_mitigated_risks_bl','Controles Implementados/Riscos Reduzidos') . FWDWebLib::getObject('st_template_help')->draw() );
	$moGridFinancialSummary->setItem(2,20,"".$miControlMitigateEfficiency."%");

	FWDWebLib::getObject('st_template_help_tt')->setValue(FWDLanguage::getPHPStringValue('tt_help_implemented_controls_treated_risks','<b>Controles Implementados/Riscos Tratados</b><br/><br/>Raz�o entre a soma de custos dos controles implementados e a soma dos impactos potenciais dos Riscos Tratados.<br/>Este valor demonstra qual o custo de controle frente a soma do potencial impacto de risco para a empresa em termos financeiros. Esta raz�o inclui todos os riscos tratados por qualquer uma das formas de tratamento poss�veis.'),true);
	$moGridFinancialSummary->setItem(1,21,FWDLanguage::getPHPStringValue('gs_implemented_controls_treated_risks_bl','Controles Implementados/Riscos Tratados') . FWDWebLib::getObject('st_template_help')->draw() );
	$moGridFinancialSummary->setItem(2,21,"".$miControlTreatmentEfficiency."%");

	FWDWebLib::getObject('st_template_help_tt')->setValue(FWDLanguage::getPHPStringValue('tt_help_impact_left','<b>Impacto Restante</b><br/><br/>Soma dos impactos dos riscos que restaram ap�s o tratamento.<br/>Este valor representa o total de impacto potencial a que a empresa est� sujeita ap�s o tratamento dos riscos.'),true);
	$moGridFinancialSummary->setItem(1,22,FWDLanguage::getPHPStringValue('gs_impact_left_bl','<b>Impacto Restante</b>') . FWDWebLib::getObject('st_template_help')->draw() );
	$maFilter = array('nta'=>true);
	$miValue = number_format($miImpactLeft,2,'.',',');
	$msLink = getReportLink($miValue,'report_risk_financial_impact','',$maFilter,"<b>\$ ","</b>");
	$moGridFinancialSummary->setItem(2,22,$msLink);

	FWDWebLib::getObject('st_template_help')->getObjFWDBox()->setAttrLeft($miOldLeft);
}

function populate_grid_control_summary(){

	$moQuery = new QuerySummaryControl(FWDWebLib::getConnection());
	$moQuery->makeQuery();
	$moQuery->executeQuery();
	$maControlSummary = $moQuery->getControlSummary();

	$moGridControl = FWDWebLib::getObject("grid_control_summary");
	$moGridControl->setObjFWDDrawGrid(new GridControlSummary());
	$miControlPlannedTotal = $maControlSummary["planned_implantation"] + $maControlSummary["planned_late"];

	// Cabe�alho de Controles Planejados
	$moGridControl->setItem(1,1,FWDLanguage::getPHPStringValue('gc_planned_controls_bl','<b>Controles Planejados</b>'));
	$moGridControl->setItem(2,1,FWDLanguage::getPHPStringValue('gs_amount_bl','<b>Qtde.</b>'));

	// Em processo de implanta��o
	$moGridControl->setItem(1,2,FWDLanguage::getPHPStringValue('gs_under_implementation','Em processo de implanta��o'));
	$miFilter = ISMSReportControlsFilter::PLANNED_IMPLANTATION;
	$msLink = getReportLink($maControlSummary["planned_implantation"],'report_controls','',$miFilter);
	$moGridControl->setItem(2,2,$msLink);

	// Atrasados
	$moGridControl->setItem(1,3,FWDLanguage::getPHPStringValue('gs_delayed','Atrasados'));
	$miFilter = ISMSReportControlsFilter::PLANNED_LATE;
	$msLink = getReportLink($maControlSummary["planned_late"],'report_controls','',$miFilter);
	$moGridControl->setItem(2,3,$msLink);

	// Total de controles planejados
	$moGridControl->setItem(1,4,FWDLanguage::getPHPStringValue('gs_total_bl','<b>Total</b>'));
	$miFilter = ISMSReportControlsFilter::PLANNED_TOTAL;
	$msLink = getReportLink($miControlPlannedTotal,'report_controls','',$miFilter);
	$moGridControl->setItem(2,4,$msLink);

	// Cabe�alho de Controles Implementados
	$moGridControl->setItem(1,5,FWDLanguage::getPHPStringValue('gc_implemented_controls_bl','<b>Controles Implementados</b>'));
	$moGridControl->setItem(2,5,FWDLanguage::getPHPStringValue('gs_amount_bl','<b>Qtde.</b>'));

	// Controles confi�veis e efficientes
	FWDWebLib::getObject('st_template_help_tt')->setValue(FWDLanguage::getPHPStringValue('tt_help_trusted_and_efficient','<b>Controles Confi�veis e Eficientes</b><br/><br/>Controles que possuem teste e revis�o de efici�ncia, que tanto o teste quanto a revis�o de efici�ncia n�o est�o atrazados, que o ultimo teste realizado foi confi�vel e que a ultima revis�o realizada foi efici�nte (efici�ncia real do controle maior ou igual a efici�ncia esperada este controle)'),true);
	$moGridControl->setItem(1,6,FWDLanguage::getPHPStringValue('gs_trusted_and_efficient_controls','Controles confi�veis e eficientes').FWDWebLib::getObject('st_template_help')->draw());
	$miFilter = ISMSReportControlsFilter::IMPLEMENTED_TRUSTED_AND_EFFICIENT;
	$msLink = getReportLink($maControlSummary["implemented_trusted_and_efficient"],'report_controls','',$miFilter);
	$moGridControl->setItem(2,6,$msLink);

	// Controles n�o confi�veis
	FWDWebLib::getObject('st_template_help_tt')->setValue(FWDLanguage::getPHPStringValue('tt_help_trusted','<b>Controles n�o Confi�veis</b><br/><br/>Controles que n�o possuem teste atrazado e cujo �ltimo teste foi marcado'),true);
	$moGridControl->setItem(1,7,FWDLanguage::getPHPStringValue('gs_not_trusted_controls','Controles n�o confi�veis').FWDWebLib::getObject('st_template_help')->draw());
	$miFilter = ISMSReportControlsFilter::CONTROL_TEST_NOT_OK;
	$msLink = getReportLink($maControlSummary["control_test_not_ok"],'report_controls','',$miFilter);
	$moGridControl->setItem(2,7,$msLink);

	// Controles n�o eficientes
	FWDWebLib::getObject('st_template_help_tt')->setValue(FWDLanguage::getPHPStringValue('tt_help_inefficient_controls','<b>Controles N�o Eficientes</b><br/><br/>Controles que n�o possuem revis�o de efici�ncia atrasada e cuja �ltima revis�o foi considerada como ineficiente (efici�ncia real do controle menor que a efici�ncia esperada deste controle)'),true);
	$moGridControl->setItem(1,8,FWDLanguage::getPHPStringValue('gs_inefficient_controls','Controles n�o eficientes').FWDWebLib::getObject('st_template_help')->draw());
	$miFilter = ISMSReportControlsFilter::CONTROL_REVISION_NOT_OK;
	$msLink = getReportLink($maControlSummary["control_revision_not_ok"],'report_controls','',$miFilter);
	$moGridControl->setItem(2,8,$msLink);

	// Controles n�o medidos
	FWDWebLib::getObject('st_template_help_tt')->setValue(FWDLanguage::getPHPStringValue('tt_help_not_measured_controls','<b>Controles N�o Medidos</b><br/><br/>Controles implementados que n�o possuem revis�o de efici�ncia ou teste ou cuja medi��o, revis�o de efici�ncia ou teste, expirou.'),true);
	$moGridControl->setItem(1,9,FWDLanguage::getPHPStringValue('gs_not_measured_controls','Controles n�o medidos').FWDWebLib::getObject('st_template_help')->draw());
	$miFilter = ISMSReportControlsFilter::CONTROL_NOT_MEASURED;
	$msLink = getReportLink($maControlSummary["control_not_measured"],'report_controls','',$miFilter);
	$moGridControl->setItem(2,9,$msLink);

	// Total de controles Implementados
	$moGridControl->setItem(1,10,FWDLanguage::getPHPStringValue('gs_total_bl','<b>Total</b>'));
	$miFilter = ISMSReportControlsFilter::IMPLEMENTED_TOTAL;
	$msLink = getReportLink($maControlSummary["implemented_total"],'report_controls','',$miFilter);
	$moGridControl->setItem(2,10,$msLink);

}

function populate_grid_doc_sumary(){
	$moGridDocSumary = FWDWebLib::getObject('grid_doc_sumary');
	$moGridDocSumary->setObjFWDDrawGrid(new GridDocumentSumary());

	$moHandlerDocByState = new QueryCountDocumentsByState(FWDWebLib::getConnection());
	$moHandlerDocsWithFiles = new QueryCountDocumentsWithFiles(FWDWebLib::getConnection());
	$moHandlerDocsWithLinks = new QueryCountDocumentsWithLinks(FWDWebLib::getConnection());

	$miAllDocs = 0;
	$maDocsByState = $moHandlerDocByState->getValues();
	foreach ($maDocsByState as $miCount){
		$miAllDocs += $miCount;
	}

	$miDocsWithFiles = $moHandlerDocsWithFiles->getValues();
	$miDocsWithLinks = $moHandlerDocsWithLinks->getValues();

	if((!$miDocsWithFiles)||($miDocsWithFiles < 0))
	$miDocsWithFiles=0;
	if((!$miDocsWithLinks)||($miDocsWithLinks < 0))
	$miDocsWithLinks=0;

	/*Popula a grid dos estados de um documento tt , si*/
	$miLine = 1;
	$moGridDocSumary->setItem(1,$miLine,FWDLanguage::getPHPStringValue('gc_documents_status_bl','<b>Status dos Documentos</b>'));
	$moGridDocSumary->setItem(2,$miLine,FWDLanguage::getPHPStringValue('gc_doc_sumary_qty_bl','<b>Qtde.</b>'));
	$moGridDocSumary->setItem(3,$miLine,FWDLanguage::getPHPStringValue('gc_doc_sumary_percentage_bl','<b>%</b>'));
	$miLine++;

	foreach($maDocsByState as $miKey=>$msValue){
		$moContextObj = new ISMSContextObject();
		$moGridDocSumary->setItem(1,$miLine,'&nbsp;&nbsp;'.$moContextObj->getContextStateAsString($miKey));
		/*
		 $msLink = getReportLink($msValue,'report_documents_by_state','PM',$miKey);
		 $moGridDocSumary->setItem(2,$miLine,$msLink);
		 /*/
		$moGridDocSumary->setItem(2,$miLine,$msValue);
		//*/
		if(!$msValue)
		$msValue = 0;
		if($miAllDocs)
		$moGridDocSumary->setItem(3,$miLine,number_format(($msValue/$miAllDocs)*100)."%");
		else
		$moGridDocSumary->setItem(3,$miLine,"0%");
		$miLine++;
	}

	$moGridDocSumary->setItem(1,$miLine,FWDLanguage::getPHPStringValue('gs_documents_total_bl','<b>Total de Documentos</b>'));
	$moGridDocSumary->setItem(2,$miLine,"<b>".$miAllDocs."</b>");
	if($miAllDocs)
	$moGridDocSumary->setItem(3,$miLine,"<b>100%</b>");
	else
	$moGridDocSumary->setItem(3,$miLine,"<b>0%</b>");
	$miLine++;

	$moGridDocSumary->setItem(1,$miLine,FWDLanguage::getPHPStringValue('gs_published_documents_information_bl','<b>Informa��es sobre Documentos Publicados</b>'));
	$moGridDocSumary->setItem(2,$miLine,FWDLanguage::getPHPStringValue('gc_doc_sumary_qty_bl','<b>Qtde.</b>'));
	$moGridDocSumary->setItem(3,$miLine,FWDLanguage::getPHPStringValue('gc_doc_sumary_percentage_bl','<b>%</b>'));
	$miLine++;
	$moContextObj = new ISMSContextObject();

	$moGridDocSumary->setItem(1,$miLine,'&nbsp;&nbsp;'.FWDLanguage::getPHPStringValue('gs_documents_with_files','Documentos com Arquivos'));
	$moGridDocSumary->setItem(2,$miLine,$miDocsWithFiles);
	if($maDocsByState[CONTEXT_STATE_DOC_APPROVED])
	$moGridDocSumary->setItem(3,$miLine,number_format(($miDocsWithFiles/$maDocsByState[CONTEXT_STATE_DOC_APPROVED])*100)."%");
	else
	$moGridDocSumary->setItem(3,$miLine,"0%");
	$miLine++;

	$moGridDocSumary->setItem(1,$miLine,'&nbsp;&nbsp;'.FWDLanguage::getPHPStringValue('gs_documents_with_links','Documentos com Links'));
	$moGridDocSumary->setItem(2,$miLine,$miDocsWithLinks);
	if($maDocsByState[CONTEXT_STATE_DOC_APPROVED])
	$moGridDocSumary->setItem(3,$miLine,number_format(($miDocsWithLinks/$maDocsByState[CONTEXT_STATE_DOC_APPROVED])*100)."%");
	else
	$moGridDocSumary->setItem(3,$miLine,"0%");
	$miLine++;

}
function populate_grid_doc_top10(){
	$moGridDocTop10 = FWDWebLib::getObject('grid_doc_top10');
	$moGridDocTop10->setObjFWDDrawGrid(new GridDiferentClassRowOne());
	$moHandlerDocTop10 = new QueryTop10DocumentsRead(FWDWebLib::getConnection());
	$maDocTop10 = $moHandlerDocTop10->getValues();

	if(count($maDocTop10)){
		$moGridDocTop10->setItem(1,1,FWDLanguage::getPHPStringValue('gc_document_bl','<b>Documento</b>'));
		$moGridDocTop10->setItem(2,1,FWDLanguage::getPHPStringValue('gc_readings_bl','<b>Leituras</b>'));
	}//else{} // nenhum documento foi lido no sistema, assim n�o deve-se popular a grid
	$miLine = 2;
	foreach($maDocTop10 as $miKey=>$maValues){
		$moGridDocTop10->setItem(1,$miLine,$maValues['name']);
		$moGridDocTop10->setItem(2,$miLine,$maValues['count']);
		$miLine++;
	}

}

function populate_grid_doc_period_revision(){
	$moGridDocPeriodRevision = FWDWebLib::getObject('grid_doc_period_revision');
	$moGridDocPeriodRevision->setObjFWDDrawGrid(new GridDiferentClassRowOne());
	$moHandlerDocRevisionPeriod = new QueryCountDocsRevisionPeriod(FWDWebLib::getConnection());
	$maRevPeriod = $moHandlerDocRevisionPeriod->getValues();

	/*Popula a grid de periodo de revis�o dos documentos*/
	$moGridDocPeriodRevision->setItem(1,1,FWDLanguage::getPHPStringValue('gc_time_period_smaller_or_equal_bl','<b>Per�odo de Tempo (menor ou igual a)</b>'));
	$moGridDocPeriodRevision->setItem(2,1,FWDLanguage::getPHPStringValue('gc_doc_rev_qty_bl','<b>Qtde.</b>'));
	$moGridDocPeriodRevision->setItem(1,2,FWDLanguage::getPHPStringValue('gs_one_week','Uma Semana'));
	$moGridDocPeriodRevision->setItem(2,2,$maRevPeriod[WEEK_INTERVAL]);
	$moGridDocPeriodRevision->setItem(1,3,FWDLanguage::getPHPStringValue('gs_one_month','Um M�s'));
	$moGridDocPeriodRevision->setItem(2,3,$maRevPeriod[MONTH_INTERVAL]);
	$moGridDocPeriodRevision->setItem(1,4,FWDLanguage::getPHPStringValue('gs_three_months','Tr�s Meses'));
	$moGridDocPeriodRevision->setItem(2,4,$maRevPeriod[TREE_MONTH_INTERVAL]);
	$moGridDocPeriodRevision->setItem(1,5,FWDLanguage::getPHPStringValue('gs_six_months','Seis Meses'));
	$moGridDocPeriodRevision->setItem(2,5,$maRevPeriod[SIX_MONTH_INTERVAL]);
	$moGridDocPeriodRevision->setItem(1,6,FWDLanguage::getPHPStringValue('gs_more_than_six_months','Maior que Seis Meses'));
	$moGridDocPeriodRevision->setItem(2,6,$maRevPeriod[YEAR_INTERVAL]);
}
function populate_grid_doc_approver_time(){
	$moGridDocDocApproverTime = FWDWebLib::getObject('grid_doc_approver_time');
	$moHandlerQueryCountDocsTimeToAprove = new QueryCountDocsTimeToApprove(FWDWebLib::getConnection());
	$msTimeToApprove = $moHandlerQueryCountDocsTimeToAprove->getValues();

	$moGridDocDocApproverTime->setItem(1,1,FWDLanguage::getPHPStringValue('gs_average_time','Tempo M�dio'));
	$moGridDocDocApproverTime->setItem(2,1,$msTimeToApprove);
}

/******************************** INFORMA��ES DO CI ***********************************/

function populate_grid_incident_summary(){
	$moGridIncidentSummary = FWDWebLib::getObject('grid_incident_summary');
	$moGridIncidentSummary->setObjFWDDrawGrid(new GridDiferentClassRowOne());
	$moHandler = new QuerySummaryIncident(FWDWebLib::getConnection());
	$maValues = $moHandler->getIncidentSummary();

	$moGridIncidentSummary->setItem(1,1,FWDLanguage::getPHPStringValue('gc_status_bl','<b>Status</b>'));
	$moGridIncidentSummary->setItem(2,1,FWDLanguage::getPHPStringValue('gc_incident_amount_bl','<b>Qtde. Incidentes</b>'));
	$miLine = 2;
	$maStates = array(CONTEXT_STATE_INC_OPEN, CONTEXT_STATE_INC_DIRECTED, CONTEXT_STATE_INC_PENDANT_DISPOSAL,
	CONTEXT_STATE_INC_WAITING_SOLUTION, CONTEXT_STATE_INC_PENDANT_SOLUTION, CONTEXT_STATE_INC_SOLVED);
	$moContextObj = new ISMSContextObject();
	foreach($maStates as $miState){
		$moGridIncidentSummary->setItem(1,$miLine,$moContextObj->getContextStateAsString($miState));

		$miValue = (isset($maValues[$miState]) ? $maValues[$miState] : 0);
		$maFilter = array('s'=>$miState);
		$msLink = getReportLink($miValue,'report_incidents','CI',$maFilter);
		$moGridIncidentSummary->setItem(2,$miLine,$msLink);

		$miLine++;
	}
}

function populate_grid_nc_summary(){
	$moGridNCSummary = FWDWebLib::getObject('grid_nc_summary');
	$moGridNCSummary->setObjFWDDrawGrid(new GridDiferentClassRowOne());
	$moHandler = new QuerySummaryNC(FWDWebLib::getConnection());
	$maValues = $moHandler->getNCSummary();

	$moGridNCSummary->setItem(1,1,FWDLanguage::getPHPStringValue('gc_status_bl','<b>Status</b>'));
	$moGridNCSummary->setItem(2,1,FWDLanguage::getPHPStringValue('gc_nc_amount','<b>Qtde. N�o Conformidades</b>'));
	$miLine = 2;
	$maStates = array(CONTEXT_STATE_CI_SENT, CONTEXT_STATE_CI_OPEN, CONTEXT_STATE_CI_DIRECTED, CONTEXT_STATE_CI_NC_PENDANT, CONTEXT_STATE_CI_AP_PENDANT,
	CONTEXT_STATE_CI_WAITING_CONCLUSION, CONTEXT_STATE_CI_FINISHED, CONTEXT_STATE_CI_CLOSED);
	$moContextObj = new ISMSContextObject();
	foreach($maStates as $miState){
		$moGridNCSummary->setItem(1,$miLine,$moContextObj->getContextStateAsString($miState));

		$miValue = (isset($maValues[$miState]) ? $maValues[$miState] : 0);
		$maFilter = array('s'=>$miState);
		$msLink = getReportLink($miValue,'report_non_conformities','CI',$maFilter);
		$moGridNCSummary->setItem(2,$miLine,$msLink);

		$miLine++;
	}
}

function populate_grid_incidents_per_user(){
	$moGrid = FWDWebLib::getObject('grid_incidents_per_user');
	$moGrid->setObjFWDDrawGrid(new GridDiferentClassRowOne());
	$moHandler = new QueryTop5IncidentsPerUser(FWDWebLib::getConnection());
	$maValues = $moHandler->getValues();

	if(count($maValues)){
		$moGrid->setItem(1,1,FWDLanguage::getPHPStringValue('gc_users_bl','<b>Funcion�rio</b>'));
		$moGrid->setItem(2,1,FWDLanguage::getPHPStringValue('gc_disciplinary_process_amount_bl','<b>Qtde. Processos Disciplinares</b>'));
		$miLine = 2;
		foreach($maValues as $miKey => $maValues){
			$moGrid->setItem(1,$miLine, $maValues[0]);

			$maFilter = array('u'=>$miKey);
			$msLink = getReportLink($maValues[1],'report_disciplinary_process','CI',$maFilter);
			$moGrid->setItem(2,$miLine,$msLink);

			$miLine++;
		}
	}
}

function populate_grid_incidents_per_asset(){
	$moGrid = FWDWebLib::getObject('grid_incidents_per_asset');
	$moGrid->setObjFWDDrawGrid(new GridDiferentClassRowOne());
	$moHandler = new QueryTop5IncidentsPerAsset(FWDWebLib::getConnection());
	$maValues = $moHandler->getValues();

	if (count($maValues)) {
		$moGrid->setItem(1,1,FWDLanguage::getPHPStringValue('gc_assets_bl','<b>Ativos</b>'));
		$moGrid->setItem(2,1,FWDLanguage::getPHPStringValue('gc_incident_amount_bl','<b>Qtde. Incidentes</b>'));
		$miLine = 2;
		foreach($maValues as $miKey => $maValues){
			$moGrid->setItem(1,$miLine, $maValues[0]);

			$maFilter = array('a'=>$miKey);
			$msLink = getReportLink($maValues[1],'report_incidents','CI',$maFilter);
			$moGrid->setItem(2,$miLine,$msLink);

			$miLine++;
		}
	}
}

function populate_grid_incidents_per_process(){
	$moGrid = FWDWebLib::getObject('grid_incidents_per_process');
	$moGrid->setObjFWDDrawGrid(new GridDiferentClassRowOne());
	$moHandler = new QueryTop5IncidentsPerProcess(FWDWebLib::getConnection());
	$maValues = $moHandler->getValues();

	if(count($maValues)){
		$moGrid->setItem(1,1,FWDLanguage::getPHPStringValue('gc_processes_bl','<b>Processos</b>'));
		$moGrid->setItem(2,1,FWDLanguage::getPHPStringValue('gc_incident_amount_bl','<b>Qtde. Incidentes</b>'));
		$miLine = 2;
		foreach($maValues as $miKey => $maValues){
			$moGrid->setItem(1,$miLine, $maValues[0]);

			$maFilter = array('p'=>$miKey);
			$msLink = getReportLink($maValues[1],'report_incidents','CI',$maFilter);
			$moGrid->setItem(2,$miLine,$msLink);

			$miLine++;
		}
	}
}

function populate_grid_nc_per_process(){
	$moGrid = FWDWebLib::getObject('grid_nc_per_process');
	$moGrid->setObjFWDDrawGrid(new GridDiferentClassRowOne());
	$moHandler = new QueryTop5NCPerProcess(FWDWebLib::getConnection());
	$maValues = $moHandler->getValues();

	if(count($maValues)){
		$moGrid->setItem(1,1,FWDLanguage::getPHPStringValue('gc_processes_bl','<b>Processos</b>'));
		$moGrid->setItem(2,1,FWDLanguage::getPHPStringValue('gc_nc_amount_bl','<b>Qtde. N�o Conformidades</b>'));
		$miLine = 2;
		foreach($maValues as $miKey => $maValues){
			$moGrid->setItem(1,$miLine, $maValues[0]);

			$maFilter = array('p'=>$miKey);
			$msLink = getReportLink($maValues[1],'report_non_conformities','CI',$maFilter);
			$moGrid->setItem(2,$miLine,$msLink);

			$miLine++;
		}
	}
}

function populate_grid_abnormality_ci(){
	$moGrid = FWDWebLib::getObject('grid_abnormality_ci');
	$moGrid->setObjFWDDrawGrid(new GridAbnormalitysSummary());

	$moGrid->setItem(2,1,FWDLanguage::getPHPStringValue('gc_description_bl','<b>Descri��o</b>'));
	$moGrid->setItem(3,1,FWDLanguage::getPHPStringValue('gc_amount_bl','<b>Qtde.</b>'));

	$moGrid->setItem(2,2,FWDLanguage::getPHPStringValue('gs_ci_pending_tasks','Tarefas pendentes da Melhoria Cont�nua'));
	$moGrid->setItem(4,2,'report_summarized_pendant_tasks_ci');
	$moGrid->setItem(2,3,FWDLanguage::getPHPStringValue('gs_ci_incident_without_risks','Incidentes sem Risco'));
	$moGrid->setItem(4,3,'report_incident_without_risk');
	$moGrid->setItem(2,4,FWDLanguage::getPHPStringValue('gs_ci_cn_without_ap','N�o conformidade sem Plano de A��o'));
	$moGrid->setItem(4,4,'report_nc_without_ap');
	$moGrid->setItem(2,5,FWDLanguage::getPHPStringValue('gs_ci_incident_without_occurrence','Incidentes sem Ocorr�ncia'));
	$moGrid->setItem(4,5,'report_incident_without_occurrence');

	$moHandler = new QuerySummaryAbnormalityCI(FWDWebLib::getConnection());
	$maValues = $moHandler->getValues();

	$moGrid->setItem(3,2,$maValues['pendant_tasks_ci']);
	$moGrid->setItem(3,3,$maValues['incident_without_risk']);
	$moGrid->setItem(3,4,$maValues['nc_without_ap']);
	$moGrid->setItem(3,5,$maValues['incident_without_occurrence']);

	if (FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(POLICY_MODE)) {
		$moGrid->setItem(2,6,FWDLanguage::getPHPStringValue('gs_ci_ap_without_doc','Plano de A��o sem Documento'));
		$moGrid->setItem(4,6,'report_ap_without_doc');
		$moGrid->setItem(3,6,$maValues['pa_without_doc']);
		$moGrid->getObjFWDBox()->setAttrHeight(140);
	}

}

function populate_grid_doc_context_with_documents(){
	$moGridDocCtxDocs = FWDWebLib::getObject('grid_doc_context_with_documents');
	$moGridDocCtxDocs->setObjFWDDrawGrid(new GridDiferentClassRowOne());
	$moHandlerDocsByType = new QueryCountDocumentsByType(FWDWebLib::getConnection());
	$maDocsByType = $moHandlerDocsByType->getValues();

	$moGridDocCtxDocs->setItem(2,1,FWDLanguage::getPHPStringValue('gc_documents_qty_bl','<b>Qtde. Documentos</b>'));
	$moGridDocCtxDocs->setItem(3,1,FWDLanguage::getPHPStringValue('gc_percentage_bl','<b>%</b>'));

	$miLine = 2;
	foreach($maDocsByType as $miKey=>$maValues){
		$moContextObj = new ISMSContextObject();
		$moCtx = $moContextObj->getContextObject($miKey);
		$moGridDocCtxDocs->setItem(1,$miLine,$moCtx->getLabel());
		$moGridDocCtxDocs->setItem(2,$miLine,$maValues['doc']?$maValues['doc']:0);

		$mfPercentual = 0;
		if($maValues['all']){
			if($maValues['doc']>$maValues['all'])
			$mfPercentual = 100;
			else
			if($maValues['all'])
			$mfPercentual = ($maValues['doc'] / $maValues['all'])*100;
			else
			$mfPercentual = 100;
		}
		$moGridDocCtxDocs->setItem(3,$miLine,number_format($mfPercentual,0)."%");


		$miLine++;
	}
}

function populate_grid_abnormality_pm(){
	$moGrid = FWDWebLib::getObject('grid_abnormality_pm');
	$moGrid->setObjFWDDrawGrid(new GridAbnormalitysSummary());

	$moGrid->setItem(2,1,FWDLanguage::getPHPStringValue('gc_description_bl','<b>Descri��o</b>'));
	$moGrid->setItem(3,1,FWDLanguage::getPHPStringValue('gc_amount_bl','<b>Qtde.</b>'));

	$moGrid->setItem(2,2,FWDLanguage::getPHPStringValue('gs_pm_pending_tasks','Tarefas pendentes da Gest�o de Pol�ticas'));
	$moGrid->setItem(4,2,'report_summarized_pendant_tasks_pm');
	$moGrid->setItem(2,3,FWDLanguage::getPHPStringValue('gs_pm_incident_without_risks','Documentos com uma alta frequ�ncia de revis�o'));
	$moGrid->setItem(4,3,'report_docs_with_high_frequency_revision');
	$moGrid->setItem(2,4,FWDLanguage::getPHPStringValue('gs_pm_docs_with_pendant_read','Documentos com leitura pendente'));
	$moGrid->setItem(4,4,'report_documents_pendent_reads');
	$moGrid->setItem(2,5,FWDLanguage::getPHPStringValue('gs_components_without_document','Componentes sem Documento'));
	$moGrid->setItem(4,5,'report_contexts_without_documents');
	$moGrid->setItem(2,6,FWDLanguage::getPHPStringValue('gs_pm_never_read_documents','Documentos nunca lidos'));
	$moGrid->setItem(4,6,'report_not_accessed_documents');
	$moGrid->setItem(2,7,FWDLanguage::getPHPStringValue('gs_pm_documents_without_register','Documentos sem Registros'));
	$moGrid->setItem(4,7,'report_documents_without_register');
	$moGrid->setItem(2,8,FWDLanguage::getPHPStringValue('gs_pm_users_with_pendant_read','Usu�rios com leitura pendente'));
	$moGrid->setItem(4,8,'report_users_with_pendant_read');

	$moHandler = new QuerySummaryAbnormalityPM(FWDWebLib::getConnection());
	$maValues = $moHandler->getValues();

	$moGrid->setItem(3,2,$maValues['pendant_tasks']);
	$moGrid->setItem(3,3,$maValues['docs_with_high_frequency_revision']);
	$moGrid->setItem(3,4,$maValues['docs_with_pendant_read']);
	$moGrid->setItem(3,5,$maValues['rm_contexts_without_document']);
	$moGrid->setItem(3,6,$maValues['pm_document_never_read']);
	$moGrid->setItem(3,7,$maValues['pm_document_without_register']);
	$moGrid->setItem(3,8,$maValues['pm_user_with_pendant_reads']);

}

function getModules(){
	return array('RM','PM','CI');
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$moWebLib = FWDWebLib::getInstance();

		$moPreference = new ISMSUserPreferences();
		if($moPreference->preferenceUserExistes("dashboard_sumary_pref")){
			$maPrefUser = FWDWebLib::unserializeString($moPreference->getPreference("dashboard_sumary_pref"));
			$maPrefUserKeys = array();
			if(is_array($maPrefUser)){
				$maPrefUserKeys = array_flip($maPrefUser);
			}
		}else{
			$maPrefUserKeys = getArrayGrids();
			$maValuesKeys ="";
			foreach($maPrefUserKeys as $miKey=>$maValue){
				$maValuesKeys[] = $miKey;
			}
			$moPreference->setPreference("dashboard_sumary_pref",serialize($maValuesKeys));
		}
		$maGrids = getArrayGrids();

		$mbColumnOneRM = true;
		$mbColumnOnePM = true;
		$mbColumnOneCI = true;
		$miTopCommon = 30;

		$miTopOneRM = $miTopCommon;
		$miTopTwoRM = $miTopCommon;
		$miTopOnePM = $miTopCommon;
		$miTopTwoPM = $miTopCommon;
		$miTopOneCI = $miTopCommon;
		$miTopTwoCI = $miTopCommon;

		$miTopFirstPanel = 15;
		$miLeftOne = 10;
		$miLeftTwo = 485;
		$miSpaceBetweenElements = 30;

		if(count($maPrefUserKeys)){
			foreach($maGrids as $miKey=>$maValues){
				FWDWebLib::getObject($miKey)->setShouldDraw(false);
			}
			foreach ($maPrefUserKeys as $miKey=>$maValues){
				$moGrid = FWDWebLib::getObject($miKey);
				if(isset($maGrids[$miKey])){
					$msModule = strtoupper($maGrids[$miKey]['module']);
					if(${"mbColumnOne".$msModule}){
						$moGrid->getObjFWDBox()->setAttrLeft($miLeftOne);
						$moGrid->getObjFWDBox()->setAttrTop(${"miTopOne".$msModule});
						${"miTopOne".$msModule} = ${"miTopOne".$msModule} + $maGrids[$miKey]['height'] + $miSpaceBetweenElements;
					}else{
						$moGrid->getObjFWDBox()->setAttrLeft($miLeftTwo);
						$moGrid->getObjFWDBox()->setAttrTop(${"miTopTwo".$msModule});
						${"miTopTwo".$msModule} = ${"miTopTwo".$msModule} + $maGrids[$miKey]['height'] + $miSpaceBetweenElements;
					}
					${"mbColumnOne".$msModule} = !${"mbColumnOne".$msModule};

					/*executa a fun��o que popula a grid correspondente, fun��o no formato "populate_"
					 * mais o nome da grid definido no xml */
					$msFuctionName = "populate_".$miKey;
					$msFuctionName();

					$moGrid->setShouldDraw(true);
				}
			}
		}else{
			foreach ($maPrefUserKeys as $miKey=>$maValues){
				$moGrid = FWDWebLib::getObject($miKey);
				$msModule = strtoupper($maGrids[$miKey]['module']);
				if(${"mbColumnOne".$msModule}){
					$moGrid->getObjFWDBox()->setAttrLeft($miLeftOne);
					$moGrid->getObjFWDBox()->setAttrTop(${"miTopOne".$msModule});
					${"miTopOne".$msModule} = ${"miTopOne".$msModule} + $maGrids[$miKey]['height'] + $miSpaceBetweenElements;
				}else{
					$moGrid->getObjFWDBox()->setAttrLeft($miLeftTwo);
					$moGrid->getObjFWDBox()->setAttrTop(${"miTopTwo".$msModule});
					${"miTopTwo".$msModule} = ${"miTopTwo".$msModule} + $maGrids[$miKey]['height'] + $miSpaceBetweenElements;
				}
				${"mbColumnOne".$msModule} = !${"mbColumnOne".$msModule};

				/*executa a fun��o que popula a grid correspondente, fun��o no formato "populate_"
				 * mais o nome da grid definido no xml */
				$msFuctionName = "populate_".$miKey;
				$msFuctionName();
				$moGrid->setShouldDraw(true);
			}
		}

		/*atualiza a altura dos panels dos m�dulos*/
		$miValuePast = $miTopFirstPanel;
		foreach (getModules() as $strModule){
			$miValue = max(${"miTopOne".$strModule},${"miTopTwo".$strModule});
			FWDWebLib::getObject(strtolower($strModule).'_sumary')->getObjFWDBox()->setAttrTop($miValuePast);
			if($miValue > $miTopCommon){
				FWDWebLib::getObject(strtolower($strModule).'_sumary')->getObjFWDBox()->setAttrHeight( $miValue + $miSpaceBetweenElements);
				$miValuePast = $miValuePast + $miValue + 2*$miSpaceBetweenElements;
			}else{
				FWDWebLib::getObject(strtolower($strModule).'_sumary')->setShouldDraw(false);
			}
		}

		$miTopOne = $miTopOneRM + $miTopOnePM + $miTopOneCI;
		$miTopTwo = $miTopTwoRM + $miTopTwoPM + $miTopTwoCI;
		FWDWebLib::getObject('dialog')->getObjFWDBox()->setAttrHeight(max($miTopOne,$miTopTwo,430));

		$mbSurveyDisplayed = false;
		$msSurvey = "";
		$moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
		$miUserId = $moSession->getUserId(true);
		$moUser = new ISMSUser();
		if ($moUser->fetchById($miUserId)){
			$mbSurveyDisplayed = (bool)$moUser->getFieldValue("user_answered_survey");
		}
		if (!$mbSurveyDisplayed && $moSession->attributeExists("surveyDisplayed")) {
			$mbSurveyDisplayed = ($moSession->getAttrSurveyDisplayed() === true);
		}
		if (!$mbSurveyDisplayed && ISMSSaaS::getConfig(ISMSSaaS::LICENSE_TYPE)==ISMSLicense::LICENSE_TYPE_TRIAL_ISMS) {
			$msSurvey = "isms_open_popup('popup_survey','popup_survey.php','','true');";
			if (!$moSession->attributeExists("surveyDisplayed")) {
				$moSession->addAttribute('surveyDisplayed');
			}
			$moSession->setAttrSurveyDisplayed(true);
		}

		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
<script language="javascript">
      function refresh(){
       showLoading();
       document.forms[0].submit();
       
      }
      
      function generateReport(psName,psModule,psFilter){
        gebi('report_name').value = psName;
        gebi('report_format').value = REPORT_FILETYPE_HTML;
        if(psFilter){
          gebi('report_filter').value = psFilter;
        }else{
          gebi('report_filter').value = '';
        }
        if(psModule){
          trigger_event('function_report_'+psModule,3);
        }else{
          trigger_event('function_report',3);
        }
      }
      
      <?=$msSurvey?>
    </script>
      <?
	}
}
FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_summary_advanced.xml");
?>