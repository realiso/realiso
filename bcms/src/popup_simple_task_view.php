<?php
include_once "include.php";
include_once $handlers_ref . "grid/QueryGridUserTasks.php";

class ContextEvent extends FWDRunnable {
  public function run(){
  	$miTaskId = FWDWebLib::getObject('task_id')->getValue();

    //teste para verificar se o sistema n�o est� sendo hakeado
    $moTaskTest = new WKFTask();
    $moTaskTest->fetchById($miTaskId);
    $moTaskTest->testPermissionToExecute($miTaskId);
    $miContextId = $moTaskTest->getFieldValue('task_context_id');
    $msJustification = FWDWebLib::getObject('task_justification')->getValue();
    $miAction = FWDWebLib::getObject('action')->getValue();

  	$moContextObject = new ISMSContextObject();
  	$moContext = $moContextObject->getContextObjectByContextId($miContextId);
  	$moContext->fetchById($miContextId);
		$moContext->stateForward($miAction, $msJustification);

		$moTask = new WKFTask();
		$moTask->setFieldValue('task_date_accomplished', ISMSLib::ISMSTime());
		$moTask->setFieldValue('task_is_visible', 0);
		$moTask->update($miTaskId);

		$msNextTaskJS = "";
		$mbOpenNextTask = (bool)array_filter(explode(':',FWDWebLib::getObject('next_task_controller')->getValue()));
		if ($mbOpenNextTask) {
		  $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
		  $moHandlerTask = new QueryGridUserTasks(FWDWebLib::getConnection());
      $moHandlerTask->setUser($miUserId);
      $moHandlerTask->setOrderBy('task_date_created','-');
      $moHandlerTask->makeQuery();
      $moHandlerTask->executeQuery();
      if ($moHandlerTask->fetch()) {
  		  $msNextTaskJS = 'soWindow.setTimeout("'.ISMSLib::getTaskPopup($moHandlerTask->getFieldValue('task_id'),
  		                                        $moHandlerTask->getFieldValue('task_activity'),
  		                                        $moHandlerTask->getFieldValue('task_context_id'),
  		                                        $moHandlerTask->getFieldValue('task_context_type'),
  		                                        2).',1");';
      }
		}
		echo "soWindow = soPopUpManager.getPopUpById('popup_simple_task_view').getOpener();"
	        ."if (soWindow.refresh_grids)"
	        ."  soWindow.refresh_grids();"
	        .$msNextTaskJS
	        ."soPopUpManager.closePopUp('popup_simple_task_view');";
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new ContextEvent('context_event'));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $miAutoTaskOpen = FWDWebLib::getObject('auto_task_open')->getValue();
    if (!$miAutoTaskOpen) FWDWebLib::getObject('next_task')->setShouldDraw(false);
    if ($miAutoTaskOpen==2) FWDWebLib::getObject('next_task_controller')->checkItem(1);

    $mbDumpHTML = true;
    $miTaskId = FWDWebLib::getObject('task_id')->getValue();
    $moTask = new WKFTask();
    $moTask->fetchById($miTaskId);

    //teste para verificar se o sistema n�o est� sendo hakeado
    $moTask->testPermissionToExecute($miTaskId);

    FWDWebLib::getObject('task_date')->setValue(ISMSLib::getISMSDate($moTask->getFieldValue('task_date_created')));

    $moUser = new ISMSUser();
    $moUser->fetchById($moTask->getFieldValue('task_creator_id'));
    FWDWebLib::getObject('task_inquirer')->setValue($moUser->getFieldValue('user_name'));

    FWDWebLib::getObject('task_name')->setValue(ISMSActivity::getDescription($moTask->getFieldValue('task_activity')));

    $moContextObject = new ISMSContextObject();
    $moContext = $moContextObject->getContextObjectByContextId($moTask->getFieldValue('task_context_id'));
    if($moContext){
      if($moContext->fetchById($moTask->getFieldValue('task_context_id'))){
        FWDWebLib::getObject('context_type')->setValue($moContext->getLabel().":");

    		$msContextName = $moContext->getName();
    		if (strlen($msContextName) > 35) {
    			$msContextName = substr($msContextName,0,32)."...";
    		}
        $msContextLink = "<a href='javascript:open_visualize(".$moContext->getId().",".$moContext->getContextType().",\"".uniqid()."\")'>".$msContextName."</a>";
        FWDWebLib::getObject('context_name')->setAttrStringNoEscape(true);
        FWDWebLib::getObject('context_name')->setValue($msContextLink);
      }else{
        $mbDumpHTML = false;
      }
    }else{
      $mbDumpHTML = false;
    }
    if($mbDumpHTML){
        FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    }else{
        return FWDLanguage::getPHPStringValue('st_denied_permission_to_execute_task','Voc� n�o tem permiss�o para executar a tarefa ');
    }
    ?>
      <script language="javascript">
        gebi('task_justification').focus();
      </script>
    <?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_simple_task_view.xml");
?>