<?php
include_once "include.php";

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $msAttrAlias = FWDWebLib::getObject("param_attribute")->getValue();
    $msAttrName = "";
    switch($msAttrAlias) {
      case "assets"     : $msAttrName = FWDLanguage::getPHPStringValue('st_assets', "ativos"); break;
      case "modules"    : $msAttrName = FWDLanguage::getPHPStringValue('st_modules', "m�dulos"); break;
      case "standards"  : $msAttrName = FWDLanguage::getPHPStringValue('st_standards', "normas"); break;
      case "users"      : $msAttrName = FWDLanguage::getPHPStringValue('st_users', "usu�rios"); break;
      default           : trigger_error("Invalid License Attribute : $msAttrAlias",E_USER_ERROR); break;
    }
    $msTitle = FWDWebLib::getObject("license_limit_reached_title")->getValue();
    $msTitle = str_replace("%attribute_name%",$msAttrName,$msTitle);
    $msMessage = FWDWebLib::getObject("license_limit_reached_message")->getValue();
    $msMessage = str_replace("%attribute_name%",$msAttrName,$msMessage);
    FWDWebLib::getObject("license_limit_reached_title")->setValue($msTitle);
    FWDWebLib::getObject("license_limit_reached_message")->setValue($msMessage);
    $msEvent = "soPopUpManager.closePopUp('popup_license_limit_reached');";
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("license_limit_reached.xml");
?>