<?php

include_once "include.php";
include_once $handlers_ref . "QueryTasks.php";
set_time_limit(600);
define('DEADLINE_RECIVE_PAYMENT', 2);

// Verifica se o usu�rio n�o est� tentando acessar o sistema pela URL errada
ISMSLib::checkUrl();

function showBlockedMessage(){
	$msTitle = FWDLanguage::getPHPStringValue('tt_blocked_instance',"Inst�ncia Bloqueada");
	$msMessage = "<b>Your system has been blocked.</b><br/><br/>"
	."For immediate assistance, please feel free to contact us at %email% between 6:00 AM and 6:00 PM Atlantic Time, "
	."Monday through Friday.<br>"
	."<br>".
              "We apologize for any inconvenience.";
	$msEmail = ISMSSaas::getConfig(ISMSSaas::SAAS_REPORT_EMAIL);
	$msMessage = str_replace('%email%',$msEmail,$msMessage);
	ISMSLib::openOk($msTitle,$msMessage,"",120);
}

class ShowBlockedMessageEvent extends FWDRunnable {
	public function run(){
		showBlockedMessage();
	}
}

class LogoutEvent extends FWDRunnable {
	public function run(){
		$moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
		$moSession->logout();
		echo "gebi('user_password').value = '';";
	}
}

class LoginEvent extends FWDRunnable {
	public function run() {
		$moConfig = new ISMSConfig();
		$moSaaSConfig = new ISMSSaas();
		if(!$moSaaSConfig->getConfig(SYSTEM_IS_ACTIVE)){
			if($moConfig->getConfigFromDB(TRIAL_INSTANCE)){
				echo "show_asaas_popup();";
			} else {
				showBlockedMessage();
			}

			exit();
		}

		if($moConfig->getConfigFromDB(CLIENT_ID_ASAAS)){
			$moISMSASaaS = new ISMSASaaS();
			$planCode = $moISMSASaaS->getPlanCode();
			if($planCode != 'TRIAL' && $moConfig->getConfigFromDB(TRIAL_INSTANCE)){
		        $dataActivation = $moISMSASaaS->getCoverInitialPayments();
		        $statusPayment = $moISMSASaaS->getStatusPayment();
		        $dataNow = time();
		        $secs = $dataNow - strtotime($dataActivation);
		        $days = ceil($secs / 86400);
		        if($days > DEADLINE_RECIVE_PAYMENT and $statusPayment=="WAIT_PAYMENT"){
		        	echo "show_asaas_popup_payment_alert();";
		        }
	    	}
	    }

		$moCrypt = new FWDCrypt();
		$msLogin = FWDWebLib::getObject('user_login')->getValue();
		$msPassword = FWDWebLib::getObject('user_password')->getValue();
		if (isset($_COOKIE['ISMSPASSWORD']) && $msPassword=="PASSWORD") {
			$msPassword = $moCrypt->decryptEmbeddedIV($_COOKIE['ISMSPASSWORD']);
		} else {
			$msPassword = md5($msPassword);
		}

		$moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());

		/*
		 * Testa situa��o em que pode ocorrer sobreposi��o de sess�o.
		 */
		if ($moSession->getUserId()) {
			$moTestUser = new ISMSUser();
			$moTestUser->createFilter($msLogin, 'user_login');
			$moTestUser->createFilter($msPassword, 'user_password');
			$moTestUser->select();
			if ($moTestUser->fetch() && ($moTestUser->getFieldValue('user_id') != $moSession->getUserId())) {
				$msTitle = FWDLanguage::getPHPStringValue('tt_session_conflict','Falha no Login');
				$msMessage = FWDLanguage::getPHPStringValue('mx_session_conflict',"O login falhou pois existe outro usu�rio logado no sistema com esse mesmo browser.");
				ISMSLib::openOk($msTitle,$msMessage,"",40);
				exit();
			}
		}

		if ($moSession->attributeExists("loginWithoutPassword")) {
			if ($moSession->getAttrLoginWithoutPassword() == $msLogin)
			$msPassword='';
			$moSession->deleteAttribute("loginWithoutPassword");
		}

		if ($moSession->attributeExists("loginExpired"))
		$moSession->deleteAttribute("loginExpired");

		if ($moSession->login($msLogin, $msPassword)) {
			$moUser = new ISMSUser();
			$miUserId = $moSession->getUserId();
			$moUser->fetchById($miUserId);

			//caso jah se tenha alcan�ado o numero maximo de usuarios simultaneos no sistema, ele nao deve logar
			$moLicense = new ISMSLicense();
			$miMaxSimUsers = $moLicense->getMaxSimultaneousUsers();
			$moSelectUser = new ISMSUser();
			$moSelectUser->select();
			$miCountUsers = 0;
			while($moSelectUser->fetch()) {
				$msSession = $moSelectUser->getFieldValue('user_session_id');
				if ($msSession && file_exists(FWDWebLib::getInstance()->getSysRef().'session/'.$msSession)) {
					$miCountUsers++;
				}
			}
			if ($miCountUsers > $miMaxSimUsers) { //A minha sessao existe, logo deve ser contada. Por isso que nao eh >=
				echo "trigger_event('logout_event',3);js_show('warning_max_users');";
				exit;
			}

			//caso o usu�rio tenha sido deletado e ainda est� na lixeira, ele n�o deve logar no sistema
			$moCtxObj = new ISMSContextObject();
			$moCtxObj->fetchById($miUserId);
			if($moCtxObj->getFieldValue('context_state')==CONTEXT_STATE_DELETED){
				echo "trigger_event('logout_event',3);js_show('warning');";
				exit;
			}

			//se o usu�rio n�o existir na tabela de hist�rico de senhas, inserir um registro.
			$moUserPasswordHistory = new ISMSUserPasswordHistory();
			if (!$moUserPasswordHistory->fetchById($miUserId)) {
				$moUserPasswordHistory->addPassword($msPassword);
			}

			if ($msPassword) { //fazer a verificacao seguinte apenas se o usu�rio n�o est� logando-se sem senha (se ele ainda n�o passou pela mudan�a de senha).
				if ($moUserPasswordHistory->willPasswordExpire($miUserId)) { //se a senha + n dias < hoje, significa que a senha vai expirar dentro de n dias.
					echo "isms_open_popup('popup_user_change_password', 'popup_user_change_password.php', '', 'true');";
					exit;
				}
			}

			/* Gravar dados de cookies, se o usu�rio tiver marcado essa op��o */
			$maRemember = array_filter(explode(':',FWDWebLib::getObject('remember_controller')->getValue()));
			if (in_array('login',$maRemember)) {
				setcookie('ISMSLOGIN',$moCrypt->getIV().$moCrypt->encrypt($msLogin),time()+60*60*24*365); //1-year cookie
				if (in_array('password',$maRemember) && $msPassword) {
					setcookie('ISMSPASSWORD',$moCrypt->getIV().$moCrypt->encrypt($msPassword),time()+60*60*24*365); //1-year cookie
				} else {
					setcookie('ISMSPASSWORD',false);
				}
			} else {
				setcookie('ISMSLOGIN',false);
				setcookie('ISMSPASSWORD',false);
			}

			/* Se foi passada uma task, deve abrir uma popup para edi��o da mesma */
			$miTaskId = FWDWebLib::getObject("task_id")->getValue();
			$moSession->cleanAttribute("task");
			if ($miTaskId) {
				$moQueryTasks = new QueryTasks(FWDWebLib::getConnection());
				$moQueryTasks->setUser($miUserId);
				$moQueryTasks->setTask($miTaskId);
				$moQueryTasks->makeQuery();
				$moQueryTasks->executeQuery();
				$moQueryTasksDataset = $moQueryTasks->getDataset();
				if ($moQueryTasksDataset->fetch()) {

					//o login foi aberto via default.php q foi aberto por um link de task
					//assim deve-se mudar o modo de path do isms para o de task para que
					//o path da popup de visualiza��o de contextos do isms
					echo " setISMSMode(TASK_EMAIL_MODE); ";

					$miActivity = $moQueryTasksDataset->getFieldByAlias("task_activity")->getValue();
					$miContextId = $moQueryTasksDataset->getFieldByAlias("task_context_id")->getValue();
					$miContextType = $moQueryTasksDataset->getFieldByAlias("task_context_type")->getValue();
					if(($miActivity == ACT_REAL_EFFICIENCY)||($miActivity == ACT_INC_CONTROL_INDUCTION)){
						// popup_control_revision
						echo "open_task_control_revision(".$miTaskId.",".$miContextId.");";
					}
					elseif($miActivity == ACT_CONTROL_TEST){
						echo "open_task_control_test(".$miTaskId.",".$miContextId.");";
					}
					elseif (($miContextType == CONTEXT_PROCESS_ASSET) || ($miContextType == CONTEXT_RISK_CONTROL)) {
						// popup_complex_task_view
						echo "open_task_view(2,".$miTaskId.",".$miContextId.",".$miContextType.");";
					}
					elseif($miActivity == ACT_NEW_RISK_PARAMETER_APPROVAL) {
						echo "
                open_task_view(4,".$miTaskId.");
				  soPopUpManager.getPopUpById('popup_new_risk_parameter_approval_task').setCloseEvent(function (){document.location.href = '../../tab_main.php';});";

					} else {
						// popup_simple_task_view
						echo "open_task_view(3, ".$miTaskId.",".$miContextId.",".$miContextType.");";
					}
					exit;
				}
			}
			/*
			 * Se o usu�rio estiver sendo representado por algum outro usu�rio,
			 * deve abrir uma popup perguntando se ele gostaria de continuar com
			 * o procurador ou ent�o acabar com o v�nculo de procurador.
			 */
			if ($moUser->hasSolicitor()) {
				echo "isms_open_popup('popup_solicitor_conflict','popup_solicitor_conflict.php','','true',127,445);";
			}
			else {
				if(FWDWebLib::getObject('exec_choose_role')->getValue()=='true'){
					$maUsers = $moUser->getRepresentedUsersList();
				}else{
					$moSession->setSolicitor(0);
					$maUsers = array();
				}
				/*
				 * Se for representante de algum usu�rio, deve abrir uma popup
				 * para que o usu�rio escolha com qual usu�rio deseja logar no
				 * sistema.
				 */
				if (count($maUsers))
				echo "isms_open_popup('popup_choose_role','popup_choose_role.php','','true',172,450);";
				else{
					if(ISMSLib::getGestorPermission())
					echo 'document.location.href = "tab_main.php";';
					elseif(ISMSLib::getAdminPermission())
					echo 'document.location.href = "./packages/admin/tab_main.php";';
					else
					echo "trigger_event('logout_event',3);js_show('warning_acl');";
				}
			}
		}else{ //Quando o usu�rio n�o fez o login corretamente, est� bloqueado, a senha expirou ou n�o tem permiss�o para logar no sistema.
			$miAction = 0; //Por enquanto n�o grava nada no log.
			$moUser = new ISMSUser();
			if ($moUser->fetchByLoginPass($msLogin,$msPassword)) {
				$miErrorId = 0;
				if (!$moSession->attributeExists('loginErrorUserId')) {
					$moSession->addAttribute('loginErrorUserId');
				}
				$moSession->setAttrLoginErrorUserId($moUser->getFieldValue('user_id'));
				if ($moSession->attributeExists('loginErrorId')) {
					$miErrorId = $moSession->getAttrLoginErrorId();
				}
				switch($miErrorId) {
					case ISMSUser::LOGIN_ERROR_USER_BLOCKED:{
						$miAction = ADT_ACTION_BLOCKED_LOGIN;
						$msUserName = $moUser->getFieldValue('user_name');
						$moLog = new ISMSAuditLog();
						$moLog->setFieldValue('log_date', ISMSLib::ISMSTime());
						$moLog->setFieldValue('log_user', $msUserName, false);
						$moLog->setFieldValue('log_action', $miAction);
						$moLog->setFieldValue('log_element', $msUserName, false);
						$msLogLogin = FWDLanguage::getPHPStringValue('ad_login_log_message', '%label_user% <b>%user_name%</b> %action%.');
						$msDescription = str_replace(array('%label_user%', '%user_name%', '%action%'), array($moUser->getLabel(), $msUserName, $moLog->getAction($miAction)), $msLogLogin);
						$moLog->setFieldValue('log_description', $msDescription, false);
						$moLog->insert();
						echo "gebi('user_password').value = '';js_show('warning_blocked');";
						break;
					}
					case ISMSUser::LOGIN_ERROR_CHANGE_PASSWORD:
					case ISMSUser::LOGIN_ERROR_PASSWORD_EXPIRED:{
						echo "isms_open_popup('popup_user_change_password', 'popup_user_change_password.php', '', 'true');";
						return; //Termina a execu��o do script, abrindo popup para troca de senha.
					}
					default:{
						echo "js_show('warning_acl');";
					}
				}
			}else{
				$moUser = new ISMSUser();
				$miAction = 0;
				if ($moUser->fetchByLogin($msLogin)) {
					$moUser->addWrongLogonAttempt();  //Caso negativo, acrescenta 1 no n�mero de tentativas inv�lidas.
					$miAction = ADT_ACTION_BAD_LOGIN;
				}
				else {
					$miAction = ADT_ACTION_LOGIN_ERROR;
				}
				$msLabelUser = FWDLanguage::getPHPStringValue('ad_login_error_user_and_password_do_not_match', ' %user_login% (%user_ip%) ');
				$miIP = 0;
				if(getenv("REMOTE_ADDR")) {
					$miIP = getenv("REMOTE_ADDR");
				}
				$msLabelUser = str_replace(array("%user_login%","%user_ip%"),array($msLogin,$miIP),$msLabelUser);
				// insere na tabela de log
				$moLog = new ISMSAuditLog();
				$moLog->setFieldValue('log_date', ISMSLib::ISMSTime());
				$moLog->setFieldValue('log_user', $msLabelUser, false);
				$moLog->setFieldValue('log_action', $miAction);
				$moLog->setFieldValue('log_element', $msLabelUser, false);

				$msLogLogin = FWDLanguage::getPHPStringValue('ad_login_message', '%label_user% <b>%user_name%</b> %action%.');
				$msDescription = str_replace(array('%label_user%', '%user_name%', '%action%'), array($moUser->getLabel(), $msLabelUser, $moLog->getAction($miAction)), $msLogLogin);
				$moLog->setFieldValue('log_description', $msDescription, false);
				$moLog->insert();

				echo "gebi('user_password').value = '';js_show('warning');";
			}
		}
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));
		$moStartEvent->addAjaxEvent(new LoginEvent("login_event"));
		$moStartEvent->addAjaxEvent(new LogoutEvent("logout_event"));
		$moStartEvent->addAjaxEvent(new ShowBlockedMessageEvent("show_blocked_message"));

	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		global $miLanguage;
		/*somente no saas
		 c�digo para detectar se � o primeiro login no sistema e mostrar a tela de contrato de licen�a
		 */
		$msFocus = '';
		$mbHasCIModule = false;
		$moSaaSConfig = new ISMSSaas();

		if(!$moSaaSConfig->getConfig(SYSTEM_IS_ACTIVE)){
			FWDWebLib::getObject('password_request')->setShouldDraw(false);
			FWDWebLib::getObject('st_login')->setShouldDraw(false);
			FWDWebLib::getObject('vb_login')->setShouldDraw(false);
			FWDWebLib::getObject('icon_iso')->setShouldDraw(false);
		}else{
			/*end somente no saas*/
			$moWebLib = FWDWebLib::getInstance();
			$msFocus = "gebi('user_login').focus();";
			if (isset($_COOKIE['ISMSLOGIN'])) {
				$moCrypt = new FWDCrypt();
				FWDWebLib::getObject('user_login')->setValue($moCrypt->decryptEmbeddedIV($_COOKIE['ISMSLOGIN']));
				FWDWebLib::getObject('remember_controller')->checkItem('login');
				FWDWebLib::getObject('remember_password')->setAttrDisplay('true');
				$msFocus = "gebi('user_password').focus();";
			}
			if (isset($_COOKIE['ISMSPASSWORD'])) {
				FWDWebLib::getObject('user_password')->setValue('PASSWORD');
				FWDWebLib::getObject('remember_controller')->checkItem('password');
			}

			if(!ISMSLib::getConfigById(GENERAL_EMAIL_ENABLED)){ //Se o email estiver desabilitado, n�o pode recuperar a senha.
				FWDWebLib::getObject('password_request')->setShouldDraw(false);
			}

			$moISMSLib = ISMSLib::getInstance();

			$msVersion = $moISMSLib->getISMSVersion();
			$msBuild = $moISMSLib->getISMSBuild();
			$maDate = getdate();

			$moLicense = new ISMSLicense();
			if ($moLicense->isISMS()) {
				$moWebLib->getObject('version_build_copyright_axur')->setValue("Real ISMS - Information Security Management System - Copyright ".$maDate['year']." Realiso Corporation");
			}
			else if ($moLicense->isBCMS()) {
				$moWebLib->getObject('version_build_copyright_axur')->setValue("Real BCMS - Business Continuity Management System - Copyright ".$maDate['year']." Realiso Corporation");
			}
			else if ($moLicense->isEMS()) {
				$moWebLib->getObject('version_build_copyright_axur')->setValue("Real EMS - Environmental Management System - Copyright ".$maDate['year']." Realiso Corporation");
			}
			else if ($moLicense->isOHS()) {
				$moWebLib->getObject('version_build_copyright_axur')->setValue("Real OHS - Occupational Health and Safety Management System - Copyright ".$maDate['year']." Realiso Corporation");
			}
			else if ($moLicense->isSOX()) {
				$moWebLib->getObject('version_build_copyright_axur')->setValue("Real SOX - Sarbanes Oxley Management System - Copyright ".$maDate['year']." Realiso Corporation");
			}
			else if ($moLicense->isPCI()) {
				$moWebLib->getObject('version_build_copyright_axur')->setValue("Real PCI - Payment Card Industry Management System - Copyright ".$maDate['year']." Realiso Corporation");
			}
			else {
				$moWebLib->getObject('version_build_copyright_axur')->setValue("Copyright ".$maDate['year']." Realiso Corporation");
			}

			/*
			 * Popula o select com as linguagens dispon�veis para o sistema
			 */

			$miSelectedLanguage = "";

			if($miLanguage) {
				$miSelectedLanguage = $miLanguage;
			} else {
				if (isset($_COOKIE['ISMSLANGUAGE'])) {
					$miSelectedLanguage = $_COOKIE['ISMSLANGUAGE'];
				}
			}

			$moLanguageSelect = FWDWebLib::getObject('user_language');
			$maAvailableLanguages = FWDLanguage::getLanguages();
			foreach ($maAvailableLanguages as $miKey => $msValue) {
				$moLanguageSelect->setItemValue($miKey, $msValue);

				if($miSelectedLanguage == $miKey) {
					$moLanguageSelect->checkItem($miKey);
				}
			}


			//			$moLanguageSelect = FWDWebLib::getObject('user_language');
			//			$maAvailableLanguages = FWDLanguage::getLanguages();
			//			foreach ($maAvailableLanguages as $miKey => $msValue) {
			//				$moLanguageSelect->setItemValue($miKey, $msValue);
			//			}


			$moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
			if ($moSession->getActivationPeriod()) {
				FWDWebLib::getObject('vb_activation')->setAttrDisplay('true');
			}

			$mbHasCIModule =" var sbHasCIModule=0; ";
			if(FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(INCIDENT_MODE)){
				$mbHasCIModule =" var sbHasCIModule=1; ";
			}
		}

		if(in_array(ISMSSaaS::getConfig(ISMSSaaS::LICENSE_TYPE), ISMSLicense::getTrialLicenseTypes())){
			FWDWebLib::getInstance()->setTracker('
        <script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
        </script>
        <script type="text/javascript">
        _uacct = "UA-252808-13";
        urchinTracker();
        </script>
      ');
		}
		else {
			FWDWebLib::getInstance()->setTracker('
        <script src="https://ssl.google-analytics.com/urchin.js" type="text/javascript">
        </script>
        <script type="text/javascript">
        _uacct = "UA-252808-14";
        urchinTracker();
        </script>
      ');
		}

		// se j� possui um usu�rio com uma sess�o ativa, utiliza o mesmo usu�rio
		$moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
		if ($moSession->getUserId()) {
			if(ISMSLib::getGestorPermission()) {
				echo "<script language='javascript'>document.location.href = 'tab_main.php';</script>";
			}
			elseif(ISMSLib::getAdminPermission()) {
				echo "<script language='javascript'>document.location.href = './packages/admin/tab_main.php';</script>";
			}
			else {
				echo "<script language='javascript'>document.location.href = 'login.php';</script>";
			}
		}

		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));

		?>

<link href="lib/css/nonauto/realiso.css" type="text/css" rel="stylesheet">
<link href="lib/css/nonauto/reveal.css" type="text/css" rel="stylesheet">
<script src="lib/javascript/nonauto/jquery-1.8.3.min.js"  type="text/javascript"></script>
<script src="lib/javascript/nonauto/jquery.reveal.js" type="text/javascript"></script>

<div id="asaas_block_popup" class="reveal-modal"></div>
<div id="asaas_block_popup_payment" class="reveal-modal"></div>

<script language="javascript">
        window.onresize = center_dialog_div;
        center_dialog_div();
        js_show('dialog');
        document.body.className = 'BODYBackground';
        <?
        	$moConfig = new ISMSConfig();

          echo $mbHasCIModule;

		if(!$moSaaSConfig->getConfig(SYSTEM_IS_ACTIVE)){
        	if($moConfig->getConfigFromDB(TRIAL_INSTANCE)){
        		echo "show_asaas_popup();";
        	} else {
        		echo "trigger_event('show_blocked_message',3);";
        	}
        }

        if($moConfig->getConfigFromDB(CLIENT_ID_ASAAS)){
	   		$moISMSASaaS = new ISMSASaaS();
			$planCode = $moISMSASaaS->getPlanCode();
			if($planCode != 'TRIAL' && $moConfig->getConfigFromDB(TRIAL_INSTANCE)){
		        $dataActivation = $moISMSASaaS->getCoverInitialPayments();
		        $statusPayment = $moISMSASaaS->getStatusPayment();
		        $dataNow = time();
		        $secs = $dataNow - strtotime($dataActivation);
		        $days = ceil($secs / 86400);
		        if($days > DEADLINE_RECIVE_PAYMENT and $statusPayment=="WAIT_PAYMENT"){
		        	echo "show_asaas_popup_payment_alert();";
		        }
	    	}
    	}

        ?>
        <?=$msFocus?>
        function execLoginEvent(){
          gebi('exec_choose_role').value='false';
          trigger_event('login_event',3);
        }
        function enter_key_event(e)
        {
          if (!e) e = window.event;
          if(e['keyCode']==13) {

            js_required_check_client("user_login:user_password","trigger_event(\"login_event\",\"3\");","");
          }
        }
        if(gebi('user_password')){
          FWDEventManager.addEvent(gebi('user_password'), 'keydown', enter_key_event);
        }
        if(gebi('user_password')){
          FWDEventManager.addEvent(gebi('user_login'), 'keydown', enter_key_event);
        }

        function refresh_grids() {
          trigger_event("login_event","3");
        }

        function open_task_view(type,task_id,context_id,context_type) {
          switch (type){
            case 2:
              isms_open_popup('popup_complex_task_view', 'popup_complex_task_view.php?task='+task_id, '', 'true', 272, 400);
            break;
            case 3:
              isms_open_popup('popup_simple_task_view', 'popup_simple_task_view.php?task='+task_id, '', 'true', 261, 440);
            break;
            case 4:
              isms_open_popup('popup_new_risk_parameter_approval_task', 'packages/improvement/popup_new_risk_parameter_approval_task.php?task='+task_id, '', 'true', 296, 440);  
            break;            
          }
        }
        function open_task_control_revision(task_id,context_id) {
          if(sbHasCIModule){
             isms_open_popup('popup_control_revision_task', 'packages/risk/popup_control_revision_task.php?task='+task_id+'&control='+context_id, '', 'true', 380, 800);
          }else{
             isms_open_popup('popup_control_revision_task', 'packages/risk/popup_control_revision_task.php?task='+task_id+'&control='+context_id, '', 'true', 380, 390);
          }
        }
        function open_task_control_test(task_id,context_id) {
          isms_open_popup('popup_control_test_task', 'packages/risk/popup_control_test_task.php?task='+task_id+'&control='+context_id, '', 'true', 422, 400);
        }

		function show_asaas_popup(){
			$('#asaas_block_popup').load('asaas_block_popup.php');
			$('#asaas_block_popup').reveal(
				{closeonbackgroundclick: false}
			);
		}

		function show_asaas_popup_payment_alert(){
			$('#asaas_block_popup_payment').load('asaas_block_popup_payment.php');
			$('#asaas_block_popup_payment').reveal(
				{closeonbackgroundclick: false}
			);
		}

      </script>
      
        <?
	}
}
if(isset($_SERVER["HTTP_ACCEPT_LANGUAGE"])){
	$usrLanguages = explode(',', strtoupper($_SERVER["HTTP_ACCEPT_LANGUAGE"]));
	for($x = 0; $x < count($usrLanguages); $x++){
		$usrLanguages[$x] = explode('-', $usrLanguages[$x]);
	}
	
	/* Por enquanto, portugues � o padrao */
	$usrLanguages[0][0]='PT';
}
if (isset($usrLanguages)){
	$maAvailableLanguages = FWDLanguage::getLanguages();
	$selected = false;
	$eng = 0;
	foreach ($maAvailableLanguages as $miKey => $msValue) {
		if (($usrLanguages[0][0] == 'PT') && $msValue == utf8_encode('Portugu�s')){
			FWDLanguage::selectLanguage($miKey);
			$selected = true;
			break;
		}
		elseif(($usrLanguages[0][0] == 'FR' ) && $msValue == utf8_encode('French')){
			FWDLanguage::selectLanguage($miKey);
			$selected = true;
			break;
		}
		elseif(($usrLanguages[0][0] == 'DE' ) && $msValue == utf8_encode('Deutch')){
			FWDLanguage::selectLanguage($miKey);
			$selected = true;
			break;
		}
		elseif($msValue == utf8_encode('English')){
			$eng = $miKey;
		}
	}
	if (!$selected && $eng){
		FWDLanguage::selectLanguage($eng);
	}
}
$miLanguage = FWDWebLib::getPOST('user_language');
if ($miLanguage) {
	$_COOKIE['ISMSLANGUAGE'] = $miLanguage;
	setcookie('ISMSLANGUAGE', $miLanguage, time()+60*60*24*365);
	FWDLanguage::selectLanguage($miLanguage);
} else {
	if(isset($_COOKIE['ISMSLANGUAGE'])){
		FWDLanguage::selectLanguage($_COOKIE['ISMSLANGUAGE']);
	} else {
		$_COOKIE['ISMSLANGUAGE'] = FWDLanguage::getSelectedLanguage();
		setcookie('ISMSLANGUAGE', FWDLanguage::getSelectedLanguage(), time()+60*60*24*365); //1-year cookie
		FWDLanguage::selectLanguage(FWDLanguage::getSelectedLanguage());
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("login.xml");
?>
<!--[if lt IE 7]>
<script defer type='text/javascript' language='javascript'>
  correct_png();
</script>
<![endif]-->
<?
if(isset($_COOKIE['SITELOGIN']) && ($_COOKIE['SITELOGIN'] == true)){
	setCookie('SITELOGIN', false);
	echo '<script> trigger_event("login_event","3"); </script>';
}

?>
