<?php
include_once "include.php";

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $msMessage = FWDLanguage::getPHPStringValue('st_license_file_not_found', "N�o foi poss�vel encontrar o arquivo de licen�a <b>'%license_name%'</b>.");
    $msMessage = str_replace("%license_name%",FWDWebLib::getObject("param_license_name")->getValue(),$msMessage);
    FWDWebLib::getObject("access_denied_message")->setValue($msMessage);
    $msEvent = "soPopUpManager.closePopUp('popup_access_denied');";
    $moEvent = new FWDClientEvent();
    $moEvent->setAttrEvent('onClick');
    $moEvent->setAttrValue($msEvent);
    FWDWebLib::getObject("vb_ok")->addObjFWDEvent($moEvent);
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("access_denied.xml");
?>