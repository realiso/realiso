<?php
  include_once "include.php";
  include_once "ISMS_import.php";

  set_time_limit(600);
 
 
class MigSession extends FWDSession { 
 
  public function __construct($psId){
    parent::__construct($psId);
    
  }
  
  public function login($psUser,$psPassword = ""){
  }
  
}
 
 class DBMigConnection{
	 	private $csSrcDataBase;
	 	
	 	private $csDstDataBase;
 
		public function __construct(){
      $msSrcHostname = FWDWebLib::getObject("src_hostname")->getValue();
      $msSrcDatabase = FWDWebLib::getObject("src_database")->getValue();
      $msSrcUserName = FWDWebLib::getObject("src_username")->getValue();
      $msSrcPassword= FWDWebLib::getObject("src_password")->getValue();
      
      $msDstHostname = FWDWebLib::getObject("dst_hostname")->getValue();
      $msDstDatabase = FWDWebLib::getObject("dst_database")->getValue();
      $msDstUserName = FWDWebLib::getObject("dst_username")->getValue();
      $msDstPassword = FWDWebLib::getObject("dst_password")->getValue();
 			$moSession =FWDWebLib::getInstance()->getSessionById('mig_session_id');
			$maValues = $moSession->getAttrIsmsMigrationValues();
			
    	$this->csSrcDataBase =  new FWDDB(DB_MSSQL, 
    																		$maValues["src_database"], 
    																		$maValues["src_username"], 
    																		$maValues["src_password"], 
    																		$maValues["src_hostname"]);
       
      
      if($this->csSrcDataBase)
      	$this->csSrcDataBase->connect();
		 
    	$this->csDstDataBase =  new FWDDB(DB_MSSQL, 
    																		$maValues["dst_database"], 
    																		$maValues["dst_username"], 
    																		$maValues["dst_password"], 
    																		$maValues["dst_hostname"]);
			if($this->csDstDataBase)    																		
    		$this->csDstDataBase->connect();
		} 
 
		public function getSrcDB(){
			return $this->csSrcDataBase;
		}
		
		public function getDstDB(){
			return $this->csDstDataBase;
		} 		
 }

function makeMigrationTime(){
      $moSession = FWDWebLib::getInstance()->getSessionById('mig_session_id');
			if(!$moSession->attributeExists('mig_time_start')){
				$moSession->addAttribute('mig_time_start');
			}
			$moSession->setAttrMig_time_start(microtime(1))	;
}

function getMigrationTime(){
      $moSession = FWDWebLib::getInstance()->getSessionById('mig_session_id');
			$msReturn = '';
			if($moSession->attributeExists('mig_time_start')){
				$miTimestamp = microtime(1) - $moSession->getAttrMig_time_start();
				
				$msReturn .=' em ' . number_format($miTimestamp,2) . ' segundo(s).';
			}
			return $msReturn;
}
  /**
   * Insere todos os dados na ordem abaixo.
   */
  class Insert extends FWDRunnable {
    
    public function errorSrcDBHandler(){
    	  $msErrorSrc =FWDLanguage::getPHPStringValue('st_source_db_error', 'Erro ao conectar ao BD origem da migra��o. \n\n Verifique se os dados preenchidos est�o corretos!'); 
    		echo "alert('{$msErrorSrc}');gobi('insert').enable();";
        exit();
    }
    
    public function errorDstDBHandler(){
    	  $msErrorSrc =FWDLanguage::getPHPStringValue('st_destination_db_error', 'Erro ao conectar ao BD destino da migra��o. \n\n Verifique se os dados preenchidos est�o corretos!'); 
    		echo "alert('{$msErrorSrc}');gobi('insert').enable();";
        exit();
    }
    
    public function run() {
      
      $msSrcHostname = FWDWebLib::getObject("src_hostname")->getValue();
      $msSrcDatabase = FWDWebLib::getObject("src_database")->getValue();
      $msSrcUserName = FWDWebLib::getObject("src_username")->getValue();
      $msSrcPassword= FWDWebLib::getObject("src_password")->getValue();
      
      $msDstHostname = FWDWebLib::getObject("dst_hostname")->getValue();
      $msDstDatabase = FWDWebLib::getObject("dst_database")->getValue();
      $msDstUserName = FWDWebLib::getObject("dst_username")->getValue();
      $msDstPassword = FWDWebLib::getObject("dst_password")->getValue();
      
      $moSrcDatabase = new FWDDB(DB_MSSQL, $msSrcDatabase, $msSrcUserName, $msSrcPassword, $msSrcHostname);
      $moDstDatabase = new FWDDB(DB_MSSQL, $msDstDatabase, $msDstUserName, $msDstPassword, $msDstHostname);
      FWDWebLib::setConnection($moDstDatabase);
  		
		  //error handler para a cone��o ao banco origem da migra��o     
      set_error_handler(array($this,"errorSrcDBHandler"));
      set_exception_handler(array($this,"errorSrcDBHandler"));    
      $moSrcDatabase->connect();
     
  		//error handler para o banco destino da migra��o
      set_error_handler(array($this,"errorDstDBHandler"));
      set_exception_handler(array($this,"errorDstDBHandler"));    
		  $moDstDatabase->connect();
 
  		//error handler default da FWD   
      set_error_handler(array(FWDWebLib::getInstance(),"FWDErrorHandler"));
      set_exception_handler(array(FWDWebLib::getInstance(),"FWDExceptionHandler"));    
      
      $mfStartTime = microtime(true);
      //deleta das tabelas de parametro do risco , ativo e controle os nomes dos parametros
      $moDeleteDS = new FWDDBDataSet($moDstDatabase);
      $moDeleteDS->setQuery("DELETE FROM rm_parameter_name");
      $moDeleteDS->execute();
      $moDeleteDS->setQuery("DELETE FROM rm_parameter_value_name");
      $moDeleteDS->execute();
      $moDeleteDS->setQuery("DELETE FROM rm_rc_parameter_value_name");
      $moDeleteDS->execute();
      $moDeleteDS->setQuery("DELETE FROM rm_risk_limits");
      $moDeleteDS->execute();
      
      echo "gobi('status').setValue('');";

			$moSession = FWDWebLib::getInstance()->getSessionById('mig_session_id');
      if(!$moSession->attributeExists('IsmsMigrationValues')){
      	$moSession->addAttribute('IsmsMigrationValues');
      }
      if(!$moSession->attributeExists('IsmsTotalTimeMigration')){
      	$moSession->addAttribute('IsmsTotalTimeMigration');
      }
			$moSession->setAttrIsmsTotalTimeMigration(microtime(true));

			$moSession->setAttrIsmsMigrationValues(
			array("src_hostname"=>$msSrcHostname,
				    "src_database"=>$msSrcDatabase,
			      "src_username"=>$msSrcUserName,
			      "src_password"=>$msSrcPassword,
			   		"dst_hostname"=>$msDstHostname,
			   		"dst_database"=>$msDstDatabase,  
			   		"dst_username"=>$msDstUserName,
			   		"dst_password"=>$msDstPassword
			));
						      
echo "
				trigger_event('MigTableProfile',3);
				trigger_event('MigTableUser',3);
				trigger_event('MigTableControl',3);
				trigger_event('MigTableControlEfficiency',3);
				trigger_event('MigTableControlEfficiencyHistory',3);
				trigger_event('MigTableCategory',3);
				trigger_event('MigTableEvent',3);
        trigger_event('MigTableArea',3);
        trigger_event('MigTableProcess',3);
				trigger_event('MigTableAsset',3);
				trigger_event('MigTableParameterValueName',3);
				trigger_event('MigTableParameterName',3);
				trigger_event('MigTableAssetValue',3);
				trigger_event('MigTableAssetAsset',3);
				trigger_event('MigTableRisk',3);
				trigger_event('MigTableRiskValue',3);
				trigger_event('MigTableProcessAsset',3);
				trigger_event('MigTableSectionBestPractice',3);
				trigger_event('MigTableBestPractice',3);
				trigger_event('MigTableControlBestPractice',3);
				trigger_event('MigTableRiskControl',3);
				trigger_event('MigTableRCParameterValueName',3);
				trigger_event('MigTableRiskControlValue',3);
				trigger_event('MigTableConfig',3);
				trigger_event('MigTableRiskLimits',3);
				trigger_event('MigTableTask',3);
				trigger_event('MigTableAlert',3);
				trigger_event('MigTableAuditLog',3);
 				trigger_event('TotalTimeMigration',3);
				trigger_event('enable_view_button',3);
			";
    }
  }

  class ReenableViewButton extends FWDRunnable {
    public function run(){
      FWDWebLib::getInstance()->destroySession('mig_session_id');
      echo "gobi('insert').enable();";
    }
  }
  class MigTableAuditLog extends FWDRunnable {
    public function run(){
      $moSession =FWDWebLib::getInstance()->getSessionById('mig_session_id'); 
      $moConn = new DBMigConnection();
      $moImport = new Import_v2($moConn->getSrcDB(),$moConn->getDstDB(),"isms_audit_log");
      $miCount = $moImport->migrate_log();
      $msTime = getMigrationTime();
      $moSession->deleteAttribute('mig_time_start');
      echo "var msg = gobi('status').getValue(); gobi('status').setValue(msg + 'isms_audit_log: ".$miCount." registro(s) inserido(s)".$msTime."<BR>');";
    }
  }
  class MigTableProfile extends FWDRunnable {
    public function run(){
      $moSession = FWDWebLib::getInstance()->getSessionById('mig_session_id');
      $moConn = new DBMigConnection();
      $moImport = new Import_v2($moConn->getSrcDB(),$moConn->getDstDB(),"isms_profile");      
      $maProfile = $moImport->migrate();
			$moSession->addAttribute('MigTableProfile');
			$moSession->setAttrMigTableProfile($maProfile);			
      echo "var msg = gobi('status').getValue(); gobi('status').setValue(msg + 'isms_profile: ".count($maProfile)." registro(s) inserido(s)".getMigrationTime()."<BR>');";
    }
  }
  class MigTableUser extends FWDRunnable {
    public function run(){
      $moSession =FWDWebLib::getInstance()->getSessionById('mig_session_id'); 
      $moConn = new DBMigConnection();
      $moConn = new DBMigConnection();
      $moImport = new Import_v2($moConn->getSrcDB(),$moConn->getDstDB(),"isms_user",$moSession->getAttrMigTableProfile());
      $maUser = $moImport->migrate();
			$moSession->addAttribute('MigTableUser');
			$moSession->setAttrMigTableUser($maUser);	
			$moSession->deleteAttribute('MigTableProfile');		
      echo "var msg = gobi('status').getValue(); gobi('status').setValue(msg + 'isms_user: ".count($maUser)." registro(s) inserido(s)".getMigrationTime()."<BR>');";
    }
  }

  class MigTableControl extends FWDRunnable {
    public function run(){
      $moSession =FWDWebLib::getInstance()->getSessionById('mig_session_id'); 
      $moConn = new DBMigConnection();
      $moImport = new Import_v2($moConn->getSrcDB(),$moConn->getDstDB(),"rm_control",$moSession->getAttrMigTableUser());
			$maControl = $moImport->migrate();
			$moSession->addAttribute('MigTableControl');
			$moSession->setAttrMigTableControl($maControl);			
      echo "var msg = gobi('status').getValue(); gobi('status').setValue(msg + 'rm_control: ".count($maControl)." registro(s) inserido(s)".getMigrationTime()."<BR>');";
    }
  }
  class MigTableControlEfficiency extends FWDRunnable {
    public function run(){
      $moSession =FWDWebLib::getInstance()->getSessionById('mig_session_id'); 
      $moConn = new DBMigConnection();
      $moImport = new Import_v2($moConn->getSrcDB(),$moConn->getDstDB(),"wkf_control_efficiency",$moSession->getAttrMigTableControl());
      $maWKFControlEfficiency = $moImport->migrate();
      
      echo "var msg = gobi('status').getValue(); gobi('status').setValue(msg + 'wkf_control_efficiency: ".count($maWKFControlEfficiency)." registro(s) inserido(s)".getMigrationTime()."<BR>');";
      $moUpdateValues = new FWDDBDataSet($moConn->getDstDB());
      /* Atualiza valor de efici�ncia dos controles porque o PHP interpreta na leitura do banco, NULL com sendo o valor 0*/
      $moUpdateValues->setQuery("UPDATE wkf_control_efficiency SET nRealEfficiency=NULL WHERE nRealEfficiency=0");
      $moUpdateValues->execute();
      /* Forca execu��o das triggers do controle para atualizar o bIsActive */
      $moUpdateValues->setQuery("
			DECLARE @fiControl numeric
			DECLARE query_control CURSOR
			FOR SELECT fkContext from rm_control
			OPEN query_control
			FETCH NEXT FROM query_control INTO @fiControl WHILE @@FETCH_STATUS = 0
			      BEGIN 
			            UPDATE rm_control SET ddateimplemented=dateadd(second,1,ddateimplemented)
			            WHERE fkContext=@fiControl
			            FETCH NEXT FROM query_control INTO @fiControl
			      END
			CLOSE query_control
			DEALLOCATE query_control
		");
      $moUpdateValues->execute();						
    
    }
  }
  class MigTableControlEfficiencyHistory extends FWDRunnable {
    public function run(){
      $moSession =FWDWebLib::getInstance()->getSessionById('mig_session_id'); 
      $moConn = new DBMigConnection();
      $moImport = new Import_v2($moConn->getSrcDB(),$moConn->getDstDB(),"rm_control_efficiency_history",$moSession->getAttrMigTableControl());
      $maControlEfficiency = $moImport->migrate();
      $moSession->deleteAttribute('MigTableControlEfficiency');
      echo "var msg = gobi('status').getValue(); gobi('status').setValue(msg + 'rm_control_efficiency_history: ".count($maControlEfficiency)." registro(s) inserido(s)".getMigrationTime()."<BR>');";
    }
  }
  class MigTableCategory extends FWDRunnable {
    public function run(){
      $moSession = FWDWebLib::getInstance()->getSessionById('mig_session_id');
      $moConn = new DBMigConnection();
      $moImport = new Import_v2($moConn->getSrcDB(),$moConn->getDstDB(),"rm_category");
      $maCategory = $moImport->migrate();
			$moSession->addAttribute('MigTableCategory');
			$moSession->setAttrMigTableCategory($maCategory);			
      echo "var msg = gobi('status').getValue(); gobi('status').setValue(msg + 'rm_category: ".count($maCategory)." registro(s) inserido(s)".getMigrationTime()."<BR>');";
    }
  }
  class MigTableEvent extends FWDRunnable {
    public function run(){
      $moSession =FWDWebLib::getInstance()->getSessionById('mig_session_id'); 
      $moConn = new DBMigConnection();
      $moImport = new Import_v2($moConn->getSrcDB(),$moConn->getDstDB(),"rm_event",$moSession->getAttrMigTableCategory());
      $maEvent = $moImport->migrate();
      $moSession->addAttribute('MigTableEvent');
			$moSession->setAttrMigTableEvent($maEvent);
      echo "var msg = gobi('status').getValue(); gobi('status').setValue(msg + 'rm_event: ".count($maEvent)." registro(s) inserido(s)".getMigrationTime()."<BR>');";
    }
  }
  class MigTableAsset extends FWDRunnable {
    public function run(){
      $moSession =FWDWebLib::getInstance()->getSessionById('mig_session_id'); 
      $moConn = new DBMigConnection();
      $moImport = new Import_v2($moConn->getSrcDB(),$moConn->getDstDB(),"rm_asset",$moSession->getAttrMigTableCategory(),$moSession->getAttrMigTableUser());
      $maAsset = $moImport->migrate();
			$moSession->addAttribute('MigTableAsset');
			$moSession->setAttrMigTableAsset($maAsset);
			$moSession->deleteAttribute('MigTableCategory');
      echo "var msg = gobi('status').getValue(); gobi('status').setValue(msg + 'rm_asset: ".count($maAsset)." registro(s) inserido(s)".getMigrationTime()."<BR>');";
    }
  }
  class MigTableParameterValueName extends FWDRunnable {
    public function run(){
      $moSession =FWDWebLib::getInstance()->getSessionById('mig_session_id'); 
      $moConn = new DBMigConnection();
      $moImport = new Import_v2($moConn->getSrcDB(),$moConn->getDstDB(),"rm_parameter_value_name");
      $maParameterValueName = $moImport->migrateParameterValueName();
			$moSession->addAttribute('MigTableParameterValueName');
			$moSession->setAttrMigTableParameterValueName($maParameterValueName);			
      echo "var msg = gobi('status').getValue(); gobi('status').setValue(msg + 'rm_parameter_value_name: ".count($maParameterValueName)." registro(s) inserido(s)".getMigrationTime()."<BR>');";
    }
  }  
  class MigTableParameterName extends FWDRunnable {
    public function run(){
      $moSession = FWDWebLib::getInstance()->getSessionById('mig_session_id');
      $moConn = new DBMigConnection();
      $moImport = new Import_v2($moConn->getSrcDB(),$moConn->getDstDB(),"rm_parameter_name");
      $maParameterName = $moImport->migrateParameterName();
			$moSession->addAttribute('MigTableParameterName');
			$moSession->setAttrMigTableParameterName($maParameterName);			
      echo "var msg = gobi('status').getValue(); gobi('status').setValue(msg + 'rm_parameter_name: ".count($maParameterName)." registro(s) inserido(s)".getMigrationTime()."<BR>');";
    }
  }
  class MigTableAssetValue extends FWDRunnable {
    public function run(){//$maAsset,$maParameterName,$maParameterValueName);
      $moSession =FWDWebLib::getInstance()->getSessionById('mig_session_id'); 
      $moConn = new DBMigConnection();
      $moImport = new Import_v2($moConn->getSrcDB(),$moConn->getDstDB(),"rm_asset_value",$moSession->getAttrMigTableAsset(),$moSession->getAttrMigTableParameterName(),$moSession->getAttrMigTableParameterValueName());
      $maAssetValue = $moImport->migrateParameterValueTable();
      echo "var msg = gobi('status').getValue(); gobi('status').setValue(msg + 'rm_asset_value: ".count($maAssetValue)." registro(s) inserido(s)".getMigrationTime()."<BR>');";
    }
  }
  class MigTableAssetAsset extends FWDRunnable {
    public function run(){
      $moSession =FWDWebLib::getInstance()->getSessionById('mig_session_id'); 
      $moConn = new DBMigConnection();
      $moImport = new Import_v2($moConn->getSrcDB(),$moConn->getDstDB(),"rm_asset_asset",$moSession->getAttrMigTableAsset());
      $maAssetAsset = $moImport->migrate();
      echo "var msg = gobi('status').getValue(); gobi('status').setValue(msg + 'rm_asset_asset: ".count($maAssetAsset)." registro(s) inserido(s)".getMigrationTime()."<BR>');";
    }
  }
  class MigTableArea extends FWDRunnable {
    public function run(){
      $moSession =FWDWebLib::getInstance()->getSessionById('mig_session_id'); 
      $moConn = new DBMigConnection();
      $moImport = new Import_v2($moConn->getSrcDB(),$moConn->getDstDB(),"rm_area",$moSession->getAttrMigTableUser());
      $maArea = $moImport->migrate();
			$moSession->addAttribute('MigTableArea');
			$moSession->setAttrMigTableArea($maArea);			
      echo "var msg = gobi('status').getValue(); gobi('status').setValue(msg + 'rm_area: ".count($maArea)." registro(s) inserido(s)".getMigrationTime()."<BR>');";
    }
  }
  class MigTableRisk extends FWDRunnable {
    public function run(){
      $moSession =FWDWebLib::getInstance()->getSessionById('mig_session_id'); 
      $moConn = new DBMigConnection();
      $moImport = new Import_v2($moConn->getSrcDB(),$moConn->getDstDB(),"rm_risk",$moSession->getAttrMigTableEvent(),$moSession->getAttrMigTableAsset(),$moSession->getAttrMigTableUser());
      $maRisk = $moImport->migrate();
			$moSession->addAttribute('MigTableRisk');
			$moSession->setAttrMigTableRisk($maRisk);			
      $moSession->deleteAttribute('MigTableEvent');
      echo "var msg = gobi('status').getValue(); gobi('status').setValue(msg + 'rm_risk: ".count($maRisk)." registro(s) inserido(s)".getMigrationTime()."<BR>');";
    }
  }
  class MigTableRiskValue extends FWDRunnable {
    public function run(){
      $moSession =FWDWebLib::getInstance()->getSessionById('mig_session_id'); 
      $moConn = new DBMigConnection();
      $moImport = new Import_v2($moConn->getSrcDB(),$moConn->getDstDB(),"rm_risk_value",$moSession->getAttrMigTableRisk(),$moSession->getAttrMigTableParameterName(),$moSession->getAttrMigTableParameterValueName());
      $maRiskValue = $moImport->migrateParameterValueTable();
			$moSession->deleteAttribute('MigTableParameterValueName');
      echo "var msg = gobi('status').getValue(); gobi('status').setValue(msg + 'rm_risk_value: ".count($maRiskValue)." registro(s) inserido(s)".getMigrationTime()."<BR>');";
    }
  }
  class MigTableProcess extends FWDRunnable {
    public function run(){
      $moSession =FWDWebLib::getInstance()->getSessionById('mig_session_id'); 
      $moConn = new DBMigConnection();
      $moImport = new Import_v2($moConn->getSrcDB(),$moConn->getDstDB(),"rm_process",$moSession->getAttrMigTableArea(),$moSession->getAttrMigTableUser());
      $maProcess = $moImport->migrate();
			$moSession->addAttribute('MigTableProcess');
			$moSession->setAttrMigTableProcess($maProcess);			
      echo "var msg = gobi('status').getValue(); gobi('status').setValue(msg + 'rm_process: ".count($maProcess)." registro(s) inserido(s)".getMigrationTime()."<BR>');";
    }
  }
  class MigTableProcessAsset extends FWDRunnable {
    public function run(){
      $moSession = FWDWebLib::getInstance()->getSessionById('mig_session_id'); 
      $moConn = new DBMigConnection();
      $moImport = new Import_v2($moConn->getSrcDB(),$moConn->getDstDB(),"rm_process_asset",$moSession->getAttrMigTableProcess(),$moSession->getAttrMigTableAsset(),$moSession->getAttrMigTableUser());
      $maProcessAsset = $moImport->migrate();
			$moSession->addAttribute('MigTableProcessAsset');
			$moSession->setAttrMigTableProcessAsset($maProcessAsset);			
      echo "var msg = gobi('status').getValue(); gobi('status').setValue(msg + 'rm_process_asset: ".count($maProcessAsset)." registro(s) inserido(s)".getMigrationTime()."<BR>');";
    }
  }
  class MigTableSectionBestPractice extends FWDRunnable {
    public function run(){
      $moSession = FWDWebLib::getInstance()->getSessionById('mig_session_id');
      $moConn = new DBMigConnection();
      $moImport = new Import_v2($moConn->getSrcDB(),$moConn->getDstDB(),"rm_section_best_practice");
      $maSectionBestPractice = $moImport->migrate();
			$moSession->addAttribute('MigTableSectionBestPractice');
			$moSession->setAttrMigTableSectionBestPractice($maSectionBestPractice);			
      echo "var msg = gobi('status').getValue(); gobi('status').setValue(msg + 'rm_section_best_practice: ".count($maSectionBestPractice)." registro(s) inserido(s)".getMigrationTime()."<BR>');";
    }
   }
  class MigTableBestPractice extends FWDRunnable {
    public function run(){
      $moSession =FWDWebLib::getInstance()->getSessionById('mig_session_id'); 
      $moConn = new DBMigConnection();
      $moImport = new Import_v2($moConn->getSrcDB(),$moConn->getDstDB(),"rm_best_practice",$moSession->getAttrMigTableSectionBestPractice());
      $maBestPractice = $moImport->migrate();
			$moSession->addAttribute('MigTableBestPractice');
			$moSession->setAttrMigTableBestPractice($maBestPractice);			
			$moSession->deleteAttribute('MigTableSectionBestPractice');
      echo "var msg = gobi('status').getValue(); gobi('status').setValue(msg + 'rm_best_practice: ".count($maBestPractice)." registro(s) inserido(s)".getMigrationTime()."<BR>');";
    }
  }
  class MigTableControlBestPractice extends FWDRunnable {
    public function run(){
      $moSession =FWDWebLib::getInstance()->getSessionById('mig_session_id'); 
      $moConn = new DBMigConnection();
      $moImport = new Import_v2($moConn->getSrcDB(),$moConn->getDstDB(),"rm_control_best_practice",$moSession->getAttrMigTableBestPractice(),$moSession->getAttrMigTableControl());
			$moSession->deleteAttribute('MigTableBestPractice');
      $maControlBestPractice = $moImport->migrate();
      echo "var msg = gobi('status').getValue(); gobi('status').setValue(msg + 'rm_control_best_practice: ".count($maControlBestPractice)." registro(s) inserido(s)".getMigrationTime()."<BR>');";
    }
  }
  class MigTableRiskControl extends FWDRunnable {
    public function run(){
      $moSession =FWDWebLib::getInstance()->getSessionById('mig_session_id'); 
      $moConn = new DBMigConnection();
      $moImport = new Import_v2($moConn->getSrcDB(),$moConn->getDstDB(),"rm_risk_control",$moSession->getAttrMigTableRisk(),$moSession->getAttrMigTableControl(),$moSession->getAttrMigTableUser());
      $maRiskControl = $moImport->migrate();
			$moSession->addAttribute('MigTableRiskControl');
			$moSession->setAttrMigTableRiskControl($maRiskControl);
      echo "var msg = gobi('status').getValue(); gobi('status').setValue(msg + 'rm_risk_control: ".count($maRiskControl)." registro(s) inserido(s)".getMigrationTime()."<BR>');";
    }
  }
  class MigTableRCParameterValueName extends FWDRunnable {
    public function run(){
      $moSession = FWDWebLib::getInstance()->getSessionById('mig_session_id');
      $moConn = new DBMigConnection();
      $moImport = new Import_v2($moConn->getSrcDB(),$moConn->getDstDB(),"rm_rc_parameter_value_name");
      $maRCParameterValueName = $moImport->migrateRCParameterValueName();
      
			$moSession->addAttribute('MigTableRCParameterValueName');
			$moSession->setAttrMigTableRCParameterValueName($maRCParameterValueName);			
      
      echo "var msg = gobi('status').getValue(); gobi('status').setValue(msg + 'rm_rc_parameter_value_name: ".count($maRCParameterValueName)." registro(s) inserido(s)".getMigrationTime()."<BR>');";
    }
  }
  class MigTableRiskControlValue extends FWDRunnable {
    public function run(){
      $moSession =FWDWebLib::getInstance()->getSessionById('mig_session_id'); 
      $moConn = new DBMigConnection();
      $moImport = new Import_v2($moConn->getSrcDB(),$moConn->getDstDB(),"rm_risk_control_value",$moSession->getAttrMigTableRiskControl(),$moSession->getAttrMigTableParameterName(),$moSession->getAttrMigTableRCParameterValueName());
      $maRiskControlValue = $moImport->migrateParameterValueTable();
      $moSession->deleteAttribute('MigTableParameterName');
      $moSession->deleteAttribute('MigTableRCParameterValueName');
      echo "var msg = gobi('status').getValue(); gobi('status').setValue(msg + 'rm_risk_control_value: ".count($maRiskControlValue)." registro(s) inserido(s)".getMigrationTime()."<BR>');";
    }
  }
  class MigTableConfig extends FWDRunnable {
    public function run(){
      $moSession =FWDWebLib::getInstance()->getSessionById('mig_session_id'); 
      $moConn = new DBMigConnection();
      $moImport = new Import_v2($moConn->getSrcDB(),$moConn->getDstDB(),"isms_config",$moSession->getAttrMigTableUser());
      $maConfig = $moImport->migrateConfiguration();
      echo "var msg = gobi('status').getValue(); gobi('status').setValue(msg + 'isms_config: ".count($maConfig)." registro(s) inserido(s)".getMigrationTime()."<BR>');";
    }
  }
  class MigTableRiskLimits extends FWDRunnable {
    public function run(){
      $moSession = FWDWebLib::getInstance()->getSessionById('mig_session_id');
      $moConn = new DBMigConnection();
      $moImport = new Import_v2($moConn->getSrcDB(),$moConn->getDstDB(),"rm_risk_limits");
      $maRiskLimits = $moImport->migrate();
			$moSession->addAttribute('MigTableRiskLimits');
			$moSession->setAttrMigTableRiskLimits($maRiskLimits);			
      echo "var msg = gobi('status').getValue(); gobi('status').setValue(msg + 'rm_risk_limits: ".count($maRiskLimits)." registro(s) inserido(s)".getMigrationTime()."<BR>');";
    }
  }
  class MigTableTask extends FWDRunnable {
    public function run(){
      $moSession =FWDWebLib::getInstance()->getSessionById('mig_session_id'); 
      $moConn = new DBMigConnection();
      $moImport = new Import_v2($moConn->getSrcDB(),$moConn->getDstDB(),"wkf_task",
                                 $moSession->getAttrMigTableArea(),
                                 $moSession->getAttrMigTableProcess(),
                                 $moSession->getAttrMigTableAsset(),
                                 $moSession->getAttrMigTableRisk(),
                                 $moSession->getAttrMigTableControl(),
                                 $moSession->getAttrMigTableProcessAsset(),	
                                 $moSession->getAttrMigTableRiskControl(),
                                 $moSession->getAttrMigTableRiskLimits(),
                                 $moSession->getAttrMigTableUser()	
                              );
      $maWKFTask = $moImport->migrate();
      $moSession->addAttribute('MigTableTask');
			$moSession->setAttrMigTableTask($maWKFTask);			
			$moSession->deleteAttribute('MigTableArea');
			$moSession->deleteAttribute('MigTableProcess');
			$moSession->deleteAttribute('MigTableAsset');
			$moSession->deleteAttribute('MigTableRisk');
			$moSession->deleteAttribute('MigTableControl');
			$moSession->deleteAttribute('MigTableProcessAsset');
			$moSession->deleteAttribute('MigTableRiskControl');
			$moSession->deleteAttribute('MigTableRiskLimits');
      echo "var msg = gobi('status').getValue(); gobi('status').setValue(msg + 'wkf_task: ".count($maWKFTask)." registro(s) inserido(s)".getMigrationTime()."<BR>');";
    }
  }
  class MigTableAlert extends FWDRunnable {
    public function run(){
      $moSession =FWDWebLib::getInstance()->getSessionById('mig_session_id'); 
      $moConn = new DBMigConnection();
      $moImport = new Import_v2($moConn->getSrcDB(),$moConn->getDstDB(),"wkf_alert",$moSession->getAttrMigTableTask(),$moSession->getAttrMigTableUser());
      $maWKFAlert = $moImport->migrate();
      $moSession->deleteAttribute('MigTableTask');
			$moSession->deleteAttribute('MigTableUser');
      echo "var msg = gobi('status').getValue(); gobi('status').setValue(msg + 'wkf_alert: ".count($maWKFAlert)." registro(s) inserido(s)".getMigrationTime()."<BR>');";
    }
  }
  class MigTotalTime extends FWDRunnable {
    public function run(){
      $moSession =FWDWebLib::getInstance()->getSessionById('mig_session_id'); 
			$miTime = microtime(true) - $moSession->getAttrIsmsTotalTimeMigration();
      $moSession->deleteAttribute('IsmsTotalTimeMigration');
			$msReturn = number_format($miTime,2) . ' segundo(s).';
      echo "var msg = gobi('status').getValue(); gobi('status').setValue(msg + '<BR>Tempo total estimado da migra��o: ".$msReturn."<BR>');";
    }
  }
  class ScreenBeforeEvent extends FWDRunnable {
    public function run(){
      $moStartEvent = FWDStartEvent::getInstance();
      $moStartEvent->setScreenEvent(new ScreenEvent(""));
      $moStartEvent->addAjaxEvent(new Insert("insert"));
	    
	    FWDWebLib::getInstance()->getSession(new MigSession('mig_session_id'));
	    
      $moSession = FWDWebLib::getInstance()->getSessionById('mig_session_id');
      $moSession->commit();
      if($moSession->attributeExists("IsmsMigrationValues")){ 
        $moConn = new DBMigConnection();
	      FWDWebLib::setConnection($moConn->getDstDB());
      }
			//registra o tempo inicial do evento que est� sendo executado
			makeMigrationTime();
			
			$moStartEvent->addAjaxEvent(new MigTableAuditLog("MigTableAuditLog"));
			$moStartEvent->addAjaxEvent(new MigTableProfile("MigTableProfile"));
			$moStartEvent->addAjaxEvent(new MigTableUser("MigTableUser"));
			$moStartEvent->addAjaxEvent(new MigTableControl("MigTableControl"));
			$moStartEvent->addAjaxEvent(new MigTableControlEfficiency("MigTableControlEfficiency"));
			$moStartEvent->addAjaxEvent(new MigTableControlEfficiencyHistory("MigTableControlEfficiencyHistory"));
			$moStartEvent->addAjaxEvent(new MigTableCategory("MigTableCategory"));
			$moStartEvent->addAjaxEvent(new MigTableEvent("MigTableEvent"));
			$moStartEvent->addAjaxEvent(new MigTableAsset("MigTableAsset"));
			$moStartEvent->addAjaxEvent(new MigTableAssetValue("MigTableAssetValue"));
			$moStartEvent->addAjaxEvent(new MigTableAssetAsset("MigTableAssetAsset"));
			$moStartEvent->addAjaxEvent(new MigTableArea("MigTableArea"));
			$moStartEvent->addAjaxEvent(new MigTableRisk("MigTableRisk"));
			$moStartEvent->addAjaxEvent(new MigTableParameterValueName("MigTableParameterValueName"));
			$moStartEvent->addAjaxEvent(new MigTableParameterName("MigTableParameterName"));
			$moStartEvent->addAjaxEvent(new MigTableRiskValue("MigTableRiskValue"));
			$moStartEvent->addAjaxEvent(new MigTableProcess("MigTableProcess"));
			$moStartEvent->addAjaxEvent(new MigTableProcessAsset("MigTableProcessAsset"));
			$moStartEvent->addAjaxEvent(new MigTableSectionBestPractice("MigTableSectionBestPractice"));
			$moStartEvent->addAjaxEvent(new MigTableBestPractice("MigTableBestPractice"));
			$moStartEvent->addAjaxEvent(new MigTableControlBestPractice("MigTableControlBestPractice"));
			$moStartEvent->addAjaxEvent(new MigTableRiskControl("MigTableRiskControl"));
			$moStartEvent->addAjaxEvent(new MigTableRCParameterValueName("MigTableRCParameterValueName"));
			$moStartEvent->addAjaxEvent(new MigTableRiskControlValue("MigTableRiskControlValue"));
			$moStartEvent->addAjaxEvent(new MigTableConfig("MigTableConfig"));
			$moStartEvent->addAjaxEvent(new MigTableRiskLimits("MigTableRiskLimits"));
			$moStartEvent->addAjaxEvent(new MigTableTask("MigTableTask"));
			$moStartEvent->addAjaxEvent(new MigTableAlert("MigTableAlert"));
			$moStartEvent->addAjaxEvent(new MigTotalTime("TotalTimeMigration"));
			$moStartEvent->addAjaxEvent(new ReenableViewButton("enable_view_button"));
    }
  }
  
  class ScreenEvent extends FWDRunnable {
    public function run(){
      FWDWebLib::getInstance()->destroySession('mig_session_id');
      FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    }
  }
  
  FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
  FWDWebLib::getInstance()->setWindowTitle("ISMS - INFORMATION SECURITY MANAGEMENT SOLUTION");
  FWDWebLib::getInstance()->xml_load("ISMS_migration.xml");  

?>