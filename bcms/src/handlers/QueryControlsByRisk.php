<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryControlsByRisk.
 *
 * <p>Consulta para buscar todos os controles que mitigam um determinado risco.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryControlsByRisk extends FWDDBQueryHandler {
  
  protected $ciRiskId = 0;  
  protected $caControls = array();  
  
  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField('fkControl','control_id',DB_NUMBER));
  }
  
  public function setRisk($piRiskId) {
    $this->ciRiskId = $piRiskId;
  }
  
  public function makeQuery() {
    $this->csSQL = "SELECT fkControl as control_id
										FROM view_rm_risk_control_active
										WHERE fkRisk = {$this->ciRiskId}";
  }
  
  public function executeQuery() {
    parent::executeQuery();
    while ($this->coDataSet->fetch()) {
      $this->caControls[] = $this->coDataSet->getFieldByAlias("control_id")->getValue();
    }
  }
  
  /**
   * Retorna os controles.
   * 
   * <p>M�todo para retornar os controles.</p>
   * @access public 
   * @return array Array de ids de controles
   */ 
  public function getControls() {  	
    return $this->caControls;
  }
}
?>