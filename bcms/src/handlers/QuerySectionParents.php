<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QuerySectionParents.
 *
 * <p>Consulta para buscar as se��es pai[,av�[,bisav�[,...]]] de uma se��o.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QuerySectionParents extends FWDDBQueryHandler {
  
  protected $ciSectionId = 0;
  
  protected $caParentSections = array();
  
  protected $cbExecByTrash = false;
  
  public function __construct($poDB,$pbExecByTrash = false) {
    parent::__construct($poDB);
    $this->cbExecByTrash = $pbExecByTrash;
    
    $this->coDataSet->addFWDDBField(new FWDDBField('pkSection','section_id',DB_NUMBER));
  }
  
  public function setSection($piSectionId) {
    $this->ciSectionId = $piSectionId;
  }
  
  public function makeQuery() {
  	$msExecTrash = '';
    if($this->cbExecByTrash)
	     $msExecTrash = '_trash';
  	
    $this->csSQL = "SELECT pkSection as section_id from ".FWDWebLib::getFunctionCall("get_section_parents{$msExecTrash}({$this->ciSectionId})");
  }
  
  public function executeQuery() {
    parent::executeQuery();
    while ($this->coDataSet->fetch()) {
      $this->caParentSections[] = $this->coDataSet->getFieldByAlias("section_id")->getValue();
    }
  }
  
  /**
   * Retorna as se��es pai de uma se��o.
   * 
   * <p>M�todo para retornar as se��es pai de uma se��o.</p>
   * @access public 
   * @return array Array de ids das se��es
   */ 
  public function getParentSections() {
    return $this->caParentSections;
  }
}
?>