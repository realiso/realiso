<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryAreaParents.
 *
 * <p>Consulta para buscar as �reas pai[,av�[,bisav�[,...]]] de uma �rea.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryControlCostNames extends FWDDBQueryHandler {
  
  protected $caCostNames = array();
  
  public function __construct($poDB) {
    parent::__construct($poDB); 
    
    $this->coDataSet->addFWDDBField(new FWDDBField('','cost_name',DB_NUMBER));
  }

  public function makeQuery() {
    $this->csSQL = "SELECT svalue as cost_name FROM isms_config WHERE pkConfig IN (201,202,203,204,205)";
  }
  
  public function executeQuery() {
    parent::executeQuery();
    while ($this->coDataSet->fetch()) {
      $this->caCostNames[] = $this->coDataSet->getFieldByAlias("cost_name")->getValue();
    }
  }
  
  /**
   * Retorna as �reas pai de uma �rea.
   * 
   * <p>M�todo para retornar as �reas pai de uma �rea.</p>
   * @access public 
   * @return array Array de ids das �reas
   */ 
  public function getCostNames() {
  	$this->makeQuery();
  	$this->executeQuery();
    return $this->caCostNames;
  }
}
?>