<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryUpdateControlsIsActive.
 *
 * <p>Query para for�ar um update do isActive dos controles com implementa��o
 * ou tarefas pendentes.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryUpdateControlsIsActive extends FWDDBQueryHandler {

  public function makeQuery(){
    $this->csSQL = "UPDATE rm_control SET bisactive = 0 WHERE fkcontext IN (
                      SELECT DISTINCT c.fkcontext
                      FROM
                        rm_control c
                        LEFT JOIN wkf_task_schedule ts ON (ts.fkcontext = c.fkcontext)
                        LEFT JOIN wkf_task t ON (t.nactivity = ts.nactivity AND t.fkcontext = ts.fkcontext AND t.bvisible = 1)
                      WHERE
                        ddateimplemented IS NULL OR t.fkcontext IS NOT NULL
                    )";
  }
  
  public static function updateControls(){
    $moQuery = new QueryUpdateControlsIsActive();
    $moQuery->makeQuery();
    $moQuery->executeQuery();
  }

}

?>