<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryUserIsDeletable.
 *
 * <p>Consulta para indicar se um usu�rio � delet�vel ou n�o.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryUserIsDeletable extends FWDDBQueryHandler {

  private $ciUserId = 0;
  
  private $caContexts = array();
  
  private $cbIsDeletable = false;
  
  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField("ordem","ordem", DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("count","count", DB_NUMBER));
 }

  public function setUserId($psUserId) {
    $this->ciUserId = $psUserId;
  }

  public function makeQuery() {
    $this->csSQL = "
SELECT 'control' as ordem, count(*) as count
FROM rm_control c
JOIN isms_context ctx ON (c.fkContext = ctx.pkContext AND ctx.nState != ".CONTEXT_STATE_DELETED.")
WHERE c.fkResponsible = ".$this->ciUserId."

UNION 

SELECT 'solicitor' as ordem, count(*) as count
FROM isms_solicitor
WHERE fkSolicitor =  ".$this->ciUserId."

UNION

SELECT 'document' as ordem, count(*) as count
FROM pm_document d
JOIN isms_context ctx ON (d.fkContext = ctx.pkContext AND ctx.nState != ".CONTEXT_STATE_DELETED.")
LEFT JOIN view_pm_doc_approvers_active a ON (d.fkContext = a.fkDocument)
WHERE d.fkAuthor =  ".$this->ciUserId." OR d.fkMainApprover =  ".$this->ciUserId." OR a.fkUser = ".$this->ciUserId."

UNION

SELECT 'register' as ordem, count(*) as count
FROM pm_register r
JOIN isms_context ctx ON (r.fkContext = ctx.pkContext AND ctx.nState != ".CONTEXT_STATE_DELETED.")
WHERE r.fkResponsible =  ".$this->ciUserId."

UNION

SELECT 'action_plan' as ordem, count(*) as count
FROM ci_action_plan ap
JOIN isms_context ctx ON (ap.fkContext = ctx.pkContext AND ctx.nState != ".CONTEXT_STATE_DELETED.")
WHERE ap.fkResponsible =  ".$this->ciUserId."

UNION

SELECT 'incident' as ordem, count(*) as count
FROM view_ci_incident_active i
WHERE 
   i.fkResponsible =  ".$this->ciUserId."

UNION

SELECT 'nc' as ordem, count(*) as count
FROM view_ci_nc_active nc
WHERE 
   nc.fkResponsible =  ".$this->ciUserId."

";
  }

  public function executeQuery() {
    parent::executeQuery();
    
    $this->caContexts = array();
    while ($this->coDataSet->fetch()) {
      $this->caContexts[$this->coDataSet->getFieldByAlias("ordem")->getValue()] = $this->coDataSet->getFieldByAlias("count")->getValue();
    }
    $maSpecialUsers = array();
    $maSpacialUsersAux = ISMSLib::getSpecialUsers();
    foreach($maSpacialUsersAux as $miKey=>$miValue){
      $maSpecialUsers[] = ISMSLib::getConfigById($miKey);
    }
    
    if ( array_sum($this->caContexts) > 0 || in_array($this->ciUserId,$maSpecialUsers) )
      $this->cbIsDeletable = false;
    else
      $this->cbIsDeletable = true;
  }

  public function getIsDeletable() {
     return $this->cbIsDeletable;
  }
}
?>