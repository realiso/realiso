<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryCategoryParentsEvents.
 *
 * <p>Consulta para buscar os eventos de uma categoria e de suas categorias
 * pais ([pai [,avo [,bisavo...]]]).</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryThreatsAssociatedByPlace extends FWDDBQueryHandler {
  
  protected $ciPlaceId = 0;
  
  protected $caThreats = array();
  
  public function __construct($poDB) {
    parent::__construct($poDB);
  
    $this->coDataSet->addFWDDBField(new FWDDBField('fkContext'   	  ,'place_threat_id',	DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("fkplace"	      ,"place_id",			DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("fkthreat" 	      ,"threat_id",			DB_NUMBER));
    
  }
  
  public function setPlace($piPlaceId) {
    $this->ciPlaceId = $piPlaceId;
  }
    
  public function makeQuery() {
    $this->csSQL = "
	SELECT t.fkContext as place_threat_id, t.fkplace as place_id, t.fkthreat as threat_id
	FROM cm_place_threat t 
	JOIN isms_context c ON (c.pkcontext = t.fkcontext AND c.nState != " . CONTEXT_STATE_DELETED . ")  
	WHERE fkplace=$this->ciPlaceId";
  }
    
  
  public function executeQuery() {
    parent::executeQuery();
    while ($this->coDataSet->fetch()) {
      $miPlaceThreatId = $this->coDataSet->getFieldByAlias("place_threat_id")->getValue();
      $miPlaceId = $this->coDataSet->getFieldByAlias("place_id")->getValue();
      $miThreatId = $this->coDataSet->getFieldByAlias("threat_id")->getValue();
      $this->caThreats[$miPlaceThreatId] = array('place_id'=>$miPlaceId,'threat_id'=>$miThreatId);
    }
  }
  
  /**
   * Retorna os eventos de uma categoria (e de suas sub-categorias, se existirem).
   * 
   * <p>M�todo para retornar os eventos de uma categoria (e de suas sub-categorias,
   * se existirem).</p>
   * @access public 
   * @return array Array de ids de eventos
   */ 
  public function getThreats() {
    return $this->caThreats;
  }
}
?>
