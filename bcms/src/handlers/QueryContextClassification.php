<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryContextClassification.
 *
 * <p>Consulta para buscar as classificações de contextos do ISMS.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryContextClassification extends FWDDBQueryHandler {
  
  protected $caClassificationNames = array();
  
  protected $caClassificationIds = array();
  
  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField("pkClassification",     "classif_id",           DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("nContextType",         "classif_context_type", DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("sName",                "classif_name",         DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("nClassificationType",  "classif_type",         DB_NUMBER));
 }
  
  public function makeQuery() {
    $this->csSQL = "SELECT
                      pkClassification as classif_id,
                      nContextType as classif_context_type,
                      sName as classif_name,
                      nClassificationType as classif_type
                    FROM isms_context_classification
                    WHERE nClassificationType!=".CONTEXT_CLASSIFICATION_FINANCIAL_IMPACT;
  }
  
  public function executeQuery() {
    parent::executeQuery();
    $miAreaTypeCount = 1;
    $miAreaPrioCount = 1;
    $miProcessTypeCount = 1;
    $miProcessPrioCount = 1;
    $miRiskTypeCount = 1;
    $miEventTypeCount = 1;
    $miControlTypeCount = 1;
    $miDocumentTypeCount = 1;
    $miRegisterTypeCount = 1;
    $miPlaceTypeCount = 1;
    $miThreatTypeCount = 1;
    $this->caClassificationNames = array();
    $this->caClassificationIds = array();
    
    /*
     * Verifica se possui o módulo de documentação para pegar valores desnecessários
     */
    $mbHasPMModule = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(POLICY_MODE);
    
    while ($this->coDataSet->fetch()) {
      $miId = $this->coDataSet->getFieldByAlias("classif_id")->getValue();
      $miContextType = $this->coDataSet->getFieldByAlias("classif_context_type")->getValue();
      $msName = $this->coDataSet->getFieldByAlias("classif_name")->getValue();
      $miType = $this->coDataSet->getFieldByAlias("classif_type")->getValue();
      
      switch ($miType) {
        case CONTEXT_CLASSIFICATION_TYPE:{
          switch ($miContextType) {
            case CONTEXT_AREA     : $msIndex = "cl_type_area".$miAreaTypeCount++; break;
            case CONTEXT_PROCESS  : $msIndex = "cl_type_process".$miProcessTypeCount++; break;
            case CONTEXT_RISK     : $msIndex = "cl_type_risk".$miRiskTypeCount++; break;
            case CONTEXT_EVENT    : $msIndex = "cl_type_event".$miEventTypeCount++; break;
            case CONTEXT_CONTROL  : $msIndex = "cl_type_control".$miControlTypeCount++; break;
            case CONTEXT_DOCUMENT : if ($mbHasPMModule) $msIndex = "cl_type_document".$miDocumentTypeCount++; break;
            case CONTEXT_REGISTER : if ($mbHasPMModule) $msIndex = "cl_type_register".$miRegisterTypeCount++; break;
            case CONTEXT_CM_PLACE : $msIndex = "cl_type_place".$miPlaceTypeCount++; break;
            case CONTEXT_CM_THREAT : $msIndex = "cl_type_threat".$miThreatTypeCount++; break;
            default : trigger_error("Invalid Context Classification Context Type: $miContextType",E_USER_ERROR); break;
          }
          break;
        }
        case CONTEXT_CLASSIFICATION_PRIORITY:{
          switch ($miContextType) {
            case CONTEXT_AREA     : $msIndex = "cl_prio_area".$miAreaPrioCount++; break;
            case CONTEXT_PROCESS  : $msIndex = "cl_prio_process".$miProcessPrioCount++; break;
            default : trigger_error("Invalid Context Classification Context Type: $miContextType",E_USER_ERROR); break;
          }
          break;
        }
        default:{
          trigger_error("Invalid Context Classification Type: $miType",E_USER_ERROR); break;
        }
      }
      
      $this->caClassificationNames[$msIndex] = $msName;
      $this->caClassificationIds[$msIndex] = $miId;
    }
  }
  
  /**
   * Retorna os nomes das classificações, indexadas pelo pelo nome do campo.
   * 
   * <p>Método para retornar os nomes das classificações, indexadas pelo pelo
   * nome do campo (ex: 'cl_type_process2', 'cl_prio_area5').</p>
   * @access public 
   * @return array Array de nomes de classificação
   */ 
  public function getClassificationNames() {
    return $this->caClassificationNames;
  }
  
  /**
   * Retorna os ids das classificações, indexadas pelo pelo nome do campo.
   * 
   * <p>Método para retornar os id das classificações, indexadas pelo pelo
   * nome do campo (ex: 'cl_type_process2', 'cl_prio_area5').</p>
   * @access public 
   * @return array Array de ids de classificação
   */ 
  public function getClassificationIds() {
    return $this->caClassificationIds;
  }
}
?>