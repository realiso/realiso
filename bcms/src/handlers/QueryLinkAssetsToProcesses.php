<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryProcessActivitiesAsset
 *
 * <p></p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryLinkAssetsToProcesses extends FWDDBQueryHandler {

  public $result;

  public function __construct($poDB){
    parent::__construct($poDB);

    $this->coDataSet->addFWDDBField(new FWDDBField('p.fkcontext',               'process_id', DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('paa.fkasset',               'asset_id', DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('paa.fkimportance',          'asset_importance', DB_NUMBER));
  }

  public function makeQuery(){
    $this->csSQL ="
SELECT 
distinct p.fkcontext AS process_id, 
paa.fkasset AS asset_id, 
max(paa.fkimportance) AS asset_importance 
FROM rm_process p
JOIN cm_process_activity pac ON p.fkcontext = pac.fkprocess
JOIN cm_process_activity_asset paa ON pac.fkcontext = paa.fkactivity
GROUP BY p.fkcontext, paa.fkasset
";
  }

  public function executeQuery() {
    $this->makeQuery();
    parent::executeQuery();

    while ($this->coDataSet->fetch()){

        $processId = $this->coDataSet->getFieldByAlias("process_id")->getValue();
        $assetId = $this->coDataSet->getFieldByAlias("asset_id")->getValue();
        $assetImportance = $this->coDataSet->getFieldByAlias("asset_importance")->getValue();


        $moProcessAsset = new RMProcessAsset();
        $moProcessAsset->setFieldValue('process_id', $processId);
        $moProcessAsset->setFieldValue('asset_id', $assetId);
        
        if($assetImportance)
          $moProcessAsset->setFieldValue('importance', $assetImportance);
        $moProcessAsset->insert();
    }
  }
}
?>