<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryCheckSceneName.
 *
 * <p>Consulta que retorna verdadeiro ou falso, verificando se o j� existe o nome do cen�rio.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryCheckSceneName extends FWDDBQueryHandler{
	
	private $selectListName = array();
	private $choice = false;
	private $ciSceneName = '';
		
	public function __construct($poDB=null){
		parent::__construct($poDB);
		
		$this->coDataSet->addFWDDBField(new FWDDBField('fkContext', 'select_scene_id'	 ,DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField('sname'	, 'select_scene_name',DB_STRING));
	}
	
	public function setSceneName($piSceneName){
		$this->ciSceneName = $piSceneName;
	}
	
	public function makeQuery(){
		$this->csSQL = "SELECT fkContext AS select_scene_id, sname AS select_scene_name
					FROM view_cm_scene_active WHERE sname = '" . $this->ciSceneName . "'";
		
	}
	
	public function fetchSceneName() {
		parent::executeQuery();
		$this->selectListName = array();
		while ($this->coDataSet->fetch()) {
			$this->selectListName[] = $this->coDataSet->getFieldByAlias("select_scene_name")->getValue();
		}		
	}
	
	public function getSceneName() {
		if(($this->selectListName != '') && (count($this->selectListName) >= 1)){
			return $this->choice = true;
		}else{
			return $this->choice = false;
		}
	}
}

?>