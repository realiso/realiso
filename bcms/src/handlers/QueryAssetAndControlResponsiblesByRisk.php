<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryAssetAndControlResponsiblesByRisk.
 *
 * <p>Consulta para retornar os responsáveis pelos ativos e controles
 * relacionados com os riscos passados para a query.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryAssetAndControlResponsiblesByRisk extends FWDDBQueryHandler {
  
  protected $csRiskIds = "0";
  protected $ciIncidentId = 0;
  
  public function __construct($poDB){
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField('','relation_id' ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('','name'       ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('','responsible',DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('','type'       ,DB_NUMBER));
  }
  
  public function setRiskIds($psRiskIds) {
    if($psRiskIds){
      $this->csRiskIds = implode(',',array_filter(explode(':',$psRiskIds)));
    }
  }
  
  public function setIncidentId($piIncidentId){
    $this->ciIncidentId = $piIncidentId;
  }

  public function makeQuery(){
    $this->csSQL = "SELECT
                      ir.fkContext as relation_id,
                      a.fkSecurityResponsible as responsible,
                      ".CONTEXT_ASSET." as type
                    FROM
                      view_ci_incident_risk_active ir
                      JOIN view_rm_risk_active r ON (r.fkContext = ir.fkRisk)
                      JOIN view_rm_asset_active a ON (a.fkContext = r.fkAsset)
                    WHERE
                      ir.fkRisk IN ({$this->csRiskIds})
                      AND ir.fkIncident = {$this->ciIncidentId}

                    UNION

                    SELECT
                      ic.fkContext as relation_id,
                      c.fkResponsible as responsible,
                      ".CONTEXT_CONTROL." as type
                    FROM
                      view_ci_inc_control_active ic
                      JOIN view_rm_risk_control_active rc ON (rc.fkControl = ic.fkControl)
                      JOIN view_rm_control_active c ON (c.fkContext = rc.fkControl)
                    WHERE
                      rc.fkRisk IN ({$this->csRiskIds})
                      AND ic.fkIncident = {$this->ciIncidentId}";
  }

}

?>