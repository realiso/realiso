<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryChartIncidentFinancialImpact.
 *
 * <p>Consulta para popular o gr�fico de incidentes com maior impacto financeiro.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryChartIncidentFinancialImpact extends FWDDBQueryHandler {

  protected $caIncidentFinancialImpactSummary = array();
  protected $ciUserId = 0;

  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField("ordem","ordem", DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("count","count", DB_NUMBER));
  }

  public function setUserId($psUserId) {
    $this->ciUserId = $psUserId;
  }

  public function makeQuery() {
    $msUserJoin = ($this->ciUserId ? " JOIN isms_user u ON (i.fkResponsible = u.fkContext AND u.fkContext = ".$this->ciUserId.")" : "");
    $this->csSQL = "SELECT i.sName as ordem, CASE WHEN SUM(ifi.nValue) IS NULL THEN 0 ELSE SUM(ifi.nValue) END as count
                    FROM view_ci_incident_active i
                    LEFT JOIN ci_incident_financial_impact ifi ON (ifi.fkIncident = i.fkContext)
                    $msUserJoin
                    GROUP BY i.fkContext, i.sName
                    ORDER BY count DESC 
";
  }

  public function executeQuery() {
    parent::executeQuery(10,0);
    $this->caIncidentFinancialImpactSummary = array();
    while ($this->coDataSet->fetch())
      $this->caIncidentFinancialImpactSummary[] = array($this->coDataSet->getFieldByAlias("ordem")->getValue(),$this->coDataSet->getFieldByAlias("count")->getValue());
  }

  public function getIncidentFinancialImpactSummary() {
    return $this->caIncidentFinancialImpactSummary;
  }
}
?>