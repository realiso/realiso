<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryChartProcessAsset.
 *
 * <p>Consulta para popular o gr�fico de sum�rio de risco potencial.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryChartProcessAsset extends FWDDBQueryHandler {

  protected $caProcessAssetSummary = array();
  protected $ciUserId = 0;

  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField("ordem","ordem", DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("count","count", DB_NUMBER));
  }

  public function setUserId($psUserId) {
    $this->ciUserId = $psUserId;
  }

  public function makeQuery() {
    $msUserJoin = ($this->ciUserId ? " JOIN isms_user u ON (p.fkResponsible = u.fkContext AND u.fkContext = ".$this->ciUserId.")" : "");
    $this->csSQL = "
										SELECT p.sName AS ordem, COUNT(pa.fkAsset) AS count 
										FROM view_rm_process_active p
                    LEFT JOIN view_rm_process_asset_active pa ON (pa.fkProcess=p.fkContext)
										JOIN isms_context c ON (p.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED.")
										$msUserJoin
                    GROUP BY p.fkContext, p.sName
                    ORDER BY count DESC 
";
  }

  public function executeQuery() {
  	//limite de 10 resultados retornados pela query. Abstra��o do TOP 10
    parent::executeQuery(10,0);
    $this->caProcessAssetSummary = array();
    while ($this->coDataSet->fetch())
      $this->caProcessAssetSummary[] = array($this->coDataSet->getFieldByAlias("ordem")->getValue(),$this->coDataSet->getFieldByAlias("count")->getValue());
  }

  public function getProcessAssetSummary() {
    return $this->caProcessAssetSummary;
  }
}
?>