<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryChartDocumentsType.
 *
 * <p>Consulta para popular o gr�fico de quantidade de documentos por tipo.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryChartDocumentsType extends FWDDBQueryHandler {

  protected $caDocumentsType = array();  

  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField("",	"doc_type",				DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("",	"doc_type_total",	DB_NUMBER));
  }

  public function makeQuery() {
  
    $miUserId = ISMSLib::getCurrentUserId();
    $this->csSQL = "SELECT d.nType as doc_type, 
                           count(d.nType) as doc_type_total 
                      FROM view_pm_published_docs d
                           JOIN isms_context c ON (d.fkContext=c.pkContext)
                           LEFT JOIN pm_doc_approvers da ON (da.fkDocument = d.fkContext AND da.fkUser = $miUserId)
                           LEFT JOIN pm_doc_readers dr ON (dr.fkDocument = d.fkContext AND dr.fkUser = $miUserId AND dr.bDenied=0)
                           LEFT JOIN pm_doc_instance di ON (d.fkCurrentVersion = di.fkContext)
                      WHERE d.fkMainApprover = $miUserId
                            OR (c.nState = ".CONTEXT_STATE_DOC_DEVELOPING." AND d.fkAuthor = $miUserId)
                            OR (c.nState = ".CONTEXT_STATE_DOC_PENDANT." AND da.fkDocument IS NOT NULL)
                            OR (
                              (
                                dr.fkDocument IS NOT NULL
                                OR da.fkDocument IS NOT NULL
                                OR d.fkAuthor = $miUserId
                              )AND(
                                di.nMajorVersion > 0
                                AND c.nState = ".CONTEXT_STATE_DOC_APPROVED."
                                AND d.dDateProduction <= '".date('Y-m-d')."'
                              )
                            )
                    GROUP BY d.nType";
  }

  public function executeQuery() {
    parent::executeQuery();
    $this->caDocumentsType = array();
    while ($this->coDataSet->fetch())
      $this->caDocumentsType[$this->coDataSet->getFieldByAlias("doc_type")->getValue()] = $this->coDataSet->getFieldByAlias("doc_type_total")->getValue();
  }

  public function getDocumentsType() {
    return $this->caDocumentsType;
  }
}
?>