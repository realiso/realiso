<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryChartIncidentLossType.
 *
 * <p>Consulta para popular o gr�fico com a propor��o de incidentes com sua categoria.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryChartIncidentCategory extends FWDDBQueryHandler {

  protected $caIncidentCategorySummary = array();
  protected $ciUserId = 0;

  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField("ordem","ordem", DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("count","count", DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("","orderby", DB_NUMBER));
  }

  public function setUserId($psUserId) {
    $this->ciUserId = $psUserId;
  }

  public function makeQuery() {
    $msWhere = ($this->ciUserId ? "WHERE i.fkResponsible = ".$this->ciUserId : "");
    $this->csSQL = "
                    SELECT c.sname as ordem, COUNT(i.fkContext) as count
                    FROM view_ci_incident_active i
                    JOIN view_ci_category_active c ON (i.fkCategory = c.fkContext)
                    $msWhere
                    GROUP BY c.sname
                    ORDER BY count DESC
                    ";
  }

  public function executeQuery() {
    parent::executeQuery();
    $this->caIncidentCategorySummary = array();
    while ($this->coDataSet->fetch())
      $this->caIncidentCategorySummary[] = array($this->coDataSet->getFieldByAlias("ordem")->getValue(),$this->coDataSet->getFieldByAlias("count")->getValue());
  }

  public function getIncidentCategorySummary() {
    return $this->caIncidentCategorySummary;
  }
}
?>