<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryChartRedAssetRisk.
 *
 * <p>Consulta para popular o gr�fico de ativos e quantidade de riscos.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryChartRedAssetRisk extends FWDDBQueryHandler {

  protected $caRedAssetRiskSummary = array();
  protected $ciUserId = 0;

  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField("ordem","ordem", DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("count","count", DB_NUMBER));
  }

  public function setUserId($psUserId) {
    $this->ciUserId = $psUserId;
  }

  public function makeQuery() {
    $msUserJoin = ($this->ciUserId ? " JOIN isms_user u ON (a.fkResponsible = u.fkContext AND u.fkContext = ".$this->ciUserId.")" : "");
    $this->csSQL = "
                    SELECT a.sname as ordem, COUNT(r.fkcontext) as count 
                    FROM view_rm_asset_active a
                    LEFT JOIN view_rm_risk_active r ON (r.fkAsset=a.fkcontext AND r.nValueResidual >= ".ISMSLib::getConfigById(RISK_HIGH).")
                    JOIN isms_context c ON (a.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED.")
                    $msUserJoin
                    GROUP BY a.fkcontext, a.sname
                    ORDER BY count DESC 
";
  }

  public function executeQuery() {
  	//limite de 10 resultados na consulta
    parent::executeQuery(10,0);
    $this->caRedAssetRiskSummary = array();
    while ($this->coDataSet->fetch())
      $this->caRedAssetRiskSummary[] = array($this->coDataSet->getFieldByAlias("ordem")->getValue(),$this->coDataSet->getFieldByAlias("count")->getValue());
  }

  public function getRedAssetRiskSummary() {
    return $this->caRedAssetRiskSummary;
  }
}
?>