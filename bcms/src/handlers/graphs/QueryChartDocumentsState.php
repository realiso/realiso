<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryChartDocumentsState.
 *
 * <p>Consulta para popular o gr�fico de quantidade de documentos por status.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryChartDocumentsState extends FWDDBQueryHandler {

  protected $caDocumentsState = array();  

  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField("",	"doc_state",				DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("",	"doc_state_total",	DB_NUMBER));
  }

  public function makeQuery() {
  
    $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    
    $this->csSQL = "
SELECT 
    res.document_status as doc_state, 
    count(res.document_status) as doc_state_total
  FROM(
    SELECT 
      d.fkContext AS document_id,
      d.sName AS document_name,
      di.fkContext as instance_id,
      CASE
        WHEN 
          d.fkCurrentVersion IS NULL 
          OR (
            c.nState = ".CONTEXT_STATE_DOC_DEVELOPING." 
            AND di.nMajorVersion = 0
          ) 
        THEN ".CONTEXT_STATE_DOC_DEVELOPING."
        WHEN 
          c.nState = ".CONTEXT_STATE_DOC_PENDANT." 
          AND di.dBeginProduction IS NULL
        THEN ".CONTEXT_STATE_DOC_PENDANT." 
        WHEN	
          d.fkCurrentVersion = di.fkContext 
          AND (
            c.nState = ".CONTEXT_STATE_DOC_APPROVED."
            OR (
              c.nState = ".CONTEXT_STATE_DOC_DEVELOPING." 
              AND di.nMajorVersion > 0
            )
            OR ( 
              c.nState = ".CONTEXT_STATE_DOC_PENDANT."
              AND di.dEndProduction IS NOT NULL
            )
          ) 
        THEN ".CONTEXT_STATE_DOC_APPROVED."
        WHEN 
          di.fkDocument = d.fkContext 
          AND di.fkContext != d.fkCurrentVersion 
          AND di.dEndProduction IS NOT NULL 
        THEN ".CONTEXT_STATE_DOC_OBSOLETE."
        ELSE ".CONTEXT_STATE_DOC_REVISION."
      END as document_status
    FROM 
      pm_document d
      JOIN isms_context c ON ( c.pkContext = d.fkContext AND c.nState <> 2705 )
      LEFT JOIN view_pm_doc_instance_active di ON (di.fkDocument = d.fkContext)
      LEFT JOIN pm_doc_approvers da ON (da.fkDocument = d.fkContext AND da.fkUser = $miUserId)
      LEFT JOIN pm_doc_readers dr ON (dr.fkDocument = d.fkContext AND dr.fkUser = $miUserId AND dr.bDenied=0)
      LEFT JOIN pm_doc_instance di2 ON (d.fkCurrentVersion = di2.fkContext)
      WHERE 
        d.nType NOT IN (".CONTEXT_REGISTER .")
        AND
        (
          d.fkMainApprover = $miUserId
          OR (c.nState = ".CONTEXT_STATE_DOC_DEVELOPING." AND d.fkAuthor = $miUserId)
          OR (c.nState = ".CONTEXT_STATE_DOC_PENDANT." AND da.fkDocument IS NOT NULL)
          OR (
            (
              dr.fkDocument IS NOT NULL
              OR da.fkDocument IS NOT NULL
              OR d.fkAuthor = $miUserId
            )AND(
              di2.nMajorVersion > 0
              AND c.nState = ".CONTEXT_STATE_DOC_APPROVED."
              AND d.dDateProduction <= '".date('Y-m-d')."'
            )
          )
        )
  )res
  GROUP BY document_status
  ORDER BY res.document_status
    ";
  }

  public function executeQuery() {
    parent::executeQuery();
    $this->caDocumentsState = array();
    while ($this->coDataSet->fetch())
      if( $this->coDataSet->getFieldByAlias('doc_state')->getValue() != CONTEXT_STATE_DOC_OBSOLETE )
        $this->caDocumentsState[$this->coDataSet->getFieldByAlias("doc_state")->getValue()] = $this->coDataSet->getFieldByAlias("doc_state_total")->getValue();
  }

  public function getDocumentsState() {
    return $this->caDocumentsState;
  }
}
?>