<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryChartResidualRisk.
 *
 * <p>Consulta para popular o gr�fico com a quantidade de riscos residuais.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryChartResidualRisk extends FWDDBQueryHandler {

  protected $caResidualRiskSummary = array();
  protected $ciUserId = 0;

  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField("ordem","ordem", DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("count","count", DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("orderby","orderby", DB_NUMBER));
  }

  public function setUserId($psUserId) {
    $this->ciUserId = $psUserId;
  }

  public function makeQuery() {
    $msUserJoin = ($this->ciUserId ? " JOIN isms_user u ON (r.fkResponsible = u.fkContext AND u.fkContext = ".$this->ciUserId.")" : "");
    $this->csSQL = "
										SELECT 'risk_high' AS ordem, COUNT(*) AS count, 1 as orderby
										FROM view_rm_risk_active r
										JOIN isms_context c ON (r.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED." AND r.nValueResidual >= ".ISMSLib::getConfigById(RISK_HIGH).")
										$msUserJoin
										UNION
										SELECT 'risk_mid' AS ordem, COUNT(*) AS count, 2 as orderby
										FROM view_rm_risk_active r
										JOIN isms_context c ON (r.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED." AND r.nValueResidual > ".ISMSLib::getConfigById(RISK_LOW)." AND r.nValueResidual < ".ISMSLib::getConfigById(RISK_HIGH).")
										$msUserJoin
										UNION
										SELECT 'risk_low' AS ordem, COUNT(*) AS count, 3 as orderby
										FROM view_rm_risk_active r
										JOIN isms_context c ON (r.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED." AND (r.nValueResidual <= ".ISMSLib::getConfigById(RISK_LOW)." AND r.nValueResidual > 0))
										$msUserJoin
										UNION
										SELECT 'risk_np' AS ordem, COUNT(*) AS count, 4 as orderby
										FROM view_rm_risk_active r
										JOIN isms_context c ON (r.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED." AND (r.nValueResidual = 0 OR r.nValueResidual IS NULL))
										$msUserJoin
                    ORDER BY orderby
";
  }

  public function executeQuery() {
    parent::executeQuery();
    $this->caResidualRiskSummary = array();
    while ($this->coDataSet->fetch())
      $this->caResidualRiskSummary[$this->coDataSet->getFieldByAlias("ordem")->getValue()] = $this->coDataSet->getFieldByAlias("count")->getValue();
  }

  public function getResidualRiskSummary() {
    return $this->caResidualRiskSummary;
  }
}
?>