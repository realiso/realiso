<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryDamagesFromImpact.
 *
 * <p>Consulta para buscar os danos de um impacto</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryDamagesFromImpact extends FWDDBQueryHandler {

  private $impactId = 0;

  public function __construct($poDB = null) {
    parent::__construct($poDB);
  
    $this->coDataSet->addFWDDBField(new FWDDBField('sname','sname',DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('nweight','nweight',DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('impactName','impactName',DB_STRING));
  }
  
  public function makeQuery() {
    $this->csSQL = "
      SELECT 
        dmp.sname AS sname, 
        dmp.nweight AS nweight, 
        dm.sname AS impactName
      FROM 
        cm_impact_damage_range idr

      LEFT JOIN cm_damage_matrix_parameter dmp ON dmp.fkcontext = idr.fkdamageparameter
      LEFT JOIN cm_damage_matrix dm ON dm.fkcontext = idr.fkdamage
      
      WHERE 
        fkimpactdamage = {$this->impactId};
    ";
  }

  public function getDamages($impactId) {
    $this->impactId = $impactId;
    $this->makeQuery();
    $this->executeQuery();
    
    $result = array();
    while($this->fetch()){
      $result["{$this->coDataSet->getFieldByAlias('impactName')->getValue()}"] = $this->coDataSet->getFieldByAlias("nweight")->getValue();
    }

    return $result;
  }
}
?>