<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryDocumentApprovers.
 *
 * <p></p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryDocumentApprovers extends FWDDBQueryHandler {
	
	private $ciDocumentId;
  private $caApprovers;
  private $caApproversNames;
  
  public function __construct($poDB,$piDocumentId){
    parent::__construct($poDB);
    $this->ciDocumentId = $piDocumentId;
    
    $this->coDataSet->addFWDDBField(new FWDDBField('fkUser','user_id',DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('fkUser','user_name',DB_STRING));
  }

  public function makeQuery(){
    $this->csSQL ="
							    SELECT da.fkUser as user_id, u.sName as user_name
									FROM pm_doc_approvers da, view_isms_user_active u
									WHERE da.fkDocument = {$this->ciDocumentId} AND da.fkUser = u.fkContext
									UNION
									SELECT d.fkMainApprover as user_id, u.sName as user_name
									FROM pm_document d, view_isms_user_active u
									WHERE d.fkContext = {$this->ciDocumentId}  AND d.fkMainApprover = u.fkContext
									ORDER BY user_name";
  }
  
  public function executeQuery() {
    parent::executeQuery();
    while ($this->coDataSet->fetch()) {
      $this->caApprovers[] = $this->coDataSet->getFieldByAlias("user_id")->getValue();
      $this->caApproversNames[] = $this->coDataSet->getFieldByAlias("user_name")->getValue();
    }
  }
  
  public function getApprovers() {
    return $this->caApprovers;
  }
  
  public function getApproversNames() {
    return $this->caApproversNames;
  }
}
?>