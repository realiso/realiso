<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QuerySummaryControl.
 *
 * <p>Consulta para popular o grid de sum�rio de controles.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QuerySummaryControl extends FWDDBQueryHandler {

  protected $caControlSummary = array();

  protected $ciUserId = 0;

  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField("ordem","ordem", DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("count","count", DB_NUMBER));
 }

  public function setUserId($psUserId) {
    $this->ciUserId = $psUserId;
  }

  public function makeQuery() {
    $miTimestamp = ISMSLib::ISMSTime();
    $miTimestamp = mktime(0, 0, 0, date("m",$miTimestamp), date("d",$miTimestamp),  date("Y",$miTimestamp));
    
    $msCurrentTime = ISMSLib::getTimestampFormat($miTimestamp);
    
    $this->csSQL = "
"./* controles implementados*/"
SELECT 'implemented_total' AS ordem, count(c.fkContext) as count
  FROM view_rm_control_active c
  WHERE c.dDateImplemented IS NOT NULL

UNION
"./* controles n�o implementados*/"
SELECT 'planned_total' AS ordem, count(c.fkContext) as count
  FROM view_rm_control_active c
  WHERE c.dDateImplemented IS NULL

UNION
"./*processo de implanta��o*/"
SELECT 'planned_implantation' as ordem, count(c.fkContext) as count
  FROM view_rm_control_active c
  WHERE c.dDateDeadline >= $msCurrentTime AND c.dDateImplemented IS NULL

UNION
"./*implanta��o atrazada*/"
SELECT 'planned_late' as ordem, count(c.fkContext) as count
  FROM view_rm_control_active c
  WHERE c.dDateDeadline < $msCurrentTime AND c.dDateImplemented IS NULL

UNION
"./*controles confi�veis e eficientes*/"
SELECT 'implemented_trusted_and_efficient' as ordem, count(c.fkContext) as count
  FROM rm_control c
  JOIN isms_context ctx ON (c.fkContext = ctx.pkContext AND ctx.nState != 2705)
  WHERE c.dDateImplemented IS NOT NULL  
        AND c.fkContext IN ( SELECT fkControlEfficiency FROM wkf_control_efficiency WHERE fkControlEfficiency = c.fkContext )
        AND c.fkContext IN ( SELECT fkControlTest FROM wkf_control_test WHERE fkControlTest = c.fkContext)
  AND c.bRevisionIsLate = 0
  AND c.bTestIsLate = 0
  AND c.bEfficiencyNotOk = 0
  AND c.bTestNotOk = 0

UNION
"./*controles n�o confiaveis*/"
SELECT 'control_test_not_ok' as ordem, count(c.fkContext) as count
  FROM rm_control c
  JOIN isms_context ctx ON (c.fkContext = ctx.pkContext AND ctx.nState != 2705)
  WHERE c.dDateImplemented IS NOT NULL
  AND c.bTestIsLate = 0
  AND c.bTestNotOk = 1 

UNION
"./* Controles n�o eficientes -> qualquer controle que teve marcado como ineficiente na sua revis�o.*/"
SELECT 'control_revision_not_ok' as ordem, count(c.fkContext) as count
  FROM rm_control c
  JOIN isms_context ctx ON (c.fkContext = ctx.pkContext AND ctx.nState != 2705)
  WHERE c.dDateImplemented IS NOT NULL
        AND c.bRevisionIsLate = 0
  AND c.bEfficiencyNotOk = 1

UNION

"./*Controles n�o medidos   -> (controles implementados que a medicao (teste ou revis�o) expirou)
                               OU (controles implementados que n�o possuem revis�o)
                               OU (controles implementados que n�o possuem teste)
                             )*/"

SELECT 'control_not_measured' as ordem, count(c.fkContext) as count
  FROM rm_control c
  JOIN isms_context ctx ON (c.fkContext = ctx.pkContext AND ctx.nState != 2705)
  WHERE c.dDateImplemented IS NOT NULL
        AND ( c.bTestIsLate = 1
              OR c.bRevisionIsLate = 1
              OR c.fkContext NOT IN ( SELECT fkControlEfficiency FROM wkf_control_efficiency WHERE fkControlEfficiency = c.fkContext )
        OR c.fkContext NOT IN ( SELECT fkControlTest FROM wkf_control_test WHERE fkControlTest = c.fkContext)
  )
";

  }

  public function executeQuery() {
    parent::executeQuery();
    $this->caControlSummary = array();
    while ($this->coDataSet->fetch())
      $this->caControlSummary[$this->coDataSet->getFieldByAlias("ordem")->getValue()] = $this->coDataSet->getFieldByAlias("count")->getValue();
  }

  public function getControlSummary() {
    return $this->caControlSummary;
  }
}
?>