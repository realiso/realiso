<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryDocumentComponents.
 *
 * <p>Consulta que busca os componentes de um documento.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryDocumentComponents extends FWDDBQueryHandler {
	
	private $ciDocumentId;
  private $caComponents;
  
  public function __construct($poDB,$piDocumentId){
    parent::__construct($poDB);
    $this->ciDocumentId = $piDocumentId;
    
    $this->coDataSet->addFWDDBField(new FWDDBField('','context_id',DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('','context_type',DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('','context_name',DB_STRING));
  }

  public function makeQuery(){    
    $this->csSQL = "SELECT context_id, context_type, context_name
                    FROM (
                      SELECT context_id, context_type, context_name
                      FROM context_names c
                      		
                      UNION SELECT fkContext as context_id, 
	                    ".CONTEXT_SCOPE." as context_type,
	                    '".ISMSContextObject::getContextObject(CONTEXT_SCOPE)->getLabel()."' as context_name
	                    FROM isms_scope
	                    
	                    UNION SELECT fkContext as context_id, 
	                    ".CONTEXT_POLICY." as context_type,
	                    '".ISMSContextObject::getContextObject(CONTEXT_POLICY)->getLabel()."' as context_name
	                    FROM isms_policy
                    ) res
                    JOIN isms_context ctx_ext ON (ctx_ext.pkContext = res.context_id AND ctx_ext.nState <> " . CONTEXT_STATE_DELETED . ")
                    JOIN pm_doc_context dc ON (ctx_ext.pkContext = dc.fkContext AND dc.fkDocument = {$this->ciDocumentId})
            ";
  }
  
  public function executeQuery() {
    parent::executeQuery();
    while ($this->coDataSet->fetch()) {
      $this->caComponents[$this->coDataSet->getFieldByAlias("context_id")->getValue()] = array($this->coDataSet->getFieldByAlias("context_type")->getValue(), $this->coDataSet->getFieldByAlias("context_name")->getValue());      
    }
  }
  
  public function getComponents() {
    return $this->caComponents;
  }
}
?>