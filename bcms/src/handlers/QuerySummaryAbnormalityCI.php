<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QuerySummaryAbnormalityCI.
 *
 * <p>Consulta para popular o grid de sum�rio de anormalidades da gest�o de incidente.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QuerySummaryAbnormalityCI extends FWDDBQueryHandler {

  protected $caValues = array();

  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField("ordem","ordem", DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("count","count", DB_NUMBER));
 }

  public function makeQuery() {
    $msCurrentTime = ISMSLib::getTimestampFormat(time());
    $moConfig = new ISMSConfig();
    $msWhere = '';
    $maFilterTypes = array(
            ACT_NON_CONFORMITY_APPROVAL,
            ACT_ACTION_PLAN_FINISH_CONFIRM,
            ACT_ACTION_PLAN_REVISION,
            ACT_OCCURRENCE_APPROVAL,
            ACT_INC_DISPOSAL_APPROVAL,
            ACT_INC_SOLUTION_APPROVAL,
            ACT_INC_CONTROL_INDUCTION,
            ACT_AP_APPROVAL
    );

    if(count($maFilterTypes)){
      $msWhere .= "AND wt.nActivity IN (".implode(',',$maFilterTypes).") ";
    }
    
    $this->csSQL = "
SELECT 'pendant_tasks_ci' as ordem, count(*) as count
   FROM wkf_task wt
   WHERE wt.bVisible = 1
   $msWhere

UNION

SELECT 'incident_without_risk' as ordem, count(*) as count
   FROM view_ci_incident_active ci
   WHERE ci.fkContext NOT IN (SELECT fkIncident FROM view_ci_incident_risk_active)

UNION

SELECT 'nc_without_ap' as ordem, count(*) as count
   FROM view_ci_nc_active nc
   WHERE nc.fkContext NOT IN (SELECT fkNC FROM ci_nc_action_plan)

UNION

SELECT 'pa_without_doc' as ordem, count(*) as count
   FROM view_ci_action_plan_active ap
   WHERE (ap.fkContext NOT IN (
    SELECT dc.fkContext 
    FROM view_pm_doc_context_active dc
      JOIN view_pm_document_active d ON (dc.fkdocument = d.fkcontext)
      JOIN isms_context c ON (d.fkcontext = c.pkcontext)
      JOIN view_pm_doc_instance_active di ON (d.fkcurrentversion = di.fkcontext)
    WHERE
    (
      c.nState = ".CONTEXT_STATE_DOC_APPROVED." 
      OR ( c.nState = ".CONTEXT_STATE_DOC_DEVELOPING."  AND di.nMajorVersion > 0)
      OR ( c.nState = ".CONTEXT_STATE_DOC_PENDANT." AND di.dEndProduction IS NOT NULL )
    )
))

UNION

SELECT 'incident_without_occurrence' as ordem, count(*) as count
  FROM view_ci_incident_active ci
  WHERE ci.fkContext NOT IN (SELECT fkIncident FROM view_ci_occurrence_active WHERE fkIncident IS NOT NULL) 
";
  }

  public function executeQuery() {

    parent::executeQuery();
    while ($this->coDataSet->fetch())
      $this->caValues[$this->coDataSet->getFieldByAlias("ordem")->getValue()] = $this->coDataSet->getFieldByAlias("count")->getValue();
  }

  public function getValues() {
    $this->makeQuery();
    $this->executeQuery();
    return $this->caValues;
  }
  
}
?>