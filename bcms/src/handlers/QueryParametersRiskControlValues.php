<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryParametersRiskControlValues.
 *
 * <p>Consulta que retorna os valores dos parametros da parametrização de riscos.</p>
 * @package ISMS
 * @subpackage select
 */
class QueryParametersRiskControlValues extends FWDDBQueryHandler {

  public function __construct($poDB){
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField('','value_id',DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('','parameter_value',DB_NUMBER));
  }  
  
  public function makeQuery(){
    
$this->csSQL ="
select pkrcvaluename as value_id, nvalue as parameter_value from rm_rc_parameter_value_name";
}
  
  public function getParameterRCValues(){
		$this->makeQuery();
		$maReturn = array();
		if($this->executeQuery()){
			$moDataset = $this->getDataset();
			while($moDataset->fetch()){
				$maReturn[$moDataset->getFieldByAlias("value_id")->getValue()]=$moDataset->getFieldByAlias("parameter_value")->getValue();
			}
		}
		return $maReturn;
	} 
}
?>