<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryWKFCIACSendNCToFinished.
 *
 * <p>Consulta que retorna todos os ids de n�o conformidade no estado CI_WAITING_CONCLUSION
 * relacionados ao PA que possuem somente esse plano de a��o no estado WAITING_CONCLUSION.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryWKFCIACSendNCToFinished extends FWDDBQueryHandler {

  private $ciAcId;

  public function __construct($poDB){
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField('nc.fkContext'  ,'nc_id'         ,DB_NUMBER));
  }

  public function setAcId($piAcId){
    $this->ciAcId = $piAcId;
  }
  
  public function makeQuery(){
    $this->csSQL = "
SELECT nc.fkContext as nc_id
  FROM view_ci_nc_active nc
  JOIN ci_nc_action_plan ncap ON (nc.fkContext = ncap.fknc)
  JOIN view_ci_action_plan_active ap ON (ap.fkContext = ncap.fkactionplan AND ap.fkContext = {$this->ciAcId})
  JOIN isms_context c ON (nc.fkContext = c.pkContext AND c.nState = ".CONTEXT_STATE_CI_WAITING_CONCLUSION.") 
  WHERE NOT EXISTS( 
    SELECT ap.fkContext FROM view_ci_action_plan_active ap
      JOIN ci_nc_action_plan ncap2 ON (ncap2.fkactionplan = ap.fkContext AND ap.fkContext != {$this->ciAcId} )
      JOIN isms_context c ON (c.pkContext = ap.fkContext AND c.nState IN (". CONTEXT_STATE_NONE .", ". CONTEXT_STATE_AP_PENDANT .",". CONTEXT_STATE_AP_WAITING_CONCLUSION ."))
    WHERE ncap2.fkNc = nc.fkContext
  )
";
  }

}

?>