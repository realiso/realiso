<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

class QueryAreaFromPlace extends FWDDBQueryHandler {

	private $placeId = 0;

	private $caAreas = array();

	public function __construct($poDB) {
		parent::__construct($poDB);

		$this->coDataSet->addFWDDBField(new FWDDBField("fkarea","area_id", 	DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("sname",  "area_name", 	DB_STRING));
	}

	public function setPlaceId($piPlaceId) {
		$this->placeId = $piPlaceId;
	}

	public function makeQuery() {
		$this->csSQL = "SELECT fkcontext as area_id, sname as area_name FROM view_rm_area_active WHERE fkplace = {$this->placeId}"; 
	}

	public function fetchAreas() {
		parent::executeQuery();
		$this->caAreas = array();
		while ($this->coDataSet->fetch()) {
			$this->caAreas[] = $this->coDataSet->getFieldByAlias("area_id")->getValue();
		}
	}

	public function getAreas() {
		return $this->caAreas;
	}
}
?>