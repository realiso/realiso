<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

class QueryTop10Qualitative extends FWDDBQueryHandler {

	public function __construct($poDB) {
		parent::__construct($poDB);
		$this->coDataSet->addFWDDBField(new FWDDBField("name","name", DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("plans","plans", DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("ranking","ranking", DB_NUMBER));
	}

	public function makeQuery() {
		$this->csSQL = "SELECT 
							p.sName as name,
							coalesce(rnk.ranking,0) as ranking,
							count(distinct pl.fkcontext) as plans
						FROM 
							view_rm_process_active p 
							LEFT JOIN view_process_ranking rnk ON p.fkcontext = rnk.fkprocess
							LEFT JOIN view_cm_plan_active pl on pl.fkprocess = p.fkcontext
						GROUP BY
							p.sName,
							rnk.ranking
						ORDER BY
							2 DESC
						LIMIT 10";
	}
}
?>