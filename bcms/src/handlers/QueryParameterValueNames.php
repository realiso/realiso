<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryParameterValueNames.
 *
 * <p>Consulta para buscar os nomes dos valores dos parāmetros.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryParameterValueNames extends FWDDBQueryHandler {

  protected $ciType;

  public function __construct($poDB) {
    parent::__construct($poDB); 

    $this->coDataSet->addFWDDBField(new FWDDBField('','value_id'  ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('','value_name',DB_STRING));
  }

  public function makeQuery(){
    if(isset($this->ciType)){
      if($this->ciType==RISKPARAMETER_ASSET){
        $this->csSQL = "SELECT pkValueName as value_id, sImportance as value_name "
                      ."FROM rm_parameter_value_name "
                      ."ORDER BY nValue";
      }elseif($this->ciType==RISKPARAMETER_RISK || $this->ciType==RISKPARAMETER_INCIDENTRISK){
        $this->csSQL = "SELECT pkValueName as value_id, sImpact as value_name "
                      ."FROM rm_parameter_value_name "
                      ."ORDER BY nValue";
      }else{
        $this->csSQL = "SELECT pkRCValueName as value_id, sControlImpact as value_name "
                      ."FROM rm_rc_parameter_value_name "
                      ."ORDER BY nValue";
      }
    }else{
      trigger_error('Type must be defined.',E_USER_ERROR);
    }
  }
  
  public function setType($psType){
    if(in_array($psType,array( RISKPARAMETER_ASSET, RISKPARAMETER_RISK, RISKPARAMETER_RISKCONTROL, RISKPARAMETER_INCIDENTRISK))){
      $this->ciType = $psType;
    }else{
      trigger_error('Invalid type. Type must be one of: asset, risk, control or incident_risk.',E_USER_ERROR);
    }
  }

}
?>