<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryCountDocumentsWithoutFiles.
 *
 * <p>Consulta para calcular a quantidade e quantidade percentual de documentos
 * que foram criados mas que n�o foi realizado upload de arquivo
 * </p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryCountDocumentsWithoutFiles extends FWDDBQueryHandler {
  protected $ciValue = 0;
  
  protected $ciUserId = 0;
  
  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField('' ,'doc_statistic' ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('' ,'num_docs'      ,DB_NUMBER));
  }

  public function setUserId($piUserId){
    $this->ciUserId = $piUserId;
  }
  
  public function makeQuery() {
  $msWhere = '';
  if($this->ciUserId){
    $msWhere .= " AND doc.fkMainApprover = ".$this->ciUserId;
  }
  $this->csSQL = "
SELECT 'docs_with_files'as doc_statistic, 
       count(fkContext) as num_docs
  FROM view_pm_document_active 
  WHERE fkContext in(
    SELECT doc.fkContext 
      FROM view_pm_document_active doc
        JOIN view_pm_doc_instance_active doc_inst ON (doc.fkContext = doc_inst.fkDocument )
    WHERE bIsLink = 0
    AND sPath != ''
    $msWhere
  )
";
  }
  
  public function executeQuery() {
    parent::executeQuery();
    if($this->coDataSet->fetch()){
      $this->ciValue = $this->coDataSet->getFieldByAlias("num_docs")->getValue();
    }
  }
  
  public function getValues() {
    $this->makeQuery();
    $this->executeQuery();
    return $this->ciValue;
  }
}
?>