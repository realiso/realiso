<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryTreeDocument.
 *
 * <p>Consulta para popular a tree de documentos.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryTreeDocument extends FWDDBQueryHandler {	
		
	protected $ciDocumentId = 0;
  
  protected $cbPublishedOnly = false;
	
  protected $ciUserId = 0;
  
	public function __construct($poDB) {
		parent::__construct($poDB);	

		$this->coDataSet->addFWDDBField(new FWDDBField('','id', DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField('','node_level', DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField('','node_value', DB_STRING));			
 }
	
	public function setRootDocument($piDocumentId) {
		$this->ciDocumentId = $piDocumentId;
	}
  
  /**
   * Exibe apenas documentos publicados.
   */
  public function setPublishedDocuments($pbPublishedOnly=false) {
    $this->cbPublishedOnly = $pbPublishedOnly;
  }
  
  public function setUserId($piUserId) {
    $this->ciUserId = $piUserId;
  }

	public function makeQuery() {
    $maFilters = array();
    if ($this->ciUserId) {
      $maFilters[] = "(
                      d.fkAuthor = {$this->ciUserId}
                      OR exists(
                        SELECT *
                        FROM pm_doc_approvers da
                        WHERE da.fkDocument = d.fkContext
                          AND da.fkUser = {$this->ciUserId}
                      )
                      OR exists(
                        SELECT *
                        FROM pm_doc_readers dr
                        WHERE dr.fkDocument = d.fkContext
                          AND dr.fkUser = {$this->ciUserId}
                          AND dr.bDenied = 0
                      )
                    )
                   ";
    }
    $msWhere = count($maFilters)?" WHERE ".implode(" AND ",$maFilters):"";
		$this->csSQL = "SELECT t.fkContext as id, t.sName as node_value, t.nLevel as node_level 
										FROM ".FWDWebLib::getFunctionCall("get_document_tree(".$this->ciDocumentId.",-1,".($this->cbPublishedOnly?"1":"0").")")." t
                    JOIN view_pm_document_active d ON (t.fkContext = d.fkContext)"
                    .$msWhere;
	}	
}
?>
