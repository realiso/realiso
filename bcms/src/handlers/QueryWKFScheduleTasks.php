<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryWKFScheduleTasks.
 *
 * <p>Query para pegar os agendamentos para os quais devem ser geradas tarefas.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryWKFScheduleTasks extends FWDDBQueryHandler {

  public function __construct($poDB=null){
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField('s.pkschedule','schedule_id',DB_NUMBER));
  }

  public function makeQuery(){
    $msCurrentDate = FWDWebLib::timestampToDBFormat(strtotime(date('Y-m-d',FWDWebLib::getTime())));
    $this->csSQL = "SELECT s.pkschedule AS schedule_id
                    FROM
                      wkf_schedule s
                      JOIN wkf_task_schedule ts ON (ts.fkschedule = s.pkschedule)
                      JOIN isms_context c ON (c.pkcontext = ts.fkcontext AND c.nstate != ".CONTEXT_STATE_DELETED.")
                    WHERE
                      s.dStart <= {$msCurrentDate}
                      AND (
                        s.dNextOccurrence < s.dEnd
                        OR s.dEnd IS NULL
                      )
                      AND s.dNextOccurrence <= {$msCurrentDate}";
  }
  
  public static function getSchedules(){
    $moQuery = new QueryWKFScheduleTasks();
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    $maSchedules = array();
    while($moQuery->fetch()){
      $maSchedules[] = $moQuery->getFieldValue('schedule_id');
    }
    return $maSchedules;
  }

}

?>