<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryImpactsIntervalInUse.
 *
 * <p>Identifica se os intervalos de impacto j� est�o em uso no sistema.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryImpactsIntervalInUse extends FWDDBQueryHandler {

  public function __construct($poDB) {
    parent::__construct($poDB);

    $this->coDataSet->addFWDDBField(new FWDDBField('s.fkcontext' ,'scene_id' ,DB_NUMBER));
  }

  public function makeQuery() {

    $this->csSQL = "
      SELECT s.fkcontext AS scene_id
      FROM view_cm_scene_active s
      LIMIT 1
		";
    
  }
  
  public function isInUse(){
    $this->makeQuery();
    $this->executeQuery();
    $this->fetch();
    
    $scene_id = $this->coDataSet->getFieldByAlias("scene_id")->getValue();
    
    if($scene_id == null)
      return false;

    return true;
  }

}
?>