<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryAssetAndControlByRisk.
 *
 * <p>Consulta para retornar os ativos e controles relacionados com os riscos passados para a query.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryAssetAndControlByRisk extends FWDDBQueryHandler {  
  
  protected $csRiskIds = "0";
  
  public function __construct($poDB) {
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField("", "context_id",     DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("", "type",   DB_STRING));
  }
  
  public function setRiskIds($psRiskIds) {
    if($psRiskIds){
      $this->csRiskIds = implode(',',array_filter(explode(':',$psRiskIds)));
    }
  }
  
  public function makeQuery() {
    $this->csSQL = "
SELECT r.fkAsset as context_id,
       'asset' as type
  FROM view_rm_risk_active r
  WHERE r.fkContext IN ( $this->csRiskIds )

UNION

SELECT rc.fkControl as context_id,
       'control' as type
  FROM view_rm_risk_control_active rc
  where rc.fkRisk IN ( $this->csRiskIds )
";
  }
  public function getValues(){
    $this->makeQuery();
    $this->executeQuery();
    $maValues = array();
    
    while($this->coDataSet->fetch()){
     $maValues [$this->coDataSet->getFieldByAlias("context_id")->getValue()] =
                $this->coDataSet->getFieldByAlias("type")->getValue();
    }
    return $maValues;
  }
  
}
?>