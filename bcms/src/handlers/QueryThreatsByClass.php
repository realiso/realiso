<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

class QueryThreatsByClass extends FWDDBQueryHandler {

	protected $ciClassId = 0;

	protected $caThreats = array();

	public function __construct($poDB) {
		parent::__construct($poDB);

		$this->coDataSet->addFWDDBField(new FWDDBField('fkContext'   	  ,'threat_id'         		,DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField('sName'		 	  ,'threat_name'		  	,DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField('sDescription'     ,'threat_description'     ,DB_STRING));
	}

	public function setClass($piClassId) {
		$this->ciClassId = $piClassId;
	}

	public function makeQuery() {
		$this->csSQL = "
SELECT t.fkContext as threat_id, t.sname as threat_name, t.sDescription as threat_description 
FROM cm_threat t 
JOIN isms_context c ON (c.pkcontext = t.fkcontext AND c.nState != " . CONTEXT_STATE_DELETED . ") 
WHERE t.nclass=$this->ciClassId";
	}

	public function executeQuery() {
		parent::executeQuery();
		while ($this->coDataSet->fetch()) {
			$miThreatId = $this->coDataSet->getFieldByAlias("threat_id")->getValue();
			$msThreatName = $this->coDataSet->getFieldByAlias("threat_name")->getValue();
			$msThreatDescription = $this->coDataSet->getFieldByAlias("threat_description")->getValue();
			$this->caThreats[$miThreatId] = array('threat_name'=>$msThreatName,'threat_description'=>$msThreatDescription);
		}
	}

	/**
	 * Retorna os eventos de uma categoria (e de suas sub-categorias, se existirem).
	 *
	 * <p>M�todo para retornar os eventos de uma categoria (e de suas sub-categorias,
	 * se existirem).</p>
	 * @access public
	 * @return array Array de ids de eventos
	 */
	public function getThreats() {
		return $this->caThreats;
	}
}
?>
