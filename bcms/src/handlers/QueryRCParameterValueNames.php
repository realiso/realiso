<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryRCParameterValueNames.
 *
 * <p>Consulta para buscar os nomes de todos os valores de parāmetros de risco.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryRCParameterValueNames extends FWDDBQueryHandler {

  public function __construct($poDB) {
    parent::__construct($poDB); 

    $this->coDataSet->addFWDDBField(new FWDDBField('pkRCValueName' ,'rc_parametervalue_id'         ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('nValue'        ,'rc_parametervalue_value'      ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('sRCProbability','rc_parametervalue_probability',DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('sControlImpact','rc_parametervalue_impact'     ,DB_STRING));
  }

  public function makeQuery(){
    $this->csSQL = "SELECT pkRCValueName as rc_parametervalue_id, nValue as rc_parametervalue_value, 
					sRCProbability as rc_parametervalue_probability, sControlImpact as rc_parametervalue_impact 
					FROM rm_rc_parameter_value_name ORDER BY nValue";
  }

}
?>