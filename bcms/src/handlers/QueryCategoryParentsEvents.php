<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryCategoryParentsEvents.
 *
 * <p>Consulta para buscar os eventos de uma categoria e de suas categorias
 * pais ([pai [,avo [,bisavo...]]]).</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryCategoryParentsEvents extends FWDDBQueryHandler {
  
  protected $ciCategoryId = 0;
  
  protected $ciAssetId = 0;
  
  protected $caEvents = array();
  
  public function __construct($poDB) {
    parent::__construct($poDB);
  
    $this->coDataSet->addFWDDBField(new FWDDBField('fkContext'   ,'event_id'         ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('sDescription','event_description',DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('tImpact'     ,'event_impact'     ,DB_STRING));
  }
  
  public function setCategory($piCategoryId) {
    $this->ciCategoryId = $piCategoryId;
  }
  
  public function setAsset($piAssetId) {
    $this->ciAssetId = $piAssetId;
  }
  
  public function makeQuery() {
    $this->csSQL = "
SELECT fkContext as event_id, sDescription as event_description, timpact as event_impact
FROM rm_event
WHERE (
        fkContext IN
        (
         SELECT pkEvent FROM ".FWDWebLib::getFunctionCall("get_supercategories_events(".$this->ciCategoryId.")") . "
        )
      )
      AND fkContext NOT IN
      (
       SELECT fkEvent
       FROM view_rm_risk_active
       WHERE fkAsset = ".$this->ciAssetId." AND fkEvent IS NOT NULL
      )
      AND fkContext IS NOT NULL";
  }
  
  public function executeQuery() {
    parent::executeQuery();
    while ($this->coDataSet->fetch()) {
      $miEventId = $this->coDataSet->getFieldByAlias("event_id")->getValue();
      $msEventName = $this->coDataSet->getFieldByAlias("event_description")->getValue();
      $msEventImpact = $this->coDataSet->getFieldByAlias("event_impact")->getValue();
      $this->caEvents[$miEventId] = array('event_description'=>$msEventName,'event_impact'=>$msEventImpact);
    }
  }
  
  /**
   * Retorna os eventos de uma categoria (e de suas sub-categorias, se existirem).
   * 
   * <p>M�todo para retornar os eventos de uma categoria (e de suas sub-categorias,
   * se existirem).</p>
   * @access public 
   * @return array Array de ids de eventos
   */ 
  public function getEvents() {
    return $this->caEvents;
  }
}
?>
