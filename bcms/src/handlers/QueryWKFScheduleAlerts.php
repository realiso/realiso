<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryWKFScheduleAlerts.
 *
 * <p>Query para pegar os agendamentos para os quais devem ser gerados alertas.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryWKFScheduleAlerts extends FWDDBQueryHandler {

  public function __construct($poDB=null){
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField('s.pkSchedule' ,'schedule_id',DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('ts.fkContext' ,'context_id' ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('ts.nActivity' ,'activity'   ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('ts.nAlertType','alert_type' ,DB_NUMBER));
  }

  public function makeQuery(){
    $msCurrentDate = FWDWebLib::timestampToDBFormat(strtotime(date('Y-m-d',FWDWebLib::getTime())));
    $this->csSQL = "SELECT DISTINCT
                      s.pkSchedule AS schedule_id,
                      ts.fkcontext AS context_id,
                      ts.nactivity AS activity,
                      ts.nAlertType AS alert_type
                    FROM
                      wkf_schedule s
                      JOIN wkf_task_schedule ts ON (ts.fkschedule = s.pkschedule AND ts.bAlertSent = 0)
                      JOIN wkf_task t ON (t.nactivity = ts.nactivity AND t.fkcontext = ts.fkcontext AND t.bvisible = 1)
                      JOIN (
                        SELECT
                          s.pkschedule AS schedule_id,
                          COUNT(*) AS pendant_tasks
                        FROM
                          wkf_schedule s
                          JOIN wkf_task_schedule ts ON (ts.fkschedule = s.pkschedule AND ts.bAlertSent = 0)
                          JOIN wkf_task t ON (t.nactivity = ts.nactivity AND t.fkcontext = ts.fkcontext AND t.bvisible = 1)
                        GROUP BY s.pkschedule
                      ) p ON (p.schedule_id = s.pkschedule)
                    WHERE
                      p.pendant_tasks > 1
                      OR (
                        p.pendant_tasks = 1
                        AND {$msCurrentDate} > s.dDateLimit
                      )";
  }
  
  public static function getTaskSchedules(){
    $moQuery = new QueryWKFScheduleAlerts();
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    $maTaskSchedules = array();
    while($moQuery->fetch()){
      $maTaskSchedules[] = $moQuery->getFieldValues();
    }
    return $maTaskSchedules;
  }

}

?>