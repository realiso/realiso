<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryContextsByDocumentId.
 *
 * <p>Obtem todos os contextos relacionados a um documento.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryContextsByDocumentId extends FWDDBQueryHandler {

  private $ciId = array();

  private $cbExecByTrash = false;
  
  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField("", "context_id", DB_NUMBER));
 }

  public function setId($piId) {
    $this->ciId = $piId;
  }

  public function setExecByTrash($pbExecByTrash){
    $this->cbExecByTrash = $pbExecByTrash;
  }

  public function makeQuery() {
    $msIn = '';
    if($this->cbExecByTrash)
      $msIn = " AND c.nState = 2705 ";
    else
      $msIn = " AND c.nState <> 2705 ";
    
  
    $this->csSQL = "
SELECT fkContext as context_id
  FROM pm_doc_context d
    JOIN isms_context c ON (d.fkContext = c.pkContext $msIn)
  WHERE d.fkDocument = $this->ciId
  ";
}

  public function getValue() {
    $this->makeQuery();
    $this->executeQuery();
    $maIds = array();
    while($this->coDataSet->fetch()){
      $maIds[] = $this->coDataSet->getFieldByAlias("context_id")->getValue();
    }
    
    return $maIds;
  }
}
?>