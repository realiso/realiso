<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryControlProcesses.
 *
 * <p>Consulta que retorna os processos afetados por um controle.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryControlProcesses extends FWDDBQueryHandler {

  protected $ciControlId;

  public function __construct($poDB=null){
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField('pa.fkProcess','process_id',DB_NUMBER));
  }

  public function setControlId($piControlId){
    $this->ciControlId = $piControlId;
  }

  public function makeQuery(){
    if($this->ciControlId){
      $this->csSQL = "SELECT DISTINCT pa.fkProcess as process_id
                      FROM
                        rm_risk_control cr
                        JOIN rm_risk r ON (r.fkContext = cr.fkRisk)
                        JOIN rm_process_asset pa ON (pa.fkAsset = r.fkAsset)
                      WHERE cr.fkControl = {$this->ciControlId}";
    }else{
      trigger_error("You must specify a control.",E_USER_ERROR);
    }
  }

}

?>