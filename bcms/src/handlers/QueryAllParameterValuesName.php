<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryAllParameterValuesName.
 *
 * <p>Consulta para buscar os nomes de todos os valores de parāmetros de risco.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryAllParameterValuesName extends FWDDBQueryHandler {

  public function __construct($poDB) {
    parent::__construct($poDB); 

    $this->coDataSet->addFWDDBField(new FWDDBField('pkValueName'     ,'parametervalue_id'        ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('nValue'          ,'parametervalue_value'     ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('sImportance'     ,'parametervalue_importance',DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('sImpact'         ,'parametervalue_impact'    ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('sRiskProbability','parametervalue_rprob'     ,DB_STRING));
  }

  public function makeQuery(){
    $this->csSQL = "SELECT pkValueName as parametervalue_id, nValue as parametervalue_value, 
					sImportance as parametervalue_importance, sImpact as parametervalue_impact, 
					sRiskProbability as parametervalue_rprob FROM rm_parameter_value_name ORDER BY nValue";
  }

}
?>