<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

class QueryResume extends FWDDBQueryHandler {

	protected $caRiskSummary = array();

	public function __construct($poDB) {
		parent::__construct($poDB);

		$this->coDataSet->addFWDDBField(new FWDDBField("ordem","ordem", DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("withPlan","withPlan", DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("percent","percent", DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField("count","count", DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("orderBy","orderBy", DB_NUMBER));
	}

	public function makeQuery() {
		$this->csSQL = "SELECT 1 as orderBy, 'places' AS ordem, COUNT(distinct plan.fkplace) as withPlan,
						round(cast(COUNT(distinct plan.fkplace) as numeric)/ coalesce(nullif(COUNT(p.fkcontext),0), 1)*100,2)||'%' as percent, 
						COUNT(p.fkcontext) AS count FROM view_cm_place_active p LEFT JOIN view_cm_plan_active plan on plan.fkplace = p.fkcontext WHERE p.fkparent IS NULL
						UNION
						SELECT 2, 'subplaces', COUNT(distinct plan.fkplace), 
						round(cast(COUNT(distinct plan.fkplace) as numeric)/ coalesce(nullif(COUNT(p.fkcontext),0), 1)*100,2)||'%', 
						COUNT(p.fkcontext) FROM view_cm_place_active p LEFT JOIN view_cm_plan_active plan on plan.fkplace = p.fkcontext WHERE p.fkparent IS NOT NULL
						UNION
						SELECT 3, 'areas', COUNT(distinct plan.fkarea), 
						round(cast(COUNT(distinct plan.fkarea) as numeric)/coalesce(nullif(COUNT(a.fkcontext),0), 1) *100,2)||'%', 
						COUNT(a.fkcontext) FROM view_rm_area_active a LEFT JOIN view_cm_plan_active plan on plan.fkarea = a.fkcontext WHERE a.fkparent IS NULL
						UNION
						SELECT 4, 'subareas', COUNT(distinct plan.fkarea), 
						round(cast(COUNT(distinct plan.fkarea) as numeric)/ coalesce(nullif(COUNT(a.fkcontext),0), 1)*100,2)||'%', 
						COUNT(a.fkcontext) FROM view_rm_area_active a LEFT JOIN view_cm_plan_active plan on plan.fkarea = a.fkcontext WHERE a.fkparent IS NOT NULL
						UNION
						SELECT 5, 'processes', COUNT(distinct plan.fkprocess), 
						round(cast(COUNT(distinct plan.fkprocess) as numeric)/ coalesce(nullif(COUNT(p.fkcontext),0), 1)*100,2)||'%', 
						COUNT(p.fkcontext) FROM view_rm_process_active p LEFT JOIN view_cm_plan_active plan on plan.fkprocess = p.fkcontext
						UNION
						SELECT 6, 'scenes', COUNT(distinct plan.fkprocess), 
						round(cast(COUNT(distinct plan.fkprocess) as numeric)/ coalesce(nullif(COUNT(s.fkcontext),0), 1)*100,2)||'%', 
						COUNT(s.fkcontext) FROM view_cm_scene_active s JOIN view_rm_process_active p on p.fkcontext = s.fkprocess LEFT JOIN view_cm_plan_active plan on plan.fkprocess = p.fkcontext
						UNION
						SELECT 7, 'activities', COUNT(distinct plan.fkprocess), 
						round(cast(COUNT(distinct plan.fkprocess) as numeric)/ coalesce(nullif(COUNT(a.fkcontext),0), 1)*100,2)||'%', 
						COUNT(a.fkcontext) FROM view_cm_process_activity_active a JOIN view_rm_process_active p on p.fkcontext = a.fkprocess LEFT JOIN view_cm_plan_active plan on plan.fkprocess = p.fkcontext
						UNION
						SELECT 8, 'assets', COUNT(distinct plan.fkasset), 
						round(cast(COUNT(distinct plan.fkasset) as numeric)/ coalesce(nullif(COUNT(a.fkcontext),0), 1)*100,2)||'%', 
						COUNT(a.fkcontext) FROM view_rm_asset_active a LEFT JOIN view_cm_plan_active plan on plan.fkasset = a.fkcontext
						ORDER BY 1";
	}

	public function executeQuery() {
		parent::executeQuery();
		$this->caRiskSummary = array();
		while ($this->coDataSet->fetch()){
			$this->caRiskSummary[$this->coDataSet->getFieldByAlias("ordem")->getValue()] = array($this->coDataSet->getFieldByAlias("count")->getValue(), $this->coDataSet->getFieldByAlias("withPlan")->getValue(), $this->coDataSet->getFieldByAlias("percent")->getValue());
		}
	}

	public function getRiskSummary() {
		return $this->caRiskSummary;
	}
}
?>