<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryDisciplinaryProcessUsers.
 *
 * <p></p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryDisciplinaryProcessUsers extends FWDDBQueryHandler {
  
  private $ciIncidentId;
  private $caUsers;  
  
  public function __construct($poDB){
    parent::__construct($poDB);

    $this->coDataSet->addFWDDBField(new FWDDBField('','user_name',DB_STRING));
  }

  public function setIncident($piIncidentId) {
    return $this->ciIncidentId = $piIncidentId;
  }

  public function makeQuery(){
    $this->csSQL ="SELECT
                      u.sName as user_name
                    FROM
                      ci_incident_user iu
                      JOIN view_ci_incident_active i ON (iu.fkIncident = i.fkContext AND i.fkContext = {$this->ciIncidentId})
                      JOIN view_isms_user_active u ON (iu.fkUser = u.fkContext)";
  }
  
  public function executeQuery() {
    parent::executeQuery();
    while ($this->coDataSet->fetch()) {      
      $this->caUsers[] = $this->coDataSet->getFieldByAlias("user_name")->getValue();
    }
  }
  
  public function getUsers() {
    return $this->caUsers;
  }
}
?>