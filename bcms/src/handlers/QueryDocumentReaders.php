<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryDocumentReaders.
 *
 * <p>Consulta que busca os leitores de um documento.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryDocumentReaders extends FWDDBQueryHandler {
	
	private $ciDocumentId;
  private $caReaders;
  
  public function __construct($poDB,$piDocumentId){
    parent::__construct($poDB);
    $this->ciDocumentId = $piDocumentId;
    
    $this->coDataSet->addFWDDBField(new FWDDBField('','doc_reader_id',DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('','doc_reader_name',DB_STRING));
  }

  public function makeQuery(){
    $this->csSQL = "SELECT
											dr.fkUser as doc_reader_id,
											u.sName as doc_reader_name
										FROM
											view_pm_document_active d
											JOIN pm_doc_readers dr ON (d.fkContext = dr.fkDocument AND dr.bDenied <> 1)
											JOIN view_isms_user_active u ON (dr.fkUser = u.fkContext)
										WHERE
											d.fkContext = {$this->ciDocumentId}
										ORDER BY
											u.sName";
  }
  
  public function executeQuery() {
    parent::executeQuery();
    while ($this->coDataSet->fetch()) {
      $this->caReaders[$this->coDataSet->getFieldByAlias("doc_reader_id")->getValue()] = $this->coDataSet->getFieldByAlias("doc_reader_name")->getValue();      
    }
  }
  
  public function getReaders() {
    return $this->caReaders;
  }
}
?>