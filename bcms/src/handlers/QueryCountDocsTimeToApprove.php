<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryCountDocsTimeToApprove.
 *
 * <p>Consulta para calcular o tempo m�dio levado para aprovar um documento
 * pelos aprovadores.
 * </p>
 * @package ISMS
 * @subpackage handlers
 */
 
 
class QueryCountDocsTimeToApprove extends FWDDBQueryHandler {
  protected $ciDocCount = 0;
  
  protected $ciUserId = 0;

  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField('fkContext'        ,'context_id'     ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('dBeginApprover'   ,'begin_approver' ,DB_DATETIME));
    $this->coDataSet->addFWDDBField(new FWDDBField('dBeginProduction' ,'end_approver'   ,DB_DATETIME));
  }
  
  public function setUserId($piUserId){
    $this->ciUserId = $piUserId;
  }

  public function makeQuery() {

  $msWhere = '';
  $msJOIN = '';
  if($this->ciUserId){
  $msWhere .= " AND doc.fkMainApprover = ".$this->ciUserId;
  $msJOIN .= " JOIN view_pm_document_active doc ON ( doc.fkContext = doc_inst.fkDocument ) ";
  }
    $this->csSQL = "
SELECT doc_inst.fkContext as context_id, 
       doc_inst.dBeginApprover as begin_approver, 
       doc_inst.dBeginProduction as end_approver 
  FROM view_pm_doc_instance_active doc_inst
    $msJOIN
  WHERE doc_inst.dBeginApprover IS NOT NULL
        AND doc_inst.dBeginProduction IS NOT NULL
        AND doc_inst.dBeginProduction > doc_inst.dBeginApprover
        $msWhere
";
  }
  
  public function executeQuery(){
    parent::executeQuery();
    $miCount = 0;
    while($this->coDataSet->fetch()){
      $miPeriod = ISMSLib::getTimestamp($this->coDataSet->getFieldByAlias("end_approver")->getValue())
                - ISMSLib::getTimestamp($this->coDataSet->getFieldByAlias("begin_approver")->getValue());
      if($miPeriod > 30){ // Desconsidera aprova��es autom�ticas
        $this->ciDocCount = $this->ciDocCount + $miPeriod;
        $miCount++;
      }
    }
    if($miCount){
      $this->ciDocCount = $this->ciDocCount/$miCount;
    }
  }
  
  public function getValues() {
    $this->makeQuery();
    $this->executeQuery();
    
    $miTotal = round($this->ciDocCount);
    
    $miSec = $miTotal%60;
    $miTotal = ($miTotal-$miSec)/60; // em minutos
    
    $miMin = $miTotal%60;
    $miTotal = ($miTotal-$miMin)/60; // em horas
    
    $miHour = $miTotal%24;
    $miTotal = ($miTotal-$miHour)/24; // em dias
    
    $miDay = $miTotal;
    
    $msDay=$msHour=$msMin=$msSec='';
    
    if($miDay){
      if($miDay==1)
        $msDay .= $miDay.' '.FWDLanguage::getPHPStringValue('gs_day','dia').' ';
      else
        $msDay .= $miDay.' '.FWDLanguage::getPHPStringValue('gs_days','dias').' ';
    }
    if($miHour){
      if($miDay==1)
        $msHour .= $miHour.' '.FWDLanguage::getPHPStringValue('gs_hour','hora').' ';  
      else
        $msHour .= $miHour.' '.FWDLanguage::getPHPStringValue('gs_hours','horas').' ';      
    }                
    if($miMin){
      if($miMin==1)
        $msMin .= $miMin.' '.FWDLanguage::getPHPStringValue('gs_minute','minuto').' ';
      else
        $msMin .= $miMin.' '.FWDLanguage::getPHPStringValue('gs_minutes','minutos').' ';
    }
    
    if($miSec){
      if($miSec==1)
        $msSec .= $miSec.' '.FWDLanguage::getPHPStringValue('gs_second','segundo').' ';
      else
        $msSec .= $miSec.' '.FWDLanguage::getPHPStringValue('gs_seconds','segundos').' ';
    }else{
      if(round($this->ciDocCount)){
        $msSec .= $miSec.' '.FWDLanguage::getPHPStringValue('gs_seconds','segundos').' ';
      }else{
        $msSec .= FWDLanguage::getPHPStringValue('gs_not_applicable','NA').' ';
      }
    }
    return $msDay.$msHour.$msMin.$msSec;
  }
}
?>