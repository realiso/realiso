<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryCountAllDocument.
 *
 * <p>Consulta para calcular o n�mero total de documentos no sistema.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryCountAllDocuments extends FWDDBQueryHandler {
  protected $ciDocCount = 0;

  protected $ciUserId = 0;
    
  public function __construct($poDB) {
    parent::__construct($poDB);

    $this->coDataSet->addFWDDBField(new FWDDBField('','num_system_doc',DB_NUMBER));
  }
  
  public function setUserId($piUserId){
    $this->ciUserId = $piUserId;
  }

  public function makeQuery() {
  $msWhere = '';
  if($this->ciUserId)
    $msWhere .= "   WHERE doc.fkMainApprover = ".$this->ciUserId;


    $this->csSQL = "
SELECT count(doc.fkContext) as num_system_doc 
  FROM view_pm_document_active doc
  $msWhere
";
  }
  
  public function executeQuery() {
    parent::executeQuery();
    if($this->coDataSet->fetch()){
      $this->ciDocCount = $this->coDataSet->getFieldByAlias("num_system_doc")->getValue();
    }
  }
  
  public function getValues() {
    $this->makeQuery();
    $this->executeQuery();
    return $this->ciDocCount;
  }
}
?>