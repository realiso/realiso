<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryTop5IncidentsPerProcess.
 *
 * <p>Consulta para popular o grid de top 5 processos com mais incidentes.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryTop5IncidentsPerProcess extends FWDDBQueryHandler {

  protected $caValues = array();

  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField("","process_id", DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("","process_name", DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("","process_inc_count", DB_NUMBER));
  }

  public function makeQuery() {
    $this->csSQL = "SELECT
											p.fkContext as process_id,
											p.sName as process_name,
											count(ip.fkIncident) as process_inc_count
										FROM
											view_rm_process_active p
											JOIN view_ci_inc_process_active ip ON (p.fkContext = ip.fkProcess)
										GROUP BY
											p.fkContext, p.sName
										ORDER BY
											process_inc_count DESC";
  }

  public function executeQuery() {
    parent::executeQuery(5);
    $this->caValues = array();
    while ($this->coDataSet->fetch())
      $this->caValues[$this->coDataSet->getFieldByAlias("process_id")->getValue()] = array($this->coDataSet->getFieldByAlias("process_name")->getValue(), $this->coDataSet->getFieldByAlias("process_inc_count")->getValue());
  }

  public function getValues() {
  	$this->makeQuery();
    $this->executeQuery();
    return $this->caValues;
  }
}
?>