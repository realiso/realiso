<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QuerySummaryRisk.
 *
 * <p>Consulta para popular o grid de sum�rio de risco .</p>
 * @package ISMS
 * @subpackage handlers
 */
class QuerySummaryRisk extends FWDDBQueryHandler {

  protected $caRiskSummary = array();

  protected $ciUserId = 0;

  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField("ordem","ordem", DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("count","count", DB_NUMBER));
 }

  public function setUserId($psUserId) {
    $this->ciUserId = $psUserId;
  }

  public function makeQuery() {
    $this->csSQL = 
"
SELECT 'risk_np' as ordem, count(*) as count
FROM rm_risk r
JOIN isms_context c ON (r.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED." AND (r.nValue = 0 OR r.nValue IS NULL))
  UNION
SELECT 'risk_high' as ordem, count(*) as count
FROM rm_risk r
JOIN isms_context c ON (r.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED." AND r.nValue >= ".ISMSLib::getConfigById(RISK_HIGH).")
  UNION
SELECT 'risk_mid' as ordem, count(*) as count
FROM rm_risk r
JOIN isms_context c ON (r.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED." AND r.nValue > ".ISMSLib::getConfigById(RISK_LOW)." AND r.nValue < ".ISMSLib::getConfigById(RISK_HIGH).")
  UNION
SELECT 'risk_low' as ordem, count(*) as count
FROM rm_risk r
JOIN isms_context c ON (r.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED." AND (r.nValue <= ".ISMSLib::getConfigById(RISK_LOW)." AND r.nValue > 0 ))
  UNION
SELECT 'risk_treated' as ordem, count(*) as count
FROM rm_risk r
JOIN isms_context c ON (r.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED." AND (r.nAcceptMode<>0 OR r.nValue != r.nValueResidual))
  UNION
SELECT 'risk_not_treated' as ordem, count(*) as count
FROM rm_risk r
JOIN isms_context c ON (r.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED." AND (r.nAcceptMode=0 AND r.nValue = r.nValueResidual AND r.nValue > 0))
";
  }

  public function executeQuery() {
    parent::executeQuery();
    $this->caRiskSummary = array();
    while ($this->coDataSet->fetch())
      $this->caRiskSummary[$this->coDataSet->getFieldByAlias("ordem")->getValue()] = $this->coDataSet->getFieldByAlias("count")->getValue();
  }

  public function getRiskSummary() {
    return $this->caRiskSummary;
  }
}
?>