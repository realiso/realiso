<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryDigestUsers.
 *
 * <p>Consulta para retornar os ids e os emails dos usu�rios que t�m emails para
 * receber que ainda n�o foram enviados.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryDigestUsers extends FWDDBQueryHandler {

  public function __construct($poDB) {
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField('u.fkContext','user_id',   DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('u.sEmail',   'user_email',DB_STRING));
 }

  public function makeQuery(){
    $this->csSQL = "SELECT
                      u.fkContext as user_id,
                      u.sEmail as user_email
                    FROM isms_user u
                    WHERE EXISTS (
                      SELECT * FROM wkf_task t
                      WHERE t.bEmailSent = 0
                        AND t.fkReceiver = u.fkContext
                    ) OR EXISTS (
                      SELECT * FROM wkf_alert a
                      WHERE a.bEmailSent = 0
                        AND a.fkReceiver = u.fkContext
                    )";
  }

}
?>