<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryTotalStandards.
 *
 * <p>Consulta para buscar o n�mero de normas cadastradas no sistema.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryTotalStandards extends FWDDBQueryHandler {

  private $ciTotalStandards = 0;

  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField("total_standards","total_standards", DB_NUMBER));
  }

  public function makeQuery() {
    $this->csSQL = "SELECT count(*) as total_standards FROM rm_standard";
 }

  public function executeQuery() {
    parent::executeQuery();
    $this->ciTotalStandards = 0;
    if ($this->coDataSet->fetch())
      $this->ciTotalStandards = $this->coDataSet->getFieldByAlias("total_standards")->getValue();
  }

  public function getTotalStandards() {
    return $this->ciTotalStandards;
  }

}
?>