<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

class QueryCheckPlaceEditResourceSearch extends FWDDBQueryHandler{

	private $selectListResourceName = array();
	private $choice = false;
	private $ciResourceName = '';
	
	public function __construct($poDB){
		parent::__construct($poDB);

		$this->coDataSet->addFWDDBField(new FWDDBField('','resource_id',     		     DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField('','resource_name',     		 	 DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField('','resource_acronym',     		 DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField('','type',     		 	 DB_STRING));
	}

	public function setResourceName($piResourceName){
		$this->ciResourceName = $piResourceName;
	} 
	
	public function makeQuery(){
		$this->csSQL .= "SELECT r.fkContext as resource_id, r.name as resource_name, 
							r.func as resource_acronym, 'resource' as type
						 FROM
							view_cm_resource_active r
						WHERE r.name = '" . $this->ciResourceName . "'";
	}
	
	public function fetchResourceName() {
		parent::executeQuery();
		$this->selectListResourceName = array();
		while ($this->coDataSet->fetch()) {
			$this->selectListResourceName[] = $this->coDataSet->getFieldByAlias("resource_name")->getValue();
		}
	}
	
	public function checkResourceName() {
		if(($this->selectListResourceName != '') && (count($this->selectListResourceName) >= 1)){
			return $this->choice = true;
		}else{
			return $this->choice = false;
		}
	}
}
?>