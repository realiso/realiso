<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QuerySummaryNC.
 *
 * <p>Consulta para popular o grid de sum�rio de n�o conformidades.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QuerySummaryNC extends FWDDBQueryHandler {

  protected $caNCSummary = array();

  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField("","nc_state", DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("","nc_count", DB_NUMBER));
  }

  public function makeQuery() {
    $this->csSQL = "SELECT
											c.nState as nc_state,
											count(nc.fkContext) as nc_count
										FROM
											view_isms_context_active c
											JOIN view_ci_nc_active nc ON (c.pkContext = nc.fkContext)
										GROUP BY
											c.nState";
  }

  public function executeQuery() {
    parent::executeQuery();
    $this->caNCSummary = array();
    while ($this->coDataSet->fetch())
      $this->caNCSummary[$this->coDataSet->getFieldByAlias("nc_state")->getValue()] = $this->coDataSet->getFieldByAlias("nc_count")->getValue();
  }

  public function getNCSummary() {
  	$this->makeQuery();
    $this->executeQuery();
    return $this->caNCSummary;
  }
}
?>