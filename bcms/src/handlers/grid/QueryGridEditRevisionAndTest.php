<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGridEditRevisionAndTest.
 *
 * <p>consulta para buscar os controles que possuem revis�o e / ou teste.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryGridEditRevisionAndTest extends FWDDBQueryHandler {  
  
  protected $csName = "";
  
  public function __construct($poDB) {
    parent::__construct($poDB);  

    

    $this->coDataSet->addFWDDBField(new FWDDBField("fkContext" ,"control_id"     , DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("sName"     ,"control_name"   , DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("bIsActive" ,"control_active" , DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField(""          ,"has_revision"   , DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField(""          ,"has_test"       , DB_NUMBER));
  }

  public function setName($psName){
    $this->csName = $psName ? " AND upper(sName) like upper('%$psName%')" : "";
  }
  
  public function makeQuery() {
    
    $msAnd =  " AND {$this->csName}";
    
    $this->csSQL = "
  SELECT fkContext AS control_id,
         sName AS control_name,
         bIsActive as control_active,
         CASE WHEN ce.fkControlEfficiency IS NOT NULL THEN 1 ELSE 0 END AS has_revision,
         CASE WHEN ct.fkControlTest IS NOT NULL THEN 1 ELSE 0 END AS has_test
    FROM view_rm_control_active c
      LEFT JOIN wkf_control_efficiency ce ON(ce.fkControlEfficiency = c.fkContext)
      LEFT JOIN wkf_control_test ct ON(ct.fkControlTest = c.fkContext)
    WHERE ( ce.fkControlEfficiency IS NOT NULL 
            OR ct.fkControlTest IS NOT NULL )
    " . $this->csName;
  } 
}
?>