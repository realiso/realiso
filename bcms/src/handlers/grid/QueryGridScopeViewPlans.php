<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGridProcessSearch.
 *
 * <p>Consulta para popular o grid de pesquisa de escopos.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryGridScopeViewPlans extends FWDDBQueryHandler {

	private $scopeId = 0;

	public function __construct($poDB){
		parent::__construct($poDB);

		$this->coDataSet->addFWDDBField(new FWDDBField('','planId',         	DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField('','planName',       	DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField('','planDescription',  	DB_STRING));
	}

	public function setScope($scopeId) {
		$this->scopeId = $scopeId;
	}

	public function makeQuery(){
		$this->csSQL = "SELECT
							plan.name as planName,
							plan.fkcontext as planId,
							plan.description as planDescription
						FROM  
							view_cm_plan_active plan 
						WHERE
							plan.fkarea in 
								(select area.fkcontext from view_rm_area_active area join view_cm_scope_area_active sa on sa.fkarea = area.fkcontext where sa.fkscope = {$this->scopeId})
							or plan.fkplace in 
								(select place.fkcontext from view_rm_place_active place join view_cm_scope_place_active sa on sa.fkplace = place.fkcontext where sa.fkscope = {$this->scopeId})
							or plan.fkprocess in 
								(select process.fkcontext from view_rm_process_active process join view_cm_scope_process_active sa on sa.fkprocess = process.fkcontext where sa.fkscope = {$this->scopeId})
							or plan.fkasset in 
								(select asset.fkcontext from view_rm_asset_active asset join view_cm_scope_asset_active sa on sa.fkasset = asset.fkcontext where sa.fkscope = {$this->scopeId})
						GROUP BY
							1,2,3";
	}

}
?>