<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

class QueryGridPlan extends FWDDBQueryHandler {

	protected $process = 0;
	protected $asset = 0;
	protected $area = 0;
	protected $place = 0;

	public function __construct($poDB=null){
		parent::__construct($poDB);
		$this->coDataSet->addFWDDBField(new FWDDBField(''  				,'id'              	,DB_NUMBER  ));
		$this->coDataSet->addFWDDBField(new FWDDBField(''      			,'name'            	,DB_STRING  ));
		$this->coDataSet->addFWDDBField(new FWDDBField(''               ,'description'		,DB_STRING	));
		$this->coDataSet->addFWDDBField(new FWDDBField(''               ,'groups'			,DB_NUMBER  ));
		$this->coDataSet->addFWDDBField(new FWDDBField(''               ,'actions'	      	,DB_NUMBER  ));
		$this->coDataSet->addFWDDBField(new FWDDBField(''               ,'origin'	      	,DB_STRING  ));
		$this->coDataSet->addFWDDBField(new FWDDBField(''               ,'nextTest'	      	,DB_DATETIME  ));
	}

	public function setProcess($process){
		$this->process = $process;
	}

	public function setArea($area){
		$this->area = $area;
	}

	public function setAsset($asset){
		$this->asset = $asset;
	}

	public function setPlace($place){
		$this->place = $place;
	}

	public function makeQuery(){

		//$this->getDataset()->setOrderBy("name","+");
		
		$where = "";
		
		if($this->place){
			$where = " and p.fkplace = {$this->place}";
		} else if($this->process){
			$where = " and p.fkprocess = {$this->process}";
		} else if($this->asset){
			$where = " and p.fkasset = {$this->asset}";
		} else if($this->area){
			$where = " and p.fkarea = {$this->area}";
		} 

		$this->csSQL = "select
							p.fkcontext as id,
							p.name as name, 
							p.description as description,
							count(pa.fkcontext) as actions, 
							count(distinct g.fkcontext) as groups,
							case when p.fkprocess is not null then 'process' else 
								case when p.fkplace is not null then 'place' else 
									case when p.fkarea is not null then 'area' else 
										case when p.fkasset is not null then 'asset' else 
										'other' 
										end
									end
								end
							end as origin,
							min(schedule.date) as nextTest
						from 
							view_cm_plan_active p
							LEFT JOIN view_cm_plan_action_active pa on pa.fkplan = p.fkcontext
							LEFT JOIN view_cm_group_active g on g.fkcontext = pa.fkgroup
							LEFT JOIN view_cm_plan_schedule_active schedule on schedule.fkplan = p.fkcontext and schedule.date > current_timestamp
							where 1=1 {$where}
						group by
							1,2,3,p.fkprocess,p.fkplace,p.fkasset,p.fkarea";
	}
}
?>