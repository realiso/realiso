<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGridRisk.
 *
 * <p>Consulta para popular a grid de riscos.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryGridRisk extends FWDDBQueryHandler {

  protected $ciAsset = 0;

  protected $ciControl = 0;

  protected $ciUserId = 0;

  protected $csName = '';

  protected $csNameDescription = '';

  protected $ciMaxControls = 0;

  protected $ciMinControls = 0;

  protected $caTreatmentTypes = array();

  protected $ciMaxRiskValue = 0;

  protected $ciMinRiskValue = 0;

  protected $ciMaxResidualValue = 0;

  protected $ciMinResidualValue = 0;

  protected $ciState = 0;
  
  protected $ciIncident = 0;
  
  public function __construct($poDB=null){
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField('r.fkContext'             ,'risk_id'             ,DB_NUMBER  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('r.fkProbabilityValueName','risk_prob'           ,DB_NUMBER  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('r.sName'                 ,'risk_name'           ,DB_STRING  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('c.nState'                ,'risk_state'          ,DB_NUMBER  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('r.nValue'                ,'risk_value'          ,DB_NUMBER  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('r.nValueResidual'        ,'risk_residual'       ,DB_NUMBER  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('a.sName'                 ,'risk_asset_name'     ,DB_STRING  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('r.nAcceptState'          ,'risk_accept_state'   ,DB_NUMBER  ));
    $this->coDataSet->addFWDDBField(new FWDDBField(''                        ,'risk_control_count'  ,DB_NUMBER  ));
    $this->coDataSet->addFWDDBField(new FWDDBField(''                        ,'user_create_name'    ,DB_STRING  ));
    $this->coDataSet->addFWDDBField(new FWDDBField(''                        ,'date_create'         ,DB_DATETIME));
    $this->coDataSet->addFWDDBField(new FWDDBField(''                        ,'user_edit_name'      ,DB_STRING  ));
    $this->coDataSet->addFWDDBField(new FWDDBField(''                        ,'date_edit'           ,DB_DATETIME));
    $this->coDataSet->addFWDDBField(new FWDDBField('r.nAcceptMode'           ,'risk_accept_mode'    ,DB_NUMBER  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('a.fkSecurityResponsible' ,'risk_responsible_id' ,DB_NUMBER  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('a.fkResponsible'         ,'asset_responsible_id',DB_NUMBER  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('a.fkContext'             ,'asset_id'            ,DB_NUMBER  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('r.tImpact'               ,'risk_impact'         ,DB_STRING  ));
  }

  public function setAsset($piAsset){
    $this->ciAsset = $piAsset;
  }

  public function setControl($piControl){
    $this->ciControl = $piControl;
  }

  public function setUserId($piUserId){
    $this->ciUserId = $piUserId;
  }

  public function setName($psName){
    $this->csName = $psName;
  }

  public function setNameDescription($psNameDescription){
    $this->csNameDescription = $psNameDescription;
  }

  public function setMaxControls($piMaxControls){
    $this->ciMaxControls = $piMaxControls;
  }

  public function setMinControls($piMinControls){
    $this->ciMinControls = $piMinControls;
  }

  public function setTreatmentTypes($paTreatmentTypes){
    $this->caTreatmentTypes = $paTreatmentTypes;
  }

  public function setMaxRiskValue($piMaxRiskValue){
    $this->ciMaxRiskValue = $piMaxRiskValue;
  }

  public function setMinRiskValue($piMinRiskValue){
    $this->ciMinRiskValue = $piMinRiskValue;
  }

  public function setMaxResidualValue($piMaxResidualValue){
    $this->ciMaxResidualValue = $piMaxResidualValue;
  }

  public function setMinResidualValue($piMinResidualValue){
    $this->ciMinResidualValue = $piMinResidualValue;
  }

  public function setIncident($piIncident){
    $this->ciIncident = $piIncident;
  }

  public function setState($piState){
    $this->ciState = $piState;
  }

  public function makeQuery(){
    $maFilters = array();
    $maHavingFilters = array();
    
    if($this->ciAsset){
      $maFilters[] = "a.fkContext = {$this->ciAsset}";
    }
    
    if($this->ciControl){
      $msLeftJoin = '';
      $maFilters[] = "rc.fkControl = {$this->ciControl}";
    }else{
      $msLeftJoin = 'LEFT';
    }
    
    if($this->ciUserId){
      /*
        ok  Todos os riscos que est�o relacionados aos controles em que o usu�rio � respons�vel
        ok  Todos os riscos dos ativos em que � respons�vel
        ok  Todos os riscos dos ativos em que � respons�vel pela seguran�a do ativo
        ok  Todos os riscos dos ativos relacionados aos processos em que � respons�vel
        ok  Todos os riscos dos ativos relacionados aos processos das �reas em que � respons�vel,
        ok  Todos os riscos dos ativos relacionados aos processos das sub-�reas das �reas em que � respons�vel
      */
      $msFunctionCall = FWDWebLib::getFunctionCall("get_areas_and_subareas_by_user({$this->ciUserId})");
      $maFilters[] = "(
                        a.fkResponsible = {$this->ciUserId}
                        OR a.fkSecurityResponsible = {$this->ciUserId}
                        OR a.fkContext IN (
                          SELECT p_a.fkAsset
                          FROM view_rm_process_asset_active p_a 
                          JOIN view_rm_process_active p ON (p.fkContext = p_a.fkProcess)
                          WHERE  p.fkArea IN (SELECT area_id FROM {$msFunctionCall})
                                 OR p.fkResponsible = {$this->ciUserId}
                        )
                        OR r.fkContext IN (
                          SELECT r_c.fkRisk
                            FROM view_rm_risk_control_active r_c
                            JOIN view_rm_control_active c_aux ON (c_aux.fkContext = r_c.fkControl AND c_aux.fkResponsible = {$this->ciUserId})
                        )
                      )";
    }
    
    if($this->csName){
      $maFilters[] = FWDWebLib::getCaseInsensitiveLike('r.sName',$this->csName);
    }
    
    if($this->csNameDescription){
      $maFilters[] = '('
                        .FWDWebLib::getCaseInsensitiveLike('r.sName',$this->csNameDescription)
                        .' OR '
                        .FWDWebLib::getCaseInsensitiveLike('r.tDescription',$this->csNameDescription)
                    .')';
    }
    
    if($this->ciMaxControls){
      $maHavingFilters[] = "COUNT(rc.fkContext) <= {$this->ciMaxControls}";
    }
    
    if($this->ciMinControls){
      $maHavingFilters[] = "COUNT(rc.fkContext) >= {$this->ciMinControls}";
    }
    
    $miCountTreatmentTypes = count($this->caTreatmentTypes);
    if($miCountTreatmentTypes > 0 && $miCountTreatmentTypes < 5){
      $maTreatmentTypeFilters = array();
      foreach($this->caTreatmentTypes as $msTreatmentType){
        switch($msTreatmentType){
          case 'reduced':{
            $maTreatmentTypeFilters[] = 'EXISTS (SELECT * FROM view_rm_risk_control_active WHERE fkRisk = r.fkContext)';
            break;
          }
          case 'restrained':{
            $maTreatmentTypeFilters[] = 'r.nAcceptMode = '.RISK_ACCEPT_MODE_ACCEPT;
            break;
          }
          case 'transferred':{
            $maTreatmentTypeFilters[] = 'r.nAcceptMode = '.RISK_ACCEPT_MODE_TRANSFER;
            break;
          }
          case 'avoided':{
            $maTreatmentTypeFilters[] = 'r.nAcceptMode = '.RISK_ACCEPT_MODE_AVOID;
            break;
          }
          case 'not_treated':{
            $maTreatmentTypeFilters[] = '(r.nAcceptMode = 0 AND NOT EXISTS (SELECT * FROM view_rm_risk_control_active WHERE fkRisk = r.fkContext))';
            break;
          }
        }
      }
      $maFilters[] = '('.implode(' OR ',$maTreatmentTypeFilters).')';
    }
    
    if($this->ciMaxRiskValue){
      $maFilters[] = "r.nValue <= {$this->ciMaxRiskValue}";
    }
    
    if($this->ciMinRiskValue){
      $maFilters[] = "r.nValue >= {$this->ciMinRiskValue}";
    }
    
    if($this->ciMaxResidualValue){
      $maFilters[] = "r.nValueResidual <= {$this->ciMaxResidualValue}";
    }
    
    if($this->ciMinResidualValue){
      $maFilters[] = "r.nValueResidual >= {$this->ciMinResidualValue}";
    }
    
    if($this->ciState){
      $maFilters[] = "c.nState = {$this->ciState}";
    }
    
    $msIncidentJoin = '';
    if($this->ciIncident){
      $msIncidentJoin .= ' JOIN ci_incident_risk ir ON ( ir.fkRisk = r.fkContext AND ir.fkIncident = '. $this->ciIncident . ')';
      
    }
    
    if(count($maFilters)==0){
      $msWhere = '';
    }else{
      $msWhere = ' WHERE '.implode(' AND ',$maFilters);
    }
    if(count($maHavingFilters)==0){
      $msHaving = '';
    }else{
      $msHaving = ' HAVING '.implode(' AND ',$maHavingFilters);
    }
    $this->csSQL = "SELECT
                      r.fkContext AS risk_id,
                      r.fkProbabilityValueName AS risk_prob,
                      r.sName AS risk_name,
                      c.nState AS risk_state,
                      r.nValue AS risk_value,
                      r.nValueResidual AS risk_residual,
                      a.sName AS risk_asset_name,
                      r.nAcceptState AS risk_accept_state,
                      COUNT(rc.fkContext) AS risk_control_count,
                      ch.context_creator_name AS user_create_name,
                      ch.context_date_created AS date_create,
                      ch.context_modifier_name AS user_edit_name,
                      ch.context_date_modified AS date_edit,
                      r.nAcceptMode AS risk_accept_mode,
                      a.fkSecurityResponsible AS risk_responsible_id,
                      a.fkResponsible AS asset_responsible_id,
                      a.fkContext as asset_id,
                      r.tImpact as risk_impact
                    FROM
                      isms_context c
                      JOIN rm_risk r ON (r.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED.")
                      JOIN rm_asset a ON (a.fkContext = r.fkAsset)
                      JOIN context_history ch ON (ch.context_id = c.pkContext)
                      {$msLeftJoin} JOIN view_rm_risk_control_active rc ON (r.fkContext = rc.fkRisk)
                      $msIncidentJoin
                    {$msWhere}
                    GROUP BY
                      r.fkContext,
                      r.fkProbabilityValueName,
                      r.sName,
                      c.nState,
                      r.nValue,
                      r.nValueResidual,
                      a.sName,
                      r.nAcceptState,
                      ch.context_creator_name,
                      ch.context_date_created,
                      ch.context_modifier_name,
                      ch.context_date_modified,
                      r.nAcceptMode,
                      a.fkSecurityResponsible,
                      a.fkResponsible,
                      a.fkContext,
                      r.tImpact
                    {$msHaving}";
  }
}

?>