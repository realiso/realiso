<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGridPendantTasks.
 *
 * <p>Consulta para popular tarefas pendentes.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryGridPendantTasks extends FWDDBQueryHandler {
	
  public function __construct($poDB){
    parent::__construct($poDB);    
    
		$this->coDataSet->addFWDDBField(new FWDDBField("","task_activity",DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("","task_count",DB_NUMBER));
  }
  
  public function makeQuery(){
  	$miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    
    $moConfig = new ISMSConfig();
    $mbHasRevision = $moConfig->getConfig(GENERAL_REVISION_ENABLED);
    $mbHasTests = $moConfig->getConfig(GENERAL_TEST_ENABLED);
    $msWhere = ' AND wt.nActivity >= 2201 AND wt.nActivity <= 2220 ';
    $maFilterTypes = array();        
    if(!$mbHasRevision){
      $maFilterTypes[]= '2208';
    }
    if(!$mbHasTests){
        $maFilterTypes[] = '2216';
    }

    if(count($maFilterTypes)){
      $msWhere = "AND wt.nActivity NOT IN (".implode(',',$maFilterTypes).") ";
    }    
    
    $this->csSQL = "SELECT wt.nActivity as task_activity, count(wt.nActivity) as task_count
											FROM isms_context cont
											JOIN isms_user u ON (cont.pkContext = u.fkContext AND cont.nState <> " . CONTEXT_STATE_DELETED . ")
											JOIN wkf_task wt ON (u.fkContext = wt.fkReceiver AND wt.bVisible = 1)
											WHERE u.fkContext = $miUserId
                      $msWhere		
											GROUP BY u.fkContext, u.sName, wt.nActivity
											ORDER BY u.fkContext";		
 	}
}
?>