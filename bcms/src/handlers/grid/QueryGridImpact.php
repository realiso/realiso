<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGridImpact.
 *
 * <p>Consulta para popular a grid de impactos de cen�rios.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryGridImpact extends FWDDBQueryHandler {

	protected $ciMaxImpacts = 0;

	protected $ciMinImpacts = 0;

	protected $ciScene = 0;

	public function __construct($poDB=null){
		parent::__construct($poDB);
		$this->coDataSet->addFWDDBField(new FWDDBField('i.fkContext'    ,'impact_id'              ,DB_NUMBER  ));
		$this->coDataSet->addFWDDBField(new FWDDBField('i.sName'        ,'impact_name'            ,DB_STRING  ));
		$this->coDataSet->addFWDDBField(new FWDDBField(''        		,'space0'            	  ,DB_STRING  ));
		$this->coDataSet->addFWDDBField(new FWDDBField(''        		,'space1'           	  ,DB_STRING  ));
		$this->coDataSet->addFWDDBField(new FWDDBField(''        		,'space2'           	  ,DB_STRING  ));
		$this->coDataSet->addFWDDBField(new FWDDBField(''        		,'space3'            	  ,DB_STRING  ));
		$this->coDataSet->addFWDDBField(new FWDDBField(''        		,'space4'           	  ,DB_STRING  ));
		$this->coDataSet->addFWDDBField(new FWDDBField(''        		,'space5'           	  ,DB_STRING  ));
		$this->coDataSet->addFWDDBField(new FWDDBField(''        		,'space6'           	  ,DB_STRING  ));			
		$this->coDataSet->addFWDDBField(new FWDDBField(''               ,'impact_ranking'	      ,DB_NUMBER  ));
		$this->coDataSet->addFWDDBField(new FWDDBField(''               ,'financial_loss'	      ,DB_NUMBER  ));
		$this->coDataSet->addFWDDBField(new FWDDBField('it.sname'       ,'impact_type'            ,DB_STRING  ));
	}


	public function setMaxImpacts($piMaxImpacts){
		$this->ciMaxImpacts = $piMaxImpacts;
	}

	public function setMinImpacts($piMinImpacts){
		$this->ciMinImpacts = $piMinImpacts;
	}

	public function setScene($piSceneId) {
		$this->ciScene = $piSceneId;
	}

	public function makeQuery(){
		$maHavingFilters = array();

		if($this->ciMaxImpacts){
			$maHavingFilters[] = "COUNT(i.fkcontext) <= {$this->ciMaxImpacts}";
		}

		if($this->ciMinImpacts){
			$maHavingFilters[] = "COUNT(i.fkcontext) >= {$this->ciMinImpacts}";
		}

		if(count($maHavingFilters)==0){
			$msHaving = '';
		}else{
			$msHaving = ' HAVING '.implode(' AND ',$maHavingFilters);
		}

		$this->getDataset()->setOrderBy("impact_type","+");
		$this->getDataset()->setOrderBy("impact_ranking","-");

		$this->csSQL = "SELECT
                      id.fkContext AS impact_id,
                      i.sName AS impact_name,
                      '' as space0,
                      '' as space1,
                      '' as space2,
                      '' as space3,
                      '' as space4,
                      '' as space5,
                      '' as space6,


					  coalesce(ranking.weight,0) as impact_ranking,
					  ranking.financialloss as financial_loss,
                      it.sName AS impact_type
                    FROM
                      view_cm_impact_active i 
                      JOIN cm_impact_type it ON (it.fkcontext = i.ntype)
                      JOIN cm_impact_damage id on id.fkimpact = i.fkcontext
                      LEFT JOIN view_impact_ranking ranking on ranking.fkcontext = id.fkcontext 
                    WHERE id.fkscene = {$this->ciScene}                         
                    GROUP BY
                      id.fkContext,
                      i.sName,
                      it.sName,
                      ranking.weight,
                      ranking.financialloss
                      {$msHaving}";


	}

}

?>