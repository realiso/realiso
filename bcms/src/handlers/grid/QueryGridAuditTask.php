<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGridAuditTask.
 *
 * <p>Consulta para popular o grid de auditoria de tarefas.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryGridAuditTask extends FWDDBQueryHandler {
	
	protected $ciCreatorId;
	protected $ciReceiverId;
	protected $ciActivityId;
	protected $ciCreatedDateStart;
	protected $ciCreatedDateEnd;
	
	public function __construct($poDB) {
		parent::__construct($poDB);	

		$this->coDataSet->addFWDDBField(new FWDDBField("t.dDateCreated","task_creation_date", DB_DATETIME));
		$this->coDataSet->addFWDDBField(new FWDDBField("t.dDateAccomplished","task_accomplished_date", DB_DATETIME));
		$this->coDataSet->addFWDDBField(new FWDDBField("uc.sName","task_creator", DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField("ur.sName","task_receiver", DB_STRING));		
		$this->coDataSet->addFWDDBField(new FWDDBField("cn.context_name","task_context_name", DB_STRING));		
		$this->coDataSet->addFWDDBField(new FWDDBField("t.nActivity","task_activity", DB_NUMBER));
	}	
	
	public function setCreator($piCreatorId) {
		$this->ciCreatorId = $piCreatorId;
	}
	
	public function setReceiver($piReceiverId) {
		$this->ciReceiverId = $piReceiverId;
	}
	
	public function setActivity($piActivityId) {
		$this->ciActivityId = $piActivityId;
	}
	
	public function setCreatedDateStart($piCreatedDateStart) {
		$this->ciCreatedDateStart = $piCreatedDateStart;
	}
	
	public function setCreatedDateEnd($piCreatedDateEnd) {
		$this->ciCreatedDateEnd = $piCreatedDateEnd;
	}
	
	public function makeQuery() {
    $maFilter = array();
    
    if($this->ciCreatorId){
      $maFilter[] = " t.fkCreator = ".$this->ciCreatorId;
    }
    if($this->ciReceiverId){
      $maFilter[]= " t.fkReceiver = ".$this->ciReceiverId;
    }
    if($this->ciActivityId){
      $maFilter[]= " t.nActivity = ".$this->ciActivityId;
    }
    if($this->ciCreatedDateStart){
      $maFilter[]= " t.dDateCreated >= ".ISMSLib::getTimestampFormat($this->ciCreatedDateStart);
    }
    if($this->ciCreatedDateEnd){
      $maFilter[]= " t.dDateCreated <= ".ISMSLib::getTimestampFormat($this->ciCreatedDateEnd);
    }
    
    $moConfig = new ISMSConfig();
    $maFilterTypes = array();        
    if(!$moConfig->getConfig(GENERAL_REVISION_ENABLED)){
      $maFilterTypes[]= '2208';
      $maFilterTypes[]=  ACT_INC_CONTROL_INDUCTION;
    }
    if(!$moConfig->getConfig(GENERAL_TEST_ENABLED)){
        $maFilterTypes[] = '2216';
    }

    //sistema sem o m�dulo de documenta��o
    if(!FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(POLICY_MODE)){
        $maFilterTypes[] = '2221';
        $maFilterTypes[] = '2222';
    }
    if(count($maFilterTypes)){
      $maFilter[] = " t.nActivity NOT IN (".implode(',',$maFilterTypes).") ";
    }    
    if(count($maFilter)){
      $msWhere = " WHERE ".implode(" AND ",$maFilter);
    }else{
      $msWhere = "";
    }
    
		$this->csSQL = "SELECT t.dDateCreated as task_creation_date, t.dDateAccomplished as task_accomplished_date,
							uc.sName as task_creator, ur.sName as task_receiver, cn.context_name as task_context_name,
							t.nActivity as task_activity
						FROM wkf_task t
						JOIN isms_context c ON (t.fkContext = c.pkContext)
						JOIN context_names cn ON (t.fkContext = cn.context_id)
						JOIN isms_user uc ON (t.fkCreator = uc.fkContext)
						JOIN isms_user ur ON (t.fkReceiver = ur.fkContext) "
						.$msWhere;
	} 
	
}
?>