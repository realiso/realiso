<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe QueryGridAssociatedRegisters
 *
 * <p>Consulta para popular o grid de registros associados a um documento.</p>
 * @package ISMS
 * @subpackage classes
 */
class QueryGridAssociatedRegisters extends FWDDBQueryHandler {

  protected $ciDocumentId;

  public function __construct($poDB){
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField("r.fkContext","register_id", DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("r.fkClassification","register_classification", DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("r.sName","register_name", DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("r.nPeriod","register_period", DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("r.nValue","register_value", DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("r.sStoragePlace","register_storage_place", DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("r.sStorageType","register_storage_type", DB_STRING));
  }

 /**
  * M�todo para especificar o id do documento utilizado para filtrar os registros.
  * 
  * <p>Ao setar o id do documento, os registros exibidos ser�o apenas aqueles
  * associados a este documento</p>
  * 
  * @access public
  * @param integer piDocumentId Id do documento
  */
  public function setDocumentId($piDocumentId){
    $this->ciDocumentId = $piDocumentId;
  }

  public function makeQuery(){
    if($this->ciDocumentId){
      $this->csSQL = "SELECT
                        r.fkContext AS register_id,
                        r.fkClassification AS register_classification,
                        r.sName AS register_name,
                        r.nValue AS register_value,
                        r.sStoragePlace AS register_storage_place,
                        r.sStorageType AS register_storage_type,
                        r.nPeriod AS register_period
                      FROM
                        view_pm_register_active r
                        JOIN pm_doc_registers dr ON (
                          dr.fkDocument = {$this->ciDocumentId}
                          AND dr.fkRegister = r.fkContext
                        )";
    }else{
      trigger_error("You must specify a document.",E_USER_ERROR);
    }
  }

}

?>