<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGridCurrentUsers.
 *
 * <p>Consulta para popular a grid de templates de documentos da biblioteca do isms.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryGridDocumentTemplate extends FWDDBQueryHandler {

  protected $ciContextType;
  
  protected $ciState;
  
  protected $ciBestPractice = 0;

  public function __construct($poDB){
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField("fkContext" ,"context_id"        ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("sName"     ,"context_name"      ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("sFileName" ,"context_file_name" ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("nType"     ,"context_type"      ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("nState"    ,"context_state"     ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("sPaph"     ,"file_path"         ,DB_STRING));
    
    $this->coDataSet->addFWDDBField(new FWDDBField("uc.sName"  ,"user_create_name"  , DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("cc.dDate"  ,"date_create"       , DB_DATETIME));
    $this->coDataSet->addFWDDBField(new FWDDBField("ud.sName"  ,"user_edit_name"    , DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("ce.dDate"  ,"date_edit"         , DB_DATETIME));
  }

  public function setState($piState){
    $this->ciState = $piState;
  }

  public function setContextType($piContextType){
    $this->ciContextType = $piContextType;
  }

  public function setBestPracticeFilter($piId = 0){
    $this->ciBestPractice = $piId;
  }
  public function makeQuery(){
    $maFilters = array();
    $maFilters[] = '';
    if($this->ciContextType)
      $maFilters[] = "t.nContextType = $this->ciContextType";
    if($this->ciState)
      $maFilters[] = "c.nState = $this->ciState";
      
    $msBestPracticeFilter = "";
    if($this->ciBestPractice){
      $msBestPracticeFilter .="
        JOIN pm_template_best_practice tbp ON (t.fkContext = tbp.fkTemplate AND tbp.fkBestPractice = $this->ciBestPractice)
      ";
    }
    $this->csSQL = "
SELECT t.fkContext as context_id,
       t.sName as context_name,
       t.sFileName as context_file_name,
       t.nContextType as context_type,
       c.nState as context_state,
       ch.context_creator_name as user_create_name, 
       ch.context_date_created as date_create, 
       ch.context_modifier_name as user_edit_name, 
       ch.context_date_modified as date_edit,
       t.sPath as file_path
  FROM view_pm_template_active t
  JOIN isms_context c ON (c.pkcontext = t.fkContext ".implode(' AND ',$maFilters)." )
  JOIN context_history ch ON (c.pkContext = ch.context_id)
  $msBestPracticeFilter
";
  }

}
?>