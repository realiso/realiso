<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
 
/**
 * Classe QueryGridContextNames
 *
 * <p>Consulta para popular o grid de nomes de contextos </p>
 * @package ISMS
 * @subpackage classes
 */
class QueryGridContextNames extends FWDDBQueryHandler {
  
  protected $csSearchString="";
  protected $ciContextType;
  protected $caExcludedContexts=array();
  protected $caContextIds=array();
  protected $cbCurrentOnly=false;
  
  public function __construct($poDB){
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField("context_id","context_id", DB_NUMBER));    
    $this->coDataSet->addFWDDBField(new FWDDBField("context_name","context_name", DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("context_type","context_type", DB_NUMBER));
  }

  public function makeQuery(){
    $maFilters=array();
    if ($this->csSearchString) {
      $maFilters[] = "context_name like '%{$this->csSearchString}%'";
    }
    if ($this->ciContextType) {
      $maFilters[] = "context_type = {$this->ciContextType}"; 
    }
    if (count($this->caExcludedContexts)) {
      $maFilters[] = "context_id NOT IN (".implode(",",$this->caExcludedContexts).")";
    }
    if (count($this->caContextIds)) {
      $maFilters[] = "context_id IN (".implode(",",$this->caContextIds).")";
    } elseif ($this->cbCurrentOnly) {
      $maFilters[] = "context_id IN (0)";
    }
    
    $msWhere = count($maFilters)?"WHERE ".implode(" AND ",$maFilters):"";
    
    $msUnion="";    
    if ($this->ciContextType == CONTEXT_SCOPE) {
      $msUnion .= " UNION ALL SELECT fkContext as context_id, 
                    ".CONTEXT_SCOPE." as context_type,
                    '".ISMSContextObject::getContextObject(CONTEXT_SCOPE)->getLabel()."' as context_name
                    FROM isms_scope";
    }
    if ($this->ciContextType == CONTEXT_POLICY) {
      $msUnion .= " UNION ALL SELECT fkContext as context_id, 
                    ".CONTEXT_POLICY." as context_type,
                    '".ISMSContextObject::getContextObject(CONTEXT_POLICY)->getLabel()."' as context_name
                    FROM isms_policy";
    }
    
    $this->csSQL = "SELECT context_id, context_type, context_name
                    FROM (
                      SELECT context_id, context_type, context_name
                      FROM context_names c
                      $msUnion
                    ) res
                    JOIN isms_context ctx_ext ON (ctx_ext.pkContext = res.context_id AND ctx_ext.nState <> " . CONTEXT_STATE_DELETED . ")
                    $msWhere
            ";
  }
  
  public function setContextType($piTypes) {
    $this->ciContextType = $piTypes;
  }
  
  public function setSearchString($psSearch) {
    $this->csSearchString = $psSearch;
  }
  
  public function setExcludedContexts($paContexts) { 
    $this->caExcludedContexts = $paContexts;
  }
  
  public function setContextIds($paContextIds) {
    $this->caContextIds = $paContextIds;
    foreach($paContextIds as $miContextId) {
      $moContext = new ISMSContextObject();
      $moContext->fetchById($miContextId);
      $miContextType = $moContext->getFieldValue('context_type');
      if ($this->ciContextType == 0){
        $this->ciContextType = $miContextType;
      } elseif($this->ciContextType != $miContextType) {
         trigger_error("Invalid operation! Contexts of different types have been passed.",E_USER_ERROR);
      }
    }
  }
  
  public function setCurrentOnly($pbCurrentOnly) {
    $this->cbCurrentOnly = $pbCurrentOnly;
  }
}
?>