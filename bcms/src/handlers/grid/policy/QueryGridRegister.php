<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe QueryGridRegister
 *
 * <p>Consulta para popular o grid de Registros</p>
 * @package ISMS
 * @subpackage classes
 */
class QueryGridRegister extends FWDDBQueryHandler {

  protected $caExcludedRegisters=array();
  protected $ciRegisterClassification;


  public function __construct($poDB){
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField('r.fkContext'       ,'register_id'            ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('r.sName'           ,'register_name'          ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('r.nValue'          ,'register_value'         ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('r.sStoragePlace'   ,'register_storage_place' ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('r.sStorageType'    ,'register_storage_type'  ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('r.fkClassification','register_classification',DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('r.nPeriod'         ,'register_period'        ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('r.fkResponsible'   ,'register_responsible'   ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('d.fkContext'       ,'approved_document_id'   ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField(''                  ,'can_edit'               ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField(''                  ,'can_read'               ,DB_NUMBER));
  }

  public function makeQuery(){
    $maFilters = array();
    if($this->ciRegisterClassification){
      $maFilters[] = "r.fkClassification = ".$this->ciRegisterClassification;
    }

    if (count($this->caExcludedRegisters)) {
      $maFilters[] = "r.fkContext NOT IN (".implode(',',$this->caExcludedRegisters).")"; 
    }
    
    $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    
    $maFilters[] = "((is_not_reader IS NULL AND ( is_reader IS NOT NULL OR count_pub IS NOT NULL ))" //can read
                  ." OR "
                  ."(dr_dev.count_not_dev IS NULL AND r.fkResponsible = $miUserId))"; // can_edit
    
    $this->csSQL = "SELECT
                      r.fkContext AS register_id,
                      r.sName AS register_name,
                      r.nValue AS register_value,
                      r.sStoragePlace AS register_storage_place,
                      r.sStorageType AS register_storage_type,
                      r.fkClassification AS register_classification,
                      r.nPeriod AS register_period,
                      r.fkResponsible AS register_responsible,
                      doc.fkContext as approved_document_id,
                      CASE WHEN dr_dev.count_not_dev IS NULL THEN 1 ELSE 0 END AS can_edit,
                      CASE WHEN is_not_reader IS NULL AND (is_reader IS NOT NULL OR count_pub IS NOT NULL) THEN 1 ELSE 0 END AS can_read
                    FROM
                      pm_register r
                      JOIN isms_context c ON (r.fkContext=c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED.")
                      LEFT JOIN pm_document doc ON (r.fkDocument = doc.fkContext AND doc.bHasApproved=1) 
                      LEFT JOIN (
                        "./*registros com contador de documentos associados que n�o est�o em desenvolvimento*/"
                        SELECT dr.fkRegister, count(dr.fkDocument) AS count_not_dev
                        FROM
                          pm_doc_registers dr
                          LEFT JOIN isms_context dc ON (dr.fkDocument = dc.pkContext)
                        WHERE dc.nState != ".CONTEXT_STATE_DOC_DEVELOPING."
                        GROUP BY dr.fkRegister
                      ) dr_dev ON (dr_dev.fkRegister = r.fkContext)
                      LEFT JOIN (
                        "./*registros com contador de documentos associados que o usu�rio pode ler*/"
                        SELECT r.fkContext AS reg_id, count(pd.readable_doc_id) AS count_pub
                        FROM
                          pm_register r
                          JOIN pm_doc_registers dr ON (r.fkContext = dr.fkRegister)
                          JOIN (
                            "./*documentos que o usu�rio pode ler*/"
                            SELECT d.fkContext AS readable_doc_id, d.sName AS doc_name
                            FROM
                              pm_document d
                              JOIN isms_context c ON (c.pkContext = d.fkContext AND c.nState = ".CONTEXT_STATE_DOC_APPROVED.")
                            WHERE
                              d.fkMainApprover = $miUserId
                              OR (
                                d.dDateProduction <= '".date('Y-m-d')."'
                                AND (
                                  d.fkAuthor = $miUserId
                                  OR exists(
                                    SELECT *
                                    FROM pm_doc_approvers da
                                    WHERE da.fkDocument = d.fkContext
                                      AND da.fkUser = $miUserId
                                  )
                                  OR exists(
                                    SELECT *
                                    FROM pm_doc_readers dr
                                    WHERE dr.fkDocument = d.fkContext
                                      AND dr.fkUser = $miUserId
                                      AND dr.bDenied = 0
                                  )
                                )
                              )
                          ) pd ON (dr.fkDocument = pd.readable_doc_id)
                        GROUP BY r.fkContext
                      ) dr_pub ON (r.fkContext = dr_pub.reg_id)
                      LEFT JOIN (
                        SELECT fkregister as reg_id, fkUser as is_reader
                        FROM pm_register_readers
                        WHERE fkUser = $miUserId
                          AND bDenied = 0
                      ) rr ON (rr.reg_id = r.fkContext)
                      LEFT JOIN (
                        SELECT fkregister as reg_id, fkUser as is_not_reader
                        FROM pm_register_readers
                        WHERE fkUser = $miUserId
                          AND bDenied = 1
                      ) rr2 ON (rr2.reg_id = r.fkContext)
                    WHERE ".implode(' AND ',$maFilters);
  }
  
  /**
   * M�todo para excluir registros do resultado.
   * 
   * <p>Os ids de registros passados para esse m�todo n�o aparecer�o
   * no resultado.</p>
   * 
   * @access public
   * @param array paExcludedRegisters Array contendo ids dos registros que n�o devem ser exibidos
   */
  public function setExcludedRegisters($paExcludedRegisters) {
    $this->caExcludedRegisters = $paExcludedRegisters;
  }
  
  /**
   * M�todo para filtrar os registros quanto � classifica��o.
   * 
   * <p>M�todo para filtrar os registros quanto � classifica��o.</p>
   * 
   * @access public
   * @param integer piClassification Id da classifica��o 
   */
  public function setRegisterClassification($piClassification) {
    $this->ciRegisterClassification = $piClassification;
  }

}

?>