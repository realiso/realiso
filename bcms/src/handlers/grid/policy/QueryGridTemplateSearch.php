<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe QueryGridTemplateSearch. 
 *
 * <p>Consulta para popular o grid de busca de templates (documentos de melhores pr�ticas).</p>
 * @package ISMS
 * @subpackage classes
 */
class QueryGridTemplateSearch extends FWDDBQueryHandler {

  protected $caFilterIds = array();
  
  protected $csFilterName;
  
  protected $cbShowOnlySuggested = false;
  
  protected $ciTemplateType = 0;
  
  /**
  * Construtor.
  * 
  * <p>Construtor da classe.</p>
  * @access public 
  */
  public function __construct($poDB){
    parent::__construct($poDB); 
    $this->coDataSet->addFWDDBField(new FWDDBField("" ,"context_id"   ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("" ,"context_name" ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("" ,"template_path" ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("" ,"template_file_name" ,DB_STRING));
  }
  
  public function setFilterIds($psFilterIds){
    $this->caFilterIds = $psFilterIds?explode(':',$psFilterIds):array();
    $this->caFilterIds[]=0;
  }
  
  public function setFilterName($psFilterName){
    $this->csFilterName = $psFilterName;
  }
  
  public function showOnlySuggested(){
    $this->cbShowOnlySuggested = true;
  }

  public function setTemplateType($psTemplateType){
    $this->csTemplateType = $psTemplateType;
  }
  public function makeQuery(){
    $msWhere ="";
    $maFilters = array();

    if($this->csTemplateType){
      $maFilters[] = " t.nContextType IN ($this->csTemplateType) ";
    }
    if($this->csFilterName){
      $maFilters[] = " upper(t.sName) like '%".strtoupper($this->csFilterName)."%'";
    }
    if($this->cbShowOnlySuggested){
      switch ($this->csTemplateType){
        case CONTEXT_CONTROL:
          $maFilters[] = "
          t.fkContext IN ( SELECT tbp.fkTemplate FROM pm_template_best_practice tbp
                                  JOIN rm_control_best_practice cbp ON ( cbp.fkBestPractice = tbp.fkBestPractice 
                                                                         AND cbp.fkControl IN (".implode(',',$this->caFilterIds).") 
                                                                       )
                      )
          ";
      }
    }
    
    //Pesquisar apenas templates aprovados
    $maFilters[] = "c.nstate = '".CONTEXT_STATE_APPROVED."'";
    if(count($maFilters)){
      $msWhere .= " WHERE " . implode(' AND ',$maFilters); 
    }
    $this->csSQL = "
          SELECT t.fkContext as context_id,
                  t.sName as context_name,
                 t.sPath as template_path,
                 t.sFileName as template_file_name
            FROM view_pm_template_active t
            JOIN isms_context c ON (t.fkContext = c.pkContext)
            $msWhere
    ";
    }

}
?>