<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGridDocumentSearch.
 *
 * <p>Consulta para popular o grid de pesquisa de documentos.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryGridDocumentSearch extends FWDDBQueryHandler {

  protected $csName = '';
  protected $ciDocumentId = 0;
  protected $caShow = null;
  protected $caExcluded = null;
  protected $cbShowAllDocuments = false;
  protected $cbIsSearchByRegister = false;

  public function __construct($poDB) {
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField("fkContext", "document_id",   DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("sName",     "document_name", DB_STRING));
  }

  public function setDocumentId($piDocumentId){
    $this->ciDocumentId = $piDocumentId;
  }

  public function setName($psName){
    $this->csName = $psName;
  }

  public function setExluded($paIds){
    $this->caExcluded = $paIds;
  }
  
  public function showAllDocuments($pbShow){
    $this->cbShowAllDocuments = $pbShow;
  }

  public function showDocuments($paIds){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,0x80000,__FILE__,__LINE__);
    $this->caShow = $paIds;
  }

  public function setIsSearchByRegister($pbIsByRegister){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,0x80000,__FILE__,__LINE__);
    $this->cbIsSearchByRegister = true;
  }
  
  public function makeQuery(){
    $maFilters = array();
    
    if($this->ciDocumentId){
      $maFilters[] = "fkContext != {$this->ciDocumentId}";
    }
    
    if($this->csName){
      $maFilters[] =  "upper(sName) like upper('%{$this->csName}%')";
    }
    
    if($this->caExcluded){
      $maFilters[] = "fkContext NOT IN (".implode(',',$this->caExcluded).")";
    }
    
    if(is_array($this->caShow)){
      if(count($this->caShow)){
        $maFilters[] = "fkContext IN (".implode(',',$this->caShow).")";
      }else{
        $maFilters[] = "0=1";
      }
    }
    
    if($this->cbIsSearchByRegister){
      $maFilters[] = " d.nType = " . CONTEXT_NONE;
      $maFilters[] = " d.fkContext NOT IN (SELECT fkContext FROM view_pm_published_docs ) ";
      $maFilters[] = " d.fkAuthor = " . FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    }
    $this->csSQL = "SELECT fkContext as document_id, sName as document_name
                    FROM " . ( $this->cbShowAllDocuments ? 'view_pm_document_active' : 'view_pm_published_docs' ) . " d";
    if(count($maFilters)){
      $this->csSQL.= " WHERE ".implode(' AND ',$maFilters);
    }
  }
}
?>