<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe QueryGridDocObsolete
 *
 * <p>Consulta para popular as grids de documentos obsoletos.</p>
 * @package ISMS
 * @subpackage classes
 */
class QueryGridDocObsolete extends FWDDBQueryHandler {

  protected $ciDocumentClassification;
  protected $ciDocumentStatus;
  protected $caTypesToShow = array();
  protected $caTypesToHide = array();
  protected $ciDocumentId = 0;
  
  /**
  * Construtor.
  * 
  * <p>Construtor da classe.</p>
  * @access public 
  */
  public function __construct($poDB){
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField('d.fkContext'         ,'document_id'              ,  DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('d.fkClassification'  ,'document_classification'  ,  DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('d.fkAuthor'          ,'document_author'          ,  DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('d.fkMainApprover'    ,'document_main_approver'   ,  DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('d.sName'             ,'document_name'            ,  DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('di.sManualVersion'   ,'document_version'         ,  DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('di.fkContext'        ,'instance_id'              ,  DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('di.sPath'            ,'file_path'                ,  DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('di.sFileName'        ,'file_name'                ,  DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('di.bIsLink'          ,'is_link'                  ,  DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('d.nType'             ,'document_type'            ,  DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('di.nMajorVersion'    ,'document_version'         ,  DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('u.sName         '    ,'responsible_name'         ,  DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('di.sLink        '    ,'document_link'            ,  DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('di.nMajorVersion'    ,'document_major_version'   ,  DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('di.nRevisionVersion' ,'document_revision_version',  DB_NUMBER));
    
  }

  public function setDocumentClassification($piClassification){
    $this->ciDocumentClassification = $piClassification;
  }

  public function setTypesToShow($piType){
    $this->caTypesToShow = func_get_args();
  }
  
  public function setTypesToHide($piType){
    $this->caTypesToHide = func_get_args();
  }
  
  public function makeQuery(){
    $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    $maFilters = array();

    $maFilters[] = "(
                      d.fkMainApprover = $miUserId
                      OR ( d.fkAuthor = $miUserId)
                      OR ( da.fkDocument IS NOT NULL)
                      OR (
                        (
                          dr.fkDocument IS NOT NULL
                          OR da.fkDocument IS NOT NULL
                          OR d.fkAuthor = $miUserId
                        )AND(
                          di.nMajorVersion > 0
                          AND c.nState = ".CONTEXT_STATE_DOC_APPROVED."
                          AND d.dDateProduction <= '".date('Y-m-d')."'
                        )
                      )
                    )";

    if(count($this->caTypesToShow)){
      $maTypesToShow = array_diff($this->caTypesToShow,$this->caTypesToHide);
      $maFilters[] = "d.nType IN (".implode(',',$maTypesToShow).")";
    }elseif(count($this->caTypesToHide)){
      $maFilters[] = "d.nType NOT IN (".implode(',',$this->caTypesToHide).")";
    }

    if($this->ciDocumentClassification){
      $maFilters[] = 'd.fkClassification = '.$this->ciDocumentClassification;
    }

    $msWhere = '';
    if(count($maFilters)){
      $msWhere = " WHERE ".implode(' AND ', $maFilters);
    }
    $this->csSQL = "
SELECT d.fkContext AS document_id,
       d.fkClassification AS document_classification,
       d.fkAuthor AS document_author,
       d.fkMainApprover AS document_main_approver,
       d.sName AS document_name,
       di.fkContext AS instance_id,
       di.sPath AS file_path,
       di.sFileName AS file_name,
       di.bIsLink AS is_link,
       d.nType AS document_type,
       di.sManualVersion AS document_version,
       di.nMajorVersion as document_major_version,
       di.nRevisionVersion as document_revision_version,
       u.sName AS responsible_name,
       di.sLink AS document_link
   FROM view_pm_doc_instance_active di 
     JOIN pm_document d ON ( di.fkDocument = d.fkContext AND di.fkContext != d.fkCurrentVersion AND di.dEndProduction IS NOT NULL )
     JOIN isms_context c ON ( c.pkContext = d.fkContext AND c.nState <> ".CONTEXT_STATE_DELETED." )
     JOIN view_isms_user_active u ON ( u.fkContext = d.fkMainApprover )
     LEFT JOIN pm_doc_readers dr ON (dr.fkDocument = d.fkContext AND dr.fkUser = $miUserId AND dr.bDenied=0)
     LEFT JOIN pm_doc_approvers da ON (da.fkDocument = d.fkContext AND da.fkUser = $miUserId)
     $msWhere
";
  }
}

?>