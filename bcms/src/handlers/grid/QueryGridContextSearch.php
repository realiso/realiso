<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGridContextSearch.
 *
 * <p>Consulta para popular a grid de pesquisa de todos os contextos do sistema.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryGridContextSearch extends FWDDBQueryHandler {

  protected $caContextTypes = array();
  protected $csString = '';
  protected $ciCreationDateStart = 0;
  protected $ciEditionDateStart = 0;
  protected $ciCreationDateEnd = 0;
  protected $ciEditionDateEnd = 0;
  protected $cbIsAdvancedSearchDocument = false;
  protected $cbIsAdvancedSearchTemplate = false;

  public function __construct($poDB) {
    parent::__construct($poDB);

    $this->coDataSet->addFWDDBField(new FWDDBField("res.context_type","context_type", DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("res.context_id","context_id", DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("res.context_string","context_string", DB_STRING));
  }

  public function setContextTypes($paContextTypes) {
    $this->caContextTypes = $paContextTypes;
  }

  public function setStringFilter($psString) {
    $this->csString = $psString;
  }

  public function setCreationDateStart($piDate) {
    $this->ciCreationDateStart = $piDate;
  }

  public function setEditionDateStart($piDate) {
    $this->ciEditionDateStart = $piDate;
  }

  public function setCreationDateEnd($piDate) {
    $this->ciCreationDateEnd = $piDate;
  }

  public function setEditionDateEnd($piDate) {
    $this->ciEditionDateEnd = $piDate;
  }

  public function setIsAdvancedSearchDocument($pbIsAdvancedSearch) {
    $this->cbIsAdvancedSearchDocument = $pbIsAdvancedSearch;
  }

  public function setIsAdvancedSearchTemplate($pbIsAdvancedSearch) {
    $this->cbIsAdvancedSearchTemplate = $pbIsAdvancedSearch;
  }

  public function makeQuery(){
    $maSubSelects = array();
    $maACL = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
    if(count($this->caContextTypes)){
      $moContextObject = new ISMSContextObject();
      foreach($this->caContextTypes as $miContextType){
        $moContext = $moContextObject->getContextObject($miContextType);
        $moHandler = $moContext->getSearchHandler();
        $moHandler->setSearch($this->csString);
        $moHandler->setCreationDateStart($this->ciCreationDateStart);
        $moHandler->setEditionDateStart($this->ciEditionDateStart);
        $moHandler->setCreationDateEnd($this->ciCreationDateEnd);
        $moHandler->setEditionDateEnd($this->ciEditionDateEnd);
        if(($miContextType==CONTEXT_DOCUMENT)||($miContextType==CONTEXT_DOCUMENT_TEMPLATE)){
          if((FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(POLICY_MODE))
              && (!in_array('M.PM',$maACL))
           ){
              if($miContextType==CONTEXT_DOCUMENT)
                $moHandler->setSearchContent($this->cbIsAdvancedSearchDocument);
              else
                $moHandler->setSearchContent($this->cbIsAdvancedSearchTemplate);
              $moHandler->makeQuery();
              $maSubSelects[] = $moHandler->getSQL();
           }
        }else{
          $moHandler->makeQuery();
          $maSubSelects[] = $moHandler->getSQL();
        }
      }
    }
    $msSubSelects = implode(' UNION ALL ', $maSubSelects);
    
    $this->csSQL = "SELECT
                      res.context_type as context_type,
                      res.context_id as context_id,
                      res.context_string as context_string
                    FROM ( $msSubSelects ) res
                      JOIN isms_context cont ON (res.context_id = cont.pkContext AND cont.nState <> " . CONTEXT_STATE_DELETED . ")";
  }

}
?>