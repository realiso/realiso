<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGridUserSearch.
 *
 * <p>Consulta para popular o grid de pesquisa de usu�rios.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryGridUserSearch extends FWDDBQueryHandler {

  protected $csName = '';

  protected $csLogin = '';

  protected $ciAreaId = 0;

  protected $ciProcessId = 0;

  protected $caExcludedUsers = array();

  public function __construct($poDB){
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField('','user_id', DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('','user_login', DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('','user_name', DB_STRING));
  }

  public function setName($psName){
    $this->csName = trim($psName);
  }

  public function setLogin($psLogin){
    $this->csLogin = trim($psLogin);
  }

  public function setArea($piAreaId){
    if($piAreaId){
      $this->ciAreaId = $piAreaId;
      $this->ciProcessId = 0;
    }
  }

  public function setProcess($piProcessId){
    if($piProcessId){
      $this->ciProcessId = $piProcessId;
      $this->ciAreaId = 0;
    }
  }

  public function setExludedUsers($paExcludeUsers){
    $this->caExcludedUsers = $paExcludeUsers;
  }

  public function makeQuery(){
    $this->maFilters = array();
    $msFrom = 'view_isms_user_active u ';
    // Filtro por nome
    if($this->csName){
      $this->maFilters[] = "upper(u.sName) like '%".strtoupper($this->csName)."%'";
    }
    // Filtro por login
    if($this->csLogin){
      $this->maFilters[] = "upper(u.sLogin) like '%".strtoupper($this->csLogin)."%'";
    }
    // Filtro por �rea
    if($this->ciAreaId){
      $this->maFilters[] = "a.fkContext = {$this->ciAreaId}";
      $msFrom.= "JOIN pm_process_user pu ON (pu.fkUser = u.fkContext)
                 JOIN view_rm_process_active p ON (p.fkContext = pu.fkProcess)
                 JOIN view_rm_area_active a ON (a.fkContext = p.fkArea)";
    }
    // Filtro por processo
    if($this->ciProcessId){
      $this->maFilters[] = "p.fkContext = {$this->ciProcessId}";
      $msFrom.= "JOIN pm_process_user pu ON (pu.fkUser = u.fkContext)
                 JOIN view_rm_process_active p ON (p.fkContext = pu.fkProcess)";
    }
    // Filtro por id de usu�rios (exclui usu�rios do resultado)
    if(count($this->caExcludedUsers)){
      $this->maFilters[] = 'u.fkContext NOT IN ('.implode(',',$this->caExcludedUsers).')';
    }
    // Monta a query
    $this->csSQL = "SELECT u.fkContext as user_id, u.sLogin as user_login, u.sName as user_name
                    FROM $msFrom";
    if(count($this->maFilters) > 0){
      $this->csSQL.= ' WHERE '.implode(' AND ', $this->maFilters);
    }
  }

}

?>