<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

class QueryGridPlaceCategory extends FWDDBQueryHandler {

	protected $ciCategoryId = 0;

	public function __construct($poDB) {
		parent::__construct($poDB);

		$this->coDataSet->addFWDDBField(new FWDDBField("pc.context_id","context_id", DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("pc.context_type", "context_type", DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("pc.context_name","context_name", DB_STRING));
	}

	public function setRootCategory($piCategoryId) {
		$this->ciCategoryId = $piCategoryId;
	}

	public function makeQuery() {
		$msUnion = "";
		$msWhere = " WHERE fkParent";
		if ($this->ciCategoryId) {
			$msWhere .= " = {$this->ciCategoryId}";
			$msUnion = " UNION
					SELECT 
						t.fkContext as context_id, 
						t.sname as context_name,
						" . CONTEXT_CM_THREAT . " as context_type
					FROM 
						cm_place_category_threat pct
						join view_cm_threat_active t on t.fkcontext = pct.fkthreat
					WHERE pct.fkplacecategory = {$this->ciCategoryId}" ;		
		}else{
			$msWhere .= " IS NULL";
		}

		$this->csSQL = "SELECT pc.context_id, pc.context_name, pc.context_type FROM
						(
						SELECT 
							cat.fkContext as context_id, 
							cat.sName as context_name,
							" . CONTEXT_CM_PLACE_CATEGORY . " as context_type
						FROM 
							view_cm_place_category_active cat
							$msWhere
							$msUnion
						) pc";
		$this->coDataSet->setOrderBy("context_type","-");
	}

}
?>