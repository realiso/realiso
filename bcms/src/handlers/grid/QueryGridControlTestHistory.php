<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGridControlTestHistory.
 *
 * <p>consulta para buscar o histórico de testes do controle selecionado.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryGridControlTestHistory extends FWDDBQueryHandler {
  protected $ciId = 0;
  
  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField("fkContext"           ,"control_id"      , DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("ddatetodo"           ,"date_to_do"      , DB_DATETIME));
    $this->coDataSet->addFWDDBField(new FWDDBField("ddateaccomplishment" ,"date_realized"   , DB_DATETIME));
    $this->coDataSet->addFWDDBField(new FWDDBField("nrealefficiency"     ,"real_efficiency" , DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("btestedvalue"        ,"test_value"      , DB_NUMBER));
  }
  
  public function setId($piId){
    $this->ciId = $piId;
  }
  
  public function makeQuery() {
    $msWhere = $this->ciId ? " WHERE c.fkContext = " . $this->ciId : "";
    $this->csSQL = "
  SELECT fkContext as control_id,
         ddatetodo as date_to_do,
         ddateaccomplishment as date_realized,
         btestedvalue as test_value
    FROM view_rm_control_active c
      JOIN rm_control_test_history cth ON (cth.fkControl = c.fkContext)
    $msWhere
";
  } 
}
?>