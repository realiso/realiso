<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

class QueryOfflinePlanTest extends FWDDBQueryHandler {

	private $plan;

	public static function replacePatterns($str){
		$desc = $str;
		if(strstr($desc,"%1%")){
			$desc = str_replace("%1%", FWDLanguage::getPHPStringValue('minutes', " Minutos"), $desc);
		}
		if(strstr($desc,"%2%")){
			$desc = str_replace("%2%", FWDLanguage::getPHPStringValue("hours", " Horas"), $desc);
		}
		if(strstr($desc,"%3%")){
			$desc = str_replace("%3%", FWDLanguage::getPHPStringValue("days", " Dias"), $desc);
		}
		if(strstr($desc,"%4%")){
			$desc = str_replace("%4%", FWDLanguage::getPHPStringValue("months", " Meses"), $desc);
		}
		$to = FWDLanguage::getPHPStringValue('to2',' a ');
		$from = FWDLanguage::getPHPStringValue('from',' De ');

		$desc = str_replace("{\$from}", $from, $desc);
		$desc = str_replace("{\$to}", $to, $desc);
		return $desc;
	}

	public function __construct($poDB=null){
		parent::__construct($poDB);
		$this->coDataSet->addFWDDBField(new FWDDBField('resume',			'resume'  			,DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField('acronym',			'acronym'  			,DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField('description',		'description'  		,DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField('estimate',			'estimate'  		,DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField('name',				'name'  			,DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField('planDescription',	'planDescription'  	,DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField('range',				'range'  			,DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField('planActionId',		'planActionId'		,DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField('planId',			'planId'			,DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField('dependsOn',			'dependsOn'			,DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField('dependsOnId',		'dependsOnId'			,DB_STRING));
	}

	public function setPlan($plan){
		$this->plan = $plan;
	}

	public function makeQuery(){
		$this->csSQL = "select
							v.resume, 
							v.acronym, 
							v.description, 
							v.estimate, 
							v.name, 
							v.plandescription, 
							v.range,
							v.planActionId as planActionId,
							v.fkcontext as planId,
							p.resume as dependsOn,
							p.fkcontext as dependsOnId
						from 
							view_plan_offline v
							left join cm_plan_action p on p.fkcontext = v.depends
						where 
							v.fkcontext = {$this->plan}";
	}
}
?>