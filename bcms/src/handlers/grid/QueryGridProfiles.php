<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGridProfiles.
 *
 * <p>Consulta para popular a grid de perfis.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryGridProfiles extends FWDDBQueryHandler {	
	
	public function __construct($poDB) {
		parent::__construct($poDB);	

		$this->coDataSet->addFWDDBField(new FWDDBField("fkContext","profile_id", DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("sName","profile_name", DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField("nId","profile_identifier", DB_NUMBER));
	}

	public function makeQuery() {						
		$this->csSQL = "SELECT fkContext as profile_id, sName as profile_name, nId as profile_identifier FROM isms_profile p
										JOIN isms_context c ON (c.pkContext = p.fkContext AND c.nState <> ". CONTEXT_STATE_DELETED .")";
	} 
}
?>