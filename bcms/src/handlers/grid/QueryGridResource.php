<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

class QueryGridResource extends FWDDBQueryHandler {

	protected $ciMaxImpacts = 0;

	protected $ciMinImpacts = 0;

	protected $groupId = 0;

	protected $providerId = 0;

	protected $provider = 0;

	protected $placeId = 0;

	protected $cmt = 0;

	protected $areaId = 0;

	protected $nameFilter = 0;
	
	protected $unId = 0;
	protected $onProviders = false;	
	protected $onResources = false;

	public function __construct($poDB=null){
		parent::__construct($poDB);
		
		$this->coDataSet->addFWDDBField(new FWDDBField('', 'resource_id', DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField('', 'resource_type', DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField('', 'resource_name', DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField('', 'resource_detail', DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField('', 'resource_acronym', DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField('', 'comercialNumber', DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField('', 'homeNumber', DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField('', 'celNumber', DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField('', 'nextelNumber', DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField('', 'email', DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField('', 'department', DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField('', 'func', DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField('', 'resource_is_default', DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField('', 'default_contact_info', DB_STRING));
		
		$this->coDataSet->addFWDDBField(new FWDDBField('', 'contact_name', DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField('', 'contact_email', DB_STRING));		
		$this->coDataSet->addFWDDBField(new FWDDBField('', 'contact_phone', DB_STRING));				
    $this->coDataSet->addFWDDBField(new FWDDBField('', 'contact_id', DB_NUMBER));		
		
		$this->coDataSet->addFWDDBField(new FWDDBField('escalation', 'escalation', DB_NUMBER));
	}
	
	

	public function setNameFilter($nameFilter){
		$this->nameFilter = $nameFilter;
	}

	public function setMaxImpacts($piMaxImpacts){
		$this->ciMaxImpacts = $piMaxImpacts;
	}

	public function setMinImpacts($piMinImpacts){
		$this->ciMinImpacts = $piMinImpacts;
	}

	public function setGroup($groupId){
		$this->groupId = $groupId;
	}

	public function setProviderId($providerId){
		$this->providerId = $providerId;
	}

	public function setProvider($provider){
		$this->provider = $provider;
	}

	public function setPlace($placeId){
		$this->placeId = $placeId;
	}

	public function setCMT($cmt){
		$this->cmt = $cmt;
	}

	public function setArea($areaId){
		$this->areaId = $areaId;
	}
	
	
	public function setUN($unId){
	  $this->unId = $unId;
	}
		
	public function setOnResources($onResources){
	  $this->onResources = $onResources;
	}

	public function setOnProviders($onProviders){
	  $this->onProviders = $onProviders;
	}	
	
	public function makeQuery(){
	  $this->makeQueryTemp();
	  return;
	  
	  $debug = true;
	  
		$maHavingFilters = array();

		if($this->ciMaxImpacts){
			$maHavingFilters[] = "COUNT(i.fkcontext) <= {$this->ciMaxImpacts}";
		}

		if($this->ciMinImpacts){
			$maHavingFilters[] = "COUNT(i.fkcontext) >= {$this->ciMinImpacts}";
		}

		if(count($maHavingFilters)==0){
			$msHaving = '';
		}else{
			$msHaving = ' HAVING '.implode(' AND ',$maHavingFilters);
		}

		$joinPlaceResource = "";
		$joinPlaceGroup = "";
		$where = "";
		
		if($this->placeId){
		   if($debug) echo " AAA ";
			if(!$this->cmt){  if($debug) echo " BBB ";
				$joinPlaceResource = "JOIN view_cm_place_resource_active pr on pr.fkresource = r.fkcontext";
				$joinPlaceGroup = "JOIN view_cm_place_resource_active pr on pr.fkgroup = g.fkcontext";
				$where = " AND pr.fkplace = {$this->placeId} AND (pr.cmt is null or pr.cmt = 0) ";
			}else{  if($debug) echo " CCC ";
				$query = new FWDDBDataSet(FWDWebLib::getConnection());
				$query->setQuery("select fkgroup as id from view_cm_place_resource_active where cmt = 1 and fkplace = {$this->placeId}");
				$query->addFWDDBField(new FWDDBField('id','id'  ,DB_NUMBER));
				$query->execute();
				if($query->fetch()){
					$this->groupId = $query->getFieldByAlias("id")->getValue();
				}else{
					$this->groupId = 999999;
				}
			}
		}

		$joinAreaResource = "";
		$joinAreaGroup = "";

		if($this->areaId){
			$joinAreaResource = "JOIN view_cm_area_resource_active ar on ar.fkresource = r.fkcontext";
			$joinAreaGroup = "JOIN view_cm_area_resource_active ar on ar.fkgroup = g.fkcontext";
			$where = " AND ar.fkarea = {$this->areaId}";
		}

		$resource = FWDLanguage::getPHPStringValue('resource','recurso(s)');
		$groups = FWDLanguage::getPHPStringValue('groups','grupo(s)');
		$function = FWDLanguage::getPHPStringValue('function','Fun��o');
		
		if($this->provider != 0){ if($debug) echo " 001 ";
		  
			if($this->providerId != 0){ if($debug) echo " 002 ";
				$this->csSQL = "
            SELECT 
            	r.fkcontext as resource_id,
  						'resource' as resource_type,
  						r.name as resource_name,
  						(select count(gr.fkcontext) from view_cm_group_resource_active gr where fkresource = r.fkcontext)||' ".$groups."' as resource_detail,
  						'".$function.": '||r.func as resource_acronym,
  						comercialNumber as comercialNumber,
  						homeNumber as homeNumber,
  						celNumber as celNumber,
  						nextelNumber as nextelNumber,
  						email as email,
  						'' as department,
  						'' as func
  					FROM
            	view_cm_resource_active r
            	JOIN cm_provider_resource pr on pr.fkresource = r.fkcontext
            WHERE
            	pr.fkprovider = ".$this->providerId."
            	AND r.provider = 1 
            GROUP BY
            	r.fkcontext,
            	r.name,
            	r.func,
            	comercialNumber,
            	homeNumber,
            	celNumber,
            	nextelNumber,
            	email
        ";
				
			}else{
			  if($debug) echo " 003 ";
				$resource = FWDLanguage::getPHPStringValue('contacts','contato(s)');
				$joinProvider = "";
				$whereProvider = "";
				
				if($this->placeId){
					$joinProvider .= " join view_cm_place_provider_active pp on pp.fkprovider = p.fkcontext ";
					$whereProvider .= " and pp.fkplace = {$this->placeId} ";
				}
				
				$this->csSQL = "
          SELECT
						p.fkcontext as resource_id,
						'provider' as resource_type,
						p.name as resource_name,
						(select count(pr.fkcontext) from view_cm_provider_resource_active pr where fkprovider = p.fkcontext)||' ".$resource."' as resource_detail,
						p.contract as resource_acronym,
						'' as comercialNumber,
						'' as homeNumber,
						'' as celNumber,
						'' as nextelNumber,
						'' as email,
						'' as department,
						'' as func
          FROM
            view_cm_provider_active p
            {$joinProvider}
          WHERE 1=1 
          {$whereProvider}
          GROUP BY
            p.fkcontext,
            p.name,
            p.contract
        ";
          
			}
			
		}else if($this->groupId != 0){
		   if($debug) echo " 004 ";
			if($this->nameFilter){
				$where .= " and lower(r.name) like lower('%".$this->nameFilter."%') ";
			}
			
			$this->csSQL = "
        SELECT 
        	r.fkcontext as resource_id,
        	gr.escalation as escalation,
					'resource' as resource_type,
					r.name as resource_name,
					(select count(gr2.fkcontext) from view_cm_group_resource_active gr2 where gr2.fkresource = r.fkcontext)||' ".$groups."' as resource_detail,
					'".$function.": '||r.func as resource_acronym,
					comercialNumber as comercialNumber,
					homeNumber as homeNumber,
					celNumber as celNumber,
					nextelNumber as nextelNumber,
					email as email,
					dep.name as department,
					func as func
				FROM
        	view_cm_resource_active r
        	JOIN cm_group_resource gr on gr.fkresource = r.fkcontext
        	LEFT JOIN view_cm_department_active dep on dep.fkcontext = r.fkdepartment
        	{$joinPlaceResource}
        	{$joinAreaResource}
        WHERE
        	gr.fkgroup = ".$this->groupId." 
        	AND r.provider <> 1 
        	{$where}
        GROUP BY
        	r.fkcontext,
        	r.name,
        	r.func,
        	gr.escalation,
        	comercialNumber,
					homeNumber,
					celNumber,
					nextelNumber,
					email,
					department,
					func
			";
        	
		}else{
       if($debug) echo " 005 ";
			$whereResource = "";
			$whereGroup = "";
			
			if($this->nameFilter){
				$whereGroup .= " and lower(g.name) like lower('%".$this->nameFilter."%') ";
				$whereResource .= " and lower(r.name) like lower('%".$this->nameFilter."%') ";
			}

			$this->csSQL = "
				SELECT
					g.fkcontext as resource_id,
					'group' as resource_type,
					g.name as resource_name,
					(select count(gr.fkcontext) from view_cm_group_resource_active gr where fkgroup = g.fkcontext)||' ".$resource."' as resource_detail,
					g.acronym as resource_acronym,
					'' as comercialNumber,
					'' as homeNumber,
					'' as celNumber,
					'' as nextelNumber,
					'' as email,
					'' as department,
					'' as func
        FROM
          view_cm_group_active g 
          {$joinPlaceGroup}
          {$joinAreaGroup}
        WHERE 1=1 
          {$where}
          {$whereGroup}
        GROUP BY
          g.fkcontext,
          g.name,
          g.acronym 
        UNION
        SELECT 
        	r.fkcontext as resource_id,
					'resource' as resource_type,
					r.name as resource_name,
					(select count(gr2.fkcontext) from view_cm_group_resource_active gr2 where gr2.fkresource = r.fkcontext)||' ".$groups."' as resource_detail,
					'".$function.": '||r.func as resource_acronym,
					comercialNumber as comercialNumber,
					homeNumber as homeNumber,
					celNumber as celNumber,
					nextelNumber as nextelNumber,
					email as email,
					dep.name as department,
					func as func
				FROM
          view_cm_resource_active r 
          LEFT JOIN view_cm_department_active dep on dep.fkcontext = r.fkdepartment
          {$joinPlaceResource}
          {$joinAreaResource}
        WHERE 
        	r.provider <> 1 
          {$where}
          {$whereResource}
        GROUP BY
          r.fkcontext,
          r.name,
          r.func,
          comercialNumber,
				  homeNumber,
				  celNumber,
				  nextelNumber,
				  email,
				  department,
				  func
			";
          
		}
  }
	
  /*
   * Recursos
   */
	private function getResources(){

		$resource = FWDLanguage::getPHPStringValue('resource','recurso(s)');
		$groups = FWDLanguage::getPHPStringValue('groups','grupo(s)');
		$function = FWDLanguage::getPHPStringValue('function','Fun��o');	  
	  
		$whereResource = "";
		$whereGroup = "";
		
		if($this->nameFilter){
			$whereGroup .= " and lower(g.name) like lower('%{$this->nameFilter}%') ";
			$whereResource .= " and lower(r.name) like lower('%{$this->nameFilter}%') ";
		} 
	        
		$sql = "
			SELECT
				g.fkcontext as resource_id,
				'group' as resource_type,
				g.name as resource_name,
				(select count(gr.fkcontext) from view_cm_group_resource_active gr where fkgroup = g.fkcontext)||' {$resource}' as resource_detail,
				g.acronym as resource_acronym,
				'' as comercialNumber,
				'' as homeNumber,
				'' as celNumber,
				'' as nextelNumber,
				'' as email,
				'' as department,
				'' as func
      FROM
        view_cm_group_active g 
      WHERE 1=1 
        {$whereGroup}
      GROUP BY
        g.fkcontext,
        g.name,
        g.acronym 
      UNION
      SELECT 
      	r.fkcontext as resource_id,
				'resource' as resource_type,
				r.name as resource_name,
				(select count(gr2.fkcontext) from view_cm_group_resource_active gr2 where gr2.fkresource = r.fkcontext)||' {$groups}' as resource_detail,
				'{$function}: '||r.func as resource_acronym,
				comercialNumber as comercialNumber,
				homeNumber as homeNumber,
				celNumber as celNumber,
				nextelNumber as nextelNumber,
				email as email,
				dep.name as department,
				func as func
			FROM
        view_cm_resource_active r 
        LEFT JOIN view_cm_department_active dep on dep.fkcontext = r.fkdepartment        
      WHERE 
      	r.provider <> 1 
        {$whereResource}
      GROUP BY
        r.fkcontext,
        r.name,
        r.func,
        comercialNumber,
			  homeNumber,
			  celNumber,
			  nextelNumber,
			  email,
			  department,
			  func
		";
     				
		return $sql;	
	}

	private function getResourcesOfPlace(){

		$resource = FWDLanguage::getPHPStringValue('resource','recurso(s)');
		$groups = FWDLanguage::getPHPStringValue('groups','grupo(s)');
		$function = FWDLanguage::getPHPStringValue('function','Fun��o');	  
		
		$whereResource = "";
		$whereGroup = "";
		
		if($this->nameFilter){
			$whereGroup .= " and lower(g.name) like lower('%{$this->nameFilter}%') ";
			$whereResource .= " and lower(r.name) like lower('%{$this->nameFilter}%') ";
		}

		$sql = "
			SELECT
				g.fkcontext as resource_id,
				'group' as resource_type,
				g.name as resource_name,
				(select count(gr.fkcontext) from view_cm_group_resource_active gr where fkgroup = g.fkcontext)||' {$resource}' as resource_detail,
				g.acronym as resource_acronym,
				'' as comercialNumber,
				'' as homeNumber,
				'' as celNumber,
				'' as nextelNumber,
				'' as email,
				'' as department,
				'' as func
      FROM
        view_cm_group_active g 
				JOIN view_cm_place_resource_active pr on pr.fkgroup = g.fkcontext
      WHERE 
        pr.fkplace = {$this->placeId} AND (pr.cmt is null or pr.cmt = 0) 
        {$whereGroup}
      GROUP BY
        g.fkcontext,
        g.name,
        g.acronym 
      UNION
      SELECT 
      	r.fkcontext as resource_id,
				'resource' as resource_type,
				r.name as resource_name,
				(select count(gr2.fkcontext) from view_cm_group_resource_active gr2 where gr2.fkresource = r.fkcontext)||' {$groups}' as resource_detail,
				'{$function}: '||r.func as resource_acronym,
				comercialNumber as comercialNumber,
				homeNumber as homeNumber,
				celNumber as celNumber,
				nextelNumber as nextelNumber,
				email as email,
				dep.name as department,
				func as func
			FROM
        view_cm_resource_active r 
        LEFT JOIN view_cm_department_active dep on dep.fkcontext = r.fkdepartment
        JOIN view_cm_place_resource_active pr on pr.fkresource = r.fkcontext
        
      WHERE 
      	r.provider <> 1 
        AND pr.fkplace = {$this->placeId} AND (pr.cmt is null or pr.cmt = 0) 
        {$whereResource}
      GROUP BY
        r.fkcontext,
        r.name,
        r.func,
        comercialNumber,
			  homeNumber,
			  celNumber,
			  nextelNumber,
			  email,
			  department,
			  func
		";
     				
		return $sql;
	}	
	
	private function getResourcesOfUN(){

		$resource = FWDLanguage::getPHPStringValue('resource','recurso(s)');
		$groups = FWDLanguage::getPHPStringValue('groups','grupo(s)');
		$function = FWDLanguage::getPHPStringValue('function','Fun��o');	  
		
		$whereResource = "";
		$whereGroup = "";
		
		if($this->nameFilter){
			$whereGroup .= " and lower(g.name) like lower('%{$this->nameFilter}%') ";
			$whereResource .= " and lower(r.name) like lower('%{$this->nameFilter}%') ";
		}

		//JOIN view_cm_place_resource_active pr on pr.fkgroup = g.fkcontext
		//pr.fkplace = {$this->placeId} AND (pr.cmt is null or pr.cmt = 0)
		
		//view_cm_place_resource_active pr on pr.fkresource = r.fkcontext
		//AND pr.fkplace = {$this->placeId} AND (pr.cmt is null or pr.cmt = 0) 
		$sql = "
			SELECT
				g.fkcontext as resource_id,
				'group' as resource_type,
				g.name as resource_name,
				(select count(gr.fkcontext) from view_cm_group_resource_active gr where fkgroup = g.fkcontext)||' {$resource}' as resource_detail,
				g.acronym as resource_acronym,
				'' as comercialNumber,
				'' as homeNumber,
				'' as celNumber,
				'' as nextelNumber,
				'' as email,
				'' as department,
				'' as func
      FROM
        view_cm_group_active g 
			JOIN
				cm_area_resource ar on (ar.fkgroup = g.fkcontext)
      WHERE 
        ar.fkarea = {$this->unId} AND (ar.cmt is null or ar.cmt = 0) 
        {$whereGroup}
      GROUP BY
        g.fkcontext,
        g.name,
        g.acronym 
      UNION
      SELECT 
      	r.fkcontext as resource_id,
				'resource' as resource_type,
				r.name as resource_name,
				(select count(gr2.fkcontext) from view_cm_group_resource_active gr2 where gr2.fkresource = r.fkcontext)||' {$groups}' as resource_detail,
				'{$function}: '||r.func as resource_acronym,
				comercialNumber as comercialNumber,
				homeNumber as homeNumber,
				celNumber as celNumber,
				nextelNumber as nextelNumber,
				email as email,
				dep.name as department,
				func as func
			FROM
        view_cm_resource_active r 
      LEFT JOIN 
      	view_cm_department_active dep on dep.fkcontext = r.fkdepartment
      JOIN 
        cm_area_resource ar on (ar.fkresource = r.fkcontext)
        
      WHERE 
      	r.provider <> 1 
        AND ar.fkarea = {$this->unId} AND (ar.cmt is null or ar.cmt = 0) 
        {$whereResource}
      GROUP BY
        r.fkcontext,
        r.name,
        r.func,
        comercialNumber,
			  homeNumber,
			  celNumber,
			  nextelNumber,
			  email,
			  department,
			  func
		";
     				
		return $sql;
	}
	
	private function getCMTOfPlace(){

		$groups = FWDLanguage::getPHPStringValue('groups','grupo(s)');
		$function = FWDLanguage::getPHPStringValue('function','Fun��o');	  	  

		$query = new FWDDBDataSet(FWDWebLib::getConnection());
		$query->setQuery("SELECT fkgroup AS id FROM view_cm_place_resource_active WHERE cmt = 1 AND fkplace = {$this->placeId}");
		$query->addFWDDBField(new FWDDBField('id','id'  ,DB_NUMBER));
		$query->execute();
		if($query->fetch()){
			$this->groupId = $query->getFieldByAlias("id")->getValue();
		}else{
			$this->groupId = 999999;
		}
	  
	  $where = "";
		if($this->nameFilter){
			$where .= " and lower(r.name) like lower('%{$this->nameFilter}%') ";
		}
		
		$sql = "
      SELECT 
      	r.fkcontext as resource_id,
      	gr.escalation as escalation,
				'resource' as resource_type,
				r.name as resource_name,
				(select count(gr2.fkcontext) from view_cm_group_resource_active gr2 where gr2.fkresource = r.fkcontext)||' {$groups}' as resource_detail,
				'{$function}: '||r.func as resource_acronym,
				comercialNumber as comercialNumber,
				homeNumber as homeNumber,
				celNumber as celNumber,
				nextelNumber as nextelNumber,
				email as email,
				dep.name as department,
				func as func
			FROM
      	view_cm_resource_active r
      	JOIN cm_group_resource gr on gr.fkresource = r.fkcontext
      	LEFT JOIN view_cm_department_active dep on dep.fkcontext = r.fkdepartment

      WHERE
      	gr.fkgroup = {$this->groupId} 
      	AND r.provider <> 1 
      	{$where}
      GROUP BY
      	r.fkcontext,
      	r.name,
      	r.func,
      	gr.escalation,
      	comercialNumber,
				homeNumber,
				celNumber,
				nextelNumber,
				email,
				department,
				func
		";	  
   	
    return $sql;
	}

	private function getResourcesOfGroup(){

		$groups = FWDLanguage::getPHPStringValue('groups','grupo(s)');
		$function = FWDLanguage::getPHPStringValue('function','Fun��o');
    
	  $where = "";
		if($this->nameFilter){
			$where .= " and lower(r.name) like lower('%{$this->nameFilter}%') ";
		}
		
		$sql = "
      SELECT 
      	r.fkcontext as resource_id,
      	gr.escalation as escalation,
				'resource' as resource_type,
				r.name as resource_name,
				(select count(gr2.fkcontext) from view_cm_group_resource_active gr2 where gr2.fkresource = r.fkcontext)||' {$groups}' as resource_detail,
				'{$function}: '||r.func as resource_acronym,
				comercialNumber as comercialNumber,
				homeNumber as homeNumber,
				celNumber as celNumber,
				nextelNumber as nextelNumber,
				email as email,
				dep.name as department,
				func as func
			FROM
      	view_cm_resource_active r
      	JOIN cm_group_resource gr on gr.fkresource = r.fkcontext
      	LEFT JOIN view_cm_department_active dep on dep.fkcontext = r.fkdepartment

      WHERE
      	gr.fkgroup = {$this->groupId} 
      	AND r.provider <> 1 
      	{$where}
      GROUP BY
      	r.fkcontext,
      	r.name,
      	r.func,
      	gr.escalation,
      	comercialNumber,
				homeNumber,
				celNumber,
				nextelNumber,
				email,
				department,
				func
		";
      		  
    return $sql;
	}
	
	/*
	 * Fornecedores
	 */
	private function getProviders(){
	  
		$resource = FWDLanguage::getPHPStringValue('contacts','contato(s)');

		$sql = "
      SELECT
				p.fkcontext as resource_id,
				'provider' as resource_type,
				p.name as resource_name,
				(select count(pr.fkcontext) from view_cm_provider_resource_active pr where fkprovider = p.fkcontext)||' {$resource}' as resource_detail,
				p.contract as resource_acronym,
				'' as comercialNumber,
				'' as homeNumber,
				'' as celNumber,
				'' as nextelNumber,
				'' as email,
				'' as department,
				'' as func,
				'' as default_contact_info,
				r.name as contact_name,
				r.email as contact_email,
				r.comercialnumber as contact_phone,
				r.fkcontext as contact_id
      FROM
        view_cm_provider_active p
        LEFT JOIN view_cm_provider_resource_active pr on (pr.fkprovider = p.fkcontext AND pr.bisdefault = 1)
        LEFT JOIN view_cm_resource_active r on (r.fkcontext = pr.fkresource)
      WHERE 1=1 
      
      GROUP BY
        p.fkcontext,
        p.name,
        p.contract,
        default_contact_info,
        r.name,
        r.email,
        r.comercialnumber,
        contact_id
    ";

    return $sql;
	}
	
	private function getProvidersOfPlace(){
	  
		$resource = FWDLanguage::getPHPStringValue('contacts','contato(s)');

		$sql = "
      SELECT
				p.fkcontext as resource_id,
				'provider' as resource_type,
				p.name as resource_name,
				(select count(pr.fkcontext) from view_cm_provider_resource_active pr where fkprovider = p.fkcontext)||' {$resource}' as resource_detail,
				p.contract as resource_acronym,
				'' as comercialNumber,
				'' as homeNumber,
				'' as celNumber,
				'' as nextelNumber,
				'' as email,
				'' as department,
				'' as func
      FROM
        view_cm_provider_active p
      JOIN 
      	view_cm_place_provider_active pp on pp.fkprovider = p.fkcontext 
      WHERE
     		pp.fkplace = {$this->placeId} 
      GROUP BY
        p.fkcontext,
        p.name,
        p.contract
    ";

    return $sql;
	}	
	
	private function getProvidersOfUN(){
	  
		$resource = FWDLanguage::getPHPStringValue('contacts','contato(s)');

		$sql = "
      SELECT
				p.fkcontext as resource_id,
				'provider' as resource_type,
				p.name as resource_name,
				(select count(pr.fkcontext) from view_cm_provider_resource_active pr where fkprovider = p.fkcontext)||' {$resource}' as resource_detail,
				p.contract as resource_acronym,
				'' as comercialNumber,
				'' as homeNumber,
				'' as celNumber,
				'' as nextelNumber,
				'' as email,
				'' as department,
				'' as func
      FROM
        view_cm_provider_active p
      JOIN 
      	rm_area_provider ap on ap.fkprovider = p.fkcontext 
      WHERE
     		ap.fkarea = {$this->unId} 
      GROUP BY
        p.fkcontext,
        p.name,
        p.contract
    ";

    return $sql;
	}		
	
	
	private function getProvidersOfGroup(){
	  
		$groups = FWDLanguage::getPHPStringValue('groups','grupo(s)');
		$function = FWDLanguage::getPHPStringValue('function','Fun��o');	  	  
	  
		$sql = "
        SELECT 
        	r.fkcontext as resource_id,
					'resource' as resource_type,
					r.name as resource_name,
					(select count(gr.fkcontext) from view_cm_group_resource_active gr where fkresource = r.fkcontext)||' {$groups}' as resource_detail,
					'{$function}: '||r.func as resource_acronym,
					comercialNumber as comercialNumber,
					homeNumber as homeNumber,
					celNumber as celNumber,
					nextelNumber as nextelNumber,
					email as email,
					'' as department,
					'' as func,
					pr.bisdefault as resource_is_default,
					'' as default_contact_info
				FROM
        	view_cm_resource_active r
        	JOIN cm_provider_resource pr on pr.fkresource = r.fkcontext
        WHERE
        	pr.fkprovider = {$this->providerId}
        	AND r.provider = 1 
        GROUP BY
        	r.fkcontext,
        	r.name,
        	r.func,
        	comercialNumber,
        	homeNumber,
        	celNumber,
        	nextelNumber,
        	email,
        	pr.bisdefault
    ";

		return $sql;
			  
	}
	
  public function makeQueryTemp(){
    
    if($this->provider){
      
      /* Fornecedores */
      
      if($this->providerId){
        $this->csSQL = $this->getProvidersOfGroup();
      }
      else if($this->placeId){
        $this->csSQL = $this->getProvidersOfPlace();
      }
      else if($this->unId){//NOVO
        $this->csSQL = $this->getProvidersOfUN();
      }      
      else{
        $this->csSQL = $this->getProviders();
      }
      
    } else {
      
      /* Recursos */
      
      if($this->placeId){
        
        if($this->cmt){
          $this->csSQL = $this->getCMTOfPlace();          
        } else {
          $this->csSQL = $this->getResourcesOfPlace();
        }
        
      }
      else if($this->unId){
        $this->csSQL = $this->getResourcesOfUN();
      }
      else if($this->groupId){
        $this->csSQL = $this->getResourcesOfGroup();
      }
      else {
        $this->csSQL = $this->getResources();
      }      
      
    }
  }
  
}

?>