<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGridProcessSearch.
 *
 * <p>Consulta para popular o grid de pesquisa de escopos.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryGridScopeViewPlaces extends FWDDBQueryHandler {

	private $scopeId = 0;

	public function __construct($poDB){
		parent::__construct($poDB);

		$this->coDataSet->addFWDDBField(new FWDDBField('','placeId',         	DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField('','placeName',       	DB_STRING));
	}

	public function setScope($scopeId) {
		$this->scopeId = $scopeId;
	}

	public function makeQuery(){
		$this->csSQL = "SELECT
							place.sname as placeName,
							place.fkcontext as placeId
						FROM  
							view_cm_place_active place 
							JOIN view_cm_scope_place_active sp on sp.fkplace = place.fkcontext
						WHERE
							sp.fkscope = {$this->scopeId}";
	}

}
?>