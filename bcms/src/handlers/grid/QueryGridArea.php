<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGridArea.
 *
 * <p>Consulta para popular a grid de �reas.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryGridArea extends FWDDBQueryHandler {

	protected $csName = '';

	protected $csNameDescription = '';

	protected $ciRootArea = 0;

	protected $scopeId = 0;

	protected $placeId = 0;

	public function __construct($poDB=null){
		parent::__construct($poDB);
		$this->coDataSet->addFWDDBField(new FWDDBField('a.fkContext'		,'area_id'            	,DB_NUMBER  ));
		$this->coDataSet->addFWDDBField(new FWDDBField('a.sName'    		,'area_name'          	,DB_STRING  ));
		$this->coDataSet->addFWDDBField(new FWDDBField('area_responsible'	,'area_responsible'   	,DB_STRING  ));
		$this->coDataSet->addFWDDBField(new FWDDBField('place'				,'place'   				,DB_STRING  ));
		$this->coDataSet->addFWDDBField(new FWDDBField(''           		,'area_process_count' 	,DB_NUMBER  ));
		$this->coDataSet->addFWDDBField(new FWDDBField(''           		,'subarea_count'      	,DB_NUMBER  ));
		$this->coDataSet->addFWDDBField(new FWDDBField('parent'           	,'parent'      			,DB_NUMBER  ));
	}

	public function setScope($scopeId){
		$this->scopeId = $scopeId;
	}

	public function setPlace($placeId){
		$this->placeId = $placeId;
	}

	public function setName($psName){
		$this->csName = $psName;
	}

	public function setNameDescription($psNameDescription){
		$this->csNameDescription = $psNameDescription;
	}

	public function setRootArea($piRootArea){
		$this->ciRootArea = $piRootArea;
	}

	public function makeQuery(){
		$maFilters = array();
		$maHavingFilters = array();

		if($this->csName){
			$maFilters[] = FWDWebLib::getCaseInsensitiveLike('a.sName',$this->csName);
		}

		if($this->csNameDescription){
			$maFilters[] = '('
			.FWDWebLib::getCaseInsensitiveLike('a.sName',$this->csNameDescription)
			.' OR '
			.FWDWebLib::getCaseInsensitiveLike('a.tDescription',$this->csNameDescription)
			.')';
		}

		if($this->ciRootArea){
			$maFilters[] = "a.fkParent = {$this->ciRootArea}";
		}else if(!$this->scopeId){
			$maFilters[] = "a.fkParent IS NULL";
		}

		$join = '';
		$parentConcat = '';
		if($this->scopeId){
			$join .= "JOIN cm_scope_area sa on sa.fkarea = a.fkcontext  ";
			$maFilters[] = " sa.fkscope = {$this->scopeId} ";
			$subArea = FWDLanguage::getPHPStringValue('subLocalOf',"Sub-local de");
			$parentConcat .= "||coalesce(('({$subArea} '||parent.sname||')'),'')";
		}

		if($this->placeId){
			//$join .= "JOIN cm_place_area pa on pa.fkarea = a.fkcontext  ";
			$maFilters[] = " a.fkplace = {$this->placeId} ";
			$subArea = FWDLanguage::getPHPStringValue('subLocalOf',"Sub-local de");
			$parentConcat .= "||coalesce(('({$subArea} '||parent.sname||')'),'')";
		}
				
		if(count($maFilters)==0){
			$msWhere = '';
		}else{
			$msWhere = ' WHERE '.implode(' AND ',$maFilters);
			
		}
		if(count($maHavingFilters)==0){
			$msHaving = '';
		}else{
			$msHaving = ' HAVING '.implode(' AND ',$maHavingFilters);
		}

		$this->csSQL = "SELECT DISTINCT
                      a.fkContext AS area_id,
                      a.sName{$parentConcat} AS area_name,
                      COUNT(p.fkContext) AS area_process_count,
                      sub.area_count AS subarea_count,
                      coalesce(gr.name,resource.name) as area_responsible,
                      a.fkparent as parent,
                      place.sname as place
                    FROM
                      view_rm_area_active a
                      LEFT JOIN view_cm_place_active place on place.fkcontext = a.fkplace
                      LEFT JOIN view_cm_group_active gr on gr.fkcontext = a.fkgroup
                      LEFT JOIN view_cm_resource_active resource on resource.fkcontext = a.fkresource
                      LEFT JOIN view_rm_process_active p ON (p.fkArea = a.fkContext)
                      LEFT JOIN view_rm_area_active parent on parent.fkcontext = a.fkparent
                      LEFT JOIN (
                        SELECT fkParent, COUNT(fkContext) AS area_count
                        FROM view_rm_area_active a
                        GROUP BY fkParent
                      ) sub ON (sub.fkParent = a.fkContext)
                      {$join}
                      {$msWhere}
                    GROUP BY
                      a.fkContext,
                      a.sName,
                      resource.name,
                      gr.name,
                      sub.area_count,
                      parent.sname,
                      a.fkparent,
                      place.sname
                      {$msHaving}
";

	                    //ORDER BY
                    	//place.sname,
                    	//a.sname
                    	//{$msHaving}
                      
                      
                    	//print $this->csSQL;
	}

}

?>