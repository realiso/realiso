<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

class QueryGridPlanSchedule extends FWDDBQueryHandler {

	private $plan = null;
	
	public function __construct($poDB=null){
		parent::__construct($poDB);
		$this->coDataSet->addFWDDBField(new FWDDBField(''  				,'id'              	,DB_NUMBER  ));
		$this->coDataSet->addFWDDBField(new FWDDBField(''      			,'name'            	,DB_STRING  ));
		$this->coDataSet->addFWDDBField(new FWDDBField(''               ,'date'				,DB_DATETIME));
		$this->coDataSet->addFWDDBField(new FWDDBField(''               ,'status'			,DB_NUMBER  ));
		$this->coDataSet->addFWDDBField(new FWDDBField(''      			,'responsible'     	,DB_STRING  ));
	}
	
	public function setPlan($plan){
		$this->plan = $plan;
	}

	public function makeQuery(){

		//$this->getDataset()->setOrderBy("date","-");
		//$this->getDataset()->setOrderBy("name","+");

		$where = "";
		if($this->plan){
			$where .= " and s.fkplan = ".$this->plan;
		}
		
		$this->csSQL = "select
							s.fkcontext as id,
							p.name as name,
							s.date as date,
							s.status as status,
							coalesce(r.name, g.name) as responsible
						from
							view_cm_plan_schedule_active s
							join view_cm_plan_active p on s.fkplan = p.fkcontext
							left join view_cm_resource_active r on r.fkcontext = s.fkresponsible
							left join view_cm_group_active g on g.fkcontext = s.fkgroup
						where 1=1
						{$where}";
	}
}
?>