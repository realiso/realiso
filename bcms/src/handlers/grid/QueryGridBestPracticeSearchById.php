<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGridBestPracticeSearchById.
 *
 * <p>Consulta para popular o grid de best_practices selecionados.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryGridBestPracticeSearchById extends FWDDBQueryHandler {	

	private $csSectionsIds;
	
	public function __construct($poDB) {
		parent::__construct($poDB);	

		$this->coDataSet->addFWDDBField(new FWDDBField("ctrl.fkContext","best_practice_id", DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("ctrl.sName","best_practice_name", DB_STRING));
	}

	public function setSectionsIds($psSectionsIds){
		$this->csSectionsIds = $psSectionsIds;
	}
	
	public function getIds(){
		return array_filter(array_unique(explode(':',$this->csSectionsIds)));
	}

	public function makeQuery() {
		$msWhere = '';
		
		if($this->csSectionsIds){
			$msSecIds = '';
			$maSecIds = array_filter(array_unique(explode(':',$this->csSectionsIds)));
			foreach($maSecIds as $msId)
					if($msSecIds)
						$msSecIds .=", $msId";
					else
						$msSecIds .=$msId;
		
			if($msSecIds)
					$msWhere .= " WHERE bp.fkcontext IN (".$msSecIds.") ";
		}
		else
			$msWhere .= " WHERE bp.fkcontext = 0 ";
		
		$this->csSQL =" 
		SELECT bp.fkcontext as best_practice_id, bp.sname as best_practice_name
		FROM rm_best_practice bp
		  JOIN isms_context c ON (bp.fkContext = c.pkContext AND c.nState <> ".CONTEXT_STATE_DELETED." )
		". $msWhere;
	}
	
	public function getSelectSessionInfo(){
		$this->makeQuery();
		if($this->executeQuery()){
			$maReturn = array();
			$moDataSet = $this->getDataset();
			while($moDataSet->fetch()){
				$maReturn[] = array($moDataSet->getFieldByAlias('best_practice_id')->getValue(),str_replace(array("\n","\r"),array(' ',''),$moDataSet->getFieldByAlias('best_practice_name')->getValue()));
			}
			return $maReturn;
		}
		else
			return array();
	} 
}
?>