<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGridRiskSearch.
 *
 * <p>Consulta para popular o grid de pesquisa de riscos.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryGridRiskSearch extends FWDDBQueryHandler {
  
  protected $csName = '';
  
  protected $csAssetName = '';
  
  protected $ciControlId = 0;
  
  public function __construct($poDB){
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField('r.fkContext',     'risk_id',            DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('r.sName',         'risk_name',          DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('r.nValueResidual','risk_residual_value',DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('a.sName'         ,'asset_name'         ,DB_STRING));
  }
  
  public function setControlId($piControlId){
    $this->ciControlId = $piControlId;
  }
  
  public function setName($psName){
    $this->csName = $psName;
  }
  
  public function setAssetName($psAssetName){
    $this->csAssetName = $psAssetName;
  }
  
  public function makeQuery(){
    $maFilters = array();
    
    $maFilters[] = "r.nValue != 0";
    
    if($this->ciControlId){
      $maFilters[] = "r.fkContext NOT IN (SELECT fkRisk FROM rm_risk_control rc WHERE rc.fkControl = {$this->ciControlId})";
    }
    
    if($this->csName){
      $maFilters[] = "UPPER(r.sName) LIKE '%".strtoupper($this->csName)."%'";
    }
    
    if($this->csAssetName){
      $maFilters[] = "UPPER(a.sName) LIKE '%".strtoupper($this->csAssetName)."%'";
    }
    
    if(count($maFilters)==0){
      $msWhere = '';
    }else{
      $msWhere = ' WHERE '.implode(' AND ',$maFilters);
    }
    $this->csSQL = "SELECT
                      r.fkContext AS risk_id,
                      r.sName AS risk_name,
                      r.nValueResidual AS risk_residual_value,
                      a.sName AS asset_name
                    FROM
                      view_rm_risk_active r
                      JOIN view_rm_asset_active a ON (a.fkContext = r.fkAsset)
                    {$msWhere}";
  }

}
?>