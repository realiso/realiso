<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

class QueryGridAssetSearch extends FWDDBQueryHandler {

	private $csNameFilter = "";

	private $caIds = array();

	private $caExcludedIds = array();

	private $cbEmpty = false;

	private $scopeId = 0;

	private $processId = 0;

	public function __construct($poDB){
		parent::__construct($poDB);

		$this->coDataSet->addFWDDBField(new FWDDBField('','asset_id',     		     DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField('','asset_name',     		 DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField('','asset_description',       DB_STRING));
	}

	public function setNameFilter($psNameFilter) {
		$this->csNameFilter = $psNameFilter;
	}

	public function setScope($scope) {
		$this->scopeId = $scope;
	}

	public function setProcess($process) {
		$this->processId = $process;
	}

	public function setIds($paIds){
		$this->caIds = $paIds;
	}

	public function setExcludedIds($paExcludedIds){
		$this->caExcludedIds = $paExcludedIds;
	}

	public function setEmpty($pbEmpty){
		$this->cbEmpty = $pbEmpty;
	}

	public function makeQuery(){
		$msWhere = '';
		$join = '';

		if($this->cbEmpty){
			$msWhere.= " AND p.fkContext = 0";
		}

		if($this->csNameFilter){
			$msWhere.= " AND upper(p.sName) like upper('%{$this->csNameFilter}%')";
		}

		if(count($this->caIds)){
			$msWhere .= " AND p.fkContext IN (".implode(',',$this->caIds).")";
		}

		if(count($this->caExcludedIds)){
			$msWhere .= " AND p.fkContext NOT IN (".implode(',',$this->caExcludedIds).")";
		}

		if($this->scopeId){
			$msWhere .= " AND ((p.fkContext IN (
				select 
					pa.fkasset 
				from 
					rm_process_asset pa 
					join cm_scope_process sp on sp.fkprocess = pa.fkprocess 
				where 
					fkscope = {$this->scopeId}))
			OR p.fkcontext in (
				select 
					paa.fkasset 
				from 
					cm_process_activity_asset paa 
					join cm_process_activity pa on pa.fkcontext = paa.fkactivity 
					join rm_process pr on pr.fkcontext = pa.fkprocess 
					join cm_scope_process sp on sp.fkprocess = pa.fkcontext 
				where 
					sp.fkscope = {$this->scopeId}) 
			OR p.fkcontext in (
				select
					pa.fkasset
				from 
					cm_place_asset pa
					JOIN cm_scope_place sp on sp.fkplace = pa.fkplace
				where
					sp.fkscope = {$this->scopeId}))";
		}

		if($this->processId){
			$join .= " JOIN view_rm_process_asset_active pa ";
			$msWhere .= "  and pa.fkprocess = {$this->processId}";
		}

		$this->csSQL = "SELECT
						p.fkContext as asset_id,  
						p.sName as asset_name,
						p.tDescription as asset_description 
					FROM  
						view_rm_asset_active p
						{$join}
					WHERE 1=1 "
					.$msWhere;
					//					print_r("<pre>{$this->csSQL}</pre>");
	}
}
?>