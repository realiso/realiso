<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGridPlace.
 *
 * <p>Consulta para popular a grid de locais.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryGridActivityAsset extends FWDDBQueryHandler {

  protected $csName = '';

  protected $csNameDescription = '';

  protected $ciMaxAssets = 0;

  protected $ciMinAssets = 0;

  protected $caValueFilter = array();

  protected $ciActivity = 0;
  
  public function __construct($poDB=null){
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField('aa.fkContext'    ,'activity_asset_id'              ,DB_NUMBER  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('a.sName'         ,'asset_name'  		           ,DB_STRING  ));
    $this->coDataSet->addFWDDBField(new FWDDBField(''		         ,'activity_asset_threat_count'    ,DB_NUMBER  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('r.sName'	     ,'asset_responsible' 			   ,DB_STRING  ));
  }

  public function setName($psName){
    $this->csName = $psName;
  }


  public function setMaxAssets($piMaxAssets){
    $this->ciMaxAssets = $piMaxAssets;
  }

  public function setMinAssets($piMinAssets){
    $this->ciMinAssets = $piMinAssets;
  }

  public function setValueFilter($paValueFilter){
    $this->caValueFilter = $paValueFilter;
  }
  
  public function setActivity($piActivityId) {
  	$this->ciActivity = $piActivityId;
  }

  
  public function makeQuery(){
    $maFilters = array();
    $maHavingFilters = array();
    
    
    if($this->csName){
      $maFilters[] = FWDWebLib::getCaseInsensitiveLike('a.sName',$this->csName);
    }
    
    if($this->csNameDescription){
      $maFilters[] = '('
                        .FWDWebLib::getCaseInsensitiveLike('a.sName',$this->csNameDescription)
                    .')';
    }
        
    if($this->ciMaxAssets){
      $maHavingFilters[] = "COUNT(a.fkcontext) <= {$this->ciMaxAssets}";
    }
    
    if($this->ciMinAssets){
      $maHavingFilters[] = "COUNT(a.fkcontext) >= {$this->ciMinAssets}";
    }
    
                
    if(count($maFilters)==0){
      $msWhere = '';
    }else{
      $msWhere = ' AND '.implode(' AND ',$maFilters);
    }
    if(count($maHavingFilters)==0){
      $msHaving = '';
    }else{
      $msHaving = ' HAVING '.implode(' AND ',$maHavingFilters);
    }
    $this->csSQL = "SELECT
                      aa.fkContext AS activity_asset_id,
                      a.sName AS asset_name,
                      ( SELECT COUNT (fkcontext) as count_asset_threats FROM cm_asset_threat cat WHERE cat.fkasset = a.fkContext) as activity_asset_threat_count,
                      r.sName as asset_responsible
                    FROM
                      isms_context c
                      JOIN cm_process_activity_asset aa ON (aa.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED.")
                      JOIN rm_asset a ON (a.fkcontext = aa.fkasset) 
                      JOIN isms_user r ON (r.fkcontext = a.fkresponsible) 
                      WHERE aa.fkactivity = {$this->ciActivity}                        
                    {$msWhere}
                    GROUP BY 
                      a.fkcontext,
                      aa.fkContext,
                      a.sName,
                      r.sName 
                    {$msHaving}";
  }

}

?>