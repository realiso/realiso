<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe QueryGridIncident
 *
 * <p>Consulta para popular a grid de incidentes.</p>
 * @package ISMS
 * @subpackage classes
 */
class QueryGridIncident extends FWDDBQueryHandler {

  protected $ciStatus;

  /**
  * Construtor.
  * 
  * <p>Construtor da classe.</p>
  * @access public 
  */
  public function __construct($poDB){
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField('i.fkContext'           ,'incident_id'                  ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('i.sName'               ,'incident_name'                ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('u.sName'               ,'incident_responsible'         ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('c.sName'               ,'incident_category'            ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('i.nLossType'           ,'incident_loss_type'           ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('i.dDateLimit'          ,'incident_date_limit'          ,DB_DATETIME));
    $this->coDataSet->addFWDDBField(new FWDDBField('ctx.nState'            ,'incident_state'               ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('u.fkContext'           ,'incident_responsible_id'      ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('i.tDisposalDescription','incident_disposal_description',DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('i.tSolutionDescription','incident_solution_description',DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('i.dDateLimit'          ,'incident_date_limit'          ,DB_DATETIME));
    $this->coDataSet->addFWDDBField(new FWDDBField('i.dDateFinish'         ,'incident_date_finish'         ,DB_DATETIME));
    
    $this->coDataSet->addFWDDBField(new FWDDBField('ch.context_creator_name',   'user_create_name', DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('ch.context_date_created',   'date_create',      DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('ch.context_modifier_name',  'user_edit_name',   DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('ch.context_date_modified',  'date_edit',        DB_STRING));
    
    $this->coDataSet->addFWDDBField(new FWDDBField('t.pkTask'    ,'task_id'      ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('t.fkReceiver','task_receiver',DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField(''            ,'risk_count'   ,DB_NUMBER));
  }
  
  public function makeQuery(){
    $maFilters = array();
    $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    if ($this->ciStatus) {
      $maFilters[] = "ctx.nState = '{$this->ciStatus}'"; 
    }
    
    /*verifica permiss�o de listar todos*/
    $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
    if(in_array('M.CI.2.1',$maACLs)&&($miUserId != ISMSLib::getConfigById(USER_INCIDENT_MANAGER))){
      $msUserIN = " AND u.fkContext = ".$miUserId;
    }else{
      $msUserIN = "";
    }

    $msWhere = count($maFilters)?" WHERE ".implode(" AND ",$maFilters):"";
    $this->csSQL = "SELECT
                      i.fkContext as incident_id,
                      i.sName as incident_name,
                      u.sName as incident_responsible,
                      c.sName as incident_category,
                      i.nLossType as incident_loss_type,
                      i.dDateLimit as incident_date_limit,
                      ctx.nState as incident_state,
                      u.fkContext as incident_responsible_id,
                      i.dDateFinish as incident_date_finish,
                      i.tDisposalDescription as incident_disposal_description,
                      i.tSolutionDescription as incident_solution_description,
                      ch.context_creator_name as user_create_name, 
                      ch.context_date_created as date_create, 
                      ch.context_modifier_name as user_edit_name, 
                      ch.context_date_modified as date_edit,
                      t.pkTask as task_id,
                      t.fkReceiver as task_receiver,
                      (SELECT count(r.fkContext) FROM view_rm_risk_active r JOIN ci_incident_risk ri ON (r.fkContext = ri.fkRisk AND ri.fkIncident = i.fkContext)) as risk_count
                    FROM
                      view_ci_incident_active i
                      LEFT JOIN view_ci_category_active c ON (i.fkCategory = c.fkContext)
                      JOIN view_isms_user_active u ON (i.fkResponsible = u.fkContext $msUserIN)
                      JOIN view_isms_context_active ctx ON (i.fkContext=ctx.pkContext)
                      JOIN context_history ch ON (i.fkContext = ch.context_id)
                      LEFT JOIN wkf_task t ON (t.fkContext=i.fkContext AND t.bVisible=1) 
                    $msWhere";
  }
  
  public function setStatusFilter($piStatus) {
    if (intval($piStatus) > 0) $this->ciStatus = $piStatus;
  }

}

?>