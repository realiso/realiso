<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGridActionPlanSearch.
 *
 * <p>Consulta para popular a grid de busca.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryGridActionPlanSearch extends FWDDBQueryHandler {

  protected $caIds = array();

  protected $caExcludedIds = array();

  protected $csName = '';

  protected $caExcludedStates = array(CONTEXT_STATE_DENIED,CONTEXT_STATE_AP_MEASURED);

  protected $ciTypeAc;

  public function __construct($poDB=null){
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField('fkcontext','ap_id'  ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('sname'    ,'ap_name',DB_STRING));
  }

  public function setIds($paIds){
    $this->caIds = $paIds;
  }

  public function setTypeAc($piTypeAc){
    $this->ciTypeAc = $piTypeAc;
  }

  public function setExcludedIds($paExcludedIds){
    $this->caExcludedIds = $paExcludedIds;
  }

  public function setName($psName){
    $this->csName = $psName;
  }

  public function setExcludedStateFilter($paStates){
    $this->caExcludedStates = $paStates;
  }

  public function makeQuery(){
    $maFilters = array();

    if(count($this->caIds)>0){
      $maFilters[] = 'fkcontext IN ('.implode(',',$this->caIds).')';
    }

    if(count($this->caExcludedIds)>0){
      $maFilters[] = 'fkcontext NOT IN ('.implode(',',$this->caExcludedIds).')';
    }

    if($this->csName){
      $maFilters[] = "sname LIKE '%{$this->csName}%'";
    }

    if(count($this->caExcludedStates)){
      $maFilters[] = " c.nState NOT IN ( ".implode(',',$this->caExcludedStates)." ) ";
    }

    if($this->ciTypeAc){
      $maFilters[] = " pa.nActionType = ". $this->ciTypeAc;
    }

    if(count($maFilters)==0){
      $msWhere = '';
    }else{
      $msWhere = ' WHERE '.implode(' AND ',$maFilters);
    }
    $this->csSQL = "SELECT
                      fkcontext AS ap_id,
                      sname AS ap_name
                    FROM view_ci_action_plan_active pa
                    JOIN isms_context c ON (pa.fkContext = c.pkContext)
                    $msWhere";
  }

}

?>