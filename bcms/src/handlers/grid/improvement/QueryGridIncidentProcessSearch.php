<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGridIncidentProcessSearch.
 *
 * <p>Consulta para popular o grid de look-up de processos e de processos relacionados a incidentes.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryGridIncidentProcessSearch extends FWDDBQueryHandler {  
  
  protected $ciId = "";
  
  protected $csName = "";
  
  protected $cbFilter = false;
  
  protected $csIdsNotIN = "";
  
  protected $csIdsIN = "";
  public function __construct($poDB) {
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField("", "context_id",    DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("", "context_name",  DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("", "context_value", DB_NUMBER));
  }
  
  public function setId($piId) {
    $this->ciId = $piId;
  }
  
  public function setName($psName) {
    $this->csName = $psName ? " upper(p.sName) like upper('%$psName%')" : "";
  }
  
  public function setFilter($pbFilter){
    $this->cbFilter = $pbFilter;
  }
  
  public function setIdsNotIn($psIdsNotIN){
    $this->csIdsNotIN = $psIdsNotIN;
  }
  
  public function setIdsIn($psIdsIN){
    $this->csIdsIN = $psIdsIN;
  }
  public function makeQuery() {
    $msWhere = '';
    if($this->cbFilter){
      $msWhere .="
  JOIN view_rm_process_asset_active pa ON (p.fkContext = pa.fkProcess)
  JOIN view_rm_asset_active a ON (a.fkContext = pa.fkAsset)
  JOIN view_rm_risk_active r ON (r.fkAsset = a.fkContext)
  JOIN view_ci_incident_risk_active ir ON (r.fkContext = ir.fkRisk)
";
  $msWhere .= " WHERE ir.fkIncident = $this->ciId ";       
    }
    if($this->csName){
      $msWhere = $msWhere? $msWhere . " AND ":" WHERE ";
      $msWhere .= $this->csName;
    }
    
    $msAuxIdsIN = implode(',',array_filter(explode(':',$this->csIdsIN)));
    if($msAuxIdsIN){
      $msWhere = (($msWhere)? $msWhere . " AND " : " WHERE ");
      $msWhere .= "p.fkContext IN ($msAuxIdsIN) ";
    }
    $msIdsNotIN = implode(',',array_filter(explode(':',$this->csIdsNotIN)));
    if($msIdsNotIN){
      $msWhere = (($msWhere)? $msWhere . " AND  " : " WHERE ");
      $msWhere .= "p.fkContext NOT IN ($msIdsNotIN) ";
    }


    $this->csSQL = "
SELECT DISTINCT
       p.fkContext as context_id,
       p.sName as context_name,
       p.nValue as context_value
  FROM view_rm_process_active p
  $msWhere
";
  }

  public function getValues(){
    $this->makeQuery();
    $this->executeQuery();
    $maValues = array();
     while($this->coDataSet->fetch()){
      $maValues [] =$this->coDataSet->getFieldByAlias("context_id")->getValue();
    }
    return $maValues;
  }
}
?>