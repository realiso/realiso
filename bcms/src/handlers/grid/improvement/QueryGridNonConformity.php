<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe QueryGridNonConformity
 *
 * <p>Consulta para popular a grid de n�o-conformidades.</p>
 * @package ISMS
 * @subpackage classes
 */
class QueryGridNonConformity extends FWDDBQueryHandler {

  protected $ciStatus;
  
  protected $ciActionPlan = 0;

  public function __construct($poDB=null){
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField('n.fkcontext'             ,'nc_id'            ,DB_NUMBER  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('u2.fkcontext'            ,'nc_responsible'   ,DB_NUMBER  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('u.sname'                 ,'nc_sender'        ,DB_NUMBER  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('n.sname'                 ,'nc_name'          ,DB_STRING  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('n.tdescription'          ,'nc_description'   ,DB_STRING  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('n.nclassification'       ,'nc_classification',DB_NUMBER  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('n.ncapability'           ,'nc_capability'    ,DB_NUMBER  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('n.ddatesent'             ,'nc_datesent'      ,DB_DATETIME));
    $this->coDataSet->addFWDDBField(new FWDDBField('ctx.nstate'              ,'nc_state'         ,DB_NUMBER  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('ch.context_creator_name' ,'creator_name'     ,DB_STRING  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('ch.context_date_created' ,'date_created'     ,DB_DATETIME));
    $this->coDataSet->addFWDDBField(new FWDDBField('ch.context_modifier_name','modifier_name'    ,DB_STRING  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('ch.context_date_modified','date_modified'    ,DB_DATETIME));

    $this->coDataSet->addFWDDBField(new FWDDBField('t.pkTask'                ,'task_id'          ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('t.fkReceiver'            ,'task_receiver'    ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField(''                        ,'count_action_plan',DB_NUMBER));
  }
  
  public function setActionPlan($piId){
    $this->ciActionPlan = $piId;
  }
  public function makeQuery(){
    $maFilters = array();
    $msJoinAP = '';
    if($this->ciActionPlan){
      $msJoinAP .= " JOIN ci_nc_action_plan ncap2 ON (ncap2.fkNC = n.fkContext AND ncap2.fkActionPlan = ".$this->ciActionPlan.")"; 
    }
    
    if ($this->ciStatus) {
      $maFilters[] = "ctx.nState = '{$this->ciStatus}'"; 
    }

    
    $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    /*verifica permiss�o de listar todos*/
    $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
    if(in_array('M.CI.3.1',$maACLs)){
      $maFilters[] =" n.fkResponsible = ".$miUserId;
    }
  
    $msWhere = count($maFilters)?" WHERE ".implode(" AND ",$maFilters):"";
    
      $this->csSQL = "
  SELECT
    n.fkcontext AS nc_id,
    u.sname AS nc_sender,
    n.sname AS nc_name,
    n.tdescription AS nc_description,
    n.nclassification AS nc_classification,
    n.ncapability AS nc_capability,
    n.ddatesent AS nc_datesent,
    ctx.nstate AS nc_state,
    ch.context_creator_name AS creator_name,
    ch.context_date_created AS date_created,
    ch.context_modifier_name AS modifier_name,
    ch.context_date_modified AS date_modified,
    t.pkTask AS task_id,
    t.fkReceiver AS task_receiver,
    (SELECT count(ncap.fkActionPlan) FROM ci_nc_action_plan ncap JOIN view_ci_action_plan_active ap ON (ncap.fkActionPlan = ap.fkContext) WHERE ncap.fkNC = n.fkContext) as count_action_plan
  FROM 
    view_ci_nc_active n
    JOIN isms_user u ON (u.fkcontext = n.fksender)
    JOIN isms_context ctx ON (ctx.pkcontext = n.fkcontext)
    JOIN context_history ch ON (ch.context_id = n.fkcontext)
    LEFT JOIN isms_user u2 ON (u2.fkContext = n.fkResponsible)
    LEFT JOIN wkf_task t ON (t.fkContext=n.fkContext AND t.bVisible=1) 
    $msJoinAP
  $msWhere";
  }

  public function setStatusFilter($piStatus) {
    if (intval($piStatus) > 0) $this->ciStatus = $piStatus;
  }
}

?>