<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe QueryGridIncidentUser
 *
 * <p>Consulta para popular a grid de processo disciplinar.</p>
 * @package ISMS
 * @subpackage classes
 */
class QueryGridIncidentUser extends FWDDBQueryHandler {
  
  protected $ciIncidentId=0;
  
  /**
  * Construtor.
  * 
  * <p>Construtor da classe.</p>
  * @access public 
  */
  public function __construct($poDB){
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField('iu.fkIncident',    'incident_id',  DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('iu.fkUser',        'user_id',      DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('u.sName',          'user_name',    DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('iu.tDescription',  'description',  DB_STRING));
  }
  
  public function makeQuery(){
    $maFilters = array();
    if ($this->ciIncidentId)
      $maFilters[] = "iu.fkIncident='{$this->ciIncidentId}'";
      
    $msWhere = count($maFilters)?" WHERE ".implode(" AND ",$maFilters):"";
    $this->csSQL = "SELECT
                      iu.fkIncident as incident_id,
                      iu.fkUser as user_id,
                      u.sName as user_name,
                      iu.tDescription as description
                    FROM
                      ci_incident_user iu
                      JOIN view_isms_user_active u ON (iu.fkUser=u.fkContext)
                    $msWhere";
  }

  public function setIncident($piIncidentId) {
    $this->ciIncidentId = intval($piIncidentId) > 0?$piIncidentId:0;
  }
  
  public function getCurrentUsers(){
    $this->makeQuery();
    if($this->executeQuery()){
      $maReturn = array();
      $moDataSet = $this->getDataset();
      while($moDataSet->fetch()){
        $maReturn[] = $moDataSet->getFieldByAlias('user_id')->getValue();
      }
      return $maReturn;
    }
    else
      return array();
  } 
}

?>