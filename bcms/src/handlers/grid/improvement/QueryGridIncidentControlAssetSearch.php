<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGridIncidentControlAssetSearch.
 *
 * <p>Consulta para popular o grid do filtro de riscos por controle e ativo.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryGridIncidentControlAssetSearch extends FWDDBQueryHandler {  
  
  protected $csControlIds = "";
  
  protected $csAssetIds = "";
  
  public function __construct($poDB) {
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField("fkContext", "context_id",     DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("sName",     "context_name",   DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("",          "stype",          DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("",          "context_value",  DB_NUMBER));
  }
  
  public function setControlIds($psControlIds) {
    $this->csControlIds = $psControlIds;
  }
  
  public function setAssetIds($psAssetIds){
    $this->csAssetIds = $psAssetIds;
  
  }
  
  public function makeQuery() {
    $msWhereControl = ' WHERE fkContext IN (0) ';
    $msWhereAsset = ' WHERE fkContext IN (0) ';
    if($this->csControlIds)
      $msWhereControl = " WHERE fkContext IN (".implode(',',array_filter(explode(':',$this->csControlIds))).")";
    if($this->csAssetIds){
      $msWhereAsset = " WHERE fkContext IN (".implode(',',array_filter(explode(':',$this->csAssetIds))).")";
    }
    
    $this->csSQL = "
SELECT fkContext as context_id,
       sName as context_name,
       'asset' as stype,
       nValue as context_value
  FROM view_rm_asset_active
  $msWhereAsset

UNION

SELECT fkContext as context_id,
       sName as context_name,
       'control' as stype,
       bIsActive as context_value
  FROM view_rm_control_active
  $msWhereControl

";
  }
}
?>