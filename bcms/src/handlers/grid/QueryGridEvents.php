<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGridEvents.
 *
 * <p>Consulta para popular o grid de eventos.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryGridEvents extends FWDDBQueryHandler {	
		
	protected $ciCategoryId = 0;
	
	public function __construct($poDB) {
		parent::__construct($poDB);	

		$this->coDataSet->addFWDDBField(new FWDDBField("res.context_id","context_id", DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("res.context_name","context_name", DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("res.context_impact","context_impact", DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField("res.context_type","context_type", DB_NUMBER));			
	
		$this->coDataSet->addFWDDBField(new FWDDBField("uc.sName","user_create_name", DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField("cc.dDate","date_create", DB_DATETIME));
		$this->coDataSet->addFWDDBField(new FWDDBField("ud.sName","user_edit_name", DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField("ce.dDate","date_edit", DB_DATETIME));
	}
	
	public function setRootCategory($piCategoryId) {
		$this->ciCategoryId = $piCategoryId;
	}

	public function makeQuery() {		
		$msUnion = "";
		$msWhere = " WHERE fkParent";
		if ($this->ciCategoryId) {
			$msWhere .= " = {$this->ciCategoryId}";
			$msUnion = " UNION						
					SELECT e.fkContext as context_id, e.sDescription as context_name, e.tImpact as context_impact, " . CONTEXT_EVENT . " as context_type
					FROM rm_event e JOIN isms_context c ON (e.fkContext = c.pkContext AND c.nState <> " . CONTEXT_STATE_DELETED . ")
					WHERE fkCategory = {$this->ciCategoryId}" ;		
		} 
		else $msWhere .= " IS NULL";
						
		$this->csSQL = "SELECT res.context_id, res.context_name, res.context_impact, res.context_type, ch.context_creator_name as user_create_name, ch.context_date_created as date_create, ch.context_modifier_name as user_edit_name, ch.context_date_modified as date_edit FROM
						(
						SELECT cat.fkContext as context_id, cat.sName as context_name, CAST('' as VARCHAR) as context_impact, " . CONTEXT_CATEGORY . " as context_type
						FROM rm_category cat JOIN isms_context c ON (cat.fkContext = c.pkContext AND c.nState <> " . CONTEXT_STATE_DELETED . ")
						$msWhere
						$msUnion											
						) res
						JOIN context_history ch ON (res.context_id = ch.context_id) "; 
								
	  $this->coDataSet->setOrderBy("context_type","+");
	} 
	
}
?>