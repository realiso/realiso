<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

class QueryGridPlanRange extends FWDDBQueryHandler {

	protected $ciMaxImpacts = 0;

	protected $ciMinImpacts = 0;

	public function __construct($poDB=null){
		parent::__construct($poDB);
		$this->coDataSet->addFWDDBField(new FWDDBField('fkcontext'   		,'id'              ,DB_NUMBER  ));
		$this->coDataSet->addFWDDBField(new FWDDBField('i.description'      ,'description'            ,DB_STRING  ));
		$this->coDataSet->addFWDDBField(new FWDDBField('i.startRange'       ,'startRange'            ,DB_NUMBER  ));
		$this->coDataSet->addFWDDBField(new FWDDBField(''       ,'rangeFlag'            ,DB_NUMBER  ));
	}

	public function setMaxImpacts($piMaxImpacts){
		$this->ciMaxImpacts = $piMaxImpacts;
	}

	public function setMinImpacts($piMinImpacts){
		$this->ciMinImpacts = $piMinImpacts;
	}

	public function makeQuery(){
		$maHavingFilters = array();

		if($this->ciMaxImpacts){
			$maHavingFilters[] = "COUNT(i.fkcontext) <= {$this->ciMaxImpacts}";
		}	

		if($this->ciMinImpacts){
			$maHavingFilters[] = "COUNT(i.fkcontext) >= {$this->ciMinImpacts}";
		}

		if(count($maHavingFilters)==0){
			$msHaving = '';
		}else{
			$msHaving = ' HAVING '.implode(' AND ',$maHavingFilters);
		}
		
		$this->getDataset()->setOrderBy("rangeFlag","+");
		
		$to = FWDLanguage::getPHPStringValue('to',' � ');
		$from = FWDLanguage::getPHPStringValue('from',' De ');
		
		$this->csSQL = "SELECT
						i.fkcontext as id,
						case when i.description = '' then '{$from}'||i.startRange||'%'||i.startRangeFlag||'%'||'{$to}'||i.endRange||'%'||i.endRangeFlag||'%' else i.description end as description,
						i.startRange,
						i.startRange*(case when i.startRangeFlag = 2 then 60 else case when i.startRangeFlag = 3 then (60*24) else case when i.startRangeFlag = 4  then (60*24*30) else 1 end end end) as rangeFlag
                    FROM
                      view_cm_plan_range_active i
                    WHERE 1=1                         
                    {$msHaving}";

	}

}

?>