<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGridProcessSearch.
 *
 * <p>Consulta para popular o grid de pesquisa de escopos.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryGridScopeSearch extends FWDDBQueryHandler {

  private $csNameFilter = "";

  private $ciAreaFilter = 0;

  private $caIds = array();

  private $caExcludedIds = array();
  
  private $cbEmpty = false;

  public function __construct($poDB){
    parent::__construct($poDB);

    $this->coDataSet->addFWDDBField(new FWDDBField('','scope_id',         	DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('','scope_name',       	DB_STRING));    
    $this->coDataSet->addFWDDBField(new FWDDBField('','scope_description',	DB_STRING));
  }

  public function setNameFilter($psNameFilter) {
    $this->csNameFilter = $psNameFilter;
  }

  public function setIds($paIds){
    $this->caIds = $paIds;
  }

  public function setExcludedIds($paExcludedIds){
    $this->caExcludedIds = $paExcludedIds;
  }

  public function setEmpty($pbEmpty){
    $this->cbEmpty = $pbEmpty;
  }

  public function makeQuery(){  	
  	$msWhere = '';
    
    if($this->cbEmpty){
      $msWhere.= " WHERE s.fkContext = 0";
    }
    
    if($this->csNameFilter){
      $msWhere.= " WHERE upper(s.sName) like upper('%{$this->csNameFilter}%')";
    }
        
    // Filtro por ids (incluir na consulta)
    if(count($this->caIds)){
      $msWhere .= " WHERE s.fkContext IN (".implode(',',$this->caIds).")";
    }
    
    // Filtro por ids (excluir da consulta)
    if(count($this->caExcludedIds)){
      $msWhere .= " WHERE s.fkContext NOT IN (".implode(',',$this->caExcludedIds).")";
    }
    
    $this->csSQL = "SELECT  
						s.fkContext as scope_id,  
						s.sName as scope_name,
						s.sDescription as scope_description
					FROM  
						cm_scope s JOIN isms_context c ON(s.fkContext = c.pkContext AND c.nState <> " . CONTEXT_STATE_DELETED . ")  
					"
                    .$msWhere;
  }
  
}
?>