<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGridControlSearch.
 *
 * <p>Consulta para popular o grid de pesquisa de controles.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryGridControlSearch extends FWDDBQueryHandler {	
	
	protected $csName = "";
	
  protected $ciRiskId = 0;
  
  protected $cbOnlySuggested = false;
  
	public function __construct($poDB) {
		parent::__construct($poDB);

		$this->coDataSet->addFWDDBField(new FWDDBField("fkContext", "control_id",   DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("sName",     "control_name", DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField("bisactive",     "control_active", DB_NUMBER));
	}
  
  public function setName($psName) {
    $this->csName = $psName ? "upper(sName) like upper('%$psName%')" : "";
  }
  
  public function setRiskId($piRiskId) {
    $this->ciRiskId = $piRiskId;
  }
  
  public function setOnlySuggested($pbOnlySuggested) {
    $this->cbOnlySuggested = $pbOnlySuggested;
  }
	
	public function makeQuery() {
    $this->csSQL = "
SELECT fkContext as control_id, sName as control_name, bisactive as control_active
FROM rm_control c
      JOIN
     isms_context ctx ON (ctx.pkcontext = c.fkContext AND ctx.nstate != ".CONTEXT_STATE_DELETED.")
WHERE fkContext NOT IN
      (
      SELECT fkControl
      FROM rm_risk_control rc
      WHERE fkRisk = ".$this->ciRiskId."
      )";
    
    if ($this->csName) $this->csSQL .= " AND {$this->csName}";
    
    if ($this->cbOnlySuggested) $this->csSQL .= "
      AND fkContext IN
      (
      SELECT c.fkContext
      FROM rm_control c
            JOIN
           rm_control_best_practice cbp ON (c.fkContext = cbp.fkControl)
            JOIN
           rm_best_practice bp ON (cbp.fkBestPractice = bp.fkContext)
            JOIN
           rm_best_practice_event bpe ON (bp.fkContext = bpe.fkBestPractice)
            JOIN
           rm_event e ON (bpe.fkEvent = e.fkContext)
            JOIN
           rm_risk r ON (e.fkContext = r.fkEvent AND r.fkContext = ".$this->ciRiskId.")
      )";
	}
}
?>