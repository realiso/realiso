<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

class QueryGridImpactLibrary extends FWDDBQueryHandler {

	protected $csName = '';

	protected $csNameDescription = '';

	protected $ciMaxImpacts = 0;

	protected $ciMinImpacts = 0;

	protected $caValueFilter = array();

	protected $ciType = 0;

	protected $ciScene = 0;


	public function __construct($poDB=null){
		parent::__construct($poDB);
		$this->coDataSet->addFWDDBField(new FWDDBField('i.fkContext'    ,'impact_id'              ,DB_NUMBER  ));
		$this->coDataSet->addFWDDBField(new FWDDBField('i.sName'        ,'impact_name'            ,DB_STRING  ));
		$this->coDataSet->addFWDDBField(new FWDDBField('it.sname'       ,'impact_type'            ,DB_STRING  ));
	}

	public function setName($psName){
		$this->csName = $psName;
	}

	public function setMaxImpacts($piMaxImpacts){
		$this->ciMaxImpacts = $piMaxImpacts;
	}

	public function setMinImpacts($piMinImpacts){
		$this->ciMinImpacts = $piMinImpacts;
	}

	public function setValueFilter($paValueFilter){
		$this->caValueFilter = $paValueFilter;
	}

	public function setScene($piSceneId) {
		$this->ciScene = $piSceneId;
	}


	public function makeQuery(){
		$maFilters = array();
		$maHavingFilters = array();


		if($this->csName){
			$maFilters[] = FWDWebLib::getCaseInsensitiveLike('s.sName',$this->csName);
		}

		if($this->ciMaxImpacts){
			$maHavingFilters[] = "COUNT(i.fkcontext) <= {$this->ciMaxImpacts}";
		}

		if($this->ciMinImpacts){
			$maHavingFilters[] = "COUNT(i.fkcontext) >= {$this->ciMinImpacts}";
		}

		if(count($maFilters)==0){
			$msWhere = '';
		}else{
			$msWhere = ' AND '.implode(' AND ',$maFilters);
		}
		if(count($maHavingFilters)==0){
			$msHaving = '';
		}else{
			$msHaving = ' HAVING '.implode(' AND ',$maHavingFilters);
		}
		
		$this->getDataset()->setOrderBy("impact_type","+");
		
		$this->csSQL = "SELECT
                      i.fkContext AS impact_id,
                      i.sName AS impact_name,
					  it.sName AS impact_type
                    FROM
                      view_cm_impact_active i 
                      JOIN cm_impact_type it ON (it.fkcontext = i.ntype)
                    WHERE 1=1                         
                    {$msWhere}
                    GROUP BY
                      i.fkContext,
                      i.sName,
                      it.sName
                      {$msHaving}";

	}

}

?>