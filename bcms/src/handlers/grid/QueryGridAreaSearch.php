<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

class QueryGridAreaSearch extends FWDDBQueryHandler {

	private $csNameFilter = "";

	private $caIds = array();

	private $caExcludedIds = array();

	private $cbEmpty = false;

	private $scope = 0;

	public function __construct($poDB){
		parent::__construct($poDB);

		$this->coDataSet->addFWDDBField(new FWDDBField('','area_id',     		     DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField('','area_name',     		 DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField('','area_description',       DB_STRING));
	}

	public function setNameFilter($psNameFilter) {
		$this->csNameFilter = $psNameFilter;
	}

	public function setIds($paIds){
		$this->caIds = $paIds;
	}

	public function setScope($scope){
		$this->scope = $scope;
	}

	public function setExcludedIds($paExcludedIds){
		$this->caExcludedIds = $paExcludedIds;
	}

	public function setEmpty($pbEmpty){
		$this->cbEmpty = $pbEmpty;
	}

	public function makeQuery(){
		$msWhere = '';
		$join = "";
		if($this->cbEmpty){
			$msWhere.= "AND p.fkContext = 0";
		}

		if($this->csNameFilter){
			$msWhere.= " AND upper(p.sName) like upper('%{$this->csNameFilter}%')";
		}

		if(count($this->caIds)){
			$msWhere .= " AND p.fkContext IN (".implode(',',$this->caIds).")";
		}

		if(count($this->caExcludedIds)){
			$msWhere .= " AND p.fkContext NOT IN (".implode(',',$this->caExcludedIds).")";
		}

		if($this->scope){
			$join .= " join view_cm_scope_place_active scopePlace on scopePlace.fkplace = p.fkplace and scopePlace.fkscope = {$this->scope} ";
		}

		$subArea = FWDLanguage::getPHPStringValue('subAreaOf',"Sub-Unidade de");
		$this->csSQL = "SELECT
						p.fkContext as area_id,  
						p.sName ||coalesce(('({$subArea} '||parent.sname||')'),'') as area_name,
						p.tDescription as area_description 
					FROM  
						view_rm_area_active p
						left join view_rm_area_active parent on p.fkparent = parent.fkcontext
						{$join}
					WHERE 1=1 "
						.$msWhere;
	}
}
?>