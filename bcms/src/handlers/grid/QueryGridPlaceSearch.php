<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information SecurityQueryGridAreaSearch.php
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGridPlaceSearch.
 *
 * <p>Consulta para popular o grid de pesquisa de processos.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryGridPlaceSearch extends FWDDBQueryHandler implements QueryAutoCompleter{

	private $csNameFilter = "";

	private $caIds = array();

	private $caExcludedIds = array();

	private $cbEmpty = false;

	public function __construct($poDB){
		parent::__construct($poDB);

		$this->coDataSet->addFWDDBField(new FWDDBField('','place_id',     		     DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField('','place_name',     		 DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField('','place_description',       DB_STRING));
	}

	public function getIdValue(){
		return $this->getFieldValue("place_id");
	}
	
	public function getNameValue(){
		return $this->getFieldValue("place_name");
	}

	public function setNameFilter($psNameFilter) {
		$this->csNameFilter = $psNameFilter;
	}

	public function setIds($paIds){
		$this->caIds = $paIds;
	}

	public function setExcludedIds($paExcludedIds){
		$this->caExcludedIds = $paExcludedIds;
	}

	public function setEmpty($pbEmpty){
		$this->cbEmpty = $pbEmpty;
	}

	public function makeQuery(){
		$msWhere = '';

		if($this->cbEmpty){
			$msWhere.= "AND p.fkContext = 0";
		}

		if($this->csNameFilter){
			$msWhere.= " AND upper(p.sName) like upper('%{$this->csNameFilter}%')";
		}

		if(count($this->caIds)){
			$msWhere .= " AND p.fkContext IN (".implode(',',$this->caIds).")";
		}

		if(count($this->caExcludedIds)){
			$msWhere .= " AND p.fkContext NOT IN (".implode(',',$this->caExcludedIds).")";
		}
		$subLocal = FWDLanguage::getPHPStringValue('subLocalOf',"Sub de");
		$this->csSQL = "SELECT
						p.fkContext as place_id,  
						p.sName ||coalesce(('({$subLocal} '||parent.sname||')'),'') as place_name,
						p.sDescription as place_description 
					FROM  
						view_cm_place_active p
						left join view_cm_place_active parent on p.fkparent = parent.fkcontext
					WHERE 1=1 "
		.$msWhere;
	}
}
?>