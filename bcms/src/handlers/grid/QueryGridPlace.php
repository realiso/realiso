<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGridPlace.
 *
 * <p>Consulta para popular a grid de locais.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryGridPlace extends FWDDBQueryHandler {

	protected $csName = '';

	protected $csNameDescription = '';

	protected $ciUserId = 0;

	protected $parent = 0;

	protected $ciMaxPlaces = 0;

	protected $ciMinPlaces = 0;

	protected $caValueFilter = array();

	protected $ciType = 0;

	protected $scopeId = 0;

	protected $ciPriority = 0;

	protected $ciState = 0;

	protected $ciProcess = 0;

	protected $ciArea = 0;

	protected $ciAsset = 0;

	public function __construct($poDB=null){
		parent::__construct($poDB);
		$this->coDataSet->addFWDDBField(new FWDDBField('p.fkContext'    ,'place_id'              ,DB_NUMBER  ));
		$this->coDataSet->addFWDDBField(new FWDDBField('p.sName'        ,'place_name'            ,DB_STRING  ));
		$this->coDataSet->addFWDDBField(new FWDDBField('place_responsible'        ,'place_responsible'     ,DB_STRING  ));
		$this->coDataSet->addFWDDBField(new FWDDBField('a.sName'        ,'place_type'            ,DB_STRING  ));
		$this->coDataSet->addFWDDBField(new FWDDBField(''               ,'place_plan_count'    ,DB_NUMBER  ));
	}

	public function setName($psName){
		$this->csName = $psName;
	}

	public function setNameDescription($psNameDescription){
		$this->csNameDescription = $psNameDescription;
	}

	public function setUserId($piUserId){
		$this->ciUserId = $piUserId;
	}

	public function setScope($scopeId){
		$this->scopeId = $scopeId;
	}

	public function setParent($p){
		$this->parent = $p;
	}

	public function setMaxPlaces($piMaxPlaces){
		$this->ciMaxPlaces = $piMaxPlaces;
	}

	public function setMinPlaces($piMinPlaces){
		$this->ciMinPlaces = $piMinPlaces;
	}

	public function setValueFilter($paValueFilter){
		$this->caValueFilter = $paValueFilter;
	}

	public function setType($piType){
		$this->ciType = $piType;
	}

	public function makeQuery(){
		$maFilters = array();
		$maHavingFilters = array();


		if($this->csName){
			$maFilters[] = FWDWebLib::getCaseInsensitiveLike('p.sName',$this->csName);
		}

		if($this->csNameDescription){
			$maFilters[] = '('
			.FWDWebLib::getCaseInsensitiveLike('p.sName',$this->csNameDescription)
			.' OR '
			.FWDWebLib::getCaseInsensitiveLike('p.tDescription',$this->csNameDescription)
			.')';
		}

		if($this->ciMaxPlaces){
			$maHavingFilters[] = "COUNT(pa.fkcontext) <= {$this->ciMaxPlaces}";
		}

		if($this->ciMinPlaces){
			$maHavingFilters[] = "COUNT(pa.fkcontext) >= {$this->ciMinAssets}";
		}

		if($this->ciType){
			$maFilters[] = "p.fkType = {$this->ciType}";
		}

		if($this->parent){
			$maFilters[] = "p.fkparent = {$this->parent}";
		}else if(!$this->scopeId){
			$maFilters[] = "p.fkparent is null";
		}

		if(count($maFilters)==0){
			$msWhere = '';
		}else{
			$msWhere = ' AND '.implode(' AND ',$maFilters);
		}

		$join = '';
		$parentConcat = '';
		if($this->scopeId){
			$join .= "JOIN cm_scope_place sp on sp.fkplace = p.fkcontext";
			$msWhere .= " AND sp.fkscope = {$this->scopeId} ";
			$subLocal = FWDLanguage::getPHPStringValue('subLocalOf',"Sub-local de");
			$parentConcat .= "||coalesce(('({$subLocal} '||parent.sname||')'),'')";
		}

		if(count($maHavingFilters)==0){
			$msHaving = '';
		}else{
			$msHaving = ' HAVING '.implode(' AND ',$maHavingFilters);
		}

		$this->csSQL = "SELECT
                      p.fkContext AS place_id,
                      p.sName{$parentConcat} AS place_name,
                      p.type AS place_type,
                      coalesce(g.name, r.name) as place_responsible,
                      count(distinct plan.fkcontext) as place_plan_count
                    FROM
                      view_cm_place_active p
                     
                      LEFT JOIN view_cm_plan_active plan on plan.fkplace = p.fkcontext
                      LEFT JOIN view_cm_group_active g on g.fkcontext = p.fkgroup
                      LEFT JOIN view_cm_resource_active r on r.fkcontext = p.fkresource
                      LEFT JOIN view_cm_place_active parent on parent.fkcontext = p.fkparent 
                      {$join}
                      WHERE 1=1 
                      {$msWhere}
                    GROUP BY
                      p.fkContext,
                      r.name,
                      g.name,
                      p.type,
                      p.sName,
                      parent.sname
                      {$msHaving}";
	}
//a.sName,
// a.sName AS place_type,
// LEFT JOIN cm_place_category a ON (a.fkcontext = p.fktype) 
}

?>