<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGridAsset.
 *
 * <p>Consulta para popular a grid de ativos.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryGridAsset extends FWDDBQueryHandler {

	protected $ciUserId = 0;

	protected $ciProcess = 0;

	protected $csName = '';

	protected $csNameDescription = '';

	protected $ciGroup = 0;

	protected $activity = 0;

	protected $place = 0;

	protected $ciCategory = 0;

	protected $ciMaxProcesses = 0;

	protected $ciMinProcesses = 0;

	protected $caValueFilter = array();

	protected $ciState = 0;

	protected $ciAsset = 0;

	protected $scopeId = 0;

	protected $cbDependencies = false;

	protected $cbDependents = false;

	public function __construct($poDB=null){
		parent::__construct($poDB);
		$this->coDataSet->addFWDDBField(new FWDDBField('a.fkContext'    ,'asset_id'                  ,DB_NUMBER  ));
		$this->coDataSet->addFWDDBField(new FWDDBField('a.sName'        ,'asset_name'                ,DB_STRING  ));
		$this->coDataSet->addFWDDBField(new FWDDBField('category'      	,'category'            		 ,DB_STRING  ));
		$this->coDataSet->addFWDDBField(new FWDDBField('description'      	,'description'            		 ,DB_STRING  ));
		$this->coDataSet->addFWDDBField(new FWDDBField('responsible'    ,'responsible'             	 ,DB_STRING  ));
		$this->coDataSet->addFWDDBField(new FWDDBField('a.tra'    ,'tra'             	 ,DB_NUMBER  ));
		$this->coDataSet->addFWDDBField(new FWDDBField(''    ,'ranking'             	 ,DB_NUMBER  ));
	}

	public function setUserId($piUserId){
		$this->ciUserId = $piUserId;
	}

	public function setProcess($piProcess){
		$this->ciProcess = $piProcess;
	}

	public function setScope($scope){
		$this->scopeId = $scope;
	}

	public function setName($psName){
		$this->csName = $psName;
	}

	public function setNameDescription($psNameDescription){
		$this->csNameDescription = $psNameDescription;
	}

	public function setGroup($piGroup){
		$this->ciGroup = $piGroup;
	}

	public function setActivity($activity){
		$this->activity= $activity;
	}

	public function setPlace($place){
		$this->place= $place;
	}

	public function setCategory($piCategory){
		$this->ciCategory = $piCategory;
	}

	public function setMaxProcesses($piMaxProcesses){
		$this->ciMaxProcesses = $piMaxProcesses;
	}

	public function setMinProcesses($piMinProcesses){
		$this->ciMinProcesses = $piMinProcesses;
	}

	public function setValueFilter($paValueFilter){
		$this->caValueFilter = $paValueFilter;
	}

	public function setState($piState){
		$this->ciState = $piState;
	}

	public function setAsset($piAsset){
		$this->ciAsset = $piAsset;
	}

	public function showDependents( $pbDependents ){
		$this->cbDependents = $pbDependents;
	}

	public function showDependencies( $pbDependencies ){
		$this->cbDependencies = $pbDependencies;
	}

	public function makeQuery(){
		$maFilters = array();
		$maHavingFilters = array();

		$join = "";

		if($this->ciProcess){
			$join .= " JOIN view_rm_process_asset_active pa ON (pa.fkAsset = a.fkContext) ";
			$maFilters[] = "pa.fkProcess = {$this->ciProcess}";

			if($this->ciMaxProcesses){
				$maHavingFilters[] = "COUNT(pa.fkProcess) <= {$this->ciMaxProcesses}";
			}

			if($this->ciMinProcesses){
				$maHavingFilters[] = "COUNT(pa.fkProcess) >= {$this->ciMinProcesses}";
			}
		}

		if($this->activity){
			$join .= " JOIN view_cm_process_activity_asset_active aa ON (aa.fkAsset = a.fkContext) ";
			$maFilters[] = "aa.fkactivity = {$this->activity}";
		}

		if($this->place){
			$maFilters[] = "a.fkplace = {$this->place}";
		}

		if($this->csName){
			$maFilters[] = FWDWebLib::getCaseInsensitiveLike('a.sName',$this->csName);
		}

		if($this->csNameDescription){
			$maFilters[] = '('
			.FWDWebLib::getCaseInsensitiveLike('a.sName',$this->csNameDescription)
			.' OR '
			.FWDWebLib::getCaseInsensitiveLike('a.tDescription',$this->csNameDescription)
			.')';
		}

		if($this->ciGroup){
			$maFilters[] = "a.fkGroup = {$this->ciGroup}";
		}

		if($this->ciCategory){
			$maFilters[] = "a.fkCategory = {$this->ciCategory}";
		}

		if($this->ciAsset && $this->cbDependents){
			$maFilters[] = " a.fkContext IN (SELECT pkAsset FROM " . FWDWebLib::getFunctionCall("get_asset_dependents({$this->ciAsset})") . " )";
		}

		if($this->ciAsset && $this->cbDependencies){
			$maFilters[] = "a.fkContext IN (SELECT pkAsset FROM " . FWDWebLib::getFunctionCall("get_asset_dependencies({$this->ciAsset})") ." )";
		}

		$miCountValueFilter = count($this->caValueFilter);
		if($miCountValueFilter > 0 && $miCountValueFilter < 4){
			$miRiskLow = ISMSLib::getConfigById(RISK_LOW);
			$miRiskHigh = ISMSLib::getConfigById(RISK_HIGH);
			$maValueFilters = array();
			foreach($this->caValueFilter as $msValueFilter){
				switch($msValueFilter){
					case 'not_estimated': $maValueFilters[] = "a.nValue = 0";                                         break;
					case 'low':           $maValueFilters[] = "a.nValue <= {$miRiskLow} AND a.nValue != 0";           break;
					case 'medium':        $maValueFilters[] = "a.nValue > {$miRiskLow} AND a.nValue < {$miRiskHigh}"; break;
					case 'high':          $maValueFilters[] = "a.nValue >= {$miRiskHigh}";                            break;
				}
			}
			$maFilters[] = '('.implode(' OR ',$maValueFilters).')';
		}

		if($this->scopeId){
			$join .= " JOIN cm_scope_asset sa on sa.fkasset = a.fkcontext ";
			$maFilters[] = " sa.fkscope = {$this->scopeId} ";
		}

		if($this->ciState){
			$maFilters[] = "c.nState = {$this->ciState}";
		}

		if(count($maFilters)==0){
			$msWhere = '';
		}else{
			$msWhere = ' WHERE '.implode(' AND ',$maFilters);
		}
		if(count($maHavingFilters)==0){
			$msHaving = '';
		}else{
			$msHaving = ' HAVING '.implode(' AND ',$maHavingFilters);
		}

		//$this->getDataset()->setOrderBy("ranking","+");
		//$this->getDataset()->setOrderBy("tra","+");  
		
		$this->csSQL = "SELECT DISTINCT
                      a.fkContext AS asset_id,
                      a.sName AS asset_name,
                      cat.sName AS category,
                      g.name as responsible,
                      a.tDescription as description,
                      a.tra as tra,
                      coalesce(ranking.ranking, 0.00) as ranking
                    FROM
                      view_rm_asset_active a
                      LEFT JOIN rm_category cat ON (cat.fkContext = a.fkCategory)
                      LEFT JOIN view_cm_group_active g on g.fkcontext = a.fkgroup
                      LEFT JOIN view_rm_process_asset_active paa on (a.fkcontext = paa.fkasset)
                      left join view_process_ranking ranking on (ranking.fkprocess = paa.fkprocess)
                      
                      {$join}
                      {$msWhere}
                      {$msHaving}";
	}

}

?>