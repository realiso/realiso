<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGridScope.
 *
 * <p>Consulta para popular a grid de escopos.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryGridScope extends FWDDBQueryHandler {

  protected $csName = '';

  protected $csNameDescription = '';

  protected $ciResponsible = 0;

  protected $ciUserId = 0;

  protected $ciMaxScopes = 0;

  protected $ciMinScopes = 0;

  protected $caValueFilter = array();

  
  public function __construct($poDB=null){
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField('p.fkContext'    ,'scope_id'              ,DB_NUMBER  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('p.sName'        ,'scope_name'            ,DB_STRING  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('description'        ,'description'          ,DB_STRING  ));
  }

  public function setName($psName){
    $this->csName = $psName;
  }

  public function setMaxScopes($piMaxScopes){
    $this->ciMaxScopes = $piMaxScopes;
  }

  public function setMinScopes($piMinScopes){
    $this->ciMinScopes = $piMinScopes;
  }

  public function setValueFilter($paValueFilter){
    $this->caValueFilter = $paValueFilter;
  }

  
  public function makeQuery(){
    $maFilters = array();
    $maHavingFilters = array();
    
    
    if($this->csName){
      $maFilters[] = FWDWebLib::getCaseInsensitiveLike('p.sName',$this->csName);
    }
    
    if($this->csNameDescription){
      $maFilters[] = '('
                        .FWDWebLib::getCaseInsensitiveLike('p.sName',$this->csNameDescription)
                    .')';
    }
    

    if($this->ciMaxScopes){
      $maHavingFilters[] = "COUNT(p.fkcontext) <= {$this->ciMaxScopes}";
    }
    
    if($this->ciMinScopes){
      $maHavingFilters[] = "COUNT(p.fkcontext) >= {$this->ciMinScopes}";
    }
                
    if(count($maFilters)==0){
      $msWhere = '';
    }else{
      $msWhere = ' WHERE '.implode(' AND ',$maFilters);
    }
    if(count($maHavingFilters)==0){
      $msHaving = '';
    }else{
      $msHaving = ' HAVING '.implode(' AND ',$maHavingFilters);
    }
    $this->csSQL = "SELECT
                      p.fkContext AS scope_id,
                      p.sName AS scope_name,
                      p.sdescription AS description
                    FROM
                      view_cm_scope_active p
                    {$msWhere}
                    GROUP BY
                      p.fkContext,
                      p.sName,
                      p.sdescription 
                    {$msHaving}";
  }

}

?>