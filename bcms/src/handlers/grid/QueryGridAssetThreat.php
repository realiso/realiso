<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

class QueryGridAssetThreat extends FWDDBQueryHandler {

  protected $csName = '';

  protected $csNameDescription = '';

  protected $ciResponsible = 0;

  protected $ciUserId = 0;

  protected $ciMaxThreats = 0;

  protected $ciMinThreats = 0;

  protected $caValueFilter = array();

  protected $ciType = 0;

  protected $ciPriority = 0;

  protected $ciState = 0;

  protected $ciAsset = 0;

  
  public function __construct($poDB=null){
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField('s.fkContext'    ,'threat_id'              ,DB_NUMBER  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('t.sName'        ,'threat_name'            ,DB_STRING  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('t.ntype'        ,'threat_type'	     	  ,DB_NUMBER  ));
    $this->coDataSet->addFWDDBField(new FWDDBField(''               ,'threat_probability'     ,DB_NUMBER  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('t.origin'               ,'origin'     ,DB_STRING  ));
  }

  public function setName($psName){
    $this->csName = $psName;
  }



  public function setMaxThreats($piMaxThreats){
    $this->ciMaxThreats = $piMaxThreats;
  }

  public function setMinThreats($piMinThreats){
    $this->ciMinThreats = $piMinThreats;
  }

  public function setValueFilter($paValueFilter){
    $this->caValueFilter = $paValueFilter;
  }
  
  public function setAsset($paAssetId) {
  	$this->ciAsset = $paAssetId;
  }

  
  public function makeQuery(){
    $maFilters = array();
    $maHavingFilters = array();
    
    
    if($this->csName){
      $maFilters[] = FWDWebLib::getCaseInsensitiveLike('t.sName',$this->csName);
    }
    
    if($this->ciMaxThreats){
      $maHavingFilters[] = "COUNT(t.fkcontext) <= {$this->ciMaxThreats}";
    }
    
    if($this->ciMinThreats){
      $maHavingFilters[] = "COUNT(t.fkcontext) >= {$this->ciMinThreats}";
    }
            
    if(count($maFilters)==0){
      $msWhere = '';
    }else{
      $msWhere = ' AND '.implode(' AND ',$maFilters);
    }
    if(count($maHavingFilters)==0){
      $msHaving = '';
    }else{
      $msHaving = ' HAVING '.implode(' AND ',$maHavingFilters);
    }
    // aqui precisa descobrir o algoritmo do ranking e fazer
    $this->csSQL = "SELECT
                      t.fkContext AS threat_id,
                      t.sName AS threat_name,
                      icc.sname as threat_type,
                      t.nprobability as threat_probability,
                        t.origin as origin
                    FROM
                      view_asset_threats t  
                      JOIN isms_context_classification icc ON (icc.pkclassification = t.ntype)
            
                    WHERE t.fkasset = {$this->ciAsset}                         
                    {$msWhere}
                    GROUP BY
                    t.origin,
                      t.fkContext,
                      t.sName,
                      icc.sname,
                      t.nprobability  
                    {$msHaving}";
  }
}
?>