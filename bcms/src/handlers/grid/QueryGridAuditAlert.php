<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGridAuditAlert.
 *
 * <p>Consulta para popular o grid de auditoria de alertas.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryGridAuditAlert extends FWDDBQueryHandler {

	protected $ciCreatorId;
	protected $ciReceiverId;
	protected $ciDateStart;
	protected $ciDateEnd;
	
  public function __construct($poDB){
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField("a.dDate",         "alert_date",         DB_DATETIME));
    $this->coDataSet->addFWDDBField(new FWDDBField("uc.sName",        "alert_creator",      DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("ur.sName",        "alert_receiver",     DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("cn.context_name", "alert_context_name", DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("c.nType",         "alert_context_type", DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("a.tJustification","alert_justification",DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("a.nType",       	"alert_type",      	 	DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("a.fkContext",     "alert_context_id",   DB_NUMBER));
  }

	public function setCreator($piCreatorId) {
		$this->ciCreatorId = $piCreatorId;
	}
	
	public function setReceiver($piReceiverId) {
		$this->ciReceiverId = $piReceiverId;
	}
	
	public function setDateStart($piDateStart) {
		$this->ciDateStart = $piDateStart;
	}
	
	public function setDateEnd($piDateEnd) {
		$this->ciDateEnd = $piDateEnd;
	}
	
  public function makeQuery(){
  	$msWhere = " WHERE 1=1";
    if($this->ciCreatorId){
      $msWhere.= " AND a.fkCreator = ".$this->ciCreatorId;
    }
    if($this->ciReceiverId){
      $msWhere.= " AND a.fkReceiver = ".$this->ciReceiverId;
    }
    if($this->ciDateStart){
      $msWhere.= " AND a.dDate >= ".ISMSLib::getTimestampFormat($this->ciDateStart);
    }
    if($this->ciDateEnd){
      $msWhere.= " AND a.dDate <= ".ISMSLib::getTimestampFormat($this->ciDateEnd);
    }
    
    $this->csSQL = "SELECT
                      a.dDate as alert_date,
                      uc.sName as alert_creator,
                      ur.sName as alert_receiver,
                      cn.context_name as alert_context_name,
                      c.nType as alert_context_type,
                      a.tJustification as alert_justification,
                      a.nType as alert_type,
                      a.fkContext as alert_context_id
                    FROM
                      wkf_alert a
                      JOIN isms_context c ON (a.fkContext = c.pkContext)
                      JOIN context_names cn ON (a.fkContext = cn.context_id)
                      JOIN isms_user uc ON (a.fkCreator = uc.fkContext)
                      JOIN isms_user ur ON (a.fkReceiver = ur.fkContext)"
                      .$msWhere;
  }

}

?>