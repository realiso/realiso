<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGridPlace.
 *
 * <p>Consulta para popular a grid de locais.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryGridProcessActivity extends FWDDBQueryHandler {

	protected $csName = '';

	protected $csNameDescription = '';

	protected $ciMaxActivities = 0;

	protected $ciMinActivities = 0;

	protected $caValueFilter = array();

	protected $ciProcess = 0;

	public function __construct($poDB=null){
		parent::__construct($poDB);
		$this->coDataSet->addFWDDBField(new FWDDBField('a.fkContext'    ,'activity_id'              ,DB_NUMBER  ));
		$this->coDataSet->addFWDDBField(new FWDDBField('a.sName'        ,'activity_name'            ,DB_STRING  ));
		$this->coDataSet->addFWDDBField(new FWDDBField(''		        ,'activity_asset_count'     ,DB_NUMBER  ));
	}

	public function setName($psName){
		$this->csName = $psName;
	}

	public function setNameDescription($psNameDescription){
		$this->csNameDescription = $psNameDescription;
	}

	public function setMaxPlaces($piMaxPlaces){
		$this->ciMaxPlaces = $piMaxPlaces;
	}

	public function setMinPlaces($piMinPlaces){
		$this->ciMinPlaces = $piMinPlaces;
	}

	public function setValueFilter($paValueFilter){
		$this->caValueFilter = $paValueFilter;
	}

	public function setProcess($piProcessId) {
		$this->ciProcess = $piProcessId;
	}


	public function makeQuery(){
		$maFilters = array();
		$maHavingFilters = array();


		if($this->csName){
			$maFilters[] = FWDWebLib::getCaseInsensitiveLike('a.sName',$this->csName);
		}

		if($this->csNameDescription){
			$maFilters[] = '('
			.FWDWebLib::getCaseInsensitiveLike('a.sName',$this->csNameDescription)
			.' OR '
			.FWDWebLib::getCaseInsensitiveLike('a.tDescription',$this->csNameDescription)
			.')';
		}

		if($this->ciMaxActivities){
			$maHavingFilters[] = "COUNT(a.fkcontext) <= {$this->ciMaxPlaces}";
		}

		if($this->ciMinActivities){
			$maHavingFilters[] = "COUNT(a.fkcontext) >= {$this->ciMinAssets}";
		}


		if(count($maFilters)==0){
			$msWhere = '';
		}else{
			$msWhere = ' AND '.implode(' AND ',$maFilters);
		}
		if(count($maHavingFilters)==0){
			$msHaving = '';
		}else{
			$msHaving = ' HAVING '.implode(' AND ',$maHavingFilters);
		}
		$this->csSQL = "SELECT
                      a.fkContext AS activity_id,
                      a.sName AS activity_name,
                      ( SELECT COUNT (fkcontext) as count_assets FROM view_cm_process_activity_asset_active pu WHERE pu.fkactivity = a.fkContext) as activity_asset_count
                    FROM
                      isms_context c
                      JOIN cm_process_activity a ON (a.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED.")
                      WHERE a.fkprocess = {$this->ciProcess}                        
                      {$msWhere}
                    GROUP BY
                      a.fkContext,
                      a.sName
                      {$msHaving}";
	}

}

?>