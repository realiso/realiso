<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGridProcessSearch.
 *
 * <p>Consulta para popular o grid de pesquisa de escopos.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryGridScopeViewAreas extends FWDDBQueryHandler {

	private $scopeId = 0;

	public function __construct($poDB){
		parent::__construct($poDB);

		$this->coDataSet->addFWDDBField(new FWDDBField('','areaId',         	DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField('','areaName',       	DB_STRING));
	}

	public function setScope($scopeId) {
		$this->scopeId = $scopeId;
	}

	public function makeQuery(){
		$this->csSQL = "SELECT
							area.sname as areaName,
							area.fkcontext as areaId
						FROM  
							view_rm_area_active area 
							JOIN view_cm_scope_area_active sp on sp.fkarea = area.fkcontext
						WHERE
							sp.fkscope = {$this->scopeId}";
	}

}
?>