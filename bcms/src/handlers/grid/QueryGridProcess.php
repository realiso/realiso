<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGridProcess.
 *
 * <p>Consulta para popular a grid de processos.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryGridProcess extends FWDDBQueryHandler {

	protected $csName = '';

	protected $csNameDescription = '';

	protected $ciProcess = 0;

	protected $ciArea = 0;

	protected $scope = 0;

	protected $ciAsset = 0;

	public function __construct($poDB=null){
		parent::__construct($poDB);
		$this->coDataSet->addFWDDBField(new FWDDBField('p.fkContext'    ,'process_id'            ,DB_NUMBER  ));
		$this->coDataSet->addFWDDBField(new FWDDBField('p.sName'        ,'process_name'          ,DB_STRING  ));
		$this->coDataSet->addFWDDBField(new FWDDBField('p.nValue'       ,'process_value'         ,DB_NUMBER  ));
		$this->coDataSet->addFWDDBField(new FWDDBField(''               ,'user_create_name'      ,DB_STRING  ));
		$this->coDataSet->addFWDDBField(new FWDDBField(''               ,'date_create'           ,DB_DATETIME));
		$this->coDataSet->addFWDDBField(new FWDDBField(''               ,'user_edit_name'        ,DB_STRING  ));
		$this->coDataSet->addFWDDBField(new FWDDBField(''               ,'date_edit'             ,DB_DATETIME));
		$this->coDataSet->addFWDDBField(new FWDDBField(''               ,'process_ranking'    	 ,DB_NUMBER  ));
		$this->coDataSet->addFWDDBField(new FWDDBField(''               ,'financialLoss'		 ,DB_NUMBER  ));
		$this->coDataSet->addFWDDBField(new FWDDBField(''               ,'rto'					 ,DB_NUMBER  ));
		$this->coDataSet->addFWDDBField(new FWDDBField(''               ,'process_activity_count',DB_NUMBER  ));
		$this->coDataSet->addFWDDBField(new FWDDBField(''               ,'process_scenario_count',DB_NUMBER  ));
		$this->coDataSet->addFWDDBField(new FWDDBField(''               ,'process_asset_count'   ,DB_NUMBER  ));
		$this->coDataSet->addFWDDBField(new FWDDBField(''               ,'rto_flag'   ,DB_NUMBER  ));
		$this->coDataSet->addFWDDBField(new FWDDBField(''               ,'rto_order'   ,DB_NUMBER  ));
		
	}

	public function setName($psName){
		$this->csName = $psName;
	}

	public function setNameDescription($psNameDescription){
		$this->csNameDescription = $psNameDescription;
	}

	public function setProcess($piProcess){
		$this->ciProcess = $piProcess;
	}

	public function setArea($piArea){
		$this->ciArea = $piArea;
	}

	public function setScope($scope){
		$this->scope = $scope;
	}

	public function setAsset($piAsset){
		$this->ciAsset = $piAsset;
	}

	public function makeQuery(){
		$maFilters = array();
		$maHavingFilters = array();


		if($this->csName){
			$maFilters[] = FWDWebLib::getCaseInsensitiveLike('p.sName',$this->csName);
		}

		if($this->csNameDescription){
			$maFilters[] = '('
			.FWDWebLib::getCaseInsensitiveLike('p.sName',$this->csNameDescription)
			.' OR '
			.FWDWebLib::getCaseInsensitiveLike('p.tDescription',$this->csNameDescription)
			.')';
		}

		if($this->ciProcess){
			$maFilters[] = "p.fkContext = {$this->ciProcess}";
		}

		if($this->ciArea){
			$maFilters[] = "p.fkArea = {$this->ciArea}";
		}

		if($this->scope){
			$maFilters[] = "sp.fkScope = {$this->scope}";
		}

		if($this->ciAsset){
			$msAssetJoin = "JOIN view_rm_process_asset_active pa ON (pa.fkProcess = p.fkContext AND pa.fkAsset = ". $this->ciAsset .")";
		}else{
			$msAssetJoin = 'LEFT JOIN view_rm_process_asset_active pa ON (pa.fkProcess = p.fkContext)';
		}

		if($this->scope){
			$msAssetJoin = $msAssetJoin." JOIN cm_scope_process sp ON (sp.fkProcess = p.fkContext AND sp.fkScope = ". $this->scope .")";
		}

		if(count($maFilters)==0){
			$msWhere = '';
		}else{
			$msWhere = ' WHERE '.implode(' AND ',$maFilters);
		}
		if(count($maHavingFilters)==0){
			$msHaving = '';
		}else{
			$msHaving = ' HAVING '.implode(' AND ',$maHavingFilters);
		}
		
		//$this->getDataset()->setOrderBy("process_ranking","-");
		//$this->getDataset()->setOrderBy("rto","+");

		//$minutes = FWDLanguage::getPHPStringValue('rtoMinutes','minutos');
		//$hours = FWDLanguage::getPHPStringValue('rtoHours','horas');
		//$days = FWDLanguage::getPHPStringValue('rtoDays','dias');
		
		$this->csSQL = "SELECT
                      p.fkContext AS process_id,
                      p.sName AS process_name,
                      p.nValue AS process_value,
                      ch.context_creator_name AS user_create_name,
                      ch.context_date_created AS date_create,
                      ch.context_modifier_name AS user_edit_name,
                      ch.context_date_modified AS date_edit,
                      ( SELECT COUNT (fkcontext) FROM view_cm_process_activity_active pa WHERE pa.fkProcess = p.fkContext) as process_activity_count,
                      ( SELECT COUNT (fkcontext) FROM view_cm_scene_active scn WHERE scn.fkProcess = p.fkContext) as process_scenario_count,
                      ( SELECT COUNT (fkcontext) FROM view_rm_process_asset_active asset WHERE asset.fkProcess = p.fkContext) as process_asset_count,
                      ranking.rto as rto,
                      coalesce(ranking.financialLoss,0) as financialLoss,
                      trunc(coalesce(ranking.ranking,0),4) as process_ranking,
                      ranking.rtoflag as rto_flag,
                      ranking.rto*(case when ranking.rtoflag = 1 then 60 else case when ranking.rtoflag = 2 then (60*60) else case when ranking.rtoflag = 3  then (60*60*24) else 1 end end end) as rto_order
                    FROM
                      view_rm_process_active p
                      JOIN context_history ch ON (ch.context_id = p.fkContext)
                      left join view_process_ranking ranking on ranking.fkprocess = p.fkcontext
                      $msAssetJoin
                      {$msWhere}
                    GROUP BY
                      p.fkContext,
                      p.sName,
                      p.nValue,
                      ch.context_creator_name,
                      ch.context_date_created,
                      ch.context_modifier_name,
                      ch.context_date_modified,
                      ranking.ranking,
                      ranking.financialLoss,
                      ranking.rto,
                      ranking.rtoflag
                      {$msHaving}
                      ";
                      //echo $this->csSQL;
                      
	}
	
	//ranking.rto|| ( case when ranking.rtoflag = 1 then ' {$minutes}' else case when ranking.rtoflag = 2 then ' {$hours}' else ' {$days}' end end) as rto,
	
}

?>