<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

class QueryGridPlanType extends FWDDBQueryHandler {

	public function __construct($poDB=null){
		parent::__construct($poDB);
		$this->coDataSet->addFWDDBField(new FWDDBField('fkcontext'   	,'id'              	,DB_NUMBER  ));
		$this->coDataSet->addFWDDBField(new FWDDBField('description'    ,'description'      ,DB_STRING  ));
		$this->coDataSet->addFWDDBField(new FWDDBField('detail'      	,'detail'           ,DB_STRING  ));
	}

	public function makeQuery(){
		$this->csSQL = "SELECT
							t.fkcontext as id,
							t.description as description,
							t.detail as detail
	                    FROM
							view_cm_plan_range_type_active t
	                    WHERE 
	                    	1=1
	                    ORDER BY
	                    	t.fkcontext ASC";
//description
	}
}
?>