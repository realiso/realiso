<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGridScene.
 *
 * <p>Consulta para popular a grid de cen�rios.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryGridScene extends FWDDBQueryHandler {

	protected $csName = '';

	protected $csNameDescription = '';

	protected $ciResponsible = 0;

	protected $ciUserId = 0;

	protected $ciMaxScenes = 0;

	protected $ciMinScenes = 0;

	protected $caValueFilter = array();

	protected $ciType = 0;

	protected $ciPriority = 0;

	protected $ciState = 0;

	protected $ciProcess = 0;


	public function __construct($poDB=null){
		parent::__construct($poDB);
		$this->coDataSet->addFWDDBField(new FWDDBField('s.fkContext'    ,'scene_id'              ,DB_NUMBER  ));
		$this->coDataSet->addFWDDBField(new FWDDBField('s.sName'        ,'scene_name'            ,DB_STRING  ));
		$this->coDataSet->addFWDDBField(new FWDDBField(''               ,'scene_ranking'	     ,DB_NUMBER  ));
		$this->coDataSet->addFWDDBField(new FWDDBField(''               ,'financial_loss'	     ,DB_NUMBER  ));
		$this->coDataSet->addFWDDBField(new FWDDBField(''               ,'rto'	     			 ,DB_NUMBER  ));
		$this->coDataSet->addFWDDBField(new FWDDBField(''               ,'impact_count'    ,DB_NUMBER  ));
	}

	public function setName($psName){
		$this->csName = $psName;
	}

	public function setNameDescription($psNameDescription){
		$this->csNameDescription = $psNameDescription;
	}


	public function setMaxScenes($piMaxScenes){
		$this->ciMaxScenes = $piMaxScenes;
	}

	public function setMinScenes($piMinScenes){
		$this->ciMinScenes = $piMinScenes;
	}

	public function setValueFilter($paValueFilter){
		$this->caValueFilter = $paValueFilter;
	}

	public function setProcess($paProcessId) {
		$this->caProcess = $paProcessId;
	}


	public function makeQuery(){
		$maFilters = array();
		$maHavingFilters = array();


		if($this->csName){
			$maFilters[] = FWDWebLib::getCaseInsensitiveLike('s.sName',$this->csName);
		}

		if($this->ciMaxScenes){
			$maHavingFilters[] = "COUNT(s.fkcontext) <= {$this->ciMaxScenes}";
		}

		if($this->ciMinScenes){
			$maHavingFilters[] = "COUNT(s.fkcontext) >= {$this->ciMinScenes}";
		}

		if(count($maFilters)==0){
			$msWhere = '';
		}else{
			$msWhere = ' AND '.implode(' AND ',$maFilters);
		}
		if(count($maHavingFilters)==0){
			$msHaving = '';
		}else{
			$msHaving = ' HAVING '.implode(' AND ',$maHavingFilters);
		}

		$this->getDataset()->setOrderBy("scene_ranking","-");
		$this->getDataset()->setOrderBy("rto","+");

		$this->csSQL = "SELECT
                      s.fkContext AS scene_id,
                      s.sName AS scene_name,
                      s.nrto as rto,
                      trunc(coalesce(ranking.ranking,0),4) AS scene_ranking,
                     coalesce(ranking.financialLoss,0) AS financial_loss,
                      ( 
                      	SELECT 
                      		COUNT(fkcontext) as count_impacts 
                      	FROM
                      		view_cm_impact_damage_active su
                      	WHERE 
                      		su.fkscene = s.fkContext
                      	) as impact_count
                    FROM
                      view_cm_scene_active s
                      LEFT JOIN view_scene_ranking ranking on s.fkcontext = ranking.fkscene 
                      LEFT JOIN cm_impact_damage si ON si.fkscene = s.fkContext
                      LEFT JOIN view_impact_ranking as impact ON impact.fkcontext = si.fkcontext
                    WHERE 
                    	s.fkprocess = {$this->caProcess}
                    	{$msWhere}
                    GROUP BY
                      s.fkContext,
                      s.sName,
                      ranking.ranking,
                      s.nrto,
                      ranking.financialLoss
                      {$msHaving}";
	}

}

?>