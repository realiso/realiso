<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

class QueryGridPlanTest extends FWDDBQueryHandler {

	public function __construct($poDB=null){
		parent::__construct($poDB);
		$this->coDataSet->addFWDDBField(new FWDDBField(''  				,'id'              	,DB_NUMBER  ));
		$this->coDataSet->addFWDDBField(new FWDDBField(''      			,'plan'            	,DB_STRING  ));
		$this->coDataSet->addFWDDBField(new FWDDBField(''               ,'observation'		,DB_STRING	));
		$this->coDataSet->addFWDDBField(new FWDDBField(''               ,'starttime'		,DB_DATETIME  ));
		$this->coDataSet->addFWDDBField(new FWDDBField(''               ,'endtime'	      	,DB_DATETIME  ));
	}

	private $plan;
	
	public function setPlan($plan){
		$this->plan = $plan;
	}
	
	public function makeQuery(){

		$this->getDataset()->setOrderBy("starttime","-");
		
		$where = "";

		if($this->plan){
			$where .= " and p.fkplan = ".$this->plan;
		}
		$this->csSQL = "select
							p.fkcontext as id,
							p.fkplan as plan,
							p.observation as observation,
							p.starttime as starttime,
							p.endtime as endtime
						from 
							view_cm_plan_test_active p
							where 1=1 {$where}
						";
	}
}
?>