<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

class QueryGridPlanAction extends FWDDBQueryHandler {

	protected $plan = 0;

	public function __construct($poDB=null){
		parent::__construct($poDB);
		$this->coDataSet->addFWDDBField(new FWDDBField('a.fkcontext'  				,'id'              	,DB_NUMBER  ));
		$this->coDataSet->addFWDDBField(new FWDDBField(''  				,'rangeFlag'        ,DB_NUMBER  ));
		$this->coDataSet->addFWDDBField(new FWDDBField('r.range'      			,'range'          	,DB_STRING  ));
		$this->coDataSet->addFWDDBField(new FWDDBField('rt.description'               ,'type'				,DB_STRING	));
		$this->coDataSet->addFWDDBField(new FWDDBField(''               ,'groupAcronym'		,DB_STRING  ));
		$this->coDataSet->addFWDDBField(new FWDDBField('a.resume'               ,'resume'	      	,DB_STRING  ));
		$this->coDataSet->addFWDDBField(new FWDDBField('a.fkplan'               ,'plan'	      		,DB_NUMBER  ));
		$this->coDataSet->addFWDDBField(new FWDDBField('rt.fkcontext'               ,'type_order'	      		,DB_NUMBER  ));
		
	}

	public function setPlan($plan){
		$this->plan = $plan;
	}

	public function makeQuery(){
		//$this->getDataset()->setOrderBy("type_order","+");
		//$this->getDataset()->setOrderBy("rangeFlag","+");
		//$this->getDataset()->setOrderBy("groupAcronym","+");
		//$this->getDataset()->setOrderBy("resume","+");
		$this->csSQL = "SELECT
							a.fkcontext AS id,
							r.range AS range,
							r.startRange*(case when r.startRangeFlag = 2 then 60 else case when r.startRangeFlag = 3 then (60*24) else case when r.startRangeFlag = 4  then (60*24*30) else 1 end end end) as rangeFlag,
							rt.description AS type,
							coalesce(g.acronym,g.name) AS groupAcronym,
							a.resume AS resume,
							a.fkplan as plan,
							rt.fkcontext as type_order
						FROM
							view_cm_plan_action_active a
							JOIN view_cm_plan_range_active r on a.fkplanrange = r.fkcontext
							left JOIN view_cm_group_active g on g.fkcontext = a.fkgroup
							JOIN view_cm_plan_range_type_active rt on a.fkplantype = rt.fkcontext
						WHERE
							a.fkplan = {$this->plan}";
	}
}
?>