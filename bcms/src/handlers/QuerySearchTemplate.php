<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QuerySearchTemplate.
 *
 * <p></p>
 * @package ISMS
 * @subpackage handlers
 */
class QuerySearchTemplate extends FWDDBQueryHandler {

  protected $cbSearchContent;
  protected $csSearch;
  protected $ciCreationDateStart = 0;
  protected $ciEditionDateStart = 0;
  protected $ciCreationDateEnd = 0;
  protected $ciEditionDateEnd = 0;

  public function __construct($poDB) {
    parent::__construct($poDB);

    $this->coDataSet->addFWDDBField(new FWDDBField('' ,'context_type'   ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('' ,'context_id'     ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('' ,'context_string' ,DB_STRING));
  }

  public function setSearch($psSearch){
    $this->csSearch = $psSearch;
  }

  public function setSearchContent($pbValue){
    $this->cbSearchContent = $pbValue;
  }

  public function setCreationDateStart($piDate) {
    $this->ciCreationDateStart = $piDate;
  }

  public function setEditionDateStart($piDate) {
    $this->ciEditionDateStart = $piDate;
  }

  public function setCreationDateEnd($piDate) {
    $this->ciCreationDateEnd = $piDate;
  }

  public function setEditionDateEnd($piDate) {
    $this->ciEditionDateEnd = $piDate;
  }

  public function makeQuery(){
    $msSearch = $this->csSearch;
    $moTemplate = new PMTemplate($this->cbSearchContent);
    $msTable = $moTemplate->getTable();
    $miType = $moTemplate->getContextType();
    $maCase = array();
    $maWhere = array();
    $msDBType = FWDWebLib::getConnection()->getDatabaseType();

    $maSearchableFields = $moTemplate->getSearchableFields();
    foreach($maSearchableFields as $msAlias){
      $msField = $moTemplate->getFieldName($msAlias);
      $maCase[] = "WHEN ".FWDWebLib::getCaseInsensitiveLike("t.".$msField,$msSearch)." THEN " . (($msDBType == DB_ORACLE) ? "TO_CLOB(t.$msField)" : "t.$msField");
      $maWhere[] = FWDWebLib::getCaseInsensitiveLike("t.".$msField,$msSearch);
    }

    $maDateFilters = array();
    $maDateFilters[] = "t.fkContext = ch.context_id";
    if($this->ciCreationDateStart){
      $maDateFilters[] = "ch.context_date_created >= " . ISMSLib::getTimestampFormat($this->ciCreationDateStart);
    }
    if($this->ciCreationDateEnd){
      $maDateFilters[] = "ch.context_date_created <= " . ISMSLib::getTimestampFormat($this->ciCreationDateEnd);
    }
    if($this->ciEditionDateStart){
      $maDateFilters[] = "ch.context_date_modified >= " . ISMSLib::getTimestampFormat($this->ciEditionDateStart);
    }
    if($this->ciEditionDateEnd){
      $maDateFilters[] = "ch.context_date_modified <= " . ISMSLib::getTimestampFormat($this->ciEditionDateEnd);
    }
    $msDateFilter = '';
    if(count($maDateFilters) > 1){
      $msDateFilter = " JOIN context_history ch ON (".implode(' AND ',$maDateFilters).")";
    }

    if(count($maWhere))
      $msWhere = " WHERE ";
    $this->csSQL = "
          SELECT $miType AS context_type,
                 t.fkContext AS context_id,
                 CASE ".implode("\n",$maCase)." END AS context_string
            FROM $msTable t
            JOIN isms_context c ON (t.fkContext=c.pkContext)
            $msDateFilter
            $msWhere
            (".implode(' OR ',$maWhere).")";
  }
}

?>