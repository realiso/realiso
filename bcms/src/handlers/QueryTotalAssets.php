<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryTotalAssets.
 *
 * <p>Consulta para buscar o n�mero de ativos cadastrados no sistema.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryTotalAssets extends FWDDBQueryHandler {

  private $ciTotalAssets = 0;

  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField("total_assets","total_assets", DB_NUMBER));
 }

  public function makeQuery() {
    $this->csSQL = "SELECT count(*) as total_assets FROM rm_asset";
  }

  public function executeQuery() {
    parent::executeQuery();
    $this->ciTotalAssets = 0;
    if ($this->coDataSet->fetch())
      $this->ciTotalAssets = $this->coDataSet->getFieldByAlias("total_assets")->getValue();
  }

  public function getTotalAssets() {
    return $this->ciTotalAssets;
  }

}
?>