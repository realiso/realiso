<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryCountDocumentsWithFiles.
 *
 * <p>Consulta para calcular a quantidade e quantidade percentual de documentos
 * publicados que foram criados e que cont�m arquivos
 * </p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryCountDocumentsWithFiles extends FWDDBQueryHandler {
  
  protected $ciValue = 0;
  protected $ciUserId = 0;
  
  public function __construct($poDB){
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField('','num_docs',DB_NUMBER));
  }

  public function setUserId($piUserId){
    $this->ciUserId = $piUserId;
  }
  
  public function makeQuery(){
    $maFilters = array();
    
    if($this->ciUserId){
      $msJoin = "JOIN view_pm_user_roles ur ON (ur.document_id = d.fkContext AND ur.user_id = {$this->ciUserId})";
      $maFilters[] = "(
           ur.user_is_main_approver = 1
        OR ur.user_is_author        = 1
        OR ur.user_is_approver      = 1
        OR ur.user_is_reader        = 1
      )";
    }else{
      $msJoin = '';
    }
    
    $maFilters[] = "dis.doc_instance_status = ".CONTEXT_STATE_DOC_PUBLISHED;
    $maFilters[] = "d.nType != ".CONTEXT_REGISTER;
    $maFilters[] = "di.bIsLink = 0";
    $maFilters[] = "di.sPath != ''";
    
    $msWhere = '';
    if(count($maFilters)){
      $msWhere = " WHERE ".implode(' AND ', $maFilters);
    }
    $this->csSQL = "SELECT
                      count(DISTINCT d.fkContext) AS num_docs
                    FROM
                      pm_document d
                      JOIN view_pm_doc_instance_status dis ON (dis.document_id = d.fkContext)
                      JOIN pm_doc_instance di ON (di.fkContext = dis.doc_instance_id)
                      {$msJoin}
                    {$msWhere}";
  }
  
  public function executeQuery(){
    parent::executeQuery();
    if($this->coDataSet->fetch()){
      $this->ciValue = $this->coDataSet->getFieldByAlias("num_docs")->getValue();
    }
  }
  
  public function getValues(){
    $this->makeQuery();
    $this->executeQuery();
    return $this->ciValue;
  }

}

?>