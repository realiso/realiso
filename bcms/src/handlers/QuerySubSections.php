<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QuerySubSections.
 *
 * <p>Consulta para buscar as sub-se��es de uma se��o.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QuerySubSections extends FWDDBQueryHandler {
  
  protected $ciSectionId = 0;
    
  protected $caSubSections = array();
  
  public function __construct($poDB) {
    parent::__construct($poDB);

    $this->coDataSet->addFWDDBField(new FWDDBField('pkSection','section_id',DB_NUMBER));
  }
  
  public function setSection($piSectionId) {
    $this->ciSectionId = $piSectionId;
  }
  
  public function makeQuery() {
    $this->csSQL = "SELECT pkSection as section_id from ".FWDWebLib::getFunctionCall("get_sub_sections({$this->ciSectionId})")."
    								JOIN isms_context c ON(c.pkContext = pkSection and c.nState <> 2705)
		";
  }
  
  public function executeQuery() {
    parent::executeQuery();
    while ($this->coDataSet->fetch()) {
      $this->caSubSections[] = $this->coDataSet->getFieldByAlias("section_id")->getValue();
    }
  }
  
  /**
   * Retorna as sub-se��es de uma se��o.
   * 
   * <p>M�todo para retornar as sub-se��es de uma se��o.</p>
   * @access public 
   * @return array Array de ids das sub-se��es
   */ 
  public function getSubSections() {
    return $this->caSubSections;
  }
}
?>