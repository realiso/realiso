<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QuerySummaryIncident.
 *
 * <p>Consulta para popular o grid de sum�rio de incidentes.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QuerySummaryIncident extends FWDDBQueryHandler {

  protected $caIncidentSummary = array();

  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField("","incident_state", DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("","incident_count", DB_NUMBER));
  }

  public function makeQuery() {
    $this->csSQL = "SELECT
											c.nState as incident_state,
											count(i.fkContext) as incident_count
										FROM
											view_isms_context_active c
											JOIN view_ci_incident_active i ON (c.pkContext = i.fkContext)
										GROUP BY
											c.nState";
  }

  public function executeQuery() {
    parent::executeQuery();
    $this->caIncidentSummary = array();
    while ($this->coDataSet->fetch())
      $this->caIncidentSummary[$this->coDataSet->getFieldByAlias("incident_state")->getValue()] = $this->coDataSet->getFieldByAlias("incident_count")->getValue();
  }

  public function getIncidentSummary() {
  	$this->makeQuery();
    $this->executeQuery();
    return $this->caIncidentSummary;
  }
}
?>