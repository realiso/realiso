<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryEventFromBestPractice.
 *
 * <p>Consulta para buscar os eventos de uma melhor pr�tica.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryEventFromBestPractice extends FWDDBQueryHandler {
  
  private $ciBestPracticeId = 0;
  
  private $caEvents = array();
  
  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField("fkEvent","event_id", DB_NUMBER));
  }
  
  public function setBestPracticeId($piBestPracticeId) {
    $this->ciBestPracticeId = $piBestPracticeId;
  }
  
  public function makeQuery() {
    $this->csSQL = "SELECT fkEvent as event_id FROM rm_best_practice_event WHERE fkBestPractice = {$this->ciBestPracticeId}";
  }
  
  public function executeQuery() {
    parent::executeQuery();
    $this->caEvents = array();
    while ($this->coDataSet->fetch()) {
      $this->caEvents[] = $this->coDataSet->getFieldByAlias("event_id")->getValue();
    }
  }
  
  /**
   * Retorna os eventos de uma melhor pr�tica.
   * 
   * <p>M�todo para retornar os eventos de uma melhor pr�tica.</p>
   * @access public 
   * @return array Array de ids dos eventos
   */ 
  public function getEvents() {
    return $this->caEvents;
  }
}
?>