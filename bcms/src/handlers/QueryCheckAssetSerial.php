<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryCheckAssetSerial.
 *
 * <p>Consulta que retorna uma lista com os n�meros de s�rie do sistema.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryCheckAssetSerial extends FWDDBQueryHandler{
	
	private $assetSerialList = array();
	private $choice = false;
	private $ciAssetSerial = '';
	
	public function __construct($poDB=null){
		parent::__construct($poDB);
		
		$this->coDataSet->addFWDDBField(new FWDDBField('fkContext', 'select_id'	 ,DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField('serial'	, 'select_serial',DB_STRING));
	}
		
	public function setAssetSerial($piAssetSerial){
		$this->ciAssetSerial = $piAssetSerial;
	}
	
	public function makeQuery(){
		$this->csSQL = "SELECT fkContext AS select_id, serial AS select_serial
						FROM view_rm_asset_active WHERE serial = '" . $this->ciAssetSerial . "'";
	}
		
	public function fetchAssetSerial() {
		parent::executeQuery();
		$this->assetSerialList = array();
		while ($this->coDataSet->fetch()) {
			$this->assetSerialList[] = $this->coDataSet->getFieldByAlias("select_serial")->getValue();
		}
	}
	
	public function getAssetSerial() {
		if(($this->assetSerialList != '') && (count($this->assetSerialList) > 1)){
			return $this->choice = true;
		}else{
			return $this->choice = false;
		}
	}		
}

?>