<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

class QueryPlaceFromProcess extends FWDDBQueryHandler {

	private $ciProcessId = 0;

	private $caPlaces = array();

	public function __construct($poDB) {
		parent::__construct($poDB);

		$this->coDataSet->addFWDDBField(new FWDDBField("fkplace","place_id", 	DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("sname",  "place_name", 	DB_STRING));
	}

	public function setProcessId($piProcessId) {
		$this->ciProcessId = $piProcessId;
	}

	public function makeQuery() {
		$this->csSQL = "SELECT fkplace as place_id,
    					   sname as place_name  
    				FROM cm_place_process pp JOIN isms_context c ON(pp.fkContext = c.pkContext AND c.nState <> " . CONTEXT_STATE_DELETED . "),
    				cm_place p
    				 WHERE fkprocess = {$this->ciProcessId} 
    				 AND pp.fkplace = p.fkcontext";
	}

	public function fetchPlaces() {
		parent::executeQuery();
		$this->caPlaces = array();
		while ($this->coDataSet->fetch()) {
			$this->caPlaces[] = $this->coDataSet->getFieldByAlias("place_id")->getValue();
		}
	}

	public function getPlaces() {
		return $this->caPlaces;
	}
}
?>