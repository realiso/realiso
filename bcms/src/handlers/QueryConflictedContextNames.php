<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryConflictedContextNames.
 *
 * <p>Consulta para retornar os contextos que possuem conflitos de nome
 * (contextos logicamente deletados, do mesmo tipo com o mesmo nome e com
 * ids diferentes).</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryConflictedContextNames extends FWDDBQueryHandler {
  
  protected $caContextIds = array();
  
  protected $caConflictedContexts = array();
  
  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField('','context_id'  ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('','context_type',DB_NUMBER));
  }
  
  public function setContextIds($paContextIds) {
    $this->caContextIds = $paContextIds;
  }
  
  public function makeQuery() {
    
    $msContextFilter = "";
    if ($this->caContextIds) {
      foreach ($this->caContextIds as $miContextId)
        $msContextFilter .= ($msContextFilter?",":"")." $miContextId ";
      $msContextFilter = " AND a.context_id IN ($msContextFilter) ";
    }
    
    $this->csSQL = "SELECT a.context_id as context_id, a.context_type as context_type
                    FROM context_names a
                    JOIN context_names b on (a.context_name = b.context_name AND
                                             a.context_type = b.context_type AND
                                             a.context_id != b.context_id AND
                                             a.context_state = ".CONTEXT_STATE_DELETED." AND
                                             b.context_state != ".CONTEXT_STATE_DELETED."
                                             $msContextFilter)";
  }
  
  public function executeQuery() {
    parent::executeQuery();
    while ($this->coDataSet->fetch()) {
      $miContextId = $this->coDataSet->getFieldByAlias("context_id")->getValue();
      $miContextType = $this->coDataSet->getFieldByAlias("context_type")->getValue();
      $this->caConflictedContexts[$miContextId] = $miContextType;
    }
  }
  
  /**
   * Retorna o array de contextos com conflitos de nome.
   * 
   * <p>M�todo que retorna o array de contextos com conflitos de nome.</p>
   * @access public 
   * @return array Array de contextos com conflitos de nome
   */ 
  public function getConflictedContexts() {
    return $this->caConflictedContexts;
  }
}
?>