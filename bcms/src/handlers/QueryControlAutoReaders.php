<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryControlAutoReaders.
 *
 * <p>Consulta para buscar os leitores automáticos de um controle.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryControlAutoReaders extends FWDDBQueryHandler {
  
  protected $ciControlId = 0;  
  protected $caAutoReaders = array();  
  
  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField('pu.fkUser','reader_id',DB_NUMBER));
  }
  
  public function setControl($piControlId) {
    $this->ciControlId = $piControlId;
  }
  
  public function makeQuery() {
    $this->csSQL = "SELECT pu.fkUser as reader_id FROM view_rm_risk_control_active rc
										JOIN view_rm_risk_active r ON (rc.fkRisk = r.fkContext)
										JOIN view_rm_asset_active a ON (r.fkAsset = a.fkContext)
										JOIN view_rm_process_asset_active pa ON (a.fkContext = pa.fkAsset)
										JOIN view_pm_process_user_active pu ON (pa.fkProcess = pu.fkProcess)
										WHERE rc.fkControl = {$this->ciControlId}
										GROUP BY pu.fkUser";
  }
  
  public function executeQuery() {
    parent::executeQuery();
    while ($this->coDataSet->fetch()) {
      $this->caAutoReaders[] = $this->coDataSet->getFieldByAlias("reader_id")->getValue();
    }
  }
  
  /**
   * Retorna os leitores automáricos.
   * 
   * <p>Método para retornar os leitores automáticos.</p>
   * @access public 
   * @return array Array de ids de usuários
   */ 
  public function getReaders() {
    return $this->caAutoReaders;
  }
}
?>