<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryContextsSubStructure.
 *
 * <p>Consulta para retornar a estrutura de contextos a ser restaurada.
 * Dado um array de contexts_ids, sub-contextos ou contextos associados a
 * estes contextos os quais n�o fazem parte deste array (ou seja, n�o
 * foram selecionados pelo usu�rio na lixeira), s�o adicionados para
 * tamb�m serem restaurados, visto que algum pai, av�, ou elemento de
 * mais alta ordem foi selecionado para ser restaurado.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryContextsSubStructure extends FWDDBQueryHandler {
  
  protected $caContexts = array();
  
  protected $caUpdatedContexts = array();
  
  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField("fkContext", "context_id",   DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("nType",     "context_type", DB_NUMBER));
 }
  
  public function setContexts($paContexts) {
    $this->caContexts = $paContexts;
  }
  
  public function makeQuery() {
    
    $this->csSQL = "";
    $this->caUpdatedContexts = array();
    
    if ($this->caContexts) {
      foreach ($this->caContexts as $miContextId => $miContextType) {
        $this->caUpdatedContexts[$miContextId] = $miContextType;
        switch($miContextType) {
          case CONTEXT_AREA :
            $this->csSQL .= $this->csSQL ? " UNION " : "";
            $this->csSQL .= "select sub.pkArea as context_id, c.nType as context_type from ".FWDWebLib::getFunctionCall("get_sub_areas($miContextId)")." sub JOIN isms_context c ON (sub.pkArea = c.pkContext)";
            $this->csSQL .= " UNION select sub.pkProcess as context_id, c.nType as context_type from ".FWDWebLib::getFunctionCall("get_processes_from_area($miContextId)")." sub JOIN isms_context c ON (sub.pkProcess = c.pkContext)";
            break;
          case CONTEXT_CATEGORY :
            $this->csSQL .= $this->csSQL ? " UNION " : "";
            $this->csSQL .= "select sub.pkCategory as context_id, c.nType as context_type from ".FWDWebLib::getFunctionCall("get_sub_categories($miContextId)")." sub JOIN isms_context c ON (sub.pkCategory = c.pkContext)";
            $this->csSQL .= " UNION select sub.pkEvent as context_id, c.nType as context_type from ".FWDWebLib::getFunctionCall("get_events_from_category($miContextId)")." sub JOIN isms_context c ON (sub.pkEvent = c.pkContext)";
            break;
          case CONTEXT_SECTION_BEST_PRACTICE :
            $this->csSQL .= $this->csSQL ? " UNION " : "";
            $this->csSQL .= "select sub.pkSection as context_id, c.nType as context_type from ".FWDWebLib::getFunctionCall("get_sub_sections($miContextId)")." sub JOIN isms_context c ON (sub.pkSection = c.pkContext)";
            $this->csSQL .= " UNION select sub.pkBestPractice as context_id, c.nType as context_type from ".FWDWebLib::getFunctionCall("get_bp_from_section($miContextId)")." sub JOIN isms_context c ON (sub.pkBestPractice = c.pkContext)";
            break;
          default : break;
        }
      }
    }
  }
  
  public function executeQuery() {
    parent::executeQuery();
    while ($this->coDataSet->fetch()) {
      $miContextId = $this->coDataSet->getFieldByAlias("context_id")->getValue();
      $miContextType = $this->coDataSet->getFieldByAlias("context_type")->getValue();
      if (!array_key_exists($miContextId,$this->caUpdatedContexts))
        $this->caUpdatedContexts[$miContextId] = $miContextType;
    }
  }
  
  /**
   * Retorna o array de ids de contextos que pertencem a uma estrutura.
   * 
   * <p>M�todo que retorna o array de ids de contextos que pertencem a uma
   * estrutura logicamente deletada necess�ria para a restaura��o de contextos
   * logicamente deletados.</p>
   * @access public 
   * @return array Array de ids de contextos que pertencem a uma estrutura
   */ 
  public function getUpdatedContexts() {
    return $this->caUpdatedContexts;
  }
}
?>