<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

class QueryAreaResource extends FWDDBQueryHandler {

	private $ciAreaId = 0;
	private $resource = false;

	private $caResources = array();

	public function __construct($poDB) {
		parent::__construct($poDB);

		$this->coDataSet->addFWDDBField(new FWDDBField("id","id", 	DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("name","name", 	DB_STRING));
	}

	public function setAreaId($areaId) {
		$this->ciAreaId = $areaId;
	}

	public function setResource($r) {
		$this->resource = $r;
	}

	public function makeQuery() {
		if($this->resource){
			$this->csSQL = "SELECT
								r.fkcontext as id,
								r.name as name
							FROM 
								view_cm_resource_active r 
								JOIN view_cm_area_resource_active pr on pr.fkresource = r.fkcontext
							WHERE
								pr.fkarea = {$this->ciAreaId}";
		}else{
			$this->csSQL = "SELECT
								g.fkcontext as id,
								g.name as name
							FROM 
								view_cm_group_active g 
								JOIN view_cm_area_resource_active pr on pr.fkgroup = g.fkcontext
							WHERE
								pr.fkarea = {$this->ciAreaId}";
		}
	}

	public function fetchResources() {
		parent::executeQuery();
		$this->caResources = array();
		while ($this->coDataSet->fetch()) {
			$this->caResources[] = $this->coDataSet->getFieldByAlias("id")->getValue();
		}
	}

	public function getResources() {
		return $this->caResources;
	}
}
?>