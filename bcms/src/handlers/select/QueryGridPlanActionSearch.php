<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

class QueryGridPlanActionSearch extends FWDDBQueryHandler {

	private $csNameFilter = "";
	
	private $plan = 0;

	private $caIds = array();

	private $caExcludedIds = array();

	private $cbEmpty = false;

	public function __construct($poDB){
		parent::__construct($poDB);

		$this->coDataSet->addFWDDBField(new FWDDBField('','plan_action_id',     		     DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField('','plan_action_group',     		 	 DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField('','plan_action_resume',     		 DB_STRING));
	}

	public function setNameFilter($psNameFilter) {
		$this->csNameFilter = $psNameFilter;
	}

	public function getIdValue(){
		return $this->getFieldValue("plan_action_id");
	}	

	public function getNameValue(){
		return $this->getFieldValue("plan_action_resume");
	}	

	public function setPlanFilter($plan) {
		$this->plan = $plan;
	}

	public function setIds($paIds){
		$this->caIds = $paIds;
	}

	public function setExcludedIds($paExcludedIds){
		$this->caExcludedIds = $paExcludedIds;
	}

	public function setEmpty($pbEmpty){
		$this->cbEmpty = $pbEmpty;
	}

	public function makeQuery(){
		$msWhere = " AND pa.fkplan = {$this->plan} ";

		if($this->cbEmpty){
			$msWhere.= " AND pa.fkContext = 0";
		}

		if($this->csNameFilter){
			$msWhere.= " AND upper(pa.resume) like upper('%{$this->csNameFilter}%')";
		}

		if(count($this->caIds)){
			$msWhere .= " AND pa.fkContext IN (".implode(',',$this->caIds).")";
		}

		if(count($this->caExcludedIds)){
			$msWhere .= " AND pa.fkContext NOT IN (".implode(',',$this->caExcludedIds).")";
		}

		$this->csSQL = "SELECT
						pa.fkContext as plan_action_id,  
						g.acronym as plan_action_group,
						pa.resume as plan_action_resume 
					FROM  
						view_cm_plan_action_active pa
						JOIN view_cm_group_active g on pa.fkgroup = g.fkcontext  
					WHERE 1=1 "
					.$msWhere;

	}
}
?>