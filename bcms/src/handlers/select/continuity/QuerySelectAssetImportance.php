<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QuerySelectNcProcess.
 *
 * <p></p>
 * @package ISMS
 * @subpackage handlers
 */
class QuerySelectAssetImportance extends FWDDBQueryHandler {

  //protected $caPeopleIds = null;
  //protected $caProcessIds = null;

  public function __construct($poDB=null){
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField('','select_id'   ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('','select_value',DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('','select_index'   ,DB_NUMBER));
  }

  public function makeQuery(){
    
    $this->csSQL = "SELECT
                      i.fkcontext AS select_id,
                      i.sName AS select_value,
                      i.nimportance AS select_index
                    FROM isms_context c
                    JOIN cm_asset_importance i ON (i.fkcontext = c.pkcontext AND c.nState != " . CONTEXT_STATE_DELETED . ") 
                    ORDER BY select_index";
  }

  public function getValues() {
    $this->makeQuery();
    $this->executeQuery();
    $maValues = array();
    while ($this->coDataSet->fetch()) {
      $maValues[] = array($this->coDataSet->getFieldByAlias("select_id")->getValue(), $this->coDataSet->getFieldByAlias("select_value")->getValue());
    }
    return $maValues;
  }

  public function getIds(){
    $this->makeQuery();
    $this->executeQuery();
    $maIds = array();
    while($this->fetch()){
      $maIds[] = $this->getFieldValue('select_id');
    }
    return $maIds;
  }

}
?>