<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QuerySelectNcProcess.
 *
 * <p></p>
 * @package ISMS
 * @subpackage handlers
 */
class QuerySelectProcessInputs extends FWDDBQueryHandler {

  protected $ciProcess = 0;
  protected $caProcessIds = null;

  public function __construct($poDB=null){
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField('fkcontext',  'process_input_id',	DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('fk_process', 'process_id',			DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('fk_process_input', 'input_id',		DB_NUMBER));
    
  }

  public function setProcess($piProcess){
    $this->ciProcess = $piProcess;
  }


  public function setProcessIds($psProcessIds){
    $this->caProcessIds = array_filter(explode(':',$psProcessIds));
  }

  public function makeQuery(){
    if($this->ciProcess){
      $msWhere = "pi.fk_process = {$this->ciProcess}";
    }elseif($this->caProcessIds!==null){
      if(count($this->caProcessIds)>0){
        $msWhere = 'pi.fkcontext IN ('.implode(',',$this->caProcessIds).')';
      }else{
        $msWhere = '1=0';
      }
    }else{
      trigger_error("You must specify a filter.",E_USER_ERROR);
    }

    $this->csSQL = "SELECT
                      pi.fkcontext AS process_input_id,
                      pi.fk_process AS process_id,
                      pi.fk_process_input AS input_id
                      
                    FROM cm_process_inputs as pi
                    WHERE {$msWhere}
                    ";
  }

/*  public function getValues() {
    $this->makeQuery();
    $this->executeQuery();
    $maValues = array();
    while ($this->coDataSet->fetch()) {
      $maValues[] = array($this->coDataSet->getFieldByAlias("select_id")->getValue(), $this->coDataSet->getFieldByAlias("select_value")->getValue());
    }
    return $maValues;
  }

  public function getIds(){
    $this->makeQuery();
    $this->executeQuery();
    $maIds = array();
    while($this->fetch()){
      $maIds[] = $this->getFieldValue('select_id');
    }
    return $maIds;
  } */

}
?>