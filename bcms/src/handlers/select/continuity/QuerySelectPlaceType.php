<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QuerySelectPlaceType.
 *
 * <p>Consulta que retorna uma lista com os tipos de locais do sistema.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QuerySelectPlaceType extends FWDDBQueryHandler {

	private $caContexts = array();

	public function __construct($poDB) {
		parent::__construct($poDB);

		$this->coDataSet->addFWDDBField(new FWDDBField("fkContext", "context_id",    DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("sName",     "context_name",  DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField("nLevel",    "context_level", DB_NUMBER));
	}

	public function makeQuery(){
		$this->csSQL = "select fkContext as context_id, sName as context_name, nLevel as context_level from ".FWDWebLib::getFunctionCall("get_place_category_tree(0,0)");
	}

	public function executeQuery() {
		parent::executeQuery();
		$this->caContexts = array();
		while ($this->coDataSet->fetch()) {
			$miContextId = $this->coDataSet->getFieldByAlias("context_id")->getValue();
			$miContextName = $this->coDataSet->getFieldByAlias("context_name")->getValue();
			$miContextLevel = $this->coDataSet->getFieldByAlias("context_level")->getValue();
			$msHierarchicalName = str_repeat("&nbsp;&nbsp;",$miContextLevel).$miContextName;
			$this->caContexts[] = array($miContextId,$msHierarchicalName);
		}
	}

	public function getContexts() {
		return $this->caContexts;
	}
}
?>
