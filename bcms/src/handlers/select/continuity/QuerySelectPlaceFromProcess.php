<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QuerySelectPlaceFromProcess.
 *
 * <p>Consulta para buscar os processos de um ativo.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QuerySelectPlaceFromProcess extends FWDDBQueryHandler {
  
  private $ciProcessId = 0;
  
  private $caPlaces = array();
  
  public function __construct($poDB) {
    parent::__construct($poDB);

    $this->coDataSet->addFWDDBField(new FWDDBField("fkplace","select_id", 	DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("sname",  "select_value", 	DB_STRING));
  }
  
  public function setProcessId($piProcessId) {
    $this->ciProcessId = $piProcessId;
  }
  
  public function makeQuery() {
    $this->csSQL = "SELECT fkplace as select_id,
    					   sname as select_value  
    				FROM cm_place_process s JOIN isms_context c ON(s.fkContext = c.pkContext AND c.nState <> " . CONTEXT_STATE_DELETED . "),
    				cm_place sc
    				 WHERE fkprocess = {$this->ciProcessId} 
    				 AND s.fkplace = sc.fkcontext";
  }
  
  public function fetchPlaces() {
    parent::executeQuery();
    $this->caPlaces = array();
    while ($this->coDataSet->fetch()) {
      $this->caPlaces[] = $this->coDataSet->getFieldByAlias("place_id")->getValue();
    }
  }
  
  /**
   * Retorna os processos de um ativo.
   * 
   * <p>M�todo para retornar os processos de um ativo.</p>
   * @access public 
   * @return array Array de ids dos processos
   */ 
  public function getPlaces() {
    return $this->caPlaces;
  }
}
?>