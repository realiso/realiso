<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QuerySelectOccurrence.
 *
 * <p>Consulta que retorna uma lista com as ocorr�ncias do m�dulo de incidente.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QuerySelectOccurrence extends FWDDBQueryHandler {
  
  protected $ciIncidentId;
  protected $caValues = array();
  
  public function __construct($poDB) {
    parent::__construct($poDB); 
    $this->coDataSet->addFWDDBField(new FWDDBField('fkContext','select_id',DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('tDescription','select_value',DB_STRING));

  }
  
  public function makeQuery() {
    $maFilters = array();
    if (intval($this->ciIncidentId) > 0) {
      $maFilters[] = "fkIncident={$this->ciIncidentId}"; 
    }
    $msWhere = count($maFilters)?" WHERE ".implode(' AND ',$maFilters):"";
    $this->csSQL = "
SELECT fkContext as select_id, tDescription as select_value
FROM view_ci_occurrence_active
$msWhere
";
  }
  
  public function executeQuery() {
    parent::executeQuery();     
    while ($this->coDataSet->fetch()) {     
      $this->caValues[] = array($this->coDataSet->getFieldByAlias("select_id")->getValue(), $this->coDataSet->getFieldByAlias("select_value")->getValue());
    }
  }
  
  public function setIncidentId($piIncidentId) {
    $this->ciIncidentId = $piIncidentId;
  }
  
  public function getValues() {
    return $this->caValues;
  }
}
?>