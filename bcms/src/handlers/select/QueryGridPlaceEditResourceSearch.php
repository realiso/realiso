<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

class QueryGridPlaceEditResourceSearch extends FWDDBQueryHandler implements QueryAutoCompleter {

	private $csNameFilter = "";

	private $showResources = true;

	private $caIds = array();

	private $caExcludedIds = array();

	private $cbEmpty = false;

	private $limit = 0;

	public function __construct($poDB){
		parent::__construct($poDB);

		$this->coDataSet->addFWDDBField(new FWDDBField('','group_id',     		     DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField('','group_name',     		 	 DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField('','group_acronym',     		 DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField('','type',     		 	 DB_STRING));
	}

	public function getIdValue(){
		return $this->getFieldValue("group_id");
	}
	
	public function getNameValue(){
		return $this->getFieldValue("group_name");
	}
	
	public function setNameFilter($psNameFilter) {
		$this->csNameFilter = $psNameFilter;
	}

	public function setShowResourcesFilter($showResources) {
		$this->showResources = $showResources;
	}

	public function setLimit($l) {
		$this->limit = $l;
	}

	public function setIds($paIds){
		$this->caIds = $paIds;
	}

	public function setExcludedIds($paExcludedIds){
		$this->caExcludedIds = $paExcludedIds;
	}

	public function setEmpty($pbEmpty){
		$this->cbEmpty = $pbEmpty;
	}

	public function makeQuery(){
		$msWhere = '';
		$msWhereResource = '';

		if($this->cbEmpty){
			$msWhereResource.= "AND r.fkContext = 0";
			$msWhere.= "AND g.fkContext = 0";
		}

		if($this->csNameFilter){
			$msWhereResource.= " AND (upper(r.name) like upper('%{$this->csNameFilter}%'))";
			$msWhere.= " AND (upper(g.name) like upper('%{$this->csNameFilter}%') OR upper(g.acronym) like upper('%{$this->csNameFilter}%'))";
		}

		if(count($this->caIds)){
			$msWhereResource .= " AND r.fkContext IN (".implode(',',$this->caIds).")";
			$msWhere .= " AND g.fkContext IN (".implode(',',$this->caIds).")";
		}

		if(count($this->caExcludedIds)){
			$msWhereResource .= " AND r.fkContext NOT IN (".implode(',',$this->caExcludedIds).")";
			$msWhere .= " AND g.fkContext NOT IN (".implode(',',$this->caExcludedIds).")";
		}

		$joinResource = "";

		$limit = "";
		if($this->limit > 0){
			$limit = " limit {$this->limit}";
		}
		$this->csSQL .= "
						SELECT					
							r.fkContext as group_id, 
							r.name as group_name, 
							r.func as group_acronym,
							'resource' as type
						FROM
							view_cm_resource_active r
							{$joinResource}
						WHERE 1=1
						{$msWhereResource}" .$limit;		
	}
}
?>