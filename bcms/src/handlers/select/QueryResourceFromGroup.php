<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

class QueryResourceFromGroup extends FWDDBQueryHandler {

	private $ciGroupId = 0;
	private $ciResourceId = 0;

	private $caResources = array();

	public function __construct($poDB) {
		parent::__construct($poDB);

		$this->coDataSet->addFWDDBField(new FWDDBField("fkresource","resource_id", 	DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("name",  "resource_name", 	DB_STRING));
	}

	public function setGroupId($groupId) {
		$this->ciGroupId = $groupId;
	}

	public function setResourceId($ciResourceId) {
		$this->ciResourceId = $ciResourceId;
	}

	public function makeQuery() {
		$where = '';
		if($this->ciResourceId != 0){
			$where = " gr.fkresource = {$this->ciResourceId} ";
			$this->csSQL = "SELECT fkgroup as resource_id,
    					   g.name as resource_name  
    				FROM cm_group_resource gr
    				JOIN view_cm_group_active g on g.fkcontext = gr.fkgroup
    				 WHERE {$where} 
    				 ORDER BY gr.escalation";
		}else{
			$this->csSQL = "SELECT fkresource as resource_id,
    					   r.name as resource_name  
    				FROM cm_group_resource gr
    				JOIN view_cm_resource_active r on r.fkcontext = gr.fkresource
    				 WHERE gr.fkgroup = {$this->ciGroupId} 
    				 ORDER BY gr.escalation";
		}
	}

	public function fetchResources() {
		parent::executeQuery();
		$this->caResources = array();
		while ($this->coDataSet->fetch()) {
			$this->caResources[] = $this->coDataSet->getFieldByAlias("resource_id")->getValue();
		}
	}

	public function getResources() {
		return $this->caResources;
	}
}
?>