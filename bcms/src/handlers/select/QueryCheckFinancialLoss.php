<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

class QueryCheckFinancialLoss extends FWDDBQueryHandler{

	private $values_financial_loss = array();
		
	public function __construct($poDB){
		parent::__construct($poDB);
		
		$this->coDataSet->addFWDDBField(new FWDDBField('fi.fkContext'		,'id'   	,DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField('fi.sName'    		,'name'		,DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField('fi.nstartrange'		,'start'   	,DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField('fi.nendrange'		,'end'   	,DB_NUMBER));
	}

	public function makeQuery(){
		$this->csSQL = "SELECT 
								fi.fkcontext as id, 
								fi.sname as name, 
								fi.nstartrange as start, 
								fi.nendrange as end
						FROM cm_financial_impact fi
						";
	}
	
	public function fetchFinancialLoss() {
		parent::executeQuery();
		while ($this->coDataSet->fetch()) {
			$this->values_financial_loss[] = $this->coDataSet->getFieldByAlias("start")->getValue();
			$this->values_financial_loss[] = $this->coDataSet->getFieldByAlias("end")->getValue();
		}
	}
	
	public function getValuesFinancialLoss() {
		return $this->values_financial_loss;
	}
}
?>