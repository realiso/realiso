<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QuerySelectReadableDocumentByContext.
 *
 * <p>Consulta que retorna os documentos, associados a um contexto, 
 * que podem ser lidos pelo usu�rio logado.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QuerySelectReadableDocumentByContext extends FWDDBQueryHandler {	
  
  private $ciContextId=0;
  
  public function __construct($poDB) {
    parent::__construct($poDB); 

    $this->coDataSet->addFWDDBField(new FWDDBField('d.fkContext','select_id',DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('d.sName','select_value',DB_STRING));
  }

  public function setContextId($piContextId){
    if ($piContextId) $this->ciContextId = $piContextId;
  }
  
	public function makeQuery(){
    $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    $maFilters = array();
    $maFilters[] = "(
        dr.fkDocument IS NOT NULL
        OR da.fkDocument IS NOT NULL
        OR d.fkMainApprover = $miUserId
        OR d.fkAuthor = $miUserId
      )AND(
        di.nMajorVersion > 0
        OR (
          c.nState = ".CONTEXT_STATE_DOC_APPROVED."
          AND (
            d.dDateProduction <= '".date('Y-m-d')."'
            OR d.fkMainApprover = $miUserId
          )
        )OR(
          c.nState = ".CONTEXT_STATE_DOC_PENDANT."
          AND (
            da.fkDocument IS NOT NULL
            OR d.fkMainApprover = $miUserId
          )
        )
      )";
    
    $maFilters[] = "dc.fkContext = {$this->ciContextId}";
        
    $this->csSQL ="SELECT d.fkContext as select_id, d.sName as select_value
                   FROM
                     view_pm_document_active d
                     JOIN isms_context c ON (d.fkContext=c.pkContext)
                     LEFT JOIN pm_doc_approvers da ON (da.fkDocument = d.fkContext AND da.fkUser = $miUserId)
                     LEFT JOIN pm_doc_readers dr ON (dr.fkDocument = d.fkContext AND dr.fkUser = $miUserId AND dr.bDenied=0)
                     LEFT JOIN pm_doc_instance di ON (d.fkCurrentVersion = di.fkContext)
                     LEFT JOIN pm_doc_context dc ON (dc.fkDocument = d.fkContext)
                   WHERE ".implode(' AND ',$maFilters);
  }		
}
?>
