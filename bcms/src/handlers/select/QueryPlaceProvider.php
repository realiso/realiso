<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

class QueryPlaceProvider extends FWDDBQueryHandler {

	private $ciPlaceId = 0;

	private $caProviders = array();

	public function __construct($poDB) {
		parent::__construct($poDB);

		$this->coDataSet->addFWDDBField(new FWDDBField("id","id", 	DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("name","name", 	DB_STRING));
	}

	public function setPlaceId($placeId) {
		$this->ciPlaceId = $placeId;
	}

	public function makeQuery() {
		$this->csSQL = "SELECT
							p.fkcontext as id,
							p.name as name
						FROM 
							view_cm_provider_active p 
							JOIN view_cm_place_provider_active pp on pp.fkprovider = p.fkcontext
						WHERE
							pp.fkplace = {$this->ciPlaceId}";
	}

	public function fetchProviders() {
		parent::executeQuery();
		$this->caProviders = array();
		while ($this->coDataSet->fetch()) {
			$this->caProviders[] = $this->coDataSet->getFieldByAlias("id")->getValue();
		}
	}

	public function getProviders() {
		return $this->caProviders;
	}
}
?>