<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

class QueryCheckTacTraAsset extends FWDDBQueryHandler{

	private $value_tra = null;
	private $value_tac = null;
	private $choice = false;
	private $ciAssetId = null;
	
	public function __construct($poDB){
		parent::__construct($poDB);
		
		$this->coDataSet->addFWDDBField(new FWDDBField('a.fkContext','asset_id'   	,DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField('a.sName'    ,'asset_name'	,DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField('a.tac'		,'value_tac'   	,DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField('a.tra'		,'value_tra'   	,DB_NUMBER));
	}

	public function setAssetId($piAssetId){
		$this->ciAssetId = $piAssetId;
	} 
	
	public function makeQuery(){
		$this->csSQL = "SELECT 
							a.fkContext as asset_id, a.sName as asset_name, a.tac as value_tac, a.tra as value_tra
		                FROM 
		                	view_rm_asset_active a 
						WHERE
							a.fkContext = $this->ciAssetId";
	}
	
	public function fetchTacTraValue() {
		parent::executeQuery();
		while ($this->coDataSet->fetch()) {
			$this->value_tra = $this->coDataSet->getFieldByAlias("value_tra")->getValue();
			$this->value_tac = $this->coDataSet->getFieldByAlias("value_tac")->getValue();
		}
	}
	
	public function checkTacTraValue() {
		if($this->value_tac > $this->value_tra){
			return $this->choice = true;
		}else{
			return $this->choice = false;
		}
	}
}
?>