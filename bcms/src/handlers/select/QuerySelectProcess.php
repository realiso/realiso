<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QuerySelectProcess.
 *
 * <p>Consulta que retorna uma lista com os processos do sistema.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QuerySelectProcess extends FWDDBQueryHandler {

  protected $ciAreaId = 0;
  
  public function __construct($poDB) {
    parent::__construct($poDB); 

    $this->coDataSet->addFWDDBField(new FWDDBField('p.fkContext','select_id'   ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('p.sName'    ,'select_value',DB_STRING));
  }

  public function setArea($piAreaId){
    $this->ciAreaId = $piAreaId;
  }

  public function makeQuery(){
    $this->csSQL = "SELECT p.fkContext as select_id, p.sName as select_value
                    FROM view_rm_process_active p ";
    if($this->ciAreaId){
      $this->csSQL.= "JOIN view_rm_area_active a ON (a.fkContext = p.fkArea)
                      WHERE a.fkContext = {$this->ciAreaId} ";
    }
    $this->csSQL.= "ORDER BY p.sName";
  }
}
?>