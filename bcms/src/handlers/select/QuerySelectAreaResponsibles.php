<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QuerySelectAreaResponsibles.
 *
 * <p>Consulta para popular o select do filtro de �reas por respons�vel.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QuerySelectAreaResponsibles extends FWDDBQueryHandler {

  protected $ciUserId = 0;

  public function __construct($poDB=null){
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField('u.fkContext','select_id'   ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('u.sName'    ,'select_value',DB_STRING));
  }

  public function setUserId($piUserId){
    $this->ciUserId = $piUserId;
  }

  public function makeQuery(){
    $maFilters = array();
    
    if($this->ciUserId){
      if(!ISMSLib::userHasACL('M.RM.1.1')){
        if(ISMSLib::userHasACL('M.RM.1.2')){
          $msFunctionCall = FWDWebLib::getFunctionCall("get_areas_by_user({$this->ciUserId})");
        }else{
          $msFunctionCall = FWDWebLib::getFunctionCall("get_superareas_by_user({$this->ciUserId})");
        }
        $maFilters[] = "a.fkContext IN (SELECT * FROM {$msFunctionCall})";
      }
    }
    
    if(count($maFilters)==0){
      $msWhere = '';
    }else{
      $msWhere = ' WHERE '.implode(' AND ',$maFilters);
    }
    $this->csSQL = "SELECT DISTINCT
                      u.fkContext AS select_id,
                      u.sName AS select_value
                    FROM
                      view_isms_user_active u
                      JOIN view_rm_area_active a ON (a.fkResponsible = u.fkContext)
                    {$msWhere}
                    ORDER BY u.sName";
  }

}

?>