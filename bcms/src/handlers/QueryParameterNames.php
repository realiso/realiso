<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryParameterNames.
 *
 * <p>Consulta para buscar os nomes dos parāmetros.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryParameterNames extends FWDDBQueryHandler {

  public function __construct($poDB) {
    parent::__construct($poDB); 

    $this->coDataSet->addFWDDBField(new FWDDBField('pkParameterName','parametername_id'  ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('sName'          ,'parametername_name',DB_STRING));
  }

  public function makeQuery(){
    $this->csSQL = "SELECT pkParameterName as parametername_id, sName as parametername_name FROM rm_parameter_name ORDER BY sName";
  }

}
?>