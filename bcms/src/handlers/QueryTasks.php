<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryTasks.
 *
 * <p>Consulta para receber a lista de tarefas do sistema.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryTasks extends FWDDBQueryHandler {	
	
	protected $ciTaskId;
	protected $ciUserId;
	
	public function __construct($poDB) {
		parent::__construct($poDB);	

		$this->coDataSet->addFWDDBField(new FWDDBField("t.pkTask","task_id", DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("t.dDateCreated","task_date_created", DB_DATETIME));
		$this->coDataSet->addFWDDBField(new FWDDBField("t.nActivity","task_activity", DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("cn.context_id","task_context_id", DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("cn.context_type","task_context_type", DB_NUMBER));
 }
	
	public function setTask($piTaskId) {
		$this->ciTaskId = $piTaskId;
	}
	
	public function setUser($piUserId) {
		$this->ciUserId = $piUserId;
	}
	
	public function makeQuery() {
		$msWhere = '';
		if ($this->ciTaskId) {
			$msWhere .= " AND t.pkTask = ".$this->ciTaskId;
		}
		$this->csSQL = "SELECT t.pkTask as task_id, t.dDateCreated as task_date_created, t.nActivity as task_activity, cn.context_id as task_context_id, cn.context_type as task_context_type
					FROM wkf_task t JOIN context_names cn ON (t.fkContext = cn.context_id)
					WHERE t.bVisible = 1 AND t.fkReceiver = ".$this->ciUserId
					.$msWhere;		
	} 
}
?>