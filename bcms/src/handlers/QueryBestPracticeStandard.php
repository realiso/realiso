<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryBestPracticeStandard.
 *
 * <p>Consulta para auxiliar na exporta��o de normas ligadas � melhores pr�ticas.</p>
 * @package ISMS
 * @subpackage handlers
 */

class QueryBestPracticeStandard extends FWDDBQueryHandler {

	protected $csStandardIds;
	protected $csBestPracticeIds;
	protected $caBestPracticeStandards;
	
  public function __construct($poDB) {
    parent::__construct($poDB); 

    $this->coDataSet->addFWDDBField(new FWDDBField('fkContext'     ,'BestPracticeStandardId',DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('fkBestPractice','BestPracticeId'        ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('fkStandard'    ,'StandardId'            ,DB_NUMBER));
  }
  public function makeQuery(){
  	$msWhere = "";
  	if ($this->csBestPracticeIds || $this->csStandardIds) { 
  		$msWhere = " WHERE";
  		if ($this->csBestPracticeIds) {
  			$msWhere .= " fkBestPractice IN (".$this->csBestPracticeIds.")";
  		}
  		if ($this->csBestPracticeIds && $this->csStandardIds) {
  			$msWhere .= " AND";
  		}
  		if ($this->csStandardIds) {
  			$msWhere .= " fkStandard IN (".$this->csStandardIds.")";
  		}
  	}
    $this->csSQL = "SELECT fkContext as BestPracticeStandardId, fkBestPractice as BestPracticeId, fkStandard as StandardId
										FROM rm_best_practice_standard".$msWhere;
  }
  
  public function executeQuery() {
    parent::executeQuery();
    while ($this->coDataSet->fetch()) {
      $this->caBestPracticeStandards[] = $this->coDataSet->getFieldByAlias("BestPracticeStandardId")->getValue();
    }
  }
  
  public function getBestPracticeStandards() {
  	return $this->caBestPracticeStandards;
  }
  
	public function setBestPracticeIds($psBestPracticeIds) {
		$this->csBestPracticeIds = $psBestPracticeIds;
	}
	
	public function setStandardIds($psStandardIds) {
		$this->csStandardIds = $psStandardIds;
	}
}
?>