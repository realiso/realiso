<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryCategoryParents.
 *
 * <p>Consulta para buscar as categorias pai[,av�[,bisav�[,...]]] de uma categoria.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryProcessCategoryParents extends FWDDBQueryHandler {
  
  protected $ciCategoryId = 0;
  
  protected $caParentCategories = array();
  
  protected $cbExecByTrash = false;
  
  public function __construct($poDB,$pbExecByTrash = false) {
    parent::__construct($poDB);
    $this->cbExecByTrash = $pbExecByTrash;
    
    $this->coDataSet->addFWDDBField(new FWDDBField('pkCategory','category_id',DB_NUMBER));
  }
  
  public function setCategory($piCategoryId) {
    $this->ciCategoryId = $piCategoryId;
  }
  
  public function makeQuery() {
    $msExecTrash = '';
		if($this->cbExecByTrash)
			$msExecTrash = '_trash';
    $this->csSQL = "SELECT pkCategory as category_id from ".FWDWebLib::getFunctionCall("get_process_category_parents{$msExecTrash}({$this->ciCategoryId})");
  }
  
  public function executeQuery() {
    parent::executeQuery();
    while ($this->coDataSet->fetch()) {
      $this->caParentCategories[] = $this->coDataSet->getFieldByAlias("category_id")->getValue();
    }
  }
  
  /**
   * Retorna as categorias pai de uma categoria.
   * 
   * <p>M�todo para retornar as categorias pai de uma categoria.</p>
   * @access public 
   * @return array Array de ids das categorias
   */ 
  public function getParentCategories() {
    return $this->caParentCategories;
  }
}
?>