<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryAssetFromActivity.
 *
 * <p>Consulta para buscar os ativos de um processo.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryAssetFromActivity extends FWDDBQueryHandler {

	private $ciActivityId = 0;

	private $caAssets = array();

	public function __construct($poDB) {
		parent::__construct($poDB);

		$this->coDataSet->addFWDDBField(new FWDDBField("fkAsset","asset_id", DB_NUMBER));
	}

	public function setActivityId($piActivityId) {
		$this->ciActivityId = $piActivityId;
	}

	public function makeQuery() {
		$this->csSQL = "SELECT fkAsset as asset_id FROM view_cm_process_activity_asset_active WHERE fkActivity = {$this->ciActivityId}";
	}

	public function executeQuery() {
		parent::executeQuery();
		$this->caAssets = array();
		while ($this->coDataSet->fetch()) {
			$this->caAssets[] = $this->coDataSet->getFieldByAlias("asset_id")->getValue();
		}
	}

	public function getAssets() {
		return $this->caAssets;
	}
}
?>