<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryControlImplementationExpired.
 *
 * <p></p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryControlImplementationExpired extends FWDDBQueryHandler {
	
	private $ciUserId;

  public function __construct($poDB,$piUserId){
    parent::__construct($poDB);
    $this->ciUserId = $piUserId;
    
    $this->coDataSet->addFWDDBField(new FWDDBField('c.fkContext'    ,'id'                 ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('c.dDateDeadline','date_implementation',DB_DATETIME));
  }

  public function makeQuery(){
    $this->csSQL =" 
SELECT c.fkContext as id, c.dDateDeadline as date_implementation
   FROM rm_control c
      JOIN isms_context co ON(c.fkContext = co.pkContext AND co.nState <> " . CONTEXT_STATE_DELETED . ")
   WHERE   
	    c.fkResponsible =". $this->ciUserId ." AND
      c.dDateImplemented is null AND 
      (c.bFlagImplExpired is null OR c.bFlagImplExpired <> 1)

";    
  }   
//c.dDateDeadline <= GETDATE() + 1 AND       c.dDateImplemented is null
//c.sName as name, co.nType as type, ce.dDateLimit as date_limit
}
?>