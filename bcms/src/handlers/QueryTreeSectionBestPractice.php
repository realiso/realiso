<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryTreeSectionBestPractice.
 *
 * <p>Consulta para popular a tree de melhores pr�ticas.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryTreeSectionBestPractice extends FWDDBQueryHandler {	
		
	protected $ciSectionBestPracticeId = 0;
	
	public function __construct($poDB) {
		parent::__construct($poDB);	

		$this->coDataSet->addFWDDBField(new FWDDBField('','id', DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField('','node_level', DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField('','node_value', DB_STRING));			
	}
	
	public function setRootSection($piSectionBestPracticeId) {
		$this->ciSectionBestPracticeId = $piSectionBestPracticeId;
	}

	public function makeQuery() {
		$this->csSQL = "SELECT fkContext as id, sName as node_value, nLevel as node_level 
										FROM ".FWDWebLib::getFunctionCall("get_section_tree(".$this->ciSectionBestPracticeId.",-1)")."
										JOIN isms_context c ON (fkContext = c.pkContext AND c.nState <> " . CONTEXT_STATE_DELETED . ")
            				JOIN context_history ch ON (fkContext = ch.context_id)";
	} 
	
}
?>