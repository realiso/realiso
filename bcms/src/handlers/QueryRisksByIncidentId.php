<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryRisksByIncidentId.
 *
 * <p>Consulta para retornar os ids dos riscos relacionados ao incident.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryRisksByIncidentId extends FWDDBQueryHandler {  
  
  protected $ciIncidentId = "";

  public function __construct($poDB) {
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField("fkRisk", "risk_id",     DB_NUMBER));
  }
  
  public function setIncidentId($piIncidentId){
    $this->ciIncidentId = $piIncidentId;
  
  }

  
    
  public function makeQuery() {
  $this->csSQL ="
SELECT fkRisk as risk_id
  FROM view_ci_incident_risk_active
  WHERE fkIncident =  $this->ciIncidentId
";  
  }
  
  public function getValues(){
    $this->makeQuery();
    $this->executeQuery();
    $maValues = array();
    
    while($this->coDataSet->fetch()){
     $maValues [] =$this->coDataSet->getFieldByAlias("risk_id")->getValue();
    }
    return $maValues;
  }
}
?>