<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryDocumentCanRead.
 *
 * <p></p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryDocumentCanRead extends FWDDBQueryHandler {

  private $ciDocumentId;

  public function __construct($poDB) {
    parent::__construct($poDB); 

    $this->coDataSet->addFWDDBField(new FWDDBField('d.fkContext','fkContext',DB_NUMBER));
  }


  public function setDocumentId($piDocumentId){
    $this->ciDocumentId = $piDocumentId;
  }
  
  public function makeQuery(){
    $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    /*descobre se � um documento do tipo registro, se for deve-se adicionar na query os leitores do registro*/
    $moDoc = new PMDocument();
    $moDoc->fetchById($this->ciDocumentId);
    if($moDoc->getFieldValue('document_type')== CONTEXT_REGISTER ){
      $msSelectRegister  = "OR rr.fkRegister IS NOT NULL";
      $msFilterRegister  = "LEFT JOIN pm_register reg ON (reg.fkDocument = d.fkContext)";
      $msFilterRegister .= "LEFT JOIN pm_register_readers rr ON (rr.fkRegister = reg.fkContext AND rr.fkUser = $miUserId AND rr.bDenied=0)";
    }else{
      $msSelectRegister = "";
      $msFilterRegister = "";
    }
    $maFilters = array();
    $maFilters[] = "(
        dr.fkDocument IS NOT NULL
        $msSelectRegister
        OR da.fkDocument IS NOT NULL
        OR d.fkMainApprover = $miUserId
        OR d.fkAuthor = $miUserId
      )AND(
        di.nMajorVersion > 0
        OR (
          c.nState = ".CONTEXT_STATE_DOC_APPROVED."
          AND (
            d.dDateProduction <= '".date('Y-m-d')."'
            OR d.fkMainApprover = $miUserId
          )
        )OR(
          c.nState = ".CONTEXT_STATE_DOC_PENDANT."
          AND (
            da.fkDocument IS NOT NULL
            OR d.fkMainApprover = $miUserId
          )
        )
      )";
    
    if($this->ciDocumentId){
      $maFilters[] = "d.fkContext = {$this->ciDocumentId}";
    }
    
    $this->csSQL ="SELECT d.fkContext as fkContext
                   FROM
                     view_pm_document_active d
                     JOIN isms_context c ON (d.fkContext=c.pkContext)
                     LEFT JOIN pm_doc_approvers da ON (da.fkDocument = d.fkContext AND da.fkUser = $miUserId)
                     LEFT JOIN pm_doc_readers dr ON (dr.fkDocument = d.fkContext AND dr.fkUser = $miUserId AND dr.bDenied=0)
                     $msFilterRegister
                     LEFT JOIN pm_doc_instance di ON (d.fkCurrentVersion = di.fkContext)
                   WHERE ".implode(' AND ',$maFilters);
  }
  
}

?>