<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

class QueryProcessFromScope extends FWDDBQueryHandler {

	private $scopeId = 0;

	private $caProcess = array();

	public function __construct($poDB) {
		parent::__construct($poDB);

		$this->coDataSet->addFWDDBField(new FWDDBField("fkprocess","process_id", 	DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("sname",  "process_name", 	DB_STRING));
	}

	public function setScopeId($piScopeId) {
		$this->scopeId = $piScopeId;
	}

	public function makeQuery() {
		$this->csSQL = "SELECT fkprocess as process_id,
    					   sname as process_name  
    				FROM cm_scope_process pp join view_rm_process_active p on p.fkcontext = pp.fkprocess
    				 WHERE fkscope = {$this->scopeId}"; 
	}

	public function fetchProcess() {
		parent::executeQuery();
		$this->caProcess = array();
		while ($this->coDataSet->fetch()) {
			$this->caProcess[] = $this->coDataSet->getFieldByAlias("process_id")->getValue();
		}
	}

	public function getProcess() {
		return $this->caProcess;
	}
}
?>