<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe QueryScheduledRisks
 *
 * <p>Consulta para obter os riscos com c�lculo autom�tico de probabilidade do
 * usu�rio atual e que ainda n�o foram verificados hoje.</p>
 * @package ISMS
 * @subpackage classes
 */
class QueryScheduledRisks extends FWDDBQueryHandler {

  /**
  * Construtor.
  * 
  * <p>Construtor da classe.</p>
  * @access public 
  */
  public function __construct($poDB){
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField('rs.fkRisk' ,'risk_id'    ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('rs.nPeriod','period_unit',DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('rs.nValue' ,'period_size',DB_NUMBER));
  }
  
  public function makeQuery(){
    $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();    
    $miToday = ISMSLib::ISMSTime();
    $miToday = mktime(0, 0, 0, date("m",$miToday), date("d",$miToday),  date("Y",$miToday));
    $msToday = ISMSLib::getTimestampFormat($miToday);
    $this->csSQL = "SELECT
                      rs.fkRisk as risk_id,
                      rs.nPeriod as period_unit,
                      rs.nValue as period_size
                    FROM
                      view_rm_risk_active r
                      JOIN ci_risk_schedule rs ON (rs.fkRisk = r.fkContext)
                      JOIN rm_asset a ON (a.fkContext = r.fkAsset)
                    WHERE
                      a.fkSecurityResponsible = $miUserId
                      AND (
                        rs.dDateLastCheck < $msToday
                        OR rs.dDateLastCheck IS NULL
                      )";
  }

}

?>