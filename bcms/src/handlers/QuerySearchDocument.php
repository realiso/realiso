<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QuerySearchDocument.
 *
 * <p></p>
 * @package ISMS
 * @subpackage handlers
 */
class QuerySearchDocument extends FWDDBQueryHandler {

  protected $cbSearchContent;
  protected $csSearch;
  protected $ciCreationDateStart = 0;
  protected $ciEditionDateStart = 0;
  protected $ciCreationDateEnd = 0;
  protected $ciEditionDateEnd = 0;

  public function __construct($poDB) {
    parent::__construct($poDB);

    $this->coDataSet->addFWDDBField(new FWDDBField('' ,'context_type'   ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('' ,'context_id'     ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('' ,'context_string' ,DB_STRING));
  }

  public function setSearch($psSearch){
    $this->csSearch = $psSearch;
  }

  public function setSearchContent($pbValue){
    $this->cbSearchContent = $pbValue;
  }

  public function setCreationDateStart($piDate) {
    $this->ciCreationDateStart = $piDate;
  }

  public function setEditionDateStart($piDate) {
    $this->ciEditionDateStart = $piDate;
  }

  public function setCreationDateEnd($piDate) {
    $this->ciCreationDateEnd = $piDate;
  }

  public function setEditionDateEnd($piDate) {
    $this->ciEditionDateEnd = $piDate;
  }

  public function makeQuery(){
    $msSearch = $this->csSearch;
    $moDocument = new PMDocument();
    $moDocInstance = new PMDocInstance($this->cbSearchContent);
    $msInstanceTable = $moDocInstance->getTable();
    $msDBType = FWDWebLib::getConnection()->getDatabaseType();

    $maCase = array();
    $maWhere = array();

    $maSearchableFields = $moDocument->getSearchableFields();
    foreach($maSearchableFields as $msAlias){
      $msField = $moDocument->getFieldName($msAlias);
      $maCase[] = "WHEN ".FWDWebLib::getCaseInsensitiveLike("d.".$msField,$msSearch)." THEN " . (($msDBType == DB_ORACLE) ? "TO_CLOB(d.$msField)" : "d.$msField");
      $maWhere[] = FWDWebLib::getCaseInsensitiveLike("d.".$msField,$msSearch);
    }

    $maSearchableFields = $moDocInstance->getSearchableFields();
    foreach($maSearchableFields as $msAlias){
      $msField = $moDocInstance->getFieldName($msAlias);
      $maCase[] = "WHEN di.$msField LIKE '%$msSearch%' THEN " . (($msDBType == DB_ORACLE) ? "TO_CLOB(di.$msField)" : "di.$msField");
      $maWhere[] = "di.$msField LIKE '%$msSearch%'";
    }

    $maDateFilters = array();
    $maDateFilters[] = "d.fkContext = ch.context_id";
    if($this->ciCreationDateStart){
      $maDateFilters[] = "ch.context_date_created >= " . ISMSLib::getTimestampFormat($this->ciCreationDateStart);
    }
    if($this->ciCreationDateEnd){
      $maDateFilters[] = "ch.context_date_created <= " . ISMSLib::getTimestampFormat($this->ciCreationDateEnd);
    }
    if($this->ciEditionDateStart){
      $maDateFilters[] = "ch.context_date_modified >= " . ISMSLib::getTimestampFormat($this->ciEditionDateStart);
    }
    if($this->ciEditionDateEnd){
      $maDateFilters[] = "ch.context_date_modified <= " . ISMSLib::getTimestampFormat($this->ciEditionDateEnd);
    }
    $msDateFilter = '';
    if(count($maDateFilters) > 1){
      $msDateFilter = " JOIN context_history ch ON (".implode(' AND ',$maDateFilters).")";
    }

    $this->csSQL = "SELECT
                      ".CONTEXT_DOCUMENT." AS context_type,
                      d.fkContext AS context_id,
                      CASE ".implode("\n",$maCase)." END AS context_string
                    FROM
                      pm_document d
                      JOIN isms_context c ON (d.fkContext=c.pkContext)
                      JOIN view_pm_di_active_with_content di ON (d.fkCurrentVersion = di.fkContext)
                      $msDateFilter
                    WHERE (
                        di.nMajorVersion >= 1
                        OR (
                          c.nState = ".CONTEXT_STATE_DOC_APPROVED."
                          AND d.dDateProduction <= '".date('Y-m-d')."'
                        )
                      ) AND (".implode(' OR ',$maWhere).")";
  }
}
?>