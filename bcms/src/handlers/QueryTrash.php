<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryTrash.
 *
 * <p>Consulta para retornar os contextos logicamente deletados.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryTrash extends FWDDBQueryHandler {
  
  protected $caContexts = array();
  
  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField("fkContext", "context_id",   DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("sName",     "context_name", DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("nType",     "context_type", DB_NUMBER));
  }
  
  public function makeQuery() {
    $this->csSQL = "SELECT context_id, context_name, context_type
                    FROM context_names
                    WHERE context_state = ".CONTEXT_STATE_DELETED."
                    ORDER BY context_type ASC, context_name ASC";
 }
  
  public function executeQuery() {
    parent::executeQuery();
    $maUsers = array();
    while ($this->coDataSet->fetch()) {
      $miContextId = $this->coDataSet->getFieldByAlias("context_id")->getValue();
      $miContextName = $this->coDataSet->getFieldByAlias("context_name")->getValue();
      $miContextType = $this->coDataSet->getFieldByAlias("context_type")->getValue();
      //modificada a ordem para garantir q o possivel usu�rio respons�vel pelos contextos
      //do sistema estar�o por ultimo no array retornado, assim garante-se que n�o ir�
      //ocorrer erro de sql na hora de deletar os contextos existentes na lixeira
      if($miContextType == CONTEXT_USER){
        $maUsers[] = array("context_id"=>$miContextId, "context_name"=>$miContextName, "context_type"=>$miContextType);  
      }else{
        $this->caContexts[] = array("context_id"=>$miContextId, "context_name"=>$miContextName, "context_type"=>$miContextType);
      }
    }
    $this->caContexts = array_merge($this->caContexts,$maUsers);
  }
  
  /**
   * Retorna os contextos setados como exlu�dos.
   * 
   * <p>M�todo para retornar os contextos setados como exclu�dos.</p>
   * @access public 
   * @return array Array de contextos
   */ 
  public function getContexts() {
    return $this->caContexts;
  }
}
?>