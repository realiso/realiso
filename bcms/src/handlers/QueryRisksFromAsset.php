<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryRisksFromAsset.
 *
 * <p>Consulta para buscar os riscos associados a um ativo.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryRisksFromAsset extends FWDDBQueryHandler {
  
  protected $ciAssetId = 0;
  
  protected $caRisks = array();
  
  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField('pkRisk','risk_id',DB_NUMBER));
  }
  
  public function setAsset($piAssetId) {
    $this->ciAssetId = $piAssetId;
  }
  
  public function makeQuery() {
    $this->csSQL = "SELECT pkRisk as risk_id from ".FWDWebLib::getFunctionCall("get_risks_from_asset({$this->ciAssetId})");
  }
  
  public function executeQuery() {
    parent::executeQuery();
    while ($this->coDataSet->fetch()) {
      $this->caRisks[] = $this->coDataSet->getFieldByAlias("risk_id")->getValue();
    }
  }
  
  /**
   * Retorna os riscos associados a um ativo.
   * 
   * <p>M�todo para retornar os riscos associados a um ativo.</p>
   * @access public 
   * @return array Array de ids de riscos
   */ 
  public function getRisks() {
    return $this->caRisks;
  }
}
?>