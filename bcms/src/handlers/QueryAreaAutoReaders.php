<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryAreaAutoReaders.
 *
 * <p>Consulta para buscar os leitores do documento de uma �rea.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryAreaAutoReaders extends FWDDBQueryHandler {
  
  protected $ciAreaId = 0;  
  protected $caAutoReaders = array();  
  
  public function __construct($poDB) {
    parent::__construct($poDB);

    $this->coDataSet->addFWDDBField(new FWDDBField('pu.fkUser','reader_id',DB_NUMBER));
  }

  public function setArea($piAreaId) {
    $this->ciAreaId = $piAreaId;
  }
  
  public function makeQuery() {
    $this->csSQL = "SELECT pu.fkUser as reader_id
										FROM view_rm_process_active p
										JOIN view_pm_process_user_active pu ON (p.fkContext = pu.fkProcess)
										WHERE p.fkArea IN (											
											" . FWDWebLib::selectConstant("{$this->ciAreaId} as pkArea") . "
											UNION							
											SELECT pkArea FROM " . FWDWebLib::getFunctionCall("get_sub_areas({$this->ciAreaId})") . " suba JOIN view_rm_area_active a ON (suba.pkArea = a.fkContext)
										)
										GROUP BY pu.fkUser";
  }
  
  public function executeQuery() {
    parent::executeQuery();
    while ($this->coDataSet->fetch()) {
      $this->caAutoReaders[] = $this->coDataSet->getFieldByAlias("reader_id")->getValue();
    }
  }
  
  /**
   * Retorna os leitores autom�ricos.
   * 
   * <p>M�todo para retornar os leitores autom�ticos.</p>
   * @access public 
   * @return array Array de ids de usu�rios
   */ 
  public function getReaders() {
    return $this->caAutoReaders;
  }
}
?>