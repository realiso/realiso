<?php

include_once 'include.php';

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(''));

		// Popula o select de dia do m�s
		$moMonthDaySelect = FWDWebLib::getObject('month_day');
		for($i=1;$i<=31;$i++){
			$moItem = new FWDItem();
			$moItem->setAttrKey($i);
			$moItem->setValue($i);
			$moMonthDaySelect->addObjFWDItem($moItem);
		}
		$moItem = new FWDItem();
		$moItem->setAttrKey(33);
		$moItem->setValue(FWDLanguage::getPHPStringValue('mx_second_last',"pen�ltimo"));
		$moMonthDaySelect->addObjFWDItem($moItem);
		$moItem = new FWDItem();
		$moItem->setAttrKey(32);
		$moItem->setValue(FWDLanguage::getPHPStringValue('mx_last',"�ltimo"));
		$moMonthDaySelect->addObjFWDItem($moItem);
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$moSchedule = new WKFSchedule();
		$msSerializedSchedule = FWDWebLib::getObject('serialized_schedule')->getValue();

		if($msSerializedSchedule){
			$moSchedule->fillFieldsFromArray(FWDWebLib::unserializeString($msSerializedSchedule));
			$miScheduleStart = $moSchedule->getFieldValue('schedule_start');
			FWDWebLib::getObject('schedule_start')->setTimestamp($miScheduleStart);
		}else{
//			$moSchedule->setFieldValue('schedule_start',FWDWebLib::getTime());
			$moSchedule->setFieldValue('schedule_type',SCHEDULE_BYDAY);
//			$moSchedule->setFieldValue('schedule_periodicity',1);
//			$moSchedule->setFieldValue('schedule_days_to_finish',1);
		}

		$miScheduleEnd = $moSchedule->getFieldValue('schedule_end');

		if($miScheduleEnd){
			FWDWebLib::getObject('schedule_finish')->setValue('yes');
			FWDWebLib::getObject('schedule_end')->setTimestamp($miScheduleEnd);
		}else{
			FWDWebLib::getObject('schedule_finish')->setValue('no');
			FWDWebLib::getObject('schedule_end')->setAttrDisabled('true');
		}
		FWDWebLib::getObject('schedule_days_to_finish')->setValue($moSchedule->getFieldValue('schedule_days_to_finish'));

		FWDWebLib::getObject('choose_months')->setValue('no');

		$process = FWDWebLib::getObject('process')->getValue();
		$title = FWDWebLib::getObject('popup_title');
		if($process){
			$title->setAttrValue(FWDLanguage::getPHPStringValue('life_cycle', "Ciclo de Vida"));
		}else{
			$title->setAttrValue(FWDLanguage::getPHPStringValue('seasonality', "Sazonalidade"));
		}


		switch($moSchedule->getFieldValue('schedule_type')){
			case SCHEDULE_BYDAY:{
				FWDWebLib::getObject('schedule_type')->setValue('pack_day');
				FWDWebLib::getObject('schedule_periodicity')->setValue($moSchedule->getFieldValue('schedule_periodicity'));
				break;
			}
			case SCHEDULE_BYWEEK:{
				FWDWebLib::getObject('schedule_type')->setValue('pack_week');
				$maWeekDays = WKFSchedule::bitmapToArray($moSchedule->getFieldValue('schedule_bitmap'));
				FWDWebLib::getObject('week_days')->setValue(implode(':',$maWeekDays));
				FWDWebLib::getObject('schedule_periodicity')->setValue($moSchedule->getFieldValue('schedule_periodicity'));
				break;
			}
			case SCHEDULE_BYMONTH:{
				FWDWebLib::getObject('schedule_type')->setValue('pack_month');
				$maMonths = WKFSchedule::bitmapToArray($moSchedule->getFieldValue('schedule_bitmap'));
				FWDWebLib::getObject('months')->setValue(implode(':',$maMonths));

				$miWeek = $moSchedule->getFieldValue('schedule_week');
				if($miWeek){
					FWDWebLib::getObject('week')->setValue($miWeek);
					FWDWebLib::getObject('week_day')->setValue($moSchedule->getFieldValue('schedule_day'));
					FWDWebLib::getObject('month_day_by')->setValue('pack_week_day');
				}else{
					FWDWebLib::getObject('month_day')->setValue($moSchedule->getFieldValue('schedule_day'));
					FWDWebLib::getObject('month_day_by')->setValue('month_day');
				}

				$miPeriodicity = $moSchedule->getFieldValue('schedule_periodicity');

				if($miPeriodicity){
					FWDWebLib::getObject('schedule_periodicity_month')->setValue($miPeriodicity);
				}else{
					FWDWebLib::getObject('choose_months')->setValue('yes');
				}

				break;
			}
		}

		$maStrings = WKFSchedule::getStrings();
		$maTemp = array();
		foreach($maStrings as $msKey=>$msValue){
			$maTemp[] = "'$msKey':'$msValue'";
		}
		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
<script language="javascript">
        var soSchedule = {
          
          'caStrings': <?='{'.implode(',',$maTemp).'}'?>,
          
          'formatList': function(paArray){
            if(paArray.length>1){
              return paArray.slice(0,paArray.length-1).join(', ')+' '+this.caStrings['and']+' '+paArray[paArray.length-1];
            }else{
              return paArray[0];
            }
          },
          
          'toString': function(){
            var msString;
            
            var msStart = gebi('schedule_start').value;
            var msEnd = (gebi('schedule_finish').value.indexOf('yes')>-1?gebi('schedule_end').value:'');
            if(msEnd){
              msString = this.caStrings['interval'].replace('%start%',msStart).replace('%end%',msEnd);
            }else{
              msString = this.caStrings['endless_interval'].replace('%start%',msStart);
            }
            msString+= ', ';
            
            var msScheduleType = gebi('schedule_type').value;
            var miPeriodicity;
            if(msScheduleType=='pack_day'){
              miPeriodicity = gebi('schedule_periodicity').value;
              if(miPeriodicity==1){
                msString+= this.caStrings['daily'];
              }else{
                msString+= this.caStrings['each_n_days'].replace('%periodicity%',miPeriodicity);
              }
            }else if(msScheduleType=='pack_week'){
              miPeriodicity = gebi('schedule_periodicity').value;
              var maWeekDays = gebi('week_days').value.split(':').sort();
              var i;
              for(i=0;i<maWeekDays.length;i++){
                if(maWeekDays[i]!=''){
                  maWeekDays.splice(0,i);
                  break;
                }
              }
              for(i=0;i<maWeekDays.length;i++){
                maWeekDays[i] = this.caStrings['day_'+maWeekDays[i]+'_pl'];
              }
              if(miPeriodicity==1){
                msString+= this.caStrings['weekly'];
              }else{
                msString+= this.caStrings['each_n_weeks'].replace('%periodicity%',miPeriodicity);
              }
              msString = msString.replace('%week_days%',this.formatList(maWeekDays));
            }else if(msScheduleType=='pack_month'){
              miPeriodicity = gebi('schedule_periodicity_month').value;
              var maMonths = gebi('months').value.split(':').sort(function(a,b){return a*1-b*1;});
              var i;
              for(i=0;i<maMonths.length;i++){
                if(maMonths[i]!=''){
                  maMonths.splice(0,i);
                  break;
                }
              }
              for(i=0;i<maMonths.length;i++){
                maMonths[i] = this.caStrings['month_'+maMonths[i]];
              }
              
              var mbSpecifyMonths = (gebi('choose_months').value.indexOf('yes')>-1);
              
              if((!mbSpecifyMonths && miPeriodicity==1) || (mbSpecifyMonths && maMonths.length==12)){
                msString+= this.caStrings['monthly'];
              }else if(mbSpecifyMonths){
                msString+= this.caStrings['specified_months'].replace('%months%',this.formatList(maMonths));
              }else{
                msString+= this.caStrings['each_n_months'].replace('%periodicity%',miPeriodicity);
              }
              var msDayExpression;
              if(gebi('month_day_by').value=='month_day'){
                var moMonthDay = gobi('month_day');
                var miMonthDay = moMonthDay.getValue();
                if(miMonthDay>31){
                  msDayExpression = this.caStrings['day_by_month_inverse'].replace('%day%',moMonthDay.getItemText(miMonthDay));
                }else{
                  msDayExpression = this.caStrings['day_by_month'].replace('%day%',miMonthDay);
                }
              }else{
                msDayExpression = this.caStrings['day_by_week'];
                msDayExpression = msDayExpression.replace('%week%',gobi('week').getItemText(gobi('week').getValue()));
                msDayExpression = msDayExpression.replace('%week_day%',gobi('week_day').getItemText(gobi('week_day').getValue()));
              }
              msString = msString.replace('%day_expression%',msDayExpression);
            }
            
            return msString+'.';
          },
          
          'updateText': function(){
            var msElementsId = '<?=str_replace(':schedule_days_to_finish','',FWDWebLib::getObject('required_check')->getAttrElements())?>';
            var msEventsOk = "gobi('schedule_text').setValue(soSchedule);";
            var msEventsNotOk = "gobi('schedule_text').setValue('');";
            js_required_check_client(msElementsId,msEventsOk,msEventsNotOk);
          },
          
          'enqueueUpdateText': function(){
            setTimeout(function(){soSchedule.updateText()},1);
          }
          
        };
        
        function save(){
          var moValues = {};
          var miType;
          var miPeriodicity = 0;
          var miBitmap = 0;
          var miWeek = 0;
          var miDay = 0;
          
          moValues['schedule_id'            ] = js_get_get_parameter('id');
          moValues['schedule_start'         ] = gebi('schedule_start').value;
          moValues['schedule_days_to_finish'] = gebi('schedule_days_to_finish').value;
          
          if(gebi('schedule_finish').value.indexOf('yes')>-1){
            moValues['schedule_end'] = gebi('schedule_end').value;
          }else{
            moValues['schedule_end'] = '';
          }
          
          switch(gebi('schedule_type').value){
            case 'pack_day':{
              miType = <?=SCHEDULE_BYDAY?>;
              miPeriodicity = gebi('schedule_periodicity').value;
              break;
            }
            case 'pack_week':{
              miType = <?=SCHEDULE_BYWEEK?>;
              miPeriodicity = gebi('schedule_periodicity').value;
              var miMask = 1;
              for(var i=1;i<=7;i++){
                if(gobi('week_days_'+i).isChecked()) miBitmap|= miMask;
                miMask<<= 1;
              }
              break;
            }
            case 'pack_month':{
              miType = <?=SCHEDULE_BYMONTH?>;
              if(gebi('choose_months').value.indexOf('yes')>-1){
                var miMask = 1;
                for(var i=1;i<=12;i++){
                  if(gobi('months_'+i).isChecked()) miBitmap|= miMask;
                  miMask<<= 1;
                }
              }else{
                miPeriodicity = gebi('schedule_periodicity_month').value;
              }
              if(gobi('month_day_by').getValue()=='month_day'){
                miDay = gobi('month_day').getValue();
              }else{
                miWeek = gobi('week').getValue();
                miDay = gobi('week_day').getValue();
              }
              break;
            }
          }
          
          moValues['schedule_type'       ] = miType;
          moValues['schedule_periodicity'] = miPeriodicity;
          moValues['schedule_bitmap'     ] = miBitmap;
          moValues['schedule_week'       ] = miWeek;
          moValues['schedule_day'        ] = miDay;
          
          self.getOpener().set_schedule(soUnSerializer.serialize(moValues));
        }
        
        if(gebi('choose_months').value.indexOf('yes')==-1){
          gobi('months').disable();
        }
        soSchedule.updateText();
      </script>
		<?
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_schedule_edit.xml');

?>
