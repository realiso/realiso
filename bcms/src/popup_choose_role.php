<?php
include_once "include.php";
include_once $handlers_ref . "select/QuerySelectSolicitor.php";

class OKEvent extends FWDRunnable {
  public function run(){
  	// pega sess�o
  	$moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
  	
  	// pega usu�rio que vai ser o procurador
  	$miSolicitorId = $moSession->getUserId();
  	 
  	// "desloga"
  	$moSession->logout();
  	
  	// cria nova sess�o
  	$moSession = FWDWebLib::getInstance()->getSession(new ISMSSession(ISMSLib::getCurrentSessionId()));
  	
  	// seta o procurador na sess�o
  	$moSession->setSolicitor($miSolicitorId);
  	
  	// loga
  	$miSelectedUser = FWDWebLib::getObject('select_users')->getValue();
  	$moUser = new ISMSUser();
  	$moUser->fetchById($miSelectedUser);
  	if ($moSession->login($moUser->getFieldValue('user_login'),'',false)) {
      if(ISMSLib::getGestorPermission()) {
         echo "parent.document.location.href = 'tab_main.php';";
      }
      else {
         echo "parent.document.location.href = './packages/admin/tab_main.php';";
      }
    }
    else {
      $msTitle = FWDLanguage::getPHPStringValue('tt_login_error','Erro ao Logar no Sistema');
      $msMessage = FWDLanguage::getPHPStringValue('mx_user_doesnt_have_permission_to_login',"O usu�rio <b>%user_name%</b> n�o possui permiss�o para acessar o sistema.");
      $msMessage = str_replace('%user_name%', $moUser->getFieldValue('user_name'), $msMessage);
      ISMSLib::openOk($msTitle,$msMessage,"",40);
    }
    echo "soPopUpManager.closePopUp('popup_choose_role');";
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new OKEvent('ok_event'));    		    
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){  	
  	$moHandler = new QuerySelectSolicitor(FWDWebLib::getConnection());  	
  	$moHandler->setSolicitor(FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId());    
    $moSelect = FWDWebLib::getObject('select_users');
    $moSelect->setQueryHandler($moHandler);
    $moSelect->populate();
      	
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));    
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_choose_role.xml");
?>