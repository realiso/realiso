<?php
include_once "include.php";
include_once $handlers_ref . "grid/QueryGridUserTasks.php";
include_once $handlers_ref . "grid/QueryGridUserAlerts.php";

function getArrayGrids(){
	$moConfig = new ISMSConfig();
	$mbHasRMModule = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(RISK_MANAGEMENT_MODE);
	$mbHasPMModule = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(POLICY_MODE);
	$mbHasCIModule = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(INCIDENT_MODE);

	$maGrids = array();
	if ($mbHasRMModule) {
		if($moConfig->getConfig(GENERAL_COST_ENABLED)){
//			$maGrids['grid_financial_summary'           ] = array('label'=>FWDLanguage::getPHPStringValue('tt_financial_summary'            ,'Sum�rio Financeiro'));
		}
		$maGrids['grid_resume_summary'       		] = array('label'=>FWDLanguage::getPHPStringValue('quantity'        					,'Quantidades'));
		$maGrids['grid_top10qualitative'       		] = array('label'=>FWDLanguage::getPHPStringValue('top10qualitative'        			,'Top 10 Qualitativo'));
		$maGrids['grid_top10qualitativeFinancial'   ] = array('label'=>FWDLanguage::getPHPStringValue('top10qualitativeFinancial'        	,'Top 10 Qualitativo Financeiro'));
		$maGrids['grid_top10asa'   					] = array('label'=>FWDLanguage::getPHPStringValue('top10asa'        					,'Top 10 ASA'));
		$maGrids['grid_dashboardScope'				] = array('label'=>FWDLanguage::getPHPStringValue('dashboardScope'        				,'Escopos'));
		$maGrids['grid_dashboardCurrentTests'		] = array('label'=>FWDLanguage::getPHPStringValue('dashboardCurrentTests'        		,'Testes atuais'));
//		$maGrids['grid_risk_summary'                ] = array('label'=>FWDLanguage::getPHPStringValue('tt_general_summary_of_risks'         ,'Risco Estimado vs. N�o-Estimado'));
//		$maGrids['grid_risk_summary2'               ] = array('label'=>FWDLanguage::getPHPStringValue('tt_summary_of_parameterized_risks'   ,'Riscos Potenciais vs. Riscos Residuais'));
//		$maGrids['grid_residual_risk_summary'       ] = array('label'=>FWDLanguage::getPHPStringValue('tt_risk_management_status'        	,'N�vel de Risco'));
//		$maGrids['grid_isms_reports_and_documents'  ] = array('label'=>FWDLanguage::getPHPStringValue('tt_sgsi_reports_and_documents'   	,'Documentos obrigat�rios do SGSI pela ISO 27001'));
//		$maGrids['grid_best_practice_summary'       ] = array('label'=>FWDLanguage::getPHPStringValue('tt_summary_of_best_practices'        ,'Melhores Pr�ticas - �ndice de Compliance'));
//		$maGrids['grid_control_summary'             ] = array('label'=>FWDLanguage::getPHPStringValue('tt_summary_of_controls'              ,'Sum�rio de Controles'));
//		$maGrids['grid_abnormalitys_summary'        ] = array('label'=>FWDLanguage::getPHPStringValue('tt_summary_of_abnormalities'         ,'Sum�rio de Anormalidades da Gest�o dos Riscos'));
	}
	if($mbHasPMModule){
//		$maGrids['grid_doc_sumary'                  ] = array('label'=>FWDLanguage::getPHPStringValue('tt_summary_of_policy_management'                   ,'Sum�rio de Gest�o de Pol�ticas'));
//		$maGrids['grid_doc_context_with_documents'  ] = array('label'=>FWDLanguage::getPHPStringValue('tt_risk_management_x_policy_management'   ,'Documentos por Elemento de Neg�cio'));
//		$maGrids['grid_doc_approver_time'           ] = array('label'=>FWDLanguage::getPHPStringValue('tt_average_time_for_documents_approval'            ,'Tempo M�dio de Aprova��o dos Documentos'));
//		$maGrids['grid_doc_top10'                   ] = array('label'=>FWDLanguage::getPHPStringValue('tt_top_10_most_read_documents'                    ,'Top 10 Documentos mais lidos'));
//		$maGrids['grid_doc_period_revision'         ] = array('label'=>FWDLanguage::getPHPStringValue('tt_summary_of_my_documents_last_revision_average_time'          ,'Sum�rio do per�odo de tempo da �ltima revis�o dos documentos'));
//		$maGrids['grid_abnormality_pm'              ] = array('label'=>FWDLanguage::getPHPStringValue('tt_grid_abnormality_pm_title'          ,'Sum�rio de Anormalidades da Gest�o das Pol�ticas'));
	}

	if($mbHasCIModule){
//		$maGrids['grid_incident_summary'] = array('label'=>FWDLanguage::getPHPStringValue('tt_grid_incident_summary_title', 'Sum�rio de Incidentes'));
//		$maGrids['grid_nc_summary'] = array('label'=>FWDLanguage::getPHPStringValue('tt_grid_nc_summary_title', 'Sum�rio de N�o Conformidades'));
//		$maGrids['grid_incidents_per_user'] = array('label'=>FWDLanguage::getPHPStringValue('tt_grid_incidents_per_user_title', 'Top 5 Usu�rios com mais Processos Disciplinares'));
//		$maGrids['grid_incidents_per_asset'] = array('label'=>FWDLanguage::getPHPStringValue('tt_grid_incidents_per_asset_title', 'Top 5 Ativos com mais Incidentes'));
//		$maGrids['grid_incidents_per_process'] = array('label'=>FWDLanguage::getPHPStringValue('tt_grid_incidents_per_process_title', 'Top 5 Processos com mais Incidentes'));
//		$maGrids['grid_nc_per_process'] = array('label'=>FWDLanguage::getPHPStringValue('tt_grid_nc_per_process_title', 'Top 5 Processos com mais N�o Conformidades'));
//		$maGrids['grid_abnormality_ci'] = array('label'=>FWDLanguage::getPHPStringValue('tt_grid_abnormality_ci_title', 'Sum�rio de Anormalidades da Melhoria Cont�nua'));
	}

	return $maGrids;
}
function getArrayGridsSimple(){
	$moConfig = new ISMSConfig();
	$mbHasRMModule = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(RISK_MANAGEMENT_MODE);
	$mbHasPMModule = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(POLICY_MODE);
	$maDeniedACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
	if($mbHasRMModule){
		if(in_array('M.RM', $maDeniedACLs)){
			$mbHasRMModule = false;
		}
	}
	if($mbHasPMModule){
		if(in_array('M.PM', $maDeniedACLs)){
			$mbHasPMModule = false;
		}
	}
	$maGrids = array();
	if ($mbHasRMModule) {
		$maGrids['grid_my_residual_risk'  ] = array('label'=>FWDLanguage::getPHPStringValue('tt_my_risk_summary' ,'Meu Sum�rio de Risco'));
		$maGrids['grid_my_pendencies'     ] = array('label'=>FWDLanguage::getPHPStringValue('tt_my_pendencies'    ,'Minhas Pend�ncias da Gest�o dos Riscos'));
		$maGrids['grid_my_elements'       ] = array('label'=>FWDLanguage::getPHPStringValue('tt_my_items'      ,'Meus Elementos'));
	}
	if($mbHasPMModule){
		$maGrids['grid_doc_sumary'                  ] = array('label'=>FWDLanguage::getPHPStringValue('tt_summary_of_policy_management_my_documents'                 ,'Sum�rio de Gest�o das Pol�ticas (Meus Documentos)'));
		$maGrids['grid_doc_context_with_documents'  ] = array('label'=>FWDLanguage::getPHPStringValue('tt_risk_management_my_items_x_policy_management','Gest�o dos Riscos (Meus Elementos) x Gest�o das Pol�ticas'));
		$maGrids['grid_doc_approver_time'           ] = array('label'=>FWDLanguage::getPHPStringValue('tt_average_time_for_my_documents_approval'          ,'Tempo M�dio de Aprova��o dos Meus Documentos'));
		$maGrids['grid_doc_top10'                   ] = array('label'=>FWDLanguage::getPHPStringValue('tt_top_10_most_read_my_documents'                  ,'Top 10 Meus Documentos Mais Lidos'));
		$maGrids['grid_doc_period_revision'         ] = array('label'=>FWDLanguage::getPHPStringValue('tt_summary_of_my_documents_last_revision_average_time'        ,'Sum�rio do per�odo de tempo da �ltima revis�o dos documentos'));
	}
	return $maGrids;
}
class SaveEvent extends FWDRunnable {
	public function run(){
		$maValuesSelect = explode(':',FWDWebLib::getObject('dashboard_pref_grids_values')->getValue());
		if(FWDWebLib::getObject('dashboard_type')->getValue()){
			$maGridInfo = getArrayGridsSimple();
		}else{
			$maGridInfo = getArrayGrids();
		}

		$maPreference = array();
		foreach($maValuesSelect as $msValue){
			if(isset($maGridInfo[$msValue])){
				$maPreference[] = $msValue;
			}
		}
		if(count($maPreference)==0){
			$maPreference[] = "grid_dummy";
		}
		$moPreference = new ISMSUserPreferences();
		if(FWDWebLib::getObject('dashboard_type')->getValue())
		$moPreference->setPreference("dashboard_sumary_pref_basic",serialize($maPreference));
		else
		$moPreference->setPreference("dashboard_sumary_pref",serialize($maPreference));

		echo "soWindow = soPopUpManager.getPopUpById('popup_user_dashboard_preferences').getOpener();"
		."if(soWindow.refresh) soWindow.refresh();"
		."soPopUpManager.closePopUp('popup_user_dashboard_preferences');";
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));
		$moStartEvent->addAjaxEvent(new SaveEvent("save_event"));
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		/*setup da tela*/
		$moSelAll = FWDWebLib::getObject('user_dashboard_sel_all');
		$moSelSelected = FWDWebLib::getObject('user_dashboard_sel_selected');
		$maPrefUserKeys = array();
		$maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
		$mbSimple = false;
		if(in_array('M.D.2',$maACLs)){
			$mbSimple = true;
		}

		FWDWebLib::getObject('dashboard_type')->setValue($mbSimple);
		$moPreference = new ISMSUserPreferences();
		if(FWDWebLib::getObject('dashboard_type')->getValue()){
			if($moPreference->preferenceUserExistes("dashboard_sumary_pref_basic")){
				$maPrefUser = FWDWebLib::unserializeString($moPreference->getPreference("dashboard_sumary_pref_basic"));
				if(is_array($maPrefUser)){
					$maPrefUserKeys = array_flip($maPrefUser);
				}
			}else{
				$maPrefUserKeys = getArrayGridsSimple();
			}
			$maGridInfo = getArrayGridsSimple();
		}else{
			if($moPreference->preferenceUserExistes("dashboard_sumary_pref")){
				$maPrefUser = FWDWebLib::unserializeString($moPreference->getPreference("dashboard_sumary_pref"));
				if(is_array($maPrefUser)){
					$maPrefUserKeys = array_flip($maPrefUser);
				}
			}else{
				$maPrefUserKeys = getArrayGrids();
			}
			$maGridInfo = getArrayGrids();
		}
		/*constroi os objetos da tela*/
		foreach($maGridInfo as $miKey=>$maValues){
			$moItem = new FWDItem();
			$moItem->setAttrKey($miKey);
			$moItem->setValue($maValues['label']);
			if(!isset($maPrefUserKeys[$miKey])){
				$moSelAll->addObjFWDItem($moItem);
			}
		}
		foreach($maPrefUserKeys as $miKey=>$maValues){
			if(isset($maGridInfo[$miKey])){
				$moItem = new FWDItem();
				$moItem->setAttrKey($miKey);
				$moItem->setValue($maGridInfo[$miKey]['label']);
				$moSelSelected->addObjFWDItem($moItem);
			}
		}

		if((in_array('M.D.1',$maACLs))&&(in_array('M.D.2',$maACLs))){
			trigger_error("You do not have permission to change dashboard configuration!",E_USER_ERROR);
		}
		//instala a seguran�a de ACL na p�gina
		FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
<script language="javascript">
        var soListEditor  = new FWDListEditor('user_dashboard_sel');
        
      </script>
		<?

	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_user_dashboard_preference.xml");
?>