<?php
include_once "include.php";
include_once $handlers_ref . "QueryContextHierarchicalStructure.php";
include_once $handlers_ref . "select/QuerySelectProcess.php";
include_once $handlers_ref . "select/QuerySelectAsset.php";
include_once $handlers_ref . "select/QuerySelectControl.php";
include_once $handlers_ref . "select/QuerySelectStandard.php";

/*
 * Evento respons�vel por fazer o download do gr�fico para uma imagem no formato 'png'.
 */
class DownloadGraphicEvent extends FWDRunnable {
  public function run(){
		$msFilename = "graph".date("YmdHis");
    header('Cache-Control: ');
    header('Pragma: ');
    header('Content-type: application/octet-stream');
    header('Content-Disposition: attachment; filename="'.$msFilename.'.png"');
    echo array_key_exists(FWDWebLib::getObject('graphic')->getChartName(),$_SESSION)?$_SESSION[FWDWebLib::getObject('graphic')->getChartName()]:"";
  }
}

/*
 * Fun��o que retorna um array associativo do tipo:
 * array('categoria_do_grafico' => array('nome_da_categoria' =>
 * 					=> array(
 * 										'grafico1' => array(nome, acl),
 * 										'grafico2' => array(nome, acl),
 * 										...
 * 							)
 * 			)
 * );
 * Esse array � utilizado para mapear quais gr�ficos pertencem a quais categorias
 * e para montar o select com os gr�ficos organizados de acordo com sua categoria.
 */
function getGraphCategoryArray() {
	return array(
		'area_graphs' => array(
				'graph_type_name' => FWDLanguage::getPHPStringValue('si_area_statistics',"Estat�sticas de �rea"),
				'graphs' =>	array(
					'areas_total' => FWDLanguage::getPHPStringValue('mx_area_amount',"Quantidade de �reas"),
					'area_value' => FWDLanguage::getPHPStringValue('mx_area_values',"Valores da �rea"),
					'process_amount_by_area' => FWDLanguage::getPHPStringValue('mx_area_processes_amount',"Quantidade de Processos da �rea"),
					'asset_amount_by_area' => FWDLanguage::getPHPStringValue('mx_area_assets_amount',"Quantidade de Ativos da �rea"),
					'risk_amount_by_area' => FWDLanguage::getPHPStringValue('mx_area_risks_amount',"Quantidade de Riscos da �rea"),
					'control_amount_by_area' => FWDLanguage::getPHPStringValue('mx_area_controls_amount',"Quantidade de Controles da �rea")
				),
				'acl' => 'M.D.6.1'
		),
		'process_graphs' => array(
				'graph_type_name' => FWDLanguage::getPHPStringValue('si_process_statistics',"Estat�sticas de Processo"),
				'graphs' =>	array(
					'process_total' => FWDLanguage::getPHPStringValue('mx_processes_amount',"Quantidade de Processos"),
					'process_value' => FWDLanguage::getPHPStringValue('mx_process_values',"Valores do Processo"),
					'asset_amount_by_process' => FWDLanguage::getPHPStringValue('mx_process_assets_amount',"Quantidade de Ativos do Processo"),
					'risk_amount_by_process' => FWDLanguage::getPHPStringValue('mx_process_risks_amount',"Quantidade de Riscos do Processo"),
					'control_amount_by_process' => FWDLanguage::getPHPStringValue('mx_process_controls_amount',"Quantidade de Controles do Processo")
				),
				'acl' => 'M.D.6.2'
		),
		'asset_graphs' => array(
				'graph_type_name' => FWDLanguage::getPHPStringValue('si_asset_statistics',"Estat�sticas de Ativo"),
				'graphs' => array(
					'asset_total' => FWDLanguage::getPHPStringValue('mx_assets_amount',"Quantidade de Ativos"),
					'asset_value' => FWDLanguage::getPHPStringValue('mx_asset_values',"Valores do Ativo"),
					'process_amount_by_asset' => FWDLanguage::getPHPStringValue('mx_asset_processes_amount',"Quantidade de Processos do Ativo"),
					'risk_amount_by_asset' => FWDLanguage::getPHPStringValue('mx_asset_risks_amount',"Quantidade de Riscos do Ativo"),
					'control_amount_by_asset' => FWDLanguage::getPHPStringValue('mx_asset_controls_amount',"Quantidade de Controles do Ativo")
					),
				'acl' => 'M.D.6.3'
		),
		'risk_summary' => array(
				'graph_type_name' => FWDLanguage::getPHPStringValue('si_risk_summary',"Sum�rio de Risco"),
				'graphs' =>	array(
					'risk_total' => FWDLanguage::getPHPStringValue('mx_risks_amount',"Quantidade de Riscos"),
					'risk_treatment' => FWDLanguage::getPHPStringValue('mx_risks_treatment',"Tratamento dos Riscos"),
					'risk_summary' => FWDLanguage::getPHPStringValue('si_risks_summary',"Sum�rio de Riscos"),
					'areas_risk_summary' => FWDLanguage::getPHPStringValue('mx_areas_risk_summary',"Sum�rio de Risco das �reas"),
					'processes_risk_summary' => FWDLanguage::getPHPStringValue('mx_processes_risk_summary',"Sum�rio de Risco dos Processos"),
					'assets_risk_summary' => FWDLanguage::getPHPStringValue('mx_assets_risk_summary',"Sum�rio de Risco dos Ativos"),
				),
				'acl' => 'M.D.6.4'
		),
		'best_practice_summary' => array(
				'graph_type_name' => FWDLanguage::getPHPStringValue('si_best_practice_summary',"Sum�rio de Melhor Pr�tica"),
				'graphs' => array(
					'bp_amount_by_standard' => FWDLanguage::getPHPStringValue('mx_standard_best_practice_amount',"Quantidade de Melhores Pr�ticas da Norma")
				),
				'acl' => 'M.D.6.5'
		),
		'control_summary' => array(
				'graph_type_name' => FWDLanguage::getPHPStringValue('si_control_summary',"Sum�rio de Controle"),
				'graphs' =>	array(
					'control_total' => FWDLanguage::getPHPStringValue('mx_controls_amount',"Quantidade de Controles"),
					'control_implantation' => FWDLanguage::getPHPStringValue('mx_controls_implementation',"Implanta��o dos Controles"),
					'control_adequacy' => FWDLanguage::getPHPStringValue('mx_controls_adequacy',"Adequa��o dos Controles"),
					'control_test' => FWDLanguage::getPHPStringValue('mx_controls_test',"Teste dos Controles"),
					'control_revision' => FWDLanguage::getPHPStringValue('mx_controls_revision',"Revis�o dos Controles"),
					'not_measured_implemented_controls' => FWDLanguage::getPHPStringValue('mx_not_measured_implemented_controls',"Controles Implementados n�o Medidos"),
					'risk_amount_by_control' => FWDLanguage::getPHPStringValue('mx_control_risks_amount',"Quantidade de Riscos do Controle")
				),
				'acl' => 'M.D.6.6'
		),
		'abnormalitys_graphs' => array(
				'graph_type_name' => FWDLanguage::getPHPStringValue('si_abnormalities',"Anormalidades"),
				'graphs' =>	array(
					'areas_without_processes' => FWDLanguage::getPHPStringValue('mx_areas_without_processes',"�reas sem Processos"),
					'processes_without_assets' => FWDLanguage::getPHPStringValue('mx_processes_without_assets',"Processos sem Ativos"),
					'assets_without_risks' => FWDLanguage::getPHPStringValue('mx_assets_without_risk_events',"Ativos sem Eventos de Risco"),
					'controls_without_risks' => FWDLanguage::getPHPStringValue('mx_controls_without_associated_risks',"Controles sem Riscos Associados"),
					'risks_with_no_value_risk' => FWDLanguage::getPHPStringValue('mx_non_parameterized_risks',"Riscos n�o Estimados")
				),
				'acl' => 'M.D.6.7'
		),
		'financial_graphs' => array(
				'graph_type_name' => FWDLanguage::getPHPStringValue('si_financial_statistics',"Estat�sticas Financeiras"),
				'graphs' => array(
					'implemented_controls_investment' => FWDLanguage::getPHPStringValue('mx_implemented_controls_investment',"Investimento dos Controles Implementados"),
					'risk_treatment_potential_impact' => FWDLanguage::getPHPStringValue('mx_risk_treatment_potential_impact',"Impacto Potencial do Tratamento do Risco"),
					'risk_potential_impact' => FWDLanguage::getPHPStringValue('mx_risks_potential_impact',"Impacto Potencial dos Riscos"),
					'control_cost_by_area' => FWDLanguage::getPHPStringValue('mx_area_controls_cost',"Custo dos Controles da �rea"),
					'control_cost_by_process' => FWDLanguage::getPHPStringValue('mx_process_controls_cost',"Custo dos Controles do Processo"),
					'control_cost_by_asset' => FWDLanguage::getPHPStringValue('mx_asset_controls_cost',"Custo dos Controles do Ativo"),
					'total_area_impact' => FWDLanguage::getPHPStringValue('mx_area_total_impact',"Total de Impacto da �rea"),
					'total_process_impact' => FWDLanguage::getPHPStringValue('mx_process_total_impact',"Total de Impacto do Processo"),
					'total_asset_impact' => FWDLanguage::getPHPStringValue('mx_asset_total_impact',"Total de Impacto do Ativo"),
					'asset_cost_by_area' => FWDLanguage::getPHPStringValue('mx_area_assets_cost',"Custo dos Ativos da �rea"),
					'asset_cost_by_process' => FWDLanguage::getPHPStringValue('mx_process_assets_cost',"Custo dos Ativos do Processo"),
					'asset_total_cost' => FWDLanguage::getPHPStringValue('mx_assets_total_cost',"Custo Total dos Ativos")
				),
				'acl' => 'M.D.6.8'
		),
		'abrangency_and_management_level' => array(
				'graph_type_name' => FWDLanguage::getPHPStringValue('si_management_level_and_range',"Abrang�ncia e N�vel de Gerenciamento"),
				'graphs' =>	array(
					'business_areas' => FWDLanguage::getPHPStringValue('mx_business_areas',"�reas de Neg�cios"),
					'business_processes' => FWDLanguage::getPHPStringValue('mx_business_processes',"Processos de Neg�cios"),
					'assets' => FWDLanguage::getPHPStringValue('mx_assets',"Ativos")
				),
				'acl' => 'M.D.6.9'
		),
    'policy_management' => array(
      'graph_type_name' => FWDLanguage::getPHPStringValue('si_policy_management',"Estat�sticas da Gest�o de Pol�ticas"),
      'graphs' => array(
        'document_total' => FWDLanguage::getPHPStringValue('mx_document_total',"Quantidade de Documentos"),
        'documents_by_state' => FWDLanguage::getPHPStringValue('mx_documents_by_state',"Quantidade de Documentos por Status"),
        'documents_by_type' => FWDLanguage::getPHPStringValue('mx_documents_by_type',"Quantidade de Documentos por Tipo"),
        'read_documents' => FWDLanguage::getPHPStringValue('mx_read_documents_proportion',"Propor��o de Documentos Lidos"),
        'unread_documents' => FWDLanguage::getPHPStringValue('mx_unread_documents_proportion',"Propor��o de Documentos N�o Lidos"),
        'scheduled_documents' => FWDLanguage::getPHPStringValue('mx_scheduled_documents',"Propor��o de Documentos que Possuem Revis�o Agendada"),
        'register_total' => FWDLanguage::getPHPStringValue('mx_register_total',"Quantidade de Registros"),
        'occupation_documents' => FWDLanguage::getPHPStringValue('mx_occupation_documents',"Ocupa��o dos Arquivos de Documentos (em MB)"),
        'occupation_template' => FWDLanguage::getPHPStringValue('mx_occupation_template',"Ocupa��o dos Arquivos de Modelo (em MB)"),
        'occupation_registers' => FWDLanguage::getPHPStringValue('mx_occupation_registers',"Ocupa��o dos Arquivos de Registro (em MB)"),
        'occupation_total' => FWDLanguage::getPHPStringValue('mx_occupation_total',"Ocupa��o Total (em MB)"),
      ),
      'acl' => 'M.D.6.11'
    ),
    'continual_improvement' => array(
      'graph_type_name' => FWDLanguage::getPHPStringValue('si_continual_improvement',"Estat�sticas da Melhoria Cont�nua"),
      'graphs' => array(
        'incident_total' => FWDLanguage::getPHPStringValue('mx_incident_total',"Quantidade de Incidentes"),
        'incidents_by_state' => FWDLanguage::getPHPStringValue('mx_incidents_by_state',"Quantidade de Incidentes por Estado"),
        'non_conformity_total' => FWDLanguage::getPHPStringValue('mx_non_conformity_total',"Quantidade de N�o Conformidades"),
        'non_conformity_by_state' => FWDLanguage::getPHPStringValue('mx_non_conformity_by_state',"Quantidade de N�o Conformidades por Estado"),
      ),
      'acl' => 'M.D.6.12'
    ),

    'incident_graphs' => array(
      'graph_type_name' => FWDLanguage::getPHPStringValue('si_action_plan_statistics',"Estat�sticas do Plano de A��o"),
      'graphs' => array(
        'ap_total' => FWDLanguage::getPHPStringValue('mx_action_plan_total',"Quantidade de Planos de A��o"),
        'ap_finished' => FWDLanguage::getPHPStringValue('mx_action_plan_finished_on_time',"Quantidade de Planos de A��o finalizados na data"),
        'ap_finished_late' => FWDLanguage::getPHPStringValue('mx_action_plan_finished_late',"Quantidade de Planos de A��o finalizados com atraso"),
        'ap_efficient_and_not' => FWDLanguage::getPHPStringValue('mx_action_plan_efficient_and_not',"Quantidade de Planos de A��o eficientes e n�o eficientes"),
        'nc_per_ap_average' => FWDLanguage::getPHPStringValue('mx_nc_per_ap_average',"Quantidade m�dia de N�o Conformidades por Plano de A��o")
      ),
      'acl' => 'M.D.6.13'
    ),
  );
}

/*
 * Fun��o utilizada para retornar o nome do gr�fico de acordo com seu identificador.
 */
function getGraphName($psName) {
	$maGraphCategory = getGraphCategoryArray();
	foreach ($maGraphCategory as $maGraphs) {
		foreach ($maGraphs['graphs'] as $msGraphKey => $msGraph)
			if ($msGraphKey == $psName) return $msGraph;
	}
	return "";
}

/*
 * Fun��o utilizada para construir o objeto respons�vel por desenhar o gr�fico.
 */
function getChart($psGraphicId) {
	$moGraph = null;									//	vari�vel que vai conter o objeto do gr�fico
	$maTypes = array();								//	array contendo os tipos de estat�sticas que ser�o exibidas no gr�fico
	$msName = $psGraphicId;						//	nome do gr�fico
	$msAdditionalName = '';						//	nome que ser� adicionado no final do nome do gr�fico (utilizado em gr�ficos de elementos espec�ficos)

	switch ($psGraphicId) {
		/*
		 * Gr�ficos gerais que n�o possuem filtros.
		 */
		case 'areas_total':
		case 'process_total':
		case 'asset_total':
		case 'risk_total':
		case 'control_total':
		case 'areas_without_processes':
		case 'processes_without_assets':
		case 'assets_without_risks':
		case 'controls_without_risks':
		case 'risks_with_no_value_risk':
		case 'not_measured_implemented_controls':
		case 'asset_total_cost':
    case 'incident_total':
    case 'non_conformity_total':
    case 'document_total':
    case 'register_total':
    case 'occupation_documents':
    case 'occupation_template':
    case 'occupation_registers':
    case 'occupation_total':
    case 'ap_total':
    case 'ap_finished':
    case 'ap_finished_late':
    case 'nc_per_ap_average':

			$moGraph = new ISMSGraphSingleStatistic();
			$maTypes[] = $psGraphicId;
			$moGraph->setTypes($maTypes);
		break;
		/*
		 * Gr�ficos espec�ficos de �rea.
		 */
		case 'area_value':
		case 'asset_cost_by_area':
		case 'control_cost_by_area':
		case 'total_area_impact':
		case 'process_amount_by_area':
		case 'asset_amount_by_area':
		case 'risk_amount_by_area':
		case 'control_amount_by_area':
			$moGraph = new ISMSGraphSingleStatistic();
			$miAreaId = FWDWebLib::getObject('select_area')->getValue();
			$moArea = new RMArea();
			$moArea->fetchById($miAreaId);
			$msAdditionalName = $moArea->getFieldValue('area_name');
			$maTypes[] = $psGraphicId;
			$moGraph->setTypes($maTypes);
			$moGraph->setContext($miAreaId);
		break;
		/*
		 * Gr�ficos espec�ficos de processo.
		 */
		case 'process_value':
		case 'asset_cost_by_process':
		case 'control_cost_by_process':
		case 'total_process_impact':
		case 'asset_amount_by_process':
		case 'risk_amount_by_process':
		case 'control_amount_by_process':
			$moGraph = new ISMSGraphSingleStatistic();
			$miProcessId = FWDWebLib::getObject('select_process')->getValue();
			$moProcess = new RMProcess();
			$moProcess->fetchById($miProcessId);
			$msAdditionalName = $moProcess->getFieldValue('process_name');
			$maTypes[] = $psGraphicId;
			$moGraph->setTypes($maTypes);
			$moGraph->setContext($miProcessId);
		break;
		/*
		 * Gr�ficos espec�ficos de ativo.
		 */
		case 'asset_value':
		case 'control_cost_by_asset':
		case 'total_asset_impact':
		case 'process_amount_by_asset':
		case 'risk_amount_by_asset':
		case 'control_amount_by_asset':
			$moGraph = new ISMSGraphSingleStatistic();
			$miAssetId = FWDWebLib::getObject('select_asset')->getValue();
			$moAsset = new RMAsset();
			$moAsset->fetchById($miAssetId);
			$msAdditionalName = $moAsset->getFieldValue('asset_name');
			$maTypes[] = $psGraphicId;
			$moGraph->setTypes($maTypes);
			$moGraph->setContext($miAssetId);
		break;
		/*
		 * Gr�ficos espec�ficos de controle.
		 */
		case 'risk_amount_by_control':
			$moGraph = new ISMSGraphSingleStatistic();
			$miControlId = FWDWebLib::getObject('select_control')->getValue();
			$moControl = new RMControl();
			$moControl->fetchById($miControlId);
			$msAdditionalName = $moControl->getFieldValue('control_name');
			$maTypes[] = $psGraphicId;
			$moGraph->setTypes($maTypes);
			$moGraph->setContext($miControlId);
		break;
		/*
		 * Gr�ficos espec�ficos.
		 */
    case 'incidents_by_state':{
      $moGraph = new ISMSGraphMultipleStatistics();
      $maTypes[] = array('incidents_by_state_open',ISMSContextObject::getContextStateAsString(CONTEXT_STATE_INC_OPEN));
      $maTypes[] = array('incidents_by_state_directed',ISMSContextObject::getContextStateAsString(CONTEXT_STATE_INC_DIRECTED));
      $maTypes[] = array('incidents_by_state_pendant_disposal',ISMSContextObject::getContextStateAsString(CONTEXT_STATE_INC_PENDANT_DISPOSAL));
      $maTypes[] = array('incidents_by_state_waiting_solution',ISMSContextObject::getContextStateAsString(CONTEXT_STATE_INC_WAITING_SOLUTION));
      $maTypes[] = array('incidents_by_state_pendant_solution',ISMSContextObject::getContextStateAsString(CONTEXT_STATE_INC_PENDANT_SOLUTION));
      $maTypes[] = array('incidents_by_state_solved',ISMSContextObject::getContextStateAsString(CONTEXT_STATE_INC_SOLVED));
      $moGraph->setFeatures($maTypes);
      break;
    }
    case 'non_conformity_by_state':{
      $moGraph = new ISMSGraphMultipleStatistics();
      $maTypes[] = array('nc_by_state_ci_sent',ISMSContextObject::getContextStateAsString(CONTEXT_STATE_CI_SENT));
      $maTypes[] = array('nc_by_state_ci_open',ISMSContextObject::getContextStateAsString(CONTEXT_STATE_CI_OPEN));
      $maTypes[] = array('nc_by_state_ci_directed',ISMSContextObject::getContextStateAsString(CONTEXT_STATE_CI_DIRECTED));
      $maTypes[] = array('nc_by_state_ci_nc_pendant',ISMSContextObject::getContextStateAsString(CONTEXT_STATE_CI_NC_PENDANT));
      $maTypes[] = array('nc_by_state_ci_ap_pendant',ISMSContextObject::getContextStateAsString(CONTEXT_STATE_CI_AP_PENDANT));
      $maTypes[] = array('nc_by_state_ci_waiting_conclusion',ISMSContextObject::getContextStateAsString(CONTEXT_STATE_CI_WAITING_CONCLUSION));
      $maTypes[] = array('nc_by_state_ci_finished',ISMSContextObject::getContextStateAsString(CONTEXT_STATE_CI_FINISHED));
      $maTypes[] = array('nc_by_state_ci_closed',ISMSContextObject::getContextStateAsString(CONTEXT_STATE_CI_CLOSED));
      $maTypes[] = array('nc_by_state_denied',ISMSContextObject::getContextStateAsString(CONTEXT_STATE_DENIED));
      $moGraph->setFeatures($maTypes);
      break;
    }
    case 'documents_by_state':{
      $moGraph = new ISMSGraphMultipleStatistics();
      $maTypes[] = array('documents_by_state_developing',ISMSContextObject::getContextStateAsString(CONTEXT_STATE_DOC_DEVELOPING));
      $maTypes[] = array('documents_by_state_approved',ISMSContextObject::getContextStateAsString(CONTEXT_STATE_DOC_APPROVED));
      $maTypes[] = array('documents_by_state_pendant',ISMSContextObject::getContextStateAsString(CONTEXT_STATE_DOC_PENDANT));
      $moGraph->setFeatures($maTypes);
      break;
    }
    case 'documents_by_type':{
      $moGraph = new ISMSGraphMultipleStatistics();
      $maTypes[] = array('documents_by_type_none',FWDLanguage::getPHPStringValue('mx_without_type','Sem Tipo'));
      $maTypes[] = array('documents_by_type_area',ISMSContextObject::getContextObject(CONTEXT_AREA)->getLabel());
      $maTypes[] = array('documents_by_type_process',ISMSContextObject::getContextObject(CONTEXT_PROCESS)->getLabel());
      $maTypes[] = array('documents_by_type_asset',ISMSContextObject::getContextObject(CONTEXT_ASSET)->getLabel());
      $maTypes[] = array('documents_by_type_control',ISMSContextObject::getContextObject(CONTEXT_CONTROL)->getLabel());
      $maTypes[] = array('documents_by_type_scope',ISMSContextObject::getContextObject(CONTEXT_SCOPE)->getLabel());
      $maTypes[] = array('documents_by_type_policy',ISMSContextObject::getContextObject(CONTEXT_POLICY)->getLabel());
      $maTypes[] = array('documents_by_type_management',FWDLanguage::getPHPStringValue('mx_management','Gest�o'));
      $maTypes[] = array('documents_by_type_others',FWDLanguage::getPHPStringValue('mx_others','Outros'));
      $moGraph->setFeatures($maTypes);
      break;
    }
    case 'read_documents':{
      $moGraph = new ISMSGraphMultipleStatistics();
      $maTypes[] = array('documents_read',FWDLanguage::getPHPStringValue('mx_read_documents','Documentos Lidos'));
      $maTypes[] = array('document_total',FWDLanguage::getPHPStringValue('mx_total_documents','Total de Documentos'));
      $moGraph->setFeatures($maTypes);
      break;
    }
    case 'unread_documents':{
      $moGraph = new ISMSGraphMultipleStatistics();
      $maTypes[] = array('documents_not_read',FWDLanguage::getPHPStringValue('mx_unread_documents','Documentos N�o Lidos'));
      $maTypes[] = array('document_total',FWDLanguage::getPHPStringValue('mx_total_documents','Total de Documentos'));
      $moGraph->setFeatures($maTypes);
      break;
    }
    case 'scheduled_documents':{
      $moGraph = new ISMSGraphMultipleStatistics();
      $maTypes[] = array('documents_scheduled',FWDLanguage::getPHPStringValue('mx_scheduled_revision_documents','Documentos Com Revis�o Agendada'));
      $maTypes[] = array('document_total',FWDLanguage::getPHPStringValue('mx_total_documents','Total de Documentos'));
      $moGraph->setFeatures($maTypes);
      break;
    }
		case 'risk_summary':
			$moGraph = new ISMSGraphMultipleStatistics();
			$msRiskValue = FWDWebLib::getObject('risk_value_controller')->getValue();
			$msRiskValue = substr($msRiskValue,1,strlen($msRiskValue)-1);
			$maTypes[] = array("risks_with_high_{$msRiskValue}_risk", FWDLanguage::getPHPStringValue('st_high_risk','Risco Alto'), 0xff0000 /*Vermelho*/);
			$maTypes[] = array("risks_with_medium_{$msRiskValue}_risk", FWDLanguage::getPHPStringValue('st_medium_risk','Risco M�dio'), 0xffff00 /*Amarelo*/);
			$maTypes[] = array("risks_with_low_{$msRiskValue}_risk", FWDLanguage::getPHPStringValue('st_low_risk','Risco Baixo'), 0x00ff00 /*Verde*/);
			$maTypes[] = array("risks_with_no_value_risk", FWDLanguage::getPHPStringValue('st_non_parameterized_risk','Risco N�o Estimado'), 0x0000ff /*Azul*/);
			$moGraph->setFeatures($maTypes);
		break;
		case 'bp_amount_by_standard':
			$moGraph = new ISMSGraphMultipleStatistics();
			$miStandardId = FWDWebLib::getObject('select_standard')->getValue();
			$moStandard = new RMStandard();
			$moStandard->fetchById($miStandardId);
			$msAdditionalName = $moStandard->getFieldValue('standard_name');
			$maTypes[] = array("best_practice_amount_by_standard", FWDLanguage::getPHPStringValue('st_best_practices_amount','Qtd. de Melhores Pr�ticas'));
			$maTypes[] = array("applied_best_practice_amount_by_standard", FWDLanguage::getPHPStringValue('st_applied_best_practices_amount','Qtd. de Melhores Pr�ticas Aplicadas'));
			$moGraph->setFeatures($maTypes);
			$moGraph->setContext($miStandardId);
		break;
		case 'control_implantation':
			$msOption = FWDWebLib::getObject('control_implantation_controller')->getValue();
			$msOption = substr($msOption,1,strlen($msOption)-1);
			if ($msOption == 'both') {
				$moGraph = new ISMSGraphMultipleStatistics();
				$maTypes[] = array('planned_implantation_controls', FWDLanguage::getPHPStringValue('st_controls_being_implemented','Controles em Implanta��o'));
				$maTypes[] = array('late_implantation_controls', FWDLanguage::getPHPStringValue('st_controls_with_delayed_implementation','Controles com Implanta��o Atrasada'));
				$moGraph->setFeatures($maTypes);
			}
			else if ($msOption == 'in_implantation') {
				$moGraph = new ISMSGraphSingleStatistic();
				$msAdditionalName = '- '.FWDLanguage::getPHPStringValue('st_controls_being_implemented','Controles em Implanta��o');
				$maTypes[] = 'planned_implantation_controls';
				$moGraph->setTypes($maTypes);
			}
			else if ($msOption == 'late') {
				$moGraph = new ISMSGraphSingleStatistic();
				$msAdditionalName = '- '.FWDLanguage::getPHPStringValue('st_controls_with_delayed_implementation','Controles com Implanta��o Atrasada');
				$maTypes[] = 'late_implantation_controls';
				$moGraph->setTypes($maTypes);
			}
		break;
		case 'control_adequacy':
			$msOption = FWDWebLib::getObject('control_adequacy_controller')->getValue();
			$msOption = substr($msOption,1,strlen($msOption)-1);
			if ($msOption == 'both') {
				$moGraph = new ISMSGraphMultipleStatistics();
				$maTypes[] = array('adequate_implemented_controls', FWDLanguage::getPHPStringValue('st_controls_correctly_implemented','Controles Implementados Adequadamente'));
				$maTypes[] = array('inadequate_implemented_controls', FWDLanguage::getPHPStringValue('st_controls_incorrectly_implemented','Controles Implementados Inadequadamente'));
				$moGraph->setFeatures($maTypes);
			}
			else if ($msOption == 'adequate') {
				$moGraph = new ISMSGraphSingleStatistic();
				$msAdditionalName = '- '.FWDLanguage::getPHPStringValue('st_controls_correctly_implemented','Controles Implementados Adequadamente');
				$maTypes[] = 'adequate_implemented_controls';
				$moGraph->setTypes($maTypes);
			}
			else if ($msOption == 'inadequate') {
				$moGraph = new ISMSGraphSingleStatistic();
				$msAdditionalName = '- '.FWDLanguage::getPHPStringValue('st_controls_incorrectly_implemented','Controles Implementados Inadequadamente');
				$maTypes[] = 'inadequate_implemented_controls';
				$moGraph->setTypes($maTypes);
			}
		break;
		case 'control_test':
			$msOption = FWDWebLib::getObject('control_test_controller')->getValue();
			$msOption = substr($msOption,1,strlen($msOption)-1);
			if ($msOption == 'both') {
				$moGraph = new ISMSGraphMultipleStatistics();
				$maTypes[] = array('tested_ok_controls', FWDLanguage::getPHPStringValue('st_successfully_tested','Testados com sucesso'));
				$maTypes[] = array('tested_not_ok_controls', FWDLanguage::getPHPStringValue('st_unsuccessfully_tested','Testados sem sucesso'));
				$moGraph->setFeatures($maTypes);
			}
			else if ($msOption == 'ok') {
				$moGraph = new ISMSGraphSingleStatistic();
				$msAdditionalName = '- '.FWDLanguage::getPHPStringValue('st_successfully_tested','Testados com sucesso');
				$maTypes[] = 'tested_ok_controls';
				$moGraph->setTypes($maTypes);
			}
			else if ($msOption == 'notok') {
				$moGraph = new ISMSGraphSingleStatistic();
				$msAdditionalName = '- '.FWDLanguage::getPHPStringValue('st_unsuccessfully_tested','Testados sem sucesso');
				$maTypes[] = 'tested_not_ok_controls';
				$moGraph->setTypes($maTypes);
			}
		break;
		case 'control_revision':
			$msOption = FWDWebLib::getObject('control_revision_controller')->getValue();
			$msOption = substr($msOption,1,strlen($msOption)-1);
			if ($msOption == 'both') {
				$moGraph = new ISMSGraphMultipleStatistics();
				$maTypes[] = array('revision_ok_controls', FWDLanguage::getPHPStringValue('st_successfully_revised','Revisados com sucesso'));
				$maTypes[] = array('revision_not_ok_controls', FWDLanguage::getPHPStringValue('st_unsuccessfully_revised','Revisados sem sucesso'));
				$moGraph->setFeatures($maTypes);
			}
			else if ($msOption == 'ok') {
				$moGraph = new ISMSGraphSingleStatistic();
				$msAdditionalName = '- '.FWDLanguage::getPHPStringValue('st_successfully_revised','Revisados com sucesso');
				$maTypes[] = 'revision_ok_controls';
				$moGraph->setTypes($maTypes);
			}
			else if ($msOption == 'notok') {
				$moGraph = new ISMSGraphSingleStatistic();
				$msAdditionalName = '- '.FWDLanguage::getPHPStringValue('st_unsuccessfully_revised','Revisados sem sucesso');
				$maTypes[] = 'revision_not_ok_controls';
				$moGraph->setTypes($maTypes);
			}
		break;
		case 'risk_treatment':
			$msOption = FWDWebLib::getObject('risk_treatment_controller')->getValue();
			$msOption = substr($msOption,1,strlen($msOption)-1);
			if ($msOption == 'both') {
				$moGraph = new ISMSGraphMultipleStatistics();
				$maTypes[] = array('risks_treated', FWDLanguage::getPHPStringValue('st_treated_risks','Riscos Tratados'));
				$maTypes[] = array('risks_not_treated', FWDLanguage::getPHPStringValue('st_not_treated_risks','Riscos n�o Tratados'));
				$moGraph->setFeatures($maTypes);
			}
			else if ($msOption == 'treated') {
				$moGraph = new ISMSGraphSingleStatistic();
				$msAdditionalName = '- '.FWDLanguage::getPHPStringValue('st_treated_risks','Riscos Tratados');
				$maTypes[] = 'risks_treated';
				$moGraph->setTypes($maTypes);
			}
			else if ($msOption == 'not_treated') {
				$moGraph = new ISMSGraphSingleStatistic();
				$msAdditionalName = '- '.FWDLanguage::getPHPStringValue('st_not_treated_risks','Riscos n�o Tratados');
				$maTypes[] = 'risks_not_treated';
				$moGraph->setTypes($maTypes);
			}
		break;
		case 'implemented_controls_investment':
			$moGraph = new ISMSGraphMultipleStatistics();
			$maTypes[] = array('control_total_cost1', ISMSLib::getConfigById(CONTROL_COST_1));
			$maTypes[] = array('control_total_cost2', ISMSLib::getConfigById(CONTROL_COST_2));
			$maTypes[] = array('control_total_cost3', ISMSLib::getConfigById(CONTROL_COST_3));
			$maTypes[] = array('control_total_cost4', ISMSLib::getConfigById(CONTROL_COST_4));
			$maTypes[] = array('control_total_cost5', ISMSLib::getConfigById(CONTROL_COST_5));
			$moGraph->setFeatures($maTypes);
		break;
		case 'risk_treatment_potential_impact':
			$moGraph = new ISMSGraphMultipleStatistics();
			$maTypes[] = array('mitigated_risk_cost', FWDLanguage::getPHPStringValue('st_mitigated_risk','Risco Reduzido'));
			$maTypes[] = array('accepted_risk_cost', FWDLanguage::getPHPStringValue('st_accepted_risk','Risco Aceito'));
			$maTypes[] = array('transfered_risk_cost', FWDLanguage::getPHPStringValue('st_tranferred_risk','Risco Transferido'));
			$maTypes[] = array('avoided_risk_cost', FWDLanguage::getPHPStringValue('st_avoided_risk','Risco Evitado'));
			$moGraph->setFeatures($maTypes);
		break;
		case 'risk_potential_impact':
			$moGraph = new ISMSGraphMultipleStatistics();
			$maTypes[] = array('potentially_low_risk_cost', FWDLanguage::getPHPStringValue('st_potentially_low_risk','Risco Potencialmente Baixo'));
			$maTypes[] = array('not_treated_and_medium_high_risk', FWDLanguage::getPHPStringValue('st_medium_high_not_treated_risk','Risco M�dio / Alto / N�o Tratado'));
			$moGraph->setFeatures($maTypes);
		break;
		case 'areas_risk_summary':
			$moGraph = new ISMSGraphMultipleStatistics();
			$maTypes[] = array('areas_with_high_risk', FWDLanguage::getPHPStringValue('st_high_risk','Risco Alto'), 0xff0000 /*Vermelho*/);
			$maTypes[] = array('areas_with_medium_risk', FWDLanguage::getPHPStringValue('st_medium_risk','Risco M�dio'), 0xffff00 /*Amarelo*/);
			$maTypes[] = array('areas_with_low_risk', FWDLanguage::getPHPStringValue('st_low_risk','Risco Baixo'), 0x00ff00 /*Verde*/);
			$maTypes[] = array('areas_with_no_value_risk', FWDLanguage::getPHPStringValue('st_non_parameterized_risk','Risco N�o Estimado'), 0x0000ff /*Azul*/);
			$moGraph->setFeatures($maTypes);
		break;
		case 'processes_risk_summary':
			$moGraph = new ISMSGraphMultipleStatistics();
			$maTypes[] = array('processes_with_high_risk', FWDLanguage::getPHPStringValue('st_high_risk','Risco Alto'), 0xff0000 /*Vermelho*/);
			$maTypes[] = array('processes_with_medium_risk', FWDLanguage::getPHPStringValue('st_medium_risk','Risco M�dio'), 0xffff00 /*Amarelo*/);
			$maTypes[] = array('processes_with_low_risk', FWDLanguage::getPHPStringValue('st_low_risk','Risco Baixo'), 0x00ff00 /*Verde*/);
			$maTypes[] = array('processes_with_no_value_risk', FWDLanguage::getPHPStringValue('st_non_parameterized_risk','Risco N�o Estimado'), 0x0000ff /*Azul*/);
			$moGraph->setFeatures($maTypes);
		break;
		case 'assets_risk_summary':
			$moGraph = new ISMSGraphMultipleStatistics();
			$maTypes[] = array('assets_with_high_risk', FWDLanguage::getPHPStringValue('st_high_risk','Risco Alto'), 0xff0000 /*Vermelho*/);
			$maTypes[] = array('assets_with_medium_risk', FWDLanguage::getPHPStringValue('st_medium_risk','Risco M�dio'), 0xffff00 /*Amarelo*/);
			$maTypes[] = array('assets_with_low_risk', FWDLanguage::getPHPStringValue('st_low_risk','Risco Baixo'), 0x00ff00 /*Verde*/);
			$maTypes[] = array('assets_with_no_value_risk', FWDLanguage::getPHPStringValue('st_non_parameterized_risk','Risco N�o Estimado'), 0x0000ff /*Azul*/);
			$moGraph->setFeatures($maTypes);
		break;
		case 'business_areas':
			$moGraph = new ISMSGraphMultipleStatistics();
			$maTypes[] = array('complete_areas', FWDLanguage::getPHPStringValue('st_complete_areas','�reas Completas'));
			$maTypes[] = array('partial_areas', FWDLanguage::getPHPStringValue('st_partial_areas','�reas Parcias'));
			$maTypes[] = array('not_managed_areas', FWDLanguage::getPHPStringValue('st_unmanaged_areas','�reas N�o Gerenciadas'));
			$moGraph->setFeatures($maTypes);
		break;
		case 'business_processes':
			$moGraph = new ISMSGraphMultipleStatistics();
			$maTypes[] = array('complete_processes', FWDLanguage::getPHPStringValue('st_complete_processes','Processos Completos'));
			$maTypes[] = array('partial_processes', FWDLanguage::getPHPStringValue('st_partial_processes','Processos Parcias'));
			$maTypes[] = array('not_managed_processes', FWDLanguage::getPHPStringValue('st_unmanaged_processes','Processos N�o Gerenciados'));
			$moGraph->setFeatures($maTypes);
		break;
		case 'assets':
			$moGraph = new ISMSGraphMultipleStatistics();
			$maTypes[] = array('complete_assets', FWDLanguage::getPHPStringValue('st_complete_assets','Ativos Completos'));
			$maTypes[] = array('partial_assets', FWDLanguage::getPHPStringValue('st_partial_assets','Ativos Parcias'));
			$maTypes[] = array('not_managed_assets', FWDLanguage::getPHPStringValue('st_unmanaged_assets','Ativos N�o Gerenciados'));
			$moGraph->setFeatures($maTypes);
		break;
    /*graficios especiais de plando de a��o*/
    case 'ap_efficient_and_not':
      $msOption = FWDWebLib::getObject('ap_efficient_controller')->getValue();
      $msOption = substr($msOption,1,strlen($msOption)-1);
      if ($msOption == 'both') {
        $moGraph = new ISMSGraphMultipleStatistics();
        $maTypes[] = array('ap_efficient', FWDLanguage::getPHPStringValue('st_efficient_action_plan','Planos de a��o eficientes'));
        $maTypes[] = array('ap_non_efficient', FWDLanguage::getPHPStringValue('st_non_efficient_action_plan','Planos de a��o n�o eficientes'));
        $moGraph->setFeatures($maTypes);
      }
      else if ($msOption == 'efficient') {
        $moGraph = new ISMSGraphMultipleStatistics();
        $maTypes[] = array('ap_efficient', FWDLanguage::getPHPStringValue('st_efficient_action_plan','Planos de a��o eficientes'));
        $moGraph->setFeatures($maTypes);
      }
      else if ($msOption == 'non_efficient') {
        $moGraph = new ISMSGraphMultipleStatistics();
        $maTypes[] = array('ap_non_efficient', FWDLanguage::getPHPStringValue('st_non_efficient_action_plan','Planos de a��o n�o eficientes'));
        $moGraph->setFeatures($maTypes);
      }
    break;
	}
  if($moGraph){
    $msGraphName = getGraphName($msName).($msAdditionalName?(substr($msAdditionalName,0,1)=='-'?' '.$msAdditionalName:': '.$msAdditionalName):'');
    $moGraph->setName($msGraphName);
    $moGraph->setStartDate(FWDWebLib::getObject('date_start')->getTimestamp());
    $moGraph->setFinishDate(FWDWebLib::getObject('date_finish')->getTimestamp());
  }
	return $moGraph;
}

/*
 * Baseado no identificador do gr�fico, verifica se necessita mostrar o filtro.
 */
function checkFilters($psGraphicId) {
	switch($psGraphicId) {
		case 'area_value':
		case 'asset_cost_by_area':
		case 'control_cost_by_area':
		case 'total_area_impact':
		case 'process_amount_by_area':
		case 'asset_amount_by_area':
		case 'risk_amount_by_area':
		case 'control_amount_by_area':
			echo "js_show('area_filter');";
		break;

		case 'process_value':
		case 'asset_cost_by_process':
		case 'control_cost_by_process':
		case 'total_process_impact':
		case 'asset_amount_by_process':
		case 'risk_amount_by_process':
		case 'control_amount_by_process':
			echo "js_show('process_filter');";
		break;

		case 'asset_value':
		case 'control_cost_by_asset':
		case 'total_asset_impact':
		case 'process_amount_by_asset':
		case 'risk_amount_by_asset':
		case 'control_amount_by_asset':
			echo "js_show('asset_filter');";
		break;

		case 'risk_amount_by_control':
			echo "js_show('control_filter');";
		break;

		case 'risk_summary':
			echo "js_show('risk_value_filter');";
		break;

		case 'control_implantation':
			echo "js_show('control_implantation_filter');";
		break;

		case 'control_adequacy':
			echo "js_show('control_adequacy_filter');";
		break;

		case 'control_test':
			echo "js_show('control_test_filter');";
		break;

		case 'control_revision':
			echo "js_show('control_revision_filter');";
		break;
		
		case 'risk_treatment':
			echo "js_show('risk_treatment_filter');";
		break;
		
		case 'bp_amount_by_standard':
			echo "js_show('standard_filter');";
		break;
    case 'ap_efficient_and_not':
      echo "js_show('ap_efficient_filter');";
    break;
	}
}

class ChangeGraphicEvent extends FWDRunnable {
  public function run() {
    $msGraphicId = FWDWebLib::getObject('select_graphic')->getValue();
    checkFilters($msGraphicId);
    $moGraph = getChart($msGraphicId);
    $moChart = FWDWebLib::getObject('graphic');
    $moGraph = $moGraph->makeGraph();
    if($moGraph!==null){
      $moChart->setChart($moGraph);
      $msImgId = $moChart->getChartName();
      $msSrc = FWDWebLib::getInstance()->getLibRef()."FWDGetImg.php?img=$msImgId&id=".uniqid(session_id());
      echo "js_hide('warning');";
      echo "js_show('graphic');";
      echo "gebi('graphic').src='".$msSrc."';";
    }else{
      echo "js_hide('graphic');";
      echo "js_show('warning');";
    }
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new ChangeGraphicEvent("change_graphic_event"));        
    $moStartEvent->addSubmitEvent(new DownloadGraphicEvent("download_graphic_event"));
    
        
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    //configura��o de coleta de dados est� desabilitada, esta tela n�o deve ser exibida
    $moConfig = new ISMSConfig();
    if(!$moConfig->getConfig(GENERAL_DATA_COLLECTION_ENABLED)){
      echo FWDLanguage::getPHPStringValue('st_denied_access_to_statistics', "A configura��o referente a coleta de dados est� desabilitada, assim, n�o � possivel visualizar esta tela.");
      exit();
    }
    
    /*
  	 * Popula o select com as �reas do sistema
  	 */ 	
    $moSelect = FWDWebLib::getObject('select_area');
    $moQuery = new QueryContextHierarchicalStructure(FWDWebLib::getConnection());
    $moQuery->setContextType(CONTEXT_AREA);
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    foreach ($moQuery->getContexts() as $maContext) {
      list($miContextId,$msHierarchicalName) = $maContext;
      $moSelect->setItemValue($miContextId,$msHierarchicalName);      
    }
    
    /*
  	 * Popula o select com os processos do sistema
  	 */  	
    $moHandler = new QuerySelectProcess(FWDWebLib::getConnection());
    $moSelect = FWDWebLib::getObject('select_process');
    $moSelect->setQueryHandler($moHandler);
    $moSelect->populate();
    
    /*
  	 * Popula o select com os ativos do sistema
  	 */  	
    $moHandler = new QuerySelectAsset(FWDWebLib::getConnection());
    $moSelect = FWDWebLib::getObject('select_asset');
    $moSelect->setQueryHandler($moHandler);
    $moSelect->populate();
    
    /*
  	 * Popula o select com os controles do sistema
  	 */  	
    $moHandler = new QuerySelectControl(FWDWebLib::getConnection());
    $moSelect = FWDWebLib::getObject('select_control');
    $moSelect->setQueryHandler($moHandler);
    $moSelect->populate();
    
    /*
  	 * Popula o select com as normas do sistema
  	 */  	
    $moHandler = new QuerySelectStandard(FWDWebLib::getConnection());
    $moHandler->setAllStandards(true);
    $moSelect = FWDWebLib::getObject('select_standard');
    $moSelect->setQueryHandler($moHandler);
    $moSelect->populate();
  	
  	/*
  	 * Popula o select com os diferentes tipos de estat�sticas
  	 * baseado nas permiss�es que o usu�rio possui
  	 */ 	 
  	$maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();  	
  	$maGraphCategory = getGraphCategoryArray();
  	$moGraphSelect = FWDWebLib::getObject('select_graphic');
    $moConfig = new ISMSConfig();
    $mbHasRevision = $moConfig->getConfig(GENERAL_REVISION_ENABLED);
    $mbHasCost = $moConfig->getConfig(GENERAL_COST_ENABLED);
    $mbHasTests = $moConfig->getConfig(GENERAL_TEST_ENABLED);
    
    $msFirstChartToShow = '';
    $msFirstSelecteGroup = '';
    $maSpecialTest = array('control_test','control_revision','not_measured_implemented_controls');
    
  	foreach ($maGraphCategory as $miKey => $maGraphs) {
  		if(($miKey!='financial_graphs') || ( ($moConfig->getConfig(GENERAL_COST_ENABLED) && ($miKey=='financial_graphs') ) )){
        if(!in_array($maGraphs['acl'], $maACLs)) {
          $moGroup = new FWDOptgroup();
          $moGroup->setValue($maGraphs['graph_type_name']);
          foreach ($maGraphs['graphs'] as $msGraphKey => $msGraph) {				
            if( ( !in_array( $msGraphKey,$maSpecialTest) )
                || ( $msGraphKey=='control_test' && $mbHasTests )
                || ( $msGraphKey=='control_revision' && $mbHasRevision )
                || ( $msGraphKey=='not_measured_implemented_controls' && ($mbHasTests || $mbHasRevision) )
              ){
                  if (!$msFirstChartToShow){
                    $msFirstChartToShow .= $msGraphKey;
                    $msFirstSelecteGroup .= $miKey;
                  }
                  $moItem = new FWDItem();
                  $moItem->setAttrKey($msGraphKey);
                  $moItem->setValue($msGraph);
                  $moGroup->addObjFWDItem($moItem);       
            }
          }
          $moGraphSelect->addObjFWDOptgroup($moGroup);
        }
      }
    }
    
    $moGraph = null;
    if($msFirstChartToShow){
      $moGraph = getChart($msFirstChartToShow);
      $moGraph = $moGraph->makeGraph();
      FWDWebLib::getObject('select_graphic')->setValue($msFirstChartToShow);
    }else{
      FWDWebLib::getObject('dialog')->setShouldDraw(false);
    }
    
    if($moGraph){
      FWDWebLib::getObject('graphic')->setChart($moGraph);
    }else{
      FWDWebLib::getObject('graphic')->setAttrDisplay('false');
      FWDWebLib::getObject('warning')->setAttrDisplay('true');
    }

    //instala a seguran�a de ACL na p�gina
    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
		    
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
	    <script language="javascript">
	    	function hide_all_filters() {
	    		js_hide('area_filter');
	    		js_hide('process_filter');
	    		js_hide('asset_filter');
	    		js_hide('risk_value_filter');
	    		js_hide('control_implantation_filter');
	    		js_hide('control_adequacy_filter');
	    		js_hide('control_test_filter');
	    		js_hide('control_revision_filter');
	    		js_hide('risk_treatment_filter');
	    		js_hide('control_filter');
	    		js_hide('standard_filter');
          js_hide('ap_efficient_filter');
	    	}
	    
		    function change_graphic() {
		    	hide_all_filters();
		      trigger_event("change_graphic_event","3");
		    }
	    </script>
    <?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_statistics.xml");
?>