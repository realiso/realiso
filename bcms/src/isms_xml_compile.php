<?php
set_time_limit(0);
include_once "include.php";

$soWebLib = FWDWebLib::getInstance();

$FWDXmlCompile_instance = FWDXmlCompile::getInstance();

// compila o modo gestor
$FWDXmlCompile_instance->xmlCompile(".");

// compila o modo administrativo
$FWDXmlCompile_instance->xmlCompile("./packages/admin");
$FWDXmlCompile_instance->xmlCompile("./packages/admin/report");

// compila o risk management
$FWDXmlCompile_instance->xmlCompile("./packages/risk");
$FWDXmlCompile_instance->xmlCompile("./packages/risk/report");

// compila as bibliotecas
$FWDXmlCompile_instance->xmlCompile("./packages/libraries");

echo "bibliotecas\n";

// compila o m�dulo de documenta��o
$FWDXmlCompile_instance->xmlCompile("./packages/policy");

echo "policias\n";

$FWDXmlCompile_instance->xmlCompile("./packages/policy/report");

echo "report\n";

// compila o m�dulo de incidente
$FWDXmlCompile_instance->xmlCompile("./packages/improvement");
$FWDXmlCompile_instance->xmlCompile("./packages/improvement/report");

echo "continuity";

$FWDXmlCompile_instance->xmlCompile("./packages/continuity");

?>