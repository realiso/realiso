<?php
include_once "include.php";

class YesEvent extends FWDRunnable {
  public function run(){
    /*
     * Caso o usu�rio decida acabar com o v�nculo,
     * deleta da tabela de procuradores a rela��o
     * e loga no sistema
     */
    $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    $moSolicitor = new ISMSSolicitor();
    $moSolicitor->createFilter($miUserId, 'user_id');
    $moSolicitor->delete();
    //echo "parent.document.location.href = 'tab_main.php';";
    echo "soPopUpManager.getPopUpById('popup_solicitor_conflict').getOpener().execLoginEvent();";
    echo "soPopUpManager.closePopUp('popup_solicitor_conflict');"; 
  }
}

class NoEvent extends FWDRunnable {
  public function run(){
    /*
     * Caso o usu�rio decida continuar sendo representado por um
     * outro usu�rio, apenas destr�i a sess�o e redireciona para
     * a tela de login
     */
    FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->logout();
    echo "parent.document.location.href = 'login.php';";
    echo "soPopUpManager.closePopUp('popup_solicitor_conflict');";
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new YesEvent('yes_event'));
    $moStartEvent->addAjaxEvent(new NoEvent('no_event'));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();    
    $moUser = new ISMSUser();
    $moUser->fetchById($miUserId);
    $moSolicitor = new ISMSUser();
    $moSolicitor->fetchById($moUser->getSolicitor());
    
    FWDWebLib::getObject('message')->replace('%solicitor_name%', $moSolicitor->getFieldValue('user_name'));
  
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));    
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_solicitor_conflict.xml");
?>