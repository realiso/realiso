<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe ISMSContextDrawGrid
 *
 * <p>DrawGrid gen�rico para desenhar grids de contextos.</p>
 * @package ISMS
 * @subpackage classes
 */
class ISMSContextDrawGrid extends FWDDrawGrid {

  const TYPE_STATE          = 1;
  const TYPE_DATE           = 2;
  const TYPE_SHORT_DATE     = 3;
  const TYPE_CLASSIFICATION = 4;

  protected $csIconSrc;
  protected $ciContextType = CONTEXT_NONE;
  protected $csIdColumn = '';
  protected $csNameColumn = '';
  
  protected $csCreateNameColumn = '';
  protected $csCreateDateColumn = '';
  protected $csModifyNameColumn = '';
  protected $csModifyDateColumn = '';

 /**
  * Construtor.
  * 
  * <p>Construtor.</p>
  * @access public
  * @param string $psIconSrc Nome do �cone
  * @param integer $piContextType C�digo de tipo do contexto (sse passado, gera link pra visualize)
  * @param string $psIdColumn Alias da coluna de id (se omitido, � usada a 1� coluna)
  * @param string $psNameColumn Alias da coluna de nome (se omitido, � usada a 2� coluna)
  */
  public function __construct($psIconSrc, $piContextType=CONTEXT_NONE, $psIdColumn='', $psNameColumn=''){
    $this->csIconSrc = $psIconSrc;
    $this->ciContextType = $piContextType;
    $this->csIdColumn = $psIdColumn;
    $this->csNameColumn = $psNameColumn;
  }
  
 /**
  * Define o tipo de uma coluna.
  * 
  * <p>Define o tipo de uma coluna entre os tipos pr�-definidos.</p>
  * @access public
  * @param string $psColumnAlias Alias da coluna
  * @param integer $piType Tipo da coluna
  */
  public function setColumnType($psColumnAlias, $piType){
    $this->caColumnTypes[$psColumnAlias] = $piType;
  }

 /**
  * Configura a ToolTip de cria��o e modifica��o.
  * 
  * <p>Define as colunas usadas pela ToolTip de cria��o e modifica��o.</p>
  * @access public
  * @param string $psCreateNameColumn Alias da coluna de nome do criador
  * @param string $psCreateDateColumn Alias da coluna de data de cria��o
  * @param string $psModifyNameColumn Alias da coluna de nome do �ltimo a modificar
  * @param string $psModifyDateColumn Alias da coluna de data de modifica��o
  */
  public function setupToolTipCreateModify($psCreateNameColumn, $psCreateDateColumn, $psModifyNameColumn, $psModifyDateColumn){
    $this->csCreateNameColumn = $psCreateNameColumn;
    $this->csCreateDateColumn = $psCreateDateColumn;
    $this->csModifyNameColumn = $psModifyNameColumn;
    $this->csModifyDateColumn = $psModifyDateColumn;
  }
  
 /**
  * Configura o menu.
  * 
  * <p>M�todo que deve ser sobrescrito para, de acordo com as permiss�es do
  * usu�rio, confugurar o menu.</p>
  * @access protected
  */
  protected function setupMenu(){
  }

 /**
  * Desenha o item.
  * 
  * <p>Sobrescreve o drawItem da FWDDrawGrid para desenhar as colunas de acordo
  * com o que foi definido.</p>
  * @access public
  */
  public function drawItem(){
    $miIdColumn = ($this->csIdColumn?$this->getIndexByAlias($this->csIdColumn):1);
    $miNameColumn = ($this->csNameColumn?$this->getIndexByAlias($this->csNameColumn):2);
    $msColumnAlias = $this->getAliasByIndex($this->ciColumnIndex);
    // A primeira coluna � do �cone
    if($this->ciColumnIndex==1){
      $this->setupMenu(); // Chama o setup do menu
      if($this->csCreateNameColumn){
        $miCreateName = $this->caData[$this->getIndexByAlias($this->csCreateNameColumn)];
        $miCreateDate = $this->caData[$this->getIndexByAlias($this->csCreateDateColumn)];
        $miModifyName = $this->caData[$this->getIndexByAlias($this->csModifyNameColumn)];
        $miModifyDate = $this->caData[$this->getIndexByAlias($this->csModifyDateColumn)];
        $this->coCellBox->setObjFWDToolTip(ISMSLib::getInstance()->getToolTipCreateModify($miCreateName,$miCreateDate,$miModifyName,$miModifyDate));
      }
      $this->coCellBox->setIconSrc($this->csIconSrc);
      return $this->coCellBox->draw();
    }
    // Na coluna do nome, se tiver definido o tipo, cria o link pra visualize
    if($this->ciColumnIndex==$miNameColumn && $this->ciContextType!=CONTEXT_NONE){
      $this->coCellBox->setAttrStringNoEscape('true');
      $miId = $this->caData[$miIdColumn];
      $msName = $this->caData[$miNameColumn];
      $this->coCellBox->setValue("<a href='javascript:open_visualize({$miId},{$this->ciContextType},\"".uniqid()."\")'>{$msName}</a>");
      return $this->coCellBox->draw();
    }
    // Colunas com tipos pr�-definidos
    if(isset($this->caColumnTypes[$msColumnAlias])){
      switch($this->caColumnTypes[$msColumnAlias]){
        case self::TYPE_STATE:{
          $this->coCellBox->setValue(ISMSContextObject::getContextStateAsString($this->coCellBox->getValue()));
          break;
        }
        case self::TYPE_SHORT_DATE:{
          if($this->coCellBox->getValue()){
            $this->coCellBox->setValue(ISMSLib::getISMSShortDate($this->coCellBox->getValue()));
          }
          break;
        }
        case self::TYPE_DATE:{
          if($this->coCellBox->getValue()){
            $this->coCellBox->setValue(ISMSLib::getISMSDate($this->coCellBox->getValue()));
          }
          break;
        }
        case self::TYPE_CLASSIFICATION:{
          $moContextClassification = new ISMSContextClassification();
          $moContextClassification->fetchById($this->coCellBox->getValue());
          $this->coCellBox->setValue($moContextClassification->getFieldValue('classif_name'));
          break;
        }
      }
    }
    return parent::drawItem();
  }

}

?>