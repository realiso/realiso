<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe RegistersDrawGrid
 *
 * <p>Responsável por desenhar as grids de registros.</p>
 * @package ISMS
 * @subpackage classes
 */
class RegistersDrawGrid extends FWDDrawGrid {
  public function drawItem(){
    switch($this->ciColumnIndex){
      case 2:
        $this->coCellBox->setAttrStringNoEscape("true");
        $this->coCellBox->setValue("<a href='javascript:open_visualize(".$this->caData[1].",".CONTEXT_REGISTER.",\"".uniqid()."\")'>{$this->caData[2]}</a>");
        return $this->coCellBox->draw();
      case 3:
        $miValue = $this->coCellBox->getValue();
        $miPeriod = $this->caData[7];
        $moRegister = new PMRegister();
        $this->coCellBox->setValue($moRegister->getRetentionTimeAsString($miValue,$miPeriod));
        return $this->coCellBox->draw();
      case 6:
        $moContextClassification = new ISMSContextClassification();
        $moContextClassification->fetchById($this->coCellBox->getValue());
        $msContextClassification = $moContextClassification->getFieldValue('classif_name');
        $this->coCellBox->setValue($msContextClassification);
        return $this->coCellBox->draw();
      default:
        return parent::drawItem();
    }
  }
}

?>