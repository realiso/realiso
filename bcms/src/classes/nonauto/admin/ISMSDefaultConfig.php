<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */


/**
 * Classe ISMSDefaultConfig
 *
 * <p>Classe que manipula a restaura��o das configura��es padr�o do sistema.</p>
 * @package ISMS
 * @subpackage classes
 */
class ISMSDefaultConfig{
  protected $caISMSDefaultConfigRiskParNames;
  protected $caISMSDefaultConfigValParRisk;
  protected $caISMSDefaultConfigCostNames;
  protected $caISMSDefaultConfigPasswdPolicy;
  protected $caISMSDefaultConfigParametrization;
  protected $caISMSDefaultConfigValParRisk3_to_5_change;
   /**
  * Construtor.
  * 
  * <p>Construtor da classe ISMSDefaultConfig.</p>
  * @access public 
  */
  public function __construct(){
    /*configura��o padr�o dos nomes de risco*/
    $this->caISMSDefaultConfigRiskParNames = array(
      'risk_par_1'=>array('' , FWDLanguage::getPHPStringValue('db_risk_disponibility'    ,'Disponibilidade'   )),
      'risk_par_2'=>array('' , FWDLanguage::getPHPStringValue('db_risk_confidenciality'  ,'Confidencialidade' )),
      'risk_par_3'=>array('' , FWDLanguage::getPHPStringValue('db_risk_intregrity'       ,'Integridade'       ))
    );

    /*configura��o padr�o dos valores dos riscos*/
    $this->caISMSDefaultConfigValParRisk = array(
      'importance3_1'=>array('' , FWDLanguage::getPHPStringValue('db_importance3_1','Baixo')),
      'importance3_2'=>array('' , FWDLanguage::getPHPStringValue('db_importance3_2','M�dio')),
      'importance3_3'=>array('' , FWDLanguage::getPHPStringValue('db_importance3_3','Alto' )),
      'impact3_1'=>array('' , FWDLanguage::getPHPStringValue('db_impact3_1','Baixo')),
      'impact3_2'=>array('' , FWDLanguage::getPHPStringValue('db_impact3_2','M�dio')),
      'impact3_3'=>array('' , FWDLanguage::getPHPStringValue('db_impact3_3','Alto' )),
      'riskprob3_1'=>array('' , FWDLanguage::getPHPStringValue('db_riskprob3_1','Improv�vel')),
      'riskprob3_2'=>array('' , FWDLanguage::getPHPStringValue('db_riskprob3_2','Moderado'  )),
      'riskprob3_3'=>array('' , FWDLanguage::getPHPStringValue('db_riskprob3_3','Prov�vel'  )),
      'rc_impact3_0'=>array('' , FWDLanguage::getPHPStringValue('db_rcimpact3_0','N�o afeta'                )),
      'rc_impact3_1'=>array('' , FWDLanguage::getPHPStringValue('db_rcimpact3_1','Decr�scimo de um N�vel'   )),
      'rc_impact3_2'=>array('' , FWDLanguage::getPHPStringValue('db_rcimpact3_2','Decr�scimo de dois N�veis')),
      'rc_prob3_0'=>array('' , FWDLanguage::getPHPStringValue('db_rcprob3_0','N�o afeta'                )),
      'rc_prob3_1'=>array('' , FWDLanguage::getPHPStringValue('db_rcprob3_1','Decr�scimo de um N�vel'   )),
      'rc_prob3_2'=>array('' , FWDLanguage::getPHPStringValue('db_rcprob3_2','Decr�scimo de dois N�veis')),
      'importance5_1'=>array('' , FWDLanguage::getPHPStringValue('db_importance_1','Negligenci�vel')),
      'importance5_2'=>array('' , FWDLanguage::getPHPStringValue('db_importance_2','Baixo'         )),
      'importance5_3'=>array('' , FWDLanguage::getPHPStringValue('db_importance_3','M�dio'         )),
      'importance5_4'=>array('' , FWDLanguage::getPHPStringValue('db_importance_4','Alto'          )),
      'importance5_5'=>array('' , FWDLanguage::getPHPStringValue('db_importance_5','Cr�tico'       )),
      'impact5_1'=>array('' , FWDLanguage::getPHPStringValue('db_impact_1','Muito Baixo')),
      'impact5_2'=>array('' , FWDLanguage::getPHPStringValue('db_impact_2','Baixo'      )),
      'impact5_3'=>array('' , FWDLanguage::getPHPStringValue('db_impact_3','M�dio'      )),
      'impact5_4'=>array('' , FWDLanguage::getPHPStringValue('db_impact_4','Alto'       )),
      'impact5_5'=>array('' , FWDLanguage::getPHPStringValue('db_impact_5','Muito Alto' )),
      'riskprob5_1'=>array('' , FWDLanguage::getPHPStringValue('db_riskprob_1','Raro (1% - 20%)'        )),
      'riskprob5_2'=>array('' , FWDLanguage::getPHPStringValue('db_riskprob_2','Improv�vel (21% - 40%)' )),
      'riskprob5_3'=>array('' , FWDLanguage::getPHPStringValue('db_riskprob_3','Moderado (41% - 60%)'   )),
      'riskprob5_4'=>array('' , FWDLanguage::getPHPStringValue('db_riskprob_4','Prov�vel (61% - 80%)'   )),
      'riskprob5_5'=>array('' , FWDLanguage::getPHPStringValue('db_riskprob_5','Quase certo (81% - 99%)')),
      'rc_impact5_0'=>array('' , FWDLanguage::getPHPStringValue('db_rcimpact_0','N�o afeta'                  )),
      'rc_impact5_1'=>array('' , FWDLanguage::getPHPStringValue('db_rcimpact_1','Decr�scimo de um N�vel'     )),
      'rc_impact5_2'=>array('' , FWDLanguage::getPHPStringValue('db_rcimpact_2','Decr�scimo de dois N�veis'  )),
      'rc_impact5_3'=>array('' , FWDLanguage::getPHPStringValue('db_rcimpact_3','Decr�scimo de tr�s N�veis'  )),
      'rc_impact5_4'=>array('' , FWDLanguage::getPHPStringValue('db_rcimpact_4','Decr�scimo de quatro N�veis')),
      'rc_prob5_0'=>array('' , FWDLanguage::getPHPStringValue('db_rcprob_0','N�o afeta'                  )),
      'rc_prob5_1'=>array('' , FWDLanguage::getPHPStringValue('db_rcprob_1','Decr�scimo de um N�vel'     )),
      'rc_prob5_2'=>array('' , FWDLanguage::getPHPStringValue('db_rcprob_2','Decr�scimo de dois N�veis'  )),
      'rc_prob5_3'=>array('' , FWDLanguage::getPHPStringValue('db_rcprob_3','Decr�scimo de tr�s N�veis'  )),
      'rc_prob5_4'=>array('' , FWDLanguage::getPHPStringValue('db_rcprob_4','Decr�scimo de quatro N�veis'))          
    );

    /*configura��o padr�o dos valores dos riscos na mudan�a de 3  valores para 5 valores*/
    $this->caISMSDefaultConfigValParRisk3_to_5_change = array(
      'importance5_1'=>array('' , FWDLanguage::getPHPStringValue('db_importance_1','Negligenci�vel')),
      'importance5_5'=>array('' , FWDLanguage::getPHPStringValue('db_importance_5','Cr�tico'       )),
      'impact5_1'    =>array('' , FWDLanguage::getPHPStringValue('db_impact_1','Muito Baixo')),
      'impact5_5'    =>array('' , FWDLanguage::getPHPStringValue('db_impact_5','Muito Alto' )),
      'riskprob5_1'  =>array('' , FWDLanguage::getPHPStringValue('db_riskprob_1','Raro (1% - 20%)'        )),
      'riskprob5_5'  =>array('' , FWDLanguage::getPHPStringValue('db_riskprob_5','Quase certo (81% - 99%)')),
      'rc_impact5_0' =>array('' , FWDLanguage::getPHPStringValue('db_rcimpact_0','N�o afeta'                  )),
      'rc_impact5_4' =>array('' , FWDLanguage::getPHPStringValue('db_rcimpact_4','Decr�scimo de quatro N�veis')),
      'rc_prob5_0'   =>array('' , FWDLanguage::getPHPStringValue('db_rcprob_0','N�o afeta'                  )),
      'rc_prob5_4'   =>array('' , FWDLanguage::getPHPStringValue('db_rcprob_4','Decr�scimo de quatro N�veis'))          
    );

    /*configura��o padr�o dos nomes dos custos*/
    $this->caISMSDefaultConfigControlCostNames = array(
      'cname1'=>array('cname1' , FWDLanguage::getPHPStringValue('db_control_cost_hardware','Hardware' )),
      'cname2'=>array('cname2' , FWDLanguage::getPHPStringValue('db_control_cost_software','Software' )),
      'cname3'=>array('cname3' , FWDLanguage::getPHPStringValue('db_control_cost_material','Servi�os' )),
      'cname4'=>array('cname4' , FWDLanguage::getPHPStringValue('db_control_cost_pessoal' ,'Pessoas'  )),
      'cname5'=>array('cname5' , FWDLanguage::getPHPStringValue('db_control_cost_others'  ,'Educa��o' )),

    );
    
    /*configura��o padr�o dos nomes dos custos financeiros do CI*/
    $this->caISMSDefaultConfigCICostNames = array(
      '1'=>array('fin_imp_text_1' , FWDLanguage::getPHPStringValue('db_incident_cost_aplicable_penalty' ,'Multa'                  )),
      '2'=>array('fin_imp_text_2' , FWDLanguage::getPHPStringValue('db_incident_cost_enviroment'        ,'Recupera��o do Ambiente')),
      '3'=>array('fin_imp_text_3' , FWDLanguage::getPHPStringValue('db_incident_cost_direct_lost'       ,'Perda Direta'           )),
      '4'=>array('fin_imp_text_4' , FWDLanguage::getPHPStringValue('db_incident_cost_indirect_lost'     ,'Perda Indireta'         ))
    );

    /*configura��o padr�o das politicas de senha*/
    $this->caISMSDefaultConfigPasswdPolicy = array(
      'char_num'=>array(PASSWORD_CHAR_NUM ,5 ),
      /*abilita a quantidade de caracteres especiais*/
      'password1'=>array('' , 0),
      'special_chars_num'=>array(PASSWORD_SPECIAL_CHARS , 0),
      /*abilita caracteres mai�sculos / min�sculos*/
      'password2'=>array(PASSWORD_CASE_CHARS , 0),
      /*qde de caracteres num�ricos*/
      'password3'=>array('' , 0),
      'numeric_chars_num'=>array(PASSWORD_NUMERIC_CHARS , 0),
      'block'=>array(PASSWORD_BLOCK_TRIES , 3),
      'history'=>array(PASSWORD_HISTORY_NUM , 5),
      'frequency'=>array(PASSWORD_CHANGE_FREQUENCY , 365),
      'days_before_warning'=>array(PASSWORD_DAYS_BEFORE_WARNING , 7)
    );

    /*configura��o padr�o de tipos / prioridades dos costextos do isms*/
    $this->caISMSDefaultConfigParametrization = array(
      'cl_type_area1'=>array('cl_type_area1' , FWDLanguage::getPHPStringValue('db_area_type_matrix'         ,'Sede'               )),
      'cl_type_area2'=>array('cl_type_area2' , FWDLanguage::getPHPStringValue('db_area_type_filial'         ,'Subsidi�rio'        )),
      'cl_type_area3'=>array('cl_type_area3' , FWDLanguage::getPHPStringValue('db_area_type_regional_office','Escrit�rio Regional')),
      'cl_type_area4'=>array('cl_type_area4' , FWDLanguage::getPHPStringValue('db_area_type_factory_unit'   ,'F�brica'            )),
      'cl_prio_area1'=>array('cl_prio_area1' , FWDLanguage::getPHPStringValue('db_area_priority_high'       ,'3 - Alto'           )),
      'cl_prio_area2'=>array('cl_prio_area2' , FWDLanguage::getPHPStringValue('db_area_priority_middle'     ,'2 - M�dio'          )),
      'cl_prio_area3'=>array('cl_prio_area3' , FWDLanguage::getPHPStringValue('db_area_priority_low'        ,'1 - Baixo'          )),
      
      'cl_type_process1'=>array('cl_type_process1' , FWDLanguage::getPHPStringValue('db_process_type_administrative'  ,'Mercado & consumidores'         )),
      'cl_type_process2'=>array('cl_type_process2' , FWDLanguage::getPHPStringValue('db_process_type_operational'     ,'Desenvolvimento de novo produto')),
      'cl_type_process3'=>array('cl_type_process3' , FWDLanguage::getPHPStringValue('db_process_type_support'         ,'Produ��o & entrega'             )),
      'cl_type_process4'=>array('cl_type_process4' , FWDLanguage::getPHPStringValue('db_process_type_4'               ,'Recursos humanos'               )),
      'cl_type_process5'=>array('cl_type_process5' , FWDLanguage::getPHPStringValue('db_process_type_5'               ,'Tecnologia da informa��o'       )),
      'cl_type_process6'=>array('cl_type_process6' , FWDLanguage::getPHPStringValue('db_process_type_6'               ,'Finan�as & contabilidade'       )),
      'cl_type_process7'=>array('cl_type_process7' , FWDLanguage::getPHPStringValue('db_process_type_7'               ,'Ambiente, sa�de & seguran�a'    )),
      'cl_type_process8'=>array('cl_type_process8' , FWDLanguage::getPHPStringValue('db_process_type_8'               ,'Melhoria de performance'        )),
      'cl_prio_process1'=>array('cl_prio_process1' , FWDLanguage::getPHPStringValue('db_process_priority_high'        ,'3 - Alto'                       )),
      'cl_prio_process2'=>array('cl_prio_process2' , FWDLanguage::getPHPStringValue('db_process_priority_middle'      ,'2 - M�dio'                      )),
      'cl_prio_process3'=>array('cl_prio_process3' , FWDLanguage::getPHPStringValue('db_process_priority_low'         ,'1 - Baixo'                      )),

      'cl_type_risk1'=>array('cl_type_risk1' ,FWDLanguage::getPHPStringValue('db_risk_type_vulnerability'          ,'Vulnerabilidade'                  )),
      'cl_type_risk2'=>array('cl_type_risk2' ,FWDLanguage::getPHPStringValue('db_risk_type_threat'                 ,'Amea�a'                           )),
      'cl_type_risk3'=>array('cl_type_risk3' ,FWDLanguage::getPHPStringValue('db_risk_type_vulnerability_x_threat' ,'Evento (Vulnerabilidade x Amea�a)')),

      'cl_type_event1'=>array('cl_type_event1' ,FWDLanguage::getPHPStringValue('db_risk_type_fisic'          ,'F�sico'        )),
      'cl_type_event2'=>array('cl_type_event2' ,FWDLanguage::getPHPStringValue('db_risk_type_tecnologic'     ,'Tecnol�gico'   )),
      'cl_type_event3'=>array('cl_type_event3' ,FWDLanguage::getPHPStringValue('db_risk_type_administrative' ,'Administrativo')),

      'cl_type_control1'=>array('cl_type_control1' ,FWDLanguage::getPHPStringValue('db_control_type_based_on_risk_analize'     ,'Baseado em An�lise de Risco')),
      'cl_type_control2'=>array('cl_type_control2' ,FWDLanguage::getPHPStringValue('db_control_type_laws_and_regulamentation'  ,'Quest�es de Compliance'     )),
      'cl_type_control3'=>array('cl_type_control3' ,FWDLanguage::getPHPStringValue('db_control_type_others'                    ,'Outros'                     )),

      'cl_type_document1'=>array('cl_type_document1' ,FWDLanguage::getPHPStringValue('db_document_type_1','Confidential' )),
      'cl_type_document2'=>array('cl_type_document2' ,FWDLanguage::getPHPStringValue('db_document_type_2','Restrito'     )),
      'cl_type_document3'=>array('cl_type_document3' ,FWDLanguage::getPHPStringValue('db_document_type_3','Institucional')),
      'cl_type_document4'=>array('cl_type_document4' ,FWDLanguage::getPHPStringValue('db_document_type_4','P�blico'      )),

      'cl_type_register1'=>array('cl_type_register1' ,FWDLanguage::getPHPStringValue('db_register_type_1','Confidencial' )),
      'cl_type_register2'=>array('cl_type_register2' ,FWDLanguage::getPHPStringValue('db_register_type_2','Restrito'     )),
      'cl_type_register3'=>array('cl_type_register3' ,FWDLanguage::getPHPStringValue('db_register_type_3','Institucional')),
      'cl_type_register4'=>array('cl_type_register4' ,FWDLanguage::getPHPStringValue('db_register_type_4','P�blico'      ))
    );
    
    
    
    
  }
  
 /**
  * Retorna o c�digo javascript que restaura  a configura��o padr�o de Tipo/Contexto.
  * 
  * <p>M�todo para retornar o c�digo javascript que restaura  a configura��o padr�o de Tipo/Contexto.</p>
  * @access public 
  * @return string c�digo javascript que restaura  a configura��o padr�o de Tipo/Contexto
  */ 
  protected function getClassificationCode(){
    $msReturn = " function setDefaultParametrization(){ ";
    foreach ($this->caISMSDefaultConfigParametrization as $miKey=>$maValue){
    	$msEscapedValue = mb_ereg_replace("'", "\'", $maValue[1]);
      $msReturn .= " if(gobi('classification_controller_{$miKey}').isChecked()){ gebi('{$miKey}').value = '".$msEscapedValue."';} ";
    }
    return $msReturn . " } ";
  }
  
 /**
  * Retorna o c�digo javascript que restaura  a configura��o padr�o de Politica de Senhas.
  * 
  * <p>M�todo para retornar o c�digo javascript que restaura  a configura��o padr�o de Politica de Senhas.</p>
  * @access public 
  * @return string c�digo javascript que restaura  a configura��o padr�o de Politica de Senhas
  */ 
  public function getPasswordPolicyCode(){
    $msReturn = " function setDefaultPasswordPolicy(){ ";
    foreach($this->caISMSDefaultConfigPasswdPolicy as $miKey=>$maValue){
      if(in_array($miKey,array('password1','password2','password3'))){
        $msReturn .= " if(gobi('password_controller_{$miKey}').isChecked()){ gfx_check('password_controller','{$miKey}'); } " ;
      }else{
        $msReturn .= "gebi('{$miKey}').value = '".$maValue[1]."';";
      }
    }
    $msReturn .= " gobi('special_chars_num').disable();";
    $msReturn .= " gobi('numeric_chars_num').disable();";
    return $msReturn . " } " ;
  }
  
 /**
  * Retorna o c�digo javascript que restaura  a configura��o padr�o do Nome dos Custos.
  * 
  * <p>M�todo para retornar o c�digo javascript que restaura  a configura��o padr�o do Nome dos Custos.</p>
  * @access public 
  * @return string c�digo javascript que restaura  a configura��o padr�o do Nome dos Custos
  */ 
  protected function getSystemControlCostNameCode(){
    $msReturn = " function setDefaultControlCostName(){ ";
    foreach ($this->caISMSDefaultConfigControlCostNames as $miKey=>$maValue){
      $msReturn .= " gebi('{$miKey}').value = '".$maValue[1]."';";
    }
    return $msReturn . " } ";
  }
  
 /**
  * Retorna o c�digo javascript que restaura  a configura��o padr�o do Nome dos Custos do m�dulo de CI.
  * 
  * <p>M�todo para retornar o c�digo javascript que restaura  a configura��o padr�o do Nome dos Custos do m�dulo de CI.</p>
  * @access public 
  * @return string c�digo javascript que restaura  a configura��o padr�o do Nome dos Custos
  */ 
  protected function getSystemCICostNameCode(){
    $msReturn = " function setDefaultCICostName(){ ";
    foreach ($this->caISMSDefaultConfigCICostNames as $miKey=>$maValue){
    	$msEscapedValue = mb_ereg_replace("'", "\'", $maValue[1]);
      $msReturn .= " if(gobi('financial_impact_controller_{$miKey}').isChecked()){ gebi('{$maValue[0]}').value = '".$msEscapedValue."';} ";
    }
    return $msReturn . " } ";
  }
  
 /**
  * Retorna o c�digo javascript que restaura  a configura��o padr�o do Valor dos par�metros do risco.
  * 
  * <p>M�todo para retornar o c�digo javascript que restaura  a configura��o padr�o do  Valor dos par�metros do risco.</p>
  * @access public 
  * @return string c�digo javascript que restaura  a configura��o padr�o do  Valor dos par�metros do risco
  */ 
  protected function getRiskValParCode(){
    $msReturn = " function setDefaultRiskParValue(){ ";
    foreach($this->caISMSDefaultConfigValParRisk as $miKey=>$maValue){
    	$msEscapedValue = mb_ereg_replace("'", "\'", $maValue[1]);
      $msReturn .= " if(gebi('{$miKey}').style.display!='none'){ gebi('{$miKey}').value = '".$msEscapedValue."'; } ";
    }
    return $msReturn . " } ";
  }
      
 /**
  * Retorna o c�digo javascript que restaura  a configura��o padr�o do Valor dos par�metros do risco na mudan�a de 3 para 5 valores.
  * 
  * <p>M�todo para retornar o c�digo javascript que restaura  a configura��o padr�o do  Valor dos par�metros do risco  na mudan�a de 3 para 5 valores.</p>
  * @access public 
  * @return string c�digo javascript que restaura  a configura��o padr�o do  Valor dos par�metros do risco
  */ 
  protected function getRiskValParCode3_to_5_Change(){
    $msReturn = " function setDefaultRiskParValue3_to_5_change(){ ";
    foreach($this->caISMSDefaultConfigValParRisk3_to_5_change as $miKey => $maValue){
    	$msEscapedValue = mb_ereg_replace("'", "\'", $maValue[1]);
      $msReturn .= " if((gebi('{$miKey}').style.display!='none')&&(gebi('{$miKey}').value!='')){ gebi('{$miKey}').value = '".$msEscapedValue."'; } ";
    } 
    return $msReturn . " } ";
  }
  
 /**
  * Retorna o c�digo javascript que restaura  a configura��o padr�o do Nome dos par�metros dos riscos.
  * 
  * <p>M�todo para retornar o c�digo javascript que restaura  a configura��o padr�o do  Valor dos par�metros do risco.</p>
  * @access public 
  * @return string c�digo javascript que restaura  a configura��o padr�o do  Valor dos par�metros do risco
  */ 
  protected function getRiskParNames(){
    $msReturn = " function setDefaultRiskParNames(){ ";
    foreach($this->caISMSDefaultConfigRiskParNames as $miKey=>$maValue){
      $msReturn .= " gebi('{$miKey}').value = '".$maValue[1]."';";
    }
    return $msReturn . " } ";
  }
  
 /**
  * Retorna o c�digo javascript que restaura  a configura��o padr�o dos elementos da nav_config.
  * 
  * <p>M�todo para retornar o c�digo javascript que restaura  a configura��o padr�o dos elementos da nav_config.</p>
  * @access public 
  * @return string c�digo javascript que restaura  a configura��o padr�o do Valor dos par�metros do risco
  */ 
  public function getJSCode(){
    return    $this->getClassificationCode() 
            . $this->getPasswordPolicyCode() 
            . $this->getSystemControlCostNameCode() 
            . $this->getSystemCICostNameCode() 
            . $this->getRiskValParCode()
            . $this->getRiskParNames()
            . $this->getRiskValParCode3_to_5_Change()
          ;
  }
}
?>