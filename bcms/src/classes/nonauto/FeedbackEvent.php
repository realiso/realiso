<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe FeedbackEvent
 *
 * <p>Evento ajax que envia um email de feedback.</p>
 * <p>Sup�e que o tipo do feedback e a sua mensagem est�o nos compos
 * 'feedback_type' e 'feedback' respectivamente.</p>
 * @package ISMS
 * @subpackage classes
 */
class FeedbackEvent extends FWDRunnable {
  public function run(){
    $miUserId = ISMSLib::getCurrentUserId();
    $moUser = new ISMSUser();
    if($moUser->fetchById($miUserId)){
      $miFeedbackType = FWDWebLib::getObject('feedback_type')->getValue();
      switch($miFeedbackType){
        case 'bug'    : $msFeedbackType = FWDLanguage::getPHPStringValue('mx_bug','Bug');                  break;
        case 'feature': $msFeedbackType = FWDLanguage::getPHPStringValue('mx_feature_request','Melhoria'); break;
        case 'content': $msFeedbackType = FWDLanguage::getPHPStringValue('mx_content_issue','Sugest�o');   break;
        default: $msFeedbackType = '';
      }
      
      $msClientAlias = ISMSSaaS::getConfig(ISMSSaaS::LICENSE_CLIENT_NAME);
      $msUserName = FWDWebLib::convertToIso($moUser->getFieldValue('user_name'),true);
      $msUserEmail = $moUser->getFieldValue('user_email');
      $msFeedback = FWDWebLib::convertToIso(FWDWebLib::getObject('feedback')->getValue(),true);
      
      ini_set('SMTP',ISMSLib::getConfigById(GENERAL_SMTP_SERVER));
      $moEmail = new FWDEmail();
      $moEmail->setSenderName(ISMSLib::getConfigById(GENERAL_SMTP_DEFAULT_MAIL_SENDER_NAME));
      $moEmail->setTo(ISMSLib::getConfigById(ERROR_REPORT_EMAIL));
      $moEmail->setFrom(ISMSLib::getConfigById(GENERAL_SMTP_DEFAULT_MAIL_SENDER));
      $moEmail->setSubject(FWDLanguage::getPHPStringValue('em_isms_feedback','Real BCMS Feedback')." ({$msFeedbackType})");
      $moEmail->setMessage(
        "<b>Feedback Type:</b> {$msFeedbackType}<br>\n"
       ."<b>Client:</b> {$msClientAlias}<br>\n"
       ."<b>User Name:</b> {$msUserName}<br>\n"
       ."<b>User e-mail:</b> {$msUserEmail}<br>\n"
       ."<b>Message:</b><br><br>{$msFeedback}<br>\n"
      );
      $moEmail->send();
    }
    echo "gebi('feedback_type').value='bug'; gebi('feedback').value=''; gebi('feedback_viewgroup').style.display='none'";
  }
}

?>