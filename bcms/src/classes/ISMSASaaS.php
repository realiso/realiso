<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSASaaS.
 *
 * <p>Implementação do suporte ao Active Directory</p>
 * @package ISMS
 * @subpackage classes
 */
class ISMSASaaS {


    protected $keyClient = null;
    protected $valuesFromSignature = null;
    protected $keySubscription = null;
    protected $valuesFromPayments = null;

    function __construct(){
        $this->keyClient = $this->getKeyClient();
        $this->keySubscription = $this->getKeySubscription();
        $this->getSignatureValues($this->keyClient, $this->keySubscription);
        $this->getPaymentsValues($this->keyClient, $this->keySubscription);
    }

    private function getSignatureValues($keyClient, $keySubscription){
        $fwdASaaS = new FWDASaaS();
        $this->valuesFromSignature = $fwdASaaS->clientGetSubscriptionFromASaaS($keyClient, $keySubscription);
    }

    private function getPaymentsValues($keyClient, $keySubscription){
        $fwdASaaS = new FWDASaaS();
        $this->valuesFromPayments = $fwdASaaS->subscriptionGetPayments($keyClient, $keySubscription);
    }

    private function getKeyClient(){
        $keyClient = "realiso_qra5bxuwyo";
        return $keyClient;
    }

    private function getKeySubscription(){
        $moConfig = new ISMSConfig();
        $keySubscription = $moConfig->getConfigFromDB(CLIENT_ID_ASAAS);
        return $keySubscription;
    }

    public function setNewCheckPointASaaS($check, $module){
        $fwdASaaS = new FWDASaaS();
        $statusNewCheckPoint = $fwdASaaS->subscriptionNewCheckPoint($this->keyClient, $this->keySubscription, $check, $module);
        return $statusNewCheckPoint;
    }

    public function getCoverInitialPayments(){
        return $this->valuesFromPayments->return->coverInitial;
    }

    public function getStatusPayment(){
        return $this->valuesFromPayments->return->status;
    }

    public function getURLPayPal($planCode, $instance, $urlSuccess, $urlCancel){
        $fwdASaaS = new FWDASaaS();
        $url = $fwdASaaS->clientGetURLPayPal($this->keyClient, $this->keySubscription, $planCode, $instance, $urlSuccess, $urlCancel);
        return $url;
    }

    public function updatePlanAccount($planCode){
        $fwdASaaS = new FWDASaaS();
        $updatedPlanAccount = $fwdASaaS->clientUpdatePlanAccount($this->keyClient, $this->keySubscription, $planCode);
        return $updatedPlanAccount;
    }

    public function updateClientActivePayPal($planCode, $payerID, $token){
        $fwdASaaS = new FWDASaaS();
        $updatedClientActivePaypal = $fwdASaaS->clientActivePayPal($this->keyClient, $this->keySubscription,  $planCode, $payerID, $token);
        return $updatedClientActivePaypal;
    }

    public function updatedClientConfirmStatusSubscription(){
        $fwdASaaS = new FWDASaaS();
        $updatedStatus = $fwdASaaS->clientConfirmStatusSubscription($this->keyClient, $this->keySubscription);
        return $updatedStatus;
    }

    public function getAccountId(){
        return $this->valuesFromSignature->return->accountId;
    }

    public function getCompanyName(){
        return $this->valuesFromSignature->return->company;
    }

    public function getEmail(){
        return $this->valuesFromSignature->return->email;
    }

    public function getExpirationDate(){
        return $this->valuesFromSignature->return->expirationDate;
    }

    public function getUserName(){
        return $this->valuesFromSignature->return->name;
    }

    public function getNextBilling(){
        return $this->valuesFromSignature->return->nextBilling;
    }

    public function getPhone(){
        return $this->valuesFromSignature->return->phone;
    }

    public function getPlanCode(){
        return $this->valuesFromSignature->return->planCode;
    }

    public function getPlanValue(){
        return $this->valuesFromSignature->return->planValue;
    }

    public function getStatus(){
        return $this->valuesFromSignature->return->status;
    }

    public function getTrialDate(){
        return $this->valuesFromSignature->return->trialDate;
    }
}
?>