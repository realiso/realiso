<?php
/*// Usado no compilado.
 $inst_dir = dirname( $_SERVER[ "SCRIPT_FILENAME" ] ) . '/' . $sys_ref;
 $handlers_ref = $inst_dir  . "handlers/";
 */
$lib_ref = $sys_ref  . "lib/";
$classes_risk_management_ref = $sys_ref  . "classes/risk_management/";
$classes_isms_ref = $sys_ref  . "classes/";
$classes_admin_ref = $sys_ref  . "classes/admin/";
$handlers_ref = $sys_ref  . "handlers/";
$report_risk_management_ref = $classes_risk_management_ref  . "report/";
$report_admin_ref = $classes_admin_ref  . "report/";
$report_ref = $sys_ref  . "reports/";
$graphs_ref = $sys_ref  . "graphs/";
$risk_param_ref = $classes_risk_management_ref  . "riskparameters/";
$classes_policy_management_ref = $sys_ref . "classes/policy_management/";
$report_policy_management_ref = $classes_policy_management_ref . "report/";
$classes_continual_improvement_ref = $sys_ref . "classes/continual_improvement/";
$report_incident_management_ref = $classes_continual_improvement_ref . "report/";
$continuity_management_ref = $sys_ref . "classes/continuity_management/";

include_once $lib_ref . "fwd5_setup.php";

include_once $classes_isms_ref . "ISMSLib.php";
include_once $classes_isms_ref . "ISMSLicense.php";
include_once $classes_isms_ref . "ISMSSession.php";
include_once $classes_isms_ref . "ISMSTable.php";
include_once $classes_isms_ref . "ISMSSaasErrorHandler.php";

include_once $classes_isms_ref . "ISMSAuditLog.php";
include_once $classes_isms_ref . "ISMSActivity.php";

include_once $classes_isms_ref . "IAssociation.php";

include_once $classes_isms_ref . "Workflow.php";

include_once $classes_isms_ref . "ISMSContextDate.php";
include_once $classes_isms_ref . "ISMSContextObject.php";
include_once $classes_isms_ref . "ISMSContext.php";
include_once $classes_isms_ref . "ISMSContextClassification.php";

include_once $classes_isms_ref . "IWorkflow.php";
include_once $classes_isms_ref . "WKFAlert.php";
include_once $classes_isms_ref . "WKFTask.php";
include_once $classes_isms_ref . "WKFTaskMetadata.php";
include_once $classes_isms_ref . "WKFSchedule.php";
include_once $classes_isms_ref . "WKFTaskSchedule.php";
include_once $classes_isms_ref . "ISMSTaskManager.php";

include_once $classes_isms_ref . "ISMSHandleJSError.php";
include_once $classes_isms_ref . "ISMSASaaS.php";

include_once $classes_risk_management_ref . "RMWorkflow.php";
include_once $classes_risk_management_ref . "WorkflowProcessAsset.php";
include_once $classes_risk_management_ref . "RiskLimitsWorkflow.php";

include_once $classes_risk_management_ref . "RMArea.php";
include_once $classes_risk_management_ref . "RMProcess.php";
include_once $classes_risk_management_ref . "RMAssetAsset.php";
include_once $classes_risk_management_ref . "RMAsset.php";
include_once $classes_risk_management_ref . "RMProcessAsset.php";
include_once $classes_risk_management_ref . "RMRisk.php";
include_once $classes_risk_management_ref . "RMAcceptRisk.php";
include_once $classes_risk_management_ref . "RMRiskControl.php";
include_once $classes_risk_management_ref . "RMRiskConfig.php";
include_once $classes_risk_management_ref . "RMRiskLimits.php";

include_once $classes_risk_management_ref . "RMCategory.php";
include_once $classes_risk_management_ref . "RMEvent.php";
include_once $classes_risk_management_ref . "RMBestPractice.php";
include_once $classes_risk_management_ref . "RMStandard.php";
include_once $classes_risk_management_ref . "RMSectionBestPractice.php";

include_once $classes_risk_management_ref . "RMControl.php";
include_once $classes_risk_management_ref . "RMControlBestPractice.php";
include_once $classes_risk_management_ref . "WKFControlEfficiency.php";
include_once $classes_risk_management_ref . "RMControlCost.php";
include_once $classes_risk_management_ref . "ControlSchedule.php";
include_once $classes_risk_management_ref . "WKFControlTest.php";
include_once $classes_risk_management_ref . "RMControlEfficiencyHistory.php";
include_once $classes_risk_management_ref . "RMControlTestHistory.php";
include_once $classes_risk_management_ref . "RMControlImplementationAccept.php";
include_once $classes_risk_management_ref . "WorkflowRiskControl.php";

include_once $classes_risk_management_ref . "RMBestPracticeStandard.php";
include_once $classes_risk_management_ref . "RMBestPracticeEvent.php";
include_once $classes_risk_management_ref . "RMAreaProvider.php";

include_once $classes_isms_ref . "ISMSXMLParser.php";
include_once $classes_isms_ref . "ISMSLibraryShowImport.php";
include_once $classes_isms_ref . "ISMSLibraryImport.php";
include_once $classes_isms_ref . "ISMSLibraryExport.php";

include_once $classes_isms_ref . "ISMSConfig.php";
include_once $classes_isms_ref . "ISMSSolicitor.php";
include_once $classes_isms_ref . "ISMSUser.php";
include_once $classes_isms_ref . "ISMSUserPasswordHistory.php";
include_once $classes_isms_ref . "ISMSProfile.php";
include_once $classes_isms_ref . "ISMSProfileAcl.php";
include_once $classes_isms_ref . "ISMSPasswordPolicy.php";
include_once $classes_isms_ref . "ISMSSyncSite.php";

include_once $classes_isms_ref . "ISMSUserPreferences.php";
include_once $classes_isms_ref . "ISMSEmailPreferences.php";
include_once $classes_isms_ref . "ISMSGridUserPreferences.php";

include_once $classes_isms_ref . "ISMSMailer.php";
include_once $classes_isms_ref . "ISMSEmailTemplates.php";

include_once $classes_isms_ref . "ISMSScope.php";
include_once $classes_isms_ref . "ISMSPolicy.php";

include_once $classes_isms_ref . "ISMSSaaS.php";
include_once $classes_isms_ref . "ISMSSaaSAdmin.php";

include_once $classes_isms_ref . "ISMSFile.php";

include_once $risk_param_ref . "RMIValue.php";
include_once $risk_param_ref . "RiskParameters.php";
include_once $risk_param_ref . "RiskParametersValue.php";
include_once $risk_param_ref . "RMAssetValue.php";
include_once $risk_param_ref . "RMAssetDependency.php";
include_once $risk_param_ref . "RMRiskValue.php";
include_once $risk_param_ref . "CIIncidentRiskValue.php";

include_once $risk_param_ref . "RMRiskControlValue.php";
include_once $risk_param_ref . "RMParameterName.php";
include_once $risk_param_ref . "RMParameterValueName.php";
include_once $risk_param_ref . "RMRCParameterValueName.php";

include_once $report_ref . "ISMSReport.php";
include_once $report_ref . "ISMSReportTemplate.php";
include_once $report_ref . "ISMSReportWindow.php";
include_once $report_ref . "ISMSReportHideLoadingEvent.php";
include_once $report_ref . "ISMSFilteredReport.php";

include_once $report_risk_management_ref . "ISMSReportGenericClassifTypePrioFilter.php";
include_once $report_risk_management_ref . "ISMSReportRiskTreatmentPlan.php";
include_once $report_risk_management_ref . "ISMSReportControlSummary.php";
include_once $report_risk_management_ref . "ISMSReportControlSummaryPM.php";
include_once $report_risk_management_ref . "ISMSReportControlAccompaniment.php";
include_once $report_risk_management_ref . "ISMSReportControlEfficiencyAccompaniment.php";
include_once $report_risk_management_ref . "ISMSReportAreasWithoutProcesses.php";
include_once $report_risk_management_ref . "ISMSReportAssetsWithoutRisks.php";
include_once $report_risk_management_ref . "ISMSReportControlsNotMeasured.php";
include_once $report_risk_management_ref . "ISMSReportRiskValues.php";
include_once $report_risk_management_ref . "ISMSReportRiskValuesFilter.php";
include_once $report_risk_management_ref . "ISMSReportBCMSFilter.php";
include_once $report_risk_management_ref . "ISMSReportRisksByControl.php";
include_once $report_risk_management_ref . "ISMSReportRisksByControlFilter.php";
include_once $report_risk_management_ref . "ISMSReportStatementOfApplicabilityFilter.php";

include_once $report_risk_management_ref . "ISMSReportUserResponsabilities.php";
include_once $report_risk_management_ref . "ISMSReportUserResponsabilitiesFilter.php";
include_once $report_risk_management_ref . "ISMSReportDetailedPendantTasks.php";
include_once $report_risk_management_ref . "ISMSReportSummarizedPendantTasks.php";
include_once $report_risk_management_ref . "ISMSReportPendantContexts.php";
include_once $report_risk_management_ref . "ISMSReportAsset.php";
include_once $report_risk_management_ref . "ISMSReportAssetFilter.php";
include_once $report_risk_management_ref . "ISMSReportProcessesWithoutAssets.php";
include_once $report_risk_management_ref . "ISMSReportRisksByAsset.php";
include_once $report_risk_management_ref . "ISMSReportRisksByAssetFilter.php";
include_once $report_risk_management_ref . "ISMSReportRisksByProcess.php";
include_once $report_risk_management_ref . "ISMSReportRisksByProcessFilter.php";

include_once $report_risk_management_ref . "ISMSReportControlsByRisk.php";
include_once $report_risk_management_ref . "ISMSReportControlsByResponsible.php";
include_once $report_risk_management_ref . "ISMSReportControlEfficiency.php";
include_once $report_risk_management_ref . "ISMSReportControlEfficiencyFilter.php";
include_once $report_risk_management_ref . "ISMSReportControlImplementationDate.php";
include_once $report_risk_management_ref . "ISMSReportControlPlanning.php";
include_once $report_risk_management_ref . "ISMSReportControlPlanningFilter.php";
include_once $report_risk_management_ref . "ISMSReportControlCostByResponsible.php";
include_once $report_risk_management_ref . "ISMSReportControlCostByAsset.php";
include_once $report_risk_management_ref . "ISMSReportControlCostByProcess.php";
include_once $report_risk_management_ref . "ISMSReportControlCostByArea.php";
include_once $report_risk_management_ref . "ISMSReportTop10AssetsWithMoreRisks.php";
include_once $report_risk_management_ref . "ISMSReportTop10AssetsWithMoreRisksFilter.php";
include_once $report_risk_management_ref . "ISMSReportControlCost.php";
include_once $report_risk_management_ref . "ISMSReportRiskAmountByProcess.php";
include_once $report_risk_management_ref . "ISMSReportRiskAmountByArea.php";
include_once $report_risk_management_ref . "ISMSReportRiskStatusByArea.php";
include_once $report_risk_management_ref . "ISMSReportRiskStatusByProcess.php";
include_once $report_risk_management_ref . "ISMSReportTop10RisksByProcess.php";
include_once $report_risk_management_ref . "ISMSReportTop10RisksByArea.php";
include_once $report_risk_management_ref . "ISMSReportStatementOfApplicability.php";
include_once $report_risk_management_ref . "ISMSReportRiskImpact.php";
include_once $report_risk_management_ref . "ISMSReportRiskImpactFilter.php";
include_once $report_risk_management_ref . "ISMSReportAssetImportance.php";
include_once $report_risk_management_ref . "ISMSReportParametersFilter.php";
include_once $report_risk_management_ref . "ISMSReportRisksByArea.php";
include_once $report_risk_management_ref . "ISMSReportRisksByAreaFilter.php";
include_once $report_risk_management_ref . "ISMSReportEventsByCategory.php";
include_once $report_risk_management_ref . "ISMSReportEventsByCategoryFilter.php";
include_once $report_risk_management_ref . "ISMSReportControlsWithoutRisks.php";
include_once $report_risk_management_ref . "ISMSReportAreasByType.php";
include_once $report_risk_management_ref . "ISMSReportAreasByPriority.php";
include_once $report_risk_management_ref . "ISMSReportProcessesByType.php";
include_once $report_risk_management_ref . "ISMSReportProcessesByPriority.php";
include_once $report_risk_management_ref . "ISMSReportRisksByType.php";
include_once $report_risk_management_ref . "ISMSReportEventsByType.php";
include_once $report_risk_management_ref . "ISMSReportControlsByType.php";
include_once $report_risk_management_ref . "ISMSReportControlTestAccompaniment.php";
include_once $report_risk_management_ref . "ISMSReportConformity.php";
include_once $report_risk_management_ref . "ISMSReportConformityFilter.php";
include_once $report_risk_management_ref . "ISMSReportAreas.php";
include_once $report_risk_management_ref . "ISMSReportAreasFilter.php";
include_once $report_risk_management_ref . "ISMSReportProcesses.php";
include_once $report_risk_management_ref . "ISMSReportProcessesFilter.php";
include_once $report_risk_management_ref . "ISMSReportAssets.php";
include_once $report_risk_management_ref . "ISMSReportAssetsFilter.php";
include_once $report_risk_management_ref . "ISMSReportRisks.php";
include_once $report_risk_management_ref . "ISMSReportRisksFilter.php";
include_once $report_risk_management_ref . "ISMSReportControls.php";
include_once $report_risk_management_ref . "ISMSReportControlsFilter.php";
include_once $report_risk_management_ref . "ISMSReportBestPractices.php";
include_once $report_risk_management_ref . "ISMSReportBestPracticesFilter.php";
include_once $report_risk_management_ref . "ISMSReportControlCostByCostCategory.php";
include_once $report_risk_management_ref . "ISMSReportControlCostByCostCategoryFilter.php";
include_once $report_risk_management_ref . "ISMSReportRiskFinancialImpact.php";
include_once $report_risk_management_ref . "ISMSReportRiskFinancialImpactFilter.php";
include_once $report_risk_management_ref . "ISMSReportAssetDependenciesAndDependents.php";
include_once $report_risk_management_ref . "ISMSReportAssetDependenciesAndDependentsFilter.php";
include_once $report_risk_management_ref . "ISMSReportControlRevisionAgendaByResponsible.php";
include_once $report_risk_management_ref . "ISMSReportControlTestAgendaByResponsible.php";
include_once $report_risk_management_ref . "ISMSReportAssetRelevance.php";
include_once $report_risk_management_ref . "ISMSReportBiaQuantitative.php";
include_once $report_risk_management_ref . "ISMSReportBiaQuantitativeFilter.php";
include_once $report_risk_management_ref . "ISMSReportBiaQualitative.php";
include_once $report_risk_management_ref . "ISMSReportBiaQualitativeFilter.php";
include_once $report_risk_management_ref . "ISMSReportASA.php";
include_once $report_risk_management_ref . "ISMSReportASAFilter.php";
include_once $report_risk_management_ref . "ISMSReportAssetByProcess.php";
include_once $report_risk_management_ref . "ISMSReportAssetByProcessFilter.php";
include_once $report_risk_management_ref . "ISMSReportTests.php";
include_once $report_risk_management_ref . "ISMSReportTestsFilter.php";
include_once $report_risk_management_ref . "ISMSReportActivities.php";
include_once $report_risk_management_ref . "ISMSReportActivitiesFilter.php";
include_once $report_risk_management_ref . "ISMSReportScope.php";
include_once $report_risk_management_ref . "ISMSReportScopeFilter.php";
include_once $report_risk_management_ref . "ISMSReportProcessByProcess.php";
include_once $report_risk_management_ref . "ISMSReportProcessByProcessFilter.php";
include_once $report_risk_management_ref . "ISMSReportProcessByAsset.php";
include_once $report_risk_management_ref . "ISMSReportProcessByAssetFilter.php";
include_once $report_risk_management_ref . "ISMSReportAssetByAsset.php";
include_once $report_risk_management_ref . "ISMSReportAssetByAssetFilter.php";
include_once $report_risk_management_ref . "ISMSReportPlaces.php";
include_once $report_risk_management_ref . "ISMSReportPlacesFilter.php";
include_once $report_risk_management_ref . "ISMSReportAssetMatrix.php";
include_once $report_risk_management_ref . "ISMSReportAssetMatrixFilter.php";
include_once $report_risk_management_ref . "ISMSReportASAByProcesses.php";
include_once $report_risk_management_ref . "ISMSReportASAByProcessesFilter.php";
include_once $report_risk_management_ref . "ISMSReportProcessesDependence.php";
include_once $report_risk_management_ref . "ISMSReportProcessesDependenceFilter.php";
include_once $report_risk_management_ref . "ISMSReportASAByAssets.php";
include_once $report_risk_management_ref . "ISMSReportASAByAssetsFilter.php";

include_once $report_admin_ref . "ISMSReportAuditTask.php";
include_once $report_admin_ref . "ISMSReportAuditTaskFilter.php";
include_once $report_admin_ref . "ISMSReportAuditAlert.php";
include_once $report_admin_ref . "ISMSReportAuditAlertFilter.php";
include_once $report_admin_ref . "ISMSReportAuditLog.php";
include_once $report_admin_ref . "ISMSReportAuditLogFilter.php";

include_once $graphs_ref  . "ISMSGraphPieChart.php";
include_once $graphs_ref  . "ISMSGraphBarChart.php";
include_once $graphs_ref  . "ISMSGraphLineChart.php";
include_once $graphs_ref  . "ISMSGraphPotentialRisk.php";
include_once $graphs_ref  . "ISMSGraphResidualRisk.php";
include_once $graphs_ref  . "ISMSGraphAssetEstimatedCost.php";
include_once $graphs_ref  . "ISMSGraphAssetCost.php";
include_once $graphs_ref  . "ISMSGraphProcessAsset.php";
include_once $graphs_ref  . "ISMSGraphAssetRisk.php";
include_once $graphs_ref  . "ISMSGraphProcessRisk.php";
include_once $graphs_ref  . "ISMSGraphRedAssetRisk.php";
include_once $graphs_ref  . "ISMSGraphRedProcessRisk.php";
include_once $graphs_ref  . "ISMSGraphTreatedRisk.php";
include_once $graphs_ref  . "ISMSGraphControlEfficiency.php";
include_once $graphs_ref  . "ISMSGraphControlTest.php";
include_once $graphs_ref  . "ISMSGraphSingleStatistic.php";
include_once $graphs_ref  . "ISMSGraphMultipleStatistics.php";
include_once $graphs_ref  . "ISMSGraphDocumentsCreated.php";
include_once $graphs_ref  . "ISMSGraphDocumentsAccessed.php";
include_once $graphs_ref  . "ISMSGraphDocumentsType.php";
include_once $graphs_ref  . "ISMSGraphDocumentsState.php";
include_once $graphs_ref  . "ISMSGraphDocumentsClassification.php";
include_once $graphs_ref  . "ISMSGraphProcessUsers.php";
include_once $graphs_ref  . "ISMSGraphProcessDocuments.php";
include_once $graphs_ref  . "ISMSGraphDocumentRegisters.php";
include_once $graphs_ref  . "ISMSGraphIncidentLossType.php";
include_once $graphs_ref  . "ISMSGraphIncidentCategory.php";
include_once $graphs_ref  . "ISMSGraphIncidentsCreated.php";
include_once $graphs_ref  . "ISMSGraphAssetIncident.php";
include_once $graphs_ref  . "ISMSGraphProcessIncident.php";
include_once $graphs_ref  . "ISMSGraphProcessNonConformity.php";
include_once $graphs_ref  . "ISMSGraphAPActionType.php";
include_once $graphs_ref  . "ISMSGraphIncidentFinancialImpact.php";

include_once $classes_policy_management_ref . "DocumentSchedule.php";
include_once $classes_policy_management_ref . "PMDocument.php";
include_once $classes_policy_management_ref . "PMDocInstance.php";
include_once $classes_policy_management_ref . "PMProcessUser.php";
include_once $classes_policy_management_ref . "PMInstanceContent.php";
include_once $classes_policy_management_ref . "PMDocReference.php";
include_once $classes_policy_management_ref . "PMDocumentReader.php";
include_once $classes_policy_management_ref . "PMInstanceComment.php";
include_once $classes_policy_management_ref . "PMDocumentApprover.php";
include_once $classes_policy_management_ref . "PMRegister.php";
include_once $classes_policy_management_ref . "PMWorkflow.php";
include_once $classes_policy_management_ref . "PMDocumentRevisionHistory.php";
include_once $classes_policy_management_ref . "PMDocRegisters.php";
include_once $classes_policy_management_ref . "PMDocReadHistory.php";
include_once $classes_policy_management_ref . "PMDocContext.php";
include_once $classes_policy_management_ref . "PMTemplate.php";
include_once $classes_policy_management_ref . "PMTemplateContent.php";
include_once $classes_policy_management_ref . "PMTemplateBestPractice.php";
include_once $classes_policy_management_ref . "PMRegisterReader.php";
include_once $classes_policy_management_ref . "PMThreatUser.php";

include_once $classes_continual_improvement_ref . "OccurrenceWorkflow.php";
include_once $classes_continual_improvement_ref . "NCWorkflow.php";
include_once $classes_continual_improvement_ref . "CIIncident.php";
include_once $classes_continual_improvement_ref . "CIIncidentUser.php";
include_once $classes_continual_improvement_ref . "CICategory.php";
include_once $classes_continual_improvement_ref . "CISolution.php";
include_once $classes_continual_improvement_ref . "CIOccurrence.php";
include_once $classes_continual_improvement_ref . "CINonConformity.php";
include_once $classes_continual_improvement_ref . "CINonConformityProcess.php";
include_once $classes_continual_improvement_ref . "CINonConformitySeed.php";
include_once $classes_continual_improvement_ref . "CIRiskProbability.php";
include_once $classes_continual_improvement_ref . "CIRiskSchedule.php";
include_once $classes_continual_improvement_ref . "CIIncidentRisk.php";
include_once $classes_continual_improvement_ref . "CIIncidentRiskParameter.php";
include_once $classes_continual_improvement_ref . "CIIncidentControl.php";
include_once $classes_continual_improvement_ref . "CIIncidentProcess.php";
include_once $classes_continual_improvement_ref . "CIIncidentFinancialImpact.php";
include_once $classes_continual_improvement_ref . "ImprovementSchedule.php";
include_once $classes_continual_improvement_ref . "IncidentWorkflow.php";
include_once $classes_continual_improvement_ref . "CIActionPlan.php";
include_once $classes_continual_improvement_ref . "CINcActionPlan.php";
include_once $classes_continual_improvement_ref . "ActionPlanWorkflow.php";


include_once $report_policy_management_ref . "ISMSReportAccessedDocuments.php";
include_once $report_policy_management_ref . "ISMSReportAccessedDocumentsFilter.php";
include_once $report_policy_management_ref . "ISMSReportDocumentAccesses.php";
include_once $report_policy_management_ref . "ISMSReportDocumentAccessesFilter.php";
include_once $report_policy_management_ref . "ISMSReportDocumentRegisters.php";
include_once $report_policy_management_ref . "ISMSReportDocumentRegistersFilter.php";
include_once $report_policy_management_ref . "ISMSReportDocumentApprovers.php";
include_once $report_policy_management_ref . "ISMSReportDocumentApproversFilter.php";
include_once $report_policy_management_ref . "ISMSReportDocumentReaders.php";
include_once $report_policy_management_ref . "ISMSReportDocumentReadersFilter.php";
include_once $report_policy_management_ref . "ISMSReportTopRevisedDocuments.php";
include_once $report_policy_management_ref . "ISMSReportTopRevisedDocumentsFilter.php";
include_once $report_policy_management_ref . "ISMSReportTopAccessedDocuments.php";
include_once $report_policy_management_ref . "ISMSReportTopAccessedDocumentsFilter.php";
include_once $report_policy_management_ref . "ISMSReportNotAccessedDocuments.php";
include_once $report_policy_management_ref . "ISMSReportNotAccessedDocumentsFilter.php";
include_once $report_policy_management_ref . "ISMSReportDocumentsPendentReads.php";
include_once $report_policy_management_ref . "ISMSReportDocumentsPendentReadsFilter.php";
include_once $report_policy_management_ref . "ISMSReportContextsWithoutDocuments.php";
include_once $report_policy_management_ref . "ISMSReportUsersByProcess.php";
include_once $report_policy_management_ref . "ISMSReportUsersByProcessFilter.php";
include_once $report_policy_management_ref . "ISMSReportDocumentDates.php";
include_once $report_policy_management_ref . "ISMSReportDocumentDatesFilter.php";
include_once $report_policy_management_ref . "ISMSReportDocumentsRevisionSchedule.php";
include_once $report_policy_management_ref . "ISMSReportDocumentsRevisionLate.php";
include_once $report_policy_management_ref . "ISMSReportDocInstanceCommentAndJustification.php";
include_once $report_policy_management_ref . "ISMSReportDocInstanceCommentAndJustificationFilter.php";
include_once $report_policy_management_ref . "ISMSReportUserComments.php";
include_once $report_policy_management_ref . "ISMSReportUserCommentsFilter.php";
include_once $report_policy_management_ref . "ISMSReportDocumentsByArea.php";
include_once $report_policy_management_ref . "ISMSReportDocumentsByAreaFilter.php";
include_once $report_policy_management_ref . "ISMSReportDocumentSummary.php";
include_once $report_policy_management_ref . "ISMSReportDocumentSummaryFilter.php";
include_once $report_policy_management_ref . "ISMSReportRegistersByType.php";
include_once $report_policy_management_ref . "ISMSReportDocumentsByType.php";
include_once $report_policy_management_ref . "ISMSReportElementClassificationFilter.php";
include_once $report_policy_management_ref . "ISMSReportDetailedPendantTasksPM.php";
include_once $report_policy_management_ref . "ISMSReportSummarizedPendantTasksPM.php";
include_once $report_policy_management_ref . "ISMSReportDocumentsWithoutRegister.php";
include_once $report_policy_management_ref . "ISMSReportDocumentsWithoutRegisterFilter.php";
include_once $report_policy_management_ref . "ISMSReportUsersWithPendantRead.php";
include_once $report_policy_management_ref . "ISMSReportUsersWithPendantReadFilter.php";
include_once $report_policy_management_ref . "ISMSReportDocsWithHighFrequencyRevision.php";
include_once $report_policy_management_ref . "ISMSReportDocsWithHighFrequencyRevisionFilter.php";
include_once $report_policy_management_ref . "ISMSReportPendantApprovals.php";
include_once $report_policy_management_ref . "ISMSReportDocumentsByComponent.php";
include_once $report_policy_management_ref . "ISMSReportDocumentsByComponentFilter.php";
include_once $report_policy_management_ref . "ISMSReportDocumentsByState.php";
include_once $report_policy_management_ref . "ISMSReportDocumentsByStateFilter.php";

include_once $report_incident_management_ref . "ISMSReportOccurrenceByIncident.php";
include_once $report_incident_management_ref . "ISMSReportUserByIncident.php";
include_once $report_incident_management_ref . "ISMSReportIncidentByResponsible.php";
include_once $report_incident_management_ref . "ISMSReportOccurrenceAccompaniment.php";
include_once $report_incident_management_ref . "ISMSReportIncidentAccompaniment.php";
include_once $report_incident_management_ref . "ISMSReportIncidentFilter.php";
include_once $report_incident_management_ref . "ISMSReportOccurrenceFilter.php";
include_once $report_incident_management_ref . "ISMSReportNonConformityFilter.php";
include_once $report_incident_management_ref . "ISMSReportNonConformityByProcess.php";
include_once $report_incident_management_ref . "ISMSReportNonConformityAccompaniment.php";
include_once $report_incident_management_ref . "ISMSReportControlNonConformityByProcess.php";
include_once $report_incident_management_ref . "ISMSReportFinancialImpactByIncident.php";
include_once $report_incident_management_ref . "ISMSReportIncidentControlRevision.php";
include_once $report_incident_management_ref . "ISMSReportElementsAffectedByIncident.php";
include_once $report_incident_management_ref . "ISMSReportElementsFilter.php";
include_once $report_incident_management_ref . "ISMSReportRiskAutoProbabilityValue.php";
include_once $report_incident_management_ref . "ISMSReportDetailedPendantTasksCI.php";
include_once $report_incident_management_ref . "ISMSReportSummarizedPendantTasksCI.php";
include_once $report_incident_management_ref . "ISMSReportIncidentWithoutRisk.php";
include_once $report_incident_management_ref . "ISMSReportNCWithoutAP.php";
include_once $report_incident_management_ref . "ISMSReportAPWithoutDoc.php";
include_once $report_incident_management_ref . "ISMSReportActionPlanByUser.php";
include_once $report_incident_management_ref . "ISMSReportActionPlanByProcess.php";
include_once $report_incident_management_ref . "ISMSReportDuplicatedRisksByAsset.php";
include_once $report_incident_management_ref . "ISMSReportDuplicatedRisks.php";
include_once $report_incident_management_ref . "ISMSReportIncidentWithoutOccurrence.php";
include_once $report_incident_management_ref . "ISMSReportIncidents.php";
include_once $report_incident_management_ref . "ISMSReportIncidentsFilter.php";
include_once $report_incident_management_ref . "ISMSReportNonConformities.php";
include_once $report_incident_management_ref . "ISMSReportNonConformitiesFilter.php";
include_once $report_incident_management_ref . "ISMSReportDisciplinaryProcess.php";
include_once $report_incident_management_ref . "ISMSReportDisciplinaryProcessFilter.php";

include_once $continuity_management_ref . "CMProcessInput.php";
include_once $continuity_management_ref . "CMProcessOutput.php";
include_once $continuity_management_ref . "CMPlace.php";
include_once $continuity_management_ref . "CMProvider.php";
include_once $continuity_management_ref . "CMProviderResource.php";
include_once $continuity_management_ref . "CMPlan.php";
include_once $continuity_management_ref . "CMPlanRange.php";
include_once $continuity_management_ref . "CMPlanRangeType.php";
include_once $continuity_management_ref . "CMDepartment.php";
include_once $continuity_management_ref . "CMPlanAction.php";
include_once $continuity_management_ref . "CMPlanActionTest.php";
include_once $continuity_management_ref . "CMPlanTest.php";
include_once $continuity_management_ref . "CMPlanSchedule.php";
include_once $continuity_management_ref . "CMScope.php";
include_once $continuity_management_ref . "CMScopePlace.php";
include_once $continuity_management_ref . "CMScopeArea.php";
include_once $continuity_management_ref . "CMScopeAsset.php";
include_once $continuity_management_ref . "CMScopeProcess.php";
include_once $continuity_management_ref . "CMPlaceProcess.php";
include_once $continuity_management_ref . "CMPlaceAsset.php";
include_once $continuity_management_ref . "CMPlaceThreat.php";
include_once $continuity_management_ref . "CMThreatVulnerability.php";
include_once $continuity_management_ref . "CMScene.php";
include_once $continuity_management_ref . "CMImpact.php";
include_once $continuity_management_ref . "CMImpactDamage.php";
include_once $continuity_management_ref . "CMImpactDamageRange.php";
include_once $continuity_management_ref . "CMDamageMatrix.php";
include_once $continuity_management_ref . "CMImpactType.php";
include_once $continuity_management_ref . "CMPriority.php";
include_once $continuity_management_ref . "CMFinancialImpact.php";
include_once $continuity_management_ref . "CMDamageMatrixParameter.php";
include_once $continuity_management_ref . "CMProcessActivity.php";
include_once $continuity_management_ref . "CMProcessActivityAsset.php";
include_once $continuity_management_ref . "CMProcessActivityPeople.php";
include_once $continuity_management_ref . "CMProcessThreat.php";
include_once $continuity_management_ref . "CMProcessCategory.php";
include_once $continuity_management_ref . "CMProcessCategoryThreat.php";
include_once $continuity_management_ref . "CMPlaceCategory.php";
include_once $continuity_management_ref . "CMPlaceCategoryThreat.php";
include_once $continuity_management_ref . "CMAssetCategory.php";
include_once $continuity_management_ref . "CMAssetCategoryThreat.php";
include_once $continuity_management_ref . "CMAssetThreat.php";
include_once $continuity_management_ref . "CMThreat.php";
include_once $continuity_management_ref . "CMThreatProbability.php";
include_once $continuity_management_ref . "CMGroup.php";
include_once $continuity_management_ref . "CMResource.php";
include_once $continuity_management_ref . "CMPlaceResource.php";
include_once $continuity_management_ref . "CMPlaceProvider.php";
include_once $continuity_management_ref . "CMAreaResource.php";
include_once $continuity_management_ref . "CMGroupResource.php";

/* //Usado no compilado.
 $moConfig = new FWDConfig($inst_dir . '../config.php');
 */
$moConfig = new FWDConfig($classes_isms_ref . 'config.php');
if (!$moConfig->load()) {
	trigger_error('error_config_corrupt',E_USER_ERROR);
}

$soWebLib = FWDWebLib::getInstance();
$soWebLib->setCSSRef($sys_ref . "lib/");
$soWebLib->setSysRef($sys_ref);
$soWebLib->setGfxRef($sys_ref . "gfx/");
$soWebLib->setSysJsRef($sys_ref. "lib/");
$soWebLib->setSysRefBasedOnTabMain($sys_ref_based_on_tab_main);


 FWDLanguage::getPHPStringValue('mx_shortdate_format', "%d/%m/%Y"); 
  
  $ssDir = ISMSLib::isCompiled() ? dirname($_SERVER[ "SCRIPT_FILENAME" ]) . '/' . $sys_ref : $sys_ref;

if (in_array(ISMSSaaS::getConfig(ISMSSaaS::LICENSE_TYPE), ISMSLicense::getEMSLicenseTypes())) {
    FWDLanguage::addLanguage(LANGUAGE_PORTUGUESE_EMS,FWDLanguage::getPHPStringValue('mx_portuguese', "Portugu�s"),$ssDir.'language/portuguese_ems.php');
    //FWDLanguage::addLanguage(LANGUAGE_ENGLISH_EMS,FWDLanguage::getPHPStringValue('mx_english', "Ingl�s"),$ssDir.'language/english_ems.php');
    FWDLanguage::selectLanguage(LANGUAGE_PORTUGUESE_EMS);
    FWDLanguage::addDefaultLanguage(LANGUAGE_PORTUGUESE_ISMS); // cuidado ao alterar essa linha!
  }
  else if (in_array(ISMSSaaS::getConfig(ISMSSaaS::LICENSE_TYPE), ISMSLicense::getOHSLicenseTypes())) {
    FWDLanguage::addLanguage(LANGUAGE_PORTUGUESE_OHS,FWDLanguage::getPHPStringValue('mx_portuguese', "Portugu�s"),$ssDir.'language/portuguese_ohs.php');
    //FWDLanguage::addLanguage(LANGUAGE_ENGLISH_OHS,FWDLanguage::getPHPStringValue('mx_english', "Ingl�s"),$ssDir.'language/english_ohs.php');
    FWDLanguage::selectLanguage(LANGUAGE_PORTUGUESE_OHS);
    FWDLanguage::addDefaultLanguage(LANGUAGE_PORTUGUESE_ISMS); // cuidado ao alterar essa linha!
  }
  else if (in_array(ISMSSaaS::getConfig(ISMSSaaS::LICENSE_TYPE), ISMSLicense::getSOXLicenseTypes())) {
/*    FWDLanguage::addLanguage(LANGUAGE_ENGLISH_SOX,FWDLanguage::getPHPStringValue('mx_english', "Ingl�s"),$ssDir.'language/english_sox.php');
    FWDLanguage::selectLanguage(LANGUAGE_ENGLISH_SOX);
    FWDLanguage::addDefaultLanguage(LANGUAGE_PORTUGUESE_ISMS); // cuidado ao alterar essa linha!*/
    FWDLanguage::addLanguage(LANGUAGE_ENGLISH_SOX,FWDLanguage::getPHPStringValue('mx_english', "English"),$ssDir.'language/english_sox.php');
    FWDLanguage::addLanguage(LANGUAGE_FRENCH_SOX,FWDLanguage::getPHPStringValue('mx_french', "Fran�ais"),$ssDir.'language/french_sox.php');
    FWDLanguage::addLanguage(LANGUAGE_GERMAN_SOX,FWDLanguage::getPHPStringValue('mx_german', "Deutsch"),$ssDir.'language/german_sox.php');
    FWDLanguage::selectLanguage(LANGUAGE_ENGLISH_SOX);
    FWDLanguage::addDefaultLanguage(LANGUAGE_PORTUGUESE_ISMS); // cuidado ao alterar essa linha!
  }
  else if (in_array(ISMSSaaS::getConfig(ISMSSaaS::LICENSE_TYPE), ISMSLicense::getPCILicenseTypes())) {
    FWDLanguage::addLanguage(LANGUAGE_ENGLISH_PCI,FWDLanguage::getPHPStringValue('mx_english', "Ingl�s"),$ssDir.'language/english_pci.php');
    FWDLanguage::selectLanguage(LANGUAGE_ENGLISH_PCI);
    FWDLanguage::addDefaultLanguage(LANGUAGE_PORTUGUESE_ISMS); // cuidado ao alterar essa linha!
  }
  else if (in_array(ISMSSaaS::getConfig(ISMSSaaS::LICENSE_TYPE), ISMSLicense::getTHREELicenseTypes())) {
    FWDLanguage::addLanguage(LANGUAGE_PORTUGUESE_THREE,FWDLanguage::getPHPStringValue('mx_portuguese', "Portugu�s"),$ssDir.'language/portuguese_3380.php');
    FWDLanguage::selectLanguage(LANGUAGE_PORTUGUESE_THREE);
    FWDLanguage::addDefaultLanguage(LANGUAGE_PORTUGUESE_ISMS); // cuidado ao alterar essa linha!
  }  
  else {
    FWDLanguage::addLanguage(LANGUAGE_PORTUGUESE_ISMS,FWDLanguage::getPHPStringValue('mx_portuguese', "Portugu�s"),$ssDir.'language/portuguese_isms.php');
    FWDLanguage::addLanguage(LANGUAGE_ENGLISH_ISMS,FWDLanguage::getPHPStringValue('mx_english', "Ingl�s"),$ssDir.'language/english_isms.php');
    FWDLanguage::addLanguage(LANGUAGE_SPANISH_ISMS,FWDLanguage::getPHPStringValue('mx_spanish', "Espa�ol (Beta!)"),$ssDir.'language/spanish_isms.php');    
//FWDLanguage::addLanguage(LANGUAGE_CHINESE,FWDLanguage::getPHPStringValue('mx_chinese', "Chin�s (Beta)"),$ssDir.'language/chinese_isms.php');    
    FWDLanguage::selectLanguage(LANGUAGE_ENGLISH_ISMS);
    FWDLanguage::addDefaultLanguage(LANGUAGE_PORTUGUESE_ISMS); // cuidado ao alterar essa linha!
  }
 
  ISMSLib::getInstance()->ISMS_INIT();   
  
  FWDWebLib::setErrorHandler(new ISMSSaasErrorHandler());
  FWDStartEvent::getInstance()->addAjaxEvent(new ISMSHandleJSError('isms_handle_js_error'));

?>