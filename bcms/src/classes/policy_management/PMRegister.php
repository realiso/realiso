<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

include_once $handlers_ref . "QueryRegisterCanEdit.php";
include_once $handlers_ref . "QueryRegisterCanRead.php";

/**
 * Classe PMRegister
 *
 * <p>Classe que representa a tabela de registros.</p>
 * @package ISMS
 * @subpackage classes
 */
class PMRegister extends ISMSContext {

  const TIME_INTERVAL_DAYS = 7801;
  const TIME_INTERVAL_WEEKS = 7802;
  const TIME_INTERVAL_MONTHS = 7803;
  
 /**
  * Construtor.
  *
  * <p>Construtor da classe PMRegister.</p>
  * @access public
  */
  public function __construct(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    parent::__construct('pm_register');
    $this->csAliasId = 'register_id';
    $this->ciContextType = CONTEXT_REGISTER;  

    $this->coDataset->addFWDDBField(new FWDDBField('fkContext',                 'register_id',                        DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('fkDocument',                'document_id',                        DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('fkClassification',          'register_classification',            DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('nPeriod',                   'register_period',                    DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('nValue',                    'register_value',                     DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('sStoragePlace',             'register_storage_place',             DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField('sStorageType',              'register_storage_type',              DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField('sName',                     'register_name',                      DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField('fkResponsible',             'register_responsible',               DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('sIndexingType',             'register_indexing_type',             DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField('sDisposition',              'register_disposition',               DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField('sProtectionRequirements',   'register_prot_requirements',         DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField('sOrigin',                   'register_origin',                    DB_STRING));
  }
 
 /**
  * Retorna o nome do contexto.
  * 
  * <p>M�todo para retornar o nome do contexto.</p>
  * @access public 
  * @return string Nome do contexto
  */
  public function getName(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    return $this->getFieldValue('register_name');
  }
  
  /**
  * Retorna o label do contexto.
  * 
  * <p>M�todo para retornar o label do contexto.</p>
  * @access public 
  * @return string Label do contexto
  */
  public function getLabel(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    return FWDLanguage::getPHPStringValue('mx_register', "Registro");
  }
  
  /**
  * Retorna o id do usu�rio respons�vel pelo contexto.
  *
  * <p>M�todo para retornar id do usu�rio respons�vel pelo contexto.</p>
  * @access public
  * @return integer Id do respons�vel
  */
  public function getResponsible(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    return $this->getFieldValue('register_responsible');
  }

 /**
  * Retorna o tempo de reten��o do registro como uma string.
  *
  * <p>Retorna o tempo de reten��o do registro como uma string.</p>
  * @access public
  * @param $piValue N�mero de unidades do per�odo (opcional)
  * @param $piPeriod C�digo da unidade de per�odo (opcional)
  * @return string O tempo de reten��o do registro como uma string
  */
  public function getRetentionTimeAsString($piValue=-1,$piPeriod=-1){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $miValue = ($piValue==-1?$this->getFieldValue('register_value'):$piValue);
    $miPeriod = ($piPeriod==-1?$this->getFieldValue('register_period'):$piPeriod);
    switch($miPeriod){
      case self::TIME_INTERVAL_DAYS:{
        return $miValue.' '.FWDLanguage::getPHPStringValue('mx_day_days','dia(s)');
      }case self::TIME_INTERVAL_WEEKS:{
        return $miValue.' '.FWDLanguage::getPHPStringValue('mx_week_weeks','semana(s)');
      }case self::TIME_INTERVAL_MONTHS:{
        return $miValue.' '.FWDLanguage::getPHPStringValue('mx_month_months','m�s(es)');
      }
    }
  }

  protected function userCanInsert(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    return true;
  }
  
  public function userCanEdit($piRegisterId, $piResponsible = 0){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

    $moHandler = new QueryRegisterCanEdit(FWDWebLib::getConnection());
    $moHandler->setRegisterId($piRegisterId);
    $moHandler->makeQuery();
    $moHandler->executeQuery();
    return $moHandler->fetch();
  }

  protected function userCanDelete($piRegisterId){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    return $this->userCanEdit($piRegisterId);
  }

  protected function userCanRead($piRegisterId){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    $moHandler = new QueryRegisterCanRead(FWDWebLib::getConnection());
    $moHandler->setRegisterId($piRegisterId);
    $moHandler->makeQuery();
    $moHandler->executeQuery();
    return $moHandler->fetch();
  }

 /**
  * Retorna o caminho para abrir a popup de edi��o desse contexto.
  * 
  * <p>M�todo para retornar o caminho para abrir a popup de edi��o desse contexto.</p>
  * @param integer $piContextId id do contexto
  * @access public 
  * @return string caminho para abrir a popup de edi��o desse contexto
  */ 
  public function getVisualizeEditEvent($piContextId,$psUniqId){
    $soSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    $msPackagesPolicyPath = ($soSession->getMode()==MANAGER_MODE) ? "packages/policy/" : "../../packages/policy/";
    $msPopupId = "popup_document_register_edit";
    
    return "isms_open_popup('{$msPopupId}','{$msPackagesPolicyPath}{$msPopupId}.php?register=$piContextId','','true');"
           ."soPopUpManager.getPopUpById('{$msPopupId}').setCloseEvent( function close_visualize_$psUniqId() {soPopUpManager.closePopUp('popup_visualize_$psUniqId');} );";
  }
  
  /**
  * Deleta logicamente um contexto.
  * 
  * <p>M�todo para deletar logicamente um contexto.</p>
  * @access public
  * @param integer $piContextId id do contexto
  */ 
  public function logicalDelete($piContextId) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

    $moDocReg = new PMDocRegisters();
    $moDocReg->createFilter($piContextId, 'register_id');
    $moDocReg->select();
    while ($moDocReg->fetch()) {
      // id do documento
      $miDocId = $moDocReg->getFieldValue('document_id');
      
      // deleta a rela��o entre o registro e documento
      $moDocRegDel = new PMDocRegisters();
      $moDocRegDel->createFilter($piContextId, 'register_id');
      $moDocRegDel->createFilter($miDocId, 'document_id');
      $moDocRegDel->delete();

      // seta o tipo do documento para 'sem tipo'
      $moDoc = new PMDocument();
      $moDoc->setFieldValue('document_type', CONTEXT_NONE);
      $moDoc->update($miDocId);
      
      // deleta o documento
      $moDoc = new PMDocument();
      $moDoc->delete($miDocId);
    }
    
    // seta o chave do documento para null
    $moReg = new PMRegister();
    $moReg->setFieldValue('document_id', 'null');
    $moReg->update($piContextId);
  }
}
?>