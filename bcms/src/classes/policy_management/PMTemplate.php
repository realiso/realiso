<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

  include_once $handlers_ref . "QuerySearchTemplate.php";
/**
 * Classe PMTemplate
 *
 * <p>Classe que representa a tabela de templates de documentos</p>
 * @package ISMS
 * @subpackage classes
 */
class PMTemplate extends ISMSContext implements IWorkflow {

 /**
  * Construtor.
  *
  * <p>Construtor da classe PMRegister.</p>
  * @access public
  */
  public function __construct($pbGetContent = false){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    if($pbGetContent){
      parent::__construct('view_pm_tp_active_with_content');
      $this->ciContextType = CONTEXT_DOCUMENT_TEMPLATE_WITH_CONTENT;
    }else{
      parent::__construct('pm_template');
      $this->ciContextType = CONTEXT_DOCUMENT_TEMPLATE;
    }
    $this->csAliasId = 'template_id';

    $this->coWorkflow = new RMWorkflow(ACT_DOCUMENT_TEMPLATE_APPROVAL);

    $this->caSearchableFields = array('template_name', 'template_description', 'template_key_words');

    $this->coDataset->addFWDDBField(new FWDDBField('fkContext'    ,'template_id'            ,DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('sname'        ,'template_name'          ,DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField('nContextType' ,'template_type'          ,DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('sPath'        ,'template_path'          ,DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField('sFileName'    ,'template_file_name'     ,DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField('tDescription' ,'template_description'   ,DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField('sKeyWords'    ,'template_key_words'     ,DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField('nFileSize'    ,'template_file_size'     ,DB_NUMBER));
    if($pbGetContent){
      $this->coDataset->addFWDDBField(new FWDDBField('tContent'     ,'template_content'     ,DB_STRING));
      $this->caSearchableFields[] =  'template_content';
    }
  }

 /**
  * Retorna o nome do contexto.
  * 
  * <p>M�todo para retornar o nome do contexto.</p>
  * @access public 
  * @return string Nome do contexto
  */
  public function getName(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    return $this->getFieldValue('template_name');
  }
  
  /**
  * Retorna o label do contexto.
  * 
  * <p>M�todo para retornar o label do contexto.</p>
  * @access public 
  * @return string Label do contexto
  */
  public function getLabel(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    return FWDLanguage::getPHPStringValue('mx_document_template', "Modelo de Documento");
  }
  
  /**
  * Retorna o id do usu�rio respons�vel pelo contexto.
  *
  * <p>M�todo para retornar id do usu�rio respons�vel pelo contexto.</p>
  * @access public
  * @return integer Id do respons�vel
  */
  public function getResponsible(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $moConfig = new ISMSConfig();
    
    return $moConfig->getConfig(USER_LIBRARIAN);
  }
  
  /**
  * Retorna o id do usu�rio aprovador do contexto.
  *
  * <p>M�todo para retornar id do usu�rio que deve aprovar o contexto.</p>
  * @access public
  * @return integer Id do respons�vel
  */
  public function getApprover(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $moConfig = new ISMSConfig();
    
    return $moConfig->getConfig(USER_LIBRARIAN);
  }

/**
  * Deleta um contexto.
  * 
  * <p>M�todo para deletar um contexto.
  * N�o deleta do banco. Somente troca seu estado para deletado.</p>
  * @access public
  * @param integer $piContextId Id do contexto
  * @param boolean $pbDeleteDB Remove do banco
  * @param boolean $pbExecByTrash Delete chamado pela Lixeira do sistema 
  */
  public function delete($piContextId, $pbDeleteDB = false,$pbExecByTrash = false){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    // necessita pegar os dados do objeto para criar o log
    if($this->fetchById($piContextId)){
      if($pbDeleteDB && $pbExecByTrash && $piContextId){
        $moContext = new PMTemplate();
        $moContext->createFilter($piContextId,'template_id');
        $moContext->select();
        while($moContext->fetch()) {
          $msPath = $moContext->getFieldValue('template_path');
          if ($msPath && is_file($msPath))
            unlink($msPath);
        }
      }
      parent::delete($piContextId, $pbDeleteDB,$pbExecByTrash);
    }
  }

  public function getSearchHandler(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $moHandler = new QuerySearchTemplate(FWDWebLib::getConnection());
    return $moHandler;
  }

 /**
  * Retorna o �cone do template de documento.
  *
  * <p>M�todo para retornar o �cone do template de documento.</p>
  * @access public
  * @param integer $piContextId Id do contexto
  * @return string Nome do �cone
  */
  public function getIcon($piContextId = 0){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    if($piContextId){
      $moTemplate = new PMTemplate();
      $moTemplate->fetchById($piContextId);
      $msIcon = $moTemplate->getFieldValue('template_file_name');
    }else{
      $msIcon = $this->getFieldValue('template_file_name');
    }
    $msIconName = ISMSLib::getIcon($msIcon);
    return $msIconName;
  }

 /**
  * Retorna o caminho do contexto.
  * 
  * <p>M�todo para retornar o caminho do contexto.</p>
  * @access public 
  * @return string Caminho do contexto
  */ 
  public function getSystemPath() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

    $msRMPath = "<a href='javascript:isms_redirect_to_mode(" . LIBRARIES_MODE . ",0,0)'>".FWDLanguage::getPHPStringValue('st_libraries_module_name','Bibliotecas')."</a>";
    $msContextPath = ISMSLib::getIconCode('icon-standard.gif',-2,-4) . "<a href='javascript:isms_redirect_to_mode(" . LIBRARIES_MODE . "," . $this->ciContextType . ",0)'>" . $this->getName() . "</a>";
    return $msRMPath . "&nbsp;".ISMSLib::getIconCode('icon-arrow-right.gif',-2,0).'&nbsp;' . $msContextPath;  
  } 
/**
  * Retorna se o usu�rio logado tem permi��o para inserir o contexto.
  * 
  * <p>M�todo para retornar se o usu�rio tem permi��o para inserir o contexto no sistema.</p>
  * @access public
  * @return boolean Se o usu�ro tem permi��o para inserir o contexto no sistema
  */
  protected function userCanInsert(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    //contexto deve ser tratado, assim assume-se que a principio o usu�rio n�o pode inserir tal contexto
    $mbReturn = false;
    $miCount = 0;
    $maPermissions = array( 'M.L.5.3' );
    //acls negadas o usu�rio logado no sistema
    $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
    foreach($maACLs as $miKey => $miValue){
      $maACLs[$miValue]=$miKey;
    }
    //acl1 AND acl2
    $mbAuxPermission = true;
    foreach($maPermissions as $msValue){
      if(isset($maACLs[$msValue])){
        $mbAuxPermission = false;
      }
      $miCount++;
    }
    //caso exista pelo menos 1 acl nas permi��es do contexto e estas permi��es n�o est�o nas permi��es negadas do usu�rio
    if(($mbAuxPermission)&&($miCount)){
      $mbReturn = $mbAuxPermission;
    }
    return $mbReturn;
  }

 /**
  * Retorna se o usu�rio logado tem permi��o para editar o contexto.
  * 
  * <p>M�todo para retornar se o usu�rio tem permi��o para editar o contexto no sistema.</p>
  * @access protected
  * @return boolean Se o usu�ro tem permi��o de editar o contexto no sistema
  */
  public function userCanEdit($piContextId,$piUserResponsible = 0){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $mbReturn = false;
    if($piContextId){
      $moObjContext = new ISMSContextObject();
      $moObj = $moObjContext->getContextObjectByContextId($piContextId);
      if($moObj){
        if($moObj->fetchById($piContextId)){
          if(in_array($moObj->getContextType(),array(CONTEXT_DOCUMENT_TEMPLATE, CONTEXT_DOCUMENT_TEMPLATE_WITH_CONTENT))){
                $mbReturn = true;
          }
        }
      }
    }
    
    if($mbReturn){
      //contexto deve ser tratado, assim assume-se que a principio o usu�rio n�o pode editar esse contexto
      $maPermissions = array('M.L.5.1');
      //acls negadas do usu�rio logado no sistema
      $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
      foreach($maACLs as $miKey => $miValue){
        $maACLs[$miValue]=$miKey;
      }
      $miCount = 0;
      $mbAuxPermission=false;
      //teste de ter permiss�o global de edi��o, caso n�o tenha, testa se ter permiss�o de edi��o se respons�vel
      //caso tenha permiss�o de respons�vel, verificar se o usu�rio � o respons�vel pelo contexto
      if(!isset($maACLs[$maPermissions[0]])){
          $mbAuxPermission=true;
          $miCount++;
      }
      //caso exista pelo menos 1 acl nas permi��es do contexto e estas permi��es n�o est�o nas permi��es negadas do usu�rio
      if(($mbAuxPermission)&&($miCount)){
        $mbReturn = $mbAuxPermission;
      }
    }
    return $mbReturn;
  }


 /**
  * Retorna se o usu�rio logado tem permi��o para excluir o contexto.
  * 
  * <p>M�todo para retornar se o usu�rio tem permi��o para excluir o contexto no sistema.</p>
  * @access protected
  * @return boolean Se o usu�ro tem permi��o de excluir o contexto no sistema
  */
  protected function userCanDelete($piContextId){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $miContextType = $this->ciContextType;
    if(!$this->contextIsOfType($piContextId,$miContextType)) return false;
    //contexto deve ser tratado, assim assume-se que a principio o usu�rio n�o pode inserir tal contexto
    $mbReturn = false;
    $maPermissions = array( 'M.L.5.2' );

    //acls negadas do usu�rio logado no sistema
    $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
    foreach($maACLs as $miKey => $miValue){
      $maACLs[$miValue]=$miKey;
    }
      $miCount = 0;
    //acl1 AND acl2
    $mbAuxPermission = true;
    foreach($maPermissions as $msValue){
      if(isset($maACLs[$msValue])){
        $mbAuxPermission = false;
      }
      $miCount++;
    }
    //caso exista pelo menos 1 acl nas permi��es do contexto e estas permi��es n�o est�o nas permi��es negadas do usu�rio
    if(($mbAuxPermission)&&($miCount)){
      $mbReturn = $mbAuxPermission;
    }
    return $mbReturn;
  }
  
  /**
  * Retorna o c�digo HTML do icone do contexto.
  * 
  * <p>M�todo para retornar o c�digo HTML do contexto</p>
  * @access public
  * @param integer $piId Id do contexto
  * @param integer $pbIsFetched id do contexto
  * @return string contendo o c�digo HTML do icone do contexto
  */ 
  public function getIconCode($piId, $pbIsFetched = false){
    if(!$pbIsFetched){
      $this->fetchById($piId);
    }
    return ISMSLib::getIconCode($this->getIcon(),-2,10);
  }

 /**
  * Retorna o caminho para abrir a popup de edi��o desse contexto.
  * 
  * <p>M�todo para retornar o caminho para abrir a popup de edi��o desse contexto.</p>
  * @param integer $piContextId id do contexto
  * @access public 
  * @return string caminho para abrir a popup de edi��o desse contexto
  */ 
  public function getVisualizeEditEvent($piContextId,$psUniqId){
    $soSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    $msPackagesLibrariesPath = ($soSession->getMode()==MANAGER_MODE) ? "packages/libraries/" : "../../packages/libraries/";
    $msPopupId = "popup_document_template_edit";
    
    return "isms_open_popup('{$msPopupId}','{$msPackagesLibrariesPath}{$msPopupId}.php?id=$piContextId','','true');"
           ."soPopUpManager.getPopUpById('{$msPopupId}').setCloseEvent( function close_visualize_$psUniqId() {soPopUpManager.closePopUp('popup_visualize_$psUniqId');} );";
    
  }
  
  /**
  * Retorna a descri��o do contexto.
  * 
  * <p>M�todo para retornar a descri��o do contexto.</p>
  * @access public 
  * @return string Descri��o do contexto
  */
  public function getDescription(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    return $this->getFieldValue('template_description');
  }
}
?>
