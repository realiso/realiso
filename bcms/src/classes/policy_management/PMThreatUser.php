<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
 
/**
 * Classe PMThreatUser
 *
 * <p>Classe que representa a tabela que relaciona usu�rios a threats.</p>
 * @package ISMS
 * @subpackage classes
 */
class PMThreatUser extends ISMSTable{
	/**
  * Construtor.
  * 
  * <p>Construtor da classe PMThreatUser.</p>
  * @access public 
  */
	public function __construct() {
		$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    parent::__construct("pm_threat_user");
        
    $this->coDataset->addFWDDBField(new FWDDBField("fkThreat", 	"threat_id", DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("fkUser", 			"user_id", DB_NUMBER));
  }
  
 /**
  * Insere rela��es entre um Threat e diversos usu�rios.
  * 
  * <p>M�todo para inserir rela��es entre uma threat e diversos usu�rios.</p>
  * @access public
  * @param integer $piThreatId Id da threat
  * @param array $paUsersIds Ids dos usu�rios
  */ 
  public function insertUsers($piThreatId, $paUsers) {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
  	$this->setFieldValue('threat_id', $piThreatId);
  	foreach ($paUsers as $piUserId) {
  		$this->setFieldValue('user_id', $piUserId);
  		$this->insert();
  	}
  }
  
 /**
  * Deleta rela��es entre uma Threat e diversos usu�rios.
  * 
  * <p>M�todo para deletar rela��es entre uma threat e diversos usu�rios.</p>
  * @access public
  * @param integer $piThreatId Id do threat
  * @param integer $paUsers Ids dos usu�rios
  */
  public function deleteUsers($piThreatId, $paUsers) {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
  	if (count($paUsers)) {
	  	$this->createFilter($piThreatId, 'threat_id');
	  	foreach ($paUsers as $piUserId)  		
	  		$this->createFilter($piUserId, 'user_id', 'in');
	  	$this->delete();
  	}
  }
 
 /**
  * Retorna os usu�rios de uma Threat.
  * 
  * <p>M�todo para retornar os usu�rios de uma Threat.</p>
  * @access public
  * @param integer $piThreatId Id da Threat
  * @return array $paUsers Ids dos usu�rios
  */ 
  public function getUsersFromThreat($piThreatId) {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
  	$this->createFilter($piThreatId,'threat_id');
  	$this->select();
  	$maUsers = array();
  	while ($this->fetch()) $maUsers[] = $this->getFieldValue('user_id');
  	return $maUsers;
  }
}
?>