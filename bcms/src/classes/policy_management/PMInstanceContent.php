<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
 
/**
 * Classe PMInstanceContent
 *
 * <p>Classe que insere o conte�do de uma inst�ncia</p>
 * @package ISMS
 * @subpackage classes
 */
class PMInstanceContent extends ISMSTable {
  /**
  * Construtor.
  * 
  * <p>Construtor da classe PMInstanceContent.</p>
  * @access public 
  */
	public function __construct() {
		$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    parent::__construct("pm_instance_content");
    $this->csAliasId = "instance_id";
        
    $this->coDataset->addFWDDBField(new FWDDBField("fkInstance", "instance_id", DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("tContent", "instance_content", DB_STRING));
    
    $this->caSearchableFields = array('instance_content');
  }
  
  /**
  * Insere um registro na tabela.
  * 
  * <p>M�todo para inserir um registro na tabela.</p>
  * @access public 
  */ 
  public function insert() {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

  	$moDBConn = FWDWebLib::getConnection();
  	if ($moDBConn->getDatabaseType() == DB_ORACLE) {
  		$msCollumn = $this->getFieldName('instance_content');
  		$msIdColumn = $this->getFieldName($this->getIdAlias());
	  	$msContent = $this->getFieldValue('instance_content');	  	
	  	$this->setFieldValue('instance_content', '');	  	
	  	parent::insert();	  	
			$moDBConn->updateClob($this->getTable(), $msCollumn, $msContent, $msIdColumn.'='.$this->getFieldValue($this->getIdAlias()));
  	}
  	else {
  		return parent::insert();
  	}
  }
  
  /**
  * Atualiza um registro da tabela.
  * 
  * <p>M�todo para atualizar um registro da tabela.</p>
  * @access public
  * @param integer $piId Identificador 
  */  
  public function update($piId = 0) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

    $moDBConn = FWDWebLib::getConnection();
  	if ($moDBConn->getDatabaseType() == DB_ORACLE) {
  		$msCollumn = $this->getFieldName('instance_content');
  		$msIdColumn = $this->getFieldName($this->getIdAlias());
	  	$msContent = $this->getFieldValue('instance_content');
	  	$miId = 0;
	  	if (!$piId) $miId = $this->getFieldValue($this->getIdAlias());
			else $miId = $piId;
			$moDBConn->updateClob($this->getTable(), $msCollumn, $msContent, $msIdColumn.'='.$miId);
  	}
  	else {
  		return parent::update($piId);
  	}
  }
}
?>