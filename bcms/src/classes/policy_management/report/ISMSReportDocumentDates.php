<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe ISMSReportDocumentDates.
 *
 * <p>Classe que implementa o relat�rio de data de cria��o e de revis�es por documento.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportDocumentDates extends ISMSReport {
  
 /**
  * Inicializa o relat�rio.
  *
  * <p>M�todo para inicializar o relat�rio.
  * Necess�rio para funcionar com o XML compilado.</p>
  * @access public
  */
  public function init(){
    parent::init();
    $this->coDataSet->addFWDDBField(new FWDDBField('d.fkContext'      ,'doc_id'                    ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('d.sName'          ,'doc_name'                  ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField(''                 ,'doc_date_crated'           ,DB_DATETIME));
    $this->coDataSet->addFWDDBField(new FWDDBField('di.sFileName'     ,'doc_instance_file_name'    ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('di.bIsLink'       ,'doc_instance_is_link'      ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('rev.dDateRevision' ,'doc_date_revision'         ,DB_DATETIME));
    $this->coDataSet->addFWDDBField(new FWDDBField('di.nMajorVersion'  ,'document_major_version' ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('di.nRevisionVersion','document_revision_version',DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('di.sManualVersion' ,'document_version'       ,DB_STRING));
  }
  
  /**
   * Executa a query do relat�rio.
   *
   * <p>M�todo para executar a query do relat�rio.</p>
   * @access public
   */
  public function makeQuery(){
    $miCreationDateStart = $this->coFilter->getCreationDateStart();
    $miCreationDateEnd = $this->coFilter->getCreationDateEnd();
    $miRevisionDateStart = $this->coFilter->getRevisionDateStart();
    $miRevisionDateEnd = $this->coFilter->getRevisionDateEnd();
    
    $maFilters = array();

    if($miCreationDateStart){
      $maFilters[] = "ch.context_date_created >= ".ISMSLib::getTimestampFormat($miCreationDateStart);
    }
    
    if($miCreationDateEnd){
      $maFilters[] = "ch.context_date_created <= ".ISMSLib::getTimestampFormat($miCreationDateEnd);
    }
    
    if($miRevisionDateStart){
      $maFilters[] = "rev.dBeginProduction >= ".ISMSLib::getTimestampFormat($miRevisionDateStart);
    }
    if($miRevisionDateEnd){
      $maFilters[] = "rev.dBeginProduction <= ".ISMSLib::getTimestampFormat($miRevisionDateEnd);
    }
    
    if(count($maFilters)){
      $msWhere = ' WHERE '.implode(' AND ',$maFilters);
    }else{
      $msWhere = '';
    }
    $this->csQuery = "SELECT                        d.fkContext as doc_id,                        d.sName as doc_name,                        ch.context_date_created as doc_date_crated,                        di.sFileName as doc_instance_file_name,
                        di.bIsLink as doc_instance_is_link,
                        rev.dBeginProduction as doc_date_revision,
                        rev.sManualVersion AS document_version,
                        rev.nMajorVersion as document_major_version,
                        rev.nRevisionVersion as document_revision_version                      FROM                        view_pm_document_active d
                        LEFT JOIN view_pm_doc_instance_active di ON (di.fkContext = d.fkCurrentVersion)
                        LEFT JOIN view_pm_doc_instance_active rev ON (rev.fkDocument = d.fkContext 
                                                                      AND rev.nMajorVersion>1 
                                                                      AND rev.dBeginProduction IS NOT NULL)                        LEFT JOIN context_history ch ON (ch.context_id = d.fkContext)
                        
                      $msWhere                      ORDER BY doc_name";
    return parent::executeQuery();
  }
  
 /**
  * Desenha o cabe�alho do relat�rio.
  *
  * <p>M�todo para desenhar o cabe�alho do relat�rio.</p>
  * @access public
  */
  public function drawHeader(){
    foreach($this->caHeaders as $moHeader){
      if($moHeader->getAttrName()=='header_filter'){
        $moHeaderFilter = $moHeader;
      }elseif($moHeader->getAttrName()=='header_filter_item'){
        $moHeaderFilterItem = $moHeader;
      }elseif($moHeader->getAttrName()=='report_header_blank'){
        $moHeaderBlank = $moHeader;
      }else{
        $this->coWriter->drawLine($moHeader,array());
      }
    }
    if($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());
    
    $moFilterText = FWDWebLib::getObject('header_filter_text');
    $moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
    $maFilters = $this->coFilter->getSummary();
    foreach($maFilters as $maFilter){
      $moFilterText->setValue($maFilter['name']);
      $this->coWriter->drawLine($moHeaderFilter,array());
      foreach($maFilter['items'] as $msFilterItem){
        $moFilterItemText->setValue($msFilterItem);
        $this->coWriter->drawLine($moHeaderFilterItem,array());
      }
    }
    $this->coWriter->drawLine($moHeaderBlank,array());
  }

}
?>