<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportDocumentsWithoutRegister.
 *
 * <p>Classe que implementa o relatório de Relatório de documentos sem registro, podendo filtrar por responsável pelo documento.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportDocumentsWithoutRegister extends ISMSFilteredReport {
  
  public function __construct(){
    parent::__construct();
  }
  
 /**
  * Inicializa o relatório.
  *
  * <p>Método para inicializar o relatório.
  * Necessário para funcionar com o XML compilado.</p>
  * @access public
  */
  public function init(){
    parent::init();
    $this->coDataSet->addFWDDBField(new FWDDBField('d.fkContext'     ,'document_id'            ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('d.sName'         ,'document_name'          ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('d.fkMainApprover','responsible_id'         ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('u.sName'         ,'responsible_name'       ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('di.bIsLink'      ,'doc_instance_is_link'   ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('rh.dDateRevision','doc_instance_file_name' ,DB_STRING));
  }
  
 /**
  * Executa a query do relatório.
  *
  * <p>Método para executar a query do relatório.</p>
  * @access public
  */
  public function makeQuery(){
    $maUserId = $this->coFilter->getUserId();
    
    $maFilters = array();
    
    $maFilters[] = "d.fkContext NOT IN (
     SELECT dr.fkDocument FROM view_pm_doc_registers_active dr
  )";
    
    if(count($maUserId)>0){
      $maFilters[] = 'd.fkMainApprover IN ('.implode(',',$maUserId).')';
    }
    
    if(count($maFilters)==0){
      $msWhere = '';
    }else{
      $msWhere = ' WHERE '.implode(' AND ',$maFilters);
    }
    $this->csQuery = "SELECT d.fkContext as document_id,
                          d.sName as document_name,
                          d.fkMainApprover as responsible_id,
                          u.sName as responsible_name,
                          di.bIsLink as doc_instance_is_link,
                          di.sFileName as doc_instance_file_name
                      FROM view_pm_published_docs d
                      JOIN view_isms_user_active u ON (u.fkContext = d.fkMainApprover)
                      LEFT JOIN view_pm_doc_instance_active di ON (di.fkContext = d.fkCurrentVersion)
                      {$msWhere}
                      ORDER BY d.sName, u.sName";
    return parent::executeQuery();
  }

}

?>