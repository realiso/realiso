<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportDocInstanceCommentAndJustification.
 *
 * <p>Classe que implementa o relat�rio de coment�rios de aprova��o e
 * justificativas de revis�o de inst�ncias de documentos.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportDocInstanceCommentAndJustification extends ISMSFilteredReport {
  
  public function __construct(){
    parent::__construct();
  }
  
 /**
  * Inicializa o relat�rio.
  *
  * <p>M�todo para inicializar o relat�rio.
  * Necess�rio para funcionar com o XML compilado.</p>
  * @access public
  */
  public function init(){
    parent::init();
    $this->coDataSet->addFWDDBField(new FWDDBField('d.fkContext'              ,'document_id'               ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('d.sName'                  ,'document_name'             ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('di.fkContext'             ,'instance_id'               ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('di.nMajorVersion'         ,'instance_major_version'    ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('di.nRevisionVersion'      ,'instance_revision_version' ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('di.sManualVersion'        ,'instance_manual_version'   ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('di.sFileName'             ,'instance_file_name'        ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('di.bIsLink'               ,'instance_is_link'          ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('di.tModifyComment'        ,'instance_modify_comment'   ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('di.tRevisionJustification','instance_rev_justification',DB_STRING));
  }
  
 /**
  * Executa a query do relat�rio.
  *
  * <p>M�todo para executar a query do relat�rio.</p>
  * @access public
  */
  public function makeQuery(){
    $maDocuments = $this->coFilter->getDocuments();
    
    $maFilters = array();
    
    $maFilters[] = 'di.dBeginProduction IS NOT NULL';
    
    if(count($maDocuments)>0){
      $maFilters[] = 'd.fkContext IN ('.implode(',',$maDocuments).')';
    }
    
    if(count($maFilters)==0){
      $msWhere = '';
    }else{
      $msWhere = ' WHERE '.implode(' AND ',$maFilters);
    }
    $this->csQuery = "SELECT
                        d.fkContext AS document_id,
                        d.sName AS document_name,
                        di.fkContext AS instance_id,
                        di.nMajorVersion AS instance_major_version,
                        di.nRevisionVersion AS instance_revision_version,
                        di.sManualVersion AS instance_manual_version,
                        di.sFileName AS instance_file_name,
                        di.bIsLink AS instance_is_link,
                        di.tModifyComment AS instance_modify_comment,
                        di.tRevisionJustification AS instance_rev_justification
                      FROM
                        view_pm_document_active d
                        JOIN pm_doc_instance di ON (di.fkdocument = d.fkcontext)
                      {$msWhere}
                      ORDER BY d.sName, di.fkContext";
    return parent::executeQuery();
  }

}

?>