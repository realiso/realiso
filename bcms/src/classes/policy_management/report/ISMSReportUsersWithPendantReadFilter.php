<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportUsersWithPendantReadFilter.
 *
 * <p>Classe que implementa o filtro do relat�rio de leitores que ainda n�o leram os documentos dos quais s�o leitores.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportUsersWithPendantReadFilter extends FWDReportFilter {

  protected $caUserId = array();

  public function setUserId($paUserId){
    $this->caUserId = $paUserId;
  }

  public function getUserId(){
    return $this->caUserId;
  }

  public function getSummary(){
    
    $maFilters = array();
    
    $maFilter = array(
      'name' => FWDLanguage::getPHPStringValue('report_filter_document_reader_cl','Leitores dos documentos:'),
      'items' => array()
    );
    if(count($this->caUserId)){
      foreach($this->caUserId as $miId){
        $moUser = new ISMSUser();
        $moUser->fetchById($miId);
        $maFilter['items'][] = $moUser->getName();
      }
    }else{
      $maFilter['items'][] = FWDLanguage::getPHPStringValue('report_filter_all_document_reader_cl','todos(as)');
    }
    $maFilters[] = $maFilter;
    
    return $maFilters;
  }
}
?>