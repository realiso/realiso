<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportDocsWithHighFrequencyRevisionFilter.
 *
 * <p>Classe que implementa o filtro do relat�rio de Relat�rio que mostra os documentos que possuem uma alta freq��ncia de revis�o. Entende-se por freq��ncia muito alta, documentos que s�o revisados em m�dia mais de uma vez por semana.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportDocsWithHighFrequencyRevisionFilter extends FWDReportFilter {

  protected $caUserId = array();

  public function setUserId($paUserId){
    $this->caUserId = $paUserId;
  }

  public function getUserId(){
    return $this->caUserId;
  }

  public function getSummary(){
    
    $maFilters = array();
    
    $maFilter = array(
      'name' => FWDLanguage::getPHPStringValue('report_filter_userid_cl','Respons�vel:'),
      'items' => array()
    );
    if(count($this->caUserId)){
      foreach($this->caUserId as $miId){
        $moUser = new ISMSUser();
        $moUser->fetchById($miId);
        $maFilter['items'][] = $moUser->getName();
      }
    }else{
      $maFilter['items'][] = FWDLanguage::getPHPStringValue('report_filter_all_userid','todos(as)');
    }

    $maFilterExp = array(
      'name' => FWDLanguage::getPHPStringValue('report_filter_high_frequency_revision_definition','Alta freq��ncia de revis�o �:'),
      'items' => array(FWDLanguage::getPHPStringValue('report_filter_high_frequency_revision_definition_explanation','mais de uma revis�o em um per�odo de uma semana.'))
    );

    $maFilters[] = $maFilter;
    $maFilters[] = $maFilterExp;
    
    return $maFilters;
  }

}

?>