<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportDocumentDatesFilter.
 *
 * <p>Classe que implementa o filtro do relat�rio de data de cria��o e de revis�es por documento.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportDocumentDatesFilter extends FWDReportFilter {

  protected $ciCreationDateStart;
  protected $ciCreationDateEnd;
  protected $ciRevisionDateStart;
  protected $ciRevisionDateEnd;

  public function getCreationDateStart(){
    return $this->ciCreationDateStart;
  }

  public function setCreationDateStart($piCreationDateStart){
    $this->ciCreationDateStart = $piCreationDateStart;
  }

  public function getCreationDateEnd(){
    return $this->ciCreationDateEnd;
  }

  public function setCreationDateEnd($piCreationDateEnd){
    $this->ciCreationDateEnd = $piCreationDateEnd;
  }

  public function getRevisionDateStart(){
    return $this->ciRevisionDateStart;
  }

  public function setRevisionDateStart($piRevisionDateStart){
    $this->ciRevisionDateStart = $piRevisionDateStart;
  }

  public function getRevisionDateEnd(){
    return $this->ciRevisionDateEnd;
  }

  public function setRevisionDateEnd($piRevisionDateEnd){
    $this->ciRevisionDateEnd = $piRevisionDateEnd;
  }

  public function getSummary(){
    $maFilters = array();
    
    $msInitialDate = ($this->ciCreationDateStart?ISMSLib::getISMSShortDate($this->ciCreationDateStart,true):'');
    $msFinalDate = ($this->ciCreationDateEnd?ISMSLib::getISMSShortDate($this->ciCreationDateEnd,true):'');
    if($msInitialDate || $msFinalDate){
      if($msInitialDate){
        if($msFinalDate){
          $msDateFilter = FWDLanguage::getPHPStringValue('rs_between_initialfinaldate','Entre %initialDate% e %finalDate%');
        }else{
          $msDateFilter = FWDLanguage::getPHPStringValue('rs_after_initialdate','Depois de %initialDate%');
        }
      }else{
        $msDateFilter = FWDLanguage::getPHPStringValue('rs_before_finaldate','Antes de %finalDate%');
      }
      $msDateFilter = str_replace(array('%initialDate%','%finalDate%'),array($msInitialDate,$msFinalDate),$msDateFilter);
      $maFilters[] = array(
        'name' => FWDLanguage::getPHPStringValue('rs_creation_date','Data de cria��o'),
        'items' => array($msDateFilter)
      );
    }

    $msInitialDate = ($this->ciRevisionDateStart?ISMSLib::getISMSShortDate($this->ciRevisionDateStart,true):'');
    $msFinalDate = ($this->ciRevisionDateEnd?ISMSLib::getISMSShortDate($this->ciRevisionDateEnd,true):'');
    if($msInitialDate || $msFinalDate){
      if($msInitialDate){
        if($msFinalDate){
          $msDateFilter = FWDLanguage::getPHPStringValue('rs_between_initialfinaldate','Entre %initialDate% e %finalDate%');
        }else{
          $msDateFilter = FWDLanguage::getPHPStringValue('rs_after_initialdate','Depois de %initialDate%');
        }
      }else{
        $msDateFilter = FWDLanguage::getPHPStringValue('rs_before_finaldate','Antes de %finalDate%');
      }
      $msDateFilter = str_replace(array('%initialDate%','%finalDate%'),array($msInitialDate,$msFinalDate),$msDateFilter);
      $maFilters[] = array(
        'name' => FWDLanguage::getPHPStringValue('rs_last_revision_date','Data da �ltima revis�o'),
        'items' => array($msDateFilter)
      );
    }

    return $maFilters;
  }

}
?>