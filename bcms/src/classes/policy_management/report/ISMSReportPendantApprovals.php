<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportPendantApprovals.
 *
 * <p>Classe que implementa o relat�rio de aprova��es pendentes.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportPendantApprovals extends ISMSReport {
  
  public function __construct(){
    parent::__construct();
  }
  
 /**
  * Inicializa o relat�rio.
  *
  * <p>M�todo para inicializar o relat�rio.
  * Necess�rio para funcionar com o XML compilado.</p>
  * @access public
  */
  public function init(){
    parent::init();
    $this->coDataSet->addFWDDBField(new FWDDBField('doc.fkContext','doc_id'      ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('doc.sName'    ,'doc_name'    ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('ins.sFilename','doc_filename',DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('ins.bIsLink'  ,'doc_is_link' ,DB_NUMBER));    
  }
  
 /**
  * Executa a query do relat�rio.
  *
  * <p>M�todo para executar a query do relat�rio.</p>
  * @access public
  */
  public function makeQuery(){
    $this->csQuery = "SELECT
                      	doc.fkContext as doc_id,
                      	doc.sName as doc_name,
                      	ins.sFilename as doc_filename,
                      	ins.bIsLink as doc_is_link
                      FROM
                      	view_pm_document_active doc
                      	JOIN view_isms_context_active ctx ON (doc.fkContext = ctx.pkContext)
                      	JOIN view_pm_doc_instance_active ins ON (doc.fkCurrentVersion = ins.fkContext)
                      WHERE
                        ctx.nState = ".CONTEXT_STATE_DOC_PENDANT." AND ctx.nType = ".CONTEXT_DOCUMENT."
                      ORDER BY doc.sName";
    return parent::executeQuery();
  }

}

?>