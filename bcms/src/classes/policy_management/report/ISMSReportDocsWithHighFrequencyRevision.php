<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportDocsWithHighFrequencyRevision.
 *
 * <p>Classe que implementa o relat�rio de Relat�rio que mostra os documentos que possuem uma alta freq��ncia de revis�o. Entende-se por freq��ncia muito alta, documentos que s�o revisados em m�dia mais de uma vez por semana.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportDocsWithHighFrequencyRevision extends ISMSFilteredReport {
  
  public function __construct(){
    parent::__construct();
  }
  
 /**
  * Inicializa o relat�rio.
  *
  * <p>M�todo para inicializar o relat�rio.
  * Necess�rio para funcionar com o XML compilado.</p>
  * @access public
  */
  public function init(){
    parent::init();
    $this->coDataSet->addFWDDBField(new FWDDBField('d.fkContext'              ,'document_id'                ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('d.sName'                  ,'document_name'              ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('di.sFileName'             ,'doc_instance_file_name'     ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('di.bIsLink'               ,'doc_instance_is_link'       ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('u.fkContext'              ,'user_id'                    ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('u.sName'                  ,'user_name'                  ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('di.dEndProduction'        ,'doc_date_revision'          ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('di.tRevisionJustification','doc_revision_justification' ,DB_STRING));
  }
  

 /**
  * Executa a query do relat�rio.
  *
  * <p>M�todo para executar a query do relat�rio.</p>
  * @access public
  */
  public function makeQuery(){
    $maUserId = $this->coFilter->getUserId();
    
    $maFilters = array();
    
    $msNumDates = "";
    switch (FWDWebLib::getConnection()->getDatabaseType()) {
      case DB_MSSQL:{
        $msNumDates = " DATEDIFF(day, dt1.dDate, dt2.dDate) ";
        break;
      }case DB_POSTGRES:{
        $msNumDates = " EXTRACT (DAYS FROM dt2.dDate  - dt1.dDate) ";
        break;
      }case DB_ORACLE:{
        $msNumDates = " floor(dt2.dDate - dt1.dDate) ";
        break;
      }
    }

    
    $maFilters[] = "( $msNumDates > 0 AND
       ( (SELECT count(*) FROM view_pm_doc_instance_active di WHERE di.fkDocument = d.fkContext AND di.dEndProduction IS NOT NULL ) * 7 )
       /
       $msNumDates
       > 1
    ) OR (
        $msNumDates = 0 AND
       (SELECT count(*) FROM view_pm_doc_instance_active di WHERE di.fkDocument = d.fkContext AND di.dEndProduction IS NOT NULL ) > 1
    )";
    
    if(count($maUserId)>0){
      $maFilters[] = 'u.fkContext IN ('.implode(',',$maUserId).')';
    }
    
    if(count($maFilters)==0){
      $msWhere = '';
    }else{
      $msWhere = ' WHERE '.implode(' AND ',$maFilters);
    }

    $this->csQuery = "SELECT d.fkContext as document_id,
                          d.sName as document_name,
                          di.sFileName AS doc_instance_file_name,
                          di.bIsLink AS doc_instance_is_link,
                          u.fkContext AS user_id,
                          u.sName AS user_name,
                          di.dEndProduction as doc_date_revision,
                          di.tRevisionJustification as doc_revision_justification
                      FROM view_pm_document_active d
                      JOIN isms_context_date dt1 ON (dt1.fkContext = d.fkContext AND dt1.nAction = 2901)
                      JOIN isms_context_date dt2 ON (dt2.fkContext = d.fkContext AND dt2.nAction = 2902)
                      JOIN view_isms_user_active u ON (u.fkContext = d.fkMainApprover)
                      LEFT JOIN view_pm_doc_instance_active di ON (di.fkDocument = d.fkContext AND di.dEndProduction IS NOT NULL)
                      
                      {$msWhere}
                      ORDER BY d.sName, d.fkContext, u.sName";
    return parent::executeQuery();
  }

}

?>