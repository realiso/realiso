<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe ISMSReportTopRevisedDocuments.
 *
 * <p>Classe que implementa o relat�rio de documentos mais revisados, por ordem de quantidade de revis�es.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportTopRevisedDocuments extends ISMSReport {
  
 /**
  * Inicializa o relat�rio.
  *
  * <p>M�todo para inicializar o relat�rio.
  * Necess�rio para funcionar com o XML compilado.</p>
  * @access public
  */
  public function init(){
    parent::init();
    $this->coDataSet->addFWDDBField(new FWDDBField('doc.fkContext'       ,'doc_id'        ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('doc.sName'           ,'doc_name'      ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('doc.revision_count'  ,'doc_rev_count' ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('curr.sPath'          ,'curr_file_name',DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('curr.bIsLink'        ,'curr_is_link'  ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('rev.fkContext'       ,'rev_id'        ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('di.nMajorVersion'  ,'document_major_version' ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('di.nRevisionVersion','document_revision_version',DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('di.sManualVersion' ,'document_version'       ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('rev.dBeginProduction','rev_date'      ,DB_DATETIME));
    
  }
  
  /**
   * Executa a query do relat�rio.
   *
   * <p>M�todo para executar a query do relat�rio.</p>
   * @access public
   */
  public function makeQuery(){
    $miTruncateNumber = $this->coFilter->getTruncateNumber();
    if(!$miTruncateNumber) $miTruncateNumber = 0;

    $maFilters = array();
    if(count($maFilters)){
      $msWhere = ' WHERE '.implode(' AND ',$maFilters);
    }else{
      $msWhere = '';
    }
    $this->csQuery = "SELECT 
                        doc.fkContext as doc_id,
                        doc.sName as doc_name,
                        doc.rev_count as doc_rev_count,
                        curr.sPath as curr_file_name,
                        curr.bIsLink as curr_is_link,
                        rev.fkContext as rev_id,
                        rev.sManualVersion AS document_version,
                        rev.nMajorVersion as document_major_version,
                        rev.nRevisionVersion as document_revision_version,
                        rev.dBeginProduction as rev_date
                      FROM ".FWDWebLib::getFunctionCall("get_topN_revised_documents($miTruncateNumber)")." doc
                      JOIN view_pm_doc_instance_active curr ON (curr.fkContext = doc.fkCurrentVersion)
                      LEFT JOIN view_pm_doc_instance_active rev ON (rev.fkDocument = doc.fkContext 
                                                                    AND rev.nMajorVersion>1 
                                                                    AND rev.dBeginProduction IS NOT NULL)                      ORDER BY doc_rev_count desc, doc_name, rev_date ASC";
    return parent::executeQuery();
  }
  
 /**
  * Desenha o cabe�alho do relat�rio.
  *
  * <p>M�todo para desenhar o cabe�alho do relat�rio.</p>
  * @access public
  */
  public function drawHeader(){
    foreach($this->caHeaders as $moHeader){
      if($moHeader->getAttrName()=='header_filter'){
        $moHeaderFilter = $moHeader;
      }elseif($moHeader->getAttrName()=='header_filter_item'){
        $moHeaderFilterItem = $moHeader;
      }elseif($moHeader->getAttrName()=='report_header_blank'){
        $moHeaderBlank = $moHeader;
      }else{
        $this->coWriter->drawLine($moHeader,array());
      }
    }
    if($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());
    
    $moFilterText = FWDWebLib::getObject('header_filter_text');
    $moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
    $maFilters = $this->coFilter->getSummary();
    foreach($maFilters as $maFilter){
      $moFilterText->setValue($maFilter['name']);
      $this->coWriter->drawLine($moHeaderFilter,array());
      foreach($maFilter['items'] as $msFilterItem){
        $moFilterItemText->setValue($msFilterItem);
        $this->coWriter->drawLine($moHeaderFilterItem,array());
      }
    }
    $this->coWriter->drawLine($moHeaderBlank,array());
  }

}
?>