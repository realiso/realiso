<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportContextsWithoutDocuments.
 *
 * <p>Classe que implementa o relat�rio de processos, ativos, �reas, controles, etc que n�o possuem documentos.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportContextsWithoutDocuments extends ISMSReport {

 /**
  * Inicializa o relat�rio.
  *
  * <p>M�todo para inicializar o relat�rio.
  * Necess�rio para funcionar com o XML compilado.</p>
  * @access public
  */
  public function init(){
    parent::init();    
    $this->coDataSet->addFWDDBField(new FWDDBField(''            ,'context_type'          ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField(''            ,'context_id'            ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField(''            ,'context_name'          ,DB_STRING));    
  }
  
  /**
   * Executa a query do relat�rio.
   *
   * <p>M�todo para executar a query do relat�rio.</p>
   * @access public
   */
  public function makeQuery(){
    $msContextsTypes = CONTEXT_AREA.",".CONTEXT_PROCESS.",".CONTEXT_ASSET.",".CONTEXT_CONTROL;
    if (FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(INCIDENT_MODE))
      $msContextsTypes .= ",".CONTEXT_CI_ACTION_PLAN;
    
    $this->csQuery = "SELECT                         cn.context_type as context_type,                         cn.context_id as context_id,                         cn.context_name as context_name                       FROM context_names cn
                       JOIN isms_context c ON (c.pkContext = cn.context_id AND c.nState <> ".CONTEXT_STATE_DELETED.")                       WHERE                         cn.context_type IN ($msContextsTypes)                         AND NOT EXISTS (                           SELECT *                           FROM view_pm_doc_context_active dc2
                             JOIN view_pm_published_docs doc ON (doc.fkContext = dc2.fkDocument)                             JOIN view_pm_doc_instance_active di2 ON (di2.fkDocument = dc2.fkDocument)                           WHERE dc2.fkContext = cn.context_id                         )
                       ORDER BY context_type, context_name";
    return parent::executeQuery();
  }
}
?>