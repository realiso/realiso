<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportDocumentReadersFilter.
 *
 * <p>Classe que implementa o filtro do relatório de documentos, seus responsáveis e leitores.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportDocumentReadersFilter extends FWDReportFilter {

  protected $caDocuments = array();

  public function getDocuments(){
    return $this->caDocuments;
  }

  public function setDocuments($paDocuments){
    $this->caDocuments = $paDocuments;
  }

  public function getSummary(){
    $maFilters = array();
    
    if(count($this->caDocuments)){
      $maFilter = array(
        'name' => FWDLanguage::getPHPStringValue('rs_documents','Documentos'),
        'items' => array()
      );
      foreach($this->caDocuments as $miDocumentId){
        $moDocument = new PMDocument();
        $moDocument->fetchById($miDocumentId);
        $maFilter['items'][] = $moDocument->getName();
      }
      $maFilters[] = $maFilter;
    }

    return $maFilters;
  }

}
?>