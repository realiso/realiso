<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportDocumentsByComponent.
 *
 * <p>Classe que implementa o relatório de documentos por componente do SGSI.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportDocumentsByComponent extends ISMSFilteredReport {
  
  public function __construct(){
    parent::__construct();
  }
  
 /**
  * Inicializa o relatório.
  *
  * <p>Método para inicializar o relatório.
  * Necessário para funcionar com o XML compilado.</p>
  * @access public
  */
  public function init(){
    parent::init();
    $this->coDataSet->addFWDDBField(new FWDDBField('doc.nType'    ,'doc_type'    ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('doc.fkContext','doc_id'      ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('doc.sName'    ,'doc_name'    ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('ins.sFilename','doc_filename',DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('ins.bIsLink'  ,'doc_is_link' ,DB_NUMBER));
  }
  
 /**
  * Executa a query do relatório.
  *
  * <p>Método para executar a query do relatório.</p>
  * @access public
  */
  public function makeQuery(){
    $maDocType = $this->coFilter->getDocType();
    
    $maFilters = array();
    
    if(count($maDocType)>0){
      $maFilters[] = 'doc.nType IN ('.implode(',',$maDocType).')';
    }
    
    if(count($maFilters)==0){
      $msWhere = '';
    }else{
      $msWhere = ' WHERE '.implode(' AND ',$maFilters);
    }
    
    $this->csQuery = "SELECT
                      	doc.nType as doc_type,
                      	doc.fkContext as doc_id,
                      	doc.sName as doc_name,
                      	ins.sFilename as doc_filename,
                      	ins.bIsLink as doc_is_link
                      FROM
                      	view_pm_published_docs doc
                      	JOIN view_pm_doc_instance_active ins ON (doc.fkCurrentVersion = ins.fkContext)
                      {$msWhere}
                      ORDER BY doc.nType, doc.sName";
    return parent::executeQuery();
  }

}

?>