<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
include_once $handlers_ref . "QueryDocumentApprovers.php";
include_once $handlers_ref . "QueryDocumentReaders.php";
include_once $handlers_ref . "QueryDocumentSubDocs.php";
include_once $handlers_ref . "QueryDocumentRegisters.php";
include_once $handlers_ref . "QueryDocumentReferences.php";
include_once $handlers_ref . "QueryDocumentComponents.php";
 
/**
 * Classe ISMSReportDocumentSummary.
 *
 * <p>Classe que implementa o relat�rio de folha de rosto de um documento.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportDocumentSummary extends ISMSReport {
  
 /**
  * Inicializa o relat�rio.
  *
  * <p>M�todo para inicializar o relat�rio.
  * Necess�rio para funcionar com o XML compilado.</p>
  * @access public
  */
  public function init(){
    parent::init();
    
    $this->coDataSet->addFWDDBField(new FWDDBField("","info",DB_STRING));
  }
  
  /**
   * Executa a query do relat�rio.
   *
   * <p>M�todo para executar a query do relat�rio.</p>
   * @access public
   */
  public function makeQuery(){
  	/*
  	 * Se for excel, retorna string dizendo que relat�rio n�o � suportado
  	 */
  	if ($this->coFilter->getFileType() == REPORT_FILETYPE_EXCEL) {
  		$this->csQuery = FWDWebLib::selectConstant(FWDLanguage::getPHPStringValue('rs_report_not_supported',"'RELAT�RIO N�O SUPORTADO NESTE FORMATO'") . " as info");
  		return $this->executeQuery();
  	}
  }
  
  /**
  * Desenha o corpo do relat�rio.
  *
  * <p>M�todo para desenhar o corpo do relat�rio.</p>
  * @access public
  */
  public function drawBody(){
  	/*
  	 * Se for excel, retorna string dizendo que relat�rio n�o � suportado
  	 */
  	if ($this->coFilter->getFileType() == REPORT_FILETYPE_EXCEL) {
  		return;
  	}
  	
  	$miDocId = $this->coFilter->getDocument();
  	  	
  	$moBlankLine = FWDWebLib::getObject('blank_line');
  	$moHeaderSection = FWDWebLib::getObject('document_header_section');
  	$moSection = FWDWebLib::getObject('document_section');    	
  	$moDocLevel = FWDWebLib::getObject('level_document');  	  	
  	  	
  	/*************** Informa��es b�sicas do documento ************/
  	$moSection->setValue(FWDLanguage::getPHPStringValue('rs_general_info_section','Informa��es Gerais'));
  	$this->coWriter->drawLine($moHeaderSection,$moDocLevel->getOffsets());
  	$this->coWriter->drawLine($moBlankLine,$moDocLevel->getOffsets());
  	
  	$moDocument = new PMDocument();
  	$moDocument->fetchById($miDocId);
  	
  	// Nome do documento
  	FWDWebLib::getObject('document_name_label')->setValue($moDocument->getLabel().':');  	
  	FWDWebLib::getObject('document_name')->setValue($moDocument->getFieldValue('document_name'));
  	$this->coWriter->drawLine(FWDWebLib::getObject('document_header_name'),$moDocLevel->getOffsets());
  	
  	// Pega inst�ncia publicada do documento
  	$moDocInstance = $moDocument->getPublishedInstance();
  	
  	$mbIsLink = $moDocInstance->getFieldValue('doc_instance_is_link');
    if ($mbIsLink) {
    	// Link
	  	FWDWebLib::getObject('document_link_label')->setValue(FWDLanguage::getPHPStringValue('rs_link','Link').':');
	  	FWDWebLib::getObject('document_link')->setValue($moDocInstance->getFieldValue('doc_instance_link'));
	  	$this->coWriter->drawLine(FWDWebLib::getObject('document_header_link'),$moDocLevel->getOffsets());
    }
    else {  	
	  	// Nome e tamanho do arquivo f�sico  	  	
	  	FWDWebLib::getObject('document_filename_label')->setValue(FWDLanguage::getPHPStringValue('rs_file','Arquivo').':');
	  	$miFileSize = $moDocInstance->getFieldValue('doc_instance_file_size');
	  	$maSizeName = array("GB", "MB", "KB", "B");
	    $msFinalFileSize = number_format($miFileSize,2)." ".array_pop($maSizeName);
	    while (($miFileSize/1024)>1) {
	     $miFileSize=$miFileSize/1024;
	     $msFinalFileSize = number_format($miFileSize,2)." ".array_pop($maSizeName);
	    }  	
	  	FWDWebLib::getObject('document_filename')->setValue($moDocInstance->getFieldValue('doc_instance_file_name').' (' . $msFinalFileSize . ')');
	  	$this->coWriter->drawLine(FWDWebLib::getObject('document_header_filename'),$moDocLevel->getOffsets());
    }
    
    // Vers�o
  	FWDWebLib::getObject('document_version_label')->setValue(FWDLanguage::getPHPStringValue('rs_version','Vers�o').':');
  	$msVersion = $moDocInstance->getVersion();
    if (!$msVersion) $msVersion="0.0";
  	FWDWebLib::getObject('document_version')->setValue($msVersion);
  	$this->coWriter->drawLine(FWDWebLib::getObject('document_header_version'),$moDocLevel->getOffsets());
  	
  	// Autor
  	FWDWebLib::getObject('document_author_label')->setValue(FWDLanguage::getPHPStringValue('rs_author','Autor').':');
  	$moAuthor = new ISMSUser();
  	$moAuthor->fetchById($moDocument->getFieldValue('document_author'));  	
  	FWDWebLib::getObject('document_author')->setValue($moAuthor->getFieldValue('user_name'));
  	$this->coWriter->drawLine(FWDWebLib::getObject('document_header_author'),$moDocLevel->getOffsets());
  	
  	// Respons�vel
  	FWDWebLib::getObject('document_responsible_label')->setValue(FWDLanguage::getPHPStringValue('rs_responsible','Respons�vel').':');
  	$moResponsible = new ISMSUser();
  	$moResponsible->fetchById($moDocument->getFieldValue('document_main_approver'));  	
  	FWDWebLib::getObject('document_responsible')->setValue($moResponsible->getFieldValue('user_name'));
  	$this->coWriter->drawLine(FWDWebLib::getObject('document_header_responsible'),$moDocLevel->getOffsets());
  	
  	// Tipo
  	FWDWebLib::getObject('document_type_label')->setValue(FWDLanguage::getPHPStringValue('rs_type','Tipo').':');
  	$miType = $moDocument->getFieldValue('document_type');
  	$msType = '';
  	if ($miType == DOCUMENT_TYPE_MANAGEMENT) {
    	$msType = FWDLanguage::getPHPStringValue('rs_document_type_management','Gest�o');
    }
    else if ($miType == DOCUMENT_TYPE_OTHERS) {
    	$msType = FWDLanguage::getPHPStringValue('rs_documents_type_others','Outros');
    }
    else {
      $moContextObject = new ISMSContextObject();
      $moContext = $moContextObject->getContextObject($miType);      
      $msType = ($moContext ? $moContext->getLabel() : FWDLanguage::getPHPStringValue('mx_without_type','Sem Tipo'));
    }
  	FWDWebLib::getObject('document_type')->setValue($msType);
  	$this->coWriter->drawLine(FWDWebLib::getObject('document_header_type'),$moDocLevel->getOffsets());
  	
  	// Publica��o
  	FWDWebLib::getObject('document_published_date_label')->setValue(FWDLanguage::getPHPStringValue('rs_published_date','Publica��o').':');
  	if ($moDocument->getFieldValue('document_date_production'))
  		FWDWebLib::getObject('document_published_date')->setValue(ISMSLib::getISMSShortDate($moDocument->getFieldValue('document_date_production')));
  	else
  		FWDWebLib::getObject('document_published_date')->setValue('---');
  	$this->coWriter->drawLine(FWDWebLib::getObject('document_header_published_date'),$moDocLevel->getOffsets());
  	
  	// Prazo  	
  	FWDWebLib::getObject('document_deadline_label')->setValue(FWDLanguage::getPHPStringValue('rs_deadline','Prazo').':');
  	if ($moDocument->getFieldValue('document_deadline'))
  		FWDWebLib::getObject('document_deadline')->setValue(ISMSLib::getISMSShortDate($moDocument->getFieldValue('document_deadline')));
  	else
  		FWDWebLib::getObject('document_deadline')->setValue('---');
  	$this->coWriter->drawLine(FWDWebLib::getObject('document_header_deadline'),$moDocLevel->getOffsets());
  	
  	// Descri��o
  	FWDWebLib::getObject('document_description_label')->setValue(FWDLanguage::getPHPStringValue('rs_description','Descri��o').':');  	
  	FWDWebLib::getObject('document_description')->setValue(trim($moDocument->getFieldValue('document_description')) ? $moDocument->getFieldValue('document_description') : '---');
  	$this->coWriter->drawLine(FWDWebLib::getObject('document_header_description'),$moDocLevel->getOffsets());
  	
  	// Palavras-Chave
  	FWDWebLib::getObject('document_keywords_label')->setValue(FWDLanguage::getPHPStringValue('rs_keywords','Palavras-chave').':');  	
  	FWDWebLib::getObject('document_keywords')->setValue(trim($moDocument->getFieldValue('document_keywords')) ? $moDocument->getFieldValue('document_keywords') : '---');
  	$this->coWriter->drawLine(FWDWebLib::getObject('document_header_keywords'),$moDocLevel->getOffsets());
  	
  	/*************** Lista de Aprovadores ************/
  	$this->coWriter->drawLine($moBlankLine,$moDocLevel->getOffsets());
  	$moSection->setValue(FWDLanguage::getPHPStringValue('rs_approvers_list_section','Lista de Aprovadores'));
  	$this->coWriter->drawLine($moHeaderSection,$moDocLevel->getOffsets());
  	$this->coWriter->drawLine($moBlankLine,$moDocLevel->getOffsets());
  	
  	$moQueryApprovers = new QueryDocumentApprovers(FWDWebLib::getConnection(),$miDocId);
	  $moQueryApprovers->makeQuery();
	  $moQueryApprovers->executeQuery();
	  $maApprovers = $moQueryApprovers->getApproversNames();
	  
	  if (count($maApprovers)) {
		  foreach ($maApprovers as $msName) {
		  	FWDWebLib::getObject('user_icon')->setAttrSrc(FWDWebLib::getInstance()->getGfxRef().'icon-user.png');
		  	FWDWebLib::getObject('document_user')->setValue($msName);
		  	$this->coWriter->drawLine(FWDWebLib::getObject('document_list_user'),$moDocLevel->getOffsets());	
		  }
	  }
	  else {
	  	FWDWebLib::getObject('message')->setValue(FWDLanguage::getPHPStringValue('rs_no_approvers_message','Esse documento n�o possui aprovadores.'));
		  $this->coWriter->drawLine(FWDWebLib::getObject('message_line'),$moDocLevel->getOffsets());
	  }
	  
	  /*************** Lista de Leitores ************/
  	$this->coWriter->drawLine($moBlankLine,$moDocLevel->getOffsets());
  	$moSection->setValue(FWDLanguage::getPHPStringValue('rs_readers_list_section','Lista de Leitores'));
  	$this->coWriter->drawLine($moHeaderSection,$moDocLevel->getOffsets());
  	$this->coWriter->drawLine($moBlankLine,$moDocLevel->getOffsets());
  	
  	$moQueryReaders = new QueryDocumentReaders(FWDWebLib::getConnection(),$miDocId);
	  $moQueryReaders->makeQuery();
	  $moQueryReaders->executeQuery();
	  $maReaders = $moQueryReaders->getReaders();
	  
	  if (count($maReaders)) {
		  foreach ($maReaders as $msName) {
		  	FWDWebLib::getObject('user_icon')->setAttrSrc(FWDWebLib::getInstance()->getGfxRef().'icon-user.png');
		  	FWDWebLib::getObject('document_user')->setValue($msName);
		  	$this->coWriter->drawLine(FWDWebLib::getObject('document_list_user'),$moDocLevel->getOffsets());	
		  }
	  }
	  else {
	  	FWDWebLib::getObject('message')->setValue(FWDLanguage::getPHPStringValue('rs_no_readers_message','Esse documento n�o possui leitores.'));
		  $this->coWriter->drawLine(FWDWebLib::getObject('message_line'),$moDocLevel->getOffsets());
	  }
	  
	  /*************** Lista de Subdocumentos ************/
  	$this->coWriter->drawLine($moBlankLine,$moDocLevel->getOffsets());
  	$moSection->setValue(FWDLanguage::getPHPStringValue('rs_subdocs_list_section','Lista de Subdocumentos'));
  	$this->coWriter->drawLine($moHeaderSection,$moDocLevel->getOffsets());
  	$this->coWriter->drawLine($moBlankLine,$moDocLevel->getOffsets());
  	
  	$moQuerySubDocs = new QueryDocumentSubDocs(FWDWebLib::getConnection(),$miDocId);
	  $moQuerySubDocs->makeQuery();
	  $moQuerySubDocs->executeQuery();
	  $maSubDocs = $moQuerySubDocs->getSubDocs();
	  
	  if (count($maSubDocs)) {
		  $moDoc = new PMDocument();
		  foreach ($maSubDocs as $miId => $msName) {
		  	FWDWebLib::getObject('subdoc_icon')->setAttrSrc(FWDWebLib::getInstance()->getGfxRef().$moDoc->getIcon($miId));
		  	FWDWebLib::getObject('document_subdoc')->setValue($msName);
		  	$this->coWriter->drawLine(FWDWebLib::getObject('document_list_subdocs'),$moDocLevel->getOffsets());	
		  }
	  }
	  else {
	  	FWDWebLib::getObject('message')->setValue(FWDLanguage::getPHPStringValue('rs_no_subdocs_message','Esse documento n�o possui subdocumentos.'));
		  $this->coWriter->drawLine(FWDWebLib::getObject('message_line'),$moDocLevel->getOffsets());
	  }
	  
	  /*************** Lista de Registros ************/
  	$this->coWriter->drawLine($moBlankLine,$moDocLevel->getOffsets());
  	$moSection->setValue(FWDLanguage::getPHPStringValue('rs_registers_list_section','Lista de Registros'));
  	$this->coWriter->drawLine($moHeaderSection,$moDocLevel->getOffsets());
  	$this->coWriter->drawLine($moBlankLine,$moDocLevel->getOffsets());
  	
  	$moQueryRegs = new QueryDocumentRegisters(FWDWebLib::getConnection(),$miDocId);
	  $moQueryRegs->makeQuery();
	  $moQueryRegs->executeQuery();
	  $maRegs = $moQueryRegs->getRegisters();
	  
	  if (count($maRegs)) {
		  foreach ($maRegs as $msName) {		  	
		  	FWDWebLib::getObject('document_register')->setValue($msName);
		  	$this->coWriter->drawLine(FWDWebLib::getObject('document_list_registers'),$moDocLevel->getOffsets());	
		  }
	  }
	  else {
	  	FWDWebLib::getObject('message')->setValue(FWDLanguage::getPHPStringValue('rs_no_registers_message','Esse documento n�o possui registros.'));
		  $this->coWriter->drawLine(FWDWebLib::getObject('message_line'),$moDocLevel->getOffsets());
	  }
	  
	  /*************** Lista de Refer�ncias ************/
  	$this->coWriter->drawLine($moBlankLine,$moDocLevel->getOffsets());
  	$moSection->setValue(FWDLanguage::getPHPStringValue('rs_references_list_section','Lista de Refer�ncias'));
  	$this->coWriter->drawLine($moHeaderSection,$moDocLevel->getOffsets());
  	$this->coWriter->drawLine($moBlankLine,$moDocLevel->getOffsets());
  	
  	$moQueryRefs = new QueryDocumentReferences(FWDWebLib::getConnection(),$miDocId);
	  $moQueryRefs->makeQuery();
	  $moQueryRefs->executeQuery();
	  $maRefs = $moQueryRefs->getReferences();
	  
	  if (count($maRefs)) {
		  foreach ($maRefs as $maInfo) {		  	
		  	FWDWebLib::getObject('document_reference')->setValue($maInfo[0] . ' (' . $maInfo[1] . ')');
		  	$this->coWriter->drawLine(FWDWebLib::getObject('document_list_references'),$moDocLevel->getOffsets());	
		  }
	  }
	  else {
	  	FWDWebLib::getObject('message')->setValue(FWDLanguage::getPHPStringValue('rs_no_references_message','Esse documento n�o possui refer�ncias.'));
		  $this->coWriter->drawLine(FWDWebLib::getObject('message_line'),$moDocLevel->getOffsets());
	  }
	  
	  /*************** Lista de Componentes ************/
  	$this->coWriter->drawLine($moBlankLine,$moDocLevel->getOffsets());
  	$moSection->setValue(FWDLanguage::getPHPStringValue('rs_components_list_section','Lista de Componentes'));
  	$this->coWriter->drawLine($moHeaderSection,$moDocLevel->getOffsets());
  	$this->coWriter->drawLine($moBlankLine,$moDocLevel->getOffsets());
  	
  	$moQueryComps = new QueryDocumentComponents(FWDWebLib::getConnection(),$miDocId);
	  $moQueryComps->makeQuery();
	  $moQueryComps->executeQuery();
	  $maComps = $moQueryComps->getComponents();	  
	  if (count($maComps)) {	  	
		  foreach ($maComps as $maInfo) {
		  	$moContext = ISMSContextObject::getContextObject($maInfo[0]);		  	
		  	FWDWebLib::getObject('comp_icon')->setAttrSrc(FWDWebLib::getInstance()->getGfxRef().$moContext->getIcon());
		  	FWDWebLib::getObject('document_component')->setValue($maInfo[1]);
		  	$this->coWriter->drawLine(FWDWebLib::getObject('document_list_components'),$moDocLevel->getOffsets());	
		  }
	  }
	  else {
	  	FWDWebLib::getObject('message')->setValue(FWDLanguage::getPHPStringValue('rs_no_components_message','Esse documento n�o possui componentes.'));
		  $this->coWriter->drawLine(FWDWebLib::getObject('message_line'),$moDocLevel->getOffsets());
	  }
  }
}
?>