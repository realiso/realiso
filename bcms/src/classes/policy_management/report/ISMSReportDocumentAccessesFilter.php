<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe ISMSReportDocumentAccessesFilter.
 *
 * <p>Classe que implementa o filtro do relat�rio de usu�rios que acessaram um
 * documento.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportDocumentAccessesFilter extends FWDReportFilter {

  protected $cbShowAllReads;
  protected $caDocuments = array();
  protected $ciInitialDate = 0;
  protected $ciFinalDate = 0;

  public function setShowAllReads($pbShowAllReads){
    $this->cbShowAllReads = $pbShowAllReads;
  }
  
  public function getShowAllReads(){
    return $this->cbShowAllReads;
  }

  public function getDocuments(){
    return $this->caDocuments;
  }

  public function setDocuments($paDocuments){
    $this->caDocuments = $paDocuments;
  }

  public function getInitialDate(){
    return $this->ciInitialDate;
  }

  public function setInitialDate($piInitialDate){
    $this->ciInitialDate = $piInitialDate;
  }

  public function getFinalDate(){
    return $this->ciFinalDate;
  }

  public function setFinalDate($piFinalDate){
    $this->ciFinalDate = $piFinalDate;
  }

  public function getSummary(){
    $maFilters = array();
    
    $maFilter = array(
      'name' => FWDLanguage::getPHPStringValue('rs_accesses','Acessos'),
      'items' => array()
    );
    if($this->cbShowAllReads){
      $maFilter['items'][] = FWDLanguage::getPHPStringValue('rs_show_all','Exibir todos');
    }else{
      $maFilter['items'][] = FWDLanguage::getPHPStringValue('rs_only_last','Somente o �ltimo');
    }
    $maFilters[] = $maFilter;
    
    if(count($this->caDocuments)){
      $maFilter = array(
        'name' => FWDLanguage::getPHPStringValue('rs_documents','Documentos'),
        'items' => array()
      );
      foreach($this->caDocuments as $miId){
        $moDocument = new PMDocument();
        $moDocument->fetchById($miId);
        $maFilter['items'][] = $moDocument->getName();
      }
      $maFilters[] = $maFilter;
    }
    
    $msInitialDate = ($this->ciInitialDate?ISMSLib::getISMSShortDate($this->ciInitialDate,true):'');
    $msFinalDate = ($this->ciFinalDate?ISMSLib::getISMSShortDate($this->ciFinalDate,true):'');
    if($msInitialDate || $msFinalDate){
      if($msInitialDate){
        if($msFinalDate){
          $msDateFilter = FWDLanguage::getPHPStringValue('rs_between_initialfinaldate','Entre %initialDate% e %finalDate%');
          $msDateFilter = str_replace(array('%initialDate%','%finalDate%'),array($msInitialDate,$msFinalDate),$msDateFilter);
        }else{
          $msDateFilter = FWDLanguage::getPHPStringValue('rs_after_initialdate','Depois de %initialDate%');
          $msDateFilter = str_replace('%initialDate%',$msInitialDate,$msDateFilter);
        }
      }else{
        $msDateFilter = FWDLanguage::getPHPStringValue('rs_before_finaldate','Antes de %finalDate%');
        $msDateFilter = str_replace('%finalDate%',$msFinalDate,$msDateFilter);
      }
      $maFilters[] = array(
        'name' => FWDLanguage::getPHPStringValue('rs_date','Data'),
        'items' => array($msDateFilter)
      );
    }
    
    return $maFilters;
  }

}
?>