<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportDocumentsRevisionLate.
 *
 * <p>Classe que implementa o relat�rio de documentos com revis�o atrasada.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportDocumentsRevisionLate extends ISMSReport {
  
  public function __construct(){
    parent::__construct();
  }
  
 /**
  * Inicializa o relat�rio.
  *
  * <p>M�todo para inicializar o relat�rio.
  * Necess�rio para funcionar com o XML compilado.</p>
  * @access public
  */
  public function init(){
    parent::init();
    $this->coDataSet->addFWDDBField(new FWDDBField('d.fkContext'      ,'document_id'           ,DB_NUMBER  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('d.sName'          ,'document_name'         ,DB_STRING  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('di.sFileName'     ,'document_file_name'    ,DB_STRING  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('di.bIsLink'       ,'document_is_link'      ,DB_NUMBER  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('d.fkSchedule'     ,'document_schedule_id'  ,DB_NUMBER  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('s.dNextOccurrence','document_next_revision',DB_DATETIME));
  }
  
 /**
  * Executa a query do relat�rio.
  *
  * <p>M�todo para executar a query do relat�rio.</p>
  * @access public
  */
  public function makeQuery(){
    $msCurrentDate = FWDWebLib::timestampToDBFormat(FWDWebLib::getTime());
    $this->csQuery = "SELECT
                        d.fkContext AS document_id,
                        d.sName AS document_name,
                        di.sFileName AS document_file_name,
                        di.bIsLink AS document_is_link,
                        d.fkSchedule AS document_schedule_id,
                        s.dNextOccurrence AS document_next_revision
                      FROM
                        view_pm_published_docs d
                        JOIN pm_doc_instance di ON (di.fkContext = d.fkCurrentVersion)
                        JOIN wkf_schedule s ON (s.pkSchedule = d.fkSchedule)
                      WHERE s.dNextOccurrence < {$msCurrentDate}
                      ORDER BY d.sName";
    return parent::executeQuery();
  }

}

?>