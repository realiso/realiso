<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe ISMSReportUsersWithPendantRead.
 *
 * <p>Classe que implementa o relat�rio de documentos que possuem leitores que ainda n�o leram- leitores com leitura pendente.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportUsersWithPendantRead extends ISMSReport {
  
 /**
  * Inicializa o relat�rio.
  *
  * <p>M�todo para inicializar o relat�rio.
  * Necess�rio para funcionar com o XML compilado.</p>
  * @access public
  */
  public function init(){
    parent::init();
    $this->coDataSet->addFWDDBField(new FWDDBField('da.fkUser'   ,'doc_reader_id'         ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('u.sName'     ,'doc_reader_name'       ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('d.fkContext' ,'doc_id'                ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('d.sName'     ,'doc_name'              ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('di.sFileName','doc_instance_file_name',DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('di.bIsLink'  ,'doc_instance_is_link'  ,DB_NUMBER));
    
  }
  
  /**
   * Executa a query do relat�rio.
   *
   * <p>M�todo para executar a query do relat�rio.</p>
   * @access public
   */
  public function makeQuery(){

    
    $maFilters = array();
    
    $maIdFilter = $this->coFilter->getUserId();
    
    
    if(count($maIdFilter)){
      $maFilters[] = " u.fkContext IN ( " .implode(',',$maIdFilter) ." ) ";
    }

    if(count($maFilters)){
      $msWhere = " AND " . implode(' AND ',$maFilters);
    }else{
      $msWhere = '';
    }
    $this->csQuery = "
SELECT da.fkUser AS doc_reader_id,
       u.sName AS doc_reader_name,
       d.fkContext AS doc_id,
       d.sName AS doc_name,
       di.sFileName AS doc_instance_file_name,
       di.bIsLink AS doc_instance_is_link
   FROM view_pm_published_docs d
        JOIN view_pm_doc_instance_active di ON ( di.fkContext = d.fkCurrentVersion )
        JOIN view_pm_doc_readers_active da ON ( d.fkContext = da.fkDocument AND bDenied = 0 )
  JOIN view_isms_user_active u ON (da.fkUser = u.fkContext)
   WHERE NOT EXISTS ( SELECT *
                        FROM pm_doc_read_history rh
                        WHERE rh.fkInstance = d.fkCurrentVersion AND rh.fkUser = da.fkUser
                    )
   $msWhere
   ORDER BY u.sName, d.sName, d.fkContext
    ";
    return parent::executeQuery();
  }
  
 /**
  * Desenha o cabe�alho do relat�rio.
  *
  * <p>M�todo para desenhar o cabe�alho do relat�rio.</p>
  * @access public
  */
  public function drawHeader(){
    foreach($this->caHeaders as $moHeader){
      if($moHeader->getAttrName()=='header_filter'){
        $moHeaderFilter = $moHeader;
      }elseif($moHeader->getAttrName()=='header_filter_item'){
        $moHeaderFilterItem = $moHeader;
      }elseif($moHeader->getAttrName()=='report_header_blank'){
        $moHeaderBlank = $moHeader;
      }else{
        $this->coWriter->drawLine($moHeader,array());
      }
    }
    if($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());
    
    $moFilterText = FWDWebLib::getObject('header_filter_text');
    $moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
    $maFilters = $this->coFilter->getSummary();
    foreach($maFilters as $maFilter){
      $moFilterText->setValue($maFilter['name']);
      $this->coWriter->drawLine($moHeaderFilter,array());
      foreach($maFilter['items'] as $msFilterItem){
        $moFilterItemText->setValue($msFilterItem);
        $this->coWriter->drawLine($moHeaderFilterItem,array());
      }
    }
    $this->coWriter->drawLine($moHeaderBlank,array());
  }

}
?>