<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe ISMSReportUsersByProcess.
 *
 * <p>Classe que implementa o relat�rio de usu�rios por processo.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportUsersByProcess extends ISMSReport {
  
 /**
  * Inicializa o relat�rio.
  *
  * <p>M�todo para inicializar o relat�rio.
  * Necess�rio para funcionar com o XML compilado.</p>
  * @access public
  */
  public function init(){
    parent::init();
    $this->coDataSet->addFWDDBField(new FWDDBField('a.fkContext','area_id'      ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('a.sName'    ,'area_name'    ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('a.nValue'   ,'area_value'   ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('p.fkContext','process_id'   ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('p.sName'    ,'process_name' ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('p.nValue'   ,'process_value',DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('u.fkContext','user_id'      ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('u.sName'    ,'user_name'    ,DB_STRING));
  }
  
  /**
   * Executa a query do relat�rio.
   *
   * <p>M�todo para executar a query do relat�rio.</p>
   * @access public
   */
  public function makeQuery(){
    $msOrderBy = 'process_name, user_name';
    if($this->coFilter->getOrganizeByArea()){
      $msOrderBy = "area_name, $msOrderBy";
    }
    
    $this->csQuery = "SELECT                        a.fkContext as area_id,                        a.sName as area_name,                        a.nValue as area_value,                        p.fkContext as process_id,                        p.sName as process_name,                        p.nValue as process_value,                        u.fkContext as user_id,                        u.sName as user_name                      FROM                        isms_user u                        JOIN pm_process_user pu ON (pu.fkUser = u.fkContext)                        JOIN rm_process p ON (p.fkContext = pu.fkProcess)                        JOIN rm_area a ON (a.fkContext = p.fkArea)                      ORDER BY $msOrderBy";
    return parent::executeQuery();
  }
  
 /**
  * Desenha o cabe�alho do relat�rio.
  *
  * <p>M�todo para desenhar o cabe�alho do relat�rio.</p>
  * @access public
  */
  public function drawHeader(){
    foreach($this->caHeaders as $moHeader){
      if($moHeader->getAttrName()=='header_filter'){
        $moHeaderFilter = $moHeader;
      }elseif($moHeader->getAttrName()=='header_filter_item'){
        $moHeaderFilterItem = $moHeader;
      }elseif($moHeader->getAttrName()=='report_header_blank'){
        $moHeaderBlank = $moHeader;
      }else{
        $this->coWriter->drawLine($moHeader,array());
      }
    }
    if($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());
    
    $moFilterText = FWDWebLib::getObject('header_filter_text');
    $moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
    $maFilters = $this->coFilter->getSummary();
    foreach($maFilters as $maFilter){
      $moFilterText->setValue($maFilter['name']);
      $this->coWriter->drawLine($moHeaderFilter,array());
      foreach($maFilter['items'] as $msFilterItem){
        $moFilterItemText->setValue($msFilterItem);
        $this->coWriter->drawLine($moHeaderFilterItem,array());
      }
    }
    $this->coWriter->drawLine($moHeaderBlank,array());
  }

}
?>