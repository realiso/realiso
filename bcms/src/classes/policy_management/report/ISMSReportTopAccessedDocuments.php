<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe ISMSReportTopAccessedDocuments.
 *
 * <p>Classe que implementa o relat�rio de documentos mais acessados, por ordem de quantidade de acessos.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportTopAccessedDocuments extends ISMSReport {
  
 /**
  * Inicializa o relat�rio.
  *
  * <p>M�todo para inicializar o relat�rio.
  * Necess�rio para funcionar com o XML compilado.</p>
  * @access public
  */
  public function init(){
    parent::init();
    $this->coDataSet->addFWDDBField(new FWDDBField('d.fkContext' ,'doc_id'                ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('d.sName'     ,'doc_name'              ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('di.sFileName','doc_instance_file_name',DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('di.bIsLink'  ,'doc_instance_is_link'  ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField(''            ,'access_count'          ,DB_NUMBER));
  }
  
  /**
   * Executa a query do relat�rio.
   *
   * <p>M�todo para executar a query do relat�rio.</p>
   * @access public
   */
  public function makeQuery(){
    $miTruncateNumber = $this->coFilter->getTruncateNumber();
    
    $maFilters = array();
    
    if($miTruncateNumber){
      $this->setAttrRowLimit($miTruncateNumber);
    }
    
    if(count($maFilters)){
      $msWhere = ' WHERE '.implode(' AND ',$maFilters);
    }else{
      $msWhere = '';
    }
    $this->csQuery = "SELECT                        d.fkContext as doc_id,                        d.sName as doc_name,                        di.sFileName as doc_instance_file_name,                        di.bIsLink as doc_instance_is_link,                        da.access_count as access_count                      FROM (                          SELECT                            d2.fkContext as doc_id,                            count(d2.fkContext) as access_count                          FROM                            view_pm_document_active d2                            LEFT JOIN view_pm_doc_instance_active di2 ON (di2.fkDocument = d2.fkContext)                            JOIN pm_doc_read_history dh ON (dh.fkInstance = di2.fkContext)                          GROUP BY d2.fkContext                        ) da                        JOIN view_pm_document_active d ON (d.fkContext = da.doc_id)                        JOIN view_pm_doc_instance_active di ON (di.fkContext = d.fkCurrentVersion)                      ORDER BY access_count DESC, doc_name";
    return parent::executeQuery();
  }
  
 /**
  * Desenha o cabe�alho do relat�rio.
  *
  * <p>M�todo para desenhar o cabe�alho do relat�rio.</p>
  * @access public
  */
  public function drawHeader(){
    foreach($this->caHeaders as $moHeader){
      if($moHeader->getAttrName()=='header_filter'){
        $moHeaderFilter = $moHeader;
      }elseif($moHeader->getAttrName()=='header_filter_item'){
        $moHeaderFilterItem = $moHeader;
      }elseif($moHeader->getAttrName()=='report_header_blank'){
        $moHeaderBlank = $moHeader;
      }else{
        $this->coWriter->drawLine($moHeader,array());
      }
    }
    if($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());
    
    $moFilterText = FWDWebLib::getObject('header_filter_text');
    $moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
    $maFilters = $this->coFilter->getSummary();
    foreach($maFilters as $maFilter){
      $moFilterText->setValue($maFilter['name']);
      $this->coWriter->drawLine($moHeaderFilter,array());
      foreach($maFilter['items'] as $msFilterItem){
        $moFilterItemText->setValue($msFilterItem);
        $this->coWriter->drawLine($moHeaderFilterItem,array());
      }
    }
    $this->coWriter->drawLine($moHeaderBlank,array());
  }

}
?>