<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe PMDocContext
 *
 * <p>Classe que representa a tabela de associação de contexto com documento.</p>
 * @package ISMS
 * @subpackage classes
 */
class PMDocContext extends ISMSTable {

 /**
  * Construtor.
  *
  * <p>Construtor da classe PMDocContext.</p>
  * @access public
  */
  public function __construct(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    parent::__construct('pm_doc_context');
    $this->coDataset->addFWDDBField(new FWDDBField('fkContext',   'context_id',   DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('fkDocument',  'document_id',  DB_NUMBER));
  }
  
  public function getCurrentContexts($piDocumentId) {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $moDocContext = new PMDocContext();    
    $moDocContext->createFilter($piDocumentId,'document_id');
    $moDocContext->select();
    $maContexts = array();
    while($moDocContext->fetch()){
      $maContexts[] = $moDocContext->getFieldValue('context_id');
    }    
    return $maContexts;
  }

  public function updateAutoApprovers($piDocumentId) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    //Apaga todos os aprovadores automáticos
    $moDocumentApprover = new PMDocumentApprover();
    $moDocumentApprover->createFilter($piDocumentId,'document_id');
    $moDocumentApprover->createFilter(false,'manual');
    $moDocumentApprover->createFilter(true,'is_context_approver');
    $moDocumentApprover->delete();
    
    //Atualiza os aprovadores inseridos manualmente e responsaveis por contexto.
    $moDocumentApprover = new PMDocumentApprover();
    $moDocumentApprover->createFilter($piDocumentId,'document_id');
    $moDocumentApprover->createFilter(true,'manual');
    $moDocumentApprover->createFilter(true,'is_context_approver');
    $moDocumentApprover->select();
    while($moDocumentApprover->fetch()) {
      $moDocumentApprover->setFieldValue('is_context_approver',false);
      $moDocumentApprover->update();
    }
    
    //Insere os atuais
    $maContexts = $this->getCurrentContexts($piDocumentId);    
    $maApprovers = array();
    foreach($maContexts as $miContextId) {
      $moContextObject = new ISMSContextObject();
      $moContext = $moContextObject->getContextObjectByContextId($miContextId);
      $moContext->fetchById($miContextId);
      $miApproverId = $moContext->getResponsible();
      if ($miApproverId) $maApprovers[] = $miApproverId;
    }
    if (count($maApprovers)) {
      $moDocumentApprover = new PMDocumentApprover();
      $moDocumentApprover->autoInsertOrUpdateUsers($piDocumentId,$maApprovers);
    }
  }
}
?>