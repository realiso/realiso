<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSTable.
 *
 * <p>Classe abstrata que representa uma tabela do sistema.</p>
 * @package ISMS
 * @subpackage classes
 */
abstract class ISMSTable extends FWDDBTable {	
  
 /**
  * Busca informa��es de um registro atrav�s de seu identificador.
  * 
  * <p>M�todo para buscar informa��es de um registro atrav�s de seu identificador.</p>
  * @access public 
  * @param integer $piId Identificador 
  */ 
  public function fetchById($piId) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);  	
  	return parent::fetchById($piId, $this->csAliasId);
  } 
 
 /**
  * Atualiza resgistros das tabelas do sistema.
  * 
  * <p>M�todo para atualizar registros da tabela do sistema.</p>
  * @access public
  * @param integer $piId Identificador 
  */  
  public function update($piId = 0) {  	
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
	if ($piId) $this->createFilter($piId, $this->csAliasId);
	parent::update();		
  }
  
 /**
  * Remove resgistros das tabelas do sistema.
  * 
  * <p>M�todo para remover registros das tabelas do sistema.</p>
  * @access public
  * @param integer $piId Identificador 
  */  
  public function delete($piId = 0) {  
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);	
    if ($piId) $this->createFilter($piId, $this->csAliasId);
	parent::delete();		
  }
  
 /**
  * Retorna o alias do identificador da tabela.
  * 
  * <p>M�todo para retornar o alias do identificador da tabela.</p>
  * @access public
  * @return string Alias do identificador 
  */
  public function getIdAlias() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
  	return $this->csAliasId;
  }

 /**
  * Preenche um objeto de interface a partir de um campo da tabela.
  * 
  * <p>Preenche um objeto de interface a partir de um campo da tabela.</p>
  * @access public
  * @param string psObjectName Nome do objeto de interface
  * @param string $psFieldAlias Alias do campo
  * @return boolean True sse o objeto foi preenchido com sucesso
  */
  public function fillObjectFromField($psObjectName,$psFieldAlias){
    $moObject = FWDWebLib::getObject($psObjectName);
    $moField = $this->getFieldByAlias($psFieldAlias);
    if($moObject && $moField){
      if($moField->getType()==DB_DATETIME){
        $moObject->setValue($moField->getValue()?ISMSLib::getISMSShortDate($moField->getValue()):"");
        return true;
      }else{
        return parent::fillObjectFromField($psObjectName,$psFieldAlias);
      }
    }
    return false;
  }

}
?>