<?php

include $handlers_ref . "QueryUserPasswordHistory.php";

/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSUserPasswordHistory.
 *
 * <p>Classe que representa a tabela de hist�rico de senhas de usu�rios.</p>
 * @package ISMS
 * @subpackage classes
 */
class ISMSUserPasswordHistory extends ISMSTable {

 /**
  * Construtor.
  * 
  * <p>Construtor da classe ISMSUserPasswordHistory.</p>
  * @access public 
  */
  public function __construct(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    parent::__construct("isms_user_password_history");

    $this->csAliasId = 'user_id';

    $this->coDataset->addFWDDBField(new FWDDBField("fkUser",                "user_id",                     DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("sPassword",             "user_password",               DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField("dDatePasswordChanged",  "user_date_password_changed",  DB_DATETIME));
  }
  
 /**
  * Busca informa��es de um usu�rio atrav�s de seu id.
  * 
  * <p>M�todo para buscar informa��es de um usu�rio atrav�s de id.</p>
  * @access public 
  * @param integer $piUserId Id do usu�rio
  */ 
  public function fetchById($piUserId) {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $this->createFilter($piUserId, 'user_id');
    $this->select();
    return $this->fetch();
  }
  
  /**
   * Verifica se a senha passada poder� ser usada.
   * 
   * <p>Verifica se a senha passada poder� ser usada, i.e., se 
   * a senha n�o consta na tabela de hist�rico de senhas</p>
   * 
   * @access public
   * @param string $psPassword Senha em md5
   * @param integer $piUserId Id do usu�rio (opcional). Se n�o passado, utiliza o usu�rio atual.
   */
  public function checkPassword($psPassword,$piUserId='') {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $miUserId = $piUserId?$piUserId:FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    $moUserPasswordHistory = new ISMSUserPasswordHistory();
    $moUserPasswordHistory->createFilter($miUserId,'user_id');
    $moUserPasswordHistory->createFilter($psPassword,'user_password');
    $moUserPasswordHistory->select();
    if($moUserPasswordHistory->fetch()) return false;
    else return true; 
  }
  
  /**
   * Acrescenta uma senha na tabela de hist�rico.
   * 
   * <p>Acrescenta uma senha na tabela de hist�rico.</p>
   * @access public
   * @param string $psPassword Senha em md5
   * @param integer $piUserId Id do usu�rio (opcional). Se n�o for passado, utiliza o usu�rio atual.
   */
  public function addPassword($psPassword,$piUserId='') {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

    $miUserId = $piUserId?$piUserId:FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    $moQuery = new QueryUserPasswordHistory(FWDWebLib::getConnection());
    $moQuery->setUserId($miUserId);
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    $maPasswordHistory = $moQuery->getPasswordHistory();
    $miMaxHistory = ISMSLib::getConfigById(PASSWORD_HISTORY_NUM);
    if ($miMaxHistory > 0 && count($maPasswordHistory) >= $miMaxHistory) {
      for($miI=$miMaxHistory-1;$miI<count($maPasswordHistory);$miI++) {
        $moUserPasswordHistory = new ISMSUserPasswordHistory();
        $moUserPasswordHistory->createFilter($maPasswordHistory[$miI][0],'user_id');
        $moUserPasswordHistory->createFilter(ISMSLib::getTimestamp($maPasswordHistory[$miI][1]),'user_date_password_changed');
        $moUserPasswordHistory->delete();
      }
    }
    
    $moUserPasswordHistory = new ISMSUserPasswordHistory();
    $moUserPasswordHistory->setFieldValue('user_id',$miUserId);
    $moUserPasswordHistory->setFieldValue('user_password',$psPassword);
    $moUserPasswordHistory->setFieldValue('user_date_password_changed',ISMSLib::ISMSTime());
    $moUserPasswordHistory->insert();
  }
  
  /**
   * M�todo para retornar a data e hora da �ltima altera��o de senha do usu�rio atual.
   * 
   * <p>M�todo para retornar a data e hora da �ltima altera��o de senha do usu�rio atual.</p>
   * @access public;
   * @param integer $piUserId Id do usu�rio (opcional). Se n�o for passado, utiliza o usu�rio atual.
   * @return string Data e hora da �ltima altera��o de senha.
   */
  public function getCurrentPasswordTime($piUserId='') {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $miUserId = $piUserId?$piUserId:FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    $moQuery = new QueryUserPasswordHistory(FWDWebLib::getConnection());
    $moQuery->setUserId($miUserId);
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    
    $maPasswordHistory = $moQuery->getPasswordHistory();
    
    if (isset($maPasswordHistory[0]))
      return ISMSLib::getTimestamp($maPasswordHistory[0][1]);
    else
      return ISMSLib::ISMSTime();
  }
  
  /**
   * M�todo que verifica se a senha do usu�rio expirou.
   * 
   * <p>M�todo que verifica se a senha do usu�rio expirou.</p>
   * @access public
   * @param integer $piUserId Id do usu�rio (opcional). Se n�o for passado, utiliza o usu�rio atual.
   * @return Verdadeiro/Falso
   */
  public function isPasswordExpired($piUserId='') {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
	  $miUserId = $piUserId?$piUserId:FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    $msPasswordTime = $this->getCurrentPasswordTime($miUserId) + (86400*(ISMSLib::getConfigById(PASSWORD_CHANGE_FREQUENCY)+1));
    if ($msPasswordTime <= ISMSLib::ISMSTime()) //se for menor que a data de hoje, eh porque a senha eh antiga demais, e deve ser trocada.
      return true;
    else
      return false;
  }
  
  /**
   * M�todo que verifica se a senha do usu�rio vai expirar.
   * 
   * <p>M�todo que verifica se a senha do usu�rio vai expirar.</p>
   * @access public
   * @param integer $piUserId Id do usu�rio (opcional). Se n�o for passado, utiliza o usu�rio atual.
   * @return Verdadeiro/Falso
   */
  public function willPasswordExpire($piUserId='') {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $miUserId = $piUserId?$piUserId:FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    //msPasswordTime = Data da ultima senha + periodo de troca de senha - periodo de aviso
    $msPasswordTime = $this->getCurrentPasswordTime($miUserId) + (86400*(ISMSLib::getConfigById(PASSWORD_CHANGE_FREQUENCY)+1)) - (86400*ISMSLib::getConfigById(PASSWORD_DAYS_BEFORE_WARNING));
    if ($msPasswordTime <= ISMSLib::ISMSTime()) //se for menor que a data de hoje, eh porque a senha esta dentro do limite de aviso, ou jah expirou.
      return true;
    else
      return false;
  }
}
?>