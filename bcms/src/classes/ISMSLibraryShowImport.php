<?
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe ISMSLibraryShowImport. Classe para exibir as bibliotecas que podem ser importadas.
 * 
 * <p>Classe, disparada quando houver arquivo carregado, respons�vel por exibir os elementos carregados no arquivo.</p>
 * 
 * @package ISMS
 * @subpackage libraries
 */
class ISMSLibraryShowImport {
	protected $coLibraryImport;
	
	/**
	 * Construtor. Recebe um LibraryImport, com o arquivo criptografado, contendo os elementos que ser�o carregados.
	 * 
	 * @access public
	 * @param object poLibraryImport Class LibraryImport.
	 */
	public function __construct($poLibraryImport) {
		$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
		$this->coLibraryImport = $poLibraryImport;
	}

	/**
	 * Fun��o que faz a exibi��o dos elementos.
	 * 
	 * @access public
	 */
	public function showImportElements() {
		$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
		$moWebLib = FWDWebLib :: getInstance();

		/* Pega a lista de classes do LibraryImport */		
		$maClassList = $this->coLibraryImport->getClassList();
		/* Salva na sess�o a lista serializada*/
		$msSerializedClassList = serialize($maClassList);
		$moSession = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId());
		if (!$moSession->attributeExists("libraryimport"))
			$moSession->addAttribute("libraryimport");  
  	$moSession->setAttrLibraryImport($msSerializedClassList);
  	
		foreach($maClassList as $msLibrarySection=>$maLibrarySection) { //Passa por cada se��o do XML (category, sectionbestpractice, standard, template)
			$moTree = null;
      switch(strtolower($msLibrarySection)) {
				case "category":
					$moTree = $moWebLib->getObject("tree_import_category");
					$msTreeBaseName = "treebase_import_category";
					$msTreeController = "controller_import_category";
					$msDependency = "category_parent_id";
					break;
				case "sectionbestpractice":
					$moTree = $moWebLib->getObject("tree_import_sectionbestpractice");
					$msTreeBaseName = "treebase_import_sectionbestpractice";
					$msTreeController = "controller_import_sectionbestpractice";
					$msDependency = "section_parent_id";
					break;
				case "standard":
					$moTree = $moWebLib->getObject("tree_import_standard");
					$msTreeBaseName = "treebase_import_standard";
					$msTreeController = "controller_import_standard";
					$msDependency = "";
					break;
        case "template":
          $moTree = $moWebLib->getObject("tree_import_template");
          $msTreeBaseName = "treebase_import_template";
          $msTreeController = "controller_import_template";
          $msDependency = "";
          break;
				default:
					break;
			}
			if($moTree instanceof FWDTree){ //verifica se $moTree foi setado, i.e., se passou por uma das quatro se��es pr�-definidas.
				if (count($maLibrarySection)>0) //se houver ao menos um elemento na se��o, apagar o valor padr�o da �rvore.
					$moTree->cleanTree();
				foreach($maLibrarySection as $miIndex=>$maClass) { //cada �ndice � uma classe, logo tamb�m � um nodo
					$moTreeClass = new FWDTree();
					$moTreeClass->setAttrName('tree_import_'.strtolower($msLibrarySection).$miIndex);
					$moTreeClass->setTreeBaseName($msTreeBaseName);
					$moParentTree = null;
					if (isset($maClass["classId"])) { //quando o campo for id, adicionar checkbox.
						$miClassId = $maClass["classId"];
						$moTreeClassId = new FWDCheckBox();
						$moTreeClassId->setAttrName('checkbox_import_'.strtolower($msLibrarySection).$miClassId);
						$moTreeClassId->setAttrKey($miClassId);
						$moTreeClassId->setAttrCheck(true);
						$moTreeClassId->setAttrController($msTreeController);
						$moTreeClassId->execute();
						$moTreeClass->addObjFWDView($moTreeClassId);
						$maParentClass[$miClassId]=$moTreeClass;
					}
					if (isset($maClass["classValue"])) {
						$msClassValue = $maClass["classValue"];
						$moTreeClassValue= new FWDStatic();
						$moTreeClassValue->setAttrName('imp_'.strtolower($msLibrarySection).'_static'.$msClassValue);
						$moTreeClassValue->setValue($msClassValue);
						$moTreeClass->addObjFWDView($moTreeClassValue);
					}
					if ($msDependency && isset($maClass[$msDependency])) {
						$miClassParentId = $maClass[$msDependency];
						if ($miClassParentId && isset($maParentClass[$miClassParentId])) {
							$moParentTree = $maParentClass[$miClassParentId];
						}
					}
					if ($moParentTree)
						$moParentTree->addObjFWDTree($moTreeClass);
					else 
						$moTree->addObjFWDTree($moTreeClass);
				}
				$moTree->setIds();
			}
		}
    $moWebLib->getObject("export_viewgroup")->setAttrDisplay("false");
		$moWebLib->getObject("import_viewgroup")->setAttrDisplay("true");
	}
}
?>