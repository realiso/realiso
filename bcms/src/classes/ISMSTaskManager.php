<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */


/**
 * Classe ISMSTaskManager.
 *
 * <p>Classe que manipula as tasks do sistema.</p>
 * @package ISMS
 * @subpackage classes
 */
class ISMSTaskManager {
  
 /**
  * Array das tasks com aprova��o / nega��o automatica
  * @var array
  * @access protected
  */  
  protected $caAutomaticExec = array();
  
  /**
  * Array das tasks com aprova��o / nega��o manual
  * @var array
  * @access protected
  */  
  protected $caManualExec = array();


  /**
  * Construtor.
  * 
  * <p>Construtor da classe ISMSConfig.</p>
  * @access public 
  */
  public function __construct(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $this->caAutomaticExec = array(
      ACT_AREA_APPROVAL,
      ACT_PROCESS_APPROVAL,
      ACT_ASSET_APPROVAL,
      ACT_PROCESS_ASSET_APPROVAL,
      ACT_RISK_APPROVAL,
      ACT_CONTROL_APPROVAL,
      ACT_RISK_CONTROL_APPROVAL,
      ACT_RISK_LIMITS_APPROVAL,
      ACT_BEST_PRACTICE_APPROVAL,
      ACT_SECTION_BEST_PRACTICE_APPROVAL,
      ACT_STANDARD_APPROVAL,
      ACT_CATEGORY_APPROVAL,
      ACT_EVENT_APPROVAL,
      ACT_DOCUMENT_TEMPLATE_APPROVAL,
      ACT_DOCUMENT_APPROVAL,
      ACT_OCCURRENCE_APPROVAL,
      ACT_INC_DISPOSAL_APPROVAL,
      ACT_INC_SOLUTION_APPROVAL,
      ACT_NON_CONFORMITY_APPROVAL,
      ACT_AP_APPROVAL,
      ACT_ACCEPT_RISK_APPROVAL,
      ACT_INC_APPROVAL,
    );
    
    $this->caManualExec = array(
      ACT_REAL_EFFICIENCY, 
      ACT_CONTROL_TEST,
      ACT_ACTION_PLAN_FINISH_CONFIRM, 
      ACT_ACTION_PLAN_REVISION, 
      ACT_INC_CONTROL_INDUCTION,
      ACT_DOCUMENT_REVISION,
      ACT_NEW_RISK_PARAMETER_APPROVAL
    );

  }
   
  public function execTask($piTaskId, $piAction, $psJustification = '', $psAuxPar = ''){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    //teste para verificar se o sistema n�o est� sendo hakeado
    $moTaskTest = new WKFTask();
    $moTaskTest->fetchById($piTaskId);
    $moTaskTest->testPermissionToExecute($piTaskId);
    $miContextId = $moTaskTest->getFieldValue('task_context_id');
    
    if(in_array($moTaskTest->getFieldValue('task_activity'),$this->caAutomaticExec)){
      $moContextObject = new ISMSContextObject();
      $moContext = $moContextObject->getContextObjectByContextId($miContextId);
      $moContext->fetchById($miContextId);
      $moContext->stateForward($piAction, $psJustification);
    
      $moTask = new WKFTask();
      $moTask->setFieldValue('task_date_accomplished', ISMSLib::ISMSTime());
      $moTask->setFieldValue('task_is_visible', 0);
      $moTask->update($piTaskId);
      echo "if( window.refresh_grids ) window.refresh_grids();";
    }elseif(in_array($moTaskTest->getFieldValue('task_activity'), $this->caManualExec)){
      $moTask = new WKFTask();
      $moTask->fetchById($piTaskId);
      $moCtx = new ISMSContextObject();
      $moCtx->fetchById($miContextId);
      echo 'window.setTimeout("'.
        ISMSLib::getTaskPopup($piTaskId,$moTask->getFieldValue('task_activity'),$moTask->getFieldValue('task_context_id'),$moCtx->getFieldValue('context_type'))
      .',1");';
    }else{
      echo trigger_error("Error in ISMSTaskManager. Task Activity '".$moTaskTest->getFieldValue('task_activity')."' not defined in this class!",E_USER_ERROR);
    }
  }
 
 /**
  * Retorna um array com os ids das tasks que devem ser executadas manualmente pelo usu�rio.
  * 
  * <p>M�todo para retornar um array com os ids das tasks que devem ser executadas manualmente pelo usu�rio.</p>
  * @access public 
  * @return Array identificadores das tarefas que devem ser executadas manualmente pelo usu�rio
  */ 
  public function getManualExec(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    return $this->caManualExec;
  }
  
}
?>
