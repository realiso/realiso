<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSEmailTemplates.
 *
 * <p>Classe que cont�m os templates de email do sistema.</p>
 * @package ISMS
 * @subpackage classes
 */
class ISMSEmailTemplates {

  protected static $caTemplates = array();
  
  private function __construct(){}
  
  private static $cbHTML;
  
  private static $csMsgLink;
  private static $csMsgSentDate;
  private static $csMsgSentTime;
  private static $csMsgActivity;
  private static $csMsgSender;
  private static $csMsgTaskCreatedAt;
  private static $csMsgContextDescription;
  private static $csMsgAlertCreatedAt;
  private static $csMsgJustification;
  private static $csMsgTaskHeader;
  private static $csMsgAlertHeader;
  private static $csMsgNewPassword;
  private static $csMsgUser;
  private static $csMsgUsers;
  private static $csMsgTaskAndAlertTransferHeader;
  private static $csMsgTaskAndAlertTransfer;
  private static $csMsgControl;
  private static $csMsgControlImplementation;
  private static $csMsgLimit;
  private static $csMsgDisciplinaryProcess;
  private static $csMsgIncident;
  private static $csMsgDescription;
  private static $csMsgIncidentRiskParametrization;
  private static $csMsgAsset;
  private static $csMsgRisk;
  private static $csMsgOriginalParameters;
  private static $csMsgNewParameters;
  private static $csMsgNewPasswordRequest;
  private static $csMsgAbandonedSystem;
  private static $csMsgClient;
  private static $csMsgISMSFeedback;
  private static $csMsgFeedbackType;
  private static $csMsgFeedback;
  private static $csMsgISMSNewUser;
  private static $csMsgLogin;
  private static $csMsgPassword;
  private static $csMsgAccessISMS;
  
  protected static function buildTemplates(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

    self::$csMsgLink = FWDLanguage::getPHPStringValue('link','Link');
    self::$csMsgSentDate = FWDLanguage::getPHPStringValue('em_sent_on','enviado em');
    self::$csMsgSentTime = FWDLanguage::getPHPStringValue('em_at','�s');
    self::$csMsgActivity = FWDLanguage::getPHPStringValue('em_activity','Atividade');
    self::$csMsgSender = FWDLanguage::getPHPStringValue('em_sender','Remetente');
    self::$csMsgTaskCreatedAt = FWDLanguage::getPHPStringValue('em_created_on','Gerada em');
    self::$csMsgContextDescription = FWDLanguage::getPHPStringValue('em_description','Descri��o');
    self::$csMsgAlertCreatedAt = FWDLanguage::getPHPStringValue('em_created_on','Gerado em');
    self::$csMsgJustification = FWDLanguage::getPHPStringValue('em_observation','Observa��o');
    self::$csMsgUser = FWDLanguage::getPHPStringValue('em_user','Usu�rio');
    self::$csMsgUsers = FWDLanguage::getPHPStringValue('em_users','Usu�rios');
    self::$csMsgControl = FWDLanguage::getPHPStringValue('em_control','Controle');
    self::$csMsgLimit = FWDLanguage::getPHPStringValue('em_limit','Limite');
    self::$csMsgTaskHeader = FWDLanguage::getPHPStringValue('em_execute_task','<b>Prezado usu�rio do Real BCMS,</b><br>solicitamos que execute a(s) atividade(s) apontada(s) abaixo.<br>');
    self::$csMsgAlertHeader = FWDLanguage::getPHPStringValue('em_alert_messages_sent','<b>Prezado usu�rio do Real BCMS,</b><br>os seguintes alertas foram enviados para voc�.<br>');
    self::$csMsgNewPassword = FWDLanguage::getPHPStringValue('em_new_password_generated','<b>Prezado usu�rio do Real BCMS,</b><br>uma nova senha foi gerada para voc�.<br>Sua nova senha �');
    self::$csMsgTaskAndAlertTransferHeader = FWDLanguage::getPHPStringValue('em_tasks_and_alerts_transferred','<b>Prezado usu�rio do Real BCMS,</b><br>Um usu�rio do sistema foi removido, e suas tarefas e alertas foram transferidas a voc�.');
    self::$csMsgTaskAndAlertTransfer = FWDLanguage::getPHPStringValue('em_tasks_and_alerts_transferred_message','O usu�rio acima foi removido do sistema, passando suas tarefas e alertas para o seu nome.');
    self::$csMsgControlImplementation = FWDLanguage::getPHPStringValue('em_control_has_deadline','<b>Prezado usu�rio do Real BCMS,</b><br>o controle abaixo possui data limite para implementa��o.');
    self::$csMsgDisciplinaryProcess = FWDLanguage::getPHPStringValue('em_disciplinary_process','<b>Prezado Gestor de Processo Disciplinar,</b><br>O(s) seguinte(s) usu�rio(s) do sistema foi/foram adicionados a um processo disciplinar.');
    self::$csMsgIncident = FWDLanguage::getPHPStringValue('em_incident','Incidente');
    self::$csMsgDescription = FWDLanguage::getPHPStringValue('em_description','Descri��o');
    self::$csMsgIncidentRiskParametrization = FWDLanguage::getPHPStringValue('em_incident_risk_parametrization_message','<b>Prezado usu�rio do Real BCMS,</b><br>o risco abaixo foi associado a um ativo de sua responsabilidade.<br>Os par�metros de risco s�o diferentes do risco original.');
    self::$csMsgAsset = FWDLanguage::getPHPStringValue('em_asset','Ativo'); 
    self::$csMsgRisk = FWDLanguage::getPHPStringValue('em_risk','Risco');
    self::$csMsgOriginalParameters = FWDLanguage::getPHPStringValue('em_original_parameters','Par�metros Originais');
    self::$csMsgNewParameters = FWDLanguage::getPHPStringValue('em_new_parameters','Novos Par�metros');
    self::$csMsgNewPasswordRequest = FWDLanguage::getPHPStringValue('em_new_password_request_ignore','Se voc� n�o solicitou uma nova senha, por favor ignore este e-mail. Suas credenciais anteriores ainda s�o v�lidas e ao us�-las esta nova senha ser� descartada.');
    self::$csMsgAbandonedSystem = FWDLanguage::getPHPStringValue('em_saas_abandoned_system_msg','<b>O Real BCMS do cliente abaixo foi abandonado.</b>');
    self::$csMsgClient = FWDLanguage::getPHPStringValue('em_client','Cliente');
    self::$csMsgISMSFeedback = FWDLanguage::getPHPStringValue('em_isms_feedback_msg','Um usu�rio escreveu um feedback para o Real BCMS.');    
    self::$csMsgFeedbackType = FWDLanguage::getPHPStringValue('em_feedback_type','Tipo de Feedback');
    self::$csMsgFeedback = FWDLanguage::getPHPStringValue('em_feedback','Feedback');
    self::$csMsgISMSNewUser = FWDLanguage::getPHPStringValue('em_isms_new_user_msg','Voc� foi adicionado ao Real BCMS. Abaixo seguem os dados de acesso:');
    self::$csMsgLogin = FWDLanguage::getPHPStringValue('em_login','Login');
    self::$csMsgPassword = FWDLanguage::getPHPStringValue('em_password','Senha');
    self::$csMsgAccessISMS = FWDLanguage::getPHPStringValue('em_click_here_to_access_the_system','Acesse o Real BCMS');
    
    if (self::$cbHTML) $msFormat = 'html';
    else $msFormat = 'text';
    
    self::$caTemplates[$msFormat] = array();
    self::$caTemplates[$msFormat]['main'] = self::buildMainTemplate();
    self::$caTemplates[$msFormat]['taskWithoutContext'] = self::buildTaskTemplate(false);
    self::$caTemplates[$msFormat]['task'] = self::buildTaskTemplate(true);
    self::$caTemplates[$msFormat]['association'] = self::buildAssociationTemplate();
    self::$caTemplates[$msFormat]['alert'] = self::buildAlertTemplate(false);
    self::$caTemplates[$msFormat]['alertWithJustification'] = self::buildAlertTemplate(true);
    self::$caTemplates[$msFormat]['taskSubject'] = self::buildTaskSubjectTemplate();
    self::$caTemplates[$msFormat]['alertSubject'] = self::buildAlertSubjectTemplate();
    self::$caTemplates[$msFormat]['taskHeader'] = self::buildTaskHeaderTemplate();
    self::$caTemplates[$msFormat]['alertHeader'] = self::buildAlertHeaderTemplate();
    self::$caTemplates[$msFormat]['separator'] = self::buildSeparatorTemplate();
    self::$caTemplates[$msFormat]['newPassword'] = self::buildNewPasswordTemplate();
    self::$caTemplates[$msFormat]['taskAndAlertTransferHeader'] = self::buildTaskAndAlertTransferHeaderTemplate();
    self::$caTemplates[$msFormat]['taskAndAlertTransfer'] = self::buildTaskAndAlertTransferTemplate();
    self::$caTemplates[$msFormat]['controlImplementation'] = self::buildControlImplementationTemplate();
    self::$caTemplates[$msFormat]['disciplinaryProcess'] = self::buildDisciplinaryProcessTemplate();
    self::$caTemplates[$msFormat]['incidentRiskParametrization'] = self::buildIncidentRiskParametrizationTemplate();
    self::$caTemplates[$msFormat]['newPasswordRequest'] = self::buildNewPasswordRequestTemplate();
    self::$caTemplates[$msFormat]['abandonedSystem'] = self::buildAbandonedSystemTemplate();
    self::$caTemplates[$msFormat]['ismsFeedback'] = self::buildIsmsFeedbackTemplate();
    self::$caTemplates[$msFormat]['ismsNewUser'] = self::buildIsmsNewUserTemplate();
  }

  protected static function buildMainTemplate(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    if (self::$cbHTML) {
      $msGfxRef = FWDWebLib::getInstance()->getGfxRef();
      return '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
              <html>
              <head>
                <meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
              </head>
              <body>
              <table border="0" cellpadding="0" cellspacing="0" width="700">
                <tbody>
                  <tr>
                    <td height="110">
                    <table cellpadding="0" cellspacing="0" height="100%" width="100%">
                      <tbody>
                        <tr>
                          <td width="50"></td>
                          <td width="360"></td>
                          <td width="240" align="right">
                            <font size="2" color="#000000" face="Verdana, Tahoma, Arial, sans-serif">
                              <b>
                                '.self::$csMsgSentDate.'<br>
                                %date%<br>
                                '.self::$csMsgSentTime.' %time% hs
                              </b>
                            </font>
                          </td>
                          <td width="50"></td>
                        </tr>
                      </tbody>
                    </table>
                    </td>
                  </tr>
                  <tr>
                    <td>%body%</td>
                  </tr>
                  <tr>
                    <td height="20"></td>
                  </tr>
                  <tr>
                    <td height="56" width="700">
                      <table border="0" cellpadding="0" cellspacing="0" height="56" width="700">
                        <tbody>
                         <tr>
                          <td width="50"></td>
                          <td width="527">
                            <font size="1" color="#000000" face="Verdana, Tahoma, Arial, sans-serif">
                              %footer_message%
                            </font>
                          </td>
                          <td width="73" align="right">
                          </td>
                          <td width="50"></td>
                        </tr>
                        </tbody>
                      </table>
                    </td>
                  </tr>
                </tbody>
              </table>
              <br>
              </body>
              </html>';
    }
    else {
      return strip_tags(self::$csMsgSentDate).' %date% '. strip_tags(self::$csMsgSentTime).' %time% hs'.
             PHP_EOL.PHP_EOL.
             '%body%'.
             PHP_EOL.PHP_EOL.
             '%footer_message%';
    }
  }

  protected static function buildTaskTemplate($pbHasContext){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    if (self::$cbHTML) {
      return '<table border="0" cellpadding="0" cellspacing="4" width="700">
                <tbody>
                  <tr>
                    <td width="46"></td>
                    <td width="100">
                      <font size="3" color="#1F5378" face="Verdana, Tahoma, Arial, sans-serif">
                        <b>'.self::$csMsgActivity.':</b>
                      </font>
                    </td>
                    <td width="500">
                      <a href="%link%">
                        <font size="3" color="#1F5378" face="Verdana, Tahoma, Arial, sans-serif">
                          <b>%activity%</b>
                        </font>
                      </a>
                    </td>
                    <td width="46"></td>
                  </tr>
                  <tr>
                    <td width="46"></td>
                    <td width="100">
                      <font size="2" color="#666666" face="Verdana, Tahoma, Arial, sans-serif">
                        <b>'.self::$csMsgSender.':</b>
                      </font>
                    </td>
                    <td width="500">
                      <font size="2" color="#666666" face="Verdana, Tahoma, Arial, sans-serif">
                        %sender%
                      </font>
                    </td>
                    <td width="46"></td>
                  </tr>
                  <tr>
                    <td width="46"></td>
                    <td width="100">
                      <font size="2" color="#666666" face="Verdana, Tahoma, Arial, sans-serif">
                        <b>'.self::$csMsgTaskCreatedAt.':</b>
                      </font>
                    </td>
                    <td width="500">
                      <font size="2" color="#666666" face="Verdana, Tahoma, Arial, sans-serif">
                        %date%
                      </font>
                    </td>
                    <td width="46"></td>
                  </tr>'
                  .($pbHasContext?
                  '<tr>
                    <td width="46"></td>
                    <td width="100">
                      <font size="2" color="#666666" face="Verdana, Tahoma, Arial, sans-serif">
                        <b>%contextType%:</b>
                      </font>
                    </td>
                    <td width="500">
                      <font size="2" color="#666666" face="Verdana, Tahoma, Arial, sans-serif">
                        %name%
                      </font>
                    </td>
                    <td width="46"></td>
                  </tr>
                  <tr>
                    <td width="46"></td>
                    <td width="100">
                      <font size="2" color="#666666" face="Verdana, Tahoma, Arial, sans-serif">
                        <b>'.self::$csMsgContextDescription.':</b>
                      </font>
                    </td>
                    <td width="500">
                      <font size="2" color="#666666" face="Verdana, Tahoma, Arial, sans-serif">
                        %description%
                      </font>
                    </td>
                    <td width="46"></td>
                  </tr>'
                  :'').
                '</tbody>
              </table>';
    }
    else {
      return strip_tags(self::$csMsgActivity).': %activity%'.
             PHP_EOL.
             strip_tags(self::$csMsgSender).': %sender%'.
             PHP_EOL.
             strip_tags(self::$csMsgLink).': %link%'.
             PHP_EOL.
             strip_tags(self::$csMsgTaskCreatedAt).': %date%'.
             PHP_EOL.
             ($pbHasContext?
                  '%contextType%: %name%'.
                  PHP_EOL.
                  strip_tags(self::$csMsgContextDescription).': %description%'
                  :''
             );
    }
  }

  protected static function buildAssociationTemplate(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    if (self::$cbHTML) {
      return '<table border="0" cellpadding="0" cellspacing="4" width="700">
                <tbody>
                  <tr>
                    <td width="46"></td>
                    <td width="100">
                      <font size="3" color="#1F5378" face="Verdana, Tahoma, Arial, sans-serif">
                        <b>'.self::$csMsgActivity.':</b>
                      </font>
                    </td>
                    <td width="500">
                      <a href="%link%">
                        <font size="3" color="#1F5378" face="Verdana, Tahoma, Arial, sans-serif">
                          <b>%activity%</b>
                        </font>
                      </a>
                    </td>
                    <td width="46"></td>
                  </tr>
                  <tr>
                    <td width="46"></td>
                    <td width="100">
                      <font size="2" color="#666666" face="Verdana, Tahoma, Arial, sans-serif">
                        <b>'.self::$csMsgSender.':</b>
                      </font>
                    </td>
                    <td width="500">
                      <font size="2" color="#666666" face="Verdana, Tahoma, Arial, sans-serif">
                        %sender%
                      </font>
                    </td>
                    <td width="46"></td>
                  </tr>
                  <tr>
                    <td width="46"></td>
                    <td width="100">
                      <font size="2" color="#666666" face="Verdana, Tahoma, Arial, sans-serif">
                        <b>'.self::$csMsgTaskCreatedAt.':</b>
                      </font>
                    </td>
                    <td width="500">
                      <font size="2" color="#666666" face="Verdana, Tahoma, Arial, sans-serif">
                        %date%
                      </font>
                    </td>
                    <td width="46"></td>
                  </tr>
                  <tr>
                    <td width="46"></td>
                    <td width="100">
                      <font size="2" color="#666666" face="Verdana, Tahoma, Arial, sans-serif">
                        <b>%contextType1%:</b>
                      </font>
                    </td>
                    <td width="500">
                      <font size="2" color="#666666" face="Verdana, Tahoma, Arial, sans-serif">
                        %name1%
                      </font>
                    </td>
                    <td width="46"></td>
                  </tr>
                  <tr>
                    <td width="46"></td>
                    <td width="100">
                      <font size="2" color="#666666" face="Verdana, Tahoma, Arial, sans-serif">
                        <b>'.self::$csMsgContextDescription.':</b>
                      </font>
                    </td>
                    <td width="500">
                      <font size="2" color="#666666" face="Verdana, Tahoma, Arial, sans-serif">
                        %description1%
                      </font>
                    </td>
                    <td width="46"></td>
                  </tr>
                  <tr>
                    <td width="46"></td>
                    <td width="100">
                      <font size="2" color="#666666" face="Verdana, Tahoma, Arial, sans-serif">
                        <b>%contextType2%:</b>
                      </font>
                    </td>
                    <td width="500">
                      <font size="2" color="#666666" face="Verdana, Tahoma, Arial, sans-serif">
                        %name2%
                      </font>
                    </td>
                    <td width="46"></td>
                  </tr>
                  <tr>
                    <td width="46"></td>
                    <td width="100">
                      <font size="2" color="#666666" face="Verdana, Tahoma, Arial, sans-serif">
                        <b>'.self::$csMsgContextDescription.':</b>
                      </font>
                    </td>
                    <td width="500">
                      <font size="2" color="#666666" face="Verdana, Tahoma, Arial, sans-serif">
                        %description2%
                      </font>
                    </td>
                    <td width="46"></td>
                  </tr>
                </tbody>
              </table>';
    }
    else {
      return strip_tags(self::$csMsgActivity).': %activity%' . PHP_EOL .
             strip_tags(self::$csMsgSender).': %sender%' . PHP_EOL .             
             strip_tags(self::$csMsgLink).': %link%'. PHP_EOL .
             strip_tags(self::$csMsgTaskCreatedAt).': %date%' . PHP_EOL .
             '%contextType1%: %name1%' . PHP_EOL .
             strip_tags(self::$csMsgContextDescription).': %description1%' . PHP_EOL .
             '%contextType2%: %name2%' . PHP_EOL .
             strip_tags(self::$csMsgContextDescription).': %description2%' . PHP_EOL;
    }
  }

  protected static function buildAlertTemplate($pbJustification){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    if (self::$cbHTML) {
      return '<table border="0" cellpadding="0" cellspacing="4" width="700">
                <tbody>
                  <tr>
                    <td width="46"></td>
                    <td width="600" colspan="2">
                      <font size="3" color="#1F5378" face="Verdana, Tahoma, Arial, sans-serif">
                        <b>%description%</b>
                      </font>
                    </td>
                    <td width="46"></td>
                  </tr>
                  <tr>
                    <td width="46"></td>
                    <td width="100">
                      <font size="2" color="#666666" face="Verdana, Tahoma, Arial, sans-serif">
                        <b>'.self::$csMsgSender.':</b>
                      </font>
                    </td>
                    <td width="500">
                      <font size="2" color="#666666" face="Verdana, Tahoma, Arial, sans-serif">
                        %sender%
                      </font>
                    </td>
                    <td width="46"></td>
                  </tr>
                  <tr>
                    <td width="46"></td>
                    <td width="100">
                      <font size="2" color="#666666" face="Verdana, Tahoma, Arial, sans-serif">
                        <b>'.self::$csMsgAlertCreatedAt.':</b>
                      </font>
                    </td>
                    <td width="500">
                      <font size="2" color="#666666" face="Verdana, Tahoma, Arial, sans-serif">
                        %date%
                      </font>
                    </td>
                    <td width="46"></td>
                  </tr>'.
                  ($pbJustification?
                  '<tr>
                    <td width="46"></td>
                    <td width="600" colspan="2">
                      <font size="2" color="#666666" face="Verdana, Tahoma, Arial, sans-serif">
                        <b>'.self::$csMsgJustification.':</b>
                      </font>
                    </td>
                    <td width="46"></td>
                  </tr>
                  <tr>
                    <td width="46"></td>
                    <td width="600" colspan="2">
                      <font size="2" color="#666666" face="Verdana, Tahoma, Arial, sans-serif">
                        %justification%
                      </font>
                    </td>
                    <td width="46"></td>
                  </tr>':'')
                .'<tr>
                    <td colspan="4" width="700" height="10">
                    </td>
                  </tr>
                </tbody>
              </table>';
    }
    else {
      return '%description%'. PHP_EOL .
             strip_tags(self::$csMsgSender).': %sender%' . PHP_EOL .
             strip_tags(self::$csMsgAlertCreatedAt).': %date%' . PHP_EOL .
             ($pbJustification?strip_tags(self::$csMsgJustification).': %justification%' . PHP_EOL :'');
    }
  }

  protected static function buildTaskSubjectTemplate(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    if (self::$cbHTML) {
      return '<table border="0" cellpadding="0" cellspacing="4" width="700">
                <tbody>
                  <tr>
                    <td width="46"></td>
                    <td width="600">
                      <a href="%link%">
                        <font size="3" color="#1F5378" face="Verdana, Tahoma, Arial, sans-serif">
                          <b>%activity%</b>
                        </font>
                      </a>
                    </td>
                    <td width="46"></td>
                  </tr>
                </tbody>
              </table>';
    }
    else {
      return '%activity% (%link%)';
    }
  }

  protected static function buildAlertSubjectTemplate(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    if (self::$cbHTML) {
      return '<table border="0" cellpadding="0" cellspacing="4" width="700">
                <tbody>
                  <tr>
                    <td width="46"></td>
                    <td width="600" colspan="2">
                      <font size="3" color="#1F5378" face="Verdana, Tahoma, Arial, sans-serif">
                        <b>%description%</b>
                      </font>
                    </td>
                    <td width="46"></td>
                  </tr>
                </tbody>
              </table>';
    }
    else {
      return '%description%';
    }
  }

  protected static function buildTaskHeaderTemplate(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    if (self::$cbHTML) {
      return '<table border="0" cellpadding="0" cellspacing="4" width="700">
                <tbody>
                  <tr>
                    <td width="46"></td>
                    <td width="600" colspan="2" rowspan="1">
                      <font size="2" color="#666666" face="Verdana, Tahoma, Arial, sans-serif">
                        '.self::$csMsgTaskHeader.'
                        <br>
                      </font>
                    </td>
                    <td width="46"></td>
                  </tr>
                </tbody>
              </table>';
    }
    else {
      return strip_tags(self::$csMsgTaskHeader).PHP_EOL.PHP_EOL;
    }              
  }

  protected static function buildAlertHeaderTemplate(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    if (self::$cbHTML) {
      return '<table border="0" cellpadding="0" cellspacing="4" width="700">
                <tbody>
                  <tr>
                    <td width="46"></td>
                    <td width="600" colspan="2" rowspan="1">
                      <font size="2" color="#666666" face="Verdana, Tahoma, Arial, sans-serif">
                        <br>
                        '.self::$csMsgAlertHeader.'
                        <br>
                      </font>
                    </td>
                    <td width="46"></td>
                  </tr>
                </tbody>
              </table>';
    }
    else {
      return PHP_EOL.PHP_EOL.strip_tags(self::$csMsgAlertHeader).PHP_EOL.PHP_EOL;
    }
  }

  protected static function buildSeparatorTemplate(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    if (self::$cbHTML) {
      return '<hr width="600" align="center">';
    }
    else {
      return PHP_EOL.PHP_EOL.'=================================================='.PHP_EOL.PHP_EOL; 
    }
  }

  protected static function buildNewPasswordTemplate() {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    if (self::$cbHTML) {
    	return '<table border="0" cellpadding="0" cellspacing="4" width="700">
                <tbody>
                  <tr>
                    <td width="46"></td>
                    <td width="600" colspan="2">
                      <font size="2" color="#666666" face="Verdana, Tahoma, Arial, sans-serif">
                        '.self::$csMsgNewPassword.': <b>%new_password%</b>
                      </font>
                    </td>
                    <td width="46"></td>
                  </tr>
                </tbody>
              </table>';
    }
    else {
      return strip_tags(self::$csMsgNewPassword).': %new_password%' . PHP_EOL;
    }
  }
  
  protected static function buildNewPasswordRequestTemplate() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    if (self::$cbHTML) {
      return '<table border="0" cellpadding="0" cellspacing="4" width="700">
                <tbody>
                  <tr>
                    <td colspan="3" height="20"></td>
                  </tr>
                  <tr>
                    <td width="46"></td>
                    <td width="600" colspan="2">
                      <font size="2" color="#666666" face="Verdana, Tahoma, Arial, sans-serif">
                        '.self::$csMsgNewPasswordRequest.'
                      </font>
                    </td>
                    <td width="46"></td>
                  </tr>
                </tbody>
              </table>';
    }
    else {
      return PHP_EOL.strip_tags(self::$csMsgNewPasswordRequest).PHP_EOL;
    }
  }

  protected static function buildTaskAndAlertTransferHeaderTemplate(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    if (self::$cbHTML) {
      return '<table border="0" cellpadding="0" cellspacing="4" width="700">
                <tbody>
                  <tr>
                    <td width="46"></td>
                    <td width="600" colspan="2" rowspan="1">
                      <font size="2" color="#666666" face="Verdana, Tahoma, Arial, sans-serif">
                        '.self::$csMsgTaskAndAlertTransferHeader.'
                        <br>
                      </font>
                    </td>
                    <td width="46"></td>
                  </tr>
                </tbody>
              </table>';
    }
    else {
      return strip_tags(self::$csMsgTaskAndAlertTransferHeader) . PHP_EOL; 
    }
  }

  protected static function buildTaskAndAlertTransferTemplate(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    if (self::$cbHTML) {
      return '<table border="0" cellpadding="0" cellspacing="4" width="700">
                <tbody>
                  <tr>
                    <td width="46"></td>
                    <td width="100">
                      <font size="3" color="#1F5378" face="Verdana, Tahoma, Arial, sans-serif">
                        <b>'.self::$csMsgUser.':</b>
                      </font>
                    </td>
                    <td width="500">
                      <font size="3" color="#1F5378" face="Verdana, Tahoma, Arial, sans-serif">
                        <b>%user%</b>
                      </font>
                    </td>
                    <td width="46"></td>
                  </tr>
                  <tr>
                    <td width="46"></td>
                    <td width="600" colspan="2">
                      <font size="2" color="#666666" face="Verdana, Tahoma, Arial, sans-serif">
                        '.self::$csMsgTaskAndAlertTransfer.'
                      </font>
                    </td>
                    <td width="46"></td>
                  </tr>
                </tbody>
              </table>';
    }
    else {
      return strip_tags(self::$csMsgUser) . ': %user%'. PHP_EOL .
             strip_tags(self::$csMsgTaskAndAlertTransfer) . PHP_EOL;
    }
  }

  protected static function buildControlImplementationTemplate(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    if (self::$cbHTML) {
      return '<table border="0" cellpadding="0" cellspacing="4" width="700">
                <tbody>
                  <tr>
                    <td width="46"></td>
                    <td width="600" colspan="2">
                      <font size="2" color="#666666" face="Verdana, Tahoma, Arial, sans-serif">
                        '.self::$csMsgControlImplementation.'
                      </font>
                    </td>
                    <td width="46"></td>
                  </tr>                
                  <tr>
                    <td width="46"></td>
                    <td width="100">
                      <font size="3" color="#1F5378" face="Verdana, Tahoma, Arial, sans-serif">
                        <b>'.self::$csMsgControl.':</b>
                      </font>
                    </td>
                    <td width="500">
                      <font size="3" color="#1F5378" face="Verdana, Tahoma, Arial, sans-serif">
                        <b>%control%</b>
                      </font>
                    </td>
                    <td width="46"></td>
                  </tr>
                  <tr>
                    <td width="46"></td>
                    <td width="100">
                      <font size="3" color="#1F5378" face="Verdana, Tahoma, Arial, sans-serif">
                        <b>'.self::$csMsgLimit.':</b>
                      </font>
                    </td>
                    <td width="500">
                      <font size="3" color="#1F5378" face="Verdana, Tahoma, Arial, sans-serif">
                        <b>%date%</b>
                      </font>
                    </td>
                    <td width="46"></td>
                  </tr>
                </tbody>
              </table>';
    }
    else {
      return strip_tags(self::$csMsgControlImplementation) . PHP_EOL .
             strip_tags(self::$csMsgControl).': %control%' . PHP_EOL . 
             strip_tags(self::$csMsgLimit).': %date%' . PHP_EOL;
    }
  }

  protected static function buildDisciplinaryProcessTemplate(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    if (self::$cbHTML) {
      return '<table border="0" cellpadding="0" cellspacing="4" width="700">
                <tbody>
                  <tr>
                    <td width="46"></td>
                    <td width="600" colspan="2">
                      <font size="2" color="#666666" face="Verdana, Tahoma, Arial, sans-serif">
                        '.self::$csMsgDisciplinaryProcess.'
                      </font>
                    </td>
                    <td width="46"></td>
                  </tr>
                  <tr>
                    <td colspan="4">&nbsp;</td>
                  </tr>              
                  <tr>
                    <td width="46"></td>
                    <td width="100">
                      <font size="3" color="#1F5378" face="Verdana, Tahoma, Arial, sans-serif">
                        <b>'.self::$csMsgIncident.':</b>
                      </font>
                    </td>
                    <td width="500">
                      <font size="3" color="#1F5378" face="Verdana, Tahoma, Arial, sans-serif">
                        <b>%incident%</b>
                      </font>
                    </td>
                    <td width="46"></td>
                  </tr>                
                  <tr>
                    <td width="46"></td>
                    <td width="100">
                      <font size="3" color="#1F5378" face="Verdana, Tahoma, Arial, sans-serif">
                        <b>'.self::$csMsgUsers.':</b>
                      </font>
                    </td>
                    <td width="500">
                      <font size="3" color="#1F5378" face="Verdana, Tahoma, Arial, sans-serif">
                        <b>%users%</b>
                      </font>
                    </td>
                    <td width="46"></td>
                  </tr>
                  
                </tbody>
              </table>';
    }
    else {
      return strip_tags(self::$csMsgDisciplinaryProcess) . PHP_EOL . PHP_EOL .
             strip_tags(self::$csMsgIncident).': %incident%' . PHP_EOL .
             strip_tags(self::$csMsgUsers).': %users%' . PHP_EOL;
    }
  }

  public static function buildIncidentRiskParametrizationTemplate() {
    if (self::$cbHTML) {
      return '<table style="width: 700px;" border="0" cellpadding="0" cellspacing="4">
                <tbody>
                  <tr>
                    <td width="46"></td>
                    <td colspan="2" width="600">
                    <font color="#666666" face="Verdana, Tahoma, Arial, sans-serif" size="2">'.self::$csMsgIncidentRiskParametrization.'
                    </font>
                    </td>
                    <td width="46"></td>
                  </tr>
                  <tr>
                    <td colspan="4">&nbsp;</td>
                  </tr>
                  <tr>
                    <td width="46"></td>
                    <td width="100">
                    <font color="#1f5378" face="Verdana, Tahoma, Arial, sans-serif" size="3"><b>'.self::$csMsgAsset.':</b>
                    </font></td>
                    <td width="500">
                    <font color="#1f5378" face="Verdana, Tahoma, Arial, sans-serif" size="3"><b>%asset%</b>
                    </font></td>
                    <td width="46"></td>
                  </tr>
                  <tr>
                    <td width="46"></td>
                    <td width="100">
                    <font color="#1f5378" face="Verdana, Tahoma, Arial, sans-serif" size="3"><b>'.self::$csMsgRisk.':</b>
                    </font></td>
                    <td width="500">
                    <font color="#1f5378" face="Verdana, Tahoma, Arial, sans-serif" size="3"><b>%risk%</b>
                    </font></td>
                    <td width="46"></td>
                  </tr>
                  <tr>
                    <td colspan="4">&nbsp;</td>
                  </tr>
                  <tr>
                    <td width="46"></td>
                    <td colspan="2" rowspan="1" style="width: 100px;">
                    <table width="600" border="0" cellpadding="0" cellspacing="4">
                      <tbody>
                        <tr>
                          <td width="300"><font color="#1f5378" face="Verdana, Tahoma, Arial, sans-serif" size="3">'.self::$csMsgOriginalParameters.'</font></td>
                          <td width="300"><font color="#1f5378" face="Verdana, Tahoma, Arial, sans-serif" size="3">'.self::$csMsgNewParameters.'</font></td>
                        </tr>
                        <tr>
                          <td width="300"><font color="#666666" face="Verdana, Tahoma, Arial, sans-serif" size="2">%original_parameters%</font></td>
                          <td width="300"><font color="#666666" face="Verdana, Tahoma, Arial, sans-serif" size="2">%new_parameters%</font></td>
                        </tr>
                      </tbody>
                    </table>
                    </td>
                    <td width="46"></td>
                  </tr>
                </tbody>
              </table>';
    }
    else {
      return strip_tags(self::$csMsgIncidentRiskParametrization) . PHP_EOL . PHP_EOL .
             strip_tags(self::$csMsgAsset).': %asset%' . PHP_EOL .
             strip_tags(self::$csMsgRisk).': %risk%' . PHP_EOL . PHP_EOL .
             strip_tags(self::$csMsgOriginalParameters) . ':' . PHP_EOL . '%original_parameters%' . PHP_EOL .
             strip_tags(self::$csMsgNewParameters) . ':' . PHP_EOL . '%new_parameters%';
    }
  }
  
  protected static function buildAbandonedSystemTemplate() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    if (self::$cbHTML) {
      return '<table border="0" cellpadding="0" cellspacing="4" width="700">
                <tbody>
                  <tr>
                    <td width="46"></td>
                    <td width="600" colspan="2">
                      <font size="2" color="#666666" face="Verdana, Tahoma, Arial, sans-serif">
                        '.self::$csMsgAbandonedSystem.'
                      </font>
                    </td>
                    <td width="46"></td>
                  </tr>
                  <tr>
                    <td height="20" colspan="4"></td>
                  </tr>
                  <tr>
                    <td width="46"></td>
                    <td width="100">
                      <font size="2" color="#1F5378" face="Verdana, Tahoma, Arial, sans-serif">
                        <b>'.self::$csMsgClient.':</b>
                      </font>
                    </td>
                    <td width="500">
                      <font size="2" color="#1F5378" face="Verdana, Tahoma, Arial, sans-serif">
                        <b>%client_name%</b>
                      </font>
                    </td>
                    <td width="46"></td>
                  </tr>
                </tbody>
              </table>';
    }
    else {
      return strip_tags(self::$csMsgAbandonedSystem) . PHP_EOL .
             strip_tags(self::$csMsgClient) . ': %client_name%'. PHP_EOL;
    }
  }
  
  protected static function buildIsmsFeedbackTemplate() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    if (self::$cbHTML) {
      return '<table border="0" cellpadding="0" cellspacing="4" width="700">
                <tbody>
                  <tr>
                    <td width="46"></td>
                    <td width="600" colspan="2">
                      <font size="2" color="#666666" face="Verdana, Tahoma, Arial, sans-serif">
                        '.self::$csMsgISMSFeedback.'
                      </font>
                    </td>
                    <td width="46"></td>
                  </tr>
                  <tr>
                    <td height="20" colspan="4"></td>
                  </tr>
                  <tr>
                    <td width="46"></td>
                    <td width="200">
                      <font size="2" color="#1F5378" face="Verdana, Tahoma, Arial, sans-serif">
                        <b>'.self::$csMsgUser.':</b>
                      </font>
                    </td>
                    <td width="400">
                      <font size="2" color="#1F5378" face="Verdana, Tahoma, Arial, sans-serif">
                        <b>%user_name%</b>
                      </font>
                    </td>
                    <td width="46"></td>
                  </tr>
                  <tr>
                    <td width="46"></td>
                    <td width="200">
                      <font size="2" color="#1F5378" face="Verdana, Tahoma, Arial, sans-serif">
                        <b>'.self::$csMsgFeedbackType.':</b>
                      </font>
                    </td>
                    <td width="400">
                      <font size="2" color="#1F5378" face="Verdana, Tahoma, Arial, sans-serif">
                        <b>%feedback_type%</b>
                      </font>
                    </td>
                    <td width="46"></td>
                  </tr>
                  <tr>
                    <td width="46"></td>
                    <td width="200">
                      <font size="2" color="#1F5378" face="Verdana, Tahoma, Arial, sans-serif">
                        <b>'.self::$csMsgFeedback.':</b>
                      </font>
                    </td>
                    <td width="400">
                      <font size="2" color="#1F5378" face="Verdana, Tahoma, Arial, sans-serif">
                        <b>%feedback%</b>
                      </font>
                    </td>
                    <td width="46"></td>
                  </tr>
                </tbody>
              </table>';
    }
    else {
      return strip_tags(self::$csMsgISMSFeedback) . PHP_EOL .
             strip_tags(self::$csMsgUser) . ': %user_name%'. PHP_EOL .
             strip_tags(self::$csMsgFeedbackType) . ': %feedback_type%'. PHP_EOL .
             strip_tags(self::$csMsgFeedback) . ': %feedback%'. PHP_EOL;
    }
  }
  
  protected static function buildIsmsNewUserTemplate() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    if (self::$cbHTML) {
      return '<table border="0" cellpadding="0" cellspacing="4" width="700">
                <tbody>
                  <tr>
                    <td width="46"></td>
                    <td width="600" colspan="2">
                      <font size="2" color="#1F5378" face="Verdana, Tahoma, Arial, sans-serif">
                        '.self::$csMsgISMSNewUser.'
                      </font>
                    </td>
                    <td width="46"></td>
                  </tr>
                  <tr>
                    <td height="20" colspan="4"></td>
                  </tr>
                  <tr>
                    <td width="46"></td>
                    <td width="200">
                      <font size="2" color="#666666" face="Verdana, Tahoma, Arial, sans-serif">
                        <b>'.self::$csMsgUser.':</b>
                      </font>
                    </td>
                    <td width="400">
                      <font size="2" color="#666666" face="Verdana, Tahoma, Arial, sans-serif">
                        %user_name%
                      </font>
                    </td>
                    <td width="46"></td>
                  </tr>
                  <tr>
                    <td width="46"></td>
                    <td width="200">
                      <font size="2" color="#666666" face="Verdana, Tahoma, Arial, sans-serif">
                        <b>'.self::$csMsgLogin.':</b>
                      </font>
                    </td>
                    <td width="400">
                      <font size="2" color="#666666" face="Verdana, Tahoma, Arial, sans-serif">
                        %user_login%
                      </font>
                    </td>
                    <td width="46"></td>
                  </tr>
                  <tr>
                    <td width="46"></td>
                    <td width="200">
                      <font size="2" color="#666666" face="Verdana, Tahoma, Arial, sans-serif">
                        <b>'.self::$csMsgPassword.':</b>
                      </font>
                    </td>
                    <td width="400">
                      <font size="2" color="#666666" face="Verdana, Tahoma, Arial, sans-serif">
                        %user_password%
                      </font>
                    </td>
                    <td width="46"></td>
                  </tr>
                  <tr>
                    <td height="20" colspan="4"></td>
                  </tr>
                  <tr>
                    <td width="46"></td>
                    <td width="200">
                      <a href="%link%">
                        <font size="2" color="#666666" face="Verdana, Tahoma, Arial, sans-serif">
                          '.self::$csMsgAccessISMS.'
                        </font>
                      </a>
                    </td>
                    <td width="400">                                            
                    </td>
                    <td width="46"></td>
                  </tr>
                </tbody>
              </table>';
    }
    else {
      return strip_tags(self::$csMsgISMSNewUser) . PHP_EOL . PHP_EOL .
             strip_tags(self::$csMsgUser) . ': %user_name%'. PHP_EOL .
             strip_tags(self::$csMsgLogin) . ': %user_login%'. PHP_EOL .
             strip_tags(self::$csMsgPassword) . ': %user_password%'. PHP_EOL .
             strip_tags(self::$csMsgLink) . ': %link%'. PHP_EOL;
    }
  }
  
  public static function getTemplate($psName, $pbHTML = true){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    self::$cbHTML = $pbHTML;
            
    $msFormat = ($pbHTML ? "html" : "text");
    if (!isset(self::$caTemplates[$msFormat])) self::buildTemplates();
    if(isset(self::$caTemplates[$msFormat][$psName])){
      return self::$caTemplates[$msFormat][$psName];
    }else{
      trigger_error("Unknown email template ($psName).",E_USER_ERROR);
    }
  }
}
?>