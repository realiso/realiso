<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe ISMSReportNonConformityAccompaniment.
 *
 * <p>Classe que implementa o relat�rio de acompanhamento de n�o conformidade.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportNonConformityAccompaniment extends ISMSReport {
  
 /**
  * Inicializa o relat�rio.
  *
  * <p>M�todo para inicializar o relat�rio.
  * Necess�rio para funcionar com o XML compilado.</p>
  * @access public
  */
  public function init(){
    parent::init();
    $this->coDataSet->addFWDDBField(new FWDDBField('',	'nc_id',										    DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('',	'nc_name',									    DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('',	'nc_responsible_name',					DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('',	'nc_state',					            DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('',	'action_plan_id',					      DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('',	'action_plan_name',		          DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('',	'action_plan_responsible_name',	DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('',  'date_deadline',                DB_DATETIME));
    $this->coDataSet->addFWDDBField(new FWDDBField('',  'date_conclusion',              DB_DATETIME));
    $this->coDataSet->addFWDDBField(new FWDDBField('',  'action_plan_state',            DB_NUMBER)); 
  }
  
  /**
   * Executa a query do relat�rio.
   *
   * <p>M�todo para executar a query do relat�rio.</p>
   * @access public
   */
  public function makeQuery(){
  	$maWhere = array();
    
    if($this->coFilter->getStatus()){      
      $maWhere[] = "c.nState = " . $this->coFilter->getStatus();
    }
    
    if($this->coFilter->getClassificationType()){      
      $maWhere[] = "nc.nClassification = " . $this->coFilter->getClassificationType();
    }
    
    if($this->coFilter->getCapability()){
      $maWhere[] = "nc.nCapability = " . $this->coFilter->getCapability();
    }
    
    if($this->coFilter->getEstimatedConclusionDateStart()){
      $maWhere[] = "ap.dDateDeadline >= ".ISMSLib::getTimestampFormat($this->coFilter->getEstimatedConclusionDateStart());
    }
    
    if($this->coFilter->getEstimatedConclusionDateFinish()){
      $maWhere[] = "ap.dDateDeadline <= ".ISMSLib::getTimestampFormat($this->coFilter->getEstimatedConclusionDateFinish());
    }
    
    if($this->coFilter->getConclusionDateStart()){
      $maWhere[] = "ap.dDateConclusion >= ".ISMSLib::getTimestampFormat($this->coFilter->getConclusionDateStart());
    }
    
    if($this->coFilter->getConclusionDateFinish()){
      $maWhere[] = "ap.dDateConclusion <= ".ISMSLib::getTimestampFormat($this->coFilter->getConclusionDateFinish());
    }
    
    if(count($maWhere)){
      $msWhere = ' WHERE ' . implode(' AND ', $maWhere);
    }else{
      $msWhere = '';
    }
    
    $this->csQuery = "SELECT
                        nc.fkContext as nc_id,
                        nc.sName as nc_name,  
                        u.sName as nc_responsible_name,
                        c.nState as nc_state,
                        ap.fkContext as action_plan_id,
                        ap.sName as action_plan_name,
                        u2.sName as action_plan_responsible_name,
                        ap.dDateDeadline as date_deadline,
                        ap.dDateConclusion as date_conclusion,
                        c2.nState as action_plan_state
                      FROM
                        view_ci_nc_active nc
                        JOIN isms_context c ON (nc.fkContext = c.pkContext)
                        LEFT JOIN ci_nc_action_plan nc_ap ON (nc.fkContext = nc_ap.fkNC)
                        LEFT JOIN view_ci_action_plan_active ap ON (nc_ap.fkActionPlan = ap.fkContext)
                        LEFT JOIN isms_context c2 ON (ap.fkContext = c2.pkContext)
                        LEFT JOIN view_isms_user_active u ON (nc.fkResponsible = u.fkContext)
                        LEFT JOIN view_isms_user_active u2 ON (ap.fkResponsible = u2.fkContext)
											  $msWhere
                      ORDER BY nc.fkContext, ap.fkContext";

    return parent::executeQuery();
  }
  
 /**
  * Desenha o cabe�alho do relat�rio.
  *
  * <p>M�todo para desenhar o cabe�alho do relat�rio.</p>
  * @access public
  */
  public function drawHeader(){
    foreach($this->caHeaders as $moHeader){
      if($moHeader->getAttrName()=='header_filter'){
        $moHeaderFilter = $moHeader;
      }elseif($moHeader->getAttrName()=='header_filter_item'){
        $moHeaderFilterItem = $moHeader;
      }elseif($moHeader->getAttrName()=='report_header_blank'){
        $moHeaderBlank = $moHeader;
      }else{
        $this->coWriter->drawLine($moHeader,array());
      }
    }
    if($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());
    
    $moFilterText = FWDWebLib::getObject('header_filter_text');
    $moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
    $maFilters = $this->coFilter->getSummary();
    foreach($maFilters as $maFilter){
      $moFilterText->setValue($maFilter['name']);
      $this->coWriter->drawLine($moHeaderFilter,array());
      foreach($maFilter['items'] as $msFilterItem){
        $moFilterItemText->setValue($msFilterItem);
        $this->coWriter->drawLine($moHeaderFilterItem,array());
      }
    }
    $this->coWriter->drawLine($moHeaderBlank,array());
  }
}
?>