<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportActionPlanByUser.
 *
 * <p>Classe que implementa o relat�rio de plano de a��o por respons�vel.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportActionPlanByUser extends ISMSReport {
  
  public function __construct(){
    parent::__construct();
  }
  
 /**
  * Inicializa o relat�rio.
  *
  * <p>M�todo para inicializar o relat�rio.
  * Necess�rio para funcionar com o XML compilado.</p>
  * @access public
  */
  public function init(){
    parent::init();
    $this->coDataSet->addFWDDBField(new FWDDBField('ap.fkResponsible','responsible_id'  ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('u.sName'         ,'responsible_name',DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('ap.fkContext'    ,'ap_id'           ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('ap.sName'        ,'ap_name'         ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('ctx.nState'      ,'ap_state'        ,DB_NUMBER));
  }
  
 /**
  * Executa a query do relat�rio.
  *
  * <p>M�todo para executar a query do relat�rio.</p>
  * @access public
  */
  public function makeQuery(){
    $this->csQuery = "SELECT
                      	ap.fkResponsible as responsible_id,
                      	u.sName as responsible_name,
                      	ap.fkContext as ap_id,
                      	ap.sName as ap_name,
                      	ctx.nState as ap_state
                      FROM
                      	view_ci_action_plan_active ap
                      	JOIN view_isms_user_active u ON (ap.fkResponsible = u.fkContext)
                      	JOIN view_isms_context_active ctx ON (ap.fkContext = ctx.pkContext)
                      ORDER BY ap.fkResponsible,
                      ap.fkContext";
    return parent::executeQuery();
  }

}

?>