<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportActionPlanByProcess.
 *
 * <p>Classe que implementa o relat�rio de plano de a��o por processo.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportActionPlanByProcess extends ISMSReport {
  
  public function __construct(){
    parent::__construct();
  }
  
 /**
  * Inicializa o relat�rio.
  *
  * <p>M�todo para inicializar o relat�rio.
  * Necess�rio para funcionar com o XML compilado.</p>
  * @access public
  */
  public function init(){
    parent::init();
    $this->coDataSet->addFWDDBField(new FWDDBField('p.fkContext' ,'process_id'      ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('p.sName'     ,'process_name'    ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('p.nValue'    ,'process_value'   ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('u.sName'     ,'responsible_name',DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('ap.fkContext','ap_id'           ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('ap.sName'    ,'ap_name'         ,DB_STRING));
  }
  
 /**
  * Executa a query do relat�rio.
  *
  * <p>M�todo para executar a query do relat�rio.</p>
  * @access public
  */
  public function makeQuery(){
    $this->csQuery = "SELECT
                      	p.fkContext as process_id,
                      	p.sName as process_name,
                      	p.nValue as process_value,
                      	u.sName as responsible_name,
                      	ap.fkContext as ap_id,
                      	ap.sName as ap_name
                      FROM
                      	view_rm_process_active p
                      	JOIN view_isms_user_active u ON (p.fkResponsible = u.fkContext)
                      	JOIN view_ci_nc_process_active nc_p ON (p.fkContext = nc_p.fkProcess)
                      	JOIN view_ci_nc_active nc ON (nc_p.fkNC = nc.fkContext)
                      	JOIN ci_nc_action_plan nc_ap ON (nc.fkContext = nc_ap.fkNC)
                      	JOIN view_ci_action_plan_active ap ON (nc_ap.fkActionPlan = ap.fkContext)
                      ORDER BY p.fkContext,
                      ap.fkContext";
    return parent::executeQuery();
  }

}

?>