<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportElementsFilter.
 *
 * <p>Classe que implementa o filtro para relatórios de elementos.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportElementsFilter extends FWDReportFilter {

  protected $cbShowProcess = false;
  protected $cbShowAsset = false;
  protected $cbShowRisk = false;
  protected $cbShowControl = false;

  public function showProcess($pbShow){
    $this->cbShowProcess = $pbShow;
  }
  
  public function showAsset($pbShow){
    $this->cbShowAsset = $pbShow;
  }
  
  public function showRisk($pbShow){
    $this->cbShowRisk = $pbShow;
  }
  
  public function showControl($pbShow){
    $this->cbShowControl = $pbShow;
  }
  
  public function mustShowProcess() {
  	return $this->cbShowProcess;
  }
  
  public function mustShowAsset() {
  	return $this->cbShowAsset;
  }
  
  public function mustShowRisk() {
  	return $this->cbShowRisk;
  }
  
  public function mustShowControl() {
  	return $this->cbShowControl;
  }
  
  public function getSummary(){
    $maFilters = array();
    
    $maElements = array();
    
    if($this->cbShowProcess){
      $moObject = new RMProcess();
      $maElements[] = $moObject->getLabel();      
    }
    if($this->cbShowAsset){
      $moObject = new RMAsset();
      $maElements[] = $moObject->getLabel();      
    }
    if($this->cbShowRisk){
      $moObject = new RMRisk();
      $maElements[] = $moObject->getLabel();      
    }
    if($this->cbShowControl){
      $moObject = new RMControl();
      $maElements[] = $moObject->getLabel();      
    }
    
    $maFilters[] = array(
      	'name' => FWDLanguage::getPHPStringValue('lb_elements_cl','Elementos:'),
      	'items' => array(implode(', ', $maElements))
    );        
    
    return $maFilters;
  }
}
?>