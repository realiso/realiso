<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe ISMSReportRiskAutoProbabilityValue.
 *
 * <p>Classe que implementa o relat�rio que exibe informa��es do c�lculo autom�tico de probabilidade do risco.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportRiskAutoProbabilityValue extends ISMSReport {
  
  /**
   * Inicializa o relat�rio.
   *
   * <p>M�todo para inicializar o relat�rio.
   * Necess�rio para funcionar com o XML compilado.</p>
   * @access public
   */  
  public function init() {
    parent::init();
        
    $this->coDataSet->addFWDDBField(new FWDDBField("","risk_id",DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("","risk_name",DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("","risk_residual_value",DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("","risk_accept_mode",DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("","schedule_period",DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("","schedule_value",DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("","schedule_date",DB_DATETIME));    
    $this->coDataSet->addFWDDBField(new FWDDBField("","incident_count",DB_NUMBER));    
  }
  
  /**
   * Executa a query do relat�rio.
   *
   * <p>M�todo para executar a query do relat�rio.</p>
   * @access public
   */
  public function makeQuery() {
  	
  	switch (FWDWebLib::getConnection()->getDatabaseType()) {
  		case DB_MSSQL:
  			$msCurrentDate = "GETDATE()";
  			$msDayCalc = "DATEADD(dd, -rs.nValue, $msCurrentDate)";
  			$msWeekCalc = "DATEADD(wk, -rs.nValue, $msCurrentDate)";
  			$msMonthCalc = "DATEADD(mm, -rs.nValue, $msCurrentDate)";
  		break;
  		
  		case DB_POSTGRES:
  			$msCurrentDate = "NOW()";
  			$msDayCalc = "$msCurrentDate - CAST(rs.nValue || ' day' as INTERVAL)";
  			$msWeekCalc = "$msCurrentDate - CAST(rs.nValue || ' week' as INTERVAL)";
  			$msMonthCalc = "$msCurrentDate - CAST(rs.nValue || ' month' as INTERVAL)";
  		break;
  		
  		case DB_ORACLE:
  			$msCurrentDate = "SYSDATE";
  			$msDayCalc = "$msCurrentDate - NUMTODSINTERVAL(rs.nValue, 'DAY')";
  			$msWeekCalc = "$msCurrentDate - NUMTODSINTERVAL(rs.nValue*7, 'DAY')";
  			$msMonthCalc = "$msCurrentDate - NUMTOYMINTERVAL(rs.nValue, 'MONTH')";
  		break;
  	}
											
		$this->csQuery = "SELECT
												r.fkContext as risk_id,
												r.sName as risk_name,
												r.nValueResidual as risk_residual_value,
												r.nAcceptMode as risk_accept_mode,
												rs.nPeriod as schedule_period,
												rs.nValue as schedule_value,
												rs.dDateLastCheck as schedule_date,
												(
												SELECT COUNT(ir.fkIncident) as incident_count
												FROM
													ci_incident_risk ir
													JOIN view_ci_incident_active i ON (i.fkContext = ir.fkIncident)
													WHERE ir.fkRisk = r.fkContext AND i.dDate >= 
														CASE rs.nPeriod
													         WHEN " . SCHEDULE_BYDAY . " THEN $msDayCalc
													         WHEN " . SCHEDULE_BYWEEK . " THEN $msWeekCalc
													         WHEN " . SCHEDULE_BYMONTH . " THEN $msMonthCalc
													      	END
													 AND i.dDate <= $msCurrentDate
												) as incident_count
											FROM
												view_rm_risk_active r
												JOIN ci_risk_schedule rs ON (r.fkContext = rs.fkRisk)
											ORDER BY r.nValueResidual DESC";
  	
    return parent::executeQuery();
  }
}
?>