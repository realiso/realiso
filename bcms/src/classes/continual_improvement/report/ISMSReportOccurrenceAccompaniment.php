<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe ISMSReportOccurrenceAccompaniment.
 *
 * <p>Classe que implementa o relat�rio acompanhamento de ocorr�ncias.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportOccurrenceAccompaniment extends ISMSReport {
  
 /**
  * Inicializa o relat�rio.
  *
  * <p>M�todo para inicializar o relat�rio.
  * Necess�rio para funcionar com o XML compilado.</p>
  * @access public
  */
  public function init(){
    parent::init();
    $this->coDataSet->addFWDDBField(new FWDDBField('',	'occur_id',											DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('',	'occur_description',						DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('',	'occur_denial_justification',		DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('',	'occur_state',									DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('',	'occur_creator',								DB_STRING));
  }
  
  /**
   * Executa a query do relat�rio.
   *
   * <p>M�todo para executar a query do relat�rio.</p>
   * @access public
   */
  public function makeQuery(){
  	$msStatus = "";
    if ($this->coFilter->getStatus()) $msStatus = " AND c.nState = " . $this->coFilter->getStatus();
    $this->csQuery = "SELECT 	o.fkContext as occur_id, 
												o.tDescription as occur_description, 
												o.tDenialJustification as occur_denial_justification, 
												c.nState as occur_state, 
												u.sName as occur_creator
											FROM
												view_ci_occurrence_active o
												JOIN isms_context c ON (o.fkContext = c.pkContext $msStatus)
												JOIN isms_context_date cd ON (c.pkContext = cd.fkContext AND cd.nAction = " . ACTION_CREATE . ")
												LEFT JOIN isms_user u ON (cd.nUserId = u.fkContext)
											ORDER BY
												o.fkContext";

    return parent::executeQuery();
  }
  
 /**
  * Desenha o cabe�alho do relat�rio.
  *
  * <p>M�todo para desenhar o cabe�alho do relat�rio.</p>
  * @access public
  */
  public function drawHeader(){
    foreach($this->caHeaders as $moHeader){
      if($moHeader->getAttrName()=='header_filter'){
        $moHeaderFilter = $moHeader;
      }elseif($moHeader->getAttrName()=='header_filter_item'){
        $moHeaderFilterItem = $moHeader;
      }elseif($moHeader->getAttrName()=='report_header_blank'){
        $moHeaderBlank = $moHeader;
      }else{
        $this->coWriter->drawLine($moHeader,array());
      }
    }
    if($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());
    
    $moFilterText = FWDWebLib::getObject('header_filter_text');
    $moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
    $maFilters = $this->coFilter->getSummary();
    foreach($maFilters as $maFilter){
      $moFilterText->setValue($maFilter['name']);
      $this->coWriter->drawLine($moHeaderFilter,array());
      foreach($maFilter['items'] as $msFilterItem){
        $moFilterItemText->setValue($msFilterItem);
        $this->coWriter->drawLine($moHeaderFilterItem,array());
      }
    }
    $this->coWriter->drawLine($moHeaderBlank,array());
  }
}
?>