<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportDuplicatedRisks.
 *
 * <p>Classe que implementa o relatório de riscos duplicados e seus ativos.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportDuplicatedRisks extends ISMSReport {
  
  public function __construct(){
    parent::__construct();
  }
  
 /**
  * Inicializa o relatório.
  *
  * <p>Método para inicializar o relatório.
  * Necessário para funcionar com o XML compilado.</p>
  * @access public
  */
  public function init(){
    parent::init();
    $this->coDataSet->addFWDDBField(new FWDDBField('r.sName',       'risk_name',  DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('a.fkContext',   'asset_id',   DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('a.sName',       'asset_name', DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('a.nValue',      'asset_value',DB_NUMBER));
  }
  
 /**
  * Executa a query do relatório.
  *
  * <p>Método para executar a query do relatório.</p>
  * @access public
  */
  public function makeQuery(){
    $this->csQuery = "SELECT DISTINCT
                        r.sName as risk_name,
                        a.fkContext as asset_id,
                        a.sName as asset_name,
                        a.nValue as asset_value
                      FROM
                        (
                          SELECT  
                            sName as risk_name,
                            fkAsset as risk_asset_id,
                            count(fkContext) as risk_count
                          FROM
                            view_rm_risk_active r
                          GROUP BY
                            sName, fkAsset
                          HAVING
                            count(fkContext) > 1
                        ) dup
                        JOIN view_rm_risk_active r ON (dup.risk_name = r.sName)
                        JOIN view_rm_asset_active a ON (r.fkAsset = a.fkContext)
                      ORDER BY
                        r.sName";
    return parent::executeQuery();
  }

}

?>