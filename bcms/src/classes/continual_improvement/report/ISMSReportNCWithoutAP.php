<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe ISMSReportNCWithoutAP.
 *
 * <p>Classe que implementa o relat�rio de n�o conformidades sem plano de a��o associado.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportNCWithoutAP extends ISMSReport {
  /**
   * Inicializa o relat�rio.
   *
   * <p>M�todo para inicializar o relat�rio.
   * Necess�rio para funcionar com o XML compilado.</p>
   * @access public
   */  
  public function init() {
    parent::init();
    
    $this->coDataSet->addFWDDBField(new FWDDBField("","nc_id",DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("","nc_name",DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("","responsible_id",DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("","responsible_name",DB_STRING));
  }

  /**
   * Executa a query do relat�rio.
   *
   * <p>M�todo para executar a query do relat�rio.</p>
   * @access public
   */
  public function makeQuery() {
    $this->csQuery = "SELECT nc.fkContext as nc_id,
                             nc.sName as nc_name,
                             u.fkContext as responsible_id,
                             u.sName as responsible_name
                        FROM view_ci_nc_active nc
                            LEFT JOIN view_isms_user_active u ON (nc.fkResponsible = u.fkContext)
                          WHERE nc.fkContext NOT IN ( SELECT fkNC FROM ci_nc_action_plan )
    ";
    return parent::executeQuery();
  }

  /**
   * Desenha o cabe�alho do relat�rio.
   *
   * <p>M�todo para desenhar o cabe�alho do relat�rio.</p>
   * @access public
   */
  public function drawHeader(){
    foreach($this->caHeaders as $moHeader){
      if($moHeader->getAttrName()=='header_filter'){
        $moHeaderFilter = $moHeader;
      }elseif($moHeader->getAttrName()=='header_filter_item'){
        $moHeaderFilterItem = $moHeader;
      }elseif($moHeader->getAttrName()=='report_header_blank'){
        $moHeaderBlank = $moHeader;
      }else{
        $this->coWriter->drawLine($moHeader,array());
      }
    }
    if ($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());
    
    $moFilterText = FWDWebLib::getObject('header_filter_text');
    $moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
    $this->coWriter->drawLine($moHeaderBlank,array());
  }
}
?>