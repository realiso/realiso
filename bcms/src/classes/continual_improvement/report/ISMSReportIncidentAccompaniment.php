<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe ISMSReportIncidentAccompaniment.
 *
 * <p>Classe que implementa o relat�rio de acompanhamento de incidentes.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportIncidentAccompaniment extends ISMSReport {
  
 /**
  * Inicializa o relat�rio.
  *
  * <p>M�todo para inicializar o relat�rio.
  * Necess�rio para funcionar com o XML compilado.</p>
  * @access public
  */
  public function init(){
    parent::init();
    $this->coDataSet->addFWDDBField(new FWDDBField('',	'incident_id',									DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('',	'incident_name',								DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('',	'incident_disposal_limit',			DB_DATETIME));
    $this->coDataSet->addFWDDBField(new FWDDBField('',	'incident_solution_limit',			DB_DATETIME));
    $this->coDataSet->addFWDDBField(new FWDDBField('',	'incident_state',								DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('',	'incident_responsible_name',		DB_STRING));
  }
  
  /**
   * Executa a query do relat�rio.
   *
   * <p>M�todo para executar a query do relat�rio.</p>
   * @access public
   */
  public function makeQuery(){
  	$maJoin = array();
    $maWhere = array();
    
    if($this->coFilter->getCategory()){      
      $maWhere[] = "i.fkCategory = " . $this->coFilter->getCategory();
    }
    
    if($this->coFilter->getLossType()){
      $maWhere[] = "i.nLossType = " . $this->coFilter->getLossType();
    }
    
    $msJoin = "";
    if($this->coFilter->getStatus()){
      $msJoin = "JOIN isms_context ctx ON (i.fkContext = ctx.pkContext AND ctx.nState = " . $this->coFilter->getStatus() . ")";      
    }
    
    if($this->coFilter->getDisposalDateStart()){
      $maWhere[] = "i.dDateLimit >= ".ISMSLib::getTimestampFormat($this->coFilter->getDisposalDateStart());
    }
    
    if($this->coFilter->getDisposalDateFinish()){
      $maWhere[] = "i.dDateLimit <= ".ISMSLib::getTimestampFormat($this->coFilter->getDisposalDateFinish());
    }
    
    if($this->coFilter->getSolutionDateStart()){
      $maWhere[] = "i.dDateFinish >= ".ISMSLib::getTimestampFormat($this->coFilter->getSolutionDateStart());
    }
    
    if($this->coFilter->getSolutionDateFinish()){
      $maWhere[] = "i.dDateFinish <= ".ISMSLib::getTimestampFormat($this->coFilter->getSolutionDateFinish());
    }
    
    if(count($maWhere)){
      $msWhere = ' WHERE ' . implode(' AND ', $maWhere);
    }else{
      $msWhere = '';
    }
    
    $this->csQuery = "SELECT 	i.fkContext as incident_id,
												i.sName as incident_name,
												i.dDateLimit as incident_disposal_limit,
												i.dDateFinish as incident_solution_limit,
												c.nState as incident_state,
												u.sName as incident_responsible_name
											FROM
												view_ci_incident_active i
												JOIN isms_context c ON (i.fkContext = c.pkContext)
												JOIN isms_user u ON (i.fkResponsible = u.fkContext)
												$msJoin
												$msWhere";

    return parent::executeQuery();
  }
  
 /**
  * Desenha o cabe�alho do relat�rio.
  *
  * <p>M�todo para desenhar o cabe�alho do relat�rio.</p>
  * @access public
  */
  public function drawHeader(){
    foreach($this->caHeaders as $moHeader){
      if($moHeader->getAttrName()=='header_filter'){
        $moHeaderFilter = $moHeader;
      }elseif($moHeader->getAttrName()=='header_filter_item'){
        $moHeaderFilterItem = $moHeader;
      }elseif($moHeader->getAttrName()=='report_header_blank'){
        $moHeaderBlank = $moHeader;
      }else{
        $this->coWriter->drawLine($moHeader,array());
      }
    }
    if($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());
    
    $moFilterText = FWDWebLib::getObject('header_filter_text');
    $moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
    $maFilters = $this->coFilter->getSummary();
    foreach($maFilters as $maFilter){
      $moFilterText->setValue($maFilter['name']);
      $this->coWriter->drawLine($moHeaderFilter,array());
      foreach($maFilter['items'] as $msFilterItem){
        $moFilterItemText->setValue($msFilterItem);
        $this->coWriter->drawLine($moHeaderFilterItem,array());
      }
    }
    $this->coWriter->drawLine($moHeaderBlank,array());
  }
}
?>