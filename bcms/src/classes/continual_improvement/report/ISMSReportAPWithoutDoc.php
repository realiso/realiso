<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe ISMSReportAPWithoutDoc.
 *
 * <p>Classe que implementa o relat�rio de plano de a��o sem documento.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportAPWithoutDoc extends ISMSReport {
  /**
   * Inicializa o relat�rio.
   *
   * <p>M�todo para inicializar o relat�rio.
   * Necess�rio para funcionar com o XML compilado.</p>
   * @access public
   */  
  public function init() {
    parent::init();
    
    $this->coDataSet->addFWDDBField(new FWDDBField("","ap_id",DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("","ap_name",DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("","responsible_id",DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("","responsible_name",DB_STRING));
  }

  /**
   * Executa a query do relat�rio.
   *
   * <p>M�todo para executar a query do relat�rio.</p>
   * @access public
   */
  public function makeQuery() {
    $this->csQuery = "SELECT ap.fkContext as ap_id,
                             ap.sName as ap_name,
                             u.fkContext as responsible_id,
                             u.sName as responsible_name
                      FROM view_ci_action_plan_active ap
                      LEFT JOIN view_isms_user_active u ON (ap.fkResponsible = u.fkContext)
                      WHERE (ap.fkContext NOT IN (
                        SELECT dc.fkContext 
                        FROM view_pm_doc_context_active dc
                          JOIN view_pm_document_active d ON (dc.fkdocument = d.fkcontext)
                          JOIN isms_context c ON (d.fkcontext = c.pkcontext)
                          JOIN view_pm_doc_instance_active di ON (d.fkcurrentversion = di.fkcontext)
                        WHERE
                        (
                          c.nState = ".CONTEXT_STATE_DOC_APPROVED." 
                          OR ( c.nState = ".CONTEXT_STATE_DOC_DEVELOPING."  AND di.nMajorVersion > 0)
                          OR ( c.nState = ".CONTEXT_STATE_DOC_PENDANT." AND di.dEndProduction IS NOT NULL )
                        )
                    ))
    ";
    return parent::executeQuery();
  }

  /**
   * Desenha o cabe�alho do relat�rio.
   *
   * <p>M�todo para desenhar o cabe�alho do relat�rio.</p>
   * @access public
   */
  public function drawHeader(){
    foreach($this->caHeaders as $moHeader){
      if($moHeader->getAttrName()=='header_filter'){
        $moHeaderFilter = $moHeader;
      }elseif($moHeader->getAttrName()=='header_filter_item'){
        $moHeaderFilterItem = $moHeader;
      }elseif($moHeader->getAttrName()=='report_header_blank'){
        $moHeaderBlank = $moHeader;
      }else{
        $this->coWriter->drawLine($moHeader,array());
      }
    }
    if ($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());
    
    $moFilterText = FWDWebLib::getObject('header_filter_text');
    $moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
    $this->coWriter->drawLine($moHeaderBlank,array());
  }
}
?>