<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe ISMSReportControlNonConformityByProcess.
 *
 * <p>Classe que implementa o relat�rio de processos por n�o conformidade de controle.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportControlNonConformityByProcess extends ISMSReport {
  
 /**
  * Inicializa o relat�rio.
  *
  * <p>M�todo para inicializar o relat�rio.
  * Necess�rio para funcionar com o XML compilado.</p>
  * @access public
  */
  public function init(){
    parent::init();
    $this->coDataSet->addFWDDBField(new FWDDBField('',	'process_id',						DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('',	'process_name',					DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('',	'process_value',				DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('',	'nc_id',								DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('',	'nc_name',							DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('',	'control_name',					DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('',	'control_is_active',		DB_NUMBER));    
  }
  
  /**
   * Executa a query do relat�rio.
   *
   * <p>M�todo para executar a query do relat�rio.</p>
   * @access public
   */
  public function makeQuery(){    
    $maWhere = array();
    
    if($this->coFilter->getClassificationType()){      
      $maWhere[] = "nc.nClassification = " . $this->coFilter->getClassificationType();
    }
    
    if($this->coFilter->getCapability()){
      $maWhere[] = "nc.ncapability = " . $this->coFilter->getCapability();
    }  
    
    if(count($maWhere)){
      $msWhere = ' WHERE ' . implode(' AND ', $maWhere);
    }else{
      $msWhere = '';
    }
    
    $msJoin='';
    if($this->coFilter->getStatus())
    	$msJoin = "JOIN isms_context ctx ON (nc.fkContext = ctx.pkContext AND ctx.nState = {$this->coFilter->getStatus()})";
    
        
    $this->csQuery = "SELECT p.fkContext as process_id,
												p.sName as process_name,
												p.nValue as process_value,
												nc.fkContext as nc_id,
												nc.sName as nc_name,
												ctr.sName as control_name,
												ctr.bIsActive as control_is_active
											FROM view_rm_process_active p
											JOIN view_ci_nc_process_active ncp ON (p.fkContext = ncp.fkProcess)
											JOIN view_ci_nc_active nc ON (ncp.fkNC = nc.fkContext)
											JOIN view_rm_control_active ctr ON (nc.fkControl = ctr.fkContext)
											$msJoin
											$msWhere
											ORDER BY p.nValue DESC, p.sName, p.fkContext";
    return parent::executeQuery();
  }
  
 /**
  * Desenha o cabe�alho do relat�rio.
  *
  * <p>M�todo para desenhar o cabe�alho do relat�rio.</p>
  * @access public
  */
  public function drawHeader(){
    foreach($this->caHeaders as $moHeader){
      if($moHeader->getAttrName()=='header_filter'){
        $moHeaderFilter = $moHeader;
      }elseif($moHeader->getAttrName()=='header_filter_item'){
        $moHeaderFilterItem = $moHeader;
      }elseif($moHeader->getAttrName()=='report_header_blank'){
        $moHeaderBlank = $moHeader;
      }else{
        $this->coWriter->drawLine($moHeader,array());
      }
    }
    if($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());
    
    $moFilterText = FWDWebLib::getObject('header_filter_text');
    $moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
    $maFilters = $this->coFilter->getSummary();
    foreach($maFilters as $maFilter){
      $moFilterText->setValue($maFilter['name']);
      $this->coWriter->drawLine($moHeaderFilter,array());
      foreach($maFilter['items'] as $msFilterItem){
        $moFilterItemText->setValue($msFilterItem);
        $this->coWriter->drawLine($moHeaderFilterItem,array());
      }
    }
    $this->coWriter->drawLine($moHeaderBlank,array());
  }
}
?>