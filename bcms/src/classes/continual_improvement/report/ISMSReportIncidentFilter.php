<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportIncidentFilter.
 *
 * <p>Classe que implementa o filtro de relat�rios de incidente.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportIncidentFilter extends FWDReportFilter {

  protected $ciStatus = 0;
  protected $ciLossType = 0;
  protected $ciCategory = 0;
  protected $ciDisposalStart = 0;
  protected $ciDisposalFinish = 0;
  protected $ciSolutionStart = 0;
  protected $ciSolutionFinish = 0;
  protected $csOrderBy = "";
  
  public function setOrderBy($psOrderBy){
  	$msOrder = "i.sName, i.fkContext";
  	switch ($psOrderBy){
  	case "responsible":
  		$this->csOrderBy = "i.fkresponsible, $msOrder";
  		break;
  	case "occurrence_qt":
  		$this->csOrderBy = "(SELECT count(1) as count FROM view_ci_occurrence_active oi where oi.fkIncident = i.fkContext) DESC, $msOrder";
  		break;
  	case "category":
  		$this->csOrderBy = "i.fkCategory, $msOrder";
  		break;
  	case "name":
  		$this->csOrderBy = $msOrder;
  		break;
  	default:
  		$this->csOrderBy = "i.ddate, $msOrder";
  	}
  }

  public function getOrderBy(){
  	return $this->csOrderBy;
  }

  public function setCategory($piCategory){
    $this->ciCategory = $piCategory;
  }
  
  public function getCategory(){
    return $this->ciCategory;
  }
  
  public function setLossType($piLossType){
    $this->ciLossType = $piLossType;
  }
  
  public function setDisposalDateStart($piDate){
    $this->ciDisposalStart = $piDate;
  }
  
  public function setDisposalDateFinish($piDate){
    $this->ciDisposalFinish = $piDate;
  }
  
  public function setSolutionDateStart($piDate){
    $this->ciSolutionStart = $piDate;
  }
  
  public function setSolutionDateFinish($piDate){
    $this->ciSolutionFinish = $piDate;
  }
  
  public function getDisposalDateStart() {
  	return $this->ciDisposalStart;
  }
  
  public function getDisposalDateFinish() {
  	return $this->ciDisposalFinish;
  }
  
  public function getSolutionDateStart() {
  	return $this->ciSolutionStart;
  }
  
  public function getSolutionDateFinish() {
  	return $this->ciSolutionFinish;
  }
  
  public function getLossType(){
    return $this->ciLossType;
  }
  
  public function setStatus($piStatus){
    $this->ciStatus = $piStatus;
  }
  
  public function getStatus(){
    return $this->ciStatus;
  }

  public function getSummary(){
    $maFilters = array();
        
    if($this->ciCategory){
      $moCICategory = new CICategory();
      $moCICategory->fetchById($this->ciCategory);
      $msCategoryFilter = $moCICategory->getFieldValue("category_name");      
    }
    else $msCategoryFilter = FWDLanguage::getPHPStringValue('mx_all','Todas');
    $maFilters[] = array(
      	'name' => FWDLanguage::getPHPStringValue('lb_incident_category_cl','Categoria:'),
      	'items' => array($msCategoryFilter)
    );
        
    if($this->ciLossType){
      if ($this->ciLossType == INCIDENT_LOSS_TYPE_DIRECT) $msLossType = FWDLanguage::getPHPStringValue('mx_direct','Direta');
      else $msLossType = FWDLanguage::getPHPStringValue('mx_indirect','Indireta');      
    }
    else $msLossType = FWDLanguage::getPHPStringValue('mx_all','Todos');
    $maFilters[] = array(
      	'name' => FWDLanguage::getPHPStringValue('lb_loss_type_cl','Tipo de Perda:'),
      	'items' => array($msLossType)
    );    
    
    if($this->ciStatus){
      $msStatus = ISMSContextObject::getContextStateAsString($this->ciStatus);      
    }
    else $msStatus = FWDLanguage::getPHPStringValue('mx_all','Todos');
    $maFilters[] = array(
      	'name' => FWDLanguage::getPHPStringValue('lb_status_cl','Status:'),
      	'items' => array($msStatus)
    );
    
    $msInitialDate = ($this->ciDisposalStart?ISMSLib::getISMSShortDate($this->ciDisposalStart,true):'');
    $msFinalDate = ($this->ciDisposalFinish?ISMSLib::getISMSShortDate($this->ciDisposalFinish,true):'');
    if($msInitialDate || $msFinalDate){
      if($msInitialDate){
        if($msFinalDate){
          $msDateFilter = FWDLanguage::getPHPStringValue('mx_between_initialdate_finaldate','Entre %initialDate% e %finalDate%');
        }else{
          $msDateFilter = FWDLanguage::getPHPStringValue('mx_after_initialdate','Depois de %initialDate%');
        }
      }else{
        $msDateFilter = FWDLanguage::getPHPStringValue('mx_before_finaldate','Antes de %finalDate%');
      }
      $msDateFilter = str_replace(array('%initialDate%','%finalDate%'),array($msInitialDate,$msFinalDate),$msDateFilter);
      $maFilters[] = array(
        'name' => FWDLanguage::getPHPStringValue('mx_immediate_disposal','Disposi��o Imediata'),
        'items' => array($msDateFilter)
      );
    }

    $msInitialDate = ($this->ciSolutionStart?ISMSLib::getISMSShortDate($this->ciSolutionStart,true):'');
    $msFinalDate = ($this->ciSolutionFinish?ISMSLib::getISMSShortDate($this->ciSolutionFinish,true):'');
    if($msInitialDate || $msFinalDate){
      if($msInitialDate){
        if($msFinalDate){
          $msDateFilter = FWDLanguage::getPHPStringValue('mx_between_initialdate_finaldate','Entre %initialDate% e %finalDate%');
        }else{
          $msDateFilter = FWDLanguage::getPHPStringValue('mx_after_initialdate','Depois de %initialDate%');
        }
      }else{
        $msDateFilter = FWDLanguage::getPHPStringValue('mx_before_finaldate','Antes de %finalDate%');
      }
      $msDateFilter = str_replace(array('%initialDate%','%finalDate%'),array($msInitialDate,$msFinalDate),$msDateFilter);
      $maFilters[] = array(
        'name' => FWDLanguage::getPHPStringValue('lb_solution_date_cl','Data da Solu��o:'),
        'items' => array($msDateFilter)
      );
    }    
    return $maFilters;
  }
}
?>