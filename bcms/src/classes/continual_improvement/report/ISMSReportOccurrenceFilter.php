<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportOccurrenceFilter.
 *
 * <p>Classe que implementa o filtro de relatórios de ocorrência.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportOccurrenceFilter extends FWDReportFilter {

  protected $ciStatus = 0;  
  
  public function setStatus($piStatus){
    $this->ciStatus = $piStatus;
  }
  
  public function getStatus(){
    return $this->ciStatus;
  }

  public function getSummary(){
    $maFilters = array();

    if($this->ciStatus){
      $msStatus = ISMSContextObject::getContextStateAsString($this->ciStatus);      
    }
    else $msStatus = FWDLanguage::getPHPStringValue('mx_all','Todos');
    $maFilters[] = array(
      	'name' => FWDLanguage::getPHPStringValue('lb_status_cl','Status:'),
      	'items' => array($msStatus)
    );

    return $maFilters;
  }

}
?>