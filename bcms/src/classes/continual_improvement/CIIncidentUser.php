<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
 
/**
 * Classe CIIncidentUser
 *
 * <p>Classe que representa a tabela de processo disciplinar (usuario x incidente).</p>
 * @package ISMS
 * @subpackage classes
 */
class CIIncidentUser extends ISMSTable {
 /**
  * Construtor.
  * 
  * <p>Construtor da classe CIIncidentUser.</p>
  * @access public 
  */
	public function __construct() {
		$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    parent::__construct("ci_incident_user");
        
    $this->coDataset->addFWDDBField(new FWDDBField("fkUser", 			  "user_id",      DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("fkIncident",    "incident_id",  DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("tDescription",  "description",  DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField("tActionTaken",  "action_taken", DB_STRING));
  }
  
  /**
  * Busca informa��es de um registro atrav�s do id do incidente e do id do usu�rio.
  * 
  * <p>M�todo para buscar informa��es de um registro atrav�s do id do incidente e do id do usu�rio.</p>
  * @access public 
  * @param integer $piIncidentId Id do incident
  * @param integer $piUserId Id do usu�rio
  */ 
  public function fetchByIncidentUser($piIncidentId, $piUserId) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $this->createFilter($piIncidentId, 'incident_id');
    $this->createFilter($piUserId, 'user_id');
    $this->select();
    return $this->fetch();
  }
}
?>