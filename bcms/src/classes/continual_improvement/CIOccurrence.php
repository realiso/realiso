<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */ 
 
/**
 * Classe CIOccurrence
 *
 * <p>Classe que representa a tabela de ocorr�ncias.</p>
 * @package ISMS
 * @subpackage classes
 */
class CIOccurrence extends ISMSContext implements IWorkflow {
 /**
  * Construtor.
  * 
  * <p>Construtor da classe CIOccurrence.</p>
  * @access public 
  */
  public function __construct() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    parent::__construct("ci_occurrence");
    $this->csAliasId = "occurrence_id";
    $this->ciContextType = CONTEXT_CI_OCCURRENCE;
    $this->csDependenceAliasId = "incident_id";  
    $this->coDataset->addFWDDBField(new FWDDBField("fkContext",             "occurrence_id",                    DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("fkIncident",            "incident_id",                      DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("tDescription",          "occurrence_description",           DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField("tDenialJustification",  "occurrence_denial_justif",         DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField("dDate",  								"occurrence_date",									DB_DATETIME));
    
	$this->caSearchableFields = array('occurrence_description','occurrence_denial_justif');
	
    $this->coWorkflow = new OccurrenceWorkflow(ACT_OCCURRENCE_APPROVAL);
  }
  
  /**
  * Retorna o label do contexto.
  *
  * <p>M�todo para retornar o label do contexto.</p>
  * @access public
  * @return string Label do contexto
  */
  public function getLabel(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return FWDLanguage::getPHPStringValue('mx_occurrence', "Ocorr�ncia");
  }
  
  /**
  * Retorna o nome do contexto.
  *
  * <p>M�todo para retornar o nome do contexto.</p>
  * @access public
  * @return string Nome do contexto
  */
  public function getName(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $msDescription = $this->getFieldValue('occurrence_description');
    return mb_substr($msDescription,0,50,'UTF-8').(strlen($msDescription)>50?utf8_encode("..."):"");
  }
  
  /**
  * Retorna a descri��o do contexto.
  *
  * <p>M�todo para retornar a descri��o do contexto.</p>
  * @access public
  * @return string Descri��o do contexto
  */
  public function getDescription(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $msDescription = $this->getFieldValue('occurrence_description');
    return substr($msDescription,0,50).(strlen($msDescription)>50?"...":"");
  }
  
  /**
   * Verifica se o usu�rio pode deletar a ocorr�ncia.
   * 
   * <p>M�todo para verificar se o usu�rio pode deletar a ocorr�ncia.</p>
   * @access public
   * @param integer piOccurrenceId Id da ocorr�ncia
   * @return boolean True|False
   */
  public function userCanDelete($piOccurrenceId){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
    if(!in_array('M.CI.1.4',$maACLs)){
      return true;
    }else{
      $moOccurrence = new CIOccurrence();
      $moOccurrence->fetchById($piOccurrenceId);
      $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
      if ($moOccurrence->getApprover() == $miUserId) {
        return true;
      }
      return false;
    }
  }
  
   /**
   * Verifica se o usu�rio pode editar a ocorr�ncia.
   * 
   * <p>M�todo para verificar se o usu�rio pode editar a ocorr�ncia.</p>
   * @access public
   * @param integer piOccurrenceId Id do ocorr�ncia
   * @return boolean True|False
   */
  public function userCanEdit($piOccurrenceId,$piUserResponsible = 0){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

    $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
    if(!in_array('M.CI.1.3',$maACLs)){
      return true;
    }else{
      $moOccurrence = new CIOccurrence();
      $moOccurrence->fetchById($piOccurrenceId);
      $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId(); 
      if ($moOccurrence->getApprover() == $miUserId) {
        return true;
      } elseif($moOccurrence->getContextState()==CONTEXT_STATE_PENDANT && $moOccurrence->getCreator() == $miUserId) {
        return true;
      } 
      return false;
    }
  }
  
  /**
   * Verifica se o usu�rio pode inserir uma ocorr�ncia.
   * 
   * <p>M�todo para verificar se o usu�rio pode inserir uma ocorr�ncia.</p>
   * @access public
   * @return boolean True|False
   */
  public function userCanInsert(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
    if(!in_array('M.CI.1.2',$maACLs)){
      return true;
    }else{
      return false;
    }
  }
  
  /**
   * Retorna o aprovador do contexto.
   * 
   * <p>Retorna o aprovador do contexto.</p>
   * @access public
   * @return integer Id do aprovador do contexto.
   */
  public function getApprover(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    return ISMSLib::getConfigById(USER_INCIDENT_MANAGER);
  }
  
  /**
   * Retorna o respons�vel do contexto.
   * 
   * <p>Retorna o respons�vel do contexto.</p>
   * @access public
   * @return integer Id do respons�vel do contexto.
   */
  public function getResponsible(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    return $this->getCreator();
  }
  
  /**
  * Indica se o contexto � delet�vel ou n�o.
  * 
  * <p>M�todo que indica se o contexto � delet�vel ou n�o.</p>
  * @access public
  * @param integer $piContextId id do contexto
  */
  public function isDeletable($piContextId){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $moOccurrence = new CIOccurrence();
    if ($moOccurrence->fetchById($piContextId) && $moOccurrence->getFieldValue('incident_id'))
      return false;
    else 
      return true;
  }
  
  /**
  * Exibe uma popup caso n�o seja poss�vel remover um contexto.
  * 
  * <p>M�todo que exibe uma popup caso n�o seja poss�vel remover um contexto.</p>
  * @access public
  * @param integer $piContextId id do contexto
  */ 
  public function showDeleteError() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $msTitle = FWDLanguage::getPHPStringValue('mx_occurrence_remove_error','Erro ao remover Ocorr�ncia');
    $msMessage = FWDLanguage::getPHPStringValue('mx_occurrence_remove_error_message',"N�o foi poss�vel remover a ocorr�ncia, pois ela est� associada a um incidente.");
    ISMSLib::openOk($msTitle,$msMessage,"",60);
  }
  
/**
  * Retorna o caminho do contexto.
  * 
  * <p>M�todo para retornar o caminho do contexto.</p>
  * @access public 
  * @return string Caminho do contexto
  */ 
  public function getSystemPath() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $msRMPath = "<a href='javascript:isms_redirect_to_mode(" . INCIDENT_MODE . ",0,0)'>".FWDLanguage::getPHPStringValue('st_incident_module_name','Melhoria Cont�nua')."</a>";
    $msContextPath = ISMSLib::getIconCode('icon-ci_occurrence.gif',-2,-4). "<a href='javascript:isms_redirect_to_mode(" . INCIDENT_MODE . "," . CONTEXT_CI_OCCURRENCE . "," . $this->getFieldValue($this->getAliasId()) . ")'>" . $this->getName() . "</a>";
    return $msRMPath ."&nbsp;".ISMSLib::getIconCode('icon-arrow-right.gif',-2,0).'&nbsp;' . $msContextPath;
  }

 /**
  * Retorna o c�digo HTML do icone do contexto.
  * 
  * <p>M�todo para retornar o c�digo HTML do contexto</p>
  * @access public
  * @param integer $piId Id do contexto
  * @param integer $pbIsFetched id do contexto
  * @return string contendo o c�digo HTML do icone do contexto
  */ 
  public function getIconCode($piId, $pbIsFetched = false){
    return ISMSLib::getIconCode('icon-ci_occurrence.gif', -2, 7);
  }

 /**
  * Retorna o caminho para abrir a popup de edi��o desse contexto.
  * 
  * <p>M�todo para retornar o caminho para abrir a popup de edi��o desse contexto.</p>
  * @param integer $piContextId id do contexto
  * @access public 
  * @return string caminho para abrir a popup de edi��o desse contexto
  */ 
  public function getVisualizeEditEvent($piContextId,$psUniqId){
    $soSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    $msPackagesIncidentPath = ($soSession->getMode()==MANAGER_MODE) ? "packages/improvement/" : "../../packages/improvement/";
    $msPopupId = "popup_occurrence_edit";
    
    return "isms_open_popup('{$msPopupId}','{$msPackagesIncidentPath}{$msPopupId}.php?occurrence=$piContextId','','true',292,420);"
           ."soPopUpManager.getPopUpById('{$msPopupId}').setCloseEvent( function close_visualize_$psUniqId() {soPopUpManager.closePopUp('popup_visualize_$psUniqId');} );";
  }

  /**
  * Retorna o �cone do contexto.
  *
  * <p>M�todo para retornar o �cone do contexto.</p>
  * @access public
  * @param integer $piContextId Id do contexto
  * @return string Nome do �cone
  */
  public function getIcon($piContextId = 0){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return 'icon-ci_occurrence.gif';
  }
}
?>