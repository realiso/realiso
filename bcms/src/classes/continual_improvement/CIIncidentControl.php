<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
 
/**
 * Classe CIIncidentControl
 *
 * <p>Classe que representa a tabela de rela��o entre incidente e controle.</p>
 * @package ISMS
 * @subpackage classes
 */
class CIIncidentControl extends ISMSContext implements IAssociation{
 /**
  * Construtor.
  * 
  * <p>Construtor da classe CIIncidentControl.</p>
  * @access public 
  */
  public function __construct() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    parent::__construct("ci_incident_control");
    $this->csAliasId = "incident_control_id";
    $this->ciContextType = CONTEXT_CI_INCIDENT_CONTROL;
    $this->coDataset->addFWDDBField(new FWDDBField("fkContext" ,"incident_control_id",DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("fkIncident","incident_id",        DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("fkControl", "control_id",         DB_NUMBER));
  }

/**
  * Retorna o nome do contexto.
  * 
  * <p>M�todo para retornar o nome do contexto.</p>
  * @access public 
  * @return string Nome do contexto
  */
  public function getName() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $moIncident = new CIIncident();
    $moIncident->fetchById($this->getFieldValue('incident_id'));
    $moControl = new RMControl();
    $moControl->fetchById($this->getFieldValue('control_id'));
    return $moControl->getName() . ' -> ' . $moIncident->getName();
  }
  
 /**
  * Retorna o label do contexto.
  * 
  * <p>M�todo para retornar o label do contexto.</p>
  * @access public 
  * @return string Label do contexto
  */
  public function getLabel() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return FWDLanguage::getPHPStringValue('mx_incident_control_association', "Associa��o de Controle a Incidente");
  }

 /**
  * Retorna o identificador do primeiro contexto da rela��o.
  * 
  * <p>M�todo para retornar o identificador do primeiro contexto da rela��o.</p>
  * @access public 
  * @return string Alias
  */
  public function getFirstContextId() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return $this->getFieldValue('control_id');
  }
 
 /**
  * Retorna o identificador do segundo contexto da rela��o.
  * 
  * <p>M�todo para retornar o identificador do segundo contexto da rela��o.</p>
  * @access public 
  * @return string Alias
  */
  public function getSecondContextId() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return $this->getFieldValue('incident_id');
  }
    protected function userCanDelete($piContextId){ return true; }
  protected function userCanEdit($piContextId,$piUserResponsible = 0){ return true; }
  protected function userCanInsert(){ return true; }

}
?>