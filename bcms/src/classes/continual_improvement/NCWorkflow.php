<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe NCWorkflow.
 *
 * <p>Classe que manipula os estados do workflow das N�o Conformidades.
 * O workflow � respons�vel por disparar as devidas tarefas.
 * � ele que sabe quais e quantas tarefas ser�o disparadas, de acordo
 * com o estado do contexto utilizado.</p>
 * @package ISMS
 * @subpackage classes
 */
class NCWorkflow extends Workflow {

 /**
  * Avan�a o estado do contexto no workflow.
  * 
  * <p>M�todo para avan�ar o estado do contexto no workflow.</p>
  * @access public 
  * 
  * @param IWorkflow $poDocument Objeto do documento
  * @param integer $piAction Tipo da a��o
  * @param string $psJustification Justificativa
  */
  public function stateForward(IWorkflow $poContext, $piAction, $psJustification = ''){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $moContext = new CINonConformity();
    $moContext->fetchById($poContext->getId());
    $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    $miApproverId = $poContext->getApprover();
    $miResponsibleId = $moContext->getResponsible();
    $mbUserEqualApprover = ($miUserId == $miApproverId);
    $mbUserEqualResponsible = ($miUserId == $miResponsibleId);
    $mbDeletedResponsible = ($miResponsibleId == 0);
    
    $miNCResponsibleId = $moContext->getNCResponsible();
    $mbUserEqualNCResponsible = ($miUserId == $miNCResponsibleId);
    $mbDeletedNCResponsible = ($miNCResponsibleId == 0);
    
    if($piAction==WKF_ACTION_DELETE){
      $moContext->setContextState(CONTEXT_STATE_DELETED);
      return;
    }
    
    switch($moContext->getContextState()){
      case CONTEXT_STATE_NONE:{ //Se n�o possui estado, � porque acabou de ser criado. 
        if($mbUserEqualApprover){ //Se o usuario que inseriu o contexto for o gestor, aprova automaticamente.
          $moContext->setContextState(CONTEXT_STATE_CI_OPEN);
        }else{ //Se o usu�rio n�o for o gestor, gera tarefa de aprova��o para o gestor e estado � Pendente.
          $this->dispatchTask($this->ciActivity, $moContext->getId(), $miApproverId);
          $moContext->setContextState(CONTEXT_STATE_CI_SENT);
        }
        return;
      }
      case CONTEXT_STATE_CI_SENT:{ //Estado Pendente
        switch($piAction){
          case WKF_ACTION_APPROVE:{ //Quando aprovado, emite alerta para o respons�vel, se este n�o for o mesmo que est� aprovando.
            if(!$mbDeletedResponsible && !$mbUserEqualResponsible) 
              $this->dispatchAlert(WKF_ALERT_APPROVED, $moContext, $psJustification);
            $moContext->setContextState(CONTEXT_STATE_CI_OPEN);
            return;
          }
          case WKF_ACTION_DENY:{ //Ap�s negado, n�o h� como voltar para estado anterior.
            if(!$mbDeletedResponsible && !$mbUserEqualResponsible) 
              $this->dispatchAlert(WKF_ALERT_DENIED, $moContext, $psJustification);
            $moContext->setContextState(CONTEXT_STATE_DENIED);
            return;
          }
        }
        return;
      }
      case CONTEXT_STATE_CI_OPEN:{ //Estado Aberto
        switch($piAction){
          case WKF_ACTION_CI_NC_SEND_TO_RESPONSIBLE:{ //Quando o respons�vel pela NC n�o for o gestor, envia alerta e encaminha a NC
            if(!$mbDeletedNCResponsible && !$mbUserEqualNCResponsible)
              $this->dispatchAlert(WKF_ALERT_DELEGATION, $moContext, $psJustification, $miNCResponsibleId);
            $moContext->setContextState(CONTEXT_STATE_CI_DIRECTED);
            return;
          }
        }
        return;
      }
      case CONTEXT_STATE_CI_DIRECTED:{ //Estado Encaminhado
        switch($piAction){
          case WKF_ACTION_SEND_NC_TO_APPROVE:{ // causa e PA (pelo menos 1), gera tarefa de aprovacao.
            $moNcAp = new CINcActionPlan();
            $moNcAp->createFilter($moContext->getFieldValue('nc_id'),'nc_id');
            $moNcAp->select();
            $mbHasDenied = false;
            $mbHasPendant = false;
            $mbHasAtLeastOne = false;
            while($moNcAp->fetch()){
              $moAp = new CIActionPlan();
              $moAp->fetchById($moNcAp->getFieldValue('ap_id'));
              switch($moAp->getContextState()){
                case CONTEXT_STATE_NONE:
                case CONTEXT_STATE_AP_PENDANT:
                case CONTEXT_STATE_PENDANT:{
                  $mbHasPendant = true;
                  break;
                }
                case CONTEXT_STATE_DENIED:{
                  $mbHasDenied = true;
                }
                default:{
                  $mbHasAtLeastOne = true;
                 break;
                }
              }
              
            }
            if($mbHasAtLeastOne && !$mbHasDenied && !$mbHasPendant){
              /*se os planos de a��o relacionados a n�o conformidade j� estiverem aprovados ou em um estado superior a este
              envia-los para o estado de CI_NC_PENDANT*/
              $moContext->setContextState(CONTEXT_STATE_CI_AP_PENDANT);
              $this->stateForward($poContext,WKF_ACTION_APPROVE);
            }elseif($mbHasAtLeastOne && $mbHasDenied){
              /*se pelo menos 1 AP relacionado a NC estiver negado, deixar a NC neste estado*/
              $moContext->setContextState(CONTEXT_STATE_CI_AP_PENDANT);
              $this->stateForward($poContext,WKF_ALERT_DENIED);
            }else{
              /*caso contr�rio enviar para o estado de CI_AP_PENDANT*/
              $moContext->setContextState(CONTEXT_STATE_CI_AP_PENDANT);
            }
            return;
          }
        }
        return;
      }
      case CONTEXT_STATE_CI_AP_PENDANT:{ //Estado Pendente - Aguardando conclus�o de todos PA antes de poder passar para o estados
                                         //"tratamento de n�o conformidade pendente"
        switch($piAction){
          case WKF_ACTION_APPROVE:{ //Se aprovado, envia alerta para o respons�vel pela NC informando aprova��o
            if(!$mbDeletedNCResponsible && !$mbUserEqualNCResponsible) //Alerta de Aprovado para o Responsavel da NC
              $this->dispatchAlert(WKF_ALERT_APPROVED, $moContext, $psJustification, $miNCResponsibleId);
              $this->dispatchTask($this->ciActivity, $moContext->getId(), $miApproverId);
              $moContext->setContextState(CONTEXT_STATE_CI_NC_PENDANT);
            return;
          }
          case WKF_ACTION_DENY:{ //Pelo menos uma plano de a��o foi reprovado, volta para o estado Encaminhado.
            if(!$mbDeletedNCResponsible && !$mbUserEqualNCResponsible) 
              $this->dispatchAlert(WKF_ALERT_DENIED, $moContext, $psJustification, $miNCResponsibleId);
            $moContext->setContextState(CONTEXT_STATE_CI_DIRECTED);
            return;
          }
        }
        return;
      }
      case CONTEXT_STATE_CI_NC_PENDANT:{
        switch($piAction){
          /*aprova os dados do IPA , aprovador � o gestor de n�o conformidade*/
          case WKF_ACTION_APPROVE:{ //Se aprovado, envia alerta para o respons�vel pela NC informando aprova��o dos dados do IPA
            if(!$mbDeletedNCResponsible && !$mbUserEqualNCResponsible) //Alerta de Aprovado para o Responsavel da NC
              $this->dispatchAlert(WKF_ALERT_NON_CONFORMITY_WAITING_CONCLUSION, $moContext, $psJustification, $miNCResponsibleId);
            $moContext->setContextState(CONTEXT_STATE_CI_WAITING_CONCLUSION);
            
            /*caso os planos de a��o relacionados a n�o conformidade j� estiverem concluidos
            dar um stateForward WKF_ACTION_CI_NC_FINISH para finalizar a n�o conformidade */
            $moNcAp = new CINcActionPlan();
            $moNcAp->createFilter($moContext->getFieldValue('nc_id'),'nc_id');
            $moNcAp->select();
            
            $mbHasApproved = false;
            $mbHasAtLeastOne = false;
            while($moNcAp->fetch()){
              $moAp = new CIActionPlan();
              $moAp->fetchById($moNcAp->getFieldValue('ap_id'));
              switch($moAp->getContextState()){
                case CONTEXT_STATE_AP_WAITING_CONCLUSION:{
                   $mbHasApproved = true;
                   $mbHasAtLeastOne = true;
                  break;
                }
                default:{
                   $mbHasAtLeastOne = true;
                 break;
                }
              }
            }
            if($mbHasAtLeastOne && !$mbHasApproved){
              /*tem pelo menos um AP e todos os AP relacionados est�o pelo menos no estado de "Concluido"*/
              $this->stateForward($poContext,WKF_ACTION_CI_NC_FINISH);
            }
            
            return;
          }
          case WKF_ACTION_DENY:{ //Pelo menos uma plano de a��o foi reprovado, volta para o estado Encaminhado.
            if(!$mbDeletedNCResponsible && !$mbUserEqualNCResponsible) 
              $this->dispatchAlert(WKF_ALERT_DENIED, $moContext, $psJustification, $miNCResponsibleId);
            $moContext->setContextState(CONTEXT_STATE_CI_DIRECTED);
            return;
          }
        }
        return;
      }
      case CONTEXT_STATE_CI_WAITING_CONCLUSION:{
        switch($piAction){
          case WKF_ACTION_CI_NC_FINISH:{ 
            $moContext->setContextState(CONTEXT_STATE_CI_FINISHED);
            
            /*caso tem pelo menos um AP e todos os AP est�o medidos, fecha a n�o conformidade*/
            $moNcAp = new CINcActionPlan();
            $moNcAp->createFilter($moContext->getFieldValue('nc_id'),'nc_id');
            $moNcAp->select();
            
            $mbHasWaitingMessured = false;
            $mbHasAtLeastOne = false;
            while($moNcAp->fetch()){
              $moAp = new CIActionPlan();
              $moAp->fetchById($moNcAp->getFieldValue('ap_id'));
              switch($moAp->getContextState()){
              case CONTEXT_STATE_AP_WAITING_CONCLUSION: /*incluido este estado porque a troca de estado pode ver do AP e no workflow do AP a troca de estado � ap�s avan�ar o estado das NCs*/
              case CONTEXT_STATE_AP_WAITING_MEASUREMENT:{
                   $mbHasWaitingMessured = true;
                   $mbHasAtLeastOne = true;
                  break;
                }
                default:{
                   $mbHasAtLeastOne = true;
                 break;
                }
              }
            }
            if($mbHasAtLeastOne && !$mbHasWaitingMessured){
              /*tem pelo menos um AP e todos os AP relacionados est�o no estado de "Medido"*/
              $this->stateForward($poContext,WKF_ACTION_APPROVE);
            }
            return;
          }
        }
        return;
      }
      case CONTEXT_STATE_CI_FINISHED:{
        switch($piAction){
          case WKF_ACTION_APPROVE:{ 
            $moContext->setContextState(CONTEXT_STATE_CI_CLOSED);
            return;
          }
        }
        return;
      }
      case CONTEXT_STATE_DELETED:{
        if($piAction == WKF_ACTION_UNDELETE){
          if($mbUserEqualApprover){ //Se o usuario que inseriu o contexto for o gestor, aprova automaticamente.
            $moContext->setContextState(CONTEXT_STATE_CI_OPEN);
          }else{ //Se o usu�rio n�o for o gestor, gera tarefa de aprova��o para o gestor e estado � Pendente.
            $this->dispatchTask($this->ciActivity, $moContext->getId(), $miApproverId);
            $moContext->setContextState(CONTEXT_STATE_CI_SENT);
          }
          return;
        }
        return;
      }
    }
    trigger_error("The action '$piAction' at state '{$moContext->getContextState()}' is not implemented.",E_USER_ERROR);
  }
}
?>