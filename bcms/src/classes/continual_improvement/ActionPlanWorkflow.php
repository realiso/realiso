<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

include_once $handlers_ref . "QueryWKFCIACSendNCToNCPendant.php";
include_once $handlers_ref . "QueryWKFCIACSendNCToFinished.php";
include_once $handlers_ref . "QueryWKFCIACSendNCToClosed.php";

/**
 * Classe NCWorkflow.
 *
 * <p>Classe que manipula os estados do workflow das N�o Conformidades.
 * O workflow � respons�vel por disparar as devidas tarefas.
 * � ele que sabe quais e quantas tarefas ser�o disparadas, de acordo
 * com o estado do contexto utilizado.</p>
 * @package ISMS
 * @subpackage classes
 */
class ActionPlanWorkflow extends Workflow {

 /**
  * Construtor.
  *
  * <p>Construtor da classe Workflow.</p>
  * @access public
  */
  public function __construct(){}

 /**
  * Avan�a o estado do contexto no workflow.
  *
  * <p>M�todo para avan�ar o estado do contexto no workflow.</p>
  * @access public
  *
  * @param IWorkflow $poDocument Objeto do documento
  * @param integer $piAction Tipo da a��o
  * @param string $psJustification Justificativa
  */
  public function stateForward(IWorkflow $poContext, $piAction, $psJustification = ''){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

    $moContext = new CIActionPlan();
    $moContext->fetchById($poContext->getId());
    $miUserId = ISMSLib::getCurrentUserId();
    $miCreatorId = $poContext->getCreator();
    $miResponsibleId = $moContext->getResponsible();
    $mbUserEqualCreator = ($miUserId == $miCreatorId);
    $mbUserEqualResponsible = ($miUserId == $miResponsibleId);

    if($piAction==WKF_ACTION_DELETE){
      $moContext->setContextState(CONTEXT_STATE_DELETED);
      return;
    }

  switch($moContext->getContextState()){
      case CONTEXT_STATE_NONE:{ //Se n�o possui estado, � porque acabou de ser criado.
        if($mbUserEqualResponsible){ //Se o usuario que inseriu o contexto for o respons�vel, aprova automaticamente passando para o estado de aguardando solu��o.
          $moContext->setContextState(CONTEXT_STATE_AP_WAITING_CONCLUSION);
          $this->stateForward($poContext,WKF_ACTION_AP_SEND_TO_WAINTING_CONCLUSION);
        }else{ //Se o usu�rio n�o for o RESPONS�VEL, gera tarefa de aprova��o para o respos�vel e estado � Pendente.
          $moContext->setContextState(CONTEXT_STATE_AP_PENDANT);
          $this->dispatchTask(ACT_AP_APPROVAL, $moContext->getId(), $miResponsibleId);
        }
        return;
      }
      case CONTEXT_STATE_AP_PENDANT:{ //Estado Pendente
        switch($piAction){
          case WKF_ACTION_APPROVE:
          case WKF_ACTION_AP_SEND_TO_WAINTING_CONCLUSION:{ //envia alerta para o criador do plano de a��o se respos�vel != criador
            if($mbUserEqualResponsible){
              if($miCreatorId != $miResponsibleId){
                $this->dispatchAlert(WKF_ALERT_APPROVED, $moContext, $psJustification,$miCreatorId);
              }
              /*avan�a as n�o conformidades relacionadas ao plano de a��o que estiverem no estado CI_AP_PENDANT para CI_NC_PENDANT*/
              $moNcQuery = new QueryWKFCIACSendNCToNCPendant(FWDWebLib::getConnection());
              $moNcQuery->setAcId($moContext->getFieldValue('ap_id'));
              $moNcQuery->makeQuery();
              $moNcQuery->executeQuery();
              while($moNcQuery->fetch()){
                //para cada NC, avan�a oworkflow para CI_NC_PENDANT
                $moNc = new CINonConformity();
                $moNc->fetchById($moNcQuery->getFieldValue('nc_id'));
                $moNc->stateForward(WKF_ACTION_APPROVE);
              }
              /*
                verifica se tem task pendente de aprova��o, se tiver, aprova a task.
              */
              $moTask = new WKFTask();
              $moTask->setFieldValue('task_context_id',$moContext->getId());
              $moTask->createFilter($moContext->getId(),'task_context_id');
              $moTask->createFilter(1,'task_is_visible');
              
              $moTask->select();
              if($moTask->fetch()){
                $moTaskUp = new WKFTask();
                $moTaskUp->setFieldValue('task_date_accomplished', ISMSLib::ISMSTime());
                $moTaskUp->setFieldValue('task_is_visible', 0);
                $moTaskUp->update($moTask->getFieldValue('task_id'));
              }
              $moContext->setContextState(CONTEXT_STATE_AP_WAITING_CONCLUSION);
              $this->stateForward($poContext,WKF_ACTION_AP_SEND_TO_WAINTING_CONCLUSION);
            }
            return;
          }
          case WKF_ACTION_DENY:{ //Ap�s negado, n�o h� como voltar para estado anterior.
            if( $mbUserEqualResponsible ){
              if($miCreatorId != $miResponsibleId){
                $this->dispatchAlert(WKF_ALERT_DENIED, $moContext, $psJustification,$miCreatorId);
              }

              /*passa as n�o conformidades relacionadas ao PA para o estado DIRECTED='encaminhado'*/
              $moNcAp = new CINcActionPlan();
              $moNcAp->createFilter($moContext->getFieldValue('ap_id'),'ap_id');
              $moNcAp->select();
              while($moNcAp->fetch()){
                $moNc = new CINonConformity();
                $moNc->fetchById($moNcAp->getFieldValue('nc_id'));
                $moNc->stateForward(WKF_ACTION_DENY);
              }

              $moContext->setContextState(CONTEXT_STATE_DENIED);
            }
            return;
          }
        }
        return;
      }
      case CONTEXT_STATE_AP_WAITING_CONCLUSION:{ //Estado Aguardando Conclus�o
        if(($piAction == WKF_ACTION_AP_SEND_TO_WAINTING_CONCLUSION)||($piAction == WKF_ACTION_AP_SEND_TO_WAINTING_MEASUREMENT)){
          if($mbUserEqualResponsible){
            if($moContext->getFieldValue('ap_dateconclusion')){
              /*avan�a as n�o conformidades relacionadas ao plano de a��o que estiverem no estado CI_WAITING_CONCLUSION para CONTEXT_STATE_CI_FINISHED*/
              $moNcQuery = new QueryWKFCIACSendNCToFinished(FWDWebLib::getConnection());
              $moNcQuery->setAcId($moContext->getFieldValue('ap_id'));
              $moNcQuery->makeQuery();
              $moNcQuery->executeQuery();
              while($moNcQuery->fetch()){
                //para cada NC, avan�a o workflow para CONTEXT_STATE_CI_FINISHED
                $moNc = new CINonConformity();
                $moNc->fetchById($moNcQuery->getFieldValue('nc_id'));
                $moNc->stateForward(WKF_ACTION_CI_NC_FINISH);
              }

              $moContext->setContextState(CONTEXT_STATE_AP_WAITING_MEASUREMENT);
              if(!$moContext->getFieldValue('ap_dateefficiencymeasured')){
                /*gera task para definir a data da medi��o de efici�ncia do plano de a��o*/
                $this->dispatchTask(ACT_ACTION_PLAN_FINISH_CONFIRM, $moContext->getId(), $miResponsibleId);
              }

              $this->stateForward($poContext,WKF_ACTION_AP_SEND_TO_WAINTING_MEASUREMENT);
            }
          }
        }
        return;
      }
      case CONTEXT_STATE_AP_WAITING_MEASUREMENT:{ //esperando medi��o
        if($mbUserEqualResponsible){
          if($piAction == WKF_ACTION_AP_SEND_TO_WAINTING_MEASUREMENT){
            if($moContext->getFieldValue('ap_dateefficiencymeasured')){
               /*se j� foi medido*/
               /*enviar alerta para os respons�veis pelas n�o conformidades relacionadas ao plano de a��o*/
               $moNCAp = new CINcActionPlan();
               $moNCAp->createFilter($moContext->getFieldValue('ap_id'),'ap_id');
               $moNCAp->select();
               while($moNCAp->fetch()){
                $moNC = new CINonConformity();
                $moNC->fetchById($moNCAp->getFieldValue('nc_id'));
                $this->dispatchAlert(WKF_ALERT_AP_CONCLUSION, $moContext, $psJustification,$moNC->getFieldValue('nc_responsible_id'));
               }

               /*avan�a as NCs, relacionadas ao plano de a��o, que estiverem no estado de CONTEXT_STATE_CI_FINISHED para CONTEXT_STATE_CI_CLOSED
               e n�o possuirem outro PA que esteja no estado de CONTEXT_STATE_AP_WAITING_MEASUREMENT ou algum estado anterior
               */
              $moNcQuery = new QueryWKFCIACSendNCToClosed(FWDWebLib::getConnection());
              $moNcQuery->setAcId($moContext->getFieldValue('ap_id'));
              $moNcQuery->makeQuery();
              $moNcQuery->executeQuery();
              while($moNcQuery->fetch()){
                //para cada NC, avan�a o workflow para CONTEXT_STATE_CI_CLOSED
                $moNc = new CINonConformity();
                $moNc->fetchById($moNcQuery->getFieldValue('nc_id'));
                $moNc->stateForward(WKF_ACTION_APPROVE);
              }

               /*seta o estado do plano de a��o para o estado final dele, o de medido*/
               $moContext->setContextState(CONTEXT_STATE_AP_MEASURED);
            }
          }
        }
        return;
      }
      case CONTEXT_STATE_AP_MEASURED:{
        /*fim do workflow do plano de a��o, este � o estado final do plano de a��o*/
        return;
      }
      case CONTEXT_STATE_DELETED:{
        if($piAction == WKF_ACTION_UNDELETE){
          if($mbUserEqualResponsible){ //Se o usuario for o respons�vel pelo plano de a��o, aprova automaticamente.
            $moContext->setContextState(CONTEXT_STATE_AP_WAITING_CONCLUSION);
            $this->stateForward($poContext,WKF_ACTION_AP_SEND_TO_WAINTING_CONCLUSION);
          }else{ //Se o usu�rio n�o for o respos�vel, gera tarefa de aprova��o para o respons�vel e o estado � Pendente.
            $this->dispatchTask(ACT_AP_APPROVAL, $moContext->getId(), $miResponsibleId);
            $moContext->setContextState(CONTEXT_STATE_AP_PENDANT);
          }
          return;
        }
        return;
      }
    }
    trigger_error("The action '$piAction' at state '{$moContext->getContextState()}' is not implemented.",E_USER_ERROR);
  }
}
?>