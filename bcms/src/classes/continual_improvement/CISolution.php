<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */


/**
 * Classe CISolution
 *
 * <p>Classe que representa a tabela de solu��es.</p>
 * @package ISMS
 * @subpackage classes
 */
class CISolution extends ISMSContext {
 /**
  * Construtor.
  *
  * <p>Construtor da classe CISolution.</p>
  * @access public
  */
  public function __construct() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

    parent::__construct("ci_solution");
    $this->csAliasId = "solution_id";
    $this->csDependenceAliasId = "category_id";
    $this->ciContextType = CONTEXT_CI_SOLUTION;

    $this->coDataset->addFWDDBField(new FWDDBField("fkContext",   "solution_id",        DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("fkCategory",  "category_id",        DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("tProblem",    "solution_problem",   DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField("tSolution",   "solution_solution",  DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField("tKeywords",   "solution_keywords",  DB_STRING));

	$this->caSearchableFields = array('solution_problem','solution_solution','solution_keywords');
  }


  /**
  * Retorna o label do contexto.
  *
  * <p>M�todo para retornar o label do contexto.</p>
  * @access public
  * @return string Label do contexto
  */
  public function getLabel(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

    return FWDLanguage::getPHPStringValue('mx_solution', "Solu��o");
  }

  /**
  * Retorna o nome do contexto.
  *
  * <p>M�todo para retornar o nome do contexto.</p>
  * @access public
  * @return string Nome do contexto
  */
  public function getName(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

    return $this->getFieldValue('solution_problem');
  }

  public function getResponsible(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

    $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
    if(!in_array('M.L.6.5',$maACLs)){
      ISMSLib::getCurrentUserId();
    }else{
      $moConfig = new ISMSConfig();
	  return $moConfig->getConfig(USER_LIBRARIAN);
    }
  }

  /**
   * Verifica se o usu�rio pode deletar a solu��o.
   *
   * <p>M�todo para verificar se o usu�rio pode deletar a solu��o.</p>
   * @access public
   * @param integer piSolutionId Id da solu��o
   * @return boolean True|False
   */
  public function userCanDelete($piSolutionId){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

    $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
    if(!in_array('M.L.6.6',$maACLs)){
      return true;
    }else{
      return false;
    }
  }

   /**
   * Verifica se o usu�rio pode editar a solu��o.
   *
   * <p>M�todo para verificar se o usu�rio pode editar a solu��o.</p>
   * @access public
   * @param integer piSolutionId Id do solu��o
   * @return boolean True|False
   */
  public function userCanEdit($piSolutionId,$piUserResponsible = 0){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

    $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
    if(!in_array('M.L.6.5',$maACLs)){
      return true;
    }else{
      return false;
    }
  }

  /**
   * Verifica se o usu�rio pode inserir uma solu��o.
   *
   * <p>M�todo para verificar se o usu�rio pode inserir uma solu��o.</p>
   * @access public
   * @return boolean True|False
   */
  public function userCanInsert(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

    $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
    if(!in_array('M.L.6.6',$maACLs)){
      return true;
    }else{
      return false;
    }
  }

/**
  * Retorna o caminho do contexto.
  *
  * <p>M�todo para retornar o caminho do contexto.</p>
  * @access public
  * @return string Caminho do contexto
  */
  public function getSystemPath() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $msRMPath = "<a href='javascript:isms_redirect_to_mode(" . LIBRARIES_MODE . ",0,0)'>".FWDLanguage::getPHPStringValue('st_libraries_module_name','Bibliotecas')."</a>";
    $msContextPath = ISMSLib::getIconCode('icon-ci_solution.gif',-2,-4). "<a href='javascript:isms_redirect_to_mode(" . LIBRARIES_MODE . "," . CONTEXT_CI_SOLUTION . "," . $this->getFieldValue('category_id') . ")'>" . $this->getName() . "</a>";
    return $msRMPath ."&nbsp;".ISMSLib::getIconCode('icon-arrow-right.gif',-2,0).'&nbsp;' . $msContextPath;
  }

 /**
  * Retorna o c�digo HTML do icone do contexto.
  *
  * <p>M�todo para retornar o c�digo HTML do contexto</p>
  * @access public
  * @param integer $piId Id do contexto
  * @param integer $pbIsFetched id do contexto
  * @return string contendo o c�digo HTML do icone do contexto
  */
  public function getIconCode($piId, $pbIsFetched = false){
    return ISMSLib::getIconCode('icon-ci_solution.gif', -2, 7);
  }

 /**
  * Retorna o caminho para abrir a popup de edi��o desse contexto.
  *
  * <p>M�todo para retornar o caminho para abrir a popup de edi��o desse contexto.</p>
  * @param integer $piContextId id do contexto
  * @access public
  * @return string caminho para abrir a popup de edi��o desse contexto
  */
  public function getVisualizeEditEvent($piContextId,$psUniqId){
    $soSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    $msPackagesIncidentPath = ($soSession->getMode()==MANAGER_MODE) ? "packages/libraries/" : "../../packages/libraries/";
    $msPopupId = "popup_incident_solution_edit";

    return "isms_open_popup('{$msPopupId}','{$msPackagesIncidentPath}{$msPopupId}.php?solution=$piContextId','','true',312,400);"
           ."soPopUpManager.getPopUpById('{$msPopupId}').setCloseEvent( function close_visualize_$psUniqId() {soPopUpManager.closePopUp('popup_visualize_$psUniqId');} );";
  }

  /**
  * Retorna o �cone do contexto.
  *
  * <p>M�todo para retornar o �cone do contexto.</p>
  * @access public
  * @param integer $piContextId Id do contexto
  * @return string Nome do �cone
  */
  public function getIcon($piContextId = 0){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return 'icon-ci_solution.gif';
  }
}
?>