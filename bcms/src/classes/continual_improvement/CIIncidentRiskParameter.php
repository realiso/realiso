<?php
/**
 * Classe CIIncidentRiskParameter
 *
 * <p>Classe que representa a tabela de rela��o entre incidente e risco e os parametros de cada risco(????).</p>
 * @package ISMS
 * @subpackage classes
 */
class CIIncidentRiskParameter extends ISMSContext {
 /**
  * Construtor.
  * 
  * <p>Construtor da classe CIIncidentRisk.</p>
  * @access public 
  */
  public function __construct() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    parent::__construct("ci_incident_risk_parameter");
    $this->csAliasId = "incident_risk_parameter_id";
    $this->ciContextType = CONTEXT_CI_INCIDENT_RISK;
    $this->coDataset->addFWDDBField(new FWDDBField("fkContext"             ,"incident_risk_parameter_id"     ,DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("fkIncident"            ,"incident_id"           ,DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("fkParameterName"                ,"parameter_name_id"     ,DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("fkParameterValue"               ,"parameter_value_id"    ,DB_NUMBER));
  }

  public function getLabel(){ return ''; }
  public function getName(){ return ''; }
  protected function userCanDelete($piContextId){ return true; }
  protected function userCanEdit($piContextId,$piUserResponsible = 0){ return true; }
  protected function userCanInsert(){ return true; }

}
?>