<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
 
/**
 * Classe CINonConformityProcess
 *
 * <p>Classe que representa a tabela de rela��o entre processo e n�o-conformidade.</p>
 * @package ISMS
 * @subpackage classes
 */
class CINonConformityProcess extends ISMSTable {
 /**
  * Construtor.
  * 
  * <p>Construtor da classe CINonConformityProcess.</p>
  * @access public 
  */
  public function __construct() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    parent::__construct("ci_nc_process");
    
        
    $this->coDataset->addFWDDBField(new FWDDBField("fkNC",       "nc_id",       DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("fkProcess",  "process_id",  DB_NUMBER));
  }
  
}
?>