<?php
/**
 * AMS - AXUR MONITOR SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe AMSFolders.
 *
 * <p>Classe que representa a tabela de folders.</p>
 * @package AMS
 * @subpackage classes
 */
class ISMSGridUserPreferences extends FWDGridParameter { 

 /**
  * Construtor da classe.
  *
  * <p>Construtor da classe AMSGridUserPreferences.</p>
  * @access public
  */
  public function __construct(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
  }	

 /**
  * Carrega no objeto de preferencias a preferencia do usu�rio para a grid passada por par�metro.
  *
  * <p>Carrega no objeto de preferencias a preferencia do usu�rio para a grid passada por par�metro.
  * a fonte dos dados neste caso � o banco de dados</p>
  * @access protected
   * @param string $psObjId Id da grid
  */
  public function getPreference($psGridId){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $moUserPreferences = new ISMSUserPreferences();
    return $moUserPreferences->getPreference($psGridId.'_preferences_code');
  }

 /**
  * Salva os dados armazenados no objeto de preferencias do usu�rio.
  *
  * <p>Salva os dados armazenados no objeto de preferencias do usu�rio no destino definido no sistema.
  * O destino neste caso � o banco de dados</p>
  * @access protected
   * @param string $psGridId Id da grid
   * @param string $psData string serializada contendo as preferencias do usu�rio da grid
   */
  public function updatePreference($psGridId,$psData){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $moUserPreferences = new ISMSUserPreferences();
    $moUserPreferences->setPreference($psGridId.'_preferences_code',$psData);
  }

 /**
  * Deleta as preferencias do usu�rio para o objeto definido em $psGridId.
  *
  * <p>deleta as preferencias do usu�rio para o objeto definido em $psGridId do sistema de armazenamento definido no sistema.
  * neste caso o meio de armazenamento � o banco de dados</p>
   * @param string $psGridId Id da grid
  * @access protected
  */
  public function deletePreference($psGridId){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $moUserPreferences = new ISMSUserPreferences();
    $moUserPreferences->deletePreference($psGridId.'_preferences_code');
  }
}
?>