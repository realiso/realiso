<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

include_once $handlers_ref . 'QueryWKFScheduleTasks.php';
include_once $handlers_ref . 'QueryWKFScheduleAlerts.php';

/**
 * Classe WKFSchedule
 *
 * <p>Classe que representa um agendamento</p>
 * @package ISMS
 * @subpackage classes
 */

class WKFSchedule extends ISMSTable {

	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe WKFSchedule.</p>
	 * @access public
	 */
	public function __construct(){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		parent::__construct('wkf_schedule');

		$this->csAliasId = 'schedule_id';

		$this->coDataset->addFWDDBField(new FWDDBField('pkSchedule'     ,'schedule_id'             ,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField('dStart'         ,'schedule_start'          ,DB_DATETIME));
		$this->coDataset->addFWDDBField(new FWDDBField('dEnd'           ,'schedule_end'            ,DB_DATETIME));
		$this->coDataset->addFWDDBField(new FWDDBField('nType'          ,'schedule_type'           ,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField('nPeriodicity'   ,'schedule_periodicity'    ,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField('nBitmap'        ,'schedule_bitmap'         ,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField('nWeek'          ,'schedule_week'           ,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField('nDay'           ,'schedule_day'            ,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField('dNextOccurrence','schedule_next_occurrence',DB_DATETIME));
		$this->coDataset->addFWDDBField(new FWDDBField('nDaysToFinish'  ,'schedule_days_to_finish' ,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField('dDateLimit'     ,'schedule_date_limit'     ,DB_DATETIME));
	}

	public function cloneEntity($seasonality){

		$this->copyAttribute('schedule_start', 				$seasonality);
		$this->copyAttribute('schedule_end', 				$seasonality);
		$this->copyAttribute('schedule_type', 				$seasonality);
		$this->copyAttribute('schedule_periodicity', 		$seasonality);
		$this->copyAttribute('schedule_week', 				$seasonality);
		$this->copyAttribute('schedule_day', 				$seasonality);
		$this->copyAttribute('schedule_next_occurrence', 	$seasonality);
		$this->copyAttribute('schedule_days_to_finish', 	$seasonality);
		$this->copyAttribute('schedule_date_limit', 		$seasonality);
	}

	public function copyAttribute($attr, $seasonality){
		$value = $seasonality->getFieldValue($attr);
		if(!$value && $this->getFieldByAlias($attr)->getType() == DB_NUMBER){
			$this->setFieldValue($attr, 'null');
		}else{
			$this->setFieldValue($attr, $value);
		}
	}

	/**
	 * Insere ou atualiza um agendamento.
	 *
	 * <p>M�todo para inserir ou editar um agendamento. Se recebe um id n�o vazio e
	 * diferente de zero, tenta dar update, sen�o insere. Sempre retorna o id.</p>
	 * @access public
	 * @param integer $piId Identificador do agendamento
	 * @return integer Id do agendamento
	 */
	public function insertOrUpdate($piId){
		if($piId){
			$this->update($piId);
			return $piId;
		}else{
			return $this->insert(true);
		}
	}

	public static function checkTasks(){
		$miNow = FWDWebLib::getTime();
		$maSchedules = QueryWKFScheduleTasks::getSchedules();

		$moConfig = new ISMSConfig();
		$miCreatorId = $moConfig->getConfig(USER_CHAIRMAN);

		foreach($maSchedules as $miScheduleId){
			$moSchedule = new WKFSchedule();
			$moSchedule->fetchById($miScheduleId);
			$miNextOccurrence = FWDWebLib::dateToTimestamp($moSchedule->getFieldValue('schedule_next_occurrence'));

			// Cria um array com os dados dos TaskSchedules do agendamento
			$moTaskSchedule = new WKFTaskSchedule();
			$moTaskSchedule->createFilter($miScheduleId,'task_schedule_schedule');
			$moTaskSchedule->select();
			$maTaskSchedules = array();
			while($moTaskSchedule->fetch()){
				$miContextId = $moTaskSchedule->getFieldValue('task_schedule_context');
				$moContext = new ISMSContextObject();
				$moContext = $moContext->getContextObjectByContextId($miContextId);
				$moContext->fetchById($miContextId);
				$maTaskSchedules[] = array(
          'context'     => $miContextId,
          'activity'    => $moTaskSchedule->getFieldValue('task_schedule_activity'),
          'responsible' => $moContext->getResponsible()
				);
			}

			// Avan�a a data da pr�xima ocorr�ncia at� que ela esteja no futuro
			while($miNextOccurrence <= $miNow){
				$miLatestTaskDate = $miNextOccurrence;
				// Gera uma tarefa para cada TaskSchedule
				foreach($maTaskSchedules as $maTaskSchedule){
					$moTask = new WKFTask();
					$moTask->setFieldValue('task_context_id'  ,$maTaskSchedule['context']);
					$moTask->setFieldValue('task_activity'    ,$maTaskSchedule['activity']);
					$moTask->setFieldValue('task_receiver_id' ,$maTaskSchedule['responsible']);
					$moTask->setFieldValue('task_creator_id'  ,$miCreatorId);
					$moTask->setFieldValue('task_is_visible'  ,true);
					$moTask->setFieldValue('task_date_created',$miNextOccurrence);
					$moTask->insert();
				}
				$miNextOccurrence = $moSchedule->getNextOccurrence($miNextOccurrence);
			}

			// Atualiza as datas do agendamento
			$miDateLimit = $miLatestTaskDate + 86400 * $moSchedule->getFieldValue('schedule_days_to_finish');
			$moSchedule->getFieldByAlias('schedule_id')->setUpdatable(false);
			$moSchedule->setFieldValue('schedule_date_limit',$miDateLimit);
			$moSchedule->update($miScheduleId);
		}
	}

	public static function checkAlerts(){
		$maTaskSchedules = QueryWKFScheduleAlerts::getTaskSchedules();

		$moConfig = new ISMSConfig();
		$miCreatorId = $moConfig->getConfig(USER_CHAIRMAN);

		foreach($maTaskSchedules as $maTaskSchedule){
			$miContextId = $maTaskSchedule['context_id'];
			$moContext = new ISMSContextObject();
			$moContext = $moContext->getContextObjectByContextId($miContextId);
			$moContext->fetchById($miContextId);
			$miReceiverId = $moContext->getResponsible();

			$moAlert = new WKFAlert();
			$moAlert->setFieldValue('alert_receiver_id',$miReceiverId);
			$moAlert->setFieldValue('alert_date',ISMSLib::ISMSTime());
			$moAlert->setFieldValue('alert_type',$maTaskSchedule['alert_type']);
			$moAlert->setFieldValue('alert_justification',' ');
			$moAlert->setFieldValue('alert_creator_id',$miCreatorId);
			$moAlert->setFieldValue('alert_context_id',$maTaskSchedule['context_id']);
			$moAlert->insert();

			$moTaskSchedule = new WKFTaskSchedule();
			$moTaskSchedule->createFilter($maTaskSchedule['context_id'],'task_schedule_context');
			$moTaskSchedule->createFilter($maTaskSchedule['activity']  ,'task_schedule_activity');
			$moTaskSchedule->setFieldValue('task_schedule_alert_sent',1);
			$moTaskSchedule->update();
		}

	}

	public static function checkSchedules(){
		WKFSchedule::checkTasks();
		WKFSchedule::checkAlerts();
	}

	/**
	 * Dado um bitmap, retorna um array com os bits que est�o ligados no bitmap.
	 *
	 * <p>Dado um bitmap, retorna um array com os bits que est�o ligados no bitmap,
	 * 1 para o bit menos significativo 2 para o seguinte e assim por diante.</p>
	 * @access public
	 * @param integer $piBitmap Bitmap
	 * @return array Lista dos bits que est�o ligados no bitmap
	 */
	public static function bitmapToArray($piBitmap){
		$miBitmap = $piBitmap;
		$maOnes = array();
		$miMask = 1;
		$miBit = 1;
		while($miBitmap){
			if($miBitmap & $miMask){
				$miBitmap-= $miMask;
				$maOnes[] = $miBit;
			}
			$miBit++;
			$miMask<<= 1;
		}
		return $maOnes;
	}

	/**
	 * Calcula o dia da pr�xima ocorr�ncia do agendamento.
	 *
	 * <p>Calcula o dia da pr�xima ocorr�ncia do agendamento, considerando que a
	 * inst�ncia esteja fetchada. N�o considera as datas de in�cio e fim.</p>
	 * @access public
	 * @param integer $piLastOccurrence Timestamp da �ltima ocorr�ncia
	 * @return integer Timestamp da pr�xima ocorr�ncia
	 */
	public function getNextOccurrence($piLastOccurrence){
		$miScheduleType = $this->getFieldValue('schedule_type');
		if(!$miScheduleType){
			trigger_error("The instance must be fetched or the fields must be manually filled.",E_USER_ERROR);
		}
		switch($miScheduleType){
			// Agendamento por dias
			case SCHEDULE_BYDAY:{
				$miPeriodicity = $this->getFieldValue('schedule_periodicity');
				return strtotime(date('Y-m-d',$piLastOccurrence)." + $miPeriodicity days");
			}

			// Agendamento por semana
			case SCHEDULE_BYWEEK:{
				$miDayAfter = strtotime(date('Y-m-d',$piLastOccurrence)." + 1 day");
				$miWeekDayAfter = idate('w',$miDayAfter);
				$maDays = self::bitmapToArray($this->getFieldValue('schedule_bitmap'));
				$miPeriodicity = $this->getFieldValue('schedule_periodicity');

				$miDay = 0;
				if(!$miWeekDayAfter) $miWeekDayAfter = 7;

				for($i=$miWeekDayAfter;$i<7;$i++){
					if(in_array($i+1,$maDays)){
						$miDay = idate('d',$miDayAfter) + $i - $miWeekDayAfter;
						break;
					}
				}

				if(!$miDay){
					$miDay = idate('d',$miDayAfter) + $miPeriodicity * 7 - $miWeekDayAfter + ($maDays[0] - 1);
				}

				return mktime(0, 0, 0, idate('m',$miDayAfter), $miDay, idate('Y',$miDayAfter));
			}

			// Agendamento por m�s
			case SCHEDULE_BYMONTH:{
				$maLastOccurrenceDate = getDate($piLastOccurrence);
				$miPeriodicity = $this->getFieldValue('schedule_periodicity');
				// Acha o primeiro dia do m�s da pr�xima ocorr�ncia
				if($miPeriodicity){
					$miNextOccurrence = mktime(0, 0, 0, idate('m', $piLastOccurrence) + $miPeriodicity, 1, idate('Y',$piLastOccurrence));
				}else{
					$miMonth = idate('m',$piLastOccurrence);
					$maMonths = self::bitmapToArray($this->getFieldValue('schedule_bitmap'));
					$miNextOccurrence = 0;
					for($i=0;$i<count($maMonths);$i++){
						if($maMonths[$i]>$miMonth){
							$miNextOccurrence = mktime(0, 0, 0, $maMonths[$i], 1, idate('Y',$piLastOccurrence));
							break;
						}
					}
					if(!$miNextOccurrence){
						$miNextOccurrence = mktime(0, 0, 0, $maMonths[0], 1, idate('Y',$piLastOccurrence) + 1);
					}
				}
				// Agora, descobre o dia
				$miWeek = $this->getFieldValue('schedule_week');
				$miDay = $this->getFieldValue('schedule_day');
				$miDaysInMonth = idate('t',$miNextOccurrence);
				if($miWeek){
					// se for por dia da semana
					$miFirstWDay = idate('w',$miNextOccurrence);
					if($miFirstWDay < $miDay){
						$miDay = $miDay - $miFirstWDay;
					}else{
						$miDay = $miDay - $miFirstWDay + 7;
					}
					if($miWeek<=4){
						$miDay+= ($miWeek - 1) * 7;
					}elseif($miWeek<=8){ // come�ando do fim (5 = �ltimo, 6 = pen�ltimo, etc)
						$miDay+= 28;
						if($miDay>$miDaysInMonth){
							$miDay-= 7;
						}
						$miDay-= ($miWeek - 5) * 7;
					}else{
						trigger_error("Week out of range '{$miWeek}'.",E_USER_ERROR);
					}
				}else{
					// se for por dia do m�s
					if($miDay>$miDaysInMonth){
						if($miDay<32){
							$miDay = $miDaysInMonth;
						}elseif($miDay<63){ // come�ando do fim (32 = �ltimo, 33 = pen�ltimo, etc)
							$miDay = $miDaysInMonth - $miDay + 32;
						}else{
							trigger_error("Day out of range '{$miDay}'.",E_USER_ERROR);
						}
					}
				}

				return mktime(0, 0, 0, idate('m',$miNextOccurrence), $miDay, idate('Y',$miNextOccurrence));
			}
			default:{
				trigger_error("Invalid schedule type '{$this->getFieldValue('schedule_type')}'.",E_USER_ERROR);
			}
		}
	}

	/**
	 * Insere um registro aa tabela.
	 *
	 * <p>M�todo para inserir um registro na tabela.</p>
	 * @param boolean $pbGetId Verdadeiro para retornar o id do registro inserido
	 * @return integer Id do �ltimo registro inserido ou falso
	 * @access public
	 */
	public function insert($pbGetId = false){
		$miNextOccurrence = $this->getNextOccurrence(max(FWDWebLib::getTime(),FWDWebLib::dateToTimestamp($this->getFieldValue('schedule_start'))));
		$this->setFieldValue('schedule_next_occurrence',$miNextOccurrence);
		return parent::insert($pbGetId);
	}

	/**
	 * Atualiza registros das tabelas do sistema.
	 *
	 * <p>M�todo para atualizar registros da tabela do sistema.</p>
	 * @access public
	 * @param integer $piId Identificador
	 */
	public function update($piId = 0){
		$miNextOccurrence = $this->getNextOccurrence(max(FWDWebLib::getTime(),FWDWebLib::dateToTimestamp($this->getFieldValue('schedule_start'))));
		$this->setFieldValue('schedule_next_occurrence',$miNextOccurrence);
		parent::update($piId);
	}

	public static function getStrings(){
		return array(
      'interval' => FWDLanguage::getPHPStringValue('st_schedule_interval',"Entre %start% e %end%"),
      'endless_interval' => FWDLanguage::getPHPStringValue('st_schedule_endless_interval',"A partir de %start%"),

      'daily' => FWDLanguage::getPHPStringValue('st_schedule_daily',"todos os dias"),
      'each_n_days' => FWDLanguage::getPHPStringValue('st_schedule_each_n_days',"a cada %periodicity% dias"),

      'weekly' => FWDLanguage::getPHPStringValue('st_schedule_weekly',"todos(as) %week_days%"),
      'each_n_weeks' => FWDLanguage::getPHPStringValue('st_schedule_each_n_weeks',"nos(as) %week_days%, a cada %periodicity% semanas"),

      'monthly' => FWDLanguage::getPHPStringValue('st_schedule_monthly',"%day_expression% de cada m�s"),
      'each_n_months' => FWDLanguage::getPHPStringValue('st_schedule_each_n_months',"a cada %periodicity% meses, %day_expression% do m�s"),
      'specified_months' => FWDLanguage::getPHPStringValue('st_schedule_specified_months',"%day_expression% de %months%"),

      'day_by_month' => FWDLanguage::getPHPStringValue('st_schedule_day_by_month',"no dia %day%"),
      'day_by_month_inverse' => FWDLanguage::getPHPStringValue('st_schedule_day_by_month_inverse',"no %day% dia"),
      'day_by_week' => FWDLanguage::getPHPStringValue('st_schedule_day_by_week',"no(a) %week% %week_day%"),

      'and' => FWDLanguage::getPHPStringValue('st_schedule_and',"e"),

      'day_1' => FWDLanguage::getPHPStringValue('mx_sunday',"domingo"),
      'day_2' => FWDLanguage::getPHPStringValue('mx_monday',"segunda-feira"),
      'day_3' => FWDLanguage::getPHPStringValue('mx_tuesday',"ter�a-feira"),
      'day_4' => FWDLanguage::getPHPStringValue('mx_wednesday',"quarta-feira"),
      'day_5' => FWDLanguage::getPHPStringValue('mx_thursday',"quinta-feira"),
      'day_6' => FWDLanguage::getPHPStringValue('mx_friday',"sexta-feira"),
      'day_7' => FWDLanguage::getPHPStringValue('mx_saturday',"s�bado"),

      'day_1_pl' => FWDLanguage::getPHPStringValue('st_schedule_sunday_pl',"domingos"),
      'day_2_pl' => FWDLanguage::getPHPStringValue('st_schedule_monday_pl',"segundas"),
      'day_3_pl' => FWDLanguage::getPHPStringValue('st_schedule_tuesday_pl',"ter�as"),
      'day_4_pl' => FWDLanguage::getPHPStringValue('st_schedule_wednesday_pl',"quartas"),
      'day_5_pl' => FWDLanguage::getPHPStringValue('st_schedule_thursday_pl',"quintas"),
      'day_6_pl' => FWDLanguage::getPHPStringValue('st_schedule_friday_pl',"sextas"),
      'day_7_pl' => FWDLanguage::getPHPStringValue('st_schedule_saturday_pl',"s�bados"),

      'month_1' => FWDLanguage::getPHPStringValue('st_schedule_january',"Janeiro"),
      'month_2' => FWDLanguage::getPHPStringValue('st_schedule_february',"Fevereiro"),
      'month_3' => FWDLanguage::getPHPStringValue('st_schedule_march',"Mar�o"),
      'month_4' => FWDLanguage::getPHPStringValue('st_schedule_april',"Abril"),
      'month_5' => FWDLanguage::getPHPStringValue('st_schedule_may',"Maio"),
      'month_6' => FWDLanguage::getPHPStringValue('st_schedule_june',"Junho"),
      'month_7' => FWDLanguage::getPHPStringValue('st_schedule_july',"Julho"),
      'month_8' => FWDLanguage::getPHPStringValue('st_schedule_august',"Agosto"),
      'month_9' => FWDLanguage::getPHPStringValue('st_schedule_september',"Setembro"),
      'month_10' => FWDLanguage::getPHPStringValue('st_schedule_october',"Outubro"),
      'month_11' => FWDLanguage::getPHPStringValue('st_schedule_november',"Novembro"),
      'month_12' => FWDLanguage::getPHPStringValue('st_schedule_december',"Dezembro"),
		);
	}

	protected static function formatList($maList){
		if(count($maList)==1){
			return $maList[0];
		}else{
			$maStrings = self::getStrings();
			$msLast = array_pop($maList);
			return implode(', ',$maList)." {$maStrings['and']} {$msLast}";
		}
	}

	public function getTextDescription(){
		$maStrings = self::getStrings();

		$miStart = FWDWebLib::dateToTimestamp($this->getFieldValue('schedule_start'));
		$msStart = FWDWebLib::getShortDate($miStart,true);
		$mmEnd = $this->getFieldValue('schedule_end');
		if($mmEnd){
			$msEnd = FWDWebLib::getShortDate(FWDWebLib::dateToTimestamp($mmEnd),true);
			$msString = str_replace(array('%start%','%end%'),array($msStart,$msEnd),$maStrings['interval']);
		}else{
			$msString = str_replace('%start%',$msStart,$maStrings['endless_interval']);
		}
		$msString.= ', ';

		$miScheduleType = $this->getFieldValue('schedule_type');
		$miPeriodicity = $this->getFieldValue('schedule_periodicity');

		switch($miScheduleType){
			// Agendamento por dia
			case SCHEDULE_BYDAY:{
				if($miPeriodicity==1){
					$msString.= $maStrings['daily'];
				}else{
					$msString.= str_replace('%periodicity%',$miPeriodicity,$maStrings['each_n_days']);
				}
				break;
			}

			// Agendamento por semana
			case SCHEDULE_BYWEEK:{
				$maWeekDays = self::bitmapToArray($this->getFieldValue('schedule_bitmap'));
				for($i=0;$i<count($maWeekDays);$i++){
					$maWeekDays[$i] = $maStrings["day_{$maWeekDays[$i]}_pl"];
				}
				if($miPeriodicity==1){
					$msString.= $maStrings['weekly'];
				}else{
					$msString.= str_replace('%periodicity%',$miPeriodicity,$maStrings['each_n_weeks']);
				}
				$msString = str_replace('%week_days%',self::formatList($maWeekDays),$msString);
				break;
			}

			// Agendamento por m�s
			case SCHEDULE_BYMONTH:{
				$miBitmap = $this->getFieldValue('schedule_bitmap');

				if($miPeriodicity==1 || $miBitmap==0xFFF){
					// Todos os meses
					$msString.= $maStrings['monthly'];
				}elseif($miPeriodicity){
					// A cada N meses
					$msString.= str_replace('%periodicity%',$miPeriodicity,$maStrings['each_n_months']);
				}else{
					// Nos meses especificados
					$maMonths = self::bitmapToArray($this->getFieldValue('schedule_bitmap'));
					for($i=0;$i<count($maMonths);$i++){
						$maMonths[$i] = $maStrings["month_{$maMonths[$i]}"];
					}
					$msString.= str_replace('%months%',self::formatList($maMonths),$maStrings['specified_months']);
				}

				// Dia no m�s
				$miWeek = $this->getFieldValue('schedule_week');
				$miDay = $this->getFieldValue('schedule_day');
				if($miWeek){
					// Dia da semana
					switch($miDay){
						case 1: $msDay = FWDLanguage::getPHPStringValue('mx_sunday',"domingo"); break;
						case 2: $msDay = FWDLanguage::getPHPStringValue('mx_monday',"segunda-feira"); break;
						case 3: $msDay = FWDLanguage::getPHPStringValue('mx_tuesday',"ter�a-feira"); break;
						case 4: $msDay = FWDLanguage::getPHPStringValue('mx_wednesday',"quarta-feira"); break;
						case 5: $msDay = FWDLanguage::getPHPStringValue('mx_thursday',"quinta-feira"); break;
						case 6: $msDay = FWDLanguage::getPHPStringValue('mx_friday',"sexta-feira"); break;
						case 7: $msDay = FWDLanguage::getPHPStringValue('mx_saturday',"s�bado"); break;
						default: trigger_error("Invalid week day '$miDay'.",E_USER_ERROR);
					}
					switch($miWeek){
						case 1: $msWeek = FWDLanguage::getPHPStringValue('mx_first_week' ,"primeiro(a)"); break;
						case 2: $msWeek = FWDLanguage::getPHPStringValue('mx_second_wee' ,"segundo(a)"); break;
						case 3: $msWeek = FWDLanguage::getPHPStringValue('mx_third_week' ,"terceiro(a)"); break;
						case 4: $msWeek = FWDLanguage::getPHPStringValue('mx_fourth_week',"quarto(a)"); break;
						case 5: $msWeek = FWDLanguage::getPHPStringValue('mx_last_week'  ,"�ltimo(a)"); break;
						default: trigger_error("Invalid week '$miWeek'.",E_USER_ERROR);
					}
					$msDayExpression = str_replace(array('%week%','%week_day%'),array($msWeek,$msDay),$maStrings['day_by_week']);
				}else{
					// Dia do m�s
					if($miDay>31){
						switch($miDay){
							case 32: $msDay = FWDLanguage::getPHPStringValue('mx_last',"�ltimo"); break;
							case 33: $msDay = FWDLanguage::getPHPStringValue('mx_second_last',"pen�ltimo"); break;
							default: trigger_error("Invalid month day '$miDay'.",E_USER_ERROR);
						}
						$msDayExpression = str_replace('%day%',$msDay,$maStrings['day_by_month_inverse']);
					}else{
						$msDayExpression = str_replace('%day%',$miDay,$maStrings['day_by_month']);
					}
				}
				$msString = str_replace('%day_expression%',$msDayExpression,$msString);
				break;
			}
		}

		return $msString.'.';
	}

}

?>