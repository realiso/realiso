<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSSaaS.
 *
 * <p>Classe que manipula as configurações da instância SaaS.</p>
 * @package ISMS
 * @subpackage classes
 */

define('SYSTEM_IS_ACTIVE'      ,711709);

class ISMSSaaS extends ISMSTable {

	//Constantes de Ids de Configuração da Instância SaaS
	const SAAS_REPORT_EMAIL = 711701;
	const LICENSE_CLIENT_NAME = 711703;
	const LICENSE_MODULES = 711704;
	const LICENSE_MAX_USERS = 711705;
	const LICENSE_EXPIRACY = 711706;
	const LICENSE_MAX_SIMULTANEOUS_USERS = 711707;
	const LICENSE_HASH = 711708;
	const SYSTEM_IS_ACTIVE = 711709;
	const LICENSE_TYPE = 711710;
	const LICENSE_ACTIVATION_DATE = 711712;
	const EMAIL_ADMIN = 711713;
	const CONFIG_PATH = 711714;
	const MAX_UPLOAD_FILE_SIZE = 711715;

	// Constantes de tipo de código de ativação
	const SUBSCRIPTION = 1;
	const USER_PACK = 2;

	/**
	 * Array de configurações
	 * @var array
	 * @access protected
	 */
	protected $caSaaSConfig = array();

	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe ISMSSaas.</p>
	 * @access public
	 */
	public function __construct(){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		parent::__construct("isms_saas");

		$this->csAliasId = 'config_id';
		$this->coDataset->addFWDDBField(new FWDDBField("pkConfig",  "config_id",    DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("sValue",    "config_value", DB_STRING));
	}

	/**
	 * Seta uma determinada configuração.
	 *
	 * <p>Método para setar uma determinada configuração.</p>
	 * @access public
	 * @param integer $piConfigId Id da configuração
	 * @param string $psConfigValue Valor da configuração
	 */
	public function setConfig($piConfigId, $psConfigValue){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		$moISMSSaaS = new ISMSSaaS();
		$moISMSSaaS->createFilter($piConfigId,'config_id');
		$moISMSSaaS->select();
		if ($moISMSSaaS->fetch()) {
			$moISMSSaaS->setFieldValue('config_value',$psConfigValue);
			$moISMSSaaS->update();
		} else {
			$moISMSSaaS->setFieldValue('config_id',$piConfigId);
			$moISMSSaaS->setFieldValue('config_value',$psConfigValue);
			$moISMSSaaS->insert();
		}
	}

	/**
	 * Retorna o valor de uma determinada configuração.
	 *
	 * <p>Método para retornar o valor de uma determinada configuração.</p>
	 * @access public
	 * @param integer $piConfigId Id da configuração
	 * @return string Valor da configuração
	 */
	public function getConfig($piConfigId){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		$moISMSSaaS = new ISMSSaaS();
		$moISMSSaaS->createFilter($piConfigId,'config_id');
		$moISMSSaaS->select();
		if ($moISMSSaaS->fetch()) {
			return $moISMSSaaS->getFieldValue('config_value');
		} else {
			return false;
		}
	}
}
?>