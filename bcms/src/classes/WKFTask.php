<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe WKFTask.
 *
 * <p>Classe que manipula as tarefas do sistema.</p>
 * @package ISMS
 * @subpackage classes
 */
class WKFTask extends ISMSTable {  
  
 /**
  * Construtor.
  * 
  * <p>Construtor da classe WKFTask.</p>
  * @access public 
  */
  public function __construct(){    
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    parent::__construct("wkf_task");
    $this->csAliasId = 'task_id';
    $this->coDataset->addFWDDBField(new FWDDBField("pkTask",           "task_id",               DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("fkContext",        "task_context_id",       DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("fkReceiver",       "task_receiver_id",      DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("fkCreator",        "task_creator_id",       DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("nActivity",        "task_activity",         DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("bVisible",         "task_is_visible",       DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("bEmailSent",       "task_email_sent",       DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("dDateCreated",     "task_date_created",     DB_DATETIME));
    $this->coDataset->addFWDDBField(new FWDDBField("dDateAccomplished","task_date_accomplished",DB_DATETIME));
  }

 /**
  * Insere um registro na tabela.
  * 
  * <p>M�todo para inserir um registro na tabela.</p>
  * @param boolean $pbGetId Verdadeiro para retornar o id do registro inserido
  * @access public 
  */
  public function insert($pbGetId = false){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

    $mbToBeSent = true;
    $mbSendEmail = false;
    if(ISMSLib::getConfigById(GENERAL_EMAIL_ENABLED)){
      $miReceiverId = $this->getFieldValue('task_receiver_id');
      $moEmailPreferences = new ISMSEmailPreferences();
      if($moEmailPreferences->getDigestType($miReceiverId)=='none'){
        $miContextId = $this->getFieldValue('task_context_id');
        $moContextObject = new ISMSContextObject();
        $moContext = $moContextObject->getContextObjectByContextId($miContextId);
        if($moContext instanceof IAssociation){
          $mbIsAssociation = true;
          $moContext->fetchById($miContextId);
          $moFirstContext = $moContextObject->getContextObjectByContextId($moContext->getFirstContextId());
          $moSecondContext = $moContextObject->getContextObjectByContextId($moContext->getSecondContextId());
          if($moEmailPreferences->isMessageTypeSet($moFirstContext->getContextType(),$miReceiverId)){
            $mbSendEmail = true;
          }else{
            $msSender = $moEmailPreferences->isMessageTypeSet($moSecondContext->getContextType(),$miReceiverId);
          }
        }else{
          $mbIsAssociation = false;
          $mbSendEmail = $moEmailPreferences->isMessageTypeSet($moContext->getContextType(),$miReceiverId);
        }
        if($mbSendEmail){
          $msActivity = ISMSActivity::getDescription($this->getFieldValue('task_activity'));
          
          $miSenderId = $this->getFieldValue('task_creator_id');
          $moSender = new ISMSUser();
          $moSender->fetchById($miSenderId);
          $msSender = $moSender->getName();
          
          $msDate = ISMSLib::getISMSDate($this->getFieldValue('task_date_created'),true);
          
          $moReceiver = new ISMSUser();
          $moReceiver->fetchById($miReceiverId);
          $msReceiverEmail = $moReceiver->getFieldValue('user_email');
          
          $moEmailPreferences = new ISMSEmailPreferences();
          $msFormat = $moEmailPreferences->getEmailFormat($miReceiverId);
          
          $msSystemName = ISMSLib::getSystemName();
          
          $moMailer = new ISMSMailer();
          $moMailer->setFormat($msFormat);
          $moMailer->setTo($msReceiverEmail);
          $moMailer->setSubject("[$msSystemName] " . date('d/m/Y') . ' - '.FWDLanguage::getPHPStringValue('wk_task','Tarefa'));
          
          $moMailer->addContent('taskHeader');
          
          if($mbIsAssociation){
            $moFirstContext->fetchById($moContext->getFirstContextId());
            $msContextLabel1 = $moFirstContext->getLabel();
            $msContextName1 = $moFirstContext->getName();
            $msContextDescription1 = $moFirstContext->getDescription();
            
            $moSecondContext->fetchById($moContext->getSecondContextId());
            $msContextLabel2 = $moSecondContext->getLabel();
            $msContextName2 = $moSecondContext->getName();
            $msContextDescription2 = $moSecondContext->getDescription();
            
            $moMailer->addContent('association');
            $moMailer->setParameter('contextType1',$msContextLabel1);
            $moMailer->setParameter('name1',$msContextName1);
            $moMailer->setParameter('description1',$msContextDescription1);
            $moMailer->setParameter('contextType2',$msContextLabel2);
            $moMailer->setParameter('name2',$msContextName2);
            $moMailer->setParameter('description2',$msContextDescription2);
          }elseif($moContext->getName()){
            $moContext->fetchById($miContextId);
            $msContextLabel = $moContext->getLabel();
            $msContextName = $moContext->getName();
            $msContextDescription = $moContext->getDescription();
            
            $moMailer->addContent('task');
            $moMailer->setParameter('contextType',$msContextLabel);
            $moMailer->setParameter('name',$msContextName);
            $moMailer->setParameter('description',$msContextDescription);
          }else{
            $moMailer->addContent('taskWithoutContext');
          }
          $moMailer->setParameter('activity',$msActivity);
          $moMailer->setParameter('sender',$msSender);
          $moMailer->setParameter('date',$msDate);
        }
        $mbToBeSent = false; // ou foi enviado, ou o usu�rio n�o quer receber mesmo
      }
    }else{
      $mbToBeSent = false;
    }
    if(!$mbToBeSent) $this->setFieldValue('task_email_sent',true);
    if($mbSendEmail){
      $miTaskId = parent::insert(true);
      $msURL = FWDWebLib::getCurrentURL();
      if(strpos($msURL,'/login.php')){
        $msURL = substr($msURL,0,strrpos($msURL,'/login.php')+1);
        $msURL = substr($msURL,0,strrpos($msURL,'/')+1).'default.php';
      }else{
      	if (strrpos($msURL, '/packages/') !== false) {
      		$msURL = substr($msURL,0,strrpos($msURL,'/packages/')+1);
      	} else {
      		$msUrl = substr($msURL,0,strrpos($msURL, '/') +1);
      	}
        $msURL = substr($msURL,0,strrpos($msURL,'/')+1).'default.php';
      }
      $moMailer->setParameter('link',"$msURL?task=$miTaskId");
      $moMailer->send();
      return $miTaskId;
    }else{
      return parent::insert($pbGetId);
    }
  }

 /**
  * Informa��o da nega��o da execu��o de uma task.
  * 
  * <p>Retorna a informa��o de que o usu�rio n�o tem permiss�o para executar
  * esta task.</p>
	*	@access	 protected
  * @return string com a informa��o de que o usu�rio n�o tem permiss�o para
  * executar esta task
  */
  protected function getDeniedPermissionToExecuteMessage(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return FWDLanguage::getPHPStringValue('wk_task_execution_permission','Voc� n�o tem permiss�o para executar esta task!');
  }

 /**
  * Testa se o usu�rio tem permiss�o para executar esta task no evento de save
  * da task.
  * 
  * <p>Testa se o usu�rio tem permiss�o para executar esta task no evento de
  * save desta task.
  * </p>
  * @param integer $piTaskId Id da task
  * @access  public
  */
  public function testPermissionToExecute($piTaskId){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    $miTaskReceiver = $this->getFieldValue('task_receiver_id'); 
    if($miTaskReceiver){
      $miContextId = $this->getFieldValue('task_context_id');
      $moTask = $this;
    }else{
      $moTask = new WKFTask();
      $moTask->fetchById($piTaskId);
      $miTaskReceiver = $moTask->getFieldValue('task_receiver_id');
      $miContextId = $moTask->getFieldValue('task_context_id');
    }
    if($miUserId == $miTaskReceiver){
      if($moTask->getFieldValue('task_is_visible')){
        /* Acho que esse teste � desnecess�rio e pode causar bugs
        $moContextObject = new ISMSContextObject();
        $moContext = $moContextObject->getContextObjectByContextId($miContextId);
        $moContext->testPermissionToExecuteTask($piTaskId);
        */
        return;
      }
    }
    trigger_error($this->getDeniedPermissionToExecuteMessage(), E_USER_ERROR);
  }

}
?>