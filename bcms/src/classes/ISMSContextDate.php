<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

// A��es
define("ACTION_CREATE", 2901);
define("ACTION_EDIT", 2902);

/**
 * Classe ISMSContextDate.
 *
 * <p>Classe que representa a tabela de �reas.</p>
 * @package ISMS
 * @subpackage classes
 */
class ISMSContextDate extends ISMSTable {
	
 /**
  * Construtor.
  * 
  * <p>Construtor da classe ISMSContextDate.</p>
  * @access public 
  */
  public function __construct(){		
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
  	parent::__construct("isms_context_date");  	  		
  	
  	$this->csAliasId = '';
  	
  	$this->coDataset->addFWDDBField(new FWDDBField("fkContext",		"context_id",		DB_NUMBER));  	  	
  	$this->coDataset->addFWDDBField(new FWDDBField("nAction",			"action",				DB_NUMBER));
  	$this->coDataset->addFWDDBField(new FWDDBField("dDate",				"ctx_date",			DB_DATETIME));
  	$this->coDataset->addFWDDBField(new FWDDBField("nUserId",			"user_id",			DB_NUMBER));
  	$this->coDataset->addFWDDBField(new FWDDBField("sUserName",		"user_name",		DB_STRING));  	  	
  }
 
 /**
  * Insere a data de cria��o de um contexto.
  * 
  * <p>M�todo para inserir a data de cria��o de um contexto.
  * Insere a data de modifica��o como sendo a data de cria��o.</p>
  * @access public 
  * 
  * @param integer $piContextId Id do contexto
  * @param integer $piUserId Id do usu�rio
  */ 
  public function insert($piContextId, $piUserId, $psUserName) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
  	$this->setFieldValue('context_id', $piContextId); 
  	$this->setFieldValue('user_id', $piUserId);
  	$this->setFieldValue('user_name', $psUserName,false);
  	$this->setFieldValue('action', ACTION_CREATE);
  	$this->setFieldValue('ctx_date', ISMSLib::ISMSTime());  	  	 	
  	parent::insert();
  	$this->setFieldValue('action', ACTION_EDIT);
  	parent::insert();
  }
 
 /**
  * Atualiza a data de modifica��o de um contexto.
  * 
  * <p>M�todo para atualizar a data de modifica��o de um contexto.</p>
  * @access public 
  * 
  * @param integer $piContextId Id do contexto
  * @param integer $piUserId Id do usu�rio
  */ 
  public function update($piContextId, $piUserId, $psUserName) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
  	$this->createFilter($piContextId, 'context_id');
  	$this->createFilter(ACTION_EDIT, 'action');
  	$this->setFieldValue('user_id', $piUserId);
  	$this->setFieldValue('user_name', $psUserName, false);
  	$this->setFieldValue('ctx_date', ISMSLib::ISMSTime());  	  	 	
  	parent::update();
  }
}
?>