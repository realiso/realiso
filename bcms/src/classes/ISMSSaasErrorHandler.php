<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSSaasErrorHandler.
 *
 * <p></p>
 * @package FWD5
 * @subpackage base
 */
class ISMSSaasErrorHandler implements FWDErrorHandler {

 /**
  * Error Handler da FWD5.
  *
  * <p>Error Handler da FWD5. Identifica o tipo de erro e levanta uma
  * exce��o que ir� efetuar seu tratamento. Aborta se necess�rio.</p>
  * @access public
  * @param integer $piErrNo N�mero do erro
  * @param string $psErrStr Mensagem de erro
  * @param string $psErrFile Nome do arquivo que gerou o erro
  * @param integer $piErrLine N�mero da linha em que ocorreu o erro
  */
  public function handleError($piErrNo, $psErrStr, $psErrFile, $piErrLine){
    try{
      $exception = new FWDException($psErrStr,$piErrNo);
      $exception->setFile($psErrFile);
      $exception->setLine($piErrLine);
      throw $exception;
    }catch(FWDException $moException){
      self::handleException($moException);
    }
  }

 /**
  * Reporta o erro.
  *
  * <p>Envia um email com as informa��es sobre o erro.</p>
  * @access public
  * @param string $psMessage Mensagem de erro
  * @param string $psErrorDump Dump de todas informa��es relevantes
  */
  public static function sendErrorReport($psMessage,$psErrorDump){
    FWDWebLib::getInstance()->writeStringDebug($psMessage,FWD_DEBUG_ERROR);

    ini_set('SMTP',ISMSLib::getConfigById(GENERAL_SMTP_SERVER));
    $msHost = ISMSLib::getConfigById(GENERAL_SMTP_SERVER);
    $mbExternal = ISMSLib::getConfigById(EXTERNAL_SERVER);
    if ($mbExternal && $msHost != 'localhost' && $msHost != '127.0.0.1'){
    	$msUser = ISMSLib::getConfigById(EXTERNAL_MAIL_USER);
    	$msPassword = ISMSLib::getConfigById(EXTERNAL_MAIL_PASSWORD);
    	$miPort = ISMSLib::getConfigById(EXTERNAL_MAIL_PORT);
    	$msEnc = ISMSLib::getConfigById(EXTERNAL_MAIL_ENCRYPTION);
    	$moEmail = new FWDEmail(true, $msHost, $miPort, $msUser, $msPassword, $msEnc);
    } else {
    	$moEmail = new FWDEmail();
    }
    $moEmail->setTo(ISMSLib::getConfigById(ERROR_REPORT_EMAIL));
    $moEmail->setFrom(ISMSLib::getConfigById(GENERAL_SMTP_DEFAULT_MAIL_SENDER));
    $moEmail->setSenderName(ISMSLib::getConfigById(GENERAL_SMTP_DEFAULT_MAIL_SENDER_NAME));
    $moEmail->setSubject('Error Report');
    $moEmail->setMessage($psMessage.'<br><br>'.base64_encode($psMessage.'<br><br>'.$psErrorDump));
    $moEmail->send();
  }

 /**
  * Exception Handler da FWD5.
  *
  * <p>Exception Handler da FWD5. Captura a exce��o e gera um trace dos erros.</p>
  * @access public
  * @param FWDException $poFWDException Exce��o
  * @static
  */
  public static function handleException($poFWDException){
    switch($poFWDException->getCode()){
      case E_USER_ERROR:   $msError = "FWD ERROR";              break;
      case E_USER_WARNING: $msError = "FWD WARNING";            break;
      case E_USER_NOTICE:  $msError = "FWD NOTICE";             break;
      case E_STRICT:       return;                              break;
      default:             $msError = "FWD Unknown error type"; break;
    }
    $msClient = ISMSSaaS::getConfig(ISMSSaaS::LICENSE_CLIENT_NAME);
    $miUser = ISMSLib::getCurrentUserId();
    $msAgent = $_SERVER['HTTP_USER_AGENT'];
    $msErrorMsg = "<b>FWD Exception Caught:</b> {$msError} [{$poFWDException->getCode()}]<br/>"
                 ."<br/>&nbsp;&nbsp;&nbsp; Message: {$poFWDException->getMessage()} <br/>"
                 ."<br/>&nbsp;&nbsp;&nbsp; File: {$poFWDException->getFile()} on line {$poFWDException->getLine()} <br/>"
                 ."<br/>&nbsp;&nbsp;&nbsp; Client: {$msClient} <br/>"
                 ."<br/>&nbsp;&nbsp;&nbsp; User: {$miUser} <br/>"
                 ."<br/>&nbsp;&nbsp;&nbsp; Browser: {$msAgent}";
                 

    $msErrorCode = "<br>TraceAsString:<br><pre>"
                  .$poFWDException->getTraceAsString()
                  ."\n\n\$_POST = ".var_export($_POST,true)."</pre>";

    if(FWDWebLib::getInstance()->getXmlLoadMethod()==XML_STATIC){
      // se o sistema t� compilado, manda email e mostra popup de comportamento inesperado
      $msJSCode = "isms_open_popup('popup_unexpected_behavior','".FWDWebLib::getInstance()->getSysRefBasedOnTabMain()."popup_unexpected_behavior.php','','true');";
      self::sendErrorReport($msErrorMsg,$msErrorCode);
    }else{
      // se o sistema n�o t� compilado, mostra popup de debug
      $msPopUpContent = $msErrorMsg.'<br><br>'.base64_encode($msErrorMsg.'<br><br>'.$msErrorCode);
      $msJSCode = "showDebug('".str_replace(array("'","\\"),array("\\'","\\\\"),$msPopUpContent)."');";
    }

    $mbIsAjax = (FWDStartEvent::getInstance()->getAjaxEventName())?1:0;

    if($mbIsAjax){
      echo $msJSCode;
    }else{
      $moWebLib = FWDWebLib::getInstance();
      $msJsRef = $moWebLib->getLibRef();
      $msSysJsRef = $moWebLib->getSysJsRef();
      echo "<script src='{$msJsRef}js.php' type='text/javascript' language='javascript'></script>";
      if($msSysJsRef){
        echo "<script src='{$msSysJsRef}js.php' type='text/javascript' language='javascript'></script>";
      }
      echo "<script>{$msJSCode}</script>";
    }
    exit();
  }

 /**
  * Adiciona um novo erro conhecido.
  *
  * <p>Adiciona um novo erro conhecido ao array de erros conhecidos.</p>
  * @access public
  * @param string $psErrorStringId Id do erro
  * @param array $paMsgSubstrings Array de strings que caracterizam o erro
  * @static
  */
  public static function addKnownError($psErrorStringId,$paMsgSubstrings){
  }

 /**
  * Remove um erro conhecido.
  *
  * <p>Remove um erro conhecido do array de erros conhecidos.</p>
  * @access public
  * @param string $psErrorStringId Id do erro
  * @static
  */
  public static function removeKnownError($psErrorStringId){
  }

}

?>