<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportAuditAlertFilter.
 *
 * <p>Classe que implementa o filtro do relat�rio de auditoria de alertas.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportAuditAlertFilter extends FWDReportFilter {
	
	protected $ciDateStart = 0;
	protected $ciDateFinish = 0;	
	protected $ciCreatorId = 0;
	protected $ciReceiverId = 0;	

	public function setDateStart($piDate) {
		$this->ciDateStart = $piDate;
	}

	public function getDateStart() {
		return $this->ciDateStart;
	}

	public function setDateFinish($piDate) {
		$this->ciDateFinish = $piDate;
	}

	public function getDateFinish() {
		return $this->ciDateFinish;
	}

	public function setCreator($piUserId) {
		$this->ciCreatorId = $piUserId;
	}

	public function getCreator() {
		return $this->ciCreatorId;
	}
	
	public function setReceiver($piUserId) {
		$this->ciReceiverId = $piUserId;
	}

	public function getReceiver() {
		return $this->ciReceiverId;
	}
	
	public function getSummary(){
    $maFilterDate = array();
    $msDateLimit = '';
		if($this->ciDateStart){
    	$msDateLimit = FWDLanguage::getPHPStringValue('rs_alerts_created_after', 'Alertas criados depois de');
    	$msDateLimit .= ' ' . ISMSLib::getISMSShortDate($this->ciDateStart, true);
    }
    if($this->ciDateFinish){    	
    	if ($msDateLimit) {
    		$msDateLimit .= ' ' . FWDLanguage::getPHPStringValue('rs_and_before', 'e antes de');
    		$msDateLimit .= ' ' . ISMSLib::getISMSShortDate($this->ciDateFinish, true);
    	}
    	else {
    		$msDateLimit = FWDLanguage::getPHPStringValue('rs_alerts_created_before', 'Alertas criados antes de');
    		$msDateLimit .= ' ' . ISMSLib::getISMSShortDate($this->ciDateFinish, true);
    	}    	
    }
    if (!$msDateLimit) $msDateLimit .= FWDLanguage::getPHPStringValue('rs_any_date', 'Qualquer data');
    $maFilterDate[] = $msDateLimit;
    
    $maFilterCreator = array();
    if ($this->ciCreatorId) {
			$moUser = new ISMSUser();
			$moUser->fetchById($this->ciCreatorId);
			$maFilterCreator[] = $moUser->getName();			
    }
    else $maFilterCreator[] = FWDLanguage::getPHPStringValue('st_all_users', 'Todos usu�rios');
    
    $maFilterReceiver = array();
    if ($this->ciReceiverId) {
			$moUser = new ISMSUser();
			$moUser->fetchById($this->ciReceiverId);
			$maFilterReceiver[] = $moUser->getName();			
    }
    else $maFilterReceiver[] = FWDLanguage::getPHPStringValue('st_all_users', 'Todos usu�rios');    
            
    $maFilters = array();
    $maFilters[] = array(
      'name' => FWDLanguage::getPHPStringValue('rs_creation_date_cl', 'Data de cria��o:'),
      'items' => $maFilterDate
    );
    $maFilters[] = array(
      'name' => FWDLanguage::getPHPStringValue('rs_creator_cl', 'Criador:'),
      'items' => $maFilterCreator
    );
    $maFilters[] = array(
      'name' => FWDLanguage::getPHPStringValue('rs_receiver_cl', 'Receptor:'),
      'items' => $maFilterReceiver
    );
    
    return $maFilters;
  }
}
?>