<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe ISMSReportAuditAlert.
 *
 * <p>Classe que implementa o relat�rio de auditoria de alertas.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportAuditAlert extends ISMSReport {
	
	/**
	 * Inicializa o relat�rio.
	 *
	 * <p>M�todo para inicializar o relat�rio.
	 * Necess�rio para funcionar com o XML compilado.</p>
	 * @access public
	 */	
	public function init() {
		parent::init();		
		
		$this->coDataSet->addFWDDBField(new FWDDBField("","alert_id", DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("","alert_date", DB_DATETIME));		
		$this->coDataSet->addFWDDBField(new FWDDBField("","alert_creator", DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField("","alert_receiver", DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField("","alert_justification", DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField("","alert_type", DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField("","alert_context_name", DB_STRING));		
		$this->coDataSet->addFWDDBField(new FWDDBField("","alert_context_type", DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("","alert_context_id", DB_NUMBER));		
	}
	
	/**
	 * Executa a query do relat�rio.
	 *
	 * <p>M�todo para executar a query do relat�rio.</p>
	 * @access public
	 */
	public function makeQuery() {
		$moFilter = $this->getFilter();
		$maFilters = array();		
    if($moFilter->getCreator()){
      $maFilters[] = "a.fkCreator = ".$moFilter->getCreator();
    }
    if($moFilter->getReceiver()){
      $maFilters[] = "a.fkReceiver = ".$moFilter->getReceiver();
    }    
    if($moFilter->getDateStart()){
      $maFilters[] = "a.dDate >= ".ISMSLib::getTimestampFormat($moFilter->getDateStart());
    }
    if($moFilter->getDateFinish()){
      $maFilters[] = "a.dDate <= ".ISMSLib::getTimestampFormat($moFilter->getDateFinish());
    }
    
    $msWhere = implode(' AND ', $maFilters);
    if ($msWhere) $msWhere = ' WHERE ' . $msWhere;
    
		$this->csQuery = "SELECT
											a.pkAlert as alert_id,
                      a.dDate as alert_date,
                      uc.sName as alert_creator,
                      ur.sName as alert_receiver,
                      cn.context_name as alert_context_name,
                      c.nType as alert_context_type,
                      a.tJustification as alert_justification,
                      a.nType as alert_type,
                      a.fkContext as alert_context_id
                    FROM
                      wkf_alert a
                      JOIN isms_context c ON (a.fkContext = c.pkContext)
                      JOIN context_names cn ON (a.fkContext = cn.context_id)
                      JOIN isms_user uc ON (a.fkCreator = uc.fkContext)
                      JOIN isms_user ur ON (a.fkReceiver = ur.fkContext)
											$msWhere ORDER BY a.dDate DESC";
		
		return parent::executeQuery();
	}
	
	/**
	 * Desenha o cabe�alho do relat�rio.
	 *
	 * <p>M�todo para desenhar o cabe�alho do relat�rio.</p>
	 * @access public
	 */
	public function drawHeader(){
    foreach($this->caHeaders as $moHeader){
      if($moHeader->getAttrName()=='header_filter'){
        $moHeaderFilter = $moHeader;
      }elseif($moHeader->getAttrName()=='header_filter_item'){
        $moHeaderFilterItem = $moHeader;
      }elseif($moHeader->getAttrName()=='report_header_blank'){
        $moHeaderBlank = $moHeader;
      }else{
        $this->coWriter->drawLine($moHeader,array());
      }
    }
    if ($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());
    
    $moFilterText = FWDWebLib::getObject('header_filter_text');
    $moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
    $maFilters = $this->coFilter->getSummary();
    foreach($maFilters as $maFilter){
      $moFilterText->setValue($maFilter['name']);
      $this->coWriter->drawLine($moHeaderFilter,array());
      foreach($maFilter['items'] as $msFilterItem){
        $moFilterItemText->setValue($msFilterItem);
        $this->coWriter->drawLine($moHeaderFilterItem,array());
      }
    }
    $this->coWriter->drawLine($moHeaderBlank,array());
  }
}
?>