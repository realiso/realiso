<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportAuditTaskFilter.
 *
 * <p>Classe que implementa o filtro do relat�rio de auditoria de tarefas.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportAuditTaskFilter extends FWDReportFilter {
	
	protected $ciDateCreationStart = 0;
	protected $ciDateCreationFinish = 0;	
	protected $ciCreatorId = 0;
	protected $ciReceiverId = 0;
	protected $ciActivityId = 0;

	public function setDateCreationStart($piDate) {
		$this->ciDateCreationStart = $piDate;
	}

	public function getDateCreationStart() {
		return $this->ciDateCreationStart;
	}

	public function setDateCreationFinish($piDate) {
		$this->ciDateCreationFinish = $piDate;
	}

	public function getDateCreationFinish() {
		return $this->ciDateCreationFinish;
	}

	public function setCreator($piUserId) {
		$this->ciCreatorId = $piUserId;
	}

	public function getCreator() {
		return $this->ciCreatorId;
	}
	
	public function setReceiver($piUserId) {
		$this->ciReceiverId = $piUserId;
	}

	public function getReceiver() {
		return $this->ciReceiverId;
	}

	public function setActivity($piActivityId) {
		$this->ciActivityId = $piActivityId;
	}

	public function getActivity() {
		return $this->ciActivityId;
	}
	
	public function getSummary(){
    $maFilterDate = array();
    $msDateLimit = '';
		if($this->ciDateCreationStart){
    	$msDateLimit = FWDLanguage::getPHPStringValue('rs_tasks_created_after', 'Tarefas criadas depois de');
    	$msDateLimit .= ' ' . ISMSLib::getISMSShortDate($this->ciDateCreationStart, true);
    }
    if($this->ciDateCreationFinish){    	
    	if ($msDateLimit) {
    		$msDateLimit .= ' ' . FWDLanguage::getPHPStringValue('rs_and_before', 'e antes de');
    		$msDateLimit .= ' ' . ISMSLib::getISMSShortDate($this->ciDateCreationFinish, true);
    	}
    	else {
    		$msDateLimit = FWDLanguage::getPHPStringValue('rs_tasks_created_before', 'Tarefas criadas antes de');
    		$msDateLimit .= ' ' . ISMSLib::getISMSShortDate($this->ciDateCreationFinish, true);
    	}    	
    }
    if (!$msDateLimit) $msDateLimit .= FWDLanguage::getPHPStringValue('rs_any_date', 'Qualquer data');
    $maFilterDate[] = $msDateLimit;
    
    $maFilterCreator = array();
    if ($this->ciCreatorId) {
			$moUser = new ISMSUser();
			$moUser->fetchById($this->ciCreatorId);
			$maFilterCreator[] = $moUser->getName();			
    }
    else $maFilterCreator[] = FWDLanguage::getPHPStringValue('st_all_users', 'Todos usu�rios');
    
    $maFilterReceiver = array();
    if ($this->ciReceiverId) {
			$moUser = new ISMSUser();
			$moUser->fetchById($this->ciReceiverId);
			$maFilterReceiver[] = $moUser->getName();			
    }
    else $maFilterReceiver[] = FWDLanguage::getPHPStringValue('st_all_users', 'Todos usu�rios');
    
    $maFilterActivity = array();
    if ($this->ciActivityId) $maFilterActivity[] = ISMSActivity::getDescription($this->ciActivityId);    
    else $maFilterActivity[] = FWDLanguage::getPHPStringValue('st_all', 'Todas');
            
    $maFilters = array();
    $maFilters[] = array(
      'name' => FWDLanguage::getPHPStringValue('rs_creation_date_cl', 'Data de cria��o:'),
      'items' => $maFilterDate
    );
    $maFilters[] = array(
      'name' => FWDLanguage::getPHPStringValue('rs_creator_cl', 'Criador:'),
      'items' => $maFilterCreator
    );
    $maFilters[] = array(
      'name' => FWDLanguage::getPHPStringValue('rs_receiver_cl', 'Receptor:'),
      'items' => $maFilterReceiver
    );
    $maFilters[] = array(
      'name' => FWDLanguage::getPHPStringValue('rs_activity_cl', 'Atividade:'),
      'items' => $maFilterActivity
    );
    
    return $maFilters;
  }
}
?>