<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

class CMPlanSchedule extends ISMSContext {

	public function __construct() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		parent::__construct("cm_plan_schedule");
		$this->csAliasId = "id";
		$this->ciContextType = CONTEXT_CM_PLAN_SCHEDULE;
		$this->coDataset->addFWDDBField(new FWDDBField("fkContext"             	,"id" 		   			,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("date"					,"date"					,DB_DATETIME));
		$this->coDataset->addFWDDBField(new FWDDBField("fkplan"	       			,"plan"	 	    		,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkgroup"	       		,"group"	 	    	,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkresponsible"	       	,"responsible"	 	    ,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("status"                	,"status" 		   		,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fktest"                	,"test" 		   		,DB_NUMBER));
	}

	public function updateStatus(){
		$query = new FWDDBDataSet(FWDWebLib::getConnection());
		$query->setQuery("select fkcontext as id, date as date from view_cm_plan_schedule_active where status <> 2 and status <> 3");
		$query->addFWDDBField(new FWDDBField('id','id'  ,DB_NUMBER));
		$query->addFWDDBField(new FWDDBField('date','date'  ,DB_DATETIME));
		$query->execute();
		$now = time();
		while($query->fetch()){
			$id = $query->getFieldByAlias("id")->getValue();
			$date = ISMSLib::getTimestamp($query->getFieldByAlias("date")->getValue());
			if($date < $now){
				$sch = new CMPlanSchedule();
				$sch->fetchById($id);
				$sch->setFieldValue("status", 3);
				if(!$sch->getFieldValue("test")){
					$sch->setFieldValue("test", 'null');
				}

				$sch->update($id);
			}
		}

		$query = new FWDDBDataSet(FWDWebLib::getConnection());
		$query->setQuery("select sch.fkcontext as id from view_cm_plan_schedule_active sch join view_cm_plan_test_active p on p.fkcontext = sch.fktest where p.startTime is not null and p.endTime is null");
		$query->addFWDDBField(new FWDDBField('id','id'  ,DB_NUMBER));
		$query->execute();
		while($query->fetch()){
			$id = $query->getFieldByAlias("id")->getValue();
			$sch = new CMPlanSchedule();
			$sch->fetchById($id);
			if($sch->getFieldValue("status") != 4){
				$sch->setFieldValue("status", 4);
				if(!$sch->getFieldValue("test")){
					$sch->setFieldValue("test", 'null');
				}
				$sch->update($id);
			}
		}
	}

	public function update($piContextId, $pbLog = true, $pbHasSensitiveChanges = false) {
		if(!$this->getFieldValue("responsible")){
			$this->setFieldValue("responsible", "null");
		}
		if(!$this->getFieldValue("group")){
			$this->setFieldValue("group", "null");
		}
		parent::update($piContextId,$pbLog,$pbHasSensitiveChanges);
	}

	public function insert($pbReturnId = false,$pbExecBySystem = false) {
		if(!$this->getFieldValue("responsible")){
			$this->setFieldValue("responsible", "null");
		}
		if(!$this->getFieldValue("group")){
			$this->setFieldValue("group", "null");
		}
		return parent::insert($pbReturnId,$pbExecBySystem);
	}

	public function setGroup($id) {
		$this->setFieldValue('responsible',"null");
		$this->setFieldValue('group',"null");
		if($id){
			$group = new CMGroup();
			$resource = new CMResource();

			if($group->fetchById($id)){
				$this->setFieldValue('group',$id);
			}else if($resource->fetchById($id)){
				$this->setFieldValue('responsible',$id);
			}
		}
	}

	public function getGroup() {
		if($this->getFieldValue('responsible') && $this->getFieldValue('responsible') != "null"){
			return $this->getFieldValue('responsible');
		}
		return $this->getFieldValue('group');
	}

	public function getGroupName() {
		if($this->getFieldValue('responsible')){
			$r = new CMResource();
			$r->fetchById($this->getFieldValue('responsible'));
			return $r->getFieldValue('resource_name');
		}
		if($this->getFieldValue('group')){
			$r = new CMGroup();
			$r->fetchById($this->getFieldValue('group'));
			return $r->getFieldValue('group_name');
		}
		return null;
	}

	public function setPlanResponsible($planId) {
		if($planId){
			$plan = new CMPlan();
			$plan->fetchById($planId);

			$areaId = $plan->getFieldValue("area");
			$processId = $plan->getFieldValue("process");
			$assetId = $plan->getFieldValue("asset");
			$placeId = $plan->getFieldValue("place");

			if($areaId){
				$area = new RMArea();
				$area->fetchById($areaId);
				if($area->getFieldValue('resource')){
					$this->setFieldValue('responsible', $area->getFieldValue('resource'));
					$this->setFieldValue('group', "null");
				}else if($area->getFieldValue('group')){
					$this->setFieldValue('group', $area->getFieldValue('group'));
					$this->setFieldValue('responsible', "null");
				}
			}else if($processId){
				$process = new RMProcess();
				$process->fetchById($processId);
				if($process->getFieldValue('resource')){
					$this->setFieldValue('responsible', $process->getFieldValue('resource'));
					$this->setFieldValue('group', "null");
				}else if($process->getFieldValue('group')){
					$this->setFieldValue('group', $process->getFieldValue('group'));
					$this->setFieldValue('responsible', "null");
				}
			}else if($assetId){
				$asset = new RMAsset();
				$asset->fetchById($assetId);
				if($asset->getFieldValue('group')){
					$this->setFieldValue('group', $asset->getFieldValue('group'));
					$this->setFieldValue('responsible', "null");
				}
			}else if($placeId){
				$place = new CMPlace();
				$place->fetchById($placeId);
				if($place->getFieldValue('resource')){
					$this->setFieldValue('responsible', $place->getFieldValue('resource'));
					$this->setFieldValue('group', "null");
				}else if($place->getFieldValue('group')){
					$this->setFieldValue('group', $place->getFieldValue('group'));
					$this->setFieldValue('responsible', "null");
				}
			}
		}
	}

	public function getSystemPathScroll($piTab, $piContextType, $context){

		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		$idPlan = 0;

		$ids = explode(':',$context);

		if((isset($ids[0])) && ($ids[0])){
			$idPlan = $ids[0];
		}

		$final = FWDLanguage::getPHPStringValue('schedule','Agenda');

		$maPath = array();
		if($idPlan != 0){
			$plan = new CMPlan();
			$plan->fetchById($idPlan);
			$maPath[] = "<a href='javascript:enterPlan();'>";
			$maPath[] = $plan->getName();
			$maPath[] = ISMSLib::getIconCode('',-1);
			$maPath[] = "</a>";
		}

		$icon = ISMSLib::getIconCode('',-1);

		$maPath[]="<a href=''>";
		$maPath[]= $final;
		$maPath[]=''.$icon;
		$maPath[]="</a>";

		$miIContPath = count($maPath);
		for($miI=$miIContPath-1;$miI>0;$miI--){
			if((isset($maPath[$miI-7]))&&($maPath[$miI-7]!=''))
			$maPath[$miI-3] = $maPath[$miI-7];
			$miI=$miI-3;
		}

		return $maPath;
	}

	public function getLabel(){ return ''; }
	public function getName(){ return ''; }
	protected function userCanDelete($piContextId){ return true; }
	protected function userCanEdit($piContextId,$piUserResponsible = 0){ return true; }
	protected function userCanInsert(){ return true; }
	public function getResponsible() {return null;}
}

?>
