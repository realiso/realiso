<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */


/**
 * Classe CMImpact
 *
 * <p>Classe que representa um impacto de um cenario</p>
 * @package ISMS
 * @subpackage classes
 */
class CMImpact extends ISMSContext {
	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe CMPlaceType.</p>
	 * @access public
	 */
	public function __construct() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		parent::__construct("cm_impact");
		$this->csAliasId = "impact_id";
		$this->ciContextType = CONTEXT_CM_IMPACT;
		$this->coDataset->addFWDDBField(new FWDDBField("fkContext"             ,"impact_id" 		   ,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("sname"	               ,"impact_name" 	       ,DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("ntype" 		           ,"impact_type"  		   ,DB_NUMBER));
	}

	public function getLabel(){ return ''; }
	public function getName(){ return $this->getFieldValue("impact_name"); }
	protected function userCanDelete($piContextId){ return true; }
	protected function userCanEdit($piContextId,$piUserResponsible = 0){ return true; }
	protected function userCanInsert(){ return true; }
	public function getResponsible(){ return null; }
	/**
	 * Retorna o caminho do contexto para a cria��o do pathscroll.
	 *
	 * <p>M�todo para retornar o caminho do contexto para a cria��o do pathscroll.</p>
	 * @access public
	 * @param integer $piTab id da tab
	 * @param integer $piContextType id do contexto
	 * @param string $psContextFunc fun��o que deve ser utilizada para gerar o c�digo do link
	 * @return array Array contendo o Caminho do contexto
	 */
	public function getSystemPathScroll($piTab,$piContextType,$psContextFunc){
		$msHome = FWDLanguage::getPHPStringValue('st_home','Raiz');
		$maPath=array();
		$maPath[] = "<a href=''>";
		$maPath[] = $msHome;
		$maPath[] = ISMSLib::getIconCode('',-1);
		$maPath[]="</a> ";;
		$maPath[] = "<a href=''>";
		$maPath[] = 'teste';
		$maPath[] = ISMSLib::getIconCode('icon-process_',$this->getValue());
		$maPath[]="</a> ";
		return $maPath;
	}


}
?>