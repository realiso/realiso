<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */


/**
 * Classe CMProcessInput
 *
 * <p>Classe que representa um tipo de lugar, de acordo com a lista de lugares da biblioteca.</p>
 * @package ISMS
 * @subpackage classes
 */
class CMPlace extends ISMSContext {

	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe CMPlaceType.</p>
	 * @access public
	 */
	public function __construct() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		parent::__construct("cm_place");
		$this->csAliasId = "place_id";
		$this->ciContextType = CONTEXT_CM_PLACE;
		$this->coDataset->addFWDDBField(new FWDDBField("fkContext"             	,"place_id" 			   	,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("sname"	               	,"place_name"           	,DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("sdescription"          	,"place_description"    	,DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("saddress" 	           	,"place_address" 	   		,DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("fkParent"				,"parent"					,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkResource"      		,"resource"       			,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkGroup"      			,"group"       				,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkResourceSubstitute"  	,"resourceSubstitute"      	,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkGroupSubstitute"     	,"groupSubstitute"       	,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("file_path"				,"file_path"				,DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("file_name"				,"file_name"				,DB_STRING));
	}

	public function removePlace($id){
		$pp = new CMPlaceProcess();
		$pp->createFilter($id, 'place_id');
		$pp->select();
		while($pp->fetch()){
			$pp->delete($pp->getFieldValue('place_process_id'));
		}

		$childs = new CMPlace();
		$childs->createFilter($id, 'parent');
		$childs->select();
		while($childs->fetch()){
			$childs->removePlace($childs->getFieldValue('place_id'));
		}
		$c = new CMPlace();
		$c->delete($id);
	}

	public function setSubstitute($id) {
		$this->setFieldValue('groupSubstitute',"null");
		$this->setFieldValue('resourceSubstitute',"null");
		if($id){
			$group = new CMGroup();
			$resource = new CMResource();

			if($group->fetchById($id)){
				$this->setFieldValue('groupSubstitute',$id);

			}else if($resource->fetchById($id)){
				$this->setFieldValue('resourceSubstitute',$id);

			}
		}
	}

	public function setGroup($id) {
		$this->setFieldValue('resource',"null");
		$this->setFieldValue('group',"null");
		if($id){
			$group = new CMGroup();
			$resource = new CMResource();

			if($group->fetchById($id)){
				$this->setFieldValue('group',$id);
			}else if($resource->fetchById($id)){
				$this->setFieldValue('resource',$id);
			}
		}
	}

	public function setCMT($id) {
		$this->setFieldValue('cmt',"null");
		if($id){
			$this->setFieldValue('cmt',$id);
		}
	}

	public function getCMT() {
		return $this->getFieldValue('cmt');
	}

	public function getCMTName() {
		if($this->getFieldValue('cmt')){
			$r = new CMGroup();
			$r->fetchById($this->getFieldValue('cmt'));
			return $r->getFieldValue('group_acronym');
		}
		return null;
	}

	public function getGroup() {
		if($this->getFieldValue('resource') && $this->getFieldValue('resource') != "null"){
			return $this->getFieldValue('resource');
		}
		return $this->getFieldValue('group');
	}

	public function getSubstitute() {
		if($this->getFieldValue('resourceSubstitute') && $this->getFieldValue('resourceSubstitute') != "null"){
			return $this->getFieldValue('resourceSubstitute');
		}
		return $this->getFieldValue('groupSubstitute');
	}

	public function getGroupName() {
		if($this->getFieldValue('resource')){
			$r = new CMResource();
			$r->fetchById($this->getFieldValue('resource'));
			return $r->getFieldValue('resource_name');
		}
		if($this->getFieldValue('group')){
			$r = new CMGroup();
			$r->fetchById($this->getFieldValue('group'));
			return $r->getFieldValue('group_name');
		}
		return null;
	}

	public function getSubstituteName() {
		if($this->getFieldValue('resourceSubstitute')){
			$r = new CMResource();
			$r->fetchById($this->getFieldValue('resourceSubstitute'));
			return $r->getFieldValue('resource_name');
		}
		if($this->getFieldValue('groupSubstitute')){
			$r = new CMGroup();
			$r->fetchById($this->getFieldValue('groupSubstitute'));
			return $r->getFieldValue('group_name');
		}
		return null;
	}

	public function getSystemPathScroll($piTab,$piContextType,$psContextId, $title){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		// Sem nenhum id, mostrar apenas 'Raiz'
		if(!$psContextId){
			$msHome = FWDLanguage::getPHPStringValue('st_home','Raiz');

			$maPath = array();
		    $maPath[] = '';
		   	$maPath[] = $msHome;
		   	$maPath[] = '';
		   	$maPath[] = '';

		   	return $maPath;
		}

	  	// Sub-Locais
		$placeParents = array_reverse($this->getPlaceParents($psContextId));
		$parentsTotal = count($placeParents);

		$maPath = array();

		for($i=0; $i<$parentsTotal; $i++){

    		$cmPlace = new CMPlace();
    		$placeId = $placeParents[$i];

    		$cmPlace->fetchById($placeId);

    		if($i == 0) {
    			$maPath[] = "<a href='javascript:isms_redirect_to_mode_scroll($piTab, " . CONTEXT_CM_PLACE . ", 0)'>";
		    } else {

		    	$returnPlaceId = $placeParents[$i - 1];
		    	$maPath[] = "<a href='javascript:enterPlace($returnPlaceId)'>";
		   	}

		   	$maPath[] = $cmPlace->getName();
		   	$maPath[] = "";
		   	$maPath[] = "</a>";

		}

		// Se j� estivermos num path, mudar o link. Se estivermos apontando para 1 item no path, esse volta para a Raiz.
		if($parentsTotal){
	   		$returnPlaceId = $placeParents[$parentsTotal - 1];
	   		$maPath[] = "<a href='javascript:enterPlace($returnPlaceId)'>";
	   	} else {
	   		$maPath[] = "<a href='javascript:isms_redirect_to_mode_scroll($piTab, ". CONTEXT_CM_PLACE . ", 0)'>";
	   	}

		$maPath[] = $this->getName();
		$maPath[] = "";
		$maPath[] = "</a>";

		// Titulo do que estamos mostrando.
	    $maPath[] = "";
	   	$maPath[] = $title;
	   	$maPath[] = "";
	   	$maPath[] = "";

	  	$miIContPath = count($maPath);
		  
	    for($miI=$miIContPath-1;$miI>0;$miI--){
				if((isset($maPath[$miI-7]))&&($maPath[$miI-7]!=''))
					$maPath[$miI-3] = $maPath[$miI-7];
				$miI=$miI-3;
	    }

	   	return $maPath;
	}

	public function getPlaceParents($piPlaceId) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		$result = array();
		$this->getParents($piPlaceId, $result);
		return $result;
	}

	protected function getParents($piPlaceId, &$result){
		$query = new FWDDBDataSet(FWDWebLib::getConnection());
		$query->setQuery("select fkparent as parent from view_cm_place_active where fkcontext = {$piPlaceId}");
		$query->addFWDDBField(new FWDDBField('parent', 'parent', DB_NUMBER));
		$query->execute();
		while($query->fetch()){
			$idParent = $query->getFieldByAlias("parent")->getValue();
			if($idParent){
				$result[] = $idParent;
				$this->getParents($idParent, $result);
			}
		}
	}

	public function getLabel(){ return ''; }
	public function getName(){ return $this->getFieldValue('place_name'); }
	
	protected function userCanDelete($piContextId){ return true; }
	protected function userCanEdit($piContextId,$piUserResponsible = 0){ return true; }
	protected function userCanInsert(){ return true; }
	public function getResponsible(){return null;}

	/* DELETE CASCADE */

	/*
	 * Verifica se tem sub-locais
	 */
	private function getSubPlaces($contextId){
	    $maParameters = func_get_args();
	    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

	    $cmPlace = new CMPlace();
	    //$cmPlace->createFilter($contextId, "place_id");
	    $cmPlace->createFilter("$contextId", "parent", "=");
	    $cmPlace->select();

	    $places = array();
	    while($cmPlace->fetch()){
	    	$places[] = $cmPlace->getFieldValue("place_id");
	    }

	    if(count($places))
	    	return $places;
	    else
	    	return null;
	}

	/*
	 * Pega as Unidades de Negocio do Local
	 */
	private function getUNFromPlace($contextId){
	    $maParameters = func_get_args();
	    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

	    $rmArea = new RMArea();
	    $rmArea->createFilter($contextId, "area_place");
	    $rmArea->select();

	    $areas = array();
	    while($rmArea->fetch()){
	    	$areas[] = $rmArea->getFieldValue("area_id");
	    }

	    if(count($areas))
	    	return $areas;
	   	else
	   		return null;
	}

	/*
	 * Pega as amea�as do Local
	 */
	private function getThreatFromPlace($contextId){
	    $maParameters = func_get_args();
	    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

	    $cmPlaceThreat = new CMPlaceThreat();
	    $cmPlaceThreat->createFilter($contextId, "place_id");
	    $cmPlaceThreat->select();

	    $threats = array();
	    while($cmPlaceThreat->fetch()){
	    	$threats[] = $cmPlaceThreat->getFieldValue("threat_id");
	    }

	    if(count($threats))
	    	return $threats;
	   	else
	   		return null;
	}

	/*
	 * Pega os ativos do Local
	 */
	private function getAssetFromPlace($contextId){
	    $maParameters = func_get_args();
	    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

	    $rmAsset = new RMAsset();
	    $rmAsset->createFilter($contextId, "place");
	    $rmAsset->select();

	    $assets = array();
	    while($rmAsset->fetch()){
	    	$assets[] = $rmAsset->getFieldValue("asset_id");
	    }

	    if(count($assets))
	    	return $assets;
	   	else
	   		return null;
	}

	/*
	 * Pega os planos do Local
	 */
	private function getPlanFromPlace($contextId){
	    $maParameters = func_get_args();
	    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

	    $cmPlan = new CMPlan();
	    $cmPlan->createFilter($contextId, "place");
	    $cmPlan->select();

	    $plans = array();
	    while($cmPlan->fetch()){
	    	$plans[] = $cmPlan->getFieldValue("id");
	    }

	    if(count($plans))
	    	return $plans;
	   	else
	   		return null;
	}

	/*
	 * Pega os CMT do Local
	 */
	private function getCMTFromPlace($contextId){
	    $maParameters = func_get_args();
	    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

	    $cmtPlaceResource = new CMPlaceResource();
	    $cmtPlaceResource->createFilter("1", "cmt");
	    $cmtPlaceResource->createFilter($contextId, "place_id");
	    $cmtPlaceResource->select();

	    $cmts = array();
	    while($cmtPlaceResource->fetch()){
	    	$cmts[] = $cmtPlaceResource->getFieldValue("id");
	    }

	    if(count($cmts))
	    	return $cmts;
	   	else
	   		return null;
	}	

	public function verifySubContexts($contextId){
	    $maParameters = func_get_args();
	    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		$subPlaces = $this->getSubPlaces($contextId);

		if($subPlaces)
			return true;

		$areas = $this->getUNFromPlace($contextId);

		if($areas)
			return true;

		$threats = $this->getThreatFromPlace($contextId);

		if($threats)
			return true;

		$assets = $this->getAssetFromPlace($contextId);

		if($assets)
			return true;		

		$plans = $this->getPlanFromPlace($contextId);

		if($plans)
			return true;

		$cmts = $this->getCMTFromPlace($contextId);

		if($cmts)
			return true;

		return false;
	}

	 /**
	  * Indica se o contexto � delet�vel ou n�o.
	  * 
	  * <p>M�todo que indica se o contexto � delet�vel ou n�o.</p>
	  * @access public
	  * @param integer $piContextId id do contexto
	  */ 
	  public function isDeletable($piContextId) {
	    $maParameters = func_get_args();
	    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
	    if (!ISMSLib::getConfigById(GENERAL_CASCADE_ON)) {
	      // cascade off: verificar exist�ncia de subcontextos
	      if ($this->verifySubContexts($piContextId))
	        return false;
	    }
	    return true;
	  }
	  
	 /**
	  * Exibe uma popup caso n�o seja poss�vel remover um contexto.
	  * 
	  * <p>M�todo que exibe uma popup caso n�o seja poss�vel remover um contexto.</p>
	  * @access public
	  * @param integer $piContextId id do contexto
	  */ 
	  public function showDeleteError($piContextId) {
	   $maParameters = func_get_args();
	    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
	    $msTitle = FWDLanguage::getPHPStringValue('tt_place_remove_error','Erro ao remover Local');
	    $msMessage = FWDLanguage::getPHPStringValue('st_place_remove_error_message',"N�o foi poss�vel remover o Local <b>%place_name%</b>: O Local cont�m Sub-Locais, Unidade de Neg�cios, Amea�as, Ativos, Planos e/ou CMT. Para remover, ative a op��o \"Delete Cascade\" na aba \"Admin -> Configura��o\".");
	    $cmPlace = new CMPlace();
	    $cmPlace->fetchById($piContextId);
	    $cmPlaceName = $cmPlace->getFieldValue('place_name');
	    $msMessage = str_replace("%place_name%",$cmPlaceName,$msMessage);
	    ISMSLib::openOk($msTitle,$msMessage,"",80);
	  }
	  
	 /**
	  * Deleta logicamente um contexto.
	  * 
	  * <p>M�todo para deletar logicamente um contexto.</p>
	  * @access public
	  * @param integer $piContextId id do contexto
	  */ 
	  public function logicalDelete($piContextId) {
	    $maParameters = func_get_args();
	    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

	    $subPlaces = null;
	    $subPlaces = $this->getSubPlaces($piContextId);

	    if($subPlaces) {
		    foreach($subPlaces as $value){
		    	$cmPlace = new CMPlace();
		    	$cmPlace->delete($value);
		    }
		}

	    $areas = null;
	    $areas = $this->getUNFromPlace($piContextId);

	    if($areas) {
		    foreach($areas as $value){
		    	$rmArea = new RMArea();
		    	$rmArea->delete($value);
		    }
		}

	    $threats = null;
	    $threats = $this->getThreatFromPlace($piContextId);

	    if($threats) {
		    foreach($threats as $value){
		    	$cmThreat = new CMThreat();
		    	$cmThreat->delete($value);
		    }
		}
	    
	    $assets = null;
	    $assets = $this->getAssetFromPlace($piContextId);

	    if($assets) {
		    foreach($assets as $value){
		    	$rmAsset = new RMAsset();
		    	$rmAsset->delete($value);
		    }
		}	    

	    $plans = null;
	    $plans = $this->getPlanFromPlace($piContextId);

	    if($plans) {
		    foreach($plans as $value){
		    	$cmPlan = new CMPlan();
		    	$cmPlan->delete($value);
		    }
		}

	    $cmts = null;
	    $cmts = $this->getCMTFromPlace($piContextId);

	    if($cmts) {
		    foreach($cmts as $value){
		    	$cmPlaceResource = new CMPlaceResource();
		    	$cmPlaceResource->delete($value);
		    }
		}
	  }

	public function associateAssets($placeId, $assets){
		foreach ($assets as $assetId) {
			$asset = new RMAsset();
			$asset->fetchById($assetId);
			$asset->setFieldValue('place', $placeId);
			$asset->update($assetId);
		}
	}

	public function removeAssets($assets){
		foreach ($assets as $assetId) {
			$asset = new RMAsset();
			$asset->fetchById($assetId);
			$asset->setFieldValue('place', 'null');
			$asset->update($assetId);
		}
	}

}
?>