<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */


/**
 * Classe CMScene
 *
 * <p>Classe que representa um escopo,</p>
 * @package ISMS
 * @subpackage classes
 */
include_once $handlers_ref . "select/QuerySelectPriority.php";
include_once $handlers_ref . "select/QuerySelectActiveImpacts.php";
class CMScene extends ISMSContext {
	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe CMPlaceType.</p>
	 * @access public
	 */
	public function __construct() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		parent::__construct("cm_scene");
		$this->csAliasId = "scene_id";
		$this->ciContextType = CONTEXT_CM_SCOPE;
		$this->coDataset->addFWDDBField(new FWDDBField("fkContext"             ,"scene_id" 			   ,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("sname"	               ,"scene_name"           ,DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("sdescription"          ,"scene_description"    ,DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("consequence"           ,"scene_consequence"    ,DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("nrto" 		           ,"scene_rto"   		   ,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("nrto_flags" 	       ,"scene_rto_flags" 	   ,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("nrpo" 		           ,"scene_rpo"   		   ,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("nrpo_flags" 	       ,"scene_rpo_flags" 	   ,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("nmtpd" 		           ,"scene_mtpd"   		   ,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("nmtpd_flags" 	       ,"scene_mtpd_flags" 	   ,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("dstartdate" 	       ,"scene_start_date" 	   ,DB_DATETIME));
		$this->coDataset->addFWDDBField(new FWDDBField("denddate" 	      	   ,"scene_end_date" 	   ,DB_DATETIME));
		$this->coDataset->addFWDDBField(new FWDDBField("nflags" 	      	   ,"scene_flags" 		   ,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkprocess" 	      	   ,"scene_process" 	   ,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("nhourstart" 	   	   ,"scene_start_hour" 	   ,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("nhourend" 	      	   ,"scene_end_hour" 	   ,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkprobability" 	       ,"probability" 	   	   ,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkSeasonality",       	"seasonality",          DB_NUMBER));
	}

	public function cloneEntity($scene){
		$msNewAssetName = $scene->getFieldValue('scene_name') .' - '.FWDLanguage::getPHPStringValue('st_scene_copy','C�pia');

		$this->setFieldValue('scene_name',$msNewAssetName,false);

		$this->copyAttribute('scene_description', 				$scene);
		$this->copyAttribute('scene_consequence', 				$scene);
		$this->copyAttribute('scene_rto', 						$scene);
		$this->copyAttribute('scene_rto_flags', 				$scene);
		$this->copyAttribute('scene_rpo', 						$scene);
		$this->copyAttribute('scene_rpo_flags', 				$scene);
		$this->copyAttribute('scene_mtpd', 						$scene);
		$this->copyAttribute('scene_mtpd_flags', 				$scene);
		$this->copyAttribute('scene_start_date', 				$scene);
		$this->copyAttribute('scene_end_date', 					$scene);
		$this->copyAttribute('scene_flags', 					$scene);
		$this->copyAttribute('scene_process',					$scene);
		$this->copyAttribute('scene_start_hour', 				$scene);
		$this->copyAttribute('scene_end_hour',			 		$scene);
		$this->copyAttribute('probability', 					$scene);

		$seasonalityId = $scene->getFieldValue("seasonality");
		if($seasonalityId){
			$seasonality = new WKFSchedule();
			$seasonality->fetchById($seasonalityId);
			$newSeasonality = new WKFSchedule();
			$newSeasonality->cloneEntity($seasonality);
			$newSeasonalityId = $newSeasonality->insert(true);
				
			$this->setFieldValue("seasonality", $newSeasonalityId);
		}
	}

	public function executeDefaultInsert($id){
		$impacts = new QuerySelectActiveImpacts();
		$impacts->makeQuery();
		$impacts->executeQuery();

		$damageMatrix = new CMDamageMatrix();
		$matrix = $damageMatrix->getDamageMatrix();

		$damageMatrixParameter = new CMDamageMatrixParameter();
		$lowestParameter = $damageMatrixParameter->getLowestDamageParameter();

		$financialImpact = new CMFinancialImpact();
		$lowestFinancialImpact = $financialImpact->getLowestFinancialImpact();

		while($impacts->fetch()){
			$impactDamage = new CMImpactDamage();
			$impactDamage->setFieldValue('impact_id', $impacts->getFieldValue('fkcontext'));
			$impactDamage->setFieldValue('scene_id', $id);

			$idImpactDamage = $impactDamage->insert(true);
			foreach($matrix as $matrixId=>$matrixValue) {
				$damageRange = new CMImpactDamageRange();
				$damageRange->setFieldValue('impact_damage_id', $idImpactDamage);
				$damageRange->setFieldValue('impact_damage_damage_id', $matrixId);
				$damageRange->setFieldValue('impact_damage_parameter_id', $lowestParameter);
				$damageRange->setFieldValue('financial_impact', $lowestFinancialImpact);
				$damageRange->insert(true);
			}
		}
	}

	public function copyAttribute($attr, $scene){
		$value = $scene->getFieldValue($attr);
		if(!$value && $this->getFieldByAlias($attr)->getType() == DB_NUMBER){
			$this->setFieldValue($attr, 'null');
		}else{
			$this->setFieldValue($attr, $value);
		}

	}

	public function getLabel(){ return ''; }
	public function getName(){ return $this->getFieldValue("scene_name"); }
	protected function userCanDelete($piContextId){ return true; }
	protected function userCanEdit($piContextId,$piUserResponsible = 0){ return true; }
	protected function userCanInsert(){ return true; }
	public function getResponsible(){ return null; }

	public function getSerializedSeasonality(){
		$miScheduleId = $this->getFieldValue('seasonality');
		if(!$miScheduleId){
			$process = $this->getProcess();
			if(!$process){
				return null;
			}

			$miScheduleId = $process->getFieldValue('seasonality');
			if(!$miScheduleId){
				return null;
			}
		}

		$moSchedule = new WKFSchedule();
		$moSchedule->fetchById($miScheduleId);
		return serialize($moSchedule->getFieldValues());
	}

	public function getProcess(){
		if(!$this->getFieldValue('scene_process')){
			return null;
		}
		$process = new RMProcess();
		$process->fetchById($this->getFieldValue('scene_process'));
		return $process;
	}

	/**
	 * Retorna o caminho do contexto para a cria��o do pathscroll.
	 *
	 * <p>M�todo para retornar o caminho do contexto para a cria��o do pathscroll.</p>
	 * @access public
	 * @param integer $piTab id da tab
	 * @param integer $piContextType id do contexto
	 * @param string $psContextFunc fun��o que deve ser utilizada para gerar o c�digo do link
	 * @return array Array contendo o Caminho do contexto
	 */
	/*
	public function getSystemPathScroll($piTab,$piContextType,$psContextId){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		$maPath = array();
		$paContextId = explode(':',$psContextId);
		$process = 0;
		if((isset($paContextId[0]))&&($paContextId[0])){
			$process = $paContextId[0];
		}else{
			$process = $this->getFieldByAlias("scene_process")->getValue();
		}

		if($psContextId){
			$maPath[]="<a href='javascript:{$psContextId}(0);'>";
		}else{
			$maPath[]="<a href='javascript:soTabSubManager.changeTab(TAB_PROCESS,'nav_process.php');'>";
		}

		if($process){
			$p = new RMProcess();
			$p->fetchById($process);
			if($p){
				$maPath[] = $p->getFieldByAlias("process_name")->getValue();
			}
		}else{
			$maPath[]=FWDLanguage::getPHPStringValue('tt_processes_bl','Processos');
		}

		$maPath[]=ISMSLib::getIconCode('',-1);
		$maPath[]="</a>";

		$maPath[] = "<a href='javascript:go_to_nav_process()'>";

		$sceneName = $this->getFieldByAlias("scene_name")->getValue();
		if($sceneName){
			$maPath[] = $sceneName;
		}else{
			$maPath[] = FWDLanguage::getPHPStringValue('tt_scenes_bl','Cen�rios');
		}

		$maPath[]=ISMSLib::getIconCode('',-1);
		$maPath[] = "</a>";

		return $maPath;

	}
	*/

	public function getSystemPathScroll($piTab,$piContextType,$context, $title){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		$contextIds = explode(":", $context);

		$sceneId = 0;
		$processId = 0;
		$areaId = 0;
		$placeId = 0;

		$idsCount = count($contextIds);
		if($idsCount >= 1)
			$sceneId = $contextIds[0];

		if($idsCount >= 2)
			$processId = $contextIds[1];

		if($idsCount >= 3)
			$areaId = $contextIds[2];

		if($idsCount >= 4)
			$placeId = $contextIds[3];

		$maPath = array();

		if($placeId){

			$place = new CMPlace();

		  	// Sub-Locais
			$placeParents = array_reverse($place->getPlaceParents($placeId));
			$parentsTotal = count($placeParents);

			$maPath = array();

			for($i=0; $i<$parentsTotal; $i++){

	    		$cmPlace = new CMPlace();
	    		$parentPlaceId = $placeParents[$i];

	    		$cmPlace->fetchById($parentPlaceId);

	    		if($i == 0) {
	    			$maPath[] = "<a href='javascript:isms_redirect_to_mode_scroll($piTab, " . CONTEXT_CM_PLACE . ", 0)'>";
			    } else {

			    	$returnPlaceId = $placeParents[$i - 1];
			    	$maPath[] = "<a href='javascript:enterPlace($returnPlaceId)'>";
			   	}

			   	$maPath[] = $cmPlace->getName();
			   	$maPath[] = "";
			   	$maPath[] = "</a>";

			}

			// Se j� estivermos num path, mudar o link. Se estivermos apontando para 1 item no path, esse volta para a Raiz.
			if($parentsTotal){
		   		$returnPlaceId = $placeParents[$parentsTotal-1];
		   		$maPath[] = "<a href='javascript:enterPlace($returnPlaceId)'>";
		   	} else {
		   		$maPath[] = "<a href='javascript:isms_redirect_to_mode_scroll($piTab, ". CONTEXT_CM_PLACE . ", 0)'>";
		   	}

		   	$cmPlace = new CMPlace();
		   	$cmPlace->fetchById($placeId);
		   	
		   	$maPath[] = $cmPlace->getName();
			$maPath[] = "";
			$maPath[] = "</a>";

	   	}

	   	if($areaId){
			$area = new RMArea();
			$area->fetchById($areaId);
			$areaName = $area->getName();

		    $maPath[] = "<a href='javascript:isms_redirect_to_mode_scroll($piTab, ". CONTEXT_AREA . ", $placeId)'>";
		   	$maPath[] = $areaName;
		   	$maPath[] = '';
		   	$maPath[] = "</a>";
	   	}

	   	if($processId){
			$process = new RMProcess();
			$process->fetchById($processId);
			$processName = $process->getName();

		    $maPath[] = "<a href='javascript:isms_redirect_to_mode_scroll($piTab, " . CONTEXT_PROCESS . ", \\\"$areaId:$placeId\\\")'>";
		   	$maPath[] = $processName;
		   	$maPath[] = '';
		   	$maPath[] = "</a>";
		}

		if($sceneId){
			$scene = new CMScene();
			$scene->fetchById($sceneId);
			$sceneName = $scene->getName();

			$maPath[] = "<a href='javascript:isms_redirect_to_mode_scroll($piTab, " . CONTEXT_CM_SCENE . ", \\\"$processId:$areaId:$placeId\\\")'>";
			$maPath[] = $sceneName;
			$maPath[] = '';
			$maPath[] = "</a>";
	   	}


	    $maPath[] = "<a href='javascript:isms_redirect_to_mode_scroll($piTab, 0, 0)'>";
	   	$maPath[] = $title;
	   	$maPath[] = '';
	   	$maPath[] = "</a>";

		$miIContPath = count($maPath);
		
		for($miI=$miIContPath-1;$miI>0;$miI--){
			if((isset($maPath[$miI-7]))&&($maPath[$miI-7]!=''))
			$maPath[$miI-3] = $maPath[$miI-7];
			$miI=$miI-3;
		}

		return $maPath;
	}
}
?>