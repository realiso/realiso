<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

class CMPlanAction extends ISMSContext {

	public function __construct() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		parent::__construct("cm_plan_action");
		$this->csAliasId = "id";
		$this->ciContextType = CONTEXT_CM_PLAN_ACTION;
		$this->coDataset->addFWDDBField(new FWDDBField("fkContext"      ,"id"				,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkGroup"        ,"group" 			,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkPlanRange"    ,"planRange" 		,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkPlanType"    	,"planType" 		,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkPlan"         ,"plan" 			,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkOtherPlan"    ,"otherPlan" 		,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkAction"       ,"previousAction" 	,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("description"	,"description" 		,DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("resume"			,"resume" 	    	,DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("estimate"		,"estimate" 		,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("estimateFlag"	,"estimateFlag" 	,DB_NUMBER));
	}

	public function getLabel(){ return ''; }
	public function getName(){ return ''; }
	protected function userCanDelete($piContextId){ return true; }
	protected function userCanEdit($piContextId, $piUserResponsible = 0){ return true; }
	protected function userCanInsert(){ return true; }

	public function getSystemPathScroll($piTab, $piContextType, $psContextId){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		
		$maPath = array();
		$paContextId = explode(':', $psContextId);
		$id = 0;

		if((isset($paContextId[0]))&&($paContextId[0])){
			$id = $paContextId[0];
		}

		if($id){
			$plan = new CMPlan();
			$plan->fetchById($id);
			$maPath = $plan->getSystemPathScroll(RISK_MANAGEMENT_MODE, $piContextType, null, '');
			$maPath[] = "<a href='#'>";
			$maPath[] = FWDLanguage::getPHPStringValue('tt_plan_action_bl', 'A��es');
			$maPath[] = "";
			$maPath[] = "</a>";
		}

		$miIContPath = count($maPath);
		
		for($miI=$miIContPath-1;$miI>0;$miI--){
			if((isset($maPath[$miI-7]))&&($maPath[$miI-7]!=''))
			$maPath[$miI-3] = $maPath[$miI-7];
			$miI=$miI-3;
		}

		return $maPath;
	}
}
?>