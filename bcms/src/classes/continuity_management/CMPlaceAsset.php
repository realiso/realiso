<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */


class CMPlaceAsset extends ISMSContext {
	public function __construct() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		parent::__construct("cm_place_asset");
		$this->csAliasId = "place_asset_id";
		$this->ciContextType = CONTEXT_CM_PLACE_ASSET;
		$this->coDataset->addFWDDBField(new FWDDBField("fkContext"             ,"place_asset_id"	   	,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkplace"	           ,"place_id"             	,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkasset" 	           ,"asset_id"	       		,DB_NUMBER));
	}

	public function getLabel(){ return ''; }
	public function getName(){ return ''; }
	protected function userCanDelete($piContextId){ return true; }
	protected function userCanEdit($piContextId,$piUserResponsible = 0){ return true; }
	protected function userCanInsert(){ return true; }

	public function insertPlaces($piAssetId, $paPlaces){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		foreach($paPlaces as $miPlaceId){
			$pa = new CMPlaceAsset();
			$pa->setFieldValue('place_id', $miPlaceId);
			$pa->setFieldValue('asset_id', $piAssetId);
			$pa->insert();
		}
	}

	public function deletePlaces($piAssetId, $paPlaces){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		foreach($paPlaces as $miPlaceId){
			parent::delete($this->getPlaceAssetId($miPlaceId, $piAssetId), true);
		}
	}

	public function insertAssets($placeId, $assets){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		foreach($assets as $assetId){
			$pa = new CMPlaceAsset();
			$pa->setFieldValue('place_id', $placeId);
			$pa->setFieldValue('asset_id', $assetId);
			$pa->insert();
		}
	}

	public function deleteAssets($placeId, $assets){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		foreach($assets as $assetId){
			parent::delete($this->getPlaceAssetId($placeId, $assetId), true);
		}
	}

	public function getPlaceAssetId($piPlaceId, $piAssetId) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		$moCMPlaceAsset = new CMPlaceAsset();
		$moCMPlaceAsset->createFilter($piPlaceId, 'place_id');
		$moCMPlaceAsset->createFilter($piAssetId, 'asset_id');
		$moCMPlaceAsset->select();
		$moCMPlaceAsset->fetch();
		return $moCMPlaceAsset->getFieldValue('place_asset_id');
	}

}
?>