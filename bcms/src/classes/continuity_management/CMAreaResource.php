<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

class CMAreaResource extends ISMSContext  {

	public function __construct(){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		parent::__construct("cm_area_resource");
		$this->ciContextType = CONTEXT_CM_AREA_RESOURCE;
			
		$this->csAliasId = "id";
			
		$this->coDataset->addFWDDBField(new FWDDBField("fkContext",		"id", 			DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkArea",		"area_id",       	DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkGroup",		"group_id",        DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkResource",	"resource_id",     DB_NUMBER));
	}

	public function getLabel(){ return ''; }
	public function getName(){ return ''; }
	protected function userCanDelete($piContextId){ return true; }
	protected function userCanEdit($piContextId,$piUserResponsible = 0){ return true; }
	protected function userCanInsert(){ return true; }

	public function insertResources($piAreaId, $paResources){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		$this->setFieldValue('area_id', $piAreaId);
		foreach($paResources as $rId){
			$this->setFieldValue('resource_id', $rId);
			parent::insert();
		}
	}

	public function insertGroup($piAreaId, $piGroups){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		$this->setFieldValue('area_id', $piAreaId);
		foreach($piGroups as $gId){
			$this->setFieldValue('group_id', $gId);
			parent::insert();
		}
	}

	public function deleteGroup($piAreaId, $piGroups){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		foreach($piGroups as $gId){
			parent::delete($this->getId(null,$gId, $piAreaId), true);
		}
	}

	public function deleteResources($piAreaId, $paResources){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		foreach($paResources as $rId){
			parent::delete($this->getId($rId, null, $piAreaId), true);
		}
	}

	public function getId($paResources, $paGroups, $piAreaId) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		$areaResource = new CMAreaResource();
		if($paResources){
			$areaResource->createFilter($paResources, 'resource_id');
		}else{
			$areaResource->createFilter($paResources, 'group_id');
		}

		$areaResource->createFilter($piAreaId, 'area_id');
		$areaResource->select();
		$areaResource->fetch();
		return $areaResource->getFieldValue('id');
	}
}
?>