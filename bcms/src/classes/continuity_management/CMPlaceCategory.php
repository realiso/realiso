<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

include_once $handlers_ref . "QueryPlaceCategoryParents.php";

class CMPlaceCategory extends ISMSContext {

	public function __construct(){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		parent::__construct("cm_place_category");
		$this->csAliasId = "place_category_id";

		$this->coDataset->addFWDDBField(new FWDDBField("fkContext",     "place_category_id",           DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkParent",   	"place_category_parent",  DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("sName",         "name",        DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("tDescription",  "description", DB_STRING));
	}

	public function getLabel(){ return ''; }
	public function getName(){

		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		return $this->getFieldValue('name');
	}
	protected function userCanDelete($piContextId){ return true; }
	protected function userCanEdit($piContextId,$piUserResponsible = 0){ return true; }
	protected function userCanInsert(){ return true; }

	public function getSystemPathScroll($piTab,$piContextType,$psContextFunc){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		$msHome = FWDLanguage::getPHPStringValue('st_home','Raiz');
		$maPath = array();
		$maPath[]="<a href='javascript:isms_redirect_to_mode_scroll(" . $piTab . ",".$piContextType.",0)'>";
		$maPath[]=$msHome;
		$maPath[]=ISMSLib::getIconCode('',-1);
		$maPath[]="</a>";
		$miCategoryId = $this->getFieldValue('place_category_id');
		$maCategoryParents = array_reverse($this->getCategoryParents($miCategoryId));
		foreach ($maCategoryParents as $miCategoryParentId) {
			$moCategoryParent = new CMPlaceCategory();
			$moCategoryParent->fetchById($miCategoryParentId);
			$maPath[] = "<a href='javascript:isms_redirect_to_mode_scroll(" . $piTab . "," . $piContextType . ",$miCategoryParentId)'>";
			$maPath[]=$moCategoryParent->getName();
			$maPath[]=ISMSLib::getIconCode('icon-category.gif',-2);
			$maPath[]="</a>";

			unset($moCategoryParent);
		}
		if($psContextFunc)
		$maPath[] = "<a href='javascript:{$psContextFunc}({$miCategoryId});'>";
		else
		$maPath[] = "<a href='javascript:isms_redirect_to_mode_scroll(" . $piTab . "," . $piContextType . ",$miCategoryId)'>";
		$maPath[]=$this->getName();
		$maPath[]=ISMSLib::getIconCode('icon-category.gif',-2);
		$maPath[]="</a>";

		$miIContPath = count($maPath);
		for($miI=$miIContPath-1;$miI>0;$miI--){
			if((isset($maPath[$miI-7]))&&($maPath[$miI-7]!=''))
			$maPath[$miI-3] = $maPath[$miI-7];
			$miI=$miI-3;
		}
		return $maPath;
	}

	public function getCategoryParents($piCategoryId,$pbExecByTrash = false) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		$moQuery = new QueryPlaceCategoryParents(FWDWebLib::getConnection(),$pbExecByTrash);
		$moQuery->setCategory($piCategoryId);
		$moQuery->makeQuery();
		$moQuery->executeQuery();
		return $moQuery->getParentCategories();
	}
}
?>