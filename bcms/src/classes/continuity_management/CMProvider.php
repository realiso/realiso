<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */


class CMProvider extends ISMSContext {
	public function __construct() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		parent::__construct("cm_provider");
		$this->csAliasId = "id";
		$this->ciContextType = CONTEXT_CM_PROVIDER;
		$this->coDataset->addFWDDBField(new FWDDBField("fkContext"			,"id"					,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("name"				,"name"					,DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("description"		,"description"			,DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("contract"			,"contract"				,DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("contractType"		,"contractType"			,DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("sla"				,"sla"					,DB_NUMBER));
	}

	public function getLabel(){ return ''; }
	public function getName(){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		return $this->getFieldValue('name');
	}
	protected function userCanDelete($piContextId){ return true; }
	protected function userCanEdit($piContextId,$piUserResponsible = 0){ return true; }
	protected function userCanInsert(){ return true; }

	public function getSystemPathScroll($piTab, $piContextType, $context){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		$idProvider = 0;
		$area = 0;

		$ids = explode(':',$context);

		if((isset($ids[0])) && ($ids[0])){
			$idProvider = $ids[0];
		}else{
			$idProvider = $this->getFieldByAlias("id")->getValue();
		}

	  if((isset($ids[1])) && ($ids[1])){
			$idArea = $ids[1];
			$area = new RMArea(); //Unidade de Negocio
			$area->fetchById($idArea);
			if(!$area->getName()){
				$area = null;
			}
		}		
		
		$final = FWDLanguage::getPHPStringValue('contacts','Contatos');

		$maPath = array();
		if($area){
			$icon = ISMSLib::getIconCode('',-1);
			$maPath[]="<a href='javascript:goToAreas();'>";
			$maPath[]=$area->getName();
			$maPath[]=$icon;
			$maPath[]="</a>";

			if($idProvider){
				$maPath[]="<a href='javascript:enterProvider(0);'>";
				$maPath[]=$this->getName();
				$maPath[]=ISMSLib::getIconCode('icon-category.gif',-2);
				$maPath[]="</a>";
				$maPath[]="<a href=''>";
				$maPath[]= FWDLanguage::getPHPStringValue('contacts','Contatos');
				$maPath[]=ISMSLib::getIconCode('icon-risk_gray.gif',-2);
				$maPath[]="</a>";
			}else{
				$maPath[]="<a href=''>";
				$maPath[]= FWDLanguage::getPHPStringValue('externalContacts','Contatos Externos');
				$maPath[]='';
				$maPath[]="</a>";
			}
		}else{
			if($idProvider != 0){
				$maPath[]="<a href='javascript:enterProvider(0);'>";
				$maPath[]=$this->getName();
				$maPath[]=ISMSLib::getIconCode('icon-category.gif',-2);
				$maPath[]="</a>";
				$final = FWDLanguage::getPHPStringValue('contacts','Contatos');
			}
			$icon = '';
			if($idProvider == 0){
				$final = FWDLanguage::getPHPStringValue('st_home','Raiz');
				$icon = ISMSLib::getIconCode('',-1);
			}
			$maPath[]="<a href=''>";
			$maPath[]= $final;
			if($idProvider != 0){
				$maPath[]=ISMSLib::getIconCode('icon-risk_gray.gif',-2);
			}else{
				$maPath[]=''.$icon;
			}
			$maPath[]="</a>";
		}

		$miIContPath = count($maPath);
		for($miI=$miIContPath-1;$miI>0;$miI--){
			if((isset($maPath[$miI-7]))&&($maPath[$miI-7]!=''))
			$maPath[$miI-3] = $maPath[$miI-7];
			$miI=$miI-3;
		}

		return $maPath;
	}
}
?>