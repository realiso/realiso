<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */


class CMResource extends ISMSContext {
	public function __construct() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		parent::__construct("cm_resource");
		$this->csAliasId = "resource_id";
		$this->ciContextType = CONTEXT_CM_RESOURCE;
		$this->coDataset->addFWDDBField(new FWDDBField("fkContext"             	,"resource_id" 		   	,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("name"	               	,"resource_name" 	   	,DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("func"	           		,"resource_function"    ,DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("provider"             	,"provider" 		   	,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkdepartment"           ,"department" 		   	,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("comercialNumber"	    ,"comercialNumber"    	,DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("homeNumber"	    		,"homeNumber"    		,DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("celNumber"	    		,"celNumber"    		,DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("nextelNumber"	    	,"nextelNumber"    		,DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("email"	    			,"email"    			,DB_STRING));
	}

	public function getLabel(){ return ''; }
	public function getName(){ return ''; }
	protected function userCanDelete($piContextId){ return true; }
	protected function userCanEdit($piContextId,$piUserResponsible = 0){ return true; }
	protected function userCanInsert(){ return true; }

	public function getSystemPathScroll($piTab, $piContextType, $context, $title){ echo "teste";

		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		$msHome = FWDLanguage::getPHPStringValue('st_home','Raiz');

		$maPath = array();
		$maPath[] = '';
		$maPath[] = $msHome;
		$maPath[] = '';
		$maPath[] = '';	

		return $maPath;
	}

}
?>