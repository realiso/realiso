<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

class CMPlanTest extends ISMSContext {

	public function __construct() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		parent::__construct("cm_plan_test");
		$this->csAliasId = "id";
		$this->ciContextType = CONTEXT_CM_PLAN_TEST;
		$this->coDataset->addFWDDBField(new FWDDBField("fkContext",		"id",					DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkplan",		"plan",					DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("observation",	"observation",			DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("starttime",		"startTime",  			DB_DATETIME));
		$this->coDataset->addFWDDBField(new FWDDBField("endtime",		"endTime",  			DB_DATETIME));
	}

	public function getLabel(){ return ''; }
	public function getName(){ return ''; }
	protected function userCanDelete($piContextId){ return true; }
	protected function userCanEdit($piContextId,$piUserResponsible = 0){ return true; }
	protected function userCanInsert(){ return true; }
	public function getResponsible() {return null;}

	public function getSystemPathScroll($piTab,$piContextType,$psContextId){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		$maPath = array();
		$paContextId = explode(':',$psContextId);
		$id = 0;
		if((isset($paContextId[0]))&&($paContextId[0])){
			$id = $paContextId[0];
		}

		$maPath[]="<a href='javascript:soTabSubManager.changeTab(TAB_PLAN,'nav_plan.php');'>";
		if($id){
			$p = new CMPlan();
			if($id){
				$p->fetchById($id);
			}
			if($p){
				$maPath[] = $p->getFieldByAlias("name")->getValue();
			}
		}

		$maPath[]=ISMSLib::getIconCode('',-1);
		$maPath[]="</a>";

		$maPath[] = "<a href='javascript:goToPlan()'>";
		$maPath[] = FWDLanguage::getPHPStringValue('tests','Testes');

		$maPath[]=ISMSLib::getIconCode('',-1);
		$maPath[] = "</a>";

		return $maPath;
	}
}
?>