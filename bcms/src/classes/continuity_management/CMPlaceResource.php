<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

class CMPlaceResource extends ISMSContext  {

	public function __construct(){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		parent::__construct("cm_place_resource");
		$this->ciContextType = CONTEXT_CM_PLACE_RESOURCE;
			
		$this->csAliasId = "id";
			
		$this->coDataset->addFWDDBField(new FWDDBField("fkContext",		"id", 				DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkPlace",		"place_id",       	DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkGroup",		"group_id",        	DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkResource",	"resource_id",     	DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("cmt",			"cmt",     			DB_NUMBER));
	}

	public function getLabel(){ return ''; }
	public function getName(){ return ''; }
	protected function userCanDelete($piContextId){ return true; }
	protected function userCanEdit($piContextId,$piUserResponsible = 0){ return true; }
	protected function userCanInsert(){ return true; }

	public function insertResources($piPlaceId, $paResources){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		$this->setFieldValue('place_id', $piPlaceId);
		$this->setFieldValue('cmt', 0);
		foreach($paResources as $rId){
			$this->setFieldValue('resource_id', $rId);
			parent::insert();
		}
	}

	public function insertGroup($piPlaceId, $piGroups, $cmt=0){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		$this->setFieldValue('place_id', $piPlaceId);
		foreach($piGroups as $gId){
			$this->setFieldValue('group_id', $gId);
			$this->setFieldValue('cmt', $cmt);
			parent::insert();
		}
	}

	public function deleteGroup($piPlaceId, $piGroups, $cmt=0){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		foreach($piGroups as $gId){
			parent::delete($this->getId(null,$gId, $piPlaceId, $cmt), true);
		}
	}

	public function deleteResources($piPlaceId, $paResources){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		foreach($paResources as $rId){
			$idToRemove = $this->getId($rId, null, $piPlaceId);
			parent::delete($idToRemove, true);
		}
	}

	public function getId($paResources, $paGroups, $piPlaceId, $cmt=0) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		$placeResource = new CMPlaceResource();
		if($paResources){
			$placeResource->createFilter($paResources, 'resource_id');
		}else{
			$placeResource->createFilter($paResources, 'group_id');
		}
		$placeResource->createFilter($cmt, 'cmt');

		$placeResource->createFilter($piPlaceId, 'place_id');
		$placeResource->select();
		$placeResource->fetch();
		//		print_r("___".$piPlaceId."___".$paResources);
		return $placeResource->getFieldValue('id');
	}

	public function insert($pbReturnId = false,$pbExecBySystem = false) {
		if(!$this->getFieldValue('cmt')){
			$this->setFieldValue('cmt', 0);
		}
		parent::insert($pbReturnId, $pbExecBySystem);
	}
}
?>