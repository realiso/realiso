<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */


class CMProcessActivityAsset extends ISMSContext {
	public function __construct() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		parent::__construct("cm_process_activity_asset");
		$this->csAliasId = "activity_asset_id";
		$this->ciContextType = CONTEXT_CM_PROCESS_ACTIVITY_ASSET;
		$this->coDataset->addFWDDBField(new FWDDBField("fkContext"             ,"activity_asset_id"			   ,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkActivity"      	   ,"activity_id"			 	   ,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkAsset"     	   	   ,"asset_id"				 	   ,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkImportance"  	   	   ,"asset_importance"		 	   ,DB_NUMBER));
	}

	public function insertAssets($piActivityId, $paAssets){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		$this->setFieldValue('activity_id', $piActivityId);
		foreach($paAssets as $miAssetId){
			$this->setFieldValue('asset_id', $miAssetId);
			parent::insert();
		}
	}

	public function deleteAssets($piActivityId, $paAssets){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		foreach($paAssets as $miAssetId){
			parent::delete($this->getActivityAssetId($piActivityId, $miAssetId), true);
		}
	}

	public function getActivityAssetId($piActivityId, $piAssetId){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		$moRMActivityAsset = new CMProcessActivityAsset();
		$moRMActivityAsset->createFilter($piActivityId, 'activity_id');
		$moRMActivityAsset->createFilter($piAssetId, 'asset_id');
		$moRMActivityAsset->select();
		$moRMActivityAsset->fetch();
		return $moRMActivityAsset->getFieldValue('activity_asset_id');
	}

	public function getLabel(){ return ''; }
	public function getName(){ return ''; }
	protected function userCanDelete($piContextId){ return true; }
	protected function userCanEdit($piContextId,$piUserResponsible = 0){ return true; }
	protected function userCanInsert(){ return true; }

	public function getSystemPathScroll($piTab,$piContextType,$psContextFunc){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		$msHome = FWDLanguage::getPHPStringValue('st_home','Raiz');
		$maPath = array();

			
		if($psContextFunc){
			$maPath[]="<a href='javascript:{$psContextFunc}(0);'>";
		}else{
			$maPath[]="<a href='javascript:soTabSubManager.changeTab(TAB_PROCESS,'nav_process.php');'>";
		}
		$maPath[]=FWDLanguage::getPHPStringValue('tt_processes_bl','Processos');;
		$maPath[]=ISMSLib::getIconCode('',-1);
		$maPath[]="</a>";

		$maPath[] = "<a href='javascript:go_to_nav_process()'>";
		$maPath[] = FWDLanguage::getPHPStringValue('tt_activity_bl','Atividades');
		$maPath[]=ISMSLib::getIconCode('',-1);
		$maPath[] = "</a>";

		$maPath[] = "<a href='javascript:go_to_nav_activity();'>";
		$maPath[] = FWDLanguage::getPHPStringValue('tt_asset_bl','Ativos');
		$maPath[]=ISMSLib::getIconCode('',-1);
		$maPath[] = "</a>";


		return $maPath;
	}
}
?>