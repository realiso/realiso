<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */


class CMPlaceThreat extends ISMSContext {
	public function __construct() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		parent::__construct("cm_place_threat");
		$this->csAliasId = "place_threat_id";
		$this->ciContextType = CONTEXT_CM_PLACE_THREAT;
		$this->coDataset->addFWDDBField(new FWDDBField("fkContext"             ,"place_threat_id" 			   ,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkplace"	           ,"place_id" 			           ,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkthreat" 	           ,"threat_id" 				   ,DB_NUMBER));
	}

	public function deletePlacesThreat($threatId, $placeId){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		parent::delete($this->getPlaceThreatId($placeId, $threatId), true);
	}

	public function getPlaceThreatId($placeId, $threatId){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		$placeThreat = new CMPlaceThreat();
		$placeThreat->createFilter($placeId, 'place_id');
		$placeThreat->createFilter($threatId, 'threat_id');
		$placeThreat->select();
		$placeThreat->fetch();
		return $placeThreat->getFieldValue('place_threat_id');
	}

	public function getLabel(){ return ''; }
	public function getName(){ return ''; }
	protected function userCanDelete($piContextId){ return true; }
	protected function userCanEdit($piContextId,$piUserResponsible = 0){ return true; }
	protected function userCanInsert(){ return true; }


}
?>