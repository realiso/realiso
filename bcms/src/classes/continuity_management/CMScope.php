<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */


/**
 * Classe CMScope
 *
 * <p>Classe que representa um escopo,</p>
 * @package ISMS
 * @subpackage classes
 */
class CMScope extends ISMSContext {
	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe CMPlaceType.</p>
	 * @access public
	 */
	public function __construct() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		parent::__construct("cm_scope");
		$this->csAliasId = "scope_id";
		$this->ciContextType = CONTEXT_CM_SCOPE;
		$this->coDataset->addFWDDBField(new FWDDBField("fkContext"             ,"scope_id" 			   ,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("sname"	               ,"scope_name"           ,DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("sdescription"          ,"scope_description"    ,DB_STRING));
	}

	public function getLabel(){ return ''; }
	public function getName(){ return $this->getFieldValue('scope_name'); }
	protected function userCanDelete($piContextId){ return true; }
	protected function userCanEdit($piContextId,$piUserResponsible = 0){ return true; }
	protected function userCanInsert(){ return true; }
	public function getResponsible(){ return null; }

	public function getSystemPathScroll($piTab,$piContextType,$psContextFunc){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		if($psContextFunc){
			$maPath[]="<a href='javascript:{$psContextFunc}(0);'>";
		}else{
			$maPath[]="<a href='javascript:soTabSubManager.changeTab(TAB_SCOPE,'nav_scope.php');'>";
		}
		$maPath[]=FWDLanguage::getPHPStringValue('tt_scopes_bl','Escopos');;
		$maPath[]=ISMSLib::getIconCode('',-1);
		$maPath[]="</a>";

		$maPath[] = "<a href='javascript:go_to_nav_scope()'>";
		$maPath[] = FWDLanguage::getPHPStringValue('tt_processes_bl','Processos');
		$maPath[]=ISMSLib::getIconCode('',-1);
		$maPath[] = "</a>";
			
		return $maPath;
	}
}
?>