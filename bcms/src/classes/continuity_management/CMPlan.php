<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

class CMPlan extends ISMSContext {

	public function __construct() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		parent::__construct("cm_plan");
		$this->csAliasId = "id";
		$this->ciContextType = CONTEXT_CM_PLAN;
		$this->coDataset->addFWDDBField(new FWDDBField("fkContext"             	,"id" 		   			,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("name"			       	,"name" 	    		,DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("description"	       	,"description"	 	    ,DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("fkArea"                	,"area" 		   		,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkProcess"             	,"process"	   			,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkAsset"             	,"asset" 		   		,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkPlace"             	,"place" 		   		,DB_NUMBER));
	}

	public function removePlan($id){
		$planActions = new CMPlanAction();
		$planActions->createFilter($id,	'plan');
		$planActions->select();
		while($planActions->fetch()){
			$planActions->delete($planActions->getFieldValue("id"));
		}

		$schedule = new CMPlanSchedule();
		$schedule->createFilter($id,	'plan');
		$schedule->select();
		while($schedule->fetch()){
			$schedule->delete($schedule->getFieldValue("id"));
		}

		$moPlan = new CMPlan();
		$moPlan->delete($id);
	}

	public function getLabel(){ return ''; }
	public function getName(){ return $this->getFieldValue('name'); }
	protected function userCanDelete($piContextId){ return true; }
	protected function userCanEdit($piContextId,$piUserResponsible = 0){ return true; }
	protected function userCanInsert(){ return true; }
	public function getResponsible() {return null;}

	public function getResponsibleId() {
		$areaId = $this->getFieldValue("area");
		if($areaId){
			$area = new RMArea();
				
		}
	}
	
	public function getSystemPathScroll($piTab,$piContextType,$context, $title){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		// Se n�o tiver id, mostra s� a 'Raiz'
		if(!$context){
			$msHome = FWDLanguage::getPHPStringValue('st_home','Raiz');

			$maPath = array();
			$maPath[] = "<a href='javascript:isms_redirect_to_mode_scroll($piTab, ". CONTEXT_CM_PLAN . ", 0)'>";
			$maPath[] = $msHome;
			$maPath[] = "";
			$maPath[] = "</a>";	

			return $maPath;
		}

		$maPath = array();
		$maPath[] = "<a href='javascript:isms_redirect_to_mode_scroll($piTab, ". CONTEXT_CM_PLACE . ", 0)'>";
		$maPath[] = $this->getName();
		$maPath[] = "";
		$maPath[] = "</a>";	

		$miIContPath = count($maPath);
		
		for($miI=$miIContPath-1;$miI>0;$miI--){
			if((isset($maPath[$miI-7]))&&($maPath[$miI-7]!=''))
			$maPath[$miI-3] = $maPath[$miI-7];
			$miI=$miI-3;
		}

		return $maPath;
	}
}

?>