<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */


/**
 * Classe CMScene
 *
 * <p>Classe que representa um escopo,</p>
 * @package ISMS
 * @subpackage classes
 */
class CMProcessActivity extends ISMSContext {
	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe CMPlaceType.</p>
	 * @access public
	 */
	public function __construct() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		parent::__construct("cm_process_activity");
		$this->csAliasId = "activity_id";
		$this->ciContextType = CONTEXT_CM_PROCESS_ACTIVITY;
		$this->coDataset->addFWDDBField(new FWDDBField("fkContext"             ,"activity_id"			   ,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("sname"	               ,"activity_name"		       ,DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("sdescription"          ,"activity_description"     ,DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("fkprocess" 	      	   ,"activity_process"	 	   ,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkgroup"     	   	   ,"group" 	   			   ,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkimportance"     	   ,"importance" 	   			   ,DB_NUMBER));
	}

	public function getLabel(){ return ''; }
	public function getName(){ return $this->getFieldValue("activity_name"); }
	protected function userCanDelete($piContextId){ return true; }
	protected function userCanEdit($piContextId,$piUserResponsible = 0){ return true; }
	protected function userCanInsert(){ return true; }
	public function getResponsible(){ return null; }
	/**
	 * Retorna o caminho do contexto para a cria��o do pathscroll.
	 *
	 * <p>M�todo para retornar o caminho do contexto para a cria��o do pathscroll.</p>
	 * @access public
	 * @param integer $piTab id da tab
	 * @param integer $piContextType id do contexto
	 * @param string $psContextFunc fun��o que deve ser utilizada para gerar o c�digo do link
	 * @return array Array contendo o Caminho do contexto
	 */
	public function getSystemPathScroll($piTab,$piContextType,$context, $title){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		$contextIds = explode(":", $context);

		$activityId = 0;
		$processId = 0;
		$areaId = 0;
		$placeId = 0;

		$idsCount = count($contextIds);
		if($idsCount >= 1)
			$activityId = $contextIds[0];

		if($idsCount >= 2)
			$processId = $contextIds[1];

		if($idsCount >= 3)
			$areaId = $contextIds[2];

		if($idsCount >= 4)
			$placeId = $contextIds[3];

		$maPath = array();

		if($placeId){

			$place = new CMPlace();

		  	// Sub-Locais
			$placeParents = array_reverse($place->getPlaceParents($placeId));
			$parentsTotal = count($placeParents);

			$maPath = array();

			for($i=0; $i<$parentsTotal; $i++){

	    		$cmPlace = new CMPlace();
	    		$parentPlaceId = $placeParents[$i];

	    		$cmPlace->fetchById($parentPlaceId);

	    		if($i == 0) {
	    			$maPath[] = "<a href='javascript:isms_redirect_to_mode_scroll($piTab, " . CONTEXT_CM_PLACE . ", 0)'>";
			    } else {

			    	$returnPlaceId = $placeParents[$i - 1];
			    	$maPath[] = "<a href='javascript:enterPlace($returnPlaceId)'>";
			   	}

			   	$maPath[] = $cmPlace->getName();
			   	$maPath[] = "";
			   	$maPath[] = "</a>";

			}

			// Se j� estivermos num path, mudar o link. Se estivermos apontando para 1 item no path, esse volta para a Raiz.
			if($parentsTotal){
		   		$returnPlaceId = $placeParents[$parentsTotal-1];
		   		$maPath[] = "<a href='javascript:enterPlace($returnPlaceId)'>";
		   	} else {
		   		$maPath[] = "<a href='javascript:isms_redirect_to_mode_scroll($piTab, ". CONTEXT_CM_PLACE . ", 0)'>";
		   	}

		   	$cmPlace = new CMPlace();
		   	$cmPlace->fetchById($placeId);
		   	
		   	$maPath[] = $cmPlace->getName();
			$maPath[] = "";
			$maPath[] = "</a>";
		   	
	   	}

	   	if($areaId){
			$area = new RMArea();
			$area->fetchById($areaId);
			$areaName = $area->getName();

		    $maPath[] = "<a href='javascript:isms_redirect_to_mode_scroll($piTab, ". CONTEXT_AREA . ", $placeId)'>";
		   	$maPath[] = $areaName;
		   	$maPath[] = '';
		   	$maPath[] = "</a>";
	   	}

	   	if($processId){
			$process = new RMProcess();
			$process->fetchById($processId);
			$processName = $process->getName();

		    $maPath[] = "<a href='javascript:isms_redirect_to_mode_scroll($piTab, " . CONTEXT_PROCESS . ", \\\"$areaId:$placeId\\\")'>";
		   	$maPath[] = $processName;
		   	$maPath[] = '';
		   	$maPath[] = "</a>";
		}

		if($activityId){
			$activity = new CMProcessActivity();
			$activity->fetchById($activityId);
			$activityName = $activity->getName();

			$maPath[] = "<a href='javascript:isms_redirect_to_mode_scroll($piTab, " . CONTEXT_CM_PROCESS_ACTIVITY . ", \\\"$processId:$areaId:$placeId\\\")'>";
			$maPath[] = $activityName;
			$maPath[] = '';
			$maPath[] = "</a>";
	   	}


	    $maPath[] = "<a href='javascript:isms_redirect_to_mode_scroll($piTab, 0, 0)'>";
	   	$maPath[] = $title;
	   	$maPath[] = '';
	   	$maPath[] = "</a>";

		$miIContPath = count($maPath);
		
		for($miI=$miIContPath-1;$miI>0;$miI--){
			if((isset($maPath[$miI-7]))&&($maPath[$miI-7]!=''))
			$maPath[$miI-3] = $maPath[$miI-7];
			$miI=$miI-3;
		}

		return $maPath;
	}

	/*
	 * Pega os ativos da atividade
	 */
	private function getAssetFromActivity($contextId){
	    $maParameters = func_get_args();
	    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

	    $cmProcessActivityAsset = new CMProcessActivityAsset();
	    $cmProcessActivityAsset->createFilter($contextId, "activity_id");
	    $cmProcessActivityAsset->select();

	    $assets = array();
	    while($cmProcessActivityAsset->fetch()){
	    	$assets[] = $cmProcessActivityAsset->getFieldValue("asset_id");
	    }

	    if(count($assets))
	    	return $assets;
	   	else
	   		return null;
	}	

	/**
	 * Retorna os sub-contextos de uma �rea (sub-�reas/processos).
	 *
	 * <p>M�todo para retornar os sub-contextos de uma �rea (sub-�reas/processos).</p>
	 * @access public
	 * @param integer $piAreaId Id da �rea
	 * @return array Array de ids dos sub-contextos desta �rea
	 */
	public function verifySubContexts($contextId) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		$assets = $this->getAssetFromActivity($contextId);

		if($assets)
			return true;		
		
		return false;			
	}

	/**
	 * Indica se o contexto � delet�vel ou n�o.
	 *
	 * <p>M�todo que indica se o contexto � delet�vel ou n�o.</p>
	 * @access public
	 * @param integer $piContextId id do contexto
	 */
	public function isDeletable($piContextId) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		if (!ISMSLib::getConfigById(GENERAL_CASCADE_ON)) {
			// cascade off: verificar exist�ncia de subcontextos
			if ($this->verifySubContexts($piContextId))
			return false;
		}
		return true;
	}

	/**
	 * Exibe uma popup caso n�o seja poss�vel remover um contexto.
	 *
	 * <p>M�todo que exibe uma popup caso n�o seja poss�vel remover um contexto.</p>
	 * @access public
	 * @param integer $piContextId id do contexto
	 */
	public function showDeleteError($piContextId) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		$msTitle = FWDLanguage::getPHPStringValue('tt_area_remove_erorr','Erro ao remover Atividade');
		$msMessage = FWDLanguage::getPHPStringValue('st_area_remove_error_message',"N�o foi poss�vel remover o Atividade <b>%activity_name%</b>: A Atividade cont�m Ativos. Para remover, ative a op��o \"Delete Cascade\" na aba \"Admin -> Configura��o\".");
		$cmProcessActivity = new CMProcessActivity();
		$cmProcessActivity->fetchById($piContextId);
		$msActivityName = $cmProcessActivity->getFieldValue('activity_name');
		$msMessage = str_replace("%activity_name%",$msActivityName,$msMessage);
		ISMSLib::openOk($msTitle,$msMessage,"",80);
	}

	/**
	 * Deleta logicamente um contexto.
	 *
	 * <p>M�todo para deletar logicamente um contexto.</p>
	 * @access public
	 * @param integer $piContextId id do contexto
	 */
	public function logicalDelete($piContextId) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

	    $assets = null;
	    $assets = $this->getAssetFromActivity($piContextId);

	    if($assets) {
		    foreach($assets as $value){
		    	$rmAsset = new RMAsset();
		    	$rmAsset->delete($value);
		    }
		}

	}

}
?>