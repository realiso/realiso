<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */


/**
 * Classe CMImpact
 *
 * <p>Classe que representa um impacto de um cenario</p>
 * @package ISMS
 * @subpackage classes
 */
class CMImpactDamage extends ISMSContext {
	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe CMPlaceType.</p>
	 * @access public
	 */
	public function __construct() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		parent::__construct("cm_impact_damage");
		$this->csAliasId = "impact_damage_id";
		$this->ciContextType = CONTEXT_CM_IMPACT_DAMAGE;
		$this->coDataset->addFWDDBField(new FWDDBField("fkContext"          ,"impact_damage_id"					,DB_NUMBER));//Chave prim�ria
		$this->coDataset->addFWDDBField(new FWDDBField("fkimpact"	        ,"impact_id"						,DB_NUMBER));//Chave estrangeira -> cm_impact -> Impacto
		$this->coDataset->addFWDDBField(new FWDDBField("fkscene"	        ,"scene_id"							,DB_NUMBER));//Chave estrangeira -> cm_scene -> cen�rio
	}

	public function getLabel(){ return ''; }
	public function getName(){
		if($this->getFieldValue("impact_id")){
			$impact = new CMImpact();
			$impact->fetchById($this->getFieldValue("impact_id"));
			$return = $impact->getFieldValue("impact_name");
			if($this->getFieldValue("scene_id")){
				$return = ISMSLib::truncateString($impact->getFieldValue("impact_name"), 35);
				$scene = new CMScene();
				$scene->fetchById($this->getFieldValue("scene_id"));
				$return .= FWDLanguage::getPHPStringValue('in_scene'," no cen�rio ").ISMSLib::truncateString($scene->getFieldValue("scene_name"), 35);
			}
			return $return;
		}
		return '';
	}
	public function getResponsible(){ return null; }
	protected function userCanDelete($piContextId){ return true; }
	protected function userCanEdit($piContextId,$piUserResponsible = 0){ return true; }
	protected function userCanInsert(){ return true; }


	public function getSystemPathScroll($piTab,$piContextType,$psContextFunc){

		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		$msHome = FWDLanguage::getPHPStringValue('st_home','Raiz');
		$maPath = array();

		$paContextId = explode(':',$psContextFunc);

		$sceneId = 0;
		if((isset($paContextId[0]))&&($paContextId[0])){
			$sceneId = $paContextId[0];
		}

		if($sceneId){
			$scene = new CMScene();
			$scene->fetchById($sceneId);
			$maPath = $scene->getSystemPathScroll(RISK_MANAGEMENT_MODE,CONTEXT_CM_SCENE,'');

			$maPath[] = "<a href='javascript:go_to_nav_scenes({$scene->getFieldByAlias("scene_process")->getValue()});'>";

			$maPath[] = FWDLanguage::getPHPStringValue('tt_impacts_bl','Impactos');
			$maPath[]=ISMSLib::getIconCode('',-1);
			$maPath[] = "</a>";
		}else{
			if($psContextFunc){
				$maPath[]="<a href='javascript:{$psContextFunc}(0);'>";
			}else{
				$maPath[]="<a href='javascript:soTabSubManager.changeTab(TAB_PROCESS,'nav_process.php');'>";
			}
			$maPath[]=FWDLanguage::getPHPStringValue('tt_processes_bl','Processos');;
			$maPath[]=ISMSLib::getIconCode('',-1);
			$maPath[]="</a>";

			$maPath[] = "<a href='javascript:go_to_nav_process()'>";
			$maPath[] = FWDLanguage::getPHPStringValue('tt_scenes_bl','Cen�rios');
			$maPath[]=ISMSLib::getIconCode('',-1);
			$maPath[] = "</a>";


			$maPath[] = "<a href='javascript:go_to_nav_scenes({$psContextFunc});'>";

			$maPath[] = FWDLanguage::getPHPStringValue('tt_impacts_bl','Impactos');
			$maPath[]=ISMSLib::getIconCode('',-1);
			$maPath[] = "</a>";
		}
		return $maPath;
	}
}
?>