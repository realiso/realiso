<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */


class CMDamageMatrixParameter extends ISMSContext {

	public function __construct() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		parent::__construct("cm_damage_matrix_parameter");
		$this->csAliasId = "damage_matrix_parameter_id";
		$this->ciContextType = CONTEXT_CM_PLACE;
		$this->coDataset->addFWDDBField(new FWDDBField("fkContext"             ,"damage_matrix_parameter_id" 			   ,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("sname"	               ,"damage_matrix_parameter_name"             ,DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("nweight"	           ,"damage_matrix_parameter_weight"           ,DB_NUMBER));
	}

	public function getLabel(){ return ''; }
	public function getName(){ return ''; }
	protected function userCanDelete($piContextId){ return true; }
	protected function userCanEdit($piContextId,$piUserResponsible = 0){ return true; }
	protected function userCanInsert(){ return true; }


	public function getDamageParameters(){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		$moCMDamageMatrixParameter = new CMDamageMatrixParameter();
		$moCMDamageMatrixParameter->setOrderBy("damage_matrix_parameter_weight","+");
		$moCMDamageMatrixParameter->select();

		$moMatrix = array();
		while($moCMDamageMatrixParameter->fetch()) {
			$moMatrix[$moCMDamageMatrixParameter->getFieldValue('damage_matrix_parameter_id')] = $moCMDamageMatrixParameter->getFieldValue('damage_matrix_parameter_name');
		}

		return($moMatrix);

	}

	public function getLowestDamageParameter(){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		$moCMDamageMatrixParameter = new CMDamageMatrixParameter();
		$moCMDamageMatrixParameter->setOrderBy("damage_matrix_parameter_weight","+");
		$moCMDamageMatrixParameter->select();

		while($moCMDamageMatrixParameter->fetch()) {
			return $moCMDamageMatrixParameter->getFieldValue('damage_matrix_parameter_id');
		}
	}
}
?>