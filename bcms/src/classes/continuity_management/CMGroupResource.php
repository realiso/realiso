<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

class CMGroupResource extends ISMSContext {

	public function __construct() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		parent::__construct("cm_group_resource");
		$this->csAliasId = "group_resource_id";
		$this->ciContextType = CONTEXT_CM_GROUP_RESOURCE;
		$this->coDataset->addFWDDBField(new FWDDBField("fkContext"			,"group_resource_id"		,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("escalation"			,"escalation"				,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkgroup"			,"group_id"					,DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("fkresource"			,"resource_id"				,DB_STRING));
	}

	public function getLabel(){ return ''; }
	public function getName(){ return ''; }
	protected function userCanDelete($piContextId){ return true; }
	protected function userCanEdit($piContextId,$piUserResponsible = 0){ return true; }
	protected function userCanInsert(){ return true; }

	public function insertResources($piGroupId, $paResources){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		$gr = new CMGroupResource();
		$gr->setFieldValue('group_id', $piGroupId);
		foreach($paResources as $rId){
			$gr->setFieldValue('resource_id', $rId);
			$gr->insert();
		}
	}

	public function orderResources($resources,$groupId){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		$i = 0;

		foreach($resources as $res){
			$gr = new CMGroupResource();
			$gr->createFilter($res, 'resource_id');
			$gr->createFilter($groupId, 'group_id');
			$gr->select();
			while($gr->fetch()){
				$gr->setFieldValue('escalation',$i++);
				$gr->update($gr->getFieldValue('group_resource_id'));
			}
		}

	}

	public function insertGroup($paResource, $piGroups){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		$this->setFieldValue('resource_id', $paResource);
		foreach($piGroups as $gId){
			$this->setFieldValue('group_id', $gId);
			parent::insert();
		}
	}

	public function deleteGroup($paResource, $piGroups){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		foreach($piGroups as $gId){
			parent::delete($this->getGroupResourceId($paResource, $gId), true);
		}
	}

	public function deleteResources($piGroupId, $paResources){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		foreach($paResources as $rId){
			parent::delete($this->getGroupResourceId($rId, $piGroupId), true);
		}
	}

	public function getGroupResourceId($paResources, $piGroupId) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		$groupResource = new CMGroupResource();
		$groupResource->createFilter($paResources, 'resource_id');
		$groupResource->createFilter($piGroupId, 'group_id');
		$groupResource->select();
		$groupResource->fetch();
		return $groupResource->getFieldValue('group_resource_id');
	}

}
?>