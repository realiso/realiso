<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

class CMScopePlace extends ISMSContext {
	public function __construct() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		parent::__construct("cm_scope_place");
		$this->csAliasId = "scope_place_id";
		$this->ciContextType = CONTEXT_CM_SCOPE_PLACE;
		$this->coDataset->addFWDDBField(new FWDDBField("fkContext"             ,"scope_place_id"	   ,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkscope"	           ,"scope_id"             ,DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("fkplace" 	           ,"place_id"	       ,DB_STRING));
	}

	public function getLabel(){ return ''; }
	public function getName(){ return ''; }
	protected function userCanDelete($piContextId){ return true; }
	protected function userCanEdit($piContextId, $piUserResponsible = 0){ return true; }
	protected function userCanInsert(){ return true; }

	public function insertPlaces($scopeId, $places){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		$this->setFieldValue('scope_id', $scopeId);
		foreach($places as $placeId){
			$this->setFieldValue('place_id', $placeId);
			parent::insert();
		}
	}

	public function deletePlaces($scopeId, $places){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		foreach($places as $placeId){
			parent::delete($this->getScopePlaceId($scopeId, $placeId), true);
		}
	}

	public function getScopePlaceId($scopeId, $placeId) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		$moCMScopePlace = new CMScopePlace();
		$moCMScopePlace->createFilter($scopeId, 'scope_id');
		$moCMScopePlace->createFilter($placeId, 'place_id');
		$moCMScopePlace->select();
		$moCMScopePlace->fetch();
		return $moCMScopePlace->getFieldValue('scope_place_id');
	}

	public function getSystemPathScroll($piTab,$piContextType,$psContextId){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		$maPath = array();
		$id = $psContextId;
		if($id){
			$p = new CMScope();
			$p->fetchById($id);
			if($p){
				$maPath[]="<a href='javascript:goToScope();'>";
				$maPath[] = $p->getFieldByAlias("scope_name")->getValue();
				$maPath[]=ISMSLib::getIconCode('',-1);
				$maPath[]="</a>";
				$maPath[]="<a href='javascript:goToScope();'>";
			}
		}
		$maPath[] = FWDLanguage::getPHPStringValue('tt_asset_bl','Locais');
		$maPath[]=ISMSLib::getIconCode('',-1);
		$maPath[] = "</a>";
		return $maPath;
	}

}
?>