<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

class CMProviderResource extends ISMSContext {

	public function __construct() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		parent::__construct("cm_provider_resource");
		$this->csAliasId = "provider_resource_id";
		$this->ciContextType = CONTEXT_CM_PROVIDER_RESOURCE;
		$this->coDataset->addFWDDBField(new FWDDBField("fkContext"			,"provider_resource_id"		,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkprovider"			,"id"					,DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("fkresource"			,"resource_id"				,DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("bisdefault"			,"is_default"				, DB_NUMBER));
	}

	public function getLabel(){ return ''; }
	public function getName(){ return ''; }
	protected function userCanDelete($piContextId){ return true; }
	protected function userCanEdit($piContextId,$piUserResponsible = 0){ return true; }
	protected function userCanInsert(){ return true; }
		
}
?>