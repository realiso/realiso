<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */


class CMGroup extends ISMSContext {
	public function __construct() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		parent::__construct("cm_group");
		$this->csAliasId = "group_id";
		$this->ciContextType = CONTEXT_CM_GROUP;
		$this->coDataset->addFWDDBField(new FWDDBField("fkContext"             ,"group_id"			,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkPriority"            ,"group_priority"	,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("name"	               ,"group_name"		,DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("acronym" 		       ,"group_acronym"		,DB_STRING));
	}

	public function getLabel(){ return ''; }
	public function getName(){

		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		return $this->getFieldValue('group_name');
	}
	protected function userCanDelete($piContextId){ return true; }
	protected function userCanEdit($piContextId,$piUserResponsible = 0){ return true; }
	protected function userCanInsert(){ return true; }

	public function getSystemPathScroll($piTab,$piContextType,$context){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		$idGroup = 0;
		$idArea = 0;
		$idPlace = 0;
		$cmt = 0;

		$ids = explode(':',$context);

		if((isset($ids[1])) && ($ids[1])){
			$idArea = $ids[1];
		}else if((isset($ids[2])) && ($ids[2])){
			$idPlace = $ids[2];
			$cmt = $ids[3];
		}

		if((isset($ids[0])) && ($ids[0])){
			$idGroup = $ids[0];
		}else{
			$idGroup = $this->getFieldByAlias("group_id")->getValue();
		}

		$final = FWDLanguage::getPHPStringValue('resources','Recursos');

		$maPath = array();
		if($idPlace){
			$place = new CMPlace();
			$place->fetchById($idPlace);
			$maPath[]="<a href='javascript:goToPlaces();'>";
			$maPath[]=$place->getFieldValue('place_name');
			$maPath[]=ISMSLib::getIconCode('',-1);
			$maPath[]="</a>";
			if($cmt){
				$final = FWDLanguage::getPHPStringValue('CMT','CMT');
			}else{
				$final = FWDLanguage::getPHPStringValue('contacts','Contatos');
			}
		} else if($idArea){
			$area = new RMArea();
			$area->fetchById($idArea);
			$maPath[]="<a href='javascript:goToArea();'>";
			$maPath[]=$area->getFieldValue('area_name');
			$maPath[]=ISMSLib::getIconCode('',-1);
			$maPath[]="</a>";
			$final = FWDLanguage::getPHPStringValue('contacts','Contatos');
		}
		if($idGroup != 0){
			$maPath[]="<a href='javascript:enterGroup(0);'>";
			$maPath[]=$this->getName();
			$maPath[]=ISMSLib::getIconCode('icon-category.gif',-2);
			$maPath[]="</a>";
			$final = FWDLanguage::getPHPStringValue('resources','Recursos');
		}
		$icon = '';
		if($idGroup == 0 && $idArea == 0 && $idPlace == 0){
			$final = FWDLanguage::getPHPStringValue('st_home','Raiz');
			$icon = ISMSLib::getIconCode('',-1);
		}
		$maPath[]="<a href=''>";
		$maPath[]= $final;
		if($idGroup != 0){
			$maPath[]=ISMSLib::getIconCode('icon-risk_gray.gif',-2);
		}else{
			$maPath[]=''.$icon;
		}
		$maPath[]="</a>";

		$miIContPath = count($maPath);
		for($miI=$miIContPath-1;$miI>0;$miI--){
			if((isset($maPath[$miI-7]))&&($maPath[$miI-7]!=''))
			$maPath[$miI-3] = $maPath[$miI-7];
			$miI=$miI-3;
		}

		return $maPath;
	}

	public function getCMT($place){
		$query = new FWDDBDataSet(FWDWebLib::getConnection());
		$query->setQuery("select fkgroup as id from view_cm_place_resource_active where cmt = 1 and fkplace = {$place}");
		$query->addFWDDBField(new FWDDBField('id', 'id', DB_NUMBER));
		$query->execute();
		$groupCMT = null;
		$groupId = null;
		while($query->fetch()){
			$groupCMT = new CMGroup();
			$groupId = $query->getFieldByAlias("id")->getValue();
			$groupCMT->setFieldValue('group_id', $groupId);
		}
		if($groupCMT == null){
			$groupCMT = new CMGroup();
			$placeObj = new CMPlace();
			$placeObj->fetchById($place);
			$groupCMT->setFieldValue("group_name", "CMT - {$placeObj->getFieldValue('place_name')}");
			$groupCMT->setFieldValue("group_acronym", "CMT");
			$groupId = $groupCMT->insert(true);
			$groupCMT->setFieldValue("group_id", $groupId);

			$maToInsert = array();
			$maToInsert[] = $groupId;
			$placeResource = new CMPlaceResource();
			$placeResource->insertGroup($place,$maToInsert,1);
		}
		return $groupCMT;
	}
}
?>