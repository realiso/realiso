<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
 
class CMPlaceProcess extends ISMSContext {
  public function __construct() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    parent::__construct("cm_place_process");
    $this->csAliasId = "place_process_id";
    $this->ciContextType = CONTEXT_CM_SCOPE_PROCESS;
    $this->coDataset->addFWDDBField(new FWDDBField("fkContext"             ,"place_process_id"	   ,DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("fkplace"	           ,"place_id"             ,DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField("fkprocess" 	           ,"process_id"	       ,DB_STRING));
  }

  public function getLabel(){ return ''; }
  public function getName(){ return ''; }
  protected function userCanDelete($piContextId){ return true; }
  protected function userCanEdit($piContextId,$piUserResponsible = 0){ return true; }
  protected function userCanInsert(){ return true; }
  
  public function insertPlaces($piProcessId, $paPlaces){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $this->setFieldValue('process_id', $piProcessId);
    foreach($paPlaces as $miPlaceId){
      $this->setFieldValue('place_id', $miPlaceId);
      parent::insert();
    }
  }
  
  public function deletePlaces($piProcessId, $paPlaces){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    foreach($paPlaces as $miPlaceId){
      parent::delete($this->getPlaceProcessId($miPlaceId, $piProcessId), true);
    }
  }
  
  public function getPlaceProcessId($piPlaceId, $piProcessId) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $moCMPlaceProcess = new CMPlaceProcess();
    $moCMPlaceProcess->createFilter($piPlaceId, 'place_id');
    $moCMPlaceProcess->createFilter($piProcessId, 'process_id');
    $moCMPlaceProcess->select();
    $moCMPlaceProcess->fetch();
    return $moCMPlaceProcess->getFieldValue('place_process_id');
  }

}
?>