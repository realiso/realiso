<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */


class CMImpactDamageRange extends ISMSContext {

	public function __construct() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		parent::__construct("cm_impact_damage_range");
		$this->csAliasId = "impact_damage_range_id";
		$this->ciContextType = CONTEXT_CM_IMPACT_DAMAGE_RANGE;
		$this->coDataset->addFWDDBField(new FWDDBField("fkContext"          ,"impact_damage_range_id"			,DB_NUMBER));//Chave prim�ria
		$this->coDataset->addFWDDBField(new FWDDBField("fkimpactdamage"     ,"impact_damage_id"					,DB_NUMBER));//Chave estrangeira -> cm_impact_damage -> Impacto por cen�rio
		$this->coDataset->addFWDDBField(new FWDDBField("fkdamage"	        ,"impact_damage_damage_id"			,DB_NUMBER));//Chave estrangeira -> cm_damage_matrix -> Matriz de danos
		$this->coDataset->addFWDDBField(new FWDDBField("fkdamageparameter"  ,"impact_damage_parameter_id"		,DB_NUMBER));//Chave estrangeira -> cm_damage_matrix_parameter -> Criticidade
		$this->coDataset->addFWDDBField(new FWDDBField("sfinancialimpact"  	,"impact_damage_financial_impact"	,DB_STRING));//Texto do impacto financeiro
		$this->coDataset->addFWDDBField(new FWDDBField("financialimpact"  	,"financial_impact"	, DB_NUMBER));
	}

	public function getLabel(){ return ''; }
	public function getName(){ return ''; }
	protected function userCanDelete($piContextId){ return true; }
	protected function userCanEdit($piContextId,$piUserResponsible = 0){ return true; }
	protected function userCanInsert(){ return true; }

	public function getDamages($idImpactDamage) {
		$moCMImpactDamage = new CMImpactDamageRange();
		$moCMImpactDamage->createFilter($idImpactDamage, 'impact_damage_id');
		$moCMImpactDamage->select();

		$moDamages = array();

		while($moCMImpactDamage->fetch()) {
			$moDamages[$moCMImpactDamage->getFieldValue('impact_damage_damage_id')] = $moCMImpactDamage->getFieldValue('impact_damage_parameter_id');
		}

		return($moDamages);
	}

	public function getFinancialImpacts($idImpactDamage) {
		$moCMImpactDamage = new CMImpactDamageRange();
		$moCMImpactDamage->createFilter($idImpactDamage, 'impact_damage_id');
		$moCMImpactDamage->select();

		$moImpacts = array();

		while($moCMImpactDamage->fetch()) {
			$moImpacts[$moCMImpactDamage->getFieldValue('impact_damage_damage_id')] = $moCMImpactDamage->getFieldValue('financial_impact');
		}

		return $moImpacts;
	}

}
?>