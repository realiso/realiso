<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */


class CMProcessThreat extends ISMSContext {
	public function __construct() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		parent::__construct("cm_process_threat");
		$this->csAliasId = "process_threat_id";
		$this->ciContextType = CONTEXT_CM_PROCESS_THREAT;
		$this->coDataset->addFWDDBField(new FWDDBField("fkContext"             ,"process_threat_id"			   ,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkprocess" 	      	   ,"process_id"			 	   ,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkthreat"    	 	   ,"threat"				 	   ,DB_NUMBER));
	}

	public function getLabel(){ return ''; }
	public function getName(){ return ''; }
	protected function userCanDelete($piContextId){ return true; }
	protected function userCanEdit($piContextId,$piUserResponsible = 0){ return true; }
	protected function userCanInsert(){ return true; }

	/**
	 * Retorna o caminho do contexto para a cria��o do pathscroll.
	 *
	 * <p>M�todo para retornar o caminho do contexto para a cria��o do pathscroll.</p>
	 * @access public
	 * @param integer $piTab id da tab
	 * @param integer $piContextType id do contexto
	 * @param string $psContextFunc fun��o que deve ser utilizada para gerar o c�digo do link
	 * @return array Array contendo o Caminho do contexto
	 */
	public function getSystemPathScroll($piTab,$piContextType,$psContextFunc){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		$msHome = FWDLanguage::getPHPStringValue('st_home','Raiz');
		$maPath = array();

		if($psContextFunc){
			$maPath[]="<a href='javascript:{$psContextFunc}(0);'>";
		}else{
			$maPath[]="<a href='javascript:soTabSubManager.changeTab(TAB_PROCESS,'nav_process.php');'>";
		}
		
		$maPath[]=FWDLanguage::getPHPStringValue('tt_processes_bl','Processos');
		$maPath[]=ISMSLib::getIconCode('',-1);
		$maPath[]="</a>";

		$maPath[] = "<a href='javascript:go_to_nav_process()'>";
		$maPath[] = FWDLanguage::getPHPStringValue('tt_activity_bl','Amea�as');
		$maPath[]=ISMSLib::getIconCode('',-1);
		$maPath[] = "</a>";
			

		return $maPath;
	}

	public function deleteProcessThreat($threatId, $processId){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		parent::delete($this->getProcessThreatId($processId, $threatId), true);
	}

	public function getProcessThreatId($processId, $threatId){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		$processThreat = new CMProcessThreat();
		$processThreat->createFilter($processId, 'process_id');
		$processThreat->createFilter($threatId, 'threat');
		$processThreat->select();
		$processThreat->fetch();
		return $processThreat->getFieldValue('process_threat_id');
	}

}
?>