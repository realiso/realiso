<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

class CMPlanRange extends ISMSContext {

	public function __construct() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		parent::__construct("cm_plan_range");
		$this->csAliasId = "id";
		$this->ciContextType = CONTEXT_CM_PLAN_RANGE;
		$this->coDataset->addFWDDBField(new FWDDBField("fkContext"          ,"id" 		   		,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("startRange"       ,"startRange" 		,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("startRangeFlag"   ,"startRangeFlag" 	,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("endRange"         ,"endRange" 		,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("endRangeFlag"     ,"endRangeFlag" 	,DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("description"	    ,"description" 	    ,DB_STRING));
			
	}

	public function getLabel(){ return ''; }
	public function getName(){ return ''; }
	protected function userCanDelete($piContextId){ return true; }
	protected function userCanEdit($piContextId,$piUserResponsible = 0){ return true; }
	protected function userCanInsert(){ return true; }
}
?>