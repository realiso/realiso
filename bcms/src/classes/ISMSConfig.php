<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/** AO INCLUIR UM NOVO USU�RIO ESPECIAL, ATUALIZAR A FUN��O ISMSLib::getSpecialUsers() **/
define("USER_CHAIRMAN", 801);
define("USER_ASSET_CONTROLLER", 803);
define("USER_CONTROL_CONTROLLER", 804);
define("USER_LIBRARIAN", 805);
define("USER_INCIDENT_MANAGER", 806);
define("USER_DISCIPLINARY_PROCESS_MANAGER", 807);
define("USER_NON_CONFORMITY_MANAGER", 808);
define("USER_EVIDENCE_MANAGER", 809);
/****************************************************************************************/

define("RISK_LOW", 7401);
define("RISK_HIGH", 7402);
define("RISK_VALUE_COUNT",7403);
define("CONTROL_COST_1", 201);
define("CONTROL_COST_2", 202);
define("CONTROL_COST_3", 203);
define("CONTROL_COST_4", 204);
define("CONTROL_COST_5", 205);
define("GENERAL_CASCADE_ON",401);
define("GENERAL_EMAIL_ENABLED",402);
define("GENERAL_SMTP_SERVER",403);
define("GENERAL_REVISION_ENABLED",404);
define("GENERAL_TEST_ENABLED",405);
define("GENERAL_COST_ENABLED",406);
define("GENERAL_SOLICITOR_ENABLED",407);
define("GENERAL_PERCENTUAL_RISK",408);
define("GENERAL_ALLOW_DIGESTS",409);
define("GENERAL_DIGEST_TIME",410);
define("GENERAL_DIGEST_LAST_TIME",411);
define("GENERAL_SMTP_DEFAULT_MAIL_SENDER",415);
define("GENERAL_AUTOMATIC_DOCUMENTS",416);
define("GENERAL_DATA_COLLECTION_PERIOD_VALUE",417);
define("GENERAL_DATA_COLLECTION_PERIOD_TYPE",418);
define("GENERAL_DATA_COLLECTION_TIME",419);
define("GENERAL_DATA_COLLECTION_LAST_RUN",420);
define("GENERAL_DATA_COLLECTION_ENABLED",421);
define("GENERAL_DOCUMENTS_MANUAL_VERSIONING",426);
define("GENERAL_SYSTEM_CLASSIFICATION",427);
define("GENERAL_SYSTEM_CLASSIFICATION_DEST",428);
define("GENERAL_MANUAL_DATA_CONTROL",429);
define("GENERAL_SMTP_DEFAULT_MAIL_SENDER_NAME",430);
define("GENERAL_CURRENCY_IDENTIFIER", 431);
define("GENERAL_HOUR_FORMAT", 432);

define("PASSWORD_CHAR_NUM",7201);
define("PASSWORD_SPECIAL_CHARS",7202);
define("PASSWORD_CASE_CHARS",7203);
define("PASSWORD_NUMERIC_CHARS",7204);
define("PASSWORD_BLOCK_TRIES",7205);
define("PASSWORD_CHANGE_FREQUENCY",7206);
define("PASSWORD_HISTORY_NUM",7207);
define("PASSWORD_DAYS_BEFORE_WARNING",7208);
define("ERROR_REPORT_EMAIL",7209);
define("RISK_FORMULA_TYPE",5701);
define("TIMEZONE",7404);
define("EXTERNAL_SERVER", 7500);
define("EXTERNAL_MAIL_PORT", 7501);
define("EXTERNAL_MAIL_USER", 7502);
define("EXTERNAL_MAIL_PASSWORD", 7503);
define("EXTERNAL_MAIL_ENCRYPTION", 7504);
define("EXTERNAL_SERVER_IP", 7510);
define("EXTERNAL_SERVER_DNS", 7511);
define("EXTERNAL_SERVER_GATEWAY", 7512);
define("ASSET_KEY", 7513);

define("LIGHT_COLOR",7515);
define("DARK_COLOR",7516);

define("DAMAGE_MATRIX_SET", 7517); //REVER
define("CURRENCY_SET", 7518);

// Configuracoes para o reset da Base de Dados
define("RDB_RESET_DATA_BASE", 7700);
define("RDB_DATE_RESET_DATA_BASE", 7701);
define("TRIAL_INSTANCE", 7702);
define("CLIENT_ID_ASAAS", 7703);

define("CURR_DOLLAR", '$');
define("CURR_EURO", '&#8364');
define("CURR_REAL", 'R$');
define("CURR_POUND", '&#163');
define("CURR_YEN", '&#165');
define("CURR_YUAN",'&#165');
define("CURR_ARG_PESO", '$');
define("CURR_MEX_PESO", '$');
define("CURR_CANADA", '$');
define("CURR_ISRAEL", '&#8362');
define("CURR_UNIVERSAL", '&curren');
define("CURR_SWITZERLAND", 'CHF');
define("CURR_AUSTRALIAN_DOLLAR", '$');
define("CURR_BRUNEI_DOLLAR", '$');
define("CURR_CHILE_PESO", '$');
define("CURR_COLOMBIA_PESO", '$');
define("CURR_CZECH_KORUNA", 'K&#269;');
define("CURR_DANISH_KRONE", 'kr');
define("CURR_INDIA_RUPEE", '&#8360;');
define("CURR_NEWZELAND_DOLLAR", '$');
define("CURR_PAKISTAN_RUPEE", '&#8360;');
define("CURR_SWEDISH_KRONA", 'kr');
define("CURR_SINGAPORE_DOLLAR", '$');
define("CURR_BOLIVAR", 'Bs F');


/**
 * Classe ISMSConfig.
 *
 * <p>Classe que manipula as configura��es do sistema.</p>
 * @package ISMS
 * @subpackage classes
 */
class ISMSConfig extends ISMSTable {
  
 /**
  * Array de configura��es
  * @var array
  * @access protected
  */  
  protected $caConfig = array();
  
 /**
  * Construtor.
  * 
  * <p>Construtor da classe ISMSConfig.</p>
  * @access public 
  */
  public function __construct(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    parent::__construct("isms_config");

    $this->csAliasId = 'config_id';
    $this->coDataset->addFWDDBField(new FWDDBField("pkConfig",  "config_id",    DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("sValue",    "config_value", DB_STRING));
    
    $this->caConfig = array(
      USER_CHAIRMAN => '',
      USER_ASSET_CONTROLLER => '',
      USER_CONTROL_CONTROLLER => '',
      USER_LIBRARIAN => '',
      USER_INCIDENT_MANAGER => '',
      USER_DISCIPLINARY_PROCESS_MANAGER => '',
      USER_NON_CONFORMITY_MANAGER => '',
      USER_EVIDENCE_MANAGER => '',
      RISK_LOW => '',
      RISK_HIGH => '',
      RISK_VALUE_COUNT => '',
      CONTROL_COST_1 => '',
      CONTROL_COST_2 => '',
      CONTROL_COST_3 => '',
      CONTROL_COST_4 => '',
      CONTROL_COST_5 => '',
      GENERAL_CASCADE_ON => '',
      GENERAL_EMAIL_ENABLED => '',
      GENERAL_ALLOW_DIGESTS => '',
      GENERAL_DIGEST_TIME => '',
      GENERAL_DIGEST_LAST_TIME => '',
      GENERAL_SMTP_SERVER => '', 
      GENERAL_REVISION_ENABLED => '',
      GENERAL_TEST_ENABLED => '',
      GENERAL_COST_ENABLED => '',
      GENERAL_SOLICITOR_ENABLED => '',
      GENERAL_PERCENTUAL_RISK => '',
      GENERAL_SMTP_DEFAULT_MAIL_SENDER => '',
      GENERAL_SMTP_DEFAULT_MAIL_SENDER_NAME => '',
      GENERAL_AUTOMATIC_DOCUMENTS => '',
      GENERAL_DATA_COLLECTION_PERIOD_VALUE => '',
      GENERAL_DATA_COLLECTION_PERIOD_TYPE => '',
      GENERAL_DATA_COLLECTION_TIME => '',
      GENERAL_DATA_COLLECTION_LAST_RUN => '',
      GENERAL_DATA_COLLECTION_ENABLED => '',
      GENERAL_DOCUMENTS_MANUAL_VERSIONING => '',
      GENERAL_SYSTEM_CLASSIFICATION => '',
      GENERAL_SYSTEM_CLASSIFICATION_DEST => '',
      GENERAL_MANUAL_DATA_CONTROL => '',
      GENERAL_CURRENCY_IDENTIFIER => '',
      GENERAL_HOUR_FORMAT => '',
      PASSWORD_CHAR_NUM => '',
      PASSWORD_SPECIAL_CHARS => '',
      PASSWORD_CASE_CHARS => '',
      PASSWORD_NUMERIC_CHARS => '',
      PASSWORD_BLOCK_TRIES => '',
      PASSWORD_CHANGE_FREQUENCY => '',
      PASSWORD_HISTORY_NUM => '',
      PASSWORD_DAYS_BEFORE_WARNING=>'',
      ERROR_REPORT_EMAIL => '',
      RISK_FORMULA_TYPE => '',
      TIMEZONE => '',
      EXTERNAL_SERVER => '',
      EXTERNAL_MAIL_PORT => '',
      EXTERNAL_MAIL_USER => '',
      EXTERNAL_MAIL_PASSWORD => '',
      EXTERNAL_MAIL_ENCRYPTION => '',
      EXTERNAL_SERVER_IP => '',
      EXTERNAL_SERVER_DNS => '',
      EXTERNAL_SERVER_GATEWAY => '',
      ASSET_KEY => '',
      LIGHT_COLOR => '',
      DARK_COLOR => '',
      DAMAGE_MATRIX_SET => '',
      CURRENCY_SET => '',
       RDB_RESET_DATA_BASE => '',
      RDB_DATE_RESET_DATA_BASE => '',
      TRIAL_INSTANCE => '',
      CLIENT_ID_ASAAS => ''
    );
  }
   
 /**
  * Carrega as configura��es atuais independende delas j� estarem na sess�o.
  * 
  * <p>M�todo para carregar as configura��es independende delas j� estarem na sess�o.</p>
  * @access public 
  */
  public function loadConfigForced(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
     $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
      if($moSession->attributeExists('ismsConfigValues')){
        $moSession->deleteAttribute('ismsConfigValues');
      }
      $this->loadConfig();
  }
   
 /**
  * Carrega as configura��es atuais.
  * 
  * <p>M�todo para carregar as configura��es atuais.</p>
  * @access public 
  */
  public function loadConfig() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    if(!$moSession->attributeExists('ismsConfigValues')){
      foreach($this->caConfig as $miKey => $msValue){
        if($miKey!=RISK_LOW && $miKey!=RISK_HIGH){
          if ($this->fetchById($miKey)) //Quando esse teste nao era feito, se o valor nao existe, assumia-se o valor da id anterior.
            $this->caConfig[$miKey] = $this->getFieldValue('config_value');
          $this->removeFilters('config_id');
        }
      }
      
      $moRiskLimits = new RMRiskLimits();
      $moRiskLimits->select();
      $moRiskLimits->fetch();
      $this->caConfig[RISK_LOW] = $moRiskLimits->getFieldValue('risk_limits_low');
      $this->caConfig[RISK_HIGH] = $moRiskLimits->getFieldValue('risk_limits_high');
      
      $moSession->addAttribute('ismsConfigValues');
      $moSession->setAttrIsmsConfigValues($this->caConfig);
      //$moSession->commit();
    }
  }
 
 /**
  * Seta uma determinada configura��o.
  * 
  * <p>M�todo para setar uma determinada configura��o.</p>
  * @access public 
  * @param integer $piConfigId Id da configura��o
  * @param string $psConfigValue Valor da configura��o
  */ 
  public function setConfig($piConfigId, $psConfigValue){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    if(!$moSession->attributeExists('ismsConfigValues')) $this->loadConfig();
    
    $maConfig = $moSession->getAttrIsmsConfigValues();
    //if(isset($maConfig[$piConfigId])){
    if(array_key_exists($piConfigId, $maConfig)){
      $maConfig[$piConfigId] = FWDWebLib::convertToISO($psConfigValue,true);
      $moSession->setAttrIsmsConfigValues($maConfig);
      //$moSession->commit();
      if($piConfigId!=RISK_LOW && $piConfigId!=RISK_HIGH){
        $this->createFilter($piConfigId, 'config_id');
        $this->setFieldValue('config_id', $piConfigId);
        $this->setFieldValue('config_value', $psConfigValue);
        $this->update();
        $this->removeFilters('config_id');
      }
    }else{
      trigger_error("Config id not found: $piConfigId", E_USER_WARNING);
    }
  }
 
 /**
  * Retorna o valor de uma determinada configura��o.
  * 
  * <p>M�todo para retornar o valor de uma determinada configura��o.</p>
  * @access public 
  * @param integer $piConfigId Id da configura��o
  * @return string Valor da configura��o
  */ 
  public function getConfig($piConfigId){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    if(!$moSession->attributeExists('ismsConfigValues')) $this->loadConfig();
    
    $maConfig = $moSession->getAttrIsmsConfigValues();
    //if(isset($maConfig[$piConfigId])){
    if(array_key_exists($piConfigId, $maConfig)){
      return $maConfig[$piConfigId];
    }else{
      trigger_error("Config id not found: $piConfigId", E_USER_WARNING);
    }
  }
  
  public function getConfigFromDB($piConfigId){
  	$msQuery = "select svalue from isms_config where pkconfig = $piConfigId;";
  	$db = FWDWebLib::getConnection();
  	$db->execute($msQuery);
  	$res = $db->fetch();
  	if ($res) return $res['svalue'];  	
  }
}
?>