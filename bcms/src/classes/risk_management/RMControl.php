<?php

/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

include_once $handlers_ref . "QueryControlAutoReaders.php";

/**
 * Classe RMControl.
 *
 * <p>Classe que representa a tabela de controles.</p>
 * @package ISMS
 * @subpackage classes
 */
class RMControl extends ISMSContext implements IWorkflow {
  
	/**
	 * Construtor.
	 * 
	 * <p>Construtor da classe RMControl.</p>
	 * @access public 
	 */
	public function __construct() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		parent::__construct("rm_control");
    $this->ciContextType = CONTEXT_CONTROL;
    
		$this->csAliasId = "control_id";
		$this->cbHasDocument = true;
		$this->coWorkflow = new RMWorkflow(ACT_CONTROL_APPROVAL);

		$this->coDataset->addFWDDBField(new FWDDBField("fkContext", "control_id", DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkResponsible", "control_responsible_id", DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("sName", "control_name", DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("tDescription", "control_description", DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("sDocument", "control_document", DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("sEvidence", "control_evidence", DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("nDaysBefore", "control_days_before", DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("bIsActive", "control_active", DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("bFlagImplAlert", "control_impl_alert", DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("bFlagImplExpired", "control_impl_expired", DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("dDateDeadline", "control_date_deadline", DB_DATETIME));
		$this->coDataset->addFWDDBField(new FWDDBField("dDateImplemented", "control_date_implemented", DB_DATETIME));
		$this->coDataset->addFWDDBField(new FWDDBField("nImplementationState", "control_implementation_state", DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("fkType", "control_type", DB_NUMBER));

    $this->caSensitiveFields = array('control_name', 'control_responsible_id', 'control_date_deadline', 'control_days_before');
		$this->caSearchableFields = array ('control_name', 'control_description', 'control_evidence');
	}

	/**
	 * Retorna o id do usu�rio que deve aprovar o contexto.
	 * 
	 * <p>M�todo para retornar id do usu�rio que deve aprovar o contexto.</p>
	 * @access public 
	 * @return integer Id do aprovador
	 */
	public function getApprover() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		return ISMSLib::getConfigById(USER_CONTROL_CONTROLLER);
	}

	/**
	 * Retorna o id do usu�rio respons�vel pelo contexto.
	 * 
	 * <p>M�todo para retornar id do usu�rio respons�vel pelo contexto.</p>
	 * @access public 
	 * @return integer Id do respons�vel
	 */
	public function getResponsible() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		return $this->getFieldValue('control_responsible_id');
	}

	/**
	 * Retorna a descri��o do contexto.
	 * 
	 * <p>M�todo para retornar a descri��o do contexto.</p>
	 * @access public 
	 * @return string Descri��o do contexto
	 */
	public function getDescription() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		return $this->getFieldValue('control_description');
	}

	/**
	 * Retorna o nome do contexto.
	 * 
	 * <p>M�todo para retornar o nome do contexto.</p>
	 * @access public 
	 * @return string Nome do contexto
	 */
	public function getName() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		return $this->getFieldValue('control_name');

	}

 /**
  * Retorna o �cone do contexto.
  *
  * <p>M�todo para retornar o �cone do contexto.</p>
  * @access public
  * @param integer $piContextId Id do contexto
  * @return string Nome do �cone
  */
  public function getIcon($piContextId = 0){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return 'icon-control.gif';
  }

	/**
	 * Retorna o label do contexto.
	 * 
	 * <p>M�todo para retornar o label do contexto.</p>
	 * @access public 
	 * @return string Label do contexto
	 */
	public function getLabel() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		return FWDLanguage::getPHPStringValue('mx_control', "Controle");
	}

 /**
  * Retorna o valor do contexto.
  * 
  * <p>M�todo para retornar o valor do contexto.</p>
  * @access public 
  * @return integer valor do contexto
  */
  public function getValue(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
  	return $this->coDataset->getFieldByAlias('control_active')->getValue();
  }


  /**
  * Retorna o caminho do contexto para a cria��o do pathscroll.
  * 
  * <p>M�todo para retornar o caminho do contexto para a cria��o do pathscroll.</p>
  * @access public 
  * @param integer $piTab id da tab
  * @param integer $piContextType id do contexto
  * @param string $psContextId indices do filtro de ids dos links do pathscroling  
  * @param string $psAuxContextId indices auxiliares (para a recurs�o (Ativo->Controle)) do filtro de ids dos links do pathscroling
  * @return array Array contendo o Caminho do contexto
  */ 
  public function getSystemPathScrollAux($piTab,$piContextType,$psContextId,$psAuxContextId =''){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
  	$msHome = FWDLanguage::getPHPStringValue('st_home','Raiz');
  	$maPath=array();
 	  if($psContextId){
			if(!(strpos($psContextId,':')===false))
					$psContextId="\\\"$psContextId\\\"";
		}else
			$psContextId = 0;
 	  if($psAuxContextId!=''){
 			$maPath[] = "<a href='javascript:isms_redirect_to_mode_scroll({$piTab},".CONTEXT_CONTROL.",$psContextId,\\\"$psAuxContextId\\\");'>";
 	  }else{
  		$maPath[] = "<a href='javascript:isms_redirect_to_mode_scroll({$piTab},".CONTEXT_CONTROL.",$psContextId);'>";
 	  }
 	  $maPath[] = $this->getName();

    if($this->getValue()==1)
    	$maPath[]=ISMSLib::getIconCode('icon-control.gif',-2);
    else
    	$maPath[]=ISMSLib::getIconCode('icon-control_red.gif',-2);

 		$maPath[] = "</a>";
    return $maPath;
  }

	/**
	 * Retorna o caminho do contexto.
	 * 
	 * <p>M�todo para retornar o caminho do contexto.</p>
	 * @access public 
	 * @return string Caminho do contexto
	 */
	public function getSystemPath() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		/*
		 * No caso do controle n�o � necess�rio passar para a fun��o isms_redirect_to_mode(...)
		 * o seu identificador pois ele apenas deve redirecionar para a aba de controles. Caso fosse
		 * passado o id, toda vez que um controle diferente fosse clicado, a aba de controles seria
		 * recarregada desnecessariamente. Desse modo, ela s� � carregada a primeira vez. 
		 */
     if($this->getValue()==1)
	    	$msIcon='icon-control.gif';
     else
	    	$msIcon ='icon-control_red.gif';

		$msRMPath = "<a href='javascript:isms_redirect_to_mode(".RISK_MANAGEMENT_MODE.",0,0)'>".FWDLanguage::getPHPStringValue('st_rm_module_name','Gest�o de Riscos')."</a>";
		$msContextPath = ISMSLib::getIconCode($msIcon,-2,-4). "<a href='javascript:isms_redirect_to_mode(".RISK_MANAGEMENT_MODE.",".$this->ciContextType.",0)'>".$this->getName()."</a>";
		return $msRMPath."&nbsp;".ISMSLib::getIconCode('icon-arrow-right.gif',-2,0).'&nbsp;'.$msContextPath;
	}
  
  /**
   * Retorna os leitores e o respons�vel do controle.
   * 
   * <p>Retorna os leitores e o respons�vel do controle.</p>
   * @access public
   * @return array Array com leitores + respons�vel do controle.
   */
  public function getReadersAndResponsible($piControlId) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $moControl = new RMControl();
    $moControl->fetchById($piControlId);
    $miResponsibleId = $moControl->getResponsible();
    
    $moQuery = new QueryControlAutoReaders(FWDWebLib::getConnection());
    $moQuery->setControl($piControlId);
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    $maUsers = $moQuery->getReaders();
    
    if (!in_array($miResponsibleId,$maUsers)) $maUsers[] = $miResponsibleId;
    return $maUsers;
  }
  
 /**
  * Atualiza os leitores do documento do controle.
  * 
  * <p>M�todo para atualizar os leitores do documento do controle.</p>
  * @access public 
  * @param integer $piControlId Id do controle
  */
  public function updateReaders($piControlId) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
  	/*
  	 * Busca, a partir dos usu�rios dos processos, todos os usu�rios
  	 * que est�o indiretamente relacionados com o controle.
  	 * Controle -> Risco -> Ativo -> Processo -> Usu�rios
  	 */
  	$moQuery = new QueryControlAutoReaders(FWDWebLib::getConnection());
    $moQuery->setControl($piControlId);
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    $maUsers = $moQuery->getReaders();
        
  	/*
  	 * Para cada documento do controle, atualiza os leitores dos documentos
  	 */
  	$moDocContext = new PMDocContext();
    $moDocContext->createFilter($piControlId, 'context_id');
    $moDocContext->select();    
    while($moDocContext->fetch()) {
      $miDocId = $moDocContext->getFieldValue('document_id');
  		$moDocReaders = new PMDocumentReader();
  		$moDocReaders->updateUsers($miDocId, $maUsers);
  	}    
  }
  
 /**
  * Retorna se o usu�rio logado tem permi��o para inserir o contexto.
  * 
  * <p>M�todo para retornar se o usu�rio tem permi��o para inserir o contexto no sistema.</p>
  * @access public
  * @return boolean Se o usu�ro tem permi��o para inserir o contexto no sistema
  */
  protected function userCanInsert(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $mbReturn = false;
    if(ISMSLib::userHasACL('M.RM.5.6')){
      $mbReturn = true;
    }
    return $mbReturn;
  }

 /**
  * Retorna se o usu�rio logado tem permi��o para editar o contexto.
  * 
  * <p>M�todo para retornar se o usu�rio tem permi��o para editar o contexto no sistema.</p>
  * @access protected
  * @return boolean Se o usu�ro tem permi��o de editar o contexto no sistema
  */
  public function userCanEdit($piContextId,$piUserResponsible = 0){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    if(!$this->contextIsOfType($piContextId,$this->ciContextType)) return false;
    $mbReturn = false;
    $miResponsibleId = $piUserResponsible;
    if(!$miResponsibleId){
      $miResponsibleId = $this->getFieldValue('control_responsible_id');
    }
    if(ISMSLib::userHasACL('M.RM.5.4')){
      $mbReturn = true;
    }elseif( ISMSLib::userHasACL('M.RM.5.7') && $miResponsibleId == ISMSLib::getCurrentUserId() ){
      $mbReturn = true;
    }
    return $mbReturn;
  }

  /**
  * Retorna se o usu�rio pode executar a task.
  * 
  * <p>M�todo para retornar se o usu�rio pode executar a task 
  * </p>
  * @access protected
  * @param integer $piTaskId Id de tasks
  * @return boolean Se o usu�ro tem permi��o executar a task
  */
  protected function userCanExecuteTask($piTaskId){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    // Por que precisa testar? Se o usu�rio recebeu a terefa, ele e somente ele tem que execut�-la.
    return true;
    
    $mbReturn = false;
    $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    $moTask = new WKFTask();
    $moTask->fetchById($piTaskId);
    $miActivity = $moTask->getFieldValue('task_activity');
    $miContextId = $moTask->getFieldValue('task_context_id');
    $moContextObject = new ISMSContextObject();
    $moContext = $moContextObject->getContextObjectByContextId($miContextId);
    $moContext->fetchById($miContextId);
    
    if(($miActivity==ACT_REAL_EFFICIENCY) || ($miActivity==ACT_CONTROL_TEST) || ($miActivity==ACT_INC_CONTROL_INDUCTION)){
        $miUserTaskReceiver =  $moContext->getResponsible();
        if($miUserTaskReceiver==$miUserId){
          $mbReturn = true;
        }
    }else{
        $miUserTaskReceiver = $moContext->getApprover();
        if($miUserTaskReceiver==$miUserId){
          $moContextObject2 = new ISMSContextObject();
          $moContextObject2->fetchById($miContextId);
          if(($moContextObject2->getFieldValue('context_state')==CONTEXT_STATE_PENDANT)|| ($moContextObject2->getFieldValue('context_state')==CONTEXT_STATE_COAPPROVED) ){
            $mbReturn = true;
          }
        }
    }
    return $mbReturn;
  }

 /**
  * Retorna se o usu�rio logado tem permi��o para excluir o contexto.
  * 
  * <p>M�todo para retornar se o usu�rio tem permi��o para excluir o contexto no sistema.</p>
  * @access protected
  * @return boolean Se o usu�ro tem permi��o de excluir o contexto no sistema
  */
  protected function userCanDelete($piContextId){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    if(!$this->contextIsOfType($piContextId,$this->ciContextType)) return false;
    $mbReturn = false;
    if(ISMSLib::userHasACL('M.RM.5.5')){
      $mbReturn = true;
    }else{
      $moContext = new RMControl();
      $moContext->fetchById($piContextId);
      if( ISMSLib::userHasACL('M.RM.5.8') && $moContext->getFieldValue('control_responsible_id') == ISMSLib::getCurrentUserId() ){
        $mbReturn = true;
      }
    }
    return $mbReturn;
  }
  
 /**
  * Atualiza o campo control_active.
  * 
  * <p>Testa se o controle est� ativo ou n�o e atualiza o campo control_active.</p>
  * @access public
  */
  public function updateActive(){
    // Seta o campo control_active somente para disparar o trigger que recalcula o valor correto do campo
    $moControl = new RMControl();
    $moControl->setFieldValue('control_active',true);
    $moControl->update($this->getFieldValue('control_id'));
  }

 /**
  * Atualiza o campo control_active de todos controles.
  * 
  * <p>Atualiza o campo control_active de todos controles.</p>
  * @access public
  */
  public static function updateActiveFromAll(){
    // Seta o campo control_active de todos controles somente para disparar o trigger que recalcula o valor correto do campo
    $moControl = new RMControl();
    $moControl->setFieldValue('control_active',true);
    $moControl->createFilter(1,'control_active');
    $moControl->createFilter(0,'control_active');
    $moControl->update(0);
  }

  /**
  * Retorna o c�digo HTML do icone do contexto.
  * 
  * <p>M�todo para retornar o c�digo HTML do contexto</p>
  * @access public
  * @param integer $piId Id do contexto
  * @param integer $pbIsFetched id do contexto
  * @return string contendo o c�digo HTML do icone do contexto
  */ 
  public function getIconCode($piId, $pbIsFetched = false){
    if(!$pbIsFetched){
      $this->fetchById($piId);
    }
    if($this->getValue()==1)
      $msIcon='icon-control.gif';
    else
      $msIcon ='icon-control_red.gif';
    return ISMSLib::getIconCode($msIcon,-2,7);
  }

  /**
  * Retorna o caminho para abrir a popup de edi��o desse contexto.
  * 
  * <p>M�todo para retornar o caminho para abrir a popup de edi��o desse contexto.</p>
  * @param integer $piContextId id do contexto
  * @access public 
  * @return string caminho para abrir a popup de edi��o desse contexto
  */ 
  public function getVisualizeEditEvent($piContextId,$psUniqId){
    $soSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    $msPackagesRiskPath = ($soSession->getMode()==MANAGER_MODE) ? "packages/risk/" : "../../packages/risk/";
    $msPopupId = "popup_control_edit";
    
    return "isms_open_popup('{$msPopupId}','{$msPackagesRiskPath}{$msPopupId}.php?control=$piContextId','','true',522,570);"
           ."soPopUpManager.getPopUpById('{$msPopupId}').setCloseEvent( function close_visualize_$psUniqId() {soPopUpManager.closePopUp('popup_visualize_$psUniqId');} );";
  }

  /**
  * Retorna o c�digo javascript espec�fico para a janela de visualiza��o.
  * 
  * <p>M�todo para retornar o c�digo javascript espec�fico para a janela de
  * visualiza��o.
  * </p>
  * @access public
  * @return string C�digo javascript espec�fico por contexto para a janela de
  * visualiza��o
  */
  public function getVisualizeJavascript() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $msJavascript = "
      function open_task_control_revision(task_id,context_id){
        isms_open_popup('popup_control_revision_task', 'packages/risk/popup_control_revision_task.php?task='+task_id+'&control='+context_id, '', 'false', 380, 800);
      }
      function open_task_control_test(task_id,context_id){
        isms_open_popup('popup_control_test_task', 'packages/risk/popup_control_test_task.php?task='+task_id+'&control='+context_id, '', 'false', 422, 400);
      }
    ";
    return $msJavascript;
  }

 /**
  * Deleta logicamente um contexto.
  * 
  * <p>M�todo para deletar logicamente um contexto. Deve ser sobrecarregada
  * por contextos para os quais for necess�ria.</p>
  * @access public
  * @param integer $piContextId id do contexto
  */ 
  public function logicalDelete($piContextId) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    //remove alertas e tarefas da real��o risco X controle
    $moRel = new RMRiskControl();
    $moRel->createFilter($piContextId,'rc_control_id');
    $moRel->select();
    while($moRel->fetch()){
      // deletar tarefas do contexto deletado
      $moTasks = new WKFTask();
      $moTasks->createFilter($moRel->getFieldValue('rc_id'), 'task_context_id');
      $moTasks->delete();
      // deletar alertas do contexto deletado
      $moAlerts = new WKFAlert();
      $moAlerts->createFilter($moRel->getFieldValue('rc_id'), 'alert_context_id');
      $moAlerts->delete();
    }
    
    //remove alertas e tarefas da rela��o controle X incidente
    $moRel = new CIIncidentControl();
    $moRel->createFilter($piContextId,'control_id');
    $moRel->select();
    while($moRel->fetch()){
      // deletar tarefas do contexto deletado
      $moTasks = new WKFTask();
      $moTasks->createFilter($moRel->getFieldValue('incident_control_id'), 'task_context_id');
      $moTasks->delete();
      // deletar alertas do contexto deletado
      $moAlerts = new WKFAlert();
      $moAlerts->createFilter($moRel->getFieldValue('incident_control_id'), 'alert_context_id');
      $moAlerts->delete();
    }

  }

}

?>