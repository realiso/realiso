<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

define("CONTROL_IMPLEMENTATION_MODE_NONE",78500);
define("CONTROL_IMPLEMENTATION_MODE_ACCEPT",78501);
define("CONTROL_IMPLEMENTATION_MODE_TRANSFER",78502);
define("CONTROL_IMPLEMENTATION_MODE_AVOID",78503);

/**
 * Classe RMControlImplementation.
 *
 * <p>Classe que representa a tabela de controles (para aceita��o da implementa��o dos controles).</p>
 * @package ISMS
 * @subpackage classes
 */
class RMControlImplementationAccept extends ISMSContext implements IWorkflow {
  
 /**
  * Construtor.
  *
  * <p>Construtor da classe RMControlImplementationAccept.</p>
  * @access public
  */
  public function __construct(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    parent::__construct('rm_control');
    $this->ciContextType = CONTEXT_ACCEPT_CONTROL_IMPLEMENTATION;

    $this->csAliasId = "control_id";
    //$this->coWorkflow = new RMWorkflow(ACT_CONTROL_IMPLEMENTATION_APPROVAL);
    //foi definido pelo andr� em 29-05-2008 que n�o deve mais existir no sistema a tarefa de aprova��o da implementa��o do controle

		$this->coDataset->addFWDDBField(new FWDDBField("fkContext", "control_id", DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkResponsible", "control_responsible_id", DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("sName", "control_name", DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("tDescription", "control_description", DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("sDocument", "control_document", DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("sEvidence", "control_evidence", DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("nDaysBefore", "control_days_before", DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("bIsActive", "control_active", DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("bFlagImplAlert", "control_impl_alert", DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("bFlagImplExpired", "control_impl_expired", DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("dDateDeadline", "control_date_deadline", DB_DATETIME));
		$this->coDataset->addFWDDBField(new FWDDBField("dDateImplemented", "control_date_implemented", DB_DATETIME));
		$this->coDataset->addFWDDBField(new FWDDBField("fkType", "control_type", DB_NUMBER));
  }

 /**
  * Retorna o id do usu�rio que deve aprovar o contexto.
  *
  * <p>M�todo para retornar id do usu�rio que deve aprovar o contexto.</p>
  * @access public
  * @return integer Id do aprovador
  */
  public function getApprover(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    return ISMSLib::getConfigById(USER_CONTROL_CONTROLLER);
  }

 /**
  * Retorna o id do usu�rio respons�vel pelo contexto.
  *
  * <p>M�todo para retornar id do usu�rio respons�vel pelo contexto.</p>
  * @access public
  * @return integer Id do respons�vel
  */
  public function getResponsible(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $this->getFieldValue("control_responsible_id");
  }

 /**
  * Retorna o nome do contexto.
  * 
  * <p>M�todo para retornar o nome do contexto.</p>
  * @access public 
  * @return string Nome do contexto
  */
  public function getName() {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    return $this->getFieldValue('control_name');
  }
 
 /**
  * Retorna a descri��o do contexto.
  * 
  * <p>M�todo para retornar a descri��o do contexto.</p>
  * @access public 
  * @return string Descri��o do contexto
  */ 
  public function getDescription() {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    return $this->getFieldValue('control_description');
  }
  
 /**
  * Retorna o label do contexto.
  * 
  * <p>M�todo para retornar o label do contexto.</p>
  * @access public 
  * @return string Label do contexto
  */
  public function getLabel() {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    return FWDLanguage::getPHPStringValue('mx_control_implementation_acceptance', "Aceita��o da Implementa��o do Controle");
  }

 /**
  * Retorna se o usu�rio logado tem permi��o para inserir o contexto.
  * 
  * <p>M�todo para retornar se o usu�rio tem permi��o para inserir o contexto no sistema.</p>
  * @access public
  * @return boolean Se o usu�ro tem permi��o para inserir o contexto no sistema
  */
  protected function userCanInsert(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $moControl = new RMControl();
    return $moControl->testPermissionToInsert();
  }

 /**
  * Retorna se o usu�rio logado tem permi��o para editar o contexto.
  * 
  * <p>M�todo para retornar se o usu�rio tem permi��o para editar o contexto no sistema.</p>
  * @access protected
  * @return boolean Se o usu�ro tem permi��o de editar o contexto no sistema
  */
  protected function userCanEdit($piContextId,$piUserResponsible = 0){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $moControl = new RMControl();
    return $moControl->testPermissionToUpdate($piContextId,$piUserResponsible);
  }
 
 /**
  * Retorna se o usu�rio logado tem permi��o para excluir o contexto.
  * 
  * <p>M�todo para retornar se o usu�rio tem permi��o para excluir o contexto no sistema.</p>
  * @access protected
  * @return boolean Se o usu�ro tem permi��o de excluir o contexto no sistema
  */
  protected function userCanDelete($piContextId){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $moControl = new RMControl();
    return $moControl->testPermissionToDelete($piContextId);
	}
}
?>