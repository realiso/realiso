<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe WorkflowRiskControl.
 *
 * <p>Classe que herda de Workflow para possibilitar dupla aprova��o.</p>
 * @package ISMS
 * @subpackage classes
 */
class WorkflowRiskControl extends Workflow { 
 
 /**
  * Avan�a o estado do contexto no workflow.
  * 
  * <p>M�todo para avan�ar o estado do contexto no workflow.</p>
  * @access public 
  * 
  * @param RMRiskControl $poRiskControl Objeto
  * @param integer $piAction Tipo da a��o
  * @param string $psJustification Justificativa
  */
  public function stateForward(IWorkflow $poRiskControl, $piAction, $psJustification = "") {    
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $miUserId = ISMSLib::getCurrentUserId();
    
    switch ($poRiskControl->getContextState()) {
      case CONTEXT_STATE_NONE:
        $moRisk = new RMRisk();
        $moRisk->fetchById($poRiskControl->getFieldValue('rc_risk_id'));
        $poRiskControl->setApprover($moRisk->getResponsibleToACLPermition());
        
        /* 
         * Se o usu�rio atual for o mesmo que o aprovador do risco, 
         * n�o precisa enviar tarefa de aprova��o para ele, ou seja,
         * passa direto para a aprova��o por parte do aprovador do controle.
         * Caso o usu�rio seja difirente, envia uma tarefa de aprova��o da
         * associa��o para ele.
         */
        if ($miUserId == $moRisk->getResponsibleToACLPermition()) {
          $moControl = new RMControl();
          $moControl->fetchById($poRiskControl->getFieldValue('rc_control_id'));
          
          /*
           * Se o usu�rio tamb�m for respons�vel pelo controle, 
           * aprovar automaticamente a associa��o. Caso contr�rio,
           * envia uma tarefa de aprova��o para o respons�vel pelo
           * controle, assim como um alerta indicando que a associa��o
           * j� foi co-aprovada.  
           */
          if ($miUserId == $moControl->getResponsible()){
            $poRiskControl->setContextState(CONTEXT_STATE_APPROVED);
          }else{
            /*como o usu�rio logado � o approver do risco, a rela��o de risco/controle fica pr� aprovada e o aprovador
            agora � o respons�vel pelo controle*/
            $poRiskControl->setApprover($moControl->getResponsible());
            $poRiskControl->setContextState(CONTEXT_STATE_COAPPROVED);
            $poRiskControl->setApprover($moControl->getResponsible());
            $this->dispatchAlert(WKF_ALERT_COAPPROVED, $poRiskControl, $psJustification);
            $this->dispatchTask($this->ciActivity, $poRiskControl->getId(), $poRiskControl->getApprover());
          }
        }else{
          $this->dispatchTask($this->ciActivity, $poRiskControl->getId(), $poRiskControl->getApprover());
          $poRiskControl->setContextState(CONTEXT_STATE_PENDANT);
        }
      break;
      case CONTEXT_STATE_PENDANT:
        switch ($piAction) {
          case WKF_ACTION_APPROVE :
            $moControl = new RMControl();
            $moControl->fetchById($poRiskControl->getFieldValue('rc_control_id'));
            $poRiskControl->setApprover($moControl->getResponsible());
            /*
             * Se o criador da associa��o tamb�m for o respons�vel pelo controle,
             * aprova automaticamente. Caso contr�rio, seta o estado para co-aprovado.
             */
            if( $poRiskControl->getCreator() == $moControl->getResponsible() ){
              $poRiskControl->setContextState(CONTEXT_STATE_APPROVED);
            }else{
              $poRiskControl->setApprover($moControl->getResponsible());
              $this->dispatchAlert(WKF_ALERT_COAPPROVED, $poRiskControl, $psJustification);
              $this->dispatchTask($this->ciActivity, $poRiskControl->getId(), $poRiskControl->getApprover());
              $poRiskControl->setContextState(CONTEXT_STATE_COAPPROVED);
            }
          break;
          case WKF_ACTION_DENY :
            $this->dispatchAlert(WKF_ALERT_DENIED, $poRiskControl, $psJustification);
            $poRiskControl->setContextState(CONTEXT_STATE_DENIED);
          break;
        }
      break;
      case CONTEXT_STATE_COAPPROVED:
        switch ($piAction){
          case WKF_ACTION_APPROVE:
            $this->dispatchAlert(WKF_ALERT_APPROVED, $poRiskControl, $psJustification);
            $poRiskControl->setContextState(CONTEXT_STATE_APPROVED);
          break;
          case WKF_ACTION_DENY:
            $this->dispatchAlert(WKF_ALERT_DENIED, $poRiskControl, $psJustification);
            $poRiskControl->setContextState(CONTEXT_STATE_DENIED);
          break;
        }
      break;
    }
  } 
}

?>