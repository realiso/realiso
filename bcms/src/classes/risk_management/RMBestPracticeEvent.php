<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe RMBestPracticeEvent.
 *
 * <p>Classe que representa a tabela de associa��o de melhores pr�ticas com
 * eventos.</p>
 * @package ISMS
 * @subpackage classes
 */
class RMBestPracticeEvent extends ISMSContext {
  
 /**
  * Construtor.
  *
  * <p>Construtor da classe RMBestPracticeEvent.</p>
  * @access public
  */
  public function __construct(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    parent::__construct('rm_best_practice_event');
    $this->ciContextType = CONTEXT_BEST_PRACTICE_EVENT;
    
    $this->csAliasId = 'best_practice_event_id';
    $this->coDataset->addFWDDBField(new FWDDBField('fkContext',     'best_practice_event_id',DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('fkBestPractice','best_practice_id',      DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('fkEvent',       'event_id',              DB_NUMBER));
  }

 /**
  * Insere rela��es entre uma melhor pr�tica e diversos eventos.
  *
  * <p>M�todo para inserir rela��es entre uma melhor pr�tica e diversos eventos.</p>
  * @access public
  * @param integer $piBestPracticeId Id da Melhor Pr�tica
  * @param array $paEvents Ids dos Eventos
  */
  public function insertEvents($piBestPracticeId, $paEvents){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $this->setFieldValue('best_practice_id', $piBestPracticeId);
    foreach($paEvents as $miEventId){
      $this->setFieldValue('event_id', $miEventId);
      parent::insert();
    }
  }

 /**
  * Deleta rela��es entre uma melhor pr�tica e diversos eventos.
  *
  * <p>M�todo para deletar rela��es entre uma melhor pr�tica e diversos eventos.</p>
  * @access public
  * @param integer $piBestPracticeId Id da Melhor Pr�tica
  * @param array $paEvents Ids dos Eventos
  */
  public function deleteEvents($piBestPracticeId, $paEvents){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    foreach($paEvents as $miEventId){
      parent::delete($this->getId($piBestPracticeId, $miEventId), true);
    }
  }

 /**
  * Insere rela��es entre um evento e diversas melhores pr�ticas.
  *
  * <p>M�todo para inserir rela��es entre um evento e diversas melhores pr�ticas.</p>
  * @access public
  * @param integer $piEventId Id do evento
  * @param array $paBestPractices Ids das melhores pr�ticas
  */
  public function insertBestPractices($piEventId, $paBestPractices){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $this->setFieldValue('event_id', $piEventId);
    foreach($paBestPractices as $miBestPracticeId){
      $this->setFieldValue('best_practice_id', $miBestPracticeId);
      parent::insert();
    }
  }

 /**
  * Deleta rela��es entre um evento e diversas melhores pr�ticas.
  *
  * <p>M�todo para deletar rela��es entre um evento e diversas melhores pr�ticas.</p>
  * @access public
  * @param integer $piEventId Id do evento
  * @param array $paBestPractices Ids das melhores pr�ticas
  */
  public function deleteBestPractices($piEventId, $paBestPractices){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    foreach($paBestPractices as $miBestPracticeId){
      parent::delete($this->getId($miBestPracticeId, $piEventId), true);
    }
  }

 /**
  * Retorna o id do contexto da associa��o.
  *
  * <p>M�todo para retornar o id do contexto da associa��o.</p>
  * @access public
  * @param integer $piBestPracticeId Id da Melhor Pr�tica
  * @param integer $piEventId Id do Evento
  * @return integer Id da associa��o
  */
  public function getId($piBestPracticeId, $piEventId){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $moRMBestPracticeEvent = new RMBestPracticeEvent();
    $moRMBestPracticeEvent->createFilter($piBestPracticeId, 'best_practice_id');
    $moRMBestPracticeEvent->createFilter($piEventId, 'event_id');
    $moRMBestPracticeEvent->select();
    $moRMBestPracticeEvent->fetch();
    return $moRMBestPracticeEvent->getFieldValue('best_practice_event_id');
  }

 /**
  * Retorna o nome do contexto.
  *
  * <p>M�todo para retornar o nome do contexto.</p>
  * @access public
  * @return string Nome do contexto
  */
  public function getName(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $moBestPractice = new RMBestPractice();
    $moBestPractice->fetchById($this->getFieldValue('best_practice_id'));
    $moEvent = new RMEvent();
    $moEvent->fetchById($this->getFieldValue('event_id'));
    return $moBestPractice->getName() . ' -> ' . $moEvent->getName();
  }

 /**
  * Retorna o label do contexto.
  *
  * <p>M�todo para retornar o label do contexto.</p>
  * @access public
  * @return string Label do contexto
  */
  public function getLabel(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    return FWDLanguage::getPHPStringValue('mx_best_practice_event_association', 'Associa��o de Melhor Pr�tica a Evento');
  }


 /**
  * Retorna se o usu�rio logado tem permi��o para inserir o contexto.
  * 
  * <p>M�todo para retornar se o usu�rio tem permi��o para inserir o contexto no sistema.</p>
  * @access public
  * @return boolean Se o usu�ro tem permi��o para inserir o contexto no sistema
  */
  protected function userCanInsert(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    //contexto deve ser tratado, assim assume-se que a principio o usu�rio n�o pode inserir tal contexto
    $mbReturn = false;
    $miCount = 0;

    $maPermissions = array( 'M.L.2.4' );
    //acls negadas o usu�rio logado no sistema
    $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
    foreach($maACLs as $miKey => $miValue){
      $maACLs[$miValue]=$miKey;
    }
    //acl1 AND acl2
    $mbAuxPermission = true;
    foreach($maPermissions as $msValue){
      if(isset($maACLs[$msValue])){
        $mbAuxPermission = false;
      }
      $miCount++;
    }
    //caso exista pelo menos 1 acl nas permi��es do contexto e estas permi��es n�o est�o nas permi��es negadas do usu�rio
    if(($mbAuxPermission)&&($miCount)){
      $mbReturn = $mbAuxPermission;
    }
    return $mbReturn;
  }

 /**
  * Retorna se o usu�rio logado tem permi��o para editar o contexto.
  * 
  * <p>M�todo para retornar se o usu�rio tem permi��o para editar o contexto no sistema.</p>
  * @access protected
  * @return boolean Se o usu�ro tem permi��o de editar o contexto no sistema
  */
  protected function userCanEdit($piContextId,$piUserResponsible = 0){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    if(!$this->contextIsOfType($piContextId,$this->ciContextType)) return false;
    //contexto deve ser tratado, assim assume-se que a principio o usu�rio n�o pode editar esse contexto
    $mbReturn = false;
    $maPermissions = array( 'M.L.2.4' );
    //acls negadas do usu�rio logado no sistema
    $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
    foreach($maACLs as $miKey => $miValue){
      $maACLs[$miValue]=$miKey;
    }
    $miCount = 0;
    //acl1 AND acl2
    $mbAuxPermission = true;
    foreach($maPermissions as $msValue){
      if(isset($maACLs[$msValue])){
        $mbAuxPermission = false;
      }
      $miCount++;
    }
    //caso exista pelo menos 1 acl nas permi��es do contexto e estas permi��es n�o est�o nas permi��es negadas do usu�rio
    if(($mbAuxPermission)&&($miCount)){
      $mbReturn = $mbAuxPermission;
      }
    return $mbReturn;
  }
 
 /**
  * Retorna se o usu�rio logado tem permi��o para excluir o contexto.
  * 
  * <p>M�todo para retornar se o usu�rio tem permi��o para excluir o contexto no sistema.</p>
  * @access protected
  * @return boolean Se o usu�ro tem permi��o de excluir o contexto no sistema
  */
  protected function userCanDelete($piContextId){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    //tester esquemas de respons�vel aq nesse teste
    $miContextType = $this->ciContextType;
     if(!$this->contextIsOfType($piContextId,$miContextType)) return false;
    //contexto deve ser tratado, assim assume-se que a principio o usu�rio n�o pode inserir tal contexto
    $mbReturn = false;
    $maPermissions = array( 'M.L.2.4' );

    //acls negadas do usu�rio logado no sistema
    $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
    foreach($maACLs as $miKey => $miValue){
      $maACLs[$miValue]=$miKey;
    }
    $miCount = 0;
    //acl1 AND acl2
    $mbAuxPermission = true;
    foreach($maPermissions as $msValue){
      if(isset($maACLs[$msValue])){
        $mbAuxPermission = false;
      }
      $miCount++;
    }
    //caso exista pelo menos 1 acl nas permi��es do contexto e estas permi��es n�o est�o nas permi��es negadas do usu�rio
    if(($mbAuxPermission)&&($miCount)){
      $mbReturn = $mbAuxPermission;
    }
    return $mbReturn;
  }
}
?>