<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe RMAssetAsset.
 *
 * <p>Classe que representa a tabela de dependÍncia de ativos.</p>
 * @package ISMS
 * @subpackage classes
 */
class RMAssetAsset extends FWDDBTable {
  
 /**
  * Construtor.
  *
  * <p>Construtor da classe RMAssetAsset.</p>
  * @access public
  */
  public function __construct(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    parent::__construct('rm_asset_asset');
    
    $this->coDataset->addFWDDBField(new FWDDBField('fkAsset',       'asset_id',     DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('fkDependent',		'dependent_id',	DB_NUMBER));    
  }
}
?>