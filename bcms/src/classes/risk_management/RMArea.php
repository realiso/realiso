<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

include_once $handlers_ref . "QueryAreaParents.php";
include_once $handlers_ref . "QuerySubAreas.php";
include_once $handlers_ref . "QueryProcessesFromArea.php";
include_once $handlers_ref . "QueryAreaAutoReaders.php";

/**
 * Classe RMArea.
 *
 * <p>Classe que representa a tabela de �reas.</p>
 * @package ISMS
 * @subpackage classes
 */
class RMArea extends ISMSContext  {

	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe RMArea.</p>
	 * @access public
	 */
	public function __construct(){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		parent::__construct("rm_area");
		$this->ciContextType = CONTEXT_AREA;

		$this->csAliasId = "area_id";
		$this->csDependenceAliasId = "area_parent_id";
		$this->csSpecialDeleteSP = "delete_area";

		$this->coDataset->addFWDDBField(new FWDDBField("fkContext",     		"area_id",              DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkParent",      		"area_parent_id",       DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("sName",         		"area_name",            DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("tDescription",  		"area_description",     DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("sDocument",     		"area_document",        DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("nValue",        		"area_value",           DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkType",        		"area_type",            DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkPriority",    		"area_priority",        DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkPlace",    			"area_place",        	DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkResource",      		"resource",       		DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkGroup",      			"group",       			DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkResourceSubstitute",  "resourceSubstitute",   DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkGroupSubstitute",     "groupSubstitute",      DB_NUMBER));

		$this->caSensitiveFields = array('area_name');
		$this->caSearchableFields = array('area_name', 'area_description');
	}

	public function setDefaultValues(){
		if(!$this->getFieldValue("area_parent_id")){
			$this->setFieldValue('area_parent_id', 'null');
		}

		if(!$this->getFieldValue("resource")){
			$this->setFieldValue('resource', 'null');
		}

		if(!$this->getFieldValue("group")){
			$this->setFieldValue('group', 'null');
		}

		if(!$this->getFieldValue("resourceSubstitute")){
			$this->setFieldValue('resourceSubstitute', 'null');
		}

		if(!$this->getFieldValue("groupSubstitute")){
			$this->setFieldValue('groupSubstitute', 'null');
		}

		if(!$this->getFieldValue("area_place")){
			$this->setFieldValue('area_place', 'null');
		}

		if(!$this->getFieldValue("area_priority")){
			$this->setFieldValue('area_priority', 'null');
		}

		if(!$this->getFieldValue("area_type")){
			$this->setFieldValue('area_type', 'null');
		}
	}

	public function setSubstitute($id) {
		$this->setFieldValue('groupSubstitute',"null");
		$this->setFieldValue('resourceSubstitute',"null");
		if($id){
			$group = new CMGroup();
			$resource = new CMResource();

			if($group->fetchById($id)){
				$this->setFieldValue('groupSubstitute',$id);

			}else if($resource->fetchById($id)){
				$this->setFieldValue('resourceSubstitute',$id);

			}
		}
	}

	public function setGroup($id) {
		$this->setFieldValue('resource',"null");
		$this->setFieldValue('group',"null");
		if($id){
			$group = new CMGroup();
			$resource = new CMResource();

			if($group->fetchById($id)){
				$this->setFieldValue('group',$id);
			}else if($resource->fetchById($id)){
				$this->setFieldValue('resource',$id);
			}
		}
	}

	public function getGroup() {
		if($this->getFieldValue('resource') && $this->getFieldValue('resource') != "null"){
			return $this->getFieldValue('resource');
		}
		return $this->getFieldValue('group');
	}

	public function getSubstitute() {
		if($this->getFieldValue('resourceSubstitute') && $this->getFieldValue('resourceSubstitute') != "null"){
			return $this->getFieldValue('resourceSubstitute');
		}
		return $this->getFieldValue('groupSubstitute');
	}

	public function getGroupName() {
		if($this->getFieldValue('resource')){
			$r = new CMResource();
			$r->fetchById($this->getFieldValue('resource'));
			return $r->getFieldValue('resource_name');
		}
		if($this->getFieldValue('group')){
			$r = new CMGroup();
			$r->fetchById($this->getFieldValue('group'));
			return $r->getFieldValue('group_name');
		}
		return null;
	}

	public function getSubstituteName() {
		if($this->getFieldValue('resourceSubstitute')){
			$r = new CMResource();
			$r->fetchById($this->getFieldValue('resourceSubstitute'));
			return $r->getFieldValue('resource_name');
		}
		if($this->getFieldValue('groupSubstitute')){
			$r = new CMGroup();
			$r->fetchById($this->getFieldValue('groupSubstitute'));
			return $r->getFieldValue('group_name');
		}
		return null;
	}

	/**
	 * Insere uma area.
	 *
	 * <p>M�todo para inserir uma �rea e retorna seu identificador.</p>
	 * @access public
	 *
	 * @param boolean $pbReturnId Indica se deve retornar o id do contexto inserido
	 * @return integer Id do contexto inserido
	 */
	public function insert($pbReturnId = false){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		// pega o nome do usu�rio atual
		$maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
		//acl de inser��o de �rea de primeiro n�vel!
		if( !isset($maACLs['M.RM.1.7']) ){
			return parent::insert($pbReturnId);
		}else{
			if($this->getFieldValue('area_parent_id')==''){
				//usu�rio n�o pode inserir �rea de primeiro n�vel e nao est� setada o id da area_parent
				trigger_error('The area could not be inserted!', E_USER_ERROR);
			}else{
				//inser��o de uma �rea filha
				return parent::insert($pbReturnId);
			}
		}
	}

	/**
	 * Retorna o id do usu�rio que deve aprovar o contexto.
	 *
	 * <p>M�todo para retornar id do usu�rio que deve aprovar o contexto.</p>
	 * @access public
	 * @return integer Id do aprovador
	 */
	public function getApprover() {return null;}

	/**
	 * Retorna o id do usu�rio respons�vel pelo contexto.
	 *
	 * <p>M�todo para retornar id do usu�rio respons�vel pelo contexto.</p>
	 * @access public
	 * @return integer Id do respons�vel
	 */
	public function getResponsible() {return null;}

	/**
	 * Retorna o valor do contexto.
	 *
	 * <p>M�todo para retornar o valor do contexto.</p>
	 * @access public
	 * @return integer valor do contexto
	 */
	public function getValue(){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		return $this->getFieldValue('area_value');
	}

	/**
	 * Retorna o nome do contexto.
	 *
	 * <p>M�todo para retornar o nome do contexto.</p>
	 * @access public
	 * @return string Nome do contexto
	 */
	public function getName() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		return $this->getFieldValue('area_name');
	}

	/**
	 * Retorna a descri��o do contexto.
	 *
	 * <p>M�todo para retornar a descri��o do contexto.</p>
	 * @access public
	 * @return string Descri��o do contexto
	 */
	public function getDescription() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		return $this->getFieldValue('area_description');
	}

	/**
	 * Retorna os pais de uma �rea.
	 *
	 * <p>M�todo para retornar os pais de uma �rea.</p>
	 * @access public
	 * @param integer $piAreaId Id da �rea
	 * @param boolean $pbExecByTrash se o metodo est� sendo chamado pela lixeira
	 * @return array Array de ids das �reas pais
	 */
	public function getAreaParents($piAreaId,$pbExecByTrash = false) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		$moQuery = new QueryAreaParents(FWDWebLib::getConnection(),$pbExecByTrash);
		$moQuery->setArea($piAreaId);
		$moQuery->makeQuery();
		$moQuery->executeQuery();
		return $moQuery->getParentAreas();
	}

	/**
	 * Retorna os ancestrais do contexto.
	 *
	 * <p>M�todo para retornar todos contextos acima de um determinado contexto na
	 * �rvore de depend�ncias.</p>
	 * @access public
	 * @param integer $piContextId Id do contexto
	 * @param boolean $pbExecByTrash se o metodo est� sendo chamado pela lixeira
	 * @return array Array de ids dos ancestrais do contexto
	 */
	public function getSuperContexts($piContextId,$pbExecByTrash = false){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		return $this->getAreaParents($piContextId,$pbExecByTrash);
	}

	/*
	 * Verifica se tem sub-areas
	 */
	private function getSubAreas($contextId){
	    $maParameters = func_get_args();
	    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

	    $rmArea = new RMArea();
	    $rmArea->createFilter("$contextId", "area_parent_id", "=");
	    $rmArea->select();

	    $areas = array();
	    while($rmArea->fetch()){
	    	$areas[] = $rmArea->getFieldValue("area_id");
	    }

	    if(count($areas))
	    	return $areas;
	    else
	    	return null;
	}

	/*
	 * Pega os processos da area
	 */
	private function getProcessFromArea($contextId){
	    $maParameters = func_get_args();
	    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

	    $rmProcess = new RMProcess();
	    $rmProcess->createFilter($contextId, "process_area_id");
	    $rmProcess->select();

	    $processes = array();
	    while($rmProcess->fetch()){
	    	$processes[] = $rmProcess->getFieldValue("process_id");
	    }

	    if(count($processes))
	    	return $processes;
	   	else
	   		return null;
	}	

	/*
	 * Pega os planos da area
	 */
	private function getPlanFromArea($contextId){
	    $maParameters = func_get_args();
	    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

	    $cmPlan = new CMPlan();
	    $cmPlan->createFilter($contextId, "process");
	    $cmPlan->select();

	    $plans = array();
	    while($cmPlan->fetch()){
	    	$plans[] = $cmPlan->getFieldValue("id");
	    }

	    if(count($plans))
	    	return $plans;
	   	else
	   		return null;
	}

	/*
	 * Pega os recursos da area
	 */
	private function getResourceFromArea($contextId){
	    $maParameters = func_get_args();
	    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

	    $cmAreaResource = new CMAreaResource();
	    $cmAreaResource->createFilter($contextId, "area_id");
	    $cmAreaResource->select();

	    $resources = array();
	    while($cmAreaResource->fetch()){
	    	$resources[] = $cmAreaResource->getFieldValue("id");
	    }

	    if(count($resources))
	    	return $resources;
	   	else
	   		return null;
	}

	/*
	 * Pega os fornecedores da area
	 */
	private function getProviderFromArea($contextId){
	    $maParameters = func_get_args();
	    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

	    $rmAreaProvider = new RMAreaProvider();
	    $rmAreaProvider->createFilter($contextId, "area_id");
	    $rmAreaProvider->select();

	    $providers = array();
	    while($rmAreaProvider->fetch()){
	    	$providers[] = $rmAreaProvider->getFieldValue("provider_id");
	    }

	    if(count($providers))
	    	return $providers;
	   	else
	   		return null;
	}	

	/**
	 * Retorna os sub-contextos de uma �rea (sub-�reas/processos).
	 *
	 * <p>M�todo para retornar os sub-contextos de uma �rea (sub-�reas/processos).</p>
	 * @access public
	 * @param integer $piAreaId Id da �rea
	 * @return array Array de ids dos sub-contextos desta �rea
	 */
	public function verifySubContexts($contextId) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		$subAreas = $this->getSubAreas($contextId);

		if($subAreas)
			return true;

		$processes = $this->getProcessFromArea($contextId);

		if($processes)
			return true;	

		$plans = $this->getPlanFromArea($contextId);

		if($plans)
			return true;

		$resources = $this->getResourceFromArea($contextId);

		if($resources)
			return true;

		$providers = $this->getProviderFromArea($contextId);

		if($providers)
			return true;							
			
		return false;
	}

	/**
	 * Indica se o contexto � delet�vel ou n�o.
	 *
	 * <p>M�todo que indica se o contexto � delet�vel ou n�o.</p>
	 * @access public
	 * @param integer $piContextId id do contexto
	 */
	public function isDeletable($piContextId) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		if (!ISMSLib::getConfigById(GENERAL_CASCADE_ON)) {
			// cascade off: verificar exist�ncia de subcontextos
			if ($this->verifySubContexts($piContextId))
			return false;
		}
		return true;
	}

	/**
	 * Exibe uma popup caso n�o seja poss�vel remover um contexto.
	 *
	 * <p>M�todo que exibe uma popup caso n�o seja poss�vel remover um contexto.</p>
	 * @access public
	 * @param integer $piContextId id do contexto
	 */
	public function showDeleteError($piContextId) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		$msTitle = FWDLanguage::getPHPStringValue('tt_area_remove_erorr','Erro ao remover Unidade de Neg�cio');
		$msMessage = FWDLanguage::getPHPStringValue('st_area_remove_error_message',"N�o foi poss�vel remover a Unidade de Neg�cio <b>%area_name%</b>: A �rea cont�m Sub-�reas, Processos, Planos, Contatos corporativos e Contatos externos. Para remover �reas de Neg�cio, ative a op��o \"Delete Cascade\" na aba \"Admin -> Configura��o\".");
		$moArea = new RMArea();
		$moArea->fetchById($piContextId);
		$msAreaName = $moArea->getFieldValue('area_name');
		$msMessage = str_replace("%area_name%",$msAreaName,$msMessage);
		ISMSLib::openOk($msTitle,$msMessage,"",80);
	}

	/**
	 * Deleta logicamente um contexto.
	 *
	 * <p>M�todo para deletar logicamente um contexto.</p>
	 * @access public
	 * @param integer $piContextId id do contexto
	 */
	public function logicalDelete($piContextId) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

	    $subAreas = null;
	    $subAreas = $this->getSubAreas($piContextId);

	    if($subAreas) {
		    foreach($subAreas as $value){
		    	$rmArea = new RMArea();
		    	$rmArea->delete($value);
		    }
		}

	    $processes = null;
	    $processes = $this->getProcessFromArea($piContextId);

	    if($processes) {
		    foreach($processes as $value){
		    	$rmProcess = new RMProcess();
		    	$rmProcess->delete($value);
		    }
		}

	    $plans = null;
	    $plans = $this->getPlanFromArea($piContextId);

	    if($plans) {
		    foreach($plans as $value){
		    	$cmPlan = new CMPlan();
		    	$cmPlan->delete($value);
		    }
		}

	    $resources = null;
	    $resources = $this->getResourceFromArea($piContextId);

	    if($resources) {
		    foreach($resources as $value){
		    	$cmResource = new CMResource();
		    	$cmResource->delete($value);
		    }
		}

	    $providers = null;
	    $providers = $this->getProviderFromArea($piContextId);

	    if($providers) {
		    foreach($providers as $value){
		    	$rmProvider = new CMProvider();
		    	$rmProvider->delete($value);
		    }
		}		

	}

	/**
	 * Retorna o �cone do contexto.
	 *
	 * <p>M�todo para retornar o �cone do contexto.</p>
	 * @access public
	 * @param integer $piContextId Id do contexto
	 * @return string Nome do �cone
	 */
	public function getIcon($piContextId = 0){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		return 'icon-area_gray.gif';
	}

	/**
	 * Retorna o label do contexto.
	 *
	 * <p>M�todo para retornar o label do contexto.</p>
	 * @access public
	 * @return string Label do contexto
	 */
	public function getLabel() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		return FWDLanguage::getPHPStringValue('mx_un', "Unidade de neg�cio");
	}

	/**
	 * Retorna o caminho do contexto para a cria��o do pathscroll.
	 *
	 * <p>M�todo para retornar o caminho do contexto para a cria��o do pathscroll.</p>
	 * @access public
	 * @param integer $piTab id da tab
	 * @param integer $piContextType id do contexto
	 * @param string $psContextFunc fun��o que deve ser utilizada para gerar o c�digo do link
	 * @return array Array contendo o Caminho do contexto
	 */
	public function getSystemPathScroll($piTab, $piContextType, $context, $title){

		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		// Se n�o tiver id, mostra s� a 'Raiz'
		if(!$context){
			$msHome = FWDLanguage::getPHPStringValue('st_home','Raiz');

			$maPath = array();
			$maPath[] = '';
			$maPath[] = $msHome;
			$maPath[] = '';
			$maPath[] = '';	

			return $maPath;
		}

		$maPath = array();
		$contextIds = explode(":", $context);

		$areaId = 0;
		$placeId = 0;

		$idsCount = count($contextIds);
		if($idsCount >= 1)
			$areaId = $contextIds[0];

		if($idsCount >= 2)
			$placeId = $contextIds[1];


		if($placeId){

			$place = new CMPlace();

		  	// Sub-Locais
			$placeParents = array_reverse($place->getPlaceParents($placeId));
			$parentsTotal = count($placeParents);

			$maPath = array();

			for($i=0; $i<$parentsTotal; $i++){

	    		$cmPlace = new CMPlace();
	    		$parentPlaceId = $placeParents[$i];

	    		$cmPlace->fetchById($parentPlaceId);

	    		if($i == 0) {
	    			$maPath[] = "<a href='javascript:isms_redirect_to_mode_scroll($piTab, " . CONTEXT_CM_PLACE . ", 0)'>";
			    } else {

			    	$returnPlaceId = $placeParents[$i - 1];
			    	$maPath[] = "<a href='javascript:enterPlace($returnPlaceId)'>";
			   	}

			   	$maPath[] = $cmPlace->getName();
			   	$maPath[] = "";
			   	$maPath[] = "</a>";

			}

			// Se j� estivermos num path, mudar o link. Se estivermos apontando para 1 item no path, esse volta para a Raiz.
			if($parentsTotal){
		   		$returnPlaceId = $placeParents[$parentsTotal-1];
		   		$maPath[] = "<a href='javascript:enterPlace($returnPlaceId)'>";
		   	} else {
		   		$maPath[] = "<a href='javascript:isms_redirect_to_mode_scroll($piTab, ". CONTEXT_CM_PLACE . ", 0)'>";
		   	}

		   	$cmPlace = new CMPlace();
		   	$cmPlace->fetchById($placeId);
		   	
		   	$maPath[] = $cmPlace->getName();
			$maPath[] = "";
			$maPath[] = "</a>";

		}

		if($areaId){

			// Sub unidade de neg�cio...
			$areaParents = array_reverse($this->getAreaParents($areaId));
			foreach($areaParents as $areaParentId){

				$area = new RMArea();
				$area->fetchById($areaParentId);
				$areaName = $area->getName();

			    $maPath[] = "<a href='javascript:isms_redirect_to_mode_scroll($piTab, ". CONTEXT_AREA . ", $placeId);'>";
			   	$maPath[] = $areaName;
			   	$maPath[] = '';
			   	$maPath[] = "</a>";

			}

			$area = new RMArea();
			$area->fetchById($areaId);
			$areaName = $area->getName();

		    $totalParents = count($areaParents);
		    if($totalParents) {
		    	$returnAreaParentId = $totalParents - 1;
		    	$maPath[] = "<a href='javascript:enter_area($areaParents[$returnAreaParentId])'>";
		    } else if ($placeId) {
		    	$maPath[] = "<a href='javascript:isms_redirect_to_mode_scroll($piTab, ". CONTEXT_AREA . ", $placeId);'>";
		    } else {
		    	$maPath[] = "<a href='javascript:isms_redirect_to_mode_scroll($piTab, ". CONTEXT_AREA . ", 0);'>";
		    }
		    
		   	$maPath[] = $areaName;
		   	$maPath[] = '';
		   	$maPath[] = "</a>";
	   	}
	   	
	    $maPath[] = '';
	   	$maPath[] = $title;
	   	$maPath[] = '';
	   	$maPath[] = '';

		$miIContPath = count($maPath);
		
		for($miI=$miIContPath-1;$miI>0;$miI--){
			if((isset($maPath[$miI-7]))&&($maPath[$miI-7]!=''))
			$maPath[$miI-3] = $maPath[$miI-7];
			$miI=$miI-3;
		}

		return $maPath;
	}

	/**
	 * Retorna o caminho do contexto.
	 *
	 * <p>M�todo para retornar o caminho do contexto.</p>
	 * @access public
	 * @return string Caminho do contexto
	 */
	public function getSystemPath() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		$maPath = array();
		$maPath[] = "<a href='javascript:isms_redirect_to_mode(" . RISK_MANAGEMENT_MODE . ",0,0)'>".FWDLanguage::getPHPStringValue('st_rm_module_name','Gest�o de Riscos')."</a>";

		$miAreaId = $this->getFieldValue('area_id');
		$maAreaParents = array_reverse($this->getAreaParents($miAreaId));
		foreach ($maAreaParents as $miAreaParentId) {
			$moAreaParent = new RMArea();
			$moAreaParent->fetchById($miAreaParentId);
			$maPath[] = ISMSLib::getIconCode('icon-area_',$moAreaParent->getValue(),-4) . "<a href='javascript:isms_redirect_to_mode(" . RISK_MANAGEMENT_MODE . "," . $this->ciContextType . "," . $miAreaParentId . ")'>" . $moAreaParent->getName() . "</a>";
			unset($moAreaParent);
		}
		$maPath[] = ISMSLib::getIconCode('icon-area_',$this->getValue(),-4) . "<a href='javascript:isms_redirect_to_mode(" . RISK_MANAGEMENT_MODE . "," . $this->ciContextType . "," . $miAreaId . ")'>" . $this->getName() . "</a>";

		return implode("&nbsp;".ISMSLib::getIconCode('icon-arrow-right.gif',-2,0).'&nbsp;', $maPath);
	}

	/**
	 * Retorna os leitores e o respons�vel da �rea.
	 *
	 * <p>Retorna os leitores e o respons�vel da �rea.</p>
	 * @access public
	 * @return array Array com leitores + respons�vel da �rea.
	 */
	public function getReadersAndResponsible($piAreaId) {return null;}

	/**
	 * Atualiza os leitores do documento da �rea.
	 *
	 * <p>M�todo para atualizar os leitores do documento da �rea.</p>
	 * @access public
	 * @param integer $piAreaId Id da �rea
	 */
	public function updateReaders($piAreaId) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		/*
		 * Busca, a partir dos usu�rios dos processos, todos os usu�rios
		 * que est�o indiretamente relacionados com a �rea (e sub�res).
		 * �rea -> [Sub�reas] -> Processos
		 */
		$moQuery = new QueryAreaAutoReaders(FWDWebLib::getConnection());
		$moQuery->setArea($piAreaId);
		$moQuery->makeQuery();
		$moQuery->executeQuery();
		$maUsers = $moQuery->getReaders();

		/*
		 * Para cada documento da �rea, atualiza seus leitores
		 */
		$moDocContext = new PMDocContext();
		$moDocContext->createFilter($piAreaId, 'context_id');
		$moDocContext->select();
		while($moDocContext->fetch()) {
			$miDocId = $moDocContext->getFieldValue('document_id');
			$moDocReaders = new PMDocumentReader();
			$moDocReaders->updateUsers($miDocId, $maUsers);
		}
	}

	/**
	 * Retorna se o usu�rio logado tem permi��o para inserir o contexto.
	 *
	 * <p>M�todo para retornar se o usu�rio tem permi��o para inserir o contexto no sistema.</p>
	 * @access public
	 * @return boolean Se o usu�ro tem permi��o para inserir o contexto no sistema
	 */
	protected function userCanInsert(){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		$mbReturn = false;
		if(ISMSLib::userHasACL('M.RM.1.7') ||  ISMSLib::userHasACL('M.RM.1.6') ){
			$mbReturn = true;
		}
		return $mbReturn;
	}

	public function userCanEdit($piContextId,$piUserResponsible = 0){return true;}

	protected function userCanDelete($piContextId){return true;}

	/**
	 * Retorna o c�digo HTML do icone do contexto.
	 *
	 * <p>M�todo para retornar o c�digo HTML do contexto</p>
	 * @access public
	 * @param integer $piId Id do contexto
	 * @param integer $pbIsFetched id do contexto
	 * @return string contendo o c�digo HTML do icone do contexto
	 */
	public function getIconCode($piId, $pbIsFetched = false){
		if(!$pbIsFetched){
			$this->fetchById($piId);
		}
		return ISMSLib::getIconCode('icon-area_'.RMRiskConfig::getRiskColor($this->getValue()).'.gif',-2,7);
	}

	/**
	 * Retorna o caminho para abrir a popup de edi��o desse contexto.
	 *
	 * <p>M�todo para retornar o caminho para abrir a popup de edi��o desse contexto.</p>
	 * @param integer $piContextId id do contexto
	 * @access public
	 * @return string caminho para abrir a popup de edi��o desse contexto
	 */
	public function getVisualizeEditEvent($piContextId,$psUniqId){
		$soSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
		$msPackagesRiskPath = ($soSession->getMode()==MANAGER_MODE) ? "packages/risk/" : "../../packages/risk/";
		$msPopupId = "popup_area_edit";

		return "isms_open_popup('{$msPopupId}','{$msPackagesRiskPath}{$msPopupId}.php?area=$piContextId','','true',297,440);"
		."soPopUpManager.getPopUpById('{$msPopupId}').setCloseEvent( function close_visualize_$psUniqId() {soPopUpManager.closePopUp('popup_visualize_$psUniqId');} );";
	}

}
?>