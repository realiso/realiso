<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
 /**
 * Classe RMRiskConfig.
 *
 * <p>Classe que retorna as configuracoes para os riscos do sistema.</p>
 * @package ISMS
 * @subpackage classes
 */
class RMRiskConfig {
  
 /**
  * M�todo para retornar a cor do risco
  * 
  * <p>M�todo para retornar a cor do risco, escrita ou em hexadecimal</p>
  * @access public
  * @param $value valor a ser comparado com vari�veis do sistema.
  * @param $hex booleano para retornar em hexadecimal ou escrita.
  */
  static function getRiskColor($piValue, $pbHex = false){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $miRiskLow = ISMSLib::getConfigById(RISK_LOW);
    $miRiskHigh = ISMSLib::getConfigById(RISK_HIGH);
    if($piValue==0){
      $return = (!$pbHex)?"blue":"#0000FF";
    }elseif($piValue <= $miRiskLow){
      $return = (!$pbHex)?"green":"#008000";
    }elseif($piValue < $miRiskHigh){
      $return = (!$pbHex)?"yellow":"#DAA520";
    }else{
      $return = (!$pbHex)?"red":"#990000";
    }
    return $return;
  }

	/**
	 * M�todo para retornar o valor do risco em percentual
	 * 
	 * <p>M�todo para retornar o valor do risco em percentual, dependendo da configura��o.</p>
	 * @access public
	 * @param $piValue valor do risco que ser� comparado
	 */
	 static function getRiskValueFormat($piValue) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
	 	$miPercentualRisk = ISMSLib::getConfigById(GENERAL_PERCENTUAL_RISK);
	 	if ($miPercentualRisk) {
	 		$miRiskCountValue = ISMSLib::getConfigById(RISK_VALUE_COUNT);
	 		$miRiskMaxValue = pow($miRiskCountValue,2);
	 		$return = ($piValue/$miRiskMaxValue);
	 		$return *= 100;
	 		$return = number_format($return,2);
	 	} else
	 		$return = number_format($piValue,2);
	 	
	 	return $return;
	 }
}
?>