<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Interface RMIValue. Interface para controle de valores de rm
 * 
 * <p>Interface para controle de valores de rm</p>
 *
 * @package ISMS2
 * @subpackage interface
 */
 
 interface RMIValue
 {
 
	 /**
	 * Define a id do valor
	 * 
	 * <p>M�todo para definir a id do valor.</p>
	 * @access public
	 */	
	 public function setValueId($piValueId);
	 
	 /**
	 * Define a id do par�metro
	 * 
	 * <p>M�todo para definir a id do par�metro.</p>
	 * @access public
	 */	
	 public function setParameterId($piParameterId);
	 
	 /**
	 * Define a id do elemento
	 * 
	 * <p>M�todo para definir a id do elemento.</p>
	 * @access public
	 */	
	 public function setElementId($piElementId);
	 
	 /**
	 * Define a justificativa do elemento
	 * 
	 * <p>M�todo para definir a justificativa do elemento.</p>
	 * @access public
	 */	
	 public function setJustification($psJustification);
	 
	 /**
	 * Cria um filtro para o id do elemento
	 * 
	 * <p>M�todo para criar um filtro para o id do elemento.</p>
	 * @access public
	 */	
	 public function createElementIdFilter($piElementId);
	 
	 /**
	 * Cria um filtro para o id do par�metro
	 * 
	 * <p>M�todo para criar um filtro para o id do par�metro.</p>
	 * @access public
	 */	
	 public function createParameterIdFilter($piParameterId);
	 
 }
 
?>