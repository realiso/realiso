<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe RMEvent.
 *
 * <p>Classe que representa a tabela de eventos.</p>
 * @package ISMS
 * @subpackage classes
 */
class RMEvent extends ISMSContext implements IWorkflow {
  
 /**
  * Construtor.
  * 
  * <p>Construtor da classe RMEvent.</p>
  * @access public 
  */
  public function __construct(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    parent::__construct("rm_event");
    $this->ciContextType = CONTEXT_EVENT;
    $this->csAliasId = "event_id";
    $this->csDependenceAliasId = "event_category_id";
    $this->coWorkflow = new RMWorkflow(ACT_EVENT_APPROVAL);

    $this->coDataset->addFWDDBField(new FWDDBField("fkContext",     "event_id",           DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("fkCategory",    "event_category_id",  DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("sDescription",  "event_description",  DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField("tObservation",  "event_observation",  DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField("bPropagate",    "event_propagate",    DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("fkType",        "event_type",         DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("tImpact",       "event_impact",         DB_STRING));

    $this->caSensitiveFields = array('event_description');
    $this->caSearchableFields = array('event_description', 'event_observation');
  }
  
 /**
  * Retorna o id do usu�rio que deve aprovar o contexto.
  *
  * <p>M�todo para retornar id do usu�rio que deve aprovar o contexto.</p>
  * @access public
  * @return integer Id do aprovador
  */
  public function getApprover(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return ISMSLib::getConfigById(USER_LIBRARIAN);
  }

 /**
  * Retorna o id do usu�rio respons�vel pelo contexto.
  *
  * <p>M�todo para retornar id do usu�rio respons�vel pelo contexto.</p>
  * @access public
  * @return integer Id do respons�vel
  */
  public function getResponsible(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return $this->getCreator();
  }
 
 /**
  * Retorna o nome do contexto.
  * 
  * <p>M�todo para retornar o nome do contexto.</p>
  * @access public 
  * @return string Nome do contexto
  */
  public function getName() {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    return $this->getFieldValue('event_description');
  }
  
 /**
  * Retorna a descri��o do contexto.
  * 
  * <p>M�todo para retornar a descri��o de um contexto.</p>
  * @access public 
  * @return string Descri��o do contexto
  */
  public function getDescription(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    return $this->getFieldValue('event_observation');
  }

 /**
  * Seta o estado do contexto.
  * 
  * <p>M�todo para setar o estado de um contexto.
  * Se n�o for passado como par�metro o id do contexto,
  * pega o id do objeto atual.</p>
  * @access public
  * @param integer $piState Estado do contexto
  * @param integer $piContextId Id do contexto
  */ 
  public function setContextState($piState, $piContextId = 0) {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $miId = $piContextId ? $piContextId : $this->ciContextId;
    $moContext = new ISMSContextObject();
    $moContext->setFieldValue('context_state', $piState);
    $moContext->update($miId);
    if ($piState == CONTEXT_STATE_APPROVED && $this->getFieldValue('event_propagate')) {
      $moRisk = new RMRisk();
      $moRisk->setFieldValue("risk_value",0);
      $moRisk->setFieldValue("risk_residual_value",0);
      $moRisk->setFieldValue("risk_event_id",$this->getFieldValue("event_id"));
      $moRisk->setFieldValue("risk_name",$this->getFieldValue("event_description"));
      
      $moConfig = new ISMSConfig();
      
      $moAsset = new RMAsset();
      $moAsset->createFilter($this->getFieldValue("event_category_id"), 'asset_category_id');
      $moAsset->select();
      while ($moAsset->fetch()) {        
        if ($moAsset->getContextState($moAsset->getFieldValue('asset_id')) != CONTEXT_STATE_DELETED) {
          $moRisk->setFieldValue("risk_asset_id",$moAsset->getFieldValue("asset_id"));
          $miRiskId = $moRisk->insert(true);
          
          $moAlert = new WKFAlert();
          $moAlert->setFieldValue('alert_receiver_id',$moAsset->getResponsible());
          $moAlert->setFieldValue('alert_date',ISMSLib::ISMSTime());
          $moAlert->setFieldValue('alert_type',WKF_ALERT_SUGGESTED_EVENT_CREATED);
          $moAlert->setFieldValue('alert_justification',' ');
          $moAlert->setFieldValue('alert_creator_id',$this->getResponsible());
          $moAlert->setFieldValue('alert_context_id',$miRiskId);
          $moAlert->insert();
        }
      }
      
      $moEvent = new RMEvent();
      $moEvent->setFieldValue('event_propagate', 0);
      $moEvent->update($miId);
    }
  }
  
 /**
  * Retorna os pais do subordinante do contexto.
  * 
  * <p>M�todo para retornar os pais do subordinante do contexto..</p>
  * @access public 
  * @param boolean $pbExecByTrash se o metodo est� sendo chamado pela lixeira
  * @return array Array de ids dos pais do subordinante do contexto
  */ 
  public function getEventParents($pbExecByTrash = false) {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $moCategory = new RMCategory();
    $miParentId = $this->getFieldValue($this->csDependenceAliasId);
    $maParents = $moCategory->getCategoryParents($miParentId,$pbExecByTrash);
    $maParents[] = $miParentId;
    return $maParents;
  }

 /**
  * Retorna os ancestrais do contexto.
  * 
  * <p>M�todo para retornar todos contextos acima de um determinado contexto na
  * �rvore de depend�ncias.</p>
  * @access public
  * @param integer $piContextId Id do contexto
  * @param boolean $pbExecByTrash se o metodo est� sendo chamado pela lixeira
  * @return array Array de ids dos ancestrais do contexto
  * /
  public function getSuperContexts($piContextId,$pbExecByTrash = false){
    $miCategoryId = $this->getFieldValue('event_category_id')->getValue();
    if(!$miCategoryId){
      $moEvent = new RMEvent();
      $moEvent->fetchById($piContextId);
      $miCategoryId = $moEvent->getFieldValue('event_category_id')->getValue();
    }
    $moCategory = new RMCategory();
    $maSuperContexts = $moCategory->getSuperContexts($miCategoryId,$pbExecByTrash);
    $maSuperContexts[] = $miCategoryId;
    return $maSuperContexts;
  }*/

 /**
  * Retorna o �cone do contexto.
  *
  * <p>M�todo para retornar o �cone do contexto.</p>
  * @access public
  * @param integer $piContextId Id do contexto
  * @return string Nome do �cone
  */
  public function getIcon($piContextId = 0){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    return 'icon-event.gif';
  }

 /**
  * Retorna o label do contexto.
  * 
  * <p>M�todo para retornar o label do contexto.</p>
  * @access public 
  * @return string Label do contexto
  */
  public function getLabel() {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    return FWDLanguage::getPHPStringValue('mx_event', "Evento");
  }
  
 /**
  * Retorna o caminho do contexto.
  * 
  * <p>M�todo para retornar o caminho do contexto.</p>
  * @access public 
  * @return string Caminho do contexto
  */ 
  public function getSystemPath() {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
  	$miCategoryId = $this->getFieldValue('event_category_id');
  	$moCategory = new RMCategory();
  	$moCategory->fetchById($miCategoryId);
  	
  	$msPath = $moCategory->getSystemPath();  	
  	$msPath .= "&nbsp;".ISMSLib::getIconCode('icon-arrow-right.gif',-2,0).'&nbsp;' . ISMSLib::getIconCode('icon-event.gif',-2,-4). "<a href='javascript:isms_redirect_to_mode(" . LIBRARIES_MODE . "," . CONTEXT_CATEGORY . "," . $miCategoryId . ")'>" . $this->getName() . "</a>";
  	
  	return $msPath;
  }
  
 /**
  * Retorna se o usu�rio logado tem permi��o para inserir o contexto.
  * 
  * <p>M�todo para retornar se o usu�rio tem permi��o para inserir o contexto no sistema.</p>
  * @access public
  * @return boolean Se o usu�ro tem permi��o para inserir o contexto no sistema
  */
  protected function userCanInsert(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    //contexto deve ser tratado, assim assume-se que a principio o usu�rio n�o pode inserir tal contexto
    $mbReturn = false;
    $miCount = 0;

    $maPermissions = array( 'M.L.1.8' );
    //acls negadas o usu�rio logado no sistema
    $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
    foreach($maACLs as $miKey => $miValue){
      $maACLs[$miValue]=$miKey;
    }
    //acl1 AND acl2
    $mbAuxPermission = true;
    foreach($maPermissions as $msValue){
      if(isset($maACLs[$msValue])){
        $mbAuxPermission = false;
      }
      $miCount++;
    }
    //caso exista pelo menos 1 acl nas permi��es do contexto e estas permi��es n�o est�o nas permi��es negadas do usu�rio
    if(($mbAuxPermission)&&($miCount)){
      $mbReturn = $mbAuxPermission;
    }
    return $mbReturn;
  }

 /**
  * Retorna se o usu�rio logado tem permi��o para editar o contexto.
  * 
  * <p>M�todo para retornar se o usu�rio tem permi��o para editar o contexto no sistema.</p>
  * @access protected
  * @return boolean Se o usu�ro tem permi��o de editar o contexto no sistema
  */
  public function userCanEdit($piContextId,$piUserResponsible = 0){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    if(!$this->contextIsOfType($piContextId,$this->ciContextType)) return false;
    //contexto deve ser tratado, assim assume-se que a principio o usu�rio n�o pode editar esse contexto
    $mbReturn = false;
    $maPermissions = array( 'M.L.1.5' );
    //acls negadas do usu�rio logado no sistema
    $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
    foreach($maACLs as $miKey => $miValue){
      $maACLs[$miValue]=$miKey;
    }
    $miCount = 0;
    //acl1 AND acl2
    $mbAuxPermission = true;
    foreach($maPermissions as $msValue){
      if(isset($maACLs[$msValue])){
        $mbAuxPermission = false;
      }
      $miCount++;
    }
    //caso exista pelo menos 1 acl nas permi��es do contexto e estas permi��es n�o est�o nas permi��es negadas do usu�rio
    if(($mbAuxPermission)&&($miCount)){
      $mbReturn = $mbAuxPermission;
      }
    return $mbReturn;
  }
 
 /**
  * Retorna se o usu�rio logado tem permi��o para excluir o contexto.
  * 
  * <p>M�todo para retornar se o usu�rio tem permi��o para excluir o contexto no sistema.</p>
  * @access protected
  * @return boolean Se o usu�ro tem permi��o de excluir o contexto no sistema
  */
  protected function userCanDelete($piContextId){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $miContextType = $this->ciContextType;
    if(!$this->contextIsOfType($piContextId,$miContextType)) return false;
    //contexto deve ser tratado, assim assume-se que a principio o usu�rio n�o pode inserir tal contexto
    $mbReturn = false;
    $maPermissions = array( 'M.L.1.6' );

    //acls negadas do usu�rio logado no sistema
    $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
    foreach($maACLs as $miKey => $miValue){
      $maACLs[$miValue]=$miKey;
    }
    $miCount = 0;
    //acl1 AND acl2
    $mbAuxPermission = true;
    foreach($maPermissions as $msValue){
      if(isset($maACLs[$msValue])){
        $mbAuxPermission = false;
      }
      $miCount++;
    }
    //caso exista pelo menos 1 acl nas permi��es do contexto e estas permi��es n�o est�o nas permi��es negadas do usu�rio
    if(($mbAuxPermission)&&($miCount)){
      $mbReturn = $mbAuxPermission;
    }
    return $mbReturn;
  }

  /**
  * Retorna o c�digo HTML do icone do contexto.
  * 
  * <p>M�todo para retornar o c�digo HTML do contexto</p>
  * @access public
  * @param integer $piId Id do contexto
  * @param integer $pbIsFetched id do contexto
  * @return string contendo o c�digo HTML do icone do contexto
  */ 
  public function getIconCode($piId, $pbIsFetched = false){
  	return ISMSLib::getIconCode('icon-event.gif', -2, 7);
  }

 /**
  * Retorna o caminho para abrir a popup de edi��o desse contexto.
  * 
  * <p>M�todo para retornar o caminho para abrir a popup de edi��o desse contexto.</p>
  * @param integer $piContextId id do contexto
  * @access public 
  * @return string caminho para abrir a popup de edi��o desse contexto
  */ 
  public function getVisualizeEditEvent($piContextId,$psUniqId){
    $soSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    $msPackagesLibrariesPath = ($soSession->getMode()==MANAGER_MODE) ? "packages/libraries/" : "../../packages/libraries/";
    $msPopupId = "popup_event_edit";
    
    return "isms_open_popup('{$msPopupId}','{$msPackagesLibrariesPath}{$msPopupId}.php?event=$piContextId','','true',320,400);"
           ."soPopUpManager.getPopUpById('{$msPopupId}').setCloseEvent( function close_visualize_$psUniqId() {soPopUpManager.closePopUp('popup_visualize_$psUniqId');} );";
  }
  
  /**
  * Retorna o identificador do primeiro contexto da rela��o.
  * 
  * <p>M�todo para retornar o identificador do primeiro contexto da rela��o.</p>
  * @access public 
  * @return string Alias
  */
  public function getFirstContextId() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
  	return $this->getFieldValue('event_id');
  }
 
 /**
  * Retorna o identificador do segundo contexto da rela��o.
  * 
  * <p>M�todo para retornar o identificador do segundo contexto da rela��o.</p>
  * @access public 
  * @return string Alias
  */
  public function getSecondContextId() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
  	return $this->getFieldValue('event_category_id');
  }
  
}
?>