<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe RMProcessAsset.
 *
 * <p>Classe que representa a tabela de associa��o de processos com ativos.</p>
 * @package ISMS
 * @subpackage classes
 */
class RMProcessAsset extends ISMSContext  {

	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe RMProcessAsset.</p>
	 * @access public
	 */
	public function __construct(){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		parent::__construct("rm_process_asset");
		$this->ciContextType = CONTEXT_PROCESS_ASSET;
		 
		$this->csAliasId = "process_asset_id";
		 
		$this->coDataset->addFWDDBField(new FWDDBField("fkContext",  "process_asset_id", DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkProcess",  "process_id",       DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkAsset",    "asset_id",         DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkImportance",    "importance",  DB_NUMBER));
	}

	/**
	 * Insere rela��es entre um ativo e diversos processos.
	 *
	 * <p>M�todo para inserir rela��es entre um ativo e diversos processos.</p>
	 * @access public
	 * @param integer $piAssetId Id do ativo
	 * @param array $paProcesses Ids dos processos
	 */
	public function insertProcesses($piAssetId, $paProcesses){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		$this->setFieldValue('asset_id', $piAssetId);
		foreach($paProcesses as $miProcessId){
			$this->setFieldValue('process_id', $miProcessId);
			parent::insert();
		}
	}

	/**
	 * Deleta rela��es entre um ativo e diversos processos.
	 *
	 * <p>M�todo para deletar rela��es entre um ativo e diversos processos.</p>
	 * @access public
	 * @param integer $piAssetId Id do ativo
	 * @param array $paProcesses Ids dos processos
	 */
	public function deleteProcesses($piAssetId, $paProcesses){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		foreach($paProcesses as $miProcessId){
			parent::delete($this->getProcessAssetId($miProcessId, $piAssetId), true);
		}
	}

	/**
	 * Insere rela��es entre um processo e diversos ativos.
	 *
	 * <p>M�todo para inserir rela��es entre um processo e diversos ativos.</p>
	 * @access public
	 * @param integer $piProcessId Id do processo
	 * @param array $paAssets Ids dos ativos
	 */
	public function insertAssets($piProcessId, $paAssets){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		$this->setFieldValue('process_id', $piProcessId);
		foreach($paAssets as $miAssetId){
			$this->setFieldValue('asset_id', $miAssetId);
			parent::insert();
		}
	}

	/**
	 * Deleta rela��es entre um processo e diversos ativos.
	 *
	 * <p>M�todo para deletar rela��es entre um processo e diversos ativos.</p>
	 * @access public
	 * @param integer $piProcessId Id do processo
	 * @param array $paAssets Ids dos ativos
	 */
	public function deleteAssets($piProcessId, $paAssets){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		foreach($paAssets as $miAssetId){
			parent::delete($this->getProcessAssetId($piProcessId, $miAssetId), true);
		}
	}

	/**
	 * Retorna o id do contexto da associa��o.
	 *
	 * <p>M�todo para retornar o id do contexto da associa��o.</p>
	 * @access public
	 * @param integer $piProcessId Id do Processo
	 * @param integer $piAssetId Id do Ativo
	 * @return integer Id da associa��o
	 */
	public function getProcessAssetId($piProcessId, $piAssetId){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		$moRMProcessAsset = new RMProcessAsset();
		$moRMProcessAsset->createFilter($piProcessId, 'process_id');
		$moRMProcessAsset->createFilter($piAssetId, 'asset_id');
		$moRMProcessAsset->select();
		$moRMProcessAsset->fetch();
		return $moRMProcessAsset->getFieldValue('process_asset_id');
	}

	/**
	 * Seta o id do usu�rio que deve aprovar o contexto.
	 *
	 * <p>M�todo para setar id do usu�rio que deve aprovar o contexto.</p>
	 * @access public
	 * @param integer $piApproverId Id do aprovador
	 */
	public function setApprover($piApproverId) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		$this->ciApproverId = $piApproverId;
	}

	/**
	 * Retorna o id do usu�rio que deve aprovar o contexto.
	 *
	 * <p>M�todo para retornar id do usu�rio que deve aprovar o contexto.</p>
	 * @access public
	 * @return integer Id do aprovador
	 */
	public function getApprover() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		$miApproverId = 0;
		if(isset($this->ciApproverId)){
			$miApproverId = $this->ciApproverId;
		}else{
			$moCtxObj = new ISMSContextObject();
			$moCtxObj->fetchById($this->getFieldValue('process_asset_id'));
			$miCtxState = $moCtxObj->getFieldValue('context_state');
			if($miCtxState == CONTEXT_STATE_PENDANT){
				$moProcess = new RMProcess();
				$moProcess->fetchById($this->getFieldValue('process_id'));
				$miApproverId = $moProcess->getResponsible();
			}elseif($miCtxState == CONTEXT_STATE_COAPPROVED){
				$moAsset = new RMAsset();
				$moAsset->fetchById($this->getFieldValue('asset_id'));
				$miApproverId = $moAsset->getResponsible();
			}else{
				//do nothing
			}
		}
		return $miApproverId;
	}

	/**
	 * Retorna o id do usu�rio respons�vel pelo contexto.
	 *
	 * <p>M�todo para retornar id do usu�rio respons�vel pelo contexto.</p>
	 * @access public
	 * @return integer Id do respons�vel
	 */
	public function getResponsible() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		return $this->getCreator();
	}

	/**
	 * Retorna o nome do contexto.
	 *
	 * <p>M�todo para retornar o nome do contexto.</p>
	 * @access public
	 * @return string Nome do contexto
	 */
	public function getName() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		$moProcess = new RMProcess();
		$moProcess->fetchById($this->getFieldValue('process_id'));
		$moAsset = new RMAsset();
		$moAsset->fetchById($this->getFieldValue('asset_id'));
		return $moAsset->getName() . ' -> ' . $moProcess->getName();
	}

	/**
	 * Retorna o label do contexto.
	 *
	 * <p>M�todo para retornar o label do contexto.</p>
	 * @access public
	 * @return string Label do contexto
	 */
	public function getLabel() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		return FWDLanguage::getPHPStringValue('mx_process_asset_association', "Associa��o de Ativo a Processo");
	}

	/**
	 * Retorna o identificador do primeiro contexto da rela��o.
	 *
	 * <p>M�todo para retornar o identificador do primeiro contexto da rela��o.</p>
	 * @access public
	 * @return string Alias
	 */
	public function getFirstContextId() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		return $this->getFieldValue('asset_id');
	}

	/**
	 * Retorna o identificador do segundo contexto da rela��o.
	 *
	 * <p>M�todo para retornar o identificador do segundo contexto da rela��o.</p>
	 * @access public
	 * @return string Alias
	 */
	public function getSecondContextId() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		return $this->getFieldValue('process_id');
	}

	/**
	 * Retorna os ativos de um processo.
	 *
	 * <p>M�todo para retornar os ativos de um processo.</p>
	 * @access public
	 * @param integer $piProcessId Id do processo
	 * @return array $paAssets Ids dos ativos
	 */
	public function getAssetsFromProcess($piProcessId) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		$moProcessAsset = new RMProcessAsset();
		$moProcessAsset->createFilter($piProcessId,'process_id');
		$moProcessAsset->select();
		$maAssets = array();
		while ($moProcessAsset->fetch()) $maAssets[] = $moProcessAsset->getFieldValue('asset_id');
		return $maAssets;
	}

	/**
	 * Retorna os processos de um ativo.
	 *
	 * <p>M�todo para retornar os processos de um ativo.</p>
	 * @access public
	 * @param integer $piAssetId Id do ativo
	 * @return array $paProcesses Ids dos processos
	 */
	public function getProcessesFromAsset($piAssetId) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		$moProcessAsset = new RMProcessAsset();
		$moProcessAsset->createFilter($piAssetId,'asset_id');
		$moProcessAsset->select();
		$maProcesses = array();
		while ($moProcessAsset->fetch()) $maProcesses[] = $moProcessAsset->getFieldValue('process_id');
		return $maProcesses;
	}


	/**
	 * Retorna se o usu�rio logado tem permi��o para inserir o contexto.
	 *
	 * <p>M�todo para retornar se o usu�rio tem permi��o para inserir o contexto no sistema.</p>
	 * @access public
	 * @return boolean Se o usu�ro tem permi��o para inserir o contexto no sistema
	 */
	protected function userCanInsert(){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		/*
		 retorna true diretamente porque a inser��o / dele��o dessa rela��o � considerada na tela como uma edi��o da rela��o
		 e o teste de permiss�o de edi��o ja � feito na tela, assim para inser��o / dele��o n�o � necessario testar as permi��es.
		 */
		return true;
	}

	/**
	 * Retorna se o usu�rio logado tem permi��o para editar o contexto.
	 *
	 * <p>M�todo para retornar se o usu�rio tem permi��o para editar o contexto no sistema.</p>
	 * @access protected
	 * @return boolean Se o usu�ro tem permi��o de editar o contexto no sistema
	 */
	protected function userCanEdit($piContextId,$piUserResponsible = 0){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		//contexto deve ser tratado, assim assume-se que a principio o usu�rio n�o pode editar esse contexto
		$mbReturn = false;
		$moObjContext = new ISMSContextObject();
		$moObj = $moObjContext->getContextObjectByContextId($piContextId);
		if($moObj){
			if($moObj->fetchById($piContextId)){
				if(($moObj->getContextType()==CONTEXT_PROCESS)||($moObj->getContextType()==CONTEXT_ASSET)){
					if(ISMSLib::getCurrentUserId()== $moObj->getResponsible()){
						$mbReturn = true;
					}
				}
			}
		}
		return $mbReturn;
	}

	/**
	 * Retorna se o usu�rio logado tem permi��o para excluir o contexto.
	 *
	 * <p>M�todo para retornar se o usu�rio tem permi��o para excluir o contexto no sistema.</p>
	 * @access protected
	 * @return boolean Se o usu�ro tem permi��o de excluir o contexto no sistema
	 */
	protected function userCanDelete($piContextId){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		$miContextType = $this->ciContextType;
		if(!$this->contextIsOfType($piContextId,$miContextType)) return false;
		/*
		 retorna true diretamente porque a inser��o / dele��o dessa rela��o � considerada na tela como uma edi��o da rela��o
		 e o teste de permiss�o de edi��o ja � feito na tela, assim para inser��o / dele��o n�o � necessario testar as permi��es.
		 */
		return true;
	}

	public function getSystemPathScroll($piTab, $piContextType, $psContextId){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		$maPath = array();
		$id = $psContextId;
		if($id){
			$p = new RMProcess();
			$p->fetchById($id);
			if($p){
				$maPath[]="<a href='javascript:goToProcess();'>";
				$maPath[] = $p->getFieldByAlias("process_name")->getValue();
				$maPath[]=ISMSLib::getIconCode('',-1);
				$maPath[]="</a>";
				$maPath[]="<a href='javascript:goToProcess();'>";
			}
		}
		$maPath[] = FWDLanguage::getPHPStringValue('tt_asset_bl','Ativos');
		$maPath[]=ISMSLib::getIconCode('',-1);
		$maPath[] = "</a>";
		return $maPath;
	}

}
?>