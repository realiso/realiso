<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

class ISMSReportProcessByProcess extends ISMSReport {

	public function init() {
		parent::init();
		$this->coDataSet->addFWDDBField(new FWDDBField("name","name", DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("financialloss","financialloss", DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("ranking","ranking", DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("rto","rto", DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("area","area", DB_STRING));
	}

	public function makeQuery() {
		$moFilter = $this->getFilter();
		$filters = array();

		$maClassifFilter = array();
		$placeType = $moFilter->getPlaceType();
		$join = '';
		if(count($placeType)){
			if(isset($placeType['null'])){
				$maClassifFilter[] = " pl.fkType IS NULL ";
			}else{
				$maClassifFilter[] = " pl.fkType IN ( ".implode(',',$placeType) ." ) ";
			}
			$join .= '	JOIN cm_place_process pp on pp.fkprocess = p.fkcontext
						JOIN view_cm_place_active pl on pl.fkcontext = pp.fkplace ';
		}
		$processType = $moFilter->getProcessType();
		if(count($processType)){
			if(isset($processType['null'])){
				$maClassifFilter[] = " p.fkType IS NULL ";
			}else{
				$maClassifFilter[] = " p.fkType IN ( ".implode(',',$processType) ." ) ";
			}
		}
		$processPriority = $moFilter->getProcessPriority();
		if(count($processPriority)){
			if(isset($processPriority['null'])){
				$maClassifFilter[] = " p.fkPriority IS NULL ";
			}else{
				$maClassifFilter[] = " p.fkPriority IN ( ".implode(',',$processPriority) ." ) ";
			}
		}
		$where = '';
		if(count($maClassifFilter)){
			$where .= " AND (". implode(' OR ',$maClassifFilter).") ";
		}

		if(count($filters)){
			$where = ' AND ( ' . implode(' AND ', $filters) ." ) ";
		}

		$this->csQuery = "	SELECT
								p.sName as name,
								ar.sname as area,
								coalesce(rnk.rto,0) as rto,
								coalesce(rnk.ranking,0) as ranking,
								coalesce(rnk.financialloss,0) as financialloss
							FROM 
								view_rm_process_active p 
								JOIN rm_area ar on ar.fkcontext = p.fkarea
								LEFT JOIN view_process_ranking rnk ON p.fkcontext = rnk.fkprocess
								{$join}
							WHERE 1=1 
								{$where}
							GROUP BY
								p.sName,
								rnk.ranking,
								ar.sname,
								rnk.rto,
								rnk.financialloss
							ORDER BY
								ranking DESC";
						return parent::executeQuery();
	}

	public function drawHeader(){
		foreach($this->caHeaders as $moHeader){
			if($moHeader->getAttrName()=='header_filter'){
				$moHeaderFilter = $moHeader;
			}elseif($moHeader->getAttrName()=='header_filter_item'){
				$moHeaderFilterItem = $moHeader;
			}elseif($moHeader->getAttrName()=='report_header_blank'){
				$moHeaderBlank = $moHeader;
			}else{
				$this->coWriter->drawLine($moHeader,array());
			}
		}
		if ($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());

		$moFilterText = FWDWebLib::getObject('header_filter_text');
		$moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
		$filters = $this->coFilter->getSummary();
		foreach($filters as $maFilter){
			$moFilterText->setValue($maFilter['name']);
			$this->coWriter->drawLine($moHeaderFilter,array());
			foreach($maFilter['items'] as $msFilterItem){
				$moFilterItemText->setValue($msFilterItem);
				$this->coWriter->drawLine($moHeaderFilterItem,array());
			}
		}
		$this->coWriter->drawLine($moHeaderBlank,array());
	}
}
?>