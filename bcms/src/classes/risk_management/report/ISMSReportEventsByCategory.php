<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe ISMSReportEventsByCategory.
 *
 * <p>Classe que implementa o relat�rio de eventos por categoria.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportEventsByCategory extends ISMSReport {
	
	/**
	 * Inicializa o relat�rio.
	 *
	 * <p>M�todo para inicializar o relat�rio.
	 * Necess�rio para funcionar com o XML compilado.</p>
	 * @access public
	 */	
	public function init() {
		parent::init();
				
		$this->coDataSet->addFWDDBField(new FWDDBField("","category_id",DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("","category_name",DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField("","event_id",DB_NUMBER));				
		$this->coDataSet->addFWDDBField(new FWDDBField("","event_description",DB_STRING));
	}
	
	/**
	 * Executa a query do relat�rio.
	 *
	 * <p>M�todo para executar a query do relat�rio.</p>
	 * @access public
	 */
	public function makeQuery() {
		$moFilter = $this->getFilter();						
		
		$this->csQuery = "SELECT ct.fkContext as category_id, ct.sName as category_name,
												e.fkContext as event_id, e.sDescription as event_description
											FROM isms_context cont_c
											JOIN " . FWDWebLib::getFunctionCall("get_category_tree(0,0)") . " ct ON (cont_c.pkContext = ct.fkContext AND cont_c.nState <> " . CONTEXT_STATE_DELETED . ")
											JOIN view_rm_event_active e ON (ct.fkContext = e.fkCategory)";
		$msWhere = "";
		if ($moFilter->getCategory())
			$msWhere .= " WHERE ct.fkContext = " . $moFilter->getCategory();
    
    $maEventClassifType = $moFilter->getEventClassifType();
    $msWhereAux = "";
    if(count($maEventClassifType)){
      if(isset($maEventClassifType['null'])){
        $msWhereAux = " e.fkType IS NULL ";
      }else{
        $msWhereAux = " e.fkType IN ( ".implode(',',array_keys($maEventClassifType)) ." ) ";
      }
    }
		if($msWhereAux){
      if(!$msWhere)
        $msWhere .=" WHERE ";
      else
        $msWhere .= " AND ";
      $msWhere .= $msWhereAux;
    }
    $this->csQuery .= $msWhere . " ORDER BY ct.fkContext, e.fkContext";
		return parent::executeQuery();
	}
	
	/**
	 * Desenha o cabe�alho do relat�rio.
	 *
	 * <p>M�todo para desenhar o cabe�alho do relat�rio.</p>
	 * @access public
	 */
	public function drawHeader(){
    foreach($this->caHeaders as $moHeader){
      if($moHeader->getAttrName()=='header_filter'){
        $moHeaderFilter = $moHeader;
      }elseif($moHeader->getAttrName()=='header_filter_item'){
        $moHeaderFilterItem = $moHeader;
      }elseif($moHeader->getAttrName()=='report_header_blank'){
        $moHeaderBlank = $moHeader;
      }else{
        $this->coWriter->drawLine($moHeader,array());
      }
    }
    if ($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());
    
    $moFilterText = FWDWebLib::getObject('header_filter_text');
    $moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
    $maFilters = $this->coFilter->getSummary();
    foreach($maFilters as $maFilter){
      $moFilterText->setValue($maFilter['name']);
      $this->coWriter->drawLine($moHeaderFilter,array());
      foreach($maFilter['items'] as $msFilterItem){
        $moFilterItemText->setValue($msFilterItem);
        $this->coWriter->drawLine($moHeaderFilterItem,array());
      }
    }
    $this->coWriter->drawLine($moHeaderBlank,array());
  }
}
?>