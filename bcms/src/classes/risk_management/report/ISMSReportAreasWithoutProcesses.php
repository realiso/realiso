<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe ISMSReportAreasWithoutProcesses.
 *
 * <p>Classe que implementa o relat�rio de �rea que n�o possuem processos.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportAreasWithoutProcesses extends ISMSReport {
  
  /**
   * Inicializa o relat�rio.
   *
   * <p>M�todo para inicializar o relat�rio.
   * Necess�rio para funcionar com o XML compilado.</p>
   * @access public
   */  
  public function init() {
    parent::init();
    
    $this->coDataSet->addFWDDBField(new FWDDBField("","area_id",DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("","area_name",DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("","area_value",DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("","area_responsible_id",DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("","area_responsible_name",DB_STRING));    
  }
  
  /**
   * Executa a query do relat�rio.
   *
   * <p>M�todo para executar a query do relat�rio.</p>
   * @access public
   */
  public function makeQuery() {
  
    $moFilter = $this->getFilter();
    $maFilters = array();
    $msWhere ="";

    $maClassifAreaType = $moFilter->getAreaClassifType();
    if(count($maClassifAreaType)){
      if(isset($maClassifAreaType['null'])){
        $maFilters[] = " a.fkType IS NULL ";
      }else{
        $maFilters[] = " a.fkType IN ( ".implode(',',array_keys($maClassifAreaType)) ." ) ";
      }
    }

    $maClassifAreaPriority = $moFilter->getAreaClassifPriority();
    if(count($maClassifAreaPriority)){
      if(isset($maClassifAreaPriority['null'])){
        $maFilters[] = " a.fkPriority IS NULL ";
      }else{
        $maFilters[] = " a.fkPriority IN ( ".implode(',',array_keys($maClassifAreaPriority)) ." ) ";
      }
    }

    if(count($maFilters)){
        $msWhere .= " AND ". implode(' AND ',$maFilters);
    }
  
    $this->csQuery = "SELECT a.fkContext as area_id, a.sName as area_name, a.nValue as area_value, u.fkContext as area_responsible_id, u.sName as area_responsible_name
                      FROM view_rm_area_active a
                      JOIN view_isms_user_active u ON (a.fkResponsible = u.fkContext)
                      WHERE a.fkContext NOT IN (SELECT p.fkArea FROM view_rm_process_active p)
                      $msWhere";
    
    return parent::executeQuery();
  }
  
    /**
   * Desenha o cabe�alho do relat�rio.
   *
   * <p>M�todo para desenhar o cabe�alho do relat�rio.</p>
   * @access public
   */
  public function drawHeader(){
    foreach($this->caHeaders as $moHeader){
      if($moHeader->getAttrName()=='header_filter'){
        $moHeaderFilter = $moHeader;
      }elseif($moHeader->getAttrName()=='header_filter_item'){
        $moHeaderFilterItem = $moHeader;
      }elseif($moHeader->getAttrName()=='report_header_blank'){
        $moHeaderBlank = $moHeader;
      }else{
        $this->coWriter->drawLine($moHeader,array());
      }
    }
    if ($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());
    
    $moFilterText = FWDWebLib::getObject('header_filter_text');
    $moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
    $maFilters = $this->coFilter->getSummary();
    foreach($maFilters as $maFilter){
      $moFilterText->setValue($maFilter['name']);
      $this->coWriter->drawLine($moHeaderFilter,array());
      foreach($maFilter['items'] as $msFilterItem){
        $moFilterItemText->setValue($msFilterItem);
        $this->coWriter->drawLine($moHeaderFilterItem,array());
      }
    }
    $this->coWriter->drawLine($moHeaderBlank,array());
  }
}
?>