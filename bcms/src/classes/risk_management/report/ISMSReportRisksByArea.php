<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe ISMSReportRisksByArea.
 *
 * <p>Classe que implementa o relat�rio de riscos por �rea.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportRisksByArea extends ISMSReport {	
	
	/**
	 * Inicializa o relat�rio.
	 *
	 * <p>M�todo para inicializar o relat�rio.
	 * Necess�rio para funcionar com o XML compilado.</p>
	 * @access public
	 */	
	public function init() {
		parent::init();
		
		$this->coDataSet->addFWDDBField(new FWDDBField("","area_id",DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("","area_name",DB_STRING));		
		$this->coDataSet->addFWDDBField(new FWDDBField("","area_value",DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("","process_id",DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("","process_name",DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField("","process_value",DB_NUMBER));		
		$this->coDataSet->addFWDDBField(new FWDDBField("","asset_id",DB_NUMBER));				
		$this->coDataSet->addFWDDBField(new FWDDBField("","asset_name",DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField("","asset_value",DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("","risk_id",DB_NUMBER));				
		$this->coDataSet->addFWDDBField(new FWDDBField("","risk_name",DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("","risk_impact",DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField("","risk_value",DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("","risk_residual_value",DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("","risk_accept_mode",DB_NUMBER));
	}
	
	/**
	 * Executa a query do relat�rio.
	 *
	 * <p>M�todo para executar a query do relat�rio.</p>
	 * @access public
	 */
	public function makeQuery() {				
		$moFilter = $this->getFilter();
		
		$maFilters = array();
		
		$miLowRisk = ISMSLib::getConfigById(RISK_LOW);
		$miHighRisk = ISMSLib::getConfigById(RISK_HIGH);
		
		$msRiskType = ($moFilter->getRiskValueType() == RISK_VALUE_TYPE_REAL_VALUE) ? 'r.nValue' : 'r.nValueResidual';
		
		if ($moFilter->mustShowHighRisk()) {
			$maFilters[] = '(' . $msRiskType . ' >= ' . $miHighRisk . ')';
		}
		if ($moFilter->mustShowMediumRisk()) {
			$maFilters[] = '(' . $msRiskType . ' > ' . $miLowRisk . ' AND ' . $msRiskType . ' < ' . $miHighRisk . ')';
		}
		if ($moFilter->mustShowLowRisk()) {
			$maFilters[] = '(' . $msRiskType . ' <= ' . $miLowRisk . ' AND ' . $msRiskType . ' <> 0)';
		}
		if ($moFilter->mustShowRiskWithNoValues()) {
			$maFilters[] = '(' . $msRiskType . ' = 0' . ')';
		}		
		
    $miArea = $moFilter->getArea();
    
    $maClassifFilter = array();
    $maRiskClassifType = $moFilter->getRiskClassifType();
    if(count($maRiskClassifType)){
      if(isset($maRiskClassifType['null'])){
        $maClassifFilter[] = " r.fkType IS NULL ";
      }else{
        $maClassifFilter[] = " r.fkType IN ( ".implode(',',array_keys($maRiskClassifType)) ." ) ";
      }
    }
    
    $maAreaClassifType = $moFilter->getAreaClassifType();
    if(count($maAreaClassifType)){
      if(isset($maAreaClassifType['null'])){
        $maClassifFilter[] = " ar.fkType IS NULL ";
      }else{
        $maClassifFilter[] = " ar.fkType IN ( ".implode(',',array_keys($maAreaClassifType)) ." ) ";
      }
    }
    $maAreaClassifPriority = $moFilter->getAreaClassifPriority();
    if(count($maAreaClassifPriority)){
      if(isset($maAreaClassifPriority['null'])){
        $maClassifFilter[] = " ar.fkPriority IS NULL ";
      }else{
        $maClassifFilter[] = " ar.fkPriority IN ( ".implode(',',array_keys($maAreaClassifPriority)) ." ) ";
      }
    }

    if($moFilter->mustOrganizeByProcess()){
      $maProcessClassifType = $moFilter->getProcessClassifType();
      if(count($maProcessClassifType)){
        if(isset($maProcessClassifType['null'])){
          $maClassifFilter[] = " p.fkType IS NULL ";
        }else{
          $maClassifFilter[] = " p.fkType IN ( ".implode(',',array_keys($maProcessClassifType)) ." ) ";
        }
      }
      $maProcessClassifPriority = $moFilter->getProcessClassifPriority();
      if(count($maProcessClassifPriority)){
        if(isset($maProcessClassifPriority['null'])){
          $maClassifFilter[] = " p.fkPriority IS NULL ";
        }else{
          $maClassifFilter[] = " p.fkPriority IN ( ".implode(',',array_keys($maProcessClassifPriority)) ." ) ";
        }
      }
    }

    if(count($maClassifFilter)){
      if ($miArea){
        $msWhere = 'WHERE (' . implode(' OR ', $maFilters) . ') AND ar.fkContext = ' . $miArea;		
        $msWhere .= " AND (". implode(' AND ',$maClassifFilter).") ";
      }else{
        $msWhere = 'WHERE ( ' . implode(' OR ', $maFilters) ." ) ";
        $msWhere .= " AND (". implode(' AND ',$maClassifFilter).") ";
      }
		}else{
      if ($miArea){
        $msWhere = 'WHERE (' . implode(' OR ', $maFilters) . ') AND ar.fkContext = ' . $miArea;		
      }else{
        $msWhere = 'WHERE' . implode(' OR ', $maFilters);
      }
    }
        
		$this->csQuery = "SELECT ar.fkContext as area_id, ar.sName as area_name, ar.nValue as area_value, ";		
		
		$msOrderBy = '';
		if ($moFilter->mustOrganizeByProcess()) {
			$this->csQuery .= "p.fkContext as process_id, p.sName as process_name, p.nValue as process_value, ";
			$msOrderBy = "p.fkContext, ";
		}		
		if ($moFilter->mustOrganizeByAsset()) {
			$this->csQuery .= "a.fkContext as asset_id, a.sName as asset_name, a.nValue as asset_value, ";
			$msOrderBy .= "a.fkContext, ";
		}
				
		$this->csQuery .= "r.fkContext as risk_id, r.sName as risk_name, r.tImpact as risk_impact, r.nValue as risk_value, r.nValueResidual as risk_residual_value, r.nAcceptMode as risk_accept_mode	
												FROM view_rm_area_active ar
												JOIN " . FWDWebLib::getFunctionCall("get_area_tree(0,0)") . " gat ON (ar.fkContext = gat.fkContext)
												JOIN view_rm_process_active p ON (ar.fkContext = p.fkArea)												
												JOIN view_rm_process_asset_active pa ON (p.fkContext = pa.fkProcess)
												JOIN view_rm_asset_active a ON (pa.fkAsset = a.fkContext)												
												JOIN view_rm_risk_active r ON (a.fkContext = r.fkAsset)
												$msWhere
												ORDER BY gat.pkId, ar.fkContext, $msOrderBy r.fkContext";
		
		return parent::executeQuery();
	}
	
	/**
	 * Desenha o cabe�alho do relat�rio.
	 *
	 * <p>M�todo para desenhar o cabe�alho do relat�rio.</p>
	 * @access public
	 */
	public function drawHeader(){
    foreach($this->caHeaders as $moHeader){
      if($moHeader->getAttrName()=='header_filter'){
        $moHeaderFilter = $moHeader;
      }elseif($moHeader->getAttrName()=='header_filter_item'){
        $moHeaderFilterItem = $moHeader;
      }elseif($moHeader->getAttrName()=='report_header_blank'){
        $moHeaderBlank = $moHeader;
      }else{
        $this->coWriter->drawLine($moHeader,array());
      }
    }
    if ($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());
    
    $moFilterText = FWDWebLib::getObject('header_filter_text');
    $moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
    $maFilters = $this->coFilter->getSummary();
    foreach($maFilters as $maFilter){
      $moFilterText->setValue($maFilter['name']);
      $this->coWriter->drawLine($moHeaderFilter,array());
      foreach($maFilter['items'] as $msFilterItem){
        $moFilterItemText->setValue($msFilterItem);
        $this->coWriter->drawLine($moHeaderFilterItem,array());
      }
    }
    $this->coWriter->drawLine($moHeaderBlank,array());
  }
}
?>