<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportEventsByCategoryFilter.
 *
 * <p>Classe que implementa o filtro do relatório de eventos por categoria.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportEventsByCategoryFilter extends FWDReportFilter {
	
	protected $ciCategoryId = 0;
  
  protected $caEventClassifType = array();
	
	public function setCategory($pbCategoryId) {
		$this->ciCategoryId = $pbCategoryId;
	}
	
	public function getCategory() {
		return $this->ciCategoryId;
	}
	
  public function getEventClassifType(){
    return $this->caEventClassifType;
  }

  public function setEventClassifType($paEventClassifType){
    $this->caEventClassifType = $paEventClassifType;
  }

  public function getSummary(){		
		$maFilterCategory = array();
    $maFilters = array();
		if ($this->ciCategoryId){      
      $moCategory = new RMCategory();
    	$moCategory->fetchById($this->ciCategoryId);
      $maFilterCategory[] = $moCategory->getName();
    }else{
    	$maFilterCategory[] = FWDLanguage::getPHPStringValue('st_all', 'Todas');
    }
    		   
    $maFilters[] = array(
      'name' => FWDLanguage::getPHPStringValue('rs_event_category_cl', 'Categoria:'),
      'items' => $maFilterCategory
    );   
    
    $maEventClassifType = $this->getEventClassifType();
    if(count($maEventClassifType)){
      $maFilters[] = array(
        'name' => FWDLanguage::getPHPStringValue('rs_event_types_cl', 'Tipos de Evento:'),
        'items' => $maEventClassifType
      );
    }

    return $maFilters;
  }
}
?>