<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportControlAccompaniment.
 *
 * <p>Classe que implementa o relatório acompanhamento de controles.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportControlAccompaniment extends ISMSFilteredReport {

 /**
  * Inicializa o relatório.
  *
  * <p>Método para inicializar o relatório.
  * Necessário para funcionar com o XML compilado.</p>
  * @access public
  */
  public function init(){
    parent::init();
    $this->coDataSet->addFWDDBField(new FWDDBField('c.fkContext'   ,'control_id'       ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('c.sName'       ,'control_name'     ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('c.bIsActive'   ,'control_is_active',DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('wce.fkSchedule','schedule_id'      ,DB_NUMBER));
  }

 /**
  * Executa a query do relatório.
  *
  * <p>Método para executar a query do relatório.</p>
  * @access public
  */
  public function makeQuery(){
    $moFilter = $this->getFilter();
    $maFilters = array();
    $msWhere = '';
    
    $maClassifType = $moFilter->getControlClassifType();
    if(count($maClassifType)){
      if(isset($maClassifType['null'])){
        $maFilters[] = " c.fkType IS NULL ";
      }else{
        $maFilters[] = " c.fkType IN ( ".implode(',',array_keys($maClassifType)) ." ) ";
      }
    }
    if(count($maFilters)){
      $msWhere .= " WHERE ". implode(' AND ',$maFilters);
    }
    
    $this->csQuery = "SELECT
                        c.fkContext as control_id,
                        c.sName as control_name,
                        c.bIsActive as control_is_active,
                        wce.fkSchedule as schedule_id
                      FROM
                        view_rm_control_active c
                        JOIN wkf_control_efficiency wce ON (wce.fkControlEfficiency = c.fkContext)
                      $msWhere
                      ORDER BY c.bIsActive DESC, c.sName";
    return parent::executeQuery();
  }

}

?>