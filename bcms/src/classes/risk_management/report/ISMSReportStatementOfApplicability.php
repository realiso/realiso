<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe ISMSReportStatementOfApplicability.
 *
 * <p>Classe que implementa o relat�rio de declara��o de aplicabilidade.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportStatementOfApplicability extends ISMSReport {
  
  /**
   * Inicializa o relat�rio.
   *
   * <p>M�todo para inicializar o relat�rio.
   * Necess�rio para funcionar com o XML compilado.</p>
   * @access public
   */  
  public function init() {
    parent::init();
    $this->coDataSet->addFWDDBField(new FWDDBField("","best_practice_id"            ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("","best_practice_name"          ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("","best_practice_justification" ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("","control_id"                  ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("","control_name"                ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("","control_is_active"           ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("","control_risk_id"             ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("","doc_instance_id"             ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("","document_name"               ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("","is_link"                     ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("","file_name"                   ,DB_STRING));
  }
  
  /**
   * Executa a query do relat�rio.
   *
   * <p>M�todo para executar a query do relat�rio.</p>
   * @access public
   */
  public function makeQuery() {
  
    $moFilter = $this->getFilter();
    $msWhere ="";
    $miFilterId = $moFilter->getStandardFilter();
    $maFilters = array();
    
    $maControlClassifType = $moFilter->getControlClassifType();
    if(count($maControlClassifType)){
      if(isset($maControlClassifType['null'])){
        $maFilters[] = " c.fkType IS NULL ";
      }else{
        $maFilters[] = " c.fkType IN ( ".implode(',',array_keys($maControlClassifType)) ." ) ";
      }
    }
    
    if($miFilterId){
    //coluna fkcontext eh ambigua
    $maFilters[] ="  bp.fkContext IN (
                     SELECT bps.fkBestPractice 
                       FROM rm_best_practice_standard bps 
                       JOIN isms_context c ON ( c.pkContext = bps.fkStandard AND c.nState <> " . CONTEXT_STATE_DELETED . " )
                       WHERE fkStandard = $miFilterId
                    )";
    }
    
    if(count($maFilters)){
      $msWhere = " WHERE " . implode(' AND ', $maFilters);
    }
    
    $this->csQuery = "
SELECT bp.fkContext as best_practice_id,
       bp.sName as best_practice_name,
       bp.tJustification as best_practice_justification,
       c.fkContext as control_id,
       c.sName as control_name,
       c.bIsActive as control_is_active,
      (SELECT count(fkRisk) FROM view_rm_risk_control_active WHERE fkControl = c.fkContext) as control_risk_id,
       di.fkContext as doc_instance_id,
       d.sName as document_name,
       di.bislink as is_link,
       di.sFileName as file_name
   FROM view_rm_best_practice_active bp 
   LEFT JOIN view_rm_control_bp_active cbp ON (bp.fkContext = cbp.fkBestPractice)
   LEFT JOIN view_rm_control_active c ON (cbp.fkControl = c.fkContext)
   LEFT JOIN view_pm_doc_context_active dc ON (dc.fkContext = c.fkContext)
   LEFT JOIN view_pm_document_active d ON (d.fkContext = dc.fkDocument)
   LEFT JOIN view_pm_doc_instance_status dis ON (dis.document_id = d.fkContext AND dis.doc_instance_status = ". CONTEXT_STATE_DOC_APPROVED .")
   LEFT JOIN view_pm_doc_instance_active di ON (di.fkContext = dis.doc_instance_id)
   $msWhere
   ORDER BY bp.sName";
   return parent::executeQuery();
  }

  /**
   * Desenha o cabe�alho do relat�rio.
   *
   * <p>M�todo para desenhar o cabe�alho do relat�rio.</p>
   * @access public
   */

  public function drawHeader(){
    foreach($this->caHeaders as $moHeader){
      if($moHeader->getAttrName()=='header_filter'){
        $moHeaderFilter = $moHeader;
      }elseif($moHeader->getAttrName()=='header_filter_item'){
        $moHeaderFilterItem = $moHeader;
      }elseif($moHeader->getAttrName()=='report_header_blank'){
        $moHeaderBlank = $moHeader;
      }else{
        $this->coWriter->drawLine($moHeader,array());
      }
    }
    if ($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());
    
    $moFilterText = FWDWebLib::getObject('header_filter_text');
    $moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
    $maFilters = $this->coFilter->getSummary();
    foreach($maFilters as $maFilter){
      $moFilterText->setValue($maFilter['name']);
      $this->coWriter->drawLine($moHeaderFilter,array());
      foreach($maFilter['items'] as $msFilterItem){
        $moFilterItemText->setValue($msFilterItem);
        $this->coWriter->drawLine($moHeaderFilterItem,array());
      }
    }
    $this->coWriter->drawLine($moHeaderBlank,array());
  }

}
?>