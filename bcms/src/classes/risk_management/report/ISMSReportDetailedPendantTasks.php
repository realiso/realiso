<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe ISMSReportDetailedPendantTasks.
 *
 * <p>Classe que implementa o relatório detalhado de tarefas pendentes.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportDetailedPendantTasks extends ISMSReport {
	
	/**
	 * Inicializa o relatório.
	 *
	 * <p>Método para inicializar o relatório.
	 * Necessário para funcionar com o XML compilado.</p>
	 * @access public
	 */	
	public function init() {
		parent::init();
				
		$this->coDataSet->addFWDDBField(new FWDDBField("","user_id",DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("","user_name",DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField("","context_id",DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("","context_name",DB_STRING));		
		$this->coDataSet->addFWDDBField(new FWDDBField("","task_id",DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("","task_activity",DB_NUMBER));
	}
	
	/**
	 * Executa a query do relatório.
	 *
	 * <p>Método para executar a query do relatório.</p>
	 * @access public
	 */
	public function makeQuery() {
		$moConfig = new ISMSConfig();
    $mbHasRevision = $moConfig->getConfig(GENERAL_REVISION_ENABLED);
    $mbHasTests = $moConfig->getConfig(GENERAL_TEST_ENABLED);    
    $msWhere = ' WHERE wt.nActivity >= 2201 AND wt.nActivity <= 2220 ';
    $maFilterTypes = array();        
    
    if(!$mbHasRevision){
      $maFilterTypes[]= '2208';
    }
    if(!$mbHasTests){
        $maFilterTypes[] = '2216';
    }
    if(count($maFilterTypes)){
      $msWhere .= "AND wt.nActivity NOT IN (".implode(',',$maFilterTypes).") ";
    }    
    
		$this->csQuery = "SELECT u.fkContext as user_id, u.sName as user_name,
												cn.context_id as context_id, cn.context_name as context_name,
												wt.pkTask as task_id, wt.nActivity as task_activity
											FROM view_isms_user_active u
											JOIN wkf_task wt ON (u.fkContext = wt.fkReceiver AND wt.bVisible = 1)
											JOIN context_names cn ON (wt.fkContext = cn.context_id)
											$msWhere
                      ORDER BY u.fkContext, cn.context_type";
		
		return parent::executeQuery();
	}
}
?>