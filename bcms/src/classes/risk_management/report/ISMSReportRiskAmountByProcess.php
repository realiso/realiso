<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe ISMSReportRiskAmountByProcess.
 *
 * <p>Classe que implementa o relat�rio de quantidade de riscos por processo.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportRiskAmountByProcess extends ISMSReport {
  
  /**
   * Inicializa o relat�rio.
   *
   * <p>M�todo para inicializar o relat�rio.
   * Necess�rio para funcionar com o XML compilado.</p>
   * @access public
   */  
  public function init() {
    parent::init();
    
    $this->coDataSet->addFWDDBField(new FWDDBField("","process_id",DB_NUMBER));    
    $this->coDataSet->addFWDDBField(new FWDDBField("","process_name",DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("","process_value",DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("","risk_count",DB_NUMBER));    
  }
  
  /**
   * Executa a query do relat�rio.
   *
   * <p>M�todo para executar a query do relat�rio.</p>
   * @access public
   */
  public function makeQuery() {
  
    $moFilter = $this->getFilter();
    $maFilters = array();
    $msWhere ="";

    $maClassifType = $moFilter->getRiskClassifType();
    if(count($maClassifType)){
      if(isset($maClassifType['null'])){
        $maFilters[] = " r.fkType IS NULL ";
      }else{
        $maFilters[] = " r.fkType IN ( ".implode(',',array_keys($maClassifType)) ." ) ";
      }
    }
    $maProcessClassifType = $moFilter->getProcessClassifType();
    if(count($maProcessClassifType)){
      if(isset($maProcessClassifType['null'])){
        $maFilters[] = " p.fkType IS NULL ";
      }else{
        $maFilters[] = " p.fkType IN ( ".implode(',',array_keys($maProcessClassifType)) ." ) ";
      }
    }
    $maProcessClassifPriority = $moFilter->getProcessClassifPriority();
    if(count($maProcessClassifPriority)){
      if(isset($maProcessClassifPriority['null'])){
        $maFilters[] = " p.fkPriority IS NULL ";
      }else{
        $maFilters[] = " p.fkPriority IN ( ".implode(',',array_keys($maProcessClassifPriority)) ." ) ";
      }
    }

    if(count($maFilters)){
        $msWhere .= " WHERE ". implode(' AND ',$maFilters);
    }

    $this->csQuery = "SELECT p.fkContext as process_id, p.sName as process_name, p.nValue as process_value, count(r.fkContext) as risk_count
                      FROM view_rm_process_active p
                      JOIN view_rm_process_asset_active pa ON (p.fkContext = pa.fkProcess)
                      JOIN view_rm_asset_active a ON (pa.fkAsset = a.fkContext)                      
                      JOIN view_rm_risk_active r ON (a.fkContext = r.fkAsset)                      
                      $msWhere
                      GROUP BY p.fkContext, p.sName, p.nValue
                      ORDER BY risk_count DESC";
    
    return parent::executeQuery();
  }
  
  /**
   * Desenha o cabe�alho do relat�rio.
   *
   * <p>M�todo para desenhar o cabe�alho do relat�rio.</p>
   * @access public
   */
  public function drawHeader(){
    foreach($this->caHeaders as $moHeader){
      if($moHeader->getAttrName()=='header_filter'){
        $moHeaderFilter = $moHeader;
      }elseif($moHeader->getAttrName()=='header_filter_item'){
        $moHeaderFilterItem = $moHeader;
      }elseif($moHeader->getAttrName()=='report_header_blank'){
        $moHeaderBlank = $moHeader;
      }else{
        $this->coWriter->drawLine($moHeader,array());
      }
    }
    if ($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());
    
    $moFilterText = FWDWebLib::getObject('header_filter_text');
    $moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
    $maFilters = $this->coFilter->getSummary();
    foreach($maFilters as $maFilter){
      $moFilterText->setValue($maFilter['name']);
      $this->coWriter->drawLine($moHeaderFilter,array());
      foreach($maFilter['items'] as $msFilterItem){
        $moFilterItemText->setValue($msFilterItem);
        $this->coWriter->drawLine($moHeaderFilterItem,array());
      }
    }
    $this->coWriter->drawLine($moHeaderBlank,array());
  }
}
?>