<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

class ISMSReportAsset extends ISMSReport {

	public function init() {
		parent::init();
		
		$col1 = new FWDDBField("assetName"					,"assetName"				,DB_STRING);
		$col1->setLabel(FWDLanguage::getPHPStringValue('rs_asset','Ativo'));
		$col1->setShowColumn(true);
		
		$col2 = new FWDDBField("assetAlias"					,"assetAlias"				,DB_STRING);
		$col2->setLabel(FWDLanguage::getPHPStringValue('rs_alias','Alias'));
		$col2->setShowColumn(true);
		
		$col3 = new FWDDBField("assetModel"					,"assetModel"				,DB_STRING);
		$col3->setLabel(FWDLanguage::getPHPStringValue('rs_model','Modelo'));
		$col3->setShowColumn(true);
		
		$col4 = new FWDDBField("assetManufacturer"			,"assetManufacturer"		,DB_STRING);
		$col4->setLabel(FWDLanguage::getPHPStringValue('rs_manufacturer','Fabricante'));
		$col4->setShowColumn(true);
		
		$col5 = new FWDDBField("assetVersion"				,"assetVersion"				,DB_STRING);
		$col5->setLabel(FWDLanguage::getPHPStringValue('rs_version','Vers�o'));
		$col5->setShowColumn(true);
		
		$col6 = new FWDDBField("assetBuild"					,"assetBuild"				,DB_STRING);
		$col6->setLabel(FWDLanguage::getPHPStringValue('rs_update','Atualiza��o'));
		$col6->setShowColumn(true);
		
		$col7 = new FWDDBField("assetSerial"				,"assetSerial"				,DB_STRING);
		$col7->setLabel(FWDLanguage::getPHPStringValue('rs_serial','Serial'));
		$col7->setShowColumn(true);
		
		$col8 = new FWDDBField("assetIp"					,"assetIp"					,DB_STRING);
		$col8->setLabel(FWDLanguage::getPHPStringValue('rs_ip','IP'));
		$col8->setShowColumn(true);
		
		$col9 = new FWDDBField("assetMac"					,"assetMac"					,DB_STRING);
		$col9->setLabel(FWDLanguage::getPHPStringValue('rs_mac','MAC'));
		$col9->setShowColumn(true);
		
		$col10 = new FWDDBField("providerName"				,"providerName"				,DB_STRING);
		$col10->setLabel(FWDLanguage::getPHPStringValue('rs_provider','Fornecedor'));
		$col10->setShowColumn(true);
		
		$this->coDataSet->addFWDDBField($col1);
		$this->coDataSet->addFWDDBField($col2);
		$this->coDataSet->addFWDDBField($col3);
		$this->coDataSet->addFWDDBField($col4);
		$this->coDataSet->addFWDDBField($col5);
		$this->coDataSet->addFWDDBField($col6);
		$this->coDataSet->addFWDDBField($col7);
		$this->coDataSet->addFWDDBField($col8);
		$this->coDataSet->addFWDDBField($col9);
		$this->coDataSet->addFWDDBField($col10);
	}

	public function makeQuery() {
		$moFilter = $this->getFilter();
		$filters = array();

		$join = '';
		$where = '';

		if(count($filters)){
			$where = ' AND ( ' . implode(' AND ', $filters) ." ) ";
		}

		$this->csQuery = "	select 
								asset.sname as assetName,
								asset.alias as assetAlias,
								asset.model as assetModel,
								asset.manufacturer as assetManufacturer,
								asset.nversion as assetVersion,
								asset.build as assetBuild,
								asset.serial as assetSerial,
								asset.ip as assetIp,
								asset.mac as assetMac,
								provider.name as providerName
								
							from 
								view_rm_asset_active asset
								left join view_cm_provider_active provider on provider.fkcontext = asset.fkprovider
							order by
								assetName,
								assetAlias";
		return parent::executeQuery();
	}

	public function drawHeader(){
		foreach($this->caHeaders as $moHeader){
			if($moHeader->getAttrName()=='header_filter'){
				$moHeaderFilter = $moHeader;
			}elseif($moHeader->getAttrName()=='header_filter_item'){
				$moHeaderFilterItem = $moHeader;
			}elseif($moHeader->getAttrName()=='report_header_blank'){
				$moHeaderBlank = $moHeader;
			}else{
				$this->coWriter->drawLine($moHeader,array());
			}
		}
		if ($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());

		$moFilterText = FWDWebLib::getObject('header_filter_text');
		$moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
		$filters = $this->coFilter->getSummary();
		foreach($filters as $maFilter){
			$moFilterText->setValue($maFilter['name']);
			$this->coWriter->drawLine($moHeaderFilter,array());
			foreach($maFilter['items'] as $msFilterItem){
				$moFilterItemText->setValue($msFilterItem);
				$this->coWriter->drawLine($moHeaderFilterItem,array());
			}
		}
		$this->coWriter->drawLine($moHeaderBlank,array());
	}
}
?>