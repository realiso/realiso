<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

class ISMSReportBiaQuantitative extends ISMSReport {

	/*private $intervals;

	public function loadIntervals(){

		$moWebLib = FWDWebLib::getInstance();
		$query = new FWDDBDataSet(FWDWebLib::getConnection());
		$query->setQuery("select fkcontext as id, sname as name from cm_damage_matrix order by id");
		$query->addFWDDBField(new FWDDBField('id','id'  ,DB_NUMBER));
		$query->addFWDDBField(new FWDDBField('name','name'  ,DB_STRING));
		$query->execute();

		while($query->fetch()){
			$this->intervals[] = array($query->getFieldByAlias("id")->getValue(), $query->getFieldByAlias("name")->getValue());
		}	
	}*/

	public function init() {
		parent::init();

		
		$col1 = new FWDDBField("","place_name", DB_STRING);
		$col1->setLabel(FWDLanguage::getPHPStringValue('rs_local', 'Local'));
		$col1->setShowColumn(true);
		
		$col2 = new FWDDBField("","name", DB_STRING);
		$col2->setLabel(FWDLanguage::getPHPStringValue('rs_process','Processo'));
		$col2->setShowColumn(true);
		
		$col3 = new FWDDBField("", "area", DB_STRING);
		$col3->setLabel(FWDLanguage::getPHPStringValue('ti_un','Unidade de Neg�cio'));
		$col3->setShowColumn(true);
		
		

		#$this->loadIntervals();

		/*$intervalCount = 1;
		foreach($this->intervals as $key => $value){

			$col = new FWDDBField('', 'f'.$intervalCount, DB_NUMBER);
			$col->setLabel($value[1]);
			$col->setShowColumn(true);

			$this->coDataSet->addFWDDBField($col);

			$intervalCount++;
		}*/

		$col4 = new FWDDBField("", "financialloss", DB_NUMBER);
		$col4->setLabel(FWDLanguage::getPHPStringValue('financial_loss','Perda Financeira'));
		$col4->setShowColumn(true);
		
		$this->coDataSet->addFWDDBField($col1);
		$this->coDataSet->addFWDDBField($col2);
		$this->coDataSet->addFWDDBField($col3);
		$this->coDataSet->addFWDDBField($col4);
	}

	public function makeQuery() {
		$moFilter = $this->getFilter();
		$filters = array();

		$maClassifFilter = array();
		$placeType = $moFilter->getPlaceType();
		$join = '';
		if(count($placeType)){
			if(isset($placeType['null'])){
				$maClassifFilter[] = " pl.fkType IS NULL ";
			}else{
				$maClassifFilter[] = " pl.fkType IN ( ".implode(',',$placeType) ." ) ";
			}
			$join .= '	JOIN cm_place_process pp on pp.fkprocess = p.fkcontext
						JOIN view_cm_place_active pl on pl.fkcontext = pp.fkplace ';
		}
		$processType = $moFilter->getProcessType();
		if(count($processType)){
			if(isset($processType['null'])){
				$maClassifFilter[] = " p.fkType IS NULL ";
			}else{
				$maClassifFilter[] = " p.fkType IN ( ".implode(',',$processType) ." ) ";
			}
		}
		$processPriority = $moFilter->getProcessPriority();
		if(count($processPriority)){
			if(isset($processPriority['null'])){
				$maClassifFilter[] = " p.fkPriority IS NULL ";
			}else{
				$maClassifFilter[] = " p.fkPriority IN ( ".implode(',',$processPriority) ." ) ";
			}
		}

		$place = $moFilter->getPlace();
		if($place){
			$join .= " JOIN CM_PLACE_PROCESS pp on pp.fkprocess = p.fkcontext 
						JOIN view_cm_place_active pl on pl.fkcontext = pp.fkplace";
			$filters[] = " pp.fkplace = {$place} or pp.fkplace in (select * from get_sub_places({$place}))";
		}else{
			$join .= '	JOIN cm_place_process pp on pp.fkprocess = p.fkcontext
						JOIN view_cm_place_active pl on pl.fkcontext = pp.fkplace ';
		}

		$where = '';
		if(count($maClassifFilter)){
			$where .= " AND (". implode(' OR ',$maClassifFilter).") ";
		}

		if(count($filters)){
			$where = ' AND ( ' . implode(' AND ', $filters) ." ) ";
		}

		/* Como o intervalo � din�mico... parametros extras! */
		$extraColumns = '';
		$extraJoin = '';
		$extraGroupBy = '';

		/*$intervalCount = 1;
		foreach($this->intervals as $key => $value){

			#$extraColumns .= 'coalesce(f' . $intervalCount .'.financialImpact,0) as f' . $intervalCount . ', ';
			
			#$extraJoin .= 'LEFT JOIN view_financial_impact_sum as f' . $intervalCount . 
						  ' on f' . $intervalCount . '.scene = rnk.worstscene and f' . $intervalCount . '.period = ' . $value[0] . ' ';

			#$extraGroupBy .= 'f' . $intervalCount . '.financialImpact, ';

			$intervalCount++;
		}*/

		$this->csQuery = "SELECT
							pl.sName as place_name,
							p.sName as name,
							ar.sname as area,
							{$extraColumns}
							ranking.financialLoss as financialloss
						FROM 
							view_rm_process_active p 
							JOIN RM_AREA ar on ar.fkcontext = p.fkarea
							LEFT JOIN view_process_ranking rnk ON p.fkcontext = rnk.fkprocess
							{$extraJoin}
							{$join}
							left join view_process_ranking ranking on ranking.fkprocess = p.fkcontext 
						WHERE 1=1
						{$where}
						GROUP BY
							pl.sName,
							p.sName,
							{$extraGroupBy}
							ar.sname,
							ranking.financialLoss
						ORDER BY
							ranking.financialLoss DESC, p.sname, pl.sName";

		//print "<pre>{$this->csQuery}</pre>";
		return parent::executeQuery();
	}

	public function drawHeader(){
		foreach($this->caHeaders as $moHeader){
			if($moHeader->getAttrName()=='header_filter'){
				$moHeaderFilter = $moHeader;
			}elseif($moHeader->getAttrName()=='header_filter_item'){
				$moHeaderFilterItem = $moHeader;
			}elseif($moHeader->getAttrName()=='report_header_blank'){
				$moHeaderBlank = $moHeader;
			}else{
				$this->coWriter->drawLine($moHeader,array());
			}
		}
		if ($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());

		$moFilterText = FWDWebLib::getObject('header_filter_text');
		$moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
		$filters = $this->coFilter->getSummary();
		foreach($filters as $maFilter){
			$moFilterText->setValue($maFilter['name']);
			$this->coWriter->drawLine($moHeaderFilter,array());
			foreach($maFilter['items'] as $msFilterItem){
				$moFilterItemText->setValue($msFilterItem);
				$this->coWriter->drawLine($moHeaderFilterItem,array());
			}
		}
		$this->coWriter->drawLine($moHeaderBlank,array());
	}
}
?>