<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe ISMSReportRiskImpact.
 *
 * <p>Classe que implementa o relat�rio de impacto dos riscos.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportRiskImpact extends ISMSReport {
	
	/**
	 * Inicializa o relat�rio.
	 *
	 * <p>M�todo para inicializar o relat�rio.
	 * Necess�rio para funcionar com o XML compilado.</p>
	 * @access public
	 */	
	public function init() {
		parent::init();
		
		$this->coDataSet->addFWDDBField(new FWDDBField("","risk_id",DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("","risk_name",DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("","risk_impact_field",DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField("","risk_value",DB_NUMBER));		
		$this->coDataSet->addFWDDBField(new FWDDBField("","risk_residual_value",DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("","risk_accept_mode",DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("","asset_id",DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("","asset_name",DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField("","risk_impact",DB_NUMBER));		
	}
	
	/**
	 * Executa a query do relat�rio.
	 *
	 * <p>M�todo para executar a query do relat�rio.</p>
	 * @access public
	 */
	public function makeQuery() {				
		$moFilter = $this->getFilter();		
		$msParameters = implode(',', $moFilter->getParameters());
    $msWhere = "";
    
    $msClassifFilter = "";
    $maClassifFilter = $moFilter->getRiskClassifType();
    if(count($maClassifFilter)){
      if(isset($maClassifFilter['null'])){
        $msClassifFilter = " r.fkType IS NULL ";
      }else{
        $msClassifFilter = " r.fkType IN ( ". implode(',',array_keys($moFilter->getRiskClassifType())) ." ) ";
      }
    }
		if($msClassifFilter){
      $msWhere = ' WHERE '.$msClassifFilter;
		}			

    
		
		$this->csQuery = "SELECT 	r.fkContext as risk_id, r.sName as risk_name, r.tImpact as risk_impact_field, r.nValue as risk_value, r.nValueResidual as risk_residual_value, r.nAcceptMode as risk_accept_mode, a.fkContext as asset_id, a.sName as asset_name, CASE WHEN sum(pvn.nValue) IS NULL THEN 0 ELSE sum(pvn.nValue) END as risk_impact
											FROM view_rm_risk_active r
											JOIN view_rm_asset_active a ON (r.fkAsset = a.fkContext)
											JOIN rm_risk_value rv ON (r.fkContext = rv.fkRisk AND fkParameterName IN ($msParameters))
											JOIN rm_parameter_value_name pvn ON (rv.fkValueName = pvn.pkValueName)
                      $msWhere
											GROUP BY r.fkContext, r.sName, r.tImpact, r.nValue, r.nValueResidual, r.nAcceptMode, a.fkContext, a.sName
											ORDER BY risk_impact DESC";
											
		return parent::executeQuery();
	}
	
	/**
	 * Desenha o cabe�alho do relat�rio.
	 *
	 * <p>M�todo para desenhar o cabe�alho do relat�rio.</p>
	 * @access public
	 */
	public function drawHeader(){
    foreach($this->caHeaders as $moHeader){
      if($moHeader->getAttrName()=='header_filter'){
        $moHeaderFilter = $moHeader;
      }elseif($moHeader->getAttrName()=='header_filter_item'){
        $moHeaderFilterItem = $moHeader;
      }elseif($moHeader->getAttrName()=='report_header_blank'){
        $moHeaderBlank = $moHeader;
      }else{
        $this->coWriter->drawLine($moHeader,array());
      }
    }
    if ($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());
    
    $moFilterText = FWDWebLib::getObject('header_filter_text');
    $moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
    $maFilters = $this->coFilter->getSummary();
    foreach($maFilters as $maFilter){
      $moFilterText->setValue($maFilter['name']);
      $this->coWriter->drawLine($moHeaderFilter,array());
      foreach($maFilter['items'] as $msFilterItem){
        $moFilterItemText->setValue($msFilterItem);
        $this->coWriter->drawLine($moHeaderFilterItem,array());
      }
    }
    $this->coWriter->drawLine($moHeaderBlank,array());
  }
}
?>