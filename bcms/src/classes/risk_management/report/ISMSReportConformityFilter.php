<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportConformityFilter.
 *
 * <p>Classe que implementa o filtro do relatório de conformidades.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportConformityFilter extends ISMSReportGenericClassifTypePrioFilter {

  protected $ciStandardId = 0;
  
  public function setStandard($piStandardId) {
    $this->ciStandardId = $piStandardId;
  }
  
  public function getStandard() {
    return $this->ciStandardId;
  }
  
  public function getSummary(){
    $maFilters = parent::getSummary();
    
    $msStandard = "";
    if ($this->ciStandardId) {
      $moStandard = new RMStandard();
      $moStandard->fetchById($this->ciStandardId);
      $msStandard = $moStandard->getFieldValue('standard_name'); 
    }
    else {
      $msStandard = FWDLanguage::getPHPStringValue('rs_all', 'Todas');
    }
    
    $maFilters[] = array(
      'name' => FWDLanguage::getPHPStringValue('rs_standard_filter_cl', 'Filtro de Norma:'),
      'items' => array($msStandard)
    );
    
    return $maFilters;
  }
}
?>