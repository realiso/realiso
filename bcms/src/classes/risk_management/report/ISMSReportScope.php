<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

class ISMSReportScope extends ISMSReport {

	public function init() {
		parent::init();
		
		
		$col1 = new FWDDBField("scopeName"				,"scopeName"				, DB_STRING);
		$col1->setLabel(FWDLanguage::getPHPStringValue('rs_scope','Escopo'));
		$col1->setShowColumn(true);
		
		$col2 = new FWDDBField("scopeDescription"		,"scopeDescription"			, DB_STRING);
		$col2->setLabel(' ');
		$col2->setShowColumn(false);
		
		$col3 = new FWDDBField("placeName"				,"placeName"				, DB_STRING);
		$col3->setLabel(FWDLanguage::getPHPStringValue('rs_local','Local'));
		$col3->setShowColumn(true);
		
		$col4 = new FWDDBField("cmtResourceName"		,"cmtResourceName"			, DB_STRING);
		$col4->setLabel(FWDLanguage::getPHPStringValue('rs_cmt','CMT'));
		$col4->setShowColumn(true);
		
		$col5 = new FWDDBField("areaName"				,"areaName"					, DB_STRING);
		$col5->setLabel(FWDLanguage::getPHPStringValue('ti_un','Unidade de Neg�cio'));
		$col5->setShowColumn(true);
		
		$col6 = new FWDDBField("areaResponsible"		,"areaResponsible"			, DB_STRING);
		$col6->setLabel(FWDLanguage::getPHPStringValue('rs_responsible','Respons�vel'));
		$col6->setShowColumn(true);
		
		$col7 = new FWDDBField("areaSubstitute"			,"areaSubstitute"			, DB_STRING);
		$col7->setLabel(FWDLanguage::getPHPStringValue('rs_substitute','Substituto'));
		$col7->setShowColumn(true);
		
		$col8 = new FWDDBField("placeId"				,"placeId"					, DB_NUMBER);
		$col8->setLabel(' ');
		$col8->setShowColumn(false);
		
		$col9 = new FWDDBField("placeParentId"			,"placeParentId"			, DB_NUMBER);
		$col9->setLabel(' ');
		$col9->setShowColumn(false);
		
		
		$col10 = new FWDDBField("escalation"				,"escalation"				, DB_STRING);
		$col10->setLabel(' ');
		$col10->setShowColumn(false);
		
		$col11 = new FWDDBField("processName"			,"processName"				, DB_STRING);
		$col11->setLabel(FWDLanguage::getPHPStringValue('rs_process','Processo'));
		$col11->setShowColumn(true);
		
		$col12 = new FWDDBField("assetName"				,"assetName"				, DB_STRING);
		$col12->setLabel(FWDLanguage::getPHPStringValue('rs_asset','Asset'));
		$col12->setShowColumn(true);
		
		$col13 = new FWDDBField("assetResponsible"		,"assetResponsible"			, DB_STRING);
		$col13->setLabel(FWDLanguage::getPHPStringValue('rs_responsible_group','Grupo Respons�vel'));
		$col13->setShowColumn(true);
		
		$this->coDataSet->addFWDDBField($col1);
		$this->coDataSet->addFWDDBField($col2);
		$this->coDataSet->addFWDDBField($col3);
		$this->coDataSet->addFWDDBField($col4);
		$this->coDataSet->addFWDDBField($col5);
		$this->coDataSet->addFWDDBField($col6);
		$this->coDataSet->addFWDDBField($col7);
		$this->coDataSet->addFWDDBField($col8);
		$this->coDataSet->addFWDDBField($col9);
		$this->coDataSet->addFWDDBField($col10);
		$this->coDataSet->addFWDDBField($col11);
		$this->coDataSet->addFWDDBField($col12);
		$this->coDataSet->addFWDDBField($col13);
		
	}

	public function makeQuery() {
		$moFilter = $this->getFilter();
		$filters = array();
		$where = '';

		if(count($filters)){
			$where = ' AND ( ' . implode(' AND ', $filters) ." ) ";
		}
		$subLocal = FWDLanguage::getPHPStringValue('subLocalOf',"Sub de");
		$this->csQuery = "	SELECT DISTINCT
								scope.sName AS scopeName,
								scope.sdescription AS scopeDescription,
								place.sname ||coalesce(('( {$subLocal} '||parent.sname||')'),'') AS placeName,
								place.fkcontext AS placeId,
								place.fkparent AS placeParentId,
								area.sname AS areaName,
								area.responsible AS areaResponsible,
								area.substitute AS areaSubstitute,
								process.sname AS processName,
								asset.sname AS assetName,
								asset.responsible AS assetResponsible
							FROM 
								view_cm_scope_active scope 
								LEFT JOIN view_cm_scope_place_active scopePlace ON scopePlace.fkscope = scope.fkcontext
								LEFT JOIN view_cm_scope_process_active scopeProcess ON scopeProcess.fkscope = scope.fkcontext
								LEFT JOIN view_cm_scope_asset_active scopeAsset ON scopeAsset.fkscope = scope.fkcontext
							
								LEFT JOIN view_cm_place_active place ON place.fkcontext = scopePlace.fkplace
							
								LEFT JOIN (	SELECT 
												area.sname,
												coalesce(g.name, r.name) as responsible, 
												coalesce(gs.name, rs.name) as substitute,
												area.fkplace, 
												area.fkcontext,
												scopeArea.fkscope 
											FROM 
												view_rm_area_active area
												LEFT JOIN view_cm_group_active g ON g.fkcontext = area.fkgroup
												LEFT JOIN view_cm_resource_active r ON r.fkcontext = area.fkresource
												LEFT JOIN view_cm_group_active gs ON gs.fkcontext = area.fkGroupSubstitute
												LEFT JOIN view_cm_resource_active rs ON rs.fkcontext = area.fkResourceSubstitute, 
												view_cm_place_active place, 
												view_cm_scope_area_active scopeArea
												
											WHERE area.fkcontext = scopeArea.fkarea and place.fkcontext = area.fkplace
									)area ON area.fkscope = scope.fkcontext and area.fkplace = place.fkcontext
										
							
								LEFT JOIN (	SELECT
												process.sname,
												process.fkcontext,
												process.fkarea,
												scopeProcess.fkscope
											FROM
												view_rm_process_active process, 
												view_cm_scope_process_active scopeProcess
											WHERE
												process.fkarea is not null and process.fkcontext = scopeprocess.fkprocess
									) process ON process.fkscope = scope.fkcontext and process.fkarea = area.fkcontext
								
							
								LEFT JOIN (	SELECT 
												processAsset.fkProcess, 
												asset.fkContext, 
												asset.sname, 
												g.name as responsible
											FROM 
												view_rm_process_asset_active processAsset, 
												view_rm_asset_active asset
												LEFT JOIN view_cm_group_active g ON g.fkcontext = asset.fkgroup
												
											WHERE 
												processAsset.fkasset = asset.fkcontext
									) as asset ON asset.fkProcess = process.fkContext and asset.fkContext = scopeAsset.fkasset
								LEFT JOIN view_cm_place_active parent ON place.fkparent = parent.fkcontext
							ORDER BY
								scopeName,
								placeParentId desc,
								placeId,
								placeName,
								areaName,
								processName,
								assetName";
		return parent::executeQuery();
	}

	public function drawHeader(){
		foreach($this->caHeaders as $moHeader){
			if($moHeader->getAttrName()=='header_filter'){
				$moHeaderFilter = $moHeader;
			}elseif($moHeader->getAttrName()=='header_filter_item'){
				$moHeaderFilterItem = $moHeader;
			}elseif($moHeader->getAttrName()=='report_header_blank'){
				$moHeaderBlank = $moHeader;
			}else{
				$this->coWriter->drawLine($moHeader,array());
			}
		}
		if ($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());

		$moFilterText = FWDWebLib::getObject('header_filter_text');
		$moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
		$filters = $this->coFilter->getSummary();
		foreach($filters as $maFilter){
			$moFilterText->setValue($maFilter['name']);
			$this->coWriter->drawLine($moHeaderFilter,array());
			foreach($maFilter['items'] as $msFilterItem){
				$moFilterItemText->setValue($msFilterItem);
				$this->coWriter->drawLine($moHeaderFilterItem,array());
			}
		}
		$this->coWriter->drawLine($moHeaderBlank,array());
	}
}
?>