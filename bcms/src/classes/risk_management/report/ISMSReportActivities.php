<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

class ISMSReportActivities extends ISMSReport {

	public function init() {
		parent::init();
		$this->coDataSet->addFWDDBField(new FWDDBField("processId"						,"processId"				,DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("actId"							,"activityId"				,DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("processName"					,"processName"				,DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField("activityName"					,"activityName"				,DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField("activityDescription"			,"activityDescription"		,DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField("responsible"					,"responsible"				,DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField("importance"						,"importance"				,DB_STRING));
	}

	public function makeQuery() {
		$moFilter = $this->getFilter();
		$filters = array();

		$join = '';
		$where = '';

		if(count($filters)){
			$where = ' AND ( ' . implode(' AND ', $filters) ." ) ";
		}

		$this->csQuery = "	select
								process.sname as processName,
								process.fkcontext as processId,
								act.fkcontext as activityId,
								act.sname as activityName,
								act.sdescription as activityDescription,
								g.name as responsible,
								p.sname as importance
							from 	
								view_cm_process_activity_active act
								join view_rm_process_active process on process.fkcontext = act.fkprocess
								left join view_cm_group_active g on g.fkcontext = act.fkgroup
								left join cm_priority p on p.fkcontext = act.fkimportance
							order by 
								processName,
								processId,
								activityName,
								activityId";
		return parent::executeQuery();
	}

	public function drawHeader(){
		foreach($this->caHeaders as $moHeader){
			if($moHeader->getAttrName()=='header_filter'){
				$moHeaderFilter = $moHeader;
			}elseif($moHeader->getAttrName()=='header_filter_item'){
				$moHeaderFilterItem = $moHeader;
			}elseif($moHeader->getAttrName()=='report_header_blank'){
				$moHeaderBlank = $moHeader;
			}else{
				$this->coWriter->drawLine($moHeader,array());
			}
		}
		if ($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());

		$moFilterText = FWDWebLib::getObject('header_filter_text');
		$moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
		$filters = $this->coFilter->getSummary();
		foreach($filters as $maFilter){
			$moFilterText->setValue($maFilter['name']);
			$this->coWriter->drawLine($moHeaderFilter,array());
			foreach($maFilter['items'] as $msFilterItem){
				$moFilterItemText->setValue($msFilterItem);
				$this->coWriter->drawLine($moHeaderFilterItem,array());
			}
		}
		$this->coWriter->drawLine($moHeaderBlank,array());
	}
}
?>