<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportBestPracticesFilter.
 *
 * <p>Classe que implementa o filtro do relatório de melhores práticas.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportBestPracticesFilter extends FWDReportFilter {

  protected $ciStandard = 0;

  protected $cbOnlyApplied = false;

  public function setStandard($piStandard){
    $this->ciStandard = $piStandard;
  }

  public function getStandard(){
    return $this->ciStandard;
  }

  public function setOnlyApplied($pbOnlyApplied){
    $this->cbOnlyApplied = $pbOnlyApplied;
  }

  public function getOnlyApplied(){
    return $this->cbOnlyApplied;
  }

  public function getSummary(){
    
    $maFilters = array();
    
    $moStandard = new RMStandard();
    $moStandard->fetchById($this->ciStandard);
    
    if($this->ciStandard){
      $maFilters[] = array(
        'name' => FWDLanguage::getPHPStringValue('rs_standard_cl','Norma:'),
        'items' => array($moStandard->getName())
      );
    }
    
    if($this->cbOnlyApplied){
      $maFilters[] = array(
        'name' => FWDLanguage::getPHPStringValue('rs_only_applied','Somente Aplicadas'),
        'items' => array()
      );
    }
    
    return $maFilters;
  }

}

?>