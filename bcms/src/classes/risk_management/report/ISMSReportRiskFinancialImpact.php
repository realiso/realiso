<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportRiskFinancialImpact.
 *
 * <p>Classe que implementa o relatório de impacto financeiro dos riscos.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportRiskFinancialImpact extends ISMSFilteredReport {
  
  public function __construct(){
    parent::__construct();
  }
  
 /**
  * Inicializa o relatório.
  *
  * <p>Método para inicializar o relatório.
  * Necessário para funcionar com o XML compilado.</p>
  * @access public
  */
  public function init(){
    parent::init();
    $this->coDataSet->addFWDDBField(new FWDDBField('r.fkContext'     ,'risk_id'            ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('r.sName'         ,'risk_name'          ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('r.nAcceptMode'   ,'risk_is_accepted'   ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('r.nCost'         ,'risk_impact'        ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('r.nValueResidual','risk_residual_value',DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('r.nValue'        ,'risk_value'         ,DB_NUMBER));
  }
  
 /**
  * Executa a query do relatório.
  *
  * <p>Método para executar a query do relatório.</p>
  * @access public
  */
  public function makeQuery(){
    $miTreatment = $this->coFilter->getTreatment();
    $miValues = $this->coFilter->getValues();
    $mbNotTreatedOrAccepted = $this->coFilter->getNotTreatedOrAccepted();
    
    $maFilters = array();
    
    if($miTreatment){
      if($miTreatment==ISMSReportRiskFinancialImpactFilter::REDUCED){
        $maFilters[] = "r.fkContext IN (SELECT fkRisk FROM rm_risk_control)";
      }elseif($miTreatment==ISMSReportRiskFinancialImpactFilter::TREATED){
        $maFilters[] = "(r.nAcceptMode != 0 OR r.fkContext IN (SELECT fkRisk FROM rm_risk_control))";
      }elseif($miTreatment==ISMSReportRiskFinancialImpactFilter::NOT_TREATED){
        $maFilters[] = "(r.nAcceptMode = 0 AND r.fkContext NOT IN (SELECT fkRisk FROM rm_risk_control))";
      }else{
        $maFilters[] = "r.nAcceptMode = {$miTreatment}";
      }
    }
    
    if($miValues){
      $miLowRisk = ISMSLib::getConfigById(RISK_LOW);
      $miHighRisk = ISMSLib::getConfigById(RISK_HIGH);
      $maValueFilters = array();
      
      if($miValues & ISMSReportRiskFinancialImpactFilter::LOW){
        $maValueFilters[] = "(r.nValue != 0 AND r.nValue <= $miLowRisk)";
      }
      if($miValues & ISMSReportRiskFinancialImpactFilter::MIDDLE){
        $maValueFilters[] = "(r.nValue > $miLowRisk AND r.nValue < $miHighRisk)";
      }
      if($miValues & ISMSReportRiskFinancialImpactFilter::HIGH){
        $maValueFilters[] = "r.nValue >= $miHighRisk";
      }
      $maFilters[] = '('.implode(' OR ',$maValueFilters).')';
    }
    
    if($mbNotTreatedOrAccepted){
      $maFilters[] = "(
                        (
                          r.nValue != 0 
                          AND r.nAcceptMode = 0
                          AND r.fkContext NOT IN (SELECT fkRisk FROM rm_risk_control)
                        ) OR r.nAcceptMode = ".ISMSReportRiskFinancialImpactFilter::ACCEPTED."
                      )";
    }
    
    if(count($maFilters)==0){
      $msWhere = '';
    }else{
      $msWhere = ' WHERE '.implode(' AND ',$maFilters);
    }
    $this->csQuery = "SELECT
                        r.fkContext AS risk_id,
                        r.sName AS risk_name,
                        r.nAcceptMode AS risk_is_accepted,
                        r.nCost AS risk_impact,
                        r.nValue AS risk_value,
                        r.nValueResidual AS risk_residual_value
                      FROM view_rm_risk_active r
                      {$msWhere}
                      ORDER BY risk_impact DESC, risk_name";
    return parent::executeQuery();
  }

}

?>