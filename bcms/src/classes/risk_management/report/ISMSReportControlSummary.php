<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe ISMSReportControlSummary.
 *
 * <p>Classe que implementa o relat�rio de resumo de controles.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportControlSummary extends ISMSReport {
	
	/**
	 * Inicializa o relat�rio.
	 *
	 * <p>M�todo para inicializar o relat�rio.
	 * Necess�rio para funcionar com o XML compilado.</p>
	 * @access public
	 */	
	public function init() {
		parent::init();
		
		$this->coDataSet->addFWDDBField(new FWDDBField("","control_id",DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("","control_name",DB_STRING));		
		$this->coDataSet->addFWDDBField(new FWDDBField("","control_is_active",DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("","control_date_implemented",DB_DATETIME));
		$this->coDataSet->addFWDDBField(new FWDDBField("","control_description",DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField("","best_practice_id",DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("","best_practice_name",DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField("","best_practice_document",DB_STRING));
	}
	
	/**
	 * Executa a query do relat�rio.
	 *
	 * <p>M�todo para executar a query do relat�rio.</p>
	 * @access public
	 */
	public function makeQuery() {
    $moFilter = $this->getFilter();
    $maFilters = array();
    $msWhere ="";

    $maClassifType = $moFilter->getControlClassifType();
    
    if(count($maClassifType)){
      if(isset($maClassifType['null'])){
        $maFilters[] = " c.fkType IS NULL ";
      }else{
        $maFilters[] = " c.fkType IN ( ".implode(',',array_keys($maClassifType)) ." ) ";
      }
    }

    if(count($maFilters)){
      $msWhere .= " AND ". implode(' AND ',$maFilters);
    }

  
		$this->csQuery = "
SELECT c.fkContext as control_id, 
       c.sName as control_name, 
       c.bIsActive as control_is_active, 
       c.dDateImplemented as control_date_implemented, 
       c.tDescription as control_description,
       bp.fkContext as best_practice_id, 
       bp.sName as best_practice_name, 
       bp.sDocument as best_practice_document
  FROM view_rm_control_active c
       LEFT JOIN rm_control_best_practice cbp ON (c.fkContext = cbp.fkControl)
       LEFT JOIN view_rm_best_practice_active bp ON (cbp.fkBestPractice = bp.fkContext)
  WHERE cbp.fkBestPractice IN (SELECT bp.fkContext FROM view_rm_best_practice_active bp)
  $msWhere
ORDER BY c.bIsActive, c.fkContext";

    return parent::executeQuery();
	}
  
  /**
   * Desenha o cabe�alho do relat�rio.
   *
   * <p>M�todo para desenhar o cabe�alho do relat�rio.</p>
   * @access public
   */
  public function drawHeader(){
    foreach($this->caHeaders as $moHeader){
      if($moHeader->getAttrName()=='header_filter'){
        $moHeaderFilter = $moHeader;
      }elseif($moHeader->getAttrName()=='header_filter_item'){
        $moHeaderFilterItem = $moHeader;
      }elseif($moHeader->getAttrName()=='report_header_blank'){
        $moHeaderBlank = $moHeader;
      }else{
        $this->coWriter->drawLine($moHeader,array());
      }
    }
    if ($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());
    
    $moFilterText = FWDWebLib::getObject('header_filter_text');
    $moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
    $maFilters = $this->coFilter->getSummary();
    foreach($maFilters as $maFilter){
      $moFilterText->setValue($maFilter['name']);
      $this->coWriter->drawLine($moHeaderFilter,array());
      foreach($maFilter['items'] as $msFilterItem){
        $moFilterItemText->setValue($msFilterItem);
        $this->coWriter->drawLine($moHeaderFilterItem,array());
      }
    }
    $this->coWriter->drawLine($moHeaderBlank,array());
  }
  
}
?>