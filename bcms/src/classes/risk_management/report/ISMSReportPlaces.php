<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

class ISMSReportPlaces extends ISMSReport {

	public function init() {
		parent::init();
		
		$col1 = new FWDDBField("name","name", DB_STRING);
		$col1->setLabel(FWDLanguage::getPHPStringValue('rs_name','Nome'));
		$col1->setShowColumn(true);
		
		$col2 = new FWDDBField("type","type", DB_STRING);
		$col2->setLabel(FWDLanguage::getPHPStringValue('rs_type','Tipo'));
		$col2->setShowColumn(true);
		
		
		$this->coDataSet->addFWDDBField($col1);
		$this->coDataSet->addFWDDBField($col2);
	}

	public function makeQuery() {
		$moFilter = $this->getFilter();
		$filters = array();

		$maClassifFilter = array();
		$join = '';
		$placeType = $moFilter->getPlaceType();
		if(count($placeType)){
			if(isset($placeType['null'])){
				$maClassifFilter[] = " place.fkType IS NULL ";
			}else{
				$maClassifFilter[] = " place.fkType IN ( ".implode(',',$placeType) ." ) ";
			}
		}
		//		$processType = $moFilter->getProcessType();
		//		if(count($processType)){
		//			if(isset($processType['null'])){
		//				$maClassifFilter[] = " p.fkType IS NULL ";
		//			}else{
		//				$maClassifFilter[] = " p.fkType IN ( ".implode(',',$processType) ." ) ";
		//			}
		//		}
		//		$processPriority = $moFilter->getProcessPriority();
		//		if(count($processPriority)){
		//			if(isset($processPriority['null'])){
		//				$maClassifFilter[] = " p.fkPriority IS NULL ";
		//			}else{
		//				$maClassifFilter[] = " p.fkPriority IN ( ".implode(',',$processPriority) ." ) ";
		//			}
		//		}
		$where = '';
		if(count($maClassifFilter)){
			$where .= " AND (". implode(' OR ',$maClassifFilter).") ";
		}

		if(count($filters)){
			$where .= ' AND ( ' . implode(' AND ', $filters) ." ) ";
		}

		$this->csQuery = "	select
								place.sname as name,
								place.type as type
							from 
								view_cm_place_active place
								
								{$join}
							where 1=1
							{$where}
							order by name, type";
							return parent::executeQuery();
							//cat.sname as type
							//join view_cm_place_category_active cat on cat.fkcontext = place.fktype
	}

	public function drawHeader(){
		foreach($this->caHeaders as $moHeader){
			if($moHeader->getAttrName()=='header_filter'){
				$moHeaderFilter = $moHeader;
			}elseif($moHeader->getAttrName()=='header_filter_item'){
				$moHeaderFilterItem = $moHeader;
			}elseif($moHeader->getAttrName()=='report_header_blank'){
				$moHeaderBlank = $moHeader;
			}else{
				$this->coWriter->drawLine($moHeader,array());
			}
		}
		if ($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());

		$moFilterText = FWDWebLib::getObject('header_filter_text');
		$moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
		$filters = $this->coFilter->getSummary();
		foreach($filters as $maFilter){
			$moFilterText->setValue($maFilter['name']);
			$this->coWriter->drawLine($moHeaderFilter,array());
			foreach($maFilter['items'] as $msFilterItem){
				$moFilterItemText->setValue($msFilterItem);
				$this->coWriter->drawLine($moHeaderFilterItem,array());
			}
		}
		$this->coWriter->drawLine($moHeaderBlank,array());
	}
}
?>