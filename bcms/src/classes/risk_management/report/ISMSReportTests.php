<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

class ISMSReportTests extends ISMSReport {

	public function init() {
		parent::init();
		
		$col1 = new FWDDBField("planId"					,"planId"						,DB_NUMBER);
		$col1->setLabel(' ');
		$col1->setShowColumn(false);
		
		$col2 = new FWDDBField("testId"					,"testId"						,DB_NUMBER);
		$col2->setLabel(' ');
		$col2->setShowColumn(false);
		
		$col3 = new FWDDBField("actionId"				,"actionId"						,DB_NUMBER);
		$col3->setLabel(' ');
		$col3->setShowColumn(false);
		
		$col4 = new FWDDBField("planName"				,"planName"						,DB_STRING);
		$col4->setLabel(FWDLanguage::getPHPStringValue('rs_plan','Plano'));
		$col4->setShowColumn(true);
		
		$col5 = new FWDDBField("planDescription"		,"planDescription"				,DB_STRING);
		$col5->setLabel(' ');
		$col5->setShowColumn(false);
		
		$col6 = new FWDDBField("actionResume"			,"actionResume"					,DB_STRING);
		$col6->setLabel(FWDLanguage::getPHPStringValue('rs_description','Descri��o'));
		$col6->setShowColumn(true);
		
		$col7 = new FWDDBField("actionEstimate"			,"actionEstimate"				,DB_STRING);
		$col7->setLabel(FWDLanguage::getPHPStringValue('rs_estimated','Estimado'));
		$col7->setShowColumn(true);
		
		$col8 = new FWDDBField("actionEstimateFlag"		,"actionEstimateFlag"			,DB_STRING);
		$col8->setLabel(' ');
		$col8->setShowColumn(false);
		
		$col9 = new FWDDBField("actionGroup"			,"actionGroup"					,DB_STRING);
		$col9->setLabel(FWDLanguage::getPHPStringValue('rs_responsible_group','Grupo Respons�vel'));
		$col9->setShowColumn(true);
		
		$col10 = new FWDDBField("actionStart"			,"actionStart"					,DB_DATETIME);
		$col10->setLabel(FWDLanguage::getPHPStringValue('rs_start','In�cio'));
		$col10->setShowColumn(true);
		
		$col11 = new FWDDBField("actionEnd"				,"actionEnd"					,DB_DATETIME);
		$col11->setLabel(FWDLanguage::getPHPStringValue('rs_end','Fim'));
		$col11->setShowColumn(true);
		
		$col12 = new FWDDBField("actionObservation"		,"actionObservation"			,DB_STRING);
		$col12->setLabel(FWDLanguage::getPHPStringValue('rs_observation','Observa��o'));
		$col12->setShowColumn(true);
		
		$col13 = new FWDDBField("testStart"				,"testStart"					,DB_DATETIME);
		$col13->setLabel(FWDLanguage::getPHPStringValue('rs_start','In�cio'));
		$col13->setShowColumn(true);
		
		$col14 = new FWDDBField("testEnd"				,"testEnd"						,DB_DATETIME);
		$col14->setLabel(FWDLanguage::getPHPStringValue('rs_end','Fim'));
		$col14->setShowColumn(true);
		
		$col15 = new FWDDBField("testObservation"		,"testObservation"				,DB_STRING);
		$col15->setLabel(FWDLanguage::getPHPStringValue('rs_observation','Observa��o'));
		$col15->setShowColumn(true);
		
		$this->coDataSet->addFWDDBField($col1);
		$this->coDataSet->addFWDDBField($col2);
		$this->coDataSet->addFWDDBField($col3);
		$this->coDataSet->addFWDDBField($col4);
		$this->coDataSet->addFWDDBField($col5);
		$this->coDataSet->addFWDDBField($col6);
		$this->coDataSet->addFWDDBField($col7);
		$this->coDataSet->addFWDDBField($col8);
		$this->coDataSet->addFWDDBField($col9);
		$this->coDataSet->addFWDDBField($col10);
		$this->coDataSet->addFWDDBField($col11);
		$this->coDataSet->addFWDDBField($col12);
		$this->coDataSet->addFWDDBField($col13);
		$this->coDataSet->addFWDDBField($col14);
		$this->coDataSet->addFWDDBField($col15);
	}

	public function makeQuery() {
		$moFilter = $this->getFilter();
		$filters = array();

		$join = '';
		$where = '';

		if(count($filters)){
			$where = ' AND ( ' . implode(' AND ', $filters) ." ) ";
		}

		$this->csQuery = "	select 
								plan.fkcontext as planId,
								test.fkcontext as testId,
								action.fkcontext as actionId,
								plan.name as planName,
								plan.description as planDescription,
								action.resume as actionResume,
								action.estimate as actionEstimate,
								action.estimateFlag as actionEstimateFlag,
								g.name as actionGroup,
								testAction.starttime as actionStart,
								testAction.endtime as actionEnd,
								testAction.observation as actionObservation,
								test.starttime as testStart,
								test.endtime as testEnd,
								test.observation as testObservation
							from
								view_cm_plan_active plan
								join view_cm_plan_action_active action on plan.fkcontext = action.fkplan
								left join view_cm_group_active g on g.fkcontext = action.fkgroup
								join view_cm_plan_test_active test on test.fkplan = plan.fkcontext
								left join view_cm_plan_action_test_active testAction on testAction.fkplantest = test.fkcontext and testAction.fkplanAction = action.fkcontext
							order by 
								testStart desc,
								testId,
								planId,
								actionStart";
						return parent::executeQuery();
	}

	public function drawHeader(){
		foreach($this->caHeaders as $moHeader){
			if($moHeader->getAttrName()=='header_filter'){
				$moHeaderFilter = $moHeader;
			}elseif($moHeader->getAttrName()=='header_filter_item'){
				$moHeaderFilterItem = $moHeader;
			}elseif($moHeader->getAttrName()=='report_header_blank'){
				$moHeaderBlank = $moHeader;
			}else{
				$this->coWriter->drawLine($moHeader,array());
			}
		}
		if ($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());

		$moFilterText = FWDWebLib::getObject('header_filter_text');
		$moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
		$filters = $this->coFilter->getSummary();
		foreach($filters as $maFilter){
			$moFilterText->setValue($maFilter['name']);
			$this->coWriter->drawLine($moHeaderFilter,array());
			foreach($maFilter['items'] as $msFilterItem){
				$moFilterItemText->setValue($msFilterItem);
				$this->coWriter->drawLine($moHeaderFilterItem,array());
			}
		}
		$this->coWriter->drawLine($moHeaderBlank,array());
	}
}
?>