<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

class ISMSReportBiaQualitative extends ISMSReport {

	public function init() {
		parent::init();
		
		$col1 = new FWDDBField("place_name","place_name", DB_STRING);
		$col1->setLabel(FWDLanguage::getPHPStringValue('rs_local', 'Local'));
		$col1->setShowColumn(true);
	
		$col2 =	new FWDDBField("name","name", DB_STRING);
		$col2->setLabel(FWDLanguage::getPHPStringValue('rs_process', 'Processo'));
		$col2->setShowColumn(true);

		$col3 =	new FWDDBField("area","area", DB_STRING);
		$col3->setLabel(FWDLanguage::getPHPStringValue('ti_un', 'UN'));
		$col3->setShowColumn(true);		

		$col4 = new FWDDBField("rto","rto", DB_NUMBER);
		$col4->setLabel(FWDLanguage::getPHPStringValue('rs_rto', 'RTO'));
		$col4->setShowColumn(true);
		
		$col5 =	new FWDDBField("ranking","ranking", DB_NUMBER);
		$col5->setLabel(FWDLanguage::getPHPStringValue('rs_rank', 'Ranking'));
		$col5->setShowColumn(true);



		$this->coDataSet->addFWDDBField($col1);
		$this->coDataSet->addFWDDBField($col2);
		$this->coDataSet->addFWDDBField($col3);
		$this->coDataSet->addFWDDBField($col4);
		$this->coDataSet->addFWDDBField($col5);
	}

	public function makeQuery() {
		$moFilter = $this->getFilter();
		$filters = array();

		$maClassifFilter = array();
		$placeType = $moFilter->getPlaceType();
		
		if(count($placeType)){
			if(isset($placeType['null'])){
				$maClassifFilter[] = " pl.fkType IS NULL ";
			}else{
				$maClassifFilter[] = " pl.fkType IN ( ".implode(',',$placeType) ." ) ";
			}
		}

		$processType = $moFilter->getProcessType();
		if(count($processType)){
			if(isset($processType['null'])){
				$maClassifFilter[] = " p.fkType IS NULL ";
			}else{
				$maClassifFilter[] = " p.fkType IN ( ".implode(',',$processType) ." ) ";
			}
		}

		$processPriority = $moFilter->getProcessPriority();
		if(count($processPriority)){
			if(isset($processPriority['null'])){
				$maClassifFilter[] = " p.fkPriority IS NULL ";
			}else{
				$maClassifFilter[] = " p.fkPriority IN ( ".implode(',',$processPriority) ." ) ";
			}
		}

		$place = $moFilter->getPlace();
		if($place){
			$filters[] = " pp.fkplace = {$place} ";
		}

		$where = '';
		if(count($maClassifFilter)){
			$where .= " AND (". implode(' OR ',$maClassifFilter).") ";
		}

		if(count($filters)){
			$where = ' AND ( ' . implode(' AND ', $filters) ." ) ";
		}

		$this->csQuery = "	SELECT
								pl.sname AS place_name,
								p.sName AS name,
								ar.sname AS area,
								coalesce(rnk.rto,0) AS rto,
								coalesce(rnk.ranking,0) AS ranking
							FROM 
								view_rm_process_active p 

							JOIN cm_place_process pp ON pp.fkprocess = p.fkcontext
							JOIN view_cm_place_active pl ON pl.fkcontext = pp.fkplace

							JOIN rm_area ar on ar.fkcontext = p.fkarea
							LEFT JOIN view_process_ranking rnk ON p.fkcontext = rnk.fkprocess
							
							WHERE 1=1 
							{$where}
							GROUP BY
								pl.sName,
								p.sName,
								rnk.ranking,
								ar.sname,
								rnk.rto
							ORDER BY
								ranking DESC";

							return parent::executeQuery();
	}

	public function drawHeader(){
		foreach($this->caHeaders as $moHeader){
			if($moHeader->getAttrName()=='header_filter'){
				$moHeaderFilter = $moHeader;
			}elseif($moHeader->getAttrName()=='header_filter_item'){
				$moHeaderFilterItem = $moHeader;
			}elseif($moHeader->getAttrName()=='report_header_blank'){
				$moHeaderBlank = $moHeader;
			}else{
				$this->coWriter->drawLine($moHeader,array());
			}
		}
		
		if ($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());

		$moFilterText = FWDWebLib::getObject('header_filter_text');
		$moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
		$filters = $this->coFilter->getSummary();
		foreach($filters as $maFilter){
			$moFilterText->setValue($maFilter['name']);
			$this->coWriter->drawLine($moHeaderFilter,array());
			foreach($maFilter['items'] as $msFilterItem){
				$moFilterItemText->setValue($msFilterItem);
				$this->coWriter->drawLine($moHeaderFilterItem,array());
			}
		}
		$this->coWriter->drawLine($moHeaderBlank,array());
	}
}
?>