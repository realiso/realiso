<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

class ISMSReportAssetByAsset extends ISMSReport {

	public function init() {
		
		parent::init();
		
		$col1 = new FWDDBField("","asset_id", DB_NUMBER);
    	$col1->setLabel(' ');
    	$col1->setShowColumn(false);
    
    	$col2 = new FWDDBField("","asset_name", DB_STRING);
    	$col2->setLabel(FWDLanguage::getPHPStringValue('rs_asset_cl','Ativo'));
    	$col2->setShowColumn(true);
    
    	$col3 = new FWDDBField("","asset_ranking", DB_NUMBER);
    	$col3->setLabel(FWDLanguage::getPHPStringValue('rs_rank','Ranking'));
    	$col3->setShowColumn(true);
    
    	$col4 = new FWDDBField("","asset_dep_id", DB_NUMBER);
    	$col4->setLabel(' ');
    	$col4->setShowColumn(false);
    
    	$col5 = new FWDDBField("","asset_dep_name", DB_STRING);
    	$col5->setLabel(FWDLanguage::getPHPStringValue('rs_asset_dep_cl_bl','DependÍncias'));
    	$col5->setShowColumn(true);
    
    	$col6 = new FWDDBField("","asset_dep_ranking", DB_NUMBER);
    	$col6->setLabel(FWDLanguage::getPHPStringValue('rs_rank','Ranking'));
    	$col6->setShowColumn(true);
    	
    	$this->coDataSet->addFWDDBField($col1);
    	$this->coDataSet->addFWDDBField($col2);
    	$this->coDataSet->addFWDDBField($col3);
    	$this->coDataSet->addFWDDBField($col4);
    	$this->coDataSet->addFWDDBField($col5);
    	$this->coDataSet->addFWDDBField($col6);
    }

	public function makeQuery() {
		
		
		$moFilter = $this->getFilter();
		$filters = array();
		$joins = array();
		$maClassifFilter = array();
		
		$placeType = $moFilter->getPlaceType();
		if(count($placeType)){
			if(isset($placeType['null'])){
				$maClassifFilter[] = " place.fkType IS NULL ";
			}else{
				$maClassifFilter[] = " place.fkType IN ( ".implode(',',$placeType) ." ) ";
			}
		}
		
		$processType = $moFilter->getProcessType();
		if(count($processType)){
			if(isset($processType['null'])){
				$maClassifFilter[] = " p.fkType IS NULL ";
			}else{
				$maClassifFilter[] = " p.fkType IN ( ".implode(',',$processType) ." ) ";
			}
		}
		
		$processPriority = $moFilter->getProcessPriority();
		if(count($processPriority)){
			if(isset($processPriority['null'])){
				$maClassifFilter[] = " p.fkPriority IS NULL ";
			}else{
				$maClassifFilter[] = " p.fkPriority IN ( ".implode(',',$processPriority) ." ) ";
			}
		}
		
		$place = $moFilter->getPlace();
		if($place){
			$filters[] = " place.fkcontext = {$place} ";
			$joins[] = " JOIN view_cm_place_active place on (place.fkcontext = a2.fkplace) ";
				
				
		}
		
		$area = $moFilter->getArea();
		if($area){
			$filters[] = " area.fkcontext = {$area} ";
			$joins[] = " join view_rm_area_active area on (area.fkcontext = p.fkarea) ";
		}
		
		$process = $moFilter->getProcess();
		if($process){
			$filters[] = " p.fkcontext = {$process} ";
		}
		
		$where = "";
		$join = "";
		if(count($filters)){
			$where = ' AND ( ' . implode(' AND ', $filters) ." ) ";
		}
		
		if(count($maClassifFilter)){
			$where .= " AND (". implode(' OR ',$maClassifFilter).") ";
		}
		
		if(count($joins)){
			$join = implode(' ', $joins);
		}
		
		$this->csQuery = "
							SELECT DISTINCT
								a2.fkContext as asset_id,
								a2.sName as asset_name,
								coalesce(ranking2.ranking, 0.00) as asset_ranking,
								aa.fkAsset as asset_dep_id,
								a.sName as asset_dep_name,
								coalesce(ranking.ranking, 0.00) as asset_dep_ranking
							FROM 
								view_rm_asset_active a
								JOIN view_rm_asset_asset_active aa ON (a.fkContext = aa.fkAsset)
								LEFT JOIN view_rm_process_asset_active paa on (a.fkcontext = paa.fkasset)
								LEFT JOIN view_process_ranking ranking on (ranking.fkprocess = paa.fkprocess)
								JOIN view_rm_asset_active a2 ON (a2.fkContext = aa.fkDependent)
								LEFT JOIN view_rm_process_asset_active paa2 on (a2.fkcontext = paa2.fkasset)
								LEFT JOIN view_process_ranking ranking2 on (ranking2.fkprocess = paa2.fkprocess)
								join view_rm_process_active p ON (p.fkContext = paa2.fkprocess)
								{$join}
							WHERE 1=1
								{$where}
							ORDER BY a2.sName
						";
		return parent::executeQuery();
	}

	public function drawHeader(){
		foreach($this->caHeaders as $moHeader){
			if($moHeader->getAttrName()=='header_filter'){
				$moHeaderFilter = $moHeader;
			}elseif($moHeader->getAttrName()=='header_filter_item'){
				$moHeaderFilterItem = $moHeader;
			}elseif($moHeader->getAttrName()=='report_header_blank'){
				$moHeaderBlank = $moHeader;
			}else{
				$this->coWriter->drawLine($moHeader,array());
			}
		}
		if ($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());

		$moFilterText = FWDWebLib::getObject('header_filter_text');
		$moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
		$filters = $this->coFilter->getSummary();
		foreach($filters as $maFilter){
			$moFilterText->setValue($maFilter['name']);
			$this->coWriter->drawLine($moHeaderFilter,array());
			foreach($maFilter['items'] as $msFilterItem){
				$moFilterItemText->setValue($msFilterItem);
				$this->coWriter->drawLine($moHeaderFilterItem,array());
			}
		}
		$this->coWriter->drawLine($moHeaderBlank,array());
	}
}
?>