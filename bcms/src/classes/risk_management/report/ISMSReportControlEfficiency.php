<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

define('CONTROL_IS_EFFICIENT', 2301);
define('CONTROL_IS_NOT_EFFICIENT', 2302);

/**
 * Classe ISMSReportControlEfficiency.
 *
 * <p>Classe que implementa o relat�rio de efici�ncia dos controles.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportControlEfficiency extends ISMSReport {
	
	/**
	 * Inicializa o relat�rio.
	 *
	 * <p>M�todo para inicializar o relat�rio.
	 * Necess�rio para funcionar com o XML compilado.</p>
	 * @access public
	 */	
	public function init() {
		parent::init();
		
		$this->coDataSet->addFWDDBField(new FWDDBField("","is_efficient",DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("","control_id",DB_NUMBER));		
		$this->coDataSet->addFWDDBField(new FWDDBField("","control_name",DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField("","control_is_active",DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("","control_real_efficiency",DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("","control_expected_efficiency",DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("","control_responsible",DB_STRING));
	}
	
	/**
	 * Executa a query do relat�rio.
	 *
	 * <p>M�todo para executar a query do relat�rio.</p>
	 * @access public
	 */
	public function makeQuery() {

    $moFilter = $this->getFilter();
    $maFilters = array();
    $msWhere ="";

    $maClassifType = $moFilter->getControlClassifType();
    
    if(count($maClassifType)){
      if(isset($maClassifType['null'])){
        $maFilters[] = " c.fkType IS NULL ";
      }else{
        $maFilters[] = " c.fkType IN ( ".implode(',',array_keys($maClassifType)) ." ) ";
      }
    }

    if(count($maFilters)){
        $msWhere .= " AND ". implode(' AND ',$maFilters);
    }

    $msSelectNotEfficient  = "SELECT " . CONTROL_IS_NOT_EFFICIENT . " as is_efficient, 
                                 c.fkContext as control_id, 
                                 c.sName as control_name, 
                                 c.bIsActive as control_is_active,
                                 wce.nRealEfficiency as control_real_efficiency,
                                 wce.nExpectedEfficiency as control_expected_efficiency,
                                 u.sName as control_responsible
                            FROM rm_control c
                                 JOIN isms_context ctx ON (ctx.pkContext = c.fkContext AND ctx.nState <> 2705)
                                 JOIN wkf_control_efficiency wce ON (c.fkContext = wce.fkControlEfficiency)
                                 JOIN view_isms_user_active u ON (c.fkResponsible = u.fkContext)
                            WHERE c.dDateImplemented IS NOT NULL
                                  AND c.bRevisionIsLate = 0
                                  AND c.bEfficiencyNotOk = 1
                                  $msWhere";

    $msSelectEfficient = "SELECT " . CONTROL_IS_EFFICIENT . " as is_efficient, 
                                     c.fkContext as control_id,
                                     c.sName as control_name,
                                     c.bIsActive as control_is_active,
                                     wce.nRealEfficiency as control_real_efficiency,
                                     wce.nExpectedEfficiency as control_expected_efficiency,
                                     u.sName as control_responsible
                                FROM rm_control c
                                     JOIN isms_context ctx ON (ctx.pkContext = c.fkContext AND ctx.nState <> 2705)
                                     JOIN wkf_control_efficiency wce ON (c.fkContext = wce.fkControlEfficiency)
                                     JOIN view_isms_user_active u ON (c.fkResponsible = u.fkContext)
                                WHERE c.dDateImplemented IS NOT NULL  
                                      AND c.bRevisionIsLate = 0
                                      AND c.bEfficiencyNotOk = 0
                                      $msWhere";

		if ($moFilter->mustShowOnlyEfficient()) $this->csQuery = $msSelectEfficient;
		else if ($moFilter->mustShowOnlyNotEfficient()) $this->csQuery = $msSelectNotEfficient;		
		else $this->csQuery = $msSelectEfficient . ' UNION ' . $msSelectNotEfficient;
		
		$this->csQuery .= ' ORDER BY is_efficient ASC';
		
		return parent::executeQuery();
	}
  
    /**
   * Desenha o cabe�alho do relat�rio.
   *
   * <p>M�todo para desenhar o cabe�alho do relat�rio.</p>
   * @access public
   */
  public function drawHeader(){
    foreach($this->caHeaders as $moHeader){
      if($moHeader->getAttrName()=='header_filter'){
        $moHeaderFilter = $moHeader;
      }elseif($moHeader->getAttrName()=='header_filter_item'){
        $moHeaderFilterItem = $moHeader;
      }elseif($moHeader->getAttrName()=='report_header_blank'){
        $moHeaderBlank = $moHeader;
      }else{
        $this->coWriter->drawLine($moHeader,array());
      }
    }
    if ($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());
    
    $moFilterText = FWDWebLib::getObject('header_filter_text');
    $moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
    $maFilters = $this->coFilter->getSummary();
    foreach($maFilters as $maFilter){
      $moFilterText->setValue($maFilter['name']);
      $this->coWriter->drawLine($moHeaderFilter,array());
      foreach($maFilter['items'] as $msFilterItem){
        $moFilterItemText->setValue($msFilterItem);
        $this->coWriter->drawLine($moHeaderFilterItem,array());
      }
    }
    $this->coWriter->drawLine($moHeaderBlank,array());
  }

}
?>