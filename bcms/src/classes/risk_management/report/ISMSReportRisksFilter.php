<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportRisksFilter.
 *
 * <p>Classe que implementa o filtro do relat�rio de riscos.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportRisksFilter extends FWDReportFilter {

  const ALL = 0;
  const ONLY_ESTIMATED = 1;
  const ONLY_NOT_ESTIMATED = 2;
  const ONLY_TREATED = 1;
  const ONLY_NOT_TREATED = 2;

  protected $csValue = '';
  
  protected $csResidualValue = '';
  
  protected $ciShowEstimated = self::ALL;
  
  protected $ciShowTreated = self::ALL;

  public function setValue($psValue){
    $this->csValue = $psValue;
  }

  public function getValue(){
    return $this->csValue;
  }

  public function setResidualValue($psResidualValue){
    $this->csResidualValue = $psResidualValue;
  }

  public function getResidualValue(){
    return $this->csResidualValue;
  }

  public function getShowEstimated(){
    return $this->ciShowEstimated;
  }

  public function setShowEstimated($piShowEstimated){
    $this->ciShowEstimated = $piShowEstimated;
  }

  public function getShowTreated(){
    return $this->ciShowTreated;
  }

  public function setShowTreated($piShowTreated){
    $this->ciShowTreated = $piShowTreated;
  }

  public function getSummary(){
    
    $maFilters = array();
    
    if($this->csValue){
      switch($this->csValue){
        case 'high': $msValueLabel = FWDLanguage::getPHPStringValue('rs_high',"Alto");                  break;
        case 'mid' : $msValueLabel = FWDLanguage::getPHPStringValue('rs_medium',"M�dio");               break;
        case 'low' : $msValueLabel = FWDLanguage::getPHPStringValue('rs_low',"Baixo");                  break;
        case 'np'  : $msValueLabel = FWDLanguage::getPHPStringValue('rs_not_estimated',"N�o Estimado"); break;
      }
      $maFilters[] = array(
        'name' => FWDLanguage::getPHPStringValue('rs_value_cl','Risco Potencial:'),
        'items' => array($msValueLabel)
      );
    }
    
    if($this->csResidualValue){
      switch($this->csResidualValue){
        case 'high': $msValueLabel = FWDLanguage::getPHPStringValue('rs_high',"Alto");                  break;
        case 'mid' : $msValueLabel = FWDLanguage::getPHPStringValue('rs_medium',"M�dio");               break;
        case 'low' : $msValueLabel = FWDLanguage::getPHPStringValue('rs_low',"Baixo");                  break;
        case 'np'  : $msValueLabel = FWDLanguage::getPHPStringValue('rs_not_estimated',"N�o Estimado"); break;
      }
      $maFilters[] = array(
        'name' => FWDLanguage::getPHPStringValue('rs_residual_value_cl','Risco Residual:'),
        'items' => array($msValueLabel)
      );
    }
    
    if($this->ciShowEstimated){
      switch($this->ciShowEstimated){
        case self::ONLY_ESTIMATED:     $msShowEstimated = FWDLanguage::getPHPStringValue('rs_estimated_risks',"riscos estimados");         break;
        case self::ONLY_NOT_ESTIMATED: $msShowEstimated = FWDLanguage::getPHPStringValue('rs_not_estimated_risks',"riscos n�o estimados"); break;
      }
      $maFilters[] = array(
        'name' => FWDLanguage::getPHPStringValue('rs_show_only_cl','Exibir Apenas:'),
        'items' => array($msShowEstimated)
      );
    }
    
    if($this->ciShowTreated){
      switch($this->ciShowTreated){
        case self::ONLY_TREATED:     $msShowTreated = FWDLanguage::getPHPStringValue('rs_treated_risks',"riscos tratados");         break;
        case self::ONLY_NOT_TREATED: $msShowTreated = FWDLanguage::getPHPStringValue('rs_not_treated_risks',"riscos n�o tratados"); break;
      }
      $maFilters[] = array(
        'name' => FWDLanguage::getPHPStringValue('rs_show_only_cl','Exibir Apenas:'),
        'items' => array($msShowTreated)
      );
    }
    
    return $maFilters;
  }

}

?>