<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe ISMSReportPendantContexts.
 *
 * <p>Classe que implementa o relat�rio contextos pendentes.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportPendantContexts extends ISMSReport {
  
  /**
   * Inicializa o relat�rio.
   *
   * <p>M�todo para inicializar o relat�rio.
   * Necess�rio para funcionar com o XML compilado.</p>
   * @access public
   */  
  public function init() {
    parent::init();
        
    $this->coDataSet->addFWDDBField(new FWDDBField("","context_id",DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("","context_type",DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("","context_name",DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("","context_value",DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("","context_approver_id",DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("","context_approver_name",DB_STRING));
  }
  
  /**
   * Executa a query do relat�rio.
   *
   * <p>M�todo para executar a query do relat�rio.</p>
   * @access public
   */
  public function makeQuery() {
    $miAssetController = ISMSLib::getConfigById(USER_ASSET_CONTROLLER);
    $miControlController = ISMSLib::getConfigById(USER_CONTROL_CONTROLLER);
    $miLibrarian = ISMSLib::getConfigById(USER_LIBRARIAN);
    
    $msAreaType = "";
    $msProcessType = "";
    $msControlType = "";
    $msAreaPriority = "";
    $msProcessPriority = "";
    $msEventType = "";
    $msArea = "";
    $msProcess = "";
    $msRiskType = "";
    
    $moFilter = $this->getFilter();
    $maFilters = array();

    $maClassifType = $moFilter->getAreaClassifType();
    if(count($maClassifType)){
      if(isset($maClassifType['null'])){
        $msAreaType = " c.fkType IS NULL ";
      }else{
        $msAreaType = " c.fkType IN ( ".implode(',',array_keys($maClassifType)) ." ) ";
      }
    }
    $maClassifType = $moFilter->getProcessClassifType();
    if(count($maClassifType)){
      if(isset($maClassifType['null'])){
        $msProcessType = " c.fkType IS NULL ";
      }else{
        $msProcessType = " c.fkType IN ( ".implode(',',array_keys($maClassifType)) ." ) ";
      }
    }
    $maClassifType = $moFilter->getRiskClassifType();
    if(count($maClassifType)){
      if(isset($maClassifType['null'])){
        $msRiskType = " WHERE c.fkType IS NULL ";
      }else{
        $msRiskType = " WHERE  c.fkType IN ( ".implode(',',array_keys($maClassifType)) ." ) ";
      }
    }
    $maClassifType = $moFilter->getControlClassifType();
    if(count($maClassifType)){
      if(isset($maClassifType['null'])){
        $msControlType = " WHERE c.fkType IS NULL ";
      }else{
        $msControlType = " WHERE c.fkType IN ( ".implode(',',array_keys($maClassifType)) ." ) ";
      }
    }
    $maClassifType = $moFilter->getEventClassifType();
    if(count($maClassifType)){
      if(isset($maClassifType['null'])){
        $msEventType = " WHERE c.fkType IS NULL ";
      }else{
        $msEventType = " WHERE c.fkType IN ( ".implode(',',array_keys($maClassifType)) ." ) ";
      }
    }
    $maClassifType = $moFilter->getAreaClassifPriority();
    if(count($maClassifType)){
      if(isset($maClassifType['null'])){
        $msAreaPriority = "  c.fkPriority IS NULL ";
      }else{
        $msAreaPriority = " c.fkPriority IN ( ".implode(',',array_keys($maClassifType)) ." ) ";
      }
    }
    $maClassifType = $moFilter->getProcessClassifPriority();
    if(count($maClassifType)){
      if(isset($maClassifType['null'])){
        $msProcessPriority = " c.fkPriority IS NULL ";
      }else{
        $msProcessPriority = " c.fkPriority IN ( ".implode(',',array_keys($maClassifType)) ." ) ";
      }
    }
    if($msAreaType && $msAreaPriority){
      $msArea = " WHERE " . $msAreaType ." AND ". $msAreaPriority;
    }else{
      if($msAreaType || $msAreaPriority){
        $msArea = " WHERE " . $msAreaType . $msAreaPriority;
      }
    }
    if($msProcessType && $msProcessPriority){
      $msProcess = " WHERE " . $msProcessType ." AND ". $msProcessPriority;
    }else{
      if($msProcessType || $msProcessPriority){
        $msProcess = " WHERE " . $msProcessType . $msProcessPriority;
      }
    }

    $this->csQuery = "
                      SELECT c.fkContext as context_id, cont.nType as context_type, c.sName as context_name, c.nValue as context_value, ap.fkResponsible as context_approver_id, u.sName as context_approver_name
                      FROM isms_context cont
                      JOIN rm_area c ON (cont.pkContext = c.fkContext AND cont.nState = " . CONTEXT_STATE_PENDANT . ")
                      LEFT JOIN rm_area ap ON (c.fkParent = ap.fkContext)
                      LEFT JOIN isms_user u ON (ap.fkResponsible = u.fkContext)
                      $msArea
                      
                      UNION
                      
                      
                      SELECT c.fkContext as context_id, cont.nType as context_type, c.sName as context_name, c.nValue as context_value, a.fkResponsible as context_approver_id, u.sName as context_approver_name
                      FROM isms_context cont
                      JOIN rm_process c ON (cont.pkContext = c.fkContext AND cont.nState = " . CONTEXT_STATE_PENDANT . ")
                      JOIN rm_area a ON (c.fkArea = a.fkContext)
                      JOIN isms_user u ON (a.fkResponsible = u.fkContext)
                      $msProcess
                      
                      UNION
                      
                      
                      SELECT c.fkContext as context_id, cont.nType as context_type, c.sName as context_name, c.nValue as context_value, $miAssetController as context_approver_id, u.sName as context_approver_name
                      FROM isms_context cont
                      JOIN rm_asset c ON (cont.pkContext = c.fkContext AND cont.nState = " . CONTEXT_STATE_PENDANT . ")
                      JOIN isms_user u ON (u.fkContext = $miAssetController)
                      
                      UNION
                      
                      
                      SELECT c.fkContext as context_id, cont.nType as context_type, c.sName as context_name, c.nValueResidual as context_value, a.fkResponsible as context_approver_id, u.sName as context_approver_name
                      FROM isms_context cont
                      JOIN rm_risk c ON (cont.pkContext = c.fkContext AND cont.nState = " . CONTEXT_STATE_PENDANT . ")
                      JOIN rm_asset a ON (c.fkAsset = a.fkContext)
                      JOIN isms_user u ON (a.fkResponsible = u.fkContext)
                      $msRiskType
                      
                      UNION
                      
                      
                      SELECT c.fkContext as context_id, cont.nType as context_type, c.sName as context_name, c.bIsActive as context_value, $miControlController as context_approver_id, u.sName as context_approver_name
                      FROM isms_context cont
                      JOIN rm_control c ON (cont.pkContext = c.fkContext AND cont.nState = " . CONTEXT_STATE_PENDANT . ")
                      JOIN isms_user u ON (u.fkContext = $miControlController)
                      $msControlType
                      
                      UNION
                      
                      
                      SELECT c.fkContext as context_id, cont.nType as context_type, c.sName as context_name, 0 as context_value, $miLibrarian as context_approver_id, u.sName as context_approver_name
                      FROM isms_context cont
                      JOIN rm_best_practice c ON (cont.pkContext = c.fkContext AND cont.nState = " . CONTEXT_STATE_PENDANT . ")
                      JOIN isms_user u ON (u.fkContext = $miLibrarian)
                      
                      UNION
                      
                      
                      SELECT c.fkContext as context_id, cont.nType as context_type, c.sName as context_name, 0 as context_value, $miLibrarian as context_approver_id, u.sName as context_approver_name
                      FROM isms_context cont
                      JOIN rm_category c ON (cont.pkContext = c.fkContext AND cont.nState = " . CONTEXT_STATE_PENDANT . ")
                      JOIN isms_user u ON (u.fkContext = $miLibrarian)
                      
                      UNION
                      
                      
                      SELECT c.fkContext as context_id, cont.nType as context_type, c.sDescription as context_name, 0 as context_value, $miLibrarian as context_approver_id, u.sName as context_approver_name
                      FROM isms_context cont
                      JOIN rm_event c ON (cont.pkContext = c.fkContext AND cont.nState = " . CONTEXT_STATE_PENDANT . ")
                      JOIN isms_user u ON (u.fkContext = $miLibrarian)
                      $msEventType
                      
                      UNION
                      
                      
                      SELECT c.fkContext as context_id, cont.nType as context_type, c.sName as context_name, 0 as context_value, $miLibrarian as context_approver_id, u.sName as context_approver_name
                      FROM isms_context cont
                      JOIN rm_section_best_practice c ON (cont.pkContext = c.fkContext AND cont.nState = " . CONTEXT_STATE_PENDANT . ")
                      JOIN isms_user u ON (u.fkContext = $miLibrarian)
                      
                      UNION
                      
                      
                      SELECT c.fkContext as context_id, cont.nType as context_type, c.sName as context_name, 0 as context_value, $miLibrarian as context_approver_id, u.sName as context_approver_name
                      FROM isms_context cont
                      JOIN rm_standard c ON (cont.pkContext = c.fkContext AND cont.nState = " . CONTEXT_STATE_PENDANT . ")
                      JOIN isms_user u ON (u.fkContext = $miLibrarian)
                      
                      ORDER BY context_type";
    
    return parent::executeQuery();
  }

  /**
   * Desenha o cabe�alho do relat�rio.
   *
   * <p>M�todo para desenhar o cabe�alho do relat�rio.</p>
   * @access public
   */
  public function drawHeader(){
    foreach($this->caHeaders as $moHeader){
      if($moHeader->getAttrName()=='header_filter'){
        $moHeaderFilter = $moHeader;
      }elseif($moHeader->getAttrName()=='header_filter_item'){
        $moHeaderFilterItem = $moHeader;
      }elseif($moHeader->getAttrName()=='report_header_blank'){
        $moHeaderBlank = $moHeader;
      }else{
        $this->coWriter->drawLine($moHeader,array());
      }
    }
    if ($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());
    
    $moFilterText = FWDWebLib::getObject('header_filter_text');
    $moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
    $maFilters = $this->coFilter->getSummary();
    foreach($maFilters as $maFilter){
      $moFilterText->setValue($maFilter['name']);
      $this->coWriter->drawLine($moHeaderFilter,array());
      foreach($maFilter['items'] as $msFilterItem){
        $moFilterItemText->setValue($msFilterItem);
        $this->coWriter->drawLine($moHeaderFilterItem,array());
      }
    }
    $this->coWriter->drawLine($moHeaderBlank,array());
  }
}
?>