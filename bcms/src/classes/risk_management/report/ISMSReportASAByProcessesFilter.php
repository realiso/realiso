<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

class ISMSReportASAByProcessesFilter extends ISMSReportBCMSFilter {

	private $place = 0;
	private $area = 0;
	private $process = 0;
	
	public function setPlace($place){
		$this->place = $place;
	}
	
	public function getPlace(){
		return $this->place;
	}
	
	public function setArea($area){
		$this->area = $area;
	}
	
	public function getArea(){
		return $this->area;
	}
	
	public function setProcess($process){
		$this->process = $process;
	}
	
	public function getProcess(){
		return $this->process;
	}
	
	public function getSummary(){
		$maFilters = parent::getSummary();
	
		$placeId = $this->getPlace();
		if($placeId){
			$place = new CMPlace();
			$place->fetchById($placeId);
			$placeName = $place->getName();
			$maFilters[] = array(
			        'name' => FWDLanguage::getPHPStringValue('asa_place', 'Local:'),
			        'items' => array($placeName)
			);
		}
	
		$areaId = $this->getArea();
		if($areaId){
			$area = new RMArea();
			$area->fetchById($areaId);
			$areaName = $area->getName();
			$maFilters[] = array(
			        'name' => FWDLanguage::getPHPStringValue('asa_area', 'Unidade de Neg�cio:'),
			        'items' => array($areaName)
			);
		}
	
		$processId = $this->getProcess();
		if($processId){
			$process = new RMProcess();
			$process->fetchById($processId);
			$processName = $process->getName();
			$maFilters[] = array(
			        'name' => FWDLanguage::getPHPStringValue('asa_process', 'Processo:'),
			        'items' => array($processName)
			);
		}
	
		return $maFilters;
	}	
}
?>