<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportControls.
 *
 * <p>Classe que implementa o relatório de controles.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportControls extends ISMSFilteredReport {
  
  public function __construct(){
    parent::__construct();
  }
  
 /**
  * Inicializa o relatório.
  *
  * <p>Método para inicializar o relatório.
  * Necessário para funcionar com o XML compilado.</p>
  * @access public
  */
  public function init(){
    parent::init();
    $this->coDataSet->addFWDDBField(new FWDDBField('c.fkContext','control_id'         ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('c.sName'    ,'control_name'       ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('u.sName'    ,'control_responsible',DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('c.bIsActive','control_is_active'  ,DB_NUMBER));
  }
  
 /**
  * Executa a query do relatório.
  *
  * <p>Método para executar a query do relatório.</p>
  * @access public
  */
  public function makeQuery(){
    $miImplementationStatus = $this->coFilter->getImplementationStatus();
    
    $miTimestamp = FWDWebLib::getTime();
    $miTimestamp = mktime(0, 0, 0, date('m',$miTimestamp), date('d',$miTimestamp),  date('Y',$miTimestamp));
    $msCurrentTime = FWDWebLib::timestampToDBFormat($miTimestamp);
    
    $maFilters = array();
    
    switch($miImplementationStatus){
      case ISMSReportControlsFilter::PLANNED_IMPLANTATION:{
        $maFilters[] = "(c.dDateDeadline >= {$msCurrentTime} AND c.dDateImplemented IS NULL)";
        break;
      }
      case ISMSReportControlsFilter::PLANNED_LATE:{
        $maFilters[] = "(c.dDateDeadline < $msCurrentTime AND c.dDateImplemented IS NULL)";
        break;
      }
      case ISMSReportControlsFilter::PLANNED_TOTAL:{
        $maFilters[] = "c.dDateImplemented IS NULL";
        break;
      }
      case ISMSReportControlsFilter::IMPLEMENTED_TRUSTED_AND_EFFICIENT:{
        $maFilters[] = "(
          c.dDateImplemented IS NOT NULL
          AND c.fkContext IN ( SELECT fkControlEfficiency FROM wkf_control_efficiency WHERE fkControlEfficiency = c.fkContext )
          AND c.fkContext IN ( SELECT fkControlTest FROM wkf_control_test WHERE fkControlTest = c.fkContext )
          AND c.bRevisionIsLate = 0
          AND c.bTestIsLate = 0
          AND c.bEfficiencyNotOk = 0
          AND c.bTestNotOk = 0
        )";
        break;
      }
      case ISMSReportControlsFilter::CONTROL_TEST_NOT_OK:{
        $maFilters[] = "(c.dDateImplemented IS NOT NULL AND c.bTestIsLate = 0 AND c.bTestNotOk = 1)";
        break;
      }
      case ISMSReportControlsFilter::CONTROL_REVISION_NOT_OK:{
        $maFilters[] = "(c.dDateImplemented IS NOT NULL AND c.bRevisionIsLate = 0 AND c.bEfficiencyNotOk = 1)";
        break;
      }
      case ISMSReportControlsFilter::CONTROL_NOT_MEASURED:{
        $maFilters[] = "(
          c.dDateImplemented IS NOT NULL
          AND (
            c.bTestIsLate = 1
            OR c.bRevisionIsLate = 1
            OR c.fkContext NOT IN ( SELECT fkControlEfficiency FROM wkf_control_efficiency WHERE fkControlEfficiency = c.fkContext )
            OR c.fkContext NOT IN ( SELECT fkControlTest FROM wkf_control_test WHERE fkControlTest = c.fkContext )
          )
        )";
        break;
      }
      case ISMSReportControlsFilter::IMPLEMENTED_TOTAL:{
        $maFilters[] = "c.dDateImplemented IS NOT NULL";
        break;
      }
    }
    
    if(count($maFilters)==0){
      $msWhere = '';
    }else{
      $msWhere = ' WHERE '.implode(' AND ',$maFilters);
    }
    $this->csQuery = "SELECT
                        c.fkContext AS control_id,
                        c.sName AS control_name,
                        u.sName AS control_responsible,
                        c.bIsActive AS control_is_active
                      FROM
                        rm_control c
                        JOIN isms_context ctx ON (c.fkContext = ctx.pkContext AND ctx.nState != ".CONTEXT_STATE_DELETED.")
                        JOIN isms_user u ON (u.fkContext = c.fkResponsible)
                      {$msWhere}
                      ORDER BY c.sName";
    return parent::executeQuery();
  }

}

?>