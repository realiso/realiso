<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportAreas.
 *
 * <p>Classe que implementa o relat�rio de �reas.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportAreas extends ISMSFilteredReport {

	public function __construct(){
		parent::__construct();
	}

	/**
	 * Inicializa o relat�rio.
	 *
	 * <p>M�todo para inicializar o relat�rio.
	 * Necess�rio para funcionar com o XML compilado.</p>
	 * @access public
	 */
	public function init(){
		parent::init();
		
		$col1 = new FWDDBField('a.sName'    ,'area_name'       ,DB_STRING);
		$col1->setLabel(FWDLanguage::getPHPStringValue('rs_name','Nome'));
		$col1->setShowColumn(true);
		
		$this->coDataSet->addFWDDBField($col1);
	}

	/**
	 * Executa a query do relat�rio.
	 *
	 * <p>M�todo para executar a query do relat�rio.</p>
	 * @access public
	 */
	public function makeQuery(){

		$maFilters = array();

		if(count($maFilters)==0){
			$msWhere = '';
		}else{
			$msWhere = ' WHERE '.implode(' AND ',$maFilters);
		}
		$this->csQuery = "SELECT
                        a.sName AS area_name
                      FROM
                        view_rm_area_active a
                        {$msWhere}
                      ORDER BY a.sName";
                        return parent::executeQuery();
	}

}

?>