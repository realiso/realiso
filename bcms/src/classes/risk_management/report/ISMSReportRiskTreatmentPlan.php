<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe ISMSReportRiskTreatmentPlan.
 *
 * <p>Classe que implementa o relat�rio de plano de tratamento de riscos.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportRiskTreatmentPlan extends ISMSReport {
  
  /**
   * Inicializa o relat�rio.
   *
   * <p>M�todo para inicializar o relat�rio.
   * Necess�rio para funcionar com o XML compilado.</p>
   * @access public
   */  
  public function init() {
    parent::init();
        
    $this->coDataSet->addFWDDBField(new FWDDBField("","asset_id",DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("","asset_name",DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("","asset_value",DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("","risk_id",DB_NUMBER));    
    $this->coDataSet->addFWDDBField(new FWDDBField("","risk_name",DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("","risk_impact",DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("","risk_value",DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("","risk_residual_value",DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("","risk_accept_mode",DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("","control_id",DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("","control_name",DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("","control_deadline",DB_DATETIME));
    $this->coDataSet->addFWDDBField(new FWDDBField("","control_is_active",DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("","control_responsible_id",DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("","control_responsible_name",DB_STRING));
  }
  
  /**
   * Executa a query do relat�rio.
   *
   * <p>M�todo para executar a query do relat�rio.</p>
   * @access public
   */
  public function makeQuery() {
    $miLowRisk = ISMSLib::getConfigById(RISK_LOW);

    $moFilter = $this->getFilter();
    $maFilters = array();
    $msWhere ="";

    $maClassifRisk = $moFilter->getRiskClassifType();
    if(count($maClassifRisk)){
      if(isset($maClassifRisk['null'])){
        $maFilters[] = " r.fkType IS NULL ";
      }else{
        $maFilters[] = " r.fkType IN ( ".implode(',',array_keys($maClassifRisk)) ." ) ";
      }
    }
    $maClassifType = $moFilter->getControlClassifType();
    if(count($maClassifType)){
      if(isset($maClassifType['null'])){
        $maFilters[] = " c.fkType IS NULL ";
      }else{
        $maFilters[] = " c.fkType IN ( ".implode(',',array_keys($maClassifType)) ." ) ";
      }
    }
    if(count($maFilters)){
        $msWhere .= " AND ". implode(' AND ',$maFilters);
    }

    $this->csQuery = "SELECT a.fkContext as asset_id, a.sName as asset_name, a.nValue as asset_value,
                        r.fkContext as risk_id, r.sName as risk_name, r.tImpact as risk_impact, r.nValue as risk_value, r.nValueResidual as risk_residual_value, r.nAcceptMode as risk_accept_mode,
                        c.fkContext as control_id, c.sName as control_name, c.dDateDeadline as control_deadline, c.bIsActive as control_is_active,
                        u.fkContext as control_responsible_id, u.sName as control_responsible_name
                      FROM view_rm_asset_active a
                      JOIN view_rm_risk_active r ON (a.fkContext = r.fkAsset)
                      LEFT JOIN view_rm_risk_control_active rc ON (r.fkContext = rc.fkRisk)
                      LEFT JOIN view_rm_control_active c ON (rc.fkControl = c.fkContext)
                      LEFT JOIN view_isms_user_active u ON (c.fkResponsible = u.fkContext)
                      WHERE r.nValue > $miLowRisk AND (c.fkContext IN (SELECT pkContext FROM isms_context WHERE nState <> " . CONTEXT_STATE_DELETED . ") OR c.fkContext IS NULL)
                      $msWhere
                      ORDER BY a.nValue, a.fkContext, r.nValueResidual, r.fkcontext, c.bIsActive, c.fkContext";
    
    return parent::executeQuery();
  }
  
  /**
   * Desenha o cabe�alho do relat�rio.
   *
   * <p>M�todo para desenhar o cabe�alho do relat�rio.</p>
   * @access public
   */
  public function drawHeader(){
    foreach($this->caHeaders as $moHeader){
      if($moHeader->getAttrName()=='header_filter'){
        $moHeaderFilter = $moHeader;
      }elseif($moHeader->getAttrName()=='header_filter_item'){
        $moHeaderFilterItem = $moHeader;
      }elseif($moHeader->getAttrName()=='report_header_blank'){
        $moHeaderBlank = $moHeader;
      }else{
        $this->coWriter->drawLine($moHeader,array());
      }
    }
    if ($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());
    
    $moFilterText = FWDWebLib::getObject('header_filter_text');
    $moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
    $maFilters = $this->coFilter->getSummary();
    foreach($maFilters as $maFilter){
      $moFilterText->setValue($maFilter['name']);
      $this->coWriter->drawLine($moHeaderFilter,array());
      foreach($maFilter['items'] as $msFilterItem){
        $moFilterItemText->setValue($msFilterItem);
        $this->coWriter->drawLine($moHeaderFilterItem,array());
      }
    }
    $this->coWriter->drawLine($moHeaderBlank,array());
  }
}
?>