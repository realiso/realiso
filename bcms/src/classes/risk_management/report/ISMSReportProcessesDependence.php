<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

class ISMSReportProcessesDependence extends ISMSReport {

	public function init() {
		parent::init();
		
		
	    $col1 = new FWDDBField("","process_name", DB_STRING);
	    $col1->setLabel(FWDLanguage::getPHPStringValue('rs_process','Processo'));
	    $col1->setShowColumn(true);
	    
	    $col2 = new FWDDBField("","area_name", DB_STRING);
	    $col2->setLabel(FWDLanguage::getPHPStringValue('ti_un','Unidade de Neg�cio'));
	    $col2->setShowColumn(true);
	    
	    $col3 = new FWDDBField("","process_input_name", DB_STRING);
	    $col3->setLabel(FWDLanguage::getPHPStringValue('rs_processes_dependence', 'Processo(s) Dependente(s)'));
	    $col3->setShowColumn(true);
	    
	    $this->coDataSet->addFWDDBField($col1);
	    $this->coDataSet->addFWDDBField($col2);
	    $this->coDataSet->addFWDDBField($col3);
	    
	}

	public function makeQuery() {
		
		$this->csQuery = "
							SELECT 
								p.sname as process_name, 
								a.sname as area_name, 
								pa.sname as process_input_name
							FROM
								view_rm_process_active p
								JOIN view_rm_area_active a on (p.fkarea = a.fkcontext)
								JOIN cm_process_inputs pi on (p.fkcontext = pi.fk_process)
								JOIN view_rm_process_active pa on (pi.fk_process_input = pa.fkcontext)
							ORDER BY
								p.sname, pa.sname
							
		                 ";
		
		
		//echo $this->csQuery;
	  	return parent::executeQuery();
	}

	public function drawHeader(){
		foreach($this->caHeaders as $moHeader){
			if($moHeader->getAttrName()=='header_filter'){
				$moHeaderFilter = $moHeader;
			}elseif($moHeader->getAttrName()=='header_filter_item'){
				$moHeaderFilterItem = $moHeader;
			}elseif($moHeader->getAttrName()=='report_header_blank'){
				$moHeaderBlank = $moHeader;
			}else{
				$this->coWriter->drawLine($moHeader,array());
			}
		}
		if ($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());

		$moFilterText = FWDWebLib::getObject('header_filter_text');
		$moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
		$filters = $this->coFilter->getSummary();
		foreach($filters as $maFilter){
			$moFilterText->setValue($maFilter['name']);
			$this->coWriter->drawLine($moHeaderFilter,array());
			foreach($maFilter['items'] as $msFilterItem){
				$moFilterItemText->setValue($msFilterItem);
				$this->coWriter->drawLine($moHeaderFilterItem,array());
			}
		}
		$this->coWriter->drawLine($moHeaderBlank,array());
	}
}
?>
