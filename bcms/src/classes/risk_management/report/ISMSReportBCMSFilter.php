<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

include_once $handlers_ref . "select/continuity/QuerySelectPlaceType.php";

class ISMSReportBCMSFilter extends FWDReportFilter {

	protected $csReportType = '';

	protected $processType = array();

	protected $processPriority = array();

	protected $placeType = array();

	public function setReportType($psReportType) {
		$this->csReportType = $psReportType;
	}

	public function getReportType() {
		return $this->csReportType;
	}

	public function getProcessType(){
		return $this->processType;
	}

	public function setProcessType($processType){
		$this->processType = $processType;
	}

	public function getPlaceType(){
		return $this->placeType;
	}

	public function setPlaceType($placeType){
		$this->placeType = $placeType;
	}


	public function getProcessPriority(){
		return $this->processPriority;
	}

	public function setProcessPriority($processPriority){
		$this->processPriority = $processPriority;
	}

	public function getSummary(){
		$maFilters = array();

		$maFilterProcess = array();

		$processType = $this->getProcessType();
		if(count($processType)){
			$processTypeNames = array();
			foreach($processType as $pt){
				$cc = new ISMSContextClassification();
				$cc->fetchById($pt);
				$processTypeNames[] = $cc->getFieldValue("classif_name");
			}
			$maFilters[] = array(
		        'name' => FWDLanguage::getPHPStringValue('lb_process_types_cl', 'Tipos de Processo:'),
		        'items' => $processTypeNames
			);
		}

		$placeType = $this->getPlaceType();
		if(count($placeType)){
			$placeTypeNames = array();
			$query = new QuerySelectPlaceType(FWDWebLib::getConnection());
			$query->makeQuery();
			$query->executeQuery();
			foreach ($query->getContexts() as $type) {
				list($miContextId,$msHierarchicalName) = $type;
				foreach($placeType as $pt){
					if($pt == $miContextId){
						$placeTypeNames[] = $msHierarchicalName;
					}
				}
			}

			$maFilters[] = array(
		        'name' => FWDLanguage::getPHPStringValue('lb_place_types_cl', 'Tipos de Local:'),
		        'items' => $placeTypeNames
			);
		}

		$processPriority = $this->getProcessPriority();
		if(count($processPriority)){
			$processPriorityNames = array();
			foreach($processPriority as $pt){
				$cc = new CMPriority();
				$cc->fetchById($pt);
				$processPriorityNames[] = $cc->getFieldValue("priority_name");
			}
			$maFilters[] = array(
		        'name' => FWDLanguage::getPHPStringValue('lb_process_priorities_cl', 'Prioridades de Processo:'),
		        'items' => $processPriorityNames
			);
		}

		return $maFilters;
	}
}
?>