<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportControlTestAgendaByResponsible.
 *
 * <p>Classe que implementa o relatório de agenda de teste dos controles por responsável.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportControlTestAgendaByResponsible extends ISMSFilteredReport {

 /**
  * Inicializa o relatório.
  *
  * <p>Método para inicializar o relatório.
  * Necessário para funcionar com o XML compilado.</p>
  * @access public
  */
  public function init(){
    parent::init();
    $this->coDataSet->addFWDDBField(new FWDDBField('u.fkContext',     'responsible_id',     DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('u.sName',         'responsible_name',   DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('c.fkContext',     'control_id',         DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('c.sName',         'control_name',       DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('c.bIsActive',     'control_is_active',  DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('wct.fkSchedule',  'schedule_id',        DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('p.fkContext',     'process_id',         DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('p.sName',         'process_name',       DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('p.nValue',        'process_value',      DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('u2.sName',        'process_responsible',DB_STRING));    
  }

 /**
  * Executa a query do relatório.
  *
  * <p>Método para executar a query do relatório.</p>
  * @access public
  */
  public function makeQuery(){
    $moFilter = $this->getFilter();
    $maFilters = array();
    $msWhere = '';

    $maClassifType = $moFilter->getControlClassifType();
    if(count($maClassifType)){
      if(isset($maClassifType['null'])){
        $maFilters[] = " c.fkType IS NULL ";
      }else{
        $maFilters[] = " c.fkType IN ( ".implode(',',array_keys($maClassifType)) ." ) ";
      }
    }
    if(count($maFilters)){
      $msWhere .= " WHERE ". implode(' AND ', $maFilters);
    }

    $this->csQuery = "SELECT DISTINCT
                      	u.fkContext as responsible_id,
                      	u.sName as responsible_name,
                      	c.fkContext as control_id,
                      	c.sName as control_name,
                      	c.bIsActive as control_is_active,
                      	wct.fkSchedule as schedule_id,
                      	p.fkContext as process_id,
                      	p.sName as process_name,
                      	p.nValue as process_value,
                      	u2.sName as process_responsible                      	
                      FROM
                      	view_isms_user_active u
                      	JOIN view_rm_control_active c ON (u.fkContext = c.fkResponsible)                      	
                      	JOIN wkf_control_test wct ON (wct.fkControlTest = c.fkContext)
                      	LEFT JOIN view_rm_risk_control_active rc ON (c.fkContext = rc.fkControl)
                      	LEFT JOIN view_rm_risk_active r ON (rc.fkRisk = r.fkContext)
                      	LEFT JOIN view_rm_asset_active a ON (r.fkAsset = a.fkContext)
                      	LEFT JOIN view_rm_process_asset_active pa ON (a.fkContext = pa.fkAsset)
                      	LEFT JOIN view_rm_process_active p ON (pa.fkProcess = p.fkContext)
                      	LEFT JOIN view_isms_user_active u2 ON (p.fkResponsible = u2.fkContext)
                        $msWhere
                      ORDER BY
                      	u.sName, u.fkContext, c.sName, c.fkContext, wct.fkSchedule, p.sName, p.fkContext";

    return parent::executeQuery();
  }
}
?>