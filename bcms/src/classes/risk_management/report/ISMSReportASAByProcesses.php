<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

class ISMSReportASAByProcesses extends ISMSReport {

	public function init() {
		parent::init();
		
		
	    $col1 = new FWDDBField("","process_name", DB_STRING);
	    $col1->setLabel(FWDLanguage::getPHPStringValue('rs_process','Processo'));
	    $col1->setShowColumn(true);
	    
	    $col2 = new FWDDBField("","process_ranking", DB_NUMBER);
	    $col2->setLabel(FWDLanguage::getPHPStringValue('rs_rank','Ranking'));
	    $col2->setShowColumn(true);
	    
	    $col3 = new FWDDBField("","process_rto", DB_NUMBER);
	    $col3->setLabel(FWDLanguage::getPHPStringValue('rs_rto_cl','RTO'));
	    $col3->setShowColumn(true);
	    
	   	$col4 = new FWDDBField("","process_rtoflag", DB_NUMBER);
	   	$col4->setLabel(' ');
	   	$col4->setShowColumn(false);
	   	
	    $col5 = new FWDDBField("","asset_name", DB_STRING);
	    $col5->setLabel(FWDLanguage::getPHPStringValue('rs_asset', 'Ativo'));
	    $col5->setShowColumn(true);
	    
	    $col6 = new FWDDBField("","asset_isa_tra", DB_NUMBER);
	    $col6->setLabel(FWDLanguage::getPHPStringValue('rs_isa_tra_cl', 'ISA (TRA)'));
	    $col6->setShowColumn(true);
	    
	    $col7 = new FWDDBField("","asset_tra", DB_NUMBER);
	    $col7->setLabel(FWDLanguage::getPHPStringValue('rs_tac_cl', 'TRA'));
	    $col7->setShowColumn(true);
	    
	    $col8 = new FWDDBField("","asset_traflag", DB_NUMBER);
	   	$col8->setLabel(' ');
	   	$col8->setShowColumn(false);
	   	
	   	$col9 = new FWDDBField("","asset_isa_tac", DB_NUMBER);
	   	$col9->setLabel(FWDLanguage::getPHPStringValue('rs_isa_tac_cl', 'ISA (TAC)'));
	   	$col9->setShowColumn(true);
	    
	    $col10 = new FWDDBField("","asset_tac", DB_NUMBER);
	    $col10->setLabel(FWDLanguage::getPHPStringValue('rs_tac_cl', 'TAC'));
	    $col10->setShowColumn(true);
	    
	    $col11 = new FWDDBField("","asset_tacflag", DB_NUMBER);
	    $col11->setLabel(' ');
	    $col11->setShowColumn(false);
	    
	    $col12 = new FWDDBField("","process_rtominutes", DB_NUMBER);
	    $col12->setLabel(' ');
	    $col12->setShowColumn(false);
	    
	    $col13 = new FWDDBField("","asset_traminutes", DB_NUMBER);
	    $col13->setLabel(' ');
	    $col13->setShowColumn(false);
	    
	    $col14 = new FWDDBField("","asset_tacminutes", DB_NUMBER);
	    $col14->setLabel(' ');
	    $col14->setShowColumn(false);
	    
	    $this->coDataSet->addFWDDBField($col1);
	    $this->coDataSet->addFWDDBField($col2);
	    $this->coDataSet->addFWDDBField($col3);
	    $this->coDataSet->addFWDDBField($col4);
	    $this->coDataSet->addFWDDBField($col5);
	    $this->coDataSet->addFWDDBField($col6);
	    $this->coDataSet->addFWDDBField($col7);
	    $this->coDataSet->addFWDDBField($col8);
	    $this->coDataSet->addFWDDBField($col9);
	    $this->coDataSet->addFWDDBField($col10);
	    $this->coDataSet->addFWDDBField($col11);
	    $this->coDataSet->addFWDDBField($col12);
	    $this->coDataSet->addFWDDBField($col13);
	    $this->coDataSet->addFWDDBField($col14);
	}

	public function makeQuery() {

		$moFilter = $this->getFilter();
		$filters = array();
		$joins = array();
		$maClassifFilter = array();

		$placeType = $moFilter->getPlaceType();
		if(count($placeType)){
			if(isset($placeType['null'])){
				$maClassifFilter[] = " place.fkType IS NULL ";
			}else{
				$maClassifFilter[] = " place.fkType IN ( ".implode(',',$placeType) ." ) ";
			}
		}
		
		$processType = $moFilter->getProcessType();
		if(count($processType)){
			if(isset($processType['null'])){
				$maClassifFilter[] = " p.fkType IS NULL ";
			}else{
				$maClassifFilter[] = " p.fkType IN ( ".implode(',',$processType) ." ) ";
			}
		}
		
		$processPriority = $moFilter->getProcessPriority();
		if(count($processPriority)){
			if(isset($processPriority['null'])){
				$maClassifFilter[] = " p.fkPriority IS NULL ";
			}else{
				$maClassifFilter[] = " p.fkPriority IN ( ".implode(',',$processPriority) ." ) ";
			}
		}

		$place = $moFilter->getPlace();
		if($place){
			$filters[] = " place.fkcontext = {$place} ";
			$joins[] = " JOIN view_cm_place_active place on (place.fkcontext = a.fkplace) ";
			
			
		}

		$area = $moFilter->getArea();
		if($area){
			$filters[] = " area.fkcontext = {$area} ";
			$joins[] = " join view_rm_area_active area on (area.fkcontext = p.fkarea) ";
		}

		$process = $moFilter->getProcess();
		if($process){
			$filters[] = " p.fkcontext = {$process} ";
		}

		$where = "";
		$join = "";
		if(count($filters)){
			$where = ' AND ( ' . implode(' AND ', $filters) ." ) ";
		}

		if(count($maClassifFilter)){
			$where .= " AND (". implode(' OR ',$maClassifFilter).") ";
		}

		if(count($joins)){
			$join = implode(' ', $joins);
		}

		$this->csQuery = "
							SELECT
								p.sname as process_name,
								pr.rto as process_rto,
								pr.rtoflag as process_rtoflag,
								pr.ranking as process_ranking,
								
		                        a.sname as asset_name,
		                        CASE
		                        	WHEN a.tra != 0 THEN trunc(coalesce((1.0*pr.rtominutes)/(1.0*a.tra),0),4)
		                        END AS asset_isa_tra,
		                        a.nrecoverytime as asset_tra,
		                        a.traflag as asset_traflag,		                       
		                        CASE
		                        	WHEN a.tac != 0 THEN trunc(coalesce((1.0*pr.rtominutes)/(1.0*a.tac),0),4)
		                        END AS asset_isa_tac,
		                        a.ntac as asset_tac,
		                        a.tacflag as asset_tacflag,
		                        pr.rtominutes as process_rtominutes,
		                        a.tra as asset_traminutes,
		                        a.tac as asset_tacminutes
							FROM
								view_rm_process_active p
								JOIN view_rm_process_asset_active pa ON (pa.fkprocess = p.fkcontext)
								JOIN view_rm_asset_active a ON (a.fkcontext = pa.fkasset)
								JOIN view_process_ranking pr ON (pr.fkprocess = p.fkcontext)
								{$join}
							WHERE 1=1
								{$where}
							ORDER BY
								asset_isa_tra, asset_isa_tac, p.sname, a.sname
		                     ";
		//echo $this->csQuery;
	  	return parent::executeQuery();
	}

	public function drawHeader(){
		foreach($this->caHeaders as $moHeader){
			if($moHeader->getAttrName()=='header_filter'){
				$moHeaderFilter = $moHeader;
			}elseif($moHeader->getAttrName()=='header_filter_item'){
				$moHeaderFilterItem = $moHeader;
			}elseif($moHeader->getAttrName()=='report_header_blank'){
				$moHeaderBlank = $moHeader;
			}else{
				$this->coWriter->drawLine($moHeader,array());
			}
		}
		if ($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());

		$moFilterText = FWDWebLib::getObject('header_filter_text');
		$moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
		$filters = $this->coFilter->getSummary();
		foreach($filters as $maFilter){
			$moFilterText->setValue($maFilter['name']);
			$this->coWriter->drawLine($moHeaderFilter,array());
			foreach($maFilter['items'] as $msFilterItem){
				$moFilterItemText->setValue($msFilterItem);
				$this->coWriter->drawLine($moHeaderFilterItem,array());
			}
		}
		$this->coWriter->drawLine($moHeaderBlank,array());
	}
}
?>
