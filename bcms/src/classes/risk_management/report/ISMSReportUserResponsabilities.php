<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe ISMSReportUserResponsabilities.
 *
 * <p>Classe que implementa o relat�rio de responsabilidades dos usu�rios.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportUserResponsabilities extends ISMSReport {
  
  /**
   * Inicializa o relat�rio.
   *
   * <p>M�todo para inicializar o relat�rio.
   * Necess�rio para funcionar com o XML compilado.</p>
   * @access public
   */  
  public function init() {
    parent::init();
        
    $this->coDataSet->addFWDDBField(new FWDDBField("","user_id",DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("","user_name",DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("","context_id",DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("","context_name",DB_STRING));    
    $this->coDataSet->addFWDDBField(new FWDDBField("","context_value",DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("","context_type",DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("","ord",DB_NUMBER));
  }
  
  /**
   * Executa a query do relat�rio.
   *
   * <p>M�todo para executar a query do relat�rio.</p>
   * @access public
   */
  public function makeQuery() {
    $moFilter = $this->getFilter();
    $msResponsibleType = $moFilter->getResponsibleType();
    $maResponsibleType = explode(':', $msResponsibleType);
    $msAreaType = "";
    $msProcessType = "";
    $msControlType = "";
    $msAreaPriority = "";
    $msProcessPriority = "";

    $moFilter = $this->getFilter();
    $maFilters = array();
    $msWhere ="";

    $maClassifType = $moFilter->getAreaClassifType();
    if(count($maClassifType)){
      if(isset($maClassifType['null'])){
        $msAreaType = " c.fkType IS NULL ";
      }else{
        $msAreaType = " c.fkType IN ( ".implode(',',array_keys($maClassifType)) ." ) ";
      }
    }
    $maClassifType = $moFilter->getProcessClassifType();
    if(count($maClassifType)){
      if(isset($maClassifType['null'])){
        $msProcessType = " c.fkType IS NULL ";
      }else{
        $msProcessType = " c.fkType IN ( ".implode(',',array_keys($maClassifType)) ." ) ";
      }
    }
    $maClassifType = $moFilter->getRiskClassifType();
    if(count($maClassifType)){
      if(isset($maClassifType['null'])){
        $msRiskType = " c.fkType IS NULL ";
      }else{
        $msRiskType = " c.fkType IN ( ".implode(',',array_keys($maClassifType)) ." ) ";
      }
    }
    $maClassifType = $moFilter->getControlClassifType();
    if(count($maClassifType)){
      if(isset($maClassifType['null'])){
        $msControlType = " c.fkType IS NULL ";
      }else{
        $msControlType = " c.fkType IN ( ".implode(',',array_keys($maClassifType)) ." ) ";
      }
    }
    $maClassifType = $moFilter->getAreaClassifPriority();
    if(count($maClassifType)){
      if(isset($maClassifType['null'])){
        $msAreaPriority = " c.fkPriority IS NULL ";
      }else{
        $msAreaPriority = " c.fkPriority IN ( ".implode(',',array_keys($maClassifType)) ." ) ";
      }
    }
    $maClassifType = $moFilter->getProcessClassifPriority();
    if(count($maClassifType)){
      if(isset($maClassifType['null'])){
        $msProcessPriority = " c.fkPriority IS NULL ";
      }else{
        $msProcessPriority = " c.fkPriority IN ( ".implode(',',array_keys($maClassifType)) ." ) ";
      }
    }

    $msArea = "SELECT u.fkContext as user_id, u.sName as user_name, c.fkContext as context_id, c.sName as context_name, c.nValue as context_value, 'area' as context_type, 1 as ord
                      FROM view_rm_area_active c
                      JOIN view_isms_user_active u ON (c.fkResponsible = u.fkContext)";
    if($msAreaType && $msAreaPriority)
      $msArea = $msArea . " WHERE " . $msAreaType ." AND ". $msAreaPriority;
    else
      if($msAreaType || $msAreaPriority){
        $msArea = $msArea . " WHERE " . $msAreaType . $msAreaPriority;
      }
                      
    $msProcess = "SELECT u.fkContext as user_id, u.sName as user_name, c.fkContext as context_id, c.sName as context_name, c.nValue as context_value, 'process' as context_type, 2 as ord
                      FROM view_rm_process_active c
                      JOIN view_isms_user_active u ON (c.fkResponsible = u.fkContext)";
    if($msProcessType && $msProcessPriority)
      $msProcess = $msProcess . " WHERE " . $msProcessType ." AND ". $msProcessPriority;
    else{
      if($msProcessType || $msProcessPriority)
        $msProcess = $msProcess . " WHERE " . $msProcessType . $msProcessPriority;
    }

    $msAsset = "SELECT u.fkContext as user_id, u.sName as user_name, c.fkContext as context_id, c.sName as context_name, c.nValue as context_value, 'asset' as context_type, 3 as ord
                      FROM view_rm_asset_active c
                      JOIN view_isms_user_active u ON (c.fkResponsible = u.fkContext)";
    $msAssetSecurity = "SELECT u.fkContext as user_id, u.sName as user_name, c.fkContext as context_id, c.sName as context_name, c.nValue as context_value, 'asset_security' as context_type, 4 as ord
                      FROM view_rm_asset_active c
                      JOIN view_isms_user_active u ON (c.fkSecurityResponsible = u.fkContext)";
    $msControl = "SELECT u.fkContext as user_id, u.sName as user_name, c.fkContext as context_id, c.sName as context_name, c.bIsActive as context_value, 'control' as context_type, 5 as ord
                      FROM view_rm_control_active c
                      JOIN view_isms_user_active u ON (c.fkResponsible = u.fkContext) ";
		
		$maSpecialUsersIds = array_keys(ISMSLib::getSpecialUsers(RISK_MANAGEMENT_MODE));
    $msSpecialUsers = "SELECT CAST(sValue as INTEGER) as user_id, sName as user_name, pkConfig as context_id, '' as context_name, -1 as context_value, 'special_user' as context_type, 0 as ord
											FROM isms_config JOIN view_isms_user_active ON (sValue = fkContext)
											WHERE pkConfig IN ( " . implode(',', $maSpecialUsersIds) . ")";											
                      
    if($msControlType){
      $msControl .= " WHERE ".$msControlType;
    }
    
    $this->csQuery = $msSpecialUsers;
    
    foreach ($maResponsibleType as $msType) {
      switch ($msType) {
        case 'area':
          $this->csQuery .= ($this->csQuery ? ' UNION ' . $msArea : $msArea);  
        break;
        case 'process':
          $this->csQuery .= ($this->csQuery ? ' UNION ' . $msProcess : $msProcess);
        break;
        case 'asset':
          $this->csQuery .= ($this->csQuery ? ' UNION ' . $msAsset : $msAsset);
        break;
        case 'asset_security':
          $this->csQuery .= ($this->csQuery ? ' UNION ' . $msAssetSecurity : $msAssetSecurity);
        break;
        case 'control':
          $this->csQuery .= ($this->csQuery ? ' UNION ' . $msControl : $msControl);
        break;
      }
    }
    
    $this->csQuery .= " ORDER BY user_id, ord, context_value";
    
    return parent::executeQuery();
  }
  
    /**
   * Desenha o cabe�alho do relat�rio.
   *
   * <p>M�todo para desenhar o cabe�alho do relat�rio.</p>
   * @access public
   */
  public function drawHeader(){
    foreach($this->caHeaders as $moHeader){
      if($moHeader->getAttrName()=='header_filter'){
        $moHeaderFilter = $moHeader;
      }elseif($moHeader->getAttrName()=='header_filter_item'){
        $moHeaderFilterItem = $moHeader;
      }elseif($moHeader->getAttrName()=='report_header_blank'){
        $moHeaderBlank = $moHeader;
      }else{
        $this->coWriter->drawLine($moHeader,array());
      }
    }
    if ($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());
    
    $moFilterText = FWDWebLib::getObject('header_filter_text');
    $moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
    $maFilters = $this->coFilter->getSummary();
    foreach($maFilters as $maFilter){
      $moFilterText->setValue($maFilter['name']);
      $this->coWriter->drawLine($moHeaderFilter,array());
      foreach($maFilter['items'] as $msFilterItem){
        $moFilterItemText->setValue($msFilterItem);
        $this->coWriter->drawLine($moHeaderFilterItem,array());
      }
    }
    $this->coWriter->drawLine($moHeaderBlank,array());
  }
}
?>