<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportControlCostByCostCategory.
 *
 * <p>Classe que implementa o relatório de custos dos controles filtrados por
 * uma determinada categoria de custo.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportControlCostByCostCategory extends ISMSFilteredReport {
  
  public function __construct(){
    parent::__construct();
  }
  
 /**
  * Inicializa o relatório.
  *
  * <p>Método para inicializar o relatório.
  * Necessário para funcionar com o XML compilado.</p>
  * @access public
  */
  public function init(){
    parent::init();
    $this->coDataSet->addFWDDBField(new FWDDBField('c.fkContext','control_id'       ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('c.sName'    ,'control_name'     ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('c.bIsActive','control_is_active',DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField(''           ,'control_cost'     ,DB_NUMBER));
  }
  
 /**
  * Executa a query do relatório.
  *
  * <p>Método para executar a query do relatório.</p>
  * @access public
  */
  public function makeQuery(){
    $miCostCategory = $this->coFilter->getCostCategory();
    
    if(!preg_match('/^[1-5]$/',$miCostCategory)){
      trigger_error("Cost category must be an integer in the interval [1,5].",E_USER_ERROR);
    }
    
    $this->csQuery = "SELECT
                        c.fkContext AS control_id,
                        c.sName AS control_name,
                        c.bIsActive AS control_is_active,
                        cc.nCost{$miCostCategory} AS control_cost
                      FROM
                        view_rm_control_active c
                        JOIN rm_control_cost cc ON (cc.fkControl = c.fkContext)
                      ORDER BY control_name";
    return parent::executeQuery();
  }

}

?>