<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe ISMSReportRiskAmountByArea.
 *
 * <p>Classe que implementa o relat�rio de quantidade de riscos por �rea.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportRiskAmountByArea extends ISMSReport {
  
  /**
   * Inicializa o relat�rio.
   *
   * <p>M�todo para inicializar o relat�rio.
   * Necess�rio para funcionar com o XML compilado.</p>
   * @access public
   */  
  public function init() {
    parent::init();
    
    $this->coDataSet->addFWDDBField(new FWDDBField("","area_id",DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("","area_name",DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("","area_value",DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("","risk_count",DB_NUMBER));
  }
  
  /**
   * Executa a query do relat�rio.
   *
   * <p>M�todo para executar a query do relat�rio.</p>
   * @access public
   */
  public function makeQuery() {
  
    $moFilter = $this->getFilter();
    $maFilters = array();
    $msWhere ="";

    $maClassifType = $moFilter->getRiskClassifType();
    if(count($maClassifType)){
      if(isset($maClassifType['null'])){
        $maFilters[] = " r.fkType IS NULL ";
      }else{
        $maFilters[] = " r.fkType IN ( ".implode(',',array_keys($maClassifType)) ." ) ";
      }
    }
    $maAreaClassifType = $moFilter->getAreaClassifType();
    if(count($maAreaClassifType)){
      if(isset($maAreaClassifType['null'])){
        $maFilters[] = " ar.fkType IS NULL ";
      }else{
        $maFilters[] = " ar.fkType IN ( ".implode(',',array_keys($maAreaClassifType)) ." ) ";
      }
    }
    $maAreaClassifPriority = $moFilter->getAreaClassifPriority();
    if(count($maAreaClassifPriority)){
      if(isset($maAreaClassifPriority['null'])){
        $maFilters[] = " ar.fkPriority IS NULL ";
      }else{
        $maFilters[] = " ar.fkPriority IN ( ".implode(',',array_keys($maAreaClassifPriority)) ." ) ";
      }
    }

    if(count($maFilters)){
        $msWhere .= " WHERE ". implode(' AND ',$maFilters);
    }

    $this->csQuery = "SELECT ar.fkContext as area_id, ar.sName as area_name, ar.nValue as area_value, count(DISTINCT(r.fkContext)) as risk_count
                      FROM view_rm_area_active ar
                      JOIN view_rm_process_active p ON (ar.fkContext = p.fkArea)                      
                      JOIN view_rm_process_asset_active pa ON (p.fkContext = pa.fkProcess)
                      JOIN view_rm_asset_active a ON (pa.fkAsset = a.fkContext)                      
                      JOIN view_rm_risk_active r ON (a.fkContext = r.fkAsset)                      
                      $msWhere
                      GROUP BY ar.fkContext, ar.sName, ar.nValue
                      ORDER BY risk_count DESC";
    
    return parent::executeQuery();
  }
  
  /**
   * Desenha o cabe�alho do relat�rio.
   *
   * <p>M�todo para desenhar o cabe�alho do relat�rio.</p>
   * @access public
   */
  public function drawHeader(){
    foreach($this->caHeaders as $moHeader){
      if($moHeader->getAttrName()=='header_filter'){
        $moHeaderFilter = $moHeader;
      }elseif($moHeader->getAttrName()=='header_filter_item'){
        $moHeaderFilterItem = $moHeader;
      }elseif($moHeader->getAttrName()=='report_header_blank'){
        $moHeaderBlank = $moHeader;
      }else{
        $this->coWriter->drawLine($moHeader,array());
      }
    }
    if ($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());
    
    $moFilterText = FWDWebLib::getObject('header_filter_text');
    $moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
    $maFilters = $this->coFilter->getSummary();
    foreach($maFilters as $maFilter){
      $moFilterText->setValue($maFilter['name']);
      $this->coWriter->drawLine($moHeaderFilter,array());
      foreach($maFilter['items'] as $msFilterItem){
        $moFilterItemText->setValue($msFilterItem);
        $this->coWriter->drawLine($moHeaderFilterItem,array());
      }
    }
    $this->coWriter->drawLine($moHeaderBlank,array());
  }

}
?>