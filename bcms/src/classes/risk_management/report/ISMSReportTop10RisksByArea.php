<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe ISMSReportTop10RisksByArea.
 *
 * <p>Classe que implementa o relat�rio top 10 riscos por �rea.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportTop10RisksByArea extends ISMSReport {  
  
  /**
   * Inicializa o relat�rio.
   *
   * <p>M�todo para inicializar o relat�rio.
   * Necess�rio para funcionar com o XML compilado.</p>
   * @access public
   */  
  public function init() {
    parent::init();
    
    $this->coDataSet->addFWDDBField(new FWDDBField("","area_id",DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("","area_name",DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("","area_value",DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("","risk_id",DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("","risk_name",DB_STRING));      
    $this->coDataSet->addFWDDBField(new FWDDBField("","risk_impact",DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("","risk_value",DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("","risk_residual_value",DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("","risk_accept_mode",DB_NUMBER));
  }
  
  /**
   * Executa a query do relat�rio.
   *
   * <p>M�todo para executar a query do relat�rio.</p>
   * @access public
   */
  public function makeQuery() {
    /*
     * Nessa query n�o � necess�rio verificar os elementos deletados
     * pois eles j� s�o filtrados na fun��o get_top10_area_risks()
     */
     

    $moFilter = $this->getFilter();
    $maFilters = array();
    $msWhere ="";

    $maClassifType = $moFilter->getRiskClassifType();
    if(count($maClassifType)){
      if(isset($maClassifType['null'])){
        $maFilters[] = " r.fkType IS NULL ";
      }else{
        $maFilters[] = " r.fkType IN ( ".implode(',',array_keys($maClassifType)) ." ) ";
      }
    }

    $maClassifAreaType = $moFilter->getAreaClassifType();
    if(count($maClassifAreaType)){
      if(isset($maClassifAreaType['null'])){
        $maFilters[] = " ar.fkType IS NULL ";
      }else{
        $maFilters[] = " ar.fkType IN ( ".implode(',',array_keys($maClassifAreaType)) ." ) ";
      }
    }

    $maClassifAreaPriority = $moFilter->getAreaClassifPriority();
    if(count($maClassifAreaPriority)){
      if(isset($maClassifAreaPriority['null'])){
        $maFilters[] = " ar.fkPriority IS NULL ";
      }else{
        $maFilters[] = " ar.fkPriority IN ( ".implode(',',array_keys($maClassifAreaPriority)) ." ) ";
      }
    }

    if(count($maFilters)){
        $msWhere .= " WHERE ". implode(' AND ',$maFilters);
    }

    $this->csQuery = "SELECT ar.fkContext as area_id, ar.sName as area_name, ar.nValue as area_value,
                        r.fkContext as risk_id, r.sName as risk_name, r.tImpact as risk_impact, r.nValue as risk_value, r.nValueresidual as risk_residual_value, r.nAcceptMode as risk_accept_mode
                      FROM view_rm_area_active ar
                      JOIN " . FWDWebLib::getFunctionCall("get_top10_area_risks()") . " top10 ON (ar.fkContext = top10.area_id)
                      JOIN " . FWDWebLib::getFunctionCall("get_area_tree(0,0)") . " art ON (ar.fkContext = art.fkContext)
                      JOIN rm_risk r ON (top10.risk_id = r.fkContext)
                      $msWhere
                      ORDER BY art.pkId, r.nValueResidual DESC";
    return parent::executeQuery();
  }
  
  /**
   * Desenha o cabe�alho do relat�rio.
   *
   * <p>M�todo para desenhar o cabe�alho do relat�rio.</p>
   * @access public
   */
  public function drawHeader(){
    foreach($this->caHeaders as $moHeader){
      if($moHeader->getAttrName()=='header_filter'){
        $moHeaderFilter = $moHeader;
      }elseif($moHeader->getAttrName()=='header_filter_item'){
        $moHeaderFilterItem = $moHeader;
      }elseif($moHeader->getAttrName()=='report_header_blank'){
        $moHeaderBlank = $moHeader;
      }else{
        $this->coWriter->drawLine($moHeader,array());
      }
    }
    if ($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());
    
    $moFilterText = FWDWebLib::getObject('header_filter_text');
    $moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
    $maFilters = $this->coFilter->getSummary();
    foreach($maFilters as $maFilter){
      $moFilterText->setValue($maFilter['name']);
      $this->coWriter->drawLine($moHeaderFilter,array());
      foreach($maFilter['items'] as $msFilterItem){
        $moFilterItemText->setValue($msFilterItem);
        $this->coWriter->drawLine($moHeaderFilterItem,array());
      }
    }
    $this->coWriter->drawLine($moHeaderBlank,array());
  }  
}
?>