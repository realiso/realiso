<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

class ISMSReportProcessByAsset extends ISMSReport {

	public function init() {
		parent::init();
		
    
    $col1 = new FWDDBField("","place_name", DB_STRING);  
    $col1->setLabel(FWDLanguage::getPHPStringValue('rs_local','Local'));
    $col1->setShowColumn(true);
    
    $col2 = new FWDDBField("","process_name", DB_STRING);
    $col2->setLabel(FWDLanguage::getPHPStringValue('rs_process','Processo'));
    $col2->setShowColumn(true);
    $col3 = new FWDDBField("","process_description", DB_STRING);
    $col3->setLabel(FWDLanguage::getPHPStringValue('rs_description','Descri��o'));
    $col3->setShowColumn(true);
    $col4 = new FWDDBField("","process_rto", DB_NUMBER);
    $col4->setLabel(FWDLanguage::getPHPStringValue('rs_rto','RTO'));
    $col4->setShowColumn(true);
    $col5 = new FWDDBField("","process_rtoflag", DB_NUMBER);
    $col5->setLabel(' ');
    $col5->setShowColumn(false);
    $col6 = new FWDDBField("","process_ranking", DB_NUMBER);
    $col6->setLabel(' ');
    $col6->setShowColumn(false);
    $col7 = new FWDDBField("","asset_name", DB_STRING);
    $col7->setLabel(FWDLanguage::getPHPStringValue('rs_asset','Asset'));
    $col7->setShowColumn(true);
    $col8 = new FWDDBField("","asset_tac", DB_NUMBER);
    $col8->setLabel(FWDLanguage::getPHPStringValue('rs_tac_cl','TAC'));
    $col8->setShowColumn(true);
    $col9 = new FWDDBField("","asset_tra", DB_NUMBER);
    $col9->setLabel(FWDLanguage::getPHPStringValue('rs_tra_cl','TRA'));
    $col9->setShowColumn(true);
    
    $this->coDataSet->addFWDDBField($col1);
    $this->coDataSet->addFWDDBField($col2);
    $this->coDataSet->addFWDDBField($col3);
    $this->coDataSet->addFWDDBField($col4);
    $this->coDataSet->addFWDDBField($col5);
    $this->coDataSet->addFWDDBField($col6);
    $this->coDataSet->addFWDDBField($col7);
    $this->coDataSet->addFWDDBField($col8);
    $this->coDataSet->addFWDDBField($col9);
    
	}

	public function makeQuery() {
		$moFilter = $this->getFilter();
		$filters = array();

		$maClassifFilter = array();
		$placeType = $moFilter->getPlaceType();
		$join = '';
		if(count($placeType)){
			if(isset($placeType['null'])){
				$maClassifFilter[] = " plc.fkType IS NULL ";
			}else{
				$maClassifFilter[] = " plc.fkType IN ( ".implode(',',$placeType) ." ) ";
			}
		}
		$processType = $moFilter->getProcessType();
		if(count($processType)){
			if(isset($processType['null'])){
				$maClassifFilter[] = " p.fkType IS NULL ";
			}else{
				$maClassifFilter[] = " p.fkType IN ( ".implode(',',$processType) ." ) ";
			}
		}
		$processPriority = $moFilter->getProcessPriority();
		if(count($processPriority)){
			if(isset($processPriority['null'])){
				$maClassifFilter[] = " p.fkPriority IS NULL ";
			}else{
				$maClassifFilter[] = " p.fkPriority IN ( ".implode(',',$processPriority) ." ) ";
			}
		}
		$where = '';
		if(count($maClassifFilter)){
			$where .= " AND (". implode(' OR ',$maClassifFilter).") ";
		}

		if(count($filters)){
			$where = ' AND ( ' . implode(' AND ', $filters) ." ) ";
		}
	  
	  $this->csQuery = "
                      select 
                        plc.sname as place_name,
                        p.sname as process_name, 
                        p.tdescription as process_description, 
                        pr.rto as process_rto,
                        pr.rtoflag as process_rtoflag,
                        pr.ranking as process_ranking,
                        a.sname as asset_name,
                        a.ntac as asset_tac,
                        a.tra as asset_tra
                      from 
                        view_rm_process_active p
                        join view_rm_process_asset_active pa on (pa.fkprocess = p.fkcontext)
                        join view_rm_asset_active a on (a.fkcontext = pa.fkasset)
                        join view_process_ranking pr on (pr.fkprocess = p.fkcontext)
                        join cm_place_process pp on (pp.fkprocess = p.fkcontext)
												join view_cm_place_active plc on (plc.fkcontext = pp.fkplace) 
                      WHERE 1=1
                      
                      {$where}
                        
                      order by 
                      	pr.ranking asc 
                     ";

	  return parent::executeQuery();
	}

	public function drawHeader(){
		foreach($this->caHeaders as $moHeader){
			if($moHeader->getAttrName()=='header_filter'){
				$moHeaderFilter = $moHeader;
			}elseif($moHeader->getAttrName()=='header_filter_item'){
				$moHeaderFilterItem = $moHeader;
			}elseif($moHeader->getAttrName()=='report_header_blank'){
				$moHeaderBlank = $moHeader;
			}else{
				$this->coWriter->drawLine($moHeader,array());
			}
		}
		if ($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());

		$moFilterText = FWDWebLib::getObject('header_filter_text');
		$moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
		$filters = $this->coFilter->getSummary();
		foreach($filters as $maFilter){
			$moFilterText->setValue($maFilter['name']);
			$this->coWriter->drawLine($moHeaderFilter,array());
			foreach($maFilter['items'] as $msFilterItem){
				$moFilterItemText->setValue($msFilterItem);
				$this->coWriter->drawLine($moHeaderFilterItem,array());
			}
		}
		$this->coWriter->drawLine($moHeaderBlank,array());
	}
}
?>