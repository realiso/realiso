<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

class ISMSReportAssetMatrix extends ISMSReport {

	public function init() {
		parent::init();
		
    $col1 = new FWDDBField("","asset_name", DB_STRING);
    $col1->setLabel(FWDLanguage::getPHPStringValue('rs_asset', 'Ativo'));
    $col1->setShowColumn(true);
    
    $col2 = new FWDDBField("","asset_description", DB_STRING);
    $col2->setLabel(FWDLanguage::getPHPStringValue('rs_description','Descri��o'));
    $col2->setShowColumn(true);
    
    $col3 = new FWDDBField("","asset_tra", DB_NUMBER);
    $col3->setLabel(FWDLanguage::getPHPStringValue('rs_tra_cl', 'TRA'));
    $col3->setShowColumn(true);
    
    $col4 = new FWDDBField("","asset_tac", DB_NUMBER);
    $col4->setLabel(FWDLanguage::getPHPStringValue('rs_tac_cl', 'TAC'));
    $col4->setShowColumn(true);
    
    $col5 = new FWDDBField("","process_rto", DB_NUMBER);
    $col5->setLabel(FWDLanguage::getPHPStringValue('rs_rto','RTO'));
    $col5->setShowColumn(true);
    
    $col6 = new FWDDBField("","process_ranking", DB_NUMBER);
    $col6->setLabel(FWDLanguage::getPHPStringValue('rs_rank','Ranking'));
    $col6->setShowColumn(true);
    
    $col7 = new FWDDBField("","process_rtoflag", DB_NUMBER);
    $col7->setLabel(' ');
    $col7->setShowColumn(false);
    
    $this->coDataSet->addFWDDBField($col1);
    $this->coDataSet->addFWDDBField($col2);
    $this->coDataSet->addFWDDBField($col3);
    $this->coDataSet->addFWDDBField($col4);
    $this->coDataSet->addFWDDBField($col5);
    $this->coDataSet->addFWDDBField($col6);
    $this->coDataSet->addFWDDBField($col7);
    
	}

	public function makeQuery() {
		/*$moFilter = $this->getFilter();
		$filters = array();

		$maClassifFilter = array();
		$placeType = $moFilter->getPlaceType();
		$join = '';
		if(count($placeType)){
			if(isset($placeType['null'])){
				$maClassifFilter[] = " pl.fkType IS NULL ";
			}else{
				$maClassifFilter[] = " pl.fkType IN ( ".implode(',',$placeType) ." ) ";
			}
			$join .= '	JOIN cm_place_process pp on pp.fkprocess = p.fkcontext
						JOIN view_cm_place_active pl on pl.fkcontext = pp.fkplace ';
		}
		$processType = $moFilter->getProcessType();
		if(count($processType)){
			if(isset($processType['null'])){
				$maClassifFilter[] = " process_type IS NULL ";
			}else{
				$maClassifFilter[] = " process_type IN ( ".implode(',',$processType) ." ) ";
			}
		}
		$processPriority = $moFilter->getProcessPriority();
		if(count($processPriority)){
			if(isset($processPriority['null'])){
				$maClassifFilter[] = " process_priority IS NULL ";
			}else{
				$maClassifFilter[] = " process_priority IN ( ".implode(',',$processPriority) ." ) ";
			}
		}
		$where = '';
		if(count($maClassifFilter)){
			$where .= " AND (". implode(' OR ',$maClassifFilter).") ";
		}

		if(count($filters)){
			$where = ' AND ( ' . implode(' AND ', $filters) ." ) ";
		}*/
	  
    $this->csQuery = "
                     select 
                      distinct
                      	a.sname as asset_name, 
                      	a.tdescription as asset_description,
                      	a.tra as asset_tra,
                      	a.ntac as asset_tac,
                      	
                      	coalesce( (select pr.ranking from view_rm_process_asset_active pa 
                      join view_process_ranking pr on (pr.fkprocess = pa.fkprocess)
                      where pa.fkasset = a.fkcontext
                      order by pr.ranking desc limit 1), 0.00)  as process_ranking,
                      
                      	coalesce( (select pr.rto from view_rm_process_asset_active pa 
                      join view_process_ranking pr on (pr.fkprocess = pa.fkprocess)
                      where pa.fkasset = a.fkcontext
                      order by pr.ranking desc limit 1), 0)  as process_rto,
                      
                      	coalesce( (select pr.rtoflag from view_rm_process_asset_active pa 
                      join view_process_ranking pr on (pr.fkprocess = pa.fkprocess)
                      where pa.fkasset = a.fkcontext
                      order by pr.ranking desc limit 1), 0)  as process_rtoflag             
                      
                      from 
                      	view_rm_asset_active a

                      order by process_ranking desc
    								";
      //{$where}
	  
			return parent::executeQuery();
	}

	public function drawHeader(){
		foreach($this->caHeaders as $moHeader){
			if($moHeader->getAttrName()=='header_filter'){
				$moHeaderFilter = $moHeader;
			}elseif($moHeader->getAttrName()=='header_filter_item'){
				$moHeaderFilterItem = $moHeader;
			}elseif($moHeader->getAttrName()=='report_header_blank'){
				$moHeaderBlank = $moHeader;
			}else{
				$this->coWriter->drawLine($moHeader,array());
			}
		}
		if ($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());

		$moFilterText = FWDWebLib::getObject('header_filter_text');
		$moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
		//$filters = $this->coFilter->getSummary();
		//foreach($filters as $maFilter){
		//	$moFilterText->setValue($maFilter['name']);
		//	$this->coWriter->drawLine($moHeaderFilter,array());
		//	foreach($maFilter['items'] as $msFilterItem){
		//		$moFilterItemText->setValue($msFilterItem);
		//		$this->coWriter->drawLine($moHeaderFilterItem,array());
		//	}
		//}
		$this->coWriter->drawLine($moHeaderBlank,array());
	}
}
?>