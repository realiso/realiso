<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe ISMSReportAssetImportance.
 *
 * <p>Classe que implementa o relat�rio de import�ncia dos ativos.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportAssetImportance extends ISMSReport {
	
	/**
	 * Inicializa o relat�rio.
	 *
	 * <p>M�todo para inicializar o relat�rio.
	 * Necess�rio para funcionar com o XML compilado.</p>
	 * @access public
	 */	
	public function init() {
		parent::init();
		
		$this->coDataSet->addFWDDBField(new FWDDBField("","asset_id",DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("","asset_name",DB_STRING));		
		$this->coDataSet->addFWDDBField(new FWDDBField("","asset_value",DB_NUMBER));		
		$this->coDataSet->addFWDDBField(new FWDDBField("","asset_importance",DB_NUMBER));		
	}
	
	/**
	 * Executa a query do relat�rio.
	 *
	 * <p>M�todo para executar a query do relat�rio.</p>
	 * @access public
	 */
	public function makeQuery() {				
		$moFilter = $this->getFilter();		
		$msParameters = implode(',', $moFilter->getParameters());
		
		$this->csQuery = "SELECT 	a.fkContext as asset_id, a.sName as asset_name, a.nValue as asset_value, CASE WHEN sum(pvn.nValue) IS NULL THEN 0 ELSE sum(pvn.nValue) END as asset_importance
											FROM view_rm_asset_active a
                      JOIN rm_asset_value av ON (a.fkContext = av.fkAsset AND fkParameterName IN ($msParameters))
                      JOIN rm_parameter_value_name pvn ON (av.fkValueName = pvn.pkValueName)
                      GROUP BY a.fkContext, a.sName, a.nValue
                      ORDER BY asset_importance DESC";
											
		return parent::executeQuery();
	}
	
	/**
	 * Desenha o cabe�alho do relat�rio.
	 *
	 * <p>M�todo para desenhar o cabe�alho do relat�rio.</p>
	 * @access public
	 */
	public function drawHeader(){
    foreach($this->caHeaders as $moHeader){
      if($moHeader->getAttrName()=='header_filter'){
        $moHeaderFilter = $moHeader;
      }elseif($moHeader->getAttrName()=='header_filter_item'){
        $moHeaderFilterItem = $moHeader;
      }elseif($moHeader->getAttrName()=='report_header_blank'){
        $moHeaderBlank = $moHeader;
      }else{
        $this->coWriter->drawLine($moHeader,array());
      }
    }
    if ($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());
    
    $moFilterText = FWDWebLib::getObject('header_filter_text');
    $moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
    $maFilters = $this->coFilter->getSummary();
    foreach($maFilters as $maFilter){
      $moFilterText->setValue($maFilter['name']);
      $this->coWriter->drawLine($moHeaderFilter,array());
      foreach($maFilter['items'] as $msFilterItem){
        $moFilterItemText->setValue($msFilterItem);
        $this->coWriter->drawLine($moHeaderFilterItem,array());
      }
    }
    $this->coWriter->drawLine($moHeaderBlank,array());
  }
}
?>