<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

class ISMSReportASA extends ISMSReport {

	public function init() {
		parent::init();
		
		$col1 = new FWDDBField("place",				"place", 				DB_STRING);
		$col1->setLabel(FWDLanguage::getPHPStringValue('rs_localTitle','Local'));
		$col1->setShowColumn(true);
		
		$col2 = new FWDDBField("process",			"process", 				DB_STRING);
		$col2->setLabel(FWDLanguage::getPHPStringValue('rs_process','Processo'));
		$col2->setShowColumn(true);
		
		$col3 = new FWDDBField("rto",				"rto",					DB_STRING);
		$col3->setLabel(FWDLanguage::getPHPStringValue('rs_rto','RTO'));
		$col3->setShowColumn(true);
		
		$col4 = new FWDDBField("asset",				"asset", 				DB_STRING);
		$col4->setLabel(FWDLanguage::getPHPStringValue('rs_asset','Ativo'));
		$col4->setShowColumn(true);
		
		$col5 = new FWDDBField("tra",				"tra", 					DB_STRING);
		$col5->setLabel(FWDLanguage::getPHPStringValue('rs_tra','TRA'));
		$col5->setShowColumn(true);
		
		$col6 = new FWDDBField("tac",				"tac", 					DB_STRING);
		$col6->setLabel(FWDLanguage::getPHPStringValue('rs_tac','TAC'));
		$col6->setShowColumn(true);
		
		$col7 = new FWDDBField("isa",				"isa", 					DB_STRING);
		$col7->setLabel(FWDLanguage::getPHPStringValue('rs_isa','ISA'));
		$col7->setShowColumn(true);
		
		$col8 = new FWDDBField("plans",				"plans", 				DB_NUMBER);
		$col8->setLabel(' ');
		$col8->setShowColumn(false);
		
		$this->coDataSet->addFWDDBField($col1);
		$this->coDataSet->addFWDDBField($col2);
		$this->coDataSet->addFWDDBField($col3);
		$this->coDataSet->addFWDDBField($col4);
		$this->coDataSet->addFWDDBField($col5);
		$this->coDataSet->addFWDDBField($col6);
		$this->coDataSet->addFWDDBField($col7);
		$this->coDataSet->addFWDDBField($col8);
	}
	

	public function makeQuery() {
		$moFilter = $this->getFilter();
		$filters = array();
		$joins = array();
		$maClassifFilter = array();

		$placeType = $moFilter->getPlaceType();
		if(count($placeType)){
			if(isset($placeType['null'])){
				$maClassifFilter[] = " place.fkType IS NULL ";
			}else{
				$maClassifFilter[] = " place.fkType IN ( ".implode(',',$placeType) ." ) ";
			}
		}
		
		$processType = $moFilter->getProcessType();
		if(count($processType)){
			if(isset($processType['null'])){
				$maClassifFilter[] = " process.fkType IS NULL ";
			}else{
				$maClassifFilter[] = " process.fkType IN ( ".implode(',',$processType) ." ) ";
			}
		}
		
		$processPriority = $moFilter->getProcessPriority();
		if(count($processPriority)){
			if(isset($processPriority['null'])){
				$maClassifFilter[] = " process.fkPriority IS NULL ";
			}else{
				$maClassifFilter[] = " process.fkPriority IN ( ".implode(',',$processPriority) ." ) ";
			}
		}

		$place = $moFilter->getPlace();
		if($place){
			$filters[] = " place.fkcontext = {$place} ";
		}

		$area = $moFilter->getArea();
		if($area){
			$filters[] = " area.fkcontext = {$area} ";
			$joins[] = " join view_rm_area_active area on area.fkcontext = process.fkarea ";
		}

		$process = $moFilter->getProcess();
		if($process){
			$filters[] = " process.fkcontext = {$process} ";
		}

		$where = "";
		$join = "";
		if(count($filters)){
			$where = ' AND ( ' . implode(' AND ', $filters) ." ) ";
		}

		if(count($maClassifFilter)){
			$where .= " AND (". implode(' OR ',$maClassifFilter).") ";
		}

		if(count($joins)){
			$join = implode(' ', $joins);
		}

		$minutes = FWDLanguage::getPHPStringValue('ASAMinutes','minutos');
		$hours = FWDLanguage::getPHPStringValue('ASAHours','horas');
		$days = FWDLanguage::getPHPStringValue('ASADays','dias');

		$this->csQuery = "	select
								place.sname as place,
								asset.sname as asset,
								
								process.sname as process,
								nrecoverytime ||(CASE
							            WHEN traflag = 2 THEN ' {$hours}'
							            ELSE 
							            CASE
							            WHEN traflag = 3 THEN ' {$days}'
							            ELSE 
										' {$minutes}'
							            END
							        END) as tra,
								case when ntac <> 0 then ntac || (CASE
							            WHEN traflag = 2 THEN ' {$hours}'
							            ELSE 
							            CASE
							            WHEN traflag = 3 THEN ' {$days}'
							            ELSE 
										' {$minutes}'
							            END
							        END) end as tac,
								round(cast(isa.isa as numeric), 2) as isa,
								count(distinct plan.fkcontext) as plans,
								ranking.rto ||(CASE
							            WHEN ranking.rtoflag = 2 THEN ' {$hours}'
							            ELSE 
							            CASE
							            WHEN ranking.rtoflag = 3 THEN ' {$days}'
							            ELSE 
										' {$minutes}'
							            END
							        END) as rto								
							from 
								view_isa isa
								JOIN view_rm_asset_active asset on asset.fkcontext = isa.fkasset
								JOIN view_rm_process_active process on process.fkcontext = isa.fkprocess
								LEFT JOIN view_cm_place_active place on place.fkcontext = asset.fkplace
								left join view_cm_plan_active plan on plan.fkasset = asset.fkcontext
								left join view_process_ranking ranking on ranking.fkprocess = process.fkcontext
								{$join}
							WHERE 1=1
							{$where}
							GROUP BY
								asset,
								place,
								process,
								isa,
								nrecoverytime,
								traflag,
								ntac,
								rto,
								rtoflag,
								ranking								
							ORDER BY
								isa, ranking.ranking DESC, place, isa, process, asset"; 

							//print_r("<pre>{$this->csQuery}</pre>");
							return parent::executeQuery();
	}

	public function drawHeader(){
		foreach($this->caHeaders as $moHeader){
			if($moHeader->getAttrName()=='header_filter'){
				$moHeaderFilter = $moHeader;
			}elseif($moHeader->getAttrName()=='header_filter_item'){
				$moHeaderFilterItem = $moHeader;
			}elseif($moHeader->getAttrName()=='report_header_blank'){
				$moHeaderBlank = $moHeader;
			}else{
				$this->coWriter->drawLine($moHeader,array());
			}
		}
		if ($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());

		$moFilterText = FWDWebLib::getObject('header_filter_text');
		$moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
		$filters = $this->coFilter->getSummary();
		foreach($filters as $maFilter){
			$moFilterText->setValue($maFilter['name']);
			$this->coWriter->drawLine($moHeaderFilter,array());
			foreach($maFilter['items'] as $msFilterItem){
				$moFilterItemText->setValue($msFilterItem);
				$this->coWriter->drawLine($moHeaderFilterItem,array());
			}
		}
		$this->coWriter->drawLine($moHeaderBlank,array());
	}
}
?>