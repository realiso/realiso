<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

class ISMSReportAssetByProcess extends ISMSReport {

	public function init() {
		parent::init();
		
		
		$col1 = new FWDDBField("asset_place",				"asset_place", 					DB_STRING);
		$col1->setLabel(FWDLanguage::getPHPStringValue('gc_place','Local'));
		$col1->setShowColumn(true);
		
		$col2 = new FWDDBField("area",						"area", 						DB_STRING);
		$col2->setLabel(FWDLanguage::getPHPStringValue('ti_un','Unidade de Neg�cio'));
		$col2->setShowColumn(true);
		
		$col3 = new FWDDBField("asset_name",				"asset_name", 					DB_STRING);
		$col3->setLabel(FWDLanguage::getPHPStringValue('si_asset','Ativo'));
		$col3->setShowColumn(true);
		
		$col4 = new FWDDBField("process_name",				"process_name", 				DB_STRING);
		$col4->setLabel(FWDLanguage::getPHPStringValue('si_process','Processo'));
		$col4->setShowColumn(true);
		
		$col5 = new FWDDBField("importance",				"importance", 					DB_STRING);
		$col5->setLabel(FWDLanguage::getPHPStringValue('lb_importance_cl','Import�ncia'));
		$col5->setShowColumn(true);
		
		$col6 = new FWDDBField("process_ranking",			"process_ranking", 				DB_NUMBER);
		$col6->setLabel(FWDLanguage::getPHPStringValue('rs_ranking','Ranking'));
		$col6->setShowColumn(true);
		
		$col7 = new FWDDBField("process_rto",				"process_rto", 					DB_STRING);
		$col7->setLabel(FWDLanguage::getPHPStringValue('rs_rto_cl','RTO'));
		$col7->setShowColumn(true);
		
		$this->coDataSet->addFWDDBField($col1);
		$this->coDataSet->addFWDDBField($col2);
		$this->coDataSet->addFWDDBField($col3);
		$this->coDataSet->addFWDDBField($col4);
		$this->coDataSet->addFWDDBField($col5);
		$this->coDataSet->addFWDDBField($col6);
		$this->coDataSet->addFWDDBField($col7);
	
		
	}

	public function makeQuery() {
		
		$moFilter = $this->getFilter();
		$filters = array();
		$joins = array();
		$maClassifFilter = array();

		$placeType = $moFilter->getPlaceType();
		if(count($placeType)){
			if(isset($placeType['null'])){
				$maClassifFilter[] = " place.fkType IS NULL ";
			}else{
				$maClassifFilter[] = " place.fkType IN ( ".implode(',',$placeType) ." ) ";
			}
		}
		
		$processType = $moFilter->getProcessType();
		if(count($processType)){
			if(isset($processType['null'])){
				$maClassifFilter[] = " p.fkType IS NULL ";
			}else{
				$maClassifFilter[] = " p.fkType IN ( ".implode(',',$processType) ." ) ";
			}
		}
		
		$processPriority = $moFilter->getProcessPriority();
		if(count($processPriority)){
			if(isset($processPriority['null'])){
				$maClassifFilter[] = " p.fkPriority IS NULL ";
			}else{
				$maClassifFilter[] = " p.fkPriority IN ( ".implode(',',$processPriority) ." ) ";
			}
		}

		$place = $moFilter->getPlace();
		if($place){
			$filters[] = " place.fkcontext = {$place} ";
		}

		$area = $moFilter->getArea();
		if($area){
			$filters[] = " area.fkcontext = {$area} ";
		}

		$where = "";

		if(count($filters)){
			$where = ' AND ( ' . implode(' AND ', $filters) ." ) ";
		}

		if(count($maClassifFilter)){
			$where .= " AND (". implode(' OR ',$maClassifFilter).") ";
		}

		$minutes = FWDLanguage::getPHPStringValue('ASAMinutes','minutos');
		$hours = FWDLanguage::getPHPStringValue('ASAHours','horas');
		$days = FWDLanguage::getPHPStringValue('ASADays','dias');		
		
		$this->csQuery = "
			SELECT	place.sname as asset_place,
					area.sname as area,
					a.sname AS asset_name, 
					p.sname AS process_name, 
					coalesce(importance.sname,'1 - Low') as importance,
					pr.ranking AS process_ranking, 
					pr.rto ||	(CASE
						WHEN pr.rtoflag = 2 THEN ' {$hours}'
						ELSE 
						CASE
						WHEN pr.rtoflag = 3 THEN ' {$days}'
						ELSE 
						' {$minutes}'
						END
						END) AS process_rto
			FROM view_rm_process_asset_active pa
			JOIN view_rm_asset_active a ON pa.fkasset = a.fkcontext
			JOIN view_rm_process_active p ON pa.fkprocess = p.fkcontext
			JOIN view_process_ranking pr ON pr.fkprocess = p.fkcontext
			JOIN view_cm_place_active place ON place.fkcontext = a.fkplace
			JOIN view_rm_area_active area ON area.fkplace = a.fkplace
			LEFT JOIN cm_priority importance ON importance.fkcontext = pa.fkimportance
			WHERE 1=1
			{$where}
			ORDER BY asset_place, area, asset_name, importance.weight ASC
		";		
		
		//print_r("<pre>{$this->csQuery}</pre>");		
		return parent::executeQuery();
	}

	public function drawHeader(){
		foreach($this->caHeaders as $moHeader){
			if($moHeader->getAttrName()=='header_filter'){
				$moHeaderFilter = $moHeader;
			}elseif($moHeader->getAttrName()=='header_filter_item'){
				$moHeaderFilterItem = $moHeader;
			}elseif($moHeader->getAttrName()=='report_header_blank'){
				$moHeaderBlank = $moHeader;
			}else{
				$this->coWriter->drawLine($moHeader,array());
			}
		}
		if ($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());

		$moFilterText = FWDWebLib::getObject('header_filter_text');
		$moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
		$filters = $this->coFilter->getSummary();
		foreach($filters as $maFilter){
			$moFilterText->setValue($maFilter['name']);
			$this->coWriter->drawLine($moHeaderFilter,array());
			foreach($maFilter['items'] as $msFilterItem){
				$moFilterItemText->setValue($msFilterItem);
				$this->coWriter->drawLine($moHeaderFilterItem,array());
			}
		}
		$this->coWriter->drawLine($moHeaderBlank,array());
	}
}
?>