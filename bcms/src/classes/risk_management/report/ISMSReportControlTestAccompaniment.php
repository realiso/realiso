<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe ISMSReportControlTestAccompaniment.
 *
 * <p>Classe que implementa o relat�rio acompanhamento da efici�ncia dos controles.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportControlTestAccompaniment extends ISMSReport {
  
  /**
   * Inicializa o relat�rio.
   *
   * <p>M�todo para inicializar o relat�rio.
   * Necess�rio para funcionar com o XML compilado.</p>
   * @access public
   */  
  public function init() {
    parent::init();
    
    $this->coDataSet->addFWDDBField(new FWDDBField("","control_id"                    ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("","control_name"                  ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("","control_is_active"             ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("","control_history_date_todo"     ,DB_DATETIME));
    $this->coDataSet->addFWDDBField(new FWDDBField("","control_hist_date_accomplish"  ,DB_DATETIME));
    $this->coDataSet->addFWDDBField(new FWDDBField("","control_test"                  ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("","control_test_description"      ,DB_NUMBER));
  }
  
  /**
   * Executa a query do relat�rio.
   *
   * <p>M�todo para executar a query do relat�rio.</p>
   * @access public
   */
  public function makeQuery() {
    $moFilter = $this->getFilter();
    $maFilters = array();
    $msWhere ="";

    $maClassifType = $moFilter->getControlClassifType();
    
    if(count($maClassifType)){
      if(isset($maClassifType['null'])){
        $maFilters[] = " c.fkType IS NULL ";
      }else{
        $maFilters[] = " c.fkType IN ( ".implode(',',array_keys($maClassifType)) ." ) ";
      }
    }

    if(count($maFilters)){
        $msWhere .= " WHERE ". implode(' AND ',$maFilters);
    }

    $this->csQuery = "SELECT c.fkContext as control_id, c.sName as control_name, c.bIsActive as control_is_active,
                        cth.dDateTodo as control_history_date_todo, cth.dDateAccomplishment as control_hist_date_accomplish,
                        cth.bTestedValue as control_test, cth.tDescription as control_test_description
                      FROM view_rm_control_active c
                      JOIN rm_control_test_history cth ON (c.fkContext = cth.fkControl)
                      $msWhere
                      ORDER BY c.fkContext, cth.dDateTodo DESC";
    
    return parent::executeQuery();
  }
  
  
    /**
   * Desenha o cabe�alho do relat�rio.
   *
   * <p>M�todo para desenhar o cabe�alho do relat�rio.</p>
   * @access public
   */
  public function drawHeader(){
    foreach($this->caHeaders as $moHeader){
      if($moHeader->getAttrName()=='header_filter'){
        $moHeaderFilter = $moHeader;
      }elseif($moHeader->getAttrName()=='header_filter_item'){
        $moHeaderFilterItem = $moHeader;
      }elseif($moHeader->getAttrName()=='report_header_blank'){
        $moHeaderBlank = $moHeader;
      }else{
        $this->coWriter->drawLine($moHeader,array());
      }
    }
    if ($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());
    
    $moFilterText = FWDWebLib::getObject('header_filter_text');
    $moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
    $maFilters = $this->coFilter->getSummary();
    foreach($maFilters as $maFilter){
      $moFilterText->setValue($maFilter['name']);
      $this->coWriter->drawLine($moHeaderFilter,array());
      foreach($maFilter['items'] as $msFilterItem){
        $moFilterItemText->setValue($msFilterItem);
        $this->coWriter->drawLine($moHeaderFilterItem,array());
      }
    }
    $this->coWriter->drawLine($moHeaderBlank,array());
  }

}
?>