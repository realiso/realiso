<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportAssetRelevance.
 *
 * <p>Classe que implementa o relat�rio que mostra a relev�ncia dos ativos.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportAssetRelevance extends ISMSReport {
  
 /**
  * Inicializa o relat�rio.
  *
  * <p>M�todo para inicializar o relat�rio.
  * Necess�rio para funcionar com o XML compilado.</p>
  * @access public
  */
  public function init(){
    parent::init();
    $this->coDataSet->addFWDDBField(new FWDDBField('a.fkcontext',       'asset_id',           DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('a.sName',           'asset_name',         DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('a.nValue',          'asset_value',        DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('u.sName',           'asset_responsible',  DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('pn.sName',          'asset_parameter',    DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('vn.sImportance',    'asset_importance',   DB_STRING));
  }
  
 /**
  * Executa a query do relat�rio.
  *
  * <p>M�todo para executar a query do relat�rio.</p>
  * @access public
  */
  public function makeQuery(){    
    $this->csQuery = "SELECT
                      	a.fkContext as asset_id,
                      	a.sName as asset_name,
                      	a.nValue as asset_value,
                      	u.sName as asset_responsible,
                      	pn.sName as asset_parameter,
                      	vn.sImportance as asset_importance
                      FROM
                      	view_rm_asset_active a
                      	LEFT JOIN view_isms_user_active u ON (a.fkResponsible = u.fkContext)
                      	LEFT JOIN rm_asset_value av ON (a.fkContext = av.fkAsset)
                      	LEFT JOIN rm_parameter_name pn ON (av.fkParameterName = pn.pkParameterName)
                      	LEFT JOIN rm_parameter_value_name vn ON (av.fkValueName = vn.pkValueName)
                      ORDER BY
                      	a.sName, a.fkContext, pn.pkParametername";
    return parent::executeQuery();
  }
}
?>