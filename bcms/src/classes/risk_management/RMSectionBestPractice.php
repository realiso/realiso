<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

include_once $handlers_ref . "QuerySectionParents.php";
include_once $handlers_ref . "QuerySubSections.php";
include_once $handlers_ref . "QueryBestPracticesFromSection.php";
include_once $handlers_ref . "QueryControlBestPracticeFromSections.php";

/**
 * Classe RMSectionBestPractice.
 *
 * <p>Classe que representa a tabela de se��es de melhores pr�ticas.</p>
 * @package ISMS
 * @subpackage classes
 */
class RMSectionBestPractice extends ISMSContext implements IWorkflow {
  
	/**
  * Indica se a mensagem de erro ao tentar excluir � a mensagem que que existe uma categoria relacionada a um controle
  * @var boolean
  * @access protected
  */  
  protected $mbHasControlBestPracticeRelation = false;
 /**
  * Construtor.
  * 
  * <p>Construtor da classe RMSectionBestPractice.</p>
  * @access public 
  */
  public function __construct(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    parent::__construct("rm_section_best_practice");
    $this->ciContextType = CONTEXT_SECTION_BEST_PRACTICE;
    
    $this->csAliasId = "section_id";
    $this->csDependenceAliasId = "section_parent_id";
    $this->csSpecialDeleteSP = "delete_section";
    $this->coWorkflow = new RMWorkflow(ACT_SECTION_BEST_PRACTICE_APPROVAL);

    $this->coDataset->addFWDDBField(new FWDDBField("fkContext", "section_id",         DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("fkParent",  "section_parent_id",  DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("sName",     "section_name",       DB_STRING));

    $this->caSensitiveFields = array('section_name');
    $this->caSearchableFields = array('section_name');
  }
 
 /**
  * Retorna o id do usu�rio que deve aprovar o contexto.
  *
  * <p>M�todo para retornar id do usu�rio que deve aprovar o contexto.</p>
  * @access public
  * @return integer Id do aprovador
  */
  public function getApprover(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    return ISMSLib::getConfigById(USER_LIBRARIAN);
  }

 /**
  * Retorna o id do usu�rio respons�vel pelo contexto.
  *
  * <p>M�todo para retornar id do usu�rio respons�vel pelo contexto.</p>
  * @access public
  * @return integer Id do respons�vel
  */
  public function getResponsible(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    return $this->getCreator();
  }
 
 /**
  * Retorna o nome do contexto.
  * 
  * <p>M�todo para retornar o nome do contexto.</p>
  * @access public 
  * @return string Nome do contexto
  */
  public function getName() {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    return $this->getFieldValue('section_name');
  }
  
 /**
  * Retorna os pais de uma se��o.
  * 
  * <p>M�todo para retornar os pais de uma se��o.</p>
  * @access public 
  * @param integer $piSectionId Id da se��o
  * @param boolean $pbExecByTrash se o metodo est� sendo chamado pela lixeira
  * @return array Array de ids das se��es pais
  */
  /*
  public function getSection Parents($piSectionId,$pbExecByTrash = false) {
  /*/
  public function getSuperContexts($piSectionId,$pbExecByTrash = false){
  //*/
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $moQuery = new QuerySectionParents(FWDWebLib::getConnection(),$pbExecByTrash);
    $moQuery->setSection($piSectionId);
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    return $moQuery->getParentSections();
  }
  
 /**
  * Retorna as sub-se��es de uma se��o.
  * 
  * <p>M�todo para retornar as sub-se��es de uma se��o.</p>
  * @access public 
  * @param integer $piSectionId Id da se��o
  * @return array Array de ids das sub-se��es
  */ 
  public function getSubSections($piSectionId) {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $moQuery = new QuerySubSections(FWDWebLib::getConnection());
    $moQuery->setSection($piSectionId);
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    return $moQuery->getSubSections();
  }
  
 /**
  * Retorna as melhores pr�ticas de uma se��o (e de suas sub-se��es, se existirem).
  * 
  * <p>M�todo para retornar as melhores pr�ticas de uma se��o (e de suas sub-se��es,
  * se existirem).</p>
  * @access public 
  * @param integer $piSectionId Id da se��o
  * @return array Array de ids das melhores pr�ticas
  */ 
  public function getBestPracticesFromSection($piSectionId) {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $moQuery = new QueryBestPracticesFromSection(FWDWebLib::getConnection());
    $moQuery->setSection($piSectionId);
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    return $moQuery->getBestPractices();
  }
  
 /**
  * Retorna os sub-contextos de uma se��o (sub-se��es/melhores pr�ticas).
  * 
  * <p>M�todo para retornar os sub-contextos de uma se��o (sub-se��es/melhores pr�ticas).</p>
  * @access public 
  * @param integer $piSectionId Id da se��o
  * @return array Array de ids dos sub-contextos desta se��o
  */ 
  public function getSubContexts($piSectionId) {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    return array_merge($this->getSubSections($piSectionId),$this->getBestPracticesFromSection($piSectionId));
  }
  
 /**
  * Indica se o contexto � delet�vel ou n�o.
  * 
  * <p>M�todo que indica se o contexto � delet�vel ou n�o.</p>
  * @access public
  * @param integer $piContextId id do contexto
  */ 
  public function isDeletable($piContextId) {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    if (!ISMSLib::getConfigById(GENERAL_CASCADE_ON)) {
      // cascade off: verificar exist�ncia de subcontextos
      if ($this->getSubContexts($piContextId))
        return false;
    }
    $moQueryCtrlBP = new QueryControlBestPracticeFromSections(FWDWebLib::getConnection());
    $moQueryCtrlBP->setSectionBPId($piContextId);
    if(count($moQueryCtrlBP->getIds())){
    	$this->mbHasControlBestPracticeRelation = true;
    	return false;
    }else
    	return true;
  }
  
 /**
  * Exibe uma popup caso n�o seja poss�vel remover um contexto.
  * 
  * <p>M�todo que exibe uma popup caso n�o seja poss�vel remover um contexto.</p>
  * @access public
  * @param integer $piContextId id do contexto
  */ 
  public function showDeleteError($piContextId) {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $msTitle = FWDLanguage::getPHPStringValue('tt_section_remove_error','Erro ao remover Se��o');
    if($this->mbHasControlBestPracticeRelation)
    	$msMessage = FWDLanguage::getPHPStringValue('st_section_control_best_practice_remove_error_message',"N�o foi poss�vel remover a Se��o <b>%section_name%</b>: A Se��o cont�m Melhores Pr�ticas e/ou Sub-Se��es que cont�m Melhores Pr�ticas relacinadas a Controles. ");
    else
    	$msMessage = FWDLanguage::getPHPStringValue('st_section_remove_error_message',"N�o foi poss�vel remover a Se��o <b>%section_name%</b>: A Se��o cont�m Sub-Se��es e/ou Melhores Pr�ticas. Para remover Se��es que contenham Sub-Se��es ou Melhores Pr�ticas, ative a op��o \"Delete Cascade\" na aba \"Admin -> Configura��o\".");
    $moSection = new RMSectionBestPractice();
    $moSection->fetchById($piContextId);
    $msSectionName = $moSection->getFieldValue('section_name');
    $msMessage = str_replace("%section_name%",$msSectionName,$msMessage);
    ISMSLib::openOk($msTitle,$msMessage,"",80);
  }
  
 /**
  * Deleta logicamente um contexto.
  * 
  * <p>M�todo para deletar logicamente um contexto.</p>
  * @access public
  * @param integer $piContextId id do contexto
  */ 
  public function logicalDelete($piContextId) {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $maBestPractices = array_reverse($this->getBestPracticesFromSection($piContextId));
    $moBestPractice = new RMBestPractice();
    foreach ($maBestPractices as $miBestPracticeId) {
      $moBestPractice->delete($miBestPracticeId);
    }
    unset($moBestPractice);
    
    // remove logicamente todas as sub-se��es desta se��o
    $maSubSections = array_reverse($this->getSubSections($piContextId));
    $moSection = new RMSectionBestPractice();
    foreach ($maSubSections as $miSubSectionId) {
      $moSection->delete($miSubSectionId);
    }
    unset($moSection);
  }
  
 /**
  * Retorna o �cone do contexto.
  *
  * <p>M�todo para retornar o �cone do contexto.</p>
  * @access public
  * @param integer $piContextId Id do contexto
  * @return string Nome do �cone
  */
  public function getIcon($piContextId = 0){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    return 'icon-section.gif';
  }

 /**
  * Retorna o label do contexto.
  * 
  * <p>M�todo para retornar o label do contexto.</p>
  * @access public 
  * @return string Label do contexto
  */
  public function getLabel() {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    return FWDLanguage::getPHPStringValue('mx_best_practice_section', "Se��o de Melhor Pr�tica");
  }

 /**
  * Retorna o caminho do contexto para a cria��o do pathscroll.
  * 
  * <p>M�todo para retornar o caminho do contexto para a cria��o do pathscroll.</p>
  * @access public
  * @param integer $piTab id da tab
  * @param integer $piContextType id do contexto
  * @param string $psContextFunc fun��o que deve ser utilizada para gerar o c�digo do link  
  * @return array Array contendo o Caminho do contexto
  */ 
  public function getSystemPathScroll($piTab,$piContextType,$psContextFunc){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
 		$msHome = FWDLanguage::getPHPStringValue('st_home','Raiz');
  	$maPath = array();  	
    if($psContextFunc){
    	$maPath[]="<a href='javascript:{$psContextFunc}(0);'>";
    }else{
    	$maPath[]="<a href='javascript:isms_redirect_to_mode_scroll(" . $piTab . ",".$piContextType.",0)'>";
    }
   	$maPath[]=$msHome;
   	$maPath[]=ISMSLib::getIconCode('',-1);
   	$maPath[]="</a>";
    $miSessionId = $this->getFieldValue('section_id');
    
    /*
    $maSessionParents = array_reverse($this->getSection Parents($miSessionId));    
    /*/
    $maSessionParents = array_reverse($this->getSuperContexts($miSessionId));
    //*/
    foreach ($maSessionParents as $miSessionParentId) {
    	$moSessionParent = new RMSectionBestPractice();
    	$moSessionParent->fetchById($miSessionParentId);
    	if($psContextFunc)
    		$maPath[] = "<a href='javascript:{$psContextFunc}({$miSessionParentId});'>";
    	else
				$maPath[] = "<a href='javascript:isms_redirect_to_mode_scroll(" . $piTab . "," . $piContextType . ",$miSessionParentId)'>";
			$maPath[]=$moSessionParent->getName();
			$maPath[]=ISMSLib::getIconCode('icon-section.gif',-2);
   		$maPath[]="</a>";
    	unset($moSessionParent);	
    }
		if($psContextFunc)
    	$maPath[] = "<a href='javascript:{$psContextFunc}({$miSessionId});'>";
		else
    	$maPath[] = "<a href='javascript:isms_redirect_to_mode_scroll(" . $piTab . "," . $piContextType . ",$miSessionId)'>";
		$maPath[]=$this->getName();
		$maPath[]=ISMSLib::getIconCode('icon-section.gif',-2);
		$maPath[]="</a>";
    
  	$miIContPath = count($maPath);	  
    for($miI=$miIContPath-1;$miI>0;$miI--){
			if((isset($maPath[$miI-7]))&&($maPath[$miI-7]!=''))
				$maPath[$miI-3] = $maPath[$miI-7];
			$miI=$miI-3;
    }
    return $maPath;
  }
 /**
  * Retorna o caminho do contexto.
  * 
  * <p>M�todo para retornar o caminho do contexto.</p>
  * @access public 
  * @return string Caminho do contexto
  */ 
  public function getSystemPath() {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
  	$maPath = array();  	
    $maPath[] = "<a href='javascript:isms_redirect_to_mode(" . LIBRARIES_MODE . ",0,0)'>".FWDLanguage::getPHPStringValue('st_libraries_module_name','Bibliotecas')."</a>";
        
    $miSectionId = $this->getFieldValue('section_id');
    $maSectionParents = array_reverse($this->getSuperContexts($miSectionId));

    foreach ($maSectionParents as $miSectionParentId) {
    	$moSectionParent = new RMSectionBestPractice();
    	$moSectionParent->fetchById($miSectionParentId);
    	$maPath[] = ISMSLib::getIconCode('icon-section.gif',-2,-4) . "<a href='javascript:isms_redirect_to_mode(" . LIBRARIES_MODE . "," . $this->ciContextType . "," . $miSectionParentId . ")'>" . $moSectionParent->getName() . "</a>";
    	unset($moSectionParent);	
    }
    $maPath[] = ISMSLib::getIconCode('icon-section.gif',-2,-4) . "<a href='javascript:isms_redirect_to_mode(" . LIBRARIES_MODE . "," . $this->ciContextType . "," . $miSectionId . ")'>" . $this->getName() . "</a>";
    
    return implode("&nbsp;".ISMSLib::getIconCode('icon-arrow-right.gif',-2,0).'&nbsp;', $maPath);
  }
  

 /**
  * Retorna se o usu�rio logado tem permi��o para inserir o contexto.
  * 
  * <p>M�todo para retornar se o usu�rio tem permi��o para inserir o contexto no sistema.</p>
  * @access public
  * @return boolean Se o usu�ro tem permi��o para inserir o contexto no sistema
  */
  protected function userCanInsert(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $miContextType = $this->ciContextType;
    //contexto deve ser tratado, assim assume-se que a principio o usu�rio n�o pode inserir tal contexto
    $mbReturn = false;
    $miCount = 0;

    $maPermissions = array( 'M.L.2.7' );
    //acls negadas o usu�rio logado no sistema
    $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
    foreach($maACLs as $miKey => $miValue){
      $maACLs[$miValue]=$miKey;
    }
    //acl1 AND acl2
    $mbAuxPermission = true;
    foreach($maPermissions as $msValue){
      if(isset($maACLs[$msValue])){
        $mbAuxPermission = false;
      }
      $miCount++;
    }
    //caso exista pelo menos 1 acl nas permi��es do contexto e estas permi��es n�o est�o nas permi��es negadas do usu�rio
    if(($mbAuxPermission)&&($miCount)){
      $mbReturn = $mbAuxPermission;
    }
    return $mbReturn;
  }

 /**
  * Retorna se o usu�rio logado tem permi��o para editar o contexto.
  * 
  * <p>M�todo para retornar se o usu�rio tem permi��o para editar o contexto no sistema.</p>
  * @access protected
  * @return boolean Se o usu�ro tem permi��o de editar o contexto no sistema
  */
  public function userCanEdit($piContextId,$piUserResponsible = 0){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    if(!$this->contextIsOfType($piContextId,$this->ciContextType)) return false;
    //contexto deve ser tratado, assim assume-se que a principio o usu�rio n�o pode editar esse contexto
    $mbReturn = false;
    $maPermissions = array( 'M.L.2.2' );
    //acls negadas do usu�rio logado no sistema
    $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
    foreach($maACLs as $miKey => $miValue){
      $maACLs[$miValue]=$miKey;
    }
    $miCount = 0;
    //acl1 AND acl2
    $mbAuxPermission = true;
    foreach($maPermissions as $msValue){
      if(isset($maACLs[$msValue])){
        $mbAuxPermission = false;
      }
      $miCount++;
    }
    //caso exista pelo menos 1 acl nas permi��es do contexto e estas permi��es n�o est�o nas permi��es negadas do usu�rio
    if(($mbAuxPermission)&&($miCount)){
      $mbReturn = $mbAuxPermission;
      }
    return $mbReturn;
  }
 
 /**
  * Retorna se o usu�rio logado tem permi��o para excluir o contexto.
  * 
  * <p>M�todo para retornar se o usu�rio tem permi��o para excluir o contexto no sistema.</p>
  * @access protected
  * @return boolean Se o usu�ro tem permi��o de excluir o contexto no sistema
  */
  protected function userCanDelete($piContextId){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    //tester esquemas de respons�vel aq nesse teste
    $miContextType = $this->ciContextType;
    if(!$this->contextIsOfType($piContextId,$miContextType)) return false;
    //contexto deve ser tratado, assim assume-se que a principio o usu�rio n�o pode inserir tal contexto
    $mbReturn = false;
    $maPermissions = array( 'M.L.2.3' );

    //acls negadas do usu�rio logado no sistema
    $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
    foreach($maACLs as $miKey => $miValue){
      $maACLs[$miValue]=$miKey;
    }
      $miCount = 0;
    //acl1 AND acl2
    $mbAuxPermission = true;
    foreach($maPermissions as $msValue){
      if(isset($maACLs[$msValue])){
        $mbAuxPermission = false;
      }
      $miCount++;
    }
    //caso exista pelo menos 1 acl nas permi��es do contexto e estas permi��es n�o est�o nas permi��es negadas do usu�rio
    if(($mbAuxPermission)&&($miCount)){
      $mbReturn = $mbAuxPermission;
    }
    return $mbReturn;
  }

  /**
  * Retorna o c�digo HTML do icone do contexto.
  * 
  * <p>M�todo para retornar o c�digo HTML do contexto</p>
  * @access public
  * @param integer $piId Id do contexto
  * @param integer $pbIsFetched id do contexto
  * @return string contendo o c�digo HTML do icone do contexto
  */ 
  public function getIconCode($piId, $pbIsFetched = false){
  	return ISMSLib::getIconCode('icon-section.gif', -2, 7);
  }

 /**
  * Retorna o caminho para abrir a popup de edi��o desse contexto.
  * 
  * <p>M�todo para retornar o caminho para abrir a popup de edi��o desse contexto.</p>
  * @param integer $piContextId id do contexto
  * @access public 
  * @return string caminho para abrir a popup de edi��o desse contexto
  */ 
  public function getVisualizeEditEvent($piContextId,$psUniqId){
    $soSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    $msPackagesLibrariesPath = ($soSession->getMode()==MANAGER_MODE) ? "packages/libraries/" : "../../packages/libraries/";
    $msPopupId = "popup_section_edit";
    
    return "isms_open_popup('{$msPopupId}','{$msPackagesLibrariesPath}{$msPopupId}.php?section=$piContextId','','true',112,350);"
           ."soPopUpManager.getPopUpById('{$msPopupId}').setCloseEvent( function close_visualize_$psUniqId() {soPopUpManager.closePopUp('popup_visualize_$psUniqId');} );";
  }

}
?>