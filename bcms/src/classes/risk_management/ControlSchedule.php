<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

include_once $handlers_ref . "QueryControlImplementationAlert.php";
include_once $handlers_ref . "QueryControlImplementationExpired.php";
include_once $handlers_ref . "QueryUpdateControlsIsActive.php";

/**
 * Classe ControlSchedule.
 *
 * <p>Classe que manipula a revis�o (schedule) dos constroles.</p>
 * @package ISMS
 * @subpackage classes
 */
class ControlSchedule extends ISMSTable {  
 
 private $ciUserId = 0;
 
 /**
  * Construtor.
  * 
  * <p>Construtor da classe ControlSchedule.</p>
  * @access public 
  */
  public function __construct($piUserId){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $this->ciUserId = $piUserId;
  }
  
 /**
  * Envia os alertas referentes � implementa��o.
  * 
  * <p>Verifica as datas de implementa��o e deadline e envia os alertas, se for
  * necess�rio.</p>
  * @access public
  */
  public function checkImplementation(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    //verifica alertas de implementa��o do controle
    $this->checkAlerts( new QueryControlImplementationAlert(FWDWebLib::getConnection(),$this->ciUserId) ,WKF_ALERT_IMPLEMENTATION_ALERT);
    
    //verifica alertas de implementa��o atrazada do controle
    $this->checkAlerts( new QueryControlImplementationExpired(FWDWebLib::getConnection(),$this->ciUserId) ,WKF_ALERT_IMPLEMENTATION_EXPIRED);
    
    return true;
  }
  
  /**
  * Dispara Alertas de efici�ncia real, alertas de implementa��o e alertas de implementa��o atrazada 
  * 
  * <p>M�todo que dispara Alertas de efici�ncia real, alertas de implementa��o e 
  * alertas de implementa��o atrazada 
  * @access private 
  * @param FWDQueryHandler $poQuery Identificador do controle
  * @param Integer $piStatus tipo do alerta que deve ser gerado
  */
  private function checkAlerts($poQuery,$piStatus){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $poQuery->makeQuery();
    $moConfig = new ISMSConfig();
    $miTimestamp = ISMSLib::ISMSTime();
    if($poQuery->executeQuery()){
      $moDataset = $poQuery->getDataset();
      while($moDataset->fetch()){
          $mbSendAlert = false;          
          switch($piStatus){
            case WKF_ALERT_IMPLEMENTATION_ALERT:
                //teste das datas que estavam na consulta
                //c.dDateDeadline <= GETDATE() + c.nDaysBefore + 1 AND
               $miDateImplementation = ISMSLib::getTimestamp($moDataset->getFieldByAlias('date_implementation')->getValue());
               if( $miDateImplementation < ($miTimestamp + (($moDataset->getFieldByAlias('days_before')->getValue() + 1) * 86400))){
                  //update do campo de envio do alerta de implementa��o do controle
                  $moObj = new RMControl();
                  $moObj->setFieldValue('control_impl_alert',true);
                  
                  // o fecthById retorna 0 quando um campo � null, ignora este campo para este update
                  $moObj->getFieldByAlias('control_type')->setUpdatable(false);
                  
                  $moObj->update($moDataset->getFieldByAlias('id')->getValue(),false);
                  $mbSendAlert = true;
               }
              break;
            case WKF_ALERT_IMPLEMENTATION_EXPIRED:
                $miDateImplementation = ISMSLib::getTimestamp($moDataset->getFieldByAlias('date_implementation')->getValue());
                if((($miDateImplementation + 86400) < $miTimestamp))
                {
                  //update do compo de envio de alerta de expira��o da implementa��o do controle
                  $moObj = new RMControl();
                  $moObj->setFieldValue('control_impl_expired',true);
                  
                  // o fecthById retorna 0 quando um campo � null, ignora este campo para este update
                  $moObj->getFieldByAlias('control_type')->setUpdatable(false);
                  
                  //O update abaixo deve ser feito com o segundo par�metro em false, pois n�o deve gravar no log que o usu�rio editou o controle, visto que o usu�rio n�o sabe o que est� acontecendo com o controle.
                  $moObj->update($moDataset->getFieldByAlias('id')->getValue(),false);
                  $mbSendAlert = true;
                }  
              break;
            default:
              break;  
          }
          if($mbSendAlert==true){
            $moAlert = new WKFAlert();
            $moAlert->setFieldValue('alert_receiver_id',$this->ciUserId);        
            $moAlert->setFieldValue('alert_date',ISMSLib::ISMSTime());
            $moAlert->setFieldValue('alert_type',$piStatus);
            $moAlert->setFieldValue('alert_justification',' ');
            $moAlert->setFieldValue('alert_creator_id',$moConfig->getConfig(USER_CHAIRMAN));
            $moAlert->setFieldValue('alert_context_id',$moDataset->getFieldByAlias('id')->getValue());
            $moAlert->insert();
          }

        }
      }
  }

 /**
  * For�a um update do isActive dos controles com implementa��o ou tarefas
  * pendentes.
  * 
  * <p>For�a um update do isActive dos controles com implementa��o ou tarefas
  * pendentes.</p>
  * @access public
  */
  public function updateControlsIsActive(){
    QueryUpdateControlsIsActive::updateControls();
  }

 /**
  * Cria n�o-conformidades a partir das sementes.
  * 
  * <p>Cria n�o-conformidades de controles inativos a partir das sementes
  * criadas pela trigger da tabela de controles.</p>
  * @access public
  */
  public function checkNonConformities(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $moNonConformitySeed = new CINonConformitySeed();
    $moNonConformitySeed->select();
    $miChairmanId = ISMSLib::getConfigById(USER_CHAIRMAN);
    while($moNonConformitySeed->fetch()){
      $moNonConformitySeed->createNonConformity();
    }
  }
  
}
?>