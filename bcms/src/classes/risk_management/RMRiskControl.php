<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

include_once $handlers_ref . "QueryControlsByRisk.php";

/**
 * Classe RMRiskControl.
 *
 * <p>Classe que representa a tabela de associa��es risco-controle.</p>
 * @package ISMS
 * @subpackage classes
 */
class RMRiskControl extends ISMSContext implements IWorkflow, IAssociation {
  
  protected $cbAssociateControlToRisk = false;
  
 /**
  * Construtor.
  *
  * <p>Construtor da classe RMRiskControl.</p>
  * @access public
  */
  public function __construct(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    parent::__construct('rm_risk_control');
    $this->ciContextType = CONTEXT_RISK_CONTROL;
    
    $this->csAliasId = "rc_id";
    $this->coWorkflow = new WorkflowRiskControl(ACT_RISK_CONTROL_APPROVAL);

    $this->coDataset->addFWDDBField(new FWDDBField('fkContext',               'rc_id',            DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('fkProbabilityValueName',  'rc_probability',   DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('fkRisk',                  'rc_risk_id',       DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('fkControl',               'rc_control_id',    DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('tJustification',          'rc_justification', DB_STRING));
    
    $this->caSearchableFields = array('rc_justification');
  }


 /**
  * Seta o id do usu�rio que deve aprovar o contexto.
  * 
  * <p>M�todo para setar id do usu�rio que deve aprovar o contexto.</p>
  * @access public 
  * @param integer $piApproverId Id do aprovador
  */
  public function setApprover($piApproverId) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $this->ciApproverId = $piApproverId;
  }

 /**
  * Retorna o id do usu�rio que deve aprovar o contexto.
  *
  * <p>M�todo para retornar id do usu�rio que deve aprovar o contexto.</p>
  * @access public
  * @return integer Id do aprovador
  */
  public function getApprover(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    //retorna usu�rios diferentes dependendo do atributo AssociateControlToRisk
    //modificado para arrumar o bug #0001109: ISMS - Saas - Envia task para usu�rio errado.
    //assim a task de aprova��o ir� para o usu�rio correto
    $miApproverId = 0;
    if(isset($this->ciApproverId) && $this->ciApproverId){
      $miApproverId = $this->ciApproverId;
    }else{
      if($this->cbAssociateControlToRisk){
        $moControl = new RMControl();
        $moControl->fetchById($this->getFieldValue('rc_control_id'));
        $miApproverId = $moControl->getApprover();
      }else{
        $moRisk = new RMRisk();
        $moRisk->fetchById($this->getFieldValue('rc_risk_id'));
        $moAsset = new RMAsset();
        $moAsset->fetchById($moRisk->getFieldValue('risk_asset_id'));
        $miApproverId = $moAsset->getFieldValue('asset_security_responsible_id');
      }
      
    }
      $moUser = new ISMSUser();
      $moUser->fetchById($miApproverId);
    return $miApproverId;
  }


 /**
  * Seta se a rela��o risco controle est� sendo inserida/editada pelo lado do risco
  *
  * <p>M�todo para setar se a rela��o risco controle est� sendo inserida/editada pelo lado do risco.</p>
  * @access public
  * @param Bolean indica se a rela��o risco/controle est� sendo editada pelo lado do risco
  */
  public function setModifyByControlSide($pbByRiskSide){
    $this->cbAssociateControlToRisk = $pbByRiskSide;
  }

  /**
  * Retorna o id do usu�rio respons�vel pelo contexto.
  *
  * <p>M�todo para retornar id do usu�rio respons�vel pelo contexto.</p>
  * @access public
  * @return integer Id do respons�vel
  */
  public function getResponsible(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return $this->getCreator();
  }

 /**
  * Retorna o nome do contexto.
  * 
  * <p>M�todo para retornar o nome do contexto.</p>
  * @access public 
  * @return string Nome do contexto
  */
  public function getName() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $moRisk = new RMRisk();
    $moRisk->fetchById($this->getFieldValue('rc_risk_id'));    
    $moControl = new RMControl();
    $moControl->fetchById($this->getFieldValue('rc_control_id'));
    return $moControl->getName() . ' -> ' . $moRisk->getName();
  }
  
 /**
  * Retorna o label do contexto.
  * 
  * <p>M�todo para retornar o label do contexto.</p>
  * @access public 
  * @return string Label do contexto
  */
  public function getLabel() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return FWDLanguage::getPHPStringValue('mx_risk_control_association', "Associa��o de Controle a Risco");
  }

 /**
  * Retorna o identificador do primeiro contexto da rela��o.
  * 
  * <p>M�todo para retornar o identificador do primeiro contexto da rela��o.</p>
  * @access public 
  * @return string Alias
  */
  public function getFirstContextId() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return $this->getFieldValue('rc_control_id');
  }
 
 /**
  * Retorna o identificador do segundo contexto da rela��o.
  * 
  * <p>M�todo para retornar o identificador do segundo contexto da rela��o.</p>
  * @access public 
  * @return string Alias
  */
  public function getSecondContextId() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return $this->getFieldValue('rc_risk_id');
  }
  
 /**
  * Retorna os controles de um risco.
  * 
  * <p>M�todo para retornar os controles de um risco.</p>
  * @access public
  * @param integer $piRiskId Id do risco
  * @return array Ids dos controles
  */ 
  public function getControlsFromRisk($piRiskId) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
  	$moQuery = new QueryControlsByRisk(FWDWebLib::getConnection());
    $moQuery->setRisk($piRiskId);
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    return $moQuery->getControls();
  }
  
 /**
  * Retorna se o usu�rio logado tem permi��o para inserir o contexto.
  * 
  * <p>M�todo para retornar se o usu�rio tem permi��o para inserir o contexto no sistema.</p>
  * @access public
  * @return boolean Se o usu�ro tem permi��o para inserir o contexto no sistema
  */
  protected function userCanInsert(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    //contexto deve ser tratado, assim assume-se que a principio o usu�rio n�o pode inserir tal contexto
    $mbReturn = false;
    if($this->getFieldValue('rc_risk_id')){
      //verifica se tem permiss�o pelo lado do risco
      $moRisk = new RMRisk();
      $moRisk->fetchById($this->getFieldValue('rc_risk_id'));
      if((ISMSLib::getCurrentUserId() == $moRisk->getResponsibleToACLPermition()) || (ISMSLib::getCurrentUserId() == $moRisk->getSecondResponsibleToACLPermition())){
        $mbReturn = true;
      }
    }
    
    if($this->getFieldValue('rc_control_id')){
      //verifica se tem permiss�o pelo lado do controle
      if(ISMSLib::userHasACL('M.RM.5.3')){
        $mbReturn = true;
      }
    }
    
    if(!$this->getFieldValue('rc_risk_id') && !$this->getFieldValue('rc_control_id')){
      //teste de permiss�o com o objeto ainda sem dados, ser� permitido, pois ao abrir uma tela para inserir a rela��o,
      //ainda n�o existe dados suficientes para testar se pode-se ou n�o criar esta rela��o
      $mbReturn = true;
    }
    
    return $mbReturn;
  }

 /**
  * Retorna se o usu�rio logado tem permi��o para editar o contexto.
  * 
  * <p>M�todo para retornar se o usu�rio tem permi��o para editar o contexto no sistema.</p>
  * @access protected
  * @return boolean Se o usu�ro tem permi��o de editar o contexto no sistema
  */
  protected function userCanEdit($piContextId,$piUserResponsible = 0){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    //contexto deve ser tratado, assim assume-se que a principio o usu�rio n�o pode editar esse contexto
    $mbReturn = false;
    $moObjContext = new ISMSContextObject();
    $moObj = $moObjContext->getContextObjectByContextId($piContextId);
    if($moObj){
      if($moObj->fetchById($piContextId)){
        switch($moObj->getContextType()){
          case CONTEXT_RISK_CONTROL:
            if(ISMSLib::userHasACL('M.RM.5.3')){
              //tem permiss�o de editar a assicia��o pelo lado do controle
              $mbReturn = true;
            }else{
              //tem permiss�o pra editar a associa��o pelo lado do risco (user=asset_security_responsible_id)
              $moCtxAux = new RMRiskControl();
              $moCtxAux->fetchById($piContextId);
              $moRisk = new RMRisk();
              $moRisk->fetchById($moCtxAux->getFieldValue('rc_risk_id'));
              if((ISMSLib::getCurrentUserId() == $moRisk->getResponsibleToACLPermition()) || (ISMSLib::getCurrentUserId() == $moRisk->getSecondResponsibleToACLPermition())){
                $mbReturn = true;
              }
            }
            break;
          case CONTEXT_CONTROL: 
            if(ISMSLib::userHasACL('M.RM.5.3')){
              //tem permiss�o de editar a assicia��o pelo lado do controle
              $mbReturn = true;
            }
          break;
          case CONTEXT_RISK:
            if(!$piUserResponsible){
              $piUserResponsible = $moObj->getResponsibleToACLPermition($piContextId);              
            }
            $miSecondUserResponsible = $moObj->getSecondResponsibleToACLPermition($piContextId);
            if((ISMSLib::getCurrentUserId() == $piUserResponsible) || (ISMSLib::getCurrentUserId() == $miSecondUserResponsible)){
              $mbReturn = true;
            }
          break;
        }
      }
    }
    return $mbReturn;
  }
 
 /**
  * Retorna se o usu�rio logado tem permi��o para excluir o contexto.
  * 
  * <p>M�todo para retornar se o usu�rio tem permi��o para excluir o contexto no sistema.</p>
  * @access protected
  * @return boolean Se o usu�ro tem permi��o de excluir o contexto no sistema
  */
  protected function userCanDelete($piContextId){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    //tester esquemas de respons�vel aq nesse teste
    $miContextType = $this->ciContextType;
    if(!$this->contextIsOfType($piContextId,$miContextType)) return false;
    //contexto deve ser tratado, assim assume-se que a principio o usu�rio n�o pode inserir tal contexto
    $mbReturn = false;
    
    if(ISMSLib::userHasACL('M.RM.5.3')){
      //tem permiss�o de editar a assicia��o pelo lado do controle
      $mbReturn = true;
    }else{
      //tem permiss�o pra editar a associa��o pelo lado do risco
      $moCtxAux = new RMRiskControl();
      $moCtxAux->fetchById($piContextId);
      $moRisk = new RMRisk();
      $moRisk->fetchById($moCtxAux->getFieldValue('rc_risk_id'));
      if((ISMSLib::getCurrentUserId() == $moRisk->getResponsibleToACLPermition()) || (ISMSLib::getCurrentUserId() == $moRisk->getSecondResponsibleToACLPermition())){
        $mbReturn = true;
      }
    }
    return $mbReturn;
  }
  
}
?>