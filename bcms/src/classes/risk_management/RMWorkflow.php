<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe RMWorkflow.
 *
 * <p>Classe que manipula os estados do workflow.
 * O workflow � respons�vel por disparar as devidas tarefas.
 * � ele que sabe quais e quantas tarefas ser�o disparadas, de acordo
 * com o estado do contexto utilizado.</p>
 * @package ISMS
 * @subpackage classes
 */
class RMWorkflow extends Workflow {

 /**
  * Avan�a o estado do contexto no workflow.
  * 
  * <p>M�todo para avan�ar o estado do contexto no workflow.</p>
  * @access public 
  * 
  * @param IWorkflow $poContext Objeto de contexto
  * @param integer $piAction Tipo da a��o
  * @param string $psJustification Justificativa
  */
  public function stateForward(IWorkflow $poContext, $piAction, $psJustification = "") {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    $miApproverId = $poContext->getApprover();
    $miResponsibleId = $poContext->getResponsible();
    $mbUserEqualApprover = ($miUserId == $miApproverId);
    $mbUserEqualResponsible = ($miUserId == $miResponsibleId);
    $mbDeletedResponsible = ($miResponsibleId == 0);

    switch($poContext->getContextState()){
      case CONTEXT_STATE_NONE:
        if($mbUserEqualApprover){
          $poContext->setContextState(CONTEXT_STATE_APPROVED);
        }else{
          $this->dispatchTask($this->ciActivity, $poContext->getId(), $miApproverId);
          $poContext->setContextState(CONTEXT_STATE_PENDANT);
        }
        if(!$mbUserEqualResponsible && !$mbDeletedResponsible) $this->dispatchAlert(WKF_ALERT_DELEGATION, $poContext);
      break;

      case CONTEXT_STATE_PENDANT:
        switch($piAction){
          case WKF_ACTION_EDIT:
            // Se editar um contexto no estado pendente, n�o precisa fazer nada!
            // E se mudar o respons�vel na edi��o! N�o deveria enviar as tarefas e alertas novamente?
          break;

          case WKF_ACTION_APPROVE:
            if(!$mbDeletedResponsible && !$mbUserEqualResponsible) $this->dispatchAlert(WKF_ALERT_APPROVED, $poContext, $psJustification);
            $poContext->setContextState(CONTEXT_STATE_APPROVED);
            // quem criou/editou nao deveria ser notificado tambem?
          break;

          case WKF_ACTION_DENY :
            if(!$mbDeletedResponsible && !$mbUserEqualResponsible) $this->dispatchAlert(WKF_ALERT_DENIED, $poContext, $psJustification);
            $poContext->setContextState(CONTEXT_STATE_DENIED);
            // quem criou/editou nao deveria ser notificado tambem?
          break;

          case WKF_ACTION_DELETE:
            $poContext->setContextState(CONTEXT_STATE_DELETED);
          break;
        }
      break;

      case CONTEXT_STATE_APPROVED:
        switch($piAction){
          case WKF_ACTION_DELETE:
            $poContext->setContextState(CONTEXT_STATE_DELETED);
          break;

          case WKF_ACTION_EDIT:
            if(!$mbUserEqualApprover){
              $poContext->setContextState(CONTEXT_STATE_PENDANT);
              $this->dispatchTask($this->ciActivity, $poContext->getId(), $miApproverId);
            }
          break;
        }
      break;

      case CONTEXT_STATE_DENIED:
        switch($piAction){
          case WKF_ACTION_DELETE:
            $poContext->setContextState(CONTEXT_STATE_DELETED);
          break;

          case WKF_ACTION_EDIT:
            if($mbUserEqualApprover){
              $poContext->setContextState(CONTEXT_STATE_APPROVED);
            }else{
              $poContext->setContextState(CONTEXT_STATE_PENDANT);
              $this->dispatchTask($this->ciActivity, $poContext->getId(), $miApproverId);
            }
          break;
        }
      break;

      case CONTEXT_STATE_DELETED:
        switch($piAction){
          case WKF_ACTION_UNDELETE:
            if($mbUserEqualApprover){
              $poContext->setContextState(CONTEXT_STATE_APPROVED);
            }else{
              $this->dispatchTask($this->ciActivity, $poContext->getId(), $miApproverId);
              $poContext->setContextState(CONTEXT_STATE_PENDANT);
            }
            if(!$mbUserEqualResponsible && !$mbDeletedResponsible) $this->dispatchAlert(WKF_ALERT_DELEGATION, $poContext);
          break;
        }
      break;
    }
  }

}
?>