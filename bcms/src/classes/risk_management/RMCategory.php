<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

include_once $handlers_ref . "QueryCategoryParents.php";
include_once $handlers_ref . "QuerySubCategories.php";
include_once $handlers_ref . "QueryEventsFromCategory.php";
include_once $handlers_ref . "QueryAssetsFromCategory.php";

/**
 * Classe RMCategory.
 *
 * <p>Classe que representa a tabela de categoria de ativos.</p>
 * @package ISMS
 * @subpackage classes
 */
class RMCategory extends ISMSContext implements IWorkflow {
  
 /**
  * Construtor.
  * 
  * <p>Construtor da classe RMCategory.</p>
  * @access public 
  */
  public function __construct(){ 	
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    parent::__construct("rm_category");
    $this->ciContextType = CONTEXT_CATEGORY;
    
    $this->csAliasId = "category_id";
    $this->csDependenceAliasId = "category_parent_id";
    $this->csSpecialDeleteSP = "delete_category";
    $this->coWorkflow = new RMWorkflow(ACT_CATEGORY_APPROVAL);

    $this->coDataset->addFWDDBField(new FWDDBField("fkContext",     "category_id",          DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("fkParent",      "category_parent_id",   DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("sName",         "category_name",        DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField("tDescription",  "category_description", DB_STRING));

    $this->caSensitiveFields = array('category_name');
    $this->caSearchableFields = array('category_name', 'category_description');
  }  
    
 /**
  * Retorna o id do usu�rio que deve aprovar o contexto.
  *
  * <p>M�todo para retornar id do usu�rio que deve aprovar o contexto.</p>
  * @access public
  * @return integer Id do aprovador
  */
  public function getApprover(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return ISMSLib::getConfigById(USER_LIBRARIAN);
  }

 /**
  * Retorna o id do usu�rio respons�vel pelo contexto.
  *
  * <p>M�todo para retornar id do usu�rio respons�vel pelo contexto.</p>
  * @access public
  * @return integer Id do respons�vel
  */
  public function getResponsible(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return $this->getCreator();
  }
  
 /**
  * Retorna o nome do contexto.
  * 
  * <p>M�todo para retornar o nome do contexto.</p>
  * @access public 
  * @return string Nome do contexto
  */
  public function getName() {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return $this->getFieldValue('category_name');
  }
 
 /**
  * Retorna a descri��o do contexto.
  * 
  * <p>M�todo para retornar a descri��o do contexto.</p>
  * @access public 
  * @return string Descri��o do contexto
  */ 
  public function getDescription() {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return $this->getFieldValue('category_description');
  }
  
 /**
  * Retorna os pais de uma categoria.
  * 
  * <p>M�todo para retornar os pais de uma categoria.</p>
  * @access public 
  * @param integer $piCategoryId Id da categoria
  * @param boolean $pbExecByTrash se o metodo est� sendo chamado pela lixeira
  * @return array Array de ids das categorias pais
  */ 
  public function getCategoryParents($piCategoryId,$pbExecByTrash = false) {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $moQuery = new QueryCategoryParents(FWDWebLib::getConnection(),$pbExecByTrash);
    $moQuery->setCategory($piCategoryId);
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    return $moQuery->getParentCategories();
  }
  
 /**
  * Retorna os ancestrais do contexto.
  * 
  * <p>M�todo para retornar todos contextos acima de um determinado contexto na
  * �rvore de depend�ncias.</p>
  * @access public
  * @param integer $piContextId Id do contexto
  * @param boolean $pbExecByTrash se o metodo est� sendo chamado pela lixeira
  * @return array Array de ids dos ancestrais do contexto
  */
  public function getSuperContexts($piContextId,$pbExecByTrash = false){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return $this->getCategoryParents($piContextId,$pbExecByTrash);
  }

 /**
  * Retorna as sub-categorias de uma categoria.
  * 
  * <p>M�todo para retornar as sub-categorias de uma categoria.</p>
  * @access public 
  * @param integer $piCategoryId Id da categoria
  * @return array Array de ids das sub-categorias
  */ 
  public function getSubCategories($piCategoryId) {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $moQuery = new QuerySubCategories(FWDWebLib::getConnection());
    $moQuery->setCategory($piCategoryId);
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    return $moQuery->getSubCategories();
  }
  
 /**
  * Retorna os eventos de uma categoria (e de suas sub-categoria, se existirem).
  * 
  * <p>M�todo para retornar os eventos de uma categoria (e de suas sub-categorias,
  * se existirem).</p>
  * @access public 
  * @param integer $piCategoryId Id da categoria
  * @return array Array de ids dos eventos
  */ 
  public function getEventsFromCategory($piCategoryId) {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $moQuery = new QueryEventsFromCategory(FWDWebLib::getConnection());
    $moQuery->setCategory($piCategoryId);
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    return $moQuery->getEvents();
  }
  
 /**
  * Retorna os sub-contextos de uma categoria (sub-categorias/eventos).
  * 
  * <p>M�todo para retornar os sub-contextos de uma categoria (sub-categorias/eventos).</p>
  * @access public 
  * @param integer $piCategoryId Id da categoria
  * @return array Array de ids dos sub-contextos desta categoria
  */ 
  public function getSubContexts($piCategoryId) {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return array_merge($this->getSubCategories($piCategoryId),$this->getEventsFromCategory($piCategoryId),$this->getAssetsFromCategory($piCategoryId));
  }
  
 /**
  * Indica se o contexto � delet�vel ou n�o.
  * 
  * <p>M�todo que indica se o contexto � delet�vel ou n�o.</p>
  * @access public
  * @param integer $piContextId id do contexto
  */ 
  public function isDeletable($piContextId) {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    if (!ISMSLib::getConfigById(GENERAL_CASCADE_ON)) {
      // cascade off: verificar exist�ncia de subcontextos
      if ($this->getSubContexts($piContextId))
        return false;
    }
    return true;
  }
  
 /**
  * Exibe uma popup caso n�o seja poss�vel remover um contexto.
  * 
  * <p>M�todo que exibe uma popup caso n�o seja poss�vel remover um contexto.</p>
  * @access public
  * @param integer $piContextId id do contexto
  */ 
  public function showDeleteError($piContextId) {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $msTitle = FWDLanguage::getPHPStringValue('tt_category_remove_error','Erro ao remover Categoria');
    $msMessage = FWDLanguage::getPHPStringValue('st_category_remove_error_message',"N�o foi poss�vel remover a Categoria <b>%category_name%</b>: A Categoria cont�m Sub-Categorias e/ou Eventos e/ou Ativos. Para remover Categorias que contenham Sub-Categorias, Eventos ou Ativos, ative a op��o \"Delete Cascade\" na aba \"Admin -> Configura��o\".");
    $moCategory = new RMCategory();
    $moCategory->fetchById($piContextId);
    $msCategoryName = $moCategory->getFieldValue('category_name');
    $msMessage = str_replace("%category_name%",$msCategoryName,$msMessage);
    ISMSLib::openOk($msTitle,$msMessage,"",80);
  }
  
 /**
  * Deleta logicamente um contexto.
  * 
  * <p>M�todo para deletar logicamente um contexto.</p>
  * @access public
  * @param integer $piContextId id do contexto
  */ 
  public function logicalDelete($piContextId) {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $maEvents = array_reverse($this->getEventsFromCategory($piContextId));
    $moEvent = new RMEvent();
    foreach ($maEvents as $miEventId) {
      $moEvent->delete($miEventId);
    }
    unset($moEvent);
    
    // remove logicamente todos os ativos desta categoria
    $maAssets = array_reverse($this->getAssetsFromCategory($piContextId));
    $moAsset = new RMAsset();
    foreach($maAssets as $miAssetId) {
      $moAsset->delete($miAssetId);
    }
    unset($moAsset);
    
    // remove logicamente todas as sub-categorias desta categoria
    $maSubCategories = array_reverse($this->getSubCategories($piContextId));
    $moCategory = new RMCategory();
    foreach ($maSubCategories as $miSubCategoryId) {
      $moCategory->delete($miSubCategoryId);
    }
    unset($moCategory);
    
  }
  
 /**
  * Retorna o �cone do contexto.
  *
  * <p>M�todo para retornar o �cone do contexto.</p>
  * @access public
  * @param integer $piContextId Id do contexto
  * @return string Nome do �cone
  */
  public function getIcon($piContextId = 0){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return 'icon-category.gif';
  }

 /**
  * Retorna o label do contexto.
  * 
  * <p>M�todo para retornar o label do contexto.</p>
  * @access public 
  * @return string Label do contexto
  */
  public function getLabel() {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return FWDLanguage::getPHPStringValue('mx_category', "Categoria");
  } 

 /**
  * Retorna o caminho do contexto para a cria��o do pathscroll.
  * 
  * <p>M�todo para retornar o caminho do contexto para a cria��o do pathscroll.</p>
  * @access public
  * @param integer $piTab id da tab
  * @param integer $piContextType id do contexto
  * @param string $psContextFunc fun��o que deve ser utilizada para gerar o c�digo do link  
  * @return array Array contendo o Caminho do contexto
  */ 
  public function getSystemPathScroll($piTab,$piContextType,$psContextFunc){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
 		$msHome = FWDLanguage::getPHPStringValue('st_home','Raiz');
  	$maPath = array();  	
    if($psContextFunc){
    	$maPath[]="<a href='javascript:{$psContextFunc}(0);'>";
    }else{
    	$maPath[]="<a href='javascript:isms_redirect_to_mode_scroll(" . $piTab . ",".$piContextType.",0)'>";
    }
   	$maPath[]=$msHome;
   	$maPath[]=ISMSLib::getIconCode('',-1);
   	$maPath[]="</a>";
    $miCategoryId = $this->getFieldValue('category_id');
    ;
    $maCategoryParents = array_reverse($this->getCategoryParents($miCategoryId));    
    foreach ($maCategoryParents as $miCategoryParentId) {
    	$moCategoryParent = new RMCategory();
    	$moCategoryParent->fetchById($miCategoryParentId);
    	if($psContextFunc)
    		$maPath[] = "<a href='javascript:{$psContextFunc}({$miCategoryParentId});'>";
    	else
				$maPath[] = "<a href='javascript:isms_redirect_to_mode_scroll(" . $piTab . "," . $piContextType . ",$miCategoryParentId)'>";
			$maPath[]=$moCategoryParent->getName();
   		$maPath[]=ISMSLib::getIconCode('icon-category.gif',-2);
   		$maPath[]="</a>";
    	unset($moCategoryParent);	
    }
		if($psContextFunc)
    	$maPath[] = "<a href='javascript:{$psContextFunc}({$miCategoryId});'>";
		else
    	$maPath[] = "<a href='javascript:isms_redirect_to_mode_scroll(" . $piTab . "," . $piContextType . ",$miCategoryId)'>";
		$maPath[]=$this->getName();
		$maPath[]=ISMSLib::getIconCode('icon-category.gif',-2);
		$maPath[]="</a>";
    
  	$miIContPath = count($maPath);	  
    for($miI=$miIContPath-1;$miI>0;$miI--){
			if((isset($maPath[$miI-7]))&&($maPath[$miI-7]!=''))
				$maPath[$miI-3] = $maPath[$miI-7];
			$miI=$miI-3;
    }
    return $maPath;
  }
  

 /**
  * Retorna o caminho do contexto.
  * 
  * <p>M�todo para retornar o caminho do contexto.</p>
  * @access public 
  * @return string Caminho do contexto
  */ 
  public function getSystemPath() {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
  	$maPath = array();  	
    $maPath[] = "<a href='javascript:isms_redirect_to_mode(" . LIBRARIES_MODE . ",0,0)'>".FWDLanguage::getPHPStringValue('st_libraries_module_name','Bibliotecas')."</a>";
        
    $miCategoryId = $this->getFieldValue('category_id');
    $maCategoryParents = array_reverse($this->getCategoryParents($miCategoryId));    
    foreach ($maCategoryParents as $miCategoryParentId) {
    	$moCategoryParent = new RMCategory();
    	$moCategoryParent->fetchById($miCategoryParentId);
    	$maPath[] = ISMSLib::getIconCode('icon-category.gif',-2,-4) . "<a href='javascript:isms_redirect_to_mode(" . LIBRARIES_MODE . "," . $this->ciContextType . "," . $miCategoryParentId . ")'>" . $moCategoryParent->getName() . "</a>";
    	unset($moCategoryParent);	
    }
    $maPath[] =ISMSLib::getIconCode('icon-category.gif',-2,-4). "<a href='javascript:isms_redirect_to_mode(" . LIBRARIES_MODE . "," . $this->ciContextType . "," . $miCategoryId . ")'>" . $this->getName() . "</a>";
    
    return implode("&nbsp;".ISMSLib::getIconCode('icon-arrow-right.gif',-2,0).'&nbsp;', $maPath);
  }
  
 /**
  * Retorna se o usu�rio logado tem permi��o para inserir o contexto.
  * 
  * <p>M�todo para retornar se o usu�rio tem permi��o para inserir o contexto no sistema.</p>
  * @access public
  * @return boolean Se o usu�ro tem permi��o para inserir o contexto no sistema
  */
  protected function userCanInsert(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $miContextType = $this->ciContextType;
    //contexto deve ser tratado, assim assume-se que a principio o usu�rio n�o pode inserir tal contexto
    $mbReturn = false;
    $miCount = 0;

    $maPermissions = array( 'M.L.1.7' );
    //acls negadas o usu�rio logado no sistema
    $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
    foreach($maACLs as $miKey => $miValue){
      $maACLs[$miValue]=$miKey;
    }
    //acl1 AND acl2
    $mbAuxPermission = true;
    foreach($maPermissions as $msValue){
      if(isset($maACLs[$msValue])){
        $mbAuxPermission = false;
      }
      $miCount++;
    }
    //caso exista pelo menos 1 acl nas permi��es do contexto e estas permi��es n�o est�o nas permi��es negadas do usu�rio
    if(($mbAuxPermission)&&($miCount)){
      $mbReturn = $mbAuxPermission;
    }
    return $mbReturn;
  }

 /**
  * Retorna se o usu�rio logado tem permi��o para editar o contexto.
  * 
  * <p>M�todo para retornar se o usu�rio tem permi��o para editar o contexto no sistema.</p>
  * @access protected
  * @return boolean Se o usu�ro tem permi��o de editar o contexto no sistema
  */
  public function userCanEdit($piContextId,$piUserResponsible = 0){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    if(!$this->contextIsOfType($piContextId,$this->ciContextType)) return false;
    //contexto deve ser tratado, assim assume-se que a principio o usu�rio n�o pode editar esse contexto
    $mbReturn = false;
    $maPermissions = array( 'M.L.1.2' );
    //acls negadas do usu�rio logado no sistema
    $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
    foreach($maACLs as $miKey => $miValue){
      $maACLs[$miValue]=$miKey;
    }
    $miCount = 0;
    //acl1 AND acl2
    $mbAuxPermission = true;
    foreach($maPermissions as $msValue){
      if(isset($maACLs[$msValue])){
        $mbAuxPermission = false;
      }
      $miCount++;
    }
    //caso exista pelo menos 1 acl nas permi��es do contexto e estas permi��es n�o est�o nas permi��es negadas do usu�rio
    if(($mbAuxPermission)&&($miCount)){
      $mbReturn = $mbAuxPermission;
      }
    return $mbReturn;
  }
 
 /**
  * Retorna se o usu�rio logado tem permi��o para excluir o contexto.
  * 
  * <p>M�todo para retornar se o usu�rio tem permi��o para excluir o contexto no sistema.</p>
  * @access protected
  * @return boolean Se o usu�ro tem permi��o de excluir o contexto no sistema
  */
  protected function userCanDelete($piContextId){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $miContextType = $this->ciContextType;
    if(!$this->contextIsOfType($piContextId,$miContextType)) return false;
    //contexto deve ser tratado, assim assume-se que a principio o usu�rio n�o pode inserir tal contexto
    $mbReturn = false;
    $maPermissions = array( 'M.L.1.3' );

    //acls negadas do usu�rio logado no sistema
    $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
    foreach($maACLs as $miKey => $miValue){
      $maACLs[$miValue]=$miKey;
    }
    $miCount = 0;
    //acl1 AND acl2
    $mbAuxPermission = true;
    foreach($maPermissions as $msValue){
      if(isset($maACLs[$msValue])){
        $mbAuxPermission = false;
      }
      $miCount++;
    }
    //caso exista pelo menos 1 acl nas permi��es do contexto e estas permi��es n�o est�o nas permi��es negadas do usu�rio
    if(($mbAuxPermission)&&($miCount)){
      $mbReturn = $mbAuxPermission;
    }
    return $mbReturn;
  }
  
  /**
  * Retorna o c�digo HTML do icone do contexto.
  * 
  * <p>M�todo para retornar o c�digo HTML do contexto</p>
  * @access public
  * @param integer $piId Id do contexto
  * @param integer $pbIsFetched id do contexto
  * @return string contendo o c�digo HTML do icone do contexto
  */ 
  public function getIconCode($piId, $pbIsFetched = false){
  	return ISMSLib::getIconCode('icon-category.gif', -2, 7);
  }

 /**
  * Retorna o caminho para abrir a popup de edi��o desse contexto.
  * 
  * <p>M�todo para retornar o caminho para abrir a popup de edi��o desse contexto.</p>
  * @param integer $piContextId id do contexto
  * @access public 
  * @return string caminho para abrir a popup de edi��o desse contexto
  */ 
  public function getVisualizeEditEvent($piContextId,$psUniqId){
    $soSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    $msPackagesLibrariesPath = ($soSession->getMode()==MANAGER_MODE) ? "packages/libraries/" : "../../packages/libraries/";
    $msPopupId = "popup_category_edit";
    
    return "isms_open_popup('{$msPopupId}','{$msPackagesLibrariesPath}{$msPopupId}.php?category=$piContextId','','true',192,350);"
           ."soPopUpManager.getPopUpById('{$msPopupId}').setCloseEvent( function close_visualize_$psUniqId() {soPopUpManager.closePopUp('popup_visualize_$psUniqId');} );";
  }

  /**
  * Retorna os ativos de uma categoria (e de suas sub-categoria, se existirem).
  * 
  * <p>M�todo para retornar os ativos de uma categoria (e de suas sub-
  * categorias, se existirem).</p>
  * @access public 
  * @param integer $piCategoryId Id da categoria
  * @return array Array de ids dos ativos
  */ 
  public function getAssetsFromCategory($piCategoryId) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $moQuery = new QueryAssetsFromCategory(FWDWebLib::getConnection());
    $moQuery->setCategory($piCategoryId);
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    return $moQuery->getAssets();
  }
}
?>