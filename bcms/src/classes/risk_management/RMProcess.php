<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe RMProcess.
 *
 * <p>Classe que representa a tabela de processos.</p>
 * @package ISMS
 * @subpackage classes
 */
class RMProcess extends ISMSContext{

	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe RMProcess.</p>
	 * @access public
	 */
	public function __construct(){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		parent::__construct("rm_process");
		$this->ciContextType = CONTEXT_PROCESS;
		$this->csAliasId = "process_id";
		$this->csDependenceAliasId = "process_area_id";

		$this->coDataset->addFWDDBField(new FWDDBField("fkContext",       		"process_id",             	DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkSeasonality",       	"seasonality",             	DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkArea",          		"process_area_id",        	DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("sName",          		"process_name",           	DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("tDescription",    		"process_description",    	DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("sDocument",       		"process_document",       	DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("nValue",          		"process_value",          	DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("tInput",          		"process_input",          	DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("tOutput",         		"process_output",         	DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("fkType",          		"process_type",           	DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkPriority",      		"process_priority",       	DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkResource",      		"resource",       			DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkGroup",      			"group",       				DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkResourceSubstitute",  "resourceSubstitute",       DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkGroupSubstitute",     "groupSubstitute",       	DB_NUMBER));

		$this->caSensitiveFields = array('process_name', 'process_area_id');
		$this->caSearchableFields = array('process_name', 'process_description', 'process_input' , 'process_output');
	}

	public function setSubstitute($id) {
		$this->setFieldValue('groupSubstitute',"null");
		$this->setFieldValue('resourceSubstitute',"null");
		if($id){
			$group = new CMGroup();
			$resource = new CMResource();

			if($group->fetchById($id)){
				$this->setFieldValue('groupSubstitute',$id);

			}else if($resource->fetchById($id)){
				$this->setFieldValue('resourceSubstitute',$id);

			}
		}
	}

	public function setGroup($id) {
		$this->setFieldValue('resource',"null");
		$this->setFieldValue('group',"null");
		if($id){
			$group = new CMGroup();
			$resource = new CMResource();

			if($group->fetchById($id)){
				$this->setFieldValue('group',$id);
			}else if($resource->fetchById($id)){
				$this->setFieldValue('resource',$id);
			}
		}
	}

	public function getGroup() {
		if($this->getFieldValue('resource') && $this->getFieldValue('resource') != "null"){
			return $this->getFieldValue('resource');
		}
		return $this->getFieldValue('group');
	}

	public function getSubstitute() {
		if($this->getFieldValue('resourceSubstitute') && $this->getFieldValue('resourceSubstitute') != "null"){
			return $this->getFieldValue('resourceSubstitute');
		}
		return $this->getFieldValue('groupSubstitute');
	}

	public function getGroupName() {
		if($this->getFieldValue('resource')){
			$r = new CMResource();
			$r->fetchById($this->getFieldValue('resource'));
			return $r->getFieldValue('resource_name');
		}
		if($this->getFieldValue('group')){
			$r = new CMGroup();
			$r->fetchById($this->getFieldValue('group'));
			return $r->getFieldValue('group_name');
		}
		return null;
	}

	public function getSubstituteName() {
		if($this->getFieldValue('resourceSubstitute')){
			$r = new CMResource();
			$r->fetchById($this->getFieldValue('resourceSubstitute'));
			return $r->getFieldValue('resource_name');
		}
		if($this->getFieldValue('groupSubstitute')){
			$r = new CMGroup();
			$r->fetchById($this->getFieldValue('groupSubstitute'));
			return $r->getFieldValue('group_name');
		}
		return null;
	}

	/**
	 * Retorna o id do usu�rio respons�vel pelo contexto.
	 *
	 * <p>M�todo para retornar id do usu�rio respons�vel pelo contexto.</p>
	 * @access public
	 * @return integer Id do respons�vel
	 */
	public function getResponsible() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		return null;
	}

	/**
	 * Retorna o nome do contexto.
	 *
	 * <p>M�todo para retornar o nome do contexto.</p>
	 * @access public
	 * @return string Nome do contexto
	 */
	public function getName() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		return $this->getFieldValue('process_name');
	}

	/**
	 * Retorna a descri��o do contexto.
	 *
	 * <p>M�todo para retornar a descri��o do contexto.</p>
	 * @access public
	 * @return string Descri��o do contexto
	 */
	public function getDescription() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		return $this->getFieldValue('process_description');
	}

	/**
	 * Retorna os pais do subordinante do contexto.
	 *
	 * <p>M�todo para retornar os pais do subordinante do contexto..</p>
	 * @access public
	 * @param boolean $pbExecByTrash se o metodo est� sendo chamado pela lixeira
	 * @return array Array de ids dos pais do subordinante do contexto
	 */
	public function getProcessParents($pbExecByTrash = false) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		$moArea = new RMArea();
		$miParentId = $this->getFieldValue($this->csDependenceAliasId);
		$maParents = $moArea->getAreaParents($miParentId,$pbExecByTrash);
		$maParents[] = $miParentId;
		return $maParents;
	}

	/**
	 * Retorna o �cone do contexto.
	 *
	 * <p>M�todo para retornar o �cone do contexto.</p>
	 * @access public
	 * @param integer $piContextId Id do contexto
	 * @return string Nome do �cone
	 */
	public function getIcon($piContextId = 0){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		return 'icon-process_gray.gif';
	}

	/**
	 * Retorna o label do contexto.
	 *
	 * <p>M�todo para retornar o label do contexto.</p>
	 * @access public
	 * @return string Label do contexto
	 */
	public function getLabel() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		return FWDLanguage::getPHPStringValue('mx_process', "Processo");
	}

	/**
	 * Retorna o valor do contexto.
	 *
	 * <p>M�todo para retornar o valor do contexto.</p>
	 * @access public
	 * @return integer valor do contexto
	 */
	public function getValue(){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		return $this->getFieldValue('process_value');
	}


	/**
	 * Retorna o caminho do contexto para a cria��o do pathscroll.
	 *
	 * <p>M�todo para retornar o caminho do contexto para a cria��o do pathscroll.</p>
	 * @access public
	 * @param integer $piTab id da tab
	 * @param integer $piContextType id do contexto
	 * @param string $psContextId indices do filtro de ids dos links do pathscroling
	 * @return array Array contendo o Caminho do contexto
	 */
	public function getSystemPathScroll($piTab, $piContextType, $context, $title){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		// Se n�o tiver id, mostra s� a 'Raiz'
		if(!$context){
			$msHome = FWDLanguage::getPHPStringValue('st_home','Raiz');

			$maPath = array();
		    $maPath[] = '';
		   	$maPath[] = $msHome;
		   	$maPath[] = '';
		   	$maPath[] = '';	

		   	return $maPath;
		}

		$contextIds = explode(":", $context);

		$processId = 0;
		$areaId = 0;
		$placeId = 0;

		$idsCount = count($contextIds);
		if($idsCount >= 1)
			$processId = $contextIds[0];

		if($idsCount >= 2)
			$areaId = $contextIds[1];

		if($idsCount >= 3)
			$placeId = $contextIds[2];


		$maPath = array();

		if($placeId){

			$place = new CMPlace();

		  	// Sub-Locais
			$placeParents = array_reverse($place->getPlaceParents($placeId));
			$parentsTotal = count($placeParents);

			$maPath = array();

			for($i=0; $i<$parentsTotal; $i++){

	    		$cmPlace = new CMPlace();
	    		$parentPlaceId = $placeParents[$i];

	    		$cmPlace->fetchById($parentPlaceId);

	    		if($i == 0) {
	    			$maPath[] = "<a href='javascript:isms_redirect_to_mode_scroll($piTab, " . CONTEXT_CM_PLACE . ", 0)'>";
			    } else {

			    	$returnPlaceId = $placeParents[$i - 1];
			    	$maPath[] = "<a href='javascript:enterPlace($returnPlaceId)'>";
			   	}

			   	$maPath[] = $cmPlace->getName();
			   	$maPath[] = "";
			   	$maPath[] = "</a>";

			}

			// Se j� estivermos num path, mudar o link. Se estivermos apontando para 1 item no path, esse volta para a Raiz.
			if($parentsTotal){
		   		$returnPlaceId = $placeParents[$parentsTotal-1];
		   		$maPath[] = "<a href='javascript:enterPlace($returnPlaceId)'>";
		   	} else {
		   		$maPath[] = "<a href='javascript:isms_redirect_to_mode_scroll($piTab, ". CONTEXT_CM_PLACE . ", 0)'>";
		   	}

		   	$cmPlace = new CMPlace();
		   	$cmPlace->fetchById($placeId);
		   	
		   	$maPath[] = $cmPlace->getName();
			$maPath[] = "";
			$maPath[] = "</a>";
		   	
	   	}

	   	if($areaId){
			$area = new RMArea();
			$area->fetchById($areaId);
			$areaName = $area->getName();

		    $maPath[] = "<a href='javascript:isms_redirect_to_mode_scroll($piTab, ". CONTEXT_AREA . ", $placeId)'>";
		   	$maPath[] = $areaName;
		   	$maPath[] = '';
		   	$maPath[] = "</a>";
	   	}

		if($processId){
			$process = new RMProcess();
			$process->fetchById($processId);
			$processName = $process->getName();

			if($areaId && $placeId)
				$maPath[] = "<a href='javascript:isms_redirect_to_mode_scroll($piTab, " . $piContextType . ", \\\"$areaId:$placeId\\\")'>";
			else
				$maPath[] = "<a href='javascript:isms_redirect_to_mode_scroll($piTab, " . $piContextType . ", 0)'>";
		    
		   	$maPath[] = $processName;
		   	$maPath[] = '';
		   	$maPath[] = "</a>";
		}

	    $maPath[] = "<a href='javascript:isms_redirect_to_mode_scroll($piTab, ". CONTEXT_PROCESS . ", 0)'>";
	   	$maPath[] = $title;
	   	$maPath[] = '';
	   	$maPath[] = "</a>";

		$miIContPath = count($maPath);
		
		for($miI=$miIContPath-1;$miI>0;$miI--){
			if((isset($maPath[$miI-7]))&&($maPath[$miI-7]!=''))
			$maPath[$miI-3] = $maPath[$miI-7];
			$miI=$miI-3;
		}

		return $maPath;
	}

	/**
	 * Retorna o caminho do contexto.
	 *
	 * <p>M�todo para retornar o caminho do contexto.</p>
	 * @access public
	 * @return string Caminho do contexto
	 */
	public function getSystemPath() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		$moArea = new RMArea();
		$moArea->fetchById($this->getFieldValue('process_area_id'));
		$msPath = $moArea->getSystemPath();
		$msPath .= "&nbsp;".ISMSLib::getIconCode('icon-arrow-right.gif',-2,0).'&nbsp;' . ISMSLib::getIconCode('icon-process_',$this->getValue(),-4) . " <a href='javascript:isms_redirect_to_mode(" . RISK_MANAGEMENT_MODE . "," . CONTEXT_ASSET . "," . $this->getFieldValue($this->getAliasId()) . ")'>" . $this->getName() . "</a>";
		return $msPath;
	}

	/**
	 * Retorna se o usu�rio logado tem permi��o para inserir o contexto.
	 *
	 * <p>M�todo para retornar se o usu�rio tem permi��o para inserir o contexto no sistema.</p>
	 * @access public
	 * @return boolean Se o usu�ro tem permi��o para inserir o contexto no sistema
	 */
	protected function userCanInsert(){return true;}

	/**
	 * Retorna se o usu�rio logado tem permi��o para editar o contexto.
	 *
	 * <p>M�todo para retornar se o usu�rio tem permi��o para editar o contexto no sistema.</p>
	 * @access protected
	 * @return boolean Se o usu�ro tem permi��o de editar o contexto no sistema
	 */
	public function userCanEdit($piContextId,$piUserResponsible = 0){return true;}

	/**
	 * Retorna se o usu�rio logado tem permi��o para excluir o contexto.
	 *
	 * <p>M�todo para retornar se o usu�rio tem permi��o para excluir o contexto no sistema.</p>
	 * @access protected
	 * @return boolean Se o usu�ro tem permi��o de excluir o contexto no sistema
	 */
	protected function userCanDelete($piContextId){return true;}

	/**
	 * Retorna o c�digo HTML do icone do contexto.
	 *
	 * <p>M�todo para retornar o c�digo HTML do contexto</p>
	 * @access public
	 * @param integer $piId Id do contexto
	 * @param integer $pbIsFetched id do contexto
	 * @return string contendo o c�digo HTML do icone do contexto
	 */
	public function getIconCode($piId, $pbIsFetched = false){
		if($pbIsFetched){
			$this->fetchById($piId);
		}
		return ISMSLib::getIconCode('icon-process_'.RMRiskConfig::getRiskColor($this->getValue()).'.gif',-2, 7);
	}

	/**
	 * Retorna o caminho para abrir a popup de edi��o desse contexto.
	 *
	 * <p>M�todo para retornar o caminho para abrir a popup de edi��o desse contexto.</p>
	 * @param integer $piContextId id do contexto
	 * @access public
	 * @return string caminho para abrir a popup de edi��o desse contexto
	 */
	public function getVisualizeEditEvent($piContextId,$psUniqId){
		$soSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
		$msPackagesRiskPath = ($soSession->getMode()==MANAGER_MODE) ? "packages/risk/" : "../../packages/risk/";
		$msPopupId = "popup_process_edit";

		return "isms_open_popup('{$msPopupId}','{$msPackagesRiskPath}{$msPopupId}.php?process=$piContextId','','true',312,440);"
		."soPopUpManager.getPopUpById('{$msPopupId}').setCloseEvent( function close_visualize_$psUniqId() {soPopUpManager.closePopUp('popup_visualize_$psUniqId');} );";
	}

	/**
	 * Deleta logicamente um contexto.
	 *
	 * <p>M�todo para deletar logicamente um contexto. Deve ser sobrecarregada
	 * por contextos para os quais for necess�ria.</p>
	 * @access public
	 * @param integer $piContextId id do contexto
	 */
	/*public function logicalDelete($piContextId) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		$moRel = new RMProcessAsset();
		$moRel->createFilter($piContextId,'process_id');
		$moRel->select();
		while($moRel->fetch()){
			// deletar tarefas do contexto deletado
			$moTasks = new WKFTask();
			$moTasks->createFilter($moRel->getFieldValue('process_asset_id'), 'task_context_id');
			$moTasks->delete();
			// deletar alertas do contexto deletado
			$moAlerts = new WKFAlert();
			$moAlerts->createFilter($moRel->getFieldValue('process_asset_id'), 'alert_context_id');
			$moAlerts->delete();
		}
	}*/

	/*
	 * Pega os cenarios do processo
	 */
	private function getSceneFromProcess($contextId){
	    $maParameters = func_get_args();
	    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

	    $cmScene = new CMScene();
	    $cmScene->createFilter($contextId, "scene_process");
	    $cmScene->select();

	    $scenes = array();
	    while($cmScene->fetch()){
	    	$scenes[] = $cmScene->getFieldValue("scene_id");
	    }

	    if(count($scenes))
	    	return $scenes;
	   	else
	   		return null;
	}

	/*
	 * Pega os ativos do processo
	 */
	private function getAssetFromProcess($contextId){
	    $maParameters = func_get_args();
	    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

	    $rmProcessAsset = new RMProcessAsset();
	    $rmProcessAsset->createFilter($contextId, "process_id");
	    $rmProcessAsset->select();

	    $assets = array();
	    while($rmProcessAsset->fetch()){
	    	$assets[] = $rmProcessAsset->getFieldValue("asset_id");
	    }

	    if(count($assets))
	    	return $assets;
	   	else
	   		return null;
	}

	/*
	 * Pega os atividades do processo
	 */
	private function getActivityFromProcess($contextId){
	    $maParameters = func_get_args();
	    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

	    $cmProcessActivity = new CMProcessActivity();
	    $cmProcessActivity->createFilter($contextId, "activity_process");
	    $cmProcessActivity->select();

	    $activities = array();
	    while($cmProcessActivity->fetch()){
	    	$activities[] = $cmProcessActivity->getFieldValue("activity_id");
	    }

	    if(count($activities))
	    	return $activities;
	   	else
	   		return null;
	}

	/*
	 * Pega os planos do processo
	 */
	private function getPlanFromProcess($contextId){
	    $maParameters = func_get_args();
	    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

	    $cmPlan = new CMPlan();
	    $cmPlan->createFilter($contextId, "process");
	    $cmPlan->select();

	    $plans = array();
	    while($cmPlan->fetch()){
	    	$plans[] = $cmPlan->getFieldValue("id");
	    }

	    if(count($plans))
	    	return $plans;
	   	else
	   		return null;
	}	

	/**
	 * Retorna os sub-contextos de uma �rea (sub-�reas/processos).
	 *
	 * <p>M�todo para retornar os sub-contextos de uma �rea (sub-�reas/processos).</p>
	 * @access public
	 * @param integer $piAreaId Id da �rea
	 * @return array Array de ids dos sub-contextos desta �rea
	 */
	public function verifySubContexts($contextId) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		$scenes = $this->getSceneFromProcess($contextId);

		if($scenes)
			return true;

		// quando for atividade ou ativo
		if(ISMSLib::getConfigById(ASSET_KEY)) {
			$assets = $this->getAssetFromProcess($contextId);
			if($assets)
				return true;
		} else {
			$activities = $this->getActivityFromProcess($contextId);
			if($activities)
				return true;			

		}

		$plans = $this->getPlanFromProcess($contextId);

		if($plans)
			return true;		

		
		return false;			
	}

	/**
	 * Indica se o contexto � delet�vel ou n�o.
	 *
	 * <p>M�todo que indica se o contexto � delet�vel ou n�o.</p>
	 * @access public
	 * @param integer $piContextId id do contexto
	 */
	public function isDeletable($piContextId) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		if (!ISMSLib::getConfigById(GENERAL_CASCADE_ON)) {
			// cascade off: verificar exist�ncia de subcontextos
			if ($this->verifySubContexts($piContextId))
			return false;
		}
		return true;
	}

	/**
	 * Exibe uma popup caso n�o seja poss�vel remover um contexto.
	 *
	 * <p>M�todo que exibe uma popup caso n�o seja poss�vel remover um contexto.</p>
	 * @access public
	 * @param integer $piContextId id do contexto
	 */
	public function showDeleteError($piContextId) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		$msTitle = FWDLanguage::getPHPStringValue('tt_area_remove_erorr','Erro ao remover Processo');
		$msMessage = FWDLanguage::getPHPStringValue('st_area_remove_error_message',"N�o foi poss�vel remover o Processo <b>%process_name%</b>: O Processo cont�m Cen�rios, Atividades/Ativos e Planos. Para remover, ative a op��o \"Delete Cascade\" na aba \"Admin -> Configura��o\".");
		$rmProcess = new RMProcess();
		$rmProcess->fetchById($piContextId);
		$msProcessName = $rmProcess->getFieldValue('process_name');
		$msMessage = str_replace("%process_name%",$msProcessName,$msMessage);
		ISMSLib::openOk($msTitle,$msMessage,"",80);
	}

	/**
	 * Deleta logicamente um contexto.
	 *
	 * <p>M�todo para deletar logicamente um contexto.</p>
	 * @access public
	 * @param integer $piContextId id do contexto
	 */
	public function logicalDelete($piContextId) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

	    $scenes = null;
	    $scenes = $this->getSceneFromProcess($piContextId);

	    if($scenes) {
		    foreach($scenes as $value){
		    	$cmScene = new CMScene();
		    	$cmScene->delete($value);
		    }
		}

		// quando for atividade ou ativo
		if(ISMSLib::getConfigById(ASSET_KEY)) {

		    $assets = null;
		    $assets = $this->getAssetFromProcess($piContextId);

		    if($assets) {
			    foreach($assets as $value){
			    	$rmAsset = new RMAsset();
			    	$rmAsset->delete($value);
			    }
			}	

		} else {

		    $activities = null;
		    $activities = $this->getActivityFromProcess($piContextId);

		    if($activities) {
			    foreach($activities as $value){
			    	$rmProcessActivity = new CMProcessActivity();
			    	$rmProcessActivity->delete($value);
			    }
			}				

		}		

	    $plans = null;
	    $plans = $this->getPlanFromProcess($piContextId);

	    if($plans) {
		    foreach($plans as $value){
		    	$cmPlan = new CMPlan();
		    	$cmPlan->delete($value);
		    }
		}	

	}


}
?>