<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

 include_once $handlers_ref . "QueryHasBPAssociated.php";
 
/**
 * Classe RMStandard.
 *
 * <p>Classe que representa a tabela de normas.</p>
 * @package ISMS
 * @subpackage classes
 */
class RMStandard extends ISMSContext implements IWorkflow {
  
 /**
  * Construtor.
  * 
  * <p>Construtor da classe RMStandard.</p>
  * @access public 
  */
  public function __construct(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    parent::__construct("rm_standard");
    $this->ciContextType = CONTEXT_STANDARD;
    
    $this->csAliasId = "standard_id";
    $this->coWorkflow = new RMWorkflow(ACT_STANDARD_APPROVAL);

    $this->coDataset->addFWDDBField(new FWDDBField("fkContext",   "standard_id",         DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("sName",       "standard_name",       DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField("tDescription","standard_description",DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField("tApplication","standard_application",DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField("tObjective",  "standard_objective",  DB_STRING));

    $this->caSensitiveFields = array('standard_name');
    $this->caSearchableFields = array('standard_name', 'standard_description', 'standard_application', 'standard_objective');
  }
 
 /**
  * Retorna o id do usu�rio que deve aprovar o contexto.
  *
  * <p>M�todo para retornar id do usu�rio que deve aprovar o contexto.</p>
  * @access public
  * @return integer Id do aprovador
  */
  public function getApprover(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    return ISMSLib::getConfigById(USER_LIBRARIAN);
  }

 /**
  * Retorna o id do usu�rio respons�vel pelo contexto.
  *
  * <p>M�todo para retornar id do usu�rio respons�vel pelo contexto.</p>
  * @access public
  * @return integer Id do respons�vel
  */
  public function getResponsible(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    return $this->getCreator();
  }
 
 /**
  * Retorna o nome do contexto.
  * 
  * <p>M�todo para retornar o nome do contexto.</p>
  * @access public 
  * @return string Nome do contexto
  */
  public function getName() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    return $this->getFieldValue('standard_name');
  }
 
 /**
  * Retorna a descri��o do contexto.
  * 
  * <p>M�todo para retornar a descri��o do contexto.</p>
  * @access public 
  * @return string Descri��o do contexto
  */ 
  public function getDescription() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    return $this->getFieldValue('standard_description');
  }
  
 /**
  * Retorna o �cone do contexto.
  *
  * <p>M�todo para retornar o �cone do contexto.</p>
  * @access public
  * @param integer $piContextId Id do contexto
  * @return string Nome do �cone
  */
  public function getIcon($piContextId = 0){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    return 'icon-standard.gif';
  }

 /**
  * Retorna o label do contexto.
  * 
  * <p>M�todo para retornar o label do contexto.</p>
  * @access public 
  * @return string Label do contexto
  */
  public function getLabel() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
  	return FWDLanguage::getPHPStringValue('mx_standard', "Norma");
  }
  
 /**
  * Retorna o caminho do contexto.
  * 
  * <p>M�todo para retornar o caminho do contexto.</p>
  * @access public 
  * @return string Caminho do contexto
  */ 
  public function getSystemPath() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    /*
     * No caso de uma norma n�o � necess�rio passar para a fun��o isms_redirect_to_mode(...)
     * o seu identificador pois ele apenas deve redirecionar para a aba de normas. Caso fosse
     * passado o id, toda vez que uma norma diferente fosse clicada, a aba de normas seria
     * recarregada desnecessariamente. Desse modo, ela s� � carregada a primeira vez. 
     */
    $msRMPath = "<a href='javascript:isms_redirect_to_mode(" . LIBRARIES_MODE . ",0,0)'>".FWDLanguage::getPHPStringValue('st_libraries_module_name','Bibliotecas')."</a>";
    $msContextPath = ISMSLib::getIconCode('icon-standard.gif',-2,-4) . "<a href='javascript:isms_redirect_to_mode(" . LIBRARIES_MODE . "," . $this->ciContextType . ",0)'>" . $this->getName() . "</a>";
    return $msRMPath . "&nbsp;".ISMSLib::getIconCode('icon-arrow-right.gif',-2,0).'&nbsp;' . $msContextPath;  
  } 
  
  
 /**
  * Retorna se o usu�rio logado tem permi��o para inserir o contexto.
  * 
  * <p>M�todo para retornar se o usu�rio tem permi��o para inserir o contexto no sistema.</p>
  * @access public
  * @return boolean Se o usu�ro tem permi��o para inserir o contexto no sistema
  */
  protected function userCanInsert(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $miContextType = $this->ciContextType;
    //contexto deve ser tratado, assim assume-se que a principio o usu�rio n�o pode inserir tal contexto
    $mbReturn = false;
    $miCount = 0;

    $maPermissions = array( 'M.L.3.3' );
    //acls negadas o usu�rio logado no sistema
    $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
    foreach($maACLs as $miKey => $miValue){
      $maACLs[$miValue]=$miKey;
    }
    //acl1 AND acl2
    $mbAuxPermission = true;
    foreach($maPermissions as $msValue){
      if(isset($maACLs[$msValue])){
        $mbAuxPermission = false;
      }
      $miCount++;
    }
    //caso exista pelo menos 1 acl nas permi��es do contexto e estas permi��es n�o est�o nas permi��es negadas do usu�rio
    if(($mbAuxPermission)&&($miCount)){
      $mbReturn = $mbAuxPermission;
    }
    return $mbReturn;
  }

 /**
  * Retorna se o usu�rio logado tem permi��o para editar o contexto.
  * 
  * <p>M�todo para retornar se o usu�rio tem permi��o para editar o contexto no sistema.</p>
  * @access protected
  * @return boolean Se o usu�ro tem permi��o de editar o contexto no sistema
  */
  public function userCanEdit($piContextId,$piUserResponsible = 0){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    if(!$this->contextIsOfType($piContextId,$this->ciContextType)) return false;
    //contexto deve ser tratado, assim assume-se que a principio o usu�rio n�o pode editar esse contexto
    $mbReturn = false;
    $maPermissions = array( 'M.L.3.1' );
    //acls negadas do usu�rio logado no sistema
    $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
    foreach($maACLs as $miKey => $miValue){
      $maACLs[$miValue]=$miKey;
    }
    $miCount = 0;
    //acl1 AND acl2
    $mbAuxPermission = true;
    foreach($maPermissions as $msValue){
      if(isset($maACLs[$msValue])){
        $mbAuxPermission = false;
      }
      $miCount++;
    }
    //caso exista pelo menos 1 acl nas permi��es do contexto e estas permi��es n�o est�o nas permi��es negadas do usu�rio
    if(($mbAuxPermission)&&($miCount)){
      $mbReturn = $mbAuxPermission;
      }
    return $mbReturn;
  }
 
 /**
  * Retorna se o usu�rio logado tem permi��o para excluir o contexto.
  * 
  * <p>M�todo para retornar se o usu�rio tem permi��o para excluir o contexto no sistema.</p>
  * @access protected
  * @return boolean Se o usu�ro tem permi��o de excluir o contexto no sistema
  */
  protected function userCanDelete($piContextId){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $miContextType = $this->ciContextType;
    if(!$this->contextIsOfType($piContextId,$miContextType)) return false;
    //contexto deve ser tratado, assim assume-se que a principio o usu�rio n�o pode inserir tal contexto
    $mbReturn = false;
    $maPermissions = array( 'M.L.3.2' );

    //acls negadas do usu�rio logado no sistema
    $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
    foreach($maACLs as $miKey => $miValue){
      $maACLs[$miValue]=$miKey;
    }
      $miCount = 0;
    //acl1 AND acl2
    $mbAuxPermission = true;
    foreach($maPermissions as $msValue){
      if(isset($maACLs[$msValue])){
        $mbAuxPermission = false;
      }
      $miCount++;
    }
    //caso exista pelo menos 1 acl nas permi��es do contexto e estas permi��es n�o est�o nas permi��es negadas do usu�rio
    if(($mbAuxPermission)&&($miCount)){
      $mbReturn = $mbAuxPermission;
    }
    return $mbReturn;
  }
  
  /**
  * Retorna o c�digo HTML do icone do contexto.
  * 
  * <p>M�todo para retornar o c�digo HTML do contexto</p>
  * @access public
  * @param integer $piId Id do contexto
  * @param integer $pbIsFetched id do contexto
  * @return string contendo o c�digo HTML do icone do contexto
  */ 
  public function getIconCode($piId, $pbIsFetched = false){
  	return ISMSLib::getIconCode('icon-standard.gif', -2, 7);
  }

 /**
  * Retorna o caminho para abrir a popup de edi��o desse contexto.
  * 
  * <p>M�todo para retornar o caminho para abrir a popup de edi��o desse contexto.</p>
  * @param integer $piContextId id do contexto
  * @access public 
  * @return string caminho para abrir a popup de edi��o desse contexto
  */ 
  public function getVisualizeEditEvent($piContextId,$psUniqId){
    $soSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    $msPackagesLibrariesPath = ($soSession->getMode()==MANAGER_MODE) ? "packages/libraries/" : "../../packages/libraries/";
    $msPopupId = "popup_standard_edit";
    
    return "isms_open_popup('{$msPopupId}','{$msPackagesLibrariesPath}{$msPopupId}.php?standard=$piContextId','','true',332,385);"
           ."soPopUpManager.getPopUpById('{$msPopupId}').setCloseEvent( function close_visualize_$psUniqId() {soPopUpManager.closePopUp('popup_visualize_$psUniqId');} );";
  }
  
  /**
  * Indica se o contexto � delet�vel ou n�o.
  * 
  * <p>M�todo que indica se o contexto � delet�vel ou n�o.</p>
  * @access public
  * @param integer $piContextId id do contexto
  */ 
  public function isDeletable($piContextId) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    //   se estiver associada com uma melhor pr�tica, n�o pode ser deletada
    $moHandler = new QueryHasBPAssociated(FWDWebLib::getConnection());
    $moHandler->setStandardId($piContextId);
    return !$moHandler->hasBPAssociated();
  }
  
 /**
  * Exibe uma popup caso n�o seja poss�vel remover um contexto.
  * 
  * <p>M�todo que exibe uma popup caso n�o seja poss�vel remover um contexto.</p>
  * @access public
  * @param integer $piContextId id do contexto
  */ 
  public function showDeleteError($piContextId) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $moStandard = new RMStandard();
    $moStandard->fetchById($piContextId);
    $msStandardName = ISMSLib::truncateString($moStandard->getFieldValue('standard_name'), 70);
    
    $msTitle = FWDLanguage::getPHPStringValue('tt_remove_standard_error','Erro ao Remover Norma');
    $msMessage = FWDLanguage::getPHPStringValue('st_remove_standard_error',"N�o � poss�vel remover a norma <b>%standard_name%</b> pois ela est� associada com melhores pr�ticas.");
    $msMessage = str_replace('%standard_name%', $msStandardName, $msMessage);
            
    ISMSLib::openOk($msTitle,$msMessage,"",45);
  }
}
?>