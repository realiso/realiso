<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

include_once $handlers_ref . "QueryBestPracticeEvent.php";
include_once $handlers_ref . "QueryBestPracticeStandard.php";
include_once $handlers_ref . "QueryTemplateBestPractice.php";

/**
 * Classe ISMSLibraryExport. Classe para ajudar na leitura dos arquivos XML.
 * 
 * <p>Classe para ajudar na escrita dos arquivos XML. </p>
 *
 * @package ISMS
 * @subpackage classes
 */
class ISMSLibraryExport extends FWDCrypt {
	
	protected $csXML;
	protected $caEventIds=array();
	protected $caBestPracticeIds=array();
	protected $caStandardIds=array();
	
	/**
	 * Construtor
	 * <p>Inicia a string XML com o cabe�alho padr�o e a tag raiz
	 * @access public
	 */
	public function __construct()
	{
		$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    parent::__construct();
		$this->csXML = '<?xml version="1.0" encoding="ISO-8859-1"?><library>';
	}
	
	/**
	 * Retorna o XML gerado, criptografado
	 * @access public
	 */
	public function getEncrypt() {
		$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
		$this->csXML .= '</library>';
    return $this->getIV().$this->encrypt(str_replace('&', '&amp;', $this->csXML));
	}
	
	/**
	 * Adiciona um trecho de c�digo XML
	 * <p>Adiciona um trecho de c�digo XML relativo �s vari�veis passadas.</p>
	 * 
	 * @access public
	 * @param string psType Tipo de classe a ser gerado em XML
	 * @param array paIds Array de ids da classe passada
	 */
	public function addXML($psType, $pmIds) {
		$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
		switch($psType) {
			case 'category':
				$this->csXML .= "<category>";
        if (is_array($pmIds)) {
  				foreach ($pmIds as $miCategoryId => $miCategoryParentId) {
  					$moCategory = new RMCategory();
  					$moCategory->fetchById($miCategoryId);
  					$this->csXML .= $this->buildXML($moCategory,array('category_id','category_parent_id','category_name','category_description'));
  					$moEvent = new RMEvent();
  					$moEvent->createFilter($miCategoryId, 'event_category_id');
  					$moEvent->select();
  					while ($moEvent->fetch()) {
              if ($moEvent->getContextState($moEvent->getFieldByAlias('event_id')->getValue()) != CONTEXT_STATE_DELETED) {
    						$this->csXML .= $this->buildXML($moEvent,array('event_id','event_category_id','event_description','event_observation','event_propagate'));
    						$this->caEventIds[] = $moEvent->getFieldByAlias('event_id')->getValue();
              }
  					}
  				}
        }
				$this->csXML .= "</category>";
				break;
			case 'sectionbestpractice':
				$this->csXML .= "<sectionbestpractice>";
        if (is_array($pmIds)) {
  				foreach ($pmIds as $miSectionBestPracticeId => $miSectionBestPracticeParentId) {
  					$moSectionBestPractice = new RMSectionBestPractice();
  					$moSectionBestPractice->fetchById($miSectionBestPracticeId);
  					$this->csXML .= $this->buildXML($moSectionBestPractice,array('section_id','section_parent_id','section_name'));
  					$moBestPractice = new RMBestPractice();
  					$moBestPractice->createFilter($miSectionBestPracticeId, 'section_best_practice_id');
  					$moBestPractice->select();
  					while ($moBestPractice->fetch()) {
              if ($moBestPractice->getContextState($moBestPractice->getFieldByAlias('best_practice_id')->getValue()) != CONTEXT_STATE_DELETED) {
                $this->csXML .= $this->buildXML($moBestPractice,array('best_practice_id','section_best_practice_id','best_practice_name','best_practice_description','best_practice_control_type','best_practice_classification','best_practice_imple_guide','best_practice_metric','best_practice_document'));
                $this->caBestPracticeIds[] = $moBestPractice->getFieldByAlias('best_practice_id')->getValue();
              }
  					}
  				}
        }
				$this->csXML .= "</sectionbestpractice>";
				break;
			case 'standard':
				$this->csXML .= "<standard>";
        if (is_array($pmIds)) {
  				foreach ($pmIds as $miStandardId) {
  					$moStandard = new RMStandard();
  					$moStandard->fetchById($miStandardId);
  					$this->csXML .= $this->buildXML($moStandard,array('standard_id','standard_name','standard_description','standard_application','standard_objective'));
  				}
        }
				$this->csXML .= "</standard>";
				$this->caStandardIds = $pmIds;
				break;
      case 'template':
        $this->csXML .= "<template>";
        if (is_array($pmIds)) {
          foreach ($pmIds as $miTemplateId) {
            $moTemplate = new PMTemplate(); //template without content
            $moTemplate->fetchById($miTemplateId);
            $this->csXML .= $this->buildXML($moTemplate,array('template_id','template_name','template_type','template_path','template_file_name','template_description','template_key_words','template_file_size'));
          }
        }
        $this->csXML .= "</template>";
        $this->caTemplateIds = $pmIds;
        break;
      case 'client_name':
        $this->csXML .= "<client name='$pmIds'/>";
        break;
			default:
				break;
		}
	}
	
	/**
	 * Adiciona o trecho de c�digo referente � rela��o entre as melhores pr�ticas e os eventos.
	 * <p>Adiciona o trecho de c�digo referente � rela��o entre as melhores pr�ticas e os eventos.
	 * Esse m�todo deve ser executado ap�s todos os addXML();</p>
	 * 
	 * @access public
	 */
	public function addBestPracticeEventXML() {
		$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
		if (count($this->caBestPracticeIds) > 0 && count($this->caEventIds) > 0) {
			$moBestPracticeEvent = new QueryBestPracticeEvent(FWDWebLib::getConnection());
			$moBestPracticeEvent->setBestPracticeIds($this->caBestPracticeIds);
			$moBestPracticeEvent->setEventIds($this->caEventIds);
			$moBestPracticeEvent->makeQuery();
			$moBestPracticeEvent->executeQuery();
			$maBestPracticeEventIds = $moBestPracticeEvent->getBestPracticeEvents();
			$this->csXML .= "<bestpracticeevent>";
			foreach ($maBestPracticeEventIds as $miBestPracticeEventId) {
				$moBestPracticeEvent = new RMBestPracticeEvent();
				$moBestPracticeEvent->fetchById($miBestPracticeEventId);
				$this->csXML .= $this->buildXML($moBestPracticeEvent,array('best_practice_event_id','best_practice_id','event_id'));
			}
			$this->csXML .= "</bestpracticeevent>";
		}
	}
	
	/**
	 * Adiciona o trecho de c�digo referente � rela��o entre as melhores pr�ticas e as normas.
	 * <p>Adiciona o trecho de c�digo referente � rela��o entre as melhores pr�ticas e as normas.
	 * Esse m�todo deve ser executado ap�s todos os addXML();</p>
	 * 
	 * @access public
	 */
	public function addBestPracticeStandardXML() {
		$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
		if (count($this->caBestPracticeIds) > 0 && count($this->caStandardIds) > 0) {
			$moBestPracticeStandard = new QueryBestPracticeStandard(FWDWebLib::getConnection());
			$moBestPracticeStandard->setBestPracticeIds(implode(",",$this->caBestPracticeIds));
			$moBestPracticeStandard->setStandardIds(implode(",",$this->caStandardIds));
			$moBestPracticeStandard->makeQuery();
			$moBestPracticeStandard->executeQuery();
			$maBestPracticeStandardIds = $moBestPracticeStandard->getBestPracticeStandards();
			$this->csXML .= "<bestpracticestandard>";
			foreach ($maBestPracticeStandardIds as $miBestPracticeStandardId) {
				$moBestPracticeStandard = new RMBestPracticeStandard();
				$moBestPracticeStandard->fetchById($miBestPracticeStandardId);
				$this->csXML .= $this->buildXML($moBestPracticeStandard,array('best_practice_standard_id','best_practice_id','standard_id'));
			}
			$this->csXML .= "</bestpracticestandard>";
		}
	}
  
  /**
   * Adiciona o trecho de c�digo referente � rela��o entre os modelos de
   * documentos e as melhores pr�ticas.
   * <p>Adiciona o trecho de c�digo referente � rela��o entre os modelos de
   * documentos e as melhores pr�ticas.
   * Esse m�todo deve ser executado ap�s todos os addXML();</p>
   * 
   * @access public
   */
  public function addTemplateBestPracticeXML() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    if (count($this->caTemplateIds) > 0 && count($this->caBestPracticeIds) > 0) {
      $this->csXML .= "<templatebestpractice>";
      foreach($this->caTemplateIds as $miTemplateId) {
        $moTemplateBestPractice = new QueryTemplateBestPractice(FWDWebLib::getConnection());
        $moTemplateBestPractice->setTemplateId($miTemplateId);
        $maBestPracticeIds = $moTemplateBestPractice->getValues();
        foreach($maBestPracticeIds as $maBestPractice) {
          $miBestPracticeId = $maBestPractice[0];
          if(in_array($miBestPracticeId,$this->caBestPracticeIds)) {
            $this->csXML .= '<class name="PMTemplateBestPractice">';
            $this->csXML .= '<field name="template_id">'.$miTemplateId.'</field>';
            $this->csXML .= '<field name="best_practice_id">'.$miBestPracticeId.'</field>';
            $this->csXML .= '</class>';
          }
        }
      }
      $this->csXML .= "</templatebestpractice>";
    }
  }
  
  /**
   * Adiciona o trecho de c�digo referente aos arquivos dos modelos de
   * documentos.
   * <p>Adiciona o trecho de c�digo referente aos arquivos dos modelos de
   * documentos. Esse m�todo deve ser executado ap�s todos os addXML();</p>
   * 
   * @access public
   */
  public function addTemplateFileXML() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    if (count($this->caTemplateIds) > 0){
      $this->csXML .= "<templatefile>";
      foreach($this->caTemplateIds as $miTemplateId) {
        $moTemplate = new PMTemplate(true);
        $moTemplate->fetchById($miTemplateId);
        $msTemplatePath = $moTemplate->getFieldValue('template_path');
        if ($msTemplatePath && file_exists($msTemplatePath)) {
          $this->csXML .= '<class name="NOCLASS_TemplateFile">';
          $this->csXML .= '<field name="template_id">'.$miTemplateId.'</field>';
          $this->csXML .= '<field name="template_file_name">'.$moTemplate->getFieldValue('template_file_name').'</field>';
          $this->csXML .= '<field name="file_content">'.base64_encode(file_get_contents($msTemplatePath)).'</field>';
          $this->csXML .= '<field name="template_content">'.base64_encode($moTemplate->getFieldValue('template_content')).'</field>';
          $this->csXML .= '</class>';
        }
      }
      $this->csXML .= "</templatefile>";
    }
  }
	
	/**
	 * Constr�i e retorna o c�digo XML
	 * 
	 * @access protected
	 * @param object poClass inst�ncia da classe
	 * @param array paFields Array de campos da classe.
	 * @param string psPrefix Prefixo que sera adicionado � tag 'class'
	 * @return string XML gerado
	 */
	protected function buildXML($poRMClass, $paFields) {
		$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
		$return = '<class name="'.get_class($poRMClass).'">';
		foreach($paFields as $msField) {
			$return .= '<field name="'.$msField.'">'.$poRMClass->getFieldValue($msField).'</field>';
		}
		$return .= '</class>';
		return $return;
	}	
}

?>
