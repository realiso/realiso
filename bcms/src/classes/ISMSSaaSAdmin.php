<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSSaaSAdmin.
 *
 * <p>Classe para interagir com o administrador de instâncias.</p>
 * @package ISMS
 * @subpackage classes
 */
class ISMSSaaSAdmin {
  
  // Constantes de configuração do admin utilizadas pela instância
  const TRIAL_URL = 3310;
  const COMMERCIAL_URL = 3316;
  const BUY_NOW_URL = 3317;
  
 /**
  * Array de configurações
  * @var array
  * @access protected
  */  
  protected $caSaaSConfig = array();
  
 /**
  * Construtor.
  * 
  * <p>Construtor da classe ISMSSaaSAdmin.</p>
  * @access public 
  */
  public function __construct(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $moISMSSaaS = new ISMSSaaS();
    
    $moConfig = new FWDConfig($moISMSSaaS->getConfig(ISMSSaaS::CONFIG_PATH));
    if(!$moConfig->load()){
      trigger_error('error_config_corrupt',E_USER_ERROR);
    }
    
    $maFields = $moConfig->getFields();
    
    $this->coDB = new FWDDB(
      constant($maFields['databaseType']),
      $maFields['databaseDatabase'],
      $maFields['databaseSchema'],
      $maFields['databasePassword'],
      $maFields['databaseHost']
    );
  }
 
  public function getConnection(){
    return $this->coDB;
  }
  
  public function getActivationId($psActivationCode){
    $moDataSet = new FWDDBDataSet($this->coDB,'saas_activation');
    $moDataSet->addFWDDBField(new FWDDBField('pkId','activation_id',DB_NUMBER));
    $moDataSet->setQuery("SELECT pkId AS activation_id
                          FROM saas_activation
                          WHERE sCode = '{$psActivationCode}' AND fkInstance IS NULL AND nType = ".ISMSSaaS::USER_PACK
    );
    $moDataSet->execute();
    if($moDataSet->fetch()){
      return $moDataSet->getFieldByAlias('activation_id')->getValue();
    }else{
      return 0;
    }
  }

  public function getInstanceId($psAlias){
    $moDataSet = new FWDDBDataSet($this->coDB,'saas_instance');
    $moDataSet->addFWDDBField(new FWDDBField('pkInstance','instance_id',DB_NUMBER));
    $moDataSet->setQuery("SELECT pkInstance AS instance_id
                          FROM saas_instance
                          WHERE sAlias = '{$psAlias}'"
    );
    $moDataSet->execute();
    if($moDataSet->fetch()){
      return $moDataSet->getFieldByAlias('instance_id')->getValue();
    }else{
      return 0;
    }
  }

  public function updateActivationId($piActivationCodeId,$piInstanceId){
    $msDate = FWDWebLib::timestampToDBFormat(FWDWebLib::getTime());
    $moDataSet = new FWDDBDataSet($this->coDB);
    $moDataSet->setQuery("UPDATE saas_activation
                          SET
                            fkInstance = {$piInstanceId},
                            dDateUsed = {$msDate}
                          WHERE pkId = {$piActivationCodeId}"
    );
    $moDataSet->execute();
  }
 
 /**
  * Retorna o valor de uma determinada configuração do admin.
  * 
  * <p>Retorna o valor de uma determinada configuração do admin.</p>
  * @access public
  * @param integer $piConfigId Id da configuração
  * @return string Valor da configuração
  */
  public function getConfig($piConfigId){
    $moDataSet = new FWDDBDataSet($this->coDB);
    $moDataSet->addFWDDBField(new FWDDBField('pkConfig','config_id',   DB_NUMBER));
    $moDataSet->addFWDDBField(new FWDDBField('sValue',  'config_value',DB_STRING));
    $moDataSet->setQuery("SELECT sValue AS config_value
                          FROM saas_config
                          WHERE pkConfig = {$piConfigId}"
    );
    $moDataSet->execute();
    if($moDataSet->fetch()){
      return $moDataSet->getFieldByAlias('config_value')->getValue();
    }else{
      trigger_error("Admin config '{$piConfigId}' not found.",E_USER_ERROR);
      return false;
    }
  }

}

?>