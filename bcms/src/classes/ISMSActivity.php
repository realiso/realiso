<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**********************************************************
                     ATEN��O!
ao incluir uma nova identifica��o de task aqui nesta classe,
deve-se incluir tambem na classe ISMSTaskManager.php para que as popups 
de task funcionem corretamente

***********************************************************/
define("ACT_AREA_APPROVAL", 2201);
define("ACT_PROCESS_APPROVAL", 2202);
define("ACT_ASSET_APPROVAL", 2203);
define("ACT_PROCESS_ASSET_APPROVAL", 2204);
define("ACT_RISK_APPROVAL", 2205);
define("ACT_CONTROL_APPROVAL", 2206);
define("ACT_RISK_CONTROL_APPROVAL", 2207);
define("ACT_REAL_EFFICIENCY", 2208);
define("ACT_ACCEPT_RISK_APPROVAL", 2209);
//define("ACT_RISK_HANDLING_APPROVAL", 2210);//n�o existe mais no sistema
define("ACT_BEST_PRACTICE_APPROVAL", 2211);
define("ACT_SECTION_BEST_PRACTICE_APPROVAL", 2212);
define("ACT_STANDARD_APPROVAL", 2213);
define("ACT_CATEGORY_APPROVAL", 2214);
define("ACT_EVENT_APPROVAL", 2215);
define("ACT_CONTROL_TEST", 2216);
//define("ACT_CONTROL_IMPLEMENTATION_APPROVAL", 2217);//tipo de tarefa que n�o � criado a bastantem tempo e pelo que falei (munari) com o andr� n�o � nescess�rio mais esse tipo de tarefa (hoje = 29-05-2008)
//define("ACT_SCOPE_APPROVAL", 2218);//n�o existe mais no sistema
//define("ACT_POLICY_APPROVAL", 2219);//n�o existe mais no sistema
define("ACT_RISK_LIMITS_APPROVAL", 2220);
define("ACT_DOCUMENT_REVISION", 2221);
define("ACT_DOCUMENT_APPROVAL", 2222);
define("ACT_DOCUMENT_TEMPLATE_APPROVAL",2223);
define("ACT_NON_CONFORMITY_APPROVAL", 2224);
//define("ACT_NON_CONFORMITY_DATA_APPROVAL", 2225);//n�o existe mais no sistema
define("ACT_ACTION_PLAN_FINISH_CONFIRM", 2226);
define("ACT_ACTION_PLAN_REVISION", 2227);
define("ACT_OCCURRENCE_APPROVAL", 2228);
define("ACT_INC_DISPOSAL_APPROVAL", 2229);
define("ACT_INC_SOLUTION_APPROVAL", 2230);
define("ACT_INC_CONTROL_INDUCTION",2231);
define("ACT_AP_APPROVAL",2232);
define("ACT_INC_APPROVAL",2233);
define("ACT_NEW_RISK_PARAMETER_APPROVAL",2234);
/**********************************************************
                     ATEN��O!
ao incluir uma nova identifica��o de task aqui nesta classe,
deve-se incluir tambem na classe ISMSTaskManager.php para que as popups 
de task funcionem corretamente

***********************************************************/


/**
 * Classe que representa as diferentes atividades do sistema.
 *
 * <p>Classe que representa as diferentes atividades do sistema.</p>
 *
 * @package ISMS
 * @subpackage classes
 */
class ISMSActivity {

 /**
  * Array de atividade
  * @var array
  * @access protected
  */
  protected static $caActivity = null;

 /**
  * Construtor.
  *
  * <p>Construtor da classe ISMSActivity.</p>
  * @access private
  */
  private function __construct(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
  }

  public static function build(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    self::$caActivity = array(
      ACT_AREA_APPROVAL => FWDLanguage::getPHPStringValue('wk_area_approval', "Aprovar Unidade de Neg�cio"),
      ACT_PROCESS_APPROVAL => FWDLanguage::getPHPStringValue('wk_process_approval', "Aprovar Processo"),
      ACT_ASSET_APPROVAL => FWDLanguage::getPHPStringValue('wk_asset_approval', "Aprovar Ativo"),
      ACT_PROCESS_ASSET_APPROVAL => FWDLanguage::getPHPStringValue('wk_process_asset_association_approval', "Aprovar Associa��o de Ativo com Processo"),
      ACT_RISK_APPROVAL => FWDLanguage::getPHPStringValue('wk_risk_approval', "Aprovar Risco"),
      ACT_CONTROL_APPROVAL => FWDLanguage::getPHPStringValue('wk_control_approval', "Aprovar Controle"),
      ACT_RISK_CONTROL_APPROVAL => FWDLanguage::getPHPStringValue('wk_risk_control_association_approval', "Aprovar Associa��o de Controle com Risco"),
      ACT_REAL_EFFICIENCY => FWDLanguage::getPHPStringValue('wk_real_efficiency_followup', "Acompanhar Efici�ncia Real"),
      ACT_ACCEPT_RISK_APPROVAL => FWDLanguage::getPHPStringValue('wk_risk_acceptance_approval', "Aprovar Aceita��o de Riscos acima do valor"),
      //ACT_RISK_HANDLING_APPROVAL => FWDLanguage::getPHPStringValue('wk_risk_acceptance_criteria_approval', "Aprovar defini��o de crit�rios de Aceita��o de Riscos"),//n�o existe mais no sistema
      ACT_BEST_PRACTICE_APPROVAL => FWDLanguage::getPHPStringValue('wk_best_practice_approval', "Aprovar Melhor Pr�tica"),
      ACT_SECTION_BEST_PRACTICE_APPROVAL => FWDLanguage::getPHPStringValue('wk_best_practice_section_approval', "Aprovar Se��o de Melhor Pr�tica"),
      ACT_STANDARD_APPROVAL => FWDLanguage::getPHPStringValue('wk_standard_approval', "Aprovar Norma"),
      ACT_CATEGORY_APPROVAL => FWDLanguage::getPHPStringValue('wk_category_approval', "Aprovar Categoria"),
      ACT_EVENT_APPROVAL => FWDLanguage::getPHPStringValue('wk_event_approval', "Aprovar Evento"),
      ACT_CONTROL_TEST => FWDLanguage::getPHPStringValue('wk_control_test_followup', "Acompanhar Teste do Controle"),
      //ACT_CONTROL_IMPLEMENTATION_APPROVAL => FWDLanguage::getPHPStringValue('wk_control_implementation_approval', "Aprovar Implementa��o do Controle"),//retirado do sistema, pois j� n�o � utilizado a um bom tempo
      //ACT_SCOPE_APPROVAL => FWDLanguage::getPHPStringValue('wk_scope_approval', "Aprovar Escopo"),//n�o existe mais no sistema
      //ACT_POLICY_APPROVAL => FWDLanguage::getPHPStringValue('wk_policy_approval', "Aprovar Pol�tica"),//n�o existe mais no sistema
      ACT_RISK_LIMITS_APPROVAL => FWDLanguage::getPHPStringValue('wk_risk_tolerance_approval', "Aprovar Toler�ncia ao Risco"),
      ACT_DOCUMENT_REVISION => FWDLanguage::getPHPStringValue('wk_document_revision_followup', "Acompanhar Revis�o de Documento"),
      ACT_DOCUMENT_APPROVAL => FWDLanguage::getPHPStringValue('wk_document_approval', "Aprovar Documento"),
      ACT_DOCUMENT_TEMPLATE_APPROVAL => FWDLanguage::getPHPStringValue('wk_document_template_approval', "Aprovar Modelo de Documento"),
      ACT_OCCURRENCE_APPROVAL => FWDLanguage::getPHPStringValue('wk_occurrence_approval', "Aprovar Ocorr�ncia"),
      ACT_NON_CONFORMITY_APPROVAL => FWDLanguage::getPHPStringValue('wk_non_conformity_approval', "Aprovar N�o Conformidade"),
      //ACT_NON_CONFORMITY_DATA_APPROVAL => FWDLanguage::getPHPStringValue('wk_non_conformity_data_approval', "Aprovar Dados da N�o Conformidade"),//n�o existe lugar no sistema que crie esse tipo de tarefa
      ACT_ACTION_PLAN_FINISH_CONFIRM => FWDLanguage::getPHPStringValue('wk_action_plan_finish_confirm', "Aprovar Previs�o de Conclus�o do Plano de A��o"),
      ACT_ACTION_PLAN_REVISION => FWDLanguage::getPHPStringValue('wk_action_plan_revision', "Acompanhar Revis�o do Plano de A��o"),

      ACT_INC_DISPOSAL_APPROVAL => FWDLanguage::getPHPStringValue('wk_incident_disposal_approval', "Aprovar Disposi��o Imediata para Incidente"),
      ACT_INC_SOLUTION_APPROVAL => FWDLanguage::getPHPStringValue('wk_incident_solution_approval', "Aprovar Solu��o para Incidente"),
      ACT_INC_CONTROL_INDUCTION => FWDLanguage::getPHPStringValue('wk_incident_control_induction', "Medi��o Induzida de Efici�ncia do Controle"),

      ACT_AP_APPROVAL => FWDLanguage::getPHPStringValue('wk_action_plan_approval', 'Aprovar Plano de A��o'),
      ACT_INC_APPROVAL => FWDLanguage::getPHPStringValue('wk_incident_approval', 'Aprovar Incidente'),
      ACT_NEW_RISK_PARAMETER_APPROVAL => FWDLanguage::getPHPStringValue('wk_act_new_risk_parameter_approval','Aprovar novo par�metro para Risco')
    );
  }

  /**
   * Retorna a descri��o da atividade.
   *
   * <p>M�todo para retornar a descri��o da atividade.</p>
   * @access public
   * @param integer $piActivityId Id da atividade
   * @return string Descri��o da atividade
   */
  public static function getDescription($piActivityId){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    if(self::$caActivity===null) self::build();
    if(isset(self::$caActivity[$piActivityId])){
      return self::$caActivity[$piActivityId];
    }else{
      trigger_error("Activity id not found: $piActivityId", E_USER_WARNING);
    }
  }

}
?>
