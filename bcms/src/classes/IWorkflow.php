<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Interface para elementos que possuem Workflow.
 *
 * <p>Interface para elementos que possuem Workflow.</p>
 * @package ISMS
 * @subpackage classes
 */
interface IWorkflow {

 /**
  * Retorna o id do usu�rio que deve aprovar o contexto.
  * 
  * <p>M�todo para retornar id do usu�rio que deve aprovar o contexto.</p>
  * @access public 
  * @return integer Id do aprovador
  */
  public function getApprover();
 
 /**
  * Retorna o id do usu�rio respons�vel pelo contexto.
  * 
  * <p>M�todo para retornar id do usu�rio respons�vel pelo contexto.</p>
  * @access public 
  * @return integer Id do respons�vel
  */
  public function getResponsible();  
  
 /**
  * Seta o estado do contexto.
  * 
  * <p>M�todo para setar o estado de um contexto.
  * Se n�o for passado como par�metro o id do contexto,
  * pega o id do objeto atual.</p>
  * @access public
  * @param integer $piState Estado do contexto
  * @param integer $piContextId Id do contexto
  */ 
  public function setContextState($piState, $piContextId = 0);

 /**
  * Retorna o estado do contexto.
  * 
  * <p>M�todo para retornar o estado de um contexto.
  * Se n�o for passado como par�metro o id do contexto,
  * pega o id do objeto atual.</p>
  * @access public
  * @param integer $piContextId Id do contexto
  * @return integer Estado do contexto
  */ 
  public function getContextState($piContextId = 0);

 /**
  * Retorna o id do contexto.
  * 
  * <p>M�todo para retornar id do contexto.</p>
  * @access public 
  * @return integer Id do contexto
  */
  public function getId();

}
?>