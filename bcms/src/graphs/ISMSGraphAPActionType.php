<?php
include_once $handlers_ref . "graphs/QueryChartAPActionType.php";

/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe que representa um gr�fico em pizza com propor��o de planos de a��o com seu tipo de a��o.
 *
 * <p>Classe que representa um gr�fico em pizza com propor��o de planos de a��o com seu tipo de a��o.</p>
 *
 * @package ISMS
 * @subpackage graphs
 */
class ISMSGraphAPActionType extends ISMSGraphPieChart {
  protected $ciResultCount;
  public function __construct() {
    parent::__construct();
    $moQuery = new QueryChartAPActionType(FWDWebLib::getConnection());
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    $maAPActionTypeSummary = $moQuery->getAPActionTypeSummary();
    
    $maData = array();
    $maLabels = array();
    $maColors = array();
    foreach($maAPActionTypeSummary as $msName=>$miValue) {
      if (!$miValue) continue;
      $maData[] = $miValue;
      switch($msName) {
        case "corrective_action":
          $maColors[] = 0xCA4343;
          $maLabels[] = FWDLanguage::getPHPStringValue('mx_corrective_action','A��o Corretiva');
          break;
        case "preventive_action":
          $maColors[] = 0xFBE33C;
          $maLabels[] = FWDLanguage::getPHPStringValue('mx_preventive_action','A��o Preventiva');
          break;
        default:
          break;
      }
    }
    $this->ciResultCount=count($maData);
    $this->setTitle(FWDLanguage::getPHPStringValue('mx_ap_action_type_proportion','Propor��o de Planos de A��o e seus Tipos'));
    $this->config($maData,$maLabels,$maColors);
  }
  
  public function getTotal() {
    return $this->ciResultCount;
  }
}
?>