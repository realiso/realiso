<?php
include_once $handlers_ref . "graphs/QueryChartAssetCost.php";

/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe que representa um gr�fico de ativos e valores de custo.
 *
 * <p>Classe que representa um gr�fico de ativos e valores de custo.</p>
 *
 * @package ISMS
 * @subpackage graphs
 */
class ISMSGraphAssetCost extends ISMSGraphPieChart {
  protected $ciResultCount;
  public function __construct() {
    parent::__construct();
    $moQuery = new QueryChartAssetCost(FWDWebLib::getConnection());
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    $maAssetCostSummary = $moQuery->getAssetCostSummary();
    
    $maData = array();
    $maLabels = array();
    $maColors = array();
    foreach($maAssetCostSummary as $msName=>$miValue) {
      if (!$miValue) continue;
      $maData[] = $miValue;
      switch($msName) {
        case "1mil_cost":
          $maColors[] = 0xF87431;
          $maLabels[] = FWDLanguage::getPHPStringValue('st_assets_cost_above_1m','Ativos com custo acima de $1.000.000');
          break;
        case "500k_cost":
          $maColors[] = 0xFFA018;
          $maLabels[] = FWDLanguage::getPHPStringValue('st_assets_cost_between_1m_and_500k','Ativos com custo entre $1.000.000 e $500.000');
          break;
        case "100k_cost":
          $maColors[] = 0x1F86FF;
          $maLabels[] = FWDLanguage::getPHPStringValue('st_assets_cost_between_500k_and_100k','Ativos com custo entre $500.000 e $100.000');
          break;
        case "low_cost":
          $maColors[] = 0x4E8975;
          $maLabels[] = FWDLanguage::getPHPStringValue('st_assets_cost_below_100k','Ativos com custo abaixo de $100.000');
          break;
        default:
          break;
      }
    }
    $this->ciResultCount=count($maData);
    $this->setTitle(FWDLanguage::getPHPStringValue('mx_summary_of_assets_costs','Sum�rio de Ativos e Custos'));
    $this->config($maData,$maLabels,$maColors);
  }
  
  public function getTotal() {
    return $this->ciResultCount;
  }
}
?>