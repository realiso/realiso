<?php
include_once $handlers_ref . "graphs/QueryChartControlTest.php";
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe que representa um gr�fico em barra com a quantidade de riscos residuais.
 *
 * <p>Classe que representa um gr�fico em barra com a quantidade de riscos residuais, separando-os em
 * riscos vermelhos, amarelos, verdes e azuis.</p>
 *
 * @package ISMS
 * @subpackage graphs
 */
class ISMSGraphControlTest extends ISMSGraphLineChart {
  protected $ciResultCount;
  public function __construct($piControlId=0) {
    parent::__construct();

    $moQuery = new QueryChartControlTest(FWDWebLib::getConnection());
    $moQuery->setControlId($piControlId);
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    $maControlTestSummary = $moQuery->getControlTestSummary();
    $maDates = array();
    $maValues = array();
    foreach($maControlTestSummary as $msDate=>$miValue) {
      $maDates[] = date("d/m/Y",strtotime($msDate));
      $maValues[] = $miValue;
    }
    $this->ciResultCount=count($maDates);
    $this->setTitle(FWDLanguage::getPHPStringValue('mx_history_of_control_test','Hist�rico dos Testes de Controle'));
    $this->addLegend(80, 275, false);
    $this->yAxis->setLinearScale2(0,1,array(FWDLanguage::getPHPStringValue('mx_test_not_ok',"Teste N�o OK"), FWDLanguage::getPHPStringValue('mx_test_ok',"Teste OK")));
    $this->xAxis->setLabels($maDates);
    $this->addLineLayer2();
    $this->setLineWidth(2);
    $this->addDataSet($maValues, 0xff0000, FWDLanguage::getPHPStringValue('mx_test_value',"Valor do Teste"));
  }
  
  public function getTotal() {
    return $this->ciResultCount;
  }
}
?>