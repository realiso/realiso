<?php
include_once $handlers_ref . "graphs/QueryChartIncidentCategory.php";

/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe que representa um gr�fico em pizza com propor��o de incidente com sua categoria.
 *
 * <p>Classe que representa um gr�fico em pizza com propor��o de incidente com sua categoria.</p>
 *
 * @package ISMS
 * @subpackage graphs
 */
class ISMSGraphIncidentCategory extends ISMSGraphPieChart {
  protected $ciResultCount;
  public function __construct() {
    parent::__construct();
    $moQuery = new QueryChartIncidentCategory(FWDWebLib::getConnection());
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    $maIncidentCategorySummary = $moQuery->getIncidentCategorySummary();
    
    $maData = array();
    $maLabels = array();
    foreach($maIncidentCategorySummary as $maIncidentCategory) {
      list($msName,$miValue) = $maIncidentCategory;
      if (!$miValue) continue;
      $maData[] = $miValue;
      $maLabels[] = $msName;
    }
    $this->ciResultCount=count($maData);
    if ($this->ciResultCount > 10) {
      $this->ciResultCount = 10;
      sort($maData);
      $maDataAux = array_slice($maData,9);
      $miOthers = array_sum($maDataAux);
      $maData = array_slice($maData,0,9);
      $maLabels = array_slice($maLabels,0,9);
      $maData[] = $miOthers;
      $maLabels[] = FWDLanguage::getPHPStringValue('st_others','Outros');
    }
    $maColors = array(0xCA4343, 0xFBE33C, 0x42B13E, 0x3939D2, 0xF87431, 0x4E8975, 0xFFA018, 0x1F86FF, 0x888888, 0xFEDCBA);
    $this->setTitle(FWDLanguage::getPHPStringValue('mx_incident_category_proportion','Propor��o de Incidentes e Categorias'));
    $this->config($maData,$maLabels,$maColors);
  }
  
  public function getTotal() {
    return $this->ciResultCount;
  }
}
?>