<?php
include_once $handlers_ref . "graphs/QueryChartControlEfficiency.php";
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe que representa um gr�fico em barra com a quantidade de riscos residuais.
 *
 * <p>Classe que representa um gr�fico em barra com a quantidade de riscos residuais, separando-os em
 * riscos vermelhos, amarelos, verdes e azuis.</p>
 *
 * @package ISMS
 * @subpackage graphs
 */
class ISMSGraphControlEfficiency extends ISMSGraphLineChart {
  protected $ciResultCount;
  public function __construct($piControlId=0) {
    parent::__construct();

    $moQuery = new QueryChartControlEfficiency(FWDWebLib::getConnection());
    $moQuery->setControlId($piControlId);
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    $maControlEfficiencySummary = $moQuery->getControlEfficiencySummary();
    $maDates = array();
    $maExpEff = array();
    $maRealEff = array();
    foreach($maControlEfficiencySummary as $msDate=>$maEfficiency) {
      $maDates[] = date("d/m/Y",strtotime($msDate));
      foreach($maEfficiency as $miExpectedEfficiency=>$miRealEfficiency) {
        $maExpEff[] = $miExpectedEfficiency;
        $maRealEff[] = $miRealEfficiency;
      }
    }
    $this->ciResultCount=count($maDates);
    $this->setTitle(FWDLanguage::getPHPStringValue('mx_history_of_controls_efficiency_revision','Hist�rico da Revis�o de Efici�ncia dos Controles'));    
    $this->addLegend(50, 275, false);
    $this->yAxis->setLinearScale(1,5,array(1,2,3,4,5));
    $this->xAxis->setLabels($maDates);
    $this->addLineLayer2();
    $this->setLineWidth(2);
    $this->addDataSet($maRealEff, 0xff0000, FWDLanguage::getPHPStringValue('mx_real_efficiency',"Efici�ncia Real"));
    $this->addDataSet($maExpEff, 0x008800, FWDLanguage::getPHPStringValue('mx_expected_efficiency',"Efici�ncia Esperada"));
  }
  
  public function getTotal() {
    return $this->ciResultCount;
  }
}
?>