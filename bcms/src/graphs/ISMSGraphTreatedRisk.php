<?php
include_once $handlers_ref . "graphs/QueryChartTreatedRisk.php";

/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe que representa um gr�fico em pizza com a quantidade de riscos potenciais.
 *
 * <p>Classe que representa um gr�fico em pizza com a quantidade de riscos potenciais, separando-os em
 * riscos vermelhos, amarelos, verdes e azuis.</p>
 *
 * @package ISMS
 * @subpackage graphs
 */
class ISMSGraphTreatedRisk extends ISMSGraphPieChart {
  protected $ciResultCount;
  public function __construct() {
    parent::__construct();
    $moQuery = new QueryChartTreatedRisk(FWDWebLib::getConnection());
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    $maTreatedRiskSummary = $moQuery->getTreatedRiskSummary();
    
    $maData = array();
    $maLabels = array();
    $maColors = array();
    foreach($maTreatedRiskSummary as $msName=>$miValue) {
      if (!$miValue) continue;
      $maData[] = $miValue;
      switch($msName) {
        case "control":
          $maColors[] = 0xF87431;
          $maLabels[] = FWDLanguage::getPHPStringValue('st_risk_treated_with_user_of_control','Risco tratado com aplica��o de controle');
          break;
        case "green":
          $maColors[] = 0x4E8975;
          $maLabels[] = FWDLanguage::getPHPStringValue('st_risk_accepted_for_being_green','Risco aceito por ser baixo');
        break;
        case "accepted":
          $maColors[] = 0xFBE33C;
          $maLabels[] = FWDLanguage::getPHPStringValue('st_risk_accepted_being_yellow_or_red','Risco aceito, sendo m�dio ou alto');
        break;
        case "transferred":
          $maColors[] = 0x1F86FF;
          $maLabels[] = FWDLanguage::getPHPStringValue('st_transferred_risk','Risco Transferido');
        break;
        case "avoided":
          $maColors[] = 0xFFA018;
          $maLabels[] = FWDLanguage::getPHPStringValue('st_avoided_risk','Risco Evitado');
        break;
        default:
          break;
      }
    }
    $this->ciResultCount=count($maData);
    $this->setTitle(FWDLanguage::getPHPStringValue('mx_summary_of_risks_treatments','Sum�rio dos Tratamentos de Riscos'));
    $this->config($maData,$maLabels,$maColors);
  }
  
  public function getTotal() {
    return $this->ciResultCount;
  }
}
?>