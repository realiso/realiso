<?php
include_once $handlers_ref . "graphs/QueryChartIncidentLossType.php";

/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe que representa um gr�fico em pizza com propor��o de incidente com seu tipo de perda.
 *
 * <p>Classe que representa um gr�fico em pizza com propor��o de incidente com seu tipo de perda.</p>
 *
 * @package ISMS
 * @subpackage graphs
 */
class ISMSGraphIncidentLossType extends ISMSGraphPieChart {
  protected $ciResultCount;
  public function __construct() {
    parent::__construct();
    $moQuery = new QueryChartIncidentLossType(FWDWebLib::getConnection());
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    $maIncidentLossTypeSummary = $moQuery->getIncidentLossTypeSummary();
    
    $maData = array();
    $maLabels = array();
    $maColors = array();
    foreach($maIncidentLossTypeSummary as $msName=>$miValue) {
      if (!$miValue) continue;
      $maData[] = $miValue;
      switch($msName) {
        case "direct_loss":
          $maColors[] = 0xCA4343;
          $maLabels[] = FWDLanguage::getPHPStringValue('mx_direct_losses','Perdas Diretas');
          break;
        case "indirect_loss":
          $maColors[] = 0xFBE33C;
          $maLabels[] = FWDLanguage::getPHPStringValue('mx_indirect_losses','Perdas Indiretas');
          break;
        default:
          break;
      }
    }
    $this->ciResultCount=count($maData);
    $this->setTitle(FWDLanguage::getPHPStringValue('mx_incident_loss_type_proportion','Propor��o de Incidentes e Tipos de Perda'));
    $this->config($maData,$maLabels,$maColors);
  }
  
  public function getTotal() {
    return $this->ciResultCount;
  }
}
?>