<?php
include_once $handlers_ref . "graphs/QueryChartProcessNonConformity.php";

/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe que representa um gr�fico dos ativos que possuem incidentes associados.
 *
 * <p>Classe que representa um gr�fico dos ativos que possuem incidentes associados.</p>
 *
 * @package ISMS
 * @subpackage graphs
 */
class ISMSGraphProcessNonConformity extends ISMSGraphBarChart {
  protected $ciResultCount;
  public function __construct() {
    parent::__construct();
    $moQuery = new QueryChartProcessNonConformity(FWDWebLib::getConnection());
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    $maProcessNCSummary = $moQuery->getProcessNCSummary();
    $maData = array();
    $maLabels = array();
    $maColors = array();
    $miHighValue = 0;
    foreach($maProcessNCSummary as $maProcessNC) {
      list($msName,$miValue) = $maProcessNC;
      if (!$miValue) continue;
      $miHighValue = $miValue>$miHighValue?$miValue:$miHighValue;
      $maLabels[] = strlen($msName)>28?substr($msName,0,25)."...":$msName;
      $maData[] = $miValue;
    }
    $this->ciResultCount=count($maData);
    $this->setTitle(FWDLanguage::getPHPStringValue('mx_10_processes_with_most_nc_summary','Os 10 Processos que possuem mais N�o Conformidades'));
    $this->config($maData,$maLabels,$maColors,$miHighValue);
  }
  
  public function getTotal() {
    return $this->ciResultCount;
  }
}
?>