<?php
include_once $handlers_ref . "graphs/QueryChartDocumentsCreated.php";

/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe que representa um gr�fico em barra com a quantidade de documentos criados.
 *
 * <p>Classe que representa um gr�fico em barra com a quantidade de documentos criados, 
 * separando-os em meses.</p>
 *
 * @package ISMS
 * @subpackage graphs
 */
class ISMSGraphDocumentsCreated extends ISMSGraphLineChart {
  protected $ciResultCount;
  public function __construct() {
    parent::__construct();
    $moQuery = new QueryChartDocumentsCreated(FWDWebLib::getConnection());
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    $maDocumentsCreated = $moQuery->getDocumentsCreated();
    $maDates = array();
    $maValues = array();
    foreach($maDocumentsCreated as $msDate=>$miValue) {
      $maDates[] = $msDate;
      $maValues[] = $miValue;
      $maCumulativeValues[] = array_sum($maValues);
    }
    $this->ciResultCount=count($maDates);
    $this->setTitle(FWDLanguage::getPHPStringValue('mx_amount_of_created_documents','Quantidade de Documentos criados'));
    $this->addLegend(50, 275, false);
    $this->yAxis->setLinearScale(0,max($maValues),0);
    //$this->yAxis->setLinearScale(0,max($maCumulativeValues),0);
    $this->xAxis->setLabels($maDates);
    $this->addLineLayer2();
    $this->setLineWidth(2);
    $moDataset = $this->addDataSet($maValues, 0xff0000, FWDLanguage::getPHPStringValue('st_created_documents','Documentos Criados'));
    $moDataset->setDataLabelFormat("{value}");
    //$moDataset = $moLayer->addDataSet($maCumulativeValues, 0x00ff00, FWDLanguage::getPHPStringValue('st_created_documents_cumulative','Documentos Criados - Cumulativo'));
    //$moDataset->setDataLabelFormat("{value}");
  }
  
  public function getTotal() {
    return $this->ciResultCount;
  }
}
?>