<?php
include_once $handlers_ref . "graphs/QueryChartIncidentsCreated.php";

/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe que representa um gr�fico em barra com a quantidade de incidentes criados nos �ltimos 6 meses.
 *
 * <p>Classe que representa um gr�fico em barra com a quantidade de incidentes criados nos �ltimos 6 meses.</p>
 *
 * @package ISMS
 * @subpackage graphs
 */
class ISMSGraphIncidentsCreated extends ISMSGraphBarChart {
  protected $ciResultCount;
  public function __construct() {
    parent::__construct();
    $moQuery = new QueryChartIncidentsCreated(FWDWebLib::getConnection());
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    $maIncidentsCreatedSummary = $moQuery->getIncidentsCreatedSummary();
    
    $maDates = array();
    $maValues = array();
    $miHighValue = 0;
    foreach($maIncidentsCreatedSummary as $msDate=>$miValue) {
      $maDates[] = $msDate;
      $maValues[] = $miValue;
      $miHighValue = $miHighValue>=$miValue?$miHighValue:$miValue;
    }
    $this->ciResultCount=count($maValues);
    $this->setTitle(FWDLanguage::getPHPStringValue('mx_incidents_created_summary','Sum�rio de Incidentes criados nos �ltimos 6 meses'));
    $this->config($maValues,$maDates,'',$miHighValue);
  }
  
  public function getTotal() {
    return $this->ciResultCount;
  }
}
?>