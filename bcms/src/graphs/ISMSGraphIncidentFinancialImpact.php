<?php
include_once $handlers_ref . "graphs/QueryChartIncidentFinancialImpact.php";

/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe que representa um gr�fico dos 10 incidentes com maior impacto financeiro.
 *
 * <p>Classe que representa um gr�fico dos 10 incidentes com maior impacto financeiro.</p>
 *
 * @package ISMS
 * @subpackage graphs
 */
class ISMSGraphIncidentFinancialImpact extends ISMSGraphBarChart {
  protected $ciResultCount;
  public function __construct() {
    parent::__construct();
    $moQuery = new QueryChartIncidentFinancialImpact(FWDWebLib::getConnection());
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    $maIncidentFinancialImpactSummary = $moQuery->getIncidentFinancialImpactSummary();
    $maData = array();
    $maLabels = array();
    $maColors = array();
    $miHighValue = 0;
    foreach($maIncidentFinancialImpactSummary as $maIncidentFinancialImpact) {
      list($msName,$miValue) = $maIncidentFinancialImpact;
      if (!$miValue) continue;
      $miHighValue = $miValue>$miHighValue?$miValue:$miHighValue;
      $maLabels[] = strlen($msName)>28?substr($msName,0,25)."...":$msName;
      $maData[] = $miValue;
    }
    $this->ciResultCount=count($maData);
    $this->setTitle(FWDLanguage::getPHPStringValue('mx_incident_financial_impact_summary','Os 10 Incidentes com maior Impacto Financeiro'));
    $this->config($maData,$maLabels,$maColors,$miHighValue,710,true);
  }
  
  public function getTotal() {
    return $this->ciResultCount;
  }
}
?>