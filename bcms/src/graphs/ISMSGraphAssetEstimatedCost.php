<?php
include_once $handlers_ref . "graphs/QueryChartAssetEstimatedCost.php";

/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe que representa um gr�fico de ativos, mostrando quantos possuem estimativa de custo.
 *
 * <p>Classe que representa um gr�fico de ativos, mostrando quantos possuem estimativa de custo.</p>
 *
 * @package ISMS
 * @subpackage graphs
 */
class ISMSGraphAssetEstimatedCost extends ISMSGraphPieChart {
  protected $ciResultCount;
  public function __construct() {
    parent::__construct();
    $moQuery = new QueryChartAssetEstimatedCost(FWDWebLib::getConnection());
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    $maAssetEstimatedCostSummary = $moQuery->getAssetEstimatedCostSummary();
    
    $maData = array();
    $maLabels = array();
    $maColors = array();
    foreach($maAssetEstimatedCostSummary as $msName=>$miValue) {
      if (!$miValue) continue;
      $maData[] = $miValue;
      switch($msName) {
        case "estimated_cost":
          $maColors[] = 0x1F86FF;
          $maLabels[] = FWDLanguage::getPHPStringValue('st_assets_with_estimated_cost','Ativos com custo estimado');
          break;
        case "not_estimated_cost":
        $maColors[] = 0xFFA018;
          $maLabels[] = FWDLanguage::getPHPStringValue('st_assets_without_estimated_cost','Ativos sem custo estimado');
          break;
        default:
          break;
      }
    }
    $this->ciResultCount=count($maData);
    $this->setTitle(FWDLanguage::getPHPStringValue('mx_assets_with_without_estimated_costs','Ativos com/sem Valor de Custo Estimado'));
    $this->config($maData,$maLabels,$maColors);
  }
  
  public function getTotal() {
    return $this->ciResultCount;
  }
}
?>