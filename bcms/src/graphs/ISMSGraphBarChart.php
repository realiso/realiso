<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe que representa um gr�fico em barra.
 *
 * <p>Classe que representa um gr�fico em barra.</p>
 *
 * @package ISMS
 * @subpackage graphs
 */
class ISMSGraphBarChart extends XYChart {
 
  protected $cbHasEnoughData = false;

  protected $csTitle;
  
  protected $csColor;

  public function __construct($piWidth=940, $piHeight=380, $psBgColor=0xF1F8FF, $psEdgeColor=-1, $piRaisedEffect=0) {
    parent::__construct($piWidth,$piHeight,$psBgColor,$psEdgeColor,$piRaisedEffect);
  }
  
  public function setTitle($psTitle,$psColor=0xD9EAFE) {
    $this->csTitle = $psTitle;
    $this->csColor = $psColor;
  }
  
  public function config($paData,$paLabels,$paColors,$piHighValue,$piLegendLeft=710,$pbCurrency=false) {
    if(count($paData)){
      $this->cbHasEnoughData = true;
      $moBarLayer = $this->addBarLayer3($paData,$paColors,$paLabels);
      //$moBarLayer->setBarShape(CircleShape);
      $moBarLayer->setAggregateLabelStyle();
      if ($pbCurrency) $moBarLayer->setAggregateLabelFormat("$ {value}");
      $this->yAxis->setLinearScale(0,$piHighValue>5?$piHighValue+1:5);
      $this->addLegend(50, 310 ,false,GRAPH_FONT,8);
    }
  }

  protected function makeTitle(){
    $moTitle = $this->addTitle2(TopLeft,$this->csTitle,GRAPH_FONT_BOLD,10,0x555555,$this->gradientColor(0, 0, 0, 24, 0xE3EFFF, 0x9EBFF4));
    $moTitle->setMargin2(6,5,5,5);
  }

  public function makeGraph($piX=50,$piY=50,$piWidth=840,$piHeight=250) {
    if($this->cbHasEnoughData){
      $this->makeTitle();
      $this->setPlotArea($piX, $piY, $piWidth, $piHeight);
    }else{
      $this->setBorder(0xF1F8FF);
      $msMessage = FWDLanguage::getPHPStringValue('st_chart_not_enough_data','N�o h� dados coletados suficientes para gerar esse gr�fico.');
      $this->addText(0,180,$msMessage,GRAPH_FONT,12,0x000000,5)->setWidth(940);
    }
    return $this;
  }

}
?>