<?php

class FWDCodeCompresser{

  protected $csInput;
  protected $csOutput;
  protected $ciBlockStart;
  protected $csLanguage;
  protected $caAllowedLanguages = array('js','css');

  public function __construct($psLanguage='js'){
    if(in_array($psLanguage,$this->caAllowedLanguages)){
      $this->csLanguage = $psLanguage;
    }else{
      trigger_error("Unsuported language.",E_USER_ERROR);
    }
  }
  
  protected function charIn($psChar,$psCharSet,$pbCaseSensitive=false){
    if($pbCaseSensitive){
      return strpos($psCharSet,$psChar)!==false;
    }else{
      return strpos(strtolower($psCharSet),strtolower($psChar))!==false;
    }
  }
  
  protected function appendBlock($piEnd,$pbString=false){
    $miStart = $this->ciBlockStart;
    $msSeparators = ';({[,)';
    $msSpace = "\t\n\r ";
    $msAlphaNum = 'abcdefghijklmnopqrstuvwxyz0123456789_';
    if($pbString){
      $this->csOutput.= substr($this->csInput,$miStart,$piEnd - $miStart);
    }else{
      $i = $miStart;
      while($i<$piEnd){
        while(!$this->charIn($this->csInput[$i],$msSpace)){
          $this->csOutput.= $this->csInput[$i++];
          if($i>=$piEnd) break;
        }
        $miLastChar = strlen($this->csOutput)-1;
        while($i<$piEnd && $this->charIn($this->csInput[$i],$msSpace)) $i++;
        $msLastChar = ($miLastChar<0?';':$this->csOutput[$miLastChar]);
        if($i<$piEnd){
          $miNextChar = $i;
          $msNextChar = $this->csInput[$miNextChar];
          if(!$this->charIn($msLastChar,$msSeparators)){ // nao era um separador
            if($this->charIn($msLastChar,$msAlphaNum) && $this->charIn($msNextChar,$msAlphaNum)){
              $this->csOutput.= ' ';
            }elseif($msLastChar=='}' && $this->csLanguage=='js' && $miNextChar<$piEnd){
              $miLookAheadLength = min(5,$piEnd-$miNextChar);
              $msLookAhead = strtolower(substr($this->csInput,$miNextChar,$miLookAheadLength));
              if($msLookAhead[0]!='}' && $msLookAhead[0]!=']' && $msLookAhead[0]!=')' && substr($msLookAhead,0,4)!='else' && $msLookAhead!='while' && $msLookAhead!='catch'){
                $this->csOutput.= ';';
              }
            }
          }
        }
      }
    }
  }
  
  public function compressString($psString,$psLanguage=''){
    if($psLanguage!=''){
      if(in_array($psLanguage,$this->caAllowedLanguages)){
        $this->csLanguage = $psLanguage;
      }else{
        trigger_error("Unsuported language.",E_USER_ERROR);
      }
    }
    $this->csInput = $psString;
    $miInputSize = strlen($this->csInput);
    $this->csOutput = '';
    $i = 0;
    $msQuote = '';
    $this->ciBlockStart = 0;
    while($i<$miInputSize){
      if($this->csInput[$i]=='"' || $this->csInput[$i]=="'"){
        if($msQuote==''){ // entrando numa string
          $this->appendBlock($i);
          $msQuote = $this->csInput[$i];
          $this->ciBlockStart = $i++;
        }elseif($this->csInput[$i] == $msQuote){ // saindo de uma string
          $this->appendBlock($i+1,true);
          $msQuote = '';
          $this->ciBlockStart = ++$i;
        }else{ // aspas dentro de uma string
          $i++;
        }
      }elseif($msQuote=='' && $this->csInput[$i]=='/'){ // possivel comentario
        if($this->csInput[$i+1]=='*'){
          $this->appendBlock($i);
          $i = $this->ciBlockStart = strpos($this->csInput,'*/',$i+2)+2;
        }elseif($this->csInput[$i+1]=='/'){
          $this->appendBlock($i);
          $this->ciBlockStart = strpos($this->csInput,"\n",$i+2);
          if($this->ciBlockStart!==false){
            $i = ++$this->ciBlockStart;
          }else{
            $i = $this->ciBlockStart = $miInputSize;
            break;
          }
        }else{
          for($j=$i-1;$j>=0;$j--){
            if(strpos("\n\t ",$this->csInput[$j])===false) break;
          }
          if($this->csInput[$j]!='(' && $this->csInput[$j]!='='){ // alarme falso, eh soh uma divisao
            $i++;
            continue;
          }
          $this->appendBlock($i);
          $this->ciBlockStart = $i++;
          while($this->csInput[$i]!='/'){
            if($this->csInput[$i]=='\\') $i+=2;
            else $i++;
          }
          $this->appendBlock($i+1,true);
          $this->ciBlockStart = ++$i;
        }
      }elseif($msQuote!='' && $this->csInput[$i]=='\\'){ // caractere escapado dentro de uma string
        $i+=2;
      }else{ // caractere normal
        $i++;
      }
    }
    $this->appendBlock($miInputSize);
    $miOutputSize = strlen($this->csOutput);
    return $this->csOutput;
  }
  
  public function compressFile($psFileName,$psLanguage=''){
    if(!file_exists($psFileName) || !is_readable($psFileName)){
      trigger_error("Could not read file '$psFileName'.",E_USER_ERROR);
    }else{
      if(in_array($psLanguage,$this->caAllowedLanguages)){
        $msLanguage = $psLanguage;
      }else{
        $msExt = substr($psFileName,strrpos($psFileName,'.')+1);
        if(in_array($msExt,$this->caAllowedLanguages)){
          $msLanguage = $msExt;
        }else{
          $msLanguage = $psLanguage;
        }
      }
      $mrFile = fopen($psFileName,'r');
      $msContent = fread($mrFile,filesize($psFileName));
      fclose($mrFile);
      return $this->compressString($msContent,$msLanguage);
    }
  }

}

?>