
/*******************************************************************************
 * Abre uma PopUp.
 *
 * <p>Abre (cria) uma PopUp.</p>
 *
 * @package javascript
 * @param String psPopUpId Id da PopUp
 * @param String psSrc URL da p�gina a ser aberta na PopUp
 * @param String psTitle String ou id do elemento HTML que ser� o t�tulo da PopUp
 * @param integer pbModal Indica se a PopUp � modal ou n�o
 * @param integer piClientHeight Altura da �rea cliente da PopUp (opcional)
 * @param integer piClientWidth Largura da �rea cliente da PopUp (opcional)
 * @param integer piTop Top da PopUp (opcional)
 * @param integer piLeft Left da PopUp (opcional)
 * OBS: Se piTop e piLeft n�o forem passados, a PopUp � criada centralizada
 * OBS2: Se piClientHeight e piClientWidth n�o forem passados, a PopUp � criada
 *   com um tamanho padr�o e � necess�rio chamar o m�todo finishLoad quando ela
 *   terminar de carregar, para que o seu tamanho seja ajustado. Esse m�todo j�
 *   � chamado no dump_html caso a PopUp tenha o id igual ao nome do arquivo
 *   (sem a extens�o).
 *******************************************************************************/
function isms_open_popup(psPopUpId,psSrc,psTitle,pbModal,piClientHeight,piClientWidth,piTop,piLeft){
  var moTitle = gebi(psTitle);
  var mmTitle = (moTitle?moTitle:psTitle);
  var miHeight = (piClientHeight?piClientHeight:100) + (browser.isIE?25:27);
  var miWidth = (piClientWidth?piClientWidth:100) + (browser.isIE?2:4);
  
  if(getISMSMode()==ADMIN_MODE){
    var maPopups = new Array();
    maPopups.push("session_expired");
    for(i=0; i<maPopups.length; i++){
      if(psSrc.indexOf(maPopups[i])!=-1){
        if(psSrc.indexOf('../') == -1){
          psSrc = '../../'+ psSrc;
        }
      }
    }
  }
  if(!piClientHeight || !piClientWidth){
    js_open_popup(psPopUpId,psSrc,mmTitle,pbModal,miHeight,miWidth,-2000,piLeft);
    soPopUpManager.getRootWindow().showLoading();
    soPopUpManager.getPopUpById(psPopUpId).setHtmlContent('',true);
  }else{
    js_open_popup(psPopUpId,psSrc,mmTitle,pbModal,miHeight,miWidth,piTop,piLeft);
  }

  psPopUpId = psSrc = msLoading = psTitle = moTitle = mmTitle = null;
}

/*******************************************************************************
 * Redireciona para um determinado modo do ISMS.
 *
 * <p>Redireciona para um determinado modo do ISMS.</p>
 *
 * @package javascript
 * @param integer piType Tipo do contexto
 * @param integer piId Id do contexto
 *******************************************************************************/
function isms_redirect_to_mode(piMode, piType, piId) {
	switch (piMode) {
		case RISK_MANAGEMENT_MODE:
			isms_change_to_sibling(2,'packages/risk/tab_risk_management.php?context_type='+piType+'&context_id='+piId);
		break;

		case POLICY_MODE:
			isms_change_to_sibling(3,'packages/policy/tab_policy_management.php?context_type='+piType+'&context_id='+piId);
		break;

		case INCIDENT_MODE:
			isms_change_to_sibling(4,'packages/improvement/tab_continual_improvement.php?context_type='+piType+'&context_id='+piId);
		break;

		case LIBRARIES_MODE:
			isms_change_to_sibling(5,'packages/libraries/tab_libraries.php?context_type='+piType+'&context_id='+piId);
		break;

		case ADMIN_MODE:
			parent.document.location = 'packages/admin/tab_main.php?context_type='+piType;
		break;
	}
}

/*******************************************************************************
 * Abre a PopUp de visualiza��o do ISMS.
 *
 * <p>Abre a PopUp de visualiza��o do ISMS.</p>
 *
 * @package javascript
 * @param integer piId Id do contexto
 * @param integer piType Tipo do contexto
 *******************************************************************************/
function open_visualize(piId,piType,psUniqId){
  var msPath = '';
  if(getISMSMode()==TASK_EMAIL_MODE){
    	msPath = "";
  }else{
  	if(getISMSMode()==MANAGER_MODE){
	  	msPath =  "";
  	}else{
	  	msPath = "../../";
  	}
  }

  isms_open_popup('popup_visualize_'+psUniqId,msPath+'visualize.php?id='+piId+'&type='+piType+'&uniqid='+psUniqId,'','true',232,510);
  msPath = piId = piType = psUniqId = null;
}

// objeto que identifica o modo atual do sistema
var soISMSMode = 0;

/*******************************************************************************
 * Seta o modo do ISMS.
 *
 * <p>Seta o modo ISMS.</p>
 *
 * @package javascript
 * @param integer piNewType Novo modo
 *******************************************************************************/
function setISMSMode(piNewMode) {
  var moParent = window;
  var moChild = null;
  while(moChild!=moParent){
    moChild = moParent;
    moParent = moParent.parent;
  }
  moParent.soISMSMode = piNewMode;
}

/*******************************************************************************
 * Retorna o modo do ISMS.
 *
 * <p>Retorna o modo ISMS.</p>
 *
 * @package javascript
 * @return integer Novo modo
 *******************************************************************************/
function getISMSMode() {
  var moParent = window;
  var moChild = null;
  while(moChild!=moParent){
    moChild = moParent;
    moParent = moParent.parent;
  }
  return moParent.soISMSMode;
}

/*******************************************************************************
 * Muda a aba selecionada para uma irm� dela.
 *
 * <p>Muda a aba selecionada para uma irm� dela. S� funciona se chamada de
 * dentro da aba selecionada.</p>
 *
 * @package javascript
 * @param integer piTab N�mero da aba
 * @param String psUrl URL que deve ser carregada na aba
 *******************************************************************************/
function isms_change_to_sibling(piTab,psUrl){
  setTimeout(function(){parent.soTabSubManager.changeTab(piTab,psUrl);},1);
}

/*******************************************************************************
 * Redireciona para um determinado modo do ISMS.
 *
 * <p>Redireciona para um determinado modo do ISMS utilizado pelos links dos pathscrolling.</p>
 *
 * @package javascript
 * @param integer piType Tipo do contexto
 * @param integer piMode Modulo do ISMS
 * @param integer piId Filtro de ids do contexto
 * @param integer piIdAux Filtro de ids do contexto (auxiliar - utilizado na recurs�o entre Risco -> Controle)
 *******************************************************************************/
function isms_redirect_to_mode_scroll(piMode, piType, piId,piIdAux){
	switch (piMode) {
		case RISK_MANAGEMENT_MODE:
			switch(piType){
				case CONTEXT_CM_PLAN:
					  setTimeout(function(){parent.soTabSubManager.changeTab(TAB_PLAN,"nav_plan.php")});
				break;
				case CONTEXT_CM_PLACE:
					  setTimeout(function(){parent.soTabSubManager.changeTab(TAB_PLACE,"nav_places.php")});
				break;
				case CONTEXT_AREA:
					  setTimeout(function(){parent.soTabSubManager.changeTab(TAB_AREA,"nav_un.php?place="+piId)});
				break;
				case CONTEXT_PROCESS:
					setTimeout(function(){parent.soTabSubManager.changeTab(TAB_PROCESS,"nav_process.php?area="+piId)});
				break;
				case CONTEXT_ASSET:
					setTimeout(function(){parent.soTabSubManager.changeTab(TAB_ASSET,"nav_asset.php?scrollfilter="+piId)});
				break;

				case CONTEXT_CM_SCENE:
					// cen�rio sempre vai precisar de um processo.
					setTimeout(function(){parent.soTabSubManager.changeTab(TAB_PROCESS,"nav_process_scene.php?scene_process_id="+piId)});
				break;

				case CONTEXT_CM_PROCESS_ACTIVITY:
					setTimeout(function(){parent.soTabSubManager.changeTab(TAB_PROCESS,"nav_process_activity.php?activity_process_id="+piId)});
				break;
			}
		break;
		case LIBRARIES_MODE:
			switch(piType){
				case CONTEXT_CM_PROCESS_CATEGORY:
						enter_category(piId);
				break;
				case CONTEXT_CM_PLACE_CATEGORY:
					enter_category(piId);
				break;
				case CONTEXT_CM_ASSET_CATEGORY:
					enter_category(piId);
				break;
			}
		break;
	}
}

function format_currency(num){
  num = num.toString().replace(/,/g,'');
  if(isNaN(num)) num = '0';
  num = Math.floor(num*100+0.50000000001);
  cents = num%100;
  num = Math.floor(num/100).toString();
  if(cents<10) cents = '0' + cents;
  for(var i = 0; i < Math.floor((num.length-(1+i))/3); i++){
    num = num.substring(0,num.length-(4*i+3))+','+num.substring(num.length-(4*i+3));
  }
  return num + '.' + cents;
};

/**
 * Retorna o valor de um risco em percentual.
 *
 * <p>Retorna o valor de um risco em percentual.</p>
 * @param integer piRiskValueCount N�mero de valores poss�veis para os riscos
 * @param integer piValue Valor absoluto do risco
 * @return String Valor do risco em percentual
 */
function getRiskPercentualValue(piRiskValueCount,piValue){
  return Math.floor((piValue/Math.pow(piRiskValueCount,2))*10000+.5)/100;
}

function destroyAutoCompleteDiv(){
	var div = gebi("autocompleteDiv");
	if(div){
		div.parentNode.removeChild(div);
	}
}

function createAutoCompleteDiv(listElements, nameElement, idElement, position){
	destroyAutoCompleteDiv();
	
	addEvent(gebi(nameElement), 'blur', function(){
		if(gebi(idElement).value == ""){
			gebi(nameElement).value = "";
			gebi(idElement).value = "";
			setTimeout(destroyAutoCompleteDiv, 200);
		}
	});
	
	var div = document.createElement("DIV");
	div.id = "autocompleteDiv";
	div.className = "divAutocomplete";
	
	div.style.top = position.top+"px";
	div.style.left = position.left+"px";
	var ul = document.createElement("UL");
	ul.style.border="1px solid";
	
	for(var i = 0; i < listElements.length; i++){
		var hidden = document.createElement("input");
		hidden.type = "hidden";
		hidden.value = listElements[i].id;
		var li = document.createElement("LI");
		li.innerHTML = "<a href='javascript:void(0)'>"+listElements[i].name+"</a>";
		li.appendChild(hidden);
		ul.appendChild(li);
		
		addEvent(li, 'click', function(event){
			var el = null;
			if(event.target)
				el = event.target;
			else if(event.srcElement)
				el = event.srcElement;
			gebi(idElement).value = el.parentNode.childNodes[1].value;
			gebi(nameElement).value = el.innerHTML;
			gebi(nameElement+'Old').value = el.innerHTML;
		});
		addEvent(li, 'click', destroyAutoCompleteDiv);
	}
	div.appendChild(ul);
	gebi(nameElement).parentNode.insertBefore(div, gebi(nameElement));
}
function addEvent( obj, type, fn ) {
	  if ( obj.attachEvent ) {
	    obj['e'+type+fn] = fn;
	    obj[type+fn] = function(){obj['e'+type+fn]( window.event );}
	    obj.attachEvent( 'on'+type, obj[type+fn] );
	  } else
	    obj.addEventListener( type, fn, false );
	}