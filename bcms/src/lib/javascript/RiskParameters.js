/*******************************************************************************
 * Classe ISMSRiskParameters. Componente de parametriza��o de riscos.
 * 
 * <p>Implementa a parte em JavaScript do componente de parametriza��o de
 * riscos.</p>
 *
 * @package javascript
 * @param String psName Identificador do componente
 * @param String psParamsIds Identificadores dos par�metros separados por ':'
 *******************************************************************************/
function ISMSRiskParameters(psName,psParamsIds){
  var self = this;

 /**
  * Identificador do componente
  * @var String
  */
  self.csName = psName;

 /**
  * Identificadores dos par�metros
  * @var Array
  */
  self.caParams = psParamsIds.split(':');

 /**
  * Identificador do par�metro selecinado
  * @var String
  */
  self.csSelected = '';

 /**
  * Classe CSS default do par�metro selecinado
  * @var String
  */
  self.csSelectedDefaultClass = '';

 /**
  * Classe CSS a ser aplicada ao par�metro selecinado
  * @var String
  */
  self.csSelectedClass = 'parameter_selected';

 /**
  * Identificador do par�metro sob o cursor do mouse
  * @var String
  */
  self.csMouseOver = '';

 /**
  * Classe CSS default do par�metro sob o cursor do mouse
  * @var String
  */
  self.csMouseOverDefaultClass = '';

 /**
  * Classe CSS a ser aplicada ao par�metro sob o cursor do mouse
  * @var String
  */
  self.csMouseOverClass = 'BdZOver';
  
 /**
  * Indica se todos os par�metros vazios deve ser considerado um valor v�lido
  * @var boolean
  */
  self.cbAllowEmpty = false;
  
 /*****************************************************************************
  * Muda as classes dos labels para indicar quais par�metros precisam ser
  * preenchidos.
  *
  * <p>Muda as classes dos labels para indicar quais par�metros precisam ser
  * preenchidos.</p>
  * @param boolean pbForceOk For�a a considerar todos par�metros preenchidos
  *****************************************************************************/
  self.updateLabels = function(pbForceOk){
    var i, moLabel;
    for(i=0;i<self.caParams.length;i++){
      moLabel = gebi(self.csName+'_static_'+self.caParams[i]);
      msClassName = moLabel.className;
      if(pbForceOk || gebi(self.csName+'_'+self.caParams[i]).value){
        moLabel.className = moLabel.className.replace(/_Label$/,'');
      }else{
        if(msClassName.substr(msClassName.length-6)!='_Label'){
          moLabel.className = moLabel.className+'_Label';
        }
      }
    }
  }
  
 /*****************************************************************************
  * Indica se o elemento est� satisfatoriamente preenchido.
  *
  * <p>Indica se o elemento est� satisfatoriamente preenchido
  * (um dos m�todos necess�rios para customizar o required check).</p>
  * @return boolean True sse est� satisfatoriamente preenchido
  *****************************************************************************/
  self.isFilled = function(){
    var i;
    var mbOneFilled = false;
    var mbOneNotFilled = false;
    if(self.cbAllowEmpty){
      for(i=0;i<self.caParams.length;i++){
        if(gebi(self.csName+'_'+self.caParams[i]).value){
          mbOneFilled = true;
        }else{
          mbOneNotFilled = true;
        }
        if(mbOneFilled && mbOneNotFilled){
          return false;
        }
      }
    }else{
      for(i=0;i<self.caParams.length;i++){
        if(!gebi(self.csName+'_'+self.caParams[i]).value){
          return false;
        }
      }
    }
    return true;
  }
  
 /*****************************************************************************
  * M�todo executado quando o elemento est� preenchido satisfatoriamente.
  *
  * <p>M�todo executado quando o elemento est� preenchido satisfatoriamente.
  * (um dos m�todos necess�rios para customizar o required check).</p>
  *****************************************************************************/
  self.onRequiredCheckOk = function(){
    self.updateLabels(true);
  }
  
 /*****************************************************************************
  * M�todo executado quando o elemento n�o est� preenchido satisfatoriamente.
  *
  * <p>M�todo executado quando o elemento n�o est� preenchido satisfatoriamente.
  * (um dos m�todos necess�rios para customizar o required check).</p>
  *****************************************************************************/
  self.onRequiredCheckNotOk = function(){
    self.updateLabels();
  }
  
 /*****************************************************************************
  * Muda o par�metro selecionado
  *
  * <p>Muda o par�metro selecionado</p>
  * 
  * @param String psParamId Identificador do par�metro a ser selecionado
  *****************************************************************************/
  self.selectParameter = function(psParamId){
    if(self.csSelected){
      gebi(self.csName+'_line_'+self.csSelected).className = self.csSelectedDefaultClass;
      js_hide(self.csName+'_justif_panel_'+self.csSelected);
    }else{
      js_hide(self.csName+'_dummy_panel');
    }
    var moNew = gebi(self.csName+'_line_'+psParamId);
    self.csSelectedDefaultClass = moNew.className;
    moNew.className = self.csSelectedClass;
    self.csSelected = psParamId;
    js_show(self.csName+'_justif_panel_'+psParamId);
  }
  
 /*****************************************************************************
  * Muda o estilo do par�metro sobre o qual o cursor do mouse est�.
  *
  * <p>Muda o estilo do par�metro sobre o qual o cursor do mouse est�.</p>
  * 
  * @param String psParamId Identificador do par�metro
  *****************************************************************************/
  self.mouseOver = function(psParamId){
    if(psParamId!=self.csSelected){
      var moNew = gebi(self.csName+'_line_'+psParamId);
      self.csMouseOver = psParamId;
      self.csMouseOverDefaultClass = moNew.className;
      moNew.className = self.csMouseOverClass;
    }
  }
  
 /*****************************************************************************
  * Seta o valor se allowEmpty.
  *
  * <p>Seta o valor se allowEmpty, que indica se todos os par�metros vazios
  * deve ser considerado um valor v�lido.</p>
  * @param boolean pbAllowEmpty Novo valor de allowEmpty
  *****************************************************************************/
  self.setAllowEmpty = function(pbAllowEmpty){
    self.cbAllowEmpty = pbAllowEmpty;
  }

 /*****************************************************************************
  * Restaura o estilo de um par�metro quando o cursor do mouse sai de cima dele
  *
  * <p>Restaura o estilo de um par�metro quando o cursor do mouse sai de cima
  * dele.</p>
  * 
  * @param String psParamId Identificador do par�metro
  *****************************************************************************/
  self.mouseOut = function(psParamId){
    if(self.csMouseOver==psParamId){
      if(psParamId!=self.csSelected){
        gebi(self.csName+'_line_'+self.csMouseOver).className = self.csMouseOverDefaultClass;
      }else{
        self.csSelectedDefaultClass = self.csMouseOverDefaultClass;
      }
    }
  }
  
  self.setParameterValue = function(psParamId,psValueId){
    var moSelect = gebi(self.csName+'_'+psParamId);
    moSelect.value = psValueId;
    moSelect.className = moSelect.options[moSelect.selectedIndex].className;
    moSelect = psParamId = psValueId = null;
  }
  
};