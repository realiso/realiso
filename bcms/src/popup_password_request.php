<?php
include_once "include.php";

class NewPasswordEvent extends FWDRunnable {
	public function run() {
  	$moWebLib = FWDWebLib::getInstance();
  	if(ISMSLib::getConfigById(GENERAL_EMAIL_ENABLED)){
      $mbFetch = false;
      $msUserLogin = FWDWebLib::getObject('user_login')->getValue();
      $moUser = new ISMSUser();
    	$moUser->createFilter($msUserLogin,'user_email');
    	$moUser->select();
      if (!$moUser->fetch()) {
        $moUser->removeFilters('user_email');
        $moUser->createFilter($msUserLogin, 'user_login');
        $moUser->select();
      } else {
        $mbFetch = true;
      }
    	if ($mbFetch || $moUser->fetch()) {
    		$miUserId = $moUser->getFieldValue('user_id');
        $moUser = new ISMSUser();
        $moPassPolicy = new ISMSPasswordPolicy();
        $msNewPassword = $moPassPolicy->generateRandomPassword();
        $moUser->setFieldValue('user_request_password', md5($msNewPassword));
        //$moUser->setFieldValue('user_must_change_password', true);
        if ($miUserId) {
          $moUser->update($miUserId,false);
          $moUser->fetchById($miUserId);
          $msReceiverEmail = $moUser->getFieldValue('user_email');
          $moEmailPreferences = new ISMSEmailPreferences();
          $msFormat = $moEmailPreferences->getEmailFormat($miUserId);
          $msSystemName = ISMSLib::getSystemName();
          $moMailer = new ISMSMailer();
          $moMailer->setFormat($msFormat);
          $moMailer->setTo($msReceiverEmail);
          $moMailer->setSubject("[$msSystemName] " . date('d/m/Y') . ' - '.FWDLanguage::getPHPStringValue('em_new_password','Nova Senha'));
          $moMailer->addContent('newPassword');
          $moMailer->setParameter('new_password',$msNewPassword);
          $moMailer->addContent('newPasswordRequest');
          $moMailer->send();
          //$moUserPasswordHistory = new ISMSUserPasswordHistory();
          //$moUserPasswordHistory->addPassword(md5($msNewPassword),$miUserId);
        }
    	}
    } else {
      echo "self.close();";
    }
  	echo "gebi('user_login').value=''; js_show('wn_password_sent');";
	}
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new NewPasswordEvent("new_password_event"));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    
    if(!ISMSLib::getConfigById(GENERAL_EMAIL_ENABLED)){ //Se o email estiver desabilitado, n�o pode recuperar a senha.
      FWDWebLib::getObject('password_request_viewgroup')->setShouldDraw(false);
      FWDWebLib::getObject('warning_general_email_disabled')->setAttrDisplay('true');
    }
    
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
    	<script language="javascript">
     		gebi('user_login').focus();	      
	    </script>
	  <?
  }
}

// temos que pegar a linguagem setada no cookie.
$miCookieLanguage = "";
if (isset($_COOKIE['ISMSLANGUAGE'])) {
   	$miCookieLanguage = $_COOKIE['ISMSLANGUAGE'];
   	FWDLanguage::selectLanguage($miCookieLanguage);
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_password_request.xml");
?>