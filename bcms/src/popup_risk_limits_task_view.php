<?php
include_once 'include.php';
include_once $handlers_ref . "grid/QueryGridUserTasks.php";

class ContextEvent extends FWDRunnable {
  public function run(){
    $miTaskId = FWDWebLib::getObject('task_id')->getValue();
   
    //teste para verificar se o sistema n�o est� sendo hakeado
    $moTaskTest = new WKFTask();
    $moTaskTest->fetchById($miTaskId);
    $moTaskTest->testPermissionToExecute($miTaskId);
   
    $msJustification = FWDWebLib::getObject('task_justification')->getValue();
    $miAction = FWDWebLib::getObject('action')->getValue();

    $moContextObject = new ISMSContextObject();
    $moContext = $moContextObject->getContextObjectByContextId($moTaskTest->getFieldValue('task_context_id'));
    $moContext->fetchById($moTaskTest->getFieldValue('task_context_id'));
    $moContext->stateForward($miAction, $msJustification);

    $moTask = new WKFTask();
    $moTask->setFieldValue('task_date_accomplished', ISMSLib::ISMSTime());
    $moTask->setFieldValue('task_is_visible', 0);
    $moTask->update($miTaskId);
    
    $msNextTaskJS = "";
		$mbOpenNextTask = (bool)array_filter(explode(':',FWDWebLib::getObject('next_task_controller')->getValue()));
		if ($mbOpenNextTask) {
		  $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
		  $moHandlerTask = new QueryGridUserTasks(FWDWebLib::getConnection());
      $moHandlerTask->setUser($miUserId);
      $moHandlerTask->setOrderBy('task_date_created','-');
      $moHandlerTask->makeQuery();
      $moHandlerTask->executeQuery();
      if ($moHandlerTask->fetch()) {
  		  $msNextTaskJS = 'soWindow.setTimeout("'.ISMSLib::getTaskPopup($moHandlerTask->getFieldValue('task_id'),
  		                                        $moHandlerTask->getFieldValue('task_activity'),
  		                                        $moHandlerTask->getFieldValue('task_context_id'),
  		                                        $moHandlerTask->getFieldValue('task_context_type'),
  		                                        2).',1");';
      }
		}
		
    echo "soWindow = soPopUpManager.getPopUpById('popup_risk_limits_task_view').getOpener();"
        ."if(soWindow.refresh_grids) soWindow.refresh_grids();"
        .$msNextTaskJS
        ."soPopUpManager.closePopUp('popup_risk_limits_task_view');";
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
    $moStartEvent->addAjaxEvent(new ContextEvent('context_event'));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $miAutoTaskOpen = FWDWebLib::getObject('auto_task_open')->getValue();
    if (!$miAutoTaskOpen) FWDWebLib::getObject('next_task')->setShouldDraw(false);
    if ($miAutoTaskOpen==2) FWDWebLib::getObject('next_task_controller')->checkItem(1);
    
    $miTaskId = FWDWebLib::getObject('task_id')->getValue();

    $moTask = new WKFTask();
    $moTask->fetchById($miTaskId);
    
    //teste para verificar se o sistema n�o est� sendo hakeado
    $moTask->testPermissionToExecute($miTaskId);
    
    $miRiskLimitsId = $moTask->getFieldValue('task_context_id');
    $moRiskLimits = new RMRiskLimits();
    $moRiskLimits->fetchById($miRiskLimitsId);
    
    $moRiskLimits->fillFormFromFields();
    
    FWDWebLib::getObject('task_date')->setValue(ISMSLib::getISMSDate($moTask->getFieldValue('task_date_created')));

    $moUser = new ISMSUser();
    $moUser->fetchById($moTask->getFieldValue('task_creator_id'));
    FWDWebLib::getObject('task_inquirer')->setValue($moUser->getFieldValue('user_name'));

    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_risk_limits_task_view.xml');

?>