<?php
include_once "include.php";

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
  }
}


class ScreenEvent extends FWDRunnable {
  public function run(){
    $miContextId = FWDWebLib::getObject("param_context_id")->getValue();
    $miContextType = FWDWebLib::getObject("param_context_type")->getValue();
    $msUniqId = FWDWebLib::getObject("param_uniqid")->getValue();
    
    FWDWebLib::getObject("var_uniqid")->setValue($msUniqId);
    
    $moButtonEdit = FWDWebLib::getObject('vb_edit');
    
    $moIcon = FWDWebLib::getObject("tooltip_icon");
    $moToolTip = ISMSLib::getInstance()->getToolTipCreateModifyById($miContextId);
    $moIcon->addObjFWDEvent($moToolTip);
    
    $moContextObject = new ISMSContextObject();
    $moContext = $moContextObject->getContextObject($miContextType);
    $moContext->fetchById($miContextId);
    
    $msTitle = FWDLanguage::getPHPStringValue('tt_visualize','Visualizar') ." ". $moContext->getLabel();
    $msEditEvent = $moContext->getVisualizeEditEvent($miContextId,$msUniqId);
    
    if($moContext->userCanEdit($miContextId,$moContext->getResponsible())){
      $moEventEdit = new FWDClientEvent();
      $moEventEdit->setAttrEvent('onClick');
      
      $moEventEdit->setAttrValue($msEditEvent);
      
      FWDWebLib::getObject('vb_edit')->addObjFWDEvent($moEventEdit);
    }else{
      $moButtonEdit->setShouldDraw(false);
    }
    
    FWDWebLib::getObject("visualize_title")->setValue($msTitle);
    FWDWebLib::getObject("visualize_name")->setValue($moContext->getName());
    FWDWebLib::getObject("visualize_description")->setValue($moContext->getDescription());
    
    $moUser = new ISMSUser();
    $moUser->fetchById($moContext->getResponsible());
    
    if ($moUser->getName())    
    	FWDWebLib::getObject("visualize_responsible")->setValue($moUser->getName());
    else {
    	/*
    	 * Necess�rio para os casos em que o respons�vel � o 'Realiso Online'
    	 * ou respons�vel pelo contexto foi deletado, ou seja, os casos em
    	 * que o campo nUserId de isms_context_date = 0.
    	 */
    	$moContextDate = new ISMSContextDate();
    	$moContextDate->createFilter($miContextId, "context_id");
    	$moContextDate->createFilter(ACTION_EDIT, "action");
    	$moContextDate->select();
    	$moContextDate->fetch();
    	$msUserName = $moContextDate->getFieldValue('user_name');
    	FWDWebLib::getObject("visualize_responsible")->setValue($msUserName);
    }
    
    $moEventClose = new FWDClientEvent();
    $moEventClose->setAttrEvent('onClick');
    $moEventClose->setAttrValue("soPopUpManager.closePopUp('popup_visualize_$msUniqId');");
    FWDWebLib::getObject('vb_close')->addObjFWDEvent($moEventClose);
   
 		//instala a seguran�a de ACL na p�gina
		FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
?>
<script language="javascript">
  function refresh_grid() {
    if (soPopUpManager.getPopUpById('popup_visualize_'+gebi('var_uniqid').value).getOpener().refresh_grid)
      soPopUpManager.getPopUpById('popup_visualize_'+gebi('var_uniqid').value).getOpener().refresh_grid();
  }
  <?=$moContext->getVisualizeJavascript();?>
</script>
<?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("visualize.xml");
?>