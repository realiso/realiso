<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

  include_once('include.php');
  include_once($handlers_ref.'QueryDigestUsers.php');
  include_once($handlers_ref.'QueryDigestAlerts.php');
  include_once($handlers_ref.'QueryDigestTasks.php');

  $soWebLib = FWDWebLib::getInstance();  
  
  $msURL = $soWebLib->getCurrentURL();
  $msLoginURL = substr($msURL,0,strrpos($msURL,'/')+1).'default.php';

  // Testa se emails e digests est�o habilitados
  if(ISMSLib::getConfigById(GENERAL_EMAIL_ENABLED) && ISMSLib::getConfigById(GENERAL_ALLOW_DIGESTS)){
    // Testa se j� passou a hora de executar, desde a �ltima execu��o
    $msDigestLastTime = ISMSLib::getConfigById(GENERAL_DIGEST_LAST_TIME);
    $msDigestTime = ISMSLib::getConfigById(GENERAL_DIGEST_TIME);
    $msNow = date('Y-m-d H:i:s');
    
    if($msDigestLastTime==''){
      $mbExecute = true;
    }else{
      list($msLastDay,$msLastTime) = explode(' ',$msDigestLastTime);
      list($msThisDay,$msThisTime) = explode(' ',$msNow);
      if(strtotime($msNow) - strtotime($msDigestLastTime) >= 86400){
        $mbExecute = true;
      }elseif($msLastDay==$msThisDay){
        $mbExecute = ($msLastTime<=$msDigestTime && $msDigestTime<=$msThisTime);
      }else{
        $mbExecute = ($msLastTime<=$msDigestTime || $msDigestTime<=$msThisTime);
      }
    }
    
  }else{
    $mbExecute = false;
  }
  // Se vai executar, atualiza a hora da �ltima execu��o
  if($mbExecute){
    ISMSLib::setConfigById(GENERAL_DIGEST_LAST_TIME,$msNow);
  }else{
    exit('You shall not execute!');
  }
  
  $moQueryUsers = new QueryDigestUsers(FWDWebLib::getConnection());
  $moQueryUsers->makeQuery();
  $moQueryUsers->executeQuery();
  
  $moUsersDataset = $moQueryUsers->getDataset();
  $moFieldUserId = $moUsersDataset->getFieldByAlias('user_id');
  $moFieldUserEmail = $moUsersDataset->getFieldByAlias('user_email');
  
  $moQueryAlerts = new QueryDigestAlerts(FWDWebLib::getConnection());
  $moQueryTasks = new QueryDigestTasks(FWDWebLib::getConnection());
  
  $moContextObject = new ISMSContextObject();
  
  $moUpdateTasksDataset = new FWDDBDataSet(FWDWebLib::getConnection(),'wkf_task');
  $moUpdateTasksDataset->addFWDDBField(new FWDDBField('pkTask',    'task_id',        DB_NUMBER));
  $moUpdateTasksDataset->addFWDDBField(new FWDDBField('bEmailSent','task_email_sent',DB_NUMBER));
  $moUpdateTasksDataset->getFieldByAlias('task_email_sent')->setValue(1);
  $moUpdateFieldTaskId = $moUpdateTasksDataset->getFieldByAlias('task_id');
  
  $moUpdateAlertsDataset = new FWDDBDataSet(FWDWebLib::getConnection(),'wkf_alert');
  $moUpdateAlertsDataset->addFWDDBField(new FWDDBField('pkAlert',   'alert_id',        DB_NUMBER));
  $moUpdateAlertsDataset->addFWDDBField(new FWDDBField('bEmailSent','alert_email_sent',DB_NUMBER));
  $moUpdateAlertsDataset->getFieldByAlias('alert_email_sent')->setValue(1);
  $moUpdateFieldAlertId = $moUpdateAlertsDataset->getFieldByAlias('alert_id');
  
  $moAlert = new WKFAlert();
  
  $moMailer = new ISMSMailer();
  
  while($moUsersDataset->fetch()){
    $miUserId = $moFieldUserId->getValue();    
    $moEmailPreferences = new ISMSEmailPreferences();
    $msDigestType = $moEmailPreferences->getDigestType($miUserId);
    $msEmailFormat = $moEmailPreferences->getEmailFormat($miUserId);
    $msSystemName = ISMSLib::getSystemName();
    if($msDigestType!='none'){
    
      $moQueryAlerts->setDigestType($msDigestType);
      $moQueryAlerts->setUserId($miUserId);
      $moQueryAlerts->makeQuery();
      $miAlerts = $moQueryAlerts->executeQuery();
      
      $moQueryTasks->setDigestType($msDigestType);
      $moQueryTasks->setUserId($miUserId);
      $moQueryTasks->makeQuery();
      $miTasks = $moQueryTasks->executeQuery();
      
      $moTaskDataset = $moQueryTasks->getDataset();
      $moFieldTaskId = $moTaskDataset->getFieldByAlias('task_id');
      $moFieldTaskActivity = $moTaskDataset->getFieldByAlias('task_activity');
      
      $moAlertDataset = $moQueryAlerts->getDataset();
      
      $moFieldAlertId = $moAlertDataset->getFieldByAlias('alert_id');
      $moFieldAlertType = $moAlertDataset->getFieldByAlias('alert_context_type');
      $moFieldAlertContextId = $moAlertDataset->getFieldByAlias('alert_context_id');
      $moFieldAlertContextName = $moAlertDataset->getFieldByAlias('alert_context_name');
      $moFieldAlertStatus = $moAlertDataset->getFieldByAlias('alert_type');
      
      if($miAlerts>0 || $miTasks>0){
        $maAlertsIds = array();
        $maTasksIds = array();
        $moMailer->newEmail();
        $moMailer->setFormat($msEmailFormat);
        $moMailer->setTo($moFieldUserEmail->getValue());
        $moMailer->setSubject("[$msSystemName] " . date('d/m/Y') . ' - '.FWDLanguage::getPHPStringValue('em_daily_digest','Digest di�rio'));
        if($miTasks>0){
          $moMailer->addContent('taskHeader');
          while($moTaskDataset->fetch()){
            $miTaskId = $moFieldTaskId->getValue();
            $maTasksIds[] = $miTaskId;
            $miTaskActivity = $moFieldTaskActivity->getValue();
            
            $moMailer->addContent('separator');
            
            if($msDigestType=='full'){

              $miContextId = $moTaskDataset->getFieldByAlias('task_context_id')->getValue();
              $moContext = $moContextObject->getContextObjectByContextId($miContextId);
              if($moContext instanceof IAssociation){
                $moContext->fetchById($miContextId);
                $moFirstContext = $moContextObject->getContextObjectByContextId($moContext->getFirstContextId());
                $moSecondContext = $moContextObject->getContextObjectByContextId($moContext->getSecondContextId());
                
                $moFirstContext->fetchById($moContext->getFirstContextId());
                $msContextLabel1 = $moFirstContext->getLabel();
                $msContextName1 = $moFirstContext->getName();
                $msContextDescription1 = $moFirstContext->getDescription();
                
                $moSecondContext->fetchById($moContext->getSecondContextId());
                $msContextLabel2 = $moSecondContext->getLabel();
                $msContextName2 = $moSecondContext->getName();
                $msContextDescription2 = $moSecondContext->getDescription();
                
                $moMailer->addContent('association');
                $moMailer->setParameter('contextType1',FWDWebLib::convertToISO($msContextLabel1));
                $moMailer->setParameter('name1',$msContextName1);
                $moMailer->setParameter('description1',$msContextDescription1);
                $moMailer->setParameter('contextType2',FWDWebLib::convertToISO($msContextLabel2));
                $moMailer->setParameter('name2',$msContextName2);
                $moMailer->setParameter('description2',$msContextDescription2);
                
              }elseif($moContext->getName()){
                $moContext->fetchById($miContextId);
                $msContextLabel = $moContext->getLabel();
                $msContextName = $moContext->getName();
                $msContextDescription = $moContext->getDescription();
                
                $moMailer->addContent('task');
                $moMailer->setParameter('contextType',FWDWebLib::convertToISO($msContextLabel));
                $moMailer->setParameter('name',$msContextName);
                $moMailer->setParameter('description',$msContextDescription);
              }else{
                $moMailer->addContent('taskWithoutContext');
              }
              
              $msTaskSender = $moTaskDataset->getFieldByAlias('task_sender')->getValue();
              $msTaskDate = ISMSLib::getISMSDate($moTaskDataset->getFieldByAlias('task_date')->getValue());
              $moMailer->setParameter('sender',$msTaskSender);
              $moMailer->setParameter('date',$msTaskDate);

            }else{
              $moMailer->addContent('taskSubject');
            }
            $moMailer->setParameter('activity',ISMSActivity::getDescription($miTaskActivity));
            $moMailer->setParameter('link',"$msLoginURL?task=$miTaskId");
          }
          $moUpdateFieldTaskId->addFilter(new FWDDBFilter('in',$maTasksIds));
          $moUpdateTasksDataset->update();
          $moUpdateFieldTaskId->clearFilters();
        }
        if($miAlerts>0){
          $moMailer->addContent('alertHeader');
          while($moAlertDataset->fetch()){
            $maAlertsIds[] = $moFieldAlertId->getValue();
            $moMailer->addContent('separator');
          
            $miAlertStatus = $moFieldAlertStatus->getValue();
            $miAlertType = $moFieldAlertType->getValue();
            $miAlertContextId = $moFieldAlertContextId->getValue();
            $msAlertContextName = $moFieldAlertContextName->getValue();
            $msAlertDescription = $moAlert->getDescription($miAlertStatus, $miAlertType, $msAlertContextName, $miAlertContextId);
            
            if($msDigestType=='full'){
              $msAlertJustification = trim($moAlertDataset->getFieldByAlias('alert_justification')->getValue());
              if($msAlertJustification!=''){
                $moMailer->addContent('alertWithJustification');
                $moMailer->setParameter('justification',$msAlertJustification);
              }else{
                $moMailer->addContent('alert');
              }
              $msAlertSender = $moAlertDataset->getFieldByAlias('alert_sender')->getValue();
              $msAlertDate = ISMSLib::getISMSDate($moAlertDataset->getFieldByAlias('alert_date')->getValue());
              $moMailer->setParameter('sender',$msAlertSender);
              $moMailer->setParameter('date',$msAlertDate);
            }else{
              $moMailer->addContent('alertSubject');
            }
            $moMailer->setParameter('description',$msAlertDescription);
          }
          $moUpdateFieldAlertId->addFilter(new FWDDBFilter('in',$maAlertsIds));
          $moUpdateAlertsDataset->update();
          $moUpdateFieldAlertId->clearFilters();
        }
        $moMailer->send();
      }
    }
    
  }

?>