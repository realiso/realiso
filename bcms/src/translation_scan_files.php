<?php
set_time_limit(3000);

require_once('include.php');
require_once("$base_ref/translation/include.php");

// id do sistema da tabela de 'lic_system'
$miSystemId = 53;

// Inicializa o DataSet pro driver de BD
$soDB = new FWDDB(DB_POSTGRES, "bcms_tools", "postgres", "informant", "127.0.0.1");
$soDataSet = new FWDDBDataSet($soDB, 'translation_strings');
$soDataSet->addFWDDBField(new FWDDBField('skSystem','string_system_id',DB_NUMBER,$miSystemId));
$soDataSet->addFWDDBField(new FWDDBField('zId','string_id',DB_STRING));
$soDataSet->addFWDDBField(new FWDDBField('zFile','string_file',DB_STRING));
$soDataSet->addFWDDBField(new FWDDBField('zValue','string_value',DB_STRING));
$soDataSet->addFWDDBField(new FWDDBField('dDateInclusion','string_date_inclusion',DB_DATETIME));
$soDataSet->addFWDDBField(new FWDDBField('zPortuguesIsms','string_portugues_isms', DB_STRING));
$soDataSet->addFWDDBField(new FWDDBField('zSpanishIsms','string_spanish_isms', DB_STRING));
$soDataSet->addFWDDBField(new FWDDBField('zEnglishIsms','string_english_isms', DB_STRING));
$soDataSet->addFWDDBField(new FWDDBField('zChineseIsms','string_chinese_isms', DB_STRING));
$soDataSet->addFWDDBField(new FWDDBField('zPortuguesEms','string_portuguese_ems', DB_STRING));
$soDataSet->addFWDDBField(new FWDDBField('zPortuguesOhs','string_portuguese_ohs', DB_STRING));
$soDataSet->addFWDDBField(new FWDDBField('zEnglishEms','string_english_ems', DB_STRING));
$soDataSet->addFWDDBField(new FWDDBField('zEnglishOhs','string_english_ohs', DB_STRING));
$soDataSet->addFWDDBField(new FWDDBField('zEnglishSox','string_english_sox', DB_STRING));
$soDataSet->addFWDDBField(new FWDDBField('zEnglishPci','string_english_pci', DB_STRING));
$soDataSet->addFWDDBField(new FWDDBField('zGermanSox','string_german_sox', DB_STRING));
$soDataSet->addFWDDBField(new FWDDBField('zFrenchSox','string_french_sox', DB_STRING));

$soDataSet->getFieldByAlias('string_system_id')->addFilter(new FWDDBFilter("=", $miSystemId));

$soDataSet->setOrderBy('string_file', '+');
$soDataSet->setOrderBy('string_id', '+');

//carrega as strings extraidas dos arquivos xml e php e insere elas no banco do sistema de tradução da axur
$miTimeStampLocal = microtime(1);
echo "<h2>Transferência do XML para o Banco";

$soDBDriverXML = new FWDDBDriver($soDataSet);
$soTranslateXML = new FWDTranslate($soDBDriverXML);
$soTranslateXML->extractXMLStrings('.',true);
echo ' em ' . number_format(microtime(1) - $miTimeStampLocal,2) . ' segundo(s).</h2>';

$miTimeStampLocal = microtime(1);
echo "<br/><h2>Transferência do PHP para o Banco";

$soDBDriverPHP = new FWDDBDriver($soDataSet,true);
$soTranslatePHP = new FWDTranslate($soDBDriverPHP);
$soTranslatePHP->extractPHPStrings('.',true);
echo ' em ' . number_format(microtime(1) - $miTimeStampLocal,2) . ' segundo(s).</h2>';
?>