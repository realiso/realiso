<?php
include_once "include.php";
include_once $handlers_ref . "grid/QueryGridUserTasks.php";
include_once $handlers_ref . "grid/QueryGridUserAlerts.php";

function getArrayGrids(){  
  $moConfig = new ISMSConfig();
  $mbHasRMModule = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(RISK_MANAGEMENT_MODE);

  $maGrids = array();
  if ($mbHasRMModule) {
    $maGrids['grid_resume_summary'       		] = array('label'=>FWDLanguage::getPHPStringValue('quantity'        				,'Quantidades'));
    $maGrids['grid_top10qualitative'       		] = array('label'=>FWDLanguage::getPHPStringValue('top10qualitative'        				,'Top 10 Qualitativo'));
  }
  
  return $maGrids;
}
function getArrayGridsSimple(){  
  $moConfig = new ISMSConfig();
  $mbHasRMModule = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(RISK_MANAGEMENT_MODE);
  $maDeniedACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
  if($mbHasRMModule){
    if(in_array('M.RM', $maDeniedACLs)){
      $mbHasRMModule = false;
    }
  }
  $maGrids = array();
  if ($mbHasRMModule) {
    $maGrids['grid_my_residual_risk'  ] = array('label'=>FWDLanguage::getPHPStringValue('tt_my_risk_summary' ,'Meu Sum�rio de Risco'));
    $maGrids['grid_my_pendencies'     ] = array('label'=>FWDLanguage::getPHPStringValue('tt_my_pendencies'    ,'Minhas Pend�ncias da Gest�o dos Riscos'));
    $maGrids['grid_my_elements'       ] = array('label'=>FWDLanguage::getPHPStringValue('tt_my_items'      ,'Meus Elementos'));
  }
  return $maGrids;
}
class SaveEvent extends FWDRunnable {
  public function run(){
    $maValuesSelect = explode(':',FWDWebLib::getObject('dashboard_pref_grids_values')->getValue());
    if(FWDWebLib::getObject('dashboard_type')->getValue()){
      $maGridInfo = getArrayGridsSimple();
    }else{
      $maGridInfo = getArrayGrids();
    }
    
    $maPreference = array();
    foreach($maValuesSelect as $msValue){
      if(isset($maGridInfo[$msValue])){
        $maPreference[] = $msValue;
      }
    }
    if(count($maPreference)==0){
      $maPreference[] = "grid_dummy";
    }
    $moPreference = new ISMSUserPreferences();
    if(FWDWebLib::getObject('dashboard_type')->getValue())
      $moPreference->setPreference("dashboard_sumary_pref_basic",serialize($maPreference));
    else
      $moPreference->setPreference("dashboard_sumary_pref",serialize($maPreference));
    
    echo "soWindow = soPopUpManager.getPopUpById('popup_user_dashboard_preferences').getOpener();"
        ."if(soWindow.refresh) soWindow.refresh();"
        ."soPopUpManager.closePopUp('popup_user_dashboard_preferences');";
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new SaveEvent("save_event"));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    /*setup da tela*/
    $moSelAll = FWDWebLib::getObject('user_dashboard_sel_all');
    $moSelSelected = FWDWebLib::getObject('user_dashboard_sel_selected');
    $maPrefUserKeys = array();
    $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
    $mbSimple = false;
    if(in_array('M.D.2',$maACLs)){
      $mbSimple = true;
    }
    
    FWDWebLib::getObject('dashboard_type')->setValue($mbSimple);
    $moPreference = new ISMSUserPreferences();
    if(FWDWebLib::getObject('dashboard_type')->getValue()){
      if($moPreference->preferenceUserExistes("dashboard_sumary_pref_basic")){
        $maPrefUser = FWDWebLib::unserializeString($moPreference->getPreference("dashboard_sumary_pref_basic"));
        if(is_array($maPrefUser)){
          $maPrefUserKeys = array_flip($maPrefUser);
        } 
      }else{
        $maPrefUserKeys = getArrayGridsSimple();
      }
      $maGridInfo = getArrayGridsSimple();
    }else{
      if($moPreference->preferenceUserExistes("dashboard_sumary_pref")){
        $maPrefUser = FWDWebLib::unserializeString($moPreference->getPreference("dashboard_sumary_pref"));
        if(is_array($maPrefUser)){
          $maPrefUserKeys = array_flip($maPrefUser);
        } 
      }else{
        $maPrefUserKeys = getArrayGrids();
      }
      $maGridInfo = getArrayGrids();
    }  
    /*constroi os objetos da tela*/
    foreach($maGridInfo as $miKey=>$maValues){
      $moItem = new FWDItem();
      $moItem->setAttrKey($miKey);
      $moItem->setValue($maValues['label']);
      if(!isset($maPrefUserKeys[$miKey])){
        $moSelAll->addObjFWDItem($moItem);
      }
    }
    foreach($maPrefUserKeys as $miKey=>$maValues){
      if(isset($maGridInfo[$miKey])){
        $moItem = new FWDItem();
        $moItem->setAttrKey($miKey);
        $moItem->setValue($maGridInfo[$miKey]['label']);
        $moSelSelected->addObjFWDItem($moItem);
      }
    }

    if((in_array('M.D.1',$maACLs))&&(in_array('M.D.2',$maACLs))){
      trigger_error("You do not have permission to change dashboard configuration!",E_USER_ERROR);
    }
    //instala a seguran�a de ACL na p�gina
    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        var soListEditor  = new FWDListEditor('user_dashboard_sel');
        
      </script>
    <?

  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_user_dashboard_preference.xml");
?>