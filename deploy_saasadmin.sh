#! /bin/bash
#
# All bugs added by jonathan pirovano
# 

echo "=============================================================="
read -p "Selecione o ambiente (homolog,prod) [ENTER]:" ambiente;
if [ "$ambiente" == "prod" ]; then
  echo "Efetuando deploy no ambiente de PRODUÇÃO."
elif [ "$ambiente" == "homolog" ]; then
  echo "Efetuando deploy no ambiente de HOMOLOGAÇÃO."
else
  echo "Cancelado, ambiente não suportado."
  exit 1
fi

echo "#0 Excluindo e criando novas pastas para deploy."

rm new_deploy_saasadmin.tar.gz
rm -rf new_deploy_saas_admin
mkdir new_deploy_saas_admin
chmod 777 new_deploy_saas_admin

#Fwd5 Primeira vez 
retorno=$(wget -q -O- localhost/FWD/src/compile/fwd5_php_compile.php)
if echo $retorno | grep -q "fwd5_setup.php created!" ; then
	echo "#1 fwd5_setup OK."
else
	echo "#1 fwd5_setup FALHOU."
	exit 1
fi

#Fwd5 Segunda vez (É preciso compilar duas vezes para evitar erros do framework) 
retorno=$(wget -q -O- localhost/FWD/src/compile/fwd5_php_compile.php)
if echo $retorno | grep -q "fwd5_setup.php created!" ; then
	echo "#2 fwd5_setup OK."
else
	echo "#2 fwd5_setup FALHOU."
	exit 1
fi

#retorno autenticação, é necessário para as próximas compilações. 



retorno=$(wget -q -O- --cookies=on --keep-session-cookies --save-cookies=cookie.txt --post-data 'fwd_ajax_trigger=1&fwd_ajax_event_name=login_event&user_login=admin&user_password=admin&user_language=3301' localhost/saasadmin/login.php)

#fwd_ajax_trigger:1
#fwd_ajax_event_name:login_event
#user_login:admin
#user_password:admin

if echo $retorno | grep -q "default.php" ; then
	echo "#3 login OK."
else
	echo "#3 login FALHOU."
	echo $retorno
	exit 1
fi

#Compilação do xml
retorno=$(wget -q -O- --load-cookies=cookie.txt localhost/saasadmin/saas_xml_compile.php)

if echo $retorno | grep -q "session_expired.php" ; then
	echo "#4 saas_xml_compile FALHOU, session_expired."
	exit 1
fi

#Verifica o retorno da compilação.
if echo $retorno | grep -q "forms.xmc SUCCESSFULLY CREATED!" ; then
	echo "#4 saas_xml_compile OK."
else
	echo "#4 saas_xml_compile FALHOU."
	echo $retorno
	exit 1
fi


#Compilação do xml
retorno=$(wget -q -O- --load-cookies=cookie.txt localhost/saasadmin/compile/saas_php_compile.php)

#Verifica o retorno da compilação.
if echo $retorno | grep -q "calendar.js'...OK" ; then
	echo "#5 saas_php_compile OK."
else
	echo "#5 saas_php_compile FALHOU."
	echo $retorno
	exit 1
fi

echo " Compactando arquivos do deploy. "
tar czf new_deploy_saasadmin.tar.gz new_deploy_saas_admin/

if [ "$ambiente" == "prod" ]; then
	
	echo "Efetuando deploy no ambiente de PRODUÇÃO."
	scp new_deploy_saasadmin.tar.gz ubuntu@saas.realiso.com:/tmp/

elif [ "$ambiente" == "homolog" ]; then
  	
  	echo "Efetuando deploy no ambiente de HOMOLOGAÇÃO."
	scp new_deploy_saasadmin.tar.gz ubuntu@homolog.realiso.com:/tmp/

fi

echo "Excluindo arquivos temporários."
	
rm cookie.txt
rm new_deploy_saasadmin.tar.gz

echo " Deploy efetuado com sucesso. "
echo "=============================================================="
