#! /bin/bash

echo "================================================"
echo "Changing instance user password..."

read -p "Enter the instance name and press [ENTER]:" instancename;
if [ "$instancename" == "" ]; then
  echo "Aborted, Intance name not informed."
  exit 1
fi

read -p "Enter the user to change the password and press [ENTER]:" username;
if [ "$username" == "" ]; then
  echo "Aborted, User not informed.";
  exit 1;
fi

TEST=$(psql -h localhost -P t -P format=unaligned inst_$instancename -c "select max(sname) from isms_user where slogin='$username' ")

if [ "$TEST" == "" ]; then
  echo "Aborted, User informed does not exist.";
  exit 1
fi

read -p "Enter the new password for the user '$TEST' [ENTER]:" newpass;
if [ "$newpass" == "" ]; then
  echo "Aborted, password not informed.";
  exit 1
fi

RETURNED=$(psql -h localhost -P t -P format=unaligned inst_$instancename -c "UPDATE isms_user SET spassword=md5('$newpass') WHERE slogin='$username' ")

if [ "$RETURNED" == "UPDATE 0" ]; then
  echo "Password not updated."
fi

if [ "$RETURNED" == "UPDATE 1" ]; then
  echo "Password changed successfully for the user '$TEST'."
fi

echo "================================================"
exit 0