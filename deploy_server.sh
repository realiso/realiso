#! /bin/bash
#
# All bugs added by jonathan pirovano
#

echo "Efetuando deploy REALISMS..."

read -p "Informe o nro da liberaçao [ENTER]:" nro;
if [ "$nro" == "" ]; then
  echo "Cancelado, informe o nro da liberação."
  exit 1
fi

sudo mv /tmp/new_deploy_isms.tar.gz /storage/realiso/isms/skel/_sources/
cd /storage/realiso/isms/skel/_sources/

echo "Descompactando arquivos,.."
tar -zxvf new_deploy_isms.tar.gz

echo "Renomeando pasta,..."
mv new_deploy_isms $nro

echo "Alterando permissões,.."
chown root:root -R $nro
chmod 755 -R $nro

echo "Deploy efetuado com sucesso!"