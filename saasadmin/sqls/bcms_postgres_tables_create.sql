
CREATE TABLE ci_action_plan (
    fkcontext integer NOT NULL,
    fkresponsible integer,
    sname character varying(256) NOT NULL,
    tactionplan text,
    nactiontype integer DEFAULT 0,
    ddatedeadline timestamp without time zone,
    ddateconclusion timestamp without time zone,
    bisefficient integer DEFAULT 0,
    ddateefficiencyrevision timestamp without time zone,
    ddateefficiencymeasured timestamp without time zone,
    ndaysbefore integer DEFAULT 0,
    bflagrevisionalert integer DEFAULT 0,
    bflagrevisionalertlate integer DEFAULT 0,
    sdocument character varying(256),
    CONSTRAINT ckc_bflagrevisionaler_ci_actio CHECK (((bflagrevisionalertlate IS NULL) OR ((bflagrevisionalertlate >= 0) AND (bflagrevisionalertlate <= 1)))),
    CONSTRAINT ckc_bflagrevisionaler_ci_actio2 CHECK (((bflagrevisionalert IS NULL) OR ((bflagrevisionalert >= 0) AND (bflagrevisionalert <= 1)))),
    CONSTRAINT ckc_bisefficient_ci_actio CHECK (((bisefficient IS NULL) OR ((bisefficient >= 0) AND (bisefficient <= 1))))
);
CREATE TABLE ci_category (
    fkcontext integer NOT NULL,
    sname character varying(256) NOT NULL
);
COMMENT ON TABLE ci_category IS 'Categoria de incidentes.';
CREATE TABLE ci_incident (
    fkcontext integer NOT NULL,
    fkcategory integer,
    fkresponsible integer,
    sname character varying(256) NOT NULL,
    taccountsplan text,
    tevidences text,
    nlosstype integer DEFAULT 0,
    tevidencerequirementcomment text,
    ddatelimit timestamp without time zone,
    ddatefinish timestamp without time zone,
    tdisposaldescription text,
    tsolutiondescription text,
    tproductservice text,
    bnotemaildp integer DEFAULT 0,
    bemaildpsent integer DEFAULT 0,
    ddate timestamp without time zone NOT NULL,
    CONSTRAINT ckc_bemaildpsent_ci_incid CHECK (((bemaildpsent IS NULL) OR ((bemaildpsent >= 0) AND (bemaildpsent <= 1)))),
    CONSTRAINT ckc_bnotemaildp_ci_incid CHECK (((bnotemaildp IS NULL) OR ((bnotemaildp >= 0) AND (bnotemaildp <= 1))))
);
COMMENT ON TABLE ci_incident IS 'Tabela de incidentes.';
CREATE TABLE ci_incident_control (
    fkincident integer NOT NULL,
    fkcontrol integer NOT NULL,
    fkcontext integer NOT NULL
);
COMMENT ON TABLE ci_incident_control IS 'Tabela que relaciona incidentes e controles.';
CREATE TABLE ci_incident_financial_impact (
    fkincident integer NOT NULL,
    fkclassification integer NOT NULL,
    nvalue double precision DEFAULT (0)::double precision
);
COMMENT ON TABLE ci_incident_financial_impact IS 'Tabela de impacto financeiro de um incidente.';
CREATE TABLE ci_incident_process (
    fkprocess integer NOT NULL,
    fkincident integer NOT NULL,
    fkcontext integer NOT NULL
);
COMMENT ON TABLE ci_incident_process IS 'Tabela que relaciona incidentes e processos.';
CREATE TABLE ci_incident_risk (
    fkcontext integer NOT NULL,
    fkrisk integer NOT NULL,
    fkincident integer NOT NULL
);
CREATE TABLE ci_incident_risk_parameter (
    fkcontext integer,
    fkincident integer,
    fkparametername integer,
    fkparametervalue integer
);
CREATE TABLE ci_incident_risk_value (
    fkparametername integer NOT NULL,
    fkvaluename integer NOT NULL,
    fkincrisk integer NOT NULL
);
COMMENT ON TABLE ci_incident_risk_value IS 'Tabela que relaciona incidentes e riscos.';
CREATE TABLE ci_incident_user (
    fkuser integer NOT NULL,
    fkincident integer NOT NULL,
    tdescription text NOT NULL,
    tactiontaken text
);
COMMENT ON TABLE ci_incident_user IS 'Tabela de relacionamento entre  incidentes e usuários.';
CREATE TABLE ci_nc (
    fkcontext integer NOT NULL,
    fkcontrol integer,
    fkresponsible integer,
    fksender integer NOT NULL,
    sname character varying(256) NOT NULL,
    nseqnumber integer NOT NULL,
    tdescription text,
    tcause text,
    tdenialjustification text,
    nclassification integer DEFAULT 0,
    ddatesent timestamp without time zone,
    ncapability integer DEFAULT 0
);
CREATE TABLE ci_nc_action_plan (
    fknc integer NOT NULL,
    fkactionplan integer NOT NULL
);
COMMENT ON TABLE ci_nc_action_plan IS 'Relaciona não conformidades com planos de ação.';
CREATE SEQUENCE ci_nc_nseqnumber_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
ALTER SEQUENCE ci_nc_nseqnumber_seq OWNED BY ci_nc.nseqnumber;
CREATE TABLE ci_nc_process (
    fknc integer NOT NULL,
    fkprocess integer NOT NULL
);
COMMENT ON TABLE ci_nc_process IS 'Tabela que relaciona não conformidades com processos.';
CREATE TABLE ci_nc_seed (
    fkcontrol integer NOT NULL,
    ndeactivationreason integer DEFAULT 0 NOT NULL,
    ddatesent timestamp without time zone
);
CREATE TABLE ci_occurrence (
    fkcontext integer NOT NULL,
    fkincident integer,
    tdescription text NOT NULL,
    tdenialjustification text,
    ddate timestamp without time zone NOT NULL
);
COMMENT ON TABLE ci_occurrence IS 'Tabela de ocorrências de incidentes.';
CREATE TABLE ci_risk_probability (
    fkrisk integer NOT NULL,
    fkvaluename integer NOT NULL,
    nincidentamount integer DEFAULT 0 NOT NULL
);
CREATE TABLE ci_risk_schedule (
    fkrisk integer NOT NULL,
    nperiod integer DEFAULT 0 NOT NULL,
    nvalue integer DEFAULT 0 NOT NULL,
    ddatelastcheck timestamp without time zone
);
CREATE TABLE ci_solution (
    fkcontext integer NOT NULL,
    fkcategory integer,
    tproblem text,
    tsolution text,
    tkeywords text
);
COMMENT ON TABLE ci_solution IS 'Tabela de soluções para incidentes.';
SET default_with_oids = false;
CREATE TABLE cm_area_resource (
    fkcontext integer,
    fkarea integer,
    fkresource integer,
    fkgroup integer,
    cmt integer
);
SET default_with_oids = true;
CREATE TABLE cm_asset_category (
    fkcontext integer NOT NULL,
    fkparent integer,
    sname character varying(256) NOT NULL,
    tdescription text
);
CREATE TABLE cm_asset_category_threat (
    fkcontext integer NOT NULL,
    fkassetcategory integer NOT NULL,
    fkthreat integer NOT NULL
);
CREATE TABLE cm_asset_importance (
    fkcontext integer,
    sname text,
    nimportance integer
);
CREATE TABLE cm_asset_threat (
    fkcontext integer,
    fkasset integer,
    fkthreat integer
);
CREATE TABLE cm_damage_matrix (
    fkcontext integer,
    sname text
);
CREATE TABLE cm_damage_matrix_parameter (
    fkcontext integer,
    sname text,
    nweight integer
);
CREATE TABLE cm_department (
    fkcontext integer,
    name text,
    description text
);
CREATE TABLE cm_financial_impact (
    fkcontext integer,
    sname text,
    nstartrange integer,
    nendrange integer
);
SET default_with_oids = false;
CREATE TABLE cm_group (
    fkcontext integer,
    name text,
    acronym text,
    fkpriority integer
);
CREATE TABLE cm_group_resource (
    fkcontext integer,
    fkgroup integer,
    fkresource integer,
    escalation integer
);
CREATE TABLE cm_impact (
    fkcontext integer,
    sname text,
    ntype integer
);
CREATE TABLE cm_impact_damage (
    fkcontext integer,
    fkimpact integer,
    fkscene integer
);
CREATE TABLE cm_impact_damage_range (
    fkcontext integer,
    fkdamage integer,
    fkdamageparameter integer,
    sfinancialimpact text,
    financialimpact integer,
    fkimpactdamage integer
);
SET default_with_oids = true;
CREATE TABLE cm_impact_type (
    fkcontext integer,
    sname text,
    sdescription text,
    nweight integer
);
CREATE TABLE cm_place (
    fkcontext integer,
    sname text,
    sdescription text,
    fktype integer,
    type text,
    zipcode integer,
    scordinate text,
    saddress text,
    fkgroup integer,
    fkgroupsubstitute integer,
    fkresource integer,
    fkresourcesubstitute integer,
    fkcmt integer,
    fkparent integer,
    file_path character varying,
    file_name character varying,
    city text
);
SET default_with_oids = false;
CREATE TABLE cm_place_area (
    fkcontext integer,
    fkplace integer,
    fkarea integer
);
CREATE TABLE cm_place_asset (
    fkcontext integer,
    fkplace integer,
    fkasset integer
);
SET default_with_oids = true;
CREATE TABLE cm_place_category (
    fkcontext integer NOT NULL,
    fkparent integer,
    sname character varying(256) NOT NULL,
    tdescription text
);
CREATE TABLE cm_place_category_threat (
    fkcontext integer NOT NULL,
    fkplacecategory integer NOT NULL,
    fkthreat integer NOT NULL
);
CREATE TABLE cm_place_process (
    fkcontext integer,
    fkplace integer,
    fkprocess integer
);
SET default_with_oids = false;
CREATE TABLE cm_place_provider (
    fkcontext integer,
    fkplace integer,
    fkprovider integer
);
CREATE TABLE cm_place_resource (
    fkcontext integer,
    fkplace integer,
    fkresource integer,
    fkgroup integer,
    cmt integer NOT NULL
);
SET default_with_oids = true;
CREATE TABLE cm_place_threat (
    fkcontext integer,
    fkplace integer,
    fkthreat integer
);
CREATE TABLE cm_plan (
    fkcontext integer,
    name text,
    description text,
    fkprocess integer,
    fkplace integer,
    fkasset integer,
    fkarea integer
);
CREATE TABLE cm_plan_action (
    fkcontext integer,
    fkgroup integer,
    fkplanrange integer,
    resume text,
    description text,
    estimate integer,
    fkplan integer,
    fkaction integer,
    estimateflag integer,
    fkplantype integer,
    fkotherplan integer
);
CREATE TABLE cm_plan_action_test (
    fkcontext integer,
    fkplantest integer,
    fkplanaction integer,
    starttime timestamp without time zone,
    endtime timestamp without time zone,
    observation character varying
);
CREATE TABLE cm_plan_range (
    fkcontext integer,
    description text,
    startrange integer,
    endrange integer,
    endrangeflag integer,
    startrangeflag integer,
    fktype integer
);
CREATE TABLE cm_plan_range_type (
    fkcontext integer,
    description text,
    detail text
);
CREATE TABLE cm_plan_schedule (
    fkcontext integer,
    fkplan integer,
    status integer,
    date timestamp without time zone,
    fktest integer,
    fkresponsible integer,
    fkgroup integer
);
CREATE TABLE cm_plan_test (
    fkcontext integer,
    fkplan integer,
    observation character varying,
    starttime timestamp without time zone,
    endtime timestamp without time zone,
    fkresponsible integer
);
SET default_with_oids = false;
CREATE TABLE cm_priority (
    fkcontext integer,
    sname text,
    weight integer DEFAULT 0 NOT NULL
);
SET default_with_oids = true;
CREATE TABLE cm_process_activity (
    fkcontext integer,
    sname text,
    sdescription text,
    fkprocess integer,
    fkresponsible integer,
    fkpriority integer,
    fkgroup integer,
    fkimportance integer
);
CREATE TABLE cm_process_activity_asset (
    fkcontext integer,
    fkactivity integer,
    fkasset integer,
    fkimportance integer
);
CREATE TABLE cm_process_activity_people (
    fkcontext integer,
    fkactivity integer,
    fkuser integer
);
CREATE TABLE cm_process_category (
    fkcontext integer NOT NULL,
    fkparent integer,
    sname character varying(256) NOT NULL,
    tdescription text
);
CREATE TABLE cm_process_category_threat (
    fkcontext integer NOT NULL,
    fkprocesscategory integer NOT NULL,
    fkthreat integer NOT NULL
);
CREATE TABLE cm_process_inputs (
    fkcontext integer NOT NULL,
    fk_process integer,
    fk_process_input integer
);
CREATE TABLE cm_process_outputs (
    fkcontext integer NOT NULL,
    fk_process integer,
    fk_process_output integer
);
CREATE TABLE cm_process_threat (
    fkcontext integer,
    fkprocess integer,
    fkthreat integer
);
SET default_with_oids = false;
CREATE TABLE cm_provider (
    fkcontext integer,
    name text,
    description text,
    contract text,
    contracttype text,
    sla integer
);
CREATE TABLE cm_provider_resource (
    fkcontext integer,
    fkprovider integer,
    fkresource integer,
    bisdefault integer
);
CREATE TABLE cm_resource (
    fkcontext integer,
    name text,
    func text,
    provider integer,
    fkdepartment integer,
    comercialnumber text,
    celnumber text,
    homenumber text,
    nextelnumber text,
    email text
);
SET default_with_oids = true;
CREATE TABLE cm_scene (
    fkcontext integer,
    sname text,
    sdescription text,
    nrto integer,
    nrpo integer,
    nmtpd integer,
    dstartdate timestamp without time zone,
    denddate timestamp without time zone,
    nflags integer,
    fkprocess integer,
    nrto_flags integer,
    nrpo_flags integer,
    nmtpd_flags integer,
    nhourstart integer,
    nhourend integer,
    consequence text,
    fkprobability integer,
    fkseasonality integer
);
CREATE TABLE cm_scope (
    fkcontext integer,
    sname text,
    sdescription text
);
SET default_with_oids = false;
CREATE TABLE cm_scope_area (
    fkcontext integer,
    fkscope integer,
    fkarea integer
);
CREATE TABLE cm_scope_asset (
    fkcontext integer,
    fkscope integer,
    fkasset integer
);
CREATE TABLE cm_scope_place (
    fkcontext integer,
    fkscope integer,
    fkplace integer
);
SET default_with_oids = true;
CREATE TABLE cm_scope_process (
    fkcontext integer,
    fkscope integer,
    fkprocess integer
);
CREATE TABLE cm_threat (
    fkcontext integer,
    sname text,
    sdescription text,
    nprobability integer,
    ntype integer,
    nclass integer
);
CREATE TABLE cm_threat_probability (
    fkcontext integer,
    sname text,
    nprobability integer
);
CREATE TABLE cm_threat_vulnerability (
    fkcontext integer NOT NULL,
    fkplace integer NOT NULL,
    fkthreat integer NOT NULL,
    fklevel integer NOT NULL,
    description text,
    controls text,
    file_path character varying,
    file_name character varying
);
CREATE TABLE isms_context_date (
    fkcontext integer NOT NULL,
    naction integer DEFAULT 0 NOT NULL,
    ddate timestamp without time zone NOT NULL,
    nuserid integer NOT NULL,
    susername character varying(256) NOT NULL
);
CREATE TABLE isms_context (
    pkcontext integer NOT NULL,
    ntype integer DEFAULT 0 NOT NULL,
    nstate integer DEFAULT 0
);
CREATE TABLE isms_profile (
    fkcontext integer NOT NULL,
    sname character varying(256) NOT NULL,
    nid integer DEFAULT 0
);
CREATE TABLE isms_user (
    fkcontext integer NOT NULL,
    fkprofile integer NOT NULL,
    sname character varying(256) NOT NULL,
    slogin character varying(256) NOT NULL,
    spassword character varying(256),
    semail character varying(256),
    nip int8 DEFAULT 0,
    nlanguage integer DEFAULT 0,
    dlastlogin timestamp without time zone,
    nwronglogonattempts integer DEFAULT 0,
    bisblocked integer DEFAULT 0,
    bmustchangepassword integer DEFAULT 0,
    srequestpassword character varying(256),
    ssession character varying(256),
    bansweredsurvey integer DEFAULT 0,
    bshowhelp integer DEFAULT 1,
    CONSTRAINT ckc_bisblocked_isms_use CHECK (((bisblocked IS NULL) OR ((bisblocked >= 0) AND (bisblocked <= 1)))),
    CONSTRAINT ckc_bmustchangepasswo_isms_use CHECK (((bmustchangepassword IS NULL) OR ((bmustchangepassword >= 0) AND (bmustchangepassword <= 1))))
);
CREATE TABLE pm_doc_instance (
    fkcontext integer NOT NULL,
    fkdocument integer,
    nmajorversion integer DEFAULT 0,
    nrevisionversion integer DEFAULT 0,
    trevisionjustification text,
    spath character varying(256),
    tmodifycomment text,
    dbeginproduction timestamp without time zone,
    dendproduction timestamp without time zone,
    sfilename character varying(256),
    bislink integer DEFAULT 0,
    slink character varying(256),
    dbeginapprover timestamp without time zone,
    dbeginrevision timestamp without time zone,
    nfilesize integer DEFAULT 0,
    smanualversion character varying(256),
    CONSTRAINT ckc_bislink_pm_doc_i CHECK (((bislink IS NULL) OR ((bislink >= 0) AND (bislink <= 1))))
);
COMMENT ON TABLE pm_doc_instance IS 'Tabela com o histórico das diferentes versões de um documento.';
COMMENT ON COLUMN pm_doc_instance.nmajorversion IS 'Versão da instância.';
COMMENT ON COLUMN pm_doc_instance.trevisionjustification IS 'Motivo pelo qual o documento entrou em revisão.';
COMMENT ON COLUMN pm_doc_instance.spath IS 'Path do arquivo no servidor de arquivos.';
COMMENT ON COLUMN pm_doc_instance.tmodifycomment IS 'Comentário sobre o que foi modificado no documento.';
COMMENT ON COLUMN pm_doc_instance.dbeginproduction IS 'Data em que o documento entrou em produção.';
COMMENT ON COLUMN pm_doc_instance.dendproduction IS 'Data em que o documento saiu de produção.';
COMMENT ON COLUMN pm_doc_instance.sfilename IS 'Nome do arquivo.';
COMMENT ON COLUMN pm_doc_instance.bislink IS 'Indica se o documento é um link ao invés de um documento físico.';
COMMENT ON COLUMN pm_doc_instance.slink IS 'Link para o documento.';
CREATE TABLE pm_document (
    fkcontext integer NOT NULL,
    fkschedule integer,
    fkclassification integer,
    fkcurrentversion integer,
    fkparent integer,
    fkauthor integer,
    fkmainapprover integer,
    sname character varying(256) NOT NULL,
    tdescription text,
    ntype integer DEFAULT 0 NOT NULL,
    skeywords character varying(256),
    ddateproduction timestamp without time zone,
    ddeadline timestamp without time zone,
    bflagdeadlinealert integer DEFAULT 0,
    bflagdeadlineexpired integer DEFAULT 0,
    ndaysbefore integer DEFAULT 0,
    bhasapproved integer DEFAULT 0,
    CONSTRAINT ckc_bflagdeadlinealer_pm_docum CHECK (((bflagdeadlinealert IS NULL) OR ((bflagdeadlinealert >= 0) AND (bflagdeadlinealert <= 1)))),
    CONSTRAINT ckc_bflagdeadlineexpi_pm_docum CHECK (((bflagdeadlineexpired IS NULL) OR ((bflagdeadlineexpired >= 0) AND (bflagdeadlineexpired <= 1)))),
    CONSTRAINT ckc_bhasapproved_pm_docum CHECK (((bhasapproved IS NULL) OR ((bhasapproved >= 0) AND (bhasapproved <= 1))))
);
COMMENT ON TABLE pm_document IS 'Tabela de documentos.';
COMMENT ON COLUMN pm_document.sname IS 'Nome do documento.';
COMMENT ON COLUMN pm_document.tdescription IS 'Descrição do documento.';
COMMENT ON COLUMN pm_document.ntype IS 'Tipo do documento (template, registro, ...).';
COMMENT ON COLUMN pm_document.skeywords IS 'Palavras para fazer uma busca rápida por documentos.';
COMMENT ON COLUMN pm_document.ddateproduction IS 'Data em que o documento foi para produção.';
COMMENT ON COLUMN pm_document.ddeadline IS 'Prazo para que o documento vá para produção.';
COMMENT ON COLUMN pm_document.bflagdeadlinealert IS 'Indica se já enviou alerta que está chegando perto do prazo.';
COMMENT ON COLUMN pm_document.bflagdeadlineexpired IS 'Indica se já enviou o alerta que o prazo encerrou.';
COMMENT ON COLUMN pm_document.ndaysbefore IS 'Quantos dias antes deve ser enviado alerta que proximadade do prazo final.';
COMMENT ON COLUMN pm_document.bhasapproved IS 'Indica se já foi aprovado pelo aprovador principal.';
CREATE TABLE pm_register (
    fkcontext integer NOT NULL,
    fkclassification integer,
    fkdocument integer,
    fkresponsible integer,
    sname character varying(256) NOT NULL,
    nperiod integer DEFAULT 0 NOT NULL,
    nvalue integer DEFAULT 0 NOT NULL,
    sstorageplace character varying(256),
    sstoragetype character varying(256),
    sindexingtype character varying(256),
    sdisposition character varying(256),
    sprotectionrequirements character varying(256),
    sorigin character varying(256)
);
COMMENT ON COLUMN pm_register.sname IS 'Nome do registro.';
COMMENT ON COLUMN pm_register.nperiod IS 'Ano, mês, dia.';
COMMENT ON COLUMN pm_register.nvalue IS 'Valor. Ex: 5 (nValue) meses (nPeriod).';
CREATE TABLE pm_template (
    fkcontext integer NOT NULL,
    sname character varying(256) NOT NULL,
    ncontexttype integer DEFAULT 0,
    spath character varying(256),
    sfilename character varying(256),
    tdescription text,
    skeywords character varying(256),
    nfilesize integer DEFAULT 0
);
CREATE TABLE rm_area (
    fkcontext integer NOT NULL,
    fkpriority integer,
    fktype integer,
    fkparent integer,
    sname character varying(256) NOT NULL,
    tdescription text,
    sdocument character varying(256),
    nvalue double precision DEFAULT (0)::double precision,
    fkplace integer,
    fkgroup integer,
    fkgroupsubstitute integer,
    fkresource integer,
    fkresourcesubstitute integer
);

CREATE TABLE rm_area_provider(
  fkarea integer,
  fkprovider integer
);

CREATE TABLE rm_asset (
    fkcontext integer NOT NULL,
    sname character varying(256) NOT NULL,
    tdescription text,
    sdocument character varying(256),
    ncost double precision DEFAULT (0)::double precision,
    nvalue double precision DEFAULT (0)::double precision,
    blegality integer DEFAULT 0,
    tjustification text,
    ntac integer,
    ntrc integer,
    nrecoverytime integer,
    ncontingency integer,
    nconformity integer,
    fkcategory integer,
    contingency character varying(256),
    traflag integer,
    tacflag integer,
    fkgroup integer,
    fksecurityresponsible integer,
    alias text,
    model text,
    manufacturer text,
    nversion text,
    build text,
    serial text,
    ip text,
    mac text,
    status text,
    fkplace integer,
    file_path character varying,
    file_name character varying,
    fkprovider integer,
    CONSTRAINT ckc_blegality_rm_asset CHECK (((blegality IS NULL) OR ((blegality >= 0) AND (blegality <= 1))))
);
CREATE TABLE rm_best_practice (
    fkcontext integer NOT NULL,
    fksectionbestpractice integer NOT NULL,
    sname character varying(256) NOT NULL,
    tdescription text,
    ncontroltype integer DEFAULT 0,
    sclassification character varying(256),
    timplementationguide text,
    tmetric text,
    sdocument character varying(256),
    tjustification text
);
CREATE TABLE rm_category (
    fkcontext integer NOT NULL,
    fkparent integer,
    sname character varying(256) NOT NULL,
    tdescription text
);
CREATE TABLE rm_control (
    fkcontext integer NOT NULL,
    fktype integer,
    fkresponsible integer NOT NULL,
    sname character varying(256) NOT NULL,
    tdescription text,
    sdocument character varying(256),
    sevidence character varying(256),
    ndaysbefore integer DEFAULT 0,
    bisactive integer DEFAULT 0,
    bflagimplalert integer DEFAULT 0,
    bflagimplexpired integer DEFAULT 0,
    ddatedeadline timestamp without time zone,
    ddateimplemented timestamp without time zone,
    nimplementationstate integer DEFAULT 0,
    bimplementationislate integer DEFAULT 0,
    brevisionislate integer DEFAULT 0,
    btestislate integer DEFAULT 0,
    befficiencynotok integer DEFAULT 0,
    btestnotok integer DEFAULT 0,
    CONSTRAINT ckc_befficiencynotok_rm_contr CHECK (((befficiencynotok IS NULL) OR ((befficiencynotok >= 0) AND (befficiencynotok <= 1)))),
    CONSTRAINT ckc_bflagimplalert_rm_contr CHECK (((bflagimplalert IS NULL) OR ((bflagimplalert >= 0) AND (bflagimplalert <= 1)))),
    CONSTRAINT ckc_bflagimplexpired_rm_contr CHECK (((bflagimplexpired IS NULL) OR ((bflagimplexpired >= 0) AND (bflagimplexpired <= 1)))),
    CONSTRAINT ckc_bimplementationis_rm_contr CHECK (((bimplementationislate IS NULL) OR ((bimplementationislate >= 0) AND (bimplementationislate <= 1)))),
    CONSTRAINT ckc_bisactive_rm_contr CHECK (((bisactive IS NULL) OR ((bisactive >= 0) AND (bisactive <= 1)))),
    CONSTRAINT ckc_brevisionislate_rm_contr CHECK (((brevisionislate IS NULL) OR ((brevisionislate >= 0) AND (brevisionislate <= 1)))),
    CONSTRAINT ckc_btestislate_rm_contr CHECK (((btestislate IS NULL) OR ((btestislate >= 0) AND (btestislate <= 1)))),
    CONSTRAINT ckc_btestnotok_rm_contr CHECK (((btestnotok IS NULL) OR ((btestnotok >= 0) AND (btestnotok <= 1))))
);
CREATE TABLE rm_event (
    fkcontext integer NOT NULL,
    fktype integer,
    fkcategory integer NOT NULL,
    sdescription character varying(256) NOT NULL,
    tobservation text,
    bpropagate integer DEFAULT 0,
    timpact text,
    CONSTRAINT ckc_bpropagate_rm_event CHECK (((bpropagate IS NULL) OR ((bpropagate >= 0) AND (bpropagate <= 1))))
);
CREATE TABLE rm_process (
    fkcontext integer NOT NULL,
    fkarea integer NOT NULL,
    sname character varying(256) NOT NULL,
    tdescription text,
    sdocument character varying(256),
    nvalue double precision DEFAULT (0)::double precision,
    tinput text,
    toutput text,
    fkgroup integer,
    fkgroupsubstitute integer,
    fkresource integer,
    fkresourcesubstitute integer,
    fktype integer,
    fkpriority integer,
    fkseasonality integer
);
CREATE TABLE rm_process_asset (
    fkcontext integer NOT NULL,
    fkprocess integer,
    fkasset integer,
    fkimportance integer
);
CREATE TABLE rm_risk (
    fkcontext integer NOT NULL,
    fkprobabilityvaluename integer,
    fktype integer,
    fkevent integer,
    fkasset integer NOT NULL,
    sname character varying(256) NOT NULL,
    tdescription text,
    nvalue double precision DEFAULT (0)::double precision,
    nvalueresidual double precision DEFAULT (0)::double precision,
    tjustification text,
    nacceptmode integer DEFAULT 0,
    nacceptstate integer DEFAULT 0,
    sacceptjustification character varying(256),
    ncost double precision DEFAULT (0)::double precision,
    timpact text
);
CREATE TABLE rm_risk_control (
    fkcontext integer NOT NULL,
    fkprobabilityvaluename integer,
    fkrisk integer,
    fkcontrol integer,
    tjustification text
);
CREATE TABLE rm_risk_limits (
    fkcontext integer NOT NULL,
    nlow double precision NOT NULL,
    nhigh double precision NOT NULL
);
CREATE TABLE rm_section_best_practice (
    fkcontext integer NOT NULL,
    fkparent integer,
    sname character varying(256) NOT NULL
);
CREATE TABLE rm_standard (
    fkcontext integer NOT NULL,
    sname character varying(256) NOT NULL,
    tdescription text,
    tapplication text,
    tobjective text
);
CREATE TABLE isms_audit_log (
    ddate timestamp without time zone NOT NULL,
    suser character varying(256),
    naction integer DEFAULT 0,
    starget character varying(256),
    tdescription text
);
CREATE TABLE isms_config (
    pkconfig integer NOT NULL,
    svalue character varying(256)
);
CREATE TABLE isms_context_classification (
    pkclassification integer NOT NULL,
    ncontexttype integer DEFAULT 0 NOT NULL,
    sname character varying(256) NOT NULL,
    nclassificationtype integer DEFAULT 0 NOT NULL
);
CREATE SEQUENCE isms_context_classification_pkclassification_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
ALTER SEQUENCE isms_context_classification_pkclassification_seq OWNED BY isms_context_classification.pkclassification;
CREATE TABLE isms_context_history (
    pkid integer NOT NULL,
    fkcontext integer,
    stype character varying(256) NOT NULL,
    ddate timestamp without time zone NOT NULL,
    nvalue double precision DEFAULT (0)::double precision NOT NULL
);
CREATE SEQUENCE isms_context_history_pkid_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
ALTER SEQUENCE isms_context_history_pkid_seq OWNED BY isms_context_history.pkid;
CREATE SEQUENCE isms_context_pkcontext_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
ALTER SEQUENCE isms_context_pkcontext_seq OWNED BY isms_context.pkcontext;

CREATE TABLE isms_non_conformity_types (
    pkclassification integer NOT NULL,
    sname character varying(256) NOT NULL
);
CREATE SEQUENCE isms_non_conformity_types_pkclassification_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
ALTER SEQUENCE isms_non_conformity_types_pkclassification_seq OWNED BY isms_non_conformity_types.pkclassification;
CREATE TABLE isms_policy (
    fkcontext integer NOT NULL,
    tdescription text NOT NULL
);
CREATE TABLE isms_profile_acl (
    fkprofile integer NOT NULL,
    stag character varying(256) NOT NULL
);
CREATE TABLE isms_saas (
    pkconfig integer NOT NULL,
    svalue character varying(256) NOT NULL
);
CREATE SEQUENCE isms_saas_pkconfig_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
ALTER SEQUENCE isms_saas_pkconfig_seq OWNED BY isms_saas.pkconfig;
CREATE TABLE isms_scope (
    fkcontext integer NOT NULL,
    tdescription text NOT NULL
);
CREATE TABLE isms_solicitor (
    fkuser integer NOT NULL,
    fksolicitor integer NOT NULL,
    ddatebegin timestamp without time zone,
    ddatefinish timestamp without time zone
);
CREATE TABLE isms_user_password_history (
    fkuser integer,
    spassword character varying(256) NOT NULL,
    ddatepasswordchanged timestamp without time zone NOT NULL
);
CREATE TABLE isms_user_preference (
    fkuser integer NOT NULL,
    spreference character varying(256) NOT NULL,
    tvalue text
);
CREATE TABLE pm_doc_approvers (
    fkdocument integer NOT NULL,
    fkuser integer NOT NULL,
    bhasapproved integer DEFAULT 0,
    bmanual integer DEFAULT 0,
    bcontextapprover integer DEFAULT 0,
    CONSTRAINT ckc_bcontextapprover_pm_doc_a CHECK (((bcontextapprover IS NULL) OR ((bcontextapprover >= 0) AND (bcontextapprover <= 1)))),
    CONSTRAINT ckc_bhasapproved_pm_doc_a CHECK (((bhasapproved IS NULL) OR ((bhasapproved >= 0) AND (bhasapproved <= 1)))),
    CONSTRAINT ckc_bmanual_pm_doc_a CHECK (((bmanual IS NULL) OR ((bmanual >= 0) AND (bmanual <= 1))))
);
COMMENT ON TABLE pm_doc_approvers IS 'Tabela que relaciona os documentos com seus respectivos aprovadores.';
COMMENT ON COLUMN pm_doc_approvers.bhasapproved IS 'Indica se o usuáio aprovou o documento.';
CREATE TABLE pm_doc_context (
    fkcontext integer NOT NULL,
    fkdocument integer NOT NULL
);
CREATE TABLE pm_doc_read_history (
    ddate timestamp without time zone NOT NULL,
    fkinstance integer NOT NULL,
    fkuser integer NOT NULL
);
CREATE TABLE pm_doc_readers (
    fkdocument integer NOT NULL,
    fkuser integer NOT NULL,
    bhasread integer DEFAULT 0 NOT NULL,
    bmanual integer DEFAULT 0,
    bdenied integer DEFAULT 0,
    CONSTRAINT ckc_bdenied_pm_doc_r CHECK (((bdenied IS NULL) OR ((bdenied >= 0) AND (bdenied <= 1)))),
    CONSTRAINT ckc_bhasread_pm_doc_r CHECK (((bhasread >= 0) AND (bhasread <= 1))),
    CONSTRAINT ckc_bmanual_pm_doc_r CHECK (((bmanual IS NULL) OR ((bmanual >= 0) AND (bmanual <= 1))))
);
COMMENT ON COLUMN pm_doc_readers.bmanual IS 'Campo para indicar se o usuário foi inserido manualmente, ou seja,
não foi inserido de forma automática pelo sistema.';
COMMENT ON COLUMN pm_doc_readers.bdenied IS 'Campo para indicar que foi negado o acesso do usuário ao documento.';
CREATE TABLE pm_doc_reference (
    pkreference integer NOT NULL,
    fkdocument integer,
    sname character varying(256) NOT NULL,
    slink character varying(256),
    dcreationdate timestamp without time zone
);
COMMENT ON COLUMN pm_doc_reference.sname IS 'Nome da referência.';
COMMENT ON COLUMN pm_doc_reference.slink IS 'Link para a referência.';
COMMENT ON COLUMN pm_doc_reference.dcreationdate IS 'Data de inserção.';
CREATE SEQUENCE pm_doc_reference_pkreference_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
ALTER SEQUENCE pm_doc_reference_pkreference_seq OWNED BY pm_doc_reference.pkreference;
CREATE TABLE pm_doc_registers (
    fkregister integer NOT NULL,
    fkdocument integer NOT NULL
);
CREATE TABLE pm_document_revision_history (
    fkdocument integer NOT NULL,
    ddaterevision timestamp without time zone NOT NULL,
    sjustification text,
    bnewversion integer DEFAULT 0,
    CONSTRAINT ckc_bnewversion_pm_docum CHECK (((bnewversion IS NULL) OR ((bnewversion >= 0) AND (bnewversion <= 1))))
);
COMMENT ON TABLE pm_document_revision_history IS 'Armazena o histórico de revisão do documento.';
COMMENT ON COLUMN pm_document_revision_history.ddaterevision IS 'Data em que ocorreu a revisão.';
COMMENT ON COLUMN pm_document_revision_history.sjustification IS 'Justificativa para a revisão.';
CREATE TABLE pm_instance_comment (
    pkcomment integer NOT NULL,
    fkinstance integer,
    fkuser integer,
    ddate timestamp without time zone NOT NULL,
    tcomment text NOT NULL
);
COMMENT ON COLUMN pm_instance_comment.ddate IS 'Data em que foi feito o comentário.';
CREATE SEQUENCE pm_instance_comment_pkcomment_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
ALTER SEQUENCE pm_instance_comment_pkcomment_seq OWNED BY pm_instance_comment.pkcomment;
CREATE TABLE pm_instance_content (
    fkinstance integer NOT NULL,
    tcontent text
);
COMMENT ON COLUMN pm_instance_content.tcontent IS 'Conteúdo do arquivo físico.';
CREATE TABLE pm_process_user (
    fkprocess integer NOT NULL,
    fkuser integer NOT NULL
);
CREATE TABLE pm_register_readers (
    fkuser integer NOT NULL,
    fkregister integer NOT NULL,
    bmanual integer DEFAULT 0,
    bdenied integer DEFAULT 0,
    CONSTRAINT ckc_bdenied_pm_regis CHECK (((bdenied IS NULL) OR ((bdenied >= 0) AND (bdenied <= 1)))),
    CONSTRAINT ckc_bmanual_pm_regis CHECK (((bmanual IS NULL) OR ((bmanual >= 0) AND (bmanual <= 1))))
);
CREATE TABLE pm_template_best_practice (
    fktemplate integer NOT NULL,
    fkbestpractice integer NOT NULL
);
CREATE TABLE pm_template_content (
    fkcontext integer NOT NULL,
    tcontent text
);
CREATE TABLE rm_asset_asset (
    fkasset integer NOT NULL,
    fkdependent integer NOT NULL
);
CREATE TABLE rm_asset_value (
    fkasset integer NOT NULL,
    fkvaluename integer NOT NULL,
    fkparametername integer NOT NULL,
    tjustification text
);
CREATE TABLE rm_best_practice_event (
    fkcontext integer NOT NULL,
    fkbestpractice integer,
    fkevent integer
);
CREATE TABLE rm_best_practice_standard (
    fkcontext integer NOT NULL,
    fkstandard integer,
    fkbestpractice integer
);
CREATE TABLE rm_control_best_practice (
    fkcontext integer NOT NULL,
    fkbestpractice integer,
    fkcontrol integer
);
CREATE TABLE rm_control_cost (
    fkcontrol integer NOT NULL,
    ncost1 double precision DEFAULT (0)::double precision,
    ncost2 double precision DEFAULT (0)::double precision,
    ncost3 double precision DEFAULT (0)::double precision,
    ncost4 double precision DEFAULT (0)::double precision,
    ncost5 double precision DEFAULT (0)::double precision
);
CREATE TABLE rm_control_efficiency_history (
    fkcontrol integer NOT NULL,
    ddatetodo timestamp without time zone NOT NULL,
    ddateaccomplishment timestamp without time zone,
    nrealefficiency integer DEFAULT 0,
    nexpectedefficiency integer DEFAULT 0,
    bincident integer DEFAULT 0,
    CONSTRAINT ckc_bincident_rm_contr CHECK (((bincident IS NULL) OR ((bincident >= 0) AND (bincident <= 1))))
);
CREATE TABLE rm_control_test_history (
    fkcontrol integer NOT NULL,
    ddatetodo timestamp without time zone NOT NULL,
    ddateaccomplishment timestamp without time zone,
    btestedvalue integer DEFAULT 0,
    tdescription text,
    CONSTRAINT ckc_btestedvalue_rm_contr CHECK (((btestedvalue IS NULL) OR ((btestedvalue >= 0) AND (btestedvalue <= 1))))
);
CREATE TABLE rm_parameter_name (
    pkparametername integer NOT NULL,
    sname character varying(256) NOT NULL,
    nweight integer DEFAULT 1
);
CREATE SEQUENCE rm_parameter_name_pkparametername_seq
    START WITH 10
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
ALTER SEQUENCE rm_parameter_name_pkparametername_seq OWNED BY rm_parameter_name.pkparametername;
CREATE TABLE rm_parameter_value_name (
    pkvaluename integer NOT NULL,
    nvalue integer DEFAULT 1,
    simportance character varying(256),
    simpact character varying(256),
    sriskprobability character varying(256),
    CONSTRAINT ckc_nvalue_rm_param CHECK (((nvalue IS NULL) OR ((nvalue >= 1) AND (nvalue <= 5))))
);
CREATE SEQUENCE rm_parameter_value_name_pkvaluename_seq
    START WITH 10
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
ALTER SEQUENCE rm_parameter_value_name_pkvaluename_seq OWNED BY rm_parameter_value_name.pkvaluename;
CREATE TABLE rm_rc_parameter_value_name (
    pkrcvaluename integer NOT NULL,
    nvalue integer DEFAULT 0,
    srcprobability character varying(256),
    scontrolimpact character varying(256)
);
CREATE SEQUENCE rm_rc_parameter_value_name_pkrcvaluename_seq
    START WITH 10
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
ALTER SEQUENCE rm_rc_parameter_value_name_pkrcvaluename_seq OWNED BY rm_rc_parameter_value_name.pkrcvaluename;
CREATE TABLE rm_risk_control_value (
    fkriskcontrol integer NOT NULL,
    fkparametername integer NOT NULL,
    fkrcvaluename integer NOT NULL,
    tjustification text
);
CREATE TABLE rm_risk_value (
    fkrisk integer NOT NULL,
    fkvaluename integer NOT NULL,
    fkparametername integer NOT NULL,
    tjustification text
);
CREATE TABLE wkf_alert (
    pkalert integer NOT NULL,
    fkcontext integer,
    fkreceiver integer,
    fkcreator integer,
    tjustification text,
    ntype integer DEFAULT 0,
    bemailsent integer DEFAULT 0,
    ddate timestamp without time zone NOT NULL,
    CONSTRAINT ckc_bemailsent_wkf_aler CHECK (((bemailsent IS NULL) OR ((bemailsent >= 0) AND (bemailsent <= 1))))
);
CREATE SEQUENCE wkf_alert_pkalert_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
ALTER SEQUENCE wkf_alert_pkalert_seq OWNED BY wkf_alert.pkalert;
CREATE TABLE wkf_control_efficiency (
    fkcontrolefficiency integer NOT NULL,
    fkschedule integer,
    nrealefficiency integer DEFAULT 0,
    nexpectedefficiency integer DEFAULT 0,
    svalue1 character varying(256),
    svalue2 character varying(256),
    svalue3 character varying(256),
    svalue4 character varying(256),
    svalue5 character varying(256),
    tmetric text
);
CREATE TABLE wkf_control_test (
    fkcontroltest integer NOT NULL,
    fkschedule integer,
    tdescription text
);
CREATE TABLE wkf_schedule (
    pkschedule integer NOT NULL,
    dstart timestamp without time zone NOT NULL,
    dend timestamp without time zone,
    ntype integer DEFAULT 0 NOT NULL,
    nperiodicity integer DEFAULT 0,
    nbitmap integer DEFAULT 0,
    nweek integer DEFAULT 0,
    nday integer DEFAULT 0,
    dnextoccurrence timestamp without time zone,
    ndaystofinish integer DEFAULT 0,
    ddatelimit timestamp without time zone
);
CREATE SEQUENCE wkf_schedule_pkschedule_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
ALTER SEQUENCE wkf_schedule_pkschedule_seq OWNED BY wkf_schedule.pkschedule;
CREATE TABLE wkf_task (
    pktask integer NOT NULL,
    fkcontext integer NOT NULL,
    fkcreator integer NOT NULL,
    fkreceiver integer NOT NULL,
    nactivity integer DEFAULT 0,
    bvisible integer,
    bemailsent integer DEFAULT 0,
    ddateaccomplished timestamp without time zone,
    ddatecreated timestamp without time zone,
    CONSTRAINT ckc_bemailsent_wkf_task CHECK (((bemailsent IS NULL) OR ((bemailsent >= 0) AND (bemailsent <= 1))))
);
CREATE SEQUENCE wkf_task_pkmetadata_seq
    START WITH 5
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
SET default_with_oids = false;
CREATE TABLE wkf_task_metadata (
    pktaskmetadata integer DEFAULT nextval('wkf_task_pkmetadata_seq'::regclass) NOT NULL,
    fkcontext integer,
    ntype integer,
    fktask integer
);
CREATE SEQUENCE wkf_task_pktask_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
ALTER SEQUENCE wkf_task_pktask_seq OWNED BY wkf_task.pktask;
SET default_with_oids = true;
CREATE TABLE wkf_task_schedule (
    fkcontext integer NOT NULL,
    nactivity integer DEFAULT 0 NOT NULL,
    fkschedule integer,
    balertsent integer DEFAULT 0 NOT NULL,
    nalerttype integer DEFAULT 0,
    CONSTRAINT ckc_balertsent_wkf_task CHECK (((balertsent >= 0) AND (balertsent <= 1)))
);

