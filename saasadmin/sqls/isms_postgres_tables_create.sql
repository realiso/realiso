/*==============================================================*/
/* Table: CI_ACTION_PLAN                                        */
/*==============================================================*/
create table CI_ACTION_PLAN (
FKCONTEXT            INT4                 not null,
FKRESPONSIBLE        INT4                 null,
SNAME                VARCHAR(256)         not null,
TACTIONPLAN          TEXT                 null,
NACTIONTYPE          INT4                 null default 0,
DDATEDEADLINE        TIMESTAMP            null,
DDATECONCLUSION      TIMESTAMP            null,
BISEFFICIENT         INT4                 null default 0 
      constraint CKC_BISEFFICIENT_CI_ACTIO check (BISEFFICIENT is null or (BISEFFICIENT between 0 and 1 )),
DDATEEFFICIENCYREVISION TIMESTAMP            null,
DDATEEFFICIENCYMEASURED TIMESTAMP            null,
NDAYSBEFORE          INT4                 null default 0,
BFLAGREVISIONALERT   INT4                 null default 0 
      constraint CKC_BFLAGREVISIONALER_CI_ACTIO2 check (BFLAGREVISIONALERT is null or (BFLAGREVISIONALERT between 0 and 1 )),
BFLAGREVISIONALERTLATE INT4                 null default 0 
      constraint CKC_BFLAGREVISIONALER_CI_ACTIO check (BFLAGREVISIONALERTLATE is null or (BFLAGREVISIONALERTLATE between 0 and 1 )),
SDOCUMENT            VARCHAR(256)         null,
TREVISIONJUSTIFICATION          TEXT                 null,
constraint PK_CI_ACTION_PLAN primary key (FKCONTEXT)
);

/*==============================================================*/
/* Index: CI_ACTION_PLAN_PK                                     */
/*==============================================================*/
create unique index CI_ACTION_PLAN_PK on CI_ACTION_PLAN (
FKCONTEXT
);

/*==============================================================*/
/* Index: FKRESPONSIBLE_FK8                                     */
/*==============================================================*/
create  index FKRESPONSIBLE_FK8 on CI_ACTION_PLAN (
FKRESPONSIBLE
);

/*==============================================================*/
/* Table: CI_CATEGORY                                           */
/*==============================================================*/
create table CI_CATEGORY (
FKCONTEXT            INT4                 not null,
SNAME                VARCHAR(256)         not null,
constraint PK_CI_CATEGORY primary key (FKCONTEXT)
);

comment on table CI_CATEGORY is
'Categoria de incidentes.';

/*==============================================================*/
/* Index: CI_CATEGORY_PK                                        */
/*==============================================================*/
create unique index CI_CATEGORY_PK on CI_CATEGORY (
FKCONTEXT
);

/*==============================================================*/
/* Table: CI_INCIDENT                                           */
/*==============================================================*/
create table CI_INCIDENT (
FKCONTEXT            INT4                 not null,
FKCATEGORY           INT4                 null,
FKRESPONSIBLE        INT4                 null,
SNAME                VARCHAR(256)         not null,
TACCOUNTSPLAN        TEXT                 null,
TEVIDENCES           TEXT                 null,
NLOSSTYPE            INT4                 null default 0,
TEVIDENCEREQUIREMENTCOMMENT TEXT                 null,
DDATELIMIT           TIMESTAMP            null,
DDATEFINISH          TIMESTAMP            null,
TDISPOSALDESCRIPTION TEXT                 null,
TSOLUTIONDESCRIPTION TEXT                 null,
TPRODUCTSERVICE      TEXT                 null,
BNOTEMAILDP          INT4                 null default 0 
      constraint CKC_BNOTEMAILDP_CI_INCID check (BNOTEMAILDP is null or (BNOTEMAILDP between 0 and 1 )),
BEMAILDPSENT         INT4                 null default 0 
      constraint CKC_BEMAILDPSENT_CI_INCID check (BEMAILDPSENT is null or (BEMAILDPSENT between 0 and 1 )),
DDATE                TIMESTAMP            not null,
constraint PK_CI_INCIDENT primary key (FKCONTEXT)
);

comment on table CI_INCIDENT is
'Tabela de incidentes.';

/*==============================================================*/
/* Index: CI_INCIDENT_PK                                        */
/*==============================================================*/
create unique index CI_INCIDENT_PK on CI_INCIDENT (
FKCONTEXT
);

/*==============================================================*/
/* Index: FKCATEGORY_FK3                                        */
/*==============================================================*/
create  index FKCATEGORY_FK3 on CI_INCIDENT (
FKCATEGORY
);

/*==============================================================*/
/* Index: FKRESPONSIBLE_FK6                                     */
/*==============================================================*/
create  index FKRESPONSIBLE_FK6 on CI_INCIDENT (
FKRESPONSIBLE
);

/*==============================================================*/
/* Table: CI_INCIDENT_CONTROL                                   */
/*==============================================================*/
create table CI_INCIDENT_CONTROL (
FKINCIDENT           INT4                 not null,
FKCONTROL            INT4                 not null,
FKCONTEXT            INT4                 not null,
constraint PK_CI_INCIDENT_CONTROL primary key (FKINCIDENT, FKCONTROL, FKCONTEXT)
);

comment on table CI_INCIDENT_CONTROL is
'Tabela que relaciona incidentes e controles.';

/*==============================================================*/
/* Index: CI_INCIDENT_CONTROL_PK                                */
/*==============================================================*/
create unique index CI_INCIDENT_CONTROL_PK on CI_INCIDENT_CONTROL (
FKINCIDENT,
FKCONTROL,
FKCONTEXT
);

/*==============================================================*/
/* Index: FKINCIDENT_FK4                                        */
/*==============================================================*/
create  index FKINCIDENT_FK4 on CI_INCIDENT_CONTROL (
FKINCIDENT
);

/*==============================================================*/
/* Index: FKCONTROL_FK6                                         */
/*==============================================================*/
create  index FKCONTROL_FK6 on CI_INCIDENT_CONTROL (
FKCONTROL
);

/*==============================================================*/
/* Index: FKCONTEXT_FK6                                         */
/*==============================================================*/
create  index FKCONTEXT_FK6 on CI_INCIDENT_CONTROL (
FKCONTEXT
);

/*==============================================================*/
/* Table: CI_INCIDENT_FINANCIAL_IMPACT                          */
/*==============================================================*/
create table CI_INCIDENT_FINANCIAL_IMPACT (
FKINCIDENT           INT4                 not null,
FKCLASSIFICATION     INT4                 not null,
NVALUE               FLOAT8               null default '0',
constraint PK_CI_INCIDENT_FINANCIAL_IMPAC primary key (FKINCIDENT, FKCLASSIFICATION)
);

comment on table CI_INCIDENT_FINANCIAL_IMPACT is
'Tabela de impacto financeiro de um incidente.';

/*==============================================================*/
/* Index: CI_INCIDENT_FINANCIAL_IMPACT_PK                       */
/*==============================================================*/
create unique index CI_INCIDENT_FINANCIAL_IMPACT_PK on CI_INCIDENT_FINANCIAL_IMPACT (
FKINCIDENT,
FKCLASSIFICATION
);

/*==============================================================*/
/* Index: FKINCIDENT_FK6                                        */
/*==============================================================*/
create  index FKINCIDENT_FK6 on CI_INCIDENT_FINANCIAL_IMPACT (
FKINCIDENT
);

/*==============================================================*/
/* Index: FKCLASSIFICATION_FK3                                  */
/*==============================================================*/
create  index FKCLASSIFICATION_FK3 on CI_INCIDENT_FINANCIAL_IMPACT (
FKCLASSIFICATION
);

/*==============================================================*/
/* Table: CI_INCIDENT_PROCESS                                   */
/*==============================================================*/
create table CI_INCIDENT_PROCESS (
FKPROCESS            INT4                 not null,
FKINCIDENT           INT4                 not null,
FKCONTEXT            INT4                 not null,
constraint PK_CI_INCIDENT_PROCESS primary key (FKPROCESS, FKINCIDENT, FKCONTEXT)
);

comment on table CI_INCIDENT_PROCESS is
'Tabela que relaciona incidentes e processos.';

/*==============================================================*/
/* Index: CI_INCIDENT_PROCESS_PK                                */
/*==============================================================*/
create unique index CI_INCIDENT_PROCESS_PK on CI_INCIDENT_PROCESS (
FKPROCESS,
FKINCIDENT,
FKCONTEXT
);

/*==============================================================*/
/* Index: FKPROCESS_FK4                                         */
/*==============================================================*/
create  index FKPROCESS_FK4 on CI_INCIDENT_PROCESS (
FKPROCESS
);

/*==============================================================*/
/* Index: FKINCIDENT_FK5                                        */
/*==============================================================*/
create  index FKINCIDENT_FK5 on CI_INCIDENT_PROCESS (
FKINCIDENT
);

/*==============================================================*/
/* Table: CI_INCIDENT_RISK                                      */
/*==============================================================*/
create table CI_INCIDENT_RISK (
FKCONTEXT            INT4                 not null,
FKRISK               INT4                 not null,
FKINCIDENT           INT4                 not null,
constraint PK_CI_INCIDENT_RISK primary key (FKCONTEXT)
);

/*==============================================================*/
/* Index: CI_INCIDENT_RISK_PK2                                  */
/*==============================================================*/
create unique index CI_INCIDENT_RISK_PK2 on CI_INCIDENT_RISK (
FKCONTEXT
);

/*==============================================================*/
/* Index: FKRISK_FK5                                            */
/*==============================================================*/
create  index FKRISK_FK5 on CI_INCIDENT_RISK (
FKRISK
);

/*==============================================================*/
/* Index: FKINCIDENT_FK7                                        */
/*==============================================================*/
create  index FKINCIDENT_FK7 on CI_INCIDENT_RISK (
FKINCIDENT
);

/*==============================================================*/
/* Table: CI_INCIDENT_RISK_VALUE                                */
/*==============================================================*/
create table CI_INCIDENT_RISK_VALUE (
FKPARAMETERNAME      INT4                 not null,
FKVALUENAME          INT4                 not null,
FKINCRISK            INT4                 not null,
constraint PK_CI_INCIDENT_RISK_VALUE primary key (FKPARAMETERNAME, FKVALUENAME, FKINCRISK)
);

comment on table CI_INCIDENT_RISK_VALUE is
'Tabela que relaciona incidentes e riscos.';

/*==============================================================*/
/* Index: CI_INCIDENT_RISK_VALUE_PK                             */
/*==============================================================*/
create unique index CI_INCIDENT_RISK_VALUE_PK on CI_INCIDENT_RISK_VALUE (
FKPARAMETERNAME,
FKVALUENAME,
FKINCRISK
);

/*==============================================================*/
/* Index: FKPARAMETERNAME_FK4                                   */
/*==============================================================*/
create  index FKPARAMETERNAME_FK4 on CI_INCIDENT_RISK_VALUE (
FKPARAMETERNAME
);

/*==============================================================*/
/* Index: FKVALUENAME_FK3                                       */
/*==============================================================*/
create  index FKVALUENAME_FK3 on CI_INCIDENT_RISK_VALUE (
FKVALUENAME
);

/*==============================================================*/
/* Index: FKINCRISK_FK                                          */
/*==============================================================*/
create  index FKINCRISK_FK on CI_INCIDENT_RISK_VALUE (
FKINCRISK
);

/*==============================================================*/
/* Table: CI_INCIDENT_USER                                      */
/*==============================================================*/
create table CI_INCIDENT_USER (
FKUSER               INT4                 not null,
FKINCIDENT           INT4                 not null,
TDESCRIPTION         TEXT                 not null,
TACTIONTAKEN         TEXT                 null,
constraint PK_CI_INCIDENT_USER primary key (FKUSER, FKINCIDENT)
);

comment on table CI_INCIDENT_USER is
'Tabela de relacionamento entre  incidentes e usuários.';

/*==============================================================*/
/* Index: CI_INCIDENT_USER_PK                                   */
/*==============================================================*/
create unique index CI_INCIDENT_USER_PK on CI_INCIDENT_USER (
FKUSER,
FKINCIDENT
);

/*==============================================================*/
/* Index: FKINCIDENT_FK2                                        */
/*==============================================================*/
create  index FKINCIDENT_FK2 on CI_INCIDENT_USER (
FKINCIDENT
);

/*==============================================================*/
/* Index: FKUSER_FK8                                            */
/*==============================================================*/
create  index FKUSER_FK8 on CI_INCIDENT_USER (
FKUSER
);

/*==============================================================*/
/* Table: CI_NC                                                 */
/*==============================================================*/
create table CI_NC (
FKCONTEXT            INT4                 not null,
FKCONTROL            INT4                 null,
FKRESPONSIBLE        INT4                 null,
FKSENDER             INT4                 not null,
SNAME                VARCHAR(600)         not null,
NSEQNUMBER           SERIAL               not null,
TDESCRIPTION         TEXT                 null,
TCAUSE               TEXT                 null,
TDENIALJUSTIFICATION TEXT                 null,
NCLASSIFICATION      INT4                 null default 0,
DDATESENT            TIMESTAMP            null,
NCAPABILITY          INT4                 null default 0,
constraint PK_CI_NC primary key (FKCONTEXT)
);

/*==============================================================*/
/* Index: CI_NC_PK                                              */
/*==============================================================*/
create unique index CI_NC_PK on CI_NC (
FKCONTEXT
);

/*==============================================================*/
/* Index: FKCONTROL_FK5                                         */
/*==============================================================*/
create  index FKCONTROL_FK5 on CI_NC (
FKCONTROL
);

/*==============================================================*/
/* Index: FKRESPONSIBLE_FK7                                     */
/*==============================================================*/
create  index FKRESPONSIBLE_FK7 on CI_NC (
FKRESPONSIBLE
);

/*==============================================================*/
/* Index: FKSENDER_FK                                           */
/*==============================================================*/
create  index FKSENDER_FK on CI_NC (
FKSENDER
);

/*==============================================================*/
/* Table: CI_NC_ACTION_PLAN                                     */
/*==============================================================*/
create table CI_NC_ACTION_PLAN (
FKNC                 INT4                 not null,
FKACTIONPLAN         INT4                 not null,
constraint PK_CI_NC_ACTION_PLAN primary key (FKNC, FKACTIONPLAN)
);

comment on table CI_NC_ACTION_PLAN is
'Relaciona não conformidades com planos de ação.';

/*==============================================================*/
/* Index: CI_NC_ACTION_PLAN_PK                                  */
/*==============================================================*/
create unique index CI_NC_ACTION_PLAN_PK on CI_NC_ACTION_PLAN (
FKNC,
FKACTIONPLAN
);

/*==============================================================*/
/* Index: FKACTIONPLAN_FK                                       */
/*==============================================================*/
create  index FKACTIONPLAN_FK on CI_NC_ACTION_PLAN (
FKACTIONPLAN
);

/*==============================================================*/
/* Index: FKNC_FK2                                              */
/*==============================================================*/
create  index FKNC_FK2 on CI_NC_ACTION_PLAN (
FKNC
);

/*==============================================================*/
/* Table: CI_NC_PROCESS                                         */
/*==============================================================*/
create table CI_NC_PROCESS (
FKNC                 INT4                 not null,
FKPROCESS            INT4                 not null,
constraint PK_CI_NC_PROCESS primary key (FKNC, FKPROCESS)
);

comment on table CI_NC_PROCESS is
'Tabela que relaciona não conformidades com processos.';

/*==============================================================*/
/* Index: CI_NC_PROCESS_PK                                      */
/*==============================================================*/
create unique index CI_NC_PROCESS_PK on CI_NC_PROCESS (
FKNC,
FKPROCESS
);

/*==============================================================*/
/* Index: FKNC_FK                                               */
/*==============================================================*/
create  index FKNC_FK on CI_NC_PROCESS (
FKNC
);

/*==============================================================*/
/* Index: FKPROCESS_FK3                                         */
/*==============================================================*/
create  index FKPROCESS_FK3 on CI_NC_PROCESS (
FKPROCESS
);

/*==============================================================*/
/* Table: CI_NC_SEED                                            */
/*==============================================================*/
create table CI_NC_SEED (
FKCONTROL            INT4                 not null,
NDEACTIVATIONREASON  INT4                 not null default 0,
DDATESENT            TIMESTAMP            null,
constraint PK_CI_NC_SEED primary key (FKCONTROL, NDEACTIVATIONREASON)
);

/*==============================================================*/
/* Index: CI_NC_SEED_PK                                         */
/*==============================================================*/
create unique index CI_NC_SEED_PK on CI_NC_SEED (
FKCONTROL,
NDEACTIVATIONREASON
);

/*==============================================================*/
/* Index: FKCONTROL_FK7                                         */
/*==============================================================*/
create  index FKCONTROL_FK7 on CI_NC_SEED (
FKCONTROL
);

/*==============================================================*/
/* Table: CI_OCCURRENCE                                         */
/*==============================================================*/
create table CI_OCCURRENCE (
FKCONTEXT            INT4                 not null,
FKINCIDENT           INT4                 null,
TDESCRIPTION         TEXT                 not null,
TDENIALJUSTIFICATION TEXT                 null,
DDATE                TIMESTAMP            not null,
constraint PK_CI_OCCURRENCE primary key (FKCONTEXT)
);

comment on table CI_OCCURRENCE is
'Tabela de ocorrências de incidentes.';

/*==============================================================*/
/* Index: CI_OCURRENCE_PK                                       */
/*==============================================================*/
create unique index CI_OCURRENCE_PK on CI_OCCURRENCE (
FKCONTEXT
);

/*==============================================================*/
/* Index: FKINCIDENT_FK                                         */
/*==============================================================*/
create  index FKINCIDENT_FK on CI_OCCURRENCE (
FKINCIDENT
);

/*==============================================================*/
/* Table: CI_RISK_PROBABILITY                                   */
/*==============================================================*/
create table CI_RISK_PROBABILITY (
FKRISK               INT4                 not null,
FKVALUENAME          INT4                 not null,
NINCIDENTAMOUNT      INT4                 not null default 0,
constraint PK_CI_RISK_PROBABILITY primary key (FKRISK, FKVALUENAME)
);

/*==============================================================*/
/* Index: CI_RISK_VALUE_PK                                      */
/*==============================================================*/
create unique index CI_RISK_VALUE_PK on CI_RISK_PROBABILITY (
FKRISK,
FKVALUENAME
);

/*==============================================================*/
/* Index: FKPARAMTER_FK                                         */
/*==============================================================*/
create  index FKPARAMTER_FK on CI_RISK_PROBABILITY (
FKVALUENAME
);

/*==============================================================*/
/* Index: FKRISK_FK4                                            */
/*==============================================================*/
create  index FKRISK_FK4 on CI_RISK_PROBABILITY (
FKRISK
);

/*==============================================================*/
/* Table: CI_RISK_SCHEDULE                                      */
/*==============================================================*/
create table CI_RISK_SCHEDULE (
FKRISK               INT4                 not null,
NPERIOD              INT4                 not null default 0,
NVALUE               INT4                 not null default 0,
DDATELASTCHECK       TIMESTAMP            null,
constraint PK_CI_RISK_SCHEDULE primary key (FKRISK)
);

/*==============================================================*/
/* Index: CI_RISK_SCHEDULE_PK                                   */
/*==============================================================*/
create unique index CI_RISK_SCHEDULE_PK on CI_RISK_SCHEDULE (
FKRISK
);

/*==============================================================*/
/* Table: CI_SOLUTION                                           */
/*==============================================================*/
create table CI_SOLUTION (
FKCONTEXT            INT4                 not null,
FKCATEGORY           INT4                 null,
TPROBLEM             TEXT                 null,
TSOLUTION            TEXT                 null,
TKEYWORDS            TEXT                 null,
constraint PK_CI_SOLUTION primary key (FKCONTEXT)
);

comment on table CI_SOLUTION is
'Tabela de soluções para incidentes.';

/*==============================================================*/
/* Index: CI_SOLUTION_PK                                        */
/*==============================================================*/
create unique index CI_SOLUTION_PK on CI_SOLUTION (
FKCONTEXT
);

/*==============================================================*/
/* Index: FKCATEGORY_FK4                                        */
/*==============================================================*/
create  index FKCATEGORY_FK4 on CI_SOLUTION (
FKCATEGORY
);

/*==============================================================*/
/* Table: ISMS_AUDIT_LOG                                        */
/*==============================================================*/
create table ISMS_AUDIT_LOG (
DDATE                TIMESTAMP            not null,
SUSER                VARCHAR(256)         null,
NACTION              INT4                 null default 0,
STARGET              VARCHAR(256)         null,
TDESCRIPTION         TEXT                 null
);

/*==============================================================*/
/* Table: ISMS_CONFIG                                           */
/*==============================================================*/
create table ISMS_CONFIG (
PKCONFIG             INT4                 not null,
SVALUE               VARCHAR(256)         null,
constraint PK_ISMS_CONFIG primary key (PKCONFIG)
);

/*==============================================================*/
/* Index: ISMS_CONFIG_PK                                        */
/*==============================================================*/
create unique index ISMS_CONFIG_PK on ISMS_CONFIG (
PKCONFIG
);

/*==============================================================*/
/* Table: ISMS_CONTEXT                                          */
/*==============================================================*/
create table ISMS_CONTEXT (
PKCONTEXT            SERIAL               not null,
NTYPE                INT4                 not null default 0,
NSTATE               INT4                 null default 0,
constraint PK_ISMS_CONTEXT primary key (PKCONTEXT)
) WITH (
  OIDS=TRUE
);

/*==============================================================*/
/* Index: ISMS_CONTEXT_PK                                       */
/*==============================================================*/
create unique index ISMS_CONTEXT_PK on ISMS_CONTEXT (
PKCONTEXT
);

/*==============================================================*/
/* Table: ISMS_CONTEXT_CLASSIFICATION                           */
/*==============================================================*/
create table ISMS_CONTEXT_CLASSIFICATION (
PKCLASSIFICATION     SERIAL               not null,
NCONTEXTTYPE         INT4                 not null default 0,
SNAME                VARCHAR(256)         not null,
NCLASSIFICATIONTYPE  INT4                 not null default 0,
NWEIGHT				 INT4				  null,
constraint PK_ISMS_CONTEXT_CLASSIFICATION primary key (PKCLASSIFICATION)
);

/*==============================================================*/
/* Index: ISMS_CONTEXT_CLASSIFICATION_PK                        */
/*==============================================================*/
create unique index ISMS_CONTEXT_CLASSIFICATION_PK on ISMS_CONTEXT_CLASSIFICATION (
PKCLASSIFICATION
);

/*==============================================================*/
/* Table: ISMS_CONTEXT_DATE                                     */
/*==============================================================*/
create table ISMS_CONTEXT_DATE (
FKCONTEXT            INT4                 not null,
NACTION              INT4                 not null default 0,
DDATE                TIMESTAMP            not null,
NUSERID              INT4                 not null,
SUSERNAME            VARCHAR(256)         not null,
constraint PK_ISMS_CONTEXT_DATE primary key (FKCONTEXT, NACTION)
);

/*==============================================================*/
/* Index: ISMS_CONTEXT_DATE_PK                                  */
/*==============================================================*/
create unique index ISMS_CONTEXT_DATE_PK on ISMS_CONTEXT_DATE (
FKCONTEXT,
NACTION
);

/*==============================================================*/
/* Index: FKCONTEXT_FK3                                         */
/*==============================================================*/
create  index FKCONTEXT_FK3 on ISMS_CONTEXT_DATE (
FKCONTEXT
);

/*==============================================================*/
/* Table: ISMS_CONTEXT_HISTORY                                  */
/*==============================================================*/
create table ISMS_CONTEXT_HISTORY (
PKID                 SERIAL               not null,
FKCONTEXT            INT4                 null,
STYPE                VARCHAR(256)         not null,
DDATE                TIMESTAMP            not null,
NVALUE               FLOAT8               not null default '0',
constraint PK_ISMS_CONTEXT_HISTORY primary key (PKID)
);

/*==============================================================*/
/* Index: ISMS_CONTEXT_HISTORY_PK                               */
/*==============================================================*/
create unique index ISMS_CONTEXT_HISTORY_PK on ISMS_CONTEXT_HISTORY (
PKID
);

/*==============================================================*/
/* Index: FKCONTEXT_FK5                                         */
/*==============================================================*/
create  index FKCONTEXT_FK5 on ISMS_CONTEXT_HISTORY (
FKCONTEXT
);

/*==============================================================*/
/* Table: ISMS_POLICY                                           */
/*==============================================================*/
create table ISMS_POLICY (
FKCONTEXT            INT4                 not null,
TDESCRIPTION         TEXT                 not null,
constraint PK_ISMS_POLICY primary key (FKCONTEXT)
);

/*==============================================================*/
/* Index: ISMS_POLICY_PK                                        */
/*==============================================================*/
create unique index ISMS_POLICY_PK on ISMS_POLICY (
FKCONTEXT
);

/*==============================================================*/
/* Table: ISMS_PROFILE                                          */
/*==============================================================*/
create table ISMS_PROFILE (
FKCONTEXT            INT4                 not null,
SNAME                VARCHAR(256)         not null,
NID                  INT4                 null default 0,
constraint PK_ISMS_PROFILE primary key (FKCONTEXT)
);

/*==============================================================*/
/* Index: ISMS_PROFILE_PK                                       */
/*==============================================================*/
create unique index ISMS_PROFILE_PK on ISMS_PROFILE (
FKCONTEXT
);

/*==============================================================*/
/* Table: ISMS_PROFILE_ACL                                      */
/*==============================================================*/
create table ISMS_PROFILE_ACL (
FKPROFILE            INT4                 not null,
STAG                 VARCHAR(256)         not null,
constraint PK_ISMS_PROFILE_ACL primary key (FKPROFILE, STAG)
);

/*==============================================================*/
/* Index: ISMS_PROFILE_ACL_PK                                   */
/*==============================================================*/
create unique index ISMS_PROFILE_ACL_PK on ISMS_PROFILE_ACL (
FKPROFILE,
STAG
);

/*==============================================================*/
/* Index: FKPROFILE_FK2                                         */
/*==============================================================*/
create  index FKPROFILE_FK2 on ISMS_PROFILE_ACL (
FKPROFILE
);

/*==============================================================*/
/* Table: ISMS_SAAS                                             */
/*==============================================================*/
create table ISMS_SAAS (
PKCONFIG             SERIAL               not null,
SVALUE               VARCHAR(256)         not null,
constraint PK_ISMS_SAAS primary key (PKCONFIG)
);

/*==============================================================*/
/* Index: ISMS_SAAS_PK                                          */
/*==============================================================*/
create unique index ISMS_SAAS_PK on ISMS_SAAS (
PKCONFIG
);

/*==============================================================*/
/* Table: ISMS_SCOPE                                            */
/*==============================================================*/
create table ISMS_SCOPE (
FKCONTEXT            INT4                 not null,
TDESCRIPTION         TEXT                 not null,
constraint PK_ISMS_SCOPE primary key (FKCONTEXT)
);

/*==============================================================*/
/* Index: ISMS_SCOPE_PK                                         */
/*==============================================================*/
create unique index ISMS_SCOPE_PK on ISMS_SCOPE (
FKCONTEXT
);

/*==============================================================*/
/* Table: ISMS_SOLICITOR                                        */
/*==============================================================*/
create table ISMS_SOLICITOR (
FKUSER               INT4                 not null,
FKSOLICITOR          INT4                 not null,
DDATEBEGIN           TIMESTAMP            null,
DDATEFINISH          TIMESTAMP            null,
constraint PK_ISMS_SOLICITOR primary key (FKUSER, FKSOLICITOR)
);

/*==============================================================*/
/* Index: ISMS_SOLICITOR_PK                                     */
/*==============================================================*/
create unique index ISMS_SOLICITOR_PK on ISMS_SOLICITOR (
FKUSER,
FKSOLICITOR
);

/*==============================================================*/
/* Index: FKSOLICITOR_FK                                        */
/*==============================================================*/
create  index FKSOLICITOR_FK on ISMS_SOLICITOR (
FKSOLICITOR
);

/*==============================================================*/
/* Index: FKUSER_FK2                                            */
/*==============================================================*/
create  index FKUSER_FK2 on ISMS_SOLICITOR (
FKUSER
);

/*==============================================================*/
/* Table: ISMS_USER                                             */
/*==============================================================*/
create table ISMS_USER (
FKCONTEXT            INT4                 not null,
FKPROFILE            INT4                 not null,
SNAME                VARCHAR(256)         not null,
SLOGIN               VARCHAR(256)         not null,
SPASSWORD            VARCHAR(256)         null,
SEMAIL               VARCHAR(256)         null,
NIP                  INT8                 null default 0,
NLANGUAGE            INT4                 null default 0,
DLASTLOGIN           TIMESTAMP            null,
NWRONGLOGONATTEMPTS  INT4                 null default 0,
BISBLOCKED           INT4                 null default 0 
      constraint CKC_BISBLOCKED_ISMS_USE check (BISBLOCKED is null or (BISBLOCKED between 0 and 1 )),
BMUSTCHANGEPASSWORD  INT4                 null default 0 
      constraint CKC_BMUSTCHANGEPASSWO_ISMS_USE check (BMUSTCHANGEPASSWORD is null or (BMUSTCHANGEPASSWORD between 0 and 1 )),
SREQUESTPASSWORD     VARCHAR(256)         null,
SSESSION             VARCHAR(256)         null,
BANSWEREDSURVEY      INT4                 null default 0,
BSHOWHELP            INT4                 null default 1,
BISAD 				 INT4				  null default 0,
constraint PK_ISMS_USER primary key (FKCONTEXT)
);

/*==============================================================*/
/* Index: ISMS_USER_PK                                          */
/*==============================================================*/
create unique index ISMS_USER_PK on ISMS_USER (
FKCONTEXT
);

/*==============================================================*/
/* Index: FKPROFILE_FK                                          */
/*==============================================================*/
create  index FKPROFILE_FK on ISMS_USER (
FKPROFILE
);

/*==============================================================*/
/* Table: ISMS_USER_PASSWORD_HISTORY                            */
/*==============================================================*/
create table ISMS_USER_PASSWORD_HISTORY (
FKUSER               INT4                 null,
SPASSWORD            VARCHAR(256)         not null,
DDATEPASSWORDCHANGED TIMESTAMP            not null
);

/*==============================================================*/
/* Table: ISMS_USER_PREFERENCE                                  */
/*==============================================================*/
create table ISMS_USER_PREFERENCE (
FKUSER               INT4                 not null,
SPREFERENCE          VARCHAR(256)         not null,
TVALUE               TEXT                 null,
constraint PK_ISMS_USER_PREFERENCE primary key (FKUSER, SPREFERENCE)
);

/*==============================================================*/
/* Index: ISMS_USER_PREFERENCE_PK                               */
/*==============================================================*/
create unique index ISMS_USER_PREFERENCE_PK on ISMS_USER_PREFERENCE (
FKUSER,
SPREFERENCE
);

/*==============================================================*/
/* Index: FKUSER_FK                                             */
/*==============================================================*/
create  index FKUSER_FK on ISMS_USER_PREFERENCE (
FKUSER
);

/*==============================================================*/
/* Table: PM_DOCUMENT                                           */
/*==============================================================*/
create table PM_DOCUMENT (
FKCONTEXT            INT4                 not null,
FKSCHEDULE           INT4                 null,
FKCLASSIFICATION     INT4                 null,
FKCURRENTVERSION     INT4                 null,
FKPARENT             INT4                 null,
FKAUTHOR             INT4                 null,
FKMAINAPPROVER       INT4                 null,
SNAME                VARCHAR(256)         not null,
TDESCRIPTION         TEXT                 null,
NTYPE                INT4                 not null default 0,
SKEYWORDS            VARCHAR(256)         null,
DDATEPRODUCTION      TIMESTAMP            null,
DDEADLINE            TIMESTAMP            null,
BFLAGDEADLINEALERT   INT4                 null default 0 
      constraint CKC_BFLAGDEADLINEALER_PM_DOCUM check (BFLAGDEADLINEALERT is null or (BFLAGDEADLINEALERT between 0 and 1 )),
BFLAGDEADLINEEXPIRED INT4                 null default 0 
      constraint CKC_BFLAGDEADLINEEXPI_PM_DOCUM check (BFLAGDEADLINEEXPIRED is null or (BFLAGDEADLINEEXPIRED between 0 and 1 )),
NDAYSBEFORE          INT4                 null default 0,
BHASAPPROVED         INT4                 null default 0 
      constraint CKC_BHASAPPROVED_PM_DOCUM check (BHASAPPROVED is null or (BHASAPPROVED between 0 and 1 )),
constraint PK_PM_DOCUMENT primary key (FKCONTEXT)
);

comment on table PM_DOCUMENT is
'Tabela de documentos.';

comment on column PM_DOCUMENT.SNAME is
'Nome do documento.';

comment on column PM_DOCUMENT.TDESCRIPTION is
'Descrição do documento.';

comment on column PM_DOCUMENT.NTYPE is
'Tipo do documento (template, registro, ...).';

comment on column PM_DOCUMENT.SKEYWORDS is
'Palavras para fazer uma busca rápida por documentos.';

comment on column PM_DOCUMENT.DDATEPRODUCTION is
'Data em que o documento foi para produção.';

comment on column PM_DOCUMENT.DDEADLINE is
'Prazo para que o documento vá para produção.';

comment on column PM_DOCUMENT.BFLAGDEADLINEALERT is
'Indica se já enviou alerta que está chegando perto do prazo.';

comment on column PM_DOCUMENT.BFLAGDEADLINEEXPIRED is
'Indica se já enviou o alerta que o prazo encerrou.';

comment on column PM_DOCUMENT.NDAYSBEFORE is
'Quantos dias antes deve ser enviado alerta que proximadade do prazo final.';

comment on column PM_DOCUMENT.BHASAPPROVED is
'Indica se já foi aprovado pelo aprovador principal.';

/*==============================================================*/
/* Index: PM_DOCUMENT_PK                                        */
/*==============================================================*/
create unique index PM_DOCUMENT_PK on PM_DOCUMENT (
FKCONTEXT
);

/*==============================================================*/
/* Index: FKSCHEDULE_FK4                                        */
/*==============================================================*/
create  index FKSCHEDULE_FK4 on PM_DOCUMENT (
FKSCHEDULE
);

/*==============================================================*/
/* Index: FKCLASSIFICATION_FK                                   */
/*==============================================================*/
create  index FKCLASSIFICATION_FK on PM_DOCUMENT (
FKCLASSIFICATION
);

/*==============================================================*/
/* Index: FKAUTHOR_FK                                           */
/*==============================================================*/
create  index FKAUTHOR_FK on PM_DOCUMENT (
FKAUTHOR
);

/*==============================================================*/
/* Index: FKPARENT_FK5                                          */
/*==============================================================*/
create  index FKPARENT_FK5 on PM_DOCUMENT (
FKPARENT
);

/*==============================================================*/
/* Index: FKMAINAPPOVER_FK                                      */
/*==============================================================*/
create  index FKMAINAPPOVER_FK on PM_DOCUMENT (
FKMAINAPPROVER
);

/*==============================================================*/
/* Index: FKCURRENTVERSION_FK                                   */
/*==============================================================*/
create  index FKCURRENTVERSION_FK on PM_DOCUMENT (
FKCURRENTVERSION
);

/*==============================================================*/
/* Table: PM_DOCUMENT_REVISION_HISTORY                          */
/*==============================================================*/
create table PM_DOCUMENT_REVISION_HISTORY (
FKDOCUMENT           INT4                 not null,
DDATEREVISION        TIMESTAMP            not null,
SJUSTIFICATION       TEXT                 null,
BNEWVERSION          INT4                 null default 0 
      constraint CKC_BNEWVERSION_PM_DOCUM check (BNEWVERSION is null or (BNEWVERSION between 0 and 1 )),
constraint PK_PM_DOCUMENT_REVISION_HISTOR primary key (FKDOCUMENT, DDATEREVISION)
);

comment on table PM_DOCUMENT_REVISION_HISTORY is
'Armazena o histórico de revisão do documento.';

comment on column PM_DOCUMENT_REVISION_HISTORY.DDATEREVISION is
'Data em que ocorreu a revisão.';

comment on column PM_DOCUMENT_REVISION_HISTORY.SJUSTIFICATION is
'Justificativa para a revisão.';

/*==============================================================*/
/* Index: PM_DOCUMENT_REVISION_HISTORY_PK                       */
/*==============================================================*/
create unique index PM_DOCUMENT_REVISION_HISTORY_PK on PM_DOCUMENT_REVISION_HISTORY (
FKDOCUMENT,
DDATEREVISION
);

/*==============================================================*/
/* Index: FKDOCUMENT_FK5                                        */
/*==============================================================*/
create  index FKDOCUMENT_FK5 on PM_DOCUMENT_REVISION_HISTORY (
FKDOCUMENT
);

/*==============================================================*/
/* Table: PM_DOC_APPROVERS                                      */
/*==============================================================*/
create table PM_DOC_APPROVERS (
FKDOCUMENT           INT4                 not null,
FKUSER               INT4                 not null,
BHASAPPROVED         INT4                 null default 0 
      constraint CKC_BHASAPPROVED_PM_DOC_A check (BHASAPPROVED is null or (BHASAPPROVED between 0 and 1 )),
BMANUAL              INT4                 null default 0 
      constraint CKC_BMANUAL_PM_DOC_A check (BMANUAL is null or (BMANUAL between 0 and 1 )),
BCONTEXTAPPROVER     INT4                 null default 0 
      constraint CKC_BCONTEXTAPPROVER_PM_DOC_A check (BCONTEXTAPPROVER is null or (BCONTEXTAPPROVER between 0 and 1 )),
constraint PK_PM_DOC_APPROVERS primary key (FKDOCUMENT, FKUSER)
);

comment on table PM_DOC_APPROVERS is
'Tabela que relaciona os documentos com seus respectivos aprovadores.';

comment on column PM_DOC_APPROVERS.BHASAPPROVED is
'Indica se o usuáio aprovou o documento.';

/*==============================================================*/
/* Index: FKDOCUMENT_FK                                         */
/*==============================================================*/
create  index FKDOCUMENT_FK on PM_DOC_APPROVERS (
FKDOCUMENT
);

/*==============================================================*/
/* Index: FKUSER_FK3                                            */
/*==============================================================*/
create  index FKUSER_FK3 on PM_DOC_APPROVERS (
FKUSER
);

/*==============================================================*/
/* Table: PM_DOC_CONTEXT                                        */
/*==============================================================*/
create table PM_DOC_CONTEXT (
FKCONTEXT            INT4                 not null,
FKDOCUMENT           INT4                 not null,
constraint PK_PM_DOC_CONTEXT primary key (FKCONTEXT, FKDOCUMENT)
);

/*==============================================================*/
/* Index: FKDOCUMENT_FK8                                        */
/*==============================================================*/
create  index FKDOCUMENT_FK8 on PM_DOC_CONTEXT (
FKDOCUMENT
);

/*==============================================================*/
/* Index: FKCONTEXT_FK4                                         */
/*==============================================================*/
create  index FKCONTEXT_FK4 on PM_DOC_CONTEXT (
FKCONTEXT
);

/*==============================================================*/
/* Table: PM_DOC_INSTANCE                                       */
/*==============================================================*/
create table PM_DOC_INSTANCE (
FKCONTEXT            INT4                 not null,
FKDOCUMENT           INT4                 null,
NMAJORVERSION        INT4                 null default 0,
NREVISIONVERSION     INT4                 null default 0,
TREVISIONJUSTIFICATION TEXT                 null,
SPATH                VARCHAR(256)         null,
TMODIFYCOMMENT       TEXT                 null,
DBEGINPRODUCTION     TIMESTAMP            null,
DENDPRODUCTION       TIMESTAMP            null,
SFILENAME            VARCHAR(256)         null,
BISLINK              INT4                 null default 0 
      constraint CKC_BISLINK_PM_DOC_I check (BISLINK is null or (BISLINK between 0 and 1 )),
SLINK                VARCHAR(256)         null,
DBEGINAPPROVER       TIMESTAMP            null,
DBEGINREVISION       TIMESTAMP            null,
NFILESIZE            INT4                 null default 0,
SMANUALVERSION       VARCHAR(256)         null,
constraint PK_PM_DOC_INSTANCE primary key (FKCONTEXT)
);

comment on table PM_DOC_INSTANCE is
'Tabela com o histórico das diferentes versões de um documento.';

comment on column PM_DOC_INSTANCE.NMAJORVERSION is
'Versão da instância.';

comment on column PM_DOC_INSTANCE.TREVISIONJUSTIFICATION is
'Motivo pelo qual o documento entrou em revisão.';

comment on column PM_DOC_INSTANCE.SPATH is
'Path do arquivo no servidor de arquivos.';

comment on column PM_DOC_INSTANCE.TMODIFYCOMMENT is
'Comentário sobre o que foi modificado no documento.';

comment on column PM_DOC_INSTANCE.DBEGINPRODUCTION is
'Data em que o documento entrou em produção.';

comment on column PM_DOC_INSTANCE.DENDPRODUCTION is
'Data em que o documento saiu de produção.';

comment on column PM_DOC_INSTANCE.SFILENAME is
'Nome do arquivo.';

comment on column PM_DOC_INSTANCE.BISLINK is
'Indica se o documento é um link ao invés de um documento físico.';

comment on column PM_DOC_INSTANCE.SLINK is
'Link para o documento.';

/*==============================================================*/
/* Index: PM_DOC_INSTANCE_PK                                    */
/*==============================================================*/
create unique index PM_DOC_INSTANCE_PK on PM_DOC_INSTANCE (
FKCONTEXT
);

/*==============================================================*/
/* Index: FKDOCUMENT_FK2                                        */
/*==============================================================*/
create  index FKDOCUMENT_FK2 on PM_DOC_INSTANCE (
FKDOCUMENT
);

/*==============================================================*/
/* Table: PM_DOC_READERS                                        */
/*==============================================================*/
create table PM_DOC_READERS (
FKDOCUMENT           INT4                 not null,
FKUSER               INT4                 not null,
BHASREAD             INT4                 not null default 0 
      constraint CKC_BHASREAD_PM_DOC_R check (BHASREAD between 0 and 1),
BMANUAL              INT4                 null default 0 
      constraint CKC_BMANUAL_PM_DOC_R check (BMANUAL is null or (BMANUAL between 0 and 1 )),
BDENIED              INT4                 null default 0 
      constraint CKC_BDENIED_PM_DOC_R check (BDENIED is null or (BDENIED between 0 and 1 )),
constraint PK_PM_DOC_READERS primary key (FKDOCUMENT, FKUSER)
);

comment on column PM_DOC_READERS.BMANUAL is
'Campo para indicar se o usuário foi inserido manualmente, ou seja,
não foi inserido de forma automática pelo sistema.';

comment on column PM_DOC_READERS.BDENIED is
'Campo para indicar que foi negado o acesso do usuário ao documento.';

/*==============================================================*/
/* Index: FKDOCUMENT_FK4                                        */
/*==============================================================*/
create  index FKDOCUMENT_FK4 on PM_DOC_READERS (
FKDOCUMENT
);

/*==============================================================*/
/* Index: FKUSER_FK5                                            */
/*==============================================================*/
create  index FKUSER_FK5 on PM_DOC_READERS (
FKUSER
);

/*==============================================================*/
/* Table: PM_DOC_READ_HISTORY                                   */
/*==============================================================*/
create table PM_DOC_READ_HISTORY (
DDATE                TIMESTAMP            not null,
FKINSTANCE           INT4                 not null,
FKUSER               INT4                 not null,
constraint PK_PM_DOC_READ_HISTORY primary key (DDATE, FKINSTANCE, FKUSER)
);

/*==============================================================*/
/* Index: PM_DOC_READ_HISTORY_PK                                */
/*==============================================================*/
create unique index PM_DOC_READ_HISTORY_PK on PM_DOC_READ_HISTORY (
DDATE,
FKINSTANCE,
FKUSER
);

/*==============================================================*/
/* Index: FKUSER_FK7                                            */
/*==============================================================*/
create  index FKUSER_FK7 on PM_DOC_READ_HISTORY (
FKUSER
);

/*==============================================================*/
/* Table: PM_DOC_REFERENCE                                      */
/*==============================================================*/
create table PM_DOC_REFERENCE (
PKREFERENCE          SERIAL               not null,
FKDOCUMENT           INT4                 null,
SNAME                VARCHAR(256)         not null,
SLINK                VARCHAR(256)         null,
DCREATIONDATE        TIMESTAMP            null,
constraint PK_PM_DOC_REFERENCE primary key (PKREFERENCE)
);

comment on column PM_DOC_REFERENCE.SNAME is
'Nome da referência.';

comment on column PM_DOC_REFERENCE.SLINK is
'Link para a referência.';

comment on column PM_DOC_REFERENCE.DCREATIONDATE is
'Data de inserção.';

/*==============================================================*/
/* Index: PM_DOC_REFERENCE_PK                                   */
/*==============================================================*/
create unique index PM_DOC_REFERENCE_PK on PM_DOC_REFERENCE (
PKREFERENCE
);

/*==============================================================*/
/* Index: FKDOCUMENT_FK3                                        */
/*==============================================================*/
create  index FKDOCUMENT_FK3 on PM_DOC_REFERENCE (
FKDOCUMENT
);

/*==============================================================*/
/* Table: PM_DOC_REGISTERS                                      */
/*==============================================================*/
create table PM_DOC_REGISTERS (
FKREGISTER           INT4                 not null,
FKDOCUMENT           INT4                 not null,
constraint PK_PM_DOC_REGISTERS primary key (FKREGISTER, FKDOCUMENT)
);

/*==============================================================*/
/* Index: FKREGISTER_FK                                         */
/*==============================================================*/
create  index FKREGISTER_FK on PM_DOC_REGISTERS (
FKREGISTER
);

/*==============================================================*/
/* Index: FKDOCUMENT_FK6                                        */
/*==============================================================*/
create  index FKDOCUMENT_FK6 on PM_DOC_REGISTERS (
FKDOCUMENT
);

/*==============================================================*/
/* Table: PM_INSTANCE_COMMENT                                   */
/*==============================================================*/
create table PM_INSTANCE_COMMENT (
PKCOMMENT            SERIAL               not null,
FKINSTANCE           INT4                 null,
FKUSER               INT4                 null,
DDATE                TIMESTAMP            not null,
TCOMMENT             TEXT                 not null,
constraint PK_PM_INSTANCE_COMMENT primary key (PKCOMMENT)
);

comment on column PM_INSTANCE_COMMENT.DDATE is
'Data em que foi feito o comentário.';

/*==============================================================*/
/* Index: PM_INSTANCE_COMMENT_PK                                */
/*==============================================================*/
create unique index PM_INSTANCE_COMMENT_PK on PM_INSTANCE_COMMENT (
PKCOMMENT
);

/*==============================================================*/
/* Index: FKINSTANCE_FK                                         */
/*==============================================================*/
create  index FKINSTANCE_FK on PM_INSTANCE_COMMENT (
FKINSTANCE
);

/*==============================================================*/
/* Index: FKUSER_FK4                                            */
/*==============================================================*/
create  index FKUSER_FK4 on PM_INSTANCE_COMMENT (
FKUSER
);

/*==============================================================*/
/* Table: PM_INSTANCE_CONTENT                                   */
/*==============================================================*/
create table PM_INSTANCE_CONTENT (
FKINSTANCE           INT4                 not null,
TCONTENT             TEXT                 null,
constraint PK_PM_INSTANCE_CONTENT primary key (FKINSTANCE)
);

comment on column PM_INSTANCE_CONTENT.TCONTENT is
'Conteúdo do arquivo físico.';

/*==============================================================*/
/* Index: PM_INSTANCE_CONTENT_PK                                */
/*==============================================================*/
create unique index PM_INSTANCE_CONTENT_PK on PM_INSTANCE_CONTENT (
FKINSTANCE
);

/*==============================================================*/
/* Table: PM_PROCESS_USER                                       */
/*==============================================================*/
create table PM_PROCESS_USER (
FKPROCESS            INT4                 not null,
FKUSER               INT4                 not null,
constraint PK_PM_PROCESS_USER primary key (FKPROCESS, FKUSER)
);

/*==============================================================*/
/* Index: FKUSER_FK6                                            */
/*==============================================================*/
create  index FKUSER_FK6 on PM_PROCESS_USER (
FKUSER
);

/*==============================================================*/
/* Index: FKPROCESS_FK2                                         */
/*==============================================================*/
create  index FKPROCESS_FK2 on PM_PROCESS_USER (
FKPROCESS
);

/*==============================================================*/
/* Table: PM_REGISTER                                           */
/*==============================================================*/
create table PM_REGISTER (
FKCONTEXT            INT4                 not null,
FKCLASSIFICATION     INT4                 null,
FKDOCUMENT           INT4                 null,
FKRESPONSIBLE        INT4                 null,
SNAME                VARCHAR(256)         not null,
NPERIOD              INT4                 not null default 0,
NVALUE               INT4                 not null default 0,
SSTORAGEPLACE        VARCHAR(256)         null,
SSTORAGETYPE         VARCHAR(256)         null,
SINDEXINGTYPE        VARCHAR(256)         null,
SDISPOSITION         VARCHAR(256)         null,
SPROTECTIONREQUIREMENTS VARCHAR(256)         null,
SORIGIN              VARCHAR(256)         null,
constraint PK_PM_REGISTER primary key (FKCONTEXT)
);

comment on column PM_REGISTER.SNAME is
'Nome do registro.';

comment on column PM_REGISTER.NPERIOD is
'Ano, mês, dia.';

comment on column PM_REGISTER.NVALUE is
'Valor. Ex: 5 (nValue) meses (nPeriod).';

/*==============================================================*/
/* Index: PM_REGISTER_PK                                        */
/*==============================================================*/
create unique index PM_REGISTER_PK on PM_REGISTER (
FKCONTEXT
);

/*==============================================================*/
/* Index: FKCLASSIFICATION_FK2                                  */
/*==============================================================*/
create  index FKCLASSIFICATION_FK2 on PM_REGISTER (
FKCLASSIFICATION
);

/*==============================================================*/
/* Index: FKDOCUMENT_FK7                                        */
/*==============================================================*/
create  index FKDOCUMENT_FK7 on PM_REGISTER (
FKDOCUMENT
);

/*==============================================================*/
/* Index: FKRESPONSIBLE_FK5                                     */
/*==============================================================*/
create  index FKRESPONSIBLE_FK5 on PM_REGISTER (
FKRESPONSIBLE
);

/*==============================================================*/
/* Table: PM_REGISTER_READERS                                   */
/*==============================================================*/
create table PM_REGISTER_READERS (
FKUSER               INT4                 not null,
FKREGISTER           INT4                 not null,
BMANUAL              INT4                 null default 0 
      constraint CKC_BMANUAL_PM_REGIS check (BMANUAL is null or (BMANUAL between 0 and 1 )),
BDENIED              INT4                 null default 0 
      constraint CKC_BDENIED_PM_REGIS check (BDENIED is null or (BDENIED between 0 and 1 )),
constraint PK_PM_REGISTER_READERS primary key (FKUSER, FKREGISTER)
);

/*==============================================================*/
/* Index: PM_REGISTER_READERS_PK                                */
/*==============================================================*/
create unique index PM_REGISTER_READERS_PK on PM_REGISTER_READERS (
FKUSER,
FKREGISTER
);

/*==============================================================*/
/* Index: FKREGISTER_FK2                                        */
/*==============================================================*/
create  index FKREGISTER_FK2 on PM_REGISTER_READERS (
FKREGISTER
);

/*==============================================================*/
/* Index: FKUSER_FK9                                            */
/*==============================================================*/
create  index FKUSER_FK9 on PM_REGISTER_READERS (
FKUSER
);

/*==============================================================*/
/* Table: PM_TEMPLATE                                           */
/*==============================================================*/
create table PM_TEMPLATE (
FKCONTEXT            INT4                 not null,
SNAME                VARCHAR(256)         not null,
NCONTEXTTYPE         INT4                 null default 0,
SPATH                VARCHAR(256)         null,
SFILENAME            VARCHAR(256)         null,
TDESCRIPTION         TEXT                 null,
SKEYWORDS            VARCHAR(256)         null,
NFILESIZE            INT4                 null default 0,
constraint PK_PM_TEMPLATE primary key (FKCONTEXT)
);

/*==============================================================*/
/* Index: PM_TEMPLATE_PK                                        */
/*==============================================================*/
create unique index PM_TEMPLATE_PK on PM_TEMPLATE (
FKCONTEXT
);

/*==============================================================*/
/* Table: PM_TEMPLATE_BEST_PRACTICE                             */
/*==============================================================*/
create table PM_TEMPLATE_BEST_PRACTICE (
FKTEMPLATE           INT4                 not null,
FKBESTPRACTICE       INT4                 not null,
constraint PK_PM_TEMPLATE_BEST_PRACTICE primary key (FKTEMPLATE, FKBESTPRACTICE)
);

/*==============================================================*/
/* Index: PM_TEMPLATE_BEST_PRACTICE_PK                          */
/*==============================================================*/
create unique index PM_TEMPLATE_BEST_PRACTICE_PK on PM_TEMPLATE_BEST_PRACTICE (
FKTEMPLATE,
FKBESTPRACTICE
);

/*==============================================================*/
/* Index: FKTEMPLATE_FK                                         */
/*==============================================================*/
create  index FKTEMPLATE_FK on PM_TEMPLATE_BEST_PRACTICE (
FKTEMPLATE
);

/*==============================================================*/
/* Index: FKBESTPRACTICE_FK4                                    */
/*==============================================================*/
create  index FKBESTPRACTICE_FK4 on PM_TEMPLATE_BEST_PRACTICE (
FKBESTPRACTICE
);

/*==============================================================*/
/* Table: PM_TEMPLATE_CONTENT                                   */
/*==============================================================*/
create table PM_TEMPLATE_CONTENT (
FKCONTEXT            INT4                 not null,
TCONTENT             TEXT                 null,
constraint PK_PM_TEMPLATE_CONTENT primary key (FKCONTEXT)
);

/*==============================================================*/
/* Index: PM_TEMPLATE_CONTENT_PK                                */
/*==============================================================*/
create unique index PM_TEMPLATE_CONTENT_PK on PM_TEMPLATE_CONTENT (
FKCONTEXT
);

/*==============================================================*/
/* Table: RM_AREA                                               */
/*==============================================================*/
create table RM_AREA (
FKCONTEXT            INT4                 not null,
FKPRIORITY           INT4                 null,
FKTYPE               INT4                 null,
FKPARENT             INT4                 null,
FKRESPONSIBLE        INT4                 not null,
SNAME                VARCHAR(256)         not null,
TDESCRIPTION         TEXT                 null,
SDOCUMENT            VARCHAR(256)         null,
NVALUE               FLOAT8               null default '0',
constraint PK_RM_AREA primary key (FKCONTEXT)
);

/*==============================================================*/
/* Index: RM_AREA_PK                                            */
/*==============================================================*/
create unique index RM_AREA_PK on RM_AREA (
FKCONTEXT
);

/*==============================================================*/
/* Index: FKPARENT_FK                                           */
/*==============================================================*/
create  index FKPARENT_FK on RM_AREA (
FKPARENT
);

/*==============================================================*/
/* Index: FKRESPONSIBLE_FK                                      */
/*==============================================================*/
create  index FKRESPONSIBLE_FK on RM_AREA (
FKRESPONSIBLE
);

/*==============================================================*/
/* Index: FKTYPE_FK                                             */
/*==============================================================*/
create  index FKTYPE_FK on RM_AREA (
FKTYPE
);

/*==============================================================*/
/* Index: FKPRIORITY_FK                                         */
/*==============================================================*/
create  index FKPRIORITY_FK on RM_AREA (
FKPRIORITY
);

/*==============================================================*/
/* Table: RM_ASSET                                              */
/*==============================================================*/
create table RM_ASSET (
FKCONTEXT            INT4                 not null,
FKCATEGORY           INT4                 not null,
FKRESPONSIBLE        INT4                 not null,
FKSECURITYRESPONSIBLE INT4                 not null,
SNAME                VARCHAR(256)         not null,
TDESCRIPTION         TEXT                 null,
SDOCUMENT            VARCHAR(256)         null,
NCOST                FLOAT8               null default '0',
NVALUE               FLOAT8               null default '0',
BLEGALITY            INT4                 null default 0 
      constraint CKC_BLEGALITY_RM_ASSET check (BLEGALITY is null or (BLEGALITY between 0 and 1 )),
TJUSTIFICATION       TEXT                 null,
constraint PK_RM_ASSET primary key (FKCONTEXT)
);

/*==============================================================*/
/* Index: RM_ASSET_PK                                           */
/*==============================================================*/
create unique index RM_ASSET_PK on RM_ASSET (
FKCONTEXT
);

/*==============================================================*/
/* Index: FKRESPONSIBLE_FK4                                     */
/*==============================================================*/
create  index FKRESPONSIBLE_FK4 on RM_ASSET (
FKRESPONSIBLE
);

/*==============================================================*/
/* Index: FKSECURITYRESPONSIBLE_FK                              */
/*==============================================================*/
create  index FKSECURITYRESPONSIBLE_FK on RM_ASSET (
FKSECURITYRESPONSIBLE
);

/*==============================================================*/
/* Index: FKCATEGORY_FK                                         */
/*==============================================================*/
create  index FKCATEGORY_FK on RM_ASSET (
FKCATEGORY
);

/*==============================================================*/
/* Table: RM_ASSET_ASSET                                        */
/*==============================================================*/
create table RM_ASSET_ASSET (
FKASSET              INT4                 not null,
FKDEPENDENT          INT4                 not null,
constraint PK_RM_ASSET_ASSET primary key (FKASSET, FKDEPENDENT)
);

/*==============================================================*/
/* Index: RM_ASSET_ASSET_PK                                     */
/*==============================================================*/
create unique index RM_ASSET_ASSET_PK on RM_ASSET_ASSET (
FKASSET,
FKDEPENDENT
);

/*==============================================================*/
/* Index: FKASSET_FK4                                           */
/*==============================================================*/
create  index FKASSET_FK4 on RM_ASSET_ASSET (
FKASSET
);

/*==============================================================*/
/* Index: FKDEPENDENT_FK                                        */
/*==============================================================*/
create  index FKDEPENDENT_FK on RM_ASSET_ASSET (
FKDEPENDENT
);

/*==============================================================*/
/* Table: RM_ASSET_VALUE                                        */
/*==============================================================*/
create table RM_ASSET_VALUE (
FKASSET              INT4                 not null,
FKVALUENAME          INT4                 not null,
FKPARAMETERNAME      INT4                 not null,
TJUSTIFICATION       TEXT                 null,
constraint PK_RM_ASSET_VALUE primary key (FKASSET, FKVALUENAME, FKPARAMETERNAME)
);

/*==============================================================*/
/* Index: RM_ASSET_VALUE_PK                                     */
/*==============================================================*/
create unique index RM_ASSET_VALUE_PK on RM_ASSET_VALUE (
FKASSET,
FKVALUENAME,
FKPARAMETERNAME
);

/*==============================================================*/
/* Index: FKASSET_FK3                                           */
/*==============================================================*/
create  index FKASSET_FK3 on RM_ASSET_VALUE (
FKASSET
);

/*==============================================================*/
/* Index: FKVALUENAME_FK                                        */
/*==============================================================*/
create  index FKVALUENAME_FK on RM_ASSET_VALUE (
FKVALUENAME
);

/*==============================================================*/
/* Index: FKPARAMETERNAME_FK                                    */
/*==============================================================*/
create  index FKPARAMETERNAME_FK on RM_ASSET_VALUE (
FKPARAMETERNAME
);

/*==============================================================*/
/* Table: RM_BEST_PRACTICE                                      */
/*==============================================================*/
create table RM_BEST_PRACTICE (
FKCONTEXT            INT4                 not null,
FKSECTIONBESTPRACTICE INT4                 not null,
SNAME                VARCHAR(256)         not null,
TDESCRIPTION         TEXT                 null,
NCONTROLTYPE         INT4                 null default 0,
SCLASSIFICATION      VARCHAR(256)         null,
TIMPLEMENTATIONGUIDE TEXT                 null,
TMETRIC              TEXT                 null,
SDOCUMENT            VARCHAR(256)         null,
TJUSTIFICATION       TEXT                 null,
constraint PK_RM_BEST_PRACTICE primary key (FKCONTEXT)
);

/*==============================================================*/
/* Index: RM_BEST_PRACTICE_PK                                   */
/*==============================================================*/
create unique index RM_BEST_PRACTICE_PK on RM_BEST_PRACTICE (
FKCONTEXT
);

/*==============================================================*/
/* Index: FKSECTIONBESTPRACTICE_FK                              */
/*==============================================================*/
create  index FKSECTIONBESTPRACTICE_FK on RM_BEST_PRACTICE (
FKSECTIONBESTPRACTICE
);

/*==============================================================*/
/* Table: RM_BEST_PRACTICE_EVENT                                */
/*==============================================================*/
create table RM_BEST_PRACTICE_EVENT (
FKCONTEXT            INT4                 not null,
FKBESTPRACTICE       INT4                 null,
FKEVENT              INT4                 null,
constraint PK_RM_BEST_PRACTICE_EVENT primary key (FKCONTEXT)
);

/*==============================================================*/
/* Index: RM_BEST_PRACTICE_EVENT_PK                             */
/*==============================================================*/
create unique index RM_BEST_PRACTICE_EVENT_PK on RM_BEST_PRACTICE_EVENT (
FKCONTEXT
);

/*==============================================================*/
/* Index: FKBESTPRACTICE_FK3                                    */
/*==============================================================*/
create  index FKBESTPRACTICE_FK3 on RM_BEST_PRACTICE_EVENT (
FKBESTPRACTICE
);

/*==============================================================*/
/* Index: FKEVENT_FK2                                           */
/*==============================================================*/
create  index FKEVENT_FK2 on RM_BEST_PRACTICE_EVENT (
FKEVENT
);

/*==============================================================*/
/* Table: RM_BEST_PRACTICE_STANDARD                             */
/*==============================================================*/
create table RM_BEST_PRACTICE_STANDARD (
FKCONTEXT            INT4                 not null,
FKSTANDARD           INT4                 null,
FKBESTPRACTICE       INT4                 null,
constraint PK_RM_BEST_PRACTICE_STANDARD primary key (FKCONTEXT)
);

/*==============================================================*/
/* Index: RM_BEST_PRACTICE_STANDARD_PK                          */
/*==============================================================*/
create unique index RM_BEST_PRACTICE_STANDARD_PK on RM_BEST_PRACTICE_STANDARD (
FKCONTEXT
);

/*==============================================================*/
/* Index: FKSTANDARD_FK                                         */
/*==============================================================*/
create  index FKSTANDARD_FK on RM_BEST_PRACTICE_STANDARD (
FKSTANDARD
);

/*==============================================================*/
/* Index: FKBESTPRACTICE_FK2                                    */
/*==============================================================*/
create  index FKBESTPRACTICE_FK2 on RM_BEST_PRACTICE_STANDARD (
FKBESTPRACTICE
);

/*==============================================================*/
/* Table: RM_CATEGORY                                           */
/*==============================================================*/
create table RM_CATEGORY (
FKCONTEXT            INT4                 not null,
FKPARENT             INT4                 null,
SNAME                VARCHAR(256)         not null,
TDESCRIPTION         TEXT                 null,
constraint PK_RM_CATEGORY primary key (FKCONTEXT)
);

/*==============================================================*/
/* Index: RM_CATEGORY_PK                                        */
/*==============================================================*/
create unique index RM_CATEGORY_PK on RM_CATEGORY (
FKCONTEXT
);

/*==============================================================*/
/* Index: FKPARENT_FK2                                          */
/*==============================================================*/
create  index FKPARENT_FK2 on RM_CATEGORY (
FKPARENT
);

/*==============================================================*/
/* Table: RM_CONTROL                                            */
/*==============================================================*/
create table RM_CONTROL (
FKCONTEXT            INT4                 not null,
FKTYPE               INT4                 null,
FKRESPONSIBLE        INT4                 not null,
SNAME                VARCHAR(256)         not null,
TDESCRIPTION         TEXT                 null,
SDOCUMENT            VARCHAR(256)         null,
SEVIDENCE            VARCHAR(256)         null,
NDAYSBEFORE          INT4                 null default 0,
BISACTIVE            INT4                 null default 0 
      constraint CKC_BISACTIVE_RM_CONTR check (BISACTIVE is null or (BISACTIVE between 0 and 1 )),
BFLAGIMPLALERT       INT4                 null default 0 
      constraint CKC_BFLAGIMPLALERT_RM_CONTR check (BFLAGIMPLALERT is null or (BFLAGIMPLALERT between 0 and 1 )),
BFLAGIMPLEXPIRED     INT4                 null default 0 
      constraint CKC_BFLAGIMPLEXPIRED_RM_CONTR check (BFLAGIMPLEXPIRED is null or (BFLAGIMPLEXPIRED between 0 and 1 )),
DDATEDEADLINE        TIMESTAMP            null,
DDATEIMPLEMENTED     TIMESTAMP            null,
NIMPLEMENTATIONSTATE INT4                 null default 0,
BIMPLEMENTATIONISLATE INT4                 null default 0 
      constraint CKC_BIMPLEMENTATIONIS_RM_CONTR check (BIMPLEMENTATIONISLATE is null or (BIMPLEMENTATIONISLATE between 0 and 1 )),
BREVISIONISLATE      INT4                 null default 0 
      constraint CKC_BREVISIONISLATE_RM_CONTR check (BREVISIONISLATE is null or (BREVISIONISLATE between 0 and 1 )),
BTESTISLATE          INT4                 null default 0 
      constraint CKC_BTESTISLATE_RM_CONTR check (BTESTISLATE is null or (BTESTISLATE between 0 and 1 )),
BEFFICIENCYNOTOK     INT4                 null default 0 
      constraint CKC_BEFFICIENCYNOTOK_RM_CONTR check (BEFFICIENCYNOTOK is null or (BEFFICIENCYNOTOK between 0 and 1 )),
BTESTNOTOK           INT4                 null default 0 
      constraint CKC_BTESTNOTOK_RM_CONTR check (BTESTNOTOK is null or (BTESTNOTOK between 0 and 1 )),
constraint PK_RM_CONTROL primary key (FKCONTEXT)
);

/*==============================================================*/
/* Index: RM_CONTROL_PK                                         */
/*==============================================================*/
create unique index RM_CONTROL_PK on RM_CONTROL (
FKCONTEXT
);

/*==============================================================*/
/* Index: FKRESPONSIBLE_FK3                                     */
/*==============================================================*/
create  index FKRESPONSIBLE_FK3 on RM_CONTROL (
FKRESPONSIBLE
);

/*==============================================================*/
/* Index: FKTYPE_FK3                                            */
/*==============================================================*/
create  index FKTYPE_FK3 on RM_CONTROL (
FKTYPE
);

/*==============================================================*/
/* Table: RM_CONTROL_BEST_PRACTICE                              */
/*==============================================================*/
create table RM_CONTROL_BEST_PRACTICE (
FKCONTEXT            INT4                 not null,
FKBESTPRACTICE       INT4                 null,
FKCONTROL            INT4                 null,
constraint PK_RM_CONTROL_BEST_PRACTICE primary key (FKCONTEXT)
);

/*==============================================================*/
/* Index: RM_CONTROL_BEST_PRACTICE_PK                           */
/*==============================================================*/
create unique index RM_CONTROL_BEST_PRACTICE_PK on RM_CONTROL_BEST_PRACTICE (
FKCONTEXT
);

/*==============================================================*/
/* Index: FKBESTPRACTICE_FK                                     */
/*==============================================================*/
create  index FKBESTPRACTICE_FK on RM_CONTROL_BEST_PRACTICE (
FKBESTPRACTICE
);

/*==============================================================*/
/* Index: FKCONTROL_FK2                                         */
/*==============================================================*/
create  index FKCONTROL_FK2 on RM_CONTROL_BEST_PRACTICE (
FKCONTROL
);

/*==============================================================*/
/* Table: RM_CONTROL_COST                                       */
/*==============================================================*/
create table RM_CONTROL_COST (
FKCONTROL            INT4                 not null,
NCOST1               FLOAT8               null default '0',
NCOST2               FLOAT8               null default '0',
NCOST3               FLOAT8               null default '0',
NCOST4               FLOAT8               null default '0',
NCOST5               FLOAT8               null default '0',
constraint PK_RM_CONTROL_COST primary key (FKCONTROL)
);

/*==============================================================*/
/* Index: RM_CONTROL_COST_PK                                    */
/*==============================================================*/
create unique index RM_CONTROL_COST_PK on RM_CONTROL_COST (
FKCONTROL
);

/*==============================================================*/
/* Table: RM_CONTROL_EFFICIENCY_HISTORY                         */
/*==============================================================*/
create table RM_CONTROL_EFFICIENCY_HISTORY (
FKCONTROL            INT4                 not null,
DDATETODO            TIMESTAMP            not null,
DDATEACCOMPLISHMENT  TIMESTAMP            null,
NREALEFFICIENCY      INT4                 null default 0,
NEXPECTEDEFFICIENCY  INT4                 null default 0,
BINCIDENT            INT4                 null default 0 
      constraint CKC_BINCIDENT_RM_CONTR check (BINCIDENT is null or (BINCIDENT between 0 and 1 )),
TJUSTIFICATION		TEXT		null,
constraint PK_RM_CONTROL_EFFICIENCY_HISTO primary key (FKCONTROL, DDATETODO)
);

/*==============================================================*/
/* Index: RM_CONTROL_EFFICIENCY_HISTORY_P                       */
/*==============================================================*/
create unique index RM_CONTROL_EFFICIENCY_HISTORY_P on RM_CONTROL_EFFICIENCY_HISTORY (
FKCONTROL,
DDATETODO
);

/*==============================================================*/
/* Index: FKCONTROL_FK3                                         */
/*==============================================================*/
create  index FKCONTROL_FK3 on RM_CONTROL_EFFICIENCY_HISTORY (
FKCONTROL
);

/*==============================================================*/
/* Table: RM_CONTROL_TEST_HISTORY                               */
/*==============================================================*/
create table RM_CONTROL_TEST_HISTORY (
FKCONTROL            INT4                 not null,
DDATETODO            TIMESTAMP            not null,
DDATEACCOMPLISHMENT  TIMESTAMP            null,
BTESTEDVALUE         INT4                 null default 0 
      constraint CKC_BTESTEDVALUE_RM_CONTR check (BTESTEDVALUE is null or (BTESTEDVALUE between 0 and 1 )),
TDESCRIPTION         TEXT                 null,
TJUSTIFICATION		TEXT		null,
constraint PK_RM_CONTROL_TEST_HISTORY primary key (FKCONTROL, DDATETODO)
);

/*==============================================================*/
/* Index: RM_CONTROL_TEST_HISTORY_PK                            */
/*==============================================================*/
create unique index RM_CONTROL_TEST_HISTORY_PK on RM_CONTROL_TEST_HISTORY (
FKCONTROL,
DDATETODO
);

/*==============================================================*/
/* Index: FKCONTROL_FK4                                         */
/*==============================================================*/
create  index FKCONTROL_FK4 on RM_CONTROL_TEST_HISTORY (
FKCONTROL
);

/*==============================================================*/
/* Table: RM_EVENT                                              */
/*==============================================================*/
create table RM_EVENT (
FKCONTEXT            INT4                 not null,
FKTYPE               INT4                 null,
FKCATEGORY           INT4                 not null,
SDESCRIPTION         VARCHAR(256)         not null,
TOBSERVATION         TEXT                 null,
BPROPAGATE           INT4                 null default 0 
      constraint CKC_BPROPAGATE_RM_EVENT check (BPROPAGATE is null or (BPROPAGATE between 0 and 1 )),
TIMPACT              TEXT                 null,
constraint PK_RM_EVENT primary key (FKCONTEXT)
);

/*==============================================================*/
/* Index: RM_EVENT_PK                                           */
/*==============================================================*/
create unique index RM_EVENT_PK on RM_EVENT (
FKCONTEXT
);

/*==============================================================*/
/* Index: FKCATEGORY_FK2                                        */
/*==============================================================*/
create  index FKCATEGORY_FK2 on RM_EVENT (
FKCATEGORY
);

/*==============================================================*/
/* Index: FKTYPE_FK5                                            */
/*==============================================================*/
create  index FKTYPE_FK5 on RM_EVENT (
FKTYPE
);

/*==============================================================*/
/* Table: RM_PARAMETER_NAME                                     */
/*==============================================================*/
create table RM_PARAMETER_NAME (
PKPARAMETERNAME      SERIAL               not null,
SNAME                VARCHAR(256)         not null,
NWEIGHT              INT4                 null default 1,
constraint PK_RM_PARAMETER_NAME primary key (PKPARAMETERNAME)
);

/*==============================================================*/
/* Index: RM_PARAMETER_NAME_PK                                  */
/*==============================================================*/
create unique index RM_PARAMETER_NAME_PK on RM_PARAMETER_NAME (
PKPARAMETERNAME
);

/*==============================================================*/
/* Table: RM_PARAMETER_VALUE_NAME                               */
/*==============================================================*/
create table RM_PARAMETER_VALUE_NAME (
PKVALUENAME          SERIAL               not null,
NVALUE               INT4                 null default 1 
      constraint CKC_NVALUE_RM_PARAM check (NVALUE is null or (NVALUE between 1 and 5 )),
SIMPORTANCE          VARCHAR(256)         null,
SIMPACT              VARCHAR(256)         null,
SRISKPROBABILITY     VARCHAR(256)         null,
constraint PK_RM_PARAMETER_VALUE_NAME primary key (PKVALUENAME)
);

/*==============================================================*/
/* Index: RM_PARAMETER_VALUE_NAME_PK                            */
/*==============================================================*/
create unique index RM_PARAMETER_VALUE_NAME_PK on RM_PARAMETER_VALUE_NAME (
PKVALUENAME
);

/*==============================================================*/
/* Table: RM_PROCESS                                            */
/*==============================================================*/
create table RM_PROCESS (
FKCONTEXT            INT4                 not null,
FKPRIORITY           INT4                 null,
FKTYPE               INT4                 null,
FKAREA               INT4                 not null,
FKRESPONSIBLE        INT4                 not null,
SNAME                VARCHAR(256)         not null,
TDESCRIPTION         TEXT                 null,
SDOCUMENT            VARCHAR(256)         null,
NVALUE               FLOAT8               null default '0',
TJUSTIFICATION       TEXT                 null,
constraint PK_RM_PROCESS primary key (FKCONTEXT)
);

/*==============================================================*/
/* Index: RM_PROCESS_PK                                         */
/*==============================================================*/
create unique index RM_PROCESS_PK on RM_PROCESS (
FKCONTEXT
);

/*==============================================================*/
/* Index: FKAREA_FK                                             */
/*==============================================================*/
create  index FKAREA_FK on RM_PROCESS (
FKAREA
);

/*==============================================================*/
/* Index: FKRESPONSIBLE_FK2                                     */
/*==============================================================*/
create  index FKRESPONSIBLE_FK2 on RM_PROCESS (
FKRESPONSIBLE
);

/*==============================================================*/
/* Index: FKTYPE_FK2                                            */
/*==============================================================*/
create  index FKTYPE_FK2 on RM_PROCESS (
FKTYPE
);

/*==============================================================*/
/* Index: FKPRIORITY_FK2                                        */
/*==============================================================*/
create  index FKPRIORITY_FK2 on RM_PROCESS (
FKPRIORITY
);

/*==============================================================*/
/* Table: RM_PROCESS_ASSET                                      */
/*==============================================================*/
create table RM_PROCESS_ASSET (
FKCONTEXT            INT4                 not null,
FKPROCESS            INT4                 null,
FKASSET              INT4                 null,
constraint PK_RM_PROCESS_ASSET primary key (FKCONTEXT)
);

/*==============================================================*/
/* Index: RM_PROCESS_ASSET_PK                                   */
/*==============================================================*/
create unique index RM_PROCESS_ASSET_PK on RM_PROCESS_ASSET (
FKCONTEXT
);

/*==============================================================*/
/* Index: FKASSET_FK                                            */
/*==============================================================*/
create  index FKASSET_FK on RM_PROCESS_ASSET (
FKASSET
);

/*==============================================================*/
/* Index: FKPROCESS_FK                                          */
/*==============================================================*/
create  index FKPROCESS_FK on RM_PROCESS_ASSET (
FKPROCESS
);

/*==============================================================*/
/* Table: RM_RC_PARAMETER_VALUE_NAME                            */
/*==============================================================*/
create table RM_RC_PARAMETER_VALUE_NAME (
PKRCVALUENAME        SERIAL               not null,
NVALUE               INT4                 null default 0,
SRCPROBABILITY       VARCHAR(256)         null,
SCONTROLIMPACT       VARCHAR(256)         null,
constraint PK_RM_RC_PARAMETER_VALUE_NAME primary key (PKRCVALUENAME)
);

/*==============================================================*/
/* Index: RM_RC_PARAMETER_VALUE_NAME_PK                         */
/*==============================================================*/
create unique index RM_RC_PARAMETER_VALUE_NAME_PK on RM_RC_PARAMETER_VALUE_NAME (
PKRCVALUENAME
);

/*==============================================================*/
/* Table: RM_RISK                                               */
/*==============================================================*/
create table RM_RISK (
FKCONTEXT            INT4                 not null,
FKPROBABILITYVALUENAME INT4                 null,
FKTYPE               INT4                 null,
FKEVENT              INT4                 null,
FKASSET              INT4                 not null,
SNAME                VARCHAR(256)         not null,
TDESCRIPTION         TEXT                 null,
NVALUE               FLOAT8               null default '0',
NVALUERESIDUAL       FLOAT8               null default '0',
TJUSTIFICATION       TEXT                 null,
NACCEPTMODE          INT4                 null default 0,
NACCEPTSTATE         INT4                 null default 0,
SACCEPTJUSTIFICATION VARCHAR(256)         null,
NCOST                FLOAT8               null default '0',
TIMPACT              TEXT                 null,
constraint PK_RM_RISK primary key (FKCONTEXT)
);

/*==============================================================*/
/* Table: TMP_AD_USER                                           */
/*==============================================================*/
CREATE TABLE TMP_AD_USER (
  sname VARCHAR(256),
  slogin VARCHAR(256),
  semail VARCHAR(256)
);

/*==============================================================*/
/* Index: RM_RISK_PK                                            */
/*==============================================================*/
create unique index RM_RISK_PK on RM_RISK (
FKCONTEXT
);

/*==============================================================*/
/* Index: FKASSET_FK2                                           */
/*==============================================================*/
create  index FKASSET_FK2 on RM_RISK (
FKASSET
);

/*==============================================================*/
/* Index: FKEVENT_FK                                            */
/*==============================================================*/
create  index FKEVENT_FK on RM_RISK (
FKEVENT
);

/*==============================================================*/
/* Index: FKPROBABILITYVALUENAME_FK                             */
/*==============================================================*/
create  index FKPROBABILITYVALUENAME_FK on RM_RISK (
FKPROBABILITYVALUENAME
);

/*==============================================================*/
/* Index: FKTYPE_FK4                                            */
/*==============================================================*/
create  index FKTYPE_FK4 on RM_RISK (
FKTYPE
);

/*==============================================================*/
/* Table: RM_RISK_CONTROL                                       */
/*==============================================================*/
create table RM_RISK_CONTROL (
FKCONTEXT            INT4                 not null,
FKPROBABILITYVALUENAME INT4                 null,
FKRISK               INT4                 null,
FKCONTROL            INT4                 null,
TJUSTIFICATION       TEXT                 null,
constraint PK_RM_RISK_CONTROL primary key (FKCONTEXT)
);

/*==============================================================*/
/* Index: RM_RISK_CONTROL_PK                                    */
/*==============================================================*/
create unique index RM_RISK_CONTROL_PK on RM_RISK_CONTROL (
FKCONTEXT
);

/*==============================================================*/
/* Index: FKRISK_FK                                             */
/*==============================================================*/
create  index FKRISK_FK on RM_RISK_CONTROL (
FKRISK
);

/*==============================================================*/
/* Index: FKCONTROL_FK                                          */
/*==============================================================*/
create  index FKCONTROL_FK on RM_RISK_CONTROL (
FKCONTROL
);

/*==============================================================*/
/* Index: FKPROBABILITYVALUENAME_FK2                            */
/*==============================================================*/
create  index FKPROBABILITYVALUENAME_FK2 on RM_RISK_CONTROL (
FKPROBABILITYVALUENAME
);

/*==============================================================*/
/* Table: RM_RISK_CONTROL_VALUE                                 */
/*==============================================================*/
create table RM_RISK_CONTROL_VALUE (
FKRISKCONTROL        INT4                 not null,
FKPARAMETERNAME      INT4                 not null,
FKRCVALUENAME        INT4                 not null,
TJUSTIFICATION       TEXT                 null,
constraint PK_RM_RISK_CONTROL_VALUE primary key (FKRISKCONTROL, FKPARAMETERNAME, FKRCVALUENAME)
);

/*==============================================================*/
/* Index: RM_RISK_CONTROL_VALUE_PK                              */
/*==============================================================*/
create unique index RM_RISK_CONTROL_VALUE_PK on RM_RISK_CONTROL_VALUE (
FKRISKCONTROL,
FKPARAMETERNAME,
FKRCVALUENAME
);

/*==============================================================*/
/* Index: FKRISKCONTROL_FK                                      */
/*==============================================================*/
create  index FKRISKCONTROL_FK on RM_RISK_CONTROL_VALUE (
FKRISKCONTROL
);

/*==============================================================*/
/* Index: FKPARAMETERNAME_FK3                                   */
/*==============================================================*/
create  index FKPARAMETERNAME_FK3 on RM_RISK_CONTROL_VALUE (
FKPARAMETERNAME
);

/*==============================================================*/
/* Index: FKRCVALUENAME_FK                                      */
/*==============================================================*/
create  index FKRCVALUENAME_FK on RM_RISK_CONTROL_VALUE (
FKRCVALUENAME
);

/*==============================================================*/
/* Table: RM_RISK_LIMITS                                        */
/*==============================================================*/
create table RM_RISK_LIMITS (
FKCONTEXT            INT4                 not null,
NLOW                 FLOAT8               not null,
NHIGH                FLOAT8               not null,
NMIDLOW                 FLOAT8               not null,
NMIDHIGH                FLOAT8               not null,
constraint PK_RM_RISK_LIMITS primary key (FKCONTEXT)
);

/*==============================================================*/
/* Index: RM_RISK_LIMITS_PK                                     */
/*==============================================================*/
create unique index RM_RISK_LIMITS_PK on RM_RISK_LIMITS (
FKCONTEXT
);

/*==============================================================*/
/* Table: RM_RISK_VALUE                                         */
/*==============================================================*/
create table RM_RISK_VALUE (
FKRISK               INT4                 not null,
FKVALUENAME          INT4                 not null,
FKPARAMETERNAME      INT4                 not null,
TJUSTIFICATION       TEXT                 null,
constraint PK_RM_RISK_VALUE primary key (FKRISK, FKVALUENAME, FKPARAMETERNAME)
);

/*==============================================================*/
/* Index: RM_RISK_VALUE_PK                                      */
/*==============================================================*/
create unique index RM_RISK_VALUE_PK on RM_RISK_VALUE (
FKRISK,
FKVALUENAME,
FKPARAMETERNAME
);

/*==============================================================*/
/* Index: FKRISK_FK2                                            */
/*==============================================================*/
create  index FKRISK_FK2 on RM_RISK_VALUE (
FKRISK
);

/*==============================================================*/
/* Index: FKVALUENAME_FK2                                       */
/*==============================================================*/
create  index FKVALUENAME_FK2 on RM_RISK_VALUE (
FKVALUENAME
);

/*==============================================================*/
/* Index: FKPARAMETERNAME_FK2                                   */
/*==============================================================*/
create  index FKPARAMETERNAME_FK2 on RM_RISK_VALUE (
FKPARAMETERNAME
);

/*==============================================================*/
/* Table: RM_SECTION_BEST_PRACTICE                              */
/*==============================================================*/
create table RM_SECTION_BEST_PRACTICE (
FKCONTEXT            INT4                 not null,
FKPARENT             INT4                 null,
SNAME                VARCHAR(256)         not null,
constraint PK_RM_SECTION_BEST_PRACTICE primary key (FKCONTEXT)
);

/*==============================================================*/
/* Index: RM_SECTION_BEST_PRACTICE_PK                           */
/*==============================================================*/
create unique index RM_SECTION_BEST_PRACTICE_PK on RM_SECTION_BEST_PRACTICE (
FKCONTEXT
);

/*==============================================================*/
/* Index: FKPARENT_FK3                                          */
/*==============================================================*/
create  index FKPARENT_FK3 on RM_SECTION_BEST_PRACTICE (
FKPARENT
);

/*==============================================================*/
/* Table: RM_STANDARD                                           */
/*==============================================================*/
create table RM_STANDARD (
FKCONTEXT            INT4                 not null,
SNAME                VARCHAR(256)         not null,
TDESCRIPTION         TEXT                 null,
TAPPLICATION         TEXT                 null,
TOBJECTIVE           TEXT                 null,
constraint PK_RM_STANDARD primary key (FKCONTEXT)
);

/*==============================================================*/
/* Index: RM_STANDARD_PK                                        */
/*==============================================================*/
create unique index RM_STANDARD_PK on RM_STANDARD (
FKCONTEXT
);

/*==============================================================*/
/* Table: WKF_ALERT                                             */
/*==============================================================*/
create table WKF_ALERT (
PKALERT              SERIAL               not null,
FKCONTEXT            INT4                 null,
FKRECEIVER           INT4                 null,
FKCREATOR            INT4                 null,
TJUSTIFICATION       TEXT                 null,
NTYPE                INT4                 null default 0,
BEMAILSENT           INT4                 null default 0 
      constraint CKC_BEMAILSENT_WKF_ALER check (BEMAILSENT is null or (BEMAILSENT between 0 and 1 )),
DDATE                TIMESTAMP            not null,
constraint PK_WKF_ALERT primary key (PKALERT)
);

/*==============================================================*/
/* Index: WKF_ALERT_PK                                          */
/*==============================================================*/
create unique index WKF_ALERT_PK on WKF_ALERT (
PKALERT
);

/*==============================================================*/
/* Index: FKRECEIVER_FK2                                        */
/*==============================================================*/
create  index FKRECEIVER_FK2 on WKF_ALERT (
FKRECEIVER
);

/*==============================================================*/
/* Index: FKCONTEXT_FK2                                         */
/*==============================================================*/
create  index FKCONTEXT_FK2 on WKF_ALERT (
FKCONTEXT
);

/*==============================================================*/
/* Index: FKCREATOR_FK2                                         */
/*==============================================================*/
create  index FKCREATOR_FK2 on WKF_ALERT (
FKCREATOR
);

/*==============================================================*/
/* Table: WKF_CONTROL_EFFICIENCY                                */
/*==============================================================*/
create table WKF_CONTROL_EFFICIENCY (
FKCONTROLEFFICIENCY  INT4                 not null,
FKSCHEDULE           INT4                 null,
NREALEFFICIENCY      INT4                 null default 0,
NEXPECTEDEFFICIENCY  INT4                 null default 0,
SVALUE1              VARCHAR(256)         null,
SVALUE2              VARCHAR(256)         null,
SVALUE3              VARCHAR(256)         null,
SVALUE4              VARCHAR(256)         null,
SVALUE5              VARCHAR(256)         null,
TMETRIC              TEXT                 null,
TOBSERVATION         TEXT                 null,
constraint PK_WKF_CONTROL_EFFICIENCY primary key (FKCONTROLEFFICIENCY)
);

/*==============================================================*/
/* Index: WKF_CONTROL_EFFICIENCY_PK                             */
/*==============================================================*/
create unique index WKF_CONTROL_EFFICIENCY_PK on WKF_CONTROL_EFFICIENCY (
FKCONTROLEFFICIENCY
);

/*==============================================================*/
/* Index: FKSCHEDULE_FK                                         */
/*==============================================================*/
create  index FKSCHEDULE_FK on WKF_CONTROL_EFFICIENCY (
FKSCHEDULE
);

/*==============================================================*/
/* Table: WKF_CONTROL_TEST                                      */
/*==============================================================*/
create table WKF_CONTROL_TEST (
FKCONTROLTEST        INT4                 not null,
FKSCHEDULE           INT4                 null,
TDESCRIPTION         TEXT                 null,
constraint PK_WKF_CONTROL_TEST primary key (FKCONTROLTEST)
);

/*==============================================================*/
/* Index: WKF_CONTROL_TEST_PK                                   */
/*==============================================================*/
create unique index WKF_CONTROL_TEST_PK on WKF_CONTROL_TEST (
FKCONTROLTEST
);

/*==============================================================*/
/* Index: FKSCHEDULE_FK2                                        */
/*==============================================================*/
create  index FKSCHEDULE_FK2 on WKF_CONTROL_TEST (
FKSCHEDULE
);

/*==============================================================*/
/* Table: WKF_SCHEDULE                                          */
/*==============================================================*/
create table WKF_SCHEDULE (
PKSCHEDULE           SERIAL               not null,
DSTART               TIMESTAMP            not null,
DEND                 TIMESTAMP            null,
NTYPE                INT4                 not null default 0,
NPERIODICITY         INT4                 null default 0,
NBITMAP              INT4                 null default 0,
NWEEK                INT4                 null default 0,
NDAY                 INT4                 null default 0,
DNEXTOCCURRENCE      TIMESTAMP            null,
NDAYSTOFINISH        INT4                 null default 0,
DDATELIMIT           TIMESTAMP            null,
constraint PK_WKF_SCHEDULE primary key (PKSCHEDULE)
) WITH (
  OIDS=TRUE
);

/*==============================================================*/
/* Index: WKF_SCHEDULE_PK                                       */
/*==============================================================*/
create unique index WKF_SCHEDULE_PK on WKF_SCHEDULE (
PKSCHEDULE
);

/*==============================================================*/
/* Table: WKF_TASK                                              */
/*==============================================================*/
create table WKF_TASK (
PKTASK               SERIAL               not null,
FKCONTEXT            INT4                 not null,
FKCREATOR            INT4                 not null,
FKRECEIVER           INT4                 not null,
NACTIVITY            INT4                 null default 0,
BVISIBLE             INT4                 null,
BEMAILSENT           INT4                 null default 0 
      constraint CKC_BEMAILSENT_WKF_TASK check (BEMAILSENT is null or (BEMAILSENT between 0 and 1 )),
DDATEACCOMPLISHED    TIMESTAMP            null,
DDATECREATED         TIMESTAMP            null,
constraint PK_WKF_TASK primary key (PKTASK)
) WITH (
  OIDS=TRUE
);

/*==============================================================*/
/* Index: WKF_TASK_PK                                           */
/*==============================================================*/
create unique index WKF_TASK_PK on WKF_TASK (
PKTASK
);

/*==============================================================*/
/* Index: FKCREATOR_FK                                          */
/*==============================================================*/
create  index FKCREATOR_FK on WKF_TASK (
FKCREATOR
);

/*==============================================================*/
/* Index: FKRECEIVER_FK                                         */
/*==============================================================*/
create  index FKRECEIVER_FK on WKF_TASK (
FKRECEIVER
);

/*==============================================================*/
/* Index: FKCONTEXT_FK                                          */
/*==============================================================*/
create  index FKCONTEXT_FK on WKF_TASK (
FKCONTEXT
);

/*==============================================================*/
/* Table: WKF_TASK_SCHEDULE                                     */
/*==============================================================*/
create table WKF_TASK_SCHEDULE (
FKCONTEXT            INT4                 not null,
NACTIVITY            INT4                 not null default 0,
FKSCHEDULE           INT4                 null,
BALERTSENT           INT4                 not null default 0 
      constraint CKC_BALERTSENT_WKF_TASK check (BALERTSENT between 0 and 1),
NALERTTYPE           INT4                 null default 0,
constraint PK_WKF_TASK_SCHEDULE primary key (FKCONTEXT, NACTIVITY)
);

/*==============================================================*/
/* Index: WKF_TASK_SCHEDULE_PK                                  */
/*==============================================================*/
create unique index WKF_TASK_SCHEDULE_PK on WKF_TASK_SCHEDULE (
FKCONTEXT,
NACTIVITY
);

/*==============================================================*/
/* Index: FKCONTEXT_FK7                                         */
/*==============================================================*/
create  index FKCONTEXT_FK7 on WKF_TASK_SCHEDULE (
FKCONTEXT
);

/*==============================================================*/
/* Index: FKSCHEDULE_FK3                                        */
/*==============================================================*/
create  index FKSCHEDULE_FK3 on WKF_TASK_SCHEDULE (
FKSCHEDULE
);
/*==============================================================*/
/* Table: WKF_TASK_METADATA                                     */
/*==============================================================*/
CREATE SEQUENCE wkf_task_pkmetadata_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 5
  CACHE 1;

CREATE TABLE wkf_task_metadata
(
  pktaskmetadata integer NOT NULL DEFAULT nextval('wkf_task_pkmetadata_seq'::regclass),
  fkcontext integer,
  ntype integer,
  fktask integer,
  CONSTRAINT pk_metadata PRIMARY KEY (pktaskmetadata),
  CONSTRAINT fk_context FOREIGN KEY (fkcontext)
      REFERENCES isms_context (pkcontext) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_task FOREIGN KEY (fktask)
      REFERENCES wkf_task (pktask) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
) WITH (OIDS=FALSE);
/*==============================================================*/
/* Table: CI_INCIDENT_RISK_PARAMETER                            */
/*==============================================================*/
CREATE TABLE ci_incident_risk_parameter
(
  fkcontext integer,
  fkincident integer,
  fkparametername integer,
  fkparametervalue integer,
  CONSTRAINT fkcontext FOREIGN KEY (fkcontext)
      REFERENCES isms_context (pkcontext) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fkincident FOREIGN KEY (fkincident)
      REFERENCES ci_incident (fkcontext) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT parameter_name FOREIGN KEY (fkparametername)
      REFERENCES rm_parameter_name (pkparametername) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT parameter_value FOREIGN KEY (fkparametervalue)
      REFERENCES rm_parameter_value_name (pkvaluename) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (OIDS=TRUE);

-- CREATE TABLE isms_context_classification
CREATE TABLE isms_non_conformity_types
(
  pkclassification serial NOT NULL,
  sname character varying(256) NOT NULL,
  CONSTRAINT pk_isms_non_conformity_types PRIMARY KEY (pkclassification)
)
WITH (OIDS=TRUE);
--ALTER TABLE isms_non_conformity_types OWNER TO postgres;

CREATE UNIQUE INDEX isms_non_conformity_types_pk
  ON isms_non_conformity_types
  USING btree
  (pkclassification);

-- cache de dependencias.
CREATE TABLE asset_dependencies_cache
(
  fkasset integer,
  asset_dependents_count bigint,
  asset_dependencies_count bigint,
  CONSTRAINT fkasset FOREIGN KEY (fkasset)
      REFERENCES rm_asset (fkcontext) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (OIDS=FALSE);

-- Table: all_asset_dependencies_cache
CREATE TABLE all_asset_dependencies_cache
(
  asset bigint NOT NULL,
  dependency bigint NOT NULL
)
WITH (
  OIDS=FALSE
);

-- Table: all_asset_dependents_cache
CREATE TABLE all_asset_dependents_cache
(
  asset bigint NOT NULL,
  dependent bigint NOT NULL
)
WITH (
  OIDS=FALSE
);
