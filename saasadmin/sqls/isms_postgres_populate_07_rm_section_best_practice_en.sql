INSERT INTO isms_context (pkContext,nType,nState) VALUES (1600,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1600,null,'ISO 27002 ');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1600,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1600,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1601,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1601,1600,'05 Security policy');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1601,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1601,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1602,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1602,1601,'5.1 Information security policy');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1602,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1602,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1603,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1603,1600,'06 Organization of information security');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1603,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1603,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1604,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1604,1603,'6.1 Internal organization');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1604,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1604,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1605,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1605,1603,'6.2 External parties');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1605,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1605,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1606,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1606,1600,'07 Asset management');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1606,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1606,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1607,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1607,1606,'7.1 Responsibility for assets');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1607,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1607,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1608,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1608,1606,'7.2 Information classification');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1608,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1608,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1609,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1609,1600,'08 Human resources security');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1609,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1609,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1610,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1610,1609,'8.1 Prior to employment');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1610,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1610,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1611,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1611,1609,'8.2 During employment');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1611,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1611,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1612,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1612,1609,'8.3 Termination or change of employment');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1612,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1612,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1613,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1613,1600,'09 Physical and environmental security');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1613,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1613,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1614,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1614,1613,'9.1 Secure areas');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1614,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1614,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1615,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1615,1613,'9.2 Equipment security');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1615,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1615,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1616,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1616,1600,'10 Communications and operations management');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1616,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1616,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1617,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1617,1616,'10.1Operational procedures and responsibilities');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1617,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1617,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1618,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1618,1616,'10.2 Third party service delivery management');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1618,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1618,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1619,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1619,1616,'10.3 System planning and acceptance');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1619,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1619,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1620,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1620,1616,'10.4 Protection against malicious and mobile code');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1620,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1620,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1621,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1621,1616,'10.5 Back-up');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1621,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1621,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1622,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1622,1616,'10.6 Network security management');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1622,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1622,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1623,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1623,1616,'10.7 Media handling');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1623,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1623,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1624,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1624,1616,'10.8 Exchange of information');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1624,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1624,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1625,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1625,1616,'10.9 Electronic commerce services');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1625,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1625,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1626,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1626,1616,'10.10 Monitoring');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1626,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1626,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1627,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1627,1600,'11 Access control');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1627,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1627,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1628,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1628,1627,'11.1 Business requirement for access control');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1628,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1628,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1629,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1629,1627,'11.2 User Access management');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1629,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1629,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1630,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1630,1627,'11.3 User responsibilities');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1630,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1630,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1631,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1631,1627,'11.4 Network access control');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1631,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1631,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1632,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1632,1627,'11.5 Operating system access control');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1632,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1632,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1633,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1633,1627,'11.6 Application and information access control');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1633,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1633,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1634,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1634,1627,'11.7 Mobile computing and teleworking');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1634,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1634,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1635,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1635,1600,'12 Information systems acquisition, development and maintenance');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1635,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1635,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1636,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1636,1635,'12.1 Security requirements of information systems');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1636,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1636,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1637,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1637,1635,'12.2 Correct processing in applications');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1637,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1637,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1638,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1638,1635,'12.3 Cryptographic controls');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1638,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1638,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1639,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1639,1635,'12.4 Security of system files');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1639,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1639,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1640,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1640,1635,'12.5 Security in development and support processes');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1640,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1640,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1641,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1641,1635,'12.6 Technical vulnerability management');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1641,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1641,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1642,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1642,1600,'13 Information security incident management');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1642,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1642,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1643,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1643,1642,'13.1 Reporting information security events and weaknesses');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1643,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1643,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1644,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1644,1642,'13.2 Management of information security incidents and improvements');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1644,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1644,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1645,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1645,1600,'14 Business continuity management');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1645,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1645,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1646,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1646,1645,'14.1 Information security aspects of business continuity management');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1646,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1646,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1647,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1647,1600,'15 Compliance');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1647,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1647,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1648,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1648,1647,'15.1 Compliance with legal requirements');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1648,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1648,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1649,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1649,1647,'15.2 Compliance with security polices and standards, and technical compliance');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1649,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1649,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1650,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1650,1647,'15.3 Information systems audit considerations');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1650,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1650,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1651,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1651,null,'CobiT');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1651,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1651,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1652,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1652,1651,'Plan and Organise');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1652,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1652,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1653,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1653,1652,'PO1Define a Strategic IT Plan');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1653,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1653,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1654,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1654,1652,'PO2Define the Information Architecture');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1654,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1654,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1655,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1655,1652,'PO3 Determine Technological Direction');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1655,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1655,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1656,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1656,1652,'PO4 Define the IT Processes, Organisation and Relationships');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1656,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1656,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1657,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1657,1652,'PO5 Manage the IT Investment');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1657,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1657,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1658,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1658,1652,'PO6 Communicate Management Aims and Direction');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1658,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1658,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1659,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1659,1652,'PO7 Manage IT Human Resources');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1659,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1659,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1660,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1660,1652,'PO8 Manage Quality');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1660,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1660,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1661,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1661,1652,'PO9 Assess and Manage IT Risks');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1661,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1661,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1662,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1662,1652,'PO10 Manage Projects');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1662,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1662,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1663,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1663,1651,'Acquire and Implement');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1663,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1663,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1664,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1664,1663,'AI1 Identify Automated Solutions');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1664,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1664,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1665,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1665,1663,'AI2 Acquire and Maintain Application Software');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1665,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1665,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1666,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1666,1663,'AI3 Acquire and Maintain Technology Infrastructure');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1666,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1666,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1667,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1667,1663,'AI4 Enable Operation and Use');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1667,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1667,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1668,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1668,1663,'AI5 Procure IT Resources');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1668,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1668,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1669,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1669,1663,'AI6 Manage Changes');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1669,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1669,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1670,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1670,1663,'AI7 Install and Accredit Solutions and Changes');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1670,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1670,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1671,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1671,1651,'Deliver and Support');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1671,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1671,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1672,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1672,1671,'DS1 Define and Manage Service Levels');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1672,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1672,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1673,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1673,1671,'DS2 Manage Third-party Services');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1673,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1673,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1674,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1674,1671,'DS3 Manage Performance and Capacity');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1674,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1674,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1675,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1675,1671,'DS4 Ensure Continuous Service');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1675,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1675,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1676,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1676,1671,'DS5 Ensure Systems Security');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1676,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1676,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1677,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1677,1671,'DS6 Identify and Allocate Costs');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1677,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1677,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1678,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1678,1671,'DS7 Educate and Train Users');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1678,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1678,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1679,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1679,1671,'DS8 Manage Service Desk and Incidents');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1679,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1679,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1680,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1680,1671,'DS9 Manage the Configuration');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1680,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1680,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1681,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1681,1671,'DS10 Manage Problems');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1681,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1681,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1682,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1682,1671,'DS11 Manage Data');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1682,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1682,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1683,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1683,1671,'DS12 Manage the Physical Environment');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1683,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1683,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1684,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1684,1671,'DS13 Manage Operations');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1684,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1684,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1685,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1685,1651,'Monitor and Evaluate');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1685,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1685,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1686,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1686,1685,'ME1 Monitor and Evaluate IT Performance');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1686,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1686,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1687,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1687,1685,'ME2 Monitor and Evaluate Internal Control');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1687,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1687,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1688,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1688,1685,'ME3 Ensure Compliance With External Requirements');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1688,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1688,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1689,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1689,1685,'ME4 Provide IT Governance');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1689,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1689,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1690,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1690,null,'PCI');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1690,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1690,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1691,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1691,1690,'Build and Maintain a Secure Network');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1691,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1691,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1692,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1692,1691,'1 Install and maintain a firewall configuration to protect cardholder data');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1692,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1692,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1693,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1693,1691,'2 Do not use vendor-supplied defaults for system passwords and other security parameters.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1693,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1693,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1694,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1694,1690,'Protect Cardholder Data');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1694,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1694,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1695,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1695,1694,'3 Protect stored cardholder data');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1695,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1695,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1696,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1696,1694,'4 Encrypt transmission of cardholder data across open, public networks');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1696,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1696,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1697,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1697,1690,'Maintain a Vulnerability Management Program');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1697,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1697,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1698,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1698,1697,'5 Use and regularly update anti-virus software or programs');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1698,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1698,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1699,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1699,1697,'6 Develop and maintain secure systems and applications');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1699,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1699,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1700,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1700,1690,'Implement Strong Access Control Measures');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1700,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1700,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1701,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1701,1700,'7 Restrict access to cardholder data by business need-to-know');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1701,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1701,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1702,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1702,1700,'8 Assign a unique ID to each person with computer access');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1702,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1702,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1703,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1703,1700,'9 Restrict physical access to cardholder data');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1703,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1703,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1704,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1704,1690,'Regularly Monitor and Test Networks ');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1704,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1704,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1705,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1705,1704,'10 Track and monitor all access to network resources and cardholder data');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1705,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1705,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1706,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1706,1704,'11 Regularly test security systems and processes');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1706,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1706,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1707,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1707,1690,'Maintain an Information Security Policy');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1707,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1707,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1708,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (1708,1707,'12 Maintain a policy that addresses information security for employees and contractors');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1708,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1708,2902,NOW());

