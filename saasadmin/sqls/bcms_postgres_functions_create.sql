CREATE LANGUAGE plpgsql;

CREATE FUNCTION delete_area(integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$DECLARE 
    ctx context_id%rowtype;
BEGIN
   FOR ctx IN EXECUTE ' SELECT fkContext FROM rm_area WHERE fkParent = ' || $1 LOOP
	if(ctx.id IS NOT NULL) THEN	
		PERFORM delete_area(ctx.id);
	END IF;
   END LOOP;
   DELETE FROM rm_area	WHERE fkContext = $1;
   DELETE FROM isms_context WHERE pkContext = $1;
   RETURN 1;	
END$$;
CREATE FUNCTION delete_category(integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$DECLARE 
    ctx context_id%rowtype;
BEGIN
   FOR ctx IN EXECUTE ' SELECT fkContext FROM rm_category WHERE fkParent = ' || $1 LOOP
	if(ctx.id IS NOT NULL) THEN	
		PERFORM delete_category(ctx.id);
	END IF;
   END LOOP;

   DELETE FROM rm_category  WHERE fkContext = $1;
   DELETE FROM isms_context WHERE pkContext = $1;
   RETURN 1;	
END$$;
CREATE FUNCTION delete_document(iddoc integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
  DECLARE
    doc pm_document%ROWTYPE;
  BEGIN
    FOR doc IN SELECT fkContext FROM pm_document WHERE fkParent = idDoc LOOP
      PERFORM delete_document(doc.fkContext);
    END LOOP;
    DELETE FROM pm_document WHERE fkContext = idDoc;
    DELETE FROM isms_context WHERE pkContext = idDoc;
    RETURN 1;
  END
$$;
CREATE FUNCTION delete_section(integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$DECLARE 
    ctx context_id%rowtype;
BEGIN
   FOR ctx IN EXECUTE ' SELECT fkContext FROM rm_section_best_practice WHERE fkParent = ' || $1 LOOP
	if(ctx.id IS NOT NULL) THEN	
		PERFORM delete_section(ctx.id);
	END IF;
   END LOOP;

   DELETE FROM rm_section_best_practice  WHERE fkContext = $1;
   DELETE FROM isms_context WHERE pkContext = $1;
   RETURN 1;	
END$$;
CREATE FUNCTION delete_user() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN

IF (TG_OP = 'DELETE') THEN
    UPDATE isms_context_date
    SET nUserId = 0
    WHERE nUserId = OLD.fkContext;
END IF;
RETURN NULL;
END$$;
CREATE FUNCTION get_all_asset_and_dependencies() RETURNS SETOF asset_dependency
    LANGUAGE plpgsql
    AS $$DECLARE 
asset_id context_id%rowtype;
asset_dep_id context_id%rowtype;
asset_assoc asset_dependency%rowtype;
BEGIN
   FOR asset_id IN EXECUTE 'SELECT fkContext FROM view_rm_asset_active' LOOP
	    FOR asset_dep_id IN EXECUTE 'SELECT pkAsset FROM get_asset_dependencies('|| asset_id.id ||')' LOOP
		asset_assoc.asset = asset_id.id;
		asset_assoc.dependency = asset_dep_id.id;
		RETURN NEXT asset_assoc;
	    END LOOP;	
   END LOOP;
   RETURN;	
END$$;
CREATE FUNCTION get_all_asset_and_dependents() RETURNS SETOF asset_dependent
    LANGUAGE plpgsql
    AS $$DECLARE
asset_id context_id%rowtype;
asset_dep_id context_id%rowtype;
asset_assoc asset_dependent%rowtype;
BEGIN
   FOR asset_id IN EXECUTE 'SELECT fkContext FROM view_rm_asset_active' LOOP
	    FOR asset_dep_id IN EXECUTE 'SELECT pkAsset FROM get_asset_dependents('|| asset_id.id ||')' LOOP
		asset_assoc.asset = asset_id.id;
		asset_assoc.dependent = asset_dep_id.id;
		RETURN NEXT asset_assoc;
	    END LOOP;
   END LOOP;
   RETURN;
END$$;
CREATE FUNCTION get_area_parents(integer) RETURNS SETOF area_id
    LANGUAGE plpgsql
    AS $$DECLARE 
area_id ALIAS FOR $1;
query_area_parents varchar(255);
area_ids context_id%rowtype;
area_rec context_id%rowtype;
BEGIN
   query_area_parents := ' SELECT a.fkParent FROM rm_area a
			 JOIN isms_context c ON(c.pkContext = a.fkContext and c.nState <> 2705)
			     WHERE fkContext = ' ||area_id;
   
   FOR area_ids IN EXECUTE query_area_parents LOOP
	IF(area_ids.id IS NOT NULL) THEN
	    RETURN NEXT area_ids;
	    FOR area_rec IN EXECUTE 'SELECT pkArea FROM get_area_parents('|| area_ids.id ||')' LOOP
		RETURN NEXT area_rec;
	    END LOOP;
	END IF;
   END LOOP;
   RETURN ;	
END$$;
CREATE FUNCTION get_area_parents_trash(integer) RETURNS SETOF area_id
    LANGUAGE plpgsql
    AS $$DECLARE 
area_id ALIAS FOR $1;
query_area_parents varchar(255);
area_ids context_id%rowtype;
area_rec context_id%rowtype;
BEGIN
   query_area_parents := ' SELECT a.fkParent FROM rm_area a WHERE fkContext = ' ||area_id;
   
   FOR area_ids IN EXECUTE query_area_parents LOOP
	IF(area_ids.id IS NOT NULL) THEN
	    RETURN NEXT area_ids;
	    FOR area_rec IN EXECUTE 'SELECT pkArea FROM get_area_parents_trash('|| area_ids.id ||')' LOOP
		RETURN NEXT area_rec;
	    END LOOP;
	END IF;
   END LOOP;
   RETURN ;	
END$$;
CREATE FUNCTION get_area_tree(integer, integer) RETURNS SETOF tree_area
    LANGUAGE plpgsql
    AS $$DECLARE
   piParent ALIAS FOR $1;
   piLevel ALIAS FOR $2;
   maArea tree_area%rowtype;
   maAreaAux tree_area_aux%rowtype;
   miId integer;
BEGIN
     miId:=1;
     FOR maAreaAux IN EXECUTE  ' SELECT * FROM get_area_tree_aux('|| piParent ||','|| piLevel ||') ' LOOP
        maArea.pkId := miId;
        maArea.fkContext := maAreaAux.fkContext;
        maArea.sName := maAreaAux.sName;
        maArea.nLevel := maAreaAux.nLevel;
	miId:=miId+1;	
	RETURN NEXT maArea;
     END LOOP;
     RETURN ;
END$$;
CREATE FUNCTION get_area_tree_aux(integer, integer) RETURNS SETOF tree_area_aux
    LANGUAGE plpgsql
    AS $$DECLARE
   piParent ALIAS FOR $1;
   piLevel ALIAS FOR $2;
   nAuxLevel integer;
   queryArea varchar(1024);
   queryAreaAux varchar(1024);
   queryAreaRecursion varchar(255);
   areaRoot tree_area_aux%rowtype;
   subArea tree_area_aux%rowtype;

BEGIN
     nAuxLevel := piLevel + 1;
     queryAreaAux := ' SELECT fkContext, sName, ' || nAuxLevel::text || ' as nLevel FROM rm_area a
		JOIN isms_context c ON(c.pkContext = a.fkContext and c.nState <> 2705)	
		WHERE  fkParent '  ;
     IF piParent > 0 THEN
       queryArea := queryAreaAux || ' = ' || piParent;
     ELSE 
       queryArea := queryAreaAux || ' is NULL';
     END IF;
     FOR areaRoot IN EXECUTE queryArea LOOP
        RETURN NEXT areaRoot;
        queryAreaRecursion := 'SELECT fkContext, sName, nLevel FROM get_area_tree_aux(' || areaRoot.fkContext::text || ' , ' || nAuxLevel::text || ' ) ';
        FOR subArea IN EXECUTE queryAreaRecursion LOOP
	    RETURN NEXT subArea;
        END LOOP;
     END LOOP;
     RETURN ;
END$$;
CREATE FUNCTION get_area_value(integer) RETURNS double precision
    LANGUAGE sql
    AS $$
SELECT MAX(nValue) FROM (
        SELECT nValue 
          FROM rm_process p
	  JOIN isms_context ctx1 ON(ctx1.pkContext = p.fkContext and ctx1.nState <> 2705 and p.fkArea = $1)
      UNION
        SELECT nValue
          FROM rm_area a
          JOIN isms_context ctx2 ON(ctx2.pkContext = a.fkContext and ctx2.nState <> 2705 and a.fkParent = $1)
      UNION
	SELECT 0 as value
     ) as buffer
$$;
CREATE FUNCTION get_areas_and_subareas_by_user(integer) RETURNS SETOF area_ids
    LANGUAGE plpgsql
    AS $$DECLARE 
    userId ALIAS FOR $1;
    query_area varchar(255);
    area context_id%rowtype;
    subArea context_id%rowtype;
    areaParent context_id%rowtype;
BEGIN
   query_area := ' SELECT fkContext FROM rm_area a 
		    JOIN isms_context c ON(c.pkContext = a.fkContext and c.nState <> 2705) 
		        WHERE a.fkResponsible = ' || userId;

   FOR area IN EXECUTE query_area LOOP
	RETURN NEXT area;
        FOR subArea IN EXECUTE ' SELECT pkArea FROM get_sub_areas(' || area.id || ') ' LOOP
	    RETURN NEXT subArea;
        END LOOP;
   END LOOP;
   RETURN ;	
END$$;
CREATE FUNCTION get_areas_by_user(integer) RETURNS SETOF area_ids
    LANGUAGE plpgsql
    AS $$DECLARE 
    userId ALIAS FOR $1;
    query_area varchar(255);
    area context_id%rowtype;
    subArea context_id%rowtype;
    areaParent context_id%rowtype;
BEGIN
   query_area := ' SELECT fkContext FROM rm_area a 
		    JOIN isms_context c ON(c.pkContext = a.fkContext and c.nState <> 2705) 
		        WHERE a.fkResponsible = ' || userId;

   FOR area IN EXECUTE query_area LOOP
	RETURN NEXT area;

        FOR subArea IN EXECUTE ' SELECT pkArea FROM get_area_parents(' || area.id || ') ' LOOP
	    RETURN NEXT subArea;
        END LOOP;

        FOR subArea IN EXECUTE ' SELECT pkArea FROM get_sub_areas(' || area.id || ') ' LOOP
	    RETURN NEXT subArea;
        END LOOP;

   END LOOP;

   RETURN ;	
END$$;
CREATE FUNCTION get_asset_category_parents(integer) RETURNS SETOF category_id
    LANGUAGE plpgsql
    AS $$DECLARE
piCategory ALIAS FOR $1;
queryCat varchar(255);
queryCatRecursion varchar(255);
category context_id%rowtype;
catParent context_id%rowtype;
BEGIN
     queryCat := ' SELECT cat.fkParent FROM cm_asset_category cat
JOIN isms_context c ON(c.pkContext = cat.fkContext and c.nState <> 2705)
WHERE  cat.fkContext = ' || piCategory;
     FOR category IN EXECUTE queryCat LOOP
if(category.id IS NOT NULL) THEN
    RETURN NEXT category;
    FOR catParent IN EXECUTE 'SELECT pkCategory FROM get_asset_category_parents('|| category.id ||')' LOOP
      RETURN NEXT catParent;
    END LOOP;
END IF;
     END LOOP;
     RETURN ;
END$$;
CREATE FUNCTION get_asset_category_parents_threats() RETURNS SETOF asset_category_threats
    LANGUAGE plpgsql
    AS $$DECLARE
queryCat varchar(1000);
queryCatRecursion varchar(255);
asset rm_asset%rowtype;
parent asset_category_threats%rowtype;
BEGIN
     FOR asset IN EXECUTE 'SELECT p.* from view_rm_asset_active p' LOOP
if(asset.fkCategory IS NOT NULL) THEN
    FOR parent IN EXECUTE 'SELECT * FROM get_asset_category_parents_threats('|| asset.fkCategory ||')' LOOP
      parent.fkasset = asset.fkcontext;
      RETURN NEXT parent;
    END LOOP;
END IF;
     END LOOP;
     RETURN ;
END$$;
CREATE FUNCTION get_asset_category_parents_threats(integer) RETURNS SETOF asset_category_threat
    LANGUAGE plpgsql
    AS $$DECLARE
piCategory ALIAS FOR $1;
queryCat varchar(1000);
queryCatRecursion varchar(255);
category asset_category_threat%rowtype;
catParent asset_category_threat%rowtype;
c varchar(1);
BEGIN
     queryCat := ' SELECT cat.fkcontext, pct.fkthreat, cat.fkparent FROM cm_asset_category cat
JOIN isms_context c ON(c.pkContext = cat.fkContext and c.nState <> 2705)
LEFT JOIN (select pct2.* from cm_asset_category_threat pct2 join view_cm_threat_active ta on ta.fkcontext=pct2.fkthreat)  
      pct on pct.fkassetcategory = cat.fkcontext
WHERE cat.fkContext = ' || piCategory;
     c := '1';
     FOR category IN EXECUTE queryCat LOOP
if(category.fkthreat IS NOT NULL) THEN
   RETURN NEXT category;
END IF;
if(category.fkParent IS NOT NULL AND c = '1') THEN
    c := '2';
    FOR catParent IN EXECUTE 'SELECT * FROM get_asset_category_parents_threats('|| category.fkParent ||')' LOOP
      RETURN NEXT catParent;
    END LOOP;
END IF;
     END LOOP;
     RETURN ;
END$$;
CREATE FUNCTION get_asset_category_tree(integer, integer) RETURNS SETOF tree_category
    LANGUAGE plpgsql
    AS $$DECLARE
piParent ALIAS FOR $1;
pnLevel ALIAS FOR $2;
nAuxLevel integer;
queryCat varchar(255);
queryCatAux varchar(255);
queryCatRecursion varchar(255);
categoryRoot tree_category%rowtype;
subCat tree_category%rowtype;
BEGIN
     nAuxLevel := pnLevel + 1;
     queryCatAux := ' SELECT fkContext, sName, ' || nAuxLevel::text || ' as nLevel FROM view_cm_asset_category_active cat WHERE  fkParent '  ;
     IF piParent > 0 THEN
       queryCat := queryCatAux || ' = ' || piParent;
     ELSE 
       queryCat := queryCatAux || ' is NULL';
     END IF;
     FOR categoryRoot IN EXECUTE queryCat LOOP
        RETURN NEXT categoryRoot;
        queryCatRecursion := 'SELECT fkContext, sName, nLevel FROM get_asset_category_tree(' || categoryRoot.fkContext::text || ' , ' || nAuxLevel::text || ' ) ';
        FOR subCat IN EXECUTE queryCatRecursion LOOP
    RETURN NEXT subCat;
        END LOOP;
     END LOOP;
     RETURN ;
END$$;
CREATE FUNCTION get_asset_dependencies(integer) RETURNS SETOF asset_id
    LANGUAGE plpgsql
    AS $$DECLARE 
asset_id ALIAS FOR $1;
query_asset_dependencies varchar(255);
asset_ids context_id%rowtype;
asset_rec context_id%rowtype;
BEGIN
   query_asset_dependencies := 'SELECT fkAsset FROM view_rm_asset_asset_active  WHERE fkDependent = ' || asset_id;
   
   FOR asset_ids IN EXECUTE query_asset_dependencies LOOP
	IF(asset_ids.id IS NOT NULL) THEN
	    RETURN NEXT asset_ids;
	    FOR asset_rec IN EXECUTE 'SELECT pkAsset FROM get_asset_dependencies('|| asset_ids.id ||')' LOOP					
		RETURN NEXT asset_rec;
	    END LOOP;
	END IF;
   END LOOP;
   RETURN ;
END$$;
CREATE FUNCTION get_asset_dependents(integer) RETURNS SETOF asset_id
    LANGUAGE plpgsql
    AS $$DECLARE 
asset_id ALIAS FOR $1;
query_asset_dependents varchar(255);
asset_ids context_id%rowtype;
asset_rec context_id%rowtype;
BEGIN
   query_asset_dependents := 'SELECT fkDependent FROM view_rm_asset_asset_active  WHERE fkAsset = ' || asset_id;
   
   FOR asset_ids IN EXECUTE query_asset_dependents LOOP
	IF(asset_ids.id IS NOT NULL) THEN
	    RETURN NEXT asset_ids;
	    FOR asset_rec IN EXECUTE 'SELECT pkAsset FROM get_asset_dependents('|| asset_ids.id ||')' LOOP					
		RETURN NEXT asset_rec;
	    END LOOP;
	END IF;
   END LOOP;
   RETURN ;
END$$;
CREATE FUNCTION get_asset_value(integer) RETURNS double precision
    LANGUAGE plpgsql
    AS $$
 DECLARE
  miResult double precision;
  miAssetValue row_double%rowtype;
  miBlueRisk integer;
  assetCount asset_value_cont%rowtype;
BEGIN
  miResult := 0;
  miBlueRisk := 0; 

  FOR assetCount IN 
    SELECT count_sys, count_asset 
      FROM (SELECT count(pkparametername) as count_sys  FROM rm_parameter_name pn) buffer_sys,
           (SELECT count(fkparametername) as count_asset 
              FROM rm_asset_value av
                JOIN isms_context ctx ON(ctx.pkContext = av.fkAsset AND ctx.nState <> 2705 AND av.fkAsset = $1)
            ) buffer_asset
  LOOP
    IF (assetCount.cont_system <> assetCount.cont_asset) THEN
      miBlueRisk := 1;
    END IF;
  END LOOP;

 IF (miBlueRisk = 1) THEN
    miResult = 0;
 ELSE
    FOR miAssetValue IN
	SELECT MAX(value) FROM (
	  SELECT MAX(nvalueresidual) as value
	    FROM view_rm_risk_active as r
	    WHERE r.fkasset = $1 AND (nAcceptMode = 0)
	  UNION
	  SELECT MAX(0.1) as value
	    FROM view_rm_risk_active as r
	    WHERE r.fkasset = $1 AND (nAcceptMode <> 0)
	  UNION
	  SELECT MAX(nvalue) as value
	    FROM view_rm_asset_asset_active aa
	      JOIN view_rm_asset_active a ON(aa.fkAsset = a.fkContext AND aa.fkdependent = $1)
	  UNION 
	  SELECT 0 as value
	) as buffer
    LOOP
	miResult = miResult + miAssetValue.value;
    END LOOP;
  END IF;

  RETURN miResult;
END$$;
CREATE FUNCTION get_assets_from_category(integer) RETURNS SETOF asset_id
    LANGUAGE plpgsql
    AS $$DECLARE
piCategory ALIAS FOR $1;
queryCat varchar(255);
queryAsset varchar(255);
assetCat context_id%rowtype;
category context_id%rowtype;
assetSubCat context_id%rowtype;
BEGIN
     queryCat := ' SELECT fkContext FROM rm_category cat
		JOIN isms_context c ON(c.pkContext = cat.fkContext and c.nState <> 2705)	
		WHERE  fkParent = ' || piCategory;

     queryAsset := ' SELECT fkContext FROM rm_asset a
		JOIN isms_context c ON(c.pkContext = a.fkContext and c.nState <> 2705)	
		WHERE fkCategory = ' || piCategory;

     FOR assetCat IN EXECUTE queryAsset LOOP
        if(assetCat.id IS NOT NULL) THEN
		RETURN NEXT assetCat;
	END IF;
     END LOOP;

     FOR category IN EXECUTE queryCat LOOP
        if(category.id IS NOT NULL) THEN
	        FOR assetSubCat IN EXECUTE ' SELECT pkAsset FROM get_assets_from_category(' || category.id || ') ' LOOP
		        RETURN NEXT assetSubCat;
	        END LOOP;
	    END IF;
     END LOOP;

     RETURN ;
END$$;
CREATE FUNCTION get_bp_from_section(integer) RETURNS SETOF best_practices
    LANGUAGE plpgsql
    AS $$DECLARE
   piSection ALIAS FOR $1;
   querySubSec varchar(255);
   queryBPfromSec varchar(255);
   ctxBP context_id%rowtype;
   ctxSubSec context_id%rowtype;
   ctxSubSecAux context_id%rowtype;
BEGIN
     querySubSec := ' SELECT fkContext
                       FROM rm_section_best_practice bp
		       JOIN isms_context c ON(c.pkContext = bp.fkContext and c.nState <> 2705)	
                       WHERE fkParent = ' || piSection;

     queryBPfromSec := ' SELECT fkContext
                          FROM rm_best_practice bp
                          JOIN isms_context c ON(c.pkContext = bp.fkContext and c.nState <> 2705)	
                              WHERE fkSectionBestPractice = ' || piSection;

     FOR ctxBP IN EXECUTE queryBPfromSec LOOP
        if(ctxBP.id IS NOT NULL) THEN
		RETURN NEXT ctxBP;
	END IF;
     END LOOP;

     FOR ctxSubSec IN EXECUTE querySubSec LOOP
        if(ctxSubSec.id IS NOT NULL) THEN
	    FOR ctxSubSecAux IN EXECUTE ' SELECT pkBestPractice FROM get_bp_from_section( ' || ctxSubSec.id || ') ' LOOP
		RETURN NEXT ctxSubSecAux;
	    END LOOP;
	END IF;
     END LOOP;

     RETURN ;
END$$;
CREATE FUNCTION get_category_parents(integer) RETURNS SETOF category_id
    LANGUAGE plpgsql
    AS $$DECLARE
piCategory ALIAS FOR $1;
queryCat varchar(255);
queryCatRecursion varchar(255);
category context_id%rowtype;
catParent context_id%rowtype;
BEGIN
     queryCat := ' SELECT cat.fkParent FROM rm_category cat
		JOIN isms_context c ON(c.pkContext = cat.fkContext and c.nState <> 2705)	
		WHERE  cat.fkContext = ' || piCategory;

     FOR category IN EXECUTE queryCat LOOP
	if(category.id IS NOT NULL) THEN
	    RETURN NEXT category;
	    FOR catParent IN EXECUTE 'SELECT pkCategory FROM get_category_parents('|| category.id ||')' LOOP
	      RETURN NEXT catParent;
	    END LOOP;
	END IF;
     END LOOP;

     RETURN ;
END$$;
CREATE FUNCTION get_category_parents_trash(integer) RETURNS SETOF category_id
    LANGUAGE plpgsql
    AS $$DECLARE
piCategory ALIAS FOR $1;
queryCat varchar(255);
queryCatRecursion varchar(255);
category context_id%rowtype;
catParent context_id%rowtype;
BEGIN
     queryCat := ' SELECT cat.fkParent FROM rm_category cat WHERE  cat.fkContext = ' || piCategory;

     FOR category IN EXECUTE queryCat LOOP
	if(category.id IS NOT NULL) THEN
	    RETURN NEXT category;
	    FOR catParent IN EXECUTE 'SELECT pkCategory FROM get_category_parents_trash('|| category.id ||')' LOOP
	      RETURN NEXT catParent;
	    END LOOP;
	END IF;
     END LOOP;

     RETURN ;
END$$;
CREATE FUNCTION get_category_tree(integer, integer) RETURNS SETOF tree_category
    LANGUAGE plpgsql
    AS $$DECLARE
piParent ALIAS FOR $1;
pnLevel ALIAS FOR $2;
nAuxLevel integer;
queryCat varchar(255);
queryCatAux varchar(255);
queryCatRecursion varchar(255);
categoryRoot tree_category%rowtype;
subCat tree_category%rowtype;

BEGIN
     nAuxLevel := pnLevel + 1;
     queryCatAux := ' SELECT fkContext, sName, ' || nAuxLevel::text || ' as nLevel FROM rm_category cat
		JOIN isms_context c ON(c.pkContext = cat.fkContext and c.nState <> 2705)	
		WHERE  fkParent '  ;
     IF piParent > 0 THEN
       queryCat := queryCatAux || ' = ' || piParent;
     ELSE 
       queryCat := queryCatAux || ' is NULL';
     END IF;
     FOR categoryRoot IN EXECUTE queryCat LOOP
        RETURN NEXT categoryRoot;
        queryCatRecursion := 'SELECT fkContext, sName, nLevel FROM get_category_tree(' || categoryRoot.fkContext::text || ' , ' || nAuxLevel::text || ' ) ';
        FOR subCat IN EXECUTE queryCatRecursion LOOP
	    RETURN NEXT subCat;
        END LOOP;
     END LOOP;
     RETURN ;
END$$;
CREATE FUNCTION get_ctx_names_by_doc_id(integer) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
   ctx_names varchar(2048);
   ctx_aux_name ctx_names%rowtype;
BEGIN
  FOR ctx_aux_name IN 
    SELECT context_name 
      FROM pm_doc_context d_c 
      JOIN context_names c_n ON (c_n.context_id = d_c.fkContext) 
      JOIN isms_context c ON (c.pkContext = d_c.fkContext AND c.nState <> 2705 ) 
      WHERE d_c.fkDocument = $1
    UNION
    SELECT '%context_scope%' as context_name
      FROM pm_doc_context d_c
      JOIN view_isms_scope_active c_n ON (c_n.fkContext = d_c.fkContext) 
      JOIN isms_context c ON (c.pkContext = d_c.fkContext AND c.nState <> 2705 ) 
      WHERE d_c.fkDocument = $1
    UNION
    SELECT '%context_policy%' as context_name
      FROM pm_doc_context d_c 
      JOIN view_isms_policy_active c_n ON (c_n.fkContext = d_c.fkContext) 
      JOIN isms_context c ON (c.pkContext = d_c.fkContext AND c.nState <> 2705 ) 
      WHERE d_c.fkDocument = $1
  LOOP
     IF ((ctx_aux_name.str IS NOT NULL) AND (ctx_aux_name.str <> '')) THEN
         IF(ctx_names <> '') THEN
     ctx_names := ctx_names || ', ' || ctx_aux_name.str;
         ELSE
     ctx_names := ctx_aux_name.str;
         END IF;
      END IF;
  END LOOP;
     RETURN ctx_names;
END $$;
CREATE FUNCTION get_document_parents(iddoc integer) RETURNS SETOF document_id
    LANGUAGE plpgsql
    AS $$
  DECLARE
    parentId document_id;
  BEGIN
    SELECT fkParent INTO parentId FROM view_pm_document_active WHERE fkContext = idDoc;
    WHILE parentId.document_id IS NOT NULL LOOP
      RETURN NEXT parentId;
      SELECT fkParent INTO parentId FROM view_pm_document_active WHERE fkContext = parentId.document_id;
    END LOOP;
    RETURN;
  END
$$;
CREATE FUNCTION get_document_parents_trash(iddoc integer) RETURNS SETOF document_id
    LANGUAGE plpgsql
    AS $$
  DECLARE
    parentId document_id;
  BEGIN
    SELECT fkParent INTO parentId FROM pm_document WHERE fkContext = idDoc;
    WHILE parentId.document_id IS NOT NULL LOOP
      RETURN NEXT parentId;
      SELECT fkParent INTO parentId FROM pm_document WHERE fkContext = parentId.document_id;
    END LOOP;
    RETURN;
  END
$$;
CREATE FUNCTION get_document_tree(integer, integer, integer) RETURNS SETOF tree_document
    LANGUAGE plpgsql
    AS $$DECLARE
piParent ALIAS FOR $1;
pnLevel ALIAS FOR $2;
pbPublished ALIAS FOR $3;
nAuxLevel integer;
queryDoc varchar(255);
queryDocAux varchar(255);
queryDocRecursion varchar(255);
documentRoot tree_document%rowtype;
subDoc tree_document%rowtype;

BEGIN
  nAuxLevel := pnLevel + 1;
  queryDocAux := ' SELECT fkContext, sName, ' || nAuxLevel::text || ' as nLevel FROM view_pm_document_active WHERE fkParent ';
  IF piParent > 0 THEN
    queryDoc := queryDocAux || ' = ' || piParent;
  ELSE 
    queryDoc := queryDocAux || ' is NULL';
  END IF;
  IF (pbPublished=1) THEN
    FOR documentRoot IN EXECUTE queryDoc LOOP
      IF EXISTS (SELECT fkContext FROM view_pm_published_docs WHERE fkContext = documentRoot.fkContext) THEN
        RETURN NEXT documentRoot;
        queryDocRecursion := 'SELECT fkContext, sName, nLevel FROM get_document_tree(' || documentRoot.fkContext::text || ' , ' || nAuxLevel::text || ' , ' || pbPublished::text || ') ';
        FOR subDoc IN EXECUTE queryDocRecursion LOOP
          RETURN NEXT subDoc;
        END LOOP;
      END IF;
    END LOOP;
  ELSE
    FOR documentRoot IN EXECUTE queryDoc LOOP
      RETURN NEXT documentRoot;
      queryDocRecursion := 'SELECT fkContext, sName, nLevel FROM get_document_tree(' || documentRoot.fkContext::text || ' , ' || nAuxLevel::text || ' , ' || pbPublished::text || ' ) ';
      FOR subDoc IN EXECUTE queryDocRecursion LOOP
        RETURN NEXT subDoc;
      END LOOP;
    END LOOP;
  END IF;
  RETURN ;
END$$;
CREATE FUNCTION get_events_from_category(integer) RETURNS SETOF event_id
    LANGUAGE plpgsql
    AS $$DECLARE
piCategory ALIAS FOR $1;
queryCat varchar(255);
queryEvent varchar(255);
eventCat context_id%rowtype;
category context_id%rowtype;
eventSubCat context_id%rowtype;
BEGIN
     queryCat := ' SELECT fkContext FROM rm_category cat
		JOIN isms_context c ON(c.pkContext = cat.fkContext and c.nState <> 2705)	
		WHERE  fkParent = ' || piCategory;

     queryEvent := ' SELECT fkContext FROM rm_event e
		JOIN isms_context c ON(c.pkContext = e.fkContext and c.nState <> 2705)	
		WHERE fkCategory = ' || piCategory;

     FOR eventCat IN EXECUTE queryEvent LOOP
        if(eventCat.id IS NOT NULL) THEN
		RETURN NEXT eventCat;
	END IF;
     END LOOP;

     FOR category IN EXECUTE queryCat LOOP
        if(category.id IS NOT NULL) THEN
	        FOR eventSubCat IN EXECUTE ' SELECT pkEvent FROM get_events_from_category(' || category.id || ') ' LOOP
		        RETURN NEXT eventSubCat;
	        END LOOP;
	    END IF;
     END LOOP;

     RETURN ;
END$$;
CREATE FUNCTION get_next_date(pdlasttime timestamp without time zone, piperiodunitid integer, piperiodscount integer) RETURNS timestamp without time zone
    LANGUAGE plpgsql
    AS $$DECLARE
  miYear  INT;
  miMonth INT;
  miDay   INT;
  miHour  INT;
  miMin   INT;
  miSec   INT;
  miDaysInMonth INT;
BEGIN
  IF piPeriodUnitId = 7801 THEN
    RETURN pdLastTime::DATE + piPeriodsCount;
  ELSEIF piPeriodUnitId = 7802 THEN
    RETURN pdLastTime::DATE + piPeriodsCount * 7;
  ELSEIF piPeriodUnitId = 7803 THEN
    SELECT
      extract(year   from pdLastTime),
      extract(month  from pdLastTime),
      extract(day    from pdLastTime),
      extract(hour   from pdLastTime),
      extract(minute from pdLastTime),
      extract(second from pdLastTime)
    INTO
      miYear ,
      miMonth,
      miDay  ,
      miHour ,
      miMin  ,
      miSec;
    
    miMonth:= miMonth + piPeriodsCount;
    IF miMonth > 12 THEN
      miYear:= miYear + miMonth/12;
      miMonth:= miMonth % 12;
      IF miMonth = 0 THEN miMonth = 12; END IF;
    END IF;
    IF miDay > 28 THEN
      IF miMonth = 2 THEN
        IF (miYear%4=0 AND miYear%100!=0) OR miYear%400=0 THEN
          miDaysInMonth:= 29;
        ELSE
          miDaysInMonth:= 28;
        END IF;
      ELSEIF miMonth IN (4,6,9,11) THEN
        miDaysInMonth:= 30;
      ELSE
        miDaysInMonth:= 31;
      END IF;
      IF miDay > miDaysInMonth THEN miDay:= miDaysInMonth; END IF;
    END IF;
    RETURN (miYear || '-' || miMonth || '-' || miDay || ' ' || miHour || ':' || miMin || ':' || miSec)::TIMESTAMP;
  ELSE
    RAISE EXCEPTION 'Invalid period unit identifier ''%''.', piPeriodUnitId;
  END IF;
END$$;
CREATE FUNCTION get_place_category_parents(integer) RETURNS SETOF category_id
    LANGUAGE plpgsql
    AS $$DECLARE
piCategory ALIAS FOR $1;
queryCat varchar(255);
queryCatRecursion varchar(255);
category context_id%rowtype;
catParent context_id%rowtype;
BEGIN
     queryCat := ' SELECT cat.fkParent FROM cm_place_category cat
JOIN isms_context c ON(c.pkContext = cat.fkContext and c.nState <> 2705)
WHERE  cat.fkContext = ' || piCategory;
     FOR category IN EXECUTE queryCat LOOP
if(category.id IS NOT NULL) THEN
    RETURN NEXT category;
    FOR catParent IN EXECUTE 'SELECT pkCategory FROM get_place_category_parents('|| category.id ||')' LOOP
      RETURN NEXT catParent;
    END LOOP;
END IF;
     END LOOP;
     RETURN ;
END$$;
CREATE FUNCTION get_place_category_parents_threats() RETURNS SETOF place_category_threats
    LANGUAGE plpgsql
    AS $$DECLARE
queryCat varchar(1000);
queryCatRecursion varchar(255);
place cm_place%rowtype;
parent place_category_threats%rowtype;
BEGIN
     FOR place IN EXECUTE 'SELECT p.* from view_rm_place_active p' LOOP
if(place.fktype IS NOT NULL) THEN
    FOR parent IN EXECUTE 'SELECT * FROM get_place_category_parents_threats('|| place.fktype ||')' LOOP
      parent.fkplace = place.fkcontext;
      RETURN NEXT parent;
    END LOOP;
END IF;
     END LOOP;
     RETURN ;
END$$;
CREATE FUNCTION get_place_category_parents_threats(integer) RETURNS SETOF place_category_threat
    LANGUAGE plpgsql
    AS $$DECLARE
piCategory ALIAS FOR $1;
queryCat varchar(1000);
queryCatRecursion varchar(255);
category place_category_threat%rowtype;
catParent place_category_threat%rowtype;
c varchar(1);
BEGIN
     queryCat := ' SELECT cat.fkcontext, pct.fkthreat, cat.fkparent FROM cm_place_category cat
JOIN isms_context c ON(c.pkContext = cat.fkContext and c.nState <> 2705)
LEFT JOIN (select pct2.* from cm_place_category_threat pct2 join view_cm_threat_active ta on ta.fkcontext=pct2.fkthreat)  
      pct on pct.fkplacecategory = cat.fkcontext
WHERE cat.fkContext = ' || piCategory;
     c := '1';
     FOR category IN EXECUTE queryCat LOOP
if(category.fkthreat IS NOT NULL) THEN
   RETURN NEXT category;
END IF;
if(category.fkParent IS NOT NULL AND c = '1') THEN
    c := '2';
    FOR catParent IN EXECUTE 'SELECT * FROM get_place_category_parents_threats('|| category.fkParent ||')' LOOP
      RETURN NEXT catParent;
    END LOOP;
END IF;
     END LOOP;
     RETURN ;
END$$;
CREATE FUNCTION get_place_category_tree(integer, integer) RETURNS SETOF tree_category
    LANGUAGE plpgsql
    AS $$DECLARE
piParent ALIAS FOR $1;
pnLevel ALIAS FOR $2;
nAuxLevel integer;
queryCat varchar(255);
queryCatAux varchar(255);
queryCatRecursion varchar(255);
categoryRoot tree_category%rowtype;
subCat tree_category%rowtype;
BEGIN
     nAuxLevel := pnLevel + 1;
     queryCatAux := ' SELECT fkContext, sName, ' || nAuxLevel::text || ' as nLevel FROM view_cm_place_category_active cat WHERE  fkParent '  ;
     IF piParent > 0 THEN
       queryCat := queryCatAux || ' = ' || piParent;
     ELSE 
       queryCat := queryCatAux || ' is NULL';
     END IF;
     FOR categoryRoot IN EXECUTE queryCat LOOP
        RETURN NEXT categoryRoot;
        queryCatRecursion := 'SELECT fkContext, sName, nLevel FROM get_place_category_tree(' || categoryRoot.fkContext::text || ' , ' || nAuxLevel::text || ' ) ';
        FOR subCat IN EXECUTE queryCatRecursion LOOP
    RETURN NEXT subCat;
        END LOOP;
     END LOOP;
     RETURN ;
END$$;
CREATE FUNCTION get_place_parents(integer, integer) RETURNS SETOF place_parent
    LANGUAGE plpgsql
    AS $$DECLARE
placeId ALIAS FOR $1;
parentId ALIAS FOR $2;
queryCat varchar(255);
queryCatRecursion varchar(255);
place place_parent%rowtype;
catParent place_parent%rowtype;
BEGIN
     queryCat := ' SELECT '||placeId||' as fkplace, p.fkParent FROM view_cm_place_active p
WHERE  p.fkContext = ' || parentId;
     FOR place IN EXECUTE queryCat LOOP
if(place.fkparent IS NOT NULL) THEN
    RETURN NEXT place;
    FOR catParent IN EXECUTE 'SELECT pp.fkplace, pp.fkparent FROM get_place_parents('||placeId||','|| place.fkparent||') pp' LOOP
      RETURN NEXT catParent;
    END LOOP;
END IF;
     END LOOP;
     RETURN ;
END$$;
CREATE FUNCTION get_place_parents() RETURNS SETOF place_parent
    LANGUAGE plpgsql
    AS $$DECLARE
queryCat varchar(255);
queryCatRecursion varchar(255);
place place_parent%rowtype;
catParent place_parent%rowtype;
BEGIN
     queryCat := ' SELECT fkcontext, fkparent FROM view_cm_place_active';
     FOR place IN EXECUTE queryCat LOOP
if(place.fkparent IS NOT NULL) THEN
    RETURN NEXT place;
    FOR catParent IN EXECUTE 'SELECT pp.fkplace, pp.fkparent FROM get_place_parents('||place.fkplace||','|| place.fkparent||') pp' LOOP
      RETURN NEXT catParent;
    END LOOP;
END IF;
     END LOOP;
     RETURN ;
END$$;
CREATE FUNCTION get_process_category_parents(integer) RETURNS SETOF category_id
    LANGUAGE plpgsql
    AS $$DECLARE
piCategory ALIAS FOR $1;
queryCat varchar(255);
queryCatRecursion varchar(255);
category context_id%rowtype;
catParent context_id%rowtype;
BEGIN
     queryCat := ' SELECT cat.fkParent FROM cm_process_category cat
JOIN isms_context c ON(c.pkContext = cat.fkContext and c.nState <> 2705)
WHERE  cat.fkContext = ' || piCategory;
     FOR category IN EXECUTE queryCat LOOP
if(category.id IS NOT NULL) THEN
    RETURN NEXT category;
    FOR catParent IN EXECUTE 'SELECT pkCategory FROM get_process_category_parents('|| category.id ||')' LOOP
      RETURN NEXT catParent;
    END LOOP;
END IF;
     END LOOP;
     RETURN ;
END$$;
CREATE FUNCTION get_process_category_parents_threats(integer) RETURNS SETOF process_category_threat
    LANGUAGE plpgsql
    AS $$DECLARE
piCategory ALIAS FOR $1;
queryCat varchar(1000);
queryCatRecursion varchar(255);
category process_category_threat%rowtype;
catParent process_category_threat%rowtype;
c varchar(1);
BEGIN
     queryCat := ' SELECT cat.fkcontext, pct.fkthreat, cat.fkparent FROM cm_process_category cat
JOIN isms_context c ON(c.pkContext = cat.fkContext and c.nState <> 2705)
LEFT JOIN (select pct2.* from cm_process_category_threat pct2 join view_cm_threat_active ta on ta.fkcontext=pct2.fkthreat)  
      pct on pct.fkprocesscategory = cat.fkcontext
WHERE cat.fkContext = ' || piCategory;
     c := '1';
     FOR category IN EXECUTE queryCat LOOP
if(category.fkthreat IS NOT NULL) THEN
   RETURN NEXT category;
END IF;
if(category.fkParent IS NOT NULL AND c = '1') THEN
    c := '2';
    FOR catParent IN EXECUTE 'SELECT * FROM get_process_category_parents_threats('|| category.fkParent ||')' LOOP
      RETURN NEXT catParent;
    END LOOP;
END IF;
     END LOOP;
     RETURN ;
END$$;
CREATE FUNCTION get_process_category_parents_threats() RETURNS SETOF process_category_threats
    LANGUAGE plpgsql
    AS $$DECLARE
queryCat varchar(1000);
queryCatRecursion varchar(255);
process rm_process%rowtype;
parent process_category_threats%rowtype;
BEGIN
     FOR process IN EXECUTE 'SELECT p.* from view_rm_process_active p' LOOP
if(process.fktype IS NOT NULL) THEN
    FOR parent IN EXECUTE 'SELECT * FROM get_process_category_parents_threats('|| process.fktype ||')' LOOP
      parent.fkprocess = process.fkcontext;
      RETURN NEXT parent;
    END LOOP;
END IF;
     END LOOP;
     RETURN ;
END$$;
CREATE FUNCTION get_process_category_tree(integer, integer) RETURNS SETOF tree_category
    LANGUAGE plpgsql
    AS $$DECLARE
piParent ALIAS FOR $1;
pnLevel ALIAS FOR $2;
nAuxLevel integer;
queryCat varchar(255);
queryCatAux varchar(255);
queryCatRecursion varchar(255);
categoryRoot tree_category%rowtype;
subCat tree_category%rowtype;
BEGIN
     nAuxLevel := pnLevel + 1;
     queryCatAux := ' SELECT fkContext, sName, ' || nAuxLevel::text || ' as nLevel FROM view_cm_process_category_active cat WHERE  fkParent '  ;
     IF piParent > 0 THEN
       queryCat := queryCatAux || ' = ' || piParent;
     ELSE 
       queryCat := queryCatAux || ' is NULL';
     END IF;
     FOR categoryRoot IN EXECUTE queryCat LOOP
        RETURN NEXT categoryRoot;
        queryCatRecursion := 'SELECT fkContext, sName, nLevel FROM get_process_category_tree(' || categoryRoot.fkContext::text || ' , ' || nAuxLevel::text || ' ) ';
        FOR subCat IN EXECUTE queryCatRecursion LOOP
    RETURN NEXT subCat;
        END LOOP;
     END LOOP;
     RETURN ;
END$$;
CREATE FUNCTION get_process_value(integer) RETURNS double precision
    LANGUAGE sql
    AS $$
   SELECT MAX(value) FROM (
      SELECT MAX(a.nValue) as value
	  FROM rm_asset a
	  JOIN isms_context ctxA ON(ctxA.pkContext = a.fkContext and ctxA.nState <> 2705)
	  JOIN rm_process_asset pa ON(pa.fkAsset = a.fkContext)
	  JOIN isms_context ctxPA ON(ctxPA.pkContext = pa.fkContext and ctxPA.nState <> 2705)
	  WHERE pa.fkProcess = $1
      UNION
	  SELECT 0 as value
   ) as buffer
$$;
CREATE FUNCTION get_processes_from_area(integer) RETURNS SETOF process_id
    LANGUAGE plpgsql
    AS $$DECLARE
   piArea ALIAS FOR $1;
   querySubAreas varchar(255);
   queryProcFromArea varchar(255);
   ctxProc context_id%rowtype;
   ctxArea context_id%rowtype;
   ctxProcAux context_id%rowtype;
BEGIN

     querySubAreas := ' SELECT fkContext FROM rm_area a
	JOIN isms_context c ON(c.pkContext = a.fkContext and c.nState <> 2705)	
        WHERE fkParent = ' || piArea;

     queryProcFromArea := ' SELECT fkContext FROM rm_process p
        JOIN isms_context c ON(c.pkContext = p.fkContext and c.nState <> 2705)	
        WHERE fkArea = ' || piArea;

     FOR ctxProc IN EXECUTE queryProcFromArea LOOP
        if(ctxProc.id IS NOT NULL) THEN
		RETURN NEXT ctxProc;
	END IF;
     END LOOP;

     FOR ctxArea IN EXECUTE querySubAreas LOOP
        if(ctxArea.id IS NOT NULL) THEN
	    FOR ctxProcAux IN EXECUTE ' SELECT pkProcess FROM get_processes_from_area( ' || ctxArea.id || ') ' LOOP
		RETURN NEXT ctxProcAux;
	    END LOOP;
	END IF;
     END LOOP;

     RETURN ;
END$$;
CREATE FUNCTION get_risk_parameter_reduction(integer, integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$DECLARE
  piRisk ALIAS FOR $1;
  piParameter ALIAS FOR $2;
  queryReduction varchar(1024);
  miValue integer;
  handleReduct risk_par_reduction%rowtype;
  
BEGIN
  miValue :=0;

  queryReduction :='
  SELECT parameter_reduction_value, risk_parameter_value FROM 
  (
    SELECT SUM(rcpvn.nvalue) as parameter_reduction_value 
      FROM view_rm_risk_control_active rc
        JOIN rm_risk_control_value rcv ON (rcv.fkriskcontrol = rc.fkcontext)
        JOIN view_rm_control_active c ON (c.fkContext = rc.fkcontrol AND c.bIsActive = 1)
        JOIN rm_rc_parameter_value_name rcpvn ON (rcv.fkrcvaluename = rcpvn.pkrcvaluename)
      WHERE fkrisk = ' || @piRisk || ' AND fkparametername = ' || @piParameter || '
  ) prv,
  (
  SELECT nvalue as risk_parameter_value 
     FROM rm_risk_value rv
        JOIN isms_context ctx ON (ctx.pkContext = rv.fkRisk AND ctx.nState <> 2705)
        JOIN rm_parameter_value_name pvn ON (rv.fkvaluename = pvn.pkvaluename)
     WHERE fkrisk = ' || @piRisk || ' AND fkparametername = ' || @piParameter || '
  ) rpv
      ';
  FOR handleReduct IN EXECUTE queryReduction LOOP
    if(handleReduct.par_red_value IS NOT NULL) THEN
        miValue = handleReduct.risk_par_value - handleReduct.par_red_value;
        if(miValue<1) THEN
             miValue:=1;
        END IF;
    ELSE
      miValue := handleReduct.risk_par_value;
    END IF;
  END LOOP;

  RETURN miValue;
END$$;
CREATE FUNCTION get_risk_value(integer) RETURNS double precision
    LANGUAGE plpgsql
    AS $$DECLARE
  piRisk ALIAS FOR $1;
  queryCompare varchar(1024);
  queryValues varchar(1024);
  queryProb varchar(256);
  riskCont risk_value_cont%rowtype;
  riskVal2 risk_values%rowtype;
  riskProb row_int%rowtype;
  miResult double precision;
  miRes1 double precision;
  miRes2 double precision;
  miParWeight double precision;
  miBlueRisk integer;
  msFormulaType varchar(32);
BEGIN
  miResult := 0;
  miRes1 := 0;
  miRes2 := 0;
  miBlueRisk := 0; 
  miParWeight :=0;

  queryCompare := '
    SELECT count_sys, count_risk, count_asset 
      FROM (SELECT count(pkparametername) as count_sys  FROM rm_parameter_name pn) buffer_sys,
           (SELECT count(fkparametername) as count_risk FROM rm_risk_value WHERE fkrisk =  ' || piRisk || ') buffer_risk,
           (SELECT count(fkparametername) as count_asset 
              FROM rm_asset_value av
                JOIN view_rm_risk_active r ON (r.fkasset = av.fkasset AND r.fkcontext =  ' || piRisk || ')
                JOIN isms_context ctx ON(ctx.pkContext = r.fkAsset AND ctx.nState <> 2705)
            ) buffer_asset
   ';
  queryValues := '
    SELECT pn.sname as parameter_name, pvn.nvalue as risk_value, pvn2.nvalue as asset_value, pn.nweight as parameter_weight 
      FROM view_rm_risk_active r
        JOIN isms_context ctx ON (ctx.pkContext = r.fkAsset AND ctx.nState <> 2705 AND r.fkContext = ' || piRisk || ')
        JOIN rm_risk_value rv ON (rv.fkrisk = r.fkcontext)
        JOIN rm_parameter_value_name pvn ON (rv.fkvaluename = pvn.pkvaluename)
        JOIN rm_parameter_name pn ON (rv.fkparametername = pn.pkparametername)
        JOIN rm_asset_value av ON (pn.pkparametername = av.fkparametername AND av.fkasset = r.fkasset)
        JOIN rm_parameter_value_name pvn2 ON (av.fkvaluename = pvn2.pkvaluename)
  ';
  queryProb :='
    SELECT pvn.nvalue as risk_prob_value 
      FROM view_rm_risk_active r
      JOIN rm_parameter_value_name pvn ON (r.fkprobabilityvaluename = pvn.pkvaluename)
      WHERE r.fkcontext = ' || piRisk || '
   ';

  FOR riskCont IN EXECUTE queryCompare LOOP
    IF ( (riskCont.cont_system <> riskCont.cont_risk) OR (riskCont.cont_system <> riskCont.cont_asset) ) THEN
      miBlueRisk := 1;
    END IF;
  END LOOP;

  SELECT INTO msFormulaType sValue FROM isms_config WHERE pkConfig = 5701;
  
  IF (msFormulaType = '8301') THEN
	    IF(miBlueRisk <> 1) THEN
	      FOR riskVal2 IN EXECUTE queryValues LOOP
      		miResult := miResult + (riskVal2.asset + riskVal2.risk)* riskVal2.par_weight;
      		miParWeight := miParWeight + riskVal2.par_weight;
	      END LOOP;
	      IF(miParWeight >= 1) THEN
      		FOR riskProb IN EXECUTE queryProb LOOP
      		  miResult := (miResult * riskProb.value) / (2 * miParWeight);
      		END LOOP;
	      ELSE
          miResult := 0;
	      END IF;
	    END IF;
  ELSE
	  IF(miBlueRisk <> 1) THEN
	    FOR riskVal2 IN EXECUTE queryValues LOOP
	      miRes1 := miRes1 + (riskVal2.asset * riskVal2.risk) * riskVal2.par_weight;
	      miRes2 := miRes2 + (riskVal2.asset + riskVal2.risk) * riskVal2.par_weight;	      
	      miParWeight := miParWeight + riskVal2.par_weight;
	    END LOOP;
	    miRes1 := miRes1 / miParWeight;
	    miRes2 := miRes2 / miParWeight / 2;
	    IF(miParWeight >= 1) THEN
	      FOR riskProb IN EXECUTE queryProb LOOP		
          miResult := (miRes1 / miRes2) * riskProb.value;
	      END LOOP;
	    ELSE
	      miResult := 0;
	    END IF;
	  END IF;	
  END IF;

  RETURN miResult;
END$$;
CREATE FUNCTION get_risk_value_residual(integer) RETURNS double precision
    LANGUAGE plpgsql
    AS $$DECLARE
  piRisk ALIAS FOR $1;
  queryCompare varchar(1024);
  queryValues varchar(1024);
  queryProb varchar(1024);
  miResult double precision;
  miRes1 double precision;
  miRes2 double precision;
  miProbResult double precision;
  miParWeight double precision;
  miBlueRisk integer;
  riskCont risk_value_cont%rowtype;
  riskProb risk_prob%rowtype;
  riskValue risk_values%rowtype;
  msFormulaType varchar(32);
BEGIN
  miParWeight:=0;
  miBlueRisk := 0;
  miResult := 0;
  miRes1 := 0;
  miRes2 := 0;
  miProbResult := 0;
  queryCompare := '
    SELECT count_sys, count_risk, count_asset
      FROM (SELECT count(pkparametername) as count_sys  FROM rm_parameter_name pn) buffer_sys,
           (SELECT count(fkparametername) as count_risk FROM rm_risk_value WHERE fkrisk = ' || piRisk || ') buffer_risk,
           (SELECT count(fkparametername) as count_asset FROM rm_asset_value av
              JOIN view_rm_risk_active r ON (r.fkasset = av.fkasset AND r.fkContext = ' || piRisk || ')
              JOIN view_rm_asset_active a ON (a.fkContext = av.fkAsset)
            ) buffer_asset
       ';
  queryValues :='
    SELECT pn.sname as parameter_name,  pn.pkparametername, pvn.nvalue as asset_value, pn.nweight as parameter_weight 
      FROM view_rm_risk_active r
        JOIN isms_context ctx ON(ctx.pkContext = r.fkAsset AND r.fkContext = ' || piRisk || ')
        JOIN rm_asset_value av ON (av.fkasset = r.fkasset)
        JOIN rm_parameter_name pn ON (av.fkparametername = pn.pkparametername)
        JOIN rm_parameter_value_name pvn ON (av.fkvaluename = pvn.pkvaluename)
       ';
  queryProb := '
    SELECT prob_reduction_value, risk_prob_value FROM 
    (
      SELECT sum(rcpvn.nvalue) as prob_reduction_value
        FROM rm_risk_control rc
          JOIN isms_context ctx ON(ctx.pkContext = rc.fkRisk and ctx.nState <> 2705 AND rc.fkrisk = ' || piRisk || ')
          JOIN view_rm_control_active c ON (c.fkContext = rc.fkControl AND c.bIsActive = 1)
          JOIN rm_rc_parameter_value_name rcpvn ON (rc.fkprobabilityvaluename = rcpvn.pkrcvaluename)
      ) prv,
      (
      SELECT pvn.nvalue as risk_prob_value 
        FROM view_rm_risk_active r
          JOIN rm_parameter_value_name pvn ON (r.fkprobabilityvaluename = pvn.pkvaluename AND r.fkContext = ' || piRisk || ')
      ) rpv
  ';
  FOR riskCont IN EXECUTE queryCompare LOOP
    IF((riskCont.cont_system <> riskCont.cont_risk) or (riskCont.cont_system <> riskCont.cont_asset)) THEN
      miBlueRisk:=1;
    END IF;
  END LOOP;
  IF(miBlueRisk <> 1) THEN
    FOR riskProb IN EXECUTE queryProb LOOP
      IF(riskProb.reduct IS NULL) THEN
         miProbResult := riskProb.value;
      ELSE
        miProbResult := riskProb.value - riskProb.reduct;
        IF(miProbResult < 1) THEN
          miProbResult := 1;
        END IF;
      END IF;
    END LOOP;

    SELECT INTO msFormulaType sValue FROM isms_config WHERE pkConfig = 5701;

    IF (msFormulaType = '8301') THEN
	    FOR riskValue IN EXECUTE queryValues LOOP
	      miParWeight := miParWeight + riskValue.par_weight;
	      miResult := miResult + (riskValue.asset + get_risk_parameter_reduction(piRisk,riskValue.risk) ) * riskValue.par_weight;
	    END LOOP;
	    IF(miParWeight >= 1) THEN
	      miResult := (miResult * miProbResult) / (2 * miParWeight);
	    ELSE
	      miResult := 0;
	    END IF;
     ELSE
      FOR riskValue IN EXECUTE queryValues LOOP
	      miRes1 := miRes1 + (riskValue.asset * get_risk_parameter_reduction(piRisk,riskValue.risk)) * riskValue.par_weight;
	      miRes2 := miRes2 + (riskValue.asset + get_risk_parameter_reduction(piRisk,riskValue.risk)) * riskValue.par_weight;	      
	      miParWeight := miParWeight + riskValue.par_weight;
	    END LOOP;
	    miRes1 := miRes1 / miParWeight;
	    miRes2 := miRes2 / miParWeight / 2;
	    IF(miParWeight >= 1) THEN	      
	      miResult := (miRes1 / miRes2) * miProbResult;	      
	    ELSE
	      miResult := 0;
	    END IF;
     END IF;
  END IF;
  RETURN miResult;
END$$;
CREATE FUNCTION get_risks_from_asset(integer) RETURNS SETOF risk_id
    LANGUAGE plpgsql
    AS $$DECLARE
   piAsset ALIAS FOR $1;
   queryRisks varchar(255);
   riskIds context_id%rowtype;
BEGIN
     queryRisks := '
          SELECT fkContext FROM rm_risk r
          JOIN isms_context ctx1 ON(ctx1.pkContext = r.fkContext and ctx1.nState <> 2705)
          JOIN isms_context ctx2 ON(ctx2.pkContext = r.fkAsset and ctx2.nState <> 2705 and r.fkAsset = ' || piAsset || ')
     ';

     FOR riskIds IN EXECUTE queryRisks LOOP
        if(riskIds.id IS NOT NULL) THEN
		RETURN NEXT riskIds;
	END IF;
     END LOOP;

     RETURN ;
END$$;
CREATE FUNCTION get_section_parents(integer) RETURNS SETOF section_id
    LANGUAGE plpgsql
    AS $$DECLARE
   piSection ALIAS FOR $1;
   queryParentSec varchar(255);
   queryEventCat varchar(255);
   ctxSec context_id%rowtype;
   ctxSecRec context_id%rowtype;
BEGIN
     queryParentSec := ' SELECT fkParent FROM rm_section_best_practice sbp
	JOIN isms_context ctx1 ON(ctx1.pkContext = sbp.fkContext and ctx1.nState <> 2705 and sbp.fkContext = ' || piSection || ') ';

     FOR ctxSec IN EXECUTE queryParentSec LOOP
        if(ctxSec.id IS NOT NULL) THEN
		RETURN NEXT ctxSec;
                FOR ctxSecRec IN EXECUTE ' SELECT pkSection FROM get_section_parents( ' || ctxSec.id || ') ' LOOP
                    RETURN NEXT ctxSecRec;
                END LOOP;
	END IF;
     END LOOP;

     RETURN ;
END$$;
CREATE FUNCTION get_section_parents_trash(integer) RETURNS SETOF section_id
    LANGUAGE plpgsql
    AS $$DECLARE
   piSection ALIAS FOR $1;
   queryParentSec varchar(255);
   queryEventCat varchar(255);
   ctxSec context_id%rowtype;
   ctxSecRec context_id%rowtype;
BEGIN
     queryParentSec := ' SELECT fkParent FROM rm_section_best_practice sbp where sbp.fkContext = ' || piSection;

     FOR ctxSec IN EXECUTE queryParentSec LOOP
        if(ctxSec.id IS NOT NULL) THEN
		RETURN NEXT ctxSec;
                FOR ctxSecRec IN EXECUTE ' SELECT pkSection FROM get_section_parents_trash( ' || ctxSec.id || ') ' LOOP
                    RETURN NEXT ctxSecRec;
                END LOOP;
	END IF;
     END LOOP;

     RETURN ;
END$$;
CREATE FUNCTION get_section_tree(integer, integer) RETURNS SETOF tree_best_practice
    LANGUAGE plpgsql
    AS $$DECLARE
   piParent ALIAS FOR $1;
   piLevel ALIAS FOR $2;
   nAuxLevel integer;
   queryBP varchar(1024);
   queryBPAux varchar(1024);
   queryBPRecursion varchar(255);
   bestPracticeRoot tree_best_practice%rowtype;
   subBestPractice tree_best_practice%rowtype;

BEGIN
     nAuxLevel := piLevel + 1;
     queryBPAux := ' SELECT fkContext, sName, ' || nAuxLevel::text || ' as nLevel FROM rm_section_best_practice sbp
		JOIN isms_context c ON(c.pkContext = sbp.fkContext and c.nState <> 2705)	
		WHERE  fkParent '  ;
     IF piParent > 0 THEN
       queryBP := queryBPAux || ' = ' || piParent;
     ELSE 
       queryBP := queryBPAux || ' is NULL';
     END IF;
     FOR bestPracticeRoot IN EXECUTE queryBP LOOP
        RETURN NEXT bestPracticeRoot;
        queryBPRecursion := 'SELECT fkContext, sName, nLevel FROM get_section_tree(' || bestPracticeRoot.fkContext::text || ' , ' || nAuxLevel::text || ' ) ';
        FOR subBestPractice IN EXECUTE queryBPRecursion LOOP
	    RETURN NEXT subBestPractice;
        END LOOP;
     END LOOP;
     RETURN ;
END$$;
CREATE FUNCTION get_sub_areas(integer) RETURNS SETOF area_id
    LANGUAGE plpgsql
    AS $$DECLARE
   piArea ALIAS FOR $1;
   querySubArea varchar(255);
   queryEventCat varchar(255);
   ctxArea context_id%rowtype;
   ctxAreaRec context_id%rowtype;
BEGIN
     querySubArea := ' SELECT fkContext FROM rm_area a
          WHERE fkParent = ' || piArea;

     FOR ctxArea IN EXECUTE querySubArea LOOP
        if(ctxArea.id IS NOT NULL) THEN
		RETURN NEXT ctxArea;
                FOR ctxAreaRec IN EXECUTE ' SELECT pkArea FROM get_sub_areas( ' || ctxArea.id || ') ' LOOP
                    RETURN NEXT ctxAreaRec;
                END LOOP;
	END IF;
     END LOOP;

     RETURN ;
END$$;
CREATE FUNCTION get_sub_categories(integer) RETURNS SETOF category_id
    LANGUAGE plpgsql
    AS $$DECLARE
   piCategory ALIAS FOR $1;
   querySubCat varchar(255);
   queryEventCat varchar(255);
   ctxCat context_id%rowtype;
   ctxCatRec context_id%rowtype;
BEGIN
    querySubCat := ' SELECT fkContext FROM rm_category cat
        WHERE cat.fkParent = ' || piCategory;

    FOR ctxCat IN EXECUTE querySubCat LOOP
        if(ctxCat.id IS NOT NULL) THEN
            RETURN NEXT ctxCat;
            FOR ctxCatRec IN EXECUTE ' SELECT pkCategory FROM get_sub_Categories( ' || ctxCat.id || ') ' LOOP
                RETURN NEXT ctxCatRec;
            END LOOP;
        END IF;
    END LOOP;
    RETURN ;
END$$;
CREATE FUNCTION get_sub_documents(iddoc integer) RETURNS SETOF document_id
    LANGUAGE plpgsql
    AS $$
  DECLARE
    doc document_id;
    subDoc document_id;
  BEGIN
    FOR doc IN SELECT fkContext FROM pm_document WHERE fkParent = idDoc LOOP
      RETURN NEXT doc;
      FOR subDoc IN SELECT * FROM get_sub_documents(doc.document_id) LOOP
        RETURN NEXT subDoc;
      END LOOP;
    END LOOP;
  END
$$;
CREATE FUNCTION get_sub_places(integer) RETURNS SETOF area_id
    LANGUAGE plpgsql
    AS $$DECLARE
   piArea ALIAS FOR $1;
   querySubArea varchar(255);
   queryEventCat varchar(255);
   ctxArea context_id%rowtype;
   ctxAreaRec context_id%rowtype;
BEGIN
     querySubArea := ' SELECT fkContext FROM cm_place a
          WHERE fkParent = ' || piArea;
     FOR ctxArea IN EXECUTE querySubArea LOOP
        if(ctxArea.id IS NOT NULL) THEN
RETURN NEXT ctxArea;
                FOR ctxAreaRec IN EXECUTE ' SELECT pkArea FROM get_sub_places( ' || ctxArea.id || ') ' LOOP
                    RETURN NEXT ctxAreaRec;
                END LOOP;
END IF;
     END LOOP;
     RETURN ;
END$$;
CREATE FUNCTION get_sub_sections(integer) RETURNS SETOF section_id
    LANGUAGE plpgsql
    AS $$DECLARE
   piSection ALIAS FOR $1;
   querySubSec varchar(255);
   queryEventSec varchar(255);
   ctxSec context_id%rowtype;
   ctxSecRec context_id%rowtype;
BEGIN
     querySubSec := ' SELECT fkContext FROM rm_section_best_practice sec
          WHERE sec.fkParent = ' || piSection;

     FOR ctxSec IN EXECUTE querySubSec LOOP
        IF(ctxSec.id IS NOT NULL) THEN
            RETURN NEXT ctxSec;
            FOR ctxSecRec IN EXECUTE ' SELECT pkSection FROM get_sub_sections( ' || ctxSec.id || ') ' LOOP
                RETURN NEXT ctxSecRec;
            END LOOP;
        END IF;
     END LOOP;

  RETURN ;
END$$;
CREATE FUNCTION get_superareas_by_user(integer) RETURNS SETOF area_ids
    LANGUAGE plpgsql
    AS $$DECLARE 
    userId ALIAS FOR $1;
    query_area varchar(255);
    area area_ids%rowtype;
    area2 area_ids%rowtype;
BEGIN
   query_area := ' SELECT fkContext FROM rm_area a
		    JOIN isms_context c ON(c.pkContext = a.fkContext and c.nState <> 2705)
		      WHERE a.fkResponsible = ' || userId;
   
    FOR area IN EXECUTE query_area LOOP
	RETURN NEXT area;
        FOR area2 IN EXECUTE ' SELECT pkArea FROM get_area_parents(' || area.area_id || ') ' LOOP
	    RETURN NEXT area2;
        END LOOP;

   END LOOP;

   RETURN ;	
END$$;
CREATE FUNCTION get_supercategories_events(picategoryid integer) RETURNS SETOF event_id
    LANGUAGE plpgsql
    AS $$DECLARE
  mrEvent record;
  miCategoryId integer;
BEGIN
  miCategoryId = piCategoryId;
  WHILE miCategoryId IS NOT NULL LOOP
    FOR mrEvent IN SELECT fkContext FROM view_rm_event_active WHERE fkCategory = miCategoryId LOOP
      RETURN NEXT mrEvent;
    END LOOP;
    SELECT fkParent INTO miCategoryId FROM view_rm_category_active WHERE fkContext = miCategoryId;
  END LOOP;
  RETURN;
END$$;
CREATE FUNCTION get_top10_area_risks() RETURNS SETOF area_risk_id
    LANGUAGE plpgsql
    AS $$DECLARE
   queryValidArea varchar(255);
   queryTop10 varchar(1024);
   validArea context_id%rowtype;
   top10AreaRisk area_risk_id%rowtype;
BEGIN
     queryValidArea := 'SELECT  ar.fkContext FROM isms_context cont_ar
	                         JOIN rm_area ar ON (cont_ar.pkContext = ar.fkContext AND cont_ar.nState <> 2705)';    

     FOR validArea IN EXECUTE queryValidArea LOOP
          queryTop10 := ' SELECT ar.fkContext, r.fkContext FROM isms_context cont_ar
		      JOIN rm_area ar ON (cont_ar.pkContext = ar.fkContext AND cont_ar.nState <> 2705)   
		      JOIN rm_process p ON (ar.fkContext = p.fkArea)
		      JOIN isms_context cont_p ON (cont_p.pkContext = p.fkContext AND cont_p.nState <> 2705)
		      JOIN rm_process_asset pa ON (p.fkContext = pa.fkProcess)
		      JOIN rm_asset a ON (pa.fkAsset = a.fkContext)
		      JOIN isms_context cont_a ON (cont_a.pkContext = a.fkContext AND cont_a.nState <> 2705)
		      JOIN rm_risk r ON (a.fkContext = r.fkAsset)
		      JOIN isms_context cont_r ON (r.fkContext = cont_r.pkContext AND cont_r.nState <> 2705)
		      WHERE ar.fkContext = ' || validArea.id || '
		      GROUP BY r.fkContext, r.nValueResidual, ar.fkContext
		      ORDER BY r.nValueResidual DESC 
		   LIMIT 10 ';
	
           FOR top10AreaRisk IN EXECUTE queryTop10 LOOP
                RETURN NEXT top10AreaRisk;
           END LOOP;
     END LOOP;

     RETURN;
END$$;
CREATE FUNCTION get_top10_process_risks() RETURNS SETOF process_risk_id
    LANGUAGE plpgsql
    AS $$DECLARE
   queryValidProcess varchar(255);
   queryTop10 varchar(1024);
   validProcess context_id%rowtype;
   top10ProcessRisk process_risk_id%rowtype;
BEGIN
     queryValidProcess := ' SELECT  p.fkContext FROM isms_context cont_p
                          JOIN rm_process p ON (cont_p.pkContext = p.fkContext AND cont_p.nState <> 2705) ';    

     FOR validProcess IN EXECUTE queryValidProcess LOOP
          queryTop10 := ' SELECT  p.fkContext, r.fkContext
		           FROM isms_context cont_p
		           JOIN rm_process p ON (cont_p.pkContext = p.fkContext AND cont_p.nState <> 2705)
		           JOIN rm_process_asset pa ON (p.fkContext = pa.fkProcess)
		           JOIN rm_asset a ON (pa.fkAsset = a.fkContext)
		           JOIN isms_context cont_a ON (cont_a.pkContext = a.fkContext AND cont_a.nState <> 2705)
		           JOIN rm_risk r ON (a.fkContext = r.fkAsset)
		           JOIN isms_context cont_r ON (r.fkContext = cont_r.pkContext AND cont_r.nState <> 2705)
		           WHERE p.fkContext = ' || validProcess.id || '		
		           ORDER BY r.nValueResidual DESC
			   LIMIT 10 ';
	
           FOR top10ProcessRisk IN EXECUTE queryTop10 LOOP
                RETURN NEXT top10ProcessRisk;
           END LOOP;
     END LOOP;

     RETURN ;
END$$;
CREATE FUNCTION get_topn_revised_documents(pitruncatenumber integer) RETURNS SETOF context_rev_count
    LANGUAGE plpgsql
    AS $$
  DECLARE
    miDocument INTEGER;
    msName VARCHAR(256);
    miCurrentVersion INT;
    miDocCount INTEGER;
    miCounter INTEGER;
    rec context_rev_count%ROWTYPE;
  BEGIN
    miCounter = 0;
    FOR rec IN
      SELECT
        d.fkContext,
        d.sName,
        d.fkCurrentVersion,
        count(di.fkContext) as rev_count
      FROM
        view_pm_document_active d
        JOIN view_pm_doc_instance_active di ON (di.fkDocument = d.fkContext 
                                                AND di.nMajorVersion>1 
                                                AND di.dBeginProduction IS NOT NULL)
      GROUP BY d.fkContext, d.sName, d.fkCurrentVersion
      ORDER BY rev_count DESC, d.sName
    LOOP
      EXIT WHEN piTruncateNumber > 0 AND miCounter = piTruncateNumber;
      RETURN NEXT rec;
      miCounter = miCounter + 1;
    END LOOP;
    RETURN;
  END
$$;
CREATE FUNCTION plpgsql_call_handler() RETURNS language_handler
    LANGUAGE c
    AS '$libdir/plpgsql', 'plpgsql_call_handler';
CREATE FUNCTION recalculation_of_area_by_area() RETURNS trigger
    LANGUAGE plpgsql
    AS $$DECLARE
    miVchg double precision;
BEGIN

IF (TG_OP = 'UPDATE') THEN
    miVchg := (NEW.nValue - OLD.nValue);
    IF (miVchg <> 0) THEN
        UPDATE rm_area
        SET nValue = get_area_value(fkContext)
        WHERE fkContext = OLD.fkParent;
    END IF;
END IF;
RETURN NEW;
END$$;
CREATE FUNCTION recalculation_of_area_by_process_delete() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN

IF (TG_OP = 'DELETE') THEN
    UPDATE rm_area
    SET nValue = get_area_value(fkContext)
    WHERE fkContext = OLD.fkArea;
END IF;
RETURN NEW;
END$$;
CREATE FUNCTION recalculation_of_area_by_process_update() RETURNS trigger
    LANGUAGE plpgsql
    AS $$DECLARE
    miArea integer;
BEGIN
   miArea := NEW.fkArea;
IF (TG_OP = 'UPDATE')  THEN
  UPDATE rm_area SET nValue = get_area_value(fkContext) WHERE fkContext = miArea;
  
  IF (NEW.fkArea <> OLD.fkArea) THEN
      UPDATE rm_area SET nValue = get_area_value(fkContext) WHERE fkContext = OLD.fkArea;
  END IF;
END IF;
RETURN NEW;
END$$;
CREATE FUNCTION recalculation_of_asset_association() RETURNS trigger
    LANGUAGE plpgsql
    AS $$DECLARE
    miAsset integer;
BEGIN

IF (TG_OP = 'DELETE') THEN
    miAsset := OLD.fkDependent;
ELSE
   IF (NEW.fkDependent IS NOT NULL) THEN
       miAsset := NEW.fkDependent;
   ELSE
       miAsset := OLD.fkDependent;
   END IF;
END IF;

UPDATE rm_asset
  SET nValue = get_asset_value(fkContext)
     WHERE fkContext = miAsset;

RETURN NEW;
END$$;
CREATE FUNCTION recalculation_of_asset_for_risk_delete() RETURNS trigger
    LANGUAGE plpgsql
    AS $$DECLARE
BEGIN
IF (TG_OP = 'DELETE') THEN
    UPDATE rm_asset
    SET nValue = get_asset_value(fkContext)
    WHERE fkContext = OLD.fkAsset;
END IF;
RETURN NEW;
END$$;
CREATE FUNCTION recalculation_of_dependent_asset() RETURNS trigger
    LANGUAGE plpgsql
    AS $$DECLARE
    miVchg double precision;
BEGIN

IF (TG_OP = 'UPDATE') THEN
    miVchg := (NEW.nValue - OLD.nValue);
    IF (miVchg <> 0) THEN
      UPDATE rm_asset
        SET nValue = get_asset_value(fkContext)
        WHERE
            fkContext IN 
                (select  fkDependent from rm_asset_asset where fkAsset = OLD.fkContext);

    END IF;
END IF;
RETURN NEW;
END$$;
CREATE FUNCTION recalculation_of_process() RETURNS trigger
    LANGUAGE plpgsql
    AS $$DECLARE
  miVchg double precision;
BEGIN
IF (TG_OP = 'UPDATE') THEN
    miVchg := (NEW.nValue - OLD.nValue);
    IF (miVchg <> 0) THEN
        UPDATE rm_process
        SET nValue = get_process_value(rm_process.fkContext)
        FROM rm_process_asset pa
        WHERE rm_process.fkContext = pa.fkProcess
        AND pa.fkAsset = OLD.fkContext;
    END IF;
END IF;
RETURN NEW;
END;$$;
CREATE FUNCTION recalculation_of_process_association() RETURNS trigger
    LANGUAGE plpgsql
    AS $$DECLARE
 miProcess integer;
BEGIN

IF( TG_OP = 'DELETE') THEN
  miProcess= OLD.fkProcess;
ELSE
  miProcess= NEW.fkProcess;
END IF;

  UPDATE rm_process
    SET nValue = get_process_value(fkContext)
    WHERE fkContext = miProcess;
RETURN NEW;
END;$$;
CREATE FUNCTION recalculation_of_risk_for_insert() RETURNS trigger
    LANGUAGE plpgsql
    AS $$DECLARE
  mbAcceptRisk boolean;
BEGIN

IF (TG_OP = 'INSERT') THEN

    UPDATE rm_risk 
      SET nValue = get_risk_value(fkContext),
      nValueResidual = get_risk_value_residual(fkContext)
      WHERE fkContext = NEW.fkContext;
    UPDATE rm_asset
	  SET nValue = get_asset_value(fkContext)
	  WHERE fkContext = NEW.fkAsset;
END IF;
RETURN NEW;
END;$$;
CREATE FUNCTION recalculation_of_risk_for_update() RETURNS trigger
    LANGUAGE plpgsql
    AS $$DECLARE
    mbAcceptRisk boolean;
    mbAssetChanged boolean;
    mfValueResidual double precision;
    mbCondition boolean;
BEGIN
IF (TG_OP = 'UPDATE') THEN
    IF(NEW.nAcceptMode <> OLD.nAcceptMode) THEN
        mbAcceptRisk := true;
    ELSE
        mbAcceptRisk := false;
    END IF;
    IF(NEW.fkAsset <> OLD.fkAsset) THEN
        mbAssetChanged := true;
    ELSE
        mbAssetChanged := false;
    END IF;
    mfValueResidual := NEW.nValueResidual - OLD.nValueResidual;
    IF(  ( (mfValueResidual <> 0) AND 
           ( (NEW.nValueResidual IS NOT NULL) OR (OLD.nValueResidual IS NOT NULL) )  
         ) 
	 OR 
	 (mbAcceptRisk)
	 OR
	 (mbAssetChanged)
      ) 
    THEN
	mbCondition := true;
    ELSE
	mbCondition := false;
    END IF;
   IF (mbCondition) THEN
       UPDATE rm_risk 
       SET nValue = get_risk_value(fkContext),
       nValueResidual = get_risk_value_residual(fkContext)
       WHERE fkContext = NEW.fkContext;
       
       UPDATE rm_asset
          SET nValue = get_asset_value(fkContext)
          WHERE fkContext = NEW.fkAsset;

       IF (mbAssetChanged) THEN
          UPDATE rm_asset
          SET nValue = get_asset_value(fkContext)
          WHERE fkContext = OLD.fkAsset;
       END IF;
    END IF;
END IF;
RETURN NEW;
END$$;
CREATE FUNCTION recalculation_of_risks_values() RETURNS trigger
    LANGUAGE plpgsql
    AS $$DECLARE
      mbAcceptRisk boolean;
      mfValueResidual double precision;
      mbCondition boolean;
  BEGIN
    IF TG_OP = 'UPDATE' AND NEW.bIsActive != OLD.bIsActive THEN
      UPDATE rm_risk SET
        nValue = get_risk_value(NEW.fkContext),
        nValueResidual = get_risk_value_residual(NEW.fkContext)
      WHERE fkContext IN (
        SELECT fkRisk
        FROM rm_risk_control rc
        WHERE fkControl = NEW.fkContext
      );
    END IF;
    RETURN NEW;
  END$$;
CREATE FUNCTION recauculate_risk_for_risk_control_association() RETURNS trigger
    LANGUAGE plpgsql
    AS $$DECLARE
BEGIN
    IF (TG_OP = 'DELETE') THEN
	UPDATE rm_risk
	  SET nvalue = get_risk_value(OLD.fkRisk),
          nValueResidual = get_risk_value_residual(OLD.fkRisk)
             WHERE fkContext = OLD.fkRisk;
    END IF;
RETURN NEW;
END$$;
CREATE FUNCTION update_alert_sent() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN
  IF OLD.ddateaccomplished IS NULL AND NEW.ddateaccomplished IS NOT NULL THEN
    PERFORM *
    FROM
      wkf_schedule s
      JOIN wkf_task_schedule ts ON (ts.fkschedule = s.pkschedule)
      JOIN wkf_task t ON (t.nactivity = ts.nactivity AND t.fkcontext = ts.fkcontext AND t.bvisible = 1)
      JOIN (
        SELECT
          s.pkschedule AS schedule_id,
          COUNT(*) AS pendant_tasks
        FROM
          wkf_schedule s
          JOIN wkf_task_schedule ts ON (ts.fkschedule = s.pkschedule)
          JOIN wkf_task t ON (t.nactivity = ts.nactivity AND t.fkcontext = ts.fkcontext AND t.bvisible = 1)
        WHERE
          ts.fkcontext = NEW.fkcontext
          AND ts.nactivity = NEW.nactivity
        GROUP BY s.pkschedule
      ) p ON (p.schedule_id = s.pkschedule)
    WHERE
      ts.fkcontext = NEW.fkcontext
      AND ts.nactivity = NEW.nactivity
      AND p.pendant_tasks > 1
      OR (
        p.pendant_tasks = 1
        AND CURRENT_DATE > s.dDateLimit
      );
      
    IF NOT FOUND THEN
      UPDATE wkf_task_schedule
      SET bAlertSent = 0
      WHERE
        nactivity = NEW.nactivity
        AND fkcontext = NEW.fkcontext;
    END IF;
  END IF;
  RETURN NEW;
END$$;
CREATE FUNCTION update_control_is_active() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  mbHasRevision INTEGER;
  mbHasTest INTEGER;
  mdDateLimit TIMESTAMP;
  mdDateTodo TIMESTAMP;
  mdDateNext TIMESTAMP;
  mbImplementationIsLate INTEGER;
  mbEfficiencyNotOk INTEGER;
  mbRevisionIsLate INTEGER;
  mbTestNotOk INTEGER;
  mbTestIsLate INTEGER;
BEGIN
  
  mbImplementationIsLate = 0;
  mbEfficiencyNotOk = 0;
  mbRevisionIsLate = 0;
  mbTestNotOk = 0;
  mbTestIsLate = 0;
  
  IF NEW.dDateImplemented IS NULL AND NEW.dDateDeadline <= NOW() THEN
    mbImplementationIsLate = 1;
  END IF;

  SELECT COUNT(*)
  INTO mbHasRevision
  FROM
    isms_config cfg,
    wkf_control_efficiency ce
  WHERE
    cfg.pkConfig = 404
    AND cfg.sValue = '1'
    AND ce.fkControlEfficiency = NEW.fkContext;

  IF mbHasRevision = 1 THEN
    SELECT CASE WHEN COUNT(*) = 0 THEN 1 ELSE 0 END
    INTO mbEfficiencyNotOk
    FROM wkf_control_efficiency ce
    WHERE
      ce.fkControlEfficiency = NEW.fkContext
      AND (
        ce.nRealEfficiency >= ce.nExpectedEfficiency
        OR ce.nRealEfficiency = 0
      );
    
    SELECT COUNT(*)
    INTO mbRevisionIsLate
    FROM
      wkf_control_efficiency ce
      JOIN wkf_schedule s ON (s.pkschedule = ce.fkschedule)
      JOIN wkf_task_schedule ts ON (ts.fkschedule = s.pkschedule)
      JOIN wkf_task t ON (t.nactivity = ts.nactivity AND t.fkcontext = ts.fkcontext AND t.bvisible = 1)
      JOIN (
        SELECT
          s2.pkschedule AS schedule_id,
          COUNT(*) AS pendant_tasks
        FROM
          wkf_schedule s2
          JOIN wkf_task_schedule ts2 ON (ts2.fkschedule = s2.pkschedule)
          JOIN wkf_task t2 ON (t2.nactivity = ts2.nactivity AND t2.fkcontext = ts2.fkcontext AND t2.bvisible = 1)
        GROUP BY s2.pkschedule
      ) p ON (p.schedule_id = s.pkschedule)
    WHERE
      ce.fkControlEfficiency = NEW.fkContext
      AND (
        p.pendant_tasks > 1
        OR (
          p.pendant_tasks = 1
          AND CURRENT_DATE > s.dDateLimit
        )
      );
    IF mbRevisionIsLate > 1 THEN mbRevisionIsLate = 1; END IF;
    
  END IF;

  SELECT COUNT(*)
  INTO mbHasTest
  FROM
    isms_config cfg,
    wkf_control_test ct
  WHERE
    cfg.pkConfig = 405
    AND cfg.sValue = '1'
    AND ct.fkControlTest = NEW.fkContext;

  IF mbHasTest = 1 THEN
    SELECT CASE WHEN COUNT(*) = 1 THEN 1 ELSE 0 END
    INTO mbTestNotOk
    FROM
      wkf_control_test ct
      LEFT JOIN rm_control_test_history th ON (
        th.fkControl = ct.fkControlTest
        AND NOT EXISTS (
          SELECT *
          FROM rm_control_test_history th2
          WHERE
            th2.fkControl = th.fkControl
            AND th2.dDateAccomplishment > th.dDateAccomplishment
        )
      )
    WHERE
      ct.fkControlTest = NEW.fkContext
      AND th.bTestedValue = 0;
    
    SELECT COUNT(*)
    INTO mbTestIsLate
    FROM
      wkf_control_test ct
      JOIN wkf_schedule s ON (s.pkschedule = ct.fkschedule)
      JOIN wkf_task_schedule ts ON (ts.fkschedule = s.pkschedule)
      JOIN wkf_task t ON (t.nactivity = ts.nactivity AND t.fkcontext = ts.fkcontext AND t.bvisible = 1)
      JOIN (
        SELECT
          s2.pkschedule AS schedule_id,
          COUNT(*) AS pendant_tasks
        FROM
          wkf_schedule s2
          JOIN wkf_task_schedule ts2 ON (ts2.fkschedule = s2.pkschedule)
          JOIN wkf_task t2 ON (t2.nactivity = ts2.nactivity AND t2.fkcontext = ts2.fkcontext AND t2.bvisible = 1)
        GROUP BY s2.pkschedule
      ) p ON (p.schedule_id = s.pkschedule)
    WHERE
      ct.fkControlTest = NEW.fkContext
      AND (
        p.pendant_tasks > 1
        OR (
          p.pendant_tasks = 1
          AND CURRENT_DATE > s.dDateLimit
        )
      );
    IF mbTestIsLate > 1 THEN mbTestIsLate = 1; END IF;
    
  END IF;

  NEW.bImplementationIsLate = mbImplementationIsLate;
  NEW.bEfficiencyNotOk = mbEfficiencyNotOk;
  NEW.bRevisionIsLate = mbRevisionIsLate;
  NEW.bTestNotOk = mbTestNotOk;
  NEW.bTestIsLate = mbTestIsLate;

  IF mbImplementationIsLate + mbEfficiencyNotOk + mbRevisionIsLate + mbTestNotOk + mbTestIsLate > 0 THEN
    NEW.bIsActive = 0;
    
    IF OLD.bImplementationIsLate = 0 AND NEW.bImplementationIsLate = 1 THEN
      IF NOT EXISTS (SELECT * FROM ci_nc_seed WHERE fkControl = NEW.fkContext AND nDeactivationReason = 1) THEN
        INSERT INTO ci_nc_seed (fkControl,nDeactivationReason,dDateSent) VALUES (NEW.fkContext,1,NOW());
      END IF;
    END IF;
    
    IF OLD.bEfficiencyNotOk = 0 AND NEW.bEfficiencyNotOk = 1 THEN
      IF NOT EXISTS (SELECT * FROM ci_nc_seed WHERE fkControl = NEW.fkContext AND nDeactivationReason = 2) THEN
        INSERT INTO ci_nc_seed (fkControl,nDeactivationReason,dDateSent) VALUES (NEW.fkContext,2,NOW());
      END IF;
    END IF;
    
    IF OLD.bRevisionIsLate = 0 AND NEW.bRevisionIsLate = 1 THEN
      IF NOT EXISTS (SELECT * FROM ci_nc_seed WHERE fkControl = NEW.fkContext AND nDeactivationReason = 4) THEN
        INSERT INTO ci_nc_seed (fkControl,nDeactivationReason,dDateSent) VALUES (NEW.fkContext,4,NOW());
      END IF;
    END IF;
    
    IF OLD.bTestNotOk = 0 AND NEW.bTestNotOk = 1 THEN
      IF NOT EXISTS (SELECT * FROM ci_nc_seed WHERE fkControl = NEW.fkContext AND nDeactivationReason = 8) THEN
        INSERT INTO ci_nc_seed (fkControl,nDeactivationReason,dDateSent) VALUES (NEW.fkContext,8,NOW());
      END IF;
    END IF;
    
    IF OLD.bTestIsLate = 0 AND NEW.bTestIsLate = 1 THEN
      IF NOT EXISTS (SELECT * FROM ci_nc_seed WHERE fkControl = NEW.fkContext AND nDeactivationReason = 16) THEN
        INSERT INTO ci_nc_seed (fkControl,nDeactivationReason,dDateSent) VALUES (NEW.fkContext,16,NOW());
      END IF;
    END IF;
    
  ELSE
    NEW.bIsActive = 1;
  END IF;

  RETURN NEW;
END
$$;
SET default_tablespace = '';
SET default_with_oids = true;

