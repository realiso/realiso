--
-- PostgreSQL database dump
--

-- Started on 2008-04-11 17:13:35

SET client_encoding = 'UTF8';

SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

--
-- TOC entry 2764 (class 0 OID 0)
-- Dependencies: 1462
-- Name: ci_nc_nseqnumber_seq; Type: SEQUENCE SET; Schema: public; Owner: isms
--

SELECT pg_catalog.setval('ci_nc_nseqnumber_seq', 4, true);


--
-- TOC entry 2765 (class 0 OID 0)
-- Dependencies: 1475
-- Name: isms_context_classification_pkclassification_seq; Type: SEQUENCE SET; Schema: public; Owner: isms
--

SELECT pg_catalog.setval('isms_context_classification_pkclassification_seq', 100, false);


--
-- TOC entry 2766 (class 0 OID 0)
-- Dependencies: 1478
-- Name: isms_context_history_pkid_seq; Type: SEQUENCE SET; Schema: public; Owner: isms
--

SELECT pg_catalog.setval('isms_context_history_pkid_seq', 1, false);


--
-- TOC entry 2767 (class 0 OID 0)
-- Dependencies: 1473
-- Name: isms_context_pkcontext_seq; Type: SEQUENCE SET; Schema: public; Owner: isms
--

SELECT pg_catalog.setval('isms_context_pkcontext_seq', 6209, true);


--
-- TOC entry 2768 (class 0 OID 0)
-- Dependencies: 1483
-- Name: isms_saas_pkconfig_seq; Type: SEQUENCE SET; Schema: public; Owner: isms
--

SELECT pg_catalog.setval('isms_saas_pkconfig_seq', 1, false);


--
-- TOC entry 2769 (class 0 OID 0)
-- Dependencies: 1497
-- Name: pm_doc_reference_pkreference_seq; Type: SEQUENCE SET; Schema: public; Owner: isms
--

SELECT pg_catalog.setval('pm_doc_reference_pkreference_seq', 1, false);


--
-- TOC entry 2770 (class 0 OID 0)
-- Dependencies: 1500
-- Name: pm_instance_comment_pkcomment_seq; Type: SEQUENCE SET; Schema: public; Owner: isms
--

SELECT pg_catalog.setval('pm_instance_comment_pkcomment_seq', 1, false);


--
-- TOC entry 2771 (class 0 OID 0)
-- Dependencies: 1523
-- Name: rm_parameter_name_pkparametername_seq; Type: SEQUENCE SET; Schema: public; Owner: isms
--

SELECT pg_catalog.setval('rm_parameter_name_pkparametername_seq', 10, false);


--
-- TOC entry 2772 (class 0 OID 0)
-- Dependencies: 1525
-- Name: rm_parameter_value_name_pkvaluename_seq; Type: SEQUENCE SET; Schema: public; Owner: isms
--

SELECT pg_catalog.setval('rm_parameter_value_name_pkvaluename_seq', 10, false);


--
-- TOC entry 2773 (class 0 OID 0)
-- Dependencies: 1529
-- Name: rm_rc_parameter_value_name_pkrcvaluename_seq; Type: SEQUENCE SET; Schema: public; Owner: isms
--

SELECT pg_catalog.setval('rm_rc_parameter_value_name_pkrcvaluename_seq', 10, false);


--
-- TOC entry 2774 (class 0 OID 0)
-- Dependencies: 1538
-- Name: wkf_alert_pkalert_seq; Type: SEQUENCE SET; Schema: public; Owner: isms
--

SELECT pg_catalog.setval('wkf_alert_pkalert_seq', 150, true);


--
-- TOC entry 2775 (class 0 OID 0)
-- Dependencies: 1542
-- Name: wkf_schedule_pkschedule_seq; Type: SEQUENCE SET; Schema: public; Owner: isms
--

SELECT pg_catalog.setval('wkf_schedule_pkschedule_seq', 1, false);


--
-- TOC entry 2776 (class 0 OID 0)
-- Dependencies: 1544
-- Name: wkf_task_pktask_seq; Type: SEQUENCE SET; Schema: public; Owner: isms
--

SELECT pg_catalog.setval('wkf_task_pktask_seq', 189, true);


--
-- TOC entry 2681 (class 0 OID 4198994)
-- Dependencies: 1453
-- Data for Name: ci_action_plan; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO ci_action_plan (fkcontext, fkresponsible, sname, tactionplan, nactiontype, ddatedeadline, ddateconclusion, bisefficient, ddateefficiencyrevision, ddateefficiencymeasured, ndaysbefore, bflagrevisionalert, bflagrevisionalertlate, sdocument) VALUES (6191, 6002, 'Monthly Access Rights Review', 'Change the periodicity of the access rigths review from each 3 months to every month.', 4780, '2008-03-18 00:00:00', NULL, 0, NULL, NULL, 0, 0, 0, '');
INSERT INTO ci_action_plan (fkcontext, fkresponsible, sname, tactionplan, nactiontype, ddatedeadline, ddateconclusion, bisefficient, ddateefficiencyrevision, ddateefficiencymeasured, ndaysbefore, bflagrevisionalert, bflagrevisionalertlate, sdocument) VALUES (6192, 13, 'NDA procedure update', 'NDA procedure update in order to clarify software suppliers cases.', 4780, '2008-04-01 00:00:00', NULL, 0, NULL, NULL, 0, 0, 0, '');
INSERT INTO ci_action_plan (fkcontext, fkresponsible, sname, tactionplan, nactiontype, ddatedeadline, ddateconclusion, bisefficient, ddateefficiencyrevision, ddateefficiencymeasured, ndaysbefore, bflagrevisionalert, bflagrevisionalertlate, sdocument) VALUES (6194, 6004, 'Capacity Planning procedure update', 'Update capacity planning procedure to avoid new traffic problems', 4780, '2008-04-22 00:00:00', NULL, 0, NULL, NULL, 0, 0, 0, '');
INSERT INTO ci_action_plan (fkcontext, fkresponsible, sname, tactionplan, nactiontype, ddatedeadline, ddateconclusion, bisefficient, ddateefficiencyrevision, ddateefficiencymeasured, ndaysbefore, bflagrevisionalert, bflagrevisionalertlate, sdocument) VALUES (6195, 13, 'Information Security Training', 'Information Security Training about computer virus.', 4781, '2008-04-14 00:00:00', '2008-04-09 00:00:00', 0, NULL, NULL, 0, 0, 0, '');
INSERT INTO ci_action_plan (fkcontext, fkresponsible, sname, tactionplan, nactiontype, ddatedeadline, ddateconclusion, bisefficient, ddateefficiencyrevision, ddateefficiencymeasured, ndaysbefore, bflagrevisionalert, bflagrevisionalertlate, sdocument) VALUES (6193, 6005, 'New Employees Training Improvement', 'Improve security controls training for new employees.', 4780, '2008-03-06 00:00:00', NULL, 0, NULL, NULL, 0, 0, 0, '');


--
-- TOC entry 2682 (class 0 OID 4199011)
-- Dependencies: 1454
-- Data for Name: ci_category; Type: TABLE DATA; Schema: public; Owner: isms
--



--
-- TOC entry 2683 (class 0 OID 4199016)
-- Dependencies: 1455
-- Data for Name: ci_incident; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO ci_incident (fkcontext, fkcategory, fkresponsible, sname, taccountsplan, tevidences, nlosstype, tevidencerequirementcomment, ddatelimit, ddatefinish, tdisposaldescription, tsolutiondescription, tproductservice, bnotemaildp, bemaildpsent, ddate) VALUES (6181, NULL, 6004, 'Router access capacity limit exceeded', '', '', 4580, NULL, '2008-04-11 00:00:00', '2008-04-11 00:00:00', 'Router change. Contingency Plan Activate', 'Monitoring services enable', '', 0, 0, '2008-02-19 15:00:00');
INSERT INTO ci_incident (fkcontext, fkcategory, fkresponsible, sname, taccountsplan, tevidences, nlosstype, tevidencerequirementcomment, ddatelimit, ddatefinish, tdisposaldescription, tsolutiondescription, tproductservice, bnotemaildp, bemaildpsent, ddate) VALUES (6182, NULL, 6004, 'Computer virus contamination', '', '', 4580, NULL, '2008-04-11 00:00:00', '2008-04-11 00:00:00', 'network virus removal', 'computer virus software change', '', 0, 0, '2008-03-18 14:00:00');
INSERT INTO ci_incident (fkcontext, fkcategory, fkresponsible, sname, taccountsplan, tevidences, nlosstype, tevidencerequirementcomment, ddatelimit, ddatefinish, tdisposaldescription, tsolutiondescription, tproductservice, bnotemaildp, bemaildpsent, ddate) VALUES (6183, NULL, 6006, 'Internal information theft', '', '', 4580, '', '2008-04-09 00:00:00', NULL, 'incident investigation.', '', '', 0, 0, '2008-03-11 14:00:00');
INSERT INTO ci_incident (fkcontext, fkcategory, fkresponsible, sname, taccountsplan, tevidences, nlosstype, tevidencerequirementcomment, ddatelimit, ddatefinish, tdisposaldescription, tsolutiondescription, tproductservice, bnotemaildp, bemaildpsent, ddate) VALUES (6184, NULL, 6004, 'Power supply down', '', '', 4580, NULL, '2008-04-09 00:00:00', NULL, 'power circuit changed', '', '', 0, 0, '2008-03-17 14:00:00');
INSERT INTO ci_incident (fkcontext, fkcategory, fkresponsible, sname, taccountsplan, tevidences, nlosstype, tevidencerequirementcomment, ddatelimit, ddatefinish, tdisposaldescription, tsolutiondescription, tproductservice, bnotemaildp, bemaildpsent, ddate) VALUES (6185, NULL, 6005, 'Inappropriate system rights concession', '', '', 4580, NULL, '2008-04-09 00:00:00', NULL, 'Systema rights revision', '', '', 0, 0, '2008-03-03 13:00:00');
INSERT INTO ci_incident (fkcontext, fkcategory, fkresponsible, sname, taccountsplan, tevidences, nlosstype, tevidencerequirementcomment, ddatelimit, ddatefinish, tdisposaldescription, tsolutiondescription, tproductservice, bnotemaildp, bemaildpsent, ddate) VALUES (6186, NULL, 6003, 'Unauthorized information disclousure', '', '', 4580, '', '2008-04-10 00:00:00', NULL, 'incident investigation', '', '', 0, 0, '2008-02-04 15:15:00');


--
-- TOC entry 2684 (class 0 OID 4199031)
-- Dependencies: 1456
-- Data for Name: ci_incident_control; Type: TABLE DATA; Schema: public; Owner: isms
--



--
-- TOC entry 2685 (class 0 OID 4199039)
-- Dependencies: 1457
-- Data for Name: ci_incident_financial_impact; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO ci_incident_financial_impact (fkincident, fkclassification, nvalue) VALUES (6181, 36, 0);
INSERT INTO ci_incident_financial_impact (fkincident, fkclassification, nvalue) VALUES (6181, 37, 30000);
INSERT INTO ci_incident_financial_impact (fkincident, fkclassification, nvalue) VALUES (6181, 38, 40000);
INSERT INTO ci_incident_financial_impact (fkincident, fkclassification, nvalue) VALUES (6181, 39, 0);


--
-- TOC entry 2686 (class 0 OID 4199047)
-- Dependencies: 1458
-- Data for Name: ci_incident_process; Type: TABLE DATA; Schema: public; Owner: isms
--



--
-- TOC entry 2687 (class 0 OID 4199054)
-- Dependencies: 1459
-- Data for Name: ci_incident_risk; Type: TABLE DATA; Schema: public; Owner: isms
--



--
-- TOC entry 2688 (class 0 OID 4199061)
-- Dependencies: 1460
-- Data for Name: ci_incident_risk_value; Type: TABLE DATA; Schema: public; Owner: isms
--



--
-- TOC entry 2689 (class 0 OID 4199069)
-- Dependencies: 1461
-- Data for Name: ci_incident_user; Type: TABLE DATA; Schema: public; Owner: isms
--



--
-- TOC entry 2690 (class 0 OID 4199081)
-- Dependencies: 1463
-- Data for Name: ci_nc; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO ci_nc (fkcontext, fkcontrol, fkresponsible, fksender, sname, nseqnumber, tdescription, tcause, tdenialjustification, nclassification, ddatesent, ncapability) VALUES (6188, NULL, 13, 13, 'Lack of NDA for software suppliers', 2, '', 'Unclear NDA procedure', NULL, 62251, '2008-03-19 00:00:00', 62281);
INSERT INTO ci_nc (fkcontext, fkcontrol, fkresponsible, fksender, sname, nseqnumber, tdescription, tcause, tdenialjustification, nclassification, ddatesent, ncapability) VALUES (6190, NULL, 6004, 13, 'Server not accessible due to excessive traffic', 4, '', 'capacity planning procure was not correctly applied.', NULL, 62253, '2008-04-10 00:00:00', 62281);
INSERT INTO ci_nc (fkcontext, fkcontrol, fkresponsible, fksender, sname, nseqnumber, tdescription, tcause, tdenialjustification, nclassification, ddatesent, ncapability) VALUES (6187, NULL, 6004, 13, 'Too many inappropriate access rights concessions in ERP system.', 1, '', 'inadequate periodicity access rights review', NULL, 62252, '2008-02-04 00:00:00', 62281);
INSERT INTO ci_nc (fkcontext, fkcontrol, fkresponsible, fksender, sname, nseqnumber, tdescription, tcause, tdenialjustification, nclassification, ddatesent, ncapability) VALUES (6189, NULL, 6003, 13, 'Sensitive information theft', 3, '', 'Information was not protect in accordance with the classification guideline, because it was an unknown by the user', NULL, 62251, '2007-11-20 00:00:00', 62281);


--
-- TOC entry 2691 (class 0 OID 4199095)
-- Dependencies: 1464
-- Data for Name: ci_nc_action_plan; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO ci_nc_action_plan (fknc, fkactionplan) VALUES (6188, 6192);
INSERT INTO ci_nc_action_plan (fknc, fkactionplan) VALUES (6190, 6194);
INSERT INTO ci_nc_action_plan (fknc, fkactionplan) VALUES (6187, 6191);
INSERT INTO ci_nc_action_plan (fknc, fkactionplan) VALUES (6189, 6193);


--
-- TOC entry 2692 (class 0 OID 4199102)
-- Dependencies: 1465
-- Data for Name: ci_nc_process; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO ci_nc_process (fknc, fkprocess) VALUES (6187, 6018);
INSERT INTO ci_nc_process (fknc, fkprocess) VALUES (6187, 6022);
INSERT INTO ci_nc_process (fknc, fkprocess) VALUES (6187, 6024);
INSERT INTO ci_nc_process (fknc, fkprocess) VALUES (6187, 6028);
INSERT INTO ci_nc_process (fknc, fkprocess) VALUES (6187, 6020);
INSERT INTO ci_nc_process (fknc, fkprocess) VALUES (6187, 6026);
INSERT INTO ci_nc_process (fknc, fkprocess) VALUES (6188, 6024);
INSERT INTO ci_nc_process (fknc, fkprocess) VALUES (6189, 6026);
INSERT INTO ci_nc_process (fknc, fkprocess) VALUES (6190, 6018);
INSERT INTO ci_nc_process (fknc, fkprocess) VALUES (6190, 6022);
INSERT INTO ci_nc_process (fknc, fkprocess) VALUES (6190, 6024);
INSERT INTO ci_nc_process (fknc, fkprocess) VALUES (6190, 6028);
INSERT INTO ci_nc_process (fknc, fkprocess) VALUES (6190, 6020);
INSERT INTO ci_nc_process (fknc, fkprocess) VALUES (6190, 6026);


--
-- TOC entry 2693 (class 0 OID 4199109)
-- Dependencies: 1466
-- Data for Name: ci_nc_seed; Type: TABLE DATA; Schema: public; Owner: isms
--



--
-- TOC entry 2694 (class 0 OID 4199116)
-- Dependencies: 1467
-- Data for Name: ci_occurrence; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO ci_occurrence (fkcontext, fkincident, tdescription, tdenialjustification, ddate) VALUES (6172, NULL, 'ERP System is difficult to access', NULL, '2008-03-18 09:00:00');
INSERT INTO ci_occurrence (fkcontext, fkincident, tdescription, tdenialjustification, ddate) VALUES (6167, 6181, 'the downloads are taking too much time', NULL, '2008-03-25 10:00:00');
INSERT INTO ci_occurrence (fkcontext, fkincident, tdescription, tdenialjustification, ddate) VALUES (6171, 6181, 'network performance is low', NULL, '2008-02-19 13:35:00');
INSERT INTO ci_occurrence (fkcontext, fkincident, tdescription, tdenialjustification, ddate) VALUES (6169, 6181, 'network is down', NULL, '2008-02-19 13:50:00');
INSERT INTO ci_occurrence (fkcontext, fkincident, tdescription, tdenialjustification, ddate) VALUES (6178, 6182, 'computer virus', NULL, '2008-01-14 13:25:00');
INSERT INTO ci_occurrence (fkcontext, fkincident, tdescription, tdenialjustification, ddate) VALUES (6180, 6182, 'an email was received with a computer virus', NULL, '2007-12-20 09:10:00');
INSERT INTO ci_occurrence (fkcontext, fkincident, tdescription, tdenialjustification, ddate) VALUES (6179, 6182, 'suspicious of Computer virus', NULL, '2008-03-11 16:45:00');
INSERT INTO ci_occurrence (fkcontext, fkincident, tdescription, tdenialjustification, ddate) VALUES (6174, 6183, 'suspicious of document theft', NULL, '2008-04-03 18:00:00');
INSERT INTO ci_occurrence (fkcontext, fkincident, tdescription, tdenialjustification, ddate) VALUES (6176, 6184, 'power supply is down', NULL, '2008-04-07 10:45:00');
INSERT INTO ci_occurrence (fkcontext, fkincident, tdescription, tdenialjustification, ddate) VALUES (6173, 6185, 'unauthorized ERP operation', NULL, '2008-01-21 10:15:00');
INSERT INTO ci_occurrence (fkcontext, fkincident, tdescription, tdenialjustification, ddate) VALUES (6175, 6185, 'unauthorized network sharing access', NULL, '2008-02-05 17:15:00');
INSERT INTO ci_occurrence (fkcontext, fkincident, tdescription, tdenialjustification, ddate) VALUES (6177, 6186, 'unauthorized information disclosure', NULL, '2008-02-18 14:35:00');


--
-- TOC entry 2695 (class 0 OID 4199125)
-- Dependencies: 1468
-- Data for Name: ci_risk_probability; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6075, 1, 2);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6075, 2, 5);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6075, 3, 8);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6075, 4, 12);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6076, 1, 5);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6076, 2, 10);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6076, 3, 15);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6076, 4, 20);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6077, 1, 3);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6077, 2, 6);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6077, 3, 10);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6077, 4, 15);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6080, 1, 3);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6080, 2, 6);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6080, 3, 9);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6080, 4, 12);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6081, 1, 5);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6081, 2, 10);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6081, 3, 15);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6081, 4, 20);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6082, 1, 2);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6082, 2, 4);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6082, 3, 6);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6082, 4, 8);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6083, 1, 2);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6083, 2, 4);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6083, 3, 6);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6083, 4, 8);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6084, 1, 2);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6084, 2, 4);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6084, 3, 6);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6084, 4, 8);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6085, 1, 3);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6085, 2, 6);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6085, 3, 9);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6085, 4, 12);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6086, 1, 1);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6086, 2, 3);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6086, 3, 5);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6086, 4, 7);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6087, 1, 1);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6087, 2, 3);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6087, 3, 5);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6087, 4, 7);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6088, 1, 5);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6088, 2, 10);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6088, 3, 15);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6088, 4, 20);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6089, 1, 3);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6089, 2, 6);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6089, 3, 9);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6089, 4, 12);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6090, 1, 3);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6090, 2, 6);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6090, 3, 9);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6090, 4, 12);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6091, 1, 3);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6091, 2, 6);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6091, 3, 9);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6091, 4, 12);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6092, 1, 2);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6092, 2, 5);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6092, 3, 8);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6092, 4, 13);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6093, 1, 4);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6093, 2, 8);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6093, 3, 12);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6093, 4, 16);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6094, 1, 3);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6094, 2, 6);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6094, 3, 9);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6094, 4, 12);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6100, 1, 2);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6100, 2, 4);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6100, 3, 6);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6100, 4, 8);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6101, 1, 2);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6101, 2, 4);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6101, 3, 6);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6101, 4, 8);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6102, 1, 3);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6102, 2, 6);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6102, 3, 9);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6102, 4, 12);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6103, 1, 3);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6103, 2, 6);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6103, 3, 9);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6103, 4, 12);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6104, 1, 3);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6104, 2, 6);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6104, 3, 9);
INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (6104, 4, 12);


--
-- TOC entry 2696 (class 0 OID 4199133)
-- Dependencies: 1469
-- Data for Name: ci_risk_schedule; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO ci_risk_schedule (fkrisk, nperiod, nvalue, ddatelastcheck) VALUES (6091, 7803, 3, '2008-04-09 13:40:01');
INSERT INTO ci_risk_schedule (fkrisk, nperiod, nvalue, ddatelastcheck) VALUES (6094, 7803, 3, '2008-04-09 13:40:01');
INSERT INTO ci_risk_schedule (fkrisk, nperiod, nvalue, ddatelastcheck) VALUES (6090, 7803, 3, '2008-04-09 13:40:01');
INSERT INTO ci_risk_schedule (fkrisk, nperiod, nvalue, ddatelastcheck) VALUES (6093, 7803, 3, '2008-04-09 13:40:01');
INSERT INTO ci_risk_schedule (fkrisk, nperiod, nvalue, ddatelastcheck) VALUES (6092, 7803, 6, '2008-04-09 13:40:01');
INSERT INTO ci_risk_schedule (fkrisk, nperiod, nvalue, ddatelastcheck) VALUES (6101, 7803, 3, '2008-04-09 13:40:01');
INSERT INTO ci_risk_schedule (fkrisk, nperiod, nvalue, ddatelastcheck) VALUES (6100, 7803, 3, '2008-04-09 13:40:01');
INSERT INTO ci_risk_schedule (fkrisk, nperiod, nvalue, ddatelastcheck) VALUES (6102, 7803, 3, '2008-04-09 13:40:01');
INSERT INTO ci_risk_schedule (fkrisk, nperiod, nvalue, ddatelastcheck) VALUES (6104, 7803, 3, '2008-04-09 13:40:01');
INSERT INTO ci_risk_schedule (fkrisk, nperiod, nvalue, ddatelastcheck) VALUES (6103, 7803, 3, '2008-04-09 13:40:01');
INSERT INTO ci_risk_schedule (fkrisk, nperiod, nvalue, ddatelastcheck) VALUES (6081, 7803, 2, '2008-04-09 15:11:45');
INSERT INTO ci_risk_schedule (fkrisk, nperiod, nvalue, ddatelastcheck) VALUES (6080, 7803, 1, '2008-04-09 15:11:45');
INSERT INTO ci_risk_schedule (fkrisk, nperiod, nvalue, ddatelastcheck) VALUES (6082, 7803, 1, '2008-04-09 15:11:45');
INSERT INTO ci_risk_schedule (fkrisk, nperiod, nvalue, ddatelastcheck) VALUES (6084, 7803, 6, '2008-04-09 15:11:45');
INSERT INTO ci_risk_schedule (fkrisk, nperiod, nvalue, ddatelastcheck) VALUES (6083, 7803, 1, '2008-04-09 15:11:45');
INSERT INTO ci_risk_schedule (fkrisk, nperiod, nvalue, ddatelastcheck) VALUES (6075, 7803, 1, '2008-04-10 16:13:47');
INSERT INTO ci_risk_schedule (fkrisk, nperiod, nvalue, ddatelastcheck) VALUES (6077, 7803, 1, '2008-04-10 16:13:47');
INSERT INTO ci_risk_schedule (fkrisk, nperiod, nvalue, ddatelastcheck) VALUES (6076, 7802, 1, '2008-04-10 16:13:47');
INSERT INTO ci_risk_schedule (fkrisk, nperiod, nvalue, ddatelastcheck) VALUES (6085, 7803, 1, '2008-04-10 16:13:47');
INSERT INTO ci_risk_schedule (fkrisk, nperiod, nvalue, ddatelastcheck) VALUES (6086, 7803, 6, '2008-04-10 16:13:47');
INSERT INTO ci_risk_schedule (fkrisk, nperiod, nvalue, ddatelastcheck) VALUES (6087, 7803, 3, '2008-04-10 16:13:47');
INSERT INTO ci_risk_schedule (fkrisk, nperiod, nvalue, ddatelastcheck) VALUES (6089, 7803, 6, '2008-04-10 16:13:47');
INSERT INTO ci_risk_schedule (fkrisk, nperiod, nvalue, ddatelastcheck) VALUES (6088, 7803, 4, '2008-04-10 16:13:47');


--
-- TOC entry 2697 (class 0 OID 4199140)
-- Dependencies: 1470
-- Data for Name: ci_solution; Type: TABLE DATA; Schema: public; Owner: isms
--


--
-- TOC entry 2700 (class 0 OID 4199162)
-- Dependencies: 1474
-- Data for Name: isms_context; Type: TABLE DATA; Schema: public; Owner: isms
--

COPY isms_context (pkcontext, ntype, nstate) FROM '/tmp/trialdata/isms_context.sql' DELIMITERS '#' NULL 'NULL';



--
-- TOC entry 2701 (class 0 OID 4199172)
-- Dependencies: 1476
-- Data for Name: isms_context_classification; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (1, 2801, 'Headquarter', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (2, 2801, 'Subsidiary', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (3, 2801, 'Regional Office', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (4, 2801, 'Factory', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype, nweight) VALUES (5, 2801, '3 - High', 5502, 1);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype, nweight) VALUES (6, 2801, '2 - Medium', 5502, 2);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype, nweight) VALUES (7, 2801, '1 - Low', 5502, 3);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (8, 2802, 'Market & customers', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (9, 2802, 'New product development', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (10, 2802, 'Production & delivery', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (11, 2802, 'Human resource', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (12, 2802, 'Information technology', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (13, 2802, 'Finance & accounting', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (14, 2802, 'Environment, health & safety', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (15, 2802, 'Performance improvement', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype, nweight) VALUES (16, 2802, '3 - High', 5502, 1);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype, nweight) VALUES (17, 2802, '2 - Medium', 5502, 2);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype, nweight) VALUES (18, 2802, '1 - Low', 5502, 3);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (19, 2804, 'Vulnerability', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (20, 2804, 'Threat', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (21, 2804, 'Event (Vulnerability x Threat)', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (22, 2807, 'Physical', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (23, 2807, 'Technological', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (24, 2807, 'Administrative', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (25, 2805, 'Based on Risk Analysis', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (26, 2805, 'Compliance Issue', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (27, 2805, 'Others', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (28, 2823, 'Confidential', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (29, 2823, 'Restrict', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (30, 2823, 'Institutional', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (31, 2823, 'Public', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (32, 2826, 'Confidential', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (33, 2826, 'Restrict', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (34, 2826, 'Institutional', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (35, 2826, 'Public', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (36, 2831, 'Fine', 5503);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (37, 2831, 'Environment Recovery', 5503);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (38, 2831, 'Direct Loss', 5503);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (39, 2831, 'Indirect Loss', 5503);


--
-- TOC entry 2702 (class 0 OID 4199180)
-- Dependencies: 1477
-- Data for Name: isms_context_date; Type: TABLE DATA; Schema: public; Owner: isms
--

COPY isms_context_date (fkcontext, naction, ddate, nuserid, susername) FROM '/tmp/trialdata/isms_context_date.sql' DELIMITERS '#' NULL 'NULL';

--
-- TOC entry 2703 (class 0 OID 4199189)
-- Dependencies: 1479
-- Data for Name: isms_context_history; Type: TABLE DATA; Schema: public; Owner: isms
--



--
-- TOC entry 2704 (class 0 OID 4199197)
-- Dependencies: 1480
-- Data for Name: isms_policy; Type: TABLE DATA; Schema: public; Owner: isms
--



--
-- TOC entry 2705 (class 0 OID 4199205)
-- Dependencies: 1481
-- Data for Name: isms_profile; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO isms_profile (fkcontext, sname, nid) VALUES (1, 'Security Officer', 8801);
INSERT INTO isms_profile (fkcontext, sname, nid) VALUES (2, 'Manager', 8802);
INSERT INTO isms_profile (fkcontext, sname, nid) VALUES (3, 'Administrator', 8803);
INSERT INTO isms_profile (fkcontext, sname, nid) VALUES (4, 'Without Permissions', 8804);
INSERT INTO isms_profile (fkcontext, sname, nid) VALUES (5, 'Document Reader', 8805);


--
-- TOC entry 2706 (class 0 OID 4199211)
-- Dependencies: 1482
-- Data for Name: isms_profile_acl; Type: TABLE DATA; Schema: public; Owner: isms
--

COPY isms_profile_acl (fkprofile,stag) FROM '/tmp/trialdata/isms_profile_acl.sql' DELIMITERS '#' NULL 'NULL';

--
-- TOC entry 2708 (class 0 OID 4199225)
-- Dependencies: 1485
-- Data for Name: isms_scope; Type: TABLE DATA; Schema: public; Owner: isms
--



--
-- TOC entry 2709 (class 0 OID 4199233)
-- Dependencies: 1486
-- Data for Name: isms_solicitor; Type: TABLE DATA; Schema: public; Owner: isms
--



--
-- TOC entry 2710 (class 0 OID 4199240)
-- Dependencies: 1487
-- Data for Name: isms_user; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO isms_user (fkcontext, fkprofile, sname, slogin, spassword, semail, nip, nlanguage, dlastlogin, nwronglogonattempts, bisblocked, bmustchangepassword, srequestpassword, ssession, bansweredsurvey) VALUES (6006, 2, 'Ali S. Hamid', 'ahamid@samplesa.com', 'c21f969b5f03d33d43e04f8f136e7682', 'ahamid@samplesa.com', -1062731676, %user_language%, '2008-04-09 17:34:26', 0, 0, 0, '', '', NULL);
INSERT INTO isms_user (fkcontext, fkprofile, sname, slogin, spassword, semail, nip, nlanguage, dlastlogin, nwronglogonattempts, bisblocked, bmustchangepassword, srequestpassword, ssession, bansweredsurvey) VALUES (6001, 2, 'Edward Dale', 'edale@samplesa.com', 'c21f969b5f03d33d43e04f8f136e7682', 'edale@samplesa.com', -1062731676, %user_language%, '2008-04-09 17:42:54', 0, 0, 0, '', '', NULL);
INSERT INTO isms_user (fkcontext, fkprofile, sname, slogin, spassword, semail, nip, nlanguage, dlastlogin, nwronglogonattempts, bisblocked, bmustchangepassword, srequestpassword, ssession, bansweredsurvey) VALUES (6002, 2, 'Ernest Collins', 'ecollins@samplesa.com', 'c21f969b5f03d33d43e04f8f136e7682', 'ecollins@samplesa.com', -1062731676, %user_language%, '2008-04-09 17:41:19', 0, 0, 0, '', '', NULL);
INSERT INTO isms_user (fkcontext, fkprofile, sname, slogin, spassword, semail, nip, nlanguage, dlastlogin, nwronglogonattempts, bisblocked, bmustchangepassword, srequestpassword, ssession, bansweredsurvey) VALUES (6000, 2, 'Hans Friedman', 'hfriedman@samplesa.com', 'c21f969b5f03d33d43e04f8f136e7682', 'hfriedman@samplesa.com', -1062731676, %user_language%, '2008-04-09 16:43:55', 0, 0, 0, '', '', NULL);
INSERT INTO isms_user (fkcontext, fkprofile, sname, slogin, spassword, semail, nip, nlanguage, dlastlogin, nwronglogonattempts, bisblocked, bmustchangepassword, srequestpassword, ssession, bansweredsurvey) VALUES (6007, 2, 'Kim Robert', 'krobert@samplesa.com', 'c21f969b5f03d33d43e04f8f136e7682', 'krobert@samplesa.com', -1062731676, %user_language%, '2008-04-09 17:46:24', 0, 0, 0, '', '', NULL);
INSERT INTO isms_user (fkcontext, fkprofile, sname, slogin, spassword, semail, nip, nlanguage, dlastlogin, nwronglogonattempts, bisblocked, bmustchangepassword, srequestpassword, ssession, bansweredsurvey) VALUES (6004, 2, 'Aaron Schneider', 'aschneider@samplesa.com', 'c21f969b5f03d33d43e04f8f136e7682', 'aschneider@samplesa.com', -1062731636, %user_language%, '2008-04-10 16:30:18', 0, 0, 0, '', '', NULL);
INSERT INTO isms_user (fkcontext, fkprofile, sname, slogin, spassword, semail, nip, nlanguage, dlastlogin, nwronglogonattempts, bisblocked, bmustchangepassword, srequestpassword, ssession, bansweredsurvey) VALUES (6003, 2, 'Marvin Stegen', 'mstegen@samplesa.com', 'c21f969b5f03d33d43e04f8f136e7682', 'mstegen@samplesa.com', -1062731636, %user_language%, '2008-04-10 17:41:52', 0, 0, 0, '', '', NULL);
INSERT INTO isms_user (fkcontext, fkprofile, sname, slogin, spassword, semail, nip, nlanguage, dlastlogin, nwronglogonattempts, bisblocked, bmustchangepassword, srequestpassword, ssession, bansweredsurvey) VALUES (6005, 2, 'Matheus Kowalski', 'mkowalski@samplesa.com', 'c21f969b5f03d33d43e04f8f136e7682', 'mkowalski@samplesa.com', -1062731649, %user_language%, '2008-04-11 14:33:40', 0, 0, 0, '', '', NULL);
INSERT INTO isms_user (fkcontext, fkprofile, sname, slogin, spassword, semail, nip, nlanguage, dlastlogin, nwronglogonattempts, bisblocked, bmustchangepassword, srequestpassword, ssession, bansweredsurvey) VALUES (13, 1, '%user_fullname%', '%user_login%', '%user_passwordmd5%', '%user_email%', -1062731641, %user_language%, '2008-04-11 16:31:42', 0, 0, 0, '', '', 0);


--
-- TOC entry 2711 (class 0 OID 4199253)
-- Dependencies: 1488
-- Data for Name: isms_user_password_history; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO isms_user_password_history (fkuser, spassword, ddatepasswordchanged) VALUES (13, 'c21f969b5f03d33d43e04f8f136e7682', '2008-04-08 12:16:12');
INSERT INTO isms_user_password_history (fkuser, spassword, ddatepasswordchanged) VALUES (6000, 'c21f969b5f03d33d43e04f8f136e7682', '2008-04-08 14:50:56');
INSERT INTO isms_user_password_history (fkuser, spassword, ddatepasswordchanged) VALUES (6001, 'c21f969b5f03d33d43e04f8f136e7682', '2008-04-08 14:51:29');
INSERT INTO isms_user_password_history (fkuser, spassword, ddatepasswordchanged) VALUES (6002, 'c21f969b5f03d33d43e04f8f136e7682', '2008-04-08 14:52:07');
INSERT INTO isms_user_password_history (fkuser, spassword, ddatepasswordchanged) VALUES (6003, 'c21f969b5f03d33d43e04f8f136e7682', '2008-04-08 14:52:58');
INSERT INTO isms_user_password_history (fkuser, spassword, ddatepasswordchanged) VALUES (6004, 'c21f969b5f03d33d43e04f8f136e7682', '2008-04-08 14:59:46');
INSERT INTO isms_user_password_history (fkuser, spassword, ddatepasswordchanged) VALUES (6005, 'c21f969b5f03d33d43e04f8f136e7682', '2008-04-08 15:00:26');
INSERT INTO isms_user_password_history (fkuser, spassword, ddatepasswordchanged) VALUES (6006, 'c21f969b5f03d33d43e04f8f136e7682', '2008-04-08 15:00:57');
INSERT INTO isms_user_password_history (fkuser, spassword, ddatepasswordchanged) VALUES (6007, 'c21f969b5f03d33d43e04f8f136e7682', '2008-04-08 15:01:56');


--
-- TOC entry 2712 (class 0 OID 4199255)
-- Dependencies: 1489
-- Data for Name: isms_user_preference; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO isms_user_preference (fkuser, spreference, tvalue) VALUES (13, 'dashboard_sumary_pref', 'a:22:{i:0;s:22:"grid_financial_summary";i:1;s:17:"grid_risk_summary";i:2;s:18:"grid_risk_summary2";i:3;s:26:"grid_residual_risk_summary";i:4;s:31:"grid_isms_reports_and_documents";i:5;s:26:"grid_best_practice_summary";i:6;s:20:"grid_control_summary";i:7;s:25:"grid_abnormalitys_summary";i:8;s:15:"grid_doc_sumary";i:9;s:31:"grid_doc_context_with_documents";i:10;s:22:"grid_doc_approver_time";i:11;s:14:"grid_doc_top10";i:12;s:16:"grid_doc_by_type";i:13;s:24:"grid_doc_period_revision";i:14;s:19:"grid_abnormality_pm";i:15;s:21:"grid_incident_summary";i:16;s:15:"grid_nc_summary";i:17;s:23:"grid_incidents_per_user";i:18;s:24:"grid_incidents_per_asset";i:19;s:26:"grid_incidents_per_process";i:20;s:19:"grid_nc_per_process";i:21;s:19:"grid_abnormality_ci";}');
INSERT INTO isms_user_preference (fkuser, spreference, tvalue) VALUES (6000, 'dashboard_sumary_pref', 'a:22:{i:0;s:22:"grid_financial_summary";i:1;s:17:"grid_risk_summary";i:2;s:18:"grid_risk_summary2";i:3;s:26:"grid_residual_risk_summary";i:4;s:31:"grid_isms_reports_and_documents";i:5;s:26:"grid_best_practice_summary";i:6;s:20:"grid_control_summary";i:7;s:25:"grid_abnormalitys_summary";i:8;s:15:"grid_doc_sumary";i:9;s:31:"grid_doc_context_with_documents";i:10;s:22:"grid_doc_approver_time";i:11;s:14:"grid_doc_top10";i:12;s:16:"grid_doc_by_type";i:13;s:24:"grid_doc_period_revision";i:14;s:19:"grid_abnormality_pm";i:15;s:21:"grid_incident_summary";i:16;s:15:"grid_nc_summary";i:17;s:23:"grid_incidents_per_user";i:18;s:24:"grid_incidents_per_asset";i:19;s:26:"grid_incidents_per_process";i:20;s:19:"grid_nc_per_process";i:21;s:19:"grid_abnormality_ci";}');
INSERT INTO isms_user_preference (fkuser, spreference, tvalue) VALUES (6004, 'dashboard_sumary_pref', 'a:22:{i:0;s:22:"grid_financial_summary";i:1;s:17:"grid_risk_summary";i:2;s:18:"grid_risk_summary2";i:3;s:26:"grid_residual_risk_summary";i:4;s:31:"grid_isms_reports_and_documents";i:5;s:26:"grid_best_practice_summary";i:6;s:20:"grid_control_summary";i:7;s:25:"grid_abnormalitys_summary";i:8;s:15:"grid_doc_sumary";i:9;s:31:"grid_doc_context_with_documents";i:10;s:22:"grid_doc_approver_time";i:11;s:14:"grid_doc_top10";i:12;s:16:"grid_doc_by_type";i:13;s:24:"grid_doc_period_revision";i:14;s:19:"grid_abnormality_pm";i:15;s:21:"grid_incident_summary";i:16;s:15:"grid_nc_summary";i:17;s:23:"grid_incidents_per_user";i:18;s:24:"grid_incidents_per_asset";i:19;s:26:"grid_incidents_per_process";i:20;s:19:"grid_nc_per_process";i:21;s:19:"grid_abnormality_ci";}');
INSERT INTO isms_user_preference (fkuser, spreference, tvalue) VALUES (6006, 'dashboard_sumary_pref', 'a:22:{i:0;s:22:"grid_financial_summary";i:1;s:17:"grid_risk_summary";i:2;s:18:"grid_risk_summary2";i:3;s:26:"grid_residual_risk_summary";i:4;s:31:"grid_isms_reports_and_documents";i:5;s:26:"grid_best_practice_summary";i:6;s:20:"grid_control_summary";i:7;s:25:"grid_abnormalitys_summary";i:8;s:15:"grid_doc_sumary";i:9;s:31:"grid_doc_context_with_documents";i:10;s:22:"grid_doc_approver_time";i:11;s:14:"grid_doc_top10";i:12;s:16:"grid_doc_by_type";i:13;s:24:"grid_doc_period_revision";i:14;s:19:"grid_abnormality_pm";i:15;s:21:"grid_incident_summary";i:16;s:15:"grid_nc_summary";i:17;s:23:"grid_incidents_per_user";i:18;s:24:"grid_incidents_per_asset";i:19;s:26:"grid_incidents_per_process";i:20;s:19:"grid_nc_per_process";i:21;s:19:"grid_abnormality_ci";}');
INSERT INTO isms_user_preference (fkuser, spreference, tvalue) VALUES (6003, 'dashboard_sumary_pref', 'a:22:{i:0;s:22:"grid_financial_summary";i:1;s:17:"grid_risk_summary";i:2;s:18:"grid_risk_summary2";i:3;s:26:"grid_residual_risk_summary";i:4;s:31:"grid_isms_reports_and_documents";i:5;s:26:"grid_best_practice_summary";i:6;s:20:"grid_control_summary";i:7;s:25:"grid_abnormalitys_summary";i:8;s:15:"grid_doc_sumary";i:9;s:31:"grid_doc_context_with_documents";i:10;s:22:"grid_doc_approver_time";i:11;s:14:"grid_doc_top10";i:12;s:16:"grid_doc_by_type";i:13;s:24:"grid_doc_period_revision";i:14;s:19:"grid_abnormality_pm";i:15;s:21:"grid_incident_summary";i:16;s:15:"grid_nc_summary";i:17;s:23:"grid_incidents_per_user";i:18;s:24:"grid_incidents_per_asset";i:19;s:26:"grid_incidents_per_process";i:20;s:19:"grid_nc_per_process";i:21;s:19:"grid_abnormality_ci";}');
INSERT INTO isms_user_preference (fkuser, spreference, tvalue) VALUES (6001, 'dashboard_sumary_pref', 'a:22:{i:0;s:22:"grid_financial_summary";i:1;s:17:"grid_risk_summary";i:2;s:18:"grid_risk_summary2";i:3;s:26:"grid_residual_risk_summary";i:4;s:31:"grid_isms_reports_and_documents";i:5;s:26:"grid_best_practice_summary";i:6;s:20:"grid_control_summary";i:7;s:25:"grid_abnormalitys_summary";i:8;s:15:"grid_doc_sumary";i:9;s:31:"grid_doc_context_with_documents";i:10;s:22:"grid_doc_approver_time";i:11;s:14:"grid_doc_top10";i:12;s:16:"grid_doc_by_type";i:13;s:24:"grid_doc_period_revision";i:14;s:19:"grid_abnormality_pm";i:15;s:21:"grid_incident_summary";i:16;s:15:"grid_nc_summary";i:17;s:23:"grid_incidents_per_user";i:18;s:24:"grid_incidents_per_asset";i:19;s:26:"grid_incidents_per_process";i:20;s:19:"grid_nc_per_process";i:21;s:19:"grid_abnormality_ci";}');
INSERT INTO isms_user_preference (fkuser, spreference, tvalue) VALUES (6002, 'dashboard_sumary_pref', 'a:22:{i:0;s:22:"grid_financial_summary";i:1;s:17:"grid_risk_summary";i:2;s:18:"grid_risk_summary2";i:3;s:26:"grid_residual_risk_summary";i:4;s:31:"grid_isms_reports_and_documents";i:5;s:26:"grid_best_practice_summary";i:6;s:20:"grid_control_summary";i:7;s:25:"grid_abnormalitys_summary";i:8;s:15:"grid_doc_sumary";i:9;s:31:"grid_doc_context_with_documents";i:10;s:22:"grid_doc_approver_time";i:11;s:14:"grid_doc_top10";i:12;s:16:"grid_doc_by_type";i:13;s:24:"grid_doc_period_revision";i:14;s:19:"grid_abnormality_pm";i:15;s:21:"grid_incident_summary";i:16;s:15:"grid_nc_summary";i:17;s:23:"grid_incidents_per_user";i:18;s:24:"grid_incidents_per_asset";i:19;s:26:"grid_incidents_per_process";i:20;s:19:"grid_nc_per_process";i:21;s:19:"grid_abnormality_ci";}');
INSERT INTO isms_user_preference (fkuser, spreference, tvalue) VALUES (6007, 'dashboard_sumary_pref', 'a:22:{i:0;s:22:"grid_financial_summary";i:1;s:17:"grid_risk_summary";i:2;s:18:"grid_risk_summary2";i:3;s:26:"grid_residual_risk_summary";i:4;s:31:"grid_isms_reports_and_documents";i:5;s:26:"grid_best_practice_summary";i:6;s:20:"grid_control_summary";i:7;s:25:"grid_abnormalitys_summary";i:8;s:15:"grid_doc_sumary";i:9;s:31:"grid_doc_context_with_documents";i:10;s:22:"grid_doc_approver_time";i:11;s:14:"grid_doc_top10";i:12;s:16:"grid_doc_by_type";i:13;s:24:"grid_doc_period_revision";i:14;s:19:"grid_abnormality_pm";i:15;s:21:"grid_incident_summary";i:16;s:15:"grid_nc_summary";i:17;s:23:"grid_incidents_per_user";i:18;s:24:"grid_incidents_per_asset";i:19;s:26:"grid_incidents_per_process";i:20;s:19:"grid_nc_per_process";i:21;s:19:"grid_abnormality_ci";}');
INSERT INTO isms_user_preference (fkuser, spreference, tvalue) VALUES (6005, 'dashboard_sumary_pref', 'a:22:{i:0;s:22:"grid_financial_summary";i:1;s:17:"grid_risk_summary";i:2;s:18:"grid_risk_summary2";i:3;s:26:"grid_residual_risk_summary";i:4;s:31:"grid_isms_reports_and_documents";i:5;s:26:"grid_best_practice_summary";i:6;s:20:"grid_control_summary";i:7;s:25:"grid_abnormalitys_summary";i:8;s:15:"grid_doc_sumary";i:9;s:31:"grid_doc_context_with_documents";i:10;s:22:"grid_doc_approver_time";i:11;s:14:"grid_doc_top10";i:12;s:16:"grid_doc_by_type";i:13;s:24:"grid_doc_period_revision";i:14;s:19:"grid_abnormality_pm";i:15;s:21:"grid_incident_summary";i:16;s:15:"grid_nc_summary";i:17;s:23:"grid_incidents_per_user";i:18;s:24:"grid_incidents_per_asset";i:19;s:26:"grid_incidents_per_process";i:20;s:19:"grid_nc_per_process";i:21;s:19:"grid_abnormality_ci";}');


--
-- TOC entry 2715 (class 0 OID 4199297)
-- Dependencies: 1492
-- Data for Name: pm_doc_approvers; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO pm_doc_approvers (fkdocument, fkuser, bhasapproved, bmanual, bcontextapprover) VALUES (6056, 6002, 1, 0, 1);
INSERT INTO pm_doc_approvers (fkdocument, fkuser, bhasapproved, bmanual, bcontextapprover) VALUES (6025, 6004, 1, 0, 1);
INSERT INTO pm_doc_approvers (fkdocument, fkuser, bhasapproved, bmanual, bcontextapprover) VALUES (6023, 6004, 1, 0, 1);
INSERT INTO pm_doc_approvers (fkdocument, fkuser, bhasapproved, bmanual, bcontextapprover) VALUES (6013, 6004, 1, 0, 1);
INSERT INTO pm_doc_approvers (fkdocument, fkuser, bhasapproved, bmanual, bcontextapprover) VALUES (6027, 6006, 1, 0, 1);
INSERT INTO pm_doc_approvers (fkdocument, fkuser, bhasapproved, bmanual, bcontextapprover) VALUES (6017, 6006, 1, 0, 1);
INSERT INTO pm_doc_approvers (fkdocument, fkuser, bhasapproved, bmanual, bcontextapprover) VALUES (6015, 6007, 1, 0, 1);
INSERT INTO pm_doc_approvers (fkdocument, fkuser, bhasapproved, bmanual, bcontextapprover) VALUES (6061, 6005, 1, 0, 1);
INSERT INTO pm_doc_approvers (fkdocument, fkuser, bhasapproved, bmanual, bcontextapprover) VALUES (6123, 6000, 1, 1, 0);
INSERT INTO pm_doc_approvers (fkdocument, fkuser, bhasapproved, bmanual, bcontextapprover) VALUES (6123, 13, 1, 1, 0);
INSERT INTO pm_doc_approvers (fkdocument, fkuser, bhasapproved, bmanual, bcontextapprover) VALUES (6043, 6003, 0, 0, 1);
INSERT INTO pm_doc_approvers (fkdocument, fkuser, bhasapproved, bmanual, bcontextapprover) VALUES (6050, 6004, 0, 0, 1);


--
-- TOC entry 2716 (class 0 OID 4199309)
-- Dependencies: 1493
-- Data for Name: pm_doc_context; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO pm_doc_context (fkcontext, fkdocument) VALUES (6012, 6013);
INSERT INTO pm_doc_context (fkcontext, fkdocument) VALUES (6014, 6015);
INSERT INTO pm_doc_context (fkcontext, fkdocument) VALUES (6016, 6017);
INSERT INTO pm_doc_context (fkcontext, fkdocument) VALUES (6022, 6023);
INSERT INTO pm_doc_context (fkcontext, fkdocument) VALUES (6024, 6025);
INSERT INTO pm_doc_context (fkcontext, fkdocument) VALUES (6026, 6027);
INSERT INTO pm_doc_context (fkcontext, fkdocument) VALUES (6042, 6043);
INSERT INTO pm_doc_context (fkcontext, fkdocument) VALUES (6046, 6047);
INSERT INTO pm_doc_context (fkcontext, fkdocument) VALUES (6049, 6050);
INSERT INTO pm_doc_context (fkcontext, fkdocument) VALUES (6055, 6056);
INSERT INTO pm_doc_context (fkcontext, fkdocument) VALUES (6060, 6061);


--
-- TOC entry 2717 (class 0 OID 4199315)
-- Dependencies: 1494
-- Data for Name: pm_doc_instance; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO pm_doc_instance (fkcontext, fkdocument, nmajorversion, nrevisionversion, trevisionjustification, spath, tmodifycomment, dbeginproduction, dendproduction, sfilename, bislink, slink, dbeginapprover, dbeginrevision, nfilesize, smanualversion) VALUES (6133, 6047, 1, 0, NULL, '', 'Publish', '2007-12-25 00:00:00', NULL, '', 1, 'http://intranet/isms/controls/nda', '2008-04-09 15:37:28', NULL, 0, '1.0');
INSERT INTO pm_doc_instance (fkcontext, fkdocument, nmajorversion, nrevisionversion, trevisionjustification, spath, tmodifycomment, dbeginproduction, dendproduction, sfilename, bislink, slink, dbeginapprover, dbeginrevision, nfilesize, smanualversion) VALUES (6127, 6015, 1, 0, NULL, '', 'publish', '2008-01-01 00:00:00', NULL, '', 1, 'http://intranet/isms/area/customer', '2008-04-09 15:54:58', NULL, 0, '1.0');
INSERT INTO pm_doc_instance (fkcontext, fkdocument, nmajorversion, nrevisionversion, trevisionjustification, spath, tmodifycomment, dbeginproduction, dendproduction, sfilename, bislink, slink, dbeginapprover, dbeginrevision, nfilesize, smanualversion) VALUES (6128, 6013, 1, 0, NULL, '', 'publish', '2008-01-07 00:00:00', NULL, '', 1, 'http://intranet/isms/area/infotecnology', '2008-04-09 15:51:02', NULL, 0, '1.0');
INSERT INTO pm_doc_instance (fkcontext, fkdocument, nmajorversion, nrevisionversion, trevisionjustification, spath, tmodifycomment, dbeginproduction, dendproduction, sfilename, bislink, slink, dbeginapprover, dbeginrevision, nfilesize, smanualversion) VALUES (6129, 6017, 1, 0, NULL, '', 'publish', '2008-01-02 00:00:00', NULL, '', 1, 'http://intranet/isms/area/researchdev', '2008-04-09 15:53:21', NULL, 0, '1.0');
INSERT INTO pm_doc_instance (fkcontext, fkdocument, nmajorversion, nrevisionversion, trevisionjustification, spath, tmodifycomment, dbeginproduction, dendproduction, sfilename, bislink, slink, dbeginapprover, dbeginrevision, nfilesize, smanualversion) VALUES (6130, 6050, 1, 0, NULL, '', 'publish', '2008-01-08 00:00:00', NULL, '', 1, 'http://intranet/isms/controls/capacityplanning', '2008-04-09 15:50:47', NULL, 0, '');
INSERT INTO pm_doc_instance (fkcontext, fkdocument, nmajorversion, nrevisionversion, trevisionjustification, spath, tmodifycomment, dbeginproduction, dendproduction, sfilename, bislink, slink, dbeginapprover, dbeginrevision, nfilesize, smanualversion) VALUES (6131, 6061, 1, 0, NULL, '', 'publish', '2008-01-02 00:00:00', NULL, '', 1, 'http://intranet/isms/controls/cleardesk', '2008-04-09 16:28:33', NULL, 0, '1.0');
INSERT INTO pm_doc_instance (fkcontext, fkdocument, nmajorversion, nrevisionversion, trevisionjustification, spath, tmodifycomment, dbeginproduction, dendproduction, sfilename, bislink, slink, dbeginapprover, dbeginrevision, nfilesize, smanualversion) VALUES (6132, 6043, 1, 0, NULL, '', 'publish', '2008-01-07 00:00:00', NULL, '', 1, 'http://intranet/isms/controls/viruslcontrol', '2008-04-09 15:48:49', NULL, 0, '1.0');
INSERT INTO pm_doc_instance (fkcontext, fkdocument, nmajorversion, nrevisionversion, trevisionjustification, spath, tmodifycomment, dbeginproduction, dendproduction, sfilename, bislink, slink, dbeginapprover, dbeginrevision, nfilesize, smanualversion) VALUES (6134, 6056, 1, 0, NULL, '', 'publish', '2007-12-19 00:00:00', NULL, '', 1, 'http://intranet/isms/controls/rightsmanagment', '2008-04-09 15:45:10', NULL, 0, '1.0');
INSERT INTO pm_doc_instance (fkcontext, fkdocument, nmajorversion, nrevisionversion, trevisionjustification, spath, tmodifycomment, dbeginproduction, dendproduction, sfilename, bislink, slink, dbeginapprover, dbeginrevision, nfilesize, smanualversion) VALUES (6137, 6025, 1, 0, NULL, '', 'publish', '2007-12-11 00:00:00', NULL, '', 1, 'http://intranet/isms/process/systemdev', '2008-04-09 15:50:18', NULL, 0, '1.0');
INSERT INTO pm_doc_instance (fkcontext, fkdocument, nmajorversion, nrevisionversion, trevisionjustification, spath, tmodifycomment, dbeginproduction, dendproduction, sfilename, bislink, slink, dbeginapprover, dbeginrevision, nfilesize, smanualversion) VALUES (6135, 6023, 1, 0, NULL, '', 'publish', '2008-01-07 00:00:00', NULL, '', 1, 'http://intranet/isms/process/netadmin', '2008-04-09 15:50:32', NULL, 0, '1.0');
INSERT INTO pm_doc_instance (fkcontext, fkdocument, nmajorversion, nrevisionversion, trevisionjustification, spath, tmodifycomment, dbeginproduction, dendproduction, sfilename, bislink, slink, dbeginapprover, dbeginrevision, nfilesize, smanualversion) VALUES (6136, 6027, 1, 0, NULL, '', 'publish', '2008-01-01 00:00:00', NULL, '', 1, 'http://intranet/isms/process/productdev', '2008-04-09 15:52:52', NULL, 0, '1.0');
INSERT INTO pm_doc_instance (fkcontext, fkdocument, nmajorversion, nrevisionversion, trevisionjustification, spath, tmodifycomment, dbeginproduction, dendproduction, sfilename, bislink, slink, dbeginapprover, dbeginrevision, nfilesize, smanualversion) VALUES (6124, 6123, 1, 0, NULL, '', 'approve', '2007-10-24 00:00:00', NULL, '', 1, 'http://intranet/isms/document/ISMSManual', '2008-04-09 16:39:56', NULL, 0, '1.0');
INSERT INTO pm_doc_instance (fkcontext, fkdocument, nmajorversion, nrevisionversion, trevisionjustification, spath, tmodifycomment, dbeginproduction, dendproduction, sfilename, bislink, slink, dbeginapprover, dbeginrevision, nfilesize, smanualversion) VALUES (6126, 6125, 1, 0, NULL, '', 'publish', '2007-10-31 00:00:00', NULL, '', 1, 'http://intranet/isms/infopolicy', '2008-04-09 16:29:25', NULL, 0, '1.0');


--
-- TOC entry 2719 (class 0 OID 4199341)
-- Dependencies: 1496
-- Data for Name: pm_doc_read_history; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO pm_doc_read_history (ddate, fkinstance, fkuser) VALUES ('2008-04-10 11:03:20', 6127, 13);


--
-- TOC entry 2718 (class 0 OID 4199329)
-- Dependencies: 1495
-- Data for Name: pm_doc_readers; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO pm_doc_readers (fkdocument, fkuser, bhasread, bmanual, bdenied) VALUES (6123, 6004, 0, 1, 0);
INSERT INTO pm_doc_readers (fkdocument, fkuser, bhasread, bmanual, bdenied) VALUES (6123, 6006, 0, 1, 0);
INSERT INTO pm_doc_readers (fkdocument, fkuser, bhasread, bmanual, bdenied) VALUES (6123, 6001, 0, 1, 0);
INSERT INTO pm_doc_readers (fkdocument, fkuser, bhasread, bmanual, bdenied) VALUES (6123, 6002, 0, 1, 0);
INSERT INTO pm_doc_readers (fkdocument, fkuser, bhasread, bmanual, bdenied) VALUES (6123, 6000, 0, 1, 0);
INSERT INTO pm_doc_readers (fkdocument, fkuser, bhasread, bmanual, bdenied) VALUES (6123, 6007, 0, 1, 0);
INSERT INTO pm_doc_readers (fkdocument, fkuser, bhasread, bmanual, bdenied) VALUES (6123, 6003, 0, 1, 0);
INSERT INTO pm_doc_readers (fkdocument, fkuser, bhasread, bmanual, bdenied) VALUES (6123, 6005, 0, 1, 0);
INSERT INTO pm_doc_readers (fkdocument, fkuser, bhasread, bmanual, bdenied) VALUES (6123, 13, 0, 1, 0);
INSERT INTO pm_doc_readers (fkdocument, fkuser, bhasread, bmanual, bdenied) VALUES (6125, 6004, 0, 1, 0);
INSERT INTO pm_doc_readers (fkdocument, fkuser, bhasread, bmanual, bdenied) VALUES (6125, 6006, 0, 1, 0);
INSERT INTO pm_doc_readers (fkdocument, fkuser, bhasread, bmanual, bdenied) VALUES (6125, 6001, 0, 1, 0);
INSERT INTO pm_doc_readers (fkdocument, fkuser, bhasread, bmanual, bdenied) VALUES (6125, 6002, 0, 1, 0);
INSERT INTO pm_doc_readers (fkdocument, fkuser, bhasread, bmanual, bdenied) VALUES (6125, 6000, 0, 1, 0);
INSERT INTO pm_doc_readers (fkdocument, fkuser, bhasread, bmanual, bdenied) VALUES (6125, 6007, 0, 1, 0);
INSERT INTO pm_doc_readers (fkdocument, fkuser, bhasread, bmanual, bdenied) VALUES (6125, 6003, 0, 1, 0);
INSERT INTO pm_doc_readers (fkdocument, fkuser, bhasread, bmanual, bdenied) VALUES (6125, 6005, 0, 1, 0);
INSERT INTO pm_doc_readers (fkdocument, fkuser, bhasread, bmanual, bdenied) VALUES (6125, 13, 0, 1, 0);
INSERT INTO pm_doc_readers (fkdocument, fkuser, bhasread, bmanual, bdenied) VALUES (6056, 13, 0, 0, 0);
INSERT INTO pm_doc_readers (fkdocument, fkuser, bhasread, bmanual, bdenied) VALUES (6050, 13, 0, 0, 0);
INSERT INTO pm_doc_readers (fkdocument, fkuser, bhasread, bmanual, bdenied) VALUES (6047, 13, 0, 0, 0);
INSERT INTO pm_doc_readers (fkdocument, fkuser, bhasread, bmanual, bdenied) VALUES (6061, 13, 0, 0, 0);
INSERT INTO pm_doc_readers (fkdocument, fkuser, bhasread, bmanual, bdenied) VALUES (6043, 13, 0, 0, 0);
INSERT INTO pm_doc_readers (fkdocument, fkuser, bhasread, bmanual, bdenied) VALUES (6056, 6007, 0, 0, 0);
INSERT INTO pm_doc_readers (fkdocument, fkuser, bhasread, bmanual, bdenied) VALUES (6050, 6007, 0, 0, 0);
INSERT INTO pm_doc_readers (fkdocument, fkuser, bhasread, bmanual, bdenied) VALUES (6047, 6007, 0, 0, 0);
INSERT INTO pm_doc_readers (fkdocument, fkuser, bhasread, bmanual, bdenied) VALUES (6061, 6007, 0, 0, 0);
INSERT INTO pm_doc_readers (fkdocument, fkuser, bhasread, bmanual, bdenied) VALUES (6043, 6007, 0, 0, 0);
INSERT INTO pm_doc_readers (fkdocument, fkuser, bhasread, bmanual, bdenied) VALUES (6056, 6006, 0, 0, 0);
INSERT INTO pm_doc_readers (fkdocument, fkuser, bhasread, bmanual, bdenied) VALUES (6050, 6006, 0, 0, 0);
INSERT INTO pm_doc_readers (fkdocument, fkuser, bhasread, bmanual, bdenied) VALUES (6047, 6006, 0, 0, 0);
INSERT INTO pm_doc_readers (fkdocument, fkuser, bhasread, bmanual, bdenied) VALUES (6061, 6006, 0, 0, 0);
INSERT INTO pm_doc_readers (fkdocument, fkuser, bhasread, bmanual, bdenied) VALUES (6043, 6006, 0, 0, 0);
INSERT INTO pm_doc_readers (fkdocument, fkuser, bhasread, bmanual, bdenied) VALUES (6056, 6004, 0, 0, 0);
INSERT INTO pm_doc_readers (fkdocument, fkuser, bhasread, bmanual, bdenied) VALUES (6043, 6004, 0, 0, 0);
INSERT INTO pm_doc_readers (fkdocument, fkuser, bhasread, bmanual, bdenied) VALUES (6050, 6004, 0, 0, 0);


--
-- TOC entry 2720 (class 0 OID 4199349)
-- Dependencies: 1498
-- Data for Name: pm_doc_reference; Type: TABLE DATA; Schema: public; Owner: isms
--



--
-- TOC entry 2721 (class 0 OID 4199356)
-- Dependencies: 1499
-- Data for Name: pm_doc_registers; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO pm_doc_registers (fkregister, fkdocument) VALUES (6139, 6047);
INSERT INTO pm_doc_registers (fkregister, fkdocument) VALUES (6140, 6050);
INSERT INTO pm_doc_registers (fkregister, fkdocument) VALUES (6138, 6043);


--
-- TOC entry 2713 (class 0 OID 4199264)
-- Dependencies: 1490
-- Data for Name: pm_document; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO pm_document (fkcontext, fkschedule, fkclassification, fkcurrentversion, fkparent, fkauthor, fkmainapprover, sname, tdescription, ntype, skeywords, ddateproduction, ddeadline, bflagdeadlinealert, bflagdeadlineexpired, ndaysbefore, bhasapproved) VALUES (6047, NULL, NULL, 6133, NULL, 13, 13, 'Control Non Disclosure Agreements Control Document', 'Control Non Disclosure Agreements Control Document', 2805, 'NDA control disclosure agreement', '2007-12-25 00:00:00', '2008-01-02 00:00:00', 1, 1, 3, 1);
INSERT INTO pm_document (fkcontext, fkschedule, fkclassification, fkcurrentversion, fkparent, fkauthor, fkmainapprover, sname, tdescription, ntype, skeywords, ddateproduction, ddeadline, bflagdeadlinealert, bflagdeadlineexpired, ndaysbefore, bhasapproved) VALUES (6015, NULL, NULL, 6127, NULL, 6007, 13, 'Area CR - Customer Relationship Document', 'Area CR - Customer Relationship Document', 2801, 'customer, relashionship, area', '2008-01-01 00:00:00', '2008-04-03 00:00:00', 1, 1, 3, 1);
INSERT INTO pm_document (fkcontext, fkschedule, fkclassification, fkcurrentversion, fkparent, fkauthor, fkmainapprover, sname, tdescription, ntype, skeywords, ddateproduction, ddeadline, bflagdeadlinealert, bflagdeadlineexpired, ndaysbefore, bhasapproved) VALUES (6013, NULL, NULL, 6128, NULL, 6004, 13, 'Area IT - Information Tecnology Document', 'Area IT - Information Tecnology Document', 2801, 'IT, area, information, tecnology', '2008-01-07 00:00:00', '2008-01-08 00:00:00', 1, 1, 2, 1);
INSERT INTO pm_document (fkcontext, fkschedule, fkclassification, fkcurrentversion, fkparent, fkauthor, fkmainapprover, sname, tdescription, ntype, skeywords, ddateproduction, ddeadline, bflagdeadlinealert, bflagdeadlineexpired, ndaysbefore, bhasapproved) VALUES (6017, NULL, NULL, 6129, NULL, 6006, 13, 'Area RD - Research and Development Document', 'Area RD - Research and Development Document', 2801, 'research, development, area', '2008-01-02 00:00:00', '2008-01-04 00:00:00', 1, 1, 2, 1);
INSERT INTO pm_document (fkcontext, fkschedule, fkclassification, fkcurrentversion, fkparent, fkauthor, fkmainapprover, sname, tdescription, ntype, skeywords, ddateproduction, ddeadline, bflagdeadlinealert, bflagdeadlineexpired, ndaysbefore, bhasapproved) VALUES (6050, NULL, NULL, 6130, NULL, 6004, 13, 'Control Capacity Planning Document', 'Control Capacity Planning Document', 2805, 'capacity planning performance control', '2008-01-08 00:00:00', '2008-01-10 00:00:00', 1, 1, 1, 1);
INSERT INTO pm_document (fkcontext, fkschedule, fkclassification, fkcurrentversion, fkparent, fkauthor, fkmainapprover, sname, tdescription, ntype, skeywords, ddateproduction, ddeadline, bflagdeadlinealert, bflagdeadlineexpired, ndaysbefore, bhasapproved) VALUES (6061, NULL, NULL, 6131, NULL, 6005, 13, 'Control Clear Desk and Clear Screen Control Document', 'Control Clear Desk and Clear Screen Control Document', 2805, 'control clear desk', '2008-01-02 00:00:00', '2008-01-03 00:00:00', 1, 1, 1, 1);
INSERT INTO pm_document (fkcontext, fkschedule, fkclassification, fkcurrentversion, fkparent, fkauthor, fkmainapprover, sname, tdescription, ntype, skeywords, ddateproduction, ddeadline, bflagdeadlinealert, bflagdeadlineexpired, ndaysbefore, bhasapproved) VALUES (6043, NULL, NULL, 6132, NULL, 6003, 13, 'Control Computer Virus Control Document', 'Control Computer Virus Control Document', 2805, 'virus malicious code control', '2008-01-07 00:00:00', '2008-01-09 00:00:00', 1, 1, 1, 1);
INSERT INTO pm_document (fkcontext, fkschedule, fkclassification, fkcurrentversion, fkparent, fkauthor, fkmainapprover, sname, tdescription, ntype, skeywords, ddateproduction, ddeadline, bflagdeadlinealert, bflagdeadlineexpired, ndaysbefore, bhasapproved) VALUES (6056, NULL, NULL, 6134, NULL, 6002, 13, 'Control System Rights Management Document', 'Control System Rights Management Document', 2805, 'rights privilegies management control', '2007-12-19 00:00:00', '2007-12-20 00:00:00', 1, 1, 1, 1);
INSERT INTO pm_document (fkcontext, fkschedule, fkclassification, fkcurrentversion, fkparent, fkauthor, fkmainapprover, sname, tdescription, ntype, skeywords, ddateproduction, ddeadline, bflagdeadlinealert, bflagdeadlineexpired, ndaysbefore, bhasapproved) VALUES (6025, NULL, NULL, 6137, NULL, 6004, 13, 'Process System Development and Maintenance Document', 'Process System Development and Maintenance Document', 2802, 'system development process description', '2007-12-11 00:00:00', '2007-12-31 00:00:00', 1, 1, 2, 1);
INSERT INTO pm_document (fkcontext, fkschedule, fkclassification, fkcurrentversion, fkparent, fkauthor, fkmainapprover, sname, tdescription, ntype, skeywords, ddateproduction, ddeadline, bflagdeadlinealert, bflagdeadlineexpired, ndaysbefore, bhasapproved) VALUES (6023, NULL, NULL, 6135, NULL, 6004, 13, 'Process Network Administration Document', 'Process Network Administration Document', 2802, 'network process desciption', '2008-01-07 00:00:00', '2008-01-15 00:00:00', 1, 1, 2, 1);
INSERT INTO pm_document (fkcontext, fkschedule, fkclassification, fkcurrentversion, fkparent, fkauthor, fkmainapprover, sname, tdescription, ntype, skeywords, ddateproduction, ddeadline, bflagdeadlinealert, bflagdeadlineexpired, ndaysbefore, bhasapproved) VALUES (6027, NULL, NULL, 6136, NULL, 6006, 13, 'Process New Product Development Document', 'Process New Product Development Document', 2802, 'product development process description', '2008-01-01 00:00:00', '2008-01-04 00:00:00', 1, 1, 2, 1);
INSERT INTO pm_document (fkcontext, fkschedule, fkclassification, fkcurrentversion, fkparent, fkauthor, fkmainapprover, sname, tdescription, ntype, skeywords, ddateproduction, ddeadline, bflagdeadlinealert, bflagdeadlineexpired, ndaysbefore, bhasapproved) VALUES (6123, NULL, NULL, 6124, NULL, 6005, 6006, 'ISMS General Manual', '', 3801, 'ISMS, Manual, Information Security. Management', '2007-10-24 00:00:00', '2007-11-01 00:00:00', 1, 1, 3, 1);
INSERT INTO pm_document (fkcontext, fkschedule, fkclassification, fkcurrentversion, fkparent, fkauthor, fkmainapprover, sname, tdescription, ntype, skeywords, ddateproduction, ddeadline, bflagdeadlinealert, bflagdeadlineexpired, ndaysbefore, bhasapproved) VALUES (6125, NULL, NULL, 6126, NULL, 6005, 6006, 'Information Security Policy', '', 2821, 'Information, security, policy', '2007-10-31 00:00:00', '2007-11-01 00:00:00', 1, 1, 3, 1);


--
-- TOC entry 2714 (class 0 OID 4199286)
-- Dependencies: 1491
-- Data for Name: pm_document_revision_history; Type: TABLE DATA; Schema: public; Owner: isms
--



--
-- TOC entry 2722 (class 0 OID 4199364)
-- Dependencies: 1501
-- Data for Name: pm_instance_comment; Type: TABLE DATA; Schema: public; Owner: isms
--



--
-- TOC entry 2723 (class 0 OID 4199375)
-- Dependencies: 1502
-- Data for Name: pm_instance_content; Type: TABLE DATA; Schema: public; Owner: isms
--



--
-- TOC entry 2724 (class 0 OID 4199383)
-- Dependencies: 1503
-- Data for Name: pm_process_user; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO pm_process_user (fkprocess, fkuser) VALUES (6018, 6007);
INSERT INTO pm_process_user (fkprocess, fkuser) VALUES (6020, 13);
INSERT INTO pm_process_user (fkprocess, fkuser) VALUES (6022, 6004);
INSERT INTO pm_process_user (fkprocess, fkuser) VALUES (6024, 6004);
INSERT INTO pm_process_user (fkprocess, fkuser) VALUES (6026, 6006);
INSERT INTO pm_process_user (fkprocess, fkuser) VALUES (6028, 6006);


--
-- TOC entry 2725 (class 0 OID 4199389)
-- Dependencies: 1504
-- Data for Name: pm_register; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO pm_register (fkcontext, fkclassification, fkdocument, fkresponsible, sname, nperiod, nvalue, sstorageplace, sstoragetype, sindexingtype, sdisposition, sprotectionrequirements, sorigin) VALUES (6138, NULL, NULL, 6003, 'Virus Scan Control Sheet', 7803, 36, 'IT Phisical File', 'folders', 'by date', '', 'regular controls', 'internal');
INSERT INTO pm_register (fkcontext, fkclassification, fkdocument, fkresponsible, sname, nperiod, nvalue, sstorageplace, sstoragetype, sindexingtype, sdisposition, sprotectionrequirements, sorigin) VALUES (6139, NULL, NULL, 13, 'NDA Agreement', 7803, 36, 'Human Resources Room', 'folders', 'by name', '', 'general protection', 'internal');
INSERT INTO pm_register (fkcontext, fkclassification, fkdocument, fkresponsible, sname, nperiod, nvalue, sstorageplace, sstoragetype, sindexingtype, sdisposition, sprotectionrequirements, sorigin) VALUES (6140, NULL, NULL, 6004, 'System Capacity Reports', 7803, 36, 'IT File', 'digital file', 'by date', '', 'general protection', 'external - monitoring service provider');


--
-- TOC entry 2726 (class 0 OID 4199399)
-- Dependencies: 1505
-- Data for Name: pm_register_readers; Type: TABLE DATA; Schema: public; Owner: isms
--



--
-- TOC entry 2727 (class 0 OID 4199410)
-- Dependencies: 1506
-- Data for Name: pm_template; Type: TABLE DATA; Schema: public; Owner: isms
--



--
-- TOC entry 2728 (class 0 OID 4199420)
-- Dependencies: 1507
-- Data for Name: pm_template_best_practice; Type: TABLE DATA; Schema: public; Owner: isms
--



--
-- TOC entry 2729 (class 0 OID 4199427)
-- Dependencies: 1508
-- Data for Name: pm_template_content; Type: TABLE DATA; Schema: public; Owner: isms
--



--
-- TOC entry 2730 (class 0 OID 4199435)
-- Dependencies: 1509
-- Data for Name: rm_area; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO rm_area (fkcontext, fkpriority, fktype, fkparent, fkresponsible, sname, tdescription, sdocument, nvalue) VALUES (6010, 5, 1, NULL, 6000, 'Sample Corporation S.A.', '', NULL, 8);
INSERT INTO rm_area (fkcontext, fkpriority, fktype, fkparent, fkresponsible, sname, tdescription, sdocument, nvalue) VALUES (6014, 5, 1, 6010, 6007, 'CR - Customer Relationship', '', NULL, 8);
INSERT INTO rm_area (fkcontext, fkpriority, fktype, fkparent, fkresponsible, sname, tdescription, sdocument, nvalue) VALUES (6016, 5, 1, 6010, 6006, 'RD - Research and Development', '', NULL, 8);
INSERT INTO rm_area (fkcontext, fkpriority, fktype, fkparent, fkresponsible, sname, tdescription, sdocument, nvalue) VALUES (6012, 5, 1, 6010, 6004, 'IT - Information Tecnology', '', NULL, 8);


--
-- TOC entry 2731 (class 0 OID 4199448)
-- Dependencies: 1510
-- Data for Name: rm_asset; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO rm_asset (fkcontext, fkcategory, fkresponsible, fksecurityresponsible, sname, tdescription, sdocument, ncost, nvalue, blegality, tjustification) VALUES (6036, 100, 6007, 6007, 'Customer Database', '', NULL, 1500000, 7.666666666666667, 0, '');
INSERT INTO rm_asset (fkcontext, fkcategory, fkresponsible, fksecurityresponsible, sname, tdescription, sdocument, ncost, nvalue, blegality, tjustification) VALUES (6038, 100, 6006, 6006, 'Products Specifications', '', NULL, 350000, 7, 0, '');
INSERT INTO rm_asset (fkcontext, fkcategory, fkresponsible, fksecurityresponsible, sname, tdescription, sdocument, ncost, nvalue, blegality, tjustification) VALUES (6030, 236, 6004, 6004, 'Data Network', '', NULL, 450000, 8, 0, '');
INSERT INTO rm_asset (fkcontext, fkcategory, fkresponsible, fksecurityresponsible, sname, tdescription, sdocument, ncost, nvalue, blegality, tjustification) VALUES (6040, 100, 6006, 6006, 'Research Reports', '', NULL, 2500000, 7.333333333333333, 0, '');
INSERT INTO rm_asset (fkcontext, fkcategory, fkresponsible, fksecurityresponsible, sname, tdescription, sdocument, ncost, nvalue, blegality, tjustification) VALUES (6034, 158, 6004, 6004, 'Business Aplication', '', NULL, 750000, 4.5, 0, '');


--
-- TOC entry 2732 (class 0 OID 4199463)
-- Dependencies: 1511
-- Data for Name: rm_asset_asset; Type: TABLE DATA; Schema: public; Owner: isms
--



--
-- TOC entry 2733 (class 0 OID 4199470)
-- Dependencies: 1512
-- Data for Name: rm_asset_value; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO rm_asset_value (fkasset, fkvaluename, fkparametername, tjustification) VALUES (6030, 5, 1, '');
INSERT INTO rm_asset_value (fkasset, fkvaluename, fkparametername, tjustification) VALUES (6030, 4, 2, '');
INSERT INTO rm_asset_value (fkasset, fkvaluename, fkparametername, tjustification) VALUES (6030, 3, 3, '');
INSERT INTO rm_asset_value (fkasset, fkvaluename, fkparametername, tjustification) VALUES (6034, 5, 1, '');
INSERT INTO rm_asset_value (fkasset, fkvaluename, fkparametername, tjustification) VALUES (6034, 5, 2, '');
INSERT INTO rm_asset_value (fkasset, fkvaluename, fkparametername, tjustification) VALUES (6034, 5, 3, '');
INSERT INTO rm_asset_value (fkasset, fkvaluename, fkparametername, tjustification) VALUES (6036, 4, 1, '');
INSERT INTO rm_asset_value (fkasset, fkvaluename, fkparametername, tjustification) VALUES (6036, 5, 2, '');
INSERT INTO rm_asset_value (fkasset, fkvaluename, fkparametername, tjustification) VALUES (6036, 4, 3, '');
INSERT INTO rm_asset_value (fkasset, fkvaluename, fkparametername, tjustification) VALUES (6038, 5, 1, '');
INSERT INTO rm_asset_value (fkasset, fkvaluename, fkparametername, tjustification) VALUES (6038, 2, 2, '');
INSERT INTO rm_asset_value (fkasset, fkvaluename, fkparametername, tjustification) VALUES (6038, 4, 3, '');
INSERT INTO rm_asset_value (fkasset, fkvaluename, fkparametername, tjustification) VALUES (6040, 2, 1, '');
INSERT INTO rm_asset_value (fkasset, fkvaluename, fkparametername, tjustification) VALUES (6040, 5, 2, '');
INSERT INTO rm_asset_value (fkasset, fkvaluename, fkparametername, tjustification) VALUES (6040, 4, 3, '');


--
-- TOC entry 2734 (class 0 OID 4199481)
-- Dependencies: 1513
-- Data for Name: rm_best_practice; Type: TABLE DATA; Schema: public; Owner: isms
--

COPY rm_best_practice (fkcontext, fksectionbestpractice, sname, tdescription, ncontroltype, sclassification, timplementationguide, tmetric, sdocument, tjustification) FROM '/tmp/trialdata/rm_best_practice.sql' DELIMITERS '#' NULL 'NULL';

--
-- TOC entry 2735 (class 0 OID 4199491)
-- Dependencies: 1514
-- Data for Name: rm_best_practice_event; Type: TABLE DATA; Schema: public; Owner: isms
--

COPY rm_best_practice_event (fkcontext, fkbestpractice, fkevent) FROM '/tmp/trialdata/rm_best_practice_event.sql' DELIMITERS '#' NULL 'NULL';



--
-- TOC entry 2736 (class 0 OID 4199498)
-- Dependencies: 1515
-- Data for Name: rm_best_practice_standard; Type: TABLE DATA; Schema: public; Owner: isms
--

COPY rm_best_practice_standard (fkcontext, fkstandard, fkbestpractice) FROM '/tmp/trialdata/rm_best_practice_standard.sql' DELIMITERS '#' NULL 'NULL';

--
-- TOC entry 2737 (class 0 OID 4199505)
-- Dependencies: 1516
-- Data for Name: rm_category; Type: TABLE DATA; Schema: public; Owner: isms
--

COPY rm_category (fkcontext, fkparent, sname, tdescription) FROM '/tmp/trialdata/rm_category.sql' DELIMITERS '#' NULL 'NULL';

--
-- TOC entry 2738 (class 0 OID 4199514)
-- Dependencies: 1517
-- Data for Name: rm_control; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO rm_control (fkcontext, fktype, fkresponsible, sname, tdescription, sdocument, sevidence, ndaysbefore, bisactive, bflagimplalert, bflagimplexpired, ddatedeadline, ddateimplemented, nimplementationstate, bimplementationislate, brevisionislate, btestislate, befficiencynotok, btestnotok) VALUES (6046, 25, 13, 'Non Disclosure Agreements Control', '', NULL, 'NDA Agreements', 5, 1, 0, 0, '2008-01-04 00:00:00', '2007-12-10 00:00:00', 0, 0, 0, 0, 0, 0);
INSERT INTO rm_control (fkcontext, fktype, fkresponsible, sname, tdescription, sdocument, sevidence, ndaysbefore, bisactive, bflagimplalert, bflagimplexpired, ddatedeadline, ddateimplemented, nimplementationstate, bimplementationislate, brevisionislate, btestislate, befficiencynotok, btestnotok) VALUES (6055, 25, 6002, 'System Rights Management', '', NULL, 'System logs and Rights Reports', 2, 1, 0, 0, '2007-12-20 00:00:00', '2008-01-02 00:00:00', 0, 0, 0, 0, 0, 0);
INSERT INTO rm_control (fkcontext, fktype, fkresponsible, sname, tdescription, sdocument, sevidence, ndaysbefore, bisactive, bflagimplalert, bflagimplexpired, ddatedeadline, ddateimplemented, nimplementationstate, bimplementationislate, brevisionislate, btestislate, befficiencynotok, btestnotok) VALUES (6060, 25, 6005, 'Clear Desk and Clear Screen Control', '', NULL, 'Clear Desk and Clear Screen Audits and Incidents', 2, 1, 0, 0, '2007-12-11 00:00:00', '2007-11-28 00:00:00', 0, 0, 0, 0, 0, 0);
INSERT INTO rm_control (fkcontext, fktype, fkresponsible, sname, tdescription, sdocument, sevidence, ndaysbefore, bisactive, bflagimplalert, bflagimplexpired, ddatedeadline, ddateimplemented, nimplementationstate, bimplementationislate, brevisionislate, btestislate, befficiencynotok, btestnotok) VALUES (6042, 25, 6003, 'Computer Virus Control', '', NULL, 'Computer Virus Software Reports', 2, 1, 0, 0, '2007-11-29 00:00:00', '2007-12-11 00:00:00', 0, 0, 0, 0, 0, 0);
INSERT INTO rm_control (fkcontext, fktype, fkresponsible, sname, tdescription, sdocument, sevidence, ndaysbefore, bisactive, bflagimplalert, bflagimplexpired, ddatedeadline, ddateimplemented, nimplementationstate, bimplementationislate, brevisionislate, btestislate, befficiencynotok, btestnotok) VALUES (6049, 25, 6004, 'Capacity Planning', '', NULL, 'Capacity Planning Reports', 2, 1, 0, 0, '2007-12-06 00:00:00', '2007-12-26 00:00:00', 0, 0, 0, 0, 0, 0);


--
-- TOC entry 2739 (class 0 OID 4199542)
-- Dependencies: 1518
-- Data for Name: rm_control_best_practice; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO rm_control_best_practice (fkcontext, fkbestpractice, fkcontrol) VALUES (6044, 1849, 6042);
INSERT INTO rm_control_best_practice (fkcontext, fkbestpractice, fkcontrol) VALUES (6045, 2081, 6042);
INSERT INTO rm_control_best_practice (fkcontext, fkbestpractice, fkcontrol) VALUES (6048, 1806, 6046);
INSERT INTO rm_control_best_practice (fkcontext, fkbestpractice, fkcontrol) VALUES (6051, 1847, 6049);
INSERT INTO rm_control_best_practice (fkcontext, fkbestpractice, fkcontrol) VALUES (6052, 2060, 6049);
INSERT INTO rm_control_best_practice (fkcontext, fkbestpractice, fkcontrol) VALUES (6053, 2058, 6049);
INSERT INTO rm_control_best_practice (fkcontext, fkbestpractice, fkcontrol) VALUES (6054, 2059, 6049);
INSERT INTO rm_control_best_practice (fkcontext, fkbestpractice, fkcontrol) VALUES (6057, 1876, 6055);
INSERT INTO rm_control_best_practice (fkcontext, fkbestpractice, fkcontrol) VALUES (6058, 1826, 6055);
INSERT INTO rm_control_best_practice (fkcontext, fkbestpractice, fkcontrol) VALUES (6059, 1874, 6055);
INSERT INTO rm_control_best_practice (fkcontext, fkbestpractice, fkcontrol) VALUES (6062, 1879, 6060);


--
-- TOC entry 2740 (class 0 OID 4199549)
-- Dependencies: 1519
-- Data for Name: rm_control_cost; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO rm_control_cost (fkcontrol, ncost1, ncost2, ncost3, ncost4, ncost5) VALUES (6046, 0, 0, 5000, 3500, 6000);
INSERT INTO rm_control_cost (fkcontrol, ncost1, ncost2, ncost3, ncost4, ncost5) VALUES (6049, 0, 18000, 23000, 4000, 0);
INSERT INTO rm_control_cost (fkcontrol, ncost1, ncost2, ncost3, ncost4, ncost5) VALUES (6055, 0, 45000, 23000, 5000, 3000);
INSERT INTO rm_control_cost (fkcontrol, ncost1, ncost2, ncost3, ncost4, ncost5) VALUES (6060, 0, 0, 0, 20000, 15000);
INSERT INTO rm_control_cost (fkcontrol, ncost1, ncost2, ncost3, ncost4, ncost5) VALUES (6042, 30000, 35000, 12500, 4000, 5000);


--
-- TOC entry 2741 (class 0 OID 4199559)
-- Dependencies: 1520
-- Data for Name: rm_control_efficiency_history; Type: TABLE DATA; Schema: public; Owner: isms
--



--
-- TOC entry 2742 (class 0 OID 4199569)
-- Dependencies: 1521
-- Data for Name: rm_control_test_history; Type: TABLE DATA; Schema: public; Owner: isms
--



--
-- TOC entry 2743 (class 0 OID 4199580)
-- Dependencies: 1522
-- Data for Name: rm_event; Type: TABLE DATA; Schema: public; Owner: isms
--

COPY rm_event (fkcontext, fktype, fkcategory, sdescription, tobservation, bpropagate, timpact) FROM '/tmp/trialdata/rm_event.sql' DELIMITERS '#' NULL 'NULL';


--
-- TOC entry 2744 (class 0 OID 4199594)
-- Dependencies: 1524
-- Data for Name: rm_parameter_name; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO rm_parameter_name (pkparametername, sname, nweight) VALUES (1, 'Availability', 1);
INSERT INTO rm_parameter_name (pkparametername, sname, nweight) VALUES (2, 'Confidentiality', 1);
INSERT INTO rm_parameter_name (pkparametername, sname, nweight) VALUES (3, 'Integrity', 1);


--
-- TOC entry 2745 (class 0 OID 4199603)
-- Dependencies: 1526
-- Data for Name: rm_parameter_value_name; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO rm_parameter_value_name (pkvaluename, nvalue, simportance, simpact, sriskprobability) VALUES (1, 1, 'Negligible', 'Very low', 'Rare (1% - 20%)');
INSERT INTO rm_parameter_value_name (pkvaluename, nvalue, simportance, simpact, sriskprobability) VALUES (2, 2, 'Low', 'Low', 'Unlikely (21% - 40%)');
INSERT INTO rm_parameter_value_name (pkvaluename, nvalue, simportance, simpact, sriskprobability) VALUES (3, 3, 'Medium', 'Medium', 'Moderate (41% - 60%)');
INSERT INTO rm_parameter_value_name (pkvaluename, nvalue, simportance, simpact, sriskprobability) VALUES (4, 4, 'High', 'High', 'Likely (61% - 80%)');
INSERT INTO rm_parameter_value_name (pkvaluename, nvalue, simportance, simpact, sriskprobability) VALUES (5, 5, 'Critical', 'Very high', 'Almost certain (81% - 99%)');


--
-- TOC entry 2746 (class 0 OID 4199611)
-- Dependencies: 1527
-- Data for Name: rm_process; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO rm_process (fkcontext, fkpriority, fktype, fkarea, fkresponsible, sname, tdescription, sdocument, nvalue, tjustification) VALUES (6020, 16, 8, 6014, 13, 'Warranty Control', '', NULL, 8, NULL);
INSERT INTO rm_process (fkcontext, fkpriority, fktype, fkarea, fkresponsible, sname, tdescription, sdocument, nvalue, tjustification) VALUES (6018, 16, 8, 6014, 6007, 'Contracts Management', '', NULL, 8, NULL);
INSERT INTO rm_process (fkcontext, fkpriority, fktype, fkarea, fkresponsible, sname, tdescription, sdocument, nvalue, tjustification) VALUES (6028, 16, 9, 6016, 6006, 'Market Test', '', NULL, 8, NULL);
INSERT INTO rm_process (fkcontext, fkpriority, fktype, fkarea, fkresponsible, sname, tdescription, sdocument, nvalue, tjustification) VALUES (6026, 16, 9, 6016, 6006, 'New Product Development', '', NULL, 8, NULL);
INSERT INTO rm_process (fkcontext, fkpriority, fktype, fkarea, fkresponsible, sname, tdescription, sdocument, nvalue, tjustification) VALUES (6022, 16, 12, 6012, 6004, 'Network Administration', '', NULL, 8, NULL);
INSERT INTO rm_process (fkcontext, fkpriority, fktype, fkarea, fkresponsible, sname, tdescription, sdocument, nvalue, tjustification) VALUES (6024, 16, 12, 6012, 6004, 'System Development and Maintenance', '', NULL, 8, NULL);


--
-- TOC entry 2747 (class 0 OID 4199624)
-- Dependencies: 1528
-- Data for Name: rm_process_asset; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO rm_process_asset (fkcontext, fkprocess, fkasset) VALUES (6141, 6020, 6034);
INSERT INTO rm_process_asset (fkcontext, fkprocess, fkasset) VALUES (6142, 6020, 6036);
INSERT INTO rm_process_asset (fkcontext, fkprocess, fkasset) VALUES (6143, 6020, 6030);
INSERT INTO rm_process_asset (fkcontext, fkprocess, fkasset) VALUES (6144, 6020, 6038);
INSERT INTO rm_process_asset (fkcontext, fkprocess, fkasset) VALUES (6146, 6018, 6034);
INSERT INTO rm_process_asset (fkcontext, fkprocess, fkasset) VALUES (6147, 6018, 6036);
INSERT INTO rm_process_asset (fkcontext, fkprocess, fkasset) VALUES (6148, 6018, 6030);
INSERT INTO rm_process_asset (fkcontext, fkprocess, fkasset) VALUES (6151, 6028, 6034);
INSERT INTO rm_process_asset (fkcontext, fkprocess, fkasset) VALUES (6152, 6028, 6036);
INSERT INTO rm_process_asset (fkcontext, fkprocess, fkasset) VALUES (6153, 6028, 6030);
INSERT INTO rm_process_asset (fkcontext, fkprocess, fkasset) VALUES (6154, 6028, 6038);
INSERT INTO rm_process_asset (fkcontext, fkprocess, fkasset) VALUES (6156, 6028, 6040);
INSERT INTO rm_process_asset (fkcontext, fkprocess, fkasset) VALUES (6157, 6026, 6034);
INSERT INTO rm_process_asset (fkcontext, fkprocess, fkasset) VALUES (6158, 6026, 6030);
INSERT INTO rm_process_asset (fkcontext, fkprocess, fkasset) VALUES (6159, 6026, 6038);
INSERT INTO rm_process_asset (fkcontext, fkprocess, fkasset) VALUES (6161, 6026, 6040);
INSERT INTO rm_process_asset (fkcontext, fkprocess, fkasset) VALUES (6162, 6022, 6030);
INSERT INTO rm_process_asset (fkcontext, fkprocess, fkasset) VALUES (6164, 6024, 6030);


--
-- TOC entry 2748 (class 0 OID 4199633)
-- Dependencies: 1530
-- Data for Name: rm_rc_parameter_value_name; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO rm_rc_parameter_value_name (pkrcvaluename, nvalue, srcprobability, scontrolimpact) VALUES (1, 0, 'Do not affect', 'Do not affect');
INSERT INTO rm_rc_parameter_value_name (pkrcvaluename, nvalue, srcprobability, scontrolimpact) VALUES (2, 1, 'One Level Decrease', 'One Level Decrease');
INSERT INTO rm_rc_parameter_value_name (pkrcvaluename, nvalue, srcprobability, scontrolimpact) VALUES (3, 2, 'Two Level Decrease', 'Two Level Decrease');
INSERT INTO rm_rc_parameter_value_name (pkrcvaluename, nvalue, srcprobability, scontrolimpact) VALUES (4, 3, 'Three Level Decrease', 'Three Level Decrease');
INSERT INTO rm_rc_parameter_value_name (pkrcvaluename, nvalue, srcprobability, scontrolimpact) VALUES (5, 4, 'Four Level Decrease', 'Four Level Decrease');


--
-- TOC entry 2749 (class 0 OID 4199640)
-- Dependencies: 1531
-- Data for Name: rm_risk; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO rm_risk (fkcontext, fkprobabilityvaluename, fktype, fkevent, fkasset, sname, tdescription, nvalue, nvalueresidual, tjustification, nacceptmode, nacceptstate, sacceptjustification, ncost, timpact) VALUES (6093, 2, 20, 6067, 6038, 'Loss of critical information.', '', 5.666666666666667, 5.666666666666667, '', 0, 0, NULL, 300000, '');
INSERT INTO rm_risk (fkcontext, fkprobabilityvaluename, fktype, fkevent, fkasset, sname, tdescription, nvalue, nvalueresidual, tjustification, nacceptmode, nacceptstate, sacceptjustification, ncost, timpact) VALUES (6103, 2, 20, 440, 6040, 'Manipulation of information in a manner inconsistent with their value and importance.', '', 5.666666666666667, 5.666666666666667, '', 0, 0, NULL, 4500000, '');
INSERT INTO rm_risk (fkcontext, fkprobabilityvaluename, fktype, fkevent, fkasset, sname, tdescription, nvalue, nvalueresidual, tjustification, nacceptmode, nacceptstate, sacceptjustification, ncost, timpact) VALUES (6104, 2, 20, 1513, 6040, 'Insufficient retention periods for important record.', '', 5.666666666666667, 5.666666666666667, '', 0, 0, NULL, 0, '');
INSERT INTO rm_risk (fkcontext, fkprobabilityvaluename, fktype, fkevent, fkasset, sname, tdescription, nvalue, nvalueresidual, tjustification, nacceptmode, nacceptstate, sacceptjustification, ncost, timpact) VALUES (6087, 4, 20, 674, 6030, 'Insufficient system performance.', '', 13.333333333333334, 3.3333333333333335, '', 0, 0, NULL, 700000, '');
INSERT INTO rm_risk (fkcontext, fkprobabilityvaluename, fktype, fkevent, fkasset, sname, tdescription, nvalue, nvalueresidual, tjustification, nacceptmode, nacceptstate, sacceptjustification, ncost, timpact) VALUES (6080, 4, 21, 377, 6036, 'Compromising of information caused by clients unauthorized access.', '', 11.333333333333334, 5.666666666666667, '', 0, 0, NULL, 50000, '');
INSERT INTO rm_risk (fkcontext, fkprobabilityvaluename, fktype, fkevent, fkasset, sname, tdescription, nvalue, nvalueresidual, tjustification, nacceptmode, nacceptstate, sacceptjustification, ncost, timpact) VALUES (6101, 4, 20, 387, 6040, 'Unauthorized access by the third parties.', '', 15.333333333333334, 7.333333333333333, '', 0, 0, NULL, 1500000, '');
INSERT INTO rm_risk (fkcontext, fkprobabilityvaluename, fktype, fkevent, fkasset, sname, tdescription, nvalue, nvalueresidual, tjustification, nacceptmode, nacceptstate, sacceptjustification, ncost, timpact) VALUES (6081, 5, 21, 342, 6036, 'Unauthorized disclosure of sensitive information during the contracting period.', '', 18.333333333333332, 7, '', 0, 0, NULL, 700000, 'Clients and contracts information disclosure');
INSERT INTO rm_risk (fkcontext, fkprobabilityvaluename, fktype, fkevent, fkasset, sname, tdescription, nvalue, nvalueresidual, tjustification, nacceptmode, nacceptstate, sacceptjustification, ncost, timpact) VALUES (6094, 5, 21, 342, 6038, 'Unauthorized disclosure of sensitive information during the contracting period.', '', 18.333333333333332, 7, '', 0, 0, NULL, 1000000, '');
INSERT INTO rm_risk (fkcontext, fkprobabilityvaluename, fktype, fkevent, fkasset, sname, tdescription, nvalue, nvalueresidual, tjustification, nacceptmode, nacceptstate, sacceptjustification, ncost, timpact) VALUES (6091, 5, 21, 346, 6038, 'Unauthorized information disclosure by the suppliers.', '', 16.666666666666668, 6.333333333333333, '', 0, 0, NULL, 1000000, '');
INSERT INTO rm_risk (fkcontext, fkprobabilityvaluename, fktype, fkevent, fkasset, sname, tdescription, nvalue, nvalueresidual, tjustification, nacceptmode, nacceptstate, sacceptjustification, ncost, timpact) VALUES (6102, 4, 20, 377, 6040, 'Compromising of information caused by clients unauthorized access.', '', 13.333333333333334, 13.333333333333334, '', 72601, 0, 'Client access is necessary, wo we will accept the risk.', 1500000, '');
INSERT INTO rm_risk (fkcontext, fkprobabilityvaluename, fktype, fkevent, fkasset, sname, tdescription, nvalue, nvalueresidual, tjustification, nacceptmode, nacceptstate, sacceptjustification, ncost, timpact) VALUES (6092, 1, 21, 1510, 6038, 'Counterfeit of important records.', '', 2.8333333333333335, 2.8333333333333335, '', 0, 0, NULL, 300000, '');
INSERT INTO rm_risk (fkcontext, fkprobabilityvaluename, fktype, fkevent, fkasset, sname, tdescription, nvalue, nvalueresidual, tjustification, nacceptmode, nacceptstate, sacceptjustification, ncost, timpact) VALUES (6083, 1, 21, 1474, 6036, 'Critical business processes interruption due to unavailability of the asset.', '', 3.1666666666666665, 3.1666666666666665, '', 0, 0, NULL, 200000, '');
INSERT INTO rm_risk (fkcontext, fkprobabilityvaluename, fktype, fkevent, fkasset, sname, tdescription, nvalue, nvalueresidual, tjustification, nacceptmode, nacceptstate, sacceptjustification, ncost, timpact) VALUES (6077, 4, 21, NULL, 6034, 'Assign privileges to users that do not need this profile.', '', 18, 4.5, '', 0, 0, NULL, 450000, '');
INSERT INTO rm_risk (fkcontext, fkprobabilityvaluename, fktype, fkevent, fkasset, sname, tdescription, nvalue, nvalueresidual, tjustification, nacceptmode, nacceptstate, sacceptjustification, ncost, timpact) VALUES (6086, 5, 20, NULL, 6030, 'Contamination by virus.', '', 20, 8, '', 0, 0, NULL, 1000000, '');
INSERT INTO rm_risk (fkcontext, fkprobabilityvaluename, fktype, fkevent, fkasset, sname, tdescription, nvalue, nvalueresidual, tjustification, nacceptmode, nacceptstate, sacceptjustification, ncost, timpact) VALUES (6088, 1, 21, NULL, 6030, 'Damage to the internal network through incidents in networks of external parties that are connected to the internal network.', '', 3, 3, '', 0, 0, NULL, 500000, '');
INSERT INTO rm_risk (fkcontext, fkprobabilityvaluename, fktype, fkevent, fkasset, sname, tdescription, nvalue, nvalueresidual, tjustification, nacceptmode, nacceptstate, sacceptjustification, ncost, timpact) VALUES (6085, 4, 21, NULL, 6030, 'Granting inappropriate access rights to network services.', '', 14, 3.5, '', 0, 0, NULL, 450000, '');
INSERT INTO rm_risk (fkcontext, fkprobabilityvaluename, fktype, fkevent, fkasset, sname, tdescription, nvalue, nvalueresidual, tjustification, nacceptmode, nacceptstate, sacceptjustification, ncost, timpact) VALUES (6075, 4, 21, NULL, 6034, 'Inadequate grant of access rights.', '', 18, 4.5, '', 0, 0, NULL, 1000000, 'Unauthorized access to the systema');
INSERT INTO rm_risk (fkcontext, fkprobabilityvaluename, fktype, fkevent, fkasset, sname, tdescription, nvalue, nvalueresidual, tjustification, nacceptmode, nacceptstate, sacceptjustification, ncost, timpact) VALUES (6084, 1, 21, NULL, 6036, 'Inability to recover important records due to storage in proprietary files format and loss of recovery softwares.', '', 3.3333333333333335, 3.3333333333333335, '', 0, 0, NULL, 300000, '');
INSERT INTO rm_risk (fkcontext, fkprobabilityvaluename, fktype, fkevent, fkasset, sname, tdescription, nvalue, nvalueresidual, tjustification, nacceptmode, nacceptstate, sacceptjustification, ncost, timpact) VALUES (6076, 4, NULL, NULL, 6034, 'Insufficient system performance.', '', 15.333333333333334, 3.8333333333333335, '', 0, 0, NULL, 300000, 'Businnes compromising');
INSERT INTO rm_risk (fkcontext, fkprobabilityvaluename, fktype, fkevent, fkasset, sname, tdescription, nvalue, nvalueresidual, tjustification, nacceptmode, nacceptstate, sacceptjustification, ncost, timpact) VALUES (6089, 2, 20, NULL, 6030, 'Internal denial-of-service attack.', '', 6, 6, '', 0, 0, NULL, 0, '');
INSERT INTO rm_risk (fkcontext, fkprobabilityvaluename, fktype, fkevent, fkasset, sname, tdescription, nvalue, nvalueresidual, tjustification, nacceptmode, nacceptstate, sacceptjustification, ncost, timpact) VALUES (6090, 4, 21, NULL, 6038, 'Unauthorized access to sensitive information left on desks.', '', 14, 7, '', 0, 0, NULL, 1000000, '');
INSERT INTO rm_risk (fkcontext, fkprobabilityvaluename, fktype, fkevent, fkasset, sname, tdescription, nvalue, nvalueresidual, tjustification, nacceptmode, nacceptstate, sacceptjustification, ncost, timpact) VALUES (6100, 4, 19, NULL, 6040, 'Unauthorized access to sensitive information left on desks.', '', 13.333333333333334, 6.666666666666667, '', 0, 0, NULL, 1500000, '');
INSERT INTO rm_risk (fkcontext, fkprobabilityvaluename, fktype, fkevent, fkasset, sname, tdescription, nvalue, nvalueresidual, tjustification, nacceptmode, nacceptstate, sacceptjustification, ncost, timpact) VALUES (6082, 4, 21, NULL, 6036, 'Unauthorized access to sensitive information left on desks.', '', 15.333333333333334, 7.666666666666667, '', 0, 0, NULL, 500000, 'Clients and contracts information disclosure or thief');


--
-- TOC entry 2750 (class 0 OID 4199657)
-- Dependencies: 1532
-- Data for Name: rm_risk_control; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO rm_risk_control (fkcontext, fkprobabilityvaluename, fkrisk, fkcontrol, tjustification) VALUES (6106, 4, 6087, 6049, NULL);
INSERT INTO rm_risk_control (fkcontext, fkprobabilityvaluename, fkrisk, fkcontrol, tjustification) VALUES (6107, 4, 6076, 6049, NULL);
INSERT INTO rm_risk_control (fkcontext, fkprobabilityvaluename, fkrisk, fkcontrol, tjustification) VALUES (6108, 3, 6082, 6060, NULL);
INSERT INTO rm_risk_control (fkcontext, fkprobabilityvaluename, fkrisk, fkcontrol, tjustification) VALUES (6109, 3, 6100, 6060, NULL);
INSERT INTO rm_risk_control (fkcontext, fkprobabilityvaluename, fkrisk, fkcontrol, tjustification) VALUES (6110, 3, 6090, 6060, NULL);
INSERT INTO rm_risk_control (fkcontext, fkprobabilityvaluename, fkrisk, fkcontrol, tjustification) VALUES (6111, 4, 6086, 6042, NULL);
INSERT INTO rm_risk_control (fkcontext, fkprobabilityvaluename, fkrisk, fkcontrol, tjustification) VALUES (6113, 3, 6080, 6046, NULL);
INSERT INTO rm_risk_control (fkcontext, fkprobabilityvaluename, fkrisk, fkcontrol, tjustification) VALUES (6116, 3, 6101, 6046, NULL);
INSERT INTO rm_risk_control (fkcontext, fkprobabilityvaluename, fkrisk, fkcontrol, tjustification) VALUES (6114, 4, 6081, 6046, '');
INSERT INTO rm_risk_control (fkcontext, fkprobabilityvaluename, fkrisk, fkcontrol, tjustification) VALUES (6117, 4, 6094, 6046, '');
INSERT INTO rm_risk_control (fkcontext, fkprobabilityvaluename, fkrisk, fkcontrol, tjustification) VALUES (6115, 4, 6091, 6046, '');
INSERT INTO rm_risk_control (fkcontext, fkprobabilityvaluename, fkrisk, fkcontrol, tjustification) VALUES (6118, 4, 6077, 6055, NULL);
INSERT INTO rm_risk_control (fkcontext, fkprobabilityvaluename, fkrisk, fkcontrol, tjustification) VALUES (6120, 4, 6085, 6055, NULL);
INSERT INTO rm_risk_control (fkcontext, fkprobabilityvaluename, fkrisk, fkcontrol, tjustification) VALUES (6121, 4, 6075, 6055, NULL);


--
-- TOC entry 2751 (class 0 OID 4199668)
-- Dependencies: 1533
-- Data for Name: rm_risk_control_value; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO rm_risk_control_value (fkriskcontrol, fkparametername, fkrcvaluename, tjustification) VALUES (6106, 1, 1, '');
INSERT INTO rm_risk_control_value (fkriskcontrol, fkparametername, fkrcvaluename, tjustification) VALUES (6106, 2, 1, '');
INSERT INTO rm_risk_control_value (fkriskcontrol, fkparametername, fkrcvaluename, tjustification) VALUES (6106, 3, 1, '');
INSERT INTO rm_risk_control_value (fkriskcontrol, fkparametername, fkrcvaluename, tjustification) VALUES (6107, 1, 1, '');
INSERT INTO rm_risk_control_value (fkriskcontrol, fkparametername, fkrcvaluename, tjustification) VALUES (6107, 2, 1, '');
INSERT INTO rm_risk_control_value (fkriskcontrol, fkparametername, fkrcvaluename, tjustification) VALUES (6107, 3, 1, '');
INSERT INTO rm_risk_control_value (fkriskcontrol, fkparametername, fkrcvaluename, tjustification) VALUES (6108, 1, 1, '');
INSERT INTO rm_risk_control_value (fkriskcontrol, fkparametername, fkrcvaluename, tjustification) VALUES (6108, 2, 1, '');
INSERT INTO rm_risk_control_value (fkriskcontrol, fkparametername, fkrcvaluename, tjustification) VALUES (6108, 3, 1, '');
INSERT INTO rm_risk_control_value (fkriskcontrol, fkparametername, fkrcvaluename, tjustification) VALUES (6109, 1, 1, '');
INSERT INTO rm_risk_control_value (fkriskcontrol, fkparametername, fkrcvaluename, tjustification) VALUES (6109, 2, 1, '');
INSERT INTO rm_risk_control_value (fkriskcontrol, fkparametername, fkrcvaluename, tjustification) VALUES (6109, 3, 1, '');
INSERT INTO rm_risk_control_value (fkriskcontrol, fkparametername, fkrcvaluename, tjustification) VALUES (6110, 1, 1, '');
INSERT INTO rm_risk_control_value (fkriskcontrol, fkparametername, fkrcvaluename, tjustification) VALUES (6110, 2, 1, '');
INSERT INTO rm_risk_control_value (fkriskcontrol, fkparametername, fkrcvaluename, tjustification) VALUES (6110, 3, 1, '');
INSERT INTO rm_risk_control_value (fkriskcontrol, fkparametername, fkrcvaluename, tjustification) VALUES (6111, 1, 1, '');
INSERT INTO rm_risk_control_value (fkriskcontrol, fkparametername, fkrcvaluename, tjustification) VALUES (6111, 2, 1, '');
INSERT INTO rm_risk_control_value (fkriskcontrol, fkparametername, fkrcvaluename, tjustification) VALUES (6111, 3, 1, '');
INSERT INTO rm_risk_control_value (fkriskcontrol, fkparametername, fkrcvaluename, tjustification) VALUES (6113, 1, 1, '');
INSERT INTO rm_risk_control_value (fkriskcontrol, fkparametername, fkrcvaluename, tjustification) VALUES (6113, 2, 2, '');
INSERT INTO rm_risk_control_value (fkriskcontrol, fkparametername, fkrcvaluename, tjustification) VALUES (6113, 3, 1, '');
INSERT INTO rm_risk_control_value (fkriskcontrol, fkparametername, fkrcvaluename, tjustification) VALUES (6116, 1, 1, '');
INSERT INTO rm_risk_control_value (fkriskcontrol, fkparametername, fkrcvaluename, tjustification) VALUES (6116, 2, 2, '');
INSERT INTO rm_risk_control_value (fkriskcontrol, fkparametername, fkrcvaluename, tjustification) VALUES (6116, 3, 1, '');
INSERT INTO rm_risk_control_value (fkriskcontrol, fkparametername, fkrcvaluename, tjustification) VALUES (6114, 1, 1, '');
INSERT INTO rm_risk_control_value (fkriskcontrol, fkparametername, fkrcvaluename, tjustification) VALUES (6114, 2, 2, '');
INSERT INTO rm_risk_control_value (fkriskcontrol, fkparametername, fkrcvaluename, tjustification) VALUES (6114, 3, 1, '');
INSERT INTO rm_risk_control_value (fkriskcontrol, fkparametername, fkrcvaluename, tjustification) VALUES (6117, 1, 1, '');
INSERT INTO rm_risk_control_value (fkriskcontrol, fkparametername, fkrcvaluename, tjustification) VALUES (6117, 2, 2, '');
INSERT INTO rm_risk_control_value (fkriskcontrol, fkparametername, fkrcvaluename, tjustification) VALUES (6117, 3, 1, '');
INSERT INTO rm_risk_control_value (fkriskcontrol, fkparametername, fkrcvaluename, tjustification) VALUES (6115, 1, 1, '');
INSERT INTO rm_risk_control_value (fkriskcontrol, fkparametername, fkrcvaluename, tjustification) VALUES (6115, 2, 2, '');
INSERT INTO rm_risk_control_value (fkriskcontrol, fkparametername, fkrcvaluename, tjustification) VALUES (6115, 3, 1, '');
INSERT INTO rm_risk_control_value (fkriskcontrol, fkparametername, fkrcvaluename, tjustification) VALUES (6118, 1, 1, '');
INSERT INTO rm_risk_control_value (fkriskcontrol, fkparametername, fkrcvaluename, tjustification) VALUES (6118, 2, 1, '');
INSERT INTO rm_risk_control_value (fkriskcontrol, fkparametername, fkrcvaluename, tjustification) VALUES (6118, 3, 1, '');
INSERT INTO rm_risk_control_value (fkriskcontrol, fkparametername, fkrcvaluename, tjustification) VALUES (6120, 1, 1, '');
INSERT INTO rm_risk_control_value (fkriskcontrol, fkparametername, fkrcvaluename, tjustification) VALUES (6120, 2, 1, '');
INSERT INTO rm_risk_control_value (fkriskcontrol, fkparametername, fkrcvaluename, tjustification) VALUES (6120, 3, 1, '');
INSERT INTO rm_risk_control_value (fkriskcontrol, fkparametername, fkrcvaluename, tjustification) VALUES (6121, 1, 1, '');
INSERT INTO rm_risk_control_value (fkriskcontrol, fkparametername, fkrcvaluename, tjustification) VALUES (6121, 2, 1, '');
INSERT INTO rm_risk_control_value (fkriskcontrol, fkparametername, fkrcvaluename, tjustification) VALUES (6121, 3, 1, '');


--
-- TOC entry 2752 (class 0 OID 4199679)
-- Dependencies: 1534
-- Data for Name: rm_risk_limits; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO rm_risk_limits (fkcontext, nlow, nhigh, nmidlow, nmidhigh) VALUES (14, 9, 18, 0, 0);


--
-- TOC entry 2753 (class 0 OID 4199684)
-- Dependencies: 1535
-- Data for Name: rm_risk_value; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6075, 4, 1, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6075, 5, 2, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6075, 3, 3, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6076, 5, 1, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6076, 1, 2, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6076, 2, 3, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6077, 4, 1, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6077, 4, 2, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6077, 4, 3, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6080, 2, 1, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6080, 1, 2, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6080, 1, 3, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6082, 4, 1, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6082, 5, 2, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6082, 1, 3, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6081, 1, 1, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6081, 5, 2, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6081, 3, 3, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6084, 5, 1, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6084, 1, 2, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6084, 1, 3, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6085, 1, 1, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6085, 5, 2, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6085, 3, 3, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6086, 5, 1, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6086, 3, 2, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6086, 4, 3, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6087, 4, 1, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6087, 1, 2, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6087, 3, 3, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6088, 4, 1, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6088, 1, 2, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6088, 1, 3, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6089, 4, 1, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6089, 1, 2, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6089, 1, 3, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6090, 4, 1, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6090, 5, 2, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6090, 1, 3, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6091, 2, 1, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6091, 5, 2, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6091, 2, 3, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6093, 4, 1, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6093, 1, 2, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6093, 1, 3, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6094, 2, 1, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6094, 5, 2, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6094, 4, 3, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6100, 3, 1, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6100, 5, 2, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6100, 1, 3, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6101, 2, 1, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6101, 5, 2, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6101, 5, 3, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6103, 2, 1, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6103, 2, 2, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6103, 2, 3, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6104, 2, 1, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6104, 2, 2, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6104, 2, 3, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6102, 2, 1, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6102, 5, 2, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6102, 2, 3, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6092, 1, 1, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6092, 1, 2, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6092, 4, 3, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6083, 4, 1, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6083, 1, 2, '');
INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (6083, 1, 3, '');


--
-- TOC entry 2754 (class 0 OID 4199695)
-- Dependencies: 1536
-- Data for Name: rm_section_best_practice; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1600, NULL, 'ISO 27002 ');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1601, 1600, '05 Security policy');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1602, 1601, '5.1 Information security policy');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1603, 1600, '06 Organization of information security');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1604, 1603, '6.1 Internal organization');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1605, 1603, '6.2 External parties');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1606, 1600, '07 Asset management');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1607, 1606, '7.1 Responsibility for assets');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1608, 1606, '7.2 Information classification');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1609, 1600, '08 Human resources security');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1610, 1609, '8.1 Prior to employment');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1611, 1609, '8.2 During employment');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1612, 1609, '8.3 Termination or change of employment');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1613, 1600, '09 Physical and environmental security');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1614, 1613, '9.1 Secure areas');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1615, 1613, '9.2 Equipment security');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1616, 1600, '10 Communications and operations management');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1617, 1616, '10.1Operational procedures and responsibilities');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1618, 1616, '10.2 Third party service delivery management');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1619, 1616, '10.3 System planning and acceptance');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1620, 1616, '10.4 Protection against malicious and mobile code');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1621, 1616, '10.5 Back-up');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1622, 1616, '10.6 Network security management');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1623, 1616, '10.7 Media handling');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1624, 1616, '10.8 Exchange of information');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1625, 1616, '10.9 Electronic commerce services');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1626, 1616, '10.10 Monitoring');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1627, 1600, '11 Access control');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1628, 1627, '11.1 Business requirement for access control');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1629, 1627, '11.2 User Access management');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1630, 1627, '11.3 User responsibilities');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1631, 1627, '11.4 Network access control');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1632, 1627, '11.5 Operating system access control');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1633, 1627, '11.6 Application and information access control');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1634, 1627, '11.7 Mobile computing and teleworking');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1635, 1600, '12 Information systems acquisition, development and maintenance');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1636, 1635, '12.1 Security requirements of information systems');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1637, 1635, '12.2 Correct processing in applications');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1638, 1635, '12.3 Cryptographic controls');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1639, 1635, '12.4 Security of system files');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1640, 1635, '12.5 Security in development and support processes');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1641, 1635, '12.6 Technical vulnerability management');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1642, 1600, '13 Information security incident management');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1643, 1642, '13.1 Reporting information security events and weaknesses');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1644, 1642, '13.2 Management of information security incidents and improvements');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1645, 1600, '14 Business continuity management');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1646, 1645, '14.1 Information security aspects of business continuity management');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1647, 1600, '15 Compliance');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1648, 1647, '15.1 Compliance with legal requirements');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1649, 1647, '15.2 Compliance with security polices and standards, and technical compliance');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1650, 1647, '15.3 Information systems audit considerations');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1651, NULL, 'CobiT');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1652, 1651, 'Plan and Organise');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1653, 1652, 'PO1Define a Strategic IT Plan');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1654, 1652, 'PO2Define the Information Architecture');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1655, 1652, 'PO3 Determine Technological Direction');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1656, 1652, 'PO4 Define the IT Processes, Organisation and Relationships');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1657, 1652, 'PO5 Manage the IT Investment');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1658, 1652, 'PO6 Communicate Management Aims and Direction');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1659, 1652, 'PO7 Manage IT Human Resources');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1660, 1652, 'PO8 Manage Quality');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1661, 1652, 'PO9 Assess and Manage IT Risks');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1662, 1652, 'PO10 Manage Projects');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1663, 1651, 'Acquire and Implement');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1664, 1663, 'AI1 Identify Automated Solutions');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1665, 1663, 'AI2 Acquire and Maintain Application Software');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1666, 1663, 'AI3 Acquire and Maintain Technology Infrastructure');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1667, 1663, 'AI4 Enable Operation and Use');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1668, 1663, 'AI5 Procure IT Resources');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1669, 1663, 'AI6 Manage Changes');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1670, 1663, 'AI7 Install and Accredit Solutions and Changes');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1671, 1651, 'Deliver and Support');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1672, 1671, 'DS1 Define and Manage Service Levels');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1673, 1671, 'DS2 Manage Third-party Services');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1674, 1671, 'DS3 Manage Performance and Capacity');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1675, 1671, 'DS4 Ensure Continuous Service');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1676, 1671, 'DS5 Ensure Systems Security');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1677, 1671, 'DS6 Identify and Allocate Costs');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1678, 1671, 'DS7 Educate and Train Users');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1679, 1671, 'DS8 Manage Service Desk and Incidents');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1680, 1671, 'DS9 Manage the Configuration');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1681, 1671, 'DS10 Manage Problems');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1682, 1671, 'DS11 Manage Data');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1683, 1671, 'DS12 Manage the Physical Environment');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1684, 1671, 'DS13 Manage Operations');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1685, 1651, 'Monitor and Evaluate');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1686, 1685, 'ME1 Monitor and Evaluate IT Performance');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1687, 1685, 'ME2 Monitor and Evaluate Internal Control');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1688, 1685, 'ME3 Ensure Compliance With External Requirements');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1689, 1685, 'ME4 Provide IT Governance');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1690, NULL, 'PCI');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1691, 1690, 'Build and Maintain a Secure Network');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1692, 1691, '1 Install and maintain a firewall configuration to protect cardholder data');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1693, 1691, '2 Do not use vendor-supplied defaults for system passwords and other security parameters.');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1694, 1690, 'Protect Cardholder Data');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1695, 1694, '3 Protect stored cardholder data');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1696, 1694, '4 Encrypt transmission of cardholder data across open, public networks');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1697, 1690, 'Maintain a Vulnerability Management Program');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1698, 1697, '5 Use and regularly update anti-virus software or programs');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1699, 1697, '6 Develop and maintain secure systems and applications');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1700, 1690, 'Implement Strong Access Control Measures');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1701, 1700, '7 Restrict access to cardholder data by business need-to-know');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1702, 1700, '8 Assign a unique ID to each person with computer access');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1703, 1700, '9 Restrict physical access to cardholder data');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1704, 1690, 'Regularly Monitor and Test Networks ');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1705, 1704, '10 Track and monitor all access to network resources and cardholder data');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1706, 1704, '11 Regularly test security systems and processes');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1707, 1690, 'Maintain an Information Security Policy');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (1708, 1707, '12 Maintain a policy that addresses information security for employees and contractors');


--
-- TOC entry 2755 (class 0 OID 4199701)
-- Dependencies: 1537
-- Data for Name: rm_standard; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO rm_standard (fkcontext, sname, tdescription, tapplication, tobjective) VALUES (2400, 'ISO 27002', '', '', '');
INSERT INTO rm_standard (fkcontext, sname, tdescription, tapplication, tobjective) VALUES (2401, 'COBIT', '', '', '');
INSERT INTO rm_standard (fkcontext, sname, tdescription, tapplication, tobjective) VALUES (2403, 'PCI', '', '', '');


--
-- TOC entry 2756 (class 0 OID 4199711)
-- Dependencies: 1539
-- Data for Name: wkf_alert; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (2, 6010, 6000, 13, '', 9204, 1, '2008-04-08 15:16:39');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (3, 6012, 6004, 13, '', 9204, 1, '2008-04-08 15:19:03');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (4, 6014, 6007, 13, '', 9204, 1, '2008-04-08 15:21:33');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (5, 6016, 6006, 13, '', 9204, 1, '2008-04-08 15:24:08');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (6, 6018, 6007, 13, '', 9204, 1, '2008-04-08 15:28:22');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (7, 6022, 6004, 13, '', 9204, 1, '2008-04-08 15:48:23');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (8, 6024, 6004, 13, '', 9204, 1, '2008-04-08 15:49:43');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (9, 6026, 6006, 13, '', 9204, 1, '2008-04-08 15:54:14');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (10, 6028, 6006, 13, '', 9204, 1, '2008-04-08 15:57:23');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (11, 6030, 6004, 13, '', 9204, 1, '2008-04-08 16:06:07');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (13, 6034, 6004, 13, '', 9204, 1, '2008-04-08 16:10:49');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (14, 6036, 6007, 13, '', 9204, 1, '2008-04-08 16:14:39');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (15, 6038, 6006, 13, '', 9204, 1, '2008-04-08 16:16:52');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (16, 6040, 6006, 13, '', 9204, 1, '2008-04-08 16:18:30');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (17, 6042, 6004, 13, '', 9204, 1, '2008-04-08 16:24:39');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (18, 6049, 6004, 13, '', 9204, 1, '2008-04-08 16:36:29');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (19, 6055, 6002, 13, '', 9204, 1, '2008-04-08 16:39:30');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (20, 6060, 6005, 13, '', 9204, 1, '2008-04-08 16:41:36');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (21, 6047, 13, 13, ' ', 9213, 1, '2008-04-09 12:44:54');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (22, 6050, 6004, 13, ' ', 9213, 1, '2008-04-09 12:44:55');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (23, 6050, 13, 13, ' ', 9213, 1, '2008-04-09 12:44:55');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (24, 6056, 6002, 13, ' ', 9213, 1, '2008-04-09 12:44:55');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (25, 6056, 13, 13, ' ', 9213, 1, '2008-04-09 12:44:55');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (26, 6061, 6005, 13, ' ', 9213, 1, '2008-04-09 12:44:55');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (27, 6061, 13, 13, ' ', 9213, 1, '2008-04-09 12:44:55');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (29, 6120, 13, 6004, '', 9201, 1, '2008-04-09 13:36:40');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (30, 6121, 13, 6004, '', 9201, 1, '2008-04-09 13:36:52');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (31, 6118, 13, 6004, '', 9201, 1, '2008-04-09 13:37:02');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (32, 6111, 13, 6004, '', 9201, 1, '2008-04-09 13:37:13');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (34, 6107, 13, 6004, '', 9201, 1, '2008-04-09 13:37:41');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (35, 6106, 13, 6004, '', 9201, 1, '2008-04-09 13:37:56');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (42, 6013, 6004, 13, ' ', 9213, 1, '2008-04-09 14:24:34');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (43, 6013, 13, 13, ' ', 9213, 1, '2008-04-09 14:24:34');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (44, 6050, 6004, 13, ' ', 9213, 1, '2008-04-09 14:24:34');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (45, 6050, 13, 13, ' ', 9213, 1, '2008-04-09 14:24:34');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (46, 6023, 6004, 13, ' ', 9213, 1, '2008-04-09 14:24:34');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (47, 6023, 13, 13, ' ', 9213, 1, '2008-04-09 14:24:34');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (48, 6025, 6004, 13, ' ', 9213, 1, '2008-04-09 14:24:34');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (49, 6025, 13, 13, ' ', 9213, 1, '2008-04-09 14:24:34');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (50, 6043, 6003, 13, ' ', 9213, 1, '2008-04-09 14:27:56');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (51, 6043, 13, 13, ' ', 9213, 1, '2008-04-09 14:27:56');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (52, 6047, 13, 13, ' ', 9213, 1, '2008-04-09 14:36:39');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (53, 6016, 6006, 6000, '', 9201, 1, '2008-04-09 14:38:58');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (54, 6014, 6007, 6000, '', 9201, 1, '2008-04-09 14:39:18');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (55, 6012, 6004, 6000, '', 9201, 1, '2008-04-09 14:40:38');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (56, 6089, 13, 6004, '', 9201, 1, '2008-04-09 14:49:43');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (57, 6088, 13, 6004, '', 9201, 1, '2008-04-09 14:49:57');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (58, 6087, 13, 6004, '', 9201, 1, '2008-04-09 14:50:09');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (59, 6086, 13, 6004, '', 9201, 1, '2008-04-09 14:51:36');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (60, 6085, 13, 6004, '', 9201, 1, '2008-04-09 14:51:50');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (63, 6077, 13, 6004, '', 9201, 1, '2008-04-09 14:55:27');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (64, 6076, 13, 6004, '', 9201, 1, '2008-04-09 14:55:53');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (65, 6075, 13, 6004, '', 9201, 1, '2008-04-09 14:57:16');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (66, 6017, 6006, 13, ' ', 9213, 1, '2008-04-09 14:58:38');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (67, 6017, 13, 13, ' ', 9213, 1, '2008-04-09 14:58:38');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (68, 6027, 6006, 13, ' ', 9213, 1, '2008-04-09 14:58:38');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (69, 6027, 13, 13, ' ', 9213, 1, '2008-04-09 14:58:38');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (70, 6117, 13, 6006, '', 9201, 1, '2008-04-09 15:00:05');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (71, 6115, 13, 6006, '', 9201, 1, '2008-04-09 15:00:34');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (72, 6116, 13, 6006, '', 9201, 1, '2008-04-09 15:01:14');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (73, 6109, 13, 6006, '', 9201, 1, '2008-04-09 15:01:39');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (74, 6110, 13, 6006, '', 9201, 1, '2008-04-09 15:02:40');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (75, 6104, 13, 6006, '', 9201, 1, '2008-04-09 15:03:50');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (76, 6103, 13, 6006, '', 9201, 1, '2008-04-09 15:04:04');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (77, 6101, 13, 6006, '', 9201, 1, '2008-04-09 15:04:49');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (78, 6100, 13, 6006, '', 9201, 1, '2008-04-09 15:05:52');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (79, 6094, 13, 6006, '', 9201, 1, '2008-04-09 15:06:29');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (80, 6093, 13, 6006, '', 9201, 1, '2008-04-09 15:06:48');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (81, 6092, 13, 6006, '', 9201, 1, '2008-04-09 15:07:09');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (82, 6091, 13, 6006, '', 9201, 1, '2008-04-09 15:07:43');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (83, 6090, 13, 6006, '', 9201, 1, '2008-04-09 15:08:46');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (84, 6015, 6007, 13, ' ', 9213, 1, '2008-04-09 15:11:45');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (85, 6015, 13, 13, ' ', 9213, 1, '2008-04-09 15:11:45');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (86, 6114, 13, 6007, '', 9201, 1, '2008-04-09 15:12:48');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (87, 6113, 13, 6007, '', 9201, 1, '2008-04-09 15:13:12');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (88, 6108, 13, 6007, '', 9201, 1, '2008-04-09 15:13:45');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (89, 6084, 13, 6007, '', 9201, 1, '2008-04-09 15:14:16');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (90, 6083, 13, 6007, '', 9201, 1, '2008-04-09 15:14:49');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (91, 6082, 13, 6007, '', 9201, 1, '2008-04-09 15:15:14');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (92, 6081, 13, 6007, '', 9201, 1, '2008-04-09 15:16:11');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (93, 6080, 13, 6007, '', 9201, 1, '2008-04-09 15:17:32');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (94, 6020, 13, 6007, '', 9201, 1, '2008-04-09 15:17:45');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (95, 6123, 6006, 13, ' ', 9213, 1, '2008-04-09 16:27:32');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (96, 6123, 6000, 13, ' ', 9213, 1, '2008-04-09 16:27:33');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (97, 6123, 13, 13, ' ', 9213, 1, '2008-04-09 16:27:33');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (98, 6125, 6006, 13, ' ', 9213, 1, '2008-04-09 16:27:33');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (99, 6061, 6005, 13, ' ', 9213, 1, '2008-04-09 16:27:33');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (100, 6061, 13, 13, ' ', 9213, 1, '2008-04-09 16:27:33');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (101, 6123, 6004, 13, 'approve', 9216, 1, '2008-04-09 16:46:52');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (102, 6123, 6006, 13, 'approve', 9216, 1, '2008-04-09 16:46:52');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (103, 6123, 6001, 13, 'approve', 9216, 1, '2008-04-09 16:46:52');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (104, 6123, 6002, 13, 'approve', 9216, 1, '2008-04-09 16:46:52');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (105, 6123, 6000, 13, 'approve', 9216, 1, '2008-04-09 16:46:52');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (106, 6123, 6007, 13, 'approve', 9216, 1, '2008-04-09 16:46:52');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (107, 6123, 6003, 13, 'approve', 9216, 1, '2008-04-09 16:46:52');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (108, 6123, 6005, 13, 'approve', 9216, 1, '2008-04-09 16:46:52');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (109, 6123, 13, 13, 'approve', 9216, 1, '2008-04-09 16:46:52');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (110, 6125, 6004, 6006, 'publish', 9216, 1, '2008-04-09 16:49:20');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (111, 6125, 6006, 6006, 'publish', 9216, 1, '2008-04-09 16:49:20');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (112, 6125, 6001, 6006, 'publish', 9216, 1, '2008-04-09 16:49:20');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (113, 6125, 6002, 6006, 'publish', 9216, 1, '2008-04-09 16:49:20');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (114, 6125, 6000, 6006, 'publish', 9216, 1, '2008-04-09 16:49:20');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (115, 6125, 6007, 6006, 'publish', 9216, 1, '2008-04-09 16:49:20');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (116, 6125, 6003, 6006, 'publish', 9216, 1, '2008-04-09 16:49:20');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (117, 6125, 6005, 6006, 'publish', 9216, 1, '2008-04-09 16:49:20');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (118, 6125, 13, 6006, 'publish', 9216, 1, '2008-04-09 16:49:20');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (119, 6141, 13, 13, '', 9205, 1, '2008-04-09 16:54:25');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (120, 6142, 13, 13, '', 9205, 1, '2008-04-09 16:54:25');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (121, 6143, 13, 13, '', 9205, 1, '2008-04-09 16:54:25');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (122, 6144, 13, 13, '', 9205, 1, '2008-04-09 16:54:25');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (124, 6146, 6007, 6007, '', 9205, 1, '2008-04-09 17:09:57');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (125, 6148, 6007, 6007, '', 9205, 1, '2008-04-09 17:09:58');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (128, 6151, 6006, 6006, '', 9205, 1, '2008-04-09 17:11:54');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (129, 6152, 6006, 6006, '', 9205, 1, '2008-04-09 17:11:54');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (130, 6153, 6006, 6006, '', 9205, 1, '2008-04-09 17:11:54');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (132, 6157, 6006, 6006, '', 9205, 1, '2008-04-09 17:12:34');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (133, 6158, 6006, 6006, '', 9205, 1, '2008-04-09 17:12:34');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (135, 6180, 6007, 13, '', 9201, 1, '2008-04-09 17:49:41');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (136, 6178, 6005, 13, '', 9201, 1, '2008-04-09 17:49:53');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (137, 6173, 6006, 13, '', 9201, 1, '2008-04-09 17:50:05');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (138, 6175, 6003, 13, '', 9201, 1, '2008-04-09 17:50:17');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (139, 6177, 6001, 13, '', 9201, 1, '2008-04-09 17:50:29');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (140, 6169, 6004, 13, '', 9201, 1, '2008-04-09 17:50:42');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (141, 6179, 6007, 13, '', 9201, 1, '2008-04-09 17:50:56');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (142, 6167, 6004, 13, '', 9201, 1, '2008-04-09 17:51:10');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (143, 6174, 6007, 13, '', 9201, 1, '2008-04-09 17:51:25');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (144, 6176, 6002, 13, '', 9201, 1, '2008-04-09 17:51:40');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (145, 6181, 6004, 13, '', 9223, 1, '2008-04-09 17:57:02');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (146, 6182, 6004, 13, '', 9223, 1, '2008-04-09 17:59:07');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (147, 6185, 6005, 13, '', 9223, 1, '2008-04-09 18:05:45');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (148, 6189, 6003, 13, '', 9204, 1, '2008-04-10 16:29:14');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (149, 6190, 6004, 13, '', 9204, 1, '2008-04-10 16:29:28');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (150, 6187, 6004, 13, '', 9204, 1, '2008-04-10 16:29:42');


--
-- TOC entry 2757 (class 0 OID 4199726)
-- Dependencies: 1540
-- Data for Name: wkf_control_efficiency; Type: TABLE DATA; Schema: public; Owner: isms
--



--
-- TOC entry 2758 (class 0 OID 4199737)
-- Dependencies: 1541
-- Data for Name: wkf_control_test; Type: TABLE DATA; Schema: public; Owner: isms
--



--
-- TOC entry 2759 (class 0 OID 4199748)
-- Dependencies: 1543
-- Data for Name: wkf_schedule; Type: TABLE DATA; Schema: public; Owner: isms
--



--
-- TOC entry 2760 (class 0 OID 4199762)
-- Dependencies: 1545
-- Data for Name: wkf_task; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (6, 6022, 13, 6004, 2202, 0, 1, '2008-04-09 13:36:02', '2008-04-08 15:48:23');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (55, 6120, 13, 6004, 2207, 0, 1, '2008-04-09 13:36:40', '2008-04-09 13:30:32');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (56, 6121, 13, 6004, 2207, 0, 1, '2008-04-09 13:36:52', '2008-04-09 13:30:32');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (53, 6118, 13, 6004, 2207, 0, 1, '2008-04-09 13:37:02', '2008-04-09 13:30:31');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (46, 6111, 13, 6004, 2207, 0, 1, '2008-04-09 13:37:13', '2008-04-09 13:27:13');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (42, 6107, 13, 6004, 2207, 0, 1, '2008-04-09 13:37:41', '2008-04-09 13:24:34');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (41, 6106, 13, 6004, 2207, 0, 1, '2008-04-09 13:37:56', '2008-04-09 13:24:33');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (3, 6016, 13, 6000, 2201, 0, 1, '2008-04-09 14:38:59', '2008-04-08 15:24:08');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (2, 6014, 13, 6000, 2201, 0, 1, '2008-04-09 14:39:18', '2008-04-08 15:21:33');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (1, 6012, 13, 6000, 2201, 0, 1, '2008-04-09 14:40:39', '2008-04-08 15:19:03');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (24, 6089, 13, 6004, 2205, 0, 1, '2008-04-09 14:49:43', '2008-04-09 11:21:34');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (23, 6088, 13, 6004, 2205, 0, 1, '2008-04-09 14:49:57', '2008-04-09 11:20:16');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (22, 6087, 13, 6004, 2205, 0, 1, '2008-04-09 14:50:09', '2008-04-09 11:18:42');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (21, 6086, 13, 6004, 2205, 0, 1, '2008-04-09 14:51:36', '2008-04-09 11:17:22');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (20, 6085, 13, 6004, 2205, 0, 1, '2008-04-09 14:51:50', '2008-04-09 11:15:45');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (12, 6077, 13, 6004, 2205, 0, 1, '2008-04-09 14:55:27', '2008-04-09 10:58:01');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (11, 6076, 13, 6004, 2205, 0, 1, '2008-04-09 14:55:53', '2008-04-09 10:47:53');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (10, 6075, 13, 6004, 2205, 0, 1, '2008-04-09 14:57:16', '2008-04-09 10:45:02');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (7, 6024, 13, 6004, 2202, 0, 1, '2008-04-09 14:57:48', '2008-04-08 15:49:43');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (52, 6117, 13, 6006, 2207, 0, 1, '2008-04-09 15:00:06', '2008-04-09 13:28:33');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (50, 6115, 13, 6006, 2207, 0, 1, '2008-04-09 15:00:35', '2008-04-09 13:28:32');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (51, 6116, 13, 6006, 2207, 0, 1, '2008-04-09 15:01:15', '2008-04-09 13:28:32');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (44, 6109, 13, 6006, 2207, 0, 1, '2008-04-09 15:01:40', '2008-04-09 13:26:08');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (45, 6110, 13, 6006, 2207, 0, 1, '2008-04-09 15:02:40', '2008-04-09 13:26:08');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (39, 6104, 13, 6006, 2205, 0, 1, '2008-04-09 15:03:51', '2008-04-09 12:08:18');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (38, 6103, 13, 6006, 2205, 0, 1, '2008-04-09 15:04:04', '2008-04-09 12:04:42');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (37, 6102, 13, 6006, 2205, 0, 1, '2008-04-09 15:04:23', '2008-04-09 11:59:09');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (36, 6101, 13, 6006, 2205, 0, 1, '2008-04-09 15:04:50', '2008-04-09 11:55:10');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (35, 6100, 13, 6006, 2205, 0, 1, '2008-04-09 15:05:52', '2008-04-09 11:52:49');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (29, 6094, 13, 6006, 2205, 0, 1, '2008-04-09 15:06:29', '2008-04-09 11:31:23');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (28, 6093, 13, 6006, 2205, 0, 1, '2008-04-09 15:06:49', '2008-04-09 11:29:18');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (27, 6092, 13, 6006, 2205, 0, 1, '2008-04-09 15:07:10', '2008-04-09 11:27:02');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (26, 6091, 13, 6006, 2205, 0, 1, '2008-04-09 15:07:43', '2008-04-09 11:25:11');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (25, 6090, 13, 6006, 2205, 0, 1, '2008-04-09 15:08:46', '2008-04-09 11:23:46');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (9, 6028, 13, 6006, 2202, 0, 1, '2008-04-09 15:09:44', '2008-04-08 15:57:23');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (8, 6026, 13, 6006, 2202, 0, 1, '2008-04-09 15:11:12', '2008-04-08 15:54:13');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (49, 6114, 13, 6007, 2207, 0, 1, '2008-04-09 15:12:48', '2008-04-09 13:28:32');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (48, 6113, 13, 6007, 2207, 0, 1, '2008-04-09 15:13:12', '2008-04-09 13:28:31');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (43, 6108, 13, 6007, 2207, 0, 1, '2008-04-09 15:13:45', '2008-04-09 13:26:07');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (19, 6084, 13, 6007, 2205, 0, 1, '2008-04-09 15:14:16', '2008-04-09 11:13:29');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (18, 6083, 13, 6007, 2205, 0, 1, '2008-04-09 15:14:49', '2008-04-09 11:11:29');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (17, 6082, 13, 6007, 2205, 0, 1, '2008-04-09 15:15:14', '2008-04-09 11:08:28');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (16, 6081, 13, 6007, 2205, 0, 1, '2008-04-09 15:16:11', '2008-04-09 11:06:39');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (15, 6080, 13, 6007, 2205, 0, 1, '2008-04-09 15:17:32', '2008-04-09 11:04:39');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (5, 6020, 13, 6007, 2202, 0, 1, '2008-04-09 15:17:45', '2008-04-08 15:29:35');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (4, 6018, 13, 6007, 2202, 0, 1, '2008-04-09 15:17:59', '2008-04-08 15:28:22');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (66, 6015, 6007, 13, 2222, 0, 1, '2008-04-09 16:32:35', '2008-04-09 15:54:58');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (63, 6013, 6004, 13, 2222, 0, 1, '2008-04-09 16:32:50', '2008-04-09 15:51:02');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (65, 6017, 6006, 13, 2222, 0, 1, '2008-04-09 16:33:06', '2008-04-09 15:53:21');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (62, 6050, 6004, 13, 2222, 0, 1, '2008-04-09 16:33:21', '2008-04-09 15:50:48');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (67, 6061, 6005, 13, 2222, 0, 1, '2008-04-09 16:33:38', '2008-04-09 16:28:34');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (59, 6043, 6003, 13, 2222, 0, 1, '2008-04-09 16:34:09', '2008-04-09 15:48:50');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (58, 6056, 6002, 13, 2222, 0, 1, '2008-04-09 16:34:46', '2008-04-09 15:45:10');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (60, 6025, 6004, 13, 2222, 0, 1, '2008-04-09 16:35:01', '2008-04-09 15:50:18');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (61, 6023, 6004, 13, 2222, 0, 1, '2008-04-09 16:35:16', '2008-04-09 15:50:32');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (64, 6027, 6006, 13, 2222, 0, 1, '2008-04-09 16:35:33', '2008-04-09 15:52:52');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (69, 6123, 6006, 6000, 2222, 0, 1, '2008-04-09 16:45:02', '2008-04-09 16:39:57');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (70, 6123, 6006, 13, 2222, 0, 1, '2008-04-09 16:46:52', '2008-04-09 16:39:57');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (68, 6125, 6005, 6006, 2222, 0, 1, '2008-04-09 16:49:20', '2008-04-09 16:29:25');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (71, 6141, 13, 6004, 2204, 1, 1, NULL, '2008-04-09 16:54:25');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (72, 6142, 13, 6007, 2204, 1, 1, NULL, '2008-04-09 16:54:25');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (73, 6143, 13, 6004, 2204, 1, 1, NULL, '2008-04-09 16:54:25');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (74, 6144, 13, 6006, 2204, 1, 1, NULL, '2008-04-09 16:54:25');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (76, 6146, 6007, 6004, 2204, 1, 1, NULL, '2008-04-09 17:09:58');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (77, 6148, 6007, 6004, 2204, 1, 1, NULL, '2008-04-09 17:09:58');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (80, 6151, 6006, 6004, 2204, 1, 1, NULL, '2008-04-09 17:11:54');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (81, 6152, 6006, 6007, 2204, 1, 1, NULL, '2008-04-09 17:11:54');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (82, 6153, 6006, 6004, 2204, 1, 1, NULL, '2008-04-09 17:11:54');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (84, 6157, 6006, 6004, 2204, 1, 1, NULL, '2008-04-09 17:12:34');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (85, 6158, 6006, 6004, 2204, 1, 1, NULL, '2008-04-09 17:12:34');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (99, 6180, 6007, 13, 2228, 0, 1, '2008-04-09 17:49:41', '2008-04-09 17:48:16');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (97, 6178, 6005, 13, 2228, 0, 1, '2008-04-09 17:49:53', '2008-04-09 17:45:31');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (92, 6173, 6006, 13, 2228, 0, 1, '2008-04-09 17:50:05', '2008-04-09 17:35:43');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (94, 6175, 6003, 13, 2228, 0, 1, '2008-04-09 17:50:17', '2008-04-09 17:40:32');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (96, 6177, 6001, 13, 2228, 0, 1, '2008-04-09 17:50:29', '2008-04-09 17:43:49');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (90, 6169, 6004, 13, 2228, 0, 1, '2008-04-09 17:50:42', '2008-04-09 17:28:46');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (98, 6179, 6007, 13, 2228, 0, 1, '2008-04-09 17:50:56', '2008-04-09 17:47:33');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (88, 6167, 6004, 13, 2228, 0, 1, '2008-04-09 17:51:10', '2008-04-09 17:27:31');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (93, 6174, 6007, 13, 2228, 0, 1, '2008-04-09 17:51:26', '2008-04-09 17:38:48');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (95, 6176, 6002, 13, 2228, 0, 1, '2008-04-09 17:51:40', '2008-04-09 17:42:18');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (100, 6191, 13, 6002, 2232, 1, 1, NULL, '2008-04-10 11:32:55');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (101, 6195, 13, 13, 2226, 1, 1, NULL, '2008-04-10 11:39:57');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (102, 6092, 6005, 6006, 2205, 1, 1, NULL, '2008-04-10 15:18:54');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (104, 6083, 6005, 6007, 2205, 1, 1, NULL, '2008-04-10 15:20:21');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (105, 6188, 13, 13, 2224, 1, 1, NULL, '2008-04-10 16:28:47');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (106, 6190, 6004, 13, 2224, 1, 1, NULL, '2008-04-10 16:32:44');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (107, 6189, 6003, 13, 2224, 1, 1, NULL, '2008-04-10 16:38:49');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (111, 454, 6005, 13, 2215, 1, 1, NULL, '2008-04-11 11:25:55');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (114, 630, 6005, 13, 2215, 1, 1, NULL, '2008-04-11 11:25:56');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (115, 827, 6005, 13, 2215, 1, 1, NULL, '2008-04-11 11:25:57');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (124, 672, 6005, 13, 2215, 1, 1, NULL, '2008-04-11 11:26:00');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (140, 483, 6005, 13, 2215, 1, 1, NULL, '2008-04-11 11:26:05');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (154, 723, 6005, 13, 2215, 1, 1, NULL, '2008-04-11 11:26:09');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (170, 475, 6005, 13, 2215, 1, 1, NULL, '2008-04-11 11:26:14');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (176, 623, 6005, 13, 2215, 1, 1, NULL, '2008-04-11 11:26:16');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (177, 625, 6005, 13, 2215, 1, 1, NULL, '2008-04-11 11:26:17');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (185, 1517, 6005, 13, 2215, 1, 1, NULL, '2008-04-11 11:26:19');


--
-- TOC entry 2761 (class 0 OID 4199774)
-- Dependencies: 1546
-- Data for Name: wkf_task_schedule; Type: TABLE DATA; Schema: public; Owner: isms
--



-- Completed on 2008-04-11 17:13:43

--
-- PostgreSQL database dump complete
--


--
-- TOC entry 2699 (class 0 OID 4199155)
-- Dependencies: 1472
-- Data for Name: isms_config; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO isms_config (pkconfig, svalue) VALUES (201, 'Hardware');
INSERT INTO isms_config (pkconfig, svalue) VALUES (202, 'Software');
INSERT INTO isms_config (pkconfig, svalue) VALUES (203, 'Service');
INSERT INTO isms_config (pkconfig, svalue) VALUES (204, 'People');
INSERT INTO isms_config (pkconfig, svalue) VALUES (205, 'Education');
INSERT INTO isms_config (pkconfig, svalue) VALUES (401, '0');
INSERT INTO isms_config (pkconfig, svalue) VALUES (403, 'localhost');
INSERT INTO isms_config (pkconfig, svalue) VALUES (404, '1');
INSERT INTO isms_config (pkconfig, svalue) VALUES (405, '1');
INSERT INTO isms_config (pkconfig, svalue) VALUES (406, '1');
INSERT INTO isms_config (pkconfig, svalue) VALUES (407, '0');
INSERT INTO isms_config (pkconfig, svalue) VALUES (408, '0');
INSERT INTO isms_config (pkconfig, svalue) VALUES (409, '0');
INSERT INTO isms_config (pkconfig, svalue) VALUES (410, '00:00');
INSERT INTO isms_config (pkconfig, svalue) VALUES (411, '1970-01-01 00:00:00');
INSERT INTO isms_config (pkconfig, svalue) VALUES (415, '%default_sender%');
INSERT INTO isms_config (pkconfig, svalue) VALUES (416, '1');
INSERT INTO isms_config (pkconfig, svalue) VALUES (417, '1');
INSERT INTO isms_config (pkconfig, svalue) VALUES (418, '7801');
INSERT INTO isms_config (pkconfig, svalue) VALUES (419, '00:00');
INSERT INTO isms_config (pkconfig, svalue) VALUES (420, '2008-4-8');
INSERT INTO isms_config (pkconfig, svalue) VALUES (421, '1');
INSERT INTO isms_config (pkconfig, svalue) VALUES (426, '1');
INSERT INTO isms_config (pkconfig, svalue) VALUES (427, 'Confidential');
INSERT INTO isms_config (pkconfig, svalue) VALUES (428, '');
INSERT INTO isms_config (pkconfig, svalue) VALUES (429, '1');
INSERT INTO isms_config (pkconfig, svalue) VALUES (430, '%default_sender_name%');
INSERT INTO isms_config (pkconfig, svalue) VALUES (431, 'CURR_DOLLAR');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7201, '5');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7202, '0');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7203, '0');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7204, '0');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7205, '3');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7206, '365');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7207, '5');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7208, '7');
INSERT INTO isms_config (pkconfig, svalue) VALUES (801, '13');
INSERT INTO isms_config (pkconfig, svalue) VALUES (803, '13');
INSERT INTO isms_config (pkconfig, svalue) VALUES (804, '13');
INSERT INTO isms_config (pkconfig, svalue) VALUES (805, '13');
INSERT INTO isms_config (pkconfig, svalue) VALUES (806, '13');
INSERT INTO isms_config (pkconfig, svalue) VALUES (807, '13');
INSERT INTO isms_config (pkconfig, svalue) VALUES (808, '13');
INSERT INTO isms_config (pkconfig, svalue) VALUES (809, '13');
INSERT INTO isms_config (pkconfig, svalue) VALUES (810, '13');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7403, '5');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7765, '');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7209, '%error_report_email%');
INSERT INTO isms_config (pkconfig, svalue) VALUES (402, '1');
INSERT INTO isms_config (pkConfig, sValue) VALUES (5701, '8301');
--INSERT INTO isms_config (pkconfig, svalue) VALUES (7404, '0');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7500, '0');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7501, '');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7502, '');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7503, '');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7504, '');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7510, '');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7511, '');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7512, '');

/***** AD *****/
INSERT INTO isms_config (pkconfig, svalue) VALUES (7600, '0');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7601, 'ad-server.local');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7602, '389');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7603, '0');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7604, 'DC=ad-server, DC=local');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7605, 'ad-server\%user%');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7606, 'admin');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7607, 'adminpass');

INSERT INTO isms_config (pkconfig, svalue) VALUES (7406, '');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7407, '');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7405, 3);

/***** Configurações para o reset da Base de Dados *****/
INSERT INTO isms_config (pkconfig, svalue) VALUES (7700, '0');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7701, '');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7702, '1');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7703, '');

/* FUSO HORARIO */
INSERT INTO isms_config (pkconfig, svalue) VALUES (7404, '0');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7514, '1');

/* CORES CUSTOMIZADAS */
INSERT INTO isms_config (pkconfig, svalue) VALUES (7515, '');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7516, '');

--
-- TOC entry 2707 (class 0 OID 4199219)
-- Dependencies: 1484
-- Data for Name: isms_saas; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO isms_saas (pkconfig, svalue) VALUES (711701, '%saas_report_email%');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711714, '%config_path%');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711703, '');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711704, '');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711705, '');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711706, '');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711707, '');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711708, '');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711709, '1');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711710, '');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711712, '');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711713, '%user_email%');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711715, '%max_file_size%');

-- Non Conformity
INSERT INTO isms_non_conformity_types(pkclassification, sname) VALUES (62251, 'Auditoria Interna');
INSERT INTO isms_non_conformity_types(pkclassification, sname) VALUES (62252, 'Auditoria Externa');
INSERT INTO isms_non_conformity_types(pkclassification, sname) VALUES (62253, 'Controle de Segurança');

