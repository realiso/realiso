INSERT INTO isms_context (pkContext,nType,nState) VALUES (6257,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (6257,null,'PCI');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6257,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6257,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6550,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (6550,6257,'Requirement 10: Track and monitor all access to network resources and cardholder data');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6550,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6550,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6551,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (6551,6257,'Requirement 11: Regularly test security systems and processes');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6551,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6551,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6262,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (6262,6257,'Requirement 01: Install and maintain a firewall configuration to protect cardholder data');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6262,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6262,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6305,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (6305,6257,'Requirement 02: Do not use vendor-supplied defaults for system passwords and other security parameters');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6305,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6305,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6324,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (6324,6257,'Requirement 03: Protect stored cardholder data');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6324,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6324,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6365,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (6365,6257,'Requirement 04: Encrypt transmission of cardholder data across open, public networks');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6365,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6365,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6372,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (6372,6257,'Requirement 05: Use and regularly update anti-virus software or programs');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6372,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6372,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6379,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (6379,6257,'Requirement 06: Develop and maintain secure systems and applications');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6379,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6379,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6444,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (6444,6257,'Requirement 07: Restrict access to cardholder data by business need to know');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6444,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6444,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6463,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (6463,6257,'Requirement 08: Assign a unique ID to each person with computer access');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6463,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6463,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6507,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (6507,6257,'Requirement 09: Restrict physical access to cardholder data');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6507,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6507,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6616,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (6616,6257,'Requirement 12: Maintain a policy that addresses information security for employees and contractors');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6616,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6616,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6695,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (6695,6257,'Requirement A.1: Shared hosting providers must protect the cardholder data environment');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6695,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6695,2902,NOW());

