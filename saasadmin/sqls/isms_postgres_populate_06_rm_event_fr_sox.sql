INSERT INTO isms_context (pkContext,nType,nState) VALUES (10008,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10008,10007,'Deficiencies found during ongoing monitoring are not communicated to at least one level of management above the individual responsible for the function.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10008,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10008,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10009,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10009,10007,'Deficiencies found during ongoing monitoring are not communicated to the individual responsible for the function.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10009,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10009,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10010,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10010,10007,'Deficiencies found during separate evaluations are not communicated to at least one level of management above the individual responsible for the function.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10010,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10010,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10011,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10011,10007,'Deficiencies found during separate evaluations are not communicated to the individual responsible for the function.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10011,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10011,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10012,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10012,10007,'Employees do not understand the Code of Conduct.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10012,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10012,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10013,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10013,10007,'Employees ignore the Code of Conduct.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10013,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10013,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10014,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10014,10007,'Hostile takeover.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10014,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10014,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10015,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10015,10007,'Inaccurate, untimely or unavailable information regarding pricing, products, actual or potential customers, advertising and promotion.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10015,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10015,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10016,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10016,10007,'Inadequate and out-dated planning systems.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10016,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10016,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10017,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10017,10007,'Inadequate attention to relationships with shareholders, investors or other outside parties.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10017,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10017,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10018,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10018,10007,'Inadequate information regarding factors that may influence the entity''s marketing strategy.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10018,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10018,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10019,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10019,10007,'Inadequate management information systems.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10019,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10019,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10020,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10020,10007,'Information does not generally flow down, across, and up organization.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10020,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10020,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10021,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10021,10007,'Insufficient information regarding available opportunities.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10021,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10021,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10022,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10022,10007,'Insufficient interaction of information technology, financial and operating management in developing strategic plans.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10022,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10022,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10023,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10023,10007,'Integrity and ethical values are not maintained and demonstrated by management and staff.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10023,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10023,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10024,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10024,10007,'Lack of awareness of entity-wide objectives.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10024,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10024,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10025,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10025,10007,'Lack of awareness of sales and marketing objectives.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10025,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10025,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10026,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10026,10007,'Lack of Code of Conduct.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10026,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10026,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10027,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10027,10007,'Lack of product demand.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10027,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10027,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10028,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10028,10007,'Lack of understanding of critical success factors.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10028,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10028,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10029,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10029,10007,'Employees may not feel their efforts are noticed or appreciated.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10029,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10029,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10030,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10030,10007,'Due dates and relative priorities of management reports are not clarified or communicated.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10030,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10030,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10031,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10031,10007,'Lack of understanding of government policies.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10031,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10031,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10032,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10032,10007,'Leakage of strategic information.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10032,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10032,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10033,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10033,10007,'Limited number of appropriate distributors.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10033,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10033,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10034,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10034,10007,'Limited number of positions.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10034,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10034,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10035,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10035,10007,'Management does not assess risk significance.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10035,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10035,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10036,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10036,10007,'Management does not assess the likelihood of risk occurrence.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10036,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10036,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10037,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10037,10007,'Management does not assess what actions should be taken to mitigate risks.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10037,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10037,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10038,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10038,10007,'Management does not compare actual performance to planned or expected results and analyze significant differences.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10038,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10038,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10039,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10039,10007,'Management does not complete, within established timeframes, all actions that correct or otherwise resolve the matters brought to management''s attention.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10039,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10039,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10040,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10040,10007,'Management does not continually assess personnel skills.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10040,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10040,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10041,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10041,10007,'Management does not determine the proper actions in response to findings and recommendations from audits and other reviews.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10041,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10041,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10042,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10042,10007,'Management does not effectively manage its workforce.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10042,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10042,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10043,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10043,10007,'Management does not have a good relationship with central oversight agencies such as OMB.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10043,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10043,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10044,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10044,10007,'Management does not have a good relationship with Congress.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10044,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10044,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10045,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10045,10007,'Management does not have a good relationship with the OIG.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10045,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10045,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10046,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10046,10007,'Management does not have access to information relating to current technological developments.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10046,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10046,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10047,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10047,10007,'Management does not have access to operational and/or financial data.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10047,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10047,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10048,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10048,10007,'Management does not have mechanisms in place to identify and deal with risks that are unique to the operating environment of the Government.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10048,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10048,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10049,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10049,10007,'Management does not hire the right personnel for the job.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10049,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10049,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10050,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10050,10007,'Management does not maintain effective controls over information processing.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10050,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10050,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10051,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10051,10007,'Management does not manage risk.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10051,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10051,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10052,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10052,10007,'Organization does not have documented procedures in place to delegate authority and responsibility.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10052,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10052,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10053,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10053,10007,'Management does not plan for personnel replacement.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10053,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10053,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10054,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10054,10007,'Organization does not have procedures in place to delegate authority and responsibility.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10054,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10054,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10055,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10055,10007,'Management does not play a key role in providing leadership in the area of integrity and ethical values.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10055,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10055,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10056,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10056,10007,'Organization does not maintain physical control of, and properly secures/safeguard, vulnerable assets.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10056,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10056,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10057,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10057,10007,'Management does not promptly evaluate findings from audits and other reviews.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10057,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10057,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10058,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10058,10007,'Organization does not provide a proper amount of supervision.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10058,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10058,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10059,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10059,10007,'Management does not provide adequate incentives for the personnel to ensure job success.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10059,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10059,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10060,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10060,10007,'Organization does not provide guidance for disciplinary actions, when appropriate.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10060,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10060,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10061,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10061,10007,'Organization does not provide guidance for proper behavior influences the quality of internal control.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10061,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10061,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10062,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10062,10007,'Management does not provide adequate training for the personnel to ensure job success.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10062,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10062,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10063,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10063,10007,'Organization does not provide guidance for removing temptations for unethical behavior.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10063,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10063,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10064,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10064,10007,'Management does not provide candid and constructive counseling.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10064,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10064,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10065,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10065,10007,'Organization has not established, and/or does not regularly review, performance measures and indicators.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10065,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10065,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10066,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10066,10007,'Management does not provide needed training.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10066,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10066,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10067,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10067,10007,'Organization structure does not define key areas of authority and responsibility.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10067,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10067,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10068,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10068,10007,'Management does not provide performance appraisals.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10068,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10068,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10069,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10069,10007,'Organization structure does not establish appropriate lines of reporting.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10069,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10069,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10070,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10070,10007,'Organizational structure does not provide a framework for planning, directing, and controlling operations to achieve agency objectives.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10070,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10070,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10071,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10071,10007,'Management does not provide qualified and continuous supervision of internal control objectives.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10071,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10071,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10072,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10072,10007,'Organization does not have appropriate practices for disciplining personnel.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10072,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10072,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10073,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10073,10007,'Plan formats are ineffective in providing necessary benchmarks against which performance can be measured.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10073,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10073,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10074,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10074,10007,'Management does not retain valuable personnel.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10074,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10074,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10075,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10075,10007,'Management does not track major organizational achievements.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10075,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10075,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10076,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10076,10007,'Management has a dismissive attitude toward accounting functions.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10076,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10076,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10077,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10077,10007,'Policies and procedures have not been established for ensuring that findings of audits and other reviews are promptly resolved.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10077,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10077,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10078,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10078,10007,'Management has a dismissive attitude toward audits and evaluations.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10078,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10078,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10079,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10079,10007,'Management has a dismissive attitude toward information systems.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10079,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10079,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10080,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10080,10007,'Management has a dismissive attitude toward monitoring functions activities.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10080,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10080,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10081,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10081,10007,'Pre-established standards are not determined.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10081,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10081,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10082,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10082,10007,'Products become obsolete.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10082,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10082,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10083,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10083,10007,'Management has a dismissive attitude toward personnel functions.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10083,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10083,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10084,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10084,10007,'Relationships between oversight agencies and management do not exist.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10084,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10084,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10085,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10085,10007,'Management has not adopted performance-based management.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10085,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10085,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10086,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10086,10007,'Sales personnel are unaware of potential customers');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10086,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10086,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10087,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10087,10007,'Management has not comprehensively identified risks at the entity-level.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10087,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10087,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10088,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10088,10007,'Management has not considered all significant interactions between individual activities and other parties.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10088,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10088,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10089,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10089,10007,'Management has not considered all significant interactions between the entity and other parties.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10089,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10089,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10090,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10090,10007,'Management has not considered all significant internal factors at the activity-level.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10090,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10090,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10091,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10091,10007,'Organization delegation procedures do not cover authority and responsibility for operating activities.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10091,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10091,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10092,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10092,10007,'Management has not considered all significant internal factors at the entity-level.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10092,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10092,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10093,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10093,10007,'Organization delegation procedures do not cover authority and responsibility for reporting relationships.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10093,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10093,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10094,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10094,10007,'Management has not established clear, consistent Agency objectives.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10094,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10094,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10095,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10095,10007,'Organization does not have a corporate information systems architecture.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10095,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10095,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10096,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10096,10007,'Organization does not have appropriate practices for compensating personnel.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10096,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10096,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10097,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10097,10007,'Management has not established clear, consistent objectives at the activity-level.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10097,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10097,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10098,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10098,10007,'Organization does not have appropriate practices for counseling personnel.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10098,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10098,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10099,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10099,10007,'Management has not recently revisited the sufficiency of disaster recovery plans.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10099,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10099,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10100,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10100,10007,'Organization does not have appropriate practices for evaluating personnel.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10100,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10100,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10101,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10101,10007,'Management is unaware of legal and regulatory requirements.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10101,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10101,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10102,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10102,10007,'Organization does not have appropriate practices for hiring personnel.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10102,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10102,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10103,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10103,10007,'Management is unaware of valuable assets.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10103,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10103,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10104,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10104,10007,'Organization does not have appropriate practices for orienting personnel.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10104,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10104,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10105,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10105,10007,'Management or supervisory personnel ignore legal and regulatory requirements or company policies.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10105,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10105,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10106,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10106,10007,'Organization does not have appropriate practices for promoting personnel.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10106,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10106,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10107,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10107,10007,'Management takes too much risk.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10107,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10107,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10108,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10108,10007,'Organization does not have appropriate practices for training personnel.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10108,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10108,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10109,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10109,10007,'Mismatch in policy and knowledge about marketing and financial strategies.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10109,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10109,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10110,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10110,10007,'Monitoring efforts do not include comparisons, reconciliations, and other actions that people take in performing their duties.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10110,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10110,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10111,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10111,10007,'Monitoring efforts do not include regular management and supervisory activities.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10111,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10111,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10112,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10112,10007,'No adequate strategy in place.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10112,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10112,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10113,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10113,10007,'Non added value based acquisition.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10113,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10113,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10114,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10114,10007,'Not meeting targets.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10114,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10114,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10115,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10115,10007,'Ongoing monitoring does not occur in the course of normal operations.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10115,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10115,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10116,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10116,10007,'Organization delegation procedures do not cover authority and responsibility for authorization protocols.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10116,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10116,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10117,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10117,10007,'Separate evaluations of controls do not focus directly on their effectiveness at a specific time.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10117,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10117,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10118,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10118,10007,'Serious matters found during ongoing monitoring are not reported to top management.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10118,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10118,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10119,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10119,10007,'Wrong information as basis for decision making; competition risk.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10119,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10119,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10120,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10120,10007,'Serious matters found during separate evaluations are not reported to top management.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10120,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10120,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10121,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10121,10007,'Management has not identified appropriate knowledge and skills needed for various jobs.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10121,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10121,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10122,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10122,10007,'Technology development projects do not support entity-wide objectives or strategies.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10122,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10122,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10123,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10123,10007,'Management information needs with respect to payroll are not defined.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10123,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10123,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10124,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10124,10007,'The organization does not have a positive ''ethical tone''.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10124,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10124,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10125,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10125,10007,'The scope and frequency of separate evaluations do not take into consideration the assessment of risks and the effectiveness of ongoing monitoring procedures.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10125,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10125,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10126,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10126,10007,'Margin deterioration, alternative sales prices quoted.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10126,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10126,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10127,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10127,10007,'Margin pressure risk.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10127,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10127,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10128,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10128,10007,'There are no separate evaluations of internal control effectiveness.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10128,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10128,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10129,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10129,10007,'There is a lack of adequate means of communicating with, and obtaining information needed throughout organization to achieve from, external stakeholders that may have a significant impact on organization achieving its goals.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10129,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10129,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10130,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10130,10007,'Organization does not have disaster recovery plans in place.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10130,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10130,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10131,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10131,10007,'Unsecure bad acquisition.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10131,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10131,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10132,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10132,10007,'Treasury risk.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10132,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10132,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10133,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10133,10007,'Waste of money as result from non economical decision making.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10133,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10133,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10134,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10134,10007,'Wrong expectations released by investor relations.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10134,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10134,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10136,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10136,10135,'Changing legal and regulatory requirements.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10136,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10136,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10137,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10137,10135,'Employees may not be aware of applicable laws and regulations.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10137,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10137,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10138,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10138,10135,'Existing patents may be disregarded.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10138,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10138,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10139,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10139,10135,'Inadequate information about, or understanding of, filing requirements and applicable laws and regulations.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10139,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10139,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10140,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10140,10135,'Lack of awareness of laws and regulations.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10140,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10140,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10141,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10141,10135,'Lack of knowledge regarding OSHA laws and regulations.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10141,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10141,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10142,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10142,10135,'Legal counsel does get all contracts where entity commits itself legally.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10142,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10142,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10143,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10143,10135,'Legal counsel does not review contracts or agreements.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10143,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10143,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10144,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10144,10135,'Legal counsel is unaware of all activities taking place within the entity.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10144,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10144,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10145,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10145,10135,'Management or supervisory personnel are unaware of legal and regulatory requirements and company policies.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10145,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10145,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10146,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10146,10135,'Nonlegal personnel are unaware that certain circumstances could potentially lead to litigation.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10146,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10146,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10147,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10147,10135,'Personnel are unaware of applicable laws and regulations.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10147,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10147,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10148,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10148,10135,'Personnel are unaware of applicable laws, regulations, rules or contractual agreements.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10148,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10148,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10150,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10150,10149,'Access to resources and records is not limited to authorized individuals.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10150,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10150,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10151,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10151,10149,'Accountability for custody and use of resources and records is not assigned and/or maintained.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10151,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10151,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10152,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10152,10149,'Analysis cannot be conducted due to inconsistent or nonexistent performance indicators.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10152,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10152,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10153,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10153,10149,'Documentation is not readily available for examination.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10153,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10153,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10154,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10154,10149,'Inaccurate information or estimates regarding costs of litigation or anticipated settlements.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10154,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10154,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10155,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10155,10149,'Inaccurate or unavailable customer information.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10155,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10155,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10156,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10156,10149,'Inaccurate, insufficient or untimely information regarding risk-related costs or accidents or incidents that could give rise to an insurance claim.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10156,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10156,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10157,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10157,10149,'Inaccurate, untimely or unavailable information regarding actual costs incurred.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10157,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10157,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10158,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10158,10149,'Inaccurate, untimely or unavailable information regarding cash inflows and outflows.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10158,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10158,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10159,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10159,10149,'Inadequate information about, or understanding of, financial reporting of tax transactions or economic events.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10159,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10159,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10160,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10160,10149,'Inadequate information regarding tax-savings opportunities.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10160,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10160,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10161,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10161,10149,'Inadequate or inaccurate information.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10161,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10161,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10162,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10162,10149,'Incomplete or inaccurate information used as the basis for document preparation.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10162,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10162,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10163,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10163,10149,'Information needs of management or others is unknown or not clearly communicated.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10163,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10163,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10164,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10164,10149,'Information systems are incapable of providing necessary information.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10164,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10164,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10165,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10165,10149,'Information systems cannot provide necessary information in a timely manner.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10165,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10165,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10166,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10166,10149,'Internal control, all transactions, and other significant events are not clearly documented.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10166,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10166,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10167,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10167,10149,'Journal entries related to tax transactions or economic events are not properly approved or posted to the general ledger.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10167,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10167,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10168,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10168,10149,'Lack of information regarding profit margins and / or sales prices.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10168,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10168,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10169,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10169,10149,'Lack of or inaccurate information regarding competitive products or potential new products.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10169,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10169,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10170,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10170,10149,'Lack of understanding of reporting requirements.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10170,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10170,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10171,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10171,10149,'Out-of-date or incomplete price information.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10171,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10171,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10172,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10172,10149,'Pertinent operational and/or financial information is not identified, captured, and distributed in a form and time frame that permits employees to perform their duties efficiently.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10172,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10172,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10173,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10173,10149,'Unavailable or inaccurate information.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10173,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10173,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10174,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10174,10149,'Unavailable or inaccurate information about fraudulent acts or other improper activities of vendors.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10174,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10174,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10175,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10175,10149,'Unavailable or inaccurate information on inventory levels or production needs.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10175,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10175,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10176,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (10176,10149,'Incomplete or inaccurate information regarding changes affecting the entity, such as competition, products, customer preferences, or legal and regulatory changes.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10176,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10176,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11410,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11410,11225,'Employees are authorizing and executing transactions and other significant events outside the scope of their authority.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11410,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11410,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11411,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11411,11225,'Handling and storage procedures, including storage containers, facilities and maintenance, are inappropriate for the nature of the products.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11411,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11411,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11412,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11412,11225,'Inaccurate information.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11412,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11412,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11413,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11413,11225,'Inaccurate input of data.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11413,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11413,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11414,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11414,11225,'Inaccurate or incomplete information is acquired and retained.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11414,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11414,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11415,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11415,11225,'Inaccurate or untimely information.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11415,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11415,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11416,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11416,11225,'Inadequate health & safety considerations.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11416,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11416,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11417,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11417,11225,'Inadequate policies and procedures to prevent unauthorized use.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11417,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11417,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11418,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11418,11225,'Inadequate transfer or requisition procedures.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11418,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11418,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11419,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11419,11225,'Inadequate vendor screening.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11419,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11419,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11420,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11420,11225,'Information is too specific to be usable.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11420,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11420,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11421,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11421,11225,'Insufficient or inappropriate resources.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11421,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11421,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11422,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11422,11225,'Insufficient staff.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11422,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11422,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11423,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11423,11225,'Lack of adequate tools for the personnel to ensure job success.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11423,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11423,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11424,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11424,11225,'Lack of awareness of entity''s current human resources.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11424,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11424,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11425,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11425,11225,'Lack of or excess staff.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11425,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11425,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11426,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11426,11225,'Lack or loss of information or documents.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11426,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11426,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11427,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11427,11225,'Lost or misplaced information.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11427,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11427,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11428,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11428,11225,'Missing documents or incorrect information.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11428,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11428,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11429,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11429,11225,'Missing or untimely receipt of documents.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11429,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11429,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11436,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11436,11225,'Misuse of data / information (inside and outside organization).');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11436,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11436,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11437,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11437,11225,'Personnel are unclear of levels of authority.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11437,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11437,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11438,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11438,11225,'Personnel do not possess and maintain the level of competence that allows them to accomplish their assigned duties.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11438,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11438,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11439,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11439,11225,'Personnel do not understand the importance of developing and implementing good internal control');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11439,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11439,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11440,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11440,11225,'Personnel enter into contracts or agreements that are beyond their scope of authority.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11440,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11440,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11441,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11441,11225,'Personnel missing industry specific knowledge.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11441,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11441,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11442,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11442,11225,'Poorly maintained or inadequate equipment.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11442,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11442,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11443,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11443,11225,'Process has not properly segregated key duties and responsibilities.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11443,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11443,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11444,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11444,11225,'Receipts are for amounts different than invoiced amounts, or are not identifiable.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11444,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11444,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11445,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11445,11225,'Record-keeping requirements are disregarded.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11445,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11445,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11446,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11446,11225,'Records are lost or prematurely destroyed.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11446,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11446,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11447,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11447,11225,'Requisitions may be lost.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11447,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11447,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11448,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11448,11225,'Reuse of supporting documents.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11448,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11448,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11449,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11449,11225,'Technology may not be adequately defined.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11449,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11449,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11450,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11450,11225,'Training requirements may not be adequately identified.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11450,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11450,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11451,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11451,11225,'Transactions are not accounted for using numerical sequences.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11451,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11451,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11452,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11452,11225,'Transactions are not entered in a timely manner.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11452,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11452,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11453,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11453,11225,'Transfer documentation may be lost.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11453,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11453,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11454,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11454,11225,'Transfer documents may be lost.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11454,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11454,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11455,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11455,11225,'Transfer procedures do not require preparation of supporting documentation: intercompany goods movement.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11455,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11455,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11456,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11456,11225,'Unauthorized access.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11456,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11456,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11457,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11457,11225,'Unavailability of service personnel.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11457,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11457,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11458,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11458,11225,'Untrained staff.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11458,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11458,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11459,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11459,11225,'Variances are computed or recorded inaccurately.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11459,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11459,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11460,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11460,11430,'Customer order information may be unclear, inaccurate or incomplete.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11460,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11460,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11461,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11461,11430,'Customer orders may not be authorized.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11461,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11461,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11462,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11462,11430,'Customers delay remittance.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11462,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11462,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11463,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11463,11430,'Fictitious documentation is created.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11463,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11463,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11464,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11464,11430,'Handling and storage procedures, including storage containers, facilities and maintenance, are inappropriate for the nature of the products.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11464,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11464,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11465,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11465,11430,'Inaccurate or untimely pricing and inventory information.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11465,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11465,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11466,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11466,11430,'Inappropriate handling and storage policies and procedures.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11466,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11466,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11467,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11467,11430,'Inappropriate or unclear specifications.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11467,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11467,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11468,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11468,11430,'Inappropriate production specifications.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11468,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11468,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11469,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11469,11430,'Incomplete or inaccurate Information from order processing.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11469,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11469,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11470,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11470,11430,'Incomplete or inaccurate information regarding materials transferred to / from storage.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11470,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11470,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11471,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11471,11430,'Incomplete, untimely or inaccurate credit information.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11471,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11471,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11472,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11472,11430,'Information on issued purchase orders is not clearly or completely communicated.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11472,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11472,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11473,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11473,11430,'Information system does not identify available discounts and related required payment dates.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11473,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11473,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11474,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11474,11430,'Insufficient goods movement administration.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11474,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11474,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11475,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11475,11430,'Insufficient number of customer service representatives or service personnel.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11475,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11475,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11476,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11476,11430,'Insufficient or excess raw materials due to poor communication with procurement, or inaccurate or untimely material requirement forecasts.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11476,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11476,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11477,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11477,11430,'Insufficient storage capacity.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11477,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11477,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11478,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11478,11430,'Invalid accounts payable fraudulently created for unauthorized or nonexistent purchases.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11478,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11478,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11479,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11479,11430,'Lack of adequate systems.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11479,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11479,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11480,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11480,11430,'Lack of customer information.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11480,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11480,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11481,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11481,11430,'Mismatch of economical stock situation and physical stocks; interruption in production process due to stock not being there.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11481,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11481,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11482,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11482,11430,'Order documentation is lost.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11482,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11482,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11483,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11483,11430,'Poor communication with marketing regarding sales forecasts.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11483,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11483,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11484,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11484,11430,'Poor performance of distributors.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11484,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11484,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11485,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11485,11430,'Product is not produced according quality control standards.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11485,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11485,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11486,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11486,11430,'Product is unavailable in sufficient quantity.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11486,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11486,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11487,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11487,11430,'Product unavailability.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11487,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11487,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11488,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11488,11430,'Purchase order specifications are unclear.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11488,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11488,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11489,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11489,11430,'Purchase orders are lost or not forwarded to inbound activities.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11489,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11489,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11490,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11490,11430,'Purchase orders are not entered into the system on a timely basis.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11490,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11490,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11491,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11491,11430,'Purchase orders may be lost.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11491,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11491,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11492,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11492,11430,'Quantities to be produced are not communicated clearly.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11492,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11492,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11493,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11493,11430,'Sales orders are lost.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11493,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11493,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11494,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11494,11430,'Sales personnel are unaware of marketing strategies.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11494,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11494,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11495,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11495,11430,'Sales personnel disregard marketing strategies.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11495,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11495,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11496,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11496,11430,'Salespeople lack knowledge about product features or benefits.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11496,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11496,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11497,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11497,11430,'Salespeople perform poorly.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11497,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11497,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11498,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11498,11430,'Uncommunicated changes in warranty policies.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11498,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11498,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11499,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11499,11430,'Unordered or unauthorized products are included in customer shipment.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11499,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11499,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11500,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11500,11430,'Untimely processing of order information.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11500,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11500,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11501,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11501,11431,'Access to data, files, and programs are not controlled.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11501,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11501,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11502,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11502,11431,'Acquisition documentation may be lost or otherwise not communicated to proper personnel.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11502,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11502,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11503,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11503,11431,'Asset disposals or transfers may not be communicated to proper personnel.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11503,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11503,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11504,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11504,11431,'Certain jobs, activities or locations are hazardous.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11504,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11504,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11505,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11505,11431,'Data entry edit checks are not conducted.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11505,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11505,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11506,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11506,11431,'Discontinuity of business processes.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11506,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11506,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11507,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11507,11431,'Dishonest employees.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11507,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11507,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11508,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11508,11431,'Disruption of normal shipping channels.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11508,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11508,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11509,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11509,11431,'Downtime as result from inadequate skilled labor.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11509,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11509,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11510,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11510,11431,'Downtime as result of natural or other disasters.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11510,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11510,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11511,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11511,11431,'Downtime as result of poorly maintained, misused or obsolete equipment.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11511,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11511,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11512,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11512,11431,'Due date information is not available.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11512,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11512,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11513,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11513,11431,'Employees are not familiar with handling and storage requirements or procedures.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11513,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11513,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11514,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11514,11431,'Employees ignore safety policies or procedures.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11514,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11514,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11515,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11515,11431,'Excessive work steps/operations.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11515,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11515,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11516,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11516,11431,'Fictitious documentation is created.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11516,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11516,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11517,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11517,11431,'Handling and storage procedures, including storage containers, facilities and maintenance, are inappropriate for the nature of the products.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11517,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11517,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11518,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11518,11431,'Improper cutoff of shipments at the end of a period.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11518,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11518,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11519,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11519,11431,'Improper organization of storage facility.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11519,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11519,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11520,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11520,11431,'Improper products or improper quantities are retrieved from storage.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11520,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11520,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11521,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11521,11431,'Improperly trained service personnel.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11521,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11521,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11522,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11522,11431,'Inability to identify the stage of production.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11522,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11522,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11523,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11523,11431,'Inaccurate or incomplete shipping documents.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11523,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11523,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11524,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11524,11431,'Inadequate physical security over fixed assets.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11524,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11524,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11525,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11525,11431,'Inadequate physical security over goods received.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11525,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11525,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11526,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11526,11431,'Inappropriate handling and storage policies and procedures.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11526,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11526,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11527,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11527,11431,'Inappropriate production specifications.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11527,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11527,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11528,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11528,11431,'Incomplete or inaccurate Information from order processing.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11528,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11528,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11529,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11529,11431,'Incomplete or inaccurate information regarding materials transferred to / from storage.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11529,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11529,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11530,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11530,11431,'Incorrect information is entered on shipping documentation.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11530,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11530,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11531,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11531,11431,'Information on materials received is not entered into the information system accurately or on a timely basis by goods receiver.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11531,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11531,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11532,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11532,11431,'Insufficient goods movement administration.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11532,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11532,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11533,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11533,11431,'Insufficient or excess raw materials due to poor communication with procurement, or inaccurate or untimely material requirement forecasts.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11533,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11533,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11534,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11534,11431,'Insufficient storage capacity.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11534,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11534,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11535,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11535,11431,'Interruption of production process due to lack of materials.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11535,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11535,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11536,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11536,11431,'Lack of adequate systems.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11536,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11536,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11537,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11537,11431,'Lost receiving reports or lost shipping records.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11537,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11537,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11538,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11538,11431,'Materials are not tested for specification compliance.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11538,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11538,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11539,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11539,11431,'Materials not requisitioned are transferred.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11539,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11539,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11540,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11540,11431,'Mismatch of economical stock situation and physical stocks; interruption in production process due to stock not being there.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11540,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11540,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11541,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11541,11431,'Order documentation is lost.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11541,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11541,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11542,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11542,11431,'Order or shipping documentation may be lost.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11542,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11542,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11543,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11543,11431,'Out-of-date production facilities.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11543,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11543,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11544,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11544,11431,'Packing materials, containers or procedures are inappropriate for the nature of the product or method of shipment.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11544,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11544,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11545,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11545,11431,'Plans and schedules are not communicated to inbound activities, or do not clearly identify when goods can be received or where materials are needed.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11545,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11545,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11546,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11546,11431,'Poor communication of operations'' or other activities'' needs.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11546,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11546,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11547,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11547,11431,'Poor organization of customer service department.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11547,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11547,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11548,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11548,11431,'Poorly organized production process.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11548,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11548,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11549,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11549,11431,'Pressure to meet production deadlines.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11549,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11549,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11550,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11550,11431,'Product is not produced according quality control standards.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11550,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11550,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11551,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11551,11431,'Product is unavailable in sufficient quantity.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11551,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11551,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11552,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11552,11431,'Product may be moved into or out of storage without proper authorization.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11552,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11552,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11553,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11553,11431,'Product moved into or out of storage may not be documented or recorded.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11553,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11553,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11554,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11554,11431,'Product unavailability.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11554,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11554,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11555,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11555,11431,'Production processes do not include procedures designed to ensure quality production.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11555,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11555,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11556,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11556,11431,'Purchase orders may be lost.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11556,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11556,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11557,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11557,11431,'Quality problems are not discovered or appropriately reported during the production process.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11557,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11557,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11558,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11558,11431,'Quantities to be produced are not communicated clearly.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11558,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11558,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11559,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11559,11431,'Receiving information may be entered inaccurately in the information system, or may not be timely.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11559,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11559,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11560,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11560,11431,'Several products compete for concurrent production.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11560,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11560,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11561,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11561,11431,'Shipping documents are lost.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11561,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11561,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11562,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11562,11431,'Unavailable or inaccurate information on items ordered but not received.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11562,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11562,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11563,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11563,11431,'Uncommunicated changes in warranty policies.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11563,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11563,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11564,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11564,11431,'Unordered or unauthorized products are included in customer shipment.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11564,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11564,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11565,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11565,11431,'Untimely processing of order information.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11565,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11565,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11566,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11566,11431,'Use of inefficient shipping methods.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11566,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11566,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11567,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11567,11431,'Vendors'' inability to provide needed quantities due to other higher-priority orders or an interruption in their own supplies.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11567,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11567,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11568,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11568,11430,'Sales orders are lost.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11568,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11568,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11569,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11569,11432,'Acquisition documentation may be lost or otherwise not communicated to proper personnel.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11569,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11569,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11570,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11570,11432,'Certain jobs, activities or locations are hazardous.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11570,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11570,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11571,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11571,11432,'Downtime as result from inadequate skilled labor.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11571,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11571,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11572,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11572,11432,'Downtime as result of poorly maintained, misused or obsolete equipment.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11572,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11572,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11573,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11573,11432,'Due date information is not available.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11573,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11573,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11574,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11574,11432,'Fictitious documentation is created.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11574,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11574,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11575,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11575,11432,'Inadequate physical security over fixed assets.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11575,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11575,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11576,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11576,11432,'Inadequate physical security over goods received.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11576,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11576,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11577,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11577,11432,'Inadequate product testing.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11577,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11577,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11578,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11578,11432,'Inappropriate or unclear specifications.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11578,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11578,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11579,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11579,11432,'Inappropriate production specifications.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11579,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11579,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11580,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11580,11432,'Interruption of production process due to lack of materials.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11580,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11580,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11581,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11581,11432,'Materials are not tested for specification compliance.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11581,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11581,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11582,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11582,11432,'Materials not requisitioned are transferred.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11582,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11582,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11583,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11583,11432,'Mismatch of economical stock situation and physical stocks; interruption in production process due to stock not being there.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11583,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11583,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11584,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11584,11432,'Out-of-date production facilities.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11584,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11584,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11585,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11585,11432,'Patents relevant in the market place are not known.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11585,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11585,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11586,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11586,11432,'Product is not produced according quality control standards.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11586,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11586,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11587,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11587,11432,'Relevant patents may not be identified.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11587,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11587,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11588,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11588,11432,'Uncommunicated changes in warranty policies.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11588,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11588,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11589,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11589,11432,'Vendors'' inability to provide needed quantities due to other higher-priority orders or an interruption in their own supplies.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11589,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11589,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11590,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11590,11433,'Acquisition documentation may be lost or otherwise not communicated to proper personnel.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11590,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11590,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11591,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11591,11433,'Asset disposals or transfers may not be communicated to proper personnel.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11591,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11591,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11592,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11592,11433,'Compensation and benefits are less than offered by other companies.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11592,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11592,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11593,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11593,11433,'Dishonest employees.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11593,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11593,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11594,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11594,11433,'Downtime as result from inadequate skilled labor.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11594,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11594,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11595,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11595,11433,'Eligible employees are improperly excluded from participation.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11595,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11595,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11596,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11596,11433,'Employee carelessness.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11596,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11596,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11597,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11597,11433,'Errors are made in calculating benefits.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11597,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11597,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11598,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11598,11433,'Hours are not authorized or are inaccurate.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11598,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11598,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11599,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11599,11433,'Human resource personnel are unaware of the records that must be retained to demonstrate compliance with applicable laws and regulations.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11599,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11599,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11600,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11600,11433,'Human resource records are not subject to proper security procedures.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11600,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11600,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11601,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11601,11433,'Improperly trained service personnel.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11601,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11601,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11602,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11602,11433,'Inaccurate employee information is provided to benefits personnel.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11602,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11602,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11603,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11603,11433,'Ineffective safety and employee training programs.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11603,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11603,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11604,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11604,11433,'Nonexistent employees are entered as program participants or beneficiaries.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11604,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11604,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11605,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11605,11433,'Not understanding moving of personnel.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11605,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11605,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11606,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11606,11433,'Over- or under qualified candidates may be hired.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11606,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11606,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11607,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11607,11433,'Plan benefit provisions are unclear or complex.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11607,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11607,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11608,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11608,11433,'Program eligibility requirements are not clearly communicated to appropriate personnel.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11608,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11608,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11609,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11609,11433,'Staff are not evaluated on regular or timely basis.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11609,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11609,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11610,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11610,11433,'The entity may be unaware of its future staffing needs.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11610,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11610,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11611,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11611,11433,'Time cards do not match with payroll register time (job time is not equal to shoptime).');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11611,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11611,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11612,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11612,11433,'Time cards or other source information is submitted for nonexistent employees.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11612,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11612,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11613,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11613,11433,'Lack of good personnel.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11613,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11613,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11614,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11614,11433,'Lack of qualified candidates.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11614,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11614,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11615,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11615,11433,'Unauthorized lay off of personnel.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11615,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11615,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11616,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11616,11433,'Unauthorized personnel may gain access to payroll information.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11616,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11616,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11617,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11617,11434,'Access to data, files, and programs are not controlled.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11617,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11617,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11618,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11618,11434,'Acquired assets may not be adequately described.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11618,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11618,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11619,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11619,11434,'Asset disposals or transfers may not be communicated to proper personnel.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11619,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11619,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11620,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11620,11434,'Computer operations fail to use correct programs, files and procedures.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11620,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11620,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11621,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11621,11434,'Data entry edit checks are not conducted.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11621,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11621,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11622,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11622,11434,'Discontinuity of business processes.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11622,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11622,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11623,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11623,11434,'Downtime as result of poorly maintained, misused or obsolete equipment.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11623,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11623,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11624,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11624,11434,'Inadequate information systems.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11624,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11624,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11625,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11625,11434,'Inadequate safeguarding of IT resources.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11625,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11625,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11626,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11626,11434,'Information system does not identify available discounts and related required payment dates.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11626,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11626,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11627,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11627,11434,'Information systems lack application controls.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11627,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11627,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11628,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11628,11434,'Information systems lack general controls.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11628,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11628,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11629,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11629,11434,'Organization has and/or is developing duplicative information system capabilities.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11629,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11629,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11630,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11630,11434,'Organization is developing information systems outside of the corporate architecture.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11630,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11630,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11631,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11631,11434,'Out-of-date systems.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11631,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11631,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11632,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11632,11434,'Poor back-up and recovery procedures.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11632,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11632,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11633,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11633,11434,'Product or processes needs are not effectively communicated to Technology Development.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11633,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11633,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11634,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11634,11434,'Programs are subjected to unauthorized modification.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11634,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11634,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11635,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11635,11434,'Purchase orders may be lost.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11635,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11635,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11636,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11636,11434,'Sales orders are lost.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11636,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11636,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11637,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11637,11434,'System and program modifications are implemented incorrectly.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11637,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11637,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11638,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11638,11434,'Systems are not designed according to user needs or are not properly implemented.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11638,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11638,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11639,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11639,11434,'Technology development management are unaware of project priorities.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11639,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11639,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11640,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11640,11434,'Technology Development personnel do not have technical ability to identify or develop appropriate technology.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11640,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11640,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11641,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11641,11434,'Technology Development personnel may acquire or have knowledge that would be useful in a development program other than that with which they are associated.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11641,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11641,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11642,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11642,11435,'Acquired assets may not be adequately described.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11642,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11642,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11643,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11643,11435,'Bad debts.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11643,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11643,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11644,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11644,11435,'Bills are paid before due dates.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11644,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11644,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11645,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11645,11435,'Cash received is diverted, lost or otherwise not reported accurately to accounts receivable.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11645,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11645,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11646,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11646,11435,'Checks clear the bank quickly.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11646,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11646,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11647,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11647,11435,'Equity risk.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11647,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11647,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11648,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11648,11435,'Excessive accounts receivable collection problems.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11648,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11648,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11649,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11649,11435,'Failure to establish or maintain appropriate relationships with financing sources.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11649,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11649,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11650,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11650,11435,'Handling cash receipts internally can delay deposit of such receipts.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11650,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11650,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11651,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11651,11435,'Inaccurate, untimely or unavailable information regarding amounts or due dates of payments.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11651,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11651,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11652,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11652,11435,'Inaccurate, untimely or unavailable information regarding payment due dates.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11652,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11652,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11653,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11653,11435,'Inadequate accounting systems for allocating costs.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11653,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11653,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11654,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11654,11435,'Inadequate physical security over cash and documents that can be used to transfer cash.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11654,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11654,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11655,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11655,11435,'Incomplete or inaccurate Information from order processing.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11655,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11655,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11656,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11656,11435,'Incomplete, untimely or inaccurate credit information.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11656,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11656,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11657,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11657,11435,'Incorrect depreciation lives or methods may be used.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11657,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11657,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11658,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11658,11435,'Information system does not identify available discounts and related required payment dates.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11658,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11658,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11659,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11659,11435,'Invalid accounts payable fraudulently created for unauthorized or nonexistent purchases.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11659,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11659,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11660,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11660,11435,'Lack of awareness regarding financing alternatives.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11660,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11660,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11661,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11661,11435,'Lack of knowledge regarding investment alternatives.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11661,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11661,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11662,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11662,11435,'Loan risk.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11662,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11662,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11663,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11663,11435,'Pay rates or deductions are not properly authorized or are inaccurate.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11663,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11663,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11664,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11664,11435,'Purchase orders may be lost.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11664,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11664,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11665,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11665,11435,'Sales orders are lost.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11665,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11665,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11666,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11666,11435,'Subsidiary ledger does not match with general ledger.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11666,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11666,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11667,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11667,11435,'System is not designed to reflect payment schedule included in collective bargaining agreements or individual agreements with employees.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11667,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11667,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11668,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11668,11435,'Unauthorized access to accounts payable records and stored data.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11668,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11668,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11669,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11669,11435,'Unauthorized access to accounts receivable records and stored data.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11669,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11669,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11670,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11670,11435,'Unauthorized additions to accounts payable.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11670,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11670,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11671,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11671,11435,'Unauthorized input for nonexistent returns.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11671,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11671,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11672,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11672,11435,'Unauthorized input for nonexistent returns, allowances and write-offs.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11672,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11672,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11673,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11673,11435,'Unauthorized personnel have access to financial information.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11673,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11673,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (11674,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (11674,11435,'Untimely processing of order information.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11674,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,11674,2902,NOW());

