
ALTER TABLE ci_nc ALTER COLUMN nseqnumber SET DEFAULT nextval('ci_nc_nseqnumber_seq'::regclass);
ALTER TABLE isms_context ALTER COLUMN pkcontext SET DEFAULT nextval('isms_context_pkcontext_seq'::regclass);
ALTER TABLE isms_context_classification ALTER COLUMN pkclassification SET DEFAULT nextval('isms_context_classification_pkclassification_seq'::regclass);
ALTER TABLE isms_context_history ALTER COLUMN pkid SET DEFAULT nextval('isms_context_history_pkid_seq'::regclass);
ALTER TABLE isms_non_conformity_types ALTER COLUMN pkclassification SET DEFAULT nextval('isms_non_conformity_types_pkclassification_seq'::regclass);
ALTER TABLE isms_saas ALTER COLUMN pkconfig SET DEFAULT nextval('isms_saas_pkconfig_seq'::regclass);
ALTER TABLE pm_doc_reference ALTER COLUMN pkreference SET DEFAULT nextval('pm_doc_reference_pkreference_seq'::regclass);
ALTER TABLE pm_instance_comment ALTER COLUMN pkcomment SET DEFAULT nextval('pm_instance_comment_pkcomment_seq'::regclass);
ALTER TABLE rm_parameter_name ALTER COLUMN pkparametername SET DEFAULT nextval('rm_parameter_name_pkparametername_seq'::regclass);
ALTER TABLE rm_parameter_value_name ALTER COLUMN pkvaluename SET DEFAULT nextval('rm_parameter_value_name_pkvaluename_seq'::regclass);
ALTER TABLE rm_rc_parameter_value_name ALTER COLUMN pkrcvaluename SET DEFAULT nextval('rm_rc_parameter_value_name_pkrcvaluename_seq'::regclass);
ALTER TABLE wkf_alert ALTER COLUMN pkalert SET DEFAULT nextval('wkf_alert_pkalert_seq'::regclass);
ALTER TABLE wkf_schedule ALTER COLUMN pkschedule SET DEFAULT nextval('wkf_schedule_pkschedule_seq'::regclass);
ALTER TABLE wkf_task ALTER COLUMN pktask SET DEFAULT nextval('wkf_task_pktask_seq'::regclass);
ALTER TABLE ONLY cm_threat ADD CONSTRAINT fkcontext UNIQUE (fkcontext);
ALTER TABLE ONLY ci_action_plan 				ADD CONSTRAINT pk_ci_action_plan PRIMARY KEY (fkcontext);
ALTER TABLE ONLY ci_category 					ADD CONSTRAINT pk_ci_category PRIMARY KEY (fkcontext);
ALTER TABLE ONLY ci_incident 					ADD CONSTRAINT pk_ci_incident PRIMARY KEY (fkcontext);
ALTER TABLE ONLY ci_incident_control 			ADD CONSTRAINT pk_ci_incident_control PRIMARY KEY (fkincident, fkcontrol, fkcontext);
ALTER TABLE ONLY ci_incident_financial_impact 	ADD CONSTRAINT pk_ci_incident_financial_impac PRIMARY KEY (fkincident, fkclassification);
ALTER TABLE ONLY ci_incident_process 			ADD CONSTRAINT pk_ci_incident_process PRIMARY KEY (fkprocess, fkincident, fkcontext);
ALTER TABLE ONLY ci_incident_risk 				ADD CONSTRAINT pk_ci_incident_risk PRIMARY KEY (fkcontext);
ALTER TABLE ONLY ci_incident_risk_value 		ADD CONSTRAINT pk_ci_incident_risk_value PRIMARY KEY (fkparametername, fkvaluename, fkincrisk);
ALTER TABLE ONLY ci_incident_user 				ADD CONSTRAINT pk_ci_incident_user PRIMARY KEY (fkuser, fkincident);
ALTER TABLE ONLY ci_nc 							ADD CONSTRAINT pk_ci_nc PRIMARY KEY (fkcontext);
ALTER TABLE ONLY ci_nc_action_plan 				ADD CONSTRAINT pk_ci_nc_action_plan PRIMARY KEY (fknc, fkactionplan);
ALTER TABLE ONLY ci_nc_process 					ADD CONSTRAINT pk_ci_nc_process PRIMARY KEY (fknc, fkprocess);
ALTER TABLE ONLY ci_nc_seed 					ADD CONSTRAINT pk_ci_nc_seed PRIMARY KEY (fkcontrol, ndeactivationreason);
ALTER TABLE ONLY ci_occurrence 					ADD CONSTRAINT pk_ci_occurrence PRIMARY KEY (fkcontext);
ALTER TABLE ONLY ci_risk_probability 			ADD CONSTRAINT pk_ci_risk_probability PRIMARY KEY (fkrisk, fkvaluename);
ALTER TABLE ONLY ci_risk_schedule 				ADD CONSTRAINT pk_ci_risk_schedule PRIMARY KEY (fkrisk);
ALTER TABLE ONLY ci_solution 					ADD CONSTRAINT pk_ci_solution PRIMARY KEY (fkcontext);

ALTER TABLE ONLY cm_area_resource               ADD CONSTRAINT pk_cm_area_resource_cat PRIMARY KEY (fkcontext);
ALTER TABLE ONLY cm_asset_importance            ADD CONSTRAINT pk_cm_asset_importance_cat PRIMARY KEY (fkcontext);
ALTER TABLE ONLY cm_asset_threat		        ADD CONSTRAINT pk_cm_asset_threat_cat PRIMARY KEY (fkcontext);
ALTER TABLE ONLY cm_damage_matrix		        ADD CONSTRAINT pk_cm_damage_matrix_cat PRIMARY KEY (fkcontext);
ALTER TABLE ONLY cm_damage_matrix_parameter		ADD CONSTRAINT pk_cm_damage_matrix_parameter_cat PRIMARY KEY (fkcontext);
ALTER TABLE ONLY cm_department		            ADD CONSTRAINT pk_cm_department_cat PRIMARY KEY (fkcontext);
ALTER TABLE ONLY cm_financial_impact		    ADD CONSTRAINT pk_cm_financial_impact_cat PRIMARY KEY (fkcontext);
ALTER TABLE ONLY cm_group		                ADD CONSTRAINT pk_cm_group_cat PRIMARY KEY (fkcontext);
ALTER TABLE ONLY cm_group_resource		        ADD CONSTRAINT pk_cm_group_resource_cat PRIMARY KEY (fkcontext);
ALTER TABLE ONLY cm_impact		                ADD CONSTRAINT pk_cm_impact_cat PRIMARY KEY (fkcontext);
ALTER TABLE ONLY cm_impact_damage		        ADD CONSTRAINT pk_cm_impact_damage_cat PRIMARY KEY (fkcontext);
ALTER TABLE ONLY cm_impact_damage_range		    ADD CONSTRAINT pk_cm_impact_damage_range_cat PRIMARY KEY (fkcontext);
ALTER TABLE ONLY cm_impact_type		            ADD CONSTRAINT pk_cm_impact_type_cat PRIMARY KEY (fkcontext);
ALTER TABLE ONLY cm_place		                ADD CONSTRAINT pk_cm_place_cat PRIMARY KEY (fkcontext);
ALTER TABLE ONLY cm_place_area		            ADD CONSTRAINT pk_cm_place_area_cat PRIMARY KEY (fkcontext);
ALTER TABLE ONLY cm_place_asset	                ADD CONSTRAINT pk_cm_place_asset_cat PRIMARY KEY (fkcontext);
ALTER TABLE ONLY cm_place_process		        ADD CONSTRAINT pk_cm_place_process_cat PRIMARY KEY (fkcontext);
ALTER TABLE ONLY cm_place_provider		        ADD CONSTRAINT pk_cm_place_provider_cat PRIMARY KEY (fkcontext);
ALTER TABLE ONLY cm_place_resource		        ADD CONSTRAINT pk_cm_place_resource_cat PRIMARY KEY (fkcontext);
ALTER TABLE ONLY cm_place_threat		        ADD CONSTRAINT pk_cm_place_threat_cat PRIMARY KEY (fkcontext);
ALTER TABLE ONLY cm_plan		                ADD CONSTRAINT pk_cm_plan_cat PRIMARY KEY (fkcontext);
ALTER TABLE ONLY cm_plan_action		            ADD CONSTRAINT pk_cm_plan_action_cat PRIMARY KEY (fkcontext);
ALTER TABLE ONLY cm_plan_action_test		    ADD CONSTRAINT pk_cm_plan_action_test_cat PRIMARY KEY (fkcontext);
ALTER TABLE ONLY cm_plan_range		            ADD CONSTRAINT pk_cm_plan_range_cat PRIMARY KEY (fkcontext);
ALTER TABLE ONLY cm_plan_range_type		        ADD CONSTRAINT pk_cm_plan_range_type_cat PRIMARY KEY (fkcontext);
ALTER TABLE ONLY cm_plan_schedule		        ADD CONSTRAINT pk_cm_plan_schedule_cat PRIMARY KEY (fkcontext);
ALTER TABLE ONLY cm_plan_test		            ADD CONSTRAINT pk_cm_plan_test_cat PRIMARY KEY (fkcontext);
ALTER TABLE ONLY cm_priority		            ADD CONSTRAINT pk_cm_priority_cat PRIMARY KEY (fkcontext);
ALTER TABLE ONLY cm_process_activity		    ADD CONSTRAINT pk_cm_process_activity_cat PRIMARY KEY (fkcontext);
ALTER TABLE ONLY cm_process_activity_asset		ADD CONSTRAINT pk_cm_process_activity_asset_cat PRIMARY KEY (fkcontext);
ALTER TABLE ONLY cm_process_activity_people     ADD CONSTRAINT pk_cm_process_activity_people_cat PRIMARY KEY (fkcontext);
ALTER TABLE ONLY cm_process_inputs		        ADD CONSTRAINT pk_cm_process_inputs_cat PRIMARY KEY (fkcontext);
ALTER TABLE ONLY cm_process_outputs		        ADD CONSTRAINT pk_cm_process_outputs_cat PRIMARY KEY (fkcontext);
ALTER TABLE ONLY cm_process_threat		        ADD CONSTRAINT pk_cm_process_threat_cat PRIMARY KEY (fkcontext);
ALTER TABLE ONLY cm_provider		            ADD CONSTRAINT pk_cm_provider_cat PRIMARY KEY (fkcontext);
ALTER TABLE ONLY cm_provider_resource		    ADD CONSTRAINT pk_cm_provider_resource_cat PRIMARY KEY (fkcontext);
ALTER TABLE ONLY cm_resource		            ADD CONSTRAINT pk_cm_resource_cat PRIMARY KEY (fkcontext);
ALTER TABLE ONLY cm_scene		                ADD CONSTRAINT pk_cm_scene_cat PRIMARY KEY (fkcontext);
ALTER TABLE ONLY cm_scope		                ADD CONSTRAINT pk_cm_scope_cat PRIMARY KEY (fkcontext);
ALTER TABLE ONLY cm_scope_area		            ADD CONSTRAINT pk_cm_scope_area_cat PRIMARY KEY (fkcontext);
ALTER TABLE ONLY cm_scope_asset		            ADD CONSTRAINT pk_cm_scope_asset_cat PRIMARY KEY (fkcontext);
ALTER TABLE ONLY cm_scope_place		            ADD CONSTRAINT pk_cm_scope_place_cat PRIMARY KEY (fkcontext);
ALTER TABLE ONLY cm_scope_process		        ADD CONSTRAINT pk_cm_scope_process_cat PRIMARY KEY (fkcontext);
ALTER TABLE ONLY cm_threat		                ADD CONSTRAINT pk_cm_threat_cat PRIMARY KEY (fkcontext);
ALTER TABLE ONLY cm_threat_probability          ADD CONSTRAINT pk_cm_threat_probability_cat PRIMARY KEY (fkcontext);

ALTER TABLE ONLY cm_asset_category 				ADD CONSTRAINT pk_cm_asset_category_cat PRIMARY KEY (fkcontext);
ALTER TABLE ONLY cm_asset_category_threat 		ADD CONSTRAINT pk_cm_asset_category_threat_cat_thr PRIMARY KEY (fkcontext);
ALTER TABLE ONLY cm_place_category 				ADD CONSTRAINT pk_cm_place_category_cat PRIMARY KEY (fkcontext);
ALTER TABLE ONLY cm_place_category_threat 		ADD CONSTRAINT pk_cm_place_category_threat_cat_thr PRIMARY KEY (fkcontext);
ALTER TABLE ONLY cm_process_category 			ADD CONSTRAINT pk_cm_process_category_cat PRIMARY KEY (fkcontext);
ALTER TABLE ONLY cm_process_category_threat 	ADD CONSTRAINT pk_cm_process_category_threat_cat_thr PRIMARY KEY (fkcontext);
ALTER TABLE ONLY cm_threat_vulnerability 		ADD CONSTRAINT pkcontext PRIMARY KEY (fkcontext);

ALTER TABLE ONLY isms_config 					ADD CONSTRAINT pk_isms_config PRIMARY KEY (pkconfig);
ALTER TABLE ONLY isms_context 					ADD CONSTRAINT pk_isms_context PRIMARY KEY (pkcontext);
ALTER TABLE ONLY isms_context_classification 	ADD CONSTRAINT pk_isms_context_classification PRIMARY KEY (pkclassification);
ALTER TABLE ONLY isms_context_date 				ADD CONSTRAINT pk_isms_context_date PRIMARY KEY (fkcontext, naction);
ALTER TABLE ONLY isms_context_history 			ADD CONSTRAINT pk_isms_context_history PRIMARY KEY (pkid);
ALTER TABLE ONLY isms_non_conformity_types 		ADD CONSTRAINT pk_isms_non_conformity_types PRIMARY KEY (pkclassification);
ALTER TABLE ONLY isms_policy 					ADD CONSTRAINT pk_isms_policy PRIMARY KEY (fkcontext);
ALTER TABLE ONLY isms_profile 					ADD CONSTRAINT pk_isms_profile PRIMARY KEY (fkcontext);
ALTER TABLE ONLY isms_profile_acl 				ADD CONSTRAINT pk_isms_profile_acl PRIMARY KEY (fkprofile, stag);
ALTER TABLE ONLY isms_saas 						ADD CONSTRAINT pk_isms_saas PRIMARY KEY (pkconfig);
ALTER TABLE ONLY isms_scope 					ADD CONSTRAINT pk_isms_scope PRIMARY KEY (fkcontext);
ALTER TABLE ONLY isms_solicitor 				ADD CONSTRAINT pk_isms_solicitor PRIMARY KEY (fkuser, fksolicitor);
ALTER TABLE ONLY isms_user 						ADD CONSTRAINT pk_isms_user PRIMARY KEY (fkcontext);
ALTER TABLE ONLY isms_user_preference 			ADD CONSTRAINT pk_isms_user_preference PRIMARY KEY (fkuser, spreference);
ALTER TABLE ONLY wkf_task_metadata 				ADD CONSTRAINT pk_metadata PRIMARY KEY (pktaskmetadata);
ALTER TABLE ONLY pm_doc_approvers 				ADD CONSTRAINT pk_pm_doc_approvers PRIMARY KEY (fkdocument, fkuser);
ALTER TABLE ONLY pm_doc_context 				ADD CONSTRAINT pk_pm_doc_context PRIMARY KEY (fkcontext, fkdocument);
ALTER TABLE ONLY pm_doc_instance 				ADD CONSTRAINT pk_pm_doc_instance PRIMARY KEY (fkcontext);
ALTER TABLE ONLY pm_doc_read_history 			ADD CONSTRAINT pk_pm_doc_read_history PRIMARY KEY (ddate, fkinstance, fkuser);
ALTER TABLE ONLY pm_doc_readers 				ADD CONSTRAINT pk_pm_doc_readers PRIMARY KEY (fkdocument, fkuser);
ALTER TABLE ONLY pm_doc_reference 				ADD CONSTRAINT pk_pm_doc_reference PRIMARY KEY (pkreference);
ALTER TABLE ONLY pm_doc_registers 				ADD CONSTRAINT pk_pm_doc_registers PRIMARY KEY (fkregister, fkdocument);
ALTER TABLE ONLY pm_document 					ADD CONSTRAINT pk_pm_document PRIMARY KEY (fkcontext);
ALTER TABLE ONLY pm_document_revision_history 	ADD CONSTRAINT pk_pm_document_revision_histor PRIMARY KEY (fkdocument, ddaterevision);
ALTER TABLE ONLY pm_instance_comment 			ADD CONSTRAINT pk_pm_instance_comment PRIMARY KEY (pkcomment);
ALTER TABLE ONLY pm_instance_content 			ADD CONSTRAINT pk_pm_instance_content PRIMARY KEY (fkinstance);
ALTER TABLE ONLY pm_process_user 				ADD CONSTRAINT pk_pm_process_user PRIMARY KEY (fkprocess, fkuser);
ALTER TABLE ONLY pm_register 					ADD CONSTRAINT pk_pm_register PRIMARY KEY (fkcontext);
ALTER TABLE ONLY pm_register_readers 			ADD CONSTRAINT pk_pm_register_readers PRIMARY KEY (fkuser, fkregister);
ALTER TABLE ONLY pm_template 					ADD CONSTRAINT pk_pm_template PRIMARY KEY (fkcontext);
ALTER TABLE ONLY pm_template_best_practice 		ADD CONSTRAINT pk_pm_template_best_practice PRIMARY KEY (fktemplate, fkbestpractice);
ALTER TABLE ONLY pm_template_content 			ADD CONSTRAINT pk_pm_template_content PRIMARY KEY (fkcontext);
ALTER TABLE ONLY rm_area 						ADD CONSTRAINT pk_rm_area PRIMARY KEY (fkcontext);
ALTER TABLE ONLY rm_asset 						ADD CONSTRAINT pk_rm_asset PRIMARY KEY (fkcontext);
ALTER TABLE ONLY rm_asset_asset 				ADD CONSTRAINT pk_rm_asset_asset PRIMARY KEY (fkasset, fkdependent);
ALTER TABLE ONLY rm_asset_value 				ADD CONSTRAINT pk_rm_asset_value PRIMARY KEY (fkasset, fkvaluename, fkparametername);
ALTER TABLE ONLY rm_best_practice 				ADD CONSTRAINT pk_rm_best_practice PRIMARY KEY (fkcontext);
ALTER TABLE ONLY rm_best_practice_event 		ADD CONSTRAINT pk_rm_best_practice_event PRIMARY KEY (fkcontext);
ALTER TABLE ONLY rm_best_practice_standard 		ADD CONSTRAINT pk_rm_best_practice_standard PRIMARY KEY (fkcontext);
ALTER TABLE ONLY rm_category 					ADD CONSTRAINT pk_rm_category PRIMARY KEY (fkcontext);
ALTER TABLE ONLY rm_control 					ADD CONSTRAINT pk_rm_control PRIMARY KEY (fkcontext);
ALTER TABLE ONLY rm_control_best_practice 		ADD CONSTRAINT pk_rm_control_best_practice PRIMARY KEY (fkcontext);
ALTER TABLE ONLY rm_control_cost 				ADD CONSTRAINT pk_rm_control_cost PRIMARY KEY (fkcontrol);
ALTER TABLE ONLY rm_control_efficiency_history 	ADD CONSTRAINT pk_rm_control_efficiency_histo PRIMARY KEY (fkcontrol, ddatetodo);
ALTER TABLE ONLY rm_control_test_history 		ADD CONSTRAINT pk_rm_control_test_history PRIMARY KEY (fkcontrol, ddatetodo);
ALTER TABLE ONLY rm_event 						ADD CONSTRAINT pk_rm_event PRIMARY KEY (fkcontext);
ALTER TABLE ONLY rm_parameter_name 				ADD CONSTRAINT pk_rm_parameter_name PRIMARY KEY (pkparametername);
ALTER TABLE ONLY rm_parameter_value_name 		ADD CONSTRAINT pk_rm_parameter_value_name PRIMARY KEY (pkvaluename);
ALTER TABLE ONLY rm_process 					ADD CONSTRAINT pk_rm_process PRIMARY KEY (fkcontext);
ALTER TABLE ONLY rm_process_asset 				ADD CONSTRAINT pk_rm_process_asset PRIMARY KEY (fkcontext);
ALTER TABLE ONLY rm_rc_parameter_value_name 	ADD CONSTRAINT pk_rm_rc_parameter_value_name PRIMARY KEY (pkrcvaluename);
ALTER TABLE ONLY rm_risk 						ADD CONSTRAINT pk_rm_risk PRIMARY KEY (fkcontext);
ALTER TABLE ONLY rm_risk_control 				ADD CONSTRAINT pk_rm_risk_control PRIMARY KEY (fkcontext);
ALTER TABLE ONLY rm_risk_control_value 			ADD CONSTRAINT pk_rm_risk_control_value PRIMARY KEY (fkriskcontrol, fkparametername, fkrcvaluename);
ALTER TABLE ONLY rm_risk_limits 				ADD CONSTRAINT pk_rm_risk_limits PRIMARY KEY (fkcontext);
ALTER TABLE ONLY rm_risk_value 					ADD CONSTRAINT pk_rm_risk_value PRIMARY KEY (fkrisk, fkvaluename, fkparametername);
ALTER TABLE ONLY rm_section_best_practice 		ADD CONSTRAINT pk_rm_section_best_practice PRIMARY KEY (fkcontext);
ALTER TABLE ONLY rm_standard 					ADD CONSTRAINT pk_rm_standard PRIMARY KEY (fkcontext);
ALTER TABLE ONLY wkf_alert 						ADD CONSTRAINT pk_wkf_alert PRIMARY KEY (pkalert);
ALTER TABLE ONLY wkf_control_efficiency 		ADD CONSTRAINT pk_wkf_control_efficiency PRIMARY KEY (fkcontrolefficiency);
ALTER TABLE ONLY wkf_control_test 				ADD CONSTRAINT pk_wkf_control_test PRIMARY KEY (fkcontroltest);
ALTER TABLE ONLY wkf_schedule 					ADD CONSTRAINT pk_wkf_schedule PRIMARY KEY (pkschedule);
ALTER TABLE ONLY wkf_task 						ADD CONSTRAINT pk_wkf_task PRIMARY KEY (pktask);
ALTER TABLE ONLY wkf_task_schedule 				ADD CONSTRAINT pk_wkf_task_schedule PRIMARY KEY (fkcontext, nactivity);

CREATE UNIQUE INDEX c_scope_fkcontext_unique ON cm_scope USING btree (fkcontext);
CREATE UNIQUE INDEX ci_action_plan_pk ON ci_action_plan USING btree (fkcontext);
CREATE UNIQUE INDEX ci_category_pk ON ci_category USING btree (fkcontext);
CREATE UNIQUE INDEX ci_incident_control_pk ON ci_incident_control USING btree (fkincident, fkcontrol, fkcontext);
CREATE UNIQUE INDEX ci_incident_financial_impact_pk ON ci_incident_financial_impact USING btree (fkincident, fkclassification);
CREATE UNIQUE INDEX ci_incident_pk ON ci_incident USING btree (fkcontext);
CREATE UNIQUE INDEX ci_incident_process_pk ON ci_incident_process USING btree (fkprocess, fkincident, fkcontext);
CREATE UNIQUE INDEX ci_incident_risk_pk2 ON ci_incident_risk USING btree (fkcontext);
CREATE UNIQUE INDEX ci_incident_risk_value_pk ON ci_incident_risk_value USING btree (fkparametername, fkvaluename, fkincrisk);
CREATE UNIQUE INDEX ci_incident_user_pk ON ci_incident_user USING btree (fkuser, fkincident);
CREATE UNIQUE INDEX ci_nc_action_plan_pk ON ci_nc_action_plan USING btree (fknc, fkactionplan);
CREATE UNIQUE INDEX ci_nc_pk ON ci_nc USING btree (fkcontext);
CREATE UNIQUE INDEX ci_nc_process_pk ON ci_nc_process USING btree (fknc, fkprocess);
CREATE UNIQUE INDEX ci_nc_seed_pk ON ci_nc_seed USING btree (fkcontrol, ndeactivationreason);
CREATE UNIQUE INDEX ci_ocurrence_pk ON ci_occurrence USING btree (fkcontext);
CREATE UNIQUE INDEX ci_risk_schedule_pk ON ci_risk_schedule USING btree (fkrisk);
CREATE UNIQUE INDEX ci_risk_value_pk ON ci_risk_probability USING btree (fkrisk, fkvaluename);
CREATE UNIQUE INDEX ci_solution_pk ON ci_solution USING btree (fkcontext);
CREATE UNIQUE INDEX cm_damage_matrix_fkcontext_unique ON cm_damage_matrix USING btree (fkcontext);
CREATE UNIQUE INDEX cm_damage_matrix_parameter_fkcontext_unique ON cm_damage_matrix_parameter USING btree (fkcontext);
CREATE UNIQUE INDEX cm_fkcontext_importance ON cm_asset_importance USING btree (fkcontext);
CREATE UNIQUE INDEX cm_group_fkcontext_unique ON cm_group USING btree (fkcontext);
CREATE UNIQUE INDEX cm_group_resource_fkcontext_unique ON cm_group_resource USING btree (fkcontext);
CREATE UNIQUE INDEX cm_impact_damage_fkcontext_unique ON cm_impact_damage USING btree (fkcontext);
CREATE UNIQUE INDEX cm_impact_damage_range_fkcontext_unique ON cm_impact_damage_range USING btree (fkcontext);
CREATE UNIQUE INDEX cm_impact_fkcontext_unique ON cm_impact USING btree (fkcontext);
CREATE UNIQUE INDEX cm_provider_fkcontext_unique ON cm_provider USING btree (fkcontext);
CREATE UNIQUE INDEX cm_provider_resource_fkcontext_unique ON cm_provider_resource USING btree (fkcontext);
CREATE UNIQUE INDEX cm_resource_fkcontext_unique ON cm_resource USING btree (fkcontext);
CREATE UNIQUE INDEX cm_threat_fkcontext ON cm_threat_probability USING btree (fkcontext);
CREATE INDEX fkactionplan_fk ON ci_nc_action_plan USING btree (fkactionplan);
CREATE INDEX fkarea_fk ON rm_process USING btree (fkarea);
CREATE INDEX fkasset_fk ON rm_process_asset USING btree (fkasset);
CREATE INDEX fkasset_fk2 ON rm_risk USING btree (fkasset);
CREATE INDEX fkasset_fk3 ON rm_asset_value USING btree (fkasset);
CREATE INDEX fkasset_fk4 ON rm_asset_asset USING btree (fkasset);
CREATE INDEX fkauthor_fk ON pm_document USING btree (fkauthor);
CREATE INDEX fkbestpractice_fk ON rm_control_best_practice USING btree (fkbestpractice);
CREATE INDEX fkbestpractice_fk2 ON rm_best_practice_standard USING btree (fkbestpractice);
CREATE INDEX fkbestpractice_fk3 ON rm_best_practice_event USING btree (fkbestpractice);
CREATE INDEX fkbestpractice_fk4 ON pm_template_best_practice USING btree (fkbestpractice);
CREATE INDEX fkcategory_fk2 ON rm_event USING btree (fkcategory);
CREATE INDEX fkcategory_fk3 ON ci_incident USING btree (fkcategory);
CREATE INDEX fkcategory_fk4 ON ci_solution USING btree (fkcategory);
CREATE INDEX fkclassification_fk ON pm_document USING btree (fkclassification);
CREATE INDEX fkclassification_fk2 ON pm_register USING btree (fkclassification);
CREATE INDEX fkclassification_fk3 ON ci_incident_financial_impact USING btree (fkclassification);
CREATE INDEX fkcontext_fk ON wkf_task USING btree (fkcontext);
CREATE INDEX fkcontext_fk2 ON wkf_alert USING btree (fkcontext);
CREATE INDEX fkcontext_fk3 ON isms_context_date USING btree (fkcontext);
CREATE INDEX fkcontext_fk4 ON pm_doc_context USING btree (fkcontext);
CREATE INDEX fkcontext_fk5 ON isms_context_history USING btree (fkcontext);
CREATE INDEX fkcontext_fk6 ON ci_incident_control USING btree (fkcontext);
CREATE INDEX fkcontext_fk7 ON wkf_task_schedule USING btree (fkcontext);
CREATE UNIQUE INDEX fkcontext_process_activity ON cm_process_activity USING btree (fkcontext);
CREATE UNIQUE INDEX fkcontext_unique ON cm_place USING btree (fkcontext);
CREATE UNIQUE INDEX fkcontext_unique_threat_vulnerability ON cm_threat_vulnerability USING btree (fkcontext);
CREATE INDEX fkcontrol_fk ON rm_risk_control USING btree (fkcontrol);
CREATE INDEX fkcontrol_fk2 ON rm_control_best_practice USING btree (fkcontrol);
CREATE INDEX fkcontrol_fk3 ON rm_control_efficiency_history USING btree (fkcontrol);
CREATE INDEX fkcontrol_fk4 ON rm_control_test_history USING btree (fkcontrol);
CREATE INDEX fkcontrol_fk5 ON ci_nc USING btree (fkcontrol);
CREATE INDEX fkcontrol_fk6 ON ci_incident_control USING btree (fkcontrol);
CREATE INDEX fkcontrol_fk7 ON ci_nc_seed USING btree (fkcontrol);
CREATE INDEX fkcreator_fk ON wkf_task USING btree (fkcreator);
CREATE INDEX fkcreator_fk2 ON wkf_alert USING btree (fkcreator);
CREATE INDEX fkcurrentversion_fk ON pm_document USING btree (fkcurrentversion);
CREATE INDEX fkdependent_fk ON rm_asset_asset USING btree (fkdependent);
CREATE INDEX fkdocument_fk ON pm_doc_approvers USING btree (fkdocument);
CREATE INDEX fkdocument_fk2 ON pm_doc_instance USING btree (fkdocument);
CREATE INDEX fkdocument_fk3 ON pm_doc_reference USING btree (fkdocument);
CREATE INDEX fkdocument_fk4 ON pm_doc_readers USING btree (fkdocument);
CREATE INDEX fkdocument_fk5 ON pm_document_revision_history USING btree (fkdocument);
CREATE INDEX fkdocument_fk6 ON pm_doc_registers USING btree (fkdocument);
CREATE INDEX fkdocument_fk7 ON pm_register USING btree (fkdocument);
CREATE INDEX fkdocument_fk8 ON pm_doc_context USING btree (fkdocument);
CREATE INDEX fkevent_fk ON rm_risk USING btree (fkevent);
CREATE INDEX fkevent_fk2 ON rm_best_practice_event USING btree (fkevent);
CREATE INDEX fkincident_fk ON ci_occurrence USING btree (fkincident);
CREATE INDEX fkincident_fk2 ON ci_incident_user USING btree (fkincident);
CREATE INDEX fkincident_fk4 ON ci_incident_control USING btree (fkincident);
CREATE INDEX fkincident_fk5 ON ci_incident_process USING btree (fkincident);
CREATE INDEX fkincident_fk6 ON ci_incident_financial_impact USING btree (fkincident);
CREATE INDEX fkincident_fk7 ON ci_incident_risk USING btree (fkincident);
CREATE INDEX fkincrisk_fk ON ci_incident_risk_value USING btree (fkincrisk);
CREATE INDEX fkinstance_fk ON pm_instance_comment USING btree (fkinstance);
CREATE INDEX fkmainappover_fk ON pm_document USING btree (fkmainapprover);
CREATE INDEX fknc_fk ON ci_nc_process USING btree (fknc);
CREATE INDEX fknc_fk2 ON ci_nc_action_plan USING btree (fknc);
CREATE INDEX fkparametername_fk ON rm_asset_value USING btree (fkparametername);
CREATE INDEX fkparametername_fk2 ON rm_risk_value USING btree (fkparametername);
CREATE INDEX fkparametername_fk3 ON rm_risk_control_value USING btree (fkparametername);
CREATE INDEX fkparametername_fk4 ON ci_incident_risk_value USING btree (fkparametername);
CREATE INDEX fkparamter_fk ON ci_risk_probability USING btree (fkvaluename);
CREATE INDEX fkparent_fk ON rm_area USING btree (fkparent);
CREATE INDEX fkparent_fk2 ON rm_category USING btree (fkparent);
CREATE INDEX fkparent_fk3 ON rm_section_best_practice USING btree (fkparent);
CREATE INDEX fkparent_fk5 ON pm_document USING btree (fkparent);
CREATE INDEX fkpriority_fk ON rm_area USING btree (fkpriority);
CREATE INDEX fkprobabilityvaluename_fk ON rm_risk USING btree (fkprobabilityvaluename);
CREATE INDEX fkprobabilityvaluename_fk2 ON rm_risk_control USING btree (fkprobabilityvaluename);
CREATE INDEX fkprocess_fk ON rm_process_asset USING btree (fkprocess);
CREATE INDEX fkprocess_fk2 ON pm_process_user USING btree (fkprocess);
CREATE INDEX fkprocess_fk3 ON ci_nc_process USING btree (fkprocess);
CREATE INDEX fkprocess_fk4 ON ci_incident_process USING btree (fkprocess);
CREATE INDEX fkprofile_fk ON isms_user USING btree (fkprofile);
CREATE INDEX fkprofile_fk2 ON isms_profile_acl USING btree (fkprofile);
CREATE INDEX fkrcvaluename_fk ON rm_risk_control_value USING btree (fkrcvaluename);
CREATE INDEX fkreceiver_fk ON wkf_task USING btree (fkreceiver);
CREATE INDEX fkreceiver_fk2 ON wkf_alert USING btree (fkreceiver);
CREATE INDEX fkregister_fk ON pm_doc_registers USING btree (fkregister);
CREATE INDEX fkregister_fk2 ON pm_register_readers USING btree (fkregister);
CREATE INDEX fkresponsible_fk3 ON rm_control USING btree (fkresponsible);
CREATE INDEX fkresponsible_fk5 ON pm_register USING btree (fkresponsible);
CREATE INDEX fkresponsible_fk6 ON ci_incident USING btree (fkresponsible);
CREATE INDEX fkresponsible_fk7 ON ci_nc USING btree (fkresponsible);
CREATE INDEX fkresponsible_fk8 ON ci_action_plan USING btree (fkresponsible);
CREATE INDEX fkrisk_fk ON rm_risk_control USING btree (fkrisk);
CREATE INDEX fkrisk_fk2 ON rm_risk_value USING btree (fkrisk);
CREATE INDEX fkrisk_fk4 ON ci_risk_probability USING btree (fkrisk);
CREATE INDEX fkrisk_fk5 ON ci_incident_risk USING btree (fkrisk);
CREATE INDEX fkriskcontrol_fk ON rm_risk_control_value USING btree (fkriskcontrol);
CREATE INDEX fkschedule_fk ON wkf_control_efficiency USING btree (fkschedule);
CREATE INDEX fkschedule_fk2 ON wkf_control_test USING btree (fkschedule);
CREATE INDEX fkschedule_fk3 ON wkf_task_schedule USING btree (fkschedule);
CREATE INDEX fkschedule_fk4 ON pm_document USING btree (fkschedule);
CREATE INDEX fksectionbestpractice_fk ON rm_best_practice USING btree (fksectionbestpractice);
CREATE INDEX fksender_fk ON ci_nc USING btree (fksender);
CREATE INDEX fksolicitor_fk ON isms_solicitor USING btree (fksolicitor);
CREATE INDEX fkstandard_fk ON rm_best_practice_standard USING btree (fkstandard);
CREATE INDEX fktemplate_fk ON pm_template_best_practice USING btree (fktemplate);
CREATE INDEX fktype_fk ON rm_area USING btree (fktype);
CREATE INDEX fktype_fk3 ON rm_control USING btree (fktype);
CREATE INDEX fktype_fk4 ON rm_risk USING btree (fktype);
CREATE INDEX fktype_fk5 ON rm_event USING btree (fktype);
CREATE INDEX fkuser_fk ON isms_user_preference USING btree (fkuser);
CREATE INDEX fkuser_fk2 ON isms_solicitor USING btree (fkuser);
CREATE INDEX fkuser_fk3 ON pm_doc_approvers USING btree (fkuser);
CREATE INDEX fkuser_fk4 ON pm_instance_comment USING btree (fkuser);
CREATE INDEX fkuser_fk5 ON pm_doc_readers USING btree (fkuser);
CREATE INDEX fkuser_fk6 ON pm_process_user USING btree (fkuser);
CREATE INDEX fkuser_fk7 ON pm_doc_read_history USING btree (fkuser);
CREATE INDEX fkuser_fk8 ON ci_incident_user USING btree (fkuser);
CREATE INDEX fkuser_fk9 ON pm_register_readers USING btree (fkuser);
CREATE INDEX fkvaluename_fk ON rm_asset_value USING btree (fkvaluename);
CREATE INDEX fkvaluename_fk2 ON rm_risk_value USING btree (fkvaluename);
CREATE INDEX fkvaluename_fk3 ON ci_incident_risk_value USING btree (fkvaluename);
CREATE UNIQUE INDEX isms_config_pk ON isms_config USING btree (pkconfig);
CREATE UNIQUE INDEX isms_context_classification_pk ON isms_context_classification USING btree (pkclassification);
CREATE UNIQUE INDEX isms_context_date_pk ON isms_context_date USING btree (fkcontext, naction);
CREATE UNIQUE INDEX isms_context_history_pk ON isms_context_history USING btree (pkid);
CREATE UNIQUE INDEX isms_context_pk ON isms_context USING btree (pkcontext);
CREATE UNIQUE INDEX isms_non_conformity_types_pk ON isms_non_conformity_types USING btree (pkclassification);
CREATE UNIQUE INDEX isms_policy_pk ON isms_policy USING btree (fkcontext);
CREATE UNIQUE INDEX isms_profile_acl_pk ON isms_profile_acl USING btree (fkprofile, stag);
CREATE UNIQUE INDEX isms_profile_pk ON isms_profile USING btree (fkcontext);
CREATE UNIQUE INDEX isms_saas_pk ON isms_saas USING btree (pkconfig);
CREATE UNIQUE INDEX isms_scope_pk ON isms_scope USING btree (fkcontext);
CREATE UNIQUE INDEX isms_solicitor_pk ON isms_solicitor USING btree (fkuser, fksolicitor);
CREATE UNIQUE INDEX isms_user_pk ON isms_user USING btree (fkcontext);
CREATE UNIQUE INDEX isms_user_preference_pk ON isms_user_preference USING btree (fkuser, spreference);
CREATE UNIQUE INDEX pm_doc_instance_pk ON pm_doc_instance USING btree (fkcontext);
CREATE UNIQUE INDEX pm_doc_read_history_pk ON pm_doc_read_history USING btree (ddate, fkinstance, fkuser);
CREATE UNIQUE INDEX pm_doc_reference_pk ON pm_doc_reference USING btree (pkreference);
CREATE UNIQUE INDEX pm_document_pk ON pm_document USING btree (fkcontext);
CREATE UNIQUE INDEX pm_document_revision_history_pk ON pm_document_revision_history USING btree (fkdocument, ddaterevision);
CREATE UNIQUE INDEX pm_instance_comment_pk ON pm_instance_comment USING btree (pkcomment);
CREATE UNIQUE INDEX pm_instance_content_pk ON pm_instance_content USING btree (fkinstance);
CREATE UNIQUE INDEX pm_register_pk ON pm_register USING btree (fkcontext);
CREATE UNIQUE INDEX pm_register_readers_pk ON pm_register_readers USING btree (fkuser, fkregister);
CREATE UNIQUE INDEX pm_template_best_practice_pk ON pm_template_best_practice USING btree (fktemplate, fkbestpractice);
CREATE UNIQUE INDEX pm_template_content_pk ON pm_template_content USING btree (fkcontext);
CREATE UNIQUE INDEX pm_template_pk ON pm_template USING btree (fkcontext);
CREATE UNIQUE INDEX rm_area_pk ON rm_area USING btree (fkcontext);
CREATE UNIQUE INDEX rm_asset_asset_pk ON rm_asset_asset USING btree (fkasset, fkdependent);
CREATE UNIQUE INDEX rm_asset_pk ON rm_asset USING btree (fkcontext);
CREATE UNIQUE INDEX rm_asset_value_pk ON rm_asset_value USING btree (fkasset, fkvaluename, fkparametername);
CREATE UNIQUE INDEX rm_best_practice_event_pk ON rm_best_practice_event USING btree (fkcontext);
CREATE UNIQUE INDEX rm_best_practice_pk ON rm_best_practice USING btree (fkcontext);
CREATE UNIQUE INDEX rm_best_practice_standard_pk ON rm_best_practice_standard USING btree (fkcontext);
CREATE UNIQUE INDEX rm_category_pk ON rm_category USING btree (fkcontext);
CREATE UNIQUE INDEX rm_control_best_practice_pk ON rm_control_best_practice USING btree (fkcontext);
CREATE UNIQUE INDEX rm_control_cost_pk ON rm_control_cost USING btree (fkcontrol);
CREATE UNIQUE INDEX rm_control_efficiency_history_p ON rm_control_efficiency_history USING btree (fkcontrol, ddatetodo);
CREATE UNIQUE INDEX rm_control_pk ON rm_control USING btree (fkcontext);
CREATE UNIQUE INDEX rm_control_test_history_pk ON rm_control_test_history USING btree (fkcontrol, ddatetodo);
CREATE UNIQUE INDEX rm_event_pk ON rm_event USING btree (fkcontext);
CREATE UNIQUE INDEX rm_parameter_name_pk ON rm_parameter_name USING btree (pkparametername);
CREATE UNIQUE INDEX rm_parameter_value_name_pk ON rm_parameter_value_name USING btree (pkvaluename);
CREATE UNIQUE INDEX rm_process_asset_pk ON rm_process_asset USING btree (fkcontext);
CREATE UNIQUE INDEX rm_process_pk ON rm_process USING btree (fkcontext);
CREATE UNIQUE INDEX rm_rc_parameter_value_name_pk ON rm_rc_parameter_value_name USING btree (pkrcvaluename);
CREATE UNIQUE INDEX rm_risk_control_pk ON rm_risk_control USING btree (fkcontext);
CREATE UNIQUE INDEX rm_risk_control_value_pk ON rm_risk_control_value USING btree (fkriskcontrol, fkparametername, fkrcvaluename);
CREATE UNIQUE INDEX rm_risk_limits_pk ON rm_risk_limits USING btree (fkcontext);
CREATE UNIQUE INDEX rm_risk_pk ON rm_risk USING btree (fkcontext);
CREATE UNIQUE INDEX rm_risk_value_pk ON rm_risk_value USING btree (fkrisk, fkvaluename, fkparametername);
CREATE UNIQUE INDEX rm_section_best_practice_pk ON rm_section_best_practice USING btree (fkcontext);
CREATE UNIQUE INDEX rm_standard_pk ON rm_standard USING btree (fkcontext);
CREATE UNIQUE INDEX unique_cm_department ON cm_department USING btree (fkcontext);
CREATE UNIQUE INDEX unique_cm_financial_impact ON cm_financial_impact USING btree (fkcontext);
CREATE UNIQUE INDEX unique_cm_impact_type ON cm_impact_type USING btree (fkcontext);
CREATE UNIQUE INDEX unique_cm_plan ON cm_plan USING btree (fkcontext);
CREATE UNIQUE INDEX unique_cm_plan_action ON cm_plan_action USING btree (fkcontext);
CREATE UNIQUE INDEX unique_cm_plan_action_test ON cm_plan_action_test USING btree (fkcontext);
CREATE UNIQUE INDEX unique_cm_plan_range ON cm_plan_range USING btree (fkcontext);
CREATE UNIQUE INDEX unique_cm_plan_range_type ON cm_plan_range_type USING btree (fkcontext);
CREATE UNIQUE INDEX unique_cm_plan_schedule ON cm_plan_schedule USING btree (fkcontext);
CREATE UNIQUE INDEX unique_cm_plan_test ON cm_plan_test USING btree (fkcontext);
CREATE UNIQUE INDEX unique_cm_priority ON cm_priority USING btree (fkcontext);
CREATE UNIQUE INDEX unique_fkcontext ON cm_scene USING btree (fkcontext);
CREATE UNIQUE INDEX wkf_alert_pk ON wkf_alert USING btree (pkalert);
CREATE UNIQUE INDEX wkf_control_efficiency_pk ON wkf_control_efficiency USING btree (fkcontrolefficiency);
CREATE UNIQUE INDEX wkf_control_test_pk ON wkf_control_test USING btree (fkcontroltest);
CREATE UNIQUE INDEX wkf_schedule_pk ON wkf_schedule USING btree (pkschedule);
CREATE UNIQUE INDEX wkf_task_pk ON wkf_task USING btree (pktask);
CREATE UNIQUE INDEX wkf_task_schedule_pk ON wkf_task_schedule USING btree (fkcontext, nactivity);

ALTER TABLE ONLY cm_impact
    ADD CONSTRAINT cm_impact_type FOREIGN KEY (ntype) REFERENCES cm_impact_type(fkcontext);
ALTER TABLE ONLY cm_scope_place
    ADD CONSTRAINT cm_place_fkcontext FOREIGN KEY (fkplace) REFERENCES cm_place(fkcontext);
ALTER TABLE ONLY cm_place
    ADD CONSTRAINT cm_places_fkcontext_fkey FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_threat
    ADD CONSTRAINT cm_threat FOREIGN KEY (ntype) REFERENCES isms_context_classification(pkclassification);
ALTER TABLE ONLY cm_process_inputs
    ADD CONSTRAINT context FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY ci_action_plan
    ADD CONSTRAINT fk_ci_actio_fkcontext_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ci_action_plan
    ADD CONSTRAINT fk_ci_actio_fkrespons_isms_use FOREIGN KEY (fkresponsible) REFERENCES isms_user(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ci_category
    ADD CONSTRAINT fk_ci_categ_fkcontext_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ci_incident
    ADD CONSTRAINT fk_ci_incid_fkcategor_ci_categ FOREIGN KEY (fkcategory) REFERENCES ci_category(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ci_incident_financial_impact
    ADD CONSTRAINT fk_ci_incid_fkclassif_isms_con FOREIGN KEY (fkclassification) REFERENCES isms_context_classification(pkclassification) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ci_incident_process
    ADD CONSTRAINT fk_ci_incid_fkcontext_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ci_incident
    ADD CONSTRAINT fk_ci_incid_fkcontext_isms_con2 FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ci_incident_risk
    ADD CONSTRAINT fk_ci_incid_fkcontext_isms_con7 FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ci_incident_control
    ADD CONSTRAINT fk_ci_incid_fkcontext_isms_con8 FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ci_incident_control
    ADD CONSTRAINT fk_ci_incid_fkcontrol_rm_contr FOREIGN KEY (fkcontrol) REFERENCES rm_control(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY ci_incident_user
    ADD CONSTRAINT fk_ci_incid_fkinciden_ci_incid FOREIGN KEY (fkincident) REFERENCES ci_incident(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY ci_incident_control
    ADD CONSTRAINT fk_ci_incid_fkinciden_ci_incid2 FOREIGN KEY (fkincident) REFERENCES ci_incident(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY ci_incident_financial_impact
    ADD CONSTRAINT fk_ci_incid_fkinciden_ci_incid3 FOREIGN KEY (fkincident) REFERENCES ci_incident(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY ci_incident_process
    ADD CONSTRAINT fk_ci_incid_fkinciden_ci_incid4 FOREIGN KEY (fkincident) REFERENCES ci_incident(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY ci_incident_risk
    ADD CONSTRAINT fk_ci_incid_fkinciden_ci_incid5 FOREIGN KEY (fkincident) REFERENCES ci_incident(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY ci_incident_risk_value
    ADD CONSTRAINT fk_ci_incid_fkincrisk_ci_incid FOREIGN KEY (fkincrisk) REFERENCES ci_incident_risk(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY ci_incident_risk_value
    ADD CONSTRAINT fk_ci_incid_fkparamet_rm_param FOREIGN KEY (fkparametername) REFERENCES rm_parameter_name(pkparametername) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY ci_incident_process
    ADD CONSTRAINT fk_ci_incid_fkprocess_rm_proce FOREIGN KEY (fkprocess) REFERENCES rm_process(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY ci_incident
    ADD CONSTRAINT fk_ci_incid_fkrespons_isms_use FOREIGN KEY (fkresponsible) REFERENCES isms_user(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ci_incident_risk
    ADD CONSTRAINT fk_ci_incid_fkrisk_rm_risk FOREIGN KEY (fkrisk) REFERENCES rm_risk(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY ci_incident_user
    ADD CONSTRAINT fk_ci_incid_fkuser_isms_use FOREIGN KEY (fkuser) REFERENCES isms_user(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY ci_incident_risk_value
    ADD CONSTRAINT fk_ci_incid_fkvaluena_rm_param FOREIGN KEY (fkvaluename) REFERENCES rm_parameter_value_name(pkvaluename) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY ci_nc_action_plan
    ADD CONSTRAINT fk_ci_nc_ac_fkactionp_ci_actio FOREIGN KEY (fkactionplan) REFERENCES ci_action_plan(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY ci_nc_action_plan
    ADD CONSTRAINT fk_ci_nc_ac_fknc_ci_nc FOREIGN KEY (fknc) REFERENCES ci_nc(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY ci_nc
    ADD CONSTRAINT fk_ci_nc_fkcontext_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ci_nc
    ADD CONSTRAINT fk_ci_nc_fkcontrol_rm_contr FOREIGN KEY (fkcontrol) REFERENCES rm_control(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY ci_nc
    ADD CONSTRAINT fk_ci_nc_fkrespons_isms_use FOREIGN KEY (fkresponsible) REFERENCES isms_user(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ci_nc
    ADD CONSTRAINT fk_ci_nc_fksender_isms_use FOREIGN KEY (fksender) REFERENCES isms_user(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ci_nc_process
    ADD CONSTRAINT fk_ci_nc_pr_fknc_ci_nc FOREIGN KEY (fknc) REFERENCES ci_nc(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY ci_nc_process
    ADD CONSTRAINT fk_ci_nc_pr_fkprocess_rm_proce FOREIGN KEY (fkprocess) REFERENCES rm_process(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY ci_nc_seed
    ADD CONSTRAINT fk_ci_nc_se_fkcontrol_rm_contr FOREIGN KEY (fkcontrol) REFERENCES rm_control(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY ci_occurrence
    ADD CONSTRAINT fk_ci_occur_fkcontext_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ci_occurrence
    ADD CONSTRAINT fk_ci_occur_fkinciden_ci_incid FOREIGN KEY (fkincident) REFERENCES ci_incident(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY ci_risk_probability
    ADD CONSTRAINT fk_ci_risk__fkrisk_ci_risk_ FOREIGN KEY (fkrisk) REFERENCES ci_risk_schedule(fkrisk) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY ci_risk_schedule
    ADD CONSTRAINT fk_ci_risk__fkrisk_rm_risk FOREIGN KEY (fkrisk) REFERENCES rm_risk(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY ci_risk_probability
    ADD CONSTRAINT fk_ci_risk__fkvaluena_rm_param FOREIGN KEY (fkvaluename) REFERENCES rm_parameter_value_name(pkvaluename) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY ci_solution
    ADD CONSTRAINT fk_ci_solut_fkcategor_ci_categ FOREIGN KEY (fkcategory) REFERENCES ci_category(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY ci_solution
    ADD CONSTRAINT fk_ci_solut_fkcontext_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY cm_asset_category
    ADD CONSTRAINT fk_cm_asset_cat FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY cm_asset_category
    ADD CONSTRAINT fk_cm_asset_cat_cm_asset_cat FOREIGN KEY (fkparent) REFERENCES cm_asset_category(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY cm_asset_category_threat
    ADD CONSTRAINT fk_cm_asset_cat_thr_cm_asset_cat FOREIGN KEY (fkassetcategory) REFERENCES cm_asset_category(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY cm_asset_category_threat
    ADD CONSTRAINT fk_cm_asset_cat_thr_cm_thr FOREIGN KEY (fkthreat) REFERENCES cm_threat(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY cm_asset_category_threat
    ADD CONSTRAINT fk_cm_asset_cat_thr_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY cm_place_category
    ADD CONSTRAINT fk_cm_place_cat FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY cm_place_category
    ADD CONSTRAINT fk_cm_place_cat_cm_place_cat FOREIGN KEY (fkparent) REFERENCES cm_place_category(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY cm_place_category_threat
    ADD CONSTRAINT fk_cm_place_cat_thr_cm_place_cat FOREIGN KEY (fkplacecategory) REFERENCES cm_place_category(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY cm_place_category_threat
    ADD CONSTRAINT fk_cm_place_cat_thr_cm_thr FOREIGN KEY (fkthreat) REFERENCES cm_threat(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY cm_place_category_threat
    ADD CONSTRAINT fk_cm_place_cat_thr_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY cm_process_category
    ADD CONSTRAINT fk_cm_proc_cat FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY cm_process_category
    ADD CONSTRAINT fk_cm_proc_cat_cm_proc_cat FOREIGN KEY (fkparent) REFERENCES cm_process_category(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY cm_process_category_threat
    ADD CONSTRAINT fk_cm_proc_cat_thr_cm_proc_cat FOREIGN KEY (fkprocesscategory) REFERENCES cm_process_category(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY cm_process_category_threat
    ADD CONSTRAINT fk_cm_proc_cat_thr_cm_thr FOREIGN KEY (fkthreat) REFERENCES cm_threat(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY cm_process_category_threat
    ADD CONSTRAINT fk_cm_proc_cat_thr_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY wkf_task_metadata
    ADD CONSTRAINT fk_context FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY rm_asset
    ADD CONSTRAINT fk_fkcategory_cat FOREIGN KEY (fkcategory) REFERENCES rm_category(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY cm_place
    ADD CONSTRAINT fk_fktype_place_cat FOREIGN KEY (fktype) REFERENCES cm_place_category(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY isms_context_date
    ADD CONSTRAINT fk_isms_con_fkcontext_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY isms_context_history
    ADD CONSTRAINT fk_isms_con_fkcontext_isms_con2 FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY isms_policy
    ADD CONSTRAINT fk_isms_pol_fkcontext_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY isms_profile
    ADD CONSTRAINT fk_isms_pro_fkcontext_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY isms_profile_acl
    ADD CONSTRAINT fk_isms_pro_fkprofile_isms_pro FOREIGN KEY (fkprofile) REFERENCES isms_profile(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY isms_scope
    ADD CONSTRAINT fk_isms_sco_fkcontext_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY isms_solicitor
    ADD CONSTRAINT fk_isms_sol_fksolicit_isms_use FOREIGN KEY (fksolicitor) REFERENCES isms_user(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY isms_solicitor
    ADD CONSTRAINT fk_isms_sol_fkuser_isms_use FOREIGN KEY (fkuser) REFERENCES isms_user(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY isms_user
    ADD CONSTRAINT fk_isms_use_fkcontext_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY isms_user
    ADD CONSTRAINT fk_isms_use_fkprofile_isms_pro FOREIGN KEY (fkprofile) REFERENCES isms_profile(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY isms_user_preference
    ADD CONSTRAINT fk_isms_use_fkuser_isms_use FOREIGN KEY (fkuser) REFERENCES isms_user(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY isms_user_password_history
    ADD CONSTRAINT fk_isms_use_fkuser_isms_use3 FOREIGN KEY (fkuser) REFERENCES isms_user(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY pm_doc_approvers
    ADD CONSTRAINT fk_pm_doc_a_fkdocumen_pm_docum FOREIGN KEY (fkdocument) REFERENCES pm_document(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY pm_doc_approvers
    ADD CONSTRAINT fk_pm_doc_a_fkuser_isms_use FOREIGN KEY (fkuser) REFERENCES isms_user(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY pm_doc_context
    ADD CONSTRAINT fk_pm_doc_c_fkcontext_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY pm_doc_context
    ADD CONSTRAINT fk_pm_doc_c_fkdocumen_pm_docum FOREIGN KEY (fkdocument) REFERENCES pm_document(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY pm_doc_instance
    ADD CONSTRAINT fk_pm_doc_i_fkcontext_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY pm_doc_instance
    ADD CONSTRAINT fk_pm_doc_i_fkdocumen_pm_docum FOREIGN KEY (fkdocument) REFERENCES pm_document(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY pm_doc_reference
    ADD CONSTRAINT fk_pm_doc_r_fkdocumen_pm_docum2 FOREIGN KEY (fkdocument) REFERENCES pm_document(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY pm_doc_registers
    ADD CONSTRAINT fk_pm_doc_r_fkdocumen_pm_docum3 FOREIGN KEY (fkdocument) REFERENCES pm_document(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY pm_doc_readers
    ADD CONSTRAINT fk_pm_doc_r_fkdocumen_pm_docum4 FOREIGN KEY (fkdocument) REFERENCES pm_document(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY pm_doc_read_history
    ADD CONSTRAINT fk_pm_doc_r_fkinstanc_pm_doc_i FOREIGN KEY (fkinstance) REFERENCES pm_doc_instance(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY pm_doc_registers
    ADD CONSTRAINT fk_pm_doc_r_fkregiste_pm_regis FOREIGN KEY (fkregister) REFERENCES pm_register(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY pm_doc_readers
    ADD CONSTRAINT fk_pm_doc_r_fkuser_isms_use FOREIGN KEY (fkuser) REFERENCES isms_user(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY pm_doc_read_history
    ADD CONSTRAINT fk_pm_doc_r_fkuser_isms_use2 FOREIGN KEY (fkuser) REFERENCES isms_user(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY pm_document
    ADD CONSTRAINT fk_pm_docum_fkauthor_isms_use FOREIGN KEY (fkauthor) REFERENCES isms_user(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY pm_document
    ADD CONSTRAINT fk_pm_docum_fkclassif_isms_con FOREIGN KEY (fkclassification) REFERENCES isms_context_classification(pkclassification) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY pm_document
    ADD CONSTRAINT fk_pm_docum_fkcontext_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY pm_document
    ADD CONSTRAINT fk_pm_docum_fkcurrent_pm_doc_i FOREIGN KEY (fkcurrentversion) REFERENCES pm_doc_instance(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY pm_document_revision_history
    ADD CONSTRAINT fk_pm_docum_fkdocumen_pm_docum FOREIGN KEY (fkdocument) REFERENCES pm_document(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY pm_document
    ADD CONSTRAINT fk_pm_docum_fkmainapp_isms_use FOREIGN KEY (fkmainapprover) REFERENCES isms_user(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY pm_document
    ADD CONSTRAINT fk_pm_docum_fkparent_pm_docum FOREIGN KEY (fkparent) REFERENCES pm_document(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY pm_document
    ADD CONSTRAINT fk_pm_docum_fkschedul_wkf_sche FOREIGN KEY (fkschedule) REFERENCES wkf_schedule(pkschedule) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY pm_instance_comment
    ADD CONSTRAINT fk_pm_insta_fkinstanc_pm_doc_i FOREIGN KEY (fkinstance) REFERENCES pm_doc_instance(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY pm_instance_content
    ADD CONSTRAINT fk_pm_insta_fkinstanc_pm_doc_i2 FOREIGN KEY (fkinstance) REFERENCES pm_doc_instance(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY pm_instance_comment
    ADD CONSTRAINT fk_pm_insta_fkuser_isms_use FOREIGN KEY (fkuser) REFERENCES isms_user(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY pm_process_user
    ADD CONSTRAINT fk_pm_proce_fkprocess_rm_proce FOREIGN KEY (fkprocess) REFERENCES rm_process(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY pm_process_user
    ADD CONSTRAINT fk_pm_proce_fkuser_isms_use FOREIGN KEY (fkuser) REFERENCES isms_user(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY pm_register
    ADD CONSTRAINT fk_pm_regis_fkclassif_isms_con FOREIGN KEY (fkclassification) REFERENCES isms_context_classification(pkclassification) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY pm_register
    ADD CONSTRAINT fk_pm_regis_fkcontext_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY pm_register
    ADD CONSTRAINT fk_pm_regis_fkdocumen_pm_docum FOREIGN KEY (fkdocument) REFERENCES pm_document(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY pm_register_readers
    ADD CONSTRAINT fk_pm_regis_fkregiste_pm_regis FOREIGN KEY (fkregister) REFERENCES pm_register(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY pm_register
    ADD CONSTRAINT fk_pm_regis_fkrespons_isms_use FOREIGN KEY (fkresponsible) REFERENCES isms_user(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY pm_register_readers
    ADD CONSTRAINT fk_pm_regis_fkuser_isms_use FOREIGN KEY (fkuser) REFERENCES isms_user(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY pm_template_best_practice
    ADD CONSTRAINT fk_pm_templ_fkbestpra_rm_best_ FOREIGN KEY (fkbestpractice) REFERENCES rm_best_practice(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY pm_template
    ADD CONSTRAINT fk_pm_templ_fkcontext_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY pm_template_content
    ADD CONSTRAINT fk_pm_templ_fkcontext_pm_templ FOREIGN KEY (fkcontext) REFERENCES pm_template(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY pm_template_best_practice
    ADD CONSTRAINT fk_pm_templ_fktemplat_pm_templ FOREIGN KEY (fktemplate) REFERENCES pm_template(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY rm_area
    ADD CONSTRAINT fk_rm_area_fkcontext_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY rm_area
    ADD CONSTRAINT fk_rm_area_fkparent_rm_area FOREIGN KEY (fkparent) REFERENCES rm_area(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY rm_area
    ADD CONSTRAINT fk_rm_area_fkpriority FOREIGN KEY (fkpriority) REFERENCES cm_priority(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY rm_area
    ADD CONSTRAINT fk_rm_area_fktype_isms_con FOREIGN KEY (fktype) REFERENCES isms_context_classification(pkclassification) ON UPDATE RESTRICT ON DELETE RESTRICT;

ALTER TABLE ONLY rm_area_provider
    ADD CONSTRAINT fkarea FOREIGN KEY (fkarea) REFERENCES rm_area (fkcontext) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE ONLY rm_area_provider
    ADD CONSTRAINT fkprovider FOREIGN KEY (fkprovider) REFERENCES cm_provider (fkcontext) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE ONLY rm_asset_asset
    ADD CONSTRAINT fk_rm_asset_fkasset_rm_asset FOREIGN KEY (fkasset) REFERENCES rm_asset(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY rm_asset_value
    ADD CONSTRAINT fk_rm_asset_fkasset_rm_asset2 FOREIGN KEY (fkasset) REFERENCES rm_asset(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY rm_asset
    ADD CONSTRAINT fk_rm_asset_fkcontext_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY rm_asset_asset
    ADD CONSTRAINT fk_rm_asset_fkdepende_rm_asset FOREIGN KEY (fkdependent) REFERENCES rm_asset(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY rm_asset_value
    ADD CONSTRAINT fk_rm_asset_fkparamet_rm_param FOREIGN KEY (fkparametername) REFERENCES rm_parameter_name(pkparametername) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY rm_asset_value
    ADD CONSTRAINT fk_rm_asset_fkvaluena_rm_param FOREIGN KEY (fkvaluename) REFERENCES rm_parameter_value_name(pkvaluename) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY rm_best_practice_standard
    ADD CONSTRAINT fk_rm_best__fkbestpra_rm_best_ FOREIGN KEY (fkbestpractice) REFERENCES rm_best_practice(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY rm_best_practice_event
    ADD CONSTRAINT fk_rm_best__fkbestpra_rm_best_2 FOREIGN KEY (fkbestpractice) REFERENCES rm_best_practice(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY rm_best_practice_standard
    ADD CONSTRAINT fk_rm_best__fkcontext_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY rm_best_practice_event
    ADD CONSTRAINT fk_rm_best__fkcontext_isms_con2 FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY rm_best_practice
    ADD CONSTRAINT fk_rm_best__fkcontext_isms_con3 FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY rm_best_practice_event
    ADD CONSTRAINT fk_rm_best__fkevent_rm_event FOREIGN KEY (fkevent) REFERENCES rm_event(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY rm_best_practice
    ADD CONSTRAINT fk_rm_best__fksection_rm_secti FOREIGN KEY (fksectionbestpractice) REFERENCES rm_section_best_practice(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY rm_best_practice_standard
    ADD CONSTRAINT fk_rm_best__fkstandar_rm_stand FOREIGN KEY (fkstandard) REFERENCES rm_standard(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY rm_category
    ADD CONSTRAINT fk_rm_categ_fkcontext_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY rm_category
    ADD CONSTRAINT fk_rm_categ_fkparent_rm_categ FOREIGN KEY (fkparent) REFERENCES rm_category(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY rm_control_best_practice
    ADD CONSTRAINT fk_rm_contr_fkbestpra_rm_best_ FOREIGN KEY (fkbestpractice) REFERENCES rm_best_practice(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY rm_control_best_practice
    ADD CONSTRAINT fk_rm_contr_fkcontext_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY rm_control
    ADD CONSTRAINT fk_rm_contr_fkcontext_isms_con2 FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY rm_control_best_practice
    ADD CONSTRAINT fk_rm_contr_fkcontrol_rm_contr FOREIGN KEY (fkcontrol) REFERENCES rm_control(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY rm_control_efficiency_history
    ADD CONSTRAINT fk_rm_contr_fkcontrol_rm_contr2 FOREIGN KEY (fkcontrol) REFERENCES rm_control(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY rm_control_test_history
    ADD CONSTRAINT fk_rm_contr_fkcontrol_rm_contr3 FOREIGN KEY (fkcontrol) REFERENCES rm_control(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY rm_control_cost
    ADD CONSTRAINT fk_rm_contr_fkcontrol_rm_contr4 FOREIGN KEY (fkcontrol) REFERENCES rm_control(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY rm_control
    ADD CONSTRAINT fk_rm_contr_fkrespons_isms_use FOREIGN KEY (fkresponsible) REFERENCES isms_user(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY rm_control
    ADD CONSTRAINT fk_rm_contr_fktype_isms_con FOREIGN KEY (fktype) REFERENCES isms_context_classification(pkclassification) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY rm_event
    ADD CONSTRAINT fk_rm_event_fkcategor_rm_categ FOREIGN KEY (fkcategory) REFERENCES rm_category(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY rm_event
    ADD CONSTRAINT fk_rm_event_fkcontext_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY rm_event
    ADD CONSTRAINT fk_rm_event_fktype_isms_con FOREIGN KEY (fktype) REFERENCES isms_context_classification(pkclassification) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY rm_process
    ADD CONSTRAINT fk_rm_proce_fkarea_rm_area FOREIGN KEY (fkarea) REFERENCES rm_area(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY rm_process_asset
    ADD CONSTRAINT fk_rm_proce_fkasset_rm_asset FOREIGN KEY (fkasset) REFERENCES rm_asset(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY rm_process_asset
    ADD CONSTRAINT fk_rm_proce_fkcontext_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY rm_process
    ADD CONSTRAINT fk_rm_proce_fkcontext_isms_con2 FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY rm_process_asset
    ADD CONSTRAINT fk_rm_proce_fkprocess_rm_proce FOREIGN KEY (fkprocess) REFERENCES rm_process(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY rm_risk_control
    ADD CONSTRAINT fk_rm_risk__fkcontext_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY rm_risk_limits
    ADD CONSTRAINT fk_rm_risk__fkcontext_isms_con2 FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY rm_risk_control
    ADD CONSTRAINT fk_rm_risk__fkcontrol_rm_contr FOREIGN KEY (fkcontrol) REFERENCES rm_control(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY rm_risk_control_value
    ADD CONSTRAINT fk_rm_risk__fkparamet_rm_param FOREIGN KEY (fkparametername) REFERENCES rm_parameter_name(pkparametername) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY rm_risk_value
    ADD CONSTRAINT fk_rm_risk__fkparamet_rm_param2 FOREIGN KEY (fkparametername) REFERENCES rm_parameter_name(pkparametername) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY rm_risk_control
    ADD CONSTRAINT fk_rm_risk__fkprobabi_rm_rc_pa FOREIGN KEY (fkprobabilityvaluename) REFERENCES rm_rc_parameter_value_name(pkrcvaluename) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY rm_risk_control_value
    ADD CONSTRAINT fk_rm_risk__fkrcvalue_rm_rc_pa FOREIGN KEY (fkrcvaluename) REFERENCES rm_rc_parameter_value_name(pkrcvaluename) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY rm_risk_control
    ADD CONSTRAINT fk_rm_risk__fkrisk_rm_risk FOREIGN KEY (fkrisk) REFERENCES rm_risk(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY rm_risk_value
    ADD CONSTRAINT fk_rm_risk__fkrisk_rm_risk2 FOREIGN KEY (fkrisk) REFERENCES rm_risk(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY rm_risk_control_value
    ADD CONSTRAINT fk_rm_risk__fkriskcon_rm_risk_ FOREIGN KEY (fkriskcontrol) REFERENCES rm_risk_control(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY rm_risk_value
    ADD CONSTRAINT fk_rm_risk__fkvaluena_rm_param FOREIGN KEY (fkvaluename) REFERENCES rm_parameter_value_name(pkvaluename) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY rm_risk
    ADD CONSTRAINT fk_rm_risk_fkasset_rm_asset FOREIGN KEY (fkasset) REFERENCES rm_asset(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY rm_risk
    ADD CONSTRAINT fk_rm_risk_fkcontext_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY rm_risk
    ADD CONSTRAINT fk_rm_risk_fkevent_rm_event FOREIGN KEY (fkevent) REFERENCES rm_event(fkcontext) ON UPDATE RESTRICT ON DELETE SET NULL;
ALTER TABLE ONLY rm_risk
    ADD CONSTRAINT fk_rm_risk_fkprobabi_rm_param FOREIGN KEY (fkprobabilityvaluename) REFERENCES rm_parameter_value_name(pkvaluename) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY rm_risk
    ADD CONSTRAINT fk_rm_risk_fktype_isms_con FOREIGN KEY (fktype) REFERENCES isms_context_classification(pkclassification) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY rm_section_best_practice
    ADD CONSTRAINT fk_rm_secti_fkcontext_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY rm_section_best_practice
    ADD CONSTRAINT fk_rm_secti_fkparent_rm_secti FOREIGN KEY (fkparent) REFERENCES rm_section_best_practice(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY rm_standard
    ADD CONSTRAINT fk_rm_stand_fkcontext_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY wkf_task_metadata
    ADD CONSTRAINT fk_task FOREIGN KEY (fktask) REFERENCES wkf_task(pktask);
ALTER TABLE ONLY wkf_alert
    ADD CONSTRAINT fk_wkf_aler_fkcontext_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY wkf_alert
    ADD CONSTRAINT fk_wkf_aler_fkcreator_isms_use FOREIGN KEY (fkcreator) REFERENCES isms_user(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY wkf_alert
    ADD CONSTRAINT fk_wkf_aler_fkreceive_isms_use FOREIGN KEY (fkreceiver) REFERENCES isms_user(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY wkf_control_test
    ADD CONSTRAINT fk_wkf_cont_fkcontrol_rm_contr FOREIGN KEY (fkcontroltest) REFERENCES rm_control(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY wkf_control_efficiency
    ADD CONSTRAINT fk_wkf_cont_fkcontrol_rm_contr2 FOREIGN KEY (fkcontrolefficiency) REFERENCES rm_control(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY wkf_control_test
    ADD CONSTRAINT fk_wkf_cont_fkschedul_wkf_sch2 FOREIGN KEY (fkschedule) REFERENCES wkf_schedule(pkschedule) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY wkf_control_efficiency
    ADD CONSTRAINT fk_wkf_cont_fkschedul_wkf_sche FOREIGN KEY (fkschedule) REFERENCES wkf_schedule(pkschedule) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY wkf_task_schedule
    ADD CONSTRAINT fk_wkf_task_fkcontext_isms_co2 FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY wkf_task
    ADD CONSTRAINT fk_wkf_task_fkcontext_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY wkf_task
    ADD CONSTRAINT fk_wkf_task_fkcreator_isms_use FOREIGN KEY (fkcreator) REFERENCES isms_user(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY wkf_task
    ADD CONSTRAINT fk_wkf_task_fkreceive_isms_use FOREIGN KEY (fkreceiver) REFERENCES isms_user(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY wkf_task_schedule
    ADD CONSTRAINT fk_wkf_task_fkschedul_wkf_sche FOREIGN KEY (fkschedule) REFERENCES wkf_schedule(pkschedule) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY cm_plan_action
    ADD CONSTRAINT fkaction FOREIGN KEY (fkaction) REFERENCES cm_plan_action(fkcontext);
ALTER TABLE ONLY cm_process_activity_asset
    ADD CONSTRAINT fkactivity FOREIGN KEY (fkactivity) REFERENCES cm_process_activity(fkcontext);
ALTER TABLE ONLY cm_process_activity_people
    ADD CONSTRAINT fkactvity FOREIGN KEY (fkactivity) REFERENCES cm_process_activity(fkcontext);
ALTER TABLE ONLY cm_plan
    ADD CONSTRAINT fkarea FOREIGN KEY (fkarea) REFERENCES rm_area(fkcontext);
ALTER TABLE ONLY cm_area_resource
    ADD CONSTRAINT fkarea FOREIGN KEY (fkarea) REFERENCES rm_area(fkcontext);
ALTER TABLE ONLY cm_place_area
    ADD CONSTRAINT fkarea FOREIGN KEY (fkarea) REFERENCES rm_area(fkcontext);
ALTER TABLE ONLY cm_asset_threat
    ADD CONSTRAINT fkasset FOREIGN KEY (fkasset) REFERENCES rm_asset(fkcontext);
ALTER TABLE ONLY cm_process_activity_asset
    ADD CONSTRAINT fkasset FOREIGN KEY (fkasset) REFERENCES rm_asset(fkcontext);
ALTER TABLE ONLY cm_plan
    ADD CONSTRAINT fkasset FOREIGN KEY (fkasset) REFERENCES rm_asset(fkcontext);
ALTER TABLE ONLY cm_place_asset
    ADD CONSTRAINT fkasset FOREIGN KEY (fkasset) REFERENCES rm_asset(fkcontext);
ALTER TABLE ONLY cm_place
    ADD CONSTRAINT fkcmt FOREIGN KEY (fkcmt) REFERENCES cm_group(fkcontext);
ALTER TABLE ONLY ci_incident_risk_parameter
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_asset_importance
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_asset_threat
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_threat_probability
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_damage_matrix_parameter
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_impact_type
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_process_activity
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_process_outputs
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_place_process
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_damage_matrix
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_process_activity_people
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_process_activity_asset
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_process_threat
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_financial_impact
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_impact
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_impact_damage
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_impact_damage_range
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_priority
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_group
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_resource
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_group_resource
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_plan_range_type
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_plan_range
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_plan
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_plan_action
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_place_asset
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_place_resource
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_area_resource
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_provider
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_provider_resource
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_department
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_threat_vulnerability
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY cm_plan_test
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_plan_action_test
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_plan_schedule
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_place_area
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_place_provider
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_scope_process
    ADD CONSTRAINT fkcontext_cm_scope FOREIGN KEY (fkscope) REFERENCES cm_scope(fkcontext);
ALTER TABLE ONLY cm_scope_place
    ADD CONSTRAINT fkcontext_cm_scope FOREIGN KEY (fkscope) REFERENCES cm_scope(fkcontext);
ALTER TABLE ONLY cm_scope_area
    ADD CONSTRAINT fkcontext_cm_scope FOREIGN KEY (fkscope) REFERENCES cm_scope(fkcontext);
ALTER TABLE ONLY cm_scope_asset
    ADD CONSTRAINT fkcontext_cm_scope FOREIGN KEY (fkscope) REFERENCES cm_scope(fkcontext);
ALTER TABLE ONLY cm_scope
    ADD CONSTRAINT fkcontext_scope FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_scope_process
    ADD CONSTRAINT fkcontext_scope FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_scope_place
    ADD CONSTRAINT fkcontext_scope FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_scope_area
    ADD CONSTRAINT fkcontext_scope FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_scope_asset
    ADD CONSTRAINT fkcontext_scope FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_impact_damage_range
    ADD CONSTRAINT fkdamage FOREIGN KEY (fkdamage) REFERENCES cm_damage_matrix(fkcontext);
ALTER TABLE ONLY cm_impact_damage_range
    ADD CONSTRAINT fkdamageparameter FOREIGN KEY (fkdamageparameter) REFERENCES cm_damage_matrix_parameter(fkcontext);
ALTER TABLE ONLY cm_resource
    ADD CONSTRAINT fkdepartment FOREIGN KEY (fkdepartment) REFERENCES cm_department(fkcontext);
ALTER TABLE ONLY cm_group_resource
    ADD CONSTRAINT fkgroup FOREIGN KEY (fkgroup) REFERENCES cm_group(fkcontext);
ALTER TABLE ONLY cm_plan_action
    ADD CONSTRAINT fkgroup FOREIGN KEY (fkgroup) REFERENCES cm_group(fkcontext);
ALTER TABLE ONLY rm_asset
    ADD CONSTRAINT fkgroup FOREIGN KEY (fkgroup) REFERENCES cm_group(fkcontext);
ALTER TABLE ONLY cm_process_activity
    ADD CONSTRAINT fkgroup FOREIGN KEY (fkgroup) REFERENCES cm_group(fkcontext);
ALTER TABLE ONLY rm_process
    ADD CONSTRAINT fkgroup FOREIGN KEY (fkgroup) REFERENCES cm_group(fkcontext);
ALTER TABLE ONLY cm_place
    ADD CONSTRAINT fkgroup FOREIGN KEY (fkgroup) REFERENCES cm_group(fkcontext);
ALTER TABLE ONLY rm_area
    ADD CONSTRAINT fkgroup FOREIGN KEY (fkgroup) REFERENCES cm_group(fkcontext);
ALTER TABLE ONLY cm_place_resource
    ADD CONSTRAINT fkgroup FOREIGN KEY (fkgroup) REFERENCES cm_group(fkcontext);
ALTER TABLE ONLY cm_area_resource
    ADD CONSTRAINT fkgroup FOREIGN KEY (fkgroup) REFERENCES cm_group(fkcontext);
ALTER TABLE ONLY cm_plan_schedule
    ADD CONSTRAINT fkgroup FOREIGN KEY (fkgroup) REFERENCES cm_group(fkcontext);
ALTER TABLE ONLY rm_process
    ADD CONSTRAINT fkgroupsubstitute FOREIGN KEY (fkgroupsubstitute) REFERENCES cm_group(fkcontext);
ALTER TABLE ONLY cm_place
    ADD CONSTRAINT fkgroupsubstitute FOREIGN KEY (fkgroupsubstitute) REFERENCES cm_group(fkcontext);
ALTER TABLE ONLY rm_area
    ADD CONSTRAINT fkgroupsubstitute FOREIGN KEY (fkgroupsubstitute) REFERENCES cm_group(fkcontext);
ALTER TABLE ONLY cm_impact_damage
    ADD CONSTRAINT fkimpact FOREIGN KEY (fkimpact) REFERENCES cm_impact(fkcontext);
ALTER TABLE ONLY cm_impact_damage_range
    ADD CONSTRAINT fkimpactdamage FOREIGN KEY (fkimpactdamage) REFERENCES cm_impact_damage(fkcontext);
ALTER TABLE ONLY cm_process_activity
    ADD CONSTRAINT fkimportance FOREIGN KEY (fkimportance) REFERENCES cm_priority(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY rm_process_asset
    ADD CONSTRAINT fkimportance FOREIGN KEY (fkimportance) REFERENCES cm_priority(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY cm_process_activity_asset
    ADD CONSTRAINT fkimportance FOREIGN KEY (fkimportance) REFERENCES cm_priority(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ci_incident_risk_parameter
    ADD CONSTRAINT fkincident FOREIGN KEY (fkincident) REFERENCES ci_incident(fkcontext);
ALTER TABLE ONLY cm_threat_vulnerability
    ADD CONSTRAINT fklevel FOREIGN KEY (fklevel) REFERENCES cm_priority(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY cm_plan_action
    ADD CONSTRAINT fkotherplan FOREIGN KEY (fkotherplan) REFERENCES cm_plan(fkcontext);
ALTER TABLE ONLY cm_place
    ADD CONSTRAINT fkparent FOREIGN KEY (fkparent) REFERENCES cm_place(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY cm_place_process
    ADD CONSTRAINT fkplace FOREIGN KEY (fkplace) REFERENCES cm_place(fkcontext);
ALTER TABLE ONLY cm_place_threat
    ADD CONSTRAINT fkplace FOREIGN KEY (fkplace) REFERENCES cm_place(fkcontext);
ALTER TABLE ONLY cm_plan
    ADD CONSTRAINT fkplace FOREIGN KEY (fkplace) REFERENCES cm_place(fkcontext);
ALTER TABLE ONLY cm_place_asset
    ADD CONSTRAINT fkplace FOREIGN KEY (fkplace) REFERENCES cm_place(fkcontext);
ALTER TABLE ONLY rm_area
    ADD CONSTRAINT fkplace FOREIGN KEY (fkplace) REFERENCES cm_place(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY cm_place_resource
    ADD CONSTRAINT fkplace FOREIGN KEY (fkplace) REFERENCES cm_place(fkcontext);
ALTER TABLE ONLY cm_threat_vulnerability
    ADD CONSTRAINT fkplace FOREIGN KEY (fkplace) REFERENCES cm_place(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY rm_asset
    ADD CONSTRAINT fkplace FOREIGN KEY (fkplace) REFERENCES cm_place(fkcontext);
ALTER TABLE ONLY cm_place_area
    ADD CONSTRAINT fkplace FOREIGN KEY (fkplace) REFERENCES cm_place(fkcontext);
ALTER TABLE ONLY cm_place_provider
    ADD CONSTRAINT fkplace FOREIGN KEY (fkplace) REFERENCES cm_place(fkcontext);
ALTER TABLE ONLY cm_plan_action
    ADD CONSTRAINT fkplan FOREIGN KEY (fkplan) REFERENCES cm_plan(fkcontext);
ALTER TABLE ONLY cm_plan_test
    ADD CONSTRAINT fkplan FOREIGN KEY (fkplan) REFERENCES cm_plan(fkcontext);
ALTER TABLE ONLY cm_plan_schedule
    ADD CONSTRAINT fkplan FOREIGN KEY (fkplan) REFERENCES cm_plan(fkcontext);
ALTER TABLE ONLY cm_plan_action_test
    ADD CONSTRAINT fkplanaction FOREIGN KEY (fkplanaction) REFERENCES cm_plan_action(fkcontext);
ALTER TABLE ONLY cm_plan_action
    ADD CONSTRAINT fkplanrange FOREIGN KEY (fkplanrange) REFERENCES cm_plan_range(fkcontext);
ALTER TABLE ONLY cm_plan_action_test
    ADD CONSTRAINT fkplantest FOREIGN KEY (fkplantest) REFERENCES cm_plan_test(fkcontext);
ALTER TABLE ONLY cm_plan_action
    ADD CONSTRAINT fkplantype FOREIGN KEY (fkplantype) REFERENCES cm_plan_range_type(fkcontext);
ALTER TABLE ONLY cm_group
    ADD CONSTRAINT fkpriority FOREIGN KEY (fkpriority) REFERENCES cm_priority(fkcontext);
ALTER TABLE ONLY cm_process_activity
    ADD CONSTRAINT fkpriority FOREIGN KEY (fkpriority) REFERENCES cm_priority(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY rm_process
    ADD CONSTRAINT fkpriority FOREIGN KEY (fkpriority) REFERENCES cm_priority(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY cm_scene
    ADD CONSTRAINT fkprobability FOREIGN KEY (fkprobability) REFERENCES cm_priority(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY cm_place_process
    ADD CONSTRAINT fkprocess FOREIGN KEY (fkprocess) REFERENCES rm_process(fkcontext);
ALTER TABLE ONLY cm_scene
    ADD CONSTRAINT fkprocess FOREIGN KEY (fkprocess) REFERENCES rm_process(fkcontext);
ALTER TABLE ONLY cm_process_activity
    ADD CONSTRAINT fkprocess FOREIGN KEY (fkprocess) REFERENCES rm_process(fkcontext);
ALTER TABLE ONLY cm_process_threat
    ADD CONSTRAINT fkprocess FOREIGN KEY (fkprocess) REFERENCES rm_process(fkcontext);
ALTER TABLE ONLY cm_plan
    ADD CONSTRAINT fkprocess FOREIGN KEY (fkprocess) REFERENCES rm_process(fkcontext);
ALTER TABLE ONLY cm_provider_resource
    ADD CONSTRAINT fkprovider FOREIGN KEY (fkprovider) REFERENCES cm_provider(fkcontext);
ALTER TABLE ONLY rm_asset
    ADD CONSTRAINT fkprovider FOREIGN KEY (fkprovider) REFERENCES cm_provider(fkcontext);
ALTER TABLE ONLY cm_place_provider
    ADD CONSTRAINT fkprovider FOREIGN KEY (fkprovider) REFERENCES cm_provider(fkcontext);
ALTER TABLE ONLY cm_group_resource
    ADD CONSTRAINT fkresource FOREIGN KEY (fkresource) REFERENCES cm_resource(fkcontext);
ALTER TABLE ONLY rm_process
    ADD CONSTRAINT fkresource FOREIGN KEY (fkresource) REFERENCES cm_resource(fkcontext);
ALTER TABLE ONLY cm_place
    ADD CONSTRAINT fkresource FOREIGN KEY (fkresource) REFERENCES cm_resource(fkcontext);
ALTER TABLE ONLY rm_area
    ADD CONSTRAINT fkresource FOREIGN KEY (fkresource) REFERENCES cm_resource(fkcontext);
ALTER TABLE ONLY cm_place_resource
    ADD CONSTRAINT fkresource FOREIGN KEY (fkresource) REFERENCES cm_resource(fkcontext);
ALTER TABLE ONLY cm_area_resource
    ADD CONSTRAINT fkresource FOREIGN KEY (fkresource) REFERENCES cm_resource(fkcontext);
ALTER TABLE ONLY cm_provider_resource
    ADD CONSTRAINT fkresource FOREIGN KEY (fkresource) REFERENCES cm_resource(fkcontext);
ALTER TABLE ONLY rm_process
    ADD CONSTRAINT fkresourcesubstitute FOREIGN KEY (fkresourcesubstitute) REFERENCES cm_resource(fkcontext);
ALTER TABLE ONLY cm_place
    ADD CONSTRAINT fkresourcesubstitute FOREIGN KEY (fkresourcesubstitute) REFERENCES cm_resource(fkcontext);
ALTER TABLE ONLY rm_area
    ADD CONSTRAINT fkresourcesubstitute FOREIGN KEY (fkresourcesubstitute) REFERENCES cm_resource(fkcontext);
ALTER TABLE ONLY cm_process_activity
    ADD CONSTRAINT fkresponsible FOREIGN KEY (fkresponsible) REFERENCES isms_user(fkcontext);
ALTER TABLE ONLY cm_plan_test
    ADD CONSTRAINT fkresponsible FOREIGN KEY (fkresponsible) REFERENCES cm_resource(fkcontext);
ALTER TABLE ONLY cm_plan_schedule
    ADD CONSTRAINT fkresponsible FOREIGN KEY (fkresponsible) REFERENCES cm_resource(fkcontext);
ALTER TABLE ONLY cm_impact_damage
    ADD CONSTRAINT fkscene FOREIGN KEY (fkscene) REFERENCES cm_scene(fkcontext);
ALTER TABLE ONLY rm_process
    ADD CONSTRAINT fkseasonality FOREIGN KEY (fkseasonality) REFERENCES wkf_schedule(pkschedule) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY cm_scene
    ADD CONSTRAINT fkseasonality FOREIGN KEY (fkseasonality) REFERENCES wkf_schedule(pkschedule) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY cm_plan_schedule
    ADD CONSTRAINT fktest FOREIGN KEY (fktest) REFERENCES cm_plan_test(fkcontext);
ALTER TABLE ONLY cm_threat
    ADD CONSTRAINT fkthreat FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_asset_threat
    ADD CONSTRAINT fkthreat FOREIGN KEY (fkthreat) REFERENCES cm_threat(fkcontext);
ALTER TABLE ONLY cm_place_threat
    ADD CONSTRAINT fkthreat FOREIGN KEY (fkthreat) REFERENCES cm_threat(fkcontext);
ALTER TABLE ONLY cm_process_threat
    ADD CONSTRAINT fkthreat FOREIGN KEY (fkthreat) REFERENCES cm_threat(fkcontext);
ALTER TABLE ONLY cm_threat_vulnerability
    ADD CONSTRAINT fkthreat FOREIGN KEY (fkthreat) REFERENCES cm_threat(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY cm_plan_range
    ADD CONSTRAINT fktype FOREIGN KEY (fktype) REFERENCES cm_plan_range_type(fkcontext);
ALTER TABLE ONLY rm_process
    ADD CONSTRAINT fktype FOREIGN KEY (fktype) REFERENCES isms_context_classification(pkclassification) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY cm_process_activity_people
    ADD CONSTRAINT fkuser FOREIGN KEY (fkuser) REFERENCES isms_user(fkcontext);
ALTER TABLE ONLY cm_process_inputs
    ADD CONSTRAINT gc_process_inputs_fk_process_fkey FOREIGN KEY (fk_process) REFERENCES rm_process(fkcontext);
ALTER TABLE ONLY cm_process_inputs
    ADD CONSTRAINT gc_process_inputs_fk_process_input_fkey FOREIGN KEY (fk_process_input) REFERENCES rm_process(fkcontext);
ALTER TABLE ONLY cm_threat
    ADD CONSTRAINT nprobability FOREIGN KEY (nprobability) REFERENCES cm_threat_probability(fkcontext);
ALTER TABLE ONLY ci_incident_risk_parameter
    ADD CONSTRAINT parameter_name FOREIGN KEY (fkparametername) REFERENCES rm_parameter_name(pkparametername);
ALTER TABLE ONLY ci_incident_risk_parameter
    ADD CONSTRAINT parameter_value FOREIGN KEY (fkparametervalue) REFERENCES rm_parameter_value_name(pkvaluename);
ALTER TABLE ONLY cm_scene
    ADD CONSTRAINT pkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_place_threat
    ADD CONSTRAINT pkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_process_outputs
    ADD CONSTRAINT process_fk FOREIGN KEY (fk_process) REFERENCES rm_process(fkcontext);
ALTER TABLE ONLY cm_process_outputs
    ADD CONSTRAINT process_output_fk FOREIGN KEY (fk_process_output) REFERENCES rm_process(fkcontext);
ALTER TABLE ONLY cm_scope_area
    ADD CONSTRAINT rm_area_fkcontext FOREIGN KEY (fkarea) REFERENCES rm_area(fkcontext);
ALTER TABLE ONLY cm_scope_asset
    ADD CONSTRAINT rm_asset_fkcontext FOREIGN KEY (fkasset) REFERENCES rm_asset(fkcontext);
ALTER TABLE ONLY cm_scope_process
    ADD CONSTRAINT rm_process_fkcontext FOREIGN KEY (fkprocess) REFERENCES rm_process(fkcontext);


