CREATE ROLE %database_login% LOGIN ENCRYPTED PASSWORD '%database_login_pass%' IN GROUP saasinstances
NOINHERIT
VALID UNTIL 'infinity';