CREATE TYPE area_id AS (
	pkarea integer
);
CREATE TYPE area_ids AS (
	area_id integer
);
CREATE TYPE area_parent_ids AS (
	parent_id integer
);
CREATE TYPE area_risk_id AS (
	area_id integer,
	risk_id integer
);
CREATE TYPE asset_category_threat AS (
	fkassetcategory integer,
	fkthreat integer,
	fkparent integer
);
CREATE TYPE asset_category_threats AS (
	fkassetcategory integer,
	fkthreat integer,
	fkparent integer,
	fkasset integer
);
CREATE TYPE asset_dependency AS (
	asset integer,
	dependency integer
);
CREATE TYPE asset_dependent AS (
	asset integer,
	dependent integer
);
CREATE TYPE asset_id AS (
	pkasset integer
);
CREATE TYPE asset_value_cont AS (
	cont_system integer,
	cont_asset integer
);
CREATE TYPE best_practices AS (
	pkbestpractice integer
);
CREATE TYPE category_id AS (
	pkcategory integer
);
CREATE TYPE context_doc_count AS (
	fkcontext integer,
	sname character varying(256),
	nvalue double precision,
	doc_count integer
);
CREATE TYPE context_id AS (
	id integer
);
CREATE TYPE context_rev_count AS (
	fkcontext integer,
	sname character varying(256),
	fkcurrentversion integer,
	rev_count integer
);
CREATE TYPE control_cost AS (
	cost1 double precision,
	cost2 double precision,
	cost3 double precision,
	cost4 double precision,
	cost5 double precision
);
CREATE TYPE ctrl_date AS (
	ddeadline timestamp without time zone,
	dimpl timestamp without time zone
);
CREATE TYPE ctrl_eff AS (
	nre integer,
	nee integer
);
CREATE TYPE ctx_names AS (
	str character varying(255)
);
CREATE TYPE document_id AS (
	document_id integer
);
CREATE TYPE event_id AS (
	pkevent integer
);
CREATE TYPE id_value AS (
	id integer,
	value double precision
);
CREATE TYPE id_values AS (
	id integer,
	value1 double precision,
	value2 double precision
);
CREATE TYPE place_category_threat AS (
	fkplacecategory integer,
	fkthreat integer,
	fkparent integer
);
CREATE TYPE place_category_threats AS (
	fkplacecategory integer,
	fkthreat integer,
	fkparent integer,
	fkplace integer
);
CREATE TYPE place_parent AS (
	fkplace integer,
	fkparent integer
);
CREATE TYPE process_category_threat AS (
	fkprocesscategory integer,
	fkthreat integer,
	fkparent integer
);
CREATE TYPE process_category_threats AS (
	fkprocesscategory integer,
	fkthreat integer,
	fkparent integer,
	fkprocess integer
);
CREATE TYPE process_id AS (
	pkprocess integer
);
CREATE TYPE process_risk_id AS (
	process_id integer,
	risk_id integer
);
CREATE TYPE recalc_risk_update AS (
	"miVRchg" double precision,
	"miAMchg" integer,
	"miRisk" integer,
	"miAsset" integer
);
CREATE TYPE risk_id AS (
	pkrisk integer
);
CREATE TYPE risk_par_reduction AS (
	par_red_value integer,
	risk_par_value integer
);
CREATE TYPE risk_prob AS (
	reduct integer,
	value integer
);
CREATE TYPE risk_value_cont AS (
	cont_system integer,
	cont_risk integer,
	cont_asset integer
);
CREATE TYPE risk_values AS (
	par_name character varying(255),
	risk integer,
	asset integer,
	par_weight integer
);
CREATE TYPE row_double AS (
	value double precision
);
CREATE TYPE row_int AS (
	value integer
);
CREATE TYPE section_id AS (
	pksection integer
);
CREATE TYPE tree_acl AS (
	pkacl integer,
	nlevel integer,
	sname character varying(256),
	stag character varying(256),
	sdescription character varying(256)
);
CREATE TYPE tree_area AS (
	pkid numeric,
	fkcontext integer,
	sname character varying(255),
	nlevel integer
);
CREATE TYPE tree_area_aux AS (
	fkcontext integer,
	sname character varying(255),
	nlevel integer
);
CREATE TYPE tree_best_practice AS (
	fkcontext integer,
	sname character varying(255),
	nlevel integer
);
CREATE TYPE tree_category AS (
	fkcontext integer,
	sname character varying(255),
	nlevel integer
);
CREATE TYPE tree_document AS (
	fkcontext integer,
	sname character varying(255),
	nlevel integer
);

CREATE TABLE ci_action_plan (
    fkcontext integer NOT NULL,
    fkresponsible integer,
    sname character varying(256) NOT NULL,
    tactionplan text,
    nactiontype integer DEFAULT 0,
    ddatedeadline timestamp without time zone,
    ddateconclusion timestamp without time zone,
    bisefficient integer DEFAULT 0,
    ddateefficiencyrevision timestamp without time zone,
    ddateefficiencymeasured timestamp without time zone,
    ndaysbefore integer DEFAULT 0,
    bflagrevisionalert integer DEFAULT 0,
    bflagrevisionalertlate integer DEFAULT 0,
    sdocument character varying(256),
    CONSTRAINT ckc_bflagrevisionaler_ci_actio CHECK (((bflagrevisionalertlate IS NULL) OR ((bflagrevisionalertlate >= 0) AND (bflagrevisionalertlate <= 1)))),
    CONSTRAINT ckc_bflagrevisionaler_ci_actio2 CHECK (((bflagrevisionalert IS NULL) OR ((bflagrevisionalert >= 0) AND (bflagrevisionalert <= 1)))),
    CONSTRAINT ckc_bisefficient_ci_actio CHECK (((bisefficient IS NULL) OR ((bisefficient >= 0) AND (bisefficient <= 1))))
);
CREATE TABLE ci_category (
    fkcontext integer NOT NULL,
    sname character varying(256) NOT NULL
);
COMMENT ON TABLE ci_category IS 'Categoria de incidentes.';
CREATE TABLE ci_incident (
    fkcontext integer NOT NULL,
    fkcategory integer,
    fkresponsible integer,
    sname character varying(256) NOT NULL,
    taccountsplan text,
    tevidences text,
    nlosstype integer DEFAULT 0,
    tevidencerequirementcomment text,
    ddatelimit timestamp without time zone,
    ddatefinish timestamp without time zone,
    tdisposaldescription text,
    tsolutiondescription text,
    tproductservice text,
    bnotemaildp integer DEFAULT 0,
    bemaildpsent integer DEFAULT 0,
    ddate timestamp without time zone NOT NULL,
    CONSTRAINT ckc_bemaildpsent_ci_incid CHECK (((bemaildpsent IS NULL) OR ((bemaildpsent >= 0) AND (bemaildpsent <= 1)))),
    CONSTRAINT ckc_bnotemaildp_ci_incid CHECK (((bnotemaildp IS NULL) OR ((bnotemaildp >= 0) AND (bnotemaildp <= 1))))
);
COMMENT ON TABLE ci_incident IS 'Tabela de incidentes.';
CREATE TABLE ci_incident_control (
    fkincident integer NOT NULL,
    fkcontrol integer NOT NULL,
    fkcontext integer NOT NULL
);
COMMENT ON TABLE ci_incident_control IS 'Tabela que relaciona incidentes e controles.';
CREATE TABLE ci_incident_financial_impact (
    fkincident integer NOT NULL,
    fkclassification integer NOT NULL,
    nvalue double precision DEFAULT (0)::double precision
);
COMMENT ON TABLE ci_incident_financial_impact IS 'Tabela de impacto financeiro de um incidente.';
CREATE TABLE ci_incident_process (
    fkprocess integer NOT NULL,
    fkincident integer NOT NULL,
    fkcontext integer NOT NULL
);
COMMENT ON TABLE ci_incident_process IS 'Tabela que relaciona incidentes e processos.';
CREATE TABLE ci_incident_risk (
    fkcontext integer NOT NULL,
    fkrisk integer NOT NULL,
    fkincident integer NOT NULL
);
CREATE TABLE ci_incident_risk_parameter (
    fkcontext integer,
    fkincident integer,
    fkparametername integer,
    fkparametervalue integer
);
CREATE TABLE ci_incident_risk_value (
    fkparametername integer NOT NULL,
    fkvaluename integer NOT NULL,
    fkincrisk integer NOT NULL
);
COMMENT ON TABLE ci_incident_risk_value IS 'Tabela que relaciona incidentes e riscos.';
CREATE TABLE ci_incident_user (
    fkuser integer NOT NULL,
    fkincident integer NOT NULL,
    tdescription text NOT NULL,
    tactiontaken text
);
COMMENT ON TABLE ci_incident_user IS 'Tabela de relacionamento entre  incidentes e usuários.';
CREATE TABLE ci_nc (
    fkcontext integer NOT NULL,
    fkcontrol integer,
    fkresponsible integer,
    fksender integer NOT NULL,
    sname character varying(256) NOT NULL,
    nseqnumber integer NOT NULL,
    tdescription text,
    tcause text,
    tdenialjustification text,
    nclassification integer DEFAULT 0,
    ddatesent timestamp without time zone,
    ncapability integer DEFAULT 0
);
CREATE TABLE ci_nc_action_plan (
    fknc integer NOT NULL,
    fkactionplan integer NOT NULL
);
COMMENT ON TABLE ci_nc_action_plan IS 'Relaciona não conformidades com planos de ação.';
CREATE SEQUENCE ci_nc_nseqnumber_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
ALTER SEQUENCE ci_nc_nseqnumber_seq OWNED BY ci_nc.nseqnumber;
CREATE TABLE ci_nc_process (
    fknc integer NOT NULL,
    fkprocess integer NOT NULL
);
COMMENT ON TABLE ci_nc_process IS 'Tabela que relaciona não conformidades com processos.';
CREATE TABLE ci_nc_seed (
    fkcontrol integer NOT NULL,
    ndeactivationreason integer DEFAULT 0 NOT NULL,
    ddatesent timestamp without time zone
);
CREATE TABLE ci_occurrence (
    fkcontext integer NOT NULL,
    fkincident integer,
    tdescription text NOT NULL,
    tdenialjustification text,
    ddate timestamp without time zone NOT NULL
);
COMMENT ON TABLE ci_occurrence IS 'Tabela de ocorrências de incidentes.';
CREATE TABLE ci_risk_probability (
    fkrisk integer NOT NULL,
    fkvaluename integer NOT NULL,
    nincidentamount integer DEFAULT 0 NOT NULL
);
CREATE TABLE ci_risk_schedule (
    fkrisk integer NOT NULL,
    nperiod integer DEFAULT 0 NOT NULL,
    nvalue integer DEFAULT 0 NOT NULL,
    ddatelastcheck timestamp without time zone
);
CREATE TABLE ci_solution (
    fkcontext integer NOT NULL,
    fkcategory integer,
    tproblem text,
    tsolution text,
    tkeywords text
);
COMMENT ON TABLE ci_solution IS 'Tabela de soluções para incidentes.';
SET default_with_oids = false;
CREATE TABLE cm_area_resource (
    fkcontext integer,
    fkarea integer,
    fkresource integer,
    fkgroup integer,
    cmt integer
);
SET default_with_oids = true;
CREATE TABLE cm_asset_category (
    fkcontext integer NOT NULL,
    fkparent integer,
    sname character varying(256) NOT NULL,
    tdescription text
);
CREATE TABLE cm_asset_category_threat (
    fkcontext integer NOT NULL,
    fkassetcategory integer NOT NULL,
    fkthreat integer NOT NULL
);
CREATE TABLE cm_asset_importance (
    fkcontext integer,
    sname text,
    nimportance integer
);
CREATE TABLE cm_asset_threat (
    fkcontext integer,
    fkasset integer,
    fkthreat integer
);
CREATE TABLE cm_damage_matrix (
    fkcontext integer,
    sname text
);
CREATE TABLE cm_damage_matrix_parameter (
    fkcontext integer,
    sname text,
    nweight integer
);
CREATE TABLE cm_department (
    fkcontext integer,
    name text,
    description text
);
CREATE TABLE cm_financial_impact (
    fkcontext integer,
    sname text,
    nstartrange integer,
    nendrange integer
);
SET default_with_oids = false;
CREATE TABLE cm_group (
    fkcontext integer,
    name text,
    acronym text,
    fkpriority integer
);
CREATE TABLE cm_group_resource (
    fkcontext integer,
    fkgroup integer,
    fkresource integer,
    escalation integer
);
CREATE TABLE cm_impact (
    fkcontext integer,
    sname text,
    ntype integer
);
CREATE TABLE cm_impact_damage (
    fkcontext integer,
    fkimpact integer,
    fkscene integer
);
CREATE TABLE cm_impact_damage_range (
    fkcontext integer,
    fkdamage integer,
    fkdamageparameter integer,
    sfinancialimpact text,
    financialimpact integer,
    fkimpactdamage integer
);
SET default_with_oids = true;
CREATE TABLE cm_impact_type (
    fkcontext integer,
    sname text,
    sdescription text,
    nweight integer
);
CREATE TABLE cm_place (
    fkcontext integer,
    sname text,
    sdescription text,
    fktype integer,
    zipcode integer,
    scordinate text,
    saddress text,
    fkgroup integer,
    fkgroupsubstitute integer,
    fkresource integer,
    fkresourcesubstitute integer,
    fkcmt integer,
    fkparent integer,
    file_path character varying,
    file_name character varying,
    city text
);
SET default_with_oids = false;
CREATE TABLE cm_place_area (
    fkcontext integer,
    fkplace integer,
    fkarea integer
);
CREATE TABLE cm_place_asset (
    fkcontext integer,
    fkplace integer,
    fkasset integer
);
SET default_with_oids = true;
CREATE TABLE cm_place_category (
    fkcontext integer NOT NULL,
    fkparent integer,
    sname character varying(256) NOT NULL,
    tdescription text
);
CREATE TABLE cm_place_category_threat (
    fkcontext integer NOT NULL,
    fkplacecategory integer NOT NULL,
    fkthreat integer NOT NULL
);
CREATE TABLE cm_place_process (
    fkcontext integer,
    fkplace integer,
    fkprocess integer
);
SET default_with_oids = false;
CREATE TABLE cm_place_provider (
    fkcontext integer,
    fkplace integer,
    fkprovider integer
);
CREATE TABLE cm_place_resource (
    fkcontext integer,
    fkplace integer,
    fkresource integer,
    fkgroup integer,
    cmt integer NOT NULL
);
SET default_with_oids = true;
CREATE TABLE cm_place_threat (
    fkcontext integer,
    fkplace integer,
    fkthreat integer
);
CREATE TABLE cm_plan (
    fkcontext integer,
    name text,
    description text,
    fkprocess integer,
    fkplace integer,
    fkasset integer,
    fkarea integer
);
CREATE TABLE cm_plan_action (
    fkcontext integer,
    fkgroup integer,
    fkplanrange integer,
    resume text,
    description text,
    estimate integer,
    fkplan integer,
    fkaction integer,
    estimateflag integer,
    fkplantype integer,
    fkotherplan integer
);
CREATE TABLE cm_plan_action_test (
    fkcontext integer,
    fkplantest integer,
    fkplanaction integer,
    starttime timestamp without time zone,
    endtime timestamp without time zone,
    observation character varying
);
CREATE TABLE cm_plan_range (
    fkcontext integer,
    description text,
    startrange integer,
    endrange integer,
    endrangeflag integer,
    startrangeflag integer,
    fktype integer
);
CREATE TABLE cm_plan_range_type (
    fkcontext integer,
    description text,
    detail text
);
CREATE TABLE cm_plan_schedule (
    fkcontext integer,
    fkplan integer,
    status integer,
    date timestamp without time zone,
    fktest integer,
    fkresponsible integer,
    fkgroup integer
);
CREATE TABLE cm_plan_test (
    fkcontext integer,
    fkplan integer,
    observation character varying,
    starttime timestamp without time zone,
    endtime timestamp without time zone,
    fkresponsible integer
);
SET default_with_oids = false;
CREATE TABLE cm_priority (
    fkcontext integer,
    sname text,
    weight integer DEFAULT 0 NOT NULL
);
SET default_with_oids = true;
CREATE TABLE cm_process_activity (
    fkcontext integer,
    sname text,
    sdescription text,
    fkprocess integer,
    fkresponsible integer,
    fkpriority integer,
    fkgroup integer,
    fkimportance integer
);
CREATE TABLE cm_process_activity_asset (
    fkcontext integer,
    fkactivity integer,
    fkasset integer,
    fkimportance integer
);
CREATE TABLE cm_process_activity_people (
    fkcontext integer,
    fkactivity integer,
    fkuser integer
);
CREATE TABLE cm_process_category (
    fkcontext integer NOT NULL,
    fkparent integer,
    sname character varying(256) NOT NULL,
    tdescription text
);
CREATE TABLE cm_process_category_threat (
    fkcontext integer NOT NULL,
    fkprocesscategory integer NOT NULL,
    fkthreat integer NOT NULL
);
CREATE TABLE cm_process_inputs (
    fkcontext integer NOT NULL,
    fk_process integer,
    fk_process_input integer
);
CREATE TABLE cm_process_outputs (
    fkcontext integer NOT NULL,
    fk_process integer,
    fk_process_output integer
);
CREATE TABLE cm_process_threat (
    fkcontext integer,
    fkprocess integer,
    fkthreat integer
);
SET default_with_oids = false;
CREATE TABLE cm_provider (
    fkcontext integer,
    name text,
    description text,
    contract text,
    contracttype text,
    sla integer
);
CREATE TABLE cm_provider_resource (
    fkcontext integer,
    fkprovider integer,
    fkresource integer,
    bisdefault integer
);
CREATE TABLE cm_resource (
    fkcontext integer,
    name text,
    func text,
    provider integer,
    fkdepartment integer,
    comercialnumber text,
    celnumber text,
    homenumber text,
    nextelnumber text,
    email text
);
SET default_with_oids = true;
CREATE TABLE cm_scene (
    fkcontext integer,
    sname text,
    sdescription text,
    nrto integer,
    nrpo integer,
    nmtpd integer,
    dstartdate timestamp without time zone,
    denddate timestamp without time zone,
    nflags integer,
    fkprocess integer,
    nrto_flags integer,
    nrpo_flags integer,
    nmtpd_flags integer,
    nhourstart integer,
    nhourend integer,
    consequence text,
    fkprobability integer,
    fkseasonality integer
);
CREATE TABLE cm_scope (
    fkcontext integer,
    sname text,
    sdescription text
);
SET default_with_oids = false;
CREATE TABLE cm_scope_area (
    fkcontext integer,
    fkscope integer,
    fkarea integer
);
CREATE TABLE cm_scope_asset (
    fkcontext integer,
    fkscope integer,
    fkasset integer
);
CREATE TABLE cm_scope_place (
    fkcontext integer,
    fkscope integer,
    fkplace integer
);
SET default_with_oids = true;
CREATE TABLE cm_scope_process (
    fkcontext integer,
    fkscope integer,
    fkprocess integer
);
CREATE TABLE cm_threat (
    fkcontext integer,
    sname text,
    sdescription text,
    nprobability integer,
    ntype integer,
    nclass integer
);
CREATE TABLE cm_threat_probability (
    fkcontext integer,
    sname text,
    nprobability integer
);
CREATE TABLE cm_threat_vulnerability (
    fkcontext integer NOT NULL,
    fkplace integer NOT NULL,
    fkthreat integer NOT NULL,
    fklevel integer NOT NULL,
    description text,
    controls text,
    file_path character varying,
    file_name character varying
);
CREATE TABLE isms_context_date (
    fkcontext integer NOT NULL,
    naction integer DEFAULT 0 NOT NULL,
    ddate timestamp without time zone NOT NULL,
    nuserid integer NOT NULL,
    susername character varying(256) NOT NULL
);
CREATE VIEW context_history AS
    SELECT cc.fkcontext AS context_id, cc.ddate AS context_date_created, cc.susername AS context_creator_name, cm.ddate AS context_date_modified, cm.susername AS context_modifier_name FROM (isms_context_date cc JOIN isms_context_date cm ON (((cc.fkcontext = cm.fkcontext) AND (cc.naction < cm.naction))));
CREATE TABLE isms_context (
    pkcontext integer NOT NULL,
    ntype integer DEFAULT 0 NOT NULL,
    nstate integer DEFAULT 0
);
CREATE TABLE isms_profile (
    fkcontext integer NOT NULL,
    sname character varying(256) NOT NULL,
    nid integer DEFAULT 0
);
CREATE TABLE isms_user (
    fkcontext integer NOT NULL,
    fkprofile integer NOT NULL,
    sname character varying(256) NOT NULL,
    slogin character varying(256) NOT NULL,
    spassword character varying(256),
    semail character varying(256),
    nip integer DEFAULT 0,
    nlanguage integer DEFAULT 0,
    dlastlogin timestamp without time zone,
    nwronglogonattempts integer DEFAULT 0,
    bisblocked integer DEFAULT 0,
    bmustchangepassword integer DEFAULT 0,
    srequestpassword character varying(256),
    ssession character varying(256),
    bansweredsurvey integer DEFAULT 0,
    bshowhelp integer DEFAULT 1,
    CONSTRAINT ckc_bisblocked_isms_use CHECK (((bisblocked IS NULL) OR ((bisblocked >= 0) AND (bisblocked <= 1)))),
    CONSTRAINT ckc_bmustchangepasswo_isms_use CHECK (((bmustchangepassword IS NULL) OR ((bmustchangepassword >= 0) AND (bmustchangepassword <= 1))))
);
CREATE TABLE pm_doc_instance (
    fkcontext integer NOT NULL,
    fkdocument integer,
    nmajorversion integer DEFAULT 0,
    nrevisionversion integer DEFAULT 0,
    trevisionjustification text,
    spath character varying(256),
    tmodifycomment text,
    dbeginproduction timestamp without time zone,
    dendproduction timestamp without time zone,
    sfilename character varying(256),
    bislink integer DEFAULT 0,
    slink character varying(256),
    dbeginapprover timestamp without time zone,
    dbeginrevision timestamp without time zone,
    nfilesize integer DEFAULT 0,
    smanualversion character varying(256),
    CONSTRAINT ckc_bislink_pm_doc_i CHECK (((bislink IS NULL) OR ((bislink >= 0) AND (bislink <= 1))))
);
COMMENT ON TABLE pm_doc_instance IS 'Tabela com o histórico das diferentes versões de um documento.';
COMMENT ON COLUMN pm_doc_instance.nmajorversion IS 'Versão da instância.';
COMMENT ON COLUMN pm_doc_instance.trevisionjustification IS 'Motivo pelo qual o documento entrou em revisão.';
COMMENT ON COLUMN pm_doc_instance.spath IS 'Path do arquivo no servidor de arquivos.';
COMMENT ON COLUMN pm_doc_instance.tmodifycomment IS 'Comentário sobre o que foi modificado no documento.';
COMMENT ON COLUMN pm_doc_instance.dbeginproduction IS 'Data em que o documento entrou em produção.';
COMMENT ON COLUMN pm_doc_instance.dendproduction IS 'Data em que o documento saiu de produção.';
COMMENT ON COLUMN pm_doc_instance.sfilename IS 'Nome do arquivo.';
COMMENT ON COLUMN pm_doc_instance.bislink IS 'Indica se o documento é um link ao invés de um documento físico.';
COMMENT ON COLUMN pm_doc_instance.slink IS 'Link para o documento.';
CREATE TABLE pm_document (
    fkcontext integer NOT NULL,
    fkschedule integer,
    fkclassification integer,
    fkcurrentversion integer,
    fkparent integer,
    fkauthor integer,
    fkmainapprover integer,
    sname character varying(256) NOT NULL,
    tdescription text,
    ntype integer DEFAULT 0 NOT NULL,
    skeywords character varying(256),
    ddateproduction timestamp without time zone,
    ddeadline timestamp without time zone,
    bflagdeadlinealert integer DEFAULT 0,
    bflagdeadlineexpired integer DEFAULT 0,
    ndaysbefore integer DEFAULT 0,
    bhasapproved integer DEFAULT 0,
    CONSTRAINT ckc_bflagdeadlinealer_pm_docum CHECK (((bflagdeadlinealert IS NULL) OR ((bflagdeadlinealert >= 0) AND (bflagdeadlinealert <= 1)))),
    CONSTRAINT ckc_bflagdeadlineexpi_pm_docum CHECK (((bflagdeadlineexpired IS NULL) OR ((bflagdeadlineexpired >= 0) AND (bflagdeadlineexpired <= 1)))),
    CONSTRAINT ckc_bhasapproved_pm_docum CHECK (((bhasapproved IS NULL) OR ((bhasapproved >= 0) AND (bhasapproved <= 1))))
);
COMMENT ON TABLE pm_document IS 'Tabela de documentos.';
COMMENT ON COLUMN pm_document.sname IS 'Nome do documento.';
COMMENT ON COLUMN pm_document.tdescription IS 'Descrição do documento.';
COMMENT ON COLUMN pm_document.ntype IS 'Tipo do documento (template, registro, ...).';
COMMENT ON COLUMN pm_document.skeywords IS 'Palavras para fazer uma busca rápida por documentos.';
COMMENT ON COLUMN pm_document.ddateproduction IS 'Data em que o documento foi para produção.';
COMMENT ON COLUMN pm_document.ddeadline IS 'Prazo para que o documento vá para produção.';
COMMENT ON COLUMN pm_document.bflagdeadlinealert IS 'Indica se já enviou alerta que está chegando perto do prazo.';
COMMENT ON COLUMN pm_document.bflagdeadlineexpired IS 'Indica se já enviou o alerta que o prazo encerrou.';
COMMENT ON COLUMN pm_document.ndaysbefore IS 'Quantos dias antes deve ser enviado alerta que proximadade do prazo final.';
COMMENT ON COLUMN pm_document.bhasapproved IS 'Indica se já foi aprovado pelo aprovador principal.';
CREATE TABLE pm_register (
    fkcontext integer NOT NULL,
    fkclassification integer,
    fkdocument integer,
    fkresponsible integer,
    sname character varying(256) NOT NULL,
    nperiod integer DEFAULT 0 NOT NULL,
    nvalue integer DEFAULT 0 NOT NULL,
    sstorageplace character varying(256),
    sstoragetype character varying(256),
    sindexingtype character varying(256),
    sdisposition character varying(256),
    sprotectionrequirements character varying(256),
    sorigin character varying(256)
);
COMMENT ON COLUMN pm_register.sname IS 'Nome do registro.';
COMMENT ON COLUMN pm_register.nperiod IS 'Ano, mês, dia.';
COMMENT ON COLUMN pm_register.nvalue IS 'Valor. Ex: 5 (nValue) meses (nPeriod).';
CREATE TABLE pm_template (
    fkcontext integer NOT NULL,
    sname character varying(256) NOT NULL,
    ncontexttype integer DEFAULT 0,
    spath character varying(256),
    sfilename character varying(256),
    tdescription text,
    skeywords character varying(256),
    nfilesize integer DEFAULT 0
);
CREATE TABLE rm_area (
    fkcontext integer NOT NULL,
    fkpriority integer,
    fktype integer,
    fkparent integer,
    sname character varying(256) NOT NULL,
    tdescription text,
    sdocument character varying(256),
    nvalue double precision DEFAULT (0)::double precision,
    fkplace integer,
    fkgroup integer,
    fkgroupsubstitute integer,
    fkresource integer,
    fkresourcesubstitute integer
);

CREATE TABLE rm_area_provider(
  fkarea integer,
  fkprovider integer
);

CREATE TABLE rm_asset (
    fkcontext integer NOT NULL,
    sname character varying(256) NOT NULL,
    tdescription text,
    sdocument character varying(256),
    ncost double precision DEFAULT (0)::double precision,
    nvalue double precision DEFAULT (0)::double precision,
    blegality integer DEFAULT 0,
    tjustification text,
    ntac integer,
    ntrc integer,
    nrecoverytime integer,
    ncontingency integer,
    nconformity integer,
    fkcategory integer,
    contingency character varying(256),
    traflag integer,
    tacflag integer,
    fkgroup integer,
    fksecurityresponsible integer,
    alias text,
    model text,
    manufacturer text,
    nversion text,
    build text,
    serial text,
    ip text,
    mac text,
    status text,
    fkplace integer,
    file_path character varying,
    file_name character varying,
    fkprovider integer,
    CONSTRAINT ckc_blegality_rm_asset CHECK (((blegality IS NULL) OR ((blegality >= 0) AND (blegality <= 1))))
);
CREATE TABLE rm_best_practice (
    fkcontext integer NOT NULL,
    fksectionbestpractice integer NOT NULL,
    sname character varying(256) NOT NULL,
    tdescription text,
    ncontroltype integer DEFAULT 0,
    sclassification character varying(256),
    timplementationguide text,
    tmetric text,
    sdocument character varying(256),
    tjustification text
);
CREATE TABLE rm_category (
    fkcontext integer NOT NULL,
    fkparent integer,
    sname character varying(256) NOT NULL,
    tdescription text
);
CREATE TABLE rm_control (
    fkcontext integer NOT NULL,
    fktype integer,
    fkresponsible integer NOT NULL,
    sname character varying(256) NOT NULL,
    tdescription text,
    sdocument character varying(256),
    sevidence character varying(256),
    ndaysbefore integer DEFAULT 0,
    bisactive integer DEFAULT 0,
    bflagimplalert integer DEFAULT 0,
    bflagimplexpired integer DEFAULT 0,
    ddatedeadline timestamp without time zone,
    ddateimplemented timestamp without time zone,
    nimplementationstate integer DEFAULT 0,
    bimplementationislate integer DEFAULT 0,
    brevisionislate integer DEFAULT 0,
    btestislate integer DEFAULT 0,
    befficiencynotok integer DEFAULT 0,
    btestnotok integer DEFAULT 0,
    CONSTRAINT ckc_befficiencynotok_rm_contr CHECK (((befficiencynotok IS NULL) OR ((befficiencynotok >= 0) AND (befficiencynotok <= 1)))),
    CONSTRAINT ckc_bflagimplalert_rm_contr CHECK (((bflagimplalert IS NULL) OR ((bflagimplalert >= 0) AND (bflagimplalert <= 1)))),
    CONSTRAINT ckc_bflagimplexpired_rm_contr CHECK (((bflagimplexpired IS NULL) OR ((bflagimplexpired >= 0) AND (bflagimplexpired <= 1)))),
    CONSTRAINT ckc_bimplementationis_rm_contr CHECK (((bimplementationislate IS NULL) OR ((bimplementationislate >= 0) AND (bimplementationislate <= 1)))),
    CONSTRAINT ckc_bisactive_rm_contr CHECK (((bisactive IS NULL) OR ((bisactive >= 0) AND (bisactive <= 1)))),
    CONSTRAINT ckc_brevisionislate_rm_contr CHECK (((brevisionislate IS NULL) OR ((brevisionislate >= 0) AND (brevisionislate <= 1)))),
    CONSTRAINT ckc_btestislate_rm_contr CHECK (((btestislate IS NULL) OR ((btestislate >= 0) AND (btestislate <= 1)))),
    CONSTRAINT ckc_btestnotok_rm_contr CHECK (((btestnotok IS NULL) OR ((btestnotok >= 0) AND (btestnotok <= 1))))
);
CREATE TABLE rm_event (
    fkcontext integer NOT NULL,
    fktype integer,
    fkcategory integer NOT NULL,
    sdescription character varying(256) NOT NULL,
    tobservation text,
    bpropagate integer DEFAULT 0,
    timpact text,
    CONSTRAINT ckc_bpropagate_rm_event CHECK (((bpropagate IS NULL) OR ((bpropagate >= 0) AND (bpropagate <= 1))))
);
CREATE TABLE rm_process (
    fkcontext integer NOT NULL,
    fkarea integer NOT NULL,
    sname character varying(256) NOT NULL,
    tdescription text,
    sdocument character varying(256),
    nvalue double precision DEFAULT (0)::double precision,
    tinput text,
    toutput text,
    fkgroup integer,
    fkgroupsubstitute integer,
    fkresource integer,
    fkresourcesubstitute integer,
    fktype integer,
    fkpriority integer,
    fkseasonality integer
);
CREATE TABLE rm_process_asset (
    fkcontext integer NOT NULL,
    fkprocess integer,
    fkasset integer,
    fkimportance integer
);
CREATE TABLE rm_risk (
    fkcontext integer NOT NULL,
    fkprobabilityvaluename integer,
    fktype integer,
    fkevent integer,
    fkasset integer NOT NULL,
    sname character varying(256) NOT NULL,
    tdescription text,
    nvalue double precision DEFAULT (0)::double precision,
    nvalueresidual double precision DEFAULT (0)::double precision,
    tjustification text,
    nacceptmode integer DEFAULT 0,
    nacceptstate integer DEFAULT 0,
    sacceptjustification character varying(256),
    ncost double precision DEFAULT (0)::double precision,
    timpact text
);
CREATE TABLE rm_risk_control (
    fkcontext integer NOT NULL,
    fkprobabilityvaluename integer,
    fkrisk integer,
    fkcontrol integer,
    tjustification text
);
CREATE TABLE rm_risk_limits (
    fkcontext integer NOT NULL,
    nlow double precision NOT NULL,
    nhigh double precision NOT NULL
);
CREATE TABLE rm_section_best_practice (
    fkcontext integer NOT NULL,
    fkparent integer,
    sname character varying(256) NOT NULL
);
CREATE TABLE rm_standard (
    fkcontext integer NOT NULL,
    sname character varying(256) NOT NULL,
    tdescription text,
    tapplication text,
    tobjective text
);
CREATE VIEW context_names AS
    (((((((((((((((((((((((((SELECT c.pkcontext AS context_id, c.ntype AS context_type, c.nstate AS context_state, t.sname AS context_name FROM (isms_context c JOIN isms_user t ON ((c.pkcontext = t.fkcontext))) UNION SELECT c.pkcontext AS context_id, c.ntype AS context_type, c.nstate AS context_state, t.sname AS context_name FROM (isms_context c JOIN isms_profile t ON ((c.pkcontext = t.fkcontext)))) UNION SELECT c.pkcontext AS context_id, c.ntype AS context_type, c.nstate AS context_state, t.sname AS context_name FROM (isms_context c JOIN rm_area t ON ((c.pkcontext = t.fkcontext)))) UNION SELECT c.pkcontext AS context_id, c.ntype AS context_type, c.nstate AS context_state, t.sname AS context_name FROM (isms_context c JOIN rm_process t ON ((c.pkcontext = t.fkcontext)))) UNION SELECT c.pkcontext AS context_id, c.ntype AS context_type, c.nstate AS context_state, t.sname AS context_name FROM (isms_context c JOIN rm_asset t ON ((c.pkcontext = t.fkcontext)))) UNION SELECT c.pkcontext AS context_id, c.ntype AS context_type, c.nstate AS context_state, t.sname AS context_name FROM (isms_context c JOIN rm_risk t ON ((c.pkcontext = t.fkcontext)))) UNION SELECT c.pkcontext AS context_id, c.ntype AS context_type, c.nstate AS context_state, t.sname AS context_name FROM (isms_context c JOIN rm_control t ON ((c.pkcontext = t.fkcontext)))) UNION SELECT c.pkcontext AS context_id, c.ntype AS context_type, c.nstate AS context_state, t.sname AS context_name FROM (isms_context c JOIN rm_category t ON ((c.pkcontext = t.fkcontext)))) UNION SELECT c.pkcontext AS context_id, c.ntype AS context_type, c.nstate AS context_state, t.sdescription AS context_name FROM (isms_context c JOIN rm_event t ON ((c.pkcontext = t.fkcontext)))) UNION SELECT c.pkcontext AS context_id, c.ntype AS context_type, c.nstate AS context_state, t.sname AS context_name FROM (isms_context c JOIN rm_best_practice t ON ((c.pkcontext = t.fkcontext)))) UNION SELECT c.pkcontext AS context_id, c.ntype AS context_type, c.nstate AS context_state, t.sname AS context_name FROM (isms_context c JOIN rm_standard t ON ((c.pkcontext = t.fkcontext)))) UNION SELECT c.pkcontext AS context_id, c.ntype AS context_type, c.nstate AS context_state, (((p.sname)::text || ' X '::text) || (a.sname)::text) AS context_name FROM (((isms_context c JOIN rm_process_asset pa ON ((c.pkcontext = pa.fkcontext))) JOIN rm_process p ON ((pa.fkprocess = p.fkcontext))) JOIN rm_asset a ON ((pa.fkasset = a.fkcontext)))) UNION SELECT c.pkcontext AS context_id, c.ntype AS context_type, c.nstate AS context_state, t.sname AS context_name FROM (isms_context c JOIN rm_section_best_practice t ON ((c.pkcontext = t.fkcontext)))) UNION SELECT c.pkcontext AS context_id, c.ntype AS context_type, c.nstate AS context_state, (((r.sname)::text || ' X '::text) || (co.sname)::text) AS context_name FROM (((isms_context c JOIN rm_risk_control rc ON ((c.pkcontext = rc.fkcontext))) JOIN rm_risk r ON ((rc.fkrisk = r.fkcontext))) JOIN rm_control co ON ((rc.fkcontrol = co.fkcontext)))) UNION SELECT c.pkcontext AS context_id, c.ntype AS context_type, c.nstate AS context_state, t.sname AS context_name FROM (isms_context c JOIN pm_document t ON ((c.pkcontext = t.fkcontext)))) UNION SELECT c.pkcontext AS context_id, c.ntype AS context_type, c.nstate AS context_state, ((((((d.sname)::text || ' ('::text) || (t.nmajorversion)::text) || '.'::text) || (t.nrevisionversion)::text) || ')'::text) AS context_name FROM ((isms_context c JOIN pm_doc_instance t ON ((c.pkcontext = t.fkcontext))) JOIN pm_document d ON ((t.fkdocument = d.fkcontext)))) UNION SELECT c.pkcontext AS context_id, c.ntype AS context_type, c.nstate AS context_state, t.sname AS context_name FROM (isms_context c JOIN ci_category t ON ((c.pkcontext = t.fkcontext)))) UNION SELECT c.pkcontext AS context_id, c.ntype AS context_type, c.nstate AS context_state, (t.tdescription)::character varying AS context_name FROM (isms_context c JOIN ci_occurrence t ON ((c.pkcontext = t.fkcontext)))) UNION SELECT c.pkcontext AS context_id, c.ntype AS context_type, c.nstate AS context_state, t.sname AS context_name FROM (isms_context c JOIN ci_incident t ON ((c.pkcontext = t.fkcontext)))) UNION SELECT c.pkcontext AS context_id, c.ntype AS context_type, c.nstate AS context_state, t.sname AS context_name FROM (isms_context c JOIN ci_nc t ON ((c.pkcontext = t.fkcontext)))) UNION SELECT c.pkcontext AS context_id, c.ntype AS context_type, c.nstate AS context_state, t.sname AS context_name FROM (isms_context c JOIN pm_template t ON ((c.pkcontext = t.fkcontext)))) UNION SELECT c.pkcontext AS context_id, c.ntype AS context_type, c.nstate AS context_state, '' AS context_name FROM (isms_context c JOIN ci_incident_risk ir ON ((ir.fkcontext = c.pkcontext)))) UNION SELECT c.pkcontext AS context_id, c.ntype AS context_type, c.nstate AS context_state, (((ctrl.sname)::text || ' -> '::text) || (i.sname)::text) AS context_name FROM (((isms_context c JOIN ci_incident_control ic ON ((c.pkcontext = ic.fkcontext))) JOIN rm_control ctrl ON ((ic.fkcontrol = ctrl.fkcontext))) JOIN ci_incident i ON ((ic.fkincident = i.fkcontext)))) UNION SELECT c.pkcontext AS context_id, c.ntype AS context_type, c.nstate AS context_state, '' AS context_name FROM (isms_context c JOIN rm_risk_limits rl ON ((c.pkcontext = rl.fkcontext)))) UNION SELECT c.pkcontext AS context_id, c.ntype AS context_type, c.nstate AS context_state, (s.tproblem)::character varying AS context_name FROM (isms_context c JOIN ci_solution s ON ((c.pkcontext = s.fkcontext)))) UNION SELECT c.pkcontext AS context_id, c.ntype AS context_type, c.nstate AS context_state, ap.sname AS context_name FROM (isms_context c JOIN ci_action_plan ap ON ((c.pkcontext = ap.fkcontext)))) UNION SELECT c.pkcontext AS context_id, c.ntype AS context_type, c.nstate AS context_state, r.sname AS context_name FROM (isms_context c JOIN pm_register r ON ((c.pkcontext = r.fkcontext)));
CREATE TABLE isms_audit_log (
    ddate timestamp without time zone NOT NULL,
    suser character varying(256),
    naction integer DEFAULT 0,
    starget character varying(256),
    tdescription text
);
CREATE TABLE isms_config (
    pkconfig integer NOT NULL,
    svalue character varying(256)
);
CREATE TABLE isms_context_classification (
    pkclassification integer NOT NULL,
    ncontexttype integer DEFAULT 0 NOT NULL,
    sname character varying(256) NOT NULL,
    nclassificationtype integer DEFAULT 0 NOT NULL
);
CREATE SEQUENCE isms_context_classification_pkclassification_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
ALTER SEQUENCE isms_context_classification_pkclassification_seq OWNED BY isms_context_classification.pkclassification;
CREATE TABLE isms_context_history (
    pkid integer NOT NULL,
    fkcontext integer,
    stype character varying(256) NOT NULL,
    ddate timestamp without time zone NOT NULL,
    nvalue double precision DEFAULT (0)::double precision NOT NULL
);
CREATE SEQUENCE isms_context_history_pkid_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
ALTER SEQUENCE isms_context_history_pkid_seq OWNED BY isms_context_history.pkid;
CREATE SEQUENCE isms_context_pkcontext_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
ALTER SEQUENCE isms_context_pkcontext_seq OWNED BY isms_context.pkcontext;

CREATE TABLE isms_non_conformity_types (
    pkclassification integer NOT NULL,
    sname character varying(256) NOT NULL
);
CREATE SEQUENCE isms_non_conformity_types_pkclassification_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
ALTER SEQUENCE isms_non_conformity_types_pkclassification_seq OWNED BY isms_non_conformity_types.pkclassification;
CREATE TABLE isms_policy (
    fkcontext integer NOT NULL,
    tdescription text NOT NULL
);
CREATE TABLE isms_profile_acl (
    fkprofile integer NOT NULL,
    stag character varying(256) NOT NULL
);
CREATE TABLE isms_saas (
    pkconfig integer NOT NULL,
    svalue character varying(256) NOT NULL
);
CREATE SEQUENCE isms_saas_pkconfig_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
ALTER SEQUENCE isms_saas_pkconfig_seq OWNED BY isms_saas.pkconfig;
CREATE TABLE isms_scope (
    fkcontext integer NOT NULL,
    tdescription text NOT NULL
);
CREATE TABLE isms_solicitor (
    fkuser integer NOT NULL,
    fksolicitor integer NOT NULL,
    ddatebegin timestamp without time zone,
    ddatefinish timestamp without time zone
);
CREATE TABLE isms_user_password_history (
    fkuser integer,
    spassword character varying(256) NOT NULL,
    ddatepasswordchanged timestamp without time zone NOT NULL
);
CREATE TABLE isms_user_preference (
    fkuser integer NOT NULL,
    spreference character varying(256) NOT NULL,
    tvalue text
);
CREATE TABLE pm_doc_approvers (
    fkdocument integer NOT NULL,
    fkuser integer NOT NULL,
    bhasapproved integer DEFAULT 0,
    bmanual integer DEFAULT 0,
    bcontextapprover integer DEFAULT 0,
    CONSTRAINT ckc_bcontextapprover_pm_doc_a CHECK (((bcontextapprover IS NULL) OR ((bcontextapprover >= 0) AND (bcontextapprover <= 1)))),
    CONSTRAINT ckc_bhasapproved_pm_doc_a CHECK (((bhasapproved IS NULL) OR ((bhasapproved >= 0) AND (bhasapproved <= 1)))),
    CONSTRAINT ckc_bmanual_pm_doc_a CHECK (((bmanual IS NULL) OR ((bmanual >= 0) AND (bmanual <= 1))))
);
COMMENT ON TABLE pm_doc_approvers IS 'Tabela que relaciona os documentos com seus respectivos aprovadores.';
COMMENT ON COLUMN pm_doc_approvers.bhasapproved IS 'Indica se o usuáio aprovou o documento.';
CREATE TABLE pm_doc_context (
    fkcontext integer NOT NULL,
    fkdocument integer NOT NULL
);
CREATE TABLE pm_doc_read_history (
    ddate timestamp without time zone NOT NULL,
    fkinstance integer NOT NULL,
    fkuser integer NOT NULL
);
CREATE TABLE pm_doc_readers (
    fkdocument integer NOT NULL,
    fkuser integer NOT NULL,
    bhasread integer DEFAULT 0 NOT NULL,
    bmanual integer DEFAULT 0,
    bdenied integer DEFAULT 0,
    CONSTRAINT ckc_bdenied_pm_doc_r CHECK (((bdenied IS NULL) OR ((bdenied >= 0) AND (bdenied <= 1)))),
    CONSTRAINT ckc_bhasread_pm_doc_r CHECK (((bhasread >= 0) AND (bhasread <= 1))),
    CONSTRAINT ckc_bmanual_pm_doc_r CHECK (((bmanual IS NULL) OR ((bmanual >= 0) AND (bmanual <= 1))))
);
COMMENT ON COLUMN pm_doc_readers.bmanual IS 'Campo para indicar se o usuário foi inserido manualmente, ou seja,
não foi inserido de forma automática pelo sistema.';
COMMENT ON COLUMN pm_doc_readers.bdenied IS 'Campo para indicar que foi negado o acesso do usuário ao documento.';
CREATE TABLE pm_doc_reference (
    pkreference integer NOT NULL,
    fkdocument integer,
    sname character varying(256) NOT NULL,
    slink character varying(256),
    dcreationdate timestamp without time zone
);
COMMENT ON COLUMN pm_doc_reference.sname IS 'Nome da referência.';
COMMENT ON COLUMN pm_doc_reference.slink IS 'Link para a referência.';
COMMENT ON COLUMN pm_doc_reference.dcreationdate IS 'Data de inserção.';
CREATE SEQUENCE pm_doc_reference_pkreference_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
ALTER SEQUENCE pm_doc_reference_pkreference_seq OWNED BY pm_doc_reference.pkreference;
CREATE TABLE pm_doc_registers (
    fkregister integer NOT NULL,
    fkdocument integer NOT NULL
);
CREATE TABLE pm_document_revision_history (
    fkdocument integer NOT NULL,
    ddaterevision timestamp without time zone NOT NULL,
    sjustification text,
    bnewversion integer DEFAULT 0,
    CONSTRAINT ckc_bnewversion_pm_docum CHECK (((bnewversion IS NULL) OR ((bnewversion >= 0) AND (bnewversion <= 1))))
);
COMMENT ON TABLE pm_document_revision_history IS 'Armazena o histórico de revisão do documento.';
COMMENT ON COLUMN pm_document_revision_history.ddaterevision IS 'Data em que ocorreu a revisão.';
COMMENT ON COLUMN pm_document_revision_history.sjustification IS 'Justificativa para a revisão.';
CREATE TABLE pm_instance_comment (
    pkcomment integer NOT NULL,
    fkinstance integer,
    fkuser integer,
    ddate timestamp without time zone NOT NULL,
    tcomment text NOT NULL
);
COMMENT ON COLUMN pm_instance_comment.ddate IS 'Data em que foi feito o comentário.';
CREATE SEQUENCE pm_instance_comment_pkcomment_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
ALTER SEQUENCE pm_instance_comment_pkcomment_seq OWNED BY pm_instance_comment.pkcomment;
CREATE TABLE pm_instance_content (
    fkinstance integer NOT NULL,
    tcontent text
);
COMMENT ON COLUMN pm_instance_content.tcontent IS 'Conteúdo do arquivo físico.';
CREATE TABLE pm_process_user (
    fkprocess integer NOT NULL,
    fkuser integer NOT NULL
);
CREATE TABLE pm_register_readers (
    fkuser integer NOT NULL,
    fkregister integer NOT NULL,
    bmanual integer DEFAULT 0,
    bdenied integer DEFAULT 0,
    CONSTRAINT ckc_bdenied_pm_regis CHECK (((bdenied IS NULL) OR ((bdenied >= 0) AND (bdenied <= 1)))),
    CONSTRAINT ckc_bmanual_pm_regis CHECK (((bmanual IS NULL) OR ((bmanual >= 0) AND (bmanual <= 1))))
);
CREATE TABLE pm_template_best_practice (
    fktemplate integer NOT NULL,
    fkbestpractice integer NOT NULL
);
CREATE TABLE pm_template_content (
    fkcontext integer NOT NULL,
    tcontent text
);
CREATE TABLE rm_asset_asset (
    fkasset integer NOT NULL,
    fkdependent integer NOT NULL
);
CREATE TABLE rm_asset_value (
    fkasset integer NOT NULL,
    fkvaluename integer NOT NULL,
    fkparametername integer NOT NULL,
    tjustification text
);
CREATE TABLE rm_best_practice_event (
    fkcontext integer NOT NULL,
    fkbestpractice integer,
    fkevent integer
);
CREATE TABLE rm_best_practice_standard (
    fkcontext integer NOT NULL,
    fkstandard integer,
    fkbestpractice integer
);
CREATE TABLE rm_control_best_practice (
    fkcontext integer NOT NULL,
    fkbestpractice integer,
    fkcontrol integer
);
CREATE TABLE rm_control_cost (
    fkcontrol integer NOT NULL,
    ncost1 double precision DEFAULT (0)::double precision,
    ncost2 double precision DEFAULT (0)::double precision,
    ncost3 double precision DEFAULT (0)::double precision,
    ncost4 double precision DEFAULT (0)::double precision,
    ncost5 double precision DEFAULT (0)::double precision
);
CREATE TABLE rm_control_efficiency_history (
    fkcontrol integer NOT NULL,
    ddatetodo timestamp without time zone NOT NULL,
    ddateaccomplishment timestamp without time zone,
    nrealefficiency integer DEFAULT 0,
    nexpectedefficiency integer DEFAULT 0,
    bincident integer DEFAULT 0,
    CONSTRAINT ckc_bincident_rm_contr CHECK (((bincident IS NULL) OR ((bincident >= 0) AND (bincident <= 1))))
);
CREATE TABLE rm_control_test_history (
    fkcontrol integer NOT NULL,
    ddatetodo timestamp without time zone NOT NULL,
    ddateaccomplishment timestamp without time zone,
    btestedvalue integer DEFAULT 0,
    tdescription text,
    CONSTRAINT ckc_btestedvalue_rm_contr CHECK (((btestedvalue IS NULL) OR ((btestedvalue >= 0) AND (btestedvalue <= 1))))
);
CREATE TABLE rm_parameter_name (
    pkparametername integer NOT NULL,
    sname character varying(256) NOT NULL,
    nweight integer DEFAULT 1
);
CREATE SEQUENCE rm_parameter_name_pkparametername_seq
    START WITH 10
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
ALTER SEQUENCE rm_parameter_name_pkparametername_seq OWNED BY rm_parameter_name.pkparametername;
CREATE TABLE rm_parameter_value_name (
    pkvaluename integer NOT NULL,
    nvalue integer DEFAULT 1,
    simportance character varying(256),
    simpact character varying(256),
    sriskprobability character varying(256),
    CONSTRAINT ckc_nvalue_rm_param CHECK (((nvalue IS NULL) OR ((nvalue >= 1) AND (nvalue <= 5))))
);
CREATE SEQUENCE rm_parameter_value_name_pkvaluename_seq
    START WITH 10
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
ALTER SEQUENCE rm_parameter_value_name_pkvaluename_seq OWNED BY rm_parameter_value_name.pkvaluename;
CREATE TABLE rm_rc_parameter_value_name (
    pkrcvaluename integer NOT NULL,
    nvalue integer DEFAULT 0,
    srcprobability character varying(256),
    scontrolimpact character varying(256)
);
CREATE SEQUENCE rm_rc_parameter_value_name_pkrcvaluename_seq
    START WITH 10
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
ALTER SEQUENCE rm_rc_parameter_value_name_pkrcvaluename_seq OWNED BY rm_rc_parameter_value_name.pkrcvaluename;
CREATE TABLE rm_risk_control_value (
    fkriskcontrol integer NOT NULL,
    fkparametername integer NOT NULL,
    fkrcvaluename integer NOT NULL,
    tjustification text
);
CREATE TABLE rm_risk_value (
    fkrisk integer NOT NULL,
    fkvaluename integer NOT NULL,
    fkparametername integer NOT NULL,
    tjustification text
);


CREATE FUNCTION create_stats() RETURNS integer
    LANGUAGE plpgsql
    AS $$DECLARE
risk_low FLOAT;
risk_high FLOAT;
quantity INTEGER;
cost FLOAT;
id_value id_value%rowtype;
id_values id_values%rowtype;
control_cost control_cost%rowtype;
query varchar(4096);
periodicy_value INTEGER;
periodicy_type INTEGER;
date_last_run TIMESTAMP;

BEGIN

/*======================================TESTE PARA VER SE NECESSITA EXECUTAR A FUNÇÃO=========================================*/

periodicy_value = (SELECT sValue FROM isms_config WHERE pkConfig = 417);
periodicy_type = (SELECT sValue FROM isms_config WHERE pkConfig = 418);
date_last_run = (SELECT sValue FROM isms_config WHERE pkConfig = 420);

IF get_next_date(date_last_run, periodicy_type, periodicy_value) <= NOW()
THEN

SELECT rl.nLow, rl.nHigh
INTO   risk_low,risk_high
FROM rm_risk_limits rl JOIN isms_context c ON (rl.fkContext = c.pkContext)
WHERE c.nState = 2702;
/*===============================================INFORMAÇÕES SOBRE �?REA========================================================*/


query := 'SELECT fkContext, nValue FROM view_rm_area_active';

FOR id_value IN EXECUTE query LOOP
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (id_value.id, now(), 'area_value', id_value.value);
END LOOP;


quantity := (SELECT count(*) FROM view_rm_area_active);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), 'areas_total', quantity);


quantity := (SELECT count(*) FROM view_rm_area_active WHERE nValue >= risk_high);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), 'areas_with_high_risk', quantity);


quantity := (SELECT count(*) FROM view_rm_area_active WHERE nValue > risk_low AND nValue < risk_high);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), 'areas_with_medium_risk', quantity);


quantity := (SELECT count(*) FROM view_rm_area_active WHERE nValue <= risk_low AND nValue <> 0);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), 'areas_with_low_risk', quantity);


quantity := (SELECT count(*) FROM view_rm_area_active WHERE nValue = 0);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), 'areas_with_no_value_risk', quantity);


quantity := (
	SELECT count(*) FROM view_rm_area_active a
	WHERE a.nValue > 0 AND NOT EXISTS (SELECT * FROM view_rm_process_active p WHERE p.fkArea = a.fkContext AND p.nValue=0)
);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), 'complete_areas', quantity);


quantity := (
	SELECT count(*) FROM view_rm_area_active a
	WHERE a.nValue > 0 AND EXISTS (SELECT * FROM view_rm_process_active p WHERE p.fkArea = a.fkContext AND p.nValue = 0)
);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), 'partial_areas', quantity);


quantity := (SELECT count(*) FROM view_rm_area_active a WHERE a.nValue = 0);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), 'not_managed_areas', quantity);


quantity := (
	SELECT count(*)
	FROM view_rm_area_active a
	WHERE a.fkContext NOT IN (SELECT fkArea FROM view_rm_process_active)
);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), 'areas_without_processes', quantity);


query := 'SELECT area_id, count(risk_id) as risk_count, CASE WHEN sum(r.nCost) IS NULL THEN 0 ELSE sum(r.nCost) END as total_impact
		FROM
		(
			SELECT ar.fkContext as area_id, r.fkContext as risk_id
			FROM view_rm_area_active ar
			LEFT JOIN view_rm_process_active p ON (ar.fkContext = p.fkArea)
			LEFT JOIN view_rm_process_asset_active pa ON (p.fkContext = pa.fkProcess)
			LEFT JOIN view_rm_asset_active a ON (pa.fkAsset = a.fkContext)
			LEFT JOIN view_rm_risk_active r ON (a.fkContext = r.fkAsset)
			GROUP BY ar.fkContext, r.fkContext
		) res
		LEFT JOIN view_rm_risk_active r ON (res.risk_id = r.fkContext)
		GROUP BY area_id';

FOR id_values IN EXECUTE query LOOP	
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (id_values.id, now(), 'risk_amount_by_area', id_values.value1);
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (id_values.id, now(), 'total_area_impact', id_values.value2);
END LOOP;


query := '
	/*Calcula o custo total dos controles de cada área e a quantidade de controles de cada área*/
	SELECT res.area_id, count(control_id) as control_count, CASE WHEN sum(cc.nCost1+cc.nCost2+cc.nCost3+cc.nCost4+cc.nCost5) IS NULL THEN 0 ELSE sum(cc.nCost1+cc.nCost2+cc.nCost3+cc.nCost4+cc.nCost5) END as control_cost
	FROM
	(
		/*Garante que um controle só irá ser contabilizado uma vez para uma determinada área*/
		SELECT ar.fkContext as area_id, rc.fkControl as control_id
		FROM view_rm_area_active ar
		LEFT JOIN view_rm_process_active p ON (ar.fkContext = p.fkArea)
		LEFT JOIN view_rm_process_asset_active pa ON (p.fkContext = pa.fkProcess)
		LEFT JOIN view_rm_asset_active a ON (pa.fkAsset = a.fkContext)
		LEFT JOIN view_rm_risk_active r ON (a.fkContext = r.fkAsset)
		LEFT JOIN view_rm_risk_control_active rc ON (r.fkContext = rc.fkRisk)
		GROUP BY ar.fkContext, rc.fkControl
	) res
	LEFT JOIN rm_control_cost cc ON (res.control_id = cc.fkControl)
	GROUP BY area_id';

FOR id_values IN EXECUTE query LOOP	
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (id_values.id, now(), 'control_amount_by_area', id_values.value1);
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (id_values.id, now(), 'control_cost_by_area', id_values.value2);	
END LOOP;
	

query := 'SELECT a.fkContext as area_id, count(p.fkContext) as process_count
	FROM view_rm_area_active a
	LEFT JOIN view_rm_process_active p ON (a.fkContext = p.fkArea)
	GROUP BY a.fkContext';

FOR id_value IN EXECUTE query LOOP	
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (id_value.id, now(), 'process_amount_by_area', id_value.value);	
END LOOP;


query := '
	SELECT area_id, count(asset_id) as asset_count, CASE WHEN sum(a.nCost) IS NULL THEN 0 ELSE sum(a.nCost) END as asset_cost
	FROM
	(
		SELECT a.fkContext as area_id, pa.fkAsset as asset_id
		FROM view_rm_area_active a
		LEFT JOIN view_rm_process_active p ON (a.fkContext = p.fkArea)
		LEFT JOIN view_rm_process_asset_active pa ON (p.fkContext = pa.fkProcess)
		GROUP BY a.fkContext, pa.fkAsset
	) res
	LEFT JOIN view_rm_asset_active a ON (res.asset_id = a.fkContext)
	GROUP BY area_id';

FOR id_values IN EXECUTE query LOOP	
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (id_values.id, now(), 'asset_amount_by_area', id_values.value1);
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (id_values.id, now(), 'asset_cost_by_area', id_values.value2);	
END LOOP;

/*===============================================INFORMAÇÕES SOBRE PROCESSO========================================================*/


query := 'SELECT fkContext, nValue FROM view_rm_process_active';

FOR id_value IN EXECUTE query LOOP	
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (id_value.id, now(), 'process_value', id_value.value);	
END LOOP;


query := 'SELECT p.fkContext, count(pa.fkAsset) as asset_count, CASE WHEN sum(a.nCost) IS NULL THEN 0 ELSE sum(a.nCost) END
	FROM view_rm_process_active p
	LEFT JOIN view_rm_process_asset_active pa ON (p.fkContext = pa.fkProcess)
	LEFT JOIN view_rm_asset_active a ON (pa.fkAsset = a.fkContext)
	GROUP BY p.fkContext';

FOR id_values IN EXECUTE query LOOP	
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (id_values.id, now(), 'asset_amount_by_process', id_values.value1);
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (id_values.id, now(), 'asset_cost_by_process', id_values.value2);	
END LOOP;
	

query := 'SELECT p.fkContext, count(r.fkContext) as risk_count
	FROM view_rm_process_active p
	LEFT JOIN view_rm_process_asset_active pa ON (p.fkContext = pa.fkProcess)
	LEFT JOIN view_rm_asset_active a ON (pa.fkAsset = a.fkContext)
	LEFT JOIN view_rm_risk_active r ON (a.fkContext = r.fkAsset)
	GROUP BY p.fkContext';

FOR id_value IN EXECUTE query LOOP	
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (id_value.id, now(), 'risk_amount_by_process', id_value.value);	
END LOOP;


quantity := (SELECT count(*) FROM view_rm_process_active);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), 'process_total', quantity);


quantity := (SELECT count(*) FROM view_rm_process_active WHERE nValue >= risk_high);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), 'processes_with_high_risk', quantity);


quantity := (SELECT count(*) FROM view_rm_process_active WHERE nValue > risk_low AND nValue < risk_high);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), 'processes_with_medium_risk', quantity);


quantity := (SELECT count(*) FROM view_rm_process_active WHERE nValue <= risk_low AND nValue <> 0);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), 'processes_with_low_risk', quantity);


quantity := (SELECT count(*) FROM view_rm_process_active WHERE nValue = 0);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), 'processes_with_no_value_risk', quantity);


quantity := (
	SELECT count(*)
	FROM view_rm_process_active p
	WHERE p.nValue > 0 AND NOT EXISTS (
		SELECT * 
		FROM view_rm_process_asset_active pa 
		JOIN view_rm_asset_active a ON (pa.fkAsset = a.fkContext) 
		WHERE pa.fkProcess = p.fkContext AND a.nValue = 0
	)
);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), 'complete_processes', quantity);


quantity := (
	SELECT count(*)
	FROM view_rm_process_active p
	WHERE p.nValue > 0 AND EXISTS (
		SELECT * 
		FROM view_rm_process_asset_active pa 
		JOIN view_rm_asset_active a ON (pa.fkAsset = a.fkContext) 
		WHERE pa.fkProcess = p.fkContext AND a.nValue = 0
	)
);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), 'partial_processes', quantity);


quantity := (SELECT count(*) FROM view_rm_process_active p WHERE p.nValue = 0);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), 'not_managed_processes', quantity);


quantity := (
	SELECT count(*)
	FROM view_rm_process_active p
	WHERE p.fkContext NOT IN (SELECT fkProcess FROM view_rm_process_asset_active)
);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), 'processes_without_assets', quantity);


query := 'SELECT p.fkContext as process_id, CASE WHEN sum(r.nCost) IS NULL THEN 0 ELSE sum(r.nCost) END as total_impact
	FROM view_rm_process_active p
	LEFT JOIN view_rm_process_asset_active pa ON (p.fkContext = pa.fkProcess)
	LEFT JOIN view_rm_asset_active a ON (pa.fkAsset = a.fkContext)
	LEFT JOIN view_rm_risk_active r ON (a.fkContext = r.fkAsset)
	GROUP BY p.fkContext';

FOR id_value IN EXECUTE query LOOP	
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (id_value.id, now(), 'total_process_impact', id_value.value);	
END LOOP;


query := '
	/*Calcula o custo total dos controles de cada processo e a quantidade de controles de cada processo*/
	SELECT process_id, count(control_id) as control_count, CASE WHEN sum(cc.nCost1+cc.nCost2+cc.nCost3+cc.nCost4+cc.nCost5) IS NULL THEN 0 ELSE sum(cc.nCost1+cc.nCost2+cc.nCost3+cc.nCost4+cc.nCost5) END as control_cost
	FROM
	(
		/*Garante que um controle só irá ser contabilizado uma vez para um determinado processo*/
		SELECT p.fkContext as process_id, rc.fkControl as control_id
		FROM view_rm_process_active p
		LEFT JOIN view_rm_process_asset_active pa ON (p.fkContext = pa.fkProcess)
		LEFT JOIN view_rm_asset_active a ON (pa.fkAsset = a.fkContext)
		LEFT JOIN view_rm_risk_active r ON (a.fkContext = r.fkAsset)
		LEFT JOIN view_rm_risk_control_active rc ON (r.fkContext = rc.fkRisk)
		GROUP BY p.fkContext, rc.fkControl
	) res
	LEFT JOIN rm_control_cost cc ON (res.control_id = cc.fkControl)
	GROUP BY process_id';

FOR id_values IN EXECUTE query LOOP	
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (id_values.id, now(), 'control_amount_by_process', id_values.value1);
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (id_values.id, now(), 'control_cost_by_process', id_values.value2);
END LOOP;

/*===============================================INFORMAÇÕES SOBRE ATIVO========================================================*/


query := 'SELECT fkContext, nValue FROM view_rm_asset_active';

FOR id_value IN EXECUTE query LOOP	
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (id_value.id, now(), 'asset_value', id_value.value);	
END LOOP;	


query := 'SELECT a.fkContext, count(pa.fkProcess) as process_count
	FROM view_rm_asset_active a
	LEFT JOIN view_rm_process_asset_active pa ON (a.fkContext = pa.fkAsset)
	GROUP BY a.fkContext';

FOR id_value IN EXECUTE query LOOP	
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (id_value.id, now(), 'process_amount_by_asset', id_value.value);	
END LOOP;


query := 'SELECT a.fkContext, count(r.fkContext) as risk_count
	FROM view_rm_asset_active a
	LEFT JOIN view_rm_risk_active r ON (a.fkContext = r.fkAsset)
	GROUP BY a.fkContext';

FOR id_value IN EXECUTE query LOOP	
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (id_value.id, now(), 'risk_amount_by_asset', id_value.value);	
END LOOP;


quantity := (SELECT count(*) FROM view_rm_asset_active);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), 'asset_total', quantity);


quantity := (SELECT count(*) FROM view_rm_asset_active WHERE nValue >= risk_high);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), 'assets_with_high_risk', quantity);


quantity := (SELECT count(*) FROM view_rm_asset_active WHERE nValue > risk_low AND nValue < risk_high);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), 'assets_with_medium_risk', quantity);


quantity := (SELECT count(*) FROM view_rm_asset_active WHERE nValue <= risk_low AND nValue <> 0);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), 'assets_with_low_risk', quantity);


quantity := (SELECT count(*) FROM view_rm_asset_active WHERE nValue = 0);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), 'assets_with_no_value_risk', quantity);


quantity := (
	SELECT count(*) FROM view_rm_asset_active a
	WHERE a.nValue > 0 AND NOT EXISTS (
		SELECT * FROM view_rm_risk_active r WHERE r.fkAsset = a.fkContext AND r.nValue = 0
	)
);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), 'complete_assets', quantity);


quantity := (
	SELECT count(*) FROM view_rm_asset_active a
	WHERE a.nValue > 0 AND EXISTS (
		SELECT * FROM view_rm_risk_active r WHERE r.fkAsset = a.fkContext AND r.nValue = 0
	)
);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), 'partial_assets', quantity);


quantity := (SELECT count(*) FROM view_rm_asset_active a WHERE a.nValue = 0);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), 'not_managed_assets', quantity);


quantity := (
	SELECT count(*)
	FROM view_rm_asset_active a
	WHERE a.fkContext NOT IN (SELECT fkAsset FROM view_rm_risk_active)
);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), 'assets_without_risks', quantity);


query := 'SELECT a.fkContext, CASE WHEN sum(r.nCost) IS NULL THEN 0 ELSE sum(r.nCost) END
	FROM view_rm_asset_active a 
	LEFT JOIN view_rm_risk_active r ON (a.fkContext = r.fkAsset)
	GROUP BY a.fkContext';

FOR id_value IN EXECUTE query LOOP	
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (id_value.id, now(), 'total_asset_impact', id_value.value);	
END LOOP;	


query := '
	/*Calcula o custo total dos controles de cada ativo e a quantidade de controles de cada ativo*/
	SELECT res.asset_id, count(control_id) as control_count, CASE WHEN sum(cc.nCost1+cc.nCost2+cc.nCost3+cc.nCost4+cc.nCost5) IS NULL THEN 0 ELSE sum(cc.nCost1+cc.nCost2+cc.nCost3+cc.nCost4+cc.nCost5) END as control_cost
	FROM
	(
		/*Garante que um controle só irá ser contabilizado uma vez para um determinado ativo*/
		SELECT a.fkContext as asset_id, rc.fkControl as control_id
		FROM view_rm_asset_active a
		LEFT JOIN view_rm_risk_active r ON (a.fkContext = r.fkAsset)
		LEFT JOIN view_rm_risk_control_active rc ON (r.fkContext = rc.fkRisk)
		GROUP BY a.fkContext, rc.fkControl
	) res 
	LEFT JOIN rm_control_cost cc ON (res.control_id = cc.fkControl)
	GROUP BY asset_id';

FOR id_values IN EXECUTE query LOOP	
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (id_values.id, now(), 'control_amount_by_asset', id_values.value1);
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (id_values.id, now(), 'control_cost_by_asset', id_values.value2);
END LOOP;

/*===============================================INFORMAÇÕES SOBRE RISCO========================================================*/


query := 'SELECT fkContext, nValue, nValueResidual FROM view_rm_risk_active';

FOR id_values IN EXECUTE query LOOP	
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (id_values.id, now(), 'risk_value', id_values.value1);
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (id_values.id, now(), 'risk_residual_value', id_values.value2);
END LOOP;


query := 'SELECT r.fkContext, count(rc.fkControl) as control_count
	FROM view_rm_risk_active r
	LEFT JOIN view_rm_risk_control_active rc ON (r.fkContext = rc.fkRisk)
	GROUP BY r.fkContext';

FOR id_value IN EXECUTE query LOOP	
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (id_value.id, now(), 'control_amount_by_risk', id_value.value);	
END LOOP;	


quantity := (SELECT count(*) FROM view_rm_risk_active);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), 'risk_total', quantity);


quantity := (SELECT count(*) FROM view_rm_risk_active WHERE nValue >= risk_high);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), 'risks_with_high_real_risk', quantity);


quantity := (SELECT count(*) FROM view_rm_risk_active WHERE nValueResidual >= risk_high);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), 'risks_with_high_residual_risk', quantity);


quantity := (SELECT count(*) FROM view_rm_risk_active WHERE nValue > risk_low AND nValue < risk_high);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), 'risks_with_medium_real_risk', quantity);


quantity := (SELECT count(*) FROM view_rm_risk_active WHERE nValueResidual > risk_low AND nValueResidual < risk_high);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), 'risks_with_medium_residual_risk', quantity);


quantity := (SELECT count(*) FROM view_rm_risk_active WHERE nValue <= risk_low AND nValue <> 0);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), 'risks_with_low_real_risk', quantity);


quantity := (SELECT count(*) FROM view_rm_risk_active WHERE nValueResidual <= risk_low AND nValueResidual <> 0);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), 'risks_with_low_residual_risk', quantity);


quantity := (SELECT count(*) FROM view_rm_risk_active WHERE nValue = 0);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), 'risks_with_no_value_risk', quantity);


quantity := (SELECT count(*) FROM view_rm_risk_active r WHERE r.nAcceptMode<>0 OR r.nValue != r.nValueResidual);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), 'risks_treated', quantity);


quantity := (SELECT count(*) FROM view_rm_risk_active r WHERE r.nAcceptMode=0 AND r.nValue = r.nValueResidual AND r.nValue > 0);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), 'risks_not_treated', quantity);


quantity := (SELECT count(*) FROM view_rm_risk_active r WHERE r.nAcceptMode = 72601);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), 'risks_accepted', quantity);


quantity := (SELECT count(*) FROM view_rm_risk_active r WHERE r.nAcceptMode = 72602);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), 'risks_transfered', quantity);


quantity := (SELECT count(*) FROM view_rm_risk_active r WHERE r.nAcceptMode = 72603);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), 'risks_avoided', quantity);

/*===============================================INFORMAÇÕES SOBRE CONTROLE========================================================*/


query := 'SELECT c.fkContext, count(rc.fkRisk) as risk_count
	FROM view_rm_control_active c
	LEFT JOIN view_rm_risk_control_active rc ON (c.fkContext = rc.fkControl)
	GROUP BY c.fkContext';

FOR id_value IN EXECUTE query LOOP	
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (id_value.id, now(), 'risk_amount_by_control', id_value.value);	
END LOOP;


quantity := (SELECT count(*) FROM view_rm_control_active);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), 'control_total', quantity);


quantity := (SELECT count(*) FROM view_rm_control_active c WHERE c.dDateDeadline >= now() AND c.dDateImplemented IS NULL);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), 'planned_implantation_controls', quantity);


quantity := (SELECT count(*) FROM view_rm_control_active c WHERE c.dDateDeadline < now() AND c.dDateImplemented IS NULL);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), 'late_implantation_controls', quantity);


quantity := (
	SELECT count(*)
	FROM view_rm_control_active c
	LEFT JOIN rm_control_test_history ct ON (c.fkContext = ct.fkControl)
	LEFT JOIN wkf_control_efficiency ce ON (c.fkContext = ce.fkControlEfficiency)
	WHERE c.dDateImplemented IS NOT NULL AND ((ct.bTestedValue = 1 AND ce.nRealEfficiency >= ce.nExpectedEfficiency) OR (ct.bTestedValue = 1 AND ce.nRealEfficiency IS NULL) OR (ct.bTestedValue IS NULL AND ce.nRealEfficiency >= ce.nExpectedEfficiency))
);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), 'adequate_implemented_controls', quantity);


quantity := (
	SELECT count(*)
	FROM view_rm_control_active c
	LEFT JOIN rm_control_test_history ct ON (c.fkContext = ct.fkControl)
	LEFT JOIN wkf_control_efficiency ce ON (c.fkContext = ce.fkControlEfficiency)
	WHERE 
	(
		(ct.bTestedValue=0 AND ce.nRealEfficiency>=ce.nExpectedEfficiency) OR
		(ct.bTestedValue=0 AND ce.nRealEfficiency IS NULL) OR
		(ct.bTestedValue IS NULL AND ce.nRealEfficiency<ce.nExpectedEfficiency) OR
		(ct.bTestedValue=1 AND ce.nRealEfficiency<ce.nExpectedEfficiency) OR
		(ct.bTestedValue=0 AND ce.nRealEfficiency<ce.nExpectedEfficiency)
	)
);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), 'inadequate_implemented_controls', quantity);


quantity := (
	SELECT count(*)
	FROM view_rm_control_active c
	LEFT JOIN rm_control_test_history ct ON (c.fkContext = ct.fkControl)
	LEFT JOIN wkf_control_efficiency ce ON (c.fkContext = ce.fkControlEfficiency)
	WHERE
	c.fkContext NOT IN (SELECT fkControlEfficiency FROM wkf_control_efficiency) AND
	c.fkContext NOT IN (SELECT fkControlTest FROM wkf_control_test)
);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), 'not_measured_implemented_controls', quantity);


quantity := (
	SELECT count(*)
	FROM view_rm_control_active c
	JOIN rm_control_test_history ct ON (c.fkContext = ct.fkControl AND ct.bTestedValue = 1 AND c.dDateImplemented IS NOT NULL)
);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), 'tested_ok_controls', quantity);


quantity := (
	SELECT count(*)
	FROM view_rm_control_active c
	JOIN rm_control_test_history ct ON (c.fkContext = ct.fkControl AND ct.bTestedValue = 0 AND c.dDateImplemented IS NOT NULL)
);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), 'tested_not_ok_controls', quantity);


quantity := (
	SELECT count(*)
	FROM view_rm_control_active c
	JOIN wkf_control_efficiency ce ON (c.fkContext = ce.fkControlEfficiency AND ce.nRealEfficiency >= ce.nExpectedEfficiency AND c.dDateImplemented IS NOT NULL)
);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), 'revision_ok_controls', quantity);


quantity := (
	SELECT count(*)
	FROM view_rm_control_active c

	JOIN wkf_control_efficiency ce ON (c.fkContext = ce.fkControlEfficiency AND ce.nRealEfficiency < ce.nExpectedEfficiency AND c.dDateImplemented IS NOT NULL)
);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), 'revision_not_ok_controls', quantity);


quantity := (
	SELECT count(*)
	FROM view_rm_control_active c
	WHERE c.fkContext NOT IN (SELECT fkControl FROM view_rm_risk_control_active)
);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), 'controls_without_risks', quantity);

/*===============================================INFORMAÇÕES SOBRE NORMA========================================================*/


query := 'SELECT s.fkContext, count(bps.fkBestPractice)
	FROM view_rm_standard_active s
	LEFT JOIN view_rm_bp_standard_active bps ON (s.fkContext = bps.fkStandard)
	GROUP BY s.fkContext';

FOR id_value IN EXECUTE query LOOP	
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (id_value.id, now(), 'best_practice_amount_by_standard', id_value.value);	
END LOOP;


query := 'SELECT s.fkContext, count(bps.fkBestPractice)
	FROM view_rm_standard_active s
	LEFT JOIN rm_best_practice_standard bps ON (s.fkContext = bps.fkStandard)
	AND bps.fkBestPractice IN (SELECT cbp.fkBestPractice FROM view_rm_control_bp_active cbp)
	GROUP BY s.fkContext';

FOR id_value IN EXECUTE query LOOP	
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (id_value.id, now(), 'applied_best_practice_amount_by_standard', id_value.value);	
END LOOP;

/*===============================================INFORMAÇÕES FINANCEIRAS========================================================*/


cost := (SELECT CASE WHEN sum(nCost) IS NULL THEN 0 ELSE sum(nCost) END FROM view_rm_asset_active);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), 'asset_total_cost', cost);


query := 'SELECT 
	CASE WHEN sum(cc.nCost1) IS NULL THEN 0 ELSE sum(cc.nCost1) END,
	CASE WHEN sum(cc.nCost2) IS NULL THEN 0 ELSE sum(cc.nCost2) END,
	CASE WHEN sum(cc.nCost3) IS NULL THEN 0 ELSE sum(cc.nCost3) END,
	CASE WHEN sum(cc.nCost4) IS NULL THEN 0 ELSE sum(cc.nCost4) END,
	CASE WHEN sum(cc.nCost5) IS NULL THEN 0 ELSE sum(cc.nCost5) END
	FROM view_rm_control_active c
	JOIN rm_control_cost cc ON (c.fkContext = cc.fkControl)';

FOR control_cost IN EXECUTE query LOOP	
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (NULL, now(), 'control_total_cost1', control_cost.cost1);
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (NULL, now(), 'control_total_cost2', control_cost.cost2);
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (NULL, now(), 'control_total_cost3', control_cost.cost3);
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (NULL, now(), 'control_total_cost4', control_cost.cost4);
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (NULL, now(), 'control_total_cost5', control_cost.cost5);
END LOOP;


cost := (SELECT CASE WHEN sum(nCost) IS NULL THEN 0 ELSE sum(nCost) END FROM view_rm_risk_active r WHERE r.nValue <= risk_low);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), 'potentially_low_risk_cost', cost);


cost := (SELECT CASE WHEN sum(nCost) IS NULL THEN 0 ELSE sum(nCost) END FROM view_rm_risk_active r WHERE r.fkContext IN (SELECT fkRisk FROM view_rm_risk_control_active));
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), 'mitigated_risk_cost', cost);


cost := (SELECT CASE WHEN sum(nCost) IS NULL THEN 0 ELSE sum(nCost) END FROM view_rm_risk_active r WHERE r.nAcceptMode = 72603);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), 'avoided_risk_cost', cost);


cost := (SELECT CASE WHEN sum(nCost) IS NULL THEN 0 ELSE sum(nCost) END FROM view_rm_risk_active r WHERE r.nAcceptMode = 72602);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), 'transfered_risk_cost', cost);


cost := (SELECT CASE WHEN sum(nCost) IS NULL THEN 0 ELSE sum(nCost) END FROM view_rm_risk_active r WHERE r.nAcceptMode = 72601);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), 'accepted_risk_cost', cost);


cost := (
	SELECT CASE WHEN sum(nCost) IS NULL THEN 0 ELSE sum(nCost) END FROM view_rm_risk_active r
	WHERE r.nAcceptMode = 0 AND r.nValue > risk_low AND r.fkContext NOT IN (SELECT fkRisk FROM view_rm_risk_control_active)
);
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, now(), 'not_treated_and_medium_high_risk', cost);

/*================================================= POLICY MANAGEMENT ==========================================================*/

INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT CAST(NULL AS INTEGER), NOW(), 'document_total', count(*) FROM view_pm_published_docs;

INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT CAST(NULL AS INTEGER), NOW(), 'documents_by_state_developing', count(d.fkContext)
FROM view_pm_document_active d JOIN isms_context c ON (c.pkContext = d.fkContext)
WHERE c.nState = 2751
UNION
SELECT CAST(NULL AS INTEGER), NOW(), 'documents_by_state_approved', count(d.fkContext)
FROM view_pm_document_active d JOIN isms_context c ON (c.pkContext = d.fkContext)
WHERE c.nState = 2752
UNION
SELECT CAST(NULL AS INTEGER), NOW(), 'documents_by_state_pendant', count(d.fkContext)
FROM view_pm_document_active d JOIN isms_context c ON (c.pkContext = d.fkContext)
WHERE c.nState = 2753;

INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT CAST(NULL AS INTEGER), NOW(), 'documents_by_type_none', count(d.fkContext)
FROM view_pm_document_active d
WHERE d.nType = 2800
UNION
SELECT CAST(NULL AS INTEGER), NOW(), 'documents_by_type_area', count(d.fkContext)
FROM view_pm_document_active d
WHERE d.nType = 2801
UNION
SELECT CAST(NULL AS INTEGER), NOW(), 'documents_by_type_process', count(d.fkContext)
FROM view_pm_document_active d
WHERE d.nType = 2802
UNION
SELECT CAST(NULL AS INTEGER), NOW(), 'documents_by_type_asset', count(d.fkContext)
FROM view_pm_document_active d
WHERE d.nType = 2803
UNION
SELECT CAST(NULL AS INTEGER), NOW(), 'documents_by_type_control', count(d.fkContext)
FROM view_pm_document_active d
WHERE d.nType = 2805
UNION
SELECT CAST(NULL AS INTEGER), NOW(), 'documents_by_type_scope', count(d.fkContext)
FROM view_pm_document_active d
WHERE d.nType = 2820
UNION
SELECT CAST(NULL AS INTEGER), NOW(), 'documents_by_type_policy', count(d.fkContext)
FROM view_pm_document_active d
WHERE d.nType = 2821
UNION
SELECT CAST(NULL AS INTEGER), NOW(), 'documents_by_type_management', count(d.fkContext)
FROM view_pm_document_active d
WHERE d.nType = 3801
UNION
SELECT CAST(NULL AS INTEGER), NOW(), 'documents_by_type_others', count(d.fkContext)
FROM view_pm_document_active d
WHERE d.nType = 3802;

INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT CAST(NULL AS INTEGER), NOW(), 'register_total', count(*) FROM view_pm_register_active;

INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT CAST(NULL AS INTEGER), NOW(), 'documents_read', count(*) FROM view_pm_read_docs;

INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT CAST(NULL AS INTEGER), NOW(), 'documents_not_read', count(fkContext)
FROM view_pm_published_docs
WHERE fkContext NOT IN (SELECT fkContext FROM view_pm_read_docs);

INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT CAST(NULL AS INTEGER), NOW(), 'documents_scheduled', count(*)
FROM view_pm_published_docs WHERE fkSchedule IS NOT NULL;

INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT CAST(NULL AS INTEGER), NOW(), 'occupation_documents', CASE WHEN SUM(di.nFileSize) > 0 THEN ROUND(CAST(SUM(di.nFileSize) AS FLOAT)/1048576.0) ELSE 0 END
FROM pm_doc_instance di
WHERE NOT EXISTS (
  SELECT * FROM pm_document d JOIN pm_register r ON (r.fkDocument = d.fkContext)
  WHERE d.fkContext = di.fkDocument
);

INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT CAST(NULL AS INTEGER), NOW(), 'occupation_template', CASE WHEN SUM(t.nFileSize) > 0 THEN ROUND(CAST(SUM(t.nFileSize) AS FLOAT)/1048576.0) ELSE 0 END
FROM pm_template t;

INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT CAST(NULL AS INTEGER), NOW(), 'occupation_registers', CASE WHEN SUM(di.nFileSize) > 0 THEN ROUND(CAST(SUM(di.nFileSize) AS FLOAT)/1048576.0) ELSE 0 END
FROM
  pm_doc_instance di
  JOIN pm_document d ON (d.fkContext = di.fkDocument)
  JOIN pm_register r ON (r.fkDocument = d.fkContext);

INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT CAST(NULL AS INTEGER), NOW(), 'occupation_total', CASE WHEN SUM(f.nFileSize) > 0 THEN ROUND(CAST(SUM(f.nFileSize) AS FLOAT)/1048576.0) ELSE 0 END
FROM (
  SELECT nFileSize FROM pm_doc_instance
  UNION ALL
  SELECT nFileSize FROM pm_template
) f;

/*=============================================== CONTINUAL IMPROVEMENT ========================================================*/

INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT CAST(NULL AS INTEGER), NOW(), 'incident_total', count(*) FROM view_ci_incident_active;

INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT CAST(NULL AS INTEGER), NOW(), 'non_conformity_total', count(*) FROM view_ci_nc_active;

INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT CAST(NULL AS INTEGER), NOW(), 'incidents_by_state_open', count(i.fkContext)
FROM view_ci_incident_active i JOIN isms_context c ON (c.pkContext = i.fkContext)
WHERE c.nState = 2779
UNION
SELECT CAST(NULL AS INTEGER), NOW(), 'incidents_by_state_directed', count(i.fkContext)
FROM view_ci_incident_active i JOIN isms_context c ON (c.pkContext = i.fkContext)
WHERE c.nState = 2780
UNION
SELECT CAST(NULL AS INTEGER), NOW(), 'incidents_by_state_pendant_disposal', count(i.fkContext)
FROM view_ci_incident_active i JOIN isms_context c ON (c.pkContext = i.fkContext)
WHERE c.nState = 2781
UNION
SELECT CAST(NULL AS INTEGER), NOW(), 'incidents_by_state_waiting_solution', count(i.fkContext)
FROM view_ci_incident_active i JOIN isms_context c ON (c.pkContext = i.fkContext)
WHERE c.nState = 2782
UNION
SELECT CAST(NULL AS INTEGER), NOW(), 'incidents_by_state_pendant_solution', count(i.fkContext)
FROM view_ci_incident_active i JOIN isms_context c ON (c.pkContext = i.fkContext)
WHERE c.nState = 2783
UNION
SELECT CAST(NULL AS INTEGER), NOW(), 'incidents_by_state_solved', count(i.fkContext)
FROM view_ci_incident_active i JOIN isms_context c ON (c.pkContext = i.fkContext)
WHERE c.nState = 2784;

INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT CAST(NULL AS INTEGER), NOW(), 'nc_by_state_ci_sent', count(nc.fkContext)
FROM view_ci_nc_active nc JOIN isms_context c ON (c.pkContext = nc.fkContext)
WHERE c.nState = 2770
UNION
SELECT CAST(NULL AS INTEGER), NOW(), 'nc_by_state_ci_open', count(nc.fkContext)
FROM view_ci_nc_active nc JOIN isms_context c ON (c.pkContext = nc.fkContext)
WHERE c.nState = 2771
UNION
SELECT CAST(NULL AS INTEGER), NOW(), 'nc_by_state_ci_directed', count(nc.fkContext)
FROM view_ci_nc_active nc JOIN isms_context c ON (c.pkContext = nc.fkContext)
WHERE c.nState = 2772
UNION
SELECT CAST(NULL AS INTEGER), NOW(), 'nc_by_state_ci_nc_pendant', count(nc.fkContext)
FROM view_ci_nc_active nc JOIN isms_context c ON (c.pkContext = nc.fkContext)
WHERE c.nState = 2773
UNION
SELECT CAST(NULL AS INTEGER), NOW(), 'nc_by_state_ci_ap_pendant', count(nc.fkContext)
FROM view_ci_nc_active nc JOIN isms_context c ON (c.pkContext = nc.fkContext)
WHERE c.nState = 2774
UNION
SELECT CAST(NULL AS INTEGER), NOW(), 'nc_by_state_ci_waiting_conclusion', count(nc.fkContext)
FROM view_ci_nc_active nc JOIN isms_context c ON (c.pkContext = nc.fkContext)
WHERE c.nState = 2775
UNION
SELECT CAST(NULL AS INTEGER), NOW(), 'nc_by_state_ci_finished', count(nc.fkContext)
FROM view_ci_nc_active nc JOIN isms_context c ON (c.pkContext = nc.fkContext)
WHERE c.nState = 2776
UNION
SELECT CAST(NULL AS INTEGER), NOW(), 'nc_by_state_ci_closed', count(nc.fkContext)
FROM view_ci_nc_active nc JOIN isms_context c ON (c.pkContext = nc.fkContext)
WHERE c.nState = 2777
UNION
SELECT CAST(NULL AS INTEGER), NOW(), 'nc_by_state_denied', count(nc.fkContext)
FROM view_ci_nc_active nc JOIN isms_context c ON (c.pkContext = nc.fkContext)
WHERE c.nState = 2703;

/*=============================================== ACTION PLAN STATISTICS ========================================================*/

INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT CAST(NULL AS INTEGER), NOW(), 'ap_total', count(*) FROM view_ci_action_plan_active;

INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT CAST(NULL AS INTEGER), NOW(), 'ap_finished', count(*) 
   FROM view_ci_action_plan_active
   WHERE dDateConclusion IS NOT NULL
         AND dDateConclusion <= dDateDeadLine;

INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT CAST(NULL AS INTEGER), NOW(), 'ap_finished_late', count(*) 
   FROM view_ci_action_plan_active
   WHERE dDateConclusion IS NOT NULL
         AND dDateConclusion > dDateDeadLine;

INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT CAST(NULL AS INTEGER), NOW(), 'ap_efficient', count(*) 
   FROM view_ci_action_plan_active
   WHERE dDateConclusion IS NOT NULL
         AND bIsEfficient = 1;

INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT CAST(NULL AS INTEGER), NOW(), 'ap_non_efficient', count(*) 
   FROM view_ci_action_plan_active
   WHERE dDateConclusion IS NOT NULL
         AND bIsEfficient = 0;

INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT CAST(NULL AS INTEGER), NOW(), 'nc_per_ap_average', 
	CASE WHEN (SELECT COUNT(*) FROM (SELECT COUNT(*) FROM ci_nc_action_plan r JOIN isms_context ctx2 ON (r.fknc = ctx2.pkContext AND ctx2.pkContext <> 2705) GROUP BY fkActionPlan)aps) > 0
	            THEN (SELECT COUNT(*) FROM (SELECT COUNT(*) FROM ci_nc_action_plan r JOIN isms_context ctx1 ON (r.fknc = ctx1.pkContext AND ctx1.pkContext <> 2705) GROUP BY r.fknc)ncs)::float
		          /
		         (SELECT COUNT(*) FROM (SELECT COUNT(*) FROM ci_nc_action_plan r JOIN isms_context ctx2 ON (r.fknc = ctx2.pkContext AND ctx2.pkContext <> 2705) GROUP BY fkActionPlan)aps)::float
	            ELSE 0 END;

/*==============================================================================================================================*/


UPDATE isms_config set sValue = date_part('year', now()) || '-' || date_part('month', now()) || '-' || date_part('day', now()) WHERE pkConfig = 420;

END IF;

RETURN 1;
END$$;
CREATE FUNCTION delete_area(integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$DECLARE 
    ctx context_id%rowtype;
BEGIN
   FOR ctx IN EXECUTE ' SELECT fkContext FROM rm_area WHERE fkParent = ' || $1 LOOP
	if(ctx.id IS NOT NULL) THEN	
		PERFORM delete_area(ctx.id);
	END IF;
   END LOOP;
   DELETE FROM rm_area	WHERE fkContext = $1;
   DELETE FROM isms_context WHERE pkContext = $1;
   RETURN 1;	
END$$;
CREATE FUNCTION delete_category(integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$DECLARE 
    ctx context_id%rowtype;
BEGIN
   FOR ctx IN EXECUTE ' SELECT fkContext FROM rm_category WHERE fkParent = ' || $1 LOOP
	if(ctx.id IS NOT NULL) THEN	
		PERFORM delete_category(ctx.id);
	END IF;
   END LOOP;

   DELETE FROM rm_category  WHERE fkContext = $1;
   DELETE FROM isms_context WHERE pkContext = $1;
   RETURN 1;	
END$$;
CREATE FUNCTION delete_document(iddoc integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
  DECLARE
    doc pm_document%ROWTYPE;
  BEGIN
    FOR doc IN SELECT fkContext FROM pm_document WHERE fkParent = idDoc LOOP
      PERFORM delete_document(doc.fkContext);
    END LOOP;
    DELETE FROM pm_document WHERE fkContext = idDoc;
    DELETE FROM isms_context WHERE pkContext = idDoc;
    RETURN 1;
  END
$$;
CREATE FUNCTION delete_section(integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$DECLARE 
    ctx context_id%rowtype;
BEGIN
   FOR ctx IN EXECUTE ' SELECT fkContext FROM rm_section_best_practice WHERE fkParent = ' || $1 LOOP
	if(ctx.id IS NOT NULL) THEN	
		PERFORM delete_section(ctx.id);
	END IF;
   END LOOP;

   DELETE FROM rm_section_best_practice  WHERE fkContext = $1;
   DELETE FROM isms_context WHERE pkContext = $1;
   RETURN 1;	
END$$;
CREATE FUNCTION delete_user() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN

IF (TG_OP = 'DELETE') THEN
    UPDATE isms_context_date
    SET nUserId = 0
    WHERE nUserId = OLD.fkContext;
END IF;
RETURN NULL;
END$$;
CREATE FUNCTION get_all_asset_and_dependencies() RETURNS SETOF asset_dependency
    LANGUAGE plpgsql
    AS $$DECLARE 
asset_id context_id%rowtype;
asset_dep_id context_id%rowtype;
asset_assoc asset_dependency%rowtype;
BEGIN
   FOR asset_id IN EXECUTE 'SELECT fkContext FROM view_rm_asset_active' LOOP
	    FOR asset_dep_id IN EXECUTE 'SELECT pkAsset FROM get_asset_dependencies('|| asset_id.id ||')' LOOP
		asset_assoc.asset = asset_id.id;
		asset_assoc.dependency = asset_dep_id.id;
		RETURN NEXT asset_assoc;
	    END LOOP;	
   END LOOP;
   RETURN;	
END$$;
CREATE FUNCTION get_all_asset_and_dependents() RETURNS SETOF asset_dependent
    LANGUAGE plpgsql
    AS $$DECLARE
asset_id context_id%rowtype;
asset_dep_id context_id%rowtype;
asset_assoc asset_dependent%rowtype;
BEGIN
   FOR asset_id IN EXECUTE 'SELECT fkContext FROM view_rm_asset_active' LOOP
	    FOR asset_dep_id IN EXECUTE 'SELECT pkAsset FROM get_asset_dependents('|| asset_id.id ||')' LOOP
		asset_assoc.asset = asset_id.id;
		asset_assoc.dependent = asset_dep_id.id;
		RETURN NEXT asset_assoc;
	    END LOOP;
   END LOOP;
   RETURN;
END$$;
CREATE FUNCTION get_area_parents(integer) RETURNS SETOF area_id
    LANGUAGE plpgsql
    AS $$DECLARE 
area_id ALIAS FOR $1;
query_area_parents varchar(255);
area_ids context_id%rowtype;
area_rec context_id%rowtype;
BEGIN
   query_area_parents := ' SELECT a.fkParent FROM rm_area a
			 JOIN isms_context c ON(c.pkContext = a.fkContext and c.nState <> 2705)
			     WHERE fkContext = ' ||area_id;
   
   FOR area_ids IN EXECUTE query_area_parents LOOP
	IF(area_ids.id IS NOT NULL) THEN
	    RETURN NEXT area_ids;
	    FOR area_rec IN EXECUTE 'SELECT pkArea FROM get_area_parents('|| area_ids.id ||')' LOOP
		RETURN NEXT area_rec;
	    END LOOP;
	END IF;
   END LOOP;
   RETURN ;	
END$$;
CREATE FUNCTION get_area_parents_trash(integer) RETURNS SETOF area_id
    LANGUAGE plpgsql
    AS $$DECLARE 
area_id ALIAS FOR $1;
query_area_parents varchar(255);
area_ids context_id%rowtype;
area_rec context_id%rowtype;
BEGIN
   query_area_parents := ' SELECT a.fkParent FROM rm_area a WHERE fkContext = ' ||area_id;
   
   FOR area_ids IN EXECUTE query_area_parents LOOP
	IF(area_ids.id IS NOT NULL) THEN
	    RETURN NEXT area_ids;
	    FOR area_rec IN EXECUTE 'SELECT pkArea FROM get_area_parents_trash('|| area_ids.id ||')' LOOP
		RETURN NEXT area_rec;
	    END LOOP;
	END IF;
   END LOOP;
   RETURN ;	
END$$;
CREATE FUNCTION get_area_tree(integer, integer) RETURNS SETOF tree_area
    LANGUAGE plpgsql
    AS $$DECLARE
   piParent ALIAS FOR $1;
   piLevel ALIAS FOR $2;
   maArea tree_area%rowtype;
   maAreaAux tree_area_aux%rowtype;
   miId integer;
BEGIN
     miId:=1;
     FOR maAreaAux IN EXECUTE  ' SELECT * FROM get_area_tree_aux('|| piParent ||','|| piLevel ||') ' LOOP
        maArea.pkId := miId;
        maArea.fkContext := maAreaAux.fkContext;
        maArea.sName := maAreaAux.sName;
        maArea.nLevel := maAreaAux.nLevel;
	miId:=miId+1;	
	RETURN NEXT maArea;
     END LOOP;
     RETURN ;
END$$;
CREATE FUNCTION get_area_tree_aux(integer, integer) RETURNS SETOF tree_area_aux
    LANGUAGE plpgsql
    AS $$DECLARE
   piParent ALIAS FOR $1;
   piLevel ALIAS FOR $2;
   nAuxLevel integer;
   queryArea varchar(1024);
   queryAreaAux varchar(1024);
   queryAreaRecursion varchar(255);
   areaRoot tree_area_aux%rowtype;
   subArea tree_area_aux%rowtype;

BEGIN
     nAuxLevel := piLevel + 1;
     queryAreaAux := ' SELECT fkContext, sName, ' || nAuxLevel::text || ' as nLevel FROM rm_area a
		JOIN isms_context c ON(c.pkContext = a.fkContext and c.nState <> 2705)	
		WHERE  fkParent '  ;
     IF piParent > 0 THEN
       queryArea := queryAreaAux || ' = ' || piParent;
     ELSE 
       queryArea := queryAreaAux || ' is NULL';
     END IF;
     FOR areaRoot IN EXECUTE queryArea LOOP
        RETURN NEXT areaRoot;
        queryAreaRecursion := 'SELECT fkContext, sName, nLevel FROM get_area_tree_aux(' || areaRoot.fkContext::text || ' , ' || nAuxLevel::text || ' ) ';
        FOR subArea IN EXECUTE queryAreaRecursion LOOP
	    RETURN NEXT subArea;
        END LOOP;
     END LOOP;
     RETURN ;
END$$;
CREATE FUNCTION get_area_value(integer) RETURNS double precision
    LANGUAGE sql
    AS $$
SELECT MAX(nValue) FROM (
        SELECT nValue 
          FROM rm_process p
	  JOIN isms_context ctx1 ON(ctx1.pkContext = p.fkContext and ctx1.nState <> 2705 and p.fkArea = $1)
      UNION
        SELECT nValue
          FROM rm_area a
          JOIN isms_context ctx2 ON(ctx2.pkContext = a.fkContext and ctx2.nState <> 2705 and a.fkParent = $1)
      UNION
	SELECT 0 as value
     ) as buffer
$$;
CREATE FUNCTION get_areas_and_subareas_by_user(integer) RETURNS SETOF area_ids
    LANGUAGE plpgsql
    AS $$DECLARE 
    userId ALIAS FOR $1;
    query_area varchar(255);
    area context_id%rowtype;
    subArea context_id%rowtype;
    areaParent context_id%rowtype;
BEGIN
   query_area := ' SELECT fkContext FROM rm_area a 
		    JOIN isms_context c ON(c.pkContext = a.fkContext and c.nState <> 2705) 
		        WHERE a.fkResponsible = ' || userId;

   FOR area IN EXECUTE query_area LOOP
	RETURN NEXT area;
        FOR subArea IN EXECUTE ' SELECT pkArea FROM get_sub_areas(' || area.id || ') ' LOOP
	    RETURN NEXT subArea;
        END LOOP;
   END LOOP;
   RETURN ;	
END$$;
CREATE FUNCTION get_areas_by_user(integer) RETURNS SETOF area_ids
    LANGUAGE plpgsql
    AS $$DECLARE 
    userId ALIAS FOR $1;
    query_area varchar(255);
    area context_id%rowtype;
    subArea context_id%rowtype;
    areaParent context_id%rowtype;
BEGIN
   query_area := ' SELECT fkContext FROM rm_area a 
		    JOIN isms_context c ON(c.pkContext = a.fkContext and c.nState <> 2705) 
		        WHERE a.fkResponsible = ' || userId;

   FOR area IN EXECUTE query_area LOOP
	RETURN NEXT area;

        FOR subArea IN EXECUTE ' SELECT pkArea FROM get_area_parents(' || area.id || ') ' LOOP
	    RETURN NEXT subArea;
        END LOOP;

        FOR subArea IN EXECUTE ' SELECT pkArea FROM get_sub_areas(' || area.id || ') ' LOOP
	    RETURN NEXT subArea;
        END LOOP;

   END LOOP;

   RETURN ;	
END$$;
CREATE FUNCTION get_asset_category_parents(integer) RETURNS SETOF category_id
    LANGUAGE plpgsql
    AS $$DECLARE
piCategory ALIAS FOR $1;
queryCat varchar(255);
queryCatRecursion varchar(255);
category context_id%rowtype;
catParent context_id%rowtype;
BEGIN
     queryCat := ' SELECT cat.fkParent FROM cm_asset_category cat
JOIN isms_context c ON(c.pkContext = cat.fkContext and c.nState <> 2705)
WHERE  cat.fkContext = ' || piCategory;
     FOR category IN EXECUTE queryCat LOOP
if(category.id IS NOT NULL) THEN
    RETURN NEXT category;
    FOR catParent IN EXECUTE 'SELECT pkCategory FROM get_asset_category_parents('|| category.id ||')' LOOP
      RETURN NEXT catParent;
    END LOOP;
END IF;
     END LOOP;
     RETURN ;
END$$;
CREATE FUNCTION get_asset_category_parents_threats() RETURNS SETOF asset_category_threats
    LANGUAGE plpgsql
    AS $$DECLARE
queryCat varchar(1000);
queryCatRecursion varchar(255);
asset rm_asset%rowtype;
parent asset_category_threats%rowtype;
BEGIN
     FOR asset IN EXECUTE 'SELECT p.* from view_rm_asset_active p' LOOP
if(asset.fkCategory IS NOT NULL) THEN
    FOR parent IN EXECUTE 'SELECT * FROM get_asset_category_parents_threats('|| asset.fkCategory ||')' LOOP
      parent.fkasset = asset.fkcontext;
      RETURN NEXT parent;
    END LOOP;
END IF;
     END LOOP;
     RETURN ;
END$$;
CREATE FUNCTION get_asset_category_parents_threats(integer) RETURNS SETOF asset_category_threat
    LANGUAGE plpgsql
    AS $$DECLARE
piCategory ALIAS FOR $1;
queryCat varchar(1000);
queryCatRecursion varchar(255);
category asset_category_threat%rowtype;
catParent asset_category_threat%rowtype;
c varchar(1);
BEGIN
     queryCat := ' SELECT cat.fkcontext, pct.fkthreat, cat.fkparent FROM cm_asset_category cat
JOIN isms_context c ON(c.pkContext = cat.fkContext and c.nState <> 2705)
LEFT JOIN (select pct2.* from cm_asset_category_threat pct2 join view_cm_threat_active ta on ta.fkcontext=pct2.fkthreat)  
      pct on pct.fkassetcategory = cat.fkcontext
WHERE cat.fkContext = ' || piCategory;
     c := '1';
     FOR category IN EXECUTE queryCat LOOP
if(category.fkthreat IS NOT NULL) THEN
   RETURN NEXT category;
END IF;
if(category.fkParent IS NOT NULL AND c = '1') THEN
    c := '2';
    FOR catParent IN EXECUTE 'SELECT * FROM get_asset_category_parents_threats('|| category.fkParent ||')' LOOP
      RETURN NEXT catParent;
    END LOOP;
END IF;
     END LOOP;
     RETURN ;
END$$;
CREATE FUNCTION get_asset_category_tree(integer, integer) RETURNS SETOF tree_category
    LANGUAGE plpgsql
    AS $$DECLARE
piParent ALIAS FOR $1;
pnLevel ALIAS FOR $2;
nAuxLevel integer;
queryCat varchar(255);
queryCatAux varchar(255);
queryCatRecursion varchar(255);
categoryRoot tree_category%rowtype;
subCat tree_category%rowtype;
BEGIN
     nAuxLevel := pnLevel + 1;
     queryCatAux := ' SELECT fkContext, sName, ' || nAuxLevel::text || ' as nLevel FROM view_cm_asset_category_active cat WHERE  fkParent '  ;
     IF piParent > 0 THEN
       queryCat := queryCatAux || ' = ' || piParent;
     ELSE 
       queryCat := queryCatAux || ' is NULL';
     END IF;
     FOR categoryRoot IN EXECUTE queryCat LOOP
        RETURN NEXT categoryRoot;
        queryCatRecursion := 'SELECT fkContext, sName, nLevel FROM get_asset_category_tree(' || categoryRoot.fkContext::text || ' , ' || nAuxLevel::text || ' ) ';
        FOR subCat IN EXECUTE queryCatRecursion LOOP
    RETURN NEXT subCat;
        END LOOP;
     END LOOP;
     RETURN ;
END$$;
CREATE FUNCTION get_asset_dependencies(integer) RETURNS SETOF asset_id
    LANGUAGE plpgsql
    AS $$DECLARE 
asset_id ALIAS FOR $1;
query_asset_dependencies varchar(255);
asset_ids context_id%rowtype;
asset_rec context_id%rowtype;
BEGIN
   query_asset_dependencies := 'SELECT fkAsset FROM view_rm_asset_asset_active  WHERE fkDependent = ' || asset_id;
   
   FOR asset_ids IN EXECUTE query_asset_dependencies LOOP
	IF(asset_ids.id IS NOT NULL) THEN
	    RETURN NEXT asset_ids;
	    FOR asset_rec IN EXECUTE 'SELECT pkAsset FROM get_asset_dependencies('|| asset_ids.id ||')' LOOP					
		RETURN NEXT asset_rec;
	    END LOOP;
	END IF;
   END LOOP;
   RETURN ;
END$$;
CREATE FUNCTION get_asset_dependents(integer) RETURNS SETOF asset_id
    LANGUAGE plpgsql
    AS $$DECLARE 
asset_id ALIAS FOR $1;
query_asset_dependents varchar(255);
asset_ids context_id%rowtype;
asset_rec context_id%rowtype;
BEGIN
   query_asset_dependents := 'SELECT fkDependent FROM view_rm_asset_asset_active  WHERE fkAsset = ' || asset_id;
   
   FOR asset_ids IN EXECUTE query_asset_dependents LOOP
	IF(asset_ids.id IS NOT NULL) THEN
	    RETURN NEXT asset_ids;
	    FOR asset_rec IN EXECUTE 'SELECT pkAsset FROM get_asset_dependents('|| asset_ids.id ||')' LOOP					
		RETURN NEXT asset_rec;
	    END LOOP;
	END IF;
   END LOOP;
   RETURN ;
END$$;
CREATE FUNCTION get_asset_value(integer) RETURNS double precision
    LANGUAGE plpgsql
    AS $$
 DECLARE
  miResult double precision;
  miAssetValue row_double%rowtype;
  miBlueRisk integer;
  assetCount asset_value_cont%rowtype;
BEGIN
  miResult := 0;
  miBlueRisk := 0; 

  FOR assetCount IN 
    SELECT count_sys, count_asset 
      FROM (SELECT count(pkparametername) as count_sys  FROM rm_parameter_name pn) buffer_sys,
           (SELECT count(fkparametername) as count_asset 
              FROM rm_asset_value av
                JOIN isms_context ctx ON(ctx.pkContext = av.fkAsset AND ctx.nState <> 2705 AND av.fkAsset = $1)
            ) buffer_asset
  LOOP
    IF (assetCount.cont_system <> assetCount.cont_asset) THEN
      miBlueRisk := 1;
    END IF;
  END LOOP;

 IF (miBlueRisk = 1) THEN
    miResult = 0;
 ELSE
    FOR miAssetValue IN
	SELECT MAX(value) FROM (
	  SELECT MAX(nvalueresidual) as value
	    FROM view_rm_risk_active as r
	    WHERE r.fkasset = $1 AND (nAcceptMode = 0)
	  UNION
	  SELECT MAX(0.1) as value
	    FROM view_rm_risk_active as r
	    WHERE r.fkasset = $1 AND (nAcceptMode <> 0)
	  UNION
	  SELECT MAX(nvalue) as value
	    FROM view_rm_asset_asset_active aa
	      JOIN view_rm_asset_active a ON(aa.fkAsset = a.fkContext AND aa.fkdependent = $1)
	  UNION 
	  SELECT 0 as value
	) as buffer
    LOOP
	miResult = miResult + miAssetValue.value;
    END LOOP;
  END IF;

  RETURN miResult;
END$$;
CREATE FUNCTION get_assets_from_category(integer) RETURNS SETOF asset_id
    LANGUAGE plpgsql
    AS $$DECLARE
piCategory ALIAS FOR $1;
queryCat varchar(255);
queryAsset varchar(255);
assetCat context_id%rowtype;
category context_id%rowtype;
assetSubCat context_id%rowtype;
BEGIN
     queryCat := ' SELECT fkContext FROM rm_category cat
		JOIN isms_context c ON(c.pkContext = cat.fkContext and c.nState <> 2705)	
		WHERE  fkParent = ' || piCategory;

     queryAsset := ' SELECT fkContext FROM rm_asset a
		JOIN isms_context c ON(c.pkContext = a.fkContext and c.nState <> 2705)	
		WHERE fkCategory = ' || piCategory;

     FOR assetCat IN EXECUTE queryAsset LOOP
        if(assetCat.id IS NOT NULL) THEN
		RETURN NEXT assetCat;
	END IF;
     END LOOP;

     FOR category IN EXECUTE queryCat LOOP
        if(category.id IS NOT NULL) THEN
	        FOR assetSubCat IN EXECUTE ' SELECT pkAsset FROM get_assets_from_category(' || category.id || ') ' LOOP
		        RETURN NEXT assetSubCat;
	        END LOOP;
	    END IF;
     END LOOP;

     RETURN ;
END$$;
CREATE FUNCTION get_bp_from_section(integer) RETURNS SETOF best_practices
    LANGUAGE plpgsql
    AS $$DECLARE
   piSection ALIAS FOR $1;
   querySubSec varchar(255);
   queryBPfromSec varchar(255);
   ctxBP context_id%rowtype;
   ctxSubSec context_id%rowtype;
   ctxSubSecAux context_id%rowtype;
BEGIN
     querySubSec := ' SELECT fkContext
                       FROM rm_section_best_practice bp
		       JOIN isms_context c ON(c.pkContext = bp.fkContext and c.nState <> 2705)	
                       WHERE fkParent = ' || piSection;

     queryBPfromSec := ' SELECT fkContext
                          FROM rm_best_practice bp
                          JOIN isms_context c ON(c.pkContext = bp.fkContext and c.nState <> 2705)	
                              WHERE fkSectionBestPractice = ' || piSection;

     FOR ctxBP IN EXECUTE queryBPfromSec LOOP
        if(ctxBP.id IS NOT NULL) THEN
		RETURN NEXT ctxBP;
	END IF;
     END LOOP;

     FOR ctxSubSec IN EXECUTE querySubSec LOOP
        if(ctxSubSec.id IS NOT NULL) THEN
	    FOR ctxSubSecAux IN EXECUTE ' SELECT pkBestPractice FROM get_bp_from_section( ' || ctxSubSec.id || ') ' LOOP
		RETURN NEXT ctxSubSecAux;
	    END LOOP;
	END IF;
     END LOOP;

     RETURN ;
END$$;
CREATE FUNCTION get_category_parents(integer) RETURNS SETOF category_id
    LANGUAGE plpgsql
    AS $$DECLARE
piCategory ALIAS FOR $1;
queryCat varchar(255);
queryCatRecursion varchar(255);
category context_id%rowtype;
catParent context_id%rowtype;
BEGIN
     queryCat := ' SELECT cat.fkParent FROM rm_category cat
		JOIN isms_context c ON(c.pkContext = cat.fkContext and c.nState <> 2705)	
		WHERE  cat.fkContext = ' || piCategory;

     FOR category IN EXECUTE queryCat LOOP
	if(category.id IS NOT NULL) THEN
	    RETURN NEXT category;
	    FOR catParent IN EXECUTE 'SELECT pkCategory FROM get_category_parents('|| category.id ||')' LOOP
	      RETURN NEXT catParent;
	    END LOOP;
	END IF;
     END LOOP;

     RETURN ;
END$$;
CREATE FUNCTION get_category_parents_trash(integer) RETURNS SETOF category_id
    LANGUAGE plpgsql
    AS $$DECLARE
piCategory ALIAS FOR $1;
queryCat varchar(255);
queryCatRecursion varchar(255);
category context_id%rowtype;
catParent context_id%rowtype;
BEGIN
     queryCat := ' SELECT cat.fkParent FROM rm_category cat WHERE  cat.fkContext = ' || piCategory;

     FOR category IN EXECUTE queryCat LOOP
	if(category.id IS NOT NULL) THEN
	    RETURN NEXT category;
	    FOR catParent IN EXECUTE 'SELECT pkCategory FROM get_category_parents_trash('|| category.id ||')' LOOP
	      RETURN NEXT catParent;
	    END LOOP;
	END IF;
     END LOOP;

     RETURN ;
END$$;
CREATE FUNCTION get_category_tree(integer, integer) RETURNS SETOF tree_category
    LANGUAGE plpgsql
    AS $$DECLARE
piParent ALIAS FOR $1;
pnLevel ALIAS FOR $2;
nAuxLevel integer;
queryCat varchar(255);
queryCatAux varchar(255);
queryCatRecursion varchar(255);
categoryRoot tree_category%rowtype;
subCat tree_category%rowtype;

BEGIN
     nAuxLevel := pnLevel + 1;
     queryCatAux := ' SELECT fkContext, sName, ' || nAuxLevel::text || ' as nLevel FROM rm_category cat
		JOIN isms_context c ON(c.pkContext = cat.fkContext and c.nState <> 2705)	
		WHERE  fkParent '  ;
     IF piParent > 0 THEN
       queryCat := queryCatAux || ' = ' || piParent;
     ELSE 
       queryCat := queryCatAux || ' is NULL';
     END IF;
     FOR categoryRoot IN EXECUTE queryCat LOOP
        RETURN NEXT categoryRoot;
        queryCatRecursion := 'SELECT fkContext, sName, nLevel FROM get_category_tree(' || categoryRoot.fkContext::text || ' , ' || nAuxLevel::text || ' ) ';
        FOR subCat IN EXECUTE queryCatRecursion LOOP
	    RETURN NEXT subCat;
        END LOOP;
     END LOOP;
     RETURN ;
END$$;
CREATE FUNCTION get_ctx_names_by_doc_id(integer) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
   ctx_names varchar(2048);
   ctx_aux_name ctx_names%rowtype;
BEGIN
  FOR ctx_aux_name IN 
    SELECT context_name 
      FROM pm_doc_context d_c 
      JOIN context_names c_n ON (c_n.context_id = d_c.fkContext) 
      JOIN isms_context c ON (c.pkContext = d_c.fkContext AND c.nState <> 2705 ) 
      WHERE d_c.fkDocument = $1
    UNION
    SELECT '%context_scope%' as context_name
      FROM pm_doc_context d_c
      JOIN view_isms_scope_active c_n ON (c_n.fkContext = d_c.fkContext) 
      JOIN isms_context c ON (c.pkContext = d_c.fkContext AND c.nState <> 2705 ) 
      WHERE d_c.fkDocument = $1
    UNION
    SELECT '%context_policy%' as context_name
      FROM pm_doc_context d_c 
      JOIN view_isms_policy_active c_n ON (c_n.fkContext = d_c.fkContext) 
      JOIN isms_context c ON (c.pkContext = d_c.fkContext AND c.nState <> 2705 ) 
      WHERE d_c.fkDocument = $1
  LOOP
     IF ((ctx_aux_name.str IS NOT NULL) AND (ctx_aux_name.str <> '')) THEN
         IF(ctx_names <> '') THEN
     ctx_names := ctx_names || ', ' || ctx_aux_name.str;
         ELSE
     ctx_names := ctx_aux_name.str;
         END IF;
      END IF;
  END LOOP;
     RETURN ctx_names;
END $$;
CREATE FUNCTION get_document_parents(iddoc integer) RETURNS SETOF document_id
    LANGUAGE plpgsql
    AS $$
  DECLARE
    parentId document_id;
  BEGIN
    SELECT fkParent INTO parentId FROM view_pm_document_active WHERE fkContext = idDoc;
    WHILE parentId.document_id IS NOT NULL LOOP
      RETURN NEXT parentId;
      SELECT fkParent INTO parentId FROM view_pm_document_active WHERE fkContext = parentId.document_id;
    END LOOP;
    RETURN;
  END
$$;
CREATE FUNCTION get_document_parents_trash(iddoc integer) RETURNS SETOF document_id
    LANGUAGE plpgsql
    AS $$
  DECLARE
    parentId document_id;
  BEGIN
    SELECT fkParent INTO parentId FROM pm_document WHERE fkContext = idDoc;
    WHILE parentId.document_id IS NOT NULL LOOP
      RETURN NEXT parentId;
      SELECT fkParent INTO parentId FROM pm_document WHERE fkContext = parentId.document_id;
    END LOOP;
    RETURN;
  END
$$;
CREATE FUNCTION get_document_tree(integer, integer, integer) RETURNS SETOF tree_document
    LANGUAGE plpgsql
    AS $$DECLARE
piParent ALIAS FOR $1;
pnLevel ALIAS FOR $2;
pbPublished ALIAS FOR $3;
nAuxLevel integer;
queryDoc varchar(255);
queryDocAux varchar(255);
queryDocRecursion varchar(255);
documentRoot tree_document%rowtype;
subDoc tree_document%rowtype;

BEGIN
  nAuxLevel := pnLevel + 1;
  queryDocAux := ' SELECT fkContext, sName, ' || nAuxLevel::text || ' as nLevel FROM view_pm_document_active WHERE fkParent ';
  IF piParent > 0 THEN
    queryDoc := queryDocAux || ' = ' || piParent;
  ELSE 
    queryDoc := queryDocAux || ' is NULL';
  END IF;
  IF (pbPublished=1) THEN
    FOR documentRoot IN EXECUTE queryDoc LOOP
      IF EXISTS (SELECT fkContext FROM view_pm_published_docs WHERE fkContext = documentRoot.fkContext) THEN
        RETURN NEXT documentRoot;
        queryDocRecursion := 'SELECT fkContext, sName, nLevel FROM get_document_tree(' || documentRoot.fkContext::text || ' , ' || nAuxLevel::text || ' , ' || pbPublished::text || ') ';
        FOR subDoc IN EXECUTE queryDocRecursion LOOP
          RETURN NEXT subDoc;
        END LOOP;
      END IF;
    END LOOP;
  ELSE
    FOR documentRoot IN EXECUTE queryDoc LOOP
      RETURN NEXT documentRoot;
      queryDocRecursion := 'SELECT fkContext, sName, nLevel FROM get_document_tree(' || documentRoot.fkContext::text || ' , ' || nAuxLevel::text || ' , ' || pbPublished::text || ' ) ';
      FOR subDoc IN EXECUTE queryDocRecursion LOOP
        RETURN NEXT subDoc;
      END LOOP;
    END LOOP;
  END IF;
  RETURN ;
END$$;
CREATE FUNCTION get_events_from_category(integer) RETURNS SETOF event_id
    LANGUAGE plpgsql
    AS $$DECLARE
piCategory ALIAS FOR $1;
queryCat varchar(255);
queryEvent varchar(255);
eventCat context_id%rowtype;
category context_id%rowtype;
eventSubCat context_id%rowtype;
BEGIN
     queryCat := ' SELECT fkContext FROM rm_category cat
		JOIN isms_context c ON(c.pkContext = cat.fkContext and c.nState <> 2705)	
		WHERE  fkParent = ' || piCategory;

     queryEvent := ' SELECT fkContext FROM rm_event e
		JOIN isms_context c ON(c.pkContext = e.fkContext and c.nState <> 2705)	
		WHERE fkCategory = ' || piCategory;

     FOR eventCat IN EXECUTE queryEvent LOOP
        if(eventCat.id IS NOT NULL) THEN
		RETURN NEXT eventCat;
	END IF;
     END LOOP;

     FOR category IN EXECUTE queryCat LOOP
        if(category.id IS NOT NULL) THEN
	        FOR eventSubCat IN EXECUTE ' SELECT pkEvent FROM get_events_from_category(' || category.id || ') ' LOOP
		        RETURN NEXT eventSubCat;
	        END LOOP;
	    END IF;
     END LOOP;

     RETURN ;
END$$;
CREATE FUNCTION get_next_date(pdlasttime timestamp without time zone, piperiodunitid integer, piperiodscount integer) RETURNS timestamp without time zone
    LANGUAGE plpgsql
    AS $$DECLARE
  miYear  INT;
  miMonth INT;
  miDay   INT;
  miHour  INT;
  miMin   INT;
  miSec   INT;
  miDaysInMonth INT;
BEGIN
  IF piPeriodUnitId = 7801 THEN
    RETURN pdLastTime::DATE + piPeriodsCount;
  ELSEIF piPeriodUnitId = 7802 THEN
    RETURN pdLastTime::DATE + piPeriodsCount * 7;
  ELSEIF piPeriodUnitId = 7803 THEN
    SELECT
      extract(year   from pdLastTime),
      extract(month  from pdLastTime),
      extract(day    from pdLastTime),
      extract(hour   from pdLastTime),
      extract(minute from pdLastTime),
      extract(second from pdLastTime)
    INTO
      miYear ,
      miMonth,
      miDay  ,
      miHour ,
      miMin  ,
      miSec;
    
    miMonth:= miMonth + piPeriodsCount;
    IF miMonth > 12 THEN
      miYear:= miYear + miMonth/12;
      miMonth:= miMonth % 12;
      IF miMonth = 0 THEN miMonth = 12; END IF;
    END IF;
    IF miDay > 28 THEN
      IF miMonth = 2 THEN
        IF (miYear%4=0 AND miYear%100!=0) OR miYear%400=0 THEN
          miDaysInMonth:= 29;
        ELSE
          miDaysInMonth:= 28;
        END IF;
      ELSEIF miMonth IN (4,6,9,11) THEN
        miDaysInMonth:= 30;
      ELSE
        miDaysInMonth:= 31;
      END IF;
      IF miDay > miDaysInMonth THEN miDay:= miDaysInMonth; END IF;
    END IF;
    RETURN (miYear || '-' || miMonth || '-' || miDay || ' ' || miHour || ':' || miMin || ':' || miSec)::TIMESTAMP;
  ELSE
    RAISE EXCEPTION 'Invalid period unit identifier ''%''.', piPeriodUnitId;
  END IF;
END$$;
CREATE FUNCTION get_place_category_parents(integer) RETURNS SETOF category_id
    LANGUAGE plpgsql
    AS $$DECLARE
piCategory ALIAS FOR $1;
queryCat varchar(255);
queryCatRecursion varchar(255);
category context_id%rowtype;
catParent context_id%rowtype;
BEGIN
     queryCat := ' SELECT cat.fkParent FROM cm_place_category cat
JOIN isms_context c ON(c.pkContext = cat.fkContext and c.nState <> 2705)
WHERE  cat.fkContext = ' || piCategory;
     FOR category IN EXECUTE queryCat LOOP
if(category.id IS NOT NULL) THEN
    RETURN NEXT category;
    FOR catParent IN EXECUTE 'SELECT pkCategory FROM get_place_category_parents('|| category.id ||')' LOOP
      RETURN NEXT catParent;
    END LOOP;
END IF;
     END LOOP;
     RETURN ;
END$$;
CREATE FUNCTION get_place_category_parents_threats() RETURNS SETOF place_category_threats
    LANGUAGE plpgsql
    AS $$DECLARE
queryCat varchar(1000);
queryCatRecursion varchar(255);
place cm_place%rowtype;
parent place_category_threats%rowtype;
BEGIN
     FOR place IN EXECUTE 'SELECT p.* from view_rm_place_active p' LOOP
if(place.fktype IS NOT NULL) THEN
    FOR parent IN EXECUTE 'SELECT * FROM get_place_category_parents_threats('|| place.fktype ||')' LOOP
      parent.fkplace = place.fkcontext;
      RETURN NEXT parent;
    END LOOP;
END IF;
     END LOOP;
     RETURN ;
END$$;
CREATE FUNCTION get_place_category_parents_threats(integer) RETURNS SETOF place_category_threat
    LANGUAGE plpgsql
    AS $$DECLARE
piCategory ALIAS FOR $1;
queryCat varchar(1000);
queryCatRecursion varchar(255);
category place_category_threat%rowtype;
catParent place_category_threat%rowtype;
c varchar(1);
BEGIN
     queryCat := ' SELECT cat.fkcontext, pct.fkthreat, cat.fkparent FROM cm_place_category cat
JOIN isms_context c ON(c.pkContext = cat.fkContext and c.nState <> 2705)
LEFT JOIN (select pct2.* from cm_place_category_threat pct2 join view_cm_threat_active ta on ta.fkcontext=pct2.fkthreat)  
      pct on pct.fkplacecategory = cat.fkcontext
WHERE cat.fkContext = ' || piCategory;
     c := '1';
     FOR category IN EXECUTE queryCat LOOP
if(category.fkthreat IS NOT NULL) THEN
   RETURN NEXT category;
END IF;
if(category.fkParent IS NOT NULL AND c = '1') THEN
    c := '2';
    FOR catParent IN EXECUTE 'SELECT * FROM get_place_category_parents_threats('|| category.fkParent ||')' LOOP
      RETURN NEXT catParent;
    END LOOP;
END IF;
     END LOOP;
     RETURN ;
END$$;
CREATE FUNCTION get_place_category_tree(integer, integer) RETURNS SETOF tree_category
    LANGUAGE plpgsql
    AS $$DECLARE
piParent ALIAS FOR $1;
pnLevel ALIAS FOR $2;
nAuxLevel integer;
queryCat varchar(255);
queryCatAux varchar(255);
queryCatRecursion varchar(255);
categoryRoot tree_category%rowtype;
subCat tree_category%rowtype;
BEGIN
     nAuxLevel := pnLevel + 1;
     queryCatAux := ' SELECT fkContext, sName, ' || nAuxLevel::text || ' as nLevel FROM view_cm_place_category_active cat WHERE  fkParent '  ;
     IF piParent > 0 THEN
       queryCat := queryCatAux || ' = ' || piParent;
     ELSE 
       queryCat := queryCatAux || ' is NULL';
     END IF;
     FOR categoryRoot IN EXECUTE queryCat LOOP
        RETURN NEXT categoryRoot;
        queryCatRecursion := 'SELECT fkContext, sName, nLevel FROM get_place_category_tree(' || categoryRoot.fkContext::text || ' , ' || nAuxLevel::text || ' ) ';
        FOR subCat IN EXECUTE queryCatRecursion LOOP
    RETURN NEXT subCat;
        END LOOP;
     END LOOP;
     RETURN ;
END$$;
CREATE FUNCTION get_place_parents(integer, integer) RETURNS SETOF place_parent
    LANGUAGE plpgsql
    AS $$DECLARE
placeId ALIAS FOR $1;
parentId ALIAS FOR $2;
queryCat varchar(255);
queryCatRecursion varchar(255);
place place_parent%rowtype;
catParent place_parent%rowtype;
BEGIN
     queryCat := ' SELECT '||placeId||' as fkplace, p.fkParent FROM view_cm_place_active p
WHERE  p.fkContext = ' || parentId;
     FOR place IN EXECUTE queryCat LOOP
if(place.fkparent IS NOT NULL) THEN
    RETURN NEXT place;
    FOR catParent IN EXECUTE 'SELECT pp.fkplace, pp.fkparent FROM get_place_parents('||placeId||','|| place.fkparent||') pp' LOOP
      RETURN NEXT catParent;
    END LOOP;
END IF;
     END LOOP;
     RETURN ;
END$$;
CREATE FUNCTION get_place_parents() RETURNS SETOF place_parent
    LANGUAGE plpgsql
    AS $$DECLARE
queryCat varchar(255);
queryCatRecursion varchar(255);
place place_parent%rowtype;
catParent place_parent%rowtype;
BEGIN
     queryCat := ' SELECT fkcontext, fkparent FROM view_cm_place_active';
     FOR place IN EXECUTE queryCat LOOP
if(place.fkparent IS NOT NULL) THEN
    RETURN NEXT place;
    FOR catParent IN EXECUTE 'SELECT pp.fkplace, pp.fkparent FROM get_place_parents('||place.fkplace||','|| place.fkparent||') pp' LOOP
      RETURN NEXT catParent;
    END LOOP;
END IF;
     END LOOP;
     RETURN ;
END$$;
CREATE FUNCTION get_process_category_parents(integer) RETURNS SETOF category_id
    LANGUAGE plpgsql
    AS $$DECLARE
piCategory ALIAS FOR $1;
queryCat varchar(255);
queryCatRecursion varchar(255);
category context_id%rowtype;
catParent context_id%rowtype;
BEGIN
     queryCat := ' SELECT cat.fkParent FROM cm_process_category cat
JOIN isms_context c ON(c.pkContext = cat.fkContext and c.nState <> 2705)
WHERE  cat.fkContext = ' || piCategory;
     FOR category IN EXECUTE queryCat LOOP
if(category.id IS NOT NULL) THEN
    RETURN NEXT category;
    FOR catParent IN EXECUTE 'SELECT pkCategory FROM get_process_category_parents('|| category.id ||')' LOOP
      RETURN NEXT catParent;
    END LOOP;
END IF;
     END LOOP;
     RETURN ;
END$$;
CREATE FUNCTION get_process_category_parents_threats(integer) RETURNS SETOF process_category_threat
    LANGUAGE plpgsql
    AS $$DECLARE
piCategory ALIAS FOR $1;
queryCat varchar(1000);
queryCatRecursion varchar(255);
category process_category_threat%rowtype;
catParent process_category_threat%rowtype;
c varchar(1);
BEGIN
     queryCat := ' SELECT cat.fkcontext, pct.fkthreat, cat.fkparent FROM cm_process_category cat
JOIN isms_context c ON(c.pkContext = cat.fkContext and c.nState <> 2705)
LEFT JOIN (select pct2.* from cm_process_category_threat pct2 join view_cm_threat_active ta on ta.fkcontext=pct2.fkthreat)  
      pct on pct.fkprocesscategory = cat.fkcontext
WHERE cat.fkContext = ' || piCategory;
     c := '1';
     FOR category IN EXECUTE queryCat LOOP
if(category.fkthreat IS NOT NULL) THEN
   RETURN NEXT category;
END IF;
if(category.fkParent IS NOT NULL AND c = '1') THEN
    c := '2';
    FOR catParent IN EXECUTE 'SELECT * FROM get_process_category_parents_threats('|| category.fkParent ||')' LOOP
      RETURN NEXT catParent;
    END LOOP;
END IF;
     END LOOP;
     RETURN ;
END$$;
CREATE FUNCTION get_process_category_parents_threats() RETURNS SETOF process_category_threats
    LANGUAGE plpgsql
    AS $$DECLARE
queryCat varchar(1000);
queryCatRecursion varchar(255);
process rm_process%rowtype;
parent process_category_threats%rowtype;
BEGIN
     FOR process IN EXECUTE 'SELECT p.* from view_rm_process_active p' LOOP
if(process.fktype IS NOT NULL) THEN
    FOR parent IN EXECUTE 'SELECT * FROM get_process_category_parents_threats('|| process.fktype ||')' LOOP
      parent.fkprocess = process.fkcontext;
      RETURN NEXT parent;
    END LOOP;
END IF;
     END LOOP;
     RETURN ;
END$$;
CREATE FUNCTION get_process_category_tree(integer, integer) RETURNS SETOF tree_category
    LANGUAGE plpgsql
    AS $$DECLARE
piParent ALIAS FOR $1;
pnLevel ALIAS FOR $2;
nAuxLevel integer;
queryCat varchar(255);
queryCatAux varchar(255);
queryCatRecursion varchar(255);
categoryRoot tree_category%rowtype;
subCat tree_category%rowtype;
BEGIN
     nAuxLevel := pnLevel + 1;
     queryCatAux := ' SELECT fkContext, sName, ' || nAuxLevel::text || ' as nLevel FROM view_cm_process_category_active cat WHERE  fkParent '  ;
     IF piParent > 0 THEN
       queryCat := queryCatAux || ' = ' || piParent;
     ELSE 
       queryCat := queryCatAux || ' is NULL';
     END IF;
     FOR categoryRoot IN EXECUTE queryCat LOOP
        RETURN NEXT categoryRoot;
        queryCatRecursion := 'SELECT fkContext, sName, nLevel FROM get_process_category_tree(' || categoryRoot.fkContext::text || ' , ' || nAuxLevel::text || ' ) ';
        FOR subCat IN EXECUTE queryCatRecursion LOOP
    RETURN NEXT subCat;
        END LOOP;
     END LOOP;
     RETURN ;
END$$;
CREATE FUNCTION get_process_value(integer) RETURNS double precision
    LANGUAGE sql
    AS $$
   SELECT MAX(value) FROM (
      SELECT MAX(a.nValue) as value
	  FROM rm_asset a
	  JOIN isms_context ctxA ON(ctxA.pkContext = a.fkContext and ctxA.nState <> 2705)
	  JOIN rm_process_asset pa ON(pa.fkAsset = a.fkContext)
	  JOIN isms_context ctxPA ON(ctxPA.pkContext = pa.fkContext and ctxPA.nState <> 2705)
	  WHERE pa.fkProcess = $1
      UNION
	  SELECT 0 as value
   ) as buffer
$$;
CREATE FUNCTION get_processes_from_area(integer) RETURNS SETOF process_id
    LANGUAGE plpgsql
    AS $$DECLARE
   piArea ALIAS FOR $1;
   querySubAreas varchar(255);
   queryProcFromArea varchar(255);
   ctxProc context_id%rowtype;
   ctxArea context_id%rowtype;
   ctxProcAux context_id%rowtype;
BEGIN

     querySubAreas := ' SELECT fkContext FROM rm_area a
	JOIN isms_context c ON(c.pkContext = a.fkContext and c.nState <> 2705)	
        WHERE fkParent = ' || piArea;

     queryProcFromArea := ' SELECT fkContext FROM rm_process p
        JOIN isms_context c ON(c.pkContext = p.fkContext and c.nState <> 2705)	
        WHERE fkArea = ' || piArea;

     FOR ctxProc IN EXECUTE queryProcFromArea LOOP
        if(ctxProc.id IS NOT NULL) THEN
		RETURN NEXT ctxProc;
	END IF;
     END LOOP;

     FOR ctxArea IN EXECUTE querySubAreas LOOP
        if(ctxArea.id IS NOT NULL) THEN
	    FOR ctxProcAux IN EXECUTE ' SELECT pkProcess FROM get_processes_from_area( ' || ctxArea.id || ') ' LOOP
		RETURN NEXT ctxProcAux;
	    END LOOP;
	END IF;
     END LOOP;

     RETURN ;
END$$;
CREATE FUNCTION get_risk_parameter_reduction(integer, integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$DECLARE
  piRisk ALIAS FOR $1;
  piParameter ALIAS FOR $2;
  queryReduction varchar(1024);
  miValue integer;
  handleReduct risk_par_reduction%rowtype;
  
BEGIN
  miValue :=0;

  queryReduction :='
  SELECT parameter_reduction_value, risk_parameter_value FROM 
  (
    SELECT SUM(rcpvn.nvalue) as parameter_reduction_value 
      FROM view_rm_risk_control_active rc
        JOIN rm_risk_control_value rcv ON (rcv.fkriskcontrol = rc.fkcontext)
        JOIN view_rm_control_active c ON (c.fkContext = rc.fkcontrol AND c.bIsActive = 1)
        JOIN rm_rc_parameter_value_name rcpvn ON (rcv.fkrcvaluename = rcpvn.pkrcvaluename)
      WHERE fkrisk = ' || @piRisk || ' AND fkparametername = ' || @piParameter || '
  ) prv,
  (
  SELECT nvalue as risk_parameter_value 
     FROM rm_risk_value rv
        JOIN isms_context ctx ON (ctx.pkContext = rv.fkRisk AND ctx.nState <> 2705)
        JOIN rm_parameter_value_name pvn ON (rv.fkvaluename = pvn.pkvaluename)
     WHERE fkrisk = ' || @piRisk || ' AND fkparametername = ' || @piParameter || '
  ) rpv
      ';
  FOR handleReduct IN EXECUTE queryReduction LOOP
    if(handleReduct.par_red_value IS NOT NULL) THEN
        miValue = handleReduct.risk_par_value - handleReduct.par_red_value;
        if(miValue<1) THEN
             miValue:=1;
        END IF;
    ELSE
      miValue := handleReduct.risk_par_value;
    END IF;
  END LOOP;

  RETURN miValue;
END$$;
CREATE FUNCTION get_risk_value(integer) RETURNS double precision
    LANGUAGE plpgsql
    AS $$DECLARE
  piRisk ALIAS FOR $1;
  queryCompare varchar(1024);
  queryValues varchar(1024);
  queryProb varchar(256);
  riskCont risk_value_cont%rowtype;
  riskVal2 risk_values%rowtype;
  riskProb row_int%rowtype;
  miResult double precision;
  miRes1 double precision;
  miRes2 double precision;
  miParWeight double precision;
  miBlueRisk integer;
  msFormulaType varchar(32);
BEGIN
  miResult := 0;
  miRes1 := 0;
  miRes2 := 0;
  miBlueRisk := 0; 
  miParWeight :=0;

  queryCompare := '
    SELECT count_sys, count_risk, count_asset 
      FROM (SELECT count(pkparametername) as count_sys  FROM rm_parameter_name pn) buffer_sys,
           (SELECT count(fkparametername) as count_risk FROM rm_risk_value WHERE fkrisk =  ' || piRisk || ') buffer_risk,
           (SELECT count(fkparametername) as count_asset 
              FROM rm_asset_value av
                JOIN view_rm_risk_active r ON (r.fkasset = av.fkasset AND r.fkcontext =  ' || piRisk || ')
                JOIN isms_context ctx ON(ctx.pkContext = r.fkAsset AND ctx.nState <> 2705)
            ) buffer_asset
   ';
  queryValues := '
    SELECT pn.sname as parameter_name, pvn.nvalue as risk_value, pvn2.nvalue as asset_value, pn.nweight as parameter_weight 
      FROM view_rm_risk_active r
        JOIN isms_context ctx ON (ctx.pkContext = r.fkAsset AND ctx.nState <> 2705 AND r.fkContext = ' || piRisk || ')
        JOIN rm_risk_value rv ON (rv.fkrisk = r.fkcontext)
        JOIN rm_parameter_value_name pvn ON (rv.fkvaluename = pvn.pkvaluename)
        JOIN rm_parameter_name pn ON (rv.fkparametername = pn.pkparametername)
        JOIN rm_asset_value av ON (pn.pkparametername = av.fkparametername AND av.fkasset = r.fkasset)
        JOIN rm_parameter_value_name pvn2 ON (av.fkvaluename = pvn2.pkvaluename)
  ';
  queryProb :='
    SELECT pvn.nvalue as risk_prob_value 
      FROM view_rm_risk_active r
      JOIN rm_parameter_value_name pvn ON (r.fkprobabilityvaluename = pvn.pkvaluename)
      WHERE r.fkcontext = ' || piRisk || '
   ';

  FOR riskCont IN EXECUTE queryCompare LOOP
    IF ( (riskCont.cont_system <> riskCont.cont_risk) OR (riskCont.cont_system <> riskCont.cont_asset) ) THEN
      miBlueRisk := 1;
    END IF;
  END LOOP;

  SELECT INTO msFormulaType sValue FROM isms_config WHERE pkConfig = 5701;
  
  IF (msFormulaType = '8301') THEN
	    IF(miBlueRisk <> 1) THEN
	      FOR riskVal2 IN EXECUTE queryValues LOOP
      		miResult := miResult + (riskVal2.asset + riskVal2.risk)* riskVal2.par_weight;
      		miParWeight := miParWeight + riskVal2.par_weight;
	      END LOOP;
	      IF(miParWeight >= 1) THEN
      		FOR riskProb IN EXECUTE queryProb LOOP
      		  miResult := (miResult * riskProb.value) / (2 * miParWeight);
      		END LOOP;
	      ELSE
          miResult := 0;
	      END IF;
	    END IF;
  ELSE
	  IF(miBlueRisk <> 1) THEN
	    FOR riskVal2 IN EXECUTE queryValues LOOP
	      miRes1 := miRes1 + (riskVal2.asset * riskVal2.risk) * riskVal2.par_weight;
	      miRes2 := miRes2 + (riskVal2.asset + riskVal2.risk) * riskVal2.par_weight;	      
	      miParWeight := miParWeight + riskVal2.par_weight;
	    END LOOP;
	    miRes1 := miRes1 / miParWeight;
	    miRes2 := miRes2 / miParWeight / 2;
	    IF(miParWeight >= 1) THEN
	      FOR riskProb IN EXECUTE queryProb LOOP		
          miResult := (miRes1 / miRes2) * riskProb.value;
	      END LOOP;
	    ELSE
	      miResult := 0;
	    END IF;
	  END IF;	
  END IF;

  RETURN miResult;
END$$;
CREATE FUNCTION get_risk_value_residual(integer) RETURNS double precision
    LANGUAGE plpgsql
    AS $$DECLARE
  piRisk ALIAS FOR $1;
  queryCompare varchar(1024);
  queryValues varchar(1024);
  queryProb varchar(1024);
  miResult double precision;
  miRes1 double precision;
  miRes2 double precision;
  miProbResult double precision;
  miParWeight double precision;
  miBlueRisk integer;
  riskCont risk_value_cont%rowtype;
  riskProb risk_prob%rowtype;
  riskValue risk_values%rowtype;
  msFormulaType varchar(32);
BEGIN
  miParWeight:=0;
  miBlueRisk := 0;
  miResult := 0;
  miRes1 := 0;
  miRes2 := 0;
  miProbResult := 0;
  queryCompare := '
    SELECT count_sys, count_risk, count_asset
      FROM (SELECT count(pkparametername) as count_sys  FROM rm_parameter_name pn) buffer_sys,
           (SELECT count(fkparametername) as count_risk FROM rm_risk_value WHERE fkrisk = ' || piRisk || ') buffer_risk,
           (SELECT count(fkparametername) as count_asset FROM rm_asset_value av
              JOIN view_rm_risk_active r ON (r.fkasset = av.fkasset AND r.fkContext = ' || piRisk || ')
              JOIN view_rm_asset_active a ON (a.fkContext = av.fkAsset)
            ) buffer_asset
       ';
  queryValues :='
    SELECT pn.sname as parameter_name,  pn.pkparametername, pvn.nvalue as asset_value, pn.nweight as parameter_weight 
      FROM view_rm_risk_active r
        JOIN isms_context ctx ON(ctx.pkContext = r.fkAsset AND r.fkContext = ' || piRisk || ')
        JOIN rm_asset_value av ON (av.fkasset = r.fkasset)
        JOIN rm_parameter_name pn ON (av.fkparametername = pn.pkparametername)
        JOIN rm_parameter_value_name pvn ON (av.fkvaluename = pvn.pkvaluename)
       ';
  queryProb := '
    SELECT prob_reduction_value, risk_prob_value FROM 
    (
      SELECT sum(rcpvn.nvalue) as prob_reduction_value
        FROM rm_risk_control rc
          JOIN isms_context ctx ON(ctx.pkContext = rc.fkRisk and ctx.nState <> 2705 AND rc.fkrisk = ' || piRisk || ')
          JOIN view_rm_control_active c ON (c.fkContext = rc.fkControl AND c.bIsActive = 1)
          JOIN rm_rc_parameter_value_name rcpvn ON (rc.fkprobabilityvaluename = rcpvn.pkrcvaluename)
      ) prv,
      (
      SELECT pvn.nvalue as risk_prob_value 
        FROM view_rm_risk_active r
          JOIN rm_parameter_value_name pvn ON (r.fkprobabilityvaluename = pvn.pkvaluename AND r.fkContext = ' || piRisk || ')
      ) rpv
  ';
  FOR riskCont IN EXECUTE queryCompare LOOP
    IF((riskCont.cont_system <> riskCont.cont_risk) or (riskCont.cont_system <> riskCont.cont_asset)) THEN
      miBlueRisk:=1;
    END IF;
  END LOOP;
  IF(miBlueRisk <> 1) THEN
    FOR riskProb IN EXECUTE queryProb LOOP
      IF(riskProb.reduct IS NULL) THEN
         miProbResult := riskProb.value;
      ELSE
        miProbResult := riskProb.value - riskProb.reduct;
        IF(miProbResult < 1) THEN
          miProbResult := 1;
        END IF;
      END IF;
    END LOOP;

    SELECT INTO msFormulaType sValue FROM isms_config WHERE pkConfig = 5701;

    IF (msFormulaType = '8301') THEN
	    FOR riskValue IN EXECUTE queryValues LOOP
	      miParWeight := miParWeight + riskValue.par_weight;
	      miResult := miResult + (riskValue.asset + get_risk_parameter_reduction(piRisk,riskValue.risk) ) * riskValue.par_weight;
	    END LOOP;
	    IF(miParWeight >= 1) THEN
	      miResult := (miResult * miProbResult) / (2 * miParWeight);
	    ELSE
	      miResult := 0;
	    END IF;
     ELSE
      FOR riskValue IN EXECUTE queryValues LOOP
	      miRes1 := miRes1 + (riskValue.asset * get_risk_parameter_reduction(piRisk,riskValue.risk)) * riskValue.par_weight;
	      miRes2 := miRes2 + (riskValue.asset + get_risk_parameter_reduction(piRisk,riskValue.risk)) * riskValue.par_weight;	      
	      miParWeight := miParWeight + riskValue.par_weight;
	    END LOOP;
	    miRes1 := miRes1 / miParWeight;
	    miRes2 := miRes2 / miParWeight / 2;
	    IF(miParWeight >= 1) THEN	      
	      miResult := (miRes1 / miRes2) * miProbResult;	      
	    ELSE
	      miResult := 0;
	    END IF;
     END IF;
  END IF;
  RETURN miResult;
END$$;
CREATE FUNCTION get_risks_from_asset(integer) RETURNS SETOF risk_id
    LANGUAGE plpgsql
    AS $$DECLARE
   piAsset ALIAS FOR $1;
   queryRisks varchar(255);
   riskIds context_id%rowtype;
BEGIN
     queryRisks := '
          SELECT fkContext FROM rm_risk r
          JOIN isms_context ctx1 ON(ctx1.pkContext = r.fkContext and ctx1.nState <> 2705)
          JOIN isms_context ctx2 ON(ctx2.pkContext = r.fkAsset and ctx2.nState <> 2705 and r.fkAsset = ' || piAsset || ')
     ';

     FOR riskIds IN EXECUTE queryRisks LOOP
        if(riskIds.id IS NOT NULL) THEN
		RETURN NEXT riskIds;
	END IF;
     END LOOP;

     RETURN ;
END$$;
CREATE FUNCTION get_section_parents(integer) RETURNS SETOF section_id
    LANGUAGE plpgsql
    AS $$DECLARE
   piSection ALIAS FOR $1;
   queryParentSec varchar(255);
   queryEventCat varchar(255);
   ctxSec context_id%rowtype;
   ctxSecRec context_id%rowtype;
BEGIN
     queryParentSec := ' SELECT fkParent FROM rm_section_best_practice sbp
	JOIN isms_context ctx1 ON(ctx1.pkContext = sbp.fkContext and ctx1.nState <> 2705 and sbp.fkContext = ' || piSection || ') ';

     FOR ctxSec IN EXECUTE queryParentSec LOOP
        if(ctxSec.id IS NOT NULL) THEN
		RETURN NEXT ctxSec;
                FOR ctxSecRec IN EXECUTE ' SELECT pkSection FROM get_section_parents( ' || ctxSec.id || ') ' LOOP
                    RETURN NEXT ctxSecRec;
                END LOOP;
	END IF;
     END LOOP;

     RETURN ;
END$$;
CREATE FUNCTION get_section_parents_trash(integer) RETURNS SETOF section_id
    LANGUAGE plpgsql
    AS $$DECLARE
   piSection ALIAS FOR $1;
   queryParentSec varchar(255);
   queryEventCat varchar(255);
   ctxSec context_id%rowtype;
   ctxSecRec context_id%rowtype;
BEGIN
     queryParentSec := ' SELECT fkParent FROM rm_section_best_practice sbp where sbp.fkContext = ' || piSection;

     FOR ctxSec IN EXECUTE queryParentSec LOOP
        if(ctxSec.id IS NOT NULL) THEN
		RETURN NEXT ctxSec;
                FOR ctxSecRec IN EXECUTE ' SELECT pkSection FROM get_section_parents_trash( ' || ctxSec.id || ') ' LOOP
                    RETURN NEXT ctxSecRec;
                END LOOP;
	END IF;
     END LOOP;

     RETURN ;
END$$;
CREATE FUNCTION get_section_tree(integer, integer) RETURNS SETOF tree_best_practice
    LANGUAGE plpgsql
    AS $$DECLARE
   piParent ALIAS FOR $1;
   piLevel ALIAS FOR $2;
   nAuxLevel integer;
   queryBP varchar(1024);
   queryBPAux varchar(1024);
   queryBPRecursion varchar(255);
   bestPracticeRoot tree_best_practice%rowtype;
   subBestPractice tree_best_practice%rowtype;

BEGIN
     nAuxLevel := piLevel + 1;
     queryBPAux := ' SELECT fkContext, sName, ' || nAuxLevel::text || ' as nLevel FROM rm_section_best_practice sbp
		JOIN isms_context c ON(c.pkContext = sbp.fkContext and c.nState <> 2705)	
		WHERE  fkParent '  ;
     IF piParent > 0 THEN
       queryBP := queryBPAux || ' = ' || piParent;
     ELSE 
       queryBP := queryBPAux || ' is NULL';
     END IF;
     FOR bestPracticeRoot IN EXECUTE queryBP LOOP
        RETURN NEXT bestPracticeRoot;
        queryBPRecursion := 'SELECT fkContext, sName, nLevel FROM get_section_tree(' || bestPracticeRoot.fkContext::text || ' , ' || nAuxLevel::text || ' ) ';
        FOR subBestPractice IN EXECUTE queryBPRecursion LOOP
	    RETURN NEXT subBestPractice;
        END LOOP;
     END LOOP;
     RETURN ;
END$$;
CREATE FUNCTION get_sub_areas(integer) RETURNS SETOF area_id
    LANGUAGE plpgsql
    AS $$DECLARE
   piArea ALIAS FOR $1;
   querySubArea varchar(255);
   queryEventCat varchar(255);
   ctxArea context_id%rowtype;
   ctxAreaRec context_id%rowtype;
BEGIN
     querySubArea := ' SELECT fkContext FROM rm_area a
          WHERE fkParent = ' || piArea;

     FOR ctxArea IN EXECUTE querySubArea LOOP
        if(ctxArea.id IS NOT NULL) THEN
		RETURN NEXT ctxArea;
                FOR ctxAreaRec IN EXECUTE ' SELECT pkArea FROM get_sub_areas( ' || ctxArea.id || ') ' LOOP
                    RETURN NEXT ctxAreaRec;
                END LOOP;
	END IF;
     END LOOP;

     RETURN ;
END$$;
CREATE FUNCTION get_sub_categories(integer) RETURNS SETOF category_id
    LANGUAGE plpgsql
    AS $$DECLARE
   piCategory ALIAS FOR $1;
   querySubCat varchar(255);
   queryEventCat varchar(255);
   ctxCat context_id%rowtype;
   ctxCatRec context_id%rowtype;
BEGIN
    querySubCat := ' SELECT fkContext FROM rm_category cat
        WHERE cat.fkParent = ' || piCategory;

    FOR ctxCat IN EXECUTE querySubCat LOOP
        if(ctxCat.id IS NOT NULL) THEN
            RETURN NEXT ctxCat;
            FOR ctxCatRec IN EXECUTE ' SELECT pkCategory FROM get_sub_Categories( ' || ctxCat.id || ') ' LOOP
                RETURN NEXT ctxCatRec;
            END LOOP;
        END IF;
    END LOOP;
    RETURN ;
END$$;
CREATE FUNCTION get_sub_documents(iddoc integer) RETURNS SETOF document_id
    LANGUAGE plpgsql
    AS $$
  DECLARE
    doc document_id;
    subDoc document_id;
  BEGIN
    FOR doc IN SELECT fkContext FROM pm_document WHERE fkParent = idDoc LOOP
      RETURN NEXT doc;
      FOR subDoc IN SELECT * FROM get_sub_documents(doc.document_id) LOOP
        RETURN NEXT subDoc;
      END LOOP;
    END LOOP;
  END
$$;
CREATE FUNCTION get_sub_places(integer) RETURNS SETOF area_id
    LANGUAGE plpgsql
    AS $$DECLARE
   piArea ALIAS FOR $1;
   querySubArea varchar(255);
   queryEventCat varchar(255);
   ctxArea context_id%rowtype;
   ctxAreaRec context_id%rowtype;
BEGIN
     querySubArea := ' SELECT fkContext FROM cm_place a
          WHERE fkParent = ' || piArea;
     FOR ctxArea IN EXECUTE querySubArea LOOP
        if(ctxArea.id IS NOT NULL) THEN
RETURN NEXT ctxArea;
                FOR ctxAreaRec IN EXECUTE ' SELECT pkArea FROM get_sub_places( ' || ctxArea.id || ') ' LOOP
                    RETURN NEXT ctxAreaRec;
                END LOOP;
END IF;
     END LOOP;
     RETURN ;
END$$;
CREATE FUNCTION get_sub_sections(integer) RETURNS SETOF section_id
    LANGUAGE plpgsql
    AS $$DECLARE
   piSection ALIAS FOR $1;
   querySubSec varchar(255);
   queryEventSec varchar(255);
   ctxSec context_id%rowtype;
   ctxSecRec context_id%rowtype;
BEGIN
     querySubSec := ' SELECT fkContext FROM rm_section_best_practice sec
          WHERE sec.fkParent = ' || piSection;

     FOR ctxSec IN EXECUTE querySubSec LOOP
        IF(ctxSec.id IS NOT NULL) THEN
            RETURN NEXT ctxSec;
            FOR ctxSecRec IN EXECUTE ' SELECT pkSection FROM get_sub_sections( ' || ctxSec.id || ') ' LOOP
                RETURN NEXT ctxSecRec;
            END LOOP;
        END IF;
     END LOOP;

  RETURN ;
END$$;
CREATE FUNCTION get_superareas_by_user(integer) RETURNS SETOF area_ids
    LANGUAGE plpgsql
    AS $$DECLARE 
    userId ALIAS FOR $1;
    query_area varchar(255);
    area area_ids%rowtype;
    area2 area_ids%rowtype;
BEGIN
   query_area := ' SELECT fkContext FROM rm_area a
		    JOIN isms_context c ON(c.pkContext = a.fkContext and c.nState <> 2705)
		      WHERE a.fkResponsible = ' || userId;
   
    FOR area IN EXECUTE query_area LOOP
	RETURN NEXT area;
        FOR area2 IN EXECUTE ' SELECT pkArea FROM get_area_parents(' || area.area_id || ') ' LOOP
	    RETURN NEXT area2;
        END LOOP;

   END LOOP;

   RETURN ;	
END$$;
CREATE FUNCTION get_supercategories_events(picategoryid integer) RETURNS SETOF event_id
    LANGUAGE plpgsql
    AS $$DECLARE
  mrEvent record;
  miCategoryId integer;
BEGIN
  miCategoryId = piCategoryId;
  WHILE miCategoryId IS NOT NULL LOOP
    FOR mrEvent IN SELECT fkContext FROM view_rm_event_active WHERE fkCategory = miCategoryId LOOP
      RETURN NEXT mrEvent;
    END LOOP;
    SELECT fkParent INTO miCategoryId FROM view_rm_category_active WHERE fkContext = miCategoryId;
  END LOOP;
  RETURN;
END$$;
CREATE FUNCTION get_top10_area_risks() RETURNS SETOF area_risk_id
    LANGUAGE plpgsql
    AS $$DECLARE
   queryValidArea varchar(255);
   queryTop10 varchar(1024);
   validArea context_id%rowtype;
   top10AreaRisk area_risk_id%rowtype;
BEGIN
     queryValidArea := 'SELECT  ar.fkContext FROM isms_context cont_ar
	                         JOIN rm_area ar ON (cont_ar.pkContext = ar.fkContext AND cont_ar.nState <> 2705)';    

     FOR validArea IN EXECUTE queryValidArea LOOP
          queryTop10 := ' SELECT ar.fkContext, r.fkContext FROM isms_context cont_ar
		      JOIN rm_area ar ON (cont_ar.pkContext = ar.fkContext AND cont_ar.nState <> 2705)   
		      JOIN rm_process p ON (ar.fkContext = p.fkArea)
		      JOIN isms_context cont_p ON (cont_p.pkContext = p.fkContext AND cont_p.nState <> 2705)
		      JOIN rm_process_asset pa ON (p.fkContext = pa.fkProcess)
		      JOIN rm_asset a ON (pa.fkAsset = a.fkContext)
		      JOIN isms_context cont_a ON (cont_a.pkContext = a.fkContext AND cont_a.nState <> 2705)
		      JOIN rm_risk r ON (a.fkContext = r.fkAsset)
		      JOIN isms_context cont_r ON (r.fkContext = cont_r.pkContext AND cont_r.nState <> 2705)
		      WHERE ar.fkContext = ' || validArea.id || '
		      GROUP BY r.fkContext, r.nValueResidual, ar.fkContext
		      ORDER BY r.nValueResidual DESC 
		   LIMIT 10 ';
	
           FOR top10AreaRisk IN EXECUTE queryTop10 LOOP
                RETURN NEXT top10AreaRisk;
           END LOOP;
     END LOOP;

     RETURN;
END$$;
CREATE FUNCTION get_top10_process_risks() RETURNS SETOF process_risk_id
    LANGUAGE plpgsql
    AS $$DECLARE
   queryValidProcess varchar(255);
   queryTop10 varchar(1024);
   validProcess context_id%rowtype;
   top10ProcessRisk process_risk_id%rowtype;
BEGIN
     queryValidProcess := ' SELECT  p.fkContext FROM isms_context cont_p
                          JOIN rm_process p ON (cont_p.pkContext = p.fkContext AND cont_p.nState <> 2705) ';    

     FOR validProcess IN EXECUTE queryValidProcess LOOP
          queryTop10 := ' SELECT  p.fkContext, r.fkContext
		           FROM isms_context cont_p
		           JOIN rm_process p ON (cont_p.pkContext = p.fkContext AND cont_p.nState <> 2705)
		           JOIN rm_process_asset pa ON (p.fkContext = pa.fkProcess)
		           JOIN rm_asset a ON (pa.fkAsset = a.fkContext)
		           JOIN isms_context cont_a ON (cont_a.pkContext = a.fkContext AND cont_a.nState <> 2705)
		           JOIN rm_risk r ON (a.fkContext = r.fkAsset)
		           JOIN isms_context cont_r ON (r.fkContext = cont_r.pkContext AND cont_r.nState <> 2705)
		           WHERE p.fkContext = ' || validProcess.id || '		
		           ORDER BY r.nValueResidual DESC
			   LIMIT 10 ';
	
           FOR top10ProcessRisk IN EXECUTE queryTop10 LOOP
                RETURN NEXT top10ProcessRisk;
           END LOOP;
     END LOOP;

     RETURN ;
END$$;
CREATE FUNCTION get_topn_revised_documents(pitruncatenumber integer) RETURNS SETOF context_rev_count
    LANGUAGE plpgsql
    AS $$
  DECLARE
    miDocument INTEGER;
    msName VARCHAR(256);
    miCurrentVersion INT;
    miDocCount INTEGER;
    miCounter INTEGER;
    rec context_rev_count%ROWTYPE;
  BEGIN
    miCounter = 0;
    FOR rec IN
      SELECT
        d.fkContext,
        d.sName,
        d.fkCurrentVersion,
        count(di.fkContext) as rev_count
      FROM
        view_pm_document_active d
        JOIN view_pm_doc_instance_active di ON (di.fkDocument = d.fkContext 
                                                AND di.nMajorVersion>1 
                                                AND di.dBeginProduction IS NOT NULL)
      GROUP BY d.fkContext, d.sName, d.fkCurrentVersion
      ORDER BY rev_count DESC, d.sName
    LOOP
      EXIT WHEN piTruncateNumber > 0 AND miCounter = piTruncateNumber;
      RETURN NEXT rec;
      miCounter = miCounter + 1;
    END LOOP;
    RETURN;
  END
$$;
CREATE FUNCTION plpgsql_call_handler() RETURNS language_handler
    LANGUAGE c
    AS '$libdir/plpgsql', 'plpgsql_call_handler';
CREATE FUNCTION recalculation_of_area_by_area() RETURNS trigger
    LANGUAGE plpgsql
    AS $$DECLARE
    miVchg double precision;
BEGIN

IF (TG_OP = 'UPDATE') THEN
    miVchg := (NEW.nValue - OLD.nValue);
    IF (miVchg <> 0) THEN
        UPDATE rm_area
        SET nValue = get_area_value(fkContext)
        WHERE fkContext = OLD.fkParent;
    END IF;
END IF;
RETURN NEW;
END$$;
CREATE FUNCTION recalculation_of_area_by_process_delete() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN

IF (TG_OP = 'DELETE') THEN
    UPDATE rm_area
    SET nValue = get_area_value(fkContext)
    WHERE fkContext = OLD.fkArea;
END IF;
RETURN NEW;
END$$;
CREATE FUNCTION recalculation_of_area_by_process_update() RETURNS trigger
    LANGUAGE plpgsql
    AS $$DECLARE
    miArea integer;
BEGIN
   miArea := NEW.fkArea;
IF (TG_OP = 'UPDATE')  THEN
  UPDATE rm_area SET nValue = get_area_value(fkContext) WHERE fkContext = miArea;
  
  IF (NEW.fkArea <> OLD.fkArea) THEN
      UPDATE rm_area SET nValue = get_area_value(fkContext) WHERE fkContext = OLD.fkArea;
  END IF;
END IF;
RETURN NEW;
END$$;
CREATE FUNCTION recalculation_of_asset_association() RETURNS trigger
    LANGUAGE plpgsql
    AS $$DECLARE
    miAsset integer;
BEGIN

IF (TG_OP = 'DELETE') THEN
    miAsset := OLD.fkDependent;
ELSE
   IF (NEW.fkDependent IS NOT NULL) THEN
       miAsset := NEW.fkDependent;
   ELSE
       miAsset := OLD.fkDependent;
   END IF;
END IF;

UPDATE rm_asset
  SET nValue = get_asset_value(fkContext)
     WHERE fkContext = miAsset;

RETURN NEW;
END$$;
CREATE FUNCTION recalculation_of_asset_for_risk_delete() RETURNS trigger
    LANGUAGE plpgsql
    AS $$DECLARE
BEGIN
IF (TG_OP = 'DELETE') THEN
    UPDATE rm_asset
    SET nValue = get_asset_value(fkContext)
    WHERE fkContext = OLD.fkAsset;
END IF;
RETURN NEW;
END$$;
CREATE FUNCTION recalculation_of_dependent_asset() RETURNS trigger
    LANGUAGE plpgsql
    AS $$DECLARE
    miVchg double precision;
BEGIN

IF (TG_OP = 'UPDATE') THEN
    miVchg := (NEW.nValue - OLD.nValue);
    IF (miVchg <> 0) THEN
      UPDATE rm_asset
        SET nValue = get_asset_value(fkContext)
        WHERE
            fkContext IN 
                (select  fkDependent from rm_asset_asset where fkAsset = OLD.fkContext);

    END IF;
END IF;
RETURN NEW;
END$$;
CREATE FUNCTION recalculation_of_process() RETURNS trigger
    LANGUAGE plpgsql
    AS $$DECLARE
  miVchg double precision;
BEGIN
IF (TG_OP = 'UPDATE') THEN
    miVchg := (NEW.nValue - OLD.nValue);
    IF (miVchg <> 0) THEN
        UPDATE rm_process
        SET nValue = get_process_value(rm_process.fkContext)
        FROM rm_process_asset pa
        WHERE rm_process.fkContext = pa.fkProcess
        AND pa.fkAsset = OLD.fkContext;
    END IF;
END IF;
RETURN NEW;
END;$$;
CREATE FUNCTION recalculation_of_process_association() RETURNS trigger
    LANGUAGE plpgsql
    AS $$DECLARE
 miProcess integer;
BEGIN

IF( TG_OP = 'DELETE') THEN
  miProcess= OLD.fkProcess;
ELSE
  miProcess= NEW.fkProcess;
END IF;

  UPDATE rm_process
    SET nValue = get_process_value(fkContext)
    WHERE fkContext = miProcess;
RETURN NEW;
END;$$;
CREATE FUNCTION recalculation_of_risk_for_insert() RETURNS trigger
    LANGUAGE plpgsql
    AS $$DECLARE
  mbAcceptRisk boolean;
BEGIN

IF (TG_OP = 'INSERT') THEN

    UPDATE rm_risk 
      SET nValue = get_risk_value(fkContext),
      nValueResidual = get_risk_value_residual(fkContext)
      WHERE fkContext = NEW.fkContext;
    UPDATE rm_asset
	  SET nValue = get_asset_value(fkContext)
	  WHERE fkContext = NEW.fkAsset;
END IF;
RETURN NEW;
END;$$;
CREATE FUNCTION recalculation_of_risk_for_update() RETURNS trigger
    LANGUAGE plpgsql
    AS $$DECLARE
    mbAcceptRisk boolean;
    mbAssetChanged boolean;
    mfValueResidual double precision;
    mbCondition boolean;
BEGIN
IF (TG_OP = 'UPDATE') THEN
    IF(NEW.nAcceptMode <> OLD.nAcceptMode) THEN
        mbAcceptRisk := true;
    ELSE
        mbAcceptRisk := false;
    END IF;
    IF(NEW.fkAsset <> OLD.fkAsset) THEN
        mbAssetChanged := true;
    ELSE
        mbAssetChanged := false;
    END IF;
    mfValueResidual := NEW.nValueResidual - OLD.nValueResidual;
    IF(  ( (mfValueResidual <> 0) AND 
           ( (NEW.nValueResidual IS NOT NULL) OR (OLD.nValueResidual IS NOT NULL) )  
         ) 
	 OR 
	 (mbAcceptRisk)
	 OR
	 (mbAssetChanged)
      ) 
    THEN
	mbCondition := true;
    ELSE
	mbCondition := false;
    END IF;
   IF (mbCondition) THEN
       UPDATE rm_risk 
       SET nValue = get_risk_value(fkContext),
       nValueResidual = get_risk_value_residual(fkContext)
       WHERE fkContext = NEW.fkContext;
       
       UPDATE rm_asset
          SET nValue = get_asset_value(fkContext)
          WHERE fkContext = NEW.fkAsset;

       IF (mbAssetChanged) THEN
          UPDATE rm_asset
          SET nValue = get_asset_value(fkContext)
          WHERE fkContext = OLD.fkAsset;
       END IF;
    END IF;
END IF;
RETURN NEW;
END$$;
CREATE FUNCTION recalculation_of_risks_values() RETURNS trigger
    LANGUAGE plpgsql
    AS $$DECLARE
      mbAcceptRisk boolean;
      mfValueResidual double precision;
      mbCondition boolean;
  BEGIN
    IF TG_OP = 'UPDATE' AND NEW.bIsActive != OLD.bIsActive THEN
      UPDATE rm_risk SET
        nValue = get_risk_value(NEW.fkContext),
        nValueResidual = get_risk_value_residual(NEW.fkContext)
      WHERE fkContext IN (
        SELECT fkRisk
        FROM rm_risk_control rc
        WHERE fkControl = NEW.fkContext
      );
    END IF;
    RETURN NEW;
  END$$;
CREATE FUNCTION recauculate_risk_for_risk_control_association() RETURNS trigger
    LANGUAGE plpgsql
    AS $$DECLARE
BEGIN
    IF (TG_OP = 'DELETE') THEN
	UPDATE rm_risk
	  SET nvalue = get_risk_value(OLD.fkRisk),
          nValueResidual = get_risk_value_residual(OLD.fkRisk)
             WHERE fkContext = OLD.fkRisk;
    END IF;
RETURN NEW;
END$$;
CREATE FUNCTION update_alert_sent() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN
  IF OLD.ddateaccomplished IS NULL AND NEW.ddateaccomplished IS NOT NULL THEN
    PERFORM *
    FROM
      wkf_schedule s
      JOIN wkf_task_schedule ts ON (ts.fkschedule = s.pkschedule)
      JOIN wkf_task t ON (t.nactivity = ts.nactivity AND t.fkcontext = ts.fkcontext AND t.bvisible = 1)
      JOIN (
        SELECT
          s.pkschedule AS schedule_id,
          COUNT(*) AS pendant_tasks
        FROM
          wkf_schedule s
          JOIN wkf_task_schedule ts ON (ts.fkschedule = s.pkschedule)
          JOIN wkf_task t ON (t.nactivity = ts.nactivity AND t.fkcontext = ts.fkcontext AND t.bvisible = 1)
        WHERE
          ts.fkcontext = NEW.fkcontext
          AND ts.nactivity = NEW.nactivity
        GROUP BY s.pkschedule
      ) p ON (p.schedule_id = s.pkschedule)
    WHERE
      ts.fkcontext = NEW.fkcontext
      AND ts.nactivity = NEW.nactivity
      AND p.pendant_tasks > 1
      OR (
        p.pendant_tasks = 1
        AND CURRENT_DATE > s.dDateLimit
      );
      
    IF NOT FOUND THEN
      UPDATE wkf_task_schedule
      SET bAlertSent = 0
      WHERE
        nactivity = NEW.nactivity
        AND fkcontext = NEW.fkcontext;
    END IF;
  END IF;
  RETURN NEW;
END$$;
CREATE FUNCTION update_control_is_active() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  mbHasRevision INTEGER;
  mbHasTest INTEGER;
  mdDateLimit TIMESTAMP;
  mdDateTodo TIMESTAMP;
  mdDateNext TIMESTAMP;
  mbImplementationIsLate INTEGER;
  mbEfficiencyNotOk INTEGER;
  mbRevisionIsLate INTEGER;
  mbTestNotOk INTEGER;
  mbTestIsLate INTEGER;
BEGIN
  
  mbImplementationIsLate = 0;
  mbEfficiencyNotOk = 0;
  mbRevisionIsLate = 0;
  mbTestNotOk = 0;
  mbTestIsLate = 0;
  
  IF NEW.dDateImplemented IS NULL AND NEW.dDateDeadline <= NOW() THEN
    mbImplementationIsLate = 1;
  END IF;

  SELECT COUNT(*)
  INTO mbHasRevision
  FROM
    isms_config cfg,
    wkf_control_efficiency ce
  WHERE
    cfg.pkConfig = 404
    AND cfg.sValue = '1'
    AND ce.fkControlEfficiency = NEW.fkContext;

  IF mbHasRevision = 1 THEN
    SELECT CASE WHEN COUNT(*) = 0 THEN 1 ELSE 0 END
    INTO mbEfficiencyNotOk
    FROM wkf_control_efficiency ce
    WHERE
      ce.fkControlEfficiency = NEW.fkContext
      AND (
        ce.nRealEfficiency >= ce.nExpectedEfficiency
        OR ce.nRealEfficiency = 0
      );
    
    SELECT COUNT(*)
    INTO mbRevisionIsLate
    FROM
      wkf_control_efficiency ce
      JOIN wkf_schedule s ON (s.pkschedule = ce.fkschedule)
      JOIN wkf_task_schedule ts ON (ts.fkschedule = s.pkschedule)
      JOIN wkf_task t ON (t.nactivity = ts.nactivity AND t.fkcontext = ts.fkcontext AND t.bvisible = 1)
      JOIN (
        SELECT
          s2.pkschedule AS schedule_id,
          COUNT(*) AS pendant_tasks
        FROM
          wkf_schedule s2
          JOIN wkf_task_schedule ts2 ON (ts2.fkschedule = s2.pkschedule)
          JOIN wkf_task t2 ON (t2.nactivity = ts2.nactivity AND t2.fkcontext = ts2.fkcontext AND t2.bvisible = 1)
        GROUP BY s2.pkschedule
      ) p ON (p.schedule_id = s.pkschedule)
    WHERE
      ce.fkControlEfficiency = NEW.fkContext
      AND (
        p.pendant_tasks > 1
        OR (
          p.pendant_tasks = 1
          AND CURRENT_DATE > s.dDateLimit
        )
      );
    IF mbRevisionIsLate > 1 THEN mbRevisionIsLate = 1; END IF;
    
  END IF;

  SELECT COUNT(*)
  INTO mbHasTest
  FROM
    isms_config cfg,
    wkf_control_test ct
  WHERE
    cfg.pkConfig = 405
    AND cfg.sValue = '1'
    AND ct.fkControlTest = NEW.fkContext;

  IF mbHasTest = 1 THEN
    SELECT CASE WHEN COUNT(*) = 1 THEN 1 ELSE 0 END
    INTO mbTestNotOk
    FROM
      wkf_control_test ct
      LEFT JOIN rm_control_test_history th ON (
        th.fkControl = ct.fkControlTest
        AND NOT EXISTS (
          SELECT *
          FROM rm_control_test_history th2
          WHERE
            th2.fkControl = th.fkControl
            AND th2.dDateAccomplishment > th.dDateAccomplishment
        )
      )
    WHERE
      ct.fkControlTest = NEW.fkContext
      AND th.bTestedValue = 0;
    
    SELECT COUNT(*)
    INTO mbTestIsLate
    FROM
      wkf_control_test ct
      JOIN wkf_schedule s ON (s.pkschedule = ct.fkschedule)
      JOIN wkf_task_schedule ts ON (ts.fkschedule = s.pkschedule)
      JOIN wkf_task t ON (t.nactivity = ts.nactivity AND t.fkcontext = ts.fkcontext AND t.bvisible = 1)
      JOIN (
        SELECT
          s2.pkschedule AS schedule_id,
          COUNT(*) AS pendant_tasks
        FROM
          wkf_schedule s2
          JOIN wkf_task_schedule ts2 ON (ts2.fkschedule = s2.pkschedule)
          JOIN wkf_task t2 ON (t2.nactivity = ts2.nactivity AND t2.fkcontext = ts2.fkcontext AND t2.bvisible = 1)
        GROUP BY s2.pkschedule
      ) p ON (p.schedule_id = s.pkschedule)
    WHERE
      ct.fkControlTest = NEW.fkContext
      AND (
        p.pendant_tasks > 1
        OR (
          p.pendant_tasks = 1
          AND CURRENT_DATE > s.dDateLimit
        )
      );
    IF mbTestIsLate > 1 THEN mbTestIsLate = 1; END IF;
    
  END IF;

  NEW.bImplementationIsLate = mbImplementationIsLate;
  NEW.bEfficiencyNotOk = mbEfficiencyNotOk;
  NEW.bRevisionIsLate = mbRevisionIsLate;
  NEW.bTestNotOk = mbTestNotOk;
  NEW.bTestIsLate = mbTestIsLate;

  IF mbImplementationIsLate + mbEfficiencyNotOk + mbRevisionIsLate + mbTestNotOk + mbTestIsLate > 0 THEN
    NEW.bIsActive = 0;
    
    IF OLD.bImplementationIsLate = 0 AND NEW.bImplementationIsLate = 1 THEN
      IF NOT EXISTS (SELECT * FROM ci_nc_seed WHERE fkControl = NEW.fkContext AND nDeactivationReason = 1) THEN
        INSERT INTO ci_nc_seed (fkControl,nDeactivationReason,dDateSent) VALUES (NEW.fkContext,1,NOW());
      END IF;
    END IF;
    
    IF OLD.bEfficiencyNotOk = 0 AND NEW.bEfficiencyNotOk = 1 THEN
      IF NOT EXISTS (SELECT * FROM ci_nc_seed WHERE fkControl = NEW.fkContext AND nDeactivationReason = 2) THEN
        INSERT INTO ci_nc_seed (fkControl,nDeactivationReason,dDateSent) VALUES (NEW.fkContext,2,NOW());
      END IF;
    END IF;
    
    IF OLD.bRevisionIsLate = 0 AND NEW.bRevisionIsLate = 1 THEN
      IF NOT EXISTS (SELECT * FROM ci_nc_seed WHERE fkControl = NEW.fkContext AND nDeactivationReason = 4) THEN
        INSERT INTO ci_nc_seed (fkControl,nDeactivationReason,dDateSent) VALUES (NEW.fkContext,4,NOW());
      END IF;
    END IF;
    
    IF OLD.bTestNotOk = 0 AND NEW.bTestNotOk = 1 THEN
      IF NOT EXISTS (SELECT * FROM ci_nc_seed WHERE fkControl = NEW.fkContext AND nDeactivationReason = 8) THEN
        INSERT INTO ci_nc_seed (fkControl,nDeactivationReason,dDateSent) VALUES (NEW.fkContext,8,NOW());
      END IF;
    END IF;
    
    IF OLD.bTestIsLate = 0 AND NEW.bTestIsLate = 1 THEN
      IF NOT EXISTS (SELECT * FROM ci_nc_seed WHERE fkControl = NEW.fkContext AND nDeactivationReason = 16) THEN
        INSERT INTO ci_nc_seed (fkControl,nDeactivationReason,dDateSent) VALUES (NEW.fkContext,16,NOW());
      END IF;
    END IF;
    
  ELSE
    NEW.bIsActive = 1;
  END IF;

  RETURN NEW;
END
$$;
SET default_tablespace = '';
SET default_with_oids = true;

CREATE VIEW view_ci_action_plan_active AS
    SELECT ac.fkcontext, ac.fkresponsible, ac.sname, ac.tactionplan, ac.nactiontype, ac.ddatedeadline, ac.ddateconclusion, ac.bisefficient, ac.ddateefficiencyrevision, ac.ddateefficiencymeasured, ac.ndaysbefore, ac.bflagrevisionalert, ac.bflagrevisionalertlate, ac.sdocument FROM (isms_context c JOIN ci_action_plan ac ON (((c.pkcontext = ac.fkcontext) AND (c.nstate <> 2705))));
CREATE VIEW view_ci_category_active AS
    SELECT cat.fkcontext, cat.sname FROM (isms_context c JOIN ci_category cat ON (((c.pkcontext = cat.fkcontext) AND (c.nstate <> 2705))));
CREATE VIEW view_ci_inc_control_active AS
    SELECT ic.fkcontext, ic.fkcontrol, ic.fkincident FROM ((ci_incident_control ic JOIN isms_context ctx_c ON (((ctx_c.pkcontext = ic.fkcontrol) AND (ctx_c.nstate <> 2705)))) JOIN isms_context ctx_i ON (((ctx_i.pkcontext = ic.fkincident) AND (ctx_i.nstate <> 2705))));
CREATE VIEW view_ci_inc_process_active AS
    SELECT ip.fkcontext, ip.fkprocess, ip.fkincident FROM ((ci_incident_process ip JOIN isms_context ctx_p ON (((ctx_p.pkcontext = ip.fkprocess) AND (ctx_p.nstate <> 2705)))) JOIN isms_context ctx_i ON (((ctx_i.pkcontext = ip.fkincident) AND (ctx_i.nstate <> 2705))));
CREATE VIEW view_ci_incident_active AS
    SELECT i.fkcontext, i.fkcategory, i.fkresponsible, i.sname, i.taccountsplan, i.tevidences, i.nlosstype, i.tevidencerequirementcomment, i.ddatelimit, i.ddatefinish, i.tdisposaldescription, i.tsolutiondescription, i.tproductservice, i.bnotemaildp, i.bemaildpsent, i.ddate FROM (isms_context c JOIN ci_incident i ON (((c.pkcontext = i.fkcontext) AND (c.nstate <> 2705))));
CREATE VIEW view_ci_incident_risk_active AS
    SELECT ir.fkcontext, ir.fkrisk, ir.fkincident FROM ((ci_incident_risk ir JOIN isms_context ctx_r ON (((ctx_r.pkcontext = ir.fkrisk) AND (ctx_r.nstate <> 2705)))) JOIN isms_context ctx_i ON (((ctx_i.pkcontext = ir.fkincident) AND (ctx_i.nstate <> 2705))));
CREATE VIEW view_ci_incident_user_active AS
    SELECT ci_incident_user.fkuser, ci_incident_user.fkincident, ci_incident_user.tdescription, ci_incident_user.tactiontaken FROM ci_incident_user WHERE ((NOT (ci_incident_user.fkuser IN (SELECT isms_context.pkcontext FROM isms_context WHERE ((isms_context.ntype = 2816) AND (isms_context.nstate = 2705))))) AND (NOT (ci_incident_user.fkincident IN (SELECT isms_context.pkcontext FROM isms_context WHERE ((isms_context.ntype = 2831) AND (isms_context.nstate = 2705))))));
CREATE VIEW view_ci_nc_active AS
    SELECT nc.fkcontext, nc.fkcontrol, nc.fkresponsible, nc.sname, nc.nseqnumber, nc.tdescription, nc.tcause, nc.tdenialjustification, nc.nclassification, nc.fksender, nc.ncapability, nc.ddatesent FROM (isms_context c JOIN ci_nc nc ON (((c.pkcontext = nc.fkcontext) AND (c.nstate <> 2705))));
CREATE VIEW view_ci_nc_process_active AS
    SELECT np.fkprocess, np.fknc FROM ((ci_nc_process np JOIN isms_context ctx_p ON (((ctx_p.pkcontext = np.fkprocess) AND (ctx_p.nstate <> 2705)))) JOIN isms_context ctx_n ON (((ctx_n.pkcontext = np.fknc) AND (ctx_n.nstate <> 2705))));
CREATE VIEW view_ci_occurrence_active AS
    SELECT o.fkcontext, o.fkincident, o.tdescription, o.tdenialjustification, o.ddate FROM (isms_context c JOIN ci_occurrence o ON (((c.pkcontext = o.fkcontext) AND (c.nstate <> 2705))));
CREATE VIEW view_ci_solution_active AS
    SELECT s.fkcontext, s.fkcategory, s.tproblem, s.tsolution, s.tkeywords FROM (isms_context c JOIN ci_solution s ON (((c.pkcontext = s.fkcontext) AND (c.nstate <> 2705))));
CREATE VIEW view_cm_area_resource_active AS
    SELECT p.fkcontext, p.fkarea, p.fkresource, p.fkgroup, p.cmt FROM (isms_context c JOIN cm_area_resource p ON (((c.pkcontext = p.fkcontext) AND (c.nstate <> 2705))));
CREATE VIEW view_cm_asset_category_active AS
    SELECT p.fkcontext, p.fkparent, p.sname, p.tdescription FROM (isms_context c JOIN cm_asset_category p ON (((c.pkcontext = p.fkcontext) AND (c.nstate <> 2705))));
CREATE VIEW view_cm_department_active AS
    SELECT si.fkcontext, si.name, si.description FROM (isms_context c JOIN cm_department si ON (((c.pkcontext = si.fkcontext) AND (c.nstate <> 2705))));
CREATE VIEW view_cm_group_active AS
    SELECT si.fkcontext, si.name, si.acronym, si.fkpriority FROM (isms_context c JOIN cm_group si ON (((c.pkcontext = si.fkcontext) AND (c.nstate <> 2705))));
CREATE VIEW view_cm_group_resource_active AS
    SELECT si.fkcontext, si.fkgroup, si.fkresource, si.escalation FROM (isms_context c JOIN cm_group_resource si ON (((c.pkcontext = si.fkcontext) AND (c.nstate <> 2705))));
CREATE VIEW view_cm_impact_active AS
    SELECT si.fkcontext, si.sname, si.ntype FROM (isms_context c JOIN cm_impact si ON (((c.pkcontext = si.fkcontext) AND (c.nstate <> 2705))));
CREATE VIEW view_cm_impact_damage_active AS
    SELECT c.pkcontext, c.ntype, c.nstate, si.fkcontext, si.fkimpact, si.fkscene FROM (isms_context c JOIN cm_impact_damage si ON (((c.pkcontext = si.fkcontext) AND (c.nstate <> 2705))));
CREATE VIEW view_cm_place_active AS
    SELECT p.fkcontext, p.sname, p.sdescription, p.fktype, p.zipcode, p.scordinate, p.saddress, p.fkgroup, p.fkgroupsubstitute, p.fkresource, p.fkresourcesubstitute, p.fkcmt, p.fkparent, p.file_path, p.file_name, p.city FROM (isms_context c JOIN cm_place p ON (((c.pkcontext = p.fkcontext) AND (c.nstate <> 2705))));
CREATE VIEW view_cm_place_asset_active AS
    SELECT p.fkcontext, p.fkplace, p.fkasset FROM (isms_context c JOIN cm_place_asset p ON (((c.pkcontext = p.fkcontext) AND (c.nstate <> 2705))));
CREATE VIEW view_cm_place_category_active AS
    SELECT p.fkcontext, p.fkparent, p.sname, p.tdescription FROM (isms_context c JOIN cm_place_category p ON (((c.pkcontext = p.fkcontext) AND (c.nstate <> 2705))));
CREATE VIEW view_cm_place_provider_active AS
    SELECT p.fkcontext, p.fkplace, p.fkprovider FROM (isms_context c JOIN cm_place_provider p ON (((c.pkcontext = p.fkcontext) AND (c.nstate <> 2705))));
CREATE VIEW view_cm_place_resource_active AS
    SELECT p.fkcontext, p.fkplace, p.fkresource, p.fkgroup, p.cmt FROM (isms_context c JOIN cm_place_resource p ON (((c.pkcontext = p.fkcontext) AND (c.nstate <> 2705))));
CREATE VIEW view_cm_plan_action_active AS
    SELECT si.fkcontext, si.fkgroup, si.fkplanrange, si.resume, si.description, si.estimate, si.fkplan, si.fkaction, si.estimateflag, si.fkplantype, si.fkotherplan FROM (isms_context c JOIN cm_plan_action si ON (((c.pkcontext = si.fkcontext) AND (c.nstate <> 2705))));
CREATE VIEW view_cm_plan_action_test_active AS
    SELECT si.fkcontext, si.fkplantest, si.fkplanaction, si.starttime, si.endtime, si.observation FROM (isms_context c JOIN cm_plan_action_test si ON (((c.pkcontext = si.fkcontext) AND (c.nstate <> 2705))));
CREATE VIEW view_cm_plan_active AS
    SELECT si.fkcontext, si.name, si.description, si.fkprocess, si.fkplace, si.fkasset, si.fkarea FROM (isms_context c JOIN cm_plan si ON (((c.pkcontext = si.fkcontext) AND (c.nstate <> 2705))));
CREATE VIEW view_cm_plan_range_active AS
    SELECT r.fkcontext, r.description, r.startrange, r.endrange, r.endrangeflag, r.startrangeflag, r.fktype, (r.startrange * CASE WHEN (r.startrangeflag = 2) THEN 60 ELSE CASE WHEN (r.startrangeflag = 3) THEN (60 * 24) ELSE CASE WHEN (r.startrangeflag = 4) THEN ((60 * 24) * 30) ELSE 1 END END END) AS rangeflag, CASE WHEN (r.description = ''::text) THEN ((((((((('{$from}'::text || r.startrange) || '%'::text) || r.startrangeflag) || '%'::text) || '{$to}'::text) || r.endrange) || '%'::text) || r.endrangeflag) || '%'::text) ELSE r.description END AS range FROM (isms_context c JOIN cm_plan_range r ON (((c.pkcontext = r.fkcontext) AND (c.nstate <> 2705))));
CREATE VIEW view_cm_plan_range_type_active AS
    SELECT si.fkcontext, si.description, si.detail FROM (isms_context c JOIN cm_plan_range_type si ON (((c.pkcontext = si.fkcontext) AND (c.nstate <> 2705))));
CREATE VIEW view_cm_plan_schedule AS
    SELECT si.fkcontext, si.fkplan, si.status, si.date FROM (isms_context c JOIN cm_plan_schedule si ON (((c.pkcontext = si.fkcontext) AND (c.nstate <> 2705))));
CREATE VIEW view_cm_plan_schedule_active AS
    SELECT si.fkcontext, si.fkplan, si.status, si.date, si.fktest, si.fkresponsible, si.fkgroup FROM (isms_context c JOIN cm_plan_schedule si ON (((c.pkcontext = si.fkcontext) AND (c.nstate <> 2705))));
CREATE VIEW view_cm_plan_test_active AS
    SELECT si.fkcontext, si.fkplan, si.observation, si.starttime, si.endtime, si.fkresponsible FROM (isms_context c JOIN cm_plan_test si ON (((c.pkcontext = si.fkcontext) AND (c.nstate <> 2705))));
CREATE VIEW view_cm_process_activity_active AS
    SELECT p.fkcontext, p.sname, p.sdescription, p.fkprocess, p.fkresponsible, p.fkpriority, p.fkgroup, p.fkimportance FROM (isms_context c JOIN cm_process_activity p ON (((c.pkcontext = p.fkcontext) AND (c.nstate <> 2705))));
CREATE VIEW view_cm_process_activity_asset_active AS
    SELECT p.fkcontext, p.fkactivity, p.fkasset, p.fkimportance FROM (isms_context c JOIN cm_process_activity_asset p ON (((c.pkcontext = p.fkcontext) AND (c.nstate <> 2705))));
CREATE VIEW view_cm_process_category_active AS
    SELECT p.fkcontext, p.fkparent, p.sname, p.tdescription FROM (isms_context c JOIN cm_process_category p ON (((c.pkcontext = p.fkcontext) AND (c.nstate <> 2705))));
CREATE VIEW view_cm_provider_active AS
    SELECT si.fkcontext, si.name, si.description, si.contract, si.contracttype, si.sla FROM (isms_context c JOIN cm_provider si ON (((c.pkcontext = si.fkcontext) AND (c.nstate <> 2705))));
CREATE VIEW view_cm_provider_resource_active AS
    SELECT si.fkcontext, si.fkprovider, si.fkresource, si.bisdefault FROM (isms_context c JOIN cm_provider_resource si ON (((c.pkcontext = si.fkcontext) AND (c.nstate <> 2705))));
CREATE VIEW view_cm_resource_active AS
    SELECT si.fkcontext, si.name, si.func, si.provider, si.fkdepartment, si.comercialnumber, si.celnumber, si.homenumber, si.nextelnumber, si.email FROM (isms_context c JOIN cm_resource si ON (((c.pkcontext = si.fkcontext) AND (c.nstate <> 2705))));
CREATE VIEW view_cm_scene_active AS
    SELECT s.fkcontext, s.sname, s.sdescription, s.nrto, s.nrpo, s.nmtpd, s.dstartdate, s.denddate, s.nflags, s.fkprocess, s.nrto_flags, s.nrpo_flags, s.nmtpd_flags, s.nhourstart, s.nhourend, s.consequence, s.fkprobability FROM (isms_context c JOIN cm_scene s ON (((c.pkcontext = s.fkcontext) AND (c.nstate <> 2705))));
CREATE VIEW view_cm_scope_active AS
    SELECT c.pkcontext, c.ntype, c.nstate, s.fkcontext, s.sname, s.sdescription FROM (isms_context c JOIN cm_scope s ON (((c.pkcontext = s.fkcontext) AND (c.nstate <> 2705))));
CREATE VIEW view_rm_area_active AS
    SELECT a.fkcontext, a.fkpriority, a.fktype, a.fkparent, a.sname, a.tdescription, a.sdocument, a.nvalue, a.fkplace, a.fkgroup, a.fkgroupsubstitute, a.fkresource, a.fkresourcesubstitute FROM (isms_context c JOIN rm_area a ON (((c.pkcontext = a.fkcontext) AND (c.nstate <> 2705))));
CREATE VIEW view_cm_scope_area_active AS
    SELECT sp.fkcontext, sp.fkscope, sp.fkarea FROM ((cm_scope_area sp JOIN view_rm_area_active p ON ((p.fkcontext = sp.fkarea))) JOIN view_cm_scope_active s ON ((s.fkcontext = sp.fkscope)));
CREATE VIEW view_rm_asset_active AS
    SELECT p.fkcontext, p.sname, p.tdescription, p.sdocument, p.ncost, p.nvalue, p.blegality, p.tjustification, p.ntac, p.ntrc, p.nrecoverytime, p.ncontingency, p.nconformity, p.fkcategory, p.contingency, p.traflag, p.tacflag, p.fkgroup, p.fksecurityresponsible, p.alias, p.model, p.manufacturer, p.nversion, p.build, p.serial, p.ip, p.mac, p.status, p.fkplace, p.file_path, p.file_name, p.fkprovider, (p.nrecoverytime * CASE WHEN (p.traflag = 2) THEN 60 ELSE CASE WHEN (p.traflag = 3) THEN (60 * 24) ELSE CASE WHEN (p.traflag = 4) THEN ((60 * 24) * 30) ELSE 1 END END END) AS tra, (p.ntac * CASE WHEN (p.tacflag = 2) THEN 60 ELSE CASE WHEN (p.tacflag = 3) THEN (60 * 24) ELSE CASE WHEN (p.tacflag = 4) THEN ((60 * 24) * 30) ELSE 1 END END END) AS tac FROM (isms_context c JOIN rm_asset p ON (((c.pkcontext = p.fkcontext) AND (c.nstate <> 2705))));
CREATE VIEW view_cm_scope_asset_active AS
    SELECT sp.fkcontext, sp.fkscope, sp.fkasset FROM ((cm_scope_asset sp JOIN view_rm_asset_active p ON ((p.fkcontext = sp.fkasset))) JOIN view_cm_scope_active s ON ((s.fkcontext = sp.fkscope)));
CREATE VIEW view_cm_scope_place_active AS
    SELECT sp.fkcontext, sp.fkscope, sp.fkplace FROM ((cm_scope_place sp JOIN view_cm_place_active p ON ((p.fkcontext = sp.fkplace))) JOIN view_cm_scope_active s ON ((s.fkcontext = sp.fkscope)));
CREATE VIEW view_rm_process_active AS
    SELECT p.fkcontext, p.fkarea, p.sname, p.tdescription, p.sdocument, p.nvalue, p.tinput, p.toutput, p.fkgroup, p.fkgroupsubstitute, p.fkresource, p.fkresourcesubstitute, p.fktype, p.fkpriority FROM (isms_context c JOIN rm_process p ON (((c.pkcontext = p.fkcontext) AND (c.nstate <> 2705))));
CREATE VIEW view_cm_scope_process_active AS
    SELECT sp.fkcontext, sp.fkscope, sp.fkprocess FROM ((cm_scope_process sp JOIN view_rm_process_active p ON ((p.fkcontext = sp.fkprocess))) JOIN view_cm_scope_active s ON ((s.fkcontext = sp.fkscope)));
CREATE VIEW view_cm_threat_active AS
    SELECT t.fkcontext, t.sname, t.sdescription, t.nprobability, t.ntype, t.nclass FROM (isms_context c JOIN cm_threat t ON (((c.pkcontext = t.fkcontext) AND (c.nstate <> 2705))));
CREATE VIEW view_cm_threat_vulnerability_active AS
    SELECT t.fkcontext, t.fkplace, t.fkthreat, t.fklevel, t.description, t.controls FROM (isms_context c JOIN cm_threat_vulnerability t ON (((c.pkcontext = t.fkcontext) AND (c.nstate <> 2705))));
CREATE VIEW view_dashboard_current_tests AS
    SELECT a.planname, a.date, a.status, a.responsible FROM (SELECT plan.name AS planname, schedule.date, schedule.status, COALESCE(resource.name, g.name) AS responsible FROM (((view_cm_plan_schedule_active schedule JOIN view_cm_plan_active plan ON ((plan.fkcontext = schedule.fkplan))) LEFT JOIN view_cm_resource_active resource ON ((resource.fkcontext = schedule.fkresponsible))) LEFT JOIN view_cm_group_active g ON ((g.fkcontext = schedule.fkgroup))) WHERE (schedule.date > now()) LIMIT 5) a UNION SELECT b.planname, b.date, b.status, b.responsible FROM (SELECT plan.name AS planname, schedule.date, schedule.status, COALESCE(resource.name, g.name) AS responsible FROM (((view_cm_plan_schedule_active schedule JOIN view_cm_plan_active plan ON ((plan.fkcontext = schedule.fkplan))) LEFT JOIN view_cm_resource_active resource ON ((resource.fkcontext = schedule.fkresponsible))) LEFT JOIN view_cm_group_active g ON ((g.fkcontext = schedule.fkgroup))) WHERE (schedule.date <= now())) b ORDER BY 2 DESC;
CREATE VIEW view_dashboard_scope AS
    SELECT scope.sname AS name, count(DISTINCT process.fkprocess) AS processes, count(DISTINCT places.fkplace) AS places, count(DISTINCT assets.fkasset) AS assets, count(DISTINCT areas.fkarea) AS areas FROM ((((view_cm_scope_active scope LEFT JOIN cm_scope_process process ON ((process.fkscope = scope.fkcontext))) LEFT JOIN cm_scope_place places ON ((places.fkscope = scope.fkcontext))) LEFT JOIN cm_scope_asset assets ON ((assets.fkscope = scope.fkcontext))) LEFT JOIN cm_scope_area areas ON ((areas.fkscope = scope.fkcontext))) GROUP BY scope.sname ORDER BY scope.sname DESC;
CREATE VIEW view_financial_impact_sum AS
    SELECT id.fkscene AS scene, sum(idr.financialimpact) AS financialimpact, dm.fkcontext AS period FROM cm_impact_damage id JOIN cm_impact_damage_range idr ON (idr.fkimpactdamage = id.fkcontext) JOIN cm_damage_matrix dm ON (dm.fkcontext = idr.fkdamage) GROUP BY id.fkscene, dm.fkcontext;

CREATE VIEW view_impact_ranking AS
    SELECT sid.fkcontext, sid.fkscene, round((avg(dmp.nweight) * ((it.nweight)::numeric / (10)::numeric)), 4) AS weight, max(sidr.financialimpact) AS financialloss FROM ((((cm_damage_matrix_parameter dmp JOIN cm_impact_damage_range sidr ON ((dmp.fkcontext = sidr.fkdamageparameter))) JOIN cm_impact_damage sid ON ((sidr.fkimpactdamage = sid.fkcontext))) JOIN view_cm_impact_active si ON ((si.fkcontext = sid.fkimpact))) JOIN cm_impact_type it ON ((si.ntype = it.fkcontext))) GROUP BY sid.fkcontext, sid.fkscene, it.nweight;

CREATE VIEW view_scene_ranking AS
    SELECT s.scene AS fkscene, round(sum(s.rankingavg), 4) AS ranking, sum(s.financialloss) AS financialloss, cs.nrto AS rto FROM ((SELECT i.fkscene AS scene, avg(v.weight) AS rankingavg, sum(v.financialloss) AS financialloss FROM ((cm_impact_damage i JOIN view_impact_ranking v ON ((v.fkcontext = i.fkcontext))) JOIN cm_impact c ON ((c.fkcontext = i.fkimpact))) GROUP BY c.ntype, i.fkscene) s JOIN cm_scene cs ON ((s.scene = cs.fkcontext))) GROUP BY s.scene, cs.nrto;
CREATE VIEW view_process_ranking AS
    SELECT s.fkprocess, sr.ranking, sr.rto, sr.financialloss, s.fkcontext AS worstscene, (s.nrto * CASE WHEN (s.nrto_flags = 2) THEN 60 ELSE CASE WHEN (s.nrto_flags = 3) THEN (60 * 24) ELSE 1 END END) AS rtominutes, s.nrto_flags AS rtoflag FROM ((cm_scene s JOIN view_scene_ranking sr ON ((sr.fkscene = s.fkcontext))) JOIN (SELECT s.fkprocess, max(sceneranking.ranking) AS ranking FROM (cm_scene s JOIN view_scene_ranking sceneranking ON ((sceneranking.fkscene = s.fkcontext))) GROUP BY s.fkprocess) sub ON (((sr.ranking = sub.ranking) AND (s.fkprocess = sub.fkprocess))));
CREATE VIEW view_rm_process_asset_active AS
    SELECT rm_process_asset.fkcontext, rm_process_asset.fkprocess, rm_process_asset.fkasset, rm_process_asset.fkimportance FROM rm_process_asset WHERE ((NOT (rm_process_asset.fkprocess IN (SELECT isms_context.pkcontext FROM isms_context WHERE ((isms_context.ntype = 2802) AND (isms_context.nstate = 2705))))) AND (NOT (rm_process_asset.fkasset IN (SELECT isms_context.pkcontext FROM isms_context WHERE ((isms_context.ntype = 2803) AND (isms_context.nstate = 2705))))));
CREATE VIEW view_isa AS
    SELECT pa.fkprocess, a.fkcontext AS fkasset, ((p.rtominutes)::double precision / (COALESCE(NULLIF(COALESCE(a.tra, a.tac), 0), 1))::double precision) AS isa, COALESCE(priority.weight, 1) AS importancetoprocess FROM (((view_rm_process_asset_active pa LEFT JOIN cm_priority priority ON ((priority.fkcontext = pa.fkimportance))) JOIN view_rm_asset_active a ON ((a.fkcontext = pa.fkasset))) JOIN view_process_ranking p ON ((p.fkprocess = pa.fkprocess)));
CREATE VIEW view_isa_activity AS
    SELECT pa.fkprocess, a.fkcontext AS fkasset, ((p.rtominutes)::double precision / (COALESCE(NULLIF(COALESCE(a.tra, a.tac), 0), 1))::double precision) AS isa, COALESCE(priority.weight, 1) AS importancetoprocess FROM ((((view_cm_process_activity_asset_active paa JOIN view_cm_process_activity_active pa ON ((pa.fkcontext = paa.fkactivity))) LEFT JOIN cm_priority priority ON ((priority.fkcontext = paa.fkimportance))) JOIN view_rm_asset_active a ON ((a.fkcontext = paa.fkasset))) JOIN view_process_ranking p ON ((p.fkprocess = pa.fkprocess)));
CREATE VIEW view_isms_context_active AS
    SELECT c.pkcontext, c.ntype, c.nstate FROM isms_context c WHERE (c.nstate <> 2705);
CREATE VIEW view_isms_context_hist_active AS
    SELECT h.pkid, h.fkcontext, h.stype, h.ddate, h.nvalue FROM (isms_context c RIGHT JOIN isms_context_history h ON (((c.pkcontext = h.fkcontext) AND (c.nstate <> 2705))));
CREATE VIEW view_isms_policy_active AS
    SELECT p.fkcontext, p.tdescription FROM (isms_context c JOIN isms_policy p ON (((c.pkcontext = p.fkcontext) AND (c.nstate <> 2705))));
CREATE VIEW view_isms_profile_active AS
    SELECT p.fkcontext, p.sname, p.nid FROM (isms_context c JOIN isms_profile p ON (((c.pkcontext = p.fkcontext) AND (c.nstate <> 2705))));
CREATE VIEW view_isms_scope_active AS
    SELECT s.fkcontext, s.tdescription FROM (isms_context c JOIN isms_scope s ON (((c.pkcontext = s.fkcontext) AND (c.nstate <> 2705))));
CREATE VIEW view_isms_user_active AS
    SELECT u.fkcontext, u.fkprofile, u.sname, u.slogin, u.spassword, u.semail, u.nip, u.nlanguage, u.dlastlogin, u.nwronglogonattempts, u.bisblocked, u.bmustchangepassword, u.srequestpassword, u.ssession, u.bansweredsurvey, u.bshowhelp FROM (isms_context c JOIN isms_user u ON (((c.pkcontext = u.fkcontext) AND (c.nstate <> 2705))));
CREATE VIEW view_place_threats AS
    (SELECT t.fkcontext, t.sname, t.sdescription, t.nprobability, t.ntype, t.nclass, pt.fkplace, 'place' AS origin FROM (view_cm_threat_active t JOIN cm_place_threat pt ON ((t.fkcontext = pt.fkthreat))) UNION ALL SELECT t.fkcontext, t.sname, t.sdescription, t.nprobability, t.ntype, t.nclass, p.fkcontext AS fkplace, 'category' AS origin FROM ((view_cm_place_active p JOIN (SELECT get_place_category_parents_threats.fkplacecategory, get_place_category_parents_threats.fkthreat, get_place_category_parents_threats.fkparent, get_place_category_parents_threats.fkplace FROM get_place_category_parents_threats() get_place_category_parents_threats(fkplacecategory, fkthreat, fkparent, fkplace)) pcpt ON ((p.fkcontext = pcpt.fkplace))) JOIN view_cm_threat_active t ON ((t.fkcontext = pcpt.fkthreat)))) UNION ALL SELECT t.fkcontext, t.sname, t.sdescription, t.nprobability, t.ntype, t.nclass, parents.fkplace, 'inheritance' AS origin FROM ((view_cm_threat_active t JOIN cm_place_threat pt ON ((pt.fkthreat = t.fkcontext))) JOIN (SELECT get_place_parents.fkplace, get_place_parents.fkparent FROM get_place_parents() get_place_parents(fkplace, fkparent)) parents ON ((parents.fkparent = pt.fkplace)));
CREATE VIEW view_plan_offline AS
    SELECT pa.resume, g.acronym, pa.description, ((pa.estimate || ' '::text) || CASE WHEN (pa.estimateflag = 1) THEN 'minutos'::text ELSE CASE WHEN (pa.estimateflag = 2) THEN 'Horas'::text ELSE CASE WHEN (pa.estimateflag = 3) THEN 'Dias'::text ELSE 'Meses'::text END END END) AS estimate, p.name, p.description AS plandescription, pr.rangeflag, pr.range, p.fkcontext, pa.fkcontext AS planactionid, pa.fkaction AS depends FROM (((cm_plan_action pa JOIN cm_plan p ON ((p.fkcontext = pa.fkplan))) JOIN cm_group g ON ((g.fkcontext = pa.fkgroup))) JOIN view_cm_plan_range_active pr ON ((pr.fkcontext = pa.fkplanrange))) ORDER BY p.fkcontext, pr.rangeflag, ((pa.estimate || ' '::text) || CASE WHEN (pa.estimateflag = 1) THEN 'minutos'::text ELSE CASE WHEN (pa.estimateflag = 2) THEN 'Horas'::text ELSE CASE WHEN (pa.estimateflag = 3) THEN 'Dias'::text ELSE 'Meses'::text END END END), g.acronym;
CREATE VIEW view_pm_di_active_with_content AS
    SELECT i.fkcontext, i.fkdocument, i.nmajorversion, i.nrevisionversion, i.trevisionjustification, i.spath, i.tmodifycomment, i.dbeginproduction, i.dendproduction, i.sfilename, i.bislink, i.slink, ic.tcontent, i.dbeginapprover, i.dbeginrevision FROM (((isms_context c JOIN pm_doc_instance i ON (((c.pkcontext = i.fkcontext) AND (c.nstate <> 2705)))) JOIN isms_context ctx_doc ON (((ctx_doc.pkcontext = i.fkdocument) AND (ctx_doc.nstate <> 2705)))) LEFT JOIN pm_instance_content ic ON ((i.fkcontext = ic.fkinstance)));
CREATE VIEW view_pm_di_with_content AS
    SELECT i.fkcontext, i.fkdocument, i.nmajorversion, i.nrevisionversion, i.trevisionjustification, i.spath, i.tmodifycomment, i.dbeginproduction, i.dendproduction, i.sfilename, i.bislink, i.slink, ic.tcontent FROM ((isms_context c JOIN pm_doc_instance i ON ((c.pkcontext = i.fkcontext))) LEFT JOIN pm_instance_content ic ON ((i.fkcontext = ic.fkinstance)));
CREATE VIEW view_pm_doc_approvers_active AS
    SELECT pm_doc_approvers.fkdocument, pm_doc_approvers.fkuser, pm_doc_approvers.bhasapproved FROM pm_doc_approvers WHERE ((NOT (pm_doc_approvers.fkdocument IN (SELECT isms_context.pkcontext FROM isms_context WHERE ((isms_context.ntype = 2823) AND (isms_context.nstate = 2705))))) AND (NOT (pm_doc_approvers.fkuser IN (SELECT isms_context.pkcontext FROM isms_context WHERE ((isms_context.ntype = 2816) AND (isms_context.nstate = 2705))))));
CREATE VIEW view_pm_doc_context_active AS
    SELECT pm_doc_context.fkcontext, pm_doc_context.fkdocument FROM pm_doc_context WHERE ((NOT (pm_doc_context.fkcontext IN (SELECT isms_context.pkcontext FROM isms_context WHERE (isms_context.nstate = 2705)))) AND (NOT (pm_doc_context.fkdocument IN (SELECT isms_context.pkcontext FROM isms_context WHERE ((isms_context.ntype = 2823) AND (isms_context.nstate = 2705))))));
CREATE VIEW view_pm_doc_instance_active AS
    SELECT i.fkcontext, i.fkdocument, i.nmajorversion, i.nrevisionversion, i.trevisionjustification, i.spath, i.tmodifycomment, i.dbeginproduction, i.dendproduction, i.sfilename, i.bislink, i.slink, i.dbeginapprover, i.dbeginrevision, i.smanualversion FROM ((isms_context c JOIN pm_doc_instance i ON (((c.pkcontext = i.fkcontext) AND (c.nstate <> 2705)))) JOIN isms_context ctx_doc ON (((ctx_doc.pkcontext = i.fkdocument) AND (ctx_doc.nstate <> 2705))));
CREATE VIEW view_pm_doc_instance_status AS
    SELECT d.fkcontext AS document_id, di.fkcontext AS doc_instance_id, CASE WHEN ((d.fkcurrentversion IS NULL) OR ((c.nstate = 2751) AND (di.nmajorversion = 0))) THEN 2751 WHEN ((c.nstate = 2753) AND (di.dbeginproduction IS NULL)) THEN 2753 WHEN ((d.fkcurrentversion = di.fkcontext) AND (((c.nstate = 2752) OR ((c.nstate = 2751) AND (di.nmajorversion > 0))) OR ((c.nstate = 2753) AND (di.dendproduction IS NOT NULL)))) THEN CASE WHEN (d.ddateproduction < now()) THEN 2752 ELSE 2756 END WHEN (((di.fkdocument = d.fkcontext) AND (di.fkcontext <> d.fkcurrentversion)) AND (di.dendproduction IS NOT NULL)) THEN 2755 ELSE 2754 END AS doc_instance_status FROM ((pm_document d JOIN isms_context c ON ((c.pkcontext = d.fkcontext))) LEFT JOIN pm_doc_instance di ON ((di.fkdocument = d.fkcontext)));
CREATE VIEW view_pm_doc_readers_active AS
    SELECT pm_doc_readers.fkuser, pm_doc_readers.fkdocument, pm_doc_readers.bmanual, pm_doc_readers.bdenied, pm_doc_readers.bhasread FROM pm_doc_readers WHERE ((NOT (pm_doc_readers.fkdocument IN (SELECT isms_context.pkcontext FROM isms_context WHERE ((isms_context.ntype = 2823) AND (isms_context.nstate = 2705))))) AND (NOT (pm_doc_readers.fkuser IN (SELECT isms_context.pkcontext FROM isms_context WHERE ((isms_context.ntype = 2816) AND (isms_context.nstate = 2705))))));
CREATE VIEW view_pm_doc_registers_active AS
    SELECT pm_doc_registers.fkregister, pm_doc_registers.fkdocument FROM pm_doc_registers WHERE ((NOT (pm_doc_registers.fkdocument IN (SELECT isms_context.pkcontext FROM isms_context WHERE ((isms_context.ntype = 2823) AND (isms_context.nstate = 2705))))) AND (NOT (pm_doc_registers.fkregister IN (SELECT isms_context.pkcontext FROM isms_context WHERE ((isms_context.ntype = 2826) AND (isms_context.nstate = 2705))))));
CREATE VIEW view_pm_document_active AS
    SELECT d.fkcontext, d.fkclassification, d.fkcurrentversion, d.fkparent, d.fkauthor, d.fkmainapprover, d.sname, d.tdescription, d.ntype, d.skeywords, d.ddateproduction, d.ddeadline, d.bflagdeadlinealert, d.bflagdeadlineexpired, d.ndaysbefore, d.bhasapproved, d.fkschedule FROM (isms_context c JOIN pm_document d ON (((c.pkcontext = d.fkcontext) AND (c.nstate <> 2705))));
CREATE VIEW view_pm_process_user_active AS
    SELECT pm_process_user.fkprocess, pm_process_user.fkuser FROM pm_process_user WHERE ((NOT (pm_process_user.fkprocess IN (SELECT isms_context.pkcontext FROM isms_context WHERE ((isms_context.ntype = 2802) AND (isms_context.nstate = 2705))))) AND (NOT (pm_process_user.fkuser IN (SELECT isms_context.pkcontext FROM isms_context WHERE ((isms_context.ntype = 2816) AND (isms_context.nstate = 2705))))));
CREATE VIEW view_pm_published_docs AS
    SELECT d.fkcontext, d.fkclassification, d.fkcurrentversion, d.fkparent, d.fkauthor, d.fkmainapprover, d.sname, d.tdescription, d.ntype, d.skeywords, d.ddateproduction, d.ddeadline, d.bflagdeadlinealert, d.bflagdeadlineexpired, d.ndaysbefore, d.bhasapproved, d.fkschedule FROM ((pm_document d JOIN isms_context c ON ((c.pkcontext = d.fkcontext))) JOIN pm_doc_instance di ON ((di.fkcontext = d.fkcurrentversion))) WHERE ((((c.nstate = 2752) AND (d.ddateproduction < now())) OR ((c.nstate = 2751) AND (di.nmajorversion > 0))) OR ((c.nstate = 2753) AND (di.dendproduction IS NOT NULL)));
CREATE VIEW view_pm_read_docs AS
    SELECT d.fkcontext, d.fkclassification, d.fkcurrentversion, d.fkparent, d.fkauthor, d.fkmainapprover, d.sname, d.tdescription, d.ntype, d.skeywords, d.ddateproduction, d.ddeadline, d.bflagdeadlinealert, d.bflagdeadlineexpired, d.ndaysbefore, d.bhasapproved, d.fkschedule FROM (view_pm_document_active d JOIN pm_doc_read_history rh ON ((rh.fkinstance = d.fkcurrentversion)));
CREATE VIEW view_pm_register_active AS
    SELECT r.fkcontext, r.fkresponsible, r.fkclassification, r.fkdocument, r.sname, r.nperiod, r.nvalue, r.sstorageplace, r.sstoragetype, r.sindexingtype, r.sdisposition, r.sprotectionrequirements, r.sorigin FROM (isms_context c JOIN pm_register r ON (((c.pkcontext = r.fkcontext) AND (c.nstate <> 2705))));
CREATE VIEW view_pm_template_active AS
    SELECT t.fkcontext, t.sname, t.ncontexttype, t.spath, t.sfilename, t.tdescription, t.skeywords, t.nfilesize FROM (isms_context c JOIN pm_template t ON (((c.pkcontext = t.fkcontext) AND (c.nstate <> 2705))));
CREATE VIEW view_pm_template_bp_active AS
    SELECT tbp.fktemplate, tbp.fkbestpractice FROM ((pm_template_best_practice tbp JOIN isms_context cbp ON (((cbp.pkcontext = tbp.fkbestpractice) AND (cbp.nstate <> 2705)))) JOIN isms_context ct ON (((ct.pkcontext = tbp.fktemplate) AND (ct.nstate <> 2705))));
CREATE VIEW view_pm_tp_active_with_content AS
    SELECT t.fkcontext, t.sname, t.ncontexttype, t.spath, t.sfilename, t.tdescription, t.skeywords, tc.tcontent, t.nfilesize FROM ((isms_context c JOIN pm_template t ON (((c.pkcontext = t.fkcontext) AND (c.nstate <> 2705)))) LEFT JOIN pm_template_content tc ON ((t.fkcontext = tc.fkcontext)));
CREATE VIEW view_pm_user_roles AS
    SELECT u.fkcontext AS user_id, d.fkcontext AS document_id, CASE WHEN (u.fkcontext = d.fkmainapprover) THEN 1 ELSE 0 END AS user_is_main_approver, CASE WHEN (u.fkcontext = d.fkauthor) THEN 1 ELSE 0 END AS user_is_author, CASE WHEN (u.fkcontext IN (SELECT da.fkuser FROM pm_doc_approvers da WHERE (da.fkdocument = d.fkcontext))) THEN 1 ELSE 0 END AS user_is_approver, CASE WHEN (u.fkcontext IN (SELECT dr.fkuser FROM pm_doc_readers dr WHERE (dr.fkdocument = d.fkcontext))) THEN 1 ELSE 0 END AS user_is_reader FROM view_isms_user_active u, view_pm_document_active d;
CREATE VIEW view_rm_asset_asset_active AS
    SELECT rm_asset_asset.fkasset, rm_asset_asset.fkdependent FROM rm_asset_asset WHERE ((NOT (rm_asset_asset.fkasset IN (SELECT isms_context.pkcontext FROM isms_context WHERE ((isms_context.ntype = 2803) AND (isms_context.nstate = 2705))))) AND (NOT (rm_asset_asset.fkdependent IN (SELECT isms_context.pkcontext FROM isms_context WHERE ((isms_context.ntype = 2803) AND (isms_context.nstate = 2705))))));
CREATE VIEW view_rm_best_practice_active AS
    SELECT bp.fkcontext, bp.fksectionbestpractice, bp.sname, bp.tdescription, bp.ncontroltype, bp.sclassification, bp.timplementationguide, bp.tmetric, bp.sdocument, bp.tjustification FROM (isms_context c JOIN rm_best_practice bp ON (((c.pkcontext = bp.fkcontext) AND (c.nstate <> 2705))));
CREATE VIEW view_rm_bp_event_active AS
    SELECT rm_best_practice_event.fkcontext, rm_best_practice_event.fkbestpractice, rm_best_practice_event.fkevent FROM rm_best_practice_event WHERE ((NOT (rm_best_practice_event.fkbestpractice IN (SELECT isms_context.pkcontext FROM isms_context WHERE ((isms_context.ntype = 2808) AND (isms_context.nstate = 2705))))) AND (NOT (rm_best_practice_event.fkevent IN (SELECT isms_context.pkcontext FROM isms_context WHERE ((isms_context.ntype = 2807) AND (isms_context.nstate = 2705))))));
CREATE VIEW view_rm_bp_standard_active AS
    SELECT rm_best_practice_standard.fkcontext, rm_best_practice_standard.fkstandard, rm_best_practice_standard.fkbestpractice FROM rm_best_practice_standard WHERE ((NOT (rm_best_practice_standard.fkstandard IN (SELECT isms_context.pkcontext FROM isms_context WHERE ((isms_context.ntype = 2809) AND (isms_context.nstate = 2705))))) AND (NOT (rm_best_practice_standard.fkbestpractice IN (SELECT isms_context.pkcontext FROM isms_context WHERE ((isms_context.ntype = 2808) AND (isms_context.nstate = 2705))))));
CREATE VIEW view_rm_category_active AS
    SELECT cat.fkcontext, cat.fkparent, cat.sname, cat.tdescription FROM (isms_context c JOIN rm_category cat ON (((c.pkcontext = cat.fkcontext) AND (c.nstate <> 2705))));
CREATE VIEW view_rm_control_active AS
    SELECT ctr.fkcontext, ctr.fktype, ctr.fkresponsible, ctr.sname, ctr.tdescription, ctr.sdocument, ctr.sevidence, ctr.ndaysbefore, ctr.bisactive, ctr.bflagimplalert, ctr.bflagimplexpired, ctr.ddatedeadline, ctr.ddateimplemented, ctr.nimplementationstate FROM (isms_context c JOIN rm_control ctr ON (((c.pkcontext = ctr.fkcontext) AND (c.nstate <> 2705))));
CREATE VIEW view_rm_control_bp_active AS
    SELECT rm_control_best_practice.fkcontext, rm_control_best_practice.fkbestpractice, rm_control_best_practice.fkcontrol FROM rm_control_best_practice WHERE ((NOT (rm_control_best_practice.fkbestpractice IN (SELECT isms_context.pkcontext FROM isms_context WHERE ((isms_context.ntype = 2808) AND (isms_context.nstate = 2705))))) AND (NOT (rm_control_best_practice.fkcontrol IN (SELECT isms_context.pkcontext FROM isms_context WHERE ((isms_context.ntype = 2805) AND (isms_context.nstate = 2705))))));
CREATE VIEW view_rm_event_active AS
    SELECT e.fkcontext, e.fktype, e.fkcategory, e.sdescription, e.tobservation, e.bpropagate, e.timpact FROM (isms_context c JOIN rm_event e ON (((c.pkcontext = e.fkcontext) AND (c.nstate <> 2705))));
CREATE VIEW view_rm_place_active AS
    SELECT p.fkcontext, p.sname, p.sdescription, p.fktype, p.zipcode, p.scordinate, p.saddress, p.fkgroup, p.fkgroupsubstitute, p.fkresource, p.fkresourcesubstitute, p.fkcmt, p.fkparent FROM (isms_context c JOIN cm_place p ON (((c.pkcontext = p.fkcontext) AND (c.nstate <> 2705))));
CREATE VIEW view_rm_risk_active AS
    SELECT r.fkcontext, r.fkprobabilityvaluename, r.fktype, r.fkevent, r.fkasset, r.sname, r.tdescription, r.nvalue, r.nvalueresidual, r.tjustification, r.nacceptmode, r.nacceptstate, r.sacceptjustification, r.ncost, r.timpact FROM (isms_context c JOIN rm_risk r ON (((c.pkcontext = r.fkcontext) AND (c.nstate <> 2705))));
CREATE VIEW view_rm_risk_control_active AS
    SELECT rm_risk_control.fkcontext, rm_risk_control.fkprobabilityvaluename, rm_risk_control.fkrisk, rm_risk_control.fkcontrol, rm_risk_control.tjustification FROM rm_risk_control WHERE ((NOT (rm_risk_control.fkrisk IN (SELECT isms_context.pkcontext FROM isms_context WHERE ((isms_context.ntype = 2804) AND (isms_context.nstate = 2705))))) AND (NOT (rm_risk_control.fkcontrol IN (SELECT isms_context.pkcontext FROM isms_context WHERE ((isms_context.ntype = 2805) AND (isms_context.nstate = 2705))))));
CREATE VIEW view_rm_sec_bp_active AS
    SELECT s.fkcontext, s.fkparent, s.sname FROM (isms_context c JOIN rm_section_best_practice s ON (((c.pkcontext = s.fkcontext) AND (c.nstate <> 2705))));
CREATE VIEW view_rm_standard_active AS
    SELECT s.fkcontext, s.sname, s.tdescription, s.tapplication, s.tobjective FROM (isms_context c JOIN rm_standard s ON (((c.pkcontext = s.fkcontext) AND (c.nstate <> 2705))));
CREATE VIEW view_top10_asa AS
    SELECT p.sname AS process, a.sname AS asset, round(((isa.isa / (isa.importancetoprocess)::double precision))::numeric, 2) AS asa, count(plan.fkcontext) AS plans FROM ((((view_rm_process_active p JOIN view_process_ranking pr ON ((p.fkcontext = pr.fkprocess))) JOIN view_isa isa ON ((p.fkcontext = isa.fkprocess))) JOIN view_rm_asset_active a ON ((a.fkcontext = isa.fkasset))) LEFT JOIN view_cm_plan_active plan ON ((plan.fkprocess = p.fkcontext))) GROUP BY p.sname, a.sname, round(((isa.isa / (isa.importancetoprocess)::double precision))::numeric, 2), pr.ranking ORDER BY pr.ranking DESC, round(((isa.isa / (isa.importancetoprocess)::double precision))::numeric, 2), a.sname, p.sname LIMIT 10;
CREATE VIEW view_top10_asa_activity AS
    SELECT p.sname AS process, a.sname AS asset, round(((isa.isa / (isa.importancetoprocess)::double precision))::numeric, 2) AS asa, count(plan.fkcontext) AS plans FROM ((((view_rm_process_active p JOIN view_process_ranking pr ON ((p.fkcontext = pr.fkprocess))) JOIN view_isa_activity isa ON ((p.fkcontext = isa.fkprocess))) JOIN view_rm_asset_active a ON ((a.fkcontext = isa.fkasset))) LEFT JOIN view_cm_plan_active plan ON ((plan.fkprocess = p.fkcontext))) GROUP BY p.sname, a.sname, round(((isa.isa / (isa.importancetoprocess)::double precision))::numeric, 2), pr.ranking ORDER BY pr.ranking DESC, round(((isa.isa / (isa.importancetoprocess)::double precision))::numeric, 2), a.sname, p.sname LIMIT 10;
CREATE TABLE wkf_alert (
    pkalert integer NOT NULL,
    fkcontext integer,
    fkreceiver integer,
    fkcreator integer,
    tjustification text,
    ntype integer DEFAULT 0,
    bemailsent integer DEFAULT 0,
    ddate timestamp without time zone NOT NULL,
    CONSTRAINT ckc_bemailsent_wkf_aler CHECK (((bemailsent IS NULL) OR ((bemailsent >= 0) AND (bemailsent <= 1))))
);
CREATE SEQUENCE wkf_alert_pkalert_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
ALTER SEQUENCE wkf_alert_pkalert_seq OWNED BY wkf_alert.pkalert;
CREATE TABLE wkf_control_efficiency (
    fkcontrolefficiency integer NOT NULL,
    fkschedule integer,
    nrealefficiency integer DEFAULT 0,
    nexpectedefficiency integer DEFAULT 0,
    svalue1 character varying(256),
    svalue2 character varying(256),
    svalue3 character varying(256),
    svalue4 character varying(256),
    svalue5 character varying(256),
    tmetric text
);
CREATE TABLE wkf_control_test (
    fkcontroltest integer NOT NULL,
    fkschedule integer,
    tdescription text
);
CREATE TABLE wkf_schedule (
    pkschedule integer NOT NULL,
    dstart timestamp without time zone NOT NULL,
    dend timestamp without time zone,
    ntype integer DEFAULT 0 NOT NULL,
    nperiodicity integer DEFAULT 0,
    nbitmap integer DEFAULT 0,
    nweek integer DEFAULT 0,
    nday integer DEFAULT 0,
    dnextoccurrence timestamp without time zone,
    ndaystofinish integer DEFAULT 0,
    ddatelimit timestamp without time zone
);
CREATE SEQUENCE wkf_schedule_pkschedule_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
ALTER SEQUENCE wkf_schedule_pkschedule_seq OWNED BY wkf_schedule.pkschedule;
CREATE TABLE wkf_task (
    pktask integer NOT NULL,
    fkcontext integer NOT NULL,
    fkcreator integer NOT NULL,
    fkreceiver integer NOT NULL,
    nactivity integer DEFAULT 0,
    bvisible integer,
    bemailsent integer DEFAULT 0,
    ddateaccomplished timestamp without time zone,
    ddatecreated timestamp without time zone,
    CONSTRAINT ckc_bemailsent_wkf_task CHECK (((bemailsent IS NULL) OR ((bemailsent >= 0) AND (bemailsent <= 1))))
);
CREATE SEQUENCE wkf_task_pkmetadata_seq
    START WITH 5
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
SET default_with_oids = false;
CREATE TABLE wkf_task_metadata (
    pktaskmetadata integer DEFAULT nextval('wkf_task_pkmetadata_seq'::regclass) NOT NULL,
    fkcontext integer,
    ntype integer,
    fktask integer
);
CREATE SEQUENCE wkf_task_pktask_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
ALTER SEQUENCE wkf_task_pktask_seq OWNED BY wkf_task.pktask;
SET default_with_oids = true;
CREATE TABLE wkf_task_schedule (
    fkcontext integer NOT NULL,
    nactivity integer DEFAULT 0 NOT NULL,
    fkschedule integer,
    balertsent integer DEFAULT 0 NOT NULL,
    nalerttype integer DEFAULT 0,
    CONSTRAINT ckc_balertsent_wkf_task CHECK (((balertsent >= 0) AND (balertsent <= 1)))
);
ALTER TABLE ci_nc ALTER COLUMN nseqnumber SET DEFAULT nextval('ci_nc_nseqnumber_seq'::regclass);
ALTER TABLE isms_context ALTER COLUMN pkcontext SET DEFAULT nextval('isms_context_pkcontext_seq'::regclass);
ALTER TABLE isms_context_classification ALTER COLUMN pkclassification SET DEFAULT nextval('isms_context_classification_pkclassification_seq'::regclass);
ALTER TABLE isms_context_history ALTER COLUMN pkid SET DEFAULT nextval('isms_context_history_pkid_seq'::regclass);
ALTER TABLE isms_non_conformity_types ALTER COLUMN pkclassification SET DEFAULT nextval('isms_non_conformity_types_pkclassification_seq'::regclass);
ALTER TABLE isms_saas ALTER COLUMN pkconfig SET DEFAULT nextval('isms_saas_pkconfig_seq'::regclass);
ALTER TABLE pm_doc_reference ALTER COLUMN pkreference SET DEFAULT nextval('pm_doc_reference_pkreference_seq'::regclass);
ALTER TABLE pm_instance_comment ALTER COLUMN pkcomment SET DEFAULT nextval('pm_instance_comment_pkcomment_seq'::regclass);
ALTER TABLE rm_parameter_name ALTER COLUMN pkparametername SET DEFAULT nextval('rm_parameter_name_pkparametername_seq'::regclass);
ALTER TABLE rm_parameter_value_name ALTER COLUMN pkvaluename SET DEFAULT nextval('rm_parameter_value_name_pkvaluename_seq'::regclass);
ALTER TABLE rm_rc_parameter_value_name ALTER COLUMN pkrcvaluename SET DEFAULT nextval('rm_rc_parameter_value_name_pkrcvaluename_seq'::regclass);
ALTER TABLE wkf_alert ALTER COLUMN pkalert SET DEFAULT nextval('wkf_alert_pkalert_seq'::regclass);
ALTER TABLE wkf_schedule ALTER COLUMN pkschedule SET DEFAULT nextval('wkf_schedule_pkschedule_seq'::regclass);
ALTER TABLE wkf_task ALTER COLUMN pktask SET DEFAULT nextval('wkf_task_pktask_seq'::regclass);
ALTER TABLE ONLY cm_threat
    ADD CONSTRAINT fkcontext UNIQUE (fkcontext);
ALTER TABLE ONLY ci_action_plan
    ADD CONSTRAINT pk_ci_action_plan PRIMARY KEY (fkcontext);
ALTER TABLE ONLY ci_category
    ADD CONSTRAINT pk_ci_category PRIMARY KEY (fkcontext);
ALTER TABLE ONLY ci_incident
    ADD CONSTRAINT pk_ci_incident PRIMARY KEY (fkcontext);
ALTER TABLE ONLY ci_incident_control
    ADD CONSTRAINT pk_ci_incident_control PRIMARY KEY (fkincident, fkcontrol, fkcontext);
ALTER TABLE ONLY ci_incident_financial_impact
    ADD CONSTRAINT pk_ci_incident_financial_impac PRIMARY KEY (fkincident, fkclassification);
ALTER TABLE ONLY ci_incident_process
    ADD CONSTRAINT pk_ci_incident_process PRIMARY KEY (fkprocess, fkincident, fkcontext);
ALTER TABLE ONLY ci_incident_risk
    ADD CONSTRAINT pk_ci_incident_risk PRIMARY KEY (fkcontext);
ALTER TABLE ONLY ci_incident_risk_value
    ADD CONSTRAINT pk_ci_incident_risk_value PRIMARY KEY (fkparametername, fkvaluename, fkincrisk);
ALTER TABLE ONLY ci_incident_user
    ADD CONSTRAINT pk_ci_incident_user PRIMARY KEY (fkuser, fkincident);
ALTER TABLE ONLY ci_nc
    ADD CONSTRAINT pk_ci_nc PRIMARY KEY (fkcontext);
ALTER TABLE ONLY ci_nc_action_plan
    ADD CONSTRAINT pk_ci_nc_action_plan PRIMARY KEY (fknc, fkactionplan);
ALTER TABLE ONLY ci_nc_process
    ADD CONSTRAINT pk_ci_nc_process PRIMARY KEY (fknc, fkprocess);
ALTER TABLE ONLY ci_nc_seed
    ADD CONSTRAINT pk_ci_nc_seed PRIMARY KEY (fkcontrol, ndeactivationreason);
ALTER TABLE ONLY ci_occurrence
    ADD CONSTRAINT pk_ci_occurrence PRIMARY KEY (fkcontext);
ALTER TABLE ONLY ci_risk_probability
    ADD CONSTRAINT pk_ci_risk_probability PRIMARY KEY (fkrisk, fkvaluename);
ALTER TABLE ONLY ci_risk_schedule
    ADD CONSTRAINT pk_ci_risk_schedule PRIMARY KEY (fkrisk);
ALTER TABLE ONLY ci_solution
    ADD CONSTRAINT pk_ci_solution PRIMARY KEY (fkcontext);
ALTER TABLE ONLY cm_asset_category
    ADD CONSTRAINT pk_cm_asset_cat PRIMARY KEY (fkcontext);
ALTER TABLE ONLY cm_asset_category_threat
    ADD CONSTRAINT pk_cm_asset_cat_thr PRIMARY KEY (fkcontext);
ALTER TABLE ONLY cm_place_category
    ADD CONSTRAINT pk_cm_place_cat PRIMARY KEY (fkcontext);
ALTER TABLE ONLY cm_place_category_threat
    ADD CONSTRAINT pk_cm_place_cat_thr PRIMARY KEY (fkcontext);
ALTER TABLE ONLY cm_process_category
    ADD CONSTRAINT pk_cm_proc_cat PRIMARY KEY (fkcontext);
ALTER TABLE ONLY cm_process_category_threat
    ADD CONSTRAINT pk_cm_proc_cat_thr PRIMARY KEY (fkcontext);
ALTER TABLE ONLY isms_config
    ADD CONSTRAINT pk_isms_config PRIMARY KEY (pkconfig);
ALTER TABLE ONLY isms_context
    ADD CONSTRAINT pk_isms_context PRIMARY KEY (pkcontext);
ALTER TABLE ONLY isms_context_classification
    ADD CONSTRAINT pk_isms_context_classification PRIMARY KEY (pkclassification);
ALTER TABLE ONLY isms_context_date
    ADD CONSTRAINT pk_isms_context_date PRIMARY KEY (fkcontext, naction);
ALTER TABLE ONLY isms_context_history
    ADD CONSTRAINT pk_isms_context_history PRIMARY KEY (pkid);
ALTER TABLE ONLY isms_non_conformity_types
    ADD CONSTRAINT pk_isms_non_conformity_types PRIMARY KEY (pkclassification);
ALTER TABLE ONLY isms_policy
    ADD CONSTRAINT pk_isms_policy PRIMARY KEY (fkcontext);
ALTER TABLE ONLY isms_profile
    ADD CONSTRAINT pk_isms_profile PRIMARY KEY (fkcontext);
ALTER TABLE ONLY isms_profile_acl
    ADD CONSTRAINT pk_isms_profile_acl PRIMARY KEY (fkprofile, stag);
ALTER TABLE ONLY isms_saas
    ADD CONSTRAINT pk_isms_saas PRIMARY KEY (pkconfig);
ALTER TABLE ONLY isms_scope
    ADD CONSTRAINT pk_isms_scope PRIMARY KEY (fkcontext);
ALTER TABLE ONLY isms_solicitor
    ADD CONSTRAINT pk_isms_solicitor PRIMARY KEY (fkuser, fksolicitor);
ALTER TABLE ONLY isms_user
    ADD CONSTRAINT pk_isms_user PRIMARY KEY (fkcontext);
ALTER TABLE ONLY isms_user_preference
    ADD CONSTRAINT pk_isms_user_preference PRIMARY KEY (fkuser, spreference);
ALTER TABLE ONLY wkf_task_metadata
    ADD CONSTRAINT pk_metadata PRIMARY KEY (pktaskmetadata);
ALTER TABLE ONLY pm_doc_approvers
    ADD CONSTRAINT pk_pm_doc_approvers PRIMARY KEY (fkdocument, fkuser);
ALTER TABLE ONLY pm_doc_context
    ADD CONSTRAINT pk_pm_doc_context PRIMARY KEY (fkcontext, fkdocument);
ALTER TABLE ONLY pm_doc_instance
    ADD CONSTRAINT pk_pm_doc_instance PRIMARY KEY (fkcontext);
ALTER TABLE ONLY pm_doc_read_history
    ADD CONSTRAINT pk_pm_doc_read_history PRIMARY KEY (ddate, fkinstance, fkuser);
ALTER TABLE ONLY pm_doc_readers
    ADD CONSTRAINT pk_pm_doc_readers PRIMARY KEY (fkdocument, fkuser);
ALTER TABLE ONLY pm_doc_reference
    ADD CONSTRAINT pk_pm_doc_reference PRIMARY KEY (pkreference);
ALTER TABLE ONLY pm_doc_registers
    ADD CONSTRAINT pk_pm_doc_registers PRIMARY KEY (fkregister, fkdocument);
ALTER TABLE ONLY pm_document
    ADD CONSTRAINT pk_pm_document PRIMARY KEY (fkcontext);
ALTER TABLE ONLY pm_document_revision_history
    ADD CONSTRAINT pk_pm_document_revision_histor PRIMARY KEY (fkdocument, ddaterevision);
ALTER TABLE ONLY pm_instance_comment
    ADD CONSTRAINT pk_pm_instance_comment PRIMARY KEY (pkcomment);
ALTER TABLE ONLY pm_instance_content
    ADD CONSTRAINT pk_pm_instance_content PRIMARY KEY (fkinstance);
ALTER TABLE ONLY pm_process_user
    ADD CONSTRAINT pk_pm_process_user PRIMARY KEY (fkprocess, fkuser);
ALTER TABLE ONLY pm_register
    ADD CONSTRAINT pk_pm_register PRIMARY KEY (fkcontext);
ALTER TABLE ONLY pm_register_readers
    ADD CONSTRAINT pk_pm_register_readers PRIMARY KEY (fkuser, fkregister);
ALTER TABLE ONLY pm_template
    ADD CONSTRAINT pk_pm_template PRIMARY KEY (fkcontext);
ALTER TABLE ONLY pm_template_best_practice
    ADD CONSTRAINT pk_pm_template_best_practice PRIMARY KEY (fktemplate, fkbestpractice);
ALTER TABLE ONLY pm_template_content
    ADD CONSTRAINT pk_pm_template_content PRIMARY KEY (fkcontext);
ALTER TABLE ONLY rm_area
    ADD CONSTRAINT pk_rm_area PRIMARY KEY (fkcontext);
ALTER TABLE ONLY rm_asset
    ADD CONSTRAINT pk_rm_asset PRIMARY KEY (fkcontext);
ALTER TABLE ONLY rm_asset_asset
    ADD CONSTRAINT pk_rm_asset_asset PRIMARY KEY (fkasset, fkdependent);
ALTER TABLE ONLY rm_asset_value
    ADD CONSTRAINT pk_rm_asset_value PRIMARY KEY (fkasset, fkvaluename, fkparametername);
ALTER TABLE ONLY rm_best_practice
    ADD CONSTRAINT pk_rm_best_practice PRIMARY KEY (fkcontext);
ALTER TABLE ONLY rm_best_practice_event
    ADD CONSTRAINT pk_rm_best_practice_event PRIMARY KEY (fkcontext);
ALTER TABLE ONLY rm_best_practice_standard
    ADD CONSTRAINT pk_rm_best_practice_standard PRIMARY KEY (fkcontext);
ALTER TABLE ONLY rm_category
    ADD CONSTRAINT pk_rm_category PRIMARY KEY (fkcontext);
ALTER TABLE ONLY rm_control
    ADD CONSTRAINT pk_rm_control PRIMARY KEY (fkcontext);
ALTER TABLE ONLY rm_control_best_practice
    ADD CONSTRAINT pk_rm_control_best_practice PRIMARY KEY (fkcontext);
ALTER TABLE ONLY rm_control_cost
    ADD CONSTRAINT pk_rm_control_cost PRIMARY KEY (fkcontrol);
ALTER TABLE ONLY rm_control_efficiency_history
    ADD CONSTRAINT pk_rm_control_efficiency_histo PRIMARY KEY (fkcontrol, ddatetodo);
ALTER TABLE ONLY rm_control_test_history
    ADD CONSTRAINT pk_rm_control_test_history PRIMARY KEY (fkcontrol, ddatetodo);
ALTER TABLE ONLY rm_event
    ADD CONSTRAINT pk_rm_event PRIMARY KEY (fkcontext);
ALTER TABLE ONLY rm_parameter_name
    ADD CONSTRAINT pk_rm_parameter_name PRIMARY KEY (pkparametername);
ALTER TABLE ONLY rm_parameter_value_name
    ADD CONSTRAINT pk_rm_parameter_value_name PRIMARY KEY (pkvaluename);
ALTER TABLE ONLY rm_process
    ADD CONSTRAINT pk_rm_process PRIMARY KEY (fkcontext);
ALTER TABLE ONLY rm_process_asset
    ADD CONSTRAINT pk_rm_process_asset PRIMARY KEY (fkcontext);
ALTER TABLE ONLY rm_rc_parameter_value_name
    ADD CONSTRAINT pk_rm_rc_parameter_value_name PRIMARY KEY (pkrcvaluename);
ALTER TABLE ONLY rm_risk
    ADD CONSTRAINT pk_rm_risk PRIMARY KEY (fkcontext);
ALTER TABLE ONLY rm_risk_control
    ADD CONSTRAINT pk_rm_risk_control PRIMARY KEY (fkcontext);
ALTER TABLE ONLY rm_risk_control_value
    ADD CONSTRAINT pk_rm_risk_control_value PRIMARY KEY (fkriskcontrol, fkparametername, fkrcvaluename);
ALTER TABLE ONLY rm_risk_limits
    ADD CONSTRAINT pk_rm_risk_limits PRIMARY KEY (fkcontext);
ALTER TABLE ONLY rm_risk_value
    ADD CONSTRAINT pk_rm_risk_value PRIMARY KEY (fkrisk, fkvaluename, fkparametername);
ALTER TABLE ONLY rm_section_best_practice
    ADD CONSTRAINT pk_rm_section_best_practice PRIMARY KEY (fkcontext);
ALTER TABLE ONLY rm_standard
    ADD CONSTRAINT pk_rm_standard PRIMARY KEY (fkcontext);
ALTER TABLE ONLY wkf_alert
    ADD CONSTRAINT pk_wkf_alert PRIMARY KEY (pkalert);
ALTER TABLE ONLY wkf_control_efficiency
    ADD CONSTRAINT pk_wkf_control_efficiency PRIMARY KEY (fkcontrolefficiency);
ALTER TABLE ONLY wkf_control_test
    ADD CONSTRAINT pk_wkf_control_test PRIMARY KEY (fkcontroltest);
ALTER TABLE ONLY wkf_schedule
    ADD CONSTRAINT pk_wkf_schedule PRIMARY KEY (pkschedule);
ALTER TABLE ONLY wkf_task
    ADD CONSTRAINT pk_wkf_task PRIMARY KEY (pktask);
ALTER TABLE ONLY wkf_task_schedule
    ADD CONSTRAINT pk_wkf_task_schedule PRIMARY KEY (fkcontext, nactivity);
ALTER TABLE ONLY cm_threat_vulnerability
    ADD CONSTRAINT pkcontext PRIMARY KEY (fkcontext);
CREATE UNIQUE INDEX c_scope_fkcontext_unique ON cm_scope USING btree (fkcontext);
CREATE UNIQUE INDEX ci_action_plan_pk ON ci_action_plan USING btree (fkcontext);
CREATE UNIQUE INDEX ci_category_pk ON ci_category USING btree (fkcontext);
CREATE UNIQUE INDEX ci_incident_control_pk ON ci_incident_control USING btree (fkincident, fkcontrol, fkcontext);
CREATE UNIQUE INDEX ci_incident_financial_impact_pk ON ci_incident_financial_impact USING btree (fkincident, fkclassification);
CREATE UNIQUE INDEX ci_incident_pk ON ci_incident USING btree (fkcontext);
CREATE UNIQUE INDEX ci_incident_process_pk ON ci_incident_process USING btree (fkprocess, fkincident, fkcontext);
CREATE UNIQUE INDEX ci_incident_risk_pk2 ON ci_incident_risk USING btree (fkcontext);
CREATE UNIQUE INDEX ci_incident_risk_value_pk ON ci_incident_risk_value USING btree (fkparametername, fkvaluename, fkincrisk);
CREATE UNIQUE INDEX ci_incident_user_pk ON ci_incident_user USING btree (fkuser, fkincident);
CREATE UNIQUE INDEX ci_nc_action_plan_pk ON ci_nc_action_plan USING btree (fknc, fkactionplan);
CREATE UNIQUE INDEX ci_nc_pk ON ci_nc USING btree (fkcontext);
CREATE UNIQUE INDEX ci_nc_process_pk ON ci_nc_process USING btree (fknc, fkprocess);
CREATE UNIQUE INDEX ci_nc_seed_pk ON ci_nc_seed USING btree (fkcontrol, ndeactivationreason);
CREATE UNIQUE INDEX ci_ocurrence_pk ON ci_occurrence USING btree (fkcontext);
CREATE UNIQUE INDEX ci_risk_schedule_pk ON ci_risk_schedule USING btree (fkrisk);
CREATE UNIQUE INDEX ci_risk_value_pk ON ci_risk_probability USING btree (fkrisk, fkvaluename);
CREATE UNIQUE INDEX ci_solution_pk ON ci_solution USING btree (fkcontext);
CREATE UNIQUE INDEX cm_damage_matrix_fkcontext_unique ON cm_damage_matrix USING btree (fkcontext);
CREATE UNIQUE INDEX cm_damage_matrix_parameter_fkcontext_unique ON cm_damage_matrix_parameter USING btree (fkcontext);
CREATE UNIQUE INDEX cm_fkcontext_importance ON cm_asset_importance USING btree (fkcontext);
CREATE UNIQUE INDEX cm_group_fkcontext_unique ON cm_group USING btree (fkcontext);
CREATE UNIQUE INDEX cm_group_resource_fkcontext_unique ON cm_group_resource USING btree (fkcontext);
CREATE UNIQUE INDEX cm_impact_damage_fkcontext_unique ON cm_impact_damage USING btree (fkcontext);
CREATE UNIQUE INDEX cm_impact_damage_range_fkcontext_unique ON cm_impact_damage_range USING btree (fkcontext);
CREATE UNIQUE INDEX cm_impact_fkcontext_unique ON cm_impact USING btree (fkcontext);
CREATE UNIQUE INDEX cm_provider_fkcontext_unique ON cm_provider USING btree (fkcontext);
CREATE UNIQUE INDEX cm_provider_resource_fkcontext_unique ON cm_provider_resource USING btree (fkcontext);
CREATE UNIQUE INDEX cm_resource_fkcontext_unique ON cm_resource USING btree (fkcontext);
CREATE UNIQUE INDEX cm_threat_fkcontext ON cm_threat_probability USING btree (fkcontext);
CREATE INDEX fkactionplan_fk ON ci_nc_action_plan USING btree (fkactionplan);
CREATE INDEX fkarea_fk ON rm_process USING btree (fkarea);
CREATE INDEX fkasset_fk ON rm_process_asset USING btree (fkasset);
CREATE INDEX fkasset_fk2 ON rm_risk USING btree (fkasset);
CREATE INDEX fkasset_fk3 ON rm_asset_value USING btree (fkasset);
CREATE INDEX fkasset_fk4 ON rm_asset_asset USING btree (fkasset);
CREATE INDEX fkauthor_fk ON pm_document USING btree (fkauthor);
CREATE INDEX fkbestpractice_fk ON rm_control_best_practice USING btree (fkbestpractice);
CREATE INDEX fkbestpractice_fk2 ON rm_best_practice_standard USING btree (fkbestpractice);
CREATE INDEX fkbestpractice_fk3 ON rm_best_practice_event USING btree (fkbestpractice);
CREATE INDEX fkbestpractice_fk4 ON pm_template_best_practice USING btree (fkbestpractice);
CREATE INDEX fkcategory_fk2 ON rm_event USING btree (fkcategory);
CREATE INDEX fkcategory_fk3 ON ci_incident USING btree (fkcategory);
CREATE INDEX fkcategory_fk4 ON ci_solution USING btree (fkcategory);
CREATE INDEX fkclassification_fk ON pm_document USING btree (fkclassification);
CREATE INDEX fkclassification_fk2 ON pm_register USING btree (fkclassification);
CREATE INDEX fkclassification_fk3 ON ci_incident_financial_impact USING btree (fkclassification);
CREATE INDEX fkcontext_fk ON wkf_task USING btree (fkcontext);
CREATE INDEX fkcontext_fk2 ON wkf_alert USING btree (fkcontext);
CREATE INDEX fkcontext_fk3 ON isms_context_date USING btree (fkcontext);
CREATE INDEX fkcontext_fk4 ON pm_doc_context USING btree (fkcontext);
CREATE INDEX fkcontext_fk5 ON isms_context_history USING btree (fkcontext);
CREATE INDEX fkcontext_fk6 ON ci_incident_control USING btree (fkcontext);
CREATE INDEX fkcontext_fk7 ON wkf_task_schedule USING btree (fkcontext);
CREATE UNIQUE INDEX fkcontext_process_activity ON cm_process_activity USING btree (fkcontext);
CREATE UNIQUE INDEX fkcontext_unique ON cm_place USING btree (fkcontext);
CREATE UNIQUE INDEX fkcontext_unique_threat_vulnerability ON cm_threat_vulnerability USING btree (fkcontext);
CREATE INDEX fkcontrol_fk ON rm_risk_control USING btree (fkcontrol);
CREATE INDEX fkcontrol_fk2 ON rm_control_best_practice USING btree (fkcontrol);
CREATE INDEX fkcontrol_fk3 ON rm_control_efficiency_history USING btree (fkcontrol);
CREATE INDEX fkcontrol_fk4 ON rm_control_test_history USING btree (fkcontrol);
CREATE INDEX fkcontrol_fk5 ON ci_nc USING btree (fkcontrol);
CREATE INDEX fkcontrol_fk6 ON ci_incident_control USING btree (fkcontrol);
CREATE INDEX fkcontrol_fk7 ON ci_nc_seed USING btree (fkcontrol);
CREATE INDEX fkcreator_fk ON wkf_task USING btree (fkcreator);
CREATE INDEX fkcreator_fk2 ON wkf_alert USING btree (fkcreator);
CREATE INDEX fkcurrentversion_fk ON pm_document USING btree (fkcurrentversion);
CREATE INDEX fkdependent_fk ON rm_asset_asset USING btree (fkdependent);
CREATE INDEX fkdocument_fk ON pm_doc_approvers USING btree (fkdocument);
CREATE INDEX fkdocument_fk2 ON pm_doc_instance USING btree (fkdocument);
CREATE INDEX fkdocument_fk3 ON pm_doc_reference USING btree (fkdocument);
CREATE INDEX fkdocument_fk4 ON pm_doc_readers USING btree (fkdocument);
CREATE INDEX fkdocument_fk5 ON pm_document_revision_history USING btree (fkdocument);
CREATE INDEX fkdocument_fk6 ON pm_doc_registers USING btree (fkdocument);
CREATE INDEX fkdocument_fk7 ON pm_register USING btree (fkdocument);
CREATE INDEX fkdocument_fk8 ON pm_doc_context USING btree (fkdocument);
CREATE INDEX fkevent_fk ON rm_risk USING btree (fkevent);
CREATE INDEX fkevent_fk2 ON rm_best_practice_event USING btree (fkevent);
CREATE INDEX fkincident_fk ON ci_occurrence USING btree (fkincident);
CREATE INDEX fkincident_fk2 ON ci_incident_user USING btree (fkincident);
CREATE INDEX fkincident_fk4 ON ci_incident_control USING btree (fkincident);
CREATE INDEX fkincident_fk5 ON ci_incident_process USING btree (fkincident);
CREATE INDEX fkincident_fk6 ON ci_incident_financial_impact USING btree (fkincident);
CREATE INDEX fkincident_fk7 ON ci_incident_risk USING btree (fkincident);
CREATE INDEX fkincrisk_fk ON ci_incident_risk_value USING btree (fkincrisk);
CREATE INDEX fkinstance_fk ON pm_instance_comment USING btree (fkinstance);
CREATE INDEX fkmainappover_fk ON pm_document USING btree (fkmainapprover);
CREATE INDEX fknc_fk ON ci_nc_process USING btree (fknc);
CREATE INDEX fknc_fk2 ON ci_nc_action_plan USING btree (fknc);
CREATE INDEX fkparametername_fk ON rm_asset_value USING btree (fkparametername);
CREATE INDEX fkparametername_fk2 ON rm_risk_value USING btree (fkparametername);
CREATE INDEX fkparametername_fk3 ON rm_risk_control_value USING btree (fkparametername);
CREATE INDEX fkparametername_fk4 ON ci_incident_risk_value USING btree (fkparametername);
CREATE INDEX fkparamter_fk ON ci_risk_probability USING btree (fkvaluename);
CREATE INDEX fkparent_fk ON rm_area USING btree (fkparent);
CREATE INDEX fkparent_fk2 ON rm_category USING btree (fkparent);
CREATE INDEX fkparent_fk3 ON rm_section_best_practice USING btree (fkparent);
CREATE INDEX fkparent_fk5 ON pm_document USING btree (fkparent);
CREATE INDEX fkpriority_fk ON rm_area USING btree (fkpriority);
CREATE INDEX fkprobabilityvaluename_fk ON rm_risk USING btree (fkprobabilityvaluename);
CREATE INDEX fkprobabilityvaluename_fk2 ON rm_risk_control USING btree (fkprobabilityvaluename);
CREATE INDEX fkprocess_fk ON rm_process_asset USING btree (fkprocess);
CREATE INDEX fkprocess_fk2 ON pm_process_user USING btree (fkprocess);
CREATE INDEX fkprocess_fk3 ON ci_nc_process USING btree (fkprocess);
CREATE INDEX fkprocess_fk4 ON ci_incident_process USING btree (fkprocess);
CREATE INDEX fkprofile_fk ON isms_user USING btree (fkprofile);
CREATE INDEX fkprofile_fk2 ON isms_profile_acl USING btree (fkprofile);
CREATE INDEX fkrcvaluename_fk ON rm_risk_control_value USING btree (fkrcvaluename);
CREATE INDEX fkreceiver_fk ON wkf_task USING btree (fkreceiver);
CREATE INDEX fkreceiver_fk2 ON wkf_alert USING btree (fkreceiver);
CREATE INDEX fkregister_fk ON pm_doc_registers USING btree (fkregister);
CREATE INDEX fkregister_fk2 ON pm_register_readers USING btree (fkregister);
CREATE INDEX fkresponsible_fk3 ON rm_control USING btree (fkresponsible);
CREATE INDEX fkresponsible_fk5 ON pm_register USING btree (fkresponsible);
CREATE INDEX fkresponsible_fk6 ON ci_incident USING btree (fkresponsible);
CREATE INDEX fkresponsible_fk7 ON ci_nc USING btree (fkresponsible);
CREATE INDEX fkresponsible_fk8 ON ci_action_plan USING btree (fkresponsible);
CREATE INDEX fkrisk_fk ON rm_risk_control USING btree (fkrisk);
CREATE INDEX fkrisk_fk2 ON rm_risk_value USING btree (fkrisk);
CREATE INDEX fkrisk_fk4 ON ci_risk_probability USING btree (fkrisk);
CREATE INDEX fkrisk_fk5 ON ci_incident_risk USING btree (fkrisk);
CREATE INDEX fkriskcontrol_fk ON rm_risk_control_value USING btree (fkriskcontrol);
CREATE INDEX fkschedule_fk ON wkf_control_efficiency USING btree (fkschedule);
CREATE INDEX fkschedule_fk2 ON wkf_control_test USING btree (fkschedule);
CREATE INDEX fkschedule_fk3 ON wkf_task_schedule USING btree (fkschedule);
CREATE INDEX fkschedule_fk4 ON pm_document USING btree (fkschedule);
CREATE INDEX fksectionbestpractice_fk ON rm_best_practice USING btree (fksectionbestpractice);
CREATE INDEX fksender_fk ON ci_nc USING btree (fksender);
CREATE INDEX fksolicitor_fk ON isms_solicitor USING btree (fksolicitor);
CREATE INDEX fkstandard_fk ON rm_best_practice_standard USING btree (fkstandard);
CREATE INDEX fktemplate_fk ON pm_template_best_practice USING btree (fktemplate);
CREATE INDEX fktype_fk ON rm_area USING btree (fktype);
CREATE INDEX fktype_fk3 ON rm_control USING btree (fktype);
CREATE INDEX fktype_fk4 ON rm_risk USING btree (fktype);
CREATE INDEX fktype_fk5 ON rm_event USING btree (fktype);
CREATE INDEX fkuser_fk ON isms_user_preference USING btree (fkuser);
CREATE INDEX fkuser_fk2 ON isms_solicitor USING btree (fkuser);
CREATE INDEX fkuser_fk3 ON pm_doc_approvers USING btree (fkuser);
CREATE INDEX fkuser_fk4 ON pm_instance_comment USING btree (fkuser);
CREATE INDEX fkuser_fk5 ON pm_doc_readers USING btree (fkuser);
CREATE INDEX fkuser_fk6 ON pm_process_user USING btree (fkuser);
CREATE INDEX fkuser_fk7 ON pm_doc_read_history USING btree (fkuser);
CREATE INDEX fkuser_fk8 ON ci_incident_user USING btree (fkuser);
CREATE INDEX fkuser_fk9 ON pm_register_readers USING btree (fkuser);
CREATE INDEX fkvaluename_fk ON rm_asset_value USING btree (fkvaluename);
CREATE INDEX fkvaluename_fk2 ON rm_risk_value USING btree (fkvaluename);
CREATE INDEX fkvaluename_fk3 ON ci_incident_risk_value USING btree (fkvaluename);
CREATE UNIQUE INDEX isms_config_pk ON isms_config USING btree (pkconfig);
CREATE UNIQUE INDEX isms_context_classification_pk ON isms_context_classification USING btree (pkclassification);
CREATE UNIQUE INDEX isms_context_date_pk ON isms_context_date USING btree (fkcontext, naction);
CREATE UNIQUE INDEX isms_context_history_pk ON isms_context_history USING btree (pkid);
CREATE UNIQUE INDEX isms_context_pk ON isms_context USING btree (pkcontext);
CREATE UNIQUE INDEX isms_non_conformity_types_pk ON isms_non_conformity_types USING btree (pkclassification);
CREATE UNIQUE INDEX isms_policy_pk ON isms_policy USING btree (fkcontext);
CREATE UNIQUE INDEX isms_profile_acl_pk ON isms_profile_acl USING btree (fkprofile, stag);
CREATE UNIQUE INDEX isms_profile_pk ON isms_profile USING btree (fkcontext);
CREATE UNIQUE INDEX isms_saas_pk ON isms_saas USING btree (pkconfig);
CREATE UNIQUE INDEX isms_scope_pk ON isms_scope USING btree (fkcontext);
CREATE UNIQUE INDEX isms_solicitor_pk ON isms_solicitor USING btree (fkuser, fksolicitor);
CREATE UNIQUE INDEX isms_user_pk ON isms_user USING btree (fkcontext);
CREATE UNIQUE INDEX isms_user_preference_pk ON isms_user_preference USING btree (fkuser, spreference);
CREATE UNIQUE INDEX pm_doc_instance_pk ON pm_doc_instance USING btree (fkcontext);
CREATE UNIQUE INDEX pm_doc_read_history_pk ON pm_doc_read_history USING btree (ddate, fkinstance, fkuser);
CREATE UNIQUE INDEX pm_doc_reference_pk ON pm_doc_reference USING btree (pkreference);
CREATE UNIQUE INDEX pm_document_pk ON pm_document USING btree (fkcontext);
CREATE UNIQUE INDEX pm_document_revision_history_pk ON pm_document_revision_history USING btree (fkdocument, ddaterevision);
CREATE UNIQUE INDEX pm_instance_comment_pk ON pm_instance_comment USING btree (pkcomment);
CREATE UNIQUE INDEX pm_instance_content_pk ON pm_instance_content USING btree (fkinstance);
CREATE UNIQUE INDEX pm_register_pk ON pm_register USING btree (fkcontext);
CREATE UNIQUE INDEX pm_register_readers_pk ON pm_register_readers USING btree (fkuser, fkregister);
CREATE UNIQUE INDEX pm_template_best_practice_pk ON pm_template_best_practice USING btree (fktemplate, fkbestpractice);
CREATE UNIQUE INDEX pm_template_content_pk ON pm_template_content USING btree (fkcontext);
CREATE UNIQUE INDEX pm_template_pk ON pm_template USING btree (fkcontext);
CREATE UNIQUE INDEX rm_area_pk ON rm_area USING btree (fkcontext);
CREATE UNIQUE INDEX rm_asset_asset_pk ON rm_asset_asset USING btree (fkasset, fkdependent);
CREATE UNIQUE INDEX rm_asset_pk ON rm_asset USING btree (fkcontext);
CREATE UNIQUE INDEX rm_asset_value_pk ON rm_asset_value USING btree (fkasset, fkvaluename, fkparametername);
CREATE UNIQUE INDEX rm_best_practice_event_pk ON rm_best_practice_event USING btree (fkcontext);
CREATE UNIQUE INDEX rm_best_practice_pk ON rm_best_practice USING btree (fkcontext);
CREATE UNIQUE INDEX rm_best_practice_standard_pk ON rm_best_practice_standard USING btree (fkcontext);
CREATE UNIQUE INDEX rm_category_pk ON rm_category USING btree (fkcontext);
CREATE UNIQUE INDEX rm_control_best_practice_pk ON rm_control_best_practice USING btree (fkcontext);
CREATE UNIQUE INDEX rm_control_cost_pk ON rm_control_cost USING btree (fkcontrol);
CREATE UNIQUE INDEX rm_control_efficiency_history_p ON rm_control_efficiency_history USING btree (fkcontrol, ddatetodo);
CREATE UNIQUE INDEX rm_control_pk ON rm_control USING btree (fkcontext);
CREATE UNIQUE INDEX rm_control_test_history_pk ON rm_control_test_history USING btree (fkcontrol, ddatetodo);
CREATE UNIQUE INDEX rm_event_pk ON rm_event USING btree (fkcontext);
CREATE UNIQUE INDEX rm_parameter_name_pk ON rm_parameter_name USING btree (pkparametername);
CREATE UNIQUE INDEX rm_parameter_value_name_pk ON rm_parameter_value_name USING btree (pkvaluename);
CREATE UNIQUE INDEX rm_process_asset_pk ON rm_process_asset USING btree (fkcontext);
CREATE UNIQUE INDEX rm_process_pk ON rm_process USING btree (fkcontext);
CREATE UNIQUE INDEX rm_rc_parameter_value_name_pk ON rm_rc_parameter_value_name USING btree (pkrcvaluename);
CREATE UNIQUE INDEX rm_risk_control_pk ON rm_risk_control USING btree (fkcontext);
CREATE UNIQUE INDEX rm_risk_control_value_pk ON rm_risk_control_value USING btree (fkriskcontrol, fkparametername, fkrcvaluename);
CREATE UNIQUE INDEX rm_risk_limits_pk ON rm_risk_limits USING btree (fkcontext);
CREATE UNIQUE INDEX rm_risk_pk ON rm_risk USING btree (fkcontext);
CREATE UNIQUE INDEX rm_risk_value_pk ON rm_risk_value USING btree (fkrisk, fkvaluename, fkparametername);
CREATE UNIQUE INDEX rm_section_best_practice_pk ON rm_section_best_practice USING btree (fkcontext);
CREATE UNIQUE INDEX rm_standard_pk ON rm_standard USING btree (fkcontext);
CREATE UNIQUE INDEX unique_cm_department ON cm_department USING btree (fkcontext);
CREATE UNIQUE INDEX unique_cm_financial_impact ON cm_financial_impact USING btree (fkcontext);
CREATE UNIQUE INDEX unique_cm_impact_type ON cm_impact_type USING btree (fkcontext);
CREATE UNIQUE INDEX unique_cm_plan ON cm_plan USING btree (fkcontext);
CREATE UNIQUE INDEX unique_cm_plan_action ON cm_plan_action USING btree (fkcontext);
CREATE UNIQUE INDEX unique_cm_plan_action_test ON cm_plan_action_test USING btree (fkcontext);
CREATE UNIQUE INDEX unique_cm_plan_range ON cm_plan_range USING btree (fkcontext);
CREATE UNIQUE INDEX unique_cm_plan_range_type ON cm_plan_range_type USING btree (fkcontext);
CREATE UNIQUE INDEX unique_cm_plan_schedule ON cm_plan_schedule USING btree (fkcontext);
CREATE UNIQUE INDEX unique_cm_plan_test ON cm_plan_test USING btree (fkcontext);
CREATE UNIQUE INDEX unique_cm_priority ON cm_priority USING btree (fkcontext);
CREATE UNIQUE INDEX unique_fkcontext ON cm_scene USING btree (fkcontext);
CREATE UNIQUE INDEX wkf_alert_pk ON wkf_alert USING btree (pkalert);
CREATE UNIQUE INDEX wkf_control_efficiency_pk ON wkf_control_efficiency USING btree (fkcontrolefficiency);
CREATE UNIQUE INDEX wkf_control_test_pk ON wkf_control_test USING btree (fkcontroltest);
CREATE UNIQUE INDEX wkf_schedule_pk ON wkf_schedule USING btree (pkschedule);
CREATE UNIQUE INDEX wkf_task_pk ON wkf_task USING btree (pktask);
CREATE UNIQUE INDEX wkf_task_schedule_pk ON wkf_task_schedule USING btree (fkcontext, nactivity);
CREATE TRIGGER delete_user
    AFTER DELETE ON isms_user
    FOR EACH ROW
    EXECUTE PROCEDURE delete_user();
CREATE TRIGGER recalculation_of_area_by_area
    AFTER UPDATE ON rm_area
    FOR EACH ROW
    EXECUTE PROCEDURE recalculation_of_area_by_area();
CREATE TRIGGER recalculation_of_area_by_process_delete
    AFTER DELETE ON rm_process
    FOR EACH ROW
    EXECUTE PROCEDURE recalculation_of_area_by_process_delete();
CREATE TRIGGER recalculation_of_area_by_process_update
    AFTER UPDATE ON rm_process
    FOR EACH ROW
    EXECUTE PROCEDURE recalculation_of_area_by_process_update();
CREATE TRIGGER recalculation_of_asset_association
    AFTER INSERT OR DELETE OR UPDATE ON rm_asset_asset
    FOR EACH ROW
    EXECUTE PROCEDURE recalculation_of_asset_association();
CREATE TRIGGER recalculation_of_asset_for_risk_delete
    AFTER DELETE ON rm_risk
    FOR EACH ROW
    EXECUTE PROCEDURE recalculation_of_asset_for_risk_delete();
CREATE TRIGGER recalculation_of_dependent_asset
    AFTER UPDATE ON rm_asset
    FOR EACH ROW
    EXECUTE PROCEDURE recalculation_of_dependent_asset();
CREATE TRIGGER recalculation_of_process
    AFTER UPDATE ON rm_asset
    FOR EACH ROW
    EXECUTE PROCEDURE recalculation_of_process();
CREATE TRIGGER recalculation_of_process_association
    AFTER INSERT OR DELETE OR UPDATE ON rm_process_asset
    FOR EACH ROW
    EXECUTE PROCEDURE recalculation_of_process_association();
CREATE TRIGGER recalculation_of_risk_for_insert
    AFTER INSERT ON rm_risk
    FOR EACH ROW
    EXECUTE PROCEDURE recalculation_of_risk_for_insert();
CREATE TRIGGER recalculation_of_risk_for_update
    AFTER UPDATE ON rm_risk
    FOR EACH ROW
    EXECUTE PROCEDURE recalculation_of_risk_for_update();
CREATE TRIGGER recalculation_of_risks_values
    AFTER UPDATE ON rm_control
    FOR EACH ROW
    EXECUTE PROCEDURE recalculation_of_risks_values();
CREATE TRIGGER recauculate_risk_for_risk_control_association
    AFTER DELETE ON rm_risk_control
    FOR EACH ROW
    EXECUTE PROCEDURE recauculate_risk_for_risk_control_association();
CREATE TRIGGER update_alert_sent
    AFTER UPDATE ON wkf_task
    FOR EACH ROW
    EXECUTE PROCEDURE update_alert_sent();
CREATE TRIGGER update_control_is_active
    BEFORE UPDATE ON rm_control
    FOR EACH ROW
    EXECUTE PROCEDURE update_control_is_active();

ALTER TABLE ONLY cm_impact
    ADD CONSTRAINT cm_impact_type FOREIGN KEY (ntype) REFERENCES cm_impact_type(fkcontext);
ALTER TABLE ONLY cm_scope_place
    ADD CONSTRAINT cm_place_fkcontext FOREIGN KEY (fkplace) REFERENCES cm_place(fkcontext);
ALTER TABLE ONLY cm_place
    ADD CONSTRAINT cm_places_fkcontext_fkey FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_threat
    ADD CONSTRAINT cm_threat FOREIGN KEY (ntype) REFERENCES isms_context_classification(pkclassification);
ALTER TABLE ONLY cm_process_inputs
    ADD CONSTRAINT context FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY ci_action_plan
    ADD CONSTRAINT fk_ci_actio_fkcontext_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ci_action_plan
    ADD CONSTRAINT fk_ci_actio_fkrespons_isms_use FOREIGN KEY (fkresponsible) REFERENCES isms_user(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ci_category
    ADD CONSTRAINT fk_ci_categ_fkcontext_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ci_incident
    ADD CONSTRAINT fk_ci_incid_fkcategor_ci_categ FOREIGN KEY (fkcategory) REFERENCES ci_category(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ci_incident_financial_impact
    ADD CONSTRAINT fk_ci_incid_fkclassif_isms_con FOREIGN KEY (fkclassification) REFERENCES isms_context_classification(pkclassification) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ci_incident_process
    ADD CONSTRAINT fk_ci_incid_fkcontext_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ci_incident
    ADD CONSTRAINT fk_ci_incid_fkcontext_isms_con2 FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ci_incident_risk
    ADD CONSTRAINT fk_ci_incid_fkcontext_isms_con7 FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ci_incident_control
    ADD CONSTRAINT fk_ci_incid_fkcontext_isms_con8 FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ci_incident_control
    ADD CONSTRAINT fk_ci_incid_fkcontrol_rm_contr FOREIGN KEY (fkcontrol) REFERENCES rm_control(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY ci_incident_user
    ADD CONSTRAINT fk_ci_incid_fkinciden_ci_incid FOREIGN KEY (fkincident) REFERENCES ci_incident(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY ci_incident_control
    ADD CONSTRAINT fk_ci_incid_fkinciden_ci_incid2 FOREIGN KEY (fkincident) REFERENCES ci_incident(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY ci_incident_financial_impact
    ADD CONSTRAINT fk_ci_incid_fkinciden_ci_incid3 FOREIGN KEY (fkincident) REFERENCES ci_incident(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY ci_incident_process
    ADD CONSTRAINT fk_ci_incid_fkinciden_ci_incid4 FOREIGN KEY (fkincident) REFERENCES ci_incident(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY ci_incident_risk
    ADD CONSTRAINT fk_ci_incid_fkinciden_ci_incid5 FOREIGN KEY (fkincident) REFERENCES ci_incident(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY ci_incident_risk_value
    ADD CONSTRAINT fk_ci_incid_fkincrisk_ci_incid FOREIGN KEY (fkincrisk) REFERENCES ci_incident_risk(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY ci_incident_risk_value
    ADD CONSTRAINT fk_ci_incid_fkparamet_rm_param FOREIGN KEY (fkparametername) REFERENCES rm_parameter_name(pkparametername) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY ci_incident_process
    ADD CONSTRAINT fk_ci_incid_fkprocess_rm_proce FOREIGN KEY (fkprocess) REFERENCES rm_process(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY ci_incident
    ADD CONSTRAINT fk_ci_incid_fkrespons_isms_use FOREIGN KEY (fkresponsible) REFERENCES isms_user(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ci_incident_risk
    ADD CONSTRAINT fk_ci_incid_fkrisk_rm_risk FOREIGN KEY (fkrisk) REFERENCES rm_risk(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY ci_incident_user
    ADD CONSTRAINT fk_ci_incid_fkuser_isms_use FOREIGN KEY (fkuser) REFERENCES isms_user(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY ci_incident_risk_value
    ADD CONSTRAINT fk_ci_incid_fkvaluena_rm_param FOREIGN KEY (fkvaluename) REFERENCES rm_parameter_value_name(pkvaluename) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY ci_nc_action_plan
    ADD CONSTRAINT fk_ci_nc_ac_fkactionp_ci_actio FOREIGN KEY (fkactionplan) REFERENCES ci_action_plan(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY ci_nc_action_plan
    ADD CONSTRAINT fk_ci_nc_ac_fknc_ci_nc FOREIGN KEY (fknc) REFERENCES ci_nc(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY ci_nc
    ADD CONSTRAINT fk_ci_nc_fkcontext_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ci_nc
    ADD CONSTRAINT fk_ci_nc_fkcontrol_rm_contr FOREIGN KEY (fkcontrol) REFERENCES rm_control(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY ci_nc
    ADD CONSTRAINT fk_ci_nc_fkrespons_isms_use FOREIGN KEY (fkresponsible) REFERENCES isms_user(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ci_nc
    ADD CONSTRAINT fk_ci_nc_fksender_isms_use FOREIGN KEY (fksender) REFERENCES isms_user(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ci_nc_process
    ADD CONSTRAINT fk_ci_nc_pr_fknc_ci_nc FOREIGN KEY (fknc) REFERENCES ci_nc(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY ci_nc_process
    ADD CONSTRAINT fk_ci_nc_pr_fkprocess_rm_proce FOREIGN KEY (fkprocess) REFERENCES rm_process(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY ci_nc_seed
    ADD CONSTRAINT fk_ci_nc_se_fkcontrol_rm_contr FOREIGN KEY (fkcontrol) REFERENCES rm_control(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY ci_occurrence
    ADD CONSTRAINT fk_ci_occur_fkcontext_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ci_occurrence
    ADD CONSTRAINT fk_ci_occur_fkinciden_ci_incid FOREIGN KEY (fkincident) REFERENCES ci_incident(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY ci_risk_probability
    ADD CONSTRAINT fk_ci_risk__fkrisk_ci_risk_ FOREIGN KEY (fkrisk) REFERENCES ci_risk_schedule(fkrisk) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY ci_risk_schedule
    ADD CONSTRAINT fk_ci_risk__fkrisk_rm_risk FOREIGN KEY (fkrisk) REFERENCES rm_risk(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY ci_risk_probability
    ADD CONSTRAINT fk_ci_risk__fkvaluena_rm_param FOREIGN KEY (fkvaluename) REFERENCES rm_parameter_value_name(pkvaluename) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY ci_solution
    ADD CONSTRAINT fk_ci_solut_fkcategor_ci_categ FOREIGN KEY (fkcategory) REFERENCES ci_category(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY ci_solution
    ADD CONSTRAINT fk_ci_solut_fkcontext_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY cm_asset_category
    ADD CONSTRAINT fk_cm_asset_cat FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY cm_asset_category
    ADD CONSTRAINT fk_cm_asset_cat_cm_asset_cat FOREIGN KEY (fkparent) REFERENCES cm_asset_category(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY cm_asset_category_threat
    ADD CONSTRAINT fk_cm_asset_cat_thr_cm_asset_cat FOREIGN KEY (fkassetcategory) REFERENCES cm_asset_category(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY cm_asset_category_threat
    ADD CONSTRAINT fk_cm_asset_cat_thr_cm_thr FOREIGN KEY (fkthreat) REFERENCES cm_threat(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY cm_asset_category_threat
    ADD CONSTRAINT fk_cm_asset_cat_thr_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY cm_place_category
    ADD CONSTRAINT fk_cm_place_cat FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY cm_place_category
    ADD CONSTRAINT fk_cm_place_cat_cm_place_cat FOREIGN KEY (fkparent) REFERENCES cm_place_category(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY cm_place_category_threat
    ADD CONSTRAINT fk_cm_place_cat_thr_cm_place_cat FOREIGN KEY (fkplacecategory) REFERENCES cm_place_category(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY cm_place_category_threat
    ADD CONSTRAINT fk_cm_place_cat_thr_cm_thr FOREIGN KEY (fkthreat) REFERENCES cm_threat(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY cm_place_category_threat
    ADD CONSTRAINT fk_cm_place_cat_thr_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY cm_process_category
    ADD CONSTRAINT fk_cm_proc_cat FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY cm_process_category
    ADD CONSTRAINT fk_cm_proc_cat_cm_proc_cat FOREIGN KEY (fkparent) REFERENCES cm_process_category(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY cm_process_category_threat
    ADD CONSTRAINT fk_cm_proc_cat_thr_cm_proc_cat FOREIGN KEY (fkprocesscategory) REFERENCES cm_process_category(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY cm_process_category_threat
    ADD CONSTRAINT fk_cm_proc_cat_thr_cm_thr FOREIGN KEY (fkthreat) REFERENCES cm_threat(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY cm_process_category_threat
    ADD CONSTRAINT fk_cm_proc_cat_thr_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY wkf_task_metadata
    ADD CONSTRAINT fk_context FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY rm_asset
    ADD CONSTRAINT fk_fkcategory_cat FOREIGN KEY (fkcategory) REFERENCES rm_category(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY cm_place
    ADD CONSTRAINT fk_fktype_place_cat FOREIGN KEY (fktype) REFERENCES cm_place_category(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY isms_context_date
    ADD CONSTRAINT fk_isms_con_fkcontext_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY isms_context_history
    ADD CONSTRAINT fk_isms_con_fkcontext_isms_con2 FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY isms_policy
    ADD CONSTRAINT fk_isms_pol_fkcontext_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY isms_profile
    ADD CONSTRAINT fk_isms_pro_fkcontext_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY isms_profile_acl
    ADD CONSTRAINT fk_isms_pro_fkprofile_isms_pro FOREIGN KEY (fkprofile) REFERENCES isms_profile(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY isms_scope
    ADD CONSTRAINT fk_isms_sco_fkcontext_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY isms_solicitor
    ADD CONSTRAINT fk_isms_sol_fksolicit_isms_use FOREIGN KEY (fksolicitor) REFERENCES isms_user(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY isms_solicitor
    ADD CONSTRAINT fk_isms_sol_fkuser_isms_use FOREIGN KEY (fkuser) REFERENCES isms_user(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY isms_user
    ADD CONSTRAINT fk_isms_use_fkcontext_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY isms_user
    ADD CONSTRAINT fk_isms_use_fkprofile_isms_pro FOREIGN KEY (fkprofile) REFERENCES isms_profile(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY isms_user_preference
    ADD CONSTRAINT fk_isms_use_fkuser_isms_use FOREIGN KEY (fkuser) REFERENCES isms_user(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY isms_user_password_history
    ADD CONSTRAINT fk_isms_use_fkuser_isms_use3 FOREIGN KEY (fkuser) REFERENCES isms_user(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY pm_doc_approvers
    ADD CONSTRAINT fk_pm_doc_a_fkdocumen_pm_docum FOREIGN KEY (fkdocument) REFERENCES pm_document(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY pm_doc_approvers
    ADD CONSTRAINT fk_pm_doc_a_fkuser_isms_use FOREIGN KEY (fkuser) REFERENCES isms_user(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY pm_doc_context
    ADD CONSTRAINT fk_pm_doc_c_fkcontext_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY pm_doc_context
    ADD CONSTRAINT fk_pm_doc_c_fkdocumen_pm_docum FOREIGN KEY (fkdocument) REFERENCES pm_document(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY pm_doc_instance
    ADD CONSTRAINT fk_pm_doc_i_fkcontext_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY pm_doc_instance
    ADD CONSTRAINT fk_pm_doc_i_fkdocumen_pm_docum FOREIGN KEY (fkdocument) REFERENCES pm_document(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY pm_doc_reference
    ADD CONSTRAINT fk_pm_doc_r_fkdocumen_pm_docum2 FOREIGN KEY (fkdocument) REFERENCES pm_document(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY pm_doc_registers
    ADD CONSTRAINT fk_pm_doc_r_fkdocumen_pm_docum3 FOREIGN KEY (fkdocument) REFERENCES pm_document(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY pm_doc_readers
    ADD CONSTRAINT fk_pm_doc_r_fkdocumen_pm_docum4 FOREIGN KEY (fkdocument) REFERENCES pm_document(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY pm_doc_read_history
    ADD CONSTRAINT fk_pm_doc_r_fkinstanc_pm_doc_i FOREIGN KEY (fkinstance) REFERENCES pm_doc_instance(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY pm_doc_registers
    ADD CONSTRAINT fk_pm_doc_r_fkregiste_pm_regis FOREIGN KEY (fkregister) REFERENCES pm_register(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY pm_doc_readers
    ADD CONSTRAINT fk_pm_doc_r_fkuser_isms_use FOREIGN KEY (fkuser) REFERENCES isms_user(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY pm_doc_read_history
    ADD CONSTRAINT fk_pm_doc_r_fkuser_isms_use2 FOREIGN KEY (fkuser) REFERENCES isms_user(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY pm_document
    ADD CONSTRAINT fk_pm_docum_fkauthor_isms_use FOREIGN KEY (fkauthor) REFERENCES isms_user(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY pm_document
    ADD CONSTRAINT fk_pm_docum_fkclassif_isms_con FOREIGN KEY (fkclassification) REFERENCES isms_context_classification(pkclassification) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY pm_document
    ADD CONSTRAINT fk_pm_docum_fkcontext_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY pm_document
    ADD CONSTRAINT fk_pm_docum_fkcurrent_pm_doc_i FOREIGN KEY (fkcurrentversion) REFERENCES pm_doc_instance(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY pm_document_revision_history
    ADD CONSTRAINT fk_pm_docum_fkdocumen_pm_docum FOREIGN KEY (fkdocument) REFERENCES pm_document(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY pm_document
    ADD CONSTRAINT fk_pm_docum_fkmainapp_isms_use FOREIGN KEY (fkmainapprover) REFERENCES isms_user(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY pm_document
    ADD CONSTRAINT fk_pm_docum_fkparent_pm_docum FOREIGN KEY (fkparent) REFERENCES pm_document(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY pm_document
    ADD CONSTRAINT fk_pm_docum_fkschedul_wkf_sche FOREIGN KEY (fkschedule) REFERENCES wkf_schedule(pkschedule) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY pm_instance_comment
    ADD CONSTRAINT fk_pm_insta_fkinstanc_pm_doc_i FOREIGN KEY (fkinstance) REFERENCES pm_doc_instance(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY pm_instance_content
    ADD CONSTRAINT fk_pm_insta_fkinstanc_pm_doc_i2 FOREIGN KEY (fkinstance) REFERENCES pm_doc_instance(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY pm_instance_comment
    ADD CONSTRAINT fk_pm_insta_fkuser_isms_use FOREIGN KEY (fkuser) REFERENCES isms_user(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY pm_process_user
    ADD CONSTRAINT fk_pm_proce_fkprocess_rm_proce FOREIGN KEY (fkprocess) REFERENCES rm_process(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY pm_process_user
    ADD CONSTRAINT fk_pm_proce_fkuser_isms_use FOREIGN KEY (fkuser) REFERENCES isms_user(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY pm_register
    ADD CONSTRAINT fk_pm_regis_fkclassif_isms_con FOREIGN KEY (fkclassification) REFERENCES isms_context_classification(pkclassification) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY pm_register
    ADD CONSTRAINT fk_pm_regis_fkcontext_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY pm_register
    ADD CONSTRAINT fk_pm_regis_fkdocumen_pm_docum FOREIGN KEY (fkdocument) REFERENCES pm_document(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY pm_register_readers
    ADD CONSTRAINT fk_pm_regis_fkregiste_pm_regis FOREIGN KEY (fkregister) REFERENCES pm_register(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY pm_register
    ADD CONSTRAINT fk_pm_regis_fkrespons_isms_use FOREIGN KEY (fkresponsible) REFERENCES isms_user(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY pm_register_readers
    ADD CONSTRAINT fk_pm_regis_fkuser_isms_use FOREIGN KEY (fkuser) REFERENCES isms_user(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY pm_template_best_practice
    ADD CONSTRAINT fk_pm_templ_fkbestpra_rm_best_ FOREIGN KEY (fkbestpractice) REFERENCES rm_best_practice(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY pm_template
    ADD CONSTRAINT fk_pm_templ_fkcontext_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY pm_template_content
    ADD CONSTRAINT fk_pm_templ_fkcontext_pm_templ FOREIGN KEY (fkcontext) REFERENCES pm_template(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY pm_template_best_practice
    ADD CONSTRAINT fk_pm_templ_fktemplat_pm_templ FOREIGN KEY (fktemplate) REFERENCES pm_template(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY rm_area
    ADD CONSTRAINT fk_rm_area_fkcontext_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY rm_area
    ADD CONSTRAINT fk_rm_area_fkparent_rm_area FOREIGN KEY (fkparent) REFERENCES rm_area(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY rm_area
    ADD CONSTRAINT fk_rm_area_fkpriority FOREIGN KEY (fkpriority) REFERENCES cm_priority(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY rm_area
    ADD CONSTRAINT fk_rm_area_fktype_isms_con FOREIGN KEY (fktype) REFERENCES isms_context_classification(pkclassification) ON UPDATE RESTRICT ON DELETE RESTRICT;

ALTER TABLE ONLY rm_area_provider
    ADD CONSTRAINT fkarea FOREIGN KEY (fkarea) REFERENCES rm_area (fkcontext) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE ONLY rm_area_provider
    ADD CONSTRAINT fkprovider FOREIGN KEY (fkprovider) REFERENCES cm_provider (fkcontext) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE ONLY rm_asset_asset
    ADD CONSTRAINT fk_rm_asset_fkasset_rm_asset FOREIGN KEY (fkasset) REFERENCES rm_asset(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY rm_asset_value
    ADD CONSTRAINT fk_rm_asset_fkasset_rm_asset2 FOREIGN KEY (fkasset) REFERENCES rm_asset(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY rm_asset
    ADD CONSTRAINT fk_rm_asset_fkcontext_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY rm_asset_asset
    ADD CONSTRAINT fk_rm_asset_fkdepende_rm_asset FOREIGN KEY (fkdependent) REFERENCES rm_asset(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY rm_asset_value
    ADD CONSTRAINT fk_rm_asset_fkparamet_rm_param FOREIGN KEY (fkparametername) REFERENCES rm_parameter_name(pkparametername) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY rm_asset_value
    ADD CONSTRAINT fk_rm_asset_fkvaluena_rm_param FOREIGN KEY (fkvaluename) REFERENCES rm_parameter_value_name(pkvaluename) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY rm_best_practice_standard
    ADD CONSTRAINT fk_rm_best__fkbestpra_rm_best_ FOREIGN KEY (fkbestpractice) REFERENCES rm_best_practice(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY rm_best_practice_event
    ADD CONSTRAINT fk_rm_best__fkbestpra_rm_best_2 FOREIGN KEY (fkbestpractice) REFERENCES rm_best_practice(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY rm_best_practice_standard
    ADD CONSTRAINT fk_rm_best__fkcontext_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY rm_best_practice_event
    ADD CONSTRAINT fk_rm_best__fkcontext_isms_con2 FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY rm_best_practice
    ADD CONSTRAINT fk_rm_best__fkcontext_isms_con3 FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY rm_best_practice_event
    ADD CONSTRAINT fk_rm_best__fkevent_rm_event FOREIGN KEY (fkevent) REFERENCES rm_event(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY rm_best_practice
    ADD CONSTRAINT fk_rm_best__fksection_rm_secti FOREIGN KEY (fksectionbestpractice) REFERENCES rm_section_best_practice(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY rm_best_practice_standard
    ADD CONSTRAINT fk_rm_best__fkstandar_rm_stand FOREIGN KEY (fkstandard) REFERENCES rm_standard(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY rm_category
    ADD CONSTRAINT fk_rm_categ_fkcontext_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY rm_category
    ADD CONSTRAINT fk_rm_categ_fkparent_rm_categ FOREIGN KEY (fkparent) REFERENCES rm_category(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY rm_control_best_practice
    ADD CONSTRAINT fk_rm_contr_fkbestpra_rm_best_ FOREIGN KEY (fkbestpractice) REFERENCES rm_best_practice(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY rm_control_best_practice
    ADD CONSTRAINT fk_rm_contr_fkcontext_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY rm_control
    ADD CONSTRAINT fk_rm_contr_fkcontext_isms_con2 FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY rm_control_best_practice
    ADD CONSTRAINT fk_rm_contr_fkcontrol_rm_contr FOREIGN KEY (fkcontrol) REFERENCES rm_control(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY rm_control_efficiency_history
    ADD CONSTRAINT fk_rm_contr_fkcontrol_rm_contr2 FOREIGN KEY (fkcontrol) REFERENCES rm_control(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY rm_control_test_history
    ADD CONSTRAINT fk_rm_contr_fkcontrol_rm_contr3 FOREIGN KEY (fkcontrol) REFERENCES rm_control(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY rm_control_cost
    ADD CONSTRAINT fk_rm_contr_fkcontrol_rm_contr4 FOREIGN KEY (fkcontrol) REFERENCES rm_control(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY rm_control
    ADD CONSTRAINT fk_rm_contr_fkrespons_isms_use FOREIGN KEY (fkresponsible) REFERENCES isms_user(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY rm_control
    ADD CONSTRAINT fk_rm_contr_fktype_isms_con FOREIGN KEY (fktype) REFERENCES isms_context_classification(pkclassification) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY rm_event
    ADD CONSTRAINT fk_rm_event_fkcategor_rm_categ FOREIGN KEY (fkcategory) REFERENCES rm_category(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY rm_event
    ADD CONSTRAINT fk_rm_event_fkcontext_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY rm_event
    ADD CONSTRAINT fk_rm_event_fktype_isms_con FOREIGN KEY (fktype) REFERENCES isms_context_classification(pkclassification) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY rm_process
    ADD CONSTRAINT fk_rm_proce_fkarea_rm_area FOREIGN KEY (fkarea) REFERENCES rm_area(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY rm_process_asset
    ADD CONSTRAINT fk_rm_proce_fkasset_rm_asset FOREIGN KEY (fkasset) REFERENCES rm_asset(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY rm_process_asset
    ADD CONSTRAINT fk_rm_proce_fkcontext_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY rm_process
    ADD CONSTRAINT fk_rm_proce_fkcontext_isms_con2 FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY rm_process_asset
    ADD CONSTRAINT fk_rm_proce_fkprocess_rm_proce FOREIGN KEY (fkprocess) REFERENCES rm_process(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY rm_risk_control
    ADD CONSTRAINT fk_rm_risk__fkcontext_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY rm_risk_limits
    ADD CONSTRAINT fk_rm_risk__fkcontext_isms_con2 FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY rm_risk_control
    ADD CONSTRAINT fk_rm_risk__fkcontrol_rm_contr FOREIGN KEY (fkcontrol) REFERENCES rm_control(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY rm_risk_control_value
    ADD CONSTRAINT fk_rm_risk__fkparamet_rm_param FOREIGN KEY (fkparametername) REFERENCES rm_parameter_name(pkparametername) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY rm_risk_value
    ADD CONSTRAINT fk_rm_risk__fkparamet_rm_param2 FOREIGN KEY (fkparametername) REFERENCES rm_parameter_name(pkparametername) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY rm_risk_control
    ADD CONSTRAINT fk_rm_risk__fkprobabi_rm_rc_pa FOREIGN KEY (fkprobabilityvaluename) REFERENCES rm_rc_parameter_value_name(pkrcvaluename) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY rm_risk_control_value
    ADD CONSTRAINT fk_rm_risk__fkrcvalue_rm_rc_pa FOREIGN KEY (fkrcvaluename) REFERENCES rm_rc_parameter_value_name(pkrcvaluename) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY rm_risk_control
    ADD CONSTRAINT fk_rm_risk__fkrisk_rm_risk FOREIGN KEY (fkrisk) REFERENCES rm_risk(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY rm_risk_value
    ADD CONSTRAINT fk_rm_risk__fkrisk_rm_risk2 FOREIGN KEY (fkrisk) REFERENCES rm_risk(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY rm_risk_control_value
    ADD CONSTRAINT fk_rm_risk__fkriskcon_rm_risk_ FOREIGN KEY (fkriskcontrol) REFERENCES rm_risk_control(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY rm_risk_value
    ADD CONSTRAINT fk_rm_risk__fkvaluena_rm_param FOREIGN KEY (fkvaluename) REFERENCES rm_parameter_value_name(pkvaluename) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY rm_risk
    ADD CONSTRAINT fk_rm_risk_fkasset_rm_asset FOREIGN KEY (fkasset) REFERENCES rm_asset(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY rm_risk
    ADD CONSTRAINT fk_rm_risk_fkcontext_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY rm_risk
    ADD CONSTRAINT fk_rm_risk_fkevent_rm_event FOREIGN KEY (fkevent) REFERENCES rm_event(fkcontext) ON UPDATE RESTRICT ON DELETE SET NULL;
ALTER TABLE ONLY rm_risk
    ADD CONSTRAINT fk_rm_risk_fkprobabi_rm_param FOREIGN KEY (fkprobabilityvaluename) REFERENCES rm_parameter_value_name(pkvaluename) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY rm_risk
    ADD CONSTRAINT fk_rm_risk_fktype_isms_con FOREIGN KEY (fktype) REFERENCES isms_context_classification(pkclassification) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY rm_section_best_practice
    ADD CONSTRAINT fk_rm_secti_fkcontext_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY rm_section_best_practice
    ADD CONSTRAINT fk_rm_secti_fkparent_rm_secti FOREIGN KEY (fkparent) REFERENCES rm_section_best_practice(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY rm_standard
    ADD CONSTRAINT fk_rm_stand_fkcontext_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY wkf_task_metadata
    ADD CONSTRAINT fk_task FOREIGN KEY (fktask) REFERENCES wkf_task(pktask);
ALTER TABLE ONLY wkf_alert
    ADD CONSTRAINT fk_wkf_aler_fkcontext_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY wkf_alert
    ADD CONSTRAINT fk_wkf_aler_fkcreator_isms_use FOREIGN KEY (fkcreator) REFERENCES isms_user(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY wkf_alert
    ADD CONSTRAINT fk_wkf_aler_fkreceive_isms_use FOREIGN KEY (fkreceiver) REFERENCES isms_user(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY wkf_control_test
    ADD CONSTRAINT fk_wkf_cont_fkcontrol_rm_contr FOREIGN KEY (fkcontroltest) REFERENCES rm_control(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY wkf_control_efficiency
    ADD CONSTRAINT fk_wkf_cont_fkcontrol_rm_contr2 FOREIGN KEY (fkcontrolefficiency) REFERENCES rm_control(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY wkf_control_test
    ADD CONSTRAINT fk_wkf_cont_fkschedul_wkf_sch2 FOREIGN KEY (fkschedule) REFERENCES wkf_schedule(pkschedule) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY wkf_control_efficiency
    ADD CONSTRAINT fk_wkf_cont_fkschedul_wkf_sche FOREIGN KEY (fkschedule) REFERENCES wkf_schedule(pkschedule) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY wkf_task_schedule
    ADD CONSTRAINT fk_wkf_task_fkcontext_isms_co2 FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY wkf_task
    ADD CONSTRAINT fk_wkf_task_fkcontext_isms_con FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY wkf_task
    ADD CONSTRAINT fk_wkf_task_fkcreator_isms_use FOREIGN KEY (fkcreator) REFERENCES isms_user(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY wkf_task
    ADD CONSTRAINT fk_wkf_task_fkreceive_isms_use FOREIGN KEY (fkreceiver) REFERENCES isms_user(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY wkf_task_schedule
    ADD CONSTRAINT fk_wkf_task_fkschedul_wkf_sche FOREIGN KEY (fkschedule) REFERENCES wkf_schedule(pkschedule) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY cm_plan_action
    ADD CONSTRAINT fkaction FOREIGN KEY (fkaction) REFERENCES cm_plan_action(fkcontext);
ALTER TABLE ONLY cm_process_activity_asset
    ADD CONSTRAINT fkactivity FOREIGN KEY (fkactivity) REFERENCES cm_process_activity(fkcontext);
ALTER TABLE ONLY cm_process_activity_people
    ADD CONSTRAINT fkactvity FOREIGN KEY (fkactivity) REFERENCES cm_process_activity(fkcontext);
ALTER TABLE ONLY cm_plan
    ADD CONSTRAINT fkarea FOREIGN KEY (fkarea) REFERENCES rm_area(fkcontext);
ALTER TABLE ONLY cm_area_resource
    ADD CONSTRAINT fkarea FOREIGN KEY (fkarea) REFERENCES rm_area(fkcontext);
ALTER TABLE ONLY cm_place_area
    ADD CONSTRAINT fkarea FOREIGN KEY (fkarea) REFERENCES rm_area(fkcontext);
ALTER TABLE ONLY cm_asset_threat
    ADD CONSTRAINT fkasset FOREIGN KEY (fkasset) REFERENCES rm_asset(fkcontext);
ALTER TABLE ONLY cm_process_activity_asset
    ADD CONSTRAINT fkasset FOREIGN KEY (fkasset) REFERENCES rm_asset(fkcontext);
ALTER TABLE ONLY cm_plan
    ADD CONSTRAINT fkasset FOREIGN KEY (fkasset) REFERENCES rm_asset(fkcontext);
ALTER TABLE ONLY cm_place_asset
    ADD CONSTRAINT fkasset FOREIGN KEY (fkasset) REFERENCES rm_asset(fkcontext);
ALTER TABLE ONLY cm_place
    ADD CONSTRAINT fkcmt FOREIGN KEY (fkcmt) REFERENCES cm_group(fkcontext);
ALTER TABLE ONLY ci_incident_risk_parameter
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_asset_importance
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_asset_threat
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_threat_probability
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_damage_matrix_parameter
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_impact_type
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_process_activity
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_process_outputs
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_place_process
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_damage_matrix
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_process_activity_people
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_process_activity_asset
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_process_threat
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_financial_impact
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_impact
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_impact_damage
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_impact_damage_range
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_priority
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_group
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_resource
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_group_resource
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_plan_range_type
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_plan_range
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_plan
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_plan_action
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_place_asset
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_place_resource
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_area_resource
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_provider
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_provider_resource
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_department
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_threat_vulnerability
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY cm_plan_test
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_plan_action_test
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_plan_schedule
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_place_area
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_place_provider
    ADD CONSTRAINT fkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_scope_process
    ADD CONSTRAINT fkcontext_cm_scope FOREIGN KEY (fkscope) REFERENCES cm_scope(fkcontext);
ALTER TABLE ONLY cm_scope_place
    ADD CONSTRAINT fkcontext_cm_scope FOREIGN KEY (fkscope) REFERENCES cm_scope(fkcontext);
ALTER TABLE ONLY cm_scope_area
    ADD CONSTRAINT fkcontext_cm_scope FOREIGN KEY (fkscope) REFERENCES cm_scope(fkcontext);
ALTER TABLE ONLY cm_scope_asset
    ADD CONSTRAINT fkcontext_cm_scope FOREIGN KEY (fkscope) REFERENCES cm_scope(fkcontext);
ALTER TABLE ONLY cm_scope
    ADD CONSTRAINT fkcontext_scope FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_scope_process
    ADD CONSTRAINT fkcontext_scope FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_scope_place
    ADD CONSTRAINT fkcontext_scope FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_scope_area
    ADD CONSTRAINT fkcontext_scope FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_scope_asset
    ADD CONSTRAINT fkcontext_scope FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_impact_damage_range
    ADD CONSTRAINT fkdamage FOREIGN KEY (fkdamage) REFERENCES cm_damage_matrix(fkcontext);
ALTER TABLE ONLY cm_impact_damage_range
    ADD CONSTRAINT fkdamageparameter FOREIGN KEY (fkdamageparameter) REFERENCES cm_damage_matrix_parameter(fkcontext);
ALTER TABLE ONLY cm_resource
    ADD CONSTRAINT fkdepartment FOREIGN KEY (fkdepartment) REFERENCES cm_department(fkcontext);
ALTER TABLE ONLY cm_group_resource
    ADD CONSTRAINT fkgroup FOREIGN KEY (fkgroup) REFERENCES cm_group(fkcontext);
ALTER TABLE ONLY cm_plan_action
    ADD CONSTRAINT fkgroup FOREIGN KEY (fkgroup) REFERENCES cm_group(fkcontext);
ALTER TABLE ONLY rm_asset
    ADD CONSTRAINT fkgroup FOREIGN KEY (fkgroup) REFERENCES cm_group(fkcontext);
ALTER TABLE ONLY cm_process_activity
    ADD CONSTRAINT fkgroup FOREIGN KEY (fkgroup) REFERENCES cm_group(fkcontext);
ALTER TABLE ONLY rm_process
    ADD CONSTRAINT fkgroup FOREIGN KEY (fkgroup) REFERENCES cm_group(fkcontext);
ALTER TABLE ONLY cm_place
    ADD CONSTRAINT fkgroup FOREIGN KEY (fkgroup) REFERENCES cm_group(fkcontext);
ALTER TABLE ONLY rm_area
    ADD CONSTRAINT fkgroup FOREIGN KEY (fkgroup) REFERENCES cm_group(fkcontext);
ALTER TABLE ONLY cm_place_resource
    ADD CONSTRAINT fkgroup FOREIGN KEY (fkgroup) REFERENCES cm_group(fkcontext);
ALTER TABLE ONLY cm_area_resource
    ADD CONSTRAINT fkgroup FOREIGN KEY (fkgroup) REFERENCES cm_group(fkcontext);
ALTER TABLE ONLY cm_plan_schedule
    ADD CONSTRAINT fkgroup FOREIGN KEY (fkgroup) REFERENCES cm_group(fkcontext);
ALTER TABLE ONLY rm_process
    ADD CONSTRAINT fkgroupsubstitute FOREIGN KEY (fkgroupsubstitute) REFERENCES cm_group(fkcontext);
ALTER TABLE ONLY cm_place
    ADD CONSTRAINT fkgroupsubstitute FOREIGN KEY (fkgroupsubstitute) REFERENCES cm_group(fkcontext);
ALTER TABLE ONLY rm_area
    ADD CONSTRAINT fkgroupsubstitute FOREIGN KEY (fkgroupsubstitute) REFERENCES cm_group(fkcontext);
ALTER TABLE ONLY cm_impact_damage
    ADD CONSTRAINT fkimpact FOREIGN KEY (fkimpact) REFERENCES cm_impact(fkcontext);
ALTER TABLE ONLY cm_impact_damage_range
    ADD CONSTRAINT fkimpactdamage FOREIGN KEY (fkimpactdamage) REFERENCES cm_impact_damage(fkcontext);
ALTER TABLE ONLY cm_process_activity
    ADD CONSTRAINT fkimportance FOREIGN KEY (fkimportance) REFERENCES cm_priority(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY rm_process_asset
    ADD CONSTRAINT fkimportance FOREIGN KEY (fkimportance) REFERENCES cm_priority(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY cm_process_activity_asset
    ADD CONSTRAINT fkimportance FOREIGN KEY (fkimportance) REFERENCES cm_priority(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY ci_incident_risk_parameter
    ADD CONSTRAINT fkincident FOREIGN KEY (fkincident) REFERENCES ci_incident(fkcontext);
ALTER TABLE ONLY cm_threat_vulnerability
    ADD CONSTRAINT fklevel FOREIGN KEY (fklevel) REFERENCES cm_priority(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY cm_plan_action
    ADD CONSTRAINT fkotherplan FOREIGN KEY (fkotherplan) REFERENCES cm_plan(fkcontext);
ALTER TABLE ONLY cm_place
    ADD CONSTRAINT fkparent FOREIGN KEY (fkparent) REFERENCES cm_place(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY cm_place_process
    ADD CONSTRAINT fkplace FOREIGN KEY (fkplace) REFERENCES cm_place(fkcontext);
ALTER TABLE ONLY cm_place_threat
    ADD CONSTRAINT fkplace FOREIGN KEY (fkplace) REFERENCES cm_place(fkcontext);
ALTER TABLE ONLY cm_plan
    ADD CONSTRAINT fkplace FOREIGN KEY (fkplace) REFERENCES cm_place(fkcontext);
ALTER TABLE ONLY cm_place_asset
    ADD CONSTRAINT fkplace FOREIGN KEY (fkplace) REFERENCES cm_place(fkcontext);
ALTER TABLE ONLY rm_area
    ADD CONSTRAINT fkplace FOREIGN KEY (fkplace) REFERENCES cm_place(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY cm_place_resource
    ADD CONSTRAINT fkplace FOREIGN KEY (fkplace) REFERENCES cm_place(fkcontext);
ALTER TABLE ONLY cm_threat_vulnerability
    ADD CONSTRAINT fkplace FOREIGN KEY (fkplace) REFERENCES cm_place(fkcontext) ON UPDATE RESTRICT ON DELETE CASCADE;
ALTER TABLE ONLY rm_asset
    ADD CONSTRAINT fkplace FOREIGN KEY (fkplace) REFERENCES cm_place(fkcontext);
ALTER TABLE ONLY cm_place_area
    ADD CONSTRAINT fkplace FOREIGN KEY (fkplace) REFERENCES cm_place(fkcontext);
ALTER TABLE ONLY cm_place_provider
    ADD CONSTRAINT fkplace FOREIGN KEY (fkplace) REFERENCES cm_place(fkcontext);
ALTER TABLE ONLY cm_plan_action
    ADD CONSTRAINT fkplan FOREIGN KEY (fkplan) REFERENCES cm_plan(fkcontext);
ALTER TABLE ONLY cm_plan_test
    ADD CONSTRAINT fkplan FOREIGN KEY (fkplan) REFERENCES cm_plan(fkcontext);
ALTER TABLE ONLY cm_plan_schedule
    ADD CONSTRAINT fkplan FOREIGN KEY (fkplan) REFERENCES cm_plan(fkcontext);
ALTER TABLE ONLY cm_plan_action_test
    ADD CONSTRAINT fkplanaction FOREIGN KEY (fkplanaction) REFERENCES cm_plan_action(fkcontext);
ALTER TABLE ONLY cm_plan_action
    ADD CONSTRAINT fkplanrange FOREIGN KEY (fkplanrange) REFERENCES cm_plan_range(fkcontext);
ALTER TABLE ONLY cm_plan_action_test
    ADD CONSTRAINT fkplantest FOREIGN KEY (fkplantest) REFERENCES cm_plan_test(fkcontext);
ALTER TABLE ONLY cm_plan_action
    ADD CONSTRAINT fkplantype FOREIGN KEY (fkplantype) REFERENCES cm_plan_range_type(fkcontext);
ALTER TABLE ONLY cm_group
    ADD CONSTRAINT fkpriority FOREIGN KEY (fkpriority) REFERENCES cm_priority(fkcontext);
ALTER TABLE ONLY cm_process_activity
    ADD CONSTRAINT fkpriority FOREIGN KEY (fkpriority) REFERENCES cm_priority(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY rm_process
    ADD CONSTRAINT fkpriority FOREIGN KEY (fkpriority) REFERENCES cm_priority(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY cm_scene
    ADD CONSTRAINT fkprobability FOREIGN KEY (fkprobability) REFERENCES cm_priority(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY cm_place_process
    ADD CONSTRAINT fkprocess FOREIGN KEY (fkprocess) REFERENCES rm_process(fkcontext);
ALTER TABLE ONLY cm_scene
    ADD CONSTRAINT fkprocess FOREIGN KEY (fkprocess) REFERENCES rm_process(fkcontext);
ALTER TABLE ONLY cm_process_activity
    ADD CONSTRAINT fkprocess FOREIGN KEY (fkprocess) REFERENCES rm_process(fkcontext);
ALTER TABLE ONLY cm_process_threat
    ADD CONSTRAINT fkprocess FOREIGN KEY (fkprocess) REFERENCES rm_process(fkcontext);
ALTER TABLE ONLY cm_plan
    ADD CONSTRAINT fkprocess FOREIGN KEY (fkprocess) REFERENCES rm_process(fkcontext);
ALTER TABLE ONLY cm_provider_resource
    ADD CONSTRAINT fkprovider FOREIGN KEY (fkprovider) REFERENCES cm_provider(fkcontext);
ALTER TABLE ONLY rm_asset
    ADD CONSTRAINT fkprovider FOREIGN KEY (fkprovider) REFERENCES cm_provider(fkcontext);
ALTER TABLE ONLY cm_place_provider
    ADD CONSTRAINT fkprovider FOREIGN KEY (fkprovider) REFERENCES cm_provider(fkcontext);
ALTER TABLE ONLY cm_group_resource
    ADD CONSTRAINT fkresource FOREIGN KEY (fkresource) REFERENCES cm_resource(fkcontext);
ALTER TABLE ONLY rm_process
    ADD CONSTRAINT fkresource FOREIGN KEY (fkresource) REFERENCES cm_resource(fkcontext);
ALTER TABLE ONLY cm_place
    ADD CONSTRAINT fkresource FOREIGN KEY (fkresource) REFERENCES cm_resource(fkcontext);
ALTER TABLE ONLY rm_area
    ADD CONSTRAINT fkresource FOREIGN KEY (fkresource) REFERENCES cm_resource(fkcontext);
ALTER TABLE ONLY cm_place_resource
    ADD CONSTRAINT fkresource FOREIGN KEY (fkresource) REFERENCES cm_resource(fkcontext);
ALTER TABLE ONLY cm_area_resource
    ADD CONSTRAINT fkresource FOREIGN KEY (fkresource) REFERENCES cm_resource(fkcontext);
ALTER TABLE ONLY cm_provider_resource
    ADD CONSTRAINT fkresource FOREIGN KEY (fkresource) REFERENCES cm_resource(fkcontext);
ALTER TABLE ONLY rm_process
    ADD CONSTRAINT fkresourcesubstitute FOREIGN KEY (fkresourcesubstitute) REFERENCES cm_resource(fkcontext);
ALTER TABLE ONLY cm_place
    ADD CONSTRAINT fkresourcesubstitute FOREIGN KEY (fkresourcesubstitute) REFERENCES cm_resource(fkcontext);
ALTER TABLE ONLY rm_area
    ADD CONSTRAINT fkresourcesubstitute FOREIGN KEY (fkresourcesubstitute) REFERENCES cm_resource(fkcontext);
ALTER TABLE ONLY cm_process_activity
    ADD CONSTRAINT fkresponsible FOREIGN KEY (fkresponsible) REFERENCES isms_user(fkcontext);
ALTER TABLE ONLY cm_plan_test
    ADD CONSTRAINT fkresponsible FOREIGN KEY (fkresponsible) REFERENCES cm_resource(fkcontext);
ALTER TABLE ONLY cm_plan_schedule
    ADD CONSTRAINT fkresponsible FOREIGN KEY (fkresponsible) REFERENCES cm_resource(fkcontext);
ALTER TABLE ONLY cm_impact_damage
    ADD CONSTRAINT fkscene FOREIGN KEY (fkscene) REFERENCES cm_scene(fkcontext);
ALTER TABLE ONLY rm_process
    ADD CONSTRAINT fkseasonality FOREIGN KEY (fkseasonality) REFERENCES wkf_schedule(pkschedule) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY cm_scene
    ADD CONSTRAINT fkseasonality FOREIGN KEY (fkseasonality) REFERENCES wkf_schedule(pkschedule) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY cm_plan_schedule
    ADD CONSTRAINT fktest FOREIGN KEY (fktest) REFERENCES cm_plan_test(fkcontext);
ALTER TABLE ONLY cm_threat
    ADD CONSTRAINT fkthreat FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_asset_threat
    ADD CONSTRAINT fkthreat FOREIGN KEY (fkthreat) REFERENCES cm_threat(fkcontext);
ALTER TABLE ONLY cm_place_threat
    ADD CONSTRAINT fkthreat FOREIGN KEY (fkthreat) REFERENCES cm_threat(fkcontext);
ALTER TABLE ONLY cm_process_threat
    ADD CONSTRAINT fkthreat FOREIGN KEY (fkthreat) REFERENCES cm_threat(fkcontext);
ALTER TABLE ONLY cm_threat_vulnerability
    ADD CONSTRAINT fkthreat FOREIGN KEY (fkthreat) REFERENCES cm_threat(fkcontext) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY cm_plan_range
    ADD CONSTRAINT fktype FOREIGN KEY (fktype) REFERENCES cm_plan_range_type(fkcontext);
ALTER TABLE ONLY rm_process
    ADD CONSTRAINT fktype FOREIGN KEY (fktype) REFERENCES isms_context_classification(pkclassification) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY cm_process_activity_people
    ADD CONSTRAINT fkuser FOREIGN KEY (fkuser) REFERENCES isms_user(fkcontext);
ALTER TABLE ONLY cm_process_inputs
    ADD CONSTRAINT gc_process_inputs_fk_process_fkey FOREIGN KEY (fk_process) REFERENCES rm_process(fkcontext);
ALTER TABLE ONLY cm_process_inputs
    ADD CONSTRAINT gc_process_inputs_fk_process_input_fkey FOREIGN KEY (fk_process_input) REFERENCES rm_process(fkcontext);
ALTER TABLE ONLY cm_threat
    ADD CONSTRAINT nprobability FOREIGN KEY (nprobability) REFERENCES cm_threat_probability(fkcontext);
ALTER TABLE ONLY ci_incident_risk_parameter
    ADD CONSTRAINT parameter_name FOREIGN KEY (fkparametername) REFERENCES rm_parameter_name(pkparametername);
ALTER TABLE ONLY ci_incident_risk_parameter
    ADD CONSTRAINT parameter_value FOREIGN KEY (fkparametervalue) REFERENCES rm_parameter_value_name(pkvaluename);
ALTER TABLE ONLY cm_scene
    ADD CONSTRAINT pkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_place_threat
    ADD CONSTRAINT pkcontext FOREIGN KEY (fkcontext) REFERENCES isms_context(pkcontext);
ALTER TABLE ONLY cm_process_outputs
    ADD CONSTRAINT process_fk FOREIGN KEY (fk_process) REFERENCES rm_process(fkcontext);
ALTER TABLE ONLY cm_process_outputs
    ADD CONSTRAINT process_output_fk FOREIGN KEY (fk_process_output) REFERENCES rm_process(fkcontext);
ALTER TABLE ONLY cm_scope_area
    ADD CONSTRAINT rm_area_fkcontext FOREIGN KEY (fkarea) REFERENCES rm_area(fkcontext);
ALTER TABLE ONLY cm_scope_asset
    ADD CONSTRAINT rm_asset_fkcontext FOREIGN KEY (fkasset) REFERENCES rm_asset(fkcontext);
ALTER TABLE ONLY cm_scope_process
    ADD CONSTRAINT rm_process_fkcontext FOREIGN KEY (fkprocess) REFERENCES rm_process(fkcontext);
REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;
