100#NULL#Information#Information
101#100#Digital Information#Digital Information
102#101#Personal (Client)#Personal (Client)
103#101#Personal (Employees)#Personal (Employees)
104#101#Financial#Financial
105#101#Legal#Legal
106#101#Research#Research
107#101#Strategy and Business#Strategy and Business
108#101#Database#Database
109#101#Electronic Mail#Electronic Mail
115#100#Paper Documents#Paper Documents
116#115#Personal (Client)#Personal (Client)
117#115#Personal (Employees)#Personal (Employees)
118#115#Financial#Financial
119#115#Legal#Legal
120#115#Research#Research
121#115#Strategy and Business#Strategy and Business
126#115#Commercial#Commercial
127#115#FAX#FAX
128#NULL#Software Assets#Software Assets
129#128#Operational Systems#Operational Systems
130#129#Windows (Desktop)#Windows (Desktop)
131#129#Windows (Server)#Windows (Server)
132#129#Windows Mobile #Windows Mobile 
133#129#MacOS#MacOS
134#129#Linux (Desktop)#Linux (Desktop)
135#129#Linux (Server)#Linux (Server)
136#129#Unix#Unix
137#129#Solaris#Solaris
139#138#Database#Database
140#139#Oracle#Oracle
141#139#Postgre#Postgre
142#139#MySQL#MySQL
143#139#DB2#DB2
144#139#Data Warehouse#Data Warehouse
152#138#Collaboration Systems#Collaboration Systems
153#152#Lotus Notes#Lotus Notes
154#152#Microsoft Exchange#Microsoft Exchange
155#152#Corporate WIKI#Corporate WIKI
156#152#Portal Collaboration#Portal Collaboration
157#138#Corporate System#Corporate System
158#157#ERP (Enterprise Resource Planning)#ERP (Enterprise Resource Planning)
159#157#CRM (Costumer Relationship Management)#CRM (Costumer Relationship Management)
160#157#E-learning#E-learning
161#157#DMS (Document Management System)#DMS (Document Management System)
162#157#BI (Business Intelligence)#BI (Business Intelligence)
167#138#Names Resolution Software  (DNS, WINS e etc)#Names Resolution Software  (DNS, WINS e etc)
168#138#Security Software#Security Software
169#168#Anti-virus#Anti-virus
170#168#Firewall#Firewall
171#168#IDS (Intrusion Detection System)#IDS (Intrusion Detection System)
172#168#IPS (Intrusion Prevention System)#IPS (Intrusion Prevention System)
173#138#Communication Software#Communication Software
174#173#Telephone Exchange Software #Telephone Exchange Software 
175#173#Central VoIP#Central VoIP
176#173#Instant Messaging#Instant Messaging
177#138#Web Systems#Web Systems
178#177#Intranet#Intranet
179#177#Webmail#Webmail
180#138#File Sharing Software#File Sharing Software
181#138#Management Software#Management Software
182#NULL#Physical Assets#Physical Assets
183#182#Infrastructure#Infrastructure
184#183#Building#Building
185#183#Data Center#Data Center
186#183#Environment  Control center#Environment  Control Center
187#183#Server Room#Server Room
188#183#Store Room#Store Room
189#183#Distribution Center#Distribution Center
190#183#Data Processing Room#Data Processing Room
191#183#Data Warehouse#Data Warehouse
192#183#Operations Room#Operations Room
193#183#Office#Office
123#115#Mail#Mail
203#182#Environmental Components#Environmental Components
204#203#Electric System#Electric System
205#203#No Break#No Break
206#203#Lighting#Lighting
207#203#Cabling#Cabling
209#208#Storage Devices#Storage Devices
210#209#CD#CD
211#209#Diskette#Diskette
212#209#HD External#HD External
213#208#Processing Devices#Processing Devices
222#221#Router#Router
223#221#Switch#Switch
224#221#Access Point (WiFi)#Access Point (WiFi)
228#NULL#Services#Services
229#228# Computer Services# Computer Services
230#235#E-mail Services#E-mail Services
231#229#File Storage Service#File Storage Service
233#229#Authentication Service#Authentication Service
234#229#Print Service#Print Service
235#228#Communication Services#Communication Services
236#235#Network Service (Internal)#Network Service (Internal)
237#235#Remote Access Service#Remote Access Service
238#235#Remote Administration Service#Remote Administration Service
239#235#External Links#External Links
240#235#Telephony Service#Telephony Service
241#228#Management Service#Management Service
244#228#Transport and Maintenance Service#Transport and Maintenance Service
245#244#Equipment  Transport Service#Equipment  Transport Service
246#244#Information Transport Service#Information Transport Service
247#228#Accounting Service#Accounting Service
248#228#Marketing Service#Marketing Service
249#228#Law Service#Law Service
250#228#Cleaning Service#Cleaning Service
251#228#IT Services#IT Services
252#228#General Utilities#General Utilities
253#252#Electrical Energy / Electrical System#Electrical Energy / Electrical System
254#252#Environment Conditioning Service#Environment Conditioning Service
255#NULL#People#People
256#255#Employees#Employees
257#256#Adviser#Adviser
258#256#Director#Director
259#256#Auditor#Auditor
260#256# Consultant# Consultant
261#256#Manager#Manager
262#256#Analyst#Analyst
263#256#Developer#Developer
264#256#Trainee#Trainee
265#256#IT Employee#IT Employee
266#256#Security Officer#Security Officer
267#256#DBA#DBA
268#256#Administrator (TI)#Administrator (TI)
269#255#Third Party#Third Party
270#255#Supplier#Supplier
271#255#Service Provider#Service Provider
272#NULL#Intangible Assets#Intangible Assets
273#272#Company Image#Company Image
274#272# Competitive Advantage# Competitive Advantage
275#272#Credibility#Credibility
277#272#Ability to Provide Services#Ability to Provide Services
279#272#Organization#Organization
280#272#Knowledge#Knowledge
114#101#Backup Information#Backup Information
113#101#E-Commerce Data#E-Commerce Data
111#101#Manual Documentation#Manual Documentation
110#101#Record#Record
125#115#Manual Documentation#Manual Documentation
124#115#Record#Record
208#182#Equipment/ Devices#Equipment/ Devices
226#208#Copier/Scanner/Printer#Copier/Scanner/Printer
227#208#Display Devices#Display Devices
221#208#Network/Communications Devices#Network/Communications Devices
6197#213#Development Server#Development Server
6198#213#Workstation#Workstation
6199#213#Mobile Devices#Mobile Devices
6200#213#Mainframe#Mainframe
6201#213#Production Server#Production Server
6202#209#Tape#Tape
232#229#Data Processing Service#Data Processing Service
138#128#Information Systems#Information Systems
6203#128#Applications#Applications
6204#6203#Browser#Browser
6205#6203#Communication Application#Communication Application
6206#6203#Development Tools#Development Tools
6207#6203#E-mail Client#E-mail Client
6208#6203#Engineering Application#Engineering Application
6209#6203#Office Application#Office Application