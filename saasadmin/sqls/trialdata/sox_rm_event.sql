10008#NULL#10007#Deficiencies found during ongoing monitoring are not communicated to at least one level of management above the individual responsible for the function.#NULL#0#NULL
10009#NULL#10007#Deficiencies found during ongoing monitoring are not communicated to the individual responsible for the function.#NULL#0#NULL
10010#NULL#10007#Deficiencies found during separate evaluations are not communicated to at least one level of management above the individual responsible for the function.#NULL#0#NULL
10011#NULL#10007#Deficiencies found during separate evaluations are not communicated to the individual responsible for the function.#NULL#0#NULL
10012#NULL#10007#Employees do not understand the Code of Conduct.#NULL#0#NULL
10013#NULL#10007#Employees ignore the Code of Conduct.#NULL#0#NULL
10014#NULL#10007#Hostile takeover.#NULL#0#NULL
10015#NULL#10007#Inaccurate, untimely or unavailable information regarding pricing, products, actual or potential customers, advertising and promotion.#NULL#0#NULL
10016#NULL#10007#Inadequate and out-dated planning systems.#NULL#0#NULL
10017#NULL#10007#Inadequate attention to relationships with shareholders, investors or other outside parties.#NULL#0#NULL
10018#NULL#10007#Inadequate information regarding factors that may influence the entity's marketing strategy.#NULL#0#NULL
10019#NULL#10007#Inadequate management information systems.#NULL#0#NULL
10020#NULL#10007#Information does not generally flow down, across, and up organization.#NULL#0#NULL
10021#NULL#10007#Insufficient information regarding available opportunities.#NULL#0#NULL
10022#NULL#10007#Insufficient interaction of information technology, financial and operating management in developing strategic plans.#NULL#0#NULL
10023#NULL#10007#Integrity and ethical values are not maintained and demonstrated by management and staff.#NULL#0#NULL
10024#NULL#10007#Lack of awareness of entity-wide objectives.#NULL#0#NULL
10025#NULL#10007#Lack of awareness of sales and marketing objectives.#NULL#0#NULL
10026#NULL#10007#Lack of Code of Conduct.#NULL#0#NULL
10027#NULL#10007#Lack of product demand.#NULL#0#NULL
10028#NULL#10007#Lack of understanding of critical success factors.#NULL#0#NULL
10029#NULL#10007#Employees may not feel their efforts are noticed or appreciated.#NULL#0#NULL
10030#NULL#10007#Due dates and relative priorities of management reports are not clarified or communicated.#NULL#0#NULL
10031#NULL#10007#Lack of understanding of government policies.#NULL#0#NULL
10032#NULL#10007#Leakage of strategic information.#NULL#0#NULL
10033#NULL#10007#Limited number of appropriate distributors.#NULL#0#NULL
10034#NULL#10007#Limited number of positions.#NULL#0#NULL
10035#NULL#10007#Management does not assess risk significance.#NULL#0#NULL
10036#NULL#10007#Management does not assess the likelihood of risk occurrence.#NULL#0#NULL
10037#NULL#10007#Management does not assess what actions should be taken to mitigate risks.#NULL#0#NULL
10038#NULL#10007#Management does not compare actual performance to planned or expected results and analyze significant differences.#NULL#0#NULL
10039#NULL#10007#Management does not complete, within established timeframes, all actions that correct or otherwise resolve the matters brought to management's attention.#NULL#0#NULL
10040#NULL#10007#Management does not continually assess personnel skills.#NULL#0#NULL
10041#NULL#10007#Management does not determine the proper actions in response to findings and recommendations from audits and other reviews.#NULL#0#NULL
10042#NULL#10007#Management does not effectively manage its workforce.#NULL#0#NULL
10043#NULL#10007#Management does not have a good relationship with central oversight agencies such as OMB.#NULL#0#NULL
10044#NULL#10007#Management does not have a good relationship with Congress.#NULL#0#NULL
10045#NULL#10007#Management does not have a good relationship with the OIG.#NULL#0#NULL
10046#NULL#10007#Management does not have access to information relating to current technological developments.#NULL#0#NULL
10047#NULL#10007#Management does not have access to operational and/or financial data.#NULL#0#NULL
10048#NULL#10007#Management does not have mechanisms in place to identify and deal with risks that are unique to the operating environment of the Government.#NULL#0#NULL
10049#NULL#10007#Management does not hire the right personnel for the job.#NULL#0#NULL
10050#NULL#10007#Management does not maintain effective controls over information processing.#NULL#0#NULL
10051#NULL#10007#Management does not manage risk.#NULL#0#NULL
10052#NULL#10007#Organization does not have documented procedures in place to delegate authority and responsibility.#NULL#0#NULL
10053#NULL#10007#Management does not plan for personnel replacement.#NULL#0#NULL
10054#NULL#10007#Organization does not have procedures in place to delegate authority and responsibility.#NULL#0#NULL
10055#NULL#10007#Management does not play a key role in providing leadership in the area of integrity and ethical values.#NULL#0#NULL
10056#NULL#10007#Organization does not maintain physical control of, and properly secures/safeguard, vulnerable assets.#NULL#0#NULL
10057#NULL#10007#Management does not promptly evaluate findings from audits and other reviews.#NULL#0#NULL
10058#NULL#10007#Organization does not provide a proper amount of supervision.#NULL#0#NULL
10059#NULL#10007#Management does not provide adequate incentives for the personnel to ensure job success.#NULL#0#NULL
10060#NULL#10007#Organization does not provide guidance for disciplinary actions, when appropriate.#NULL#0#NULL
10061#NULL#10007#Organization does not provide guidance for proper behavior influences the quality of internal control.#NULL#0#NULL
10062#NULL#10007#Management does not provide adequate training for the personnel to ensure job success.#NULL#0#NULL
10063#NULL#10007#Organization does not provide guidance for removing temptations for unethical behavior.#NULL#0#NULL
10064#NULL#10007#Management does not provide candid and constructive counseling.#NULL#0#NULL
10065#NULL#10007#Organization has not established, and/or does not regularly review, performance measures and indicators.#NULL#0#NULL
10066#NULL#10007#Management does not provide needed training.#NULL#0#NULL
10067#NULL#10007#Organization structure does not define key areas of authority and responsibility.#NULL#0#NULL
10068#NULL#10007#Management does not provide performance appraisals.#NULL#0#NULL
10069#NULL#10007#Organization structure does not establish appropriate lines of reporting.#NULL#0#NULL
10070#NULL#10007#Organizational structure does not provide a framework for planning, directing, and controlling operations to achieve agency objectives.#NULL#0#NULL
10071#NULL#10007#Management does not provide qualified and continuous supervision of internal control objectives.#NULL#0#NULL
10072#NULL#10007#Organization does not have appropriate practices for disciplining personnel.#NULL#0#NULL
10143#NULL#10135#Legal counsel does not review contracts or agreements.#NULL#0#NULL
10073#NULL#10007#Plan formats are ineffective in providing necessary benchmarks against which performance can be measured.#NULL#0#NULL
10074#NULL#10007#Management does not retain valuable personnel.#NULL#0#NULL
10075#NULL#10007#Management does not track major organizational achievements.#NULL#0#NULL
10076#NULL#10007#Management has a dismissive attitude toward accounting functions.#NULL#0#NULL
10077#NULL#10007#Policies and procedures have not been established for ensuring that findings of audits and other reviews are promptly resolved.#NULL#0#NULL
10078#NULL#10007#Management has a dismissive attitude toward audits and evaluations.#NULL#0#NULL
10079#NULL#10007#Management has a dismissive attitude toward information systems.#NULL#0#NULL
10080#NULL#10007#Management has a dismissive attitude toward monitoring functions activities.#NULL#0#NULL
10081#NULL#10007#Pre-established standards are not determined.#NULL#0#NULL
10082#NULL#10007#Products become obsolete.#NULL#0#NULL
10083#NULL#10007#Management has a dismissive attitude toward personnel functions.#NULL#0#NULL
10084#NULL#10007#Relationships between oversight agencies and management do not exist.#NULL#0#NULL
10085#NULL#10007#Management has not adopted performance-based management.#NULL#0#NULL
10086#NULL#10007#Sales personnel are unaware of potential customers#NULL#0#NULL
10087#NULL#10007#Management has not comprehensively identified risks at the entity-level.#NULL#0#NULL
10088#NULL#10007#Management has not considered all significant interactions between individual activities and other parties.#NULL#0#NULL
10089#NULL#10007#Management has not considered all significant interactions between the entity and other parties.#NULL#0#NULL
10090#NULL#10007#Management has not considered all significant internal factors at the activity-level.#NULL#0#NULL
10091#NULL#10007#Organization delegation procedures do not cover authority and responsibility for operating activities.#NULL#0#NULL
10092#NULL#10007#Management has not considered all significant internal factors at the entity-level.#NULL#0#NULL
10093#NULL#10007#Organization delegation procedures do not cover authority and responsibility for reporting relationships.#NULL#0#NULL
10094#NULL#10007#Management has not established clear, consistent Agency objectives.#NULL#0#NULL
10095#NULL#10007#Organization does not have a corporate information systems architecture.#NULL#0#NULL
10096#NULL#10007#Organization does not have appropriate practices for compensating personnel.#NULL#0#NULL
10097#NULL#10007#Management has not established clear, consistent objectives at the activity-level.#NULL#0#NULL
10098#NULL#10007#Organization does not have appropriate practices for counseling personnel.#NULL#0#NULL
10099#NULL#10007#Management has not recently revisited the sufficiency of disaster recovery plans.#NULL#0#NULL
10100#NULL#10007#Organization does not have appropriate practices for evaluating personnel.#NULL#0#NULL
10101#NULL#10007#Management is unaware of legal and regulatory requirements.#NULL#0#NULL
10102#NULL#10007#Organization does not have appropriate practices for hiring personnel.#NULL#0#NULL
10103#NULL#10007#Management is unaware of valuable assets.#NULL#0#NULL
10104#NULL#10007#Organization does not have appropriate practices for orienting personnel.#NULL#0#NULL
10105#NULL#10007#Management or supervisory personnel ignore legal and regulatory requirements or company policies.#NULL#0#NULL
10106#NULL#10007#Organization does not have appropriate practices for promoting personnel.#NULL#0#NULL
10107#NULL#10007#Management takes too much risk.#NULL#0#NULL
10108#NULL#10007#Organization does not have appropriate practices for training personnel.#NULL#0#NULL
10109#NULL#10007#Mismatch in policy and knowledge about marketing and financial strategies.#NULL#0#NULL
10110#NULL#10007#Monitoring efforts do not include comparisons, reconciliations, and other actions that people take in performing their duties.#NULL#0#NULL
10111#NULL#10007#Monitoring efforts do not include regular management and supervisory activities.#NULL#0#NULL
10112#NULL#10007#No adequate strategy in place.#NULL#0#NULL
10113#NULL#10007#Non added value based acquisition.#NULL#0#NULL
10114#NULL#10007#Not meeting targets.#NULL#0#NULL
10115#NULL#10007#Ongoing monitoring does not occur in the course of normal operations.#NULL#0#NULL
10116#NULL#10007#Organization delegation procedures do not cover authority and responsibility for authorization protocols.#NULL#0#NULL
10117#NULL#10007#Separate evaluations of controls do not focus directly on their effectiveness at a specific time.#NULL#0#NULL
10118#NULL#10007#Serious matters found during ongoing monitoring are not reported to top management.#NULL#0#NULL
10119#NULL#10007#Wrong information as basis for decision making; competition risk.#NULL#0#NULL
10120#NULL#10007#Serious matters found during separate evaluations are not reported to top management.#NULL#0#NULL
10121#NULL#10007#Management has not identified appropriate knowledge and skills needed for various jobs.#NULL#0#NULL
10122#NULL#10007#Technology development projects do not support entity-wide objectives or strategies.#NULL#0#NULL
10123#NULL#10007#Management information needs with respect to payroll are not defined.#NULL#0#NULL
10124#NULL#10007#The organization does not have a positive \\"ethical tone\\".#NULL#0#NULL
10125#NULL#10007#The scope and frequency of separate evaluations do not take into consideration the assessment of risks and the effectiveness of ongoing monitoring procedures.#NULL#0#NULL
10126#NULL#10007#Margin deterioration, alternative sales prices quoted.#NULL#0#NULL
10127#NULL#10007#Margin pressure risk.#NULL#0#NULL
10128#NULL#10007#There are no separate evaluations of internal control effectiveness.#NULL#0#NULL
10129#NULL#10007#There is a lack of adequate means of communicating with, and obtaining information needed throughout organization to achieve from, external stakeholders that may have a significant impact on organization achieving its goals.#NULL#0#NULL
10130#NULL#10007#Organization does not have disaster recovery plans in place.#NULL#0#NULL
10131#NULL#10007#Unsecure bad acquisition.#NULL#0#NULL
10132#NULL#10007#Treasury risk.#NULL#0#NULL
10133#NULL#10007#Waste of money as result from non economical decision making.#NULL#0#NULL
10134#NULL#10007#Wrong expectations released by investor relations.#NULL#0#NULL
10136#NULL#10135#Changing legal and regulatory requirements.#NULL#0#NULL
10137#NULL#10135#Employees may not be aware of applicable laws and regulations.#NULL#0#NULL
10138#NULL#10135#Existing patents may be disregarded.#NULL#0#NULL
10139#NULL#10135#Inadequate information about, or understanding of, filing requirements and applicable laws and regulations.#NULL#0#NULL
10140#NULL#10135#Lack of awareness of laws and regulations.#NULL#0#NULL
10141#NULL#10135#Lack of knowledge regarding OSHA laws and regulations.#NULL#0#NULL
10142#NULL#10135#Legal counsel does get all contracts where entity commits itself legally.#NULL#0#NULL
10144#NULL#10135#Legal counsel is unaware of all activities taking place within the entity.#NULL#0#NULL
10145#NULL#10135#Management or supervisory personnel are unaware of legal and regulatory requirements and company policies.#NULL#0#NULL
10146#NULL#10135#Nonlegal personnel are unaware that certain circumstances could potentially lead to litigation.#NULL#0#NULL
10147#NULL#10135#Personnel are unaware of applicable laws and regulations.#NULL#0#NULL
10148#NULL#10135#Personnel are unaware of applicable laws, regulations, rules or contractual agreements.#NULL#0#NULL
10150#NULL#10149#Access to resources and records is not limited to authorized individuals.#NULL#0#NULL
10151#NULL#10149#Accountability for custody and use of resources and records is not assigned and/or maintained.#NULL#0#NULL
10152#NULL#10149#Analysis cannot be conducted due to inconsistent or nonexistent performance indicators.#NULL#0#NULL
10153#NULL#10149#Documentation is not readily available for examination.#NULL#0#NULL
10154#NULL#10149#Inaccurate information or estimates regarding costs of litigation or anticipated settlements.#NULL#0#NULL
10155#NULL#10149#Inaccurate or unavailable customer information.#NULL#0#NULL
10156#NULL#10149#Inaccurate, insufficient or untimely information regarding risk-related costs or accidents or incidents that could give rise to an insurance claim.#NULL#0#NULL
10157#NULL#10149#Inaccurate, untimely or unavailable information regarding actual costs incurred.#NULL#0#NULL
10158#NULL#10149#Inaccurate, untimely or unavailable information regarding cash inflows and outflows.#NULL#0#NULL
10159#NULL#10149#Inadequate information about, or understanding of, financial reporting of tax transactions or economic events.#NULL#0#NULL
10160#NULL#10149#Inadequate information regarding tax-savings opportunities.#NULL#0#NULL
10161#NULL#10149#Inadequate or inaccurate information.#NULL#0#NULL
10162#NULL#10149#Incomplete or inaccurate information used as the basis for document preparation.#NULL#0#NULL
10163#NULL#10149#Information needs of management or others is unknown or not clearly communicated.#NULL#0#NULL
10164#NULL#10149#Information systems are incapable of providing necessary information.#NULL#0#NULL
10165#NULL#10149#Information systems cannot provide necessary information in a timely manner.#NULL#0#NULL
10166#NULL#10149#Internal control, all transactions, and other significant events are not clearly documented.#NULL#0#NULL
10167#NULL#10149#Journal entries related to tax transactions or economic events are not properly approved or posted to the general ledger.#NULL#0#NULL
10168#NULL#10149#Lack of information regarding profit margins and / or sales prices.#NULL#0#NULL
10169#NULL#10149#Lack of or inaccurate information regarding competitive products or potential new products.#NULL#0#NULL
10170#NULL#10149#Lack of understanding of reporting requirements.#NULL#0#NULL
10171#NULL#10149#Out-of-date or incomplete price information.#NULL#0#NULL
10172#NULL#10149#Pertinent operational and/or financial information is not identified, captured, and distributed in a form and time frame that permits employees to perform their duties efficiently.#NULL#0#NULL
10173#NULL#10149#Unavailable or inaccurate information.#NULL#0#NULL
10174#NULL#10149#Unavailable or inaccurate information about fraudulent acts or other improper activities of vendors.#NULL#0#NULL
10175#NULL#10149#Unavailable or inaccurate information on inventory levels or production needs.#NULL#0#NULL
10176#NULL#10149#Incomplete or inaccurate information regarding changes affecting the entity, such as competition, products, customer preferences, or legal and regulatory changes.#NULL#0#NULL
11410#NULL#11225#Employees are authorizing and executing transactions and other significant events outside the scope of their authority.##0#
11411#NULL#11225#Handling and storage procedures, including storage containers, facilities and maintenance, are inappropriate for the nature of the products.##0#
11412#NULL#11225#Inaccurate information.##0#
11413#NULL#11225#Inaccurate input of data.##0#
11414#NULL#11225#Inaccurate or incomplete information is acquired and retained.##0#
11415#NULL#11225#Inaccurate or untimely information.##0#
11416#NULL#11225#Inadequate health & safety considerations.##0#
11417#NULL#11225#Inadequate policies and procedures to prevent unauthorized use.##0#
11418#NULL#11225#Inadequate transfer or requisition procedures.##0#
11419#NULL#11225#Inadequate vendor screening.##0#
11420#NULL#11225#Information is too specific to be usable.##0#
11421#NULL#11225#Insufficient or inappropriate resources.##0#
11422#NULL#11225#Insufficient staff.##0#
11423#NULL#11225#Lack of adequate tools for the personnel to ensure job success.##0#
11424#NULL#11225#Lack of awareness of entity's current human resources.##0#
11425#NULL#11225#Lack of or excess staff.##0#
11426#NULL#11225#Lack or loss of information or documents.##0#
11427#NULL#11225#Lost or misplaced information.##0#
11428#NULL#11225#Missing documents or incorrect information.##0#
11429#NULL#11225#Missing or untimely receipt of documents.##0#
11436#NULL#11225#Misuse of data / information (inside and outside organization).##0#
11437#NULL#11225#Personnel are unclear of levels of authority.##0#
11438#NULL#11225#Personnel do not possess and maintain the level of competence that allows them to accomplish their assigned duties.##0#
11439#NULL#11225#Personnel do not understand the importance of developing and implementing good internal control##0#
11440#NULL#11225#Personnel enter into contracts or agreements that are beyond their scope of authority.##0#
11441#NULL#11225#Personnel missing industry specific knowledge.##0#
11442#NULL#11225#Poorly maintained or inadequate equipment.##0#
11443#NULL#11225#Process has not properly segregated key duties and responsibilities.##0#
11444#NULL#11225#Receipts are for amounts different than invoiced amounts, or are not identifiable.##0#
11445#NULL#11225#Record-keeping requirements are disregarded.##0#
11446#NULL#11225#Records are lost or prematurely destroyed.##0#
11447#NULL#11225#Requisitions may be lost.##0#
11448#NULL#11225#Reuse of supporting documents.##0#
11449#NULL#11225#Technology may not be adequately defined.##0#
11450#NULL#11225#Training requirements may not be adequately identified.##0#
11451#NULL#11225#Transactions are not accounted for using numerical sequences.##0#
11452#NULL#11225#Transactions are not entered in a timely manner.##0#
11453#NULL#11225#Transfer documentation may be lost.##0#
11454#NULL#11225#Transfer documents may be lost.##0#
11455#NULL#11225#Transfer procedures do not require preparation of supporting documentation: intercompany goods movement.##0#
11456#NULL#11225#Unauthorized access.##0#
11457#NULL#11225#Unavailability of service personnel.##0#
11458#NULL#11225#Untrained staff.##0#
11459#NULL#11225#Variances are computed or recorded inaccurately.##0#
11460#NULL#11430#Customer order information may be unclear, inaccurate or incomplete.##0#
11461#NULL#11430#Customer orders may not be authorized.##0#
11462#NULL#11430#Customers delay remittance.##0#
11463#NULL#11430#Fictitious documentation is created.##0#
11464#NULL#11430#Handling and storage procedures, including storage containers, facilities and maintenance, are inappropriate for the nature of the products.##0#
11465#NULL#11430#Inaccurate or untimely pricing and inventory information.##0#
11466#NULL#11430#Inappropriate handling and storage policies and procedures.##0#
11467#NULL#11430#Inappropriate or unclear specifications.##0#
11468#NULL#11430#Inappropriate production specifications.##0#
11469#NULL#11430#Incomplete or inaccurate Information from order processing.##0#
11470#NULL#11430#Incomplete or inaccurate information regarding materials transferred to / from storage.##0#
11471#NULL#11430#Incomplete, untimely or inaccurate credit information.##0#
11472#NULL#11430#Information on issued purchase orders is not clearly or completely communicated.##0#
11473#NULL#11430#Information system does not identify available discounts and related required payment dates.##0#
11474#NULL#11430#Insufficient goods movement administration.##0#
11475#NULL#11430#Insufficient number of customer service representatives or service personnel.##0#
11476#NULL#11430#Insufficient or excess raw materials due to poor communication with procurement, or inaccurate or untimely material requirement forecasts.##0#
11477#NULL#11430#Insufficient storage capacity.##0#
11478#NULL#11430#Invalid accounts payable fraudulently created for unauthorized or nonexistent purchases.##0#
11479#NULL#11430#Lack of adequate systems.##0#
11480#NULL#11430#Lack of customer information.##0#
11481#NULL#11430#Mismatch of economical stock situation and physical stocks; interruption in production process due to stock not being there.##0#
11482#NULL#11430#Order documentation is lost.##0#
11483#NULL#11430#Poor communication with marketing regarding sales forecasts.##0#
11484#NULL#11430#Poor performance of distributors.##0#
11485#NULL#11430#Product is not produced according quality control standards.##0#
11486#NULL#11430#Product is unavailable in sufficient quantity.##0#
11487#NULL#11430#Product unavailability.##0#
11488#NULL#11430#Purchase order specifications are unclear.##0#
11489#NULL#11430#Purchase orders are lost or not forwarded to inbound activities.##0#
11490#NULL#11430#Purchase orders are not entered into the system on a timely basis.##0#
11491#NULL#11430#Purchase orders may be lost.##0#
11492#NULL#11430#Quantities to be produced are not communicated clearly.##0#
11493#NULL#11430#Sales orders are lost.##0#
11494#NULL#11430#Sales personnel are unaware of marketing strategies.##0#
11495#NULL#11430#Sales personnel disregard marketing strategies.##0#
11496#NULL#11430#Salespeople lack knowledge about product features or benefits.##0#
11497#NULL#11430#Salespeople perform poorly.##0#
11498#NULL#11430#Uncommunicated changes in warranty policies.##0#
11499#NULL#11430#Unordered or unauthorized products are included in customer shipment.##0#
11500#NULL#11430#Untimely processing of order information.##0#
11501#NULL#11431#Access to data, files, and programs are not controlled.##0#
11502#NULL#11431#Acquisition documentation may be lost or otherwise not communicated to proper personnel.##0#
11503#NULL#11431#Asset disposals or transfers may not be communicated to proper personnel.##0#
11504#NULL#11431#Certain jobs, activities or locations are hazardous.##0#
11505#NULL#11431#Data entry edit checks are not conducted.##0#
11506#NULL#11431#Discontinuity of business processes.##0#
11507#NULL#11431#Dishonest employees.##0#
11508#NULL#11431#Disruption of normal shipping channels.##0#
11509#NULL#11431#Downtime as result from inadequate skilled labor.##0#
11510#NULL#11431#Downtime as result of natural or other disasters.##0#
11511#NULL#11431#Downtime as result of poorly maintained, misused or obsolete equipment.##0#
11512#NULL#11431#Due date information is not available.##0#
11513#NULL#11431#Employees are not familiar with handling and storage requirements or procedures.##0#
11514#NULL#11431#Employees ignore safety policies or procedures.##0#
11515#NULL#11431#Excessive work steps/operations.##0#
11516#NULL#11431#Fictitious documentation is created.##0#
11517#NULL#11431#Handling and storage procedures, including storage containers, facilities and maintenance, are inappropriate for the nature of the products.##0#
11518#NULL#11431#Improper cutoff of shipments at the end of a period.##0#
11519#NULL#11431#Improper organization of storage facility.##0#
11520#NULL#11431#Improper products or improper quantities are retrieved from storage.##0#
11521#NULL#11431#Improperly trained service personnel.##0#
11522#NULL#11431#Inability to identify the stage of production.##0#
11523#NULL#11431#Inaccurate or incomplete shipping documents.##0#
11524#NULL#11431#Inadequate physical security over fixed assets.##0#
11525#NULL#11431#Inadequate physical security over goods received.##0#
11526#NULL#11431#Inappropriate handling and storage policies and procedures.##0#
11527#NULL#11431#Inappropriate production specifications.##0#
11528#NULL#11431#Incomplete or inaccurate Information from order processing.##0#
11529#NULL#11431#Incomplete or inaccurate information regarding materials transferred to / from storage.##0#
11530#NULL#11431#Incorrect information is entered on shipping documentation.##0#
11531#NULL#11431#Information on materials received is not entered into the information system accurately or on a timely basis by goods receiver.##0#
11532#NULL#11431#Insufficient goods movement administration.##0#
11533#NULL#11431#Insufficient or excess raw materials due to poor communication with procurement, or inaccurate or untimely material requirement forecasts.##0#
11534#NULL#11431#Insufficient storage capacity.##0#
11535#NULL#11431#Interruption of production process due to lack of materials.##0#
11536#NULL#11431#Lack of adequate systems.##0#
11537#NULL#11431#Lost receiving reports or lost shipping records.##0#
11538#NULL#11431#Materials are not tested for specification compliance.##0#
11539#NULL#11431#Materials not requisitioned are transferred.##0#
11540#NULL#11431#Mismatch of economical stock situation and physical stocks; interruption in production process due to stock not being there.##0#
11541#NULL#11431#Order documentation is lost.##0#
11542#NULL#11431#Order or shipping documentation may be lost.##0#
11543#NULL#11431#Out-of-date production facilities.##0#
11544#NULL#11431#Packing materials, containers or procedures are inappropriate for the nature of the product or method of shipment.##0#
11545#NULL#11431#Plans and schedules are not communicated to inbound activities, or do not clearly identify when goods can be received or where materials are needed.##0#
11546#NULL#11431#Poor communication of operations' or other activities' needs.##0#
11547#NULL#11431#Poor organization of customer service department.##0#
11548#NULL#11431#Poorly organized production process.##0#
11549#NULL#11431#Pressure to meet production deadlines.##0#
11550#NULL#11431#Product is not produced according quality control standards.##0#
11551#NULL#11431#Product is unavailable in sufficient quantity.##0#
11552#NULL#11431#Product may be moved into or out of storage without proper authorization.##0#
11553#NULL#11431#Product moved into or out of storage may not be documented or recorded.##0#
11554#NULL#11431#Product unavailability.##0#
11555#NULL#11431#Production processes do not include procedures designed to ensure quality production.##0#
11556#NULL#11431#Purchase orders may be lost.##0#
11557#NULL#11431#Quality problems are not discovered or appropriately reported during the production process.##0#
11558#NULL#11431#Quantities to be produced are not communicated clearly.##0#
11559#NULL#11431#Receiving information may be entered inaccurately in the information system, or may not be timely.##0#
11560#NULL#11431#Several products compete for concurrent production.##0#
11561#NULL#11431#Shipping documents are lost.##0#
11562#NULL#11431#Unavailable or inaccurate information on items ordered but not received.##0#
11563#NULL#11431#Uncommunicated changes in warranty policies.##0#
11564#NULL#11431#Unordered or unauthorized products are included in customer shipment.##0#
11565#NULL#11431#Untimely processing of order information.##0#
11566#NULL#11431#Use of inefficient shipping methods.##0#
11567#NULL#11431#Vendors' inability to provide needed quantities due to other higher-priority orders or an interruption in their own supplies.##0#
11568#NULL#11430#Sales orders are lost.##0#
11569#NULL#11432#Acquisition documentation may be lost or otherwise not communicated to proper personnel.##0#
11570#NULL#11432#Certain jobs, activities or locations are hazardous.##0#
11571#NULL#11432#Downtime as result from inadequate skilled labor.##0#
11572#NULL#11432#Downtime as result of poorly maintained, misused or obsolete equipment.##0#
11573#NULL#11432#Due date information is not available.##0#
11574#NULL#11432#Fictitious documentation is created.##0#
11575#NULL#11432#Inadequate physical security over fixed assets.##0#
11576#NULL#11432#Inadequate physical security over goods received.##0#
11577#NULL#11432#Inadequate product testing.##0#
11578#NULL#11432#Inappropriate or unclear specifications.##0#
11579#NULL#11432#Inappropriate production specifications.##0#
11580#NULL#11432#Interruption of production process due to lack of materials.##0#
11581#NULL#11432#Materials are not tested for specification compliance.##0#
11582#NULL#11432#Materials not requisitioned are transferred.##0#
11583#NULL#11432#Mismatch of economical stock situation and physical stocks; interruption in production process due to stock not being there.##0#
11584#NULL#11432#Out-of-date production facilities.##0#
11585#NULL#11432#Patents relevant in the market place are not known.##0#
11586#NULL#11432#Product is not produced according quality control standards.##0#
11587#NULL#11432#Relevant patents may not be identified.##0#
11588#NULL#11432#Uncommunicated changes in warranty policies.##0#
11589#NULL#11432#Vendors' inability to provide needed quantities due to other higher-priority orders or an interruption in their own supplies.##0#
11590#NULL#11433#Acquisition documentation may be lost or otherwise not communicated to proper personnel.##0#
11591#NULL#11433#Asset disposals or transfers may not be communicated to proper personnel.##0#
11592#NULL#11433#Compensation and benefits are less than offered by other companies.##0#
11593#NULL#11433#Dishonest employees.##0#
11594#NULL#11433#Downtime as result from inadequate skilled labor.##0#
11595#NULL#11433#Eligible employees are improperly excluded from participation.##0#
11596#NULL#11433#Employee carelessness.##0#
11597#NULL#11433#Errors are made in calculating benefits.##0#
11598#NULL#11433#Hours are not authorized or are inaccurate.##0#
11599#NULL#11433#Human resource personnel are unaware of the records that must be retained to demonstrate compliance with applicable laws and regulations.##0#
11600#NULL#11433#Human resource records are not subject to proper security procedures.##0#
11601#NULL#11433#Improperly trained service personnel.##0#
11602#NULL#11433#Inaccurate employee information is provided to benefits personnel.##0#
11603#NULL#11433#Ineffective safety and employee training programs.##0#
11604#NULL#11433#Nonexistent employees are entered as program participants or beneficiaries.##0#
11605#NULL#11433#Not understanding moving of personnel.##0#
11606#NULL#11433#Over- or under qualified candidates may be hired.##0#
11607#NULL#11433#Plan benefit provisions are unclear or complex.##0#
11608#NULL#11433#Program eligibility requirements are not clearly communicated to appropriate personnel.##0#
11609#NULL#11433#Staff are not evaluated on regular or timely basis.##0#
11610#NULL#11433#The entity may be unaware of its future staffing needs.##0#
11611#NULL#11433#Time cards do not match with payroll register time (job time is not equal to shoptime).##0#
11612#NULL#11433#Time cards or other source information is submitted for nonexistent employees.##0#
11613#NULL#11433#Lack of good personnel.##0#
11614#NULL#11433#Lack of qualified candidates.##0#
11615#NULL#11433#Unauthorized lay off of personnel.##0#
11616#NULL#11433#Unauthorized personnel may gain access to payroll information.##0#
11617#NULL#11434#Access to data, files, and programs are not controlled.##0#
11618#NULL#11434#Acquired assets may not be adequately described.##0#
11619#NULL#11434#Asset disposals or transfers may not be communicated to proper personnel.##0#
11620#NULL#11434#Computer operations fail to use correct programs, files and procedures.##0#
11621#NULL#11434#Data entry edit checks are not conducted.##0#
11622#NULL#11434#Discontinuity of business processes.##0#
11623#NULL#11434#Downtime as result of poorly maintained, misused or obsolete equipment.##0#
11624#NULL#11434#Inadequate information systems.##0#
11625#NULL#11434#Inadequate safeguarding of IT resources.##0#
11626#NULL#11434#Information system does not identify available discounts and related required payment dates.##0#
11627#NULL#11434#Information systems lack application controls.##0#
11628#NULL#11434#Information systems lack general controls.##0#
11629#NULL#11434#Organization has and/or is developing duplicative information system capabilities.##0#
11630#NULL#11434#Organization is developing information systems outside of the corporate architecture.##0#
11631#NULL#11434#Out-of-date systems.##0#
11632#NULL#11434#Poor back-up and recovery procedures.##0#
11633#NULL#11434#Product or processes needs are not effectively communicated to Technology Development.##0#
11634#NULL#11434#Programs are subjected to unauthorized modification.##0#
11635#NULL#11434#Purchase orders may be lost.##0#
11636#NULL#11434#Sales orders are lost.##0#
11637#NULL#11434#System and program modifications are implemented incorrectly.##0#
11638#NULL#11434#Systems are not designed according to user needs or are not properly implemented.##0#
11639#NULL#11434#Technology development management are unaware of project priorities.##0#
11640#NULL#11434#Technology Development personnel do not have technical ability to identify or develop appropriate technology.##0#
11641#NULL#11434#Technology Development personnel may acquire or have knowledge that would be useful in a development program other than that with which they are associated.##0#
11642#NULL#11435#Acquired assets may not be adequately described.##0#
11643#NULL#11435#Bad debts.##0#
11644#NULL#11435#Bills are paid before due dates.##0#
11645#NULL#11435#Cash received is diverted, lost or otherwise not reported accurately to accounts receivable.##0#
11646#NULL#11435#Checks clear the bank quickly.##0#
11647#NULL#11435#Equity risk.##0#
11648#NULL#11435#Excessive accounts receivable collection problems.##0#
11649#NULL#11435#Failure to establish or maintain appropriate relationships with financing sources.##0#
11650#NULL#11435#Handling cash receipts internally can delay deposit of such receipts.##0#
11651#NULL#11435#Inaccurate, untimely or unavailable information regarding amounts or due dates of payments.##0#
11652#NULL#11435#Inaccurate, untimely or unavailable information regarding payment due dates.##0#
11653#NULL#11435#Inadequate accounting systems for allocating costs.##0#
11654#NULL#11435#Inadequate physical security over cash and documents that can be used to transfer cash.##0#
11655#NULL#11435#Incomplete or inaccurate Information from order processing.##0#
11656#NULL#11435#Incomplete, untimely or inaccurate credit information.##0#
11657#NULL#11435#Incorrect depreciation lives or methods may be used.##0#
11658#NULL#11435#Information system does not identify available discounts and related required payment dates.##0#
11659#NULL#11435#Invalid accounts payable fraudulently created for unauthorized or nonexistent purchases.##0#
11660#NULL#11435#Lack of awareness regarding financing alternatives.##0#
11661#NULL#11435#Lack of knowledge regarding investment alternatives.##0#
11662#NULL#11435#Loan risk.##0#
11663#NULL#11435#Pay rates or deductions are not properly authorized or are inaccurate.##0#
11664#NULL#11435#Purchase orders may be lost.##0#
11665#NULL#11435#Sales orders are lost.##0#
11666#NULL#11435#Subsidiary ledger does not match with general ledger.##0#
11667#NULL#11435#System is not designed to reflect payment schedule included in collective bargaining agreements or individual agreements with employees.##0#
11668#NULL#11435#Unauthorized access to accounts payable records and stored data.##0#
11669#NULL#11435#Unauthorized access to accounts receivable records and stored data.##0#
11670#NULL#11435#Unauthorized additions to accounts payable.##0#
11671#NULL#11435#Unauthorized input for nonexistent returns.##0#
11672#NULL#11435#Unauthorized input for nonexistent returns, allowances and write-offs.##0#
11673#NULL#11435#Unauthorized personnel have access to financial information.##0#
11674#NULL#11435#Untimely processing of order information.##0#
