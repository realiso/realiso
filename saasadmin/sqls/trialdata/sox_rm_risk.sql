11675#NULL#NULL#10148#11389#Personnel are unaware of applicable laws, regulations, rules or contractual agreements.#NULL#0#0#NULL#0#0#NULL#0#
11676#NULL#NULL#10147#11389#Personnel are unaware of applicable laws and regulations.#NULL#0#0#NULL#0#0#NULL#0#
11677#NULL#NULL#10146#11389#Nonlegal personnel are unaware that certain circumstances could potentially lead to litigation.#NULL#0#0#NULL#0#0#NULL#0#
11678#NULL#NULL#10145#11389#Management or supervisory personnel are unaware of legal and regulatory requirements and company policies.#NULL#0#0#NULL#0#0#NULL#0#
11679#NULL#NULL#10144#11389#Legal counsel is unaware of all activities taking place within the entity.#NULL#0#0#NULL#0#0#NULL#0#
11680#NULL#NULL#10143#11389#Legal counsel does not review contracts or agreements.#NULL#0#0#NULL#0#0#NULL#0#
11933#4#NULL#10136#11238#Changing legal and regulatory requirements.#NULL#8#8#NULL#0#0#NULL#0#
11925#1#NULL#11460#11248#Customer order information may be unclear, inaccurate or incomplete.#NULL#1.5#1.5#NULL#0#0#NULL#0#
11924#4#NULL#11461#11248#Customer orders may not be authorized.#NULL#6#6#NULL#0#0#NULL#0#
11923#2#NULL#11462#11248#Customers delay remittance.#NULL#3#3#NULL#0#0#NULL#0#
11931#4#NULL#10138#11238#Existing patents may be disregarded.#NULL#14#14#NULL#0#0#NULL#0#
11922#2#NULL#11463#11248#Fictitious documentation is created.#NULL#6#6#NULL#0#0#NULL#0#
11701#4#NULL#11411#11240#Handling and storage procedures, including storage containers, facilities and maintenance, are inappropriate for the nature of the products.#NULL#10#10#NULL#0#0#NULL#0#
11928#4#NULL#11411#11248#Handling and storage procedures, including storage containers, facilities and maintenance, are inappropriate for the nature of the products.#NULL#4#4#NULL#0#0#NULL#0#
11927#4#NULL#11412#11248#Inaccurate information.#NULL#6#6#NULL#0#0#NULL#0#
11926#4#NULL#11413#11248#Inaccurate input of data.#NULL#8#8#NULL#0#0#NULL#0#
11697#4#NULL#11415#11240#Inaccurate or untimely information.#NULL#10#10#NULL#0#0#NULL#0#
11696#4#NULL#11416#11240#Inadequate health & safety considerations.#NULL#14#14#NULL#0#0#NULL#0#
11684#2#NULL#10139#11389#Inadequate information about, or understanding of, filing requirements and applicable laws and regulations.#NULL#7#7#NULL#0#0#NULL#0#
11683#2#NULL#10140#11389#Lack of awareness of laws and regulations.#NULL#7#7#NULL#0#0#NULL#0#
11692#1#NULL#11461#11240#Customer orders may not be authorized.#NULL#2.5#2.5#NULL#0#0#NULL#0#
11685#2#NULL#10138#11389#Existing patents may be disregarded.#NULL#7#7#NULL#0#0#NULL#0#
11689#4#NULL#11464#11240#Handling and storage procedures, including storage containers, facilities and maintenance, are inappropriate for the nature of the products.#NULL#10#10#NULL#0#0#NULL#0#
11700#5#NULL#11412#11240#Inaccurate information.#NULL#12.5#12.5#NULL#0#0#NULL#0#
11698#4#NULL#11414#11240#Inaccurate or incomplete information is acquired and retained.#NULL#8#8#NULL#0#0#NULL#0#
11695#1#NULL#11417#11240#Inadequate policies and procedures to prevent unauthorized use.#NULL#4#4#NULL#0#0#NULL#0#
11681#4#NULL#10142#11389#Legal counsel does get all contracts where entity commits itself legally.#NULL#14#14#NULL#0#0#NULL#0#
11693#2#NULL#11460#11240#Customer order information may be unclear, inaccurate or incomplete.#NULL#5#5#NULL#0#0#NULL#0#
11921#1#NULL#11464#11248#Handling and storage procedures, including storage containers, facilities and maintenance, are inappropriate for the nature of the products.#NULL#1.5#1.5#NULL#0#0#NULL#0#
11688#3#NULL#11465#11240#Inaccurate or untimely pricing and inventory information.#NULL#7.5#7.5#NULL#0#0#NULL#0#
11694#4#NULL#11418#11240#Inadequate transfer or requisition procedures.#NULL#10#10#NULL#0#0#NULL#0#
11691#4#NULL#11462#11240#Customers delay remittance.#NULL#10#10#NULL#0#0#NULL#0#
11699#2#NULL#11413#11240#Inaccurate input of data.#NULL#5#5#NULL#0#0#NULL#0#
11690#4#NULL#11463#11240#Fictitious documentation is created.#NULL#10#10#NULL#0#0#NULL#0#
11682#4#NULL#10141#11389#Lack of knowledge regarding OSHA laws and regulations.#NULL#14#14#NULL#0#0#NULL#0#
11930#4#NULL#10139#11238#Inadequate information about, or understanding of, filing requirements and applicable laws and regulations.#NULL#8#8#NULL#0#0#NULL#0#
11718#4#NULL#11422#11242#Insufficient staff.#NULL#12#12#NULL#0#0#NULL#0#
11738#4#NULL#11628#11242#Information systems lack general controls.#NULL#14#14#NULL#0#0#NULL#0#
11715#5#NULL#11425#11242#Lack of or excess staff.#NULL#17.5#17.5#NULL#0#0#NULL#0#
11717#4#NULL#11423#11242#Lack of adequate tools for the personnel to ensure job success.#NULL#20#20#NULL#0#0#NULL#0#
11719#4#NULL#11421#11242#Insufficient or inappropriate resources.#NULL#14#14#NULL#0#0#NULL#0#
11721#4#NULL#11419#11242#Inadequate vendor screening.#NULL#14#14#NULL#0#0#NULL#0#
11741#3#NULL#11625#11242#Inadequate safeguarding of IT resources.#NULL#10.5#10.5#NULL#0#0#NULL#0#
11749#4#NULL#11617#11242#Access to data, files, and programs are not controlled.#NULL#14#14#NULL#0#0#NULL#0#
11751#NULL#NULL#11458#11244#Untrained staff.#NULL#0#0#NULL#0#0#NULL#0#
11750#NULL#NULL#11459#11244#Variances are computed or recorded inaccurately.#NULL#0#0#NULL#0#0#NULL#0#
11784#4#NULL#11419#11244#Inadequate vendor screening.#NULL#10#10#NULL#0#0#NULL#0#
11787#4#NULL#11416#11244#Inadequate health & safety considerations.#NULL#10#10#NULL#0#0#NULL#0#
11783#4#NULL#11420#11244#Information is too specific to be usable.#NULL#16#16#NULL#0#0#NULL#0#
11799#5#NULL#11578#11244#Inappropriate or unclear specifications.#NULL#12.5#12.5#NULL#0#0#NULL#0#
11802#4#NULL#11575#11244#Inadequate physical security over fixed assets.#NULL#10#10#NULL#0#0#NULL#0#
11920#1#NULL#11636#11246#Sales orders are lost.#NULL#4#4#NULL#0#0#NULL#0#
11918#1#NULL#11638#11246#Systems are not designed according to user needs or are not properly implemented.#NULL#4#4#NULL#0#0#NULL#0#
11916#1#NULL#11640#11246#Technology Development personnel do not have technical ability to identify or develop appropriate technology.#NULL#4#4#NULL#0#0#NULL#0#
11914#1#NULL#11446#11246#Records are lost or prematurely destroyed.#NULL#4#4#NULL#0#0#NULL#0#
11912#1#NULL#11448#11246#Reuse of supporting documents.#NULL#4#4#NULL#0#0#NULL#0#
11910#1#NULL#11450#11246#Training requirements may not be adequately identified.#NULL#4#4#NULL#0#0#NULL#0#
11908#1#NULL#11452#11246#Transactions are not entered in a timely manner.#NULL#4#4#NULL#0#0#NULL#0#
11906#1#NULL#11454#11246#Transfer documents may be lost.#NULL#4#4#NULL#0#0#NULL#0#
11904#1#NULL#11456#11246#Unauthorized access.#NULL#4#4#NULL#0#0#NULL#0#
11902#1#NULL#11458#11246#Untrained staff.#NULL#4#4#NULL#0#0#NULL#0#
11885#1#NULL#10052#11260#Organization does not have documented procedures in place to delegate authority and responsibility.#NULL#5#5#NULL#0#0#NULL#0#
11881#1#NULL#10056#11260#Organization does not maintain physical control of, and properly secures/safeguard, vulnerable assets.#NULL#5#5#NULL#0#0#NULL#0#
11864#1#NULL#10073#11260#Plan formats are ineffective in providing necessary benchmarks against which performance can be measured.#NULL#5#5#NULL#0#0#NULL#0#
11856#1#NULL#10081#11260#Pre-established standards are not determined.#NULL#5#5#NULL#0#0#NULL#0#
11853#1#NULL#10084#11260#Relationships between oversight agencies and management do not exist.#NULL#5#5#NULL#0#0#NULL#0#
11919#1#NULL#11637#11246#System and program modifications are implemented incorrectly.#NULL#4#4#NULL#0#0#NULL#0#
11915#1#NULL#11641#11246#Technology Development personnel may acquire or have knowledge that would be useful in a development program other than that with which they are associated.#NULL#4#4#NULL#0#0#NULL#0#
11911#1#NULL#11449#11246#Technology may not be adequately defined.#NULL#4#4#NULL#0#0#NULL#0#
11907#1#NULL#11453#11246#Transfer documentation may be lost.#NULL#4#4#NULL#0#0#NULL#0#
11903#1#NULL#11457#11246#Unavailability of service personnel.#NULL#4#4#NULL#0#0#NULL#0#
11883#1#NULL#10054#11260#Organization does not have procedures in place to delegate authority and responsibility.#NULL#5#5#NULL#0#0#NULL#0#
11860#1#NULL#10077#11260#Policies and procedures have not been established for ensuring that findings of audits and other reviews are promptly resolved.#NULL#5#5#NULL#0#0#NULL#0#
11851#1#NULL#10086#11260#Sales personnel are unaware of potential customers#NULL#5#5#NULL#0#0#NULL#0#
11833#1#NULL#10104#11260#Organization does not have appropriate practices for orienting personnel.#NULL#5#5#NULL#0#0#NULL#0#
11917#1#NULL#11639#11246#Technology development management are unaware of project priorities.#NULL#4#4#NULL#0#0#NULL#0#
11909#1#NULL#11451#11246#Transactions are not accounted for using numerical sequences.#NULL#4#4#NULL#0#0#NULL#0#
11901#1#NULL#11459#11246#Variances are computed or recorded inaccurately.#NULL#4#4#NULL#0#0#NULL#0#
11855#1#NULL#10082#11260#Products become obsolete.#NULL#5#5#NULL#0#0#NULL#0#
11831#1#NULL#10106#11260#Organization does not have appropriate practices for promoting personnel.#NULL#5#5#NULL#0#0#NULL#0#
11913#1#NULL#11447#11246#Requisitions may be lost.#NULL#4#4#NULL#0#0#NULL#0#
11868#1#NULL#10069#11260#Organization structure does not establish appropriate lines of reporting.#NULL#5#5#NULL#0#0#NULL#0#
11905#1#NULL#11455#11246#Transfer procedures do not require preparation of supporting documentation: intercompany goods movement.#NULL#4#4#NULL#0#0#NULL#0#
11761#3#NULL#11448#11244#Reuse of supporting documents.#NULL#10.5#10.5#NULL#0#0#NULL#0#
11771#3#NULL#11438#11244#Personnel do not possess and maintain the level of competence that allows them to accomplish their assigned duties.#NULL#10.5#10.5#NULL#0#0#NULL#0#
11773#3#NULL#11436#11244#Misuse of data / information (inside and outside organization).#NULL#10.5#10.5#NULL#0#0#NULL#0#
11775#3#NULL#11428#11244#Missing documents or incorrect information.#NULL#10.5#10.5#NULL#0#0#NULL#0#
11770#3#NULL#11439#11244#Personnel do not understand the importance of developing and implementing good internal control#NULL#10.5#10.5#NULL#0#0#NULL#0#
11774#3#NULL#11429#11244#Missing or untimely receipt of documents.#NULL#10.5#10.5#NULL#0#0#NULL#0#
11772#3#NULL#11437#11244#Personnel are unclear of levels of authority.#NULL#10.5#10.5#NULL#0#0#NULL#0#
11788#2#NULL#11589#11244#Vendors' inability to provide needed quantities due to other higher-priority orders or an interruption in their own supplies.#NULL#4#4#NULL#0#0#NULL#0#
11793#2#NULL#11584#11244#Out-of-date production facilities.#NULL#4#4#NULL#0#0#NULL#0#
11798#2#NULL#11579#11244#Inappropriate production specifications.#NULL#4#4#NULL#0#0#NULL#0#
11792#2#NULL#11585#11244#Patents relevant in the market place are not known.#NULL#4#4#NULL#0#0#NULL#0#
11794#2#NULL#11583#11244#Mismatch of economical stock situation and physical stocks; interruption in production process due to stock not being there.#NULL#4#4#NULL#0#0#NULL#0#
11803#3#NULL#10134#11260#Wrong expectations released by investor relations.#NULL#9#9#NULL#0#0#NULL#0#
11805#3#NULL#10132#11260#Treasury risk.#NULL#9#9#NULL#0#0#NULL#0#
11807#3#NULL#10130#11260#Organization does not have disaster recovery plans in place.#NULL#9#9#NULL#0#0#NULL#0#
11809#3#NULL#10128#11260#There are no separate evaluations of internal control effectiveness.#NULL#9#9#NULL#0#0#NULL#0#
11811#3#NULL#10126#11260#Margin deterioration, alternative sales prices quoted.#NULL#9#9#NULL#0#0#NULL#0#
11813#3#NULL#10124#11260#The organization does not have a positive \\"ethical tone\\".#NULL#9#9#NULL#0#0#NULL#0#
11815#3#NULL#10122#11260#Technology development projects do not support entity-wide objectives or strategies.#NULL#9#9#NULL#0#0#NULL#0#
11818#3#NULL#10119#11260#Wrong information as basis for decision making; competition risk.#NULL#9#9#NULL#0#0#NULL#0#
11820#3#NULL#10117#11260#Separate evaluations of controls do not focus directly on their effectiveness at a specific time.#NULL#9#9#NULL#0#0#NULL#0#
11824#3#NULL#10113#11260#Non added value based acquisition.#NULL#9#9#NULL#0#0#NULL#0#
11828#3#NULL#10109#11260#Mismatch in policy and knowledge about marketing and financial strategies.#NULL#9#9#NULL#0#0#NULL#0#
11804#3#NULL#10133#11260#Waste of money as result from non economical decision making.#NULL#9#9#NULL#0#0#NULL#0#
11808#3#NULL#10129#11260#There is a lack of adequate means of communicating with, and obtaining information needed throughout organization to achieve from, external stakeholders that may have a significant impact on organization achieving its goals.#NULL#9#9#NULL#0#0#NULL#0#
11812#3#NULL#10125#11260#The scope and frequency of separate evaluations do not take into consideration the assessment of risks and the effectiveness of ongoing monitoring procedures.#NULL#9#9#NULL#0#0#NULL#0#
11817#3#NULL#10120#11260#Serious matters found during separate evaluations are not reported to top management.#NULL#9#9#NULL#0#0#NULL#0#
11823#3#NULL#10114#11260#Not meeting targets.#NULL#9#9#NULL#0#0#NULL#0#
11827#3#NULL#10110#11260#Monitoring efforts do not include comparisons, reconciliations, and other actions that people take in performing their duties.#NULL#9#9#NULL#0#0#NULL#0#
11806#3#NULL#10131#11260#Unsecure bad acquisition.#NULL#9#9#NULL#0#0#NULL#0#
11825#3#NULL#10112#11260#No adequate strategy in place.#NULL#9#9#NULL#0#0#NULL#0#
11810#3#NULL#10127#11260#Margin pressure risk.#NULL#9#9#NULL#0#0#NULL#0#
11829#3#NULL#10108#11260#Organization does not have appropriate practices for training personnel.#NULL#9#9#NULL#0#0#NULL#0#
11819#3#NULL#10118#11260#Serious matters found during ongoing monitoring are not reported to top management.#NULL#9#9#NULL#0#0#NULL#0#
11713#4#NULL#11427#11242#Lost or misplaced information.#NULL#16#16#NULL#0#0#NULL#0#
11737#4#NULL#11629#11242#Organization has and/or is developing duplicative information system capabilities.#NULL#16#16#NULL#0#0#NULL#0#
11708#4#NULL#11438#11242#Personnel do not possess and maintain the level of competence that allows them to accomplish their assigned duties.#NULL#16#16#NULL#0#0#NULL#0#
11706#4#NULL#11440#11242#Personnel enter into contracts or agreements that are beyond their scope of authority.#NULL#16#16#NULL#0#0#NULL#0#
11704#4#NULL#11442#11242#Poorly maintained or inadequate equipment.#NULL#16#16#NULL#0#0#NULL#0#
11732#4#NULL#11634#11242#Programs are subjected to unauthorized modification.#NULL#16#16#NULL#0#0#NULL#0#
11712#4#NULL#11428#11242#Missing documents or incorrect information.#NULL#16#16#NULL#0#0#NULL#0#
11707#4#NULL#11439#11242#Personnel do not understand the importance of developing and implementing good internal control#NULL#16#16#NULL#0#0#NULL#0#
11733#4#NULL#11633#11242#Product or processes needs are not effectively communicated to Technology Development.#NULL#16#16#NULL#0#0#NULL#0#
11735#4#NULL#11631#11242#Out-of-date systems.#NULL#16#16#NULL#0#0#NULL#0#
11734#4#NULL#11632#11242#Poor back-up and recovery procedures.#NULL#16#16#NULL#0#0#NULL#0#
11838#1#NULL#10099#11260#Management has not recently revisited the sufficiency of disaster recovery plans.#NULL#5#4#NULL#0#0#NULL#0#
11836#1#NULL#10101#11260#Management is unaware of legal and regulatory requirements.#NULL#5#4#NULL#0#0#NULL#0#
11832#1#NULL#10105#11260#Management or supervisory personnel ignore legal and regulatory requirements or company policies.#NULL#5#4#NULL#0#0#NULL#0#
11826#3#NULL#10111#11260#Monitoring efforts do not include regular management and supervisory activities.#NULL#9#6#NULL#0#0#NULL#0#
11814#3#NULL#10123#11260#Management information needs with respect to payroll are not defined.#NULL#9#6#NULL#0#0#NULL#0#
11830#1#NULL#10107#11260#Management takes too much risk.#NULL#5#4#NULL#0#0#NULL#0#
11834#1#NULL#10103#11260#Management is unaware of valuable assets.#NULL#5#4#NULL#0#0#NULL#0#
11687#4#NULL#10136#11389#Changing legal and regulatory requirements.#NULL#12#12#NULL#72601#0#Accepted by Management.#0#
11702#4#NULL#11410#11240#Employees are authorizing and executing transactions and other significant events outside the scope of their authority.#NULL#12#12#NULL#0#0#NULL#0#
11716#5#NULL#11424#11242#Lack of awareness of entity's current human resources.#NULL#25#25#NULL#0#0#NULL#0#
11929#4#NULL#11410#11248#Employees are authorizing and executing transactions and other significant events outside the scope of their authority.#NULL#8#8#NULL#0#0#NULL#0#
11686#4#NULL#10137#11389#Employees may not be aware of applicable laws and regulations.#NULL#14#14#NULL#0#0#NULL#0#
11932#1#NULL#10137#11238#Employees may not be aware of applicable laws and regulations.#NULL#2.5#2.5#NULL#0#0#NULL#0#