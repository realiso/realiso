300#NULL#279#Different interpretations about the meaning of information security.#NULL#0#NULL
301#NULL#279#Lack of company's directions related to information security.#NULL#0#NULL
302#NULL#279#Divergence between the information security objectives and the business objectives.#NULL#0#NULL
303#NULL#279#Non-existence of a clear focus on the information security management.#NULL#0#NULL
304#NULL#279#Security problems caused by the non-existence of a formal policy that declares the approach to the information security management.#NULL#0#NULL
305#NULL#279#Lack of comprehension by the staff about how important the information security is.#NULL#0#NULL
306#NULL#255#Not updating the Security Policy against the business needs and goals.#NULL#0#NULL
307#NULL#279#Not updating the Security Policy against the importance and the need of business objectives.#NULL#0#NULL
308#NULL#279#Inefficiency of Security Policy as an instrument of orientation to the information security management process.#NULL#0#NULL
309#NULL#279#Lack of management commitment to initiatives of information security.#NULL#0#NULL
310#NULL#279#Lack of knowledge about information security processes and responsibilities by the management.#NULL#0#NULL
311#NULL#279#Lack of definition of responsibilities for the information security due to lack of a clear definition by the management.#NULL#0#NULL
312#NULL#279#Lack of strategic alignment between the information security and the business objectives.#NULL#0#NULL
313#NULL#279#Lack of credibility in the management process of information security due to lack of support from management.#NULL#0#NULL
314#NULL#279#Lack of people with expertise to conduct the process of information security management.#NULL#0#NULL
315#NULL#279#Lack of resources for the fulfillment of the requirements of information security.#NULL#0#NULL
316#NULL#279#Difficulty in implementation of security controls due to lack of coordination with relevant functions and roles.#NULL#0#NULL
317#NULL#279#Misalignment between actions of information security and security policy.#NULL#0#NULL
318#NULL#279#Lack of support for the information security process by the key roles in the company.#NULL#0#NULL
319#NULL#279#Difficulty in conduct and solve non-conformities of information security process.#NULL#0#NULL
320#NULL#279#Inability to homologate methods for risk management.#NULL#0#NULL
321#NULL#279#Security problems due to lack of definition of responsibilities for information security processes.#NULL#0#NULL
322#NULL#279#Lack of implementation of controls due to lack of clear definition of responsibilities.#NULL#0#NULL
323#NULL#279#Difficulty by people in identifying and recognizing their information security responsibilities.#NULL#0#NULL
324#NULL#255#Difficulty by people in identifying and recognizing their information security responsibilities.#NULL#0#NULL
325#NULL#279#Security problemas caused by divergences from who is the responsible for the information security.#NULL#0#NULL
329#NULL#241#Processing information in environments  not properly controlled.#NULL#0#NULL
337#NULL#241#Insertions of new hardware and/or software not compatible with the rest of the system devices.#NULL#0#NULL
342#NULL#100#Unauthorized disclosure of sensitive information during the contracting period.#NULL#0#NULL
343#NULL#255#Unauthorized disclosure of sensitive information during the contracting period.#NULL#0#NULL
346#NULL#100#Unauthorized information disclosure by the suppliers.#NULL#0#NULL
347#NULL#255#Unauthorized disclosure of information after the contract is finished.#NULL#0#NULL
348#NULL#279#Lack of knowledge about the need to contact the authorities when a relevant security incident occurs.#NULL#0#NULL
349#NULL#279#Lack of knowledge about the need to contact the authorities when a relevant security incident occurs.#NULL#0#NULL
350#NULL#279#Difficulty in contacting authorities  by lack of contact information.#NULL#0#NULL
352#NULL#279#Lack of knowledge of new vulnerabilities due to lack of contact with research groups and services of vulnerabilities notification.#NULL#0#NULL
353#NULL#279#Lack of specialized knowledge on information security.#NULL#0#NULL
354#NULL#279#Lack of knowledge about new techniques of attack.#NULL#0#NULL
355#NULL#279#Inability to assess whether the information security process remains relevant, appropriate and effective.#NULL#0#NULL
356#NULL#279#Inability to identify opportunities of improvement security processes.#NULL#0#NULL
357#NULL#279#Inability to identify if the information security processes are being properly executed.#NULL#0#NULL
358#NULL#100#Unauthorized access of external parties to company information, compromising the information security.#NULL#0#NULL
363#NULL#100#Unavailability of access to external parties when it would be necessary.#NULL#0#NULL
377#NULL#100#Compromising of information caused by clients unauthorized access.#NULL#0#NULL
382#NULL#100#Loss of clients information.#NULL#0#NULL
383#NULL#100#Lack of information security commitment by third party.#NULL#0#NULL
387#NULL#100#Unauthorized access by the third parties.#NULL#0#NULL
391#NULL#100#Lack of knowledge by third parties of their job's security requirements.#NULL#0#NULL
398#NULL#100#Compromising of information managed by third parties.#NULL#0#NULL
402#NULL#100#Compromising of information stored or processed in third parties environment.#NULL#0#NULL
412#NULL#228#Inability by the third party to recover their service level according to the business continuity planning.#NULL#0#NULL
416#NULL#228#Problems due to faulty provisions of security by the third parties.#NULL#0#NULL
420#NULL#279#Lack of knowledge of information assets existent in organization.#NULL#0#NULL
421#NULL#279#Lack of knowledge about the location of critical security assets.#NULL#0#NULL
422#NULL#279#Lack of knowledge about which assets must be restored after a disaster.#NULL#0#NULL
423#NULL#182#Inability to identify theft or loss of information assets.#NULL#0#NULL
425#NULL#279#Failure in protection due to lack of knowledge about the importance of information assets.#NULL#0#NULL
426#NULL#279#Failure in protecting information assets due to lack of definition about who is responsible for the protection.#NULL#0#NULL
427#NULL#279#Failure in information assets protection due to lack of detailed knowledge about the assets.#NULL#0#NULL
428#NULL#279#Conflict of interests between security staff and users of certain information assets.#NULL#0#NULL
429#NULL#128#Inappropriate use of information assets.#NULL#0#NULL
433#NULL#128#Misuse of information assets by lack of clear rules about the appropriate and permitted  use.#NULL#0#NULL
438#NULL#100#Inability to understand how sensitive or important are specific information.#NULL#0#NULL
439#NULL#100#Inability by the users to decide how to deal with certain information.#NULL#0#NULL
440#NULL#100#Manipulation of information in a manner inconsistent with their value and importance.#NULL#0#NULL
441#NULL#100#Inability by the users to identify the level of classification of certain information.#NULL#0#NULL
442#NULL#100#Information treatment incompatible with the level of the assigned classification.#NULL#0#NULL
446#NULL#100#Inappropriate information treatment by third parties.#NULL#0#NULL
447#NULL#100#Inability by the users to identify the importance and security level required for the information.#NULL#0#NULL
448#NULL#255#Lack of knowledge of each employee about their responsibilities in the information security management process.#NULL#0#NULL
450#NULL#279#Lack of clear definitions about the responsibilities for certain activities.#NULL#0#NULL
451#NULL#255#Recruitment of employees unable to fulfill its responsibilities of information security.#NULL#0#NULL
452#NULL#255#Recruitment and grant access to critical systems for potential fraudsters.#NULL#0#NULL
453#NULL#255#Recruitment of people with a past history that violate the law.#NULL#0#NULL
454#NULL#255#Access to personal information of candidates to a job by unauthorized people.#NULL#0#NULL
457#NULL#255#Security problems caused by employees or by third parties that do not agree with their information security responsibilities.#NULL#0#NULL
458#NULL#255#Undue remote access to internal network by employees and/or third parties.#NULL#0#NULL
459#NULL#255#Inability to legally prove that the employee or third party agreed with their information security responsibilities.#NULL#0#NULL
460#NULL#279#Poor management that may cause personnel to feel undervalued.#NULL#0#NULL
461#NULL#279#Inability to implement security actions due to lack of support and comprehension from the management about security requirements.#NULL#0#NULL
462#NULL#279#Lack of motivation of employees, suppliers and third parties for the security process due to lack of a clear request from the management.#NULL#0#NULL
463#NULL#279#Lack of motivation of employees and other collaborators for the information security process.#NULL#0#NULL
464#NULL#255#Lack of awareness on the importance and the need for ensuring the information security.#NULL#0#NULL
465#NULL#255#Lack of knowledge on the meaning of information security.#NULL#0#NULL
466#NULL#255#Lack of knowledge about the information security policy and the need of its application.#NULL#0#NULL
467#NULL#255#Security problems caused by new employees or third parties that have had access to information and systems before they have been properly trained.#NULL#0#NULL
468#NULL#255#Lack of knowledge about the security standards that need to be applied to the correct information protection.#NULL#0#NULL
469#NULL#255#Lack of ability to attend and perform the information security functions.#NULL#0#NULL
470#NULL#255#Inability to recognize an information security incident.#NULL#0#NULL
471#NULL#255#Lack of knowledge about new policies, standards and procedures caused by lack of updates of security training.#NULL#0#NULL
472#NULL#279#Recurrence of security problems due to lack of proper treatment with those who violate security policies and standards.#NULL#0#NULL
473#NULL#279#Inability to treat security polices violations due to lack of a formal disciplinary process.#NULL#0#NULL
474#NULL#255#Lack of commitment to the information security.#NULL#0#NULL
475#NULL#255#Disclosure of confidential information after contract termination.#NULL#0#NULL
476#NULL#255#Lack of updates of responsibilities for security information after a change of function or position within the company.#NULL#0#NULL
477#NULL#255#Unauthorized access to systems and information by users or third parties whose contract has expired.#NULL#0#NULL
478#NULL#255#Lack of knowledge about new responsibilities and security functions in case of  a change of function or position within the company.#NULL#0#NULL
479#NULL#255#Retention of information assets granted by the organization for employees, or third parties, or service providers, after termination of contract.#NULL#0#NULL
483#NULL#255#Compromising of  information after resignation or termination of contract.#NULL#0#NULL
546#NULL#100#Unauthorized access to information by storing it in inappropriate place.#NULL#0#NULL
619#NULL#100#Unauthorized access to information through removal of important organization's assets.#NULL#0#NULL
620#NULL#228#Wrong  execution  of operating services due to lack of formal documented procedures for system activities.#NULL#0#NULL
621#NULL#228#Interruption of services due to unplanned changes.#NULL#0#NULL
622#NULL#128#Execution of unauthorized changes.#NULL#0#NULL
623#NULL#182#Execution of unauthorized changes.#NULL#0#NULL
624#NULL#228#Execution of unauthorized changes.#NULL#0#NULL
625#NULL#255#Execution of unauthorized changes.#NULL#0#NULL
626#NULL#228#Inability to recover a service after a failure in performed changes.#NULL#0#NULL
628#NULL#128#New vulnerabilities due to changes.#NULL#0#NULL
629#NULL#128#Misuse of systems.#NULL#0#NULL
630#NULL#128#Accidental misuse of systems.#NULL#0#NULL
631#NULL#100#Unauthorized change of information.#NULL#0#NULL
671#NULL#228#Changes in third party service provider.#NULL#0#NULL
673#NULL#228#Inability of the service provider to attend the improvement requirements.#NULL#0#NULL
674#NULL#228#Insufficient system performance.#NULL#0#NULL
683#NULL#228#Inability to manage systems and services due to key person dependency.#NULL#0#NULL
723#NULL#128#Computer virus contamination.#NULL#0#NULL
728#NULL#241#Computer virus contamination.#NULL#0#NULL
744#NULL#128#Loss of critical software#NULL#0#NULL
750#NULL#279#Policy back-up copies incompatible with business continuity plans.#NULL#0#NULL
783#NULL#100#Unauthorized access to information due to lack of restriction in access consistent with the sensitive information level.#NULL#0#NULL
784#NULL#100#Access to sensitive information by unauthorized recipients.#NULL#0#NULL
818#NULL#273#Defamation through communication facilities.#NULL#0#NULL
819#NULL#100#Unauthorized access to sensitive information left on printers.#NULL#0#NULL
821#NULL#100#Unauthorized access to sensitive information left on copiers.#NULL#0#NULL
822#NULL#100#Unauthorized access to sensitive information left on fax machines.#NULL#0#NULL
827#NULL#255#Accidental reveal of sensitive information to immediate vicinity when making a phone call.#NULL#0#NULL
883#NULL#273#Incorrect or unauthorized information in systems directly accessible from the Internet.#NULL#0#NULL
1451#NULL#279#Inability of management to identify occurrence of information security events.#NULL#0#NULL
1452#NULL#279#Inability to respond security events.#NULL#0#NULL
1453#NULL#255#Lack of knowledge about the appropriate channels to communication security events.#NULL#0#NULL
1454#NULL#255#Lack of notification of occurrences, by lack of knowledge in what should be considered security information events.#NULL#0#NULL
1455#NULL#279#Lack of knowledge about security weaknesses in the environment.#NULL#0#NULL
1456#NULL#255#Inability to identify security weaknesses in the environment.#NULL#0#NULL
1457#NULL#255#Lack of knowledge about the correct channels for the communication of information security weaknesses.#NULL#0#NULL
1458#NULL#279#Security weaknesses not previously detected.#NULL#0#NULL
1459#NULL#279#Inadequate response to information security incidents.#NULL#0#NULL
1460#NULL#279#Delay in response to information security incidents.#NULL#0#NULL
1461#NULL#279#Information security incidents responded by inadequate people.#NULL#0#NULL
1462#NULL#279#Failure in response to security incidents caused by the inability to assume the responsibilities for the occurrence of events or for problems resolution.#NULL#0#NULL
1463#NULL#279#Inability to respond to security incidents.#NULL#0#NULL
1464#NULL#279#Recurrence of security incidents.#NULL#0#NULL
1465#NULL#279#Inability to quantify the costs impact for the occurrence of information security incidents.#NULL#0#NULL
1466#NULL#279#Inability to measure the effectiveness of security controls based on the occurrence of security incidents.#NULL#0#NULL
1467#NULL#279#Inability to identify the continual improvement of security controls based on the occurrence of incidents.#NULL#0#NULL
1468#NULL#279#Difficulty in a critical analysis of the information security management process due to lack of evaluation and accounting of security incidents.#NULL#0#NULL
1469#NULL#279#Inability to identify the causes of security incidents due to lack of evidences.#NULL#0#NULL
1470#NULL#279#Inability to bring an action (civil or criminal) against the responsible (person and/or organization) for the security incidents due to lack of evidences.#NULL#0#NULL
1471#NULL#279#Inability to ensure the admissibility of evidences collected.#NULL#0#NULL
1472#NULL#279#Compromising of the integrity of evidence. #NULL#0#NULL
1473#NULL#279#Impossibility to collect evidences in environments outside the organization jurisdiction.#NULL#0#NULL
1474#NULL#100#Stop critical business processes due to unavailability of the asset.#NULL#0#NULL
1475#NULL#128#Stop critical business processes due to unavailability of the asset.#NULL#0#NULL
1476#NULL#182#Stop critical business processes due to unavailability of the asset.#NULL#0#NULL
1477#NULL#255#Stop critical business processes due to unavailability of the asset.#NULL#0#NULL
1478#NULL#228#Stop critical business processes due to unavailability of the asset.#NULL#0#NULL
1479#NULL#182#Stop critical business processes due to failure in equipment.#NULL#0#NULL
1480#NULL#255#Stop critical business processes due to human errors.#NULL#0#NULL
1481#NULL#273#Stop of services by unavailability of information assets.#NULL#0#NULL
1482#NULL#279#Impact on execution of critical business process after information security incidents.#NULL#0#NULL
1483#NULL#279#Security failure in processes working in a contingency regime due to the fact of not having the same security controls of normal processes.#NULL#0#NULL
1484#NULL#255#Incapacity to reach job facilities due to social disturbance.#NULL#0#NULL
1485#NULL#279#Lack of knowledge about events and incidents involving information security.#NULL#0#NULL
1486#NULL#279#Lack of knowledge about the impact of events and incidents of information security on the critical business processes.#NULL#0#NULL
1487#NULL#279#Lack of a comprehensive strategy to ensure the recovery of the main processes.#NULL#0#NULL
1488#NULL#279#Inability to recover the main business processes in accordance with the continuity strategy due to lack of knowledge of people involved.#NULL#0#NULL
1489#NULL#279#Inability to recover the main business processes in time due to lack of knowledge of people involved about which action should be done.#NULL#0#NULL
1490#NULL#279#Errors in executing the continuity strategies due to lack of documentation about what actions should be done by people involved.#NULL#0#NULL
1491#NULL#279#Inability to meet the continuity plans and strategies developed due to lack of adequate training.#NULL#0#NULL
1492#NULL#255#Inability to recover critical business processes due to difficulty in crisis management. #NULL#0#NULL
1494#NULL#279#Lack of the necessary resources in remote sites for recovery of the main processes.#NULL#0#NULL
1495#NULL#279#Unavailability of access the continuity plans and business due to loss of plans.#NULL#0#NULL
1496#NULL#279#Difficulty to execute the business continuity plans due to lack of standardization among the plans.#NULL#0#NULL
1497#NULL#279#Failure in putting into action the business continuity plans, due to lack of clear conditions in which the plan should be activated.#NULL#0#NULL
1498#NULL#279#Failure in putting into action the business continuity plans, due to lack of emergency procedures that should be done just after the activation conditions had been confirmed.#NULL#0#NULL
1499#NULL#279#Failure in putting into action the business continuity plans, due to lack of procedures that describe the actions to recovery of critical services and assets.#NULL#0#NULL
1500#NULL#279#Failure in execution of the Business Continuity Plan due to lack of responsibilities clearly defined.#NULL#0#NULL
1501#NULL#279#Failure in execution of the Business Continuity Plan due to not updating these plans.#NULL#0#NULL
1502#NULL#279#Failure in execution of the Business Continuity Plan due to lack of tests that ensure the correct and proper function as it has been required.#NULL#0#NULL
1503#NULL#279#Violation of laws, contracts, statutes, and regulations that involve the information security due to unknown legal requirements#NULL#0#NULL
1504#NULL#279#Violation of laws, contracts, statutes, and regulations that involve the information security due to failure in the communication of legal requirements to people responsible for information systems.#NULL#0#NULL
1505#NULL#128#Violation of intellectual property rights in the use of softwares.#NULL#0#NULL
1506#NULL#100#Violation of intellectual property rights in the use of information.#NULL#0#NULL
1507#NULL#255#Loss of intellectual property rights or information developed in the organization due to lack of clear specification of such rights in contracts.#NULL#0#NULL
1508#NULL#255#Violation of intellectual property rights by users when copying softwares, images, music or any other information to the company's computers.#NULL#0#NULL
1509#NULL#100#Loss of important records.#NULL#0#NULL
1510#NULL#100#Falsification of important records.#NULL#0#NULL
1511#NULL#100#Unauthorized access to important records due to the application of insufficient security controls.#NULL#0#NULL
1512#NULL#100#Inability to recover important record stored in media by long period of time.#NULL#0#NULL
1513#NULL#100#Insufficient retention periods for important record.#NULL#0#NULL
1514#NULL#100#Inability to recover important records due to storage in proprietary files format and loss of recovery software.#NULL#0#NULL
1515#NULL#102#Unauthorized disclosure of data and personal information.#NULL#0#NULL
1516#NULL#102#Unauthorized access to data and personal information.#NULL#0#NULL
1517#NULL#102#Capture of personal information through illegal mechanisms.#NULL#0#NULL
1518#NULL#102#Undue use of personal information.#NULL#0#NULL
1519#NULL#128#Security fault by inappropriate use of information asset.#NULL#0#NULL
1523#NULL#128#Misuse of information assets due to non-existence of clear rules about the adequate and permitted use.#NULL#0#NULL
1527#NULL#279#Violation of legal aspects during the monitoring of  the use of information processing facilities.#NULL#0#NULL
1540#NULL#279#Failure in execution of information security procedures.#NULL#0#NULL
1541#NULL#128#Failure in implementation of technical aspects required by the Standards and security information procedures.#NULL#0#NULL
1542#NULL#182#Failure in implementation of technical aspects required by the Standards and security information procedures.#NULL#0#NULL
1543#NULL#228#Failure in implementation of technical aspects required by the Standards and security information procedures.#NULL#0#NULL
1544#NULL#279#Interruption of business processes due to the information systems audit.#NULL#0#NULL
1545#NULL#128#Unauthorized use of specific tools for system audits.#NULL#0#NULL
1546#NULL#182#Unauthorized use of specific tools for system audits.#NULL#0#NULL
1547#NULL#228#Unauthorized use of specific tools for system audits.#NULL#0#NULL
6067#NULL#100#Loss of critical information.##0#
6068#NULL#100#Inadequate treatment of information due to conflicts with the criticality of the information.##0#
6070#NULL#100#Compromising of information stored on media due to inappropriate storage.##0#
6071#NULL#100#Loss of information stored for long periods of time due to media deterioration.##0#
6072#NULL#100#Unauthorized access to information stored in disposal of medias.##0#
6073#NULL#100#Unauthorized access to information storage medias.##0#
6074#NULL#100#Unauthorized remove of information stored in medias to outside the organization.##0#