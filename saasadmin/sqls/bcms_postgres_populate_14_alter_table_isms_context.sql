-- alterada a sequencia da chave da tabela isms_context
-- para pular os ids da dos contextos das bibliotecas criadas pelos scripts sql;

ALTER SEQUENCE isms_context_pkcontext_seq RESTART WITH 150000;

-- altera a sequencia das chaves da tabela de classificação dos contextos do isms
-- para pular os ids inseridos no script de populate_config do isms
ALTER SEQUENCE isms_context_classification_pkclassification_seq RESTART WITH 200;

--alterada a sequencia dos parametros e valores de parametros de risco e controle
--para evitar o erro que acontece ao criar novos valores de parâmetros
ALTER SEQUENCE rm_parameter_name_pkparametername_seq RESTART WITH 10;
ALTER SEQUENCE rm_parameter_value_name_pkvaluename_seq RESTART WITH 10;
ALTER SEQUENCE rm_rc_parameter_value_name_pkrcvaluename_seq RESTART WITH 10;
