INSERT INTO isms_context (pkContext,nType,nState) VALUES (300,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (300,279,'Different interpretations about the meaning of information security.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,300,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,300,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (301,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (301,279,'Lack of company''s directions in relation to information security.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,301,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,301,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (302,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (302,279,'Divergence between the information security objectives and the business objectives.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,302,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,302,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (303,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (303,279,'Unclear focus on the information security management.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,303,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,303,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (304,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (304,279,'Security problems caused by the non-existence of a formal policy that defines the approach to the information security management.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,304,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,304,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (305,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (305,279,'Lack of comprehension by the staff about how important the information security is.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,305,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,305,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (307,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (307,279,'Outdated Security Policy regarding the importance and the need of business objectives.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,307,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,307,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (308,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (308,279,'Inefficiency of Security Policy as an instrument of orientation to the information security management process.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,308,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,308,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (309,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (309,279,'Lack of management commitment to initiatives of information security.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,309,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,309,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (310,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (310,279,'Lack of knowledge about information security processes and responsibilities by the management.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,310,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,310,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (311,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (311,279,'Lack of responsibilities definition for the information security due to lack of a clear definition by the management.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,311,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,311,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (312,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (312,279,'Lack of strategic alignment between the information security and the business objectives.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,312,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,312,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (313,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (313,279,'Lack of credibility in the management process of information security due to lack of support from the management.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,313,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,313,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (314,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (314,279,'Lack of people with expertise to conduct the process of information security management.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,314,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,314,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (315,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (315,279,'Lack of resources for the fulfillment of the requirements of information security.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,315,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,315,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (316,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (316,279,'Difficulty in implementation of security controls due to lack of coordination with relevant functions and roles.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,316,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,316,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (317,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (317,279,'Misalignment between actions of information security and the security policy.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,317,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,317,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (318,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (318,279,'Lack of support for the information security process by the key roles in the company.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,318,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,318,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (319,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (319,279,'Difficulty in conducting and solving non-conformities of information security processes.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,319,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,319,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (320,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (320,279,'Inability to homologate methods for risk management.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,320,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,320,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (321,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (321,279,'Security problems due to lack of definition of responsibilities for information security processes.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,321,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,321,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (322,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (322,279,'Lack of controls implementation due to lack of clear definition of responsibilities.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,322,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,322,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (324,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (324,255,'Difficulty by people in identifying and recognizing their information security responsibilities.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,324,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,324,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (325,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (325,279,'Security problems caused by divergences about who is the responsible for the information security.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,325,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,325,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (326,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (326,101,'Information processing in environments  not properly controlled.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,326,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,326,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (327,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (327,229,'Information processing in environments  not properly controlled.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,327,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,327,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (328,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (328,235,'Information processing in environments  not properly controlled.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,328,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,328,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (334,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (334,101,'Information processing in notebooks and devices, personal or private, that do not offer appropriate security levels.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,334,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,334,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (335,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (335,229,'Insertions of new hardware and/or software not compatible with the rest of system devices.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,335,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,335,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (336,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (336,235,'Insertions of new hardware and/or software not compatible with the rest of system devices.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,336,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,336,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (343,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (343,255,'Unauthorized disclosure of sensitive information during the contracting period.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,343,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,343,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (344,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (344,270,'Unauthorized information disclosure by the suppliers.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,344,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,344,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (345,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (345,271,'Unauthorized information disclosure by the suppliers.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,345,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,345,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (347,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (347,255,'Unauthorized disclosure of information after the contract is finished.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,347,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,347,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (348,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (348,279,'Lack of knowledge about the need to contact the authorities when a relevant security incident occurs.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,348,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,348,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (349,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (349,279,'Lack of knowledge about the need to contact the authorities when a relevant security incident occurs.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,349,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,349,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (350,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (350,279,'Difficulty in contacting authorities due to lack of contact information.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,350,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,350,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (351,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (351,235,'Difficulty in dealing with external attacks due to a difficulty in contacting the proper authorities, such as telecommunication providers and incident response centers.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,351,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,351,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (352,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (352,279,'Lack of knowledge of new vulnerabilities due to lack of contact with research groups and services of vulnerabilities notification.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,352,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,352,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (353,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (353,279,'Lack of specialists in information security.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,353,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,353,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (354,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (354,279,'Lack of knowledge about new techniques of attack.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,354,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,354,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (355,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (355,279,'Inability to assess whether the information security process remains relevant, appropriate and effective.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,355,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,355,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (356,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (356,279,'Inability to identify opportunities to improve the security processes.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,356,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,356,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (357,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (357,279,'Inability to identify if the information security processes are being properly executed.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,357,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,357,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (359,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (359,270,'Unauthorized access by external parties due to interconnecting to the company information, compromising the information security.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,359,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,359,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (360,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (360,271,'Unauthorized access by external parties due to interconnecting to the company information, compromising the information security.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,360,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,360,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (362,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (362,236,'Damage to the internal network through incidents in networks of external parties that are connected to the internal network.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,362,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,362,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (363,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (363,100,'Unavailability of access to external parties.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,363,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,363,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (364,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (364,270,'Unavailability of access to external parties.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,364,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,364,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (365,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (365,271,'Unavailability of access to external parties.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,365,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,365,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (367,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (367,236,'Remote access not authorized due to security failure of external parties that access the internal network.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,367,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,367,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (368,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (368,270,'Remote access not authorized due to security failure of external parties that access the internal network.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,368,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,368,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (369,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (369,271,'Remote access not authorized due to security failure of external parties that access the internal network.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,369,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,369,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (371,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (371,270,'Lack of knowledge by external parties, of their responsibilities related to the information security.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,371,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,371,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (372,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (372,271,'Lack of knowledge by external parties, of their responsibilities related to the information security.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,372,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,372,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (374,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (374,270,'Lack of knowledge of the risks related to being engaged with external parties.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,374,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,374,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (375,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (375,271,'Lack of knowledge of the risks related to being engaged with external parties.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,375,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,375,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (377,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (377,100,'Compromising of information caused by client unauthorized access.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,377,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,377,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (378,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (378,236,'Unauthorized access by clients to the internal network.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,378,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,378,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (385,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (385,270,'Lack of commitment by third party to information security.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,385,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,385,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (386,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (386,271,'Lack of commitment by third party to information security.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,386,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,386,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (387,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (387,100,'Unauthorized access by the third parties.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,387,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,387,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (388,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (388,269,'Unauthorized access by the third parties.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,388,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,388,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (389,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (389,270,'Unauthorized access by the third parties.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,389,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,389,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (390,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (390,271,'Unauthorized access by the third parties.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,390,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,390,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (392,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (392,269,'Lack of knowledge by third parties of their job''s security requirements.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,392,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,392,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (393,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (393,270,'Lack of knowledge by third parties of their job''s security requirements.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,393,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,393,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (394,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (394,271,'Lack of knowledge by third parties of their job''s security requirements.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,394,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,394,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (395,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (395,269,'Inability by the third party to attend to the security requirements.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,395,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,395,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (396,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (396,270,'Inability by the third party to attend to the security requirements.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,396,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,396,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (397,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (397,271,'Inability by the third party to attend to the security requirements.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,397,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,397,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (398,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (398,100,'Disruption of information managed by third parties.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,398,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,398,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (399,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (399,269,'Disruption of information managed by third parties.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,399,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,399,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (400,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (400,270,'Disruption of information managed by third parties.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,400,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,400,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (401,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (401,271,'Disruption of information managed by third parties.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,401,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,401,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (402,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (402,100,'Disruption of information stored or processed in third parties environment.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,402,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,402,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (403,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (403,269,'Disruption of information stored or processed in third parties environment.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,403,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,403,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (404,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (404,270,'Disruption of information stored or processed in third parties environment.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,404,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,404,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (405,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (405,271,'Disruption of information stored or processed in third parties environment.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,405,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,405,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (406,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (406,269,'Inability by the third party to deal with security incidents.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,406,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,406,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (407,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (407,270,'Inability by the third party to deal with security incidents.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,407,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,407,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (408,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (408,271,'Inability by the third party to deal with security incidents.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,408,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,408,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (409,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (409,269,'Lack of knowledge by the third party, of controls that must be applied to protect information.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,409,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,409,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (410,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (410,270,'Lack of knowledge by the third party, of controls that must be applied to protect information.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,410,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,410,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (411,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (411,271,'Lack of knowledge by the third party, of controls that must be applied to protect information.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,411,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,411,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (412,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (412,228,'Inability by the third parties to recover their service level according to the Business Continuity Planning.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,412,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,412,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (413,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (413,269,'Inability by the third parties to recover their service level according to the Business Continuity Planning.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,413,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,413,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (414,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (414,270,'Inability by the third parties to recover their service level according to the Business Continuity Planning.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,414,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,414,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (415,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (415,271,'Inability by the third parties to recover their service level according to the Business Continuity Planning.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,415,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,415,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (416,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (416,228,'Problems due to faulty provisions of security by the third parties.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,416,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,416,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (417,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (417,269,'Problems due to faulty provisions of security by the third parties.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,417,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,417,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (418,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (418,270,'Problems due to faulty provisions of security by the third parties.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,418,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,418,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (419,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (419,271,'Problems due to faulty provisions of security by the third parties.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,419,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,419,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (420,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (420,279,'Lack of knowledge of information assets existent in the organization.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,420,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,420,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (421,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (421,279,'Lack of knowledge about the location of critical security assets.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,421,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,421,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (422,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (422,279,'Lack of knowledge about which assets must be restored after a disaster.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,422,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,422,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (423,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (423,182,'Inability to identify theft or loss of information assets.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,423,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,423,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (424,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (424,213,'Inability to identify hardware or software changes in information assets.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,424,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,424,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (425,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (425,279,'Failure in protection due to lack of knowledge about the importance of information assets.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,425,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,425,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (426,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (426,279,'Failure in protecting information assets due to lack of definition about who is responsible for this protection.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,426,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,426,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (427,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (427,279,'Failure in information assets protection due to lack of detailed knowledge about the assets and how they work.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,427,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,427,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (428,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (428,279,'Conflict of interests between security staff and users of certain information assets.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,428,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,428,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (430,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (430,213,'Inappropriate use of information assets.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,430,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,430,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (431,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (431,229,'Inappropriate use of information assets.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,431,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,431,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (435,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (435,229,'Misuse of information assets due to lack of clear rules about the appropriate and permitted use.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,435,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,435,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (436,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (436,235,'Misuse of information assets due to lack of clear rules about the appropriate and permitted use.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,436,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,436,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (438,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (438,100,'Inability to understand how sensitive and important are some information.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,438,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,438,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (439,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (439,100,'Inability by the users to decide how to deal with certain information.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,439,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,439,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (440,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (440,100,'Manipulation of information in a manner inconsistent with its value and importance.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,440,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,440,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (441,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (441,100,'Inability by the users to identify the level of classification of certain information.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,441,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,441,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (442,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (442,100,'Information treatment incompatible with the level of the assigned classification.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,442,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,442,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (446,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (446,100,'Inappropriate information treatment by third parties.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,446,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,446,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (447,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (447,100,'Inability by the users to identify the importance and security level required for the information.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,447,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,447,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (448,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (448,255,'Lack of knowledge of each employee about their responsibilities in the information security management process.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,448,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,448,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (450,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (450,279,'Lack of clear definitions about the responsibilities for certain activities.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,450,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,450,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (451,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (451,255,'Recruitment of employees unable to fulfill its responsibilities of information security.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,451,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,451,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (452,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (452,255,'Recruitment and access grant to critical systems for potential fraudsters.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,452,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,452,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (453,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (453,255,'Recruitment of people with a past history that violate the law.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,453,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,453,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (455,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (455,271,'Grant access of information and critical systems to third parties whose background has not been adequately evaluated by the company providing the services.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,455,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,455,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (456,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (456,269,'Granted access of information and critical systems to third parties whose background has not been adequately evaluated by the company providing the services.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,456,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,456,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (457,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (457,255,'Security problems caused by employees or by third parties that do not agree with their information security responsibilities.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,457,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,457,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (459,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (459,255,'Inability to legally prove that the employee or third party agreed with their information security responsibilities.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,459,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,459,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (460,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (460,279,'Poor management that may cause lack of engagement by people.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,460,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,460,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (461,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (461,279,'Inability to implement security actions due to lack of support and comprehension from the management about security requirements.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,461,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,461,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (462,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (462,279,'Lack of motivation of employees, suppliers and third parties for the security process due to lack of a clear request from the management.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,462,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,462,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (463,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (463,279,'Lack of motivation of employees and other collaborators for the information security process.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,463,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,463,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (464,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (464,255,'Lack of awareness on the importance and the need of ensuring the information security.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,464,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,464,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (465,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (465,255,'Lack of knowledge on the meaning of information security.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,465,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,465,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (466,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (466,255,'Lack of knowledge about the information security policy and the need of its application.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,466,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,466,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (467,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (467,255,'Security problems caused by new employees or third parties that have had access to information and systems before they have been properly trained.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,467,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,467,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (468,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (468,255,'Lack of knowledge about the security standards that need to be applied to the correct information protection.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,468,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,468,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (469,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (469,255,'Lack of ability to attend and perform the information security functions.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,469,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,469,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (470,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (470,255,'Inability to recognize an information security incident.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,470,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,470,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (471,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (471,255,'Lack of knowledge about new policies, standards and procedures caused by lack of updates of security training.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,471,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,471,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (472,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (472,279,'Recurrence of security problems due to lack of proper treatment with those that violate security policies and standards.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,472,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,472,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (473,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (473,279,'Inability to treat security polices violations due to lack of a formal disciplinary process.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,473,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,473,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (474,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (474,255,'Lack of commitment to the information security.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,474,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,474,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (475,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (475,255,'Disclosure of secret information after the contract termination with the organization.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,475,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,475,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (476,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (476,255,'Lack of updates of responsibilities for security information after a change of function or position within the company.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,476,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,476,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (477,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (477,255,'Unauthorized access to systems and information by users or third parties whose contract has expired, or by ignorance of the employees about the contract termination.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,477,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,477,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (478,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (478,255,'Lack of knowledge about new responsibilities and security functions in case of a change of function or position within the company.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,478,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,478,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (479,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (479,255,'Retention of information assets granted by the organization for employees, or third parties, or service providers, after termination of contract.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,479,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,479,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (483,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (483,255,'Compromising of information after resignation or termination of contract.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,483,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,483,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (488,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (488,213,'Unauthorized access to systems and information by users whose contract with the organization has been terminated.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,488,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,488,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (489,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (489,229,'Unauthorized access to systems and information by users whose contract with the organization has been terminated.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,489,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,489,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (498,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (498,183,'Unauthorized access to infrastructure users whose contract with the organization has been terminated.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,498,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,498,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (503,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (503,213,'Undue or unauthorized access to systems and information caused by lack of access rights maintenance or privileges of employees who changed their position and / or function.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,503,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,503,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (504,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (504,229,'Undue or unauthorized access to systems and information caused by lack of access rights maintenance or privileges of employees who changed their position and / or function.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,504,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,504,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (509,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (509,235,'Undue or unauthorized access to systems and information caused by lack of access rights maintenance or privileges of employees who changed their position and / or function.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,509,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,509,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (514,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (514,183,'Unauthorized access to physical environments through fragile walls and materials used for division of environments.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,514,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,514,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (515,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (515,183,'Unauthorized access to physical environments through unprotected windows.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,515,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,515,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (516,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (516,183,'Unauthorized access to physical environments caused by lack of division and control among areas of different levels of criticality.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,516,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,516,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (517,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (517,183,'Unauthorized access to physical environments caused by lack of ability to define the level of physical protection required for a particular area.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,517,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,517,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (518,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (518,183,'Unauthorized access to physical environments through emergency doors.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,518,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,518,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (519,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (519,183,'Unauthorized access to critical information processing areas due to the sharing of the area with other non-related activities.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,519,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,519,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (520,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (520,183,'Violations of fire combat rules by poor implementation of security controls.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,520,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,520,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (521,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (521,183,'Unauthorized access to security areas caused by lack of control of physical access.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,521,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,521,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (522,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (522,183,'Inability to determine accesses because of lack of proper registration.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,522,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,522,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (523,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (523,183,'Inability to identify unauthorized people in restricted areas.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,523,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,523,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (524,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (524,183,'Unauthorized access to physical environments through incorrectly configured access rights.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,524,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,524,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (525,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (525,183,'Unauthorized access to physical environments because of its proximity to public access.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,525,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,525,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (526,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (526,183,'Unauthorized access to physical environments due to opened environments where there are critical information.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,526,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,526,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (527,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (527,183,'Compromising of the physical environment due to the occurrence of fire in neighbor places.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,527,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,527,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (528,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (528,183,'Compromising of the physical environment due to the occurrence of fire locally.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,528,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,528,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (529,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (529,183,'Compromising of the physical environment due to the occurrence of flood.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,529,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,529,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (530,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (530,183,'Compromising of the physical environment due to the occurrence of explosions.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,530,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,530,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (531,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (531,183,'Compromising of the physical environment due to the occurrence of disturbance of public order.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,531,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,531,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (532,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (532,183,'Compromising of the physical environment due to the occurrence of natural disasters.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,532,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,532,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (533,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (533,183,'Compromising of equipment caused by water leak or pipeline problems.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,533,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,533,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (534,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (534,183,'Compromising of security because of use of cameras or cell phones.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,534,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,534,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (535,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (535,183,'Compromising of security due to the use of video or audio recording devices.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,535,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,535,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (536,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (536,183,'Compromising of security because of bad-intentioned actions of visitors.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,536,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,536,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (537,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (537,183,'Security problems caused by lack of clear rules about how to work in secure areas.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,537,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,537,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (538,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (538,183,'Unauthorized access to deliveries and / or loadings areas.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,538,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,538,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (539,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (539,183,'Compromising of security through goods delivered and loaded to the interior of the organization.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,539,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,539,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (540,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (540,183,'Unauthorized removal of information assets.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,540,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,540,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (546,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (546,100,'Unauthorized access to information by storing it in an inappropriate place.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,546,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,546,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (566,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (566,207,'Theft of equipments caused by installation in inadequate places.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,566,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,566,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (573,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (573,253,'Unavailability of the resources by failure in the electric power supply.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,573,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,573,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (574,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (574,252,'Compromising of the resources by failure in the water supply.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,574,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,574,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (575,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (575,254,'Compromising of the resources caused by failure in the heating / ventilation system.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,575,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,575,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (576,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (576,254,'Compromising of the resources caused by failure in the air-conditioning system.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,576,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,576,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (577,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (577,253,'Power supply is less than necessary for the installed equipment.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,577,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,577,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (578,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (578,253,'Failure in UPS devices.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,578,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,578,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (580,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (580,252,'Failure in environment lighting.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,580,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,580,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (581,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (581,252,'Failure of the fire extinguisher equipment caused by a failure in water supply.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,581,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,581,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (582,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (582,252,'Failure of environment conditioning equipment caused by failure in water supply.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,582,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,582,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (585,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (585,235,'Interception of information through access to the cabling.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,585,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,585,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (586,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (586,253,'Interruption of electric energy supply caused by failure in the cabling.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,586,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,586,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (587,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (587,235,'Failure in the logical cabling interrupting the data communication.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,587,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,587,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (589,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (589,235,'Failure in the communications by electrical equipment interference.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,589,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,589,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (590,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (590,253,'Unauthorized access to energy control panels.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,590,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,590,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (591,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (591,235,'Unauthorized access to cabling control panels.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,591,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,591,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (594,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (594,252,'Lack of periodic maintenance');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,594,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,594,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (597,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (597,252,'Compromising of equipment caused by maintenance made by unqualified people.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,597,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,597,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (602,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (602,252,'Insurance loss due to not meeting established requirements about maintenance of equipment and/or structure.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,602,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,602,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (619,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (619,100,'Unauthorized access to information through removal of important organization''s assets.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,619,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,619,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (620,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (620,228,'Wrong  execution  of operating services due to lack of formal documented procedures for system activities.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,620,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,620,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (621,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (621,228,'Interruption of services due to unplanned changes.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,621,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,621,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (623,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (623,182,'Execution of unauthorized changes.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,623,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,623,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (624,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (624,228,'Execution of unauthorized changes.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,624,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,624,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (625,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (625,255,'Execution of unauthorized changes.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,625,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,625,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (626,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (626,228,'Inability to recover a service after a failure in performed changes.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,626,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,626,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (631,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (631,100,'Unauthorized change of information.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,631,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,631,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (633,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (633,236,'Unauthorized access to operational facilities information by developers.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,633,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,633,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (641,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (641,101,'Unauthorized access to sensitive information that has been compiled to the test facilities or development facilities.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,641,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,641,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (652,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (652,270,'Lack of service delivery stipulated by contract.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,652,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,652,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (653,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (653,271,'Inability to attend the business continuity plans.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,653,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,653,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (654,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (654,269,'Inability to attend the business continuity plans.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,654,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,654,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (655,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (655,270,'Inability to attend the business continuity plans.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,655,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,655,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (659,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (659,271,'Inability to identify and manage incidents occurrence in services provided by third party. ');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,659,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,659,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (660,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (660,269,'Inability to identify and manage incidents occurrence in services provided by third party. ');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,660,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,660,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (661,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (661,270,'Inability to identify and manage incidents occurrence in services provided by third party. ');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,661,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,661,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (671,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (671,228,'Changes in the third party service provider.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,671,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,671,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (673,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (673,228,'Inability of the service provider to attend improvement requirements.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,673,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,673,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (683,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (683,228,'Inability to manage systems and services due to key person dependency.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,683,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,683,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (684,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (684,213,'Inability to manage systems and services due to key person dependency.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,684,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,684,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (692,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (692,209,'Lack of space for storage.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,692,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,692,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (726,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (726,229,'Computer virus contamination.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,726,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,726,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (727,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (727,235,'Computer virus contamination.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,727,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,727,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (730,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (730,213,' Installation of Trojan horses"."');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,730,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,730,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (732,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (732,213,'Receivement of malicious code by e-mail.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,732,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,732,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (734,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (734,213,'Contamination of resources caused by insertion of infected files on computers.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,734,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,734,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (736,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (736,213,'Insertion of malicious code during emergency procedures in which security controls are not considered.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,736,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,736,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (738,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (738,213,'Contamination of information assets due to updates of malicious code detection and repair software.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,738,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,738,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (739,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (739,213,'Execution of unauthorized mobile code.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,739,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,739,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (740,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (740,213,'Inability to identify an attempt to execute mobile code.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,740,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,740,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (741,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (741,213,'Mobile code manipulation to execute unauthorized activities.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,741,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,741,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (742,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (742,213,'Misuse of local resources by the mobile code.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,742,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,742,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (749,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (749,209,'Inability to restore back-up copies due to deteriorating in the storage media.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,749,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,749,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (750,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (750,279,'Policy back-up copies incompatible with business continuity plans.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,750,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,750,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (753,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (753,236,'Unauthorized access to network through not an uncontrolled access point.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,753,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,753,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (754,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (754,236,'Unauthorized access to the network caused by an inadequate user management process.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,754,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,754,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (755,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (755,236,'Insertion of non-homologated equipments (personal resources, notebook of third party ...).');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,755,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,755,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (756,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (756,236,'Disclosure of information related to network due to DNS configuration failure.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,756,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,756,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (757,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (757,236,'Network access problems due to failure in DNS configuration.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,757,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,757,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (758,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (758,236,'Undue access through wrong configuration of network address service.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,758,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,758,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (759,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (759,236,'Network access problems due to failure at main services (AD, DHCP, DNS, NetBIOS...).');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,759,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,759,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (760,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (760,101,'Inappropriate sharing of files in network.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,760,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,760,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (761,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (761,236,'Inability to identify unauthorized actions in the network.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,761,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,761,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (762,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (762,236,'Network unavailability due to internal attack.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,762,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,762,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (763,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (763,236,'Overlap network security controls through the use of services that mask access.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,763,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,763,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (764,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (764,236,'Lack of security in external networks (cryptography, VPN, etc) due to lack of security controls.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,764,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,764,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (765,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (765,236,'Unavailability of network services.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,765,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,765,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (768,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (768,236,'Inappropriate network service management.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,768,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,768,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (771,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (771,209,'Unauthorized access to information storage medias.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,771,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,771,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (773,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (773,209,'Unauthorized removal of information stored in medias to outside the organization.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,773,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,773,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (775,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (775,209,'Compromising of information stored on media due to inappropriate storage.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,775,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,775,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (777,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (777,209,'Loss of information stored for long periods of time due to media deterioration.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,777,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,777,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (778,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (778,209,'Unauthorized copy of information through removable media drives unnecessary enabled.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,778,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,778,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (779,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (779,209,'Unauthorized copy of information through removable media drives enabled for business reason.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,779,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,779,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (781,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (781,209,'Unauthorized access to information stored in media disposal.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,781,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,781,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (783,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (783,100,'Unauthorized access to information due to lack of access restriction to sensitive information level.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,783,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,783,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (784,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (784,100,'Access to sensitive information by an unauthorized recipients.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,784,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,784,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (803,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (803,235,'Sending malicious code through communication facilities.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,803,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,803,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (805,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (805,235,'Unauthorized transmission of sensitive information in the form of attachment.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,805,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,805,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (806,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (806,230,'Unauthorized transmission of sensitive information in the form of attachment.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,806,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,806,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (807,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (807,235,'Receiving unauthorized information through files attached.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,807,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,807,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (809,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (809,235,'Receiving of unauthorized information through download of data.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,809,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,809,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (811,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (811,235,'Sending unauthorized information through upload of data.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,811,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,811,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (813,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (813,235,'Misuse of communication facilities.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,813,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,813,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (815,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (815,236,'Use of wireless communications without appropriate protection.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,815,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,815,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (818,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (818,273,'Defamation through communication facilities.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,818,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,818,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (820,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (820,234,'Unauthorized access to sensitive information left on printers.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,820,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,820,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (824,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (824,230,'Unauthorized sending of information through automatic forwarding of electronic mail .');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,824,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,824,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (825,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (825,235,'Unauthorized access to information through wiretapping and other forms of eavesdropping.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,825,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,825,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (827,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (827,255,'Accidental reveal of sensitive information to immediate vicinity when making a phone call.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,827,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,827,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (831,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (831,230,'Accidental sending of sensitive information by mistake in addressing e-mails.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,831,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,831,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (832,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (832,269,'Loss of information sent to external parties.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,832,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,832,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (834,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (834,271,'Loss of information sent to external parties.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,834,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,834,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (835,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (835,235,'Loss of information sent to external parties.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,835,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,835,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (836,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (836,235,'Not receiving information sent by external parties.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,836,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,836,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (837,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (837,235,'Unauthorized access to information shared with external parties during sending/receiving.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,837,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,837,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (839,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (839,209,'Theft of medias during transportation beyond the organization''s physical boundaries.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,839,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,839,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (841,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (841,209,'Damages caused by the use of inappropriate package to transport medias  beyond the organization''s physical boundaries.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,841,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,841,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (842,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (842,209,'Compromising of information contained in medias which are transported to outside the organization via the postal service or via courier.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,842,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,842,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (843,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (843,235,'Unauthorized access to electronic sent messages.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,843,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,843,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (844,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (844,235,'Incorrect addressing when sending electronic messages.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,844,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,844,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (845,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (845,235,'Failure in delivering electronic messages.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,845,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,845,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (846,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (846,235,'Inability to grant the authenticity of electronic messages.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,846,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,846,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (848,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (848,235,'Unauthorized disclosure of information through the use of services such as instant messaging or file sharing.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,848,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,848,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (883,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (883,273,'Incorrect or unauthorized information in systems directly accessible from the Internet.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,883,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,883,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (978,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (978,183,'Inadequate allocation of access rights and privileges due to lack of an access control policy.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,978,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,978,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (984,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (984,183,'Access rights violation due to lack of clear rules that establish what is permitted and what is forbidden.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,984,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,984,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (990,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (990,183,'Incompatibility between access rights granted and the real need of access based in business requirements.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,990,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,990,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (996,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (996,183,'Users with unnecessary access rights regarding their job roles.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,996,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,996,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (997,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (997,269,'Lack of knowledge about access rights by third parties, suppliers and service providers.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,997,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,997,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (998,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (998,270,'Lack of knowledge about access rights by third parties, suppliers and service providers.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,998,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,998,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (999,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (999,271,'Lack of knowledge about access rights by third parties, suppliers and service providers.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,999,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,999,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1115,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1115,213,'Unauthorized access through unattended equipment.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1115,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1115,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1116,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1116,213,'Unauthorized access to equipment during short periods of absence of employees.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1116,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1116,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1117,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1117,213,'Unauthorized access to equipment during periods outside normal working hours.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1117,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1117,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1118,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1118,213,'Lack of means to block unattended equipment.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1118,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1118,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1119,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1119,115,'Unauthorized access to sensitive information left on desks.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1119,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1119,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1120,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1120,101,'Unauthorized access to sensitive information through undue reading of computers'' screen.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1120,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1120,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1122,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1122,236,'Granting inappropriate access rights to network services.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1122,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1122,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1124,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1124,236,'Granting access rights to network services incompatible with the access controls.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1124,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1124,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1126,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1126,236,'Unauthorized dial-up connection to other networks');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1126,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1126,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1128,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1128,236,'Unauthorized connection of equipment to network.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1128,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1128,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1129,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1129,237,'Unauthorized remote access by network users.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1129,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1129,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1130,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1130,236,'Unauthorized access to wireless network.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1130,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1130,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1131,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1131,237,'Unauthorized remote access by attackers (hackers).');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1131,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1131,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1133,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1133,236,'Access to the network through identity counterfeit for specific equipment.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1133,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1133,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1134,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1134,213,'Unauthorized access to equipment through logical access to remote diagnostic and configuration ports.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1134,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1134,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1135,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1135,213,'Unauthorized access to equipment through physical access to remote diagnostic and configuration ports.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1135,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1135,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1136,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1136,213,'Unauthorized access during maintenance through remote diagnostic and support ports.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1136,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1136,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1137,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1137,236,'Unauthorized access through network services and critical servers.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1137,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1137,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1138,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1138,236,'Unauthorized access by external users to the internal network through server and services publicly available.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1138,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1138,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1139,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1139,236,'Unauthorized access to sensitive information that are transmitted in common networks.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1139,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1139,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1140,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1140,236,'Unauthorized access to services that handle sensitive information available in common networks.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1140,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1140,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1141,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1141,236,'Access to sensitive information that are unnecessarily transmitted in networks.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1141,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1141,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1142,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1142,236,'Inability to filter information transmitted in shared networks.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1142,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1142,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1143,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1143,236,'Unauthorized access to internal network through public networks, such as Internet.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1143,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1143,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1144,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1144,236,'Users from a network having unauthorized access to information of another network.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1144,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1144,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1145,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1145,236,'Attack to internal network and servers through public networks such as Internet.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1145,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1145,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1146,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1146,236,'Attack to internal network and servers through interconnected networks.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1146,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1146,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1152,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1152,236,'External denial-of-service attack.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1152,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1152,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1153,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1153,236,'Internal denial-of-service attack.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1153,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1153,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1154,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1154,236,'Unauthorized access to network through a faulty routing.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1154,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1154,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1155,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1155,236,'Failed to access external network due to routing failures.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1155,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1155,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1156,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1156,236,'Failed to access internal network due to routing failures.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1156,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1156,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1157,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1157,236,'Unauthorized access to the internal network equipment by poor configuration of network addresses..');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1157,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1157,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1158,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1158,236,'Unauthorized use of routing devices.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1158,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1158,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1159,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1159,236,'Inadequate routing of information transmitted through internal and external networks.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1159,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1159,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1203,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1203,213,'Unauthorized access to equipment during users'' absence.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1203,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1203,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1230,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1230,213,'Unauthorized access to information stored in mobile computers (notebooks, palmtops, cell phones) due to equipment theft.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1230,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1230,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1233,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1233,213,'Contamination of mobile resources by malicious code.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1233,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1233,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1234,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1234,213,'Loss of sensitive information by loss of mobile computing resources.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1234,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1234,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1235,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1235,213,'Theft of mobile computing equipments.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1235,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1235,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1236,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1236,237,'Compromising of information remotely accessed due to lack of security in remote environment.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1236,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1236,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1237,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1237,237,'Unauthorized remote access to the operational environment through unprotected teleworking.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1237,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1237,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1238,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1238,237,'Theft of equipment and information of teleworking site.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1238,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1238,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1239,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1239,237,'Interception of traffic transmitted and received during the teleworking.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1239,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1239,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1240,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1240,237,'Compromising of information by unrestricted access to equipment used for teleworking.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1240,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1240,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1241,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1241,237,'Use of personal equipment for teleworking activities.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1241,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1241,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1320,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1320,101,'Undue use of cryptographic controls.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1320,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1320,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1326,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1326,101,'Use of cryptographic controls for information in cases that it would not be necessary.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1326,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1326,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1327,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1327,101,'Lack of use of cryptographic controls for protection of sensitive information.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1327,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1327,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1329,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1329,101,'Lack of strength or quality of the encryption algorithm applied.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1329,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1329,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1336,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1336,101,'Inability to recover lost keys due to use of proprietary encryption techniques or without documentation.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1336,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1336,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1346,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1346,101,'Unauthorized access to cryptographic keys.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1346,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1346,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1352,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1352,101,'Distribution of cryptographic keys to unauthorized users.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1352,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1352,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1410,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1410,235,'Leak of information through the use and exploration of covert channels.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1410,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1410,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1451,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1451,279,'Inability of management to identify occurrence of information security events.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1451,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1451,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1452,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1452,279,'Inability to respond security events.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1452,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1452,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1453,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1453,255,'Lack of knowledge about the appropriate channels to communicate security events.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1453,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1453,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1454,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1454,255,'Lack of notification of occurrences, by lack of knowledge in what should be considered security information events.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1454,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1454,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1455,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1455,279,'Lack of knowledge about security weaknesses in the environment.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1455,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1455,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1456,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1456,255,'Inability to identify security weaknesses in the environment.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1456,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1456,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1457,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1457,255,'Lack of knowledge about the correct channels for the communication of information security weaknesses.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1457,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1457,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1458,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1458,279,'Security weaknesses not previously detected.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1458,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1458,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1459,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1459,279,'Inadequate response to information security incidents.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1459,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1459,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1460,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1460,279,'Delay in response to information security incidents.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1460,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1460,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1461,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1461,279,'Information security incidents responded by inadequate people.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1461,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1461,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1462,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1462,279,'Failure in response to security incidents caused by the inability to assume the responsibilities for the occurrence of events or for problems resolution.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1462,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1462,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1463,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1463,279,'Inability to respond to security incidents.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1463,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1463,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1464,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1464,279,'Recurrence of security incidents.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1464,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1464,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1465,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1465,279,'Inability to quantify the costs impact for the occurrence of information security incidents.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1465,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1465,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1466,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1466,279,'Inability to measure the effectiveness of security controls based on the occurrence of security incidents.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1466,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1466,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1467,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1467,279,'Inability to identify the continual improvement of security controls based on the occurrence of incidents.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1467,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1467,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1468,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1468,279,'Difficulty in a critical analysis of the information security management process due to lack of evaluation and accounting of security incidents.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1468,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1468,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1469,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1469,279,'Inability to identify the causes of security incidents due to lack of evidences.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1469,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1469,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1470,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1470,279,'Inability to bring an action (civil or criminal) against the responsible (person and/or organization) for the security incidents due to lack of evidences.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1470,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1470,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1471,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1471,279,'Inability to ensure the admissibility of evidences collected.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1471,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1471,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1473,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1473,279,'Impossibility to collect evidences in environments outside the organization jurisdiction.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1473,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1473,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1474,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1474,100,'Stop critical business processes due to unavailability of the asset.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1474,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1474,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1476,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1476,182,'Stop critical business processes due to unavailability of the asset.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1476,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1476,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1477,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1477,255,'Stop critical business processes due to unavailability of the asset.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1477,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1477,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1480,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1480,255,'Stop critical business processes due to human errors.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1480,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1480,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1481,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1481,273,'Stop of services by unavailability of information assets.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1481,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1481,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1482,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1482,279,'Impact on execution of critical business process after information security incidents.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1482,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1482,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1483,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1483,279,'Security failure in processes working in a contingency regime due to the fact of not having the same security controls of normal processes.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1483,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1483,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1484,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1484,255,'Incapacity to reach job facilities due to social disturbance.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1484,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1484,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1485,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1485,279,'Lack of knowledge about events and incidents involving information security.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1485,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1485,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1486,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1486,279,'Lack of knowledge about the impact of events and incidents of information security on critical business processes.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1486,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1486,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1487,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1487,279,'Lack of a comprehensive strategy to ensure the recovery of main processes.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1487,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1487,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1488,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1488,279,'Inability to recover main business processes in accordance with the continuity strategy due to lack of knowledge of people involved.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1488,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1488,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1489,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1489,279,'Inability to recover the main business processes in time due to lack of knowledge of people involved.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1489,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1489,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1490,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1490,279,'Errors in executing the continuity strategies due to lack of documentation about what actions should be done by people involved.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1490,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1490,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1491,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1491,279,'Inability to meet the continuity plans and strategies developed due to lack of adequate training.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1491,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1491,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1493,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1493,183,'Compromising of information in contingency environments due to failure in the implementation of security requirements in accordance with the original environment.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1493,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1493,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1494,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1494,279,'Lack of the necessary resources in remote sites for recovery of the main processes.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1494,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1494,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1495,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1495,279,'Unavailability of access to the continuity plans due to unavailable plans.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1495,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1495,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1496,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1496,279,'Difficulty to execute the business continuity plans due to lack of standardization.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1496,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1496,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1497,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1497,279,'Failure in putting into action the business continuity plans, due to lack of clear conditions in which the plan should be activated.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1497,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1497,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1498,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1498,279,'Failure in putting into action the business continuity plans, due to lack of emergency procedures that should be done just after the activation conditions had been confirmed.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1498,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1498,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1499,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1499,279,'Failure in putting into action the business continuity plans, due to lack of procedures that describe the actions to recovery of critical services and assets.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1499,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1499,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1500,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1500,279,'Failure in execution of the Business Continuity Plan due to lack of clearly defined responsibilities.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1500,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1500,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1501,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1501,279,'Failure in execution of the Business Continuity Plan due to not updating these plans.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1501,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1501,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1502,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1502,279,'Failure in execution of the Business Continuity Plan due to lack of tests that ensure the correct and proper function as it has been required.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1502,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1502,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1503,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1503,279,'Violation of laws, contracts, statutes, and regulations that involve the information security due to unknown legal requirements');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1503,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1503,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1504,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1504,279,'Violation of laws, contracts, statutes, and regulations that involve the information security due to failure in the communication of legal requirements to people responsible for information systems.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1504,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1504,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1506,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1506,100,'Violation of intellectual property rights in the use of information.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1506,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1506,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1509,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1509,100,'Loss of important records.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1509,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1509,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1510,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1510,100,'Counterfeit of important records.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1510,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1510,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1511,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1511,100,'Unauthorized access to important records due to the application of insufficient security controls.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1511,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1511,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1513,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1513,100,'Insufficient retention periods for important record.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1513,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1513,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1515,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1515,102,'Unauthorized disclosure of data and personal information.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1515,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1515,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1516,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1516,102,'Unauthorized access to data and personal information.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1516,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1516,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1517,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1517,102,'Capture of personal information through illegal mechanisms.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1517,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1517,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1518,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1518,102,'Undue use of personal information.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1518,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1518,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1521,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1521,229,'Security fault by inappropriate use of information asset.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1521,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1521,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1527,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1527,279,'Violation of legal aspects during the monitoring of  the use of information processing facilities.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1527,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1527,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1533,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1533,101,'Violation of legal aspects in the use of cryptographic controls.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1533,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1533,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1539,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1539,101,'Violation of legal aspects in the transmission of the cryptographic information to other countries.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1539,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1539,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1540,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1540,279,'Failure in execution of information security procedures.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1540,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1540,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1542,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1542,182,'Failure in implementation of technical aspects required by the Standards and security information procedures.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1542,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1542,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1543,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1543,228,'Failure in implementation of technical aspects required by the Standards and security information procedures.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1543,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1543,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (1544,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (1544,279,'Interruption of business processes due to the information systems audit.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1544,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1544,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5679,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5679,100,'Compromising of information stored on media due to inappropriate storage.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5679,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5679,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5680,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5680,100,'Loss of information stored for long periods of time due to media deterioration.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5680,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5680,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5681,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5681,100,'Unauthorized access to information stored in media disposal.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5681,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5681,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5682,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5682,100,'Unauthorized removal of information stored in medias to outside the organization.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5682,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5682,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5683,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5683,115,'Unauthorized access to sensitive information left on copiers.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5683,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5683,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5684,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5684,115,'Unauthorized access to sensitive information left on fax machines.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5684,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5684,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5685,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5685,115,'Unauthorized access to sensitive information left on printers.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5685,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5685,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5686,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5686,101,'Inability to recover important records due to storage in proprietary files format and unavailability of recovery software.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5686,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5686,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5687,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5687,101,'Inability to recover important record stored in media by long period of time.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5687,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5687,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5688,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5688,269,'Lack of commitment by third party to information security.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5688,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5688,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5690,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5690,100,'Inadequate information treatment which conflicts with the criticality of the information.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5690,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5690,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5691,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5691,100,'Loss of critical information.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5691,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5691,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5692,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5692,110,'Compromising of evidence integrity.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5692,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5692,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5693,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5693,124,'Compromising of evidence integrity.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5693,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5693,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5695,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5695,226,'Granting print out by unauthorized users.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5695,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5695,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5717,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5717,128,'Inappropriate use of information assets.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5717,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5717,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5718,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5718,128,'Misuse of information assets due to lack of clear rules about the appropriate and permitted use.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5718,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5718,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5719,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5719,128,'Execution of unauthorized changes.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5719,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5719,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5720,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5720,128,'Insertion of new vulnerabilities after changes.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5720,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5720,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5721,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5721,128,'Misuse of systems.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5721,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5721,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5722,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5722,128,'Accidental misuse of systems.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5722,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5722,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5723,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5723,128,'Computer virus contamination.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5723,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5723,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5724,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5724,128,'Loss of critical software.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5724,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5724,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5725,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5725,128,'Stop critical business processes due to unavailability of the asset.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5725,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5725,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5726,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5726,128,'Violation of intellectual property rights in the use of software.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5726,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5726,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5727,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5727,128,'Security fault by inappropriate use of information asset.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5727,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5727,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5728,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5728,128,'Misuse of information assets due to non-existence of clear rules about the adequate and permitted use.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5728,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5728,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5729,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5729,128,'Failure in implementation of technical aspects required by the Standards and security information procedures.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5729,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5729,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5731,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5731,128,'Inability to manage systems and services due to key person dependency.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5731,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5731,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5732,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5732,128,'Exceed of human faults due to installation of complex usability systems.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5732,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5732,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5733,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5733,129,'Unauthorized access to the operating system of operational facilities by developers.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5733,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5733,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5734,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5734,128,'Unauthorized access to systems or information by users whose contract with has been terminated.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5734,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5734,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5735,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5735,128,'Unauthorized access to systems in abnormal working hours.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5735,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5735,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5736,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5736,129,'Unauthorized access to operating systems due to failure in procedure to validate users log-on.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5736,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5736,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5737,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5737,128,'Undue or unauthorized access to systems and information caused by lack of access rights or privileges of employees who changed their position and / or function.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5737,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5737,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5738,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5738,129,'Violation of systems security controls through use of system utilities.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5738,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5738,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5739,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5739,128,'Inadequate allocation of access rights and privileges due to lack of an access control policy.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5739,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5739,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5740,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5740,129,'Use of system utilities by unauthorized users.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5740,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5740,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5741,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5741,128,'Access rights violation due to lack of clear rules that establish what is permitted and what is forbidden.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5741,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5741,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5742,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5742,129,'Corruption of operating systems caused by inadequate installation of softwares.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5742,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5742,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5743,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5743,128,'Incompatibility between access rights granted and the real need of access based in business requirements.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5743,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5743,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5744,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5744,128,'Users with unnecessary access rights regarding their job roles.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5744,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5744,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5745,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5745,128,'Incompatibility between access control rules and guidelines for information classification.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5745,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5745,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5746,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5746,128,'Inadequate grant of access rights.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5746,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5746,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5747,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5747,128,'Unable to make users responsible for their activities due to lack of unique user IDs.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5747,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5747,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5748,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5748,128,'Users with unnecessary access rights due to lack of a clear procedure to remove access rights in situations of inexistence or cancelation of contract.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5748,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5748,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5749,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5749,128,'Incompatibility of granted access level and the business purposes.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5749,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5749,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5750,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5750,128,'Unauthorized access caused by lack of formalization in users permissions.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5750,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5750,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5751,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5751,128,'Inappropriate use of privileged access.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5751,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5751,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5752,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5752,128,'Inadequate privilege assignment to users.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5752,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5752,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5753,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5753,128,'Redundant user IDs issued to other users.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5753,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5753,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5754,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5754,139,'Use of real information in test databases.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5754,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5754,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5755,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5755,128,'Assign privileges not related to user ID, making impossible to identify or monitor system activities.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5755,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5755,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5756,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5756,128,'Unrestricted use of privileged users in activities that it would not be necessary.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5756,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5756,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5757,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5757,128,'Compromising of password during the process of delivery to the users.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5757,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5757,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5758,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5758,128,'Access to systems through temporary passwords of general knowledge.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5758,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5758,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5759,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5759,128,'Inappropriate access rights due or privileges to a faulty update after changes of users'' roles or jobs.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5759,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5759,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5760,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5760,128,'Unauthorized access rights or privileges granted to system users.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5760,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5760,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5761,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5761,128,'Access rights that have not been removed after termination of employment.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5761,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5761,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5762,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5762,128,'Unauthorized external access to applications and services that are available only to internal network users.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5762,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5762,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5763,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5763,128,'Knowledge of users'' passwords by the personnel responsible for managing passwords.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5763,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5763,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5764,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5764,128,'Disclosure of passwords to other users by the owner of the password.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5764,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5764,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5765,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5765,128,'Disclosure of password by lack of care from users.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5765,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5765,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5766,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5766,177,'Cross Site Scripting (XSS)');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5766,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5766,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5767,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5767,128,'Provide information about the system during the log-in procedure, that could aid an unauthorized user to gain access.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5767,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5767,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5768,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5768,128,'Capture of passwords transmitted through network during the log-on session over a network.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5768,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5768,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5769,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5769,128,'Unauthorized access to systems through failure in log-on procedure.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5769,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5769,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5770,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5770,128,'Use of the same password for a long period of time.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5770,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5770,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5771,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5771,128,'Compromising of passwords included in an automated log-on process, such as stored in a macro or function key.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5771,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5771,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5772,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5772,128,'Do not enforce a choice of quality passwords by users.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5772,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5772,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5773,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5773,128,'Unauthorized disclosure of passwords to the administrators or support operators in case of a necessary change of passwords.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5773,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5773,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5774,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5774,128,'Acquisition of software with inadequate security controls.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5774,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5774,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5775,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5775,128,'Lack of formal change control procedures.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5775,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5775,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5776,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5776,128,'No formal testing and acquisition process is used when products are purchased.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5776,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5776,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5777,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5777,128,'Low systems performance due to lack of testing and evaluation.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5777,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5777,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5778,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5778,177,'Injection Flaws');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5778,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5778,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5779,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5779,128,'Failure in the continuity plans, due to lack of updates in the new systems.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5779,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5779,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5780,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5780,128,'Using of systems that have not been tested and homologated.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5780,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5780,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5781,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5781,177,'Insecure Remote File Included');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5781,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5781,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5782,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5782,128,'Systems installation without proper evaluation.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5782,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5782,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5783,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5783,177,'Insecure Direct Object Reference');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5783,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5783,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5784,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5784,128,'Systems installation that do not meet the business requirements.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5784,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5784,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5785,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5785,128,'Compromising of security configurations due to performed changes.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5785,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5785,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5786,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5786,177,'Cross Site Request Forgery (CSRF)');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5786,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5786,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5788,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5788,128,'Inability to identify information leakage due to lack of regular monitoring of system activities.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5788,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5788,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5790,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5790,128,'Inability to indentify actions made by users in the system.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5790,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5790,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5791,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5791,128,'Inability to indentify exceptions in the system normal activities.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5791,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5791,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5792,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5792,177,'Failure to Restrict URL Access');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5792,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5792,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5793,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5793,128,'Inability to identify security events in systems.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5793,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5793,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5794,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5794,128,'Inability to identify unauthorized system access attempts.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5794,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5794,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5795,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5795,177,'Unauthorized access to information of e-commerce payment that are traded in public networks.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5795,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5795,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5796,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5796,128,'Unauthorized activities in the system.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5796,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5796,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5797,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5797,177,'Unauthorized change of information of e-commerce payment that are read in public networks.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5797,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5797,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5798,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5798,128,'Unauthorized access to the system.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5798,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5798,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5799,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5799,128,'Unauthorized change of system logs by external users (e.g. Hackers, intruders).');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5799,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5799,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5800,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5800,177,'Unauthorized change of prices and information in the e-commerce website.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5800,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5800,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5801,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5801,128,'Unauthorized change of system logs by administrators or privileged users.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5801,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5801,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5802,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5802,128,'Unauthorized reading of system logs.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5802,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5802,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5803,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5803,177,'Violation of consumer privacy laws during the e-commerce payment.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5803,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5803,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5804,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5804,128,'Inability to identify unauthorized activities executed by privileged users such as system administrator and system operator.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5804,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5804,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5805,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5805,177,'Change in e-commerce website trough undue access by hackers and/or unauthorized users.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5805,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5805,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5806,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5806,128,'Inability to detect errors and failures in working systems by lack of failure records.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5806,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5806,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5807,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5807,177,'Provision of incorrect information for the payment of purchases made in e-commerce website.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5807,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5807,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5808,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5808,128,'Delay in restoring controls after failure in a system due to lack of fault logging.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5808,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5808,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5809,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5809,177,'Unavailability of the e-commerce website.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5809,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5809,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5810,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5810,128,'Differences between the time shown in systems and the time that a real action occurred.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5810,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5810,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5811,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5811,128,'Inability to identify the precise time that an action has been made due to lack of clock synchronization.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5811,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5811,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5812,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5812,128,'Lack of reliability in evidences used in the forensic investigation due to lack of clock synchronization.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5812,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5812,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5813,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5813,128,'Undue use of cryptographic controls.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5813,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5813,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5814,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5814,128,'Lack of strength or quality of the encryption algorithm applied.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5814,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5814,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5815,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5815,138,'Injection of unauthorized code into operational facilities.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5815,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5815,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5816,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5816,128,'Use of proprietary encryption techniques or lack of documentation that allows the recovery of encrypted information in the case of lost, compromised or damaged keys.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5816,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5816,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5817,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5817,138,'Insertion of untested code into operational facilities.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5817,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5817,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5818,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5818,128,'Unauthorized access to cryptographic keys.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5818,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5818,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5819,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5819,138,'Failure in working systems due to differences between operational facilities and development facilities.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5819,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5819,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5820,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5820,128,'Distribution of cryptographic keys to unauthorized users.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5820,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5820,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5821,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5821,138,'Use of development tools that facilitate the unauthorized access to the information or system.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5821,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5821,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5822,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5822,128,'Loss of cryptographic keys.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5822,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5822,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5823,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5823,128,'Inability to recover encrypted information due to lack of key management.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5823,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5823,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5824,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5824,138,'Intentional input of incorrect data into the system.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5824,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5824,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5825,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5825,128,'Incorrect storage of the equipment used to generate, store and archive cryptographic keys.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5825,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5825,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5826,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5826,138,'Input of incorrect data due to typing errors.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5826,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5826,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5827,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5827,128,'Falsification of cryptographic keys.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5827,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5827,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5828,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5828,138,'Incorrect transmission of on-line transactions');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5828,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5828,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5829,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5829,128,'Violation of legal aspects in the use of cryptographic controls.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5829,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5829,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5830,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5830,138,'Routing errors when sending or receiving on-line transactions.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5830,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5830,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5831,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5831,128,'Violation of legal aspects in the transmission of the cryptographic information to other countries.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5831,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5831,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5832,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5832,138,'Unauthorized changes in on-line transactions messages.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5832,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5832,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5833,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5833,128,'Falsification of messages.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5833,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5833,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5834,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5834,138,'Unauthorized reading and disclosure of information inside on-line transactions messages.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5834,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5834,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5835,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5835,128,'Repudiation  of messages transmitted among systems.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5835,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5835,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5836,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5836,138,'Unauthorized replay of messages on on-line transactions.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5836,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5836,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5837,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5837,128,'Unauthorized transmission of information to application outputs due to inability to filter information.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5837,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5837,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5838,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5838,128,'Presenting incorrect or out of context results.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5838,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5838,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5839,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5839,138,'Incorrect or unauthorized information in systems directly accessible from the Internet.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5839,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5839,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5840,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5840,128,'Compromising of the system through the use of buffer overflow technique.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5840,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5840,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5841,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5841,138,'Unauthorized publication of data by using information unapproved and inserted in systems directly accessible from the Internet.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5841,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5841,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5843,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5843,128,'Compromising of information and system functions caused by failure in another interconnected system.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5843,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5843,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5844,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5844,138,'Violations of intellectual property laws through undue providing of information systems directly accessible from the Internet.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5844,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5844,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5845,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5845,138,'Lack or failure in security requirements analysis and specification when developing a system.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5845,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5845,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5846,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5846,128,'Insertions of new hardware and/or software not compatible with the rest of system devices.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5846,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5846,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5848,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5848,128,'Unauthorized access to systems through users that chosen a low quality password.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5848,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5848,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5849,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5849,138,'Inability to integrate security controls due to the fact that statements of business requirements has been made after the development of the system.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5849,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5849,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5850,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5850,128,'Unauthorized access to business process description in system documentation.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5850,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5850,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5851,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5851,128,'Unauthorized disclosure of information due to systems interconnection.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5851,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5851,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5852,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5852,138,'Input of incomplete data into the system.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5852,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5852,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5853,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5853,128,'Violation of legal requirements by clients while using internal systems.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5853,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5853,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5854,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5854,138,'Undue alteration of data caused by processing errors.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5854,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5854,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5855,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5855,128,'Unauthorized access to information through sharing data between systems.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5855,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5855,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5856,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5856,138,'Failure in data corruption detection due to lack of testing the plausibility of the input data.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5856,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5856,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5857,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5857,128,'Compromising of systems by failure in other systems that share the same computing environment.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5857,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5857,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5858,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5858,138,'Unauthorized access to data due to lack of a verification list (e.g.: hash, controls run-to-run and program-to-program...).');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5858,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5858,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5859,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5859,128,'Compromising of several critical systems due to failure in the computing environment where those systems are installed.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5859,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5859,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5860,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5860,138,'Unauthorized reading of system source code.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5860,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5860,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5861,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5861,138,'Unauthorized change of the system source code.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5861,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5861,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5862,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5862,128,'Insertion of malicious code during emergency procedures in which security controls are not considered.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5862,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5862,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5863,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5863,128,'Contamination of resources caused by insertion of infected files on computers.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5863,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5863,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5864,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5864,138,'Unauthorized access to the development documentation of the system.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5864,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5864,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5865,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5865,128,'Contamination of information assets due to lack updates of malicious code detection and repair software.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5865,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5865,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5866,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5866,138,'Compromising of applications after changes made in the operating system in which the application is executed.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5866,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5866,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5867,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5867,128,'Lack of updates caused by absence of contract support with suppliers.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5867,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5867,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5868,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5868,128,'System unavailability or malfunction due to installation of software updates  without extensive tests.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5868,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5868,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5869,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5869,138,'Compromising of security controls and integrity mechanisms of software packages after changes or customizations.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5869,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5869,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5870,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5870,138,'Unauthorized access to information or environment by outsourced developers.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5870,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5870,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5871,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5871,128,'Lack of critical analysis on changes made in the operational environment.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5871,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5871,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5872,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5872,138,'Inability to ensure the system maintenance after failure of attendance by outsourced developers.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5872,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5872,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5873,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5873,128,'Leak of information through the use and exploration of covert channels.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5873,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5873,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5874,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5874,138,'Indiscriminate use of the software developed by lack of licensing agreements, code ownership and intellectual property rights.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5874,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5874,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5875,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5875,128,'Leak of information through the use of systems with dubious integrity.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5875,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5875,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5876,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5876,128,'Compromise the system due to the presence of well known vulnerabilities');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5876,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5876,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5877,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5877,138,'Lack of periodic contents verification of key fields or data files.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5877,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5877,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5878,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5878,128,'Technical vulnerabilities not identified due to lack of assets inventory and of a management process on discovered vulnerabilities.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5878,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5878,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5879,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5879,138,'Information processing in environments  not properly controlled.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5879,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5879,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5880,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5880,128,'Installation of a fix with security problems.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5880,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5880,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5882,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5882,128,'Installation of Trojan horses"."');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5882,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5882,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5883,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5883,101,'Unauthorized access by external parties due to interconnecting to the company information, compromising the information security.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5883,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5883,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5884,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5884,116,'Loss of client''s information.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5884,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5884,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5885,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5885,102,'Loss of client''s information.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5885,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5885,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5886,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5886,296,'Use of development tools that facilitate the unauthorized access to the information or system.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5886,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5886,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5887,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5887,114,'Inability to restore information due to execution of back-up copies of information only, not of the software necessary to data interpretation.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5887,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5887,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5888,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5888,116,'Capture of personal information through illegal mechanisms.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5888,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5888,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5889,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5889,114,'Compromising of original information and security copies during disasters due to proximity of storage of such copies.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5889,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5889,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5890,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5890,114,'Inability to restore back-up copies due to deteriorating in the storage media.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5890,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5890,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5891,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5891,116,'Unauthorized access to data and personal information.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5891,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5891,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5892,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5892,116,'Unauthorized disclosure of data and personal information.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5892,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5892,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5893,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5893,116,'Undue use of personal information.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5893,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5893,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5894,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5894,208,'Stop critical business processes due to failure in equipment.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5894,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5894,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5895,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5895,128,'Unauthorized use of specific tools for systems audits.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5895,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5895,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (551,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (551,207,'Compromising of cabling by installing it in industrial environment.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,551,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,551,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (569,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (569,207,'Compromising of cabling caused by instability in the electric power supply.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,569,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,569,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (563,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (563,207,'Compromising of cabling due to chemical effects.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,563,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,563,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (554,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (554,207,'Compromising of cabling due to electric discharge provoked by lightning.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,554,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,554,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (557,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (557,207,'Compromising of cabling due to environment conditions such as humidity and temperature.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,557,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,557,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (572,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (572,207,'Compromising of cabling through electromagnetic interference.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,572,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,572,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (543,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (543,207,'Unauthorized access to critical cabling caused by installation in a high risk unauthorized location.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,543,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,543,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5896,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5896,208,'Unauthorized access to critical equipment caused by installation in a high risk unauthorized location.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5896,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5896,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5897,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5897,208,'Security fault by inappropriate use of information asset.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5897,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5897,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5898,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5898,208,'Misuse of information assets due to non-existence of clear rules about the adequate and permitted use.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5898,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5898,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5900,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5900,128,'Insufficient system performance.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5900,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5900,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5901,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5901,228,'Stop critical business processes due to unavailability of the asset.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5901,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5901,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5903,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5903,235,'Unauthorized access to information through communications interception.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5903,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5903,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5904,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5904,235,'Unauthorized changes of data received or sent.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5904,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5904,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5905,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5905,235,'Receiving malicious code through communication facilities.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5905,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5905,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5907,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5907,235,'Receiving unauthorized information through files attached.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5907,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5907,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5908,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5908,235,'Receiving of unauthorized information through download of data.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5908,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5908,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5909,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5909,235,'Sending unauthorized information through upload of data.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5909,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5909,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5911,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5911,235,'Defamation through communication facilities.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5911,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5911,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5912,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5912,228,'Inappropriate use of information assets.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5912,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5912,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5913,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5913,128,'Unauthorized access to systems and information by users whose contract with the organization has been terminated.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5913,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5913,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5915,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5915,213,'Unauthorized external using of information processing equipment.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5915,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5915,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5916,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5916,240,'Accidental reveal of sensitive information to immediate vicinity when making a phone call.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5916,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5916,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5917,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5917,231,'Lack of storage space.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5917,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5917,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5918,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5918,240,'Unauthorized access to information left on answering machines.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5918,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5918,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5919,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5919,240,'Unauthorized receiving of sensitive information through fax due to inability to grant the identity of the receptor.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5919,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5919,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5920,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5920,240,'Accidental sending of sensitive information through fax due to unauthorized changes in facsimile machines.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5920,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5920,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5922,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5922,208,'Misuse of information assets due to lack of clear rules about the appropriate and permitted use.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5922,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5922,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5925,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5925,208,'Unauthorized access to information by storing it in an inappropriate place.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5925,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5925,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5926,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5926,208,'Compromising of equipment by installing it in industrial environment.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5926,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5926,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5927,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5927,208,'Compromising of equipment due to electric discharge provoked by lightning.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5927,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5927,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5928,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5928,208,'Compromising of equipment due to environment conditions such as humidity and temperature.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5928,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5928,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5929,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5929,208,'Compromising of equipment due to accidents with food or drink kept near the equipment.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5929,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5929,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5930,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5930,208,'Compromising of equipment due to chemical effects.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5930,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5930,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5931,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5931,208,'Theft of equipments caused by installation in inadequate places.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5931,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5931,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5934,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5934,244,'Delivery of information to unauthorized couriers during messages transport.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5934,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5934,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5935,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5935,208,'Failure in equipment caused by failure in electric energy supply.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5935,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5935,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5937,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5937,208,'Compromising of equipment caused by maintenance made by unqualified people.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5937,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5937,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5941,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5941,236,'Compromising of internal network by the use of services such as instant messaging or file sharing.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5941,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5941,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5944,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5944,208,'Unauthorized access to information trough re-use of equipment.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5944,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5944,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5946,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5946,208,'Compromising of equipment caused by instability in the electric power supply.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5946,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5946,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5947,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5947,208,'Unauthorized moving of assets inside the organization.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5947,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5947,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5948,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5948,208,'Compromising of equipment through electromagnetic interference.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5948,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5948,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5949,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5949,208,'Insufficient system performance.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5949,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5949,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5952,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5952,208,'Lack of periodic maintenance.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5952,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5952,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5954,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5954,208,'Unauthorized access to information during equipment maintenance.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5954,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5954,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5955,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5955,208,'Unauthorized external use of information processing equipment.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5955,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5955,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5956,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5956,208,'Unauthorized access to information in equipment used outside the organization''s premises.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5956,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5956,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5957,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5957,182,'Insurance loss due to not meeting established requirements about maintenance of equipment and/or structure.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5957,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5957,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5958,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5958,208,'Mislay off-site equipment working outside the organization''s premises.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5958,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5958,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5959,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5959,208,'Exposure of off-site equipment to strong electromagnetic fields.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5959,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5959,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5960,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5960,208,'Unauthorized access to information through disposed equipment.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5960,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5960,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5962,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5962,208,'Take assets to off-site without prior authorization.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5962,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5962,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5966,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5966,235,'Unavailability of access due to failure at configuration.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5966,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5966,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5968,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5968,229,'Unavailability of access due to failure at configuration.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5968,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5968,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5969,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5969,208,'Computer virus contamination.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5969,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5969,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5971,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5971,235,'Unauthorized access due to interception of transmitting data.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5971,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5971,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5972,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5972,236,'Unauthorized remote access to internal networks.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5972,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5972,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5973,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5973,239,'Insufficient system capacity.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5973,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5973,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5975,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5975,228,'Inability to provide appropriate service level.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5975,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5975,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5976,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5976,228,'Inability to apply the appropriate security requirements for each service.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5976,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5976,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5978,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5978,103,'Access to personal information of the candidates to a job by unauthorized people.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5978,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5978,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5979,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5979,117,'Access to personal information of the candidates to a job by unauthorized people.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5979,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5979,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5980,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5980,235,'Failure in communication and security incidents treatment.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5980,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5980,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5981,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5981,237,'Undue remote access to internal network by employees and/or third parties.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5981,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5981,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5983,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5983,128,'Unauthorized access to systems and information by users or third parties whose contract has expired, or by lack of knowledge of the employees about the contract termination.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5983,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5983,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5984,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5984,100,'Unauthorized access to systems and information by users or third parties whose contract has expired, or by lack of knowledge of the employees about the contract termination.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5984,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5984,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5985,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5985,128,'Lack of knowledge of new vulnerabilities due to lack of contact with research groups and services of vulnerabilities notification.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5985,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5985,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5986,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5986,279,'Inability to recover critical business processes due to difficulty in crisis management.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5986,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5986,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5987,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5987,128,'Lack of knowledge about new techniques of attack.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5987,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5987,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5988,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5988,128,'Lack of knowledge about security weaknesses in the software.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5988,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5988,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5989,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5989,100,'Loss of intellectual property rights or information developed in the organization due to lack of clear specification of such rights in contracts.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5989,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5989,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5990,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5990,128,'Security weaknesses not previously detected.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5990,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5990,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5991,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5991,279,'Violation of intellectual property rights by users when copying softwares, images, music or any other information to the company computers.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5991,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5991,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5995,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5995,228,'Not meeting the requirements for providing services in a secure way.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5995,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5995,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5997,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5997,228,'Lack of service delivery stipulated by contract.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5997,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5997,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5998,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5998,228,'Not attend the necessary security requirements to provide the service.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5998,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5998,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (5999,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (5999,228,'Unauthorized access to the information and systems by service providers.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5999,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5999,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6000,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription) VALUES (6000,228,'Nonconformity between the provided services and the agreements/contracts established.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6000,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6000,2902,NOW());

