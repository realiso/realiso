--
-- PostgreSQL database dump
--

-- Started on 2008-12-09 11:54:28

SET client_encoding = 'UTF8';

SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

--
-- TOC entry 2780 (class 0 OID 0)
-- Dependencies: 1509
-- Name: ci_nc_nseqnumber_seq; Type: SEQUENCE SET; Schema: public; Owner: isms
--

SELECT pg_catalog.setval('ci_nc_nseqnumber_seq', 1, true);


--
-- TOC entry 2781 (class 0 OID 0)
-- Dependencies: 1544
-- Name: isms_context_pkcontext_seq; Type: SEQUENCE SET; Schema: public; Owner: isms
--

SELECT pg_catalog.setval('isms_context_pkcontext_seq', 11980, true);


--
-- TOC entry 2782 (class 0 OID 0)
-- Dependencies: 1541
-- Name: isms_context_classification_pkclassification_seq; Type: SEQUENCE SET; Schema: public; Owner: isms
--

SELECT pg_catalog.setval('isms_context_classification_pkclassification_seq', 105, true);


--
-- TOC entry 2783 (class 0 OID 0)
-- Dependencies: 1543
-- Name: isms_context_history_pkid_seq; Type: SEQUENCE SET; Schema: public; Owner: isms
--

SELECT pg_catalog.setval('isms_context_history_pkid_seq', 4172, true);


--
-- TOC entry 2784 (class 0 OID 0)
-- Dependencies: 1548
-- Name: isms_saas_pkconfig_seq; Type: SEQUENCE SET; Schema: public; Owner: isms
--

SELECT pg_catalog.setval('isms_saas_pkconfig_seq', 1, false);


--
-- TOC entry 2785 (class 0 OID 0)
-- Dependencies: 1558
-- Name: pm_doc_reference_pkreference_seq; Type: SEQUENCE SET; Schema: public; Owner: isms
--

SELECT pg_catalog.setval('pm_doc_reference_pkreference_seq', 1, false);


--
-- TOC entry 2786 (class 0 OID 0)
-- Dependencies: 1562
-- Name: pm_instance_comment_pkcomment_seq; Type: SEQUENCE SET; Schema: public; Owner: isms
--

SELECT pg_catalog.setval('pm_instance_comment_pkcomment_seq', 1, false);


--
-- TOC entry 2787 (class 0 OID 0)
-- Dependencies: 1578
-- Name: rm_parameter_name_pkparametername_seq; Type: SEQUENCE SET; Schema: public; Owner: isms
--

SELECT pg_catalog.setval('rm_parameter_name_pkparametername_seq', 10, false);


--
-- TOC entry 2788 (class 0 OID 0)
-- Dependencies: 1580
-- Name: rm_parameter_value_name_pkvaluename_seq; Type: SEQUENCE SET; Schema: public; Owner: isms
--

SELECT pg_catalog.setval('rm_parameter_value_name_pkvaluename_seq', 10, false);


--
-- TOC entry 2789 (class 0 OID 0)
-- Dependencies: 1582
-- Name: rm_rc_parameter_value_name_pkrcvaluename_seq; Type: SEQUENCE SET; Schema: public; Owner: isms
--

SELECT pg_catalog.setval('rm_rc_parameter_value_name_pkrcvaluename_seq', 10, false);


--
-- TOC entry 2790 (class 0 OID 0)
-- Dependencies: 1636
-- Name: wkf_alert_pkalert_seq; Type: SEQUENCE SET; Schema: public; Owner: isms
--

SELECT pg_catalog.setval('wkf_alert_pkalert_seq', 2, true);


--
-- TOC entry 2791 (class 0 OID 0)
-- Dependencies: 1640
-- Name: wkf_schedule_pkschedule_seq; Type: SEQUENCE SET; Schema: public; Owner: isms
--

SELECT pg_catalog.setval('wkf_schedule_pkschedule_seq', 1, true);


--
-- TOC entry 2792 (class 0 OID 0)
-- Dependencies: 1642
-- Name: wkf_task_pktask_seq; Type: SEQUENCE SET; Schema: public; Owner: isms
--

SELECT pg_catalog.setval('wkf_task_pktask_seq', 1, false);


--
-- TOC entry 2697 (class 0 OID 30366630)
-- Dependencies: 1498
-- Data for Name: ci_action_plan; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO ci_action_plan (fkcontext, fkresponsible, sname, tactionplan, nactiontype, ddatedeadline, ddateconclusion, bisefficient, ddateefficiencyrevision, ddateefficiencymeasured, ndaysbefore, bflagrevisionalert, bflagrevisionalertlate, sdocument) VALUES (11980, 13, 'teste1', 'ytrytrytr', 4780, '2008-12-05 00:00:00', NULL, 0, NULL, NULL, 0, 0, 0, '');


--
-- TOC entry 2698 (class 0 OID 30366643)
-- Dependencies: 1499
-- Data for Name: ci_category; Type: TABLE DATA; Schema: public; Owner: isms
--



--
-- TOC entry 2699 (class 0 OID 30366645)
-- Dependencies: 1500
-- Data for Name: ci_incident; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO ci_incident (fkcontext, fkcategory, fkresponsible, sname, taccountsplan, tevidences, nlosstype, tevidencerequirementcomment, ddatelimit, ddatefinish, tdisposaldescription, tsolutiondescription, tproductservice, bnotemaildp, bemaildpsent, ddate) VALUES (11975, NULL, 13, 'Commercial CRM - Data loss', '', '', 4580, NULL, '2008-12-05 00:00:00', NULL, '', '', '', 0, 0, '2008-12-05 08:02:00');


--
-- TOC entry 2700 (class 0 OID 30366655)
-- Dependencies: 1501
-- Data for Name: ci_incident_control; Type: TABLE DATA; Schema: public; Owner: isms
--



--
-- TOC entry 2701 (class 0 OID 30366657)
-- Dependencies: 1502
-- Data for Name: ci_incident_financial_impact; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO ci_incident_financial_impact (fkincident, fkclassification, nvalue) VALUES (11975, 36, 12312);
INSERT INTO ci_incident_financial_impact (fkincident, fkclassification, nvalue) VALUES (11975, 37, 0);
INSERT INTO ci_incident_financial_impact (fkincident, fkclassification, nvalue) VALUES (11975, 38, 5000);
INSERT INTO ci_incident_financial_impact (fkincident, fkclassification, nvalue) VALUES (11975, 39, 0);


--
-- TOC entry 2702 (class 0 OID 30366660)
-- Dependencies: 1503
-- Data for Name: ci_incident_process; Type: TABLE DATA; Schema: public; Owner: isms
--



--
-- TOC entry 2703 (class 0 OID 30366662)
-- Dependencies: 1504
-- Data for Name: ci_incident_risk; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO ci_incident_risk (fkcontext, fkrisk, fkincident) VALUES (11976, 11798, 11975);
INSERT INTO ci_incident_risk (fkcontext, fkrisk, fkincident) VALUES (11977, 11794, 11975);
INSERT INTO ci_incident_risk (fkcontext, fkrisk, fkincident) VALUES (11978, 11775, 11975);


--
-- TOC entry 2704 (class 0 OID 30366664)
-- Dependencies: 1505
-- Data for Name: ci_incident_risk_value; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO ci_incident_risk_value (fkparametername, fkvaluename, fkincrisk) VALUES (1, 3, 11976);
INSERT INTO ci_incident_risk_value (fkparametername, fkvaluename, fkincrisk) VALUES (1, 5, 11977);
INSERT INTO ci_incident_risk_value (fkparametername, fkvaluename, fkincrisk) VALUES (1, 1, 11978);


--
-- TOC entry 2705 (class 0 OID 30366666)
-- Dependencies: 1506
-- Data for Name: ci_incident_user; Type: TABLE DATA; Schema: public; Owner: isms
--



--
-- TOC entry 2706 (class 0 OID 30366671)
-- Dependencies: 1507
-- Data for Name: ci_nc; Type: TABLE DATA; Schema: public; Owner: isms
--



--
-- TOC entry 2707 (class 0 OID 30366678)
-- Dependencies: 1508
-- Data for Name: ci_nc_action_plan; Type: TABLE DATA; Schema: public; Owner: isms
--



--
-- TOC entry 2708 (class 0 OID 30366682)
-- Dependencies: 1510
-- Data for Name: ci_nc_process; Type: TABLE DATA; Schema: public; Owner: isms
--



--
-- TOC entry 2709 (class 0 OID 30366684)
-- Dependencies: 1511
-- Data for Name: ci_nc_seed; Type: TABLE DATA; Schema: public; Owner: isms
--



--
-- TOC entry 2710 (class 0 OID 30366687)
-- Dependencies: 1512
-- Data for Name: ci_occurrence; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO ci_occurrence (fkcontext, fkincident, tdescription, tdenialjustification, ddate) VALUES (11974, 11975, 'Information corrupted on main SQL instance.', NULL, '2008-12-05 08:00:00');


--
-- TOC entry 2711 (class 0 OID 30366692)
-- Dependencies: 1513
-- Data for Name: ci_risk_probability; Type: TABLE DATA; Schema: public; Owner: isms
--



--
-- TOC entry 2712 (class 0 OID 30366695)
-- Dependencies: 1514
-- Data for Name: ci_risk_schedule; Type: TABLE DATA; Schema: public; Owner: isms
--



--
-- TOC entry 2713 (class 0 OID 30366699)
-- Dependencies: 1515
-- Data for Name: ci_solution; Type: TABLE DATA; Schema: public; Owner: isms
--


--
-- TOC entry 2735 (class 0 OID 30366859)
-- Dependencies: 1539
-- Data for Name: isms_config; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO isms_config (pkconfig, svalue) VALUES (401, '0');
INSERT INTO isms_config (pkconfig, svalue) VALUES (402, '1');
INSERT INTO isms_config (pkconfig, svalue) VALUES (403, 'localhost');
INSERT INTO isms_config (pkconfig, svalue) VALUES (404, '1');
INSERT INTO isms_config (pkconfig, svalue) VALUES (405, '1');
INSERT INTO isms_config (pkconfig, svalue) VALUES (406, '1');
INSERT INTO isms_config (pkconfig, svalue) VALUES (407, '0');
INSERT INTO isms_config (pkconfig, svalue) VALUES (408, '0');
INSERT INTO isms_config (pkconfig, svalue) VALUES (409, '0');
INSERT INTO isms_config (pkconfig, svalue) VALUES (410, '00:00');
INSERT INTO isms_config (pkconfig, svalue) VALUES (411, '1970-01-01 00:00:00');
INSERT INTO isms_config (pkconfig, svalue) VALUES (415, '%default_sender%');
INSERT INTO isms_config (pkconfig, svalue) VALUES (416, '1');
INSERT INTO isms_config (pkconfig, svalue) VALUES (417, '1');
INSERT INTO isms_config (pkconfig, svalue) VALUES (418, '7801');
INSERT INTO isms_config (pkconfig, svalue) VALUES (419, '00:00');
INSERT INTO isms_config (pkconfig, svalue) VALUES (421, '1');
INSERT INTO isms_config (pkconfig, svalue) VALUES (426, '1');
INSERT INTO isms_config (pkconfig, svalue) VALUES (427, 'Confidential');
INSERT INTO isms_config (pkconfig, svalue) VALUES (428, '');
INSERT INTO isms_config (pkconfig, svalue) VALUES (429, '1');
INSERT INTO isms_config (pkconfig, svalue) VALUES (430, '%default_sender_name%');
INSERT INTO isms_config (pkconfig, svalue) VALUES (431, 'CURR_DOLLAR');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7201, '5');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7202, '0');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7203, '0');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7204, '0');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7205, '3');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7206, '365');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7207, '5');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7208, '7');
INSERT INTO isms_config (pkconfig, svalue) VALUES (801, '13');
INSERT INTO isms_config (pkconfig, svalue) VALUES (803, '13');
INSERT INTO isms_config (pkconfig, svalue) VALUES (804, '13');
INSERT INTO isms_config (pkconfig, svalue) VALUES (805, '13');
INSERT INTO isms_config (pkconfig, svalue) VALUES (806, '13');
INSERT INTO isms_config (pkconfig, svalue) VALUES (807, '13');
INSERT INTO isms_config (pkconfig, svalue) VALUES (808, '13');
INSERT INTO isms_config (pkconfig, svalue) VALUES (809, '13');
INSERT INTO isms_config (pkconfig, svalue) VALUES (810, '13');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7403, '5');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7209, '%error_report_email%');
INSERT INTO isms_config (pkconfig, svalue) VALUES (5701, '8301');
INSERT INTO isms_config (pkconfig, svalue) VALUES (201, 'Hardware');
INSERT INTO isms_config (pkconfig, svalue) VALUES (202, 'Software');
INSERT INTO isms_config (pkconfig, svalue) VALUES (203, 'Service / Consulting');
INSERT INTO isms_config (pkconfig, svalue) VALUES (204, 'People');
INSERT INTO isms_config (pkconfig, svalue) VALUES (205, 'Education / Training');
INSERT INTO isms_config (pkconfig, svalue) VALUES (420, '2008-12-9');
--INSERT INTO isms_config (pkconfig, svalue) VALUES (7404, '0');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7500, '0');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7501, '');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7502, '');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7503, '');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7504, '');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7510, '');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7511, '');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7512, '');

/***** AD *****/
INSERT INTO isms_config (pkconfig, svalue) VALUES (7600, '0');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7601, 'ad-server.local');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7602, '389');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7603, '0');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7604, 'DC=ad-server, DC=local');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7605, 'ad-server\%user%');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7606, 'admin');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7607, 'adminpass');

INSERT INTO isms_config (pkconfig, svalue) VALUES (7406, '');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7407, '');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7405, 3);


/***** Configurações para o reset da Base de Dados *****/
INSERT INTO isms_config (pkconfig, svalue) VALUES (7700, '0');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7701, '');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7702, '1');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7703, '');

/* FUSO HORARIO */
INSERT INTO isms_config (pkconfig, svalue) VALUES (7404, '0');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7514, '1');

/* CORES CUSTOMIZADAS */
INSERT INTO isms_config (pkconfig, svalue) VALUES (7515, '');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7516, '');

--
-- TOC entry 2715 (class 0 OID 30366710)
-- Dependencies: 1518
-- Data for Name: isms_context; Type: TABLE DATA; Schema: public; Owner: isms
--

COPY isms_context (pkcontext, ntype, nstate) FROM '/tmp/trialdata/sox_isms_context.sql' DELIMITERS '#' NULL 'NULL';

--
-- TOC entry 2736 (class 0 OID 30366861)
-- Dependencies: 1540
-- Data for Name: isms_context_classification; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (36, 2831, 'Fine', 5503);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (37, 2831, 'Environment Recovery', 5503);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (38, 2831, 'Direct Loss', 5503);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (39, 2831, 'Indirect Loss', 5503);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (1, 2801, 'Headquarter', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (2, 2801, 'Subsidiary', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (3, 2801, 'Regional Office', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (4, 2801, 'Factory', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype, nweight) VALUES (5, 2801, '3 - High', 5502, 1);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype, nweight) VALUES (6, 2801, '2 - Medium', 5502, 2);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype, nweight) VALUES (7, 2801, '1 - Low', 5502, 3);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (8, 2802, 'Market & customers', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (9, 2802, 'New product development', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (10, 2802, 'Production & delivery', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (11, 2802, 'Human resource', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (12, 2802, 'Information technology', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (13, 2802, 'Finance & accounting', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (14, 2802, 'Environment, health & safety', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (15, 2802, 'Performance improvement', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype, nweight) VALUES (16, 2802, '3 - High', 5502, 1);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype, nweight) VALUES (17, 2802, '2 - Medium', 5502, 2);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype, nweight) VALUES (18, 2802, '1 - Low', 5502, 3);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (19, 2804, 'People event', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (20, 2804, 'Internal process-related event', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (21, 2804, 'Internal information system event', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (100, 2804, 'External economic event', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (101, 2804, 'Natural environment event', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (102, 2804, 'Political event', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (103, 2804, 'Social factor', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (104, 2804, 'External & internal technological event', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (22, 2807, 'Internal', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (23, 2807, 'External', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (32, 2826, 'Confidential', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (33, 2826, 'Restrict', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (34, 2826, 'Institutional', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (35, 2826, 'Public', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (25, 2805, 'Preventive (manual)', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (26, 2805, 'Preventive (automated)', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (27, 2805, 'Detective (manual)', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (105, 2805, 'Detective (automated)', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (28, 2823, 'Confidential', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (29, 2823, 'Restrict', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (30, 2823, 'Institutional', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (31, 2823, 'Public', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (24, 2807, 'Internal & External', 5501);


--
-- TOC entry 2714 (class 0 OID 30366704)
-- Dependencies: 1516
-- Data for Name: isms_context_date; Type: TABLE DATA; Schema: public; Owner: isms
--


COPY isms_context_date (fkcontext, naction, ddate, nuserid, susername) FROM '/tmp/trialdata/sox_isms_context_date.sql' DELIMITERS '#' NULL 'NULL';


--
-- TOC entry 2737 (class 0 OID 30366867)
-- Dependencies: 1542
-- Data for Name: isms_context_history; Type: TABLE DATA; Schema: public; Owner: isms
--


COPY isms_context_history (pkid, fkcontext, stype, ddate, nvalue) FROM '/tmp/trialdata/sox_isms_context_history.sql' DELIMITERS '#' NULL 'NULL';

--
-- TOC entry 2738 (class 0 OID 30366874)
-- Dependencies: 1545
-- Data for Name: isms_policy; Type: TABLE DATA; Schema: public; Owner: isms
--



--
-- TOC entry 2716 (class 0 OID 30366714)
-- Dependencies: 1519
-- Data for Name: isms_profile; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO isms_profile (fkcontext, sname, nid) VALUES (1, 'Security Officer', 8801);
INSERT INTO isms_profile (fkcontext, sname, nid) VALUES (2, 'Manager', 8802);
INSERT INTO isms_profile (fkcontext, sname, nid) VALUES (3, 'Administrator', 8803);
INSERT INTO isms_profile (fkcontext, sname, nid) VALUES (4, 'Without Permissions', 8804);
INSERT INTO isms_profile (fkcontext, sname, nid) VALUES (5, 'Document Reader', 8805);


--
-- TOC entry 2739 (class 0 OID 30366879)
-- Dependencies: 1546
-- Data for Name: isms_profile_acl; Type: TABLE DATA; Schema: public; Owner: isms
--

COPY isms_profile_acl (fkprofile,stag) FROM '/tmp/trialdata/sox_isms_profile_acl.sql' DELIMITERS '#' NULL 'NULL';

--
-- TOC entry 2740 (class 0 OID 30366881)
-- Dependencies: 1547
-- Data for Name: isms_saas; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO isms_saas (pkconfig, svalue) VALUES (711709, '1');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711713, '%user_email%');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711714, '%config_path%');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711715, '%max_file_size%');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711701, '%saas_report_email%');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711712, '');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711703, '');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711704, '');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711705, '');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711706, '');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711707, '');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711710, '');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711708, '');


--
-- TOC entry 2741 (class 0 OID 30366885)
-- Dependencies: 1549
-- Data for Name: isms_scope; Type: TABLE DATA; Schema: public; Owner: isms
--



--
-- TOC entry 2742 (class 0 OID 30366890)
-- Dependencies: 1550
-- Data for Name: isms_solicitor; Type: TABLE DATA; Schema: public; Owner: isms
--



--
-- TOC entry 2717 (class 0 OID 30366717)
-- Dependencies: 1520
-- Data for Name: isms_user; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO isms_user (fkcontext, fkprofile, sname, slogin, spassword, semail, nip, nlanguage, dlastlogin, nwronglogonattempts, bisblocked, bmustchangepassword, srequestpassword, ssession, bansweredsurvey, bshowhelp) VALUES (13, 1, '%user_fullname%', '%user_login%', '%user_passwordmd5%', '%user_email%', -1062731653, %user_language%, null, 0, 0, 0, '', '', 0, 0);


--
-- TOC entry 2743 (class 0 OID 30366892)
-- Dependencies: 1551
-- Data for Name: isms_user_password_history; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO isms_user_password_history (fkuser, spassword, ddatepasswordchanged) VALUES (13, 'c21f969b5f03d33d43e04f8f136e7682', '2008-12-01 08:42:06');


--
-- TOC entry 2744 (class 0 OID 30366894)
-- Dependencies: 1552
-- Data for Name: isms_user_preference; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO isms_user_preference (fkuser, spreference, tvalue) VALUES (13, 'dashboard_sumary_pref', 'a:21:{i:0;s:26:"grid_best_practice_summary";i:1;s:17:"grid_risk_summary";i:2;s:20:"grid_control_summary";i:3;s:18:"grid_risk_summary2";i:4;s:26:"grid_residual_risk_summary";i:5;s:22:"grid_financial_summary";i:6;s:31:"grid_isms_reports_and_documents";i:7;s:25:"grid_abnormalitys_summary";i:8;s:15:"grid_doc_sumary";i:9;s:31:"grid_doc_context_with_documents";i:10;s:22:"grid_doc_approver_time";i:11;s:14:"grid_doc_top10";i:12;s:24:"grid_doc_period_revision";i:13;s:19:"grid_abnormality_pm";i:14;s:21:"grid_incident_summary";i:15;s:15:"grid_nc_summary";i:16;s:23:"grid_incidents_per_user";i:17;s:24:"grid_incidents_per_asset";i:18;s:26:"grid_incidents_per_process";i:19;s:19:"grid_nc_per_process";i:20;s:19:"grid_abnormality_ci";}');


--
-- TOC entry 2745 (class 0 OID 30366899)
-- Dependencies: 1553
-- Data for Name: pm_doc_approvers; Type: TABLE DATA; Schema: public; Owner: isms
--



--
-- TOC entry 2746 (class 0 OID 30366907)
-- Dependencies: 1554
-- Data for Name: pm_doc_context; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO pm_doc_context (fkcontext, fkdocument) VALUES (11226, 11227);
INSERT INTO pm_doc_context (fkcontext, fkdocument) VALUES (11228, 11229);
INSERT INTO pm_doc_context (fkcontext, fkdocument) VALUES (11230, 11231);
INSERT INTO pm_doc_context (fkcontext, fkdocument) VALUES (11232, 11233);
INSERT INTO pm_doc_context (fkcontext, fkdocument) VALUES (11234, 11235);
INSERT INTO pm_doc_context (fkcontext, fkdocument) VALUES (11236, 11237);
INSERT INTO pm_doc_context (fkcontext, fkdocument) VALUES (11240, 11241);
INSERT INTO pm_doc_context (fkcontext, fkdocument) VALUES (11242, 11243);
INSERT INTO pm_doc_context (fkcontext, fkdocument) VALUES (11244, 11245);
INSERT INTO pm_doc_context (fkcontext, fkdocument) VALUES (11246, 11247);
INSERT INTO pm_doc_context (fkcontext, fkdocument) VALUES (11248, 11249);
INSERT INTO pm_doc_context (fkcontext, fkdocument) VALUES (11258, 11259);
INSERT INTO pm_doc_context (fkcontext, fkdocument) VALUES (11260, 11261);
INSERT INTO pm_doc_context (fkcontext, fkdocument) VALUES (11389, 11390);
INSERT INTO pm_doc_context (fkcontext, fkdocument) VALUES (11404, 11405);
INSERT INTO pm_doc_context (fkcontext, fkdocument) VALUES (11934, 11935);
INSERT INTO pm_doc_context (fkcontext, fkdocument) VALUES (11952, 11953);
INSERT INTO pm_doc_context (fkcontext, fkdocument) VALUES (11965, 11966);


--
-- TOC entry 2718 (class 0 OID 30366731)
-- Dependencies: 1521
-- Data for Name: pm_doc_instance; Type: TABLE DATA; Schema: public; Owner: isms
--



--
-- TOC entry 2747 (class 0 OID 30366909)
-- Dependencies: 1555
-- Data for Name: pm_doc_read_history; Type: TABLE DATA; Schema: public; Owner: isms
--



--
-- TOC entry 2748 (class 0 OID 30366911)
-- Dependencies: 1556
-- Data for Name: pm_doc_readers; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO pm_doc_readers (fkdocument, fkuser, bhasread, bmanual, bdenied) VALUES (11241, 13, 0, 0, 0);
INSERT INTO pm_doc_readers (fkdocument, fkuser, bhasread, bmanual, bdenied) VALUES (11243, 13, 0, 0, 0);
INSERT INTO pm_doc_readers (fkdocument, fkuser, bhasread, bmanual, bdenied) VALUES (11245, 13, 0, 0, 0);
INSERT INTO pm_doc_readers (fkdocument, fkuser, bhasread, bmanual, bdenied) VALUES (11247, 13, 0, 0, 0);
INSERT INTO pm_doc_readers (fkdocument, fkuser, bhasread, bmanual, bdenied) VALUES (11249, 13, 0, 0, 0);
INSERT INTO pm_doc_readers (fkdocument, fkuser, bhasread, bmanual, bdenied) VALUES (11261, 13, 0, 0, 0);
INSERT INTO pm_doc_readers (fkdocument, fkuser, bhasread, bmanual, bdenied) VALUES (11390, 13, 0, 0, 0);
INSERT INTO pm_doc_readers (fkdocument, fkuser, bhasread, bmanual, bdenied) VALUES (11935, 13, 0, 0, 0);
INSERT INTO pm_doc_readers (fkdocument, fkuser, bhasread, bmanual, bdenied) VALUES (11953, 13, 0, 0, 0);


--
-- TOC entry 2749 (class 0 OID 30366919)
-- Dependencies: 1557
-- Data for Name: pm_doc_reference; Type: TABLE DATA; Schema: public; Owner: isms
--



--
-- TOC entry 2750 (class 0 OID 30366926)
-- Dependencies: 1559
-- Data for Name: pm_doc_registers; Type: TABLE DATA; Schema: public; Owner: isms
--



--
-- TOC entry 2719 (class 0 OID 30366741)
-- Dependencies: 1522
-- Data for Name: pm_document; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO pm_document (fkcontext, fkschedule, fkclassification, fkcurrentversion, fkparent, fkauthor, fkmainapprover, sname, tdescription, ntype, skeywords, ddateproduction, ddeadline, bflagdeadlinealert, bflagdeadlineexpired, ndaysbefore, bhasapproved) VALUES (11227, NULL, NULL, NULL, NULL, 13, 13, 'Unit Sample SOX Corporation Inc. Document', 'Unit Sample SOX Corporation Inc. Document', 2801, NULL, NULL, NULL, 0, 0, 0, 0);
INSERT INTO pm_document (fkcontext, fkschedule, fkclassification, fkcurrentversion, fkparent, fkauthor, fkmainapprover, sname, tdescription, ntype, skeywords, ddateproduction, ddeadline, bflagdeadlinealert, bflagdeadlineexpired, ndaysbefore, bhasapproved) VALUES (11229, NULL, NULL, NULL, NULL, 13, 13, 'Unit Sample Corp - Italy Business Unit Document', 'Unit Sample Corp - Italy Business Unit Document', 2801, NULL, NULL, NULL, 0, 0, 0, 0);
INSERT INTO pm_document (fkcontext, fkschedule, fkclassification, fkcurrentversion, fkparent, fkauthor, fkmainapprover, sname, tdescription, ntype, skeywords, ddateproduction, ddeadline, bflagdeadlinealert, bflagdeadlineexpired, ndaysbefore, bhasapproved) VALUES (11231, NULL, NULL, NULL, NULL, 13, 13, 'Unit Sample Corp - Germany Business Unit Document', 'Unit Sample Corp - Germany Business Unit Document', 2801, NULL, NULL, NULL, 0, 0, 0, 0);
INSERT INTO pm_document (fkcontext, fkschedule, fkclassification, fkcurrentversion, fkparent, fkauthor, fkmainapprover, sname, tdescription, ntype, skeywords, ddateproduction, ddeadline, bflagdeadlinealert, bflagdeadlineexpired, ndaysbefore, bhasapproved) VALUES (11233, NULL, NULL, NULL, NULL, 13, 13, 'Department CR - Customer Relationship Document', 'Department CR - Customer Relationship Document', 2802, NULL, NULL, NULL, 0, 0, 0, 0);
INSERT INTO pm_document (fkcontext, fkschedule, fkclassification, fkcurrentversion, fkparent, fkauthor, fkmainapprover, sname, tdescription, ntype, skeywords, ddateproduction, ddeadline, bflagdeadlinealert, bflagdeadlineexpired, ndaysbefore, bhasapproved) VALUES (11235, NULL, NULL, NULL, NULL, 13, 13, 'Department IT - Information Technology Document', 'Department IT - Information Technology Document', 2802, NULL, NULL, NULL, 0, 0, 0, 0);
INSERT INTO pm_document (fkcontext, fkschedule, fkclassification, fkcurrentversion, fkparent, fkauthor, fkmainapprover, sname, tdescription, ntype, skeywords, ddateproduction, ddeadline, bflagdeadlinealert, bflagdeadlineexpired, ndaysbefore, bhasapproved) VALUES (11237, NULL, NULL, NULL, NULL, 13, 13, 'Department RD - Research & Development Document', 'Department RD - Research & Development Document', 2802, NULL, NULL, NULL, 0, 0, 0, 0);
INSERT INTO pm_document (fkcontext, fkschedule, fkclassification, fkcurrentversion, fkparent, fkauthor, fkmainapprover, sname, tdescription, ntype, skeywords, ddateproduction, ddeadline, bflagdeadlinealert, bflagdeadlineexpired, ndaysbefore, bhasapproved) VALUES (11241, NULL, NULL, NULL, NULL, 13, 13, 'Process Market Test Document', 'Process Market Test Document', 2803, NULL, NULL, NULL, 0, 0, 0, 0);
INSERT INTO pm_document (fkcontext, fkschedule, fkclassification, fkcurrentversion, fkparent, fkauthor, fkmainapprover, sname, tdescription, ntype, skeywords, ddateproduction, ddeadline, bflagdeadlinealert, bflagdeadlineexpired, ndaysbefore, bhasapproved) VALUES (11243, NULL, NULL, NULL, NULL, 13, 13, 'Process Network Administration Document', 'Process Network Administration Document', 2803, NULL, NULL, NULL, 0, 0, 0, 0);
INSERT INTO pm_document (fkcontext, fkschedule, fkclassification, fkcurrentversion, fkparent, fkauthor, fkmainapprover, sname, tdescription, ntype, skeywords, ddateproduction, ddeadline, bflagdeadlinealert, bflagdeadlineexpired, ndaysbefore, bhasapproved) VALUES (11245, NULL, NULL, NULL, NULL, 13, 13, 'Process New Product Development Document', 'Process New Product Development Document', 2803, NULL, NULL, NULL, 0, 0, 0, 0);
INSERT INTO pm_document (fkcontext, fkschedule, fkclassification, fkcurrentversion, fkparent, fkauthor, fkmainapprover, sname, tdescription, ntype, skeywords, ddateproduction, ddeadline, bflagdeadlinealert, bflagdeadlineexpired, ndaysbefore, bhasapproved) VALUES (11247, NULL, NULL, NULL, NULL, 13, 13, 'Process System Development and Maintenance Document', 'Process System Development and Maintenance Document', 2803, NULL, NULL, NULL, 0, 0, 0, 0);
INSERT INTO pm_document (fkcontext, fkschedule, fkclassification, fkcurrentversion, fkparent, fkauthor, fkmainapprover, sname, tdescription, ntype, skeywords, ddateproduction, ddeadline, bflagdeadlinealert, bflagdeadlineexpired, ndaysbefore, bhasapproved) VALUES (11249, NULL, NULL, NULL, NULL, 13, 13, 'Process Warranty Control Document', 'Process Warranty Control Document', 2803, NULL, NULL, NULL, 0, 0, 0, 0);
INSERT INTO pm_document (fkcontext, fkschedule, fkclassification, fkcurrentversion, fkparent, fkauthor, fkmainapprover, sname, tdescription, ntype, skeywords, ddateproduction, ddeadline, bflagdeadlinealert, bflagdeadlineexpired, ndaysbefore, bhasapproved) VALUES (11259, NULL, NULL, NULL, NULL, 13, 13, 'Department Board of Directors Document', 'Department Board of Directors Document', 2802, NULL, NULL, NULL, 0, 0, 0, 0);
INSERT INTO pm_document (fkcontext, fkschedule, fkclassification, fkcurrentversion, fkparent, fkauthor, fkmainapprover, sname, tdescription, ntype, skeywords, ddateproduction, ddeadline, bflagdeadlinealert, bflagdeadlineexpired, ndaysbefore, bhasapproved) VALUES (11261, NULL, NULL, NULL, NULL, 13, 13, 'Process Strategic Management Document', 'Process Strategic Management Document', 2803, NULL, NULL, NULL, 0, 0, 0, 0);
INSERT INTO pm_document (fkcontext, fkschedule, fkclassification, fkcurrentversion, fkparent, fkauthor, fkmainapprover, sname, tdescription, ntype, skeywords, ddateproduction, ddeadline, bflagdeadlinealert, bflagdeadlineexpired, ndaysbefore, bhasapproved) VALUES (11390, NULL, NULL, NULL, NULL, 13, 13, 'Process Compliance Management Document', 'Process Compliance Management Document', 2803, NULL, NULL, NULL, 0, 0, 0, 0);
INSERT INTO pm_document (fkcontext, fkschedule, fkclassification, fkcurrentversion, fkparent, fkauthor, fkmainapprover, sname, tdescription, ntype, skeywords, ddateproduction, ddeadline, bflagdeadlinealert, bflagdeadlineexpired, ndaysbefore, bhasapproved) VALUES (11405, NULL, NULL, NULL, NULL, 13, 13, 'Department Legal & Compliance (Worldwide) Document', 'Department Legal & Compliance (Worldwide) Document', 2802, NULL, NULL, NULL, 0, 0, 0, 0);
INSERT INTO pm_document (fkcontext, fkschedule, fkclassification, fkcurrentversion, fkparent, fkauthor, fkmainapprover, sname, tdescription, ntype, skeywords, ddateproduction, ddeadline, bflagdeadlinealert, bflagdeadlineexpired, ndaysbefore, bhasapproved) VALUES (11953, NULL, NULL, NULL, NULL, 13, 13, 'Control Human Resource Policy Document', 'Control Human Resource Policy Document', 2805, NULL, NULL, '2008-12-31 00:00:00', 0, 0, 0, 0);
INSERT INTO pm_document (fkcontext, fkschedule, fkclassification, fkcurrentversion, fkparent, fkauthor, fkmainapprover, sname, tdescription, ntype, skeywords, ddateproduction, ddeadline, bflagdeadlinealert, bflagdeadlineexpired, ndaysbefore, bhasapproved) VALUES (11935, NULL, NULL, NULL, NULL, 13, 13, 'Control Sample Corp. Ethical Code Document', 'Control Sample Corp. Ethical Code Document', 2805, NULL, NULL, '2008-12-05 00:00:00', 1, 1, 0, 0);
INSERT INTO pm_document (fkcontext, fkschedule, fkclassification, fkcurrentversion, fkparent, fkauthor, fkmainapprover, sname, tdescription, ntype, skeywords, ddateproduction, ddeadline, bflagdeadlinealert, bflagdeadlineexpired, ndaysbefore, bhasapproved) VALUES (11966, NULL, NULL, NULL, NULL, 13, 13, 'Control Information Security Policy Document', 'Control Information Security Policy Document', 2805, NULL, NULL, '2008-12-05 00:00:00', 1, 1, 0, 0);


--
-- TOC entry 2751 (class 0 OID 30366928)
-- Dependencies: 1560
-- Data for Name: pm_document_revision_history; Type: TABLE DATA; Schema: public; Owner: isms
--



--
-- TOC entry 2752 (class 0 OID 30366935)
-- Dependencies: 1561
-- Data for Name: pm_instance_comment; Type: TABLE DATA; Schema: public; Owner: isms
--



--
-- TOC entry 2753 (class 0 OID 30366942)
-- Dependencies: 1563
-- Data for Name: pm_instance_content; Type: TABLE DATA; Schema: public; Owner: isms
--



--
-- TOC entry 2754 (class 0 OID 30366947)
-- Dependencies: 1564
-- Data for Name: pm_process_user; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO pm_process_user (fkprocess, fkuser) VALUES (11232, 13);
INSERT INTO pm_process_user (fkprocess, fkuser) VALUES (11234, 13);
INSERT INTO pm_process_user (fkprocess, fkuser) VALUES (11236, 13);
INSERT INTO pm_process_user (fkprocess, fkuser) VALUES (11258, 13);
INSERT INTO pm_process_user (fkprocess, fkuser) VALUES (11404, 13);


--
-- TOC entry 2755 (class 0 OID 30366949)
-- Dependencies: 1565
-- Data for Name: pm_register; Type: TABLE DATA; Schema: public; Owner: isms
--



--
-- TOC entry 2756 (class 0 OID 30366956)
-- Dependencies: 1566
-- Data for Name: pm_register_readers; Type: TABLE DATA; Schema: public; Owner: isms
--



--
-- TOC entry 2720 (class 0 OID 30366754)
-- Dependencies: 1523
-- Data for Name: pm_template; Type: TABLE DATA; Schema: public; Owner: isms
--



--
-- TOC entry 2757 (class 0 OID 30366962)
-- Dependencies: 1567
-- Data for Name: pm_template_best_practice; Type: TABLE DATA; Schema: public; Owner: isms
--



--
-- TOC entry 2758 (class 0 OID 30366964)
-- Dependencies: 1568
-- Data for Name: pm_template_content; Type: TABLE DATA; Schema: public; Owner: isms
--



--
-- TOC entry 2721 (class 0 OID 30366761)
-- Dependencies: 1524
-- Data for Name: rm_area; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO rm_area (fkcontext, fkpriority, fktype, fkparent, fkresponsible, sname, tdescription, sdocument, nvalue) VALUES (11228, NULL, 2, 11226, 13, 'Sample Corp - Italy Business Unit', '', NULL, 14);
INSERT INTO rm_area (fkcontext, fkpriority, fktype, fkparent, fkresponsible, sname, tdescription, sdocument, nvalue) VALUES (11230, NULL, NULL, 11226, 13, 'Sample Corp - Germany Business Unit', '', NULL, 20);
INSERT INTO rm_area (fkcontext, fkpriority, fktype, fkparent, fkresponsible, sname, tdescription, sdocument, nvalue) VALUES (11226, NULL, 1, NULL, 13, 'Sample Corporation Inc.', '', NULL, 20);


--
-- TOC entry 2722 (class 0 OID 30366767)
-- Dependencies: 1525
-- Data for Name: rm_asset; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO rm_asset (fkcontext, fkcategory, fkresponsible, fksecurityresponsible, sname, tdescription, sdocument, ncost, nvalue, blegality, tjustification) VALUES (11246, 11225, 13, 13, 'System Development and Maintenance', '', NULL, 0, 14, 0, '');
INSERT INTO rm_asset (fkcontext, fkcategory, fkresponsible, fksecurityresponsible, sname, tdescription, sdocument, ncost, nvalue, blegality, tjustification) VALUES (11248, 11225, 13, 13, 'Warranty Control', '', NULL, 0, 14, 0, '');
INSERT INTO rm_asset (fkcontext, fkcategory, fkresponsible, fksecurityresponsible, sname, tdescription, sdocument, ncost, nvalue, blegality, tjustification) VALUES (11240, 11225, 13, 13, 'Market Test', '', NULL, 0, 14, 0, '');
INSERT INTO rm_asset (fkcontext, fkcategory, fkresponsible, fksecurityresponsible, sname, tdescription, sdocument, ncost, nvalue, blegality, tjustification) VALUES (11238, 11225, 13, 13, 'Contracts Management', '', NULL, 0, 14, 0, '');
INSERT INTO rm_asset (fkcontext, fkcategory, fkresponsible, fksecurityresponsible, sname, tdescription, sdocument, ncost, nvalue, blegality, tjustification) VALUES (11389, 10135, 13, 13, 'Compliance Management', '', NULL, 0, 14, 0, '');
INSERT INTO rm_asset (fkcontext, fkcategory, fkresponsible, fksecurityresponsible, sname, tdescription, sdocument, ncost, nvalue, blegality, tjustification) VALUES (11242, 11225, 13, 13, 'Network Administration', '', NULL, 0, 20, 0, '');
INSERT INTO rm_asset (fkcontext, fkcategory, fkresponsible, fksecurityresponsible, sname, tdescription, sdocument, ncost, nvalue, blegality, tjustification) VALUES (11244, 11225, 13, 13, 'New Product Development', '', NULL, 0, 16, 0, '');
INSERT INTO rm_asset (fkcontext, fkcategory, fkresponsible, fksecurityresponsible, sname, tdescription, sdocument, ncost, nvalue, blegality, tjustification) VALUES (11260, 10007, 13, 13, 'Strategic Management', '', NULL, 0, 14, 0, '');


--
-- TOC entry 2759 (class 0 OID 30366969)
-- Dependencies: 1569
-- Data for Name: rm_asset_asset; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO rm_asset_asset (fkasset, fkdependent) VALUES (11389, 11240);
INSERT INTO rm_asset_asset (fkasset, fkdependent) VALUES (11389, 11242);
INSERT INTO rm_asset_asset (fkasset, fkdependent) VALUES (11389, 11244);
INSERT INTO rm_asset_asset (fkasset, fkdependent) VALUES (11389, 11260);
INSERT INTO rm_asset_asset (fkasset, fkdependent) VALUES (11389, 11246);
INSERT INTO rm_asset_asset (fkasset, fkdependent) VALUES (11389, 11248);
INSERT INTO rm_asset_asset (fkasset, fkdependent) VALUES (11238, 11240);
INSERT INTO rm_asset_asset (fkasset, fkdependent) VALUES (11238, 11242);
INSERT INTO rm_asset_asset (fkasset, fkdependent) VALUES (11238, 11244);
INSERT INTO rm_asset_asset (fkasset, fkdependent) VALUES (11238, 11246);
INSERT INTO rm_asset_asset (fkasset, fkdependent) VALUES (11238, 11248);


--
-- TOC entry 2760 (class 0 OID 30366971)
-- Dependencies: 1570
-- Data for Name: rm_asset_value; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO rm_asset_value (fkasset, fkvaluename, fkparametername, tjustification) VALUES (11238, 3, 1, '');
INSERT INTO rm_asset_value (fkasset, fkvaluename, fkparametername, tjustification) VALUES (11240, 3, 1, '');
INSERT INTO rm_asset_value (fkasset, fkvaluename, fkparametername, tjustification) VALUES (11248, 1, 1, '');
INSERT INTO rm_asset_value (fkasset, fkvaluename, fkparametername, tjustification) VALUES (11389, 5, 1, '');
INSERT INTO rm_asset_value (fkasset, fkvaluename, fkparametername, tjustification) VALUES (11242, 5, 1, '');
INSERT INTO rm_asset_value (fkasset, fkvaluename, fkparametername, tjustification) VALUES (11244, 3, 1, '');
INSERT INTO rm_asset_value (fkasset, fkvaluename, fkparametername, tjustification) VALUES (11246, 3, 1, '');
INSERT INTO rm_asset_value (fkasset, fkvaluename, fkparametername, tjustification) VALUES (11260, 5, 1, '');


--
-- TOC entry 2723 (class 0 OID 30366776)
-- Dependencies: 1526
-- Data for Name: rm_best_practice; Type: TABLE DATA; Schema: public; Owner: isms
--

COPY rm_best_practice (fkcontext, fksectionbestpractice, sname, tdescription, ncontroltype, sclassification, timplementationguide, tmetric, sdocument, tjustification) FROM '/tmp/trialdata/sox_rm_best_practice.sql' DELIMITERS '#' NULL 'NULL';

--
-- TOC entry 2761 (class 0 OID 30366976)
-- Dependencies: 1571
-- Data for Name: rm_best_practice_event; Type: TABLE DATA; Schema: public; Owner: isms
--



--
-- TOC entry 2762 (class 0 OID 30366978)
-- Dependencies: 1572
-- Data for Name: rm_best_practice_standard; Type: TABLE DATA; Schema: public; Owner: isms
--

COPY rm_best_practice_standard (fkcontext, fkstandard, fkbestpractice) FROM '/tmp/trialdata/sox_rm_best_practice_standard.sql' DELIMITERS '#' NULL 'NULL';

--
-- TOC entry 2724 (class 0 OID 30366782)
-- Dependencies: 1527
-- Data for Name: rm_category; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO rm_category (fkcontext, fkparent, sname, tdescription) VALUES (10007, NULL, 'Strategic', NULL);
INSERT INTO rm_category (fkcontext, fkparent, sname, tdescription) VALUES (10135, NULL, 'Compliance', NULL);
INSERT INTO rm_category (fkcontext, fkparent, sname, tdescription) VALUES (10149, NULL, 'Financial Reporting', NULL);
INSERT INTO rm_category (fkcontext, fkparent, sname, tdescription) VALUES (11225, NULL, 'Operations', '');
INSERT INTO rm_category (fkcontext, fkparent, sname, tdescription) VALUES (11430, 11225, 'Sales', '');
INSERT INTO rm_category (fkcontext, fkparent, sname, tdescription) VALUES (11431, 11225, 'Production & Delivery', '');
INSERT INTO rm_category (fkcontext, fkparent, sname, tdescription) VALUES (11435, 11225, 'Finance & Accounting', '');
INSERT INTO rm_category (fkcontext, fkparent, sname, tdescription) VALUES (11433, 11225, 'Human Resource', '');
INSERT INTO rm_category (fkcontext, fkparent, sname, tdescription) VALUES (11434, 11225, 'Information Technology', '');
INSERT INTO rm_category (fkcontext, fkparent, sname, tdescription) VALUES (11432, 11225, 'New Product Development', '');


--
-- TOC entry 2725 (class 0 OID 30366787)
-- Dependencies: 1528
-- Data for Name: rm_control; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO rm_control (fkcontext, fktype, fkresponsible, sname, tdescription, sdocument, sevidence, ndaysbefore, bisactive, bflagimplalert, bflagimplexpired, ddatedeadline, ddateimplemented, nimplementationstate, bimplementationislate, brevisionislate, btestislate, befficiencynotok, btestnotok) VALUES (11934, 25, 13, 'Sample Corp. Ethical Code', '', NULL, '', 1, 1, 0, 0, '2008-12-05 00:00:00', '2008-12-05 00:00:00', 0, 0, 0, 0, 0, 0);
INSERT INTO rm_control (fkcontext, fktype, fkresponsible, sname, tdescription, sdocument, sevidence, ndaysbefore, bisactive, bflagimplalert, bflagimplexpired, ddatedeadline, ddateimplemented, nimplementationstate, bimplementationislate, brevisionislate, btestislate, befficiencynotok, btestnotok) VALUES (11965, 25, 13, 'Information Security Policy', '', NULL, '', 2, 1, 0, 0, '2008-12-05 00:00:00', '2008-12-12 00:00:00', 0, 0, 0, 0, 0, 0);
INSERT INTO rm_control (fkcontext, fktype, fkresponsible, sname, tdescription, sdocument, sevidence, ndaysbefore, bisactive, bflagimplalert, bflagimplexpired, ddatedeadline, ddateimplemented, nimplementationstate, bimplementationislate, brevisionislate, btestislate, befficiencynotok, btestnotok) VALUES (11952, 25, 13, 'Human Resource Policy', '', NULL, '', 1, 1, 0, 0, '2008-12-31 00:00:00', '2008-12-05 00:00:00', 0, 0, 0, 0, 0, 0);


--
-- TOC entry 2763 (class 0 OID 30366980)
-- Dependencies: 1573
-- Data for Name: rm_control_best_practice; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO rm_control_best_practice (fkcontext, fkbestpractice, fkcontrol) VALUES (11936, 10640, 11934);
INSERT INTO rm_control_best_practice (fkcontext, fkbestpractice, fkcontrol) VALUES (11937, 10615, 11934);
INSERT INTO rm_control_best_practice (fkcontext, fkbestpractice, fkcontrol) VALUES (11938, 10617, 11934);
INSERT INTO rm_control_best_practice (fkcontext, fkbestpractice, fkcontrol) VALUES (11939, 10619, 11934);
INSERT INTO rm_control_best_practice (fkcontext, fkbestpractice, fkcontrol) VALUES (11940, 10621, 11934);
INSERT INTO rm_control_best_practice (fkcontext, fkbestpractice, fkcontrol) VALUES (11941, 10616, 11934);
INSERT INTO rm_control_best_practice (fkcontext, fkbestpractice, fkcontrol) VALUES (11942, 10620, 11934);
INSERT INTO rm_control_best_practice (fkcontext, fkbestpractice, fkcontrol) VALUES (11943, 10618, 11934);
INSERT INTO rm_control_best_practice (fkcontext, fkbestpractice, fkcontrol) VALUES (11944, 10622, 11934);
INSERT INTO rm_control_best_practice (fkcontext, fkbestpractice, fkcontrol) VALUES (11954, 10670, 11952);
INSERT INTO rm_control_best_practice (fkcontext, fkbestpractice, fkcontrol) VALUES (11955, 10672, 11952);
INSERT INTO rm_control_best_practice (fkcontext, fkbestpractice, fkcontrol) VALUES (11956, 10674, 11952);
INSERT INTO rm_control_best_practice (fkcontext, fkbestpractice, fkcontrol) VALUES (11957, 10671, 11952);
INSERT INTO rm_control_best_practice (fkcontext, fkbestpractice, fkcontrol) VALUES (11958, 10675, 11952);
INSERT INTO rm_control_best_practice (fkcontext, fkbestpractice, fkcontrol) VALUES (11959, 10673, 11952);
INSERT INTO rm_control_best_practice (fkcontext, fkbestpractice, fkcontrol) VALUES (11967, 10703, 11965);
INSERT INTO rm_control_best_practice (fkcontext, fkbestpractice, fkcontrol) VALUES (11968, 10705, 11965);
INSERT INTO rm_control_best_practice (fkcontext, fkbestpractice, fkcontrol) VALUES (11969, 10707, 11965);
INSERT INTO rm_control_best_practice (fkcontext, fkbestpractice, fkcontrol) VALUES (11970, 10709, 11965);
INSERT INTO rm_control_best_practice (fkcontext, fkbestpractice, fkcontrol) VALUES (11971, 10704, 11965);
INSERT INTO rm_control_best_practice (fkcontext, fkbestpractice, fkcontrol) VALUES (11972, 10708, 11965);
INSERT INTO rm_control_best_practice (fkcontext, fkbestpractice, fkcontrol) VALUES (11973, 10706, 11965);


--
-- TOC entry 2764 (class 0 OID 30366982)
-- Dependencies: 1574
-- Data for Name: rm_control_cost; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO rm_control_cost (fkcontrol, ncost1, ncost2, ncost3, ncost4, ncost5) VALUES (11934, 0, 0, 120000, 280000, 380000);
INSERT INTO rm_control_cost (fkcontrol, ncost1, ncost2, ncost3, ncost4, ncost5) VALUES (11965, 30000, 183000, 241000, 180000, 0);
INSERT INTO rm_control_cost (fkcontrol, ncost1, ncost2, ncost3, ncost4, ncost5) VALUES (11952, 0, 0, 80000, 0, 0);


--
-- TOC entry 2765 (class 0 OID 30366989)
-- Dependencies: 1575
-- Data for Name: rm_control_efficiency_history; Type: TABLE DATA; Schema: public; Owner: isms
--



--
-- TOC entry 2766 (class 0 OID 30366995)
-- Dependencies: 1576
-- Data for Name: rm_control_test_history; Type: TABLE DATA; Schema: public; Owner: isms
--



--
-- TOC entry 2726 (class 0 OID 30366810)
-- Dependencies: 1529
-- Data for Name: rm_event; Type: TABLE DATA; Schema: public; Owner: isms
--

COPY rm_event (fkcontext, fktype, fkcategory, sdescription, tobservation, bpropagate, timpact) FROM '/tmp/trialdata/sox_rm_event.sql' DELIMITERS '#' NULL 'NULL';

--
-- TOC entry 2767 (class 0 OID 30367002)
-- Dependencies: 1577
-- Data for Name: rm_parameter_name; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO rm_parameter_name (pkparametername, sname, nweight) VALUES (1, 'Significance', 1);


--
-- TOC entry 2768 (class 0 OID 30367007)
-- Dependencies: 1579
-- Data for Name: rm_parameter_value_name; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO rm_parameter_value_name (pkvaluename, nvalue, simportance, simpact, sriskprobability) VALUES (1, 1, 'Negligible', 'Very low', 'Rare (1% - 20%)');
INSERT INTO rm_parameter_value_name (pkvaluename, nvalue, simportance, simpact, sriskprobability) VALUES (2, 2, 'Low', 'Low', 'Unlikely (21% - 40%)');
INSERT INTO rm_parameter_value_name (pkvaluename, nvalue, simportance, simpact, sriskprobability) VALUES (3, 3, 'Medium', 'Medium', 'Moderate (41% - 60%)');
INSERT INTO rm_parameter_value_name (pkvaluename, nvalue, simportance, simpact, sriskprobability) VALUES (4, 4, 'High', 'High', 'Likely (61% - 80%)');
INSERT INTO rm_parameter_value_name (pkvaluename, nvalue, simportance, simpact, sriskprobability) VALUES (5, 5, 'Critical', 'Very high', 'Almost certain (81% - 99%)');


--
-- TOC entry 2727 (class 0 OID 30366817)
-- Dependencies: 1530
-- Data for Name: rm_process; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO rm_process (fkcontext, fkpriority, fktype, fkarea, fkresponsible, sname, tdescription, sdocument, nvalue, tjustification) VALUES (11232, NULL, 8, 11228, 13, 'CR - Customer Relationship', '', NULL, 14, NULL);
INSERT INTO rm_process (fkcontext, fkpriority, fktype, fkarea, fkresponsible, sname, tdescription, sdocument, nvalue, tjustification) VALUES (11258, NULL, NULL, 11226, 13, 'Worldwide - Board of Directors', '', NULL, 14, NULL);
INSERT INTO rm_process (fkcontext, fkpriority, fktype, fkarea, fkresponsible, sname, tdescription, sdocument, nvalue, tjustification) VALUES (11404, NULL, NULL, 11226, 13, 'Worldwide - Legal & Compliance', '', NULL, 14, NULL);
INSERT INTO rm_process (fkcontext, fkpriority, fktype, fkarea, fkresponsible, sname, tdescription, sdocument, nvalue, tjustification) VALUES (11236, NULL, NULL, 11230, 13, 'RD - Research & Development', '', NULL, 16, NULL);
INSERT INTO rm_process (fkcontext, fkpriority, fktype, fkarea, fkresponsible, sname, tdescription, sdocument, nvalue, tjustification) VALUES (11234, NULL, 12, 11230, 13, 'IT - Information Technology', '', NULL, 20, NULL);


--
-- TOC entry 2728 (class 0 OID 30366823)
-- Dependencies: 1531
-- Data for Name: rm_process_asset; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO rm_process_asset (fkcontext, fkprocess, fkasset) VALUES (11250, 11232, 11238);
INSERT INTO rm_process_asset (fkcontext, fkprocess, fkasset) VALUES (11251, 11236, 11240);
INSERT INTO rm_process_asset (fkcontext, fkprocess, fkasset) VALUES (11252, 11234, 11242);
INSERT INTO rm_process_asset (fkcontext, fkprocess, fkasset) VALUES (11253, 11236, 11244);
INSERT INTO rm_process_asset (fkcontext, fkprocess, fkasset) VALUES (11254, 11234, 11246);
INSERT INTO rm_process_asset (fkcontext, fkprocess, fkasset) VALUES (11255, 11232, 11248);
INSERT INTO rm_process_asset (fkcontext, fkprocess, fkasset) VALUES (11406, 11258, 11260);
INSERT INTO rm_process_asset (fkcontext, fkprocess, fkasset) VALUES (11407, 11258, 11389);
INSERT INTO rm_process_asset (fkcontext, fkprocess, fkasset) VALUES (11408, 11404, 11389);
INSERT INTO rm_process_asset (fkcontext, fkprocess, fkasset) VALUES (11409, 11404, 11260);


--
-- TOC entry 2769 (class 0 OID 30367016)
-- Dependencies: 1581
-- Data for Name: rm_rc_parameter_value_name; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO rm_rc_parameter_value_name (pkrcvaluename, nvalue, srcprobability, scontrolimpact) VALUES (1, 0, 'Do not affect', 'Do not affect');
INSERT INTO rm_rc_parameter_value_name (pkrcvaluename, nvalue, srcprobability, scontrolimpact) VALUES (2, 1, 'One Level Decrease', 'One Level Decrease');
INSERT INTO rm_rc_parameter_value_name (pkrcvaluename, nvalue, srcprobability, scontrolimpact) VALUES (3, 2, 'Two Level Decrease', 'Two Level Decrease');
INSERT INTO rm_rc_parameter_value_name (pkrcvaluename, nvalue, srcprobability, scontrolimpact) VALUES (4, 3, 'Three Level Decrease', 'Three Level Decrease');
INSERT INTO rm_rc_parameter_value_name (pkrcvaluename, nvalue, srcprobability, scontrolimpact) VALUES (5, 4, 'Four Level Decrease', 'Four Level Decrease');


--
-- TOC entry 2729 (class 0 OID 30366825)
-- Dependencies: 1532
-- Data for Name: rm_risk; Type: TABLE DATA; Schema: public; Owner: isms
--

COPY rm_risk (fkcontext, fkprobabilityvaluename, fktype, fkevent, fkasset, sname, tdescription, nvalue, nvalueresidual, tjustification, nacceptmode, nacceptstate, sacceptjustification, ncost, timpact) FROM '/tmp/trialdata/sox_rm_risk.sql' DELIMITERS '#' NULL 'NULL';

--
-- TOC entry 2730 (class 0 OID 30366835)
-- Dependencies: 1533
-- Data for Name: rm_risk_control; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO rm_risk_control (fkcontext, fkprobabilityvaluename, fkrisk, fkcontrol, tjustification) VALUES (11945, 2, 11838, 11934, NULL);
INSERT INTO rm_risk_control (fkcontext, fkprobabilityvaluename, fkrisk, fkcontrol, tjustification) VALUES (11946, 2, 11836, 11934, NULL);
INSERT INTO rm_risk_control (fkcontext, fkprobabilityvaluename, fkrisk, fkcontrol, tjustification) VALUES (11947, 2, 11832, 11934, NULL);
INSERT INTO rm_risk_control (fkcontext, fkprobabilityvaluename, fkrisk, fkcontrol, tjustification) VALUES (11948, 2, 11826, 11934, NULL);
INSERT INTO rm_risk_control (fkcontext, fkprobabilityvaluename, fkrisk, fkcontrol, tjustification) VALUES (11949, 2, 11814, 11934, NULL);
INSERT INTO rm_risk_control (fkcontext, fkprobabilityvaluename, fkrisk, fkcontrol, tjustification) VALUES (11950, 2, 11830, 11934, NULL);
INSERT INTO rm_risk_control (fkcontext, fkprobabilityvaluename, fkrisk, fkcontrol, tjustification) VALUES (11951, 2, 11834, 11934, NULL);
INSERT INTO rm_risk_control (fkcontext, fkprobabilityvaluename, fkrisk, fkcontrol, tjustification) VALUES (11960, 3, 11716, 11952, '');
INSERT INTO rm_risk_control (fkcontext, fkprobabilityvaluename, fkrisk, fkcontrol, tjustification) VALUES (11961, 2, 11929, 11952, NULL);
INSERT INTO rm_risk_control (fkcontext, fkprobabilityvaluename, fkrisk, fkcontrol, tjustification) VALUES (11962, 2, 11932, 11952, NULL);
INSERT INTO rm_risk_control (fkcontext, fkprobabilityvaluename, fkrisk, fkcontrol, tjustification) VALUES (11963, 2, 11702, 11952, NULL);
INSERT INTO rm_risk_control (fkcontext, fkprobabilityvaluename, fkrisk, fkcontrol, tjustification) VALUES (11964, 2, 11686, 11952, NULL);


--
-- TOC entry 2770 (class 0 OID 30367024)
-- Dependencies: 1583
-- Data for Name: rm_risk_control_value; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO rm_risk_control_value (fkriskcontrol, fkparametername, fkrcvaluename, tjustification) VALUES (11945, 1, 3, '');
INSERT INTO rm_risk_control_value (fkriskcontrol, fkparametername, fkrcvaluename, tjustification) VALUES (11946, 1, 3, '');
INSERT INTO rm_risk_control_value (fkriskcontrol, fkparametername, fkrcvaluename, tjustification) VALUES (11947, 1, 3, '');
INSERT INTO rm_risk_control_value (fkriskcontrol, fkparametername, fkrcvaluename, tjustification) VALUES (11948, 1, 3, '');
INSERT INTO rm_risk_control_value (fkriskcontrol, fkparametername, fkrcvaluename, tjustification) VALUES (11949, 1, 3, '');
INSERT INTO rm_risk_control_value (fkriskcontrol, fkparametername, fkrcvaluename, tjustification) VALUES (11950, 1, 3, '');
INSERT INTO rm_risk_control_value (fkriskcontrol, fkparametername, fkrcvaluename, tjustification) VALUES (11951, 1, 3, '');
INSERT INTO rm_risk_control_value (fkriskcontrol, fkparametername, fkrcvaluename, tjustification) VALUES (11960, 1, 2, '');
INSERT INTO rm_risk_control_value (fkriskcontrol, fkparametername, fkrcvaluename, tjustification) VALUES (11961, 1, 1, '');
INSERT INTO rm_risk_control_value (fkriskcontrol, fkparametername, fkrcvaluename, tjustification) VALUES (11962, 1, 1, '');
INSERT INTO rm_risk_control_value (fkriskcontrol, fkparametername, fkrcvaluename, tjustification) VALUES (11963, 1, 1, '');
INSERT INTO rm_risk_control_value (fkriskcontrol, fkparametername, fkrcvaluename, tjustification) VALUES (11964, 1, 1, '');


--
-- TOC entry 2731 (class 0 OID 30366840)
-- Dependencies: 1534
-- Data for Name: rm_risk_limits; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO rm_risk_limits (fkcontext, nlow, nhigh, nmidlow, nmidhigh) VALUES (14, 9, 18, 0, 0);


--
-- TOC entry 2771 (class 0 OID 30367029)
-- Dependencies: 1584
-- Data for Name: rm_risk_value; Type: TABLE DATA; Schema: public; Owner: isms
--

COPY rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) FROM '/tmp/trialdata/sox_rm_risk_value.sql' DELIMITERS '#' NULL 'NULL';

--
-- TOC entry 2732 (class 0 OID 30366842)
-- Dependencies: 1535
-- Data for Name: rm_section_best_practice; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (10361, NULL, 'CobiT');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (10362, 10361, 'Plan and Organise');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (10363, 10362, 'PO1Define a Strategic IT Plan');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (10370, 10362, 'PO2Define the Information Architecture');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (10375, 10362, 'PO3 Determine Technological Direction');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (10381, 10362, 'PO4 Define the IT Processes, Organisation and Relationships');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (10397, 10362, 'PO5 Manage the IT Investment');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (10403, 10362, 'PO6 Communicate Management Aims and Direction');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (10409, 10362, 'PO7 Manage IT Human Resources');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (10418, 10362, 'PO8 Manage Quality');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (10425, 10362, 'PO9 Assess and Manage IT Risks');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (10432, 10362, 'PO10 Manage Projects');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (10447, 10361, 'Acquire and Implement');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (10448, 10447, 'AI1 Identify Automated Solutions');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (10453, 10447, 'AI2 Acquire and Maintain Application Software');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (10464, 10447, 'AI3 Acquire and Maintain Technology Infrastructure');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (10469, 10447, 'AI4 Enable Operation and Use');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (10474, 10447, 'AI5 Procure IT Resources');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (10479, 10447, 'AI6 Manage Changes');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (10485, 10447, 'AI7 Install and Accredit Solutions and Changes');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (10495, 10361, 'Deliver and Support');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (10496, 10495, 'DS1 Define and Manage Service Levels');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (10503, 10495, 'DS2 Manage Third-party Services');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (10508, 10495, 'DS3 Manage Performance and Capacity');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (10514, 10495, 'DS4 Ensure Continuous Service');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (10525, 10495, 'DS5 Ensure Systems Security');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (10537, 10495, 'DS6 Identify and Allocate Costs');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (10542, 10495, 'DS7 Educate and Train Users');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (10546, 10495, 'DS8 Manage Service Desk and Incidents');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (10552, 10495, 'DS9 Manage the Configuration');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (10556, 10495, 'DS10 Manage Problems');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (10561, 10495, 'DS11 Manage Data');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (10568, 10495, 'DS12 Manage the Physical Environment');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (10574, 10495, 'DS13 Manage Operations');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (10580, 10361, 'Monitor and Evaluate');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (10581, 10580, 'ME1 Monitor and Evaluate IT Performance');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (10588, 10580, 'ME2 Monitor and Evaluate Internal Control');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (10596, 10580, 'ME3 Ensure Compliance With External Requirements');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (10602, 10580, 'ME4 Provide IT Governance');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (10610, NULL, 'COSO - General Entity-Wide Controls');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (10611, 10610, 'B - Commitment to Competence');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (10614, 10610, 'C - Board of Directors or Audit Committee');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (10634, 10610, 'D - Management Philosophy and Operating Style');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (10645, 10610, 'E - Organizational Structure');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (10651, 10610, 'F - Assignment of Authority and Responsibility');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (10656, 10610, 'A - Integrity and Ethical Values');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (10669, 10610, 'G - Human Resource Policies and Practices');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (10676, 10610, 'H - Entity-Wide Objectives');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (10681, 10610, 'I - Activity-Wide Objectives');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (10687, 10610, 'J - Risk Assessment');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (10695, 10610, 'K - Managing Change');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (10698, 10610, 'L - Control Activities');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (10702, 10610, 'M - Information');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (10710, 10610, 'N - Communication');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (10725, 10610, 'O - Ongoing Monitoring');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (10738, 10610, 'P - Separate Evaluation');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (10746, 10610, 'Q - Reporting');


--
-- TOC entry 2733 (class 0 OID 30366844)
-- Dependencies: 1536
-- Data for Name: rm_standard; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO rm_standard (fkcontext, sname, tdescription, tapplication, tobjective) VALUES (10754, 'COBIT', NULL, NULL, NULL);
INSERT INTO rm_standard (fkcontext, sname, tdescription, tapplication, tobjective) VALUES (10756, 'SOX - Section 404', '', '', '');


--
-- TOC entry 2772 (class 0 OID 30367188)
-- Dependencies: 1635
-- Data for Name: wkf_alert; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (1, 11935, 13, 13, ' ', 9213, 1, '2008-12-06 07:08:56');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (2, 11966, 13, 13, ' ', 9213, 1, '2008-12-06 07:08:56');


--
-- TOC entry 2773 (class 0 OID 30367198)
-- Dependencies: 1637
-- Data for Name: wkf_control_efficiency; Type: TABLE DATA; Schema: public; Owner: isms
--



--
-- TOC entry 2774 (class 0 OID 30367205)
-- Dependencies: 1638
-- Data for Name: wkf_control_test; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO wkf_control_test (fkcontroltest, fkschedule, tdescription) VALUES (11952, 1, 'Check the policy document.');


--
-- TOC entry 2775 (class 0 OID 30367210)
-- Dependencies: 1639
-- Data for Name: wkf_schedule; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO wkf_schedule (pkschedule, dstart, dend, ntype, nperiodicity, nbitmap, nweek, nday, dnextoccurrence, ndaystofinish, ddatelimit) VALUES (1, '2008-12-05 00:00:00', NULL, 7803, 6, 0, 0, 8, '2009-06-08 00:00:00', 1, NULL);


--
-- TOC entry 2776 (class 0 OID 30367220)
-- Dependencies: 1641
-- Data for Name: wkf_task; Type: TABLE DATA; Schema: public; Owner: isms
--



--
-- TOC entry 2777 (class 0 OID 30367227)
-- Dependencies: 1643
-- Data for Name: wkf_task_schedule; Type: TABLE DATA; Schema: public; Owner: isms
--

INSERT INTO wkf_task_schedule (fkcontext, nactivity, fkschedule, balertsent, nalerttype) VALUES (11952, 2216, 1, 0, 9209);


-- Completed on 2008-12-09 11:54:31

--
-- PostgreSQL database dump complete
--

-- Non Conformity
INSERT INTO isms_non_conformity_types(pkclassification, sname) VALUES (62251, 'Auditoria Interna');
INSERT INTO isms_non_conformity_types(pkclassification, sname) VALUES (62252, 'Auditoria Externa');
INSERT INTO isms_non_conformity_types(pkclassification, sname) VALUES (62253, 'Controle de Segurança');


