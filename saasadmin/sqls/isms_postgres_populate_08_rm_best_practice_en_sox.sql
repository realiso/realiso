INSERT INTO isms_context (pkContext,nType,nState) VALUES (10364,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10364,10363,0,'PO1.1 IT Value Management','Work with the business to ensure that the enterprise portfolio of IT-enabled investments contains programmes that have solid business cases. Recognise that there are mandatory, sustaining and discretionary investments that differ in complexity and degree of freedom in allocating funds. IT processes should provide effective and efficient delivery of the IT components of programmes and early warning of any deviations from plan, including cost, schedule or functionality, that might impact the expected outcomes of the programmes. IT services should be executed against equitable and enforceable service level agreements (SLAs). Accountability for achieving the benefits and controlling the costs should be clearly assigned and monitored. Establish fair, transparent, repeatable and comparable evaluation of business cases, including financial worth, the risk of not delivering a capability and the risk of not realising the expected benefits.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10364,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10364,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10365,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10365,10363,0,'PO1.2 Business-IT Alignment','Establish processes of bi-directional education and reciprocal involvement in strategic planning to achieve business and IT alignment and integration. Mediate between business and IT imperatives so priorities can be mutually agreed.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10365,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10365,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10366,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10366,10363,0,'PO1.3 Assessment of Current Capability and Performance','Assess the current capability and performance of solution and service delivery to establish a baseline against which future requirements can be compared. Define performance in terms of IT''s contribution to business objectives, functionality, stability, complexity, costs, strengths and weaknesses.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10366,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10366,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10367,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10367,10363,0,'PO1.4 IT Strategic Plan','Create a strategic plan that defines, in co-operation with relevant stakeholders, how IT goals will contribute to the enterprise''s strategic objectives and related costs and risks. It should include how IT will support IT-enabled investment programmes, IT services and IT assets. IT should define how the objectives will be met, the measurements to be used and the procedures to obtain formal sign-off from the stakeholders. The IT strategic plan should cover investment/operational budget, funding sources, sourcing strategy, acquisition strategy, and legal and regulatory requirements. The strategic plan should be sufficiently detailed to allow for the definition of tactical IT plans.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10367,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10367,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10368,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10368,10363,0,'PO1.5 IT Tactical Plans','Create a portfolio of tactical IT plans that are derived from the IT strategic plan. The tactical plans should address IT-enabled programme investments, IT services and IT assets. The tactical plans should describe required IT initiatives, resource requirements, and how the use of resources and achievement of benefits will be monitored and managed. The tactical plans should be sufficiently detailed to allow the definition of project plans. Actively manage the set of tactical IT plans and initiatives through analysis of project and service portfolios.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10368,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10368,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10369,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10369,10363,0,'PO1.6 IT Portfolio Management','Actively manage with the business the portfolio of IT-enabled investment programmes required to achieve specific strategic business objectives by identifying, defining, evaluating, prioritising, selecting, initiating, managing and controlling programmes. This should include clarifying desired business outcomes, ensuring that programme objectives support achievement of the outcomes, understanding the full scope of effort required to achieve the outcomes, assigning clear accountability with supporting measures, defining projects within the programme, allocating resources and funding, delegating authority, and commissioning required projects at programme launch.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10369,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10369,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10371,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10371,10370,0,'PO2.1 Enterprise Information Architecture Model','Establish and maintain an enterprise information model to enable applications development and decision-supporting activities, consistent with IT plans as described in PO1. The model should facilitate the optimal creation, use and sharing of information by the business in a way that maintains integrity and is flexible, functional, cost-effective, timely, secure and resilient to failure.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10371,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10371,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10372,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10372,10370,0,'PO2.2 Enterprise Data Dictionary and Data Syntax Rules','Maintain an enterprise data dictionary that incorporates the organisation''s data syntax rules. This dictionary should enable the sharing of data elements amongst applications and systems, promote a common understanding of data amongst IT and business users, and prevent incompatible data elements from being created.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10372,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10372,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10373,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10373,10370,0,'PO2.3 Data Classification Scheme','Establish a classification scheme that applies throughout the enterprise, based on the criticality and sensitivity (e.g., public, confidential, top secret) of enterprise data. This scheme should include details about data ownership; definition of appropriate security levels and protection controls; and a brief description of data retention and destruction requirements, criticality and sensitivity. It should be used as the basis for applying controls such as access controls, archiving or encryption.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10373,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10373,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10374,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10374,10370,0,'PO2.4 Integrity Management','Define and implement procedures to ensure the integrity and consistency of all data stored in electronic form, such as databases, data warehouses and data archives.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10374,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10374,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10376,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10376,10375,0,'PO3.1 Technological Direction Planning','Analyse existing and emerging technologies, and plan which technological direction is appropriate to realise the IT strategy and the business systems architecture. Also identify in the plan which technologies have the potential to create business opportunities. The plan should address systems architecture, technological direction, migration strategies and contingency aspects of infrastructure components.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10376,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10376,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10377,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10377,10375,0,'PO3.2 Technology Infrastructure Plan','Create and maintain a technology infrastructure plan that is in accordance with the IT strategic and tactical plans. The plan should be based on the technological direction and include contingency arrangements and direction for acquisition of technology resources. It should consider changes in the competitive environment, economies of scale for information systems staffing and investments, and improved interoperability of platforms and applications.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10377,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10377,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10378,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10378,10375,0,'PO3.3 Monitor Future Trends and Regulations','Establish a process to monitor the business sector, industry, technology, infrastructure, legal and regulatory environment trends. Incorporate the consequences of these trends into the development of the IT technology infrastructure plan.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10378,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10378,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10379,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10379,10375,0,'PO3.4 Technology Standards','To provide consistent, effective and secure technological solutions enterprisewide, establish a technology forum to provide technology guidelines, advice on infrastructure products and guidance on the selection of technology, and measure compliance with these standards and guidelines. This forum should direct technology standards and practices based on their business relevance, risks and compliance with external requirements.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10379,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10379,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10380,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10380,10375,0,'PO3.5 IT Architecture Board','Establish an IT architecture board to provide architecture guidelines and advice on their application, and to verify compliance. This entity should direct IT architecture design, ensuring that it enables the business strategy and considers regulatory compliance and continuity requirements. This is related/linked to PO2 Define the information architecture.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10380,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10380,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10382,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10382,10381,0,'PO4.1 IT Process Framework','Define an IT process framework to execute the IT strategic plan. This framework should include an IT process structure and relationships (e.g., to manage process gaps and overlaps), ownership, maturity, performance measurement, improvement, compliance, quality targets and plans to achieve them. It should provide integration amongst the processes that are specific to IT, enterprise portfolio management, business processes and business change processes. The IT process framework should be integrated into a quality management system (QMS) and the internal control framework.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10382,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10382,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10383,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10383,10381,0,'PO4.2 IT Strategy Committee','Establish an IT strategy committee at the board level. This committee should ensure that IT governance, as part of enterprise governance, is adequately addressed; advise on strategic direction; and review major investments on behalf of the full board.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10383,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10383,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10384,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10384,10381,0,'PO4.3 IT Steering Committee','Establish an IT steering committee (or equivalent) composed of executive, business and IT management to: - Determine prioritisation of IT-enabled investment programmes in line with the enterprise''s business strategy and priorities - Track status of projects and resolve resource conflict - Monitor service levels and service improvements','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10384,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10384,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10385,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10385,10381,0,'PO4.4 Organisational Placement of the IT Function','Place the IT function in the overall organisational structure with a business model contingent on the importance of IT within the enterprise, specifically its criticality to business strategy and the level of operational dependence on IT. The reporting line of the CIO should be commensurate with the importance of IT within the enterprise.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10385,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10385,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10386,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10386,10381,0,'PO4.5 IT Organisational Structure','Establish an internal and external IT organisational structure that reflects business needs. In addition, put a process in place for periodically reviewing the IT organisational structure to adjust staffing requirements and sourcing strategies to meet expected business objectives and changing circumstances.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10386,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10386,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10387,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10387,10381,0,'PO4.6 Establishment of Roles and Responsibilities','Establish and communicate roles and responsibilities for IT personnel and end users that delineate between IT personnel and end-user authority, responsibilities and accountability for meeting the organisation''s needs.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10387,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10387,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10388,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10388,10381,0,'PO4.7 Responsibility for IT Quality Assurance','Assign responsibility for the performance of the quality assurance (QA) function and provide the QA group with appropriate QA systems, controls and communications expertise. Ensure that the organisational placement and the responsibilities and size of the QA group satisfy the requirements of the organisation.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10388,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10388,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10389,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10389,10381,0,'PO4.8 Responsibility for Risk, Security and Compliance','Embed ownership and responsibility for IT-related risks within the business at an appropriate senior level. Define and assign roles critical for managing IT risks, including the specific responsibility for information security, physical security and compliance. Establish risk and security management responsibility at the enterprise level to deal with organisationwide issues. Additional security management responsibilities may need to be assigned at a system-specific level to deal with related security issues. Obtain direction from senior management on the appetite for IT risk and approval of any residual IT risks.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10389,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10389,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10390,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10390,10381,0,'PO4.9 Data and System Ownership','Provide the business with procedures and tools, enabling it to address its responsibilities for ownership of data and information systems. Owners should make decisions about classifying information and systems and protecting them in line with this classification.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10390,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10390,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10391,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10391,10381,0,'PO4.10 Supervision','Implement adequate supervisory practices in the IT function to ensure that roles and responsibilities are properly exercised, to assess whether all personnel have sufficient authority and resources to execute their roles and responsibilities, and to generally review KPIs.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10391,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10391,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10392,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10392,10381,0,'PO4.11 Segregation of Duties','Implement a division of roles and responsibilities that reduces the possibility for a single individual to compromise a critical process. Make sure that personnel are performing only authorised duties relevant to their respective jobs and positions.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10392,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10392,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10393,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10393,10381,0,'PO4.12 IT Staffing','Evaluate staffing requirements on a regular basis or upon major changes to the business, operational or IT environments to ensure that the IT function has sufficient resources to adequately and appropriately support the business goals and objectives.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10393,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10393,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10394,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10394,10381,0,'PO4.13 Key IT Personnel','Define and identify key IT personnel (e.g., replacements/backup personnel), and minimise reliance on a single individual performing a critical job function.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10394,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10394,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10395,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10395,10381,0,'PO4.14 Contracted Staff Policies and Procedures','Ensure that consultants and contract personnel who support the IT function know and comply with the organisation''s policies for the protection of the organisation''s information assets such that they meet agreed-upon contractual requirements.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10395,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10395,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10396,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10396,10381,0,'PO4.15 Relationships','Establish and maintain an optimal co-ordination, communication and liaison structure between the IT function and various other interests inside and outside the IT function, such as the board, executives, business units, individual users, suppliers, security officers, risk managers, the corporate compliance group, outsourcers and offsite management.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10396,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10396,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10398,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10398,10397,0,'PO5.1 Financial Management Framework','Establish and maintain a financial framework to manage the investment and cost of IT assets and services through portfolios of Itenabled investments, business cases and IT budgets.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10398,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10398,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10399,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10399,10397,0,'PO5.2 Prioritisation Within IT Budget','Implement a decision-making process to prioritise the allocation of IT resources for operations, projects and maintenance to maximise IT''s contribution to optimising the return on the enterprise''s portfolio of IT-enabled investment programmes and other IT services and assets.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10399,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10399,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10400,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10400,10397,0,'PO5.3 IT Budgeting','Establish and implement practices to prepare a budget reflecting the priorities established by the enterprise''s portfolio of IT-enabled investment programmes, and including the ongoing costs of operating and maintaining the current infrastructure. The practices should support development of an overall IT budget as well as development of budgets for individual programmes, with specific emphasis on the IT components of those programmes. The practices should allow for ongoing review, refinement and approval of the overall budget and the budgets for individual programmes.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10400,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10400,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10401,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10401,10397,0,'PO5.4 Cost Management','Implement a cost management process comparing actual costs to budgets. Costs should be monitored and reported. Where there are deviations, these should be identified in a timely manner and the impact of those deviations on programmes should be assessed. Together with the business sponsor of those programmes, appropriate remedial action should be taken and, if necessary, the programme business case should be updated.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10401,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10401,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10402,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10402,10397,0,'PO5.5 Benefit Management','Implement a process to monitor the benefits from providing and maintaining appropriate IT capabilities. IT''s contribution to the business, either as a component of IT-enabled investment programmes or as part of regular operational support, should be identified and documented in a business case, agreed to, monitored and reported. Reports should be reviewed and, where there are opportunities to improve IT''s contribution, appropriate actions should be defined and taken. Where changes in IT''s contribution impact the programme, or where changes to other related projects impact the programme, the programme business case should be updated.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10402,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10402,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10404,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10404,10403,0,'PO6.1 IT Policy and Control Environment','Define the elements of a control environment for IT, aligned with the enterprise''s management philosophy and operating style. These elements should include expectations/requirements regarding delivery of value from IT investments, appetite for risk, integrity, ethical values, staff competence, accountability and responsibility. The control environment should be based on a culture that supports value delivery whilst managing significant risks, encourages cross-divisional co-operation and teamwork, promotes compliance and continuous process improvement, and handles process deviations (including failure) well.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10404,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10404,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10405,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10405,10403,0,'PO6.2 Enterprise IT Risk and Control Framework','Develop and maintain a framework that defines the enterprise''s overall approach to IT risk and control and that aligns with the IT policy and control environment and the enterprise risk and control framework.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10405,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10405,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10406,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10406,10403,0,'PO6.3 IT Policies Management','Develop and maintain a set of policies to support IT strategy. These policies should include policy intent; roles and responsibilities; exception process; compliance approach; and references to procedures, standards and guidelines. Their relevance should be confirmed and approved regularly.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10406,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10406,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10407,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10407,10403,0,'PO6.4 Policy, Standard and Procedures Rollout','Roll out and enforce IT policies to all relevant staff, so they are built into and are an integral part of enterprise operations.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10407,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10407,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10408,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10408,10403,0,'PO6.5 Communication of IT Objectives and Direction','Communicate awareness and understanding of business and IT objectives and direction to appropriate stakeholders and users throughout the enterprise.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10408,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10408,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10410,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10410,10409,0,'PO7.1 Personnel Recruitment and Retention','Maintain IT personnel recruitment processes in line with the overall organisation''s personnel policies and procedures (e.g., hiring, positive work environment, orienting). Implement processes to ensure that the organisation has an appropriately deployed IT workforce with the skills necessary to achieve organisational goals.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10410,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10410,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10411,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10411,10409,0,'PO7.2 Personnel Competencies','Regularly verify that personnel have the competencies to fulfil their roles on the basis of their education, training and/or experience. Define core IT competency requirements and verify that they are being maintained, using qualification and certification programmes where appropriate.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10411,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10411,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10412,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10412,10409,0,'PO7.3 Staffing of Roles','Define, monitor and supervise roles, responsibilities and compensation frameworks for personnel, including the requirement to adhere to management policies and procedures, the code of ethics, and professional practices. The level of supervision should be in line with the sensitivity of the position and extent of responsibilities assigned.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10412,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10412,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10413,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10413,10409,0,'PO7.4 Personnel Training','Provide IT employees with appropriate orientation when hired and ongoing training to maintain their knowledge, skills, abilities, internal controls and security awareness at the level required to achieve organisational goals.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10413,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10413,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10414,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10414,10409,0,'PO7.5 Dependence Upon Individuals','Minimise the exposure to critical dependency on key individuals through knowledge capture (documentation), knowledge sharing, succession planning and staff backup.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10414,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10414,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10415,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10415,10409,0,'PO7.6 Personnel Clearance Procedures','Include background checks in the IT recruitment process. The extent and frequency of periodic reviews of these checks should depend on the sensitivity and/or criticality of the function and should be applied for employees, contractors and vendors.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10415,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10415,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10416,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10416,10409,0,'PO7.7 Employee Job Performance Evaluation','Require a timely evaluation to be performed on a regular basis against individual objectives derived from the organisation''s goals, established standards and specific job responsibilities. Employees should receive coaching on performance and conduct whenever appropriate.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10416,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10416,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10417,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10417,10409,0,'PO7.8 Job Change and Termination','Take expedient actions regarding job changes, especially job terminations. Knowledge transfer should be arranged, responsibilities reassigned and access rights removed such that risks are minimised and continuity of the function is guaranteed.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10417,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10417,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10419,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10419,10418,0,'PO8.1 Quality Management System','Establish and maintain a QMS that provides a standard, formal and continuous approach regarding quality management that is aligned with business requirements. The QMS should identify quality requirements and criteria; key IT processes and their sequence and interaction; and the policies, criteria and methods for defining, detecting, correcting and preventing non-conformity. The QMS should define the organisational structure for quality management, covering the roles, tasks and responsibilities. All key areas should develop their quality plans in line with criteria and policies and record quality data. Monitor and measure the effectiveness and acceptance of the QMS, and improve it when needed.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10419,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10419,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10420,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10420,10418,0,'PO8.2 IT Standards and Quality Practices','Identify and maintain standards, procedures and practices for key IT processes to guide the organisation in meeting the intent of the QMS. Use industry good practices for reference when improving and tailoring the organisation''s quality practices.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10420,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10420,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10421,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10421,10418,0,'PO8.3 Development and Acquisition Standards','Adopt and maintain standards for all development and acquisition that follow the life cycle of the ultimate deliverable, and include sign-off at key milestones based on agreed-upon sign-off criteria. Consider software coding standards; naming conventions; file formats; schema and data dictionary design standards; user interface standards; interoperability; system performance efficiency; scalability; standards for development and testing; validation against requirements; test plans; and unit, regression and integration testing.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10421,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10421,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10422,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10422,10418,0,'PO8.4 Customer Focus','Focus quality management on customers by determining their requirements and aligning them to the IT standards and practices. Define roles and responsibilities concerning conflict resolution between the user/customer and the IT organisation.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10422,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10422,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10423,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10423,10418,0,'PO8.5 Continuous Improvement','Maintain and regularly communicate an overall quality plan that promotes continuous improvement.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10423,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10423,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10424,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10424,10418,0,'PO8.6 Quality Measurement, Monitoring and Review','Define, plan and implement measurements to monitor continuing compliance to the QMS, as well as the value the QMS provides. Measurement, monitoring and recording of information should be used by the process owner to take appropriate corrective and preventive actions.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10424,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10424,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10426,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10426,10425,0,'PO9.1 IT Risk Management Framework','Establish an IT risk management framework that is aligned to the organisation''s (enterprise''s) risk management framework.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10426,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10426,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10427,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10427,10425,0,'PO9.2 Establishment of Risk Context','Establish the context in which the risk assessment framework is applied to ensure appropriate outcomes. This should include determining the internal and external context of each risk assessment, the goal of the assessment, and the criteria against which risks are evaluated.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10427,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10427,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10428,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10428,10425,0,'PO9.3 Event Identification','Identify events (an important realistic threat that exploits a significant applicable vulnerability) with a potential negative impact on the goals or operations of the enterprise, including business, regulatory, legal, technology, trading partner, human resources and operational aspects. Determine the nature of the impact and maintain this information. Record and maintain relevant risks in a risk registry.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10428,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10428,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10429,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10429,10425,0,'PO9.4 Risk Assessment','Assess on a recurrent basis the likelihood and impact of all identified risks, using qualitative and quantitative methods. The likelihood and impact associated with inherent and residual risk should be determined individually, by category and on a portfolio basis.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10429,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10429,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10430,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10430,10425,0,'PO9.5 Risk Response','Develop and maintain a risk response process designed to ensure that cost-effective controls mitigate exposure to risks on a continuing basis. The risk response process should identify risk strategies such as avoidance, reduction, sharing or acceptance; determine associated responsibilities; and consider risk tolerance levels.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10430,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10430,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10431,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10431,10425,0,'PO9.6 Maintenance and Monitoring of a Risk Action Plan','Prioritise and plan the control activities at all levels to implement the risk responses identified as necessary, including identification of costs, benefits and responsibility for execution. Obtain approval for recommended actions and acceptance of any residual risks, and ensure that committed actions are owned by the affected process owner(s). Monitor execution of the plans, and report on any deviations to senior management.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10431,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10431,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10433,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10433,10432,0,'PO10.1 Programme Management Framework','Maintain the programme of projects, related to the portfolio of IT-enabled investment programmes, by identifying, defining, evaluating, prioritising, selecting, initiating, managing and controlling projects. Ensure that the projects support the programme''s objectives. Co-ordinate the activities and interdependencies of multiple projects, manage the contribution of all the projects within the programme to expected outcomes, and resolve resource requirements and conflicts.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10433,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10433,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10434,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10434,10432,0,'PO10.2 Project Management Framework','Establish and maintain a project management framework that defines the scope and boundaries of managing projects, as well as the method to be adopted and applied to each project undertaken. The framework and supporting method should be integrated with the programme management processes.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10434,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10434,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10435,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10435,10432,0,'PO10.3 Project Management Approach','Establish a project management approach commensurate with the size, complexity and regulatory requirements of each project. The project governance structure can include the roles, responsibilities and accountabilities of the programme sponsor, project sponsors, steering committee, project office and project manager, and the mechanisms through which they can meet those responsibilities (such as reporting and stage reviews). Make sure all IT projects have sponsors with sufficient authority to own the execution of the project within the overall strategic programme.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10435,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10435,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10436,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10436,10432,0,'PO10.4 Stakeholder Commitment','Obtain commitment and participation from the affected stakeholders in the definition and execution of the project within the context of the overall IT-enabled investment programme.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10436,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10436,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10437,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10437,10432,0,'PO10.5 Project Scope Statement','Define and document the nature and scope of the project to confirm and develop amongst stakeholders a common understanding of project scope and how it relates to other projects within the overall IT-enabled investment programme. The definition should be formally approved by the programme and project sponsors before project initiation.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10437,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10437,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10438,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10438,10432,0,'PO10.6 Project Phase Initiation','Approve the initiation of each major project phase and communicate it to all stakeholders. Base the approval of the initial phase on programme governance decisions. Approval of subsequent phases should be based on review and acceptance of the deliverables of the previous phase, and approval of an updated business case at the next major review of the programme. In the event of overlapping project phases, an approval point should be established by programme and project sponsors to authorise project progression.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10438,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10438,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10439,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10439,10432,0,'PO10.7 Integrated Project Plan','Establish a formal, approved integrated project plan (covering business and information systems resources) to guide project execution and control throughout the life of the project. The activities and interdependencies of multiple projects within a programme should be understood and documented. The project plan should be maintained throughout the life of the project. The project plan, and changes to it, should be approved in line with the programme and project governance framework.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10439,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10439,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10440,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10440,10432,0,'PO10.8 Project Resources','Define the responsibilities, relationships, authorities and performance criteria of project team members, and specify the basis for acquiring and assigning competent staff members and/or contractors to the project. The procurement of products and services required for each project should be planned and managed to achieve project objectives using the organisation''s procurement practices.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10440,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10440,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10441,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10441,10432,0,'PO10.9 Project Risk Management','Eliminate or minimise specific risks associated with individual projects through a systematic process of planning, identifying, analysing, responding to, monitoring and controlling the areas or events that have the potential to cause unwanted change. Risks faced by the project management process and the project deliverable should be established and centrally recorded.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10441,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10441,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10442,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10442,10432,0,'PO10.10 Project Quality Plan','Prepare a quality management plan that describes the project quality system and how it will be implemented. The plan should be formally reviewed and agreed to by all parties concerned and then incorporated into the integrated project plan.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10442,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10442,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10443,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10443,10432,0,'PO10.11 Project Change Control','Establish a change control system for each project, so all changes to the project baseline (e.g., cost, schedule, scope, quality) are appropriately reviewed, approved and incorporated into the integrated project plan in line with the programme and project governance framework.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10443,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10443,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10444,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10444,10432,0,'PO10.12 Project Planning of Assurance Methods','Identify assurance tasks required to support the accreditation of new or modified systems during project planning, and include them in the integrated project plan. The tasks should provide assurance that internal controls and security features meet the defined requirements.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10444,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10444,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10445,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10445,10432,0,'PO10.13 Project Performance Measurement, Reporting and Monitoring','Measure project performance against key project performance scope, schedule, quality, cost and risk criteria. Identify any deviations from the plan. Assess the impact of deviations on the project and overall programme, and report results to key stakeholders. Recommend, implement and monitor remedial action, when required, in line with the programme and project governance framework.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10445,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10445,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10446,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10446,10432,0,'PO10.14 Project Closure','Require that, at the end of each project, the project stakeholders ascertain whether the project delivered the planned results and benefits. Identify and communicate any outstanding activities required to achieve the planned results of the project and the benefits of the programme, and identify and document lessons learned for use on future projects and programmes.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10446,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10446,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10449,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10449,10448,0,'AI1.1 Definition and Maintenance of Business Functional and Technical Requirements','Identify, prioritise, specify and agree on business functional and technical requirements covering the full scope of all initiatives required to achieve the expected outcomes of the IT-enabled investment programme.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10449,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10449,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10450,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10450,10448,0,'AI1.2 Risk Analysis Report','Identify, document and analyse risks associated with the business requirements and solution design as part of the organisation''s process for the development of requirements.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10450,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10450,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10451,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10451,10448,0,'AI1.3 Feasibility Study and Formulation of Alternative Courses of Action','Develop a feasibility study that examines the possibility of implementing the requirements. Business management, supported by the IT function, should assess the feasibility and alternative courses of action and make a recommendation to the business sponsor.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10451,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10451,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10452,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10452,10448,0,'AI1.4 Requirements and Feasibility Decision and Approval','Verify that the process requires the business sponsor to approve and sign off on business functional and technical requirements and feasibility study reports at predetermined key stages. The business sponsor should make the final decision with respect to the choice of solution and acquisition approach.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10452,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10452,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10454,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10454,10453,0,'AI2.1 High-level Design','Translate business requirements into a high-level design specification for software acquisition, taking into account the organisation''s technological direction and information architecture. Have the design specifications approved by management to ensure that the high-level design responds to the requirements. Reassess when significant technical or logical discrepancies occur during development or maintenance.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10454,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10454,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10455,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10455,10453,0,'AI2.2 Detailed Design','Prepare detailed design and technical software application requirements. Define the criteria for acceptance of the requirements. Have the requirements approved to ensure that they correspond to the high-level design. Perform reassessment when significant technical or logical discrepancies occur during development or maintenance.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10455,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10455,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10456,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10456,10453,0,'AI2.3 Application Control and Auditability','Implement business controls, where appropriate, into automated application controls such that processing is accurate, complete, timely, authorised and auditable.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10456,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10456,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10457,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10457,10453,0,'AI2.4 Application Security and Availability','Address application security and availability requirements in response to identified risks and in line with the organisation''s data classification, information architecture, information security architecture and risk tolerance.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10457,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10457,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10458,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10458,10453,0,'AI2.5 Configuration and Implementation of Acquired Application Software','Configure and implement acquired application software to meet business objectives.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10458,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10458,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10459,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10459,10453,0,'AI2.6 Major Upgrades to Existing Systems','In the event of major changes to existing systems that result in significant change in current designs and/or functionality, follow a similar development process as that used for the development of new systems.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10459,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10459,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10460,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10460,10453,0,'AI2.7 Development of Application Software','Ensure that automated functionality is developed in accordance with design specifications, development and documentation standards, QA requirements, and approval standards. Ensure that all legal and contractual aspects are identified and addressed for application software developed by third parties.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10460,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10460,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10461,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10461,10453,0,'AI2.8 Software Quality Assurance','Develop, resource and execute a software QA plan to obtain the quality specified in the requirements definition and the organisation''s quality policies and procedures.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10461,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10461,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10462,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10462,10453,0,'AI2.9 Applications Requirements Management','Track the status of individual requirements (including all rejected requirements) during the design, development and implementation, and approve changes to requirements through an established change management process.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10462,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10462,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10463,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10463,10453,0,'AI2.10 Application Software Maintenance','Develop a strategy and plan for the maintenance of software applications.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10463,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10463,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10465,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10465,10464,0,'AI3.1 Technological Infrastructure Acquisition Plan','Produce a plan for the acquisition, implementation and maintenance of the technological infrastructure that meets established business functional and technical requirements and is in accord with the organisation''s technology direction.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10465,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10465,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10466,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10466,10464,0,'AI3.2 Infrastructure Resource Protection and Availability','rastructural software to protect resources and ensure availability and integrity. Responsibilities for using sensitive infrastructure components should be clearly defined and understood by those who develop and integrate infrastructure components. Their use should be monitored and evaluated.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10466,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10466,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10467,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10467,10464,0,'AI3.3 Infrastructure Maintenance','Develop a strategy and plan for infrastructure maintenance, and ensure that changes are controlled in line with the organisation''s change management procedure. Include periodic reviews against business needs, patch management, upgrade strategies, risks, vulnerabilities assessment and security requirements.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10467,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10467,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10468,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10468,10464,0,'AI3.4 Feasibility Test Environment','Establish development and test environments to support effective and efficient feasibility and integration testing of infrastructure components.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10468,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10468,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10470,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10470,10469,0,'AI4.1 Planning for Operational Solutions','Develop a plan to identify and document all technical, operational and usage aspects such that all those who will operate, use and maintain the automated solutions can exercise their responsibility.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10470,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10470,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10471,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10471,10469,0,'AI4.2 Knowledge Transfer to Business Management','Transfer knowledge to business management to allow those individuals to take ownership of the system and data, and exercise responsibility for service delivery and quality, internal control, and application administration.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10471,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10471,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10472,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10472,10469,0,'AI4.3 Knowledge Transfer to End Users','Transfer knowledge and skills to allow end users to effectively and efficiently use the system in support of business processes.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10472,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10472,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10473,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10473,10469,0,'AI4.4 Knowledge Transfer to Operations and Support Staff','Transfer knowledge and skills to enable operations and technical support staff to effectively and efficiently deliver, support and maintain the system and associated infrastructure.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10473,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10473,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10475,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10475,10474,0,'AI5.1 Procurement Control','Develop and follow a set of procedures and standards that is consistent with the business organisation''s overall procurement process and acquisition strategy to acquire IT-related infrastructure, facilities, hardware, software and services needed by the business.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10475,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10475,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10476,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10476,10474,0,'AI5.2 Supplier Contract Management','Set up a procedure for establishing, modifying and terminating contracts for all suppliers. The procedure should cover, at a minimum, legal, financial, organisational, documentary, performance, security, intellectual property, and termination responsibilities and liabilities (including penalty clauses). All contracts and contract changes should be reviewed by legal advisors.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10476,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10476,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10477,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10477,10474,0,'AI5.3 Supplier Selection','Select suppliers according to a fair and formal practice to ensure a viable best fit based on specified requirements. Requirements should be optimised with input from potential suppliers.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10477,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10477,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10478,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10478,10474,0,'AI5.4 IT Resources Acquisition','Protect and enforce the organisation''s interests in all acquisition contractual agreements, including the rights and obligations of all parties in the contractual terms for the acquisition of software, development resources, infrastructure and services.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10478,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10478,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10480,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10480,10479,0,'AI6.1 Change Standards and Procedures','Set up formal change management procedures to handle in a standardised manner all requests (including maintenance and patches) for changes to applications, procedures, processes, system and service parameters, and the underlying platforms.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10480,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10480,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10481,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10481,10479,0,'AI6.2 Impact Assessment, Prioritisation and Authorisation','Assess all requests for change in a structured way to determine the impact on the operational system and its functionality. Ensure that changes are categorised, prioritised and authorised.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10481,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10481,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10482,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10482,10479,0,'AI6.3 Emergency Changes','Establish a process for defining, raising, testing, documenting, assessing and authorising emergency changes that do not follow the established change process.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10482,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10482,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10483,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10483,10479,0,'AI6.4 Change Status Tracking and Reporting','Establish a tracking and reporting system to document rejected changes, communicate the status of approved and in-process changes, and complete changes. Make certain that approved changes are implemented as planned.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10483,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10483,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10484,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10484,10479,0,'AI6.5 Change Closure and Documentation','Whenever changes are implemented, update the associated system and user documentation and procedures accordingly.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10484,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10484,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10486,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10486,10485,0,'AI7.1 Training','Train the staff members of the affected user departments and the operations group of the IT function in accordance with the defined training and implementation plan and associated materials, as part of every information systems development, implementation or modification project.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10486,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10486,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10487,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10487,10485,0,'AI7.2 Test Plan','Establish a test plan based on organisationwide standards that defines roles, responsibilities, and entry and exit criteria. Ensure that the plan is approved by relevant parties.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10487,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10487,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10488,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10488,10485,0,'AI7.3 Implementation Plan','Establish an implementation and fallback/backout plan. Obtain approval from relevant parties.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10488,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10488,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10489,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10489,10485,0,'AI7.4 Test Environment','Define and establish a secure test environment representative of the planned operations environment relative to security, internal controls, operational practices, data quality and privacy requirements, and workloads.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10489,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10489,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10490,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10490,10485,0,'AI7.5 System and Data Conversion','Plan data conversion and infrastructure migration as part of the organisation''s development methods, including audit trails, rollbacks and fallbacks.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10490,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10490,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10491,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10491,10485,0,'AI7.6 Testing of Changes','Test changes independently in accordance with the defined test plan prior to migration to the operational environment. Ensure that the plan considers security and performance.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10491,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10491,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10492,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10492,10485,0,'AI7.7 Final Acceptance Test','Ensure that business process owners and IT stakeholders evaluate the outcome of the testing process as determined by the test plan. Remediate significant errors identified in the testing process, having completed the suite of tests identified in the test plan and any necessary regression tests. Following evaluation, approve promotion to production.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10492,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10492,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10493,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10493,10485,0,'AI7.8 Promotion to Production','Following testing, control the handover of the changed system to operations, keeping it in line with the implementation plan. Obtain approval of the key stakeholders, such as users, system owner and operational management. Where appropriate, run the system in parallel with the old system for a while, and compare behaviour and results.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10493,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10493,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10494,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10494,10485,0,'AI7.9 Post-implementation Review','Establish procedures in line with the organisational change management standards to require a post-implementation review as set out in the implementation plan.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10494,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10494,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10497,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10497,10496,0,'DS1.1 Service Level Management Framework','Define a framework that provides a formalised service level management process between the customer and service provider. The framework should maintain continuous alignment with business requirements and priorities and facilitate common understanding between the customer and provider(s). The framework should include processes for creating service requirements, service definitions, SLAs, OLAs and funding sources. These attributes should be organised in a service catalogue. The framework should define the organisational structure for service level management, covering the roles, tasks and responsibilities of internal and external service providers and customers.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10497,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10497,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10498,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10498,10496,0,'DS1.2 Definition of Services','Base definitions of IT services on service characteristics and business requirements. Ensure that they are organised and stored centrally via the implementation of a service catalogue portfolio approach.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10498,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10498,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10499,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10499,10496,0,'DS1.3 Service Level Agreements','Define and agree to SLAs for all critical IT services based on customer requirements and IT capabilities. This should cover customer commitments; service support requirements; quantitative and qualitative metrics for measuring the service signed off on by the stakeholders; funding and commercial arrangements, if applicable; and roles and responsibilities, including oversight of the SLA. Consider items such as availability, reliability, performance, capacity for growth, levels of support, continuity planning, security and demand constraints.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10499,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10499,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10500,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10500,10496,0,'DS1.4 Operating Level Agreements','Define OLAs that explain how the services will be technically delivered to support the SLA(s) in an optimal manner. The OLAs should specify the technical processes in terms meaningful to the provider and may support several SLAs.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10500,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10500,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10501,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10501,10496,0,'DS1.5 Monitoring and Reporting of Service Level Achievements','Continuously monitor specified service level performance criteria. Reports on achievement of service levels should be provided in a format that is meaningful to the stakeholders. The monitoring statistics should be analysed and acted upon to identify negative and positive trends for individual services as well as for services overall.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10501,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10501,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10502,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10502,10496,0,'DS1.6 Review of Service Level Agreements and Contracts','Regularly review SLAs and underpinning contracts (UCs) with internal and external service providers to ensure that they are effective and up to date and that changes in requirements have been taken into account.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10502,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10502,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10504,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10504,10503,0,'DS2.1 Identification of All Supplier Relationships','Identify all supplier services, and categorise them according to supplier type, significance and criticality. Maintain formal documentation of technical and organisational relationships covering the roles and responsibilities, goals, expected deliverables, and credentials of representatives of these suppliers.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10504,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10504,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10505,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10505,10503,0,'DS2.2 Supplier Relationship Management','Formalise the supplier relationship management process for each supplier. The relationship owners should liaise on customer and supplier issues and ensure the quality of the relationship based on trust and transparency (e.g., through SLAs).','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10505,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10505,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10506,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10506,10503,0,'DS2.3 Supplier Risk Management','Identify and mitigate risks relating to suppliers'' ability to continue effective service delivery in a secure and efficient manner on a continual basis. Ensure that contracts conform to universal business standards in accordance with legal and regulatory requirements. Risk management should further consider non-disclosure agreements (NDAs), escrow contracts, continued supplier viability, conformance with security requirements, alternative suppliers, penalties and rewards, etc.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10506,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10506,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10507,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10507,10503,0,'DS2.4 Supplier Performance Monitoring','Establish a process to monitor service delivery to ensure that the supplier is meeting current business requirements and continuing to adhere to the contract agreements and SLAs, and that performance is competitive with alternative suppliers and market conditions.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10507,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10507,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10509,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10509,10508,0,'DS3.1 Performance and Capacity Planning','Establish a planning process for the review of performance and capacity of IT resources to ensure that cost-justifiable capacity and performance are available to process the agreed-upon workloads as determined by the SLAs. Capacity and performance plans should leverage appropriate modelling techniques to produce a model of the current and forecasted performance, capacity and throughput of the IT resources.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10509,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10509,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10510,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10510,10508,0,'DS3.2 Current Performance and Capacity','Assess current performance and capacity of IT resources to determine if sufficient capacity and performance exist to deliver against agreed-upon service levels.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10510,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10510,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10511,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10511,10508,0,'DS3.3 Future Performance and Capacity','Conduct performance and capacity forecasting of IT resources at regular intervals to minimise the risk of service disruptions due to insufficient capacity or performance degradation, and identify excess capacity for possible redeployment. Identify workload trends and determine forecasts to be input to performance and capacity plans.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10511,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10511,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10512,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10512,10508,0,'DS3.4 IT Resources Availability','Provide the required capacity and performance, taking into account aspects such as normal workloads, contingencies, storage requirements and IT resource life cycles. Provisions such as prioritising tasks, fault-tolerance mechanisms and resource allocation practices should be made. Management should ensure that contingency plans properly address availability, capacity and performance of individual IT resources.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10512,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10512,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10513,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10513,10508,0,'DS3.5 Monitoring and Reporting','Continuously monitor the performance and capacity of IT resources. Data gathered should serve two purposes: - To maintain and tune current performance within IT and address such issues as resilience, contingency, current and  projected workloads, storage plans, and resource acquisition - To report delivered service availability to the business, as required by the SLAs Accompany all exception reports with recommendations for corrective action.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10513,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10513,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10515,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10515,10514,0,'DS4.1 IT Continuity Framework','Develop a framework for IT continuity to support enterprisewide business continuity management using a consistent process. The objective of the framework should be to assist in determining the required resilience of the infrastructure and to drive the development of disaster recovery and IT contingency plans. The framework should address the organisational structure for continuity management, covering the roles, tasks and responsibilities of internal and external service providers, their management and their customers, and the planning processes that create the rules and structures to document, test and execute the disaster recovery and IT contingency plans. The plan should also address items such as the identification of critical resources, noting key dependencies, the monitoring and reporting of the availability of critical resources, alternative processing, and the principles of backup and recovery.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10515,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10515,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10516,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10516,10514,0,'DS4.2 IT Continuity Plans','Develop IT continuity plans based on the framework and designed to reduce the impact of a major disruption on key business functions and processes. The plans should be based on risk understanding of potential business impacts and address requirements for resilience, alternative processing and recovery capability of all critical IT services. They should also cover usage guidelines, roles and responsibilities, procedures, communication processes, and the testing approach.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10516,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10516,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10517,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10517,10514,0,'DS4.3 Critical IT Resources','Focus attention on items specified as most critical in the IT continuity plan to build in resilience and establish priorities in recovery situations. Avoid the distraction of recovering less-critical items and ensure response and recovery in line with prioritised business needs, while ensuring that costs are kept at an acceptable level and complying with regulatory and contractual requirements. Consider resilience, response and recovery requirements for different tiers, e.g., one to four hours, four to 24 hours, more than 24 hours and critical business operational periods.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10517,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10517,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10518,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10518,10514,0,'DS4.4 Maintenance of the IT Continuity Plan','Encourage IT management to define and execute change control procedures to ensure that the IT continuity plan is kept up to date and continually reflects actual business requirements. Communicate changes in procedures and responsibilities clearly and in a timely manner.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10518,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10518,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10519,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10519,10514,0,'DS4.5 Testing of the IT Continuity Plan','Test the IT continuity plan on a regular basis to ensure that IT systems can be effectively recovered, shortcomings are addressed and the plan remains relevant. This requires careful preparation, documentation, reporting of test results and, according to the results, implementation of an action plan. Consider the extent of testing recovery of single applications to integrated testing scenarios to end to-end testing and integrated vendor testing.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10519,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10519,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10520,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10520,10514,0,'DS4.6 IT Continuity Plan Training','Provide all concerned parties with regular training sessions regarding the procedures and their roles and responsibilities in case of na incident or disaster. Verify and enhance training according to the results of the contingency tests.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10520,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10520,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10521,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10521,10514,0,'DS4.7 Distribution of the IT Continuity Plan','Determine that a defined and managed distribution strategy exists to ensure that plans are properly and securely distributed and available to appropriately authorised interested parties when and where needed. Attention should be paid to making the plans accessible under all disaster scenarios.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10521,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10521,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10522,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10522,10514,0,'DS4.8 IT Services Recovery and Resumption','Plan the actions to be taken for the period when IT is recovering and resuming services. This may include activation of backup sites, initiation of alternative processing, customer and stakeholder communication, and resumption procedures. Ensure that the business understands IT recovery times and the necessary technology investments to support business recovery and resumption needs.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10522,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10522,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10523,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10523,10514,0,'DS4.9 Offsite Backup Storage','Store offsite all critical backup media, documentation and other IT resources necessary for IT recovery and business continuity plans. Determine the content of backup storage in collaboration between business process owners and IT personnel. Management of the offsite storage facility should respond to the data classification policy and the enterprise''s media storage practices. IT management should ensure that offsite arrangements are periodically assessed, at least annually, for content, environmental protection and security. Ensure compatibility of hardware and software to restore archived data, and periodically test and refresh archived data.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10523,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10523,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10524,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10524,10514,0,'DS4.10 Post-resumption Review','Determine whether IT management has established procedures for assessing the adequacy of the plan in regard to the successful resumption of the IT function after a disaster, and update the plan accordingly.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10524,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10524,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10526,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10526,10525,0,'DS5.1 Management of IT Security','Manage IT security at the highest appropriate organisational level, so the management of security actions is in line with business requirements.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10526,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10526,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10527,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10527,10525,0,'DS5.2 IT Security Plan','Translate business, risk and compliance requirements into an overall IT security plan, taking into consideration the IT infrastructure and the security culture. Ensure that the plan is implemented in security policies and procedures together with appropriate investments in services, personnel, software and hardware. Communicate security policies and procedures to stakeholders and users.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10527,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10527,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10528,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10528,10525,0,'DS5.3 Identity Management','Ensure that all users (internal, external and temporary) and their activity on IT systems (business application, IT environment, system operations, development and maintenance) are uniquely identifiable. Enable user identities via authentication mechanisms. Confirm that user access rights to systems and data are in line with defined and documented business needs and that job requirements are attached to user identities. Ensure that user access rights are requested by user management, approved by system owners and implemented by the security-responsible person. Maintain user identities and access rights in a central repository. Deploy cost-effective technical and procedural measures, and keep them current to establish user identification, implement authentication and enforce access rights.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10528,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10528,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10529,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10529,10525,0,'DS5.4 User Account Management','Address requesting, establishing, issuing, suspending, modifying and closing user accounts and related user privileges with a set of user account management procedures. Include an approval procedure outlining the data or system owner granting the access privileges. These procedures should apply for all users, including administrators (privileged users) and internal and external users, for normal and emergency cases. Rights and obligations relative to access to enterprise systems and information should be contractually arranged for all types of users. Perform regular management review of all accounts and related privileges.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10529,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10529,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10530,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10530,10525,0,'DS5.5 Security Testing, Surveillance and Monitoring','Test and monitor the IT security implementation in a proactive way. IT security should be reaccredited in a timely manner to ensure that the approved enterprise''s information security baseline is maintained. A logging and monitoring function will enable the early prevention and/or detection and subsequent timely reporting of unusual and/or abnormal activities that may need to be addressed.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10530,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10530,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10531,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10531,10525,0,'DS5.6 Security Incident Definition','Clearly define and communicate the characteristics of potential security incidents so they can be properly classified and treated by the incident and problem management process.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10531,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10531,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10532,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10532,10525,0,'DS5.7 Protection of Security Technology','Make security-related technology resistant to tampering, and do not disclose security documentation unnecessarily.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10532,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10532,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10533,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10533,10525,0,'DS5.8 Cryptographic Key Management','Determine that policies and procedures are in place to organise the generation, change, revocation, destruction, distribution, certification, storage, entry, use and archiving of cryptographic keys to ensure the protection of keys against modification and unauthorised disclosure.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10533,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10533,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10534,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10534,10525,0,'DS5.9 Malicious Software Prevention, Detection and Correction','Put preventive, detective and corrective measures in place (especially up-to-date security patches and virus control) across the organisation to protect information systems and technology from malware (e.g., viruses, worms, spyware, spam).','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10534,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10534,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10535,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10535,10525,0,'DS5.10 Network Security','Use security techniques and related management procedures (e.g., firewalls, security appliances, network segmentation, intrusion detection) to authorise access and control information flows from and to networks.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10535,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10535,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10536,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10536,10525,0,'DS5.11 Exchange of Sensitive Data','Exchange sensitive transaction data only over a trusted path or medium with controls to provide authenticity of content, proof of submission, proof of receipt and non-repudiation of origin.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10536,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10536,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10538,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10538,10537,0,'DS6.1 Definition of Services','Identify all IT costs, and map them to IT services to support a transparent cost model. IT services should be linked to business processes such that the business can identify associated service billing levels.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10538,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10538,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10539,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10539,10537,0,'DS6.2 IT Accounting','Capture and allocate actual costs according to the enterprise cost model. Variances between forecasts and actual costs should be analysed and reported on, in compliance with the enterprise''s financial measurement systems.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10539,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10539,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10540,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10540,10537,0,'DS6.3 Cost Modelling and Charging','Establish and use an IT costing model based on the service definitions that support the calculation of chargeback rates per service. The IT cost model should ensure that charging for services is identifiable, measurable and predictable by users to encourage proper use of resources.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10540,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10540,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10541,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10541,10537,0,'DS6.4 Cost Model Maintenance','Regularly review and benchmark the appropriateness of the cost/recharge model to maintain its relevance and appropriateness to the evolving business and IT activities.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10541,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10541,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10543,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10543,10542,0,'DS7.1 Identification of Education and Training Needs','Establish and regularly update a curriculum for each target group of employees considering:  - Current and future business needs and strategy  - Value of information as an asset  - Corporate values (ethical values, control and security culture, etc.)  - Implementation of new IT infrastructure and software (i.e., packages, applications)  - Current and future skills, competence profiles, and certification and/or credentialing needs as well as required reaccreditation  - Delivery methods (e.g., classroom, web-based), target group size, accessibility and timing','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10543,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10543,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10544,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10544,10542,0,'DS7.2 Delivery of Training and Education','Based on the identified education and training needs, identify target groups and their members, efficient delivery mechanisms, teachers, trainers, and mentors. Appoint trainers and organise timely training sessions. Record registration (including prerequisites), attendance and training session performance evaluations.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10544,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10544,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10545,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10545,10542,0,'DS7.3 Evaluation of Training Received','Evaluate education and training content delivery upon completion for relevance, quality, effectiveness, the retention of knowledge, cost and value. The results of this evaluation should serve as input for future curriculum definition and the delivery of training sessions.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10545,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10545,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10547,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10547,10546,0,'DS8.1 Service Desk','Establish a service desk function, which is the user interface with IT, to register, communicate, dispatch and analyse all calls, reported incidents, service requests and information demands. There should be monitoring and escalation procedures based on agreed-upon service levels relative to the appropriate SLA that allow classification and prioritisation of any reported issue as an incident, service request or information request. Measure end users'' satisfaction with the quality of the service desk and IT services.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10547,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10547,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10548,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10548,10546,0,'DS8.2 Registration of Customer Queries','Establish a function and system to allow logging and tracking of calls, incidents, service requests and information needs. It should work closely with such processes as incident management, problem management, change management, capacity management and availability management. Incidents should be classified according to a business and service priority and routed to the appropriate problem management team, where necessary. Customers should be kept informed of the status of their queries.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10548,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10548,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10549,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10549,10546,0,'DS8.3 Incident Escalation','Establish service desk procedures, so incidents that cannot be resolved immediately are appropriately escalated according to limits defined in the SLA and, if appropriate, workarounds are provided. Ensure that incident ownership and life cycle monitoring remain with the service desk for user-based incidents, regardless which IT group is working on resolution activities.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10549,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10549,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10550,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10550,10546,0,'DS8.4 Incident Closure','Establish procedures for the timely monitoring of clearance of customer queries. When the incident has been resolved, ensure that the service desk records the resolution steps, and confirm that the action taken has been agreed to by the customer. Also record and report unresolved incidents (known errors and workarounds) to provide information for proper problem management.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10550,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10550,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10551,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10551,10546,0,'DS8.5 Reporting and Trend Analysis','Produce reports of service desk activity to enable management to measure service performance and service response times and to identify trends or recurring problems, so service can be continually improved.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10551,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10551,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10553,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10553,10552,0,'DS9.1 Configuration Repository and Baseline','Establish a supporting tool and a central repository to contain all relevant information on configuration items. Monitor and record all assets and changes to assets. Maintain a baseline of configuration items for every system and service as a checkpoint to which to return after changes.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10553,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10553,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10554,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10554,10552,0,'DS9.2 Identification and Maintenance of Configuration Items','Establish configuration procedures to support management and logging of all changes to the configuration repository. Integrate these procedures with change management, incident management and problem management procedures.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10554,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10554,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10555,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10555,10552,0,'DS9.3 Configuration Integrity Review','Periodically review the configuration data to verify and confirm the integrity of the current and historical configuration. Periodically review installed software against the policy for software usage to identify personal or unlicensed software or any software instances in excess of current license agreements. Report, act on and correct errors and deviations.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10555,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10555,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10557,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10557,10556,0,'DS10.1 Identification and Classification of Problems','Implement processes to report and classify problems that have been identified as part of incident management. The steps involved in problem classification are similar to the steps in classifying incidents; they are to determine category, impact, urgency and priority. Categorise problems as appropriate into related groups or domains (e.g., hardware, software, support software). These groups may match the organisational responsibilities of the user and customer base, and should be the basis for allocating problems to support staff.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10557,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10557,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10558,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10558,10556,0,'DS10.2 Problem Tracking and Resolution','Ensure that the problem management system provides for adequate audit trail facilities that allow tracking, analysing and determining the root cause of all reported problems considering:  - All associated configuration items  - Outstanding problems and incidents  - Known and suspected errors  - Tracking of problem trends  Identify and initiate sustainable solutions addressing the root cause, raising change requests via the established change management process. Throughout the resolution process, problem management should obtain regular reports from change management on progress in resolving problems and errors. Problem management should monitor the continuing impact of problems and known errors on user services. In the event that this impact becomes severe, problem management should escalate the problem, perhaps referring it to an appropriate board to increase the priority of the (RFC or to implement an urgent change as appropriate. Monitor the progress of problem resolution against SLAs.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10558,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10558,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10559,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10559,10556,0,'DS10.3 Problem Closure','Put in place a procedure to close problem records either after confirmation of successful elimination of the known error or after agreement with the business on how to alternatively handle the problem.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10559,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10559,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10560,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10560,10556,0,'DS10.4 Integration of Configuration, Incident and Problem Management','Integrate the related processes of configuration, incident and problem management to ensure effective management of problems and enable improvements.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10560,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10560,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10562,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10562,10561,0,'DS11.1 Business Requirements for Data Management','Verify that all data expected for processing are received and processed completely, accurately and in a timely manner, and all output is delivered in accordance with business requirements. Support restart and reprocessing needs.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10562,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10562,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10563,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10563,10561,0,'DS11.2 Storage and Retention Arrangements','Define and implement procedures for effective and efficient data storage, retention and archiving to meet business objectives, the organisation''s security policy and regulatory requirements.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10563,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10563,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10564,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10564,10561,0,'DS11.3 Media Library Management System','Define and implement procedures to maintain na inventory of stored and archived media to ensure their usability and integrity.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10564,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10564,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10565,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10565,10561,0,'DS11.4 Disposal','Define and implement procedures to ensure that business requirements for protection of sensitive data and software are met when data and hardware are disposed or transferred.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10565,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10565,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10566,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10566,10561,0,'DS11.5 Backup and Restoration','Define and implement procedures for backup and restoration of systems, applications, data and documentation in line with business requirements and the continuity plan.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10566,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10566,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10567,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10567,10561,0,'DS11.6 Security Requirements for Data Management','Define and implement policies and procedures to identify and apply security requirements applicable to the receipt, processing, storage and output of data to meet business objectives, the organisation''s security policy and regulatory requirements.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10567,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10567,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10569,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10569,10568,0,'DS12.1 Site Selection and Layout','Define and select the physical sites for IT equipment to support the technology strategy linked to the business strategy. The selection and design of the layout of a site should take into account the risk associated with natural and man-made disasters, whilst considering relevant laws and regulations, such as occupational health and safety regulations.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10569,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10569,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10570,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10570,10568,0,'DS12.2 Physical Security Measures','Define and implement physical security measures in line with business requirements to secure the location and the physical assets. Physical security measures must be capable of effectively preventing, detecting and mitigating risks relating to theft, temperature, fire, smoke, water, vibration, terror, vandalism, power outages, chemicals or explosives.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10570,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10570,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10571,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10571,10568,0,'DS12.3 Physical Access','Define and implement procedures to grant, limit and revoke access to premises, buildings and areas according to business needs, including emergencies. Access to premises, buildings and areas should be justified, authorised, logged and monitored. This should apply to all persons entering the premises, including staff, temporary staff, clients, vendors, visitors or any other third party.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10571,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10571,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10572,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10572,10568,0,'DS12.4 Protection Against Environmental Factors','Design and implement measures for protection against environmental factors. Install specialised equipment and devices to monitor and control the environment.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10572,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10572,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10573,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10573,10568,0,'DS12.5 Physical Facilities Management','Manage facilities, including power and communications equipment, in line with laws and regulations, technical and business requirements, vendor specifications, and health and safety guidelines.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10573,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10573,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10575,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10575,10574,0,'DS13.1 Operations Procedures and Instructions','Define, implement and maintain procedures for IT operations, ensuring that the operations staff members are familiar with all operations tasks relevant to them. Operational procedures should cover shift handover (formal handover of activity, status updates, operational problems, escalation procedures and reports on current responsibilities) to support agreed-upon service levels and ensure continuous operations.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10575,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10575,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10576,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10576,10574,0,'DS13.2 Job Scheduling','Organise the scheduling of jobs, processes and tasks into the most efficient sequence, maximising throughput and utilisation to meet business requirements.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10576,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10576,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10577,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10577,10574,0,'DS13.3 IT Infrastructure Monitoring','Define and implement procedures to monitor the IT infrastructure and related events. Ensure that sufficient chronological information is being stored in operations logs to enable the reconstruction, review and examination of the time sequences of operations and the other activities surrounding or supporting operations.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10577,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10577,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10578,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10578,10574,0,'DS13.4 Sensitive Documents and Output Devices','Establish appropriate physical safeguards, accounting practices and inventory management over sensitive IT assets, such as special forms, negotiable instruments, special purpose printers or security tokens.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10578,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10578,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10579,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10579,10574,0,'DS13.5 Preventive Maintenance for Hardware','Define and implement procedures to ensure timely maintenance of infrastructure to reduce the frequency and impact of failures or performance degradation.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10579,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10579,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10582,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10582,10581,0,'ME1.1 Monitoring Approach','Establish a general monitoring framework and approach to define the scope, methodology and process to be followed for measuring IT''s solution and service delivery, and monitor IT''s contribution to the business. Integrate the framework with the corporate performance management system.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10582,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10582,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10583,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10583,10581,0,'ME1.2 Definition and Collection of Monitoring Data','Work with the business to define a balanced set of performance targets and have them approved by the business and other relevant stakeholders. Define benchmarks with which to compare the targets, and identify available data to be collected to measure the targets. Establish processes to collect timely and accurate data to report on progress against targets.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10583,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10583,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10584,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10584,10581,0,'ME1.3 Monitoring Method','Deploy a performance monitoring method (e.g., balanced scorecard) that records targets; captures measurements; provides a succinct, all-around view of IT performance; and fits within the enterprise monitoring system.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10584,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10584,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10585,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10585,10581,0,'ME1.4 Performance Assessment','Periodically review performance against targets, analyse the cause of any deviations, and initiate remedial action to address the underlying causes. At appropriate times, perform root cause analysis across deviations.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10585,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10585,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10586,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10586,10581,0,'ME1.5 Board and Executive Reporting','Develop senior management reports on IT''s contribution to the business, specifically in terms of the performance of the enterprise''s portfolio, IT-enabled investment programmes, and the solution and service deliverable performance of individual programmes. Include in status reports the extent to which planned objectives have been achieved, budgeted resources used,  set performance targets met and identified risks mitigated. Anticipate senior management''s review by suggesting remedial actions for major deviations. Provide the report to senior management, and solicit feedback from management''s review.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10586,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10586,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10587,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10587,10581,0,'ME1.6 Remedial Actions','Identify and initiate remedial actions based on performance monitoring, assessment and reporting. This includes follow-up of all monitoring, reporting and assessments through: - Review, negotiation and establishment of management responses - Assignment of responsibility for remediation - Tracking of the results of actions committed','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10587,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10587,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10589,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10589,10588,0,'ME2.1 Monitoring of Internal Control Framework','Continuously monitor, benchmark and improve the IT control environment and control framework to meet organisational objectives.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10589,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10589,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10590,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10590,10588,0,'ME2.2 Supervisory Review','Monitor and evaluate the efficiency and effectiveness of internal IT managerial review controls.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10590,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10590,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10591,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10591,10588,0,'ME2.3 Control Exceptions','Identify control exceptions, and analyse and identify their underlying root causes. Escalate control exceptions and report to stakeholders appropriately. Institute necessary corrective action.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10591,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10591,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10592,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10592,10588,0,'ME2.4 Control Self-assessment','Evaluate the completeness and effectiveness of management''s control over IT processes, policies and contracts through a continuing programme of self-assessment.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10592,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10592,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10593,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10593,10588,0,'ME2.5 Assurance of Internal Control','Obtain, as needed, further assurance of the completeness and effectiveness of internal controls through third-party reviews.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10593,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10593,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10594,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10594,10588,0,'ME2.6 Internal Control at Third Parties','Assess the status of external service providers'' internal controls. Confirm that external service providers comply with legal and regulatory requirements and contractual obligations.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10594,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10594,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10595,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10595,10588,0,'ME2.7 Remedial Actions','Identify, initiate, track and implement remedial actions arising from control assessments and reporting.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10595,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10595,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10597,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10597,10596,0,'ME3.1 Identification of External Legal, Regulatory and Contractual Compliance Requirements','Identify, on a continuous basis, local and international laws, regulations, and other external requirements that must be complied with for incorporation into the organisation''s IT policies, standards, procedures and methodologies.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10597,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10597,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10598,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10598,10596,0,'ME3.2 Optimisation of Response to External Requirements','Review and adjust IT policies, standards, procedures and methodologies to ensure that legal, regulatory and contractual requirements are addressed and communicated.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10598,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10598,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10599,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10599,10596,0,'ME3.3 Evaluation of Compliance With External Requirements','Confirm compliance of IT policies, standards, procedures and methodologies with legal and regulatory requirements.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10599,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10599,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10600,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10600,10596,0,'ME3.4 Positive Assurance of Compliance','Obtain and report assurance of compliance and adherence to all internal policies derived from internal directives or external legal, regulatory or contractual requirements, confirming that any corrective actions to address any compliance gaps have been taken by the responsible process owner in a timely manner.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10600,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10600,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10601,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10601,10596,0,'ME3.5 Integrated Reporting','Integrate IT reporting on legal, regulatory and contractual requirements with similar output from other business functions.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10601,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10601,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10603,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10603,10602,0,'ME4.1 Establishment of an IT Governance Framework','Define, establish and align the IT governance framework with the overall enterprise governance and control environment. Base the framework on a suitable IT process and control model and provide for unambiguous accountability and practices to avoid a breakdown in internal control and oversight. Confirm that the IT governance framework ensures compliance with laws and regulations and is aligned with, and confirms delivery of, the enterprise''s strategies and objectives. Report IT governance status and issues.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10603,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10603,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10604,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10604,10602,0,'ME4.2 Strategic Alignment','Enable board and executive understanding of strategic IT issues, such as the role of IT, technology insights and capabilities. Ensure that there is a shared understanding between the business and IT regarding the potential contribution of IT to the business strategy. Work with the board and the established governance bodies, such as an IT strategy committee, to provide strategic direction to management relative to IT, ensuring that the strategy and objectives are cascaded into business units and IT functions, and that confidence and trust are developed between the business and IT. Enable the alignment of IT to the business in strategy and operations, encouraging co-responsibility between the business and IT for making strategic decisions and obtaining benefits from IT-enabled investments.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10604,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10604,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10605,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10605,10602,0,'ME4.3 Value Delivery','Manage IT-enabled investment programmes and other IT assets and services to ensure that they deliver the greatest possible value in supporting the enterprise''s strategy and objectives. Ensure that the expected business outcomes of IT-enabled investments and the full scope of effort required to achieve those outcomes are understood; that comprehensive and consistent business cases are created and approved by stakeholders; that assets and investments are managed throughout their economic life cycle; and that there is active management of the realisation of benefits, such as contribution to new services, efficiency gains and improved responsiveness to customer demands. Enforce a disciplined approach to portfolio, programme and project management, insisting that the business takes ownership of all IT-enabled investments and IT ensures optimisation of the costs of delivering IT capabilities and services.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10605,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10605,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10606,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10606,10602,0,'ME4.4 Resource Management','Oversee the investment, use and allocation of IT resources through regular assessments of IT initiatives and operations to ensure appropriate resourcing and alignment with current and future strategic objectives and business imperatives.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10606,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10606,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10607,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10607,10602,0,'ME4.5 Risk Management','Work with the board to define the enterprise''s appetite for IT risk, and obtain reasonable assurance that IT risk management practices are appropriate to ensure that the actual IT risk does not exceed the board''s risk appetite. Embed risk management responsibilities into the organisation, ensuring that the business and IT regularly assess and report IT-related risks and their impact and that the enterprise''s IT risk position is transparent to all stakeholders.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10607,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10607,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10608,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10608,10602,0,'ME4.6 Performance Measurement','Confirm that agreed-upon IT objectives have been met or exceeded, or that progress toward IT goals meets expectations. Where agreed-upon objectives have been missed or progress is not as expected, review management''s remedial action. Report to the board relevant portfolios, programme and IT performance, supported by reports to enable senior management to review the enterprise''s progress toward identified goals.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10608,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10608,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10609,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10609,10602,0,'ME4.7 Independent Assurance','Obtain independent assurance (internal or external) about the conformance of IT with relevant laws and regulations; the organisation''s policies, standards and procedures;generally accepted practices; and the effective and efficient performance of IT.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10609,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10609,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10612,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10612,10611,1,'Existence of job description documents.','Management specifies the level of competence needed for particular jobs and translates the desired levels of competence into requisite knowledge and skills.','Select a sample of 25 employees and determine if job descriptions are available.  Obtain security access records for the folder where job descriptions are maintained and determine whether non-HR employees have write access.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10612,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10612,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10613,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10613,10611,1,'Evidence exists indicating that employees have the requisite knowledge and skills.','Management specifies the level of competence needed for particular jobs and translates the desired levels of competence into requisite knowledge and skills.','Obtain copies of hiring procedures and determine whether procedures include guidance for ensuring employees wit requisite skills are hired.  Select a sample of 25 employees and note whether recent evaluations in accordance with policy are available.  Note what their rating is and chart to the bell curve.  If any low performers were selected in the sample, determine if appropriate action was taken.  Obtain documentation of the requirements for the annual performance review system and select a sample of 25 employees and determine if performance reviews were conducted in accordance with policy.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10613,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10613,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10615,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10615,10614,1,'The Board consists of members independent from management.','An active and effective board, or committees thereof, provides an oversight function in ensuring effective internal control.','Obtain the Bylaws and determine whether review of independence is required.  If anyone has been admitted during the current year, obtain documentation supporting this review/evaluation.  Obtain a sample of 5 questionnaires and determine if questions regarding independence were addressed.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10615,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10615,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10616,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10616,10614,1,'The Board constructively challenges management''s planned decisions, e.g., strategic initiatives and major transactions, and probes for explanations of past results.','An active and effective board, or committees thereof, provides an oversight function in ensuring effective internal control.','Obtain a sample of minutes from 10 meetings of the Board and Committees (10 in total) and note whether members asked probing questions, and whether financial statements (where appropriate) and/or other information provided was reviewed by the members present.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10616,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10616,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10617,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10617,10614,1,'Board committees are used where warranted, e.g., Audit Committee, Credit Committee.','An active and effective board, or committees thereof, provides an oversight function in ensuring effective internal control.','The Board has implemented the committees it deems necessary to address Alamosa''s needs - no test required.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10617,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10617,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10618,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10618,10614,1,'Directors have sufficient knowledge, industry experience, and time to serve effectively.','An active and effective board, or committees thereof, provides an oversight function in ensuring effective internal control.','Obtain a sample of 5 questionnaires and determine if questions regarding competencies were addressed.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10618,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10618,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10619,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10619,10614,1,'The Audit Committee meets with the CFO, internal and external auditors frequently.','An active and effective board, or committees thereof, provides an oversight function in ensuring effective internal control.','Obtain minutes of two meetings during the year to determine whether the Audit Committee met with the CFO and the internal and external auditors.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10619,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10619,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10620,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10620,10614,1,'The Audit Committee reviews the scope of activities of the internal and external auditors annually.','An active and effective board, or committees thereof, provides an oversight function in ensuring effective internal control.','Obtain minutes showing compliance with the description of the existing control.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10620,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10620,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10621,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10621,10614,1,'The Audit Committee meets privately with the CFO and internal and external auditors to discuss the reasonableness of the financial reporting process, system of internal control, significant comments and recommendations, and management''s performance.','An active and effective board, or committees thereof, provides an oversight function in ensuring effective internal control.','Obtain minutes of two meetings during the year to determine whether the Audit Committee met privately with the external auditors.  ');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10621,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10621,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10622,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10622,10614,1,'The Board regularly receives timely key information such as financial statements, significant contracts, and major marketing initiatives.','An active and effective board, or committees thereof, provides an oversight function in ensuring effective internal control.','Obtain minutes of two meetings during the year to determine whether the Board received timely information (e.g., financial statements, etc.)');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10622,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10622,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10623,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10623,10614,1,'The Board or Audit Committee is apprised timely of sensitive information, investigations, and improper acts.  A process exists for informing the Board or Audit Committee of significant issues.','Obtain minutes of two meetings during the year to determine whether the Audit Committee received timely information about fraud, follow-up status, etc.','Obtain minutes of two meetings during the year to determine whether the Audit Committee received timely information about fraud, follow-up status, etc.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10623,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10623,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10624,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10624,10614,1,'The Audit Committee deals with compensation and retention issues regarding the internal auditor.','An active and effective board, or committees thereof, provides an oversight function in ensuring effective internal control.','Obtain minutes that indicate that the Audit Committee considered compensation and retention of the externally-provided internal audit function.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10624,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10624,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10625,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10625,10614,1,'The Compensation Committee approves all management incentive plans tied to performance.','An active and effective board, or committees thereof, provides an oversight function in ensuring effective internal control.','Obtain minutes of two meetings of the Compensation Committee and note whether short- and long-term incentives were properly approved.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10625,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10625,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10626,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10626,10614,1,'The Board specifically addresses management''s adherence to the code of conduct.','An active and effective board, or committees thereof, provides an oversight function in ensuring effective internal control.','Obtain minutes that indicate that the Baord addressed management''s adherence to the code of conduct in the Code of Business Conduct and Ethics.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10626,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10626,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10627,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10627,10614,1,'The Board and Audit Committee are involved sufficiently in monitoring the effectiveness of the ''tone at the top.''','An active and effective board, or committees thereof, provides an oversight function in ensuring effective internal control.','Obtain minutes that indicate that the Board evaluated the CEO and the ''tone at the top'' that he sets.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10627,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10627,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10628,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10628,10614,1,'The Board and Audit Committee are involved sufficiently in monitoring the effectiveness of the ''tone at the top.''','An active and effective board, or committees thereof, provides an oversight function in ensuring effective internal control.','Obtain minutes of two meetings of the Audit Committee and note whether any of the issues that impact the ''tone at the top'' were addressed.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10628,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10628,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10629,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10629,10614,1,'As a result of its findings, the Board or Audit Committee takes actions including special investigations, as needed.','An active and effective board, or committees thereof, provides an oversight function in ensuring effective internal control.','Obtain the Governance Committee charter and determine if there is provision for investigations as required by the expected control.  Obtain minutes of two meetings and determine if activities around investigations (requesting an investigation, following up on an existing investigation, etc.) are addressed.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10629,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10629,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10630,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10630,10614,1,'The Audit Committee has the authority to engage independent counsel and other advisers, as it determines necessary to carry out its duties.','An active and effective board, or committees thereof, provides an oversight function in ensuring effective internal control.','Obtain the current Audit Committee charter and determine if the required provision is included.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10630,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10630,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10631,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10631,10614,1,'The Audit Committee is active in inquiring about fraud and other related issues.','An active and effective board, or committees thereof, provides an oversight function in ensuring effective internal control.','Obtain copies of the Audit Committee meeting agendas since May 3, 2004 and determine if the fraud agenda item is included.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10631,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10631,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10632,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10632,10614,1,'The Company has designated someone to report fraud and related issues to the Audit Committee.','An active and effective board, or committees thereof, provides an oversight function in ensuring effective internal control.','Obtain minutes and presentation materials demonstating this.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10632,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10632,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10633,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10633,10614,1,'There is formal documentation via meeting agendas, minutes, presentations made, etc. documenting fraud related issues covered, including questions asked.','An active and effective board, or committees thereof, provides an oversight function in ensuring effective internal control.','Obtain copies of the Audit Committee meeting agendas since May 3, 2004 and determine if the fraud agenda item is included.


Obtain minutes and presentation materials demonstating this.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10633,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10633,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10635,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10635,10634,1,'Management has a policy that analyzes the risks and benefits of a potential venture.','Management is active and effective in providing oversight, guidance and enhancing an effective control environment.','Obtain the practices and processes documentation and determine if it addresses analyzing the risks and benefits of a potential venture.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10635,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10635,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10636,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10636,10634,1,'There is a low level of personnel turnover in key functions such as accounting, operating, data processing, and internal audit.','Management is active and effective in providing oversight, guidance and enhancing an effective control environment.','No testing necessary.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10636,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10636,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10637,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10637,10634,1,'Accounting principles are not selected based on the favourable financial results of acceptable principles.','Management is active and effective in providing oversight, guidance and enhancing an effective control environment.','No testing necessary.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10637,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10637,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10638,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10638,10634,1,'Attitudes and actions toward financial reporting, including disputes over application of accounting treatments.','Management is active and effective in providing oversight, guidance and enhancing an effective control environment.','Obtain copies of Audit Committee meeting minutes from two meetings and determine that the Committee oversees the accounting and financial reporting processes.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10638,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10638,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10639,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10639,10634,1,'Operating management sign-off on reported results.','Management is active and effective in providing oversight, guidance and enhancing an effective control environment.','Obtain route sheets from two quarterly (or one quarterly and one annual) financial statements, and determine that there was proper sign-off.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10639,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10639,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10640,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10640,10634,1,'Management''s attitude toward the data processing and accounting functions, and reliance on financial reporting.','Management is active and effective in providing oversight, guidance and enhancing an effective control environment.','No testing necessary.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10640,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10640,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10641,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10641,10634,1,'Valuable physical assets are protected from unauthorized access or use.','Management is active and effective in providing oversight, guidance and enhancing an effective control environment.','Observe the tracking system and obtain a basic understanding.  Describe results.  Obtain copies of inventory reports from the warehouse (sample of 5) to determine that assets are properly recorded & tracked.


Obtain system documentation showing the locations where identification cards are in use.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10641,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10641,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10642,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10642,10634,1,'Financial assets are protected from unauthorized access or use.','Management is active and effective in providing oversight, guidance and enhancing an effective control environment.','See testing in Treasury/Debt.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10642,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10642,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10643,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10643,10634,1,'Computer systems and networks, including intellectual assets are protected from unauthorized access or use.','Management is active and effective in providing oversight, guidance and enhancing an effective control environment.','Observe the tracking system and obtain a basic understanding.  Describe results.  Obtain copies of inventory reports from the warehouse (sample of 5) to determine that assets are properly recorded & tracked.


Obtain system documentation showing the locations where identification cards are in use.


&


See Information Systems control testing for system access.  Testing NCN for safeguarding hard-copy documents.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10643,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10643,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10644,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10644,10634,1,'Senior managers frequently visit subsidiary or divisional operations.  Group or divisional management meetings are held frequently.','Management is active and effective in providing oversight, guidance and enhancing an effective control environment.','No testing necessary.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10644,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10644,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10646,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10646,10645,1,'The organizational structure facilitates the flow of information upstream, downstream, and across all business activities.','Executives fully understand their control responsibilities and possess the requisite experience and levels of knowledge commensurate with their positions.','No testing necessary.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10646,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10646,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10647,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10647,10645,1,'Responsibilities and expectations for the entity''s business activities are communicated clearly to the executives in charge of those activities.','Executives fully understand their control responsibilities and possess the requisite experience and levels of knowledge commensurate with their positions.','Obtain documentation of objectives for the February 2004 planning session and objectives (if already available) for the September 2004 session.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10647,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10647,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10648,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10648,10645,1,'Established reporting relationships are effective and provide managers information appropriate to their responsibilities and authority.  Executives of the business activities have access to senior operating executives.','Executives fully understand their control responsibilities and possess the requisite experience and levels of knowledge commensurate with their positions.','No testing necessary.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10648,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10648,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10649,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10649,10645,1,'Management periodically evaluates the entity''s organizational structure in light of changes in the business or industry.','Executives fully understand their control responsibilities and possess the requisite experience and levels of knowledge commensurate with their positions.','No testing necessary.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10649,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10649,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10650,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10650,10645,1,'Managers and supervisors have sufficient time to carry out their responsibilities effectively.','Executives fully understand their control responsibilities and possess the requisite experience and levels of knowledge commensurate with their positions.','Select a sample of 15 action plans and determine that target dates are set and responsible parties agreed to the plan.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10650,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10650,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10652,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10652,10651,1,'Responsibility for decisions is related to assignment of authority.','Assignment of responsibility and delegation of authority to deal with organizational goals and objectives, operating functions, and regulatory requirements, including responsibility for information systems and authorization for changes.','Determine whether a schedule of authority is easily accessible to employees, is updated and is properly approved.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10652,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10652,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10653,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10653,10651,1,'Delegated authority is appropriate in relation to assigned responsibilities.','Assignment of responsibility and delegation of authority to deal with organizational goals and objectives, operating functions, and regulatory requirements, including responsibility for information systems and authorization for changes.','Determine whether a schedule of authority is easily accessible to employees, is updated and is properly approved.


Proper approval of the schedule is basis for those included being properly authorized.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10653,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10653,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10654,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10654,10651,1,'Proper information is considered in determining the level of authority and scope of responsibility assigned to an individual.','Assignment of responsibility and delegation of authority to deal with organizational goals and objectives, operating functions, and regulatory requirements, including responsibility for information systems and authorization for changes.','Determine whether a schedule of authority is easily accessible to employees, is updated and is properly approved.


Proper approval of the schedule is basis for those included being properly authorized.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10654,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10654,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10655,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10655,10651,1,'Job descriptions contain specific references to control related responsibilities.','Assignment of responsibility and delegation of authority to deal with organizational goals and objectives, operating functions, and regulatory requirements, including responsibility for information systems and authorization for changes.','Select a sample of 10 job descriptions of employees within the accounting and finance function that have specific financial reporting control responsibilities and determine if the descriptions contain control-related responsibilities.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10655,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10655,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10657,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10657,10656,1,'Existence and implementation of codes of conduct and other policies regarding acceptable business practice, conflicts of interest, or expected standards of ethical and moral behaviour.','Management demonstrates that integrity and ethical values cannot be compromised.','Obtain the Code of Business Conduct and Ethics and examine it for inclusion of codes of conduct and other policies regarding acceptable business practice, conflicts of interest, or expected standards of ethical and moral behaviour.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10657,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10657,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10658,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10658,10656,1,'Establishment of ''tone at the top'', explicit moral guidance of what is right and wrong and extent of communication throughout the organization.','Management demonstrates that integrity and ethical values cannot be compromised.','(1) Obtain the Code of Business Conduct and Ethics and examine it for establishment of the ''tone at the top'' - i.e., the Company''s expectation of ethical and moral behaviour.  (2) Observe whether whistleblower posters and Alamosa posters addressing its core values are posted at office locations and stores (10 locations).  (3) Examine the intranet and determine if Alamosa''s core values are displayed.  (4) Obtain and examine the criteria for the Champion''s Club and determine if they are consistent with Alamosa''s core values. ');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10658,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10658,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10659,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10659,10656,1,'Dealings with employees, suppliers, customers, investors, creditors, insurers, competitors, auditors, etc., conducted on a high ethical plane.','Management demonstrates that integrity and ethical values cannot be compromised.','Obtain the Code of Business Conduct and Ethics and determine if it addresses dealings with employees, suppliers, customers, investors, creditors, insurers, competitors, auditors, etc., that should be conducted on a high ethical plane.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10659,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10659,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10660,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10660,10656,1,'Appropriateness of remedial action in the event of departure from procedures, policies, or code of conduct.','Management demonstrates that integrity and ethical values cannot be compromised.','Obtain the employee handbook and the sales compensation handbook and note whether the progressive discipline is included.  ');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10660,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10660,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10661,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10661,10656,1,'Management intervention in overriding established controls is documented.  Deviations from established policies are investigated and documented.','Management demonstrates that integrity and ethical values cannot be compromised.','Obtain the Code of Business Conduct and Ethics & Employee Handbook and determine if specific instances of control override are provided for that are consistent with Alamosa''s core values and good business practices.  Note whether management approval is required.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10661,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10661,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10662,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10662,10656,1,'Performance targets do not unnecessarily test people''s ability to adhere to ethical values.','Management demonstrates that integrity and ethical values cannot be compromised.','(1) Obtain the Code of Business Conduct and Ethics and examine it for establishment of the ''tone at the top'' - i.e., the Company''s expectation of ethical and moral behaviour.  (2) Observe whether whistleblower posters and Alamosa posters addressing its core values are posted at office locations and stores (10 locations).  (3) Examine the intranet and determine if Alamosa''s core values are displayed.  (4) Obtain and examine the criteria for the Champion''s Club and determine if they are consistent with Alamosa''s core values. 


Obtain the employee handbook and the sales compensation handbook and note whether the progressive discipline is included.  ');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10662,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10662,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10663,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10663,10656,1,'Senior management has adopted a Code of Ethics','Management demonstrates that integrity and ethical values cannot be compromised.','Obtain the Code of Business Conduct and Ethics and determine if it 

addresses the following points:


(1) honest and ethical conduct, including the ethical handling of actual or apparent conflicts of interest between personal and professional relationships;

(2) full, fair, accurate, timely, and understandable disclosure in the periodic reports required to be filed by the Company;and

(3) compliance with applicable governmental rules and regulations.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10663,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10663,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10664,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10664,10656,1,'Methods of reporting violations of the Code of Business Conduct and Ethics are documented, and who the violations should be reported to.','Management demonstrates that integrity and ethical values cannot be compromised.','Obtain the Code of Business Conduct and Ethics and determine if it includes methods of reporting violations of the Code to appropriate individuals.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10664,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10664,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10665,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10665,10656,1,'Methods of reporting violations of the Code of Business Conduct and Ethics are documented, and who the violations should be reported to.','Management demonstrates that integrity and ethical values cannot be compromised.','Obtain the Code of Business Conduct and Ethics and determine if it includes methods of reporting violations of the Code to appropriate individuals.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10665,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10665,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10666,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10666,10656,1,'There is documentation of the acceptance of the Code of Business Conduct and Ethics.','Management demonstrates that integrity and ethical values cannot be compromised.','Select a sample of 25 employees and obtain the signed acknowledgement forms that they turned in to HR.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10666,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10666,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10667,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10667,10656,1,'Provision for corrective action related to violations of the Code of Business Conduct and Ethics is provided for.','Management demonstrates that integrity and ethical values cannot be compromised.','Obtain the Code of Business Conduct and Ethics and determine if provisions for corrective actions related to violations of the Code are provided for.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10667,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10667,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10668,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10668,10656,1,'An anti-fraud program and related controls exist.','Management demonstrates that integrity and ethical values cannot be compromised.','Obtain a copy of the MySafeWorkplace contract.  See other tests that address communication of Alamosa''s values and position towards fraud.  Testing of segregation of duties is covered in the control matrices for the various cycles.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10668,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10668,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10670,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10670,10669,1,'Employee background checks are completed and considered to be adequate, particularly with regard to prior actions or activities considered unacceptable to the entity.','Human resources is active in the control environment by ensuring that appropriate policies and practices are adopted, that qualified employees exhibiting integrity are hired, and that employees are properly supervised and trained.','Select a sample of 15 employees hired during the current year and determine if background (if deemed necessary) were performed (if the position requires driving, ensure a drivers license search was done).');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10670,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10670,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10671,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10671,10669,1,'Personnel policies and procedures result in recruiting and developing competent and trustworthy people necessary to support an effective internal control system.','Human resources is active in the control environment by ensuring that appropriate policies and practices are adopted, that qualified employees exhibiting integrity are hired, and that employees are properly supervised and trained.','Review the hiring guidlines and determine if appropriate language based on the expected control exists.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10671,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10671,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10672,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10672,10669,1,'Personnel policies and procedures result in training, promoting and compensating employees to support an effective internal control system.','Human resources is active in the control environment by ensuring that appropriate policies and practices are adopted, that qualified employees exhibiting integrity are hired, and that employees are properly supervised and trained.','No testing necessary.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10672,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10672,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10673,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10673,10669,1,'Employees are made aware of their responsibilities and management''s expectations of them.','Human resources is active in the control environment by ensuring that appropriate policies and practices are adopted, that qualified employees exhibiting integrity are hired, and that employees are properly supervised and trained.','Select a sample of 25 employees and determine if job descriptions are available.  Obtain security access records for the folder where job descriptions are maintained and determine whether non-HR employees have write access.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10673,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10673,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10674,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10674,10669,1,'Integrity and ethical values are criteria in performance appraisals.','Human resources is active in the control environment by ensuring that appropriate policies and practices are adopted, that qualified employees exhibiting integrity are hired, and that employees are properly supervised and trained.','Obtain an evaluation form and determine if wording from existing control is on form.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10674,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10674,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10675,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10675,10669,1,'Employees complete orientation and compliance with orientation and completion of required modules is tracked by Human Resources.','Employees are trained on ethical and compliance responsibilities and the importance of compliance with policies and procedures.','Select a sample of 15 employees that were hired during the current year and determine if orientation forms were signed by the employees.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10675,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10675,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10677,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10677,10676,1,'Entity-wide objectives provide sufficiently broad statements and guidance of what the entity desires to achieve.','For an entity to have effective control, it must have established objectives.','Determine if strategic planning documents exist for recently held strategic meetings.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10677,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10677,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10678,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10678,10676,1,'Entity-wide objectives are communicated effectively to employees and Board of Directors.','For an entity to have effective control, it must have established objectives.','No testing necessary.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10678,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10678,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10679,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10679,10676,1,'Strategies are consistent with entity-wide objectives.','For an entity to have effective control, it must have established objectives.','No testing necessary.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10679,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10679,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10680,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10680,10676,1,'Business plans and budgets are consistent with entity-wide objectives.','For an entity to have effective control, it must have established objectives.','No testing necessary.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10680,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10680,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10682,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10682,10681,1,'Activity-level objectives are linked with entity-wide objectives and strategic plans.','Objectives should be established for each significant activity and those activity-level objectives should be consistent.','No testing necessary.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10682,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10682,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10683,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10683,10681,1,'Activity-level objectives are relevant to all significant business processes.','Objectives should be established for each significant activity and those activity-level objectives should be consistent.','No testing necessary.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10683,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10683,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10684,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10684,10681,1,'Resources are adequate relative to the objectives.','Objectives should be established for each significant activity and those activity-level objectives should be consistent.','Obtain the most recent budget and determine whether it was properly approved in accordance with the strategic planning process.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10684,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10684,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10685,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10685,10681,1,'Objectives (Critical Success factors) that are important to achievement of entity-wide objectives are identified.','Objectives should be established for each significant activity and those activity-level objectives should be consistent.','Obtain the strategic planning document and determine if organizational objectives were identified.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10685,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10685,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10686,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10686,10681,1,'All levels of management are involved in objective setting.','Objectives should be established for each significant activity and those activity-level objectives should be consistent.','No testing necessary.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10686,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10686,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10688,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10688,10687,1,'Mechanisms to identify risks arising from external and internal sources are adequate.','The entity''s risk assessment process should consider the implications of relevant risks at both the entity and activity level.','Determine whether an approved (by the Audit Committee) internal audit plan exists and is based on a risk assessment.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10688,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10688,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10689,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10689,10687,1,'The risk analysis process, including assessing significance and likelihood and determining actions is thorough and relevant.','The entity''s risk assessment process should consider the implications of relevant risks at both the entity and activity level.','No testing necessary.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10689,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10689,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10690,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10690,10687,1,'There is a systematic, documented approach to assessing the risk of fraud and related risks (fraud risk factors).','The entity''s risk assessment process should consider the implications of relevant risks at both the entity and activity level.','N/A - documentation of control not yet in place.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10690,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10690,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10691,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10691,10687,1,'Mitigating controls are identified relating to identified fraud related risks.','The entity''s risk assessment process should consider the implications of relevant risks at both the entity and activity level.','N/A - documentation of control not yet in place.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10691,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10691,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10692,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10692,10687,1,'The fraud risk assessment includes consideration of risks of management override, manipulation of estimates, accuracy and completeness of financial statement disclosures and MD&A.','The entity''s risk assessment process should consider the implications of relevant risks at both the entity and activity level.','N/A - documentation of control not yet in place.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10692,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10692,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10693,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10693,10687,1,'Identified fraud and related issues are trended to identiy additional risk areas.','The entity''s risk assessment process should consider the implications of relevant risks at both the entity and activity level.','Obtain as sample of 15 IS-generated reports that are used to identify areas of higher risk, and note whether documentation of management review exists.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10693,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10693,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10694,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10694,10687,1,'There is a systematic, documented approach to reviewing fraud and related items.','The entity''s risk assessment process should consider the implications of relevant risks at both the entity and activity level.','Obtain the CFO''s notes and determine whether communication about fraud review is documented (on a quarterly basis).');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10694,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10694,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10696,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10696,10695,1,'Mechanisms exist to anticipate, identify, and react to routine events or activities that affect achievement of the entity or activity-level objectives.','Mechanisms need to be in place to identify and react to changing conditions.','Obtain a sample of 10 action plans used for individual goals.  Determine whether documentation of these goals was monitored.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10696,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10696,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10697,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10697,10695,1,'Mechanisms exist to identify and react to changes that may have a more dramatic effect of the entity and these are communicated to top management.','Mechanisms need to be in place to identify and react to changing conditions.','Obtain the notes kept by the CFO and determine whether matters pertaining to each executive''s organization were discussed.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10697,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10697,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10699,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10699,10698,1,'Appropriate policies and procedures necessary, with respect to each of the entity''s activities, exists.','Appropriate policies and procedures necessary, with respect to each of the entity''s activities, exists and are being applied properly.','This is covered in each of the cycles covered by 404 testing - pass further testing here.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10699,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10699,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10700,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10700,10698,1,'Identified control activities in place are being applied properly.','Appropriate policies and procedures necessary, with respect to each of the entity''s activities, exists and are being applied properly.','This is covered in each of the cycles covered by 404 testing - pass further testing here.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10700,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10700,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10701,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10701,10698,1,'Investigative protocols are in place regarding following up on fraud and related issues raised.','Appropriate policies and procedures necessary, with respect to each of the entity''s activities, exists and are being applied properly.','N/A - documentation of control not yet in place.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10701,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10701,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10703,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10703,10702,1,'Mechanisms are in place to obtain relevant external information-on market conditions, competitors'' programs, legislative or regulatory developments, and economic changes.','An effective information infrastructure is in place.','No testing necessary.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10703,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10703,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10704,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10704,10702,1,'Internally generated information, critical to the achievement of the business objectives, is identified and regularly reported.','An effective information infrastructure is in place.','These various reports and the controls over the data obtained are tested in the respective 404 cycles.  Pass further testing here.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10704,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10704,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10705,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10705,10702,1,'Managers receive analytical information that enables them to identify what action needs to be taken.','An effective information infrastructure is in place.','These various reports and the controls over the data obtained are tested in the respective 404 cycles.  Pass further testing here.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10705,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10705,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10706,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10706,10702,1,'Information is provided at the right level of detail for different levels of management.','An effective information infrastructure is in place.','These various reports and the controls over the data obtained are tested in the respective 404 cycles.  Pass further testing here.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10706,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10706,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10707,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10707,10702,1,'Information is available on a timely basis to allow effective monitoring of events and activities.','An effective information infrastructure is in place.','These various reports and the controls over the data obtained are tested in the respective 404 cycles.  Pass further testing here.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10707,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10707,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10708,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10708,10702,1,'Executives with sufficiently broad responsibilities determine information needs and priorities.','An effective information infrastructure is in place.','Determine which surveys were performed during the current year and select an appropriate sample to determine whether the objectives of the surveys were met.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10708,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10708,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10709,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10709,10702,1,'Sufficient resources are provided to develop/enhance information systems.','An effective information infrastructure is in place.','Obtain the budgets and business plan and determine whether appropriate consideration of IS needs exists.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10709,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10709,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10711,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10711,10710,1,'Communication vehicles are sufficient in effecting communications.','Appropriate information is communicated in a timely and effective manner.','No testing necessary.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10711,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10711,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10712,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10712,10710,1,'Employees know the objectives of their own activities.','Appropriate information is communicated in a timely and effective manner.','From the sample of evaluations obtained in ''Evidence exists indicating that employees have the requisite knowledge and skills'' determine whether individual goals were addressed.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10712,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10712,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10713,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10713,10710,1,'There is a channel to communicate upstream through someone other than a direct superior.','Appropriate information is communicated in a timely and effective manner.','(1) Obtain the Code of Business Conduct and Ethics and examine it for establishment of the ''tone at the top'' - i.e., the Company''s expectation of ethical and moral behaviour.  (2) Observe whether whistleblower posters and Alamosa posters addressing its core values are posted at office locations and stores (10 locations).  (3) Examine the intranet and determine if Alamosa''s core values are displayed.  (4) Obtain and examine the criteria for the Champion''s Club and determine if they are consistent with Alamosa''s core values. ');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10713,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10713,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10714,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10714,10710,1,'People who report suspected improprieties are provided feedback and have immunity from reprisal.','Appropriate information is communicated in a timely and effective manner.','No testing necessary.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10714,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10714,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10715,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10715,10710,1,'Information is communicated across the organization completely and in a timely manner to enable people to discharge their responsibilities.','Appropriate information is communicated in a timely and effective manner.','No testing necessary.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10715,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10715,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10716,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10716,10710,1,'Channels with customers, suppliers, and other external parties for communicating changing needs are open and effective.','Appropriate information is communicated in a timely and effective manner.','No testing necessary.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10716,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10716,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10717,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10717,10710,1,'Outside parties have been made aware of the entity''s ethical standards.','Appropriate information is communicated in a timely and effective manner.','View the referenced web page and determine if the values are listed there and the page is easily accessible from the home page.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10717,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10717,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10718,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10718,10710,1,'Improprieties by employees or external parties are reported to the appropriate personnel.','Appropriate information is communicated in a timely and effective manner.','View the online employee handbook and determine whether there is documentation of how improprieties are to be addressed.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10718,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10718,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10719,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10719,10710,1,'Communications received from customers are followed up appropriately and in a timely manner.','Appropriate information is communicated in a timely and effective manner.','No testing necessary.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10719,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10719,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10720,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10720,10710,1,'Communications received from regulators or other external parties are followed up appropriately and in a timely manner.','Appropriate information is communicated in a timely and effective manner.','N/A - documentation of control not yet in place.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10720,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10720,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10721,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10721,10710,1,'Errors in customer billings are corrected and the error is investigated and corrected.','Appropriate information is communicated in a timely and effective manner.','The controlled portion of adjustments is insignificant, therefore pass testing in this area.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10721,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10721,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10722,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10722,10710,1,'Appropriate personnel, independent of those involved with the original transaction, process complaints.','Appropriate information is communicated in a timely and effective manner.','Obtain the organization chart to determine if these two areas are properly segregated.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10722,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10722,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10723,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10723,10710,1,'Importance of being aware of fraud and related issues, and the Company''s views on these issues is effectively communicated.','Appropriate information is communicated in a timely and effective manner.','Obtain a copy of the MySafeWorkplace contract.  See other tests that address communication of Alamosa''s values and position towards fraud.  Testing of segregation of duties is covered in the control matrices for the various cycles.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10723,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10723,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10724,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10724,10710,1,'A procedure is in place to ensure that fraud and related issues are escalated to the Audit Committee.','Appropriate information is communicated in a timely and effective manner.','Obtain notes taken by the Manager of Internal Audit at the meeting to determine if fraud issues were addressed.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10724,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10724,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10726,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10726,10725,1,'Personnel, in carrying out their regular activities, obtain evidence as to whether the system of internal control continues to function.','There is ongoing monitoring of the appropriateness, completeness and effectiveness of the entity''s control environment.','This is covered in each of the cycles covered by 404 testing - pass further testing here.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10726,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10726,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10727,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10727,10725,1,'Communications from external parties corroborate internally generated information.','There is ongoing monitoring of the appropriateness, completeness and effectiveness of the entity''s control environment.','This is covered in relevant cycles covered by 404 testing - pass further testing here.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10727,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10727,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10728,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10728,10725,1,'Customers generally pay invoices or customer complaints on billings are investigated for their underlying causes.','There is ongoing monitoring of the appropriateness, completeness and effectiveness of the entity''s control environment.','No testing necessary.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10728,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10728,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10729,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10729,10725,1,'Supplier complaints regarding unfair practices by purchasing agents are fully investigated.','There is ongoing monitoring of the appropriateness, completeness and effectiveness of the entity''s control environment.','Obtain the P&P prepared by the purchasing department, and determine whether it appropriately addresses supplier complaints.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10729,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10729,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10730,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10730,10725,1,'Amounts recorded by the accounting system are periodically compared to physical assets.','There is ongoing monitoring of the appropriateness, completeness and effectiveness of the entity''s control environment.','Observe the tracking system and obtain a basic understanding.  Describe results.  Obtain copies of inventory reports from the warehouse (sample of 5) to determine that assets are properly recorded & tracked.


Obtain system documentation showing the locations where identification cards are in use.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10730,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10730,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10731,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10731,10725,1,'Executives with proper authority decide on which auditor recommendations regarding means to strengthen internal controls are implemented.','There is ongoing monitoring of the appropriateness, completeness and effectiveness of the entity''s control environment.','No testing necessary.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10731,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10731,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10732,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10732,10725,1,'Personnel are required periodically to acknowledge compliance with the code of conduct.','There is ongoing monitoring of the appropriateness, completeness and effectiveness of the entity''s control environment.','Select a sample of 25 employees and obtain the signed acknowledgement forms that they turned in to HR.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10732,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10732,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10733,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10733,10725,1,'Internal audit has an appropriate position within the organization.','There is ongoing monitoring of the appropriateness, completeness and effectiveness of the entity''s control environment.','Obtain the Internal Audit Charter Statement and determine whether the appropriate wording is included.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10733,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10733,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10734,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10734,10725,1,'Staff members are experienced and competent.','There is ongoing monitoring of the appropriateness, completeness and effectiveness of the entity''s control environment.','No testing necessary.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10734,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10734,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10735,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10735,10725,1,'Internal audit has access to the Audit Committee.','There is ongoing monitoring of the appropriateness, completeness and effectiveness of the entity''s control environment.','Obtain the Internal Audit Charter Statement and determine whether the appropriate wording is included.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10735,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10735,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10736,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10736,10725,1,'Operating personnel are required to ''sign-off'' on the accuracy of their units'' financial information and are held responsible for errors discovered.','There is ongoing monitoring of the appropriateness, completeness and effectiveness of the entity''s control environment.','Select two ICQs (Internal Control Questionary) completed each quarter and determine if proper signoff by the appropriate process owners was obtained.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10736,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10736,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10737,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10737,10725,1,'Signatures are required to evidence performance of specific control activities/functions.','There is ongoing monitoring of the appropriateness, completeness and effectiveness of the entity''s control environment.','Select two ICQs (Internal Control Questionary) completed each quarter and determine if proper signoff by the appropriate process owners was obtained.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10737,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10737,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10739,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10739,10738,1,'Appropriate portions of the internal control systems are evaluated.','There is effective and objective evaluation of the appropriateness, completeness and effectiveness of the entity''s control environment.','This is being evaluated as part of the 404 testing therefore pass testing here.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10739,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10739,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10740,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10740,10738,1,'The evaluation was conducted by personnel with the requisite skills.','There is effective and objective evaluation of the appropriateness, completeness and effectiveness of the entity''s control environment.','This is being evaluated as part of the 404 testing therefore pass testing here.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10740,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10740,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10741,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10741,10738,1,'The evaluation process is appropriate.','There is effective and objective evaluation of the appropriateness, completeness and effectiveness of the entity''s control environment.','This is being evaluated as part of the 404 testing therefore pass testing here.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10741,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10741,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10742,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10742,10738,1,'An understanding is gained of how the system is supposed to work and how it actually does work.','There is effective and objective evaluation of the appropriateness, completeness and effectiveness of the entity''s control environment.','This is being evaluated as part of the 404 testing therefore pass testing here.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10742,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10742,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10743,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10743,10738,1,'The methodology for evaluating the system is logical and appropriate.','There is effective and objective evaluation of the appropriateness, completeness and effectiveness of the entity''s control environment.','This is being evaluated as part of the 404 testing therefore pass testing here.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10743,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10743,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10744,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10744,10738,1,'The evaluation is adequately planned and coordinated.','There is effective and objective evaluation of the appropriateness, completeness and effectiveness of the entity''s control environment.','');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10744,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10744,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10745,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10745,10738,1,'Documentation is available and appropriate.','There is effective and objective evaluation of the appropriateness, completeness and effectiveness of the entity''s control environment.','This is being evaluated as part of the 404 testing therefore pass testing here.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10745,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10745,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10747,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10747,10746,1,'A mechanism exists for capturing and reporting identified internal control weaknesses.','Noted deficiencies are appropriately and timely reported, and properly followed up.','Obtain findings status reports presented to the Audit Committee for two quarters.  Also obtain and review the findings database for evidence that the status of findings is being monitored.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10747,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10747,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10748,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10748,10746,1,'Deficiencies are reported to a person directly responsible for the activity and a person at least one level higher.','Noted deficiencies are appropriately and timely reported, and properly followed up.','Obtain findings status reports presented to the Audit Committee for two quarters.  Also obtain and review the findings database for evidence that the status of findings is being monitored.


Additionally, note whether those responsible for ensuring the corrective actions are taken are listed and whether there is an action plan included.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10748,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10748,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10749,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10749,10746,1,'Follow-up actions are appropriate and ensure necessary action is taken.','Noted deficiencies are appropriately and timely reported, and properly followed up.','Obtain findings status reports presented to the Audit Committee for two quarters.  Also obtain and review the findings database for evidence that the status of findings is being monitored.


Additionally, note whether follow actions are properly.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10749,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10749,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10750,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10750,10746,1,'Any fraud, whether or not material, that involves management or other employees who have a significant role in the Companyâs internal controls or financial reporting is reported to the Audit Committee.','Noted deficiencies are appropriately and timely reported, and properly followed up.','Obtain the presentation materials presented at the Audit Committee meeting and the minutes.  Determine whether fraud concerning management or other employees who have a significant role in the Companyâs internal controls or financial reporting is reported to the Audit Committee - whether or not any has occurred.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10750,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10750,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10751,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10751,10746,1,'Any fraud that is significant to the recording of transactions or accounting policies, significant to internal controls, has a significant effect on the company''s Company''s financial statements is reported to the Audit Committee.','Noted deficiencies are appropriately and timely reported, and properly followed up.gnificant to the Company or its operations, significant to the recording of transactions or accounting policies, significant to internal controls, is significant to audit-related','Obtain the presentation materials presented at the Audit Committee meeting and the minutes.  Determine whether fraud concerning management or other employees who have a significant role in the Companyâs internal controls or financial reporting is reported to the Audit Committee - whether or not any has occurred.


Determine whether this is reported on - whether or not any has occurred.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10751,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10751,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10752,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (10752,10746,1,'Reporting of suspected ethical, fraud, and/or compliance issues on an anonymous basis is available.','Noted deficiencies are appropriately and timely reported, and properly followed up.','(1) Obtain the Code of Business Conduct and Ethics and examine it for establishment of the ''tone at the top'' - i.e., the Company''s expectation of ethical and moral behaviour.  (2) Observe whether whistleblower posters and Alamosa posters addressing its core values are posted at office locations and stores (10 locations).  (3) Examine the intranet and determine if Alamosa''s core values are displayed.  (4) Obtain and examine the criteria for the Champion''s Club and determine if they are consistent with Alamosa''s core values. ');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10752,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10752,2902,NOW());

