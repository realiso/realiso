CREATE DATABASE %database_name%
WITH OWNER = %database_login%
ENCODING = 'UTF8'
TABLESPACE = pg_default;