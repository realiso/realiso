INSERT INTO isms_context (pkContext,nType,nState) VALUES (300,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (300,100,'Efluentes orgânicos','Alteração da qualidade da água');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,300,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,300,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (301,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (301,100,'Efluentes orgânicos','Contaminação do solo');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,301,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,301,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (302,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (302,100,'Efluentes orgânicos','Impacto visual no meio ambiente');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,302,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,302,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (303,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (303,100,'Efluentes orgânicos','Danos a vegetação');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,303,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,303,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (304,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (304,100,'Efluentes orgânicos','Incômodo à vizinhança');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,304,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,304,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (305,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (305,100,'Efluentes orgânicos','Destino incorreto');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,305,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,305,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (306,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (306,100,'Efluentes industriais','Alteração da qualidade da água');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,306,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,306,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (307,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (307,100,'Efluentes industriais','Contaminação do solo');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,307,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,307,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (308,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (308,100,'Efluentes industriais','Impacto visual no meio ambiente');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,308,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,308,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (309,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (309,100,'Efluentes industriais','Danos a vegetação');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,309,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,309,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (310,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (310,100,'Efluentes industriais','Incômodo à vizinhança');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,310,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,310,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (311,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (311,100,'Efluentes industriais','Destino incorreto');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,311,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,311,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (312,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (312,100,'Emissões de material particulado','Alteração da qualidade do ar');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,312,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,312,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (313,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (313,100,'Emissões de material particulado','Alteração da camada de ozônio');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,313,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,313,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (314,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (314,100,'Emissões de material particulado','Danos à saúde do funcionário');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,314,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,314,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (315,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (315,100,'Emissões de material particulado','Danos a vegetação');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,315,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,315,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (316,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (316,100,'Emissões de material particulado','Incômodo à vizinhança');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,316,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,316,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (317,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (317,100,'Emissões CFC''s','Alteração da qualidade do ar');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,317,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,317,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (318,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (318,100,'Emissões CFC''s','Aumento do efeito estufa');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,318,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,318,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (319,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (319,100,'Emissões CFC''s','Alteração da camada de ozônio');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,319,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,319,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (320,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (320,100,'Emissões CFC''s','Danos à saúde do funcionário');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,320,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,320,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (321,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (321,100,'Emissões CFC''s','Danos a vegetação');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,321,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,321,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (322,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (322,100,'Emissões CFC''s','Incômodo à vizinhança');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,322,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,322,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (323,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (323,100,'Emissões de SOx, NOx, COx Pb, Sn, Hidrocarbonetos','Alteração da qualidade do ar');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,323,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,323,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (324,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (324,100,'Emissões de SOx, NOx, COx Pb, Sn, Hidrocarbonetos','Aumento do efeito estufa');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,324,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,324,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (325,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (325,100,'Emissões de SOx, NOx, COx Pb, Sn, Hidrocarbonetos','Alteração da camada de ozônio');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,325,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,325,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (326,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (326,100,'Emissões de SOx, NOx, COx Pb, Sn, Hidrocarbonetos','Danos à saúde do funcionário');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,326,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,326,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (327,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (327,100,'Emissões de SOx, NOx, COx Pb, Sn, Hidrocarbonetos','Danos a vegetação');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,327,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,327,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (328,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (328,100,'Emissões de SOx, NOx, COx Pb, Sn, Hidrocarbonetos','Incômodo à vizinhança');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,328,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,328,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (329,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (329,100,'Geração de Ruído','Danos à saúde do funcionário');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,329,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,329,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (330,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (330,100,'Geração de Ruído','Incômodo à vizinhança');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,330,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,330,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (331,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (331,100,'Emissão de Radiação','Alteração da qualidade do ar');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,331,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,331,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (332,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (332,100,'Emissão de Radiação','Danos à saúde do funcionário');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,332,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,332,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (333,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (333,100,'Geração de Poeira do processo','Alteração da qualidade do ar');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,333,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,333,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (334,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (334,100,'Geração de Poeira do processo','Danos à saúde do funcionário');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,334,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,334,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (335,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (335,100,'Geração de Poeira do processo','Incômodo à vizinhança');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,335,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,335,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (336,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (336,100,'Consumo de energia elétrica','Esgotamento de recursos naturais');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,336,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,336,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (337,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (337,100,'Consumo de derivados de petróleo','Alteração da qualidade da água');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,337,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,337,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (338,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (338,100,'Consumo de derivados de petróleo','Contaminação do solo');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,338,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,338,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (339,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (339,100,'Consumo de derivados de petróleo','Impacto visual no meio ambiente');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,339,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,339,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (340,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (340,100,'Consumo de derivados de petróleo','Danos a vegetação');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,340,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,340,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (341,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (341,100,'Consumo de derivados de petróleo','Incômodo à vizinhança');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,341,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,341,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (342,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (342,100,'Consumo de derivados de petróleo','Destino incorreto');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,342,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,342,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (343,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (343,100,'Consumo de água','Esgotamento de recursos naturais');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,343,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,343,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (344,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (344,100,'Consumo de Lenha/Carvão e derivados','Esgotamento de recursos naturais');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,344,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,344,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (345,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (345,100,'Consumo de gás','Esgotamento de recursos naturais');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,345,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,345,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (346,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (346,100,'Consumo de gás','Incômodo à vizinhança');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,346,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,346,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (347,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (347,100,'Ligas metálicas, fios','Destino incorreto');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,347,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,347,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (348,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (348,100,'Ligas metálicas, fios','Impacto visual no meio ambiente');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,348,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,348,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (349,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (349,100,'Borrachas','Destino incorreto');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,349,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,349,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (350,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (350,100,'Borrachas','Impacto visual no meio ambiente');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,350,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,350,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (351,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (351,100,'Resíduo classe I','Alteração da qualidade da água');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,351,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,351,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (352,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (352,100,'Resíduo classe I','Contaminação do solo');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,352,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,352,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (353,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (353,100,'Resíduo classe I','Impacto visual no meio ambiente');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,353,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,353,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (354,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (354,100,'Resíduo classe I','Danos a vegetação');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,354,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,354,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (355,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (355,100,'Resíduo classe I','Incômodo à vizinhança');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,355,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,355,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (356,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (356,100,'Resíduo classe I','Destino incorreto');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,356,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,356,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (357,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (357,100,'EPI''s / Panos / Tecido de Forração / Carpete','Destino incorreto');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,357,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,357,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (358,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (358,100,'EPI''s / Panos / Tecido de Forração / Carpete','Impacto visual no meio ambiente');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,358,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,358,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (359,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (359,100,'Lixo Ambulatorial','Alteração da qualidade da água');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,359,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,359,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (360,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (360,100,'Lixo Ambulatorial','Contaminação do solo');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,360,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,360,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (361,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (361,100,'Lixo Ambulatorial','Impacto visual no meio ambiente');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,361,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,361,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (362,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (362,100,'Lixo Ambulatorial','Danos a vegetação');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,362,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,362,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (363,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (363,100,'Lixo Ambulatorial','Incômodo à vizinhança');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,363,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,363,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (364,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (364,100,'Lixo Ambulatorial','Destino incorreto');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,364,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,364,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (365,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (365,100,'Lâmpadas fluorescentes, vapor de sódio ou mercúrio','Alteração da qualidade da água');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,365,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,365,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (366,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (366,100,'Lâmpadas fluorescentes, vapor de sódio ou mercúrio','Contaminação do solo');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,366,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,366,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (367,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (367,100,'Lâmpadas fluorescentes, vapor de sódio ou mercúrio','Impacto visual no meio ambiente');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,367,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,367,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (368,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (368,100,'Lâmpadas fluorescentes, vapor de sódio ou mercúrio','Danos a vegetação');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,368,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,368,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (369,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (369,100,'Lâmpadas fluorescentes, vapor de sódio ou mercúrio','Incômodo à vizinhança');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,369,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,369,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (370,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (370,100,'Lâmpadas fluorescentes, vapor de sódio ou mercúrio','Destino incorreto');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,370,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,370,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (371,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (371,100,'Solventes Sujos/Limpo (acetona, alcool, isoparafina, thinner)','Alteração da qualidade da água');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,371,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,371,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (372,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (372,100,'Solventes Sujos/Limpo (acetona, alcool, isoparafina, thinner)','Contaminação do solo');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,372,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,372,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (373,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (373,100,'Solventes Sujos/Limpo (acetona, alcool, isoparafina, thinner)','Impacto visual no meio ambiente');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,373,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,373,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (374,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (374,100,'Solventes Sujos/Limpo (acetona, alcool, isoparafina, thinner)','Danos a vegetação');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,374,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,374,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (375,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (375,100,'Solventes Sujos/Limpo (acetona, alcool, isoparafina, thinner)','Incômodo à vizinhança');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,375,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,375,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (376,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (376,100,'Solventes Sujos/Limpo (acetona, alcool, isoparafina, thinner)','Destino incorreto');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,376,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,376,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (377,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (377,100,'Duratex / Madeira / Fórmica','Destino incorreto');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,377,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,377,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (378,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (378,100,'Duratex / Madeira / Fórmica','Impacto visual no meio ambiente');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,378,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,378,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (379,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (379,100,'Varredura (Pó de varrição, lã de vidro, termo acústico, cinzas, gesso, areia, bombril, abrasivos, lixas, cotonetes, pinceis, cera, restos de massa poliéster, fita adesiva, filtros, couro, etc)','Destino incorreto');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,379,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,379,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (380,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (380,100,'Varredura (Pó de varrição, lã de vidro, termo acústico, cinzas, gesso, areia, bombril, abrasivos, lixas, cotonetes, pinceis, cera, restos de massa poliéster, fita adesiva, filtros, couro, etc)','Impacto visual no meio ambiente');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,380,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,380,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (381,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (381,100,'Espumas / Isopor / Napa / Vulcouro / Taraflex / Poliuretano Expandido / ABS / teflon','Destino incorreto');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,381,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,381,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (382,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (382,100,'Espumas / Isopor / Napa / Vulcouro / Taraflex / Poliuretano Expandido / ABS / teflon','Impacto visual no meio ambiente');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,382,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,382,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (383,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (383,100,'Fibras','Destino incorreto');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,383,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,383,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (384,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (384,100,'Fibras','Impacto visual no meio ambiente');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,384,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,384,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (385,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (385,100,'Papel e Papelão Limpos','Destino incorreto');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,385,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,385,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (386,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (386,100,'Papel e Papelão Limpos','Impacto visual no meio ambiente');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,386,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,386,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (387,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (387,100,'Papel/Plástico contaminado (resina, cola, tinta, óleo, graxa, ácido)','Alteração da qualidade da água');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,387,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,387,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (388,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (388,100,'Papel/Plástico contaminado (resina, cola, tinta, óleo, graxa, ácido)','Contaminação do solo');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,388,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,388,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (389,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (389,100,'Papel/Plástico contaminado (resina, cola, tinta, óleo, graxa, ácido)','Impacto visual no meio ambiente');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,389,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,389,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (390,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (390,100,'Papel/Plástico contaminado (resina, cola, tinta, óleo, graxa, ácido)','Danos a vegetação');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,390,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,390,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (391,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (391,100,'Papel/Plástico contaminado (resina, cola, tinta, óleo, graxa, ácido)','Incômodo à vizinhança');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,391,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,391,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (392,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (392,100,'Papel/Plástico contaminado (resina, cola, tinta, óleo, graxa, ácido)','Destino incorreto');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,392,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,392,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (393,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (393,100,'Plástico Duro (embalagens, bombonas, peças, perfis, mangueiras, cintas, etc) / Plástico Mole (embalagens, filmes, etc) / Copos','Destino incorreto');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,393,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,393,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (394,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (394,100,'Plástico Duro (embalagens, bombonas, peças, perfis, mangueiras, cintas, etc) / Plástico Mole (embalagens, filmes, etc) / Copos','Impacto visual no meio ambiente');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,394,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,394,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (395,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (395,100,'Tonner, Cartuchos de copiadoras e impressoras','Destino incorreto');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,395,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,395,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (396,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (396,100,'Tonner, Cartuchos de copiadoras e impressoras','Impacto visual no meio ambiente');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,396,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,396,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (397,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (397,100,'Óleo sujo, borra de óleo e Graxas, filtros de óleo','Alteração da qualidade da água');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,397,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,397,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (398,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (398,100,'Óleo sujo, borra de óleo e Graxas, filtros de óleo','Contaminação do solo');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,398,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,398,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (399,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (399,100,'Óleo sujo, borra de óleo e Graxas, filtros de óleo','Impacto visual no meio ambiente');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,399,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,399,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (400,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (400,100,'Óleo sujo, borra de óleo e Graxas, filtros de óleo','Danos a vegetação');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,400,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,400,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (401,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (401,100,'Óleo sujo, borra de óleo e Graxas, filtros de óleo','Incômodo à vizinhança');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,401,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,401,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (402,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (402,100,'Óleo sujo, borra de óleo e Graxas, filtros de óleo','Destino incorreto');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,402,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,402,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (403,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (403,100,'Equipamentos e acessórios elétricos e eletrônicos (Fiações,Pilhas, Baterias, Sensores, Fusíveis, Relés,etc)','Alteração da qualidade da água');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,403,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,403,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (404,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (404,100,'Equipamentos e acessórios elétricos e eletrônicos (Fiações,Pilhas, Baterias, Sensores, Fusíveis, Relés,etc)','Contaminação do solo');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,404,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,404,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (405,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (405,100,'Equipamentos e acessórios elétricos e eletrônicos (Fiações,Pilhas, Baterias, Sensores, Fusíveis, Relés,etc)','Impacto visual no meio ambiente');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,405,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,405,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (406,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (406,100,'Equipamentos e acessórios elétricos e eletrônicos (Fiações,Pilhas, Baterias, Sensores, Fusíveis, Relés,etc)','Danos a vegetação');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,406,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,406,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (407,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (407,100,'Equipamentos e acessórios elétricos e eletrônicos (Fiações,Pilhas, Baterias, Sensores, Fusíveis, Relés,etc)','Incômodo à vizinhança');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,407,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,407,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (408,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (408,100,'Equipamentos e acessórios elétricos e eletrônicos (Fiações,Pilhas, Baterias, Sensores, Fusíveis, Relés,etc)','Destino incorreto');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,408,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,408,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (409,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (409,100,'Vidros','Destino incorreto');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,409,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,409,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (410,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (410,100,'Vidros','Impacto visual no meio ambiente');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,410,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,410,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (411,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (411,100,'Sobra de processo','Destino incorreto');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,411,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,411,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (412,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (412,100,'Sobra de processo','Impacto visual no meio ambiente');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,412,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,412,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (413,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (413,100,'Borra de tinta','Alteração da qualidade da água');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,413,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,413,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (414,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (414,100,'Borra de tinta','Contaminação do solo');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,414,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,414,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (415,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (415,100,'Borra de tinta','Impacto visual no meio ambiente');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,415,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,415,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (416,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (416,100,'Borra de tinta','Danos a vegetação');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,416,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,416,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (417,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (417,100,'Borra de tinta','Incômodo à vizinhança');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,417,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,417,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (418,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (418,100,'Borra de tinta','Destino incorreto');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,418,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,418,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (419,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (419,100,'Papel contaminado (resíduo de higienização pessoal)','Alteração da qualidade da água');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,419,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,419,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (420,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (420,100,'Papel contaminado (resíduo de higienização pessoal)','Contaminação do solo');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,420,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,420,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (421,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (421,100,'Papel contaminado (resíduo de higienização pessoal)','Impacto visual no meio ambiente');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,421,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,421,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (422,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (422,100,'Papel contaminado (resíduo de higienização pessoal)','Danos a vegetação');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,422,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,422,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (423,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (423,100,'Papel contaminado (resíduo de higienização pessoal)','Incômodo à vizinhança');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,423,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,423,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (424,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (424,100,'Papel contaminado (resíduo de higienização pessoal)','Destino incorreto');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,424,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,424,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (425,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (425,100,'Resíduos Orgânicos','Alteração da qualidade da água');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,425,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,425,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (426,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (426,100,'Resíduos Orgânicos','Contaminação do solo');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,426,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,426,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (427,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (427,100,'Resíduos Orgânicos','Impacto visual no meio ambiente');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,427,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,427,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (428,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (428,100,'Resíduos Orgânicos','Danos a vegetação');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,428,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,428,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (429,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (429,100,'Resíduos Orgânicos','Incômodo à vizinhança');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,429,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,429,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (430,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (430,100,'Resíduos Orgânicos','Destino incorreto');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,430,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,430,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (431,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (431,100,'Solução ácida','Alteração da qualidade da água');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,431,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,431,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (432,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (432,100,'Solução ácida','Contaminação do solo');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,432,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,432,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (433,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (433,100,'Solução ácida','Impacto visual no meio ambiente');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,433,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,433,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (434,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (434,100,'Solução ácida','Danos a vegetação');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,434,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,434,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (435,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (435,100,'Solução ácida','Incômodo à vizinhança');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,435,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,435,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (436,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (436,100,'Solução ácida','Destino incorreto');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,436,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,436,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (437,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (437,100,'Óleo hidráulico, água de limpeza de pisos e paredes','Alteração da qualidade da água');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,437,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,437,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (438,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (438,100,'Óleo hidráulico, água de limpeza de pisos e paredes','Contaminação do solo');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,438,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,438,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (439,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (439,100,'Óleo hidráulico, água de limpeza de pisos e paredes','Impacto visual no meio ambiente');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,439,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,439,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (440,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (440,100,'Óleo hidráulico, água de limpeza de pisos e paredes','Danos a vegetação');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,440,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,440,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (441,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (441,100,'Óleo hidráulico, água de limpeza de pisos e paredes','Incômodo à vizinhança');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,441,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,441,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (442,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (442,100,'Óleo hidráulico, água de limpeza de pisos e paredes','Destino incorreto');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,442,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,442,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (443,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (443,100,'Água condensada de vapor contaminado','Alteração da qualidade da água');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,443,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,443,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (444,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (444,100,'Água condensada de vapor contaminado','Contaminação do solo');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,444,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,444,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (445,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (445,100,'Água condensada de vapor contaminado','Impacto visual no meio ambiente');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,445,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,445,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (446,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (446,100,'Água condensada de vapor contaminado','Danos a vegetação');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,446,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,446,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (447,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (447,100,'Água condensada de vapor contaminado','Incômodo à vizinhança');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,447,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,447,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (448,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (448,100,'Água condensada de vapor contaminado','Destino incorreto');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,448,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,448,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (449,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (449,100,'Desconforto Térmico','Danos à saúde do funcionário');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,449,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,449,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (450,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (450,100,'Desconforto Respiratório','Danos à saúde do funcionário');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,450,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,450,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (451,2807,2702);
INSERT INTO rm_event (fkContext,fkCategory,sDescription,tImpact) VALUES (451,100,'Desconforto Respiratório','Incômodo à vizinhança');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,451,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,451,2902,NOW());

