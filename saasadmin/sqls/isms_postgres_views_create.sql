CREATE VIEW context_history AS
SELECT cc.fkContext as context_id, cc.dDate as context_date_created, cc.sUserName as context_creator_name, cm.dDate as context_date_modified, cm.sUserName as context_modifier_name
FROM isms_context_date cc JOIN isms_context_date cm ON (cc.fkContext = cm.fkContext AND cc.nAction < cm.nAction);

CREATE VIEW context_names AS
/*Usuário*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, t.sName as context_name FROM isms_context c JOIN isms_user t ON (c.pkContext = t.fkContext) UNION
/*Perfil*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, t.sName as context_name FROM isms_context c JOIN isms_profile t ON (c.pkContext = t.fkContext) UNION
/*Área*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, t.sName as context_name FROM isms_context c JOIN rm_area t ON (c.pkContext = t.fkContext) UNION
/*Processo*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, t.sName as context_name FROM isms_context c JOIN rm_process t ON (c.pkContext = t.fkContext) UNION
/*Ativo*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, t.sName as context_name FROM isms_context c JOIN rm_asset t ON (c.pkContext = t.fkContext) UNION
/*Risco*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, t.sName as context_name FROM isms_context c JOIN rm_risk t ON (c.pkContext = t.fkContext) UNION
/*Controle*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, t.sName as context_name FROM isms_context c JOIN rm_control t ON (c.pkContext = t.fkContext) UNION
/*Categoria*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, t.sName as context_name FROM isms_context c JOIN rm_category t ON (c.pkContext = t.fkContext) UNION
/*Evento*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, t.sDescription as context_name FROM isms_context c JOIN rm_event t ON (c.pkContext = t.fkContext) UNION
/*Melhor Prática*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, t.sName as context_name FROM isms_context c JOIN rm_best_practice t ON (c.pkContext = t.fkContext) UNION
/*Norma*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, t.sName as context_name FROM isms_context c JOIN rm_standard t ON (c.pkContext = t.fkContext) UNION
/*Relação Processo X Ativo*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, (p.sName || ' X ' || a.sName) as context_name FROM isms_context c JOIN rm_process_asset pa ON (c.pkContext = pa.fkContext)
JOIN rm_process p ON (pa.fkProcess = p.fkContext) JOIN rm_asset a ON (pa.fkAsset = a.fkContext) UNION
/*Seção de Melhor Prática*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, t.sName as context_name FROM isms_context c JOIN rm_section_best_practice t ON (c.pkContext = t.fkContext) UNION
/*Relação Risco X Controle*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, (r.sName || ' X ' || co.sName) as context_name FROM isms_context c JOIN rm_risk_control rc ON (c.pkContext = rc.fkContext)
JOIN rm_risk r ON (rc.fkRisk = r.fkContext) JOIN rm_control co ON (rc.fkControl = co.fkContext) UNION
/*Documento*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, t.sName as context_name FROM isms_context c JOIN pm_document t ON (c.pkContext = t.fkContext) UNION
/*Instância do Documento*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, (d.sName || ' (' || t.nMajorVersion::text || '.' || t.nRevisionVersion::text) || ')' as context_name 
FROM isms_context c JOIN pm_doc_instance t ON (c.pkContext = t.fkContext) JOIN pm_document d ON (t.fkDocument = d.fkContext) UNION
/*Categoria de Incidente*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, t.sName as context_name FROM isms_context c JOIN ci_category t ON(c.pkContext = t.fkContext) UNION
/*Ocorrência*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, CAST(t.tDescription as varchar) as context_name FROM isms_context c JOIN ci_occurrence t ON(c.pkContext = t.fkContext) UNION
/*Incidente*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, t.sName as context_name FROM isms_context c JOIN ci_incident t ON(c.pkContext = t.fkContext) UNION
/*Não Conformidade*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, t.sName as context_name FROM isms_context c JOIN ci_nc t ON(c.pkContext = t.fkContext) UNION
/*Template de Documento*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, t.sName as context_name FROM isms_context c JOIN pm_template t ON (c.pkContext = t.fkContext) UNION
/*Relação entre Incidente e Risco*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, '' as context_name FROM isms_context c JOIN ci_incident_risk ir ON (ir.fkContext = c.pkContext) UNION
/*Relação entre Incidente e Controle*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, (ctrl.sName || ' -> ' || i.sName) as context_name FROM isms_context c JOIN ci_incident_control ic ON (c.pkContext = ic.fkContext)
JOIN rm_control ctrl ON (ic.fkControl = ctrl.fkContext) JOIN ci_incident i ON (ic.fkIncident = i.fkContext) UNION
/*Tolerância ao Risco*/
SELECT c.pkcontext AS context_id, c.ntype AS context_type, c.nstate AS context_state, '' AS context_name FROM isms_context c JOIN rm_risk_limits rl ON c.pkcontext = rl.fkcontext UNION
/*Solução da Categoria de Incidente*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, CAST(s.tProblem as varchar) as context_name FROM isms_context c JOIN ci_solution s ON(c.pkContext = s.fkContext) UNION
/*Plano de Ação*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, ap.sName as context_name FROM isms_context c JOIN ci_action_plan ap ON(c.pkContext = ap.fkContext) UNION
/*Registro*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, r.sName as context_name FROM isms_context c JOIN pm_register r ON(c.pkContext = r.fkContext);


CREATE VIEW view_pm_document_active AS
SELECT 
  d.fkContext,
  d.fkClassification,
  d.fkCurrentVersion,
  d.fkParent,
  d.fkAuthor,
  d.fkMainApprover,
  d.sName,
  d.tDescription,
  d.nType,
  d.sKeywords,
  d.dDateProduction,
  d.dDeadline,
  d.bFlagDeadlineAlert,
  d.bFlagDeadlineExpired,
  d.nDaysBefore,
  d.bHasApproved,
  d.fkSchedule
FROM
  isms_context c
  JOIN pm_document d ON (c.pkContext = d.fkContext AND c.nState <> 2705);

CREATE VIEW view_pm_doc_instance_active AS
SELECT
	i.fkContext, i.fkDocument, i.nMajorVersion, i.nRevisionVersion, i.tRevisionJustification, i.sPath, i.tModifyComment,
	i.dBeginProduction, i.dEndProduction, i.sFilename, i.bIsLink, i.sLink,i.dBeginApprover,i.dBeginRevision,i.sManualVersion
FROM isms_context c 
  JOIN pm_doc_instance i ON (c.pkContext = i.fkContext AND c.nState <> 2705)
  JOIN isms_context ctx_doc ON (ctx_doc.pkContext = i.fkDocument AND ctx_doc.nState <> 2705);

CREATE VIEW view_pm_di_active_with_content AS
SELECT
	i.fkContext, i.fkDocument, i.nMajorVersion, i.nRevisionVersion, i.tRevisionJustification, i.sPath, i.tModifyComment,
	i.dBeginProduction, i.dEndProduction, i.sFilename, i.bIsLink, i.sLink, ic.tContent,i.dBeginApprover,i.dBeginRevision
FROM isms_context c 
JOIN pm_doc_instance i ON (c.pkContext = i.fkContext AND c.nState <> 2705)
JOIN isms_context ctx_doc ON (ctx_doc.pkContext = i.fkDocument AND ctx_doc.nState <> 2705)
LEFT JOIN pm_instance_content ic ON (i.fkContext = ic.fkInstance);

CREATE VIEW view_pm_di_with_content AS
SELECT
	i.fkContext, i.fkDocument, i.nMajorVersion, i.nRevisionVersion, i.tRevisionJustification, i.sPath, i.tModifyComment,
	i.dBeginProduction, i.dEndProduction, i.sFilename, i.bIsLink, i.sLink, ic.tContent
FROM isms_context c 
JOIN pm_doc_instance i ON (c.pkContext = i.fkContext)
LEFT JOIN pm_instance_content ic ON (i.fkContext = ic.fkInstance);

CREATE VIEW view_isms_user_active AS
SELECT
	u.fkContext, u.fkProfile, u.sName, u.sLogin, u.sPassword, u.sEmail, u.nIp,
	u.nLanguage, u.dLastLogin, u.nWrongLogonAttempts, u.bIsBlocked, u.bMustChangePassword, u.sRequestPassword, u.sSession, u.bAnsweredSurvey, u.bShowHelp, u.bIsAD
FROM isms_context c JOIN isms_user u ON (c.pkContext = u.fkContext AND c.nState <> 2705);


CREATE VIEW view_rm_area_active AS
SELECT
	a.fkContext, a.fkPriority, a.fkType, a.fkParent, a.fkResponsible, a.sName,
	a.tDescription, a.sDocument, a.nValue
FROM isms_context c JOIN rm_area a ON (c.pkContext = a.fkContext AND c.nState <> 2705);

CREATE VIEW view_rm_process_active AS
SELECT
	p.fkContext, p.fkPriority, p.fkType, p.fkArea, p.fkResponsible, p.sName,
	p.tDescription, p.sDocument, p.nValue, p.tJustification
FROM isms_context c JOIN rm_process p ON (c.pkContext = p.fkContext AND c.nState <> 2705);

CREATE VIEW view_rm_process_asset_active AS
SELECT fkContext, fkProcess, fkAsset FROM rm_process_asset
WHERE
fkProcess NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2802 AND nState = 2705)
AND
fkAsset NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2803 AND nState = 2705);

CREATE VIEW view_rm_risk_control_active AS
SELECT fkContext, fkProbabilityValueName, fkRisk, fkControl, tJustification FROM rm_risk_control
WHERE
fkRisk NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2804 AND nState = 2705)
AND
fkControl NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2805 AND nState = 2705);

CREATE VIEW view_rm_risk_active AS
SELECT
	r.fkContext, r.fkProbabilityValueName, r.fkType, r.fkEvent, r.fkAsset, r.sName,
	r.tDescription, r.nValue, r.nValueResidual, r.tJustification, r.nAcceptMode, r.nAcceptState,
	r.sAcceptJustification, r.nCost, r.tImpact
FROM isms_context c JOIN rm_risk r ON (c.pkContext = r.fkContext AND c.nState <> 2705);

CREATE VIEW view_rm_asset_active AS
SELECT
	a.fkContext, a.fkCategory, a.fkResponsible, a.fkSecurityResponsible, a.sName,
	a.tDescription, a.sDocument, a.nCost, a.nValue, a.bLegality, a.tJustification
FROM isms_context c JOIN rm_asset a ON (c.pkContext = a.fkContext AND c.nState <> 2705);

CREATE VIEW view_pm_process_user_active AS
SELECT fkProcess, fkUser FROM pm_process_user
WHERE
fkProcess NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2802 AND nState = 2705)
AND
fkUser NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2816 AND nState = 2705);

CREATE VIEW view_rm_control_bp_active AS
SELECT fkContext, fkBestPractice, fkControl FROM rm_control_best_practice
WHERE
fkBestPractice NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2808 AND nState = 2705)
AND
fkControl NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2805 AND nState = 2705);

CREATE VIEW view_rm_control_active AS
SELECT
	ctr.fkContext, ctr.fkType, ctr.fkResponsible, ctr.sName, ctr.tDescription,
	ctr.sDocument, ctr.sEvidence, ctr.nDaysBefore, ctr.bIsActive, ctr.bFlagImplAlert, ctr.bFlagImplExpired,
	ctr.dDateDeadline, ctr.dDateImplemented, ctr.nImplementationState
FROM isms_context c JOIN rm_control ctr ON (c.pkContext = ctr.fkContext AND c.nState <> 2705);

CREATE VIEW view_rm_best_practice_active AS
SELECT
	bp.fkContext, bp.fkSectionBestPractice, bp.sName, bp.tDescription,
	bp.nControlType, bp.sClassification, bp.tImplementationGuide, bp.tMetric, bp.sDocument, bp.tJustification
FROM isms_context c JOIN rm_best_practice bp ON (c.pkContext = bp.fkContext AND c.nState <> 2705);

CREATE VIEW view_isms_context_active AS
SELECT c.pkContext, c.nType, c.nState
FROM isms_context c WHERE c.nState <> 2705;

CREATE VIEW view_rm_asset_asset_active AS
SELECT fkAsset, fkDependent
FROM rm_asset_asset
WHERE
fkAsset NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2803 AND nState = 2705)
AND
fkDependent NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2803 AND nState = 2705);

CREATE VIEW view_rm_bp_standard_active AS
SELECT fkContext, fkStandard, fkBestPractice FROM rm_best_practice_standard
WHERE
fkStandard NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2809 AND nState = 2705)
AND
fkBestPractice NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2808 AND nState = 2705);

CREATE VIEW view_rm_bp_event_active AS
SELECT fkContext, fkBestPractice, fkEvent FROM rm_best_practice_event
WHERE
fkBestPractice NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2808 AND nState = 2705)
AND
fkEvent NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2807 AND nState = 2705);

CREATE VIEW view_rm_standard_active AS
SELECT
	s.fkContext, s.sName, s.tDescription, s.tApplication, s.tObjective
FROM isms_context c JOIN rm_standard s ON (c.pkContext = s.fkContext AND c.nState <> 2705);

CREATE VIEW view_pm_register_active AS
SELECT
	r.fkContext, r.fkResponsible, r.fkClassification, r.fkDocument, r.sName, r.nPeriod, r.nValue,
	r.sStoragePlace, r.sStorageType, r.sIndexingType, r.sDisposition, r.sProtectionRequirements, r.sOrigin
FROM isms_context c JOIN pm_register r ON (c.pkContext = r.fkContext AND c.nState <> 2705);

CREATE VIEW view_isms_context_hist_active AS
SELECT h.pkId, h.fkContext, h.sType, h.dDate, h.nValue
FROM isms_context c RIGHT JOIN isms_context_history h ON (c.pkContext = h.fkContext AND c.nState <> 2705);

CREATE VIEW view_isms_profile_active AS
SELECT p.fkContext, p.sName, p.nId
FROM isms_context c JOIN isms_profile p ON (c.pkContext = p.fkContext AND c.nState <> 2705);

CREATE VIEW view_pm_doc_registers_active AS
SELECT fkRegister, fkDocument FROM pm_doc_registers
WHERE
fkDocument NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2823 AND nState = 2705)
AND
fkRegister NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2826 AND nState = 2705);

CREATE VIEW view_pm_doc_context_active AS
SELECT fkContext, fkDocument FROM pm_doc_context
WHERE
fkContext NOT IN (SELECT pkContext FROM isms_context WHERE nState = 2705)
AND
fkDocument NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2823 AND nState = 2705);

CREATE VIEW view_pm_doc_approvers_active AS
SELECT fkDocument, fkUser, bHasApproved FROM pm_doc_approvers
WHERE
fkDocument NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2823 AND nState = 2705)
AND
fkUser NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2816 AND nState = 2705);

CREATE VIEW view_pm_doc_readers_active AS
SELECT fkUser, fkDocument, bManual, bDenied, bHasRead FROM pm_doc_readers
WHERE
fkDocument NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2823 AND nState = 2705)
AND
fkUser NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2816 AND nState = 2705);

CREATE VIEW view_isms_scope_active AS
SELECT s.fkContext, s.tDescription
FROM isms_context c JOIN isms_scope s ON (c.pkContext = s.fkContext AND c.nState <> 2705);

CREATE VIEW view_isms_policy_active AS
SELECT p.fkContext, p.tDescription
FROM isms_context c JOIN isms_policy p ON (c.pkContext = p.fkContext AND c.nState <> 2705);

CREATE VIEW view_rm_category_active AS
SELECT cat.fkContext, cat.fkParent, cat.sName, cat.tDescription
FROM isms_context c JOIN rm_category cat ON (c.pkContext = cat.fkContext AND c.nState <> 2705);

CREATE VIEW view_rm_event_active AS
SELECT e.fkContext, e.fkType, e.fkCategory, e.sDescription, e.tObservation, e.bPropagate, e.tImpact
FROM isms_context c JOIN rm_event e ON (c.pkContext = e.fkContext AND c.nState <> 2705);

CREATE VIEW view_rm_sec_bp_active AS
SELECT s.fkContext, s.fkParent, s.sName
FROM isms_context c JOIN rm_section_best_practice s ON (c.pkContext = s.fkContext AND c.nState <> 2705);

CREATE VIEW view_pm_template_active AS
SELECT t.fkContext, t.sName, t.nContextType, t.sPath, t.sFileName, t.tDescription, t.sKeywords,t.nfilesize
FROM isms_context c JOIN pm_template t ON (c.pkContext = t.fkContext AND c.nState <> 2705);

CREATE VIEW view_pm_tp_active_with_content AS
SELECT
  t.fkContext, t.sName, t.nContextType, t.sPath, t.sFileName, t.tDescription, t.sKeywords, tc.tContent, t.nfilesize
FROM isms_context c 
JOIN pm_template t ON (c.pkContext = t.fkContext AND c.nState <> 2705)
LEFT JOIN pm_template_content tc ON (t.fkContext = tc.fkContext);

CREATE VIEW view_pm_template_bp_active AS
SELECT tbp.fkTemplate, 
  tbp.fkBestPractice
FROM pm_template_best_practice tbp
JOIN isms_context cbp ON (cbp.pkContext = tbp.fkBestPractice AND cbp.nState <> 2705)
JOIN isms_context ct ON (ct.pkContext = tbp.fkTemplate AND ct.nState <> 2705);

CREATE VIEW view_ci_solution_active AS
SELECT s.fkContext, s.fkCategory, s.tProblem, s.tSolution, s.tKeywords
FROM isms_context c JOIN ci_solution s ON (c.pkContext = s.fkContext AND c.nState <> 2705);

CREATE OR REPLACE VIEW view_ci_nc_active AS 
 SELECT nc.fkcontext, nc.fkcontrol, nc.fkresponsible, nc.sname, nc.nseqnumber, nc.tdescription, nc.tcause, nc.tdenialjustification, nc.nclassification, nc.fksender, nc.ncapability, nc.ddatesent
   FROM isms_context c
   JOIN ci_nc nc ON c.pkcontext = nc.fkcontext AND c.nstate <> 2705;


CREATE VIEW view_ci_incident_active AS
SELECT i.fkContext, i.fkCategory, i.fkResponsible, i.sName, i.tAccountsPlan, i.tEvidences, i.nLossType,
	i.tEvidenceRequirementComment, i.dDateLimit, i.dDateFinish, i.tDisposalDescription, i.tSolutionDescription,
	i.tProductService, i.bNotEmailDP, i.bEmailDPSent, i.dDate
FROM isms_context c JOIN ci_incident i ON (c.pkContext = i.fkContext AND c.nState <> 2705);

CREATE VIEW view_ci_category_active AS
SELECT cat.fkContext, cat.sName
FROM isms_context c JOIN ci_category cat ON (c.pkContext = cat.fkContext AND c.nState <> 2705);

CREATE VIEW view_ci_incident_risk_active AS
SELECT ir.fkContext, ir.fkRisk,ir.fkIncident
FROM ci_incident_risk ir
JOIN isms_context ctx_r ON (ctx_r.pkContext = ir.fkRisk AND ctx_r.nState <> 2705)
JOIN isms_context ctx_i ON (ctx_i.pkContext = ir.fkIncident AND ctx_i.nState <> 2705);

CREATE VIEW view_ci_inc_control_active AS
SELECT ic.fkContext, ic.fkControl,ic.fkIncident
FROM ci_incident_control ic
JOIN isms_context ctx_c ON (ctx_c.pkContext = ic.fkControl AND ctx_c.nState <> 2705)
JOIN isms_context ctx_i ON (ctx_i.pkContext = ic.fkIncident AND ctx_i.nState <> 2705);

CREATE VIEW view_ci_inc_process_active AS
SELECT ip.fkContext, ip.fkProcess,ip.fkIncident
FROM ci_incident_process ip
JOIN isms_context ctx_p ON (ctx_p.pkContext = ip.fkProcess AND ctx_p.nState <> 2705)
JOIN isms_context ctx_i ON (ctx_i.pkContext = ip.fkIncident AND ctx_i.nState <> 2705);

CREATE VIEW view_ci_nc_process_active AS
SELECT np.fkProcess,np.fkNC
FROM ci_nc_process np
JOIN isms_context ctx_p ON (ctx_p.pkContext = np.fkProcess AND ctx_p.nState <> 2705)
JOIN isms_context ctx_n ON (ctx_n.pkContext = np.fkNC AND ctx_n.nState <> 2705);

CREATE VIEW view_ci_occurrence_active AS
SELECT o.fkContext, o.fkIncident, o.tDescription, o.tDenialJustification, o.dDate
FROM isms_context c JOIN ci_occurrence o ON (c.pkContext = o.fkContext AND c.nState <> 2705);

CREATE VIEW view_ci_incident_user_active AS
SELECT fkUser, fkIncident, tDescription, tActionTaken FROM ci_incident_user
WHERE
fkUser NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2816 AND nState = 2705)
AND
fkIncident NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2831 AND nState = 2705);

CREATE VIEW view_pm_published_docs AS
SELECT
  d.fkContext,
  d.fkClassification,
  d.fkCurrentVersion,
  d.fkParent,
  d.fkAuthor,
  d.fkMainApprover,
  d.sName,
  d.tDescription,
  d.nType,
  d.sKeywords,
  d.dDateProduction,
  d.dDeadline,
  d.bFlagDeadlineAlert,
  d.bFlagDeadlineExpired,
  d.nDaysBefore,
  d.bHasApproved,
  d.fkSchedule
FROM
  pm_document d
  JOIN isms_context c ON (c.pkContext = d.fkContext)
  JOIN pm_doc_instance di ON (di.fkContext = d.fkCurrentVersion)
WHERE
  (c.nState = 2752 AND d.dDateProduction < NOW())
  OR ( c.nState = 2751 AND di.nMajorVersion > 0 )
  OR ( c.nState = 2753 AND di.dEndProduction IS NOT NULL );


CREATE VIEW view_pm_read_docs AS
SELECT
  d.fkContext,
  d.fkClassification,
  d.fkCurrentVersion,
  d.fkParent,
  d.fkAuthor,
  d.fkMainApprover,
  d.sName,
  d.tDescription,
  d.nType,
  d.sKeywords,
  d.dDateProduction,
  d.dDeadline,
  d.bFlagDeadlineAlert,
  d.bFlagDeadlineExpired,
  d.nDaysBefore,
  d.bHasApproved,
  d.fkSchedule
FROM
  view_pm_document_active d
  JOIN pm_doc_read_history rh ON (rh.fkInstance = d.fkCurrentVersion);

CREATE OR REPLACE VIEW view_ci_action_plan_active AS 
 SELECT ac.fkcontext, ac.fkresponsible, ac.sname, ac.tactionplan, ac.nactiontype, ac.ddatedeadline, ac.ddateconclusion, ac.bisefficient, ac.ddateefficiencyrevision, ac.ddateefficiencymeasured, ac.ndaysbefore, ac.bflagrevisionalert, ac.bflagrevisionalertlate, ac.sdocument, ac.trevisionjustification
   FROM isms_context c
   JOIN ci_action_plan ac ON c.pkcontext = ac.fkcontext AND c.nstate <> 2705;


CREATE VIEW view_pm_doc_instance_status AS
SELECT
  d.fkContext AS document_id,
  di.fkContext AS doc_instance_id,
  CASE
    WHEN
      d.fkCurrentVersion IS NULL
      OR ( c.nState = 2751 AND di.nMajorVersion = 0 )
      THEN 2751
    WHEN
      c.nState = 2753
      AND di.dBeginProduction IS NULL
      THEN 2753
    WHEN
      d.fkCurrentVersion = di.fkContext
      AND (
        c.nState = 2752
        OR ( c.nState = 2751 AND di.nMajorVersion > 0 )
        OR ( c.nState = 2753 AND di.dEndProduction IS NOT NULL )
      )
      THEN
        CASE
          WHEN d.dDateProduction < NOW() THEN 2752
          ELSE 2756
        END
    WHEN
      di.fkDocument = d.fkContext
      AND di.fkContext != d.fkCurrentVersion
      AND di.dEndProduction IS NOT NULL
      THEN 2755
    ELSE 2754
  END AS doc_instance_status
FROM
  pm_document d
  JOIN isms_context c ON (c.pkContext = d.fkContext)
  LEFT JOIN pm_doc_instance di ON (di.fkDocument = d.fkContext);

CREATE VIEW view_pm_user_roles AS
SELECT
  u.fkContext AS user_id,
  d.fkContext AS document_id,
  CASE
    WHEN u.fkContext = d.fkMainApprover THEN 1
    ELSE 0
  END AS user_is_main_approver,
  CASE
    WHEN u.fkContext = d.fkAuthor THEN 1
    ELSE 0
  END AS user_is_author,
  CASE
    WHEN u.fkContext IN ( SELECT da.fkUser FROM pm_doc_approvers da WHERE da.fkDocument = d.fkContext ) THEN 1
    ELSE 0
  END AS user_is_approver,
  CASE
    WHEN u.fkContext IN ( SELECT dr.fkUser FROM pm_doc_readers dr WHERE dr.fkDocument = d.fkContext ) THEN 1
    ELSE 0
  END AS user_is_reader
FROM
  view_isms_user_active u,
  view_pm_document_active d;

CREATE OR REPLACE VIEW context_document_names AS 
 SELECT c.pkcontext AS context_id, c.ntype AS context_type, c.nstate AS context_state, t.sname AS context_name
 FROM isms_context c
 JOIN rm_area t ON c.pkcontext = t .fkcontext
 UNION 
 SELECT c.pkcontext AS context_id, c.ntype AS context_type, c.nstate AS context_state, t.sname AS context_name
 FROM isms_context c
 JOIN rm_process t ON c.pkcontext = t .fkcontext
 UNION 
 SELECT c.pkcontext AS context_id, c.ntype AS context_type, c.nstate AS context_state, t.sname AS context_name
 FROM isms_context c
 JOIN rm_asset t ON c.pkcontext = t .fkcontext
 UNION 
 SELECT c.pkcontext AS context_id, c.ntype AS context_type, c.nstate AS context_state, t.sname AS context_name
 FROM isms_context c
 JOIN rm_control t ON c.pkcontext = t .fkcontext
 UNION 
 SELECT c.pkcontext AS context_id, c.ntype AS context_type, c.nstate AS context_state, t.sname AS context_name
 FROM isms_context c
 JOIN rm_best_practice t ON c.pkcontext = t .fkcontext
 UNION 
 SELECT c.pkcontext AS context_id, c.ntype AS context_type, c.nstate AS context_state, ap.sname AS context_name
 FROM isms_context c
 JOIN ci_action_plan ap ON c.pkcontext = ap .fkcontext;
